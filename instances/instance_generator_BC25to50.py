#! /usr/bin/python3

from math import ceil
from math import floor
from random import randint
from random import seed
from random import shuffle
import sys
import os

def genInstance(numItems, binCapacity, l_min, l_max, nColors, suff, half, instCount = 1):
  if half and nColors != 2:
    raise Exception("Invalid configuration")
  
  for count in range(1, instCount + 1):

    lb = max(floor(binCapacity * l_min), 1)
    ub = min(ceil(binCapacity * l_max), binCapacity - 1)
    
    bins = []
    for i in range(ceil(numItems/3)):
      i1 = randint(lb, ub)
      i2 = randint(lb, binCapacity - i1 - lb)
      i3 = binCapacity - i1 - i2
      bins.append([i1, i2, i3])

    items = []
    for bin in bins:
      bin.sort()
      if half:
        items.append([bin[0], 0])
        items.append([bin[1], 1])
        items.append([bin[2], 1])
      else:
        counter = [0 for i in range(nColors)]
        for it in bin:
          c = randint(0, nColors - 1)
          while 2 * (counter[c] + 1) > len(bin) + 1:
            c = randint(0, nColors - 1)
          items.append([it, c])
          counter[c] += 1
    items.sort()

    name = "N%dW%dR2550%s-%03d" % (len(items), binCapacity, suff, count)
    with open(outdir + "/" + name, "w") as instFile:
      instFile.write("%d %d %d\n" % (len(items), nColors, binCapacity))
      for size, color in items:
        instFile.write("%d %d\n" % (size, color))

if __name__ == "__main__":
  programDir = sys.argv[0][:sys.argv[0].rfind("/")]

  numItems = [102, 501, 2001, 10002]
  binCapacitys = [1001, 1000001]
  l_min = 0.25
  l_max = 0.5
  colors = [2, 5, 20]

  for nColors in colors:
    outdir = programDir + "/Random25to50Q" + str(nColors)
    if(not os.path.exists(outdir)):
      os.mkdir(outdir)
    else:
      os.system("rm " + outdir + "/*")
    for n in numItems:
      for w in binCapacitys:
        genInstance(n, w, l_min, l_max, nColors, "Q" + str(nColors), False, instCount = 5)
  
  outdir = programDir + "/Random25to50HalfQ2"
  if(not os.path.exists(outdir)):
    os.mkdir(outdir)
  else:
    os.system("rm " + outdir + "/*")
  for n in numItems:
    for w in binCapacitys:
      genInstance(n, w, l_min, l_max, 2, "Q2Half", True, instCount = 5)
