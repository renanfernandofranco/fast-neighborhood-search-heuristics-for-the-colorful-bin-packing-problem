import os
from glob import glob

instancesDirsName = ["FalkenauerT-Q2", "Random25to50Q2", "AI-Q2"]

for instancesDir in instancesDirsName:
    instancesDir2 = instancesDir[:-1] + "N"
    os.system("rm -rf " + instancesDir2)
    os.system("mkdir " + instancesDir2)

    for instance in sorted(glob(os.path.join(instancesDir, "*")), key=os.path.getsize):
        print ("Running instance", instance)
        instanceName = instance[instance.rfind('/') + 1:]

        inst = open(instancesDir + '/' + instanceName, "r")
        lines = [[int(h) for h in x[:-1].split(" ")] for x in inst.readlines()]
        n, q, w = lines[0]
        lines = lines[1:]
        
        if "-Q" in instanceName:
            instanceName = instanceName.split("-Q")[0] + "-QN"
        else:
            instanceName = instanceName.split("Q")[0] + "QN-" + instanceName.split("-")[1]

        instOut = open(instancesDir2 + '/' + instanceName, "w")
        instOut.write(f"{n} {n} {w}\n")

        for i in range(n):
            instOut.write(f"{lines[i][0]} {i}\n")
    