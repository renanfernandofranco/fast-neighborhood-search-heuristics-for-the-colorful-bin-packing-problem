#! /usr/bin/python3

from math import ceil
from math import floor
from random import randint
from random import seed
from random import shuffle
import sys
import os

def genInstance(numItems, binCapacity, l_min, l_max, nColors, instCount = 1):
  for count in range(1,instCount+1):

    lb = max(floor(binCapacity * l_min), 1)
    ub = min(ceil(binCapacity * l_max), binCapacity - 1)
    
    items = []
    for i in range(3 * ceil(numItems/3)):
      items.append([randint(lb, ub), randint(0,  nColors - 1)])
    items.sort()

    name = "N%dW%dR1080Q%d-%03d" % (len(items), binCapacity, nColors, count)
    with open(outdir + "/" + name, "w") as instFile:
      instFile.write("%d %d %d\n" % (len(items), nColors, binCapacity))
      for size, color in items:
        instFile.write("%d %d\n" % (size, color))

if __name__ == "__main__":
  programDir = sys.argv[0][:sys.argv[0].rfind("/")]

  numItems = [102, 501, 2001, 10002]
  binCapacitys = [1001, 1000001]
  l_min = 0.1
  l_max = 0.8
  colors = [2, 5, 20]

  for nColors in colors:
    outdir = programDir + "/Random10to80Q" + str(nColors)
    if(not os.path.exists(outdir)):
      os.mkdir(outdir)
    else:
      os.system("rm " + outdir + "/*")
    for n in numItems:
      for w in binCapacitys:
        genInstance(n, w, l_min, l_max, nColors, instCount = 5)
  
