Capicity Bin: 1000001
Lower Bound: 886

Bins used: 891
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 526199 Color: 4
Size: 473802 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 542983 Color: 1
Size: 457018 Color: 2

Bin 3: 1 of cap free
Amount of items: 2
Items: 
Size: 577378 Color: 3
Size: 422622 Color: 1

Bin 4: 1 of cap free
Amount of items: 2
Items: 
Size: 595360 Color: 4
Size: 404640 Color: 3

Bin 5: 1 of cap free
Amount of items: 2
Items: 
Size: 669001 Color: 0
Size: 330999 Color: 3

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 727287 Color: 2
Size: 272713 Color: 0

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 744497 Color: 0
Size: 255503 Color: 2

Bin 8: 2 of cap free
Amount of items: 2
Items: 
Size: 510513 Color: 1
Size: 489486 Color: 2

Bin 9: 2 of cap free
Amount of items: 2
Items: 
Size: 546003 Color: 1
Size: 453996 Color: 2

Bin 10: 3 of cap free
Amount of items: 2
Items: 
Size: 782585 Color: 0
Size: 217413 Color: 3

Bin 11: 4 of cap free
Amount of items: 2
Items: 
Size: 658912 Color: 3
Size: 341085 Color: 4

Bin 12: 4 of cap free
Amount of items: 2
Items: 
Size: 709772 Color: 0
Size: 290225 Color: 4

Bin 13: 5 of cap free
Amount of items: 3
Items: 
Size: 381054 Color: 3
Size: 365151 Color: 1
Size: 253791 Color: 0

Bin 14: 7 of cap free
Amount of items: 2
Items: 
Size: 510021 Color: 0
Size: 489973 Color: 4

Bin 15: 8 of cap free
Amount of items: 2
Items: 
Size: 789679 Color: 3
Size: 210314 Color: 0

Bin 16: 9 of cap free
Amount of items: 2
Items: 
Size: 756817 Color: 2
Size: 243175 Color: 3

Bin 17: 10 of cap free
Amount of items: 3
Items: 
Size: 382318 Color: 1
Size: 381861 Color: 2
Size: 235812 Color: 1

Bin 18: 10 of cap free
Amount of items: 2
Items: 
Size: 640882 Color: 2
Size: 359109 Color: 4

Bin 19: 10 of cap free
Amount of items: 2
Items: 
Size: 654318 Color: 1
Size: 345673 Color: 2

Bin 20: 11 of cap free
Amount of items: 2
Items: 
Size: 548752 Color: 0
Size: 451238 Color: 2

Bin 21: 12 of cap free
Amount of items: 2
Items: 
Size: 592374 Color: 3
Size: 407615 Color: 0

Bin 22: 12 of cap free
Amount of items: 2
Items: 
Size: 604478 Color: 1
Size: 395511 Color: 3

Bin 23: 12 of cap free
Amount of items: 2
Items: 
Size: 687748 Color: 3
Size: 312241 Color: 2

Bin 24: 12 of cap free
Amount of items: 2
Items: 
Size: 772242 Color: 4
Size: 227747 Color: 1

Bin 25: 14 of cap free
Amount of items: 2
Items: 
Size: 795918 Color: 4
Size: 204069 Color: 0

Bin 26: 15 of cap free
Amount of items: 2
Items: 
Size: 627665 Color: 0
Size: 372321 Color: 4

Bin 27: 15 of cap free
Amount of items: 2
Items: 
Size: 675477 Color: 4
Size: 324509 Color: 2

Bin 28: 15 of cap free
Amount of items: 2
Items: 
Size: 722908 Color: 4
Size: 277078 Color: 3

Bin 29: 16 of cap free
Amount of items: 2
Items: 
Size: 534065 Color: 1
Size: 465920 Color: 0

Bin 30: 16 of cap free
Amount of items: 2
Items: 
Size: 563603 Color: 0
Size: 436382 Color: 2

Bin 31: 16 of cap free
Amount of items: 2
Items: 
Size: 600736 Color: 3
Size: 399249 Color: 1

Bin 32: 16 of cap free
Amount of items: 2
Items: 
Size: 636008 Color: 3
Size: 363977 Color: 0

Bin 33: 17 of cap free
Amount of items: 2
Items: 
Size: 558141 Color: 4
Size: 441843 Color: 2

Bin 34: 18 of cap free
Amount of items: 2
Items: 
Size: 784366 Color: 1
Size: 215617 Color: 3

Bin 35: 19 of cap free
Amount of items: 2
Items: 
Size: 738175 Color: 4
Size: 261807 Color: 3

Bin 36: 21 of cap free
Amount of items: 2
Items: 
Size: 505787 Color: 4
Size: 494193 Color: 3

Bin 37: 22 of cap free
Amount of items: 2
Items: 
Size: 720651 Color: 1
Size: 279328 Color: 4

Bin 38: 22 of cap free
Amount of items: 2
Items: 
Size: 757394 Color: 0
Size: 242585 Color: 2

Bin 39: 22 of cap free
Amount of items: 2
Items: 
Size: 793577 Color: 2
Size: 206402 Color: 0

Bin 40: 23 of cap free
Amount of items: 2
Items: 
Size: 786913 Color: 3
Size: 213065 Color: 4

Bin 41: 24 of cap free
Amount of items: 2
Items: 
Size: 506100 Color: 3
Size: 493877 Color: 1

Bin 42: 25 of cap free
Amount of items: 2
Items: 
Size: 774238 Color: 1
Size: 225738 Color: 0

Bin 43: 26 of cap free
Amount of items: 2
Items: 
Size: 595444 Color: 3
Size: 404531 Color: 2

Bin 44: 27 of cap free
Amount of items: 2
Items: 
Size: 636539 Color: 1
Size: 363435 Color: 2

Bin 45: 27 of cap free
Amount of items: 2
Items: 
Size: 770140 Color: 4
Size: 229834 Color: 2

Bin 46: 28 of cap free
Amount of items: 2
Items: 
Size: 742383 Color: 2
Size: 257590 Color: 1

Bin 47: 29 of cap free
Amount of items: 3
Items: 
Size: 413855 Color: 4
Size: 410540 Color: 0
Size: 175577 Color: 1

Bin 48: 29 of cap free
Amount of items: 2
Items: 
Size: 701937 Color: 4
Size: 298035 Color: 0

Bin 49: 29 of cap free
Amount of items: 2
Items: 
Size: 725730 Color: 4
Size: 274242 Color: 0

Bin 50: 30 of cap free
Amount of items: 2
Items: 
Size: 522266 Color: 3
Size: 477705 Color: 2

Bin 51: 30 of cap free
Amount of items: 2
Items: 
Size: 555319 Color: 0
Size: 444652 Color: 2

Bin 52: 30 of cap free
Amount of items: 2
Items: 
Size: 610034 Color: 1
Size: 389937 Color: 4

Bin 53: 33 of cap free
Amount of items: 2
Items: 
Size: 534503 Color: 2
Size: 465465 Color: 1

Bin 54: 33 of cap free
Amount of items: 2
Items: 
Size: 598924 Color: 4
Size: 401044 Color: 0

Bin 55: 33 of cap free
Amount of items: 2
Items: 
Size: 603917 Color: 3
Size: 396051 Color: 2

Bin 56: 33 of cap free
Amount of items: 2
Items: 
Size: 773086 Color: 4
Size: 226882 Color: 0

Bin 57: 34 of cap free
Amount of items: 2
Items: 
Size: 680416 Color: 2
Size: 319551 Color: 0

Bin 58: 35 of cap free
Amount of items: 2
Items: 
Size: 684370 Color: 1
Size: 315596 Color: 0

Bin 59: 35 of cap free
Amount of items: 2
Items: 
Size: 768835 Color: 1
Size: 231131 Color: 4

Bin 60: 36 of cap free
Amount of items: 2
Items: 
Size: 500028 Color: 2
Size: 499937 Color: 0

Bin 61: 39 of cap free
Amount of items: 2
Items: 
Size: 604822 Color: 4
Size: 395140 Color: 1

Bin 62: 39 of cap free
Amount of items: 2
Items: 
Size: 680184 Color: 0
Size: 319778 Color: 3

Bin 63: 40 of cap free
Amount of items: 2
Items: 
Size: 612464 Color: 1
Size: 387497 Color: 2

Bin 64: 41 of cap free
Amount of items: 2
Items: 
Size: 571726 Color: 0
Size: 428234 Color: 1

Bin 65: 41 of cap free
Amount of items: 2
Items: 
Size: 631204 Color: 4
Size: 368756 Color: 2

Bin 66: 41 of cap free
Amount of items: 2
Items: 
Size: 769497 Color: 1
Size: 230463 Color: 4

Bin 67: 45 of cap free
Amount of items: 2
Items: 
Size: 686767 Color: 2
Size: 313189 Color: 0

Bin 68: 45 of cap free
Amount of items: 2
Items: 
Size: 734789 Color: 3
Size: 265167 Color: 4

Bin 69: 46 of cap free
Amount of items: 2
Items: 
Size: 540428 Color: 3
Size: 459527 Color: 2

Bin 70: 46 of cap free
Amount of items: 2
Items: 
Size: 651302 Color: 0
Size: 348653 Color: 1

Bin 71: 49 of cap free
Amount of items: 2
Items: 
Size: 550528 Color: 2
Size: 449424 Color: 1

Bin 72: 49 of cap free
Amount of items: 2
Items: 
Size: 642080 Color: 0
Size: 357872 Color: 2

Bin 73: 50 of cap free
Amount of items: 2
Items: 
Size: 544106 Color: 0
Size: 455845 Color: 1

Bin 74: 51 of cap free
Amount of items: 2
Items: 
Size: 647105 Color: 2
Size: 352845 Color: 1

Bin 75: 52 of cap free
Amount of items: 2
Items: 
Size: 526859 Color: 4
Size: 473090 Color: 2

Bin 76: 52 of cap free
Amount of items: 2
Items: 
Size: 750895 Color: 2
Size: 249054 Color: 4

Bin 77: 54 of cap free
Amount of items: 2
Items: 
Size: 635440 Color: 2
Size: 364507 Color: 1

Bin 78: 55 of cap free
Amount of items: 2
Items: 
Size: 736836 Color: 0
Size: 263110 Color: 4

Bin 79: 57 of cap free
Amount of items: 2
Items: 
Size: 590813 Color: 4
Size: 409131 Color: 3

Bin 80: 57 of cap free
Amount of items: 2
Items: 
Size: 778026 Color: 0
Size: 221918 Color: 1

Bin 81: 58 of cap free
Amount of items: 2
Items: 
Size: 518833 Color: 2
Size: 481110 Color: 0

Bin 82: 58 of cap free
Amount of items: 2
Items: 
Size: 542108 Color: 3
Size: 457835 Color: 1

Bin 83: 58 of cap free
Amount of items: 2
Items: 
Size: 631745 Color: 0
Size: 368198 Color: 1

Bin 84: 60 of cap free
Amount of items: 6
Items: 
Size: 171910 Color: 4
Size: 171320 Color: 2
Size: 171256 Color: 0
Size: 171188 Color: 4
Size: 171132 Color: 1
Size: 143135 Color: 0

Bin 85: 61 of cap free
Amount of items: 2
Items: 
Size: 668352 Color: 0
Size: 331588 Color: 1

Bin 86: 62 of cap free
Amount of items: 2
Items: 
Size: 799188 Color: 4
Size: 200751 Color: 1

Bin 87: 63 of cap free
Amount of items: 3
Items: 
Size: 364185 Color: 1
Size: 362638 Color: 3
Size: 273115 Color: 1

Bin 88: 63 of cap free
Amount of items: 2
Items: 
Size: 787707 Color: 3
Size: 212231 Color: 0

Bin 89: 65 of cap free
Amount of items: 2
Items: 
Size: 661382 Color: 0
Size: 338554 Color: 2

Bin 90: 65 of cap free
Amount of items: 2
Items: 
Size: 760050 Color: 4
Size: 239886 Color: 2

Bin 91: 66 of cap free
Amount of items: 2
Items: 
Size: 701638 Color: 1
Size: 298297 Color: 0

Bin 92: 66 of cap free
Amount of items: 2
Items: 
Size: 761025 Color: 1
Size: 238910 Color: 4

Bin 93: 68 of cap free
Amount of items: 2
Items: 
Size: 644941 Color: 4
Size: 354992 Color: 3

Bin 94: 68 of cap free
Amount of items: 2
Items: 
Size: 724944 Color: 3
Size: 274989 Color: 4

Bin 95: 69 of cap free
Amount of items: 2
Items: 
Size: 510268 Color: 3
Size: 489664 Color: 4

Bin 96: 69 of cap free
Amount of items: 2
Items: 
Size: 594562 Color: 2
Size: 405370 Color: 4

Bin 97: 69 of cap free
Amount of items: 2
Items: 
Size: 637737 Color: 4
Size: 362195 Color: 1

Bin 98: 69 of cap free
Amount of items: 2
Items: 
Size: 776039 Color: 2
Size: 223893 Color: 4

Bin 99: 69 of cap free
Amount of items: 2
Items: 
Size: 798462 Color: 1
Size: 201470 Color: 4

Bin 100: 70 of cap free
Amount of items: 2
Items: 
Size: 719938 Color: 2
Size: 279993 Color: 0

Bin 101: 70 of cap free
Amount of items: 2
Items: 
Size: 767545 Color: 2
Size: 232386 Color: 3

Bin 102: 71 of cap free
Amount of items: 2
Items: 
Size: 644653 Color: 3
Size: 355277 Color: 0

Bin 103: 72 of cap free
Amount of items: 2
Items: 
Size: 553975 Color: 1
Size: 445954 Color: 0

Bin 104: 72 of cap free
Amount of items: 2
Items: 
Size: 753524 Color: 3
Size: 246405 Color: 4

Bin 105: 73 of cap free
Amount of items: 2
Items: 
Size: 583988 Color: 2
Size: 415940 Color: 3

Bin 106: 73 of cap free
Amount of items: 2
Items: 
Size: 588267 Color: 0
Size: 411661 Color: 1

Bin 107: 73 of cap free
Amount of items: 2
Items: 
Size: 692146 Color: 4
Size: 307782 Color: 2

Bin 108: 73 of cap free
Amount of items: 2
Items: 
Size: 764278 Color: 1
Size: 235650 Color: 4

Bin 109: 74 of cap free
Amount of items: 2
Items: 
Size: 516495 Color: 2
Size: 483432 Color: 0

Bin 110: 75 of cap free
Amount of items: 3
Items: 
Size: 348412 Color: 2
Size: 345160 Color: 4
Size: 306354 Color: 0

Bin 111: 76 of cap free
Amount of items: 2
Items: 
Size: 571717 Color: 1
Size: 428208 Color: 2

Bin 112: 76 of cap free
Amount of items: 2
Items: 
Size: 682949 Color: 2
Size: 316976 Color: 1

Bin 113: 76 of cap free
Amount of items: 2
Items: 
Size: 769154 Color: 2
Size: 230771 Color: 3

Bin 114: 77 of cap free
Amount of items: 2
Items: 
Size: 605022 Color: 0
Size: 394902 Color: 2

Bin 115: 77 of cap free
Amount of items: 2
Items: 
Size: 666378 Color: 3
Size: 333546 Color: 1

Bin 116: 77 of cap free
Amount of items: 2
Items: 
Size: 736949 Color: 2
Size: 262975 Color: 0

Bin 117: 77 of cap free
Amount of items: 2
Items: 
Size: 749090 Color: 0
Size: 250834 Color: 4

Bin 118: 78 of cap free
Amount of items: 2
Items: 
Size: 557001 Color: 1
Size: 442922 Color: 4

Bin 119: 78 of cap free
Amount of items: 2
Items: 
Size: 692395 Color: 3
Size: 307528 Color: 1

Bin 120: 79 of cap free
Amount of items: 6
Items: 
Size: 179947 Color: 3
Size: 177866 Color: 1
Size: 177560 Color: 0
Size: 177060 Color: 1
Size: 176631 Color: 3
Size: 110858 Color: 0

Bin 121: 79 of cap free
Amount of items: 3
Items: 
Size: 407903 Color: 1
Size: 385100 Color: 2
Size: 206919 Color: 0

Bin 122: 79 of cap free
Amount of items: 2
Items: 
Size: 675115 Color: 1
Size: 324807 Color: 4

Bin 123: 79 of cap free
Amount of items: 2
Items: 
Size: 748138 Color: 2
Size: 251784 Color: 1

Bin 124: 80 of cap free
Amount of items: 2
Items: 
Size: 650099 Color: 3
Size: 349822 Color: 4

Bin 125: 80 of cap free
Amount of items: 2
Items: 
Size: 736276 Color: 2
Size: 263645 Color: 0

Bin 126: 80 of cap free
Amount of items: 2
Items: 
Size: 765309 Color: 2
Size: 234612 Color: 0

Bin 127: 80 of cap free
Amount of items: 2
Items: 
Size: 789346 Color: 4
Size: 210575 Color: 3

Bin 128: 84 of cap free
Amount of items: 2
Items: 
Size: 590057 Color: 2
Size: 409860 Color: 1

Bin 129: 84 of cap free
Amount of items: 2
Items: 
Size: 645387 Color: 3
Size: 354530 Color: 4

Bin 130: 86 of cap free
Amount of items: 2
Items: 
Size: 698567 Color: 1
Size: 301348 Color: 3

Bin 131: 86 of cap free
Amount of items: 2
Items: 
Size: 756139 Color: 3
Size: 243776 Color: 0

Bin 132: 88 of cap free
Amount of items: 2
Items: 
Size: 523908 Color: 4
Size: 476005 Color: 3

Bin 133: 89 of cap free
Amount of items: 2
Items: 
Size: 705618 Color: 3
Size: 294294 Color: 1

Bin 134: 90 of cap free
Amount of items: 2
Items: 
Size: 719319 Color: 2
Size: 280592 Color: 3

Bin 135: 91 of cap free
Amount of items: 2
Items: 
Size: 572713 Color: 2
Size: 427197 Color: 3

Bin 136: 91 of cap free
Amount of items: 2
Items: 
Size: 733984 Color: 0
Size: 265926 Color: 3

Bin 137: 92 of cap free
Amount of items: 2
Items: 
Size: 677228 Color: 0
Size: 322681 Color: 4

Bin 138: 92 of cap free
Amount of items: 2
Items: 
Size: 694047 Color: 2
Size: 305862 Color: 1

Bin 139: 93 of cap free
Amount of items: 3
Items: 
Size: 350846 Color: 4
Size: 350324 Color: 0
Size: 298738 Color: 4

Bin 140: 93 of cap free
Amount of items: 2
Items: 
Size: 629982 Color: 2
Size: 369926 Color: 0

Bin 141: 93 of cap free
Amount of items: 2
Items: 
Size: 790807 Color: 4
Size: 209101 Color: 3

Bin 142: 94 of cap free
Amount of items: 2
Items: 
Size: 609710 Color: 3
Size: 390197 Color: 0

Bin 143: 94 of cap free
Amount of items: 2
Items: 
Size: 628100 Color: 1
Size: 371807 Color: 4

Bin 144: 94 of cap free
Amount of items: 2
Items: 
Size: 667655 Color: 0
Size: 332252 Color: 3

Bin 145: 95 of cap free
Amount of items: 2
Items: 
Size: 598470 Color: 0
Size: 401436 Color: 3

Bin 146: 95 of cap free
Amount of items: 2
Items: 
Size: 643550 Color: 2
Size: 356356 Color: 1

Bin 147: 97 of cap free
Amount of items: 2
Items: 
Size: 575916 Color: 0
Size: 423988 Color: 1

Bin 148: 98 of cap free
Amount of items: 2
Items: 
Size: 532494 Color: 2
Size: 467409 Color: 3

Bin 149: 98 of cap free
Amount of items: 2
Items: 
Size: 774663 Color: 3
Size: 225240 Color: 0

Bin 150: 100 of cap free
Amount of items: 2
Items: 
Size: 554248 Color: 3
Size: 445653 Color: 4

Bin 151: 100 of cap free
Amount of items: 2
Items: 
Size: 617817 Color: 4
Size: 382084 Color: 2

Bin 152: 100 of cap free
Amount of items: 2
Items: 
Size: 655428 Color: 1
Size: 344473 Color: 0

Bin 153: 100 of cap free
Amount of items: 2
Items: 
Size: 798402 Color: 4
Size: 201499 Color: 1

Bin 154: 101 of cap free
Amount of items: 2
Items: 
Size: 546790 Color: 4
Size: 453110 Color: 1

Bin 155: 101 of cap free
Amount of items: 2
Items: 
Size: 575124 Color: 2
Size: 424776 Color: 4

Bin 156: 101 of cap free
Amount of items: 2
Items: 
Size: 660843 Color: 1
Size: 339057 Color: 2

Bin 157: 101 of cap free
Amount of items: 2
Items: 
Size: 697017 Color: 3
Size: 302883 Color: 2

Bin 158: 101 of cap free
Amount of items: 2
Items: 
Size: 730329 Color: 2
Size: 269571 Color: 4

Bin 159: 102 of cap free
Amount of items: 2
Items: 
Size: 539944 Color: 2
Size: 459955 Color: 1

Bin 160: 102 of cap free
Amount of items: 2
Items: 
Size: 566307 Color: 3
Size: 433592 Color: 2

Bin 161: 102 of cap free
Amount of items: 2
Items: 
Size: 602478 Color: 3
Size: 397421 Color: 2

Bin 162: 102 of cap free
Amount of items: 2
Items: 
Size: 695587 Color: 0
Size: 304312 Color: 2

Bin 163: 102 of cap free
Amount of items: 2
Items: 
Size: 705262 Color: 1
Size: 294637 Color: 2

Bin 164: 103 of cap free
Amount of items: 2
Items: 
Size: 606635 Color: 0
Size: 393263 Color: 3

Bin 165: 104 of cap free
Amount of items: 2
Items: 
Size: 785510 Color: 1
Size: 214387 Color: 2

Bin 166: 106 of cap free
Amount of items: 2
Items: 
Size: 516888 Color: 2
Size: 483007 Color: 0

Bin 167: 107 of cap free
Amount of items: 2
Items: 
Size: 539621 Color: 1
Size: 460273 Color: 4

Bin 168: 107 of cap free
Amount of items: 2
Items: 
Size: 747818 Color: 4
Size: 252076 Color: 0

Bin 169: 107 of cap free
Amount of items: 2
Items: 
Size: 784644 Color: 1
Size: 215250 Color: 3

Bin 170: 109 of cap free
Amount of items: 2
Items: 
Size: 655162 Color: 2
Size: 344730 Color: 4

Bin 171: 109 of cap free
Amount of items: 2
Items: 
Size: 687338 Color: 0
Size: 312554 Color: 3

Bin 172: 110 of cap free
Amount of items: 2
Items: 
Size: 618258 Color: 4
Size: 381633 Color: 0

Bin 173: 110 of cap free
Amount of items: 2
Items: 
Size: 674570 Color: 2
Size: 325321 Color: 4

Bin 174: 110 of cap free
Amount of items: 2
Items: 
Size: 726038 Color: 2
Size: 273853 Color: 1

Bin 175: 112 of cap free
Amount of items: 2
Items: 
Size: 508483 Color: 3
Size: 491406 Color: 2

Bin 176: 112 of cap free
Amount of items: 2
Items: 
Size: 520909 Color: 2
Size: 478980 Color: 0

Bin 177: 113 of cap free
Amount of items: 6
Items: 
Size: 175322 Color: 4
Size: 174396 Color: 3
Size: 174348 Color: 3
Size: 173958 Color: 2
Size: 173543 Color: 2
Size: 128321 Color: 1

Bin 178: 114 of cap free
Amount of items: 2
Items: 
Size: 548554 Color: 3
Size: 451333 Color: 0

Bin 179: 114 of cap free
Amount of items: 2
Items: 
Size: 678180 Color: 2
Size: 321707 Color: 1

Bin 180: 114 of cap free
Amount of items: 2
Items: 
Size: 763700 Color: 0
Size: 236187 Color: 1

Bin 181: 115 of cap free
Amount of items: 3
Items: 
Size: 410192 Color: 1
Size: 409505 Color: 2
Size: 180189 Color: 1

Bin 182: 115 of cap free
Amount of items: 2
Items: 
Size: 515152 Color: 0
Size: 484734 Color: 3

Bin 183: 117 of cap free
Amount of items: 2
Items: 
Size: 751903 Color: 0
Size: 247981 Color: 3

Bin 184: 118 of cap free
Amount of items: 2
Items: 
Size: 589612 Color: 1
Size: 410271 Color: 4

Bin 185: 118 of cap free
Amount of items: 2
Items: 
Size: 777703 Color: 2
Size: 222180 Color: 3

Bin 186: 120 of cap free
Amount of items: 2
Items: 
Size: 614262 Color: 2
Size: 385619 Color: 0

Bin 187: 121 of cap free
Amount of items: 2
Items: 
Size: 657284 Color: 0
Size: 342596 Color: 4

Bin 188: 122 of cap free
Amount of items: 2
Items: 
Size: 792079 Color: 2
Size: 207800 Color: 4

Bin 189: 123 of cap free
Amount of items: 2
Items: 
Size: 776460 Color: 3
Size: 223418 Color: 4

Bin 190: 124 of cap free
Amount of items: 7
Items: 
Size: 152495 Color: 3
Size: 147137 Color: 2
Size: 146216 Color: 2
Size: 145090 Color: 3
Size: 144984 Color: 4
Size: 144949 Color: 1
Size: 119006 Color: 1

Bin 191: 124 of cap free
Amount of items: 2
Items: 
Size: 580402 Color: 1
Size: 419475 Color: 0

Bin 192: 124 of cap free
Amount of items: 2
Items: 
Size: 676517 Color: 1
Size: 323360 Color: 4

Bin 193: 124 of cap free
Amount of items: 2
Items: 
Size: 698065 Color: 0
Size: 301812 Color: 2

Bin 194: 126 of cap free
Amount of items: 2
Items: 
Size: 626595 Color: 0
Size: 373280 Color: 4

Bin 195: 126 of cap free
Amount of items: 2
Items: 
Size: 673961 Color: 3
Size: 325914 Color: 0

Bin 196: 127 of cap free
Amount of items: 5
Items: 
Size: 252627 Color: 0
Size: 188706 Color: 3
Size: 188510 Color: 4
Size: 188335 Color: 0
Size: 181696 Color: 2

Bin 197: 127 of cap free
Amount of items: 2
Items: 
Size: 717543 Color: 4
Size: 282331 Color: 0

Bin 198: 128 of cap free
Amount of items: 2
Items: 
Size: 561827 Color: 1
Size: 438046 Color: 3

Bin 199: 128 of cap free
Amount of items: 2
Items: 
Size: 693755 Color: 0
Size: 306118 Color: 4

Bin 200: 128 of cap free
Amount of items: 2
Items: 
Size: 710448 Color: 4
Size: 289425 Color: 0

Bin 201: 130 of cap free
Amount of items: 3
Items: 
Size: 348149 Color: 2
Size: 345071 Color: 0
Size: 306651 Color: 1

Bin 202: 130 of cap free
Amount of items: 2
Items: 
Size: 612432 Color: 2
Size: 387439 Color: 4

Bin 203: 131 of cap free
Amount of items: 2
Items: 
Size: 578292 Color: 2
Size: 421578 Color: 1

Bin 204: 132 of cap free
Amount of items: 2
Items: 
Size: 583196 Color: 1
Size: 416673 Color: 3

Bin 205: 132 of cap free
Amount of items: 2
Items: 
Size: 650579 Color: 1
Size: 349290 Color: 3

Bin 206: 133 of cap free
Amount of items: 2
Items: 
Size: 754517 Color: 2
Size: 245351 Color: 3

Bin 207: 135 of cap free
Amount of items: 2
Items: 
Size: 527113 Color: 0
Size: 472753 Color: 4

Bin 208: 136 of cap free
Amount of items: 2
Items: 
Size: 507543 Color: 0
Size: 492322 Color: 1

Bin 209: 136 of cap free
Amount of items: 2
Items: 
Size: 710583 Color: 4
Size: 289282 Color: 0

Bin 210: 137 of cap free
Amount of items: 2
Items: 
Size: 799809 Color: 3
Size: 200055 Color: 4

Bin 211: 138 of cap free
Amount of items: 2
Items: 
Size: 723569 Color: 1
Size: 276294 Color: 2

Bin 212: 139 of cap free
Amount of items: 2
Items: 
Size: 718651 Color: 4
Size: 281211 Color: 1

Bin 213: 139 of cap free
Amount of items: 2
Items: 
Size: 741353 Color: 3
Size: 258509 Color: 2

Bin 214: 140 of cap free
Amount of items: 2
Items: 
Size: 536325 Color: 2
Size: 463536 Color: 0

Bin 215: 140 of cap free
Amount of items: 2
Items: 
Size: 613969 Color: 3
Size: 385892 Color: 4

Bin 216: 140 of cap free
Amount of items: 2
Items: 
Size: 787468 Color: 2
Size: 212393 Color: 3

Bin 217: 141 of cap free
Amount of items: 2
Items: 
Size: 795354 Color: 0
Size: 204506 Color: 1

Bin 218: 143 of cap free
Amount of items: 2
Items: 
Size: 641562 Color: 3
Size: 358296 Color: 1

Bin 219: 144 of cap free
Amount of items: 2
Items: 
Size: 624990 Color: 4
Size: 374867 Color: 1

Bin 220: 146 of cap free
Amount of items: 2
Items: 
Size: 562299 Color: 3
Size: 437556 Color: 0

Bin 221: 146 of cap free
Amount of items: 2
Items: 
Size: 602773 Color: 2
Size: 397082 Color: 0

Bin 222: 146 of cap free
Amount of items: 2
Items: 
Size: 786114 Color: 4
Size: 213741 Color: 1

Bin 223: 147 of cap free
Amount of items: 2
Items: 
Size: 560723 Color: 1
Size: 439131 Color: 0

Bin 224: 147 of cap free
Amount of items: 2
Items: 
Size: 730438 Color: 0
Size: 269416 Color: 4

Bin 225: 148 of cap free
Amount of items: 2
Items: 
Size: 707793 Color: 3
Size: 292060 Color: 1

Bin 226: 149 of cap free
Amount of items: 2
Items: 
Size: 599566 Color: 1
Size: 400286 Color: 3

Bin 227: 150 of cap free
Amount of items: 2
Items: 
Size: 709327 Color: 2
Size: 290524 Color: 1

Bin 228: 151 of cap free
Amount of items: 2
Items: 
Size: 532644 Color: 2
Size: 467206 Color: 3

Bin 229: 152 of cap free
Amount of items: 2
Items: 
Size: 712272 Color: 1
Size: 287577 Color: 3

Bin 230: 153 of cap free
Amount of items: 2
Items: 
Size: 637125 Color: 1
Size: 362723 Color: 0

Bin 231: 154 of cap free
Amount of items: 2
Items: 
Size: 663833 Color: 4
Size: 336014 Color: 3

Bin 232: 154 of cap free
Amount of items: 2
Items: 
Size: 738778 Color: 2
Size: 261069 Color: 0

Bin 233: 155 of cap free
Amount of items: 2
Items: 
Size: 563075 Color: 2
Size: 436771 Color: 1

Bin 234: 156 of cap free
Amount of items: 2
Items: 
Size: 541472 Color: 3
Size: 458373 Color: 0

Bin 235: 156 of cap free
Amount of items: 2
Items: 
Size: 543628 Color: 0
Size: 456217 Color: 1

Bin 236: 156 of cap free
Amount of items: 2
Items: 
Size: 636629 Color: 3
Size: 363216 Color: 0

Bin 237: 160 of cap free
Amount of items: 2
Items: 
Size: 761806 Color: 4
Size: 238035 Color: 3

Bin 238: 162 of cap free
Amount of items: 2
Items: 
Size: 765238 Color: 2
Size: 234601 Color: 0

Bin 239: 164 of cap free
Amount of items: 2
Items: 
Size: 688300 Color: 1
Size: 311537 Color: 0

Bin 240: 168 of cap free
Amount of items: 2
Items: 
Size: 778637 Color: 3
Size: 221196 Color: 0

Bin 241: 169 of cap free
Amount of items: 2
Items: 
Size: 532597 Color: 3
Size: 467235 Color: 2

Bin 242: 169 of cap free
Amount of items: 2
Items: 
Size: 582715 Color: 1
Size: 417117 Color: 2

Bin 243: 169 of cap free
Amount of items: 2
Items: 
Size: 764260 Color: 3
Size: 235572 Color: 4

Bin 244: 171 of cap free
Amount of items: 2
Items: 
Size: 635021 Color: 0
Size: 364809 Color: 2

Bin 245: 171 of cap free
Amount of items: 2
Items: 
Size: 684640 Color: 1
Size: 315190 Color: 3

Bin 246: 171 of cap free
Amount of items: 2
Items: 
Size: 688997 Color: 1
Size: 310833 Color: 2

Bin 247: 174 of cap free
Amount of items: 2
Items: 
Size: 667332 Color: 2
Size: 332495 Color: 4

Bin 248: 174 of cap free
Amount of items: 2
Items: 
Size: 724923 Color: 3
Size: 274904 Color: 4

Bin 249: 174 of cap free
Amount of items: 2
Items: 
Size: 798818 Color: 4
Size: 201009 Color: 0

Bin 250: 176 of cap free
Amount of items: 2
Items: 
Size: 585691 Color: 4
Size: 414134 Color: 3

Bin 251: 176 of cap free
Amount of items: 2
Items: 
Size: 785457 Color: 4
Size: 214368 Color: 2

Bin 252: 177 of cap free
Amount of items: 2
Items: 
Size: 621380 Color: 4
Size: 378444 Color: 0

Bin 253: 179 of cap free
Amount of items: 2
Items: 
Size: 521661 Color: 0
Size: 478161 Color: 4

Bin 254: 179 of cap free
Amount of items: 2
Items: 
Size: 660450 Color: 4
Size: 339372 Color: 2

Bin 255: 179 of cap free
Amount of items: 2
Items: 
Size: 781511 Color: 2
Size: 218311 Color: 0

Bin 256: 180 of cap free
Amount of items: 2
Items: 
Size: 650845 Color: 0
Size: 348976 Color: 3

Bin 257: 180 of cap free
Amount of items: 2
Items: 
Size: 703989 Color: 0
Size: 295832 Color: 2

Bin 258: 180 of cap free
Amount of items: 2
Items: 
Size: 707094 Color: 3
Size: 292727 Color: 4

Bin 259: 182 of cap free
Amount of items: 7
Items: 
Size: 152061 Color: 3
Size: 144762 Color: 0
Size: 144593 Color: 3
Size: 144320 Color: 4
Size: 143892 Color: 2
Size: 143672 Color: 1
Size: 126519 Color: 0

Bin 260: 182 of cap free
Amount of items: 2
Items: 
Size: 610138 Color: 3
Size: 389681 Color: 4

Bin 261: 184 of cap free
Amount of items: 3
Items: 
Size: 409476 Color: 0
Size: 409034 Color: 3
Size: 181307 Color: 0

Bin 262: 184 of cap free
Amount of items: 2
Items: 
Size: 522334 Color: 1
Size: 477483 Color: 2

Bin 263: 185 of cap free
Amount of items: 2
Items: 
Size: 618606 Color: 3
Size: 381210 Color: 4

Bin 264: 185 of cap free
Amount of items: 2
Items: 
Size: 659242 Color: 3
Size: 340574 Color: 0

Bin 265: 189 of cap free
Amount of items: 2
Items: 
Size: 589351 Color: 0
Size: 410461 Color: 2

Bin 266: 189 of cap free
Amount of items: 2
Items: 
Size: 742882 Color: 4
Size: 256930 Color: 3

Bin 267: 192 of cap free
Amount of items: 2
Items: 
Size: 522933 Color: 1
Size: 476876 Color: 3

Bin 268: 192 of cap free
Amount of items: 2
Items: 
Size: 618257 Color: 2
Size: 381552 Color: 1

Bin 269: 193 of cap free
Amount of items: 2
Items: 
Size: 526775 Color: 1
Size: 473033 Color: 0

Bin 270: 193 of cap free
Amount of items: 2
Items: 
Size: 541471 Color: 1
Size: 458337 Color: 2

Bin 271: 194 of cap free
Amount of items: 2
Items: 
Size: 709352 Color: 1
Size: 290455 Color: 2

Bin 272: 194 of cap free
Amount of items: 2
Items: 
Size: 776807 Color: 4
Size: 223000 Color: 3

Bin 273: 195 of cap free
Amount of items: 2
Items: 
Size: 734429 Color: 3
Size: 265377 Color: 2

Bin 274: 196 of cap free
Amount of items: 2
Items: 
Size: 524658 Color: 3
Size: 475147 Color: 2

Bin 275: 196 of cap free
Amount of items: 2
Items: 
Size: 578743 Color: 1
Size: 421062 Color: 3

Bin 276: 201 of cap free
Amount of items: 2
Items: 
Size: 616295 Color: 3
Size: 383505 Color: 0

Bin 277: 204 of cap free
Amount of items: 2
Items: 
Size: 652548 Color: 4
Size: 347249 Color: 1

Bin 278: 205 of cap free
Amount of items: 2
Items: 
Size: 510173 Color: 0
Size: 489623 Color: 1

Bin 279: 207 of cap free
Amount of items: 2
Items: 
Size: 541767 Color: 1
Size: 458027 Color: 0

Bin 280: 210 of cap free
Amount of items: 2
Items: 
Size: 701907 Color: 4
Size: 297884 Color: 1

Bin 281: 211 of cap free
Amount of items: 2
Items: 
Size: 791795 Color: 2
Size: 207995 Color: 1

Bin 282: 212 of cap free
Amount of items: 6
Items: 
Size: 172712 Color: 1
Size: 172448 Color: 3
Size: 172088 Color: 2
Size: 172076 Color: 2
Size: 172054 Color: 1
Size: 138411 Color: 1

Bin 283: 212 of cap free
Amount of items: 2
Items: 
Size: 567133 Color: 4
Size: 432656 Color: 1

Bin 284: 212 of cap free
Amount of items: 2
Items: 
Size: 724658 Color: 1
Size: 275131 Color: 3

Bin 285: 214 of cap free
Amount of items: 2
Items: 
Size: 593870 Color: 4
Size: 405917 Color: 1

Bin 286: 214 of cap free
Amount of items: 2
Items: 
Size: 631935 Color: 0
Size: 367852 Color: 4

Bin 287: 217 of cap free
Amount of items: 2
Items: 
Size: 597132 Color: 2
Size: 402652 Color: 0

Bin 288: 217 of cap free
Amount of items: 2
Items: 
Size: 683349 Color: 3
Size: 316435 Color: 1

Bin 289: 219 of cap free
Amount of items: 2
Items: 
Size: 518120 Color: 3
Size: 481662 Color: 2

Bin 290: 225 of cap free
Amount of items: 2
Items: 
Size: 754074 Color: 1
Size: 245702 Color: 2

Bin 291: 226 of cap free
Amount of items: 2
Items: 
Size: 594976 Color: 0
Size: 404799 Color: 2

Bin 292: 228 of cap free
Amount of items: 2
Items: 
Size: 725704 Color: 1
Size: 274069 Color: 4

Bin 293: 230 of cap free
Amount of items: 2
Items: 
Size: 553414 Color: 3
Size: 446357 Color: 1

Bin 294: 231 of cap free
Amount of items: 2
Items: 
Size: 782355 Color: 1
Size: 217415 Color: 0

Bin 295: 232 of cap free
Amount of items: 2
Items: 
Size: 586952 Color: 2
Size: 412817 Color: 4

Bin 296: 232 of cap free
Amount of items: 2
Items: 
Size: 684226 Color: 3
Size: 315543 Color: 1

Bin 297: 236 of cap free
Amount of items: 2
Items: 
Size: 573911 Color: 2
Size: 425854 Color: 0

Bin 298: 236 of cap free
Amount of items: 2
Items: 
Size: 679882 Color: 1
Size: 319883 Color: 2

Bin 299: 239 of cap free
Amount of items: 2
Items: 
Size: 770375 Color: 1
Size: 229387 Color: 2

Bin 300: 240 of cap free
Amount of items: 2
Items: 
Size: 756888 Color: 3
Size: 242873 Color: 0

Bin 301: 241 of cap free
Amount of items: 2
Items: 
Size: 501455 Color: 0
Size: 498305 Color: 3

Bin 302: 241 of cap free
Amount of items: 2
Items: 
Size: 755788 Color: 4
Size: 243972 Color: 1

Bin 303: 243 of cap free
Amount of items: 2
Items: 
Size: 628336 Color: 2
Size: 371422 Color: 4

Bin 304: 245 of cap free
Amount of items: 2
Items: 
Size: 751306 Color: 0
Size: 248450 Color: 4

Bin 305: 248 of cap free
Amount of items: 2
Items: 
Size: 597662 Color: 4
Size: 402091 Color: 0

Bin 306: 252 of cap free
Amount of items: 2
Items: 
Size: 675149 Color: 4
Size: 324600 Color: 1

Bin 307: 252 of cap free
Amount of items: 2
Items: 
Size: 799089 Color: 0
Size: 200660 Color: 3

Bin 308: 253 of cap free
Amount of items: 2
Items: 
Size: 735949 Color: 4
Size: 263799 Color: 3

Bin 309: 255 of cap free
Amount of items: 2
Items: 
Size: 632568 Color: 2
Size: 367178 Color: 4

Bin 310: 256 of cap free
Amount of items: 2
Items: 
Size: 767248 Color: 1
Size: 232497 Color: 0

Bin 311: 258 of cap free
Amount of items: 2
Items: 
Size: 547473 Color: 0
Size: 452270 Color: 2

Bin 312: 259 of cap free
Amount of items: 2
Items: 
Size: 587754 Color: 1
Size: 411988 Color: 3

Bin 313: 259 of cap free
Amount of items: 2
Items: 
Size: 595823 Color: 4
Size: 403919 Color: 3

Bin 314: 259 of cap free
Amount of items: 2
Items: 
Size: 760875 Color: 3
Size: 238867 Color: 2

Bin 315: 260 of cap free
Amount of items: 2
Items: 
Size: 562710 Color: 2
Size: 437031 Color: 4

Bin 316: 260 of cap free
Amount of items: 2
Items: 
Size: 679295 Color: 0
Size: 320446 Color: 1

Bin 317: 264 of cap free
Amount of items: 6
Items: 
Size: 180406 Color: 3
Size: 179268 Color: 1
Size: 179143 Color: 2
Size: 178995 Color: 0
Size: 178613 Color: 1
Size: 103312 Color: 4

Bin 318: 264 of cap free
Amount of items: 2
Items: 
Size: 707679 Color: 4
Size: 292058 Color: 3

Bin 319: 266 of cap free
Amount of items: 2
Items: 
Size: 530374 Color: 0
Size: 469361 Color: 1

Bin 320: 267 of cap free
Amount of items: 2
Items: 
Size: 740933 Color: 1
Size: 258801 Color: 2

Bin 321: 268 of cap free
Amount of items: 2
Items: 
Size: 556438 Color: 0
Size: 443295 Color: 4

Bin 322: 269 of cap free
Amount of items: 2
Items: 
Size: 539460 Color: 0
Size: 460272 Color: 4

Bin 323: 270 of cap free
Amount of items: 2
Items: 
Size: 692666 Color: 0
Size: 307065 Color: 3

Bin 324: 272 of cap free
Amount of items: 2
Items: 
Size: 613448 Color: 3
Size: 386281 Color: 0

Bin 325: 272 of cap free
Amount of items: 2
Items: 
Size: 621694 Color: 0
Size: 378035 Color: 2

Bin 326: 272 of cap free
Amount of items: 2
Items: 
Size: 641183 Color: 0
Size: 358546 Color: 1

Bin 327: 275 of cap free
Amount of items: 2
Items: 
Size: 753105 Color: 1
Size: 246621 Color: 0

Bin 328: 276 of cap free
Amount of items: 2
Items: 
Size: 560393 Color: 2
Size: 439332 Color: 1

Bin 329: 280 of cap free
Amount of items: 2
Items: 
Size: 704405 Color: 4
Size: 295316 Color: 3

Bin 330: 282 of cap free
Amount of items: 2
Items: 
Size: 564545 Color: 1
Size: 435174 Color: 4

Bin 331: 282 of cap free
Amount of items: 2
Items: 
Size: 596386 Color: 3
Size: 403333 Color: 0

Bin 332: 283 of cap free
Amount of items: 2
Items: 
Size: 656607 Color: 3
Size: 343111 Color: 4

Bin 333: 287 of cap free
Amount of items: 2
Items: 
Size: 619261 Color: 2
Size: 380453 Color: 1

Bin 334: 287 of cap free
Amount of items: 2
Items: 
Size: 677842 Color: 0
Size: 321872 Color: 3

Bin 335: 287 of cap free
Amount of items: 2
Items: 
Size: 723556 Color: 2
Size: 276158 Color: 1

Bin 336: 287 of cap free
Amount of items: 2
Items: 
Size: 769921 Color: 4
Size: 229793 Color: 2

Bin 337: 288 of cap free
Amount of items: 2
Items: 
Size: 533371 Color: 1
Size: 466342 Color: 3

Bin 338: 288 of cap free
Amount of items: 2
Items: 
Size: 608988 Color: 3
Size: 390725 Color: 2

Bin 339: 288 of cap free
Amount of items: 2
Items: 
Size: 637414 Color: 4
Size: 362299 Color: 1

Bin 340: 290 of cap free
Amount of items: 2
Items: 
Size: 517823 Color: 2
Size: 481888 Color: 1

Bin 341: 293 of cap free
Amount of items: 2
Items: 
Size: 546227 Color: 0
Size: 453481 Color: 3

Bin 342: 293 of cap free
Amount of items: 2
Items: 
Size: 598425 Color: 0
Size: 401283 Color: 2

Bin 343: 295 of cap free
Amount of items: 2
Items: 
Size: 678482 Color: 0
Size: 321224 Color: 4

Bin 344: 296 of cap free
Amount of items: 2
Items: 
Size: 746751 Color: 1
Size: 252954 Color: 4

Bin 345: 299 of cap free
Amount of items: 2
Items: 
Size: 720264 Color: 4
Size: 279438 Color: 0

Bin 346: 300 of cap free
Amount of items: 2
Items: 
Size: 695676 Color: 2
Size: 304025 Color: 1

Bin 347: 303 of cap free
Amount of items: 2
Items: 
Size: 555734 Color: 4
Size: 443964 Color: 0

Bin 348: 306 of cap free
Amount of items: 2
Items: 
Size: 770632 Color: 0
Size: 229063 Color: 2

Bin 349: 308 of cap free
Amount of items: 2
Items: 
Size: 589962 Color: 1
Size: 409731 Color: 4

Bin 350: 309 of cap free
Amount of items: 2
Items: 
Size: 775185 Color: 0
Size: 224507 Color: 4

Bin 351: 310 of cap free
Amount of items: 2
Items: 
Size: 702141 Color: 3
Size: 297550 Color: 1

Bin 352: 315 of cap free
Amount of items: 2
Items: 
Size: 499888 Color: 4
Size: 499798 Color: 1

Bin 353: 315 of cap free
Amount of items: 2
Items: 
Size: 701408 Color: 4
Size: 298278 Color: 2

Bin 354: 316 of cap free
Amount of items: 6
Items: 
Size: 176264 Color: 2
Size: 175924 Color: 4
Size: 175614 Color: 1
Size: 175568 Color: 2
Size: 175507 Color: 1
Size: 120808 Color: 2

Bin 355: 317 of cap free
Amount of items: 2
Items: 
Size: 647507 Color: 4
Size: 352177 Color: 1

Bin 356: 318 of cap free
Amount of items: 2
Items: 
Size: 583440 Color: 3
Size: 416243 Color: 1

Bin 357: 320 of cap free
Amount of items: 2
Items: 
Size: 766331 Color: 1
Size: 233350 Color: 4

Bin 358: 320 of cap free
Amount of items: 2
Items: 
Size: 777981 Color: 4
Size: 221700 Color: 2

Bin 359: 322 of cap free
Amount of items: 2
Items: 
Size: 514550 Color: 2
Size: 485129 Color: 1

Bin 360: 322 of cap free
Amount of items: 2
Items: 
Size: 573681 Color: 1
Size: 425998 Color: 2

Bin 361: 325 of cap free
Amount of items: 2
Items: 
Size: 763284 Color: 0
Size: 236392 Color: 4

Bin 362: 326 of cap free
Amount of items: 2
Items: 
Size: 774708 Color: 0
Size: 224967 Color: 4

Bin 363: 329 of cap free
Amount of items: 2
Items: 
Size: 531880 Color: 2
Size: 467792 Color: 3

Bin 364: 330 of cap free
Amount of items: 2
Items: 
Size: 793487 Color: 0
Size: 206184 Color: 1

Bin 365: 338 of cap free
Amount of items: 2
Items: 
Size: 588893 Color: 3
Size: 410770 Color: 4

Bin 366: 339 of cap free
Amount of items: 2
Items: 
Size: 699243 Color: 1
Size: 300419 Color: 0

Bin 367: 340 of cap free
Amount of items: 2
Items: 
Size: 672718 Color: 0
Size: 326943 Color: 1

Bin 368: 343 of cap free
Amount of items: 2
Items: 
Size: 784569 Color: 2
Size: 215089 Color: 0

Bin 369: 348 of cap free
Amount of items: 6
Items: 
Size: 170978 Color: 2
Size: 170364 Color: 4
Size: 170035 Color: 0
Size: 169891 Color: 0
Size: 169327 Color: 2
Size: 149058 Color: 2

Bin 370: 349 of cap free
Amount of items: 2
Items: 
Size: 531654 Color: 4
Size: 467998 Color: 2

Bin 371: 354 of cap free
Amount of items: 2
Items: 
Size: 561435 Color: 2
Size: 438212 Color: 3

Bin 372: 358 of cap free
Amount of items: 2
Items: 
Size: 560533 Color: 1
Size: 439110 Color: 3

Bin 373: 361 of cap free
Amount of items: 2
Items: 
Size: 764153 Color: 4
Size: 235487 Color: 2

Bin 374: 362 of cap free
Amount of items: 2
Items: 
Size: 536851 Color: 4
Size: 462788 Color: 3

Bin 375: 362 of cap free
Amount of items: 2
Items: 
Size: 732255 Color: 4
Size: 267384 Color: 1

Bin 376: 366 of cap free
Amount of items: 2
Items: 
Size: 586336 Color: 4
Size: 413299 Color: 2

Bin 377: 369 of cap free
Amount of items: 2
Items: 
Size: 519844 Color: 3
Size: 479788 Color: 1

Bin 378: 370 of cap free
Amount of items: 2
Items: 
Size: 549024 Color: 1
Size: 450607 Color: 0

Bin 379: 371 of cap free
Amount of items: 2
Items: 
Size: 673959 Color: 4
Size: 325671 Color: 2

Bin 380: 372 of cap free
Amount of items: 2
Items: 
Size: 707979 Color: 2
Size: 291650 Color: 0

Bin 381: 373 of cap free
Amount of items: 2
Items: 
Size: 681515 Color: 4
Size: 318113 Color: 1

Bin 382: 373 of cap free
Amount of items: 2
Items: 
Size: 718679 Color: 1
Size: 280949 Color: 3

Bin 383: 376 of cap free
Amount of items: 2
Items: 
Size: 737516 Color: 4
Size: 262109 Color: 2

Bin 384: 376 of cap free
Amount of items: 2
Items: 
Size: 752061 Color: 2
Size: 247564 Color: 3

Bin 385: 379 of cap free
Amount of items: 2
Items: 
Size: 607159 Color: 4
Size: 392463 Color: 1

Bin 386: 379 of cap free
Amount of items: 2
Items: 
Size: 682720 Color: 2
Size: 316902 Color: 0

Bin 387: 379 of cap free
Amount of items: 2
Items: 
Size: 695971 Color: 1
Size: 303651 Color: 0

Bin 388: 379 of cap free
Amount of items: 2
Items: 
Size: 772230 Color: 0
Size: 227392 Color: 4

Bin 389: 380 of cap free
Amount of items: 2
Items: 
Size: 563365 Color: 2
Size: 436256 Color: 4

Bin 390: 380 of cap free
Amount of items: 2
Items: 
Size: 661255 Color: 2
Size: 338366 Color: 1

Bin 391: 382 of cap free
Amount of items: 2
Items: 
Size: 577444 Color: 3
Size: 422175 Color: 1

Bin 392: 383 of cap free
Amount of items: 2
Items: 
Size: 778393 Color: 0
Size: 221225 Color: 3

Bin 393: 386 of cap free
Amount of items: 2
Items: 
Size: 715034 Color: 4
Size: 284581 Color: 3

Bin 394: 392 of cap free
Amount of items: 2
Items: 
Size: 639712 Color: 2
Size: 359897 Color: 3

Bin 395: 392 of cap free
Amount of items: 2
Items: 
Size: 659183 Color: 3
Size: 340426 Color: 4

Bin 396: 395 of cap free
Amount of items: 2
Items: 
Size: 624412 Color: 4
Size: 375194 Color: 3

Bin 397: 399 of cap free
Amount of items: 2
Items: 
Size: 585645 Color: 2
Size: 413957 Color: 4

Bin 398: 399 of cap free
Amount of items: 2
Items: 
Size: 758871 Color: 3
Size: 240731 Color: 2

Bin 399: 400 of cap free
Amount of items: 2
Items: 
Size: 559675 Color: 2
Size: 439926 Color: 3

Bin 400: 401 of cap free
Amount of items: 2
Items: 
Size: 554898 Color: 2
Size: 444702 Color: 0

Bin 401: 402 of cap free
Amount of items: 2
Items: 
Size: 757139 Color: 4
Size: 242460 Color: 0

Bin 402: 404 of cap free
Amount of items: 2
Items: 
Size: 754049 Color: 4
Size: 245548 Color: 3

Bin 403: 405 of cap free
Amount of items: 2
Items: 
Size: 775597 Color: 4
Size: 223999 Color: 2

Bin 404: 407 of cap free
Amount of items: 2
Items: 
Size: 625230 Color: 2
Size: 374364 Color: 3

Bin 405: 412 of cap free
Amount of items: 2
Items: 
Size: 532443 Color: 0
Size: 467146 Color: 4

Bin 406: 412 of cap free
Amount of items: 2
Items: 
Size: 563658 Color: 4
Size: 435931 Color: 0

Bin 407: 412 of cap free
Amount of items: 2
Items: 
Size: 753493 Color: 0
Size: 246096 Color: 4

Bin 408: 413 of cap free
Amount of items: 2
Items: 
Size: 570112 Color: 2
Size: 429476 Color: 4

Bin 409: 413 of cap free
Amount of items: 2
Items: 
Size: 698440 Color: 4
Size: 301148 Color: 0

Bin 410: 417 of cap free
Amount of items: 2
Items: 
Size: 516585 Color: 0
Size: 482999 Color: 4

Bin 411: 422 of cap free
Amount of items: 2
Items: 
Size: 546143 Color: 3
Size: 453436 Color: 0

Bin 412: 423 of cap free
Amount of items: 2
Items: 
Size: 529293 Color: 4
Size: 470285 Color: 0

Bin 413: 423 of cap free
Amount of items: 2
Items: 
Size: 575265 Color: 4
Size: 424313 Color: 3

Bin 414: 423 of cap free
Amount of items: 2
Items: 
Size: 750846 Color: 2
Size: 248732 Color: 3

Bin 415: 424 of cap free
Amount of items: 2
Items: 
Size: 618217 Color: 4
Size: 381360 Color: 3

Bin 416: 425 of cap free
Amount of items: 2
Items: 
Size: 523955 Color: 3
Size: 475621 Color: 4

Bin 417: 425 of cap free
Amount of items: 2
Items: 
Size: 568748 Color: 0
Size: 430828 Color: 2

Bin 418: 425 of cap free
Amount of items: 2
Items: 
Size: 616323 Color: 0
Size: 383253 Color: 1

Bin 419: 425 of cap free
Amount of items: 2
Items: 
Size: 722195 Color: 4
Size: 277381 Color: 2

Bin 420: 426 of cap free
Amount of items: 2
Items: 
Size: 742427 Color: 0
Size: 257148 Color: 4

Bin 421: 433 of cap free
Amount of items: 2
Items: 
Size: 716288 Color: 1
Size: 283280 Color: 0

Bin 422: 437 of cap free
Amount of items: 2
Items: 
Size: 554951 Color: 0
Size: 444613 Color: 3

Bin 423: 437 of cap free
Amount of items: 2
Items: 
Size: 660413 Color: 3
Size: 339151 Color: 1

Bin 424: 444 of cap free
Amount of items: 2
Items: 
Size: 768586 Color: 2
Size: 230971 Color: 0

Bin 425: 446 of cap free
Amount of items: 2
Items: 
Size: 628258 Color: 1
Size: 371297 Color: 0

Bin 426: 446 of cap free
Amount of items: 2
Items: 
Size: 787615 Color: 4
Size: 211940 Color: 0

Bin 427: 447 of cap free
Amount of items: 2
Items: 
Size: 759990 Color: 1
Size: 239564 Color: 0

Bin 428: 448 of cap free
Amount of items: 2
Items: 
Size: 610075 Color: 1
Size: 389478 Color: 3

Bin 429: 450 of cap free
Amount of items: 8
Items: 
Size: 134969 Color: 4
Size: 124887 Color: 2
Size: 124774 Color: 2
Size: 124731 Color: 3
Size: 124570 Color: 4
Size: 124368 Color: 2
Size: 123864 Color: 2
Size: 117388 Color: 1

Bin 430: 450 of cap free
Amount of items: 2
Items: 
Size: 543585 Color: 4
Size: 455966 Color: 2

Bin 431: 451 of cap free
Amount of items: 5
Items: 
Size: 249805 Color: 0
Size: 188255 Color: 3
Size: 188222 Color: 1
Size: 187587 Color: 1
Size: 185681 Color: 3

Bin 432: 451 of cap free
Amount of items: 2
Items: 
Size: 525775 Color: 3
Size: 473775 Color: 2

Bin 433: 454 of cap free
Amount of items: 2
Items: 
Size: 576222 Color: 2
Size: 423325 Color: 3

Bin 434: 454 of cap free
Amount of items: 2
Items: 
Size: 589295 Color: 0
Size: 410252 Color: 4

Bin 435: 454 of cap free
Amount of items: 2
Items: 
Size: 786317 Color: 2
Size: 213230 Color: 3

Bin 436: 455 of cap free
Amount of items: 2
Items: 
Size: 611630 Color: 4
Size: 387916 Color: 3

Bin 437: 457 of cap free
Amount of items: 3
Items: 
Size: 341099 Color: 3
Size: 340361 Color: 0
Size: 318084 Color: 4

Bin 438: 458 of cap free
Amount of items: 2
Items: 
Size: 526514 Color: 2
Size: 473029 Color: 0

Bin 439: 459 of cap free
Amount of items: 2
Items: 
Size: 553350 Color: 2
Size: 446192 Color: 1

Bin 440: 466 of cap free
Amount of items: 2
Items: 
Size: 748312 Color: 4
Size: 251223 Color: 0

Bin 441: 468 of cap free
Amount of items: 2
Items: 
Size: 675951 Color: 2
Size: 323582 Color: 1

Bin 442: 471 of cap free
Amount of items: 2
Items: 
Size: 718970 Color: 3
Size: 280560 Color: 4

Bin 443: 472 of cap free
Amount of items: 2
Items: 
Size: 625225 Color: 0
Size: 374304 Color: 3

Bin 444: 472 of cap free
Amount of items: 2
Items: 
Size: 791558 Color: 4
Size: 207971 Color: 1

Bin 445: 474 of cap free
Amount of items: 2
Items: 
Size: 608289 Color: 2
Size: 391238 Color: 3

Bin 446: 478 of cap free
Amount of items: 2
Items: 
Size: 669245 Color: 0
Size: 330278 Color: 1

Bin 447: 479 of cap free
Amount of items: 2
Items: 
Size: 759969 Color: 0
Size: 239553 Color: 1

Bin 448: 480 of cap free
Amount of items: 2
Items: 
Size: 556323 Color: 2
Size: 443198 Color: 1

Bin 449: 480 of cap free
Amount of items: 2
Items: 
Size: 638680 Color: 1
Size: 360841 Color: 2

Bin 450: 489 of cap free
Amount of items: 2
Items: 
Size: 724680 Color: 3
Size: 274832 Color: 0

Bin 451: 499 of cap free
Amount of items: 2
Items: 
Size: 550234 Color: 2
Size: 449268 Color: 4

Bin 452: 502 of cap free
Amount of items: 2
Items: 
Size: 504748 Color: 1
Size: 494751 Color: 4

Bin 453: 502 of cap free
Amount of items: 2
Items: 
Size: 646831 Color: 4
Size: 352668 Color: 1

Bin 454: 506 of cap free
Amount of items: 2
Items: 
Size: 584444 Color: 0
Size: 415051 Color: 1

Bin 455: 511 of cap free
Amount of items: 2
Items: 
Size: 685296 Color: 2
Size: 314194 Color: 1

Bin 456: 514 of cap free
Amount of items: 2
Items: 
Size: 577364 Color: 4
Size: 422123 Color: 3

Bin 457: 517 of cap free
Amount of items: 2
Items: 
Size: 762336 Color: 1
Size: 237148 Color: 3

Bin 458: 520 of cap free
Amount of items: 2
Items: 
Size: 507740 Color: 0
Size: 491741 Color: 1

Bin 459: 522 of cap free
Amount of items: 2
Items: 
Size: 735270 Color: 2
Size: 264209 Color: 4

Bin 460: 527 of cap free
Amount of items: 2
Items: 
Size: 705502 Color: 2
Size: 293972 Color: 4

Bin 461: 528 of cap free
Amount of items: 2
Items: 
Size: 502503 Color: 1
Size: 496970 Color: 2

Bin 462: 529 of cap free
Amount of items: 2
Items: 
Size: 678438 Color: 1
Size: 321034 Color: 3

Bin 463: 532 of cap free
Amount of items: 2
Items: 
Size: 733417 Color: 3
Size: 266052 Color: 2

Bin 464: 536 of cap free
Amount of items: 2
Items: 
Size: 557726 Color: 4
Size: 441739 Color: 3

Bin 465: 536 of cap free
Amount of items: 2
Items: 
Size: 602639 Color: 0
Size: 396826 Color: 1

Bin 466: 538 of cap free
Amount of items: 2
Items: 
Size: 598326 Color: 4
Size: 401137 Color: 1

Bin 467: 544 of cap free
Amount of items: 2
Items: 
Size: 550260 Color: 4
Size: 449197 Color: 1

Bin 468: 544 of cap free
Amount of items: 2
Items: 
Size: 626981 Color: 0
Size: 372476 Color: 1

Bin 469: 560 of cap free
Amount of items: 2
Items: 
Size: 651894 Color: 0
Size: 347547 Color: 1

Bin 470: 562 of cap free
Amount of items: 3
Items: 
Size: 362178 Color: 4
Size: 350457 Color: 2
Size: 286804 Color: 3

Bin 471: 563 of cap free
Amount of items: 2
Items: 
Size: 767646 Color: 2
Size: 231792 Color: 4

Bin 472: 567 of cap free
Amount of items: 2
Items: 
Size: 706194 Color: 2
Size: 293240 Color: 1

Bin 473: 568 of cap free
Amount of items: 2
Items: 
Size: 672663 Color: 0
Size: 326770 Color: 2

Bin 474: 576 of cap free
Amount of items: 2
Items: 
Size: 714970 Color: 0
Size: 284455 Color: 4

Bin 475: 578 of cap free
Amount of items: 2
Items: 
Size: 666088 Color: 0
Size: 333335 Color: 4

Bin 476: 579 of cap free
Amount of items: 2
Items: 
Size: 578241 Color: 2
Size: 421181 Color: 1

Bin 477: 580 of cap free
Amount of items: 2
Items: 
Size: 543976 Color: 2
Size: 455445 Color: 4

Bin 478: 586 of cap free
Amount of items: 2
Items: 
Size: 635018 Color: 4
Size: 364397 Color: 1

Bin 479: 586 of cap free
Amount of items: 2
Items: 
Size: 644436 Color: 1
Size: 354979 Color: 3

Bin 480: 586 of cap free
Amount of items: 2
Items: 
Size: 782147 Color: 1
Size: 217268 Color: 3

Bin 481: 589 of cap free
Amount of items: 2
Items: 
Size: 774595 Color: 4
Size: 224817 Color: 3

Bin 482: 595 of cap free
Amount of items: 2
Items: 
Size: 782175 Color: 3
Size: 217231 Color: 1

Bin 483: 596 of cap free
Amount of items: 2
Items: 
Size: 534222 Color: 4
Size: 465183 Color: 3

Bin 484: 605 of cap free
Amount of items: 2
Items: 
Size: 552648 Color: 2
Size: 446748 Color: 0

Bin 485: 614 of cap free
Amount of items: 2
Items: 
Size: 690377 Color: 2
Size: 309010 Color: 3

Bin 486: 616 of cap free
Amount of items: 3
Items: 
Size: 381860 Color: 3
Size: 381195 Color: 1
Size: 236330 Color: 4

Bin 487: 616 of cap free
Amount of items: 2
Items: 
Size: 775521 Color: 0
Size: 223864 Color: 2

Bin 488: 617 of cap free
Amount of items: 2
Items: 
Size: 608179 Color: 2
Size: 391205 Color: 3

Bin 489: 625 of cap free
Amount of items: 2
Items: 
Size: 787457 Color: 3
Size: 211919 Color: 4

Bin 490: 628 of cap free
Amount of items: 2
Items: 
Size: 718852 Color: 3
Size: 280521 Color: 4

Bin 491: 632 of cap free
Amount of items: 2
Items: 
Size: 794931 Color: 4
Size: 204438 Color: 1

Bin 492: 638 of cap free
Amount of items: 2
Items: 
Size: 748665 Color: 0
Size: 250698 Color: 1

Bin 493: 643 of cap free
Amount of items: 2
Items: 
Size: 528045 Color: 2
Size: 471313 Color: 4

Bin 494: 644 of cap free
Amount of items: 2
Items: 
Size: 686177 Color: 3
Size: 313180 Color: 2

Bin 495: 648 of cap free
Amount of items: 2
Items: 
Size: 762223 Color: 4
Size: 237130 Color: 0

Bin 496: 652 of cap free
Amount of items: 2
Items: 
Size: 726671 Color: 2
Size: 272678 Color: 4

Bin 497: 653 of cap free
Amount of items: 2
Items: 
Size: 785403 Color: 2
Size: 213945 Color: 1

Bin 498: 654 of cap free
Amount of items: 2
Items: 
Size: 542525 Color: 0
Size: 456822 Color: 2

Bin 499: 654 of cap free
Amount of items: 2
Items: 
Size: 561629 Color: 3
Size: 437718 Color: 2

Bin 500: 655 of cap free
Amount of items: 3
Items: 
Size: 350251 Color: 2
Size: 346171 Color: 0
Size: 302924 Color: 3

Bin 501: 658 of cap free
Amount of items: 3
Items: 
Size: 408767 Color: 4
Size: 408490 Color: 0
Size: 182086 Color: 1

Bin 502: 658 of cap free
Amount of items: 2
Items: 
Size: 766115 Color: 3
Size: 233228 Color: 0

Bin 503: 662 of cap free
Amount of items: 2
Items: 
Size: 525110 Color: 0
Size: 474229 Color: 2

Bin 504: 668 of cap free
Amount of items: 2
Items: 
Size: 771243 Color: 4
Size: 228090 Color: 1

Bin 505: 692 of cap free
Amount of items: 6
Items: 
Size: 168787 Color: 4
Size: 168629 Color: 2
Size: 168628 Color: 1
Size: 168586 Color: 0
Size: 168352 Color: 4
Size: 156327 Color: 3

Bin 506: 692 of cap free
Amount of items: 2
Items: 
Size: 639695 Color: 4
Size: 359614 Color: 3

Bin 507: 693 of cap free
Amount of items: 2
Items: 
Size: 622617 Color: 1
Size: 376691 Color: 3

Bin 508: 693 of cap free
Amount of items: 2
Items: 
Size: 638692 Color: 2
Size: 360616 Color: 4

Bin 509: 693 of cap free
Amount of items: 2
Items: 
Size: 771964 Color: 0
Size: 227344 Color: 2

Bin 510: 694 of cap free
Amount of items: 2
Items: 
Size: 653881 Color: 2
Size: 345426 Color: 4

Bin 511: 696 of cap free
Amount of items: 2
Items: 
Size: 562593 Color: 3
Size: 436712 Color: 2

Bin 512: 698 of cap free
Amount of items: 2
Items: 
Size: 713380 Color: 0
Size: 285923 Color: 2

Bin 513: 700 of cap free
Amount of items: 2
Items: 
Size: 613117 Color: 3
Size: 386184 Color: 4

Bin 514: 702 of cap free
Amount of items: 2
Items: 
Size: 600073 Color: 2
Size: 399226 Color: 0

Bin 515: 704 of cap free
Amount of items: 2
Items: 
Size: 522876 Color: 1
Size: 476421 Color: 0

Bin 516: 708 of cap free
Amount of items: 2
Items: 
Size: 535513 Color: 3
Size: 463780 Color: 4

Bin 517: 711 of cap free
Amount of items: 2
Items: 
Size: 630365 Color: 4
Size: 368925 Color: 3

Bin 518: 713 of cap free
Amount of items: 2
Items: 
Size: 587206 Color: 0
Size: 412082 Color: 1

Bin 519: 718 of cap free
Amount of items: 2
Items: 
Size: 711165 Color: 0
Size: 288118 Color: 1

Bin 520: 723 of cap free
Amount of items: 2
Items: 
Size: 641848 Color: 1
Size: 357430 Color: 0

Bin 521: 734 of cap free
Amount of items: 2
Items: 
Size: 724586 Color: 0
Size: 274681 Color: 2

Bin 522: 738 of cap free
Amount of items: 2
Items: 
Size: 517633 Color: 4
Size: 481630 Color: 3

Bin 523: 740 of cap free
Amount of items: 2
Items: 
Size: 666069 Color: 1
Size: 333192 Color: 4

Bin 524: 748 of cap free
Amount of items: 2
Items: 
Size: 629548 Color: 0
Size: 369705 Color: 3

Bin 525: 749 of cap free
Amount of items: 2
Items: 
Size: 581681 Color: 1
Size: 417571 Color: 0

Bin 526: 754 of cap free
Amount of items: 2
Items: 
Size: 791587 Color: 1
Size: 207660 Color: 0

Bin 527: 754 of cap free
Amount of items: 2
Items: 
Size: 796557 Color: 0
Size: 202690 Color: 3

Bin 528: 762 of cap free
Amount of items: 2
Items: 
Size: 732359 Color: 1
Size: 266880 Color: 3

Bin 529: 767 of cap free
Amount of items: 2
Items: 
Size: 699103 Color: 3
Size: 300131 Color: 1

Bin 530: 768 of cap free
Amount of items: 2
Items: 
Size: 683791 Color: 4
Size: 315442 Color: 1

Bin 531: 769 of cap free
Amount of items: 2
Items: 
Size: 542562 Color: 2
Size: 456670 Color: 1

Bin 532: 776 of cap free
Amount of items: 2
Items: 
Size: 713619 Color: 2
Size: 285606 Color: 0

Bin 533: 776 of cap free
Amount of items: 2
Items: 
Size: 758831 Color: 3
Size: 240394 Color: 4

Bin 534: 781 of cap free
Amount of items: 2
Items: 
Size: 686059 Color: 2
Size: 313161 Color: 0

Bin 535: 785 of cap free
Amount of items: 2
Items: 
Size: 779580 Color: 0
Size: 219636 Color: 4

Bin 536: 794 of cap free
Amount of items: 2
Items: 
Size: 562472 Color: 2
Size: 436735 Color: 3

Bin 537: 797 of cap free
Amount of items: 2
Items: 
Size: 668995 Color: 0
Size: 330209 Color: 3

Bin 538: 798 of cap free
Amount of items: 2
Items: 
Size: 599256 Color: 0
Size: 399947 Color: 2

Bin 539: 800 of cap free
Amount of items: 2
Items: 
Size: 510635 Color: 2
Size: 488566 Color: 0

Bin 540: 807 of cap free
Amount of items: 2
Items: 
Size: 512299 Color: 0
Size: 486895 Color: 4

Bin 541: 808 of cap free
Amount of items: 2
Items: 
Size: 787397 Color: 3
Size: 211796 Color: 1

Bin 542: 812 of cap free
Amount of items: 2
Items: 
Size: 783647 Color: 0
Size: 215542 Color: 4

Bin 543: 819 of cap free
Amount of items: 2
Items: 
Size: 568714 Color: 2
Size: 430468 Color: 1

Bin 544: 819 of cap free
Amount of items: 2
Items: 
Size: 728849 Color: 3
Size: 270333 Color: 2

Bin 545: 821 of cap free
Amount of items: 2
Items: 
Size: 622270 Color: 4
Size: 376910 Color: 1

Bin 546: 835 of cap free
Amount of items: 2
Items: 
Size: 532946 Color: 3
Size: 466220 Color: 2

Bin 547: 841 of cap free
Amount of items: 2
Items: 
Size: 607222 Color: 1
Size: 391938 Color: 4

Bin 548: 842 of cap free
Amount of items: 2
Items: 
Size: 765935 Color: 4
Size: 233224 Color: 0

Bin 549: 846 of cap free
Amount of items: 2
Items: 
Size: 728617 Color: 2
Size: 270538 Color: 3

Bin 550: 850 of cap free
Amount of items: 2
Items: 
Size: 595748 Color: 0
Size: 403403 Color: 3

Bin 551: 858 of cap free
Amount of items: 3
Items: 
Size: 344598 Color: 2
Size: 341387 Color: 3
Size: 313158 Color: 0

Bin 552: 860 of cap free
Amount of items: 2
Items: 
Size: 667293 Color: 2
Size: 331848 Color: 3

Bin 553: 868 of cap free
Amount of items: 2
Items: 
Size: 760743 Color: 2
Size: 238390 Color: 4

Bin 554: 883 of cap free
Amount of items: 2
Items: 
Size: 545692 Color: 3
Size: 453426 Color: 1

Bin 555: 891 of cap free
Amount of items: 2
Items: 
Size: 570762 Color: 4
Size: 428348 Color: 1

Bin 556: 892 of cap free
Amount of items: 2
Items: 
Size: 536427 Color: 0
Size: 462682 Color: 2

Bin 557: 892 of cap free
Amount of items: 2
Items: 
Size: 656716 Color: 4
Size: 342393 Color: 3

Bin 558: 894 of cap free
Amount of items: 2
Items: 
Size: 718610 Color: 3
Size: 280497 Color: 1

Bin 559: 895 of cap free
Amount of items: 2
Items: 
Size: 716856 Color: 4
Size: 282250 Color: 3

Bin 560: 901 of cap free
Amount of items: 2
Items: 
Size: 599162 Color: 0
Size: 399938 Color: 3

Bin 561: 902 of cap free
Amount of items: 2
Items: 
Size: 673666 Color: 2
Size: 325433 Color: 1

Bin 562: 902 of cap free
Amount of items: 2
Items: 
Size: 741936 Color: 3
Size: 257163 Color: 0

Bin 563: 906 of cap free
Amount of items: 2
Items: 
Size: 548873 Color: 1
Size: 450222 Color: 3

Bin 564: 916 of cap free
Amount of items: 2
Items: 
Size: 672663 Color: 4
Size: 326422 Color: 3

Bin 565: 919 of cap free
Amount of items: 2
Items: 
Size: 727605 Color: 3
Size: 271477 Color: 1

Bin 566: 920 of cap free
Amount of items: 2
Items: 
Size: 542431 Color: 1
Size: 456650 Color: 3

Bin 567: 926 of cap free
Amount of items: 7
Items: 
Size: 153769 Color: 3
Size: 148822 Color: 0
Size: 148368 Color: 0
Size: 148362 Color: 2
Size: 147363 Color: 2
Size: 147148 Color: 2
Size: 105243 Color: 2

Bin 568: 926 of cap free
Amount of items: 2
Items: 
Size: 546042 Color: 1
Size: 453033 Color: 3

Bin 569: 926 of cap free
Amount of items: 2
Items: 
Size: 794284 Color: 2
Size: 204791 Color: 4

Bin 570: 928 of cap free
Amount of items: 2
Items: 
Size: 593077 Color: 1
Size: 405996 Color: 4

Bin 571: 929 of cap free
Amount of items: 2
Items: 
Size: 607069 Color: 3
Size: 392003 Color: 1

Bin 572: 936 of cap free
Amount of items: 2
Items: 
Size: 671320 Color: 1
Size: 327745 Color: 2

Bin 573: 939 of cap free
Amount of items: 2
Items: 
Size: 560514 Color: 1
Size: 438548 Color: 2

Bin 574: 958 of cap free
Amount of items: 2
Items: 
Size: 701920 Color: 1
Size: 297123 Color: 0

Bin 575: 959 of cap free
Amount of items: 2
Items: 
Size: 631927 Color: 4
Size: 367115 Color: 0

Bin 576: 962 of cap free
Amount of items: 2
Items: 
Size: 685957 Color: 4
Size: 313082 Color: 2

Bin 577: 967 of cap free
Amount of items: 2
Items: 
Size: 791490 Color: 2
Size: 207544 Color: 1

Bin 578: 969 of cap free
Amount of items: 2
Items: 
Size: 608165 Color: 0
Size: 390867 Color: 3

Bin 579: 986 of cap free
Amount of items: 3
Items: 
Size: 407284 Color: 1
Size: 384197 Color: 0
Size: 207534 Color: 4

Bin 580: 991 of cap free
Amount of items: 2
Items: 
Size: 696409 Color: 0
Size: 302601 Color: 2

Bin 581: 994 of cap free
Amount of items: 2
Items: 
Size: 726343 Color: 0
Size: 272664 Color: 1

Bin 582: 1010 of cap free
Amount of items: 2
Items: 
Size: 536324 Color: 1
Size: 462667 Color: 0

Bin 583: 1017 of cap free
Amount of items: 2
Items: 
Size: 755585 Color: 1
Size: 243399 Color: 0

Bin 584: 1018 of cap free
Amount of items: 2
Items: 
Size: 608807 Color: 3
Size: 390176 Color: 4

Bin 585: 1018 of cap free
Amount of items: 2
Items: 
Size: 722063 Color: 0
Size: 276920 Color: 3

Bin 586: 1022 of cap free
Amount of items: 2
Items: 
Size: 751863 Color: 2
Size: 247116 Color: 0

Bin 587: 1028 of cap free
Amount of items: 2
Items: 
Size: 668565 Color: 1
Size: 330408 Color: 0

Bin 588: 1030 of cap free
Amount of items: 2
Items: 
Size: 692379 Color: 3
Size: 306592 Color: 0

Bin 589: 1032 of cap free
Amount of items: 2
Items: 
Size: 521497 Color: 0
Size: 477472 Color: 2

Bin 590: 1040 of cap free
Amount of items: 4
Items: 
Size: 308121 Color: 0
Size: 295462 Color: 4
Size: 274650 Color: 3
Size: 120728 Color: 2

Bin 591: 1040 of cap free
Amount of items: 2
Items: 
Size: 616237 Color: 4
Size: 382724 Color: 3

Bin 592: 1042 of cap free
Amount of items: 2
Items: 
Size: 653340 Color: 0
Size: 345619 Color: 2

Bin 593: 1047 of cap free
Amount of items: 2
Items: 
Size: 656680 Color: 4
Size: 342274 Color: 2

Bin 594: 1047 of cap free
Amount of items: 2
Items: 
Size: 765877 Color: 0
Size: 233077 Color: 2

Bin 595: 1060 of cap free
Amount of items: 2
Items: 
Size: 733615 Color: 2
Size: 265326 Color: 3

Bin 596: 1064 of cap free
Amount of items: 2
Items: 
Size: 566531 Color: 2
Size: 432406 Color: 4

Bin 597: 1065 of cap free
Amount of items: 2
Items: 
Size: 557999 Color: 3
Size: 440937 Color: 0

Bin 598: 1075 of cap free
Amount of items: 2
Items: 
Size: 580461 Color: 0
Size: 418465 Color: 2

Bin 599: 1077 of cap free
Amount of items: 2
Items: 
Size: 626528 Color: 3
Size: 372396 Color: 0

Bin 600: 1082 of cap free
Amount of items: 2
Items: 
Size: 758732 Color: 3
Size: 240187 Color: 4

Bin 601: 1083 of cap free
Amount of items: 2
Items: 
Size: 791392 Color: 2
Size: 207526 Color: 1

Bin 602: 1106 of cap free
Amount of items: 4
Items: 
Size: 306330 Color: 0
Size: 263574 Color: 2
Size: 262619 Color: 4
Size: 166372 Color: 1

Bin 603: 1107 of cap free
Amount of items: 2
Items: 
Size: 679652 Color: 3
Size: 319242 Color: 1

Bin 604: 1112 of cap free
Amount of items: 2
Items: 
Size: 512191 Color: 0
Size: 486698 Color: 4

Bin 605: 1121 of cap free
Amount of items: 2
Items: 
Size: 765804 Color: 4
Size: 233076 Color: 1

Bin 606: 1144 of cap free
Amount of items: 2
Items: 
Size: 608742 Color: 3
Size: 390115 Color: 1

Bin 607: 1148 of cap free
Amount of items: 2
Items: 
Size: 718146 Color: 1
Size: 280707 Color: 3

Bin 608: 1150 of cap free
Amount of items: 2
Items: 
Size: 785105 Color: 0
Size: 213746 Color: 4

Bin 609: 1151 of cap free
Amount of items: 2
Items: 
Size: 769833 Color: 0
Size: 229017 Color: 1

Bin 610: 1155 of cap free
Amount of items: 2
Items: 
Size: 783910 Color: 4
Size: 214936 Color: 2

Bin 611: 1162 of cap free
Amount of items: 2
Items: 
Size: 587648 Color: 1
Size: 411191 Color: 3

Bin 612: 1169 of cap free
Amount of items: 2
Items: 
Size: 698863 Color: 3
Size: 299969 Color: 1

Bin 613: 1176 of cap free
Amount of items: 2
Items: 
Size: 641509 Color: 3
Size: 357316 Color: 2

Bin 614: 1186 of cap free
Amount of items: 2
Items: 
Size: 593066 Color: 3
Size: 405749 Color: 1

Bin 615: 1203 of cap free
Amount of items: 2
Items: 
Size: 779204 Color: 2
Size: 219594 Color: 3

Bin 616: 1224 of cap free
Amount of items: 2
Items: 
Size: 612976 Color: 1
Size: 385801 Color: 4

Bin 617: 1224 of cap free
Amount of items: 2
Items: 
Size: 731898 Color: 2
Size: 266879 Color: 1

Bin 618: 1244 of cap free
Amount of items: 2
Items: 
Size: 562517 Color: 3
Size: 436240 Color: 4

Bin 619: 1254 of cap free
Amount of items: 2
Items: 
Size: 737113 Color: 4
Size: 261634 Color: 3

Bin 620: 1263 of cap free
Amount of items: 4
Items: 
Size: 306065 Color: 0
Size: 252395 Color: 2
Size: 250460 Color: 4
Size: 189818 Color: 4

Bin 621: 1272 of cap free
Amount of items: 2
Items: 
Size: 595535 Color: 1
Size: 403194 Color: 4

Bin 622: 1274 of cap free
Amount of items: 3
Items: 
Size: 361675 Color: 4
Size: 350403 Color: 1
Size: 286649 Color: 2

Bin 623: 1282 of cap free
Amount of items: 2
Items: 
Size: 788390 Color: 1
Size: 210329 Color: 2

Bin 624: 1309 of cap free
Amount of items: 2
Items: 
Size: 709067 Color: 0
Size: 289625 Color: 1

Bin 625: 1315 of cap free
Amount of items: 2
Items: 
Size: 737082 Color: 4
Size: 261604 Color: 2

Bin 626: 1322 of cap free
Amount of items: 2
Items: 
Size: 574887 Color: 4
Size: 423792 Color: 1

Bin 627: 1325 of cap free
Amount of items: 2
Items: 
Size: 665580 Color: 0
Size: 333096 Color: 3

Bin 628: 1328 of cap free
Amount of items: 2
Items: 
Size: 675784 Color: 3
Size: 322889 Color: 1

Bin 629: 1330 of cap free
Amount of items: 2
Items: 
Size: 765737 Color: 4
Size: 232934 Color: 1

Bin 630: 1343 of cap free
Amount of items: 2
Items: 
Size: 679773 Color: 1
Size: 318885 Color: 0

Bin 631: 1358 of cap free
Amount of items: 2
Items: 
Size: 548703 Color: 0
Size: 449940 Color: 2

Bin 632: 1360 of cap free
Amount of items: 2
Items: 
Size: 559310 Color: 2
Size: 439331 Color: 1

Bin 633: 1368 of cap free
Amount of items: 2
Items: 
Size: 632486 Color: 0
Size: 366147 Color: 2

Bin 634: 1382 of cap free
Amount of items: 2
Items: 
Size: 660339 Color: 2
Size: 338280 Color: 3

Bin 635: 1384 of cap free
Amount of items: 2
Items: 
Size: 656420 Color: 2
Size: 342197 Color: 1

Bin 636: 1393 of cap free
Amount of items: 2
Items: 
Size: 619786 Color: 3
Size: 378822 Color: 4

Bin 637: 1401 of cap free
Amount of items: 2
Items: 
Size: 542137 Color: 1
Size: 456463 Color: 0

Bin 638: 1408 of cap free
Amount of items: 2
Items: 
Size: 514152 Color: 1
Size: 484441 Color: 4

Bin 639: 1409 of cap free
Amount of items: 2
Items: 
Size: 517035 Color: 4
Size: 481557 Color: 2

Bin 640: 1411 of cap free
Amount of items: 2
Items: 
Size: 510086 Color: 0
Size: 488504 Color: 1

Bin 641: 1422 of cap free
Amount of items: 2
Items: 
Size: 583683 Color: 1
Size: 414896 Color: 2

Bin 642: 1424 of cap free
Amount of items: 2
Items: 
Size: 740519 Color: 4
Size: 258058 Color: 3

Bin 643: 1426 of cap free
Amount of items: 2
Items: 
Size: 539303 Color: 1
Size: 459272 Color: 3

Bin 644: 1429 of cap free
Amount of items: 2
Items: 
Size: 791113 Color: 4
Size: 207459 Color: 0

Bin 645: 1437 of cap free
Amount of items: 2
Items: 
Size: 794270 Color: 2
Size: 204294 Color: 4

Bin 646: 1445 of cap free
Amount of items: 2
Items: 
Size: 522308 Color: 2
Size: 476248 Color: 1

Bin 647: 1459 of cap free
Amount of items: 2
Items: 
Size: 660977 Color: 3
Size: 337565 Color: 4

Bin 648: 1505 of cap free
Amount of items: 2
Items: 
Size: 733355 Color: 1
Size: 265141 Color: 2

Bin 649: 1515 of cap free
Amount of items: 2
Items: 
Size: 677605 Color: 1
Size: 320881 Color: 0

Bin 650: 1523 of cap free
Amount of items: 2
Items: 
Size: 767717 Color: 4
Size: 230761 Color: 3

Bin 651: 1536 of cap free
Amount of items: 2
Items: 
Size: 796450 Color: 3
Size: 202015 Color: 1

Bin 652: 1546 of cap free
Amount of items: 2
Items: 
Size: 668311 Color: 4
Size: 330144 Color: 0

Bin 653: 1560 of cap free
Amount of items: 2
Items: 
Size: 545422 Color: 4
Size: 453019 Color: 0

Bin 654: 1561 of cap free
Amount of items: 2
Items: 
Size: 771483 Color: 1
Size: 226957 Color: 3

Bin 655: 1565 of cap free
Amount of items: 2
Items: 
Size: 568417 Color: 1
Size: 430019 Color: 3

Bin 656: 1565 of cap free
Amount of items: 2
Items: 
Size: 644024 Color: 1
Size: 354412 Color: 0

Bin 657: 1567 of cap free
Amount of items: 2
Items: 
Size: 715677 Color: 2
Size: 282757 Color: 4

Bin 658: 1575 of cap free
Amount of items: 2
Items: 
Size: 755036 Color: 0
Size: 243390 Color: 3

Bin 659: 1576 of cap free
Amount of items: 2
Items: 
Size: 721661 Color: 1
Size: 276764 Color: 3

Bin 660: 1592 of cap free
Amount of items: 2
Items: 
Size: 595407 Color: 3
Size: 403002 Color: 1

Bin 661: 1599 of cap free
Amount of items: 2
Items: 
Size: 570710 Color: 2
Size: 427692 Color: 4

Bin 662: 1602 of cap free
Amount of items: 2
Items: 
Size: 565788 Color: 4
Size: 432611 Color: 2

Bin 663: 1643 of cap free
Amount of items: 2
Items: 
Size: 602397 Color: 4
Size: 395961 Color: 1

Bin 664: 1659 of cap free
Amount of items: 2
Items: 
Size: 592623 Color: 1
Size: 405719 Color: 2

Bin 665: 1661 of cap free
Amount of items: 2
Items: 
Size: 683262 Color: 2
Size: 315078 Color: 1

Bin 666: 1676 of cap free
Amount of items: 2
Items: 
Size: 769415 Color: 2
Size: 228910 Color: 1

Bin 667: 1679 of cap free
Amount of items: 2
Items: 
Size: 509862 Color: 0
Size: 488460 Color: 1

Bin 668: 1686 of cap free
Amount of items: 2
Items: 
Size: 714444 Color: 0
Size: 283871 Color: 2

Bin 669: 1686 of cap free
Amount of items: 2
Items: 
Size: 775076 Color: 3
Size: 223239 Color: 2

Bin 670: 1695 of cap free
Amount of items: 2
Items: 
Size: 629396 Color: 4
Size: 368910 Color: 2

Bin 671: 1697 of cap free
Amount of items: 3
Items: 
Size: 381145 Color: 0
Size: 381074 Color: 3
Size: 236085 Color: 4

Bin 672: 1749 of cap free
Amount of items: 2
Items: 
Size: 675649 Color: 1
Size: 322603 Color: 2

Bin 673: 1821 of cap free
Amount of items: 2
Items: 
Size: 501839 Color: 0
Size: 496341 Color: 3

Bin 674: 1821 of cap free
Amount of items: 2
Items: 
Size: 794023 Color: 4
Size: 204157 Color: 3

Bin 675: 1849 of cap free
Amount of items: 2
Items: 
Size: 718342 Color: 3
Size: 279810 Color: 2

Bin 676: 1871 of cap free
Amount of items: 2
Items: 
Size: 710055 Color: 1
Size: 288075 Color: 4

Bin 677: 1882 of cap free
Amount of items: 2
Items: 
Size: 530976 Color: 0
Size: 467143 Color: 2

Bin 678: 1911 of cap free
Amount of items: 2
Items: 
Size: 513679 Color: 1
Size: 484411 Color: 3

Bin 679: 1912 of cap free
Amount of items: 2
Items: 
Size: 721586 Color: 1
Size: 276503 Color: 3

Bin 680: 1954 of cap free
Amount of items: 2
Items: 
Size: 624041 Color: 0
Size: 374006 Color: 2

Bin 681: 1961 of cap free
Amount of items: 3
Items: 
Size: 344071 Color: 2
Size: 341103 Color: 4
Size: 312866 Color: 4

Bin 682: 1962 of cap free
Amount of items: 2
Items: 
Size: 539340 Color: 3
Size: 458699 Color: 2

Bin 683: 1977 of cap free
Amount of items: 2
Items: 
Size: 778868 Color: 2
Size: 219156 Color: 4

Bin 684: 2012 of cap free
Amount of items: 2
Items: 
Size: 629271 Color: 2
Size: 368718 Color: 4

Bin 685: 2014 of cap free
Amount of items: 2
Items: 
Size: 629271 Color: 3
Size: 368716 Color: 4

Bin 686: 2040 of cap free
Amount of items: 2
Items: 
Size: 667925 Color: 3
Size: 330036 Color: 0

Bin 687: 2077 of cap free
Amount of items: 2
Items: 
Size: 525242 Color: 2
Size: 472682 Color: 0

Bin 688: 2105 of cap free
Amount of items: 2
Items: 
Size: 500940 Color: 2
Size: 496956 Color: 0

Bin 689: 2105 of cap free
Amount of items: 2
Items: 
Size: 645903 Color: 2
Size: 351993 Color: 1

Bin 690: 2106 of cap free
Amount of items: 2
Items: 
Size: 796177 Color: 4
Size: 201718 Color: 3

Bin 691: 2128 of cap free
Amount of items: 2
Items: 
Size: 598126 Color: 1
Size: 399747 Color: 2

Bin 692: 2147 of cap free
Amount of items: 2
Items: 
Size: 580792 Color: 2
Size: 417062 Color: 1

Bin 693: 2178 of cap free
Amount of items: 2
Items: 
Size: 727505 Color: 1
Size: 270318 Color: 0

Bin 694: 2187 of cap free
Amount of items: 2
Items: 
Size: 634907 Color: 1
Size: 362907 Color: 3

Bin 695: 2200 of cap free
Amount of items: 2
Items: 
Size: 608376 Color: 3
Size: 389425 Color: 1

Bin 696: 2217 of cap free
Amount of items: 2
Items: 
Size: 576999 Color: 0
Size: 420785 Color: 2

Bin 697: 2222 of cap free
Amount of items: 9
Items: 
Size: 113542 Color: 1
Size: 112768 Color: 0
Size: 111567 Color: 3
Size: 111296 Color: 4
Size: 110320 Color: 2
Size: 109821 Color: 3
Size: 109698 Color: 2
Size: 109464 Color: 2
Size: 109303 Color: 3

Bin 698: 2235 of cap free
Amount of items: 2
Items: 
Size: 767141 Color: 0
Size: 230625 Color: 4

Bin 699: 2266 of cap free
Amount of items: 2
Items: 
Size: 516352 Color: 4
Size: 481383 Color: 2

Bin 700: 2290 of cap free
Amount of items: 2
Items: 
Size: 640543 Color: 2
Size: 357168 Color: 4

Bin 701: 2296 of cap free
Amount of items: 2
Items: 
Size: 697986 Color: 1
Size: 299719 Color: 4

Bin 702: 2312 of cap free
Amount of items: 2
Items: 
Size: 557437 Color: 4
Size: 440252 Color: 2

Bin 703: 2323 of cap free
Amount of items: 2
Items: 
Size: 629132 Color: 2
Size: 368546 Color: 0

Bin 704: 2333 of cap free
Amount of items: 2
Items: 
Size: 645989 Color: 1
Size: 351679 Color: 4

Bin 705: 2336 of cap free
Amount of items: 3
Items: 
Size: 408233 Color: 1
Size: 408166 Color: 2
Size: 181266 Color: 1

Bin 706: 2358 of cap free
Amount of items: 2
Items: 
Size: 612326 Color: 2
Size: 385317 Color: 1

Bin 707: 2387 of cap free
Amount of items: 2
Items: 
Size: 650813 Color: 2
Size: 346801 Color: 1

Bin 708: 2394 of cap free
Amount of items: 2
Items: 
Size: 685089 Color: 3
Size: 312518 Color: 2

Bin 709: 2406 of cap free
Amount of items: 2
Items: 
Size: 697880 Color: 1
Size: 299715 Color: 2

Bin 710: 2429 of cap free
Amount of items: 2
Items: 
Size: 777967 Color: 1
Size: 219605 Color: 2

Bin 711: 2441 of cap free
Amount of items: 2
Items: 
Size: 787358 Color: 1
Size: 210202 Color: 2

Bin 712: 2489 of cap free
Amount of items: 2
Items: 
Size: 548544 Color: 0
Size: 448968 Color: 2

Bin 713: 2503 of cap free
Amount of items: 2
Items: 
Size: 623785 Color: 3
Size: 373713 Color: 4

Bin 714: 2560 of cap free
Amount of items: 2
Items: 
Size: 762064 Color: 2
Size: 235377 Color: 4

Bin 715: 2574 of cap free
Amount of items: 2
Items: 
Size: 565389 Color: 4
Size: 432038 Color: 0

Bin 716: 2575 of cap free
Amount of items: 2
Items: 
Size: 509517 Color: 1
Size: 487909 Color: 3

Bin 717: 2626 of cap free
Amount of items: 2
Items: 
Size: 793457 Color: 3
Size: 203918 Color: 2

Bin 718: 2657 of cap free
Amount of items: 2
Items: 
Size: 623749 Color: 3
Size: 373595 Color: 0

Bin 719: 2687 of cap free
Amount of items: 2
Items: 
Size: 631444 Color: 0
Size: 365870 Color: 2

Bin 720: 2698 of cap free
Amount of items: 2
Items: 
Size: 524646 Color: 3
Size: 472657 Color: 1

Bin 721: 2756 of cap free
Amount of items: 3
Items: 
Size: 408168 Color: 1
Size: 407999 Color: 0
Size: 181078 Color: 4

Bin 722: 2859 of cap free
Amount of items: 2
Items: 
Size: 655136 Color: 2
Size: 342006 Color: 0

Bin 723: 2859 of cap free
Amount of items: 2
Items: 
Size: 740744 Color: 3
Size: 256398 Color: 0

Bin 724: 2869 of cap free
Amount of items: 2
Items: 
Size: 576543 Color: 3
Size: 420589 Color: 4

Bin 725: 2871 of cap free
Amount of items: 2
Items: 
Size: 601200 Color: 3
Size: 395930 Color: 1

Bin 726: 2909 of cap free
Amount of items: 2
Items: 
Size: 796119 Color: 1
Size: 200973 Color: 4

Bin 727: 2956 of cap free
Amount of items: 2
Items: 
Size: 727453 Color: 1
Size: 269592 Color: 2

Bin 728: 2999 of cap free
Amount of items: 2
Items: 
Size: 538668 Color: 1
Size: 458334 Color: 4

Bin 729: 3009 of cap free
Amount of items: 2
Items: 
Size: 547962 Color: 4
Size: 449030 Color: 0

Bin 730: 3021 of cap free
Amount of items: 2
Items: 
Size: 534987 Color: 3
Size: 461993 Color: 0

Bin 731: 3044 of cap free
Amount of items: 2
Items: 
Size: 660100 Color: 2
Size: 336857 Color: 4

Bin 732: 3048 of cap free
Amount of items: 2
Items: 
Size: 777691 Color: 1
Size: 219262 Color: 2

Bin 733: 3060 of cap free
Amount of items: 2
Items: 
Size: 666938 Color: 0
Size: 330003 Color: 4

Bin 734: 3174 of cap free
Amount of items: 2
Items: 
Size: 691453 Color: 0
Size: 305374 Color: 2

Bin 735: 3206 of cap free
Amount of items: 2
Items: 
Size: 530095 Color: 2
Size: 466700 Color: 3

Bin 736: 3233 of cap free
Amount of items: 2
Items: 
Size: 556935 Color: 1
Size: 439833 Color: 2

Bin 737: 3308 of cap free
Amount of items: 2
Items: 
Size: 727347 Color: 1
Size: 269346 Color: 2

Bin 738: 3332 of cap free
Amount of items: 2
Items: 
Size: 774125 Color: 0
Size: 222544 Color: 2

Bin 739: 3348 of cap free
Amount of items: 2
Items: 
Size: 524526 Color: 4
Size: 472127 Color: 1

Bin 740: 3353 of cap free
Amount of items: 2
Items: 
Size: 544489 Color: 4
Size: 452159 Color: 3

Bin 741: 3364 of cap free
Amount of items: 2
Items: 
Size: 580617 Color: 2
Size: 416020 Color: 1

Bin 742: 3398 of cap free
Amount of items: 2
Items: 
Size: 787300 Color: 2
Size: 209303 Color: 3

Bin 743: 3437 of cap free
Amount of items: 2
Items: 
Size: 684274 Color: 1
Size: 312290 Color: 0

Bin 744: 3455 of cap free
Amount of items: 2
Items: 
Size: 602158 Color: 1
Size: 394388 Color: 3

Bin 745: 3527 of cap free
Amount of items: 2
Items: 
Size: 597113 Color: 3
Size: 399361 Color: 2

Bin 746: 3575 of cap free
Amount of items: 2
Items: 
Size: 500345 Color: 0
Size: 496081 Color: 3

Bin 747: 3595 of cap free
Amount of items: 5
Items: 
Size: 248248 Color: 0
Size: 187319 Color: 1
Size: 187204 Color: 1
Size: 187041 Color: 0
Size: 186594 Color: 0

Bin 748: 3667 of cap free
Amount of items: 2
Items: 
Size: 548235 Color: 0
Size: 448099 Color: 4

Bin 749: 3714 of cap free
Amount of items: 2
Items: 
Size: 543572 Color: 0
Size: 452715 Color: 4

Bin 750: 3723 of cap free
Amount of items: 2
Items: 
Size: 567952 Color: 0
Size: 428326 Color: 2

Bin 751: 3734 of cap free
Amount of items: 2
Items: 
Size: 508361 Color: 2
Size: 487906 Color: 1

Bin 752: 3780 of cap free
Amount of items: 2
Items: 
Size: 508321 Color: 4
Size: 487900 Color: 1

Bin 753: 3834 of cap free
Amount of items: 2
Items: 
Size: 560431 Color: 1
Size: 435736 Color: 4

Bin 754: 3883 of cap free
Amount of items: 2
Items: 
Size: 509269 Color: 1
Size: 486849 Color: 0

Bin 755: 3910 of cap free
Amount of items: 2
Items: 
Size: 529008 Color: 4
Size: 467083 Color: 2

Bin 756: 4212 of cap free
Amount of items: 2
Items: 
Size: 683594 Color: 1
Size: 312195 Color: 4

Bin 757: 4274 of cap free
Amount of items: 2
Items: 
Size: 740447 Color: 2
Size: 255280 Color: 3

Bin 758: 4313 of cap free
Amount of items: 2
Items: 
Size: 596848 Color: 1
Size: 398840 Color: 0

Bin 759: 4389 of cap free
Amount of items: 2
Items: 
Size: 556840 Color: 0
Size: 438772 Color: 1

Bin 760: 4390 of cap free
Amount of items: 2
Items: 
Size: 665964 Color: 3
Size: 329647 Color: 2

Bin 761: 4404 of cap free
Amount of items: 2
Items: 
Size: 740434 Color: 4
Size: 255163 Color: 1

Bin 762: 4577 of cap free
Amount of items: 2
Items: 
Size: 574911 Color: 1
Size: 420513 Color: 0

Bin 763: 4691 of cap free
Amount of items: 2
Items: 
Size: 499274 Color: 0
Size: 496036 Color: 1

Bin 764: 4707 of cap free
Amount of items: 2
Items: 
Size: 697854 Color: 0
Size: 297440 Color: 1

Bin 765: 5027 of cap free
Amount of items: 2
Items: 
Size: 643452 Color: 0
Size: 351522 Color: 1

Bin 766: 5035 of cap free
Amount of items: 2
Items: 
Size: 740124 Color: 0
Size: 254842 Color: 3

Bin 767: 5061 of cap free
Amount of items: 2
Items: 
Size: 508256 Color: 1
Size: 486684 Color: 3

Bin 768: 5066 of cap free
Amount of items: 2
Items: 
Size: 753050 Color: 4
Size: 241885 Color: 1

Bin 769: 5148 of cap free
Amount of items: 2
Items: 
Size: 607051 Color: 0
Size: 387802 Color: 2

Bin 770: 5153 of cap free
Amount of items: 2
Items: 
Size: 775696 Color: 2
Size: 219152 Color: 0

Bin 771: 5175 of cap free
Amount of items: 2
Items: 
Size: 726274 Color: 1
Size: 268552 Color: 2

Bin 772: 5217 of cap free
Amount of items: 2
Items: 
Size: 574272 Color: 3
Size: 420512 Color: 2

Bin 773: 5394 of cap free
Amount of items: 2
Items: 
Size: 682539 Color: 2
Size: 312068 Color: 4

Bin 774: 5606 of cap free
Amount of items: 2
Items: 
Size: 606957 Color: 0
Size: 387438 Color: 2

Bin 775: 5683 of cap free
Amount of items: 2
Items: 
Size: 697367 Color: 2
Size: 296951 Color: 1

Bin 776: 5772 of cap free
Amount of items: 2
Items: 
Size: 623208 Color: 3
Size: 371021 Color: 4

Bin 777: 5893 of cap free
Amount of items: 6
Items: 
Size: 165954 Color: 4
Size: 165924 Color: 2
Size: 165732 Color: 0
Size: 165612 Color: 3
Size: 165546 Color: 4
Size: 165340 Color: 4

Bin 778: 5979 of cap free
Amount of items: 2
Items: 
Size: 726086 Color: 1
Size: 267936 Color: 2

Bin 779: 6117 of cap free
Amount of items: 2
Items: 
Size: 664714 Color: 1
Size: 329170 Color: 4

Bin 780: 6230 of cap free
Amount of items: 2
Items: 
Size: 507439 Color: 4
Size: 486332 Color: 2

Bin 781: 6266 of cap free
Amount of items: 2
Items: 
Size: 739786 Color: 2
Size: 253949 Color: 1

Bin 782: 6352 of cap free
Amount of items: 2
Items: 
Size: 697425 Color: 1
Size: 296224 Color: 4

Bin 783: 6566 of cap free
Amount of items: 2
Items: 
Size: 794019 Color: 2
Size: 199416 Color: 1

Bin 784: 6573 of cap free
Amount of items: 2
Items: 
Size: 682237 Color: 1
Size: 311191 Color: 0

Bin 785: 6809 of cap free
Amount of items: 2
Items: 
Size: 664140 Color: 2
Size: 329052 Color: 0

Bin 786: 7174 of cap free
Amount of items: 2
Items: 
Size: 520863 Color: 0
Size: 471964 Color: 2

Bin 787: 7182 of cap free
Amount of items: 2
Items: 
Size: 739762 Color: 0
Size: 253057 Color: 1

Bin 788: 7183 of cap free
Amount of items: 2
Items: 
Size: 725973 Color: 0
Size: 266845 Color: 2

Bin 789: 7201 of cap free
Amount of items: 2
Items: 
Size: 663838 Color: 3
Size: 328962 Color: 0

Bin 790: 7219 of cap free
Amount of items: 2
Items: 
Size: 565014 Color: 4
Size: 427768 Color: 2

Bin 791: 7321 of cap free
Amount of items: 2
Items: 
Size: 793290 Color: 1
Size: 199390 Color: 0

Bin 792: 7346 of cap free
Amount of items: 3
Items: 
Size: 339974 Color: 2
Size: 326412 Color: 3
Size: 326269 Color: 2

Bin 793: 7513 of cap free
Amount of items: 2
Items: 
Size: 738658 Color: 4
Size: 253830 Color: 0

Bin 794: 7577 of cap free
Amount of items: 2
Items: 
Size: 496720 Color: 0
Size: 495704 Color: 3

Bin 795: 7591 of cap free
Amount of items: 2
Items: 
Size: 606813 Color: 4
Size: 385597 Color: 2

Bin 796: 7605 of cap free
Amount of items: 2
Items: 
Size: 793155 Color: 1
Size: 199241 Color: 4

Bin 797: 7619 of cap free
Amount of items: 2
Items: 
Size: 773589 Color: 3
Size: 218793 Color: 0

Bin 798: 7763 of cap free
Amount of items: 2
Items: 
Size: 521030 Color: 2
Size: 471208 Color: 0

Bin 799: 7981 of cap free
Amount of items: 2
Items: 
Size: 663202 Color: 1
Size: 328818 Color: 0

Bin 800: 8014 of cap free
Amount of items: 2
Items: 
Size: 773451 Color: 4
Size: 218536 Color: 1

Bin 801: 8240 of cap free
Amount of items: 2
Items: 
Size: 709759 Color: 1
Size: 282002 Color: 0

Bin 802: 8348 of cap free
Amount of items: 2
Items: 
Size: 640626 Color: 4
Size: 351027 Color: 1

Bin 803: 8348 of cap free
Amount of items: 2
Items: 
Size: 764957 Color: 4
Size: 226696 Color: 0

Bin 804: 8440 of cap free
Amount of items: 2
Items: 
Size: 738833 Color: 0
Size: 252728 Color: 4

Bin 805: 8577 of cap free
Amount of items: 2
Items: 
Size: 640040 Color: 3
Size: 351384 Color: 4

Bin 806: 8589 of cap free
Amount of items: 2
Items: 
Size: 695243 Color: 1
Size: 296169 Color: 3

Bin 807: 8631 of cap free
Amount of items: 2
Items: 
Size: 726258 Color: 2
Size: 265112 Color: 1

Bin 808: 8836 of cap free
Amount of items: 2
Items: 
Size: 519819 Color: 3
Size: 471346 Color: 2

Bin 809: 8864 of cap free
Amount of items: 2
Items: 
Size: 663592 Color: 0
Size: 327545 Color: 1

Bin 810: 8875 of cap free
Amount of items: 2
Items: 
Size: 605803 Color: 4
Size: 385323 Color: 2

Bin 811: 9160 of cap free
Amount of items: 2
Items: 
Size: 495425 Color: 3
Size: 495416 Color: 2

Bin 812: 9549 of cap free
Amount of items: 2
Items: 
Size: 573630 Color: 1
Size: 416822 Color: 2

Bin 813: 10258 of cap free
Amount of items: 2
Items: 
Size: 495107 Color: 1
Size: 494636 Color: 0

Bin 814: 10333 of cap free
Amount of items: 2
Items: 
Size: 562446 Color: 4
Size: 427222 Color: 2

Bin 815: 10619 of cap free
Amount of items: 2
Items: 
Size: 542412 Color: 0
Size: 446970 Color: 2

Bin 816: 10929 of cap free
Amount of items: 2
Items: 
Size: 725531 Color: 2
Size: 263541 Color: 0

Bin 817: 10955 of cap free
Amount of items: 2
Items: 
Size: 595269 Color: 0
Size: 393777 Color: 4

Bin 818: 10972 of cap free
Amount of items: 2
Items: 
Size: 621937 Color: 2
Size: 367092 Color: 0

Bin 819: 11058 of cap free
Amount of items: 2
Items: 
Size: 708711 Color: 4
Size: 280232 Color: 3

Bin 820: 11373 of cap free
Amount of items: 2
Items: 
Size: 494393 Color: 3
Size: 494235 Color: 0

Bin 821: 11836 of cap free
Amount of items: 2
Items: 
Size: 708643 Color: 1
Size: 279522 Color: 4

Bin 822: 12263 of cap free
Amount of items: 2
Items: 
Size: 708608 Color: 1
Size: 279130 Color: 3

Bin 823: 12471 of cap free
Amount of items: 2
Items: 
Size: 621327 Color: 2
Size: 366203 Color: 0

Bin 824: 12646 of cap free
Amount of items: 2
Items: 
Size: 708387 Color: 3
Size: 278968 Color: 1

Bin 825: 13088 of cap free
Amount of items: 2
Items: 
Size: 520908 Color: 2
Size: 466005 Color: 3

Bin 826: 13931 of cap free
Amount of items: 2
Items: 
Size: 707523 Color: 1
Size: 278547 Color: 2

Bin 827: 14186 of cap free
Amount of items: 7
Items: 
Size: 143413 Color: 4
Size: 141668 Color: 0
Size: 140941 Color: 2
Size: 140755 Color: 3
Size: 139772 Color: 2
Size: 139757 Color: 2
Size: 139509 Color: 2

Bin 828: 14349 of cap free
Amount of items: 2
Items: 
Size: 519793 Color: 0
Size: 465859 Color: 4

Bin 829: 14505 of cap free
Amount of items: 2
Items: 
Size: 706951 Color: 0
Size: 278545 Color: 1

Bin 830: 14698 of cap free
Amount of items: 2
Items: 
Size: 786823 Color: 0
Size: 198480 Color: 4

Bin 831: 15587 of cap free
Amount of items: 2
Items: 
Size: 619261 Color: 3
Size: 365153 Color: 1

Bin 832: 15607 of cap free
Amount of items: 2
Items: 
Size: 519374 Color: 3
Size: 465020 Color: 4

Bin 833: 16011 of cap free
Amount of items: 2
Items: 
Size: 705924 Color: 4
Size: 278066 Color: 1

Bin 834: 16033 of cap free
Amount of items: 2
Items: 
Size: 786401 Color: 3
Size: 197567 Color: 2

Bin 835: 16331 of cap free
Amount of items: 2
Items: 
Size: 786398 Color: 3
Size: 197272 Color: 1

Bin 836: 16366 of cap free
Amount of items: 6
Items: 
Size: 165212 Color: 0
Size: 164316 Color: 4
Size: 163776 Color: 1
Size: 163720 Color: 4
Size: 163455 Color: 4
Size: 163156 Color: 2

Bin 837: 17102 of cap free
Amount of items: 2
Items: 
Size: 556297 Color: 1
Size: 426602 Color: 3

Bin 838: 18012 of cap free
Amount of items: 2
Items: 
Size: 705123 Color: 4
Size: 276866 Color: 1

Bin 839: 18628 of cap free
Amount of items: 2
Items: 
Size: 764907 Color: 4
Size: 216466 Color: 0

Bin 840: 18952 of cap free
Amount of items: 2
Items: 
Size: 519101 Color: 0
Size: 461948 Color: 1

Bin 841: 19605 of cap free
Amount of items: 2
Items: 
Size: 704893 Color: 1
Size: 275503 Color: 4

Bin 842: 20326 of cap free
Amount of items: 2
Items: 
Size: 763315 Color: 4
Size: 216360 Color: 0

Bin 843: 20379 of cap free
Amount of items: 2
Items: 
Size: 704193 Color: 4
Size: 275429 Color: 1

Bin 844: 20459 of cap free
Amount of items: 2
Items: 
Size: 594280 Color: 2
Size: 385262 Color: 0

Bin 845: 20711 of cap free
Amount of items: 8
Items: 
Size: 125265 Color: 4
Size: 123238 Color: 3
Size: 123011 Color: 1
Size: 122314 Color: 1
Size: 122215 Color: 2
Size: 121944 Color: 1
Size: 120689 Color: 0
Size: 120614 Color: 2

Bin 846: 22338 of cap free
Amount of items: 2
Items: 
Size: 516145 Color: 4
Size: 461518 Color: 1

Bin 847: 22672 of cap free
Amount of items: 2
Items: 
Size: 592837 Color: 2
Size: 384492 Color: 4

Bin 848: 24895 of cap free
Amount of items: 6
Items: 
Size: 165041 Color: 0
Size: 162878 Color: 2
Size: 162361 Color: 2
Size: 161961 Color: 4
Size: 161588 Color: 1
Size: 161277 Color: 2

Bin 849: 28209 of cap free
Amount of items: 3
Items: 
Size: 326069 Color: 2
Size: 324900 Color: 3
Size: 320823 Color: 2

Bin 850: 30674 of cap free
Amount of items: 7
Items: 
Size: 142238 Color: 4
Size: 138259 Color: 3
Size: 138078 Color: 0
Size: 138007 Color: 2
Size: 137952 Color: 1
Size: 137475 Color: 4
Size: 137318 Color: 2

Bin 851: 32859 of cap free
Amount of items: 2
Items: 
Size: 518818 Color: 1
Size: 448324 Color: 0

Bin 852: 33234 of cap free
Amount of items: 9
Items: 
Size: 108417 Color: 4
Size: 108093 Color: 0
Size: 107886 Color: 0
Size: 107643 Color: 1
Size: 107417 Color: 2
Size: 107241 Color: 0
Size: 106829 Color: 0
Size: 106649 Color: 2
Size: 106592 Color: 3

Bin 853: 35067 of cap free
Amount of items: 2
Items: 
Size: 518788 Color: 1
Size: 446146 Color: 2

Bin 854: 35336 of cap free
Amount of items: 2
Items: 
Size: 518734 Color: 1
Size: 445931 Color: 4

Bin 855: 35475 of cap free
Amount of items: 6
Items: 
Size: 161211 Color: 2
Size: 161075 Color: 0
Size: 160979 Color: 2
Size: 160661 Color: 3
Size: 160547 Color: 3
Size: 160053 Color: 2

Bin 856: 36003 of cap free
Amount of items: 3
Items: 
Size: 325146 Color: 2
Size: 320787 Color: 4
Size: 318065 Color: 1

Bin 857: 38956 of cap free
Amount of items: 2
Items: 
Size: 515654 Color: 3
Size: 445391 Color: 1

Bin 858: 39156 of cap free
Amount of items: 2
Items: 
Size: 515612 Color: 4
Size: 445233 Color: 1

Bin 859: 40657 of cap free
Amount of items: 2
Items: 
Size: 762120 Color: 4
Size: 197224 Color: 2

Bin 860: 42181 of cap free
Amount of items: 2
Items: 
Size: 514065 Color: 3
Size: 443755 Color: 2

Bin 861: 42513 of cap free
Amount of items: 2
Items: 
Size: 760806 Color: 4
Size: 196682 Color: 0

Bin 862: 43805 of cap free
Amount of items: 7
Items: 
Size: 139359 Color: 4
Size: 136742 Color: 3
Size: 136517 Color: 0
Size: 136242 Color: 4
Size: 136104 Color: 2
Size: 135735 Color: 0
Size: 135497 Color: 2

Bin 863: 45102 of cap free
Amount of items: 2
Items: 
Size: 759558 Color: 4
Size: 195341 Color: 1

Bin 864: 46826 of cap free
Amount of items: 6
Items: 
Size: 159688 Color: 3
Size: 159670 Color: 1
Size: 158999 Color: 3
Size: 158573 Color: 0
Size: 158476 Color: 2
Size: 157769 Color: 0

Bin 865: 46953 of cap free
Amount of items: 8
Items: 
Size: 119902 Color: 0
Size: 119893 Color: 4
Size: 119452 Color: 1
Size: 119409 Color: 3
Size: 119282 Color: 4
Size: 118919 Color: 2
Size: 118245 Color: 4
Size: 117946 Color: 2

Bin 866: 47839 of cap free
Amount of items: 2
Items: 
Size: 758662 Color: 3
Size: 193500 Color: 1

Bin 867: 49123 of cap free
Amount of items: 2
Items: 
Size: 507127 Color: 1
Size: 443751 Color: 0

Bin 868: 49708 of cap free
Amount of items: 2
Items: 
Size: 507121 Color: 0
Size: 443172 Color: 1

Bin 869: 53695 of cap free
Amount of items: 2
Items: 
Size: 753001 Color: 3
Size: 193305 Color: 4

Bin 870: 54013 of cap free
Amount of items: 9
Items: 
Size: 106749 Color: 0
Size: 106267 Color: 1
Size: 106185 Color: 0
Size: 104930 Color: 2
Size: 104887 Color: 1
Size: 104866 Color: 1
Size: 104557 Color: 1
Size: 103925 Color: 3
Size: 103622 Color: 2

Bin 871: 55621 of cap free
Amount of items: 2
Items: 
Size: 751808 Color: 3
Size: 192572 Color: 2

Bin 872: 60732 of cap free
Amount of items: 7
Items: 
Size: 139070 Color: 4
Size: 134224 Color: 0
Size: 133975 Color: 2
Size: 133361 Color: 1
Size: 133003 Color: 3
Size: 132982 Color: 3
Size: 132654 Color: 0

Bin 873: 66766 of cap free
Amount of items: 8
Items: 
Size: 117526 Color: 2
Size: 117119 Color: 0
Size: 116940 Color: 1
Size: 116748 Color: 2
Size: 116534 Color: 3
Size: 116368 Color: 2
Size: 116066 Color: 1
Size: 115934 Color: 2

Bin 874: 67984 of cap free
Amount of items: 6
Items: 
Size: 157639 Color: 2
Size: 155977 Color: 1
Size: 155123 Color: 4
Size: 154684 Color: 1
Size: 154443 Color: 0
Size: 154151 Color: 0

Bin 875: 68237 of cap free
Amount of items: 5
Items: 
Size: 191150 Color: 0
Size: 186248 Color: 2
Size: 184935 Color: 1
Size: 184755 Color: 1
Size: 184676 Color: 0

Bin 876: 69181 of cap free
Amount of items: 3
Items: 
Size: 310322 Color: 0
Size: 310313 Color: 2
Size: 310185 Color: 2

Bin 877: 73917 of cap free
Amount of items: 5
Items: 
Size: 188730 Color: 0
Size: 184579 Color: 3
Size: 184352 Color: 2
Size: 184291 Color: 2
Size: 184132 Color: 4

Bin 878: 78110 of cap free
Amount of items: 7
Items: 
Size: 137239 Color: 4
Size: 132608 Color: 2
Size: 131907 Color: 1
Size: 130854 Color: 3
Size: 130432 Color: 0
Size: 129726 Color: 0
Size: 129125 Color: 2

Bin 879: 78475 of cap free
Amount of items: 2
Items: 
Size: 506845 Color: 0
Size: 414681 Color: 2

Bin 880: 78732 of cap free
Amount of items: 4
Items: 
Size: 295962 Color: 0
Size: 241301 Color: 3
Size: 192518 Color: 0
Size: 191488 Color: 2

Bin 881: 80327 of cap free
Amount of items: 3
Items: 
Size: 309913 Color: 0
Size: 304989 Color: 4
Size: 304772 Color: 2

Bin 882: 80732 of cap free
Amount of items: 5
Items: 
Size: 186339 Color: 0
Size: 183998 Color: 4
Size: 183962 Color: 1
Size: 183938 Color: 3
Size: 181032 Color: 3

Bin 883: 81525 of cap free
Amount of items: 9
Items: 
Size: 103591 Color: 3
Size: 102957 Color: 2
Size: 102839 Color: 1
Size: 102828 Color: 0
Size: 102203 Color: 0
Size: 101381 Color: 1
Size: 101079 Color: 2
Size: 100831 Color: 1
Size: 100767 Color: 3

Bin 884: 82579 of cap free
Amount of items: 2
Items: 
Size: 506708 Color: 4
Size: 410714 Color: 1

Bin 885: 85854 of cap free
Amount of items: 8
Items: 
Size: 115580 Color: 4
Size: 115403 Color: 1
Size: 114979 Color: 0
Size: 114952 Color: 1
Size: 114152 Color: 1
Size: 113137 Color: 2
Size: 113045 Color: 3
Size: 112899 Color: 2

Bin 886: 91632 of cap free
Amount of items: 3
Items: 
Size: 308306 Color: 0
Size: 303689 Color: 1
Size: 296374 Color: 1

Bin 887: 92235 of cap free
Amount of items: 5
Items: 
Size: 184650 Color: 0
Size: 180962 Color: 2
Size: 180835 Color: 1
Size: 180786 Color: 2
Size: 180533 Color: 1

Bin 888: 92356 of cap free
Amount of items: 6
Items: 
Size: 153976 Color: 3
Size: 151052 Color: 1
Size: 151005 Color: 3
Size: 150817 Color: 4
Size: 150792 Color: 1
Size: 150003 Color: 0

Bin 889: 98407 of cap free
Amount of items: 7
Items: 
Size: 137012 Color: 4
Size: 128963 Color: 2
Size: 127887 Color: 0
Size: 127411 Color: 2
Size: 126997 Color: 0
Size: 126946 Color: 0
Size: 126378 Color: 2

Bin 890: 799381 of cap free
Amount of items: 2
Items: 
Size: 100473 Color: 2
Size: 100147 Color: 4

Bin 891: 899570 of cap free
Amount of items: 1
Items: 
Size: 100431 Color: 2

Total size: 885451266
Total free space: 5549625

