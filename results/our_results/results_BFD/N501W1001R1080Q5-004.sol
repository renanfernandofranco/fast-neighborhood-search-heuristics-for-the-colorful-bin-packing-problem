Capicity Bin: 1001
Lower Bound: 228

Bins used: 230
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 436 Color: 0
Size: 129 Color: 4

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 499 Color: 0

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 498 Color: 1

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 3

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 494 Color: 3

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 2
Size: 492 Color: 3

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 491 Color: 0

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 486 Color: 3

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 483 Color: 2

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 3

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 2

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 4
Size: 458 Color: 3

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 0
Size: 451 Color: 1

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 449 Color: 3

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 0
Size: 442 Color: 3

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 2
Size: 438 Color: 0

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 434 Color: 1

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 431 Color: 1

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 0
Size: 422 Color: 3

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 4

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 417 Color: 0

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 597 Color: 3
Size: 143 Color: 0
Size: 142 Color: 4
Size: 119 Color: 4

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 4
Size: 399 Color: 2

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 392 Color: 2

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 2

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 1
Size: 379 Color: 3

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 378 Color: 1

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 371 Color: 3

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 2

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 1
Size: 368 Color: 3

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 0
Size: 367 Color: 2

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 1
Size: 365 Color: 4

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 357 Color: 2

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 2
Size: 354 Color: 0

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 3
Size: 353 Color: 4

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 2
Size: 352 Color: 3

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 4
Size: 352 Color: 2

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 4
Size: 348 Color: 3

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 4
Size: 344 Color: 2

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 341 Color: 4

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 673 Color: 4
Size: 174 Color: 0
Size: 154 Color: 3

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 1
Size: 326 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 0
Size: 186 Color: 1
Size: 123 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 2
Size: 175 Color: 4
Size: 134 Color: 3

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 1
Size: 307 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 2
Size: 192 Color: 1
Size: 113 Color: 1

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 2

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 1

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 302 Color: 4

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 302 Color: 3

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 1
Size: 182 Color: 3
Size: 115 Color: 1

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 1
Size: 185 Color: 3
Size: 103 Color: 2

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 2

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 282 Color: 4

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 0
Size: 273 Color: 4

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 1

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 267 Color: 3

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 3
Size: 257 Color: 1

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 3
Size: 253 Color: 4

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 252 Color: 4

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 245 Color: 1

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 243 Color: 0

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 4
Size: 236 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 230 Color: 4

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 230 Color: 2

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 2
Size: 225 Color: 3

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 2
Size: 223 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 2
Size: 216 Color: 4

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 0

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 0

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 0

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 1
Size: 484 Color: 2

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 480 Color: 3

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 474 Color: 4

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 462 Color: 0

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 0
Size: 450 Color: 3

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 439 Color: 4

Bin 82: 1 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 424 Color: 0

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 416 Color: 4

Bin 84: 1 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 408 Color: 0

Bin 85: 1 of cap free
Amount of items: 4
Items: 
Size: 596 Color: 1
Size: 142 Color: 3
Size: 142 Color: 3
Size: 120 Color: 1

Bin 86: 1 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 394 Color: 0

Bin 87: 1 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 1
Size: 383 Color: 3

Bin 88: 1 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 366 Color: 4

Bin 89: 1 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 1
Size: 363 Color: 3

Bin 90: 1 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 363 Color: 1

Bin 91: 1 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 1
Size: 360 Color: 3

Bin 92: 1 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 348 Color: 1

Bin 93: 1 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 2
Size: 342 Color: 4

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 1
Size: 171 Color: 3
Size: 166 Color: 1

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 0
Size: 172 Color: 3
Size: 164 Color: 2

Bin 96: 1 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 335 Color: 1

Bin 97: 1 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 331 Color: 1

Bin 98: 1 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 329 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 4
Size: 175 Color: 2
Size: 151 Color: 4

Bin 100: 1 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 320 Color: 1

Bin 101: 1 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 312 Color: 4

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 0
Size: 189 Color: 1
Size: 116 Color: 1

Bin 103: 1 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 299 Color: 3

Bin 104: 1 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 0
Size: 291 Color: 3

Bin 105: 1 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 286 Color: 0

Bin 106: 1 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 2
Size: 275 Color: 0

Bin 107: 1 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 3
Size: 269 Color: 0

Bin 108: 1 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 0
Size: 267 Color: 3

Bin 109: 1 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 4
Size: 265 Color: 1

Bin 110: 1 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 263 Color: 2

Bin 111: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 199 Color: 4

Bin 112: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 1
Size: 199 Color: 2

Bin 113: 2 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 1
Size: 499 Color: 3

Bin 114: 2 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 496 Color: 3

Bin 115: 2 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 4
Size: 496 Color: 1

Bin 116: 2 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 2
Size: 492 Color: 0

Bin 117: 2 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 4
Size: 454 Color: 3

Bin 118: 2 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 4
Size: 445 Color: 2

Bin 119: 2 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 0
Size: 407 Color: 3

Bin 120: 2 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 2
Size: 397 Color: 4

Bin 121: 2 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 3
Size: 368 Color: 1

Bin 122: 2 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 0
Size: 353 Color: 1

Bin 123: 2 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 0
Size: 324 Color: 3

Bin 124: 2 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 315 Color: 0

Bin 125: 2 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 299 Color: 2

Bin 126: 2 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 294 Color: 0

Bin 127: 2 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 291 Color: 0

Bin 128: 2 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 4
Size: 251 Color: 3

Bin 129: 2 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 238 Color: 0

Bin 130: 2 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 0
Size: 233 Color: 1

Bin 131: 2 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 0
Size: 222 Color: 4

Bin 132: 2 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 0
Size: 219 Color: 2

Bin 133: 3 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 4
Size: 492 Color: 0

Bin 134: 3 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 4
Size: 482 Color: 3

Bin 135: 3 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 468 Color: 1

Bin 136: 3 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 3
Size: 411 Color: 0

Bin 137: 3 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 400 Color: 4

Bin 138: 3 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 389 Color: 4

Bin 139: 3 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 383 Color: 0

Bin 140: 3 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 0
Size: 382 Color: 1

Bin 141: 3 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 1
Size: 173 Color: 0
Size: 159 Color: 4

Bin 142: 3 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 324 Color: 1

Bin 143: 3 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 3
Size: 319 Color: 1

Bin 144: 3 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 318 Color: 2

Bin 145: 3 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 0
Size: 188 Color: 1
Size: 118 Color: 4

Bin 146: 3 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 256 Color: 1

Bin 147: 3 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 4
Size: 247 Color: 2

Bin 148: 3 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 197 Color: 2

Bin 149: 4 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 486 Color: 2

Bin 150: 4 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 468 Color: 2

Bin 151: 4 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 462 Color: 0

Bin 152: 4 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 415 Color: 1

Bin 153: 4 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 2
Size: 400 Color: 4

Bin 154: 4 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 1
Size: 323 Color: 4

Bin 155: 4 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 2
Size: 277 Color: 1

Bin 156: 4 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 203 Color: 3

Bin 157: 4 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 0
Size: 198 Color: 3

Bin 158: 4 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 0
Size: 198 Color: 3

Bin 159: 5 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 467 Color: 4

Bin 160: 5 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 443 Color: 3

Bin 161: 5 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 406 Color: 1

Bin 162: 5 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 382 Color: 0

Bin 163: 5 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 2
Size: 382 Color: 1

Bin 164: 5 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 2
Size: 255 Color: 4

Bin 165: 5 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 0
Size: 197 Color: 1

Bin 166: 6 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 3
Size: 460 Color: 4

Bin 167: 6 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 4
Size: 397 Color: 3

Bin 168: 6 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 4
Size: 397 Color: 0

Bin 169: 6 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 1
Size: 229 Color: 4

Bin 170: 6 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 214 Color: 4

Bin 171: 7 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 452 Color: 2

Bin 172: 7 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 289 Color: 3

Bin 173: 7 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 0
Size: 255 Color: 1

Bin 174: 8 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 351 Color: 3

Bin 175: 8 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 314 Color: 2

Bin 176: 8 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 4
Size: 229 Color: 3

Bin 177: 9 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 2
Size: 411 Color: 0

Bin 178: 10 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 251 Color: 0

Bin 179: 11 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 2
Size: 170 Color: 1
Size: 157 Color: 2

Bin 180: 11 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 181 Color: 2
Size: 111 Color: 1

Bin 181: 12 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 460 Color: 1

Bin 182: 13 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 436 Color: 3
Size: 111 Color: 0

Bin 183: 13 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 4
Size: 375 Color: 2

Bin 184: 13 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 2
Size: 211 Color: 1

Bin 185: 14 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 4
Size: 407 Color: 0

Bin 186: 14 of cap free
Amount of items: 3
Items: 
Size: 662 Color: 1
Size: 169 Color: 0
Size: 156 Color: 0

Bin 187: 14 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 4
Size: 227 Color: 3

Bin 188: 15 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 347 Color: 3

Bin 189: 15 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 312 Color: 1

Bin 190: 15 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 1
Size: 250 Color: 4

Bin 191: 18 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 4
Size: 246 Color: 2

Bin 192: 18 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 226 Color: 3

Bin 193: 19 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 345 Color: 3

Bin 194: 19 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 345 Color: 3

Bin 195: 20 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 4
Size: 407 Color: 0

Bin 196: 20 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 1
Size: 311 Color: 4

Bin 197: 21 of cap free
Amount of items: 4
Items: 
Size: 589 Color: 0
Size: 141 Color: 1
Size: 140 Color: 4
Size: 110 Color: 3

Bin 198: 22 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 1
Size: 479 Color: 2

Bin 199: 22 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 4
Size: 246 Color: 1

Bin 200: 24 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 0
Size: 310 Color: 4

Bin 201: 24 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 176 Color: 0
Size: 109 Color: 4

Bin 202: 25 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 176 Color: 0
Size: 108 Color: 3

Bin 203: 25 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 246 Color: 4

Bin 204: 28 of cap free
Amount of items: 4
Items: 
Size: 589 Color: 0
Size: 139 Color: 4
Size: 138 Color: 1
Size: 107 Color: 3

Bin 205: 28 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 344 Color: 1

Bin 206: 28 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 344 Color: 0

Bin 207: 30 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 443 Color: 2

Bin 208: 31 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 342 Color: 3

Bin 209: 33 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 2
Size: 341 Color: 3

Bin 210: 34 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 443 Color: 0

Bin 211: 35 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 442 Color: 2

Bin 212: 35 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 442 Color: 3

Bin 213: 40 of cap free
Amount of items: 6
Items: 
Size: 433 Color: 2
Size: 107 Color: 0
Size: 106 Color: 3
Size: 105 Color: 1
Size: 105 Color: 0
Size: 105 Color: 0

Bin 214: 42 of cap free
Amount of items: 4
Items: 
Size: 581 Color: 0
Size: 136 Color: 3
Size: 136 Color: 0
Size: 106 Color: 4

Bin 215: 47 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 2
Size: 341 Color: 1

Bin 216: 48 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 2
Size: 442 Color: 0

Bin 217: 60 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 1
Size: 441 Color: 4

Bin 218: 65 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 4
Size: 206 Color: 1

Bin 219: 68 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 204 Color: 1

Bin 220: 80 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 196 Color: 3

Bin 221: 81 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 4
Size: 155 Color: 3
Size: 153 Color: 2

Bin 222: 83 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 1
Size: 194 Color: 4

Bin 223: 86 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 4
Size: 195 Color: 1

Bin 224: 88 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 4
Size: 153 Color: 1
Size: 149 Color: 3

Bin 225: 89 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 194 Color: 1

Bin 226: 89 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 194 Color: 3

Bin 227: 94 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 192 Color: 2

Bin 228: 97 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 0
Size: 148 Color: 4
Size: 148 Color: 0

Bin 229: 99 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 3
Size: 147 Color: 2
Size: 147 Color: 0

Bin 230: 384 of cap free
Amount of items: 6
Items: 
Size: 105 Color: 0
Size: 103 Color: 2
Size: 103 Color: 0
Size: 102 Color: 4
Size: 102 Color: 3
Size: 102 Color: 1

Total size: 227577
Total free space: 2653

