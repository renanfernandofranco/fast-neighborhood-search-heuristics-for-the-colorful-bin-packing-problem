Capicity Bin: 1001
Lower Bound: 914

Bins used: 918
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 128 Color: 0
Size: 126 Color: 1
Size: 125 Color: 4
Size: 125 Color: 4
Size: 125 Color: 3
Size: 125 Color: 3
Size: 125 Color: 3
Size: 122 Color: 2

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 132 Color: 0
Size: 128 Color: 3
Size: 128 Color: 3
Size: 128 Color: 1
Size: 128 Color: 0
Size: 128 Color: 0
Size: 128 Color: 0
Size: 101 Color: 4

Bin 3: 0 of cap free
Amount of items: 7
Items: 
Size: 145 Color: 1
Size: 144 Color: 3
Size: 144 Color: 1
Size: 144 Color: 0
Size: 143 Color: 4
Size: 142 Color: 2
Size: 139 Color: 3

Bin 4: 0 of cap free
Amount of items: 7
Items: 
Size: 148 Color: 4
Size: 147 Color: 2
Size: 147 Color: 2
Size: 147 Color: 0
Size: 146 Color: 1
Size: 146 Color: 1
Size: 120 Color: 4

Bin 5: 0 of cap free
Amount of items: 7
Items: 
Size: 150 Color: 1
Size: 150 Color: 0
Size: 149 Color: 4
Size: 149 Color: 3
Size: 149 Color: 2
Size: 149 Color: 2
Size: 105 Color: 2

Bin 6: 0 of cap free
Amount of items: 6
Items: 
Size: 172 Color: 3
Size: 172 Color: 2
Size: 172 Color: 1
Size: 171 Color: 3
Size: 170 Color: 3
Size: 144 Color: 2

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 499 Color: 4
Size: 173 Color: 3
Size: 173 Color: 1
Size: 156 Color: 2

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 4

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 2
Size: 500 Color: 3

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 4
Size: 500 Color: 2

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 503 Color: 1
Size: 174 Color: 4
Size: 174 Color: 0
Size: 150 Color: 4

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 497 Color: 4

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 497 Color: 1

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 497 Color: 1

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 497 Color: 3

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 4

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 3

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 2

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 505 Color: 1
Size: 176 Color: 0
Size: 175 Color: 3
Size: 145 Color: 4

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 505 Color: 1
Size: 174 Color: 4
Size: 174 Color: 4
Size: 148 Color: 0

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 4

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 3

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 3

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 2
Size: 494 Color: 4

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 4
Size: 494 Color: 3

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 493 Color: 4

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 1
Size: 492 Color: 3

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 1
Size: 492 Color: 3

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 2

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 491 Color: 3

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 0
Size: 490 Color: 1

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 490 Color: 0

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 3

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 489 Color: 2

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 489 Color: 0

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 3

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 3
Size: 486 Color: 1

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 4

Bin 39: 0 of cap free
Amount of items: 4
Items: 
Size: 516 Color: 1
Size: 178 Color: 4
Size: 178 Color: 0
Size: 129 Color: 4

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 2
Size: 485 Color: 1

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 0
Size: 484 Color: 3

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 483 Color: 4

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 482 Color: 3

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 2
Size: 482 Color: 1

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 4
Size: 482 Color: 1

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 3

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 2

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 1
Size: 480 Color: 3

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 480 Color: 3

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 480 Color: 1

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 2

Bin 52: 0 of cap free
Amount of items: 4
Items: 
Size: 521 Color: 4
Size: 177 Color: 0
Size: 176 Color: 4
Size: 127 Color: 3

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 0
Size: 479 Color: 4

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 0
Size: 479 Color: 3

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 0

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 0

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 0
Size: 478 Color: 4

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 3

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 2
Size: 478 Color: 3

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 2

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 4
Size: 478 Color: 1

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 477 Color: 4

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 0

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 476 Color: 2

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 475 Color: 2

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 474 Color: 1

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 2
Size: 473 Color: 0

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 472 Color: 4

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 2

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 4
Size: 472 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 471 Color: 0

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 470 Color: 4

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 470 Color: 3

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 1

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 3
Size: 470 Color: 2

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 469 Color: 4

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 469 Color: 4

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 469 Color: 2

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 3
Size: 469 Color: 0

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 468 Color: 4

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 2

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 4
Size: 466 Color: 1

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 2

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 0

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 464 Color: 4

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 0
Size: 463 Color: 4

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 0
Size: 463 Color: 1

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 462 Color: 3

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 0
Size: 460 Color: 3

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 1
Size: 460 Color: 3

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 459 Color: 3

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 4

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 0
Size: 457 Color: 4

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 2

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 3
Size: 456 Color: 4

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 3
Size: 456 Color: 1

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 4
Size: 456 Color: 0

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 455 Color: 4

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 455 Color: 0

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 454 Color: 3

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 4
Size: 454 Color: 3

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 1
Size: 453 Color: 4

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 0
Size: 452 Color: 4

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 1
Size: 452 Color: 3

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 451 Color: 2

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 2

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 1

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 0

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 4

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 450 Color: 4

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 3
Size: 449 Color: 2

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 4

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 0
Size: 446 Color: 4

Bin 115: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 1
Size: 446 Color: 0

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 3
Size: 445 Color: 2

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 3
Size: 444 Color: 4

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 3
Size: 444 Color: 2

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 3

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 2
Size: 443 Color: 0

Bin 121: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 0
Size: 442 Color: 2

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 0
Size: 441 Color: 4

Bin 123: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 0
Size: 441 Color: 3

Bin 124: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 441 Color: 3

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 3
Size: 439 Color: 2

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 4
Size: 439 Color: 3

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 1
Size: 438 Color: 0

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 4

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 4
Size: 437 Color: 3

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 4

Bin 131: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 3

Bin 132: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 1

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 2

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 4

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 0

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 1
Size: 432 Color: 3

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 431 Color: 0

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 0
Size: 430 Color: 1

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 429 Color: 2

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 0
Size: 428 Color: 1

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 0
Size: 426 Color: 4

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 4
Size: 426 Color: 3

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 425 Color: 3

Bin 144: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 424 Color: 4

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 424 Color: 3

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 2

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 2

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 423 Color: 3

Bin 149: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 0
Size: 422 Color: 3

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 0
Size: 422 Color: 1

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 3
Size: 420 Color: 4

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 1

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 419 Color: 0

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 419 Color: 1

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 418 Color: 4

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 418 Color: 3

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 1

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 3
Size: 418 Color: 1

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 417 Color: 2

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 417 Color: 1

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 417 Color: 0

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 416 Color: 3

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 416 Color: 2

Bin 164: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 0
Size: 415 Color: 4

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 3

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 3

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 4

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 0
Size: 412 Color: 3

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 4

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 0

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 410 Color: 4

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 1
Size: 410 Color: 2

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 1
Size: 410 Color: 0

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 409 Color: 4

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 3
Size: 409 Color: 4

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 409 Color: 1

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 3
Size: 408 Color: 4

Bin 178: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 0
Size: 407 Color: 2

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 0
Size: 406 Color: 4

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 406 Color: 3

Bin 181: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 2

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 4
Size: 405 Color: 1

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 1
Size: 404 Color: 0

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 402 Color: 4

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 1
Size: 401 Color: 4

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 0

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 4

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 4

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 2
Size: 399 Color: 0

Bin 190: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 4

Bin 191: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 2

Bin 192: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 3
Size: 398 Color: 0

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 398 Color: 0

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 1
Size: 396 Color: 3

Bin 195: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 2
Size: 396 Color: 1

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 395 Color: 4

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 2

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 1

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 0
Size: 394 Color: 1

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 0

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 393 Color: 3

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 393 Color: 2

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 393 Color: 1

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 1

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 4
Size: 393 Color: 1

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 4

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 4
Size: 392 Color: 3

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 0
Size: 391 Color: 4

Bin 209: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 390 Color: 1

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 3

Bin 211: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 4
Size: 389 Color: 2

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 388 Color: 4

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 4
Size: 388 Color: 3

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 387 Color: 4

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 387 Color: 1

Bin 216: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 387 Color: 1

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 0
Size: 386 Color: 4

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 385 Color: 1

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 3
Size: 385 Color: 2

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 384 Color: 3

Bin 221: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 2
Size: 384 Color: 3

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 2

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 4
Size: 383 Color: 3

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 4
Size: 383 Color: 3

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 382 Color: 0

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 1

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 1
Size: 381 Color: 3

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 4
Size: 381 Color: 1

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 4
Size: 381 Color: 0

Bin 230: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 2
Size: 379 Color: 4

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 379 Color: 0

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 1
Size: 378 Color: 3

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 2
Size: 378 Color: 3

Bin 234: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 4
Size: 378 Color: 1

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 4

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 1

Bin 237: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 3
Size: 377 Color: 2

Bin 238: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 3
Size: 377 Color: 2

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 0
Size: 376 Color: 4

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 376 Color: 2

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 3

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 0

Bin 243: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 374 Color: 2

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 2
Size: 374 Color: 0

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 373 Color: 3

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 3
Size: 373 Color: 1

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 0
Size: 372 Color: 4

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 1
Size: 372 Color: 4

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 372 Color: 0

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 371 Color: 4

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 2
Size: 370 Color: 0

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 369 Color: 4

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 0
Size: 368 Color: 2

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 367 Color: 4

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 367 Color: 3

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 367 Color: 0

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 4
Size: 367 Color: 2

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 366 Color: 4

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 366 Color: 4

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 366 Color: 3

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 3
Size: 365 Color: 4

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 0
Size: 364 Color: 2

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 364 Color: 1

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 2

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 0

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 1
Size: 363 Color: 3

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 362 Color: 4

Bin 268: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 362 Color: 4

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 3

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 3
Size: 361 Color: 1

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 4

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 0
Size: 359 Color: 4

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 2
Size: 359 Color: 4

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 0
Size: 358 Color: 4

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 2
Size: 358 Color: 4

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 357 Color: 0

Bin 277: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 357 Color: 0

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 4

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 1

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 355 Color: 4

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 3
Size: 355 Color: 2

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 354 Color: 4

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 4
Size: 354 Color: 1

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 4
Size: 354 Color: 1

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 353 Color: 4

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 3

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 2
Size: 353 Color: 0

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 352 Color: 2

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 0

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 0

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 0

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 351 Color: 4

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 350 Color: 4

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 350 Color: 4

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 349 Color: 4

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 2
Size: 348 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 3
Size: 188 Color: 2
Size: 160 Color: 4

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 347 Color: 2

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 0
Size: 346 Color: 3

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 1
Size: 346 Color: 2

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 0

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 4
Size: 346 Color: 0

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 0
Size: 345 Color: 4

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 3
Size: 345 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 3
Size: 189 Color: 0
Size: 156 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 3
Size: 188 Color: 4
Size: 157 Color: 4

Bin 307: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 2
Size: 344 Color: 3

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 0
Size: 343 Color: 2

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 4
Size: 343 Color: 0

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 0
Size: 342 Color: 3

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 342 Color: 0

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 3
Size: 342 Color: 1

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 4

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 3

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 2
Size: 341 Color: 1

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 340 Color: 3

Bin 317: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 1

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 340 Color: 2

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 338 Color: 0

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 337 Color: 0

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 336 Color: 4

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 4
Size: 336 Color: 1

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 335 Color: 4

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 2
Size: 335 Color: 0

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 3
Size: 335 Color: 2

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 3
Size: 334 Color: 4

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 3
Size: 334 Color: 4

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 4
Size: 334 Color: 2

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 333 Color: 4

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 4

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 2

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 2

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 332 Color: 2

Bin 334: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 0
Size: 331 Color: 4

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 0
Size: 331 Color: 3

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 1
Size: 331 Color: 3

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 2
Size: 331 Color: 3

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 3
Size: 331 Color: 1

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 3

Bin 340: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 2

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 2

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 1

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 0
Size: 329 Color: 4

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 2

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 328 Color: 0

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 4
Size: 328 Color: 1

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 3

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 1
Size: 327 Color: 3

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 1
Size: 327 Color: 0

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 1

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 1
Size: 326 Color: 2

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 3
Size: 326 Color: 1

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 325 Color: 3

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 3
Size: 324 Color: 4

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 1
Size: 323 Color: 4

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 0
Size: 322 Color: 4

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 322 Color: 0

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 4

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 3
Size: 321 Color: 2

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 1
Size: 320 Color: 0

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 2

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 0
Size: 191 Color: 4
Size: 127 Color: 4

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 1
Size: 191 Color: 4
Size: 127 Color: 4

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 2
Size: 191 Color: 3
Size: 127 Color: 0

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 3

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 317 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 0
Size: 193 Color: 1
Size: 123 Color: 2

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 1
Size: 316 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 1
Size: 192 Color: 0
Size: 124 Color: 4

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 2
Size: 192 Color: 0
Size: 124 Color: 3

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 1
Size: 315 Color: 3

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 1
Size: 315 Color: 0

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 1
Size: 315 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 3
Size: 193 Color: 1
Size: 122 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 1
Size: 193 Color: 0
Size: 121 Color: 3

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 4
Size: 314 Color: 1

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 4

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 1
Size: 193 Color: 4
Size: 120 Color: 4

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 1

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 3

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 3
Size: 311 Color: 2

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 0
Size: 310 Color: 4

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 0
Size: 310 Color: 4

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 1
Size: 310 Color: 2

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 0

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 3

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 2

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 195 Color: 4
Size: 114 Color: 4

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 194 Color: 0
Size: 115 Color: 3

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 4
Size: 195 Color: 1
Size: 114 Color: 2

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 4
Size: 194 Color: 1
Size: 115 Color: 1

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 4
Size: 194 Color: 0
Size: 115 Color: 1

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 308 Color: 3

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 308 Color: 1

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 4
Size: 196 Color: 0
Size: 112 Color: 4

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 1
Size: 196 Color: 3
Size: 111 Color: 0

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 307 Color: 1

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 4

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 3

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 1
Size: 306 Color: 2

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 306 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 4
Size: 198 Color: 0
Size: 108 Color: 1

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 4
Size: 197 Color: 1
Size: 109 Color: 3

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 305 Color: 2

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 1
Size: 198 Color: 4
Size: 107 Color: 4

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 2
Size: 305 Color: 1

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 304 Color: 4

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 304 Color: 2

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 4
Size: 199 Color: 2
Size: 105 Color: 2

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 4
Size: 199 Color: 1
Size: 105 Color: 1

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 2
Size: 303 Color: 4

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 303 Color: 2

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 301 Color: 1

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 2
Size: 301 Color: 0

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 4

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 2

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 2

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 1

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 3

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 1

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 3

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 3
Size: 298 Color: 1

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 4

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 2
Size: 297 Color: 3

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 3
Size: 297 Color: 0

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 296 Color: 2

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 2
Size: 296 Color: 1

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 295 Color: 4

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 2
Size: 295 Color: 1

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 0
Size: 294 Color: 2

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 1
Size: 294 Color: 0

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 294 Color: 1

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 1

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 1
Size: 293 Color: 0

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 2
Size: 293 Color: 1

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 2
Size: 292 Color: 4

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 291 Color: 4

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 291 Color: 3

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 291 Color: 3

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 4

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 3

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 289 Color: 4

Bin 444: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 1
Size: 289 Color: 3

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 1
Size: 289 Color: 0

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 3

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 0

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 287 Color: 4

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 1

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 4
Size: 287 Color: 3

Bin 451: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 4
Size: 287 Color: 0

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 286 Color: 4

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 286 Color: 4

Bin 454: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 286 Color: 3

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 285 Color: 1

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 283 Color: 4

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 1

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 1
Size: 282 Color: 4

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 282 Color: 4

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 1
Size: 281 Color: 0

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 1
Size: 279 Color: 4

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 279 Color: 1

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 2
Size: 278 Color: 4

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 2
Size: 278 Color: 3

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 3
Size: 278 Color: 0

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 277 Color: 2

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 277 Color: 1

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 1
Size: 276 Color: 2

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 3
Size: 276 Color: 1

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 274 Color: 4

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 3
Size: 274 Color: 1

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 2
Size: 273 Color: 1

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 3
Size: 273 Color: 2

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 4
Size: 273 Color: 0

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 4

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 3

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 3
Size: 272 Color: 1

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 272 Color: 1

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 1

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 271 Color: 0

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 2
Size: 270 Color: 0

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 0

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 0

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 1
Size: 268 Color: 3

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 267 Color: 4

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 1
Size: 267 Color: 2

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 267 Color: 1

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 0

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 266 Color: 1

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 1
Size: 265 Color: 4

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 2
Size: 265 Color: 3

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 2

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 4
Size: 265 Color: 2

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 4

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 2
Size: 264 Color: 0

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 2
Size: 263 Color: 4

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 263 Color: 2

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 3

Bin 499: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 3
Size: 262 Color: 1

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 3
Size: 261 Color: 4

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 4
Size: 261 Color: 2

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 260 Color: 4

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 260 Color: 3

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 259 Color: 4

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 259 Color: 2

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 259 Color: 0

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 258 Color: 2

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 257 Color: 3

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 257 Color: 2

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 257 Color: 2

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 3

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 3

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 2

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 1
Size: 255 Color: 4

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 1

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 1

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 2
Size: 253 Color: 1

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 2
Size: 253 Color: 0

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 3
Size: 253 Color: 2

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 252 Color: 4

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 252 Color: 0

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 2
Size: 252 Color: 0

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 4

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 2

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 251 Color: 0

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 251 Color: 0

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 4
Size: 251 Color: 0

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 1

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 249 Color: 3

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 249 Color: 1

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 249 Color: 0

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 248 Color: 2

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 1

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 0

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 247 Color: 4

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 2
Size: 247 Color: 1

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 4
Size: 247 Color: 0

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 1

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 4

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 245 Color: 1

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 0
Size: 244 Color: 2

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 3

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 4

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 241 Color: 1

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 240 Color: 4

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 4

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 2
Size: 240 Color: 3

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 2
Size: 240 Color: 1

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 2

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 4

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 2

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 2
Size: 238 Color: 3

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 4
Size: 238 Color: 2

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 4

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 4

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 237 Color: 2

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 237 Color: 0

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 1
Size: 236 Color: 4

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 3
Size: 236 Color: 1

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 3
Size: 235 Color: 1

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 234 Color: 0

Bin 562: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 232 Color: 0

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 231 Color: 1

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 230 Color: 3

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 0
Size: 229 Color: 4

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 2
Size: 229 Color: 0

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 2

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 228 Color: 4

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 2
Size: 228 Color: 1

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 228 Color: 2

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 226 Color: 3

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 226 Color: 2

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 4
Size: 226 Color: 1

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 4
Size: 226 Color: 1

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 2
Size: 225 Color: 4

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 4
Size: 225 Color: 3

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 0
Size: 224 Color: 4

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 2

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 4
Size: 223 Color: 1

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 4
Size: 223 Color: 1

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 2

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 2
Size: 222 Color: 0

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 221 Color: 2

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 4

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 220 Color: 0

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 2
Size: 219 Color: 3

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 3
Size: 218 Color: 0

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 1
Size: 217 Color: 3

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 2

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 3

Bin 591: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 1
Size: 216 Color: 0

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 0

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 1
Size: 214 Color: 2

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 1
Size: 214 Color: 0

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 4

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 2
Size: 213 Color: 1

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 3
Size: 213 Color: 0

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 212 Color: 4

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 212 Color: 4

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 3

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 1

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 211 Color: 1

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 2
Size: 211 Color: 1

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 210 Color: 4

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 0
Size: 208 Color: 4

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 0
Size: 208 Color: 4

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 2
Size: 208 Color: 3

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 208 Color: 2

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 1

Bin 610: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 4
Size: 207 Color: 1

Bin 611: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 4

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 4
Size: 206 Color: 3

Bin 613: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 4
Size: 206 Color: 2

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 2

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 2

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 203 Color: 4

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 203 Color: 3

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 203 Color: 0

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 4

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 2
Size: 202 Color: 0

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 0
Size: 201 Color: 4

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 201 Color: 2

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 2
Size: 200 Color: 4

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 4

Bin 625: 1 of cap free
Amount of items: 6
Items: 
Size: 170 Color: 0
Size: 169 Color: 3
Size: 169 Color: 2
Size: 169 Color: 2
Size: 169 Color: 0
Size: 154 Color: 4

Bin 626: 1 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 497 Color: 0

Bin 627: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 4
Size: 492 Color: 0

Bin 628: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 4
Size: 483 Color: 2

Bin 629: 1 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 476 Color: 0

Bin 630: 1 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 0
Size: 470 Color: 2

Bin 631: 1 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 463 Color: 1

Bin 632: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 3
Size: 453 Color: 1

Bin 633: 1 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 451 Color: 0

Bin 634: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 2
Size: 447 Color: 1

Bin 635: 1 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 2
Size: 445 Color: 3

Bin 636: 1 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 2
Size: 445 Color: 1

Bin 637: 1 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 2
Size: 440 Color: 4

Bin 638: 1 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 3
Size: 440 Color: 2

Bin 639: 1 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 436 Color: 2

Bin 640: 1 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 2
Size: 434 Color: 1

Bin 641: 1 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 430 Color: 1

Bin 642: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 427 Color: 0

Bin 643: 1 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 418 Color: 0

Bin 644: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 414 Color: 2

Bin 645: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 405 Color: 1

Bin 646: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 386 Color: 3

Bin 647: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 386 Color: 2

Bin 648: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 386 Color: 1

Bin 649: 1 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 383 Color: 2

Bin 650: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 3
Size: 373 Color: 0

Bin 651: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 0
Size: 367 Color: 2

Bin 652: 1 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 1
Size: 360 Color: 4

Bin 653: 1 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 1
Size: 360 Color: 4

Bin 654: 1 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 2
Size: 358 Color: 4

Bin 655: 1 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 3
Size: 356 Color: 1

Bin 656: 1 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 4
Size: 356 Color: 3

Bin 657: 1 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 354 Color: 4

Bin 658: 1 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 4
Size: 351 Color: 3

Bin 659: 1 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 1
Size: 347 Color: 4

Bin 660: 1 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 3
Size: 190 Color: 4
Size: 154 Color: 2

Bin 661: 1 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 3
Size: 336 Color: 4

Bin 662: 1 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 3
Size: 334 Color: 0

Bin 663: 1 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 330 Color: 1

Bin 664: 1 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 3
Size: 323 Color: 2

Bin 665: 1 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 3
Size: 191 Color: 0
Size: 126 Color: 1

Bin 666: 1 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 0
Size: 311 Color: 3

Bin 667: 1 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 1
Size: 198 Color: 4
Size: 106 Color: 2

Bin 668: 1 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 303 Color: 2

Bin 669: 1 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 4
Size: 295 Color: 2

Bin 670: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 2
Size: 290 Color: 3

Bin 671: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 290 Color: 2

Bin 672: 1 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 4
Size: 286 Color: 3

Bin 673: 1 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 280 Color: 4

Bin 674: 1 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 4
Size: 280 Color: 3

Bin 675: 1 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 4
Size: 277 Color: 0

Bin 676: 1 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 274 Color: 1

Bin 677: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 268 Color: 3

Bin 678: 1 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 238 Color: 0

Bin 679: 1 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 3
Size: 233 Color: 0

Bin 680: 1 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 228 Color: 0

Bin 681: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 1
Size: 225 Color: 3

Bin 682: 1 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 219 Color: 2

Bin 683: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 2
Size: 210 Color: 4

Bin 684: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 2
Size: 210 Color: 4

Bin 685: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 210 Color: 2

Bin 686: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 210 Color: 2

Bin 687: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 210 Color: 2

Bin 688: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 210 Color: 1

Bin 689: 1 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 200 Color: 3

Bin 690: 1 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 200 Color: 2

Bin 691: 2 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 492 Color: 0

Bin 692: 2 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 475 Color: 1

Bin 693: 2 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 469 Color: 0

Bin 694: 2 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 460 Color: 3

Bin 695: 2 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 453 Color: 0

Bin 696: 2 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 4
Size: 447 Color: 2

Bin 697: 2 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 444 Color: 2

Bin 698: 2 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 444 Color: 1

Bin 699: 2 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 0
Size: 440 Color: 3

Bin 700: 2 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 440 Color: 3

Bin 701: 2 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 2
Size: 440 Color: 3

Bin 702: 2 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 3
Size: 440 Color: 0

Bin 703: 2 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 436 Color: 3

Bin 704: 2 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 426 Color: 4

Bin 705: 2 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 3
Size: 426 Color: 1

Bin 706: 2 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 3
Size: 426 Color: 0

Bin 707: 2 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 4
Size: 426 Color: 0

Bin 708: 2 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 2
Size: 421 Color: 3

Bin 709: 2 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 411 Color: 0

Bin 710: 2 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 400 Color: 3

Bin 711: 2 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 400 Color: 0

Bin 712: 2 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 393 Color: 0

Bin 713: 2 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 382 Color: 1

Bin 714: 2 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 375 Color: 4

Bin 715: 2 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 0
Size: 366 Color: 2

Bin 716: 2 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 366 Color: 2

Bin 717: 2 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 350 Color: 1

Bin 718: 2 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 350 Color: 1

Bin 719: 2 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 339 Color: 4

Bin 720: 2 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 339 Color: 2

Bin 721: 2 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 339 Color: 0

Bin 722: 2 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 4
Size: 333 Color: 2

Bin 723: 2 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 4
Size: 333 Color: 2

Bin 724: 2 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 329 Color: 1

Bin 725: 2 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 3
Size: 325 Color: 4

Bin 726: 2 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 310 Color: 0

Bin 727: 2 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 3
Size: 198 Color: 2
Size: 105 Color: 1

Bin 728: 2 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 3
Size: 295 Color: 2

Bin 729: 2 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 3
Size: 295 Color: 2

Bin 730: 2 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 282 Color: 1

Bin 731: 2 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 282 Color: 1

Bin 732: 2 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 1
Size: 273 Color: 0

Bin 733: 2 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 4
Size: 268 Color: 3

Bin 734: 2 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 261 Color: 1

Bin 735: 2 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 256 Color: 2

Bin 736: 2 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 256 Color: 1

Bin 737: 2 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 246 Color: 1

Bin 738: 2 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 246 Color: 0

Bin 739: 2 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 246 Color: 0

Bin 740: 2 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 237 Color: 0

Bin 741: 2 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 1
Size: 233 Color: 3

Bin 742: 2 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 225 Color: 1

Bin 743: 2 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 225 Color: 1

Bin 744: 2 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 1
Size: 222 Color: 2

Bin 745: 2 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 4
Size: 222 Color: 0

Bin 746: 2 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 218 Color: 3

Bin 747: 2 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 210 Color: 0

Bin 748: 2 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 210 Color: 0

Bin 749: 2 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 2
Size: 200 Color: 3

Bin 750: 2 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 2
Size: 200 Color: 1

Bin 751: 3 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 488 Color: 3

Bin 752: 3 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 488 Color: 0

Bin 753: 3 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 4
Size: 488 Color: 3

Bin 754: 3 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 483 Color: 1

Bin 755: 3 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 468 Color: 2

Bin 756: 3 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 4
Size: 468 Color: 1

Bin 757: 3 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 2
Size: 440 Color: 3

Bin 758: 3 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 3
Size: 421 Color: 2

Bin 759: 3 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 3
Size: 400 Color: 2

Bin 760: 3 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 3
Size: 393 Color: 0

Bin 761: 3 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 3
Size: 371 Color: 4

Bin 762: 3 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 4
Size: 371 Color: 1

Bin 763: 3 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 365 Color: 3

Bin 764: 3 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 365 Color: 3

Bin 765: 3 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 4
Size: 325 Color: 3

Bin 766: 3 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 4
Size: 321 Color: 1

Bin 767: 3 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 299 Color: 3

Bin 768: 3 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 2
Size: 299 Color: 0

Bin 769: 3 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 285 Color: 3

Bin 770: 3 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 280 Color: 2

Bin 771: 3 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 272 Color: 0

Bin 772: 3 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 2
Size: 268 Color: 1

Bin 773: 3 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 255 Color: 0

Bin 774: 3 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 4
Size: 237 Color: 3

Bin 775: 3 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 2
Size: 199 Color: 4

Bin 776: 3 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 2
Size: 199 Color: 4

Bin 777: 3 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 4
Size: 199 Color: 2

Bin 778: 4 of cap free
Amount of items: 6
Items: 
Size: 170 Color: 0
Size: 166 Color: 3
Size: 166 Color: 1
Size: 165 Color: 3
Size: 165 Color: 3
Size: 165 Color: 1

Bin 779: 4 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 4
Size: 487 Color: 0

Bin 780: 4 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 460 Color: 1

Bin 781: 4 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 459 Color: 0

Bin 782: 4 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 451 Color: 0

Bin 783: 4 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 434 Color: 1

Bin 784: 4 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 425 Color: 3

Bin 785: 4 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 3
Size: 421 Color: 2

Bin 786: 4 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 393 Color: 0

Bin 787: 4 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 4
Size: 350 Color: 3

Bin 788: 4 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 345 Color: 3

Bin 789: 4 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 3
Size: 333 Color: 0

Bin 790: 4 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 325 Color: 0

Bin 791: 4 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 279 Color: 0

Bin 792: 4 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 279 Color: 0

Bin 793: 4 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 272 Color: 0

Bin 794: 4 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 267 Color: 0

Bin 795: 4 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 254 Color: 0

Bin 796: 4 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 1
Size: 237 Color: 3

Bin 797: 4 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 2
Size: 199 Color: 4

Bin 798: 5 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 475 Color: 1

Bin 799: 5 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 3
Size: 468 Color: 1

Bin 800: 5 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 444 Color: 1

Bin 801: 5 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 3
Size: 414 Color: 2

Bin 802: 5 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 392 Color: 2

Bin 803: 5 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 350 Color: 1

Bin 804: 5 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 244 Color: 2

Bin 805: 5 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 3
Size: 236 Color: 1

Bin 806: 5 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 210 Color: 1

Bin 807: 6 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 468 Color: 1

Bin 808: 6 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 458 Color: 3

Bin 809: 6 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 2
Size: 408 Color: 0

Bin 810: 6 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 3
Size: 371 Color: 4

Bin 811: 6 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 4
Size: 371 Color: 0

Bin 812: 6 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 363 Color: 1

Bin 813: 6 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 279 Color: 1

Bin 814: 6 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 2
Size: 243 Color: 3

Bin 815: 7 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 4
Size: 392 Color: 1

Bin 816: 7 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 242 Color: 0

Bin 817: 7 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 208 Color: 1

Bin 818: 8 of cap free
Amount of items: 9
Items: 
Size: 112 Color: 3
Size: 112 Color: 2
Size: 112 Color: 0
Size: 111 Color: 0
Size: 110 Color: 4
Size: 110 Color: 4
Size: 109 Color: 0
Size: 109 Color: 0
Size: 108 Color: 0

Bin 819: 8 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 1
Size: 421 Color: 3

Bin 820: 8 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 3
Size: 421 Color: 2

Bin 821: 8 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 4
Size: 421 Color: 3

Bin 822: 8 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 4
Size: 421 Color: 3

Bin 823: 8 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 0
Size: 362 Color: 3

Bin 824: 8 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 207 Color: 0

Bin 825: 9 of cap free
Amount of items: 3
Items: 
Size: 616 Color: 2
Size: 188 Color: 3
Size: 188 Color: 2

Bin 826: 9 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 221 Color: 1

Bin 827: 10 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 421 Color: 0

Bin 828: 10 of cap free
Amount of items: 3
Items: 
Size: 616 Color: 4
Size: 188 Color: 2
Size: 187 Color: 2

Bin 829: 10 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 2
Size: 360 Color: 3

Bin 830: 10 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 3
Size: 360 Color: 0

Bin 831: 10 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 4
Size: 360 Color: 3

Bin 832: 11 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 392 Color: 1

Bin 833: 12 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 483 Color: 1

Bin 834: 12 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 2
Size: 420 Color: 3

Bin 835: 13 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 4
Size: 482 Color: 1

Bin 836: 13 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 391 Color: 2

Bin 837: 13 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 217 Color: 0

Bin 838: 14 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 3
Size: 482 Color: 1

Bin 839: 14 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 450 Color: 3

Bin 840: 14 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 392 Color: 0

Bin 841: 14 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 392 Color: 0

Bin 842: 14 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 390 Color: 4

Bin 843: 14 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 237 Color: 3

Bin 844: 14 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 3
Size: 236 Color: 0

Bin 845: 14 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 2
Size: 217 Color: 0

Bin 846: 15 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 450 Color: 4

Bin 847: 15 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 450 Color: 2

Bin 848: 15 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 450 Color: 1

Bin 849: 15 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 391 Color: 0

Bin 850: 15 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 4
Size: 391 Color: 0

Bin 851: 15 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 3
Size: 187 Color: 1
Size: 186 Color: 4

Bin 852: 16 of cap free
Amount of items: 6
Items: 
Size: 167 Color: 0
Size: 164 Color: 1
Size: 164 Color: 1
Size: 164 Color: 0
Size: 163 Color: 4
Size: 163 Color: 2

Bin 853: 16 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 4
Size: 390 Color: 1

Bin 854: 16 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 3
Size: 186 Color: 1
Size: 186 Color: 1

Bin 855: 17 of cap free
Amount of items: 7
Items: 
Size: 142 Color: 2
Size: 142 Color: 1
Size: 141 Color: 2
Size: 140 Color: 4
Size: 140 Color: 3
Size: 140 Color: 3
Size: 139 Color: 2

Bin 856: 17 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 3
Size: 390 Color: 1

Bin 857: 17 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 4
Size: 186 Color: 1
Size: 185 Color: 4

Bin 858: 17 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 4
Size: 321 Color: 0

Bin 859: 18 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 3
Size: 389 Color: 2

Bin 860: 18 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 389 Color: 2

Bin 861: 18 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 4
Size: 233 Color: 3

Bin 862: 19 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 360 Color: 3

Bin 863: 20 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 2
Size: 360 Color: 0

Bin 864: 21 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 4
Size: 481 Color: 1

Bin 865: 21 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 359 Color: 2

Bin 866: 21 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 3
Size: 265 Color: 1

Bin 867: 22 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 444 Color: 0

Bin 868: 23 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 2
Size: 183 Color: 3
Size: 183 Color: 2

Bin 869: 23 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 4
Size: 319 Color: 1

Bin 870: 23 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 265 Color: 1

Bin 871: 24 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 4
Size: 443 Color: 2

Bin 872: 25 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 359 Color: 2

Bin 873: 25 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 4
Size: 206 Color: 0

Bin 874: 25 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 4
Size: 206 Color: 0

Bin 875: 26 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 358 Color: 2

Bin 876: 27 of cap free
Amount of items: 8
Items: 
Size: 125 Color: 2
Size: 124 Color: 1
Size: 123 Color: 0
Size: 121 Color: 3
Size: 121 Color: 1
Size: 120 Color: 3
Size: 120 Color: 2
Size: 120 Color: 2

Bin 877: 27 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 358 Color: 3

Bin 878: 27 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 358 Color: 1

Bin 879: 27 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 4
Size: 358 Color: 2

Bin 880: 27 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 265 Color: 0

Bin 881: 27 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 0
Size: 205 Color: 4

Bin 882: 27 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 205 Color: 2

Bin 883: 28 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 265 Color: 0

Bin 884: 29 of cap free
Amount of items: 6
Items: 
Size: 163 Color: 2
Size: 163 Color: 1
Size: 162 Color: 4
Size: 162 Color: 3
Size: 161 Color: 4
Size: 161 Color: 3

Bin 885: 29 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 264 Color: 2

Bin 886: 32 of cap free
Amount of items: 7
Items: 
Size: 142 Color: 2
Size: 139 Color: 1
Size: 138 Color: 3
Size: 138 Color: 3
Size: 138 Color: 2
Size: 138 Color: 2
Size: 136 Color: 4

Bin 887: 33 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 264 Color: 0

Bin 888: 35 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 439 Color: 1

Bin 889: 36 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 261 Color: 0

Bin 890: 38 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 3
Size: 260 Color: 2

Bin 891: 38 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 260 Color: 3

Bin 892: 40 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 4
Size: 434 Color: 0

Bin 893: 42 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 260 Color: 3

Bin 894: 42 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 3
Size: 260 Color: 2

Bin 895: 46 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 434 Color: 0

Bin 896: 47 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 3
Size: 205 Color: 0

Bin 897: 47 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 205 Color: 0

Bin 898: 49 of cap free
Amount of items: 6
Items: 
Size: 161 Color: 1
Size: 160 Color: 0
Size: 159 Color: 2
Size: 159 Color: 0
Size: 157 Color: 3
Size: 156 Color: 0

Bin 899: 50 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 254 Color: 4

Bin 900: 51 of cap free
Amount of items: 3
Items: 
Size: 581 Color: 3
Size: 185 Color: 2
Size: 184 Color: 3

Bin 901: 51 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 2
Size: 183 Color: 1
Size: 182 Color: 0

Bin 902: 56 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 2
Size: 180 Color: 4
Size: 180 Color: 4

Bin 903: 57 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 2
Size: 180 Color: 4
Size: 180 Color: 2

Bin 904: 59 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 2
Size: 180 Color: 1
Size: 180 Color: 1

Bin 905: 60 of cap free
Amount of items: 7
Items: 
Size: 136 Color: 1
Size: 135 Color: 4
Size: 135 Color: 3
Size: 135 Color: 0
Size: 134 Color: 1
Size: 133 Color: 4
Size: 133 Color: 2

Bin 906: 63 of cap free
Amount of items: 9
Items: 
Size: 112 Color: 3
Size: 105 Color: 0
Size: 104 Color: 1
Size: 104 Color: 1
Size: 104 Color: 0
Size: 103 Color: 2
Size: 103 Color: 1
Size: 102 Color: 0
Size: 101 Color: 2

Bin 907: 64 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 3
Size: 184 Color: 2
Size: 184 Color: 0

Bin 908: 70 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 3
Size: 181 Color: 2
Size: 181 Color: 1

Bin 909: 75 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 3
Size: 179 Color: 4
Size: 179 Color: 2

Bin 910: 75 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 3
Size: 179 Color: 0
Size: 179 Color: 0

Bin 911: 77 of cap free
Amount of items: 8
Items: 
Size: 119 Color: 3
Size: 119 Color: 2
Size: 116 Color: 2
Size: 116 Color: 1
Size: 116 Color: 0
Size: 113 Color: 4
Size: 113 Color: 0
Size: 112 Color: 4

Bin 912: 78 of cap free
Amount of items: 6
Items: 
Size: 160 Color: 1
Size: 154 Color: 2
Size: 154 Color: 0
Size: 152 Color: 4
Size: 152 Color: 3
Size: 151 Color: 4

Bin 913: 79 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 4
Size: 177 Color: 0
Size: 177 Color: 0

Bin 914: 81 of cap free
Amount of items: 7
Items: 
Size: 133 Color: 0
Size: 132 Color: 4
Size: 132 Color: 0
Size: 131 Color: 3
Size: 131 Color: 2
Size: 131 Color: 0
Size: 130 Color: 1

Bin 915: 86 of cap free
Amount of items: 6
Items: 
Size: 160 Color: 1
Size: 151 Color: 3
Size: 151 Color: 2
Size: 151 Color: 1
Size: 151 Color: 0
Size: 151 Color: 0

Bin 916: 93 of cap free
Amount of items: 7
Items: 
Size: 133 Color: 0
Size: 130 Color: 1
Size: 130 Color: 0
Size: 129 Color: 2
Size: 129 Color: 1
Size: 129 Color: 0
Size: 128 Color: 4

Bin 917: 99 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 205 Color: 0

Bin 918: 901 of cap free
Amount of items: 1
Items: 
Size: 100 Color: 2

Total size: 914544
Total free space: 4374

