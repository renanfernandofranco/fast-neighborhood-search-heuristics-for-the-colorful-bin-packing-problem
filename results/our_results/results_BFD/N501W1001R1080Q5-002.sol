Capicity Bin: 1001
Lower Bound: 230

Bins used: 233
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 494 Color: 0

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 4

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 3

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 2
Size: 484 Color: 1

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 2
Size: 481 Color: 1

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 1

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 0
Size: 474 Color: 3

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 2

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 466 Color: 2

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 4
Size: 463 Color: 3

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 462 Color: 4

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 462 Color: 2

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 540 Color: 3
Size: 156 Color: 2
Size: 153 Color: 4
Size: 152 Color: 1

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 459 Color: 2

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 2
Size: 458 Color: 4

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 2
Size: 457 Color: 1

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 547 Color: 2
Size: 164 Color: 0
Size: 164 Color: 0
Size: 126 Color: 2

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 551 Color: 2
Size: 171 Color: 0
Size: 169 Color: 3
Size: 110 Color: 1

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 3

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 2
Size: 436 Color: 1

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 576 Color: 2
Size: 283 Color: 3
Size: 142 Color: 2

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 4
Size: 423 Color: 1

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 0
Size: 415 Color: 3

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 413 Color: 4

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 4
Size: 412 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 590 Color: 0
Size: 304 Color: 2
Size: 107 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 590 Color: 4
Size: 298 Color: 0
Size: 113 Color: 4

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 401 Color: 2

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 3
Size: 399 Color: 0

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 4

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 2
Size: 387 Color: 4

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 384 Color: 1

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 1
Size: 379 Color: 0

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 373 Color: 3

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 2

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 0

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 0
Size: 366 Color: 3

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 2

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 0
Size: 364 Color: 4

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 359 Color: 3

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 0
Size: 358 Color: 2

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 4

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 1
Size: 334 Color: 4

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 1
Size: 334 Color: 3

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 4

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 0
Size: 324 Color: 1

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 3

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 2

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 308 Color: 3

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 4

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 1
Size: 298 Color: 2

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 291 Color: 2

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 280 Color: 3

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 1

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 1

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 3
Size: 274 Color: 2

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 3

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 3
Size: 264 Color: 1

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 2
Size: 262 Color: 0

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 1
Size: 254 Color: 4

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 0
Size: 245 Color: 3

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 0

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 0
Size: 241 Color: 4

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 1
Size: 241 Color: 0

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 2

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 2
Size: 232 Color: 1

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 3

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 227 Color: 1

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 3

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 222 Color: 0

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 3

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 0
Size: 214 Color: 4

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 212 Color: 4

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 211 Color: 0

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 2

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 3
Size: 207 Color: 4

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 3
Size: 207 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 4
Size: 204 Color: 1

Bin 81: 1 of cap free
Amount of items: 7
Items: 
Size: 156 Color: 3
Size: 148 Color: 4
Size: 148 Color: 2
Size: 147 Color: 4
Size: 145 Color: 3
Size: 145 Color: 2
Size: 111 Color: 2

Bin 82: 1 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 496 Color: 1

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 4
Size: 482 Color: 3

Bin 84: 1 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 3
Size: 475 Color: 2

Bin 85: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 453 Color: 4

Bin 86: 1 of cap free
Amount of items: 4
Items: 
Size: 550 Color: 0
Size: 158 Color: 1
Size: 156 Color: 4
Size: 136 Color: 4

Bin 87: 1 of cap free
Amount of items: 4
Items: 
Size: 551 Color: 3
Size: 168 Color: 4
Size: 165 Color: 0
Size: 116 Color: 3

Bin 88: 1 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 443 Color: 2

Bin 89: 1 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 421 Color: 0

Bin 90: 1 of cap free
Amount of items: 3
Items: 
Size: 579 Color: 3
Size: 299 Color: 4
Size: 122 Color: 2

Bin 91: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 3
Size: 414 Color: 0

Bin 92: 1 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 4
Size: 305 Color: 1
Size: 104 Color: 0

Bin 93: 1 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 3
Size: 400 Color: 2

Bin 94: 1 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 394 Color: 1

Bin 95: 1 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 394 Color: 1

Bin 96: 1 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 390 Color: 1

Bin 97: 1 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 389 Color: 4

Bin 98: 1 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 2
Size: 385 Color: 4

Bin 99: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 373 Color: 0

Bin 100: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 367 Color: 4

Bin 101: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 367 Color: 1

Bin 102: 1 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 0
Size: 362 Color: 1

Bin 103: 1 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 3
Size: 346 Color: 2

Bin 104: 1 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 346 Color: 0

Bin 105: 1 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 3
Size: 335 Color: 0

Bin 106: 1 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 3
Size: 331 Color: 0

Bin 107: 1 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 0
Size: 323 Color: 3

Bin 108: 1 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 4
Size: 323 Color: 2

Bin 109: 1 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 0
Size: 313 Color: 2

Bin 110: 1 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 0
Size: 311 Color: 4

Bin 111: 1 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 4
Size: 311 Color: 3

Bin 112: 1 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 309 Color: 3

Bin 113: 1 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 2
Size: 307 Color: 1

Bin 114: 1 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 302 Color: 2

Bin 115: 1 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 295 Color: 4

Bin 116: 1 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 2
Size: 291 Color: 3

Bin 117: 1 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 291 Color: 3

Bin 118: 1 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 287 Color: 2

Bin 119: 1 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 2
Size: 285 Color: 3

Bin 120: 1 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 2
Size: 253 Color: 1

Bin 121: 1 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 238 Color: 2

Bin 122: 1 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 2
Size: 222 Color: 0

Bin 123: 1 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 216 Color: 1

Bin 124: 1 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 0
Size: 205 Color: 1

Bin 125: 2 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 2
Size: 491 Color: 3

Bin 126: 2 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 486 Color: 4

Bin 127: 2 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 3
Size: 482 Color: 2

Bin 128: 2 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 4
Size: 476 Color: 3

Bin 129: 2 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 3
Size: 468 Color: 4

Bin 130: 2 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 433 Color: 3

Bin 131: 2 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 4
Size: 414 Color: 3

Bin 132: 2 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 4
Size: 403 Color: 2

Bin 133: 2 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 1
Size: 378 Color: 3

Bin 134: 2 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 0
Size: 364 Color: 2

Bin 135: 2 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 326 Color: 4

Bin 136: 2 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 306 Color: 1

Bin 137: 2 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 295 Color: 0

Bin 138: 2 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 286 Color: 2

Bin 139: 2 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 2
Size: 285 Color: 1

Bin 140: 2 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 273 Color: 3

Bin 141: 2 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 259 Color: 2

Bin 142: 2 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 0
Size: 233 Color: 4

Bin 143: 2 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 198 Color: 1

Bin 144: 3 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 487 Color: 2

Bin 145: 3 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 2
Size: 486 Color: 3

Bin 146: 3 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 471 Color: 3

Bin 147: 3 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 3
Size: 464 Color: 4

Bin 148: 3 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 446 Color: 1

Bin 149: 3 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 3
Size: 395 Color: 2

Bin 150: 3 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 355 Color: 4

Bin 151: 3 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 315 Color: 4

Bin 152: 3 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 3
Size: 300 Color: 4

Bin 153: 3 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 277 Color: 1

Bin 154: 3 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 3
Size: 258 Color: 1

Bin 155: 4 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 2
Size: 481 Color: 0

Bin 156: 4 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 433 Color: 1

Bin 157: 4 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 413 Color: 1

Bin 158: 4 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 378 Color: 2

Bin 159: 4 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 369 Color: 3

Bin 160: 4 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 258 Color: 3

Bin 161: 4 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 2
Size: 253 Color: 1

Bin 162: 5 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 3
Size: 480 Color: 4

Bin 163: 5 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 4
Size: 480 Color: 3

Bin 164: 5 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 424 Color: 2

Bin 165: 5 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 378 Color: 0

Bin 166: 5 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 2
Size: 328 Color: 0

Bin 167: 5 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 276 Color: 2

Bin 168: 6 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 491 Color: 0

Bin 169: 6 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 442 Color: 2

Bin 170: 6 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 432 Color: 3

Bin 171: 6 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 224 Color: 3

Bin 172: 6 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 224 Color: 0

Bin 173: 7 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 3
Size: 432 Color: 2

Bin 174: 7 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 394 Color: 2

Bin 175: 7 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 393 Color: 4

Bin 176: 7 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 0
Size: 202 Color: 4

Bin 177: 7 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 194 Color: 3

Bin 178: 7 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 194 Color: 3

Bin 179: 8 of cap free
Amount of items: 8
Items: 
Size: 133 Color: 4
Size: 130 Color: 0
Size: 127 Color: 1
Size: 124 Color: 3
Size: 124 Color: 3
Size: 121 Color: 3
Size: 120 Color: 4
Size: 114 Color: 0

Bin 180: 8 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 356 Color: 3

Bin 181: 8 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 0
Size: 323 Color: 1

Bin 182: 9 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 440 Color: 2

Bin 183: 9 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 393 Color: 3

Bin 184: 9 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 378 Color: 0

Bin 185: 10 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 353 Color: 1

Bin 186: 10 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 272 Color: 3

Bin 187: 10 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 3
Size: 271 Color: 4

Bin 188: 11 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 486 Color: 2

Bin 189: 11 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 3
Size: 250 Color: 4

Bin 190: 11 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 2
Size: 221 Color: 0

Bin 191: 12 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 4
Size: 327 Color: 0

Bin 192: 14 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 3
Size: 486 Color: 1

Bin 193: 14 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 376 Color: 4

Bin 194: 15 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 2
Size: 252 Color: 3

Bin 195: 15 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 248 Color: 1

Bin 196: 15 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 2
Size: 221 Color: 0

Bin 197: 16 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 193 Color: 4

Bin 198: 17 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 4
Size: 220 Color: 2

Bin 199: 18 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 248 Color: 1

Bin 200: 20 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 220 Color: 2

Bin 201: 21 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 345 Color: 3

Bin 202: 21 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 320 Color: 4

Bin 203: 21 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 2
Size: 320 Color: 1

Bin 204: 25 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 343 Color: 1

Bin 205: 26 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 3
Size: 343 Color: 0

Bin 206: 26 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 4
Size: 215 Color: 1

Bin 207: 29 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 215 Color: 0

Bin 208: 31 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 213 Color: 3

Bin 209: 32 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 213 Color: 0

Bin 210: 34 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 1
Size: 212 Color: 3

Bin 211: 36 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 211 Color: 0

Bin 212: 42 of cap free
Amount of items: 7
Items: 
Size: 143 Color: 4
Size: 140 Color: 0
Size: 138 Color: 2
Size: 135 Color: 2
Size: 135 Color: 0
Size: 135 Color: 0
Size: 133 Color: 4

Bin 213: 49 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 1
Size: 342 Color: 3

Bin 214: 54 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 3
Size: 315 Color: 1

Bin 215: 55 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 1
Size: 193 Color: 3

Bin 216: 73 of cap free
Amount of items: 4
Items: 
Size: 475 Color: 3
Size: 153 Color: 2
Size: 150 Color: 3
Size: 150 Color: 1

Bin 217: 73 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 2
Size: 178 Color: 3
Size: 177 Color: 2

Bin 218: 76 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 190 Color: 2

Bin 219: 77 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 2
Size: 190 Color: 0

Bin 220: 78 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 4
Size: 189 Color: 3

Bin 221: 79 of cap free
Amount of items: 3
Items: 
Size: 572 Color: 4
Size: 176 Color: 1
Size: 174 Color: 2

Bin 222: 79 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 4
Size: 188 Color: 3

Bin 223: 80 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 311 Color: 0

Bin 224: 80 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 187 Color: 2

Bin 225: 81 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 186 Color: 0

Bin 226: 84 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 186 Color: 3

Bin 227: 93 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 3
Size: 309 Color: 1

Bin 228: 97 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 3
Size: 305 Color: 2

Bin 229: 98 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 2
Size: 174 Color: 1
Size: 171 Color: 0

Bin 230: 101 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 3
Size: 183 Color: 1

Bin 231: 102 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 304 Color: 3

Bin 232: 110 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 301 Color: 3

Bin 233: 887 of cap free
Amount of items: 1
Items: 
Size: 114 Color: 0

Total size: 229868
Total free space: 3365

