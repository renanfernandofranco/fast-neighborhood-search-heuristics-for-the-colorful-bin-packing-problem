Capicity Bin: 1000001
Lower Bound: 897

Bins used: 901
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 583302 Color: 0
Size: 416699 Color: 3

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 584735 Color: 1
Size: 415266 Color: 4

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 631055 Color: 3
Size: 368946 Color: 1

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 702073 Color: 2
Size: 297928 Color: 1

Bin 5: 1 of cap free
Amount of items: 2
Items: 
Size: 685790 Color: 0
Size: 314210 Color: 4

Bin 6: 2 of cap free
Amount of items: 2
Items: 
Size: 766506 Color: 0
Size: 233493 Color: 3

Bin 7: 3 of cap free
Amount of items: 2
Items: 
Size: 779337 Color: 3
Size: 220661 Color: 4

Bin 8: 4 of cap free
Amount of items: 2
Items: 
Size: 618885 Color: 0
Size: 381112 Color: 2

Bin 9: 4 of cap free
Amount of items: 2
Items: 
Size: 690985 Color: 0
Size: 309012 Color: 2

Bin 10: 5 of cap free
Amount of items: 2
Items: 
Size: 554086 Color: 3
Size: 445910 Color: 0

Bin 11: 5 of cap free
Amount of items: 2
Items: 
Size: 582371 Color: 0
Size: 417625 Color: 1

Bin 12: 5 of cap free
Amount of items: 2
Items: 
Size: 666520 Color: 0
Size: 333476 Color: 3

Bin 13: 5 of cap free
Amount of items: 2
Items: 
Size: 673247 Color: 1
Size: 326749 Color: 3

Bin 14: 6 of cap free
Amount of items: 2
Items: 
Size: 546328 Color: 0
Size: 453667 Color: 3

Bin 15: 6 of cap free
Amount of items: 2
Items: 
Size: 693726 Color: 3
Size: 306269 Color: 0

Bin 16: 6 of cap free
Amount of items: 2
Items: 
Size: 787001 Color: 1
Size: 212994 Color: 0

Bin 17: 8 of cap free
Amount of items: 2
Items: 
Size: 557598 Color: 1
Size: 442395 Color: 0

Bin 18: 9 of cap free
Amount of items: 2
Items: 
Size: 543314 Color: 3
Size: 456678 Color: 1

Bin 19: 9 of cap free
Amount of items: 2
Items: 
Size: 637608 Color: 4
Size: 362384 Color: 1

Bin 20: 9 of cap free
Amount of items: 2
Items: 
Size: 682853 Color: 3
Size: 317139 Color: 2

Bin 21: 9 of cap free
Amount of items: 2
Items: 
Size: 777560 Color: 3
Size: 222432 Color: 1

Bin 22: 9 of cap free
Amount of items: 2
Items: 
Size: 795485 Color: 3
Size: 204507 Color: 0

Bin 23: 10 of cap free
Amount of items: 2
Items: 
Size: 554423 Color: 2
Size: 445568 Color: 1

Bin 24: 10 of cap free
Amount of items: 2
Items: 
Size: 629948 Color: 4
Size: 370043 Color: 0

Bin 25: 10 of cap free
Amount of items: 2
Items: 
Size: 650020 Color: 4
Size: 349971 Color: 1

Bin 26: 10 of cap free
Amount of items: 2
Items: 
Size: 791910 Color: 1
Size: 208081 Color: 0

Bin 27: 11 of cap free
Amount of items: 2
Items: 
Size: 567243 Color: 4
Size: 432747 Color: 2

Bin 28: 11 of cap free
Amount of items: 2
Items: 
Size: 672099 Color: 3
Size: 327891 Color: 0

Bin 29: 11 of cap free
Amount of items: 2
Items: 
Size: 763397 Color: 0
Size: 236593 Color: 3

Bin 30: 11 of cap free
Amount of items: 2
Items: 
Size: 789722 Color: 0
Size: 210268 Color: 1

Bin 31: 12 of cap free
Amount of items: 2
Items: 
Size: 734455 Color: 1
Size: 265534 Color: 4

Bin 32: 13 of cap free
Amount of items: 2
Items: 
Size: 612875 Color: 4
Size: 387113 Color: 1

Bin 33: 14 of cap free
Amount of items: 2
Items: 
Size: 543631 Color: 2
Size: 456356 Color: 3

Bin 34: 14 of cap free
Amount of items: 2
Items: 
Size: 554927 Color: 4
Size: 445060 Color: 3

Bin 35: 14 of cap free
Amount of items: 2
Items: 
Size: 600938 Color: 3
Size: 399049 Color: 1

Bin 36: 15 of cap free
Amount of items: 2
Items: 
Size: 526184 Color: 0
Size: 473802 Color: 2

Bin 37: 15 of cap free
Amount of items: 2
Items: 
Size: 735477 Color: 1
Size: 264509 Color: 2

Bin 38: 16 of cap free
Amount of items: 2
Items: 
Size: 512706 Color: 2
Size: 487279 Color: 4

Bin 39: 16 of cap free
Amount of items: 2
Items: 
Size: 619539 Color: 1
Size: 380446 Color: 2

Bin 40: 16 of cap free
Amount of items: 2
Items: 
Size: 779113 Color: 3
Size: 220872 Color: 0

Bin 41: 17 of cap free
Amount of items: 2
Items: 
Size: 513490 Color: 0
Size: 486494 Color: 3

Bin 42: 18 of cap free
Amount of items: 2
Items: 
Size: 590261 Color: 0
Size: 409722 Color: 3

Bin 43: 18 of cap free
Amount of items: 2
Items: 
Size: 713251 Color: 3
Size: 286732 Color: 1

Bin 44: 19 of cap free
Amount of items: 2
Items: 
Size: 675941 Color: 1
Size: 324041 Color: 0

Bin 45: 19 of cap free
Amount of items: 2
Items: 
Size: 752592 Color: 3
Size: 247390 Color: 0

Bin 46: 20 of cap free
Amount of items: 2
Items: 
Size: 535210 Color: 1
Size: 464771 Color: 4

Bin 47: 20 of cap free
Amount of items: 2
Items: 
Size: 555275 Color: 4
Size: 444706 Color: 0

Bin 48: 20 of cap free
Amount of items: 2
Items: 
Size: 676240 Color: 0
Size: 323741 Color: 3

Bin 49: 20 of cap free
Amount of items: 2
Items: 
Size: 679638 Color: 1
Size: 320343 Color: 0

Bin 50: 21 of cap free
Amount of items: 2
Items: 
Size: 579996 Color: 0
Size: 419984 Color: 1

Bin 51: 21 of cap free
Amount of items: 2
Items: 
Size: 601436 Color: 3
Size: 398544 Color: 2

Bin 52: 21 of cap free
Amount of items: 2
Items: 
Size: 783257 Color: 4
Size: 216723 Color: 2

Bin 53: 23 of cap free
Amount of items: 2
Items: 
Size: 609318 Color: 4
Size: 390660 Color: 1

Bin 54: 23 of cap free
Amount of items: 2
Items: 
Size: 624411 Color: 3
Size: 375567 Color: 2

Bin 55: 23 of cap free
Amount of items: 2
Items: 
Size: 795244 Color: 4
Size: 204734 Color: 0

Bin 56: 24 of cap free
Amount of items: 6
Items: 
Size: 171044 Color: 3
Size: 170844 Color: 1
Size: 170295 Color: 4
Size: 168760 Color: 2
Size: 168687 Color: 3
Size: 150347 Color: 2

Bin 57: 24 of cap free
Amount of items: 2
Items: 
Size: 516629 Color: 1
Size: 483348 Color: 4

Bin 58: 25 of cap free
Amount of items: 2
Items: 
Size: 505449 Color: 4
Size: 494527 Color: 3

Bin 59: 25 of cap free
Amount of items: 2
Items: 
Size: 587267 Color: 0
Size: 412709 Color: 1

Bin 60: 25 of cap free
Amount of items: 2
Items: 
Size: 780450 Color: 4
Size: 219526 Color: 0

Bin 61: 26 of cap free
Amount of items: 3
Items: 
Size: 525698 Color: 1
Size: 339363 Color: 2
Size: 134914 Color: 1

Bin 62: 27 of cap free
Amount of items: 2
Items: 
Size: 635593 Color: 3
Size: 364381 Color: 1

Bin 63: 29 of cap free
Amount of items: 6
Items: 
Size: 175548 Color: 3
Size: 175266 Color: 2
Size: 175036 Color: 3
Size: 174659 Color: 2
Size: 173844 Color: 0
Size: 125619 Color: 1

Bin 64: 29 of cap free
Amount of items: 2
Items: 
Size: 535155 Color: 3
Size: 464817 Color: 0

Bin 65: 29 of cap free
Amount of items: 2
Items: 
Size: 699916 Color: 2
Size: 300056 Color: 3

Bin 66: 30 of cap free
Amount of items: 2
Items: 
Size: 734302 Color: 0
Size: 265669 Color: 1

Bin 67: 31 of cap free
Amount of items: 2
Items: 
Size: 658315 Color: 4
Size: 341655 Color: 2

Bin 68: 32 of cap free
Amount of items: 2
Items: 
Size: 666950 Color: 2
Size: 333019 Color: 3

Bin 69: 32 of cap free
Amount of items: 2
Items: 
Size: 674065 Color: 2
Size: 325904 Color: 4

Bin 70: 32 of cap free
Amount of items: 2
Items: 
Size: 781642 Color: 0
Size: 218327 Color: 4

Bin 71: 33 of cap free
Amount of items: 2
Items: 
Size: 603570 Color: 1
Size: 396398 Color: 0

Bin 72: 33 of cap free
Amount of items: 2
Items: 
Size: 645034 Color: 2
Size: 354934 Color: 4

Bin 73: 33 of cap free
Amount of items: 2
Items: 
Size: 678743 Color: 4
Size: 321225 Color: 0

Bin 74: 34 of cap free
Amount of items: 2
Items: 
Size: 553589 Color: 2
Size: 446378 Color: 4

Bin 75: 34 of cap free
Amount of items: 2
Items: 
Size: 647515 Color: 0
Size: 352452 Color: 4

Bin 76: 35 of cap free
Amount of items: 2
Items: 
Size: 544419 Color: 1
Size: 455547 Color: 2

Bin 77: 35 of cap free
Amount of items: 2
Items: 
Size: 591248 Color: 1
Size: 408718 Color: 0

Bin 78: 35 of cap free
Amount of items: 2
Items: 
Size: 723419 Color: 3
Size: 276547 Color: 1

Bin 79: 36 of cap free
Amount of items: 2
Items: 
Size: 548188 Color: 3
Size: 451777 Color: 0

Bin 80: 36 of cap free
Amount of items: 2
Items: 
Size: 649720 Color: 3
Size: 350245 Color: 2

Bin 81: 36 of cap free
Amount of items: 2
Items: 
Size: 786596 Color: 2
Size: 213369 Color: 3

Bin 82: 37 of cap free
Amount of items: 2
Items: 
Size: 635199 Color: 2
Size: 364765 Color: 1

Bin 83: 38 of cap free
Amount of items: 2
Items: 
Size: 704588 Color: 2
Size: 295375 Color: 1

Bin 84: 39 of cap free
Amount of items: 2
Items: 
Size: 706473 Color: 1
Size: 293489 Color: 3

Bin 85: 40 of cap free
Amount of items: 2
Items: 
Size: 520286 Color: 4
Size: 479675 Color: 3

Bin 86: 40 of cap free
Amount of items: 2
Items: 
Size: 698977 Color: 3
Size: 300984 Color: 4

Bin 87: 40 of cap free
Amount of items: 2
Items: 
Size: 792495 Color: 3
Size: 207466 Color: 4

Bin 88: 41 of cap free
Amount of items: 2
Items: 
Size: 774057 Color: 2
Size: 225903 Color: 3

Bin 89: 42 of cap free
Amount of items: 2
Items: 
Size: 757737 Color: 3
Size: 242222 Color: 2

Bin 90: 43 of cap free
Amount of items: 2
Items: 
Size: 789090 Color: 0
Size: 210868 Color: 2

Bin 91: 44 of cap free
Amount of items: 2
Items: 
Size: 684623 Color: 3
Size: 315334 Color: 4

Bin 92: 45 of cap free
Amount of items: 2
Items: 
Size: 544813 Color: 3
Size: 455143 Color: 4

Bin 93: 45 of cap free
Amount of items: 2
Items: 
Size: 703468 Color: 3
Size: 296488 Color: 1

Bin 94: 46 of cap free
Amount of items: 2
Items: 
Size: 769654 Color: 1
Size: 230301 Color: 4

Bin 95: 46 of cap free
Amount of items: 2
Items: 
Size: 772459 Color: 1
Size: 227496 Color: 4

Bin 96: 49 of cap free
Amount of items: 2
Items: 
Size: 518442 Color: 1
Size: 481510 Color: 3

Bin 97: 49 of cap free
Amount of items: 2
Items: 
Size: 644561 Color: 2
Size: 355391 Color: 3

Bin 98: 51 of cap free
Amount of items: 2
Items: 
Size: 602607 Color: 2
Size: 397343 Color: 1

Bin 99: 51 of cap free
Amount of items: 2
Items: 
Size: 642322 Color: 2
Size: 357628 Color: 3

Bin 100: 51 of cap free
Amount of items: 2
Items: 
Size: 732877 Color: 4
Size: 267073 Color: 3

Bin 101: 52 of cap free
Amount of items: 2
Items: 
Size: 699530 Color: 3
Size: 300419 Color: 2

Bin 102: 52 of cap free
Amount of items: 2
Items: 
Size: 736246 Color: 0
Size: 263703 Color: 3

Bin 103: 54 of cap free
Amount of items: 2
Items: 
Size: 786460 Color: 3
Size: 213487 Color: 2

Bin 104: 55 of cap free
Amount of items: 4
Items: 
Size: 315932 Color: 1
Size: 280720 Color: 4
Size: 250616 Color: 4
Size: 152678 Color: 0

Bin 105: 55 of cap free
Amount of items: 2
Items: 
Size: 534050 Color: 1
Size: 465896 Color: 0

Bin 106: 55 of cap free
Amount of items: 2
Items: 
Size: 609658 Color: 2
Size: 390288 Color: 1

Bin 107: 55 of cap free
Amount of items: 2
Items: 
Size: 745718 Color: 2
Size: 254228 Color: 4

Bin 108: 56 of cap free
Amount of items: 2
Items: 
Size: 521523 Color: 1
Size: 478422 Color: 4

Bin 109: 56 of cap free
Amount of items: 2
Items: 
Size: 539872 Color: 4
Size: 460073 Color: 0

Bin 110: 56 of cap free
Amount of items: 2
Items: 
Size: 642707 Color: 0
Size: 357238 Color: 3

Bin 111: 56 of cap free
Amount of items: 2
Items: 
Size: 695154 Color: 1
Size: 304791 Color: 0

Bin 112: 56 of cap free
Amount of items: 2
Items: 
Size: 749163 Color: 0
Size: 250782 Color: 1

Bin 113: 57 of cap free
Amount of items: 2
Items: 
Size: 511966 Color: 0
Size: 487978 Color: 3

Bin 114: 57 of cap free
Amount of items: 2
Items: 
Size: 714668 Color: 1
Size: 285276 Color: 4

Bin 115: 57 of cap free
Amount of items: 2
Items: 
Size: 725920 Color: 0
Size: 274024 Color: 1

Bin 116: 60 of cap free
Amount of items: 2
Items: 
Size: 533308 Color: 0
Size: 466633 Color: 4

Bin 117: 60 of cap free
Amount of items: 2
Items: 
Size: 594551 Color: 3
Size: 405390 Color: 4

Bin 118: 60 of cap free
Amount of items: 2
Items: 
Size: 693297 Color: 2
Size: 306644 Color: 0

Bin 119: 62 of cap free
Amount of items: 2
Items: 
Size: 596090 Color: 3
Size: 403849 Color: 0

Bin 120: 63 of cap free
Amount of items: 2
Items: 
Size: 550954 Color: 0
Size: 448984 Color: 4

Bin 121: 63 of cap free
Amount of items: 2
Items: 
Size: 698616 Color: 3
Size: 301322 Color: 0

Bin 122: 64 of cap free
Amount of items: 2
Items: 
Size: 676898 Color: 4
Size: 323039 Color: 3

Bin 123: 64 of cap free
Amount of items: 2
Items: 
Size: 690073 Color: 2
Size: 309864 Color: 1

Bin 124: 65 of cap free
Amount of items: 2
Items: 
Size: 645621 Color: 4
Size: 354315 Color: 1

Bin 125: 65 of cap free
Amount of items: 2
Items: 
Size: 701706 Color: 4
Size: 298230 Color: 1

Bin 126: 66 of cap free
Amount of items: 2
Items: 
Size: 756199 Color: 0
Size: 243736 Color: 1

Bin 127: 67 of cap free
Amount of items: 2
Items: 
Size: 510958 Color: 1
Size: 488976 Color: 4

Bin 128: 67 of cap free
Amount of items: 2
Items: 
Size: 567093 Color: 2
Size: 432841 Color: 1

Bin 129: 67 of cap free
Amount of items: 2
Items: 
Size: 674731 Color: 3
Size: 325203 Color: 1

Bin 130: 68 of cap free
Amount of items: 2
Items: 
Size: 503157 Color: 1
Size: 496776 Color: 3

Bin 131: 68 of cap free
Amount of items: 2
Items: 
Size: 570153 Color: 4
Size: 429780 Color: 1

Bin 132: 68 of cap free
Amount of items: 2
Items: 
Size: 745220 Color: 2
Size: 254713 Color: 3

Bin 133: 69 of cap free
Amount of items: 2
Items: 
Size: 598483 Color: 0
Size: 401449 Color: 1

Bin 134: 70 of cap free
Amount of items: 2
Items: 
Size: 533010 Color: 3
Size: 466921 Color: 1

Bin 135: 70 of cap free
Amount of items: 2
Items: 
Size: 573704 Color: 0
Size: 426227 Color: 2

Bin 136: 70 of cap free
Amount of items: 2
Items: 
Size: 670042 Color: 3
Size: 329889 Color: 1

Bin 137: 70 of cap free
Amount of items: 2
Items: 
Size: 750164 Color: 1
Size: 249767 Color: 2

Bin 138: 70 of cap free
Amount of items: 2
Items: 
Size: 783826 Color: 3
Size: 216105 Color: 0

Bin 139: 71 of cap free
Amount of items: 5
Items: 
Size: 222223 Color: 3
Size: 222116 Color: 4
Size: 214965 Color: 1
Size: 210375 Color: 3
Size: 130251 Color: 3

Bin 140: 71 of cap free
Amount of items: 2
Items: 
Size: 524692 Color: 1
Size: 475238 Color: 4

Bin 141: 71 of cap free
Amount of items: 2
Items: 
Size: 634451 Color: 3
Size: 365479 Color: 4

Bin 142: 71 of cap free
Amount of items: 2
Items: 
Size: 652738 Color: 3
Size: 347192 Color: 1

Bin 143: 71 of cap free
Amount of items: 2
Items: 
Size: 774715 Color: 1
Size: 225215 Color: 2

Bin 144: 72 of cap free
Amount of items: 2
Items: 
Size: 686347 Color: 3
Size: 313582 Color: 2

Bin 145: 73 of cap free
Amount of items: 2
Items: 
Size: 762957 Color: 2
Size: 236971 Color: 1

Bin 146: 74 of cap free
Amount of items: 2
Items: 
Size: 505798 Color: 4
Size: 494129 Color: 0

Bin 147: 74 of cap free
Amount of items: 2
Items: 
Size: 644715 Color: 4
Size: 355212 Color: 1

Bin 148: 74 of cap free
Amount of items: 2
Items: 
Size: 660164 Color: 2
Size: 339763 Color: 4

Bin 149: 74 of cap free
Amount of items: 2
Items: 
Size: 769880 Color: 3
Size: 230047 Color: 4

Bin 150: 76 of cap free
Amount of items: 2
Items: 
Size: 618223 Color: 0
Size: 381702 Color: 1

Bin 151: 77 of cap free
Amount of items: 2
Items: 
Size: 723046 Color: 2
Size: 276878 Color: 3

Bin 152: 78 of cap free
Amount of items: 2
Items: 
Size: 593531 Color: 4
Size: 406392 Color: 2

Bin 153: 78 of cap free
Amount of items: 2
Items: 
Size: 614766 Color: 2
Size: 385157 Color: 4

Bin 154: 80 of cap free
Amount of items: 2
Items: 
Size: 581305 Color: 1
Size: 418616 Color: 4

Bin 155: 80 of cap free
Amount of items: 2
Items: 
Size: 582225 Color: 2
Size: 417696 Color: 0

Bin 156: 80 of cap free
Amount of items: 2
Items: 
Size: 676717 Color: 1
Size: 323204 Color: 3

Bin 157: 80 of cap free
Amount of items: 2
Items: 
Size: 701922 Color: 2
Size: 297999 Color: 0

Bin 158: 82 of cap free
Amount of items: 3
Items: 
Size: 527344 Color: 1
Size: 341905 Color: 0
Size: 130670 Color: 4

Bin 159: 82 of cap free
Amount of items: 2
Items: 
Size: 595777 Color: 2
Size: 404142 Color: 3

Bin 160: 83 of cap free
Amount of items: 2
Items: 
Size: 501745 Color: 2
Size: 498173 Color: 4

Bin 161: 83 of cap free
Amount of items: 2
Items: 
Size: 795944 Color: 4
Size: 203974 Color: 2

Bin 162: 84 of cap free
Amount of items: 2
Items: 
Size: 512792 Color: 2
Size: 487125 Color: 4

Bin 163: 85 of cap free
Amount of items: 2
Items: 
Size: 590111 Color: 2
Size: 409805 Color: 3

Bin 164: 86 of cap free
Amount of items: 2
Items: 
Size: 521871 Color: 0
Size: 478044 Color: 1

Bin 165: 86 of cap free
Amount of items: 2
Items: 
Size: 702596 Color: 3
Size: 297319 Color: 4

Bin 166: 87 of cap free
Amount of items: 2
Items: 
Size: 630783 Color: 3
Size: 369131 Color: 1

Bin 167: 87 of cap free
Amount of items: 2
Items: 
Size: 664107 Color: 4
Size: 335807 Color: 3

Bin 168: 87 of cap free
Amount of items: 2
Items: 
Size: 725469 Color: 4
Size: 274445 Color: 2

Bin 169: 87 of cap free
Amount of items: 2
Items: 
Size: 743574 Color: 3
Size: 256340 Color: 4

Bin 170: 88 of cap free
Amount of items: 2
Items: 
Size: 660405 Color: 2
Size: 339508 Color: 4

Bin 171: 88 of cap free
Amount of items: 2
Items: 
Size: 732294 Color: 2
Size: 267619 Color: 0

Bin 172: 89 of cap free
Amount of items: 2
Items: 
Size: 757892 Color: 0
Size: 242020 Color: 4

Bin 173: 90 of cap free
Amount of items: 2
Items: 
Size: 538123 Color: 4
Size: 461788 Color: 3

Bin 174: 90 of cap free
Amount of items: 2
Items: 
Size: 720103 Color: 0
Size: 279808 Color: 3

Bin 175: 91 of cap free
Amount of items: 7
Items: 
Size: 152040 Color: 4
Size: 148808 Color: 2
Size: 148787 Color: 1
Size: 147501 Color: 4
Size: 147480 Color: 0
Size: 147319 Color: 0
Size: 107975 Color: 3

Bin 176: 91 of cap free
Amount of items: 2
Items: 
Size: 566798 Color: 4
Size: 433112 Color: 0

Bin 177: 91 of cap free
Amount of items: 2
Items: 
Size: 629418 Color: 4
Size: 370492 Color: 1

Bin 178: 91 of cap free
Amount of items: 2
Items: 
Size: 744126 Color: 3
Size: 255784 Color: 4

Bin 179: 92 of cap free
Amount of items: 2
Items: 
Size: 563781 Color: 1
Size: 436128 Color: 4

Bin 180: 92 of cap free
Amount of items: 2
Items: 
Size: 636065 Color: 2
Size: 363844 Color: 4

Bin 181: 93 of cap free
Amount of items: 2
Items: 
Size: 639301 Color: 2
Size: 360607 Color: 3

Bin 182: 93 of cap free
Amount of items: 2
Items: 
Size: 779140 Color: 2
Size: 220768 Color: 3

Bin 183: 94 of cap free
Amount of items: 2
Items: 
Size: 574792 Color: 4
Size: 425115 Color: 3

Bin 184: 95 of cap free
Amount of items: 2
Items: 
Size: 781727 Color: 1
Size: 218179 Color: 3

Bin 185: 96 of cap free
Amount of items: 2
Items: 
Size: 530419 Color: 0
Size: 469486 Color: 2

Bin 186: 96 of cap free
Amount of items: 2
Items: 
Size: 593803 Color: 3
Size: 406102 Color: 0

Bin 187: 97 of cap free
Amount of items: 2
Items: 
Size: 609210 Color: 3
Size: 390694 Color: 4

Bin 188: 97 of cap free
Amount of items: 2
Items: 
Size: 614596 Color: 0
Size: 385308 Color: 3

Bin 189: 98 of cap free
Amount of items: 2
Items: 
Size: 564905 Color: 2
Size: 434998 Color: 4

Bin 190: 98 of cap free
Amount of items: 2
Items: 
Size: 586204 Color: 0
Size: 413699 Color: 4

Bin 191: 99 of cap free
Amount of items: 2
Items: 
Size: 637212 Color: 3
Size: 362690 Color: 4

Bin 192: 99 of cap free
Amount of items: 2
Items: 
Size: 709885 Color: 0
Size: 290017 Color: 1

Bin 193: 100 of cap free
Amount of items: 2
Items: 
Size: 586808 Color: 4
Size: 413093 Color: 3

Bin 194: 100 of cap free
Amount of items: 2
Items: 
Size: 772416 Color: 0
Size: 227485 Color: 1

Bin 195: 102 of cap free
Amount of items: 2
Items: 
Size: 687783 Color: 0
Size: 312116 Color: 2

Bin 196: 102 of cap free
Amount of items: 2
Items: 
Size: 770097 Color: 2
Size: 229802 Color: 1

Bin 197: 102 of cap free
Amount of items: 2
Items: 
Size: 782493 Color: 3
Size: 217406 Color: 0

Bin 198: 105 of cap free
Amount of items: 2
Items: 
Size: 543389 Color: 3
Size: 456507 Color: 2

Bin 199: 105 of cap free
Amount of items: 2
Items: 
Size: 755084 Color: 4
Size: 244812 Color: 0

Bin 200: 107 of cap free
Amount of items: 2
Items: 
Size: 695291 Color: 4
Size: 304603 Color: 0

Bin 201: 107 of cap free
Amount of items: 2
Items: 
Size: 771273 Color: 3
Size: 228621 Color: 1

Bin 202: 108 of cap free
Amount of items: 2
Items: 
Size: 609854 Color: 2
Size: 390039 Color: 3

Bin 203: 108 of cap free
Amount of items: 2
Items: 
Size: 657049 Color: 3
Size: 342844 Color: 0

Bin 204: 110 of cap free
Amount of items: 2
Items: 
Size: 683399 Color: 2
Size: 316492 Color: 4

Bin 205: 111 of cap free
Amount of items: 2
Items: 
Size: 554449 Color: 1
Size: 445441 Color: 4

Bin 206: 112 of cap free
Amount of items: 2
Items: 
Size: 502924 Color: 4
Size: 496965 Color: 0

Bin 207: 113 of cap free
Amount of items: 2
Items: 
Size: 548283 Color: 3
Size: 451605 Color: 0

Bin 208: 114 of cap free
Amount of items: 2
Items: 
Size: 647231 Color: 2
Size: 352656 Color: 4

Bin 209: 117 of cap free
Amount of items: 2
Items: 
Size: 784438 Color: 1
Size: 215446 Color: 0

Bin 210: 119 of cap free
Amount of items: 2
Items: 
Size: 796442 Color: 1
Size: 203440 Color: 4

Bin 211: 120 of cap free
Amount of items: 2
Items: 
Size: 569549 Color: 0
Size: 430332 Color: 2

Bin 212: 120 of cap free
Amount of items: 2
Items: 
Size: 763890 Color: 4
Size: 235991 Color: 2

Bin 213: 122 of cap free
Amount of items: 2
Items: 
Size: 593055 Color: 0
Size: 406824 Color: 4

Bin 214: 122 of cap free
Amount of items: 2
Items: 
Size: 757096 Color: 3
Size: 242783 Color: 2

Bin 215: 124 of cap free
Amount of items: 2
Items: 
Size: 765897 Color: 0
Size: 233980 Color: 4

Bin 216: 125 of cap free
Amount of items: 2
Items: 
Size: 602810 Color: 4
Size: 397066 Color: 1

Bin 217: 125 of cap free
Amount of items: 2
Items: 
Size: 621351 Color: 4
Size: 378525 Color: 3

Bin 218: 126 of cap free
Amount of items: 2
Items: 
Size: 710935 Color: 1
Size: 288940 Color: 2

Bin 219: 128 of cap free
Amount of items: 2
Items: 
Size: 656353 Color: 1
Size: 343520 Color: 4

Bin 220: 128 of cap free
Amount of items: 2
Items: 
Size: 770326 Color: 0
Size: 229547 Color: 2

Bin 221: 131 of cap free
Amount of items: 2
Items: 
Size: 645973 Color: 3
Size: 353897 Color: 2

Bin 222: 131 of cap free
Amount of items: 2
Items: 
Size: 660706 Color: 0
Size: 339164 Color: 4

Bin 223: 131 of cap free
Amount of items: 2
Items: 
Size: 679987 Color: 2
Size: 319883 Color: 4

Bin 224: 134 of cap free
Amount of items: 2
Items: 
Size: 568752 Color: 4
Size: 431115 Color: 2

Bin 225: 135 of cap free
Amount of items: 2
Items: 
Size: 573215 Color: 0
Size: 426651 Color: 3

Bin 226: 135 of cap free
Amount of items: 2
Items: 
Size: 756306 Color: 0
Size: 243560 Color: 2

Bin 227: 136 of cap free
Amount of items: 2
Items: 
Size: 599498 Color: 2
Size: 400367 Color: 3

Bin 228: 137 of cap free
Amount of items: 2
Items: 
Size: 567772 Color: 4
Size: 432092 Color: 3

Bin 229: 137 of cap free
Amount of items: 2
Items: 
Size: 775989 Color: 2
Size: 223875 Color: 0

Bin 230: 138 of cap free
Amount of items: 2
Items: 
Size: 504035 Color: 1
Size: 495828 Color: 4

Bin 231: 138 of cap free
Amount of items: 2
Items: 
Size: 554567 Color: 3
Size: 445296 Color: 2

Bin 232: 138 of cap free
Amount of items: 2
Items: 
Size: 631867 Color: 2
Size: 367996 Color: 0

Bin 233: 139 of cap free
Amount of items: 2
Items: 
Size: 778529 Color: 0
Size: 221333 Color: 4

Bin 234: 142 of cap free
Amount of items: 2
Items: 
Size: 673075 Color: 1
Size: 326784 Color: 2

Bin 235: 143 of cap free
Amount of items: 2
Items: 
Size: 608717 Color: 0
Size: 391141 Color: 2

Bin 236: 143 of cap free
Amount of items: 2
Items: 
Size: 688644 Color: 1
Size: 311214 Color: 3

Bin 237: 144 of cap free
Amount of items: 2
Items: 
Size: 570751 Color: 0
Size: 429106 Color: 2

Bin 238: 146 of cap free
Amount of items: 5
Items: 
Size: 209751 Color: 4
Size: 209702 Color: 0
Size: 207798 Color: 1
Size: 206800 Color: 3
Size: 165804 Color: 1

Bin 239: 146 of cap free
Amount of items: 2
Items: 
Size: 685063 Color: 0
Size: 314792 Color: 4

Bin 240: 147 of cap free
Amount of items: 2
Items: 
Size: 631863 Color: 2
Size: 367991 Color: 3

Bin 241: 148 of cap free
Amount of items: 2
Items: 
Size: 713060 Color: 4
Size: 286793 Color: 0

Bin 242: 149 of cap free
Amount of items: 2
Items: 
Size: 522547 Color: 0
Size: 477305 Color: 4

Bin 243: 150 of cap free
Amount of items: 2
Items: 
Size: 700937 Color: 1
Size: 298914 Color: 2

Bin 244: 151 of cap free
Amount of items: 2
Items: 
Size: 585742 Color: 2
Size: 414108 Color: 4

Bin 245: 151 of cap free
Amount of items: 2
Items: 
Size: 592220 Color: 4
Size: 407630 Color: 2

Bin 246: 151 of cap free
Amount of items: 2
Items: 
Size: 684106 Color: 1
Size: 315744 Color: 0

Bin 247: 154 of cap free
Amount of items: 2
Items: 
Size: 502247 Color: 1
Size: 497600 Color: 4

Bin 248: 154 of cap free
Amount of items: 2
Items: 
Size: 625403 Color: 2
Size: 374444 Color: 0

Bin 249: 160 of cap free
Amount of items: 2
Items: 
Size: 618617 Color: 2
Size: 381224 Color: 1

Bin 250: 166 of cap free
Amount of items: 2
Items: 
Size: 633434 Color: 0
Size: 366401 Color: 4

Bin 251: 167 of cap free
Amount of items: 2
Items: 
Size: 709465 Color: 0
Size: 290369 Color: 3

Bin 252: 168 of cap free
Amount of items: 2
Items: 
Size: 797158 Color: 2
Size: 202675 Color: 0

Bin 253: 169 of cap free
Amount of items: 2
Items: 
Size: 736385 Color: 2
Size: 263447 Color: 1

Bin 254: 174 of cap free
Amount of items: 2
Items: 
Size: 527953 Color: 2
Size: 471874 Color: 0

Bin 255: 174 of cap free
Amount of items: 2
Items: 
Size: 640648 Color: 3
Size: 359179 Color: 0

Bin 256: 174 of cap free
Amount of items: 2
Items: 
Size: 758589 Color: 1
Size: 241238 Color: 4

Bin 257: 175 of cap free
Amount of items: 2
Items: 
Size: 726216 Color: 4
Size: 273610 Color: 2

Bin 258: 175 of cap free
Amount of items: 2
Items: 
Size: 750625 Color: 3
Size: 249201 Color: 0

Bin 259: 176 of cap free
Amount of items: 2
Items: 
Size: 537319 Color: 0
Size: 462506 Color: 2

Bin 260: 176 of cap free
Amount of items: 2
Items: 
Size: 639031 Color: 2
Size: 360794 Color: 3

Bin 261: 176 of cap free
Amount of items: 2
Items: 
Size: 665238 Color: 4
Size: 334587 Color: 3

Bin 262: 179 of cap free
Amount of items: 2
Items: 
Size: 654319 Color: 0
Size: 345503 Color: 3

Bin 263: 180 of cap free
Amount of items: 2
Items: 
Size: 546420 Color: 2
Size: 453401 Color: 4

Bin 264: 180 of cap free
Amount of items: 2
Items: 
Size: 611899 Color: 4
Size: 387922 Color: 0

Bin 265: 182 of cap free
Amount of items: 2
Items: 
Size: 612755 Color: 0
Size: 387064 Color: 4

Bin 266: 183 of cap free
Amount of items: 2
Items: 
Size: 740435 Color: 0
Size: 259383 Color: 4

Bin 267: 183 of cap free
Amount of items: 2
Items: 
Size: 745695 Color: 1
Size: 254123 Color: 3

Bin 268: 185 of cap free
Amount of items: 2
Items: 
Size: 799440 Color: 1
Size: 200376 Color: 3

Bin 269: 187 of cap free
Amount of items: 2
Items: 
Size: 733382 Color: 4
Size: 266432 Color: 0

Bin 270: 187 of cap free
Amount of items: 2
Items: 
Size: 767895 Color: 2
Size: 231919 Color: 4

Bin 271: 188 of cap free
Amount of items: 3
Items: 
Size: 474826 Color: 0
Size: 339113 Color: 1
Size: 185874 Color: 1

Bin 272: 188 of cap free
Amount of items: 2
Items: 
Size: 618134 Color: 1
Size: 381679 Color: 0

Bin 273: 188 of cap free
Amount of items: 2
Items: 
Size: 752853 Color: 1
Size: 246960 Color: 2

Bin 274: 189 of cap free
Amount of items: 8
Items: 
Size: 130469 Color: 1
Size: 129092 Color: 2
Size: 128918 Color: 0
Size: 127357 Color: 1
Size: 127079 Color: 1
Size: 126583 Color: 3
Size: 125684 Color: 3
Size: 104630 Color: 4

Bin 275: 191 of cap free
Amount of items: 2
Items: 
Size: 691635 Color: 4
Size: 308175 Color: 2

Bin 276: 191 of cap free
Amount of items: 2
Items: 
Size: 712495 Color: 3
Size: 287315 Color: 1

Bin 277: 193 of cap free
Amount of items: 2
Items: 
Size: 596175 Color: 2
Size: 403633 Color: 3

Bin 278: 196 of cap free
Amount of items: 2
Items: 
Size: 536469 Color: 1
Size: 463336 Color: 3

Bin 279: 196 of cap free
Amount of items: 2
Items: 
Size: 788326 Color: 0
Size: 211479 Color: 4

Bin 280: 197 of cap free
Amount of items: 2
Items: 
Size: 599739 Color: 3
Size: 400065 Color: 0

Bin 281: 197 of cap free
Amount of items: 2
Items: 
Size: 664046 Color: 4
Size: 335758 Color: 0

Bin 282: 198 of cap free
Amount of items: 2
Items: 
Size: 639452 Color: 4
Size: 360351 Color: 2

Bin 283: 202 of cap free
Amount of items: 2
Items: 
Size: 624738 Color: 3
Size: 375061 Color: 0

Bin 284: 206 of cap free
Amount of items: 2
Items: 
Size: 598883 Color: 1
Size: 400912 Color: 3

Bin 285: 207 of cap free
Amount of items: 2
Items: 
Size: 701815 Color: 3
Size: 297979 Color: 4

Bin 286: 209 of cap free
Amount of items: 2
Items: 
Size: 607796 Color: 3
Size: 391996 Color: 4

Bin 287: 209 of cap free
Amount of items: 2
Items: 
Size: 647754 Color: 3
Size: 352038 Color: 2

Bin 288: 209 of cap free
Amount of items: 2
Items: 
Size: 676271 Color: 1
Size: 323521 Color: 0

Bin 289: 209 of cap free
Amount of items: 2
Items: 
Size: 778301 Color: 1
Size: 221491 Color: 4

Bin 290: 210 of cap free
Amount of items: 2
Items: 
Size: 715721 Color: 2
Size: 284070 Color: 3

Bin 291: 213 of cap free
Amount of items: 2
Items: 
Size: 734352 Color: 0
Size: 265436 Color: 3

Bin 292: 214 of cap free
Amount of items: 7
Items: 
Size: 147119 Color: 2
Size: 146938 Color: 4
Size: 146793 Color: 4
Size: 145591 Color: 2
Size: 145265 Color: 4
Size: 145093 Color: 1
Size: 122988 Color: 2

Bin 293: 214 of cap free
Amount of items: 2
Items: 
Size: 738046 Color: 0
Size: 261741 Color: 1

Bin 294: 216 of cap free
Amount of items: 2
Items: 
Size: 665853 Color: 2
Size: 333932 Color: 4

Bin 295: 216 of cap free
Amount of items: 2
Items: 
Size: 795688 Color: 2
Size: 204097 Color: 1

Bin 296: 219 of cap free
Amount of items: 2
Items: 
Size: 547751 Color: 0
Size: 452031 Color: 1

Bin 297: 219 of cap free
Amount of items: 2
Items: 
Size: 563738 Color: 1
Size: 436044 Color: 3

Bin 298: 220 of cap free
Amount of items: 2
Items: 
Size: 593266 Color: 0
Size: 406515 Color: 4

Bin 299: 222 of cap free
Amount of items: 2
Items: 
Size: 697252 Color: 2
Size: 302527 Color: 4

Bin 300: 224 of cap free
Amount of items: 2
Items: 
Size: 685032 Color: 0
Size: 314745 Color: 3

Bin 301: 226 of cap free
Amount of items: 2
Items: 
Size: 506939 Color: 4
Size: 492836 Color: 0

Bin 302: 226 of cap free
Amount of items: 2
Items: 
Size: 610092 Color: 2
Size: 389683 Color: 3

Bin 303: 227 of cap free
Amount of items: 2
Items: 
Size: 702809 Color: 2
Size: 296965 Color: 3

Bin 304: 229 of cap free
Amount of items: 2
Items: 
Size: 792430 Color: 3
Size: 207342 Color: 1

Bin 305: 231 of cap free
Amount of items: 2
Items: 
Size: 597733 Color: 1
Size: 402037 Color: 4

Bin 306: 232 of cap free
Amount of items: 2
Items: 
Size: 655968 Color: 0
Size: 343801 Color: 1

Bin 307: 233 of cap free
Amount of items: 2
Items: 
Size: 607543 Color: 4
Size: 392225 Color: 2

Bin 308: 235 of cap free
Amount of items: 2
Items: 
Size: 707190 Color: 3
Size: 292576 Color: 0

Bin 309: 236 of cap free
Amount of items: 2
Items: 
Size: 587517 Color: 0
Size: 412248 Color: 1

Bin 310: 236 of cap free
Amount of items: 2
Items: 
Size: 594906 Color: 2
Size: 404859 Color: 4

Bin 311: 238 of cap free
Amount of items: 2
Items: 
Size: 683343 Color: 1
Size: 316420 Color: 4

Bin 312: 240 of cap free
Amount of items: 2
Items: 
Size: 753346 Color: 4
Size: 246415 Color: 3

Bin 313: 241 of cap free
Amount of items: 2
Items: 
Size: 756826 Color: 4
Size: 242934 Color: 2

Bin 314: 242 of cap free
Amount of items: 2
Items: 
Size: 623463 Color: 0
Size: 376296 Color: 1

Bin 315: 244 of cap free
Amount of items: 2
Items: 
Size: 565035 Color: 4
Size: 434722 Color: 2

Bin 316: 246 of cap free
Amount of items: 2
Items: 
Size: 533437 Color: 3
Size: 466318 Color: 1

Bin 317: 247 of cap free
Amount of items: 6
Items: 
Size: 178148 Color: 4
Size: 177535 Color: 0
Size: 177094 Color: 2
Size: 176251 Color: 1
Size: 175775 Color: 1
Size: 114951 Color: 4

Bin 318: 247 of cap free
Amount of items: 2
Items: 
Size: 780609 Color: 3
Size: 219145 Color: 2

Bin 319: 249 of cap free
Amount of items: 2
Items: 
Size: 659810 Color: 3
Size: 339942 Color: 0

Bin 320: 254 of cap free
Amount of items: 6
Items: 
Size: 172446 Color: 2
Size: 171513 Color: 4
Size: 171482 Color: 4
Size: 171245 Color: 2
Size: 171188 Color: 4
Size: 141873 Color: 3

Bin 321: 254 of cap free
Amount of items: 2
Items: 
Size: 574399 Color: 2
Size: 425348 Color: 1

Bin 322: 255 of cap free
Amount of items: 2
Items: 
Size: 531561 Color: 2
Size: 468185 Color: 0

Bin 323: 255 of cap free
Amount of items: 2
Items: 
Size: 650754 Color: 1
Size: 348992 Color: 3

Bin 324: 257 of cap free
Amount of items: 2
Items: 
Size: 542335 Color: 3
Size: 457409 Color: 4

Bin 325: 258 of cap free
Amount of items: 2
Items: 
Size: 517884 Color: 1
Size: 481859 Color: 4

Bin 326: 258 of cap free
Amount of items: 2
Items: 
Size: 695000 Color: 3
Size: 304743 Color: 2

Bin 327: 259 of cap free
Amount of items: 2
Items: 
Size: 659002 Color: 0
Size: 340740 Color: 3

Bin 328: 261 of cap free
Amount of items: 2
Items: 
Size: 509495 Color: 1
Size: 490245 Color: 0

Bin 329: 262 of cap free
Amount of items: 2
Items: 
Size: 575996 Color: 3
Size: 423743 Color: 1

Bin 330: 263 of cap free
Amount of items: 2
Items: 
Size: 548148 Color: 2
Size: 451590 Color: 1

Bin 331: 263 of cap free
Amount of items: 2
Items: 
Size: 555684 Color: 1
Size: 444054 Color: 0

Bin 332: 263 of cap free
Amount of items: 2
Items: 
Size: 648492 Color: 1
Size: 351246 Color: 2

Bin 333: 264 of cap free
Amount of items: 2
Items: 
Size: 708657 Color: 2
Size: 291080 Color: 4

Bin 334: 267 of cap free
Amount of items: 2
Items: 
Size: 578690 Color: 0
Size: 421044 Color: 1

Bin 335: 281 of cap free
Amount of items: 6
Items: 
Size: 168644 Color: 4
Size: 168145 Color: 1
Size: 167615 Color: 2
Size: 167431 Color: 2
Size: 166895 Color: 1
Size: 160990 Color: 2

Bin 336: 282 of cap free
Amount of items: 2
Items: 
Size: 770228 Color: 4
Size: 229491 Color: 3

Bin 337: 283 of cap free
Amount of items: 2
Items: 
Size: 621006 Color: 2
Size: 378712 Color: 4

Bin 338: 286 of cap free
Amount of items: 2
Items: 
Size: 741569 Color: 1
Size: 258146 Color: 2

Bin 339: 287 of cap free
Amount of items: 2
Items: 
Size: 668638 Color: 3
Size: 331076 Color: 0

Bin 340: 287 of cap free
Amount of items: 2
Items: 
Size: 713163 Color: 0
Size: 286551 Color: 1

Bin 341: 288 of cap free
Amount of items: 2
Items: 
Size: 541361 Color: 2
Size: 458352 Color: 3

Bin 342: 291 of cap free
Amount of items: 2
Items: 
Size: 582367 Color: 3
Size: 417343 Color: 0

Bin 343: 291 of cap free
Amount of items: 2
Items: 
Size: 736953 Color: 4
Size: 262757 Color: 0

Bin 344: 294 of cap free
Amount of items: 2
Items: 
Size: 768265 Color: 4
Size: 231442 Color: 2

Bin 345: 297 of cap free
Amount of items: 2
Items: 
Size: 519817 Color: 1
Size: 479887 Color: 3

Bin 346: 299 of cap free
Amount of items: 2
Items: 
Size: 684602 Color: 0
Size: 315100 Color: 4

Bin 347: 299 of cap free
Amount of items: 2
Items: 
Size: 711967 Color: 2
Size: 287735 Color: 0

Bin 348: 300 of cap free
Amount of items: 2
Items: 
Size: 736555 Color: 2
Size: 263146 Color: 4

Bin 349: 301 of cap free
Amount of items: 2
Items: 
Size: 530570 Color: 4
Size: 469130 Color: 1

Bin 350: 301 of cap free
Amount of items: 2
Items: 
Size: 747601 Color: 0
Size: 252099 Color: 4

Bin 351: 307 of cap free
Amount of items: 2
Items: 
Size: 636955 Color: 2
Size: 362739 Color: 3

Bin 352: 309 of cap free
Amount of items: 2
Items: 
Size: 760771 Color: 3
Size: 238921 Color: 4

Bin 353: 310 of cap free
Amount of items: 2
Items: 
Size: 738643 Color: 3
Size: 261048 Color: 1

Bin 354: 311 of cap free
Amount of items: 2
Items: 
Size: 556343 Color: 3
Size: 443347 Color: 1

Bin 355: 311 of cap free
Amount of items: 2
Items: 
Size: 673310 Color: 2
Size: 326380 Color: 0

Bin 356: 314 of cap free
Amount of items: 2
Items: 
Size: 638490 Color: 0
Size: 361197 Color: 3

Bin 357: 315 of cap free
Amount of items: 2
Items: 
Size: 635538 Color: 2
Size: 364148 Color: 1

Bin 358: 321 of cap free
Amount of items: 2
Items: 
Size: 764869 Color: 3
Size: 234811 Color: 4

Bin 359: 323 of cap free
Amount of items: 2
Items: 
Size: 784564 Color: 1
Size: 215114 Color: 0

Bin 360: 325 of cap free
Amount of items: 2
Items: 
Size: 516608 Color: 3
Size: 483068 Color: 1

Bin 361: 325 of cap free
Amount of items: 2
Items: 
Size: 608327 Color: 2
Size: 391349 Color: 3

Bin 362: 327 of cap free
Amount of items: 2
Items: 
Size: 603701 Color: 3
Size: 395973 Color: 4

Bin 363: 329 of cap free
Amount of items: 2
Items: 
Size: 520371 Color: 0
Size: 479301 Color: 2

Bin 364: 329 of cap free
Amount of items: 2
Items: 
Size: 606620 Color: 0
Size: 393052 Color: 1

Bin 365: 329 of cap free
Amount of items: 2
Items: 
Size: 704523 Color: 2
Size: 295149 Color: 3

Bin 366: 330 of cap free
Amount of items: 2
Items: 
Size: 741906 Color: 2
Size: 257765 Color: 1

Bin 367: 332 of cap free
Amount of items: 2
Items: 
Size: 758208 Color: 0
Size: 241461 Color: 1

Bin 368: 333 of cap free
Amount of items: 2
Items: 
Size: 537655 Color: 2
Size: 462013 Color: 4

Bin 369: 333 of cap free
Amount of items: 2
Items: 
Size: 796804 Color: 1
Size: 202864 Color: 0

Bin 370: 336 of cap free
Amount of items: 2
Items: 
Size: 504312 Color: 4
Size: 495353 Color: 3

Bin 371: 337 of cap free
Amount of items: 2
Items: 
Size: 691068 Color: 1
Size: 308596 Color: 3

Bin 372: 338 of cap free
Amount of items: 2
Items: 
Size: 594078 Color: 4
Size: 405585 Color: 0

Bin 373: 338 of cap free
Amount of items: 2
Items: 
Size: 772270 Color: 0
Size: 227393 Color: 1

Bin 374: 339 of cap free
Amount of items: 2
Items: 
Size: 613826 Color: 2
Size: 385836 Color: 0

Bin 375: 342 of cap free
Amount of items: 2
Items: 
Size: 778588 Color: 4
Size: 221071 Color: 1

Bin 376: 346 of cap free
Amount of items: 7
Items: 
Size: 147074 Color: 2
Size: 144669 Color: 1
Size: 144226 Color: 0
Size: 143833 Color: 0
Size: 143366 Color: 4
Size: 142319 Color: 1
Size: 134168 Color: 4

Bin 377: 346 of cap free
Amount of items: 2
Items: 
Size: 762923 Color: 4
Size: 236732 Color: 1

Bin 378: 347 of cap free
Amount of items: 2
Items: 
Size: 767034 Color: 0
Size: 232620 Color: 3

Bin 379: 349 of cap free
Amount of items: 2
Items: 
Size: 504908 Color: 1
Size: 494744 Color: 2

Bin 380: 351 of cap free
Amount of items: 2
Items: 
Size: 730688 Color: 1
Size: 268962 Color: 2

Bin 381: 353 of cap free
Amount of items: 2
Items: 
Size: 679807 Color: 1
Size: 319841 Color: 4

Bin 382: 354 of cap free
Amount of items: 2
Items: 
Size: 626918 Color: 3
Size: 372729 Color: 1

Bin 383: 355 of cap free
Amount of items: 2
Items: 
Size: 529012 Color: 0
Size: 470634 Color: 3

Bin 384: 356 of cap free
Amount of items: 2
Items: 
Size: 681325 Color: 1
Size: 318320 Color: 4

Bin 385: 361 of cap free
Amount of items: 2
Items: 
Size: 667673 Color: 4
Size: 331967 Color: 2

Bin 386: 363 of cap free
Amount of items: 2
Items: 
Size: 519411 Color: 4
Size: 480227 Color: 3

Bin 387: 365 of cap free
Amount of items: 2
Items: 
Size: 724499 Color: 4
Size: 275137 Color: 0

Bin 388: 366 of cap free
Amount of items: 2
Items: 
Size: 796206 Color: 2
Size: 203429 Color: 0

Bin 389: 369 of cap free
Amount of items: 2
Items: 
Size: 766354 Color: 4
Size: 233278 Color: 1

Bin 390: 373 of cap free
Amount of items: 2
Items: 
Size: 761512 Color: 2
Size: 238116 Color: 3

Bin 391: 375 of cap free
Amount of items: 2
Items: 
Size: 512400 Color: 3
Size: 487226 Color: 0

Bin 392: 379 of cap free
Amount of items: 2
Items: 
Size: 752941 Color: 2
Size: 246681 Color: 4

Bin 393: 381 of cap free
Amount of items: 2
Items: 
Size: 780556 Color: 1
Size: 219064 Color: 3

Bin 394: 382 of cap free
Amount of items: 2
Items: 
Size: 779461 Color: 0
Size: 220158 Color: 4

Bin 395: 384 of cap free
Amount of items: 2
Items: 
Size: 547713 Color: 4
Size: 451904 Color: 3

Bin 396: 384 of cap free
Amount of items: 2
Items: 
Size: 711237 Color: 2
Size: 288380 Color: 4

Bin 397: 385 of cap free
Amount of items: 2
Items: 
Size: 663533 Color: 0
Size: 336083 Color: 1

Bin 398: 386 of cap free
Amount of items: 2
Items: 
Size: 658889 Color: 0
Size: 340726 Color: 4

Bin 399: 386 of cap free
Amount of items: 2
Items: 
Size: 696310 Color: 4
Size: 303305 Color: 2

Bin 400: 386 of cap free
Amount of items: 2
Items: 
Size: 724976 Color: 1
Size: 274639 Color: 2

Bin 401: 386 of cap free
Amount of items: 2
Items: 
Size: 798176 Color: 2
Size: 201439 Color: 4

Bin 402: 387 of cap free
Amount of items: 2
Items: 
Size: 571135 Color: 4
Size: 428479 Color: 0

Bin 403: 388 of cap free
Amount of items: 2
Items: 
Size: 681326 Color: 4
Size: 318287 Color: 0

Bin 404: 388 of cap free
Amount of items: 2
Items: 
Size: 706290 Color: 2
Size: 293323 Color: 1

Bin 405: 392 of cap free
Amount of items: 2
Items: 
Size: 577419 Color: 3
Size: 422190 Color: 2

Bin 406: 396 of cap free
Amount of items: 2
Items: 
Size: 715661 Color: 2
Size: 283944 Color: 3

Bin 407: 396 of cap free
Amount of items: 2
Items: 
Size: 717166 Color: 2
Size: 282439 Color: 1

Bin 408: 397 of cap free
Amount of items: 2
Items: 
Size: 539666 Color: 1
Size: 459938 Color: 4

Bin 409: 399 of cap free
Amount of items: 2
Items: 
Size: 730983 Color: 2
Size: 268619 Color: 3

Bin 410: 400 of cap free
Amount of items: 2
Items: 
Size: 772267 Color: 2
Size: 227334 Color: 4

Bin 411: 402 of cap free
Amount of items: 2
Items: 
Size: 690058 Color: 1
Size: 309541 Color: 2

Bin 412: 403 of cap free
Amount of items: 2
Items: 
Size: 540952 Color: 1
Size: 458646 Color: 4

Bin 413: 406 of cap free
Amount of items: 2
Items: 
Size: 531058 Color: 2
Size: 468537 Color: 0

Bin 414: 407 of cap free
Amount of items: 2
Items: 
Size: 558127 Color: 2
Size: 441467 Color: 0

Bin 415: 407 of cap free
Amount of items: 2
Items: 
Size: 781969 Color: 4
Size: 217625 Color: 2

Bin 416: 411 of cap free
Amount of items: 2
Items: 
Size: 583385 Color: 1
Size: 416205 Color: 4

Bin 417: 414 of cap free
Amount of items: 2
Items: 
Size: 548927 Color: 1
Size: 450660 Color: 4

Bin 418: 414 of cap free
Amount of items: 2
Items: 
Size: 796664 Color: 4
Size: 202923 Color: 1

Bin 419: 415 of cap free
Amount of items: 2
Items: 
Size: 546335 Color: 1
Size: 453251 Color: 4

Bin 420: 416 of cap free
Amount of items: 2
Items: 
Size: 568478 Color: 2
Size: 431107 Color: 1

Bin 421: 416 of cap free
Amount of items: 2
Items: 
Size: 581566 Color: 1
Size: 418019 Color: 4

Bin 422: 416 of cap free
Amount of items: 2
Items: 
Size: 744690 Color: 4
Size: 254895 Color: 2

Bin 423: 417 of cap free
Amount of items: 2
Items: 
Size: 586976 Color: 2
Size: 412608 Color: 3

Bin 424: 422 of cap free
Amount of items: 2
Items: 
Size: 683201 Color: 4
Size: 316378 Color: 1

Bin 425: 425 of cap free
Amount of items: 2
Items: 
Size: 663571 Color: 1
Size: 336005 Color: 0

Bin 426: 425 of cap free
Amount of items: 2
Items: 
Size: 773282 Color: 3
Size: 226294 Color: 4

Bin 427: 427 of cap free
Amount of items: 2
Items: 
Size: 592588 Color: 1
Size: 406986 Color: 4

Bin 428: 427 of cap free
Amount of items: 2
Items: 
Size: 719956 Color: 0
Size: 279618 Color: 1

Bin 429: 428 of cap free
Amount of items: 2
Items: 
Size: 575942 Color: 3
Size: 423631 Color: 1

Bin 430: 428 of cap free
Amount of items: 2
Items: 
Size: 664325 Color: 2
Size: 335248 Color: 0

Bin 431: 433 of cap free
Amount of items: 2
Items: 
Size: 539729 Color: 4
Size: 459839 Color: 0

Bin 432: 434 of cap free
Amount of items: 2
Items: 
Size: 735495 Color: 3
Size: 264072 Color: 0

Bin 433: 436 of cap free
Amount of items: 2
Items: 
Size: 645956 Color: 2
Size: 353609 Color: 4

Bin 434: 437 of cap free
Amount of items: 2
Items: 
Size: 515652 Color: 1
Size: 483912 Color: 4

Bin 435: 437 of cap free
Amount of items: 2
Items: 
Size: 720522 Color: 1
Size: 279042 Color: 4

Bin 436: 440 of cap free
Amount of items: 2
Items: 
Size: 629583 Color: 1
Size: 369978 Color: 2

Bin 437: 442 of cap free
Amount of items: 2
Items: 
Size: 548147 Color: 1
Size: 451412 Color: 0

Bin 438: 442 of cap free
Amount of items: 2
Items: 
Size: 732577 Color: 2
Size: 266982 Color: 3

Bin 439: 446 of cap free
Amount of items: 2
Items: 
Size: 542599 Color: 0
Size: 456956 Color: 2

Bin 440: 451 of cap free
Amount of items: 2
Items: 
Size: 630691 Color: 2
Size: 368859 Color: 4

Bin 441: 452 of cap free
Amount of items: 2
Items: 
Size: 678135 Color: 1
Size: 321414 Color: 0

Bin 442: 453 of cap free
Amount of items: 2
Items: 
Size: 652644 Color: 4
Size: 346904 Color: 0

Bin 443: 454 of cap free
Amount of items: 2
Items: 
Size: 684914 Color: 1
Size: 314633 Color: 2

Bin 444: 456 of cap free
Amount of items: 2
Items: 
Size: 722085 Color: 1
Size: 277460 Color: 2

Bin 445: 457 of cap free
Amount of items: 2
Items: 
Size: 617602 Color: 4
Size: 381942 Color: 2

Bin 446: 458 of cap free
Amount of items: 2
Items: 
Size: 694027 Color: 3
Size: 305516 Color: 4

Bin 447: 458 of cap free
Amount of items: 2
Items: 
Size: 728111 Color: 1
Size: 271432 Color: 0

Bin 448: 459 of cap free
Amount of items: 2
Items: 
Size: 516578 Color: 3
Size: 482964 Color: 0

Bin 449: 470 of cap free
Amount of items: 2
Items: 
Size: 727132 Color: 2
Size: 272399 Color: 3

Bin 450: 471 of cap free
Amount of items: 2
Items: 
Size: 636462 Color: 0
Size: 363068 Color: 4

Bin 451: 473 of cap free
Amount of items: 2
Items: 
Size: 766952 Color: 0
Size: 232576 Color: 2

Bin 452: 482 of cap free
Amount of items: 2
Items: 
Size: 759357 Color: 1
Size: 240162 Color: 4

Bin 453: 491 of cap free
Amount of items: 2
Items: 
Size: 588133 Color: 2
Size: 411377 Color: 4

Bin 454: 493 of cap free
Amount of items: 2
Items: 
Size: 748034 Color: 4
Size: 251474 Color: 3

Bin 455: 494 of cap free
Amount of items: 2
Items: 
Size: 533485 Color: 1
Size: 466022 Color: 4

Bin 456: 497 of cap free
Amount of items: 2
Items: 
Size: 734198 Color: 1
Size: 265306 Color: 0

Bin 457: 499 of cap free
Amount of items: 2
Items: 
Size: 506709 Color: 1
Size: 492793 Color: 4

Bin 458: 503 of cap free
Amount of items: 6
Items: 
Size: 178893 Color: 4
Size: 178844 Color: 1
Size: 178445 Color: 4
Size: 177842 Color: 2
Size: 177619 Color: 2
Size: 107855 Color: 1

Bin 459: 511 of cap free
Amount of items: 2
Items: 
Size: 529742 Color: 1
Size: 469748 Color: 4

Bin 460: 524 of cap free
Amount of items: 2
Items: 
Size: 575598 Color: 0
Size: 423879 Color: 3

Bin 461: 526 of cap free
Amount of items: 2
Items: 
Size: 600770 Color: 1
Size: 398705 Color: 0

Bin 462: 529 of cap free
Amount of items: 2
Items: 
Size: 635116 Color: 4
Size: 364356 Color: 2

Bin 463: 532 of cap free
Amount of items: 2
Items: 
Size: 669054 Color: 3
Size: 330415 Color: 4

Bin 464: 532 of cap free
Amount of items: 2
Items: 
Size: 687137 Color: 2
Size: 312332 Color: 4

Bin 465: 537 of cap free
Amount of items: 2
Items: 
Size: 615797 Color: 2
Size: 383667 Color: 3

Bin 466: 537 of cap free
Amount of items: 2
Items: 
Size: 781899 Color: 4
Size: 217565 Color: 3

Bin 467: 540 of cap free
Amount of items: 2
Items: 
Size: 773227 Color: 3
Size: 226234 Color: 1

Bin 468: 543 of cap free
Amount of items: 2
Items: 
Size: 567550 Color: 4
Size: 431908 Color: 2

Bin 469: 546 of cap free
Amount of items: 2
Items: 
Size: 745623 Color: 0
Size: 253832 Color: 2

Bin 470: 554 of cap free
Amount of items: 2
Items: 
Size: 568387 Color: 3
Size: 431060 Color: 4

Bin 471: 555 of cap free
Amount of items: 2
Items: 
Size: 546268 Color: 4
Size: 453178 Color: 0

Bin 472: 555 of cap free
Amount of items: 2
Items: 
Size: 734995 Color: 4
Size: 264451 Color: 3

Bin 473: 557 of cap free
Amount of items: 2
Items: 
Size: 588368 Color: 4
Size: 411076 Color: 3

Bin 474: 558 of cap free
Amount of items: 2
Items: 
Size: 589065 Color: 2
Size: 410378 Color: 3

Bin 475: 567 of cap free
Amount of items: 2
Items: 
Size: 719557 Color: 3
Size: 279877 Color: 0

Bin 476: 568 of cap free
Amount of items: 2
Items: 
Size: 604934 Color: 0
Size: 394499 Color: 3

Bin 477: 569 of cap free
Amount of items: 2
Items: 
Size: 584722 Color: 0
Size: 414710 Color: 3

Bin 478: 571 of cap free
Amount of items: 2
Items: 
Size: 645907 Color: 4
Size: 353523 Color: 0

Bin 479: 579 of cap free
Amount of items: 2
Items: 
Size: 602435 Color: 3
Size: 396987 Color: 4

Bin 480: 581 of cap free
Amount of items: 2
Items: 
Size: 731856 Color: 0
Size: 267564 Color: 4

Bin 481: 586 of cap free
Amount of items: 2
Items: 
Size: 658704 Color: 1
Size: 340711 Color: 0

Bin 482: 586 of cap free
Amount of items: 2
Items: 
Size: 667199 Color: 1
Size: 332216 Color: 4

Bin 483: 588 of cap free
Amount of items: 2
Items: 
Size: 628785 Color: 1
Size: 370628 Color: 3

Bin 484: 589 of cap free
Amount of items: 2
Items: 
Size: 723861 Color: 2
Size: 275551 Color: 3

Bin 485: 593 of cap free
Amount of items: 2
Items: 
Size: 730273 Color: 3
Size: 269135 Color: 1

Bin 486: 600 of cap free
Amount of items: 2
Items: 
Size: 556275 Color: 4
Size: 443126 Color: 3

Bin 487: 602 of cap free
Amount of items: 2
Items: 
Size: 649721 Color: 2
Size: 349678 Color: 3

Bin 488: 603 of cap free
Amount of items: 2
Items: 
Size: 728040 Color: 1
Size: 271358 Color: 2

Bin 489: 613 of cap free
Amount of items: 2
Items: 
Size: 583313 Color: 3
Size: 416075 Color: 2

Bin 490: 613 of cap free
Amount of items: 2
Items: 
Size: 622232 Color: 1
Size: 377156 Color: 2

Bin 491: 613 of cap free
Amount of items: 2
Items: 
Size: 727062 Color: 2
Size: 272326 Color: 1

Bin 492: 618 of cap free
Amount of items: 3
Items: 
Size: 525717 Color: 1
Size: 339576 Color: 2
Size: 134090 Color: 3

Bin 493: 618 of cap free
Amount of items: 2
Items: 
Size: 652019 Color: 2
Size: 347364 Color: 4

Bin 494: 618 of cap free
Amount of items: 2
Items: 
Size: 760037 Color: 0
Size: 239346 Color: 1

Bin 495: 621 of cap free
Amount of items: 2
Items: 
Size: 798029 Color: 1
Size: 201351 Color: 2

Bin 496: 625 of cap free
Amount of items: 2
Items: 
Size: 538587 Color: 1
Size: 460789 Color: 0

Bin 497: 625 of cap free
Amount of items: 2
Items: 
Size: 561770 Color: 3
Size: 437606 Color: 2

Bin 498: 626 of cap free
Amount of items: 2
Items: 
Size: 580310 Color: 2
Size: 419065 Color: 1

Bin 499: 628 of cap free
Amount of items: 2
Items: 
Size: 792195 Color: 1
Size: 207178 Color: 3

Bin 500: 630 of cap free
Amount of items: 2
Items: 
Size: 546169 Color: 3
Size: 453202 Color: 4

Bin 501: 638 of cap free
Amount of items: 2
Items: 
Size: 781240 Color: 1
Size: 218123 Color: 3

Bin 502: 639 of cap free
Amount of items: 2
Items: 
Size: 585551 Color: 1
Size: 413811 Color: 0

Bin 503: 644 of cap free
Amount of items: 2
Items: 
Size: 650473 Color: 0
Size: 348884 Color: 2

Bin 504: 649 of cap free
Amount of items: 2
Items: 
Size: 737731 Color: 4
Size: 261621 Color: 1

Bin 505: 662 of cap free
Amount of items: 2
Items: 
Size: 524259 Color: 0
Size: 475080 Color: 2

Bin 506: 662 of cap free
Amount of items: 2
Items: 
Size: 693260 Color: 2
Size: 306079 Color: 1

Bin 507: 668 of cap free
Amount of items: 2
Items: 
Size: 643412 Color: 4
Size: 355921 Color: 2

Bin 508: 674 of cap free
Amount of items: 2
Items: 
Size: 656258 Color: 3
Size: 343069 Color: 1

Bin 509: 681 of cap free
Amount of items: 2
Items: 
Size: 631595 Color: 2
Size: 367725 Color: 0

Bin 510: 695 of cap free
Amount of items: 2
Items: 
Size: 750693 Color: 0
Size: 248613 Color: 4

Bin 511: 697 of cap free
Amount of items: 2
Items: 
Size: 600909 Color: 0
Size: 398395 Color: 4

Bin 512: 703 of cap free
Amount of items: 2
Items: 
Size: 698246 Color: 4
Size: 301052 Color: 0

Bin 513: 703 of cap free
Amount of items: 2
Items: 
Size: 789360 Color: 3
Size: 209938 Color: 1

Bin 514: 712 of cap free
Amount of items: 2
Items: 
Size: 537753 Color: 4
Size: 461536 Color: 1

Bin 515: 712 of cap free
Amount of items: 2
Items: 
Size: 614276 Color: 2
Size: 385013 Color: 4

Bin 516: 713 of cap free
Amount of items: 2
Items: 
Size: 677681 Color: 4
Size: 321607 Color: 1

Bin 517: 714 of cap free
Amount of items: 2
Items: 
Size: 740220 Color: 0
Size: 259067 Color: 3

Bin 518: 720 of cap free
Amount of items: 2
Items: 
Size: 535569 Color: 0
Size: 463712 Color: 3

Bin 519: 721 of cap free
Amount of items: 2
Items: 
Size: 744582 Color: 0
Size: 254698 Color: 3

Bin 520: 724 of cap free
Amount of items: 2
Items: 
Size: 633002 Color: 0
Size: 366275 Color: 3

Bin 521: 726 of cap free
Amount of items: 2
Items: 
Size: 767981 Color: 4
Size: 231294 Color: 2

Bin 522: 734 of cap free
Amount of items: 2
Items: 
Size: 602387 Color: 3
Size: 396880 Color: 4

Bin 523: 736 of cap free
Amount of items: 2
Items: 
Size: 721074 Color: 3
Size: 278191 Color: 2

Bin 524: 739 of cap free
Amount of items: 2
Items: 
Size: 597263 Color: 2
Size: 401999 Color: 1

Bin 525: 741 of cap free
Amount of items: 2
Items: 
Size: 594016 Color: 0
Size: 405244 Color: 4

Bin 526: 742 of cap free
Amount of items: 2
Items: 
Size: 708607 Color: 0
Size: 290652 Color: 1

Bin 527: 747 of cap free
Amount of items: 2
Items: 
Size: 746429 Color: 0
Size: 252825 Color: 4

Bin 528: 750 of cap free
Amount of items: 2
Items: 
Size: 773116 Color: 3
Size: 226135 Color: 4

Bin 529: 753 of cap free
Amount of items: 2
Items: 
Size: 563635 Color: 1
Size: 435613 Color: 0

Bin 530: 755 of cap free
Amount of items: 2
Items: 
Size: 560143 Color: 1
Size: 439103 Color: 4

Bin 531: 757 of cap free
Amount of items: 2
Items: 
Size: 574369 Color: 4
Size: 424875 Color: 0

Bin 532: 759 of cap free
Amount of items: 2
Items: 
Size: 643736 Color: 2
Size: 355506 Color: 0

Bin 533: 760 of cap free
Amount of items: 9
Items: 
Size: 112576 Color: 3
Size: 112564 Color: 2
Size: 111076 Color: 1
Size: 110829 Color: 0
Size: 110829 Color: 0
Size: 110433 Color: 2
Size: 110328 Color: 0
Size: 110309 Color: 4
Size: 110297 Color: 1

Bin 534: 768 of cap free
Amount of items: 2
Items: 
Size: 603558 Color: 3
Size: 395675 Color: 4

Bin 535: 770 of cap free
Amount of items: 2
Items: 
Size: 602362 Color: 0
Size: 396869 Color: 4

Bin 536: 770 of cap free
Amount of items: 2
Items: 
Size: 724913 Color: 0
Size: 274318 Color: 3

Bin 537: 771 of cap free
Amount of items: 2
Items: 
Size: 554109 Color: 4
Size: 445121 Color: 1

Bin 538: 775 of cap free
Amount of items: 2
Items: 
Size: 785580 Color: 4
Size: 213646 Color: 3

Bin 539: 776 of cap free
Amount of items: 2
Items: 
Size: 774267 Color: 1
Size: 224958 Color: 3

Bin 540: 782 of cap free
Amount of items: 2
Items: 
Size: 585895 Color: 0
Size: 413324 Color: 2

Bin 541: 787 of cap free
Amount of items: 2
Items: 
Size: 681063 Color: 4
Size: 318151 Color: 2

Bin 542: 788 of cap free
Amount of items: 2
Items: 
Size: 717950 Color: 2
Size: 281263 Color: 4

Bin 543: 794 of cap free
Amount of items: 2
Items: 
Size: 501243 Color: 3
Size: 497964 Color: 2

Bin 544: 796 of cap free
Amount of items: 2
Items: 
Size: 526269 Color: 3
Size: 472936 Color: 2

Bin 545: 799 of cap free
Amount of items: 2
Items: 
Size: 666185 Color: 2
Size: 333017 Color: 3

Bin 546: 801 of cap free
Amount of items: 2
Items: 
Size: 785539 Color: 3
Size: 213661 Color: 4

Bin 547: 802 of cap free
Amount of items: 2
Items: 
Size: 507813 Color: 4
Size: 491386 Color: 3

Bin 548: 808 of cap free
Amount of items: 2
Items: 
Size: 639811 Color: 0
Size: 359382 Color: 4

Bin 549: 810 of cap free
Amount of items: 2
Items: 
Size: 505703 Color: 3
Size: 493488 Color: 2

Bin 550: 813 of cap free
Amount of items: 2
Items: 
Size: 615639 Color: 1
Size: 383549 Color: 0

Bin 551: 817 of cap free
Amount of items: 2
Items: 
Size: 661795 Color: 2
Size: 337389 Color: 3

Bin 552: 819 of cap free
Amount of items: 2
Items: 
Size: 756621 Color: 0
Size: 242561 Color: 2

Bin 553: 820 of cap free
Amount of items: 2
Items: 
Size: 671139 Color: 2
Size: 328042 Color: 3

Bin 554: 820 of cap free
Amount of items: 2
Items: 
Size: 729797 Color: 2
Size: 269384 Color: 3

Bin 555: 821 of cap free
Amount of items: 2
Items: 
Size: 623990 Color: 4
Size: 375190 Color: 3

Bin 556: 822 of cap free
Amount of items: 2
Items: 
Size: 513814 Color: 2
Size: 485365 Color: 1

Bin 557: 823 of cap free
Amount of items: 2
Items: 
Size: 575735 Color: 3
Size: 423443 Color: 1

Bin 558: 830 of cap free
Amount of items: 2
Items: 
Size: 520773 Color: 3
Size: 478398 Color: 0

Bin 559: 830 of cap free
Amount of items: 2
Items: 
Size: 702373 Color: 3
Size: 296798 Color: 1

Bin 560: 842 of cap free
Amount of items: 2
Items: 
Size: 577667 Color: 2
Size: 421492 Color: 4

Bin 561: 842 of cap free
Amount of items: 2
Items: 
Size: 589488 Color: 3
Size: 409671 Color: 1

Bin 562: 847 of cap free
Amount of items: 2
Items: 
Size: 721886 Color: 0
Size: 277268 Color: 3

Bin 563: 849 of cap free
Amount of items: 2
Items: 
Size: 573196 Color: 3
Size: 425956 Color: 0

Bin 564: 859 of cap free
Amount of items: 2
Items: 
Size: 627869 Color: 4
Size: 371273 Color: 3

Bin 565: 871 of cap free
Amount of items: 2
Items: 
Size: 778198 Color: 1
Size: 220932 Color: 2

Bin 566: 879 of cap free
Amount of items: 2
Items: 
Size: 733805 Color: 2
Size: 265317 Color: 1

Bin 567: 879 of cap free
Amount of items: 2
Items: 
Size: 762648 Color: 3
Size: 236474 Color: 2

Bin 568: 896 of cap free
Amount of items: 2
Items: 
Size: 787761 Color: 2
Size: 211344 Color: 1

Bin 569: 897 of cap free
Amount of items: 2
Items: 
Size: 651977 Color: 2
Size: 347127 Color: 4

Bin 570: 925 of cap free
Amount of items: 2
Items: 
Size: 699773 Color: 4
Size: 299303 Color: 0

Bin 571: 926 of cap free
Amount of items: 2
Items: 
Size: 704007 Color: 2
Size: 295068 Color: 3

Bin 572: 929 of cap free
Amount of items: 2
Items: 
Size: 546020 Color: 3
Size: 453052 Color: 1

Bin 573: 932 of cap free
Amount of items: 2
Items: 
Size: 704640 Color: 3
Size: 294429 Color: 0

Bin 574: 932 of cap free
Amount of items: 2
Items: 
Size: 741484 Color: 1
Size: 257585 Color: 0

Bin 575: 933 of cap free
Amount of items: 2
Items: 
Size: 690597 Color: 2
Size: 308471 Color: 3

Bin 576: 944 of cap free
Amount of items: 2
Items: 
Size: 673155 Color: 2
Size: 325902 Color: 3

Bin 577: 948 of cap free
Amount of items: 2
Items: 
Size: 689640 Color: 0
Size: 309413 Color: 3

Bin 578: 959 of cap free
Amount of items: 2
Items: 
Size: 618323 Color: 4
Size: 380719 Color: 0

Bin 579: 963 of cap free
Amount of items: 2
Items: 
Size: 721859 Color: 0
Size: 277179 Color: 1

Bin 580: 968 of cap free
Amount of items: 2
Items: 
Size: 517855 Color: 4
Size: 481178 Color: 1

Bin 581: 977 of cap free
Amount of items: 2
Items: 
Size: 672006 Color: 0
Size: 327018 Color: 3

Bin 582: 985 of cap free
Amount of items: 2
Items: 
Size: 573081 Color: 0
Size: 425935 Color: 4

Bin 583: 995 of cap free
Amount of items: 2
Items: 
Size: 752625 Color: 4
Size: 246381 Color: 2

Bin 584: 1000 of cap free
Amount of items: 2
Items: 
Size: 661766 Color: 1
Size: 337235 Color: 2

Bin 585: 1000 of cap free
Amount of items: 2
Items: 
Size: 737389 Color: 4
Size: 261612 Color: 1

Bin 586: 1006 of cap free
Amount of items: 3
Items: 
Size: 527810 Color: 1
Size: 343770 Color: 4
Size: 127415 Color: 4

Bin 587: 1015 of cap free
Amount of items: 2
Items: 
Size: 741418 Color: 1
Size: 257568 Color: 0

Bin 588: 1023 of cap free
Amount of items: 2
Items: 
Size: 556246 Color: 1
Size: 442732 Color: 4

Bin 589: 1024 of cap free
Amount of items: 2
Items: 
Size: 797773 Color: 1
Size: 201204 Color: 3

Bin 590: 1030 of cap free
Amount of items: 2
Items: 
Size: 686002 Color: 2
Size: 312969 Color: 1

Bin 591: 1033 of cap free
Amount of items: 2
Items: 
Size: 689749 Color: 3
Size: 309219 Color: 2

Bin 592: 1034 of cap free
Amount of items: 2
Items: 
Size: 626385 Color: 2
Size: 372582 Color: 1

Bin 593: 1054 of cap free
Amount of items: 2
Items: 
Size: 581438 Color: 2
Size: 417509 Color: 3

Bin 594: 1054 of cap free
Amount of items: 2
Items: 
Size: 772806 Color: 2
Size: 226141 Color: 3

Bin 595: 1075 of cap free
Amount of items: 2
Items: 
Size: 639953 Color: 4
Size: 358973 Color: 1

Bin 596: 1079 of cap free
Amount of items: 2
Items: 
Size: 706257 Color: 2
Size: 292665 Color: 3

Bin 597: 1086 of cap free
Amount of items: 2
Items: 
Size: 653979 Color: 1
Size: 344936 Color: 3

Bin 598: 1100 of cap free
Amount of items: 2
Items: 
Size: 550927 Color: 2
Size: 447974 Color: 0

Bin 599: 1118 of cap free
Amount of items: 6
Items: 
Size: 179621 Color: 2
Size: 179486 Color: 3
Size: 179465 Color: 1
Size: 179175 Color: 4
Size: 179056 Color: 2
Size: 102080 Color: 0

Bin 600: 1122 of cap free
Amount of items: 2
Items: 
Size: 563058 Color: 2
Size: 435821 Color: 1

Bin 601: 1127 of cap free
Amount of items: 2
Items: 
Size: 697184 Color: 0
Size: 301690 Color: 4

Bin 602: 1150 of cap free
Amount of items: 6
Items: 
Size: 173569 Color: 2
Size: 173540 Color: 4
Size: 173177 Color: 2
Size: 172405 Color: 1
Size: 172356 Color: 1
Size: 133804 Color: 4

Bin 603: 1154 of cap free
Amount of items: 2
Items: 
Size: 623797 Color: 3
Size: 375050 Color: 0

Bin 604: 1176 of cap free
Amount of items: 2
Items: 
Size: 568072 Color: 2
Size: 430753 Color: 4

Bin 605: 1185 of cap free
Amount of items: 2
Items: 
Size: 747516 Color: 4
Size: 251300 Color: 2

Bin 606: 1190 of cap free
Amount of items: 2
Items: 
Size: 630915 Color: 4
Size: 367896 Color: 2

Bin 607: 1211 of cap free
Amount of items: 2
Items: 
Size: 558052 Color: 2
Size: 440738 Color: 1

Bin 608: 1213 of cap free
Amount of items: 2
Items: 
Size: 795436 Color: 3
Size: 203352 Color: 4

Bin 609: 1215 of cap free
Amount of items: 2
Items: 
Size: 711064 Color: 1
Size: 287722 Color: 4

Bin 610: 1215 of cap free
Amount of items: 2
Items: 
Size: 712301 Color: 1
Size: 286485 Color: 3

Bin 611: 1221 of cap free
Amount of items: 2
Items: 
Size: 715001 Color: 2
Size: 283779 Color: 4

Bin 612: 1223 of cap free
Amount of items: 2
Items: 
Size: 785319 Color: 1
Size: 213459 Color: 2

Bin 613: 1235 of cap free
Amount of items: 2
Items: 
Size: 799369 Color: 2
Size: 199397 Color: 4

Bin 614: 1250 of cap free
Amount of items: 2
Items: 
Size: 533237 Color: 0
Size: 465514 Color: 3

Bin 615: 1253 of cap free
Amount of items: 2
Items: 
Size: 517792 Color: 2
Size: 480956 Color: 1

Bin 616: 1258 of cap free
Amount of items: 2
Items: 
Size: 603234 Color: 1
Size: 395509 Color: 4

Bin 617: 1265 of cap free
Amount of items: 2
Items: 
Size: 550867 Color: 4
Size: 447869 Color: 1

Bin 618: 1271 of cap free
Amount of items: 2
Items: 
Size: 601883 Color: 4
Size: 396847 Color: 2

Bin 619: 1294 of cap free
Amount of items: 2
Items: 
Size: 623706 Color: 4
Size: 375001 Color: 0

Bin 620: 1295 of cap free
Amount of items: 2
Items: 
Size: 690924 Color: 3
Size: 307782 Color: 1

Bin 621: 1298 of cap free
Amount of items: 2
Items: 
Size: 573011 Color: 0
Size: 425692 Color: 4

Bin 622: 1300 of cap free
Amount of items: 2
Items: 
Size: 756502 Color: 1
Size: 242199 Color: 3

Bin 623: 1308 of cap free
Amount of items: 2
Items: 
Size: 677626 Color: 0
Size: 321067 Color: 4

Bin 624: 1326 of cap free
Amount of items: 2
Items: 
Size: 577113 Color: 4
Size: 421562 Color: 2

Bin 625: 1340 of cap free
Amount of items: 2
Items: 
Size: 714991 Color: 3
Size: 283670 Color: 2

Bin 626: 1351 of cap free
Amount of items: 2
Items: 
Size: 569751 Color: 4
Size: 428899 Color: 3

Bin 627: 1359 of cap free
Amount of items: 2
Items: 
Size: 612261 Color: 2
Size: 386381 Color: 0

Bin 628: 1367 of cap free
Amount of items: 2
Items: 
Size: 731663 Color: 3
Size: 266971 Color: 2

Bin 629: 1369 of cap free
Amount of items: 2
Items: 
Size: 618211 Color: 0
Size: 380421 Color: 3

Bin 630: 1407 of cap free
Amount of items: 2
Items: 
Size: 632354 Color: 2
Size: 366240 Color: 1

Bin 631: 1411 of cap free
Amount of items: 2
Items: 
Size: 598863 Color: 3
Size: 399727 Color: 4

Bin 632: 1423 of cap free
Amount of items: 2
Items: 
Size: 769305 Color: 2
Size: 229273 Color: 3

Bin 633: 1426 of cap free
Amount of items: 2
Items: 
Size: 503266 Color: 0
Size: 495309 Color: 4

Bin 634: 1426 of cap free
Amount of items: 2
Items: 
Size: 781535 Color: 3
Size: 217040 Color: 0

Bin 635: 1430 of cap free
Amount of items: 2
Items: 
Size: 513617 Color: 2
Size: 484954 Color: 0

Bin 636: 1437 of cap free
Amount of items: 2
Items: 
Size: 536593 Color: 3
Size: 461971 Color: 4

Bin 637: 1444 of cap free
Amount of items: 2
Items: 
Size: 593726 Color: 0
Size: 404831 Color: 4

Bin 638: 1463 of cap free
Amount of items: 2
Items: 
Size: 609054 Color: 0
Size: 389484 Color: 1

Bin 639: 1463 of cap free
Amount of items: 2
Items: 
Size: 761203 Color: 0
Size: 237335 Color: 3

Bin 640: 1481 of cap free
Amount of items: 2
Items: 
Size: 714861 Color: 0
Size: 283659 Color: 1

Bin 641: 1483 of cap free
Amount of items: 2
Items: 
Size: 706256 Color: 4
Size: 292262 Color: 2

Bin 642: 1491 of cap free
Amount of items: 2
Items: 
Size: 661649 Color: 2
Size: 336861 Color: 3

Bin 643: 1498 of cap free
Amount of items: 2
Items: 
Size: 797717 Color: 3
Size: 200786 Color: 4

Bin 644: 1501 of cap free
Amount of items: 2
Items: 
Size: 575459 Color: 1
Size: 423041 Color: 2

Bin 645: 1512 of cap free
Amount of items: 2
Items: 
Size: 513605 Color: 1
Size: 484884 Color: 0

Bin 646: 1523 of cap free
Amount of items: 2
Items: 
Size: 790622 Color: 4
Size: 207856 Color: 1

Bin 647: 1530 of cap free
Amount of items: 2
Items: 
Size: 673727 Color: 3
Size: 324744 Color: 4

Bin 648: 1536 of cap free
Amount of items: 2
Items: 
Size: 570272 Color: 3
Size: 428193 Color: 2

Bin 649: 1543 of cap free
Amount of items: 2
Items: 
Size: 752576 Color: 0
Size: 245882 Color: 1

Bin 650: 1554 of cap free
Amount of items: 2
Items: 
Size: 532964 Color: 3
Size: 465483 Color: 4

Bin 651: 1607 of cap free
Amount of items: 2
Items: 
Size: 706235 Color: 4
Size: 292159 Color: 0

Bin 652: 1609 of cap free
Amount of items: 2
Items: 
Size: 593593 Color: 2
Size: 404799 Color: 3

Bin 653: 1613 of cap free
Amount of items: 2
Items: 
Size: 651665 Color: 3
Size: 346723 Color: 0

Bin 654: 1625 of cap free
Amount of items: 2
Items: 
Size: 705833 Color: 3
Size: 292543 Color: 4

Bin 655: 1626 of cap free
Amount of items: 2
Items: 
Size: 567662 Color: 2
Size: 430713 Color: 0

Bin 656: 1630 of cap free
Amount of items: 2
Items: 
Size: 545848 Color: 4
Size: 452523 Color: 0

Bin 657: 1640 of cap free
Amount of items: 2
Items: 
Size: 649511 Color: 2
Size: 348850 Color: 3

Bin 658: 1673 of cap free
Amount of items: 2
Items: 
Size: 729753 Color: 1
Size: 268575 Color: 2

Bin 659: 1683 of cap free
Amount of items: 2
Items: 
Size: 675832 Color: 2
Size: 322486 Color: 4

Bin 660: 1716 of cap free
Amount of items: 2
Items: 
Size: 535304 Color: 0
Size: 462981 Color: 3

Bin 661: 1725 of cap free
Amount of items: 2
Items: 
Size: 697000 Color: 1
Size: 301276 Color: 4

Bin 662: 1735 of cap free
Amount of items: 2
Items: 
Size: 766159 Color: 3
Size: 232107 Color: 0

Bin 663: 1780 of cap free
Amount of items: 2
Items: 
Size: 621980 Color: 2
Size: 376241 Color: 4

Bin 664: 1783 of cap free
Amount of items: 2
Items: 
Size: 699712 Color: 0
Size: 298506 Color: 4

Bin 665: 1819 of cap free
Amount of items: 2
Items: 
Size: 743515 Color: 4
Size: 254667 Color: 2

Bin 666: 1830 of cap free
Amount of items: 2
Items: 
Size: 635862 Color: 0
Size: 362309 Color: 2

Bin 667: 1848 of cap free
Amount of items: 2
Items: 
Size: 532724 Color: 4
Size: 465429 Color: 0

Bin 668: 1851 of cap free
Amount of items: 2
Items: 
Size: 513309 Color: 4
Size: 484841 Color: 0

Bin 669: 1853 of cap free
Amount of items: 2
Items: 
Size: 540799 Color: 1
Size: 457349 Color: 0

Bin 670: 1856 of cap free
Amount of items: 2
Items: 
Size: 562871 Color: 2
Size: 435274 Color: 4

Bin 671: 1866 of cap free
Amount of items: 2
Items: 
Size: 618067 Color: 1
Size: 380068 Color: 4

Bin 672: 1874 of cap free
Amount of items: 2
Items: 
Size: 703745 Color: 2
Size: 294382 Color: 3

Bin 673: 1881 of cap free
Amount of items: 2
Items: 
Size: 658489 Color: 4
Size: 339631 Color: 2

Bin 674: 1939 of cap free
Amount of items: 2
Items: 
Size: 545604 Color: 3
Size: 452458 Color: 4

Bin 675: 1952 of cap free
Amount of items: 2
Items: 
Size: 643215 Color: 3
Size: 354834 Color: 1

Bin 676: 1972 of cap free
Amount of items: 2
Items: 
Size: 573158 Color: 4
Size: 424871 Color: 2

Bin 677: 2004 of cap free
Amount of items: 2
Items: 
Size: 772010 Color: 2
Size: 225987 Color: 4

Bin 678: 2014 of cap free
Amount of items: 2
Items: 
Size: 500588 Color: 3
Size: 497399 Color: 2

Bin 679: 2017 of cap free
Amount of items: 2
Items: 
Size: 736878 Color: 1
Size: 261106 Color: 3

Bin 680: 2018 of cap free
Amount of items: 2
Items: 
Size: 521427 Color: 0
Size: 476556 Color: 1

Bin 681: 2036 of cap free
Amount of items: 2
Items: 
Size: 588746 Color: 3
Size: 409219 Color: 2

Bin 682: 2062 of cap free
Amount of items: 2
Items: 
Size: 723665 Color: 2
Size: 274274 Color: 1

Bin 683: 2064 of cap free
Amount of items: 2
Items: 
Size: 503250 Color: 3
Size: 494687 Color: 0

Bin 684: 2068 of cap free
Amount of items: 2
Items: 
Size: 752163 Color: 0
Size: 245770 Color: 1

Bin 685: 2138 of cap free
Amount of items: 2
Items: 
Size: 699375 Color: 4
Size: 298488 Color: 2

Bin 686: 2159 of cap free
Amount of items: 2
Items: 
Size: 781304 Color: 3
Size: 216538 Color: 0

Bin 687: 2196 of cap free
Amount of items: 2
Items: 
Size: 550185 Color: 1
Size: 447620 Color: 3

Bin 688: 2206 of cap free
Amount of items: 2
Items: 
Size: 532707 Color: 2
Size: 465088 Color: 0

Bin 689: 2210 of cap free
Amount of items: 2
Items: 
Size: 696922 Color: 2
Size: 300869 Color: 4

Bin 690: 2283 of cap free
Amount of items: 2
Items: 
Size: 608168 Color: 3
Size: 389550 Color: 0

Bin 691: 2288 of cap free
Amount of items: 2
Items: 
Size: 577111 Color: 2
Size: 420602 Color: 1

Bin 692: 2304 of cap free
Amount of items: 2
Items: 
Size: 685794 Color: 3
Size: 311903 Color: 2

Bin 693: 2312 of cap free
Amount of items: 2
Items: 
Size: 552817 Color: 4
Size: 444872 Color: 2

Bin 694: 2319 of cap free
Amount of items: 2
Items: 
Size: 639428 Color: 4
Size: 358254 Color: 1

Bin 695: 2356 of cap free
Amount of items: 2
Items: 
Size: 557765 Color: 3
Size: 439880 Color: 1

Bin 696: 2359 of cap free
Amount of items: 2
Items: 
Size: 649243 Color: 4
Size: 348399 Color: 1

Bin 697: 2396 of cap free
Amount of items: 2
Items: 
Size: 606500 Color: 4
Size: 391105 Color: 3

Bin 698: 2418 of cap free
Amount of items: 2
Items: 
Size: 605686 Color: 0
Size: 391897 Color: 4

Bin 699: 2435 of cap free
Amount of items: 2
Items: 
Size: 558557 Color: 1
Size: 439009 Color: 3

Bin 700: 2462 of cap free
Amount of items: 2
Items: 
Size: 643033 Color: 4
Size: 354506 Color: 0

Bin 701: 2474 of cap free
Amount of items: 2
Items: 
Size: 552791 Color: 1
Size: 444736 Color: 2

Bin 702: 2525 of cap free
Amount of items: 2
Items: 
Size: 500248 Color: 3
Size: 497228 Color: 4

Bin 703: 2563 of cap free
Amount of items: 2
Items: 
Size: 569388 Color: 1
Size: 428050 Color: 0

Bin 704: 2579 of cap free
Amount of items: 2
Items: 
Size: 668253 Color: 2
Size: 329169 Color: 0

Bin 705: 2611 of cap free
Amount of items: 2
Items: 
Size: 552763 Color: 3
Size: 444627 Color: 0

Bin 706: 2638 of cap free
Amount of items: 2
Items: 
Size: 557550 Color: 2
Size: 439813 Color: 1

Bin 707: 2656 of cap free
Amount of items: 2
Items: 
Size: 593218 Color: 1
Size: 404127 Color: 2

Bin 708: 2668 of cap free
Amount of items: 2
Items: 
Size: 699029 Color: 4
Size: 298304 Color: 0

Bin 709: 2688 of cap free
Amount of items: 2
Items: 
Size: 720453 Color: 3
Size: 276860 Color: 1

Bin 710: 2700 of cap free
Amount of items: 2
Items: 
Size: 540588 Color: 3
Size: 456713 Color: 0

Bin 711: 2758 of cap free
Amount of items: 2
Items: 
Size: 540536 Color: 3
Size: 456707 Color: 4

Bin 712: 2767 of cap free
Amount of items: 2
Items: 
Size: 781096 Color: 2
Size: 216138 Color: 3

Bin 713: 2802 of cap free
Amount of items: 2
Items: 
Size: 520892 Color: 0
Size: 476307 Color: 3

Bin 714: 2822 of cap free
Amount of items: 2
Items: 
Size: 540382 Color: 4
Size: 456797 Color: 3

Bin 715: 2829 of cap free
Amount of items: 2
Items: 
Size: 663960 Color: 0
Size: 333212 Color: 2

Bin 716: 2865 of cap free
Amount of items: 2
Items: 
Size: 572491 Color: 4
Size: 424645 Color: 3

Bin 717: 2881 of cap free
Amount of items: 2
Items: 
Size: 525497 Color: 0
Size: 471623 Color: 1

Bin 718: 2894 of cap free
Amount of items: 2
Items: 
Size: 601801 Color: 3
Size: 395306 Color: 0

Bin 719: 2957 of cap free
Amount of items: 2
Items: 
Size: 513297 Color: 0
Size: 483747 Color: 2

Bin 720: 2965 of cap free
Amount of items: 2
Items: 
Size: 520734 Color: 2
Size: 476302 Color: 1

Bin 721: 3016 of cap free
Amount of items: 2
Items: 
Size: 598645 Color: 4
Size: 398340 Color: 3

Bin 722: 3029 of cap free
Amount of items: 2
Items: 
Size: 797750 Color: 4
Size: 199222 Color: 2

Bin 723: 3102 of cap free
Amount of items: 2
Items: 
Size: 743106 Color: 1
Size: 253793 Color: 0

Bin 724: 3139 of cap free
Amount of items: 2
Items: 
Size: 699302 Color: 0
Size: 297560 Color: 3

Bin 725: 3155 of cap free
Amount of items: 2
Items: 
Size: 689207 Color: 1
Size: 307639 Color: 4

Bin 726: 3161 of cap free
Amount of items: 2
Items: 
Size: 787058 Color: 3
Size: 209782 Color: 4

Bin 727: 3182 of cap free
Amount of items: 2
Items: 
Size: 621936 Color: 3
Size: 374883 Color: 2

Bin 728: 3190 of cap free
Amount of items: 2
Items: 
Size: 755737 Color: 4
Size: 241074 Color: 3

Bin 729: 3233 of cap free
Amount of items: 2
Items: 
Size: 710913 Color: 1
Size: 285855 Color: 4

Bin 730: 3267 of cap free
Amount of items: 2
Items: 
Size: 598587 Color: 1
Size: 398147 Color: 3

Bin 731: 3299 of cap free
Amount of items: 2
Items: 
Size: 592332 Color: 2
Size: 404370 Color: 1

Bin 732: 3440 of cap free
Amount of items: 2
Items: 
Size: 587920 Color: 4
Size: 408641 Color: 1

Bin 733: 3444 of cap free
Amount of items: 2
Items: 
Size: 630671 Color: 1
Size: 365886 Color: 2

Bin 734: 3506 of cap free
Amount of items: 2
Items: 
Size: 684601 Color: 0
Size: 311894 Color: 2

Bin 735: 3546 of cap free
Amount of items: 2
Items: 
Size: 688853 Color: 4
Size: 307602 Color: 2

Bin 736: 3556 of cap free
Amount of items: 2
Items: 
Size: 557745 Color: 1
Size: 438700 Color: 0

Bin 737: 3559 of cap free
Amount of items: 2
Items: 
Size: 761182 Color: 1
Size: 235260 Color: 2

Bin 738: 3577 of cap free
Amount of items: 2
Items: 
Size: 630658 Color: 3
Size: 365766 Color: 1

Bin 739: 3597 of cap free
Amount of items: 2
Items: 
Size: 675887 Color: 4
Size: 320517 Color: 2

Bin 740: 3725 of cap free
Amount of items: 2
Items: 
Size: 671137 Color: 4
Size: 325139 Color: 3

Bin 741: 3774 of cap free
Amount of items: 2
Items: 
Size: 684915 Color: 2
Size: 311312 Color: 1

Bin 742: 3776 of cap free
Amount of items: 2
Items: 
Size: 780303 Color: 4
Size: 215922 Color: 3

Bin 743: 3788 of cap free
Amount of items: 2
Items: 
Size: 562584 Color: 4
Size: 433629 Color: 3

Bin 744: 3814 of cap free
Amount of items: 2
Items: 
Size: 592155 Color: 3
Size: 404032 Color: 0

Bin 745: 3891 of cap free
Amount of items: 2
Items: 
Size: 675632 Color: 1
Size: 320478 Color: 4

Bin 746: 3969 of cap free
Amount of items: 2
Items: 
Size: 540246 Color: 1
Size: 455786 Color: 4

Bin 747: 3993 of cap free
Amount of items: 2
Items: 
Size: 642646 Color: 3
Size: 353362 Color: 2

Bin 748: 4106 of cap free
Amount of items: 2
Items: 
Size: 540355 Color: 4
Size: 455540 Color: 1

Bin 749: 4125 of cap free
Amount of items: 2
Items: 
Size: 703610 Color: 2
Size: 292266 Color: 4

Bin 750: 4135 of cap free
Amount of items: 2
Items: 
Size: 771508 Color: 2
Size: 224358 Color: 4

Bin 751: 4153 of cap free
Amount of items: 2
Items: 
Size: 620998 Color: 3
Size: 374850 Color: 2

Bin 752: 4161 of cap free
Amount of items: 2
Items: 
Size: 630654 Color: 1
Size: 365186 Color: 2

Bin 753: 4165 of cap free
Amount of items: 2
Items: 
Size: 754862 Color: 2
Size: 240974 Color: 3

Bin 754: 4191 of cap free
Amount of items: 2
Items: 
Size: 512887 Color: 3
Size: 482923 Color: 0

Bin 755: 4432 of cap free
Amount of items: 2
Items: 
Size: 600452 Color: 3
Size: 395117 Color: 1

Bin 756: 4442 of cap free
Amount of items: 2
Items: 
Size: 630557 Color: 1
Size: 365002 Color: 0

Bin 757: 4452 of cap free
Amount of items: 2
Items: 
Size: 780302 Color: 2
Size: 215247 Color: 1

Bin 758: 4509 of cap free
Amount of items: 2
Items: 
Size: 592138 Color: 1
Size: 403354 Color: 3

Bin 759: 4530 of cap free
Amount of items: 2
Items: 
Size: 771121 Color: 3
Size: 224350 Color: 1

Bin 760: 4558 of cap free
Amount of items: 2
Items: 
Size: 771113 Color: 3
Size: 224330 Color: 2

Bin 761: 4563 of cap free
Amount of items: 2
Items: 
Size: 649115 Color: 4
Size: 346323 Color: 0

Bin 762: 4578 of cap free
Amount of items: 2
Items: 
Size: 703606 Color: 3
Size: 291817 Color: 4

Bin 763: 4646 of cap free
Amount of items: 2
Items: 
Size: 630045 Color: 0
Size: 365310 Color: 1

Bin 764: 4703 of cap free
Amount of items: 2
Items: 
Size: 557056 Color: 4
Size: 438242 Color: 0

Bin 765: 4715 of cap free
Amount of items: 8
Items: 
Size: 129995 Color: 1
Size: 125210 Color: 3
Size: 124809 Color: 1
Size: 124534 Color: 0
Size: 123519 Color: 2
Size: 122814 Color: 0
Size: 122701 Color: 0
Size: 121704 Color: 1

Bin 766: 4754 of cap free
Amount of items: 2
Items: 
Size: 512359 Color: 2
Size: 482888 Color: 0

Bin 767: 4822 of cap free
Amount of items: 2
Items: 
Size: 620848 Color: 4
Size: 374331 Color: 2

Bin 768: 4840 of cap free
Amount of items: 2
Items: 
Size: 675538 Color: 2
Size: 319623 Color: 1

Bin 769: 4886 of cap free
Amount of items: 2
Items: 
Size: 512246 Color: 0
Size: 482869 Color: 3

Bin 770: 4912 of cap free
Amount of items: 2
Items: 
Size: 754298 Color: 3
Size: 240791 Color: 4

Bin 771: 4934 of cap free
Amount of items: 2
Items: 
Size: 779377 Color: 0
Size: 215690 Color: 2

Bin 772: 4956 of cap free
Amount of items: 2
Items: 
Size: 663255 Color: 2
Size: 331790 Color: 3

Bin 773: 5152 of cap free
Amount of items: 2
Items: 
Size: 661713 Color: 3
Size: 333136 Color: 2

Bin 774: 5200 of cap free
Amount of items: 2
Items: 
Size: 740183 Color: 0
Size: 254618 Color: 1

Bin 775: 5203 of cap free
Amount of items: 2
Items: 
Size: 620532 Color: 4
Size: 374266 Color: 1

Bin 776: 5214 of cap free
Amount of items: 2
Items: 
Size: 561765 Color: 4
Size: 433022 Color: 1

Bin 777: 5283 of cap free
Amount of items: 2
Items: 
Size: 703211 Color: 0
Size: 291507 Color: 3

Bin 778: 5321 of cap free
Amount of items: 6
Items: 
Size: 166759 Color: 3
Size: 166613 Color: 1
Size: 166544 Color: 0
Size: 166009 Color: 3
Size: 164654 Color: 3
Size: 164101 Color: 0

Bin 779: 5472 of cap free
Amount of items: 2
Items: 
Size: 661411 Color: 1
Size: 333118 Color: 2

Bin 780: 5570 of cap free
Amount of items: 2
Items: 
Size: 661359 Color: 0
Size: 333072 Color: 2

Bin 781: 5716 of cap free
Amount of items: 2
Items: 
Size: 604848 Color: 1
Size: 389437 Color: 4

Bin 782: 5776 of cap free
Amount of items: 2
Items: 
Size: 511683 Color: 0
Size: 482542 Color: 3

Bin 783: 5793 of cap free
Amount of items: 2
Items: 
Size: 511680 Color: 3
Size: 482528 Color: 1

Bin 784: 6084 of cap free
Amount of items: 2
Items: 
Size: 549921 Color: 2
Size: 443996 Color: 1

Bin 785: 6353 of cap free
Amount of items: 2
Items: 
Size: 511332 Color: 2
Size: 482316 Color: 1

Bin 786: 6469 of cap free
Amount of items: 2
Items: 
Size: 532089 Color: 4
Size: 461443 Color: 1

Bin 787: 6475 of cap free
Amount of items: 2
Items: 
Size: 795357 Color: 0
Size: 198169 Color: 1

Bin 788: 6529 of cap free
Amount of items: 2
Items: 
Size: 733697 Color: 2
Size: 259775 Color: 0

Bin 789: 6553 of cap free
Amount of items: 2
Items: 
Size: 696488 Color: 2
Size: 296960 Color: 3

Bin 790: 6646 of cap free
Amount of items: 2
Items: 
Size: 675276 Color: 0
Size: 318079 Color: 4

Bin 791: 6825 of cap free
Amount of items: 2
Items: 
Size: 661314 Color: 4
Size: 331862 Color: 2

Bin 792: 6866 of cap free
Amount of items: 2
Items: 
Size: 795372 Color: 1
Size: 197763 Color: 4

Bin 793: 7097 of cap free
Amount of items: 2
Items: 
Size: 510712 Color: 4
Size: 482192 Color: 2

Bin 794: 7417 of cap free
Amount of items: 2
Items: 
Size: 794837 Color: 0
Size: 197747 Color: 2

Bin 795: 7509 of cap free
Amount of items: 2
Items: 
Size: 751979 Color: 3
Size: 240513 Color: 1

Bin 796: 8031 of cap free
Amount of items: 2
Items: 
Size: 561168 Color: 0
Size: 430802 Color: 2

Bin 797: 8043 of cap free
Amount of items: 2
Items: 
Size: 684488 Color: 4
Size: 307470 Color: 3

Bin 798: 8045 of cap free
Amount of items: 2
Items: 
Size: 733065 Color: 2
Size: 258891 Color: 0

Bin 799: 8272 of cap free
Amount of items: 2
Items: 
Size: 497306 Color: 3
Size: 494423 Color: 1

Bin 800: 8275 of cap free
Amount of items: 2
Items: 
Size: 751807 Color: 1
Size: 239919 Color: 0

Bin 801: 8288 of cap free
Amount of items: 2
Items: 
Size: 795044 Color: 2
Size: 196669 Color: 4

Bin 802: 8378 of cap free
Amount of items: 2
Items: 
Size: 617409 Color: 0
Size: 374214 Color: 2

Bin 803: 8435 of cap free
Amount of items: 2
Items: 
Size: 510460 Color: 4
Size: 481106 Color: 2

Bin 804: 8583 of cap free
Amount of items: 2
Items: 
Size: 751759 Color: 2
Size: 239659 Color: 0

Bin 805: 8843 of cap free
Amount of items: 2
Items: 
Size: 751746 Color: 3
Size: 239412 Color: 0

Bin 806: 8865 of cap free
Amount of items: 2
Items: 
Size: 673074 Color: 3
Size: 318062 Color: 2

Bin 807: 8954 of cap free
Amount of items: 2
Items: 
Size: 617188 Color: 0
Size: 373859 Color: 4

Bin 808: 9130 of cap free
Amount of items: 2
Items: 
Size: 751483 Color: 4
Size: 239388 Color: 0

Bin 809: 9318 of cap free
Amount of items: 2
Items: 
Size: 794477 Color: 3
Size: 196206 Color: 4

Bin 810: 9921 of cap free
Amount of items: 2
Items: 
Size: 793831 Color: 4
Size: 196249 Color: 3

Bin 811: 9999 of cap free
Amount of items: 2
Items: 
Size: 684267 Color: 2
Size: 305735 Color: 3

Bin 812: 10080 of cap free
Amount of items: 2
Items: 
Size: 750594 Color: 3
Size: 239327 Color: 1

Bin 813: 10150 of cap free
Amount of items: 2
Items: 
Size: 509034 Color: 0
Size: 480817 Color: 3

Bin 814: 10466 of cap free
Amount of items: 2
Items: 
Size: 793527 Color: 0
Size: 196008 Color: 3

Bin 815: 10611 of cap free
Amount of items: 7
Items: 
Size: 144677 Color: 2
Size: 141572 Color: 1
Size: 141362 Color: 3
Size: 140743 Color: 2
Size: 140371 Color: 4
Size: 140369 Color: 0
Size: 140296 Color: 3

Bin 816: 10638 of cap free
Amount of items: 2
Items: 
Size: 580725 Color: 1
Size: 408638 Color: 3

Bin 817: 11059 of cap free
Amount of items: 2
Items: 
Size: 670878 Color: 2
Size: 318064 Color: 3

Bin 818: 11144 of cap free
Amount of items: 2
Items: 
Size: 749944 Color: 0
Size: 238913 Color: 1

Bin 819: 11351 of cap free
Amount of items: 2
Items: 
Size: 670849 Color: 3
Size: 317801 Color: 1

Bin 820: 11643 of cap free
Amount of items: 2
Items: 
Size: 580219 Color: 1
Size: 408139 Color: 3

Bin 821: 12216 of cap free
Amount of items: 2
Items: 
Size: 598352 Color: 0
Size: 389433 Color: 4

Bin 822: 12329 of cap free
Amount of items: 2
Items: 
Size: 696887 Color: 3
Size: 290785 Color: 0

Bin 823: 12424 of cap free
Amount of items: 2
Items: 
Size: 507226 Color: 0
Size: 480351 Color: 4

Bin 824: 12528 of cap free
Amount of items: 2
Items: 
Size: 598329 Color: 0
Size: 389144 Color: 2

Bin 825: 12617 of cap free
Amount of items: 2
Items: 
Size: 670444 Color: 0
Size: 316940 Color: 2

Bin 826: 12627 of cap free
Amount of items: 2
Items: 
Size: 532345 Color: 1
Size: 455029 Color: 0

Bin 827: 12891 of cap free
Amount of items: 2
Items: 
Size: 598063 Color: 2
Size: 389047 Color: 0

Bin 828: 13034 of cap free
Amount of items: 2
Items: 
Size: 696405 Color: 2
Size: 290562 Color: 3

Bin 829: 13824 of cap free
Amount of items: 2
Items: 
Size: 696179 Color: 4
Size: 289998 Color: 1

Bin 830: 14468 of cap free
Amount of items: 2
Items: 
Size: 696112 Color: 4
Size: 289421 Color: 1

Bin 831: 14598 of cap free
Amount of items: 2
Items: 
Size: 530981 Color: 1
Size: 454422 Color: 2

Bin 832: 14655 of cap free
Amount of items: 2
Items: 
Size: 596806 Color: 1
Size: 388540 Color: 0

Bin 833: 14884 of cap free
Amount of items: 2
Items: 
Size: 596777 Color: 3
Size: 388340 Color: 1

Bin 834: 14993 of cap free
Amount of items: 2
Items: 
Size: 627169 Color: 1
Size: 357839 Color: 2

Bin 835: 15211 of cap free
Amount of items: 2
Items: 
Size: 596481 Color: 3
Size: 388309 Color: 0

Bin 836: 15718 of cap free
Amount of items: 2
Items: 
Size: 596460 Color: 3
Size: 387823 Color: 2

Bin 837: 15897 of cap free
Amount of items: 2
Items: 
Size: 695983 Color: 3
Size: 288121 Color: 1

Bin 838: 16267 of cap free
Amount of items: 2
Items: 
Size: 531824 Color: 2
Size: 451910 Color: 4

Bin 839: 17316 of cap free
Amount of items: 2
Items: 
Size: 506644 Color: 0
Size: 476041 Color: 2

Bin 840: 18497 of cap free
Amount of items: 2
Items: 
Size: 505883 Color: 2
Size: 475621 Color: 0

Bin 841: 18536 of cap free
Amount of items: 2
Items: 
Size: 506554 Color: 0
Size: 474911 Color: 3

Bin 842: 19145 of cap free
Amount of items: 2
Items: 
Size: 530550 Color: 0
Size: 450306 Color: 1

Bin 843: 19623 of cap free
Amount of items: 2
Items: 
Size: 694824 Color: 4
Size: 285554 Color: 2

Bin 844: 20103 of cap free
Amount of items: 2
Items: 
Size: 726276 Color: 2
Size: 253622 Color: 3

Bin 845: 20757 of cap free
Amount of items: 2
Items: 
Size: 726525 Color: 3
Size: 252719 Color: 4

Bin 846: 21584 of cap free
Amount of items: 2
Items: 
Size: 528303 Color: 1
Size: 450114 Color: 3

Bin 847: 22277 of cap free
Amount of items: 2
Items: 
Size: 616633 Color: 3
Size: 361091 Color: 1

Bin 848: 22565 of cap free
Amount of items: 2
Items: 
Size: 661190 Color: 0
Size: 316246 Color: 3

Bin 849: 23230 of cap free
Amount of items: 5
Items: 
Size: 195775 Color: 2
Size: 195481 Color: 3
Size: 195332 Color: 3
Size: 195219 Color: 4
Size: 194964 Color: 4

Bin 850: 23771 of cap free
Amount of items: 2
Items: 
Size: 723408 Color: 0
Size: 252822 Color: 3

Bin 851: 24148 of cap free
Amount of items: 2
Items: 
Size: 528445 Color: 3
Size: 447408 Color: 2

Bin 852: 25056 of cap free
Amount of items: 2
Items: 
Size: 722989 Color: 1
Size: 251956 Color: 4

Bin 853: 25403 of cap free
Amount of items: 7
Items: 
Size: 140050 Color: 3
Size: 139680 Color: 4
Size: 139368 Color: 1
Size: 139343 Color: 2
Size: 138898 Color: 4
Size: 138747 Color: 2
Size: 138512 Color: 0

Bin 854: 26168 of cap free
Amount of items: 6
Items: 
Size: 163581 Color: 1
Size: 162388 Color: 4
Size: 162107 Color: 4
Size: 162085 Color: 1
Size: 162057 Color: 1
Size: 161615 Color: 4

Bin 855: 27150 of cap free
Amount of items: 2
Items: 
Size: 614990 Color: 3
Size: 357861 Color: 1

Bin 856: 27752 of cap free
Amount of items: 2
Items: 
Size: 720503 Color: 1
Size: 251746 Color: 4

Bin 857: 28001 of cap free
Amount of items: 2
Items: 
Size: 719458 Color: 3
Size: 252542 Color: 1

Bin 858: 28330 of cap free
Amount of items: 8
Items: 
Size: 129860 Color: 1
Size: 121205 Color: 3
Size: 120950 Color: 2
Size: 120852 Color: 1
Size: 120457 Color: 2
Size: 119601 Color: 1
Size: 119494 Color: 2
Size: 119252 Color: 3

Bin 859: 28752 of cap free
Amount of items: 4
Items: 
Size: 250397 Color: 3
Size: 250153 Color: 1
Size: 236319 Color: 1
Size: 234380 Color: 0

Bin 860: 29594 of cap free
Amount of items: 2
Items: 
Size: 719360 Color: 2
Size: 251047 Color: 0

Bin 861: 29987 of cap free
Amount of items: 2
Items: 
Size: 684435 Color: 3
Size: 285579 Color: 4

Bin 862: 30671 of cap free
Amount of items: 2
Items: 
Size: 718560 Color: 4
Size: 250770 Color: 2

Bin 863: 32237 of cap free
Amount of items: 5
Items: 
Size: 194259 Color: 0
Size: 194171 Color: 2
Size: 193319 Color: 2
Size: 193274 Color: 0
Size: 192741 Color: 3

Bin 864: 32381 of cap free
Amount of items: 2
Items: 
Size: 684031 Color: 1
Size: 283589 Color: 2

Bin 865: 32484 of cap free
Amount of items: 2
Items: 
Size: 716953 Color: 4
Size: 250564 Color: 2

Bin 866: 33285 of cap free
Amount of items: 9
Items: 
Size: 109238 Color: 2
Size: 109190 Color: 4
Size: 107696 Color: 3
Size: 107340 Color: 3
Size: 107309 Color: 0
Size: 106839 Color: 4
Size: 106676 Color: 1
Size: 106291 Color: 2
Size: 106137 Color: 4

Bin 867: 34313 of cap free
Amount of items: 2
Items: 
Size: 591937 Color: 0
Size: 373751 Color: 3

Bin 868: 35115 of cap free
Amount of items: 2
Items: 
Size: 683785 Color: 1
Size: 281101 Color: 4

Bin 869: 36009 of cap free
Amount of items: 7
Items: 
Size: 138352 Color: 0
Size: 138172 Color: 1
Size: 138020 Color: 0
Size: 137575 Color: 3
Size: 137491 Color: 1
Size: 137405 Color: 3
Size: 136977 Color: 2

Bin 870: 40653 of cap free
Amount of items: 6
Items: 
Size: 160776 Color: 1
Size: 160368 Color: 4
Size: 160193 Color: 1
Size: 159647 Color: 3
Size: 159246 Color: 1
Size: 159118 Color: 0

Bin 871: 43672 of cap free
Amount of items: 5
Items: 
Size: 191962 Color: 1
Size: 191639 Color: 3
Size: 191372 Color: 1
Size: 191062 Color: 0
Size: 190294 Color: 3

Bin 872: 47731 of cap free
Amount of items: 8
Items: 
Size: 125464 Color: 1
Size: 119038 Color: 0
Size: 118468 Color: 0
Size: 118391 Color: 2
Size: 118072 Color: 2
Size: 117865 Color: 4
Size: 117557 Color: 0
Size: 117415 Color: 0

Bin 873: 50196 of cap free
Amount of items: 5
Items: 
Size: 191063 Color: 1
Size: 190288 Color: 2
Size: 189886 Color: 4
Size: 189708 Color: 4
Size: 188860 Color: 1

Bin 874: 51679 of cap free
Amount of items: 7
Items: 
Size: 136875 Color: 2
Size: 136606 Color: 3
Size: 136344 Color: 0
Size: 136150 Color: 1
Size: 135460 Color: 2
Size: 133497 Color: 4
Size: 133390 Color: 1

Bin 875: 55458 of cap free
Amount of items: 2
Items: 
Size: 591387 Color: 3
Size: 353156 Color: 2

Bin 876: 56367 of cap free
Amount of items: 6
Items: 
Size: 158889 Color: 0
Size: 158688 Color: 2
Size: 157010 Color: 4
Size: 156679 Color: 1
Size: 156299 Color: 1
Size: 156069 Color: 4

Bin 877: 58927 of cap free
Amount of items: 9
Items: 
Size: 105406 Color: 3
Size: 105281 Color: 0
Size: 104940 Color: 3
Size: 104460 Color: 2
Size: 104345 Color: 1
Size: 104335 Color: 1
Size: 104247 Color: 4
Size: 104212 Color: 2
Size: 103848 Color: 2

Bin 878: 61034 of cap free
Amount of items: 2
Items: 
Size: 586715 Color: 3
Size: 352252 Color: 2

Bin 879: 63227 of cap free
Amount of items: 4
Items: 
Size: 234452 Color: 1
Size: 234313 Color: 0
Size: 234032 Color: 0
Size: 233977 Color: 1

Bin 880: 65800 of cap free
Amount of items: 2
Items: 
Size: 580075 Color: 2
Size: 354126 Color: 3

Bin 881: 66068 of cap free
Amount of items: 5
Items: 
Size: 188757 Color: 1
Size: 186496 Color: 0
Size: 186484 Color: 2
Size: 186336 Color: 4
Size: 185860 Color: 3

Bin 882: 69504 of cap free
Amount of items: 2
Items: 
Size: 577056 Color: 1
Size: 353441 Color: 3

Bin 883: 70654 of cap free
Amount of items: 2
Items: 
Size: 577053 Color: 2
Size: 352294 Color: 3

Bin 884: 74008 of cap free
Amount of items: 6
Items: 
Size: 155846 Color: 1
Size: 155572 Color: 4
Size: 154635 Color: 1
Size: 154128 Color: 3
Size: 152973 Color: 3
Size: 152839 Color: 3

Bin 885: 75126 of cap free
Amount of items: 5
Items: 
Size: 188365 Color: 1
Size: 184713 Color: 4
Size: 184457 Color: 1
Size: 183813 Color: 2
Size: 183527 Color: 1

Bin 886: 75401 of cap free
Amount of items: 2
Items: 
Size: 572374 Color: 4
Size: 352226 Color: 3

Bin 887: 75560 of cap free
Amount of items: 2
Items: 
Size: 572426 Color: 3
Size: 352015 Color: 4

Bin 888: 75775 of cap free
Amount of items: 2
Items: 
Size: 572322 Color: 3
Size: 351904 Color: 0

Bin 889: 76902 of cap free
Amount of items: 7
Items: 
Size: 133383 Color: 4
Size: 132163 Color: 3
Size: 132007 Color: 2
Size: 131674 Color: 3
Size: 131651 Color: 0
Size: 131220 Color: 4
Size: 131001 Color: 1

Bin 890: 78887 of cap free
Amount of items: 2
Items: 
Size: 569351 Color: 1
Size: 351763 Color: 3

Bin 891: 79545 of cap free
Amount of items: 2
Items: 
Size: 569364 Color: 3
Size: 351092 Color: 0

Bin 892: 79851 of cap free
Amount of items: 8
Items: 
Size: 119133 Color: 1
Size: 116988 Color: 2
Size: 115467 Color: 3
Size: 115218 Color: 4
Size: 114644 Color: 0
Size: 112973 Color: 0
Size: 112916 Color: 4
Size: 112811 Color: 0

Bin 893: 81529 of cap free
Amount of items: 5
Items: 
Size: 187822 Color: 1
Size: 183065 Color: 0
Size: 182621 Color: 3
Size: 182542 Color: 0
Size: 182422 Color: 2

Bin 894: 83907 of cap free
Amount of items: 9
Items: 
Size: 103672 Color: 2
Size: 103481 Color: 4
Size: 101964 Color: 4
Size: 101894 Color: 2
Size: 101378 Color: 0
Size: 101199 Color: 4
Size: 101119 Color: 3
Size: 100801 Color: 2
Size: 100586 Color: 2

Bin 895: 87383 of cap free
Amount of items: 2
Items: 
Size: 567461 Color: 2
Size: 345157 Color: 1

Bin 896: 90511 of cap free
Amount of items: 4
Items: 
Size: 232387 Color: 3
Size: 229231 Color: 1
Size: 224149 Color: 3
Size: 223723 Color: 0

Bin 897: 93930 of cap free
Amount of items: 2
Items: 
Size: 561150 Color: 0
Size: 344921 Color: 3

Bin 898: 96455 of cap free
Amount of items: 5
Items: 
Size: 181780 Color: 2
Size: 181131 Color: 0
Size: 180772 Color: 4
Size: 180008 Color: 4
Size: 179855 Color: 3

Bin 899: 97222 of cap free
Amount of items: 6
Items: 
Size: 152600 Color: 4
Size: 150487 Color: 3
Size: 150238 Color: 0
Size: 150026 Color: 1
Size: 149952 Color: 4
Size: 149476 Color: 2

Bin 900: 99289 of cap free
Amount of items: 4
Items: 
Size: 231091 Color: 3
Size: 223590 Color: 0
Size: 223554 Color: 4
Size: 222477 Color: 0

Bin 901: 100086 of cap free
Amount of items: 2
Items: 
Size: 556118 Color: 4
Size: 343797 Color: 2

Total size: 896617650
Total free space: 4383251

