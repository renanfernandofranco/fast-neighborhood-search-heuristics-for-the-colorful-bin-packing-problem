Capicity Bin: 1000001
Lower Bound: 223

Bins used: 226
Amount of Colors: 5

Bin 1: 14 of cap free
Amount of items: 2
Items: 
Size: 596303 Color: 1
Size: 403684 Color: 2

Bin 2: 19 of cap free
Amount of items: 2
Items: 
Size: 667804 Color: 0
Size: 332178 Color: 3

Bin 3: 47 of cap free
Amount of items: 2
Items: 
Size: 757510 Color: 1
Size: 242444 Color: 4

Bin 4: 54 of cap free
Amount of items: 2
Items: 
Size: 585548 Color: 4
Size: 414399 Color: 1

Bin 5: 56 of cap free
Amount of items: 2
Items: 
Size: 509877 Color: 0
Size: 490068 Color: 1

Bin 6: 80 of cap free
Amount of items: 2
Items: 
Size: 620636 Color: 3
Size: 379285 Color: 4

Bin 7: 96 of cap free
Amount of items: 2
Items: 
Size: 744778 Color: 3
Size: 255127 Color: 1

Bin 8: 104 of cap free
Amount of items: 2
Items: 
Size: 536181 Color: 0
Size: 463716 Color: 1

Bin 9: 107 of cap free
Amount of items: 2
Items: 
Size: 526675 Color: 3
Size: 473219 Color: 0

Bin 10: 107 of cap free
Amount of items: 2
Items: 
Size: 629447 Color: 4
Size: 370447 Color: 2

Bin 11: 109 of cap free
Amount of items: 2
Items: 
Size: 723059 Color: 2
Size: 276833 Color: 4

Bin 12: 110 of cap free
Amount of items: 2
Items: 
Size: 645504 Color: 4
Size: 354387 Color: 3

Bin 13: 112 of cap free
Amount of items: 2
Items: 
Size: 687216 Color: 4
Size: 312673 Color: 2

Bin 14: 127 of cap free
Amount of items: 2
Items: 
Size: 755802 Color: 3
Size: 244072 Color: 1

Bin 15: 128 of cap free
Amount of items: 2
Items: 
Size: 507507 Color: 3
Size: 492366 Color: 1

Bin 16: 128 of cap free
Amount of items: 2
Items: 
Size: 516930 Color: 4
Size: 482943 Color: 0

Bin 17: 140 of cap free
Amount of items: 2
Items: 
Size: 513247 Color: 0
Size: 486614 Color: 3

Bin 18: 147 of cap free
Amount of items: 2
Items: 
Size: 725564 Color: 0
Size: 274290 Color: 2

Bin 19: 154 of cap free
Amount of items: 2
Items: 
Size: 526453 Color: 4
Size: 473394 Color: 0

Bin 20: 174 of cap free
Amount of items: 2
Items: 
Size: 774129 Color: 1
Size: 225698 Color: 4

Bin 21: 175 of cap free
Amount of items: 2
Items: 
Size: 745429 Color: 3
Size: 254397 Color: 0

Bin 22: 177 of cap free
Amount of items: 2
Items: 
Size: 654012 Color: 0
Size: 345812 Color: 3

Bin 23: 188 of cap free
Amount of items: 2
Items: 
Size: 508980 Color: 1
Size: 490833 Color: 0

Bin 24: 190 of cap free
Amount of items: 2
Items: 
Size: 517531 Color: 3
Size: 482280 Color: 0

Bin 25: 196 of cap free
Amount of items: 2
Items: 
Size: 736512 Color: 0
Size: 263293 Color: 2

Bin 26: 201 of cap free
Amount of items: 2
Items: 
Size: 656750 Color: 0
Size: 343050 Color: 3

Bin 27: 202 of cap free
Amount of items: 3
Items: 
Size: 701965 Color: 2
Size: 196236 Color: 0
Size: 101598 Color: 1

Bin 28: 206 of cap free
Amount of items: 2
Items: 
Size: 676027 Color: 3
Size: 323768 Color: 2

Bin 29: 223 of cap free
Amount of items: 2
Items: 
Size: 774824 Color: 4
Size: 224954 Color: 1

Bin 30: 256 of cap free
Amount of items: 2
Items: 
Size: 560029 Color: 1
Size: 439716 Color: 4

Bin 31: 260 of cap free
Amount of items: 2
Items: 
Size: 733422 Color: 4
Size: 266319 Color: 0

Bin 32: 275 of cap free
Amount of items: 2
Items: 
Size: 540746 Color: 1
Size: 458980 Color: 2

Bin 33: 300 of cap free
Amount of items: 2
Items: 
Size: 694738 Color: 0
Size: 304963 Color: 2

Bin 34: 302 of cap free
Amount of items: 7
Items: 
Size: 158933 Color: 2
Size: 145383 Color: 3
Size: 144807 Color: 0
Size: 143913 Color: 0
Size: 142927 Color: 1
Size: 139621 Color: 4
Size: 124115 Color: 2

Bin 35: 307 of cap free
Amount of items: 2
Items: 
Size: 698443 Color: 0
Size: 301251 Color: 4

Bin 36: 308 of cap free
Amount of items: 2
Items: 
Size: 703950 Color: 0
Size: 295743 Color: 4

Bin 37: 329 of cap free
Amount of items: 2
Items: 
Size: 632790 Color: 1
Size: 366882 Color: 3

Bin 38: 331 of cap free
Amount of items: 2
Items: 
Size: 574179 Color: 4
Size: 425491 Color: 3

Bin 39: 331 of cap free
Amount of items: 2
Items: 
Size: 686034 Color: 3
Size: 313636 Color: 1

Bin 40: 335 of cap free
Amount of items: 2
Items: 
Size: 604830 Color: 3
Size: 394836 Color: 0

Bin 41: 339 of cap free
Amount of items: 2
Items: 
Size: 679414 Color: 1
Size: 320248 Color: 3

Bin 42: 344 of cap free
Amount of items: 2
Items: 
Size: 763649 Color: 0
Size: 236008 Color: 1

Bin 43: 352 of cap free
Amount of items: 2
Items: 
Size: 675321 Color: 1
Size: 324328 Color: 4

Bin 44: 360 of cap free
Amount of items: 2
Items: 
Size: 766692 Color: 3
Size: 232949 Color: 0

Bin 45: 361 of cap free
Amount of items: 2
Items: 
Size: 712630 Color: 1
Size: 287010 Color: 0

Bin 46: 390 of cap free
Amount of items: 2
Items: 
Size: 592218 Color: 3
Size: 407393 Color: 1

Bin 47: 391 of cap free
Amount of items: 2
Items: 
Size: 505737 Color: 2
Size: 493873 Color: 3

Bin 48: 397 of cap free
Amount of items: 2
Items: 
Size: 741285 Color: 4
Size: 258319 Color: 2

Bin 49: 403 of cap free
Amount of items: 2
Items: 
Size: 504799 Color: 1
Size: 494799 Color: 3

Bin 50: 415 of cap free
Amount of items: 2
Items: 
Size: 681960 Color: 4
Size: 317626 Color: 3

Bin 51: 441 of cap free
Amount of items: 2
Items: 
Size: 731256 Color: 1
Size: 268304 Color: 4

Bin 52: 465 of cap free
Amount of items: 2
Items: 
Size: 749341 Color: 2
Size: 250195 Color: 3

Bin 53: 467 of cap free
Amount of items: 2
Items: 
Size: 696537 Color: 4
Size: 302997 Color: 0

Bin 54: 489 of cap free
Amount of items: 2
Items: 
Size: 633775 Color: 4
Size: 365737 Color: 2

Bin 55: 506 of cap free
Amount of items: 2
Items: 
Size: 577784 Color: 2
Size: 421711 Color: 4

Bin 56: 511 of cap free
Amount of items: 2
Items: 
Size: 606379 Color: 4
Size: 393111 Color: 3

Bin 57: 511 of cap free
Amount of items: 2
Items: 
Size: 730159 Color: 2
Size: 269331 Color: 1

Bin 58: 519 of cap free
Amount of items: 2
Items: 
Size: 556707 Color: 3
Size: 442775 Color: 1

Bin 59: 526 of cap free
Amount of items: 2
Items: 
Size: 791770 Color: 2
Size: 207705 Color: 3

Bin 60: 531 of cap free
Amount of items: 2
Items: 
Size: 554706 Color: 3
Size: 444764 Color: 4

Bin 61: 538 of cap free
Amount of items: 2
Items: 
Size: 736389 Color: 1
Size: 263074 Color: 0

Bin 62: 543 of cap free
Amount of items: 2
Items: 
Size: 690830 Color: 2
Size: 308628 Color: 0

Bin 63: 546 of cap free
Amount of items: 2
Items: 
Size: 534435 Color: 3
Size: 465020 Color: 1

Bin 64: 548 of cap free
Amount of items: 3
Items: 
Size: 692321 Color: 0
Size: 195917 Color: 2
Size: 111215 Color: 2

Bin 65: 566 of cap free
Amount of items: 3
Items: 
Size: 389182 Color: 1
Size: 377336 Color: 3
Size: 232917 Color: 4

Bin 66: 587 of cap free
Amount of items: 2
Items: 
Size: 689018 Color: 3
Size: 310396 Color: 1

Bin 67: 609 of cap free
Amount of items: 2
Items: 
Size: 538549 Color: 0
Size: 460843 Color: 1

Bin 68: 609 of cap free
Amount of items: 2
Items: 
Size: 780056 Color: 2
Size: 219336 Color: 0

Bin 69: 626 of cap free
Amount of items: 2
Items: 
Size: 686493 Color: 0
Size: 312882 Color: 4

Bin 70: 679 of cap free
Amount of items: 2
Items: 
Size: 613672 Color: 3
Size: 385650 Color: 0

Bin 71: 690 of cap free
Amount of items: 2
Items: 
Size: 516753 Color: 0
Size: 482558 Color: 1

Bin 72: 738 of cap free
Amount of items: 2
Items: 
Size: 793873 Color: 1
Size: 205390 Color: 3

Bin 73: 761 of cap free
Amount of items: 2
Items: 
Size: 705881 Color: 4
Size: 293359 Color: 3

Bin 74: 774 of cap free
Amount of items: 2
Items: 
Size: 641124 Color: 3
Size: 358103 Color: 0

Bin 75: 785 of cap free
Amount of items: 2
Items: 
Size: 676506 Color: 3
Size: 322710 Color: 0

Bin 76: 788 of cap free
Amount of items: 2
Items: 
Size: 540707 Color: 2
Size: 458506 Color: 4

Bin 77: 858 of cap free
Amount of items: 2
Items: 
Size: 617421 Color: 2
Size: 381722 Color: 1

Bin 78: 860 of cap free
Amount of items: 2
Items: 
Size: 691158 Color: 0
Size: 307983 Color: 4

Bin 79: 982 of cap free
Amount of items: 2
Items: 
Size: 561865 Color: 2
Size: 437154 Color: 3

Bin 80: 1003 of cap free
Amount of items: 2
Items: 
Size: 745967 Color: 0
Size: 253031 Color: 2

Bin 81: 1013 of cap free
Amount of items: 2
Items: 
Size: 647306 Color: 2
Size: 351682 Color: 4

Bin 82: 1033 of cap free
Amount of items: 2
Items: 
Size: 529429 Color: 3
Size: 469539 Color: 2

Bin 83: 1044 of cap free
Amount of items: 2
Items: 
Size: 761921 Color: 1
Size: 237036 Color: 3

Bin 84: 1080 of cap free
Amount of items: 2
Items: 
Size: 602831 Color: 2
Size: 396090 Color: 1

Bin 85: 1084 of cap free
Amount of items: 2
Items: 
Size: 546367 Color: 4
Size: 452550 Color: 2

Bin 86: 1107 of cap free
Amount of items: 2
Items: 
Size: 558282 Color: 1
Size: 440612 Color: 2

Bin 87: 1115 of cap free
Amount of items: 2
Items: 
Size: 625345 Color: 0
Size: 373541 Color: 2

Bin 88: 1122 of cap free
Amount of items: 2
Items: 
Size: 566291 Color: 1
Size: 432588 Color: 4

Bin 89: 1204 of cap free
Amount of items: 2
Items: 
Size: 582941 Color: 3
Size: 415856 Color: 0

Bin 90: 1248 of cap free
Amount of items: 2
Items: 
Size: 759340 Color: 4
Size: 239413 Color: 0

Bin 91: 1262 of cap free
Amount of items: 2
Items: 
Size: 776943 Color: 2
Size: 221796 Color: 0

Bin 92: 1311 of cap free
Amount of items: 2
Items: 
Size: 638098 Color: 2
Size: 360592 Color: 1

Bin 93: 1327 of cap free
Amount of items: 2
Items: 
Size: 601419 Color: 3
Size: 397255 Color: 1

Bin 94: 1343 of cap free
Amount of items: 2
Items: 
Size: 789498 Color: 3
Size: 209160 Color: 0

Bin 95: 1355 of cap free
Amount of items: 2
Items: 
Size: 669118 Color: 3
Size: 329528 Color: 1

Bin 96: 1356 of cap free
Amount of items: 3
Items: 
Size: 343747 Color: 2
Size: 332586 Color: 3
Size: 322312 Color: 3

Bin 97: 1383 of cap free
Amount of items: 2
Items: 
Size: 677923 Color: 0
Size: 320695 Color: 2

Bin 98: 1385 of cap free
Amount of items: 2
Items: 
Size: 750162 Color: 1
Size: 248454 Color: 4

Bin 99: 1434 of cap free
Amount of items: 2
Items: 
Size: 752016 Color: 3
Size: 246551 Color: 2

Bin 100: 1457 of cap free
Amount of items: 2
Items: 
Size: 742745 Color: 2
Size: 255799 Color: 0

Bin 101: 1460 of cap free
Amount of items: 2
Items: 
Size: 626756 Color: 4
Size: 371785 Color: 2

Bin 102: 1478 of cap free
Amount of items: 2
Items: 
Size: 569023 Color: 4
Size: 429500 Color: 0

Bin 103: 1520 of cap free
Amount of items: 2
Items: 
Size: 594437 Color: 2
Size: 404044 Color: 4

Bin 104: 1592 of cap free
Amount of items: 3
Items: 
Size: 377299 Color: 0
Size: 376868 Color: 3
Size: 244242 Color: 1

Bin 105: 1613 of cap free
Amount of items: 2
Items: 
Size: 735595 Color: 4
Size: 262793 Color: 3

Bin 106: 1619 of cap free
Amount of items: 2
Items: 
Size: 558734 Color: 2
Size: 439648 Color: 0

Bin 107: 1651 of cap free
Amount of items: 2
Items: 
Size: 729585 Color: 3
Size: 268765 Color: 2

Bin 108: 1707 of cap free
Amount of items: 2
Items: 
Size: 659640 Color: 1
Size: 338654 Color: 0

Bin 109: 1769 of cap free
Amount of items: 2
Items: 
Size: 723187 Color: 4
Size: 275045 Color: 1

Bin 110: 1777 of cap free
Amount of items: 2
Items: 
Size: 776566 Color: 1
Size: 221658 Color: 2

Bin 111: 1810 of cap free
Amount of items: 2
Items: 
Size: 699260 Color: 0
Size: 298931 Color: 3

Bin 112: 1839 of cap free
Amount of items: 2
Items: 
Size: 561771 Color: 3
Size: 436391 Color: 2

Bin 113: 1839 of cap free
Amount of items: 2
Items: 
Size: 670557 Color: 2
Size: 327605 Color: 3

Bin 114: 1850 of cap free
Amount of items: 2
Items: 
Size: 537424 Color: 0
Size: 460727 Color: 2

Bin 115: 1852 of cap free
Amount of items: 2
Items: 
Size: 748041 Color: 1
Size: 250108 Color: 3

Bin 116: 1898 of cap free
Amount of items: 2
Items: 
Size: 500726 Color: 2
Size: 497377 Color: 3

Bin 117: 1909 of cap free
Amount of items: 2
Items: 
Size: 618591 Color: 0
Size: 379501 Color: 4

Bin 118: 1950 of cap free
Amount of items: 2
Items: 
Size: 546189 Color: 2
Size: 451862 Color: 0

Bin 119: 1970 of cap free
Amount of items: 2
Items: 
Size: 503526 Color: 3
Size: 494505 Color: 0

Bin 120: 1990 of cap free
Amount of items: 2
Items: 
Size: 761284 Color: 0
Size: 236727 Color: 1

Bin 121: 1997 of cap free
Amount of items: 2
Items: 
Size: 518318 Color: 0
Size: 479686 Color: 4

Bin 122: 2023 of cap free
Amount of items: 2
Items: 
Size: 626390 Color: 2
Size: 371588 Color: 3

Bin 123: 2085 of cap free
Amount of items: 2
Items: 
Size: 614586 Color: 3
Size: 383330 Color: 1

Bin 124: 2201 of cap free
Amount of items: 5
Items: 
Size: 312178 Color: 0
Size: 194200 Color: 4
Size: 193872 Color: 1
Size: 193320 Color: 0
Size: 104230 Color: 4

Bin 125: 2230 of cap free
Amount of items: 5
Items: 
Size: 298117 Color: 0
Size: 178322 Color: 3
Size: 178096 Color: 2
Size: 173652 Color: 2
Size: 169584 Color: 4

Bin 126: 2254 of cap free
Amount of items: 2
Items: 
Size: 708765 Color: 4
Size: 288982 Color: 2

Bin 127: 2266 of cap free
Amount of items: 2
Items: 
Size: 696451 Color: 2
Size: 301284 Color: 0

Bin 128: 2272 of cap free
Amount of items: 2
Items: 
Size: 703311 Color: 2
Size: 294418 Color: 1

Bin 129: 2296 of cap free
Amount of items: 2
Items: 
Size: 732470 Color: 1
Size: 265235 Color: 2

Bin 130: 2316 of cap free
Amount of items: 2
Items: 
Size: 796392 Color: 0
Size: 201293 Color: 1

Bin 131: 2341 of cap free
Amount of items: 2
Items: 
Size: 763200 Color: 1
Size: 234460 Color: 4

Bin 132: 2378 of cap free
Amount of items: 2
Items: 
Size: 575955 Color: 4
Size: 421668 Color: 1

Bin 133: 2406 of cap free
Amount of items: 2
Items: 
Size: 590432 Color: 4
Size: 407163 Color: 2

Bin 134: 2440 of cap free
Amount of items: 2
Items: 
Size: 718478 Color: 0
Size: 279083 Color: 3

Bin 135: 2447 of cap free
Amount of items: 2
Items: 
Size: 602451 Color: 1
Size: 395103 Color: 3

Bin 136: 2527 of cap free
Amount of items: 3
Items: 
Size: 365612 Color: 2
Size: 363736 Color: 1
Size: 268126 Color: 3

Bin 137: 2745 of cap free
Amount of items: 2
Items: 
Size: 782489 Color: 1
Size: 214767 Color: 0

Bin 138: 2914 of cap free
Amount of items: 2
Items: 
Size: 616721 Color: 3
Size: 380366 Color: 0

Bin 139: 2928 of cap free
Amount of items: 5
Items: 
Size: 312108 Color: 0
Size: 192529 Color: 2
Size: 192461 Color: 3
Size: 192377 Color: 2
Size: 107598 Color: 1

Bin 140: 2962 of cap free
Amount of items: 2
Items: 
Size: 639062 Color: 1
Size: 357977 Color: 0

Bin 141: 2978 of cap free
Amount of items: 2
Items: 
Size: 663673 Color: 3
Size: 333350 Color: 0

Bin 142: 3022 of cap free
Amount of items: 2
Items: 
Size: 692860 Color: 2
Size: 304119 Color: 0

Bin 143: 3096 of cap free
Amount of items: 2
Items: 
Size: 500459 Color: 4
Size: 496446 Color: 3

Bin 144: 3116 of cap free
Amount of items: 2
Items: 
Size: 530620 Color: 1
Size: 466265 Color: 2

Bin 145: 3129 of cap free
Amount of items: 2
Items: 
Size: 737806 Color: 2
Size: 259066 Color: 3

Bin 146: 3228 of cap free
Amount of items: 2
Items: 
Size: 596035 Color: 4
Size: 400738 Color: 2

Bin 147: 3259 of cap free
Amount of items: 2
Items: 
Size: 646569 Color: 2
Size: 350173 Color: 3

Bin 148: 3388 of cap free
Amount of items: 2
Items: 
Size: 717845 Color: 0
Size: 278768 Color: 2

Bin 149: 3409 of cap free
Amount of items: 5
Items: 
Size: 307458 Color: 0
Size: 191677 Color: 1
Size: 191457 Color: 1
Size: 187942 Color: 2
Size: 118058 Color: 2

Bin 150: 3409 of cap free
Amount of items: 2
Items: 
Size: 579524 Color: 4
Size: 417068 Color: 1

Bin 151: 3489 of cap free
Amount of items: 2
Items: 
Size: 574956 Color: 3
Size: 421556 Color: 0

Bin 152: 3611 of cap free
Amount of items: 2
Items: 
Size: 535945 Color: 1
Size: 460445 Color: 2

Bin 153: 3637 of cap free
Amount of items: 2
Items: 
Size: 775571 Color: 4
Size: 220793 Color: 1

Bin 154: 3694 of cap free
Amount of items: 3
Items: 
Size: 435853 Color: 1
Size: 378039 Color: 0
Size: 182415 Color: 0

Bin 155: 3745 of cap free
Amount of items: 3
Items: 
Size: 473746 Color: 1
Size: 388729 Color: 3
Size: 133781 Color: 3

Bin 156: 3880 of cap free
Amount of items: 2
Items: 
Size: 782029 Color: 1
Size: 214092 Color: 3

Bin 157: 4083 of cap free
Amount of items: 2
Items: 
Size: 568640 Color: 2
Size: 427278 Color: 0

Bin 158: 4170 of cap free
Amount of items: 2
Items: 
Size: 663623 Color: 3
Size: 332208 Color: 4

Bin 159: 4229 of cap free
Amount of items: 2
Items: 
Size: 629099 Color: 0
Size: 366673 Color: 4

Bin 160: 4293 of cap free
Amount of items: 2
Items: 
Size: 511962 Color: 4
Size: 483746 Color: 3

Bin 161: 4635 of cap free
Amount of items: 2
Items: 
Size: 729358 Color: 3
Size: 266008 Color: 1

Bin 162: 4686 of cap free
Amount of items: 2
Items: 
Size: 723032 Color: 1
Size: 272283 Color: 3

Bin 163: 4773 of cap free
Amount of items: 2
Items: 
Size: 679757 Color: 1
Size: 315471 Color: 0

Bin 164: 4807 of cap free
Amount of items: 2
Items: 
Size: 556253 Color: 0
Size: 438941 Color: 4

Bin 165: 4894 of cap free
Amount of items: 2
Items: 
Size: 765600 Color: 4
Size: 229507 Color: 3

Bin 166: 5110 of cap free
Amount of items: 2
Items: 
Size: 544561 Color: 3
Size: 450330 Color: 4

Bin 167: 5201 of cap free
Amount of items: 2
Items: 
Size: 545256 Color: 4
Size: 449544 Color: 1

Bin 168: 5328 of cap free
Amount of items: 3
Items: 
Size: 692090 Color: 0
Size: 195751 Color: 4
Size: 106832 Color: 3

Bin 169: 5396 of cap free
Amount of items: 2
Items: 
Size: 696384 Color: 4
Size: 298221 Color: 3

Bin 170: 5450 of cap free
Amount of items: 2
Items: 
Size: 759301 Color: 3
Size: 235250 Color: 1

Bin 171: 5496 of cap free
Amount of items: 2
Items: 
Size: 578731 Color: 3
Size: 415774 Color: 1

Bin 172: 5654 of cap free
Amount of items: 2
Items: 
Size: 795317 Color: 0
Size: 199030 Color: 2

Bin 173: 5767 of cap free
Amount of items: 2
Items: 
Size: 668931 Color: 1
Size: 325303 Color: 0

Bin 174: 5923 of cap free
Amount of items: 2
Items: 
Size: 765167 Color: 4
Size: 228911 Color: 1

Bin 175: 6161 of cap free
Amount of items: 2
Items: 
Size: 650513 Color: 2
Size: 343327 Color: 3

Bin 176: 6174 of cap free
Amount of items: 2
Items: 
Size: 722059 Color: 3
Size: 271768 Color: 0

Bin 177: 6214 of cap free
Amount of items: 2
Items: 
Size: 503439 Color: 3
Size: 490348 Color: 2

Bin 178: 6356 of cap free
Amount of items: 2
Items: 
Size: 613613 Color: 1
Size: 380032 Color: 0

Bin 179: 6372 of cap free
Amount of items: 2
Items: 
Size: 517970 Color: 4
Size: 475659 Color: 2

Bin 180: 7359 of cap free
Amount of items: 2
Items: 
Size: 694312 Color: 0
Size: 298330 Color: 4

Bin 181: 8230 of cap free
Amount of items: 6
Items: 
Size: 178893 Color: 0
Size: 173430 Color: 2
Size: 172999 Color: 1
Size: 172443 Color: 1
Size: 168987 Color: 1
Size: 125019 Color: 0

Bin 182: 8346 of cap free
Amount of items: 2
Items: 
Size: 637995 Color: 2
Size: 353660 Color: 1

Bin 183: 8866 of cap free
Amount of items: 6
Items: 
Size: 167437 Color: 2
Size: 165986 Color: 1
Size: 165286 Color: 2
Size: 165072 Color: 3
Size: 163751 Color: 2
Size: 163603 Color: 4

Bin 184: 8921 of cap free
Amount of items: 2
Items: 
Size: 775482 Color: 2
Size: 215598 Color: 1

Bin 185: 9121 of cap free
Amount of items: 2
Items: 
Size: 637149 Color: 0
Size: 353731 Color: 2

Bin 186: 9571 of cap free
Amount of items: 2
Items: 
Size: 574817 Color: 3
Size: 415613 Color: 4

Bin 187: 9636 of cap free
Amount of items: 2
Items: 
Size: 533776 Color: 0
Size: 456589 Color: 3

Bin 188: 9806 of cap free
Amount of items: 2
Items: 
Size: 668068 Color: 1
Size: 322127 Color: 2

Bin 189: 10071 of cap free
Amount of items: 2
Items: 
Size: 611760 Color: 4
Size: 378170 Color: 2

Bin 190: 10658 of cap free
Amount of items: 2
Items: 
Size: 499995 Color: 2
Size: 489348 Color: 1

Bin 191: 10678 of cap free
Amount of items: 2
Items: 
Size: 589249 Color: 3
Size: 400074 Color: 0

Bin 192: 11359 of cap free
Amount of items: 2
Items: 
Size: 623782 Color: 2
Size: 364860 Color: 0

Bin 193: 11386 of cap free
Amount of items: 2
Items: 
Size: 717289 Color: 4
Size: 271326 Color: 3

Bin 194: 13672 of cap free
Amount of items: 2
Items: 
Size: 715502 Color: 4
Size: 270827 Color: 3

Bin 195: 13774 of cap free
Amount of items: 2
Items: 
Size: 637014 Color: 0
Size: 349213 Color: 2

Bin 196: 14703 of cap free
Amount of items: 2
Items: 
Size: 755762 Color: 0
Size: 229536 Color: 4

Bin 197: 18909 of cap free
Amount of items: 2
Items: 
Size: 715434 Color: 0
Size: 265658 Color: 1

Bin 198: 19781 of cap free
Amount of items: 2
Items: 
Size: 715055 Color: 2
Size: 265165 Color: 0

Bin 199: 20217 of cap free
Amount of items: 2
Items: 
Size: 636492 Color: 2
Size: 343292 Color: 1

Bin 200: 20655 of cap free
Amount of items: 2
Items: 
Size: 566132 Color: 2
Size: 413214 Color: 3

Bin 201: 20710 of cap free
Amount of items: 2
Items: 
Size: 539994 Color: 3
Size: 439297 Color: 0

Bin 202: 20766 of cap free
Amount of items: 2
Items: 
Size: 750403 Color: 4
Size: 228832 Color: 1

Bin 203: 23534 of cap free
Amount of items: 2
Items: 
Size: 528896 Color: 4
Size: 447571 Color: 3

Bin 204: 24372 of cap free
Amount of items: 2
Items: 
Size: 539571 Color: 3
Size: 436058 Color: 1

Bin 205: 26805 of cap free
Amount of items: 2
Items: 
Size: 747538 Color: 1
Size: 225658 Color: 4

Bin 206: 26824 of cap free
Amount of items: 2
Items: 
Size: 708751 Color: 1
Size: 264426 Color: 2

Bin 207: 28066 of cap free
Amount of items: 2
Items: 
Size: 565437 Color: 1
Size: 406498 Color: 3

Bin 208: 29794 of cap free
Amount of items: 2
Items: 
Size: 708423 Color: 3
Size: 261784 Color: 2

Bin 209: 31456 of cap free
Amount of items: 2
Items: 
Size: 708207 Color: 4
Size: 260338 Color: 2

Bin 210: 32088 of cap free
Amount of items: 2
Items: 
Size: 573333 Color: 3
Size: 394580 Color: 2

Bin 211: 33075 of cap free
Amount of items: 2
Items: 
Size: 707020 Color: 3
Size: 259906 Color: 2

Bin 212: 38741 of cap free
Amount of items: 2
Items: 
Size: 526117 Color: 1
Size: 435143 Color: 0

Bin 213: 39267 of cap free
Amount of items: 2
Items: 
Size: 706341 Color: 3
Size: 254393 Color: 2

Bin 214: 39399 of cap free
Amount of items: 3
Items: 
Size: 363950 Color: 2
Size: 341856 Color: 1
Size: 254796 Color: 3

Bin 215: 41727 of cap free
Amount of items: 2
Items: 
Size: 482108 Color: 0
Size: 476166 Color: 4

Bin 216: 43181 of cap free
Amount of items: 2
Items: 
Size: 565242 Color: 0
Size: 391578 Color: 3

Bin 217: 48639 of cap free
Amount of items: 3
Items: 
Size: 322083 Color: 1
Size: 314956 Color: 2
Size: 314323 Color: 0

Bin 218: 49540 of cap free
Amount of items: 2
Items: 
Size: 475621 Color: 0
Size: 474840 Color: 2

Bin 219: 55671 of cap free
Amount of items: 2
Items: 
Size: 747552 Color: 4
Size: 196778 Color: 0

Bin 220: 67557 of cap free
Amount of items: 7
Items: 
Size: 139068 Color: 2
Size: 138912 Color: 4
Size: 137722 Color: 2
Size: 133601 Color: 3
Size: 133437 Color: 0
Size: 126727 Color: 1
Size: 122977 Color: 2

Bin 221: 75018 of cap free
Amount of items: 6
Items: 
Size: 166685 Color: 2
Size: 155954 Color: 3
Size: 151850 Color: 1
Size: 151687 Color: 2
Size: 150165 Color: 0
Size: 148642 Color: 1

Bin 222: 88185 of cap free
Amount of items: 2
Items: 
Size: 705372 Color: 1
Size: 206444 Color: 4

Bin 223: 97213 of cap free
Amount of items: 8
Items: 
Size: 137617 Color: 2
Size: 122473 Color: 1
Size: 117493 Color: 3
Size: 112604 Color: 1
Size: 106808 Color: 1
Size: 102862 Color: 3
Size: 102425 Color: 1
Size: 100506 Color: 2

Bin 224: 99623 of cap free
Amount of items: 2
Items: 
Size: 703659 Color: 1
Size: 196719 Color: 3

Bin 225: 896616 of cap free
Amount of items: 1
Items: 
Size: 103385 Color: 1

Bin 226: 898759 of cap free
Amount of items: 1
Items: 
Size: 101242 Color: 1

Total size: 222554922
Total free space: 3445304

