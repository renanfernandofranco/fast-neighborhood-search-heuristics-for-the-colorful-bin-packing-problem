Capicity Bin: 1000001
Lower Bound: 46

Bins used: 47
Amount of Colors: 5

Bin 1: 18 of cap free
Amount of items: 3
Items: 
Size: 632707 Color: 1
Size: 187903 Color: 3
Size: 179373 Color: 2

Bin 2: 208 of cap free
Amount of items: 2
Items: 
Size: 729667 Color: 0
Size: 270126 Color: 3

Bin 3: 235 of cap free
Amount of items: 2
Items: 
Size: 604333 Color: 2
Size: 395433 Color: 4

Bin 4: 337 of cap free
Amount of items: 3
Items: 
Size: 769156 Color: 1
Size: 116867 Color: 2
Size: 113641 Color: 2

Bin 5: 410 of cap free
Amount of items: 3
Items: 
Size: 554672 Color: 2
Size: 241894 Color: 3
Size: 203025 Color: 0

Bin 6: 442 of cap free
Amount of items: 2
Items: 
Size: 692395 Color: 1
Size: 307164 Color: 4

Bin 7: 526 of cap free
Amount of items: 2
Items: 
Size: 571472 Color: 3
Size: 428003 Color: 1

Bin 8: 581 of cap free
Amount of items: 3
Items: 
Size: 375097 Color: 4
Size: 314084 Color: 3
Size: 310239 Color: 0

Bin 9: 729 of cap free
Amount of items: 2
Items: 
Size: 507025 Color: 2
Size: 492247 Color: 0

Bin 10: 804 of cap free
Amount of items: 2
Items: 
Size: 537872 Color: 0
Size: 461325 Color: 3

Bin 11: 1017 of cap free
Amount of items: 2
Items: 
Size: 606278 Color: 4
Size: 392706 Color: 2

Bin 12: 1275 of cap free
Amount of items: 2
Items: 
Size: 571301 Color: 0
Size: 427425 Color: 2

Bin 13: 1408 of cap free
Amount of items: 2
Items: 
Size: 531845 Color: 3
Size: 466748 Color: 2

Bin 14: 1698 of cap free
Amount of items: 2
Items: 
Size: 691973 Color: 4
Size: 306330 Color: 2

Bin 15: 2341 of cap free
Amount of items: 2
Items: 
Size: 786464 Color: 2
Size: 211196 Color: 4

Bin 16: 2799 of cap free
Amount of items: 2
Items: 
Size: 598544 Color: 1
Size: 398658 Color: 3

Bin 17: 3558 of cap free
Amount of items: 3
Items: 
Size: 734031 Color: 1
Size: 159775 Color: 3
Size: 102637 Color: 0

Bin 18: 4097 of cap free
Amount of items: 2
Items: 
Size: 650219 Color: 4
Size: 345685 Color: 1

Bin 19: 4298 of cap free
Amount of items: 2
Items: 
Size: 654486 Color: 2
Size: 341217 Color: 1

Bin 20: 4666 of cap free
Amount of items: 3
Items: 
Size: 702385 Color: 1
Size: 168624 Color: 4
Size: 124326 Color: 0

Bin 21: 5657 of cap free
Amount of items: 2
Items: 
Size: 676395 Color: 1
Size: 317949 Color: 4

Bin 22: 5876 of cap free
Amount of items: 2
Items: 
Size: 595915 Color: 4
Size: 398210 Color: 3

Bin 23: 6495 of cap free
Amount of items: 3
Items: 
Size: 577060 Color: 3
Size: 221388 Color: 4
Size: 195058 Color: 3

Bin 24: 6765 of cap free
Amount of items: 2
Items: 
Size: 660337 Color: 1
Size: 332899 Color: 0

Bin 25: 8492 of cap free
Amount of items: 3
Items: 
Size: 401676 Color: 4
Size: 350259 Color: 2
Size: 239574 Color: 2

Bin 26: 8705 of cap free
Amount of items: 2
Items: 
Size: 547454 Color: 2
Size: 443842 Color: 4

Bin 27: 8993 of cap free
Amount of items: 3
Items: 
Size: 784327 Color: 3
Size: 103838 Color: 2
Size: 102843 Color: 3

Bin 28: 9685 of cap free
Amount of items: 2
Items: 
Size: 794527 Color: 0
Size: 195789 Color: 1

Bin 29: 11948 of cap free
Amount of items: 2
Items: 
Size: 546367 Color: 1
Size: 441686 Color: 4

Bin 30: 12876 of cap free
Amount of items: 2
Items: 
Size: 764380 Color: 0
Size: 222745 Color: 2

Bin 31: 13548 of cap free
Amount of items: 2
Items: 
Size: 740526 Color: 0
Size: 245927 Color: 1

Bin 32: 14437 of cap free
Amount of items: 2
Items: 
Size: 694877 Color: 0
Size: 290687 Color: 2

Bin 33: 15267 of cap free
Amount of items: 2
Items: 
Size: 620791 Color: 4
Size: 363943 Color: 3

Bin 34: 16312 of cap free
Amount of items: 2
Items: 
Size: 545956 Color: 0
Size: 437733 Color: 3

Bin 35: 16664 of cap free
Amount of items: 2
Items: 
Size: 502270 Color: 2
Size: 481067 Color: 0

Bin 36: 17172 of cap free
Amount of items: 2
Items: 
Size: 711777 Color: 0
Size: 271052 Color: 4

Bin 37: 17919 of cap free
Amount of items: 2
Items: 
Size: 525585 Color: 1
Size: 456497 Color: 0

Bin 38: 21142 of cap free
Amount of items: 2
Items: 
Size: 715277 Color: 4
Size: 263582 Color: 1

Bin 39: 30202 of cap free
Amount of items: 2
Items: 
Size: 709329 Color: 4
Size: 260470 Color: 0

Bin 40: 31631 of cap free
Amount of items: 2
Items: 
Size: 563283 Color: 3
Size: 405087 Color: 2

Bin 41: 57904 of cap free
Amount of items: 2
Items: 
Size: 519903 Color: 4
Size: 422194 Color: 3

Bin 42: 59743 of cap free
Amount of items: 2
Items: 
Size: 694935 Color: 2
Size: 245323 Color: 0

Bin 43: 79658 of cap free
Amount of items: 2
Items: 
Size: 504029 Color: 0
Size: 416314 Color: 3

Bin 44: 220237 of cap free
Amount of items: 2
Items: 
Size: 390924 Color: 4
Size: 388840 Color: 0

Bin 45: 258279 of cap free
Amount of items: 2
Items: 
Size: 380208 Color: 4
Size: 361514 Color: 1

Bin 46: 280763 of cap free
Amount of items: 2
Items: 
Size: 369329 Color: 4
Size: 349909 Color: 0

Bin 47: 674427 of cap free
Amount of items: 1
Items: 
Size: 325574 Color: 3

Total size: 45057803
Total free space: 1942244

