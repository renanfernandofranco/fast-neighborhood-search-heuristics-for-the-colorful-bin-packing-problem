Capicity Bin: 1001
Lower Bound: 45

Bins used: 47
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 496 Color: 3

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 466 Color: 3

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 2
Size: 343 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 4
Size: 137 Color: 2
Size: 110 Color: 0

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 3
Size: 243 Color: 1

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 394 Color: 0

Bin 7: 1 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 4
Size: 204 Color: 2
Size: 183 Color: 3

Bin 8: 1 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 1
Size: 191 Color: 4
Size: 191 Color: 3

Bin 9: 2 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 4
Size: 489 Color: 1

Bin 10: 2 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 1
Size: 262 Color: 2
Size: 167 Color: 1

Bin 11: 2 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 333 Color: 0

Bin 12: 2 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 3
Size: 152 Color: 4
Size: 151 Color: 2

Bin 13: 2 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 243 Color: 3

Bin 14: 3 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 403 Color: 3

Bin 15: 3 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 4
Size: 317 Color: 3

Bin 16: 3 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 2
Size: 233 Color: 1

Bin 17: 3 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 3
Size: 114 Color: 0
Size: 104 Color: 1

Bin 18: 3 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 205 Color: 3

Bin 19: 4 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 2
Size: 408 Color: 1

Bin 20: 4 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 391 Color: 2

Bin 21: 4 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 293 Color: 2

Bin 22: 4 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 0
Size: 122 Color: 3
Size: 121 Color: 4

Bin 23: 5 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 3
Size: 143 Color: 4
Size: 143 Color: 1

Bin 24: 7 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 1
Size: 266 Color: 3
Size: 159 Color: 2

Bin 25: 7 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 196 Color: 2

Bin 26: 8 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 4
Size: 217 Color: 1

Bin 27: 10 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 486 Color: 1

Bin 28: 11 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 3
Size: 237 Color: 2

Bin 29: 13 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 484 Color: 0

Bin 30: 13 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 484 Color: 3

Bin 31: 14 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 216 Color: 2

Bin 32: 18 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 422 Color: 1

Bin 33: 20 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 0
Size: 482 Color: 3

Bin 34: 21 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 231 Color: 0

Bin 35: 24 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 377 Color: 0

Bin 36: 35 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 266 Color: 0

Bin 37: 36 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 366 Color: 4

Bin 38: 37 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 217 Color: 1

Bin 39: 46 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 211 Color: 1

Bin 40: 49 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 353 Color: 0

Bin 41: 71 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 344 Color: 3

Bin 42: 113 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 3
Size: 351 Color: 1

Bin 43: 188 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 3
Size: 335 Color: 1

Bin 44: 220 of cap free
Amount of items: 2
Items: 
Size: 448 Color: 3
Size: 333 Color: 1

Bin 45: 243 of cap free
Amount of items: 2
Items: 
Size: 448 Color: 3
Size: 310 Color: 4

Bin 46: 433 of cap free
Amount of items: 2
Items: 
Size: 309 Color: 4
Size: 259 Color: 2

Bin 47: 693 of cap free
Amount of items: 1
Items: 
Size: 308 Color: 4

Total size: 44668
Total free space: 2379

