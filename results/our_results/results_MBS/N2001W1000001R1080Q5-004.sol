Capicity Bin: 1000001
Lower Bound: 886

Bins used: 894
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 526199 Color: 4
Size: 473802 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 542983 Color: 1
Size: 457018 Color: 2

Bin 3: 1 of cap free
Amount of items: 2
Items: 
Size: 577378 Color: 3
Size: 422622 Color: 1

Bin 4: 1 of cap free
Amount of items: 2
Items: 
Size: 595360 Color: 4
Size: 404640 Color: 3

Bin 5: 1 of cap free
Amount of items: 3
Items: 
Size: 647105 Color: 2
Size: 176631 Color: 3
Size: 176264 Color: 2

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 669001 Color: 0
Size: 330999 Color: 3

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 727287 Color: 2
Size: 272713 Color: 0

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 744497 Color: 0
Size: 255503 Color: 2

Bin 9: 1 of cap free
Amount of items: 3
Items: 
Size: 798402 Color: 4
Size: 100831 Color: 1
Size: 100767 Color: 3

Bin 10: 2 of cap free
Amount of items: 3
Items: 
Size: 416822 Color: 2
Size: 318065 Color: 1
Size: 265112 Color: 1

Bin 11: 2 of cap free
Amount of items: 2
Items: 
Size: 510513 Color: 1
Size: 489486 Color: 2

Bin 12: 2 of cap free
Amount of items: 2
Items: 
Size: 546003 Color: 1
Size: 453996 Color: 2

Bin 13: 2 of cap free
Amount of items: 3
Items: 
Size: 638680 Color: 1
Size: 180786 Color: 2
Size: 180533 Color: 1

Bin 14: 3 of cap free
Amount of items: 3
Items: 
Size: 435736 Color: 4
Size: 310313 Color: 2
Size: 253949 Color: 1

Bin 15: 3 of cap free
Amount of items: 3
Items: 
Size: 631745 Color: 0
Size: 184291 Color: 2
Size: 183962 Color: 1

Bin 16: 3 of cap free
Amount of items: 2
Items: 
Size: 782585 Color: 0
Size: 217413 Color: 3

Bin 17: 4 of cap free
Amount of items: 2
Items: 
Size: 658912 Color: 3
Size: 341085 Color: 4

Bin 18: 4 of cap free
Amount of items: 2
Items: 
Size: 709772 Color: 0
Size: 290225 Color: 4

Bin 19: 4 of cap free
Amount of items: 3
Items: 
Size: 718970 Color: 3
Size: 141668 Color: 0
Size: 139359 Color: 4

Bin 20: 5 of cap free
Amount of items: 3
Items: 
Size: 629396 Color: 4
Size: 186248 Color: 2
Size: 184352 Color: 2

Bin 21: 5 of cap free
Amount of items: 3
Items: 
Size: 645387 Color: 3
Size: 178995 Color: 0
Size: 175614 Color: 1

Bin 22: 6 of cap free
Amount of items: 3
Items: 
Size: 409476 Color: 0
Size: 337565 Color: 4
Size: 252954 Color: 4

Bin 23: 7 of cap free
Amount of items: 2
Items: 
Size: 510021 Color: 0
Size: 489973 Color: 4

Bin 24: 7 of cap free
Amount of items: 3
Items: 
Size: 750895 Color: 2
Size: 124731 Color: 3
Size: 124368 Color: 2

Bin 25: 8 of cap free
Amount of items: 2
Items: 
Size: 789679 Color: 3
Size: 210314 Color: 0

Bin 26: 8 of cap free
Amount of items: 3
Items: 
Size: 799089 Color: 0
Size: 100473 Color: 2
Size: 100431 Color: 2

Bin 27: 9 of cap free
Amount of items: 3
Items: 
Size: 385323 Color: 2
Size: 312068 Color: 4
Size: 302601 Color: 2

Bin 28: 9 of cap free
Amount of items: 2
Items: 
Size: 756817 Color: 2
Size: 243175 Color: 3

Bin 29: 10 of cap free
Amount of items: 3
Items: 
Size: 635021 Color: 0
Size: 183938 Color: 3
Size: 181032 Color: 3

Bin 30: 10 of cap free
Amount of items: 2
Items: 
Size: 640882 Color: 2
Size: 359109 Color: 4

Bin 31: 10 of cap free
Amount of items: 2
Items: 
Size: 654318 Color: 1
Size: 345673 Color: 2

Bin 32: 11 of cap free
Amount of items: 2
Items: 
Size: 548752 Color: 0
Size: 451238 Color: 2

Bin 33: 11 of cap free
Amount of items: 3
Items: 
Size: 668995 Color: 0
Size: 165954 Color: 4
Size: 165041 Color: 0

Bin 34: 11 of cap free
Amount of items: 3
Items: 
Size: 679773 Color: 1
Size: 160547 Color: 3
Size: 159670 Color: 1

Bin 35: 11 of cap free
Amount of items: 3
Items: 
Size: 760806 Color: 4
Size: 119902 Color: 0
Size: 119282 Color: 4

Bin 36: 12 of cap free
Amount of items: 2
Items: 
Size: 592374 Color: 3
Size: 407615 Color: 0

Bin 37: 12 of cap free
Amount of items: 2
Items: 
Size: 604478 Color: 1
Size: 395511 Color: 3

Bin 38: 12 of cap free
Amount of items: 3
Items: 
Size: 685089 Color: 3
Size: 158573 Color: 0
Size: 156327 Color: 3

Bin 39: 12 of cap free
Amount of items: 2
Items: 
Size: 687748 Color: 3
Size: 312241 Color: 2

Bin 40: 12 of cap free
Amount of items: 2
Items: 
Size: 772242 Color: 4
Size: 227747 Color: 1

Bin 41: 12 of cap free
Amount of items: 3
Items: 
Size: 785105 Color: 0
Size: 107643 Color: 1
Size: 107241 Color: 0

Bin 42: 13 of cap free
Amount of items: 3
Items: 
Size: 675149 Color: 4
Size: 162878 Color: 2
Size: 161961 Color: 4

Bin 43: 13 of cap free
Amount of items: 3
Items: 
Size: 710055 Color: 1
Size: 144984 Color: 4
Size: 144949 Color: 1

Bin 44: 14 of cap free
Amount of items: 3
Items: 
Size: 718679 Color: 1
Size: 142238 Color: 4
Size: 139070 Color: 4

Bin 45: 14 of cap free
Amount of items: 3
Items: 
Size: 775521 Color: 0
Size: 112899 Color: 2
Size: 111567 Color: 3

Bin 46: 14 of cap free
Amount of items: 2
Items: 
Size: 795918 Color: 4
Size: 204069 Color: 0

Bin 47: 15 of cap free
Amount of items: 3
Items: 
Size: 536427 Color: 0
Size: 232934 Color: 1
Size: 230625 Color: 4

Bin 48: 15 of cap free
Amount of items: 2
Items: 
Size: 627665 Color: 0
Size: 372321 Color: 4

Bin 49: 15 of cap free
Amount of items: 2
Items: 
Size: 675477 Color: 4
Size: 324509 Color: 2

Bin 50: 15 of cap free
Amount of items: 2
Items: 
Size: 722908 Color: 4
Size: 277078 Color: 3

Bin 51: 16 of cap free
Amount of items: 3
Items: 
Size: 420512 Color: 2
Size: 311537 Color: 0
Size: 267936 Color: 2

Bin 52: 16 of cap free
Amount of items: 2
Items: 
Size: 534065 Color: 1
Size: 465920 Color: 0

Bin 53: 16 of cap free
Amount of items: 2
Items: 
Size: 563603 Color: 0
Size: 436382 Color: 2

Bin 54: 16 of cap free
Amount of items: 2
Items: 
Size: 600736 Color: 3
Size: 399249 Color: 1

Bin 55: 16 of cap free
Amount of items: 2
Items: 
Size: 636008 Color: 3
Size: 363977 Color: 0

Bin 56: 16 of cap free
Amount of items: 3
Items: 
Size: 660100 Color: 2
Size: 171256 Color: 0
Size: 168629 Color: 2

Bin 57: 17 of cap free
Amount of items: 2
Items: 
Size: 558141 Color: 4
Size: 441843 Color: 2

Bin 58: 17 of cap free
Amount of items: 3
Items: 
Size: 766331 Color: 1
Size: 117119 Color: 0
Size: 116534 Color: 3

Bin 59: 18 of cap free
Amount of items: 3
Items: 
Size: 634907 Color: 1
Size: 183998 Color: 4
Size: 181078 Color: 4

Bin 60: 18 of cap free
Amount of items: 2
Items: 
Size: 784366 Color: 1
Size: 215617 Color: 3

Bin 61: 19 of cap free
Amount of items: 3
Items: 
Size: 610138 Color: 3
Size: 197272 Color: 1
Size: 192572 Color: 2

Bin 62: 19 of cap free
Amount of items: 2
Items: 
Size: 738175 Color: 4
Size: 261807 Color: 3

Bin 63: 20 of cap free
Amount of items: 3
Items: 
Size: 408233 Color: 1
Size: 326422 Color: 3
Size: 265326 Color: 3

Bin 64: 21 of cap free
Amount of items: 3
Items: 
Size: 415051 Color: 1
Size: 318084 Color: 4
Size: 266845 Color: 2

Bin 65: 21 of cap free
Amount of items: 2
Items: 
Size: 505787 Color: 4
Size: 494193 Color: 3

Bin 66: 21 of cap free
Amount of items: 3
Items: 
Size: 656716 Color: 4
Size: 172076 Color: 2
Size: 171188 Color: 4

Bin 67: 21 of cap free
Amount of items: 3
Items: 
Size: 753105 Color: 1
Size: 123864 Color: 2
Size: 123011 Color: 1

Bin 68: 22 of cap free
Amount of items: 3
Items: 
Size: 382724 Color: 3
Size: 320881 Color: 0
Size: 296374 Color: 1

Bin 69: 22 of cap free
Amount of items: 2
Items: 
Size: 720651 Color: 1
Size: 279328 Color: 4

Bin 70: 22 of cap free
Amount of items: 2
Items: 
Size: 757394 Color: 0
Size: 242585 Color: 2

Bin 71: 22 of cap free
Amount of items: 2
Items: 
Size: 793577 Color: 2
Size: 206402 Color: 0

Bin 72: 23 of cap free
Amount of items: 3
Items: 
Size: 635440 Color: 2
Size: 184132 Color: 4
Size: 180406 Color: 3

Bin 73: 23 of cap free
Amount of items: 2
Items: 
Size: 786913 Color: 3
Size: 213065 Color: 4

Bin 74: 24 of cap free
Amount of items: 2
Items: 
Size: 506100 Color: 3
Size: 493877 Color: 1

Bin 75: 24 of cap free
Amount of items: 3
Items: 
Size: 660413 Color: 3
Size: 170978 Color: 2
Size: 168586 Color: 0

Bin 76: 25 of cap free
Amount of items: 2
Items: 
Size: 774238 Color: 1
Size: 225738 Color: 0

Bin 77: 26 of cap free
Amount of items: 2
Items: 
Size: 595444 Color: 3
Size: 404531 Color: 2

Bin 78: 27 of cap free
Amount of items: 3
Items: 
Size: 407903 Color: 1
Size: 338280 Color: 3
Size: 253791 Color: 0

Bin 79: 27 of cap free
Amount of items: 2
Items: 
Size: 636539 Color: 1
Size: 363435 Color: 2

Bin 80: 27 of cap free
Amount of items: 3
Items: 
Size: 723556 Color: 2
Size: 138411 Color: 1
Size: 138007 Color: 2

Bin 81: 27 of cap free
Amount of items: 2
Items: 
Size: 770140 Color: 4
Size: 229834 Color: 2

Bin 82: 28 of cap free
Amount of items: 3
Items: 
Size: 544106 Color: 0
Size: 228910 Color: 1
Size: 226957 Color: 3

Bin 83: 28 of cap free
Amount of items: 2
Items: 
Size: 742383 Color: 2
Size: 257590 Color: 1

Bin 84: 29 of cap free
Amount of items: 2
Items: 
Size: 701937 Color: 4
Size: 298035 Color: 0

Bin 85: 29 of cap free
Amount of items: 2
Items: 
Size: 725730 Color: 4
Size: 274242 Color: 0

Bin 86: 30 of cap free
Amount of items: 3
Items: 
Size: 410714 Color: 1
Size: 311191 Color: 0
Size: 278066 Color: 1

Bin 87: 30 of cap free
Amount of items: 2
Items: 
Size: 522266 Color: 3
Size: 477705 Color: 2

Bin 88: 30 of cap free
Amount of items: 2
Items: 
Size: 555319 Color: 0
Size: 444652 Color: 2

Bin 89: 30 of cap free
Amount of items: 2
Items: 
Size: 610034 Color: 1
Size: 389937 Color: 4

Bin 90: 30 of cap free
Amount of items: 3
Items: 
Size: 661255 Color: 2
Size: 170364 Color: 4
Size: 168352 Color: 4

Bin 91: 32 of cap free
Amount of items: 3
Items: 
Size: 413855 Color: 4
Size: 306592 Color: 0
Size: 279522 Color: 4

Bin 92: 32 of cap free
Amount of items: 3
Items: 
Size: 733984 Color: 0
Size: 133003 Color: 3
Size: 132982 Color: 3

Bin 93: 33 of cap free
Amount of items: 2
Items: 
Size: 534503 Color: 2
Size: 465465 Color: 1

Bin 94: 33 of cap free
Amount of items: 2
Items: 
Size: 598924 Color: 4
Size: 401044 Color: 0

Bin 95: 33 of cap free
Amount of items: 2
Items: 
Size: 603917 Color: 3
Size: 396051 Color: 2

Bin 96: 33 of cap free
Amount of items: 2
Items: 
Size: 773086 Color: 4
Size: 226882 Color: 0

Bin 97: 33 of cap free
Amount of items: 3
Items: 
Size: 791113 Color: 4
Size: 104930 Color: 2
Size: 103925 Color: 3

Bin 98: 33 of cap free
Amount of items: 3
Items: 
Size: 791490 Color: 2
Size: 104887 Color: 1
Size: 103591 Color: 3

Bin 99: 34 of cap free
Amount of items: 2
Items: 
Size: 680416 Color: 2
Size: 319551 Color: 0

Bin 100: 35 of cap free
Amount of items: 3
Items: 
Size: 352177 Color: 1
Size: 324900 Color: 3
Size: 322889 Color: 1

Bin 101: 35 of cap free
Amount of items: 2
Items: 
Size: 684370 Color: 1
Size: 315596 Color: 0

Bin 102: 35 of cap free
Amount of items: 2
Items: 
Size: 768835 Color: 1
Size: 231131 Color: 4

Bin 103: 36 of cap free
Amount of items: 3
Items: 
Size: 443755 Color: 2
Size: 280707 Color: 3
Size: 275503 Color: 4

Bin 104: 36 of cap free
Amount of items: 2
Items: 
Size: 500028 Color: 2
Size: 499937 Color: 0

Bin 105: 36 of cap free
Amount of items: 3
Items: 
Size: 637737 Color: 4
Size: 181266 Color: 1
Size: 180962 Color: 2

Bin 106: 36 of cap free
Amount of items: 3
Items: 
Size: 650845 Color: 0
Size: 175577 Color: 1
Size: 173543 Color: 2

Bin 107: 36 of cap free
Amount of items: 3
Items: 
Size: 687338 Color: 0
Size: 158476 Color: 2
Size: 154151 Color: 0

Bin 108: 36 of cap free
Amount of items: 3
Items: 
Size: 758662 Color: 3
Size: 120689 Color: 0
Size: 120614 Color: 2

Bin 109: 36 of cap free
Amount of items: 2
Items: 
Size: 764153 Color: 4
Size: 235812 Color: 1

Bin 110: 38 of cap free
Amount of items: 3
Items: 
Size: 616295 Color: 3
Size: 192518 Color: 0
Size: 191150 Color: 0

Bin 111: 39 of cap free
Amount of items: 2
Items: 
Size: 604822 Color: 4
Size: 395140 Color: 1

Bin 112: 39 of cap free
Amount of items: 3
Items: 
Size: 679295 Color: 0
Size: 160979 Color: 2
Size: 159688 Color: 3

Bin 113: 39 of cap free
Amount of items: 2
Items: 
Size: 680184 Color: 0
Size: 319778 Color: 3

Bin 114: 40 of cap free
Amount of items: 2
Items: 
Size: 612464 Color: 1
Size: 387497 Color: 2

Bin 115: 40 of cap free
Amount of items: 3
Items: 
Size: 642080 Color: 0
Size: 179268 Color: 1
Size: 178613 Color: 1

Bin 116: 41 of cap free
Amount of items: 2
Items: 
Size: 571726 Color: 0
Size: 428234 Color: 1

Bin 117: 41 of cap free
Amount of items: 2
Items: 
Size: 631204 Color: 4
Size: 368756 Color: 2

Bin 118: 41 of cap free
Amount of items: 2
Items: 
Size: 769497 Color: 1
Size: 230463 Color: 4

Bin 119: 42 of cap free
Amount of items: 3
Items: 
Size: 567133 Color: 4
Size: 216466 Color: 0
Size: 216360 Color: 0

Bin 120: 42 of cap free
Amount of items: 3
Items: 
Size: 646831 Color: 4
Size: 177560 Color: 0
Size: 175568 Color: 2

Bin 121: 43 of cap free
Amount of items: 3
Items: 
Size: 640626 Color: 4
Size: 180189 Color: 1
Size: 179143 Color: 2

Bin 122: 43 of cap free
Amount of items: 3
Items: 
Size: 659242 Color: 3
Size: 172088 Color: 2
Size: 168628 Color: 1

Bin 123: 45 of cap free
Amount of items: 3
Items: 
Size: 628336 Color: 2
Size: 187041 Color: 0
Size: 184579 Color: 3

Bin 124: 45 of cap free
Amount of items: 3
Items: 
Size: 679882 Color: 1
Size: 161075 Color: 0
Size: 158999 Color: 3

Bin 125: 45 of cap free
Amount of items: 2
Items: 
Size: 686767 Color: 2
Size: 313189 Color: 0

Bin 126: 45 of cap free
Amount of items: 2
Items: 
Size: 734789 Color: 3
Size: 265167 Color: 4

Bin 127: 46 of cap free
Amount of items: 3
Items: 
Size: 409505 Color: 2
Size: 321034 Color: 3
Size: 269416 Color: 4

Bin 128: 46 of cap free
Amount of items: 2
Items: 
Size: 540428 Color: 3
Size: 459527 Color: 2

Bin 129: 46 of cap free
Amount of items: 2
Items: 
Size: 651302 Color: 0
Size: 348653 Color: 1

Bin 130: 47 of cap free
Amount of items: 3
Items: 
Size: 650099 Color: 3
Size: 175507 Color: 1
Size: 174348 Color: 3

Bin 131: 47 of cap free
Amount of items: 3
Items: 
Size: 725973 Color: 0
Size: 137239 Color: 4
Size: 136742 Color: 3

Bin 132: 49 of cap free
Amount of items: 2
Items: 
Size: 550528 Color: 2
Size: 449424 Color: 1

Bin 133: 50 of cap free
Amount of items: 3
Items: 
Size: 794284 Color: 2
Size: 102839 Color: 1
Size: 102828 Color: 0

Bin 134: 52 of cap free
Amount of items: 2
Items: 
Size: 526859 Color: 4
Size: 473090 Color: 2

Bin 135: 52 of cap free
Amount of items: 3
Items: 
Size: 672718 Color: 0
Size: 163776 Color: 1
Size: 163455 Color: 4

Bin 136: 53 of cap free
Amount of items: 3
Items: 
Size: 427222 Color: 2
Size: 295962 Color: 0
Size: 276764 Color: 3

Bin 137: 53 of cap free
Amount of items: 3
Items: 
Size: 578743 Color: 1
Size: 213746 Color: 4
Size: 207459 Color: 0

Bin 138: 53 of cap free
Amount of items: 3
Items: 
Size: 792079 Color: 2
Size: 104557 Color: 1
Size: 103312 Color: 4

Bin 139: 54 of cap free
Amount of items: 3
Items: 
Size: 755788 Color: 4
Size: 122215 Color: 2
Size: 121944 Color: 1

Bin 140: 54 of cap free
Amount of items: 3
Items: 
Size: 784644 Color: 1
Size: 107886 Color: 0
Size: 107417 Color: 2

Bin 141: 55 of cap free
Amount of items: 3
Items: 
Size: 613117 Color: 3
Size: 195341 Color: 1
Size: 191488 Color: 2

Bin 142: 55 of cap free
Amount of items: 3
Items: 
Size: 638692 Color: 2
Size: 181307 Color: 0
Size: 179947 Color: 3

Bin 143: 55 of cap free
Amount of items: 2
Items: 
Size: 736836 Color: 0
Size: 263110 Color: 4

Bin 144: 56 of cap free
Amount of items: 3
Items: 
Size: 637414 Color: 4
Size: 181696 Color: 2
Size: 180835 Color: 1

Bin 145: 57 of cap free
Amount of items: 2
Items: 
Size: 590813 Color: 4
Size: 409131 Color: 3

Bin 146: 57 of cap free
Amount of items: 2
Items: 
Size: 778026 Color: 0
Size: 221918 Color: 1

Bin 147: 58 of cap free
Amount of items: 2
Items: 
Size: 518833 Color: 2
Size: 481110 Color: 0

Bin 148: 58 of cap free
Amount of items: 2
Items: 
Size: 542108 Color: 3
Size: 457835 Color: 1

Bin 149: 58 of cap free
Amount of items: 3
Items: 
Size: 690377 Color: 2
Size: 155123 Color: 4
Size: 154443 Color: 0

Bin 150: 61 of cap free
Amount of items: 3
Items: 
Size: 623208 Color: 3
Size: 188510 Color: 4
Size: 188222 Color: 1

Bin 151: 61 of cap free
Amount of items: 2
Items: 
Size: 668352 Color: 0
Size: 331588 Color: 1

Bin 152: 62 of cap free
Amount of items: 2
Items: 
Size: 799188 Color: 4
Size: 200751 Color: 1

Bin 153: 63 of cap free
Amount of items: 3
Items: 
Size: 445233 Color: 1
Size: 278547 Color: 2
Size: 276158 Color: 1

Bin 154: 63 of cap free
Amount of items: 3
Items: 
Size: 710583 Color: 4
Size: 144762 Color: 0
Size: 144593 Color: 3

Bin 155: 63 of cap free
Amount of items: 2
Items: 
Size: 787707 Color: 3
Size: 212231 Color: 0

Bin 156: 64 of cap free
Amount of items: 3
Items: 
Size: 561629 Color: 3
Size: 219156 Color: 4
Size: 219152 Color: 0

Bin 157: 65 of cap free
Amount of items: 2
Items: 
Size: 661382 Color: 0
Size: 338554 Color: 2

Bin 158: 65 of cap free
Amount of items: 2
Items: 
Size: 760050 Color: 4
Size: 239886 Color: 2

Bin 159: 66 of cap free
Amount of items: 2
Items: 
Size: 701638 Color: 1
Size: 298297 Color: 0

Bin 160: 66 of cap free
Amount of items: 2
Items: 
Size: 761025 Color: 1
Size: 238910 Color: 4

Bin 161: 67 of cap free
Amount of items: 3
Items: 
Size: 698065 Color: 0
Size: 151052 Color: 1
Size: 150817 Color: 4

Bin 162: 67 of cap free
Amount of items: 3
Items: 
Size: 794931 Color: 4
Size: 103622 Color: 2
Size: 101381 Color: 1

Bin 163: 68 of cap free
Amount of items: 3
Items: 
Size: 553975 Color: 1
Size: 226696 Color: 0
Size: 219262 Color: 2

Bin 164: 68 of cap free
Amount of items: 2
Items: 
Size: 644941 Color: 4
Size: 354992 Color: 3

Bin 165: 68 of cap free
Amount of items: 3
Items: 
Size: 669245 Color: 0
Size: 166372 Color: 1
Size: 164316 Color: 4

Bin 166: 68 of cap free
Amount of items: 2
Items: 
Size: 724944 Color: 3
Size: 274989 Color: 4

Bin 167: 69 of cap free
Amount of items: 2
Items: 
Size: 510268 Color: 3
Size: 489664 Color: 4

Bin 168: 69 of cap free
Amount of items: 2
Items: 
Size: 594562 Color: 2
Size: 405370 Color: 4

Bin 169: 69 of cap free
Amount of items: 2
Items: 
Size: 776039 Color: 2
Size: 223893 Color: 4

Bin 170: 69 of cap free
Amount of items: 2
Items: 
Size: 798462 Color: 1
Size: 201470 Color: 4

Bin 171: 70 of cap free
Amount of items: 3
Items: 
Size: 496081 Color: 3
Size: 252627 Color: 0
Size: 251223 Color: 0

Bin 172: 70 of cap free
Amount of items: 2
Items: 
Size: 719938 Color: 2
Size: 279993 Color: 0

Bin 173: 70 of cap free
Amount of items: 2
Items: 
Size: 767545 Color: 2
Size: 232386 Color: 3

Bin 174: 71 of cap free
Amount of items: 2
Items: 
Size: 644653 Color: 3
Size: 355277 Color: 0

Bin 175: 71 of cap free
Amount of items: 3
Items: 
Size: 748665 Color: 0
Size: 126378 Color: 2
Size: 124887 Color: 2

Bin 176: 72 of cap free
Amount of items: 3
Items: 
Size: 410540 Color: 0
Size: 326770 Color: 2
Size: 262619 Color: 4

Bin 177: 72 of cap free
Amount of items: 3
Items: 
Size: 414896 Color: 2
Size: 306065 Color: 0
Size: 278968 Color: 1

Bin 178: 72 of cap free
Amount of items: 2
Items: 
Size: 753524 Color: 3
Size: 246405 Color: 4

Bin 179: 73 of cap free
Amount of items: 2
Items: 
Size: 583988 Color: 2
Size: 415940 Color: 3

Bin 180: 73 of cap free
Amount of items: 2
Items: 
Size: 588267 Color: 0
Size: 411661 Color: 1

Bin 181: 73 of cap free
Amount of items: 3
Items: 
Size: 651894 Color: 0
Size: 175322 Color: 4
Size: 172712 Color: 1

Bin 182: 73 of cap free
Amount of items: 2
Items: 
Size: 692146 Color: 4
Size: 307782 Color: 2

Bin 183: 73 of cap free
Amount of items: 2
Items: 
Size: 764278 Color: 1
Size: 235650 Color: 4

Bin 184: 74 of cap free
Amount of items: 2
Items: 
Size: 516495 Color: 2
Size: 483432 Color: 0

Bin 185: 74 of cap free
Amount of items: 3
Items: 
Size: 711165 Color: 0
Size: 145090 Color: 3
Size: 143672 Color: 1

Bin 186: 75 of cap free
Amount of items: 3
Items: 
Size: 426602 Color: 3
Size: 304772 Color: 2
Size: 268552 Color: 2

Bin 187: 76 of cap free
Amount of items: 2
Items: 
Size: 571717 Color: 1
Size: 428208 Color: 2

Bin 188: 76 of cap free
Amount of items: 2
Items: 
Size: 682949 Color: 2
Size: 316976 Color: 1

Bin 189: 76 of cap free
Amount of items: 2
Items: 
Size: 769154 Color: 2
Size: 230771 Color: 3

Bin 190: 77 of cap free
Amount of items: 2
Items: 
Size: 605022 Color: 0
Size: 394902 Color: 2

Bin 191: 77 of cap free
Amount of items: 2
Items: 
Size: 666378 Color: 3
Size: 333546 Color: 1

Bin 192: 77 of cap free
Amount of items: 3
Items: 
Size: 704193 Color: 4
Size: 148368 Color: 0
Size: 147363 Color: 2

Bin 193: 77 of cap free
Amount of items: 2
Items: 
Size: 736949 Color: 2
Size: 262975 Color: 0

Bin 194: 77 of cap free
Amount of items: 2
Items: 
Size: 749090 Color: 0
Size: 250834 Color: 4

Bin 195: 77 of cap free
Amount of items: 3
Items: 
Size: 772230 Color: 0
Size: 114152 Color: 1
Size: 113542 Color: 1

Bin 196: 78 of cap free
Amount of items: 2
Items: 
Size: 557001 Color: 1
Size: 442922 Color: 4

Bin 197: 78 of cap free
Amount of items: 3
Items: 
Size: 686177 Color: 3
Size: 157769 Color: 0
Size: 155977 Color: 1

Bin 198: 78 of cap free
Amount of items: 2
Items: 
Size: 692395 Color: 3
Size: 307528 Color: 1

Bin 199: 79 of cap free
Amount of items: 3
Items: 
Size: 562593 Color: 3
Size: 218793 Color: 0
Size: 218536 Color: 1

Bin 200: 79 of cap free
Amount of items: 2
Items: 
Size: 675115 Color: 1
Size: 324807 Color: 4

Bin 201: 79 of cap free
Amount of items: 3
Items: 
Size: 728849 Color: 3
Size: 136104 Color: 2
Size: 134969 Color: 4

Bin 202: 79 of cap free
Amount of items: 2
Items: 
Size: 748138 Color: 2
Size: 251784 Color: 1

Bin 203: 80 of cap free
Amount of items: 2
Items: 
Size: 736276 Color: 2
Size: 263645 Color: 0

Bin 204: 80 of cap free
Amount of items: 2
Items: 
Size: 765309 Color: 2
Size: 234612 Color: 0

Bin 205: 80 of cap free
Amount of items: 2
Items: 
Size: 789346 Color: 4
Size: 210575 Color: 3

Bin 206: 81 of cap free
Amount of items: 3
Items: 
Size: 787468 Color: 2
Size: 106267 Color: 1
Size: 106185 Color: 0

Bin 207: 82 of cap free
Amount of items: 3
Items: 
Size: 722195 Color: 4
Size: 139772 Color: 2
Size: 137952 Color: 1

Bin 208: 84 of cap free
Amount of items: 2
Items: 
Size: 590057 Color: 2
Size: 409860 Color: 1

Bin 209: 86 of cap free
Amount of items: 3
Items: 
Size: 688300 Color: 1
Size: 157639 Color: 2
Size: 153976 Color: 3

Bin 210: 86 of cap free
Amount of items: 2
Items: 
Size: 698567 Color: 1
Size: 301348 Color: 3

Bin 211: 86 of cap free
Amount of items: 2
Items: 
Size: 756139 Color: 3
Size: 243776 Color: 0

Bin 212: 88 of cap free
Amount of items: 2
Items: 
Size: 523908 Color: 4
Size: 476005 Color: 3

Bin 213: 88 of cap free
Amount of items: 3
Items: 
Size: 709352 Color: 1
Size: 147148 Color: 2
Size: 143413 Color: 4

Bin 214: 89 of cap free
Amount of items: 2
Items: 
Size: 705618 Color: 3
Size: 294294 Color: 1

Bin 215: 89 of cap free
Amount of items: 3
Items: 
Size: 782355 Color: 1
Size: 109464 Color: 2
Size: 108093 Color: 0

Bin 216: 90 of cap free
Amount of items: 2
Items: 
Size: 719319 Color: 2
Size: 280592 Color: 3

Bin 217: 91 of cap free
Amount of items: 3
Items: 
Size: 393777 Color: 4
Size: 309010 Color: 3
Size: 297123 Color: 0

Bin 218: 91 of cap free
Amount of items: 2
Items: 
Size: 572713 Color: 2
Size: 427197 Color: 3

Bin 219: 91 of cap free
Amount of items: 3
Items: 
Size: 730438 Color: 0
Size: 135497 Color: 2
Size: 133975 Color: 2

Bin 220: 92 of cap free
Amount of items: 3
Items: 
Size: 668565 Color: 1
Size: 165732 Color: 0
Size: 165612 Color: 3

Bin 221: 92 of cap free
Amount of items: 2
Items: 
Size: 677228 Color: 0
Size: 322681 Color: 4

Bin 222: 92 of cap free
Amount of items: 2
Items: 
Size: 694047 Color: 2
Size: 305862 Color: 1

Bin 223: 93 of cap free
Amount of items: 3
Items: 
Size: 496720 Color: 0
Size: 252728 Color: 4
Size: 250460 Color: 4

Bin 224: 93 of cap free
Amount of items: 2
Items: 
Size: 629982 Color: 2
Size: 369926 Color: 0

Bin 225: 93 of cap free
Amount of items: 2
Items: 
Size: 790807 Color: 4
Size: 209101 Color: 3

Bin 226: 94 of cap free
Amount of items: 3
Items: 
Size: 443172 Color: 1
Size: 280232 Color: 3
Size: 276503 Color: 3

Bin 227: 94 of cap free
Amount of items: 2
Items: 
Size: 609710 Color: 3
Size: 390197 Color: 0

Bin 228: 94 of cap free
Amount of items: 2
Items: 
Size: 628100 Color: 1
Size: 371807 Color: 4

Bin 229: 94 of cap free
Amount of items: 2
Items: 
Size: 667655 Color: 0
Size: 332252 Color: 3

Bin 230: 95 of cap free
Amount of items: 3
Items: 
Size: 577444 Color: 3
Size: 214936 Color: 2
Size: 207526 Color: 1

Bin 231: 95 of cap free
Amount of items: 2
Items: 
Size: 598470 Color: 0
Size: 401436 Color: 3

Bin 232: 95 of cap free
Amount of items: 2
Items: 
Size: 643550 Color: 2
Size: 356356 Color: 1

Bin 233: 95 of cap free
Amount of items: 3
Items: 
Size: 691453 Color: 0
Size: 154684 Color: 1
Size: 153769 Color: 3

Bin 234: 95 of cap free
Amount of items: 3
Items: 
Size: 723569 Color: 1
Size: 138259 Color: 3
Size: 138078 Color: 0

Bin 235: 96 of cap free
Amount of items: 3
Items: 
Size: 583683 Color: 1
Size: 209303 Color: 3
Size: 206919 Color: 0

Bin 236: 96 of cap free
Amount of items: 3
Items: 
Size: 788390 Color: 1
Size: 106649 Color: 2
Size: 104866 Color: 1

Bin 237: 97 of cap free
Amount of items: 2
Items: 
Size: 575916 Color: 0
Size: 423988 Color: 1

Bin 238: 98 of cap free
Amount of items: 3
Items: 
Size: 384492 Color: 4
Size: 319242 Color: 1
Size: 296169 Color: 3

Bin 239: 98 of cap free
Amount of items: 2
Items: 
Size: 532494 Color: 2
Size: 467409 Color: 3

Bin 240: 98 of cap free
Amount of items: 3
Items: 
Size: 726343 Color: 0
Size: 137318 Color: 2
Size: 136242 Color: 4

Bin 241: 98 of cap free
Amount of items: 3
Items: 
Size: 746751 Color: 1
Size: 127887 Color: 0
Size: 125265 Color: 4

Bin 242: 98 of cap free
Amount of items: 2
Items: 
Size: 774663 Color: 3
Size: 225240 Color: 0

Bin 243: 100 of cap free
Amount of items: 2
Items: 
Size: 554248 Color: 3
Size: 445653 Color: 4

Bin 244: 100 of cap free
Amount of items: 2
Items: 
Size: 617817 Color: 4
Size: 382084 Color: 2

Bin 245: 100 of cap free
Amount of items: 2
Items: 
Size: 655428 Color: 1
Size: 344473 Color: 0

Bin 246: 101 of cap free
Amount of items: 3
Items: 
Size: 408490 Color: 0
Size: 326269 Color: 2
Size: 265141 Color: 2

Bin 247: 101 of cap free
Amount of items: 2
Items: 
Size: 546790 Color: 4
Size: 453110 Color: 1

Bin 248: 101 of cap free
Amount of items: 2
Items: 
Size: 575124 Color: 2
Size: 424776 Color: 4

Bin 249: 101 of cap free
Amount of items: 3
Items: 
Size: 596386 Color: 3
Size: 202015 Color: 1
Size: 201499 Color: 1

Bin 250: 101 of cap free
Amount of items: 2
Items: 
Size: 660843 Color: 1
Size: 339057 Color: 2

Bin 251: 101 of cap free
Amount of items: 2
Items: 
Size: 697017 Color: 3
Size: 302883 Color: 2

Bin 252: 101 of cap free
Amount of items: 2
Items: 
Size: 730329 Color: 2
Size: 269571 Color: 4

Bin 253: 101 of cap free
Amount of items: 2
Items: 
Size: 750846 Color: 2
Size: 249054 Color: 4

Bin 254: 102 of cap free
Amount of items: 2
Items: 
Size: 539944 Color: 2
Size: 459955 Color: 1

Bin 255: 102 of cap free
Amount of items: 2
Items: 
Size: 566307 Color: 3
Size: 433592 Color: 2

Bin 256: 102 of cap free
Amount of items: 2
Items: 
Size: 602478 Color: 3
Size: 397421 Color: 2

Bin 257: 102 of cap free
Amount of items: 2
Items: 
Size: 695587 Color: 0
Size: 304312 Color: 2

Bin 258: 102 of cap free
Amount of items: 2
Items: 
Size: 705262 Color: 1
Size: 294637 Color: 2

Bin 259: 103 of cap free
Amount of items: 2
Items: 
Size: 606635 Color: 0
Size: 393263 Color: 3

Bin 260: 104 of cap free
Amount of items: 2
Items: 
Size: 785510 Color: 1
Size: 214387 Color: 2

Bin 261: 106 of cap free
Amount of items: 2
Items: 
Size: 516888 Color: 2
Size: 483007 Color: 0

Bin 262: 107 of cap free
Amount of items: 2
Items: 
Size: 539621 Color: 1
Size: 460273 Color: 4

Bin 263: 107 of cap free
Amount of items: 2
Items: 
Size: 747818 Color: 4
Size: 252076 Color: 0

Bin 264: 109 of cap free
Amount of items: 2
Items: 
Size: 655162 Color: 2
Size: 344730 Color: 4

Bin 265: 110 of cap free
Amount of items: 2
Items: 
Size: 618258 Color: 4
Size: 381633 Color: 0

Bin 266: 110 of cap free
Amount of items: 2
Items: 
Size: 674570 Color: 2
Size: 325321 Color: 4

Bin 267: 110 of cap free
Amount of items: 3
Items: 
Size: 721661 Color: 1
Size: 140755 Color: 3
Size: 137475 Color: 4

Bin 268: 110 of cap free
Amount of items: 2
Items: 
Size: 726038 Color: 2
Size: 273853 Color: 1

Bin 269: 110 of cap free
Amount of items: 3
Items: 
Size: 763700 Color: 0
Size: 118245 Color: 4
Size: 117946 Color: 2

Bin 270: 112 of cap free
Amount of items: 2
Items: 
Size: 508483 Color: 3
Size: 491406 Color: 2

Bin 271: 112 of cap free
Amount of items: 2
Items: 
Size: 520909 Color: 2
Size: 478980 Color: 0

Bin 272: 113 of cap free
Amount of items: 3
Items: 
Size: 565389 Color: 4
Size: 217268 Color: 3
Size: 217231 Color: 1

Bin 273: 114 of cap free
Amount of items: 2
Items: 
Size: 548554 Color: 3
Size: 451333 Color: 0

Bin 274: 114 of cap free
Amount of items: 2
Items: 
Size: 678180 Color: 2
Size: 321707 Color: 1

Bin 275: 115 of cap free
Amount of items: 2
Items: 
Size: 515152 Color: 0
Size: 484734 Color: 3

Bin 276: 117 of cap free
Amount of items: 2
Items: 
Size: 751903 Color: 0
Size: 247981 Color: 3

Bin 277: 118 of cap free
Amount of items: 2
Items: 
Size: 589612 Color: 1
Size: 410271 Color: 4

Bin 278: 118 of cap free
Amount of items: 3
Items: 
Size: 624041 Color: 0
Size: 188255 Color: 3
Size: 187587 Color: 1

Bin 279: 118 of cap free
Amount of items: 2
Items: 
Size: 777703 Color: 2
Size: 222180 Color: 3

Bin 280: 120 of cap free
Amount of items: 2
Items: 
Size: 614262 Color: 2
Size: 385619 Color: 0

Bin 281: 121 of cap free
Amount of items: 2
Items: 
Size: 657284 Color: 0
Size: 342596 Color: 4

Bin 282: 122 of cap free
Amount of items: 3
Items: 
Size: 602158 Color: 1
Size: 199241 Color: 4
Size: 198480 Color: 4

Bin 283: 122 of cap free
Amount of items: 3
Items: 
Size: 629548 Color: 0
Size: 185681 Color: 3
Size: 184650 Color: 0

Bin 284: 122 of cap free
Amount of items: 3
Items: 
Size: 748312 Color: 4
Size: 126997 Color: 0
Size: 124570 Color: 4

Bin 285: 123 of cap free
Amount of items: 2
Items: 
Size: 776460 Color: 3
Size: 223418 Color: 4

Bin 286: 124 of cap free
Amount of items: 2
Items: 
Size: 580402 Color: 1
Size: 419475 Color: 0

Bin 287: 124 of cap free
Amount of items: 2
Items: 
Size: 676517 Color: 1
Size: 323360 Color: 4

Bin 288: 126 of cap free
Amount of items: 2
Items: 
Size: 626595 Color: 0
Size: 373280 Color: 4

Bin 289: 126 of cap free
Amount of items: 2
Items: 
Size: 673961 Color: 3
Size: 325914 Color: 0

Bin 290: 127 of cap free
Amount of items: 2
Items: 
Size: 717543 Color: 4
Size: 282331 Color: 0

Bin 291: 128 of cap free
Amount of items: 2
Items: 
Size: 561827 Color: 1
Size: 438046 Color: 3

Bin 292: 128 of cap free
Amount of items: 2
Items: 
Size: 693755 Color: 0
Size: 306118 Color: 4

Bin 293: 128 of cap free
Amount of items: 2
Items: 
Size: 710448 Color: 4
Size: 289425 Color: 0

Bin 294: 128 of cap free
Amount of items: 3
Items: 
Size: 742427 Color: 0
Size: 129125 Color: 2
Size: 128321 Color: 1

Bin 295: 130 of cap free
Amount of items: 2
Items: 
Size: 612432 Color: 2
Size: 387439 Color: 4

Bin 296: 130 of cap free
Amount of items: 3
Items: 
Size: 766115 Color: 3
Size: 117388 Color: 1
Size: 116368 Color: 2

Bin 297: 131 of cap free
Amount of items: 2
Items: 
Size: 578292 Color: 2
Size: 421578 Color: 1

Bin 298: 132 of cap free
Amount of items: 2
Items: 
Size: 583196 Color: 1
Size: 416673 Color: 3

Bin 299: 132 of cap free
Amount of items: 2
Items: 
Size: 650579 Color: 1
Size: 349290 Color: 3

Bin 300: 133 of cap free
Amount of items: 2
Items: 
Size: 754517 Color: 2
Size: 245351 Color: 3

Bin 301: 134 of cap free
Amount of items: 3
Items: 
Size: 352668 Color: 1
Size: 326412 Color: 3
Size: 320787 Color: 4

Bin 302: 134 of cap free
Amount of items: 3
Items: 
Size: 408168 Color: 1
Size: 336857 Color: 4
Size: 254842 Color: 3

Bin 303: 134 of cap free
Amount of items: 3
Items: 
Size: 709759 Color: 1
Size: 146216 Color: 2
Size: 143892 Color: 2

Bin 304: 134 of cap free
Amount of items: 3
Items: 
Size: 782147 Color: 1
Size: 109303 Color: 3
Size: 108417 Color: 4

Bin 305: 135 of cap free
Amount of items: 2
Items: 
Size: 527113 Color: 0
Size: 472753 Color: 4

Bin 306: 135 of cap free
Amount of items: 3
Items: 
Size: 656680 Color: 4
Size: 172054 Color: 1
Size: 171132 Color: 1

Bin 307: 136 of cap free
Amount of items: 2
Items: 
Size: 507543 Color: 0
Size: 492322 Color: 1

Bin 308: 137 of cap free
Amount of items: 2
Items: 
Size: 799809 Color: 3
Size: 200055 Color: 4

Bin 309: 139 of cap free
Amount of items: 2
Items: 
Size: 718651 Color: 4
Size: 281211 Color: 1

Bin 310: 139 of cap free
Amount of items: 2
Items: 
Size: 741353 Color: 3
Size: 258509 Color: 2

Bin 311: 140 of cap free
Amount of items: 2
Items: 
Size: 536325 Color: 2
Size: 463536 Color: 0

Bin 312: 140 of cap free
Amount of items: 2
Items: 
Size: 613969 Color: 3
Size: 385892 Color: 4

Bin 313: 141 of cap free
Amount of items: 2
Items: 
Size: 795354 Color: 0
Size: 204506 Color: 1

Bin 314: 142 of cap free
Amount of items: 3
Items: 
Size: 432406 Color: 4
Size: 312290 Color: 0
Size: 255163 Color: 1

Bin 315: 143 of cap free
Amount of items: 2
Items: 
Size: 641562 Color: 3
Size: 358296 Color: 1

Bin 316: 144 of cap free
Amount of items: 2
Items: 
Size: 624990 Color: 4
Size: 374867 Color: 1

Bin 317: 144 of cap free
Amount of items: 3
Items: 
Size: 727605 Color: 3
Size: 136517 Color: 0
Size: 135735 Color: 0

Bin 318: 146 of cap free
Amount of items: 3
Items: 
Size: 443751 Color: 0
Size: 279810 Color: 2
Size: 276294 Color: 2

Bin 319: 146 of cap free
Amount of items: 2
Items: 
Size: 562299 Color: 3
Size: 437556 Color: 0

Bin 320: 146 of cap free
Amount of items: 2
Items: 
Size: 602773 Color: 2
Size: 397082 Color: 0

Bin 321: 146 of cap free
Amount of items: 2
Items: 
Size: 786114 Color: 4
Size: 213741 Color: 1

Bin 322: 147 of cap free
Amount of items: 2
Items: 
Size: 560723 Color: 1
Size: 439131 Color: 0

Bin 323: 148 of cap free
Amount of items: 3
Items: 
Size: 408767 Color: 4
Size: 327545 Color: 1
Size: 263541 Color: 0

Bin 324: 148 of cap free
Amount of items: 3
Items: 
Size: 409034 Color: 3
Size: 310322 Color: 0
Size: 280497 Color: 1

Bin 325: 148 of cap free
Amount of items: 3
Items: 
Size: 414681 Color: 2
Size: 308306 Color: 0
Size: 276866 Color: 1

Bin 326: 148 of cap free
Amount of items: 2
Items: 
Size: 707793 Color: 3
Size: 292060 Color: 1

Bin 327: 148 of cap free
Amount of items: 3
Items: 
Size: 728617 Color: 2
Size: 137012 Color: 4
Size: 134224 Color: 0

Bin 328: 149 of cap free
Amount of items: 3
Items: 
Size: 408166 Color: 2
Size: 296224 Color: 4
Size: 295462 Color: 4

Bin 329: 149 of cap free
Amount of items: 2
Items: 
Size: 599566 Color: 1
Size: 400286 Color: 3

Bin 330: 149 of cap free
Amount of items: 3
Items: 
Size: 769921 Color: 4
Size: 114979 Color: 0
Size: 114952 Color: 1

Bin 331: 150 of cap free
Amount of items: 3
Items: 
Size: 621327 Color: 2
Size: 189818 Color: 4
Size: 188706 Color: 3

Bin 332: 150 of cap free
Amount of items: 2
Items: 
Size: 709327 Color: 2
Size: 290524 Color: 1

Bin 333: 151 of cap free
Amount of items: 2
Items: 
Size: 532644 Color: 2
Size: 467206 Color: 3

Bin 334: 152 of cap free
Amount of items: 2
Items: 
Size: 712272 Color: 1
Size: 287577 Color: 3

Bin 335: 153 of cap free
Amount of items: 2
Items: 
Size: 637125 Color: 1
Size: 362723 Color: 0

Bin 336: 154 of cap free
Amount of items: 2
Items: 
Size: 663833 Color: 4
Size: 336014 Color: 3

Bin 337: 154 of cap free
Amount of items: 2
Items: 
Size: 738778 Color: 2
Size: 261069 Color: 0

Bin 338: 154 of cap free
Amount of items: 3
Items: 
Size: 763315 Color: 4
Size: 119006 Color: 1
Size: 117526 Color: 2

Bin 339: 155 of cap free
Amount of items: 2
Items: 
Size: 563075 Color: 2
Size: 436771 Color: 1

Bin 340: 156 of cap free
Amount of items: 3
Items: 
Size: 494393 Color: 3
Size: 253057 Color: 1
Size: 252395 Color: 2

Bin 341: 156 of cap free
Amount of items: 2
Items: 
Size: 541472 Color: 3
Size: 458373 Color: 0

Bin 342: 156 of cap free
Amount of items: 2
Items: 
Size: 543628 Color: 0
Size: 456217 Color: 1

Bin 343: 156 of cap free
Amount of items: 2
Items: 
Size: 636629 Color: 3
Size: 363216 Color: 0

Bin 344: 157 of cap free
Amount of items: 3
Items: 
Size: 708387 Color: 3
Size: 147137 Color: 2
Size: 144320 Color: 4

Bin 345: 160 of cap free
Amount of items: 2
Items: 
Size: 761806 Color: 4
Size: 238035 Color: 3

Bin 346: 162 of cap free
Amount of items: 2
Items: 
Size: 765238 Color: 2
Size: 234601 Color: 0

Bin 347: 164 of cap free
Amount of items: 3
Items: 
Size: 656607 Color: 3
Size: 171910 Color: 4
Size: 171320 Color: 2

Bin 348: 166 of cap free
Amount of items: 3
Items: 
Size: 410192 Color: 1
Size: 326069 Color: 2
Size: 263574 Color: 2

Bin 349: 168 of cap free
Amount of items: 3
Items: 
Size: 742882 Color: 4
Size: 130432 Color: 0
Size: 126519 Color: 0

Bin 350: 168 of cap free
Amount of items: 2
Items: 
Size: 778637 Color: 3
Size: 221196 Color: 0

Bin 351: 169 of cap free
Amount of items: 2
Items: 
Size: 532597 Color: 3
Size: 467235 Color: 2

Bin 352: 169 of cap free
Amount of items: 2
Items: 
Size: 582715 Color: 1
Size: 417117 Color: 2

Bin 353: 169 of cap free
Amount of items: 2
Items: 
Size: 764260 Color: 3
Size: 235572 Color: 4

Bin 354: 171 of cap free
Amount of items: 2
Items: 
Size: 684640 Color: 1
Size: 315190 Color: 3

Bin 355: 171 of cap free
Amount of items: 2
Items: 
Size: 688997 Color: 1
Size: 310833 Color: 2

Bin 356: 172 of cap free
Amount of items: 3
Items: 
Size: 533371 Color: 1
Size: 235487 Color: 2
Size: 230971 Color: 0

Bin 357: 174 of cap free
Amount of items: 2
Items: 
Size: 635018 Color: 4
Size: 364809 Color: 2

Bin 358: 174 of cap free
Amount of items: 2
Items: 
Size: 667332 Color: 2
Size: 332495 Color: 4

Bin 359: 174 of cap free
Amount of items: 2
Items: 
Size: 724923 Color: 3
Size: 274904 Color: 4

Bin 360: 174 of cap free
Amount of items: 2
Items: 
Size: 798818 Color: 4
Size: 201009 Color: 0

Bin 361: 176 of cap free
Amount of items: 2
Items: 
Size: 585691 Color: 4
Size: 414134 Color: 3

Bin 362: 176 of cap free
Amount of items: 2
Items: 
Size: 785457 Color: 4
Size: 214368 Color: 2

Bin 363: 177 of cap free
Amount of items: 2
Items: 
Size: 621380 Color: 4
Size: 378444 Color: 0

Bin 364: 179 of cap free
Amount of items: 2
Items: 
Size: 521661 Color: 0
Size: 478161 Color: 4

Bin 365: 179 of cap free
Amount of items: 2
Items: 
Size: 660450 Color: 4
Size: 339372 Color: 2

Bin 366: 179 of cap free
Amount of items: 2
Items: 
Size: 781511 Color: 2
Size: 218311 Color: 0

Bin 367: 180 of cap free
Amount of items: 2
Items: 
Size: 543976 Color: 2
Size: 455845 Color: 1

Bin 368: 180 of cap free
Amount of items: 2
Items: 
Size: 703989 Color: 0
Size: 295832 Color: 2

Bin 369: 180 of cap free
Amount of items: 2
Items: 
Size: 707094 Color: 3
Size: 292727 Color: 4

Bin 370: 182 of cap free
Amount of items: 2
Items: 
Size: 784569 Color: 2
Size: 215250 Color: 3

Bin 371: 184 of cap free
Amount of items: 2
Items: 
Size: 522334 Color: 1
Size: 477483 Color: 2

Bin 372: 185 of cap free
Amount of items: 2
Items: 
Size: 618606 Color: 3
Size: 381210 Color: 4

Bin 373: 189 of cap free
Amount of items: 2
Items: 
Size: 589351 Color: 0
Size: 410461 Color: 2

Bin 374: 189 of cap free
Amount of items: 3
Items: 
Size: 678482 Color: 0
Size: 161277 Color: 2
Size: 160053 Color: 2

Bin 375: 192 of cap free
Amount of items: 2
Items: 
Size: 522933 Color: 1
Size: 476876 Color: 3

Bin 376: 192 of cap free
Amount of items: 2
Items: 
Size: 618257 Color: 2
Size: 381552 Color: 1

Bin 377: 193 of cap free
Amount of items: 2
Items: 
Size: 526775 Color: 1
Size: 473033 Color: 0

Bin 378: 193 of cap free
Amount of items: 2
Items: 
Size: 541471 Color: 1
Size: 458337 Color: 2

Bin 379: 194 of cap free
Amount of items: 2
Items: 
Size: 776807 Color: 4
Size: 223000 Color: 3

Bin 380: 195 of cap free
Amount of items: 2
Items: 
Size: 734429 Color: 3
Size: 265377 Color: 2

Bin 381: 196 of cap free
Amount of items: 2
Items: 
Size: 524658 Color: 3
Size: 475147 Color: 2

Bin 382: 201 of cap free
Amount of items: 3
Items: 
Size: 701920 Color: 1
Size: 149058 Color: 2
Size: 148822 Color: 0

Bin 383: 202 of cap free
Amount of items: 3
Items: 
Size: 660977 Color: 3
Size: 170035 Color: 0
Size: 168787 Color: 4

Bin 384: 202 of cap free
Amount of items: 3
Items: 
Size: 695243 Color: 1
Size: 152495 Color: 3
Size: 152061 Color: 3

Bin 385: 203 of cap free
Amount of items: 2
Items: 
Size: 697986 Color: 1
Size: 301812 Color: 2

Bin 386: 204 of cap free
Amount of items: 2
Items: 
Size: 652548 Color: 4
Size: 347249 Color: 1

Bin 387: 205 of cap free
Amount of items: 2
Items: 
Size: 510173 Color: 0
Size: 489623 Color: 1

Bin 388: 205 of cap free
Amount of items: 3
Items: 
Size: 630365 Color: 4
Size: 184755 Color: 1
Size: 184676 Color: 0

Bin 389: 207 of cap free
Amount of items: 2
Items: 
Size: 541767 Color: 1
Size: 458027 Color: 0

Bin 390: 209 of cap free
Amount of items: 3
Items: 
Size: 385317 Color: 1
Size: 308121 Color: 0
Size: 306354 Color: 0

Bin 391: 210 of cap free
Amount of items: 2
Items: 
Size: 701907 Color: 4
Size: 297884 Color: 1

Bin 392: 211 of cap free
Amount of items: 2
Items: 
Size: 791795 Color: 2
Size: 207995 Color: 1

Bin 393: 212 of cap free
Amount of items: 2
Items: 
Size: 650813 Color: 2
Size: 348976 Color: 3

Bin 394: 212 of cap free
Amount of items: 2
Items: 
Size: 724658 Color: 1
Size: 275131 Color: 3

Bin 395: 214 of cap free
Amount of items: 2
Items: 
Size: 593870 Color: 4
Size: 405917 Color: 1

Bin 396: 214 of cap free
Amount of items: 2
Items: 
Size: 631935 Color: 0
Size: 367852 Color: 4

Bin 397: 215 of cap free
Amount of items: 2
Items: 
Size: 726671 Color: 2
Size: 273115 Color: 1

Bin 398: 217 of cap free
Amount of items: 2
Items: 
Size: 597132 Color: 2
Size: 402652 Color: 0

Bin 399: 217 of cap free
Amount of items: 2
Items: 
Size: 683349 Color: 3
Size: 316435 Color: 1

Bin 400: 219 of cap free
Amount of items: 2
Items: 
Size: 518120 Color: 3
Size: 481662 Color: 2

Bin 401: 220 of cap free
Amount of items: 3
Items: 
Size: 668311 Color: 4
Size: 165924 Color: 2
Size: 165546 Color: 4

Bin 402: 224 of cap free
Amount of items: 3
Items: 
Size: 499274 Color: 0
Size: 250698 Color: 1
Size: 249805 Color: 0

Bin 403: 225 of cap free
Amount of items: 2
Items: 
Size: 754074 Color: 1
Size: 245702 Color: 2

Bin 404: 226 of cap free
Amount of items: 2
Items: 
Size: 594976 Color: 0
Size: 404799 Color: 2

Bin 405: 228 of cap free
Amount of items: 3
Items: 
Size: 701408 Color: 4
Size: 150003 Color: 0
Size: 148362 Color: 2

Bin 406: 228 of cap free
Amount of items: 2
Items: 
Size: 725704 Color: 1
Size: 274069 Color: 4

Bin 407: 230 of cap free
Amount of items: 2
Items: 
Size: 553414 Color: 3
Size: 446357 Color: 1

Bin 408: 230 of cap free
Amount of items: 3
Items: 
Size: 773589 Color: 3
Size: 113137 Color: 2
Size: 113045 Color: 3

Bin 409: 232 of cap free
Amount of items: 2
Items: 
Size: 586952 Color: 2
Size: 412817 Color: 4

Bin 410: 232 of cap free
Amount of items: 3
Items: 
Size: 647507 Color: 4
Size: 177866 Color: 1
Size: 174396 Color: 3

Bin 411: 232 of cap free
Amount of items: 2
Items: 
Size: 684226 Color: 3
Size: 315543 Color: 1

Bin 412: 236 of cap free
Amount of items: 2
Items: 
Size: 573911 Color: 2
Size: 425854 Color: 0

Bin 413: 239 of cap free
Amount of items: 2
Items: 
Size: 770375 Color: 1
Size: 229387 Color: 2

Bin 414: 240 of cap free
Amount of items: 2
Items: 
Size: 756888 Color: 3
Size: 242873 Color: 0

Bin 415: 241 of cap free
Amount of items: 2
Items: 
Size: 501455 Color: 0
Size: 498305 Color: 3

Bin 416: 241 of cap free
Amount of items: 3
Items: 
Size: 775696 Color: 2
Size: 112768 Color: 0
Size: 111296 Color: 4

Bin 417: 241 of cap free
Amount of items: 3
Items: 
Size: 779204 Color: 2
Size: 110858 Color: 0
Size: 109698 Color: 2

Bin 418: 244 of cap free
Amount of items: 2
Items: 
Size: 659183 Color: 3
Size: 340574 Color: 0

Bin 419: 245 of cap free
Amount of items: 2
Items: 
Size: 610075 Color: 1
Size: 389681 Color: 4

Bin 420: 245 of cap free
Amount of items: 2
Items: 
Size: 751306 Color: 0
Size: 248450 Color: 4

Bin 421: 248 of cap free
Amount of items: 2
Items: 
Size: 597662 Color: 4
Size: 402091 Color: 0

Bin 422: 248 of cap free
Amount of items: 3
Items: 
Size: 625230 Color: 2
Size: 187319 Color: 1
Size: 187204 Color: 1

Bin 423: 250 of cap free
Amount of items: 2
Items: 
Size: 787358 Color: 1
Size: 212393 Color: 3

Bin 424: 253 of cap free
Amount of items: 2
Items: 
Size: 735949 Color: 4
Size: 263799 Color: 3

Bin 425: 254 of cap free
Amount of items: 3
Items: 
Size: 381861 Color: 2
Size: 320446 Color: 1
Size: 297440 Color: 1

Bin 426: 255 of cap free
Amount of items: 2
Items: 
Size: 632568 Color: 2
Size: 367178 Color: 4

Bin 427: 255 of cap free
Amount of items: 3
Items: 
Size: 653340 Color: 0
Size: 173958 Color: 2
Size: 172448 Color: 3

Bin 428: 256 of cap free
Amount of items: 3
Items: 
Size: 385801 Color: 4
Size: 344598 Color: 2
Size: 269346 Color: 2

Bin 429: 256 of cap free
Amount of items: 2
Items: 
Size: 767248 Color: 1
Size: 232497 Color: 0

Bin 430: 258 of cap free
Amount of items: 2
Items: 
Size: 547473 Color: 0
Size: 452270 Color: 2

Bin 431: 259 of cap free
Amount of items: 2
Items: 
Size: 587754 Color: 1
Size: 411988 Color: 3

Bin 432: 259 of cap free
Amount of items: 2
Items: 
Size: 595823 Color: 4
Size: 403919 Color: 3

Bin 433: 259 of cap free
Amount of items: 2
Items: 
Size: 616237 Color: 4
Size: 383505 Color: 0

Bin 434: 259 of cap free
Amount of items: 2
Items: 
Size: 760875 Color: 3
Size: 238867 Color: 2

Bin 435: 259 of cap free
Amount of items: 3
Items: 
Size: 786401 Color: 3
Size: 106749 Color: 0
Size: 106592 Color: 3

Bin 436: 260 of cap free
Amount of items: 2
Items: 
Size: 562710 Color: 2
Size: 437031 Color: 4

Bin 437: 261 of cap free
Amount of items: 3
Items: 
Size: 394388 Color: 3
Size: 351522 Color: 1
Size: 253830 Color: 0

Bin 438: 264 of cap free
Amount of items: 2
Items: 
Size: 707679 Color: 4
Size: 292058 Color: 3

Bin 439: 266 of cap free
Amount of items: 2
Items: 
Size: 530374 Color: 0
Size: 469361 Color: 1

Bin 440: 267 of cap free
Amount of items: 2
Items: 
Size: 740933 Color: 1
Size: 258801 Color: 2

Bin 441: 268 of cap free
Amount of items: 2
Items: 
Size: 556438 Color: 0
Size: 443295 Color: 4

Bin 442: 268 of cap free
Amount of items: 3
Items: 
Size: 675784 Color: 3
Size: 162361 Color: 2
Size: 161588 Color: 1

Bin 443: 269 of cap free
Amount of items: 2
Items: 
Size: 539460 Color: 0
Size: 460272 Color: 4

Bin 444: 269 of cap free
Amount of items: 3
Items: 
Size: 796450 Color: 3
Size: 102203 Color: 0
Size: 101079 Color: 2

Bin 445: 270 of cap free
Amount of items: 2
Items: 
Size: 692666 Color: 0
Size: 307065 Color: 3

Bin 446: 272 of cap free
Amount of items: 2
Items: 
Size: 613448 Color: 3
Size: 386281 Color: 0

Bin 447: 272 of cap free
Amount of items: 2
Items: 
Size: 621694 Color: 0
Size: 378035 Color: 2

Bin 448: 272 of cap free
Amount of items: 2
Items: 
Size: 641183 Color: 0
Size: 358546 Color: 1

Bin 449: 276 of cap free
Amount of items: 2
Items: 
Size: 560393 Color: 2
Size: 439332 Color: 1

Bin 450: 280 of cap free
Amount of items: 2
Items: 
Size: 704405 Color: 4
Size: 295316 Color: 3

Bin 451: 280 of cap free
Amount of items: 3
Items: 
Size: 779580 Color: 0
Size: 110320 Color: 2
Size: 109821 Color: 3

Bin 452: 281 of cap free
Amount of items: 2
Items: 
Size: 641848 Color: 1
Size: 357872 Color: 2

Bin 453: 282 of cap free
Amount of items: 2
Items: 
Size: 564545 Color: 1
Size: 435174 Color: 4

Bin 454: 284 of cap free
Amount of items: 3
Items: 
Size: 759990 Color: 1
Size: 120808 Color: 2
Size: 118919 Color: 2

Bin 455: 284 of cap free
Amount of items: 3
Items: 
Size: 767717 Color: 4
Size: 116066 Color: 1
Size: 115934 Color: 2

Bin 456: 287 of cap free
Amount of items: 2
Items: 
Size: 619261 Color: 3
Size: 380453 Color: 1

Bin 457: 287 of cap free
Amount of items: 2
Items: 
Size: 677842 Color: 0
Size: 321872 Color: 3

Bin 458: 288 of cap free
Amount of items: 2
Items: 
Size: 608988 Color: 3
Size: 390725 Color: 2

Bin 459: 290 of cap free
Amount of items: 2
Items: 
Size: 517823 Color: 2
Size: 481888 Color: 1

Bin 460: 292 of cap free
Amount of items: 3
Items: 
Size: 605803 Color: 4
Size: 197224 Color: 2
Size: 196682 Color: 0

Bin 461: 293 of cap free
Amount of items: 2
Items: 
Size: 546227 Color: 0
Size: 453481 Color: 3

Bin 462: 293 of cap free
Amount of items: 2
Items: 
Size: 598425 Color: 0
Size: 401283 Color: 2

Bin 463: 299 of cap free
Amount of items: 2
Items: 
Size: 720264 Color: 4
Size: 279438 Color: 0

Bin 464: 300 of cap free
Amount of items: 2
Items: 
Size: 695676 Color: 2
Size: 304025 Color: 1

Bin 465: 303 of cap free
Amount of items: 2
Items: 
Size: 555734 Color: 4
Size: 443964 Color: 0

Bin 466: 306 of cap free
Amount of items: 2
Items: 
Size: 770632 Color: 0
Size: 229063 Color: 2

Bin 467: 308 of cap free
Amount of items: 2
Items: 
Size: 589962 Color: 1
Size: 409731 Color: 4

Bin 468: 309 of cap free
Amount of items: 2
Items: 
Size: 775185 Color: 0
Size: 224507 Color: 4

Bin 469: 310 of cap free
Amount of items: 2
Items: 
Size: 702141 Color: 3
Size: 297550 Color: 1

Bin 470: 314 of cap free
Amount of items: 3
Items: 
Size: 787615 Color: 4
Size: 106829 Color: 0
Size: 105243 Color: 2

Bin 471: 315 of cap free
Amount of items: 2
Items: 
Size: 499888 Color: 4
Size: 499798 Color: 1

Bin 472: 318 of cap free
Amount of items: 2
Items: 
Size: 583440 Color: 3
Size: 416243 Color: 1

Bin 473: 319 of cap free
Amount of items: 3
Items: 
Size: 622617 Color: 1
Size: 188730 Color: 0
Size: 188335 Color: 0

Bin 474: 320 of cap free
Amount of items: 2
Items: 
Size: 777981 Color: 4
Size: 221700 Color: 2

Bin 475: 321 of cap free
Amount of items: 2
Items: 
Size: 628258 Color: 1
Size: 371422 Color: 4

Bin 476: 322 of cap free
Amount of items: 2
Items: 
Size: 514550 Color: 2
Size: 485129 Color: 1

Bin 477: 322 of cap free
Amount of items: 2
Items: 
Size: 573681 Color: 1
Size: 425998 Color: 2

Bin 478: 324 of cap free
Amount of items: 3
Items: 
Size: 697880 Color: 1
Size: 151005 Color: 3
Size: 150792 Color: 1

Bin 479: 325 of cap free
Amount of items: 2
Items: 
Size: 763284 Color: 0
Size: 236392 Color: 4

Bin 480: 326 of cap free
Amount of items: 2
Items: 
Size: 774708 Color: 0
Size: 224967 Color: 4

Bin 481: 329 of cap free
Amount of items: 2
Items: 
Size: 531880 Color: 2
Size: 467792 Color: 3

Bin 482: 330 of cap free
Amount of items: 2
Items: 
Size: 753050 Color: 4
Size: 246621 Color: 0

Bin 483: 330 of cap free
Amount of items: 2
Items: 
Size: 793487 Color: 0
Size: 206184 Color: 1

Bin 484: 338 of cap free
Amount of items: 2
Items: 
Size: 588893 Color: 3
Size: 410770 Color: 4

Bin 485: 339 of cap free
Amount of items: 2
Items: 
Size: 678438 Color: 1
Size: 321224 Color: 4

Bin 486: 339 of cap free
Amount of items: 2
Items: 
Size: 699243 Color: 1
Size: 300419 Color: 0

Bin 487: 340 of cap free
Amount of items: 3
Items: 
Size: 796557 Color: 0
Size: 102957 Color: 2
Size: 100147 Color: 4

Bin 488: 349 of cap free
Amount of items: 2
Items: 
Size: 531654 Color: 4
Size: 467998 Color: 2

Bin 489: 354 of cap free
Amount of items: 3
Items: 
Size: 407999 Color: 0
Size: 312518 Color: 2
Size: 279130 Color: 3

Bin 490: 354 of cap free
Amount of items: 2
Items: 
Size: 561435 Color: 2
Size: 438212 Color: 3

Bin 491: 358 of cap free
Amount of items: 2
Items: 
Size: 560533 Color: 1
Size: 439110 Color: 3

Bin 492: 359 of cap free
Amount of items: 2
Items: 
Size: 631444 Color: 0
Size: 368198 Color: 1

Bin 493: 362 of cap free
Amount of items: 2
Items: 
Size: 536851 Color: 4
Size: 462788 Color: 3

Bin 494: 362 of cap free
Amount of items: 2
Items: 
Size: 732255 Color: 4
Size: 267384 Color: 1

Bin 495: 366 of cap free
Amount of items: 2
Items: 
Size: 586336 Color: 4
Size: 413299 Color: 2

Bin 496: 367 of cap free
Amount of items: 3
Items: 
Size: 381860 Color: 3
Size: 320823 Color: 2
Size: 296951 Color: 1

Bin 497: 369 of cap free
Amount of items: 2
Items: 
Size: 519844 Color: 3
Size: 479788 Color: 1

Bin 498: 370 of cap free
Amount of items: 2
Items: 
Size: 549024 Color: 1
Size: 450607 Color: 0

Bin 499: 371 of cap free
Amount of items: 2
Items: 
Size: 673959 Color: 4
Size: 325671 Color: 2

Bin 500: 372 of cap free
Amount of items: 2
Items: 
Size: 707979 Color: 2
Size: 291650 Color: 0

Bin 501: 373 of cap free
Amount of items: 2
Items: 
Size: 681515 Color: 4
Size: 318113 Color: 1

Bin 502: 375 of cap free
Amount of items: 2
Items: 
Size: 769833 Color: 0
Size: 229793 Color: 2

Bin 503: 376 of cap free
Amount of items: 2
Items: 
Size: 737516 Color: 4
Size: 262109 Color: 2

Bin 504: 376 of cap free
Amount of items: 2
Items: 
Size: 752061 Color: 2
Size: 247564 Color: 3

Bin 505: 378 of cap free
Amount of items: 3
Items: 
Size: 765935 Color: 4
Size: 116940 Color: 1
Size: 116748 Color: 2

Bin 506: 379 of cap free
Amount of items: 3
Items: 
Size: 602639 Color: 0
Size: 199416 Color: 1
Size: 197567 Color: 2

Bin 507: 379 of cap free
Amount of items: 2
Items: 
Size: 607159 Color: 4
Size: 392463 Color: 1

Bin 508: 379 of cap free
Amount of items: 2
Items: 
Size: 682720 Color: 2
Size: 316902 Color: 0

Bin 509: 379 of cap free
Amount of items: 2
Items: 
Size: 695971 Color: 1
Size: 303651 Color: 0

Bin 510: 380 of cap free
Amount of items: 2
Items: 
Size: 563365 Color: 2
Size: 436256 Color: 4

Bin 511: 383 of cap free
Amount of items: 2
Items: 
Size: 778393 Color: 0
Size: 221225 Color: 3

Bin 512: 386 of cap free
Amount of items: 2
Items: 
Size: 715034 Color: 4
Size: 284581 Color: 3

Bin 513: 392 of cap free
Amount of items: 3
Items: 
Size: 341387 Color: 3
Size: 329170 Color: 4
Size: 329052 Color: 0

Bin 514: 392 of cap free
Amount of items: 2
Items: 
Size: 639712 Color: 2
Size: 359897 Color: 3

Bin 515: 395 of cap free
Amount of items: 2
Items: 
Size: 624412 Color: 4
Size: 375194 Color: 3

Bin 516: 395 of cap free
Amount of items: 2
Items: 
Size: 672663 Color: 4
Size: 326943 Color: 1

Bin 517: 397 of cap free
Amount of items: 3
Items: 
Size: 760743 Color: 2
Size: 119452 Color: 1
Size: 119409 Color: 3

Bin 518: 399 of cap free
Amount of items: 2
Items: 
Size: 585645 Color: 2
Size: 413957 Color: 4

Bin 519: 399 of cap free
Amount of items: 2
Items: 
Size: 758871 Color: 3
Size: 240731 Color: 2

Bin 520: 400 of cap free
Amount of items: 2
Items: 
Size: 559675 Color: 2
Size: 439926 Color: 3

Bin 521: 400 of cap free
Amount of items: 3
Items: 
Size: 754049 Color: 4
Size: 123238 Color: 3
Size: 122314 Color: 1

Bin 522: 401 of cap free
Amount of items: 2
Items: 
Size: 554898 Color: 2
Size: 444702 Color: 0

Bin 523: 402 of cap free
Amount of items: 2
Items: 
Size: 757139 Color: 4
Size: 242460 Color: 0

Bin 524: 404 of cap free
Amount of items: 3
Items: 
Size: 740744 Color: 3
Size: 131907 Color: 1
Size: 126946 Color: 0

Bin 525: 405 of cap free
Amount of items: 2
Items: 
Size: 775597 Color: 4
Size: 223999 Color: 2

Bin 526: 411 of cap free
Amount of items: 2
Items: 
Size: 782175 Color: 3
Size: 217415 Color: 0

Bin 527: 412 of cap free
Amount of items: 2
Items: 
Size: 532443 Color: 0
Size: 467146 Color: 4

Bin 528: 412 of cap free
Amount of items: 2
Items: 
Size: 563658 Color: 4
Size: 435931 Color: 0

Bin 529: 412 of cap free
Amount of items: 2
Items: 
Size: 625225 Color: 0
Size: 374364 Color: 3

Bin 530: 412 of cap free
Amount of items: 2
Items: 
Size: 753493 Color: 0
Size: 246096 Color: 4

Bin 531: 413 of cap free
Amount of items: 2
Items: 
Size: 570112 Color: 2
Size: 429476 Color: 4

Bin 532: 413 of cap free
Amount of items: 2
Items: 
Size: 698440 Color: 4
Size: 301148 Color: 0

Bin 533: 417 of cap free
Amount of items: 2
Items: 
Size: 516585 Color: 0
Size: 482999 Color: 4

Bin 534: 417 of cap free
Amount of items: 3
Items: 
Size: 733615 Color: 2
Size: 133361 Color: 1
Size: 132608 Color: 2

Bin 535: 422 of cap free
Amount of items: 2
Items: 
Size: 546143 Color: 3
Size: 453436 Color: 0

Bin 536: 423 of cap free
Amount of items: 2
Items: 
Size: 529293 Color: 4
Size: 470285 Color: 0

Bin 537: 423 of cap free
Amount of items: 2
Items: 
Size: 575265 Color: 4
Size: 424313 Color: 3

Bin 538: 424 of cap free
Amount of items: 2
Items: 
Size: 618217 Color: 4
Size: 381360 Color: 3

Bin 539: 425 of cap free
Amount of items: 2
Items: 
Size: 523955 Color: 3
Size: 475621 Color: 4

Bin 540: 425 of cap free
Amount of items: 2
Items: 
Size: 568748 Color: 0
Size: 430828 Color: 2

Bin 541: 425 of cap free
Amount of items: 2
Items: 
Size: 616323 Color: 0
Size: 383253 Color: 1

Bin 542: 428 of cap free
Amount of items: 3
Items: 
Size: 510635 Color: 2
Size: 245548 Color: 3
Size: 243390 Color: 3

Bin 543: 432 of cap free
Amount of items: 3
Items: 
Size: 768586 Color: 2
Size: 115580 Color: 4
Size: 115403 Color: 1

Bin 544: 433 of cap free
Amount of items: 3
Items: 
Size: 349822 Color: 4
Size: 325146 Color: 2
Size: 324600 Color: 1

Bin 545: 433 of cap free
Amount of items: 2
Items: 
Size: 716288 Color: 1
Size: 283280 Color: 0

Bin 546: 437 of cap free
Amount of items: 2
Items: 
Size: 554951 Color: 0
Size: 444613 Color: 3

Bin 547: 444 of cap free
Amount of items: 3
Items: 
Size: 660339 Color: 2
Size: 169891 Color: 0
Size: 169327 Color: 2

Bin 548: 450 of cap free
Amount of items: 2
Items: 
Size: 543585 Color: 4
Size: 455966 Color: 2

Bin 549: 451 of cap free
Amount of items: 2
Items: 
Size: 525775 Color: 3
Size: 473775 Color: 2

Bin 550: 451 of cap free
Amount of items: 3
Items: 
Size: 718852 Color: 3
Size: 140941 Color: 2
Size: 139757 Color: 2

Bin 551: 454 of cap free
Amount of items: 2
Items: 
Size: 576222 Color: 2
Size: 423325 Color: 3

Bin 552: 454 of cap free
Amount of items: 2
Items: 
Size: 589295 Color: 0
Size: 410252 Color: 4

Bin 553: 454 of cap free
Amount of items: 2
Items: 
Size: 786317 Color: 2
Size: 213230 Color: 3

Bin 554: 455 of cap free
Amount of items: 2
Items: 
Size: 611630 Color: 4
Size: 387916 Color: 3

Bin 555: 458 of cap free
Amount of items: 2
Items: 
Size: 526514 Color: 2
Size: 473029 Color: 0

Bin 556: 459 of cap free
Amount of items: 2
Items: 
Size: 553350 Color: 2
Size: 446192 Color: 1

Bin 557: 462 of cap free
Amount of items: 2
Items: 
Size: 577364 Color: 4
Size: 422175 Color: 1

Bin 558: 462 of cap free
Amount of items: 3
Items: 
Size: 672663 Color: 0
Size: 163720 Color: 4
Size: 163156 Color: 2

Bin 559: 466 of cap free
Amount of items: 2
Items: 
Size: 679652 Color: 3
Size: 319883 Color: 2

Bin 560: 468 of cap free
Amount of items: 2
Items: 
Size: 675951 Color: 2
Size: 323582 Color: 1

Bin 561: 470 of cap free
Amount of items: 2
Items: 
Size: 656420 Color: 2
Size: 343111 Color: 4

Bin 562: 472 of cap free
Amount of items: 2
Items: 
Size: 791558 Color: 4
Size: 207971 Color: 1

Bin 563: 474 of cap free
Amount of items: 2
Items: 
Size: 608289 Color: 2
Size: 391238 Color: 3

Bin 564: 479 of cap free
Amount of items: 3
Items: 
Size: 584444 Color: 0
Size: 207544 Color: 1
Size: 207534 Color: 4

Bin 565: 479 of cap free
Amount of items: 2
Items: 
Size: 709067 Color: 0
Size: 290455 Color: 2

Bin 566: 479 of cap free
Amount of items: 2
Items: 
Size: 759969 Color: 0
Size: 239553 Color: 1

Bin 567: 480 of cap free
Amount of items: 2
Items: 
Size: 556323 Color: 2
Size: 443198 Color: 1

Bin 568: 489 of cap free
Amount of items: 2
Items: 
Size: 724680 Color: 3
Size: 274832 Color: 0

Bin 569: 494 of cap free
Amount of items: 3
Items: 
Size: 528045 Color: 2
Size: 236085 Color: 4
Size: 235377 Color: 4

Bin 570: 494 of cap free
Amount of items: 3
Items: 
Size: 632486 Color: 0
Size: 184935 Color: 1
Size: 182086 Color: 1

Bin 571: 499 of cap free
Amount of items: 2
Items: 
Size: 550234 Color: 2
Size: 449268 Color: 4

Bin 572: 501 of cap free
Amount of items: 3
Items: 
Size: 716856 Color: 4
Size: 143135 Color: 0
Size: 139509 Color: 2

Bin 573: 502 of cap free
Amount of items: 2
Items: 
Size: 504748 Color: 1
Size: 494751 Color: 4

Bin 574: 511 of cap free
Amount of items: 2
Items: 
Size: 685296 Color: 2
Size: 314194 Color: 1

Bin 575: 517 of cap free
Amount of items: 3
Items: 
Size: 519793 Color: 0
Size: 241301 Color: 3
Size: 238390 Color: 4

Bin 576: 517 of cap free
Amount of items: 2
Items: 
Size: 762336 Color: 1
Size: 237148 Color: 3

Bin 577: 518 of cap free
Amount of items: 3
Items: 
Size: 502503 Color: 1
Size: 248732 Color: 3
Size: 248248 Color: 0

Bin 578: 520 of cap free
Amount of items: 3
Items: 
Size: 382318 Color: 1
Size: 318885 Color: 0
Size: 298278 Color: 2

Bin 579: 520 of cap free
Amount of items: 2
Items: 
Size: 507740 Color: 0
Size: 491741 Color: 1

Bin 580: 522 of cap free
Amount of items: 2
Items: 
Size: 735270 Color: 2
Size: 264209 Color: 4

Bin 581: 524 of cap free
Amount of items: 3
Items: 
Size: 677605 Color: 1
Size: 161211 Color: 2
Size: 160661 Color: 3

Bin 582: 527 of cap free
Amount of items: 2
Items: 
Size: 705502 Color: 2
Size: 293972 Color: 4

Bin 583: 532 of cap free
Amount of items: 2
Items: 
Size: 733417 Color: 3
Size: 266052 Color: 2

Bin 584: 536 of cap free
Amount of items: 2
Items: 
Size: 557726 Color: 4
Size: 441739 Color: 3

Bin 585: 538 of cap free
Amount of items: 2
Items: 
Size: 598326 Color: 4
Size: 401137 Color: 1

Bin 586: 540 of cap free
Amount of items: 3
Items: 
Size: 626528 Color: 3
Size: 186594 Color: 0
Size: 186339 Color: 0

Bin 587: 544 of cap free
Amount of items: 2
Items: 
Size: 550260 Color: 4
Size: 449197 Color: 1

Bin 588: 544 of cap free
Amount of items: 2
Items: 
Size: 626981 Color: 0
Size: 372476 Color: 1

Bin 589: 549 of cap free
Amount of items: 3
Items: 
Size: 758831 Color: 3
Size: 120728 Color: 2
Size: 119893 Color: 4

Bin 590: 557 of cap free
Amount of items: 2
Items: 
Size: 722063 Color: 0
Size: 277381 Color: 2

Bin 591: 563 of cap free
Amount of items: 2
Items: 
Size: 767646 Color: 2
Size: 231792 Color: 4

Bin 592: 567 of cap free
Amount of items: 2
Items: 
Size: 706194 Color: 2
Size: 293240 Color: 1

Bin 593: 576 of cap free
Amount of items: 2
Items: 
Size: 714970 Color: 0
Size: 284455 Color: 4

Bin 594: 578 of cap free
Amount of items: 2
Items: 
Size: 666088 Color: 0
Size: 333335 Color: 4

Bin 595: 579 of cap free
Amount of items: 2
Items: 
Size: 578241 Color: 2
Size: 421181 Color: 1

Bin 596: 586 of cap free
Amount of items: 2
Items: 
Size: 644436 Color: 1
Size: 354979 Color: 3

Bin 597: 589 of cap free
Amount of items: 2
Items: 
Size: 774595 Color: 4
Size: 224817 Color: 3

Bin 598: 596 of cap free
Amount of items: 2
Items: 
Size: 534222 Color: 4
Size: 465183 Color: 3

Bin 599: 603 of cap free
Amount of items: 3
Items: 
Size: 385597 Color: 2
Size: 339151 Color: 1
Size: 274650 Color: 3

Bin 600: 604 of cap free
Amount of items: 2
Items: 
Size: 787457 Color: 3
Size: 211940 Color: 0

Bin 601: 605 of cap free
Amount of items: 2
Items: 
Size: 552648 Color: 2
Size: 446748 Color: 0

Bin 602: 614 of cap free
Amount of items: 2
Items: 
Size: 791587 Color: 1
Size: 207800 Color: 4

Bin 603: 617 of cap free
Amount of items: 2
Items: 
Size: 608179 Color: 2
Size: 391205 Color: 3

Bin 604: 637 of cap free
Amount of items: 3
Items: 
Size: 741936 Color: 3
Size: 132654 Color: 0
Size: 124774 Color: 2

Bin 605: 645 of cap free
Amount of items: 2
Items: 
Size: 771964 Color: 0
Size: 227392 Color: 4

Bin 606: 648 of cap free
Amount of items: 2
Items: 
Size: 762223 Color: 4
Size: 237130 Color: 0

Bin 607: 653 of cap free
Amount of items: 2
Items: 
Size: 785403 Color: 2
Size: 213945 Color: 1

Bin 608: 654 of cap free
Amount of items: 2
Items: 
Size: 542525 Color: 0
Size: 456822 Color: 2

Bin 609: 662 of cap free
Amount of items: 2
Items: 
Size: 525110 Color: 0
Size: 474229 Color: 2

Bin 610: 668 of cap free
Amount of items: 2
Items: 
Size: 696409 Color: 0
Size: 302924 Color: 3

Bin 611: 668 of cap free
Amount of items: 2
Items: 
Size: 771243 Color: 4
Size: 228090 Color: 1

Bin 612: 685 of cap free
Amount of items: 2
Items: 
Size: 787397 Color: 3
Size: 211919 Color: 4

Bin 613: 692 of cap free
Amount of items: 2
Items: 
Size: 639695 Color: 4
Size: 359614 Color: 3

Bin 614: 694 of cap free
Amount of items: 2
Items: 
Size: 653881 Color: 2
Size: 345426 Color: 4

Bin 615: 695 of cap free
Amount of items: 3
Items: 
Size: 599256 Color: 0
Size: 200660 Color: 3
Size: 199390 Color: 0

Bin 616: 698 of cap free
Amount of items: 2
Items: 
Size: 713380 Color: 0
Size: 285923 Color: 2

Bin 617: 702 of cap free
Amount of items: 2
Items: 
Size: 600073 Color: 2
Size: 399226 Color: 0

Bin 618: 704 of cap free
Amount of items: 2
Items: 
Size: 522876 Color: 1
Size: 476421 Color: 0

Bin 619: 708 of cap free
Amount of items: 2
Items: 
Size: 535513 Color: 3
Size: 463780 Color: 4

Bin 620: 713 of cap free
Amount of items: 2
Items: 
Size: 587206 Color: 0
Size: 412082 Color: 1

Bin 621: 720 of cap free
Amount of items: 2
Items: 
Size: 733355 Color: 1
Size: 265926 Color: 3

Bin 622: 734 of cap free
Amount of items: 2
Items: 
Size: 724586 Color: 0
Size: 274681 Color: 2

Bin 623: 738 of cap free
Amount of items: 2
Items: 
Size: 517633 Color: 4
Size: 481630 Color: 3

Bin 624: 740 of cap free
Amount of items: 2
Items: 
Size: 666069 Color: 1
Size: 333192 Color: 4

Bin 625: 749 of cap free
Amount of items: 2
Items: 
Size: 581681 Color: 1
Size: 417571 Color: 0

Bin 626: 762 of cap free
Amount of items: 2
Items: 
Size: 732359 Color: 1
Size: 266880 Color: 3

Bin 627: 767 of cap free
Amount of items: 2
Items: 
Size: 699103 Color: 3
Size: 300131 Color: 1

Bin 628: 768 of cap free
Amount of items: 2
Items: 
Size: 683791 Color: 4
Size: 315442 Color: 1

Bin 629: 769 of cap free
Amount of items: 2
Items: 
Size: 542562 Color: 2
Size: 456670 Color: 1

Bin 630: 772 of cap free
Amount of items: 2
Items: 
Size: 562517 Color: 3
Size: 436712 Color: 2

Bin 631: 774 of cap free
Amount of items: 2
Items: 
Size: 765877 Color: 0
Size: 233350 Color: 4

Bin 632: 776 of cap free
Amount of items: 2
Items: 
Size: 713619 Color: 2
Size: 285606 Color: 0

Bin 633: 778 of cap free
Amount of items: 2
Items: 
Size: 602397 Color: 4
Size: 396826 Color: 1

Bin 634: 781 of cap free
Amount of items: 2
Items: 
Size: 686059 Color: 2
Size: 313161 Color: 0

Bin 635: 793 of cap free
Amount of items: 3
Items: 
Size: 740519 Color: 4
Size: 129726 Color: 0
Size: 128963 Color: 2

Bin 636: 794 of cap free
Amount of items: 2
Items: 
Size: 562472 Color: 2
Size: 436735 Color: 3

Bin 637: 807 of cap free
Amount of items: 2
Items: 
Size: 512299 Color: 0
Size: 486895 Color: 4

Bin 638: 812 of cap free
Amount of items: 2
Items: 
Size: 783647 Color: 0
Size: 215542 Color: 4

Bin 639: 814 of cap free
Amount of items: 2
Items: 
Size: 566531 Color: 2
Size: 432656 Color: 1

Bin 640: 819 of cap free
Amount of items: 2
Items: 
Size: 568714 Color: 2
Size: 430468 Color: 1

Bin 641: 821 of cap free
Amount of items: 2
Items: 
Size: 622270 Color: 4
Size: 376910 Color: 1

Bin 642: 831 of cap free
Amount of items: 2
Items: 
Size: 718610 Color: 3
Size: 280560 Color: 4

Bin 643: 835 of cap free
Amount of items: 2
Items: 
Size: 532946 Color: 3
Size: 466220 Color: 2

Bin 644: 841 of cap free
Amount of items: 2
Items: 
Size: 607222 Color: 1
Size: 391938 Color: 4

Bin 645: 841 of cap free
Amount of items: 2
Items: 
Size: 612976 Color: 1
Size: 386184 Color: 4

Bin 646: 850 of cap free
Amount of items: 2
Items: 
Size: 595748 Color: 0
Size: 403403 Color: 3

Bin 647: 860 of cap free
Amount of items: 2
Items: 
Size: 667293 Color: 2
Size: 331848 Color: 3

Bin 648: 864 of cap free
Amount of items: 2
Items: 
Size: 685957 Color: 4
Size: 313180 Color: 2

Bin 649: 865 of cap free
Amount of items: 3
Items: 
Size: 385262 Color: 0
Size: 310185 Color: 2
Size: 303689 Color: 1

Bin 650: 870 of cap free
Amount of items: 3
Items: 
Size: 612326 Color: 2
Size: 193500 Color: 1
Size: 193305 Color: 4

Bin 651: 875 of cap free
Amount of items: 2
Items: 
Size: 758732 Color: 3
Size: 240394 Color: 4

Bin 652: 879 of cap free
Amount of items: 2
Items: 
Size: 576999 Color: 0
Size: 422123 Color: 3

Bin 653: 879 of cap free
Amount of items: 2
Items: 
Size: 759558 Color: 4
Size: 239564 Color: 0

Bin 654: 883 of cap free
Amount of items: 2
Items: 
Size: 545692 Color: 3
Size: 453426 Color: 1

Bin 655: 891 of cap free
Amount of items: 2
Items: 
Size: 570762 Color: 4
Size: 428348 Color: 1

Bin 656: 892 of cap free
Amount of items: 2
Items: 
Size: 599162 Color: 0
Size: 399947 Color: 2

Bin 657: 902 of cap free
Amount of items: 2
Items: 
Size: 673666 Color: 2
Size: 325433 Color: 1

Bin 658: 905 of cap free
Amount of items: 2
Items: 
Size: 787300 Color: 2
Size: 211796 Color: 1

Bin 659: 906 of cap free
Amount of items: 2
Items: 
Size: 548873 Color: 1
Size: 450222 Color: 3

Bin 660: 906 of cap free
Amount of items: 2
Items: 
Size: 718146 Color: 1
Size: 280949 Color: 3

Bin 661: 920 of cap free
Amount of items: 2
Items: 
Size: 542431 Color: 1
Size: 456650 Color: 3

Bin 662: 926 of cap free
Amount of items: 2
Items: 
Size: 546042 Color: 1
Size: 453033 Color: 3

Bin 663: 928 of cap free
Amount of items: 2
Items: 
Size: 593077 Color: 1
Size: 405996 Color: 4

Bin 664: 929 of cap free
Amount of items: 2
Items: 
Size: 607069 Color: 3
Size: 392003 Color: 1

Bin 665: 936 of cap free
Amount of items: 2
Items: 
Size: 671320 Color: 1
Size: 327745 Color: 2

Bin 666: 939 of cap free
Amount of items: 2
Items: 
Size: 560514 Color: 1
Size: 438548 Color: 2

Bin 667: 940 of cap free
Amount of items: 2
Items: 
Size: 794270 Color: 2
Size: 204791 Color: 4

Bin 668: 949 of cap free
Amount of items: 2
Items: 
Size: 791392 Color: 2
Size: 207660 Color: 0

Bin 669: 959 of cap free
Amount of items: 2
Items: 
Size: 631927 Color: 4
Size: 367115 Color: 0

Bin 670: 969 of cap free
Amount of items: 2
Items: 
Size: 608165 Color: 0
Size: 390867 Color: 3

Bin 671: 969 of cap free
Amount of items: 2
Items: 
Size: 765804 Color: 4
Size: 233228 Color: 0

Bin 672: 971 of cap free
Amount of items: 2
Items: 
Size: 692379 Color: 3
Size: 306651 Color: 1

Bin 673: 984 of cap free
Amount of items: 2
Items: 
Size: 543572 Color: 0
Size: 455445 Color: 4

Bin 674: 993 of cap free
Amount of items: 2
Items: 
Size: 755036 Color: 0
Size: 243972 Color: 1

Bin 675: 995 of cap free
Amount of items: 2
Items: 
Size: 536324 Color: 1
Size: 462682 Color: 2

Bin 676: 1002 of cap free
Amount of items: 2
Items: 
Size: 783910 Color: 4
Size: 215089 Color: 0

Bin 677: 1017 of cap free
Amount of items: 2
Items: 
Size: 755585 Color: 1
Size: 243399 Color: 0

Bin 678: 1018 of cap free
Amount of items: 2
Items: 
Size: 608807 Color: 3
Size: 390176 Color: 4

Bin 679: 1022 of cap free
Amount of items: 2
Items: 
Size: 751863 Color: 2
Size: 247116 Color: 0

Bin 680: 1025 of cap free
Amount of items: 2
Items: 
Size: 629271 Color: 2
Size: 369705 Color: 3

Bin 681: 1032 of cap free
Amount of items: 2
Items: 
Size: 521497 Color: 0
Size: 477472 Color: 2

Bin 682: 1040 of cap free
Amount of items: 2
Items: 
Size: 765737 Color: 4
Size: 233224 Color: 0

Bin 683: 1049 of cap free
Amount of items: 2
Items: 
Size: 726274 Color: 1
Size: 272678 Color: 4

Bin 684: 1061 of cap free
Amount of items: 2
Items: 
Size: 775076 Color: 3
Size: 223864 Color: 2

Bin 685: 1062 of cap free
Amount of items: 2
Items: 
Size: 641509 Color: 3
Size: 357430 Color: 0

Bin 686: 1065 of cap free
Amount of items: 2
Items: 
Size: 557999 Color: 3
Size: 440937 Color: 0

Bin 687: 1075 of cap free
Amount of items: 2
Items: 
Size: 580461 Color: 0
Size: 418465 Color: 2

Bin 688: 1079 of cap free
Amount of items: 2
Items: 
Size: 726258 Color: 2
Size: 272664 Color: 1

Bin 689: 1106 of cap free
Amount of items: 3
Items: 
Size: 385100 Color: 2
Size: 338366 Color: 1
Size: 275429 Color: 1

Bin 690: 1112 of cap free
Amount of items: 2
Items: 
Size: 512191 Color: 0
Size: 486698 Color: 4

Bin 691: 1114 of cap free
Amount of items: 3
Items: 
Size: 645903 Color: 2
Size: 177060 Color: 1
Size: 175924 Color: 4

Bin 692: 1118 of cap free
Amount of items: 3
Items: 
Size: 341103 Color: 4
Size: 328962 Color: 0
Size: 328818 Color: 0

Bin 693: 1133 of cap free
Amount of items: 2
Items: 
Size: 595535 Color: 1
Size: 403333 Color: 0

Bin 694: 1134 of cap free
Amount of items: 2
Items: 
Size: 796177 Color: 4
Size: 202690 Color: 3

Bin 695: 1138 of cap free
Amount of items: 2
Items: 
Size: 718342 Color: 3
Size: 280521 Color: 4

Bin 696: 1144 of cap free
Amount of items: 2
Items: 
Size: 608742 Color: 3
Size: 390115 Color: 1

Bin 697: 1162 of cap free
Amount of items: 2
Items: 
Size: 587648 Color: 1
Size: 411191 Color: 3

Bin 698: 1169 of cap free
Amount of items: 2
Items: 
Size: 698863 Color: 3
Size: 299969 Color: 1

Bin 699: 1174 of cap free
Amount of items: 2
Items: 
Size: 771483 Color: 1
Size: 227344 Color: 2

Bin 700: 1186 of cap free
Amount of items: 2
Items: 
Size: 593066 Color: 3
Size: 405749 Color: 1

Bin 701: 1192 of cap free
Amount of items: 2
Items: 
Size: 501839 Color: 0
Size: 496970 Color: 2

Bin 702: 1224 of cap free
Amount of items: 2
Items: 
Size: 731898 Color: 2
Size: 266879 Color: 1

Bin 703: 1254 of cap free
Amount of items: 2
Items: 
Size: 737113 Color: 4
Size: 261634 Color: 3

Bin 704: 1289 of cap free
Amount of items: 3
Items: 
Size: 740447 Color: 2
Size: 130854 Color: 3
Size: 127411 Color: 2

Bin 705: 1315 of cap free
Amount of items: 2
Items: 
Size: 737082 Color: 4
Size: 261604 Color: 2

Bin 706: 1322 of cap free
Amount of items: 2
Items: 
Size: 574887 Color: 4
Size: 423792 Color: 1

Bin 707: 1325 of cap free
Amount of items: 2
Items: 
Size: 665580 Color: 0
Size: 333096 Color: 3

Bin 708: 1358 of cap free
Amount of items: 2
Items: 
Size: 548703 Color: 0
Size: 449940 Color: 2

Bin 709: 1360 of cap free
Amount of items: 2
Items: 
Size: 559310 Color: 2
Size: 439331 Color: 1

Bin 710: 1373 of cap free
Amount of items: 2
Items: 
Size: 621937 Color: 2
Size: 376691 Color: 3

Bin 711: 1393 of cap free
Amount of items: 2
Items: 
Size: 619786 Color: 3
Size: 378822 Color: 4

Bin 712: 1400 of cap free
Amount of items: 2
Items: 
Size: 595407 Color: 3
Size: 403194 Color: 4

Bin 713: 1401 of cap free
Amount of items: 2
Items: 
Size: 542137 Color: 1
Size: 456463 Color: 0

Bin 714: 1408 of cap free
Amount of items: 2
Items: 
Size: 514152 Color: 1
Size: 484441 Color: 4

Bin 715: 1409 of cap free
Amount of items: 2
Items: 
Size: 517035 Color: 4
Size: 481557 Color: 2

Bin 716: 1411 of cap free
Amount of items: 2
Items: 
Size: 510086 Color: 0
Size: 488504 Color: 1

Bin 717: 1426 of cap free
Amount of items: 2
Items: 
Size: 539303 Color: 1
Size: 459272 Color: 3

Bin 718: 1445 of cap free
Amount of items: 2
Items: 
Size: 522308 Color: 2
Size: 476248 Color: 1

Bin 719: 1447 of cap free
Amount of items: 2
Items: 
Size: 644024 Color: 1
Size: 354530 Color: 4

Bin 720: 1495 of cap free
Amount of items: 2
Items: 
Size: 721586 Color: 1
Size: 276920 Color: 3

Bin 721: 1497 of cap free
Amount of items: 2
Items: 
Size: 778868 Color: 2
Size: 219636 Color: 4

Bin 722: 1509 of cap free
Amount of items: 2
Items: 
Size: 740434 Color: 4
Size: 258058 Color: 3

Bin 723: 1524 of cap free
Amount of items: 3
Items: 
Size: 667925 Color: 3
Size: 165340 Color: 4
Size: 165212 Color: 0

Bin 724: 1540 of cap free
Amount of items: 2
Items: 
Size: 794023 Color: 4
Size: 204438 Color: 1

Bin 725: 1560 of cap free
Amount of items: 2
Items: 
Size: 545422 Color: 4
Size: 453019 Color: 0

Bin 726: 1565 of cap free
Amount of items: 2
Items: 
Size: 568417 Color: 1
Size: 430019 Color: 3

Bin 727: 1567 of cap free
Amount of items: 2
Items: 
Size: 715677 Color: 2
Size: 282757 Color: 4

Bin 728: 1569 of cap free
Amount of items: 2
Items: 
Size: 769415 Color: 2
Size: 229017 Color: 1

Bin 729: 1599 of cap free
Amount of items: 2
Items: 
Size: 570710 Color: 2
Size: 427692 Color: 4

Bin 730: 1602 of cap free
Amount of items: 2
Items: 
Size: 565788 Color: 4
Size: 432611 Color: 2

Bin 731: 1607 of cap free
Amount of items: 2
Items: 
Size: 762064 Color: 2
Size: 236330 Color: 4

Bin 732: 1659 of cap free
Amount of items: 2
Items: 
Size: 592623 Color: 1
Size: 405719 Color: 2

Bin 733: 1661 of cap free
Amount of items: 2
Items: 
Size: 683262 Color: 2
Size: 315078 Color: 1

Bin 734: 1665 of cap free
Amount of items: 2
Items: 
Size: 708711 Color: 4
Size: 289625 Color: 1

Bin 735: 1679 of cap free
Amount of items: 2
Items: 
Size: 509862 Color: 0
Size: 488460 Color: 1

Bin 736: 1686 of cap free
Amount of items: 2
Items: 
Size: 714444 Color: 0
Size: 283871 Color: 2

Bin 737: 1688 of cap free
Amount of items: 2
Items: 
Size: 794019 Color: 2
Size: 204294 Color: 4

Bin 738: 1694 of cap free
Amount of items: 2
Items: 
Size: 762120 Color: 4
Size: 236187 Color: 1

Bin 739: 1730 of cap free
Amount of items: 2
Items: 
Size: 595269 Color: 0
Size: 403002 Color: 1

Bin 740: 1749 of cap free
Amount of items: 2
Items: 
Size: 675649 Color: 1
Size: 322603 Color: 2

Bin 741: 1820 of cap free
Amount of items: 2
Items: 
Size: 629271 Color: 3
Size: 368910 Color: 2

Bin 742: 1852 of cap free
Amount of items: 2
Items: 
Size: 560431 Color: 1
Size: 437718 Color: 2

Bin 743: 1882 of cap free
Amount of items: 2
Items: 
Size: 530976 Color: 0
Size: 467143 Color: 2

Bin 744: 1911 of cap free
Amount of items: 2
Items: 
Size: 513679 Color: 1
Size: 484411 Color: 3

Bin 745: 1918 of cap free
Amount of items: 2
Items: 
Size: 509517 Color: 1
Size: 488566 Color: 0

Bin 746: 1937 of cap free
Amount of items: 2
Items: 
Size: 598126 Color: 1
Size: 399938 Color: 3

Bin 747: 1944 of cap free
Amount of items: 2
Items: 
Size: 629132 Color: 2
Size: 368925 Color: 3

Bin 748: 1958 of cap free
Amount of items: 2
Items: 
Size: 727505 Color: 1
Size: 270538 Color: 3

Bin 749: 1962 of cap free
Amount of items: 2
Items: 
Size: 539340 Color: 3
Size: 458699 Color: 2

Bin 750: 1967 of cap free
Amount of items: 2
Items: 
Size: 764957 Color: 4
Size: 233077 Color: 2

Bin 751: 1977 of cap free
Amount of items: 3
Items: 
Size: 407284 Color: 1
Size: 312195 Color: 4
Size: 278545 Color: 1

Bin 752: 2018 of cap free
Amount of items: 2
Items: 
Size: 764907 Color: 4
Size: 233076 Color: 1

Bin 753: 2076 of cap free
Amount of items: 2
Items: 
Size: 708643 Color: 1
Size: 289282 Color: 0

Bin 754: 2077 of cap free
Amount of items: 2
Items: 
Size: 525242 Color: 2
Size: 472682 Color: 0

Bin 755: 2099 of cap free
Amount of items: 2
Items: 
Size: 767141 Color: 0
Size: 230761 Color: 3

Bin 756: 2105 of cap free
Amount of items: 2
Items: 
Size: 500940 Color: 2
Size: 496956 Color: 0

Bin 757: 2147 of cap free
Amount of items: 2
Items: 
Size: 580792 Color: 2
Size: 417062 Color: 1

Bin 758: 2164 of cap free
Amount of items: 2
Items: 
Size: 796119 Color: 1
Size: 201718 Color: 3

Bin 759: 2200 of cap free
Amount of items: 2
Items: 
Size: 608376 Color: 3
Size: 389425 Color: 1

Bin 760: 2210 of cap free
Amount of items: 2
Items: 
Size: 623785 Color: 3
Size: 374006 Color: 2

Bin 761: 2215 of cap free
Amount of items: 2
Items: 
Size: 727453 Color: 1
Size: 270333 Color: 2

Bin 762: 2266 of cap free
Amount of items: 2
Items: 
Size: 516352 Color: 4
Size: 481383 Color: 2

Bin 763: 2290 of cap free
Amount of items: 2
Items: 
Size: 640543 Color: 2
Size: 357168 Color: 4

Bin 764: 2312 of cap free
Amount of items: 2
Items: 
Size: 557437 Color: 4
Size: 440252 Color: 2

Bin 765: 2333 of cap free
Amount of items: 2
Items: 
Size: 645989 Color: 1
Size: 351679 Color: 4

Bin 766: 2336 of cap free
Amount of items: 2
Items: 
Size: 727347 Color: 1
Size: 270318 Color: 0

Bin 767: 2347 of cap free
Amount of items: 2
Items: 
Size: 534987 Color: 3
Size: 462667 Color: 0

Bin 768: 2428 of cap free
Amount of items: 2
Items: 
Size: 697854 Color: 0
Size: 299719 Color: 4

Bin 769: 2429 of cap free
Amount of items: 2
Items: 
Size: 777967 Color: 1
Size: 219605 Color: 2

Bin 770: 2472 of cap free
Amount of items: 2
Items: 
Size: 655136 Color: 2
Size: 342393 Color: 3

Bin 771: 2489 of cap free
Amount of items: 2
Items: 
Size: 548544 Color: 0
Size: 448968 Color: 2

Bin 772: 2539 of cap free
Amount of items: 2
Items: 
Size: 623749 Color: 3
Size: 373713 Color: 4

Bin 773: 2554 of cap free
Amount of items: 2
Items: 
Size: 793290 Color: 1
Size: 204157 Color: 3

Bin 774: 2563 of cap free
Amount of items: 3
Items: 
Size: 381195 Color: 1
Size: 309913 Color: 0
Size: 306330 Color: 0

Bin 775: 2569 of cap free
Amount of items: 2
Items: 
Size: 684274 Color: 1
Size: 313158 Color: 0

Bin 776: 2626 of cap free
Amount of items: 2
Items: 
Size: 793457 Color: 3
Size: 203918 Color: 2

Bin 777: 2637 of cap free
Amount of items: 2
Items: 
Size: 774125 Color: 0
Size: 223239 Color: 2

Bin 778: 2645 of cap free
Amount of items: 2
Items: 
Size: 640040 Color: 3
Size: 357316 Color: 2

Bin 779: 2673 of cap free
Amount of items: 2
Items: 
Size: 576543 Color: 3
Size: 420785 Color: 2

Bin 780: 2698 of cap free
Amount of items: 2
Items: 
Size: 524646 Color: 3
Size: 472657 Color: 1

Bin 781: 2716 of cap free
Amount of items: 2
Items: 
Size: 777691 Color: 1
Size: 219594 Color: 3

Bin 782: 2729 of cap free
Amount of items: 2
Items: 
Size: 740124 Color: 0
Size: 257148 Color: 4

Bin 783: 2785 of cap free
Amount of items: 2
Items: 
Size: 666938 Color: 0
Size: 330278 Color: 1

Bin 784: 2823 of cap free
Amount of items: 2
Items: 
Size: 509269 Color: 1
Size: 487909 Color: 3

Bin 785: 2840 of cap free
Amount of items: 2
Items: 
Size: 601200 Color: 3
Size: 395961 Color: 1

Bin 786: 2849 of cap free
Amount of items: 2
Items: 
Size: 786823 Color: 0
Size: 210329 Color: 2

Bin 787: 2861 of cap free
Amount of items: 2
Items: 
Size: 697425 Color: 1
Size: 299715 Color: 2

Bin 788: 2949 of cap free
Amount of items: 2
Items: 
Size: 565014 Color: 4
Size: 432038 Color: 0

Bin 789: 2993 of cap free
Amount of items: 2
Items: 
Size: 725531 Color: 2
Size: 271477 Color: 1

Bin 790: 2999 of cap free
Amount of items: 2
Items: 
Size: 538668 Color: 1
Size: 458334 Color: 4

Bin 791: 3009 of cap free
Amount of items: 2
Items: 
Size: 547962 Color: 4
Size: 449030 Color: 0

Bin 792: 3052 of cap free
Amount of items: 2
Items: 
Size: 739786 Color: 2
Size: 257163 Color: 0

Bin 793: 3141 of cap free
Amount of items: 2
Items: 
Size: 597113 Color: 3
Size: 399747 Color: 2

Bin 794: 3206 of cap free
Amount of items: 2
Items: 
Size: 530095 Color: 2
Size: 466700 Color: 3

Bin 795: 3233 of cap free
Amount of items: 2
Items: 
Size: 556935 Color: 1
Size: 439833 Color: 2

Bin 796: 3309 of cap free
Amount of items: 2
Items: 
Size: 739762 Color: 0
Size: 256930 Color: 3

Bin 797: 3315 of cap free
Amount of items: 2
Items: 
Size: 500345 Color: 0
Size: 496341 Color: 3

Bin 798: 3318 of cap free
Amount of items: 2
Items: 
Size: 708608 Color: 1
Size: 288075 Color: 4

Bin 799: 3325 of cap free
Amount of items: 2
Items: 
Size: 683594 Color: 1
Size: 313082 Color: 2

Bin 800: 3348 of cap free
Amount of items: 2
Items: 
Size: 524526 Color: 4
Size: 472127 Color: 1

Bin 801: 3353 of cap free
Amount of items: 2
Items: 
Size: 544489 Color: 4
Size: 452159 Color: 3

Bin 802: 3364 of cap free
Amount of items: 2
Items: 
Size: 580617 Color: 2
Size: 416020 Color: 1

Bin 803: 3401 of cap free
Amount of items: 2
Items: 
Size: 786398 Color: 3
Size: 210202 Color: 2

Bin 804: 3472 of cap free
Amount of items: 2
Items: 
Size: 607051 Color: 0
Size: 389478 Color: 3

Bin 805: 3629 of cap free
Amount of items: 2
Items: 
Size: 665964 Color: 3
Size: 330408 Color: 0

Bin 806: 3667 of cap free
Amount of items: 2
Items: 
Size: 548235 Color: 0
Size: 448099 Color: 4

Bin 807: 3704 of cap free
Amount of items: 2
Items: 
Size: 643452 Color: 0
Size: 352845 Color: 1

Bin 808: 3723 of cap free
Amount of items: 2
Items: 
Size: 567952 Color: 0
Size: 428326 Color: 2

Bin 809: 3734 of cap free
Amount of items: 2
Items: 
Size: 508361 Color: 2
Size: 487906 Color: 1

Bin 810: 3780 of cap free
Amount of items: 2
Items: 
Size: 508321 Color: 4
Size: 487900 Color: 1

Bin 811: 3792 of cap free
Amount of items: 2
Items: 
Size: 596848 Color: 1
Size: 399361 Color: 2

Bin 812: 3896 of cap free
Amount of items: 2
Items: 
Size: 697367 Color: 2
Size: 298738 Color: 4

Bin 813: 3910 of cap free
Amount of items: 2
Items: 
Size: 529008 Color: 4
Size: 467083 Color: 2

Bin 814: 4006 of cap free
Amount of items: 2
Items: 
Size: 773451 Color: 4
Size: 222544 Color: 2

Bin 815: 4028 of cap free
Amount of items: 2
Items: 
Size: 574911 Color: 1
Size: 421062 Color: 3

Bin 816: 4323 of cap free
Amount of items: 2
Items: 
Size: 726086 Color: 1
Size: 269592 Color: 2

Bin 817: 4389 of cap free
Amount of items: 2
Items: 
Size: 556840 Color: 0
Size: 438772 Color: 1

Bin 818: 4596 of cap free
Amount of items: 2
Items: 
Size: 682539 Color: 2
Size: 312866 Color: 4

Bin 819: 4874 of cap free
Amount of items: 2
Items: 
Size: 542412 Color: 0
Size: 452715 Color: 4

Bin 820: 4896 of cap free
Amount of items: 2
Items: 
Size: 508256 Color: 1
Size: 486849 Color: 0

Bin 821: 4932 of cap free
Amount of items: 2
Items: 
Size: 706951 Color: 0
Size: 288118 Color: 1

Bin 822: 4945 of cap free
Amount of items: 2
Items: 
Size: 738658 Color: 4
Size: 256398 Color: 0

Bin 823: 5078 of cap free
Amount of items: 2
Items: 
Size: 664714 Color: 1
Size: 330209 Color: 3

Bin 824: 5115 of cap free
Amount of items: 2
Items: 
Size: 753001 Color: 3
Size: 241885 Color: 1

Bin 825: 5140 of cap free
Amount of items: 2
Items: 
Size: 574272 Color: 3
Size: 420589 Color: 4

Bin 826: 5210 of cap free
Amount of items: 2
Items: 
Size: 682237 Color: 1
Size: 312554 Color: 3

Bin 827: 5242 of cap free
Amount of items: 2
Items: 
Size: 606957 Color: 0
Size: 387802 Color: 2

Bin 828: 5441 of cap free
Amount of items: 3
Items: 
Size: 384197 Color: 0
Size: 305374 Color: 2
Size: 304989 Color: 4

Bin 829: 5674 of cap free
Amount of items: 2
Items: 
Size: 707523 Color: 1
Size: 286804 Color: 3

Bin 830: 5717 of cap free
Amount of items: 2
Items: 
Size: 664140 Color: 2
Size: 330144 Color: 0

Bin 831: 5750 of cap free
Amount of items: 2
Items: 
Size: 606813 Color: 4
Size: 387438 Color: 2

Bin 832: 5858 of cap free
Amount of items: 2
Items: 
Size: 573630 Color: 1
Size: 420513 Color: 0

Bin 833: 5873 of cap free
Amount of items: 2
Items: 
Size: 793155 Color: 1
Size: 200973 Color: 4

Bin 834: 5878 of cap free
Amount of items: 2
Items: 
Size: 507439 Color: 4
Size: 486684 Color: 3

Bin 835: 5888 of cap free
Amount of items: 2
Items: 
Size: 738833 Color: 0
Size: 255280 Color: 3

Bin 836: 6127 of cap free
Amount of items: 2
Items: 
Size: 663838 Color: 3
Size: 330036 Color: 0

Bin 837: 6406 of cap free
Amount of items: 2
Items: 
Size: 663592 Color: 0
Size: 330003 Color: 4

Bin 838: 6436 of cap free
Amount of items: 2
Items: 
Size: 619261 Color: 2
Size: 374304 Color: 3

Bin 839: 6542 of cap free
Amount of items: 2
Items: 
Size: 507127 Color: 1
Size: 486332 Color: 2

Bin 840: 6881 of cap free
Amount of items: 2
Items: 
Size: 594280 Color: 2
Size: 398840 Color: 0

Bin 841: 7152 of cap free
Amount of items: 2
Items: 
Size: 663202 Color: 1
Size: 329647 Color: 2

Bin 842: 7174 of cap free
Amount of items: 2
Items: 
Size: 520863 Color: 0
Size: 471964 Color: 2

Bin 843: 7428 of cap free
Amount of items: 2
Items: 
Size: 705924 Color: 4
Size: 286649 Color: 2

Bin 844: 7464 of cap free
Amount of items: 2
Items: 
Size: 556297 Color: 1
Size: 436240 Color: 4

Bin 845: 7658 of cap free
Amount of items: 2
Items: 
Size: 521030 Color: 2
Size: 471313 Color: 4

Bin 846: 7885 of cap free
Amount of items: 2
Items: 
Size: 520908 Color: 2
Size: 471208 Color: 0

Bin 847: 8006 of cap free
Amount of items: 2
Items: 
Size: 751808 Color: 3
Size: 240187 Color: 4

Bin 848: 8261 of cap free
Amount of items: 2
Items: 
Size: 496036 Color: 1
Size: 495704 Color: 3

Bin 849: 8836 of cap free
Amount of items: 2
Items: 
Size: 519819 Color: 3
Size: 471346 Color: 2

Bin 850: 9160 of cap free
Amount of items: 2
Items: 
Size: 495425 Color: 3
Size: 495416 Color: 2

Bin 851: 9787 of cap free
Amount of items: 2
Items: 
Size: 562446 Color: 4
Size: 427768 Color: 2

Bin 852: 10258 of cap free
Amount of items: 2
Items: 
Size: 495107 Color: 1
Size: 494636 Color: 0

Bin 853: 11234 of cap free
Amount of items: 2
Items: 
Size: 592837 Color: 2
Size: 395930 Color: 1

Bin 854: 12628 of cap free
Amount of items: 2
Items: 
Size: 705123 Color: 4
Size: 282250 Color: 3

Bin 855: 13106 of cap free
Amount of items: 2
Items: 
Size: 704893 Color: 1
Size: 282002 Color: 0

Bin 856: 14558 of cap free
Amount of items: 2
Items: 
Size: 519101 Color: 0
Size: 466342 Color: 3

Bin 857: 14768 of cap free
Amount of items: 2
Items: 
Size: 519374 Color: 3
Size: 465859 Color: 4

Bin 858: 15178 of cap free
Amount of items: 2
Items: 
Size: 518818 Color: 1
Size: 466005 Color: 3

Bin 859: 16193 of cap free
Amount of items: 2
Items: 
Size: 518788 Color: 1
Size: 465020 Color: 4

Bin 860: 19274 of cap free
Amount of items: 2
Items: 
Size: 518734 Color: 1
Size: 461993 Color: 0

Bin 861: 21908 of cap free
Amount of items: 2
Items: 
Size: 516145 Color: 4
Size: 461948 Color: 1

Bin 862: 22829 of cap free
Amount of items: 2
Items: 
Size: 515654 Color: 3
Size: 461518 Color: 1

Bin 863: 36065 of cap free
Amount of items: 2
Items: 
Size: 515612 Color: 4
Size: 448324 Color: 0

Bin 864: 38966 of cap free
Amount of items: 2
Items: 
Size: 514065 Color: 3
Size: 446970 Color: 2

Bin 865: 46734 of cap free
Amount of items: 2
Items: 
Size: 507121 Color: 0
Size: 446146 Color: 2

Bin 866: 47225 of cap free
Amount of items: 2
Items: 
Size: 506845 Color: 0
Size: 445931 Color: 4

Bin 867: 47339 of cap free
Amount of items: 2
Items: 
Size: 506708 Color: 4
Size: 445954 Color: 0

Bin 868: 60375 of cap free
Amount of items: 2
Items: 
Size: 494235 Color: 0
Size: 445391 Color: 1

Bin 869: 237782 of cap free
Amount of items: 2
Items: 
Size: 381145 Color: 0
Size: 381074 Color: 3

Bin 870: 245352 of cap free
Amount of items: 2
Items: 
Size: 381054 Color: 3
Size: 373595 Color: 0

Bin 871: 256584 of cap free
Amount of items: 2
Items: 
Size: 372396 Color: 0
Size: 371021 Color: 4

Bin 872: 259986 of cap free
Amount of items: 2
Items: 
Size: 371297 Color: 0
Size: 368718 Color: 4

Bin 873: 262739 of cap free
Amount of items: 2
Items: 
Size: 368716 Color: 4
Size: 368546 Color: 0

Bin 874: 266762 of cap free
Amount of items: 2
Items: 
Size: 367092 Color: 0
Size: 366147 Color: 2

Bin 875: 267928 of cap free
Amount of items: 2
Items: 
Size: 366203 Color: 0
Size: 365870 Color: 2

Bin 876: 271941 of cap free
Amount of items: 2
Items: 
Size: 365153 Color: 1
Size: 362907 Color: 3

Bin 877: 272212 of cap free
Amount of items: 2
Items: 
Size: 365151 Color: 1
Size: 362638 Color: 3

Bin 878: 273316 of cap free
Amount of items: 2
Items: 
Size: 364507 Color: 1
Size: 362178 Color: 4

Bin 879: 273929 of cap free
Amount of items: 2
Items: 
Size: 364397 Color: 1
Size: 361675 Color: 4

Bin 880: 274975 of cap free
Amount of items: 2
Items: 
Size: 364185 Color: 1
Size: 360841 Color: 2

Bin 881: 277086 of cap free
Amount of items: 2
Items: 
Size: 362299 Color: 1
Size: 360616 Color: 4

Bin 882: 283394 of cap free
Amount of items: 2
Items: 
Size: 362195 Color: 1
Size: 354412 Color: 0

Bin 883: 296624 of cap free
Amount of items: 2
Items: 
Size: 351993 Color: 1
Size: 351384 Color: 4

Bin 884: 298128 of cap free
Amount of items: 2
Items: 
Size: 351027 Color: 1
Size: 350846 Color: 4

Bin 885: 299141 of cap free
Amount of items: 2
Items: 
Size: 350457 Color: 2
Size: 350403 Color: 1

Bin 886: 299426 of cap free
Amount of items: 2
Items: 
Size: 350324 Color: 0
Size: 350251 Color: 2

Bin 887: 304042 of cap free
Amount of items: 2
Items: 
Size: 348412 Color: 2
Size: 347547 Color: 1

Bin 888: 305051 of cap free
Amount of items: 2
Items: 
Size: 348149 Color: 2
Size: 346801 Color: 1

Bin 889: 308211 of cap free
Amount of items: 2
Items: 
Size: 346171 Color: 0
Size: 345619 Color: 2

Bin 890: 309770 of cap free
Amount of items: 2
Items: 
Size: 345160 Color: 4
Size: 345071 Color: 0

Bin 891: 313733 of cap free
Amount of items: 2
Items: 
Size: 344071 Color: 2
Size: 342197 Color: 1

Bin 892: 315721 of cap free
Amount of items: 2
Items: 
Size: 342274 Color: 2
Size: 342006 Color: 0

Bin 893: 318476 of cap free
Amount of items: 2
Items: 
Size: 341099 Color: 3
Size: 340426 Color: 4

Bin 894: 319666 of cap free
Amount of items: 2
Items: 
Size: 340361 Color: 0
Size: 339974 Color: 2

Total size: 885451266
Total free space: 8549628

