Capicity Bin: 1000001
Lower Bound: 43

Bins used: 44
Amount of Colors: 5

Bin 1: 286 of cap free
Amount of items: 3
Items: 
Size: 764251 Color: 2
Size: 129468 Color: 3
Size: 105996 Color: 1

Bin 2: 338 of cap free
Amount of items: 2
Items: 
Size: 740749 Color: 3
Size: 258914 Color: 2

Bin 3: 357 of cap free
Amount of items: 2
Items: 
Size: 610485 Color: 0
Size: 389159 Color: 2

Bin 4: 383 of cap free
Amount of items: 3
Items: 
Size: 595170 Color: 4
Size: 249463 Color: 1
Size: 154985 Color: 0

Bin 5: 606 of cap free
Amount of items: 3
Items: 
Size: 656394 Color: 3
Size: 171927 Color: 4
Size: 171074 Color: 2

Bin 6: 664 of cap free
Amount of items: 3
Items: 
Size: 650858 Color: 4
Size: 183229 Color: 3
Size: 165250 Color: 1

Bin 7: 736 of cap free
Amount of items: 2
Items: 
Size: 615157 Color: 3
Size: 384108 Color: 1

Bin 8: 1006 of cap free
Amount of items: 3
Items: 
Size: 591588 Color: 2
Size: 266989 Color: 4
Size: 140418 Color: 0

Bin 9: 1253 of cap free
Amount of items: 2
Items: 
Size: 751346 Color: 3
Size: 247402 Color: 1

Bin 10: 1757 of cap free
Amount of items: 3
Items: 
Size: 729941 Color: 1
Size: 167604 Color: 3
Size: 100699 Color: 4

Bin 11: 1763 of cap free
Amount of items: 2
Items: 
Size: 709936 Color: 1
Size: 288302 Color: 2

Bin 12: 1791 of cap free
Amount of items: 3
Items: 
Size: 641027 Color: 3
Size: 256535 Color: 0
Size: 100648 Color: 0

Bin 13: 2120 of cap free
Amount of items: 2
Items: 
Size: 569972 Color: 2
Size: 427909 Color: 4

Bin 14: 2662 of cap free
Amount of items: 2
Items: 
Size: 788908 Color: 3
Size: 208431 Color: 0

Bin 15: 3059 of cap free
Amount of items: 2
Items: 
Size: 759236 Color: 2
Size: 237706 Color: 3

Bin 16: 3360 of cap free
Amount of items: 3
Items: 
Size: 572973 Color: 0
Size: 268047 Color: 1
Size: 155621 Color: 3

Bin 17: 4136 of cap free
Amount of items: 3
Items: 
Size: 638825 Color: 3
Size: 214851 Color: 0
Size: 142189 Color: 2

Bin 18: 4541 of cap free
Amount of items: 2
Items: 
Size: 513410 Color: 4
Size: 482050 Color: 1

Bin 19: 4746 of cap free
Amount of items: 2
Items: 
Size: 695856 Color: 1
Size: 299399 Color: 3

Bin 20: 5335 of cap free
Amount of items: 3
Items: 
Size: 622406 Color: 0
Size: 190131 Color: 1
Size: 182129 Color: 2

Bin 21: 6187 of cap free
Amount of items: 2
Items: 
Size: 787021 Color: 1
Size: 206793 Color: 2

Bin 22: 6223 of cap free
Amount of items: 3
Items: 
Size: 653613 Color: 1
Size: 172894 Color: 0
Size: 167271 Color: 3

Bin 23: 7983 of cap free
Amount of items: 2
Items: 
Size: 597173 Color: 3
Size: 394845 Color: 1

Bin 24: 8166 of cap free
Amount of items: 2
Items: 
Size: 776140 Color: 4
Size: 215695 Color: 3

Bin 25: 8678 of cap free
Amount of items: 2
Items: 
Size: 734676 Color: 1
Size: 256647 Color: 0

Bin 26: 9085 of cap free
Amount of items: 2
Items: 
Size: 576460 Color: 0
Size: 414456 Color: 3

Bin 27: 9376 of cap free
Amount of items: 2
Items: 
Size: 564623 Color: 2
Size: 426002 Color: 0

Bin 28: 10156 of cap free
Amount of items: 2
Items: 
Size: 496019 Color: 3
Size: 493826 Color: 1

Bin 29: 11313 of cap free
Amount of items: 2
Items: 
Size: 633572 Color: 4
Size: 355116 Color: 0

Bin 30: 11708 of cap free
Amount of items: 2
Items: 
Size: 763222 Color: 4
Size: 225071 Color: 3

Bin 31: 11730 of cap free
Amount of items: 2
Items: 
Size: 507674 Color: 1
Size: 480597 Color: 2

Bin 32: 21848 of cap free
Amount of items: 2
Items: 
Size: 506714 Color: 0
Size: 471439 Color: 1

Bin 33: 29586 of cap free
Amount of items: 3
Items: 
Size: 560009 Color: 0
Size: 214306 Color: 1
Size: 196100 Color: 3

Bin 34: 37029 of cap free
Amount of items: 2
Items: 
Size: 559853 Color: 3
Size: 403119 Color: 1

Bin 35: 48598 of cap free
Amount of items: 2
Items: 
Size: 557059 Color: 2
Size: 394344 Color: 3

Bin 36: 69289 of cap free
Amount of items: 2
Items: 
Size: 543668 Color: 4
Size: 387044 Color: 2

Bin 37: 92867 of cap free
Amount of items: 3
Items: 
Size: 305884 Color: 2
Size: 302020 Color: 1
Size: 299230 Color: 1

Bin 38: 120946 of cap free
Amount of items: 2
Items: 
Size: 542318 Color: 2
Size: 336737 Color: 3

Bin 39: 121595 of cap free
Amount of items: 2
Items: 
Size: 542836 Color: 3
Size: 335570 Color: 1

Bin 40: 139098 of cap free
Amount of items: 2
Items: 
Size: 534684 Color: 0
Size: 326219 Color: 2

Bin 41: 146115 of cap free
Amount of items: 2
Items: 
Size: 533057 Color: 2
Size: 320829 Color: 1

Bin 42: 151328 of cap free
Amount of items: 2
Items: 
Size: 529778 Color: 3
Size: 318895 Color: 2

Bin 43: 154790 of cap free
Amount of items: 2
Items: 
Size: 532094 Color: 2
Size: 313117 Color: 1

Bin 44: 180007 of cap free
Amount of items: 3
Items: 
Size: 282922 Color: 1
Size: 281292 Color: 3
Size: 255780 Color: 3

Total size: 42545444
Total free space: 1454600

