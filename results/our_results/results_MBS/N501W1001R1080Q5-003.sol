Capicity Bin: 1001
Lower Bound: 226

Bins used: 227
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1
Size: 335 Color: 4
Size: 319 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 328 Color: 2
Size: 313 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 4
Size: 342 Color: 2
Size: 298 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 322 Color: 2
Size: 315 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 331 Color: 0
Size: 301 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 331 Color: 1
Size: 298 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 328 Color: 4
Size: 299 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 3
Size: 269 Color: 0
Size: 266 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 262 Color: 2
Size: 240 Color: 4

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 491 Color: 2

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 1

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 2
Size: 486 Color: 4

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 0
Size: 480 Color: 1

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 477 Color: 3

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 3
Size: 476 Color: 4

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 4
Size: 468 Color: 2

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 4

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 464 Color: 1

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 3
Size: 461 Color: 2

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 3

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 1

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 4
Size: 451 Color: 1

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 4
Size: 441 Color: 3

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 1
Size: 440 Color: 3

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 3
Size: 439 Color: 4

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 435 Color: 4

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 431 Color: 2

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 1

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 408 Color: 3

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 1
Size: 404 Color: 2

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 1
Size: 396 Color: 2

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 391 Color: 0

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 4
Size: 389 Color: 0

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 384 Color: 2

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 382 Color: 0

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 378 Color: 3

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 1

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 2
Size: 185 Color: 3
Size: 178 Color: 0

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 4
Size: 363 Color: 1

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 0
Size: 357 Color: 3

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 2
Size: 356 Color: 4

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 2
Size: 354 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 0
Size: 173 Color: 4
Size: 172 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 4
Size: 184 Color: 2
Size: 159 Color: 0

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 1

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 662 Color: 2
Size: 184 Color: 3
Size: 155 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 0
Size: 181 Color: 1
Size: 155 Color: 3

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 2
Size: 336 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 4

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 4
Size: 327 Color: 3

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 3

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 325 Color: 4

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 2
Size: 323 Color: 1

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 4
Size: 323 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 680 Color: 2
Size: 169 Color: 1
Size: 152 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 1
Size: 160 Color: 3
Size: 160 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 0
Size: 161 Color: 4
Size: 157 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 1
Size: 167 Color: 0
Size: 151 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 1
Size: 169 Color: 0
Size: 149 Color: 3

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 317 Color: 0

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 0
Size: 315 Color: 1

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 1
Size: 315 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 2
Size: 162 Color: 4
Size: 152 Color: 3

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 4
Size: 312 Color: 3

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 308 Color: 0

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 308 Color: 3

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 306 Color: 4

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 4
Size: 306 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 154 Color: 4
Size: 149 Color: 3

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 2
Size: 303 Color: 4

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 4
Size: 295 Color: 1

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 716 Color: 2
Size: 143 Color: 4
Size: 142 Color: 2

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 4
Size: 271 Color: 0

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 2

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 4
Size: 261 Color: 0

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 258 Color: 3

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 2

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 3
Size: 254 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 2
Size: 128 Color: 3
Size: 124 Color: 3

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 251 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 2
Size: 125 Color: 3
Size: 125 Color: 3

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 249 Color: 3

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 0

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 3
Size: 248 Color: 1

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 245 Color: 1

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 243 Color: 3

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 4
Size: 238 Color: 1

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 237 Color: 0

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 237 Color: 4

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 2
Size: 236 Color: 3

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 2
Size: 235 Color: 0

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 2
Size: 233 Color: 1

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 1

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 2
Size: 229 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 3
Size: 111 Color: 1
Size: 110 Color: 1

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 3
Size: 221 Color: 2

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 4

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 3
Size: 216 Color: 4

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 4
Size: 109 Color: 1
Size: 107 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 1
Size: 114 Color: 3
Size: 101 Color: 0

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 3

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 0
Size: 213 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 1
Size: 110 Color: 3
Size: 103 Color: 1

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 211 Color: 2

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 2
Size: 209 Color: 4

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 208 Color: 4

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 2
Size: 207 Color: 1

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 3

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 2

Bin 114: 1 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 3
Size: 348 Color: 1
Size: 304 Color: 0

Bin 115: 1 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 332 Color: 2
Size: 308 Color: 1

Bin 116: 1 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 499 Color: 3

Bin 117: 1 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 495 Color: 1

Bin 118: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 488 Color: 2

Bin 119: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 0
Size: 483 Color: 3

Bin 120: 1 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 4
Size: 473 Color: 1

Bin 121: 1 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 469 Color: 0

Bin 122: 1 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 2
Size: 454 Color: 0

Bin 123: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 453 Color: 4

Bin 124: 1 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 445 Color: 1

Bin 125: 1 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 4
Size: 426 Color: 3

Bin 126: 1 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 420 Color: 0

Bin 127: 1 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 1
Size: 223 Color: 4
Size: 175 Color: 3

Bin 128: 1 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 0
Size: 207 Color: 2
Size: 182 Color: 0

Bin 129: 1 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 4
Size: 372 Color: 0

Bin 130: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 367 Color: 3

Bin 131: 1 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 2
Size: 192 Color: 3
Size: 173 Color: 3

Bin 132: 1 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 362 Color: 1

Bin 133: 1 of cap free
Amount of items: 3
Items: 
Size: 644 Color: 0
Size: 194 Color: 4
Size: 162 Color: 1

Bin 134: 1 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 355 Color: 0

Bin 135: 1 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 342 Color: 4

Bin 136: 1 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 337 Color: 4

Bin 137: 1 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 296 Color: 1

Bin 138: 1 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 292 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 712 Color: 3
Size: 146 Color: 1
Size: 142 Color: 1

Bin 140: 1 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 282 Color: 0

Bin 141: 1 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 282 Color: 0

Bin 142: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 4
Size: 276 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 2
Size: 149 Color: 4
Size: 124 Color: 2

Bin 144: 1 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 3
Size: 267 Color: 0

Bin 145: 1 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 258 Color: 1

Bin 146: 1 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 2
Size: 253 Color: 3

Bin 147: 1 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 242 Color: 4

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 3
Size: 122 Color: 0
Size: 115 Color: 4

Bin 149: 1 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 1
Size: 235 Color: 2

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 1
Size: 110 Color: 0
Size: 108 Color: 1

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 800 Color: 0
Size: 100 Color: 2
Size: 100 Color: 1

Bin 152: 2 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 480 Color: 0

Bin 153: 2 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 468 Color: 1

Bin 154: 2 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 4
Size: 431 Color: 1

Bin 155: 2 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 4
Size: 401 Color: 3

Bin 156: 2 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 392 Color: 4

Bin 157: 2 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 386 Color: 3

Bin 158: 2 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 374 Color: 2

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 0
Size: 195 Color: 4
Size: 155 Color: 0

Bin 160: 2 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 323 Color: 0

Bin 161: 2 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 287 Color: 3

Bin 162: 2 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 4
Size: 249 Color: 1

Bin 163: 2 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 228 Color: 3

Bin 164: 2 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 3
Size: 224 Color: 4

Bin 165: 3 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 472 Color: 3

Bin 166: 3 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 4
Size: 450 Color: 3

Bin 167: 3 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 443 Color: 2

Bin 168: 3 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 2
Size: 436 Color: 1

Bin 169: 3 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 422 Color: 0

Bin 170: 3 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 2
Size: 401 Color: 4

Bin 171: 3 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 383 Color: 2

Bin 172: 3 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 1
Size: 214 Color: 4

Bin 173: 3 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 0
Size: 198 Color: 3

Bin 174: 4 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 0
Size: 330 Color: 1
Size: 322 Color: 0

Bin 175: 4 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 322 Color: 4
Size: 269 Color: 0

Bin 176: 4 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 0
Size: 276 Color: 4

Bin 177: 4 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 257 Color: 4

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 4
Size: 131 Color: 3
Size: 125 Color: 0

Bin 179: 4 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 241 Color: 1

Bin 180: 4 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 227 Color: 3

Bin 181: 5 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 270 Color: 3
Size: 228 Color: 0

Bin 182: 5 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 0
Size: 441 Color: 1

Bin 183: 5 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 441 Color: 1

Bin 184: 5 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 392 Color: 4

Bin 185: 5 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 4
Size: 388 Color: 0

Bin 186: 5 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 234 Color: 0

Bin 187: 5 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 212 Color: 1

Bin 188: 5 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 198 Color: 2

Bin 189: 6 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 409 Color: 2

Bin 190: 6 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 0
Size: 373 Color: 4

Bin 191: 6 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 197 Color: 3

Bin 192: 7 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 286 Color: 3

Bin 193: 8 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 3
Size: 419 Color: 1

Bin 194: 8 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 0
Size: 272 Color: 1

Bin 195: 8 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 272 Color: 2

Bin 196: 8 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 1
Size: 209 Color: 3

Bin 197: 10 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 3
Size: 328 Color: 0
Size: 319 Color: 3

Bin 198: 10 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 430 Color: 1

Bin 199: 10 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 224 Color: 3

Bin 200: 11 of cap free
Amount of items: 2
Items: 
Size: 497 Color: 1
Size: 493 Color: 3

Bin 201: 11 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 430 Color: 3

Bin 202: 11 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 417 Color: 4

Bin 203: 12 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 430 Color: 0

Bin 204: 12 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 2
Size: 417 Color: 3

Bin 205: 13 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 3
Size: 207 Color: 2

Bin 206: 14 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 2
Size: 196 Color: 0
Size: 187 Color: 1

Bin 207: 15 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 0
Size: 224 Color: 3

Bin 208: 17 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 3
Size: 269 Color: 4

Bin 209: 17 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 2
Size: 247 Color: 3

Bin 210: 18 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 2
Size: 204 Color: 1

Bin 211: 20 of cap free
Amount of items: 2
Items: 
Size: 493 Color: 3
Size: 488 Color: 1

Bin 212: 22 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 0
Size: 319 Color: 3
Size: 316 Color: 4

Bin 213: 22 of cap free
Amount of items: 2
Items: 
Size: 492 Color: 3
Size: 487 Color: 0

Bin 214: 22 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 4
Size: 377 Color: 0

Bin 215: 25 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 2
Size: 197 Color: 0

Bin 216: 26 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 381 Color: 4

Bin 217: 30 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 1
Size: 430 Color: 0

Bin 218: 36 of cap free
Amount of items: 2
Items: 
Size: 486 Color: 0
Size: 479 Color: 4

Bin 219: 37 of cap free
Amount of items: 2
Items: 
Size: 486 Color: 0
Size: 478 Color: 1

Bin 220: 43 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 416 Color: 1

Bin 221: 46 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 416 Color: 1

Bin 222: 51 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 0
Size: 471 Color: 1

Bin 223: 53 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 409 Color: 2

Bin 224: 55 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 0
Size: 408 Color: 2

Bin 225: 58 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 3
Size: 408 Color: 0

Bin 226: 59 of cap free
Amount of items: 2
Items: 
Size: 475 Color: 0
Size: 467 Color: 4

Bin 227: 401 of cap free
Amount of items: 2
Items: 
Size: 301 Color: 1
Size: 299 Color: 2

Total size: 225824
Total free space: 1403

