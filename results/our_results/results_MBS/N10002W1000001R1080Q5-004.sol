Capicity Bin: 1000001
Lower Bound: 4508

Bins used: 5667
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 410307 Color: 2
Size: 197190 Color: 3
Size: 196806 Color: 1
Size: 195698 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 410665 Color: 2
Size: 198170 Color: 3
Size: 196059 Color: 3
Size: 195107 Color: 1

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 410932 Color: 1
Size: 197767 Color: 3
Size: 195679 Color: 0
Size: 195623 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 411023 Color: 1
Size: 198495 Color: 3
Size: 195377 Color: 1
Size: 195106 Color: 3

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 412664 Color: 1
Size: 199956 Color: 3
Size: 194243 Color: 3
Size: 193138 Color: 2

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 414056 Color: 0
Size: 202265 Color: 3
Size: 191915 Color: 1
Size: 191765 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 414706 Color: 0
Size: 204484 Color: 4
Size: 191217 Color: 2
Size: 189594 Color: 3

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 415669 Color: 0
Size: 207924 Color: 3
Size: 188467 Color: 2
Size: 187941 Color: 1

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 415728 Color: 1
Size: 208084 Color: 3
Size: 188176 Color: 4
Size: 188013 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 420300 Color: 2
Size: 395404 Color: 4
Size: 184297 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 423130 Color: 3
Size: 394484 Color: 0
Size: 182387 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 426214 Color: 0
Size: 393111 Color: 3
Size: 180676 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 440517 Color: 1
Size: 385501 Color: 0
Size: 173983 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 441273 Color: 1
Size: 388639 Color: 4
Size: 170089 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 441332 Color: 1
Size: 281593 Color: 4
Size: 277076 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 441703 Color: 4
Size: 389286 Color: 2
Size: 169012 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 442739 Color: 1
Size: 281330 Color: 4
Size: 275932 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 443139 Color: 4
Size: 386604 Color: 1
Size: 170258 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 443381 Color: 3
Size: 379836 Color: 2
Size: 176784 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 443654 Color: 2
Size: 388471 Color: 4
Size: 167876 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 443763 Color: 0
Size: 384658 Color: 1
Size: 171580 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 443788 Color: 1
Size: 385766 Color: 3
Size: 170447 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 443951 Color: 3
Size: 279156 Color: 1
Size: 276894 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 444185 Color: 1
Size: 281389 Color: 3
Size: 274427 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 444603 Color: 1
Size: 281080 Color: 2
Size: 274318 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 445094 Color: 0
Size: 385332 Color: 2
Size: 169575 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 446495 Color: 0
Size: 280601 Color: 4
Size: 272905 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 446718 Color: 4
Size: 276937 Color: 3
Size: 276346 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 446719 Color: 3
Size: 387520 Color: 2
Size: 165762 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 447226 Color: 4
Size: 279719 Color: 1
Size: 273056 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447344 Color: 2
Size: 278245 Color: 0
Size: 274412 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 447383 Color: 3
Size: 282849 Color: 0
Size: 269769 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 447519 Color: 1
Size: 384624 Color: 2
Size: 167858 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 447591 Color: 0
Size: 382769 Color: 1
Size: 169641 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 447663 Color: 1
Size: 382938 Color: 0
Size: 169400 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 447720 Color: 3
Size: 277510 Color: 1
Size: 274771 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 447967 Color: 2
Size: 386304 Color: 4
Size: 165730 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 448020 Color: 3
Size: 279204 Color: 1
Size: 272777 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 448134 Color: 4
Size: 280461 Color: 3
Size: 271406 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 448753 Color: 3
Size: 277717 Color: 0
Size: 273531 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 448931 Color: 1
Size: 384958 Color: 3
Size: 166112 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 449263 Color: 0
Size: 382364 Color: 4
Size: 168374 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 449282 Color: 1
Size: 282920 Color: 4
Size: 267799 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 449614 Color: 4
Size: 381241 Color: 0
Size: 169146 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 449683 Color: 2
Size: 379770 Color: 0
Size: 170548 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 450022 Color: 1
Size: 277932 Color: 0
Size: 272047 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 450125 Color: 0
Size: 385260 Color: 4
Size: 164616 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 450232 Color: 4
Size: 282537 Color: 1
Size: 267232 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 450353 Color: 1
Size: 279806 Color: 0
Size: 269842 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 450384 Color: 3
Size: 279406 Color: 2
Size: 270211 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 450515 Color: 1
Size: 383860 Color: 2
Size: 165626 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 450642 Color: 1
Size: 281757 Color: 0
Size: 267602 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 450743 Color: 0
Size: 277750 Color: 2
Size: 271508 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 451038 Color: 3
Size: 284216 Color: 4
Size: 264747 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 451160 Color: 2
Size: 282482 Color: 0
Size: 266359 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 451329 Color: 0
Size: 382462 Color: 2
Size: 166210 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 451378 Color: 2
Size: 278183 Color: 4
Size: 270440 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 451416 Color: 4
Size: 279789 Color: 2
Size: 268796 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 451489 Color: 4
Size: 281751 Color: 0
Size: 266761 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 451560 Color: 2
Size: 383809 Color: 1
Size: 164632 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 451583 Color: 2
Size: 385268 Color: 3
Size: 163150 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 451785 Color: 0
Size: 382784 Color: 4
Size: 165432 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 451888 Color: 1
Size: 383100 Color: 2
Size: 165013 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 451917 Color: 3
Size: 381965 Color: 0
Size: 166119 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 452113 Color: 1
Size: 381438 Color: 0
Size: 166450 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 452215 Color: 3
Size: 281663 Color: 4
Size: 266123 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 452278 Color: 0
Size: 380604 Color: 4
Size: 167119 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 452406 Color: 3
Size: 285508 Color: 0
Size: 262087 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 452650 Color: 4
Size: 280469 Color: 0
Size: 266882 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 452702 Color: 3
Size: 282138 Color: 0
Size: 265161 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 452708 Color: 4
Size: 278122 Color: 2
Size: 269171 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 452895 Color: 2
Size: 279558 Color: 4
Size: 267548 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 452915 Color: 2
Size: 283310 Color: 3
Size: 263776 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 453158 Color: 0
Size: 278821 Color: 3
Size: 268022 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 453262 Color: 0
Size: 278830 Color: 3
Size: 267909 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 453437 Color: 2
Size: 382058 Color: 3
Size: 164506 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 453645 Color: 1
Size: 282542 Color: 2
Size: 263814 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 453843 Color: 2
Size: 282298 Color: 4
Size: 263860 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 453867 Color: 4
Size: 280530 Color: 1
Size: 265604 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 453871 Color: 0
Size: 281362 Color: 2
Size: 264768 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 453908 Color: 1
Size: 382323 Color: 3
Size: 163770 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 454089 Color: 2
Size: 281805 Color: 4
Size: 264107 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 454097 Color: 3
Size: 381172 Color: 4
Size: 164732 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 454103 Color: 0
Size: 282132 Color: 4
Size: 263766 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 454122 Color: 1
Size: 381137 Color: 4
Size: 164742 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 454138 Color: 2
Size: 286793 Color: 0
Size: 259070 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 454224 Color: 2
Size: 386993 Color: 4
Size: 158784 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 454263 Color: 4
Size: 380748 Color: 1
Size: 164990 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 454353 Color: 0
Size: 283901 Color: 1
Size: 261747 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 454439 Color: 3
Size: 284306 Color: 0
Size: 261256 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 454825 Color: 4
Size: 283829 Color: 2
Size: 261347 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 455160 Color: 1
Size: 284516 Color: 4
Size: 260325 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 455211 Color: 2
Size: 283997 Color: 0
Size: 260793 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 455457 Color: 2
Size: 284955 Color: 4
Size: 259589 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 455520 Color: 3
Size: 380159 Color: 0
Size: 164322 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 455899 Color: 2
Size: 292034 Color: 1
Size: 252068 Color: 4

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 455930 Color: 4
Size: 283890 Color: 3
Size: 260181 Color: 4

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 456104 Color: 4
Size: 294050 Color: 1
Size: 249847 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 456114 Color: 3
Size: 380125 Color: 4
Size: 163762 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 456293 Color: 0
Size: 377969 Color: 3
Size: 165739 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 456343 Color: 2
Size: 284033 Color: 1
Size: 259625 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 456374 Color: 0
Size: 286315 Color: 3
Size: 257312 Color: 4

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 456476 Color: 3
Size: 288215 Color: 4
Size: 255310 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 456602 Color: 4
Size: 285117 Color: 3
Size: 258282 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 456818 Color: 2
Size: 285125 Color: 3
Size: 258058 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 456948 Color: 0
Size: 379010 Color: 1
Size: 164043 Color: 2

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 457089 Color: 3
Size: 283032 Color: 4
Size: 259880 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 457154 Color: 3
Size: 288483 Color: 1
Size: 254364 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 457903 Color: 1
Size: 376046 Color: 4
Size: 166052 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 458847 Color: 2
Size: 376559 Color: 1
Size: 164595 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 459089 Color: 3
Size: 298077 Color: 0
Size: 242835 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 459399 Color: 0
Size: 379200 Color: 4
Size: 161402 Color: 2

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 459480 Color: 4
Size: 286783 Color: 1
Size: 253738 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 459713 Color: 4
Size: 379948 Color: 0
Size: 160340 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 459744 Color: 1
Size: 292571 Color: 4
Size: 247686 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 459939 Color: 4
Size: 287749 Color: 0
Size: 252313 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 460204 Color: 4
Size: 291583 Color: 2
Size: 248214 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 460610 Color: 4
Size: 376851 Color: 3
Size: 162540 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 460615 Color: 0
Size: 292990 Color: 4
Size: 246396 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 460737 Color: 2
Size: 291517 Color: 1
Size: 247747 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 461120 Color: 4
Size: 292534 Color: 1
Size: 246347 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 461160 Color: 0
Size: 375491 Color: 4
Size: 163350 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 461203 Color: 1
Size: 291092 Color: 4
Size: 247706 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 461230 Color: 1
Size: 379594 Color: 4
Size: 159177 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 461236 Color: 0
Size: 290715 Color: 4
Size: 248050 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 461363 Color: 3
Size: 379226 Color: 1
Size: 159412 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 461443 Color: 4
Size: 284850 Color: 3
Size: 253708 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 461606 Color: 3
Size: 379444 Color: 1
Size: 158951 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 461677 Color: 3
Size: 287963 Color: 4
Size: 250361 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 461708 Color: 0
Size: 289690 Color: 2
Size: 248603 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 461997 Color: 3
Size: 375834 Color: 1
Size: 162170 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 462537 Color: 4
Size: 284799 Color: 0
Size: 252665 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 462690 Color: 0
Size: 376478 Color: 3
Size: 160833 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 462764 Color: 0
Size: 373809 Color: 4
Size: 163428 Color: 4

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 462883 Color: 0
Size: 377657 Color: 1
Size: 159461 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 463003 Color: 2
Size: 371813 Color: 0
Size: 165185 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 463041 Color: 2
Size: 288594 Color: 0
Size: 248366 Color: 3

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 463450 Color: 4
Size: 379348 Color: 0
Size: 157203 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 463558 Color: 1
Size: 287205 Color: 0
Size: 249238 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 463830 Color: 0
Size: 292114 Color: 4
Size: 244057 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 465308 Color: 2
Size: 289109 Color: 0
Size: 245584 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 465439 Color: 4
Size: 372700 Color: 0
Size: 161862 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 465442 Color: 4
Size: 378311 Color: 2
Size: 156248 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 465509 Color: 0
Size: 287645 Color: 4
Size: 246847 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 465638 Color: 3
Size: 375708 Color: 0
Size: 158655 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 465654 Color: 2
Size: 377491 Color: 3
Size: 156856 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 465857 Color: 2
Size: 287496 Color: 3
Size: 246648 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 466265 Color: 3
Size: 289154 Color: 2
Size: 244582 Color: 2

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 466311 Color: 1
Size: 377918 Color: 2
Size: 155772 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 466411 Color: 3
Size: 291403 Color: 1
Size: 242187 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 466577 Color: 1
Size: 292079 Color: 0
Size: 241345 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 466832 Color: 0
Size: 373558 Color: 1
Size: 159611 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 466846 Color: 0
Size: 291995 Color: 1
Size: 241160 Color: 3

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 466880 Color: 3
Size: 289002 Color: 1
Size: 244119 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 466962 Color: 1
Size: 376734 Color: 4
Size: 156305 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 467030 Color: 4
Size: 287229 Color: 2
Size: 245742 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 467075 Color: 3
Size: 287019 Color: 4
Size: 245907 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 467306 Color: 2
Size: 374289 Color: 3
Size: 158406 Color: 2

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 467348 Color: 1
Size: 299039 Color: 3
Size: 233614 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 467393 Color: 0
Size: 377554 Color: 4
Size: 155054 Color: 4

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 467558 Color: 4
Size: 285728 Color: 3
Size: 246715 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 467719 Color: 1
Size: 288481 Color: 4
Size: 243801 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 467867 Color: 1
Size: 287306 Color: 2
Size: 244828 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 468091 Color: 0
Size: 377337 Color: 1
Size: 154573 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 468308 Color: 3
Size: 287769 Color: 1
Size: 243924 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 468433 Color: 4
Size: 290690 Color: 0
Size: 240878 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 468551 Color: 0
Size: 292760 Color: 1
Size: 238690 Color: 3

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 468751 Color: 2
Size: 293561 Color: 0
Size: 237689 Color: 4

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 468811 Color: 1
Size: 293386 Color: 4
Size: 237804 Color: 3

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 468835 Color: 1
Size: 294628 Color: 3
Size: 236538 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 468983 Color: 4
Size: 296473 Color: 3
Size: 234545 Color: 3

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 469022 Color: 1
Size: 287296 Color: 0
Size: 243683 Color: 3

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 469096 Color: 4
Size: 297702 Color: 1
Size: 233203 Color: 3

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 469143 Color: 3
Size: 310186 Color: 0
Size: 220672 Color: 4

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 469145 Color: 3
Size: 290456 Color: 0
Size: 240400 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 469390 Color: 2
Size: 291821 Color: 1
Size: 238790 Color: 2

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 469451 Color: 3
Size: 288887 Color: 2
Size: 241663 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 469499 Color: 0
Size: 289504 Color: 4
Size: 240998 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 469508 Color: 1
Size: 377538 Color: 4
Size: 152955 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 469515 Color: 1
Size: 287435 Color: 0
Size: 243051 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 469683 Color: 2
Size: 294879 Color: 1
Size: 235439 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 469893 Color: 1
Size: 374719 Color: 2
Size: 155389 Color: 2

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 470092 Color: 4
Size: 304727 Color: 0
Size: 225182 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 470130 Color: 4
Size: 372994 Color: 0
Size: 156877 Color: 3

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 470274 Color: 0
Size: 374703 Color: 4
Size: 155024 Color: 2

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 470749 Color: 3
Size: 292455 Color: 1
Size: 236797 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 470833 Color: 2
Size: 300816 Color: 0
Size: 228352 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 470839 Color: 2
Size: 367272 Color: 4
Size: 161890 Color: 3

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 470901 Color: 1
Size: 299067 Color: 0
Size: 230033 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 471308 Color: 1
Size: 289545 Color: 3
Size: 239148 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 471375 Color: 4
Size: 298228 Color: 0
Size: 230398 Color: 4

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 471785 Color: 3
Size: 292462 Color: 0
Size: 235754 Color: 2

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 471797 Color: 0
Size: 296869 Color: 2
Size: 231335 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 471992 Color: 2
Size: 294154 Color: 1
Size: 233855 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 472207 Color: 3
Size: 293207 Color: 2
Size: 234587 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 472240 Color: 0
Size: 378027 Color: 2
Size: 149734 Color: 3

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 472365 Color: 4
Size: 296876 Color: 2
Size: 230760 Color: 3

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 472400 Color: 4
Size: 374787 Color: 0
Size: 152814 Color: 3

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 472461 Color: 3
Size: 292605 Color: 2
Size: 234935 Color: 2

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 472508 Color: 1
Size: 374362 Color: 0
Size: 153131 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 472595 Color: 4
Size: 291999 Color: 2
Size: 235407 Color: 4

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 472716 Color: 1
Size: 375173 Color: 2
Size: 152112 Color: 2

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 472953 Color: 3
Size: 292845 Color: 4
Size: 234203 Color: 3

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 473033 Color: 3
Size: 293064 Color: 1
Size: 233904 Color: 3

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 473269 Color: 2
Size: 294870 Color: 4
Size: 231862 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 473329 Color: 3
Size: 298339 Color: 2
Size: 228333 Color: 3

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 473346 Color: 1
Size: 291729 Color: 0
Size: 234926 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 473458 Color: 4
Size: 373006 Color: 1
Size: 153537 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 473503 Color: 1
Size: 368446 Color: 0
Size: 158052 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 473548 Color: 0
Size: 299462 Color: 2
Size: 226991 Color: 2

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 473883 Color: 4
Size: 294891 Color: 0
Size: 231227 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 473900 Color: 3
Size: 292671 Color: 4
Size: 233430 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 473954 Color: 2
Size: 370430 Color: 0
Size: 155617 Color: 3

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 474035 Color: 2
Size: 294957 Color: 4
Size: 231009 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 474055 Color: 4
Size: 368631 Color: 2
Size: 157315 Color: 3

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 474086 Color: 3
Size: 376891 Color: 0
Size: 149024 Color: 3

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 474148 Color: 1
Size: 291013 Color: 3
Size: 234840 Color: 4

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 474172 Color: 2
Size: 295124 Color: 1
Size: 230705 Color: 4

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 474263 Color: 4
Size: 291421 Color: 0
Size: 234317 Color: 2

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 474367 Color: 4
Size: 369679 Color: 0
Size: 155955 Color: 2

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 474544 Color: 3
Size: 304978 Color: 4
Size: 220479 Color: 2

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 474551 Color: 2
Size: 294018 Color: 0
Size: 231432 Color: 3

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 474759 Color: 0
Size: 293047 Color: 2
Size: 232195 Color: 4

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 474814 Color: 0
Size: 299231 Color: 1
Size: 225956 Color: 2

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 474959 Color: 1
Size: 366973 Color: 4
Size: 158069 Color: 2

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 475174 Color: 0
Size: 377164 Color: 1
Size: 147663 Color: 2

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 475773 Color: 3
Size: 294762 Color: 0
Size: 229466 Color: 2

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 475946 Color: 4
Size: 293905 Color: 3
Size: 230150 Color: 3

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 475996 Color: 2
Size: 292898 Color: 4
Size: 231107 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 476179 Color: 0
Size: 295883 Color: 4
Size: 227939 Color: 2

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 476373 Color: 2
Size: 373262 Color: 3
Size: 150366 Color: 4

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 476558 Color: 0
Size: 293683 Color: 1
Size: 229760 Color: 4

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 476593 Color: 2
Size: 375022 Color: 1
Size: 148386 Color: 2

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 476670 Color: 1
Size: 296846 Color: 0
Size: 226485 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 476693 Color: 3
Size: 293552 Color: 0
Size: 229756 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 476739 Color: 2
Size: 300219 Color: 3
Size: 223043 Color: 2

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 477221 Color: 4
Size: 292344 Color: 0
Size: 230436 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 477314 Color: 1
Size: 300100 Color: 4
Size: 222587 Color: 4

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 477566 Color: 1
Size: 369379 Color: 0
Size: 153056 Color: 4

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 477597 Color: 0
Size: 304270 Color: 2
Size: 218134 Color: 3

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 477691 Color: 1
Size: 295568 Color: 2
Size: 226742 Color: 3

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 477697 Color: 4
Size: 369815 Color: 3
Size: 152489 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 477733 Color: 0
Size: 296362 Color: 4
Size: 225906 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 477870 Color: 4
Size: 297897 Color: 3
Size: 224234 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 477901 Color: 4
Size: 294089 Color: 2
Size: 228011 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 477984 Color: 0
Size: 295186 Color: 4
Size: 226831 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 478001 Color: 1
Size: 298255 Color: 0
Size: 223745 Color: 3

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 478031 Color: 4
Size: 372127 Color: 1
Size: 149843 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 478399 Color: 1
Size: 295202 Color: 0
Size: 226400 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 478437 Color: 3
Size: 296228 Color: 1
Size: 225336 Color: 3

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 478440 Color: 2
Size: 294563 Color: 0
Size: 226998 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 478541 Color: 3
Size: 298244 Color: 4
Size: 223216 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 478623 Color: 4
Size: 295852 Color: 2
Size: 225526 Color: 3

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 478892 Color: 0
Size: 295498 Color: 3
Size: 225611 Color: 2

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 479125 Color: 3
Size: 293145 Color: 2
Size: 227731 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 479251 Color: 4
Size: 296061 Color: 3
Size: 224689 Color: 4

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 479367 Color: 2
Size: 377399 Color: 3
Size: 143235 Color: 4

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 479477 Color: 0
Size: 295682 Color: 1
Size: 224842 Color: 3

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 479574 Color: 1
Size: 296005 Color: 2
Size: 224422 Color: 2

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 479725 Color: 1
Size: 295768 Color: 4
Size: 224508 Color: 2

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 479817 Color: 0
Size: 297039 Color: 3
Size: 223145 Color: 4

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 479838 Color: 4
Size: 371688 Color: 1
Size: 148475 Color: 4

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 479853 Color: 3
Size: 304148 Color: 1
Size: 216000 Color: 3

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 480001 Color: 4
Size: 296189 Color: 3
Size: 223811 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 480077 Color: 2
Size: 371006 Color: 0
Size: 148918 Color: 2

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 480143 Color: 2
Size: 370175 Color: 4
Size: 149683 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 480237 Color: 2
Size: 296873 Color: 1
Size: 222891 Color: 2

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 480401 Color: 4
Size: 298114 Color: 2
Size: 221486 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 480505 Color: 0
Size: 372440 Color: 2
Size: 147056 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 480732 Color: 4
Size: 296537 Color: 3
Size: 222732 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 480845 Color: 3
Size: 305393 Color: 4
Size: 213763 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 481196 Color: 2
Size: 300088 Color: 4
Size: 218717 Color: 2

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 481395 Color: 1
Size: 296946 Color: 2
Size: 221660 Color: 2

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 481446 Color: 2
Size: 298625 Color: 4
Size: 219930 Color: 4

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 481691 Color: 2
Size: 371686 Color: 3
Size: 146624 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 481713 Color: 4
Size: 304871 Color: 2
Size: 213417 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 481721 Color: 4
Size: 298797 Color: 3
Size: 219483 Color: 2

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 481848 Color: 1
Size: 298011 Color: 2
Size: 220142 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 481879 Color: 0
Size: 369036 Color: 3
Size: 149086 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 481885 Color: 4
Size: 366347 Color: 0
Size: 151769 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 482286 Color: 4
Size: 306117 Color: 0
Size: 211598 Color: 4

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 482828 Color: 3
Size: 302980 Color: 1
Size: 214193 Color: 4

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 482866 Color: 0
Size: 303289 Color: 2
Size: 213846 Color: 3

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 482870 Color: 3
Size: 302089 Color: 1
Size: 215042 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 483177 Color: 1
Size: 299435 Color: 2
Size: 217389 Color: 2

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 483342 Color: 3
Size: 302182 Color: 4
Size: 214477 Color: 2

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 483389 Color: 1
Size: 369463 Color: 2
Size: 147149 Color: 2

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 484443 Color: 1
Size: 301061 Color: 2
Size: 214497 Color: 2

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 484697 Color: 1
Size: 306720 Color: 0
Size: 208584 Color: 3

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 485115 Color: 1
Size: 302601 Color: 3
Size: 212285 Color: 2

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 485163 Color: 4
Size: 301993 Color: 3
Size: 212845 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 485408 Color: 2
Size: 299462 Color: 3
Size: 215131 Color: 2

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 485583 Color: 4
Size: 298523 Color: 1
Size: 215895 Color: 4

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 485586 Color: 1
Size: 365560 Color: 3
Size: 148855 Color: 3

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 485773 Color: 4
Size: 301636 Color: 0
Size: 212592 Color: 2

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 486011 Color: 0
Size: 301263 Color: 2
Size: 212727 Color: 2

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 486114 Color: 4
Size: 372714 Color: 3
Size: 141173 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 486142 Color: 3
Size: 304985 Color: 2
Size: 208874 Color: 4

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 486186 Color: 1
Size: 366432 Color: 4
Size: 147383 Color: 3

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 486373 Color: 2
Size: 305736 Color: 3
Size: 207892 Color: 2

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 486553 Color: 2
Size: 308733 Color: 1
Size: 204715 Color: 2

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 486629 Color: 0
Size: 370849 Color: 4
Size: 142523 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 486863 Color: 0
Size: 302853 Color: 3
Size: 210285 Color: 3

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 487043 Color: 3
Size: 302952 Color: 1
Size: 210006 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 487081 Color: 4
Size: 304373 Color: 2
Size: 208547 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 487511 Color: 1
Size: 369045 Color: 3
Size: 143445 Color: 2

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 487668 Color: 0
Size: 305158 Color: 2
Size: 207175 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 487827 Color: 4
Size: 300825 Color: 1
Size: 211349 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 487849 Color: 4
Size: 302455 Color: 3
Size: 209697 Color: 4

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 487932 Color: 0
Size: 311547 Color: 3
Size: 200522 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 488102 Color: 2
Size: 301426 Color: 3
Size: 210473 Color: 4

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 488112 Color: 1
Size: 306467 Color: 4
Size: 205422 Color: 3

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 488355 Color: 2
Size: 363648 Color: 0
Size: 147998 Color: 2

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 488452 Color: 4
Size: 369865 Color: 1
Size: 141684 Color: 3

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 488537 Color: 2
Size: 305722 Color: 3
Size: 205742 Color: 2

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 488552 Color: 1
Size: 365706 Color: 4
Size: 145743 Color: 2

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 488683 Color: 3
Size: 304823 Color: 2
Size: 206495 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 489065 Color: 1
Size: 302743 Color: 3
Size: 208193 Color: 2

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 489089 Color: 3
Size: 302404 Color: 0
Size: 208508 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 489095 Color: 0
Size: 304899 Color: 1
Size: 206007 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 489241 Color: 2
Size: 301731 Color: 0
Size: 209029 Color: 3

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 489517 Color: 0
Size: 302407 Color: 2
Size: 208077 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 489542 Color: 3
Size: 304636 Color: 4
Size: 205823 Color: 2

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 489588 Color: 2
Size: 303190 Color: 4
Size: 207223 Color: 4

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 489638 Color: 1
Size: 302856 Color: 4
Size: 207507 Color: 1

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 489890 Color: 2
Size: 304926 Color: 3
Size: 205185 Color: 2

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 489937 Color: 2
Size: 303455 Color: 0
Size: 206609 Color: 3

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 490224 Color: 0
Size: 302316 Color: 3
Size: 207461 Color: 3

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 490264 Color: 1
Size: 369100 Color: 3
Size: 140637 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 490286 Color: 4
Size: 369629 Color: 1
Size: 140086 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 490580 Color: 2
Size: 304436 Color: 1
Size: 204985 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 490783 Color: 1
Size: 301746 Color: 2
Size: 207472 Color: 4

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 490868 Color: 0
Size: 304243 Color: 3
Size: 204890 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 491009 Color: 1
Size: 307841 Color: 0
Size: 201151 Color: 3

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 491244 Color: 1
Size: 368206 Color: 3
Size: 140551 Color: 4

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 491367 Color: 2
Size: 303636 Color: 4
Size: 204998 Color: 3

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 491416 Color: 2
Size: 308184 Color: 0
Size: 200401 Color: 4

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 491630 Color: 3
Size: 300160 Color: 0
Size: 208211 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 491668 Color: 2
Size: 304122 Color: 4
Size: 204211 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 491872 Color: 1
Size: 368411 Color: 0
Size: 139718 Color: 3

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 491883 Color: 2
Size: 308753 Color: 4
Size: 199365 Color: 3

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 492121 Color: 1
Size: 302196 Color: 0
Size: 205684 Color: 2

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 492297 Color: 3
Size: 303365 Color: 1
Size: 204339 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 492613 Color: 1
Size: 303027 Color: 4
Size: 204361 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 492690 Color: 0
Size: 303553 Color: 2
Size: 203758 Color: 2

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 492825 Color: 2
Size: 366855 Color: 4
Size: 140321 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 492954 Color: 3
Size: 304662 Color: 0
Size: 202385 Color: 4

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 493225 Color: 0
Size: 306265 Color: 2
Size: 200511 Color: 3

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 493294 Color: 4
Size: 303504 Color: 1
Size: 203203 Color: 3

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 493329 Color: 0
Size: 300933 Color: 4
Size: 205739 Color: 3

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 493371 Color: 2
Size: 306967 Color: 0
Size: 199663 Color: 3

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 493425 Color: 3
Size: 309350 Color: 2
Size: 197226 Color: 2

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 493430 Color: 1
Size: 300401 Color: 0
Size: 206170 Color: 4

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 493570 Color: 2
Size: 306460 Color: 1
Size: 199971 Color: 3

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 493727 Color: 2
Size: 302450 Color: 3
Size: 203824 Color: 3

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 493736 Color: 2
Size: 306205 Color: 1
Size: 200060 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 493901 Color: 1
Size: 305730 Color: 3
Size: 200370 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 494008 Color: 2
Size: 302027 Color: 1
Size: 203966 Color: 3

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 494107 Color: 1
Size: 301910 Color: 2
Size: 203984 Color: 4

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 494162 Color: 1
Size: 300922 Color: 3
Size: 204917 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 494615 Color: 0
Size: 365152 Color: 1
Size: 140234 Color: 4

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 494801 Color: 3
Size: 373678 Color: 2
Size: 131522 Color: 1

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 494826 Color: 0
Size: 364920 Color: 3
Size: 140255 Color: 2

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 494905 Color: 0
Size: 310308 Color: 1
Size: 194788 Color: 3

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 494944 Color: 4
Size: 368252 Color: 1
Size: 136805 Color: 3

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 495085 Color: 0
Size: 307397 Color: 3
Size: 197519 Color: 4

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 495427 Color: 3
Size: 311806 Color: 1
Size: 192768 Color: 2

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 495523 Color: 2
Size: 363186 Color: 1
Size: 141292 Color: 3

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 495543 Color: 4
Size: 364897 Color: 1
Size: 139561 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 495951 Color: 1
Size: 365603 Color: 3
Size: 138447 Color: 3

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 496408 Color: 1
Size: 363982 Color: 3
Size: 139611 Color: 1

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 496702 Color: 3
Size: 369213 Color: 4
Size: 134086 Color: 3

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 497192 Color: 1
Size: 308151 Color: 2
Size: 194658 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 497404 Color: 3
Size: 359645 Color: 4
Size: 142952 Color: 3

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 497467 Color: 1
Size: 307124 Color: 4
Size: 195410 Color: 4

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 497736 Color: 1
Size: 315534 Color: 2
Size: 186731 Color: 1

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 497891 Color: 0
Size: 306997 Color: 2
Size: 195113 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 498179 Color: 4
Size: 313325 Color: 2
Size: 188497 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 498235 Color: 0
Size: 309082 Color: 3
Size: 192684 Color: 4

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 498585 Color: 4
Size: 308382 Color: 3
Size: 193034 Color: 3

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 498833 Color: 2
Size: 365126 Color: 1
Size: 136042 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 499058 Color: 3
Size: 360958 Color: 2
Size: 139985 Color: 4

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 499106 Color: 1
Size: 366375 Color: 3
Size: 134520 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 499142 Color: 0
Size: 310890 Color: 3
Size: 189969 Color: 4

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 499145 Color: 1
Size: 364289 Color: 3
Size: 136567 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 499238 Color: 3
Size: 353014 Color: 0
Size: 147749 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 499271 Color: 4
Size: 369114 Color: 0
Size: 131616 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 499350 Color: 0
Size: 368358 Color: 2
Size: 132293 Color: 3

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 499916 Color: 4
Size: 359857 Color: 2
Size: 140228 Color: 2

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 500366 Color: 0
Size: 364210 Color: 1
Size: 135425 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 500382 Color: 4
Size: 311373 Color: 2
Size: 188246 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 500516 Color: 2
Size: 308569 Color: 4
Size: 190916 Color: 3

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 500551 Color: 1
Size: 359471 Color: 2
Size: 139979 Color: 4

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 500647 Color: 3
Size: 367272 Color: 4
Size: 132082 Color: 4

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 500956 Color: 2
Size: 307696 Color: 4
Size: 191349 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 500969 Color: 4
Size: 367596 Color: 1
Size: 131436 Color: 3

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 501061 Color: 1
Size: 314302 Color: 2
Size: 184638 Color: 2

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 501096 Color: 3
Size: 315698 Color: 4
Size: 183207 Color: 2

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 501189 Color: 3
Size: 365745 Color: 2
Size: 133067 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 501345 Color: 1
Size: 365155 Color: 3
Size: 133501 Color: 1

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 501661 Color: 0
Size: 314632 Color: 3
Size: 183708 Color: 4

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 501729 Color: 4
Size: 309441 Color: 0
Size: 188831 Color: 3

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 501967 Color: 2
Size: 314493 Color: 3
Size: 183541 Color: 3

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 502320 Color: 4
Size: 310136 Color: 1
Size: 187545 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 502357 Color: 1
Size: 363262 Color: 3
Size: 134382 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 502374 Color: 3
Size: 364690 Color: 2
Size: 132937 Color: 3

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 502783 Color: 4
Size: 308596 Color: 0
Size: 188622 Color: 2

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 502922 Color: 2
Size: 312507 Color: 4
Size: 184572 Color: 1

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 502947 Color: 4
Size: 364128 Color: 3
Size: 132926 Color: 3

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 503221 Color: 2
Size: 308072 Color: 4
Size: 188708 Color: 2

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 503250 Color: 4
Size: 310494 Color: 2
Size: 186257 Color: 3

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 503519 Color: 2
Size: 306644 Color: 0
Size: 189838 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 503610 Color: 4
Size: 352373 Color: 3
Size: 144018 Color: 4

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 504000 Color: 0
Size: 361606 Color: 4
Size: 134395 Color: 4

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 504131 Color: 2
Size: 311523 Color: 3
Size: 184347 Color: 3

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 504171 Color: 1
Size: 354275 Color: 0
Size: 141555 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 504464 Color: 1
Size: 363833 Color: 0
Size: 131704 Color: 4

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 504511 Color: 2
Size: 308507 Color: 3
Size: 186983 Color: 3

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 504743 Color: 2
Size: 310022 Color: 0
Size: 185236 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 505012 Color: 0
Size: 365509 Color: 3
Size: 129480 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 505224 Color: 4
Size: 359928 Color: 1
Size: 134849 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 505248 Color: 0
Size: 315342 Color: 4
Size: 179411 Color: 4

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 505296 Color: 2
Size: 306384 Color: 4
Size: 188321 Color: 4

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 505305 Color: 2
Size: 361251 Color: 3
Size: 133445 Color: 4

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 505308 Color: 4
Size: 318434 Color: 3
Size: 176259 Color: 3

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 505316 Color: 1
Size: 365936 Color: 4
Size: 128749 Color: 4

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 505372 Color: 1
Size: 363280 Color: 4
Size: 131349 Color: 3

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 505393 Color: 2
Size: 318876 Color: 1
Size: 175732 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 505459 Color: 0
Size: 308765 Color: 4
Size: 185777 Color: 2

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 505487 Color: 3
Size: 366129 Color: 0
Size: 128385 Color: 1

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 505745 Color: 1
Size: 309350 Color: 3
Size: 184906 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 505754 Color: 3
Size: 308992 Color: 0
Size: 185255 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 505812 Color: 1
Size: 313311 Color: 0
Size: 180878 Color: 2

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 506145 Color: 2
Size: 310911 Color: 0
Size: 182945 Color: 4

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 506168 Color: 2
Size: 363043 Color: 0
Size: 130790 Color: 3

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 506374 Color: 1
Size: 313875 Color: 3
Size: 179752 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 506380 Color: 1
Size: 309847 Color: 0
Size: 183774 Color: 3

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 506390 Color: 3
Size: 314296 Color: 4
Size: 179315 Color: 3

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 506434 Color: 3
Size: 362680 Color: 1
Size: 130887 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 506454 Color: 0
Size: 307929 Color: 4
Size: 185618 Color: 4

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 506471 Color: 4
Size: 308161 Color: 3
Size: 185369 Color: 4

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 506890 Color: 3
Size: 310405 Color: 2
Size: 182706 Color: 3

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 507067 Color: 3
Size: 306812 Color: 0
Size: 186122 Color: 2

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 507280 Color: 1
Size: 366201 Color: 2
Size: 126520 Color: 3

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 507455 Color: 4
Size: 309308 Color: 0
Size: 183238 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 507718 Color: 3
Size: 309585 Color: 1
Size: 182698 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 507791 Color: 4
Size: 308888 Color: 3
Size: 183322 Color: 2

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 507813 Color: 4
Size: 311171 Color: 2
Size: 181017 Color: 3

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 507831 Color: 0
Size: 361096 Color: 1
Size: 131074 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 507853 Color: 3
Size: 319170 Color: 4
Size: 172978 Color: 2

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 507918 Color: 2
Size: 362234 Color: 0
Size: 129849 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 507965 Color: 3
Size: 368082 Color: 2
Size: 123954 Color: 4

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 508359 Color: 4
Size: 364957 Color: 2
Size: 126685 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 508550 Color: 2
Size: 358897 Color: 4
Size: 132554 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 508803 Color: 3
Size: 310022 Color: 0
Size: 181176 Color: 1

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 508837 Color: 3
Size: 363796 Color: 2
Size: 127368 Color: 4

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 509015 Color: 3
Size: 309627 Color: 2
Size: 181359 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 509123 Color: 4
Size: 350715 Color: 1
Size: 140163 Color: 1

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 509258 Color: 2
Size: 317324 Color: 4
Size: 173419 Color: 3

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 509342 Color: 3
Size: 314573 Color: 1
Size: 176086 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 509655 Color: 2
Size: 315459 Color: 4
Size: 174887 Color: 1

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 509743 Color: 1
Size: 309722 Color: 3
Size: 180536 Color: 3

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 509807 Color: 2
Size: 352641 Color: 4
Size: 137553 Color: 3

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 509845 Color: 1
Size: 318386 Color: 3
Size: 171770 Color: 3

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 509931 Color: 0
Size: 310271 Color: 4
Size: 179799 Color: 3

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 510020 Color: 2
Size: 313637 Color: 3
Size: 176344 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 510204 Color: 1
Size: 365845 Color: 2
Size: 123952 Color: 2

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 510324 Color: 4
Size: 314674 Color: 0
Size: 175003 Color: 2

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 510361 Color: 3
Size: 319502 Color: 1
Size: 170138 Color: 3

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 510599 Color: 3
Size: 312716 Color: 0
Size: 176686 Color: 4

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 510963 Color: 0
Size: 317562 Color: 1
Size: 171476 Color: 4

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 510971 Color: 2
Size: 352781 Color: 4
Size: 136249 Color: 4

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 511049 Color: 0
Size: 317991 Color: 4
Size: 170961 Color: 3

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 511200 Color: 2
Size: 310826 Color: 0
Size: 177975 Color: 4

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 511210 Color: 2
Size: 313903 Color: 0
Size: 174888 Color: 3

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 511425 Color: 4
Size: 313059 Color: 2
Size: 175517 Color: 1

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 511775 Color: 0
Size: 311177 Color: 2
Size: 177049 Color: 3

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 511830 Color: 2
Size: 320550 Color: 3
Size: 167621 Color: 4

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 511862 Color: 2
Size: 488139 Color: 1

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 511915 Color: 3
Size: 309793 Color: 2
Size: 178293 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 511964 Color: 4
Size: 313989 Color: 1
Size: 174048 Color: 3

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 512245 Color: 0
Size: 356060 Color: 1
Size: 131696 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 512427 Color: 0
Size: 310666 Color: 4
Size: 176908 Color: 2

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 512472 Color: 4
Size: 312946 Color: 2
Size: 174583 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 512506 Color: 3
Size: 311593 Color: 2
Size: 175902 Color: 4

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 512636 Color: 3
Size: 314095 Color: 4
Size: 173270 Color: 3

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 512668 Color: 0
Size: 312226 Color: 1
Size: 175107 Color: 3

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 512687 Color: 1
Size: 317263 Color: 2
Size: 170051 Color: 2

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 512737 Color: 2
Size: 322178 Color: 0
Size: 165086 Color: 1

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 512768 Color: 3
Size: 312282 Color: 2
Size: 174951 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 512790 Color: 3
Size: 313636 Color: 0
Size: 173575 Color: 2

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 513027 Color: 0
Size: 316295 Color: 1
Size: 170679 Color: 1

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 513099 Color: 2
Size: 313267 Color: 4
Size: 173635 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 514425 Color: 0
Size: 357597 Color: 2
Size: 127979 Color: 3

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 514509 Color: 3
Size: 355066 Color: 0
Size: 130426 Color: 3

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 514881 Color: 3
Size: 363588 Color: 4
Size: 121532 Color: 4

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 516049 Color: 0
Size: 358570 Color: 2
Size: 125382 Color: 3

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 516167 Color: 2
Size: 322522 Color: 3
Size: 161312 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 516202 Color: 3
Size: 348940 Color: 0
Size: 134859 Color: 4

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 516470 Color: 4
Size: 320099 Color: 3
Size: 163432 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 516497 Color: 4
Size: 326419 Color: 2
Size: 157085 Color: 3

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 516975 Color: 1
Size: 359304 Color: 2
Size: 123722 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 517025 Color: 2
Size: 321221 Color: 3
Size: 161755 Color: 4

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 517274 Color: 1
Size: 342927 Color: 2
Size: 139800 Color: 3

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 517427 Color: 1
Size: 324400 Color: 0
Size: 158174 Color: 3

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 517766 Color: 0
Size: 350303 Color: 4
Size: 131932 Color: 3

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 518016 Color: 4
Size: 320330 Color: 1
Size: 161655 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 518056 Color: 1
Size: 350222 Color: 0
Size: 131723 Color: 4

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 518175 Color: 2
Size: 359621 Color: 4
Size: 122205 Color: 4

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 518188 Color: 3
Size: 350088 Color: 0
Size: 131725 Color: 4

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 518276 Color: 1
Size: 360706 Color: 2
Size: 121019 Color: 2

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 518279 Color: 3
Size: 326074 Color: 0
Size: 155648 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 518576 Color: 0
Size: 351385 Color: 2
Size: 130040 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 518685 Color: 2
Size: 355658 Color: 1
Size: 125658 Color: 3

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 518951 Color: 4
Size: 324313 Color: 0
Size: 156737 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 518954 Color: 2
Size: 349022 Color: 0
Size: 132025 Color: 3

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 519472 Color: 4
Size: 480529 Color: 1

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 519524 Color: 4
Size: 355873 Color: 3
Size: 124604 Color: 0

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 519741 Color: 1
Size: 480260 Color: 4

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 520465 Color: 3
Size: 326203 Color: 1
Size: 153333 Color: 3

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 520589 Color: 0
Size: 348097 Color: 1
Size: 131315 Color: 1

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 520630 Color: 2
Size: 328184 Color: 3
Size: 151187 Color: 1

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 520820 Color: 1
Size: 321471 Color: 4
Size: 157710 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 520822 Color: 4
Size: 320093 Color: 3
Size: 159086 Color: 4

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 520847 Color: 4
Size: 354921 Color: 0
Size: 124233 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 520963 Color: 0
Size: 355294 Color: 3
Size: 123744 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 521189 Color: 3
Size: 328868 Color: 0
Size: 149944 Color: 2

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 521363 Color: 0
Size: 321194 Color: 2
Size: 157444 Color: 3

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 521424 Color: 4
Size: 352332 Color: 0
Size: 126245 Color: 2

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 521508 Color: 2
Size: 478493 Color: 4

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 521526 Color: 1
Size: 319335 Color: 4
Size: 159140 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 521613 Color: 0
Size: 358968 Color: 4
Size: 119420 Color: 3

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 521903 Color: 3
Size: 322614 Color: 0
Size: 155484 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 522163 Color: 2
Size: 315143 Color: 1
Size: 162695 Color: 4

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 522168 Color: 4
Size: 323243 Color: 1
Size: 154590 Color: 4

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 522242 Color: 1
Size: 323892 Color: 0
Size: 153867 Color: 1

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 522437 Color: 3
Size: 322921 Color: 0
Size: 154643 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 522517 Color: 1
Size: 317441 Color: 4
Size: 160043 Color: 2

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 522599 Color: 3
Size: 346153 Color: 4
Size: 131249 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 522618 Color: 4
Size: 320324 Color: 2
Size: 157059 Color: 4

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 522620 Color: 3
Size: 358567 Color: 2
Size: 118814 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 522623 Color: 3
Size: 354183 Color: 0
Size: 123195 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 522778 Color: 4
Size: 359041 Color: 0
Size: 118182 Color: 2

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 523049 Color: 4
Size: 325732 Color: 3
Size: 151220 Color: 2

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 523101 Color: 4
Size: 319140 Color: 2
Size: 157760 Color: 4

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 523154 Color: 0
Size: 316223 Color: 1
Size: 160624 Color: 1

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 523155 Color: 1
Size: 353509 Color: 0
Size: 123337 Color: 3

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 523437 Color: 3
Size: 326283 Color: 4
Size: 150281 Color: 3

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 523522 Color: 1
Size: 354706 Color: 2
Size: 121773 Color: 4

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 523581 Color: 3
Size: 320855 Color: 4
Size: 155565 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 523666 Color: 2
Size: 360316 Color: 4
Size: 116019 Color: 4

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 523773 Color: 3
Size: 359780 Color: 1
Size: 116448 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 523815 Color: 4
Size: 324965 Color: 0
Size: 151221 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 523982 Color: 2
Size: 348764 Color: 0
Size: 127255 Color: 4

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 524117 Color: 3
Size: 317808 Color: 4
Size: 158076 Color: 3

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 524214 Color: 4
Size: 318759 Color: 2
Size: 157028 Color: 4

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 524352 Color: 1
Size: 345342 Color: 0
Size: 130307 Color: 2

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 524718 Color: 2
Size: 322873 Color: 3
Size: 152410 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 524776 Color: 0
Size: 325222 Color: 2
Size: 150003 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 524855 Color: 0
Size: 314641 Color: 4
Size: 160505 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 524886 Color: 4
Size: 319632 Color: 0
Size: 155483 Color: 1

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 525013 Color: 3
Size: 319689 Color: 0
Size: 155299 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 525119 Color: 0
Size: 321387 Color: 3
Size: 153495 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 525410 Color: 4
Size: 356751 Color: 1
Size: 117840 Color: 2

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 525489 Color: 1
Size: 317104 Color: 3
Size: 157408 Color: 4

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 525497 Color: 2
Size: 352613 Color: 0
Size: 121891 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 525644 Color: 0
Size: 318791 Color: 2
Size: 155566 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 525737 Color: 3
Size: 356893 Color: 2
Size: 117371 Color: 4

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 525860 Color: 0
Size: 328309 Color: 1
Size: 145832 Color: 3

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 525990 Color: 0
Size: 359235 Color: 4
Size: 114776 Color: 1

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 526024 Color: 2
Size: 351021 Color: 1
Size: 122956 Color: 4

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 526103 Color: 0
Size: 316448 Color: 4
Size: 157450 Color: 2

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 526327 Color: 2
Size: 317049 Color: 4
Size: 156625 Color: 2

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 526339 Color: 4
Size: 321727 Color: 1
Size: 151935 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 526381 Color: 4
Size: 315499 Color: 0
Size: 158121 Color: 4

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 526460 Color: 4
Size: 315603 Color: 2
Size: 157938 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 526528 Color: 4
Size: 354657 Color: 3
Size: 118816 Color: 4

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 526572 Color: 2
Size: 321195 Color: 0
Size: 152234 Color: 4

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 526721 Color: 0
Size: 320330 Color: 3
Size: 152950 Color: 1

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 526758 Color: 3
Size: 318343 Color: 2
Size: 154900 Color: 3

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 526892 Color: 3
Size: 322050 Color: 1
Size: 151059 Color: 1

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 526974 Color: 3
Size: 320487 Color: 0
Size: 152540 Color: 3

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 526982 Color: 2
Size: 345851 Color: 1
Size: 127168 Color: 1

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 527072 Color: 0
Size: 318176 Color: 2
Size: 154753 Color: 3

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 527087 Color: 0
Size: 324413 Color: 2
Size: 148501 Color: 1

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 527224 Color: 1
Size: 318587 Color: 3
Size: 154190 Color: 3

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 527233 Color: 4
Size: 317903 Color: 0
Size: 154865 Color: 4

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 527297 Color: 1
Size: 358141 Color: 2
Size: 114563 Color: 1

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 527345 Color: 2
Size: 326366 Color: 0
Size: 146290 Color: 3

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 527797 Color: 0
Size: 353538 Color: 4
Size: 118666 Color: 4

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 527805 Color: 0
Size: 353546 Color: 2
Size: 118650 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 527851 Color: 2
Size: 318280 Color: 0
Size: 153870 Color: 1

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 527896 Color: 0
Size: 360485 Color: 3
Size: 111620 Color: 3

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 528190 Color: 4
Size: 323904 Color: 3
Size: 147907 Color: 2

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 528239 Color: 2
Size: 319424 Color: 1
Size: 152338 Color: 4

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 528345 Color: 1
Size: 320972 Color: 3
Size: 150684 Color: 2

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 528348 Color: 3
Size: 353275 Color: 0
Size: 118378 Color: 4

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 528556 Color: 2
Size: 321380 Color: 4
Size: 150065 Color: 4

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 528558 Color: 1
Size: 351886 Color: 4
Size: 119557 Color: 1

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 528621 Color: 1
Size: 353314 Color: 2
Size: 118066 Color: 3

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 528882 Color: 4
Size: 325492 Color: 2
Size: 145627 Color: 2

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 528968 Color: 1
Size: 352502 Color: 4
Size: 118531 Color: 2

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 529192 Color: 1
Size: 320015 Color: 3
Size: 150794 Color: 4

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 529261 Color: 4
Size: 352123 Color: 3
Size: 118617 Color: 2

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 529270 Color: 1
Size: 352642 Color: 2
Size: 118089 Color: 1

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 529483 Color: 2
Size: 351709 Color: 1
Size: 118809 Color: 2

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 529521 Color: 1
Size: 327484 Color: 4
Size: 142996 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 529718 Color: 1
Size: 357343 Color: 2
Size: 112940 Color: 4

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 530076 Color: 0
Size: 325652 Color: 1
Size: 144273 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 530130 Color: 0
Size: 319627 Color: 4
Size: 150244 Color: 4

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 530272 Color: 2
Size: 323174 Color: 3
Size: 146555 Color: 2

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 530356 Color: 0
Size: 323505 Color: 1
Size: 146140 Color: 4

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 530476 Color: 2
Size: 326893 Color: 3
Size: 142632 Color: 3

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 530651 Color: 0
Size: 321296 Color: 2
Size: 148054 Color: 1

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 530701 Color: 0
Size: 324047 Color: 1
Size: 145253 Color: 1

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 530715 Color: 4
Size: 321375 Color: 1
Size: 147911 Color: 2

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 530751 Color: 0
Size: 341991 Color: 4
Size: 127259 Color: 3

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 530870 Color: 1
Size: 354927 Color: 3
Size: 114204 Color: 3

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 531083 Color: 1
Size: 325545 Color: 2
Size: 143373 Color: 4

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 531097 Color: 0
Size: 322523 Color: 3
Size: 146381 Color: 4

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 531135 Color: 1
Size: 322939 Color: 0
Size: 145927 Color: 1

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 531284 Color: 1
Size: 468717 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 531671 Color: 0
Size: 322179 Color: 2
Size: 146151 Color: 3

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 531678 Color: 3
Size: 347782 Color: 1
Size: 120541 Color: 3

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 531702 Color: 4
Size: 326344 Color: 1
Size: 141955 Color: 1

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 531814 Color: 4
Size: 342372 Color: 3
Size: 125815 Color: 1

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 531878 Color: 0
Size: 338423 Color: 2
Size: 129700 Color: 1

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 532097 Color: 2
Size: 320394 Color: 4
Size: 147510 Color: 4

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 532266 Color: 0
Size: 327586 Color: 1
Size: 140149 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 532291 Color: 0
Size: 348896 Color: 3
Size: 118814 Color: 3

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 532465 Color: 4
Size: 325747 Color: 1
Size: 141789 Color: 1

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 532500 Color: 1
Size: 467501 Color: 2

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 532650 Color: 2
Size: 344897 Color: 0
Size: 122454 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 532725 Color: 2
Size: 324883 Color: 4
Size: 142393 Color: 2

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 532729 Color: 3
Size: 342371 Color: 0
Size: 124901 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 532754 Color: 2
Size: 348189 Color: 1
Size: 119058 Color: 4

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 532816 Color: 1
Size: 344944 Color: 3
Size: 122241 Color: 2

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 532847 Color: 2
Size: 325612 Color: 1
Size: 141542 Color: 3

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 533088 Color: 4
Size: 350625 Color: 0
Size: 116288 Color: 4

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 533206 Color: 0
Size: 324716 Color: 3
Size: 142079 Color: 3

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 533247 Color: 1
Size: 346984 Color: 2
Size: 119770 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 533371 Color: 1
Size: 323479 Color: 2
Size: 143151 Color: 2

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 533596 Color: 0
Size: 319354 Color: 1
Size: 147051 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 533767 Color: 0
Size: 350939 Color: 1
Size: 115295 Color: 2

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 533865 Color: 1
Size: 323284 Color: 2
Size: 142852 Color: 1

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 533981 Color: 3
Size: 326043 Color: 1
Size: 139977 Color: 4

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 534112 Color: 0
Size: 347573 Color: 3
Size: 118316 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 534199 Color: 1
Size: 341645 Color: 3
Size: 124157 Color: 4

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 534200 Color: 4
Size: 354718 Color: 0
Size: 111083 Color: 3

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 534357 Color: 4
Size: 360303 Color: 2
Size: 105341 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 534400 Color: 1
Size: 349465 Color: 4
Size: 116136 Color: 2

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 534401 Color: 2
Size: 352435 Color: 3
Size: 113165 Color: 3

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 534469 Color: 2
Size: 356978 Color: 3
Size: 108554 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 534475 Color: 2
Size: 326536 Color: 3
Size: 138990 Color: 4

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 534645 Color: 1
Size: 326414 Color: 3
Size: 138942 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 534749 Color: 2
Size: 350867 Color: 3
Size: 114385 Color: 2

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 534801 Color: 0
Size: 357645 Color: 1
Size: 107555 Color: 3

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 534897 Color: 3
Size: 333577 Color: 0
Size: 131527 Color: 3

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 534898 Color: 0
Size: 357002 Color: 2
Size: 108101 Color: 4

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 535085 Color: 1
Size: 332983 Color: 4
Size: 131933 Color: 4

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 535096 Color: 0
Size: 326036 Color: 1
Size: 138869 Color: 2

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 535934 Color: 2
Size: 329127 Color: 3
Size: 134940 Color: 3

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 536033 Color: 2
Size: 349831 Color: 1
Size: 114137 Color: 1

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 536073 Color: 0
Size: 327538 Color: 2
Size: 136390 Color: 3

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 536539 Color: 0
Size: 347140 Color: 2
Size: 116322 Color: 0

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 537001 Color: 4
Size: 329796 Color: 2
Size: 133204 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 537134 Color: 4
Size: 327102 Color: 1
Size: 135765 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 537239 Color: 2
Size: 335650 Color: 1
Size: 127112 Color: 3

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 537340 Color: 0
Size: 333818 Color: 2
Size: 128843 Color: 3

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 537486 Color: 2
Size: 333489 Color: 0
Size: 129026 Color: 2

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 537708 Color: 1
Size: 348261 Color: 3
Size: 114032 Color: 2

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 538025 Color: 0
Size: 348068 Color: 3
Size: 113908 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 538226 Color: 3
Size: 325874 Color: 2
Size: 135901 Color: 1

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 538295 Color: 1
Size: 326336 Color: 2
Size: 135370 Color: 4

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 538637 Color: 4
Size: 332265 Color: 2
Size: 129099 Color: 2

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 538819 Color: 4
Size: 347994 Color: 3
Size: 113188 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 539239 Color: 2
Size: 329672 Color: 0
Size: 131090 Color: 3

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 539372 Color: 1
Size: 328754 Color: 3
Size: 131875 Color: 4

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 539404 Color: 1
Size: 325315 Color: 2
Size: 135282 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 539583 Color: 0
Size: 349471 Color: 3
Size: 110947 Color: 1

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 539720 Color: 4
Size: 345434 Color: 2
Size: 114847 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 540008 Color: 1
Size: 356469 Color: 4
Size: 103524 Color: 1

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 540148 Color: 2
Size: 330470 Color: 0
Size: 129383 Color: 3

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 540223 Color: 1
Size: 347738 Color: 4
Size: 112040 Color: 2

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 540262 Color: 0
Size: 339260 Color: 1
Size: 120479 Color: 2

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 540435 Color: 2
Size: 347086 Color: 1
Size: 112480 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 540457 Color: 3
Size: 335785 Color: 4
Size: 123759 Color: 2

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 540733 Color: 3
Size: 336428 Color: 4
Size: 122840 Color: 3

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 540780 Color: 0
Size: 326950 Color: 4
Size: 132271 Color: 4

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 540840 Color: 2
Size: 353991 Color: 3
Size: 105170 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 540958 Color: 2
Size: 328202 Color: 4
Size: 130841 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 541090 Color: 3
Size: 341662 Color: 2
Size: 117249 Color: 0

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 541186 Color: 0
Size: 458815 Color: 3

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 541236 Color: 4
Size: 350519 Color: 3
Size: 108246 Color: 3

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 541335 Color: 4
Size: 329997 Color: 1
Size: 128669 Color: 3

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 541400 Color: 2
Size: 327635 Color: 0
Size: 130966 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 541487 Color: 2
Size: 352196 Color: 1
Size: 106318 Color: 1

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 541787 Color: 4
Size: 354539 Color: 0
Size: 103675 Color: 3

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 541858 Color: 0
Size: 353347 Color: 2
Size: 104796 Color: 1

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 541902 Color: 3
Size: 334627 Color: 1
Size: 123472 Color: 4

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 542079 Color: 1
Size: 329887 Color: 3
Size: 128035 Color: 2

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 542221 Color: 4
Size: 349525 Color: 0
Size: 108255 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 542409 Color: 3
Size: 351099 Color: 1
Size: 106493 Color: 4

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 542440 Color: 3
Size: 345190 Color: 2
Size: 112371 Color: 4

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 542447 Color: 1
Size: 327832 Color: 0
Size: 129722 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 542496 Color: 2
Size: 332151 Color: 3
Size: 125354 Color: 1

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 542512 Color: 1
Size: 347190 Color: 2
Size: 110299 Color: 2

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 542573 Color: 3
Size: 328412 Color: 1
Size: 129016 Color: 4

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 542768 Color: 2
Size: 351545 Color: 1
Size: 105688 Color: 3

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 542853 Color: 0
Size: 343613 Color: 4
Size: 113535 Color: 3

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 543003 Color: 0
Size: 336608 Color: 2
Size: 120390 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 543024 Color: 0
Size: 341347 Color: 2
Size: 115630 Color: 4

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 543056 Color: 0
Size: 332502 Color: 4
Size: 124443 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 543155 Color: 3
Size: 334737 Color: 4
Size: 122109 Color: 1

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 543182 Color: 0
Size: 331388 Color: 1
Size: 125431 Color: 4

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 543189 Color: 3
Size: 339702 Color: 0
Size: 117110 Color: 3

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 543283 Color: 1
Size: 329347 Color: 2
Size: 127371 Color: 2

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 543451 Color: 1
Size: 343104 Color: 0
Size: 113446 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 543581 Color: 0
Size: 330119 Color: 4
Size: 126301 Color: 1

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 543717 Color: 2
Size: 345442 Color: 1
Size: 110842 Color: 1

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 543724 Color: 0
Size: 327803 Color: 2
Size: 128474 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 543844 Color: 4
Size: 331952 Color: 1
Size: 124205 Color: 1

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 543888 Color: 1
Size: 328943 Color: 3
Size: 127170 Color: 2

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 544116 Color: 3
Size: 355656 Color: 2
Size: 100229 Color: 2

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 544129 Color: 4
Size: 335770 Color: 1
Size: 120102 Color: 3

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 544351 Color: 2
Size: 332817 Color: 1
Size: 122833 Color: 2

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 544583 Color: 2
Size: 347658 Color: 0
Size: 107760 Color: 2

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 544607 Color: 3
Size: 339592 Color: 2
Size: 115802 Color: 3

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 544784 Color: 2
Size: 346733 Color: 0
Size: 108484 Color: 4

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 544964 Color: 1
Size: 352070 Color: 0
Size: 102967 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 545108 Color: 0
Size: 342420 Color: 4
Size: 112473 Color: 2

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 545130 Color: 0
Size: 348105 Color: 4
Size: 106766 Color: 4

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 545143 Color: 1
Size: 333972 Color: 2
Size: 120886 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 545303 Color: 3
Size: 336944 Color: 1
Size: 117754 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 545656 Color: 2
Size: 332797 Color: 3
Size: 121548 Color: 1

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 545905 Color: 2
Size: 349183 Color: 0
Size: 104913 Color: 3

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 545943 Color: 1
Size: 346030 Color: 3
Size: 108028 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 545966 Color: 0
Size: 332673 Color: 2
Size: 121362 Color: 4

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 545993 Color: 4
Size: 338938 Color: 3
Size: 115070 Color: 4

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 546217 Color: 2
Size: 342930 Color: 1
Size: 110854 Color: 3

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 546402 Color: 2
Size: 343309 Color: 3
Size: 110290 Color: 1

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 546513 Color: 1
Size: 346564 Color: 2
Size: 106924 Color: 0

Bin 743: 0 of cap free
Amount of items: 2
Items: 
Size: 546871 Color: 1
Size: 453130 Color: 3

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 546960 Color: 3
Size: 343737 Color: 2
Size: 109304 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 547109 Color: 0
Size: 347926 Color: 4
Size: 104966 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 547279 Color: 0
Size: 349510 Color: 4
Size: 103212 Color: 3

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 547343 Color: 2
Size: 347075 Color: 4
Size: 105583 Color: 4

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 547353 Color: 2
Size: 332673 Color: 1
Size: 119975 Color: 1

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 547439 Color: 4
Size: 347104 Color: 0
Size: 105458 Color: 1

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 547499 Color: 2
Size: 331123 Color: 3
Size: 121379 Color: 4

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 547554 Color: 0
Size: 345603 Color: 3
Size: 106844 Color: 2

Bin 752: 0 of cap free
Amount of items: 2
Items: 
Size: 547559 Color: 0
Size: 452442 Color: 2

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 547584 Color: 0
Size: 347577 Color: 3
Size: 104840 Color: 3

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 547612 Color: 4
Size: 340145 Color: 2
Size: 112244 Color: 3

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 547618 Color: 0
Size: 337900 Color: 1
Size: 114483 Color: 3

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 547640 Color: 3
Size: 343855 Color: 2
Size: 108506 Color: 3

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 547665 Color: 3
Size: 332526 Color: 2
Size: 119810 Color: 2

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 547841 Color: 3
Size: 342599 Color: 1
Size: 109561 Color: 3

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 547869 Color: 0
Size: 330794 Color: 1
Size: 121338 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 547943 Color: 4
Size: 340786 Color: 0
Size: 111272 Color: 4

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 548022 Color: 1
Size: 344624 Color: 3
Size: 107355 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 548031 Color: 3
Size: 346968 Color: 4
Size: 105002 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 548059 Color: 3
Size: 333675 Color: 1
Size: 118267 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 548175 Color: 4
Size: 345037 Color: 3
Size: 106789 Color: 4

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 548202 Color: 4
Size: 336599 Color: 1
Size: 115200 Color: 2

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 548233 Color: 0
Size: 341577 Color: 2
Size: 110191 Color: 2

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 548239 Color: 0
Size: 351624 Color: 1
Size: 100138 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 548320 Color: 1
Size: 343847 Color: 0
Size: 107834 Color: 3

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 548331 Color: 3
Size: 349279 Color: 1
Size: 102391 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 548348 Color: 3
Size: 351182 Color: 1
Size: 100471 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 548410 Color: 3
Size: 338798 Color: 1
Size: 112793 Color: 3

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 548440 Color: 4
Size: 348363 Color: 3
Size: 103198 Color: 3

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 548667 Color: 4
Size: 347337 Color: 3
Size: 103997 Color: 1

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 548683 Color: 1
Size: 341870 Color: 3
Size: 109448 Color: 3

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 548741 Color: 0
Size: 335686 Color: 4
Size: 115574 Color: 1

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 548930 Color: 4
Size: 343377 Color: 2
Size: 107694 Color: 2

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 549047 Color: 1
Size: 333173 Color: 2
Size: 117781 Color: 2

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 549140 Color: 3
Size: 333686 Color: 2
Size: 117175 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 549192 Color: 0
Size: 350704 Color: 2
Size: 100105 Color: 4

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 549595 Color: 3
Size: 336149 Color: 1
Size: 114257 Color: 0

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 549603 Color: 4
Size: 330646 Color: 0
Size: 119752 Color: 2

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 549731 Color: 4
Size: 347350 Color: 3
Size: 102920 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 549772 Color: 0
Size: 341100 Color: 4
Size: 109129 Color: 1

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 549837 Color: 2
Size: 347226 Color: 4
Size: 102938 Color: 3

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 549950 Color: 1
Size: 344382 Color: 3
Size: 105669 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 550068 Color: 0
Size: 336196 Color: 2
Size: 113737 Color: 3

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 550079 Color: 2
Size: 335980 Color: 1
Size: 113942 Color: 3

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 550286 Color: 1
Size: 338512 Color: 2
Size: 111203 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 550571 Color: 0
Size: 332033 Color: 4
Size: 117397 Color: 3

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 550794 Color: 4
Size: 347031 Color: 2
Size: 102176 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 550961 Color: 0
Size: 344909 Color: 3
Size: 104131 Color: 4

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 551002 Color: 1
Size: 340198 Color: 4
Size: 108801 Color: 1

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 551012 Color: 1
Size: 348268 Color: 3
Size: 100721 Color: 3

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 551029 Color: 3
Size: 345004 Color: 2
Size: 103968 Color: 4

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 551096 Color: 2
Size: 335885 Color: 3
Size: 113020 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 551135 Color: 4
Size: 346699 Color: 3
Size: 102167 Color: 2

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 551247 Color: 1
Size: 344767 Color: 3
Size: 103987 Color: 4

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 551286 Color: 3
Size: 341440 Color: 4
Size: 107275 Color: 1

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 551407 Color: 3
Size: 342131 Color: 2
Size: 106463 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 551587 Color: 3
Size: 332293 Color: 2
Size: 116121 Color: 3

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 551669 Color: 3
Size: 338510 Color: 1
Size: 109822 Color: 2

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 551678 Color: 2
Size: 342928 Color: 1
Size: 105395 Color: 1

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 551749 Color: 3
Size: 335674 Color: 2
Size: 112578 Color: 3

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 551786 Color: 2
Size: 346490 Color: 0
Size: 101725 Color: 3

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 551864 Color: 0
Size: 334723 Color: 3
Size: 113414 Color: 4

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 551923 Color: 3
Size: 333694 Color: 0
Size: 114384 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 551967 Color: 0
Size: 343209 Color: 4
Size: 104825 Color: 1

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 552052 Color: 0
Size: 346242 Color: 1
Size: 101707 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 552104 Color: 3
Size: 340125 Color: 1
Size: 107772 Color: 2

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 552177 Color: 1
Size: 334628 Color: 2
Size: 113196 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 552297 Color: 0
Size: 342957 Color: 1
Size: 104747 Color: 3

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 552374 Color: 4
Size: 334461 Color: 3
Size: 113166 Color: 3

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 552396 Color: 0
Size: 338791 Color: 4
Size: 108814 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 552462 Color: 1
Size: 346329 Color: 0
Size: 101210 Color: 3

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 552498 Color: 1
Size: 330665 Color: 4
Size: 116838 Color: 2

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 552504 Color: 3
Size: 345985 Color: 0
Size: 101512 Color: 2

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 552594 Color: 3
Size: 340621 Color: 2
Size: 106786 Color: 3

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 552639 Color: 4
Size: 330500 Color: 1
Size: 116862 Color: 3

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 552683 Color: 0
Size: 343996 Color: 3
Size: 103322 Color: 2

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 552776 Color: 2
Size: 333793 Color: 3
Size: 113432 Color: 1

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 552793 Color: 1
Size: 344184 Color: 4
Size: 103024 Color: 1

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 552907 Color: 2
Size: 343707 Color: 4
Size: 103387 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 552909 Color: 4
Size: 333502 Color: 2
Size: 113590 Color: 4

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 552981 Color: 0
Size: 344665 Color: 2
Size: 102355 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 553016 Color: 0
Size: 345515 Color: 4
Size: 101470 Color: 2

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 553027 Color: 4
Size: 333485 Color: 3
Size: 113489 Color: 2

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 553191 Color: 0
Size: 336635 Color: 4
Size: 110175 Color: 2

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 553242 Color: 1
Size: 342139 Color: 3
Size: 104620 Color: 2

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 553395 Color: 1
Size: 331461 Color: 3
Size: 115145 Color: 4

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 553448 Color: 1
Size: 337078 Color: 3
Size: 109475 Color: 2

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 553459 Color: 0
Size: 337267 Color: 2
Size: 109275 Color: 2

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 553533 Color: 2
Size: 345895 Color: 3
Size: 100573 Color: 2

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 553712 Color: 0
Size: 338852 Color: 3
Size: 107437 Color: 3

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 553734 Color: 0
Size: 330856 Color: 1
Size: 115411 Color: 4

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 553736 Color: 2
Size: 344688 Color: 4
Size: 101577 Color: 1

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 553809 Color: 2
Size: 339334 Color: 0
Size: 106858 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 553810 Color: 1
Size: 336022 Color: 0
Size: 110169 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 553960 Color: 1
Size: 335495 Color: 3
Size: 110546 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 554158 Color: 0
Size: 345274 Color: 2
Size: 100569 Color: 0

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 554283 Color: 0
Size: 338393 Color: 3
Size: 107325 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 554303 Color: 0
Size: 341446 Color: 3
Size: 104252 Color: 2

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 554349 Color: 1
Size: 341186 Color: 3
Size: 104466 Color: 1

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 554386 Color: 4
Size: 337117 Color: 1
Size: 108498 Color: 3

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 554494 Color: 3
Size: 338061 Color: 0
Size: 107446 Color: 3

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 554563 Color: 3
Size: 334596 Color: 1
Size: 110842 Color: 3

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 554873 Color: 1
Size: 335928 Color: 4
Size: 109200 Color: 2

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 554891 Color: 0
Size: 341243 Color: 3
Size: 103867 Color: 1

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 554930 Color: 0
Size: 338148 Color: 2
Size: 106923 Color: 2

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 554949 Color: 0
Size: 339538 Color: 3
Size: 105514 Color: 4

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 555049 Color: 0
Size: 339686 Color: 4
Size: 105266 Color: 1

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 555225 Color: 1
Size: 339226 Color: 4
Size: 105550 Color: 2

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 555246 Color: 0
Size: 330085 Color: 2
Size: 114670 Color: 4

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 555403 Color: 0
Size: 341873 Color: 2
Size: 102725 Color: 1

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 555435 Color: 2
Size: 343889 Color: 0
Size: 100677 Color: 2

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 555545 Color: 1
Size: 339889 Color: 4
Size: 104567 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 555627 Color: 1
Size: 343730 Color: 0
Size: 100644 Color: 4

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 555671 Color: 2
Size: 339340 Color: 4
Size: 104990 Color: 3

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 555672 Color: 4
Size: 343253 Color: 2
Size: 101076 Color: 2

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 555792 Color: 3
Size: 344004 Color: 1
Size: 100205 Color: 3

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 555912 Color: 0
Size: 333614 Color: 3
Size: 110475 Color: 4

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 555978 Color: 4
Size: 336863 Color: 3
Size: 107160 Color: 1

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 556055 Color: 4
Size: 343484 Color: 1
Size: 100462 Color: 1

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 556162 Color: 2
Size: 337463 Color: 3
Size: 106376 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 556242 Color: 2
Size: 339452 Color: 0
Size: 104307 Color: 4

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 556325 Color: 3
Size: 340928 Color: 0
Size: 102748 Color: 3

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 556467 Color: 1
Size: 343428 Color: 3
Size: 100106 Color: 4

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 557139 Color: 4
Size: 339926 Color: 3
Size: 102936 Color: 1

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 557212 Color: 2
Size: 338137 Color: 4
Size: 104652 Color: 3

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 557282 Color: 3
Size: 335351 Color: 0
Size: 107368 Color: 2

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 557324 Color: 4
Size: 340876 Color: 1
Size: 101801 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 557486 Color: 1
Size: 337386 Color: 4
Size: 105129 Color: 1

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 557514 Color: 2
Size: 332360 Color: 3
Size: 110127 Color: 1

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 557608 Color: 4
Size: 338928 Color: 2
Size: 103465 Color: 3

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 557930 Color: 4
Size: 339088 Color: 0
Size: 102983 Color: 1

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 557947 Color: 4
Size: 332468 Color: 1
Size: 109586 Color: 0

Bin 876: 0 of cap free
Amount of items: 2
Items: 
Size: 557964 Color: 4
Size: 442037 Color: 3

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 558038 Color: 1
Size: 334859 Color: 4
Size: 107104 Color: 2

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 558042 Color: 2
Size: 331124 Color: 1
Size: 110835 Color: 4

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 558097 Color: 0
Size: 335945 Color: 4
Size: 105959 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 558120 Color: 2
Size: 333619 Color: 1
Size: 108262 Color: 4

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 558399 Color: 0
Size: 340032 Color: 3
Size: 101570 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 558443 Color: 2
Size: 336062 Color: 4
Size: 105496 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 558495 Color: 0
Size: 331069 Color: 3
Size: 110437 Color: 2

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 558550 Color: 0
Size: 335907 Color: 4
Size: 105544 Color: 3

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 558802 Color: 3
Size: 330576 Color: 0
Size: 110623 Color: 0

Bin 886: 1 of cap free
Amount of items: 3
Items: 
Size: 404695 Color: 0
Size: 397225 Color: 1
Size: 198080 Color: 0

Bin 887: 1 of cap free
Amount of items: 4
Items: 
Size: 410010 Color: 2
Size: 198139 Color: 4
Size: 197449 Color: 0
Size: 194402 Color: 0

Bin 888: 1 of cap free
Amount of items: 4
Items: 
Size: 411374 Color: 4
Size: 198847 Color: 3
Size: 195552 Color: 0
Size: 194227 Color: 1

Bin 889: 1 of cap free
Amount of items: 4
Items: 
Size: 414571 Color: 4
Size: 205304 Color: 2
Size: 190371 Color: 1
Size: 189754 Color: 3

Bin 890: 1 of cap free
Amount of items: 3
Items: 
Size: 416193 Color: 2
Size: 397102 Color: 3
Size: 186705 Color: 3

Bin 891: 1 of cap free
Amount of items: 4
Items: 
Size: 416764 Color: 4
Size: 210025 Color: 3
Size: 186738 Color: 4
Size: 186473 Color: 1

Bin 892: 1 of cap free
Amount of items: 3
Items: 
Size: 421133 Color: 4
Size: 395102 Color: 3
Size: 183765 Color: 2

Bin 893: 1 of cap free
Amount of items: 3
Items: 
Size: 443421 Color: 1
Size: 384911 Color: 3
Size: 171668 Color: 2

Bin 894: 1 of cap free
Amount of items: 3
Items: 
Size: 444686 Color: 3
Size: 384202 Color: 1
Size: 171112 Color: 1

Bin 895: 1 of cap free
Amount of items: 3
Items: 
Size: 445252 Color: 0
Size: 384046 Color: 2
Size: 170702 Color: 2

Bin 896: 1 of cap free
Amount of items: 3
Items: 
Size: 445404 Color: 3
Size: 385560 Color: 2
Size: 169036 Color: 4

Bin 897: 1 of cap free
Amount of items: 3
Items: 
Size: 445631 Color: 0
Size: 386815 Color: 3
Size: 167554 Color: 3

Bin 898: 1 of cap free
Amount of items: 3
Items: 
Size: 445976 Color: 2
Size: 277912 Color: 3
Size: 276112 Color: 4

Bin 899: 1 of cap free
Amount of items: 3
Items: 
Size: 446159 Color: 2
Size: 279253 Color: 0
Size: 274588 Color: 2

Bin 900: 1 of cap free
Amount of items: 3
Items: 
Size: 446234 Color: 2
Size: 277593 Color: 1
Size: 276173 Color: 3

Bin 901: 1 of cap free
Amount of items: 3
Items: 
Size: 446277 Color: 2
Size: 386989 Color: 3
Size: 166734 Color: 4

Bin 902: 1 of cap free
Amount of items: 3
Items: 
Size: 446597 Color: 4
Size: 386066 Color: 0
Size: 167337 Color: 4

Bin 903: 1 of cap free
Amount of items: 3
Items: 
Size: 446846 Color: 0
Size: 277489 Color: 2
Size: 275665 Color: 1

Bin 904: 1 of cap free
Amount of items: 3
Items: 
Size: 446940 Color: 4
Size: 278452 Color: 0
Size: 274608 Color: 1

Bin 905: 1 of cap free
Amount of items: 3
Items: 
Size: 447143 Color: 1
Size: 387944 Color: 2
Size: 164913 Color: 2

Bin 906: 1 of cap free
Amount of items: 3
Items: 
Size: 447152 Color: 3
Size: 276609 Color: 4
Size: 276239 Color: 4

Bin 907: 1 of cap free
Amount of items: 3
Items: 
Size: 447498 Color: 4
Size: 280826 Color: 0
Size: 271676 Color: 2

Bin 908: 1 of cap free
Amount of items: 3
Items: 
Size: 447680 Color: 0
Size: 282083 Color: 3
Size: 270237 Color: 2

Bin 909: 1 of cap free
Amount of items: 3
Items: 
Size: 447786 Color: 3
Size: 277148 Color: 2
Size: 275066 Color: 2

Bin 910: 1 of cap free
Amount of items: 3
Items: 
Size: 447891 Color: 1
Size: 384371 Color: 0
Size: 167738 Color: 1

Bin 911: 1 of cap free
Amount of items: 3
Items: 
Size: 448336 Color: 2
Size: 383400 Color: 3
Size: 168264 Color: 0

Bin 912: 1 of cap free
Amount of items: 3
Items: 
Size: 448888 Color: 4
Size: 382277 Color: 1
Size: 168835 Color: 2

Bin 913: 1 of cap free
Amount of items: 3
Items: 
Size: 449140 Color: 4
Size: 379378 Color: 3
Size: 171482 Color: 4

Bin 914: 1 of cap free
Amount of items: 3
Items: 
Size: 449176 Color: 0
Size: 279760 Color: 2
Size: 271064 Color: 3

Bin 915: 1 of cap free
Amount of items: 3
Items: 
Size: 449467 Color: 0
Size: 280216 Color: 1
Size: 270317 Color: 2

Bin 916: 1 of cap free
Amount of items: 3
Items: 
Size: 449782 Color: 2
Size: 283479 Color: 0
Size: 266739 Color: 4

Bin 917: 1 of cap free
Amount of items: 3
Items: 
Size: 450044 Color: 0
Size: 283830 Color: 2
Size: 266126 Color: 3

Bin 918: 1 of cap free
Amount of items: 3
Items: 
Size: 450241 Color: 0
Size: 383138 Color: 2
Size: 166621 Color: 2

Bin 919: 1 of cap free
Amount of items: 3
Items: 
Size: 450384 Color: 2
Size: 280389 Color: 0
Size: 269227 Color: 3

Bin 920: 1 of cap free
Amount of items: 3
Items: 
Size: 450606 Color: 1
Size: 282755 Color: 2
Size: 266639 Color: 0

Bin 921: 1 of cap free
Amount of items: 3
Items: 
Size: 450748 Color: 0
Size: 282679 Color: 4
Size: 266573 Color: 0

Bin 922: 1 of cap free
Amount of items: 3
Items: 
Size: 450762 Color: 4
Size: 278131 Color: 3
Size: 271107 Color: 1

Bin 923: 1 of cap free
Amount of items: 3
Items: 
Size: 450797 Color: 1
Size: 380298 Color: 2
Size: 168905 Color: 3

Bin 924: 1 of cap free
Amount of items: 3
Items: 
Size: 451057 Color: 0
Size: 285230 Color: 1
Size: 263713 Color: 4

Bin 925: 1 of cap free
Amount of items: 3
Items: 
Size: 451337 Color: 1
Size: 384212 Color: 3
Size: 164451 Color: 3

Bin 926: 1 of cap free
Amount of items: 3
Items: 
Size: 451621 Color: 1
Size: 279371 Color: 2
Size: 269008 Color: 3

Bin 927: 1 of cap free
Amount of items: 3
Items: 
Size: 451733 Color: 2
Size: 282571 Color: 4
Size: 265696 Color: 2

Bin 928: 1 of cap free
Amount of items: 3
Items: 
Size: 451737 Color: 0
Size: 376358 Color: 3
Size: 171905 Color: 1

Bin 929: 1 of cap free
Amount of items: 3
Items: 
Size: 451761 Color: 3
Size: 381840 Color: 4
Size: 166399 Color: 3

Bin 930: 1 of cap free
Amount of items: 3
Items: 
Size: 451928 Color: 3
Size: 278718 Color: 0
Size: 269354 Color: 2

Bin 931: 1 of cap free
Amount of items: 3
Items: 
Size: 451933 Color: 1
Size: 281784 Color: 0
Size: 266283 Color: 3

Bin 932: 1 of cap free
Amount of items: 3
Items: 
Size: 452688 Color: 0
Size: 284587 Color: 3
Size: 262725 Color: 4

Bin 933: 1 of cap free
Amount of items: 3
Items: 
Size: 453120 Color: 0
Size: 279957 Color: 1
Size: 266923 Color: 1

Bin 934: 1 of cap free
Amount of items: 3
Items: 
Size: 453518 Color: 4
Size: 381703 Color: 2
Size: 164779 Color: 1

Bin 935: 1 of cap free
Amount of items: 3
Items: 
Size: 453586 Color: 3
Size: 281606 Color: 1
Size: 264808 Color: 3

Bin 936: 1 of cap free
Amount of items: 3
Items: 
Size: 453725 Color: 4
Size: 283419 Color: 2
Size: 262856 Color: 4

Bin 937: 1 of cap free
Amount of items: 3
Items: 
Size: 453837 Color: 4
Size: 284877 Color: 3
Size: 261286 Color: 3

Bin 938: 1 of cap free
Amount of items: 3
Items: 
Size: 454182 Color: 4
Size: 281832 Color: 0
Size: 263986 Color: 3

Bin 939: 1 of cap free
Amount of items: 3
Items: 
Size: 454414 Color: 0
Size: 284068 Color: 4
Size: 261518 Color: 4

Bin 940: 1 of cap free
Amount of items: 3
Items: 
Size: 454524 Color: 0
Size: 282624 Color: 1
Size: 262852 Color: 1

Bin 941: 1 of cap free
Amount of items: 3
Items: 
Size: 454956 Color: 3
Size: 381440 Color: 4
Size: 163604 Color: 4

Bin 942: 1 of cap free
Amount of items: 3
Items: 
Size: 455468 Color: 3
Size: 284428 Color: 0
Size: 260104 Color: 3

Bin 943: 1 of cap free
Amount of items: 3
Items: 
Size: 455950 Color: 1
Size: 284867 Color: 3
Size: 259183 Color: 2

Bin 944: 1 of cap free
Amount of items: 3
Items: 
Size: 456100 Color: 0
Size: 285692 Color: 2
Size: 258208 Color: 3

Bin 945: 1 of cap free
Amount of items: 3
Items: 
Size: 456112 Color: 2
Size: 284114 Color: 3
Size: 259774 Color: 3

Bin 946: 1 of cap free
Amount of items: 3
Items: 
Size: 456147 Color: 1
Size: 285454 Color: 2
Size: 258399 Color: 1

Bin 947: 1 of cap free
Amount of items: 3
Items: 
Size: 456275 Color: 4
Size: 380214 Color: 3
Size: 163511 Color: 0

Bin 948: 1 of cap free
Amount of items: 3
Items: 
Size: 456740 Color: 4
Size: 372524 Color: 3
Size: 170736 Color: 1

Bin 949: 1 of cap free
Amount of items: 3
Items: 
Size: 456835 Color: 3
Size: 283669 Color: 1
Size: 259496 Color: 4

Bin 950: 1 of cap free
Amount of items: 3
Items: 
Size: 457269 Color: 2
Size: 380060 Color: 4
Size: 162671 Color: 0

Bin 951: 1 of cap free
Amount of items: 3
Items: 
Size: 457460 Color: 2
Size: 381795 Color: 0
Size: 160745 Color: 3

Bin 952: 1 of cap free
Amount of items: 3
Items: 
Size: 460046 Color: 0
Size: 379606 Color: 3
Size: 160348 Color: 2

Bin 953: 1 of cap free
Amount of items: 3
Items: 
Size: 460682 Color: 4
Size: 378505 Color: 2
Size: 160813 Color: 1

Bin 954: 1 of cap free
Amount of items: 3
Items: 
Size: 461694 Color: 2
Size: 378139 Color: 3
Size: 160167 Color: 0

Bin 955: 1 of cap free
Amount of items: 3
Items: 
Size: 461777 Color: 1
Size: 377435 Color: 4
Size: 160788 Color: 1

Bin 956: 1 of cap free
Amount of items: 3
Items: 
Size: 462055 Color: 1
Size: 374910 Color: 3
Size: 163035 Color: 1

Bin 957: 1 of cap free
Amount of items: 3
Items: 
Size: 462588 Color: 4
Size: 377460 Color: 0
Size: 159952 Color: 1

Bin 958: 1 of cap free
Amount of items: 3
Items: 
Size: 462594 Color: 2
Size: 379205 Color: 1
Size: 158201 Color: 2

Bin 959: 1 of cap free
Amount of items: 3
Items: 
Size: 463322 Color: 2
Size: 288339 Color: 0
Size: 248339 Color: 4

Bin 960: 1 of cap free
Amount of items: 3
Items: 
Size: 463761 Color: 2
Size: 288065 Color: 4
Size: 248174 Color: 1

Bin 961: 1 of cap free
Amount of items: 3
Items: 
Size: 463779 Color: 3
Size: 303530 Color: 4
Size: 232691 Color: 4

Bin 962: 1 of cap free
Amount of items: 3
Items: 
Size: 463869 Color: 4
Size: 289558 Color: 0
Size: 246573 Color: 2

Bin 963: 1 of cap free
Amount of items: 3
Items: 
Size: 464078 Color: 0
Size: 381787 Color: 3
Size: 154135 Color: 1

Bin 964: 1 of cap free
Amount of items: 3
Items: 
Size: 464594 Color: 3
Size: 378323 Color: 0
Size: 157083 Color: 0

Bin 965: 1 of cap free
Amount of items: 3
Items: 
Size: 464872 Color: 4
Size: 375148 Color: 0
Size: 159980 Color: 2

Bin 966: 1 of cap free
Amount of items: 3
Items: 
Size: 465255 Color: 0
Size: 287743 Color: 2
Size: 247002 Color: 2

Bin 967: 1 of cap free
Amount of items: 3
Items: 
Size: 465259 Color: 1
Size: 370242 Color: 2
Size: 164499 Color: 2

Bin 968: 1 of cap free
Amount of items: 3
Items: 
Size: 465332 Color: 0
Size: 380208 Color: 4
Size: 154460 Color: 4

Bin 969: 1 of cap free
Amount of items: 3
Items: 
Size: 465576 Color: 2
Size: 375759 Color: 3
Size: 158665 Color: 2

Bin 970: 1 of cap free
Amount of items: 3
Items: 
Size: 465616 Color: 2
Size: 290297 Color: 0
Size: 244087 Color: 1

Bin 971: 1 of cap free
Amount of items: 3
Items: 
Size: 465632 Color: 3
Size: 375773 Color: 1
Size: 158595 Color: 3

Bin 972: 1 of cap free
Amount of items: 3
Items: 
Size: 465926 Color: 2
Size: 375624 Color: 1
Size: 158450 Color: 1

Bin 973: 1 of cap free
Amount of items: 3
Items: 
Size: 466342 Color: 4
Size: 286524 Color: 1
Size: 247134 Color: 2

Bin 974: 1 of cap free
Amount of items: 3
Items: 
Size: 466991 Color: 0
Size: 375454 Color: 1
Size: 157555 Color: 0

Bin 975: 1 of cap free
Amount of items: 3
Items: 
Size: 467419 Color: 0
Size: 289789 Color: 2
Size: 242792 Color: 1

Bin 976: 1 of cap free
Amount of items: 3
Items: 
Size: 468019 Color: 2
Size: 287539 Color: 1
Size: 244442 Color: 0

Bin 977: 1 of cap free
Amount of items: 3
Items: 
Size: 468257 Color: 4
Size: 378645 Color: 0
Size: 153098 Color: 1

Bin 978: 1 of cap free
Amount of items: 3
Items: 
Size: 468663 Color: 1
Size: 286382 Color: 4
Size: 244955 Color: 4

Bin 979: 1 of cap free
Amount of items: 3
Items: 
Size: 468800 Color: 3
Size: 289636 Color: 0
Size: 241564 Color: 0

Bin 980: 1 of cap free
Amount of items: 3
Items: 
Size: 468958 Color: 2
Size: 369507 Color: 1
Size: 161535 Color: 2

Bin 981: 1 of cap free
Amount of items: 3
Items: 
Size: 469056 Color: 0
Size: 289285 Color: 1
Size: 241659 Color: 1

Bin 982: 1 of cap free
Amount of items: 3
Items: 
Size: 469425 Color: 2
Size: 289420 Color: 1
Size: 241155 Color: 1

Bin 983: 1 of cap free
Amount of items: 3
Items: 
Size: 469774 Color: 3
Size: 372447 Color: 4
Size: 157779 Color: 1

Bin 984: 1 of cap free
Amount of items: 3
Items: 
Size: 470249 Color: 0
Size: 374955 Color: 4
Size: 154796 Color: 1

Bin 985: 1 of cap free
Amount of items: 3
Items: 
Size: 470349 Color: 3
Size: 294582 Color: 4
Size: 235069 Color: 3

Bin 986: 1 of cap free
Amount of items: 3
Items: 
Size: 470530 Color: 0
Size: 291803 Color: 3
Size: 237667 Color: 4

Bin 987: 1 of cap free
Amount of items: 3
Items: 
Size: 470787 Color: 0
Size: 369882 Color: 3
Size: 159331 Color: 2

Bin 988: 1 of cap free
Amount of items: 3
Items: 
Size: 471048 Color: 1
Size: 292674 Color: 2
Size: 236278 Color: 1

Bin 989: 1 of cap free
Amount of items: 3
Items: 
Size: 471404 Color: 4
Size: 299771 Color: 2
Size: 228825 Color: 1

Bin 990: 1 of cap free
Amount of items: 3
Items: 
Size: 471485 Color: 1
Size: 293167 Color: 0
Size: 235348 Color: 3

Bin 991: 1 of cap free
Amount of items: 3
Items: 
Size: 471491 Color: 3
Size: 374937 Color: 0
Size: 153572 Color: 0

Bin 992: 1 of cap free
Amount of items: 3
Items: 
Size: 472283 Color: 0
Size: 292462 Color: 2
Size: 235255 Color: 4

Bin 993: 1 of cap free
Amount of items: 3
Items: 
Size: 472420 Color: 3
Size: 293225 Color: 2
Size: 234355 Color: 0

Bin 994: 1 of cap free
Amount of items: 3
Items: 
Size: 473059 Color: 1
Size: 290937 Color: 0
Size: 236004 Color: 2

Bin 995: 1 of cap free
Amount of items: 3
Items: 
Size: 473096 Color: 3
Size: 296931 Color: 2
Size: 229973 Color: 3

Bin 996: 1 of cap free
Amount of items: 3
Items: 
Size: 473259 Color: 1
Size: 293476 Color: 3
Size: 233265 Color: 1

Bin 997: 1 of cap free
Amount of items: 3
Items: 
Size: 473320 Color: 2
Size: 377299 Color: 3
Size: 149381 Color: 3

Bin 998: 1 of cap free
Amount of items: 3
Items: 
Size: 473961 Color: 1
Size: 305881 Color: 4
Size: 220158 Color: 1

Bin 999: 1 of cap free
Amount of items: 3
Items: 
Size: 474023 Color: 3
Size: 294763 Color: 1
Size: 231214 Color: 1

Bin 1000: 1 of cap free
Amount of items: 3
Items: 
Size: 474967 Color: 4
Size: 367767 Color: 3
Size: 157266 Color: 3

Bin 1001: 1 of cap free
Amount of items: 3
Items: 
Size: 475215 Color: 0
Size: 375466 Color: 2
Size: 149319 Color: 0

Bin 1002: 1 of cap free
Amount of items: 3
Items: 
Size: 475321 Color: 0
Size: 297904 Color: 3
Size: 226775 Color: 2

Bin 1003: 1 of cap free
Amount of items: 3
Items: 
Size: 475351 Color: 2
Size: 304073 Color: 3
Size: 220576 Color: 3

Bin 1004: 1 of cap free
Amount of items: 3
Items: 
Size: 475466 Color: 2
Size: 369555 Color: 0
Size: 154979 Color: 0

Bin 1005: 1 of cap free
Amount of items: 3
Items: 
Size: 475544 Color: 0
Size: 295282 Color: 4
Size: 229174 Color: 3

Bin 1006: 1 of cap free
Amount of items: 3
Items: 
Size: 475739 Color: 0
Size: 297574 Color: 4
Size: 226687 Color: 4

Bin 1007: 1 of cap free
Amount of items: 3
Items: 
Size: 476340 Color: 0
Size: 295402 Color: 3
Size: 228258 Color: 1

Bin 1008: 1 of cap free
Amount of items: 3
Items: 
Size: 477071 Color: 1
Size: 369941 Color: 3
Size: 152988 Color: 2

Bin 1009: 1 of cap free
Amount of items: 3
Items: 
Size: 477398 Color: 4
Size: 293741 Color: 2
Size: 228861 Color: 4

Bin 1010: 1 of cap free
Amount of items: 3
Items: 
Size: 477542 Color: 1
Size: 294641 Color: 3
Size: 227817 Color: 0

Bin 1011: 1 of cap free
Amount of items: 3
Items: 
Size: 479243 Color: 1
Size: 368241 Color: 2
Size: 152516 Color: 3

Bin 1012: 1 of cap free
Amount of items: 3
Items: 
Size: 479485 Color: 3
Size: 296940 Color: 4
Size: 223575 Color: 3

Bin 1013: 1 of cap free
Amount of items: 3
Items: 
Size: 479686 Color: 2
Size: 371369 Color: 1
Size: 148945 Color: 1

Bin 1014: 1 of cap free
Amount of items: 3
Items: 
Size: 479935 Color: 1
Size: 299783 Color: 4
Size: 220282 Color: 2

Bin 1015: 1 of cap free
Amount of items: 3
Items: 
Size: 480011 Color: 3
Size: 296716 Color: 4
Size: 223273 Color: 3

Bin 1016: 1 of cap free
Amount of items: 3
Items: 
Size: 480029 Color: 3
Size: 305797 Color: 1
Size: 214174 Color: 1

Bin 1017: 1 of cap free
Amount of items: 3
Items: 
Size: 480209 Color: 3
Size: 297029 Color: 2
Size: 222762 Color: 0

Bin 1018: 1 of cap free
Amount of items: 3
Items: 
Size: 480250 Color: 2
Size: 303379 Color: 3
Size: 216371 Color: 3

Bin 1019: 1 of cap free
Amount of items: 3
Items: 
Size: 481237 Color: 2
Size: 297247 Color: 4
Size: 221516 Color: 0

Bin 1020: 1 of cap free
Amount of items: 3
Items: 
Size: 481713 Color: 1
Size: 370797 Color: 4
Size: 147490 Color: 2

Bin 1021: 1 of cap free
Amount of items: 3
Items: 
Size: 481876 Color: 4
Size: 306931 Color: 1
Size: 211193 Color: 0

Bin 1022: 1 of cap free
Amount of items: 3
Items: 
Size: 481909 Color: 1
Size: 303492 Color: 3
Size: 214599 Color: 0

Bin 1023: 1 of cap free
Amount of items: 3
Items: 
Size: 482402 Color: 0
Size: 370568 Color: 2
Size: 147030 Color: 0

Bin 1024: 1 of cap free
Amount of items: 3
Items: 
Size: 482740 Color: 0
Size: 376683 Color: 3
Size: 140577 Color: 0

Bin 1025: 1 of cap free
Amount of items: 3
Items: 
Size: 482998 Color: 3
Size: 376477 Color: 4
Size: 140525 Color: 3

Bin 1026: 1 of cap free
Amount of items: 3
Items: 
Size: 483147 Color: 3
Size: 301048 Color: 2
Size: 215805 Color: 4

Bin 1027: 1 of cap free
Amount of items: 3
Items: 
Size: 483179 Color: 4
Size: 301115 Color: 3
Size: 215706 Color: 4

Bin 1028: 1 of cap free
Amount of items: 3
Items: 
Size: 483522 Color: 1
Size: 364024 Color: 4
Size: 152454 Color: 3

Bin 1029: 1 of cap free
Amount of items: 3
Items: 
Size: 483827 Color: 4
Size: 375375 Color: 0
Size: 140798 Color: 1

Bin 1030: 1 of cap free
Amount of items: 3
Items: 
Size: 484775 Color: 0
Size: 365613 Color: 1
Size: 149612 Color: 0

Bin 1031: 1 of cap free
Amount of items: 3
Items: 
Size: 484879 Color: 1
Size: 305945 Color: 2
Size: 209176 Color: 4

Bin 1032: 1 of cap free
Amount of items: 3
Items: 
Size: 484949 Color: 2
Size: 301727 Color: 0
Size: 213324 Color: 2

Bin 1033: 1 of cap free
Amount of items: 3
Items: 
Size: 485207 Color: 4
Size: 304838 Color: 1
Size: 209955 Color: 1

Bin 1034: 1 of cap free
Amount of items: 3
Items: 
Size: 485527 Color: 4
Size: 300112 Color: 1
Size: 214361 Color: 4

Bin 1035: 1 of cap free
Amount of items: 3
Items: 
Size: 485546 Color: 3
Size: 363581 Color: 1
Size: 150873 Color: 0

Bin 1036: 1 of cap free
Amount of items: 3
Items: 
Size: 486107 Color: 0
Size: 306357 Color: 1
Size: 207536 Color: 3

Bin 1037: 1 of cap free
Amount of items: 3
Items: 
Size: 486152 Color: 0
Size: 372064 Color: 4
Size: 141784 Color: 2

Bin 1038: 1 of cap free
Amount of items: 3
Items: 
Size: 486168 Color: 4
Size: 370220 Color: 0
Size: 143612 Color: 3

Bin 1039: 1 of cap free
Amount of items: 3
Items: 
Size: 486174 Color: 0
Size: 371320 Color: 2
Size: 142506 Color: 1

Bin 1040: 1 of cap free
Amount of items: 3
Items: 
Size: 486328 Color: 4
Size: 369046 Color: 3
Size: 144626 Color: 3

Bin 1041: 1 of cap free
Amount of items: 3
Items: 
Size: 486493 Color: 0
Size: 372681 Color: 4
Size: 140826 Color: 2

Bin 1042: 1 of cap free
Amount of items: 3
Items: 
Size: 486797 Color: 2
Size: 306099 Color: 4
Size: 207104 Color: 3

Bin 1043: 1 of cap free
Amount of items: 3
Items: 
Size: 486844 Color: 3
Size: 371299 Color: 2
Size: 141857 Color: 0

Bin 1044: 1 of cap free
Amount of items: 3
Items: 
Size: 486910 Color: 2
Size: 304877 Color: 1
Size: 208213 Color: 4

Bin 1045: 1 of cap free
Amount of items: 3
Items: 
Size: 487228 Color: 1
Size: 370143 Color: 2
Size: 142629 Color: 4

Bin 1046: 1 of cap free
Amount of items: 3
Items: 
Size: 487417 Color: 3
Size: 305776 Color: 0
Size: 206807 Color: 2

Bin 1047: 1 of cap free
Amount of items: 3
Items: 
Size: 487673 Color: 4
Size: 309416 Color: 1
Size: 202911 Color: 3

Bin 1048: 1 of cap free
Amount of items: 3
Items: 
Size: 487880 Color: 1
Size: 302028 Color: 0
Size: 210092 Color: 1

Bin 1049: 1 of cap free
Amount of items: 3
Items: 
Size: 488088 Color: 1
Size: 371546 Color: 4
Size: 140366 Color: 2

Bin 1050: 1 of cap free
Amount of items: 3
Items: 
Size: 488612 Color: 2
Size: 305483 Color: 3
Size: 205905 Color: 3

Bin 1051: 1 of cap free
Amount of items: 3
Items: 
Size: 488758 Color: 0
Size: 370374 Color: 3
Size: 140868 Color: 4

Bin 1052: 1 of cap free
Amount of items: 3
Items: 
Size: 489371 Color: 3
Size: 305668 Color: 0
Size: 204961 Color: 1

Bin 1053: 1 of cap free
Amount of items: 3
Items: 
Size: 490831 Color: 4
Size: 305588 Color: 3
Size: 203581 Color: 0

Bin 1054: 1 of cap free
Amount of items: 3
Items: 
Size: 491081 Color: 1
Size: 370632 Color: 4
Size: 138287 Color: 1

Bin 1055: 1 of cap free
Amount of items: 3
Items: 
Size: 491136 Color: 1
Size: 303055 Color: 2
Size: 205809 Color: 0

Bin 1056: 1 of cap free
Amount of items: 3
Items: 
Size: 491600 Color: 3
Size: 302942 Color: 2
Size: 205458 Color: 3

Bin 1057: 1 of cap free
Amount of items: 3
Items: 
Size: 492602 Color: 3
Size: 301159 Color: 4
Size: 206239 Color: 0

Bin 1058: 1 of cap free
Amount of items: 3
Items: 
Size: 492958 Color: 3
Size: 308968 Color: 0
Size: 198074 Color: 0

Bin 1059: 1 of cap free
Amount of items: 3
Items: 
Size: 493310 Color: 4
Size: 304070 Color: 1
Size: 202620 Color: 1

Bin 1060: 1 of cap free
Amount of items: 3
Items: 
Size: 493410 Color: 4
Size: 302580 Color: 0
Size: 204010 Color: 2

Bin 1061: 1 of cap free
Amount of items: 3
Items: 
Size: 493715 Color: 0
Size: 305496 Color: 4
Size: 200789 Color: 0

Bin 1062: 1 of cap free
Amount of items: 3
Items: 
Size: 494498 Color: 0
Size: 366411 Color: 4
Size: 139091 Color: 2

Bin 1063: 1 of cap free
Amount of items: 3
Items: 
Size: 494504 Color: 3
Size: 365914 Color: 0
Size: 139582 Color: 1

Bin 1064: 1 of cap free
Amount of items: 3
Items: 
Size: 494509 Color: 2
Size: 364567 Color: 1
Size: 140924 Color: 1

Bin 1065: 1 of cap free
Amount of items: 3
Items: 
Size: 496348 Color: 2
Size: 316035 Color: 4
Size: 187617 Color: 2

Bin 1066: 1 of cap free
Amount of items: 3
Items: 
Size: 496706 Color: 2
Size: 358536 Color: 4
Size: 144758 Color: 3

Bin 1067: 1 of cap free
Amount of items: 3
Items: 
Size: 496945 Color: 0
Size: 362977 Color: 1
Size: 140078 Color: 2

Bin 1068: 1 of cap free
Amount of items: 3
Items: 
Size: 497082 Color: 1
Size: 365672 Color: 4
Size: 137246 Color: 2

Bin 1069: 1 of cap free
Amount of items: 3
Items: 
Size: 497971 Color: 0
Size: 365534 Color: 2
Size: 136495 Color: 3

Bin 1070: 1 of cap free
Amount of items: 3
Items: 
Size: 498494 Color: 3
Size: 360644 Color: 4
Size: 140862 Color: 2

Bin 1071: 1 of cap free
Amount of items: 3
Items: 
Size: 498621 Color: 4
Size: 306872 Color: 0
Size: 194507 Color: 2

Bin 1072: 1 of cap free
Amount of items: 3
Items: 
Size: 498636 Color: 1
Size: 366922 Color: 3
Size: 134442 Color: 2

Bin 1073: 1 of cap free
Amount of items: 3
Items: 
Size: 498727 Color: 2
Size: 314478 Color: 4
Size: 186795 Color: 2

Bin 1074: 1 of cap free
Amount of items: 3
Items: 
Size: 498743 Color: 4
Size: 309539 Color: 1
Size: 191718 Color: 4

Bin 1075: 1 of cap free
Amount of items: 3
Items: 
Size: 499007 Color: 4
Size: 363723 Color: 1
Size: 137270 Color: 2

Bin 1076: 1 of cap free
Amount of items: 3
Items: 
Size: 499177 Color: 3
Size: 363043 Color: 0
Size: 137780 Color: 3

Bin 1077: 1 of cap free
Amount of items: 3
Items: 
Size: 500160 Color: 1
Size: 308360 Color: 4
Size: 191480 Color: 2

Bin 1078: 1 of cap free
Amount of items: 3
Items: 
Size: 500267 Color: 2
Size: 363842 Color: 1
Size: 135891 Color: 0

Bin 1079: 1 of cap free
Amount of items: 3
Items: 
Size: 500761 Color: 4
Size: 357781 Color: 1
Size: 141458 Color: 2

Bin 1080: 1 of cap free
Amount of items: 3
Items: 
Size: 500805 Color: 3
Size: 307728 Color: 0
Size: 191467 Color: 0

Bin 1081: 1 of cap free
Amount of items: 3
Items: 
Size: 501231 Color: 4
Size: 361778 Color: 1
Size: 136991 Color: 3

Bin 1082: 1 of cap free
Amount of items: 3
Items: 
Size: 501618 Color: 4
Size: 313060 Color: 3
Size: 185322 Color: 1

Bin 1083: 1 of cap free
Amount of items: 3
Items: 
Size: 501924 Color: 0
Size: 308467 Color: 2
Size: 189609 Color: 2

Bin 1084: 1 of cap free
Amount of items: 3
Items: 
Size: 501983 Color: 2
Size: 308949 Color: 3
Size: 189068 Color: 4

Bin 1085: 1 of cap free
Amount of items: 3
Items: 
Size: 502711 Color: 4
Size: 363194 Color: 0
Size: 134095 Color: 4

Bin 1086: 1 of cap free
Amount of items: 3
Items: 
Size: 503447 Color: 3
Size: 317032 Color: 1
Size: 179521 Color: 2

Bin 1087: 1 of cap free
Amount of items: 3
Items: 
Size: 503541 Color: 3
Size: 363795 Color: 4
Size: 132664 Color: 1

Bin 1088: 1 of cap free
Amount of items: 3
Items: 
Size: 504234 Color: 0
Size: 308506 Color: 1
Size: 187260 Color: 3

Bin 1089: 1 of cap free
Amount of items: 3
Items: 
Size: 505234 Color: 3
Size: 310546 Color: 1
Size: 184220 Color: 2

Bin 1090: 1 of cap free
Amount of items: 3
Items: 
Size: 505857 Color: 1
Size: 362839 Color: 4
Size: 131304 Color: 3

Bin 1091: 1 of cap free
Amount of items: 3
Items: 
Size: 507251 Color: 1
Size: 309536 Color: 0
Size: 183213 Color: 4

Bin 1092: 1 of cap free
Amount of items: 3
Items: 
Size: 507403 Color: 1
Size: 312523 Color: 2
Size: 180074 Color: 2

Bin 1093: 1 of cap free
Amount of items: 3
Items: 
Size: 507440 Color: 3
Size: 308536 Color: 4
Size: 184024 Color: 1

Bin 1094: 1 of cap free
Amount of items: 3
Items: 
Size: 507534 Color: 3
Size: 360447 Color: 4
Size: 132019 Color: 1

Bin 1095: 1 of cap free
Amount of items: 3
Items: 
Size: 508362 Color: 0
Size: 318215 Color: 3
Size: 173423 Color: 4

Bin 1096: 1 of cap free
Amount of items: 3
Items: 
Size: 508378 Color: 4
Size: 361580 Color: 1
Size: 130042 Color: 3

Bin 1097: 1 of cap free
Amount of items: 3
Items: 
Size: 508655 Color: 4
Size: 311776 Color: 0
Size: 179569 Color: 1

Bin 1098: 1 of cap free
Amount of items: 3
Items: 
Size: 509154 Color: 4
Size: 357147 Color: 0
Size: 133699 Color: 3

Bin 1099: 1 of cap free
Amount of items: 3
Items: 
Size: 509547 Color: 4
Size: 315963 Color: 2
Size: 174490 Color: 0

Bin 1100: 1 of cap free
Amount of items: 3
Items: 
Size: 510052 Color: 4
Size: 309710 Color: 3
Size: 180238 Color: 0

Bin 1101: 1 of cap free
Amount of items: 3
Items: 
Size: 510087 Color: 0
Size: 355911 Color: 4
Size: 134002 Color: 4

Bin 1102: 1 of cap free
Amount of items: 3
Items: 
Size: 510544 Color: 3
Size: 309960 Color: 4
Size: 179496 Color: 1

Bin 1103: 1 of cap free
Amount of items: 3
Items: 
Size: 510589 Color: 4
Size: 314114 Color: 1
Size: 175297 Color: 3

Bin 1104: 1 of cap free
Amount of items: 3
Items: 
Size: 510650 Color: 3
Size: 312170 Color: 1
Size: 177180 Color: 2

Bin 1105: 1 of cap free
Amount of items: 3
Items: 
Size: 510690 Color: 1
Size: 310146 Color: 2
Size: 179164 Color: 4

Bin 1106: 1 of cap free
Amount of items: 3
Items: 
Size: 511598 Color: 3
Size: 309412 Color: 0
Size: 178990 Color: 2

Bin 1107: 1 of cap free
Amount of items: 3
Items: 
Size: 511693 Color: 0
Size: 360970 Color: 3
Size: 127337 Color: 4

Bin 1108: 1 of cap free
Amount of items: 3
Items: 
Size: 511701 Color: 0
Size: 311096 Color: 3
Size: 177203 Color: 1

Bin 1109: 1 of cap free
Amount of items: 3
Items: 
Size: 512580 Color: 4
Size: 320081 Color: 1
Size: 167339 Color: 0

Bin 1110: 1 of cap free
Amount of items: 3
Items: 
Size: 513032 Color: 3
Size: 319410 Color: 1
Size: 167558 Color: 2

Bin 1111: 1 of cap free
Amount of items: 3
Items: 
Size: 514621 Color: 1
Size: 357402 Color: 2
Size: 127977 Color: 1

Bin 1112: 1 of cap free
Amount of items: 3
Items: 
Size: 515152 Color: 4
Size: 359315 Color: 2
Size: 125533 Color: 0

Bin 1113: 1 of cap free
Amount of items: 3
Items: 
Size: 516244 Color: 3
Size: 320249 Color: 4
Size: 163507 Color: 2

Bin 1114: 1 of cap free
Amount of items: 3
Items: 
Size: 516311 Color: 2
Size: 352033 Color: 1
Size: 131656 Color: 2

Bin 1115: 1 of cap free
Amount of items: 3
Items: 
Size: 517034 Color: 1
Size: 326156 Color: 4
Size: 156810 Color: 0

Bin 1116: 1 of cap free
Amount of items: 3
Items: 
Size: 517057 Color: 3
Size: 354815 Color: 1
Size: 128128 Color: 2

Bin 1117: 1 of cap free
Amount of items: 3
Items: 
Size: 517288 Color: 3
Size: 326208 Color: 0
Size: 156504 Color: 1

Bin 1118: 1 of cap free
Amount of items: 2
Items: 
Size: 517291 Color: 3
Size: 482709 Color: 2

Bin 1119: 1 of cap free
Amount of items: 3
Items: 
Size: 517410 Color: 2
Size: 325264 Color: 4
Size: 157326 Color: 4

Bin 1120: 1 of cap free
Amount of items: 3
Items: 
Size: 517501 Color: 3
Size: 352212 Color: 2
Size: 130287 Color: 1

Bin 1121: 1 of cap free
Amount of items: 3
Items: 
Size: 518027 Color: 1
Size: 358883 Color: 4
Size: 123090 Color: 1

Bin 1122: 1 of cap free
Amount of items: 3
Items: 
Size: 518183 Color: 2
Size: 325188 Color: 0
Size: 156629 Color: 4

Bin 1123: 1 of cap free
Amount of items: 3
Items: 
Size: 518317 Color: 3
Size: 320970 Color: 1
Size: 160713 Color: 2

Bin 1124: 1 of cap free
Amount of items: 3
Items: 
Size: 518571 Color: 4
Size: 352699 Color: 3
Size: 128730 Color: 3

Bin 1125: 1 of cap free
Amount of items: 3
Items: 
Size: 518639 Color: 4
Size: 345442 Color: 1
Size: 135919 Color: 3

Bin 1126: 1 of cap free
Amount of items: 3
Items: 
Size: 518971 Color: 0
Size: 359244 Color: 2
Size: 121785 Color: 0

Bin 1127: 1 of cap free
Amount of items: 3
Items: 
Size: 520328 Color: 4
Size: 315312 Color: 3
Size: 164360 Color: 3

Bin 1128: 1 of cap free
Amount of items: 3
Items: 
Size: 520591 Color: 2
Size: 318500 Color: 4
Size: 160909 Color: 0

Bin 1129: 1 of cap free
Amount of items: 3
Items: 
Size: 520748 Color: 1
Size: 319946 Color: 0
Size: 159306 Color: 2

Bin 1130: 1 of cap free
Amount of items: 3
Items: 
Size: 520800 Color: 0
Size: 357649 Color: 1
Size: 121551 Color: 1

Bin 1131: 1 of cap free
Amount of items: 3
Items: 
Size: 520885 Color: 2
Size: 354118 Color: 3
Size: 124997 Color: 2

Bin 1132: 1 of cap free
Amount of items: 3
Items: 
Size: 521102 Color: 3
Size: 354925 Color: 2
Size: 123973 Color: 1

Bin 1133: 1 of cap free
Amount of items: 3
Items: 
Size: 522301 Color: 1
Size: 356321 Color: 4
Size: 121378 Color: 2

Bin 1134: 1 of cap free
Amount of items: 3
Items: 
Size: 522452 Color: 4
Size: 315973 Color: 2
Size: 161575 Color: 0

Bin 1135: 1 of cap free
Amount of items: 3
Items: 
Size: 522559 Color: 2
Size: 326325 Color: 3
Size: 151116 Color: 4

Bin 1136: 1 of cap free
Amount of items: 3
Items: 
Size: 523049 Color: 2
Size: 315391 Color: 0
Size: 161560 Color: 4

Bin 1137: 1 of cap free
Amount of items: 3
Items: 
Size: 523451 Color: 1
Size: 348875 Color: 4
Size: 127674 Color: 3

Bin 1138: 1 of cap free
Amount of items: 3
Items: 
Size: 523588 Color: 1
Size: 317140 Color: 4
Size: 159272 Color: 3

Bin 1139: 1 of cap free
Amount of items: 3
Items: 
Size: 523631 Color: 1
Size: 321031 Color: 4
Size: 155338 Color: 2

Bin 1140: 1 of cap free
Amount of items: 3
Items: 
Size: 524358 Color: 1
Size: 350468 Color: 2
Size: 125174 Color: 4

Bin 1141: 1 of cap free
Amount of items: 2
Items: 
Size: 524533 Color: 2
Size: 475467 Color: 1

Bin 1142: 1 of cap free
Amount of items: 3
Items: 
Size: 525429 Color: 0
Size: 326903 Color: 4
Size: 147668 Color: 3

Bin 1143: 1 of cap free
Amount of items: 3
Items: 
Size: 525569 Color: 0
Size: 349677 Color: 2
Size: 124754 Color: 3

Bin 1144: 1 of cap free
Amount of items: 3
Items: 
Size: 525953 Color: 4
Size: 350399 Color: 0
Size: 123648 Color: 1

Bin 1145: 1 of cap free
Amount of items: 3
Items: 
Size: 525979 Color: 2
Size: 318505 Color: 0
Size: 155516 Color: 2

Bin 1146: 1 of cap free
Amount of items: 3
Items: 
Size: 526597 Color: 4
Size: 319856 Color: 0
Size: 153547 Color: 3

Bin 1147: 1 of cap free
Amount of items: 3
Items: 
Size: 528152 Color: 0
Size: 319484 Color: 4
Size: 152364 Color: 3

Bin 1148: 1 of cap free
Amount of items: 3
Items: 
Size: 528236 Color: 1
Size: 351175 Color: 3
Size: 120589 Color: 1

Bin 1149: 1 of cap free
Amount of items: 3
Items: 
Size: 528683 Color: 3
Size: 339345 Color: 0
Size: 131972 Color: 3

Bin 1150: 1 of cap free
Amount of items: 3
Items: 
Size: 528823 Color: 4
Size: 348303 Color: 1
Size: 122874 Color: 4

Bin 1151: 1 of cap free
Amount of items: 3
Items: 
Size: 528914 Color: 3
Size: 325040 Color: 4
Size: 146046 Color: 0

Bin 1152: 1 of cap free
Amount of items: 3
Items: 
Size: 529151 Color: 0
Size: 352557 Color: 3
Size: 118292 Color: 2

Bin 1153: 1 of cap free
Amount of items: 3
Items: 
Size: 529357 Color: 1
Size: 351733 Color: 4
Size: 118910 Color: 4

Bin 1154: 1 of cap free
Amount of items: 3
Items: 
Size: 529375 Color: 1
Size: 322100 Color: 0
Size: 148525 Color: 1

Bin 1155: 1 of cap free
Amount of items: 3
Items: 
Size: 529598 Color: 0
Size: 318890 Color: 2
Size: 151512 Color: 1

Bin 1156: 1 of cap free
Amount of items: 3
Items: 
Size: 529759 Color: 3
Size: 325415 Color: 4
Size: 144826 Color: 0

Bin 1157: 1 of cap free
Amount of items: 3
Items: 
Size: 530092 Color: 2
Size: 352084 Color: 1
Size: 117824 Color: 2

Bin 1158: 1 of cap free
Amount of items: 3
Items: 
Size: 530404 Color: 4
Size: 325943 Color: 2
Size: 143653 Color: 3

Bin 1159: 1 of cap free
Amount of items: 3
Items: 
Size: 530628 Color: 1
Size: 325890 Color: 3
Size: 143482 Color: 0

Bin 1160: 1 of cap free
Amount of items: 3
Items: 
Size: 530863 Color: 0
Size: 324592 Color: 1
Size: 144545 Color: 0

Bin 1161: 1 of cap free
Amount of items: 3
Items: 
Size: 531192 Color: 0
Size: 321074 Color: 3
Size: 147734 Color: 2

Bin 1162: 1 of cap free
Amount of items: 3
Items: 
Size: 531737 Color: 4
Size: 337631 Color: 1
Size: 130632 Color: 1

Bin 1163: 1 of cap free
Amount of items: 3
Items: 
Size: 532344 Color: 4
Size: 350051 Color: 2
Size: 117605 Color: 0

Bin 1164: 1 of cap free
Amount of items: 3
Items: 
Size: 532408 Color: 0
Size: 347933 Color: 2
Size: 119659 Color: 1

Bin 1165: 1 of cap free
Amount of items: 3
Items: 
Size: 533089 Color: 1
Size: 351561 Color: 4
Size: 115350 Color: 2

Bin 1166: 1 of cap free
Amount of items: 2
Items: 
Size: 533482 Color: 2
Size: 466518 Color: 4

Bin 1167: 1 of cap free
Amount of items: 3
Items: 
Size: 533919 Color: 0
Size: 322869 Color: 2
Size: 143212 Color: 3

Bin 1168: 1 of cap free
Amount of items: 3
Items: 
Size: 534480 Color: 3
Size: 342912 Color: 2
Size: 122608 Color: 3

Bin 1169: 1 of cap free
Amount of items: 3
Items: 
Size: 534827 Color: 1
Size: 352801 Color: 0
Size: 112372 Color: 2

Bin 1170: 1 of cap free
Amount of items: 3
Items: 
Size: 534850 Color: 0
Size: 333103 Color: 1
Size: 132047 Color: 3

Bin 1171: 1 of cap free
Amount of items: 3
Items: 
Size: 535611 Color: 2
Size: 325805 Color: 3
Size: 138584 Color: 4

Bin 1172: 1 of cap free
Amount of items: 3
Items: 
Size: 535958 Color: 2
Size: 327396 Color: 0
Size: 136646 Color: 3

Bin 1173: 1 of cap free
Amount of items: 3
Items: 
Size: 536355 Color: 0
Size: 351333 Color: 4
Size: 112312 Color: 1

Bin 1174: 1 of cap free
Amount of items: 3
Items: 
Size: 536431 Color: 3
Size: 326995 Color: 0
Size: 136574 Color: 1

Bin 1175: 1 of cap free
Amount of items: 3
Items: 
Size: 537468 Color: 4
Size: 351030 Color: 1
Size: 111502 Color: 3

Bin 1176: 1 of cap free
Amount of items: 3
Items: 
Size: 537494 Color: 1
Size: 332166 Color: 2
Size: 130340 Color: 3

Bin 1177: 1 of cap free
Amount of items: 3
Items: 
Size: 537819 Color: 2
Size: 340465 Color: 0
Size: 121716 Color: 0

Bin 1178: 1 of cap free
Amount of items: 3
Items: 
Size: 537944 Color: 3
Size: 338670 Color: 1
Size: 123386 Color: 3

Bin 1179: 1 of cap free
Amount of items: 3
Items: 
Size: 537965 Color: 1
Size: 325352 Color: 2
Size: 136683 Color: 3

Bin 1180: 1 of cap free
Amount of items: 3
Items: 
Size: 538330 Color: 2
Size: 326579 Color: 0
Size: 135091 Color: 3

Bin 1181: 1 of cap free
Amount of items: 3
Items: 
Size: 538481 Color: 1
Size: 347920 Color: 2
Size: 113599 Color: 0

Bin 1182: 1 of cap free
Amount of items: 2
Items: 
Size: 538922 Color: 3
Size: 461078 Color: 2

Bin 1183: 1 of cap free
Amount of items: 3
Items: 
Size: 539113 Color: 0
Size: 336955 Color: 4
Size: 123932 Color: 2

Bin 1184: 1 of cap free
Amount of items: 3
Items: 
Size: 540026 Color: 4
Size: 342407 Color: 0
Size: 117567 Color: 0

Bin 1185: 1 of cap free
Amount of items: 3
Items: 
Size: 540083 Color: 4
Size: 352361 Color: 1
Size: 107556 Color: 1

Bin 1186: 1 of cap free
Amount of items: 3
Items: 
Size: 540490 Color: 4
Size: 328891 Color: 3
Size: 130619 Color: 2

Bin 1187: 1 of cap free
Amount of items: 3
Items: 
Size: 540800 Color: 3
Size: 356832 Color: 2
Size: 102368 Color: 0

Bin 1188: 1 of cap free
Amount of items: 3
Items: 
Size: 541277 Color: 3
Size: 349119 Color: 4
Size: 109604 Color: 2

Bin 1189: 1 of cap free
Amount of items: 3
Items: 
Size: 541334 Color: 3
Size: 329858 Color: 4
Size: 128808 Color: 1

Bin 1190: 1 of cap free
Amount of items: 3
Items: 
Size: 541803 Color: 4
Size: 340484 Color: 0
Size: 117713 Color: 2

Bin 1191: 1 of cap free
Amount of items: 3
Items: 
Size: 541966 Color: 1
Size: 327850 Color: 4
Size: 130184 Color: 2

Bin 1192: 1 of cap free
Amount of items: 3
Items: 
Size: 542493 Color: 0
Size: 327817 Color: 4
Size: 129690 Color: 3

Bin 1193: 1 of cap free
Amount of items: 3
Items: 
Size: 542574 Color: 0
Size: 334373 Color: 4
Size: 123053 Color: 0

Bin 1194: 1 of cap free
Amount of items: 3
Items: 
Size: 542629 Color: 0
Size: 333448 Color: 4
Size: 123923 Color: 0

Bin 1195: 1 of cap free
Amount of items: 3
Items: 
Size: 542841 Color: 4
Size: 327752 Color: 1
Size: 129407 Color: 0

Bin 1196: 1 of cap free
Amount of items: 3
Items: 
Size: 543035 Color: 2
Size: 331568 Color: 1
Size: 125397 Color: 0

Bin 1197: 1 of cap free
Amount of items: 3
Items: 
Size: 543238 Color: 1
Size: 329518 Color: 2
Size: 127244 Color: 0

Bin 1198: 1 of cap free
Amount of items: 3
Items: 
Size: 543316 Color: 3
Size: 336016 Color: 4
Size: 120668 Color: 4

Bin 1199: 1 of cap free
Amount of items: 3
Items: 
Size: 543540 Color: 2
Size: 348584 Color: 0
Size: 107876 Color: 0

Bin 1200: 1 of cap free
Amount of items: 3
Items: 
Size: 543603 Color: 1
Size: 330503 Color: 3
Size: 125894 Color: 4

Bin 1201: 1 of cap free
Amount of items: 3
Items: 
Size: 543765 Color: 3
Size: 349866 Color: 4
Size: 106369 Color: 0

Bin 1202: 1 of cap free
Amount of items: 3
Items: 
Size: 543985 Color: 3
Size: 346610 Color: 4
Size: 109405 Color: 2

Bin 1203: 1 of cap free
Amount of items: 3
Items: 
Size: 544463 Color: 2
Size: 342632 Color: 1
Size: 112905 Color: 1

Bin 1204: 1 of cap free
Amount of items: 3
Items: 
Size: 544974 Color: 0
Size: 339352 Color: 3
Size: 115674 Color: 3

Bin 1205: 1 of cap free
Amount of items: 3
Items: 
Size: 545035 Color: 1
Size: 338464 Color: 0
Size: 116501 Color: 4

Bin 1206: 1 of cap free
Amount of items: 3
Items: 
Size: 545312 Color: 1
Size: 348321 Color: 4
Size: 106367 Color: 1

Bin 1207: 1 of cap free
Amount of items: 3
Items: 
Size: 545372 Color: 0
Size: 349850 Color: 3
Size: 104778 Color: 2

Bin 1208: 1 of cap free
Amount of items: 3
Items: 
Size: 545815 Color: 3
Size: 338074 Color: 0
Size: 116111 Color: 3

Bin 1209: 1 of cap free
Amount of items: 3
Items: 
Size: 546001 Color: 0
Size: 330465 Color: 3
Size: 123534 Color: 1

Bin 1210: 1 of cap free
Amount of items: 3
Items: 
Size: 546115 Color: 3
Size: 346746 Color: 1
Size: 107139 Color: 1

Bin 1211: 1 of cap free
Amount of items: 3
Items: 
Size: 546535 Color: 4
Size: 335239 Color: 2
Size: 118226 Color: 4

Bin 1212: 1 of cap free
Amount of items: 3
Items: 
Size: 546882 Color: 1
Size: 347081 Color: 4
Size: 106037 Color: 4

Bin 1213: 1 of cap free
Amount of items: 3
Items: 
Size: 546918 Color: 1
Size: 348931 Color: 0
Size: 104151 Color: 3

Bin 1214: 1 of cap free
Amount of items: 3
Items: 
Size: 547039 Color: 3
Size: 343680 Color: 2
Size: 109281 Color: 2

Bin 1215: 1 of cap free
Amount of items: 3
Items: 
Size: 547535 Color: 2
Size: 330494 Color: 1
Size: 121971 Color: 2

Bin 1216: 1 of cap free
Amount of items: 3
Items: 
Size: 547708 Color: 4
Size: 350751 Color: 2
Size: 101541 Color: 1

Bin 1217: 1 of cap free
Amount of items: 3
Items: 
Size: 547742 Color: 0
Size: 330429 Color: 3
Size: 121829 Color: 2

Bin 1218: 1 of cap free
Amount of items: 3
Items: 
Size: 547824 Color: 1
Size: 338629 Color: 4
Size: 113547 Color: 4

Bin 1219: 1 of cap free
Amount of items: 3
Items: 
Size: 548479 Color: 0
Size: 342363 Color: 4
Size: 109158 Color: 0

Bin 1220: 1 of cap free
Amount of items: 3
Items: 
Size: 549927 Color: 1
Size: 330330 Color: 3
Size: 119743 Color: 1

Bin 1221: 1 of cap free
Amount of items: 3
Items: 
Size: 550280 Color: 3
Size: 348633 Color: 0
Size: 101087 Color: 2

Bin 1222: 1 of cap free
Amount of items: 3
Items: 
Size: 550390 Color: 3
Size: 339859 Color: 2
Size: 109751 Color: 2

Bin 1223: 1 of cap free
Amount of items: 3
Items: 
Size: 552067 Color: 1
Size: 346852 Color: 2
Size: 101081 Color: 3

Bin 1224: 1 of cap free
Amount of items: 3
Items: 
Size: 552874 Color: 2
Size: 336676 Color: 4
Size: 110450 Color: 0

Bin 1225: 1 of cap free
Amount of items: 3
Items: 
Size: 554765 Color: 3
Size: 336391 Color: 0
Size: 108844 Color: 2

Bin 1226: 1 of cap free
Amount of items: 2
Items: 
Size: 555310 Color: 1
Size: 444690 Color: 4

Bin 1227: 1 of cap free
Amount of items: 3
Items: 
Size: 555787 Color: 3
Size: 339479 Color: 0
Size: 104734 Color: 1

Bin 1228: 1 of cap free
Amount of items: 3
Items: 
Size: 556656 Color: 0
Size: 342916 Color: 4
Size: 100428 Color: 4

Bin 1229: 1 of cap free
Amount of items: 3
Items: 
Size: 556732 Color: 0
Size: 334916 Color: 1
Size: 108352 Color: 0

Bin 1230: 1 of cap free
Amount of items: 3
Items: 
Size: 557509 Color: 1
Size: 340561 Color: 4
Size: 101930 Color: 4

Bin 1231: 1 of cap free
Amount of items: 3
Items: 
Size: 557845 Color: 0
Size: 341788 Color: 2
Size: 100367 Color: 3

Bin 1232: 1 of cap free
Amount of items: 3
Items: 
Size: 558510 Color: 0
Size: 337294 Color: 3
Size: 104196 Color: 4

Bin 1233: 1 of cap free
Amount of items: 2
Items: 
Size: 558717 Color: 1
Size: 441283 Color: 3

Bin 1234: 2 of cap free
Amount of items: 4
Items: 
Size: 411053 Color: 1
Size: 198057 Color: 0
Size: 195625 Color: 0
Size: 195264 Color: 4

Bin 1235: 2 of cap free
Amount of items: 4
Items: 
Size: 411295 Color: 0
Size: 198248 Color: 4
Size: 195398 Color: 3
Size: 195058 Color: 2

Bin 1236: 2 of cap free
Amount of items: 4
Items: 
Size: 414089 Color: 4
Size: 202266 Color: 3
Size: 191965 Color: 2
Size: 191679 Color: 1

Bin 1237: 2 of cap free
Amount of items: 4
Items: 
Size: 414935 Color: 2
Size: 207018 Color: 0
Size: 189756 Color: 2
Size: 188290 Color: 1

Bin 1238: 2 of cap free
Amount of items: 3
Items: 
Size: 441307 Color: 0
Size: 386529 Color: 1
Size: 172163 Color: 2

Bin 1239: 2 of cap free
Amount of items: 3
Items: 
Size: 443943 Color: 0
Size: 281243 Color: 3
Size: 274813 Color: 0

Bin 1240: 2 of cap free
Amount of items: 3
Items: 
Size: 444704 Color: 2
Size: 288623 Color: 4
Size: 266672 Color: 3

Bin 1241: 2 of cap free
Amount of items: 3
Items: 
Size: 445408 Color: 2
Size: 386755 Color: 4
Size: 167836 Color: 3

Bin 1242: 2 of cap free
Amount of items: 3
Items: 
Size: 446260 Color: 1
Size: 277655 Color: 0
Size: 276084 Color: 1

Bin 1243: 2 of cap free
Amount of items: 3
Items: 
Size: 447078 Color: 4
Size: 278387 Color: 3
Size: 274534 Color: 1

Bin 1244: 2 of cap free
Amount of items: 3
Items: 
Size: 447761 Color: 0
Size: 280561 Color: 3
Size: 271677 Color: 2

Bin 1245: 2 of cap free
Amount of items: 3
Items: 
Size: 447898 Color: 2
Size: 389119 Color: 0
Size: 162982 Color: 1

Bin 1246: 2 of cap free
Amount of items: 3
Items: 
Size: 448044 Color: 3
Size: 278828 Color: 1
Size: 273127 Color: 2

Bin 1247: 2 of cap free
Amount of items: 3
Items: 
Size: 448158 Color: 4
Size: 280892 Color: 3
Size: 270949 Color: 4

Bin 1248: 2 of cap free
Amount of items: 3
Items: 
Size: 448826 Color: 0
Size: 387240 Color: 4
Size: 163933 Color: 2

Bin 1249: 2 of cap free
Amount of items: 3
Items: 
Size: 450060 Color: 2
Size: 381315 Color: 4
Size: 168624 Color: 1

Bin 1250: 2 of cap free
Amount of items: 3
Items: 
Size: 450562 Color: 4
Size: 285642 Color: 3
Size: 263795 Color: 1

Bin 1251: 2 of cap free
Amount of items: 3
Items: 
Size: 450641 Color: 4
Size: 383199 Color: 1
Size: 166159 Color: 2

Bin 1252: 2 of cap free
Amount of items: 3
Items: 
Size: 450661 Color: 4
Size: 279667 Color: 2
Size: 269671 Color: 0

Bin 1253: 2 of cap free
Amount of items: 3
Items: 
Size: 452111 Color: 0
Size: 282761 Color: 2
Size: 265127 Color: 4

Bin 1254: 2 of cap free
Amount of items: 3
Items: 
Size: 452295 Color: 1
Size: 380489 Color: 4
Size: 167215 Color: 2

Bin 1255: 2 of cap free
Amount of items: 3
Items: 
Size: 452343 Color: 4
Size: 281304 Color: 3
Size: 266352 Color: 1

Bin 1256: 2 of cap free
Amount of items: 3
Items: 
Size: 454193 Color: 3
Size: 281439 Color: 0
Size: 264367 Color: 3

Bin 1257: 2 of cap free
Amount of items: 3
Items: 
Size: 455013 Color: 3
Size: 284834 Color: 4
Size: 260152 Color: 3

Bin 1258: 2 of cap free
Amount of items: 3
Items: 
Size: 455861 Color: 3
Size: 378741 Color: 0
Size: 165397 Color: 3

Bin 1259: 2 of cap free
Amount of items: 3
Items: 
Size: 456787 Color: 1
Size: 379879 Color: 4
Size: 163333 Color: 3

Bin 1260: 2 of cap free
Amount of items: 3
Items: 
Size: 456981 Color: 3
Size: 284604 Color: 1
Size: 258414 Color: 2

Bin 1261: 2 of cap free
Amount of items: 3
Items: 
Size: 457271 Color: 1
Size: 386185 Color: 2
Size: 156543 Color: 3

Bin 1262: 2 of cap free
Amount of items: 3
Items: 
Size: 457805 Color: 1
Size: 288791 Color: 2
Size: 253403 Color: 1

Bin 1263: 2 of cap free
Amount of items: 3
Items: 
Size: 458095 Color: 1
Size: 378708 Color: 0
Size: 163196 Color: 3

Bin 1264: 2 of cap free
Amount of items: 3
Items: 
Size: 458548 Color: 0
Size: 378049 Color: 1
Size: 163402 Color: 0

Bin 1265: 2 of cap free
Amount of items: 3
Items: 
Size: 459414 Color: 0
Size: 371805 Color: 1
Size: 168780 Color: 1

Bin 1266: 2 of cap free
Amount of items: 3
Items: 
Size: 459469 Color: 1
Size: 287731 Color: 4
Size: 252799 Color: 1

Bin 1267: 2 of cap free
Amount of items: 3
Items: 
Size: 459964 Color: 4
Size: 375050 Color: 1
Size: 164985 Color: 0

Bin 1268: 2 of cap free
Amount of items: 3
Items: 
Size: 461008 Color: 3
Size: 378157 Color: 1
Size: 160834 Color: 2

Bin 1269: 2 of cap free
Amount of items: 3
Items: 
Size: 462164 Color: 1
Size: 381377 Color: 4
Size: 156458 Color: 1

Bin 1270: 2 of cap free
Amount of items: 3
Items: 
Size: 462771 Color: 4
Size: 286730 Color: 2
Size: 250498 Color: 0

Bin 1271: 2 of cap free
Amount of items: 3
Items: 
Size: 462890 Color: 4
Size: 374122 Color: 0
Size: 162987 Color: 3

Bin 1272: 2 of cap free
Amount of items: 3
Items: 
Size: 463191 Color: 3
Size: 300222 Color: 2
Size: 236586 Color: 0

Bin 1273: 2 of cap free
Amount of items: 3
Items: 
Size: 463740 Color: 2
Size: 291140 Color: 0
Size: 245119 Color: 3

Bin 1274: 2 of cap free
Amount of items: 3
Items: 
Size: 463891 Color: 0
Size: 287008 Color: 3
Size: 249100 Color: 3

Bin 1275: 2 of cap free
Amount of items: 3
Items: 
Size: 464249 Color: 3
Size: 374079 Color: 4
Size: 161671 Color: 4

Bin 1276: 2 of cap free
Amount of items: 3
Items: 
Size: 464286 Color: 4
Size: 285750 Color: 0
Size: 249963 Color: 2

Bin 1277: 2 of cap free
Amount of items: 3
Items: 
Size: 464946 Color: 0
Size: 288543 Color: 4
Size: 246510 Color: 0

Bin 1278: 2 of cap free
Amount of items: 3
Items: 
Size: 465594 Color: 1
Size: 291327 Color: 3
Size: 243078 Color: 2

Bin 1279: 2 of cap free
Amount of items: 3
Items: 
Size: 465920 Color: 0
Size: 287582 Color: 3
Size: 246497 Color: 4

Bin 1280: 2 of cap free
Amount of items: 3
Items: 
Size: 467761 Color: 4
Size: 287014 Color: 3
Size: 245224 Color: 3

Bin 1281: 2 of cap free
Amount of items: 3
Items: 
Size: 467898 Color: 3
Size: 286674 Color: 4
Size: 245427 Color: 0

Bin 1282: 2 of cap free
Amount of items: 3
Items: 
Size: 468686 Color: 3
Size: 378066 Color: 1
Size: 153247 Color: 0

Bin 1283: 2 of cap free
Amount of items: 3
Items: 
Size: 469622 Color: 2
Size: 380735 Color: 4
Size: 149642 Color: 4

Bin 1284: 2 of cap free
Amount of items: 3
Items: 
Size: 469940 Color: 3
Size: 380756 Color: 4
Size: 149303 Color: 1

Bin 1285: 2 of cap free
Amount of items: 3
Items: 
Size: 470101 Color: 1
Size: 299352 Color: 3
Size: 230546 Color: 3

Bin 1286: 2 of cap free
Amount of items: 3
Items: 
Size: 470132 Color: 4
Size: 371646 Color: 0
Size: 158221 Color: 2

Bin 1287: 2 of cap free
Amount of items: 3
Items: 
Size: 471399 Color: 2
Size: 296329 Color: 1
Size: 232271 Color: 1

Bin 1288: 2 of cap free
Amount of items: 3
Items: 
Size: 471514 Color: 4
Size: 294585 Color: 3
Size: 233900 Color: 3

Bin 1289: 2 of cap free
Amount of items: 3
Items: 
Size: 471540 Color: 1
Size: 377513 Color: 4
Size: 150946 Color: 4

Bin 1290: 2 of cap free
Amount of items: 3
Items: 
Size: 471540 Color: 4
Size: 294864 Color: 3
Size: 233595 Color: 2

Bin 1291: 2 of cap free
Amount of items: 3
Items: 
Size: 472113 Color: 2
Size: 374868 Color: 1
Size: 153018 Color: 1

Bin 1292: 2 of cap free
Amount of items: 3
Items: 
Size: 472293 Color: 1
Size: 368612 Color: 4
Size: 159094 Color: 1

Bin 1293: 2 of cap free
Amount of items: 3
Items: 
Size: 474309 Color: 3
Size: 293075 Color: 2
Size: 232615 Color: 1

Bin 1294: 2 of cap free
Amount of items: 3
Items: 
Size: 474568 Color: 4
Size: 374542 Color: 1
Size: 150889 Color: 4

Bin 1295: 2 of cap free
Amount of items: 3
Items: 
Size: 475686 Color: 4
Size: 375807 Color: 3
Size: 148506 Color: 2

Bin 1296: 2 of cap free
Amount of items: 3
Items: 
Size: 477344 Color: 3
Size: 294295 Color: 2
Size: 228360 Color: 4

Bin 1297: 2 of cap free
Amount of items: 3
Items: 
Size: 477380 Color: 1
Size: 298302 Color: 0
Size: 224317 Color: 3

Bin 1298: 2 of cap free
Amount of items: 3
Items: 
Size: 478160 Color: 0
Size: 371193 Color: 1
Size: 150646 Color: 4

Bin 1299: 2 of cap free
Amount of items: 3
Items: 
Size: 478256 Color: 4
Size: 304155 Color: 1
Size: 217588 Color: 0

Bin 1300: 2 of cap free
Amount of items: 3
Items: 
Size: 479739 Color: 4
Size: 301896 Color: 2
Size: 218364 Color: 2

Bin 1301: 2 of cap free
Amount of items: 3
Items: 
Size: 479945 Color: 2
Size: 368598 Color: 4
Size: 151456 Color: 1

Bin 1302: 2 of cap free
Amount of items: 3
Items: 
Size: 479995 Color: 3
Size: 306586 Color: 4
Size: 213418 Color: 4

Bin 1303: 2 of cap free
Amount of items: 3
Items: 
Size: 480315 Color: 1
Size: 304298 Color: 3
Size: 215386 Color: 3

Bin 1304: 2 of cap free
Amount of items: 3
Items: 
Size: 480624 Color: 1
Size: 371238 Color: 4
Size: 148137 Color: 4

Bin 1305: 2 of cap free
Amount of items: 3
Items: 
Size: 480828 Color: 3
Size: 301016 Color: 1
Size: 218155 Color: 3

Bin 1306: 2 of cap free
Amount of items: 3
Items: 
Size: 481460 Color: 2
Size: 296849 Color: 1
Size: 221690 Color: 1

Bin 1307: 2 of cap free
Amount of items: 3
Items: 
Size: 482873 Color: 3
Size: 299331 Color: 4
Size: 217795 Color: 1

Bin 1308: 2 of cap free
Amount of items: 3
Items: 
Size: 483638 Color: 0
Size: 299346 Color: 2
Size: 217015 Color: 4

Bin 1309: 2 of cap free
Amount of items: 3
Items: 
Size: 484966 Color: 2
Size: 363546 Color: 3
Size: 151487 Color: 3

Bin 1310: 2 of cap free
Amount of items: 3
Items: 
Size: 485218 Color: 3
Size: 299635 Color: 2
Size: 215146 Color: 1

Bin 1311: 2 of cap free
Amount of items: 3
Items: 
Size: 485687 Color: 0
Size: 297931 Color: 4
Size: 216381 Color: 2

Bin 1312: 2 of cap free
Amount of items: 3
Items: 
Size: 485717 Color: 1
Size: 305086 Color: 4
Size: 209196 Color: 0

Bin 1313: 2 of cap free
Amount of items: 3
Items: 
Size: 485824 Color: 2
Size: 303858 Color: 1
Size: 210317 Color: 0

Bin 1314: 2 of cap free
Amount of items: 3
Items: 
Size: 485879 Color: 4
Size: 307370 Color: 1
Size: 206750 Color: 4

Bin 1315: 2 of cap free
Amount of items: 3
Items: 
Size: 488388 Color: 1
Size: 302500 Color: 3
Size: 209111 Color: 3

Bin 1316: 2 of cap free
Amount of items: 3
Items: 
Size: 488551 Color: 4
Size: 306360 Color: 0
Size: 205088 Color: 2

Bin 1317: 2 of cap free
Amount of items: 3
Items: 
Size: 488720 Color: 4
Size: 303584 Color: 1
Size: 207695 Color: 3

Bin 1318: 2 of cap free
Amount of items: 3
Items: 
Size: 489566 Color: 0
Size: 371432 Color: 3
Size: 139001 Color: 3

Bin 1319: 2 of cap free
Amount of items: 3
Items: 
Size: 490070 Color: 0
Size: 306090 Color: 2
Size: 203839 Color: 0

Bin 1320: 2 of cap free
Amount of items: 3
Items: 
Size: 491390 Color: 3
Size: 365929 Color: 0
Size: 142680 Color: 0

Bin 1321: 2 of cap free
Amount of items: 3
Items: 
Size: 491993 Color: 3
Size: 302970 Color: 1
Size: 205036 Color: 4

Bin 1322: 2 of cap free
Amount of items: 3
Items: 
Size: 493866 Color: 3
Size: 306708 Color: 2
Size: 199425 Color: 0

Bin 1323: 2 of cap free
Amount of items: 3
Items: 
Size: 494175 Color: 4
Size: 312243 Color: 3
Size: 193581 Color: 2

Bin 1324: 2 of cap free
Amount of items: 3
Items: 
Size: 495152 Color: 0
Size: 363185 Color: 3
Size: 141662 Color: 0

Bin 1325: 2 of cap free
Amount of items: 3
Items: 
Size: 495165 Color: 2
Size: 368459 Color: 0
Size: 136375 Color: 2

Bin 1326: 2 of cap free
Amount of items: 3
Items: 
Size: 495211 Color: 4
Size: 318159 Color: 3
Size: 186629 Color: 2

Bin 1327: 2 of cap free
Amount of items: 3
Items: 
Size: 496021 Color: 1
Size: 364991 Color: 2
Size: 138987 Color: 1

Bin 1328: 2 of cap free
Amount of items: 3
Items: 
Size: 496643 Color: 0
Size: 366190 Color: 4
Size: 137166 Color: 0

Bin 1329: 2 of cap free
Amount of items: 3
Items: 
Size: 498394 Color: 3
Size: 363356 Color: 4
Size: 138249 Color: 4

Bin 1330: 2 of cap free
Amount of items: 3
Items: 
Size: 499194 Color: 4
Size: 366361 Color: 2
Size: 134444 Color: 0

Bin 1331: 2 of cap free
Amount of items: 3
Items: 
Size: 499918 Color: 1
Size: 360369 Color: 4
Size: 139712 Color: 0

Bin 1332: 2 of cap free
Amount of items: 3
Items: 
Size: 500230 Color: 1
Size: 318424 Color: 3
Size: 181345 Color: 1

Bin 1333: 2 of cap free
Amount of items: 3
Items: 
Size: 501080 Color: 0
Size: 363346 Color: 4
Size: 135573 Color: 1

Bin 1334: 2 of cap free
Amount of items: 3
Items: 
Size: 501631 Color: 2
Size: 367186 Color: 1
Size: 131182 Color: 3

Bin 1335: 2 of cap free
Amount of items: 3
Items: 
Size: 502287 Color: 4
Size: 308045 Color: 0
Size: 189667 Color: 1

Bin 1336: 2 of cap free
Amount of items: 3
Items: 
Size: 502531 Color: 1
Size: 362014 Color: 0
Size: 135454 Color: 2

Bin 1337: 2 of cap free
Amount of items: 3
Items: 
Size: 503033 Color: 2
Size: 311459 Color: 1
Size: 185507 Color: 2

Bin 1338: 2 of cap free
Amount of items: 3
Items: 
Size: 505150 Color: 1
Size: 306436 Color: 4
Size: 188413 Color: 3

Bin 1339: 2 of cap free
Amount of items: 3
Items: 
Size: 505636 Color: 4
Size: 307071 Color: 2
Size: 187292 Color: 1

Bin 1340: 2 of cap free
Amount of items: 3
Items: 
Size: 505719 Color: 4
Size: 362686 Color: 2
Size: 131594 Color: 0

Bin 1341: 2 of cap free
Amount of items: 3
Items: 
Size: 506202 Color: 0
Size: 307359 Color: 3
Size: 186438 Color: 1

Bin 1342: 2 of cap free
Amount of items: 3
Items: 
Size: 506418 Color: 1
Size: 360863 Color: 3
Size: 132718 Color: 3

Bin 1343: 2 of cap free
Amount of items: 3
Items: 
Size: 507032 Color: 2
Size: 314831 Color: 1
Size: 178136 Color: 3

Bin 1344: 2 of cap free
Amount of items: 3
Items: 
Size: 508495 Color: 1
Size: 313412 Color: 0
Size: 178092 Color: 1

Bin 1345: 2 of cap free
Amount of items: 3
Items: 
Size: 510315 Color: 4
Size: 319365 Color: 3
Size: 170319 Color: 0

Bin 1346: 2 of cap free
Amount of items: 3
Items: 
Size: 511267 Color: 1
Size: 310167 Color: 0
Size: 178565 Color: 0

Bin 1347: 2 of cap free
Amount of items: 3
Items: 
Size: 511510 Color: 1
Size: 312129 Color: 4
Size: 176360 Color: 3

Bin 1348: 2 of cap free
Amount of items: 3
Items: 
Size: 511547 Color: 4
Size: 358784 Color: 2
Size: 129668 Color: 2

Bin 1349: 2 of cap free
Amount of items: 3
Items: 
Size: 512057 Color: 4
Size: 309090 Color: 0
Size: 178852 Color: 2

Bin 1350: 2 of cap free
Amount of items: 3
Items: 
Size: 512387 Color: 2
Size: 309900 Color: 4
Size: 177712 Color: 0

Bin 1351: 2 of cap free
Amount of items: 3
Items: 
Size: 512850 Color: 4
Size: 314406 Color: 0
Size: 172743 Color: 2

Bin 1352: 2 of cap free
Amount of items: 3
Items: 
Size: 515463 Color: 4
Size: 358395 Color: 1
Size: 126141 Color: 4

Bin 1353: 2 of cap free
Amount of items: 3
Items: 
Size: 515735 Color: 1
Size: 354973 Color: 3
Size: 129291 Color: 3

Bin 1354: 2 of cap free
Amount of items: 2
Items: 
Size: 516176 Color: 0
Size: 483823 Color: 3

Bin 1355: 2 of cap free
Amount of items: 3
Items: 
Size: 516465 Color: 2
Size: 360009 Color: 0
Size: 123525 Color: 0

Bin 1356: 2 of cap free
Amount of items: 3
Items: 
Size: 516682 Color: 0
Size: 357158 Color: 4
Size: 126159 Color: 0

Bin 1357: 2 of cap free
Amount of items: 3
Items: 
Size: 518363 Color: 1
Size: 352120 Color: 3
Size: 129516 Color: 4

Bin 1358: 2 of cap free
Amount of items: 3
Items: 
Size: 519233 Color: 3
Size: 363943 Color: 4
Size: 116823 Color: 1

Bin 1359: 2 of cap free
Amount of items: 3
Items: 
Size: 519841 Color: 3
Size: 352747 Color: 0
Size: 127411 Color: 3

Bin 1360: 2 of cap free
Amount of items: 3
Items: 
Size: 519844 Color: 0
Size: 323996 Color: 2
Size: 156159 Color: 1

Bin 1361: 2 of cap free
Amount of items: 3
Items: 
Size: 520338 Color: 4
Size: 356578 Color: 1
Size: 123083 Color: 4

Bin 1362: 2 of cap free
Amount of items: 3
Items: 
Size: 520362 Color: 1
Size: 318251 Color: 0
Size: 161386 Color: 4

Bin 1363: 2 of cap free
Amount of items: 3
Items: 
Size: 520641 Color: 0
Size: 317508 Color: 2
Size: 161850 Color: 4

Bin 1364: 2 of cap free
Amount of items: 3
Items: 
Size: 521341 Color: 2
Size: 321965 Color: 0
Size: 156693 Color: 4

Bin 1365: 2 of cap free
Amount of items: 2
Items: 
Size: 522555 Color: 3
Size: 477444 Color: 0

Bin 1366: 2 of cap free
Amount of items: 3
Items: 
Size: 525673 Color: 3
Size: 316368 Color: 2
Size: 157958 Color: 0

Bin 1367: 2 of cap free
Amount of items: 3
Items: 
Size: 526554 Color: 0
Size: 355689 Color: 2
Size: 117756 Color: 4

Bin 1368: 2 of cap free
Amount of items: 3
Items: 
Size: 528185 Color: 2
Size: 320332 Color: 0
Size: 151482 Color: 4

Bin 1369: 2 of cap free
Amount of items: 3
Items: 
Size: 528476 Color: 0
Size: 320296 Color: 2
Size: 151227 Color: 3

Bin 1370: 2 of cap free
Amount of items: 3
Items: 
Size: 529247 Color: 4
Size: 320592 Color: 2
Size: 150160 Color: 4

Bin 1371: 2 of cap free
Amount of items: 3
Items: 
Size: 529494 Color: 4
Size: 326348 Color: 0
Size: 144157 Color: 4

Bin 1372: 2 of cap free
Amount of items: 3
Items: 
Size: 529539 Color: 3
Size: 352172 Color: 0
Size: 118288 Color: 1

Bin 1373: 2 of cap free
Amount of items: 3
Items: 
Size: 531528 Color: 1
Size: 324615 Color: 0
Size: 143856 Color: 0

Bin 1374: 2 of cap free
Amount of items: 3
Items: 
Size: 532832 Color: 1
Size: 352155 Color: 2
Size: 115012 Color: 3

Bin 1375: 2 of cap free
Amount of items: 3
Items: 
Size: 532909 Color: 3
Size: 322822 Color: 0
Size: 144268 Color: 2

Bin 1376: 2 of cap free
Amount of items: 3
Items: 
Size: 533438 Color: 1
Size: 323390 Color: 3
Size: 143171 Color: 2

Bin 1377: 2 of cap free
Amount of items: 3
Items: 
Size: 534799 Color: 3
Size: 326944 Color: 0
Size: 138256 Color: 4

Bin 1378: 2 of cap free
Amount of items: 3
Items: 
Size: 535212 Color: 4
Size: 329962 Color: 1
Size: 134825 Color: 2

Bin 1379: 2 of cap free
Amount of items: 3
Items: 
Size: 536158 Color: 1
Size: 331098 Color: 0
Size: 132743 Color: 0

Bin 1380: 2 of cap free
Amount of items: 3
Items: 
Size: 536576 Color: 2
Size: 325510 Color: 4
Size: 137913 Color: 2

Bin 1381: 2 of cap free
Amount of items: 3
Items: 
Size: 536664 Color: 0
Size: 334829 Color: 4
Size: 128506 Color: 2

Bin 1382: 2 of cap free
Amount of items: 3
Items: 
Size: 536705 Color: 2
Size: 346268 Color: 4
Size: 117026 Color: 2

Bin 1383: 2 of cap free
Amount of items: 3
Items: 
Size: 536861 Color: 0
Size: 350131 Color: 2
Size: 113007 Color: 1

Bin 1384: 2 of cap free
Amount of items: 3
Items: 
Size: 536962 Color: 1
Size: 353362 Color: 2
Size: 109675 Color: 3

Bin 1385: 2 of cap free
Amount of items: 3
Items: 
Size: 537273 Color: 2
Size: 327327 Color: 3
Size: 135399 Color: 0

Bin 1386: 2 of cap free
Amount of items: 3
Items: 
Size: 538087 Color: 3
Size: 355962 Color: 2
Size: 105950 Color: 0

Bin 1387: 2 of cap free
Amount of items: 3
Items: 
Size: 539677 Color: 2
Size: 343611 Color: 0
Size: 116711 Color: 0

Bin 1388: 2 of cap free
Amount of items: 3
Items: 
Size: 541257 Color: 1
Size: 326890 Color: 2
Size: 131852 Color: 3

Bin 1389: 2 of cap free
Amount of items: 3
Items: 
Size: 542235 Color: 2
Size: 340448 Color: 4
Size: 117316 Color: 2

Bin 1390: 2 of cap free
Amount of items: 3
Items: 
Size: 542546 Color: 1
Size: 329262 Color: 4
Size: 128191 Color: 0

Bin 1391: 2 of cap free
Amount of items: 3
Items: 
Size: 542602 Color: 4
Size: 329705 Color: 0
Size: 127692 Color: 4

Bin 1392: 2 of cap free
Amount of items: 3
Items: 
Size: 542734 Color: 2
Size: 327668 Color: 1
Size: 129597 Color: 4

Bin 1393: 2 of cap free
Amount of items: 3
Items: 
Size: 544246 Color: 3
Size: 333894 Color: 4
Size: 121859 Color: 0

Bin 1394: 2 of cap free
Amount of items: 3
Items: 
Size: 544343 Color: 1
Size: 334450 Color: 0
Size: 121206 Color: 2

Bin 1395: 2 of cap free
Amount of items: 3
Items: 
Size: 545847 Color: 1
Size: 335814 Color: 4
Size: 118338 Color: 2

Bin 1396: 2 of cap free
Amount of items: 3
Items: 
Size: 546188 Color: 2
Size: 342094 Color: 1
Size: 111717 Color: 0

Bin 1397: 2 of cap free
Amount of items: 3
Items: 
Size: 546780 Color: 1
Size: 349365 Color: 3
Size: 103854 Color: 3

Bin 1398: 2 of cap free
Amount of items: 3
Items: 
Size: 547597 Color: 4
Size: 341082 Color: 2
Size: 111320 Color: 1

Bin 1399: 2 of cap free
Amount of items: 3
Items: 
Size: 547923 Color: 4
Size: 335876 Color: 1
Size: 116200 Color: 4

Bin 1400: 2 of cap free
Amount of items: 3
Items: 
Size: 547995 Color: 1
Size: 338592 Color: 3
Size: 113412 Color: 3

Bin 1401: 2 of cap free
Amount of items: 3
Items: 
Size: 548218 Color: 1
Size: 346369 Color: 0
Size: 105412 Color: 0

Bin 1402: 2 of cap free
Amount of items: 3
Items: 
Size: 548981 Color: 1
Size: 349947 Color: 0
Size: 101071 Color: 0

Bin 1403: 2 of cap free
Amount of items: 3
Items: 
Size: 550985 Color: 2
Size: 331171 Color: 1
Size: 117843 Color: 0

Bin 1404: 3 of cap free
Amount of items: 4
Items: 
Size: 415007 Color: 0
Size: 205749 Color: 2
Size: 190568 Color: 4
Size: 188674 Color: 0

Bin 1405: 3 of cap free
Amount of items: 3
Items: 
Size: 421978 Color: 1
Size: 394663 Color: 3
Size: 183357 Color: 4

Bin 1406: 3 of cap free
Amount of items: 3
Items: 
Size: 423671 Color: 4
Size: 394073 Color: 3
Size: 182254 Color: 1

Bin 1407: 3 of cap free
Amount of items: 3
Items: 
Size: 429806 Color: 3
Size: 391663 Color: 1
Size: 178529 Color: 3

Bin 1408: 3 of cap free
Amount of items: 3
Items: 
Size: 441346 Color: 1
Size: 386174 Color: 0
Size: 172478 Color: 1

Bin 1409: 3 of cap free
Amount of items: 3
Items: 
Size: 442601 Color: 2
Size: 385809 Color: 3
Size: 171588 Color: 2

Bin 1410: 3 of cap free
Amount of items: 3
Items: 
Size: 443256 Color: 1
Size: 384003 Color: 2
Size: 172739 Color: 4

Bin 1411: 3 of cap free
Amount of items: 3
Items: 
Size: 445871 Color: 0
Size: 389572 Color: 2
Size: 164555 Color: 4

Bin 1412: 3 of cap free
Amount of items: 3
Items: 
Size: 446166 Color: 0
Size: 384101 Color: 2
Size: 169731 Color: 0

Bin 1413: 3 of cap free
Amount of items: 3
Items: 
Size: 446613 Color: 2
Size: 277708 Color: 0
Size: 275677 Color: 1

Bin 1414: 3 of cap free
Amount of items: 3
Items: 
Size: 446913 Color: 2
Size: 284050 Color: 4
Size: 269035 Color: 1

Bin 1415: 3 of cap free
Amount of items: 3
Items: 
Size: 447304 Color: 4
Size: 385391 Color: 1
Size: 167303 Color: 2

Bin 1416: 3 of cap free
Amount of items: 3
Items: 
Size: 447716 Color: 4
Size: 281070 Color: 0
Size: 271212 Color: 4

Bin 1417: 3 of cap free
Amount of items: 3
Items: 
Size: 447732 Color: 2
Size: 286470 Color: 3
Size: 265796 Color: 1

Bin 1418: 3 of cap free
Amount of items: 3
Items: 
Size: 449003 Color: 2
Size: 384463 Color: 1
Size: 166532 Color: 2

Bin 1419: 3 of cap free
Amount of items: 3
Items: 
Size: 449813 Color: 4
Size: 278519 Color: 0
Size: 271666 Color: 1

Bin 1420: 3 of cap free
Amount of items: 3
Items: 
Size: 449818 Color: 1
Size: 277680 Color: 2
Size: 272500 Color: 4

Bin 1421: 3 of cap free
Amount of items: 3
Items: 
Size: 449981 Color: 4
Size: 281395 Color: 0
Size: 268622 Color: 2

Bin 1422: 3 of cap free
Amount of items: 3
Items: 
Size: 450673 Color: 2
Size: 381719 Color: 1
Size: 167606 Color: 2

Bin 1423: 3 of cap free
Amount of items: 3
Items: 
Size: 452115 Color: 3
Size: 383758 Color: 2
Size: 164125 Color: 1

Bin 1424: 3 of cap free
Amount of items: 3
Items: 
Size: 452458 Color: 2
Size: 382101 Color: 0
Size: 165439 Color: 0

Bin 1425: 3 of cap free
Amount of items: 3
Items: 
Size: 453395 Color: 3
Size: 282501 Color: 2
Size: 264102 Color: 2

Bin 1426: 3 of cap free
Amount of items: 3
Items: 
Size: 455301 Color: 2
Size: 284711 Color: 0
Size: 259986 Color: 4

Bin 1427: 3 of cap free
Amount of items: 3
Items: 
Size: 456074 Color: 3
Size: 282650 Color: 1
Size: 261274 Color: 3

Bin 1428: 3 of cap free
Amount of items: 3
Items: 
Size: 457061 Color: 1
Size: 283696 Color: 4
Size: 259241 Color: 0

Bin 1429: 3 of cap free
Amount of items: 3
Items: 
Size: 459353 Color: 3
Size: 380402 Color: 4
Size: 160243 Color: 3

Bin 1430: 3 of cap free
Amount of items: 3
Items: 
Size: 459461 Color: 3
Size: 376555 Color: 4
Size: 163982 Color: 2

Bin 1431: 3 of cap free
Amount of items: 3
Items: 
Size: 459938 Color: 4
Size: 286410 Color: 0
Size: 253650 Color: 0

Bin 1432: 3 of cap free
Amount of items: 3
Items: 
Size: 460075 Color: 0
Size: 287598 Color: 2
Size: 252325 Color: 3

Bin 1433: 3 of cap free
Amount of items: 3
Items: 
Size: 460196 Color: 1
Size: 287385 Color: 2
Size: 252417 Color: 3

Bin 1434: 3 of cap free
Amount of items: 3
Items: 
Size: 460327 Color: 0
Size: 376564 Color: 4
Size: 163107 Color: 1

Bin 1435: 3 of cap free
Amount of items: 3
Items: 
Size: 461475 Color: 1
Size: 288357 Color: 4
Size: 250166 Color: 1

Bin 1436: 3 of cap free
Amount of items: 3
Items: 
Size: 462121 Color: 4
Size: 287808 Color: 3
Size: 250069 Color: 2

Bin 1437: 3 of cap free
Amount of items: 3
Items: 
Size: 462863 Color: 2
Size: 288086 Color: 1
Size: 249049 Color: 2

Bin 1438: 3 of cap free
Amount of items: 3
Items: 
Size: 462990 Color: 2
Size: 300665 Color: 1
Size: 236343 Color: 4

Bin 1439: 3 of cap free
Amount of items: 3
Items: 
Size: 464906 Color: 0
Size: 377647 Color: 2
Size: 157445 Color: 3

Bin 1440: 3 of cap free
Amount of items: 3
Items: 
Size: 465700 Color: 2
Size: 371567 Color: 1
Size: 162731 Color: 3

Bin 1441: 3 of cap free
Amount of items: 3
Items: 
Size: 466058 Color: 4
Size: 370249 Color: 1
Size: 163691 Color: 3

Bin 1442: 3 of cap free
Amount of items: 3
Items: 
Size: 469695 Color: 1
Size: 375176 Color: 3
Size: 155127 Color: 0

Bin 1443: 3 of cap free
Amount of items: 3
Items: 
Size: 469774 Color: 0
Size: 291541 Color: 4
Size: 238683 Color: 0

Bin 1444: 3 of cap free
Amount of items: 3
Items: 
Size: 470080 Color: 2
Size: 297404 Color: 0
Size: 232514 Color: 0

Bin 1445: 3 of cap free
Amount of items: 3
Items: 
Size: 470176 Color: 3
Size: 378842 Color: 2
Size: 150980 Color: 2

Bin 1446: 3 of cap free
Amount of items: 3
Items: 
Size: 470757 Color: 0
Size: 291522 Color: 4
Size: 237719 Color: 3

Bin 1447: 3 of cap free
Amount of items: 3
Items: 
Size: 471640 Color: 2
Size: 374768 Color: 4
Size: 153590 Color: 1

Bin 1448: 3 of cap free
Amount of items: 3
Items: 
Size: 471962 Color: 3
Size: 297803 Color: 2
Size: 230233 Color: 4

Bin 1449: 3 of cap free
Amount of items: 3
Items: 
Size: 472854 Color: 1
Size: 293774 Color: 0
Size: 233370 Color: 2

Bin 1450: 3 of cap free
Amount of items: 3
Items: 
Size: 474288 Color: 2
Size: 298680 Color: 0
Size: 227030 Color: 1

Bin 1451: 3 of cap free
Amount of items: 3
Items: 
Size: 475806 Color: 4
Size: 371795 Color: 3
Size: 152397 Color: 3

Bin 1452: 3 of cap free
Amount of items: 3
Items: 
Size: 479492 Color: 1
Size: 305562 Color: 3
Size: 214944 Color: 4

Bin 1453: 3 of cap free
Amount of items: 3
Items: 
Size: 480463 Color: 2
Size: 298594 Color: 0
Size: 220941 Color: 1

Bin 1454: 3 of cap free
Amount of items: 3
Items: 
Size: 481695 Color: 2
Size: 316420 Color: 4
Size: 201883 Color: 3

Bin 1455: 3 of cap free
Amount of items: 3
Items: 
Size: 481774 Color: 3
Size: 305472 Color: 1
Size: 212752 Color: 3

Bin 1456: 3 of cap free
Amount of items: 3
Items: 
Size: 482339 Color: 1
Size: 304629 Color: 2
Size: 213030 Color: 1

Bin 1457: 3 of cap free
Amount of items: 3
Items: 
Size: 483056 Color: 2
Size: 371386 Color: 0
Size: 145556 Color: 3

Bin 1458: 3 of cap free
Amount of items: 3
Items: 
Size: 483272 Color: 2
Size: 303150 Color: 3
Size: 213576 Color: 2

Bin 1459: 3 of cap free
Amount of items: 3
Items: 
Size: 483853 Color: 0
Size: 303006 Color: 2
Size: 213139 Color: 2

Bin 1460: 3 of cap free
Amount of items: 3
Items: 
Size: 484294 Color: 1
Size: 299620 Color: 3
Size: 216084 Color: 2

Bin 1461: 3 of cap free
Amount of items: 3
Items: 
Size: 484548 Color: 4
Size: 372213 Color: 0
Size: 143237 Color: 2

Bin 1462: 3 of cap free
Amount of items: 3
Items: 
Size: 484717 Color: 2
Size: 298721 Color: 0
Size: 216560 Color: 1

Bin 1463: 3 of cap free
Amount of items: 3
Items: 
Size: 486127 Color: 0
Size: 369897 Color: 4
Size: 143974 Color: 1

Bin 1464: 3 of cap free
Amount of items: 3
Items: 
Size: 487277 Color: 2
Size: 306633 Color: 0
Size: 206088 Color: 1

Bin 1465: 3 of cap free
Amount of items: 3
Items: 
Size: 487399 Color: 0
Size: 305651 Color: 2
Size: 206948 Color: 3

Bin 1466: 3 of cap free
Amount of items: 3
Items: 
Size: 488460 Color: 0
Size: 305002 Color: 2
Size: 206536 Color: 1

Bin 1467: 3 of cap free
Amount of items: 3
Items: 
Size: 490105 Color: 3
Size: 370308 Color: 1
Size: 139585 Color: 2

Bin 1468: 3 of cap free
Amount of items: 3
Items: 
Size: 495306 Color: 2
Size: 369831 Color: 0
Size: 134861 Color: 2

Bin 1469: 3 of cap free
Amount of items: 3
Items: 
Size: 495784 Color: 0
Size: 349032 Color: 1
Size: 155182 Color: 4

Bin 1470: 3 of cap free
Amount of items: 3
Items: 
Size: 496662 Color: 3
Size: 311598 Color: 4
Size: 191738 Color: 2

Bin 1471: 3 of cap free
Amount of items: 3
Items: 
Size: 496991 Color: 4
Size: 328515 Color: 1
Size: 174492 Color: 4

Bin 1472: 3 of cap free
Amount of items: 3
Items: 
Size: 497892 Color: 2
Size: 364936 Color: 3
Size: 137170 Color: 2

Bin 1473: 3 of cap free
Amount of items: 3
Items: 
Size: 498094 Color: 4
Size: 367031 Color: 0
Size: 134873 Color: 4

Bin 1474: 3 of cap free
Amount of items: 3
Items: 
Size: 498548 Color: 3
Size: 367000 Color: 4
Size: 134450 Color: 4

Bin 1475: 3 of cap free
Amount of items: 3
Items: 
Size: 498678 Color: 2
Size: 364621 Color: 0
Size: 136699 Color: 0

Bin 1476: 3 of cap free
Amount of items: 3
Items: 
Size: 499748 Color: 2
Size: 310866 Color: 0
Size: 189384 Color: 2

Bin 1477: 3 of cap free
Amount of items: 3
Items: 
Size: 500544 Color: 3
Size: 362086 Color: 2
Size: 137368 Color: 3

Bin 1478: 3 of cap free
Amount of items: 3
Items: 
Size: 507817 Color: 4
Size: 312385 Color: 2
Size: 179796 Color: 2

Bin 1479: 3 of cap free
Amount of items: 3
Items: 
Size: 508529 Color: 2
Size: 313617 Color: 0
Size: 177852 Color: 0

Bin 1480: 3 of cap free
Amount of items: 3
Items: 
Size: 509242 Color: 4
Size: 313298 Color: 0
Size: 177458 Color: 4

Bin 1481: 3 of cap free
Amount of items: 3
Items: 
Size: 509429 Color: 1
Size: 318862 Color: 4
Size: 171707 Color: 1

Bin 1482: 3 of cap free
Amount of items: 3
Items: 
Size: 510307 Color: 4
Size: 312203 Color: 3
Size: 177488 Color: 2

Bin 1483: 3 of cap free
Amount of items: 3
Items: 
Size: 511574 Color: 3
Size: 322684 Color: 1
Size: 165740 Color: 3

Bin 1484: 3 of cap free
Amount of items: 3
Items: 
Size: 513804 Color: 3
Size: 360304 Color: 2
Size: 125890 Color: 4

Bin 1485: 3 of cap free
Amount of items: 3
Items: 
Size: 514906 Color: 2
Size: 363310 Color: 0
Size: 121782 Color: 1

Bin 1486: 3 of cap free
Amount of items: 3
Items: 
Size: 514984 Color: 3
Size: 353455 Color: 1
Size: 131559 Color: 2

Bin 1487: 3 of cap free
Amount of items: 3
Items: 
Size: 516743 Color: 1
Size: 361625 Color: 4
Size: 121630 Color: 2

Bin 1488: 3 of cap free
Amount of items: 3
Items: 
Size: 516950 Color: 2
Size: 359694 Color: 0
Size: 123354 Color: 0

Bin 1489: 3 of cap free
Amount of items: 3
Items: 
Size: 519019 Color: 0
Size: 357476 Color: 4
Size: 123503 Color: 4

Bin 1490: 3 of cap free
Amount of items: 3
Items: 
Size: 520861 Color: 2
Size: 324183 Color: 0
Size: 154954 Color: 3

Bin 1491: 3 of cap free
Amount of items: 3
Items: 
Size: 529996 Color: 3
Size: 353825 Color: 1
Size: 116177 Color: 0

Bin 1492: 3 of cap free
Amount of items: 3
Items: 
Size: 530076 Color: 1
Size: 324994 Color: 0
Size: 144928 Color: 4

Bin 1493: 3 of cap free
Amount of items: 3
Items: 
Size: 535142 Color: 2
Size: 350391 Color: 4
Size: 114465 Color: 4

Bin 1494: 3 of cap free
Amount of items: 3
Items: 
Size: 535158 Color: 3
Size: 352143 Color: 2
Size: 112697 Color: 0

Bin 1495: 3 of cap free
Amount of items: 3
Items: 
Size: 535254 Color: 3
Size: 356495 Color: 1
Size: 108249 Color: 4

Bin 1496: 3 of cap free
Amount of items: 3
Items: 
Size: 541062 Color: 4
Size: 337518 Color: 1
Size: 121418 Color: 0

Bin 1497: 3 of cap free
Amount of items: 3
Items: 
Size: 541257 Color: 1
Size: 349619 Color: 0
Size: 109122 Color: 4

Bin 1498: 3 of cap free
Amount of items: 3
Items: 
Size: 543931 Color: 0
Size: 334975 Color: 4
Size: 121092 Color: 4

Bin 1499: 3 of cap free
Amount of items: 3
Items: 
Size: 545228 Color: 0
Size: 330570 Color: 4
Size: 124200 Color: 3

Bin 1500: 3 of cap free
Amount of items: 3
Items: 
Size: 545472 Color: 4
Size: 330145 Color: 3
Size: 124381 Color: 3

Bin 1501: 3 of cap free
Amount of items: 3
Items: 
Size: 549272 Color: 4
Size: 333182 Color: 2
Size: 117544 Color: 1

Bin 1502: 3 of cap free
Amount of items: 3
Items: 
Size: 549587 Color: 3
Size: 348054 Color: 1
Size: 102357 Color: 2

Bin 1503: 4 of cap free
Amount of items: 4
Items: 
Size: 412041 Color: 1
Size: 199228 Color: 4
Size: 194530 Color: 4
Size: 194198 Color: 2

Bin 1504: 4 of cap free
Amount of items: 4
Items: 
Size: 414581 Color: 0
Size: 204313 Color: 4
Size: 191569 Color: 0
Size: 189534 Color: 4

Bin 1505: 4 of cap free
Amount of items: 4
Items: 
Size: 414923 Color: 3
Size: 206845 Color: 2
Size: 189481 Color: 1
Size: 188748 Color: 0

Bin 1506: 4 of cap free
Amount of items: 3
Items: 
Size: 416753 Color: 3
Size: 396792 Color: 4
Size: 186452 Color: 4

Bin 1507: 4 of cap free
Amount of items: 3
Items: 
Size: 428108 Color: 3
Size: 392413 Color: 4
Size: 179476 Color: 4

Bin 1508: 4 of cap free
Amount of items: 3
Items: 
Size: 429879 Color: 3
Size: 391640 Color: 1
Size: 178478 Color: 4

Bin 1509: 4 of cap free
Amount of items: 3
Items: 
Size: 442878 Color: 1
Size: 386814 Color: 3
Size: 170305 Color: 3

Bin 1510: 4 of cap free
Amount of items: 3
Items: 
Size: 443261 Color: 1
Size: 385033 Color: 4
Size: 171703 Color: 3

Bin 1511: 4 of cap free
Amount of items: 3
Items: 
Size: 445194 Color: 0
Size: 385155 Color: 4
Size: 169648 Color: 0

Bin 1512: 4 of cap free
Amount of items: 3
Items: 
Size: 457416 Color: 3
Size: 380560 Color: 2
Size: 162021 Color: 2

Bin 1513: 4 of cap free
Amount of items: 3
Items: 
Size: 459584 Color: 2
Size: 292782 Color: 0
Size: 247631 Color: 3

Bin 1514: 4 of cap free
Amount of items: 3
Items: 
Size: 459723 Color: 3
Size: 383736 Color: 2
Size: 156538 Color: 3

Bin 1515: 4 of cap free
Amount of items: 3
Items: 
Size: 460637 Color: 1
Size: 377151 Color: 0
Size: 162209 Color: 4

Bin 1516: 4 of cap free
Amount of items: 3
Items: 
Size: 463231 Color: 2
Size: 376693 Color: 4
Size: 160073 Color: 3

Bin 1517: 4 of cap free
Amount of items: 3
Items: 
Size: 466917 Color: 4
Size: 288098 Color: 1
Size: 244982 Color: 0

Bin 1518: 4 of cap free
Amount of items: 3
Items: 
Size: 468426 Color: 1
Size: 376447 Color: 2
Size: 155124 Color: 0

Bin 1519: 4 of cap free
Amount of items: 3
Items: 
Size: 468778 Color: 3
Size: 289173 Color: 1
Size: 242046 Color: 0

Bin 1520: 4 of cap free
Amount of items: 3
Items: 
Size: 469132 Color: 4
Size: 288727 Color: 0
Size: 242138 Color: 3

Bin 1521: 4 of cap free
Amount of items: 3
Items: 
Size: 469951 Color: 3
Size: 297812 Color: 4
Size: 232234 Color: 1

Bin 1522: 4 of cap free
Amount of items: 3
Items: 
Size: 471391 Color: 0
Size: 294017 Color: 4
Size: 234589 Color: 0

Bin 1523: 4 of cap free
Amount of items: 3
Items: 
Size: 475026 Color: 2
Size: 375099 Color: 1
Size: 149872 Color: 0

Bin 1524: 4 of cap free
Amount of items: 3
Items: 
Size: 477026 Color: 4
Size: 294520 Color: 1
Size: 228451 Color: 3

Bin 1525: 4 of cap free
Amount of items: 3
Items: 
Size: 479993 Color: 4
Size: 296760 Color: 0
Size: 223244 Color: 1

Bin 1526: 4 of cap free
Amount of items: 3
Items: 
Size: 483307 Color: 4
Size: 305421 Color: 3
Size: 211269 Color: 4

Bin 1527: 4 of cap free
Amount of items: 3
Items: 
Size: 489905 Color: 2
Size: 307877 Color: 0
Size: 202215 Color: 4

Bin 1528: 4 of cap free
Amount of items: 3
Items: 
Size: 490289 Color: 3
Size: 304500 Color: 0
Size: 205208 Color: 0

Bin 1529: 4 of cap free
Amount of items: 3
Items: 
Size: 494166 Color: 3
Size: 376864 Color: 4
Size: 128967 Color: 4

Bin 1530: 4 of cap free
Amount of items: 3
Items: 
Size: 495170 Color: 1
Size: 361357 Color: 2
Size: 143470 Color: 0

Bin 1531: 4 of cap free
Amount of items: 3
Items: 
Size: 495518 Color: 3
Size: 371308 Color: 4
Size: 133171 Color: 4

Bin 1532: 4 of cap free
Amount of items: 3
Items: 
Size: 496778 Color: 1
Size: 307019 Color: 0
Size: 196200 Color: 3

Bin 1533: 4 of cap free
Amount of items: 3
Items: 
Size: 497552 Color: 1
Size: 350292 Color: 2
Size: 152153 Color: 0

Bin 1534: 4 of cap free
Amount of items: 3
Items: 
Size: 498842 Color: 4
Size: 359937 Color: 3
Size: 141218 Color: 2

Bin 1535: 4 of cap free
Amount of items: 3
Items: 
Size: 499092 Color: 0
Size: 365095 Color: 3
Size: 135810 Color: 3

Bin 1536: 4 of cap free
Amount of items: 3
Items: 
Size: 500261 Color: 3
Size: 307339 Color: 2
Size: 192397 Color: 2

Bin 1537: 4 of cap free
Amount of items: 3
Items: 
Size: 514302 Color: 1
Size: 361594 Color: 0
Size: 124101 Color: 4

Bin 1538: 4 of cap free
Amount of items: 2
Items: 
Size: 515144 Color: 4
Size: 484853 Color: 3

Bin 1539: 4 of cap free
Amount of items: 3
Items: 
Size: 517720 Color: 1
Size: 324207 Color: 0
Size: 158070 Color: 3

Bin 1540: 4 of cap free
Amount of items: 3
Items: 
Size: 528882 Color: 0
Size: 326860 Color: 3
Size: 144255 Color: 2

Bin 1541: 4 of cap free
Amount of items: 3
Items: 
Size: 529401 Color: 3
Size: 355735 Color: 0
Size: 114861 Color: 1

Bin 1542: 4 of cap free
Amount of items: 3
Items: 
Size: 530746 Color: 2
Size: 339035 Color: 3
Size: 130216 Color: 4

Bin 1543: 4 of cap free
Amount of items: 3
Items: 
Size: 531786 Color: 3
Size: 347645 Color: 1
Size: 120566 Color: 1

Bin 1544: 4 of cap free
Amount of items: 3
Items: 
Size: 538092 Color: 3
Size: 340039 Color: 0
Size: 121866 Color: 2

Bin 1545: 4 of cap free
Amount of items: 3
Items: 
Size: 539600 Color: 3
Size: 333798 Color: 1
Size: 126599 Color: 2

Bin 1546: 4 of cap free
Amount of items: 3
Items: 
Size: 539979 Color: 0
Size: 347550 Color: 1
Size: 112468 Color: 3

Bin 1547: 4 of cap free
Amount of items: 3
Items: 
Size: 543345 Color: 1
Size: 329736 Color: 0
Size: 126916 Color: 4

Bin 1548: 4 of cap free
Amount of items: 3
Items: 
Size: 544748 Color: 0
Size: 347754 Color: 4
Size: 107495 Color: 0

Bin 1549: 4 of cap free
Amount of items: 3
Items: 
Size: 545555 Color: 2
Size: 332016 Color: 0
Size: 122426 Color: 2

Bin 1550: 5 of cap free
Amount of items: 4
Items: 
Size: 410147 Color: 3
Size: 197702 Color: 0
Size: 197245 Color: 2
Size: 194902 Color: 1

Bin 1551: 5 of cap free
Amount of items: 4
Items: 
Size: 411140 Color: 0
Size: 199619 Color: 1
Size: 194669 Color: 0
Size: 194568 Color: 3

Bin 1552: 5 of cap free
Amount of items: 4
Items: 
Size: 411334 Color: 2
Size: 198253 Color: 3
Size: 195775 Color: 4
Size: 194634 Color: 0

Bin 1553: 5 of cap free
Amount of items: 3
Items: 
Size: 421964 Color: 1
Size: 394776 Color: 0
Size: 183256 Color: 2

Bin 1554: 5 of cap free
Amount of items: 3
Items: 
Size: 435154 Color: 0
Size: 389445 Color: 4
Size: 175397 Color: 4

Bin 1555: 5 of cap free
Amount of items: 3
Items: 
Size: 441262 Color: 3
Size: 386062 Color: 1
Size: 172672 Color: 2

Bin 1556: 5 of cap free
Amount of items: 3
Items: 
Size: 441820 Color: 2
Size: 388741 Color: 1
Size: 169435 Color: 0

Bin 1557: 5 of cap free
Amount of items: 3
Items: 
Size: 450147 Color: 3
Size: 282927 Color: 2
Size: 266922 Color: 1

Bin 1558: 5 of cap free
Amount of items: 3
Items: 
Size: 451112 Color: 3
Size: 382784 Color: 4
Size: 166100 Color: 2

Bin 1559: 5 of cap free
Amount of items: 3
Items: 
Size: 457402 Color: 0
Size: 380539 Color: 4
Size: 162055 Color: 0

Bin 1560: 5 of cap free
Amount of items: 3
Items: 
Size: 458249 Color: 2
Size: 379115 Color: 0
Size: 162632 Color: 2

Bin 1561: 5 of cap free
Amount of items: 3
Items: 
Size: 460713 Color: 4
Size: 290190 Color: 1
Size: 249093 Color: 1

Bin 1562: 5 of cap free
Amount of items: 3
Items: 
Size: 463118 Color: 4
Size: 378089 Color: 2
Size: 158789 Color: 0

Bin 1563: 5 of cap free
Amount of items: 3
Items: 
Size: 469814 Color: 2
Size: 374935 Color: 0
Size: 155247 Color: 0

Bin 1564: 5 of cap free
Amount of items: 3
Items: 
Size: 470501 Color: 4
Size: 373953 Color: 1
Size: 155542 Color: 3

Bin 1565: 5 of cap free
Amount of items: 3
Items: 
Size: 474156 Color: 3
Size: 292269 Color: 4
Size: 233571 Color: 3

Bin 1566: 5 of cap free
Amount of items: 3
Items: 
Size: 476836 Color: 3
Size: 296609 Color: 4
Size: 226551 Color: 2

Bin 1567: 5 of cap free
Amount of items: 3
Items: 
Size: 482788 Color: 1
Size: 306975 Color: 2
Size: 210233 Color: 1

Bin 1568: 5 of cap free
Amount of items: 3
Items: 
Size: 483109 Color: 0
Size: 298214 Color: 4
Size: 218673 Color: 4

Bin 1569: 5 of cap free
Amount of items: 3
Items: 
Size: 483120 Color: 4
Size: 299310 Color: 2
Size: 217566 Color: 4

Bin 1570: 5 of cap free
Amount of items: 3
Items: 
Size: 485728 Color: 3
Size: 375950 Color: 0
Size: 138318 Color: 3

Bin 1571: 5 of cap free
Amount of items: 3
Items: 
Size: 490821 Color: 4
Size: 304764 Color: 3
Size: 204411 Color: 3

Bin 1572: 5 of cap free
Amount of items: 3
Items: 
Size: 495137 Color: 3
Size: 367481 Color: 1
Size: 137378 Color: 2

Bin 1573: 5 of cap free
Amount of items: 3
Items: 
Size: 495421 Color: 0
Size: 367445 Color: 3
Size: 137130 Color: 1

Bin 1574: 5 of cap free
Amount of items: 3
Items: 
Size: 499327 Color: 0
Size: 323156 Color: 4
Size: 177513 Color: 1

Bin 1575: 5 of cap free
Amount of items: 3
Items: 
Size: 514256 Color: 0
Size: 361800 Color: 1
Size: 123940 Color: 1

Bin 1576: 5 of cap free
Amount of items: 3
Items: 
Size: 515039 Color: 0
Size: 354388 Color: 4
Size: 130569 Color: 0

Bin 1577: 5 of cap free
Amount of items: 3
Items: 
Size: 517278 Color: 0
Size: 328506 Color: 1
Size: 154212 Color: 3

Bin 1578: 5 of cap free
Amount of items: 3
Items: 
Size: 529245 Color: 3
Size: 325825 Color: 0
Size: 144926 Color: 0

Bin 1579: 5 of cap free
Amount of items: 3
Items: 
Size: 530738 Color: 4
Size: 328658 Color: 0
Size: 140600 Color: 2

Bin 1580: 5 of cap free
Amount of items: 3
Items: 
Size: 536344 Color: 1
Size: 351976 Color: 3
Size: 111676 Color: 0

Bin 1581: 5 of cap free
Amount of items: 3
Items: 
Size: 537272 Color: 0
Size: 343964 Color: 1
Size: 118760 Color: 2

Bin 1582: 6 of cap free
Amount of items: 4
Items: 
Size: 411379 Color: 3
Size: 198420 Color: 0
Size: 195965 Color: 1
Size: 194231 Color: 2

Bin 1583: 6 of cap free
Amount of items: 4
Items: 
Size: 414964 Color: 4
Size: 206334 Color: 0
Size: 190220 Color: 2
Size: 188477 Color: 0

Bin 1584: 6 of cap free
Amount of items: 3
Items: 
Size: 418348 Color: 3
Size: 396288 Color: 2
Size: 185359 Color: 0

Bin 1585: 6 of cap free
Amount of items: 3
Items: 
Size: 426091 Color: 4
Size: 393270 Color: 2
Size: 180634 Color: 4

Bin 1586: 6 of cap free
Amount of items: 3
Items: 
Size: 440816 Color: 4
Size: 388952 Color: 1
Size: 170227 Color: 3

Bin 1587: 6 of cap free
Amount of items: 3
Items: 
Size: 441550 Color: 1
Size: 387453 Color: 2
Size: 170992 Color: 1

Bin 1588: 6 of cap free
Amount of items: 3
Items: 
Size: 441638 Color: 0
Size: 386769 Color: 1
Size: 171588 Color: 0

Bin 1589: 6 of cap free
Amount of items: 3
Items: 
Size: 442362 Color: 0
Size: 386368 Color: 1
Size: 171265 Color: 4

Bin 1590: 6 of cap free
Amount of items: 3
Items: 
Size: 442824 Color: 1
Size: 384487 Color: 0
Size: 172684 Color: 1

Bin 1591: 6 of cap free
Amount of items: 3
Items: 
Size: 443223 Color: 1
Size: 387416 Color: 0
Size: 169356 Color: 1

Bin 1592: 6 of cap free
Amount of items: 3
Items: 
Size: 443785 Color: 4
Size: 387210 Color: 1
Size: 169000 Color: 0

Bin 1593: 6 of cap free
Amount of items: 3
Items: 
Size: 444445 Color: 0
Size: 384189 Color: 4
Size: 171361 Color: 4

Bin 1594: 6 of cap free
Amount of items: 3
Items: 
Size: 444445 Color: 0
Size: 390001 Color: 2
Size: 165549 Color: 0

Bin 1595: 6 of cap free
Amount of items: 3
Items: 
Size: 445798 Color: 0
Size: 386743 Color: 4
Size: 167454 Color: 4

Bin 1596: 6 of cap free
Amount of items: 3
Items: 
Size: 449012 Color: 4
Size: 387729 Color: 0
Size: 163254 Color: 2

Bin 1597: 6 of cap free
Amount of items: 3
Items: 
Size: 451212 Color: 3
Size: 381662 Color: 0
Size: 167121 Color: 3

Bin 1598: 6 of cap free
Amount of items: 3
Items: 
Size: 454855 Color: 2
Size: 381807 Color: 3
Size: 163333 Color: 0

Bin 1599: 6 of cap free
Amount of items: 3
Items: 
Size: 458456 Color: 2
Size: 379427 Color: 4
Size: 162112 Color: 2

Bin 1600: 6 of cap free
Amount of items: 3
Items: 
Size: 459227 Color: 2
Size: 373395 Color: 0
Size: 167373 Color: 4

Bin 1601: 6 of cap free
Amount of items: 3
Items: 
Size: 459289 Color: 3
Size: 285614 Color: 1
Size: 255092 Color: 2

Bin 1602: 6 of cap free
Amount of items: 3
Items: 
Size: 470043 Color: 1
Size: 291096 Color: 3
Size: 238856 Color: 1

Bin 1603: 6 of cap free
Amount of items: 3
Items: 
Size: 480630 Color: 4
Size: 360111 Color: 0
Size: 159254 Color: 1

Bin 1604: 6 of cap free
Amount of items: 3
Items: 
Size: 482900 Color: 2
Size: 367336 Color: 0
Size: 149759 Color: 1

Bin 1605: 6 of cap free
Amount of items: 3
Items: 
Size: 486371 Color: 4
Size: 372834 Color: 0
Size: 140790 Color: 4

Bin 1606: 6 of cap free
Amount of items: 3
Items: 
Size: 495074 Color: 1
Size: 312267 Color: 3
Size: 192654 Color: 0

Bin 1607: 6 of cap free
Amount of items: 3
Items: 
Size: 495315 Color: 3
Size: 364927 Color: 0
Size: 139753 Color: 3

Bin 1608: 6 of cap free
Amount of items: 3
Items: 
Size: 495572 Color: 0
Size: 362681 Color: 4
Size: 141742 Color: 2

Bin 1609: 6 of cap free
Amount of items: 3
Items: 
Size: 495900 Color: 3
Size: 309636 Color: 4
Size: 194459 Color: 2

Bin 1610: 6 of cap free
Amount of items: 3
Items: 
Size: 508086 Color: 0
Size: 324098 Color: 2
Size: 167811 Color: 1

Bin 1611: 6 of cap free
Amount of items: 3
Items: 
Size: 508311 Color: 4
Size: 309857 Color: 3
Size: 181827 Color: 1

Bin 1612: 6 of cap free
Amount of items: 3
Items: 
Size: 515723 Color: 1
Size: 355996 Color: 0
Size: 128276 Color: 1

Bin 1613: 6 of cap free
Amount of items: 3
Items: 
Size: 519607 Color: 4
Size: 362482 Color: 2
Size: 117906 Color: 4

Bin 1614: 6 of cap free
Amount of items: 3
Items: 
Size: 520905 Color: 0
Size: 342361 Color: 2
Size: 136729 Color: 2

Bin 1615: 6 of cap free
Amount of items: 3
Items: 
Size: 546927 Color: 4
Size: 335539 Color: 2
Size: 117529 Color: 1

Bin 1616: 7 of cap free
Amount of items: 3
Items: 
Size: 418572 Color: 1
Size: 396062 Color: 4
Size: 185360 Color: 0

Bin 1617: 7 of cap free
Amount of items: 3
Items: 
Size: 442931 Color: 1
Size: 384363 Color: 0
Size: 172700 Color: 3

Bin 1618: 7 of cap free
Amount of items: 3
Items: 
Size: 443131 Color: 4
Size: 384307 Color: 3
Size: 172556 Color: 3

Bin 1619: 7 of cap free
Amount of items: 3
Items: 
Size: 451386 Color: 1
Size: 375048 Color: 4
Size: 173560 Color: 3

Bin 1620: 7 of cap free
Amount of items: 3
Items: 
Size: 460001 Color: 3
Size: 288721 Color: 1
Size: 251272 Color: 1

Bin 1621: 7 of cap free
Amount of items: 3
Items: 
Size: 461160 Color: 4
Size: 380428 Color: 1
Size: 158406 Color: 1

Bin 1622: 7 of cap free
Amount of items: 3
Items: 
Size: 465784 Color: 3
Size: 289544 Color: 1
Size: 244666 Color: 0

Bin 1623: 7 of cap free
Amount of items: 3
Items: 
Size: 470180 Color: 3
Size: 298342 Color: 1
Size: 231472 Color: 1

Bin 1624: 7 of cap free
Amount of items: 3
Items: 
Size: 481456 Color: 2
Size: 304691 Color: 3
Size: 213847 Color: 1

Bin 1625: 7 of cap free
Amount of items: 3
Items: 
Size: 488158 Color: 4
Size: 368346 Color: 3
Size: 143490 Color: 2

Bin 1626: 7 of cap free
Amount of items: 3
Items: 
Size: 488484 Color: 4
Size: 301339 Color: 0
Size: 210171 Color: 0

Bin 1627: 7 of cap free
Amount of items: 3
Items: 
Size: 495076 Color: 2
Size: 361918 Color: 0
Size: 143000 Color: 2

Bin 1628: 7 of cap free
Amount of items: 3
Items: 
Size: 495602 Color: 1
Size: 365051 Color: 3
Size: 139341 Color: 0

Bin 1629: 7 of cap free
Amount of items: 3
Items: 
Size: 498401 Color: 2
Size: 363404 Color: 0
Size: 138189 Color: 3

Bin 1630: 7 of cap free
Amount of items: 3
Items: 
Size: 513319 Color: 1
Size: 360102 Color: 3
Size: 126573 Color: 4

Bin 1631: 7 of cap free
Amount of items: 3
Items: 
Size: 517719 Color: 1
Size: 321133 Color: 2
Size: 161142 Color: 1

Bin 1632: 7 of cap free
Amount of items: 3
Items: 
Size: 532027 Color: 2
Size: 329254 Color: 4
Size: 138713 Color: 1

Bin 1633: 7 of cap free
Amount of items: 3
Items: 
Size: 534333 Color: 3
Size: 359672 Color: 2
Size: 105989 Color: 3

Bin 1634: 7 of cap free
Amount of items: 3
Items: 
Size: 540346 Color: 0
Size: 338283 Color: 3
Size: 121365 Color: 0

Bin 1635: 8 of cap free
Amount of items: 4
Items: 
Size: 410243 Color: 1
Size: 197991 Color: 3
Size: 196778 Color: 2
Size: 194981 Color: 1

Bin 1636: 8 of cap free
Amount of items: 4
Items: 
Size: 416104 Color: 4
Size: 208256 Color: 0
Size: 188380 Color: 2
Size: 187253 Color: 0

Bin 1637: 8 of cap free
Amount of items: 3
Items: 
Size: 421750 Color: 3
Size: 394700 Color: 4
Size: 183543 Color: 3

Bin 1638: 8 of cap free
Amount of items: 3
Items: 
Size: 424003 Color: 0
Size: 394099 Color: 1
Size: 181891 Color: 0

Bin 1639: 8 of cap free
Amount of items: 3
Items: 
Size: 435892 Color: 0
Size: 389069 Color: 1
Size: 175032 Color: 0

Bin 1640: 8 of cap free
Amount of items: 3
Items: 
Size: 445180 Color: 3
Size: 284701 Color: 0
Size: 270112 Color: 3

Bin 1641: 8 of cap free
Amount of items: 3
Items: 
Size: 446198 Color: 0
Size: 384438 Color: 1
Size: 169357 Color: 0

Bin 1642: 8 of cap free
Amount of items: 3
Items: 
Size: 458566 Color: 3
Size: 300489 Color: 1
Size: 240938 Color: 2

Bin 1643: 8 of cap free
Amount of items: 3
Items: 
Size: 459590 Color: 2
Size: 294516 Color: 1
Size: 245887 Color: 1

Bin 1644: 8 of cap free
Amount of items: 3
Items: 
Size: 470782 Color: 3
Size: 377486 Color: 1
Size: 151725 Color: 4

Bin 1645: 8 of cap free
Amount of items: 3
Items: 
Size: 482779 Color: 0
Size: 372308 Color: 4
Size: 144906 Color: 1

Bin 1646: 8 of cap free
Amount of items: 3
Items: 
Size: 494541 Color: 1
Size: 360139 Color: 2
Size: 145313 Color: 1

Bin 1647: 8 of cap free
Amount of items: 3
Items: 
Size: 513738 Color: 1
Size: 359797 Color: 2
Size: 126458 Color: 2

Bin 1648: 8 of cap free
Amount of items: 3
Items: 
Size: 515330 Color: 1
Size: 353796 Color: 2
Size: 130867 Color: 1

Bin 1649: 8 of cap free
Amount of items: 3
Items: 
Size: 515571 Color: 4
Size: 359487 Color: 3
Size: 124935 Color: 1

Bin 1650: 8 of cap free
Amount of items: 3
Items: 
Size: 520154 Color: 1
Size: 363007 Color: 4
Size: 116832 Color: 0

Bin 1651: 9 of cap free
Amount of items: 4
Items: 
Size: 410801 Color: 3
Size: 197806 Color: 2
Size: 195992 Color: 4
Size: 195393 Color: 0

Bin 1652: 9 of cap free
Amount of items: 3
Items: 
Size: 418117 Color: 0
Size: 396395 Color: 3
Size: 185480 Color: 0

Bin 1653: 9 of cap free
Amount of items: 3
Items: 
Size: 423036 Color: 2
Size: 394485 Color: 0
Size: 182471 Color: 0

Bin 1654: 9 of cap free
Amount of items: 3
Items: 
Size: 426752 Color: 3
Size: 392731 Color: 1
Size: 180509 Color: 4

Bin 1655: 9 of cap free
Amount of items: 3
Items: 
Size: 431845 Color: 4
Size: 390757 Color: 0
Size: 177390 Color: 0

Bin 1656: 9 of cap free
Amount of items: 3
Items: 
Size: 441385 Color: 4
Size: 384519 Color: 3
Size: 174088 Color: 4

Bin 1657: 9 of cap free
Amount of items: 3
Items: 
Size: 442038 Color: 1
Size: 390689 Color: 3
Size: 167265 Color: 1

Bin 1658: 9 of cap free
Amount of items: 3
Items: 
Size: 442389 Color: 3
Size: 288216 Color: 4
Size: 269387 Color: 4

Bin 1659: 9 of cap free
Amount of items: 3
Items: 
Size: 443335 Color: 0
Size: 389854 Color: 1
Size: 166803 Color: 3

Bin 1660: 9 of cap free
Amount of items: 3
Items: 
Size: 444898 Color: 1
Size: 281727 Color: 4
Size: 273367 Color: 0

Bin 1661: 9 of cap free
Amount of items: 3
Items: 
Size: 459299 Color: 0
Size: 380719 Color: 1
Size: 159974 Color: 2

Bin 1662: 9 of cap free
Amount of items: 3
Items: 
Size: 513307 Color: 3
Size: 355409 Color: 2
Size: 131276 Color: 0

Bin 1663: 10 of cap free
Amount of items: 4
Items: 
Size: 415508 Color: 4
Size: 207163 Color: 2
Size: 188810 Color: 1
Size: 188510 Color: 1

Bin 1664: 10 of cap free
Amount of items: 3
Items: 
Size: 419071 Color: 2
Size: 396021 Color: 0
Size: 184899 Color: 0

Bin 1665: 10 of cap free
Amount of items: 3
Items: 
Size: 419287 Color: 1
Size: 395894 Color: 3
Size: 184810 Color: 2

Bin 1666: 10 of cap free
Amount of items: 3
Items: 
Size: 440284 Color: 1
Size: 389730 Color: 0
Size: 169977 Color: 2

Bin 1667: 10 of cap free
Amount of items: 3
Items: 
Size: 458402 Color: 4
Size: 286760 Color: 2
Size: 254829 Color: 4

Bin 1668: 10 of cap free
Amount of items: 3
Items: 
Size: 459428 Color: 0
Size: 292983 Color: 3
Size: 247580 Color: 2

Bin 1669: 10 of cap free
Amount of items: 3
Items: 
Size: 498421 Color: 2
Size: 362766 Color: 1
Size: 138804 Color: 1

Bin 1670: 10 of cap free
Amount of items: 3
Items: 
Size: 513467 Color: 2
Size: 361373 Color: 3
Size: 125151 Color: 1

Bin 1671: 11 of cap free
Amount of items: 3
Items: 
Size: 401945 Color: 4
Size: 397560 Color: 0
Size: 200485 Color: 1

Bin 1672: 11 of cap free
Amount of items: 4
Items: 
Size: 414652 Color: 1
Size: 205142 Color: 0
Size: 191175 Color: 3
Size: 189021 Color: 1

Bin 1673: 11 of cap free
Amount of items: 4
Items: 
Size: 415272 Color: 0
Size: 207587 Color: 2
Size: 188638 Color: 0
Size: 188493 Color: 4

Bin 1674: 11 of cap free
Amount of items: 3
Items: 
Size: 442117 Color: 3
Size: 279954 Color: 2
Size: 277919 Color: 0

Bin 1675: 11 of cap free
Amount of items: 3
Items: 
Size: 443333 Color: 0
Size: 282419 Color: 4
Size: 274238 Color: 4

Bin 1676: 11 of cap free
Amount of items: 3
Items: 
Size: 458049 Color: 3
Size: 377462 Color: 1
Size: 164479 Color: 2

Bin 1677: 11 of cap free
Amount of items: 3
Items: 
Size: 458816 Color: 1
Size: 371931 Color: 2
Size: 169243 Color: 4

Bin 1678: 11 of cap free
Amount of items: 3
Items: 
Size: 459742 Color: 0
Size: 366642 Color: 2
Size: 173606 Color: 3

Bin 1679: 11 of cap free
Amount of items: 3
Items: 
Size: 485053 Color: 0
Size: 369157 Color: 1
Size: 145780 Color: 1

Bin 1680: 11 of cap free
Amount of items: 3
Items: 
Size: 485780 Color: 4
Size: 302750 Color: 0
Size: 211460 Color: 2

Bin 1681: 11 of cap free
Amount of items: 3
Items: 
Size: 513721 Color: 0
Size: 360930 Color: 3
Size: 125339 Color: 2

Bin 1682: 11 of cap free
Amount of items: 3
Items: 
Size: 513905 Color: 4
Size: 366255 Color: 2
Size: 119830 Color: 1

Bin 1683: 12 of cap free
Amount of items: 4
Items: 
Size: 411321 Color: 2
Size: 198250 Color: 1
Size: 195597 Color: 3
Size: 194821 Color: 3

Bin 1684: 12 of cap free
Amount of items: 4
Items: 
Size: 414871 Color: 3
Size: 206934 Color: 1
Size: 189281 Color: 2
Size: 188903 Color: 0

Bin 1685: 12 of cap free
Amount of items: 3
Items: 
Size: 440098 Color: 2
Size: 386009 Color: 0
Size: 173882 Color: 0

Bin 1686: 12 of cap free
Amount of items: 3
Items: 
Size: 443145 Color: 4
Size: 386160 Color: 1
Size: 170684 Color: 4

Bin 1687: 12 of cap free
Amount of items: 3
Items: 
Size: 494194 Color: 0
Size: 369655 Color: 2
Size: 136140 Color: 3

Bin 1688: 12 of cap free
Amount of items: 3
Items: 
Size: 497740 Color: 4
Size: 311808 Color: 2
Size: 190441 Color: 0

Bin 1689: 12 of cap free
Amount of items: 3
Items: 
Size: 513634 Color: 2
Size: 364573 Color: 3
Size: 121782 Color: 0

Bin 1690: 12 of cap free
Amount of items: 3
Items: 
Size: 515497 Color: 1
Size: 362259 Color: 0
Size: 122233 Color: 2

Bin 1691: 13 of cap free
Amount of items: 4
Items: 
Size: 412929 Color: 1
Size: 201559 Color: 0
Size: 192957 Color: 1
Size: 192543 Color: 0

Bin 1692: 13 of cap free
Amount of items: 4
Items: 
Size: 415144 Color: 3
Size: 207256 Color: 4
Size: 189252 Color: 0
Size: 188336 Color: 2

Bin 1693: 13 of cap free
Amount of items: 3
Items: 
Size: 457742 Color: 0
Size: 380190 Color: 4
Size: 162056 Color: 0

Bin 1694: 13 of cap free
Amount of items: 3
Items: 
Size: 495467 Color: 1
Size: 316933 Color: 4
Size: 187588 Color: 0

Bin 1695: 14 of cap free
Amount of items: 4
Items: 
Size: 414155 Color: 4
Size: 202431 Color: 1
Size: 191718 Color: 1
Size: 191683 Color: 0

Bin 1696: 14 of cap free
Amount of items: 3
Items: 
Size: 440354 Color: 1
Size: 378951 Color: 3
Size: 180682 Color: 3

Bin 1697: 16 of cap free
Amount of items: 4
Items: 
Size: 413944 Color: 4
Size: 201668 Color: 1
Size: 192382 Color: 0
Size: 191991 Color: 4

Bin 1698: 16 of cap free
Amount of items: 3
Items: 
Size: 420396 Color: 3
Size: 395252 Color: 4
Size: 184337 Color: 2

Bin 1699: 16 of cap free
Amount of items: 3
Items: 
Size: 421680 Color: 1
Size: 394746 Color: 0
Size: 183559 Color: 4

Bin 1700: 16 of cap free
Amount of items: 3
Items: 
Size: 432813 Color: 1
Size: 390586 Color: 2
Size: 176586 Color: 1

Bin 1701: 16 of cap free
Amount of items: 3
Items: 
Size: 457369 Color: 3
Size: 387870 Color: 0
Size: 154746 Color: 0

Bin 1702: 16 of cap free
Amount of items: 3
Items: 
Size: 459577 Color: 1
Size: 286734 Color: 4
Size: 253674 Color: 0

Bin 1703: 16 of cap free
Amount of items: 3
Items: 
Size: 513661 Color: 4
Size: 360674 Color: 3
Size: 125650 Color: 4

Bin 1704: 16 of cap free
Amount of items: 3
Items: 
Size: 514100 Color: 1
Size: 360807 Color: 4
Size: 125078 Color: 2

Bin 1705: 17 of cap free
Amount of items: 3
Items: 
Size: 423152 Color: 3
Size: 394429 Color: 1
Size: 182403 Color: 3

Bin 1706: 17 of cap free
Amount of items: 3
Items: 
Size: 513231 Color: 1
Size: 371144 Color: 4
Size: 115609 Color: 4

Bin 1707: 17 of cap free
Amount of items: 3
Items: 
Size: 513323 Color: 3
Size: 358707 Color: 2
Size: 127954 Color: 1

Bin 1708: 18 of cap free
Amount of items: 4
Items: 
Size: 413728 Color: 0
Size: 201364 Color: 3
Size: 192858 Color: 0
Size: 192033 Color: 4

Bin 1709: 18 of cap free
Amount of items: 3
Items: 
Size: 422661 Color: 0
Size: 394511 Color: 1
Size: 182811 Color: 1

Bin 1710: 18 of cap free
Amount of items: 3
Items: 
Size: 440738 Color: 2
Size: 281506 Color: 4
Size: 277739 Color: 3

Bin 1711: 18 of cap free
Amount of items: 3
Items: 
Size: 442497 Color: 3
Size: 386739 Color: 0
Size: 170747 Color: 0

Bin 1712: 18 of cap free
Amount of items: 3
Items: 
Size: 444436 Color: 0
Size: 380630 Color: 1
Size: 174917 Color: 0

Bin 1713: 18 of cap free
Amount of items: 3
Items: 
Size: 513932 Color: 3
Size: 357344 Color: 1
Size: 128707 Color: 2

Bin 1714: 19 of cap free
Amount of items: 3
Items: 
Size: 425708 Color: 0
Size: 393616 Color: 1
Size: 180658 Color: 2

Bin 1715: 20 of cap free
Amount of items: 4
Items: 
Size: 415206 Color: 2
Size: 207575 Color: 4
Size: 189567 Color: 2
Size: 187633 Color: 0

Bin 1716: 20 of cap free
Amount of items: 3
Items: 
Size: 419244 Color: 0
Size: 395881 Color: 1
Size: 184856 Color: 4

Bin 1717: 20 of cap free
Amount of items: 3
Items: 
Size: 425360 Color: 2
Size: 392979 Color: 4
Size: 181642 Color: 3

Bin 1718: 20 of cap free
Amount of items: 3
Items: 
Size: 429118 Color: 2
Size: 392027 Color: 0
Size: 178836 Color: 0

Bin 1719: 20 of cap free
Amount of items: 3
Items: 
Size: 439767 Color: 4
Size: 386069 Color: 1
Size: 174145 Color: 0

Bin 1720: 21 of cap free
Amount of items: 4
Items: 
Size: 410112 Color: 0
Size: 197430 Color: 1
Size: 197035 Color: 2
Size: 195403 Color: 2

Bin 1721: 21 of cap free
Amount of items: 3
Items: 
Size: 429423 Color: 0
Size: 391653 Color: 1
Size: 178904 Color: 2

Bin 1722: 21 of cap free
Amount of items: 3
Items: 
Size: 457964 Color: 1
Size: 368570 Color: 2
Size: 173446 Color: 0

Bin 1723: 22 of cap free
Amount of items: 3
Items: 
Size: 416183 Color: 3
Size: 397107 Color: 2
Size: 186689 Color: 3

Bin 1724: 22 of cap free
Amount of items: 3
Items: 
Size: 418813 Color: 4
Size: 396070 Color: 3
Size: 185096 Color: 2

Bin 1725: 22 of cap free
Amount of items: 3
Items: 
Size: 423214 Color: 3
Size: 394361 Color: 4
Size: 182404 Color: 0

Bin 1726: 22 of cap free
Amount of items: 3
Items: 
Size: 427568 Color: 3
Size: 392608 Color: 4
Size: 179803 Color: 2

Bin 1727: 22 of cap free
Amount of items: 3
Items: 
Size: 434797 Color: 0
Size: 389571 Color: 1
Size: 175611 Color: 1

Bin 1728: 22 of cap free
Amount of items: 3
Items: 
Size: 441191 Color: 0
Size: 388906 Color: 4
Size: 169882 Color: 1

Bin 1729: 23 of cap free
Amount of items: 3
Items: 
Size: 421316 Color: 3
Size: 394534 Color: 2
Size: 184128 Color: 4

Bin 1730: 24 of cap free
Amount of items: 3
Items: 
Size: 430701 Color: 1
Size: 391539 Color: 4
Size: 177737 Color: 0

Bin 1731: 25 of cap free
Amount of items: 3
Items: 
Size: 428319 Color: 4
Size: 392390 Color: 2
Size: 179267 Color: 0

Bin 1732: 25 of cap free
Amount of items: 3
Items: 
Size: 441819 Color: 0
Size: 384324 Color: 4
Size: 173833 Color: 0

Bin 1733: 26 of cap free
Amount of items: 3
Items: 
Size: 433754 Color: 0
Size: 390261 Color: 1
Size: 175960 Color: 0

Bin 1734: 26 of cap free
Amount of items: 3
Items: 
Size: 441952 Color: 0
Size: 387584 Color: 3
Size: 170439 Color: 0

Bin 1735: 27 of cap free
Amount of items: 4
Items: 
Size: 416060 Color: 2
Size: 209464 Color: 0
Size: 187490 Color: 2
Size: 186960 Color: 0

Bin 1736: 27 of cap free
Amount of items: 3
Items: 
Size: 429154 Color: 3
Size: 391684 Color: 1
Size: 179136 Color: 2

Bin 1737: 27 of cap free
Amount of items: 3
Items: 
Size: 431318 Color: 2
Size: 390988 Color: 4
Size: 177668 Color: 4

Bin 1738: 27 of cap free
Amount of items: 3
Items: 
Size: 441332 Color: 0
Size: 389495 Color: 4
Size: 169147 Color: 4

Bin 1739: 29 of cap free
Amount of items: 3
Items: 
Size: 428856 Color: 0
Size: 391879 Color: 1
Size: 179237 Color: 2

Bin 1740: 29 of cap free
Amount of items: 3
Items: 
Size: 440675 Color: 3
Size: 284443 Color: 0
Size: 274854 Color: 3

Bin 1741: 30 of cap free
Amount of items: 3
Items: 
Size: 403544 Color: 1
Size: 397182 Color: 3
Size: 199245 Color: 0

Bin 1742: 30 of cap free
Amount of items: 3
Items: 
Size: 425536 Color: 4
Size: 393352 Color: 3
Size: 181083 Color: 0

Bin 1743: 31 of cap free
Amount of items: 3
Items: 
Size: 440276 Color: 1
Size: 388643 Color: 3
Size: 171051 Color: 4

Bin 1744: 32 of cap free
Amount of items: 3
Items: 
Size: 421490 Color: 1
Size: 394896 Color: 2
Size: 183583 Color: 0

Bin 1745: 32 of cap free
Amount of items: 3
Items: 
Size: 430209 Color: 3
Size: 391789 Color: 2
Size: 177971 Color: 3

Bin 1746: 32 of cap free
Amount of items: 3
Items: 
Size: 439548 Color: 4
Size: 386525 Color: 1
Size: 173896 Color: 1

Bin 1747: 33 of cap free
Amount of items: 4
Items: 
Size: 412526 Color: 1
Size: 200859 Color: 2
Size: 193535 Color: 4
Size: 193048 Color: 2

Bin 1748: 34 of cap free
Amount of items: 3
Items: 
Size: 425238 Color: 2
Size: 393056 Color: 0
Size: 181673 Color: 2

Bin 1749: 36 of cap free
Amount of items: 3
Items: 
Size: 416408 Color: 4
Size: 397037 Color: 1
Size: 186520 Color: 0

Bin 1750: 37 of cap free
Amount of items: 4
Items: 
Size: 407568 Color: 4
Size: 198015 Color: 3
Size: 197953 Color: 0
Size: 196428 Color: 4

Bin 1751: 37 of cap free
Amount of items: 3
Items: 
Size: 423994 Color: 0
Size: 394108 Color: 3
Size: 181862 Color: 1

Bin 1752: 38 of cap free
Amount of items: 3
Items: 
Size: 425423 Color: 1
Size: 392983 Color: 0
Size: 181557 Color: 3

Bin 1753: 40 of cap free
Amount of items: 3
Items: 
Size: 402092 Color: 3
Size: 397428 Color: 4
Size: 200441 Color: 3

Bin 1754: 40 of cap free
Amount of items: 4
Items: 
Size: 414272 Color: 0
Size: 203067 Color: 4
Size: 191563 Color: 1
Size: 191059 Color: 1

Bin 1755: 41 of cap free
Amount of items: 3
Items: 
Size: 429048 Color: 4
Size: 391811 Color: 3
Size: 179101 Color: 3

Bin 1756: 41 of cap free
Amount of items: 3
Items: 
Size: 431974 Color: 4
Size: 390893 Color: 3
Size: 177093 Color: 3

Bin 1757: 43 of cap free
Amount of items: 3
Items: 
Size: 430646 Color: 1
Size: 391671 Color: 0
Size: 177641 Color: 2

Bin 1758: 44 of cap free
Amount of items: 3
Items: 
Size: 426840 Color: 2
Size: 392659 Color: 0
Size: 180458 Color: 0

Bin 1759: 45 of cap free
Amount of items: 3
Items: 
Size: 418339 Color: 2
Size: 396232 Color: 0
Size: 185385 Color: 1

Bin 1760: 46 of cap free
Amount of items: 3
Items: 
Size: 433971 Color: 4
Size: 390317 Color: 1
Size: 175667 Color: 2

Bin 1761: 46 of cap free
Amount of items: 3
Items: 
Size: 440158 Color: 4
Size: 385897 Color: 1
Size: 173900 Color: 0

Bin 1762: 47 of cap free
Amount of items: 4
Items: 
Size: 411990 Color: 3
Size: 200194 Color: 1
Size: 194335 Color: 0
Size: 193435 Color: 3

Bin 1763: 48 of cap free
Amount of items: 3
Items: 
Size: 434106 Color: 0
Size: 390094 Color: 1
Size: 175753 Color: 3

Bin 1764: 49 of cap free
Amount of items: 3
Items: 
Size: 417931 Color: 4
Size: 396454 Color: 0
Size: 185567 Color: 2

Bin 1765: 49 of cap free
Amount of items: 3
Items: 
Size: 431723 Color: 0
Size: 390853 Color: 2
Size: 177376 Color: 0

Bin 1766: 52 of cap free
Amount of items: 3
Items: 
Size: 438985 Color: 2
Size: 386972 Color: 4
Size: 173992 Color: 0

Bin 1767: 53 of cap free
Amount of items: 3
Items: 
Size: 425560 Color: 3
Size: 393654 Color: 1
Size: 180734 Color: 1

Bin 1768: 53 of cap free
Amount of items: 3
Items: 
Size: 431463 Color: 3
Size: 391005 Color: 0
Size: 177480 Color: 1

Bin 1769: 53 of cap free
Amount of items: 3
Items: 
Size: 437112 Color: 0
Size: 388452 Color: 4
Size: 174384 Color: 1

Bin 1770: 54 of cap free
Amount of items: 3
Items: 
Size: 423582 Color: 3
Size: 394364 Color: 0
Size: 182001 Color: 2

Bin 1771: 55 of cap free
Amount of items: 3
Items: 
Size: 432835 Color: 3
Size: 390521 Color: 4
Size: 176590 Color: 0

Bin 1772: 56 of cap free
Amount of items: 4
Items: 
Size: 415873 Color: 2
Size: 208775 Color: 0
Size: 187881 Color: 1
Size: 187416 Color: 2

Bin 1773: 57 of cap free
Amount of items: 3
Items: 
Size: 432495 Color: 3
Size: 390628 Color: 0
Size: 176821 Color: 1

Bin 1774: 58 of cap free
Amount of items: 4
Items: 
Size: 414419 Color: 1
Size: 205363 Color: 4
Size: 190456 Color: 2
Size: 189705 Color: 4

Bin 1775: 59 of cap free
Amount of items: 3
Items: 
Size: 437429 Color: 2
Size: 388103 Color: 4
Size: 174410 Color: 2

Bin 1776: 60 of cap free
Amount of items: 3
Items: 
Size: 430194 Color: 1
Size: 391538 Color: 4
Size: 178209 Color: 2

Bin 1777: 62 of cap free
Amount of items: 3
Items: 
Size: 420156 Color: 1
Size: 395514 Color: 2
Size: 184269 Color: 4

Bin 1778: 65 of cap free
Amount of items: 3
Items: 
Size: 402694 Color: 0
Size: 397566 Color: 1
Size: 199676 Color: 3

Bin 1779: 67 of cap free
Amount of items: 3
Items: 
Size: 433372 Color: 1
Size: 390273 Color: 0
Size: 176289 Color: 0

Bin 1780: 68 of cap free
Amount of items: 4
Items: 
Size: 412116 Color: 1
Size: 199936 Color: 3
Size: 194428 Color: 3
Size: 193453 Color: 0

Bin 1781: 68 of cap free
Amount of items: 3
Items: 
Size: 431376 Color: 3
Size: 391093 Color: 0
Size: 177464 Color: 4

Bin 1782: 69 of cap free
Amount of items: 3
Items: 
Size: 440754 Color: 4
Size: 387154 Color: 1
Size: 172024 Color: 4

Bin 1783: 69 of cap free
Amount of items: 3
Items: 
Size: 441166 Color: 0
Size: 284044 Color: 4
Size: 274722 Color: 4

Bin 1784: 70 of cap free
Amount of items: 3
Items: 
Size: 441023 Color: 1
Size: 382047 Color: 0
Size: 176861 Color: 0

Bin 1785: 71 of cap free
Amount of items: 3
Items: 
Size: 416939 Color: 2
Size: 396689 Color: 1
Size: 186302 Color: 2

Bin 1786: 71 of cap free
Amount of items: 3
Items: 
Size: 434219 Color: 1
Size: 390187 Color: 4
Size: 175524 Color: 0

Bin 1787: 72 of cap free
Amount of items: 3
Items: 
Size: 417067 Color: 3
Size: 396809 Color: 1
Size: 186053 Color: 4

Bin 1788: 72 of cap free
Amount of items: 3
Items: 
Size: 423028 Color: 1
Size: 394301 Color: 3
Size: 182600 Color: 4

Bin 1789: 74 of cap free
Amount of items: 3
Items: 
Size: 427104 Color: 3
Size: 392472 Color: 1
Size: 180351 Color: 4

Bin 1790: 75 of cap free
Amount of items: 3
Items: 
Size: 429028 Color: 1
Size: 391968 Color: 0
Size: 178930 Color: 2

Bin 1791: 75 of cap free
Amount of items: 3
Items: 
Size: 440032 Color: 1
Size: 387499 Color: 2
Size: 172395 Color: 2

Bin 1792: 77 of cap free
Amount of items: 3
Items: 
Size: 419661 Color: 3
Size: 395736 Color: 0
Size: 184527 Color: 3

Bin 1793: 79 of cap free
Amount of items: 3
Items: 
Size: 422482 Color: 2
Size: 394467 Color: 4
Size: 182973 Color: 4

Bin 1794: 86 of cap free
Amount of items: 3
Items: 
Size: 418897 Color: 2
Size: 396072 Color: 0
Size: 184946 Color: 4

Bin 1795: 89 of cap free
Amount of items: 3
Items: 
Size: 436367 Color: 4
Size: 388374 Color: 3
Size: 175171 Color: 0

Bin 1796: 92 of cap free
Amount of items: 4
Items: 
Size: 413023 Color: 3
Size: 202388 Color: 0
Size: 192271 Color: 0
Size: 192227 Color: 3

Bin 1797: 92 of cap free
Amount of items: 3
Items: 
Size: 417645 Color: 3
Size: 396584 Color: 0
Size: 185680 Color: 1

Bin 1798: 101 of cap free
Amount of items: 3
Items: 
Size: 427920 Color: 3
Size: 392349 Color: 1
Size: 179631 Color: 0

Bin 1799: 103 of cap free
Amount of items: 4
Items: 
Size: 414196 Color: 0
Size: 202932 Color: 2
Size: 191538 Color: 4
Size: 191232 Color: 3

Bin 1800: 129 of cap free
Amount of items: 4
Items: 
Size: 415961 Color: 2
Size: 209006 Color: 4
Size: 187716 Color: 1
Size: 187189 Color: 4

Bin 1801: 130 of cap free
Amount of items: 4
Items: 
Size: 409959 Color: 1
Size: 197199 Color: 0
Size: 196569 Color: 4
Size: 196144 Color: 4

Bin 1802: 132 of cap free
Amount of items: 3
Items: 
Size: 425489 Color: 2
Size: 393730 Color: 1
Size: 180650 Color: 0

Bin 1803: 143 of cap free
Amount of items: 3
Items: 
Size: 425047 Color: 3
Size: 393867 Color: 4
Size: 180944 Color: 3

Bin 1804: 166 of cap free
Amount of items: 3
Items: 
Size: 440032 Color: 1
Size: 389180 Color: 4
Size: 170623 Color: 1

Bin 1805: 182 of cap free
Amount of items: 3
Items: 
Size: 421469 Color: 3
Size: 394796 Color: 2
Size: 183554 Color: 0

Bin 1806: 182 of cap free
Amount of items: 3
Items: 
Size: 439083 Color: 0
Size: 386663 Color: 4
Size: 174073 Color: 0

Bin 1807: 195 of cap free
Amount of items: 3
Items: 
Size: 421293 Color: 1
Size: 394704 Color: 3
Size: 183809 Color: 0

Bin 1808: 223 of cap free
Amount of items: 3
Items: 
Size: 423010 Color: 4
Size: 394218 Color: 3
Size: 182550 Color: 2

Bin 1809: 239 of cap free
Amount of items: 3
Items: 
Size: 438143 Color: 2
Size: 387154 Color: 1
Size: 174465 Color: 4

Bin 1810: 250 of cap free
Amount of items: 4
Items: 
Size: 416033 Color: 2
Size: 209437 Color: 1
Size: 187359 Color: 3
Size: 186922 Color: 2

Bin 1811: 482 of cap free
Amount of items: 3
Items: 
Size: 425387 Color: 3
Size: 393062 Color: 2
Size: 181070 Color: 3

Bin 1812: 2969 of cap free
Amount of items: 3
Items: 
Size: 439652 Color: 3
Size: 278834 Color: 4
Size: 278546 Color: 4

Bin 1813: 4206 of cap free
Amount of items: 3
Items: 
Size: 439662 Color: 4
Size: 278337 Color: 0
Size: 277796 Color: 2

Bin 1814: 5280 of cap free
Amount of items: 3
Items: 
Size: 439639 Color: 0
Size: 277701 Color: 1
Size: 277381 Color: 1

Bin 1815: 6170 of cap free
Amount of items: 3
Items: 
Size: 439470 Color: 0
Size: 277226 Color: 3
Size: 277135 Color: 0

Bin 1816: 6531 of cap free
Amount of items: 3
Items: 
Size: 439410 Color: 2
Size: 277109 Color: 1
Size: 276951 Color: 4

Bin 1817: 7165 of cap free
Amount of items: 3
Items: 
Size: 439229 Color: 4
Size: 276835 Color: 3
Size: 276772 Color: 1

Bin 1818: 8641 of cap free
Amount of items: 3
Items: 
Size: 438972 Color: 3
Size: 276223 Color: 0
Size: 276165 Color: 2

Bin 1819: 8657 of cap free
Amount of items: 3
Items: 
Size: 438932 Color: 1
Size: 276319 Color: 3
Size: 276093 Color: 2

Bin 1820: 9356 of cap free
Amount of items: 3
Items: 
Size: 438925 Color: 0
Size: 275934 Color: 2
Size: 275786 Color: 0

Bin 1821: 10710 of cap free
Amount of items: 3
Items: 
Size: 438821 Color: 2
Size: 275376 Color: 4
Size: 275094 Color: 3

Bin 1822: 10753 of cap free
Amount of items: 3
Items: 
Size: 438920 Color: 4
Size: 275200 Color: 3
Size: 275128 Color: 3

Bin 1823: 11219 of cap free
Amount of items: 3
Items: 
Size: 438716 Color: 3
Size: 275038 Color: 1
Size: 275028 Color: 3

Bin 1824: 11358 of cap free
Amount of items: 3
Items: 
Size: 438668 Color: 1
Size: 274992 Color: 4
Size: 274983 Color: 3

Bin 1825: 12267 of cap free
Amount of items: 3
Items: 
Size: 438653 Color: 1
Size: 274631 Color: 3
Size: 274450 Color: 3

Bin 1826: 13046 of cap free
Amount of items: 3
Items: 
Size: 438571 Color: 0
Size: 274204 Color: 3
Size: 274180 Color: 1

Bin 1827: 13980 of cap free
Amount of items: 3
Items: 
Size: 438263 Color: 1
Size: 274101 Color: 2
Size: 273657 Color: 4

Bin 1828: 14821 of cap free
Amount of items: 3
Items: 
Size: 438107 Color: 3
Size: 273645 Color: 2
Size: 273428 Color: 4

Bin 1829: 15878 of cap free
Amount of items: 3
Items: 
Size: 438018 Color: 2
Size: 273151 Color: 1
Size: 272954 Color: 3

Bin 1830: 16567 of cap free
Amount of items: 3
Items: 
Size: 438002 Color: 0
Size: 272761 Color: 2
Size: 272671 Color: 3

Bin 1831: 17112 of cap free
Amount of items: 3
Items: 
Size: 437906 Color: 0
Size: 272506 Color: 4
Size: 272477 Color: 4

Bin 1832: 17464 of cap free
Amount of items: 3
Items: 
Size: 437818 Color: 0
Size: 272435 Color: 4
Size: 272284 Color: 4

Bin 1833: 17898 of cap free
Amount of items: 3
Items: 
Size: 437726 Color: 1
Size: 272245 Color: 4
Size: 272132 Color: 1

Bin 1834: 18362 of cap free
Amount of items: 3
Items: 
Size: 437555 Color: 3
Size: 272053 Color: 2
Size: 272031 Color: 1

Bin 1835: 19280 of cap free
Amount of items: 3
Items: 
Size: 437408 Color: 1
Size: 271908 Color: 4
Size: 271405 Color: 2

Bin 1836: 19294 of cap free
Amount of items: 3
Items: 
Size: 437506 Color: 4
Size: 271641 Color: 1
Size: 271560 Color: 4

Bin 1837: 19633 of cap free
Amount of items: 3
Items: 
Size: 437454 Color: 4
Size: 271475 Color: 3
Size: 271439 Color: 4

Bin 1838: 20006 of cap free
Amount of items: 3
Items: 
Size: 437375 Color: 4
Size: 271328 Color: 0
Size: 271292 Color: 1

Bin 1839: 20578 of cap free
Amount of items: 3
Items: 
Size: 437310 Color: 4
Size: 271068 Color: 3
Size: 271045 Color: 1

Bin 1840: 21574 of cap free
Amount of items: 3
Items: 
Size: 437027 Color: 0
Size: 271227 Color: 4
Size: 270173 Color: 0

Bin 1841: 21875 of cap free
Amount of items: 3
Items: 
Size: 437062 Color: 4
Size: 270663 Color: 0
Size: 270401 Color: 3

Bin 1842: 22813 of cap free
Amount of items: 3
Items: 
Size: 436918 Color: 1
Size: 270143 Color: 3
Size: 270127 Color: 1

Bin 1843: 23633 of cap free
Amount of items: 3
Items: 
Size: 436651 Color: 1
Size: 269944 Color: 0
Size: 269773 Color: 2

Bin 1844: 23656 of cap free
Amount of items: 3
Items: 
Size: 436708 Color: 0
Size: 269825 Color: 4
Size: 269812 Color: 0

Bin 1845: 24013 of cap free
Amount of items: 3
Items: 
Size: 436637 Color: 1
Size: 269761 Color: 3
Size: 269590 Color: 0

Bin 1846: 24248 of cap free
Amount of items: 3
Items: 
Size: 436611 Color: 4
Size: 269576 Color: 3
Size: 269566 Color: 4

Bin 1847: 24806 of cap free
Amount of items: 3
Items: 
Size: 436568 Color: 3
Size: 269417 Color: 0
Size: 269210 Color: 2

Bin 1848: 26332 of cap free
Amount of items: 3
Items: 
Size: 436210 Color: 0
Size: 269137 Color: 3
Size: 268322 Color: 3

Bin 1849: 26460 of cap free
Amount of items: 3
Items: 
Size: 436470 Color: 3
Size: 268683 Color: 1
Size: 268388 Color: 2

Bin 1850: 27392 of cap free
Amount of items: 3
Items: 
Size: 436167 Color: 0
Size: 268235 Color: 4
Size: 268207 Color: 3

Bin 1851: 27756 of cap free
Amount of items: 3
Items: 
Size: 436147 Color: 0
Size: 268172 Color: 2
Size: 267926 Color: 1

Bin 1852: 28167 of cap free
Amount of items: 3
Items: 
Size: 436054 Color: 4
Size: 267903 Color: 3
Size: 267877 Color: 3

Bin 1853: 28644 of cap free
Amount of items: 3
Items: 
Size: 436035 Color: 3
Size: 267746 Color: 1
Size: 267576 Color: 1

Bin 1854: 29205 of cap free
Amount of items: 3
Items: 
Size: 435865 Color: 4
Size: 267479 Color: 2
Size: 267452 Color: 1

Bin 1855: 29776 of cap free
Amount of items: 3
Items: 
Size: 435861 Color: 2
Size: 267253 Color: 0
Size: 267111 Color: 4

Bin 1856: 29936 of cap free
Amount of items: 3
Items: 
Size: 435746 Color: 3
Size: 267363 Color: 2
Size: 266956 Color: 2

Bin 1857: 31725 of cap free
Amount of items: 3
Items: 
Size: 435731 Color: 0
Size: 266396 Color: 1
Size: 266149 Color: 4

Bin 1858: 32492 of cap free
Amount of items: 3
Items: 
Size: 435628 Color: 1
Size: 265995 Color: 3
Size: 265886 Color: 0

Bin 1859: 32788 of cap free
Amount of items: 3
Items: 
Size: 435536 Color: 4
Size: 265852 Color: 0
Size: 265825 Color: 1

Bin 1860: 33436 of cap free
Amount of items: 3
Items: 
Size: 435467 Color: 1
Size: 265553 Color: 2
Size: 265545 Color: 2

Bin 1861: 33721 of cap free
Amount of items: 3
Items: 
Size: 435359 Color: 2
Size: 265483 Color: 1
Size: 265438 Color: 3

Bin 1862: 34857 of cap free
Amount of items: 3
Items: 
Size: 435257 Color: 2
Size: 264999 Color: 0
Size: 264888 Color: 0

Bin 1863: 35418 of cap free
Amount of items: 3
Items: 
Size: 435210 Color: 2
Size: 264827 Color: 0
Size: 264546 Color: 3

Bin 1864: 35439 of cap free
Amount of items: 3
Items: 
Size: 435033 Color: 3
Size: 265046 Color: 2
Size: 264483 Color: 2

Bin 1865: 36213 of cap free
Amount of items: 3
Items: 
Size: 435032 Color: 0
Size: 264433 Color: 4
Size: 264323 Color: 0

Bin 1866: 36451 of cap free
Amount of items: 3
Items: 
Size: 435017 Color: 3
Size: 264286 Color: 2
Size: 264247 Color: 0

Bin 1867: 37561 of cap free
Amount of items: 3
Items: 
Size: 434961 Color: 2
Size: 263867 Color: 0
Size: 263612 Color: 2

Bin 1868: 37623 of cap free
Amount of items: 3
Items: 
Size: 434992 Color: 0
Size: 263747 Color: 4
Size: 263639 Color: 4

Bin 1869: 38349 of cap free
Amount of items: 3
Items: 
Size: 434779 Color: 3
Size: 263573 Color: 4
Size: 263300 Color: 3

Bin 1870: 38358 of cap free
Amount of items: 3
Items: 
Size: 434785 Color: 4
Size: 263478 Color: 2
Size: 263380 Color: 1

Bin 1871: 39140 of cap free
Amount of items: 3
Items: 
Size: 434734 Color: 4
Size: 263086 Color: 2
Size: 263041 Color: 3

Bin 1872: 39429 of cap free
Amount of items: 3
Items: 
Size: 434694 Color: 0
Size: 262961 Color: 4
Size: 262917 Color: 2

Bin 1873: 39732 of cap free
Amount of items: 3
Items: 
Size: 434684 Color: 2
Size: 262839 Color: 0
Size: 262746 Color: 4

Bin 1874: 40121 of cap free
Amount of items: 3
Items: 
Size: 434609 Color: 2
Size: 262680 Color: 0
Size: 262591 Color: 2

Bin 1875: 40681 of cap free
Amount of items: 3
Items: 
Size: 434607 Color: 3
Size: 262449 Color: 4
Size: 262264 Color: 3

Bin 1876: 41110 of cap free
Amount of items: 3
Items: 
Size: 434492 Color: 1
Size: 262263 Color: 3
Size: 262136 Color: 3

Bin 1877: 41891 of cap free
Amount of items: 3
Items: 
Size: 434475 Color: 3
Size: 261885 Color: 2
Size: 261750 Color: 2

Bin 1878: 42426 of cap free
Amount of items: 3
Items: 
Size: 434405 Color: 3
Size: 261625 Color: 4
Size: 261545 Color: 3

Bin 1879: 42635 of cap free
Amount of items: 3
Items: 
Size: 433960 Color: 4
Size: 261886 Color: 3
Size: 261520 Color: 1

Bin 1880: 43262 of cap free
Amount of items: 3
Items: 
Size: 433877 Color: 3
Size: 261434 Color: 2
Size: 261428 Color: 4

Bin 1881: 43593 of cap free
Amount of items: 3
Items: 
Size: 433773 Color: 1
Size: 261357 Color: 0
Size: 261278 Color: 1

Bin 1882: 43944 of cap free
Amount of items: 3
Items: 
Size: 433728 Color: 0
Size: 261177 Color: 4
Size: 261152 Color: 3

Bin 1883: 44434 of cap free
Amount of items: 3
Items: 
Size: 433589 Color: 0
Size: 261017 Color: 3
Size: 260961 Color: 3

Bin 1884: 44566 of cap free
Amount of items: 3
Items: 
Size: 433686 Color: 3
Size: 260916 Color: 1
Size: 260833 Color: 1

Bin 1885: 44880 of cap free
Amount of items: 3
Items: 
Size: 433569 Color: 0
Size: 260810 Color: 1
Size: 260742 Color: 2

Bin 1886: 45520 of cap free
Amount of items: 3
Items: 
Size: 433565 Color: 1
Size: 260510 Color: 4
Size: 260406 Color: 0

Bin 1887: 45783 of cap free
Amount of items: 3
Items: 
Size: 433538 Color: 0
Size: 260383 Color: 2
Size: 260297 Color: 2

Bin 1888: 46228 of cap free
Amount of items: 3
Items: 
Size: 433536 Color: 0
Size: 260197 Color: 4
Size: 260040 Color: 0

Bin 1889: 47382 of cap free
Amount of items: 3
Items: 
Size: 433352 Color: 1
Size: 259843 Color: 2
Size: 259424 Color: 3

Bin 1890: 47519 of cap free
Amount of items: 3
Items: 
Size: 433313 Color: 2
Size: 259878 Color: 1
Size: 259291 Color: 2

Bin 1891: 48422 of cap free
Amount of items: 3
Items: 
Size: 433284 Color: 3
Size: 259286 Color: 1
Size: 259009 Color: 3

Bin 1892: 49102 of cap free
Amount of items: 3
Items: 
Size: 433227 Color: 0
Size: 258867 Color: 2
Size: 258805 Color: 0

Bin 1893: 49419 of cap free
Amount of items: 3
Items: 
Size: 433129 Color: 0
Size: 258767 Color: 4
Size: 258686 Color: 1

Bin 1894: 49839 of cap free
Amount of items: 3
Items: 
Size: 433007 Color: 4
Size: 258589 Color: 1
Size: 258566 Color: 0

Bin 1895: 49905 of cap free
Amount of items: 3
Items: 
Size: 432912 Color: 2
Size: 258652 Color: 4
Size: 258532 Color: 1

Bin 1896: 50712 of cap free
Amount of items: 3
Items: 
Size: 432753 Color: 3
Size: 258355 Color: 4
Size: 258181 Color: 4

Bin 1897: 51388 of cap free
Amount of items: 3
Items: 
Size: 432797 Color: 4
Size: 257934 Color: 2
Size: 257882 Color: 0

Bin 1898: 51968 of cap free
Amount of items: 3
Items: 
Size: 432764 Color: 4
Size: 257797 Color: 2
Size: 257472 Color: 3

Bin 1899: 52579 of cap free
Amount of items: 3
Items: 
Size: 432720 Color: 1
Size: 257381 Color: 3
Size: 257321 Color: 1

Bin 1900: 52914 of cap free
Amount of items: 3
Items: 
Size: 432720 Color: 0
Size: 257203 Color: 4
Size: 257164 Color: 1

Bin 1901: 53018 of cap free
Amount of items: 3
Items: 
Size: 432622 Color: 4
Size: 257208 Color: 0
Size: 257153 Color: 1

Bin 1902: 53348 of cap free
Amount of items: 3
Items: 
Size: 432595 Color: 0
Size: 257079 Color: 3
Size: 256979 Color: 1

Bin 1903: 53489 of cap free
Amount of items: 3
Items: 
Size: 432586 Color: 2
Size: 256967 Color: 3
Size: 256959 Color: 3

Bin 1904: 53588 of cap free
Amount of items: 3
Items: 
Size: 432576 Color: 2
Size: 256935 Color: 1
Size: 256902 Color: 3

Bin 1905: 54320 of cap free
Amount of items: 3
Items: 
Size: 432478 Color: 1
Size: 256737 Color: 4
Size: 256466 Color: 1

Bin 1906: 55580 of cap free
Amount of items: 3
Items: 
Size: 431966 Color: 0
Size: 256290 Color: 2
Size: 256165 Color: 2

Bin 1907: 56237 of cap free
Amount of items: 3
Items: 
Size: 431944 Color: 4
Size: 256041 Color: 1
Size: 255779 Color: 3

Bin 1908: 56281 of cap free
Amount of items: 3
Items: 
Size: 431945 Color: 1
Size: 255953 Color: 4
Size: 255822 Color: 4

Bin 1909: 56715 of cap free
Amount of items: 3
Items: 
Size: 431932 Color: 2
Size: 255696 Color: 3
Size: 255658 Color: 4

Bin 1910: 56905 of cap free
Amount of items: 3
Items: 
Size: 431840 Color: 2
Size: 255648 Color: 4
Size: 255608 Color: 2

Bin 1911: 57070 of cap free
Amount of items: 3
Items: 
Size: 431784 Color: 4
Size: 255575 Color: 3
Size: 255572 Color: 4

Bin 1912: 57151 of cap free
Amount of items: 3
Items: 
Size: 431698 Color: 1
Size: 255586 Color: 4
Size: 255566 Color: 1

Bin 1913: 57284 of cap free
Amount of items: 3
Items: 
Size: 431686 Color: 0
Size: 255542 Color: 2
Size: 255489 Color: 2

Bin 1914: 57440 of cap free
Amount of items: 3
Items: 
Size: 431683 Color: 2
Size: 255470 Color: 1
Size: 255408 Color: 3

Bin 1915: 57695 of cap free
Amount of items: 3
Items: 
Size: 431649 Color: 2
Size: 255334 Color: 1
Size: 255323 Color: 2

Bin 1916: 58120 of cap free
Amount of items: 3
Items: 
Size: 431642 Color: 3
Size: 255160 Color: 4
Size: 255079 Color: 2

Bin 1917: 58497 of cap free
Amount of items: 3
Items: 
Size: 431176 Color: 4
Size: 255254 Color: 3
Size: 255074 Color: 3

Bin 1918: 58553 of cap free
Amount of items: 3
Items: 
Size: 431569 Color: 3
Size: 254946 Color: 1
Size: 254933 Color: 0

Bin 1919: 59322 of cap free
Amount of items: 3
Items: 
Size: 431068 Color: 1
Size: 254850 Color: 0
Size: 254761 Color: 3

Bin 1920: 59394 of cap free
Amount of items: 3
Items: 
Size: 430950 Color: 4
Size: 254916 Color: 1
Size: 254741 Color: 3

Bin 1921: 59973 of cap free
Amount of items: 3
Items: 
Size: 430862 Color: 4
Size: 254629 Color: 1
Size: 254537 Color: 4

Bin 1922: 60291 of cap free
Amount of items: 3
Items: 
Size: 430638 Color: 3
Size: 254631 Color: 4
Size: 254441 Color: 0

Bin 1923: 60612 of cap free
Amount of items: 3
Items: 
Size: 430588 Color: 0
Size: 254408 Color: 2
Size: 254393 Color: 2

Bin 1924: 60978 of cap free
Amount of items: 3
Items: 
Size: 430548 Color: 2
Size: 254393 Color: 1
Size: 254082 Color: 4

Bin 1925: 61984 of cap free
Amount of items: 3
Items: 
Size: 430152 Color: 3
Size: 254021 Color: 2
Size: 253844 Color: 1

Bin 1926: 62470 of cap free
Amount of items: 3
Items: 
Size: 430087 Color: 3
Size: 253742 Color: 2
Size: 253702 Color: 0

Bin 1927: 62922 of cap free
Amount of items: 3
Items: 
Size: 430045 Color: 1
Size: 253831 Color: 3
Size: 253203 Color: 3

Bin 1928: 64059 of cap free
Amount of items: 3
Items: 
Size: 429997 Color: 2
Size: 253097 Color: 1
Size: 252848 Color: 3

Bin 1929: 64172 of cap free
Amount of items: 3
Items: 
Size: 430012 Color: 1
Size: 252956 Color: 3
Size: 252861 Color: 0

Bin 1930: 64447 of cap free
Amount of items: 3
Items: 
Size: 429983 Color: 0
Size: 252826 Color: 1
Size: 252745 Color: 2

Bin 1931: 64746 of cap free
Amount of items: 3
Items: 
Size: 429981 Color: 2
Size: 252652 Color: 1
Size: 252622 Color: 1

Bin 1932: 65889 of cap free
Amount of items: 3
Items: 
Size: 429907 Color: 3
Size: 252336 Color: 1
Size: 251869 Color: 1

Bin 1933: 66091 of cap free
Amount of items: 3
Items: 
Size: 429766 Color: 0
Size: 252410 Color: 3
Size: 251734 Color: 2

Bin 1934: 67289 of cap free
Amount of items: 3
Items: 
Size: 429646 Color: 4
Size: 251675 Color: 1
Size: 251391 Color: 4

Bin 1935: 68579 of cap free
Amount of items: 3
Items: 
Size: 428978 Color: 4
Size: 251344 Color: 3
Size: 251100 Color: 2

Bin 1936: 69194 of cap free
Amount of items: 3
Items: 
Size: 428943 Color: 0
Size: 250942 Color: 2
Size: 250922 Color: 1

Bin 1937: 69368 of cap free
Amount of items: 3
Items: 
Size: 428715 Color: 1
Size: 251029 Color: 0
Size: 250889 Color: 1

Bin 1938: 69733 of cap free
Amount of items: 3
Items: 
Size: 428595 Color: 1
Size: 250856 Color: 0
Size: 250817 Color: 4

Bin 1939: 70003 of cap free
Amount of items: 3
Items: 
Size: 428558 Color: 3
Size: 250782 Color: 4
Size: 250658 Color: 4

Bin 1940: 70806 of cap free
Amount of items: 3
Items: 
Size: 428541 Color: 3
Size: 250334 Color: 4
Size: 250320 Color: 4

Bin 1941: 70906 of cap free
Amount of items: 3
Items: 
Size: 428297 Color: 1
Size: 250535 Color: 3
Size: 250263 Color: 4

Bin 1942: 71643 of cap free
Amount of items: 3
Items: 
Size: 428263 Color: 2
Size: 250057 Color: 4
Size: 250038 Color: 2

Bin 1943: 71848 of cap free
Amount of items: 3
Items: 
Size: 428178 Color: 2
Size: 249991 Color: 4
Size: 249984 Color: 0

Bin 1944: 71971 of cap free
Amount of items: 3
Items: 
Size: 428100 Color: 1
Size: 250078 Color: 2
Size: 249852 Color: 0

Bin 1945: 72375 of cap free
Amount of items: 3
Items: 
Size: 428057 Color: 3
Size: 249816 Color: 2
Size: 249753 Color: 1

Bin 1946: 72687 of cap free
Amount of items: 3
Items: 
Size: 427918 Color: 0
Size: 249752 Color: 2
Size: 249644 Color: 3

Bin 1947: 73239 of cap free
Amount of items: 3
Items: 
Size: 427892 Color: 2
Size: 249436 Color: 3
Size: 249434 Color: 0

Bin 1948: 73397 of cap free
Amount of items: 3
Items: 
Size: 427853 Color: 2
Size: 249408 Color: 3
Size: 249343 Color: 4

Bin 1949: 73930 of cap free
Amount of items: 3
Items: 
Size: 427767 Color: 0
Size: 249160 Color: 1
Size: 249144 Color: 4

Bin 1950: 74341 of cap free
Amount of items: 3
Items: 
Size: 427648 Color: 1
Size: 249040 Color: 2
Size: 248972 Color: 4

Bin 1951: 74694 of cap free
Amount of items: 3
Items: 
Size: 427557 Color: 3
Size: 248911 Color: 2
Size: 248839 Color: 4

Bin 1952: 74957 of cap free
Amount of items: 3
Items: 
Size: 427507 Color: 3
Size: 248777 Color: 1
Size: 248760 Color: 3

Bin 1953: 75405 of cap free
Amount of items: 3
Items: 
Size: 427463 Color: 0
Size: 248709 Color: 2
Size: 248424 Color: 1

Bin 1954: 76032 of cap free
Amount of items: 3
Items: 
Size: 427385 Color: 4
Size: 248402 Color: 0
Size: 248182 Color: 1

Bin 1955: 76255 of cap free
Amount of items: 3
Items: 
Size: 427292 Color: 0
Size: 248402 Color: 4
Size: 248052 Color: 1

Bin 1956: 77688 of cap free
Amount of items: 3
Items: 
Size: 427191 Color: 2
Size: 247783 Color: 0
Size: 247339 Color: 3

Bin 1957: 77742 of cap free
Amount of items: 3
Items: 
Size: 427210 Color: 0
Size: 247534 Color: 4
Size: 247515 Color: 0

Bin 1958: 78514 of cap free
Amount of items: 3
Items: 
Size: 427142 Color: 1
Size: 247263 Color: 2
Size: 247082 Color: 1

Bin 1959: 79312 of cap free
Amount of items: 3
Items: 
Size: 426975 Color: 4
Size: 246985 Color: 0
Size: 246729 Color: 0

Bin 1960: 79623 of cap free
Amount of items: 3
Items: 
Size: 426966 Color: 4
Size: 246708 Color: 3
Size: 246704 Color: 3

Bin 1961: 80131 of cap free
Amount of items: 3
Items: 
Size: 426916 Color: 0
Size: 246479 Color: 3
Size: 246475 Color: 1

Bin 1962: 80381 of cap free
Amount of items: 3
Items: 
Size: 426814 Color: 4
Size: 246429 Color: 3
Size: 246377 Color: 0

Bin 1963: 81099 of cap free
Amount of items: 3
Items: 
Size: 426690 Color: 1
Size: 246124 Color: 4
Size: 246088 Color: 1

Bin 1964: 81283 of cap free
Amount of items: 3
Items: 
Size: 426662 Color: 1
Size: 246039 Color: 2
Size: 246017 Color: 3

Bin 1965: 81928 of cap free
Amount of items: 3
Items: 
Size: 426637 Color: 4
Size: 245887 Color: 0
Size: 245549 Color: 2

Bin 1966: 82841 of cap free
Amount of items: 3
Items: 
Size: 426470 Color: 2
Size: 245504 Color: 1
Size: 245186 Color: 1

Bin 1967: 82999 of cap free
Amount of items: 3
Items: 
Size: 426539 Color: 1
Size: 245247 Color: 0
Size: 245216 Color: 4

Bin 1968: 85781 of cap free
Amount of items: 3
Items: 
Size: 425193 Color: 4
Size: 244731 Color: 3
Size: 244296 Color: 3

Bin 1969: 86154 of cap free
Amount of items: 3
Items: 
Size: 425026 Color: 1
Size: 244837 Color: 4
Size: 243984 Color: 4

Bin 1970: 86525 of cap free
Amount of items: 3
Items: 
Size: 425148 Color: 4
Size: 244283 Color: 2
Size: 244045 Color: 0

Bin 1971: 87234 of cap free
Amount of items: 3
Items: 
Size: 425089 Color: 4
Size: 243949 Color: 0
Size: 243729 Color: 0

Bin 1972: 88118 of cap free
Amount of items: 3
Items: 
Size: 425038 Color: 4
Size: 243555 Color: 3
Size: 243290 Color: 0

Bin 1973: 88731 of cap free
Amount of items: 3
Items: 
Size: 425006 Color: 0
Size: 243231 Color: 3
Size: 243033 Color: 0

Bin 1974: 90580 of cap free
Amount of items: 3
Items: 
Size: 424970 Color: 0
Size: 242476 Color: 3
Size: 241975 Color: 1

Bin 1975: 90628 of cap free
Amount of items: 3
Items: 
Size: 424975 Color: 3
Size: 242308 Color: 4
Size: 242090 Color: 2

Bin 1976: 91761 of cap free
Amount of items: 3
Items: 
Size: 424892 Color: 4
Size: 241751 Color: 3
Size: 241597 Color: 1

Bin 1977: 92207 of cap free
Amount of items: 3
Items: 
Size: 424875 Color: 1
Size: 241553 Color: 3
Size: 241366 Color: 4

Bin 1978: 92936 of cap free
Amount of items: 3
Items: 
Size: 424865 Color: 4
Size: 241219 Color: 0
Size: 240981 Color: 4

Bin 1979: 93462 of cap free
Amount of items: 3
Items: 
Size: 424729 Color: 3
Size: 240929 Color: 0
Size: 240881 Color: 0

Bin 1980: 93540 of cap free
Amount of items: 3
Items: 
Size: 424719 Color: 2
Size: 240871 Color: 4
Size: 240871 Color: 3

Bin 1981: 94190 of cap free
Amount of items: 3
Items: 
Size: 424639 Color: 3
Size: 240781 Color: 4
Size: 240391 Color: 3

Bin 1982: 94720 of cap free
Amount of items: 3
Items: 
Size: 424587 Color: 1
Size: 240351 Color: 3
Size: 240343 Color: 2

Bin 1983: 94855 of cap free
Amount of items: 3
Items: 
Size: 424506 Color: 3
Size: 240336 Color: 1
Size: 240304 Color: 4

Bin 1984: 95121 of cap free
Amount of items: 3
Items: 
Size: 424433 Color: 2
Size: 240274 Color: 0
Size: 240173 Color: 0

Bin 1985: 95879 of cap free
Amount of items: 3
Items: 
Size: 424476 Color: 0
Size: 239933 Color: 2
Size: 239713 Color: 4

Bin 1986: 96509 of cap free
Amount of items: 3
Items: 
Size: 424189 Color: 4
Size: 239968 Color: 0
Size: 239335 Color: 1

Bin 1987: 96623 of cap free
Amount of items: 3
Items: 
Size: 424336 Color: 0
Size: 239601 Color: 1
Size: 239441 Color: 3

Bin 1988: 97891 of cap free
Amount of items: 3
Items: 
Size: 424123 Color: 2
Size: 239050 Color: 0
Size: 238937 Color: 2

Bin 1989: 98073 of cap free
Amount of items: 3
Items: 
Size: 423978 Color: 0
Size: 239103 Color: 2
Size: 238847 Color: 4

Bin 1990: 98622 of cap free
Amount of items: 3
Items: 
Size: 423974 Color: 1
Size: 238707 Color: 2
Size: 238698 Color: 4

Bin 1991: 99271 of cap free
Amount of items: 3
Items: 
Size: 423872 Color: 3
Size: 238546 Color: 1
Size: 238312 Color: 3

Bin 1992: 99577 of cap free
Amount of items: 3
Items: 
Size: 423842 Color: 3
Size: 238300 Color: 2
Size: 238282 Color: 1

Bin 1993: 99857 of cap free
Amount of items: 3
Items: 
Size: 423669 Color: 3
Size: 238268 Color: 4
Size: 238207 Color: 2

Bin 1994: 100395 of cap free
Amount of items: 3
Items: 
Size: 423496 Color: 4
Size: 238138 Color: 1
Size: 237972 Color: 1

Bin 1995: 100763 of cap free
Amount of items: 3
Items: 
Size: 423418 Color: 0
Size: 237971 Color: 4
Size: 237849 Color: 3

Bin 1996: 101471 of cap free
Amount of items: 3
Items: 
Size: 422986 Color: 4
Size: 237777 Color: 2
Size: 237767 Color: 3

Bin 1997: 102102 of cap free
Amount of items: 3
Items: 
Size: 422974 Color: 3
Size: 237548 Color: 1
Size: 237377 Color: 1

Bin 1998: 102591 of cap free
Amount of items: 3
Items: 
Size: 422937 Color: 1
Size: 237310 Color: 2
Size: 237163 Color: 4

Bin 1999: 103187 of cap free
Amount of items: 3
Items: 
Size: 422617 Color: 4
Size: 237146 Color: 3
Size: 237051 Color: 4

Bin 2000: 103701 of cap free
Amount of items: 3
Items: 
Size: 422460 Color: 0
Size: 236977 Color: 3
Size: 236863 Color: 3

Bin 2001: 104401 of cap free
Amount of items: 3
Items: 
Size: 422582 Color: 3
Size: 236510 Color: 2
Size: 236508 Color: 1

Bin 2002: 105029 of cap free
Amount of items: 3
Items: 
Size: 422353 Color: 1
Size: 236418 Color: 4
Size: 236201 Color: 2

Bin 2003: 105124 of cap free
Amount of items: 3
Items: 
Size: 422434 Color: 4
Size: 236232 Color: 3
Size: 236211 Color: 3

Bin 2004: 105524 of cap free
Amount of items: 3
Items: 
Size: 422344 Color: 1
Size: 236100 Color: 0
Size: 236033 Color: 2

Bin 2005: 107474 of cap free
Amount of items: 3
Items: 
Size: 421188 Color: 4
Size: 235884 Color: 3
Size: 235455 Color: 1

Bin 2006: 107595 of cap free
Amount of items: 3
Items: 
Size: 421222 Color: 3
Size: 235694 Color: 2
Size: 235490 Color: 3

Bin 2007: 108467 of cap free
Amount of items: 3
Items: 
Size: 421159 Color: 4
Size: 235197 Color: 3
Size: 235178 Color: 2

Bin 2008: 109007 of cap free
Amount of items: 3
Items: 
Size: 421108 Color: 3
Size: 234962 Color: 2
Size: 234924 Color: 1

Bin 2009: 109374 of cap free
Amount of items: 3
Items: 
Size: 421055 Color: 2
Size: 234861 Color: 1
Size: 234711 Color: 3

Bin 2010: 109593 of cap free
Amount of items: 3
Items: 
Size: 421053 Color: 1
Size: 234710 Color: 0
Size: 234645 Color: 4

Bin 2011: 110094 of cap free
Amount of items: 3
Items: 
Size: 420786 Color: 1
Size: 234627 Color: 0
Size: 234494 Color: 4

Bin 2012: 110499 of cap free
Amount of items: 3
Items: 
Size: 420782 Color: 2
Size: 234407 Color: 1
Size: 234313 Color: 1

Bin 2013: 110662 of cap free
Amount of items: 3
Items: 
Size: 420767 Color: 4
Size: 234289 Color: 2
Size: 234283 Color: 0

Bin 2014: 111208 of cap free
Amount of items: 3
Items: 
Size: 420620 Color: 1
Size: 234117 Color: 4
Size: 234056 Color: 1

Bin 2015: 111838 of cap free
Amount of items: 3
Items: 
Size: 420558 Color: 4
Size: 233996 Color: 0
Size: 233609 Color: 0

Bin 2016: 111992 of cap free
Amount of items: 3
Items: 
Size: 420368 Color: 1
Size: 234047 Color: 4
Size: 233594 Color: 4

Bin 2017: 113295 of cap free
Amount of items: 3
Items: 
Size: 420431 Color: 4
Size: 233176 Color: 3
Size: 233099 Color: 1

Bin 2018: 114370 of cap free
Amount of items: 3
Items: 
Size: 420382 Color: 4
Size: 232752 Color: 0
Size: 232497 Color: 2

Bin 2019: 115551 of cap free
Amount of items: 3
Items: 
Size: 420279 Color: 3
Size: 232209 Color: 1
Size: 231962 Color: 2

Bin 2020: 116250 of cap free
Amount of items: 3
Items: 
Size: 419982 Color: 1
Size: 231919 Color: 3
Size: 231850 Color: 0

Bin 2021: 117057 of cap free
Amount of items: 3
Items: 
Size: 419641 Color: 4
Size: 231654 Color: 3
Size: 231649 Color: 3

Bin 2022: 117860 of cap free
Amount of items: 3
Items: 
Size: 419519 Color: 0
Size: 231375 Color: 1
Size: 231247 Color: 3

Bin 2023: 119153 of cap free
Amount of items: 3
Items: 
Size: 419508 Color: 4
Size: 230889 Color: 3
Size: 230451 Color: 4

Bin 2024: 119162 of cap free
Amount of items: 3
Items: 
Size: 419511 Color: 3
Size: 230676 Color: 2
Size: 230652 Color: 3

Bin 2025: 119820 of cap free
Amount of items: 3
Items: 
Size: 419504 Color: 1
Size: 230356 Color: 3
Size: 230321 Color: 1

Bin 2026: 120370 of cap free
Amount of items: 3
Items: 
Size: 419418 Color: 1
Size: 230290 Color: 0
Size: 229923 Color: 4

Bin 2027: 121032 of cap free
Amount of items: 3
Items: 
Size: 419404 Color: 4
Size: 229876 Color: 2
Size: 229689 Color: 4

Bin 2028: 122555 of cap free
Amount of items: 3
Items: 
Size: 419340 Color: 3
Size: 229404 Color: 0
Size: 228702 Color: 1

Bin 2029: 123830 of cap free
Amount of items: 3
Items: 
Size: 419058 Color: 4
Size: 228632 Color: 0
Size: 228481 Color: 0

Bin 2030: 124498 of cap free
Amount of items: 3
Items: 
Size: 418893 Color: 1
Size: 228466 Color: 2
Size: 228144 Color: 3

Bin 2031: 125052 of cap free
Amount of items: 3
Items: 
Size: 418769 Color: 0
Size: 228133 Color: 3
Size: 228047 Color: 1

Bin 2032: 125409 of cap free
Amount of items: 3
Items: 
Size: 418767 Color: 2
Size: 227995 Color: 3
Size: 227830 Color: 3

Bin 2033: 126196 of cap free
Amount of items: 3
Items: 
Size: 418766 Color: 1
Size: 227557 Color: 3
Size: 227482 Color: 0

Bin 2034: 126618 of cap free
Amount of items: 3
Items: 
Size: 418734 Color: 4
Size: 227441 Color: 3
Size: 227208 Color: 4

Bin 2035: 126890 of cap free
Amount of items: 3
Items: 
Size: 418612 Color: 0
Size: 227476 Color: 4
Size: 227023 Color: 1

Bin 2036: 128434 of cap free
Amount of items: 3
Items: 
Size: 418531 Color: 3
Size: 226638 Color: 1
Size: 226398 Color: 1

Bin 2037: 128493 of cap free
Amount of items: 3
Items: 
Size: 418305 Color: 2
Size: 226878 Color: 3
Size: 226325 Color: 3

Bin 2038: 129257 of cap free
Amount of items: 3
Items: 
Size: 418216 Color: 0
Size: 226323 Color: 2
Size: 226205 Color: 2

Bin 2039: 129529 of cap free
Amount of items: 3
Items: 
Size: 418246 Color: 2
Size: 226163 Color: 4
Size: 226063 Color: 3

Bin 2040: 129889 of cap free
Amount of items: 3
Items: 
Size: 418190 Color: 1
Size: 226023 Color: 0
Size: 225899 Color: 0

Bin 2041: 130170 of cap free
Amount of items: 3
Items: 
Size: 418098 Color: 2
Size: 225897 Color: 1
Size: 225836 Color: 0

Bin 2042: 130882 of cap free
Amount of items: 3
Items: 
Size: 418085 Color: 4
Size: 225597 Color: 1
Size: 225437 Color: 4

Bin 2043: 131384 of cap free
Amount of items: 3
Items: 
Size: 417928 Color: 2
Size: 225374 Color: 1
Size: 225315 Color: 4

Bin 2044: 131598 of cap free
Amount of items: 3
Items: 
Size: 417896 Color: 2
Size: 225288 Color: 4
Size: 225219 Color: 4

Bin 2045: 132068 of cap free
Amount of items: 3
Items: 
Size: 417856 Color: 1
Size: 225074 Color: 4
Size: 225003 Color: 1

Bin 2046: 132398 of cap free
Amount of items: 3
Items: 
Size: 417795 Color: 4
Size: 224929 Color: 3
Size: 224879 Color: 3

Bin 2047: 132787 of cap free
Amount of items: 3
Items: 
Size: 417493 Color: 4
Size: 224865 Color: 3
Size: 224856 Color: 3

Bin 2048: 133304 of cap free
Amount of items: 3
Items: 
Size: 417588 Color: 3
Size: 224597 Color: 2
Size: 224512 Color: 4

Bin 2049: 133706 of cap free
Amount of items: 3
Items: 
Size: 417464 Color: 0
Size: 224453 Color: 1
Size: 224378 Color: 2

Bin 2050: 134091 of cap free
Amount of items: 3
Items: 
Size: 417451 Color: 0
Size: 224287 Color: 4
Size: 224172 Color: 1

Bin 2051: 134169 of cap free
Amount of items: 3
Items: 
Size: 417451 Color: 4
Size: 224192 Color: 1
Size: 224189 Color: 3

Bin 2052: 134920 of cap free
Amount of items: 3
Items: 
Size: 417025 Color: 1
Size: 224067 Color: 4
Size: 223989 Color: 1

Bin 2053: 135425 of cap free
Amount of items: 3
Items: 
Size: 416927 Color: 3
Size: 223884 Color: 2
Size: 223765 Color: 4

Bin 2054: 135780 of cap free
Amount of items: 3
Items: 
Size: 416872 Color: 2
Size: 223765 Color: 0
Size: 223584 Color: 4

Bin 2055: 136893 of cap free
Amount of items: 3
Items: 
Size: 416829 Color: 2
Size: 223191 Color: 0
Size: 223088 Color: 1

Bin 2056: 137328 of cap free
Amount of items: 3
Items: 
Size: 416821 Color: 2
Size: 222931 Color: 4
Size: 222921 Color: 1

Bin 2057: 139175 of cap free
Amount of items: 3
Items: 
Size: 416790 Color: 1
Size: 222061 Color: 3
Size: 221975 Color: 2

Bin 2058: 139539 of cap free
Amount of items: 3
Items: 
Size: 416783 Color: 2
Size: 221943 Color: 0
Size: 221736 Color: 0

Bin 2059: 140171 of cap free
Amount of items: 3
Items: 
Size: 416772 Color: 1
Size: 221620 Color: 2
Size: 221438 Color: 0

Bin 2060: 142014 of cap free
Amount of items: 3
Items: 
Size: 416767 Color: 2
Size: 220628 Color: 3
Size: 220592 Color: 0

Bin 2061: 142597 of cap free
Amount of items: 3
Items: 
Size: 416642 Color: 2
Size: 220484 Color: 4
Size: 220278 Color: 3

Bin 2062: 142696 of cap free
Amount of items: 3
Items: 
Size: 416523 Color: 3
Size: 220718 Color: 2
Size: 220064 Color: 4

Bin 2063: 143172 of cap free
Amount of items: 3
Items: 
Size: 416633 Color: 2
Size: 220129 Color: 4
Size: 220067 Color: 0

Bin 2064: 143871 of cap free
Amount of items: 3
Items: 
Size: 416342 Color: 2
Size: 219954 Color: 3
Size: 219834 Color: 2

Bin 2065: 144768 of cap free
Amount of items: 3
Items: 
Size: 416297 Color: 2
Size: 219487 Color: 0
Size: 219449 Color: 1

Bin 2066: 144957 of cap free
Amount of items: 3
Items: 
Size: 416192 Color: 2
Size: 219442 Color: 4
Size: 219410 Color: 0

Bin 2067: 146196 of cap free
Amount of items: 3
Items: 
Size: 415288 Color: 3
Size: 219281 Color: 1
Size: 219236 Color: 1

Bin 2068: 147343 of cap free
Amount of items: 3
Items: 
Size: 414262 Color: 0
Size: 219218 Color: 1
Size: 219178 Color: 4

Bin 2069: 147656 of cap free
Amount of items: 3
Items: 
Size: 414153 Color: 3
Size: 219131 Color: 4
Size: 219061 Color: 1

Bin 2070: 147681 of cap free
Amount of items: 3
Items: 
Size: 414105 Color: 1
Size: 219175 Color: 3
Size: 219040 Color: 0

Bin 2071: 149072 of cap free
Amount of items: 3
Items: 
Size: 413281 Color: 3
Size: 218840 Color: 4
Size: 218808 Color: 0

Bin 2072: 149674 of cap free
Amount of items: 3
Items: 
Size: 413017 Color: 3
Size: 218671 Color: 0
Size: 218639 Color: 2

Bin 2073: 150275 of cap free
Amount of items: 3
Items: 
Size: 412745 Color: 3
Size: 218598 Color: 1
Size: 218383 Color: 4

Bin 2074: 151382 of cap free
Amount of items: 3
Items: 
Size: 412677 Color: 3
Size: 217973 Color: 1
Size: 217969 Color: 1

Bin 2075: 152110 of cap free
Amount of items: 3
Items: 
Size: 412085 Color: 3
Size: 217907 Color: 4
Size: 217899 Color: 0

Bin 2076: 154113 of cap free
Amount of items: 3
Items: 
Size: 409984 Color: 0
Size: 218106 Color: 3
Size: 217798 Color: 0

Bin 2077: 154853 of cap free
Amount of items: 3
Items: 
Size: 409926 Color: 2
Size: 217667 Color: 1
Size: 217555 Color: 4

Bin 2078: 155467 of cap free
Amount of items: 3
Items: 
Size: 409920 Color: 4
Size: 217455 Color: 2
Size: 217159 Color: 1

Bin 2079: 156439 of cap free
Amount of items: 3
Items: 
Size: 409853 Color: 1
Size: 216968 Color: 0
Size: 216741 Color: 0

Bin 2080: 157551 of cap free
Amount of items: 3
Items: 
Size: 409910 Color: 0
Size: 216367 Color: 4
Size: 216173 Color: 1

Bin 2081: 157738 of cap free
Amount of items: 3
Items: 
Size: 409751 Color: 3
Size: 216419 Color: 0
Size: 216093 Color: 2

Bin 2082: 158715 of cap free
Amount of items: 3
Items: 
Size: 409405 Color: 3
Size: 216000 Color: 1
Size: 215881 Color: 2

Bin 2083: 159724 of cap free
Amount of items: 3
Items: 
Size: 409337 Color: 1
Size: 215572 Color: 3
Size: 215368 Color: 2

Bin 2084: 160316 of cap free
Amount of items: 3
Items: 
Size: 409283 Color: 2
Size: 215266 Color: 4
Size: 215136 Color: 4

Bin 2085: 160704 of cap free
Amount of items: 3
Items: 
Size: 409245 Color: 0
Size: 215046 Color: 3
Size: 215006 Color: 3

Bin 2086: 161634 of cap free
Amount of items: 3
Items: 
Size: 409041 Color: 3
Size: 214665 Color: 4
Size: 214661 Color: 4

Bin 2087: 162871 of cap free
Amount of items: 3
Items: 
Size: 408975 Color: 4
Size: 214089 Color: 3
Size: 214066 Color: 0

Bin 2088: 163482 of cap free
Amount of items: 3
Items: 
Size: 408941 Color: 2
Size: 214423 Color: 4
Size: 213155 Color: 3

Bin 2089: 163756 of cap free
Amount of items: 3
Items: 
Size: 408941 Color: 4
Size: 214025 Color: 2
Size: 213279 Color: 2

Bin 2090: 165150 of cap free
Amount of items: 3
Items: 
Size: 408922 Color: 4
Size: 213154 Color: 1
Size: 212775 Color: 4

Bin 2091: 165759 of cap free
Amount of items: 3
Items: 
Size: 408893 Color: 4
Size: 212677 Color: 1
Size: 212672 Color: 0

Bin 2092: 167188 of cap free
Amount of items: 3
Items: 
Size: 408852 Color: 4
Size: 212041 Color: 0
Size: 211920 Color: 0

Bin 2093: 168089 of cap free
Amount of items: 3
Items: 
Size: 408752 Color: 1
Size: 212443 Color: 4
Size: 210717 Color: 2

Bin 2094: 168479 of cap free
Amount of items: 3
Items: 
Size: 408776 Color: 4
Size: 211546 Color: 2
Size: 211200 Color: 3

Bin 2095: 170371 of cap free
Amount of items: 3
Items: 
Size: 408741 Color: 2
Size: 210453 Color: 0
Size: 210436 Color: 1

Bin 2096: 174131 of cap free
Amount of items: 3
Items: 
Size: 408717 Color: 1
Size: 209735 Color: 2
Size: 207418 Color: 0

Bin 2097: 177872 of cap free
Amount of items: 3
Items: 
Size: 408604 Color: 4
Size: 206939 Color: 3
Size: 206586 Color: 4

Bin 2098: 181027 of cap free
Amount of items: 3
Items: 
Size: 408403 Color: 4
Size: 205729 Color: 3
Size: 204842 Color: 2

Bin 2099: 181422 of cap free
Amount of items: 3
Items: 
Size: 408400 Color: 3
Size: 205816 Color: 4
Size: 204363 Color: 1

Bin 2100: 183425 of cap free
Amount of items: 2
Items: 
Size: 408318 Color: 0
Size: 408258 Color: 4

Bin 2101: 183662 of cap free
Amount of items: 2
Items: 
Size: 408235 Color: 3
Size: 408104 Color: 2

Bin 2102: 183816 of cap free
Amount of items: 3
Items: 
Size: 408178 Color: 3
Size: 204005 Color: 2
Size: 204002 Color: 4

Bin 2103: 184152 of cap free
Amount of items: 2
Items: 
Size: 407953 Color: 4
Size: 407896 Color: 2

Bin 2104: 184496 of cap free
Amount of items: 3
Items: 
Size: 407787 Color: 3
Size: 204002 Color: 2
Size: 203716 Color: 0

Bin 2105: 185214 of cap free
Amount of items: 2
Items: 
Size: 407518 Color: 2
Size: 407269 Color: 3

Bin 2106: 185576 of cap free
Amount of items: 2
Items: 
Size: 407247 Color: 0
Size: 407178 Color: 2

Bin 2107: 185717 of cap free
Amount of items: 2
Items: 
Size: 407162 Color: 2
Size: 407122 Color: 0

Bin 2108: 186310 of cap free
Amount of items: 2
Items: 
Size: 406850 Color: 4
Size: 406841 Color: 0

Bin 2109: 186520 of cap free
Amount of items: 2
Items: 
Size: 406768 Color: 4
Size: 406713 Color: 0

Bin 2110: 186543 of cap free
Amount of items: 2
Items: 
Size: 406751 Color: 4
Size: 406707 Color: 0

Bin 2111: 186740 of cap free
Amount of items: 2
Items: 
Size: 406656 Color: 1
Size: 406605 Color: 2

Bin 2112: 186784 of cap free
Amount of items: 3
Items: 
Size: 406549 Color: 0
Size: 203623 Color: 1
Size: 203045 Color: 3

Bin 2113: 187092 of cap free
Amount of items: 2
Items: 
Size: 406455 Color: 3
Size: 406454 Color: 2

Bin 2114: 187311 of cap free
Amount of items: 2
Items: 
Size: 406389 Color: 2
Size: 406301 Color: 0

Bin 2115: 187538 of cap free
Amount of items: 2
Items: 
Size: 406271 Color: 4
Size: 406192 Color: 3

Bin 2116: 187639 of cap free
Amount of items: 2
Items: 
Size: 406181 Color: 4
Size: 406181 Color: 3

Bin 2117: 187750 of cap free
Amount of items: 2
Items: 
Size: 406166 Color: 0
Size: 406085 Color: 2

Bin 2118: 187910 of cap free
Amount of items: 3
Items: 
Size: 406029 Color: 0
Size: 203042 Color: 1
Size: 203020 Color: 0

Bin 2119: 188352 of cap free
Amount of items: 2
Items: 
Size: 405833 Color: 1
Size: 405816 Color: 2

Bin 2120: 188737 of cap free
Amount of items: 2
Items: 
Size: 405641 Color: 4
Size: 405623 Color: 1

Bin 2121: 188912 of cap free
Amount of items: 2
Items: 
Size: 405562 Color: 2
Size: 405527 Color: 0

Bin 2122: 189000 of cap free
Amount of items: 2
Items: 
Size: 405516 Color: 0
Size: 405485 Color: 2

Bin 2123: 189332 of cap free
Amount of items: 2
Items: 
Size: 405338 Color: 0
Size: 405331 Color: 4

Bin 2124: 189525 of cap free
Amount of items: 2
Items: 
Size: 405305 Color: 2
Size: 405171 Color: 4

Bin 2125: 189739 of cap free
Amount of items: 3
Items: 
Size: 405041 Color: 3
Size: 202925 Color: 2
Size: 202296 Color: 0

Bin 2126: 190120 of cap free
Amount of items: 2
Items: 
Size: 405030 Color: 3
Size: 404851 Color: 4

Bin 2127: 190512 of cap free
Amount of items: 2
Items: 
Size: 404766 Color: 0
Size: 404723 Color: 1

Bin 2128: 190717 of cap free
Amount of items: 2
Items: 
Size: 404693 Color: 2
Size: 404591 Color: 4

Bin 2129: 190931 of cap free
Amount of items: 2
Items: 
Size: 404535 Color: 4
Size: 404535 Color: 1

Bin 2130: 191402 of cap free
Amount of items: 2
Items: 
Size: 404393 Color: 2
Size: 404206 Color: 3

Bin 2131: 191628 of cap free
Amount of items: 3
Items: 
Size: 404239 Color: 2
Size: 202080 Color: 0
Size: 202054 Color: 1

Bin 2132: 191887 of cap free
Amount of items: 2
Items: 
Size: 404062 Color: 0
Size: 404052 Color: 1

Bin 2133: 192047 of cap free
Amount of items: 2
Items: 
Size: 404017 Color: 3
Size: 403937 Color: 4

Bin 2134: 192201 of cap free
Amount of items: 2
Items: 
Size: 403913 Color: 3
Size: 403887 Color: 1

Bin 2135: 192338 of cap free
Amount of items: 2
Items: 
Size: 403842 Color: 2
Size: 403821 Color: 4

Bin 2136: 192600 of cap free
Amount of items: 2
Items: 
Size: 403722 Color: 2
Size: 403679 Color: 0

Bin 2137: 192793 of cap free
Amount of items: 2
Items: 
Size: 403637 Color: 1
Size: 403571 Color: 4

Bin 2138: 193090 of cap free
Amount of items: 2
Items: 
Size: 403466 Color: 3
Size: 403445 Color: 0

Bin 2139: 193462 of cap free
Amount of items: 3
Items: 
Size: 403278 Color: 3
Size: 201667 Color: 0
Size: 201594 Color: 1

Bin 2140: 193641 of cap free
Amount of items: 2
Items: 
Size: 403199 Color: 4
Size: 403161 Color: 1

Bin 2141: 193764 of cap free
Amount of items: 2
Items: 
Size: 403147 Color: 2
Size: 403090 Color: 1

Bin 2142: 193840 of cap free
Amount of items: 2
Items: 
Size: 403082 Color: 0
Size: 403079 Color: 1

Bin 2143: 193963 of cap free
Amount of items: 3
Items: 
Size: 403061 Color: 0
Size: 201509 Color: 1
Size: 201468 Color: 3

Bin 2144: 194244 of cap free
Amount of items: 2
Items: 
Size: 402886 Color: 4
Size: 402871 Color: 1

Bin 2145: 194571 of cap free
Amount of items: 3
Items: 
Size: 402685 Color: 0
Size: 201403 Color: 4
Size: 201342 Color: 3

Bin 2146: 194815 of cap free
Amount of items: 2
Items: 
Size: 402683 Color: 1
Size: 402503 Color: 4

Bin 2147: 194849 of cap free
Amount of items: 2
Items: 
Size: 402667 Color: 1
Size: 402485 Color: 0

Bin 2148: 194943 of cap free
Amount of items: 2
Items: 
Size: 402607 Color: 1
Size: 402451 Color: 4

Bin 2149: 195000 of cap free
Amount of items: 3
Items: 
Size: 402557 Color: 1
Size: 201259 Color: 2
Size: 201185 Color: 1

Bin 2150: 195156 of cap free
Amount of items: 3
Items: 
Size: 402419 Color: 2
Size: 201273 Color: 1
Size: 201153 Color: 0

Bin 2151: 195708 of cap free
Amount of items: 2
Items: 
Size: 402155 Color: 0
Size: 402138 Color: 2

Bin 2152: 195889 of cap free
Amount of items: 2
Items: 
Size: 402072 Color: 4
Size: 402040 Color: 2

Bin 2153: 196085 of cap free
Amount of items: 3
Items: 
Size: 401888 Color: 4
Size: 201036 Color: 1
Size: 200992 Color: 1

Bin 2154: 196364 of cap free
Amount of items: 2
Items: 
Size: 401822 Color: 3
Size: 401815 Color: 2

Bin 2155: 196457 of cap free
Amount of items: 3
Items: 
Size: 401733 Color: 4
Size: 200966 Color: 1
Size: 200845 Color: 2

Bin 2156: 196664 of cap free
Amount of items: 2
Items: 
Size: 401689 Color: 2
Size: 401648 Color: 0

Bin 2157: 196895 of cap free
Amount of items: 2
Items: 
Size: 401554 Color: 1
Size: 401552 Color: 2

Bin 2158: 196931 of cap free
Amount of items: 2
Items: 
Size: 401540 Color: 2
Size: 401530 Color: 1

Bin 2159: 197069 of cap free
Amount of items: 2
Items: 
Size: 401490 Color: 2
Size: 401442 Color: 3

Bin 2160: 197400 of cap free
Amount of items: 2
Items: 
Size: 401376 Color: 2
Size: 401225 Color: 4

Bin 2161: 197730 of cap free
Amount of items: 2
Items: 
Size: 401224 Color: 3
Size: 401047 Color: 0

Bin 2162: 197810 of cap free
Amount of items: 2
Items: 
Size: 401222 Color: 3
Size: 400969 Color: 1

Bin 2163: 198104 of cap free
Amount of items: 2
Items: 
Size: 400961 Color: 3
Size: 400936 Color: 1

Bin 2164: 198239 of cap free
Amount of items: 2
Items: 
Size: 400923 Color: 0
Size: 400839 Color: 3

Bin 2165: 198508 of cap free
Amount of items: 2
Items: 
Size: 400792 Color: 0
Size: 400701 Color: 3

Bin 2166: 198705 of cap free
Amount of items: 2
Items: 
Size: 400656 Color: 4
Size: 400640 Color: 2

Bin 2167: 199067 of cap free
Amount of items: 2
Items: 
Size: 400629 Color: 4
Size: 400305 Color: 2

Bin 2168: 199544 of cap free
Amount of items: 2
Items: 
Size: 400274 Color: 4
Size: 400183 Color: 3

Bin 2169: 199625 of cap free
Amount of items: 2
Items: 
Size: 400236 Color: 4
Size: 400140 Color: 0

Bin 2170: 199712 of cap free
Amount of items: 2
Items: 
Size: 400215 Color: 4
Size: 400074 Color: 3

Bin 2171: 200048 of cap free
Amount of items: 1
Items: 
Size: 799953 Color: 2

Bin 2172: 200107 of cap free
Amount of items: 1
Items: 
Size: 799894 Color: 0

Bin 2173: 200130 of cap free
Amount of items: 2
Items: 
Size: 399936 Color: 2
Size: 399935 Color: 0

Bin 2174: 200138 of cap free
Amount of items: 1
Items: 
Size: 799863 Color: 0

Bin 2175: 200189 of cap free
Amount of items: 1
Items: 
Size: 799812 Color: 1

Bin 2176: 200260 of cap free
Amount of items: 1
Items: 
Size: 799741 Color: 1

Bin 2177: 200318 of cap free
Amount of items: 2
Items: 
Size: 399865 Color: 3
Size: 399818 Color: 0

Bin 2178: 200343 of cap free
Amount of items: 1
Items: 
Size: 799658 Color: 4

Bin 2179: 200560 of cap free
Amount of items: 1
Items: 
Size: 799441 Color: 1

Bin 2180: 200654 of cap free
Amount of items: 2
Items: 
Size: 399679 Color: 0
Size: 399668 Color: 1

Bin 2181: 200666 of cap free
Amount of items: 1
Items: 
Size: 799335 Color: 0

Bin 2182: 200696 of cap free
Amount of items: 1
Items: 
Size: 799305 Color: 1

Bin 2183: 200795 of cap free
Amount of items: 2
Items: 
Size: 399657 Color: 4
Size: 399549 Color: 3

Bin 2184: 200808 of cap free
Amount of items: 1
Items: 
Size: 799193 Color: 4

Bin 2185: 200840 of cap free
Amount of items: 1
Items: 
Size: 799161 Color: 2

Bin 2186: 200873 of cap free
Amount of items: 1
Items: 
Size: 799128 Color: 0

Bin 2187: 200884 of cap free
Amount of items: 2
Items: 
Size: 399570 Color: 4
Size: 399547 Color: 0

Bin 2188: 200945 of cap free
Amount of items: 1
Items: 
Size: 799056 Color: 0

Bin 2189: 201229 of cap free
Amount of items: 1
Items: 
Size: 798772 Color: 4

Bin 2190: 201337 of cap free
Amount of items: 1
Items: 
Size: 798664 Color: 1

Bin 2191: 201339 of cap free
Amount of items: 2
Items: 
Size: 399381 Color: 2
Size: 399281 Color: 4

Bin 2192: 201441 of cap free
Amount of items: 1
Items: 
Size: 798560 Color: 2

Bin 2193: 201504 of cap free
Amount of items: 1
Items: 
Size: 798497 Color: 4

Bin 2194: 201509 of cap free
Amount of items: 1
Items: 
Size: 798492 Color: 0

Bin 2195: 201532 of cap free
Amount of items: 1
Items: 
Size: 798469 Color: 2

Bin 2196: 201662 of cap free
Amount of items: 2
Items: 
Size: 399215 Color: 0
Size: 399124 Color: 4

Bin 2197: 201706 of cap free
Amount of items: 1
Items: 
Size: 798295 Color: 3

Bin 2198: 201847 of cap free
Amount of items: 1
Items: 
Size: 798154 Color: 4

Bin 2199: 201995 of cap free
Amount of items: 1
Items: 
Size: 798006 Color: 2

Bin 2200: 202116 of cap free
Amount of items: 1
Items: 
Size: 797885 Color: 1

Bin 2201: 202132 of cap free
Amount of items: 1
Items: 
Size: 797869 Color: 0

Bin 2202: 202188 of cap free
Amount of items: 1
Items: 
Size: 797813 Color: 2

Bin 2203: 202189 of cap free
Amount of items: 1
Items: 
Size: 797812 Color: 0

Bin 2204: 202204 of cap free
Amount of items: 2
Items: 
Size: 398958 Color: 2
Size: 398839 Color: 0

Bin 2205: 202367 of cap free
Amount of items: 1
Items: 
Size: 797634 Color: 2

Bin 2206: 202386 of cap free
Amount of items: 1
Items: 
Size: 797615 Color: 4

Bin 2207: 202412 of cap free
Amount of items: 1
Items: 
Size: 797589 Color: 4

Bin 2208: 202423 of cap free
Amount of items: 2
Items: 
Size: 398800 Color: 3
Size: 398778 Color: 0

Bin 2209: 202468 of cap free
Amount of items: 1
Items: 
Size: 797533 Color: 0

Bin 2210: 202472 of cap free
Amount of items: 2
Items: 
Size: 398767 Color: 3
Size: 398762 Color: 1

Bin 2211: 202516 of cap free
Amount of items: 1
Items: 
Size: 797485 Color: 1

Bin 2212: 202550 of cap free
Amount of items: 1
Items: 
Size: 797451 Color: 3

Bin 2213: 202627 of cap free
Amount of items: 1
Items: 
Size: 797374 Color: 3

Bin 2214: 202767 of cap free
Amount of items: 1
Items: 
Size: 797234 Color: 4

Bin 2215: 202823 of cap free
Amount of items: 1
Items: 
Size: 797178 Color: 3

Bin 2216: 202833 of cap free
Amount of items: 1
Items: 
Size: 797168 Color: 3

Bin 2217: 202863 of cap free
Amount of items: 1
Items: 
Size: 797138 Color: 4

Bin 2218: 202875 of cap free
Amount of items: 2
Items: 
Size: 398603 Color: 1
Size: 398523 Color: 3

Bin 2219: 202917 of cap free
Amount of items: 1
Items: 
Size: 797084 Color: 4

Bin 2220: 203007 of cap free
Amount of items: 1
Items: 
Size: 796994 Color: 1

Bin 2221: 203057 of cap free
Amount of items: 1
Items: 
Size: 796944 Color: 2

Bin 2222: 203074 of cap free
Amount of items: 2
Items: 
Size: 398497 Color: 2
Size: 398430 Color: 0

Bin 2223: 203084 of cap free
Amount of items: 1
Items: 
Size: 796917 Color: 2

Bin 2224: 203129 of cap free
Amount of items: 1
Items: 
Size: 796872 Color: 2

Bin 2225: 203153 of cap free
Amount of items: 1
Items: 
Size: 796848 Color: 2

Bin 2226: 203209 of cap free
Amount of items: 1
Items: 
Size: 796792 Color: 4

Bin 2227: 203364 of cap free
Amount of items: 1
Items: 
Size: 796637 Color: 4

Bin 2228: 203394 of cap free
Amount of items: 2
Items: 
Size: 398313 Color: 4
Size: 398294 Color: 2

Bin 2229: 203404 of cap free
Amount of items: 1
Items: 
Size: 796597 Color: 0

Bin 2230: 203525 of cap free
Amount of items: 1
Items: 
Size: 796476 Color: 2

Bin 2231: 203577 of cap free
Amount of items: 1
Items: 
Size: 796424 Color: 1

Bin 2232: 203599 of cap free
Amount of items: 1
Items: 
Size: 796402 Color: 1

Bin 2233: 203706 of cap free
Amount of items: 1
Items: 
Size: 796295 Color: 1

Bin 2234: 203743 of cap free
Amount of items: 1
Items: 
Size: 796258 Color: 0

Bin 2235: 203906 of cap free
Amount of items: 1
Items: 
Size: 796095 Color: 4

Bin 2236: 204003 of cap free
Amount of items: 2
Items: 
Size: 398049 Color: 4
Size: 397949 Color: 3

Bin 2237: 204078 of cap free
Amount of items: 2
Items: 
Size: 398042 Color: 4
Size: 397881 Color: 1

Bin 2238: 204089 of cap free
Amount of items: 1
Items: 
Size: 795912 Color: 2

Bin 2239: 204120 of cap free
Amount of items: 1
Items: 
Size: 795881 Color: 3

Bin 2240: 204139 of cap free
Amount of items: 1
Items: 
Size: 795862 Color: 0

Bin 2241: 204249 of cap free
Amount of items: 1
Items: 
Size: 795752 Color: 4

Bin 2242: 204254 of cap free
Amount of items: 1
Items: 
Size: 795747 Color: 3

Bin 2243: 204366 of cap free
Amount of items: 1
Items: 
Size: 795635 Color: 3

Bin 2244: 204429 of cap free
Amount of items: 2
Items: 
Size: 397815 Color: 3
Size: 397757 Color: 4

Bin 2245: 204466 of cap free
Amount of items: 1
Items: 
Size: 795535 Color: 1

Bin 2246: 204541 of cap free
Amount of items: 1
Items: 
Size: 795460 Color: 2

Bin 2247: 204576 of cap free
Amount of items: 2
Items: 
Size: 397755 Color: 4
Size: 397670 Color: 1

Bin 2248: 204653 of cap free
Amount of items: 1
Items: 
Size: 795348 Color: 2

Bin 2249: 204692 of cap free
Amount of items: 1
Items: 
Size: 795309 Color: 4

Bin 2250: 204719 of cap free
Amount of items: 2
Items: 
Size: 397641 Color: 2
Size: 397641 Color: 0

Bin 2251: 204734 of cap free
Amount of items: 1
Items: 
Size: 795267 Color: 2

Bin 2252: 204738 of cap free
Amount of items: 1
Items: 
Size: 795263 Color: 1

Bin 2253: 205172 of cap free
Amount of items: 1
Items: 
Size: 794829 Color: 1

Bin 2254: 205374 of cap free
Amount of items: 1
Items: 
Size: 794627 Color: 0

Bin 2255: 205399 of cap free
Amount of items: 1
Items: 
Size: 794602 Color: 3

Bin 2256: 205417 of cap free
Amount of items: 1
Items: 
Size: 794584 Color: 2

Bin 2257: 205446 of cap free
Amount of items: 1
Items: 
Size: 794555 Color: 1

Bin 2258: 205503 of cap free
Amount of items: 1
Items: 
Size: 794498 Color: 3

Bin 2259: 205666 of cap free
Amount of items: 1
Items: 
Size: 794335 Color: 0

Bin 2260: 205669 of cap free
Amount of items: 1
Items: 
Size: 794332 Color: 3

Bin 2261: 205719 of cap free
Amount of items: 1
Items: 
Size: 794282 Color: 3

Bin 2262: 205771 of cap free
Amount of items: 1
Items: 
Size: 794230 Color: 2

Bin 2263: 205970 of cap free
Amount of items: 1
Items: 
Size: 794031 Color: 1

Bin 2264: 206106 of cap free
Amount of items: 1
Items: 
Size: 793895 Color: 2

Bin 2265: 206114 of cap free
Amount of items: 1
Items: 
Size: 793887 Color: 1

Bin 2266: 206246 of cap free
Amount of items: 1
Items: 
Size: 793755 Color: 4

Bin 2267: 206365 of cap free
Amount of items: 1
Items: 
Size: 793636 Color: 4

Bin 2268: 206382 of cap free
Amount of items: 1
Items: 
Size: 793619 Color: 4

Bin 2269: 206474 of cap free
Amount of items: 1
Items: 
Size: 793527 Color: 4

Bin 2270: 206513 of cap free
Amount of items: 1
Items: 
Size: 793488 Color: 3

Bin 2271: 206673 of cap free
Amount of items: 1
Items: 
Size: 793328 Color: 0

Bin 2272: 206708 of cap free
Amount of items: 1
Items: 
Size: 793293 Color: 3

Bin 2273: 206726 of cap free
Amount of items: 1
Items: 
Size: 793275 Color: 4

Bin 2274: 206794 of cap free
Amount of items: 1
Items: 
Size: 793207 Color: 0

Bin 2275: 206912 of cap free
Amount of items: 1
Items: 
Size: 793089 Color: 3

Bin 2276: 207113 of cap free
Amount of items: 1
Items: 
Size: 792888 Color: 0

Bin 2277: 207148 of cap free
Amount of items: 1
Items: 
Size: 792853 Color: 0

Bin 2278: 207148 of cap free
Amount of items: 1
Items: 
Size: 792853 Color: 2

Bin 2279: 207477 of cap free
Amount of items: 1
Items: 
Size: 792524 Color: 4

Bin 2280: 207487 of cap free
Amount of items: 1
Items: 
Size: 792514 Color: 3

Bin 2281: 207642 of cap free
Amount of items: 1
Items: 
Size: 792359 Color: 1

Bin 2282: 207685 of cap free
Amount of items: 1
Items: 
Size: 792316 Color: 3

Bin 2283: 207757 of cap free
Amount of items: 1
Items: 
Size: 792244 Color: 0

Bin 2284: 207803 of cap free
Amount of items: 1
Items: 
Size: 792198 Color: 3

Bin 2285: 207822 of cap free
Amount of items: 1
Items: 
Size: 792179 Color: 1

Bin 2286: 207845 of cap free
Amount of items: 1
Items: 
Size: 792156 Color: 4

Bin 2287: 207897 of cap free
Amount of items: 1
Items: 
Size: 792104 Color: 0

Bin 2288: 207914 of cap free
Amount of items: 1
Items: 
Size: 792087 Color: 4

Bin 2289: 207986 of cap free
Amount of items: 1
Items: 
Size: 792015 Color: 0

Bin 2290: 207991 of cap free
Amount of items: 1
Items: 
Size: 792010 Color: 4

Bin 2291: 208006 of cap free
Amount of items: 1
Items: 
Size: 791995 Color: 0

Bin 2292: 208037 of cap free
Amount of items: 1
Items: 
Size: 791964 Color: 1

Bin 2293: 208121 of cap free
Amount of items: 1
Items: 
Size: 791880 Color: 1

Bin 2294: 208136 of cap free
Amount of items: 1
Items: 
Size: 791865 Color: 1

Bin 2295: 208235 of cap free
Amount of items: 1
Items: 
Size: 791766 Color: 3

Bin 2296: 208291 of cap free
Amount of items: 1
Items: 
Size: 791710 Color: 4

Bin 2297: 208356 of cap free
Amount of items: 1
Items: 
Size: 791645 Color: 1

Bin 2298: 208359 of cap free
Amount of items: 1
Items: 
Size: 791642 Color: 4

Bin 2299: 208432 of cap free
Amount of items: 1
Items: 
Size: 791569 Color: 1

Bin 2300: 208467 of cap free
Amount of items: 1
Items: 
Size: 791534 Color: 1

Bin 2301: 208502 of cap free
Amount of items: 1
Items: 
Size: 791499 Color: 2

Bin 2302: 208619 of cap free
Amount of items: 1
Items: 
Size: 791382 Color: 2

Bin 2303: 208634 of cap free
Amount of items: 1
Items: 
Size: 791367 Color: 4

Bin 2304: 208730 of cap free
Amount of items: 1
Items: 
Size: 791271 Color: 3

Bin 2305: 208757 of cap free
Amount of items: 1
Items: 
Size: 791244 Color: 3

Bin 2306: 208798 of cap free
Amount of items: 1
Items: 
Size: 791203 Color: 0

Bin 2307: 208901 of cap free
Amount of items: 1
Items: 
Size: 791100 Color: 1

Bin 2308: 209003 of cap free
Amount of items: 1
Items: 
Size: 790998 Color: 2

Bin 2309: 209018 of cap free
Amount of items: 1
Items: 
Size: 790983 Color: 0

Bin 2310: 209036 of cap free
Amount of items: 1
Items: 
Size: 790965 Color: 4

Bin 2311: 209055 of cap free
Amount of items: 1
Items: 
Size: 790946 Color: 2

Bin 2312: 209106 of cap free
Amount of items: 1
Items: 
Size: 790895 Color: 2

Bin 2313: 209123 of cap free
Amount of items: 1
Items: 
Size: 790878 Color: 4

Bin 2314: 209162 of cap free
Amount of items: 1
Items: 
Size: 790839 Color: 3

Bin 2315: 209347 of cap free
Amount of items: 1
Items: 
Size: 790654 Color: 1

Bin 2316: 209371 of cap free
Amount of items: 1
Items: 
Size: 790630 Color: 3

Bin 2317: 209403 of cap free
Amount of items: 1
Items: 
Size: 790598 Color: 4

Bin 2318: 209458 of cap free
Amount of items: 1
Items: 
Size: 790543 Color: 1

Bin 2319: 209531 of cap free
Amount of items: 1
Items: 
Size: 790470 Color: 0

Bin 2320: 209603 of cap free
Amount of items: 1
Items: 
Size: 790398 Color: 1

Bin 2321: 209633 of cap free
Amount of items: 1
Items: 
Size: 790368 Color: 0

Bin 2322: 209642 of cap free
Amount of items: 1
Items: 
Size: 790359 Color: 4

Bin 2323: 209873 of cap free
Amount of items: 1
Items: 
Size: 790128 Color: 2

Bin 2324: 209919 of cap free
Amount of items: 1
Items: 
Size: 790082 Color: 3

Bin 2325: 209930 of cap free
Amount of items: 1
Items: 
Size: 790071 Color: 4

Bin 2326: 209939 of cap free
Amount of items: 1
Items: 
Size: 790062 Color: 4

Bin 2327: 209949 of cap free
Amount of items: 1
Items: 
Size: 790052 Color: 4

Bin 2328: 209960 of cap free
Amount of items: 1
Items: 
Size: 790041 Color: 2

Bin 2329: 210145 of cap free
Amount of items: 1
Items: 
Size: 789856 Color: 0

Bin 2330: 210176 of cap free
Amount of items: 1
Items: 
Size: 789825 Color: 0

Bin 2331: 210185 of cap free
Amount of items: 1
Items: 
Size: 789816 Color: 3

Bin 2332: 210215 of cap free
Amount of items: 1
Items: 
Size: 789786 Color: 4

Bin 2333: 210248 of cap free
Amount of items: 1
Items: 
Size: 789753 Color: 3

Bin 2334: 210275 of cap free
Amount of items: 1
Items: 
Size: 789726 Color: 2

Bin 2335: 210289 of cap free
Amount of items: 1
Items: 
Size: 789712 Color: 3

Bin 2336: 210323 of cap free
Amount of items: 1
Items: 
Size: 789678 Color: 3

Bin 2337: 210372 of cap free
Amount of items: 1
Items: 
Size: 789629 Color: 4

Bin 2338: 210427 of cap free
Amount of items: 1
Items: 
Size: 789574 Color: 4

Bin 2339: 210466 of cap free
Amount of items: 1
Items: 
Size: 789535 Color: 1

Bin 2340: 210722 of cap free
Amount of items: 1
Items: 
Size: 789279 Color: 2

Bin 2341: 210739 of cap free
Amount of items: 1
Items: 
Size: 789262 Color: 4

Bin 2342: 210763 of cap free
Amount of items: 1
Items: 
Size: 789238 Color: 0

Bin 2343: 210938 of cap free
Amount of items: 1
Items: 
Size: 789063 Color: 3

Bin 2344: 210989 of cap free
Amount of items: 1
Items: 
Size: 789012 Color: 2

Bin 2345: 211197 of cap free
Amount of items: 1
Items: 
Size: 788804 Color: 3

Bin 2346: 211242 of cap free
Amount of items: 1
Items: 
Size: 788759 Color: 3

Bin 2347: 211317 of cap free
Amount of items: 1
Items: 
Size: 788684 Color: 1

Bin 2348: 211374 of cap free
Amount of items: 1
Items: 
Size: 788627 Color: 1

Bin 2349: 211385 of cap free
Amount of items: 1
Items: 
Size: 788616 Color: 2

Bin 2350: 211457 of cap free
Amount of items: 1
Items: 
Size: 788544 Color: 4

Bin 2351: 211473 of cap free
Amount of items: 1
Items: 
Size: 788528 Color: 3

Bin 2352: 211587 of cap free
Amount of items: 1
Items: 
Size: 788414 Color: 4

Bin 2353: 211595 of cap free
Amount of items: 1
Items: 
Size: 788406 Color: 4

Bin 2354: 211606 of cap free
Amount of items: 1
Items: 
Size: 788395 Color: 0

Bin 2355: 211674 of cap free
Amount of items: 1
Items: 
Size: 788327 Color: 0

Bin 2356: 211810 of cap free
Amount of items: 1
Items: 
Size: 788191 Color: 1

Bin 2357: 211886 of cap free
Amount of items: 1
Items: 
Size: 788115 Color: 0

Bin 2358: 211937 of cap free
Amount of items: 1
Items: 
Size: 788064 Color: 4

Bin 2359: 212067 of cap free
Amount of items: 1
Items: 
Size: 787934 Color: 1

Bin 2360: 212210 of cap free
Amount of items: 1
Items: 
Size: 787791 Color: 2

Bin 2361: 212333 of cap free
Amount of items: 1
Items: 
Size: 787668 Color: 3

Bin 2362: 212351 of cap free
Amount of items: 1
Items: 
Size: 787650 Color: 1

Bin 2363: 212572 of cap free
Amount of items: 1
Items: 
Size: 787429 Color: 4

Bin 2364: 212579 of cap free
Amount of items: 1
Items: 
Size: 787422 Color: 3

Bin 2365: 212586 of cap free
Amount of items: 1
Items: 
Size: 787415 Color: 1

Bin 2366: 212634 of cap free
Amount of items: 1
Items: 
Size: 787367 Color: 4

Bin 2367: 212688 of cap free
Amount of items: 1
Items: 
Size: 787313 Color: 0

Bin 2368: 212969 of cap free
Amount of items: 1
Items: 
Size: 787032 Color: 0

Bin 2369: 213149 of cap free
Amount of items: 1
Items: 
Size: 786852 Color: 3

Bin 2370: 213223 of cap free
Amount of items: 1
Items: 
Size: 786778 Color: 2

Bin 2371: 213236 of cap free
Amount of items: 1
Items: 
Size: 786765 Color: 0

Bin 2372: 213267 of cap free
Amount of items: 1
Items: 
Size: 786734 Color: 0

Bin 2373: 213351 of cap free
Amount of items: 1
Items: 
Size: 786650 Color: 3

Bin 2374: 213399 of cap free
Amount of items: 1
Items: 
Size: 786602 Color: 4

Bin 2375: 213421 of cap free
Amount of items: 1
Items: 
Size: 786580 Color: 0

Bin 2376: 213490 of cap free
Amount of items: 1
Items: 
Size: 786511 Color: 0

Bin 2377: 213575 of cap free
Amount of items: 1
Items: 
Size: 786426 Color: 3

Bin 2378: 213648 of cap free
Amount of items: 1
Items: 
Size: 786353 Color: 1

Bin 2379: 213667 of cap free
Amount of items: 1
Items: 
Size: 786334 Color: 3

Bin 2380: 213727 of cap free
Amount of items: 1
Items: 
Size: 786274 Color: 3

Bin 2381: 213802 of cap free
Amount of items: 1
Items: 
Size: 786199 Color: 3

Bin 2382: 213884 of cap free
Amount of items: 1
Items: 
Size: 786117 Color: 4

Bin 2383: 213894 of cap free
Amount of items: 1
Items: 
Size: 786107 Color: 3

Bin 2384: 213951 of cap free
Amount of items: 1
Items: 
Size: 786050 Color: 4

Bin 2385: 214057 of cap free
Amount of items: 1
Items: 
Size: 785944 Color: 1

Bin 2386: 214064 of cap free
Amount of items: 1
Items: 
Size: 785937 Color: 4

Bin 2387: 214106 of cap free
Amount of items: 1
Items: 
Size: 785895 Color: 3

Bin 2388: 214111 of cap free
Amount of items: 1
Items: 
Size: 785890 Color: 4

Bin 2389: 214162 of cap free
Amount of items: 1
Items: 
Size: 785839 Color: 0

Bin 2390: 214176 of cap free
Amount of items: 1
Items: 
Size: 785825 Color: 4

Bin 2391: 214399 of cap free
Amount of items: 1
Items: 
Size: 785602 Color: 3

Bin 2392: 214409 of cap free
Amount of items: 1
Items: 
Size: 785592 Color: 2

Bin 2393: 214421 of cap free
Amount of items: 1
Items: 
Size: 785580 Color: 2

Bin 2394: 214611 of cap free
Amount of items: 1
Items: 
Size: 785390 Color: 3

Bin 2395: 214629 of cap free
Amount of items: 1
Items: 
Size: 785372 Color: 2

Bin 2396: 214684 of cap free
Amount of items: 1
Items: 
Size: 785317 Color: 1

Bin 2397: 214816 of cap free
Amount of items: 1
Items: 
Size: 785185 Color: 1

Bin 2398: 214919 of cap free
Amount of items: 1
Items: 
Size: 785082 Color: 2

Bin 2399: 215074 of cap free
Amount of items: 1
Items: 
Size: 784927 Color: 4

Bin 2400: 215094 of cap free
Amount of items: 1
Items: 
Size: 784907 Color: 4

Bin 2401: 215115 of cap free
Amount of items: 1
Items: 
Size: 784886 Color: 1

Bin 2402: 215147 of cap free
Amount of items: 1
Items: 
Size: 784854 Color: 0

Bin 2403: 215188 of cap free
Amount of items: 1
Items: 
Size: 784813 Color: 4

Bin 2404: 215238 of cap free
Amount of items: 1
Items: 
Size: 784763 Color: 1

Bin 2405: 215257 of cap free
Amount of items: 1
Items: 
Size: 784744 Color: 3

Bin 2406: 215333 of cap free
Amount of items: 1
Items: 
Size: 784668 Color: 4

Bin 2407: 215376 of cap free
Amount of items: 1
Items: 
Size: 784625 Color: 3

Bin 2408: 215378 of cap free
Amount of items: 1
Items: 
Size: 784623 Color: 0

Bin 2409: 215409 of cap free
Amount of items: 1
Items: 
Size: 784592 Color: 1

Bin 2410: 215556 of cap free
Amount of items: 1
Items: 
Size: 784445 Color: 2

Bin 2411: 215588 of cap free
Amount of items: 1
Items: 
Size: 784413 Color: 4

Bin 2412: 215694 of cap free
Amount of items: 1
Items: 
Size: 784307 Color: 0

Bin 2413: 215785 of cap free
Amount of items: 1
Items: 
Size: 784216 Color: 0

Bin 2414: 215933 of cap free
Amount of items: 1
Items: 
Size: 784068 Color: 4

Bin 2415: 215959 of cap free
Amount of items: 1
Items: 
Size: 784042 Color: 2

Bin 2416: 216107 of cap free
Amount of items: 1
Items: 
Size: 783894 Color: 1

Bin 2417: 216122 of cap free
Amount of items: 1
Items: 
Size: 783879 Color: 1

Bin 2418: 216262 of cap free
Amount of items: 1
Items: 
Size: 783739 Color: 1

Bin 2419: 216454 of cap free
Amount of items: 1
Items: 
Size: 783547 Color: 3

Bin 2420: 216545 of cap free
Amount of items: 1
Items: 
Size: 783456 Color: 1

Bin 2421: 216587 of cap free
Amount of items: 1
Items: 
Size: 783414 Color: 0

Bin 2422: 216623 of cap free
Amount of items: 1
Items: 
Size: 783378 Color: 2

Bin 2423: 216762 of cap free
Amount of items: 1
Items: 
Size: 783239 Color: 4

Bin 2424: 216778 of cap free
Amount of items: 1
Items: 
Size: 783223 Color: 1

Bin 2425: 216865 of cap free
Amount of items: 1
Items: 
Size: 783136 Color: 3

Bin 2426: 216925 of cap free
Amount of items: 1
Items: 
Size: 783076 Color: 4

Bin 2427: 217020 of cap free
Amount of items: 1
Items: 
Size: 782981 Color: 0

Bin 2428: 217042 of cap free
Amount of items: 1
Items: 
Size: 782959 Color: 2

Bin 2429: 217075 of cap free
Amount of items: 1
Items: 
Size: 782926 Color: 1

Bin 2430: 217092 of cap free
Amount of items: 1
Items: 
Size: 782909 Color: 2

Bin 2431: 217174 of cap free
Amount of items: 1
Items: 
Size: 782827 Color: 1

Bin 2432: 217179 of cap free
Amount of items: 1
Items: 
Size: 782822 Color: 0

Bin 2433: 217318 of cap free
Amount of items: 1
Items: 
Size: 782683 Color: 1

Bin 2434: 217353 of cap free
Amount of items: 1
Items: 
Size: 782648 Color: 4

Bin 2435: 217509 of cap free
Amount of items: 1
Items: 
Size: 782492 Color: 1

Bin 2436: 217517 of cap free
Amount of items: 1
Items: 
Size: 782484 Color: 0

Bin 2437: 217555 of cap free
Amount of items: 1
Items: 
Size: 782446 Color: 0

Bin 2438: 217609 of cap free
Amount of items: 1
Items: 
Size: 782392 Color: 3

Bin 2439: 217646 of cap free
Amount of items: 1
Items: 
Size: 782355 Color: 0

Bin 2440: 217872 of cap free
Amount of items: 1
Items: 
Size: 782129 Color: 1

Bin 2441: 218100 of cap free
Amount of items: 1
Items: 
Size: 781901 Color: 0

Bin 2442: 218417 of cap free
Amount of items: 1
Items: 
Size: 781584 Color: 4

Bin 2443: 218420 of cap free
Amount of items: 1
Items: 
Size: 781581 Color: 4

Bin 2444: 218452 of cap free
Amount of items: 1
Items: 
Size: 781549 Color: 4

Bin 2445: 218458 of cap free
Amount of items: 1
Items: 
Size: 781543 Color: 1

Bin 2446: 218462 of cap free
Amount of items: 1
Items: 
Size: 781539 Color: 3

Bin 2447: 218526 of cap free
Amount of items: 1
Items: 
Size: 781475 Color: 3

Bin 2448: 218530 of cap free
Amount of items: 1
Items: 
Size: 781471 Color: 0

Bin 2449: 218538 of cap free
Amount of items: 1
Items: 
Size: 781463 Color: 0

Bin 2450: 218543 of cap free
Amount of items: 1
Items: 
Size: 781458 Color: 2

Bin 2451: 218545 of cap free
Amount of items: 1
Items: 
Size: 781456 Color: 2

Bin 2452: 218560 of cap free
Amount of items: 1
Items: 
Size: 781441 Color: 3

Bin 2453: 218617 of cap free
Amount of items: 1
Items: 
Size: 781384 Color: 2

Bin 2454: 218637 of cap free
Amount of items: 1
Items: 
Size: 781364 Color: 2

Bin 2455: 218855 of cap free
Amount of items: 1
Items: 
Size: 781146 Color: 4

Bin 2456: 218901 of cap free
Amount of items: 1
Items: 
Size: 781100 Color: 2

Bin 2457: 218947 of cap free
Amount of items: 1
Items: 
Size: 781054 Color: 3

Bin 2458: 219059 of cap free
Amount of items: 1
Items: 
Size: 780942 Color: 4

Bin 2459: 219068 of cap free
Amount of items: 1
Items: 
Size: 780933 Color: 2

Bin 2460: 219070 of cap free
Amount of items: 1
Items: 
Size: 780931 Color: 3

Bin 2461: 219113 of cap free
Amount of items: 1
Items: 
Size: 780888 Color: 1

Bin 2462: 219160 of cap free
Amount of items: 1
Items: 
Size: 780841 Color: 2

Bin 2463: 219242 of cap free
Amount of items: 1
Items: 
Size: 780759 Color: 0

Bin 2464: 219306 of cap free
Amount of items: 1
Items: 
Size: 780695 Color: 1

Bin 2465: 219354 of cap free
Amount of items: 1
Items: 
Size: 780647 Color: 4

Bin 2466: 219426 of cap free
Amount of items: 1
Items: 
Size: 780575 Color: 3

Bin 2467: 219501 of cap free
Amount of items: 1
Items: 
Size: 780500 Color: 0

Bin 2468: 219527 of cap free
Amount of items: 1
Items: 
Size: 780474 Color: 1

Bin 2469: 219578 of cap free
Amount of items: 1
Items: 
Size: 780423 Color: 3

Bin 2470: 219630 of cap free
Amount of items: 1
Items: 
Size: 780371 Color: 2

Bin 2471: 219643 of cap free
Amount of items: 1
Items: 
Size: 780358 Color: 2

Bin 2472: 219800 of cap free
Amount of items: 1
Items: 
Size: 780201 Color: 4

Bin 2473: 219864 of cap free
Amount of items: 1
Items: 
Size: 780137 Color: 2

Bin 2474: 219907 of cap free
Amount of items: 1
Items: 
Size: 780094 Color: 4

Bin 2475: 220089 of cap free
Amount of items: 1
Items: 
Size: 779912 Color: 1

Bin 2476: 220172 of cap free
Amount of items: 1
Items: 
Size: 779829 Color: 4

Bin 2477: 220222 of cap free
Amount of items: 1
Items: 
Size: 779779 Color: 2

Bin 2478: 220244 of cap free
Amount of items: 1
Items: 
Size: 779757 Color: 3

Bin 2479: 220292 of cap free
Amount of items: 1
Items: 
Size: 779709 Color: 1

Bin 2480: 220447 of cap free
Amount of items: 1
Items: 
Size: 779554 Color: 0

Bin 2481: 220515 of cap free
Amount of items: 1
Items: 
Size: 779486 Color: 1

Bin 2482: 220579 of cap free
Amount of items: 1
Items: 
Size: 779422 Color: 4

Bin 2483: 220602 of cap free
Amount of items: 1
Items: 
Size: 779399 Color: 0

Bin 2484: 220608 of cap free
Amount of items: 1
Items: 
Size: 779393 Color: 1

Bin 2485: 220647 of cap free
Amount of items: 1
Items: 
Size: 779354 Color: 0

Bin 2486: 220666 of cap free
Amount of items: 1
Items: 
Size: 779335 Color: 4

Bin 2487: 220976 of cap free
Amount of items: 1
Items: 
Size: 779025 Color: 3

Bin 2488: 221033 of cap free
Amount of items: 1
Items: 
Size: 778968 Color: 0

Bin 2489: 221035 of cap free
Amount of items: 1
Items: 
Size: 778966 Color: 3

Bin 2490: 221039 of cap free
Amount of items: 1
Items: 
Size: 778962 Color: 4

Bin 2491: 221155 of cap free
Amount of items: 1
Items: 
Size: 778846 Color: 3

Bin 2492: 221445 of cap free
Amount of items: 1
Items: 
Size: 778556 Color: 2

Bin 2493: 221460 of cap free
Amount of items: 1
Items: 
Size: 778541 Color: 4

Bin 2494: 221514 of cap free
Amount of items: 1
Items: 
Size: 778487 Color: 1

Bin 2495: 221621 of cap free
Amount of items: 1
Items: 
Size: 778380 Color: 4

Bin 2496: 221633 of cap free
Amount of items: 1
Items: 
Size: 778368 Color: 4

Bin 2497: 221695 of cap free
Amount of items: 1
Items: 
Size: 778306 Color: 0

Bin 2498: 221784 of cap free
Amount of items: 1
Items: 
Size: 778217 Color: 1

Bin 2499: 221843 of cap free
Amount of items: 1
Items: 
Size: 778158 Color: 4

Bin 2500: 221848 of cap free
Amount of items: 1
Items: 
Size: 778153 Color: 1

Bin 2501: 221948 of cap free
Amount of items: 1
Items: 
Size: 778053 Color: 2

Bin 2502: 221962 of cap free
Amount of items: 1
Items: 
Size: 778039 Color: 1

Bin 2503: 222027 of cap free
Amount of items: 1
Items: 
Size: 777974 Color: 3

Bin 2504: 222076 of cap free
Amount of items: 1
Items: 
Size: 777925 Color: 0

Bin 2505: 222214 of cap free
Amount of items: 1
Items: 
Size: 777787 Color: 4

Bin 2506: 222278 of cap free
Amount of items: 1
Items: 
Size: 777723 Color: 0

Bin 2507: 222356 of cap free
Amount of items: 1
Items: 
Size: 777645 Color: 1

Bin 2508: 222404 of cap free
Amount of items: 1
Items: 
Size: 777597 Color: 3

Bin 2509: 222406 of cap free
Amount of items: 1
Items: 
Size: 777595 Color: 3

Bin 2510: 222461 of cap free
Amount of items: 1
Items: 
Size: 777540 Color: 0

Bin 2511: 222559 of cap free
Amount of items: 1
Items: 
Size: 777442 Color: 4

Bin 2512: 222571 of cap free
Amount of items: 1
Items: 
Size: 777430 Color: 4

Bin 2513: 222592 of cap free
Amount of items: 1
Items: 
Size: 777409 Color: 0

Bin 2514: 222619 of cap free
Amount of items: 1
Items: 
Size: 777382 Color: 3

Bin 2515: 222672 of cap free
Amount of items: 1
Items: 
Size: 777329 Color: 3

Bin 2516: 222770 of cap free
Amount of items: 1
Items: 
Size: 777231 Color: 1

Bin 2517: 222847 of cap free
Amount of items: 1
Items: 
Size: 777154 Color: 1

Bin 2518: 222874 of cap free
Amount of items: 1
Items: 
Size: 777127 Color: 2

Bin 2519: 222930 of cap free
Amount of items: 1
Items: 
Size: 777071 Color: 0

Bin 2520: 222974 of cap free
Amount of items: 1
Items: 
Size: 777027 Color: 4

Bin 2521: 223045 of cap free
Amount of items: 1
Items: 
Size: 776956 Color: 3

Bin 2522: 223061 of cap free
Amount of items: 1
Items: 
Size: 776940 Color: 0

Bin 2523: 223063 of cap free
Amount of items: 1
Items: 
Size: 776938 Color: 4

Bin 2524: 223209 of cap free
Amount of items: 1
Items: 
Size: 776792 Color: 1

Bin 2525: 223241 of cap free
Amount of items: 1
Items: 
Size: 776760 Color: 1

Bin 2526: 223341 of cap free
Amount of items: 1
Items: 
Size: 776660 Color: 3

Bin 2527: 223415 of cap free
Amount of items: 1
Items: 
Size: 776586 Color: 0

Bin 2528: 223419 of cap free
Amount of items: 1
Items: 
Size: 776582 Color: 0

Bin 2529: 223465 of cap free
Amount of items: 1
Items: 
Size: 776536 Color: 1

Bin 2530: 223492 of cap free
Amount of items: 1
Items: 
Size: 776509 Color: 4

Bin 2531: 223549 of cap free
Amount of items: 1
Items: 
Size: 776452 Color: 1

Bin 2532: 223551 of cap free
Amount of items: 1
Items: 
Size: 776450 Color: 4

Bin 2533: 223586 of cap free
Amount of items: 1
Items: 
Size: 776415 Color: 2

Bin 2534: 223670 of cap free
Amount of items: 1
Items: 
Size: 776331 Color: 3

Bin 2535: 223886 of cap free
Amount of items: 1
Items: 
Size: 776115 Color: 0

Bin 2536: 223904 of cap free
Amount of items: 1
Items: 
Size: 776097 Color: 3

Bin 2537: 223919 of cap free
Amount of items: 1
Items: 
Size: 776082 Color: 3

Bin 2538: 223985 of cap free
Amount of items: 1
Items: 
Size: 776016 Color: 4

Bin 2539: 224046 of cap free
Amount of items: 1
Items: 
Size: 775955 Color: 0

Bin 2540: 224083 of cap free
Amount of items: 1
Items: 
Size: 775918 Color: 1

Bin 2541: 224097 of cap free
Amount of items: 1
Items: 
Size: 775904 Color: 4

Bin 2542: 224125 of cap free
Amount of items: 1
Items: 
Size: 775876 Color: 0

Bin 2543: 224269 of cap free
Amount of items: 1
Items: 
Size: 775732 Color: 2

Bin 2544: 224435 of cap free
Amount of items: 1
Items: 
Size: 775566 Color: 0

Bin 2545: 224509 of cap free
Amount of items: 1
Items: 
Size: 775492 Color: 3

Bin 2546: 224515 of cap free
Amount of items: 1
Items: 
Size: 775486 Color: 2

Bin 2547: 224644 of cap free
Amount of items: 1
Items: 
Size: 775357 Color: 0

Bin 2548: 224671 of cap free
Amount of items: 1
Items: 
Size: 775330 Color: 3

Bin 2549: 224719 of cap free
Amount of items: 1
Items: 
Size: 775282 Color: 0

Bin 2550: 224735 of cap free
Amount of items: 1
Items: 
Size: 775266 Color: 4

Bin 2551: 224782 of cap free
Amount of items: 1
Items: 
Size: 775219 Color: 2

Bin 2552: 224902 of cap free
Amount of items: 1
Items: 
Size: 775099 Color: 1

Bin 2553: 225029 of cap free
Amount of items: 1
Items: 
Size: 774972 Color: 0

Bin 2554: 225100 of cap free
Amount of items: 1
Items: 
Size: 774901 Color: 0

Bin 2555: 225189 of cap free
Amount of items: 1
Items: 
Size: 774812 Color: 0

Bin 2556: 225195 of cap free
Amount of items: 1
Items: 
Size: 774806 Color: 4

Bin 2557: 225265 of cap free
Amount of items: 1
Items: 
Size: 774736 Color: 3

Bin 2558: 225320 of cap free
Amount of items: 1
Items: 
Size: 774681 Color: 0

Bin 2559: 225396 of cap free
Amount of items: 1
Items: 
Size: 774605 Color: 4

Bin 2560: 225448 of cap free
Amount of items: 1
Items: 
Size: 774553 Color: 4

Bin 2561: 225501 of cap free
Amount of items: 1
Items: 
Size: 774500 Color: 4

Bin 2562: 225557 of cap free
Amount of items: 1
Items: 
Size: 774444 Color: 2

Bin 2563: 225662 of cap free
Amount of items: 1
Items: 
Size: 774339 Color: 0

Bin 2564: 225684 of cap free
Amount of items: 1
Items: 
Size: 774317 Color: 4

Bin 2565: 225789 of cap free
Amount of items: 1
Items: 
Size: 774212 Color: 0

Bin 2566: 225980 of cap free
Amount of items: 1
Items: 
Size: 774021 Color: 1

Bin 2567: 226094 of cap free
Amount of items: 1
Items: 
Size: 773907 Color: 4

Bin 2568: 226155 of cap free
Amount of items: 1
Items: 
Size: 773846 Color: 1

Bin 2569: 226305 of cap free
Amount of items: 1
Items: 
Size: 773696 Color: 4

Bin 2570: 226338 of cap free
Amount of items: 1
Items: 
Size: 773663 Color: 4

Bin 2571: 226348 of cap free
Amount of items: 1
Items: 
Size: 773653 Color: 2

Bin 2572: 226546 of cap free
Amount of items: 1
Items: 
Size: 773455 Color: 4

Bin 2573: 226569 of cap free
Amount of items: 1
Items: 
Size: 773432 Color: 1

Bin 2574: 226729 of cap free
Amount of items: 1
Items: 
Size: 773272 Color: 2

Bin 2575: 226733 of cap free
Amount of items: 1
Items: 
Size: 773268 Color: 1

Bin 2576: 226867 of cap free
Amount of items: 1
Items: 
Size: 773134 Color: 3

Bin 2577: 226935 of cap free
Amount of items: 1
Items: 
Size: 773066 Color: 3

Bin 2578: 227035 of cap free
Amount of items: 1
Items: 
Size: 772966 Color: 2

Bin 2579: 227080 of cap free
Amount of items: 1
Items: 
Size: 772921 Color: 1

Bin 2580: 227178 of cap free
Amount of items: 1
Items: 
Size: 772823 Color: 2

Bin 2581: 227233 of cap free
Amount of items: 1
Items: 
Size: 772768 Color: 4

Bin 2582: 227306 of cap free
Amount of items: 1
Items: 
Size: 772695 Color: 4

Bin 2583: 227621 of cap free
Amount of items: 1
Items: 
Size: 772380 Color: 4

Bin 2584: 227783 of cap free
Amount of items: 1
Items: 
Size: 772218 Color: 4

Bin 2585: 227891 of cap free
Amount of items: 1
Items: 
Size: 772110 Color: 2

Bin 2586: 227935 of cap free
Amount of items: 1
Items: 
Size: 772066 Color: 1

Bin 2587: 228128 of cap free
Amount of items: 1
Items: 
Size: 771873 Color: 1

Bin 2588: 228215 of cap free
Amount of items: 1
Items: 
Size: 771786 Color: 0

Bin 2589: 228334 of cap free
Amount of items: 1
Items: 
Size: 771667 Color: 0

Bin 2590: 228532 of cap free
Amount of items: 1
Items: 
Size: 771469 Color: 2

Bin 2591: 228545 of cap free
Amount of items: 1
Items: 
Size: 771456 Color: 1

Bin 2592: 228699 of cap free
Amount of items: 1
Items: 
Size: 771302 Color: 1

Bin 2593: 228712 of cap free
Amount of items: 1
Items: 
Size: 771289 Color: 4

Bin 2594: 228920 of cap free
Amount of items: 1
Items: 
Size: 771081 Color: 4

Bin 2595: 228987 of cap free
Amount of items: 1
Items: 
Size: 771014 Color: 4

Bin 2596: 229086 of cap free
Amount of items: 1
Items: 
Size: 770915 Color: 2

Bin 2597: 229099 of cap free
Amount of items: 1
Items: 
Size: 770902 Color: 4

Bin 2598: 229103 of cap free
Amount of items: 1
Items: 
Size: 770898 Color: 0

Bin 2599: 229154 of cap free
Amount of items: 1
Items: 
Size: 770847 Color: 0

Bin 2600: 229161 of cap free
Amount of items: 1
Items: 
Size: 770840 Color: 3

Bin 2601: 229384 of cap free
Amount of items: 1
Items: 
Size: 770617 Color: 0

Bin 2602: 229410 of cap free
Amount of items: 1
Items: 
Size: 770591 Color: 3

Bin 2603: 229420 of cap free
Amount of items: 1
Items: 
Size: 770581 Color: 3

Bin 2604: 229507 of cap free
Amount of items: 1
Items: 
Size: 770494 Color: 0

Bin 2605: 229572 of cap free
Amount of items: 1
Items: 
Size: 770429 Color: 2

Bin 2606: 229671 of cap free
Amount of items: 1
Items: 
Size: 770330 Color: 2

Bin 2607: 229740 of cap free
Amount of items: 1
Items: 
Size: 770261 Color: 1

Bin 2608: 229758 of cap free
Amount of items: 1
Items: 
Size: 770243 Color: 1

Bin 2609: 229807 of cap free
Amount of items: 1
Items: 
Size: 770194 Color: 2

Bin 2610: 229860 of cap free
Amount of items: 1
Items: 
Size: 770141 Color: 4

Bin 2611: 229889 of cap free
Amount of items: 1
Items: 
Size: 770112 Color: 4

Bin 2612: 229926 of cap free
Amount of items: 1
Items: 
Size: 770075 Color: 0

Bin 2613: 230045 of cap free
Amount of items: 1
Items: 
Size: 769956 Color: 1

Bin 2614: 230045 of cap free
Amount of items: 1
Items: 
Size: 769956 Color: 3

Bin 2615: 230047 of cap free
Amount of items: 1
Items: 
Size: 769954 Color: 1

Bin 2616: 230160 of cap free
Amount of items: 1
Items: 
Size: 769841 Color: 1

Bin 2617: 230205 of cap free
Amount of items: 1
Items: 
Size: 769796 Color: 2

Bin 2618: 230205 of cap free
Amount of items: 1
Items: 
Size: 769796 Color: 4

Bin 2619: 230270 of cap free
Amount of items: 1
Items: 
Size: 769731 Color: 0

Bin 2620: 230273 of cap free
Amount of items: 1
Items: 
Size: 769728 Color: 3

Bin 2621: 230352 of cap free
Amount of items: 1
Items: 
Size: 769649 Color: 1

Bin 2622: 230366 of cap free
Amount of items: 1
Items: 
Size: 769635 Color: 4

Bin 2623: 230525 of cap free
Amount of items: 1
Items: 
Size: 769476 Color: 2

Bin 2624: 230588 of cap free
Amount of items: 1
Items: 
Size: 769413 Color: 1

Bin 2625: 230709 of cap free
Amount of items: 1
Items: 
Size: 769292 Color: 0

Bin 2626: 230737 of cap free
Amount of items: 1
Items: 
Size: 769264 Color: 4

Bin 2627: 230804 of cap free
Amount of items: 1
Items: 
Size: 769197 Color: 1

Bin 2628: 230878 of cap free
Amount of items: 1
Items: 
Size: 769123 Color: 1

Bin 2629: 230894 of cap free
Amount of items: 1
Items: 
Size: 769107 Color: 1

Bin 2630: 230901 of cap free
Amount of items: 1
Items: 
Size: 769100 Color: 4

Bin 2631: 230932 of cap free
Amount of items: 1
Items: 
Size: 769069 Color: 4

Bin 2632: 231029 of cap free
Amount of items: 1
Items: 
Size: 768972 Color: 2

Bin 2633: 231098 of cap free
Amount of items: 1
Items: 
Size: 768903 Color: 1

Bin 2634: 231163 of cap free
Amount of items: 1
Items: 
Size: 768838 Color: 4

Bin 2635: 231242 of cap free
Amount of items: 1
Items: 
Size: 768759 Color: 4

Bin 2636: 231303 of cap free
Amount of items: 1
Items: 
Size: 768698 Color: 3

Bin 2637: 231305 of cap free
Amount of items: 1
Items: 
Size: 768696 Color: 0

Bin 2638: 231352 of cap free
Amount of items: 1
Items: 
Size: 768649 Color: 0

Bin 2639: 231370 of cap free
Amount of items: 1
Items: 
Size: 768631 Color: 4

Bin 2640: 231379 of cap free
Amount of items: 1
Items: 
Size: 768622 Color: 0

Bin 2641: 231406 of cap free
Amount of items: 1
Items: 
Size: 768595 Color: 1

Bin 2642: 231438 of cap free
Amount of items: 1
Items: 
Size: 768563 Color: 2

Bin 2643: 231509 of cap free
Amount of items: 1
Items: 
Size: 768492 Color: 1

Bin 2644: 231543 of cap free
Amount of items: 1
Items: 
Size: 768458 Color: 3

Bin 2645: 231573 of cap free
Amount of items: 1
Items: 
Size: 768428 Color: 0

Bin 2646: 231619 of cap free
Amount of items: 1
Items: 
Size: 768382 Color: 3

Bin 2647: 231688 of cap free
Amount of items: 1
Items: 
Size: 768313 Color: 0

Bin 2648: 231732 of cap free
Amount of items: 1
Items: 
Size: 768269 Color: 3

Bin 2649: 231767 of cap free
Amount of items: 1
Items: 
Size: 768234 Color: 3

Bin 2650: 231866 of cap free
Amount of items: 1
Items: 
Size: 768135 Color: 2

Bin 2651: 231876 of cap free
Amount of items: 1
Items: 
Size: 768125 Color: 0

Bin 2652: 232080 of cap free
Amount of items: 1
Items: 
Size: 767921 Color: 2

Bin 2653: 232189 of cap free
Amount of items: 1
Items: 
Size: 767812 Color: 0

Bin 2654: 232240 of cap free
Amount of items: 1
Items: 
Size: 767761 Color: 0

Bin 2655: 232253 of cap free
Amount of items: 1
Items: 
Size: 767748 Color: 0

Bin 2656: 232267 of cap free
Amount of items: 1
Items: 
Size: 767734 Color: 4

Bin 2657: 232393 of cap free
Amount of items: 1
Items: 
Size: 767608 Color: 1

Bin 2658: 232574 of cap free
Amount of items: 1
Items: 
Size: 767427 Color: 0

Bin 2659: 232586 of cap free
Amount of items: 1
Items: 
Size: 767415 Color: 0

Bin 2660: 232708 of cap free
Amount of items: 1
Items: 
Size: 767293 Color: 3

Bin 2661: 232726 of cap free
Amount of items: 1
Items: 
Size: 767275 Color: 2

Bin 2662: 232753 of cap free
Amount of items: 1
Items: 
Size: 767248 Color: 0

Bin 2663: 232757 of cap free
Amount of items: 1
Items: 
Size: 767244 Color: 2

Bin 2664: 232758 of cap free
Amount of items: 1
Items: 
Size: 767243 Color: 4

Bin 2665: 232784 of cap free
Amount of items: 1
Items: 
Size: 767217 Color: 3

Bin 2666: 232817 of cap free
Amount of items: 1
Items: 
Size: 767184 Color: 0

Bin 2667: 232849 of cap free
Amount of items: 1
Items: 
Size: 767152 Color: 4

Bin 2668: 232892 of cap free
Amount of items: 1
Items: 
Size: 767109 Color: 4

Bin 2669: 232926 of cap free
Amount of items: 1
Items: 
Size: 767075 Color: 1

Bin 2670: 233066 of cap free
Amount of items: 1
Items: 
Size: 766935 Color: 3

Bin 2671: 233083 of cap free
Amount of items: 1
Items: 
Size: 766918 Color: 2

Bin 2672: 233277 of cap free
Amount of items: 1
Items: 
Size: 766724 Color: 1

Bin 2673: 233437 of cap free
Amount of items: 1
Items: 
Size: 766564 Color: 3

Bin 2674: 233466 of cap free
Amount of items: 1
Items: 
Size: 766535 Color: 2

Bin 2675: 233738 of cap free
Amount of items: 1
Items: 
Size: 766263 Color: 2

Bin 2676: 233947 of cap free
Amount of items: 1
Items: 
Size: 766054 Color: 1

Bin 2677: 233986 of cap free
Amount of items: 1
Items: 
Size: 766015 Color: 3

Bin 2678: 234012 of cap free
Amount of items: 1
Items: 
Size: 765989 Color: 3

Bin 2679: 234034 of cap free
Amount of items: 1
Items: 
Size: 765967 Color: 3

Bin 2680: 234227 of cap free
Amount of items: 1
Items: 
Size: 765774 Color: 0

Bin 2681: 234335 of cap free
Amount of items: 1
Items: 
Size: 765666 Color: 1

Bin 2682: 234336 of cap free
Amount of items: 1
Items: 
Size: 765665 Color: 2

Bin 2683: 234418 of cap free
Amount of items: 1
Items: 
Size: 765583 Color: 3

Bin 2684: 234422 of cap free
Amount of items: 1
Items: 
Size: 765579 Color: 4

Bin 2685: 234453 of cap free
Amount of items: 1
Items: 
Size: 765548 Color: 0

Bin 2686: 234470 of cap free
Amount of items: 1
Items: 
Size: 765531 Color: 0

Bin 2687: 234528 of cap free
Amount of items: 1
Items: 
Size: 765473 Color: 2

Bin 2688: 234618 of cap free
Amount of items: 1
Items: 
Size: 765383 Color: 2

Bin 2689: 234648 of cap free
Amount of items: 1
Items: 
Size: 765353 Color: 4

Bin 2690: 234692 of cap free
Amount of items: 1
Items: 
Size: 765309 Color: 0

Bin 2691: 234766 of cap free
Amount of items: 1
Items: 
Size: 765235 Color: 0

Bin 2692: 234826 of cap free
Amount of items: 1
Items: 
Size: 765175 Color: 1

Bin 2693: 234827 of cap free
Amount of items: 1
Items: 
Size: 765174 Color: 2

Bin 2694: 234957 of cap free
Amount of items: 1
Items: 
Size: 765044 Color: 1

Bin 2695: 234959 of cap free
Amount of items: 1
Items: 
Size: 765042 Color: 3

Bin 2696: 234995 of cap free
Amount of items: 1
Items: 
Size: 765006 Color: 2

Bin 2697: 235243 of cap free
Amount of items: 1
Items: 
Size: 764758 Color: 0

Bin 2698: 235272 of cap free
Amount of items: 1
Items: 
Size: 764729 Color: 3

Bin 2699: 235305 of cap free
Amount of items: 1
Items: 
Size: 764696 Color: 1

Bin 2700: 235358 of cap free
Amount of items: 1
Items: 
Size: 764643 Color: 2

Bin 2701: 235380 of cap free
Amount of items: 1
Items: 
Size: 764621 Color: 4

Bin 2702: 235425 of cap free
Amount of items: 1
Items: 
Size: 764576 Color: 3

Bin 2703: 235484 of cap free
Amount of items: 1
Items: 
Size: 764517 Color: 2

Bin 2704: 235774 of cap free
Amount of items: 1
Items: 
Size: 764227 Color: 0

Bin 2705: 235832 of cap free
Amount of items: 1
Items: 
Size: 764169 Color: 3

Bin 2706: 235897 of cap free
Amount of items: 1
Items: 
Size: 764104 Color: 1

Bin 2707: 235903 of cap free
Amount of items: 1
Items: 
Size: 764098 Color: 0

Bin 2708: 235976 of cap free
Amount of items: 1
Items: 
Size: 764025 Color: 3

Bin 2709: 236017 of cap free
Amount of items: 1
Items: 
Size: 763984 Color: 3

Bin 2710: 236031 of cap free
Amount of items: 1
Items: 
Size: 763970 Color: 1

Bin 2711: 236055 of cap free
Amount of items: 1
Items: 
Size: 763946 Color: 4

Bin 2712: 236069 of cap free
Amount of items: 1
Items: 
Size: 763932 Color: 2

Bin 2713: 236090 of cap free
Amount of items: 1
Items: 
Size: 763911 Color: 3

Bin 2714: 236091 of cap free
Amount of items: 1
Items: 
Size: 763910 Color: 0

Bin 2715: 236095 of cap free
Amount of items: 1
Items: 
Size: 763906 Color: 3

Bin 2716: 236316 of cap free
Amount of items: 1
Items: 
Size: 763685 Color: 1

Bin 2717: 236369 of cap free
Amount of items: 1
Items: 
Size: 763632 Color: 0

Bin 2718: 236463 of cap free
Amount of items: 1
Items: 
Size: 763538 Color: 4

Bin 2719: 236492 of cap free
Amount of items: 1
Items: 
Size: 763509 Color: 1

Bin 2720: 236499 of cap free
Amount of items: 1
Items: 
Size: 763502 Color: 2

Bin 2721: 236521 of cap free
Amount of items: 1
Items: 
Size: 763480 Color: 2

Bin 2722: 236748 of cap free
Amount of items: 1
Items: 
Size: 763253 Color: 4

Bin 2723: 236786 of cap free
Amount of items: 1
Items: 
Size: 763215 Color: 2

Bin 2724: 236817 of cap free
Amount of items: 1
Items: 
Size: 763184 Color: 0

Bin 2725: 236898 of cap free
Amount of items: 1
Items: 
Size: 763103 Color: 3

Bin 2726: 236948 of cap free
Amount of items: 1
Items: 
Size: 763053 Color: 3

Bin 2727: 237010 of cap free
Amount of items: 1
Items: 
Size: 762991 Color: 0

Bin 2728: 237015 of cap free
Amount of items: 1
Items: 
Size: 762986 Color: 0

Bin 2729: 237152 of cap free
Amount of items: 1
Items: 
Size: 762849 Color: 1

Bin 2730: 237174 of cap free
Amount of items: 1
Items: 
Size: 762827 Color: 4

Bin 2731: 237183 of cap free
Amount of items: 1
Items: 
Size: 762818 Color: 1

Bin 2732: 237316 of cap free
Amount of items: 1
Items: 
Size: 762685 Color: 1

Bin 2733: 237349 of cap free
Amount of items: 1
Items: 
Size: 762652 Color: 0

Bin 2734: 237363 of cap free
Amount of items: 1
Items: 
Size: 762638 Color: 0

Bin 2735: 237371 of cap free
Amount of items: 1
Items: 
Size: 762630 Color: 4

Bin 2736: 237438 of cap free
Amount of items: 1
Items: 
Size: 762563 Color: 1

Bin 2737: 237586 of cap free
Amount of items: 1
Items: 
Size: 762415 Color: 1

Bin 2738: 237603 of cap free
Amount of items: 1
Items: 
Size: 762398 Color: 0

Bin 2739: 237636 of cap free
Amount of items: 1
Items: 
Size: 762365 Color: 2

Bin 2740: 237685 of cap free
Amount of items: 1
Items: 
Size: 762316 Color: 2

Bin 2741: 237767 of cap free
Amount of items: 1
Items: 
Size: 762234 Color: 0

Bin 2742: 237767 of cap free
Amount of items: 1
Items: 
Size: 762234 Color: 0

Bin 2743: 237789 of cap free
Amount of items: 1
Items: 
Size: 762212 Color: 0

Bin 2744: 237923 of cap free
Amount of items: 1
Items: 
Size: 762078 Color: 3

Bin 2745: 237944 of cap free
Amount of items: 1
Items: 
Size: 762057 Color: 2

Bin 2746: 238095 of cap free
Amount of items: 1
Items: 
Size: 761906 Color: 0

Bin 2747: 238277 of cap free
Amount of items: 1
Items: 
Size: 761724 Color: 1

Bin 2748: 238287 of cap free
Amount of items: 1
Items: 
Size: 761714 Color: 3

Bin 2749: 238372 of cap free
Amount of items: 1
Items: 
Size: 761629 Color: 0

Bin 2750: 238424 of cap free
Amount of items: 1
Items: 
Size: 761577 Color: 0

Bin 2751: 238613 of cap free
Amount of items: 1
Items: 
Size: 761388 Color: 2

Bin 2752: 238637 of cap free
Amount of items: 1
Items: 
Size: 761364 Color: 3

Bin 2753: 238733 of cap free
Amount of items: 1
Items: 
Size: 761268 Color: 0

Bin 2754: 238844 of cap free
Amount of items: 1
Items: 
Size: 761157 Color: 4

Bin 2755: 238852 of cap free
Amount of items: 1
Items: 
Size: 761149 Color: 1

Bin 2756: 239090 of cap free
Amount of items: 1
Items: 
Size: 760911 Color: 1

Bin 2757: 239094 of cap free
Amount of items: 1
Items: 
Size: 760907 Color: 4

Bin 2758: 239099 of cap free
Amount of items: 1
Items: 
Size: 760902 Color: 4

Bin 2759: 239129 of cap free
Amount of items: 1
Items: 
Size: 760872 Color: 2

Bin 2760: 239213 of cap free
Amount of items: 1
Items: 
Size: 760788 Color: 1

Bin 2761: 239305 of cap free
Amount of items: 1
Items: 
Size: 760696 Color: 3

Bin 2762: 239403 of cap free
Amount of items: 1
Items: 
Size: 760598 Color: 1

Bin 2763: 239553 of cap free
Amount of items: 1
Items: 
Size: 760448 Color: 0

Bin 2764: 239579 of cap free
Amount of items: 1
Items: 
Size: 760422 Color: 1

Bin 2765: 239663 of cap free
Amount of items: 1
Items: 
Size: 760338 Color: 3

Bin 2766: 239682 of cap free
Amount of items: 1
Items: 
Size: 760319 Color: 3

Bin 2767: 239765 of cap free
Amount of items: 1
Items: 
Size: 760236 Color: 0

Bin 2768: 239855 of cap free
Amount of items: 1
Items: 
Size: 760146 Color: 0

Bin 2769: 239890 of cap free
Amount of items: 1
Items: 
Size: 760111 Color: 4

Bin 2770: 239958 of cap free
Amount of items: 1
Items: 
Size: 760043 Color: 4

Bin 2771: 240070 of cap free
Amount of items: 1
Items: 
Size: 759931 Color: 3

Bin 2772: 240260 of cap free
Amount of items: 1
Items: 
Size: 759741 Color: 1

Bin 2773: 240316 of cap free
Amount of items: 1
Items: 
Size: 759685 Color: 4

Bin 2774: 240323 of cap free
Amount of items: 1
Items: 
Size: 759678 Color: 4

Bin 2775: 240328 of cap free
Amount of items: 1
Items: 
Size: 759673 Color: 0

Bin 2776: 240351 of cap free
Amount of items: 1
Items: 
Size: 759650 Color: 0

Bin 2777: 240440 of cap free
Amount of items: 1
Items: 
Size: 759561 Color: 1

Bin 2778: 240577 of cap free
Amount of items: 1
Items: 
Size: 759424 Color: 4

Bin 2779: 240738 of cap free
Amount of items: 1
Items: 
Size: 759263 Color: 4

Bin 2780: 240763 of cap free
Amount of items: 1
Items: 
Size: 759238 Color: 3

Bin 2781: 240901 of cap free
Amount of items: 1
Items: 
Size: 759100 Color: 0

Bin 2782: 240918 of cap free
Amount of items: 1
Items: 
Size: 759083 Color: 4

Bin 2783: 241087 of cap free
Amount of items: 1
Items: 
Size: 758914 Color: 0

Bin 2784: 241183 of cap free
Amount of items: 1
Items: 
Size: 758818 Color: 1

Bin 2785: 241218 of cap free
Amount of items: 1
Items: 
Size: 758783 Color: 2

Bin 2786: 241320 of cap free
Amount of items: 1
Items: 
Size: 758681 Color: 2

Bin 2787: 241453 of cap free
Amount of items: 1
Items: 
Size: 758548 Color: 3

Bin 2788: 241495 of cap free
Amount of items: 1
Items: 
Size: 758506 Color: 0

Bin 2789: 241551 of cap free
Amount of items: 1
Items: 
Size: 758450 Color: 4

Bin 2790: 241557 of cap free
Amount of items: 1
Items: 
Size: 758444 Color: 1

Bin 2791: 241902 of cap free
Amount of items: 1
Items: 
Size: 758099 Color: 0

Bin 2792: 241942 of cap free
Amount of items: 1
Items: 
Size: 758059 Color: 4

Bin 2793: 241946 of cap free
Amount of items: 1
Items: 
Size: 758055 Color: 1

Bin 2794: 242008 of cap free
Amount of items: 1
Items: 
Size: 757993 Color: 3

Bin 2795: 242032 of cap free
Amount of items: 1
Items: 
Size: 757969 Color: 0

Bin 2796: 242084 of cap free
Amount of items: 1
Items: 
Size: 757917 Color: 1

Bin 2797: 242099 of cap free
Amount of items: 1
Items: 
Size: 757902 Color: 2

Bin 2798: 242180 of cap free
Amount of items: 1
Items: 
Size: 757821 Color: 0

Bin 2799: 242181 of cap free
Amount of items: 1
Items: 
Size: 757820 Color: 3

Bin 2800: 242184 of cap free
Amount of items: 1
Items: 
Size: 757817 Color: 1

Bin 2801: 242320 of cap free
Amount of items: 1
Items: 
Size: 757681 Color: 0

Bin 2802: 242321 of cap free
Amount of items: 1
Items: 
Size: 757680 Color: 1

Bin 2803: 242324 of cap free
Amount of items: 1
Items: 
Size: 757677 Color: 2

Bin 2804: 242386 of cap free
Amount of items: 1
Items: 
Size: 757615 Color: 1

Bin 2805: 242543 of cap free
Amount of items: 1
Items: 
Size: 757458 Color: 0

Bin 2806: 242642 of cap free
Amount of items: 1
Items: 
Size: 757359 Color: 1

Bin 2807: 242733 of cap free
Amount of items: 1
Items: 
Size: 757268 Color: 2

Bin 2808: 242769 of cap free
Amount of items: 1
Items: 
Size: 757232 Color: 4

Bin 2809: 242788 of cap free
Amount of items: 1
Items: 
Size: 757213 Color: 0

Bin 2810: 242859 of cap free
Amount of items: 1
Items: 
Size: 757142 Color: 2

Bin 2811: 242904 of cap free
Amount of items: 1
Items: 
Size: 757097 Color: 1

Bin 2812: 242980 of cap free
Amount of items: 1
Items: 
Size: 757021 Color: 3

Bin 2813: 242994 of cap free
Amount of items: 1
Items: 
Size: 757007 Color: 3

Bin 2814: 243070 of cap free
Amount of items: 1
Items: 
Size: 756931 Color: 3

Bin 2815: 243114 of cap free
Amount of items: 1
Items: 
Size: 756887 Color: 1

Bin 2816: 243151 of cap free
Amount of items: 1
Items: 
Size: 756850 Color: 0

Bin 2817: 243160 of cap free
Amount of items: 1
Items: 
Size: 756841 Color: 2

Bin 2818: 243250 of cap free
Amount of items: 1
Items: 
Size: 756751 Color: 3

Bin 2819: 243270 of cap free
Amount of items: 1
Items: 
Size: 756731 Color: 1

Bin 2820: 243285 of cap free
Amount of items: 1
Items: 
Size: 756716 Color: 2

Bin 2821: 243324 of cap free
Amount of items: 1
Items: 
Size: 756677 Color: 4

Bin 2822: 243407 of cap free
Amount of items: 1
Items: 
Size: 756594 Color: 2

Bin 2823: 243485 of cap free
Amount of items: 1
Items: 
Size: 756516 Color: 3

Bin 2824: 243521 of cap free
Amount of items: 1
Items: 
Size: 756480 Color: 1

Bin 2825: 243558 of cap free
Amount of items: 1
Items: 
Size: 756443 Color: 2

Bin 2826: 243569 of cap free
Amount of items: 1
Items: 
Size: 756432 Color: 0

Bin 2827: 243581 of cap free
Amount of items: 1
Items: 
Size: 756420 Color: 2

Bin 2828: 243629 of cap free
Amount of items: 1
Items: 
Size: 756372 Color: 0

Bin 2829: 243653 of cap free
Amount of items: 1
Items: 
Size: 756348 Color: 2

Bin 2830: 243734 of cap free
Amount of items: 1
Items: 
Size: 756267 Color: 4

Bin 2831: 243816 of cap free
Amount of items: 1
Items: 
Size: 756185 Color: 2

Bin 2832: 243833 of cap free
Amount of items: 1
Items: 
Size: 756168 Color: 2

Bin 2833: 243852 of cap free
Amount of items: 1
Items: 
Size: 756149 Color: 3

Bin 2834: 244022 of cap free
Amount of items: 1
Items: 
Size: 755979 Color: 2

Bin 2835: 244040 of cap free
Amount of items: 1
Items: 
Size: 755961 Color: 0

Bin 2836: 244076 of cap free
Amount of items: 1
Items: 
Size: 755925 Color: 4

Bin 2837: 244126 of cap free
Amount of items: 1
Items: 
Size: 755875 Color: 2

Bin 2838: 244173 of cap free
Amount of items: 1
Items: 
Size: 755828 Color: 0

Bin 2839: 244204 of cap free
Amount of items: 1
Items: 
Size: 755797 Color: 4

Bin 2840: 244266 of cap free
Amount of items: 1
Items: 
Size: 755735 Color: 0

Bin 2841: 244398 of cap free
Amount of items: 1
Items: 
Size: 755603 Color: 2

Bin 2842: 244437 of cap free
Amount of items: 1
Items: 
Size: 755564 Color: 3

Bin 2843: 244465 of cap free
Amount of items: 1
Items: 
Size: 755536 Color: 1

Bin 2844: 244522 of cap free
Amount of items: 1
Items: 
Size: 755479 Color: 1

Bin 2845: 244555 of cap free
Amount of items: 1
Items: 
Size: 755446 Color: 2

Bin 2846: 244580 of cap free
Amount of items: 1
Items: 
Size: 755421 Color: 2

Bin 2847: 244613 of cap free
Amount of items: 1
Items: 
Size: 755388 Color: 1

Bin 2848: 244772 of cap free
Amount of items: 1
Items: 
Size: 755229 Color: 1

Bin 2849: 244775 of cap free
Amount of items: 1
Items: 
Size: 755226 Color: 1

Bin 2850: 245056 of cap free
Amount of items: 1
Items: 
Size: 754945 Color: 1

Bin 2851: 245298 of cap free
Amount of items: 1
Items: 
Size: 754703 Color: 2

Bin 2852: 245465 of cap free
Amount of items: 1
Items: 
Size: 754536 Color: 3

Bin 2853: 245494 of cap free
Amount of items: 1
Items: 
Size: 754507 Color: 1

Bin 2854: 245500 of cap free
Amount of items: 1
Items: 
Size: 754501 Color: 1

Bin 2855: 245607 of cap free
Amount of items: 1
Items: 
Size: 754394 Color: 2

Bin 2856: 245676 of cap free
Amount of items: 1
Items: 
Size: 754325 Color: 3

Bin 2857: 245720 of cap free
Amount of items: 1
Items: 
Size: 754281 Color: 0

Bin 2858: 245723 of cap free
Amount of items: 1
Items: 
Size: 754278 Color: 1

Bin 2859: 245781 of cap free
Amount of items: 1
Items: 
Size: 754220 Color: 1

Bin 2860: 245823 of cap free
Amount of items: 1
Items: 
Size: 754178 Color: 3

Bin 2861: 245835 of cap free
Amount of items: 1
Items: 
Size: 754166 Color: 1

Bin 2862: 245852 of cap free
Amount of items: 1
Items: 
Size: 754149 Color: 4

Bin 2863: 245948 of cap free
Amount of items: 1
Items: 
Size: 754053 Color: 3

Bin 2864: 246216 of cap free
Amount of items: 1
Items: 
Size: 753785 Color: 1

Bin 2865: 246311 of cap free
Amount of items: 1
Items: 
Size: 753690 Color: 3

Bin 2866: 246504 of cap free
Amount of items: 1
Items: 
Size: 753497 Color: 0

Bin 2867: 246619 of cap free
Amount of items: 1
Items: 
Size: 753382 Color: 2

Bin 2868: 246633 of cap free
Amount of items: 1
Items: 
Size: 753368 Color: 2

Bin 2869: 246654 of cap free
Amount of items: 1
Items: 
Size: 753347 Color: 4

Bin 2870: 246713 of cap free
Amount of items: 1
Items: 
Size: 753288 Color: 0

Bin 2871: 246827 of cap free
Amount of items: 1
Items: 
Size: 753174 Color: 0

Bin 2872: 246900 of cap free
Amount of items: 1
Items: 
Size: 753101 Color: 1

Bin 2873: 246967 of cap free
Amount of items: 1
Items: 
Size: 753034 Color: 0

Bin 2874: 247113 of cap free
Amount of items: 1
Items: 
Size: 752888 Color: 4

Bin 2875: 247147 of cap free
Amount of items: 1
Items: 
Size: 752854 Color: 3

Bin 2876: 247273 of cap free
Amount of items: 1
Items: 
Size: 752728 Color: 0

Bin 2877: 247287 of cap free
Amount of items: 1
Items: 
Size: 752714 Color: 1

Bin 2878: 247434 of cap free
Amount of items: 1
Items: 
Size: 752567 Color: 3

Bin 2879: 247443 of cap free
Amount of items: 1
Items: 
Size: 752558 Color: 1

Bin 2880: 247537 of cap free
Amount of items: 1
Items: 
Size: 752464 Color: 2

Bin 2881: 247541 of cap free
Amount of items: 1
Items: 
Size: 752460 Color: 2

Bin 2882: 247545 of cap free
Amount of items: 1
Items: 
Size: 752456 Color: 0

Bin 2883: 247566 of cap free
Amount of items: 1
Items: 
Size: 752435 Color: 2

Bin 2884: 247607 of cap free
Amount of items: 1
Items: 
Size: 752394 Color: 0

Bin 2885: 247640 of cap free
Amount of items: 1
Items: 
Size: 752361 Color: 2

Bin 2886: 247677 of cap free
Amount of items: 1
Items: 
Size: 752324 Color: 0

Bin 2887: 247687 of cap free
Amount of items: 1
Items: 
Size: 752314 Color: 3

Bin 2888: 247858 of cap free
Amount of items: 1
Items: 
Size: 752143 Color: 1

Bin 2889: 247896 of cap free
Amount of items: 1
Items: 
Size: 752105 Color: 2

Bin 2890: 247920 of cap free
Amount of items: 1
Items: 
Size: 752081 Color: 3

Bin 2891: 247939 of cap free
Amount of items: 1
Items: 
Size: 752062 Color: 1

Bin 2892: 248166 of cap free
Amount of items: 1
Items: 
Size: 751835 Color: 1

Bin 2893: 248170 of cap free
Amount of items: 1
Items: 
Size: 751831 Color: 0

Bin 2894: 248350 of cap free
Amount of items: 1
Items: 
Size: 751651 Color: 4

Bin 2895: 248396 of cap free
Amount of items: 1
Items: 
Size: 751605 Color: 2

Bin 2896: 248429 of cap free
Amount of items: 1
Items: 
Size: 751572 Color: 1

Bin 2897: 248488 of cap free
Amount of items: 1
Items: 
Size: 751513 Color: 3

Bin 2898: 248648 of cap free
Amount of items: 1
Items: 
Size: 751353 Color: 0

Bin 2899: 248860 of cap free
Amount of items: 1
Items: 
Size: 751141 Color: 2

Bin 2900: 248911 of cap free
Amount of items: 1
Items: 
Size: 751090 Color: 2

Bin 2901: 248980 of cap free
Amount of items: 1
Items: 
Size: 751021 Color: 1

Bin 2902: 248997 of cap free
Amount of items: 1
Items: 
Size: 751004 Color: 3

Bin 2903: 249012 of cap free
Amount of items: 1
Items: 
Size: 750989 Color: 2

Bin 2904: 249151 of cap free
Amount of items: 1
Items: 
Size: 750850 Color: 4

Bin 2905: 249164 of cap free
Amount of items: 1
Items: 
Size: 750837 Color: 4

Bin 2906: 249185 of cap free
Amount of items: 1
Items: 
Size: 750816 Color: 0

Bin 2907: 249185 of cap free
Amount of items: 1
Items: 
Size: 750816 Color: 3

Bin 2908: 249196 of cap free
Amount of items: 1
Items: 
Size: 750805 Color: 4

Bin 2909: 249226 of cap free
Amount of items: 1
Items: 
Size: 750775 Color: 3

Bin 2910: 249369 of cap free
Amount of items: 1
Items: 
Size: 750632 Color: 1

Bin 2911: 249370 of cap free
Amount of items: 1
Items: 
Size: 750631 Color: 4

Bin 2912: 249457 of cap free
Amount of items: 1
Items: 
Size: 750544 Color: 3

Bin 2913: 249601 of cap free
Amount of items: 1
Items: 
Size: 750400 Color: 4

Bin 2914: 249637 of cap free
Amount of items: 1
Items: 
Size: 750364 Color: 0

Bin 2915: 249695 of cap free
Amount of items: 1
Items: 
Size: 750306 Color: 0

Bin 2916: 249780 of cap free
Amount of items: 1
Items: 
Size: 750221 Color: 1

Bin 2917: 249920 of cap free
Amount of items: 1
Items: 
Size: 750081 Color: 4

Bin 2918: 249986 of cap free
Amount of items: 1
Items: 
Size: 750015 Color: 1

Bin 2919: 250030 of cap free
Amount of items: 1
Items: 
Size: 749971 Color: 4

Bin 2920: 250098 of cap free
Amount of items: 1
Items: 
Size: 749903 Color: 3

Bin 2921: 250181 of cap free
Amount of items: 1
Items: 
Size: 749820 Color: 2

Bin 2922: 250252 of cap free
Amount of items: 1
Items: 
Size: 749749 Color: 2

Bin 2923: 250427 of cap free
Amount of items: 1
Items: 
Size: 749574 Color: 4

Bin 2924: 250474 of cap free
Amount of items: 1
Items: 
Size: 749527 Color: 2

Bin 2925: 250520 of cap free
Amount of items: 1
Items: 
Size: 749481 Color: 0

Bin 2926: 250628 of cap free
Amount of items: 1
Items: 
Size: 749373 Color: 3

Bin 2927: 250848 of cap free
Amount of items: 1
Items: 
Size: 749153 Color: 1

Bin 2928: 250874 of cap free
Amount of items: 1
Items: 
Size: 749127 Color: 3

Bin 2929: 251004 of cap free
Amount of items: 1
Items: 
Size: 748997 Color: 4

Bin 2930: 251048 of cap free
Amount of items: 1
Items: 
Size: 748953 Color: 4

Bin 2931: 251149 of cap free
Amount of items: 1
Items: 
Size: 748852 Color: 2

Bin 2932: 251210 of cap free
Amount of items: 1
Items: 
Size: 748791 Color: 2

Bin 2933: 251261 of cap free
Amount of items: 1
Items: 
Size: 748740 Color: 3

Bin 2934: 251317 of cap free
Amount of items: 1
Items: 
Size: 748684 Color: 1

Bin 2935: 251427 of cap free
Amount of items: 1
Items: 
Size: 748574 Color: 4

Bin 2936: 251702 of cap free
Amount of items: 1
Items: 
Size: 748299 Color: 3

Bin 2937: 251727 of cap free
Amount of items: 1
Items: 
Size: 748274 Color: 2

Bin 2938: 251795 of cap free
Amount of items: 1
Items: 
Size: 748206 Color: 1

Bin 2939: 251806 of cap free
Amount of items: 1
Items: 
Size: 748195 Color: 2

Bin 2940: 251826 of cap free
Amount of items: 1
Items: 
Size: 748175 Color: 2

Bin 2941: 251909 of cap free
Amount of items: 1
Items: 
Size: 748092 Color: 2

Bin 2942: 252018 of cap free
Amount of items: 1
Items: 
Size: 747983 Color: 4

Bin 2943: 252035 of cap free
Amount of items: 1
Items: 
Size: 747966 Color: 0

Bin 2944: 252222 of cap free
Amount of items: 1
Items: 
Size: 747779 Color: 3

Bin 2945: 252278 of cap free
Amount of items: 1
Items: 
Size: 747723 Color: 3

Bin 2946: 252287 of cap free
Amount of items: 1
Items: 
Size: 747714 Color: 4

Bin 2947: 252319 of cap free
Amount of items: 1
Items: 
Size: 747682 Color: 2

Bin 2948: 252336 of cap free
Amount of items: 1
Items: 
Size: 747665 Color: 1

Bin 2949: 252373 of cap free
Amount of items: 1
Items: 
Size: 747628 Color: 3

Bin 2950: 252386 of cap free
Amount of items: 1
Items: 
Size: 747615 Color: 0

Bin 2951: 252546 of cap free
Amount of items: 1
Items: 
Size: 747455 Color: 2

Bin 2952: 252561 of cap free
Amount of items: 1
Items: 
Size: 747440 Color: 4

Bin 2953: 252638 of cap free
Amount of items: 1
Items: 
Size: 747363 Color: 0

Bin 2954: 252878 of cap free
Amount of items: 1
Items: 
Size: 747123 Color: 0

Bin 2955: 252896 of cap free
Amount of items: 1
Items: 
Size: 747105 Color: 3

Bin 2956: 252915 of cap free
Amount of items: 1
Items: 
Size: 747086 Color: 4

Bin 2957: 253018 of cap free
Amount of items: 1
Items: 
Size: 746983 Color: 1

Bin 2958: 253033 of cap free
Amount of items: 1
Items: 
Size: 746968 Color: 3

Bin 2959: 253054 of cap free
Amount of items: 1
Items: 
Size: 746947 Color: 2

Bin 2960: 253100 of cap free
Amount of items: 1
Items: 
Size: 746901 Color: 3

Bin 2961: 253143 of cap free
Amount of items: 1
Items: 
Size: 746858 Color: 1

Bin 2962: 253177 of cap free
Amount of items: 1
Items: 
Size: 746824 Color: 3

Bin 2963: 253383 of cap free
Amount of items: 1
Items: 
Size: 746618 Color: 3

Bin 2964: 253428 of cap free
Amount of items: 1
Items: 
Size: 746573 Color: 4

Bin 2965: 253664 of cap free
Amount of items: 1
Items: 
Size: 746337 Color: 4

Bin 2966: 253696 of cap free
Amount of items: 1
Items: 
Size: 746305 Color: 3

Bin 2967: 253963 of cap free
Amount of items: 1
Items: 
Size: 746038 Color: 0

Bin 2968: 254052 of cap free
Amount of items: 1
Items: 
Size: 745949 Color: 4

Bin 2969: 254135 of cap free
Amount of items: 1
Items: 
Size: 745866 Color: 0

Bin 2970: 254143 of cap free
Amount of items: 1
Items: 
Size: 745858 Color: 3

Bin 2971: 254479 of cap free
Amount of items: 1
Items: 
Size: 745522 Color: 3

Bin 2972: 254511 of cap free
Amount of items: 1
Items: 
Size: 745490 Color: 1

Bin 2973: 254513 of cap free
Amount of items: 1
Items: 
Size: 745488 Color: 0

Bin 2974: 254528 of cap free
Amount of items: 1
Items: 
Size: 745473 Color: 0

Bin 2975: 254921 of cap free
Amount of items: 1
Items: 
Size: 745080 Color: 2

Bin 2976: 254925 of cap free
Amount of items: 1
Items: 
Size: 745076 Color: 3

Bin 2977: 254998 of cap free
Amount of items: 1
Items: 
Size: 745003 Color: 2

Bin 2978: 255060 of cap free
Amount of items: 1
Items: 
Size: 744941 Color: 2

Bin 2979: 255063 of cap free
Amount of items: 1
Items: 
Size: 744938 Color: 4

Bin 2980: 255151 of cap free
Amount of items: 1
Items: 
Size: 744850 Color: 4

Bin 2981: 255204 of cap free
Amount of items: 1
Items: 
Size: 744797 Color: 0

Bin 2982: 255293 of cap free
Amount of items: 1
Items: 
Size: 744708 Color: 0

Bin 2983: 255315 of cap free
Amount of items: 1
Items: 
Size: 744686 Color: 2

Bin 2984: 255413 of cap free
Amount of items: 1
Items: 
Size: 744588 Color: 0

Bin 2985: 255472 of cap free
Amount of items: 1
Items: 
Size: 744529 Color: 0

Bin 2986: 255549 of cap free
Amount of items: 1
Items: 
Size: 744452 Color: 2

Bin 2987: 255650 of cap free
Amount of items: 1
Items: 
Size: 744351 Color: 2

Bin 2988: 255673 of cap free
Amount of items: 1
Items: 
Size: 744328 Color: 2

Bin 2989: 255724 of cap free
Amount of items: 1
Items: 
Size: 744277 Color: 4

Bin 2990: 255780 of cap free
Amount of items: 1
Items: 
Size: 744221 Color: 0

Bin 2991: 255889 of cap free
Amount of items: 1
Items: 
Size: 744112 Color: 3

Bin 2992: 256126 of cap free
Amount of items: 1
Items: 
Size: 743875 Color: 4

Bin 2993: 256214 of cap free
Amount of items: 1
Items: 
Size: 743787 Color: 0

Bin 2994: 256244 of cap free
Amount of items: 1
Items: 
Size: 743757 Color: 2

Bin 2995: 256321 of cap free
Amount of items: 1
Items: 
Size: 743680 Color: 0

Bin 2996: 256397 of cap free
Amount of items: 1
Items: 
Size: 743604 Color: 4

Bin 2997: 256531 of cap free
Amount of items: 1
Items: 
Size: 743470 Color: 4

Bin 2998: 256543 of cap free
Amount of items: 1
Items: 
Size: 743458 Color: 1

Bin 2999: 256616 of cap free
Amount of items: 1
Items: 
Size: 743385 Color: 2

Bin 3000: 256734 of cap free
Amount of items: 1
Items: 
Size: 743267 Color: 1

Bin 3001: 256746 of cap free
Amount of items: 1
Items: 
Size: 743255 Color: 0

Bin 3002: 256758 of cap free
Amount of items: 1
Items: 
Size: 743243 Color: 2

Bin 3003: 256777 of cap free
Amount of items: 1
Items: 
Size: 743224 Color: 4

Bin 3004: 256782 of cap free
Amount of items: 1
Items: 
Size: 743219 Color: 2

Bin 3005: 256809 of cap free
Amount of items: 1
Items: 
Size: 743192 Color: 2

Bin 3006: 256940 of cap free
Amount of items: 1
Items: 
Size: 743061 Color: 2

Bin 3007: 257047 of cap free
Amount of items: 1
Items: 
Size: 742954 Color: 4

Bin 3008: 257213 of cap free
Amount of items: 1
Items: 
Size: 742788 Color: 1

Bin 3009: 257230 of cap free
Amount of items: 1
Items: 
Size: 742771 Color: 2

Bin 3010: 257276 of cap free
Amount of items: 1
Items: 
Size: 742725 Color: 1

Bin 3011: 257389 of cap free
Amount of items: 1
Items: 
Size: 742612 Color: 0

Bin 3012: 257437 of cap free
Amount of items: 1
Items: 
Size: 742564 Color: 3

Bin 3013: 257545 of cap free
Amount of items: 1
Items: 
Size: 742456 Color: 0

Bin 3014: 257748 of cap free
Amount of items: 1
Items: 
Size: 742253 Color: 3

Bin 3015: 257782 of cap free
Amount of items: 1
Items: 
Size: 742219 Color: 2

Bin 3016: 258237 of cap free
Amount of items: 1
Items: 
Size: 741764 Color: 3

Bin 3017: 258252 of cap free
Amount of items: 1
Items: 
Size: 741749 Color: 2

Bin 3018: 258262 of cap free
Amount of items: 1
Items: 
Size: 741739 Color: 3

Bin 3019: 258295 of cap free
Amount of items: 1
Items: 
Size: 741706 Color: 0

Bin 3020: 258297 of cap free
Amount of items: 1
Items: 
Size: 741704 Color: 2

Bin 3021: 258340 of cap free
Amount of items: 1
Items: 
Size: 741661 Color: 0

Bin 3022: 258386 of cap free
Amount of items: 1
Items: 
Size: 741615 Color: 1

Bin 3023: 258414 of cap free
Amount of items: 1
Items: 
Size: 741587 Color: 0

Bin 3024: 258662 of cap free
Amount of items: 1
Items: 
Size: 741339 Color: 4

Bin 3025: 258729 of cap free
Amount of items: 1
Items: 
Size: 741272 Color: 0

Bin 3026: 258765 of cap free
Amount of items: 1
Items: 
Size: 741236 Color: 3

Bin 3027: 258790 of cap free
Amount of items: 1
Items: 
Size: 741211 Color: 4

Bin 3028: 258906 of cap free
Amount of items: 1
Items: 
Size: 741095 Color: 0

Bin 3029: 259071 of cap free
Amount of items: 1
Items: 
Size: 740930 Color: 3

Bin 3030: 259117 of cap free
Amount of items: 1
Items: 
Size: 740884 Color: 0

Bin 3031: 259132 of cap free
Amount of items: 1
Items: 
Size: 740869 Color: 4

Bin 3032: 259152 of cap free
Amount of items: 1
Items: 
Size: 740849 Color: 4

Bin 3033: 259342 of cap free
Amount of items: 1
Items: 
Size: 740659 Color: 2

Bin 3034: 259447 of cap free
Amount of items: 1
Items: 
Size: 740554 Color: 3

Bin 3035: 259499 of cap free
Amount of items: 1
Items: 
Size: 740502 Color: 4

Bin 3036: 259610 of cap free
Amount of items: 1
Items: 
Size: 740391 Color: 2

Bin 3037: 259690 of cap free
Amount of items: 1
Items: 
Size: 740311 Color: 0

Bin 3038: 259699 of cap free
Amount of items: 1
Items: 
Size: 740302 Color: 2

Bin 3039: 259718 of cap free
Amount of items: 1
Items: 
Size: 740283 Color: 3

Bin 3040: 259881 of cap free
Amount of items: 1
Items: 
Size: 740120 Color: 0

Bin 3041: 259951 of cap free
Amount of items: 1
Items: 
Size: 740050 Color: 4

Bin 3042: 260180 of cap free
Amount of items: 1
Items: 
Size: 739821 Color: 2

Bin 3043: 260187 of cap free
Amount of items: 1
Items: 
Size: 739814 Color: 1

Bin 3044: 260204 of cap free
Amount of items: 1
Items: 
Size: 739797 Color: 1

Bin 3045: 260217 of cap free
Amount of items: 1
Items: 
Size: 739784 Color: 2

Bin 3046: 260245 of cap free
Amount of items: 1
Items: 
Size: 739756 Color: 0

Bin 3047: 260291 of cap free
Amount of items: 1
Items: 
Size: 739710 Color: 4

Bin 3048: 260339 of cap free
Amount of items: 1
Items: 
Size: 739662 Color: 3

Bin 3049: 260463 of cap free
Amount of items: 1
Items: 
Size: 739538 Color: 0

Bin 3050: 260496 of cap free
Amount of items: 1
Items: 
Size: 739505 Color: 3

Bin 3051: 260596 of cap free
Amount of items: 1
Items: 
Size: 739405 Color: 3

Bin 3052: 260618 of cap free
Amount of items: 1
Items: 
Size: 739383 Color: 3

Bin 3053: 260669 of cap free
Amount of items: 1
Items: 
Size: 739332 Color: 3

Bin 3054: 260729 of cap free
Amount of items: 1
Items: 
Size: 739272 Color: 4

Bin 3055: 260744 of cap free
Amount of items: 1
Items: 
Size: 739257 Color: 4

Bin 3056: 260879 of cap free
Amount of items: 1
Items: 
Size: 739122 Color: 4

Bin 3057: 260898 of cap free
Amount of items: 1
Items: 
Size: 739103 Color: 4

Bin 3058: 261112 of cap free
Amount of items: 1
Items: 
Size: 738889 Color: 1

Bin 3059: 261216 of cap free
Amount of items: 1
Items: 
Size: 738785 Color: 3

Bin 3060: 261348 of cap free
Amount of items: 1
Items: 
Size: 738653 Color: 0

Bin 3061: 261498 of cap free
Amount of items: 1
Items: 
Size: 738503 Color: 3

Bin 3062: 261659 of cap free
Amount of items: 1
Items: 
Size: 738342 Color: 2

Bin 3063: 261691 of cap free
Amount of items: 1
Items: 
Size: 738310 Color: 2

Bin 3064: 261764 of cap free
Amount of items: 1
Items: 
Size: 738237 Color: 4

Bin 3065: 261840 of cap free
Amount of items: 1
Items: 
Size: 738161 Color: 3

Bin 3066: 261912 of cap free
Amount of items: 1
Items: 
Size: 738089 Color: 2

Bin 3067: 261922 of cap free
Amount of items: 1
Items: 
Size: 738079 Color: 4

Bin 3068: 261931 of cap free
Amount of items: 1
Items: 
Size: 738070 Color: 2

Bin 3069: 261941 of cap free
Amount of items: 1
Items: 
Size: 738060 Color: 2

Bin 3070: 262022 of cap free
Amount of items: 1
Items: 
Size: 737979 Color: 4

Bin 3071: 262027 of cap free
Amount of items: 1
Items: 
Size: 737974 Color: 2

Bin 3072: 262087 of cap free
Amount of items: 1
Items: 
Size: 737914 Color: 2

Bin 3073: 262120 of cap free
Amount of items: 1
Items: 
Size: 737881 Color: 1

Bin 3074: 262180 of cap free
Amount of items: 1
Items: 
Size: 737821 Color: 3

Bin 3075: 262204 of cap free
Amount of items: 1
Items: 
Size: 737797 Color: 1

Bin 3076: 262257 of cap free
Amount of items: 1
Items: 
Size: 737744 Color: 2

Bin 3077: 262370 of cap free
Amount of items: 1
Items: 
Size: 737631 Color: 1

Bin 3078: 262409 of cap free
Amount of items: 1
Items: 
Size: 737592 Color: 3

Bin 3079: 262416 of cap free
Amount of items: 1
Items: 
Size: 737585 Color: 4

Bin 3080: 262462 of cap free
Amount of items: 1
Items: 
Size: 737539 Color: 0

Bin 3081: 262519 of cap free
Amount of items: 1
Items: 
Size: 737482 Color: 4

Bin 3082: 262649 of cap free
Amount of items: 1
Items: 
Size: 737352 Color: 2

Bin 3083: 262664 of cap free
Amount of items: 1
Items: 
Size: 737337 Color: 3

Bin 3084: 262814 of cap free
Amount of items: 1
Items: 
Size: 737187 Color: 1

Bin 3085: 262856 of cap free
Amount of items: 1
Items: 
Size: 737145 Color: 3

Bin 3086: 262917 of cap free
Amount of items: 1
Items: 
Size: 737084 Color: 4

Bin 3087: 263059 of cap free
Amount of items: 1
Items: 
Size: 736942 Color: 2

Bin 3088: 263159 of cap free
Amount of items: 1
Items: 
Size: 736842 Color: 3

Bin 3089: 263180 of cap free
Amount of items: 1
Items: 
Size: 736821 Color: 2

Bin 3090: 263183 of cap free
Amount of items: 1
Items: 
Size: 736818 Color: 0

Bin 3091: 263184 of cap free
Amount of items: 1
Items: 
Size: 736817 Color: 1

Bin 3092: 263263 of cap free
Amount of items: 1
Items: 
Size: 736738 Color: 3

Bin 3093: 263635 of cap free
Amount of items: 1
Items: 
Size: 736366 Color: 3

Bin 3094: 263645 of cap free
Amount of items: 1
Items: 
Size: 736356 Color: 3

Bin 3095: 263647 of cap free
Amount of items: 1
Items: 
Size: 736354 Color: 1

Bin 3096: 263719 of cap free
Amount of items: 1
Items: 
Size: 736282 Color: 1

Bin 3097: 263928 of cap free
Amount of items: 1
Items: 
Size: 736073 Color: 1

Bin 3098: 263933 of cap free
Amount of items: 1
Items: 
Size: 736068 Color: 2

Bin 3099: 263996 of cap free
Amount of items: 1
Items: 
Size: 736005 Color: 4

Bin 3100: 264004 of cap free
Amount of items: 1
Items: 
Size: 735997 Color: 3

Bin 3101: 264011 of cap free
Amount of items: 1
Items: 
Size: 735990 Color: 2

Bin 3102: 264121 of cap free
Amount of items: 1
Items: 
Size: 735880 Color: 2

Bin 3103: 264161 of cap free
Amount of items: 1
Items: 
Size: 735840 Color: 0

Bin 3104: 264248 of cap free
Amount of items: 1
Items: 
Size: 735753 Color: 3

Bin 3105: 264298 of cap free
Amount of items: 1
Items: 
Size: 735703 Color: 3

Bin 3106: 264377 of cap free
Amount of items: 1
Items: 
Size: 735624 Color: 3

Bin 3107: 264414 of cap free
Amount of items: 1
Items: 
Size: 735587 Color: 0

Bin 3108: 264593 of cap free
Amount of items: 1
Items: 
Size: 735408 Color: 4

Bin 3109: 264668 of cap free
Amount of items: 1
Items: 
Size: 735333 Color: 0

Bin 3110: 264743 of cap free
Amount of items: 1
Items: 
Size: 735258 Color: 1

Bin 3111: 264881 of cap free
Amount of items: 1
Items: 
Size: 735120 Color: 2

Bin 3112: 264886 of cap free
Amount of items: 1
Items: 
Size: 735115 Color: 0

Bin 3113: 264925 of cap free
Amount of items: 1
Items: 
Size: 735076 Color: 0

Bin 3114: 264982 of cap free
Amount of items: 1
Items: 
Size: 735019 Color: 1

Bin 3115: 265193 of cap free
Amount of items: 1
Items: 
Size: 734808 Color: 3

Bin 3116: 265211 of cap free
Amount of items: 1
Items: 
Size: 734790 Color: 3

Bin 3117: 265234 of cap free
Amount of items: 1
Items: 
Size: 734767 Color: 2

Bin 3118: 265284 of cap free
Amount of items: 1
Items: 
Size: 734717 Color: 4

Bin 3119: 265378 of cap free
Amount of items: 1
Items: 
Size: 734623 Color: 4

Bin 3120: 265404 of cap free
Amount of items: 1
Items: 
Size: 734597 Color: 4

Bin 3121: 265512 of cap free
Amount of items: 1
Items: 
Size: 734489 Color: 1

Bin 3122: 265612 of cap free
Amount of items: 1
Items: 
Size: 734389 Color: 2

Bin 3123: 265633 of cap free
Amount of items: 1
Items: 
Size: 734368 Color: 1

Bin 3124: 265887 of cap free
Amount of items: 1
Items: 
Size: 734114 Color: 0

Bin 3125: 265894 of cap free
Amount of items: 1
Items: 
Size: 734107 Color: 0

Bin 3126: 265923 of cap free
Amount of items: 1
Items: 
Size: 734078 Color: 2

Bin 3127: 265964 of cap free
Amount of items: 1
Items: 
Size: 734037 Color: 0

Bin 3128: 265981 of cap free
Amount of items: 1
Items: 
Size: 734020 Color: 2

Bin 3129: 266167 of cap free
Amount of items: 1
Items: 
Size: 733834 Color: 1

Bin 3130: 266220 of cap free
Amount of items: 1
Items: 
Size: 733781 Color: 3

Bin 3131: 266376 of cap free
Amount of items: 1
Items: 
Size: 733625 Color: 2

Bin 3132: 266417 of cap free
Amount of items: 1
Items: 
Size: 733584 Color: 1

Bin 3133: 266423 of cap free
Amount of items: 1
Items: 
Size: 733578 Color: 0

Bin 3134: 266474 of cap free
Amount of items: 1
Items: 
Size: 733527 Color: 4

Bin 3135: 266492 of cap free
Amount of items: 1
Items: 
Size: 733509 Color: 4

Bin 3136: 266730 of cap free
Amount of items: 1
Items: 
Size: 733271 Color: 1

Bin 3137: 266969 of cap free
Amount of items: 1
Items: 
Size: 733032 Color: 2

Bin 3138: 267055 of cap free
Amount of items: 1
Items: 
Size: 732946 Color: 0

Bin 3139: 267155 of cap free
Amount of items: 1
Items: 
Size: 732846 Color: 0

Bin 3140: 267294 of cap free
Amount of items: 1
Items: 
Size: 732707 Color: 1

Bin 3141: 267370 of cap free
Amount of items: 1
Items: 
Size: 732631 Color: 0

Bin 3142: 267521 of cap free
Amount of items: 1
Items: 
Size: 732480 Color: 0

Bin 3143: 267598 of cap free
Amount of items: 1
Items: 
Size: 732403 Color: 4

Bin 3144: 267702 of cap free
Amount of items: 1
Items: 
Size: 732299 Color: 1

Bin 3145: 267726 of cap free
Amount of items: 1
Items: 
Size: 732275 Color: 3

Bin 3146: 267815 of cap free
Amount of items: 1
Items: 
Size: 732186 Color: 1

Bin 3147: 267845 of cap free
Amount of items: 1
Items: 
Size: 732156 Color: 2

Bin 3148: 267862 of cap free
Amount of items: 1
Items: 
Size: 732139 Color: 1

Bin 3149: 267908 of cap free
Amount of items: 1
Items: 
Size: 732093 Color: 0

Bin 3150: 267925 of cap free
Amount of items: 1
Items: 
Size: 732076 Color: 2

Bin 3151: 268257 of cap free
Amount of items: 1
Items: 
Size: 731744 Color: 0

Bin 3152: 268270 of cap free
Amount of items: 1
Items: 
Size: 731731 Color: 4

Bin 3153: 268448 of cap free
Amount of items: 1
Items: 
Size: 731553 Color: 0

Bin 3154: 268453 of cap free
Amount of items: 1
Items: 
Size: 731548 Color: 0

Bin 3155: 268465 of cap free
Amount of items: 1
Items: 
Size: 731536 Color: 0

Bin 3156: 268508 of cap free
Amount of items: 1
Items: 
Size: 731493 Color: 3

Bin 3157: 268524 of cap free
Amount of items: 1
Items: 
Size: 731477 Color: 2

Bin 3158: 268671 of cap free
Amount of items: 1
Items: 
Size: 731330 Color: 2

Bin 3159: 268852 of cap free
Amount of items: 1
Items: 
Size: 731149 Color: 4

Bin 3160: 268956 of cap free
Amount of items: 1
Items: 
Size: 731045 Color: 3

Bin 3161: 269040 of cap free
Amount of items: 1
Items: 
Size: 730961 Color: 1

Bin 3162: 269050 of cap free
Amount of items: 1
Items: 
Size: 730951 Color: 4

Bin 3163: 269069 of cap free
Amount of items: 1
Items: 
Size: 730932 Color: 1

Bin 3164: 269174 of cap free
Amount of items: 1
Items: 
Size: 730827 Color: 2

Bin 3165: 269208 of cap free
Amount of items: 1
Items: 
Size: 730793 Color: 0

Bin 3166: 269208 of cap free
Amount of items: 1
Items: 
Size: 730793 Color: 2

Bin 3167: 269457 of cap free
Amount of items: 1
Items: 
Size: 730544 Color: 0

Bin 3168: 269457 of cap free
Amount of items: 1
Items: 
Size: 730544 Color: 4

Bin 3169: 269604 of cap free
Amount of items: 1
Items: 
Size: 730397 Color: 3

Bin 3170: 269606 of cap free
Amount of items: 1
Items: 
Size: 730395 Color: 4

Bin 3171: 269675 of cap free
Amount of items: 1
Items: 
Size: 730326 Color: 4

Bin 3172: 269731 of cap free
Amount of items: 1
Items: 
Size: 730270 Color: 2

Bin 3173: 269814 of cap free
Amount of items: 1
Items: 
Size: 730187 Color: 1

Bin 3174: 270242 of cap free
Amount of items: 1
Items: 
Size: 729759 Color: 4

Bin 3175: 270277 of cap free
Amount of items: 1
Items: 
Size: 729724 Color: 2

Bin 3176: 270279 of cap free
Amount of items: 1
Items: 
Size: 729722 Color: 0

Bin 3177: 270313 of cap free
Amount of items: 1
Items: 
Size: 729688 Color: 4

Bin 3178: 270387 of cap free
Amount of items: 1
Items: 
Size: 729614 Color: 0

Bin 3179: 270399 of cap free
Amount of items: 1
Items: 
Size: 729602 Color: 2

Bin 3180: 270403 of cap free
Amount of items: 1
Items: 
Size: 729598 Color: 2

Bin 3181: 270485 of cap free
Amount of items: 1
Items: 
Size: 729516 Color: 2

Bin 3182: 270634 of cap free
Amount of items: 1
Items: 
Size: 729367 Color: 0

Bin 3183: 270644 of cap free
Amount of items: 1
Items: 
Size: 729357 Color: 2

Bin 3184: 270734 of cap free
Amount of items: 1
Items: 
Size: 729267 Color: 2

Bin 3185: 270822 of cap free
Amount of items: 1
Items: 
Size: 729179 Color: 1

Bin 3186: 270864 of cap free
Amount of items: 1
Items: 
Size: 729137 Color: 0

Bin 3187: 270903 of cap free
Amount of items: 1
Items: 
Size: 729098 Color: 0

Bin 3188: 270994 of cap free
Amount of items: 1
Items: 
Size: 729007 Color: 2

Bin 3189: 271019 of cap free
Amount of items: 1
Items: 
Size: 728982 Color: 3

Bin 3190: 271175 of cap free
Amount of items: 1
Items: 
Size: 728826 Color: 3

Bin 3191: 271195 of cap free
Amount of items: 1
Items: 
Size: 728806 Color: 2

Bin 3192: 271235 of cap free
Amount of items: 1
Items: 
Size: 728766 Color: 0

Bin 3193: 271278 of cap free
Amount of items: 1
Items: 
Size: 728723 Color: 2

Bin 3194: 271296 of cap free
Amount of items: 1
Items: 
Size: 728705 Color: 4

Bin 3195: 271429 of cap free
Amount of items: 1
Items: 
Size: 728572 Color: 1

Bin 3196: 271429 of cap free
Amount of items: 1
Items: 
Size: 728572 Color: 4

Bin 3197: 271492 of cap free
Amount of items: 1
Items: 
Size: 728509 Color: 1

Bin 3198: 271505 of cap free
Amount of items: 1
Items: 
Size: 728496 Color: 4

Bin 3199: 271775 of cap free
Amount of items: 1
Items: 
Size: 728226 Color: 0

Bin 3200: 271934 of cap free
Amount of items: 1
Items: 
Size: 728067 Color: 2

Bin 3201: 271939 of cap free
Amount of items: 1
Items: 
Size: 728062 Color: 4

Bin 3202: 272057 of cap free
Amount of items: 1
Items: 
Size: 727944 Color: 0

Bin 3203: 272060 of cap free
Amount of items: 1
Items: 
Size: 727941 Color: 4

Bin 3204: 272491 of cap free
Amount of items: 1
Items: 
Size: 727510 Color: 0

Bin 3205: 272533 of cap free
Amount of items: 1
Items: 
Size: 727468 Color: 0

Bin 3206: 272571 of cap free
Amount of items: 1
Items: 
Size: 727430 Color: 2

Bin 3207: 272604 of cap free
Amount of items: 1
Items: 
Size: 727397 Color: 0

Bin 3208: 272674 of cap free
Amount of items: 1
Items: 
Size: 727327 Color: 0

Bin 3209: 272735 of cap free
Amount of items: 1
Items: 
Size: 727266 Color: 3

Bin 3210: 272739 of cap free
Amount of items: 1
Items: 
Size: 727262 Color: 2

Bin 3211: 272985 of cap free
Amount of items: 1
Items: 
Size: 727016 Color: 2

Bin 3212: 273063 of cap free
Amount of items: 1
Items: 
Size: 726938 Color: 0

Bin 3213: 273096 of cap free
Amount of items: 1
Items: 
Size: 726905 Color: 3

Bin 3214: 273219 of cap free
Amount of items: 1
Items: 
Size: 726782 Color: 4

Bin 3215: 273512 of cap free
Amount of items: 1
Items: 
Size: 726489 Color: 2

Bin 3216: 273513 of cap free
Amount of items: 1
Items: 
Size: 726488 Color: 2

Bin 3217: 273541 of cap free
Amount of items: 1
Items: 
Size: 726460 Color: 0

Bin 3218: 273558 of cap free
Amount of items: 1
Items: 
Size: 726443 Color: 4

Bin 3219: 273576 of cap free
Amount of items: 1
Items: 
Size: 726425 Color: 0

Bin 3220: 273610 of cap free
Amount of items: 1
Items: 
Size: 726391 Color: 0

Bin 3221: 273618 of cap free
Amount of items: 1
Items: 
Size: 726383 Color: 2

Bin 3222: 273714 of cap free
Amount of items: 1
Items: 
Size: 726287 Color: 1

Bin 3223: 273743 of cap free
Amount of items: 1
Items: 
Size: 726258 Color: 1

Bin 3224: 273800 of cap free
Amount of items: 1
Items: 
Size: 726201 Color: 3

Bin 3225: 273847 of cap free
Amount of items: 1
Items: 
Size: 726154 Color: 3

Bin 3226: 274017 of cap free
Amount of items: 1
Items: 
Size: 725984 Color: 2

Bin 3227: 274071 of cap free
Amount of items: 1
Items: 
Size: 725930 Color: 0

Bin 3228: 274089 of cap free
Amount of items: 1
Items: 
Size: 725912 Color: 0

Bin 3229: 274147 of cap free
Amount of items: 1
Items: 
Size: 725854 Color: 3

Bin 3230: 274342 of cap free
Amount of items: 1
Items: 
Size: 725659 Color: 2

Bin 3231: 274563 of cap free
Amount of items: 1
Items: 
Size: 725438 Color: 1

Bin 3232: 274661 of cap free
Amount of items: 1
Items: 
Size: 725340 Color: 2

Bin 3233: 274672 of cap free
Amount of items: 1
Items: 
Size: 725329 Color: 1

Bin 3234: 274743 of cap free
Amount of items: 1
Items: 
Size: 725258 Color: 4

Bin 3235: 274817 of cap free
Amount of items: 1
Items: 
Size: 725184 Color: 4

Bin 3236: 274864 of cap free
Amount of items: 1
Items: 
Size: 725137 Color: 1

Bin 3237: 274924 of cap free
Amount of items: 1
Items: 
Size: 725077 Color: 3

Bin 3238: 274948 of cap free
Amount of items: 1
Items: 
Size: 725053 Color: 4

Bin 3239: 274969 of cap free
Amount of items: 1
Items: 
Size: 725032 Color: 0

Bin 3240: 275130 of cap free
Amount of items: 1
Items: 
Size: 724871 Color: 4

Bin 3241: 275189 of cap free
Amount of items: 1
Items: 
Size: 724812 Color: 4

Bin 3242: 275226 of cap free
Amount of items: 1
Items: 
Size: 724775 Color: 0

Bin 3243: 275280 of cap free
Amount of items: 1
Items: 
Size: 724721 Color: 2

Bin 3244: 275347 of cap free
Amount of items: 1
Items: 
Size: 724654 Color: 0

Bin 3245: 275409 of cap free
Amount of items: 1
Items: 
Size: 724592 Color: 3

Bin 3246: 275502 of cap free
Amount of items: 1
Items: 
Size: 724499 Color: 1

Bin 3247: 275533 of cap free
Amount of items: 1
Items: 
Size: 724468 Color: 4

Bin 3248: 275591 of cap free
Amount of items: 1
Items: 
Size: 724410 Color: 1

Bin 3249: 275637 of cap free
Amount of items: 1
Items: 
Size: 724364 Color: 1

Bin 3250: 275701 of cap free
Amount of items: 1
Items: 
Size: 724300 Color: 3

Bin 3251: 275702 of cap free
Amount of items: 1
Items: 
Size: 724299 Color: 2

Bin 3252: 275717 of cap free
Amount of items: 1
Items: 
Size: 724284 Color: 0

Bin 3253: 275725 of cap free
Amount of items: 1
Items: 
Size: 724276 Color: 0

Bin 3254: 275744 of cap free
Amount of items: 1
Items: 
Size: 724257 Color: 1

Bin 3255: 275817 of cap free
Amount of items: 1
Items: 
Size: 724184 Color: 4

Bin 3256: 275990 of cap free
Amount of items: 1
Items: 
Size: 724011 Color: 0

Bin 3257: 276002 of cap free
Amount of items: 1
Items: 
Size: 723999 Color: 0

Bin 3258: 276475 of cap free
Amount of items: 1
Items: 
Size: 723526 Color: 2

Bin 3259: 276527 of cap free
Amount of items: 1
Items: 
Size: 723474 Color: 0

Bin 3260: 276619 of cap free
Amount of items: 1
Items: 
Size: 723382 Color: 1

Bin 3261: 276639 of cap free
Amount of items: 1
Items: 
Size: 723362 Color: 1

Bin 3262: 276752 of cap free
Amount of items: 1
Items: 
Size: 723249 Color: 4

Bin 3263: 276778 of cap free
Amount of items: 1
Items: 
Size: 723223 Color: 0

Bin 3264: 276788 of cap free
Amount of items: 1
Items: 
Size: 723213 Color: 4

Bin 3265: 276808 of cap free
Amount of items: 1
Items: 
Size: 723193 Color: 0

Bin 3266: 276863 of cap free
Amount of items: 1
Items: 
Size: 723138 Color: 3

Bin 3267: 276898 of cap free
Amount of items: 1
Items: 
Size: 723103 Color: 0

Bin 3268: 276899 of cap free
Amount of items: 1
Items: 
Size: 723102 Color: 3

Bin 3269: 277170 of cap free
Amount of items: 1
Items: 
Size: 722831 Color: 1

Bin 3270: 277320 of cap free
Amount of items: 1
Items: 
Size: 722681 Color: 0

Bin 3271: 277460 of cap free
Amount of items: 1
Items: 
Size: 722541 Color: 0

Bin 3272: 277502 of cap free
Amount of items: 1
Items: 
Size: 722499 Color: 3

Bin 3273: 277511 of cap free
Amount of items: 1
Items: 
Size: 722490 Color: 4

Bin 3274: 277633 of cap free
Amount of items: 1
Items: 
Size: 722368 Color: 2

Bin 3275: 277731 of cap free
Amount of items: 1
Items: 
Size: 722270 Color: 4

Bin 3276: 277851 of cap free
Amount of items: 1
Items: 
Size: 722150 Color: 4

Bin 3277: 277900 of cap free
Amount of items: 1
Items: 
Size: 722101 Color: 2

Bin 3278: 278025 of cap free
Amount of items: 1
Items: 
Size: 721976 Color: 3

Bin 3279: 278075 of cap free
Amount of items: 1
Items: 
Size: 721926 Color: 3

Bin 3280: 278208 of cap free
Amount of items: 1
Items: 
Size: 721793 Color: 2

Bin 3281: 278328 of cap free
Amount of items: 1
Items: 
Size: 721673 Color: 3

Bin 3282: 278642 of cap free
Amount of items: 1
Items: 
Size: 721359 Color: 1

Bin 3283: 278786 of cap free
Amount of items: 1
Items: 
Size: 721215 Color: 2

Bin 3284: 278841 of cap free
Amount of items: 1
Items: 
Size: 721160 Color: 4

Bin 3285: 278849 of cap free
Amount of items: 1
Items: 
Size: 721152 Color: 3

Bin 3286: 278981 of cap free
Amount of items: 1
Items: 
Size: 721020 Color: 4

Bin 3287: 279055 of cap free
Amount of items: 1
Items: 
Size: 720946 Color: 3

Bin 3288: 279064 of cap free
Amount of items: 1
Items: 
Size: 720937 Color: 0

Bin 3289: 279068 of cap free
Amount of items: 1
Items: 
Size: 720933 Color: 4

Bin 3290: 279177 of cap free
Amount of items: 1
Items: 
Size: 720824 Color: 2

Bin 3291: 279187 of cap free
Amount of items: 1
Items: 
Size: 720814 Color: 0

Bin 3292: 279299 of cap free
Amount of items: 1
Items: 
Size: 720702 Color: 1

Bin 3293: 279459 of cap free
Amount of items: 1
Items: 
Size: 720542 Color: 1

Bin 3294: 279555 of cap free
Amount of items: 1
Items: 
Size: 720446 Color: 1

Bin 3295: 279606 of cap free
Amount of items: 1
Items: 
Size: 720395 Color: 4

Bin 3296: 279618 of cap free
Amount of items: 1
Items: 
Size: 720383 Color: 3

Bin 3297: 279751 of cap free
Amount of items: 1
Items: 
Size: 720250 Color: 4

Bin 3298: 279793 of cap free
Amount of items: 1
Items: 
Size: 720208 Color: 4

Bin 3299: 279899 of cap free
Amount of items: 1
Items: 
Size: 720102 Color: 1

Bin 3300: 279918 of cap free
Amount of items: 1
Items: 
Size: 720083 Color: 2

Bin 3301: 279927 of cap free
Amount of items: 1
Items: 
Size: 720074 Color: 2

Bin 3302: 279984 of cap free
Amount of items: 1
Items: 
Size: 720017 Color: 1

Bin 3303: 280221 of cap free
Amount of items: 1
Items: 
Size: 719780 Color: 1

Bin 3304: 280258 of cap free
Amount of items: 1
Items: 
Size: 719743 Color: 4

Bin 3305: 280261 of cap free
Amount of items: 1
Items: 
Size: 719740 Color: 0

Bin 3306: 280273 of cap free
Amount of items: 1
Items: 
Size: 719728 Color: 2

Bin 3307: 280288 of cap free
Amount of items: 1
Items: 
Size: 719713 Color: 1

Bin 3308: 280483 of cap free
Amount of items: 1
Items: 
Size: 719518 Color: 3

Bin 3309: 280532 of cap free
Amount of items: 1
Items: 
Size: 719469 Color: 0

Bin 3310: 280605 of cap free
Amount of items: 1
Items: 
Size: 719396 Color: 2

Bin 3311: 280640 of cap free
Amount of items: 1
Items: 
Size: 719361 Color: 3

Bin 3312: 280968 of cap free
Amount of items: 1
Items: 
Size: 719033 Color: 3

Bin 3313: 281017 of cap free
Amount of items: 1
Items: 
Size: 718984 Color: 2

Bin 3314: 281147 of cap free
Amount of items: 1
Items: 
Size: 718854 Color: 2

Bin 3315: 281257 of cap free
Amount of items: 1
Items: 
Size: 718744 Color: 2

Bin 3316: 281268 of cap free
Amount of items: 1
Items: 
Size: 718733 Color: 4

Bin 3317: 281286 of cap free
Amount of items: 1
Items: 
Size: 718715 Color: 4

Bin 3318: 281323 of cap free
Amount of items: 1
Items: 
Size: 718678 Color: 4

Bin 3319: 281369 of cap free
Amount of items: 1
Items: 
Size: 718632 Color: 0

Bin 3320: 281414 of cap free
Amount of items: 1
Items: 
Size: 718587 Color: 3

Bin 3321: 281416 of cap free
Amount of items: 1
Items: 
Size: 718585 Color: 2

Bin 3322: 281480 of cap free
Amount of items: 1
Items: 
Size: 718521 Color: 3

Bin 3323: 281529 of cap free
Amount of items: 1
Items: 
Size: 718472 Color: 2

Bin 3324: 281594 of cap free
Amount of items: 1
Items: 
Size: 718407 Color: 2

Bin 3325: 281794 of cap free
Amount of items: 1
Items: 
Size: 718207 Color: 3

Bin 3326: 281873 of cap free
Amount of items: 1
Items: 
Size: 718128 Color: 4

Bin 3327: 281905 of cap free
Amount of items: 1
Items: 
Size: 718096 Color: 4

Bin 3328: 281982 of cap free
Amount of items: 1
Items: 
Size: 718019 Color: 3

Bin 3329: 282082 of cap free
Amount of items: 1
Items: 
Size: 717919 Color: 1

Bin 3330: 282097 of cap free
Amount of items: 1
Items: 
Size: 717904 Color: 3

Bin 3331: 282230 of cap free
Amount of items: 1
Items: 
Size: 717771 Color: 1

Bin 3332: 282340 of cap free
Amount of items: 1
Items: 
Size: 717661 Color: 4

Bin 3333: 282384 of cap free
Amount of items: 1
Items: 
Size: 717617 Color: 0

Bin 3334: 282398 of cap free
Amount of items: 1
Items: 
Size: 717603 Color: 3

Bin 3335: 282463 of cap free
Amount of items: 1
Items: 
Size: 717538 Color: 4

Bin 3336: 282472 of cap free
Amount of items: 1
Items: 
Size: 717529 Color: 1

Bin 3337: 282484 of cap free
Amount of items: 1
Items: 
Size: 717517 Color: 1

Bin 3338: 282504 of cap free
Amount of items: 1
Items: 
Size: 717497 Color: 0

Bin 3339: 282521 of cap free
Amount of items: 1
Items: 
Size: 717480 Color: 0

Bin 3340: 282731 of cap free
Amount of items: 1
Items: 
Size: 717270 Color: 4

Bin 3341: 282734 of cap free
Amount of items: 1
Items: 
Size: 717267 Color: 3

Bin 3342: 282741 of cap free
Amount of items: 1
Items: 
Size: 717260 Color: 2

Bin 3343: 282777 of cap free
Amount of items: 1
Items: 
Size: 717224 Color: 1

Bin 3344: 282783 of cap free
Amount of items: 1
Items: 
Size: 717218 Color: 2

Bin 3345: 282826 of cap free
Amount of items: 1
Items: 
Size: 717175 Color: 4

Bin 3346: 282875 of cap free
Amount of items: 1
Items: 
Size: 717126 Color: 2

Bin 3347: 282912 of cap free
Amount of items: 1
Items: 
Size: 717089 Color: 3

Bin 3348: 283085 of cap free
Amount of items: 1
Items: 
Size: 716916 Color: 0

Bin 3349: 283153 of cap free
Amount of items: 1
Items: 
Size: 716848 Color: 1

Bin 3350: 283163 of cap free
Amount of items: 1
Items: 
Size: 716838 Color: 1

Bin 3351: 283186 of cap free
Amount of items: 1
Items: 
Size: 716815 Color: 0

Bin 3352: 283192 of cap free
Amount of items: 1
Items: 
Size: 716809 Color: 2

Bin 3353: 283433 of cap free
Amount of items: 1
Items: 
Size: 716568 Color: 1

Bin 3354: 283434 of cap free
Amount of items: 1
Items: 
Size: 716567 Color: 3

Bin 3355: 283597 of cap free
Amount of items: 1
Items: 
Size: 716404 Color: 2

Bin 3356: 283720 of cap free
Amount of items: 1
Items: 
Size: 716281 Color: 0

Bin 3357: 283845 of cap free
Amount of items: 1
Items: 
Size: 716156 Color: 4

Bin 3358: 283935 of cap free
Amount of items: 1
Items: 
Size: 716066 Color: 3

Bin 3359: 283952 of cap free
Amount of items: 1
Items: 
Size: 716049 Color: 2

Bin 3360: 284030 of cap free
Amount of items: 1
Items: 
Size: 715971 Color: 3

Bin 3361: 284196 of cap free
Amount of items: 1
Items: 
Size: 715805 Color: 2

Bin 3362: 284325 of cap free
Amount of items: 1
Items: 
Size: 715676 Color: 4

Bin 3363: 284372 of cap free
Amount of items: 1
Items: 
Size: 715629 Color: 4

Bin 3364: 284387 of cap free
Amount of items: 1
Items: 
Size: 715614 Color: 4

Bin 3365: 284693 of cap free
Amount of items: 1
Items: 
Size: 715308 Color: 3

Bin 3366: 284840 of cap free
Amount of items: 1
Items: 
Size: 715161 Color: 2

Bin 3367: 284885 of cap free
Amount of items: 1
Items: 
Size: 715116 Color: 4

Bin 3368: 284917 of cap free
Amount of items: 1
Items: 
Size: 715084 Color: 0

Bin 3369: 284928 of cap free
Amount of items: 1
Items: 
Size: 715073 Color: 2

Bin 3370: 285135 of cap free
Amount of items: 1
Items: 
Size: 714866 Color: 1

Bin 3371: 285186 of cap free
Amount of items: 1
Items: 
Size: 714815 Color: 0

Bin 3372: 285437 of cap free
Amount of items: 1
Items: 
Size: 714564 Color: 2

Bin 3373: 285525 of cap free
Amount of items: 1
Items: 
Size: 714476 Color: 1

Bin 3374: 285531 of cap free
Amount of items: 1
Items: 
Size: 714470 Color: 1

Bin 3375: 285559 of cap free
Amount of items: 1
Items: 
Size: 714442 Color: 0

Bin 3376: 285591 of cap free
Amount of items: 1
Items: 
Size: 714410 Color: 1

Bin 3377: 285624 of cap free
Amount of items: 1
Items: 
Size: 714377 Color: 1

Bin 3378: 285740 of cap free
Amount of items: 1
Items: 
Size: 714261 Color: 3

Bin 3379: 285786 of cap free
Amount of items: 1
Items: 
Size: 714215 Color: 2

Bin 3380: 286026 of cap free
Amount of items: 1
Items: 
Size: 713975 Color: 1

Bin 3381: 286033 of cap free
Amount of items: 1
Items: 
Size: 713968 Color: 1

Bin 3382: 286046 of cap free
Amount of items: 1
Items: 
Size: 713955 Color: 4

Bin 3383: 286060 of cap free
Amount of items: 1
Items: 
Size: 713941 Color: 4

Bin 3384: 286106 of cap free
Amount of items: 1
Items: 
Size: 713895 Color: 4

Bin 3385: 286194 of cap free
Amount of items: 1
Items: 
Size: 713807 Color: 0

Bin 3386: 286203 of cap free
Amount of items: 1
Items: 
Size: 713798 Color: 1

Bin 3387: 286308 of cap free
Amount of items: 1
Items: 
Size: 713693 Color: 1

Bin 3388: 286339 of cap free
Amount of items: 1
Items: 
Size: 713662 Color: 3

Bin 3389: 286387 of cap free
Amount of items: 1
Items: 
Size: 713614 Color: 4

Bin 3390: 286393 of cap free
Amount of items: 1
Items: 
Size: 713608 Color: 3

Bin 3391: 286468 of cap free
Amount of items: 1
Items: 
Size: 713533 Color: 0

Bin 3392: 286584 of cap free
Amount of items: 1
Items: 
Size: 713417 Color: 3

Bin 3393: 286619 of cap free
Amount of items: 1
Items: 
Size: 713382 Color: 0

Bin 3394: 286626 of cap free
Amount of items: 1
Items: 
Size: 713375 Color: 2

Bin 3395: 286784 of cap free
Amount of items: 1
Items: 
Size: 713217 Color: 3

Bin 3396: 286807 of cap free
Amount of items: 1
Items: 
Size: 713194 Color: 1

Bin 3397: 286826 of cap free
Amount of items: 1
Items: 
Size: 713175 Color: 0

Bin 3398: 286830 of cap free
Amount of items: 1
Items: 
Size: 713171 Color: 3

Bin 3399: 286869 of cap free
Amount of items: 1
Items: 
Size: 713132 Color: 2

Bin 3400: 287062 of cap free
Amount of items: 1
Items: 
Size: 712939 Color: 1

Bin 3401: 287200 of cap free
Amount of items: 1
Items: 
Size: 712801 Color: 1

Bin 3402: 287287 of cap free
Amount of items: 1
Items: 
Size: 712714 Color: 1

Bin 3403: 287343 of cap free
Amount of items: 1
Items: 
Size: 712658 Color: 3

Bin 3404: 287440 of cap free
Amount of items: 1
Items: 
Size: 712561 Color: 3

Bin 3405: 287476 of cap free
Amount of items: 1
Items: 
Size: 712525 Color: 4

Bin 3406: 287548 of cap free
Amount of items: 1
Items: 
Size: 712453 Color: 3

Bin 3407: 287612 of cap free
Amount of items: 1
Items: 
Size: 712389 Color: 1

Bin 3408: 288002 of cap free
Amount of items: 1
Items: 
Size: 711999 Color: 1

Bin 3409: 288058 of cap free
Amount of items: 1
Items: 
Size: 711943 Color: 1

Bin 3410: 288109 of cap free
Amount of items: 1
Items: 
Size: 711892 Color: 0

Bin 3411: 288124 of cap free
Amount of items: 1
Items: 
Size: 711877 Color: 0

Bin 3412: 288176 of cap free
Amount of items: 1
Items: 
Size: 711825 Color: 0

Bin 3413: 288205 of cap free
Amount of items: 1
Items: 
Size: 711796 Color: 3

Bin 3414: 288212 of cap free
Amount of items: 1
Items: 
Size: 711789 Color: 1

Bin 3415: 288231 of cap free
Amount of items: 1
Items: 
Size: 711770 Color: 1

Bin 3416: 288232 of cap free
Amount of items: 1
Items: 
Size: 711769 Color: 2

Bin 3417: 288266 of cap free
Amount of items: 1
Items: 
Size: 711735 Color: 2

Bin 3418: 288351 of cap free
Amount of items: 1
Items: 
Size: 711650 Color: 0

Bin 3419: 288425 of cap free
Amount of items: 1
Items: 
Size: 711576 Color: 4

Bin 3420: 288496 of cap free
Amount of items: 1
Items: 
Size: 711505 Color: 4

Bin 3421: 288656 of cap free
Amount of items: 1
Items: 
Size: 711345 Color: 1

Bin 3422: 288699 of cap free
Amount of items: 1
Items: 
Size: 711302 Color: 2

Bin 3423: 288726 of cap free
Amount of items: 1
Items: 
Size: 711275 Color: 1

Bin 3424: 288857 of cap free
Amount of items: 1
Items: 
Size: 711144 Color: 0

Bin 3425: 288883 of cap free
Amount of items: 1
Items: 
Size: 711118 Color: 0

Bin 3426: 288986 of cap free
Amount of items: 1
Items: 
Size: 711015 Color: 0

Bin 3427: 289033 of cap free
Amount of items: 1
Items: 
Size: 710968 Color: 1

Bin 3428: 289111 of cap free
Amount of items: 1
Items: 
Size: 710890 Color: 3

Bin 3429: 289138 of cap free
Amount of items: 1
Items: 
Size: 710863 Color: 1

Bin 3430: 289220 of cap free
Amount of items: 1
Items: 
Size: 710781 Color: 4

Bin 3431: 289293 of cap free
Amount of items: 1
Items: 
Size: 710708 Color: 3

Bin 3432: 289359 of cap free
Amount of items: 1
Items: 
Size: 710642 Color: 4

Bin 3433: 289378 of cap free
Amount of items: 1
Items: 
Size: 710623 Color: 3

Bin 3434: 289398 of cap free
Amount of items: 1
Items: 
Size: 710603 Color: 4

Bin 3435: 289452 of cap free
Amount of items: 1
Items: 
Size: 710549 Color: 4

Bin 3436: 289504 of cap free
Amount of items: 1
Items: 
Size: 710497 Color: 3

Bin 3437: 289545 of cap free
Amount of items: 1
Items: 
Size: 710456 Color: 4

Bin 3438: 289578 of cap free
Amount of items: 1
Items: 
Size: 710423 Color: 1

Bin 3439: 289771 of cap free
Amount of items: 1
Items: 
Size: 710230 Color: 4

Bin 3440: 289975 of cap free
Amount of items: 1
Items: 
Size: 710026 Color: 3

Bin 3441: 289978 of cap free
Amount of items: 1
Items: 
Size: 710023 Color: 4

Bin 3442: 290190 of cap free
Amount of items: 1
Items: 
Size: 709811 Color: 1

Bin 3443: 290237 of cap free
Amount of items: 1
Items: 
Size: 709764 Color: 1

Bin 3444: 290372 of cap free
Amount of items: 1
Items: 
Size: 709629 Color: 4

Bin 3445: 290407 of cap free
Amount of items: 1
Items: 
Size: 709594 Color: 0

Bin 3446: 290513 of cap free
Amount of items: 1
Items: 
Size: 709488 Color: 1

Bin 3447: 290673 of cap free
Amount of items: 1
Items: 
Size: 709328 Color: 0

Bin 3448: 290703 of cap free
Amount of items: 1
Items: 
Size: 709298 Color: 0

Bin 3449: 290724 of cap free
Amount of items: 1
Items: 
Size: 709277 Color: 0

Bin 3450: 290765 of cap free
Amount of items: 1
Items: 
Size: 709236 Color: 4

Bin 3451: 290769 of cap free
Amount of items: 1
Items: 
Size: 709232 Color: 2

Bin 3452: 290867 of cap free
Amount of items: 1
Items: 
Size: 709134 Color: 1

Bin 3453: 290871 of cap free
Amount of items: 1
Items: 
Size: 709130 Color: 0

Bin 3454: 290930 of cap free
Amount of items: 1
Items: 
Size: 709071 Color: 3

Bin 3455: 290960 of cap free
Amount of items: 1
Items: 
Size: 709041 Color: 0

Bin 3456: 291062 of cap free
Amount of items: 1
Items: 
Size: 708939 Color: 1

Bin 3457: 291095 of cap free
Amount of items: 1
Items: 
Size: 708906 Color: 1

Bin 3458: 291250 of cap free
Amount of items: 1
Items: 
Size: 708751 Color: 4

Bin 3459: 291297 of cap free
Amount of items: 1
Items: 
Size: 708704 Color: 0

Bin 3460: 291375 of cap free
Amount of items: 1
Items: 
Size: 708626 Color: 1

Bin 3461: 291416 of cap free
Amount of items: 1
Items: 
Size: 708585 Color: 3

Bin 3462: 291477 of cap free
Amount of items: 1
Items: 
Size: 708524 Color: 3

Bin 3463: 291478 of cap free
Amount of items: 1
Items: 
Size: 708523 Color: 0

Bin 3464: 291575 of cap free
Amount of items: 1
Items: 
Size: 708426 Color: 4

Bin 3465: 291639 of cap free
Amount of items: 1
Items: 
Size: 708362 Color: 4

Bin 3466: 291645 of cap free
Amount of items: 1
Items: 
Size: 708356 Color: 4

Bin 3467: 291651 of cap free
Amount of items: 1
Items: 
Size: 708350 Color: 3

Bin 3468: 291726 of cap free
Amount of items: 1
Items: 
Size: 708275 Color: 2

Bin 3469: 291761 of cap free
Amount of items: 1
Items: 
Size: 708240 Color: 2

Bin 3470: 291826 of cap free
Amount of items: 1
Items: 
Size: 708175 Color: 4

Bin 3471: 291861 of cap free
Amount of items: 1
Items: 
Size: 708140 Color: 4

Bin 3472: 291942 of cap free
Amount of items: 1
Items: 
Size: 708059 Color: 2

Bin 3473: 291945 of cap free
Amount of items: 1
Items: 
Size: 708056 Color: 4

Bin 3474: 292012 of cap free
Amount of items: 1
Items: 
Size: 707989 Color: 4

Bin 3475: 292049 of cap free
Amount of items: 1
Items: 
Size: 707952 Color: 1

Bin 3476: 292065 of cap free
Amount of items: 1
Items: 
Size: 707936 Color: 1

Bin 3477: 292227 of cap free
Amount of items: 1
Items: 
Size: 707774 Color: 2

Bin 3478: 292250 of cap free
Amount of items: 1
Items: 
Size: 707751 Color: 2

Bin 3479: 292279 of cap free
Amount of items: 1
Items: 
Size: 707722 Color: 2

Bin 3480: 292358 of cap free
Amount of items: 1
Items: 
Size: 707643 Color: 0

Bin 3481: 292393 of cap free
Amount of items: 1
Items: 
Size: 707608 Color: 2

Bin 3482: 292410 of cap free
Amount of items: 1
Items: 
Size: 707591 Color: 1

Bin 3483: 292416 of cap free
Amount of items: 1
Items: 
Size: 707585 Color: 4

Bin 3484: 292418 of cap free
Amount of items: 1
Items: 
Size: 707583 Color: 4

Bin 3485: 292548 of cap free
Amount of items: 1
Items: 
Size: 707453 Color: 0

Bin 3486: 292601 of cap free
Amount of items: 1
Items: 
Size: 707400 Color: 3

Bin 3487: 292613 of cap free
Amount of items: 1
Items: 
Size: 707388 Color: 0

Bin 3488: 292630 of cap free
Amount of items: 1
Items: 
Size: 707371 Color: 0

Bin 3489: 292723 of cap free
Amount of items: 1
Items: 
Size: 707278 Color: 1

Bin 3490: 292812 of cap free
Amount of items: 1
Items: 
Size: 707189 Color: 3

Bin 3491: 292836 of cap free
Amount of items: 1
Items: 
Size: 707165 Color: 4

Bin 3492: 292879 of cap free
Amount of items: 1
Items: 
Size: 707122 Color: 2

Bin 3493: 292883 of cap free
Amount of items: 1
Items: 
Size: 707118 Color: 3

Bin 3494: 292905 of cap free
Amount of items: 1
Items: 
Size: 707096 Color: 2

Bin 3495: 293103 of cap free
Amount of items: 1
Items: 
Size: 706898 Color: 4

Bin 3496: 293145 of cap free
Amount of items: 1
Items: 
Size: 706856 Color: 1

Bin 3497: 293195 of cap free
Amount of items: 1
Items: 
Size: 706806 Color: 2

Bin 3498: 293212 of cap free
Amount of items: 1
Items: 
Size: 706789 Color: 3

Bin 3499: 293298 of cap free
Amount of items: 1
Items: 
Size: 706703 Color: 4

Bin 3500: 293356 of cap free
Amount of items: 1
Items: 
Size: 706645 Color: 4

Bin 3501: 293778 of cap free
Amount of items: 1
Items: 
Size: 706223 Color: 4

Bin 3502: 293810 of cap free
Amount of items: 1
Items: 
Size: 706191 Color: 2

Bin 3503: 293885 of cap free
Amount of items: 1
Items: 
Size: 706116 Color: 1

Bin 3504: 293949 of cap free
Amount of items: 1
Items: 
Size: 706052 Color: 1

Bin 3505: 293994 of cap free
Amount of items: 1
Items: 
Size: 706007 Color: 3

Bin 3506: 294018 of cap free
Amount of items: 1
Items: 
Size: 705983 Color: 3

Bin 3507: 294057 of cap free
Amount of items: 1
Items: 
Size: 705944 Color: 1

Bin 3508: 294100 of cap free
Amount of items: 1
Items: 
Size: 705901 Color: 3

Bin 3509: 294228 of cap free
Amount of items: 1
Items: 
Size: 705773 Color: 1

Bin 3510: 294348 of cap free
Amount of items: 1
Items: 
Size: 705653 Color: 3

Bin 3511: 294369 of cap free
Amount of items: 1
Items: 
Size: 705632 Color: 1

Bin 3512: 294486 of cap free
Amount of items: 1
Items: 
Size: 705515 Color: 4

Bin 3513: 294509 of cap free
Amount of items: 1
Items: 
Size: 705492 Color: 2

Bin 3514: 294662 of cap free
Amount of items: 1
Items: 
Size: 705339 Color: 4

Bin 3515: 294751 of cap free
Amount of items: 1
Items: 
Size: 705250 Color: 4

Bin 3516: 294764 of cap free
Amount of items: 1
Items: 
Size: 705237 Color: 1

Bin 3517: 294846 of cap free
Amount of items: 1
Items: 
Size: 705155 Color: 3

Bin 3518: 295025 of cap free
Amount of items: 1
Items: 
Size: 704976 Color: 2

Bin 3519: 295100 of cap free
Amount of items: 1
Items: 
Size: 704901 Color: 3

Bin 3520: 295125 of cap free
Amount of items: 1
Items: 
Size: 704876 Color: 2

Bin 3521: 295136 of cap free
Amount of items: 1
Items: 
Size: 704865 Color: 0

Bin 3522: 295480 of cap free
Amount of items: 1
Items: 
Size: 704521 Color: 2

Bin 3523: 295492 of cap free
Amount of items: 1
Items: 
Size: 704509 Color: 1

Bin 3524: 295498 of cap free
Amount of items: 1
Items: 
Size: 704503 Color: 1

Bin 3525: 295653 of cap free
Amount of items: 1
Items: 
Size: 704348 Color: 3

Bin 3526: 295691 of cap free
Amount of items: 1
Items: 
Size: 704310 Color: 1

Bin 3527: 295692 of cap free
Amount of items: 1
Items: 
Size: 704309 Color: 1

Bin 3528: 295747 of cap free
Amount of items: 1
Items: 
Size: 704254 Color: 1

Bin 3529: 295757 of cap free
Amount of items: 1
Items: 
Size: 704244 Color: 2

Bin 3530: 295825 of cap free
Amount of items: 1
Items: 
Size: 704176 Color: 0

Bin 3531: 295964 of cap free
Amount of items: 1
Items: 
Size: 704037 Color: 2

Bin 3532: 296032 of cap free
Amount of items: 1
Items: 
Size: 703969 Color: 0

Bin 3533: 296139 of cap free
Amount of items: 1
Items: 
Size: 703862 Color: 4

Bin 3534: 296140 of cap free
Amount of items: 1
Items: 
Size: 703861 Color: 4

Bin 3535: 296291 of cap free
Amount of items: 1
Items: 
Size: 703710 Color: 2

Bin 3536: 296376 of cap free
Amount of items: 1
Items: 
Size: 703625 Color: 2

Bin 3537: 296450 of cap free
Amount of items: 1
Items: 
Size: 703551 Color: 0

Bin 3538: 296815 of cap free
Amount of items: 1
Items: 
Size: 703186 Color: 1

Bin 3539: 296887 of cap free
Amount of items: 1
Items: 
Size: 703114 Color: 2

Bin 3540: 296990 of cap free
Amount of items: 1
Items: 
Size: 703011 Color: 0

Bin 3541: 297000 of cap free
Amount of items: 1
Items: 
Size: 703001 Color: 2

Bin 3542: 297010 of cap free
Amount of items: 1
Items: 
Size: 702991 Color: 1

Bin 3543: 297012 of cap free
Amount of items: 1
Items: 
Size: 702989 Color: 2

Bin 3544: 297095 of cap free
Amount of items: 1
Items: 
Size: 702906 Color: 0

Bin 3545: 297158 of cap free
Amount of items: 1
Items: 
Size: 702843 Color: 2

Bin 3546: 297173 of cap free
Amount of items: 1
Items: 
Size: 702828 Color: 3

Bin 3547: 297245 of cap free
Amount of items: 1
Items: 
Size: 702756 Color: 0

Bin 3548: 297259 of cap free
Amount of items: 1
Items: 
Size: 702742 Color: 3

Bin 3549: 297318 of cap free
Amount of items: 1
Items: 
Size: 702683 Color: 1

Bin 3550: 297319 of cap free
Amount of items: 1
Items: 
Size: 702682 Color: 3

Bin 3551: 297372 of cap free
Amount of items: 1
Items: 
Size: 702629 Color: 3

Bin 3552: 297536 of cap free
Amount of items: 1
Items: 
Size: 702465 Color: 2

Bin 3553: 297538 of cap free
Amount of items: 1
Items: 
Size: 702463 Color: 1

Bin 3554: 297856 of cap free
Amount of items: 1
Items: 
Size: 702145 Color: 3

Bin 3555: 297947 of cap free
Amount of items: 1
Items: 
Size: 702054 Color: 1

Bin 3556: 297960 of cap free
Amount of items: 1
Items: 
Size: 702041 Color: 3

Bin 3557: 297996 of cap free
Amount of items: 1
Items: 
Size: 702005 Color: 3

Bin 3558: 298006 of cap free
Amount of items: 1
Items: 
Size: 701995 Color: 3

Bin 3559: 298055 of cap free
Amount of items: 1
Items: 
Size: 701946 Color: 4

Bin 3560: 298137 of cap free
Amount of items: 1
Items: 
Size: 701864 Color: 4

Bin 3561: 298147 of cap free
Amount of items: 1
Items: 
Size: 701854 Color: 2

Bin 3562: 298159 of cap free
Amount of items: 1
Items: 
Size: 701842 Color: 3

Bin 3563: 298285 of cap free
Amount of items: 1
Items: 
Size: 701716 Color: 3

Bin 3564: 298289 of cap free
Amount of items: 1
Items: 
Size: 701712 Color: 1

Bin 3565: 298314 of cap free
Amount of items: 1
Items: 
Size: 701687 Color: 4

Bin 3566: 298348 of cap free
Amount of items: 1
Items: 
Size: 701653 Color: 1

Bin 3567: 298350 of cap free
Amount of items: 1
Items: 
Size: 701651 Color: 1

Bin 3568: 298368 of cap free
Amount of items: 1
Items: 
Size: 701633 Color: 3

Bin 3569: 298495 of cap free
Amount of items: 1
Items: 
Size: 701506 Color: 2

Bin 3570: 298613 of cap free
Amount of items: 1
Items: 
Size: 701388 Color: 0

Bin 3571: 298681 of cap free
Amount of items: 1
Items: 
Size: 701320 Color: 2

Bin 3572: 298696 of cap free
Amount of items: 1
Items: 
Size: 701305 Color: 0

Bin 3573: 298815 of cap free
Amount of items: 1
Items: 
Size: 701186 Color: 3

Bin 3574: 298825 of cap free
Amount of items: 1
Items: 
Size: 701176 Color: 4

Bin 3575: 299140 of cap free
Amount of items: 1
Items: 
Size: 700861 Color: 2

Bin 3576: 299180 of cap free
Amount of items: 1
Items: 
Size: 700821 Color: 1

Bin 3577: 299298 of cap free
Amount of items: 1
Items: 
Size: 700703 Color: 0

Bin 3578: 299303 of cap free
Amount of items: 1
Items: 
Size: 700698 Color: 3

Bin 3579: 299436 of cap free
Amount of items: 1
Items: 
Size: 700565 Color: 2

Bin 3580: 299457 of cap free
Amount of items: 1
Items: 
Size: 700544 Color: 3

Bin 3581: 299485 of cap free
Amount of items: 1
Items: 
Size: 700516 Color: 3

Bin 3582: 299490 of cap free
Amount of items: 1
Items: 
Size: 700511 Color: 2

Bin 3583: 299563 of cap free
Amount of items: 1
Items: 
Size: 700438 Color: 1

Bin 3584: 299626 of cap free
Amount of items: 1
Items: 
Size: 700375 Color: 3

Bin 3585: 299719 of cap free
Amount of items: 1
Items: 
Size: 700282 Color: 2

Bin 3586: 299776 of cap free
Amount of items: 1
Items: 
Size: 700225 Color: 2

Bin 3587: 299801 of cap free
Amount of items: 1
Items: 
Size: 700200 Color: 3

Bin 3588: 299869 of cap free
Amount of items: 1
Items: 
Size: 700132 Color: 4

Bin 3589: 299978 of cap free
Amount of items: 1
Items: 
Size: 700023 Color: 0

Bin 3590: 300034 of cap free
Amount of items: 1
Items: 
Size: 699967 Color: 0

Bin 3591: 300122 of cap free
Amount of items: 1
Items: 
Size: 699879 Color: 1

Bin 3592: 300177 of cap free
Amount of items: 1
Items: 
Size: 699824 Color: 1

Bin 3593: 300236 of cap free
Amount of items: 1
Items: 
Size: 699765 Color: 1

Bin 3594: 300401 of cap free
Amount of items: 1
Items: 
Size: 699600 Color: 0

Bin 3595: 300647 of cap free
Amount of items: 1
Items: 
Size: 699354 Color: 1

Bin 3596: 300661 of cap free
Amount of items: 1
Items: 
Size: 699340 Color: 2

Bin 3597: 300760 of cap free
Amount of items: 1
Items: 
Size: 699241 Color: 3

Bin 3598: 300800 of cap free
Amount of items: 1
Items: 
Size: 699201 Color: 1

Bin 3599: 300876 of cap free
Amount of items: 1
Items: 
Size: 699125 Color: 3

Bin 3600: 300888 of cap free
Amount of items: 1
Items: 
Size: 699113 Color: 4

Bin 3601: 300908 of cap free
Amount of items: 1
Items: 
Size: 699093 Color: 4

Bin 3602: 300923 of cap free
Amount of items: 1
Items: 
Size: 699078 Color: 4

Bin 3603: 300938 of cap free
Amount of items: 1
Items: 
Size: 699063 Color: 4

Bin 3604: 301034 of cap free
Amount of items: 1
Items: 
Size: 698967 Color: 4

Bin 3605: 301255 of cap free
Amount of items: 1
Items: 
Size: 698746 Color: 4

Bin 3606: 301304 of cap free
Amount of items: 1
Items: 
Size: 698697 Color: 3

Bin 3607: 301590 of cap free
Amount of items: 1
Items: 
Size: 698411 Color: 2

Bin 3608: 301620 of cap free
Amount of items: 1
Items: 
Size: 698381 Color: 1

Bin 3609: 301628 of cap free
Amount of items: 1
Items: 
Size: 698373 Color: 1

Bin 3610: 301637 of cap free
Amount of items: 1
Items: 
Size: 698364 Color: 3

Bin 3611: 301682 of cap free
Amount of items: 1
Items: 
Size: 698319 Color: 3

Bin 3612: 301887 of cap free
Amount of items: 1
Items: 
Size: 698114 Color: 2

Bin 3613: 301913 of cap free
Amount of items: 1
Items: 
Size: 698088 Color: 2

Bin 3614: 301950 of cap free
Amount of items: 1
Items: 
Size: 698051 Color: 3

Bin 3615: 301985 of cap free
Amount of items: 1
Items: 
Size: 698016 Color: 4

Bin 3616: 302042 of cap free
Amount of items: 1
Items: 
Size: 697959 Color: 3

Bin 3617: 302058 of cap free
Amount of items: 1
Items: 
Size: 697943 Color: 4

Bin 3618: 302081 of cap free
Amount of items: 1
Items: 
Size: 697920 Color: 1

Bin 3619: 302130 of cap free
Amount of items: 1
Items: 
Size: 697871 Color: 3

Bin 3620: 302138 of cap free
Amount of items: 1
Items: 
Size: 697863 Color: 0

Bin 3621: 302140 of cap free
Amount of items: 1
Items: 
Size: 697861 Color: 1

Bin 3622: 302150 of cap free
Amount of items: 1
Items: 
Size: 697851 Color: 3

Bin 3623: 302155 of cap free
Amount of items: 1
Items: 
Size: 697846 Color: 4

Bin 3624: 302218 of cap free
Amount of items: 1
Items: 
Size: 697783 Color: 4

Bin 3625: 302230 of cap free
Amount of items: 1
Items: 
Size: 697771 Color: 2

Bin 3626: 302291 of cap free
Amount of items: 1
Items: 
Size: 697710 Color: 2

Bin 3627: 302419 of cap free
Amount of items: 1
Items: 
Size: 697582 Color: 4

Bin 3628: 302509 of cap free
Amount of items: 1
Items: 
Size: 697492 Color: 0

Bin 3629: 302518 of cap free
Amount of items: 1
Items: 
Size: 697483 Color: 1

Bin 3630: 302659 of cap free
Amount of items: 1
Items: 
Size: 697342 Color: 4

Bin 3631: 302688 of cap free
Amount of items: 1
Items: 
Size: 697313 Color: 4

Bin 3632: 302698 of cap free
Amount of items: 1
Items: 
Size: 697303 Color: 1

Bin 3633: 302729 of cap free
Amount of items: 1
Items: 
Size: 697272 Color: 1

Bin 3634: 302771 of cap free
Amount of items: 1
Items: 
Size: 697230 Color: 3

Bin 3635: 302781 of cap free
Amount of items: 1
Items: 
Size: 697220 Color: 4

Bin 3636: 302818 of cap free
Amount of items: 1
Items: 
Size: 697183 Color: 0

Bin 3637: 302903 of cap free
Amount of items: 1
Items: 
Size: 697098 Color: 3

Bin 3638: 302967 of cap free
Amount of items: 1
Items: 
Size: 697034 Color: 3

Bin 3639: 302972 of cap free
Amount of items: 1
Items: 
Size: 697029 Color: 0

Bin 3640: 303048 of cap free
Amount of items: 1
Items: 
Size: 696953 Color: 0

Bin 3641: 303227 of cap free
Amount of items: 1
Items: 
Size: 696774 Color: 4

Bin 3642: 303293 of cap free
Amount of items: 1
Items: 
Size: 696708 Color: 1

Bin 3643: 303301 of cap free
Amount of items: 1
Items: 
Size: 696700 Color: 0

Bin 3644: 303395 of cap free
Amount of items: 1
Items: 
Size: 696606 Color: 4

Bin 3645: 303447 of cap free
Amount of items: 1
Items: 
Size: 696554 Color: 1

Bin 3646: 303477 of cap free
Amount of items: 1
Items: 
Size: 696524 Color: 3

Bin 3647: 303591 of cap free
Amount of items: 1
Items: 
Size: 696410 Color: 0

Bin 3648: 303716 of cap free
Amount of items: 1
Items: 
Size: 696285 Color: 2

Bin 3649: 303763 of cap free
Amount of items: 1
Items: 
Size: 696238 Color: 3

Bin 3650: 303825 of cap free
Amount of items: 1
Items: 
Size: 696176 Color: 3

Bin 3651: 303860 of cap free
Amount of items: 1
Items: 
Size: 696141 Color: 0

Bin 3652: 303860 of cap free
Amount of items: 1
Items: 
Size: 696141 Color: 4

Bin 3653: 304009 of cap free
Amount of items: 1
Items: 
Size: 695992 Color: 4

Bin 3654: 304026 of cap free
Amount of items: 1
Items: 
Size: 695975 Color: 3

Bin 3655: 304118 of cap free
Amount of items: 1
Items: 
Size: 695883 Color: 0

Bin 3656: 304257 of cap free
Amount of items: 1
Items: 
Size: 695744 Color: 4

Bin 3657: 304262 of cap free
Amount of items: 1
Items: 
Size: 695739 Color: 1

Bin 3658: 304264 of cap free
Amount of items: 1
Items: 
Size: 695737 Color: 1

Bin 3659: 304397 of cap free
Amount of items: 1
Items: 
Size: 695604 Color: 4

Bin 3660: 304404 of cap free
Amount of items: 1
Items: 
Size: 695597 Color: 3

Bin 3661: 304636 of cap free
Amount of items: 1
Items: 
Size: 695365 Color: 2

Bin 3662: 304640 of cap free
Amount of items: 1
Items: 
Size: 695361 Color: 0

Bin 3663: 304737 of cap free
Amount of items: 1
Items: 
Size: 695264 Color: 0

Bin 3664: 304783 of cap free
Amount of items: 1
Items: 
Size: 695218 Color: 4

Bin 3665: 304829 of cap free
Amount of items: 1
Items: 
Size: 695172 Color: 1

Bin 3666: 304962 of cap free
Amount of items: 1
Items: 
Size: 695039 Color: 0

Bin 3667: 305097 of cap free
Amount of items: 1
Items: 
Size: 694904 Color: 2

Bin 3668: 305227 of cap free
Amount of items: 1
Items: 
Size: 694774 Color: 4

Bin 3669: 305286 of cap free
Amount of items: 1
Items: 
Size: 694715 Color: 0

Bin 3670: 305324 of cap free
Amount of items: 1
Items: 
Size: 694677 Color: 4

Bin 3671: 305391 of cap free
Amount of items: 1
Items: 
Size: 694610 Color: 2

Bin 3672: 305398 of cap free
Amount of items: 1
Items: 
Size: 694603 Color: 1

Bin 3673: 305506 of cap free
Amount of items: 1
Items: 
Size: 694495 Color: 4

Bin 3674: 305939 of cap free
Amount of items: 1
Items: 
Size: 694062 Color: 0

Bin 3675: 305963 of cap free
Amount of items: 1
Items: 
Size: 694038 Color: 4

Bin 3676: 306097 of cap free
Amount of items: 1
Items: 
Size: 693904 Color: 4

Bin 3677: 306127 of cap free
Amount of items: 1
Items: 
Size: 693874 Color: 0

Bin 3678: 306250 of cap free
Amount of items: 1
Items: 
Size: 693751 Color: 4

Bin 3679: 306257 of cap free
Amount of items: 1
Items: 
Size: 693744 Color: 4

Bin 3680: 306315 of cap free
Amount of items: 1
Items: 
Size: 693686 Color: 3

Bin 3681: 306540 of cap free
Amount of items: 1
Items: 
Size: 693461 Color: 4

Bin 3682: 306569 of cap free
Amount of items: 1
Items: 
Size: 693432 Color: 3

Bin 3683: 306724 of cap free
Amount of items: 1
Items: 
Size: 693277 Color: 0

Bin 3684: 306728 of cap free
Amount of items: 1
Items: 
Size: 693273 Color: 4

Bin 3685: 306757 of cap free
Amount of items: 1
Items: 
Size: 693244 Color: 3

Bin 3686: 306767 of cap free
Amount of items: 1
Items: 
Size: 693234 Color: 4

Bin 3687: 306835 of cap free
Amount of items: 1
Items: 
Size: 693166 Color: 1

Bin 3688: 306862 of cap free
Amount of items: 1
Items: 
Size: 693139 Color: 4

Bin 3689: 307189 of cap free
Amount of items: 1
Items: 
Size: 692812 Color: 0

Bin 3690: 307245 of cap free
Amount of items: 1
Items: 
Size: 692756 Color: 1

Bin 3691: 307362 of cap free
Amount of items: 1
Items: 
Size: 692639 Color: 4

Bin 3692: 307449 of cap free
Amount of items: 1
Items: 
Size: 692552 Color: 3

Bin 3693: 307568 of cap free
Amount of items: 1
Items: 
Size: 692433 Color: 3

Bin 3694: 307593 of cap free
Amount of items: 1
Items: 
Size: 692408 Color: 4

Bin 3695: 307630 of cap free
Amount of items: 1
Items: 
Size: 692371 Color: 3

Bin 3696: 307668 of cap free
Amount of items: 1
Items: 
Size: 692333 Color: 4

Bin 3697: 307673 of cap free
Amount of items: 1
Items: 
Size: 692328 Color: 2

Bin 3698: 307757 of cap free
Amount of items: 1
Items: 
Size: 692244 Color: 3

Bin 3699: 307913 of cap free
Amount of items: 1
Items: 
Size: 692088 Color: 2

Bin 3700: 307955 of cap free
Amount of items: 1
Items: 
Size: 692046 Color: 4

Bin 3701: 308110 of cap free
Amount of items: 1
Items: 
Size: 691891 Color: 4

Bin 3702: 308248 of cap free
Amount of items: 1
Items: 
Size: 691753 Color: 2

Bin 3703: 308287 of cap free
Amount of items: 1
Items: 
Size: 691714 Color: 0

Bin 3704: 308297 of cap free
Amount of items: 1
Items: 
Size: 691704 Color: 0

Bin 3705: 308373 of cap free
Amount of items: 1
Items: 
Size: 691628 Color: 4

Bin 3706: 308416 of cap free
Amount of items: 1
Items: 
Size: 691585 Color: 4

Bin 3707: 308437 of cap free
Amount of items: 1
Items: 
Size: 691564 Color: 4

Bin 3708: 308569 of cap free
Amount of items: 1
Items: 
Size: 691432 Color: 2

Bin 3709: 308570 of cap free
Amount of items: 1
Items: 
Size: 691431 Color: 3

Bin 3710: 308624 of cap free
Amount of items: 1
Items: 
Size: 691377 Color: 2

Bin 3711: 308763 of cap free
Amount of items: 1
Items: 
Size: 691238 Color: 4

Bin 3712: 308926 of cap free
Amount of items: 1
Items: 
Size: 691075 Color: 1

Bin 3713: 308934 of cap free
Amount of items: 1
Items: 
Size: 691067 Color: 0

Bin 3714: 308950 of cap free
Amount of items: 1
Items: 
Size: 691051 Color: 4

Bin 3715: 309090 of cap free
Amount of items: 1
Items: 
Size: 690911 Color: 4

Bin 3716: 309144 of cap free
Amount of items: 1
Items: 
Size: 690857 Color: 1

Bin 3717: 309156 of cap free
Amount of items: 1
Items: 
Size: 690845 Color: 2

Bin 3718: 309283 of cap free
Amount of items: 1
Items: 
Size: 690718 Color: 4

Bin 3719: 309324 of cap free
Amount of items: 1
Items: 
Size: 690677 Color: 0

Bin 3720: 309341 of cap free
Amount of items: 1
Items: 
Size: 690660 Color: 1

Bin 3721: 309540 of cap free
Amount of items: 1
Items: 
Size: 690461 Color: 0

Bin 3722: 309557 of cap free
Amount of items: 1
Items: 
Size: 690444 Color: 3

Bin 3723: 309585 of cap free
Amount of items: 1
Items: 
Size: 690416 Color: 4

Bin 3724: 309724 of cap free
Amount of items: 1
Items: 
Size: 690277 Color: 4

Bin 3725: 309798 of cap free
Amount of items: 1
Items: 
Size: 690203 Color: 3

Bin 3726: 309943 of cap free
Amount of items: 1
Items: 
Size: 690058 Color: 4

Bin 3727: 310064 of cap free
Amount of items: 1
Items: 
Size: 689937 Color: 2

Bin 3728: 310238 of cap free
Amount of items: 1
Items: 
Size: 689763 Color: 3

Bin 3729: 310239 of cap free
Amount of items: 1
Items: 
Size: 689762 Color: 3

Bin 3730: 310249 of cap free
Amount of items: 1
Items: 
Size: 689752 Color: 2

Bin 3731: 310283 of cap free
Amount of items: 1
Items: 
Size: 689718 Color: 3

Bin 3732: 310483 of cap free
Amount of items: 1
Items: 
Size: 689518 Color: 4

Bin 3733: 310546 of cap free
Amount of items: 1
Items: 
Size: 689455 Color: 3

Bin 3734: 310667 of cap free
Amount of items: 1
Items: 
Size: 689334 Color: 2

Bin 3735: 310693 of cap free
Amount of items: 1
Items: 
Size: 689308 Color: 3

Bin 3736: 310706 of cap free
Amount of items: 1
Items: 
Size: 689295 Color: 2

Bin 3737: 310740 of cap free
Amount of items: 1
Items: 
Size: 689261 Color: 3

Bin 3738: 310785 of cap free
Amount of items: 1
Items: 
Size: 689216 Color: 4

Bin 3739: 310793 of cap free
Amount of items: 1
Items: 
Size: 689208 Color: 1

Bin 3740: 310956 of cap free
Amount of items: 1
Items: 
Size: 689045 Color: 1

Bin 3741: 311012 of cap free
Amount of items: 1
Items: 
Size: 688989 Color: 3

Bin 3742: 311076 of cap free
Amount of items: 1
Items: 
Size: 688925 Color: 0

Bin 3743: 311144 of cap free
Amount of items: 1
Items: 
Size: 688857 Color: 3

Bin 3744: 311154 of cap free
Amount of items: 1
Items: 
Size: 688847 Color: 1

Bin 3745: 311194 of cap free
Amount of items: 1
Items: 
Size: 688807 Color: 2

Bin 3746: 311219 of cap free
Amount of items: 1
Items: 
Size: 688782 Color: 0

Bin 3747: 311268 of cap free
Amount of items: 1
Items: 
Size: 688733 Color: 0

Bin 3748: 311313 of cap free
Amount of items: 1
Items: 
Size: 688688 Color: 2

Bin 3749: 311381 of cap free
Amount of items: 1
Items: 
Size: 688620 Color: 0

Bin 3750: 311531 of cap free
Amount of items: 1
Items: 
Size: 688470 Color: 2

Bin 3751: 311546 of cap free
Amount of items: 1
Items: 
Size: 688455 Color: 2

Bin 3752: 311552 of cap free
Amount of items: 1
Items: 
Size: 688449 Color: 3

Bin 3753: 311567 of cap free
Amount of items: 1
Items: 
Size: 688434 Color: 4

Bin 3754: 311632 of cap free
Amount of items: 1
Items: 
Size: 688369 Color: 0

Bin 3755: 311656 of cap free
Amount of items: 1
Items: 
Size: 688345 Color: 2

Bin 3756: 311803 of cap free
Amount of items: 1
Items: 
Size: 688198 Color: 3

Bin 3757: 311936 of cap free
Amount of items: 1
Items: 
Size: 688065 Color: 1

Bin 3758: 311963 of cap free
Amount of items: 1
Items: 
Size: 688038 Color: 0

Bin 3759: 311968 of cap free
Amount of items: 1
Items: 
Size: 688033 Color: 4

Bin 3760: 312007 of cap free
Amount of items: 1
Items: 
Size: 687994 Color: 0

Bin 3761: 312021 of cap free
Amount of items: 1
Items: 
Size: 687980 Color: 4

Bin 3762: 312039 of cap free
Amount of items: 1
Items: 
Size: 687962 Color: 1

Bin 3763: 312141 of cap free
Amount of items: 1
Items: 
Size: 687860 Color: 3

Bin 3764: 312160 of cap free
Amount of items: 1
Items: 
Size: 687841 Color: 1

Bin 3765: 312196 of cap free
Amount of items: 1
Items: 
Size: 687805 Color: 2

Bin 3766: 312250 of cap free
Amount of items: 1
Items: 
Size: 687751 Color: 3

Bin 3767: 312281 of cap free
Amount of items: 1
Items: 
Size: 687720 Color: 4

Bin 3768: 312283 of cap free
Amount of items: 1
Items: 
Size: 687718 Color: 2

Bin 3769: 312343 of cap free
Amount of items: 1
Items: 
Size: 687658 Color: 2

Bin 3770: 312348 of cap free
Amount of items: 1
Items: 
Size: 687653 Color: 3

Bin 3771: 312374 of cap free
Amount of items: 1
Items: 
Size: 687627 Color: 1

Bin 3772: 312385 of cap free
Amount of items: 1
Items: 
Size: 687616 Color: 2

Bin 3773: 312449 of cap free
Amount of items: 1
Items: 
Size: 687552 Color: 4

Bin 3774: 312476 of cap free
Amount of items: 1
Items: 
Size: 687525 Color: 2

Bin 3775: 312517 of cap free
Amount of items: 1
Items: 
Size: 687484 Color: 4

Bin 3776: 312549 of cap free
Amount of items: 1
Items: 
Size: 687452 Color: 1

Bin 3777: 312602 of cap free
Amount of items: 1
Items: 
Size: 687399 Color: 1

Bin 3778: 312625 of cap free
Amount of items: 1
Items: 
Size: 687376 Color: 0

Bin 3779: 312648 of cap free
Amount of items: 1
Items: 
Size: 687353 Color: 0

Bin 3780: 312695 of cap free
Amount of items: 1
Items: 
Size: 687306 Color: 3

Bin 3781: 312702 of cap free
Amount of items: 1
Items: 
Size: 687299 Color: 0

Bin 3782: 312719 of cap free
Amount of items: 1
Items: 
Size: 687282 Color: 2

Bin 3783: 312771 of cap free
Amount of items: 1
Items: 
Size: 687230 Color: 0

Bin 3784: 312800 of cap free
Amount of items: 1
Items: 
Size: 687201 Color: 1

Bin 3785: 312834 of cap free
Amount of items: 1
Items: 
Size: 687167 Color: 4

Bin 3786: 312839 of cap free
Amount of items: 1
Items: 
Size: 687162 Color: 3

Bin 3787: 312897 of cap free
Amount of items: 1
Items: 
Size: 687104 Color: 3

Bin 3788: 313130 of cap free
Amount of items: 1
Items: 
Size: 686871 Color: 2

Bin 3789: 313141 of cap free
Amount of items: 1
Items: 
Size: 686860 Color: 3

Bin 3790: 313184 of cap free
Amount of items: 1
Items: 
Size: 686817 Color: 4

Bin 3791: 313197 of cap free
Amount of items: 1
Items: 
Size: 686804 Color: 1

Bin 3792: 313201 of cap free
Amount of items: 1
Items: 
Size: 686800 Color: 1

Bin 3793: 313213 of cap free
Amount of items: 1
Items: 
Size: 686788 Color: 3

Bin 3794: 313282 of cap free
Amount of items: 1
Items: 
Size: 686719 Color: 4

Bin 3795: 313322 of cap free
Amount of items: 1
Items: 
Size: 686679 Color: 2

Bin 3796: 313501 of cap free
Amount of items: 1
Items: 
Size: 686500 Color: 3

Bin 3797: 313534 of cap free
Amount of items: 1
Items: 
Size: 686467 Color: 0

Bin 3798: 313545 of cap free
Amount of items: 1
Items: 
Size: 686456 Color: 4

Bin 3799: 313583 of cap free
Amount of items: 1
Items: 
Size: 686418 Color: 4

Bin 3800: 313794 of cap free
Amount of items: 1
Items: 
Size: 686207 Color: 3

Bin 3801: 313798 of cap free
Amount of items: 1
Items: 
Size: 686203 Color: 0

Bin 3802: 314116 of cap free
Amount of items: 1
Items: 
Size: 685885 Color: 2

Bin 3803: 314193 of cap free
Amount of items: 1
Items: 
Size: 685808 Color: 0

Bin 3804: 314481 of cap free
Amount of items: 1
Items: 
Size: 685520 Color: 4

Bin 3805: 314490 of cap free
Amount of items: 1
Items: 
Size: 685511 Color: 4

Bin 3806: 314557 of cap free
Amount of items: 1
Items: 
Size: 685444 Color: 0

Bin 3807: 314578 of cap free
Amount of items: 1
Items: 
Size: 685423 Color: 3

Bin 3808: 314580 of cap free
Amount of items: 1
Items: 
Size: 685421 Color: 4

Bin 3809: 314783 of cap free
Amount of items: 1
Items: 
Size: 685218 Color: 0

Bin 3810: 314784 of cap free
Amount of items: 1
Items: 
Size: 685217 Color: 4

Bin 3811: 314817 of cap free
Amount of items: 1
Items: 
Size: 685184 Color: 1

Bin 3812: 314836 of cap free
Amount of items: 1
Items: 
Size: 685165 Color: 1

Bin 3813: 314960 of cap free
Amount of items: 1
Items: 
Size: 685041 Color: 2

Bin 3814: 314983 of cap free
Amount of items: 1
Items: 
Size: 685018 Color: 3

Bin 3815: 315032 of cap free
Amount of items: 1
Items: 
Size: 684969 Color: 4

Bin 3816: 315052 of cap free
Amount of items: 1
Items: 
Size: 684949 Color: 1

Bin 3817: 315119 of cap free
Amount of items: 1
Items: 
Size: 684882 Color: 4

Bin 3818: 315281 of cap free
Amount of items: 1
Items: 
Size: 684720 Color: 0

Bin 3819: 315283 of cap free
Amount of items: 1
Items: 
Size: 684718 Color: 4

Bin 3820: 315394 of cap free
Amount of items: 1
Items: 
Size: 684607 Color: 3

Bin 3821: 315395 of cap free
Amount of items: 1
Items: 
Size: 684606 Color: 0

Bin 3822: 315406 of cap free
Amount of items: 1
Items: 
Size: 684595 Color: 3

Bin 3823: 315437 of cap free
Amount of items: 1
Items: 
Size: 684564 Color: 3

Bin 3824: 315530 of cap free
Amount of items: 1
Items: 
Size: 684471 Color: 4

Bin 3825: 315669 of cap free
Amount of items: 1
Items: 
Size: 684332 Color: 1

Bin 3826: 315845 of cap free
Amount of items: 1
Items: 
Size: 684156 Color: 1

Bin 3827: 315945 of cap free
Amount of items: 1
Items: 
Size: 684056 Color: 0

Bin 3828: 316086 of cap free
Amount of items: 1
Items: 
Size: 683915 Color: 2

Bin 3829: 316117 of cap free
Amount of items: 1
Items: 
Size: 683884 Color: 3

Bin 3830: 316141 of cap free
Amount of items: 1
Items: 
Size: 683860 Color: 0

Bin 3831: 316178 of cap free
Amount of items: 1
Items: 
Size: 683823 Color: 2

Bin 3832: 316275 of cap free
Amount of items: 1
Items: 
Size: 683726 Color: 0

Bin 3833: 316303 of cap free
Amount of items: 1
Items: 
Size: 683698 Color: 2

Bin 3834: 316368 of cap free
Amount of items: 1
Items: 
Size: 683633 Color: 4

Bin 3835: 316404 of cap free
Amount of items: 1
Items: 
Size: 683597 Color: 4

Bin 3836: 316514 of cap free
Amount of items: 1
Items: 
Size: 683487 Color: 0

Bin 3837: 316555 of cap free
Amount of items: 1
Items: 
Size: 683446 Color: 4

Bin 3838: 316736 of cap free
Amount of items: 1
Items: 
Size: 683265 Color: 0

Bin 3839: 316800 of cap free
Amount of items: 1
Items: 
Size: 683201 Color: 0

Bin 3840: 316833 of cap free
Amount of items: 1
Items: 
Size: 683168 Color: 1

Bin 3841: 316908 of cap free
Amount of items: 1
Items: 
Size: 683093 Color: 4

Bin 3842: 317082 of cap free
Amount of items: 1
Items: 
Size: 682919 Color: 2

Bin 3843: 317514 of cap free
Amount of items: 1
Items: 
Size: 682487 Color: 3

Bin 3844: 317619 of cap free
Amount of items: 1
Items: 
Size: 682382 Color: 0

Bin 3845: 317630 of cap free
Amount of items: 1
Items: 
Size: 682371 Color: 2

Bin 3846: 317697 of cap free
Amount of items: 1
Items: 
Size: 682304 Color: 3

Bin 3847: 317734 of cap free
Amount of items: 1
Items: 
Size: 682267 Color: 1

Bin 3848: 317800 of cap free
Amount of items: 1
Items: 
Size: 682201 Color: 3

Bin 3849: 317919 of cap free
Amount of items: 1
Items: 
Size: 682082 Color: 3

Bin 3850: 317962 of cap free
Amount of items: 1
Items: 
Size: 682039 Color: 2

Bin 3851: 317965 of cap free
Amount of items: 1
Items: 
Size: 682036 Color: 1

Bin 3852: 317970 of cap free
Amount of items: 1
Items: 
Size: 682031 Color: 4

Bin 3853: 317988 of cap free
Amount of items: 1
Items: 
Size: 682013 Color: 1

Bin 3854: 318057 of cap free
Amount of items: 1
Items: 
Size: 681944 Color: 1

Bin 3855: 318114 of cap free
Amount of items: 1
Items: 
Size: 681887 Color: 4

Bin 3856: 318128 of cap free
Amount of items: 1
Items: 
Size: 681873 Color: 0

Bin 3857: 318142 of cap free
Amount of items: 1
Items: 
Size: 681859 Color: 2

Bin 3858: 318186 of cap free
Amount of items: 1
Items: 
Size: 681815 Color: 0

Bin 3859: 318218 of cap free
Amount of items: 1
Items: 
Size: 681783 Color: 0

Bin 3860: 318384 of cap free
Amount of items: 1
Items: 
Size: 681617 Color: 3

Bin 3861: 318443 of cap free
Amount of items: 1
Items: 
Size: 681558 Color: 3

Bin 3862: 318640 of cap free
Amount of items: 1
Items: 
Size: 681361 Color: 3

Bin 3863: 318653 of cap free
Amount of items: 1
Items: 
Size: 681348 Color: 2

Bin 3864: 318688 of cap free
Amount of items: 1
Items: 
Size: 681313 Color: 2

Bin 3865: 318722 of cap free
Amount of items: 1
Items: 
Size: 681279 Color: 1

Bin 3866: 318791 of cap free
Amount of items: 1
Items: 
Size: 681210 Color: 2

Bin 3867: 318992 of cap free
Amount of items: 1
Items: 
Size: 681009 Color: 0

Bin 3868: 319048 of cap free
Amount of items: 1
Items: 
Size: 680953 Color: 4

Bin 3869: 319126 of cap free
Amount of items: 1
Items: 
Size: 680875 Color: 3

Bin 3870: 319145 of cap free
Amount of items: 1
Items: 
Size: 680856 Color: 1

Bin 3871: 319237 of cap free
Amount of items: 1
Items: 
Size: 680764 Color: 0

Bin 3872: 319306 of cap free
Amount of items: 1
Items: 
Size: 680695 Color: 4

Bin 3873: 319477 of cap free
Amount of items: 1
Items: 
Size: 680524 Color: 2

Bin 3874: 319592 of cap free
Amount of items: 1
Items: 
Size: 680409 Color: 3

Bin 3875: 319603 of cap free
Amount of items: 1
Items: 
Size: 680398 Color: 0

Bin 3876: 319650 of cap free
Amount of items: 1
Items: 
Size: 680351 Color: 4

Bin 3877: 319681 of cap free
Amount of items: 1
Items: 
Size: 680320 Color: 0

Bin 3878: 319686 of cap free
Amount of items: 1
Items: 
Size: 680315 Color: 3

Bin 3879: 319886 of cap free
Amount of items: 1
Items: 
Size: 680115 Color: 0

Bin 3880: 319978 of cap free
Amount of items: 1
Items: 
Size: 680023 Color: 3

Bin 3881: 320066 of cap free
Amount of items: 1
Items: 
Size: 679935 Color: 4

Bin 3882: 320068 of cap free
Amount of items: 1
Items: 
Size: 679933 Color: 4

Bin 3883: 320144 of cap free
Amount of items: 1
Items: 
Size: 679857 Color: 4

Bin 3884: 320153 of cap free
Amount of items: 1
Items: 
Size: 679848 Color: 2

Bin 3885: 320270 of cap free
Amount of items: 1
Items: 
Size: 679731 Color: 0

Bin 3886: 320514 of cap free
Amount of items: 1
Items: 
Size: 679487 Color: 3

Bin 3887: 320607 of cap free
Amount of items: 1
Items: 
Size: 679394 Color: 1

Bin 3888: 320672 of cap free
Amount of items: 1
Items: 
Size: 679329 Color: 2

Bin 3889: 320882 of cap free
Amount of items: 1
Items: 
Size: 679119 Color: 0

Bin 3890: 320903 of cap free
Amount of items: 1
Items: 
Size: 679098 Color: 2

Bin 3891: 320933 of cap free
Amount of items: 1
Items: 
Size: 679068 Color: 0

Bin 3892: 320950 of cap free
Amount of items: 1
Items: 
Size: 679051 Color: 1

Bin 3893: 321022 of cap free
Amount of items: 1
Items: 
Size: 678979 Color: 1

Bin 3894: 321043 of cap free
Amount of items: 1
Items: 
Size: 678958 Color: 0

Bin 3895: 321081 of cap free
Amount of items: 1
Items: 
Size: 678920 Color: 1

Bin 3896: 321087 of cap free
Amount of items: 1
Items: 
Size: 678914 Color: 0

Bin 3897: 321131 of cap free
Amount of items: 1
Items: 
Size: 678870 Color: 4

Bin 3898: 321167 of cap free
Amount of items: 1
Items: 
Size: 678834 Color: 4

Bin 3899: 321237 of cap free
Amount of items: 1
Items: 
Size: 678764 Color: 2

Bin 3900: 321431 of cap free
Amount of items: 1
Items: 
Size: 678570 Color: 3

Bin 3901: 321474 of cap free
Amount of items: 1
Items: 
Size: 678527 Color: 4

Bin 3902: 321524 of cap free
Amount of items: 1
Items: 
Size: 678477 Color: 3

Bin 3903: 321624 of cap free
Amount of items: 1
Items: 
Size: 678377 Color: 4

Bin 3904: 321654 of cap free
Amount of items: 1
Items: 
Size: 678347 Color: 1

Bin 3905: 321662 of cap free
Amount of items: 1
Items: 
Size: 678339 Color: 0

Bin 3906: 321670 of cap free
Amount of items: 1
Items: 
Size: 678331 Color: 0

Bin 3907: 321704 of cap free
Amount of items: 1
Items: 
Size: 678297 Color: 0

Bin 3908: 321776 of cap free
Amount of items: 1
Items: 
Size: 678225 Color: 0

Bin 3909: 321810 of cap free
Amount of items: 1
Items: 
Size: 678191 Color: 1

Bin 3910: 322267 of cap free
Amount of items: 1
Items: 
Size: 677734 Color: 4

Bin 3911: 322303 of cap free
Amount of items: 1
Items: 
Size: 677698 Color: 1

Bin 3912: 322310 of cap free
Amount of items: 1
Items: 
Size: 677691 Color: 4

Bin 3913: 322385 of cap free
Amount of items: 1
Items: 
Size: 677616 Color: 0

Bin 3914: 322448 of cap free
Amount of items: 1
Items: 
Size: 677553 Color: 4

Bin 3915: 322453 of cap free
Amount of items: 1
Items: 
Size: 677548 Color: 1

Bin 3916: 322635 of cap free
Amount of items: 1
Items: 
Size: 677366 Color: 2

Bin 3917: 322649 of cap free
Amount of items: 1
Items: 
Size: 677352 Color: 3

Bin 3918: 322801 of cap free
Amount of items: 1
Items: 
Size: 677200 Color: 1

Bin 3919: 322827 of cap free
Amount of items: 1
Items: 
Size: 677174 Color: 2

Bin 3920: 322896 of cap free
Amount of items: 1
Items: 
Size: 677105 Color: 1

Bin 3921: 322916 of cap free
Amount of items: 1
Items: 
Size: 677085 Color: 4

Bin 3922: 322992 of cap free
Amount of items: 1
Items: 
Size: 677009 Color: 1

Bin 3923: 323009 of cap free
Amount of items: 1
Items: 
Size: 676992 Color: 0

Bin 3924: 323024 of cap free
Amount of items: 1
Items: 
Size: 676977 Color: 3

Bin 3925: 323028 of cap free
Amount of items: 1
Items: 
Size: 676973 Color: 4

Bin 3926: 323046 of cap free
Amount of items: 1
Items: 
Size: 676955 Color: 4

Bin 3927: 323141 of cap free
Amount of items: 1
Items: 
Size: 676860 Color: 0

Bin 3928: 323158 of cap free
Amount of items: 1
Items: 
Size: 676843 Color: 0

Bin 3929: 323170 of cap free
Amount of items: 1
Items: 
Size: 676831 Color: 1

Bin 3930: 323273 of cap free
Amount of items: 1
Items: 
Size: 676728 Color: 4

Bin 3931: 323419 of cap free
Amount of items: 1
Items: 
Size: 676582 Color: 1

Bin 3932: 323788 of cap free
Amount of items: 1
Items: 
Size: 676213 Color: 0

Bin 3933: 323946 of cap free
Amount of items: 1
Items: 
Size: 676055 Color: 4

Bin 3934: 323957 of cap free
Amount of items: 1
Items: 
Size: 676044 Color: 1

Bin 3935: 323981 of cap free
Amount of items: 1
Items: 
Size: 676020 Color: 3

Bin 3936: 324019 of cap free
Amount of items: 1
Items: 
Size: 675982 Color: 2

Bin 3937: 324035 of cap free
Amount of items: 1
Items: 
Size: 675966 Color: 3

Bin 3938: 324225 of cap free
Amount of items: 1
Items: 
Size: 675776 Color: 3

Bin 3939: 324248 of cap free
Amount of items: 1
Items: 
Size: 675753 Color: 4

Bin 3940: 324277 of cap free
Amount of items: 1
Items: 
Size: 675724 Color: 4

Bin 3941: 324385 of cap free
Amount of items: 1
Items: 
Size: 675616 Color: 4

Bin 3942: 324446 of cap free
Amount of items: 1
Items: 
Size: 675555 Color: 0

Bin 3943: 324462 of cap free
Amount of items: 1
Items: 
Size: 675539 Color: 3

Bin 3944: 324626 of cap free
Amount of items: 1
Items: 
Size: 675375 Color: 1

Bin 3945: 324632 of cap free
Amount of items: 1
Items: 
Size: 675369 Color: 4

Bin 3946: 324636 of cap free
Amount of items: 1
Items: 
Size: 675365 Color: 4

Bin 3947: 324656 of cap free
Amount of items: 1
Items: 
Size: 675345 Color: 1

Bin 3948: 324664 of cap free
Amount of items: 1
Items: 
Size: 675337 Color: 1

Bin 3949: 324814 of cap free
Amount of items: 1
Items: 
Size: 675187 Color: 4

Bin 3950: 324876 of cap free
Amount of items: 1
Items: 
Size: 675125 Color: 1

Bin 3951: 324941 of cap free
Amount of items: 1
Items: 
Size: 675060 Color: 1

Bin 3952: 324985 of cap free
Amount of items: 1
Items: 
Size: 675016 Color: 0

Bin 3953: 325012 of cap free
Amount of items: 1
Items: 
Size: 674989 Color: 1

Bin 3954: 325064 of cap free
Amount of items: 1
Items: 
Size: 674937 Color: 3

Bin 3955: 325230 of cap free
Amount of items: 1
Items: 
Size: 674771 Color: 1

Bin 3956: 325309 of cap free
Amount of items: 1
Items: 
Size: 674692 Color: 4

Bin 3957: 325318 of cap free
Amount of items: 1
Items: 
Size: 674683 Color: 3

Bin 3958: 325446 of cap free
Amount of items: 1
Items: 
Size: 674555 Color: 0

Bin 3959: 325486 of cap free
Amount of items: 1
Items: 
Size: 674515 Color: 0

Bin 3960: 325497 of cap free
Amount of items: 1
Items: 
Size: 674504 Color: 1

Bin 3961: 325520 of cap free
Amount of items: 1
Items: 
Size: 674481 Color: 0

Bin 3962: 325617 of cap free
Amount of items: 1
Items: 
Size: 674384 Color: 3

Bin 3963: 325732 of cap free
Amount of items: 1
Items: 
Size: 674269 Color: 1

Bin 3964: 325738 of cap free
Amount of items: 1
Items: 
Size: 674263 Color: 1

Bin 3965: 325798 of cap free
Amount of items: 1
Items: 
Size: 674203 Color: 4

Bin 3966: 325872 of cap free
Amount of items: 1
Items: 
Size: 674129 Color: 1

Bin 3967: 325941 of cap free
Amount of items: 1
Items: 
Size: 674060 Color: 3

Bin 3968: 326036 of cap free
Amount of items: 1
Items: 
Size: 673965 Color: 4

Bin 3969: 326130 of cap free
Amount of items: 1
Items: 
Size: 673871 Color: 3

Bin 3970: 326208 of cap free
Amount of items: 1
Items: 
Size: 673793 Color: 2

Bin 3971: 326231 of cap free
Amount of items: 1
Items: 
Size: 673770 Color: 2

Bin 3972: 326329 of cap free
Amount of items: 1
Items: 
Size: 673672 Color: 1

Bin 3973: 326335 of cap free
Amount of items: 1
Items: 
Size: 673666 Color: 3

Bin 3974: 326485 of cap free
Amount of items: 1
Items: 
Size: 673516 Color: 0

Bin 3975: 326496 of cap free
Amount of items: 1
Items: 
Size: 673505 Color: 1

Bin 3976: 326598 of cap free
Amount of items: 1
Items: 
Size: 673403 Color: 3

Bin 3977: 326648 of cap free
Amount of items: 1
Items: 
Size: 673353 Color: 3

Bin 3978: 326707 of cap free
Amount of items: 1
Items: 
Size: 673294 Color: 0

Bin 3979: 326734 of cap free
Amount of items: 1
Items: 
Size: 673267 Color: 1

Bin 3980: 326761 of cap free
Amount of items: 1
Items: 
Size: 673240 Color: 3

Bin 3981: 326774 of cap free
Amount of items: 1
Items: 
Size: 673227 Color: 2

Bin 3982: 326803 of cap free
Amount of items: 1
Items: 
Size: 673198 Color: 0

Bin 3983: 326857 of cap free
Amount of items: 1
Items: 
Size: 673144 Color: 0

Bin 3984: 326938 of cap free
Amount of items: 1
Items: 
Size: 673063 Color: 2

Bin 3985: 327138 of cap free
Amount of items: 1
Items: 
Size: 672863 Color: 0

Bin 3986: 327167 of cap free
Amount of items: 1
Items: 
Size: 672834 Color: 2

Bin 3987: 327233 of cap free
Amount of items: 1
Items: 
Size: 672768 Color: 3

Bin 3988: 327328 of cap free
Amount of items: 1
Items: 
Size: 672673 Color: 4

Bin 3989: 327377 of cap free
Amount of items: 1
Items: 
Size: 672624 Color: 1

Bin 3990: 327396 of cap free
Amount of items: 1
Items: 
Size: 672605 Color: 0

Bin 3991: 327457 of cap free
Amount of items: 1
Items: 
Size: 672544 Color: 1

Bin 3992: 327511 of cap free
Amount of items: 1
Items: 
Size: 672490 Color: 3

Bin 3993: 327548 of cap free
Amount of items: 1
Items: 
Size: 672453 Color: 3

Bin 3994: 327604 of cap free
Amount of items: 1
Items: 
Size: 672397 Color: 3

Bin 3995: 327648 of cap free
Amount of items: 1
Items: 
Size: 672353 Color: 3

Bin 3996: 327774 of cap free
Amount of items: 1
Items: 
Size: 672227 Color: 1

Bin 3997: 327781 of cap free
Amount of items: 1
Items: 
Size: 672220 Color: 3

Bin 3998: 327889 of cap free
Amount of items: 1
Items: 
Size: 672112 Color: 3

Bin 3999: 327895 of cap free
Amount of items: 1
Items: 
Size: 672106 Color: 2

Bin 4000: 327907 of cap free
Amount of items: 1
Items: 
Size: 672094 Color: 3

Bin 4001: 327957 of cap free
Amount of items: 1
Items: 
Size: 672044 Color: 1

Bin 4002: 328260 of cap free
Amount of items: 1
Items: 
Size: 671741 Color: 3

Bin 4003: 328289 of cap free
Amount of items: 1
Items: 
Size: 671712 Color: 4

Bin 4004: 328349 of cap free
Amount of items: 1
Items: 
Size: 671652 Color: 2

Bin 4005: 328409 of cap free
Amount of items: 1
Items: 
Size: 671592 Color: 4

Bin 4006: 328461 of cap free
Amount of items: 1
Items: 
Size: 671540 Color: 4

Bin 4007: 328487 of cap free
Amount of items: 1
Items: 
Size: 671514 Color: 4

Bin 4008: 328546 of cap free
Amount of items: 1
Items: 
Size: 671455 Color: 3

Bin 4009: 328604 of cap free
Amount of items: 1
Items: 
Size: 671397 Color: 2

Bin 4010: 328606 of cap free
Amount of items: 1
Items: 
Size: 671395 Color: 3

Bin 4011: 328641 of cap free
Amount of items: 1
Items: 
Size: 671360 Color: 1

Bin 4012: 328651 of cap free
Amount of items: 1
Items: 
Size: 671350 Color: 2

Bin 4013: 328806 of cap free
Amount of items: 1
Items: 
Size: 671195 Color: 2

Bin 4014: 328889 of cap free
Amount of items: 1
Items: 
Size: 671112 Color: 2

Bin 4015: 328982 of cap free
Amount of items: 1
Items: 
Size: 671019 Color: 1

Bin 4016: 329057 of cap free
Amount of items: 1
Items: 
Size: 670944 Color: 1

Bin 4017: 329075 of cap free
Amount of items: 1
Items: 
Size: 670926 Color: 2

Bin 4018: 329097 of cap free
Amount of items: 1
Items: 
Size: 670904 Color: 3

Bin 4019: 329149 of cap free
Amount of items: 1
Items: 
Size: 670852 Color: 1

Bin 4020: 329194 of cap free
Amount of items: 1
Items: 
Size: 670807 Color: 3

Bin 4021: 329234 of cap free
Amount of items: 1
Items: 
Size: 670767 Color: 2

Bin 4022: 329242 of cap free
Amount of items: 1
Items: 
Size: 670759 Color: 1

Bin 4023: 329385 of cap free
Amount of items: 1
Items: 
Size: 670616 Color: 4

Bin 4024: 329435 of cap free
Amount of items: 1
Items: 
Size: 670566 Color: 2

Bin 4025: 329555 of cap free
Amount of items: 1
Items: 
Size: 670446 Color: 0

Bin 4026: 329621 of cap free
Amount of items: 1
Items: 
Size: 670380 Color: 3

Bin 4027: 329810 of cap free
Amount of items: 1
Items: 
Size: 670191 Color: 1

Bin 4028: 330082 of cap free
Amount of items: 1
Items: 
Size: 669919 Color: 0

Bin 4029: 330205 of cap free
Amount of items: 1
Items: 
Size: 669796 Color: 0

Bin 4030: 330205 of cap free
Amount of items: 1
Items: 
Size: 669796 Color: 0

Bin 4031: 330397 of cap free
Amount of items: 1
Items: 
Size: 669604 Color: 2

Bin 4032: 330449 of cap free
Amount of items: 1
Items: 
Size: 669552 Color: 3

Bin 4033: 330457 of cap free
Amount of items: 1
Items: 
Size: 669544 Color: 2

Bin 4034: 330672 of cap free
Amount of items: 1
Items: 
Size: 669329 Color: 2

Bin 4035: 330709 of cap free
Amount of items: 1
Items: 
Size: 669292 Color: 3

Bin 4036: 330766 of cap free
Amount of items: 1
Items: 
Size: 669235 Color: 3

Bin 4037: 330772 of cap free
Amount of items: 1
Items: 
Size: 669229 Color: 2

Bin 4038: 330789 of cap free
Amount of items: 1
Items: 
Size: 669212 Color: 0

Bin 4039: 330819 of cap free
Amount of items: 1
Items: 
Size: 669182 Color: 0

Bin 4040: 330829 of cap free
Amount of items: 1
Items: 
Size: 669172 Color: 0

Bin 4041: 330903 of cap free
Amount of items: 1
Items: 
Size: 669098 Color: 0

Bin 4042: 330914 of cap free
Amount of items: 1
Items: 
Size: 669087 Color: 3

Bin 4043: 330929 of cap free
Amount of items: 1
Items: 
Size: 669072 Color: 4

Bin 4044: 331032 of cap free
Amount of items: 1
Items: 
Size: 668969 Color: 3

Bin 4045: 331103 of cap free
Amount of items: 1
Items: 
Size: 668898 Color: 1

Bin 4046: 331166 of cap free
Amount of items: 1
Items: 
Size: 668835 Color: 0

Bin 4047: 331210 of cap free
Amount of items: 1
Items: 
Size: 668791 Color: 1

Bin 4048: 331314 of cap free
Amount of items: 1
Items: 
Size: 668687 Color: 2

Bin 4049: 331323 of cap free
Amount of items: 1
Items: 
Size: 668678 Color: 2

Bin 4050: 331391 of cap free
Amount of items: 1
Items: 
Size: 668610 Color: 1

Bin 4051: 331408 of cap free
Amount of items: 1
Items: 
Size: 668593 Color: 4

Bin 4052: 331578 of cap free
Amount of items: 1
Items: 
Size: 668423 Color: 2

Bin 4053: 331580 of cap free
Amount of items: 1
Items: 
Size: 668421 Color: 3

Bin 4054: 331589 of cap free
Amount of items: 1
Items: 
Size: 668412 Color: 0

Bin 4055: 331636 of cap free
Amount of items: 1
Items: 
Size: 668365 Color: 2

Bin 4056: 331637 of cap free
Amount of items: 1
Items: 
Size: 668364 Color: 4

Bin 4057: 331690 of cap free
Amount of items: 1
Items: 
Size: 668311 Color: 1

Bin 4058: 331727 of cap free
Amount of items: 1
Items: 
Size: 668274 Color: 2

Bin 4059: 331739 of cap free
Amount of items: 1
Items: 
Size: 668262 Color: 0

Bin 4060: 331933 of cap free
Amount of items: 1
Items: 
Size: 668068 Color: 2

Bin 4061: 331958 of cap free
Amount of items: 1
Items: 
Size: 668043 Color: 0

Bin 4062: 332074 of cap free
Amount of items: 1
Items: 
Size: 667927 Color: 0

Bin 4063: 332217 of cap free
Amount of items: 1
Items: 
Size: 667784 Color: 2

Bin 4064: 332246 of cap free
Amount of items: 1
Items: 
Size: 667755 Color: 4

Bin 4065: 332344 of cap free
Amount of items: 1
Items: 
Size: 667657 Color: 1

Bin 4066: 332373 of cap free
Amount of items: 1
Items: 
Size: 667628 Color: 0

Bin 4067: 332486 of cap free
Amount of items: 1
Items: 
Size: 667515 Color: 4

Bin 4068: 332566 of cap free
Amount of items: 1
Items: 
Size: 667435 Color: 2

Bin 4069: 332664 of cap free
Amount of items: 1
Items: 
Size: 667337 Color: 3

Bin 4070: 332686 of cap free
Amount of items: 1
Items: 
Size: 667315 Color: 2

Bin 4071: 332736 of cap free
Amount of items: 1
Items: 
Size: 667265 Color: 2

Bin 4072: 332758 of cap free
Amount of items: 1
Items: 
Size: 667243 Color: 3

Bin 4073: 332977 of cap free
Amount of items: 1
Items: 
Size: 667024 Color: 2

Bin 4074: 333170 of cap free
Amount of items: 1
Items: 
Size: 666831 Color: 0

Bin 4075: 333171 of cap free
Amount of items: 1
Items: 
Size: 666830 Color: 4

Bin 4076: 333222 of cap free
Amount of items: 1
Items: 
Size: 666779 Color: 0

Bin 4077: 333348 of cap free
Amount of items: 1
Items: 
Size: 666653 Color: 2

Bin 4078: 333348 of cap free
Amount of items: 1
Items: 
Size: 666653 Color: 4

Bin 4079: 333425 of cap free
Amount of items: 1
Items: 
Size: 666576 Color: 4

Bin 4080: 333454 of cap free
Amount of items: 1
Items: 
Size: 666547 Color: 1

Bin 4081: 333487 of cap free
Amount of items: 1
Items: 
Size: 666514 Color: 2

Bin 4082: 333487 of cap free
Amount of items: 1
Items: 
Size: 666514 Color: 4

Bin 4083: 333520 of cap free
Amount of items: 1
Items: 
Size: 666481 Color: 2

Bin 4084: 333591 of cap free
Amount of items: 1
Items: 
Size: 666410 Color: 3

Bin 4085: 333608 of cap free
Amount of items: 1
Items: 
Size: 666393 Color: 1

Bin 4086: 333708 of cap free
Amount of items: 1
Items: 
Size: 666293 Color: 3

Bin 4087: 333725 of cap free
Amount of items: 1
Items: 
Size: 666276 Color: 2

Bin 4088: 333822 of cap free
Amount of items: 1
Items: 
Size: 666179 Color: 0

Bin 4089: 333877 of cap free
Amount of items: 1
Items: 
Size: 666124 Color: 0

Bin 4090: 333880 of cap free
Amount of items: 1
Items: 
Size: 666121 Color: 3

Bin 4091: 334009 of cap free
Amount of items: 1
Items: 
Size: 665992 Color: 3

Bin 4092: 334036 of cap free
Amount of items: 1
Items: 
Size: 665965 Color: 1

Bin 4093: 334087 of cap free
Amount of items: 1
Items: 
Size: 665914 Color: 0

Bin 4094: 334088 of cap free
Amount of items: 1
Items: 
Size: 665913 Color: 2

Bin 4095: 334118 of cap free
Amount of items: 1
Items: 
Size: 665883 Color: 2

Bin 4096: 334124 of cap free
Amount of items: 1
Items: 
Size: 665877 Color: 1

Bin 4097: 334128 of cap free
Amount of items: 1
Items: 
Size: 665873 Color: 0

Bin 4098: 334137 of cap free
Amount of items: 1
Items: 
Size: 665864 Color: 3

Bin 4099: 334180 of cap free
Amount of items: 1
Items: 
Size: 665821 Color: 3

Bin 4100: 334315 of cap free
Amount of items: 1
Items: 
Size: 665686 Color: 2

Bin 4101: 334424 of cap free
Amount of items: 1
Items: 
Size: 665577 Color: 2

Bin 4102: 334504 of cap free
Amount of items: 1
Items: 
Size: 665497 Color: 4

Bin 4103: 334560 of cap free
Amount of items: 1
Items: 
Size: 665441 Color: 4

Bin 4104: 334608 of cap free
Amount of items: 1
Items: 
Size: 665393 Color: 3

Bin 4105: 334627 of cap free
Amount of items: 1
Items: 
Size: 665374 Color: 3

Bin 4106: 334635 of cap free
Amount of items: 1
Items: 
Size: 665366 Color: 3

Bin 4107: 334660 of cap free
Amount of items: 1
Items: 
Size: 665341 Color: 1

Bin 4108: 334710 of cap free
Amount of items: 1
Items: 
Size: 665291 Color: 0

Bin 4109: 334816 of cap free
Amount of items: 1
Items: 
Size: 665185 Color: 3

Bin 4110: 334852 of cap free
Amount of items: 1
Items: 
Size: 665149 Color: 3

Bin 4111: 334919 of cap free
Amount of items: 1
Items: 
Size: 665082 Color: 0

Bin 4112: 334997 of cap free
Amount of items: 1
Items: 
Size: 665004 Color: 0

Bin 4113: 335002 of cap free
Amount of items: 1
Items: 
Size: 664999 Color: 2

Bin 4114: 335020 of cap free
Amount of items: 1
Items: 
Size: 664981 Color: 3

Bin 4115: 335076 of cap free
Amount of items: 1
Items: 
Size: 664925 Color: 4

Bin 4116: 335247 of cap free
Amount of items: 1
Items: 
Size: 664754 Color: 4

Bin 4117: 335427 of cap free
Amount of items: 1
Items: 
Size: 664574 Color: 0

Bin 4118: 335446 of cap free
Amount of items: 1
Items: 
Size: 664555 Color: 3

Bin 4119: 335554 of cap free
Amount of items: 1
Items: 
Size: 664447 Color: 3

Bin 4120: 335665 of cap free
Amount of items: 1
Items: 
Size: 664336 Color: 0

Bin 4121: 335739 of cap free
Amount of items: 1
Items: 
Size: 664262 Color: 4

Bin 4122: 335861 of cap free
Amount of items: 1
Items: 
Size: 664140 Color: 0

Bin 4123: 335905 of cap free
Amount of items: 1
Items: 
Size: 664096 Color: 4

Bin 4124: 335916 of cap free
Amount of items: 1
Items: 
Size: 664085 Color: 2

Bin 4125: 335931 of cap free
Amount of items: 1
Items: 
Size: 664070 Color: 1

Bin 4126: 335932 of cap free
Amount of items: 1
Items: 
Size: 664069 Color: 2

Bin 4127: 335941 of cap free
Amount of items: 1
Items: 
Size: 664060 Color: 0

Bin 4128: 336004 of cap free
Amount of items: 1
Items: 
Size: 663997 Color: 2

Bin 4129: 336055 of cap free
Amount of items: 1
Items: 
Size: 663946 Color: 0

Bin 4130: 336078 of cap free
Amount of items: 1
Items: 
Size: 663923 Color: 0

Bin 4131: 336119 of cap free
Amount of items: 1
Items: 
Size: 663882 Color: 1

Bin 4132: 336410 of cap free
Amount of items: 1
Items: 
Size: 663591 Color: 2

Bin 4133: 336465 of cap free
Amount of items: 1
Items: 
Size: 663536 Color: 0

Bin 4134: 336498 of cap free
Amount of items: 1
Items: 
Size: 663503 Color: 2

Bin 4135: 336577 of cap free
Amount of items: 1
Items: 
Size: 663424 Color: 0

Bin 4136: 336689 of cap free
Amount of items: 1
Items: 
Size: 663312 Color: 1

Bin 4137: 336736 of cap free
Amount of items: 1
Items: 
Size: 663265 Color: 2

Bin 4138: 336839 of cap free
Amount of items: 1
Items: 
Size: 663162 Color: 3

Bin 4139: 336846 of cap free
Amount of items: 1
Items: 
Size: 663155 Color: 3

Bin 4140: 336961 of cap free
Amount of items: 1
Items: 
Size: 663040 Color: 4

Bin 4141: 336977 of cap free
Amount of items: 1
Items: 
Size: 663024 Color: 1

Bin 4142: 337010 of cap free
Amount of items: 1
Items: 
Size: 662991 Color: 0

Bin 4143: 337354 of cap free
Amount of items: 1
Items: 
Size: 662647 Color: 1

Bin 4144: 337452 of cap free
Amount of items: 1
Items: 
Size: 662549 Color: 4

Bin 4145: 337502 of cap free
Amount of items: 1
Items: 
Size: 662499 Color: 4

Bin 4146: 337516 of cap free
Amount of items: 1
Items: 
Size: 662485 Color: 1

Bin 4147: 337545 of cap free
Amount of items: 1
Items: 
Size: 662456 Color: 4

Bin 4148: 337566 of cap free
Amount of items: 1
Items: 
Size: 662435 Color: 2

Bin 4149: 337606 of cap free
Amount of items: 1
Items: 
Size: 662395 Color: 1

Bin 4150: 337690 of cap free
Amount of items: 1
Items: 
Size: 662311 Color: 3

Bin 4151: 337768 of cap free
Amount of items: 1
Items: 
Size: 662233 Color: 0

Bin 4152: 337793 of cap free
Amount of items: 1
Items: 
Size: 662208 Color: 2

Bin 4153: 337794 of cap free
Amount of items: 1
Items: 
Size: 662207 Color: 2

Bin 4154: 337798 of cap free
Amount of items: 1
Items: 
Size: 662203 Color: 1

Bin 4155: 337827 of cap free
Amount of items: 1
Items: 
Size: 662174 Color: 2

Bin 4156: 337899 of cap free
Amount of items: 1
Items: 
Size: 662102 Color: 2

Bin 4157: 337904 of cap free
Amount of items: 1
Items: 
Size: 662097 Color: 3

Bin 4158: 337915 of cap free
Amount of items: 1
Items: 
Size: 662086 Color: 1

Bin 4159: 337920 of cap free
Amount of items: 1
Items: 
Size: 662081 Color: 4

Bin 4160: 337942 of cap free
Amount of items: 1
Items: 
Size: 662059 Color: 4

Bin 4161: 338022 of cap free
Amount of items: 1
Items: 
Size: 661979 Color: 2

Bin 4162: 338036 of cap free
Amount of items: 1
Items: 
Size: 661965 Color: 3

Bin 4163: 338247 of cap free
Amount of items: 1
Items: 
Size: 661754 Color: 3

Bin 4164: 338388 of cap free
Amount of items: 1
Items: 
Size: 661613 Color: 3

Bin 4165: 338508 of cap free
Amount of items: 1
Items: 
Size: 661493 Color: 2

Bin 4166: 338510 of cap free
Amount of items: 1
Items: 
Size: 661491 Color: 1

Bin 4167: 338531 of cap free
Amount of items: 1
Items: 
Size: 661470 Color: 4

Bin 4168: 338639 of cap free
Amount of items: 1
Items: 
Size: 661362 Color: 1

Bin 4169: 338669 of cap free
Amount of items: 1
Items: 
Size: 661332 Color: 2

Bin 4170: 338686 of cap free
Amount of items: 1
Items: 
Size: 661315 Color: 2

Bin 4171: 338698 of cap free
Amount of items: 1
Items: 
Size: 661303 Color: 3

Bin 4172: 338763 of cap free
Amount of items: 1
Items: 
Size: 661238 Color: 1

Bin 4173: 338803 of cap free
Amount of items: 1
Items: 
Size: 661198 Color: 0

Bin 4174: 338893 of cap free
Amount of items: 1
Items: 
Size: 661108 Color: 3

Bin 4175: 339065 of cap free
Amount of items: 1
Items: 
Size: 660936 Color: 1

Bin 4176: 339176 of cap free
Amount of items: 1
Items: 
Size: 660825 Color: 3

Bin 4177: 339202 of cap free
Amount of items: 1
Items: 
Size: 660799 Color: 1

Bin 4178: 339212 of cap free
Amount of items: 1
Items: 
Size: 660789 Color: 1

Bin 4179: 339294 of cap free
Amount of items: 1
Items: 
Size: 660707 Color: 1

Bin 4180: 339386 of cap free
Amount of items: 1
Items: 
Size: 660615 Color: 4

Bin 4181: 339410 of cap free
Amount of items: 1
Items: 
Size: 660591 Color: 0

Bin 4182: 339509 of cap free
Amount of items: 1
Items: 
Size: 660492 Color: 4

Bin 4183: 339519 of cap free
Amount of items: 1
Items: 
Size: 660482 Color: 0

Bin 4184: 339537 of cap free
Amount of items: 1
Items: 
Size: 660464 Color: 1

Bin 4185: 339556 of cap free
Amount of items: 1
Items: 
Size: 660445 Color: 4

Bin 4186: 339566 of cap free
Amount of items: 1
Items: 
Size: 660435 Color: 3

Bin 4187: 339585 of cap free
Amount of items: 1
Items: 
Size: 660416 Color: 3

Bin 4188: 339623 of cap free
Amount of items: 1
Items: 
Size: 660378 Color: 4

Bin 4189: 339747 of cap free
Amount of items: 1
Items: 
Size: 660254 Color: 2

Bin 4190: 339783 of cap free
Amount of items: 1
Items: 
Size: 660218 Color: 1

Bin 4191: 339794 of cap free
Amount of items: 1
Items: 
Size: 660207 Color: 3

Bin 4192: 339889 of cap free
Amount of items: 1
Items: 
Size: 660112 Color: 0

Bin 4193: 340018 of cap free
Amount of items: 1
Items: 
Size: 659983 Color: 4

Bin 4194: 340113 of cap free
Amount of items: 1
Items: 
Size: 659888 Color: 2

Bin 4195: 340154 of cap free
Amount of items: 1
Items: 
Size: 659847 Color: 4

Bin 4196: 340249 of cap free
Amount of items: 1
Items: 
Size: 659752 Color: 4

Bin 4197: 340282 of cap free
Amount of items: 1
Items: 
Size: 659719 Color: 2

Bin 4198: 340403 of cap free
Amount of items: 1
Items: 
Size: 659598 Color: 1

Bin 4199: 340421 of cap free
Amount of items: 1
Items: 
Size: 659580 Color: 2

Bin 4200: 340428 of cap free
Amount of items: 1
Items: 
Size: 659573 Color: 4

Bin 4201: 340551 of cap free
Amount of items: 1
Items: 
Size: 659450 Color: 2

Bin 4202: 340641 of cap free
Amount of items: 1
Items: 
Size: 659360 Color: 3

Bin 4203: 340724 of cap free
Amount of items: 1
Items: 
Size: 659277 Color: 0

Bin 4204: 340879 of cap free
Amount of items: 1
Items: 
Size: 659122 Color: 0

Bin 4205: 340971 of cap free
Amount of items: 1
Items: 
Size: 659030 Color: 2

Bin 4206: 341061 of cap free
Amount of items: 1
Items: 
Size: 658940 Color: 1

Bin 4207: 341120 of cap free
Amount of items: 1
Items: 
Size: 658881 Color: 3

Bin 4208: 341132 of cap free
Amount of items: 1
Items: 
Size: 658869 Color: 4

Bin 4209: 341162 of cap free
Amount of items: 1
Items: 
Size: 658839 Color: 4

Bin 4210: 341185 of cap free
Amount of items: 1
Items: 
Size: 658816 Color: 3

Bin 4211: 341252 of cap free
Amount of items: 1
Items: 
Size: 658749 Color: 4

Bin 4212: 341355 of cap free
Amount of items: 1
Items: 
Size: 658646 Color: 3

Bin 4213: 341419 of cap free
Amount of items: 1
Items: 
Size: 658582 Color: 4

Bin 4214: 341989 of cap free
Amount of items: 1
Items: 
Size: 658012 Color: 3

Bin 4215: 342181 of cap free
Amount of items: 1
Items: 
Size: 657820 Color: 2

Bin 4216: 342320 of cap free
Amount of items: 1
Items: 
Size: 657681 Color: 3

Bin 4217: 342354 of cap free
Amount of items: 1
Items: 
Size: 657647 Color: 2

Bin 4218: 342387 of cap free
Amount of items: 1
Items: 
Size: 657614 Color: 2

Bin 4219: 342405 of cap free
Amount of items: 1
Items: 
Size: 657596 Color: 1

Bin 4220: 342517 of cap free
Amount of items: 1
Items: 
Size: 657484 Color: 3

Bin 4221: 342554 of cap free
Amount of items: 1
Items: 
Size: 657447 Color: 4

Bin 4222: 342624 of cap free
Amount of items: 1
Items: 
Size: 657377 Color: 2

Bin 4223: 342686 of cap free
Amount of items: 1
Items: 
Size: 657315 Color: 1

Bin 4224: 342812 of cap free
Amount of items: 1
Items: 
Size: 657189 Color: 2

Bin 4225: 342931 of cap free
Amount of items: 1
Items: 
Size: 657070 Color: 3

Bin 4226: 342964 of cap free
Amount of items: 1
Items: 
Size: 657037 Color: 2

Bin 4227: 342983 of cap free
Amount of items: 1
Items: 
Size: 657018 Color: 1

Bin 4228: 342999 of cap free
Amount of items: 1
Items: 
Size: 657002 Color: 3

Bin 4229: 343018 of cap free
Amount of items: 1
Items: 
Size: 656983 Color: 0

Bin 4230: 343120 of cap free
Amount of items: 1
Items: 
Size: 656881 Color: 0

Bin 4231: 343188 of cap free
Amount of items: 1
Items: 
Size: 656813 Color: 0

Bin 4232: 343198 of cap free
Amount of items: 1
Items: 
Size: 656803 Color: 4

Bin 4233: 343358 of cap free
Amount of items: 1
Items: 
Size: 656643 Color: 1

Bin 4234: 343392 of cap free
Amount of items: 1
Items: 
Size: 656609 Color: 3

Bin 4235: 343522 of cap free
Amount of items: 1
Items: 
Size: 656479 Color: 2

Bin 4236: 343571 of cap free
Amount of items: 1
Items: 
Size: 656430 Color: 0

Bin 4237: 343639 of cap free
Amount of items: 1
Items: 
Size: 656362 Color: 3

Bin 4238: 343739 of cap free
Amount of items: 1
Items: 
Size: 656262 Color: 2

Bin 4239: 343743 of cap free
Amount of items: 1
Items: 
Size: 656258 Color: 1

Bin 4240: 343771 of cap free
Amount of items: 1
Items: 
Size: 656230 Color: 0

Bin 4241: 343782 of cap free
Amount of items: 1
Items: 
Size: 656219 Color: 4

Bin 4242: 344016 of cap free
Amount of items: 1
Items: 
Size: 655985 Color: 3

Bin 4243: 344042 of cap free
Amount of items: 1
Items: 
Size: 655959 Color: 0

Bin 4244: 344084 of cap free
Amount of items: 1
Items: 
Size: 655917 Color: 3

Bin 4245: 344179 of cap free
Amount of items: 1
Items: 
Size: 655822 Color: 4

Bin 4246: 344301 of cap free
Amount of items: 1
Items: 
Size: 655700 Color: 2

Bin 4247: 344314 of cap free
Amount of items: 1
Items: 
Size: 655687 Color: 2

Bin 4248: 344465 of cap free
Amount of items: 1
Items: 
Size: 655536 Color: 0

Bin 4249: 344487 of cap free
Amount of items: 1
Items: 
Size: 655514 Color: 3

Bin 4250: 344488 of cap free
Amount of items: 1
Items: 
Size: 655513 Color: 3

Bin 4251: 344501 of cap free
Amount of items: 1
Items: 
Size: 655500 Color: 1

Bin 4252: 344554 of cap free
Amount of items: 1
Items: 
Size: 655447 Color: 1

Bin 4253: 344586 of cap free
Amount of items: 1
Items: 
Size: 655415 Color: 3

Bin 4254: 344668 of cap free
Amount of items: 1
Items: 
Size: 655333 Color: 1

Bin 4255: 344867 of cap free
Amount of items: 1
Items: 
Size: 655134 Color: 4

Bin 4256: 344903 of cap free
Amount of items: 1
Items: 
Size: 655098 Color: 0

Bin 4257: 344927 of cap free
Amount of items: 1
Items: 
Size: 655074 Color: 2

Bin 4258: 344951 of cap free
Amount of items: 1
Items: 
Size: 655050 Color: 3

Bin 4259: 344952 of cap free
Amount of items: 1
Items: 
Size: 655049 Color: 1

Bin 4260: 344977 of cap free
Amount of items: 1
Items: 
Size: 655024 Color: 2

Bin 4261: 345011 of cap free
Amount of items: 1
Items: 
Size: 654990 Color: 2

Bin 4262: 345024 of cap free
Amount of items: 1
Items: 
Size: 654977 Color: 0

Bin 4263: 345061 of cap free
Amount of items: 1
Items: 
Size: 654940 Color: 3

Bin 4264: 345103 of cap free
Amount of items: 1
Items: 
Size: 654898 Color: 3

Bin 4265: 345310 of cap free
Amount of items: 1
Items: 
Size: 654691 Color: 3

Bin 4266: 345328 of cap free
Amount of items: 1
Items: 
Size: 654673 Color: 4

Bin 4267: 345400 of cap free
Amount of items: 1
Items: 
Size: 654601 Color: 4

Bin 4268: 345473 of cap free
Amount of items: 1
Items: 
Size: 654528 Color: 0

Bin 4269: 345603 of cap free
Amount of items: 1
Items: 
Size: 654398 Color: 3

Bin 4270: 345634 of cap free
Amount of items: 1
Items: 
Size: 654367 Color: 2

Bin 4271: 345701 of cap free
Amount of items: 1
Items: 
Size: 654300 Color: 3

Bin 4272: 345845 of cap free
Amount of items: 1
Items: 
Size: 654156 Color: 2

Bin 4273: 346063 of cap free
Amount of items: 1
Items: 
Size: 653938 Color: 3

Bin 4274: 346078 of cap free
Amount of items: 1
Items: 
Size: 653923 Color: 3

Bin 4275: 346088 of cap free
Amount of items: 1
Items: 
Size: 653913 Color: 1

Bin 4276: 346127 of cap free
Amount of items: 1
Items: 
Size: 653874 Color: 0

Bin 4277: 346241 of cap free
Amount of items: 1
Items: 
Size: 653760 Color: 4

Bin 4278: 346297 of cap free
Amount of items: 1
Items: 
Size: 653704 Color: 3

Bin 4279: 346543 of cap free
Amount of items: 1
Items: 
Size: 653458 Color: 3

Bin 4280: 346660 of cap free
Amount of items: 1
Items: 
Size: 653341 Color: 2

Bin 4281: 347095 of cap free
Amount of items: 1
Items: 
Size: 652906 Color: 1

Bin 4282: 347106 of cap free
Amount of items: 1
Items: 
Size: 652895 Color: 4

Bin 4283: 347132 of cap free
Amount of items: 1
Items: 
Size: 652869 Color: 4

Bin 4284: 347142 of cap free
Amount of items: 1
Items: 
Size: 652859 Color: 1

Bin 4285: 347180 of cap free
Amount of items: 1
Items: 
Size: 652821 Color: 0

Bin 4286: 347301 of cap free
Amount of items: 1
Items: 
Size: 652700 Color: 3

Bin 4287: 347307 of cap free
Amount of items: 1
Items: 
Size: 652694 Color: 1

Bin 4288: 347325 of cap free
Amount of items: 1
Items: 
Size: 652676 Color: 4

Bin 4289: 347335 of cap free
Amount of items: 1
Items: 
Size: 652666 Color: 1

Bin 4290: 347418 of cap free
Amount of items: 1
Items: 
Size: 652583 Color: 4

Bin 4291: 347426 of cap free
Amount of items: 1
Items: 
Size: 652575 Color: 4

Bin 4292: 347451 of cap free
Amount of items: 1
Items: 
Size: 652550 Color: 3

Bin 4293: 347488 of cap free
Amount of items: 1
Items: 
Size: 652513 Color: 2

Bin 4294: 347566 of cap free
Amount of items: 1
Items: 
Size: 652435 Color: 1

Bin 4295: 347608 of cap free
Amount of items: 1
Items: 
Size: 652393 Color: 1

Bin 4296: 347624 of cap free
Amount of items: 1
Items: 
Size: 652377 Color: 2

Bin 4297: 347662 of cap free
Amount of items: 1
Items: 
Size: 652339 Color: 4

Bin 4298: 347732 of cap free
Amount of items: 1
Items: 
Size: 652269 Color: 1

Bin 4299: 347753 of cap free
Amount of items: 1
Items: 
Size: 652248 Color: 0

Bin 4300: 347812 of cap free
Amount of items: 1
Items: 
Size: 652189 Color: 1

Bin 4301: 347846 of cap free
Amount of items: 1
Items: 
Size: 652155 Color: 3

Bin 4302: 347853 of cap free
Amount of items: 1
Items: 
Size: 652148 Color: 4

Bin 4303: 347906 of cap free
Amount of items: 1
Items: 
Size: 652095 Color: 0

Bin 4304: 348006 of cap free
Amount of items: 1
Items: 
Size: 651995 Color: 1

Bin 4305: 348149 of cap free
Amount of items: 1
Items: 
Size: 651852 Color: 0

Bin 4306: 348200 of cap free
Amount of items: 1
Items: 
Size: 651801 Color: 4

Bin 4307: 348216 of cap free
Amount of items: 1
Items: 
Size: 651785 Color: 1

Bin 4308: 348310 of cap free
Amount of items: 1
Items: 
Size: 651691 Color: 3

Bin 4309: 348346 of cap free
Amount of items: 1
Items: 
Size: 651655 Color: 2

Bin 4310: 348563 of cap free
Amount of items: 1
Items: 
Size: 651438 Color: 2

Bin 4311: 348656 of cap free
Amount of items: 1
Items: 
Size: 651345 Color: 2

Bin 4312: 348695 of cap free
Amount of items: 1
Items: 
Size: 651306 Color: 4

Bin 4313: 348743 of cap free
Amount of items: 1
Items: 
Size: 651258 Color: 4

Bin 4314: 348858 of cap free
Amount of items: 1
Items: 
Size: 651143 Color: 0

Bin 4315: 349001 of cap free
Amount of items: 1
Items: 
Size: 651000 Color: 2

Bin 4316: 349005 of cap free
Amount of items: 1
Items: 
Size: 650996 Color: 0

Bin 4317: 349051 of cap free
Amount of items: 1
Items: 
Size: 650950 Color: 1

Bin 4318: 349222 of cap free
Amount of items: 1
Items: 
Size: 650779 Color: 0

Bin 4319: 349261 of cap free
Amount of items: 1
Items: 
Size: 650740 Color: 4

Bin 4320: 349287 of cap free
Amount of items: 1
Items: 
Size: 650714 Color: 0

Bin 4321: 349313 of cap free
Amount of items: 1
Items: 
Size: 650688 Color: 4

Bin 4322: 349398 of cap free
Amount of items: 1
Items: 
Size: 650603 Color: 4

Bin 4323: 349435 of cap free
Amount of items: 1
Items: 
Size: 650566 Color: 0

Bin 4324: 349491 of cap free
Amount of items: 1
Items: 
Size: 650510 Color: 4

Bin 4325: 349497 of cap free
Amount of items: 1
Items: 
Size: 650504 Color: 1

Bin 4326: 349565 of cap free
Amount of items: 1
Items: 
Size: 650436 Color: 4

Bin 4327: 349587 of cap free
Amount of items: 1
Items: 
Size: 650414 Color: 4

Bin 4328: 349651 of cap free
Amount of items: 1
Items: 
Size: 650350 Color: 2

Bin 4329: 349651 of cap free
Amount of items: 1
Items: 
Size: 650350 Color: 3

Bin 4330: 349679 of cap free
Amount of items: 1
Items: 
Size: 650322 Color: 0

Bin 4331: 349696 of cap free
Amount of items: 1
Items: 
Size: 650305 Color: 2

Bin 4332: 349698 of cap free
Amount of items: 1
Items: 
Size: 650303 Color: 3

Bin 4333: 349705 of cap free
Amount of items: 1
Items: 
Size: 650296 Color: 4

Bin 4334: 349736 of cap free
Amount of items: 1
Items: 
Size: 650265 Color: 3

Bin 4335: 349783 of cap free
Amount of items: 1
Items: 
Size: 650218 Color: 4

Bin 4336: 349793 of cap free
Amount of items: 1
Items: 
Size: 650208 Color: 3

Bin 4337: 349795 of cap free
Amount of items: 1
Items: 
Size: 650206 Color: 0

Bin 4338: 349810 of cap free
Amount of items: 1
Items: 
Size: 650191 Color: 3

Bin 4339: 349886 of cap free
Amount of items: 1
Items: 
Size: 650115 Color: 1

Bin 4340: 349908 of cap free
Amount of items: 1
Items: 
Size: 650093 Color: 3

Bin 4341: 349916 of cap free
Amount of items: 1
Items: 
Size: 650085 Color: 1

Bin 4342: 350024 of cap free
Amount of items: 1
Items: 
Size: 649977 Color: 2

Bin 4343: 350088 of cap free
Amount of items: 1
Items: 
Size: 649913 Color: 2

Bin 4344: 350170 of cap free
Amount of items: 1
Items: 
Size: 649831 Color: 0

Bin 4345: 350327 of cap free
Amount of items: 1
Items: 
Size: 649674 Color: 2

Bin 4346: 350347 of cap free
Amount of items: 1
Items: 
Size: 649654 Color: 4

Bin 4347: 350410 of cap free
Amount of items: 1
Items: 
Size: 649591 Color: 0

Bin 4348: 350585 of cap free
Amount of items: 1
Items: 
Size: 649416 Color: 0

Bin 4349: 350613 of cap free
Amount of items: 1
Items: 
Size: 649388 Color: 4

Bin 4350: 350764 of cap free
Amount of items: 1
Items: 
Size: 649237 Color: 1

Bin 4351: 350779 of cap free
Amount of items: 1
Items: 
Size: 649222 Color: 2

Bin 4352: 350788 of cap free
Amount of items: 1
Items: 
Size: 649213 Color: 2

Bin 4353: 350820 of cap free
Amount of items: 1
Items: 
Size: 649181 Color: 2

Bin 4354: 350918 of cap free
Amount of items: 1
Items: 
Size: 649083 Color: 2

Bin 4355: 350941 of cap free
Amount of items: 1
Items: 
Size: 649060 Color: 1

Bin 4356: 350944 of cap free
Amount of items: 1
Items: 
Size: 649057 Color: 2

Bin 4357: 351028 of cap free
Amount of items: 1
Items: 
Size: 648973 Color: 1

Bin 4358: 351242 of cap free
Amount of items: 1
Items: 
Size: 648759 Color: 4

Bin 4359: 351269 of cap free
Amount of items: 1
Items: 
Size: 648732 Color: 4

Bin 4360: 351273 of cap free
Amount of items: 1
Items: 
Size: 648728 Color: 2

Bin 4361: 351299 of cap free
Amount of items: 1
Items: 
Size: 648702 Color: 1

Bin 4362: 351324 of cap free
Amount of items: 1
Items: 
Size: 648677 Color: 1

Bin 4363: 351344 of cap free
Amount of items: 1
Items: 
Size: 648657 Color: 1

Bin 4364: 351486 of cap free
Amount of items: 1
Items: 
Size: 648515 Color: 4

Bin 4365: 351551 of cap free
Amount of items: 1
Items: 
Size: 648450 Color: 1

Bin 4366: 351600 of cap free
Amount of items: 1
Items: 
Size: 648401 Color: 3

Bin 4367: 351612 of cap free
Amount of items: 1
Items: 
Size: 648389 Color: 1

Bin 4368: 351632 of cap free
Amount of items: 1
Items: 
Size: 648369 Color: 2

Bin 4369: 351658 of cap free
Amount of items: 1
Items: 
Size: 648343 Color: 4

Bin 4370: 351723 of cap free
Amount of items: 1
Items: 
Size: 648278 Color: 0

Bin 4371: 351827 of cap free
Amount of items: 1
Items: 
Size: 648174 Color: 1

Bin 4372: 351983 of cap free
Amount of items: 1
Items: 
Size: 648018 Color: 2

Bin 4373: 352252 of cap free
Amount of items: 1
Items: 
Size: 647749 Color: 2

Bin 4374: 352410 of cap free
Amount of items: 1
Items: 
Size: 647591 Color: 1

Bin 4375: 352458 of cap free
Amount of items: 1
Items: 
Size: 647543 Color: 0

Bin 4376: 352469 of cap free
Amount of items: 1
Items: 
Size: 647532 Color: 4

Bin 4377: 352499 of cap free
Amount of items: 1
Items: 
Size: 647502 Color: 3

Bin 4378: 352650 of cap free
Amount of items: 1
Items: 
Size: 647351 Color: 3

Bin 4379: 352653 of cap free
Amount of items: 1
Items: 
Size: 647348 Color: 4

Bin 4380: 352876 of cap free
Amount of items: 1
Items: 
Size: 647125 Color: 4

Bin 4381: 352916 of cap free
Amount of items: 1
Items: 
Size: 647085 Color: 0

Bin 4382: 352986 of cap free
Amount of items: 1
Items: 
Size: 647015 Color: 1

Bin 4383: 353039 of cap free
Amount of items: 1
Items: 
Size: 646962 Color: 4

Bin 4384: 353164 of cap free
Amount of items: 1
Items: 
Size: 646837 Color: 1

Bin 4385: 353228 of cap free
Amount of items: 1
Items: 
Size: 646773 Color: 0

Bin 4386: 353447 of cap free
Amount of items: 1
Items: 
Size: 646554 Color: 1

Bin 4387: 353470 of cap free
Amount of items: 1
Items: 
Size: 646531 Color: 2

Bin 4388: 353508 of cap free
Amount of items: 1
Items: 
Size: 646493 Color: 0

Bin 4389: 353553 of cap free
Amount of items: 1
Items: 
Size: 646448 Color: 1

Bin 4390: 353670 of cap free
Amount of items: 1
Items: 
Size: 646331 Color: 3

Bin 4391: 353694 of cap free
Amount of items: 1
Items: 
Size: 646307 Color: 2

Bin 4392: 353714 of cap free
Amount of items: 1
Items: 
Size: 646287 Color: 3

Bin 4393: 353870 of cap free
Amount of items: 1
Items: 
Size: 646131 Color: 0

Bin 4394: 353942 of cap free
Amount of items: 1
Items: 
Size: 646059 Color: 1

Bin 4395: 353993 of cap free
Amount of items: 1
Items: 
Size: 646008 Color: 4

Bin 4396: 354013 of cap free
Amount of items: 1
Items: 
Size: 645988 Color: 1

Bin 4397: 354088 of cap free
Amount of items: 1
Items: 
Size: 645913 Color: 0

Bin 4398: 354154 of cap free
Amount of items: 1
Items: 
Size: 645847 Color: 4

Bin 4399: 354241 of cap free
Amount of items: 1
Items: 
Size: 645760 Color: 2

Bin 4400: 354291 of cap free
Amount of items: 1
Items: 
Size: 645710 Color: 3

Bin 4401: 354296 of cap free
Amount of items: 1
Items: 
Size: 645705 Color: 0

Bin 4402: 354349 of cap free
Amount of items: 1
Items: 
Size: 645652 Color: 1

Bin 4403: 354519 of cap free
Amount of items: 1
Items: 
Size: 645482 Color: 4

Bin 4404: 354576 of cap free
Amount of items: 1
Items: 
Size: 645425 Color: 1

Bin 4405: 354658 of cap free
Amount of items: 1
Items: 
Size: 645343 Color: 4

Bin 4406: 354768 of cap free
Amount of items: 1
Items: 
Size: 645233 Color: 4

Bin 4407: 354794 of cap free
Amount of items: 1
Items: 
Size: 645207 Color: 1

Bin 4408: 354807 of cap free
Amount of items: 1
Items: 
Size: 645194 Color: 1

Bin 4409: 354826 of cap free
Amount of items: 1
Items: 
Size: 645175 Color: 3

Bin 4410: 354895 of cap free
Amount of items: 1
Items: 
Size: 645106 Color: 1

Bin 4411: 354990 of cap free
Amount of items: 1
Items: 
Size: 645011 Color: 4

Bin 4412: 355010 of cap free
Amount of items: 1
Items: 
Size: 644991 Color: 3

Bin 4413: 355218 of cap free
Amount of items: 1
Items: 
Size: 644783 Color: 4

Bin 4414: 355262 of cap free
Amount of items: 1
Items: 
Size: 644739 Color: 1

Bin 4415: 355286 of cap free
Amount of items: 1
Items: 
Size: 644715 Color: 1

Bin 4416: 355322 of cap free
Amount of items: 1
Items: 
Size: 644679 Color: 3

Bin 4417: 355380 of cap free
Amount of items: 1
Items: 
Size: 644621 Color: 2

Bin 4418: 355405 of cap free
Amount of items: 1
Items: 
Size: 644596 Color: 0

Bin 4419: 355508 of cap free
Amount of items: 1
Items: 
Size: 644493 Color: 2

Bin 4420: 355651 of cap free
Amount of items: 1
Items: 
Size: 644350 Color: 0

Bin 4421: 355704 of cap free
Amount of items: 1
Items: 
Size: 644297 Color: 1

Bin 4422: 355735 of cap free
Amount of items: 1
Items: 
Size: 644266 Color: 1

Bin 4423: 355807 of cap free
Amount of items: 1
Items: 
Size: 644194 Color: 2

Bin 4424: 356051 of cap free
Amount of items: 1
Items: 
Size: 643950 Color: 0

Bin 4425: 356188 of cap free
Amount of items: 1
Items: 
Size: 643813 Color: 2

Bin 4426: 356190 of cap free
Amount of items: 1
Items: 
Size: 643811 Color: 0

Bin 4427: 356228 of cap free
Amount of items: 1
Items: 
Size: 643773 Color: 4

Bin 4428: 356243 of cap free
Amount of items: 1
Items: 
Size: 643758 Color: 4

Bin 4429: 356426 of cap free
Amount of items: 1
Items: 
Size: 643575 Color: 4

Bin 4430: 356481 of cap free
Amount of items: 1
Items: 
Size: 643520 Color: 0

Bin 4431: 356646 of cap free
Amount of items: 1
Items: 
Size: 643355 Color: 2

Bin 4432: 356830 of cap free
Amount of items: 1
Items: 
Size: 643171 Color: 0

Bin 4433: 356868 of cap free
Amount of items: 1
Items: 
Size: 643133 Color: 1

Bin 4434: 356884 of cap free
Amount of items: 1
Items: 
Size: 643117 Color: 0

Bin 4435: 356945 of cap free
Amount of items: 1
Items: 
Size: 643056 Color: 1

Bin 4436: 357194 of cap free
Amount of items: 1
Items: 
Size: 642807 Color: 3

Bin 4437: 357307 of cap free
Amount of items: 1
Items: 
Size: 642694 Color: 2

Bin 4438: 357342 of cap free
Amount of items: 1
Items: 
Size: 642659 Color: 4

Bin 4439: 357419 of cap free
Amount of items: 1
Items: 
Size: 642582 Color: 0

Bin 4440: 357467 of cap free
Amount of items: 1
Items: 
Size: 642534 Color: 2

Bin 4441: 357530 of cap free
Amount of items: 1
Items: 
Size: 642471 Color: 1

Bin 4442: 357758 of cap free
Amount of items: 1
Items: 
Size: 642243 Color: 2

Bin 4443: 357764 of cap free
Amount of items: 1
Items: 
Size: 642237 Color: 1

Bin 4444: 357784 of cap free
Amount of items: 1
Items: 
Size: 642217 Color: 1

Bin 4445: 357832 of cap free
Amount of items: 1
Items: 
Size: 642169 Color: 3

Bin 4446: 357980 of cap free
Amount of items: 1
Items: 
Size: 642021 Color: 0

Bin 4447: 358058 of cap free
Amount of items: 1
Items: 
Size: 641943 Color: 2

Bin 4448: 358293 of cap free
Amount of items: 1
Items: 
Size: 641708 Color: 4

Bin 4449: 358323 of cap free
Amount of items: 1
Items: 
Size: 641678 Color: 0

Bin 4450: 358427 of cap free
Amount of items: 1
Items: 
Size: 641574 Color: 0

Bin 4451: 358432 of cap free
Amount of items: 1
Items: 
Size: 641569 Color: 1

Bin 4452: 358435 of cap free
Amount of items: 1
Items: 
Size: 641566 Color: 3

Bin 4453: 358439 of cap free
Amount of items: 1
Items: 
Size: 641562 Color: 4

Bin 4454: 358523 of cap free
Amount of items: 1
Items: 
Size: 641478 Color: 2

Bin 4455: 358551 of cap free
Amount of items: 1
Items: 
Size: 641450 Color: 0

Bin 4456: 358559 of cap free
Amount of items: 1
Items: 
Size: 641442 Color: 4

Bin 4457: 358615 of cap free
Amount of items: 1
Items: 
Size: 641386 Color: 4

Bin 4458: 358622 of cap free
Amount of items: 1
Items: 
Size: 641379 Color: 2

Bin 4459: 358676 of cap free
Amount of items: 1
Items: 
Size: 641325 Color: 0

Bin 4460: 358762 of cap free
Amount of items: 1
Items: 
Size: 641239 Color: 0

Bin 4461: 358811 of cap free
Amount of items: 1
Items: 
Size: 641190 Color: 0

Bin 4462: 358824 of cap free
Amount of items: 1
Items: 
Size: 641177 Color: 1

Bin 4463: 358839 of cap free
Amount of items: 1
Items: 
Size: 641162 Color: 0

Bin 4464: 358903 of cap free
Amount of items: 1
Items: 
Size: 641098 Color: 0

Bin 4465: 358928 of cap free
Amount of items: 1
Items: 
Size: 641073 Color: 3

Bin 4466: 359017 of cap free
Amount of items: 1
Items: 
Size: 640984 Color: 0

Bin 4467: 359090 of cap free
Amount of items: 1
Items: 
Size: 640911 Color: 3

Bin 4468: 359159 of cap free
Amount of items: 1
Items: 
Size: 640842 Color: 4

Bin 4469: 359284 of cap free
Amount of items: 1
Items: 
Size: 640717 Color: 1

Bin 4470: 359331 of cap free
Amount of items: 1
Items: 
Size: 640670 Color: 3

Bin 4471: 359387 of cap free
Amount of items: 1
Items: 
Size: 640614 Color: 0

Bin 4472: 359396 of cap free
Amount of items: 1
Items: 
Size: 640605 Color: 4

Bin 4473: 359438 of cap free
Amount of items: 1
Items: 
Size: 640563 Color: 3

Bin 4474: 359562 of cap free
Amount of items: 1
Items: 
Size: 640439 Color: 1

Bin 4475: 359601 of cap free
Amount of items: 1
Items: 
Size: 640400 Color: 4

Bin 4476: 359770 of cap free
Amount of items: 1
Items: 
Size: 640231 Color: 3

Bin 4477: 359803 of cap free
Amount of items: 1
Items: 
Size: 640198 Color: 2

Bin 4478: 359901 of cap free
Amount of items: 1
Items: 
Size: 640100 Color: 2

Bin 4479: 359915 of cap free
Amount of items: 1
Items: 
Size: 640086 Color: 0

Bin 4480: 359989 of cap free
Amount of items: 1
Items: 
Size: 640012 Color: 3

Bin 4481: 360008 of cap free
Amount of items: 1
Items: 
Size: 639993 Color: 4

Bin 4482: 360139 of cap free
Amount of items: 1
Items: 
Size: 639862 Color: 0

Bin 4483: 360183 of cap free
Amount of items: 1
Items: 
Size: 639818 Color: 2

Bin 4484: 360204 of cap free
Amount of items: 1
Items: 
Size: 639797 Color: 1

Bin 4485: 360206 of cap free
Amount of items: 1
Items: 
Size: 639795 Color: 0

Bin 4486: 360240 of cap free
Amount of items: 1
Items: 
Size: 639761 Color: 3

Bin 4487: 360266 of cap free
Amount of items: 1
Items: 
Size: 639735 Color: 0

Bin 4488: 360311 of cap free
Amount of items: 1
Items: 
Size: 639690 Color: 4

Bin 4489: 360423 of cap free
Amount of items: 1
Items: 
Size: 639578 Color: 0

Bin 4490: 360514 of cap free
Amount of items: 1
Items: 
Size: 639487 Color: 2

Bin 4491: 360550 of cap free
Amount of items: 1
Items: 
Size: 639451 Color: 0

Bin 4492: 360700 of cap free
Amount of items: 1
Items: 
Size: 639301 Color: 4

Bin 4493: 360713 of cap free
Amount of items: 1
Items: 
Size: 639288 Color: 0

Bin 4494: 360738 of cap free
Amount of items: 1
Items: 
Size: 639263 Color: 2

Bin 4495: 360793 of cap free
Amount of items: 1
Items: 
Size: 639208 Color: 0

Bin 4496: 360816 of cap free
Amount of items: 1
Items: 
Size: 639185 Color: 0

Bin 4497: 360839 of cap free
Amount of items: 1
Items: 
Size: 639162 Color: 1

Bin 4498: 360843 of cap free
Amount of items: 1
Items: 
Size: 639158 Color: 3

Bin 4499: 360844 of cap free
Amount of items: 1
Items: 
Size: 639157 Color: 0

Bin 4500: 361013 of cap free
Amount of items: 1
Items: 
Size: 638988 Color: 2

Bin 4501: 361018 of cap free
Amount of items: 1
Items: 
Size: 638983 Color: 2

Bin 4502: 361173 of cap free
Amount of items: 1
Items: 
Size: 638828 Color: 4

Bin 4503: 361196 of cap free
Amount of items: 1
Items: 
Size: 638805 Color: 3

Bin 4504: 361242 of cap free
Amount of items: 1
Items: 
Size: 638759 Color: 4

Bin 4505: 361277 of cap free
Amount of items: 1
Items: 
Size: 638724 Color: 3

Bin 4506: 361319 of cap free
Amount of items: 1
Items: 
Size: 638682 Color: 0

Bin 4507: 361363 of cap free
Amount of items: 1
Items: 
Size: 638638 Color: 1

Bin 4508: 361425 of cap free
Amount of items: 1
Items: 
Size: 638576 Color: 1

Bin 4509: 361475 of cap free
Amount of items: 1
Items: 
Size: 638526 Color: 0

Bin 4510: 361545 of cap free
Amount of items: 1
Items: 
Size: 638456 Color: 3

Bin 4511: 361548 of cap free
Amount of items: 1
Items: 
Size: 638453 Color: 0

Bin 4512: 361690 of cap free
Amount of items: 1
Items: 
Size: 638311 Color: 1

Bin 4513: 361723 of cap free
Amount of items: 1
Items: 
Size: 638278 Color: 3

Bin 4514: 361863 of cap free
Amount of items: 1
Items: 
Size: 638138 Color: 4

Bin 4515: 361907 of cap free
Amount of items: 1
Items: 
Size: 638094 Color: 0

Bin 4516: 361907 of cap free
Amount of items: 1
Items: 
Size: 638094 Color: 0

Bin 4517: 361942 of cap free
Amount of items: 1
Items: 
Size: 638059 Color: 0

Bin 4518: 362184 of cap free
Amount of items: 1
Items: 
Size: 637817 Color: 4

Bin 4519: 362194 of cap free
Amount of items: 1
Items: 
Size: 637807 Color: 3

Bin 4520: 362307 of cap free
Amount of items: 1
Items: 
Size: 637694 Color: 2

Bin 4521: 362337 of cap free
Amount of items: 1
Items: 
Size: 637664 Color: 2

Bin 4522: 362523 of cap free
Amount of items: 1
Items: 
Size: 637478 Color: 0

Bin 4523: 362577 of cap free
Amount of items: 1
Items: 
Size: 637424 Color: 2

Bin 4524: 362594 of cap free
Amount of items: 1
Items: 
Size: 637407 Color: 1

Bin 4525: 362675 of cap free
Amount of items: 1
Items: 
Size: 637326 Color: 4

Bin 4526: 362775 of cap free
Amount of items: 1
Items: 
Size: 637226 Color: 3

Bin 4527: 362881 of cap free
Amount of items: 1
Items: 
Size: 637120 Color: 4

Bin 4528: 363004 of cap free
Amount of items: 1
Items: 
Size: 636997 Color: 1

Bin 4529: 363018 of cap free
Amount of items: 1
Items: 
Size: 636983 Color: 3

Bin 4530: 363028 of cap free
Amount of items: 1
Items: 
Size: 636973 Color: 2

Bin 4531: 363091 of cap free
Amount of items: 1
Items: 
Size: 636910 Color: 0

Bin 4532: 363125 of cap free
Amount of items: 1
Items: 
Size: 636876 Color: 3

Bin 4533: 363182 of cap free
Amount of items: 1
Items: 
Size: 636819 Color: 0

Bin 4534: 363277 of cap free
Amount of items: 1
Items: 
Size: 636724 Color: 1

Bin 4535: 363324 of cap free
Amount of items: 1
Items: 
Size: 636677 Color: 4

Bin 4536: 363352 of cap free
Amount of items: 1
Items: 
Size: 636649 Color: 0

Bin 4537: 363353 of cap free
Amount of items: 1
Items: 
Size: 636648 Color: 3

Bin 4538: 363369 of cap free
Amount of items: 1
Items: 
Size: 636632 Color: 4

Bin 4539: 363412 of cap free
Amount of items: 1
Items: 
Size: 636589 Color: 4

Bin 4540: 363442 of cap free
Amount of items: 1
Items: 
Size: 636559 Color: 1

Bin 4541: 363456 of cap free
Amount of items: 1
Items: 
Size: 636545 Color: 3

Bin 4542: 363495 of cap free
Amount of items: 1
Items: 
Size: 636506 Color: 4

Bin 4543: 363604 of cap free
Amount of items: 1
Items: 
Size: 636397 Color: 0

Bin 4544: 363628 of cap free
Amount of items: 1
Items: 
Size: 636373 Color: 4

Bin 4545: 363775 of cap free
Amount of items: 1
Items: 
Size: 636226 Color: 0

Bin 4546: 363871 of cap free
Amount of items: 1
Items: 
Size: 636130 Color: 1

Bin 4547: 363921 of cap free
Amount of items: 1
Items: 
Size: 636080 Color: 0

Bin 4548: 364015 of cap free
Amount of items: 1
Items: 
Size: 635986 Color: 3

Bin 4549: 364061 of cap free
Amount of items: 1
Items: 
Size: 635940 Color: 2

Bin 4550: 364175 of cap free
Amount of items: 1
Items: 
Size: 635826 Color: 0

Bin 4551: 364317 of cap free
Amount of items: 1
Items: 
Size: 635684 Color: 4

Bin 4552: 364366 of cap free
Amount of items: 1
Items: 
Size: 635635 Color: 0

Bin 4553: 364388 of cap free
Amount of items: 1
Items: 
Size: 635613 Color: 1

Bin 4554: 364404 of cap free
Amount of items: 1
Items: 
Size: 635597 Color: 2

Bin 4555: 364472 of cap free
Amount of items: 1
Items: 
Size: 635529 Color: 4

Bin 4556: 364489 of cap free
Amount of items: 1
Items: 
Size: 635512 Color: 2

Bin 4557: 364938 of cap free
Amount of items: 1
Items: 
Size: 635063 Color: 2

Bin 4558: 364999 of cap free
Amount of items: 1
Items: 
Size: 635002 Color: 1

Bin 4559: 365260 of cap free
Amount of items: 1
Items: 
Size: 634741 Color: 4

Bin 4560: 365267 of cap free
Amount of items: 1
Items: 
Size: 634734 Color: 1

Bin 4561: 365668 of cap free
Amount of items: 1
Items: 
Size: 634333 Color: 2

Bin 4562: 365723 of cap free
Amount of items: 1
Items: 
Size: 634278 Color: 2

Bin 4563: 365791 of cap free
Amount of items: 1
Items: 
Size: 634210 Color: 1

Bin 4564: 365844 of cap free
Amount of items: 1
Items: 
Size: 634157 Color: 2

Bin 4565: 365940 of cap free
Amount of items: 1
Items: 
Size: 634061 Color: 1

Bin 4566: 365966 of cap free
Amount of items: 1
Items: 
Size: 634035 Color: 1

Bin 4567: 366002 of cap free
Amount of items: 1
Items: 
Size: 633999 Color: 3

Bin 4568: 366082 of cap free
Amount of items: 1
Items: 
Size: 633919 Color: 4

Bin 4569: 366315 of cap free
Amount of items: 1
Items: 
Size: 633686 Color: 4

Bin 4570: 366329 of cap free
Amount of items: 1
Items: 
Size: 633672 Color: 4

Bin 4571: 366357 of cap free
Amount of items: 1
Items: 
Size: 633644 Color: 1

Bin 4572: 366448 of cap free
Amount of items: 1
Items: 
Size: 633553 Color: 3

Bin 4573: 366457 of cap free
Amount of items: 1
Items: 
Size: 633544 Color: 3

Bin 4574: 366493 of cap free
Amount of items: 1
Items: 
Size: 633508 Color: 3

Bin 4575: 366608 of cap free
Amount of items: 1
Items: 
Size: 633393 Color: 4

Bin 4576: 366623 of cap free
Amount of items: 1
Items: 
Size: 633378 Color: 3

Bin 4577: 366651 of cap free
Amount of items: 1
Items: 
Size: 633350 Color: 4

Bin 4578: 366652 of cap free
Amount of items: 1
Items: 
Size: 633349 Color: 1

Bin 4579: 366667 of cap free
Amount of items: 1
Items: 
Size: 633334 Color: 2

Bin 4580: 366747 of cap free
Amount of items: 1
Items: 
Size: 633254 Color: 1

Bin 4581: 366766 of cap free
Amount of items: 1
Items: 
Size: 633235 Color: 2

Bin 4582: 366827 of cap free
Amount of items: 1
Items: 
Size: 633174 Color: 0

Bin 4583: 366897 of cap free
Amount of items: 1
Items: 
Size: 633104 Color: 4

Bin 4584: 367026 of cap free
Amount of items: 1
Items: 
Size: 632975 Color: 1

Bin 4585: 367026 of cap free
Amount of items: 1
Items: 
Size: 632975 Color: 3

Bin 4586: 367077 of cap free
Amount of items: 1
Items: 
Size: 632924 Color: 4

Bin 4587: 367100 of cap free
Amount of items: 1
Items: 
Size: 632901 Color: 2

Bin 4588: 367104 of cap free
Amount of items: 1
Items: 
Size: 632897 Color: 2

Bin 4589: 367119 of cap free
Amount of items: 1
Items: 
Size: 632882 Color: 3

Bin 4590: 367121 of cap free
Amount of items: 1
Items: 
Size: 632880 Color: 0

Bin 4591: 367206 of cap free
Amount of items: 1
Items: 
Size: 632795 Color: 1

Bin 4592: 367248 of cap free
Amount of items: 1
Items: 
Size: 632753 Color: 3

Bin 4593: 367345 of cap free
Amount of items: 1
Items: 
Size: 632656 Color: 4

Bin 4594: 367418 of cap free
Amount of items: 1
Items: 
Size: 632583 Color: 1

Bin 4595: 367539 of cap free
Amount of items: 1
Items: 
Size: 632462 Color: 4

Bin 4596: 367693 of cap free
Amount of items: 1
Items: 
Size: 632308 Color: 4

Bin 4597: 367744 of cap free
Amount of items: 1
Items: 
Size: 632257 Color: 0

Bin 4598: 367761 of cap free
Amount of items: 1
Items: 
Size: 632240 Color: 2

Bin 4599: 367835 of cap free
Amount of items: 1
Items: 
Size: 632166 Color: 1

Bin 4600: 367845 of cap free
Amount of items: 1
Items: 
Size: 632156 Color: 3

Bin 4601: 367886 of cap free
Amount of items: 1
Items: 
Size: 632115 Color: 1

Bin 4602: 368006 of cap free
Amount of items: 1
Items: 
Size: 631995 Color: 3

Bin 4603: 368143 of cap free
Amount of items: 1
Items: 
Size: 631858 Color: 0

Bin 4604: 368304 of cap free
Amount of items: 1
Items: 
Size: 631697 Color: 2

Bin 4605: 368368 of cap free
Amount of items: 1
Items: 
Size: 631633 Color: 2

Bin 4606: 368402 of cap free
Amount of items: 1
Items: 
Size: 631599 Color: 2

Bin 4607: 368420 of cap free
Amount of items: 1
Items: 
Size: 631581 Color: 4

Bin 4608: 368526 of cap free
Amount of items: 1
Items: 
Size: 631475 Color: 3

Bin 4609: 368556 of cap free
Amount of items: 1
Items: 
Size: 631445 Color: 1

Bin 4610: 368566 of cap free
Amount of items: 1
Items: 
Size: 631435 Color: 0

Bin 4611: 368626 of cap free
Amount of items: 1
Items: 
Size: 631375 Color: 0

Bin 4612: 368652 of cap free
Amount of items: 1
Items: 
Size: 631349 Color: 0

Bin 4613: 368731 of cap free
Amount of items: 1
Items: 
Size: 631270 Color: 3

Bin 4614: 368794 of cap free
Amount of items: 1
Items: 
Size: 631207 Color: 4

Bin 4615: 368962 of cap free
Amount of items: 1
Items: 
Size: 631039 Color: 4

Bin 4616: 368977 of cap free
Amount of items: 1
Items: 
Size: 631024 Color: 3

Bin 4617: 368991 of cap free
Amount of items: 1
Items: 
Size: 631010 Color: 3

Bin 4618: 369008 of cap free
Amount of items: 1
Items: 
Size: 630993 Color: 4

Bin 4619: 369014 of cap free
Amount of items: 1
Items: 
Size: 630987 Color: 4

Bin 4620: 369037 of cap free
Amount of items: 1
Items: 
Size: 630964 Color: 1

Bin 4621: 369130 of cap free
Amount of items: 1
Items: 
Size: 630871 Color: 2

Bin 4622: 369132 of cap free
Amount of items: 1
Items: 
Size: 630869 Color: 3

Bin 4623: 369141 of cap free
Amount of items: 1
Items: 
Size: 630860 Color: 1

Bin 4624: 369192 of cap free
Amount of items: 1
Items: 
Size: 630809 Color: 2

Bin 4625: 369365 of cap free
Amount of items: 1
Items: 
Size: 630636 Color: 4

Bin 4626: 369428 of cap free
Amount of items: 1
Items: 
Size: 630573 Color: 2

Bin 4627: 369478 of cap free
Amount of items: 1
Items: 
Size: 630523 Color: 4

Bin 4628: 369669 of cap free
Amount of items: 1
Items: 
Size: 630332 Color: 3

Bin 4629: 369797 of cap free
Amount of items: 1
Items: 
Size: 630204 Color: 2

Bin 4630: 369850 of cap free
Amount of items: 1
Items: 
Size: 630151 Color: 4

Bin 4631: 369975 of cap free
Amount of items: 1
Items: 
Size: 630026 Color: 0

Bin 4632: 369982 of cap free
Amount of items: 1
Items: 
Size: 630019 Color: 4

Bin 4633: 370113 of cap free
Amount of items: 1
Items: 
Size: 629888 Color: 3

Bin 4634: 370139 of cap free
Amount of items: 1
Items: 
Size: 629862 Color: 1

Bin 4635: 370213 of cap free
Amount of items: 1
Items: 
Size: 629788 Color: 1

Bin 4636: 370277 of cap free
Amount of items: 1
Items: 
Size: 629724 Color: 2

Bin 4637: 370376 of cap free
Amount of items: 1
Items: 
Size: 629625 Color: 1

Bin 4638: 370390 of cap free
Amount of items: 1
Items: 
Size: 629611 Color: 3

Bin 4639: 370473 of cap free
Amount of items: 1
Items: 
Size: 629528 Color: 4

Bin 4640: 370488 of cap free
Amount of items: 1
Items: 
Size: 629513 Color: 0

Bin 4641: 370515 of cap free
Amount of items: 1
Items: 
Size: 629486 Color: 1

Bin 4642: 370520 of cap free
Amount of items: 1
Items: 
Size: 629481 Color: 0

Bin 4643: 370523 of cap free
Amount of items: 1
Items: 
Size: 629478 Color: 1

Bin 4644: 370567 of cap free
Amount of items: 1
Items: 
Size: 629434 Color: 2

Bin 4645: 370643 of cap free
Amount of items: 1
Items: 
Size: 629358 Color: 4

Bin 4646: 370655 of cap free
Amount of items: 1
Items: 
Size: 629346 Color: 2

Bin 4647: 370671 of cap free
Amount of items: 1
Items: 
Size: 629330 Color: 2

Bin 4648: 370685 of cap free
Amount of items: 1
Items: 
Size: 629316 Color: 3

Bin 4649: 370838 of cap free
Amount of items: 1
Items: 
Size: 629163 Color: 1

Bin 4650: 370938 of cap free
Amount of items: 1
Items: 
Size: 629063 Color: 2

Bin 4651: 371166 of cap free
Amount of items: 1
Items: 
Size: 628835 Color: 4

Bin 4652: 371285 of cap free
Amount of items: 1
Items: 
Size: 628716 Color: 3

Bin 4653: 371396 of cap free
Amount of items: 1
Items: 
Size: 628605 Color: 3

Bin 4654: 371426 of cap free
Amount of items: 1
Items: 
Size: 628575 Color: 0

Bin 4655: 371432 of cap free
Amount of items: 1
Items: 
Size: 628569 Color: 4

Bin 4656: 371537 of cap free
Amount of items: 1
Items: 
Size: 628464 Color: 4

Bin 4657: 371540 of cap free
Amount of items: 1
Items: 
Size: 628461 Color: 2

Bin 4658: 371584 of cap free
Amount of items: 1
Items: 
Size: 628417 Color: 0

Bin 4659: 371662 of cap free
Amount of items: 1
Items: 
Size: 628339 Color: 2

Bin 4660: 371775 of cap free
Amount of items: 1
Items: 
Size: 628226 Color: 2

Bin 4661: 371843 of cap free
Amount of items: 1
Items: 
Size: 628158 Color: 1

Bin 4662: 371873 of cap free
Amount of items: 1
Items: 
Size: 628128 Color: 3

Bin 4663: 371889 of cap free
Amount of items: 1
Items: 
Size: 628112 Color: 1

Bin 4664: 371926 of cap free
Amount of items: 1
Items: 
Size: 628075 Color: 0

Bin 4665: 372023 of cap free
Amount of items: 1
Items: 
Size: 627978 Color: 2

Bin 4666: 372081 of cap free
Amount of items: 1
Items: 
Size: 627920 Color: 1

Bin 4667: 372145 of cap free
Amount of items: 1
Items: 
Size: 627856 Color: 1

Bin 4668: 372199 of cap free
Amount of items: 1
Items: 
Size: 627802 Color: 0

Bin 4669: 372229 of cap free
Amount of items: 1
Items: 
Size: 627772 Color: 1

Bin 4670: 372356 of cap free
Amount of items: 1
Items: 
Size: 627645 Color: 0

Bin 4671: 372644 of cap free
Amount of items: 1
Items: 
Size: 627357 Color: 0

Bin 4672: 372690 of cap free
Amount of items: 1
Items: 
Size: 627311 Color: 0

Bin 4673: 372761 of cap free
Amount of items: 1
Items: 
Size: 627240 Color: 2

Bin 4674: 372802 of cap free
Amount of items: 1
Items: 
Size: 627199 Color: 2

Bin 4675: 373096 of cap free
Amount of items: 1
Items: 
Size: 626905 Color: 1

Bin 4676: 373229 of cap free
Amount of items: 1
Items: 
Size: 626772 Color: 0

Bin 4677: 373320 of cap free
Amount of items: 1
Items: 
Size: 626681 Color: 0

Bin 4678: 373370 of cap free
Amount of items: 1
Items: 
Size: 626631 Color: 2

Bin 4679: 373417 of cap free
Amount of items: 1
Items: 
Size: 626584 Color: 4

Bin 4680: 373627 of cap free
Amount of items: 1
Items: 
Size: 626374 Color: 0

Bin 4681: 373630 of cap free
Amount of items: 1
Items: 
Size: 626371 Color: 3

Bin 4682: 373794 of cap free
Amount of items: 1
Items: 
Size: 626207 Color: 4

Bin 4683: 373842 of cap free
Amount of items: 1
Items: 
Size: 626159 Color: 4

Bin 4684: 373850 of cap free
Amount of items: 1
Items: 
Size: 626151 Color: 1

Bin 4685: 373898 of cap free
Amount of items: 1
Items: 
Size: 626103 Color: 1

Bin 4686: 373907 of cap free
Amount of items: 1
Items: 
Size: 626094 Color: 3

Bin 4687: 373908 of cap free
Amount of items: 1
Items: 
Size: 626093 Color: 2

Bin 4688: 373991 of cap free
Amount of items: 1
Items: 
Size: 626010 Color: 1

Bin 4689: 374039 of cap free
Amount of items: 1
Items: 
Size: 625962 Color: 3

Bin 4690: 374063 of cap free
Amount of items: 1
Items: 
Size: 625938 Color: 1

Bin 4691: 374097 of cap free
Amount of items: 1
Items: 
Size: 625904 Color: 0

Bin 4692: 374133 of cap free
Amount of items: 1
Items: 
Size: 625868 Color: 4

Bin 4693: 374397 of cap free
Amount of items: 1
Items: 
Size: 625604 Color: 1

Bin 4694: 374458 of cap free
Amount of items: 1
Items: 
Size: 625543 Color: 1

Bin 4695: 374474 of cap free
Amount of items: 1
Items: 
Size: 625527 Color: 4

Bin 4696: 374523 of cap free
Amount of items: 1
Items: 
Size: 625478 Color: 3

Bin 4697: 374542 of cap free
Amount of items: 1
Items: 
Size: 625459 Color: 1

Bin 4698: 374574 of cap free
Amount of items: 1
Items: 
Size: 625427 Color: 0

Bin 4699: 374581 of cap free
Amount of items: 1
Items: 
Size: 625420 Color: 1

Bin 4700: 374709 of cap free
Amount of items: 1
Items: 
Size: 625292 Color: 0

Bin 4701: 374881 of cap free
Amount of items: 1
Items: 
Size: 625120 Color: 2

Bin 4702: 374997 of cap free
Amount of items: 1
Items: 
Size: 625004 Color: 2

Bin 4703: 375010 of cap free
Amount of items: 1
Items: 
Size: 624991 Color: 3

Bin 4704: 375057 of cap free
Amount of items: 1
Items: 
Size: 624944 Color: 0

Bin 4705: 375120 of cap free
Amount of items: 1
Items: 
Size: 624881 Color: 0

Bin 4706: 375123 of cap free
Amount of items: 1
Items: 
Size: 624878 Color: 1

Bin 4707: 375296 of cap free
Amount of items: 1
Items: 
Size: 624705 Color: 1

Bin 4708: 375301 of cap free
Amount of items: 1
Items: 
Size: 624700 Color: 4

Bin 4709: 375373 of cap free
Amount of items: 1
Items: 
Size: 624628 Color: 2

Bin 4710: 375442 of cap free
Amount of items: 1
Items: 
Size: 624559 Color: 1

Bin 4711: 375484 of cap free
Amount of items: 1
Items: 
Size: 624517 Color: 1

Bin 4712: 375529 of cap free
Amount of items: 1
Items: 
Size: 624472 Color: 2

Bin 4713: 375555 of cap free
Amount of items: 1
Items: 
Size: 624446 Color: 4

Bin 4714: 375791 of cap free
Amount of items: 1
Items: 
Size: 624210 Color: 3

Bin 4715: 375798 of cap free
Amount of items: 1
Items: 
Size: 624203 Color: 1

Bin 4716: 375952 of cap free
Amount of items: 1
Items: 
Size: 624049 Color: 1

Bin 4717: 376002 of cap free
Amount of items: 1
Items: 
Size: 623999 Color: 1

Bin 4718: 376074 of cap free
Amount of items: 1
Items: 
Size: 623927 Color: 4

Bin 4719: 376101 of cap free
Amount of items: 1
Items: 
Size: 623900 Color: 1

Bin 4720: 376193 of cap free
Amount of items: 1
Items: 
Size: 623808 Color: 0

Bin 4721: 376195 of cap free
Amount of items: 1
Items: 
Size: 623806 Color: 1

Bin 4722: 376228 of cap free
Amount of items: 1
Items: 
Size: 623773 Color: 4

Bin 4723: 376229 of cap free
Amount of items: 1
Items: 
Size: 623772 Color: 0

Bin 4724: 376229 of cap free
Amount of items: 1
Items: 
Size: 623772 Color: 3

Bin 4725: 376244 of cap free
Amount of items: 1
Items: 
Size: 623757 Color: 1

Bin 4726: 376284 of cap free
Amount of items: 1
Items: 
Size: 623717 Color: 4

Bin 4727: 376295 of cap free
Amount of items: 1
Items: 
Size: 623706 Color: 4

Bin 4728: 376299 of cap free
Amount of items: 1
Items: 
Size: 623702 Color: 4

Bin 4729: 376315 of cap free
Amount of items: 1
Items: 
Size: 623686 Color: 1

Bin 4730: 376370 of cap free
Amount of items: 1
Items: 
Size: 623631 Color: 2

Bin 4731: 376429 of cap free
Amount of items: 1
Items: 
Size: 623572 Color: 1

Bin 4732: 376499 of cap free
Amount of items: 1
Items: 
Size: 623502 Color: 0

Bin 4733: 376515 of cap free
Amount of items: 1
Items: 
Size: 623486 Color: 2

Bin 4734: 376587 of cap free
Amount of items: 1
Items: 
Size: 623414 Color: 3

Bin 4735: 376672 of cap free
Amount of items: 1
Items: 
Size: 623329 Color: 2

Bin 4736: 376683 of cap free
Amount of items: 1
Items: 
Size: 623318 Color: 3

Bin 4737: 376708 of cap free
Amount of items: 1
Items: 
Size: 623293 Color: 4

Bin 4738: 376713 of cap free
Amount of items: 1
Items: 
Size: 623288 Color: 1

Bin 4739: 376749 of cap free
Amount of items: 1
Items: 
Size: 623252 Color: 4

Bin 4740: 376756 of cap free
Amount of items: 1
Items: 
Size: 623245 Color: 2

Bin 4741: 376843 of cap free
Amount of items: 1
Items: 
Size: 623158 Color: 0

Bin 4742: 377029 of cap free
Amount of items: 1
Items: 
Size: 622972 Color: 4

Bin 4743: 377053 of cap free
Amount of items: 1
Items: 
Size: 622948 Color: 3

Bin 4744: 377071 of cap free
Amount of items: 1
Items: 
Size: 622930 Color: 4

Bin 4745: 377202 of cap free
Amount of items: 1
Items: 
Size: 622799 Color: 1

Bin 4746: 377206 of cap free
Amount of items: 1
Items: 
Size: 622795 Color: 1

Bin 4747: 377224 of cap free
Amount of items: 1
Items: 
Size: 622777 Color: 0

Bin 4748: 377262 of cap free
Amount of items: 1
Items: 
Size: 622739 Color: 0

Bin 4749: 377275 of cap free
Amount of items: 1
Items: 
Size: 622726 Color: 4

Bin 4750: 377282 of cap free
Amount of items: 1
Items: 
Size: 622719 Color: 3

Bin 4751: 377283 of cap free
Amount of items: 1
Items: 
Size: 622718 Color: 1

Bin 4752: 377336 of cap free
Amount of items: 1
Items: 
Size: 622665 Color: 0

Bin 4753: 377444 of cap free
Amount of items: 1
Items: 
Size: 622557 Color: 3

Bin 4754: 377445 of cap free
Amount of items: 1
Items: 
Size: 622556 Color: 4

Bin 4755: 377480 of cap free
Amount of items: 1
Items: 
Size: 622521 Color: 1

Bin 4756: 377559 of cap free
Amount of items: 1
Items: 
Size: 622442 Color: 3

Bin 4757: 377697 of cap free
Amount of items: 1
Items: 
Size: 622304 Color: 1

Bin 4758: 377924 of cap free
Amount of items: 1
Items: 
Size: 622077 Color: 1

Bin 4759: 377936 of cap free
Amount of items: 1
Items: 
Size: 622065 Color: 1

Bin 4760: 378008 of cap free
Amount of items: 1
Items: 
Size: 621993 Color: 3

Bin 4761: 378175 of cap free
Amount of items: 1
Items: 
Size: 621826 Color: 0

Bin 4762: 378351 of cap free
Amount of items: 1
Items: 
Size: 621650 Color: 3

Bin 4763: 378374 of cap free
Amount of items: 1
Items: 
Size: 621627 Color: 3

Bin 4764: 378497 of cap free
Amount of items: 1
Items: 
Size: 621504 Color: 4

Bin 4765: 378549 of cap free
Amount of items: 1
Items: 
Size: 621452 Color: 0

Bin 4766: 378582 of cap free
Amount of items: 1
Items: 
Size: 621419 Color: 1

Bin 4767: 378624 of cap free
Amount of items: 1
Items: 
Size: 621377 Color: 2

Bin 4768: 378667 of cap free
Amount of items: 1
Items: 
Size: 621334 Color: 0

Bin 4769: 378859 of cap free
Amount of items: 1
Items: 
Size: 621142 Color: 3

Bin 4770: 378956 of cap free
Amount of items: 1
Items: 
Size: 621045 Color: 4

Bin 4771: 379029 of cap free
Amount of items: 1
Items: 
Size: 620972 Color: 1

Bin 4772: 379034 of cap free
Amount of items: 1
Items: 
Size: 620967 Color: 0

Bin 4773: 379065 of cap free
Amount of items: 1
Items: 
Size: 620936 Color: 4

Bin 4774: 379159 of cap free
Amount of items: 1
Items: 
Size: 620842 Color: 1

Bin 4775: 379205 of cap free
Amount of items: 1
Items: 
Size: 620796 Color: 2

Bin 4776: 379252 of cap free
Amount of items: 1
Items: 
Size: 620749 Color: 3

Bin 4777: 379253 of cap free
Amount of items: 1
Items: 
Size: 620748 Color: 4

Bin 4778: 379311 of cap free
Amount of items: 1
Items: 
Size: 620690 Color: 4

Bin 4779: 379322 of cap free
Amount of items: 1
Items: 
Size: 620679 Color: 1

Bin 4780: 379367 of cap free
Amount of items: 1
Items: 
Size: 620634 Color: 2

Bin 4781: 379393 of cap free
Amount of items: 1
Items: 
Size: 620608 Color: 1

Bin 4782: 379636 of cap free
Amount of items: 1
Items: 
Size: 620365 Color: 4

Bin 4783: 379680 of cap free
Amount of items: 1
Items: 
Size: 620321 Color: 2

Bin 4784: 379805 of cap free
Amount of items: 1
Items: 
Size: 620196 Color: 3

Bin 4785: 379885 of cap free
Amount of items: 1
Items: 
Size: 620116 Color: 0

Bin 4786: 379891 of cap free
Amount of items: 1
Items: 
Size: 620110 Color: 1

Bin 4787: 379892 of cap free
Amount of items: 1
Items: 
Size: 620109 Color: 1

Bin 4788: 380015 of cap free
Amount of items: 1
Items: 
Size: 619986 Color: 0

Bin 4789: 380162 of cap free
Amount of items: 1
Items: 
Size: 619839 Color: 4

Bin 4790: 380179 of cap free
Amount of items: 1
Items: 
Size: 619822 Color: 2

Bin 4791: 380220 of cap free
Amount of items: 1
Items: 
Size: 619781 Color: 0

Bin 4792: 380316 of cap free
Amount of items: 1
Items: 
Size: 619685 Color: 3

Bin 4793: 380318 of cap free
Amount of items: 1
Items: 
Size: 619683 Color: 1

Bin 4794: 380363 of cap free
Amount of items: 1
Items: 
Size: 619638 Color: 4

Bin 4795: 380502 of cap free
Amount of items: 1
Items: 
Size: 619499 Color: 2

Bin 4796: 380555 of cap free
Amount of items: 1
Items: 
Size: 619446 Color: 2

Bin 4797: 380565 of cap free
Amount of items: 1
Items: 
Size: 619436 Color: 0

Bin 4798: 380632 of cap free
Amount of items: 1
Items: 
Size: 619369 Color: 0

Bin 4799: 380640 of cap free
Amount of items: 1
Items: 
Size: 619361 Color: 1

Bin 4800: 380696 of cap free
Amount of items: 1
Items: 
Size: 619305 Color: 0

Bin 4801: 380722 of cap free
Amount of items: 1
Items: 
Size: 619279 Color: 4

Bin 4802: 380733 of cap free
Amount of items: 1
Items: 
Size: 619268 Color: 2

Bin 4803: 380833 of cap free
Amount of items: 1
Items: 
Size: 619168 Color: 2

Bin 4804: 380883 of cap free
Amount of items: 1
Items: 
Size: 619118 Color: 4

Bin 4805: 380964 of cap free
Amount of items: 1
Items: 
Size: 619037 Color: 3

Bin 4806: 381029 of cap free
Amount of items: 1
Items: 
Size: 618972 Color: 1

Bin 4807: 381113 of cap free
Amount of items: 1
Items: 
Size: 618888 Color: 4

Bin 4808: 381121 of cap free
Amount of items: 1
Items: 
Size: 618880 Color: 4

Bin 4809: 381144 of cap free
Amount of items: 1
Items: 
Size: 618857 Color: 4

Bin 4810: 381188 of cap free
Amount of items: 1
Items: 
Size: 618813 Color: 2

Bin 4811: 381392 of cap free
Amount of items: 1
Items: 
Size: 618609 Color: 2

Bin 4812: 381468 of cap free
Amount of items: 1
Items: 
Size: 618533 Color: 0

Bin 4813: 381611 of cap free
Amount of items: 1
Items: 
Size: 618390 Color: 3

Bin 4814: 381616 of cap free
Amount of items: 1
Items: 
Size: 618385 Color: 0

Bin 4815: 381634 of cap free
Amount of items: 1
Items: 
Size: 618367 Color: 1

Bin 4816: 381719 of cap free
Amount of items: 1
Items: 
Size: 618282 Color: 4

Bin 4817: 381742 of cap free
Amount of items: 1
Items: 
Size: 618259 Color: 3

Bin 4818: 381776 of cap free
Amount of items: 1
Items: 
Size: 618225 Color: 4

Bin 4819: 381805 of cap free
Amount of items: 1
Items: 
Size: 618196 Color: 3

Bin 4820: 381864 of cap free
Amount of items: 1
Items: 
Size: 618137 Color: 3

Bin 4821: 381901 of cap free
Amount of items: 1
Items: 
Size: 618100 Color: 2

Bin 4822: 381983 of cap free
Amount of items: 1
Items: 
Size: 618018 Color: 3

Bin 4823: 382042 of cap free
Amount of items: 1
Items: 
Size: 617959 Color: 3

Bin 4824: 382148 of cap free
Amount of items: 1
Items: 
Size: 617853 Color: 2

Bin 4825: 382152 of cap free
Amount of items: 1
Items: 
Size: 617849 Color: 0

Bin 4826: 382236 of cap free
Amount of items: 1
Items: 
Size: 617765 Color: 1

Bin 4827: 382279 of cap free
Amount of items: 1
Items: 
Size: 617722 Color: 1

Bin 4828: 382354 of cap free
Amount of items: 1
Items: 
Size: 617647 Color: 1

Bin 4829: 382412 of cap free
Amount of items: 1
Items: 
Size: 617589 Color: 2

Bin 4830: 382436 of cap free
Amount of items: 1
Items: 
Size: 617565 Color: 3

Bin 4831: 382584 of cap free
Amount of items: 1
Items: 
Size: 617417 Color: 3

Bin 4832: 382608 of cap free
Amount of items: 1
Items: 
Size: 617393 Color: 1

Bin 4833: 382761 of cap free
Amount of items: 1
Items: 
Size: 617240 Color: 1

Bin 4834: 382860 of cap free
Amount of items: 1
Items: 
Size: 617141 Color: 4

Bin 4835: 382992 of cap free
Amount of items: 1
Items: 
Size: 617009 Color: 2

Bin 4836: 383148 of cap free
Amount of items: 1
Items: 
Size: 616853 Color: 0

Bin 4837: 383277 of cap free
Amount of items: 1
Items: 
Size: 616724 Color: 0

Bin 4838: 383300 of cap free
Amount of items: 1
Items: 
Size: 616701 Color: 1

Bin 4839: 383428 of cap free
Amount of items: 1
Items: 
Size: 616573 Color: 3

Bin 4840: 383442 of cap free
Amount of items: 1
Items: 
Size: 616559 Color: 0

Bin 4841: 383472 of cap free
Amount of items: 1
Items: 
Size: 616529 Color: 1

Bin 4842: 383554 of cap free
Amount of items: 1
Items: 
Size: 616447 Color: 2

Bin 4843: 383629 of cap free
Amount of items: 1
Items: 
Size: 616372 Color: 1

Bin 4844: 383712 of cap free
Amount of items: 1
Items: 
Size: 616289 Color: 2

Bin 4845: 383790 of cap free
Amount of items: 1
Items: 
Size: 616211 Color: 2

Bin 4846: 383850 of cap free
Amount of items: 1
Items: 
Size: 616151 Color: 2

Bin 4847: 383869 of cap free
Amount of items: 1
Items: 
Size: 616132 Color: 4

Bin 4848: 383883 of cap free
Amount of items: 1
Items: 
Size: 616118 Color: 1

Bin 4849: 383959 of cap free
Amount of items: 1
Items: 
Size: 616042 Color: 4

Bin 4850: 384044 of cap free
Amount of items: 1
Items: 
Size: 615957 Color: 1

Bin 4851: 384103 of cap free
Amount of items: 1
Items: 
Size: 615898 Color: 4

Bin 4852: 384162 of cap free
Amount of items: 1
Items: 
Size: 615839 Color: 2

Bin 4853: 384198 of cap free
Amount of items: 1
Items: 
Size: 615803 Color: 2

Bin 4854: 384316 of cap free
Amount of items: 1
Items: 
Size: 615685 Color: 4

Bin 4855: 384370 of cap free
Amount of items: 1
Items: 
Size: 615631 Color: 1

Bin 4856: 384408 of cap free
Amount of items: 1
Items: 
Size: 615593 Color: 2

Bin 4857: 384424 of cap free
Amount of items: 1
Items: 
Size: 615577 Color: 3

Bin 4858: 384467 of cap free
Amount of items: 1
Items: 
Size: 615534 Color: 2

Bin 4859: 384501 of cap free
Amount of items: 1
Items: 
Size: 615500 Color: 0

Bin 4860: 384598 of cap free
Amount of items: 1
Items: 
Size: 615403 Color: 4

Bin 4861: 384599 of cap free
Amount of items: 1
Items: 
Size: 615402 Color: 3

Bin 4862: 384622 of cap free
Amount of items: 1
Items: 
Size: 615379 Color: 1

Bin 4863: 384629 of cap free
Amount of items: 1
Items: 
Size: 615372 Color: 3

Bin 4864: 384651 of cap free
Amount of items: 1
Items: 
Size: 615350 Color: 4

Bin 4865: 384759 of cap free
Amount of items: 1
Items: 
Size: 615242 Color: 0

Bin 4866: 384895 of cap free
Amount of items: 1
Items: 
Size: 615106 Color: 1

Bin 4867: 384899 of cap free
Amount of items: 1
Items: 
Size: 615102 Color: 1

Bin 4868: 384905 of cap free
Amount of items: 1
Items: 
Size: 615096 Color: 2

Bin 4869: 384991 of cap free
Amount of items: 1
Items: 
Size: 615010 Color: 4

Bin 4870: 385053 of cap free
Amount of items: 1
Items: 
Size: 614948 Color: 2

Bin 4871: 385096 of cap free
Amount of items: 1
Items: 
Size: 614905 Color: 0

Bin 4872: 385145 of cap free
Amount of items: 1
Items: 
Size: 614856 Color: 0

Bin 4873: 385183 of cap free
Amount of items: 1
Items: 
Size: 614818 Color: 2

Bin 4874: 385215 of cap free
Amount of items: 1
Items: 
Size: 614786 Color: 4

Bin 4875: 385331 of cap free
Amount of items: 1
Items: 
Size: 614670 Color: 2

Bin 4876: 385427 of cap free
Amount of items: 1
Items: 
Size: 614574 Color: 2

Bin 4877: 385468 of cap free
Amount of items: 1
Items: 
Size: 614533 Color: 4

Bin 4878: 385811 of cap free
Amount of items: 1
Items: 
Size: 614190 Color: 1

Bin 4879: 385943 of cap free
Amount of items: 1
Items: 
Size: 614058 Color: 1

Bin 4880: 385953 of cap free
Amount of items: 1
Items: 
Size: 614048 Color: 2

Bin 4881: 386006 of cap free
Amount of items: 1
Items: 
Size: 613995 Color: 1

Bin 4882: 386282 of cap free
Amount of items: 1
Items: 
Size: 613719 Color: 4

Bin 4883: 386285 of cap free
Amount of items: 1
Items: 
Size: 613716 Color: 1

Bin 4884: 386458 of cap free
Amount of items: 1
Items: 
Size: 613543 Color: 1

Bin 4885: 386464 of cap free
Amount of items: 1
Items: 
Size: 613537 Color: 1

Bin 4886: 386611 of cap free
Amount of items: 1
Items: 
Size: 613390 Color: 2

Bin 4887: 386726 of cap free
Amount of items: 1
Items: 
Size: 613275 Color: 4

Bin 4888: 386739 of cap free
Amount of items: 1
Items: 
Size: 613262 Color: 3

Bin 4889: 386797 of cap free
Amount of items: 1
Items: 
Size: 613204 Color: 2

Bin 4890: 386908 of cap free
Amount of items: 1
Items: 
Size: 613093 Color: 2

Bin 4891: 386947 of cap free
Amount of items: 1
Items: 
Size: 613054 Color: 0

Bin 4892: 386956 of cap free
Amount of items: 1
Items: 
Size: 613045 Color: 0

Bin 4893: 386989 of cap free
Amount of items: 1
Items: 
Size: 613012 Color: 3

Bin 4894: 387119 of cap free
Amount of items: 1
Items: 
Size: 612882 Color: 0

Bin 4895: 387189 of cap free
Amount of items: 1
Items: 
Size: 612812 Color: 3

Bin 4896: 387316 of cap free
Amount of items: 1
Items: 
Size: 612685 Color: 3

Bin 4897: 387470 of cap free
Amount of items: 1
Items: 
Size: 612531 Color: 3

Bin 4898: 387546 of cap free
Amount of items: 1
Items: 
Size: 612455 Color: 4

Bin 4899: 387746 of cap free
Amount of items: 1
Items: 
Size: 612255 Color: 2

Bin 4900: 387819 of cap free
Amount of items: 1
Items: 
Size: 612182 Color: 1

Bin 4901: 387835 of cap free
Amount of items: 1
Items: 
Size: 612166 Color: 4

Bin 4902: 387869 of cap free
Amount of items: 1
Items: 
Size: 612132 Color: 1

Bin 4903: 387897 of cap free
Amount of items: 1
Items: 
Size: 612104 Color: 2

Bin 4904: 387900 of cap free
Amount of items: 1
Items: 
Size: 612101 Color: 3

Bin 4905: 388110 of cap free
Amount of items: 1
Items: 
Size: 611891 Color: 4

Bin 4906: 388121 of cap free
Amount of items: 1
Items: 
Size: 611880 Color: 4

Bin 4907: 388127 of cap free
Amount of items: 1
Items: 
Size: 611874 Color: 0

Bin 4908: 388134 of cap free
Amount of items: 1
Items: 
Size: 611867 Color: 1

Bin 4909: 388246 of cap free
Amount of items: 1
Items: 
Size: 611755 Color: 1

Bin 4910: 388280 of cap free
Amount of items: 1
Items: 
Size: 611721 Color: 1

Bin 4911: 388313 of cap free
Amount of items: 1
Items: 
Size: 611688 Color: 3

Bin 4912: 388334 of cap free
Amount of items: 1
Items: 
Size: 611667 Color: 2

Bin 4913: 388338 of cap free
Amount of items: 1
Items: 
Size: 611663 Color: 4

Bin 4914: 388371 of cap free
Amount of items: 1
Items: 
Size: 611630 Color: 0

Bin 4915: 388397 of cap free
Amount of items: 1
Items: 
Size: 611604 Color: 3

Bin 4916: 388402 of cap free
Amount of items: 1
Items: 
Size: 611599 Color: 3

Bin 4917: 388403 of cap free
Amount of items: 1
Items: 
Size: 611598 Color: 4

Bin 4918: 388430 of cap free
Amount of items: 1
Items: 
Size: 611571 Color: 2

Bin 4919: 388506 of cap free
Amount of items: 1
Items: 
Size: 611495 Color: 2

Bin 4920: 388790 of cap free
Amount of items: 1
Items: 
Size: 611211 Color: 2

Bin 4921: 388820 of cap free
Amount of items: 1
Items: 
Size: 611181 Color: 2

Bin 4922: 388858 of cap free
Amount of items: 1
Items: 
Size: 611143 Color: 4

Bin 4923: 388887 of cap free
Amount of items: 1
Items: 
Size: 611114 Color: 4

Bin 4924: 389083 of cap free
Amount of items: 1
Items: 
Size: 610918 Color: 4

Bin 4925: 389154 of cap free
Amount of items: 1
Items: 
Size: 610847 Color: 0

Bin 4926: 389196 of cap free
Amount of items: 1
Items: 
Size: 610805 Color: 1

Bin 4927: 389238 of cap free
Amount of items: 1
Items: 
Size: 610763 Color: 2

Bin 4928: 389250 of cap free
Amount of items: 1
Items: 
Size: 610751 Color: 2

Bin 4929: 389255 of cap free
Amount of items: 1
Items: 
Size: 610746 Color: 4

Bin 4930: 389374 of cap free
Amount of items: 1
Items: 
Size: 610627 Color: 4

Bin 4931: 389381 of cap free
Amount of items: 1
Items: 
Size: 610620 Color: 0

Bin 4932: 389477 of cap free
Amount of items: 1
Items: 
Size: 610524 Color: 1

Bin 4933: 389516 of cap free
Amount of items: 1
Items: 
Size: 610485 Color: 4

Bin 4934: 389638 of cap free
Amount of items: 1
Items: 
Size: 610363 Color: 3

Bin 4935: 389653 of cap free
Amount of items: 1
Items: 
Size: 610348 Color: 3

Bin 4936: 389672 of cap free
Amount of items: 1
Items: 
Size: 610329 Color: 2

Bin 4937: 389678 of cap free
Amount of items: 1
Items: 
Size: 610323 Color: 1

Bin 4938: 389707 of cap free
Amount of items: 1
Items: 
Size: 610294 Color: 4

Bin 4939: 389730 of cap free
Amount of items: 1
Items: 
Size: 610271 Color: 1

Bin 4940: 389745 of cap free
Amount of items: 1
Items: 
Size: 610256 Color: 2

Bin 4941: 389816 of cap free
Amount of items: 1
Items: 
Size: 610185 Color: 3

Bin 4942: 389851 of cap free
Amount of items: 1
Items: 
Size: 610150 Color: 2

Bin 4943: 389857 of cap free
Amount of items: 1
Items: 
Size: 610144 Color: 3

Bin 4944: 389940 of cap free
Amount of items: 1
Items: 
Size: 610061 Color: 1

Bin 4945: 389983 of cap free
Amount of items: 1
Items: 
Size: 610018 Color: 0

Bin 4946: 390002 of cap free
Amount of items: 1
Items: 
Size: 609999 Color: 1

Bin 4947: 390033 of cap free
Amount of items: 1
Items: 
Size: 609968 Color: 2

Bin 4948: 390105 of cap free
Amount of items: 1
Items: 
Size: 609896 Color: 4

Bin 4949: 390120 of cap free
Amount of items: 1
Items: 
Size: 609881 Color: 1

Bin 4950: 390202 of cap free
Amount of items: 1
Items: 
Size: 609799 Color: 0

Bin 4951: 390203 of cap free
Amount of items: 1
Items: 
Size: 609798 Color: 1

Bin 4952: 390497 of cap free
Amount of items: 1
Items: 
Size: 609504 Color: 1

Bin 4953: 390555 of cap free
Amount of items: 1
Items: 
Size: 609446 Color: 1

Bin 4954: 390576 of cap free
Amount of items: 1
Items: 
Size: 609425 Color: 4

Bin 4955: 390683 of cap free
Amount of items: 1
Items: 
Size: 609318 Color: 4

Bin 4956: 390720 of cap free
Amount of items: 1
Items: 
Size: 609281 Color: 4

Bin 4957: 390916 of cap free
Amount of items: 1
Items: 
Size: 609085 Color: 0

Bin 4958: 390977 of cap free
Amount of items: 1
Items: 
Size: 609024 Color: 0

Bin 4959: 391235 of cap free
Amount of items: 1
Items: 
Size: 608766 Color: 4

Bin 4960: 391311 of cap free
Amount of items: 1
Items: 
Size: 608690 Color: 2

Bin 4961: 391335 of cap free
Amount of items: 1
Items: 
Size: 608666 Color: 4

Bin 4962: 391567 of cap free
Amount of items: 1
Items: 
Size: 608434 Color: 3

Bin 4963: 391614 of cap free
Amount of items: 1
Items: 
Size: 608387 Color: 3

Bin 4964: 391730 of cap free
Amount of items: 1
Items: 
Size: 608271 Color: 0

Bin 4965: 391742 of cap free
Amount of items: 1
Items: 
Size: 608259 Color: 2

Bin 4966: 391789 of cap free
Amount of items: 1
Items: 
Size: 608212 Color: 4

Bin 4967: 391815 of cap free
Amount of items: 1
Items: 
Size: 608186 Color: 0

Bin 4968: 391842 of cap free
Amount of items: 1
Items: 
Size: 608159 Color: 4

Bin 4969: 391866 of cap free
Amount of items: 1
Items: 
Size: 608135 Color: 3

Bin 4970: 391875 of cap free
Amount of items: 1
Items: 
Size: 608126 Color: 3

Bin 4971: 392007 of cap free
Amount of items: 1
Items: 
Size: 607994 Color: 1

Bin 4972: 392009 of cap free
Amount of items: 1
Items: 
Size: 607992 Color: 2

Bin 4973: 392210 of cap free
Amount of items: 1
Items: 
Size: 607791 Color: 1

Bin 4974: 392246 of cap free
Amount of items: 1
Items: 
Size: 607755 Color: 2

Bin 4975: 392286 of cap free
Amount of items: 1
Items: 
Size: 607715 Color: 4

Bin 4976: 392336 of cap free
Amount of items: 1
Items: 
Size: 607665 Color: 2

Bin 4977: 392371 of cap free
Amount of items: 1
Items: 
Size: 607630 Color: 3

Bin 4978: 392757 of cap free
Amount of items: 1
Items: 
Size: 607244 Color: 3

Bin 4979: 392910 of cap free
Amount of items: 1
Items: 
Size: 607091 Color: 1

Bin 4980: 392933 of cap free
Amount of items: 1
Items: 
Size: 607068 Color: 0

Bin 4981: 393075 of cap free
Amount of items: 1
Items: 
Size: 606926 Color: 0

Bin 4982: 393081 of cap free
Amount of items: 1
Items: 
Size: 606920 Color: 3

Bin 4983: 393148 of cap free
Amount of items: 1
Items: 
Size: 606853 Color: 0

Bin 4984: 393255 of cap free
Amount of items: 1
Items: 
Size: 606746 Color: 3

Bin 4985: 393271 of cap free
Amount of items: 1
Items: 
Size: 606730 Color: 2

Bin 4986: 393310 of cap free
Amount of items: 1
Items: 
Size: 606691 Color: 1

Bin 4987: 393336 of cap free
Amount of items: 1
Items: 
Size: 606665 Color: 1

Bin 4988: 393359 of cap free
Amount of items: 1
Items: 
Size: 606642 Color: 1

Bin 4989: 393370 of cap free
Amount of items: 1
Items: 
Size: 606631 Color: 1

Bin 4990: 393379 of cap free
Amount of items: 1
Items: 
Size: 606622 Color: 2

Bin 4991: 393393 of cap free
Amount of items: 1
Items: 
Size: 606608 Color: 0

Bin 4992: 393492 of cap free
Amount of items: 1
Items: 
Size: 606509 Color: 1

Bin 4993: 393547 of cap free
Amount of items: 1
Items: 
Size: 606454 Color: 1

Bin 4994: 393569 of cap free
Amount of items: 1
Items: 
Size: 606432 Color: 3

Bin 4995: 393617 of cap free
Amount of items: 1
Items: 
Size: 606384 Color: 2

Bin 4996: 393627 of cap free
Amount of items: 1
Items: 
Size: 606374 Color: 0

Bin 4997: 393678 of cap free
Amount of items: 1
Items: 
Size: 606323 Color: 4

Bin 4998: 393764 of cap free
Amount of items: 1
Items: 
Size: 606237 Color: 4

Bin 4999: 393796 of cap free
Amount of items: 1
Items: 
Size: 606205 Color: 2

Bin 5000: 393801 of cap free
Amount of items: 1
Items: 
Size: 606200 Color: 1

Bin 5001: 394063 of cap free
Amount of items: 1
Items: 
Size: 605938 Color: 1

Bin 5002: 394094 of cap free
Amount of items: 1
Items: 
Size: 605907 Color: 3

Bin 5003: 394121 of cap free
Amount of items: 1
Items: 
Size: 605880 Color: 0

Bin 5004: 394144 of cap free
Amount of items: 1
Items: 
Size: 605857 Color: 3

Bin 5005: 394151 of cap free
Amount of items: 1
Items: 
Size: 605850 Color: 0

Bin 5006: 394181 of cap free
Amount of items: 1
Items: 
Size: 605820 Color: 0

Bin 5007: 394323 of cap free
Amount of items: 1
Items: 
Size: 605678 Color: 0

Bin 5008: 394338 of cap free
Amount of items: 1
Items: 
Size: 605663 Color: 4

Bin 5009: 394359 of cap free
Amount of items: 1
Items: 
Size: 605642 Color: 0

Bin 5010: 394404 of cap free
Amount of items: 1
Items: 
Size: 605597 Color: 3

Bin 5011: 394415 of cap free
Amount of items: 1
Items: 
Size: 605586 Color: 2

Bin 5012: 394483 of cap free
Amount of items: 1
Items: 
Size: 605518 Color: 1

Bin 5013: 394484 of cap free
Amount of items: 1
Items: 
Size: 605517 Color: 0

Bin 5014: 394737 of cap free
Amount of items: 1
Items: 
Size: 605264 Color: 3

Bin 5015: 394827 of cap free
Amount of items: 1
Items: 
Size: 605174 Color: 3

Bin 5016: 395140 of cap free
Amount of items: 1
Items: 
Size: 604861 Color: 1

Bin 5017: 395187 of cap free
Amount of items: 1
Items: 
Size: 604814 Color: 2

Bin 5018: 395214 of cap free
Amount of items: 1
Items: 
Size: 604787 Color: 0

Bin 5019: 395221 of cap free
Amount of items: 1
Items: 
Size: 604780 Color: 0

Bin 5020: 395370 of cap free
Amount of items: 1
Items: 
Size: 604631 Color: 2

Bin 5021: 395428 of cap free
Amount of items: 1
Items: 
Size: 604573 Color: 1

Bin 5022: 395435 of cap free
Amount of items: 1
Items: 
Size: 604566 Color: 1

Bin 5023: 395503 of cap free
Amount of items: 1
Items: 
Size: 604498 Color: 2

Bin 5024: 395548 of cap free
Amount of items: 1
Items: 
Size: 604453 Color: 4

Bin 5025: 395679 of cap free
Amount of items: 1
Items: 
Size: 604322 Color: 2

Bin 5026: 395680 of cap free
Amount of items: 1
Items: 
Size: 604321 Color: 0

Bin 5027: 395760 of cap free
Amount of items: 1
Items: 
Size: 604241 Color: 0

Bin 5028: 395937 of cap free
Amount of items: 1
Items: 
Size: 604064 Color: 1

Bin 5029: 395989 of cap free
Amount of items: 1
Items: 
Size: 604012 Color: 1

Bin 5030: 396106 of cap free
Amount of items: 1
Items: 
Size: 603895 Color: 2

Bin 5031: 396346 of cap free
Amount of items: 1
Items: 
Size: 603655 Color: 3

Bin 5032: 396462 of cap free
Amount of items: 1
Items: 
Size: 603539 Color: 4

Bin 5033: 396520 of cap free
Amount of items: 1
Items: 
Size: 603481 Color: 4

Bin 5034: 396529 of cap free
Amount of items: 1
Items: 
Size: 603472 Color: 0

Bin 5035: 396541 of cap free
Amount of items: 1
Items: 
Size: 603460 Color: 2

Bin 5036: 396642 of cap free
Amount of items: 1
Items: 
Size: 603359 Color: 3

Bin 5037: 396669 of cap free
Amount of items: 1
Items: 
Size: 603332 Color: 2

Bin 5038: 396725 of cap free
Amount of items: 1
Items: 
Size: 603276 Color: 0

Bin 5039: 396795 of cap free
Amount of items: 1
Items: 
Size: 603206 Color: 1

Bin 5040: 396874 of cap free
Amount of items: 1
Items: 
Size: 603127 Color: 2

Bin 5041: 396948 of cap free
Amount of items: 1
Items: 
Size: 603053 Color: 2

Bin 5042: 396961 of cap free
Amount of items: 1
Items: 
Size: 603040 Color: 0

Bin 5043: 396965 of cap free
Amount of items: 1
Items: 
Size: 603036 Color: 4

Bin 5044: 397020 of cap free
Amount of items: 1
Items: 
Size: 602981 Color: 2

Bin 5045: 397074 of cap free
Amount of items: 1
Items: 
Size: 602927 Color: 4

Bin 5046: 397186 of cap free
Amount of items: 1
Items: 
Size: 602815 Color: 2

Bin 5047: 397221 of cap free
Amount of items: 1
Items: 
Size: 602780 Color: 1

Bin 5048: 397230 of cap free
Amount of items: 1
Items: 
Size: 602771 Color: 2

Bin 5049: 397382 of cap free
Amount of items: 1
Items: 
Size: 602619 Color: 4

Bin 5050: 397464 of cap free
Amount of items: 1
Items: 
Size: 602537 Color: 1

Bin 5051: 397606 of cap free
Amount of items: 1
Items: 
Size: 602395 Color: 1

Bin 5052: 397645 of cap free
Amount of items: 1
Items: 
Size: 602356 Color: 4

Bin 5053: 397696 of cap free
Amount of items: 1
Items: 
Size: 602305 Color: 3

Bin 5054: 397833 of cap free
Amount of items: 1
Items: 
Size: 602168 Color: 1

Bin 5055: 397885 of cap free
Amount of items: 1
Items: 
Size: 602116 Color: 0

Bin 5056: 397895 of cap free
Amount of items: 1
Items: 
Size: 602106 Color: 4

Bin 5057: 397957 of cap free
Amount of items: 1
Items: 
Size: 602044 Color: 2

Bin 5058: 397959 of cap free
Amount of items: 1
Items: 
Size: 602042 Color: 3

Bin 5059: 397962 of cap free
Amount of items: 1
Items: 
Size: 602039 Color: 1

Bin 5060: 397963 of cap free
Amount of items: 1
Items: 
Size: 602038 Color: 4

Bin 5061: 398009 of cap free
Amount of items: 1
Items: 
Size: 601992 Color: 1

Bin 5062: 398042 of cap free
Amount of items: 1
Items: 
Size: 601959 Color: 4

Bin 5063: 398123 of cap free
Amount of items: 1
Items: 
Size: 601878 Color: 0

Bin 5064: 398195 of cap free
Amount of items: 1
Items: 
Size: 601806 Color: 2

Bin 5065: 398210 of cap free
Amount of items: 1
Items: 
Size: 601791 Color: 0

Bin 5066: 398288 of cap free
Amount of items: 1
Items: 
Size: 601713 Color: 2

Bin 5067: 398329 of cap free
Amount of items: 1
Items: 
Size: 601672 Color: 0

Bin 5068: 398443 of cap free
Amount of items: 1
Items: 
Size: 601558 Color: 3

Bin 5069: 398483 of cap free
Amount of items: 1
Items: 
Size: 601518 Color: 1

Bin 5070: 398559 of cap free
Amount of items: 1
Items: 
Size: 601442 Color: 0

Bin 5071: 398583 of cap free
Amount of items: 1
Items: 
Size: 601418 Color: 2

Bin 5072: 398756 of cap free
Amount of items: 1
Items: 
Size: 601245 Color: 4

Bin 5073: 398774 of cap free
Amount of items: 1
Items: 
Size: 601227 Color: 0

Bin 5074: 398952 of cap free
Amount of items: 1
Items: 
Size: 601049 Color: 0

Bin 5075: 399000 of cap free
Amount of items: 1
Items: 
Size: 601001 Color: 0

Bin 5076: 399136 of cap free
Amount of items: 1
Items: 
Size: 600865 Color: 1

Bin 5077: 399188 of cap free
Amount of items: 1
Items: 
Size: 600813 Color: 1

Bin 5078: 399272 of cap free
Amount of items: 1
Items: 
Size: 600729 Color: 1

Bin 5079: 399333 of cap free
Amount of items: 1
Items: 
Size: 600668 Color: 1

Bin 5080: 399335 of cap free
Amount of items: 1
Items: 
Size: 600666 Color: 4

Bin 5081: 399442 of cap free
Amount of items: 1
Items: 
Size: 600559 Color: 2

Bin 5082: 399458 of cap free
Amount of items: 1
Items: 
Size: 600543 Color: 1

Bin 5083: 399502 of cap free
Amount of items: 1
Items: 
Size: 600499 Color: 1

Bin 5084: 399506 of cap free
Amount of items: 1
Items: 
Size: 600495 Color: 1

Bin 5085: 399607 of cap free
Amount of items: 1
Items: 
Size: 600394 Color: 3

Bin 5086: 399661 of cap free
Amount of items: 1
Items: 
Size: 600340 Color: 4

Bin 5087: 399682 of cap free
Amount of items: 1
Items: 
Size: 600319 Color: 1

Bin 5088: 399689 of cap free
Amount of items: 1
Items: 
Size: 600312 Color: 3

Bin 5089: 399826 of cap free
Amount of items: 1
Items: 
Size: 600175 Color: 4

Bin 5090: 399900 of cap free
Amount of items: 1
Items: 
Size: 600101 Color: 0

Bin 5091: 400003 of cap free
Amount of items: 1
Items: 
Size: 599998 Color: 2

Bin 5092: 400096 of cap free
Amount of items: 1
Items: 
Size: 599905 Color: 3

Bin 5093: 400177 of cap free
Amount of items: 1
Items: 
Size: 599824 Color: 4

Bin 5094: 400358 of cap free
Amount of items: 1
Items: 
Size: 599643 Color: 3

Bin 5095: 400359 of cap free
Amount of items: 1
Items: 
Size: 599642 Color: 4

Bin 5096: 400511 of cap free
Amount of items: 1
Items: 
Size: 599490 Color: 2

Bin 5097: 400546 of cap free
Amount of items: 1
Items: 
Size: 599455 Color: 2

Bin 5098: 400663 of cap free
Amount of items: 1
Items: 
Size: 599338 Color: 3

Bin 5099: 400709 of cap free
Amount of items: 1
Items: 
Size: 599292 Color: 2

Bin 5100: 400763 of cap free
Amount of items: 1
Items: 
Size: 599238 Color: 1

Bin 5101: 400764 of cap free
Amount of items: 1
Items: 
Size: 599237 Color: 2

Bin 5102: 400801 of cap free
Amount of items: 1
Items: 
Size: 599200 Color: 3

Bin 5103: 400814 of cap free
Amount of items: 1
Items: 
Size: 599187 Color: 3

Bin 5104: 400838 of cap free
Amount of items: 1
Items: 
Size: 599163 Color: 2

Bin 5105: 400965 of cap free
Amount of items: 1
Items: 
Size: 599036 Color: 4

Bin 5106: 400968 of cap free
Amount of items: 1
Items: 
Size: 599033 Color: 4

Bin 5107: 401239 of cap free
Amount of items: 1
Items: 
Size: 598762 Color: 1

Bin 5108: 401262 of cap free
Amount of items: 1
Items: 
Size: 598739 Color: 4

Bin 5109: 401277 of cap free
Amount of items: 1
Items: 
Size: 598724 Color: 4

Bin 5110: 401290 of cap free
Amount of items: 1
Items: 
Size: 598711 Color: 2

Bin 5111: 401336 of cap free
Amount of items: 1
Items: 
Size: 598665 Color: 0

Bin 5112: 401373 of cap free
Amount of items: 1
Items: 
Size: 598628 Color: 2

Bin 5113: 401377 of cap free
Amount of items: 1
Items: 
Size: 598624 Color: 2

Bin 5114: 401413 of cap free
Amount of items: 1
Items: 
Size: 598588 Color: 0

Bin 5115: 401500 of cap free
Amount of items: 1
Items: 
Size: 598501 Color: 4

Bin 5116: 401538 of cap free
Amount of items: 1
Items: 
Size: 598463 Color: 1

Bin 5117: 401579 of cap free
Amount of items: 1
Items: 
Size: 598422 Color: 1

Bin 5118: 401613 of cap free
Amount of items: 1
Items: 
Size: 598388 Color: 4

Bin 5119: 401645 of cap free
Amount of items: 1
Items: 
Size: 598356 Color: 0

Bin 5120: 401785 of cap free
Amount of items: 1
Items: 
Size: 598216 Color: 1

Bin 5121: 401852 of cap free
Amount of items: 1
Items: 
Size: 598149 Color: 3

Bin 5122: 401945 of cap free
Amount of items: 1
Items: 
Size: 598056 Color: 1

Bin 5123: 402060 of cap free
Amount of items: 1
Items: 
Size: 597941 Color: 1

Bin 5124: 402164 of cap free
Amount of items: 1
Items: 
Size: 597837 Color: 4

Bin 5125: 402260 of cap free
Amount of items: 1
Items: 
Size: 597741 Color: 4

Bin 5126: 402314 of cap free
Amount of items: 1
Items: 
Size: 597687 Color: 3

Bin 5127: 402332 of cap free
Amount of items: 1
Items: 
Size: 597669 Color: 4

Bin 5128: 402386 of cap free
Amount of items: 1
Items: 
Size: 597615 Color: 1

Bin 5129: 402438 of cap free
Amount of items: 1
Items: 
Size: 597563 Color: 4

Bin 5130: 402525 of cap free
Amount of items: 1
Items: 
Size: 597476 Color: 3

Bin 5131: 402545 of cap free
Amount of items: 1
Items: 
Size: 597456 Color: 1

Bin 5132: 402640 of cap free
Amount of items: 1
Items: 
Size: 597361 Color: 4

Bin 5133: 402650 of cap free
Amount of items: 1
Items: 
Size: 597351 Color: 1

Bin 5134: 402698 of cap free
Amount of items: 1
Items: 
Size: 597303 Color: 3

Bin 5135: 402739 of cap free
Amount of items: 1
Items: 
Size: 597262 Color: 0

Bin 5136: 402784 of cap free
Amount of items: 1
Items: 
Size: 597217 Color: 2

Bin 5137: 402789 of cap free
Amount of items: 1
Items: 
Size: 597212 Color: 0

Bin 5138: 402829 of cap free
Amount of items: 1
Items: 
Size: 597172 Color: 1

Bin 5139: 402895 of cap free
Amount of items: 1
Items: 
Size: 597106 Color: 4

Bin 5140: 402955 of cap free
Amount of items: 1
Items: 
Size: 597046 Color: 1

Bin 5141: 402986 of cap free
Amount of items: 1
Items: 
Size: 597015 Color: 2

Bin 5142: 403138 of cap free
Amount of items: 1
Items: 
Size: 596863 Color: 1

Bin 5143: 403168 of cap free
Amount of items: 1
Items: 
Size: 596833 Color: 1

Bin 5144: 403172 of cap free
Amount of items: 1
Items: 
Size: 596829 Color: 0

Bin 5145: 403217 of cap free
Amount of items: 1
Items: 
Size: 596784 Color: 1

Bin 5146: 403336 of cap free
Amount of items: 1
Items: 
Size: 596665 Color: 0

Bin 5147: 403493 of cap free
Amount of items: 1
Items: 
Size: 596508 Color: 2

Bin 5148: 403605 of cap free
Amount of items: 1
Items: 
Size: 596396 Color: 4

Bin 5149: 403624 of cap free
Amount of items: 1
Items: 
Size: 596377 Color: 2

Bin 5150: 403631 of cap free
Amount of items: 1
Items: 
Size: 596370 Color: 3

Bin 5151: 403789 of cap free
Amount of items: 1
Items: 
Size: 596212 Color: 3

Bin 5152: 403931 of cap free
Amount of items: 1
Items: 
Size: 596070 Color: 4

Bin 5153: 403962 of cap free
Amount of items: 1
Items: 
Size: 596039 Color: 1

Bin 5154: 404141 of cap free
Amount of items: 1
Items: 
Size: 595860 Color: 4

Bin 5155: 404170 of cap free
Amount of items: 1
Items: 
Size: 595831 Color: 3

Bin 5156: 404294 of cap free
Amount of items: 1
Items: 
Size: 595707 Color: 4

Bin 5157: 404326 of cap free
Amount of items: 1
Items: 
Size: 595675 Color: 4

Bin 5158: 404450 of cap free
Amount of items: 1
Items: 
Size: 595551 Color: 4

Bin 5159: 404462 of cap free
Amount of items: 1
Items: 
Size: 595539 Color: 4

Bin 5160: 404546 of cap free
Amount of items: 1
Items: 
Size: 595455 Color: 4

Bin 5161: 404645 of cap free
Amount of items: 1
Items: 
Size: 595356 Color: 1

Bin 5162: 404766 of cap free
Amount of items: 1
Items: 
Size: 595235 Color: 3

Bin 5163: 404773 of cap free
Amount of items: 1
Items: 
Size: 595228 Color: 2

Bin 5164: 404926 of cap free
Amount of items: 1
Items: 
Size: 595075 Color: 0

Bin 5165: 404935 of cap free
Amount of items: 1
Items: 
Size: 595066 Color: 3

Bin 5166: 404948 of cap free
Amount of items: 1
Items: 
Size: 595053 Color: 0

Bin 5167: 405021 of cap free
Amount of items: 1
Items: 
Size: 594980 Color: 2

Bin 5168: 405058 of cap free
Amount of items: 1
Items: 
Size: 594943 Color: 2

Bin 5169: 405065 of cap free
Amount of items: 1
Items: 
Size: 594936 Color: 4

Bin 5170: 405128 of cap free
Amount of items: 1
Items: 
Size: 594873 Color: 2

Bin 5171: 405196 of cap free
Amount of items: 1
Items: 
Size: 594805 Color: 0

Bin 5172: 405230 of cap free
Amount of items: 1
Items: 
Size: 594771 Color: 2

Bin 5173: 405339 of cap free
Amount of items: 1
Items: 
Size: 594662 Color: 1

Bin 5174: 405350 of cap free
Amount of items: 1
Items: 
Size: 594651 Color: 1

Bin 5175: 405458 of cap free
Amount of items: 1
Items: 
Size: 594543 Color: 4

Bin 5176: 405498 of cap free
Amount of items: 1
Items: 
Size: 594503 Color: 3

Bin 5177: 405521 of cap free
Amount of items: 1
Items: 
Size: 594480 Color: 0

Bin 5178: 405552 of cap free
Amount of items: 1
Items: 
Size: 594449 Color: 0

Bin 5179: 405658 of cap free
Amount of items: 1
Items: 
Size: 594343 Color: 3

Bin 5180: 405780 of cap free
Amount of items: 1
Items: 
Size: 594221 Color: 3

Bin 5181: 405975 of cap free
Amount of items: 1
Items: 
Size: 594026 Color: 2

Bin 5182: 406023 of cap free
Amount of items: 1
Items: 
Size: 593978 Color: 4

Bin 5183: 406164 of cap free
Amount of items: 1
Items: 
Size: 593837 Color: 0

Bin 5184: 406335 of cap free
Amount of items: 1
Items: 
Size: 593666 Color: 2

Bin 5185: 406368 of cap free
Amount of items: 1
Items: 
Size: 593633 Color: 3

Bin 5186: 406502 of cap free
Amount of items: 1
Items: 
Size: 593499 Color: 2

Bin 5187: 406706 of cap free
Amount of items: 1
Items: 
Size: 593295 Color: 0

Bin 5188: 406712 of cap free
Amount of items: 1
Items: 
Size: 593289 Color: 4

Bin 5189: 406884 of cap free
Amount of items: 1
Items: 
Size: 593117 Color: 1

Bin 5190: 406899 of cap free
Amount of items: 1
Items: 
Size: 593102 Color: 2

Bin 5191: 406945 of cap free
Amount of items: 1
Items: 
Size: 593056 Color: 3

Bin 5192: 407032 of cap free
Amount of items: 1
Items: 
Size: 592969 Color: 1

Bin 5193: 407089 of cap free
Amount of items: 1
Items: 
Size: 592912 Color: 1

Bin 5194: 407157 of cap free
Amount of items: 1
Items: 
Size: 592844 Color: 1

Bin 5195: 407164 of cap free
Amount of items: 1
Items: 
Size: 592837 Color: 1

Bin 5196: 407310 of cap free
Amount of items: 1
Items: 
Size: 592691 Color: 4

Bin 5197: 407318 of cap free
Amount of items: 1
Items: 
Size: 592683 Color: 1

Bin 5198: 407318 of cap free
Amount of items: 1
Items: 
Size: 592683 Color: 3

Bin 5199: 407510 of cap free
Amount of items: 1
Items: 
Size: 592491 Color: 0

Bin 5200: 407524 of cap free
Amount of items: 1
Items: 
Size: 592477 Color: 3

Bin 5201: 407610 of cap free
Amount of items: 1
Items: 
Size: 592391 Color: 4

Bin 5202: 407667 of cap free
Amount of items: 1
Items: 
Size: 592334 Color: 3

Bin 5203: 407787 of cap free
Amount of items: 1
Items: 
Size: 592214 Color: 1

Bin 5204: 407844 of cap free
Amount of items: 1
Items: 
Size: 592157 Color: 2

Bin 5205: 407894 of cap free
Amount of items: 1
Items: 
Size: 592107 Color: 3

Bin 5206: 408046 of cap free
Amount of items: 1
Items: 
Size: 591955 Color: 4

Bin 5207: 408146 of cap free
Amount of items: 1
Items: 
Size: 591855 Color: 3

Bin 5208: 408177 of cap free
Amount of items: 1
Items: 
Size: 591824 Color: 4

Bin 5209: 408188 of cap free
Amount of items: 1
Items: 
Size: 591813 Color: 3

Bin 5210: 408337 of cap free
Amount of items: 1
Items: 
Size: 591664 Color: 4

Bin 5211: 408365 of cap free
Amount of items: 1
Items: 
Size: 591636 Color: 0

Bin 5212: 408375 of cap free
Amount of items: 1
Items: 
Size: 591626 Color: 2

Bin 5213: 408445 of cap free
Amount of items: 1
Items: 
Size: 591556 Color: 0

Bin 5214: 408519 of cap free
Amount of items: 1
Items: 
Size: 591482 Color: 1

Bin 5215: 408558 of cap free
Amount of items: 1
Items: 
Size: 591443 Color: 2

Bin 5216: 408570 of cap free
Amount of items: 1
Items: 
Size: 591431 Color: 1

Bin 5217: 408702 of cap free
Amount of items: 1
Items: 
Size: 591299 Color: 3

Bin 5218: 408707 of cap free
Amount of items: 1
Items: 
Size: 591294 Color: 0

Bin 5219: 408725 of cap free
Amount of items: 1
Items: 
Size: 591276 Color: 2

Bin 5220: 408764 of cap free
Amount of items: 1
Items: 
Size: 591237 Color: 0

Bin 5221: 408785 of cap free
Amount of items: 1
Items: 
Size: 591216 Color: 2

Bin 5222: 408796 of cap free
Amount of items: 1
Items: 
Size: 591205 Color: 0

Bin 5223: 408882 of cap free
Amount of items: 1
Items: 
Size: 591119 Color: 3

Bin 5224: 408976 of cap free
Amount of items: 1
Items: 
Size: 591025 Color: 2

Bin 5225: 409100 of cap free
Amount of items: 1
Items: 
Size: 590901 Color: 4

Bin 5226: 409204 of cap free
Amount of items: 1
Items: 
Size: 590797 Color: 0

Bin 5227: 409211 of cap free
Amount of items: 1
Items: 
Size: 590790 Color: 0

Bin 5228: 409313 of cap free
Amount of items: 1
Items: 
Size: 590688 Color: 4

Bin 5229: 409487 of cap free
Amount of items: 1
Items: 
Size: 590514 Color: 2

Bin 5230: 409534 of cap free
Amount of items: 1
Items: 
Size: 590467 Color: 1

Bin 5231: 409630 of cap free
Amount of items: 1
Items: 
Size: 590371 Color: 2

Bin 5232: 409636 of cap free
Amount of items: 1
Items: 
Size: 590365 Color: 4

Bin 5233: 409651 of cap free
Amount of items: 1
Items: 
Size: 590350 Color: 2

Bin 5234: 409803 of cap free
Amount of items: 1
Items: 
Size: 590198 Color: 4

Bin 5235: 409921 of cap free
Amount of items: 1
Items: 
Size: 590080 Color: 0

Bin 5236: 409993 of cap free
Amount of items: 1
Items: 
Size: 590008 Color: 0

Bin 5237: 410074 of cap free
Amount of items: 1
Items: 
Size: 589927 Color: 2

Bin 5238: 410153 of cap free
Amount of items: 1
Items: 
Size: 589848 Color: 1

Bin 5239: 410183 of cap free
Amount of items: 1
Items: 
Size: 589818 Color: 4

Bin 5240: 410213 of cap free
Amount of items: 1
Items: 
Size: 589788 Color: 3

Bin 5241: 410236 of cap free
Amount of items: 1
Items: 
Size: 589765 Color: 4

Bin 5242: 410278 of cap free
Amount of items: 1
Items: 
Size: 589723 Color: 1

Bin 5243: 410393 of cap free
Amount of items: 1
Items: 
Size: 589608 Color: 1

Bin 5244: 410437 of cap free
Amount of items: 1
Items: 
Size: 589564 Color: 3

Bin 5245: 410461 of cap free
Amount of items: 1
Items: 
Size: 589540 Color: 3

Bin 5246: 410510 of cap free
Amount of items: 1
Items: 
Size: 589491 Color: 3

Bin 5247: 410703 of cap free
Amount of items: 1
Items: 
Size: 589298 Color: 0

Bin 5248: 410886 of cap free
Amount of items: 1
Items: 
Size: 589115 Color: 1

Bin 5249: 410962 of cap free
Amount of items: 1
Items: 
Size: 589039 Color: 0

Bin 5250: 411052 of cap free
Amount of items: 1
Items: 
Size: 588949 Color: 0

Bin 5251: 411084 of cap free
Amount of items: 1
Items: 
Size: 588917 Color: 1

Bin 5252: 411230 of cap free
Amount of items: 1
Items: 
Size: 588771 Color: 2

Bin 5253: 411265 of cap free
Amount of items: 1
Items: 
Size: 588736 Color: 2

Bin 5254: 411362 of cap free
Amount of items: 1
Items: 
Size: 588639 Color: 1

Bin 5255: 411577 of cap free
Amount of items: 1
Items: 
Size: 588424 Color: 0

Bin 5256: 411602 of cap free
Amount of items: 1
Items: 
Size: 588399 Color: 4

Bin 5257: 411703 of cap free
Amount of items: 1
Items: 
Size: 588298 Color: 0

Bin 5258: 411722 of cap free
Amount of items: 1
Items: 
Size: 588279 Color: 2

Bin 5259: 411732 of cap free
Amount of items: 1
Items: 
Size: 588269 Color: 2

Bin 5260: 412011 of cap free
Amount of items: 1
Items: 
Size: 587990 Color: 3

Bin 5261: 412021 of cap free
Amount of items: 1
Items: 
Size: 587980 Color: 3

Bin 5262: 412036 of cap free
Amount of items: 1
Items: 
Size: 587965 Color: 4

Bin 5263: 412122 of cap free
Amount of items: 1
Items: 
Size: 587879 Color: 0

Bin 5264: 412301 of cap free
Amount of items: 1
Items: 
Size: 587700 Color: 0

Bin 5265: 412311 of cap free
Amount of items: 1
Items: 
Size: 587690 Color: 0

Bin 5266: 412318 of cap free
Amount of items: 1
Items: 
Size: 587683 Color: 1

Bin 5267: 412333 of cap free
Amount of items: 1
Items: 
Size: 587668 Color: 0

Bin 5268: 412426 of cap free
Amount of items: 1
Items: 
Size: 587575 Color: 0

Bin 5269: 412429 of cap free
Amount of items: 1
Items: 
Size: 587572 Color: 1

Bin 5270: 412568 of cap free
Amount of items: 1
Items: 
Size: 587433 Color: 4

Bin 5271: 412612 of cap free
Amount of items: 1
Items: 
Size: 587389 Color: 3

Bin 5272: 412651 of cap free
Amount of items: 1
Items: 
Size: 587350 Color: 3

Bin 5273: 412671 of cap free
Amount of items: 1
Items: 
Size: 587330 Color: 3

Bin 5274: 412731 of cap free
Amount of items: 1
Items: 
Size: 587270 Color: 2

Bin 5275: 412749 of cap free
Amount of items: 1
Items: 
Size: 587252 Color: 0

Bin 5276: 412760 of cap free
Amount of items: 1
Items: 
Size: 587241 Color: 2

Bin 5277: 413037 of cap free
Amount of items: 1
Items: 
Size: 586964 Color: 4

Bin 5278: 413052 of cap free
Amount of items: 1
Items: 
Size: 586949 Color: 2

Bin 5279: 413095 of cap free
Amount of items: 1
Items: 
Size: 586906 Color: 3

Bin 5280: 413422 of cap free
Amount of items: 1
Items: 
Size: 586579 Color: 0

Bin 5281: 413541 of cap free
Amount of items: 1
Items: 
Size: 586460 Color: 1

Bin 5282: 413543 of cap free
Amount of items: 1
Items: 
Size: 586458 Color: 1

Bin 5283: 413652 of cap free
Amount of items: 1
Items: 
Size: 586349 Color: 3

Bin 5284: 413849 of cap free
Amount of items: 1
Items: 
Size: 586152 Color: 2

Bin 5285: 413901 of cap free
Amount of items: 1
Items: 
Size: 586100 Color: 4

Bin 5286: 413912 of cap free
Amount of items: 1
Items: 
Size: 586089 Color: 1

Bin 5287: 413948 of cap free
Amount of items: 1
Items: 
Size: 586053 Color: 4

Bin 5288: 413954 of cap free
Amount of items: 1
Items: 
Size: 586047 Color: 4

Bin 5289: 414008 of cap free
Amount of items: 1
Items: 
Size: 585993 Color: 4

Bin 5290: 414025 of cap free
Amount of items: 1
Items: 
Size: 585976 Color: 2

Bin 5291: 414090 of cap free
Amount of items: 1
Items: 
Size: 585911 Color: 0

Bin 5292: 414119 of cap free
Amount of items: 1
Items: 
Size: 585882 Color: 0

Bin 5293: 414139 of cap free
Amount of items: 1
Items: 
Size: 585862 Color: 2

Bin 5294: 414224 of cap free
Amount of items: 1
Items: 
Size: 585777 Color: 0

Bin 5295: 414236 of cap free
Amount of items: 1
Items: 
Size: 585765 Color: 4

Bin 5296: 414306 of cap free
Amount of items: 1
Items: 
Size: 585695 Color: 1

Bin 5297: 414454 of cap free
Amount of items: 1
Items: 
Size: 585547 Color: 0

Bin 5298: 414463 of cap free
Amount of items: 1
Items: 
Size: 585538 Color: 1

Bin 5299: 414493 of cap free
Amount of items: 1
Items: 
Size: 585508 Color: 2

Bin 5300: 414546 of cap free
Amount of items: 1
Items: 
Size: 585455 Color: 2

Bin 5301: 414589 of cap free
Amount of items: 1
Items: 
Size: 585412 Color: 1

Bin 5302: 414645 of cap free
Amount of items: 1
Items: 
Size: 585356 Color: 3

Bin 5303: 414770 of cap free
Amount of items: 1
Items: 
Size: 585231 Color: 2

Bin 5304: 414806 of cap free
Amount of items: 1
Items: 
Size: 585195 Color: 0

Bin 5305: 414926 of cap free
Amount of items: 1
Items: 
Size: 585075 Color: 0

Bin 5306: 414930 of cap free
Amount of items: 1
Items: 
Size: 585071 Color: 2

Bin 5307: 414946 of cap free
Amount of items: 1
Items: 
Size: 585055 Color: 2

Bin 5308: 415125 of cap free
Amount of items: 1
Items: 
Size: 584876 Color: 3

Bin 5309: 415241 of cap free
Amount of items: 1
Items: 
Size: 584760 Color: 3

Bin 5310: 415283 of cap free
Amount of items: 1
Items: 
Size: 584718 Color: 0

Bin 5311: 415311 of cap free
Amount of items: 1
Items: 
Size: 584690 Color: 2

Bin 5312: 415459 of cap free
Amount of items: 1
Items: 
Size: 584542 Color: 2

Bin 5313: 415492 of cap free
Amount of items: 1
Items: 
Size: 584509 Color: 3

Bin 5314: 415538 of cap free
Amount of items: 1
Items: 
Size: 584463 Color: 4

Bin 5315: 415584 of cap free
Amount of items: 1
Items: 
Size: 584417 Color: 2

Bin 5316: 415839 of cap free
Amount of items: 1
Items: 
Size: 584162 Color: 1

Bin 5317: 415913 of cap free
Amount of items: 1
Items: 
Size: 584088 Color: 0

Bin 5318: 416021 of cap free
Amount of items: 1
Items: 
Size: 583980 Color: 1

Bin 5319: 416021 of cap free
Amount of items: 1
Items: 
Size: 583980 Color: 2

Bin 5320: 416056 of cap free
Amount of items: 1
Items: 
Size: 583945 Color: 4

Bin 5321: 416071 of cap free
Amount of items: 1
Items: 
Size: 583930 Color: 3

Bin 5322: 416089 of cap free
Amount of items: 1
Items: 
Size: 583912 Color: 2

Bin 5323: 416093 of cap free
Amount of items: 1
Items: 
Size: 583908 Color: 3

Bin 5324: 416159 of cap free
Amount of items: 1
Items: 
Size: 583842 Color: 4

Bin 5325: 416184 of cap free
Amount of items: 1
Items: 
Size: 583817 Color: 3

Bin 5326: 416235 of cap free
Amount of items: 1
Items: 
Size: 583766 Color: 2

Bin 5327: 416252 of cap free
Amount of items: 1
Items: 
Size: 583749 Color: 2

Bin 5328: 416375 of cap free
Amount of items: 1
Items: 
Size: 583626 Color: 2

Bin 5329: 416491 of cap free
Amount of items: 1
Items: 
Size: 583510 Color: 1

Bin 5330: 416537 of cap free
Amount of items: 1
Items: 
Size: 583464 Color: 4

Bin 5331: 416581 of cap free
Amount of items: 1
Items: 
Size: 583420 Color: 0

Bin 5332: 416707 of cap free
Amount of items: 1
Items: 
Size: 583294 Color: 3

Bin 5333: 416714 of cap free
Amount of items: 1
Items: 
Size: 583287 Color: 4

Bin 5334: 416795 of cap free
Amount of items: 1
Items: 
Size: 583206 Color: 2

Bin 5335: 416866 of cap free
Amount of items: 1
Items: 
Size: 583135 Color: 2

Bin 5336: 416887 of cap free
Amount of items: 1
Items: 
Size: 583114 Color: 0

Bin 5337: 416923 of cap free
Amount of items: 1
Items: 
Size: 583078 Color: 0

Bin 5338: 416970 of cap free
Amount of items: 1
Items: 
Size: 583031 Color: 3

Bin 5339: 416986 of cap free
Amount of items: 1
Items: 
Size: 583015 Color: 3

Bin 5340: 417047 of cap free
Amount of items: 1
Items: 
Size: 582954 Color: 2

Bin 5341: 417122 of cap free
Amount of items: 1
Items: 
Size: 582879 Color: 2

Bin 5342: 417201 of cap free
Amount of items: 1
Items: 
Size: 582800 Color: 0

Bin 5343: 417294 of cap free
Amount of items: 1
Items: 
Size: 582707 Color: 2

Bin 5344: 417533 of cap free
Amount of items: 1
Items: 
Size: 582468 Color: 2

Bin 5345: 417550 of cap free
Amount of items: 1
Items: 
Size: 582451 Color: 4

Bin 5346: 417556 of cap free
Amount of items: 1
Items: 
Size: 582445 Color: 4

Bin 5347: 417602 of cap free
Amount of items: 1
Items: 
Size: 582399 Color: 0

Bin 5348: 417638 of cap free
Amount of items: 1
Items: 
Size: 582363 Color: 1

Bin 5349: 417707 of cap free
Amount of items: 1
Items: 
Size: 582294 Color: 3

Bin 5350: 417715 of cap free
Amount of items: 1
Items: 
Size: 582286 Color: 0

Bin 5351: 417744 of cap free
Amount of items: 1
Items: 
Size: 582257 Color: 2

Bin 5352: 417887 of cap free
Amount of items: 1
Items: 
Size: 582114 Color: 0

Bin 5353: 418024 of cap free
Amount of items: 1
Items: 
Size: 581977 Color: 3

Bin 5354: 418066 of cap free
Amount of items: 1
Items: 
Size: 581935 Color: 2

Bin 5355: 418468 of cap free
Amount of items: 1
Items: 
Size: 581533 Color: 0

Bin 5356: 418496 of cap free
Amount of items: 1
Items: 
Size: 581505 Color: 0

Bin 5357: 418517 of cap free
Amount of items: 1
Items: 
Size: 581484 Color: 0

Bin 5358: 418627 of cap free
Amount of items: 1
Items: 
Size: 581374 Color: 2

Bin 5359: 418730 of cap free
Amount of items: 1
Items: 
Size: 581271 Color: 4

Bin 5360: 418956 of cap free
Amount of items: 1
Items: 
Size: 581045 Color: 1

Bin 5361: 418964 of cap free
Amount of items: 1
Items: 
Size: 581037 Color: 1

Bin 5362: 419041 of cap free
Amount of items: 1
Items: 
Size: 580960 Color: 4

Bin 5363: 419067 of cap free
Amount of items: 1
Items: 
Size: 580934 Color: 4

Bin 5364: 419070 of cap free
Amount of items: 1
Items: 
Size: 580931 Color: 0

Bin 5365: 419091 of cap free
Amount of items: 1
Items: 
Size: 580910 Color: 4

Bin 5366: 419139 of cap free
Amount of items: 1
Items: 
Size: 580862 Color: 0

Bin 5367: 419187 of cap free
Amount of items: 1
Items: 
Size: 580814 Color: 4

Bin 5368: 419242 of cap free
Amount of items: 1
Items: 
Size: 580759 Color: 1

Bin 5369: 419262 of cap free
Amount of items: 1
Items: 
Size: 580739 Color: 3

Bin 5370: 419377 of cap free
Amount of items: 1
Items: 
Size: 580624 Color: 1

Bin 5371: 419431 of cap free
Amount of items: 1
Items: 
Size: 580570 Color: 2

Bin 5372: 419530 of cap free
Amount of items: 1
Items: 
Size: 580471 Color: 3

Bin 5373: 419578 of cap free
Amount of items: 1
Items: 
Size: 580423 Color: 3

Bin 5374: 419636 of cap free
Amount of items: 1
Items: 
Size: 580365 Color: 0

Bin 5375: 419671 of cap free
Amount of items: 1
Items: 
Size: 580330 Color: 0

Bin 5376: 419719 of cap free
Amount of items: 1
Items: 
Size: 580282 Color: 4

Bin 5377: 419754 of cap free
Amount of items: 1
Items: 
Size: 580247 Color: 0

Bin 5378: 419807 of cap free
Amount of items: 1
Items: 
Size: 580194 Color: 0

Bin 5379: 419827 of cap free
Amount of items: 1
Items: 
Size: 580174 Color: 0

Bin 5380: 419919 of cap free
Amount of items: 1
Items: 
Size: 580082 Color: 0

Bin 5381: 419928 of cap free
Amount of items: 1
Items: 
Size: 580073 Color: 3

Bin 5382: 419941 of cap free
Amount of items: 1
Items: 
Size: 580060 Color: 0

Bin 5383: 420023 of cap free
Amount of items: 1
Items: 
Size: 579978 Color: 2

Bin 5384: 420054 of cap free
Amount of items: 1
Items: 
Size: 579947 Color: 1

Bin 5385: 420087 of cap free
Amount of items: 1
Items: 
Size: 579914 Color: 3

Bin 5386: 420306 of cap free
Amount of items: 1
Items: 
Size: 579695 Color: 2

Bin 5387: 420338 of cap free
Amount of items: 1
Items: 
Size: 579663 Color: 1

Bin 5388: 420341 of cap free
Amount of items: 1
Items: 
Size: 579660 Color: 0

Bin 5389: 420404 of cap free
Amount of items: 1
Items: 
Size: 579597 Color: 4

Bin 5390: 420468 of cap free
Amount of items: 1
Items: 
Size: 579533 Color: 3

Bin 5391: 420516 of cap free
Amount of items: 1
Items: 
Size: 579485 Color: 3

Bin 5392: 420580 of cap free
Amount of items: 1
Items: 
Size: 579421 Color: 1

Bin 5393: 420623 of cap free
Amount of items: 1
Items: 
Size: 579378 Color: 4

Bin 5394: 420662 of cap free
Amount of items: 1
Items: 
Size: 579339 Color: 2

Bin 5395: 420738 of cap free
Amount of items: 1
Items: 
Size: 579263 Color: 1

Bin 5396: 420870 of cap free
Amount of items: 1
Items: 
Size: 579131 Color: 0

Bin 5397: 420927 of cap free
Amount of items: 1
Items: 
Size: 579074 Color: 3

Bin 5398: 421012 of cap free
Amount of items: 1
Items: 
Size: 578989 Color: 4

Bin 5399: 421074 of cap free
Amount of items: 1
Items: 
Size: 578927 Color: 1

Bin 5400: 421171 of cap free
Amount of items: 1
Items: 
Size: 578830 Color: 1

Bin 5401: 421250 of cap free
Amount of items: 1
Items: 
Size: 578751 Color: 1

Bin 5402: 421264 of cap free
Amount of items: 1
Items: 
Size: 578737 Color: 4

Bin 5403: 421267 of cap free
Amount of items: 1
Items: 
Size: 578734 Color: 0

Bin 5404: 421341 of cap free
Amount of items: 1
Items: 
Size: 578660 Color: 0

Bin 5405: 421439 of cap free
Amount of items: 1
Items: 
Size: 578562 Color: 0

Bin 5406: 421512 of cap free
Amount of items: 1
Items: 
Size: 578489 Color: 0

Bin 5407: 421603 of cap free
Amount of items: 1
Items: 
Size: 578398 Color: 2

Bin 5408: 421616 of cap free
Amount of items: 1
Items: 
Size: 578385 Color: 2

Bin 5409: 421703 of cap free
Amount of items: 1
Items: 
Size: 578298 Color: 2

Bin 5410: 421757 of cap free
Amount of items: 1
Items: 
Size: 578244 Color: 2

Bin 5411: 421800 of cap free
Amount of items: 1
Items: 
Size: 578201 Color: 0

Bin 5412: 421802 of cap free
Amount of items: 1
Items: 
Size: 578199 Color: 3

Bin 5413: 421950 of cap free
Amount of items: 1
Items: 
Size: 578051 Color: 3

Bin 5414: 422096 of cap free
Amount of items: 1
Items: 
Size: 577905 Color: 1

Bin 5415: 422221 of cap free
Amount of items: 1
Items: 
Size: 577780 Color: 3

Bin 5416: 422223 of cap free
Amount of items: 1
Items: 
Size: 577778 Color: 0

Bin 5417: 422224 of cap free
Amount of items: 1
Items: 
Size: 577777 Color: 2

Bin 5418: 422278 of cap free
Amount of items: 1
Items: 
Size: 577723 Color: 2

Bin 5419: 422286 of cap free
Amount of items: 1
Items: 
Size: 577715 Color: 4

Bin 5420: 422338 of cap free
Amount of items: 1
Items: 
Size: 577663 Color: 4

Bin 5421: 422366 of cap free
Amount of items: 1
Items: 
Size: 577635 Color: 3

Bin 5422: 422406 of cap free
Amount of items: 1
Items: 
Size: 577595 Color: 0

Bin 5423: 422538 of cap free
Amount of items: 1
Items: 
Size: 577463 Color: 1

Bin 5424: 422539 of cap free
Amount of items: 1
Items: 
Size: 577462 Color: 3

Bin 5425: 422684 of cap free
Amount of items: 1
Items: 
Size: 577317 Color: 2

Bin 5426: 422786 of cap free
Amount of items: 1
Items: 
Size: 577215 Color: 4

Bin 5427: 422789 of cap free
Amount of items: 1
Items: 
Size: 577212 Color: 4

Bin 5428: 422832 of cap free
Amount of items: 1
Items: 
Size: 577169 Color: 0

Bin 5429: 423072 of cap free
Amount of items: 1
Items: 
Size: 576929 Color: 4

Bin 5430: 423082 of cap free
Amount of items: 1
Items: 
Size: 576919 Color: 2

Bin 5431: 423158 of cap free
Amount of items: 1
Items: 
Size: 576843 Color: 3

Bin 5432: 423343 of cap free
Amount of items: 1
Items: 
Size: 576658 Color: 4

Bin 5433: 423435 of cap free
Amount of items: 1
Items: 
Size: 576566 Color: 3

Bin 5434: 423463 of cap free
Amount of items: 1
Items: 
Size: 576538 Color: 4

Bin 5435: 423500 of cap free
Amount of items: 1
Items: 
Size: 576501 Color: 2

Bin 5436: 423560 of cap free
Amount of items: 1
Items: 
Size: 576441 Color: 2

Bin 5437: 423850 of cap free
Amount of items: 1
Items: 
Size: 576151 Color: 3

Bin 5438: 423866 of cap free
Amount of items: 1
Items: 
Size: 576135 Color: 2

Bin 5439: 423872 of cap free
Amount of items: 1
Items: 
Size: 576129 Color: 3

Bin 5440: 423909 of cap free
Amount of items: 1
Items: 
Size: 576092 Color: 4

Bin 5441: 423926 of cap free
Amount of items: 1
Items: 
Size: 576075 Color: 2

Bin 5442: 423945 of cap free
Amount of items: 1
Items: 
Size: 576056 Color: 1

Bin 5443: 424034 of cap free
Amount of items: 1
Items: 
Size: 575967 Color: 3

Bin 5444: 424099 of cap free
Amount of items: 1
Items: 
Size: 575902 Color: 2

Bin 5445: 424238 of cap free
Amount of items: 1
Items: 
Size: 575763 Color: 0

Bin 5446: 424319 of cap free
Amount of items: 1
Items: 
Size: 575682 Color: 4

Bin 5447: 424419 of cap free
Amount of items: 1
Items: 
Size: 575582 Color: 2

Bin 5448: 424493 of cap free
Amount of items: 1
Items: 
Size: 575508 Color: 3

Bin 5449: 424504 of cap free
Amount of items: 1
Items: 
Size: 575497 Color: 3

Bin 5450: 424567 of cap free
Amount of items: 1
Items: 
Size: 575434 Color: 4

Bin 5451: 424678 of cap free
Amount of items: 1
Items: 
Size: 575323 Color: 2

Bin 5452: 424764 of cap free
Amount of items: 1
Items: 
Size: 575237 Color: 2

Bin 5453: 424801 of cap free
Amount of items: 1
Items: 
Size: 575200 Color: 0

Bin 5454: 424879 of cap free
Amount of items: 1
Items: 
Size: 575122 Color: 1

Bin 5455: 424919 of cap free
Amount of items: 1
Items: 
Size: 575082 Color: 2

Bin 5456: 425033 of cap free
Amount of items: 1
Items: 
Size: 574968 Color: 1

Bin 5457: 425054 of cap free
Amount of items: 1
Items: 
Size: 574947 Color: 1

Bin 5458: 425097 of cap free
Amount of items: 1
Items: 
Size: 574904 Color: 3

Bin 5459: 425297 of cap free
Amount of items: 1
Items: 
Size: 574704 Color: 4

Bin 5460: 425300 of cap free
Amount of items: 1
Items: 
Size: 574701 Color: 2

Bin 5461: 425302 of cap free
Amount of items: 1
Items: 
Size: 574699 Color: 2

Bin 5462: 425400 of cap free
Amount of items: 1
Items: 
Size: 574601 Color: 3

Bin 5463: 425447 of cap free
Amount of items: 1
Items: 
Size: 574554 Color: 1

Bin 5464: 425480 of cap free
Amount of items: 1
Items: 
Size: 574521 Color: 3

Bin 5465: 425496 of cap free
Amount of items: 1
Items: 
Size: 574505 Color: 2

Bin 5466: 425615 of cap free
Amount of items: 1
Items: 
Size: 574386 Color: 1

Bin 5467: 425734 of cap free
Amount of items: 1
Items: 
Size: 574267 Color: 3

Bin 5468: 425833 of cap free
Amount of items: 1
Items: 
Size: 574168 Color: 3

Bin 5469: 425854 of cap free
Amount of items: 1
Items: 
Size: 574147 Color: 4

Bin 5470: 425941 of cap free
Amount of items: 1
Items: 
Size: 574060 Color: 3

Bin 5471: 426111 of cap free
Amount of items: 1
Items: 
Size: 573890 Color: 1

Bin 5472: 426162 of cap free
Amount of items: 1
Items: 
Size: 573839 Color: 4

Bin 5473: 426211 of cap free
Amount of items: 1
Items: 
Size: 573790 Color: 2

Bin 5474: 426340 of cap free
Amount of items: 1
Items: 
Size: 573661 Color: 2

Bin 5475: 426383 of cap free
Amount of items: 1
Items: 
Size: 573618 Color: 1

Bin 5476: 426459 of cap free
Amount of items: 1
Items: 
Size: 573542 Color: 1

Bin 5477: 426506 of cap free
Amount of items: 1
Items: 
Size: 573495 Color: 2

Bin 5478: 426528 of cap free
Amount of items: 1
Items: 
Size: 573473 Color: 0

Bin 5479: 426572 of cap free
Amount of items: 1
Items: 
Size: 573429 Color: 4

Bin 5480: 426855 of cap free
Amount of items: 1
Items: 
Size: 573146 Color: 0

Bin 5481: 426907 of cap free
Amount of items: 1
Items: 
Size: 573094 Color: 1

Bin 5482: 426933 of cap free
Amount of items: 1
Items: 
Size: 573068 Color: 2

Bin 5483: 426955 of cap free
Amount of items: 1
Items: 
Size: 573046 Color: 2

Bin 5484: 426964 of cap free
Amount of items: 1
Items: 
Size: 573037 Color: 4

Bin 5485: 427155 of cap free
Amount of items: 1
Items: 
Size: 572846 Color: 2

Bin 5486: 427320 of cap free
Amount of items: 1
Items: 
Size: 572681 Color: 0

Bin 5487: 427415 of cap free
Amount of items: 1
Items: 
Size: 572586 Color: 3

Bin 5488: 427428 of cap free
Amount of items: 1
Items: 
Size: 572573 Color: 2

Bin 5489: 427440 of cap free
Amount of items: 1
Items: 
Size: 572561 Color: 1

Bin 5490: 427443 of cap free
Amount of items: 1
Items: 
Size: 572558 Color: 3

Bin 5491: 427793 of cap free
Amount of items: 1
Items: 
Size: 572208 Color: 1

Bin 5492: 427890 of cap free
Amount of items: 1
Items: 
Size: 572111 Color: 4

Bin 5493: 427910 of cap free
Amount of items: 1
Items: 
Size: 572091 Color: 2

Bin 5494: 427978 of cap free
Amount of items: 1
Items: 
Size: 572023 Color: 0

Bin 5495: 428258 of cap free
Amount of items: 1
Items: 
Size: 571743 Color: 3

Bin 5496: 428406 of cap free
Amount of items: 1
Items: 
Size: 571595 Color: 0

Bin 5497: 428747 of cap free
Amount of items: 1
Items: 
Size: 571254 Color: 0

Bin 5498: 428995 of cap free
Amount of items: 1
Items: 
Size: 571006 Color: 1

Bin 5499: 429000 of cap free
Amount of items: 1
Items: 
Size: 571001 Color: 1

Bin 5500: 429081 of cap free
Amount of items: 1
Items: 
Size: 570920 Color: 2

Bin 5501: 429133 of cap free
Amount of items: 1
Items: 
Size: 570868 Color: 0

Bin 5502: 429206 of cap free
Amount of items: 1
Items: 
Size: 570795 Color: 3

Bin 5503: 429219 of cap free
Amount of items: 1
Items: 
Size: 570782 Color: 3

Bin 5504: 429277 of cap free
Amount of items: 1
Items: 
Size: 570724 Color: 0

Bin 5505: 429372 of cap free
Amount of items: 1
Items: 
Size: 570629 Color: 2

Bin 5506: 429373 of cap free
Amount of items: 1
Items: 
Size: 570628 Color: 3

Bin 5507: 429437 of cap free
Amount of items: 1
Items: 
Size: 570564 Color: 2

Bin 5508: 429491 of cap free
Amount of items: 1
Items: 
Size: 570510 Color: 0

Bin 5509: 429492 of cap free
Amount of items: 1
Items: 
Size: 570509 Color: 1

Bin 5510: 429579 of cap free
Amount of items: 1
Items: 
Size: 570422 Color: 1

Bin 5511: 429587 of cap free
Amount of items: 1
Items: 
Size: 570414 Color: 3

Bin 5512: 429637 of cap free
Amount of items: 1
Items: 
Size: 570364 Color: 4

Bin 5513: 429718 of cap free
Amount of items: 1
Items: 
Size: 570283 Color: 2

Bin 5514: 429894 of cap free
Amount of items: 1
Items: 
Size: 570107 Color: 2

Bin 5515: 429915 of cap free
Amount of items: 1
Items: 
Size: 570086 Color: 1

Bin 5516: 430155 of cap free
Amount of items: 1
Items: 
Size: 569846 Color: 3

Bin 5517: 430329 of cap free
Amount of items: 1
Items: 
Size: 569672 Color: 0

Bin 5518: 430463 of cap free
Amount of items: 1
Items: 
Size: 569538 Color: 4

Bin 5519: 430499 of cap free
Amount of items: 1
Items: 
Size: 569502 Color: 0

Bin 5520: 430515 of cap free
Amount of items: 1
Items: 
Size: 569486 Color: 3

Bin 5521: 430566 of cap free
Amount of items: 1
Items: 
Size: 569435 Color: 0

Bin 5522: 430600 of cap free
Amount of items: 1
Items: 
Size: 569401 Color: 3

Bin 5523: 430755 of cap free
Amount of items: 1
Items: 
Size: 569246 Color: 2

Bin 5524: 430814 of cap free
Amount of items: 1
Items: 
Size: 569187 Color: 1

Bin 5525: 430821 of cap free
Amount of items: 1
Items: 
Size: 569180 Color: 2

Bin 5526: 430947 of cap free
Amount of items: 1
Items: 
Size: 569054 Color: 2

Bin 5527: 431026 of cap free
Amount of items: 1
Items: 
Size: 568975 Color: 1

Bin 5528: 431050 of cap free
Amount of items: 1
Items: 
Size: 568951 Color: 1

Bin 5529: 431227 of cap free
Amount of items: 1
Items: 
Size: 568774 Color: 3

Bin 5530: 431245 of cap free
Amount of items: 1
Items: 
Size: 568756 Color: 0

Bin 5531: 431420 of cap free
Amount of items: 1
Items: 
Size: 568581 Color: 0

Bin 5532: 431444 of cap free
Amount of items: 1
Items: 
Size: 568557 Color: 4

Bin 5533: 431478 of cap free
Amount of items: 1
Items: 
Size: 568523 Color: 1

Bin 5534: 431500 of cap free
Amount of items: 1
Items: 
Size: 568501 Color: 4

Bin 5535: 431502 of cap free
Amount of items: 1
Items: 
Size: 568499 Color: 3

Bin 5536: 431626 of cap free
Amount of items: 1
Items: 
Size: 568375 Color: 4

Bin 5537: 431643 of cap free
Amount of items: 1
Items: 
Size: 568358 Color: 1

Bin 5538: 431657 of cap free
Amount of items: 1
Items: 
Size: 568344 Color: 2

Bin 5539: 431710 of cap free
Amount of items: 1
Items: 
Size: 568291 Color: 0

Bin 5540: 431759 of cap free
Amount of items: 1
Items: 
Size: 568242 Color: 2

Bin 5541: 431805 of cap free
Amount of items: 1
Items: 
Size: 568196 Color: 0

Bin 5542: 431830 of cap free
Amount of items: 1
Items: 
Size: 568171 Color: 3

Bin 5543: 431909 of cap free
Amount of items: 1
Items: 
Size: 568092 Color: 3

Bin 5544: 431926 of cap free
Amount of items: 1
Items: 
Size: 568075 Color: 2

Bin 5545: 431935 of cap free
Amount of items: 1
Items: 
Size: 568066 Color: 0

Bin 5546: 432069 of cap free
Amount of items: 1
Items: 
Size: 567932 Color: 4

Bin 5547: 432170 of cap free
Amount of items: 1
Items: 
Size: 567831 Color: 1

Bin 5548: 432180 of cap free
Amount of items: 1
Items: 
Size: 567821 Color: 3

Bin 5549: 432203 of cap free
Amount of items: 1
Items: 
Size: 567798 Color: 2

Bin 5550: 432262 of cap free
Amount of items: 1
Items: 
Size: 567739 Color: 4

Bin 5551: 432277 of cap free
Amount of items: 1
Items: 
Size: 567724 Color: 1

Bin 5552: 432377 of cap free
Amount of items: 1
Items: 
Size: 567624 Color: 2

Bin 5553: 432413 of cap free
Amount of items: 1
Items: 
Size: 567588 Color: 1

Bin 5554: 432463 of cap free
Amount of items: 1
Items: 
Size: 567538 Color: 2

Bin 5555: 432552 of cap free
Amount of items: 1
Items: 
Size: 567449 Color: 4

Bin 5556: 432617 of cap free
Amount of items: 1
Items: 
Size: 567384 Color: 1

Bin 5557: 432655 of cap free
Amount of items: 1
Items: 
Size: 567346 Color: 3

Bin 5558: 432655 of cap free
Amount of items: 1
Items: 
Size: 567346 Color: 3

Bin 5559: 432725 of cap free
Amount of items: 1
Items: 
Size: 567276 Color: 2

Bin 5560: 432787 of cap free
Amount of items: 1
Items: 
Size: 567214 Color: 0

Bin 5561: 432905 of cap free
Amount of items: 1
Items: 
Size: 567096 Color: 0

Bin 5562: 432913 of cap free
Amount of items: 1
Items: 
Size: 567088 Color: 3

Bin 5563: 432968 of cap free
Amount of items: 1
Items: 
Size: 567033 Color: 1

Bin 5564: 432971 of cap free
Amount of items: 1
Items: 
Size: 567030 Color: 4

Bin 5565: 433044 of cap free
Amount of items: 1
Items: 
Size: 566957 Color: 3

Bin 5566: 433052 of cap free
Amount of items: 1
Items: 
Size: 566949 Color: 0

Bin 5567: 433111 of cap free
Amount of items: 1
Items: 
Size: 566890 Color: 1

Bin 5568: 433113 of cap free
Amount of items: 1
Items: 
Size: 566888 Color: 2

Bin 5569: 433191 of cap free
Amount of items: 1
Items: 
Size: 566810 Color: 0

Bin 5570: 433379 of cap free
Amount of items: 1
Items: 
Size: 566622 Color: 0

Bin 5571: 433385 of cap free
Amount of items: 1
Items: 
Size: 566616 Color: 4

Bin 5572: 433387 of cap free
Amount of items: 1
Items: 
Size: 566614 Color: 2

Bin 5573: 433620 of cap free
Amount of items: 1
Items: 
Size: 566381 Color: 0

Bin 5574: 433715 of cap free
Amount of items: 1
Items: 
Size: 566286 Color: 0

Bin 5575: 433722 of cap free
Amount of items: 1
Items: 
Size: 566279 Color: 1

Bin 5576: 433789 of cap free
Amount of items: 1
Items: 
Size: 566212 Color: 1

Bin 5577: 433790 of cap free
Amount of items: 1
Items: 
Size: 566211 Color: 3

Bin 5578: 433796 of cap free
Amount of items: 1
Items: 
Size: 566205 Color: 1

Bin 5579: 433818 of cap free
Amount of items: 1
Items: 
Size: 566183 Color: 3

Bin 5580: 433851 of cap free
Amount of items: 1
Items: 
Size: 566150 Color: 0

Bin 5581: 433956 of cap free
Amount of items: 1
Items: 
Size: 566045 Color: 4

Bin 5582: 434085 of cap free
Amount of items: 1
Items: 
Size: 565916 Color: 4

Bin 5583: 434165 of cap free
Amount of items: 1
Items: 
Size: 565836 Color: 3

Bin 5584: 434294 of cap free
Amount of items: 1
Items: 
Size: 565707 Color: 1

Bin 5585: 434361 of cap free
Amount of items: 1
Items: 
Size: 565640 Color: 0

Bin 5586: 434366 of cap free
Amount of items: 1
Items: 
Size: 565635 Color: 2

Bin 5587: 434405 of cap free
Amount of items: 1
Items: 
Size: 565596 Color: 4

Bin 5588: 434710 of cap free
Amount of items: 1
Items: 
Size: 565291 Color: 2

Bin 5589: 434721 of cap free
Amount of items: 1
Items: 
Size: 565280 Color: 1

Bin 5590: 435355 of cap free
Amount of items: 1
Items: 
Size: 564646 Color: 3

Bin 5591: 435439 of cap free
Amount of items: 1
Items: 
Size: 564562 Color: 3

Bin 5592: 435447 of cap free
Amount of items: 1
Items: 
Size: 564554 Color: 4

Bin 5593: 435509 of cap free
Amount of items: 1
Items: 
Size: 564492 Color: 3

Bin 5594: 435533 of cap free
Amount of items: 1
Items: 
Size: 564468 Color: 3

Bin 5595: 435540 of cap free
Amount of items: 1
Items: 
Size: 564461 Color: 4

Bin 5596: 435582 of cap free
Amount of items: 1
Items: 
Size: 564419 Color: 2

Bin 5597: 435666 of cap free
Amount of items: 1
Items: 
Size: 564335 Color: 3

Bin 5598: 435814 of cap free
Amount of items: 1
Items: 
Size: 564187 Color: 2

Bin 5599: 435816 of cap free
Amount of items: 1
Items: 
Size: 564185 Color: 3

Bin 5600: 435921 of cap free
Amount of items: 1
Items: 
Size: 564080 Color: 3

Bin 5601: 435923 of cap free
Amount of items: 1
Items: 
Size: 564078 Color: 2

Bin 5602: 435966 of cap free
Amount of items: 1
Items: 
Size: 564035 Color: 1

Bin 5603: 436178 of cap free
Amount of items: 1
Items: 
Size: 563823 Color: 3

Bin 5604: 436273 of cap free
Amount of items: 1
Items: 
Size: 563728 Color: 0

Bin 5605: 436358 of cap free
Amount of items: 1
Items: 
Size: 563643 Color: 2

Bin 5606: 436373 of cap free
Amount of items: 1
Items: 
Size: 563628 Color: 2

Bin 5607: 436382 of cap free
Amount of items: 1
Items: 
Size: 563619 Color: 1

Bin 5608: 436560 of cap free
Amount of items: 1
Items: 
Size: 563441 Color: 0

Bin 5609: 436603 of cap free
Amount of items: 1
Items: 
Size: 563398 Color: 0

Bin 5610: 436720 of cap free
Amount of items: 1
Items: 
Size: 563281 Color: 0

Bin 5611: 436743 of cap free
Amount of items: 1
Items: 
Size: 563258 Color: 0

Bin 5612: 436758 of cap free
Amount of items: 1
Items: 
Size: 563243 Color: 2

Bin 5613: 436803 of cap free
Amount of items: 1
Items: 
Size: 563198 Color: 1

Bin 5614: 436910 of cap free
Amount of items: 1
Items: 
Size: 563091 Color: 3

Bin 5615: 436984 of cap free
Amount of items: 1
Items: 
Size: 563017 Color: 0

Bin 5616: 437205 of cap free
Amount of items: 1
Items: 
Size: 562796 Color: 0

Bin 5617: 437222 of cap free
Amount of items: 1
Items: 
Size: 562779 Color: 0

Bin 5618: 437229 of cap free
Amount of items: 1
Items: 
Size: 562772 Color: 1

Bin 5619: 437274 of cap free
Amount of items: 1
Items: 
Size: 562727 Color: 4

Bin 5620: 437415 of cap free
Amount of items: 1
Items: 
Size: 562586 Color: 0

Bin 5621: 437420 of cap free
Amount of items: 1
Items: 
Size: 562581 Color: 2

Bin 5622: 437620 of cap free
Amount of items: 1
Items: 
Size: 562381 Color: 2

Bin 5623: 437711 of cap free
Amount of items: 1
Items: 
Size: 562290 Color: 3

Bin 5624: 437732 of cap free
Amount of items: 1
Items: 
Size: 562269 Color: 3

Bin 5625: 437737 of cap free
Amount of items: 1
Items: 
Size: 562264 Color: 2

Bin 5626: 437773 of cap free
Amount of items: 1
Items: 
Size: 562228 Color: 4

Bin 5627: 437784 of cap free
Amount of items: 1
Items: 
Size: 562217 Color: 4

Bin 5628: 437890 of cap free
Amount of items: 1
Items: 
Size: 562111 Color: 1

Bin 5629: 437920 of cap free
Amount of items: 1
Items: 
Size: 562081 Color: 0

Bin 5630: 438072 of cap free
Amount of items: 1
Items: 
Size: 561929 Color: 3

Bin 5631: 438309 of cap free
Amount of items: 1
Items: 
Size: 561692 Color: 3

Bin 5632: 438380 of cap free
Amount of items: 1
Items: 
Size: 561621 Color: 4

Bin 5633: 438564 of cap free
Amount of items: 1
Items: 
Size: 561437 Color: 3

Bin 5634: 438644 of cap free
Amount of items: 1
Items: 
Size: 561357 Color: 2

Bin 5635: 438733 of cap free
Amount of items: 1
Items: 
Size: 561268 Color: 2

Bin 5636: 438801 of cap free
Amount of items: 1
Items: 
Size: 561200 Color: 3

Bin 5637: 438822 of cap free
Amount of items: 1
Items: 
Size: 561179 Color: 2

Bin 5638: 438828 of cap free
Amount of items: 1
Items: 
Size: 561173 Color: 0

Bin 5639: 438871 of cap free
Amount of items: 1
Items: 
Size: 561130 Color: 0

Bin 5640: 438911 of cap free
Amount of items: 1
Items: 
Size: 561090 Color: 2

Bin 5641: 438913 of cap free
Amount of items: 1
Items: 
Size: 561088 Color: 2

Bin 5642: 438975 of cap free
Amount of items: 1
Items: 
Size: 561026 Color: 1

Bin 5643: 439004 of cap free
Amount of items: 1
Items: 
Size: 560997 Color: 3

Bin 5644: 439126 of cap free
Amount of items: 1
Items: 
Size: 560875 Color: 3

Bin 5645: 439235 of cap free
Amount of items: 1
Items: 
Size: 560766 Color: 4

Bin 5646: 439264 of cap free
Amount of items: 1
Items: 
Size: 560737 Color: 3

Bin 5647: 439397 of cap free
Amount of items: 1
Items: 
Size: 560604 Color: 2

Bin 5648: 439416 of cap free
Amount of items: 1
Items: 
Size: 560585 Color: 2

Bin 5649: 439416 of cap free
Amount of items: 1
Items: 
Size: 560585 Color: 4

Bin 5650: 439432 of cap free
Amount of items: 1
Items: 
Size: 560569 Color: 0

Bin 5651: 439455 of cap free
Amount of items: 1
Items: 
Size: 560546 Color: 3

Bin 5652: 439476 of cap free
Amount of items: 1
Items: 
Size: 560525 Color: 4

Bin 5653: 439642 of cap free
Amount of items: 1
Items: 
Size: 560359 Color: 4

Bin 5654: 439748 of cap free
Amount of items: 1
Items: 
Size: 560253 Color: 0

Bin 5655: 440068 of cap free
Amount of items: 1
Items: 
Size: 559933 Color: 2

Bin 5656: 440254 of cap free
Amount of items: 1
Items: 
Size: 559747 Color: 3

Bin 5657: 440274 of cap free
Amount of items: 1
Items: 
Size: 559727 Color: 3

Bin 5658: 440306 of cap free
Amount of items: 1
Items: 
Size: 559695 Color: 0

Bin 5659: 440328 of cap free
Amount of items: 1
Items: 
Size: 559673 Color: 0

Bin 5660: 440429 of cap free
Amount of items: 1
Items: 
Size: 559572 Color: 2

Bin 5661: 440504 of cap free
Amount of items: 1
Items: 
Size: 559497 Color: 2

Bin 5662: 440737 of cap free
Amount of items: 1
Items: 
Size: 559264 Color: 0

Bin 5663: 440854 of cap free
Amount of items: 1
Items: 
Size: 559147 Color: 4

Bin 5664: 440966 of cap free
Amount of items: 1
Items: 
Size: 559035 Color: 3

Bin 5665: 440971 of cap free
Amount of items: 1
Items: 
Size: 559030 Color: 4

Bin 5666: 440984 of cap free
Amount of items: 1
Items: 
Size: 559017 Color: 2

Bin 5667: 602492 of cap free
Amount of items: 1
Items: 
Size: 397509 Color: 0

Total size: 4507902896
Total free space: 1159102771

