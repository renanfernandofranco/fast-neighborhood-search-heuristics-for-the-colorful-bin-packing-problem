Capicity Bin: 1000001
Lower Bound: 4533

Bins used: 5704
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 408208 Color: 4
Size: 393691 Color: 1
Size: 198102 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 418837 Color: 2
Size: 195858 Color: 4
Size: 193280 Color: 1
Size: 192026 Color: 4

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 418838 Color: 2
Size: 195149 Color: 4
Size: 193678 Color: 2
Size: 192336 Color: 1

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 418879 Color: 2
Size: 195760 Color: 3
Size: 194297 Color: 0
Size: 191065 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 422092 Color: 2
Size: 392480 Color: 0
Size: 185429 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 422271 Color: 4
Size: 392391 Color: 3
Size: 185339 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 426575 Color: 3
Size: 390700 Color: 0
Size: 182726 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 435965 Color: 4
Size: 386653 Color: 3
Size: 177383 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 438634 Color: 1
Size: 385420 Color: 0
Size: 175947 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 446347 Color: 2
Size: 384455 Color: 1
Size: 169199 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 446524 Color: 2
Size: 380558 Color: 1
Size: 172919 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 449187 Color: 4
Size: 378830 Color: 3
Size: 171984 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 449581 Color: 1
Size: 374238 Color: 0
Size: 176182 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 450318 Color: 2
Size: 275948 Color: 1
Size: 273735 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 450540 Color: 0
Size: 381925 Color: 3
Size: 167536 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 450561 Color: 2
Size: 278408 Color: 4
Size: 271032 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 450681 Color: 3
Size: 380717 Color: 2
Size: 168603 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 450922 Color: 4
Size: 377477 Color: 3
Size: 171602 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 451125 Color: 2
Size: 381180 Color: 0
Size: 167696 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 451185 Color: 4
Size: 275981 Color: 3
Size: 272835 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 451345 Color: 0
Size: 277078 Color: 4
Size: 271578 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 452806 Color: 2
Size: 277073 Color: 4
Size: 270122 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 452924 Color: 3
Size: 381094 Color: 0
Size: 165983 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 453806 Color: 2
Size: 379978 Color: 0
Size: 166217 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 454881 Color: 3
Size: 380494 Color: 0
Size: 164626 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 455018 Color: 4
Size: 279354 Color: 3
Size: 265629 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 455022 Color: 4
Size: 279972 Color: 2
Size: 265007 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 455554 Color: 2
Size: 380211 Color: 3
Size: 164236 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 455579 Color: 1
Size: 377159 Color: 2
Size: 167263 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 455687 Color: 3
Size: 380853 Color: 1
Size: 163461 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 455708 Color: 2
Size: 277477 Color: 1
Size: 266816 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 455807 Color: 0
Size: 372867 Color: 3
Size: 171327 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 455859 Color: 2
Size: 280234 Color: 0
Size: 263908 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 456020 Color: 2
Size: 378516 Color: 0
Size: 165465 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 456246 Color: 0
Size: 281833 Color: 2
Size: 261922 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 456372 Color: 3
Size: 279376 Color: 1
Size: 264253 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 456506 Color: 3
Size: 281498 Color: 1
Size: 261997 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 456606 Color: 2
Size: 279554 Color: 4
Size: 263841 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 457064 Color: 4
Size: 281484 Color: 0
Size: 261453 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 457220 Color: 3
Size: 379490 Color: 0
Size: 163291 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 457589 Color: 1
Size: 281419 Color: 2
Size: 260993 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 457758 Color: 0
Size: 282611 Color: 3
Size: 259632 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 457863 Color: 2
Size: 276878 Color: 3
Size: 265260 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 458186 Color: 0
Size: 279198 Color: 3
Size: 262617 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 458247 Color: 1
Size: 379111 Color: 3
Size: 162643 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 458298 Color: 0
Size: 276171 Color: 4
Size: 265532 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 458743 Color: 4
Size: 286989 Color: 3
Size: 254269 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 458838 Color: 3
Size: 376655 Color: 2
Size: 164508 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 458959 Color: 3
Size: 275608 Color: 4
Size: 265434 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 459055 Color: 1
Size: 375177 Color: 3
Size: 165769 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 459114 Color: 0
Size: 276412 Color: 1
Size: 264475 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 459313 Color: 2
Size: 278034 Color: 0
Size: 262654 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 459585 Color: 3
Size: 379430 Color: 4
Size: 160986 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 459585 Color: 4
Size: 288920 Color: 2
Size: 251496 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 459669 Color: 1
Size: 283614 Color: 0
Size: 256718 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 459860 Color: 3
Size: 276990 Color: 2
Size: 263151 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 459895 Color: 3
Size: 275569 Color: 2
Size: 264537 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 459996 Color: 0
Size: 378073 Color: 1
Size: 161932 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 460014 Color: 4
Size: 277041 Color: 1
Size: 262946 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 460064 Color: 3
Size: 375961 Color: 1
Size: 163976 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 460224 Color: 2
Size: 277140 Color: 3
Size: 262637 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 460236 Color: 3
Size: 371227 Color: 2
Size: 168538 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 460398 Color: 4
Size: 282960 Color: 0
Size: 256643 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 460404 Color: 3
Size: 377264 Color: 4
Size: 162333 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 460781 Color: 0
Size: 278842 Color: 1
Size: 260378 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 461360 Color: 2
Size: 280584 Color: 1
Size: 258057 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 461444 Color: 4
Size: 373382 Color: 2
Size: 165175 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 461755 Color: 2
Size: 374939 Color: 4
Size: 163307 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 461955 Color: 4
Size: 281836 Color: 1
Size: 256210 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 462041 Color: 3
Size: 377250 Color: 1
Size: 160710 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 462143 Color: 1
Size: 281539 Color: 3
Size: 256319 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 462395 Color: 4
Size: 278315 Color: 1
Size: 259291 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 462546 Color: 2
Size: 281879 Color: 0
Size: 255576 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 462762 Color: 4
Size: 279541 Color: 2
Size: 257698 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 462898 Color: 2
Size: 378719 Color: 3
Size: 158384 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 462919 Color: 2
Size: 281511 Color: 0
Size: 255571 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 463038 Color: 1
Size: 279140 Color: 0
Size: 257823 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 463139 Color: 0
Size: 280540 Color: 4
Size: 256322 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 463280 Color: 4
Size: 287090 Color: 0
Size: 249631 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 463316 Color: 0
Size: 283346 Color: 1
Size: 253339 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 463347 Color: 1
Size: 374590 Color: 3
Size: 162064 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 463515 Color: 4
Size: 369637 Color: 1
Size: 166849 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 463683 Color: 2
Size: 380554 Color: 1
Size: 155764 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 464179 Color: 1
Size: 284892 Color: 4
Size: 250930 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 464230 Color: 2
Size: 367384 Color: 1
Size: 168387 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 464247 Color: 2
Size: 284377 Color: 1
Size: 251377 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 465892 Color: 2
Size: 373099 Color: 4
Size: 161010 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 466116 Color: 4
Size: 299038 Color: 2
Size: 234847 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 467223 Color: 4
Size: 374032 Color: 0
Size: 158746 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 467275 Color: 3
Size: 285133 Color: 0
Size: 247593 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 467310 Color: 0
Size: 284831 Color: 3
Size: 247860 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 467346 Color: 2
Size: 292895 Color: 1
Size: 239760 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 467369 Color: 3
Size: 286652 Color: 4
Size: 245980 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 467477 Color: 2
Size: 375609 Color: 1
Size: 156915 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 467560 Color: 0
Size: 290899 Color: 2
Size: 241542 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 467570 Color: 1
Size: 292136 Color: 2
Size: 240295 Color: 4

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 467636 Color: 1
Size: 288759 Color: 2
Size: 243606 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 467734 Color: 4
Size: 381716 Color: 3
Size: 150551 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 467759 Color: 4
Size: 373017 Color: 0
Size: 159225 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 467838 Color: 4
Size: 376333 Color: 3
Size: 155830 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 467953 Color: 3
Size: 372231 Color: 0
Size: 159817 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 469332 Color: 0
Size: 288672 Color: 4
Size: 241997 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 469371 Color: 2
Size: 286649 Color: 3
Size: 243981 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 469754 Color: 3
Size: 376156 Color: 2
Size: 154091 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 469833 Color: 0
Size: 286295 Color: 4
Size: 243873 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 469892 Color: 0
Size: 372914 Color: 1
Size: 157195 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 469906 Color: 0
Size: 287500 Color: 3
Size: 242595 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 470216 Color: 0
Size: 289953 Color: 1
Size: 239832 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 470324 Color: 0
Size: 287541 Color: 2
Size: 242136 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 470614 Color: 2
Size: 287109 Color: 1
Size: 242278 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 470977 Color: 2
Size: 285232 Color: 1
Size: 243792 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 471191 Color: 4
Size: 371237 Color: 2
Size: 157573 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 471265 Color: 4
Size: 288703 Color: 0
Size: 240033 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 471397 Color: 2
Size: 288077 Color: 3
Size: 240527 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 471550 Color: 0
Size: 369979 Color: 3
Size: 158472 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 471744 Color: 1
Size: 284515 Color: 4
Size: 243742 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 471772 Color: 1
Size: 288260 Color: 2
Size: 239969 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 471916 Color: 2
Size: 288608 Color: 4
Size: 239477 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 471970 Color: 1
Size: 286098 Color: 3
Size: 241933 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 472046 Color: 4
Size: 291102 Color: 3
Size: 236853 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 472279 Color: 2
Size: 284851 Color: 0
Size: 242871 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 472384 Color: 2
Size: 288467 Color: 0
Size: 239150 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 472584 Color: 3
Size: 288001 Color: 0
Size: 239416 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 472741 Color: 3
Size: 290435 Color: 4
Size: 236825 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 472780 Color: 1
Size: 374590 Color: 4
Size: 152631 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 472851 Color: 3
Size: 286454 Color: 0
Size: 240696 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 472873 Color: 0
Size: 291800 Color: 3
Size: 235328 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 472900 Color: 2
Size: 295596 Color: 0
Size: 231505 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 472910 Color: 4
Size: 294031 Color: 1
Size: 233060 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 473060 Color: 2
Size: 368367 Color: 3
Size: 158574 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 473163 Color: 4
Size: 376814 Color: 1
Size: 150024 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 473244 Color: 2
Size: 286760 Color: 4
Size: 239997 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 473343 Color: 2
Size: 371603 Color: 4
Size: 155055 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 473392 Color: 1
Size: 290316 Color: 4
Size: 236293 Color: 3

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 473523 Color: 3
Size: 287894 Color: 2
Size: 238584 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 473582 Color: 1
Size: 293689 Color: 4
Size: 232730 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 473787 Color: 3
Size: 287500 Color: 2
Size: 238714 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 473877 Color: 2
Size: 291405 Color: 3
Size: 234719 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 473895 Color: 3
Size: 292370 Color: 2
Size: 233736 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 473990 Color: 2
Size: 375930 Color: 0
Size: 150081 Color: 4

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 474132 Color: 1
Size: 289305 Color: 3
Size: 236564 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 474259 Color: 2
Size: 291464 Color: 1
Size: 234278 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 474441 Color: 3
Size: 290165 Color: 2
Size: 235395 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 474490 Color: 2
Size: 291416 Color: 3
Size: 234095 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 474502 Color: 1
Size: 287268 Color: 0
Size: 238231 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 474523 Color: 4
Size: 375512 Color: 1
Size: 149966 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 474724 Color: 2
Size: 286037 Color: 0
Size: 239240 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 474795 Color: 2
Size: 286943 Color: 1
Size: 238263 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 474896 Color: 3
Size: 286386 Color: 2
Size: 238719 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 474898 Color: 3
Size: 289011 Color: 4
Size: 236092 Color: 3

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 474972 Color: 2
Size: 289205 Color: 1
Size: 235824 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 475049 Color: 1
Size: 367421 Color: 2
Size: 157531 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 475223 Color: 4
Size: 291368 Color: 3
Size: 233410 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 475702 Color: 1
Size: 292527 Color: 3
Size: 231772 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 475862 Color: 4
Size: 372566 Color: 0
Size: 151573 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 476036 Color: 4
Size: 370798 Color: 1
Size: 153167 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 476260 Color: 1
Size: 294328 Color: 3
Size: 229413 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 476342 Color: 2
Size: 370684 Color: 0
Size: 152975 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 476344 Color: 2
Size: 289589 Color: 1
Size: 234068 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 476404 Color: 4
Size: 291933 Color: 3
Size: 231664 Color: 4

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 476577 Color: 1
Size: 378908 Color: 2
Size: 144516 Color: 4

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 477182 Color: 0
Size: 294692 Color: 1
Size: 228127 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 477623 Color: 3
Size: 378146 Color: 0
Size: 144232 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 477709 Color: 1
Size: 372649 Color: 3
Size: 149643 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 478052 Color: 3
Size: 292171 Color: 1
Size: 229778 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 478251 Color: 0
Size: 370662 Color: 3
Size: 151088 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 478257 Color: 1
Size: 374975 Color: 4
Size: 146769 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 478391 Color: 4
Size: 293136 Color: 1
Size: 228474 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 478406 Color: 3
Size: 297625 Color: 1
Size: 223970 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 478440 Color: 2
Size: 372768 Color: 1
Size: 148793 Color: 4

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 478923 Color: 3
Size: 293174 Color: 1
Size: 227904 Color: 2

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 479479 Color: 4
Size: 294034 Color: 1
Size: 226488 Color: 2

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 479623 Color: 4
Size: 373614 Color: 2
Size: 146764 Color: 3

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 479647 Color: 2
Size: 290538 Color: 3
Size: 229816 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 479702 Color: 0
Size: 295840 Color: 2
Size: 224459 Color: 4

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 479704 Color: 0
Size: 290508 Color: 1
Size: 229789 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 479796 Color: 2
Size: 370552 Color: 4
Size: 149653 Color: 4

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 479881 Color: 2
Size: 293081 Color: 3
Size: 227039 Color: 3

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 479905 Color: 2
Size: 368981 Color: 0
Size: 151115 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 480594 Color: 3
Size: 372854 Color: 1
Size: 146553 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 480776 Color: 3
Size: 296930 Color: 0
Size: 222295 Color: 3

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 480855 Color: 3
Size: 369749 Color: 0
Size: 149397 Color: 2

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 480977 Color: 0
Size: 372087 Color: 3
Size: 146937 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 481125 Color: 3
Size: 373662 Color: 0
Size: 145214 Color: 3

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 481706 Color: 2
Size: 298771 Color: 0
Size: 219524 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 481791 Color: 2
Size: 298666 Color: 3
Size: 219544 Color: 4

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 481893 Color: 1
Size: 296218 Color: 2
Size: 221890 Color: 2

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 482215 Color: 4
Size: 362838 Color: 0
Size: 154948 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 482616 Color: 3
Size: 294871 Color: 0
Size: 222514 Color: 3

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 482640 Color: 1
Size: 294154 Color: 4
Size: 223207 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 482672 Color: 1
Size: 371767 Color: 3
Size: 145562 Color: 2

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 482993 Color: 0
Size: 293003 Color: 2
Size: 224005 Color: 2

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 483095 Color: 2
Size: 365772 Color: 0
Size: 151134 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 483130 Color: 2
Size: 293306 Color: 0
Size: 223565 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 483380 Color: 3
Size: 292417 Color: 2
Size: 224204 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 483969 Color: 4
Size: 295917 Color: 0
Size: 220115 Color: 2

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 484189 Color: 2
Size: 296074 Color: 1
Size: 219738 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 484195 Color: 1
Size: 295417 Color: 0
Size: 220389 Color: 2

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 484264 Color: 0
Size: 372553 Color: 3
Size: 143184 Color: 4

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 484291 Color: 1
Size: 296635 Color: 2
Size: 219075 Color: 4

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 484563 Color: 0
Size: 293087 Color: 3
Size: 222351 Color: 4

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 484669 Color: 0
Size: 371873 Color: 3
Size: 143459 Color: 3

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 484833 Color: 0
Size: 292022 Color: 3
Size: 223146 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 484859 Color: 2
Size: 293372 Color: 3
Size: 221770 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 484888 Color: 3
Size: 294179 Color: 0
Size: 220934 Color: 2

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 484904 Color: 4
Size: 294675 Color: 0
Size: 220422 Color: 4

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 484908 Color: 0
Size: 293719 Color: 1
Size: 221374 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 485253 Color: 2
Size: 375562 Color: 0
Size: 139186 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 485329 Color: 4
Size: 297121 Color: 1
Size: 217551 Color: 3

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 485498 Color: 0
Size: 294859 Color: 2
Size: 219644 Color: 3

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 485650 Color: 4
Size: 363932 Color: 2
Size: 150419 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 485830 Color: 3
Size: 376928 Color: 1
Size: 137243 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 486144 Color: 2
Size: 366874 Color: 1
Size: 146983 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 486327 Color: 0
Size: 299034 Color: 2
Size: 214640 Color: 3

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 486581 Color: 4
Size: 371257 Color: 1
Size: 142163 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 486834 Color: 3
Size: 368832 Color: 1
Size: 144335 Color: 2

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 486898 Color: 1
Size: 368516 Color: 2
Size: 144587 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 486988 Color: 3
Size: 369476 Color: 0
Size: 143537 Color: 4

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 487143 Color: 1
Size: 366961 Color: 3
Size: 145897 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 487233 Color: 0
Size: 374657 Color: 2
Size: 138111 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 487459 Color: 1
Size: 306378 Color: 2
Size: 206164 Color: 3

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 487571 Color: 4
Size: 298057 Color: 0
Size: 214373 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 487638 Color: 2
Size: 368579 Color: 3
Size: 143784 Color: 3

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 487671 Color: 2
Size: 299581 Color: 4
Size: 212749 Color: 3

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 487804 Color: 2
Size: 372601 Color: 4
Size: 139596 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 488096 Color: 2
Size: 313073 Color: 4
Size: 198832 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 488581 Color: 1
Size: 297877 Color: 4
Size: 213543 Color: 2

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 488672 Color: 4
Size: 298673 Color: 0
Size: 212656 Color: 4

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 489102 Color: 3
Size: 298385 Color: 0
Size: 212514 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 489176 Color: 3
Size: 369388 Color: 1
Size: 141437 Color: 2

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 489230 Color: 3
Size: 299346 Color: 1
Size: 211425 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 489432 Color: 4
Size: 302122 Color: 0
Size: 208447 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 489540 Color: 3
Size: 300167 Color: 1
Size: 210294 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 489605 Color: 1
Size: 295739 Color: 4
Size: 214657 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 489646 Color: 2
Size: 297531 Color: 3
Size: 212824 Color: 3

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 489717 Color: 1
Size: 359777 Color: 2
Size: 150507 Color: 4

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 489728 Color: 1
Size: 297942 Color: 0
Size: 212331 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 490222 Color: 4
Size: 367547 Color: 0
Size: 142232 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 490305 Color: 0
Size: 297436 Color: 4
Size: 212260 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 490475 Color: 1
Size: 298945 Color: 3
Size: 210581 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 490691 Color: 3
Size: 317076 Color: 1
Size: 192234 Color: 4

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 491011 Color: 2
Size: 364785 Color: 1
Size: 144205 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 491128 Color: 4
Size: 302109 Color: 3
Size: 206764 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 491442 Color: 0
Size: 298969 Color: 4
Size: 209590 Color: 3

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 491509 Color: 2
Size: 300315 Color: 0
Size: 208177 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 491513 Color: 0
Size: 297685 Color: 3
Size: 210803 Color: 3

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 491568 Color: 3
Size: 297432 Color: 1
Size: 211001 Color: 3

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 492135 Color: 1
Size: 366302 Color: 2
Size: 141564 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 492348 Color: 0
Size: 297409 Color: 4
Size: 210244 Color: 3

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 492584 Color: 4
Size: 300710 Color: 3
Size: 206707 Color: 4

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 492794 Color: 2
Size: 301986 Color: 0
Size: 205221 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 493005 Color: 0
Size: 365757 Color: 3
Size: 141239 Color: 4

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 493038 Color: 0
Size: 298383 Color: 2
Size: 208580 Color: 4

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 493066 Color: 0
Size: 300377 Color: 4
Size: 206558 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 493559 Color: 4
Size: 306868 Color: 0
Size: 199574 Color: 2

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 493569 Color: 1
Size: 311775 Color: 2
Size: 194657 Color: 4

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 493614 Color: 1
Size: 301322 Color: 0
Size: 205065 Color: 4

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 493783 Color: 1
Size: 372266 Color: 3
Size: 133952 Color: 4

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 494560 Color: 1
Size: 305969 Color: 0
Size: 199472 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 494574 Color: 0
Size: 301073 Color: 1
Size: 204354 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 494789 Color: 3
Size: 300306 Color: 2
Size: 204906 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 494920 Color: 4
Size: 369098 Color: 3
Size: 135983 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 495140 Color: 0
Size: 363963 Color: 1
Size: 140898 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 495240 Color: 0
Size: 300019 Color: 2
Size: 204742 Color: 2

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 495384 Color: 2
Size: 302427 Color: 4
Size: 202190 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 495875 Color: 2
Size: 362222 Color: 3
Size: 141904 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 495916 Color: 3
Size: 366649 Color: 1
Size: 137436 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 495966 Color: 1
Size: 302029 Color: 4
Size: 202006 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 496011 Color: 3
Size: 370711 Color: 2
Size: 133279 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 496148 Color: 2
Size: 301423 Color: 3
Size: 202430 Color: 3

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 496149 Color: 1
Size: 300447 Color: 3
Size: 203405 Color: 2

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 496648 Color: 3
Size: 306691 Color: 4
Size: 196662 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 496802 Color: 2
Size: 366363 Color: 3
Size: 136836 Color: 3

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 496998 Color: 0
Size: 303387 Color: 2
Size: 199616 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 497048 Color: 4
Size: 306092 Color: 0
Size: 196861 Color: 2

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 497190 Color: 1
Size: 367499 Color: 2
Size: 135312 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 497246 Color: 0
Size: 303211 Color: 3
Size: 199544 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 497401 Color: 3
Size: 308561 Color: 0
Size: 194039 Color: 4

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 497401 Color: 4
Size: 364891 Color: 2
Size: 137709 Color: 3

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 497548 Color: 2
Size: 304678 Color: 0
Size: 197775 Color: 3

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 497634 Color: 0
Size: 303518 Color: 1
Size: 198849 Color: 4

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 497696 Color: 3
Size: 367252 Color: 4
Size: 135053 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 497712 Color: 2
Size: 356302 Color: 0
Size: 145987 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 497739 Color: 1
Size: 302359 Color: 0
Size: 199903 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 497800 Color: 3
Size: 309760 Color: 1
Size: 192441 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 497847 Color: 0
Size: 362304 Color: 3
Size: 139850 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 497856 Color: 4
Size: 312149 Color: 0
Size: 189996 Color: 2

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 497876 Color: 2
Size: 322085 Color: 3
Size: 180040 Color: 3

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 497926 Color: 3
Size: 301647 Color: 1
Size: 200428 Color: 2

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 497957 Color: 0
Size: 308862 Color: 1
Size: 193182 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 498087 Color: 1
Size: 365483 Color: 0
Size: 136431 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 498272 Color: 4
Size: 363503 Color: 0
Size: 138226 Color: 3

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 498364 Color: 1
Size: 309522 Color: 2
Size: 192115 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 498427 Color: 2
Size: 304690 Color: 4
Size: 196884 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 498612 Color: 1
Size: 302167 Color: 3
Size: 199222 Color: 3

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 498776 Color: 0
Size: 311225 Color: 1
Size: 190000 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 499015 Color: 2
Size: 361271 Color: 0
Size: 139715 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 499115 Color: 4
Size: 365986 Color: 3
Size: 134900 Color: 3

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 499258 Color: 1
Size: 302591 Color: 0
Size: 198152 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 499262 Color: 4
Size: 368179 Color: 1
Size: 132560 Color: 4

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 499388 Color: 4
Size: 311436 Color: 3
Size: 189177 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 499644 Color: 4
Size: 364979 Color: 1
Size: 135378 Color: 4

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 499734 Color: 3
Size: 303433 Color: 0
Size: 196834 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 499736 Color: 0
Size: 303990 Color: 1
Size: 196275 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 499749 Color: 2
Size: 302659 Color: 1
Size: 197593 Color: 3

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 499892 Color: 3
Size: 368806 Color: 1
Size: 131303 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 499947 Color: 1
Size: 303135 Color: 0
Size: 196919 Color: 2

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 499976 Color: 3
Size: 361006 Color: 2
Size: 139019 Color: 2

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 500333 Color: 3
Size: 304322 Color: 1
Size: 195346 Color: 2

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 500376 Color: 2
Size: 363536 Color: 4
Size: 136089 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 500424 Color: 1
Size: 312255 Color: 3
Size: 187322 Color: 3

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 500606 Color: 1
Size: 366713 Color: 2
Size: 132682 Color: 3

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 500644 Color: 3
Size: 303622 Color: 4
Size: 195735 Color: 3

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 500669 Color: 3
Size: 303053 Color: 4
Size: 196279 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 500676 Color: 0
Size: 308178 Color: 2
Size: 191147 Color: 2

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 500689 Color: 4
Size: 363662 Color: 3
Size: 135650 Color: 2

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 500749 Color: 2
Size: 363153 Color: 3
Size: 136099 Color: 3

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 500803 Color: 4
Size: 306627 Color: 1
Size: 192571 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 500879 Color: 1
Size: 366565 Color: 2
Size: 132557 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 500883 Color: 3
Size: 368388 Color: 1
Size: 130730 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 500997 Color: 3
Size: 303659 Color: 1
Size: 195345 Color: 3

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 501179 Color: 2
Size: 314990 Color: 1
Size: 183832 Color: 3

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 501332 Color: 0
Size: 361941 Color: 2
Size: 136728 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 501482 Color: 2
Size: 302717 Color: 0
Size: 195802 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 501501 Color: 4
Size: 367267 Color: 3
Size: 131233 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 501554 Color: 0
Size: 361526 Color: 1
Size: 136921 Color: 2

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 501658 Color: 4
Size: 362078 Color: 3
Size: 136265 Color: 2

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 502323 Color: 3
Size: 304652 Color: 0
Size: 193026 Color: 3

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 502572 Color: 3
Size: 304364 Color: 2
Size: 193065 Color: 3

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 502589 Color: 4
Size: 497412 Color: 3

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 502655 Color: 4
Size: 353441 Color: 1
Size: 143905 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 502667 Color: 2
Size: 303009 Color: 4
Size: 194325 Color: 3

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 502785 Color: 1
Size: 497216 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 502802 Color: 0
Size: 316952 Color: 1
Size: 180247 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 502888 Color: 4
Size: 306114 Color: 3
Size: 190999 Color: 4

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 502911 Color: 0
Size: 306537 Color: 4
Size: 190553 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 503049 Color: 4
Size: 363551 Color: 0
Size: 133401 Color: 2

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 503234 Color: 3
Size: 305291 Color: 0
Size: 191476 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 503494 Color: 2
Size: 362961 Color: 3
Size: 133546 Color: 2

Bin 340: 0 of cap free
Amount of items: 2
Items: 
Size: 503572 Color: 2
Size: 496429 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 503668 Color: 3
Size: 366486 Color: 4
Size: 129847 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 503732 Color: 2
Size: 303408 Color: 1
Size: 192861 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 504018 Color: 1
Size: 308201 Color: 0
Size: 187782 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 504027 Color: 4
Size: 362181 Color: 3
Size: 133793 Color: 2

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 504051 Color: 1
Size: 305211 Color: 0
Size: 190739 Color: 4

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 504068 Color: 2
Size: 495933 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 504204 Color: 1
Size: 309594 Color: 0
Size: 186203 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 504345 Color: 3
Size: 305099 Color: 4
Size: 190557 Color: 3

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 504387 Color: 0
Size: 303093 Color: 3
Size: 192521 Color: 3

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 504562 Color: 3
Size: 304918 Color: 1
Size: 190521 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 504601 Color: 1
Size: 353338 Color: 4
Size: 142062 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 504710 Color: 0
Size: 358682 Color: 4
Size: 136609 Color: 3

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 504787 Color: 3
Size: 361414 Color: 0
Size: 133800 Color: 3

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 504820 Color: 4
Size: 303914 Color: 2
Size: 191267 Color: 4

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 504828 Color: 3
Size: 362687 Color: 1
Size: 132486 Color: 3

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 505114 Color: 1
Size: 367430 Color: 0
Size: 127457 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 505153 Color: 4
Size: 307476 Color: 1
Size: 187372 Color: 4

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 505346 Color: 2
Size: 362645 Color: 1
Size: 132010 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 505613 Color: 3
Size: 305225 Color: 0
Size: 189163 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 505958 Color: 2
Size: 309207 Color: 4
Size: 184836 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 506031 Color: 3
Size: 358646 Color: 2
Size: 135324 Color: 3

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 506062 Color: 3
Size: 364388 Color: 0
Size: 129551 Color: 3

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 506071 Color: 2
Size: 356906 Color: 3
Size: 137024 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 506103 Color: 3
Size: 306532 Color: 4
Size: 187366 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 506158 Color: 4
Size: 304247 Color: 3
Size: 189596 Color: 3

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 506291 Color: 1
Size: 309938 Color: 4
Size: 183772 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 506370 Color: 3
Size: 308388 Color: 2
Size: 185243 Color: 2

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 506454 Color: 0
Size: 307966 Color: 4
Size: 185581 Color: 2

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 506496 Color: 1
Size: 307124 Color: 0
Size: 186381 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 506588 Color: 3
Size: 307707 Color: 1
Size: 185706 Color: 2

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 506633 Color: 3
Size: 305994 Color: 0
Size: 187374 Color: 1

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 506837 Color: 2
Size: 308871 Color: 1
Size: 184293 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 506877 Color: 3
Size: 304811 Color: 4
Size: 188313 Color: 3

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 507014 Color: 0
Size: 307614 Color: 1
Size: 185373 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 507214 Color: 0
Size: 304016 Color: 1
Size: 188771 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 507239 Color: 0
Size: 360323 Color: 3
Size: 132439 Color: 2

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 507255 Color: 2
Size: 359083 Color: 3
Size: 133663 Color: 2

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 507303 Color: 1
Size: 362949 Color: 2
Size: 129749 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 507351 Color: 2
Size: 307515 Color: 0
Size: 185135 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 507361 Color: 3
Size: 365786 Color: 2
Size: 126854 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 507385 Color: 2
Size: 362805 Color: 4
Size: 129811 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 507538 Color: 0
Size: 359023 Color: 4
Size: 133440 Color: 3

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 507702 Color: 3
Size: 492299 Color: 4

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 507713 Color: 1
Size: 314741 Color: 0
Size: 177547 Color: 2

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 507769 Color: 2
Size: 309432 Color: 3
Size: 182800 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 507982 Color: 1
Size: 315583 Color: 4
Size: 176436 Color: 2

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 508013 Color: 3
Size: 308884 Color: 4
Size: 183104 Color: 4

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 508074 Color: 0
Size: 361911 Color: 2
Size: 130016 Color: 3

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 508194 Color: 0
Size: 307429 Color: 2
Size: 184378 Color: 2

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 508208 Color: 2
Size: 304906 Color: 4
Size: 186887 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 508248 Color: 2
Size: 356502 Color: 3
Size: 135251 Color: 3

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 508301 Color: 1
Size: 362285 Color: 0
Size: 129415 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 508326 Color: 2
Size: 361461 Color: 4
Size: 130214 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 508429 Color: 0
Size: 315786 Color: 2
Size: 175786 Color: 4

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 508767 Color: 1
Size: 362765 Color: 3
Size: 128469 Color: 2

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 508770 Color: 1
Size: 316367 Color: 2
Size: 174864 Color: 2

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 508790 Color: 2
Size: 308089 Color: 4
Size: 183122 Color: 2

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 508820 Color: 2
Size: 312780 Color: 1
Size: 178401 Color: 4

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 509084 Color: 2
Size: 308210 Color: 4
Size: 182707 Color: 3

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 509226 Color: 3
Size: 357466 Color: 1
Size: 133309 Color: 2

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 509371 Color: 3
Size: 308760 Color: 0
Size: 181870 Color: 4

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 509386 Color: 4
Size: 360421 Color: 2
Size: 130194 Color: 2

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 509404 Color: 1
Size: 305459 Color: 4
Size: 185138 Color: 3

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 509469 Color: 1
Size: 310554 Color: 4
Size: 179978 Color: 3

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 509835 Color: 0
Size: 307509 Color: 4
Size: 182657 Color: 1

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 509996 Color: 0
Size: 352522 Color: 3
Size: 137483 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 510189 Color: 4
Size: 358487 Color: 2
Size: 131325 Color: 4

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 510314 Color: 0
Size: 367931 Color: 4
Size: 121756 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 510442 Color: 3
Size: 358550 Color: 4
Size: 131009 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 510628 Color: 0
Size: 312372 Color: 2
Size: 177001 Color: 2

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 510655 Color: 2
Size: 489346 Color: 1

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 510858 Color: 2
Size: 360591 Color: 3
Size: 128552 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 510892 Color: 4
Size: 357041 Color: 1
Size: 132068 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 511036 Color: 2
Size: 322253 Color: 3
Size: 166712 Color: 2

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 511066 Color: 2
Size: 310307 Color: 4
Size: 178628 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 511218 Color: 3
Size: 309549 Color: 1
Size: 179234 Color: 1

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 511311 Color: 2
Size: 309482 Color: 3
Size: 179208 Color: 4

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 511404 Color: 4
Size: 309347 Color: 3
Size: 179250 Color: 4

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 511464 Color: 2
Size: 361702 Color: 1
Size: 126835 Color: 4

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 511560 Color: 3
Size: 313321 Color: 4
Size: 175120 Color: 2

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 511843 Color: 0
Size: 315401 Color: 2
Size: 172757 Color: 2

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 511987 Color: 1
Size: 358355 Color: 3
Size: 129659 Color: 3

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 512107 Color: 2
Size: 309050 Color: 4
Size: 178844 Color: 3

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 512161 Color: 4
Size: 317236 Color: 2
Size: 170604 Color: 2

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 512169 Color: 3
Size: 311250 Color: 1
Size: 176582 Color: 3

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 512229 Color: 0
Size: 314553 Color: 2
Size: 173219 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 512388 Color: 0
Size: 313214 Color: 2
Size: 174399 Color: 3

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 512429 Color: 0
Size: 315766 Color: 2
Size: 171806 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 512511 Color: 2
Size: 353567 Color: 1
Size: 133923 Color: 2

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 512543 Color: 4
Size: 311319 Color: 1
Size: 176139 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 512605 Color: 1
Size: 321908 Color: 0
Size: 165488 Color: 2

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 512710 Color: 4
Size: 365338 Color: 3
Size: 121953 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 512713 Color: 0
Size: 314807 Color: 3
Size: 172481 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 512927 Color: 2
Size: 324546 Color: 3
Size: 162528 Color: 4

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 512928 Color: 0
Size: 361006 Color: 3
Size: 126067 Color: 1

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 512938 Color: 0
Size: 311151 Color: 2
Size: 175912 Color: 4

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 512987 Color: 3
Size: 360152 Color: 4
Size: 126862 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 513040 Color: 2
Size: 314346 Color: 0
Size: 172615 Color: 3

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 513050 Color: 4
Size: 358856 Color: 2
Size: 128095 Color: 1

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 513229 Color: 1
Size: 358030 Color: 3
Size: 128742 Color: 2

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 513393 Color: 3
Size: 359955 Color: 0
Size: 126653 Color: 1

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 513481 Color: 0
Size: 313861 Color: 4
Size: 172659 Color: 3

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 513666 Color: 1
Size: 309723 Color: 4
Size: 176612 Color: 3

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 513698 Color: 3
Size: 357980 Color: 1
Size: 128323 Color: 3

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 513750 Color: 3
Size: 356422 Color: 4
Size: 129829 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 513803 Color: 4
Size: 324351 Color: 0
Size: 161847 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 513967 Color: 1
Size: 354951 Color: 0
Size: 131083 Color: 4

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 513973 Color: 1
Size: 486028 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 514043 Color: 2
Size: 312321 Color: 0
Size: 173637 Color: 1

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 514300 Color: 2
Size: 315282 Color: 4
Size: 170419 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 514575 Color: 3
Size: 356277 Color: 0
Size: 129149 Color: 4

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 514720 Color: 2
Size: 312886 Color: 4
Size: 172395 Color: 3

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 514802 Color: 2
Size: 356490 Color: 3
Size: 128709 Color: 2

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 515076 Color: 2
Size: 356129 Color: 0
Size: 128796 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 515084 Color: 1
Size: 340478 Color: 3
Size: 144439 Color: 3

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 515240 Color: 3
Size: 356080 Color: 0
Size: 128681 Color: 2

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 515332 Color: 0
Size: 355527 Color: 3
Size: 129142 Color: 1

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 515340 Color: 3
Size: 312209 Color: 1
Size: 172452 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 515556 Color: 0
Size: 357454 Color: 4
Size: 126991 Color: 1

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 515713 Color: 3
Size: 309514 Color: 4
Size: 174774 Color: 3

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 515739 Color: 2
Size: 353251 Color: 3
Size: 131011 Color: 4

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 516018 Color: 4
Size: 357818 Color: 0
Size: 126165 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 516028 Color: 3
Size: 350467 Color: 1
Size: 133506 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 516081 Color: 2
Size: 315084 Color: 1
Size: 168836 Color: 1

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 516131 Color: 0
Size: 312081 Color: 1
Size: 171789 Color: 4

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 516168 Color: 3
Size: 314563 Color: 2
Size: 169270 Color: 3

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 516275 Color: 3
Size: 312372 Color: 4
Size: 171354 Color: 1

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 516877 Color: 0
Size: 320378 Color: 4
Size: 162746 Color: 4

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 516942 Color: 1
Size: 315360 Color: 0
Size: 167699 Color: 1

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 517152 Color: 4
Size: 355499 Color: 3
Size: 127350 Color: 4

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 517181 Color: 1
Size: 314043 Color: 0
Size: 168777 Color: 3

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 517297 Color: 1
Size: 312629 Color: 4
Size: 170075 Color: 2

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 517502 Color: 0
Size: 313522 Color: 1
Size: 168977 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 517531 Color: 4
Size: 355304 Color: 1
Size: 127166 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 517682 Color: 4
Size: 354225 Color: 3
Size: 128094 Color: 3

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 517716 Color: 4
Size: 314105 Color: 3
Size: 168180 Color: 4

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 517868 Color: 3
Size: 314032 Color: 4
Size: 168101 Color: 3

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 517962 Color: 2
Size: 318531 Color: 4
Size: 163508 Color: 3

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 518020 Color: 1
Size: 310099 Color: 3
Size: 171882 Color: 2

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 518063 Color: 0
Size: 355804 Color: 2
Size: 126134 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 518230 Color: 1
Size: 310511 Color: 3
Size: 171260 Color: 3

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 518283 Color: 3
Size: 314215 Color: 4
Size: 167503 Color: 2

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 518288 Color: 3
Size: 312367 Color: 2
Size: 169346 Color: 4

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 518405 Color: 2
Size: 314323 Color: 4
Size: 167273 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 518502 Color: 1
Size: 325999 Color: 4
Size: 155500 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 518619 Color: 1
Size: 351306 Color: 2
Size: 130076 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 518662 Color: 2
Size: 321975 Color: 4
Size: 159364 Color: 4

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 518679 Color: 4
Size: 354543 Color: 1
Size: 126779 Color: 4

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 518855 Color: 4
Size: 310059 Color: 0
Size: 171087 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 518867 Color: 3
Size: 314704 Color: 4
Size: 166430 Color: 1

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 518918 Color: 1
Size: 312525 Color: 4
Size: 168558 Color: 4

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 518928 Color: 4
Size: 312570 Color: 0
Size: 168503 Color: 1

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 518983 Color: 2
Size: 354985 Color: 0
Size: 126033 Color: 3

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 519059 Color: 3
Size: 315975 Color: 2
Size: 164967 Color: 1

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 519156 Color: 3
Size: 325456 Color: 1
Size: 155389 Color: 3

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 519167 Color: 3
Size: 353811 Color: 1
Size: 127023 Color: 2

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 519275 Color: 3
Size: 313829 Color: 2
Size: 166897 Color: 1

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 519287 Color: 3
Size: 324735 Color: 0
Size: 155979 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 519462 Color: 1
Size: 318482 Color: 4
Size: 162057 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 519533 Color: 2
Size: 316894 Color: 0
Size: 163574 Color: 2

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 519545 Color: 1
Size: 358111 Color: 3
Size: 122345 Color: 3

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 519678 Color: 0
Size: 311618 Color: 2
Size: 168705 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 519752 Color: 1
Size: 353428 Color: 0
Size: 126821 Color: 4

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 519956 Color: 4
Size: 313522 Color: 3
Size: 166523 Color: 4

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 520012 Color: 3
Size: 349504 Color: 2
Size: 130485 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 520388 Color: 1
Size: 353285 Color: 4
Size: 126328 Color: 2

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 520978 Color: 0
Size: 361429 Color: 1
Size: 117594 Color: 3

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 521451 Color: 1
Size: 318312 Color: 2
Size: 160238 Color: 3

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 521605 Color: 4
Size: 357475 Color: 2
Size: 120921 Color: 4

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 521935 Color: 4
Size: 318711 Color: 2
Size: 159355 Color: 4

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 521939 Color: 3
Size: 344902 Color: 0
Size: 133160 Color: 1

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 522140 Color: 4
Size: 317303 Color: 0
Size: 160558 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 522298 Color: 3
Size: 317891 Color: 4
Size: 159812 Color: 2

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 522313 Color: 2
Size: 319050 Color: 3
Size: 158638 Color: 1

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 522379 Color: 1
Size: 316696 Color: 4
Size: 160926 Color: 2

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 522503 Color: 2
Size: 355963 Color: 0
Size: 121535 Color: 3

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 522646 Color: 0
Size: 323625 Color: 2
Size: 153730 Color: 4

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 522701 Color: 1
Size: 321633 Color: 0
Size: 155667 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 522759 Color: 3
Size: 317672 Color: 2
Size: 159570 Color: 1

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 522872 Color: 3
Size: 320915 Color: 1
Size: 156214 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 523026 Color: 2
Size: 316723 Color: 1
Size: 160252 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 523106 Color: 1
Size: 321599 Color: 0
Size: 155296 Color: 1

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 523143 Color: 0
Size: 337533 Color: 3
Size: 139325 Color: 4

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 523533 Color: 1
Size: 322067 Color: 3
Size: 154401 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 523665 Color: 4
Size: 324479 Color: 1
Size: 151857 Color: 1

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 523714 Color: 0
Size: 327468 Color: 2
Size: 148819 Color: 4

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 523903 Color: 0
Size: 317481 Color: 2
Size: 158617 Color: 4

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 524123 Color: 1
Size: 324419 Color: 4
Size: 151459 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 524127 Color: 1
Size: 321160 Color: 3
Size: 154714 Color: 3

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 524316 Color: 2
Size: 317390 Color: 0
Size: 158295 Color: 1

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 524394 Color: 3
Size: 352376 Color: 0
Size: 123231 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 524420 Color: 0
Size: 325140 Color: 3
Size: 150441 Color: 4

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 524478 Color: 2
Size: 320764 Color: 3
Size: 154759 Color: 2

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 524825 Color: 4
Size: 352930 Color: 2
Size: 122246 Color: 4

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 524863 Color: 2
Size: 324352 Color: 4
Size: 150786 Color: 3

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 524891 Color: 1
Size: 318772 Color: 2
Size: 156338 Color: 4

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 524920 Color: 2
Size: 326524 Color: 3
Size: 148557 Color: 4

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 524923 Color: 2
Size: 316608 Color: 0
Size: 158470 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 525023 Color: 0
Size: 317712 Color: 1
Size: 157266 Color: 2

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 525580 Color: 4
Size: 351130 Color: 3
Size: 123291 Color: 2

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 525649 Color: 3
Size: 318998 Color: 1
Size: 155354 Color: 1

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 525657 Color: 3
Size: 317251 Color: 4
Size: 157093 Color: 2

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 525771 Color: 4
Size: 352869 Color: 0
Size: 121361 Color: 4

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 525827 Color: 3
Size: 320821 Color: 4
Size: 153353 Color: 1

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 525847 Color: 1
Size: 321656 Color: 4
Size: 152498 Color: 4

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 525911 Color: 0
Size: 322213 Color: 3
Size: 151877 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 526000 Color: 1
Size: 319016 Color: 0
Size: 154985 Color: 3

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 526460 Color: 1
Size: 352227 Color: 0
Size: 121314 Color: 1

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 526541 Color: 0
Size: 352346 Color: 1
Size: 121114 Color: 2

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 526548 Color: 3
Size: 355422 Color: 2
Size: 118031 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 526615 Color: 0
Size: 321614 Color: 2
Size: 151772 Color: 2

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 526640 Color: 3
Size: 349132 Color: 1
Size: 124229 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 526672 Color: 4
Size: 354712 Color: 2
Size: 118617 Color: 2

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 526686 Color: 4
Size: 318432 Color: 3
Size: 154883 Color: 3

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 526701 Color: 1
Size: 316957 Color: 3
Size: 156343 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 526905 Color: 2
Size: 320726 Color: 4
Size: 152370 Color: 1

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 526944 Color: 0
Size: 323431 Color: 4
Size: 149626 Color: 1

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 526977 Color: 2
Size: 319522 Color: 1
Size: 153502 Color: 2

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 527056 Color: 0
Size: 322542 Color: 1
Size: 150403 Color: 2

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 527361 Color: 0
Size: 353905 Color: 1
Size: 118735 Color: 4

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 527485 Color: 0
Size: 321158 Color: 3
Size: 151358 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 527535 Color: 1
Size: 323368 Color: 4
Size: 149098 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 527546 Color: 4
Size: 320963 Color: 1
Size: 151492 Color: 1

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 527742 Color: 3
Size: 320337 Color: 1
Size: 151922 Color: 1

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 527867 Color: 0
Size: 472134 Color: 2

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 528002 Color: 4
Size: 324951 Color: 0
Size: 147048 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 528007 Color: 2
Size: 350197 Color: 0
Size: 121797 Color: 2

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 528302 Color: 0
Size: 349767 Color: 2
Size: 121932 Color: 1

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 528315 Color: 4
Size: 327004 Color: 1
Size: 144682 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 528334 Color: 3
Size: 354825 Color: 1
Size: 116842 Color: 1

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 528426 Color: 2
Size: 322038 Color: 1
Size: 149537 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 528504 Color: 0
Size: 321812 Color: 1
Size: 149685 Color: 1

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 528681 Color: 4
Size: 322654 Color: 3
Size: 148666 Color: 2

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 528711 Color: 2
Size: 317432 Color: 4
Size: 153858 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 528829 Color: 0
Size: 318360 Color: 2
Size: 152812 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 528898 Color: 4
Size: 320245 Color: 3
Size: 150858 Color: 2

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 529132 Color: 3
Size: 319121 Color: 2
Size: 151748 Color: 4

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 529157 Color: 0
Size: 322739 Color: 3
Size: 148105 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 529163 Color: 2
Size: 323642 Color: 1
Size: 147196 Color: 3

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 529236 Color: 0
Size: 318999 Color: 3
Size: 151766 Color: 2

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 529368 Color: 2
Size: 322958 Color: 0
Size: 147675 Color: 4

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 529493 Color: 3
Size: 356186 Color: 4
Size: 114322 Color: 0

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 529625 Color: 1
Size: 350714 Color: 0
Size: 119662 Color: 2

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 529645 Color: 4
Size: 359575 Color: 2
Size: 110781 Color: 3

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 529705 Color: 1
Size: 319997 Color: 0
Size: 150299 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 529765 Color: 4
Size: 326432 Color: 3
Size: 143804 Color: 4

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 529771 Color: 2
Size: 344451 Color: 0
Size: 125779 Color: 2

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 529971 Color: 0
Size: 325392 Color: 2
Size: 144638 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 530028 Color: 3
Size: 362973 Color: 2
Size: 107000 Color: 1

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 530085 Color: 0
Size: 345700 Color: 4
Size: 124216 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 530616 Color: 1
Size: 344942 Color: 3
Size: 124443 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 530652 Color: 3
Size: 327465 Color: 2
Size: 141884 Color: 2

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 530804 Color: 2
Size: 325396 Color: 1
Size: 143801 Color: 4

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 530826 Color: 1
Size: 325355 Color: 0
Size: 143820 Color: 3

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 531028 Color: 1
Size: 324675 Color: 4
Size: 144298 Color: 4

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 531452 Color: 1
Size: 326984 Color: 4
Size: 141565 Color: 4

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 531652 Color: 1
Size: 326117 Color: 2
Size: 142232 Color: 4

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 531723 Color: 3
Size: 325149 Color: 0
Size: 143129 Color: 1

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 531745 Color: 3
Size: 358109 Color: 4
Size: 110147 Color: 3

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 531962 Color: 1
Size: 326066 Color: 2
Size: 141973 Color: 2

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 532132 Color: 3
Size: 348882 Color: 0
Size: 118987 Color: 2

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 532152 Color: 4
Size: 344316 Color: 2
Size: 123533 Color: 2

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 532220 Color: 3
Size: 326146 Color: 0
Size: 141635 Color: 3

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 532306 Color: 1
Size: 325215 Color: 2
Size: 142480 Color: 4

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 532676 Color: 1
Size: 326345 Color: 2
Size: 140980 Color: 2

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 532760 Color: 3
Size: 350392 Color: 4
Size: 116849 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 532898 Color: 3
Size: 345276 Color: 0
Size: 121827 Color: 4

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 532952 Color: 4
Size: 325167 Color: 1
Size: 141882 Color: 1

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 532971 Color: 4
Size: 340147 Color: 0
Size: 126883 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 533156 Color: 1
Size: 341325 Color: 3
Size: 125520 Color: 1

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 533164 Color: 2
Size: 324572 Color: 4
Size: 142265 Color: 0

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 533229 Color: 3
Size: 466772 Color: 2

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 534165 Color: 3
Size: 326962 Color: 0
Size: 138874 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 534474 Color: 1
Size: 325908 Color: 2
Size: 139619 Color: 1

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 534861 Color: 1
Size: 341528 Color: 3
Size: 123612 Color: 3

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 534913 Color: 4
Size: 343978 Color: 1
Size: 121110 Color: 3

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 535117 Color: 1
Size: 352852 Color: 4
Size: 112032 Color: 4

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 535280 Color: 1
Size: 351856 Color: 3
Size: 112865 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 535370 Color: 0
Size: 343206 Color: 4
Size: 121425 Color: 1

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 535375 Color: 2
Size: 350678 Color: 4
Size: 113948 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 535476 Color: 4
Size: 334367 Color: 0
Size: 130158 Color: 3

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 535518 Color: 1
Size: 352091 Color: 2
Size: 112392 Color: 1

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 536051 Color: 2
Size: 325717 Color: 4
Size: 138233 Color: 4

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 536292 Color: 0
Size: 347484 Color: 4
Size: 116225 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 536477 Color: 3
Size: 326883 Color: 2
Size: 136641 Color: 2

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 536602 Color: 1
Size: 329936 Color: 4
Size: 133463 Color: 3

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 536616 Color: 3
Size: 332407 Color: 0
Size: 130978 Color: 4

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 536619 Color: 1
Size: 349896 Color: 0
Size: 113486 Color: 3

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 536624 Color: 1
Size: 327376 Color: 0
Size: 136001 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 536670 Color: 0
Size: 351562 Color: 3
Size: 111769 Color: 2

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 536889 Color: 1
Size: 327574 Color: 3
Size: 135538 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 536979 Color: 4
Size: 330999 Color: 3
Size: 132023 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 537225 Color: 3
Size: 328173 Color: 4
Size: 134603 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 537269 Color: 2
Size: 350134 Color: 3
Size: 112598 Color: 1

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 537281 Color: 0
Size: 326657 Color: 3
Size: 136063 Color: 4

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 537810 Color: 0
Size: 328801 Color: 1
Size: 133390 Color: 1

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 537837 Color: 4
Size: 336413 Color: 2
Size: 125751 Color: 2

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 537867 Color: 2
Size: 334342 Color: 1
Size: 127792 Color: 3

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 538013 Color: 0
Size: 330988 Color: 1
Size: 131000 Color: 4

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 538167 Color: 2
Size: 354504 Color: 3
Size: 107330 Color: 3

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 538205 Color: 0
Size: 331009 Color: 1
Size: 130787 Color: 2

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 538519 Color: 1
Size: 328841 Color: 0
Size: 132641 Color: 4

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 538600 Color: 2
Size: 346971 Color: 4
Size: 114430 Color: 2

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 538668 Color: 2
Size: 350391 Color: 4
Size: 110942 Color: 1

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 539136 Color: 2
Size: 335433 Color: 0
Size: 125432 Color: 1

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 539471 Color: 4
Size: 353353 Color: 2
Size: 107177 Color: 2

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 539493 Color: 0
Size: 342845 Color: 4
Size: 117663 Color: 4

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 539620 Color: 1
Size: 350561 Color: 4
Size: 109820 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 539680 Color: 4
Size: 331443 Color: 0
Size: 128878 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 539859 Color: 4
Size: 340298 Color: 2
Size: 119844 Color: 3

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 539962 Color: 1
Size: 334172 Color: 0
Size: 125867 Color: 3

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 540132 Color: 0
Size: 330393 Color: 2
Size: 129476 Color: 4

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 540548 Color: 1
Size: 347361 Color: 4
Size: 112092 Color: 4

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 540804 Color: 1
Size: 344299 Color: 3
Size: 114898 Color: 4

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 540863 Color: 1
Size: 331264 Color: 0
Size: 127874 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 541025 Color: 0
Size: 347448 Color: 3
Size: 111528 Color: 4

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 541078 Color: 3
Size: 345381 Color: 4
Size: 113542 Color: 4

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 541165 Color: 2
Size: 337410 Color: 1
Size: 121426 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 541283 Color: 3
Size: 339380 Color: 4
Size: 119338 Color: 4

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 541632 Color: 2
Size: 346586 Color: 0
Size: 111783 Color: 2

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 541815 Color: 0
Size: 329696 Color: 1
Size: 128490 Color: 4

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 542042 Color: 3
Size: 353195 Color: 0
Size: 104764 Color: 3

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 542048 Color: 0
Size: 335818 Color: 1
Size: 122135 Color: 1

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 542106 Color: 4
Size: 331592 Color: 1
Size: 126303 Color: 3

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 542325 Color: 1
Size: 345057 Color: 3
Size: 112619 Color: 3

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 542674 Color: 1
Size: 350816 Color: 2
Size: 106511 Color: 1

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 542960 Color: 4
Size: 330894 Color: 3
Size: 126147 Color: 4

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 543176 Color: 3
Size: 332477 Color: 1
Size: 124348 Color: 3

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 543183 Color: 4
Size: 335914 Color: 3
Size: 120904 Color: 3

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 543308 Color: 1
Size: 348809 Color: 4
Size: 107884 Color: 1

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 543325 Color: 2
Size: 352923 Color: 0
Size: 103753 Color: 1

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 543381 Color: 3
Size: 348251 Color: 0
Size: 108369 Color: 3

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 543411 Color: 1
Size: 353213 Color: 0
Size: 103377 Color: 2

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 543544 Color: 2
Size: 332126 Color: 4
Size: 124331 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 543607 Color: 0
Size: 336317 Color: 4
Size: 120077 Color: 1

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 543844 Color: 3
Size: 337036 Color: 4
Size: 119121 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 544033 Color: 1
Size: 351195 Color: 2
Size: 104773 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 544215 Color: 1
Size: 336174 Color: 3
Size: 119612 Color: 4

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 544266 Color: 2
Size: 341407 Color: 1
Size: 114328 Color: 4

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 544329 Color: 1
Size: 337140 Color: 0
Size: 118532 Color: 3

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 544718 Color: 2
Size: 346402 Color: 4
Size: 108881 Color: 4

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 544802 Color: 4
Size: 327698 Color: 1
Size: 127501 Color: 2

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 544867 Color: 4
Size: 327475 Color: 1
Size: 127659 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 544905 Color: 0
Size: 340756 Color: 4
Size: 114340 Color: 1

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 544937 Color: 0
Size: 349855 Color: 2
Size: 105209 Color: 4

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 545132 Color: 4
Size: 342846 Color: 2
Size: 112023 Color: 4

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 545179 Color: 1
Size: 330209 Color: 3
Size: 124613 Color: 4

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 545182 Color: 3
Size: 345303 Color: 1
Size: 109516 Color: 2

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 545216 Color: 0
Size: 353389 Color: 3
Size: 101396 Color: 4

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 545224 Color: 1
Size: 348647 Color: 0
Size: 106130 Color: 3

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 545229 Color: 4
Size: 337401 Color: 0
Size: 117371 Color: 1

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 545234 Color: 4
Size: 351793 Color: 1
Size: 102974 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 545306 Color: 3
Size: 338298 Color: 0
Size: 116397 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 545342 Color: 3
Size: 352679 Color: 2
Size: 101980 Color: 4

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 545633 Color: 1
Size: 351018 Color: 0
Size: 103350 Color: 3

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 545646 Color: 0
Size: 345018 Color: 2
Size: 109337 Color: 4

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 545681 Color: 2
Size: 454320 Color: 3

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 545769 Color: 1
Size: 333274 Color: 3
Size: 120958 Color: 3

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 545785 Color: 0
Size: 339236 Color: 2
Size: 114980 Color: 3

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 545812 Color: 4
Size: 332387 Color: 3
Size: 121802 Color: 3

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 545848 Color: 3
Size: 350146 Color: 2
Size: 104007 Color: 2

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 545938 Color: 1
Size: 351662 Color: 4
Size: 102401 Color: 3

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 545979 Color: 4
Size: 334933 Color: 2
Size: 119089 Color: 2

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 546010 Color: 3
Size: 352824 Color: 2
Size: 101167 Color: 3

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 546029 Color: 3
Size: 344399 Color: 2
Size: 109573 Color: 3

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 546066 Color: 0
Size: 328634 Color: 3
Size: 125301 Color: 2

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 546463 Color: 3
Size: 349774 Color: 0
Size: 103764 Color: 4

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 546480 Color: 2
Size: 335928 Color: 4
Size: 117593 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 546516 Color: 0
Size: 331706 Color: 1
Size: 121779 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 546605 Color: 4
Size: 333940 Color: 3
Size: 119456 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 546915 Color: 4
Size: 328203 Color: 1
Size: 124883 Color: 1

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 546998 Color: 1
Size: 344819 Color: 3
Size: 108184 Color: 1

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 547035 Color: 4
Size: 329530 Color: 1
Size: 123436 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 547134 Color: 2
Size: 345620 Color: 3
Size: 107247 Color: 2

Bin 715: 0 of cap free
Amount of items: 2
Items: 
Size: 547265 Color: 0
Size: 452736 Color: 3

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 547340 Color: 1
Size: 333499 Color: 2
Size: 119162 Color: 3

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 547377 Color: 4
Size: 349040 Color: 2
Size: 103584 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 547431 Color: 2
Size: 350861 Color: 0
Size: 101709 Color: 1

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 547630 Color: 4
Size: 346967 Color: 2
Size: 105404 Color: 2

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 547687 Color: 3
Size: 337270 Color: 0
Size: 115044 Color: 1

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 547724 Color: 1
Size: 341176 Color: 0
Size: 111101 Color: 4

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 547746 Color: 4
Size: 334421 Color: 0
Size: 117834 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 547799 Color: 0
Size: 350944 Color: 1
Size: 101258 Color: 2

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 547851 Color: 1
Size: 343272 Color: 2
Size: 108878 Color: 2

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 547856 Color: 4
Size: 349699 Color: 2
Size: 102446 Color: 2

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 547864 Color: 0
Size: 329793 Color: 3
Size: 122344 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 547968 Color: 4
Size: 340370 Color: 1
Size: 111663 Color: 1

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 548092 Color: 1
Size: 333207 Color: 0
Size: 118702 Color: 4

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 548106 Color: 0
Size: 333464 Color: 2
Size: 118431 Color: 2

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 548111 Color: 4
Size: 345869 Color: 3
Size: 106021 Color: 2

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 548160 Color: 1
Size: 346198 Color: 2
Size: 105643 Color: 3

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 548198 Color: 0
Size: 327862 Color: 1
Size: 123941 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 548255 Color: 4
Size: 338182 Color: 0
Size: 113564 Color: 3

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 548291 Color: 2
Size: 329061 Color: 3
Size: 122649 Color: 3

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 548383 Color: 2
Size: 350475 Color: 4
Size: 101143 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 548421 Color: 1
Size: 351288 Color: 2
Size: 100292 Color: 1

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 548516 Color: 2
Size: 347407 Color: 1
Size: 104078 Color: 4

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 548567 Color: 2
Size: 347011 Color: 3
Size: 104423 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 548601 Color: 4
Size: 349988 Color: 2
Size: 101412 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 548604 Color: 4
Size: 344250 Color: 2
Size: 107147 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 548807 Color: 1
Size: 349406 Color: 3
Size: 101788 Color: 1

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 548932 Color: 3
Size: 348045 Color: 2
Size: 103024 Color: 3

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 549150 Color: 3
Size: 338502 Color: 0
Size: 112349 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 549451 Color: 3
Size: 338281 Color: 2
Size: 112269 Color: 1

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 549518 Color: 1
Size: 340757 Color: 0
Size: 109726 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 549612 Color: 0
Size: 339201 Color: 4
Size: 111188 Color: 2

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 549625 Color: 3
Size: 332594 Color: 4
Size: 117782 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 549878 Color: 4
Size: 347765 Color: 2
Size: 102358 Color: 3

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 550236 Color: 1
Size: 349615 Color: 3
Size: 100150 Color: 3

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 550248 Color: 3
Size: 333465 Color: 1
Size: 116288 Color: 4

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 550258 Color: 1
Size: 338253 Color: 0
Size: 111490 Color: 4

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 550286 Color: 3
Size: 345072 Color: 2
Size: 104643 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 550451 Color: 0
Size: 341582 Color: 2
Size: 107968 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 550564 Color: 0
Size: 339011 Color: 2
Size: 110426 Color: 4

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 550572 Color: 0
Size: 330108 Color: 3
Size: 119321 Color: 4

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 550735 Color: 2
Size: 337773 Color: 1
Size: 111493 Color: 3

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 550758 Color: 1
Size: 329968 Color: 0
Size: 119275 Color: 2

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 550870 Color: 1
Size: 330909 Color: 3
Size: 118222 Color: 2

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 551138 Color: 3
Size: 348591 Color: 1
Size: 100272 Color: 3

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 551193 Color: 4
Size: 334621 Color: 2
Size: 114187 Color: 1

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 551400 Color: 0
Size: 346871 Color: 1
Size: 101730 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 551481 Color: 4
Size: 332707 Color: 2
Size: 115813 Color: 3

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 551764 Color: 0
Size: 334072 Color: 3
Size: 114165 Color: 1

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 551892 Color: 4
Size: 346310 Color: 2
Size: 101799 Color: 3

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 552010 Color: 3
Size: 330477 Color: 1
Size: 117514 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 552089 Color: 1
Size: 342271 Color: 4
Size: 105641 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 552122 Color: 2
Size: 339628 Color: 3
Size: 108251 Color: 3

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 552280 Color: 0
Size: 327912 Color: 4
Size: 119809 Color: 2

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 552420 Color: 1
Size: 343557 Color: 2
Size: 104024 Color: 2

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 552494 Color: 3
Size: 339172 Color: 1
Size: 108335 Color: 1

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 552555 Color: 2
Size: 341576 Color: 1
Size: 105870 Color: 2

Bin 772: 0 of cap free
Amount of items: 2
Items: 
Size: 552580 Color: 0
Size: 447421 Color: 4

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 552725 Color: 0
Size: 336488 Color: 2
Size: 110788 Color: 1

Bin 774: 0 of cap free
Amount of items: 2
Items: 
Size: 552800 Color: 3
Size: 447201 Color: 1

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 552848 Color: 1
Size: 339937 Color: 3
Size: 107216 Color: 3

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 552863 Color: 3
Size: 343143 Color: 4
Size: 103995 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 552937 Color: 0
Size: 345040 Color: 4
Size: 102024 Color: 3

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 552985 Color: 2
Size: 346572 Color: 0
Size: 100444 Color: 1

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 553236 Color: 0
Size: 339797 Color: 4
Size: 106968 Color: 1

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 553451 Color: 3
Size: 338105 Color: 0
Size: 108445 Color: 4

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 553483 Color: 3
Size: 333303 Color: 1
Size: 113215 Color: 1

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 553559 Color: 0
Size: 344580 Color: 2
Size: 101862 Color: 4

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 553592 Color: 0
Size: 333272 Color: 3
Size: 113137 Color: 3

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 553629 Color: 0
Size: 344555 Color: 4
Size: 101817 Color: 3

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 553658 Color: 4
Size: 335838 Color: 0
Size: 110505 Color: 3

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 553749 Color: 0
Size: 328222 Color: 4
Size: 118030 Color: 1

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 553984 Color: 1
Size: 344118 Color: 2
Size: 101899 Color: 2

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 554040 Color: 0
Size: 340140 Color: 3
Size: 105821 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 554191 Color: 4
Size: 340199 Color: 2
Size: 105611 Color: 2

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 554325 Color: 3
Size: 343173 Color: 2
Size: 102503 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 554360 Color: 0
Size: 332233 Color: 3
Size: 113408 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 554364 Color: 1
Size: 334361 Color: 3
Size: 111276 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 554454 Color: 1
Size: 332240 Color: 2
Size: 113307 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 554491 Color: 3
Size: 340871 Color: 0
Size: 104639 Color: 4

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 554534 Color: 3
Size: 331680 Color: 2
Size: 113787 Color: 1

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 554543 Color: 2
Size: 337191 Color: 4
Size: 108267 Color: 2

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 554727 Color: 1
Size: 343438 Color: 2
Size: 101836 Color: 1

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 554830 Color: 3
Size: 343369 Color: 0
Size: 101802 Color: 1

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 554864 Color: 1
Size: 328753 Color: 2
Size: 116384 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 554899 Color: 1
Size: 337348 Color: 4
Size: 107754 Color: 4

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 554981 Color: 0
Size: 340048 Color: 1
Size: 104972 Color: 3

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 555001 Color: 2
Size: 336092 Color: 0
Size: 108908 Color: 3

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 555120 Color: 2
Size: 340956 Color: 0
Size: 103925 Color: 4

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 555145 Color: 1
Size: 336252 Color: 0
Size: 108604 Color: 4

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 555277 Color: 3
Size: 344243 Color: 0
Size: 100481 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 555288 Color: 0
Size: 342021 Color: 3
Size: 102692 Color: 1

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 555298 Color: 1
Size: 332147 Color: 2
Size: 112556 Color: 3

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 555303 Color: 1
Size: 343044 Color: 3
Size: 101654 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 555436 Color: 4
Size: 343834 Color: 0
Size: 100731 Color: 3

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 555509 Color: 4
Size: 336745 Color: 0
Size: 107747 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 555568 Color: 4
Size: 344018 Color: 0
Size: 100415 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 555737 Color: 1
Size: 332159 Color: 0
Size: 112105 Color: 1

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 555744 Color: 2
Size: 343592 Color: 4
Size: 100665 Color: 2

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 555800 Color: 2
Size: 337974 Color: 4
Size: 106227 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 555907 Color: 0
Size: 334089 Color: 3
Size: 110005 Color: 0

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 555996 Color: 0
Size: 341013 Color: 4
Size: 102992 Color: 4

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 556017 Color: 0
Size: 341742 Color: 1
Size: 102242 Color: 1

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 556033 Color: 4
Size: 338342 Color: 3
Size: 105626 Color: 4

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 556084 Color: 4
Size: 341499 Color: 0
Size: 102418 Color: 2

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 556108 Color: 3
Size: 343041 Color: 4
Size: 100852 Color: 4

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 556123 Color: 4
Size: 339035 Color: 3
Size: 104843 Color: 4

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 556127 Color: 4
Size: 336640 Color: 0
Size: 107234 Color: 1

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 556205 Color: 1
Size: 337206 Color: 0
Size: 106590 Color: 1

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 556208 Color: 1
Size: 341093 Color: 4
Size: 102700 Color: 3

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 556394 Color: 0
Size: 336962 Color: 1
Size: 106645 Color: 1

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 556510 Color: 3
Size: 341415 Color: 4
Size: 102076 Color: 2

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 556540 Color: 3
Size: 333009 Color: 4
Size: 110452 Color: 3

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 556697 Color: 4
Size: 339732 Color: 3
Size: 103572 Color: 1

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 556802 Color: 0
Size: 333680 Color: 3
Size: 109519 Color: 2

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 557011 Color: 1
Size: 341534 Color: 0
Size: 101456 Color: 1

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 557085 Color: 2
Size: 331503 Color: 0
Size: 111413 Color: 1

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 557205 Color: 0
Size: 333190 Color: 1
Size: 109606 Color: 3

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 557446 Color: 3
Size: 341929 Color: 4
Size: 100626 Color: 1

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 557473 Color: 1
Size: 339312 Color: 4
Size: 103216 Color: 3

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 557507 Color: 0
Size: 340690 Color: 1
Size: 101804 Color: 1

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 557622 Color: 3
Size: 330881 Color: 1
Size: 111498 Color: 4

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 557683 Color: 0
Size: 338233 Color: 3
Size: 104085 Color: 4

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 557719 Color: 2
Size: 342253 Color: 4
Size: 100029 Color: 4

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 557729 Color: 4
Size: 331103 Color: 0
Size: 111169 Color: 4

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 557762 Color: 1
Size: 331028 Color: 4
Size: 111211 Color: 4

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 557865 Color: 2
Size: 341820 Color: 0
Size: 100316 Color: 2

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 557974 Color: 1
Size: 341432 Color: 4
Size: 100595 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 557995 Color: 2
Size: 338374 Color: 0
Size: 103632 Color: 3

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 558027 Color: 3
Size: 338831 Color: 0
Size: 103143 Color: 3

Bin 845: 1 of cap free
Amount of items: 4
Items: 
Size: 417324 Color: 2
Size: 194929 Color: 1
Size: 193874 Color: 4
Size: 193873 Color: 4

Bin 846: 1 of cap free
Amount of items: 4
Items: 
Size: 419852 Color: 3
Size: 199305 Color: 4
Size: 190553 Color: 0
Size: 190290 Color: 1

Bin 847: 1 of cap free
Amount of items: 3
Items: 
Size: 422208 Color: 3
Size: 392376 Color: 4
Size: 185416 Color: 3

Bin 848: 1 of cap free
Amount of items: 3
Items: 
Size: 428178 Color: 3
Size: 389986 Color: 0
Size: 181836 Color: 0

Bin 849: 1 of cap free
Amount of items: 3
Items: 
Size: 446966 Color: 0
Size: 382394 Color: 2
Size: 170640 Color: 4

Bin 850: 1 of cap free
Amount of items: 3
Items: 
Size: 448389 Color: 3
Size: 378757 Color: 2
Size: 172854 Color: 0

Bin 851: 1 of cap free
Amount of items: 3
Items: 
Size: 448529 Color: 2
Size: 281133 Color: 1
Size: 270338 Color: 1

Bin 852: 1 of cap free
Amount of items: 3
Items: 
Size: 448814 Color: 1
Size: 277679 Color: 4
Size: 273507 Color: 2

Bin 853: 1 of cap free
Amount of items: 3
Items: 
Size: 450864 Color: 2
Size: 274661 Color: 1
Size: 274475 Color: 3

Bin 854: 1 of cap free
Amount of items: 3
Items: 
Size: 450902 Color: 3
Size: 275004 Color: 1
Size: 274094 Color: 0

Bin 855: 1 of cap free
Amount of items: 3
Items: 
Size: 451234 Color: 2
Size: 282069 Color: 0
Size: 266697 Color: 2

Bin 856: 1 of cap free
Amount of items: 3
Items: 
Size: 451373 Color: 1
Size: 279435 Color: 4
Size: 269192 Color: 3

Bin 857: 1 of cap free
Amount of items: 3
Items: 
Size: 452553 Color: 4
Size: 281797 Color: 3
Size: 265650 Color: 1

Bin 858: 1 of cap free
Amount of items: 3
Items: 
Size: 452658 Color: 3
Size: 379394 Color: 4
Size: 167948 Color: 3

Bin 859: 1 of cap free
Amount of items: 3
Items: 
Size: 453465 Color: 3
Size: 282046 Color: 0
Size: 264489 Color: 0

Bin 860: 1 of cap free
Amount of items: 3
Items: 
Size: 453740 Color: 2
Size: 277390 Color: 1
Size: 268870 Color: 1

Bin 861: 1 of cap free
Amount of items: 3
Items: 
Size: 453846 Color: 3
Size: 376950 Color: 1
Size: 169204 Color: 1

Bin 862: 1 of cap free
Amount of items: 3
Items: 
Size: 454215 Color: 2
Size: 275469 Color: 4
Size: 270316 Color: 2

Bin 863: 1 of cap free
Amount of items: 3
Items: 
Size: 454358 Color: 3
Size: 275772 Color: 0
Size: 269870 Color: 2

Bin 864: 1 of cap free
Amount of items: 3
Items: 
Size: 454492 Color: 3
Size: 278443 Color: 2
Size: 267065 Color: 0

Bin 865: 1 of cap free
Amount of items: 3
Items: 
Size: 454549 Color: 0
Size: 275073 Color: 2
Size: 270378 Color: 0

Bin 866: 1 of cap free
Amount of items: 3
Items: 
Size: 454672 Color: 1
Size: 379991 Color: 2
Size: 165337 Color: 4

Bin 867: 1 of cap free
Amount of items: 3
Items: 
Size: 454749 Color: 1
Size: 283301 Color: 0
Size: 261950 Color: 2

Bin 868: 1 of cap free
Amount of items: 3
Items: 
Size: 454790 Color: 0
Size: 282248 Color: 1
Size: 262962 Color: 3

Bin 869: 1 of cap free
Amount of items: 3
Items: 
Size: 454793 Color: 0
Size: 279126 Color: 4
Size: 266081 Color: 1

Bin 870: 1 of cap free
Amount of items: 3
Items: 
Size: 454926 Color: 0
Size: 276897 Color: 4
Size: 268177 Color: 1

Bin 871: 1 of cap free
Amount of items: 3
Items: 
Size: 455241 Color: 0
Size: 384163 Color: 4
Size: 160596 Color: 0

Bin 872: 1 of cap free
Amount of items: 3
Items: 
Size: 455608 Color: 3
Size: 279526 Color: 1
Size: 264866 Color: 2

Bin 873: 1 of cap free
Amount of items: 3
Items: 
Size: 455692 Color: 2
Size: 275177 Color: 4
Size: 269131 Color: 0

Bin 874: 1 of cap free
Amount of items: 3
Items: 
Size: 456417 Color: 2
Size: 381119 Color: 3
Size: 162464 Color: 2

Bin 875: 1 of cap free
Amount of items: 3
Items: 
Size: 456690 Color: 4
Size: 299036 Color: 2
Size: 244274 Color: 2

Bin 876: 1 of cap free
Amount of items: 3
Items: 
Size: 457362 Color: 0
Size: 377941 Color: 1
Size: 164697 Color: 2

Bin 877: 1 of cap free
Amount of items: 3
Items: 
Size: 457508 Color: 2
Size: 277440 Color: 0
Size: 265052 Color: 0

Bin 878: 1 of cap free
Amount of items: 3
Items: 
Size: 457622 Color: 4
Size: 277491 Color: 0
Size: 264887 Color: 1

Bin 879: 1 of cap free
Amount of items: 3
Items: 
Size: 458664 Color: 3
Size: 376392 Color: 0
Size: 164944 Color: 4

Bin 880: 1 of cap free
Amount of items: 3
Items: 
Size: 458879 Color: 2
Size: 277251 Color: 0
Size: 263870 Color: 4

Bin 881: 1 of cap free
Amount of items: 3
Items: 
Size: 459026 Color: 2
Size: 377143 Color: 1
Size: 163831 Color: 4

Bin 882: 1 of cap free
Amount of items: 3
Items: 
Size: 459092 Color: 0
Size: 280233 Color: 3
Size: 260675 Color: 4

Bin 883: 1 of cap free
Amount of items: 3
Items: 
Size: 459207 Color: 1
Size: 368430 Color: 4
Size: 172363 Color: 1

Bin 884: 1 of cap free
Amount of items: 3
Items: 
Size: 459494 Color: 4
Size: 277278 Color: 3
Size: 263228 Color: 1

Bin 885: 1 of cap free
Amount of items: 3
Items: 
Size: 459571 Color: 4
Size: 281838 Color: 0
Size: 258591 Color: 1

Bin 886: 1 of cap free
Amount of items: 3
Items: 
Size: 459614 Color: 4
Size: 379300 Color: 3
Size: 161086 Color: 4

Bin 887: 1 of cap free
Amount of items: 3
Items: 
Size: 459920 Color: 4
Size: 279566 Color: 1
Size: 260514 Color: 2

Bin 888: 1 of cap free
Amount of items: 3
Items: 
Size: 459930 Color: 1
Size: 379521 Color: 3
Size: 160549 Color: 3

Bin 889: 1 of cap free
Amount of items: 3
Items: 
Size: 460174 Color: 3
Size: 276347 Color: 2
Size: 263479 Color: 1

Bin 890: 1 of cap free
Amount of items: 3
Items: 
Size: 460291 Color: 4
Size: 282242 Color: 0
Size: 257467 Color: 1

Bin 891: 1 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 1
Size: 378442 Color: 2
Size: 161079 Color: 0

Bin 892: 1 of cap free
Amount of items: 3
Items: 
Size: 460785 Color: 3
Size: 279200 Color: 2
Size: 260015 Color: 1

Bin 893: 1 of cap free
Amount of items: 3
Items: 
Size: 461160 Color: 0
Size: 280779 Color: 1
Size: 258061 Color: 2

Bin 894: 1 of cap free
Amount of items: 3
Items: 
Size: 461521 Color: 1
Size: 275589 Color: 2
Size: 262890 Color: 2

Bin 895: 1 of cap free
Amount of items: 3
Items: 
Size: 461539 Color: 2
Size: 279138 Color: 3
Size: 259323 Color: 0

Bin 896: 1 of cap free
Amount of items: 3
Items: 
Size: 461549 Color: 2
Size: 277592 Color: 0
Size: 260859 Color: 4

Bin 897: 1 of cap free
Amount of items: 3
Items: 
Size: 462645 Color: 4
Size: 282142 Color: 3
Size: 255213 Color: 3

Bin 898: 1 of cap free
Amount of items: 3
Items: 
Size: 463018 Color: 4
Size: 281788 Color: 3
Size: 255194 Color: 4

Bin 899: 1 of cap free
Amount of items: 3
Items: 
Size: 464528 Color: 0
Size: 374890 Color: 4
Size: 160582 Color: 4

Bin 900: 1 of cap free
Amount of items: 3
Items: 
Size: 464819 Color: 0
Size: 283735 Color: 4
Size: 251446 Color: 4

Bin 901: 1 of cap free
Amount of items: 3
Items: 
Size: 464924 Color: 0
Size: 283788 Color: 1
Size: 251288 Color: 3

Bin 902: 1 of cap free
Amount of items: 3
Items: 
Size: 465038 Color: 2
Size: 283396 Color: 1
Size: 251566 Color: 3

Bin 903: 1 of cap free
Amount of items: 3
Items: 
Size: 465731 Color: 0
Size: 283788 Color: 3
Size: 250481 Color: 2

Bin 904: 1 of cap free
Amount of items: 3
Items: 
Size: 466658 Color: 0
Size: 379103 Color: 4
Size: 154239 Color: 1

Bin 905: 1 of cap free
Amount of items: 3
Items: 
Size: 466715 Color: 0
Size: 285769 Color: 1
Size: 247516 Color: 1

Bin 906: 1 of cap free
Amount of items: 3
Items: 
Size: 466860 Color: 4
Size: 287110 Color: 1
Size: 246030 Color: 2

Bin 907: 1 of cap free
Amount of items: 3
Items: 
Size: 468028 Color: 1
Size: 369445 Color: 0
Size: 162527 Color: 1

Bin 908: 1 of cap free
Amount of items: 3
Items: 
Size: 468044 Color: 2
Size: 373963 Color: 1
Size: 157993 Color: 3

Bin 909: 1 of cap free
Amount of items: 3
Items: 
Size: 468992 Color: 2
Size: 293647 Color: 1
Size: 237361 Color: 4

Bin 910: 1 of cap free
Amount of items: 3
Items: 
Size: 469258 Color: 2
Size: 288952 Color: 3
Size: 241790 Color: 4

Bin 911: 1 of cap free
Amount of items: 3
Items: 
Size: 469299 Color: 1
Size: 285883 Color: 0
Size: 244818 Color: 0

Bin 912: 1 of cap free
Amount of items: 3
Items: 
Size: 470129 Color: 1
Size: 289480 Color: 3
Size: 240391 Color: 1

Bin 913: 1 of cap free
Amount of items: 3
Items: 
Size: 470280 Color: 1
Size: 369645 Color: 2
Size: 160075 Color: 4

Bin 914: 1 of cap free
Amount of items: 3
Items: 
Size: 471116 Color: 3
Size: 289071 Color: 1
Size: 239813 Color: 3

Bin 915: 1 of cap free
Amount of items: 3
Items: 
Size: 472133 Color: 3
Size: 295057 Color: 1
Size: 232810 Color: 0

Bin 916: 1 of cap free
Amount of items: 3
Items: 
Size: 472275 Color: 0
Size: 291304 Color: 2
Size: 236421 Color: 4

Bin 917: 1 of cap free
Amount of items: 3
Items: 
Size: 472315 Color: 1
Size: 288692 Color: 0
Size: 238993 Color: 4

Bin 918: 1 of cap free
Amount of items: 3
Items: 
Size: 472394 Color: 3
Size: 294388 Color: 4
Size: 233218 Color: 3

Bin 919: 1 of cap free
Amount of items: 3
Items: 
Size: 472548 Color: 2
Size: 373381 Color: 0
Size: 154071 Color: 0

Bin 920: 1 of cap free
Amount of items: 3
Items: 
Size: 472613 Color: 1
Size: 286691 Color: 2
Size: 240696 Color: 1

Bin 921: 1 of cap free
Amount of items: 3
Items: 
Size: 472778 Color: 0
Size: 287426 Color: 2
Size: 239796 Color: 3

Bin 922: 1 of cap free
Amount of items: 3
Items: 
Size: 472781 Color: 1
Size: 295734 Color: 2
Size: 231485 Color: 2

Bin 923: 1 of cap free
Amount of items: 3
Items: 
Size: 472957 Color: 0
Size: 374771 Color: 2
Size: 152272 Color: 2

Bin 924: 1 of cap free
Amount of items: 3
Items: 
Size: 473483 Color: 3
Size: 286570 Color: 1
Size: 239947 Color: 4

Bin 925: 1 of cap free
Amount of items: 3
Items: 
Size: 473895 Color: 0
Size: 287816 Color: 1
Size: 238289 Color: 2

Bin 926: 1 of cap free
Amount of items: 3
Items: 
Size: 473994 Color: 0
Size: 363059 Color: 3
Size: 162947 Color: 3

Bin 927: 1 of cap free
Amount of items: 3
Items: 
Size: 474493 Color: 2
Size: 375025 Color: 1
Size: 150482 Color: 0

Bin 928: 1 of cap free
Amount of items: 3
Items: 
Size: 475001 Color: 4
Size: 289698 Color: 3
Size: 235301 Color: 3

Bin 929: 1 of cap free
Amount of items: 3
Items: 
Size: 475187 Color: 3
Size: 377881 Color: 0
Size: 146932 Color: 1

Bin 930: 1 of cap free
Amount of items: 3
Items: 
Size: 475194 Color: 1
Size: 289317 Color: 3
Size: 235489 Color: 2

Bin 931: 1 of cap free
Amount of items: 3
Items: 
Size: 475452 Color: 2
Size: 291315 Color: 0
Size: 233233 Color: 0

Bin 932: 1 of cap free
Amount of items: 3
Items: 
Size: 475751 Color: 2
Size: 289905 Color: 1
Size: 234344 Color: 4

Bin 933: 1 of cap free
Amount of items: 3
Items: 
Size: 476643 Color: 0
Size: 292534 Color: 4
Size: 230823 Color: 2

Bin 934: 1 of cap free
Amount of items: 3
Items: 
Size: 477152 Color: 1
Size: 290702 Color: 2
Size: 232146 Color: 3

Bin 935: 1 of cap free
Amount of items: 3
Items: 
Size: 477324 Color: 2
Size: 370560 Color: 3
Size: 152116 Color: 3

Bin 936: 1 of cap free
Amount of items: 3
Items: 
Size: 477828 Color: 1
Size: 290228 Color: 3
Size: 231944 Color: 4

Bin 937: 1 of cap free
Amount of items: 3
Items: 
Size: 477829 Color: 3
Size: 368621 Color: 2
Size: 153550 Color: 3

Bin 938: 1 of cap free
Amount of items: 3
Items: 
Size: 478219 Color: 3
Size: 371981 Color: 0
Size: 149800 Color: 3

Bin 939: 1 of cap free
Amount of items: 3
Items: 
Size: 478415 Color: 4
Size: 292518 Color: 3
Size: 229067 Color: 1

Bin 940: 1 of cap free
Amount of items: 3
Items: 
Size: 478611 Color: 4
Size: 292036 Color: 1
Size: 229353 Color: 0

Bin 941: 1 of cap free
Amount of items: 3
Items: 
Size: 478926 Color: 4
Size: 296489 Color: 0
Size: 224585 Color: 2

Bin 942: 1 of cap free
Amount of items: 3
Items: 
Size: 479502 Color: 3
Size: 291956 Color: 0
Size: 228542 Color: 2

Bin 943: 1 of cap free
Amount of items: 3
Items: 
Size: 479772 Color: 3
Size: 291624 Color: 4
Size: 228604 Color: 1

Bin 944: 1 of cap free
Amount of items: 3
Items: 
Size: 479883 Color: 0
Size: 291608 Color: 3
Size: 228509 Color: 4

Bin 945: 1 of cap free
Amount of items: 3
Items: 
Size: 480823 Color: 1
Size: 297235 Color: 4
Size: 221942 Color: 2

Bin 946: 1 of cap free
Amount of items: 3
Items: 
Size: 481471 Color: 0
Size: 294147 Color: 3
Size: 224382 Color: 2

Bin 947: 1 of cap free
Amount of items: 3
Items: 
Size: 481655 Color: 2
Size: 303049 Color: 3
Size: 215296 Color: 3

Bin 948: 1 of cap free
Amount of items: 3
Items: 
Size: 482188 Color: 1
Size: 371317 Color: 3
Size: 146495 Color: 2

Bin 949: 1 of cap free
Amount of items: 3
Items: 
Size: 482315 Color: 1
Size: 292951 Color: 4
Size: 224734 Color: 3

Bin 950: 1 of cap free
Amount of items: 3
Items: 
Size: 482423 Color: 4
Size: 295288 Color: 2
Size: 222289 Color: 4

Bin 951: 1 of cap free
Amount of items: 3
Items: 
Size: 483259 Color: 3
Size: 373476 Color: 1
Size: 143265 Color: 4

Bin 952: 1 of cap free
Amount of items: 3
Items: 
Size: 483507 Color: 2
Size: 370878 Color: 0
Size: 145615 Color: 2

Bin 953: 1 of cap free
Amount of items: 3
Items: 
Size: 483510 Color: 4
Size: 373686 Color: 2
Size: 142804 Color: 1

Bin 954: 1 of cap free
Amount of items: 3
Items: 
Size: 483620 Color: 1
Size: 296897 Color: 2
Size: 219483 Color: 0

Bin 955: 1 of cap free
Amount of items: 3
Items: 
Size: 483777 Color: 3
Size: 294044 Color: 2
Size: 222179 Color: 3

Bin 956: 1 of cap free
Amount of items: 3
Items: 
Size: 484677 Color: 4
Size: 298882 Color: 0
Size: 216441 Color: 3

Bin 957: 1 of cap free
Amount of items: 3
Items: 
Size: 485239 Color: 4
Size: 295362 Color: 0
Size: 219399 Color: 2

Bin 958: 1 of cap free
Amount of items: 3
Items: 
Size: 485927 Color: 3
Size: 298625 Color: 0
Size: 215448 Color: 1

Bin 959: 1 of cap free
Amount of items: 3
Items: 
Size: 486166 Color: 2
Size: 297005 Color: 3
Size: 216829 Color: 0

Bin 960: 1 of cap free
Amount of items: 3
Items: 
Size: 487161 Color: 2
Size: 297791 Color: 1
Size: 215048 Color: 3

Bin 961: 1 of cap free
Amount of items: 3
Items: 
Size: 487669 Color: 0
Size: 303269 Color: 1
Size: 209062 Color: 4

Bin 962: 1 of cap free
Amount of items: 3
Items: 
Size: 487737 Color: 4
Size: 298614 Color: 2
Size: 213649 Color: 2

Bin 963: 1 of cap free
Amount of items: 3
Items: 
Size: 488117 Color: 1
Size: 305064 Color: 3
Size: 206819 Color: 3

Bin 964: 1 of cap free
Amount of items: 3
Items: 
Size: 488580 Color: 2
Size: 296036 Color: 1
Size: 215384 Color: 3

Bin 965: 1 of cap free
Amount of items: 3
Items: 
Size: 489684 Color: 3
Size: 295428 Color: 1
Size: 214888 Color: 0

Bin 966: 1 of cap free
Amount of items: 3
Items: 
Size: 489882 Color: 1
Size: 375075 Color: 2
Size: 135043 Color: 4

Bin 967: 1 of cap free
Amount of items: 3
Items: 
Size: 490188 Color: 4
Size: 301785 Color: 0
Size: 208027 Color: 3

Bin 968: 1 of cap free
Amount of items: 3
Items: 
Size: 490303 Color: 0
Size: 366607 Color: 4
Size: 143090 Color: 2

Bin 969: 1 of cap free
Amount of items: 3
Items: 
Size: 490693 Color: 2
Size: 370366 Color: 0
Size: 138941 Color: 4

Bin 970: 1 of cap free
Amount of items: 3
Items: 
Size: 491335 Color: 0
Size: 297263 Color: 4
Size: 211402 Color: 0

Bin 971: 1 of cap free
Amount of items: 3
Items: 
Size: 491355 Color: 4
Size: 300851 Color: 3
Size: 207794 Color: 4

Bin 972: 1 of cap free
Amount of items: 3
Items: 
Size: 492493 Color: 1
Size: 299520 Color: 4
Size: 207987 Color: 4

Bin 973: 1 of cap free
Amount of items: 3
Items: 
Size: 492596 Color: 0
Size: 299501 Color: 2
Size: 207903 Color: 1

Bin 974: 1 of cap free
Amount of items: 3
Items: 
Size: 493761 Color: 1
Size: 303304 Color: 4
Size: 202935 Color: 1

Bin 975: 1 of cap free
Amount of items: 3
Items: 
Size: 494066 Color: 2
Size: 369066 Color: 4
Size: 136868 Color: 4

Bin 976: 1 of cap free
Amount of items: 3
Items: 
Size: 494238 Color: 2
Size: 368893 Color: 0
Size: 136869 Color: 0

Bin 977: 1 of cap free
Amount of items: 3
Items: 
Size: 494905 Color: 3
Size: 369488 Color: 1
Size: 135607 Color: 1

Bin 978: 1 of cap free
Amount of items: 3
Items: 
Size: 495203 Color: 3
Size: 363665 Color: 2
Size: 141132 Color: 2

Bin 979: 1 of cap free
Amount of items: 3
Items: 
Size: 495245 Color: 4
Size: 308064 Color: 1
Size: 196691 Color: 4

Bin 980: 1 of cap free
Amount of items: 3
Items: 
Size: 495718 Color: 4
Size: 361748 Color: 3
Size: 142534 Color: 3

Bin 981: 1 of cap free
Amount of items: 3
Items: 
Size: 495736 Color: 1
Size: 305532 Color: 0
Size: 198732 Color: 3

Bin 982: 1 of cap free
Amount of items: 3
Items: 
Size: 495887 Color: 3
Size: 300547 Color: 2
Size: 203566 Color: 0

Bin 983: 1 of cap free
Amount of items: 3
Items: 
Size: 495951 Color: 4
Size: 371600 Color: 0
Size: 132449 Color: 2

Bin 984: 1 of cap free
Amount of items: 3
Items: 
Size: 496050 Color: 4
Size: 362590 Color: 3
Size: 141360 Color: 0

Bin 985: 1 of cap free
Amount of items: 3
Items: 
Size: 496053 Color: 4
Size: 363284 Color: 2
Size: 140663 Color: 0

Bin 986: 1 of cap free
Amount of items: 3
Items: 
Size: 496344 Color: 0
Size: 299848 Color: 1
Size: 203808 Color: 4

Bin 987: 1 of cap free
Amount of items: 3
Items: 
Size: 496508 Color: 3
Size: 301357 Color: 2
Size: 202135 Color: 2

Bin 988: 1 of cap free
Amount of items: 3
Items: 
Size: 496818 Color: 4
Size: 367188 Color: 0
Size: 135994 Color: 2

Bin 989: 1 of cap free
Amount of items: 3
Items: 
Size: 496850 Color: 4
Size: 373484 Color: 2
Size: 129666 Color: 2

Bin 990: 1 of cap free
Amount of items: 3
Items: 
Size: 497557 Color: 2
Size: 370114 Color: 4
Size: 132329 Color: 3

Bin 991: 1 of cap free
Amount of items: 3
Items: 
Size: 497945 Color: 3
Size: 302002 Color: 1
Size: 200053 Color: 2

Bin 992: 1 of cap free
Amount of items: 3
Items: 
Size: 497946 Color: 4
Size: 366915 Color: 2
Size: 135139 Color: 0

Bin 993: 1 of cap free
Amount of items: 3
Items: 
Size: 497983 Color: 0
Size: 302907 Color: 4
Size: 199110 Color: 3

Bin 994: 1 of cap free
Amount of items: 3
Items: 
Size: 498025 Color: 2
Size: 303322 Color: 4
Size: 198653 Color: 1

Bin 995: 1 of cap free
Amount of items: 3
Items: 
Size: 498145 Color: 0
Size: 308391 Color: 2
Size: 193464 Color: 3

Bin 996: 1 of cap free
Amount of items: 3
Items: 
Size: 498295 Color: 0
Size: 303272 Color: 2
Size: 198433 Color: 2

Bin 997: 1 of cap free
Amount of items: 3
Items: 
Size: 498388 Color: 0
Size: 307584 Color: 1
Size: 194028 Color: 2

Bin 998: 1 of cap free
Amount of items: 3
Items: 
Size: 498494 Color: 1
Size: 364203 Color: 4
Size: 137303 Color: 1

Bin 999: 1 of cap free
Amount of items: 3
Items: 
Size: 498604 Color: 1
Size: 367454 Color: 4
Size: 133942 Color: 3

Bin 1000: 1 of cap free
Amount of items: 3
Items: 
Size: 498763 Color: 1
Size: 367434 Color: 0
Size: 133803 Color: 1

Bin 1001: 1 of cap free
Amount of items: 3
Items: 
Size: 499388 Color: 0
Size: 365410 Color: 1
Size: 135202 Color: 0

Bin 1002: 1 of cap free
Amount of items: 3
Items: 
Size: 499704 Color: 4
Size: 302842 Color: 3
Size: 197454 Color: 1

Bin 1003: 1 of cap free
Amount of items: 3
Items: 
Size: 499998 Color: 3
Size: 310125 Color: 2
Size: 189877 Color: 4

Bin 1004: 1 of cap free
Amount of items: 3
Items: 
Size: 500610 Color: 3
Size: 307053 Color: 2
Size: 192337 Color: 2

Bin 1005: 1 of cap free
Amount of items: 3
Items: 
Size: 500764 Color: 4
Size: 308867 Color: 1
Size: 190369 Color: 4

Bin 1006: 1 of cap free
Amount of items: 3
Items: 
Size: 500771 Color: 3
Size: 360274 Color: 4
Size: 138955 Color: 2

Bin 1007: 1 of cap free
Amount of items: 3
Items: 
Size: 502472 Color: 3
Size: 304449 Color: 4
Size: 193079 Color: 2

Bin 1008: 1 of cap free
Amount of items: 3
Items: 
Size: 503161 Color: 4
Size: 362660 Color: 1
Size: 134179 Color: 4

Bin 1009: 1 of cap free
Amount of items: 3
Items: 
Size: 503289 Color: 4
Size: 313436 Color: 0
Size: 183275 Color: 4

Bin 1010: 1 of cap free
Amount of items: 3
Items: 
Size: 503298 Color: 3
Size: 366640 Color: 1
Size: 130062 Color: 4

Bin 1011: 1 of cap free
Amount of items: 3
Items: 
Size: 503490 Color: 3
Size: 350474 Color: 0
Size: 146036 Color: 0

Bin 1012: 1 of cap free
Amount of items: 3
Items: 
Size: 503574 Color: 4
Size: 366188 Color: 1
Size: 130238 Color: 1

Bin 1013: 1 of cap free
Amount of items: 3
Items: 
Size: 503587 Color: 4
Size: 306780 Color: 2
Size: 189633 Color: 1

Bin 1014: 1 of cap free
Amount of items: 3
Items: 
Size: 504057 Color: 4
Size: 308002 Color: 3
Size: 187941 Color: 4

Bin 1015: 1 of cap free
Amount of items: 3
Items: 
Size: 504172 Color: 0
Size: 353454 Color: 1
Size: 142374 Color: 3

Bin 1016: 1 of cap free
Amount of items: 3
Items: 
Size: 504331 Color: 2
Size: 356963 Color: 0
Size: 138706 Color: 1

Bin 1017: 1 of cap free
Amount of items: 3
Items: 
Size: 504633 Color: 3
Size: 303001 Color: 1
Size: 192366 Color: 2

Bin 1018: 1 of cap free
Amount of items: 3
Items: 
Size: 504665 Color: 0
Size: 360634 Color: 3
Size: 134701 Color: 1

Bin 1019: 1 of cap free
Amount of items: 3
Items: 
Size: 504799 Color: 0
Size: 361340 Color: 4
Size: 133861 Color: 0

Bin 1020: 1 of cap free
Amount of items: 3
Items: 
Size: 505168 Color: 3
Size: 364710 Color: 2
Size: 130122 Color: 3

Bin 1021: 1 of cap free
Amount of items: 3
Items: 
Size: 505668 Color: 3
Size: 363619 Color: 0
Size: 130713 Color: 1

Bin 1022: 1 of cap free
Amount of items: 3
Items: 
Size: 505798 Color: 0
Size: 307352 Color: 2
Size: 186850 Color: 3

Bin 1023: 1 of cap free
Amount of items: 3
Items: 
Size: 505893 Color: 0
Size: 328730 Color: 3
Size: 165377 Color: 1

Bin 1024: 1 of cap free
Amount of items: 3
Items: 
Size: 506334 Color: 3
Size: 307748 Color: 0
Size: 185918 Color: 3

Bin 1025: 1 of cap free
Amount of items: 3
Items: 
Size: 506422 Color: 2
Size: 308864 Color: 0
Size: 184714 Color: 2

Bin 1026: 1 of cap free
Amount of items: 3
Items: 
Size: 506438 Color: 2
Size: 305596 Color: 0
Size: 187966 Color: 1

Bin 1027: 1 of cap free
Amount of items: 3
Items: 
Size: 506503 Color: 0
Size: 303299 Color: 4
Size: 190198 Color: 1

Bin 1028: 1 of cap free
Amount of items: 3
Items: 
Size: 506629 Color: 2
Size: 304215 Color: 1
Size: 189156 Color: 4

Bin 1029: 1 of cap free
Amount of items: 3
Items: 
Size: 506937 Color: 1
Size: 311438 Color: 0
Size: 181625 Color: 4

Bin 1030: 1 of cap free
Amount of items: 3
Items: 
Size: 506957 Color: 3
Size: 360154 Color: 4
Size: 132889 Color: 1

Bin 1031: 1 of cap free
Amount of items: 3
Items: 
Size: 507632 Color: 2
Size: 308757 Color: 4
Size: 183611 Color: 3

Bin 1032: 1 of cap free
Amount of items: 3
Items: 
Size: 508994 Color: 4
Size: 359205 Color: 1
Size: 131801 Color: 4

Bin 1033: 1 of cap free
Amount of items: 3
Items: 
Size: 509230 Color: 1
Size: 311156 Color: 4
Size: 179614 Color: 3

Bin 1034: 1 of cap free
Amount of items: 3
Items: 
Size: 509507 Color: 2
Size: 322621 Color: 1
Size: 167872 Color: 2

Bin 1035: 1 of cap free
Amount of items: 3
Items: 
Size: 509798 Color: 3
Size: 355494 Color: 2
Size: 134708 Color: 1

Bin 1036: 1 of cap free
Amount of items: 3
Items: 
Size: 509897 Color: 2
Size: 307362 Color: 3
Size: 182741 Color: 2

Bin 1037: 1 of cap free
Amount of items: 3
Items: 
Size: 510302 Color: 1
Size: 308140 Color: 4
Size: 181558 Color: 0

Bin 1038: 1 of cap free
Amount of items: 3
Items: 
Size: 510594 Color: 3
Size: 360294 Color: 4
Size: 129112 Color: 0

Bin 1039: 1 of cap free
Amount of items: 3
Items: 
Size: 510672 Color: 2
Size: 359366 Color: 1
Size: 129962 Color: 3

Bin 1040: 1 of cap free
Amount of items: 3
Items: 
Size: 511098 Color: 0
Size: 313163 Color: 2
Size: 175739 Color: 0

Bin 1041: 1 of cap free
Amount of items: 3
Items: 
Size: 511146 Color: 2
Size: 309355 Color: 4
Size: 179499 Color: 1

Bin 1042: 1 of cap free
Amount of items: 3
Items: 
Size: 511422 Color: 3
Size: 310573 Color: 1
Size: 178005 Color: 4

Bin 1043: 1 of cap free
Amount of items: 3
Items: 
Size: 511588 Color: 1
Size: 358632 Color: 2
Size: 129780 Color: 4

Bin 1044: 1 of cap free
Amount of items: 3
Items: 
Size: 511807 Color: 4
Size: 366551 Color: 1
Size: 121642 Color: 1

Bin 1045: 1 of cap free
Amount of items: 3
Items: 
Size: 512200 Color: 4
Size: 310577 Color: 2
Size: 177223 Color: 0

Bin 1046: 1 of cap free
Amount of items: 3
Items: 
Size: 512238 Color: 1
Size: 313516 Color: 4
Size: 174246 Color: 3

Bin 1047: 1 of cap free
Amount of items: 3
Items: 
Size: 512369 Color: 0
Size: 362394 Color: 3
Size: 125237 Color: 2

Bin 1048: 1 of cap free
Amount of items: 3
Items: 
Size: 513312 Color: 2
Size: 311593 Color: 3
Size: 175095 Color: 0

Bin 1049: 1 of cap free
Amount of items: 3
Items: 
Size: 513507 Color: 4
Size: 314022 Color: 2
Size: 172471 Color: 2

Bin 1050: 1 of cap free
Amount of items: 3
Items: 
Size: 513702 Color: 3
Size: 312600 Color: 2
Size: 173698 Color: 1

Bin 1051: 1 of cap free
Amount of items: 3
Items: 
Size: 514061 Color: 1
Size: 310250 Color: 4
Size: 175689 Color: 3

Bin 1052: 1 of cap free
Amount of items: 3
Items: 
Size: 514069 Color: 1
Size: 354654 Color: 0
Size: 131277 Color: 0

Bin 1053: 1 of cap free
Amount of items: 3
Items: 
Size: 514664 Color: 0
Size: 314454 Color: 3
Size: 170882 Color: 2

Bin 1054: 1 of cap free
Amount of items: 3
Items: 
Size: 514670 Color: 0
Size: 358386 Color: 1
Size: 126944 Color: 2

Bin 1055: 1 of cap free
Amount of items: 3
Items: 
Size: 514823 Color: 4
Size: 310832 Color: 0
Size: 174345 Color: 2

Bin 1056: 1 of cap free
Amount of items: 3
Items: 
Size: 514888 Color: 2
Size: 358559 Color: 0
Size: 126553 Color: 4

Bin 1057: 1 of cap free
Amount of items: 3
Items: 
Size: 515277 Color: 3
Size: 315670 Color: 0
Size: 169053 Color: 1

Bin 1058: 1 of cap free
Amount of items: 3
Items: 
Size: 515831 Color: 2
Size: 314931 Color: 1
Size: 169238 Color: 4

Bin 1059: 1 of cap free
Amount of items: 3
Items: 
Size: 516004 Color: 3
Size: 313328 Color: 4
Size: 170668 Color: 1

Bin 1060: 1 of cap free
Amount of items: 3
Items: 
Size: 516025 Color: 2
Size: 313619 Color: 1
Size: 170356 Color: 3

Bin 1061: 1 of cap free
Amount of items: 3
Items: 
Size: 516072 Color: 2
Size: 358327 Color: 4
Size: 125601 Color: 3

Bin 1062: 1 of cap free
Amount of items: 3
Items: 
Size: 516157 Color: 0
Size: 358875 Color: 4
Size: 124968 Color: 3

Bin 1063: 1 of cap free
Amount of items: 3
Items: 
Size: 516547 Color: 3
Size: 357278 Color: 0
Size: 126175 Color: 2

Bin 1064: 1 of cap free
Amount of items: 3
Items: 
Size: 516964 Color: 0
Size: 310573 Color: 2
Size: 172463 Color: 4

Bin 1065: 1 of cap free
Amount of items: 3
Items: 
Size: 518164 Color: 1
Size: 324081 Color: 2
Size: 157755 Color: 2

Bin 1066: 1 of cap free
Amount of items: 3
Items: 
Size: 518302 Color: 1
Size: 313433 Color: 2
Size: 168265 Color: 2

Bin 1067: 1 of cap free
Amount of items: 3
Items: 
Size: 518404 Color: 0
Size: 327036 Color: 2
Size: 154560 Color: 2

Bin 1068: 1 of cap free
Amount of items: 3
Items: 
Size: 518738 Color: 0
Size: 318431 Color: 3
Size: 162831 Color: 0

Bin 1069: 1 of cap free
Amount of items: 3
Items: 
Size: 518745 Color: 2
Size: 313803 Color: 1
Size: 167452 Color: 2

Bin 1070: 1 of cap free
Amount of items: 3
Items: 
Size: 519925 Color: 4
Size: 315228 Color: 3
Size: 164847 Color: 2

Bin 1071: 1 of cap free
Amount of items: 3
Items: 
Size: 520077 Color: 4
Size: 316663 Color: 3
Size: 163260 Color: 1

Bin 1072: 1 of cap free
Amount of items: 3
Items: 
Size: 520130 Color: 2
Size: 323468 Color: 3
Size: 156402 Color: 2

Bin 1073: 1 of cap free
Amount of items: 3
Items: 
Size: 520324 Color: 3
Size: 351089 Color: 4
Size: 128587 Color: 1

Bin 1074: 1 of cap free
Amount of items: 3
Items: 
Size: 520326 Color: 4
Size: 325720 Color: 0
Size: 153954 Color: 3

Bin 1075: 1 of cap free
Amount of items: 3
Items: 
Size: 520704 Color: 3
Size: 318298 Color: 1
Size: 160998 Color: 1

Bin 1076: 1 of cap free
Amount of items: 3
Items: 
Size: 520869 Color: 3
Size: 317722 Color: 0
Size: 161409 Color: 1

Bin 1077: 1 of cap free
Amount of items: 3
Items: 
Size: 520932 Color: 4
Size: 317160 Color: 3
Size: 161908 Color: 2

Bin 1078: 1 of cap free
Amount of items: 3
Items: 
Size: 520961 Color: 4
Size: 319188 Color: 2
Size: 159851 Color: 1

Bin 1079: 1 of cap free
Amount of items: 3
Items: 
Size: 520977 Color: 0
Size: 355207 Color: 2
Size: 123816 Color: 2

Bin 1080: 1 of cap free
Amount of items: 3
Items: 
Size: 521357 Color: 4
Size: 353859 Color: 1
Size: 124784 Color: 1

Bin 1081: 1 of cap free
Amount of items: 3
Items: 
Size: 521737 Color: 0
Size: 357943 Color: 3
Size: 120320 Color: 2

Bin 1082: 1 of cap free
Amount of items: 3
Items: 
Size: 522079 Color: 2
Size: 322219 Color: 4
Size: 155702 Color: 0

Bin 1083: 1 of cap free
Amount of items: 3
Items: 
Size: 522395 Color: 1
Size: 319303 Color: 4
Size: 158302 Color: 1

Bin 1084: 1 of cap free
Amount of items: 3
Items: 
Size: 522473 Color: 3
Size: 315430 Color: 0
Size: 162097 Color: 0

Bin 1085: 1 of cap free
Amount of items: 3
Items: 
Size: 522606 Color: 0
Size: 318554 Color: 2
Size: 158840 Color: 1

Bin 1086: 1 of cap free
Amount of items: 3
Items: 
Size: 523263 Color: 2
Size: 319901 Color: 4
Size: 156836 Color: 3

Bin 1087: 1 of cap free
Amount of items: 3
Items: 
Size: 523483 Color: 0
Size: 351714 Color: 1
Size: 124803 Color: 4

Bin 1088: 1 of cap free
Amount of items: 3
Items: 
Size: 523525 Color: 2
Size: 322253 Color: 3
Size: 154222 Color: 1

Bin 1089: 1 of cap free
Amount of items: 3
Items: 
Size: 524083 Color: 3
Size: 358504 Color: 0
Size: 117413 Color: 4

Bin 1090: 1 of cap free
Amount of items: 3
Items: 
Size: 524185 Color: 0
Size: 324534 Color: 1
Size: 151281 Color: 3

Bin 1091: 1 of cap free
Amount of items: 2
Items: 
Size: 524897 Color: 3
Size: 475103 Color: 2

Bin 1092: 1 of cap free
Amount of items: 3
Items: 
Size: 525004 Color: 4
Size: 320331 Color: 1
Size: 154665 Color: 4

Bin 1093: 1 of cap free
Amount of items: 3
Items: 
Size: 525124 Color: 1
Size: 356034 Color: 2
Size: 118842 Color: 3

Bin 1094: 1 of cap free
Amount of items: 3
Items: 
Size: 525362 Color: 4
Size: 320184 Color: 3
Size: 154454 Color: 3

Bin 1095: 1 of cap free
Amount of items: 3
Items: 
Size: 525519 Color: 4
Size: 318228 Color: 1
Size: 156253 Color: 4

Bin 1096: 1 of cap free
Amount of items: 3
Items: 
Size: 525542 Color: 3
Size: 317632 Color: 1
Size: 156826 Color: 3

Bin 1097: 1 of cap free
Amount of items: 2
Items: 
Size: 526686 Color: 0
Size: 473314 Color: 1

Bin 1098: 1 of cap free
Amount of items: 3
Items: 
Size: 527000 Color: 2
Size: 318695 Color: 1
Size: 154305 Color: 3

Bin 1099: 1 of cap free
Amount of items: 3
Items: 
Size: 527114 Color: 3
Size: 319851 Color: 0
Size: 153035 Color: 2

Bin 1100: 1 of cap free
Amount of items: 3
Items: 
Size: 527286 Color: 4
Size: 325664 Color: 1
Size: 147050 Color: 4

Bin 1101: 1 of cap free
Amount of items: 3
Items: 
Size: 527457 Color: 3
Size: 325665 Color: 4
Size: 146878 Color: 4

Bin 1102: 1 of cap free
Amount of items: 3
Items: 
Size: 527935 Color: 4
Size: 321444 Color: 1
Size: 150621 Color: 1

Bin 1103: 1 of cap free
Amount of items: 3
Items: 
Size: 528025 Color: 0
Size: 319690 Color: 1
Size: 152285 Color: 0

Bin 1104: 1 of cap free
Amount of items: 3
Items: 
Size: 528250 Color: 4
Size: 352668 Color: 0
Size: 119082 Color: 2

Bin 1105: 1 of cap free
Amount of items: 2
Items: 
Size: 528339 Color: 0
Size: 471661 Color: 3

Bin 1106: 1 of cap free
Amount of items: 3
Items: 
Size: 528520 Color: 0
Size: 341100 Color: 1
Size: 130380 Color: 1

Bin 1107: 1 of cap free
Amount of items: 3
Items: 
Size: 529547 Color: 0
Size: 321048 Color: 4
Size: 149405 Color: 1

Bin 1108: 1 of cap free
Amount of items: 3
Items: 
Size: 529626 Color: 0
Size: 318325 Color: 3
Size: 152049 Color: 0

Bin 1109: 1 of cap free
Amount of items: 3
Items: 
Size: 530025 Color: 0
Size: 349625 Color: 2
Size: 120350 Color: 0

Bin 1110: 1 of cap free
Amount of items: 2
Items: 
Size: 530034 Color: 1
Size: 469966 Color: 0

Bin 1111: 1 of cap free
Amount of items: 3
Items: 
Size: 530560 Color: 1
Size: 355098 Color: 0
Size: 114342 Color: 2

Bin 1112: 1 of cap free
Amount of items: 3
Items: 
Size: 530641 Color: 4
Size: 355029 Color: 2
Size: 114330 Color: 3

Bin 1113: 1 of cap free
Amount of items: 3
Items: 
Size: 530678 Color: 2
Size: 326945 Color: 0
Size: 142377 Color: 3

Bin 1114: 1 of cap free
Amount of items: 3
Items: 
Size: 530803 Color: 4
Size: 355894 Color: 2
Size: 113303 Color: 1

Bin 1115: 1 of cap free
Amount of items: 3
Items: 
Size: 531154 Color: 3
Size: 351113 Color: 0
Size: 117733 Color: 2

Bin 1116: 1 of cap free
Amount of items: 2
Items: 
Size: 531498 Color: 0
Size: 468502 Color: 4

Bin 1117: 1 of cap free
Amount of items: 3
Items: 
Size: 532162 Color: 4
Size: 356347 Color: 3
Size: 111491 Color: 3

Bin 1118: 1 of cap free
Amount of items: 3
Items: 
Size: 532446 Color: 2
Size: 328627 Color: 3
Size: 138927 Color: 0

Bin 1119: 1 of cap free
Amount of items: 3
Items: 
Size: 532516 Color: 0
Size: 332857 Color: 1
Size: 134627 Color: 3

Bin 1120: 1 of cap free
Amount of items: 3
Items: 
Size: 532738 Color: 2
Size: 324772 Color: 1
Size: 142490 Color: 2

Bin 1121: 1 of cap free
Amount of items: 3
Items: 
Size: 533170 Color: 4
Size: 326072 Color: 1
Size: 140758 Color: 3

Bin 1122: 1 of cap free
Amount of items: 3
Items: 
Size: 533266 Color: 1
Size: 360601 Color: 4
Size: 106133 Color: 3

Bin 1123: 1 of cap free
Amount of items: 3
Items: 
Size: 533314 Color: 3
Size: 353002 Color: 0
Size: 113684 Color: 1

Bin 1124: 1 of cap free
Amount of items: 3
Items: 
Size: 533328 Color: 1
Size: 325294 Color: 2
Size: 141378 Color: 0

Bin 1125: 1 of cap free
Amount of items: 3
Items: 
Size: 533623 Color: 0
Size: 330638 Color: 1
Size: 135739 Color: 1

Bin 1126: 1 of cap free
Amount of items: 3
Items: 
Size: 533838 Color: 2
Size: 328448 Color: 4
Size: 137714 Color: 0

Bin 1127: 1 of cap free
Amount of items: 3
Items: 
Size: 533889 Color: 1
Size: 328725 Color: 4
Size: 137386 Color: 0

Bin 1128: 1 of cap free
Amount of items: 3
Items: 
Size: 534515 Color: 0
Size: 325109 Color: 1
Size: 140376 Color: 2

Bin 1129: 1 of cap free
Amount of items: 3
Items: 
Size: 534833 Color: 3
Size: 327840 Color: 4
Size: 137327 Color: 0

Bin 1130: 1 of cap free
Amount of items: 3
Items: 
Size: 534912 Color: 0
Size: 329897 Color: 4
Size: 135191 Color: 1

Bin 1131: 1 of cap free
Amount of items: 3
Items: 
Size: 535052 Color: 3
Size: 338890 Color: 4
Size: 126058 Color: 4

Bin 1132: 1 of cap free
Amount of items: 3
Items: 
Size: 535456 Color: 2
Size: 355415 Color: 4
Size: 109129 Color: 2

Bin 1133: 1 of cap free
Amount of items: 3
Items: 
Size: 535555 Color: 4
Size: 343481 Color: 0
Size: 120964 Color: 4

Bin 1134: 1 of cap free
Amount of items: 3
Items: 
Size: 535919 Color: 3
Size: 347236 Color: 1
Size: 116845 Color: 3

Bin 1135: 1 of cap free
Amount of items: 3
Items: 
Size: 536483 Color: 4
Size: 350518 Color: 0
Size: 112999 Color: 3

Bin 1136: 1 of cap free
Amount of items: 3
Items: 
Size: 536632 Color: 2
Size: 359276 Color: 0
Size: 104092 Color: 0

Bin 1137: 1 of cap free
Amount of items: 3
Items: 
Size: 536731 Color: 2
Size: 339629 Color: 0
Size: 123640 Color: 2

Bin 1138: 1 of cap free
Amount of items: 3
Items: 
Size: 537006 Color: 2
Size: 341780 Color: 1
Size: 121214 Color: 4

Bin 1139: 1 of cap free
Amount of items: 3
Items: 
Size: 537162 Color: 0
Size: 326498 Color: 1
Size: 136340 Color: 3

Bin 1140: 1 of cap free
Amount of items: 3
Items: 
Size: 537301 Color: 2
Size: 334646 Color: 0
Size: 128053 Color: 2

Bin 1141: 1 of cap free
Amount of items: 3
Items: 
Size: 537810 Color: 1
Size: 327128 Color: 0
Size: 135062 Color: 3

Bin 1142: 1 of cap free
Amount of items: 3
Items: 
Size: 538225 Color: 4
Size: 344779 Color: 0
Size: 116996 Color: 0

Bin 1143: 1 of cap free
Amount of items: 3
Items: 
Size: 538232 Color: 1
Size: 326005 Color: 2
Size: 135763 Color: 3

Bin 1144: 1 of cap free
Amount of items: 3
Items: 
Size: 538294 Color: 2
Size: 327129 Color: 0
Size: 134577 Color: 0

Bin 1145: 1 of cap free
Amount of items: 3
Items: 
Size: 538470 Color: 0
Size: 329221 Color: 1
Size: 132309 Color: 3

Bin 1146: 1 of cap free
Amount of items: 3
Items: 
Size: 538727 Color: 0
Size: 360084 Color: 3
Size: 101189 Color: 3

Bin 1147: 1 of cap free
Amount of items: 3
Items: 
Size: 538955 Color: 0
Size: 343026 Color: 4
Size: 118019 Color: 1

Bin 1148: 1 of cap free
Amount of items: 3
Items: 
Size: 539169 Color: 1
Size: 336370 Color: 2
Size: 124461 Color: 2

Bin 1149: 1 of cap free
Amount of items: 3
Items: 
Size: 539185 Color: 4
Size: 335462 Color: 0
Size: 125353 Color: 2

Bin 1150: 1 of cap free
Amount of items: 3
Items: 
Size: 539492 Color: 1
Size: 334601 Color: 0
Size: 125907 Color: 0

Bin 1151: 1 of cap free
Amount of items: 3
Items: 
Size: 539913 Color: 2
Size: 342532 Color: 0
Size: 117555 Color: 4

Bin 1152: 1 of cap free
Amount of items: 3
Items: 
Size: 539930 Color: 1
Size: 338602 Color: 3
Size: 121468 Color: 1

Bin 1153: 1 of cap free
Amount of items: 3
Items: 
Size: 540039 Color: 1
Size: 355742 Color: 3
Size: 104219 Color: 1

Bin 1154: 1 of cap free
Amount of items: 3
Items: 
Size: 540474 Color: 4
Size: 333485 Color: 2
Size: 126041 Color: 2

Bin 1155: 1 of cap free
Amount of items: 3
Items: 
Size: 540497 Color: 4
Size: 349350 Color: 2
Size: 110153 Color: 1

Bin 1156: 1 of cap free
Amount of items: 3
Items: 
Size: 541364 Color: 2
Size: 349618 Color: 4
Size: 109018 Color: 4

Bin 1157: 1 of cap free
Amount of items: 2
Items: 
Size: 541601 Color: 3
Size: 458399 Color: 0

Bin 1158: 1 of cap free
Amount of items: 3
Items: 
Size: 541648 Color: 1
Size: 340581 Color: 4
Size: 117771 Color: 0

Bin 1159: 1 of cap free
Amount of items: 3
Items: 
Size: 541789 Color: 1
Size: 340508 Color: 4
Size: 117703 Color: 0

Bin 1160: 1 of cap free
Amount of items: 3
Items: 
Size: 541914 Color: 0
Size: 335699 Color: 1
Size: 122387 Color: 4

Bin 1161: 1 of cap free
Amount of items: 3
Items: 
Size: 542108 Color: 2
Size: 349391 Color: 3
Size: 108501 Color: 2

Bin 1162: 1 of cap free
Amount of items: 3
Items: 
Size: 542116 Color: 0
Size: 351854 Color: 2
Size: 106030 Color: 4

Bin 1163: 1 of cap free
Amount of items: 3
Items: 
Size: 542175 Color: 2
Size: 338765 Color: 0
Size: 119060 Color: 4

Bin 1164: 1 of cap free
Amount of items: 3
Items: 
Size: 542565 Color: 3
Size: 353085 Color: 4
Size: 104350 Color: 3

Bin 1165: 1 of cap free
Amount of items: 3
Items: 
Size: 542580 Color: 3
Size: 339322 Color: 1
Size: 118098 Color: 3

Bin 1166: 1 of cap free
Amount of items: 3
Items: 
Size: 543046 Color: 2
Size: 353618 Color: 4
Size: 103336 Color: 4

Bin 1167: 1 of cap free
Amount of items: 3
Items: 
Size: 543255 Color: 1
Size: 335296 Color: 4
Size: 121449 Color: 2

Bin 1168: 1 of cap free
Amount of items: 3
Items: 
Size: 543641 Color: 0
Size: 336837 Color: 3
Size: 119522 Color: 0

Bin 1169: 1 of cap free
Amount of items: 2
Items: 
Size: 543700 Color: 4
Size: 456300 Color: 2

Bin 1170: 1 of cap free
Amount of items: 3
Items: 
Size: 544070 Color: 3
Size: 332355 Color: 2
Size: 123575 Color: 2

Bin 1171: 1 of cap free
Amount of items: 3
Items: 
Size: 544435 Color: 0
Size: 339374 Color: 1
Size: 116191 Color: 3

Bin 1172: 1 of cap free
Amount of items: 3
Items: 
Size: 544963 Color: 4
Size: 337359 Color: 3
Size: 117678 Color: 1

Bin 1173: 1 of cap free
Amount of items: 3
Items: 
Size: 545081 Color: 1
Size: 340802 Color: 0
Size: 114117 Color: 0

Bin 1174: 1 of cap free
Amount of items: 3
Items: 
Size: 545342 Color: 0
Size: 345077 Color: 3
Size: 109581 Color: 0

Bin 1175: 1 of cap free
Amount of items: 3
Items: 
Size: 545825 Color: 4
Size: 332028 Color: 1
Size: 122147 Color: 1

Bin 1176: 1 of cap free
Amount of items: 3
Items: 
Size: 545977 Color: 2
Size: 344660 Color: 0
Size: 109363 Color: 1

Bin 1177: 1 of cap free
Amount of items: 3
Items: 
Size: 546185 Color: 1
Size: 346747 Color: 4
Size: 107068 Color: 1

Bin 1178: 1 of cap free
Amount of items: 3
Items: 
Size: 546567 Color: 3
Size: 339859 Color: 1
Size: 113574 Color: 1

Bin 1179: 1 of cap free
Amount of items: 3
Items: 
Size: 546594 Color: 0
Size: 330203 Color: 4
Size: 123203 Color: 0

Bin 1180: 1 of cap free
Amount of items: 3
Items: 
Size: 547220 Color: 3
Size: 344604 Color: 1
Size: 108176 Color: 4

Bin 1181: 1 of cap free
Amount of items: 3
Items: 
Size: 547352 Color: 0
Size: 345821 Color: 3
Size: 106827 Color: 1

Bin 1182: 1 of cap free
Amount of items: 3
Items: 
Size: 547682 Color: 3
Size: 340688 Color: 0
Size: 111630 Color: 0

Bin 1183: 1 of cap free
Amount of items: 3
Items: 
Size: 548485 Color: 2
Size: 337597 Color: 0
Size: 113918 Color: 1

Bin 1184: 1 of cap free
Amount of items: 3
Items: 
Size: 548553 Color: 2
Size: 333694 Color: 1
Size: 117753 Color: 3

Bin 1185: 1 of cap free
Amount of items: 3
Items: 
Size: 548609 Color: 3
Size: 336750 Color: 1
Size: 114641 Color: 4

Bin 1186: 1 of cap free
Amount of items: 3
Items: 
Size: 549143 Color: 2
Size: 342856 Color: 0
Size: 108001 Color: 2

Bin 1187: 1 of cap free
Amount of items: 3
Items: 
Size: 549168 Color: 1
Size: 347531 Color: 4
Size: 103301 Color: 4

Bin 1188: 1 of cap free
Amount of items: 3
Items: 
Size: 549435 Color: 2
Size: 342879 Color: 0
Size: 107686 Color: 4

Bin 1189: 1 of cap free
Amount of items: 3
Items: 
Size: 549669 Color: 1
Size: 347614 Color: 3
Size: 102717 Color: 4

Bin 1190: 1 of cap free
Amount of items: 3
Items: 
Size: 549724 Color: 2
Size: 349377 Color: 4
Size: 100899 Color: 1

Bin 1191: 1 of cap free
Amount of items: 3
Items: 
Size: 550195 Color: 3
Size: 333084 Color: 0
Size: 116721 Color: 2

Bin 1192: 1 of cap free
Amount of items: 3
Items: 
Size: 550237 Color: 1
Size: 338295 Color: 3
Size: 111468 Color: 4

Bin 1193: 1 of cap free
Amount of items: 3
Items: 
Size: 550892 Color: 1
Size: 341181 Color: 3
Size: 107927 Color: 3

Bin 1194: 1 of cap free
Amount of items: 3
Items: 
Size: 552740 Color: 0
Size: 335327 Color: 1
Size: 111933 Color: 4

Bin 1195: 1 of cap free
Amount of items: 3
Items: 
Size: 552931 Color: 3
Size: 345758 Color: 0
Size: 101311 Color: 2

Bin 1196: 1 of cap free
Amount of items: 3
Items: 
Size: 553109 Color: 1
Size: 340078 Color: 0
Size: 106813 Color: 4

Bin 1197: 1 of cap free
Amount of items: 3
Items: 
Size: 553364 Color: 2
Size: 334991 Color: 3
Size: 111645 Color: 4

Bin 1198: 1 of cap free
Amount of items: 3
Items: 
Size: 554384 Color: 2
Size: 343425 Color: 0
Size: 102191 Color: 4

Bin 1199: 1 of cap free
Amount of items: 3
Items: 
Size: 554493 Color: 2
Size: 339634 Color: 1
Size: 105873 Color: 3

Bin 1200: 1 of cap free
Amount of items: 3
Items: 
Size: 554899 Color: 0
Size: 333264 Color: 3
Size: 111837 Color: 4

Bin 1201: 1 of cap free
Amount of items: 3
Items: 
Size: 555146 Color: 3
Size: 342702 Color: 2
Size: 102152 Color: 2

Bin 1202: 1 of cap free
Amount of items: 3
Items: 
Size: 555341 Color: 2
Size: 343638 Color: 3
Size: 101021 Color: 1

Bin 1203: 2 of cap free
Amount of items: 4
Items: 
Size: 419397 Color: 4
Size: 197783 Color: 3
Size: 191463 Color: 1
Size: 191356 Color: 0

Bin 1204: 2 of cap free
Amount of items: 4
Items: 
Size: 419588 Color: 3
Size: 198180 Color: 1
Size: 191335 Color: 2
Size: 190896 Color: 0

Bin 1205: 2 of cap free
Amount of items: 4
Items: 
Size: 420629 Color: 3
Size: 202031 Color: 0
Size: 189310 Color: 2
Size: 188029 Color: 3

Bin 1206: 2 of cap free
Amount of items: 3
Items: 
Size: 447482 Color: 3
Size: 384722 Color: 4
Size: 167795 Color: 2

Bin 1207: 2 of cap free
Amount of items: 3
Items: 
Size: 447588 Color: 3
Size: 380351 Color: 0
Size: 172060 Color: 0

Bin 1208: 2 of cap free
Amount of items: 3
Items: 
Size: 449934 Color: 2
Size: 382939 Color: 4
Size: 167126 Color: 4

Bin 1209: 2 of cap free
Amount of items: 3
Items: 
Size: 450245 Color: 3
Size: 275350 Color: 2
Size: 274404 Color: 3

Bin 1210: 2 of cap free
Amount of items: 3
Items: 
Size: 452993 Color: 1
Size: 281014 Color: 2
Size: 265992 Color: 1

Bin 1211: 2 of cap free
Amount of items: 3
Items: 
Size: 453081 Color: 4
Size: 278850 Color: 1
Size: 268068 Color: 0

Bin 1212: 2 of cap free
Amount of items: 3
Items: 
Size: 454120 Color: 4
Size: 277390 Color: 2
Size: 268489 Color: 1

Bin 1213: 2 of cap free
Amount of items: 3
Items: 
Size: 454760 Color: 2
Size: 379719 Color: 3
Size: 165520 Color: 4

Bin 1214: 2 of cap free
Amount of items: 3
Items: 
Size: 455987 Color: 3
Size: 283192 Color: 4
Size: 260820 Color: 2

Bin 1215: 2 of cap free
Amount of items: 3
Items: 
Size: 456127 Color: 4
Size: 276668 Color: 1
Size: 267204 Color: 3

Bin 1216: 2 of cap free
Amount of items: 3
Items: 
Size: 456188 Color: 0
Size: 278104 Color: 1
Size: 265707 Color: 1

Bin 1217: 2 of cap free
Amount of items: 3
Items: 
Size: 456348 Color: 2
Size: 275446 Color: 1
Size: 268205 Color: 4

Bin 1218: 2 of cap free
Amount of items: 3
Items: 
Size: 456564 Color: 0
Size: 281723 Color: 1
Size: 261712 Color: 0

Bin 1219: 2 of cap free
Amount of items: 3
Items: 
Size: 457173 Color: 0
Size: 288930 Color: 2
Size: 253896 Color: 3

Bin 1220: 2 of cap free
Amount of items: 3
Items: 
Size: 457329 Color: 3
Size: 277871 Color: 2
Size: 264799 Color: 2

Bin 1221: 2 of cap free
Amount of items: 3
Items: 
Size: 457722 Color: 1
Size: 286578 Color: 2
Size: 255699 Color: 0

Bin 1222: 2 of cap free
Amount of items: 3
Items: 
Size: 458017 Color: 3
Size: 295656 Color: 1
Size: 246326 Color: 3

Bin 1223: 2 of cap free
Amount of items: 3
Items: 
Size: 458333 Color: 0
Size: 278024 Color: 3
Size: 263642 Color: 4

Bin 1224: 2 of cap free
Amount of items: 3
Items: 
Size: 458684 Color: 0
Size: 376545 Color: 2
Size: 164770 Color: 2

Bin 1225: 2 of cap free
Amount of items: 3
Items: 
Size: 458766 Color: 0
Size: 375948 Color: 3
Size: 165285 Color: 3

Bin 1226: 2 of cap free
Amount of items: 3
Items: 
Size: 459788 Color: 0
Size: 275660 Color: 3
Size: 264551 Color: 0

Bin 1227: 2 of cap free
Amount of items: 3
Items: 
Size: 460278 Color: 1
Size: 379248 Color: 2
Size: 160473 Color: 3

Bin 1228: 2 of cap free
Amount of items: 3
Items: 
Size: 460334 Color: 0
Size: 278751 Color: 1
Size: 260914 Color: 1

Bin 1229: 2 of cap free
Amount of items: 3
Items: 
Size: 460382 Color: 0
Size: 286712 Color: 1
Size: 252905 Color: 1

Bin 1230: 2 of cap free
Amount of items: 3
Items: 
Size: 460459 Color: 4
Size: 376778 Color: 0
Size: 162762 Color: 4

Bin 1231: 2 of cap free
Amount of items: 3
Items: 
Size: 461889 Color: 2
Size: 281170 Color: 4
Size: 256940 Color: 3

Bin 1232: 2 of cap free
Amount of items: 3
Items: 
Size: 461954 Color: 2
Size: 279799 Color: 1
Size: 258246 Color: 0

Bin 1233: 2 of cap free
Amount of items: 3
Items: 
Size: 462312 Color: 2
Size: 277730 Color: 1
Size: 259957 Color: 4

Bin 1234: 2 of cap free
Amount of items: 3
Items: 
Size: 462354 Color: 0
Size: 279551 Color: 2
Size: 258094 Color: 1

Bin 1235: 2 of cap free
Amount of items: 3
Items: 
Size: 462499 Color: 3
Size: 280414 Color: 2
Size: 257086 Color: 4

Bin 1236: 2 of cap free
Amount of items: 3
Items: 
Size: 463913 Color: 2
Size: 283422 Color: 1
Size: 252664 Color: 4

Bin 1237: 2 of cap free
Amount of items: 3
Items: 
Size: 464029 Color: 0
Size: 285875 Color: 1
Size: 250095 Color: 4

Bin 1238: 2 of cap free
Amount of items: 3
Items: 
Size: 464586 Color: 4
Size: 376169 Color: 3
Size: 159244 Color: 0

Bin 1239: 2 of cap free
Amount of items: 3
Items: 
Size: 465164 Color: 3
Size: 293650 Color: 2
Size: 241185 Color: 2

Bin 1240: 2 of cap free
Amount of items: 3
Items: 
Size: 465776 Color: 2
Size: 283355 Color: 4
Size: 250868 Color: 4

Bin 1241: 2 of cap free
Amount of items: 3
Items: 
Size: 466104 Color: 1
Size: 379733 Color: 0
Size: 154162 Color: 0

Bin 1242: 2 of cap free
Amount of items: 3
Items: 
Size: 466874 Color: 4
Size: 372465 Color: 1
Size: 160660 Color: 1

Bin 1243: 2 of cap free
Amount of items: 3
Items: 
Size: 467924 Color: 4
Size: 381941 Color: 1
Size: 150134 Color: 4

Bin 1244: 2 of cap free
Amount of items: 3
Items: 
Size: 468965 Color: 2
Size: 289247 Color: 3
Size: 241787 Color: 2

Bin 1245: 2 of cap free
Amount of items: 3
Items: 
Size: 469088 Color: 1
Size: 374009 Color: 3
Size: 156902 Color: 1

Bin 1246: 2 of cap free
Amount of items: 3
Items: 
Size: 469147 Color: 2
Size: 284917 Color: 1
Size: 245935 Color: 2

Bin 1247: 2 of cap free
Amount of items: 3
Items: 
Size: 470828 Color: 2
Size: 283736 Color: 1
Size: 245435 Color: 3

Bin 1248: 2 of cap free
Amount of items: 3
Items: 
Size: 471413 Color: 2
Size: 285959 Color: 3
Size: 242627 Color: 0

Bin 1249: 2 of cap free
Amount of items: 3
Items: 
Size: 471542 Color: 1
Size: 377442 Color: 3
Size: 151015 Color: 1

Bin 1250: 2 of cap free
Amount of items: 3
Items: 
Size: 471943 Color: 3
Size: 292497 Color: 2
Size: 235559 Color: 1

Bin 1251: 2 of cap free
Amount of items: 3
Items: 
Size: 472016 Color: 1
Size: 285648 Color: 3
Size: 242335 Color: 3

Bin 1252: 2 of cap free
Amount of items: 3
Items: 
Size: 472804 Color: 3
Size: 291204 Color: 1
Size: 235991 Color: 0

Bin 1253: 2 of cap free
Amount of items: 3
Items: 
Size: 473230 Color: 1
Size: 376076 Color: 0
Size: 150693 Color: 3

Bin 1254: 2 of cap free
Amount of items: 3
Items: 
Size: 474140 Color: 2
Size: 293311 Color: 3
Size: 232548 Color: 1

Bin 1255: 2 of cap free
Amount of items: 3
Items: 
Size: 474665 Color: 2
Size: 294657 Color: 1
Size: 230677 Color: 0

Bin 1256: 2 of cap free
Amount of items: 3
Items: 
Size: 475331 Color: 0
Size: 290733 Color: 2
Size: 233935 Color: 4

Bin 1257: 2 of cap free
Amount of items: 3
Items: 
Size: 475757 Color: 4
Size: 371189 Color: 2
Size: 153053 Color: 3

Bin 1258: 2 of cap free
Amount of items: 3
Items: 
Size: 477072 Color: 1
Size: 290152 Color: 2
Size: 232775 Color: 2

Bin 1259: 2 of cap free
Amount of items: 3
Items: 
Size: 477857 Color: 1
Size: 372300 Color: 2
Size: 149842 Color: 2

Bin 1260: 2 of cap free
Amount of items: 3
Items: 
Size: 478321 Color: 2
Size: 289973 Color: 0
Size: 231705 Color: 3

Bin 1261: 2 of cap free
Amount of items: 3
Items: 
Size: 478385 Color: 3
Size: 371822 Color: 1
Size: 149792 Color: 1

Bin 1262: 2 of cap free
Amount of items: 3
Items: 
Size: 478410 Color: 2
Size: 294689 Color: 0
Size: 226900 Color: 4

Bin 1263: 2 of cap free
Amount of items: 3
Items: 
Size: 478701 Color: 2
Size: 296522 Color: 1
Size: 224776 Color: 1

Bin 1264: 2 of cap free
Amount of items: 3
Items: 
Size: 480098 Color: 3
Size: 295734 Color: 4
Size: 224167 Color: 0

Bin 1265: 2 of cap free
Amount of items: 3
Items: 
Size: 480121 Color: 4
Size: 296437 Color: 0
Size: 223441 Color: 2

Bin 1266: 2 of cap free
Amount of items: 3
Items: 
Size: 480196 Color: 1
Size: 292970 Color: 4
Size: 226833 Color: 1

Bin 1267: 2 of cap free
Amount of items: 3
Items: 
Size: 480267 Color: 1
Size: 295987 Color: 3
Size: 223745 Color: 4

Bin 1268: 2 of cap free
Amount of items: 3
Items: 
Size: 481443 Color: 4
Size: 313190 Color: 1
Size: 205366 Color: 1

Bin 1269: 2 of cap free
Amount of items: 3
Items: 
Size: 482002 Color: 1
Size: 366862 Color: 4
Size: 151135 Color: 0

Bin 1270: 2 of cap free
Amount of items: 3
Items: 
Size: 483492 Color: 3
Size: 298683 Color: 2
Size: 217824 Color: 3

Bin 1271: 2 of cap free
Amount of items: 3
Items: 
Size: 485489 Color: 2
Size: 298127 Color: 3
Size: 216383 Color: 4

Bin 1272: 2 of cap free
Amount of items: 3
Items: 
Size: 486395 Color: 4
Size: 298667 Color: 2
Size: 214937 Color: 2

Bin 1273: 2 of cap free
Amount of items: 3
Items: 
Size: 486480 Color: 4
Size: 367738 Color: 3
Size: 145781 Color: 2

Bin 1274: 2 of cap free
Amount of items: 3
Items: 
Size: 486610 Color: 2
Size: 367978 Color: 1
Size: 145411 Color: 2

Bin 1275: 2 of cap free
Amount of items: 3
Items: 
Size: 487007 Color: 4
Size: 367808 Color: 0
Size: 145184 Color: 4

Bin 1276: 2 of cap free
Amount of items: 3
Items: 
Size: 487124 Color: 2
Size: 366328 Color: 4
Size: 146547 Color: 3

Bin 1277: 2 of cap free
Amount of items: 3
Items: 
Size: 488933 Color: 0
Size: 303282 Color: 1
Size: 207784 Color: 4

Bin 1278: 2 of cap free
Amount of items: 3
Items: 
Size: 489709 Color: 2
Size: 295926 Color: 0
Size: 214364 Color: 2

Bin 1279: 2 of cap free
Amount of items: 3
Items: 
Size: 489737 Color: 3
Size: 366093 Color: 1
Size: 144169 Color: 2

Bin 1280: 2 of cap free
Amount of items: 3
Items: 
Size: 489928 Color: 3
Size: 298661 Color: 0
Size: 211410 Color: 3

Bin 1281: 2 of cap free
Amount of items: 3
Items: 
Size: 490734 Color: 4
Size: 364635 Color: 2
Size: 144630 Color: 0

Bin 1282: 2 of cap free
Amount of items: 3
Items: 
Size: 493325 Color: 3
Size: 363123 Color: 1
Size: 143551 Color: 3

Bin 1283: 2 of cap free
Amount of items: 3
Items: 
Size: 493597 Color: 3
Size: 315906 Color: 1
Size: 190496 Color: 4

Bin 1284: 2 of cap free
Amount of items: 3
Items: 
Size: 493837 Color: 2
Size: 366139 Color: 1
Size: 140023 Color: 1

Bin 1285: 2 of cap free
Amount of items: 3
Items: 
Size: 493840 Color: 1
Size: 302886 Color: 4
Size: 203273 Color: 0

Bin 1286: 2 of cap free
Amount of items: 3
Items: 
Size: 493856 Color: 2
Size: 306752 Color: 1
Size: 199391 Color: 0

Bin 1287: 2 of cap free
Amount of items: 3
Items: 
Size: 494540 Color: 1
Size: 369087 Color: 4
Size: 136372 Color: 0

Bin 1288: 2 of cap free
Amount of items: 3
Items: 
Size: 495083 Color: 4
Size: 304979 Color: 2
Size: 199937 Color: 4

Bin 1289: 2 of cap free
Amount of items: 3
Items: 
Size: 495091 Color: 1
Size: 360404 Color: 0
Size: 144504 Color: 4

Bin 1290: 2 of cap free
Amount of items: 3
Items: 
Size: 495304 Color: 1
Size: 299762 Color: 3
Size: 204933 Color: 0

Bin 1291: 2 of cap free
Amount of items: 3
Items: 
Size: 495334 Color: 3
Size: 300963 Color: 0
Size: 203702 Color: 2

Bin 1292: 2 of cap free
Amount of items: 3
Items: 
Size: 496134 Color: 4
Size: 301581 Color: 0
Size: 202284 Color: 2

Bin 1293: 2 of cap free
Amount of items: 3
Items: 
Size: 497022 Color: 4
Size: 317065 Color: 2
Size: 185912 Color: 4

Bin 1294: 2 of cap free
Amount of items: 3
Items: 
Size: 497557 Color: 4
Size: 309333 Color: 2
Size: 193109 Color: 0

Bin 1295: 2 of cap free
Amount of items: 3
Items: 
Size: 497734 Color: 4
Size: 306698 Color: 1
Size: 195567 Color: 2

Bin 1296: 2 of cap free
Amount of items: 3
Items: 
Size: 497891 Color: 0
Size: 304159 Color: 1
Size: 197949 Color: 1

Bin 1297: 2 of cap free
Amount of items: 3
Items: 
Size: 498482 Color: 4
Size: 359907 Color: 1
Size: 141610 Color: 0

Bin 1298: 2 of cap free
Amount of items: 3
Items: 
Size: 498886 Color: 2
Size: 303157 Color: 0
Size: 197956 Color: 4

Bin 1299: 2 of cap free
Amount of items: 3
Items: 
Size: 499139 Color: 4
Size: 368675 Color: 0
Size: 132185 Color: 4

Bin 1300: 2 of cap free
Amount of items: 3
Items: 
Size: 500553 Color: 0
Size: 361020 Color: 2
Size: 138426 Color: 0

Bin 1301: 2 of cap free
Amount of items: 3
Items: 
Size: 501952 Color: 1
Size: 363572 Color: 0
Size: 134475 Color: 0

Bin 1302: 2 of cap free
Amount of items: 3
Items: 
Size: 501964 Color: 3
Size: 363031 Color: 1
Size: 135004 Color: 2

Bin 1303: 2 of cap free
Amount of items: 3
Items: 
Size: 503231 Color: 3
Size: 355592 Color: 4
Size: 141176 Color: 0

Bin 1304: 2 of cap free
Amount of items: 3
Items: 
Size: 504720 Color: 2
Size: 358869 Color: 4
Size: 136410 Color: 1

Bin 1305: 2 of cap free
Amount of items: 3
Items: 
Size: 504968 Color: 2
Size: 363856 Color: 1
Size: 131175 Color: 2

Bin 1306: 2 of cap free
Amount of items: 3
Items: 
Size: 506203 Color: 1
Size: 362644 Color: 3
Size: 131152 Color: 0

Bin 1307: 2 of cap free
Amount of items: 3
Items: 
Size: 509242 Color: 3
Size: 364304 Color: 1
Size: 126453 Color: 3

Bin 1308: 2 of cap free
Amount of items: 3
Items: 
Size: 510818 Color: 3
Size: 312590 Color: 1
Size: 176591 Color: 2

Bin 1309: 2 of cap free
Amount of items: 3
Items: 
Size: 512088 Color: 2
Size: 358755 Color: 0
Size: 129156 Color: 0

Bin 1310: 2 of cap free
Amount of items: 3
Items: 
Size: 512148 Color: 2
Size: 309973 Color: 1
Size: 177878 Color: 1

Bin 1311: 2 of cap free
Amount of items: 3
Items: 
Size: 513048 Color: 4
Size: 330377 Color: 0
Size: 156574 Color: 2

Bin 1312: 2 of cap free
Amount of items: 3
Items: 
Size: 513196 Color: 4
Size: 311188 Color: 0
Size: 175615 Color: 0

Bin 1313: 2 of cap free
Amount of items: 3
Items: 
Size: 513908 Color: 4
Size: 357537 Color: 2
Size: 128554 Color: 1

Bin 1314: 2 of cap free
Amount of items: 3
Items: 
Size: 515778 Color: 4
Size: 311398 Color: 2
Size: 172823 Color: 1

Bin 1315: 2 of cap free
Amount of items: 3
Items: 
Size: 516948 Color: 0
Size: 356185 Color: 1
Size: 126866 Color: 0

Bin 1316: 2 of cap free
Amount of items: 3
Items: 
Size: 517765 Color: 0
Size: 355761 Color: 4
Size: 126473 Color: 0

Bin 1317: 2 of cap free
Amount of items: 3
Items: 
Size: 519048 Color: 3
Size: 315798 Color: 0
Size: 165153 Color: 4

Bin 1318: 2 of cap free
Amount of items: 3
Items: 
Size: 520565 Color: 4
Size: 318874 Color: 1
Size: 160560 Color: 0

Bin 1319: 2 of cap free
Amount of items: 3
Items: 
Size: 520753 Color: 1
Size: 318273 Color: 2
Size: 160973 Color: 1

Bin 1320: 2 of cap free
Amount of items: 3
Items: 
Size: 522599 Color: 0
Size: 315792 Color: 2
Size: 161608 Color: 4

Bin 1321: 2 of cap free
Amount of items: 3
Items: 
Size: 522728 Color: 1
Size: 354605 Color: 3
Size: 122666 Color: 2

Bin 1322: 2 of cap free
Amount of items: 3
Items: 
Size: 522963 Color: 4
Size: 318871 Color: 1
Size: 158165 Color: 4

Bin 1323: 2 of cap free
Amount of items: 3
Items: 
Size: 523621 Color: 4
Size: 321953 Color: 2
Size: 154425 Color: 1

Bin 1324: 2 of cap free
Amount of items: 3
Items: 
Size: 523964 Color: 4
Size: 356707 Color: 2
Size: 119328 Color: 4

Bin 1325: 2 of cap free
Amount of items: 3
Items: 
Size: 524203 Color: 3
Size: 325377 Color: 2
Size: 150419 Color: 2

Bin 1326: 2 of cap free
Amount of items: 3
Items: 
Size: 525011 Color: 1
Size: 324273 Color: 4
Size: 150715 Color: 0

Bin 1327: 2 of cap free
Amount of items: 3
Items: 
Size: 525077 Color: 1
Size: 316268 Color: 3
Size: 158654 Color: 1

Bin 1328: 2 of cap free
Amount of items: 3
Items: 
Size: 525083 Color: 0
Size: 316849 Color: 4
Size: 158067 Color: 1

Bin 1329: 2 of cap free
Amount of items: 3
Items: 
Size: 525169 Color: 1
Size: 317202 Color: 4
Size: 157628 Color: 2

Bin 1330: 2 of cap free
Amount of items: 3
Items: 
Size: 526161 Color: 4
Size: 325892 Color: 2
Size: 147946 Color: 0

Bin 1331: 2 of cap free
Amount of items: 3
Items: 
Size: 526283 Color: 0
Size: 347193 Color: 4
Size: 126523 Color: 3

Bin 1332: 2 of cap free
Amount of items: 3
Items: 
Size: 526948 Color: 2
Size: 322998 Color: 4
Size: 150053 Color: 2

Bin 1333: 2 of cap free
Amount of items: 3
Items: 
Size: 527282 Color: 1
Size: 322233 Color: 4
Size: 150484 Color: 0

Bin 1334: 2 of cap free
Amount of items: 3
Items: 
Size: 528909 Color: 0
Size: 323305 Color: 1
Size: 147785 Color: 2

Bin 1335: 2 of cap free
Amount of items: 3
Items: 
Size: 529687 Color: 0
Size: 321107 Color: 3
Size: 149205 Color: 2

Bin 1336: 2 of cap free
Amount of items: 3
Items: 
Size: 530769 Color: 3
Size: 348376 Color: 1
Size: 120854 Color: 3

Bin 1337: 2 of cap free
Amount of items: 3
Items: 
Size: 530825 Color: 2
Size: 326658 Color: 0
Size: 142516 Color: 1

Bin 1338: 2 of cap free
Amount of items: 3
Items: 
Size: 531829 Color: 3
Size: 349132 Color: 1
Size: 119038 Color: 2

Bin 1339: 2 of cap free
Amount of items: 3
Items: 
Size: 532555 Color: 0
Size: 325073 Color: 3
Size: 142371 Color: 4

Bin 1340: 2 of cap free
Amount of items: 3
Items: 
Size: 532828 Color: 3
Size: 352106 Color: 4
Size: 115065 Color: 2

Bin 1341: 2 of cap free
Amount of items: 3
Items: 
Size: 534481 Color: 1
Size: 327544 Color: 2
Size: 137974 Color: 3

Bin 1342: 2 of cap free
Amount of items: 3
Items: 
Size: 534574 Color: 3
Size: 359618 Color: 1
Size: 105807 Color: 0

Bin 1343: 2 of cap free
Amount of items: 3
Items: 
Size: 536244 Color: 2
Size: 352932 Color: 1
Size: 110823 Color: 2

Bin 1344: 2 of cap free
Amount of items: 3
Items: 
Size: 536339 Color: 4
Size: 330566 Color: 2
Size: 133094 Color: 3

Bin 1345: 2 of cap free
Amount of items: 3
Items: 
Size: 536657 Color: 2
Size: 342370 Color: 0
Size: 120972 Color: 0

Bin 1346: 2 of cap free
Amount of items: 3
Items: 
Size: 538127 Color: 1
Size: 338095 Color: 2
Size: 123777 Color: 0

Bin 1347: 2 of cap free
Amount of items: 3
Items: 
Size: 540208 Color: 0
Size: 353581 Color: 4
Size: 106210 Color: 3

Bin 1348: 2 of cap free
Amount of items: 3
Items: 
Size: 541496 Color: 3
Size: 347180 Color: 4
Size: 111323 Color: 1

Bin 1349: 2 of cap free
Amount of items: 3
Items: 
Size: 541952 Color: 3
Size: 356343 Color: 0
Size: 101704 Color: 1

Bin 1350: 2 of cap free
Amount of items: 3
Items: 
Size: 543524 Color: 1
Size: 347195 Color: 2
Size: 109280 Color: 4

Bin 1351: 2 of cap free
Amount of items: 3
Items: 
Size: 543576 Color: 1
Size: 339366 Color: 0
Size: 117057 Color: 4

Bin 1352: 2 of cap free
Amount of items: 3
Items: 
Size: 543585 Color: 2
Size: 332190 Color: 1
Size: 124224 Color: 3

Bin 1353: 2 of cap free
Amount of items: 3
Items: 
Size: 544886 Color: 4
Size: 341950 Color: 2
Size: 113163 Color: 3

Bin 1354: 2 of cap free
Amount of items: 3
Items: 
Size: 545013 Color: 2
Size: 332210 Color: 3
Size: 122776 Color: 2

Bin 1355: 2 of cap free
Amount of items: 3
Items: 
Size: 546208 Color: 4
Size: 333593 Color: 3
Size: 120198 Color: 2

Bin 1356: 2 of cap free
Amount of items: 3
Items: 
Size: 546562 Color: 1
Size: 350168 Color: 0
Size: 103269 Color: 1

Bin 1357: 2 of cap free
Amount of items: 3
Items: 
Size: 546695 Color: 3
Size: 351091 Color: 2
Size: 102213 Color: 0

Bin 1358: 2 of cap free
Amount of items: 3
Items: 
Size: 549034 Color: 4
Size: 340633 Color: 0
Size: 110332 Color: 1

Bin 1359: 2 of cap free
Amount of items: 3
Items: 
Size: 549122 Color: 3
Size: 337519 Color: 4
Size: 113358 Color: 0

Bin 1360: 2 of cap free
Amount of items: 3
Items: 
Size: 549922 Color: 4
Size: 340803 Color: 0
Size: 109274 Color: 1

Bin 1361: 2 of cap free
Amount of items: 3
Items: 
Size: 557969 Color: 4
Size: 341486 Color: 2
Size: 100544 Color: 0

Bin 1362: 3 of cap free
Amount of items: 4
Items: 
Size: 419148 Color: 2
Size: 196404 Color: 4
Size: 193267 Color: 3
Size: 191179 Color: 2

Bin 1363: 3 of cap free
Amount of items: 4
Items: 
Size: 419938 Color: 2
Size: 199587 Color: 3
Size: 190575 Color: 4
Size: 189898 Color: 1

Bin 1364: 3 of cap free
Amount of items: 4
Items: 
Size: 420115 Color: 2
Size: 200746 Color: 0
Size: 190679 Color: 2
Size: 188458 Color: 1

Bin 1365: 3 of cap free
Amount of items: 3
Items: 
Size: 429548 Color: 0
Size: 389328 Color: 2
Size: 181122 Color: 1

Bin 1366: 3 of cap free
Amount of items: 3
Items: 
Size: 447757 Color: 2
Size: 385428 Color: 0
Size: 166813 Color: 2

Bin 1367: 3 of cap free
Amount of items: 3
Items: 
Size: 449516 Color: 0
Size: 380738 Color: 3
Size: 169744 Color: 2

Bin 1368: 3 of cap free
Amount of items: 3
Items: 
Size: 449597 Color: 2
Size: 384626 Color: 0
Size: 165775 Color: 0

Bin 1369: 3 of cap free
Amount of items: 3
Items: 
Size: 450271 Color: 4
Size: 381285 Color: 2
Size: 168442 Color: 1

Bin 1370: 3 of cap free
Amount of items: 3
Items: 
Size: 450880 Color: 2
Size: 277666 Color: 3
Size: 271452 Color: 3

Bin 1371: 3 of cap free
Amount of items: 3
Items: 
Size: 452076 Color: 4
Size: 378173 Color: 0
Size: 169749 Color: 4

Bin 1372: 3 of cap free
Amount of items: 3
Items: 
Size: 453089 Color: 4
Size: 380971 Color: 0
Size: 165938 Color: 4

Bin 1373: 3 of cap free
Amount of items: 3
Items: 
Size: 453493 Color: 3
Size: 376982 Color: 2
Size: 169523 Color: 2

Bin 1374: 3 of cap free
Amount of items: 3
Items: 
Size: 453602 Color: 4
Size: 377728 Color: 2
Size: 168668 Color: 2

Bin 1375: 3 of cap free
Amount of items: 3
Items: 
Size: 453763 Color: 0
Size: 377341 Color: 1
Size: 168894 Color: 1

Bin 1376: 3 of cap free
Amount of items: 3
Items: 
Size: 454028 Color: 4
Size: 379604 Color: 1
Size: 166366 Color: 4

Bin 1377: 3 of cap free
Amount of items: 3
Items: 
Size: 454597 Color: 2
Size: 274876 Color: 0
Size: 270525 Color: 1

Bin 1378: 3 of cap free
Amount of items: 3
Items: 
Size: 455138 Color: 4
Size: 280224 Color: 3
Size: 264636 Color: 4

Bin 1379: 3 of cap free
Amount of items: 3
Items: 
Size: 455434 Color: 3
Size: 279379 Color: 2
Size: 265185 Color: 3

Bin 1380: 3 of cap free
Amount of items: 3
Items: 
Size: 457077 Color: 1
Size: 376447 Color: 4
Size: 166474 Color: 2

Bin 1381: 3 of cap free
Amount of items: 3
Items: 
Size: 457255 Color: 1
Size: 280786 Color: 0
Size: 261957 Color: 4

Bin 1382: 3 of cap free
Amount of items: 3
Items: 
Size: 458562 Color: 3
Size: 275981 Color: 2
Size: 265455 Color: 2

Bin 1383: 3 of cap free
Amount of items: 3
Items: 
Size: 458655 Color: 4
Size: 278198 Color: 3
Size: 263145 Color: 0

Bin 1384: 3 of cap free
Amount of items: 3
Items: 
Size: 460493 Color: 3
Size: 280330 Color: 1
Size: 259175 Color: 3

Bin 1385: 3 of cap free
Amount of items: 3
Items: 
Size: 462289 Color: 2
Size: 282998 Color: 0
Size: 254711 Color: 2

Bin 1386: 3 of cap free
Amount of items: 3
Items: 
Size: 463017 Color: 2
Size: 279954 Color: 4
Size: 257027 Color: 3

Bin 1387: 3 of cap free
Amount of items: 3
Items: 
Size: 463631 Color: 3
Size: 283134 Color: 1
Size: 253233 Color: 2

Bin 1388: 3 of cap free
Amount of items: 3
Items: 
Size: 464613 Color: 1
Size: 371750 Color: 0
Size: 163635 Color: 3

Bin 1389: 3 of cap free
Amount of items: 3
Items: 
Size: 464811 Color: 1
Size: 285488 Color: 2
Size: 249699 Color: 3

Bin 1390: 3 of cap free
Amount of items: 3
Items: 
Size: 464899 Color: 1
Size: 283851 Color: 0
Size: 251248 Color: 1

Bin 1391: 3 of cap free
Amount of items: 3
Items: 
Size: 465403 Color: 2
Size: 287515 Color: 4
Size: 247080 Color: 1

Bin 1392: 3 of cap free
Amount of items: 3
Items: 
Size: 465651 Color: 0
Size: 372819 Color: 1
Size: 161528 Color: 0

Bin 1393: 3 of cap free
Amount of items: 3
Items: 
Size: 466241 Color: 1
Size: 289177 Color: 4
Size: 244580 Color: 1

Bin 1394: 3 of cap free
Amount of items: 3
Items: 
Size: 466247 Color: 3
Size: 375342 Color: 0
Size: 158409 Color: 0

Bin 1395: 3 of cap free
Amount of items: 3
Items: 
Size: 468532 Color: 2
Size: 287761 Color: 3
Size: 243705 Color: 1

Bin 1396: 3 of cap free
Amount of items: 3
Items: 
Size: 469491 Color: 4
Size: 287894 Color: 0
Size: 242613 Color: 2

Bin 1397: 3 of cap free
Amount of items: 3
Items: 
Size: 471004 Color: 1
Size: 379338 Color: 2
Size: 149656 Color: 1

Bin 1398: 3 of cap free
Amount of items: 3
Items: 
Size: 472608 Color: 2
Size: 286131 Color: 1
Size: 241259 Color: 2

Bin 1399: 3 of cap free
Amount of items: 3
Items: 
Size: 472731 Color: 1
Size: 285430 Color: 4
Size: 241837 Color: 0

Bin 1400: 3 of cap free
Amount of items: 3
Items: 
Size: 474330 Color: 4
Size: 371870 Color: 0
Size: 153798 Color: 4

Bin 1401: 3 of cap free
Amount of items: 3
Items: 
Size: 474884 Color: 0
Size: 288636 Color: 2
Size: 236478 Color: 3

Bin 1402: 3 of cap free
Amount of items: 3
Items: 
Size: 476479 Color: 3
Size: 290378 Color: 0
Size: 233141 Color: 4

Bin 1403: 3 of cap free
Amount of items: 3
Items: 
Size: 476682 Color: 2
Size: 291436 Color: 3
Size: 231880 Color: 4

Bin 1404: 3 of cap free
Amount of items: 3
Items: 
Size: 479016 Color: 3
Size: 294451 Color: 2
Size: 226531 Color: 1

Bin 1405: 3 of cap free
Amount of items: 3
Items: 
Size: 479251 Color: 4
Size: 293991 Color: 0
Size: 226756 Color: 2

Bin 1406: 3 of cap free
Amount of items: 3
Items: 
Size: 479303 Color: 0
Size: 297230 Color: 4
Size: 223465 Color: 0

Bin 1407: 3 of cap free
Amount of items: 3
Items: 
Size: 479446 Color: 3
Size: 291584 Color: 2
Size: 228968 Color: 2

Bin 1408: 3 of cap free
Amount of items: 3
Items: 
Size: 479739 Color: 4
Size: 295529 Color: 3
Size: 224730 Color: 2

Bin 1409: 3 of cap free
Amount of items: 3
Items: 
Size: 479891 Color: 1
Size: 293564 Color: 4
Size: 226543 Color: 1

Bin 1410: 3 of cap free
Amount of items: 3
Items: 
Size: 479928 Color: 2
Size: 374825 Color: 3
Size: 145245 Color: 2

Bin 1411: 3 of cap free
Amount of items: 3
Items: 
Size: 480284 Color: 3
Size: 299716 Color: 1
Size: 219998 Color: 4

Bin 1412: 3 of cap free
Amount of items: 3
Items: 
Size: 480630 Color: 3
Size: 294807 Color: 0
Size: 224561 Color: 1

Bin 1413: 3 of cap free
Amount of items: 3
Items: 
Size: 481105 Color: 4
Size: 375734 Color: 2
Size: 143159 Color: 2

Bin 1414: 3 of cap free
Amount of items: 3
Items: 
Size: 482093 Color: 4
Size: 294397 Color: 2
Size: 223508 Color: 3

Bin 1415: 3 of cap free
Amount of items: 3
Items: 
Size: 482199 Color: 2
Size: 294277 Color: 4
Size: 223522 Color: 3

Bin 1416: 3 of cap free
Amount of items: 3
Items: 
Size: 483057 Color: 4
Size: 305503 Color: 3
Size: 211438 Color: 0

Bin 1417: 3 of cap free
Amount of items: 3
Items: 
Size: 483395 Color: 3
Size: 294840 Color: 4
Size: 221763 Color: 0

Bin 1418: 3 of cap free
Amount of items: 3
Items: 
Size: 485318 Color: 2
Size: 294575 Color: 0
Size: 220105 Color: 1

Bin 1419: 3 of cap free
Amount of items: 3
Items: 
Size: 485832 Color: 1
Size: 300454 Color: 2
Size: 213712 Color: 3

Bin 1420: 3 of cap free
Amount of items: 3
Items: 
Size: 485909 Color: 4
Size: 364570 Color: 3
Size: 149519 Color: 0

Bin 1421: 3 of cap free
Amount of items: 3
Items: 
Size: 486509 Color: 3
Size: 367471 Color: 2
Size: 146018 Color: 3

Bin 1422: 3 of cap free
Amount of items: 3
Items: 
Size: 487974 Color: 3
Size: 314154 Color: 4
Size: 197870 Color: 1

Bin 1423: 3 of cap free
Amount of items: 3
Items: 
Size: 488852 Color: 1
Size: 299593 Color: 0
Size: 211553 Color: 4

Bin 1424: 3 of cap free
Amount of items: 3
Items: 
Size: 489184 Color: 0
Size: 365290 Color: 4
Size: 145524 Color: 1

Bin 1425: 3 of cap free
Amount of items: 3
Items: 
Size: 494898 Color: 3
Size: 371894 Color: 0
Size: 133206 Color: 3

Bin 1426: 3 of cap free
Amount of items: 3
Items: 
Size: 496726 Color: 0
Size: 299778 Color: 2
Size: 203494 Color: 0

Bin 1427: 3 of cap free
Amount of items: 3
Items: 
Size: 498412 Color: 4
Size: 308680 Color: 3
Size: 192906 Color: 3

Bin 1428: 3 of cap free
Amount of items: 3
Items: 
Size: 502458 Color: 0
Size: 358852 Color: 2
Size: 138688 Color: 3

Bin 1429: 3 of cap free
Amount of items: 2
Items: 
Size: 502472 Color: 3
Size: 497526 Color: 4

Bin 1430: 3 of cap free
Amount of items: 3
Items: 
Size: 506128 Color: 4
Size: 307382 Color: 1
Size: 186488 Color: 2

Bin 1431: 3 of cap free
Amount of items: 3
Items: 
Size: 508480 Color: 0
Size: 307004 Color: 2
Size: 184514 Color: 4

Bin 1432: 3 of cap free
Amount of items: 3
Items: 
Size: 511050 Color: 3
Size: 312090 Color: 0
Size: 176858 Color: 2

Bin 1433: 3 of cap free
Amount of items: 3
Items: 
Size: 513171 Color: 4
Size: 317655 Color: 1
Size: 169172 Color: 2

Bin 1434: 3 of cap free
Amount of items: 3
Items: 
Size: 513738 Color: 3
Size: 315921 Color: 0
Size: 170339 Color: 1

Bin 1435: 3 of cap free
Amount of items: 3
Items: 
Size: 514238 Color: 3
Size: 310346 Color: 1
Size: 175414 Color: 0

Bin 1436: 3 of cap free
Amount of items: 3
Items: 
Size: 515022 Color: 3
Size: 357990 Color: 1
Size: 126986 Color: 3

Bin 1437: 3 of cap free
Amount of items: 3
Items: 
Size: 520286 Color: 1
Size: 326028 Color: 4
Size: 153684 Color: 2

Bin 1438: 3 of cap free
Amount of items: 3
Items: 
Size: 520801 Color: 4
Size: 354574 Color: 1
Size: 124623 Color: 0

Bin 1439: 3 of cap free
Amount of items: 3
Items: 
Size: 521022 Color: 1
Size: 320874 Color: 2
Size: 158102 Color: 1

Bin 1440: 3 of cap free
Amount of items: 3
Items: 
Size: 521047 Color: 4
Size: 333706 Color: 1
Size: 145245 Color: 2

Bin 1441: 3 of cap free
Amount of items: 3
Items: 
Size: 521698 Color: 0
Size: 359699 Color: 4
Size: 118601 Color: 0

Bin 1442: 3 of cap free
Amount of items: 3
Items: 
Size: 523388 Color: 1
Size: 321694 Color: 0
Size: 154916 Color: 1

Bin 1443: 3 of cap free
Amount of items: 3
Items: 
Size: 523968 Color: 4
Size: 319848 Color: 3
Size: 156182 Color: 2

Bin 1444: 3 of cap free
Amount of items: 3
Items: 
Size: 524149 Color: 0
Size: 321230 Color: 2
Size: 154619 Color: 3

Bin 1445: 3 of cap free
Amount of items: 3
Items: 
Size: 525732 Color: 3
Size: 353406 Color: 4
Size: 120860 Color: 3

Bin 1446: 3 of cap free
Amount of items: 3
Items: 
Size: 525791 Color: 3
Size: 319409 Color: 1
Size: 154798 Color: 0

Bin 1447: 3 of cap free
Amount of items: 3
Items: 
Size: 525791 Color: 3
Size: 320035 Color: 0
Size: 154172 Color: 4

Bin 1448: 3 of cap free
Amount of items: 3
Items: 
Size: 526714 Color: 1
Size: 354203 Color: 0
Size: 119081 Color: 4

Bin 1449: 3 of cap free
Amount of items: 3
Items: 
Size: 529794 Color: 2
Size: 327619 Color: 3
Size: 142585 Color: 4

Bin 1450: 3 of cap free
Amount of items: 2
Items: 
Size: 530388 Color: 1
Size: 469610 Color: 0

Bin 1451: 3 of cap free
Amount of items: 3
Items: 
Size: 530448 Color: 3
Size: 349518 Color: 0
Size: 120032 Color: 1

Bin 1452: 3 of cap free
Amount of items: 3
Items: 
Size: 532286 Color: 1
Size: 350197 Color: 3
Size: 117515 Color: 1

Bin 1453: 3 of cap free
Amount of items: 3
Items: 
Size: 534744 Color: 3
Size: 332030 Color: 1
Size: 133224 Color: 2

Bin 1454: 3 of cap free
Amount of items: 3
Items: 
Size: 534751 Color: 2
Size: 327883 Color: 0
Size: 137364 Color: 2

Bin 1455: 3 of cap free
Amount of items: 3
Items: 
Size: 535105 Color: 3
Size: 329617 Color: 1
Size: 135276 Color: 1

Bin 1456: 3 of cap free
Amount of items: 3
Items: 
Size: 540959 Color: 1
Size: 333112 Color: 2
Size: 125927 Color: 0

Bin 1457: 3 of cap free
Amount of items: 3
Items: 
Size: 541738 Color: 1
Size: 329985 Color: 2
Size: 128275 Color: 1

Bin 1458: 3 of cap free
Amount of items: 3
Items: 
Size: 542329 Color: 4
Size: 331132 Color: 0
Size: 126537 Color: 4

Bin 1459: 3 of cap free
Amount of items: 3
Items: 
Size: 546461 Color: 1
Size: 350767 Color: 2
Size: 102770 Color: 1

Bin 1460: 3 of cap free
Amount of items: 3
Items: 
Size: 549363 Color: 1
Size: 332630 Color: 3
Size: 118005 Color: 4

Bin 1461: 3 of cap free
Amount of items: 3
Items: 
Size: 552930 Color: 3
Size: 340471 Color: 4
Size: 106597 Color: 2

Bin 1462: 4 of cap free
Amount of items: 3
Items: 
Size: 404232 Color: 1
Size: 395290 Color: 4
Size: 200475 Color: 4

Bin 1463: 4 of cap free
Amount of items: 4
Items: 
Size: 418684 Color: 4
Size: 195761 Color: 2
Size: 194152 Color: 3
Size: 191400 Color: 0

Bin 1464: 4 of cap free
Amount of items: 3
Items: 
Size: 434805 Color: 4
Size: 387071 Color: 0
Size: 178121 Color: 3

Bin 1465: 4 of cap free
Amount of items: 3
Items: 
Size: 438873 Color: 4
Size: 385367 Color: 1
Size: 175757 Color: 4

Bin 1466: 4 of cap free
Amount of items: 3
Items: 
Size: 447116 Color: 1
Size: 382192 Color: 2
Size: 170689 Color: 1

Bin 1467: 4 of cap free
Amount of items: 3
Items: 
Size: 447809 Color: 0
Size: 283241 Color: 2
Size: 268947 Color: 2

Bin 1468: 4 of cap free
Amount of items: 3
Items: 
Size: 450715 Color: 3
Size: 381349 Color: 0
Size: 167933 Color: 4

Bin 1469: 4 of cap free
Amount of items: 3
Items: 
Size: 451844 Color: 2
Size: 385197 Color: 3
Size: 162956 Color: 2

Bin 1470: 4 of cap free
Amount of items: 3
Items: 
Size: 452171 Color: 1
Size: 384438 Color: 2
Size: 163388 Color: 2

Bin 1471: 4 of cap free
Amount of items: 3
Items: 
Size: 452420 Color: 4
Size: 376087 Color: 2
Size: 171490 Color: 0

Bin 1472: 4 of cap free
Amount of items: 3
Items: 
Size: 453659 Color: 3
Size: 279806 Color: 2
Size: 266532 Color: 1

Bin 1473: 4 of cap free
Amount of items: 3
Items: 
Size: 454463 Color: 2
Size: 380839 Color: 4
Size: 164695 Color: 1

Bin 1474: 4 of cap free
Amount of items: 3
Items: 
Size: 454633 Color: 0
Size: 275419 Color: 3
Size: 269945 Color: 4

Bin 1475: 4 of cap free
Amount of items: 3
Items: 
Size: 455679 Color: 0
Size: 278310 Color: 1
Size: 266008 Color: 4

Bin 1476: 4 of cap free
Amount of items: 3
Items: 
Size: 457561 Color: 3
Size: 375707 Color: 4
Size: 166729 Color: 3

Bin 1477: 4 of cap free
Amount of items: 3
Items: 
Size: 460419 Color: 0
Size: 287865 Color: 4
Size: 251713 Color: 0

Bin 1478: 4 of cap free
Amount of items: 3
Items: 
Size: 464195 Color: 4
Size: 375523 Color: 0
Size: 160279 Color: 2

Bin 1479: 4 of cap free
Amount of items: 3
Items: 
Size: 464831 Color: 4
Size: 371368 Color: 3
Size: 163798 Color: 3

Bin 1480: 4 of cap free
Amount of items: 3
Items: 
Size: 465374 Color: 0
Size: 283765 Color: 3
Size: 250858 Color: 3

Bin 1481: 4 of cap free
Amount of items: 3
Items: 
Size: 465951 Color: 1
Size: 294444 Color: 2
Size: 239602 Color: 2

Bin 1482: 4 of cap free
Amount of items: 3
Items: 
Size: 466450 Color: 1
Size: 287239 Color: 2
Size: 246308 Color: 1

Bin 1483: 4 of cap free
Amount of items: 3
Items: 
Size: 466785 Color: 0
Size: 288997 Color: 4
Size: 244215 Color: 0

Bin 1484: 4 of cap free
Amount of items: 3
Items: 
Size: 466926 Color: 4
Size: 285266 Color: 0
Size: 247805 Color: 0

Bin 1485: 4 of cap free
Amount of items: 3
Items: 
Size: 467435 Color: 1
Size: 283965 Color: 0
Size: 248597 Color: 4

Bin 1486: 4 of cap free
Amount of items: 3
Items: 
Size: 467857 Color: 4
Size: 372199 Color: 0
Size: 159941 Color: 1

Bin 1487: 4 of cap free
Amount of items: 3
Items: 
Size: 467863 Color: 3
Size: 373900 Color: 2
Size: 158234 Color: 3

Bin 1488: 4 of cap free
Amount of items: 3
Items: 
Size: 471995 Color: 0
Size: 292953 Color: 2
Size: 235049 Color: 3

Bin 1489: 4 of cap free
Amount of items: 3
Items: 
Size: 477564 Color: 1
Size: 306285 Color: 2
Size: 216148 Color: 0

Bin 1490: 4 of cap free
Amount of items: 3
Items: 
Size: 480096 Color: 0
Size: 378139 Color: 3
Size: 141762 Color: 1

Bin 1491: 4 of cap free
Amount of items: 3
Items: 
Size: 480381 Color: 1
Size: 375254 Color: 0
Size: 144362 Color: 2

Bin 1492: 4 of cap free
Amount of items: 3
Items: 
Size: 480609 Color: 2
Size: 374924 Color: 1
Size: 144464 Color: 1

Bin 1493: 4 of cap free
Amount of items: 3
Items: 
Size: 485467 Color: 1
Size: 300712 Color: 2
Size: 213818 Color: 4

Bin 1494: 4 of cap free
Amount of items: 3
Items: 
Size: 486048 Color: 0
Size: 367000 Color: 2
Size: 146949 Color: 1

Bin 1495: 4 of cap free
Amount of items: 3
Items: 
Size: 486246 Color: 3
Size: 370997 Color: 4
Size: 142754 Color: 3

Bin 1496: 4 of cap free
Amount of items: 3
Items: 
Size: 489091 Color: 3
Size: 298544 Color: 0
Size: 212362 Color: 2

Bin 1497: 4 of cap free
Amount of items: 3
Items: 
Size: 490790 Color: 4
Size: 360818 Color: 3
Size: 148389 Color: 2

Bin 1498: 4 of cap free
Amount of items: 3
Items: 
Size: 492238 Color: 2
Size: 362715 Color: 4
Size: 145044 Color: 0

Bin 1499: 4 of cap free
Amount of items: 3
Items: 
Size: 496445 Color: 4
Size: 354571 Color: 1
Size: 148981 Color: 0

Bin 1500: 4 of cap free
Amount of items: 3
Items: 
Size: 498375 Color: 1
Size: 363212 Color: 4
Size: 138410 Color: 2

Bin 1501: 4 of cap free
Amount of items: 3
Items: 
Size: 499762 Color: 4
Size: 368302 Color: 0
Size: 131933 Color: 0

Bin 1502: 4 of cap free
Amount of items: 3
Items: 
Size: 502322 Color: 1
Size: 369112 Color: 0
Size: 128563 Color: 3

Bin 1503: 4 of cap free
Amount of items: 3
Items: 
Size: 508054 Color: 1
Size: 304901 Color: 2
Size: 187042 Color: 2

Bin 1504: 4 of cap free
Amount of items: 3
Items: 
Size: 510927 Color: 3
Size: 313041 Color: 1
Size: 176029 Color: 3

Bin 1505: 4 of cap free
Amount of items: 3
Items: 
Size: 511692 Color: 1
Size: 318386 Color: 4
Size: 169919 Color: 0

Bin 1506: 4 of cap free
Amount of items: 3
Items: 
Size: 512933 Color: 0
Size: 313012 Color: 2
Size: 174052 Color: 2

Bin 1507: 4 of cap free
Amount of items: 3
Items: 
Size: 520113 Color: 2
Size: 317207 Color: 0
Size: 162677 Color: 2

Bin 1508: 4 of cap free
Amount of items: 3
Items: 
Size: 520231 Color: 3
Size: 355632 Color: 1
Size: 124134 Color: 3

Bin 1509: 4 of cap free
Amount of items: 3
Items: 
Size: 520723 Color: 2
Size: 319995 Color: 3
Size: 159279 Color: 0

Bin 1510: 4 of cap free
Amount of items: 3
Items: 
Size: 521329 Color: 4
Size: 354184 Color: 3
Size: 124484 Color: 0

Bin 1511: 4 of cap free
Amount of items: 3
Items: 
Size: 521522 Color: 3
Size: 316425 Color: 2
Size: 162050 Color: 3

Bin 1512: 4 of cap free
Amount of items: 3
Items: 
Size: 521977 Color: 3
Size: 350276 Color: 4
Size: 127744 Color: 2

Bin 1513: 4 of cap free
Amount of items: 3
Items: 
Size: 524510 Color: 2
Size: 324979 Color: 1
Size: 150508 Color: 3

Bin 1514: 4 of cap free
Amount of items: 3
Items: 
Size: 525467 Color: 2
Size: 319985 Color: 3
Size: 154545 Color: 3

Bin 1515: 4 of cap free
Amount of items: 3
Items: 
Size: 533320 Color: 0
Size: 342959 Color: 2
Size: 123718 Color: 0

Bin 1516: 4 of cap free
Amount of items: 3
Items: 
Size: 534540 Color: 3
Size: 357660 Color: 0
Size: 107797 Color: 3

Bin 1517: 4 of cap free
Amount of items: 3
Items: 
Size: 535783 Color: 1
Size: 348264 Color: 0
Size: 115950 Color: 3

Bin 1518: 4 of cap free
Amount of items: 3
Items: 
Size: 539509 Color: 0
Size: 328819 Color: 4
Size: 131669 Color: 3

Bin 1519: 4 of cap free
Amount of items: 3
Items: 
Size: 540584 Color: 4
Size: 358318 Color: 1
Size: 101095 Color: 3

Bin 1520: 4 of cap free
Amount of items: 3
Items: 
Size: 542318 Color: 3
Size: 336026 Color: 1
Size: 121653 Color: 0

Bin 1521: 5 of cap free
Amount of items: 4
Items: 
Size: 418637 Color: 2
Size: 194875 Color: 1
Size: 193810 Color: 2
Size: 192674 Color: 3

Bin 1522: 5 of cap free
Amount of items: 4
Items: 
Size: 419247 Color: 0
Size: 198432 Color: 4
Size: 191429 Color: 4
Size: 190888 Color: 3

Bin 1523: 5 of cap free
Amount of items: 3
Items: 
Size: 422342 Color: 0
Size: 392380 Color: 3
Size: 185274 Color: 0

Bin 1524: 5 of cap free
Amount of items: 3
Items: 
Size: 427212 Color: 3
Size: 390706 Color: 1
Size: 182078 Color: 0

Bin 1525: 5 of cap free
Amount of items: 3
Items: 
Size: 432148 Color: 3
Size: 388168 Color: 0
Size: 179680 Color: 2

Bin 1526: 5 of cap free
Amount of items: 3
Items: 
Size: 447324 Color: 4
Size: 377406 Color: 1
Size: 175266 Color: 1

Bin 1527: 5 of cap free
Amount of items: 3
Items: 
Size: 448069 Color: 1
Size: 376818 Color: 3
Size: 175109 Color: 2

Bin 1528: 5 of cap free
Amount of items: 3
Items: 
Size: 448600 Color: 4
Size: 382264 Color: 1
Size: 169132 Color: 2

Bin 1529: 5 of cap free
Amount of items: 3
Items: 
Size: 452407 Color: 3
Size: 380398 Color: 0
Size: 167191 Color: 3

Bin 1530: 5 of cap free
Amount of items: 3
Items: 
Size: 453049 Color: 1
Size: 383624 Color: 2
Size: 163323 Color: 3

Bin 1531: 5 of cap free
Amount of items: 3
Items: 
Size: 453814 Color: 4
Size: 284648 Color: 3
Size: 261534 Color: 2

Bin 1532: 5 of cap free
Amount of items: 3
Items: 
Size: 454834 Color: 1
Size: 276454 Color: 2
Size: 268708 Color: 1

Bin 1533: 5 of cap free
Amount of items: 3
Items: 
Size: 457047 Color: 0
Size: 275769 Color: 2
Size: 267180 Color: 2

Bin 1534: 5 of cap free
Amount of items: 3
Items: 
Size: 466240 Color: 4
Size: 288445 Color: 2
Size: 245311 Color: 0

Bin 1535: 5 of cap free
Amount of items: 3
Items: 
Size: 466860 Color: 3
Size: 285809 Color: 2
Size: 247327 Color: 1

Bin 1536: 5 of cap free
Amount of items: 3
Items: 
Size: 470091 Color: 4
Size: 295120 Color: 0
Size: 234785 Color: 2

Bin 1537: 5 of cap free
Amount of items: 3
Items: 
Size: 476424 Color: 4
Size: 373745 Color: 0
Size: 149827 Color: 3

Bin 1538: 5 of cap free
Amount of items: 3
Items: 
Size: 476528 Color: 2
Size: 375011 Color: 4
Size: 148457 Color: 4

Bin 1539: 5 of cap free
Amount of items: 3
Items: 
Size: 478384 Color: 3
Size: 369984 Color: 0
Size: 151628 Color: 4

Bin 1540: 5 of cap free
Amount of items: 3
Items: 
Size: 479063 Color: 1
Size: 292612 Color: 3
Size: 228321 Color: 1

Bin 1541: 5 of cap free
Amount of items: 3
Items: 
Size: 479499 Color: 2
Size: 298511 Color: 1
Size: 221986 Color: 2

Bin 1542: 5 of cap free
Amount of items: 3
Items: 
Size: 481116 Color: 2
Size: 374592 Color: 3
Size: 144288 Color: 2

Bin 1543: 5 of cap free
Amount of items: 3
Items: 
Size: 481261 Color: 1
Size: 367233 Color: 2
Size: 151502 Color: 4

Bin 1544: 5 of cap free
Amount of items: 3
Items: 
Size: 485408 Color: 0
Size: 297359 Color: 2
Size: 217229 Color: 3

Bin 1545: 5 of cap free
Amount of items: 3
Items: 
Size: 485897 Color: 1
Size: 370814 Color: 4
Size: 143285 Color: 1

Bin 1546: 5 of cap free
Amount of items: 3
Items: 
Size: 486229 Color: 2
Size: 368279 Color: 3
Size: 145488 Color: 2

Bin 1547: 5 of cap free
Amount of items: 3
Items: 
Size: 486408 Color: 2
Size: 359594 Color: 3
Size: 153994 Color: 1

Bin 1548: 5 of cap free
Amount of items: 3
Items: 
Size: 486516 Color: 1
Size: 297446 Color: 4
Size: 216034 Color: 3

Bin 1549: 5 of cap free
Amount of items: 3
Items: 
Size: 491488 Color: 0
Size: 299619 Color: 4
Size: 208889 Color: 2

Bin 1550: 5 of cap free
Amount of items: 3
Items: 
Size: 514015 Color: 3
Size: 310540 Color: 4
Size: 175441 Color: 2

Bin 1551: 5 of cap free
Amount of items: 3
Items: 
Size: 522303 Color: 0
Size: 321353 Color: 4
Size: 156340 Color: 0

Bin 1552: 5 of cap free
Amount of items: 3
Items: 
Size: 525649 Color: 0
Size: 317717 Color: 3
Size: 156630 Color: 0

Bin 1553: 5 of cap free
Amount of items: 3
Items: 
Size: 531366 Color: 4
Size: 341464 Color: 1
Size: 127166 Color: 3

Bin 1554: 5 of cap free
Amount of items: 3
Items: 
Size: 533493 Color: 0
Size: 327455 Color: 2
Size: 139048 Color: 4

Bin 1555: 5 of cap free
Amount of items: 3
Items: 
Size: 534479 Color: 3
Size: 347257 Color: 4
Size: 118260 Color: 1

Bin 1556: 5 of cap free
Amount of items: 3
Items: 
Size: 536575 Color: 4
Size: 327601 Color: 1
Size: 135820 Color: 0

Bin 1557: 5 of cap free
Amount of items: 3
Items: 
Size: 539118 Color: 0
Size: 329546 Color: 2
Size: 131332 Color: 2

Bin 1558: 5 of cap free
Amount of items: 3
Items: 
Size: 541030 Color: 2
Size: 353455 Color: 0
Size: 105511 Color: 2

Bin 1559: 6 of cap free
Amount of items: 3
Items: 
Size: 403987 Color: 2
Size: 395421 Color: 3
Size: 200587 Color: 4

Bin 1560: 6 of cap free
Amount of items: 4
Items: 
Size: 418828 Color: 4
Size: 195450 Color: 1
Size: 193342 Color: 4
Size: 192375 Color: 0

Bin 1561: 6 of cap free
Amount of items: 4
Items: 
Size: 421183 Color: 3
Size: 203403 Color: 0
Size: 187805 Color: 4
Size: 187604 Color: 1

Bin 1562: 6 of cap free
Amount of items: 3
Items: 
Size: 422400 Color: 1
Size: 392362 Color: 3
Size: 185233 Color: 4

Bin 1563: 6 of cap free
Amount of items: 3
Items: 
Size: 428715 Color: 3
Size: 389613 Color: 2
Size: 181667 Color: 2

Bin 1564: 6 of cap free
Amount of items: 3
Items: 
Size: 431189 Color: 0
Size: 388550 Color: 3
Size: 180256 Color: 3

Bin 1565: 6 of cap free
Amount of items: 3
Items: 
Size: 434924 Color: 2
Size: 386962 Color: 1
Size: 178109 Color: 2

Bin 1566: 6 of cap free
Amount of items: 3
Items: 
Size: 446456 Color: 0
Size: 382922 Color: 1
Size: 170617 Color: 4

Bin 1567: 6 of cap free
Amount of items: 3
Items: 
Size: 449367 Color: 4
Size: 280334 Color: 0
Size: 270294 Color: 3

Bin 1568: 6 of cap free
Amount of items: 3
Items: 
Size: 456257 Color: 0
Size: 277671 Color: 3
Size: 266067 Color: 2

Bin 1569: 6 of cap free
Amount of items: 3
Items: 
Size: 463945 Color: 1
Size: 283483 Color: 2
Size: 252567 Color: 4

Bin 1570: 6 of cap free
Amount of items: 3
Items: 
Size: 465374 Color: 3
Size: 374599 Color: 2
Size: 160022 Color: 2

Bin 1571: 6 of cap free
Amount of items: 3
Items: 
Size: 466913 Color: 4
Size: 284256 Color: 1
Size: 248826 Color: 2

Bin 1572: 6 of cap free
Amount of items: 3
Items: 
Size: 468910 Color: 1
Size: 373180 Color: 0
Size: 157905 Color: 1

Bin 1573: 6 of cap free
Amount of items: 3
Items: 
Size: 476606 Color: 1
Size: 371271 Color: 3
Size: 152118 Color: 1

Bin 1574: 6 of cap free
Amount of items: 3
Items: 
Size: 485608 Color: 3
Size: 367744 Color: 0
Size: 146643 Color: 0

Bin 1575: 6 of cap free
Amount of items: 3
Items: 
Size: 486993 Color: 0
Size: 375657 Color: 4
Size: 137345 Color: 0

Bin 1576: 6 of cap free
Amount of items: 3
Items: 
Size: 487158 Color: 2
Size: 297793 Color: 3
Size: 215044 Color: 0

Bin 1577: 6 of cap free
Amount of items: 3
Items: 
Size: 491626 Color: 4
Size: 299697 Color: 1
Size: 208672 Color: 1

Bin 1578: 6 of cap free
Amount of items: 2
Items: 
Size: 504621 Color: 3
Size: 495374 Color: 4

Bin 1579: 6 of cap free
Amount of items: 3
Items: 
Size: 512390 Color: 4
Size: 360741 Color: 0
Size: 126864 Color: 2

Bin 1580: 6 of cap free
Amount of items: 3
Items: 
Size: 521034 Color: 0
Size: 322687 Color: 4
Size: 156274 Color: 3

Bin 1581: 6 of cap free
Amount of items: 3
Items: 
Size: 539696 Color: 2
Size: 351945 Color: 1
Size: 108354 Color: 2

Bin 1582: 7 of cap free
Amount of items: 4
Items: 
Size: 418682 Color: 1
Size: 195033 Color: 0
Size: 194151 Color: 0
Size: 192128 Color: 1

Bin 1583: 7 of cap free
Amount of items: 4
Items: 
Size: 420400 Color: 0
Size: 201309 Color: 2
Size: 189397 Color: 2
Size: 188888 Color: 4

Bin 1584: 7 of cap free
Amount of items: 3
Items: 
Size: 448371 Color: 1
Size: 375839 Color: 2
Size: 175784 Color: 0

Bin 1585: 7 of cap free
Amount of items: 3
Items: 
Size: 454367 Color: 0
Size: 385313 Color: 2
Size: 160314 Color: 1

Bin 1586: 7 of cap free
Amount of items: 3
Items: 
Size: 455062 Color: 1
Size: 281113 Color: 3
Size: 263819 Color: 4

Bin 1587: 7 of cap free
Amount of items: 3
Items: 
Size: 465362 Color: 4
Size: 377537 Color: 3
Size: 157095 Color: 3

Bin 1588: 7 of cap free
Amount of items: 3
Items: 
Size: 466150 Color: 3
Size: 377280 Color: 4
Size: 156564 Color: 2

Bin 1589: 7 of cap free
Amount of items: 3
Items: 
Size: 467992 Color: 0
Size: 374607 Color: 1
Size: 157395 Color: 0

Bin 1590: 7 of cap free
Amount of items: 3
Items: 
Size: 475345 Color: 1
Size: 379288 Color: 4
Size: 145361 Color: 4

Bin 1591: 7 of cap free
Amount of items: 3
Items: 
Size: 480106 Color: 3
Size: 294638 Color: 4
Size: 225250 Color: 3

Bin 1592: 7 of cap free
Amount of items: 3
Items: 
Size: 486878 Color: 1
Size: 366758 Color: 2
Size: 146358 Color: 0

Bin 1593: 7 of cap free
Amount of items: 3
Items: 
Size: 493149 Color: 4
Size: 375791 Color: 0
Size: 131054 Color: 4

Bin 1594: 7 of cap free
Amount of items: 3
Items: 
Size: 531964 Color: 1
Size: 331850 Color: 3
Size: 136180 Color: 1

Bin 1595: 7 of cap free
Amount of items: 3
Items: 
Size: 533772 Color: 0
Size: 350310 Color: 4
Size: 115912 Color: 0

Bin 1596: 7 of cap free
Amount of items: 3
Items: 
Size: 540695 Color: 3
Size: 358009 Color: 4
Size: 101290 Color: 3

Bin 1597: 8 of cap free
Amount of items: 3
Items: 
Size: 421864 Color: 2
Size: 392538 Color: 1
Size: 185591 Color: 3

Bin 1598: 8 of cap free
Amount of items: 3
Items: 
Size: 430721 Color: 0
Size: 388693 Color: 1
Size: 180579 Color: 3

Bin 1599: 8 of cap free
Amount of items: 3
Items: 
Size: 433331 Color: 2
Size: 387571 Color: 0
Size: 179091 Color: 4

Bin 1600: 8 of cap free
Amount of items: 3
Items: 
Size: 438229 Color: 1
Size: 385579 Color: 4
Size: 176185 Color: 0

Bin 1601: 8 of cap free
Amount of items: 3
Items: 
Size: 450712 Color: 1
Size: 381434 Color: 0
Size: 167847 Color: 0

Bin 1602: 8 of cap free
Amount of items: 3
Items: 
Size: 451675 Color: 1
Size: 280621 Color: 2
Size: 267697 Color: 3

Bin 1603: 8 of cap free
Amount of items: 3
Items: 
Size: 463334 Color: 3
Size: 384606 Color: 0
Size: 152053 Color: 4

Bin 1604: 8 of cap free
Amount of items: 3
Items: 
Size: 464638 Color: 4
Size: 288608 Color: 3
Size: 246747 Color: 3

Bin 1605: 8 of cap free
Amount of items: 3
Items: 
Size: 466072 Color: 2
Size: 297789 Color: 3
Size: 236132 Color: 2

Bin 1606: 8 of cap free
Amount of items: 3
Items: 
Size: 476970 Color: 4
Size: 301134 Color: 1
Size: 221889 Color: 1

Bin 1607: 8 of cap free
Amount of items: 3
Items: 
Size: 493706 Color: 1
Size: 363991 Color: 2
Size: 142296 Color: 1

Bin 1608: 8 of cap free
Amount of items: 3
Items: 
Size: 498392 Color: 4
Size: 307637 Color: 3
Size: 193964 Color: 3

Bin 1609: 8 of cap free
Amount of items: 3
Items: 
Size: 522014 Color: 0
Size: 316966 Color: 3
Size: 161013 Color: 4

Bin 1610: 8 of cap free
Amount of items: 3
Items: 
Size: 530258 Color: 4
Size: 349769 Color: 1
Size: 119966 Color: 3

Bin 1611: 9 of cap free
Amount of items: 3
Items: 
Size: 452377 Color: 3
Size: 380735 Color: 2
Size: 166880 Color: 1

Bin 1612: 9 of cap free
Amount of items: 3
Items: 
Size: 464572 Color: 2
Size: 379217 Color: 4
Size: 156203 Color: 0

Bin 1613: 10 of cap free
Amount of items: 3
Items: 
Size: 409857 Color: 3
Size: 392717 Color: 0
Size: 197417 Color: 2

Bin 1614: 10 of cap free
Amount of items: 4
Items: 
Size: 420919 Color: 3
Size: 202352 Color: 2
Size: 188400 Color: 2
Size: 188320 Color: 1

Bin 1615: 10 of cap free
Amount of items: 4
Items: 
Size: 421393 Color: 3
Size: 205003 Color: 0
Size: 186926 Color: 3
Size: 186669 Color: 2

Bin 1616: 10 of cap free
Amount of items: 3
Items: 
Size: 446867 Color: 1
Size: 385635 Color: 4
Size: 167489 Color: 0

Bin 1617: 10 of cap free
Amount of items: 3
Items: 
Size: 520030 Color: 4
Size: 364711 Color: 2
Size: 115250 Color: 0

Bin 1618: 10 of cap free
Amount of items: 3
Items: 
Size: 533536 Color: 3
Size: 325880 Color: 2
Size: 140575 Color: 3

Bin 1619: 10 of cap free
Amount of items: 3
Items: 
Size: 534476 Color: 0
Size: 352466 Color: 1
Size: 113049 Color: 2

Bin 1620: 10 of cap free
Amount of items: 3
Items: 
Size: 538966 Color: 1
Size: 340776 Color: 4
Size: 120249 Color: 2

Bin 1621: 11 of cap free
Amount of items: 3
Items: 
Size: 408107 Color: 2
Size: 393466 Color: 1
Size: 198417 Color: 4

Bin 1622: 11 of cap free
Amount of items: 4
Items: 
Size: 418997 Color: 0
Size: 197985 Color: 2
Size: 191795 Color: 4
Size: 191213 Color: 3

Bin 1623: 11 of cap free
Amount of items: 4
Items: 
Size: 420295 Color: 3
Size: 200483 Color: 0
Size: 190306 Color: 1
Size: 188906 Color: 2

Bin 1624: 11 of cap free
Amount of items: 3
Items: 
Size: 449869 Color: 0
Size: 380213 Color: 4
Size: 169908 Color: 2

Bin 1625: 12 of cap free
Amount of items: 3
Items: 
Size: 431648 Color: 0
Size: 388266 Color: 2
Size: 180075 Color: 0

Bin 1626: 12 of cap free
Amount of items: 3
Items: 
Size: 436042 Color: 4
Size: 386563 Color: 0
Size: 177384 Color: 2

Bin 1627: 12 of cap free
Amount of items: 3
Items: 
Size: 451607 Color: 1
Size: 388431 Color: 4
Size: 159951 Color: 0

Bin 1628: 13 of cap free
Amount of items: 4
Items: 
Size: 421960 Color: 0
Size: 206575 Color: 2
Size: 185844 Color: 2
Size: 185609 Color: 1

Bin 1629: 13 of cap free
Amount of items: 3
Items: 
Size: 426421 Color: 4
Size: 390858 Color: 2
Size: 182709 Color: 0

Bin 1630: 13 of cap free
Amount of items: 3
Items: 
Size: 427291 Color: 0
Size: 390638 Color: 2
Size: 182059 Color: 3

Bin 1631: 13 of cap free
Amount of items: 3
Items: 
Size: 436060 Color: 4
Size: 386537 Color: 0
Size: 177391 Color: 2

Bin 1632: 13 of cap free
Amount of items: 3
Items: 
Size: 437192 Color: 4
Size: 386089 Color: 1
Size: 176707 Color: 1

Bin 1633: 13 of cap free
Amount of items: 3
Items: 
Size: 446515 Color: 4
Size: 383576 Color: 2
Size: 169897 Color: 3

Bin 1634: 13 of cap free
Amount of items: 3
Items: 
Size: 464605 Color: 4
Size: 376456 Color: 1
Size: 158927 Color: 3

Bin 1635: 14 of cap free
Amount of items: 3
Items: 
Size: 407333 Color: 2
Size: 393951 Color: 1
Size: 198703 Color: 4

Bin 1636: 14 of cap free
Amount of items: 4
Items: 
Size: 421017 Color: 2
Size: 203219 Color: 4
Size: 187916 Color: 4
Size: 187835 Color: 1

Bin 1637: 14 of cap free
Amount of items: 3
Items: 
Size: 428459 Color: 3
Size: 389599 Color: 4
Size: 181929 Color: 3

Bin 1638: 14 of cap free
Amount of items: 3
Items: 
Size: 429678 Color: 4
Size: 389405 Color: 0
Size: 180904 Color: 3

Bin 1639: 14 of cap free
Amount of items: 3
Items: 
Size: 439663 Color: 4
Size: 385110 Color: 1
Size: 175214 Color: 0

Bin 1640: 14 of cap free
Amount of items: 3
Items: 
Size: 439929 Color: 0
Size: 384861 Color: 3
Size: 175197 Color: 3

Bin 1641: 14 of cap free
Amount of items: 3
Items: 
Size: 448517 Color: 0
Size: 381306 Color: 3
Size: 170164 Color: 3

Bin 1642: 14 of cap free
Amount of items: 3
Items: 
Size: 463767 Color: 4
Size: 377504 Color: 3
Size: 158716 Color: 0

Bin 1643: 15 of cap free
Amount of items: 3
Items: 
Size: 426159 Color: 0
Size: 390969 Color: 2
Size: 182858 Color: 4

Bin 1644: 15 of cap free
Amount of items: 3
Items: 
Size: 426714 Color: 2
Size: 390676 Color: 0
Size: 182596 Color: 1

Bin 1645: 15 of cap free
Amount of items: 3
Items: 
Size: 450367 Color: 1
Size: 382343 Color: 0
Size: 167276 Color: 4

Bin 1646: 15 of cap free
Amount of items: 3
Items: 
Size: 466781 Color: 2
Size: 285675 Color: 1
Size: 247530 Color: 3

Bin 1647: 16 of cap free
Amount of items: 3
Items: 
Size: 441738 Color: 2
Size: 384080 Color: 3
Size: 174167 Color: 2

Bin 1648: 17 of cap free
Amount of items: 3
Items: 
Size: 404956 Color: 4
Size: 395107 Color: 1
Size: 199921 Color: 3

Bin 1649: 17 of cap free
Amount of items: 3
Items: 
Size: 426237 Color: 0
Size: 390999 Color: 2
Size: 182748 Color: 3

Bin 1650: 17 of cap free
Amount of items: 3
Items: 
Size: 432289 Color: 3
Size: 388095 Color: 0
Size: 179600 Color: 1

Bin 1651: 17 of cap free
Amount of items: 3
Items: 
Size: 439519 Color: 1
Size: 384899 Color: 2
Size: 175566 Color: 3

Bin 1652: 17 of cap free
Amount of items: 3
Items: 
Size: 450020 Color: 3
Size: 381299 Color: 1
Size: 168665 Color: 0

Bin 1653: 18 of cap free
Amount of items: 3
Items: 
Size: 431680 Color: 3
Size: 388227 Color: 1
Size: 180076 Color: 2

Bin 1654: 18 of cap free
Amount of items: 3
Items: 
Size: 434673 Color: 1
Size: 387063 Color: 3
Size: 178247 Color: 1

Bin 1655: 18 of cap free
Amount of items: 3
Items: 
Size: 447928 Color: 0
Size: 380607 Color: 4
Size: 171448 Color: 3

Bin 1656: 18 of cap free
Amount of items: 3
Items: 
Size: 469253 Color: 4
Size: 372106 Color: 1
Size: 158624 Color: 0

Bin 1657: 18 of cap free
Amount of items: 3
Items: 
Size: 469327 Color: 2
Size: 286732 Color: 4
Size: 243924 Color: 2

Bin 1658: 19 of cap free
Amount of items: 3
Items: 
Size: 430536 Color: 4
Size: 388845 Color: 1
Size: 180601 Color: 0

Bin 1659: 19 of cap free
Amount of items: 3
Items: 
Size: 430609 Color: 1
Size: 388833 Color: 4
Size: 180540 Color: 3

Bin 1660: 19 of cap free
Amount of items: 3
Items: 
Size: 435667 Color: 4
Size: 386734 Color: 3
Size: 177581 Color: 2

Bin 1661: 19 of cap free
Amount of items: 3
Items: 
Size: 448990 Color: 4
Size: 380271 Color: 3
Size: 170721 Color: 1

Bin 1662: 19 of cap free
Amount of items: 3
Items: 
Size: 449342 Color: 0
Size: 280864 Color: 2
Size: 269776 Color: 2

Bin 1663: 20 of cap free
Amount of items: 3
Items: 
Size: 403757 Color: 3
Size: 395484 Color: 0
Size: 200740 Color: 3

Bin 1664: 20 of cap free
Amount of items: 3
Items: 
Size: 446945 Color: 2
Size: 380982 Color: 0
Size: 172054 Color: 1

Bin 1665: 21 of cap free
Amount of items: 3
Items: 
Size: 424339 Color: 1
Size: 391621 Color: 0
Size: 184020 Color: 2

Bin 1666: 21 of cap free
Amount of items: 3
Items: 
Size: 441555 Color: 4
Size: 384065 Color: 3
Size: 174360 Color: 1

Bin 1667: 22 of cap free
Amount of items: 3
Items: 
Size: 446682 Color: 2
Size: 382929 Color: 0
Size: 170368 Color: 0

Bin 1668: 22 of cap free
Amount of items: 3
Items: 
Size: 446888 Color: 2
Size: 383974 Color: 4
Size: 169117 Color: 4

Bin 1669: 23 of cap free
Amount of items: 3
Items: 
Size: 428708 Color: 0
Size: 389630 Color: 1
Size: 181640 Color: 3

Bin 1670: 23 of cap free
Amount of items: 3
Items: 
Size: 447996 Color: 0
Size: 384089 Color: 4
Size: 167893 Color: 0

Bin 1671: 24 of cap free
Amount of items: 3
Items: 
Size: 429918 Color: 3
Size: 388692 Color: 2
Size: 181367 Color: 1

Bin 1672: 24 of cap free
Amount of items: 3
Items: 
Size: 436256 Color: 3
Size: 386384 Color: 4
Size: 177337 Color: 2

Bin 1673: 25 of cap free
Amount of items: 3
Items: 
Size: 450370 Color: 0
Size: 275139 Color: 4
Size: 274467 Color: 3

Bin 1674: 26 of cap free
Amount of items: 3
Items: 
Size: 408199 Color: 0
Size: 393998 Color: 1
Size: 197778 Color: 2

Bin 1675: 26 of cap free
Amount of items: 3
Items: 
Size: 443458 Color: 0
Size: 383091 Color: 1
Size: 173426 Color: 0

Bin 1676: 26 of cap free
Amount of items: 3
Items: 
Size: 446114 Color: 2
Size: 383544 Color: 3
Size: 170317 Color: 0

Bin 1677: 27 of cap free
Amount of items: 3
Items: 
Size: 407605 Color: 0
Size: 393972 Color: 3
Size: 198397 Color: 4

Bin 1678: 27 of cap free
Amount of items: 3
Items: 
Size: 409923 Color: 4
Size: 392963 Color: 2
Size: 197088 Color: 0

Bin 1679: 27 of cap free
Amount of items: 4
Items: 
Size: 419831 Color: 1
Size: 201172 Color: 4
Size: 189954 Color: 3
Size: 189017 Color: 1

Bin 1680: 27 of cap free
Amount of items: 3
Items: 
Size: 445953 Color: 1
Size: 383613 Color: 4
Size: 170408 Color: 2

Bin 1681: 28 of cap free
Amount of items: 3
Items: 
Size: 406014 Color: 0
Size: 394851 Color: 3
Size: 199108 Color: 0

Bin 1682: 28 of cap free
Amount of items: 3
Items: 
Size: 406448 Color: 2
Size: 394508 Color: 0
Size: 199017 Color: 2

Bin 1683: 28 of cap free
Amount of items: 3
Items: 
Size: 433761 Color: 4
Size: 387373 Color: 2
Size: 178839 Color: 2

Bin 1684: 29 of cap free
Amount of items: 3
Items: 
Size: 432997 Color: 3
Size: 387560 Color: 0
Size: 179415 Color: 3

Bin 1685: 29 of cap free
Amount of items: 3
Items: 
Size: 433245 Color: 3
Size: 387598 Color: 1
Size: 179129 Color: 0

Bin 1686: 30 of cap free
Amount of items: 3
Items: 
Size: 407323 Color: 3
Size: 394080 Color: 2
Size: 198568 Color: 2

Bin 1687: 32 of cap free
Amount of items: 4
Items: 
Size: 421021 Color: 2
Size: 203327 Color: 4
Size: 188261 Color: 4
Size: 187360 Color: 0

Bin 1688: 32 of cap free
Amount of items: 3
Items: 
Size: 424908 Color: 3
Size: 391187 Color: 2
Size: 183874 Color: 3

Bin 1689: 32 of cap free
Amount of items: 3
Items: 
Size: 427538 Color: 0
Size: 390533 Color: 3
Size: 181898 Color: 1

Bin 1690: 32 of cap free
Amount of items: 3
Items: 
Size: 437178 Color: 4
Size: 385907 Color: 0
Size: 176884 Color: 2

Bin 1691: 32 of cap free
Amount of items: 3
Items: 
Size: 446418 Color: 2
Size: 378865 Color: 0
Size: 174686 Color: 0

Bin 1692: 33 of cap free
Amount of items: 4
Items: 
Size: 418636 Color: 0
Size: 194329 Color: 1
Size: 194299 Color: 1
Size: 192704 Color: 4

Bin 1693: 33 of cap free
Amount of items: 3
Items: 
Size: 444584 Color: 3
Size: 382458 Color: 1
Size: 172926 Color: 4

Bin 1694: 34 of cap free
Amount of items: 3
Items: 
Size: 425384 Color: 1
Size: 391205 Color: 3
Size: 183378 Color: 3

Bin 1695: 34 of cap free
Amount of items: 3
Items: 
Size: 435656 Color: 0
Size: 386794 Color: 2
Size: 177517 Color: 4

Bin 1696: 34 of cap free
Amount of items: 3
Items: 
Size: 436082 Color: 2
Size: 386432 Color: 1
Size: 177453 Color: 3

Bin 1697: 35 of cap free
Amount of items: 4
Items: 
Size: 419805 Color: 4
Size: 198671 Color: 1
Size: 191132 Color: 4
Size: 190358 Color: 3

Bin 1698: 35 of cap free
Amount of items: 3
Items: 
Size: 439459 Color: 2
Size: 385039 Color: 4
Size: 175468 Color: 1

Bin 1699: 35 of cap free
Amount of items: 3
Items: 
Size: 447714 Color: 3
Size: 383394 Color: 2
Size: 168858 Color: 3

Bin 1700: 36 of cap free
Amount of items: 3
Items: 
Size: 432653 Color: 3
Size: 387929 Color: 4
Size: 179383 Color: 3

Bin 1701: 37 of cap free
Amount of items: 3
Items: 
Size: 405102 Color: 2
Size: 394984 Color: 0
Size: 199878 Color: 3

Bin 1702: 37 of cap free
Amount of items: 4
Items: 
Size: 412793 Color: 2
Size: 195734 Color: 3
Size: 195723 Color: 4
Size: 195714 Color: 3

Bin 1703: 40 of cap free
Amount of items: 3
Items: 
Size: 424608 Color: 3
Size: 391422 Color: 4
Size: 183931 Color: 1

Bin 1704: 40 of cap free
Amount of items: 3
Items: 
Size: 431661 Color: 3
Size: 388188 Color: 1
Size: 180112 Color: 4

Bin 1705: 41 of cap free
Amount of items: 3
Items: 
Size: 431897 Color: 1
Size: 388221 Color: 4
Size: 179842 Color: 1

Bin 1706: 42 of cap free
Amount of items: 3
Items: 
Size: 438286 Color: 1
Size: 385754 Color: 2
Size: 175919 Color: 0

Bin 1707: 43 of cap free
Amount of items: 3
Items: 
Size: 433961 Color: 0
Size: 387209 Color: 2
Size: 178788 Color: 0

Bin 1708: 43 of cap free
Amount of items: 3
Items: 
Size: 447012 Color: 1
Size: 381971 Color: 0
Size: 170975 Color: 1

Bin 1709: 44 of cap free
Amount of items: 3
Items: 
Size: 423658 Color: 1
Size: 392068 Color: 3
Size: 184231 Color: 0

Bin 1710: 44 of cap free
Amount of items: 3
Items: 
Size: 432169 Color: 4
Size: 388066 Color: 3
Size: 179722 Color: 1

Bin 1711: 45 of cap free
Amount of items: 3
Items: 
Size: 405886 Color: 0
Size: 394910 Color: 4
Size: 199160 Color: 4

Bin 1712: 45 of cap free
Amount of items: 3
Items: 
Size: 410547 Color: 0
Size: 392812 Color: 1
Size: 196597 Color: 2

Bin 1713: 45 of cap free
Amount of items: 3
Items: 
Size: 440131 Color: 2
Size: 384601 Color: 1
Size: 175224 Color: 0

Bin 1714: 47 of cap free
Amount of items: 3
Items: 
Size: 409316 Color: 4
Size: 393305 Color: 3
Size: 197333 Color: 2

Bin 1715: 51 of cap free
Amount of items: 3
Items: 
Size: 440440 Color: 2
Size: 384427 Color: 4
Size: 175083 Color: 0

Bin 1716: 52 of cap free
Amount of items: 3
Items: 
Size: 404757 Color: 4
Size: 394711 Color: 1
Size: 200481 Color: 2

Bin 1717: 53 of cap free
Amount of items: 3
Items: 
Size: 425330 Color: 2
Size: 391156 Color: 0
Size: 183462 Color: 3

Bin 1718: 54 of cap free
Amount of items: 3
Items: 
Size: 405264 Color: 2
Size: 395224 Color: 1
Size: 199459 Color: 3

Bin 1719: 54 of cap free
Amount of items: 3
Items: 
Size: 439226 Color: 4
Size: 384956 Color: 2
Size: 175765 Color: 1

Bin 1720: 54 of cap free
Amount of items: 3
Items: 
Size: 446238 Color: 1
Size: 380441 Color: 2
Size: 173268 Color: 1

Bin 1721: 55 of cap free
Amount of items: 3
Items: 
Size: 403199 Color: 4
Size: 395360 Color: 3
Size: 201387 Color: 4

Bin 1722: 56 of cap free
Amount of items: 3
Items: 
Size: 428590 Color: 2
Size: 389620 Color: 0
Size: 181735 Color: 1

Bin 1723: 58 of cap free
Amount of items: 3
Items: 
Size: 446863 Color: 1
Size: 379399 Color: 3
Size: 173681 Color: 1

Bin 1724: 61 of cap free
Amount of items: 3
Items: 
Size: 403527 Color: 2
Size: 395362 Color: 0
Size: 201051 Color: 2

Bin 1725: 61 of cap free
Amount of items: 3
Items: 
Size: 437712 Color: 2
Size: 385595 Color: 3
Size: 176633 Color: 1

Bin 1726: 62 of cap free
Amount of items: 4
Items: 
Size: 420924 Color: 0
Size: 203354 Color: 2
Size: 187884 Color: 4
Size: 187777 Color: 3

Bin 1727: 62 of cap free
Amount of items: 3
Items: 
Size: 432569 Color: 2
Size: 387862 Color: 0
Size: 179508 Color: 0

Bin 1728: 64 of cap free
Amount of items: 3
Items: 
Size: 426849 Color: 1
Size: 390714 Color: 4
Size: 182374 Color: 1

Bin 1729: 64 of cap free
Amount of items: 3
Items: 
Size: 440531 Color: 2
Size: 384444 Color: 0
Size: 174962 Color: 0

Bin 1730: 65 of cap free
Amount of items: 4
Items: 
Size: 422119 Color: 2
Size: 206475 Color: 4
Size: 185676 Color: 4
Size: 185666 Color: 0

Bin 1731: 68 of cap free
Amount of items: 4
Items: 
Size: 420678 Color: 4
Size: 202470 Color: 1
Size: 188926 Color: 0
Size: 187859 Color: 4

Bin 1732: 68 of cap free
Amount of items: 3
Items: 
Size: 435204 Color: 2
Size: 386839 Color: 4
Size: 177890 Color: 4

Bin 1733: 70 of cap free
Amount of items: 3
Items: 
Size: 406339 Color: 3
Size: 394612 Color: 0
Size: 198980 Color: 0

Bin 1734: 74 of cap free
Amount of items: 3
Items: 
Size: 442243 Color: 2
Size: 384259 Color: 3
Size: 173425 Color: 4

Bin 1735: 75 of cap free
Amount of items: 3
Items: 
Size: 403719 Color: 3
Size: 395604 Color: 4
Size: 200603 Color: 0

Bin 1736: 75 of cap free
Amount of items: 3
Items: 
Size: 405019 Color: 3
Size: 394762 Color: 2
Size: 200145 Color: 2

Bin 1737: 75 of cap free
Amount of items: 3
Items: 
Size: 407192 Color: 4
Size: 394039 Color: 0
Size: 198695 Color: 4

Bin 1738: 76 of cap free
Amount of items: 3
Items: 
Size: 428200 Color: 2
Size: 390016 Color: 4
Size: 181709 Color: 0

Bin 1739: 80 of cap free
Amount of items: 3
Items: 
Size: 407021 Color: 0
Size: 394376 Color: 1
Size: 198524 Color: 4

Bin 1740: 83 of cap free
Amount of items: 3
Items: 
Size: 422909 Color: 0
Size: 392247 Color: 4
Size: 184762 Color: 4

Bin 1741: 83 of cap free
Amount of items: 3
Items: 
Size: 435453 Color: 1
Size: 386862 Color: 2
Size: 177603 Color: 4

Bin 1742: 85 of cap free
Amount of items: 4
Items: 
Size: 414965 Color: 3
Size: 195095 Color: 0
Size: 194956 Color: 4
Size: 194900 Color: 1

Bin 1743: 87 of cap free
Amount of items: 3
Items: 
Size: 423924 Color: 4
Size: 391742 Color: 0
Size: 184248 Color: 3

Bin 1744: 88 of cap free
Amount of items: 3
Items: 
Size: 407189 Color: 3
Size: 393941 Color: 1
Size: 198783 Color: 0

Bin 1745: 88 of cap free
Amount of items: 4
Items: 
Size: 412354 Color: 4
Size: 195932 Color: 1
Size: 195828 Color: 3
Size: 195799 Color: 0

Bin 1746: 89 of cap free
Amount of items: 3
Items: 
Size: 429281 Color: 1
Size: 389458 Color: 0
Size: 181173 Color: 4

Bin 1747: 91 of cap free
Amount of items: 3
Items: 
Size: 405366 Color: 1
Size: 395022 Color: 0
Size: 199522 Color: 2

Bin 1748: 93 of cap free
Amount of items: 3
Items: 
Size: 426108 Color: 3
Size: 390889 Color: 1
Size: 182911 Color: 2

Bin 1749: 95 of cap free
Amount of items: 3
Items: 
Size: 433863 Color: 0
Size: 387138 Color: 1
Size: 178905 Color: 2

Bin 1750: 95 of cap free
Amount of items: 3
Items: 
Size: 434642 Color: 1
Size: 387063 Color: 2
Size: 178201 Color: 1

Bin 1751: 95 of cap free
Amount of items: 3
Items: 
Size: 445754 Color: 2
Size: 381149 Color: 3
Size: 173003 Color: 4

Bin 1752: 99 of cap free
Amount of items: 4
Items: 
Size: 421744 Color: 3
Size: 206036 Color: 4
Size: 186072 Color: 2
Size: 186050 Color: 4

Bin 1753: 101 of cap free
Amount of items: 4
Items: 
Size: 414242 Color: 0
Size: 195386 Color: 4
Size: 195192 Color: 4
Size: 195080 Color: 2

Bin 1754: 103 of cap free
Amount of items: 3
Items: 
Size: 424439 Color: 0
Size: 391377 Color: 1
Size: 184082 Color: 3

Bin 1755: 106 of cap free
Amount of items: 3
Items: 
Size: 423617 Color: 4
Size: 392048 Color: 3
Size: 184230 Color: 2

Bin 1756: 107 of cap free
Amount of items: 3
Items: 
Size: 409723 Color: 0
Size: 392999 Color: 1
Size: 197172 Color: 2

Bin 1757: 114 of cap free
Amount of items: 3
Items: 
Size: 429883 Color: 3
Size: 388520 Color: 4
Size: 181484 Color: 1

Bin 1758: 116 of cap free
Amount of items: 3
Items: 
Size: 430558 Color: 4
Size: 388933 Color: 0
Size: 180394 Color: 3

Bin 1759: 123 of cap free
Amount of items: 4
Items: 
Size: 421132 Color: 2
Size: 204067 Color: 0
Size: 187563 Color: 1
Size: 187116 Color: 0

Bin 1760: 132 of cap free
Amount of items: 3
Items: 
Size: 445594 Color: 1
Size: 383103 Color: 4
Size: 171172 Color: 1

Bin 1761: 134 of cap free
Amount of items: 3
Items: 
Size: 428014 Color: 1
Size: 390213 Color: 3
Size: 181640 Color: 3

Bin 1762: 137 of cap free
Amount of items: 3
Items: 
Size: 410870 Color: 2
Size: 392847 Color: 0
Size: 196147 Color: 4

Bin 1763: 149 of cap free
Amount of items: 4
Items: 
Size: 420674 Color: 4
Size: 202463 Color: 0
Size: 188622 Color: 4
Size: 188093 Color: 0

Bin 1764: 150 of cap free
Amount of items: 3
Items: 
Size: 409257 Color: 2
Size: 393135 Color: 1
Size: 197459 Color: 2

Bin 1765: 151 of cap free
Amount of items: 3
Items: 
Size: 445393 Color: 2
Size: 382112 Color: 3
Size: 172345 Color: 3

Bin 1766: 162 of cap free
Amount of items: 3
Items: 
Size: 404821 Color: 1
Size: 394920 Color: 0
Size: 200098 Color: 4

Bin 1767: 169 of cap free
Amount of items: 3
Items: 
Size: 442244 Color: 3
Size: 384044 Color: 4
Size: 173544 Color: 3

Bin 1768: 181 of cap free
Amount of items: 3
Items: 
Size: 427529 Color: 3
Size: 390429 Color: 2
Size: 181862 Color: 1

Bin 1769: 188 of cap free
Amount of items: 3
Items: 
Size: 445808 Color: 3
Size: 383194 Color: 1
Size: 170811 Color: 2

Bin 1770: 197 of cap free
Amount of items: 4
Items: 
Size: 418845 Color: 1
Size: 197956 Color: 2
Size: 191523 Color: 4
Size: 191480 Color: 0

Bin 1771: 200 of cap free
Amount of items: 3
Items: 
Size: 429842 Color: 4
Size: 388721 Color: 0
Size: 181238 Color: 0

Bin 1772: 243 of cap free
Amount of items: 3
Items: 
Size: 408198 Color: 3
Size: 393606 Color: 4
Size: 197954 Color: 4

Bin 1773: 243 of cap free
Amount of items: 3
Items: 
Size: 422769 Color: 2
Size: 392041 Color: 3
Size: 184948 Color: 4

Bin 1774: 279 of cap free
Amount of items: 3
Items: 
Size: 409157 Color: 2
Size: 393108 Color: 1
Size: 197457 Color: 1

Bin 1775: 280 of cap free
Amount of items: 3
Items: 
Size: 423733 Color: 0
Size: 391634 Color: 1
Size: 184354 Color: 4

Bin 1776: 282 of cap free
Amount of items: 3
Items: 
Size: 436774 Color: 4
Size: 386104 Color: 3
Size: 176841 Color: 0

Bin 1777: 302 of cap free
Amount of items: 3
Items: 
Size: 426074 Color: 4
Size: 390817 Color: 3
Size: 182808 Color: 2

Bin 1778: 2851 of cap free
Amount of items: 3
Items: 
Size: 445266 Color: 0
Size: 276804 Color: 1
Size: 275080 Color: 0

Bin 1779: 4862 of cap free
Amount of items: 3
Items: 
Size: 445203 Color: 2
Size: 274989 Color: 4
Size: 274947 Color: 2

Bin 1780: 5495 of cap free
Amount of items: 3
Items: 
Size: 445045 Color: 2
Size: 274753 Color: 4
Size: 274708 Color: 3

Bin 1781: 5831 of cap free
Amount of items: 3
Items: 
Size: 445045 Color: 1
Size: 274690 Color: 3
Size: 274435 Color: 3

Bin 1782: 6747 of cap free
Amount of items: 3
Items: 
Size: 445009 Color: 1
Size: 274176 Color: 0
Size: 274069 Color: 0

Bin 1783: 7012 of cap free
Amount of items: 3
Items: 
Size: 445005 Color: 1
Size: 274020 Color: 2
Size: 273964 Color: 1

Bin 1784: 7767 of cap free
Amount of items: 3
Items: 
Size: 444754 Color: 4
Size: 273788 Color: 0
Size: 273692 Color: 4

Bin 1785: 8414 of cap free
Amount of items: 3
Items: 
Size: 444582 Color: 2
Size: 273592 Color: 0
Size: 273413 Color: 2

Bin 1786: 8700 of cap free
Amount of items: 3
Items: 
Size: 444521 Color: 3
Size: 273393 Color: 0
Size: 273387 Color: 4

Bin 1787: 9414 of cap free
Amount of items: 3
Items: 
Size: 444520 Color: 2
Size: 273082 Color: 1
Size: 272985 Color: 3

Bin 1788: 10001 of cap free
Amount of items: 3
Items: 
Size: 444394 Color: 2
Size: 272945 Color: 4
Size: 272661 Color: 4

Bin 1789: 10989 of cap free
Amount of items: 3
Items: 
Size: 443932 Color: 4
Size: 272560 Color: 0
Size: 272520 Color: 3

Bin 1790: 11349 of cap free
Amount of items: 3
Items: 
Size: 443848 Color: 4
Size: 272426 Color: 2
Size: 272378 Color: 1

Bin 1791: 11632 of cap free
Amount of items: 3
Items: 
Size: 443690 Color: 2
Size: 272352 Color: 1
Size: 272327 Color: 1

Bin 1792: 11802 of cap free
Amount of items: 3
Items: 
Size: 443804 Color: 1
Size: 272235 Color: 3
Size: 272160 Color: 1

Bin 1793: 12037 of cap free
Amount of items: 3
Items: 
Size: 443782 Color: 1
Size: 272110 Color: 2
Size: 272072 Color: 0

Bin 1794: 12291 of cap free
Amount of items: 3
Items: 
Size: 443610 Color: 4
Size: 272053 Color: 0
Size: 272047 Color: 4

Bin 1795: 12665 of cap free
Amount of items: 3
Items: 
Size: 443448 Color: 4
Size: 271983 Color: 3
Size: 271905 Color: 3

Bin 1796: 13127 of cap free
Amount of items: 3
Items: 
Size: 443440 Color: 4
Size: 271797 Color: 3
Size: 271637 Color: 2

Bin 1797: 13760 of cap free
Amount of items: 3
Items: 
Size: 443387 Color: 4
Size: 271527 Color: 3
Size: 271327 Color: 0

Bin 1798: 13855 of cap free
Amount of items: 3
Items: 
Size: 443414 Color: 3
Size: 271385 Color: 1
Size: 271347 Color: 2

Bin 1799: 14354 of cap free
Amount of items: 3
Items: 
Size: 443352 Color: 1
Size: 271159 Color: 2
Size: 271136 Color: 0

Bin 1800: 14810 of cap free
Amount of items: 3
Items: 
Size: 443334 Color: 4
Size: 270973 Color: 0
Size: 270884 Color: 1

Bin 1801: 15411 of cap free
Amount of items: 3
Items: 
Size: 443196 Color: 1
Size: 270700 Color: 3
Size: 270694 Color: 0

Bin 1802: 15584 of cap free
Amount of items: 3
Items: 
Size: 443164 Color: 2
Size: 270736 Color: 1
Size: 270517 Color: 1

Bin 1803: 16159 of cap free
Amount of items: 3
Items: 
Size: 443170 Color: 1
Size: 270352 Color: 0
Size: 270320 Color: 2

Bin 1804: 16366 of cap free
Amount of items: 3
Items: 
Size: 443114 Color: 1
Size: 270303 Color: 3
Size: 270218 Color: 2

Bin 1805: 16834 of cap free
Amount of items: 3
Items: 
Size: 442931 Color: 1
Size: 270201 Color: 2
Size: 270035 Color: 1

Bin 1806: 17373 of cap free
Amount of items: 3
Items: 
Size: 442902 Color: 0
Size: 269883 Color: 1
Size: 269843 Color: 0

Bin 1807: 17764 of cap free
Amount of items: 3
Items: 
Size: 442891 Color: 1
Size: 269719 Color: 2
Size: 269627 Color: 0

Bin 1808: 18551 of cap free
Amount of items: 3
Items: 
Size: 442818 Color: 3
Size: 269390 Color: 1
Size: 269242 Color: 3

Bin 1809: 19109 of cap free
Amount of items: 3
Items: 
Size: 442631 Color: 4
Size: 269143 Color: 2
Size: 269118 Color: 3

Bin 1810: 20323 of cap free
Amount of items: 3
Items: 
Size: 442564 Color: 0
Size: 268607 Color: 4
Size: 268507 Color: 1

Bin 1811: 21105 of cap free
Amount of items: 3
Items: 
Size: 442238 Color: 3
Size: 268357 Color: 2
Size: 268301 Color: 1

Bin 1812: 21465 of cap free
Amount of items: 3
Items: 
Size: 442163 Color: 3
Size: 268276 Color: 1
Size: 268097 Color: 0

Bin 1813: 22094 of cap free
Amount of items: 3
Items: 
Size: 442112 Color: 3
Size: 267906 Color: 4
Size: 267889 Color: 1

Bin 1814: 22811 of cap free
Amount of items: 3
Items: 
Size: 442078 Color: 2
Size: 267732 Color: 4
Size: 267380 Color: 0

Bin 1815: 24164 of cap free
Amount of items: 3
Items: 
Size: 441926 Color: 4
Size: 267160 Color: 3
Size: 266751 Color: 4

Bin 1816: 24309 of cap free
Amount of items: 3
Items: 
Size: 441992 Color: 3
Size: 266877 Color: 1
Size: 266823 Color: 1

Bin 1817: 24713 of cap free
Amount of items: 3
Items: 
Size: 441899 Color: 3
Size: 266710 Color: 4
Size: 266679 Color: 2

Bin 1818: 25318 of cap free
Amount of items: 3
Items: 
Size: 441776 Color: 0
Size: 266454 Color: 2
Size: 266453 Color: 1

Bin 1819: 25596 of cap free
Amount of items: 3
Items: 
Size: 441645 Color: 1
Size: 266412 Color: 3
Size: 266348 Color: 1

Bin 1820: 26154 of cap free
Amount of items: 3
Items: 
Size: 441489 Color: 4
Size: 266326 Color: 3
Size: 266032 Color: 0

Bin 1821: 26851 of cap free
Amount of items: 3
Items: 
Size: 441423 Color: 1
Size: 265864 Color: 3
Size: 265863 Color: 4

Bin 1822: 27106 of cap free
Amount of items: 3
Items: 
Size: 441388 Color: 3
Size: 265801 Color: 0
Size: 265706 Color: 1

Bin 1823: 27404 of cap free
Amount of items: 3
Items: 
Size: 441294 Color: 0
Size: 265694 Color: 4
Size: 265609 Color: 4

Bin 1824: 27961 of cap free
Amount of items: 3
Items: 
Size: 441205 Color: 2
Size: 265588 Color: 3
Size: 265247 Color: 0

Bin 1825: 28905 of cap free
Amount of items: 3
Items: 
Size: 441150 Color: 0
Size: 264977 Color: 4
Size: 264969 Color: 0

Bin 1826: 29277 of cap free
Amount of items: 3
Items: 
Size: 441137 Color: 4
Size: 265039 Color: 0
Size: 264548 Color: 3

Bin 1827: 30293 of cap free
Amount of items: 3
Items: 
Size: 441133 Color: 4
Size: 264322 Color: 3
Size: 264253 Color: 2

Bin 1828: 30604 of cap free
Amount of items: 3
Items: 
Size: 441017 Color: 4
Size: 264195 Color: 0
Size: 264185 Color: 0

Bin 1829: 31526 of cap free
Amount of items: 3
Items: 
Size: 440954 Color: 4
Size: 264173 Color: 0
Size: 263348 Color: 2

Bin 1830: 31862 of cap free
Amount of items: 3
Items: 
Size: 441102 Color: 0
Size: 263688 Color: 1
Size: 263349 Color: 2

Bin 1831: 33706 of cap free
Amount of items: 3
Items: 
Size: 440925 Color: 2
Size: 262697 Color: 0
Size: 262673 Color: 1

Bin 1832: 34216 of cap free
Amount of items: 3
Items: 
Size: 440924 Color: 3
Size: 262562 Color: 1
Size: 262299 Color: 3

Bin 1833: 34806 of cap free
Amount of items: 3
Items: 
Size: 440807 Color: 3
Size: 262293 Color: 4
Size: 262095 Color: 1

Bin 1834: 35876 of cap free
Amount of items: 3
Items: 
Size: 440807 Color: 2
Size: 261835 Color: 0
Size: 261483 Color: 4

Bin 1835: 35916 of cap free
Amount of items: 3
Items: 
Size: 440801 Color: 3
Size: 261892 Color: 2
Size: 261392 Color: 0

Bin 1836: 37309 of cap free
Amount of items: 3
Items: 
Size: 440755 Color: 4
Size: 261158 Color: 3
Size: 260779 Color: 3

Bin 1837: 37477 of cap free
Amount of items: 3
Items: 
Size: 440685 Color: 0
Size: 261195 Color: 4
Size: 260644 Color: 3

Bin 1838: 38280 of cap free
Amount of items: 3
Items: 
Size: 440674 Color: 0
Size: 260543 Color: 4
Size: 260504 Color: 4

Bin 1839: 38919 of cap free
Amount of items: 3
Items: 
Size: 440528 Color: 4
Size: 260472 Color: 3
Size: 260082 Color: 2

Bin 1840: 39751 of cap free
Amount of items: 3
Items: 
Size: 440446 Color: 4
Size: 259936 Color: 1
Size: 259868 Color: 3

Bin 1841: 40261 of cap free
Amount of items: 3
Items: 
Size: 440421 Color: 0
Size: 259741 Color: 2
Size: 259578 Color: 1

Bin 1842: 41455 of cap free
Amount of items: 3
Items: 
Size: 440419 Color: 0
Size: 259132 Color: 4
Size: 258995 Color: 1

Bin 1843: 41748 of cap free
Amount of items: 3
Items: 
Size: 440393 Color: 2
Size: 259228 Color: 0
Size: 258632 Color: 0

Bin 1844: 42754 of cap free
Amount of items: 3
Items: 
Size: 440326 Color: 0
Size: 258465 Color: 3
Size: 258456 Color: 4

Bin 1845: 42833 of cap free
Amount of items: 3
Items: 
Size: 440281 Color: 4
Size: 258538 Color: 0
Size: 258349 Color: 0

Bin 1846: 43109 of cap free
Amount of items: 3
Items: 
Size: 440248 Color: 3
Size: 258325 Color: 1
Size: 258319 Color: 0

Bin 1847: 43624 of cap free
Amount of items: 3
Items: 
Size: 440013 Color: 2
Size: 258194 Color: 1
Size: 258170 Color: 1

Bin 1848: 43673 of cap free
Amount of items: 3
Items: 
Size: 439975 Color: 3
Size: 258294 Color: 2
Size: 258059 Color: 0

Bin 1849: 44110 of cap free
Amount of items: 3
Items: 
Size: 439918 Color: 1
Size: 258015 Color: 3
Size: 257958 Color: 2

Bin 1850: 45025 of cap free
Amount of items: 3
Items: 
Size: 439397 Color: 0
Size: 257836 Color: 3
Size: 257743 Color: 1

Bin 1851: 45118 of cap free
Amount of items: 3
Items: 
Size: 439381 Color: 1
Size: 257851 Color: 0
Size: 257651 Color: 3

Bin 1852: 45500 of cap free
Amount of items: 3
Items: 
Size: 439363 Color: 3
Size: 257617 Color: 1
Size: 257521 Color: 3

Bin 1853: 46309 of cap free
Amount of items: 3
Items: 
Size: 439173 Color: 1
Size: 257345 Color: 2
Size: 257174 Color: 0

Bin 1854: 47683 of cap free
Amount of items: 3
Items: 
Size: 439013 Color: 1
Size: 256745 Color: 2
Size: 256560 Color: 2

Bin 1855: 48109 of cap free
Amount of items: 3
Items: 
Size: 438869 Color: 3
Size: 256559 Color: 0
Size: 256464 Color: 1

Bin 1856: 48285 of cap free
Amount of items: 3
Items: 
Size: 438804 Color: 2
Size: 256456 Color: 0
Size: 256456 Color: 0

Bin 1857: 48594 of cap free
Amount of items: 3
Items: 
Size: 438752 Color: 1
Size: 256351 Color: 4
Size: 256304 Color: 4

Bin 1858: 49331 of cap free
Amount of items: 3
Items: 
Size: 438631 Color: 4
Size: 256277 Color: 0
Size: 255762 Color: 3

Bin 1859: 49558 of cap free
Amount of items: 3
Items: 
Size: 438720 Color: 0
Size: 255867 Color: 4
Size: 255856 Color: 1

Bin 1860: 50863 of cap free
Amount of items: 3
Items: 
Size: 438528 Color: 2
Size: 255453 Color: 0
Size: 255157 Color: 1

Bin 1861: 51716 of cap free
Amount of items: 3
Items: 
Size: 438264 Color: 0
Size: 255039 Color: 2
Size: 254982 Color: 4

Bin 1862: 51951 of cap free
Amount of items: 3
Items: 
Size: 438202 Color: 2
Size: 254925 Color: 1
Size: 254923 Color: 1

Bin 1863: 52164 of cap free
Amount of items: 3
Items: 
Size: 438166 Color: 0
Size: 254849 Color: 2
Size: 254822 Color: 0

Bin 1864: 52847 of cap free
Amount of items: 3
Items: 
Size: 437844 Color: 0
Size: 254717 Color: 4
Size: 254593 Color: 4

Bin 1865: 53155 of cap free
Amount of items: 3
Items: 
Size: 437817 Color: 2
Size: 254571 Color: 0
Size: 254458 Color: 2

Bin 1866: 53547 of cap free
Amount of items: 3
Items: 
Size: 437750 Color: 3
Size: 254430 Color: 4
Size: 254274 Color: 1

Bin 1867: 53941 of cap free
Amount of items: 3
Items: 
Size: 437664 Color: 2
Size: 254228 Color: 1
Size: 254168 Color: 4

Bin 1868: 54337 of cap free
Amount of items: 3
Items: 
Size: 437656 Color: 4
Size: 254091 Color: 3
Size: 253917 Color: 0

Bin 1869: 54702 of cap free
Amount of items: 3
Items: 
Size: 437608 Color: 1
Size: 253847 Color: 3
Size: 253844 Color: 2

Bin 1870: 54826 of cap free
Amount of items: 3
Items: 
Size: 437532 Color: 4
Size: 253899 Color: 1
Size: 253744 Color: 2

Bin 1871: 55425 of cap free
Amount of items: 3
Items: 
Size: 437498 Color: 0
Size: 253703 Color: 2
Size: 253375 Color: 0

Bin 1872: 56352 of cap free
Amount of items: 3
Items: 
Size: 437447 Color: 3
Size: 253186 Color: 0
Size: 253016 Color: 3

Bin 1873: 57275 of cap free
Amount of items: 3
Items: 
Size: 437426 Color: 2
Size: 252713 Color: 4
Size: 252587 Color: 3

Bin 1874: 57509 of cap free
Amount of items: 3
Items: 
Size: 437412 Color: 2
Size: 252551 Color: 3
Size: 252529 Color: 3

Bin 1875: 57522 of cap free
Amount of items: 3
Items: 
Size: 437393 Color: 1
Size: 252806 Color: 2
Size: 252280 Color: 4

Bin 1876: 58500 of cap free
Amount of items: 3
Items: 
Size: 437234 Color: 0
Size: 252164 Color: 1
Size: 252103 Color: 4

Bin 1877: 58745 of cap free
Amount of items: 3
Items: 
Size: 437123 Color: 0
Size: 252103 Color: 4
Size: 252030 Color: 3

Bin 1878: 59048 of cap free
Amount of items: 3
Items: 
Size: 437098 Color: 4
Size: 251933 Color: 1
Size: 251922 Color: 4

Bin 1879: 59681 of cap free
Amount of items: 3
Items: 
Size: 436740 Color: 0
Size: 251819 Color: 4
Size: 251761 Color: 3

Bin 1880: 59868 of cap free
Amount of items: 3
Items: 
Size: 436694 Color: 1
Size: 251736 Color: 3
Size: 251703 Color: 4

Bin 1881: 60154 of cap free
Amount of items: 3
Items: 
Size: 436686 Color: 4
Size: 251609 Color: 1
Size: 251552 Color: 4

Bin 1882: 60838 of cap free
Amount of items: 3
Items: 
Size: 436482 Color: 1
Size: 251475 Color: 3
Size: 251206 Color: 0

Bin 1883: 61297 of cap free
Amount of items: 3
Items: 
Size: 436399 Color: 1
Size: 251158 Color: 2
Size: 251147 Color: 3

Bin 1884: 61346 of cap free
Amount of items: 3
Items: 
Size: 436230 Color: 3
Size: 251481 Color: 1
Size: 250944 Color: 1

Bin 1885: 61913 of cap free
Amount of items: 3
Items: 
Size: 436363 Color: 1
Size: 250874 Color: 3
Size: 250851 Color: 1

Bin 1886: 62323 of cap free
Amount of items: 3
Items: 
Size: 436178 Color: 1
Size: 250846 Color: 3
Size: 250654 Color: 2

Bin 1887: 62888 of cap free
Amount of items: 3
Items: 
Size: 436176 Color: 1
Size: 250481 Color: 0
Size: 250456 Color: 4

Bin 1888: 63397 of cap free
Amount of items: 3
Items: 
Size: 435869 Color: 4
Size: 250400 Color: 3
Size: 250335 Color: 3

Bin 1889: 63626 of cap free
Amount of items: 3
Items: 
Size: 435847 Color: 1
Size: 250448 Color: 4
Size: 250080 Color: 0

Bin 1890: 64439 of cap free
Amount of items: 3
Items: 
Size: 435819 Color: 1
Size: 249941 Color: 3
Size: 249802 Color: 1

Bin 1891: 65080 of cap free
Amount of items: 3
Items: 
Size: 435803 Color: 3
Size: 249586 Color: 2
Size: 249532 Color: 3

Bin 1892: 65543 of cap free
Amount of items: 3
Items: 
Size: 435793 Color: 3
Size: 249344 Color: 1
Size: 249321 Color: 2

Bin 1893: 66331 of cap free
Amount of items: 3
Items: 
Size: 435727 Color: 3
Size: 249074 Color: 4
Size: 248869 Color: 3

Bin 1894: 66614 of cap free
Amount of items: 3
Items: 
Size: 435334 Color: 0
Size: 249411 Color: 3
Size: 248642 Color: 4

Bin 1895: 67536 of cap free
Amount of items: 3
Items: 
Size: 435324 Color: 3
Size: 248596 Color: 0
Size: 248545 Color: 0

Bin 1896: 68389 of cap free
Amount of items: 3
Items: 
Size: 435030 Color: 0
Size: 248387 Color: 4
Size: 248195 Color: 4

Bin 1897: 68637 of cap free
Amount of items: 3
Items: 
Size: 435272 Color: 4
Size: 248091 Color: 0
Size: 248001 Color: 0

Bin 1898: 69322 of cap free
Amount of items: 3
Items: 
Size: 434967 Color: 2
Size: 247900 Color: 0
Size: 247812 Color: 4

Bin 1899: 69556 of cap free
Amount of items: 3
Items: 
Size: 434935 Color: 3
Size: 247761 Color: 0
Size: 247749 Color: 2

Bin 1900: 70086 of cap free
Amount of items: 3
Items: 
Size: 434581 Color: 1
Size: 247679 Color: 2
Size: 247655 Color: 3

Bin 1901: 70150 of cap free
Amount of items: 3
Items: 
Size: 434580 Color: 2
Size: 247655 Color: 1
Size: 247616 Color: 2

Bin 1902: 70440 of cap free
Amount of items: 3
Items: 
Size: 434427 Color: 3
Size: 247610 Color: 1
Size: 247524 Color: 1

Bin 1903: 71234 of cap free
Amount of items: 3
Items: 
Size: 434511 Color: 1
Size: 247150 Color: 0
Size: 247106 Color: 4

Bin 1904: 71632 of cap free
Amount of items: 3
Items: 
Size: 434479 Color: 1
Size: 247065 Color: 4
Size: 246825 Color: 1

Bin 1905: 72142 of cap free
Amount of items: 3
Items: 
Size: 434471 Color: 1
Size: 246811 Color: 0
Size: 246577 Color: 3

Bin 1906: 72270 of cap free
Amount of items: 3
Items: 
Size: 434382 Color: 0
Size: 247283 Color: 1
Size: 246066 Color: 1

Bin 1907: 72848 of cap free
Amount of items: 3
Items: 
Size: 434397 Color: 1
Size: 246565 Color: 3
Size: 246191 Color: 2

Bin 1908: 73957 of cap free
Amount of items: 3
Items: 
Size: 434365 Color: 3
Size: 245880 Color: 0
Size: 245799 Color: 4

Bin 1909: 75396 of cap free
Amount of items: 3
Items: 
Size: 434314 Color: 2
Size: 245559 Color: 4
Size: 244732 Color: 1

Bin 1910: 75807 of cap free
Amount of items: 3
Items: 
Size: 434274 Color: 1
Size: 245566 Color: 2
Size: 244354 Color: 2

Bin 1911: 77199 of cap free
Amount of items: 3
Items: 
Size: 434295 Color: 2
Size: 244263 Color: 0
Size: 244244 Color: 1

Bin 1912: 77777 of cap free
Amount of items: 3
Items: 
Size: 434122 Color: 1
Size: 244075 Color: 0
Size: 244027 Color: 2

Bin 1913: 77973 of cap free
Amount of items: 3
Items: 
Size: 434086 Color: 0
Size: 243972 Color: 1
Size: 243970 Color: 2

Bin 1914: 78905 of cap free
Amount of items: 3
Items: 
Size: 433761 Color: 3
Size: 243738 Color: 1
Size: 243597 Color: 2

Bin 1915: 79348 of cap free
Amount of items: 3
Items: 
Size: 433751 Color: 3
Size: 243530 Color: 1
Size: 243372 Color: 0

Bin 1916: 79713 of cap free
Amount of items: 3
Items: 
Size: 433647 Color: 4
Size: 243324 Color: 1
Size: 243317 Color: 0

Bin 1917: 80015 of cap free
Amount of items: 3
Items: 
Size: 433510 Color: 1
Size: 243240 Color: 0
Size: 243236 Color: 1

Bin 1918: 80483 of cap free
Amount of items: 3
Items: 
Size: 433488 Color: 2
Size: 243098 Color: 1
Size: 242932 Color: 4

Bin 1919: 80911 of cap free
Amount of items: 3
Items: 
Size: 433380 Color: 1
Size: 242870 Color: 0
Size: 242840 Color: 4

Bin 1920: 81077 of cap free
Amount of items: 3
Items: 
Size: 433359 Color: 4
Size: 242784 Color: 1
Size: 242781 Color: 1

Bin 1921: 81303 of cap free
Amount of items: 3
Items: 
Size: 433357 Color: 0
Size: 242778 Color: 3
Size: 242563 Color: 0

Bin 1922: 81943 of cap free
Amount of items: 3
Items: 
Size: 433326 Color: 3
Size: 242493 Color: 0
Size: 242239 Color: 1

Bin 1923: 82109 of cap free
Amount of items: 3
Items: 
Size: 433183 Color: 0
Size: 242513 Color: 3
Size: 242196 Color: 1

Bin 1924: 82581 of cap free
Amount of items: 3
Items: 
Size: 433124 Color: 2
Size: 242190 Color: 0
Size: 242106 Color: 2

Bin 1925: 82948 of cap free
Amount of items: 3
Items: 
Size: 433117 Color: 2
Size: 242087 Color: 0
Size: 241849 Color: 1

Bin 1926: 84192 of cap free
Amount of items: 3
Items: 
Size: 433102 Color: 3
Size: 241390 Color: 0
Size: 241317 Color: 0

Bin 1927: 84724 of cap free
Amount of items: 3
Items: 
Size: 432898 Color: 3
Size: 241265 Color: 1
Size: 241114 Color: 4

Bin 1928: 85115 of cap free
Amount of items: 3
Items: 
Size: 432873 Color: 2
Size: 241067 Color: 4
Size: 240946 Color: 4

Bin 1929: 85456 of cap free
Amount of items: 3
Items: 
Size: 432839 Color: 1
Size: 240890 Color: 2
Size: 240816 Color: 0

Bin 1930: 85528 of cap free
Amount of items: 3
Items: 
Size: 432768 Color: 3
Size: 240945 Color: 1
Size: 240760 Color: 3

Bin 1931: 86930 of cap free
Amount of items: 3
Items: 
Size: 432549 Color: 3
Size: 240480 Color: 4
Size: 240042 Color: 0

Bin 1932: 87060 of cap free
Amount of items: 3
Items: 
Size: 432632 Color: 4
Size: 240253 Color: 0
Size: 240056 Color: 1

Bin 1933: 88386 of cap free
Amount of items: 3
Items: 
Size: 432512 Color: 0
Size: 239663 Color: 2
Size: 239440 Color: 2

Bin 1934: 89432 of cap free
Amount of items: 3
Items: 
Size: 432506 Color: 0
Size: 239144 Color: 2
Size: 238919 Color: 2

Bin 1935: 89573 of cap free
Amount of items: 3
Items: 
Size: 432393 Color: 2
Size: 239774 Color: 0
Size: 238261 Color: 3

Bin 1936: 90153 of cap free
Amount of items: 3
Items: 
Size: 432419 Color: 0
Size: 238843 Color: 1
Size: 238586 Color: 4

Bin 1937: 91539 of cap free
Amount of items: 3
Items: 
Size: 432052 Color: 4
Size: 238229 Color: 0
Size: 238181 Color: 1

Bin 1938: 91925 of cap free
Amount of items: 3
Items: 
Size: 432013 Color: 1
Size: 238037 Color: 2
Size: 238026 Color: 4

Bin 1939: 92243 of cap free
Amount of items: 3
Items: 
Size: 431822 Color: 2
Size: 237972 Color: 0
Size: 237964 Color: 0

Bin 1940: 92432 of cap free
Amount of items: 3
Items: 
Size: 431884 Color: 0
Size: 237868 Color: 4
Size: 237817 Color: 2

Bin 1941: 92866 of cap free
Amount of items: 3
Items: 
Size: 431877 Color: 0
Size: 237728 Color: 4
Size: 237530 Color: 2

Bin 1942: 93372 of cap free
Amount of items: 3
Items: 
Size: 431796 Color: 4
Size: 237453 Color: 3
Size: 237380 Color: 2

Bin 1943: 93671 of cap free
Amount of items: 3
Items: 
Size: 431773 Color: 0
Size: 237323 Color: 2
Size: 237234 Color: 2

Bin 1944: 94369 of cap free
Amount of items: 3
Items: 
Size: 431637 Color: 0
Size: 237112 Color: 2
Size: 236883 Color: 0

Bin 1945: 94761 of cap free
Amount of items: 3
Items: 
Size: 431635 Color: 4
Size: 236825 Color: 0
Size: 236780 Color: 1

Bin 1946: 94991 of cap free
Amount of items: 3
Items: 
Size: 431577 Color: 0
Size: 236731 Color: 3
Size: 236702 Color: 1

Bin 1947: 95137 of cap free
Amount of items: 3
Items: 
Size: 431538 Color: 3
Size: 236699 Color: 4
Size: 236627 Color: 2

Bin 1948: 95622 of cap free
Amount of items: 3
Items: 
Size: 431414 Color: 3
Size: 236506 Color: 2
Size: 236459 Color: 2

Bin 1949: 95761 of cap free
Amount of items: 3
Items: 
Size: 431384 Color: 0
Size: 236441 Color: 2
Size: 236415 Color: 4

Bin 1950: 97114 of cap free
Amount of items: 3
Items: 
Size: 431319 Color: 2
Size: 235883 Color: 0
Size: 235685 Color: 0

Bin 1951: 97260 of cap free
Amount of items: 3
Items: 
Size: 431248 Color: 0
Size: 236014 Color: 2
Size: 235479 Color: 4

Bin 1952: 98243 of cap free
Amount of items: 3
Items: 
Size: 431153 Color: 4
Size: 235325 Color: 3
Size: 235280 Color: 4

Bin 1953: 98881 of cap free
Amount of items: 3
Items: 
Size: 430920 Color: 4
Size: 235216 Color: 3
Size: 234984 Color: 2

Bin 1954: 100481 of cap free
Amount of items: 3
Items: 
Size: 429811 Color: 0
Size: 234869 Color: 3
Size: 234840 Color: 0

Bin 1955: 101768 of cap free
Amount of items: 3
Items: 
Size: 429773 Color: 0
Size: 234349 Color: 4
Size: 234111 Color: 1

Bin 1956: 102323 of cap free
Amount of items: 3
Items: 
Size: 429627 Color: 2
Size: 234085 Color: 4
Size: 233966 Color: 2

Bin 1957: 102739 of cap free
Amount of items: 3
Items: 
Size: 429613 Color: 1
Size: 233890 Color: 2
Size: 233759 Color: 3

Bin 1958: 103404 of cap free
Amount of items: 3
Items: 
Size: 429398 Color: 4
Size: 233703 Color: 0
Size: 233496 Color: 4

Bin 1959: 104731 of cap free
Amount of items: 3
Items: 
Size: 429385 Color: 0
Size: 233183 Color: 4
Size: 232702 Color: 1

Bin 1960: 105193 of cap free
Amount of items: 3
Items: 
Size: 429204 Color: 3
Size: 233340 Color: 0
Size: 232264 Color: 3

Bin 1961: 105745 of cap free
Amount of items: 3
Items: 
Size: 429336 Color: 0
Size: 232557 Color: 2
Size: 232363 Color: 0

Bin 1962: 106760 of cap free
Amount of items: 3
Items: 
Size: 429180 Color: 1
Size: 232135 Color: 2
Size: 231926 Color: 2

Bin 1963: 107079 of cap free
Amount of items: 3
Items: 
Size: 429124 Color: 0
Size: 231907 Color: 4
Size: 231891 Color: 0

Bin 1964: 107528 of cap free
Amount of items: 3
Items: 
Size: 429048 Color: 2
Size: 231748 Color: 3
Size: 231677 Color: 4

Bin 1965: 108095 of cap free
Amount of items: 3
Items: 
Size: 428993 Color: 4
Size: 231504 Color: 2
Size: 231409 Color: 4

Bin 1966: 108900 of cap free
Amount of items: 3
Items: 
Size: 428921 Color: 2
Size: 231252 Color: 3
Size: 230928 Color: 1

Bin 1967: 109742 of cap free
Amount of items: 3
Items: 
Size: 428789 Color: 1
Size: 230825 Color: 4
Size: 230645 Color: 4

Bin 1968: 110535 of cap free
Amount of items: 3
Items: 
Size: 428543 Color: 4
Size: 230495 Color: 3
Size: 230428 Color: 2

Bin 1969: 110763 of cap free
Amount of items: 3
Items: 
Size: 428454 Color: 2
Size: 230427 Color: 1
Size: 230357 Color: 0

Bin 1970: 111479 of cap free
Amount of items: 3
Items: 
Size: 428357 Color: 0
Size: 230145 Color: 1
Size: 230020 Color: 3

Bin 1971: 111604 of cap free
Amount of items: 3
Items: 
Size: 428163 Color: 3
Size: 230288 Color: 0
Size: 229946 Color: 2

Bin 1972: 113623 of cap free
Amount of items: 3
Items: 
Size: 427863 Color: 0
Size: 229360 Color: 2
Size: 229155 Color: 4

Bin 1973: 113840 of cap free
Amount of items: 3
Items: 
Size: 427817 Color: 3
Size: 229368 Color: 0
Size: 228976 Color: 0

Bin 1974: 115249 of cap free
Amount of items: 3
Items: 
Size: 427501 Color: 2
Size: 228707 Color: 0
Size: 228544 Color: 4

Bin 1975: 115986 of cap free
Amount of items: 3
Items: 
Size: 427477 Color: 0
Size: 228478 Color: 3
Size: 228060 Color: 3

Bin 1976: 117436 of cap free
Amount of items: 3
Items: 
Size: 427387 Color: 2
Size: 227637 Color: 3
Size: 227541 Color: 3

Bin 1977: 118392 of cap free
Amount of items: 3
Items: 
Size: 427284 Color: 3
Size: 227280 Color: 4
Size: 227045 Color: 3

Bin 1978: 119762 of cap free
Amount of items: 3
Items: 
Size: 426988 Color: 4
Size: 226770 Color: 2
Size: 226481 Color: 2

Bin 1979: 120573 of cap free
Amount of items: 3
Items: 
Size: 426791 Color: 2
Size: 226430 Color: 0
Size: 226207 Color: 2

Bin 1980: 121599 of cap free
Amount of items: 3
Items: 
Size: 426544 Color: 2
Size: 225980 Color: 3
Size: 225878 Color: 3

Bin 1981: 121863 of cap free
Amount of items: 3
Items: 
Size: 426771 Color: 3
Size: 225731 Color: 0
Size: 225636 Color: 1

Bin 1982: 122511 of cap free
Amount of items: 3
Items: 
Size: 426499 Color: 3
Size: 225623 Color: 2
Size: 225368 Color: 3

Bin 1983: 124177 of cap free
Amount of items: 3
Items: 
Size: 425911 Color: 3
Size: 224998 Color: 1
Size: 224915 Color: 3

Bin 1984: 124442 of cap free
Amount of items: 3
Items: 
Size: 425885 Color: 4
Size: 224867 Color: 0
Size: 224807 Color: 4

Bin 1985: 124867 of cap free
Amount of items: 3
Items: 
Size: 425793 Color: 2
Size: 224707 Color: 4
Size: 224634 Color: 0

Bin 1986: 125328 of cap free
Amount of items: 3
Items: 
Size: 425705 Color: 0
Size: 224501 Color: 3
Size: 224467 Color: 1

Bin 1987: 125351 of cap free
Amount of items: 3
Items: 
Size: 425682 Color: 1
Size: 224532 Color: 0
Size: 224436 Color: 1

Bin 1988: 125581 of cap free
Amount of items: 3
Items: 
Size: 425678 Color: 0
Size: 224424 Color: 3
Size: 224318 Color: 4

Bin 1989: 126226 of cap free
Amount of items: 3
Items: 
Size: 425655 Color: 0
Size: 224104 Color: 3
Size: 224016 Color: 0

Bin 1990: 126528 of cap free
Amount of items: 3
Items: 
Size: 425606 Color: 1
Size: 223953 Color: 2
Size: 223914 Color: 1

Bin 1991: 126875 of cap free
Amount of items: 3
Items: 
Size: 425506 Color: 3
Size: 223814 Color: 4
Size: 223806 Color: 4

Bin 1992: 128168 of cap free
Amount of items: 3
Items: 
Size: 425328 Color: 0
Size: 223348 Color: 1
Size: 223157 Color: 1

Bin 1993: 128886 of cap free
Amount of items: 3
Items: 
Size: 425317 Color: 4
Size: 222938 Color: 3
Size: 222860 Color: 2

Bin 1994: 129167 of cap free
Amount of items: 3
Items: 
Size: 425223 Color: 4
Size: 222835 Color: 1
Size: 222776 Color: 3

Bin 1995: 129813 of cap free
Amount of items: 3
Items: 
Size: 425129 Color: 0
Size: 222537 Color: 4
Size: 222522 Color: 3

Bin 1996: 130468 of cap free
Amount of items: 3
Items: 
Size: 425087 Color: 4
Size: 222235 Color: 3
Size: 222211 Color: 3

Bin 1997: 130482 of cap free
Amount of items: 3
Items: 
Size: 424813 Color: 2
Size: 222520 Color: 4
Size: 222186 Color: 3

Bin 1998: 131263 of cap free
Amount of items: 3
Items: 
Size: 424714 Color: 1
Size: 222119 Color: 4
Size: 221905 Color: 4

Bin 1999: 131903 of cap free
Amount of items: 3
Items: 
Size: 424680 Color: 1
Size: 221785 Color: 2
Size: 221633 Color: 1

Bin 2000: 132599 of cap free
Amount of items: 3
Items: 
Size: 424604 Color: 1
Size: 221552 Color: 3
Size: 221246 Color: 1

Bin 2001: 132945 of cap free
Amount of items: 3
Items: 
Size: 424269 Color: 0
Size: 221873 Color: 1
Size: 220914 Color: 0

Bin 2002: 133196 of cap free
Amount of items: 3
Items: 
Size: 424483 Color: 1
Size: 221232 Color: 0
Size: 221090 Color: 0

Bin 2003: 133618 of cap free
Amount of items: 3
Items: 
Size: 424321 Color: 1
Size: 221061 Color: 2
Size: 221001 Color: 1

Bin 2004: 134004 of cap free
Amount of items: 3
Items: 
Size: 424267 Color: 0
Size: 220899 Color: 2
Size: 220831 Color: 1

Bin 2005: 134881 of cap free
Amount of items: 3
Items: 
Size: 424189 Color: 4
Size: 220614 Color: 0
Size: 220317 Color: 4

Bin 2006: 135410 of cap free
Amount of items: 3
Items: 
Size: 424040 Color: 1
Size: 220276 Color: 4
Size: 220275 Color: 4

Bin 2007: 135658 of cap free
Amount of items: 3
Items: 
Size: 423877 Color: 1
Size: 220250 Color: 2
Size: 220216 Color: 3

Bin 2008: 136394 of cap free
Amount of items: 3
Items: 
Size: 423507 Color: 3
Size: 220157 Color: 1
Size: 219943 Color: 1

Bin 2009: 136446 of cap free
Amount of items: 3
Items: 
Size: 423761 Color: 1
Size: 219930 Color: 2
Size: 219864 Color: 3

Bin 2010: 137406 of cap free
Amount of items: 3
Items: 
Size: 423285 Color: 4
Size: 219678 Color: 1
Size: 219632 Color: 3

Bin 2011: 138636 of cap free
Amount of items: 3
Items: 
Size: 423211 Color: 0
Size: 219198 Color: 4
Size: 218956 Color: 4

Bin 2012: 138773 of cap free
Amount of items: 3
Items: 
Size: 423177 Color: 3
Size: 219247 Color: 0
Size: 218804 Color: 3

Bin 2013: 139742 of cap free
Amount of items: 3
Items: 
Size: 422867 Color: 3
Size: 218710 Color: 4
Size: 218682 Color: 4

Bin 2014: 140383 of cap free
Amount of items: 3
Items: 
Size: 422731 Color: 2
Size: 218585 Color: 3
Size: 218302 Color: 1

Bin 2015: 141447 of cap free
Amount of items: 3
Items: 
Size: 422653 Color: 2
Size: 218104 Color: 4
Size: 217797 Color: 3

Bin 2016: 142055 of cap free
Amount of items: 3
Items: 
Size: 422587 Color: 3
Size: 217779 Color: 1
Size: 217580 Color: 1

Bin 2017: 142099 of cap free
Amount of items: 3
Items: 
Size: 422623 Color: 1
Size: 217663 Color: 3
Size: 217616 Color: 2

Bin 2018: 143051 of cap free
Amount of items: 3
Items: 
Size: 422480 Color: 4
Size: 217411 Color: 3
Size: 217059 Color: 3

Bin 2019: 143310 of cap free
Amount of items: 3
Items: 
Size: 422488 Color: 3
Size: 217107 Color: 4
Size: 217096 Color: 3

Bin 2020: 143587 of cap free
Amount of items: 3
Items: 
Size: 422442 Color: 4
Size: 216990 Color: 0
Size: 216982 Color: 4

Bin 2021: 143868 of cap free
Amount of items: 3
Items: 
Size: 422439 Color: 4
Size: 216940 Color: 0
Size: 216754 Color: 3

Bin 2022: 144891 of cap free
Amount of items: 3
Items: 
Size: 422203 Color: 3
Size: 216462 Color: 2
Size: 216445 Color: 1

Bin 2023: 145378 of cap free
Amount of items: 3
Items: 
Size: 422181 Color: 1
Size: 216265 Color: 0
Size: 216177 Color: 0

Bin 2024: 146701 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 0
Size: 215578 Color: 1
Size: 215525 Color: 4

Bin 2025: 146780 of cap free
Amount of items: 3
Items: 
Size: 421742 Color: 4
Size: 215797 Color: 0
Size: 215682 Color: 0

Bin 2026: 147193 of cap free
Amount of items: 3
Items: 
Size: 421552 Color: 1
Size: 215659 Color: 0
Size: 215597 Color: 0

Bin 2027: 148352 of cap free
Amount of items: 3
Items: 
Size: 421599 Color: 0
Size: 215040 Color: 1
Size: 215010 Color: 4

Bin 2028: 148580 of cap free
Amount of items: 3
Items: 
Size: 421397 Color: 1
Size: 215279 Color: 0
Size: 214745 Color: 4

Bin 2029: 148819 of cap free
Amount of items: 3
Items: 
Size: 421549 Color: 0
Size: 214863 Color: 3
Size: 214770 Color: 0

Bin 2030: 149736 of cap free
Amount of items: 3
Items: 
Size: 420999 Color: 1
Size: 214641 Color: 4
Size: 214625 Color: 4

Bin 2031: 149853 of cap free
Amount of items: 3
Items: 
Size: 421249 Color: 4
Size: 214536 Color: 0
Size: 214363 Color: 2

Bin 2032: 150763 of cap free
Amount of items: 3
Items: 
Size: 420992 Color: 4
Size: 214126 Color: 3
Size: 214120 Color: 2

Bin 2033: 150955 of cap free
Amount of items: 3
Items: 
Size: 420948 Color: 0
Size: 214058 Color: 3
Size: 214040 Color: 3

Bin 2034: 151577 of cap free
Amount of items: 3
Items: 
Size: 420820 Color: 1
Size: 213881 Color: 2
Size: 213723 Color: 2

Bin 2035: 152382 of cap free
Amount of items: 3
Items: 
Size: 420403 Color: 4
Size: 213690 Color: 1
Size: 213526 Color: 1

Bin 2036: 152795 of cap free
Amount of items: 3
Items: 
Size: 420178 Color: 1
Size: 213524 Color: 0
Size: 213504 Color: 0

Bin 2037: 153046 of cap free
Amount of items: 3
Items: 
Size: 420044 Color: 1
Size: 213473 Color: 2
Size: 213438 Color: 0

Bin 2038: 154086 of cap free
Amount of items: 3
Items: 
Size: 419982 Color: 1
Size: 212979 Color: 2
Size: 212954 Color: 1

Bin 2039: 154926 of cap free
Amount of items: 3
Items: 
Size: 419403 Color: 1
Size: 212906 Color: 2
Size: 212766 Color: 0

Bin 2040: 155509 of cap free
Amount of items: 3
Items: 
Size: 419181 Color: 1
Size: 212685 Color: 2
Size: 212626 Color: 0

Bin 2041: 155990 of cap free
Amount of items: 3
Items: 
Size: 418450 Color: 4
Size: 213487 Color: 1
Size: 212074 Color: 1

Bin 2042: 156463 of cap free
Amount of items: 3
Items: 
Size: 418530 Color: 1
Size: 212588 Color: 0
Size: 212420 Color: 2

Bin 2043: 156915 of cap free
Amount of items: 3
Items: 
Size: 418498 Color: 1
Size: 212337 Color: 0
Size: 212251 Color: 3

Bin 2044: 158199 of cap free
Amount of items: 3
Items: 
Size: 418284 Color: 1
Size: 211764 Color: 3
Size: 211754 Color: 4

Bin 2045: 158709 of cap free
Amount of items: 3
Items: 
Size: 418177 Color: 2
Size: 211599 Color: 4
Size: 211516 Color: 3

Bin 2046: 158877 of cap free
Amount of items: 3
Items: 
Size: 418141 Color: 4
Size: 211495 Color: 1
Size: 211488 Color: 1

Bin 2047: 159466 of cap free
Amount of items: 3
Items: 
Size: 418026 Color: 3
Size: 211346 Color: 2
Size: 211163 Color: 3

Bin 2048: 160045 of cap free
Amount of items: 3
Items: 
Size: 417965 Color: 4
Size: 210998 Color: 2
Size: 210993 Color: 2

Bin 2049: 160558 of cap free
Amount of items: 3
Items: 
Size: 417864 Color: 2
Size: 210792 Color: 3
Size: 210787 Color: 2

Bin 2050: 161202 of cap free
Amount of items: 3
Items: 
Size: 417837 Color: 2
Size: 210513 Color: 0
Size: 210449 Color: 4

Bin 2051: 161841 of cap free
Amount of items: 3
Items: 
Size: 417633 Color: 3
Size: 210411 Color: 4
Size: 210116 Color: 4

Bin 2052: 161864 of cap free
Amount of items: 3
Items: 
Size: 417711 Color: 4
Size: 210243 Color: 3
Size: 210183 Color: 2

Bin 2053: 162786 of cap free
Amount of items: 3
Items: 
Size: 417449 Color: 0
Size: 209915 Color: 3
Size: 209851 Color: 1

Bin 2054: 164288 of cap free
Amount of items: 3
Items: 
Size: 417196 Color: 0
Size: 209270 Color: 1
Size: 209247 Color: 2

Bin 2055: 164360 of cap free
Amount of items: 3
Items: 
Size: 417169 Color: 0
Size: 209239 Color: 1
Size: 209233 Color: 2

Bin 2056: 164567 of cap free
Amount of items: 3
Items: 
Size: 417143 Color: 3
Size: 209184 Color: 4
Size: 209107 Color: 1

Bin 2057: 164740 of cap free
Amount of items: 3
Items: 
Size: 417090 Color: 1
Size: 209195 Color: 3
Size: 208976 Color: 3

Bin 2058: 165262 of cap free
Amount of items: 3
Items: 
Size: 417056 Color: 0
Size: 208972 Color: 4
Size: 208711 Color: 0

Bin 2059: 165611 of cap free
Amount of items: 3
Items: 
Size: 417040 Color: 4
Size: 208707 Color: 0
Size: 208643 Color: 4

Bin 2060: 166345 of cap free
Amount of items: 3
Items: 
Size: 416992 Color: 2
Size: 208337 Color: 4
Size: 208327 Color: 2

Bin 2061: 167094 of cap free
Amount of items: 2
Items: 
Size: 416523 Color: 3
Size: 416384 Color: 4

Bin 2062: 167329 of cap free
Amount of items: 3
Items: 
Size: 416376 Color: 3
Size: 208149 Color: 0
Size: 208147 Color: 4

Bin 2063: 167519 of cap free
Amount of items: 2
Items: 
Size: 416283 Color: 0
Size: 416199 Color: 4

Bin 2064: 167845 of cap free
Amount of items: 2
Items: 
Size: 416134 Color: 2
Size: 416022 Color: 1

Bin 2065: 168043 of cap free
Amount of items: 2
Items: 
Size: 415997 Color: 3
Size: 415961 Color: 1

Bin 2066: 168102 of cap free
Amount of items: 3
Items: 
Size: 415975 Color: 3
Size: 208084 Color: 0
Size: 207840 Color: 4

Bin 2067: 169056 of cap free
Amount of items: 3
Items: 
Size: 415457 Color: 1
Size: 207802 Color: 3
Size: 207686 Color: 2

Bin 2068: 169350 of cap free
Amount of items: 2
Items: 
Size: 415327 Color: 2
Size: 415324 Color: 4

Bin 2069: 169784 of cap free
Amount of items: 2
Items: 
Size: 415179 Color: 4
Size: 415038 Color: 0

Bin 2070: 170460 of cap free
Amount of items: 2
Items: 
Size: 414869 Color: 2
Size: 414672 Color: 0

Bin 2071: 170499 of cap free
Amount of items: 3
Items: 
Size: 414475 Color: 3
Size: 207647 Color: 2
Size: 207380 Color: 2

Bin 2072: 170905 of cap free
Amount of items: 3
Items: 
Size: 414506 Color: 2
Size: 207327 Color: 1
Size: 207263 Color: 3

Bin 2073: 171336 of cap free
Amount of items: 3
Items: 
Size: 414237 Color: 3
Size: 207256 Color: 0
Size: 207172 Color: 3

Bin 2074: 171513 of cap free
Amount of items: 3
Items: 
Size: 414233 Color: 1
Size: 207137 Color: 3
Size: 207118 Color: 3

Bin 2075: 171682 of cap free
Amount of items: 3
Items: 
Size: 414195 Color: 0
Size: 207095 Color: 3
Size: 207029 Color: 4

Bin 2076: 171819 of cap free
Amount of items: 2
Items: 
Size: 414101 Color: 1
Size: 414081 Color: 3

Bin 2077: 171991 of cap free
Amount of items: 3
Items: 
Size: 414007 Color: 2
Size: 207019 Color: 3
Size: 206984 Color: 3

Bin 2078: 172149 of cap free
Amount of items: 2
Items: 
Size: 413950 Color: 2
Size: 413902 Color: 1

Bin 2079: 172284 of cap free
Amount of items: 3
Items: 
Size: 413827 Color: 1
Size: 206961 Color: 3
Size: 206929 Color: 3

Bin 2080: 172712 of cap free
Amount of items: 2
Items: 
Size: 413657 Color: 0
Size: 413632 Color: 3

Bin 2081: 172794 of cap free
Amount of items: 2
Items: 
Size: 413616 Color: 2
Size: 413591 Color: 3

Bin 2082: 172959 of cap free
Amount of items: 3
Items: 
Size: 413492 Color: 4
Size: 206801 Color: 2
Size: 206749 Color: 2

Bin 2083: 173546 of cap free
Amount of items: 2
Items: 
Size: 413256 Color: 2
Size: 413199 Color: 4

Bin 2084: 173619 of cap free
Amount of items: 2
Items: 
Size: 413208 Color: 2
Size: 413174 Color: 3

Bin 2085: 173710 of cap free
Amount of items: 2
Items: 
Size: 413164 Color: 2
Size: 413127 Color: 0

Bin 2086: 173878 of cap free
Amount of items: 2
Items: 
Size: 413099 Color: 1
Size: 413024 Color: 4

Bin 2087: 174168 of cap free
Amount of items: 2
Items: 
Size: 412968 Color: 0
Size: 412865 Color: 3

Bin 2088: 174543 of cap free
Amount of items: 2
Items: 
Size: 412747 Color: 2
Size: 412711 Color: 0

Bin 2089: 174596 of cap free
Amount of items: 3
Items: 
Size: 412620 Color: 0
Size: 206494 Color: 2
Size: 206291 Color: 3

Bin 2090: 175561 of cap free
Amount of items: 2
Items: 
Size: 412253 Color: 0
Size: 412187 Color: 3

Bin 2091: 175730 of cap free
Amount of items: 2
Items: 
Size: 412161 Color: 0
Size: 412110 Color: 2

Bin 2092: 175796 of cap free
Amount of items: 2
Items: 
Size: 412106 Color: 4
Size: 412099 Color: 2

Bin 2093: 176084 of cap free
Amount of items: 2
Items: 
Size: 411993 Color: 1
Size: 411924 Color: 0

Bin 2094: 176228 of cap free
Amount of items: 3
Items: 
Size: 411888 Color: 2
Size: 205982 Color: 3
Size: 205903 Color: 3

Bin 2095: 176622 of cap free
Amount of items: 3
Items: 
Size: 411831 Color: 0
Size: 205786 Color: 3
Size: 205762 Color: 4

Bin 2096: 177522 of cap free
Amount of items: 3
Items: 
Size: 411374 Color: 4
Size: 205748 Color: 1
Size: 205357 Color: 2

Bin 2097: 177831 of cap free
Amount of items: 2
Items: 
Size: 411296 Color: 4
Size: 410874 Color: 0

Bin 2098: 178353 of cap free
Amount of items: 2
Items: 
Size: 410863 Color: 1
Size: 410785 Color: 4

Bin 2099: 178494 of cap free
Amount of items: 2
Items: 
Size: 410777 Color: 0
Size: 410730 Color: 1

Bin 2100: 178668 of cap free
Amount of items: 2
Items: 
Size: 410722 Color: 3
Size: 410611 Color: 4

Bin 2101: 179009 of cap free
Amount of items: 3
Items: 
Size: 410388 Color: 4
Size: 205310 Color: 0
Size: 205294 Color: 4

Bin 2102: 179275 of cap free
Amount of items: 3
Items: 
Size: 410363 Color: 0
Size: 205249 Color: 3
Size: 205114 Color: 4

Bin 2103: 179309 of cap free
Amount of items: 2
Items: 
Size: 410361 Color: 0
Size: 410331 Color: 4

Bin 2104: 179368 of cap free
Amount of items: 2
Items: 
Size: 410320 Color: 4
Size: 410313 Color: 1

Bin 2105: 179670 of cap free
Amount of items: 2
Items: 
Size: 410223 Color: 1
Size: 410108 Color: 0

Bin 2106: 180790 of cap free
Amount of items: 2
Items: 
Size: 409655 Color: 0
Size: 409556 Color: 2

Bin 2107: 181200 of cap free
Amount of items: 3
Items: 
Size: 409258 Color: 1
Size: 204780 Color: 0
Size: 204763 Color: 4

Bin 2108: 181346 of cap free
Amount of items: 3
Items: 
Size: 409141 Color: 1
Size: 204759 Color: 0
Size: 204755 Color: 3

Bin 2109: 181998 of cap free
Amount of items: 3
Items: 
Size: 408974 Color: 3
Size: 204569 Color: 4
Size: 204460 Color: 0

Bin 2110: 182253 of cap free
Amount of items: 2
Items: 
Size: 408911 Color: 4
Size: 408837 Color: 1

Bin 2111: 182483 of cap free
Amount of items: 2
Items: 
Size: 408786 Color: 1
Size: 408732 Color: 4

Bin 2112: 182792 of cap free
Amount of items: 2
Items: 
Size: 408717 Color: 1
Size: 408492 Color: 4

Bin 2113: 182840 of cap free
Amount of items: 2
Items: 
Size: 408711 Color: 1
Size: 408450 Color: 2

Bin 2114: 183070 of cap free
Amount of items: 2
Items: 
Size: 408659 Color: 1
Size: 408272 Color: 2

Bin 2115: 183642 of cap free
Amount of items: 2
Items: 
Size: 408187 Color: 3
Size: 408172 Color: 2

Bin 2116: 183731 of cap free
Amount of items: 2
Items: 
Size: 408150 Color: 3
Size: 408120 Color: 4

Bin 2117: 183909 of cap free
Amount of items: 2
Items: 
Size: 408098 Color: 1
Size: 407994 Color: 2

Bin 2118: 184241 of cap free
Amount of items: 2
Items: 
Size: 407984 Color: 1
Size: 407776 Color: 0

Bin 2119: 184490 of cap free
Amount of items: 2
Items: 
Size: 407803 Color: 1
Size: 407708 Color: 4

Bin 2120: 185753 of cap free
Amount of items: 3
Items: 
Size: 407122 Color: 1
Size: 203909 Color: 0
Size: 203217 Color: 4

Bin 2121: 186075 of cap free
Amount of items: 2
Items: 
Size: 406977 Color: 2
Size: 406949 Color: 3

Bin 2122: 186246 of cap free
Amount of items: 2
Items: 
Size: 406931 Color: 2
Size: 406824 Color: 3

Bin 2123: 186339 of cap free
Amount of items: 2
Items: 
Size: 406887 Color: 2
Size: 406775 Color: 4

Bin 2124: 188139 of cap free
Amount of items: 2
Items: 
Size: 406250 Color: 0
Size: 405612 Color: 4

Bin 2125: 188274 of cap free
Amount of items: 2
Items: 
Size: 406204 Color: 0
Size: 405523 Color: 3

Bin 2126: 188715 of cap free
Amount of items: 2
Items: 
Size: 405868 Color: 0
Size: 405418 Color: 4

Bin 2127: 188775 of cap free
Amount of items: 2
Items: 
Size: 405852 Color: 0
Size: 405374 Color: 4

Bin 2128: 189212 of cap free
Amount of items: 3
Items: 
Size: 404741 Color: 4
Size: 203026 Color: 0
Size: 203022 Color: 0

Bin 2129: 190424 of cap free
Amount of items: 3
Items: 
Size: 404726 Color: 4
Size: 202451 Color: 2
Size: 202400 Color: 2

Bin 2130: 190602 of cap free
Amount of items: 3
Items: 
Size: 404632 Color: 3
Size: 202393 Color: 4
Size: 202374 Color: 3

Bin 2131: 190914 of cap free
Amount of items: 2
Items: 
Size: 404557 Color: 2
Size: 404530 Color: 0

Bin 2132: 191210 of cap free
Amount of items: 2
Items: 
Size: 404417 Color: 2
Size: 404374 Color: 3

Bin 2133: 191339 of cap free
Amount of items: 3
Items: 
Size: 404315 Color: 4
Size: 202330 Color: 0
Size: 202017 Color: 0

Bin 2134: 191702 of cap free
Amount of items: 2
Items: 
Size: 404190 Color: 2
Size: 404109 Color: 3

Bin 2135: 191884 of cap free
Amount of items: 2
Items: 
Size: 404142 Color: 2
Size: 403975 Color: 4

Bin 2136: 191898 of cap free
Amount of items: 2
Items: 
Size: 404134 Color: 2
Size: 403969 Color: 1

Bin 2137: 192637 of cap free
Amount of items: 2
Items: 
Size: 403703 Color: 3
Size: 403661 Color: 1

Bin 2138: 192778 of cap free
Amount of items: 2
Items: 
Size: 403635 Color: 4
Size: 403588 Color: 1

Bin 2139: 193053 of cap free
Amount of items: 3
Items: 
Size: 403435 Color: 1
Size: 201998 Color: 0
Size: 201515 Color: 2

Bin 2140: 193180 of cap free
Amount of items: 2
Items: 
Size: 403422 Color: 1
Size: 403399 Color: 0

Bin 2141: 193702 of cap free
Amount of items: 2
Items: 
Size: 403195 Color: 0
Size: 403104 Color: 2

Bin 2142: 193835 of cap free
Amount of items: 2
Items: 
Size: 403096 Color: 4
Size: 403070 Color: 1

Bin 2143: 194424 of cap free
Amount of items: 2
Items: 
Size: 402913 Color: 1
Size: 402664 Color: 4

Bin 2144: 194712 of cap free
Amount of items: 2
Items: 
Size: 402785 Color: 1
Size: 402504 Color: 0

Bin 2145: 195247 of cap free
Amount of items: 2
Items: 
Size: 402435 Color: 3
Size: 402319 Color: 2

Bin 2146: 195293 of cap free
Amount of items: 2
Items: 
Size: 402397 Color: 3
Size: 402311 Color: 0

Bin 2147: 195416 of cap free
Amount of items: 2
Items: 
Size: 402296 Color: 3
Size: 402289 Color: 1

Bin 2148: 195439 of cap free
Amount of items: 2
Items: 
Size: 402282 Color: 1
Size: 402280 Color: 3

Bin 2149: 195506 of cap free
Amount of items: 2
Items: 
Size: 402257 Color: 4
Size: 402238 Color: 3

Bin 2150: 195565 of cap free
Amount of items: 2
Items: 
Size: 402239 Color: 4
Size: 402197 Color: 2

Bin 2151: 195622 of cap free
Amount of items: 2
Items: 
Size: 402220 Color: 4
Size: 402159 Color: 2

Bin 2152: 195791 of cap free
Amount of items: 2
Items: 
Size: 402105 Color: 3
Size: 402105 Color: 1

Bin 2153: 196288 of cap free
Amount of items: 2
Items: 
Size: 401875 Color: 2
Size: 401838 Color: 3

Bin 2154: 196816 of cap free
Amount of items: 2
Items: 
Size: 401624 Color: 4
Size: 401561 Color: 3

Bin 2155: 197196 of cap free
Amount of items: 2
Items: 
Size: 401419 Color: 2
Size: 401386 Color: 3

Bin 2156: 197476 of cap free
Amount of items: 2
Items: 
Size: 401272 Color: 4
Size: 401253 Color: 0

Bin 2157: 197886 of cap free
Amount of items: 2
Items: 
Size: 401234 Color: 1
Size: 400881 Color: 0

Bin 2158: 198260 of cap free
Amount of items: 2
Items: 
Size: 400892 Color: 1
Size: 400849 Color: 3

Bin 2159: 198421 of cap free
Amount of items: 2
Items: 
Size: 400793 Color: 4
Size: 400787 Color: 3

Bin 2160: 198548 of cap free
Amount of items: 2
Items: 
Size: 400774 Color: 0
Size: 400679 Color: 2

Bin 2161: 198913 of cap free
Amount of items: 2
Items: 
Size: 400591 Color: 3
Size: 400497 Color: 4

Bin 2162: 199273 of cap free
Amount of items: 2
Items: 
Size: 400430 Color: 3
Size: 400298 Color: 1

Bin 2163: 199595 of cap free
Amount of items: 2
Items: 
Size: 400213 Color: 2
Size: 400193 Color: 3

Bin 2164: 199768 of cap free
Amount of items: 2
Items: 
Size: 400144 Color: 0
Size: 400089 Color: 4

Bin 2165: 200010 of cap free
Amount of items: 1
Items: 
Size: 799991 Color: 0

Bin 2166: 200033 of cap free
Amount of items: 1
Items: 
Size: 799968 Color: 3

Bin 2167: 200054 of cap free
Amount of items: 1
Items: 
Size: 799947 Color: 3

Bin 2168: 200056 of cap free
Amount of items: 1
Items: 
Size: 799945 Color: 1

Bin 2169: 200069 of cap free
Amount of items: 1
Items: 
Size: 799932 Color: 0

Bin 2170: 200189 of cap free
Amount of items: 2
Items: 
Size: 399984 Color: 4
Size: 399828 Color: 1

Bin 2171: 200370 of cap free
Amount of items: 1
Items: 
Size: 799631 Color: 2

Bin 2172: 200433 of cap free
Amount of items: 1
Items: 
Size: 799568 Color: 0

Bin 2173: 200437 of cap free
Amount of items: 1
Items: 
Size: 799564 Color: 1

Bin 2174: 200464 of cap free
Amount of items: 1
Items: 
Size: 799537 Color: 4

Bin 2175: 200487 of cap free
Amount of items: 1
Items: 
Size: 799514 Color: 0

Bin 2176: 200521 of cap free
Amount of items: 1
Items: 
Size: 799480 Color: 1

Bin 2177: 200562 of cap free
Amount of items: 1
Items: 
Size: 799439 Color: 0

Bin 2178: 200629 of cap free
Amount of items: 2
Items: 
Size: 399742 Color: 2
Size: 399630 Color: 1

Bin 2179: 200650 of cap free
Amount of items: 2
Items: 
Size: 399726 Color: 2
Size: 399625 Color: 0

Bin 2180: 200674 of cap free
Amount of items: 1
Items: 
Size: 799327 Color: 0

Bin 2181: 200694 of cap free
Amount of items: 1
Items: 
Size: 799307 Color: 4

Bin 2182: 200838 of cap free
Amount of items: 1
Items: 
Size: 799163 Color: 2

Bin 2183: 200871 of cap free
Amount of items: 2
Items: 
Size: 399589 Color: 3
Size: 399541 Color: 4

Bin 2184: 200941 of cap free
Amount of items: 2
Items: 
Size: 399548 Color: 3
Size: 399512 Color: 2

Bin 2185: 200989 of cap free
Amount of items: 1
Items: 
Size: 799012 Color: 4

Bin 2186: 201042 of cap free
Amount of items: 1
Items: 
Size: 798959 Color: 2

Bin 2187: 201249 of cap free
Amount of items: 2
Items: 
Size: 399402 Color: 1
Size: 399350 Color: 3

Bin 2188: 201296 of cap free
Amount of items: 1
Items: 
Size: 798705 Color: 3

Bin 2189: 201309 of cap free
Amount of items: 1
Items: 
Size: 798692 Color: 3

Bin 2190: 201475 of cap free
Amount of items: 2
Items: 
Size: 399288 Color: 0
Size: 399238 Color: 3

Bin 2191: 201557 of cap free
Amount of items: 1
Items: 
Size: 798444 Color: 2

Bin 2192: 201608 of cap free
Amount of items: 1
Items: 
Size: 798393 Color: 2

Bin 2193: 201610 of cap free
Amount of items: 1
Items: 
Size: 798391 Color: 0

Bin 2194: 201637 of cap free
Amount of items: 1
Items: 
Size: 798364 Color: 1

Bin 2195: 201683 of cap free
Amount of items: 1
Items: 
Size: 798318 Color: 1

Bin 2196: 201719 of cap free
Amount of items: 1
Items: 
Size: 798282 Color: 3

Bin 2197: 201749 of cap free
Amount of items: 1
Items: 
Size: 798252 Color: 2

Bin 2198: 201754 of cap free
Amount of items: 1
Items: 
Size: 798247 Color: 3

Bin 2199: 201910 of cap free
Amount of items: 2
Items: 
Size: 399089 Color: 1
Size: 399002 Color: 3

Bin 2200: 201921 of cap free
Amount of items: 1
Items: 
Size: 798080 Color: 2

Bin 2201: 201925 of cap free
Amount of items: 1
Items: 
Size: 798076 Color: 0

Bin 2202: 201948 of cap free
Amount of items: 1
Items: 
Size: 798053 Color: 3

Bin 2203: 202037 of cap free
Amount of items: 1
Items: 
Size: 797964 Color: 2

Bin 2204: 202081 of cap free
Amount of items: 1
Items: 
Size: 797920 Color: 2

Bin 2205: 202140 of cap free
Amount of items: 1
Items: 
Size: 797861 Color: 1

Bin 2206: 202170 of cap free
Amount of items: 1
Items: 
Size: 797831 Color: 2

Bin 2207: 202230 of cap free
Amount of items: 1
Items: 
Size: 797771 Color: 0

Bin 2208: 202371 of cap free
Amount of items: 1
Items: 
Size: 797630 Color: 0

Bin 2209: 202382 of cap free
Amount of items: 1
Items: 
Size: 797619 Color: 0

Bin 2210: 202405 of cap free
Amount of items: 1
Items: 
Size: 797596 Color: 1

Bin 2211: 202440 of cap free
Amount of items: 1
Items: 
Size: 797561 Color: 3

Bin 2212: 202503 of cap free
Amount of items: 1
Items: 
Size: 797498 Color: 3

Bin 2213: 202519 of cap free
Amount of items: 1
Items: 
Size: 797482 Color: 3

Bin 2214: 202530 of cap free
Amount of items: 1
Items: 
Size: 797471 Color: 4

Bin 2215: 202533 of cap free
Amount of items: 1
Items: 
Size: 797468 Color: 3

Bin 2216: 202571 of cap free
Amount of items: 2
Items: 
Size: 398899 Color: 4
Size: 398531 Color: 2

Bin 2217: 202633 of cap free
Amount of items: 1
Items: 
Size: 797368 Color: 1

Bin 2218: 202653 of cap free
Amount of items: 1
Items: 
Size: 797348 Color: 1

Bin 2219: 202786 of cap free
Amount of items: 1
Items: 
Size: 797215 Color: 0

Bin 2220: 202846 of cap free
Amount of items: 1
Items: 
Size: 797155 Color: 1

Bin 2221: 202967 of cap free
Amount of items: 2
Items: 
Size: 398520 Color: 1
Size: 398514 Color: 4

Bin 2222: 203048 of cap free
Amount of items: 1
Items: 
Size: 796953 Color: 2

Bin 2223: 203052 of cap free
Amount of items: 1
Items: 
Size: 796949 Color: 0

Bin 2224: 203117 of cap free
Amount of items: 1
Items: 
Size: 796884 Color: 4

Bin 2225: 203171 of cap free
Amount of items: 2
Items: 
Size: 398475 Color: 4
Size: 398355 Color: 0

Bin 2226: 203321 of cap free
Amount of items: 1
Items: 
Size: 796680 Color: 4

Bin 2227: 203423 of cap free
Amount of items: 2
Items: 
Size: 398290 Color: 1
Size: 398288 Color: 3

Bin 2228: 203553 of cap free
Amount of items: 1
Items: 
Size: 796448 Color: 4

Bin 2229: 203587 of cap free
Amount of items: 1
Items: 
Size: 796414 Color: 0

Bin 2230: 203708 of cap free
Amount of items: 1
Items: 
Size: 796293 Color: 1

Bin 2231: 203806 of cap free
Amount of items: 2
Items: 
Size: 398222 Color: 2
Size: 397973 Color: 3

Bin 2232: 203881 of cap free
Amount of items: 1
Items: 
Size: 796120 Color: 1

Bin 2233: 203905 of cap free
Amount of items: 1
Items: 
Size: 796096 Color: 0

Bin 2234: 203950 of cap free
Amount of items: 1
Items: 
Size: 796051 Color: 4

Bin 2235: 203995 of cap free
Amount of items: 1
Items: 
Size: 796006 Color: 4

Bin 2236: 204158 of cap free
Amount of items: 1
Items: 
Size: 795843 Color: 2

Bin 2237: 204207 of cap free
Amount of items: 1
Items: 
Size: 795794 Color: 0

Bin 2238: 204282 of cap free
Amount of items: 2
Items: 
Size: 397970 Color: 2
Size: 397749 Color: 1

Bin 2239: 204282 of cap free
Amount of items: 1
Items: 
Size: 795719 Color: 0

Bin 2240: 204288 of cap free
Amount of items: 1
Items: 
Size: 795713 Color: 3

Bin 2241: 204369 of cap free
Amount of items: 1
Items: 
Size: 795632 Color: 2

Bin 2242: 204377 of cap free
Amount of items: 1
Items: 
Size: 795624 Color: 4

Bin 2243: 204526 of cap free
Amount of items: 1
Items: 
Size: 795475 Color: 4

Bin 2244: 204540 of cap free
Amount of items: 1
Items: 
Size: 795461 Color: 1

Bin 2245: 204576 of cap free
Amount of items: 1
Items: 
Size: 795425 Color: 2

Bin 2246: 204640 of cap free
Amount of items: 1
Items: 
Size: 795361 Color: 4

Bin 2247: 204687 of cap free
Amount of items: 1
Items: 
Size: 795314 Color: 2

Bin 2248: 204732 of cap free
Amount of items: 1
Items: 
Size: 795269 Color: 3

Bin 2249: 204762 of cap free
Amount of items: 1
Items: 
Size: 795239 Color: 4

Bin 2250: 204844 of cap free
Amount of items: 1
Items: 
Size: 795157 Color: 4

Bin 2251: 204849 of cap free
Amount of items: 1
Items: 
Size: 795152 Color: 1

Bin 2252: 204867 of cap free
Amount of items: 2
Items: 
Size: 397674 Color: 1
Size: 397460 Color: 4

Bin 2253: 204895 of cap free
Amount of items: 2
Items: 
Size: 397648 Color: 1
Size: 397458 Color: 3

Bin 2254: 204916 of cap free
Amount of items: 1
Items: 
Size: 795085 Color: 2

Bin 2255: 204982 of cap free
Amount of items: 1
Items: 
Size: 795019 Color: 2

Bin 2256: 205182 of cap free
Amount of items: 1
Items: 
Size: 794819 Color: 0

Bin 2257: 205268 of cap free
Amount of items: 1
Items: 
Size: 794733 Color: 3

Bin 2258: 205325 of cap free
Amount of items: 2
Items: 
Size: 397365 Color: 3
Size: 397311 Color: 2

Bin 2259: 205340 of cap free
Amount of items: 2
Items: 
Size: 397355 Color: 3
Size: 397306 Color: 0

Bin 2260: 205349 of cap free
Amount of items: 1
Items: 
Size: 794652 Color: 0

Bin 2261: 205400 of cap free
Amount of items: 2
Items: 
Size: 397331 Color: 3
Size: 397270 Color: 2

Bin 2262: 205449 of cap free
Amount of items: 1
Items: 
Size: 794552 Color: 1

Bin 2263: 205484 of cap free
Amount of items: 1
Items: 
Size: 794517 Color: 2

Bin 2264: 205648 of cap free
Amount of items: 1
Items: 
Size: 794353 Color: 3

Bin 2265: 205668 of cap free
Amount of items: 1
Items: 
Size: 794333 Color: 4

Bin 2266: 205745 of cap free
Amount of items: 2
Items: 
Size: 397184 Color: 0
Size: 397072 Color: 2

Bin 2267: 205794 of cap free
Amount of items: 1
Items: 
Size: 794207 Color: 4

Bin 2268: 205843 of cap free
Amount of items: 1
Items: 
Size: 794158 Color: 2

Bin 2269: 205908 of cap free
Amount of items: 1
Items: 
Size: 794093 Color: 4

Bin 2270: 205974 of cap free
Amount of items: 1
Items: 
Size: 794027 Color: 2

Bin 2271: 205984 of cap free
Amount of items: 1
Items: 
Size: 794017 Color: 1

Bin 2272: 206093 of cap free
Amount of items: 2
Items: 
Size: 397019 Color: 3
Size: 396889 Color: 4

Bin 2273: 206197 of cap free
Amount of items: 1
Items: 
Size: 793804 Color: 2

Bin 2274: 206241 of cap free
Amount of items: 1
Items: 
Size: 793760 Color: 3

Bin 2275: 206287 of cap free
Amount of items: 1
Items: 
Size: 793714 Color: 3

Bin 2276: 206296 of cap free
Amount of items: 2
Items: 
Size: 396862 Color: 1
Size: 396843 Color: 2

Bin 2277: 206320 of cap free
Amount of items: 1
Items: 
Size: 793681 Color: 0

Bin 2278: 206323 of cap free
Amount of items: 1
Items: 
Size: 793678 Color: 3

Bin 2279: 206393 of cap free
Amount of items: 1
Items: 
Size: 793608 Color: 2

Bin 2280: 206441 of cap free
Amount of items: 2
Items: 
Size: 396826 Color: 2
Size: 396734 Color: 0

Bin 2281: 206450 of cap free
Amount of items: 1
Items: 
Size: 793551 Color: 3

Bin 2282: 206532 of cap free
Amount of items: 1
Items: 
Size: 793469 Color: 2

Bin 2283: 206579 of cap free
Amount of items: 1
Items: 
Size: 793422 Color: 0

Bin 2284: 206589 of cap free
Amount of items: 1
Items: 
Size: 793412 Color: 4

Bin 2285: 206734 of cap free
Amount of items: 1
Items: 
Size: 793267 Color: 3

Bin 2286: 206750 of cap free
Amount of items: 1
Items: 
Size: 793251 Color: 2

Bin 2287: 206754 of cap free
Amount of items: 2
Items: 
Size: 396719 Color: 0
Size: 396528 Color: 4

Bin 2288: 206755 of cap free
Amount of items: 1
Items: 
Size: 793246 Color: 4

Bin 2289: 206830 of cap free
Amount of items: 1
Items: 
Size: 793171 Color: 2

Bin 2290: 206862 of cap free
Amount of items: 1
Items: 
Size: 793139 Color: 4

Bin 2291: 206933 of cap free
Amount of items: 1
Items: 
Size: 793068 Color: 1

Bin 2292: 206992 of cap free
Amount of items: 2
Items: 
Size: 396527 Color: 2
Size: 396482 Color: 3

Bin 2293: 207111 of cap free
Amount of items: 2
Items: 
Size: 396480 Color: 2
Size: 396410 Color: 0

Bin 2294: 207199 of cap free
Amount of items: 2
Items: 
Size: 396403 Color: 4
Size: 396399 Color: 3

Bin 2295: 207283 of cap free
Amount of items: 1
Items: 
Size: 792718 Color: 1

Bin 2296: 207353 of cap free
Amount of items: 2
Items: 
Size: 396356 Color: 0
Size: 396292 Color: 1

Bin 2297: 207462 of cap free
Amount of items: 2
Items: 
Size: 396284 Color: 2
Size: 396255 Color: 4

Bin 2298: 207470 of cap free
Amount of items: 1
Items: 
Size: 792531 Color: 1

Bin 2299: 207658 of cap free
Amount of items: 1
Items: 
Size: 792343 Color: 3

Bin 2300: 207699 of cap free
Amount of items: 2
Items: 
Size: 396181 Color: 2
Size: 396121 Color: 4

Bin 2301: 207751 of cap free
Amount of items: 1
Items: 
Size: 792250 Color: 4

Bin 2302: 207776 of cap free
Amount of items: 1
Items: 
Size: 792225 Color: 2

Bin 2303: 207780 of cap free
Amount of items: 2
Items: 
Size: 396175 Color: 2
Size: 396046 Color: 4

Bin 2304: 207868 of cap free
Amount of items: 1
Items: 
Size: 792133 Color: 1

Bin 2305: 207890 of cap free
Amount of items: 1
Items: 
Size: 792111 Color: 0

Bin 2306: 207924 of cap free
Amount of items: 1
Items: 
Size: 792077 Color: 4

Bin 2307: 208091 of cap free
Amount of items: 1
Items: 
Size: 791910 Color: 4

Bin 2308: 208124 of cap free
Amount of items: 1
Items: 
Size: 791877 Color: 1

Bin 2309: 208255 of cap free
Amount of items: 1
Items: 
Size: 791746 Color: 1

Bin 2310: 208337 of cap free
Amount of items: 1
Items: 
Size: 791664 Color: 2

Bin 2311: 208381 of cap free
Amount of items: 1
Items: 
Size: 791620 Color: 0

Bin 2312: 208402 of cap free
Amount of items: 1
Items: 
Size: 791599 Color: 3

Bin 2313: 208416 of cap free
Amount of items: 1
Items: 
Size: 791585 Color: 3

Bin 2314: 208456 of cap free
Amount of items: 1
Items: 
Size: 791545 Color: 2

Bin 2315: 208474 of cap free
Amount of items: 1
Items: 
Size: 791527 Color: 0

Bin 2316: 208541 of cap free
Amount of items: 1
Items: 
Size: 791460 Color: 1

Bin 2317: 208595 of cap free
Amount of items: 1
Items: 
Size: 791406 Color: 1

Bin 2318: 208598 of cap free
Amount of items: 2
Items: 
Size: 395724 Color: 1
Size: 395679 Color: 3

Bin 2319: 208672 of cap free
Amount of items: 1
Items: 
Size: 791329 Color: 1

Bin 2320: 208753 of cap free
Amount of items: 1
Items: 
Size: 791248 Color: 2

Bin 2321: 208765 of cap free
Amount of items: 1
Items: 
Size: 791236 Color: 0

Bin 2322: 208834 of cap free
Amount of items: 1
Items: 
Size: 791167 Color: 0

Bin 2323: 208889 of cap free
Amount of items: 1
Items: 
Size: 791112 Color: 0

Bin 2324: 209059 of cap free
Amount of items: 1
Items: 
Size: 790942 Color: 4

Bin 2325: 209154 of cap free
Amount of items: 1
Items: 
Size: 790847 Color: 3

Bin 2326: 209179 of cap free
Amount of items: 1
Items: 
Size: 790822 Color: 3

Bin 2327: 209336 of cap free
Amount of items: 1
Items: 
Size: 790665 Color: 1

Bin 2328: 209342 of cap free
Amount of items: 1
Items: 
Size: 790659 Color: 0

Bin 2329: 209351 of cap free
Amount of items: 1
Items: 
Size: 790650 Color: 2

Bin 2330: 209478 of cap free
Amount of items: 1
Items: 
Size: 790523 Color: 0

Bin 2331: 209479 of cap free
Amount of items: 1
Items: 
Size: 790522 Color: 3

Bin 2332: 209499 of cap free
Amount of items: 1
Items: 
Size: 790502 Color: 4

Bin 2333: 209570 of cap free
Amount of items: 1
Items: 
Size: 790431 Color: 3

Bin 2334: 209606 of cap free
Amount of items: 1
Items: 
Size: 790395 Color: 4

Bin 2335: 209656 of cap free
Amount of items: 1
Items: 
Size: 790345 Color: 1

Bin 2336: 209664 of cap free
Amount of items: 1
Items: 
Size: 790337 Color: 0

Bin 2337: 209684 of cap free
Amount of items: 1
Items: 
Size: 790317 Color: 0

Bin 2338: 209813 of cap free
Amount of items: 1
Items: 
Size: 790188 Color: 1

Bin 2339: 209843 of cap free
Amount of items: 1
Items: 
Size: 790158 Color: 3

Bin 2340: 209857 of cap free
Amount of items: 1
Items: 
Size: 790144 Color: 3

Bin 2341: 210111 of cap free
Amount of items: 1
Items: 
Size: 789890 Color: 3

Bin 2342: 210196 of cap free
Amount of items: 1
Items: 
Size: 789805 Color: 4

Bin 2343: 210204 of cap free
Amount of items: 1
Items: 
Size: 789797 Color: 3

Bin 2344: 210245 of cap free
Amount of items: 1
Items: 
Size: 789756 Color: 1

Bin 2345: 210278 of cap free
Amount of items: 1
Items: 
Size: 789723 Color: 1

Bin 2346: 210395 of cap free
Amount of items: 1
Items: 
Size: 789606 Color: 1

Bin 2347: 210505 of cap free
Amount of items: 1
Items: 
Size: 789496 Color: 3

Bin 2348: 210559 of cap free
Amount of items: 1
Items: 
Size: 789442 Color: 2

Bin 2349: 210601 of cap free
Amount of items: 1
Items: 
Size: 789400 Color: 3

Bin 2350: 210631 of cap free
Amount of items: 1
Items: 
Size: 789370 Color: 0

Bin 2351: 210652 of cap free
Amount of items: 1
Items: 
Size: 789349 Color: 4

Bin 2352: 210684 of cap free
Amount of items: 1
Items: 
Size: 789317 Color: 1

Bin 2353: 210688 of cap free
Amount of items: 1
Items: 
Size: 789313 Color: 3

Bin 2354: 210802 of cap free
Amount of items: 1
Items: 
Size: 789199 Color: 3

Bin 2355: 210819 of cap free
Amount of items: 1
Items: 
Size: 789182 Color: 0

Bin 2356: 210820 of cap free
Amount of items: 1
Items: 
Size: 789181 Color: 2

Bin 2357: 210967 of cap free
Amount of items: 1
Items: 
Size: 789034 Color: 4

Bin 2358: 211013 of cap free
Amount of items: 1
Items: 
Size: 788988 Color: 4

Bin 2359: 211058 of cap free
Amount of items: 1
Items: 
Size: 788943 Color: 0

Bin 2360: 211090 of cap free
Amount of items: 1
Items: 
Size: 788911 Color: 4

Bin 2361: 211119 of cap free
Amount of items: 1
Items: 
Size: 788882 Color: 2

Bin 2362: 211142 of cap free
Amount of items: 1
Items: 
Size: 788859 Color: 0

Bin 2363: 211268 of cap free
Amount of items: 1
Items: 
Size: 788733 Color: 1

Bin 2364: 211374 of cap free
Amount of items: 1
Items: 
Size: 788627 Color: 1

Bin 2365: 211420 of cap free
Amount of items: 1
Items: 
Size: 788581 Color: 1

Bin 2366: 211428 of cap free
Amount of items: 1
Items: 
Size: 788573 Color: 3

Bin 2367: 211487 of cap free
Amount of items: 1
Items: 
Size: 788514 Color: 1

Bin 2368: 211515 of cap free
Amount of items: 1
Items: 
Size: 788486 Color: 0

Bin 2369: 211693 of cap free
Amount of items: 1
Items: 
Size: 788308 Color: 0

Bin 2370: 211730 of cap free
Amount of items: 1
Items: 
Size: 788271 Color: 2

Bin 2371: 211763 of cap free
Amount of items: 1
Items: 
Size: 788238 Color: 3

Bin 2372: 211877 of cap free
Amount of items: 1
Items: 
Size: 788124 Color: 0

Bin 2373: 211981 of cap free
Amount of items: 1
Items: 
Size: 788020 Color: 3

Bin 2374: 212104 of cap free
Amount of items: 1
Items: 
Size: 787897 Color: 1

Bin 2375: 212105 of cap free
Amount of items: 1
Items: 
Size: 787896 Color: 3

Bin 2376: 212405 of cap free
Amount of items: 1
Items: 
Size: 787596 Color: 1

Bin 2377: 212546 of cap free
Amount of items: 1
Items: 
Size: 787455 Color: 3

Bin 2378: 212690 of cap free
Amount of items: 1
Items: 
Size: 787311 Color: 4

Bin 2379: 212822 of cap free
Amount of items: 1
Items: 
Size: 787179 Color: 3

Bin 2380: 213038 of cap free
Amount of items: 1
Items: 
Size: 786963 Color: 1

Bin 2381: 213117 of cap free
Amount of items: 1
Items: 
Size: 786884 Color: 4

Bin 2382: 213267 of cap free
Amount of items: 1
Items: 
Size: 786734 Color: 1

Bin 2383: 213405 of cap free
Amount of items: 1
Items: 
Size: 786596 Color: 0

Bin 2384: 213443 of cap free
Amount of items: 1
Items: 
Size: 786558 Color: 2

Bin 2385: 213493 of cap free
Amount of items: 1
Items: 
Size: 786508 Color: 1

Bin 2386: 213499 of cap free
Amount of items: 1
Items: 
Size: 786502 Color: 2

Bin 2387: 213638 of cap free
Amount of items: 1
Items: 
Size: 786363 Color: 2

Bin 2388: 213657 of cap free
Amount of items: 1
Items: 
Size: 786344 Color: 4

Bin 2389: 213667 of cap free
Amount of items: 1
Items: 
Size: 786334 Color: 3

Bin 2390: 213669 of cap free
Amount of items: 1
Items: 
Size: 786332 Color: 1

Bin 2391: 213674 of cap free
Amount of items: 1
Items: 
Size: 786327 Color: 4

Bin 2392: 213745 of cap free
Amount of items: 1
Items: 
Size: 786256 Color: 4

Bin 2393: 213761 of cap free
Amount of items: 1
Items: 
Size: 786240 Color: 4

Bin 2394: 213817 of cap free
Amount of items: 1
Items: 
Size: 786184 Color: 1

Bin 2395: 213822 of cap free
Amount of items: 1
Items: 
Size: 786179 Color: 2

Bin 2396: 214374 of cap free
Amount of items: 1
Items: 
Size: 785627 Color: 1

Bin 2397: 214425 of cap free
Amount of items: 1
Items: 
Size: 785576 Color: 0

Bin 2398: 214475 of cap free
Amount of items: 1
Items: 
Size: 785526 Color: 0

Bin 2399: 214599 of cap free
Amount of items: 1
Items: 
Size: 785402 Color: 1

Bin 2400: 214688 of cap free
Amount of items: 1
Items: 
Size: 785313 Color: 4

Bin 2401: 214697 of cap free
Amount of items: 1
Items: 
Size: 785304 Color: 1

Bin 2402: 214807 of cap free
Amount of items: 1
Items: 
Size: 785194 Color: 0

Bin 2403: 214816 of cap free
Amount of items: 1
Items: 
Size: 785185 Color: 1

Bin 2404: 214852 of cap free
Amount of items: 1
Items: 
Size: 785149 Color: 1

Bin 2405: 214936 of cap free
Amount of items: 1
Items: 
Size: 785065 Color: 0

Bin 2406: 215023 of cap free
Amount of items: 1
Items: 
Size: 784978 Color: 4

Bin 2407: 215047 of cap free
Amount of items: 1
Items: 
Size: 784954 Color: 4

Bin 2408: 215160 of cap free
Amount of items: 1
Items: 
Size: 784841 Color: 1

Bin 2409: 215174 of cap free
Amount of items: 1
Items: 
Size: 784827 Color: 4

Bin 2410: 215252 of cap free
Amount of items: 1
Items: 
Size: 784749 Color: 0

Bin 2411: 215307 of cap free
Amount of items: 1
Items: 
Size: 784694 Color: 3

Bin 2412: 215389 of cap free
Amount of items: 1
Items: 
Size: 784612 Color: 3

Bin 2413: 215411 of cap free
Amount of items: 1
Items: 
Size: 784590 Color: 4

Bin 2414: 215529 of cap free
Amount of items: 1
Items: 
Size: 784472 Color: 1

Bin 2415: 215567 of cap free
Amount of items: 1
Items: 
Size: 784434 Color: 2

Bin 2416: 215600 of cap free
Amount of items: 1
Items: 
Size: 784401 Color: 2

Bin 2417: 215708 of cap free
Amount of items: 1
Items: 
Size: 784293 Color: 1

Bin 2418: 215843 of cap free
Amount of items: 1
Items: 
Size: 784158 Color: 0

Bin 2419: 215850 of cap free
Amount of items: 1
Items: 
Size: 784151 Color: 2

Bin 2420: 215907 of cap free
Amount of items: 1
Items: 
Size: 784094 Color: 0

Bin 2421: 216000 of cap free
Amount of items: 1
Items: 
Size: 784001 Color: 2

Bin 2422: 216034 of cap free
Amount of items: 1
Items: 
Size: 783967 Color: 1

Bin 2423: 216098 of cap free
Amount of items: 1
Items: 
Size: 783903 Color: 0

Bin 2424: 216117 of cap free
Amount of items: 1
Items: 
Size: 783884 Color: 3

Bin 2425: 216168 of cap free
Amount of items: 1
Items: 
Size: 783833 Color: 2

Bin 2426: 216175 of cap free
Amount of items: 1
Items: 
Size: 783826 Color: 0

Bin 2427: 216187 of cap free
Amount of items: 1
Items: 
Size: 783814 Color: 2

Bin 2428: 216282 of cap free
Amount of items: 1
Items: 
Size: 783719 Color: 2

Bin 2429: 216347 of cap free
Amount of items: 1
Items: 
Size: 783654 Color: 1

Bin 2430: 216347 of cap free
Amount of items: 1
Items: 
Size: 783654 Color: 2

Bin 2431: 216419 of cap free
Amount of items: 1
Items: 
Size: 783582 Color: 4

Bin 2432: 216451 of cap free
Amount of items: 1
Items: 
Size: 783550 Color: 1

Bin 2433: 216500 of cap free
Amount of items: 1
Items: 
Size: 783501 Color: 4

Bin 2434: 216580 of cap free
Amount of items: 1
Items: 
Size: 783421 Color: 0

Bin 2435: 216603 of cap free
Amount of items: 1
Items: 
Size: 783398 Color: 0

Bin 2436: 216672 of cap free
Amount of items: 1
Items: 
Size: 783329 Color: 1

Bin 2437: 216674 of cap free
Amount of items: 1
Items: 
Size: 783327 Color: 0

Bin 2438: 216725 of cap free
Amount of items: 1
Items: 
Size: 783276 Color: 2

Bin 2439: 216877 of cap free
Amount of items: 1
Items: 
Size: 783124 Color: 0

Bin 2440: 216893 of cap free
Amount of items: 1
Items: 
Size: 783108 Color: 1

Bin 2441: 216910 of cap free
Amount of items: 1
Items: 
Size: 783091 Color: 2

Bin 2442: 216913 of cap free
Amount of items: 1
Items: 
Size: 783088 Color: 4

Bin 2443: 216932 of cap free
Amount of items: 1
Items: 
Size: 783069 Color: 4

Bin 2444: 217061 of cap free
Amount of items: 1
Items: 
Size: 782940 Color: 1

Bin 2445: 217070 of cap free
Amount of items: 1
Items: 
Size: 782931 Color: 1

Bin 2446: 217290 of cap free
Amount of items: 1
Items: 
Size: 782711 Color: 2

Bin 2447: 217296 of cap free
Amount of items: 1
Items: 
Size: 782705 Color: 2

Bin 2448: 217431 of cap free
Amount of items: 1
Items: 
Size: 782570 Color: 2

Bin 2449: 217476 of cap free
Amount of items: 1
Items: 
Size: 782525 Color: 0

Bin 2450: 217539 of cap free
Amount of items: 1
Items: 
Size: 782462 Color: 0

Bin 2451: 217565 of cap free
Amount of items: 1
Items: 
Size: 782436 Color: 0

Bin 2452: 217744 of cap free
Amount of items: 1
Items: 
Size: 782257 Color: 2

Bin 2453: 217827 of cap free
Amount of items: 1
Items: 
Size: 782174 Color: 2

Bin 2454: 217881 of cap free
Amount of items: 1
Items: 
Size: 782120 Color: 0

Bin 2455: 217889 of cap free
Amount of items: 1
Items: 
Size: 782112 Color: 4

Bin 2456: 217953 of cap free
Amount of items: 1
Items: 
Size: 782048 Color: 3

Bin 2457: 218015 of cap free
Amount of items: 1
Items: 
Size: 781986 Color: 3

Bin 2458: 218181 of cap free
Amount of items: 1
Items: 
Size: 781820 Color: 3

Bin 2459: 218345 of cap free
Amount of items: 1
Items: 
Size: 781656 Color: 3

Bin 2460: 218391 of cap free
Amount of items: 1
Items: 
Size: 781610 Color: 0

Bin 2461: 218393 of cap free
Amount of items: 1
Items: 
Size: 781608 Color: 3

Bin 2462: 218641 of cap free
Amount of items: 1
Items: 
Size: 781360 Color: 3

Bin 2463: 218893 of cap free
Amount of items: 1
Items: 
Size: 781108 Color: 3

Bin 2464: 218924 of cap free
Amount of items: 1
Items: 
Size: 781077 Color: 2

Bin 2465: 219022 of cap free
Amount of items: 1
Items: 
Size: 780979 Color: 4

Bin 2466: 219093 of cap free
Amount of items: 1
Items: 
Size: 780908 Color: 2

Bin 2467: 219169 of cap free
Amount of items: 1
Items: 
Size: 780832 Color: 3

Bin 2468: 219216 of cap free
Amount of items: 1
Items: 
Size: 780785 Color: 4

Bin 2469: 219465 of cap free
Amount of items: 1
Items: 
Size: 780536 Color: 1

Bin 2470: 219550 of cap free
Amount of items: 1
Items: 
Size: 780451 Color: 1

Bin 2471: 219571 of cap free
Amount of items: 1
Items: 
Size: 780430 Color: 3

Bin 2472: 219583 of cap free
Amount of items: 1
Items: 
Size: 780418 Color: 0

Bin 2473: 219661 of cap free
Amount of items: 1
Items: 
Size: 780340 Color: 3

Bin 2474: 219749 of cap free
Amount of items: 1
Items: 
Size: 780252 Color: 1

Bin 2475: 219801 of cap free
Amount of items: 1
Items: 
Size: 780200 Color: 4

Bin 2476: 219897 of cap free
Amount of items: 1
Items: 
Size: 780104 Color: 0

Bin 2477: 219898 of cap free
Amount of items: 1
Items: 
Size: 780103 Color: 1

Bin 2478: 219922 of cap free
Amount of items: 1
Items: 
Size: 780079 Color: 0

Bin 2479: 219954 of cap free
Amount of items: 1
Items: 
Size: 780047 Color: 0

Bin 2480: 219987 of cap free
Amount of items: 1
Items: 
Size: 780014 Color: 2

Bin 2481: 220180 of cap free
Amount of items: 1
Items: 
Size: 779821 Color: 0

Bin 2482: 220323 of cap free
Amount of items: 1
Items: 
Size: 779678 Color: 3

Bin 2483: 220327 of cap free
Amount of items: 1
Items: 
Size: 779674 Color: 2

Bin 2484: 220350 of cap free
Amount of items: 1
Items: 
Size: 779651 Color: 3

Bin 2485: 220374 of cap free
Amount of items: 1
Items: 
Size: 779627 Color: 4

Bin 2486: 220382 of cap free
Amount of items: 1
Items: 
Size: 779619 Color: 1

Bin 2487: 220494 of cap free
Amount of items: 1
Items: 
Size: 779507 Color: 4

Bin 2488: 220653 of cap free
Amount of items: 1
Items: 
Size: 779348 Color: 1

Bin 2489: 220653 of cap free
Amount of items: 1
Items: 
Size: 779348 Color: 2

Bin 2490: 220727 of cap free
Amount of items: 1
Items: 
Size: 779274 Color: 4

Bin 2491: 220822 of cap free
Amount of items: 1
Items: 
Size: 779179 Color: 3

Bin 2492: 220921 of cap free
Amount of items: 1
Items: 
Size: 779080 Color: 0

Bin 2493: 220978 of cap free
Amount of items: 1
Items: 
Size: 779023 Color: 3

Bin 2494: 221011 of cap free
Amount of items: 1
Items: 
Size: 778990 Color: 3

Bin 2495: 221053 of cap free
Amount of items: 1
Items: 
Size: 778948 Color: 3

Bin 2496: 221054 of cap free
Amount of items: 1
Items: 
Size: 778947 Color: 0

Bin 2497: 221163 of cap free
Amount of items: 1
Items: 
Size: 778838 Color: 4

Bin 2498: 221222 of cap free
Amount of items: 1
Items: 
Size: 778779 Color: 2

Bin 2499: 221232 of cap free
Amount of items: 1
Items: 
Size: 778769 Color: 2

Bin 2500: 221373 of cap free
Amount of items: 1
Items: 
Size: 778628 Color: 1

Bin 2501: 221391 of cap free
Amount of items: 1
Items: 
Size: 778610 Color: 4

Bin 2502: 221404 of cap free
Amount of items: 1
Items: 
Size: 778597 Color: 4

Bin 2503: 221413 of cap free
Amount of items: 1
Items: 
Size: 778588 Color: 2

Bin 2504: 221723 of cap free
Amount of items: 1
Items: 
Size: 778278 Color: 2

Bin 2505: 221815 of cap free
Amount of items: 1
Items: 
Size: 778186 Color: 1

Bin 2506: 221969 of cap free
Amount of items: 1
Items: 
Size: 778032 Color: 2

Bin 2507: 221974 of cap free
Amount of items: 1
Items: 
Size: 778027 Color: 3

Bin 2508: 222081 of cap free
Amount of items: 1
Items: 
Size: 777920 Color: 1

Bin 2509: 222087 of cap free
Amount of items: 1
Items: 
Size: 777914 Color: 0

Bin 2510: 222136 of cap free
Amount of items: 1
Items: 
Size: 777865 Color: 0

Bin 2511: 222143 of cap free
Amount of items: 1
Items: 
Size: 777858 Color: 1

Bin 2512: 222250 of cap free
Amount of items: 1
Items: 
Size: 777751 Color: 2

Bin 2513: 222443 of cap free
Amount of items: 1
Items: 
Size: 777558 Color: 4

Bin 2514: 222561 of cap free
Amount of items: 1
Items: 
Size: 777440 Color: 4

Bin 2515: 222581 of cap free
Amount of items: 1
Items: 
Size: 777420 Color: 2

Bin 2516: 222609 of cap free
Amount of items: 1
Items: 
Size: 777392 Color: 0

Bin 2517: 222690 of cap free
Amount of items: 1
Items: 
Size: 777311 Color: 2

Bin 2518: 222959 of cap free
Amount of items: 1
Items: 
Size: 777042 Color: 2

Bin 2519: 223197 of cap free
Amount of items: 1
Items: 
Size: 776804 Color: 2

Bin 2520: 223347 of cap free
Amount of items: 1
Items: 
Size: 776654 Color: 1

Bin 2521: 223379 of cap free
Amount of items: 1
Items: 
Size: 776622 Color: 2

Bin 2522: 223440 of cap free
Amount of items: 1
Items: 
Size: 776561 Color: 4

Bin 2523: 223476 of cap free
Amount of items: 1
Items: 
Size: 776525 Color: 1

Bin 2524: 223490 of cap free
Amount of items: 1
Items: 
Size: 776511 Color: 4

Bin 2525: 223504 of cap free
Amount of items: 1
Items: 
Size: 776497 Color: 2

Bin 2526: 223539 of cap free
Amount of items: 1
Items: 
Size: 776462 Color: 3

Bin 2527: 223583 of cap free
Amount of items: 1
Items: 
Size: 776418 Color: 1

Bin 2528: 223711 of cap free
Amount of items: 1
Items: 
Size: 776290 Color: 2

Bin 2529: 223814 of cap free
Amount of items: 1
Items: 
Size: 776187 Color: 3

Bin 2530: 223921 of cap free
Amount of items: 1
Items: 
Size: 776080 Color: 1

Bin 2531: 223942 of cap free
Amount of items: 1
Items: 
Size: 776059 Color: 3

Bin 2532: 224007 of cap free
Amount of items: 1
Items: 
Size: 775994 Color: 4

Bin 2533: 224301 of cap free
Amount of items: 1
Items: 
Size: 775700 Color: 4

Bin 2534: 224320 of cap free
Amount of items: 1
Items: 
Size: 775681 Color: 0

Bin 2535: 224387 of cap free
Amount of items: 1
Items: 
Size: 775614 Color: 1

Bin 2536: 224394 of cap free
Amount of items: 1
Items: 
Size: 775607 Color: 3

Bin 2537: 224395 of cap free
Amount of items: 1
Items: 
Size: 775606 Color: 4

Bin 2538: 224425 of cap free
Amount of items: 1
Items: 
Size: 775576 Color: 0

Bin 2539: 224589 of cap free
Amount of items: 1
Items: 
Size: 775412 Color: 4

Bin 2540: 224696 of cap free
Amount of items: 1
Items: 
Size: 775305 Color: 2

Bin 2541: 224699 of cap free
Amount of items: 1
Items: 
Size: 775302 Color: 2

Bin 2542: 224723 of cap free
Amount of items: 1
Items: 
Size: 775278 Color: 0

Bin 2543: 224860 of cap free
Amount of items: 1
Items: 
Size: 775141 Color: 0

Bin 2544: 224889 of cap free
Amount of items: 1
Items: 
Size: 775112 Color: 1

Bin 2545: 224938 of cap free
Amount of items: 1
Items: 
Size: 775063 Color: 4

Bin 2546: 224956 of cap free
Amount of items: 1
Items: 
Size: 775045 Color: 3

Bin 2547: 224979 of cap free
Amount of items: 1
Items: 
Size: 775022 Color: 3

Bin 2548: 225108 of cap free
Amount of items: 1
Items: 
Size: 774893 Color: 3

Bin 2549: 225125 of cap free
Amount of items: 1
Items: 
Size: 774876 Color: 4

Bin 2550: 225197 of cap free
Amount of items: 1
Items: 
Size: 774804 Color: 3

Bin 2551: 225299 of cap free
Amount of items: 1
Items: 
Size: 774702 Color: 0

Bin 2552: 225321 of cap free
Amount of items: 1
Items: 
Size: 774680 Color: 0

Bin 2553: 225383 of cap free
Amount of items: 1
Items: 
Size: 774618 Color: 2

Bin 2554: 225527 of cap free
Amount of items: 1
Items: 
Size: 774474 Color: 1

Bin 2555: 225648 of cap free
Amount of items: 1
Items: 
Size: 774353 Color: 3

Bin 2556: 225661 of cap free
Amount of items: 1
Items: 
Size: 774340 Color: 0

Bin 2557: 225754 of cap free
Amount of items: 1
Items: 
Size: 774247 Color: 1

Bin 2558: 225834 of cap free
Amount of items: 1
Items: 
Size: 774167 Color: 1

Bin 2559: 225879 of cap free
Amount of items: 1
Items: 
Size: 774122 Color: 3

Bin 2560: 225893 of cap free
Amount of items: 1
Items: 
Size: 774108 Color: 4

Bin 2561: 225938 of cap free
Amount of items: 1
Items: 
Size: 774063 Color: 0

Bin 2562: 226084 of cap free
Amount of items: 1
Items: 
Size: 773917 Color: 2

Bin 2563: 226153 of cap free
Amount of items: 1
Items: 
Size: 773848 Color: 2

Bin 2564: 226212 of cap free
Amount of items: 1
Items: 
Size: 773789 Color: 2

Bin 2565: 226240 of cap free
Amount of items: 1
Items: 
Size: 773761 Color: 2

Bin 2566: 226322 of cap free
Amount of items: 1
Items: 
Size: 773679 Color: 4

Bin 2567: 226385 of cap free
Amount of items: 1
Items: 
Size: 773616 Color: 0

Bin 2568: 226469 of cap free
Amount of items: 1
Items: 
Size: 773532 Color: 4

Bin 2569: 226535 of cap free
Amount of items: 1
Items: 
Size: 773466 Color: 3

Bin 2570: 226542 of cap free
Amount of items: 1
Items: 
Size: 773459 Color: 1

Bin 2571: 226681 of cap free
Amount of items: 1
Items: 
Size: 773320 Color: 3

Bin 2572: 226743 of cap free
Amount of items: 1
Items: 
Size: 773258 Color: 3

Bin 2573: 226766 of cap free
Amount of items: 1
Items: 
Size: 773235 Color: 3

Bin 2574: 226903 of cap free
Amount of items: 1
Items: 
Size: 773098 Color: 2

Bin 2575: 227012 of cap free
Amount of items: 1
Items: 
Size: 772989 Color: 3

Bin 2576: 227071 of cap free
Amount of items: 1
Items: 
Size: 772930 Color: 2

Bin 2577: 227200 of cap free
Amount of items: 1
Items: 
Size: 772801 Color: 4

Bin 2578: 227215 of cap free
Amount of items: 1
Items: 
Size: 772786 Color: 1

Bin 2579: 227247 of cap free
Amount of items: 1
Items: 
Size: 772754 Color: 4

Bin 2580: 227364 of cap free
Amount of items: 1
Items: 
Size: 772637 Color: 4

Bin 2581: 227456 of cap free
Amount of items: 1
Items: 
Size: 772545 Color: 4

Bin 2582: 227474 of cap free
Amount of items: 1
Items: 
Size: 772527 Color: 2

Bin 2583: 227596 of cap free
Amount of items: 1
Items: 
Size: 772405 Color: 4

Bin 2584: 227638 of cap free
Amount of items: 1
Items: 
Size: 772363 Color: 0

Bin 2585: 227807 of cap free
Amount of items: 1
Items: 
Size: 772194 Color: 4

Bin 2586: 227833 of cap free
Amount of items: 1
Items: 
Size: 772168 Color: 1

Bin 2587: 227902 of cap free
Amount of items: 1
Items: 
Size: 772099 Color: 4

Bin 2588: 227902 of cap free
Amount of items: 1
Items: 
Size: 772099 Color: 4

Bin 2589: 227924 of cap free
Amount of items: 1
Items: 
Size: 772077 Color: 2

Bin 2590: 228020 of cap free
Amount of items: 1
Items: 
Size: 771981 Color: 0

Bin 2591: 228025 of cap free
Amount of items: 1
Items: 
Size: 771976 Color: 3

Bin 2592: 228037 of cap free
Amount of items: 1
Items: 
Size: 771964 Color: 2

Bin 2593: 228047 of cap free
Amount of items: 1
Items: 
Size: 771954 Color: 3

Bin 2594: 228185 of cap free
Amount of items: 1
Items: 
Size: 771816 Color: 0

Bin 2595: 228215 of cap free
Amount of items: 1
Items: 
Size: 771786 Color: 3

Bin 2596: 228457 of cap free
Amount of items: 1
Items: 
Size: 771544 Color: 3

Bin 2597: 228542 of cap free
Amount of items: 1
Items: 
Size: 771459 Color: 1

Bin 2598: 228621 of cap free
Amount of items: 1
Items: 
Size: 771380 Color: 4

Bin 2599: 228653 of cap free
Amount of items: 1
Items: 
Size: 771348 Color: 4

Bin 2600: 228675 of cap free
Amount of items: 1
Items: 
Size: 771326 Color: 4

Bin 2601: 228772 of cap free
Amount of items: 1
Items: 
Size: 771229 Color: 2

Bin 2602: 228982 of cap free
Amount of items: 1
Items: 
Size: 771019 Color: 0

Bin 2603: 229011 of cap free
Amount of items: 1
Items: 
Size: 770990 Color: 1

Bin 2604: 229012 of cap free
Amount of items: 1
Items: 
Size: 770989 Color: 3

Bin 2605: 229089 of cap free
Amount of items: 1
Items: 
Size: 770912 Color: 0

Bin 2606: 229174 of cap free
Amount of items: 1
Items: 
Size: 770827 Color: 0

Bin 2607: 229385 of cap free
Amount of items: 1
Items: 
Size: 770616 Color: 0

Bin 2608: 229405 of cap free
Amount of items: 1
Items: 
Size: 770596 Color: 3

Bin 2609: 229409 of cap free
Amount of items: 1
Items: 
Size: 770592 Color: 3

Bin 2610: 229546 of cap free
Amount of items: 1
Items: 
Size: 770455 Color: 1

Bin 2611: 229583 of cap free
Amount of items: 1
Items: 
Size: 770418 Color: 0

Bin 2612: 229706 of cap free
Amount of items: 1
Items: 
Size: 770295 Color: 4

Bin 2613: 230048 of cap free
Amount of items: 1
Items: 
Size: 769953 Color: 2

Bin 2614: 230078 of cap free
Amount of items: 1
Items: 
Size: 769923 Color: 0

Bin 2615: 230111 of cap free
Amount of items: 1
Items: 
Size: 769890 Color: 4

Bin 2616: 230125 of cap free
Amount of items: 1
Items: 
Size: 769876 Color: 0

Bin 2617: 230154 of cap free
Amount of items: 1
Items: 
Size: 769847 Color: 4

Bin 2618: 230188 of cap free
Amount of items: 1
Items: 
Size: 769813 Color: 1

Bin 2619: 230188 of cap free
Amount of items: 1
Items: 
Size: 769813 Color: 1

Bin 2620: 230199 of cap free
Amount of items: 1
Items: 
Size: 769802 Color: 4

Bin 2621: 230243 of cap free
Amount of items: 1
Items: 
Size: 769758 Color: 1

Bin 2622: 230251 of cap free
Amount of items: 1
Items: 
Size: 769750 Color: 0

Bin 2623: 230314 of cap free
Amount of items: 1
Items: 
Size: 769687 Color: 4

Bin 2624: 230418 of cap free
Amount of items: 1
Items: 
Size: 769583 Color: 0

Bin 2625: 230432 of cap free
Amount of items: 1
Items: 
Size: 769569 Color: 3

Bin 2626: 230574 of cap free
Amount of items: 1
Items: 
Size: 769427 Color: 4

Bin 2627: 230582 of cap free
Amount of items: 1
Items: 
Size: 769419 Color: 3

Bin 2628: 230770 of cap free
Amount of items: 1
Items: 
Size: 769231 Color: 3

Bin 2629: 230809 of cap free
Amount of items: 1
Items: 
Size: 769192 Color: 3

Bin 2630: 230833 of cap free
Amount of items: 1
Items: 
Size: 769168 Color: 2

Bin 2631: 230846 of cap free
Amount of items: 1
Items: 
Size: 769155 Color: 4

Bin 2632: 230968 of cap free
Amount of items: 1
Items: 
Size: 769033 Color: 4

Bin 2633: 230986 of cap free
Amount of items: 1
Items: 
Size: 769015 Color: 0

Bin 2634: 231029 of cap free
Amount of items: 1
Items: 
Size: 768972 Color: 4

Bin 2635: 231099 of cap free
Amount of items: 1
Items: 
Size: 768902 Color: 1

Bin 2636: 231164 of cap free
Amount of items: 1
Items: 
Size: 768837 Color: 2

Bin 2637: 231180 of cap free
Amount of items: 1
Items: 
Size: 768821 Color: 0

Bin 2638: 231202 of cap free
Amount of items: 1
Items: 
Size: 768799 Color: 0

Bin 2639: 231256 of cap free
Amount of items: 1
Items: 
Size: 768745 Color: 2

Bin 2640: 231289 of cap free
Amount of items: 1
Items: 
Size: 768712 Color: 1

Bin 2641: 231438 of cap free
Amount of items: 1
Items: 
Size: 768563 Color: 0

Bin 2642: 231444 of cap free
Amount of items: 1
Items: 
Size: 768557 Color: 1

Bin 2643: 231463 of cap free
Amount of items: 1
Items: 
Size: 768538 Color: 1

Bin 2644: 231472 of cap free
Amount of items: 1
Items: 
Size: 768529 Color: 0

Bin 2645: 231509 of cap free
Amount of items: 1
Items: 
Size: 768492 Color: 1

Bin 2646: 231536 of cap free
Amount of items: 1
Items: 
Size: 768465 Color: 2

Bin 2647: 231649 of cap free
Amount of items: 1
Items: 
Size: 768352 Color: 0

Bin 2648: 231723 of cap free
Amount of items: 1
Items: 
Size: 768278 Color: 4

Bin 2649: 231746 of cap free
Amount of items: 1
Items: 
Size: 768255 Color: 1

Bin 2650: 231750 of cap free
Amount of items: 1
Items: 
Size: 768251 Color: 3

Bin 2651: 231750 of cap free
Amount of items: 1
Items: 
Size: 768251 Color: 3

Bin 2652: 231850 of cap free
Amount of items: 1
Items: 
Size: 768151 Color: 1

Bin 2653: 231858 of cap free
Amount of items: 1
Items: 
Size: 768143 Color: 0

Bin 2654: 232110 of cap free
Amount of items: 1
Items: 
Size: 767891 Color: 0

Bin 2655: 232142 of cap free
Amount of items: 1
Items: 
Size: 767859 Color: 2

Bin 2656: 232310 of cap free
Amount of items: 1
Items: 
Size: 767691 Color: 4

Bin 2657: 232324 of cap free
Amount of items: 1
Items: 
Size: 767677 Color: 3

Bin 2658: 232329 of cap free
Amount of items: 1
Items: 
Size: 767672 Color: 2

Bin 2659: 232478 of cap free
Amount of items: 1
Items: 
Size: 767523 Color: 3

Bin 2660: 232673 of cap free
Amount of items: 1
Items: 
Size: 767328 Color: 1

Bin 2661: 232785 of cap free
Amount of items: 1
Items: 
Size: 767216 Color: 1

Bin 2662: 232845 of cap free
Amount of items: 1
Items: 
Size: 767156 Color: 2

Bin 2663: 232983 of cap free
Amount of items: 1
Items: 
Size: 767018 Color: 0

Bin 2664: 233053 of cap free
Amount of items: 1
Items: 
Size: 766948 Color: 0

Bin 2665: 233061 of cap free
Amount of items: 1
Items: 
Size: 766940 Color: 2

Bin 2666: 233076 of cap free
Amount of items: 1
Items: 
Size: 766925 Color: 2

Bin 2667: 233098 of cap free
Amount of items: 1
Items: 
Size: 766903 Color: 1

Bin 2668: 233165 of cap free
Amount of items: 1
Items: 
Size: 766836 Color: 4

Bin 2669: 233255 of cap free
Amount of items: 1
Items: 
Size: 766746 Color: 1

Bin 2670: 233510 of cap free
Amount of items: 1
Items: 
Size: 766491 Color: 4

Bin 2671: 233525 of cap free
Amount of items: 1
Items: 
Size: 766476 Color: 3

Bin 2672: 233529 of cap free
Amount of items: 1
Items: 
Size: 766472 Color: 4

Bin 2673: 233549 of cap free
Amount of items: 1
Items: 
Size: 766452 Color: 0

Bin 2674: 233675 of cap free
Amount of items: 1
Items: 
Size: 766326 Color: 0

Bin 2675: 233688 of cap free
Amount of items: 1
Items: 
Size: 766313 Color: 4

Bin 2676: 233712 of cap free
Amount of items: 1
Items: 
Size: 766289 Color: 1

Bin 2677: 233732 of cap free
Amount of items: 1
Items: 
Size: 766269 Color: 2

Bin 2678: 233849 of cap free
Amount of items: 1
Items: 
Size: 766152 Color: 4

Bin 2679: 233888 of cap free
Amount of items: 1
Items: 
Size: 766113 Color: 4

Bin 2680: 233892 of cap free
Amount of items: 1
Items: 
Size: 766109 Color: 1

Bin 2681: 233897 of cap free
Amount of items: 1
Items: 
Size: 766104 Color: 1

Bin 2682: 233929 of cap free
Amount of items: 1
Items: 
Size: 766072 Color: 0

Bin 2683: 234124 of cap free
Amount of items: 1
Items: 
Size: 765877 Color: 3

Bin 2684: 234164 of cap free
Amount of items: 1
Items: 
Size: 765837 Color: 3

Bin 2685: 234191 of cap free
Amount of items: 1
Items: 
Size: 765810 Color: 2

Bin 2686: 234348 of cap free
Amount of items: 1
Items: 
Size: 765653 Color: 0

Bin 2687: 234372 of cap free
Amount of items: 1
Items: 
Size: 765629 Color: 2

Bin 2688: 234466 of cap free
Amount of items: 1
Items: 
Size: 765535 Color: 4

Bin 2689: 234473 of cap free
Amount of items: 1
Items: 
Size: 765528 Color: 4

Bin 2690: 234524 of cap free
Amount of items: 1
Items: 
Size: 765477 Color: 0

Bin 2691: 234644 of cap free
Amount of items: 1
Items: 
Size: 765357 Color: 4

Bin 2692: 234660 of cap free
Amount of items: 1
Items: 
Size: 765341 Color: 4

Bin 2693: 234772 of cap free
Amount of items: 1
Items: 
Size: 765229 Color: 2

Bin 2694: 234854 of cap free
Amount of items: 1
Items: 
Size: 765147 Color: 3

Bin 2695: 234863 of cap free
Amount of items: 1
Items: 
Size: 765138 Color: 3

Bin 2696: 234979 of cap free
Amount of items: 1
Items: 
Size: 765022 Color: 2

Bin 2697: 235042 of cap free
Amount of items: 1
Items: 
Size: 764959 Color: 3

Bin 2698: 235064 of cap free
Amount of items: 1
Items: 
Size: 764937 Color: 1

Bin 2699: 235222 of cap free
Amount of items: 1
Items: 
Size: 764779 Color: 1

Bin 2700: 235236 of cap free
Amount of items: 1
Items: 
Size: 764765 Color: 4

Bin 2701: 235260 of cap free
Amount of items: 1
Items: 
Size: 764741 Color: 1

Bin 2702: 235353 of cap free
Amount of items: 1
Items: 
Size: 764648 Color: 2

Bin 2703: 235470 of cap free
Amount of items: 1
Items: 
Size: 764531 Color: 0

Bin 2704: 235500 of cap free
Amount of items: 1
Items: 
Size: 764501 Color: 3

Bin 2705: 235547 of cap free
Amount of items: 1
Items: 
Size: 764454 Color: 2

Bin 2706: 235619 of cap free
Amount of items: 1
Items: 
Size: 764382 Color: 0

Bin 2707: 235647 of cap free
Amount of items: 1
Items: 
Size: 764354 Color: 2

Bin 2708: 235656 of cap free
Amount of items: 1
Items: 
Size: 764345 Color: 0

Bin 2709: 235714 of cap free
Amount of items: 1
Items: 
Size: 764287 Color: 4

Bin 2710: 235770 of cap free
Amount of items: 1
Items: 
Size: 764231 Color: 1

Bin 2711: 235800 of cap free
Amount of items: 1
Items: 
Size: 764201 Color: 4

Bin 2712: 235801 of cap free
Amount of items: 1
Items: 
Size: 764200 Color: 2

Bin 2713: 236072 of cap free
Amount of items: 1
Items: 
Size: 763929 Color: 3

Bin 2714: 236075 of cap free
Amount of items: 1
Items: 
Size: 763926 Color: 3

Bin 2715: 236104 of cap free
Amount of items: 1
Items: 
Size: 763897 Color: 3

Bin 2716: 236111 of cap free
Amount of items: 1
Items: 
Size: 763890 Color: 4

Bin 2717: 236149 of cap free
Amount of items: 1
Items: 
Size: 763852 Color: 1

Bin 2718: 236329 of cap free
Amount of items: 1
Items: 
Size: 763672 Color: 1

Bin 2719: 236376 of cap free
Amount of items: 1
Items: 
Size: 763625 Color: 3

Bin 2720: 236416 of cap free
Amount of items: 1
Items: 
Size: 763585 Color: 1

Bin 2721: 236524 of cap free
Amount of items: 1
Items: 
Size: 763477 Color: 2

Bin 2722: 236548 of cap free
Amount of items: 1
Items: 
Size: 763453 Color: 1

Bin 2723: 236765 of cap free
Amount of items: 1
Items: 
Size: 763236 Color: 2

Bin 2724: 236805 of cap free
Amount of items: 1
Items: 
Size: 763196 Color: 0

Bin 2725: 236819 of cap free
Amount of items: 1
Items: 
Size: 763182 Color: 4

Bin 2726: 236843 of cap free
Amount of items: 1
Items: 
Size: 763158 Color: 4

Bin 2727: 236988 of cap free
Amount of items: 1
Items: 
Size: 763013 Color: 1

Bin 2728: 237081 of cap free
Amount of items: 1
Items: 
Size: 762920 Color: 3

Bin 2729: 237287 of cap free
Amount of items: 1
Items: 
Size: 762714 Color: 2

Bin 2730: 237429 of cap free
Amount of items: 1
Items: 
Size: 762572 Color: 4

Bin 2731: 237453 of cap free
Amount of items: 1
Items: 
Size: 762548 Color: 2

Bin 2732: 237536 of cap free
Amount of items: 1
Items: 
Size: 762465 Color: 3

Bin 2733: 237672 of cap free
Amount of items: 1
Items: 
Size: 762329 Color: 4

Bin 2734: 237783 of cap free
Amount of items: 1
Items: 
Size: 762218 Color: 4

Bin 2735: 237988 of cap free
Amount of items: 1
Items: 
Size: 762013 Color: 4

Bin 2736: 238004 of cap free
Amount of items: 1
Items: 
Size: 761997 Color: 2

Bin 2737: 238087 of cap free
Amount of items: 1
Items: 
Size: 761914 Color: 0

Bin 2738: 238140 of cap free
Amount of items: 1
Items: 
Size: 761861 Color: 4

Bin 2739: 238213 of cap free
Amount of items: 1
Items: 
Size: 761788 Color: 2

Bin 2740: 238319 of cap free
Amount of items: 1
Items: 
Size: 761682 Color: 0

Bin 2741: 238323 of cap free
Amount of items: 1
Items: 
Size: 761678 Color: 3

Bin 2742: 238324 of cap free
Amount of items: 1
Items: 
Size: 761677 Color: 3

Bin 2743: 238478 of cap free
Amount of items: 1
Items: 
Size: 761523 Color: 0

Bin 2744: 238518 of cap free
Amount of items: 1
Items: 
Size: 761483 Color: 4

Bin 2745: 238526 of cap free
Amount of items: 1
Items: 
Size: 761475 Color: 3

Bin 2746: 238605 of cap free
Amount of items: 1
Items: 
Size: 761396 Color: 1

Bin 2747: 238631 of cap free
Amount of items: 1
Items: 
Size: 761370 Color: 0

Bin 2748: 238634 of cap free
Amount of items: 1
Items: 
Size: 761367 Color: 2

Bin 2749: 238673 of cap free
Amount of items: 1
Items: 
Size: 761328 Color: 2

Bin 2750: 238690 of cap free
Amount of items: 1
Items: 
Size: 761311 Color: 1

Bin 2751: 238783 of cap free
Amount of items: 1
Items: 
Size: 761218 Color: 4

Bin 2752: 238828 of cap free
Amount of items: 1
Items: 
Size: 761173 Color: 2

Bin 2753: 238896 of cap free
Amount of items: 1
Items: 
Size: 761105 Color: 2

Bin 2754: 238909 of cap free
Amount of items: 1
Items: 
Size: 761092 Color: 0

Bin 2755: 238961 of cap free
Amount of items: 1
Items: 
Size: 761040 Color: 3

Bin 2756: 239012 of cap free
Amount of items: 1
Items: 
Size: 760989 Color: 2

Bin 2757: 239149 of cap free
Amount of items: 1
Items: 
Size: 760852 Color: 4

Bin 2758: 239272 of cap free
Amount of items: 1
Items: 
Size: 760729 Color: 2

Bin 2759: 239283 of cap free
Amount of items: 1
Items: 
Size: 760718 Color: 4

Bin 2760: 239371 of cap free
Amount of items: 1
Items: 
Size: 760630 Color: 2

Bin 2761: 239460 of cap free
Amount of items: 1
Items: 
Size: 760541 Color: 0

Bin 2762: 239558 of cap free
Amount of items: 1
Items: 
Size: 760443 Color: 3

Bin 2763: 239585 of cap free
Amount of items: 1
Items: 
Size: 760416 Color: 2

Bin 2764: 239750 of cap free
Amount of items: 1
Items: 
Size: 760251 Color: 2

Bin 2765: 239951 of cap free
Amount of items: 1
Items: 
Size: 760050 Color: 2

Bin 2766: 239983 of cap free
Amount of items: 1
Items: 
Size: 760018 Color: 4

Bin 2767: 240078 of cap free
Amount of items: 1
Items: 
Size: 759923 Color: 2

Bin 2768: 240097 of cap free
Amount of items: 1
Items: 
Size: 759904 Color: 1

Bin 2769: 240115 of cap free
Amount of items: 1
Items: 
Size: 759886 Color: 4

Bin 2770: 240260 of cap free
Amount of items: 1
Items: 
Size: 759741 Color: 0

Bin 2771: 240367 of cap free
Amount of items: 1
Items: 
Size: 759634 Color: 4

Bin 2772: 240442 of cap free
Amount of items: 1
Items: 
Size: 759559 Color: 0

Bin 2773: 240459 of cap free
Amount of items: 1
Items: 
Size: 759542 Color: 0

Bin 2774: 240479 of cap free
Amount of items: 1
Items: 
Size: 759522 Color: 1

Bin 2775: 240486 of cap free
Amount of items: 1
Items: 
Size: 759515 Color: 4

Bin 2776: 240850 of cap free
Amount of items: 1
Items: 
Size: 759151 Color: 3

Bin 2777: 240894 of cap free
Amount of items: 1
Items: 
Size: 759107 Color: 4

Bin 2778: 241028 of cap free
Amount of items: 1
Items: 
Size: 758973 Color: 0

Bin 2779: 241101 of cap free
Amount of items: 1
Items: 
Size: 758900 Color: 3

Bin 2780: 241120 of cap free
Amount of items: 1
Items: 
Size: 758881 Color: 0

Bin 2781: 241316 of cap free
Amount of items: 1
Items: 
Size: 758685 Color: 1

Bin 2782: 241382 of cap free
Amount of items: 1
Items: 
Size: 758619 Color: 3

Bin 2783: 241393 of cap free
Amount of items: 1
Items: 
Size: 758608 Color: 1

Bin 2784: 241473 of cap free
Amount of items: 1
Items: 
Size: 758528 Color: 4

Bin 2785: 241510 of cap free
Amount of items: 1
Items: 
Size: 758491 Color: 1

Bin 2786: 241521 of cap free
Amount of items: 1
Items: 
Size: 758480 Color: 4

Bin 2787: 241638 of cap free
Amount of items: 1
Items: 
Size: 758363 Color: 1

Bin 2788: 241752 of cap free
Amount of items: 1
Items: 
Size: 758249 Color: 2

Bin 2789: 241786 of cap free
Amount of items: 1
Items: 
Size: 758215 Color: 4

Bin 2790: 241810 of cap free
Amount of items: 1
Items: 
Size: 758191 Color: 3

Bin 2791: 241871 of cap free
Amount of items: 1
Items: 
Size: 758130 Color: 3

Bin 2792: 241905 of cap free
Amount of items: 1
Items: 
Size: 758096 Color: 2

Bin 2793: 241908 of cap free
Amount of items: 1
Items: 
Size: 758093 Color: 3

Bin 2794: 241989 of cap free
Amount of items: 1
Items: 
Size: 758012 Color: 2

Bin 2795: 242064 of cap free
Amount of items: 1
Items: 
Size: 757937 Color: 4

Bin 2796: 242131 of cap free
Amount of items: 1
Items: 
Size: 757870 Color: 4

Bin 2797: 242518 of cap free
Amount of items: 1
Items: 
Size: 757483 Color: 4

Bin 2798: 242620 of cap free
Amount of items: 1
Items: 
Size: 757381 Color: 0

Bin 2799: 242715 of cap free
Amount of items: 1
Items: 
Size: 757286 Color: 2

Bin 2800: 242737 of cap free
Amount of items: 1
Items: 
Size: 757264 Color: 0

Bin 2801: 242884 of cap free
Amount of items: 1
Items: 
Size: 757117 Color: 1

Bin 2802: 242884 of cap free
Amount of items: 1
Items: 
Size: 757117 Color: 1

Bin 2803: 242894 of cap free
Amount of items: 1
Items: 
Size: 757107 Color: 4

Bin 2804: 242990 of cap free
Amount of items: 1
Items: 
Size: 757011 Color: 3

Bin 2805: 243037 of cap free
Amount of items: 1
Items: 
Size: 756964 Color: 2

Bin 2806: 243144 of cap free
Amount of items: 1
Items: 
Size: 756857 Color: 3

Bin 2807: 243154 of cap free
Amount of items: 1
Items: 
Size: 756847 Color: 4

Bin 2808: 243246 of cap free
Amount of items: 1
Items: 
Size: 756755 Color: 1

Bin 2809: 243251 of cap free
Amount of items: 1
Items: 
Size: 756750 Color: 0

Bin 2810: 243332 of cap free
Amount of items: 1
Items: 
Size: 756669 Color: 0

Bin 2811: 243335 of cap free
Amount of items: 1
Items: 
Size: 756666 Color: 3

Bin 2812: 243403 of cap free
Amount of items: 1
Items: 
Size: 756598 Color: 2

Bin 2813: 243463 of cap free
Amount of items: 1
Items: 
Size: 756538 Color: 3

Bin 2814: 243481 of cap free
Amount of items: 1
Items: 
Size: 756520 Color: 2

Bin 2815: 243528 of cap free
Amount of items: 1
Items: 
Size: 756473 Color: 1

Bin 2816: 243859 of cap free
Amount of items: 1
Items: 
Size: 756142 Color: 1

Bin 2817: 243865 of cap free
Amount of items: 1
Items: 
Size: 756136 Color: 1

Bin 2818: 243882 of cap free
Amount of items: 1
Items: 
Size: 756119 Color: 1

Bin 2819: 243909 of cap free
Amount of items: 1
Items: 
Size: 756092 Color: 0

Bin 2820: 244033 of cap free
Amount of items: 1
Items: 
Size: 755968 Color: 0

Bin 2821: 244040 of cap free
Amount of items: 1
Items: 
Size: 755961 Color: 3

Bin 2822: 244092 of cap free
Amount of items: 1
Items: 
Size: 755909 Color: 3

Bin 2823: 244094 of cap free
Amount of items: 1
Items: 
Size: 755907 Color: 4

Bin 2824: 244150 of cap free
Amount of items: 1
Items: 
Size: 755851 Color: 2

Bin 2825: 244228 of cap free
Amount of items: 1
Items: 
Size: 755773 Color: 4

Bin 2826: 244238 of cap free
Amount of items: 1
Items: 
Size: 755763 Color: 4

Bin 2827: 244272 of cap free
Amount of items: 1
Items: 
Size: 755729 Color: 3

Bin 2828: 244294 of cap free
Amount of items: 1
Items: 
Size: 755707 Color: 1

Bin 2829: 244422 of cap free
Amount of items: 1
Items: 
Size: 755579 Color: 1

Bin 2830: 244523 of cap free
Amount of items: 1
Items: 
Size: 755478 Color: 4

Bin 2831: 244638 of cap free
Amount of items: 1
Items: 
Size: 755363 Color: 2

Bin 2832: 244642 of cap free
Amount of items: 1
Items: 
Size: 755359 Color: 4

Bin 2833: 244647 of cap free
Amount of items: 1
Items: 
Size: 755354 Color: 0

Bin 2834: 244679 of cap free
Amount of items: 1
Items: 
Size: 755322 Color: 0

Bin 2835: 244679 of cap free
Amount of items: 1
Items: 
Size: 755322 Color: 2

Bin 2836: 244751 of cap free
Amount of items: 1
Items: 
Size: 755250 Color: 4

Bin 2837: 244827 of cap free
Amount of items: 1
Items: 
Size: 755174 Color: 3

Bin 2838: 244880 of cap free
Amount of items: 1
Items: 
Size: 755121 Color: 0

Bin 2839: 244914 of cap free
Amount of items: 1
Items: 
Size: 755087 Color: 0

Bin 2840: 245017 of cap free
Amount of items: 1
Items: 
Size: 754984 Color: 2

Bin 2841: 245082 of cap free
Amount of items: 1
Items: 
Size: 754919 Color: 2

Bin 2842: 245113 of cap free
Amount of items: 1
Items: 
Size: 754888 Color: 4

Bin 2843: 245130 of cap free
Amount of items: 1
Items: 
Size: 754871 Color: 0

Bin 2844: 245137 of cap free
Amount of items: 1
Items: 
Size: 754864 Color: 4

Bin 2845: 245146 of cap free
Amount of items: 1
Items: 
Size: 754855 Color: 1

Bin 2846: 245164 of cap free
Amount of items: 1
Items: 
Size: 754837 Color: 4

Bin 2847: 245174 of cap free
Amount of items: 1
Items: 
Size: 754827 Color: 2

Bin 2848: 245203 of cap free
Amount of items: 1
Items: 
Size: 754798 Color: 3

Bin 2849: 245242 of cap free
Amount of items: 1
Items: 
Size: 754759 Color: 4

Bin 2850: 245257 of cap free
Amount of items: 1
Items: 
Size: 754744 Color: 4

Bin 2851: 245309 of cap free
Amount of items: 1
Items: 
Size: 754692 Color: 4

Bin 2852: 245417 of cap free
Amount of items: 1
Items: 
Size: 754584 Color: 3

Bin 2853: 245485 of cap free
Amount of items: 1
Items: 
Size: 754516 Color: 2

Bin 2854: 245494 of cap free
Amount of items: 1
Items: 
Size: 754507 Color: 4

Bin 2855: 245510 of cap free
Amount of items: 1
Items: 
Size: 754491 Color: 0

Bin 2856: 245567 of cap free
Amount of items: 1
Items: 
Size: 754434 Color: 1

Bin 2857: 245621 of cap free
Amount of items: 1
Items: 
Size: 754380 Color: 0

Bin 2858: 245674 of cap free
Amount of items: 1
Items: 
Size: 754327 Color: 4

Bin 2859: 245755 of cap free
Amount of items: 1
Items: 
Size: 754246 Color: 4

Bin 2860: 245857 of cap free
Amount of items: 1
Items: 
Size: 754144 Color: 0

Bin 2861: 246061 of cap free
Amount of items: 1
Items: 
Size: 753940 Color: 1

Bin 2862: 246094 of cap free
Amount of items: 1
Items: 
Size: 753907 Color: 0

Bin 2863: 246122 of cap free
Amount of items: 1
Items: 
Size: 753879 Color: 1

Bin 2864: 246141 of cap free
Amount of items: 1
Items: 
Size: 753860 Color: 0

Bin 2865: 246142 of cap free
Amount of items: 1
Items: 
Size: 753859 Color: 0

Bin 2866: 246291 of cap free
Amount of items: 1
Items: 
Size: 753710 Color: 2

Bin 2867: 246348 of cap free
Amount of items: 1
Items: 
Size: 753653 Color: 2

Bin 2868: 246433 of cap free
Amount of items: 1
Items: 
Size: 753568 Color: 2

Bin 2869: 246593 of cap free
Amount of items: 1
Items: 
Size: 753408 Color: 1

Bin 2870: 246626 of cap free
Amount of items: 1
Items: 
Size: 753375 Color: 1

Bin 2871: 246660 of cap free
Amount of items: 1
Items: 
Size: 753341 Color: 3

Bin 2872: 246686 of cap free
Amount of items: 1
Items: 
Size: 753315 Color: 1

Bin 2873: 246816 of cap free
Amount of items: 1
Items: 
Size: 753185 Color: 1

Bin 2874: 246925 of cap free
Amount of items: 1
Items: 
Size: 753076 Color: 4

Bin 2875: 247023 of cap free
Amount of items: 1
Items: 
Size: 752978 Color: 1

Bin 2876: 247036 of cap free
Amount of items: 1
Items: 
Size: 752965 Color: 2

Bin 2877: 247041 of cap free
Amount of items: 1
Items: 
Size: 752960 Color: 0

Bin 2878: 247098 of cap free
Amount of items: 1
Items: 
Size: 752903 Color: 1

Bin 2879: 247101 of cap free
Amount of items: 1
Items: 
Size: 752900 Color: 3

Bin 2880: 247161 of cap free
Amount of items: 1
Items: 
Size: 752840 Color: 1

Bin 2881: 247181 of cap free
Amount of items: 1
Items: 
Size: 752820 Color: 4

Bin 2882: 247200 of cap free
Amount of items: 1
Items: 
Size: 752801 Color: 3

Bin 2883: 247219 of cap free
Amount of items: 1
Items: 
Size: 752782 Color: 1

Bin 2884: 247245 of cap free
Amount of items: 1
Items: 
Size: 752756 Color: 4

Bin 2885: 247479 of cap free
Amount of items: 1
Items: 
Size: 752522 Color: 1

Bin 2886: 247663 of cap free
Amount of items: 1
Items: 
Size: 752338 Color: 1

Bin 2887: 247690 of cap free
Amount of items: 1
Items: 
Size: 752311 Color: 2

Bin 2888: 247768 of cap free
Amount of items: 1
Items: 
Size: 752233 Color: 3

Bin 2889: 247834 of cap free
Amount of items: 1
Items: 
Size: 752167 Color: 3

Bin 2890: 247889 of cap free
Amount of items: 1
Items: 
Size: 752112 Color: 1

Bin 2891: 247900 of cap free
Amount of items: 1
Items: 
Size: 752101 Color: 2

Bin 2892: 247927 of cap free
Amount of items: 1
Items: 
Size: 752074 Color: 4

Bin 2893: 247984 of cap free
Amount of items: 1
Items: 
Size: 752017 Color: 3

Bin 2894: 247989 of cap free
Amount of items: 1
Items: 
Size: 752012 Color: 1

Bin 2895: 248016 of cap free
Amount of items: 1
Items: 
Size: 751985 Color: 0

Bin 2896: 248057 of cap free
Amount of items: 1
Items: 
Size: 751944 Color: 4

Bin 2897: 248074 of cap free
Amount of items: 1
Items: 
Size: 751927 Color: 4

Bin 2898: 248200 of cap free
Amount of items: 1
Items: 
Size: 751801 Color: 4

Bin 2899: 248203 of cap free
Amount of items: 1
Items: 
Size: 751798 Color: 2

Bin 2900: 248209 of cap free
Amount of items: 1
Items: 
Size: 751792 Color: 4

Bin 2901: 248275 of cap free
Amount of items: 1
Items: 
Size: 751726 Color: 3

Bin 2902: 248283 of cap free
Amount of items: 1
Items: 
Size: 751718 Color: 1

Bin 2903: 248298 of cap free
Amount of items: 1
Items: 
Size: 751703 Color: 3

Bin 2904: 248340 of cap free
Amount of items: 1
Items: 
Size: 751661 Color: 0

Bin 2905: 248354 of cap free
Amount of items: 1
Items: 
Size: 751647 Color: 0

Bin 2906: 248513 of cap free
Amount of items: 1
Items: 
Size: 751488 Color: 0

Bin 2907: 248658 of cap free
Amount of items: 1
Items: 
Size: 751343 Color: 1

Bin 2908: 248749 of cap free
Amount of items: 1
Items: 
Size: 751252 Color: 4

Bin 2909: 248783 of cap free
Amount of items: 1
Items: 
Size: 751218 Color: 3

Bin 2910: 248858 of cap free
Amount of items: 1
Items: 
Size: 751143 Color: 4

Bin 2911: 248869 of cap free
Amount of items: 1
Items: 
Size: 751132 Color: 2

Bin 2912: 248869 of cap free
Amount of items: 1
Items: 
Size: 751132 Color: 4

Bin 2913: 248879 of cap free
Amount of items: 1
Items: 
Size: 751122 Color: 1

Bin 2914: 248916 of cap free
Amount of items: 1
Items: 
Size: 751085 Color: 4

Bin 2915: 249025 of cap free
Amount of items: 1
Items: 
Size: 750976 Color: 1

Bin 2916: 249060 of cap free
Amount of items: 1
Items: 
Size: 750941 Color: 3

Bin 2917: 249119 of cap free
Amount of items: 1
Items: 
Size: 750882 Color: 4

Bin 2918: 249134 of cap free
Amount of items: 1
Items: 
Size: 750867 Color: 2

Bin 2919: 249143 of cap free
Amount of items: 1
Items: 
Size: 750858 Color: 4

Bin 2920: 249214 of cap free
Amount of items: 1
Items: 
Size: 750787 Color: 2

Bin 2921: 249223 of cap free
Amount of items: 1
Items: 
Size: 750778 Color: 0

Bin 2922: 249239 of cap free
Amount of items: 1
Items: 
Size: 750762 Color: 4

Bin 2923: 249323 of cap free
Amount of items: 1
Items: 
Size: 750678 Color: 3

Bin 2924: 249407 of cap free
Amount of items: 1
Items: 
Size: 750594 Color: 2

Bin 2925: 249464 of cap free
Amount of items: 1
Items: 
Size: 750537 Color: 4

Bin 2926: 249478 of cap free
Amount of items: 1
Items: 
Size: 750523 Color: 1

Bin 2927: 249508 of cap free
Amount of items: 1
Items: 
Size: 750493 Color: 2

Bin 2928: 249531 of cap free
Amount of items: 1
Items: 
Size: 750470 Color: 2

Bin 2929: 249540 of cap free
Amount of items: 1
Items: 
Size: 750461 Color: 4

Bin 2930: 249647 of cap free
Amount of items: 1
Items: 
Size: 750354 Color: 2

Bin 2931: 249866 of cap free
Amount of items: 1
Items: 
Size: 750135 Color: 2

Bin 2932: 249902 of cap free
Amount of items: 1
Items: 
Size: 750099 Color: 4

Bin 2933: 249909 of cap free
Amount of items: 1
Items: 
Size: 750092 Color: 0

Bin 2934: 249976 of cap free
Amount of items: 1
Items: 
Size: 750025 Color: 3

Bin 2935: 250030 of cap free
Amount of items: 1
Items: 
Size: 749971 Color: 1

Bin 2936: 250033 of cap free
Amount of items: 1
Items: 
Size: 749968 Color: 4

Bin 2937: 250109 of cap free
Amount of items: 1
Items: 
Size: 749892 Color: 2

Bin 2938: 250125 of cap free
Amount of items: 1
Items: 
Size: 749876 Color: 4

Bin 2939: 250237 of cap free
Amount of items: 1
Items: 
Size: 749764 Color: 3

Bin 2940: 250243 of cap free
Amount of items: 1
Items: 
Size: 749758 Color: 1

Bin 2941: 250314 of cap free
Amount of items: 1
Items: 
Size: 749687 Color: 3

Bin 2942: 250344 of cap free
Amount of items: 1
Items: 
Size: 749657 Color: 2

Bin 2943: 250515 of cap free
Amount of items: 1
Items: 
Size: 749486 Color: 0

Bin 2944: 250607 of cap free
Amount of items: 1
Items: 
Size: 749394 Color: 2

Bin 2945: 250609 of cap free
Amount of items: 1
Items: 
Size: 749392 Color: 3

Bin 2946: 250611 of cap free
Amount of items: 1
Items: 
Size: 749390 Color: 2

Bin 2947: 250891 of cap free
Amount of items: 1
Items: 
Size: 749110 Color: 2

Bin 2948: 250944 of cap free
Amount of items: 1
Items: 
Size: 749057 Color: 0

Bin 2949: 250960 of cap free
Amount of items: 1
Items: 
Size: 749041 Color: 2

Bin 2950: 250964 of cap free
Amount of items: 1
Items: 
Size: 749037 Color: 2

Bin 2951: 250988 of cap free
Amount of items: 1
Items: 
Size: 749013 Color: 1

Bin 2952: 251010 of cap free
Amount of items: 1
Items: 
Size: 748991 Color: 3

Bin 2953: 251125 of cap free
Amount of items: 1
Items: 
Size: 748876 Color: 3

Bin 2954: 251293 of cap free
Amount of items: 1
Items: 
Size: 748708 Color: 2

Bin 2955: 251298 of cap free
Amount of items: 1
Items: 
Size: 748703 Color: 0

Bin 2956: 251446 of cap free
Amount of items: 1
Items: 
Size: 748555 Color: 4

Bin 2957: 251482 of cap free
Amount of items: 1
Items: 
Size: 748519 Color: 4

Bin 2958: 251507 of cap free
Amount of items: 1
Items: 
Size: 748494 Color: 3

Bin 2959: 251801 of cap free
Amount of items: 1
Items: 
Size: 748200 Color: 0

Bin 2960: 251901 of cap free
Amount of items: 1
Items: 
Size: 748100 Color: 1

Bin 2961: 251990 of cap free
Amount of items: 1
Items: 
Size: 748011 Color: 3

Bin 2962: 252000 of cap free
Amount of items: 1
Items: 
Size: 748001 Color: 1

Bin 2963: 252098 of cap free
Amount of items: 1
Items: 
Size: 747903 Color: 4

Bin 2964: 252200 of cap free
Amount of items: 1
Items: 
Size: 747801 Color: 0

Bin 2965: 252218 of cap free
Amount of items: 1
Items: 
Size: 747783 Color: 0

Bin 2966: 252244 of cap free
Amount of items: 1
Items: 
Size: 747757 Color: 1

Bin 2967: 252281 of cap free
Amount of items: 1
Items: 
Size: 747720 Color: 4

Bin 2968: 252349 of cap free
Amount of items: 1
Items: 
Size: 747652 Color: 2

Bin 2969: 252667 of cap free
Amount of items: 1
Items: 
Size: 747334 Color: 1

Bin 2970: 252715 of cap free
Amount of items: 1
Items: 
Size: 747286 Color: 4

Bin 2971: 252723 of cap free
Amount of items: 1
Items: 
Size: 747278 Color: 3

Bin 2972: 252734 of cap free
Amount of items: 1
Items: 
Size: 747267 Color: 1

Bin 2973: 252831 of cap free
Amount of items: 1
Items: 
Size: 747170 Color: 0

Bin 2974: 252833 of cap free
Amount of items: 1
Items: 
Size: 747168 Color: 3

Bin 2975: 252980 of cap free
Amount of items: 1
Items: 
Size: 747021 Color: 2

Bin 2976: 253419 of cap free
Amount of items: 1
Items: 
Size: 746582 Color: 0

Bin 2977: 253436 of cap free
Amount of items: 1
Items: 
Size: 746565 Color: 1

Bin 2978: 253539 of cap free
Amount of items: 1
Items: 
Size: 746462 Color: 3

Bin 2979: 253599 of cap free
Amount of items: 1
Items: 
Size: 746402 Color: 0

Bin 2980: 253668 of cap free
Amount of items: 1
Items: 
Size: 746333 Color: 2

Bin 2981: 253688 of cap free
Amount of items: 1
Items: 
Size: 746313 Color: 2

Bin 2982: 253757 of cap free
Amount of items: 1
Items: 
Size: 746244 Color: 2

Bin 2983: 253831 of cap free
Amount of items: 1
Items: 
Size: 746170 Color: 4

Bin 2984: 254062 of cap free
Amount of items: 1
Items: 
Size: 745939 Color: 4

Bin 2985: 254132 of cap free
Amount of items: 1
Items: 
Size: 745869 Color: 2

Bin 2986: 254148 of cap free
Amount of items: 1
Items: 
Size: 745853 Color: 3

Bin 2987: 254166 of cap free
Amount of items: 1
Items: 
Size: 745835 Color: 2

Bin 2988: 254288 of cap free
Amount of items: 1
Items: 
Size: 745713 Color: 4

Bin 2989: 254296 of cap free
Amount of items: 1
Items: 
Size: 745705 Color: 1

Bin 2990: 254411 of cap free
Amount of items: 1
Items: 
Size: 745590 Color: 4

Bin 2991: 254445 of cap free
Amount of items: 1
Items: 
Size: 745556 Color: 3

Bin 2992: 254457 of cap free
Amount of items: 1
Items: 
Size: 745544 Color: 1

Bin 2993: 254595 of cap free
Amount of items: 1
Items: 
Size: 745406 Color: 1

Bin 2994: 254628 of cap free
Amount of items: 1
Items: 
Size: 745373 Color: 2

Bin 2995: 254864 of cap free
Amount of items: 1
Items: 
Size: 745137 Color: 2

Bin 2996: 254974 of cap free
Amount of items: 1
Items: 
Size: 745027 Color: 3

Bin 2997: 254976 of cap free
Amount of items: 1
Items: 
Size: 745025 Color: 0

Bin 2998: 254998 of cap free
Amount of items: 1
Items: 
Size: 745003 Color: 1

Bin 2999: 255103 of cap free
Amount of items: 1
Items: 
Size: 744898 Color: 4

Bin 3000: 255383 of cap free
Amount of items: 1
Items: 
Size: 744618 Color: 0

Bin 3001: 255418 of cap free
Amount of items: 1
Items: 
Size: 744583 Color: 3

Bin 3002: 255457 of cap free
Amount of items: 1
Items: 
Size: 744544 Color: 0

Bin 3003: 255476 of cap free
Amount of items: 1
Items: 
Size: 744525 Color: 0

Bin 3004: 255526 of cap free
Amount of items: 1
Items: 
Size: 744475 Color: 0

Bin 3005: 255635 of cap free
Amount of items: 1
Items: 
Size: 744366 Color: 4

Bin 3006: 255647 of cap free
Amount of items: 1
Items: 
Size: 744354 Color: 3

Bin 3007: 255743 of cap free
Amount of items: 1
Items: 
Size: 744258 Color: 3

Bin 3008: 255764 of cap free
Amount of items: 1
Items: 
Size: 744237 Color: 1

Bin 3009: 255803 of cap free
Amount of items: 1
Items: 
Size: 744198 Color: 2

Bin 3010: 255822 of cap free
Amount of items: 1
Items: 
Size: 744179 Color: 4

Bin 3011: 255873 of cap free
Amount of items: 1
Items: 
Size: 744128 Color: 2

Bin 3012: 255920 of cap free
Amount of items: 1
Items: 
Size: 744081 Color: 4

Bin 3013: 255946 of cap free
Amount of items: 1
Items: 
Size: 744055 Color: 2

Bin 3014: 256027 of cap free
Amount of items: 1
Items: 
Size: 743974 Color: 0

Bin 3015: 256043 of cap free
Amount of items: 1
Items: 
Size: 743958 Color: 4

Bin 3016: 256053 of cap free
Amount of items: 1
Items: 
Size: 743948 Color: 2

Bin 3017: 256092 of cap free
Amount of items: 1
Items: 
Size: 743909 Color: 4

Bin 3018: 256212 of cap free
Amount of items: 1
Items: 
Size: 743789 Color: 1

Bin 3019: 256214 of cap free
Amount of items: 1
Items: 
Size: 743787 Color: 2

Bin 3020: 256352 of cap free
Amount of items: 1
Items: 
Size: 743649 Color: 0

Bin 3021: 256428 of cap free
Amount of items: 1
Items: 
Size: 743573 Color: 1

Bin 3022: 256458 of cap free
Amount of items: 1
Items: 
Size: 743543 Color: 3

Bin 3023: 256461 of cap free
Amount of items: 1
Items: 
Size: 743540 Color: 4

Bin 3024: 256646 of cap free
Amount of items: 1
Items: 
Size: 743355 Color: 1

Bin 3025: 256680 of cap free
Amount of items: 1
Items: 
Size: 743321 Color: 2

Bin 3026: 256745 of cap free
Amount of items: 1
Items: 
Size: 743256 Color: 3

Bin 3027: 256813 of cap free
Amount of items: 1
Items: 
Size: 743188 Color: 0

Bin 3028: 256887 of cap free
Amount of items: 1
Items: 
Size: 743114 Color: 3

Bin 3029: 257156 of cap free
Amount of items: 1
Items: 
Size: 742845 Color: 4

Bin 3030: 257250 of cap free
Amount of items: 1
Items: 
Size: 742751 Color: 4

Bin 3031: 257288 of cap free
Amount of items: 1
Items: 
Size: 742713 Color: 2

Bin 3032: 257309 of cap free
Amount of items: 1
Items: 
Size: 742692 Color: 3

Bin 3033: 257369 of cap free
Amount of items: 1
Items: 
Size: 742632 Color: 3

Bin 3034: 257474 of cap free
Amount of items: 1
Items: 
Size: 742527 Color: 2

Bin 3035: 257505 of cap free
Amount of items: 1
Items: 
Size: 742496 Color: 3

Bin 3036: 257682 of cap free
Amount of items: 1
Items: 
Size: 742319 Color: 2

Bin 3037: 257774 of cap free
Amount of items: 1
Items: 
Size: 742227 Color: 4

Bin 3038: 257841 of cap free
Amount of items: 1
Items: 
Size: 742160 Color: 3

Bin 3039: 257943 of cap free
Amount of items: 1
Items: 
Size: 742058 Color: 3

Bin 3040: 258044 of cap free
Amount of items: 1
Items: 
Size: 741957 Color: 1

Bin 3041: 258050 of cap free
Amount of items: 1
Items: 
Size: 741951 Color: 3

Bin 3042: 258061 of cap free
Amount of items: 1
Items: 
Size: 741940 Color: 3

Bin 3043: 258121 of cap free
Amount of items: 1
Items: 
Size: 741880 Color: 4

Bin 3044: 258156 of cap free
Amount of items: 1
Items: 
Size: 741845 Color: 2

Bin 3045: 258327 of cap free
Amount of items: 1
Items: 
Size: 741674 Color: 2

Bin 3046: 258398 of cap free
Amount of items: 1
Items: 
Size: 741603 Color: 2

Bin 3047: 258444 of cap free
Amount of items: 1
Items: 
Size: 741557 Color: 2

Bin 3048: 258519 of cap free
Amount of items: 1
Items: 
Size: 741482 Color: 4

Bin 3049: 258532 of cap free
Amount of items: 1
Items: 
Size: 741469 Color: 0

Bin 3050: 258569 of cap free
Amount of items: 1
Items: 
Size: 741432 Color: 0

Bin 3051: 258618 of cap free
Amount of items: 1
Items: 
Size: 741383 Color: 3

Bin 3052: 258863 of cap free
Amount of items: 1
Items: 
Size: 741138 Color: 3

Bin 3053: 259005 of cap free
Amount of items: 1
Items: 
Size: 740996 Color: 1

Bin 3054: 259061 of cap free
Amount of items: 1
Items: 
Size: 740940 Color: 0

Bin 3055: 259144 of cap free
Amount of items: 1
Items: 
Size: 740857 Color: 2

Bin 3056: 259188 of cap free
Amount of items: 1
Items: 
Size: 740813 Color: 0

Bin 3057: 259316 of cap free
Amount of items: 1
Items: 
Size: 740685 Color: 3

Bin 3058: 259402 of cap free
Amount of items: 1
Items: 
Size: 740599 Color: 0

Bin 3059: 259447 of cap free
Amount of items: 1
Items: 
Size: 740554 Color: 4

Bin 3060: 259547 of cap free
Amount of items: 1
Items: 
Size: 740454 Color: 4

Bin 3061: 259716 of cap free
Amount of items: 1
Items: 
Size: 740285 Color: 4

Bin 3062: 259812 of cap free
Amount of items: 1
Items: 
Size: 740189 Color: 0

Bin 3063: 259836 of cap free
Amount of items: 1
Items: 
Size: 740165 Color: 4

Bin 3064: 259838 of cap free
Amount of items: 1
Items: 
Size: 740163 Color: 4

Bin 3065: 259845 of cap free
Amount of items: 1
Items: 
Size: 740156 Color: 0

Bin 3066: 259880 of cap free
Amount of items: 1
Items: 
Size: 740121 Color: 2

Bin 3067: 260111 of cap free
Amount of items: 1
Items: 
Size: 739890 Color: 2

Bin 3068: 260233 of cap free
Amount of items: 1
Items: 
Size: 739768 Color: 1

Bin 3069: 260610 of cap free
Amount of items: 1
Items: 
Size: 739391 Color: 0

Bin 3070: 260619 of cap free
Amount of items: 1
Items: 
Size: 739382 Color: 0

Bin 3071: 260650 of cap free
Amount of items: 1
Items: 
Size: 739351 Color: 1

Bin 3072: 260651 of cap free
Amount of items: 1
Items: 
Size: 739350 Color: 3

Bin 3073: 260704 of cap free
Amount of items: 1
Items: 
Size: 739297 Color: 4

Bin 3074: 260708 of cap free
Amount of items: 1
Items: 
Size: 739293 Color: 3

Bin 3075: 260787 of cap free
Amount of items: 1
Items: 
Size: 739214 Color: 3

Bin 3076: 260895 of cap free
Amount of items: 1
Items: 
Size: 739106 Color: 2

Bin 3077: 260905 of cap free
Amount of items: 1
Items: 
Size: 739096 Color: 3

Bin 3078: 260914 of cap free
Amount of items: 1
Items: 
Size: 739087 Color: 0

Bin 3079: 261054 of cap free
Amount of items: 1
Items: 
Size: 738947 Color: 3

Bin 3080: 261071 of cap free
Amount of items: 1
Items: 
Size: 738930 Color: 1

Bin 3081: 261089 of cap free
Amount of items: 1
Items: 
Size: 738912 Color: 1

Bin 3082: 261136 of cap free
Amount of items: 1
Items: 
Size: 738865 Color: 0

Bin 3083: 261270 of cap free
Amount of items: 1
Items: 
Size: 738731 Color: 3

Bin 3084: 261273 of cap free
Amount of items: 1
Items: 
Size: 738728 Color: 4

Bin 3085: 261414 of cap free
Amount of items: 1
Items: 
Size: 738587 Color: 4

Bin 3086: 261447 of cap free
Amount of items: 1
Items: 
Size: 738554 Color: 2

Bin 3087: 261465 of cap free
Amount of items: 1
Items: 
Size: 738536 Color: 3

Bin 3088: 261470 of cap free
Amount of items: 1
Items: 
Size: 738531 Color: 1

Bin 3089: 261472 of cap free
Amount of items: 1
Items: 
Size: 738529 Color: 0

Bin 3090: 261484 of cap free
Amount of items: 1
Items: 
Size: 738517 Color: 3

Bin 3091: 261599 of cap free
Amount of items: 1
Items: 
Size: 738402 Color: 0

Bin 3092: 261695 of cap free
Amount of items: 1
Items: 
Size: 738306 Color: 2

Bin 3093: 261748 of cap free
Amount of items: 1
Items: 
Size: 738253 Color: 3

Bin 3094: 261774 of cap free
Amount of items: 1
Items: 
Size: 738227 Color: 4

Bin 3095: 261779 of cap free
Amount of items: 1
Items: 
Size: 738222 Color: 2

Bin 3096: 261785 of cap free
Amount of items: 1
Items: 
Size: 738216 Color: 1

Bin 3097: 261827 of cap free
Amount of items: 1
Items: 
Size: 738174 Color: 2

Bin 3098: 261875 of cap free
Amount of items: 1
Items: 
Size: 738126 Color: 3

Bin 3099: 261879 of cap free
Amount of items: 1
Items: 
Size: 738122 Color: 3

Bin 3100: 261905 of cap free
Amount of items: 1
Items: 
Size: 738096 Color: 1

Bin 3101: 261956 of cap free
Amount of items: 1
Items: 
Size: 738045 Color: 0

Bin 3102: 262065 of cap free
Amount of items: 1
Items: 
Size: 737936 Color: 4

Bin 3103: 262068 of cap free
Amount of items: 1
Items: 
Size: 737933 Color: 3

Bin 3104: 262143 of cap free
Amount of items: 1
Items: 
Size: 737858 Color: 2

Bin 3105: 262202 of cap free
Amount of items: 1
Items: 
Size: 737799 Color: 1

Bin 3106: 262221 of cap free
Amount of items: 1
Items: 
Size: 737780 Color: 3

Bin 3107: 262251 of cap free
Amount of items: 1
Items: 
Size: 737750 Color: 1

Bin 3108: 262266 of cap free
Amount of items: 1
Items: 
Size: 737735 Color: 3

Bin 3109: 262291 of cap free
Amount of items: 1
Items: 
Size: 737710 Color: 1

Bin 3110: 262347 of cap free
Amount of items: 1
Items: 
Size: 737654 Color: 0

Bin 3111: 262417 of cap free
Amount of items: 1
Items: 
Size: 737584 Color: 2

Bin 3112: 262420 of cap free
Amount of items: 1
Items: 
Size: 737581 Color: 4

Bin 3113: 262674 of cap free
Amount of items: 1
Items: 
Size: 737327 Color: 3

Bin 3114: 262755 of cap free
Amount of items: 1
Items: 
Size: 737246 Color: 2

Bin 3115: 262839 of cap free
Amount of items: 1
Items: 
Size: 737162 Color: 2

Bin 3116: 262943 of cap free
Amount of items: 1
Items: 
Size: 737058 Color: 1

Bin 3117: 263078 of cap free
Amount of items: 1
Items: 
Size: 736923 Color: 0

Bin 3118: 263081 of cap free
Amount of items: 1
Items: 
Size: 736920 Color: 4

Bin 3119: 263187 of cap free
Amount of items: 1
Items: 
Size: 736814 Color: 0

Bin 3120: 263203 of cap free
Amount of items: 1
Items: 
Size: 736798 Color: 3

Bin 3121: 263230 of cap free
Amount of items: 1
Items: 
Size: 736771 Color: 2

Bin 3122: 263240 of cap free
Amount of items: 1
Items: 
Size: 736761 Color: 3

Bin 3123: 263254 of cap free
Amount of items: 1
Items: 
Size: 736747 Color: 3

Bin 3124: 263293 of cap free
Amount of items: 1
Items: 
Size: 736708 Color: 2

Bin 3125: 263364 of cap free
Amount of items: 1
Items: 
Size: 736637 Color: 0

Bin 3126: 263523 of cap free
Amount of items: 1
Items: 
Size: 736478 Color: 4

Bin 3127: 263530 of cap free
Amount of items: 1
Items: 
Size: 736471 Color: 4

Bin 3128: 263585 of cap free
Amount of items: 1
Items: 
Size: 736416 Color: 2

Bin 3129: 263828 of cap free
Amount of items: 1
Items: 
Size: 736173 Color: 3

Bin 3130: 263860 of cap free
Amount of items: 1
Items: 
Size: 736141 Color: 3

Bin 3131: 263894 of cap free
Amount of items: 1
Items: 
Size: 736107 Color: 1

Bin 3132: 263983 of cap free
Amount of items: 1
Items: 
Size: 736018 Color: 3

Bin 3133: 264138 of cap free
Amount of items: 1
Items: 
Size: 735863 Color: 0

Bin 3134: 264178 of cap free
Amount of items: 1
Items: 
Size: 735823 Color: 3

Bin 3135: 264335 of cap free
Amount of items: 1
Items: 
Size: 735666 Color: 3

Bin 3136: 264486 of cap free
Amount of items: 1
Items: 
Size: 735515 Color: 2

Bin 3137: 264490 of cap free
Amount of items: 1
Items: 
Size: 735511 Color: 3

Bin 3138: 264537 of cap free
Amount of items: 1
Items: 
Size: 735464 Color: 2

Bin 3139: 264586 of cap free
Amount of items: 1
Items: 
Size: 735415 Color: 4

Bin 3140: 264639 of cap free
Amount of items: 1
Items: 
Size: 735362 Color: 0

Bin 3141: 264718 of cap free
Amount of items: 1
Items: 
Size: 735283 Color: 0

Bin 3142: 264848 of cap free
Amount of items: 1
Items: 
Size: 735153 Color: 0

Bin 3143: 264973 of cap free
Amount of items: 1
Items: 
Size: 735028 Color: 4

Bin 3144: 265032 of cap free
Amount of items: 1
Items: 
Size: 734969 Color: 1

Bin 3145: 265068 of cap free
Amount of items: 1
Items: 
Size: 734933 Color: 0

Bin 3146: 265129 of cap free
Amount of items: 1
Items: 
Size: 734872 Color: 3

Bin 3147: 265148 of cap free
Amount of items: 1
Items: 
Size: 734853 Color: 2

Bin 3148: 265370 of cap free
Amount of items: 1
Items: 
Size: 734631 Color: 4

Bin 3149: 265408 of cap free
Amount of items: 1
Items: 
Size: 734593 Color: 3

Bin 3150: 265447 of cap free
Amount of items: 1
Items: 
Size: 734554 Color: 0

Bin 3151: 265586 of cap free
Amount of items: 1
Items: 
Size: 734415 Color: 3

Bin 3152: 265615 of cap free
Amount of items: 1
Items: 
Size: 734386 Color: 3

Bin 3153: 265868 of cap free
Amount of items: 1
Items: 
Size: 734133 Color: 4

Bin 3154: 265952 of cap free
Amount of items: 1
Items: 
Size: 734049 Color: 1

Bin 3155: 266141 of cap free
Amount of items: 1
Items: 
Size: 733860 Color: 3

Bin 3156: 266224 of cap free
Amount of items: 1
Items: 
Size: 733777 Color: 0

Bin 3157: 266288 of cap free
Amount of items: 1
Items: 
Size: 733713 Color: 2

Bin 3158: 266420 of cap free
Amount of items: 1
Items: 
Size: 733581 Color: 0

Bin 3159: 266425 of cap free
Amount of items: 1
Items: 
Size: 733576 Color: 0

Bin 3160: 266466 of cap free
Amount of items: 1
Items: 
Size: 733535 Color: 3

Bin 3161: 266519 of cap free
Amount of items: 1
Items: 
Size: 733482 Color: 4

Bin 3162: 266590 of cap free
Amount of items: 1
Items: 
Size: 733411 Color: 1

Bin 3163: 266609 of cap free
Amount of items: 1
Items: 
Size: 733392 Color: 2

Bin 3164: 266618 of cap free
Amount of items: 1
Items: 
Size: 733383 Color: 4

Bin 3165: 266643 of cap free
Amount of items: 1
Items: 
Size: 733358 Color: 3

Bin 3166: 266695 of cap free
Amount of items: 1
Items: 
Size: 733306 Color: 3

Bin 3167: 266776 of cap free
Amount of items: 1
Items: 
Size: 733225 Color: 4

Bin 3168: 266862 of cap free
Amount of items: 1
Items: 
Size: 733139 Color: 3

Bin 3169: 266870 of cap free
Amount of items: 1
Items: 
Size: 733131 Color: 4

Bin 3170: 266963 of cap free
Amount of items: 1
Items: 
Size: 733038 Color: 4

Bin 3171: 267169 of cap free
Amount of items: 1
Items: 
Size: 732832 Color: 2

Bin 3172: 267190 of cap free
Amount of items: 1
Items: 
Size: 732811 Color: 3

Bin 3173: 267248 of cap free
Amount of items: 1
Items: 
Size: 732753 Color: 3

Bin 3174: 267265 of cap free
Amount of items: 1
Items: 
Size: 732736 Color: 2

Bin 3175: 267277 of cap free
Amount of items: 1
Items: 
Size: 732724 Color: 1

Bin 3176: 267279 of cap free
Amount of items: 1
Items: 
Size: 732722 Color: 2

Bin 3177: 267293 of cap free
Amount of items: 1
Items: 
Size: 732708 Color: 2

Bin 3178: 267382 of cap free
Amount of items: 1
Items: 
Size: 732619 Color: 1

Bin 3179: 267453 of cap free
Amount of items: 1
Items: 
Size: 732548 Color: 3

Bin 3180: 267520 of cap free
Amount of items: 1
Items: 
Size: 732481 Color: 1

Bin 3181: 267524 of cap free
Amount of items: 1
Items: 
Size: 732477 Color: 1

Bin 3182: 267611 of cap free
Amount of items: 1
Items: 
Size: 732390 Color: 1

Bin 3183: 267649 of cap free
Amount of items: 1
Items: 
Size: 732352 Color: 3

Bin 3184: 267671 of cap free
Amount of items: 1
Items: 
Size: 732330 Color: 0

Bin 3185: 267702 of cap free
Amount of items: 1
Items: 
Size: 732299 Color: 2

Bin 3186: 267806 of cap free
Amount of items: 1
Items: 
Size: 732195 Color: 1

Bin 3187: 267829 of cap free
Amount of items: 1
Items: 
Size: 732172 Color: 0

Bin 3188: 267837 of cap free
Amount of items: 1
Items: 
Size: 732164 Color: 1

Bin 3189: 267953 of cap free
Amount of items: 1
Items: 
Size: 732048 Color: 3

Bin 3190: 267977 of cap free
Amount of items: 1
Items: 
Size: 732024 Color: 3

Bin 3191: 268032 of cap free
Amount of items: 1
Items: 
Size: 731969 Color: 0

Bin 3192: 268054 of cap free
Amount of items: 1
Items: 
Size: 731947 Color: 3

Bin 3193: 268087 of cap free
Amount of items: 1
Items: 
Size: 731914 Color: 1

Bin 3194: 268165 of cap free
Amount of items: 1
Items: 
Size: 731836 Color: 4

Bin 3195: 268169 of cap free
Amount of items: 1
Items: 
Size: 731832 Color: 3

Bin 3196: 268251 of cap free
Amount of items: 1
Items: 
Size: 731750 Color: 3

Bin 3197: 268273 of cap free
Amount of items: 1
Items: 
Size: 731728 Color: 3

Bin 3198: 268305 of cap free
Amount of items: 1
Items: 
Size: 731696 Color: 4

Bin 3199: 268312 of cap free
Amount of items: 1
Items: 
Size: 731689 Color: 2

Bin 3200: 268408 of cap free
Amount of items: 1
Items: 
Size: 731593 Color: 0

Bin 3201: 268477 of cap free
Amount of items: 1
Items: 
Size: 731524 Color: 2

Bin 3202: 268762 of cap free
Amount of items: 1
Items: 
Size: 731239 Color: 3

Bin 3203: 268808 of cap free
Amount of items: 1
Items: 
Size: 731193 Color: 2

Bin 3204: 268823 of cap free
Amount of items: 1
Items: 
Size: 731178 Color: 2

Bin 3205: 268934 of cap free
Amount of items: 1
Items: 
Size: 731067 Color: 1

Bin 3206: 268974 of cap free
Amount of items: 1
Items: 
Size: 731027 Color: 4

Bin 3207: 269015 of cap free
Amount of items: 1
Items: 
Size: 730986 Color: 1

Bin 3208: 269250 of cap free
Amount of items: 1
Items: 
Size: 730751 Color: 1

Bin 3209: 269307 of cap free
Amount of items: 1
Items: 
Size: 730694 Color: 0

Bin 3210: 269344 of cap free
Amount of items: 1
Items: 
Size: 730657 Color: 1

Bin 3211: 269433 of cap free
Amount of items: 1
Items: 
Size: 730568 Color: 4

Bin 3212: 269469 of cap free
Amount of items: 1
Items: 
Size: 730532 Color: 0

Bin 3213: 269594 of cap free
Amount of items: 1
Items: 
Size: 730407 Color: 0

Bin 3214: 269703 of cap free
Amount of items: 1
Items: 
Size: 730298 Color: 3

Bin 3215: 269769 of cap free
Amount of items: 1
Items: 
Size: 730232 Color: 4

Bin 3216: 269863 of cap free
Amount of items: 1
Items: 
Size: 730138 Color: 0

Bin 3217: 269959 of cap free
Amount of items: 1
Items: 
Size: 730042 Color: 3

Bin 3218: 270055 of cap free
Amount of items: 1
Items: 
Size: 729946 Color: 1

Bin 3219: 270163 of cap free
Amount of items: 1
Items: 
Size: 729838 Color: 1

Bin 3220: 270292 of cap free
Amount of items: 1
Items: 
Size: 729709 Color: 3

Bin 3221: 270294 of cap free
Amount of items: 1
Items: 
Size: 729707 Color: 3

Bin 3222: 270329 of cap free
Amount of items: 1
Items: 
Size: 729672 Color: 1

Bin 3223: 270417 of cap free
Amount of items: 1
Items: 
Size: 729584 Color: 2

Bin 3224: 270421 of cap free
Amount of items: 1
Items: 
Size: 729580 Color: 2

Bin 3225: 270444 of cap free
Amount of items: 1
Items: 
Size: 729557 Color: 4

Bin 3226: 270460 of cap free
Amount of items: 1
Items: 
Size: 729541 Color: 1

Bin 3227: 270485 of cap free
Amount of items: 1
Items: 
Size: 729516 Color: 3

Bin 3228: 270539 of cap free
Amount of items: 1
Items: 
Size: 729462 Color: 1

Bin 3229: 270571 of cap free
Amount of items: 1
Items: 
Size: 729430 Color: 3

Bin 3230: 270581 of cap free
Amount of items: 1
Items: 
Size: 729420 Color: 4

Bin 3231: 270726 of cap free
Amount of items: 1
Items: 
Size: 729275 Color: 2

Bin 3232: 270758 of cap free
Amount of items: 1
Items: 
Size: 729243 Color: 0

Bin 3233: 270760 of cap free
Amount of items: 1
Items: 
Size: 729241 Color: 3

Bin 3234: 270796 of cap free
Amount of items: 1
Items: 
Size: 729205 Color: 3

Bin 3235: 270878 of cap free
Amount of items: 1
Items: 
Size: 729123 Color: 1

Bin 3236: 270885 of cap free
Amount of items: 1
Items: 
Size: 729116 Color: 1

Bin 3237: 271100 of cap free
Amount of items: 1
Items: 
Size: 728901 Color: 4

Bin 3238: 271289 of cap free
Amount of items: 1
Items: 
Size: 728712 Color: 4

Bin 3239: 271359 of cap free
Amount of items: 1
Items: 
Size: 728642 Color: 4

Bin 3240: 271365 of cap free
Amount of items: 1
Items: 
Size: 728636 Color: 2

Bin 3241: 271370 of cap free
Amount of items: 1
Items: 
Size: 728631 Color: 3

Bin 3242: 271395 of cap free
Amount of items: 1
Items: 
Size: 728606 Color: 0

Bin 3243: 271421 of cap free
Amount of items: 1
Items: 
Size: 728580 Color: 4

Bin 3244: 271472 of cap free
Amount of items: 1
Items: 
Size: 728529 Color: 0

Bin 3245: 271512 of cap free
Amount of items: 1
Items: 
Size: 728489 Color: 1

Bin 3246: 271550 of cap free
Amount of items: 1
Items: 
Size: 728451 Color: 0

Bin 3247: 271580 of cap free
Amount of items: 1
Items: 
Size: 728421 Color: 4

Bin 3248: 271602 of cap free
Amount of items: 1
Items: 
Size: 728399 Color: 3

Bin 3249: 271608 of cap free
Amount of items: 1
Items: 
Size: 728393 Color: 1

Bin 3250: 271625 of cap free
Amount of items: 1
Items: 
Size: 728376 Color: 3

Bin 3251: 271868 of cap free
Amount of items: 1
Items: 
Size: 728133 Color: 2

Bin 3252: 271938 of cap free
Amount of items: 1
Items: 
Size: 728063 Color: 0

Bin 3253: 272001 of cap free
Amount of items: 1
Items: 
Size: 728000 Color: 3

Bin 3254: 272137 of cap free
Amount of items: 1
Items: 
Size: 727864 Color: 0

Bin 3255: 272242 of cap free
Amount of items: 1
Items: 
Size: 727759 Color: 1

Bin 3256: 272415 of cap free
Amount of items: 1
Items: 
Size: 727586 Color: 3

Bin 3257: 272453 of cap free
Amount of items: 1
Items: 
Size: 727548 Color: 3

Bin 3258: 272477 of cap free
Amount of items: 1
Items: 
Size: 727524 Color: 1

Bin 3259: 272529 of cap free
Amount of items: 1
Items: 
Size: 727472 Color: 2

Bin 3260: 272724 of cap free
Amount of items: 1
Items: 
Size: 727277 Color: 0

Bin 3261: 272883 of cap free
Amount of items: 1
Items: 
Size: 727118 Color: 2

Bin 3262: 272966 of cap free
Amount of items: 1
Items: 
Size: 727035 Color: 1

Bin 3263: 273033 of cap free
Amount of items: 1
Items: 
Size: 726968 Color: 4

Bin 3264: 273252 of cap free
Amount of items: 1
Items: 
Size: 726749 Color: 2

Bin 3265: 273263 of cap free
Amount of items: 1
Items: 
Size: 726738 Color: 2

Bin 3266: 273445 of cap free
Amount of items: 1
Items: 
Size: 726556 Color: 0

Bin 3267: 273469 of cap free
Amount of items: 1
Items: 
Size: 726532 Color: 3

Bin 3268: 273475 of cap free
Amount of items: 1
Items: 
Size: 726526 Color: 3

Bin 3269: 273511 of cap free
Amount of items: 1
Items: 
Size: 726490 Color: 2

Bin 3270: 273559 of cap free
Amount of items: 1
Items: 
Size: 726442 Color: 4

Bin 3271: 273596 of cap free
Amount of items: 1
Items: 
Size: 726405 Color: 0

Bin 3272: 273748 of cap free
Amount of items: 1
Items: 
Size: 726253 Color: 4

Bin 3273: 273787 of cap free
Amount of items: 1
Items: 
Size: 726214 Color: 3

Bin 3274: 273791 of cap free
Amount of items: 1
Items: 
Size: 726210 Color: 2

Bin 3275: 273809 of cap free
Amount of items: 1
Items: 
Size: 726192 Color: 2

Bin 3276: 273878 of cap free
Amount of items: 1
Items: 
Size: 726123 Color: 0

Bin 3277: 273890 of cap free
Amount of items: 1
Items: 
Size: 726111 Color: 0

Bin 3278: 274017 of cap free
Amount of items: 1
Items: 
Size: 725984 Color: 1

Bin 3279: 274030 of cap free
Amount of items: 1
Items: 
Size: 725971 Color: 3

Bin 3280: 274063 of cap free
Amount of items: 1
Items: 
Size: 725938 Color: 2

Bin 3281: 274095 of cap free
Amount of items: 1
Items: 
Size: 725906 Color: 3

Bin 3282: 274174 of cap free
Amount of items: 1
Items: 
Size: 725827 Color: 3

Bin 3283: 274234 of cap free
Amount of items: 1
Items: 
Size: 725767 Color: 2

Bin 3284: 274292 of cap free
Amount of items: 1
Items: 
Size: 725709 Color: 3

Bin 3285: 274355 of cap free
Amount of items: 1
Items: 
Size: 725646 Color: 0

Bin 3286: 274440 of cap free
Amount of items: 1
Items: 
Size: 725561 Color: 0

Bin 3287: 274514 of cap free
Amount of items: 1
Items: 
Size: 725487 Color: 4

Bin 3288: 274542 of cap free
Amount of items: 1
Items: 
Size: 725459 Color: 3

Bin 3289: 274556 of cap free
Amount of items: 1
Items: 
Size: 725445 Color: 3

Bin 3290: 274632 of cap free
Amount of items: 1
Items: 
Size: 725369 Color: 3

Bin 3291: 274663 of cap free
Amount of items: 1
Items: 
Size: 725338 Color: 0

Bin 3292: 274699 of cap free
Amount of items: 1
Items: 
Size: 725302 Color: 0

Bin 3293: 274806 of cap free
Amount of items: 1
Items: 
Size: 725195 Color: 4

Bin 3294: 274989 of cap free
Amount of items: 1
Items: 
Size: 725012 Color: 1

Bin 3295: 275075 of cap free
Amount of items: 1
Items: 
Size: 724926 Color: 4

Bin 3296: 275357 of cap free
Amount of items: 1
Items: 
Size: 724644 Color: 2

Bin 3297: 275410 of cap free
Amount of items: 1
Items: 
Size: 724591 Color: 1

Bin 3298: 275546 of cap free
Amount of items: 1
Items: 
Size: 724455 Color: 4

Bin 3299: 275572 of cap free
Amount of items: 1
Items: 
Size: 724429 Color: 0

Bin 3300: 275588 of cap free
Amount of items: 1
Items: 
Size: 724413 Color: 0

Bin 3301: 275681 of cap free
Amount of items: 1
Items: 
Size: 724320 Color: 3

Bin 3302: 275693 of cap free
Amount of items: 1
Items: 
Size: 724308 Color: 0

Bin 3303: 275701 of cap free
Amount of items: 1
Items: 
Size: 724300 Color: 4

Bin 3304: 275754 of cap free
Amount of items: 1
Items: 
Size: 724247 Color: 2

Bin 3305: 275766 of cap free
Amount of items: 1
Items: 
Size: 724235 Color: 4

Bin 3306: 275787 of cap free
Amount of items: 1
Items: 
Size: 724214 Color: 4

Bin 3307: 275824 of cap free
Amount of items: 1
Items: 
Size: 724177 Color: 1

Bin 3308: 275835 of cap free
Amount of items: 1
Items: 
Size: 724166 Color: 4

Bin 3309: 275838 of cap free
Amount of items: 1
Items: 
Size: 724163 Color: 1

Bin 3310: 275854 of cap free
Amount of items: 1
Items: 
Size: 724147 Color: 3

Bin 3311: 275880 of cap free
Amount of items: 1
Items: 
Size: 724121 Color: 4

Bin 3312: 275989 of cap free
Amount of items: 1
Items: 
Size: 724012 Color: 1

Bin 3313: 276194 of cap free
Amount of items: 1
Items: 
Size: 723807 Color: 0

Bin 3314: 276552 of cap free
Amount of items: 1
Items: 
Size: 723449 Color: 4

Bin 3315: 276694 of cap free
Amount of items: 1
Items: 
Size: 723307 Color: 4

Bin 3316: 276797 of cap free
Amount of items: 1
Items: 
Size: 723204 Color: 3

Bin 3317: 276833 of cap free
Amount of items: 1
Items: 
Size: 723168 Color: 3

Bin 3318: 276923 of cap free
Amount of items: 1
Items: 
Size: 723078 Color: 1

Bin 3319: 276942 of cap free
Amount of items: 1
Items: 
Size: 723059 Color: 3

Bin 3320: 276957 of cap free
Amount of items: 1
Items: 
Size: 723044 Color: 1

Bin 3321: 276962 of cap free
Amount of items: 1
Items: 
Size: 723039 Color: 4

Bin 3322: 277095 of cap free
Amount of items: 1
Items: 
Size: 722906 Color: 3

Bin 3323: 277211 of cap free
Amount of items: 1
Items: 
Size: 722790 Color: 1

Bin 3324: 277236 of cap free
Amount of items: 1
Items: 
Size: 722765 Color: 2

Bin 3325: 277344 of cap free
Amount of items: 1
Items: 
Size: 722657 Color: 2

Bin 3326: 277407 of cap free
Amount of items: 1
Items: 
Size: 722594 Color: 3

Bin 3327: 277499 of cap free
Amount of items: 1
Items: 
Size: 722502 Color: 2

Bin 3328: 277555 of cap free
Amount of items: 1
Items: 
Size: 722446 Color: 1

Bin 3329: 277576 of cap free
Amount of items: 1
Items: 
Size: 722425 Color: 1

Bin 3330: 277588 of cap free
Amount of items: 1
Items: 
Size: 722413 Color: 1

Bin 3331: 277605 of cap free
Amount of items: 1
Items: 
Size: 722396 Color: 4

Bin 3332: 278033 of cap free
Amount of items: 1
Items: 
Size: 721968 Color: 0

Bin 3333: 278044 of cap free
Amount of items: 1
Items: 
Size: 721957 Color: 4

Bin 3334: 278057 of cap free
Amount of items: 1
Items: 
Size: 721944 Color: 3

Bin 3335: 278092 of cap free
Amount of items: 1
Items: 
Size: 721909 Color: 1

Bin 3336: 278109 of cap free
Amount of items: 1
Items: 
Size: 721892 Color: 4

Bin 3337: 278187 of cap free
Amount of items: 1
Items: 
Size: 721814 Color: 0

Bin 3338: 278204 of cap free
Amount of items: 1
Items: 
Size: 721797 Color: 2

Bin 3339: 278222 of cap free
Amount of items: 1
Items: 
Size: 721779 Color: 3

Bin 3340: 278238 of cap free
Amount of items: 1
Items: 
Size: 721763 Color: 4

Bin 3341: 278528 of cap free
Amount of items: 1
Items: 
Size: 721473 Color: 3

Bin 3342: 278528 of cap free
Amount of items: 1
Items: 
Size: 721473 Color: 4

Bin 3343: 278638 of cap free
Amount of items: 1
Items: 
Size: 721363 Color: 3

Bin 3344: 278652 of cap free
Amount of items: 1
Items: 
Size: 721349 Color: 4

Bin 3345: 278791 of cap free
Amount of items: 1
Items: 
Size: 721210 Color: 2

Bin 3346: 278903 of cap free
Amount of items: 1
Items: 
Size: 721098 Color: 3

Bin 3347: 278941 of cap free
Amount of items: 1
Items: 
Size: 721060 Color: 2

Bin 3348: 279064 of cap free
Amount of items: 1
Items: 
Size: 720937 Color: 3

Bin 3349: 279152 of cap free
Amount of items: 1
Items: 
Size: 720849 Color: 3

Bin 3350: 279233 of cap free
Amount of items: 1
Items: 
Size: 720768 Color: 2

Bin 3351: 279295 of cap free
Amount of items: 1
Items: 
Size: 720706 Color: 4

Bin 3352: 279308 of cap free
Amount of items: 1
Items: 
Size: 720693 Color: 3

Bin 3353: 279505 of cap free
Amount of items: 1
Items: 
Size: 720496 Color: 3

Bin 3354: 279529 of cap free
Amount of items: 1
Items: 
Size: 720472 Color: 1

Bin 3355: 279600 of cap free
Amount of items: 1
Items: 
Size: 720401 Color: 2

Bin 3356: 279707 of cap free
Amount of items: 1
Items: 
Size: 720294 Color: 2

Bin 3357: 279775 of cap free
Amount of items: 1
Items: 
Size: 720226 Color: 2

Bin 3358: 279882 of cap free
Amount of items: 1
Items: 
Size: 720119 Color: 4

Bin 3359: 279966 of cap free
Amount of items: 1
Items: 
Size: 720035 Color: 0

Bin 3360: 280090 of cap free
Amount of items: 1
Items: 
Size: 719911 Color: 0

Bin 3361: 280151 of cap free
Amount of items: 1
Items: 
Size: 719850 Color: 4

Bin 3362: 280246 of cap free
Amount of items: 1
Items: 
Size: 719755 Color: 1

Bin 3363: 280258 of cap free
Amount of items: 1
Items: 
Size: 719743 Color: 3

Bin 3364: 280355 of cap free
Amount of items: 1
Items: 
Size: 719646 Color: 1

Bin 3365: 280379 of cap free
Amount of items: 1
Items: 
Size: 719622 Color: 4

Bin 3366: 280470 of cap free
Amount of items: 1
Items: 
Size: 719531 Color: 1

Bin 3367: 280514 of cap free
Amount of items: 1
Items: 
Size: 719487 Color: 3

Bin 3368: 280587 of cap free
Amount of items: 1
Items: 
Size: 719414 Color: 2

Bin 3369: 280846 of cap free
Amount of items: 1
Items: 
Size: 719155 Color: 2

Bin 3370: 280852 of cap free
Amount of items: 1
Items: 
Size: 719149 Color: 0

Bin 3371: 280957 of cap free
Amount of items: 1
Items: 
Size: 719044 Color: 4

Bin 3372: 281057 of cap free
Amount of items: 1
Items: 
Size: 718944 Color: 1

Bin 3373: 281336 of cap free
Amount of items: 1
Items: 
Size: 718665 Color: 3

Bin 3374: 281344 of cap free
Amount of items: 1
Items: 
Size: 718657 Color: 2

Bin 3375: 281375 of cap free
Amount of items: 1
Items: 
Size: 718626 Color: 4

Bin 3376: 281382 of cap free
Amount of items: 1
Items: 
Size: 718619 Color: 4

Bin 3377: 281491 of cap free
Amount of items: 1
Items: 
Size: 718510 Color: 1

Bin 3378: 281647 of cap free
Amount of items: 1
Items: 
Size: 718354 Color: 1

Bin 3379: 281721 of cap free
Amount of items: 1
Items: 
Size: 718280 Color: 3

Bin 3380: 281724 of cap free
Amount of items: 1
Items: 
Size: 718277 Color: 0

Bin 3381: 281789 of cap free
Amount of items: 1
Items: 
Size: 718212 Color: 2

Bin 3382: 281906 of cap free
Amount of items: 1
Items: 
Size: 718095 Color: 1

Bin 3383: 281970 of cap free
Amount of items: 1
Items: 
Size: 718031 Color: 4

Bin 3384: 282019 of cap free
Amount of items: 1
Items: 
Size: 717982 Color: 4

Bin 3385: 282033 of cap free
Amount of items: 1
Items: 
Size: 717968 Color: 2

Bin 3386: 282169 of cap free
Amount of items: 1
Items: 
Size: 717832 Color: 4

Bin 3387: 282199 of cap free
Amount of items: 1
Items: 
Size: 717802 Color: 3

Bin 3388: 282221 of cap free
Amount of items: 1
Items: 
Size: 717780 Color: 4

Bin 3389: 282265 of cap free
Amount of items: 1
Items: 
Size: 717736 Color: 0

Bin 3390: 282356 of cap free
Amount of items: 1
Items: 
Size: 717645 Color: 3

Bin 3391: 282526 of cap free
Amount of items: 1
Items: 
Size: 717475 Color: 0

Bin 3392: 282536 of cap free
Amount of items: 1
Items: 
Size: 717465 Color: 3

Bin 3393: 282587 of cap free
Amount of items: 1
Items: 
Size: 717414 Color: 1

Bin 3394: 282715 of cap free
Amount of items: 1
Items: 
Size: 717286 Color: 0

Bin 3395: 282768 of cap free
Amount of items: 1
Items: 
Size: 717233 Color: 0

Bin 3396: 282800 of cap free
Amount of items: 1
Items: 
Size: 717201 Color: 2

Bin 3397: 282833 of cap free
Amount of items: 1
Items: 
Size: 717168 Color: 0

Bin 3398: 282896 of cap free
Amount of items: 1
Items: 
Size: 717105 Color: 0

Bin 3399: 283132 of cap free
Amount of items: 1
Items: 
Size: 716869 Color: 1

Bin 3400: 283315 of cap free
Amount of items: 1
Items: 
Size: 716686 Color: 4

Bin 3401: 283430 of cap free
Amount of items: 1
Items: 
Size: 716571 Color: 3

Bin 3402: 283456 of cap free
Amount of items: 1
Items: 
Size: 716545 Color: 2

Bin 3403: 283476 of cap free
Amount of items: 1
Items: 
Size: 716525 Color: 1

Bin 3404: 283511 of cap free
Amount of items: 1
Items: 
Size: 716490 Color: 1

Bin 3405: 283618 of cap free
Amount of items: 1
Items: 
Size: 716383 Color: 1

Bin 3406: 283665 of cap free
Amount of items: 1
Items: 
Size: 716336 Color: 1

Bin 3407: 283690 of cap free
Amount of items: 1
Items: 
Size: 716311 Color: 0

Bin 3408: 283706 of cap free
Amount of items: 1
Items: 
Size: 716295 Color: 2

Bin 3409: 283815 of cap free
Amount of items: 1
Items: 
Size: 716186 Color: 4

Bin 3410: 283843 of cap free
Amount of items: 1
Items: 
Size: 716158 Color: 1

Bin 3411: 283848 of cap free
Amount of items: 1
Items: 
Size: 716153 Color: 0

Bin 3412: 283955 of cap free
Amount of items: 1
Items: 
Size: 716046 Color: 0

Bin 3413: 284085 of cap free
Amount of items: 1
Items: 
Size: 715916 Color: 4

Bin 3414: 284130 of cap free
Amount of items: 1
Items: 
Size: 715871 Color: 2

Bin 3415: 284151 of cap free
Amount of items: 1
Items: 
Size: 715850 Color: 3

Bin 3416: 284166 of cap free
Amount of items: 1
Items: 
Size: 715835 Color: 1

Bin 3417: 284188 of cap free
Amount of items: 1
Items: 
Size: 715813 Color: 3

Bin 3418: 284286 of cap free
Amount of items: 1
Items: 
Size: 715715 Color: 1

Bin 3419: 284373 of cap free
Amount of items: 1
Items: 
Size: 715628 Color: 2

Bin 3420: 284579 of cap free
Amount of items: 1
Items: 
Size: 715422 Color: 4

Bin 3421: 284585 of cap free
Amount of items: 1
Items: 
Size: 715416 Color: 4

Bin 3422: 284680 of cap free
Amount of items: 1
Items: 
Size: 715321 Color: 0

Bin 3423: 284721 of cap free
Amount of items: 1
Items: 
Size: 715280 Color: 1

Bin 3424: 284772 of cap free
Amount of items: 1
Items: 
Size: 715229 Color: 4

Bin 3425: 284853 of cap free
Amount of items: 1
Items: 
Size: 715148 Color: 4

Bin 3426: 284892 of cap free
Amount of items: 1
Items: 
Size: 715109 Color: 2

Bin 3427: 284943 of cap free
Amount of items: 1
Items: 
Size: 715058 Color: 4

Bin 3428: 285049 of cap free
Amount of items: 1
Items: 
Size: 714952 Color: 4

Bin 3429: 285064 of cap free
Amount of items: 1
Items: 
Size: 714937 Color: 0

Bin 3430: 285084 of cap free
Amount of items: 1
Items: 
Size: 714917 Color: 3

Bin 3431: 285138 of cap free
Amount of items: 1
Items: 
Size: 714863 Color: 3

Bin 3432: 285162 of cap free
Amount of items: 1
Items: 
Size: 714839 Color: 4

Bin 3433: 285218 of cap free
Amount of items: 1
Items: 
Size: 714783 Color: 1

Bin 3434: 285392 of cap free
Amount of items: 1
Items: 
Size: 714609 Color: 3

Bin 3435: 285418 of cap free
Amount of items: 1
Items: 
Size: 714583 Color: 1

Bin 3436: 285430 of cap free
Amount of items: 1
Items: 
Size: 714571 Color: 0

Bin 3437: 285469 of cap free
Amount of items: 1
Items: 
Size: 714532 Color: 0

Bin 3438: 285521 of cap free
Amount of items: 1
Items: 
Size: 714480 Color: 2

Bin 3439: 285581 of cap free
Amount of items: 1
Items: 
Size: 714420 Color: 4

Bin 3440: 285657 of cap free
Amount of items: 1
Items: 
Size: 714344 Color: 0

Bin 3441: 285771 of cap free
Amount of items: 1
Items: 
Size: 714230 Color: 2

Bin 3442: 285858 of cap free
Amount of items: 1
Items: 
Size: 714143 Color: 0

Bin 3443: 285908 of cap free
Amount of items: 1
Items: 
Size: 714093 Color: 0

Bin 3444: 285923 of cap free
Amount of items: 1
Items: 
Size: 714078 Color: 1

Bin 3445: 285977 of cap free
Amount of items: 1
Items: 
Size: 714024 Color: 0

Bin 3446: 285992 of cap free
Amount of items: 1
Items: 
Size: 714009 Color: 0

Bin 3447: 286055 of cap free
Amount of items: 1
Items: 
Size: 713946 Color: 4

Bin 3448: 286057 of cap free
Amount of items: 1
Items: 
Size: 713944 Color: 2

Bin 3449: 286079 of cap free
Amount of items: 1
Items: 
Size: 713922 Color: 1

Bin 3450: 286115 of cap free
Amount of items: 1
Items: 
Size: 713886 Color: 1

Bin 3451: 286126 of cap free
Amount of items: 1
Items: 
Size: 713875 Color: 1

Bin 3452: 286162 of cap free
Amount of items: 1
Items: 
Size: 713839 Color: 0

Bin 3453: 286219 of cap free
Amount of items: 1
Items: 
Size: 713782 Color: 3

Bin 3454: 286233 of cap free
Amount of items: 1
Items: 
Size: 713768 Color: 0

Bin 3455: 286275 of cap free
Amount of items: 1
Items: 
Size: 713726 Color: 4

Bin 3456: 286341 of cap free
Amount of items: 1
Items: 
Size: 713660 Color: 1

Bin 3457: 286344 of cap free
Amount of items: 1
Items: 
Size: 713657 Color: 1

Bin 3458: 286472 of cap free
Amount of items: 1
Items: 
Size: 713529 Color: 3

Bin 3459: 286493 of cap free
Amount of items: 1
Items: 
Size: 713508 Color: 0

Bin 3460: 286537 of cap free
Amount of items: 1
Items: 
Size: 713464 Color: 1

Bin 3461: 286567 of cap free
Amount of items: 1
Items: 
Size: 713434 Color: 4

Bin 3462: 286793 of cap free
Amount of items: 1
Items: 
Size: 713208 Color: 0

Bin 3463: 286875 of cap free
Amount of items: 1
Items: 
Size: 713126 Color: 2

Bin 3464: 286970 of cap free
Amount of items: 1
Items: 
Size: 713031 Color: 2

Bin 3465: 287000 of cap free
Amount of items: 1
Items: 
Size: 713001 Color: 3

Bin 3466: 287189 of cap free
Amount of items: 1
Items: 
Size: 712812 Color: 4

Bin 3467: 287326 of cap free
Amount of items: 1
Items: 
Size: 712675 Color: 2

Bin 3468: 287349 of cap free
Amount of items: 1
Items: 
Size: 712652 Color: 1

Bin 3469: 287359 of cap free
Amount of items: 1
Items: 
Size: 712642 Color: 4

Bin 3470: 287381 of cap free
Amount of items: 1
Items: 
Size: 712620 Color: 2

Bin 3471: 287397 of cap free
Amount of items: 1
Items: 
Size: 712604 Color: 0

Bin 3472: 287403 of cap free
Amount of items: 1
Items: 
Size: 712598 Color: 2

Bin 3473: 287454 of cap free
Amount of items: 1
Items: 
Size: 712547 Color: 1

Bin 3474: 287568 of cap free
Amount of items: 1
Items: 
Size: 712433 Color: 2

Bin 3475: 287833 of cap free
Amount of items: 1
Items: 
Size: 712168 Color: 0

Bin 3476: 287924 of cap free
Amount of items: 1
Items: 
Size: 712077 Color: 2

Bin 3477: 287961 of cap free
Amount of items: 1
Items: 
Size: 712040 Color: 0

Bin 3478: 287967 of cap free
Amount of items: 1
Items: 
Size: 712034 Color: 0

Bin 3479: 288001 of cap free
Amount of items: 1
Items: 
Size: 712000 Color: 1

Bin 3480: 288073 of cap free
Amount of items: 1
Items: 
Size: 711928 Color: 3

Bin 3481: 288132 of cap free
Amount of items: 1
Items: 
Size: 711869 Color: 0

Bin 3482: 288140 of cap free
Amount of items: 1
Items: 
Size: 711861 Color: 2

Bin 3483: 288143 of cap free
Amount of items: 1
Items: 
Size: 711858 Color: 0

Bin 3484: 288160 of cap free
Amount of items: 1
Items: 
Size: 711841 Color: 4

Bin 3485: 288206 of cap free
Amount of items: 1
Items: 
Size: 711795 Color: 2

Bin 3486: 288234 of cap free
Amount of items: 1
Items: 
Size: 711767 Color: 0

Bin 3487: 288371 of cap free
Amount of items: 1
Items: 
Size: 711630 Color: 2

Bin 3488: 288454 of cap free
Amount of items: 1
Items: 
Size: 711547 Color: 4

Bin 3489: 288528 of cap free
Amount of items: 1
Items: 
Size: 711473 Color: 2

Bin 3490: 288546 of cap free
Amount of items: 1
Items: 
Size: 711455 Color: 3

Bin 3491: 288606 of cap free
Amount of items: 1
Items: 
Size: 711395 Color: 4

Bin 3492: 288707 of cap free
Amount of items: 1
Items: 
Size: 711294 Color: 0

Bin 3493: 288861 of cap free
Amount of items: 1
Items: 
Size: 711140 Color: 2

Bin 3494: 288895 of cap free
Amount of items: 1
Items: 
Size: 711106 Color: 3

Bin 3495: 288960 of cap free
Amount of items: 1
Items: 
Size: 711041 Color: 4

Bin 3496: 289124 of cap free
Amount of items: 1
Items: 
Size: 710877 Color: 0

Bin 3497: 289150 of cap free
Amount of items: 1
Items: 
Size: 710851 Color: 3

Bin 3498: 289253 of cap free
Amount of items: 1
Items: 
Size: 710748 Color: 3

Bin 3499: 289434 of cap free
Amount of items: 1
Items: 
Size: 710567 Color: 4

Bin 3500: 289574 of cap free
Amount of items: 1
Items: 
Size: 710427 Color: 4

Bin 3501: 289591 of cap free
Amount of items: 1
Items: 
Size: 710410 Color: 0

Bin 3502: 289732 of cap free
Amount of items: 1
Items: 
Size: 710269 Color: 4

Bin 3503: 289734 of cap free
Amount of items: 1
Items: 
Size: 710267 Color: 1

Bin 3504: 289884 of cap free
Amount of items: 1
Items: 
Size: 710117 Color: 1

Bin 3505: 289930 of cap free
Amount of items: 1
Items: 
Size: 710071 Color: 0

Bin 3506: 289977 of cap free
Amount of items: 1
Items: 
Size: 710024 Color: 1

Bin 3507: 290052 of cap free
Amount of items: 1
Items: 
Size: 709949 Color: 0

Bin 3508: 290215 of cap free
Amount of items: 1
Items: 
Size: 709786 Color: 0

Bin 3509: 290287 of cap free
Amount of items: 1
Items: 
Size: 709714 Color: 0

Bin 3510: 290388 of cap free
Amount of items: 1
Items: 
Size: 709613 Color: 0

Bin 3511: 290389 of cap free
Amount of items: 1
Items: 
Size: 709612 Color: 2

Bin 3512: 290464 of cap free
Amount of items: 1
Items: 
Size: 709537 Color: 4

Bin 3513: 290519 of cap free
Amount of items: 1
Items: 
Size: 709482 Color: 2

Bin 3514: 290524 of cap free
Amount of items: 1
Items: 
Size: 709477 Color: 2

Bin 3515: 290566 of cap free
Amount of items: 1
Items: 
Size: 709435 Color: 0

Bin 3516: 290594 of cap free
Amount of items: 1
Items: 
Size: 709407 Color: 1

Bin 3517: 290597 of cap free
Amount of items: 1
Items: 
Size: 709404 Color: 1

Bin 3518: 290662 of cap free
Amount of items: 1
Items: 
Size: 709339 Color: 3

Bin 3519: 290714 of cap free
Amount of items: 1
Items: 
Size: 709287 Color: 3

Bin 3520: 290767 of cap free
Amount of items: 1
Items: 
Size: 709234 Color: 4

Bin 3521: 290909 of cap free
Amount of items: 1
Items: 
Size: 709092 Color: 2

Bin 3522: 290923 of cap free
Amount of items: 1
Items: 
Size: 709078 Color: 3

Bin 3523: 290939 of cap free
Amount of items: 1
Items: 
Size: 709062 Color: 4

Bin 3524: 291026 of cap free
Amount of items: 1
Items: 
Size: 708975 Color: 3

Bin 3525: 291162 of cap free
Amount of items: 1
Items: 
Size: 708839 Color: 3

Bin 3526: 291350 of cap free
Amount of items: 1
Items: 
Size: 708651 Color: 4

Bin 3527: 291355 of cap free
Amount of items: 1
Items: 
Size: 708646 Color: 0

Bin 3528: 291392 of cap free
Amount of items: 1
Items: 
Size: 708609 Color: 3

Bin 3529: 291431 of cap free
Amount of items: 1
Items: 
Size: 708570 Color: 2

Bin 3530: 291485 of cap free
Amount of items: 1
Items: 
Size: 708516 Color: 3

Bin 3531: 291543 of cap free
Amount of items: 1
Items: 
Size: 708458 Color: 1

Bin 3532: 291553 of cap free
Amount of items: 1
Items: 
Size: 708448 Color: 2

Bin 3533: 291580 of cap free
Amount of items: 1
Items: 
Size: 708421 Color: 3

Bin 3534: 291623 of cap free
Amount of items: 1
Items: 
Size: 708378 Color: 2

Bin 3535: 291657 of cap free
Amount of items: 1
Items: 
Size: 708344 Color: 3

Bin 3536: 291700 of cap free
Amount of items: 1
Items: 
Size: 708301 Color: 2

Bin 3537: 291746 of cap free
Amount of items: 1
Items: 
Size: 708255 Color: 2

Bin 3538: 291792 of cap free
Amount of items: 1
Items: 
Size: 708209 Color: 4

Bin 3539: 291819 of cap free
Amount of items: 1
Items: 
Size: 708182 Color: 3

Bin 3540: 291820 of cap free
Amount of items: 1
Items: 
Size: 708181 Color: 1

Bin 3541: 292072 of cap free
Amount of items: 1
Items: 
Size: 707929 Color: 2

Bin 3542: 292124 of cap free
Amount of items: 1
Items: 
Size: 707877 Color: 0

Bin 3543: 292132 of cap free
Amount of items: 1
Items: 
Size: 707869 Color: 0

Bin 3544: 292279 of cap free
Amount of items: 1
Items: 
Size: 707722 Color: 0

Bin 3545: 292356 of cap free
Amount of items: 1
Items: 
Size: 707645 Color: 0

Bin 3546: 292373 of cap free
Amount of items: 1
Items: 
Size: 707628 Color: 3

Bin 3547: 292488 of cap free
Amount of items: 1
Items: 
Size: 707513 Color: 2

Bin 3548: 292492 of cap free
Amount of items: 1
Items: 
Size: 707509 Color: 1

Bin 3549: 292511 of cap free
Amount of items: 1
Items: 
Size: 707490 Color: 1

Bin 3550: 292519 of cap free
Amount of items: 1
Items: 
Size: 707482 Color: 0

Bin 3551: 292522 of cap free
Amount of items: 1
Items: 
Size: 707479 Color: 0

Bin 3552: 292578 of cap free
Amount of items: 1
Items: 
Size: 707423 Color: 4

Bin 3553: 292605 of cap free
Amount of items: 1
Items: 
Size: 707396 Color: 0

Bin 3554: 292647 of cap free
Amount of items: 1
Items: 
Size: 707354 Color: 1

Bin 3555: 292693 of cap free
Amount of items: 1
Items: 
Size: 707308 Color: 4

Bin 3556: 292862 of cap free
Amount of items: 1
Items: 
Size: 707139 Color: 4

Bin 3557: 292926 of cap free
Amount of items: 1
Items: 
Size: 707075 Color: 2

Bin 3558: 292990 of cap free
Amount of items: 1
Items: 
Size: 707011 Color: 2

Bin 3559: 293028 of cap free
Amount of items: 1
Items: 
Size: 706973 Color: 3

Bin 3560: 293098 of cap free
Amount of items: 1
Items: 
Size: 706903 Color: 0

Bin 3561: 293184 of cap free
Amount of items: 1
Items: 
Size: 706817 Color: 0

Bin 3562: 293265 of cap free
Amount of items: 1
Items: 
Size: 706736 Color: 1

Bin 3563: 293283 of cap free
Amount of items: 1
Items: 
Size: 706718 Color: 3

Bin 3564: 293316 of cap free
Amount of items: 1
Items: 
Size: 706685 Color: 4

Bin 3565: 293380 of cap free
Amount of items: 1
Items: 
Size: 706621 Color: 0

Bin 3566: 293459 of cap free
Amount of items: 1
Items: 
Size: 706542 Color: 3

Bin 3567: 293598 of cap free
Amount of items: 1
Items: 
Size: 706403 Color: 1

Bin 3568: 293751 of cap free
Amount of items: 1
Items: 
Size: 706250 Color: 3

Bin 3569: 293808 of cap free
Amount of items: 1
Items: 
Size: 706193 Color: 1

Bin 3570: 293895 of cap free
Amount of items: 1
Items: 
Size: 706106 Color: 2

Bin 3571: 293997 of cap free
Amount of items: 1
Items: 
Size: 706004 Color: 2

Bin 3572: 294010 of cap free
Amount of items: 1
Items: 
Size: 705991 Color: 0

Bin 3573: 294086 of cap free
Amount of items: 1
Items: 
Size: 705915 Color: 1

Bin 3574: 294128 of cap free
Amount of items: 1
Items: 
Size: 705873 Color: 1

Bin 3575: 294247 of cap free
Amount of items: 1
Items: 
Size: 705754 Color: 1

Bin 3576: 294419 of cap free
Amount of items: 1
Items: 
Size: 705582 Color: 4

Bin 3577: 294420 of cap free
Amount of items: 1
Items: 
Size: 705581 Color: 0

Bin 3578: 294498 of cap free
Amount of items: 1
Items: 
Size: 705503 Color: 3

Bin 3579: 294604 of cap free
Amount of items: 1
Items: 
Size: 705397 Color: 3

Bin 3580: 294745 of cap free
Amount of items: 1
Items: 
Size: 705256 Color: 3

Bin 3581: 294796 of cap free
Amount of items: 1
Items: 
Size: 705205 Color: 1

Bin 3582: 294820 of cap free
Amount of items: 1
Items: 
Size: 705181 Color: 0

Bin 3583: 294935 of cap free
Amount of items: 1
Items: 
Size: 705066 Color: 3

Bin 3584: 295019 of cap free
Amount of items: 1
Items: 
Size: 704982 Color: 1

Bin 3585: 295211 of cap free
Amount of items: 1
Items: 
Size: 704790 Color: 0

Bin 3586: 295212 of cap free
Amount of items: 1
Items: 
Size: 704789 Color: 1

Bin 3587: 295224 of cap free
Amount of items: 1
Items: 
Size: 704777 Color: 0

Bin 3588: 295233 of cap free
Amount of items: 1
Items: 
Size: 704768 Color: 3

Bin 3589: 295263 of cap free
Amount of items: 1
Items: 
Size: 704738 Color: 2

Bin 3590: 295277 of cap free
Amount of items: 1
Items: 
Size: 704724 Color: 2

Bin 3591: 295302 of cap free
Amount of items: 1
Items: 
Size: 704699 Color: 3

Bin 3592: 295338 of cap free
Amount of items: 1
Items: 
Size: 704663 Color: 2

Bin 3593: 295369 of cap free
Amount of items: 1
Items: 
Size: 704632 Color: 0

Bin 3594: 295375 of cap free
Amount of items: 1
Items: 
Size: 704626 Color: 0

Bin 3595: 295419 of cap free
Amount of items: 1
Items: 
Size: 704582 Color: 4

Bin 3596: 295487 of cap free
Amount of items: 1
Items: 
Size: 704514 Color: 2

Bin 3597: 295535 of cap free
Amount of items: 1
Items: 
Size: 704466 Color: 0

Bin 3598: 295768 of cap free
Amount of items: 1
Items: 
Size: 704233 Color: 0

Bin 3599: 295908 of cap free
Amount of items: 1
Items: 
Size: 704093 Color: 4

Bin 3600: 295916 of cap free
Amount of items: 1
Items: 
Size: 704085 Color: 3

Bin 3601: 295940 of cap free
Amount of items: 1
Items: 
Size: 704061 Color: 4

Bin 3602: 295957 of cap free
Amount of items: 1
Items: 
Size: 704044 Color: 1

Bin 3603: 295972 of cap free
Amount of items: 1
Items: 
Size: 704029 Color: 3

Bin 3604: 296030 of cap free
Amount of items: 1
Items: 
Size: 703971 Color: 1

Bin 3605: 296037 of cap free
Amount of items: 1
Items: 
Size: 703964 Color: 4

Bin 3606: 296050 of cap free
Amount of items: 1
Items: 
Size: 703951 Color: 2

Bin 3607: 296053 of cap free
Amount of items: 1
Items: 
Size: 703948 Color: 2

Bin 3608: 296182 of cap free
Amount of items: 1
Items: 
Size: 703819 Color: 1

Bin 3609: 296269 of cap free
Amount of items: 1
Items: 
Size: 703732 Color: 2

Bin 3610: 296298 of cap free
Amount of items: 1
Items: 
Size: 703703 Color: 2

Bin 3611: 296405 of cap free
Amount of items: 1
Items: 
Size: 703596 Color: 2

Bin 3612: 296426 of cap free
Amount of items: 1
Items: 
Size: 703575 Color: 1

Bin 3613: 296466 of cap free
Amount of items: 1
Items: 
Size: 703535 Color: 0

Bin 3614: 296482 of cap free
Amount of items: 1
Items: 
Size: 703519 Color: 3

Bin 3615: 296494 of cap free
Amount of items: 1
Items: 
Size: 703507 Color: 3

Bin 3616: 296528 of cap free
Amount of items: 1
Items: 
Size: 703473 Color: 1

Bin 3617: 296579 of cap free
Amount of items: 1
Items: 
Size: 703422 Color: 1

Bin 3618: 296644 of cap free
Amount of items: 1
Items: 
Size: 703357 Color: 1

Bin 3619: 296670 of cap free
Amount of items: 1
Items: 
Size: 703331 Color: 1

Bin 3620: 296724 of cap free
Amount of items: 1
Items: 
Size: 703277 Color: 4

Bin 3621: 296783 of cap free
Amount of items: 1
Items: 
Size: 703218 Color: 0

Bin 3622: 296953 of cap free
Amount of items: 1
Items: 
Size: 703048 Color: 1

Bin 3623: 296966 of cap free
Amount of items: 1
Items: 
Size: 703035 Color: 1

Bin 3624: 297161 of cap free
Amount of items: 1
Items: 
Size: 702840 Color: 1

Bin 3625: 297166 of cap free
Amount of items: 1
Items: 
Size: 702835 Color: 2

Bin 3626: 297264 of cap free
Amount of items: 1
Items: 
Size: 702737 Color: 4

Bin 3627: 297315 of cap free
Amount of items: 1
Items: 
Size: 702686 Color: 2

Bin 3628: 297437 of cap free
Amount of items: 1
Items: 
Size: 702564 Color: 0

Bin 3629: 297461 of cap free
Amount of items: 1
Items: 
Size: 702540 Color: 2

Bin 3630: 297470 of cap free
Amount of items: 1
Items: 
Size: 702531 Color: 1

Bin 3631: 297475 of cap free
Amount of items: 1
Items: 
Size: 702526 Color: 2

Bin 3632: 297483 of cap free
Amount of items: 1
Items: 
Size: 702518 Color: 0

Bin 3633: 297522 of cap free
Amount of items: 1
Items: 
Size: 702479 Color: 0

Bin 3634: 297623 of cap free
Amount of items: 1
Items: 
Size: 702378 Color: 0

Bin 3635: 297716 of cap free
Amount of items: 1
Items: 
Size: 702285 Color: 0

Bin 3636: 297805 of cap free
Amount of items: 1
Items: 
Size: 702196 Color: 4

Bin 3637: 297955 of cap free
Amount of items: 1
Items: 
Size: 702046 Color: 1

Bin 3638: 298000 of cap free
Amount of items: 1
Items: 
Size: 702001 Color: 1

Bin 3639: 298041 of cap free
Amount of items: 1
Items: 
Size: 701960 Color: 3

Bin 3640: 298049 of cap free
Amount of items: 1
Items: 
Size: 701952 Color: 4

Bin 3641: 298050 of cap free
Amount of items: 1
Items: 
Size: 701951 Color: 1

Bin 3642: 298229 of cap free
Amount of items: 1
Items: 
Size: 701772 Color: 1

Bin 3643: 298253 of cap free
Amount of items: 1
Items: 
Size: 701748 Color: 4

Bin 3644: 298264 of cap free
Amount of items: 1
Items: 
Size: 701737 Color: 2

Bin 3645: 298383 of cap free
Amount of items: 1
Items: 
Size: 701618 Color: 0

Bin 3646: 298514 of cap free
Amount of items: 1
Items: 
Size: 701487 Color: 3

Bin 3647: 298531 of cap free
Amount of items: 1
Items: 
Size: 701470 Color: 4

Bin 3648: 298606 of cap free
Amount of items: 1
Items: 
Size: 701395 Color: 1

Bin 3649: 298618 of cap free
Amount of items: 1
Items: 
Size: 701383 Color: 1

Bin 3650: 298619 of cap free
Amount of items: 1
Items: 
Size: 701382 Color: 4

Bin 3651: 298656 of cap free
Amount of items: 1
Items: 
Size: 701345 Color: 2

Bin 3652: 298676 of cap free
Amount of items: 1
Items: 
Size: 701325 Color: 3

Bin 3653: 298905 of cap free
Amount of items: 1
Items: 
Size: 701096 Color: 3

Bin 3654: 298982 of cap free
Amount of items: 1
Items: 
Size: 701019 Color: 3

Bin 3655: 299050 of cap free
Amount of items: 1
Items: 
Size: 700951 Color: 4

Bin 3656: 299173 of cap free
Amount of items: 1
Items: 
Size: 700828 Color: 3

Bin 3657: 299238 of cap free
Amount of items: 1
Items: 
Size: 700763 Color: 4

Bin 3658: 299241 of cap free
Amount of items: 1
Items: 
Size: 700760 Color: 4

Bin 3659: 299318 of cap free
Amount of items: 1
Items: 
Size: 700683 Color: 0

Bin 3660: 299326 of cap free
Amount of items: 1
Items: 
Size: 700675 Color: 0

Bin 3661: 299388 of cap free
Amount of items: 1
Items: 
Size: 700613 Color: 4

Bin 3662: 299430 of cap free
Amount of items: 1
Items: 
Size: 700571 Color: 1

Bin 3663: 299436 of cap free
Amount of items: 1
Items: 
Size: 700565 Color: 1

Bin 3664: 299555 of cap free
Amount of items: 1
Items: 
Size: 700446 Color: 0

Bin 3665: 299638 of cap free
Amount of items: 1
Items: 
Size: 700363 Color: 1

Bin 3666: 299665 of cap free
Amount of items: 1
Items: 
Size: 700336 Color: 3

Bin 3667: 299682 of cap free
Amount of items: 1
Items: 
Size: 700319 Color: 4

Bin 3668: 299757 of cap free
Amount of items: 1
Items: 
Size: 700244 Color: 3

Bin 3669: 299807 of cap free
Amount of items: 1
Items: 
Size: 700194 Color: 0

Bin 3670: 299822 of cap free
Amount of items: 1
Items: 
Size: 700179 Color: 2

Bin 3671: 300049 of cap free
Amount of items: 1
Items: 
Size: 699952 Color: 0

Bin 3672: 300055 of cap free
Amount of items: 1
Items: 
Size: 699946 Color: 4

Bin 3673: 300094 of cap free
Amount of items: 1
Items: 
Size: 699907 Color: 3

Bin 3674: 300156 of cap free
Amount of items: 1
Items: 
Size: 699845 Color: 1

Bin 3675: 300167 of cap free
Amount of items: 1
Items: 
Size: 699834 Color: 1

Bin 3676: 300306 of cap free
Amount of items: 1
Items: 
Size: 699695 Color: 4

Bin 3677: 300334 of cap free
Amount of items: 1
Items: 
Size: 699667 Color: 4

Bin 3678: 300426 of cap free
Amount of items: 1
Items: 
Size: 699575 Color: 4

Bin 3679: 300466 of cap free
Amount of items: 1
Items: 
Size: 699535 Color: 2

Bin 3680: 300484 of cap free
Amount of items: 1
Items: 
Size: 699517 Color: 3

Bin 3681: 300488 of cap free
Amount of items: 1
Items: 
Size: 699513 Color: 2

Bin 3682: 300624 of cap free
Amount of items: 1
Items: 
Size: 699377 Color: 0

Bin 3683: 300639 of cap free
Amount of items: 1
Items: 
Size: 699362 Color: 1

Bin 3684: 300779 of cap free
Amount of items: 1
Items: 
Size: 699222 Color: 3

Bin 3685: 300887 of cap free
Amount of items: 1
Items: 
Size: 699114 Color: 1

Bin 3686: 300974 of cap free
Amount of items: 1
Items: 
Size: 699027 Color: 3

Bin 3687: 301032 of cap free
Amount of items: 1
Items: 
Size: 698969 Color: 4

Bin 3688: 301041 of cap free
Amount of items: 1
Items: 
Size: 698960 Color: 3

Bin 3689: 301055 of cap free
Amount of items: 1
Items: 
Size: 698946 Color: 0

Bin 3690: 301133 of cap free
Amount of items: 1
Items: 
Size: 698868 Color: 2

Bin 3691: 301274 of cap free
Amount of items: 1
Items: 
Size: 698727 Color: 2

Bin 3692: 301329 of cap free
Amount of items: 1
Items: 
Size: 698672 Color: 1

Bin 3693: 301347 of cap free
Amount of items: 1
Items: 
Size: 698654 Color: 4

Bin 3694: 301357 of cap free
Amount of items: 1
Items: 
Size: 698644 Color: 3

Bin 3695: 301364 of cap free
Amount of items: 1
Items: 
Size: 698637 Color: 4

Bin 3696: 301396 of cap free
Amount of items: 1
Items: 
Size: 698605 Color: 4

Bin 3697: 301447 of cap free
Amount of items: 1
Items: 
Size: 698554 Color: 2

Bin 3698: 301483 of cap free
Amount of items: 1
Items: 
Size: 698518 Color: 4

Bin 3699: 301518 of cap free
Amount of items: 1
Items: 
Size: 698483 Color: 4

Bin 3700: 301596 of cap free
Amount of items: 1
Items: 
Size: 698405 Color: 4

Bin 3701: 301606 of cap free
Amount of items: 1
Items: 
Size: 698395 Color: 0

Bin 3702: 301850 of cap free
Amount of items: 1
Items: 
Size: 698151 Color: 1

Bin 3703: 301874 of cap free
Amount of items: 1
Items: 
Size: 698127 Color: 4

Bin 3704: 302045 of cap free
Amount of items: 1
Items: 
Size: 697956 Color: 1

Bin 3705: 302164 of cap free
Amount of items: 1
Items: 
Size: 697837 Color: 0

Bin 3706: 302245 of cap free
Amount of items: 1
Items: 
Size: 697756 Color: 1

Bin 3707: 302397 of cap free
Amount of items: 1
Items: 
Size: 697604 Color: 2

Bin 3708: 302413 of cap free
Amount of items: 1
Items: 
Size: 697588 Color: 2

Bin 3709: 302487 of cap free
Amount of items: 1
Items: 
Size: 697514 Color: 0

Bin 3710: 302505 of cap free
Amount of items: 1
Items: 
Size: 697496 Color: 1

Bin 3711: 302540 of cap free
Amount of items: 1
Items: 
Size: 697461 Color: 3

Bin 3712: 302547 of cap free
Amount of items: 1
Items: 
Size: 697454 Color: 2

Bin 3713: 302755 of cap free
Amount of items: 1
Items: 
Size: 697246 Color: 0

Bin 3714: 302804 of cap free
Amount of items: 1
Items: 
Size: 697197 Color: 3

Bin 3715: 302839 of cap free
Amount of items: 1
Items: 
Size: 697162 Color: 0

Bin 3716: 302871 of cap free
Amount of items: 1
Items: 
Size: 697130 Color: 3

Bin 3717: 302960 of cap free
Amount of items: 1
Items: 
Size: 697041 Color: 4

Bin 3718: 302970 of cap free
Amount of items: 1
Items: 
Size: 697031 Color: 3

Bin 3719: 302993 of cap free
Amount of items: 1
Items: 
Size: 697008 Color: 4

Bin 3720: 303116 of cap free
Amount of items: 1
Items: 
Size: 696885 Color: 0

Bin 3721: 303178 of cap free
Amount of items: 1
Items: 
Size: 696823 Color: 2

Bin 3722: 303524 of cap free
Amount of items: 1
Items: 
Size: 696477 Color: 3

Bin 3723: 303543 of cap free
Amount of items: 1
Items: 
Size: 696458 Color: 0

Bin 3724: 303564 of cap free
Amount of items: 1
Items: 
Size: 696437 Color: 4

Bin 3725: 303565 of cap free
Amount of items: 1
Items: 
Size: 696436 Color: 0

Bin 3726: 303605 of cap free
Amount of items: 1
Items: 
Size: 696396 Color: 4

Bin 3727: 303660 of cap free
Amount of items: 1
Items: 
Size: 696341 Color: 1

Bin 3728: 303663 of cap free
Amount of items: 1
Items: 
Size: 696338 Color: 3

Bin 3729: 303702 of cap free
Amount of items: 1
Items: 
Size: 696299 Color: 4

Bin 3730: 303713 of cap free
Amount of items: 1
Items: 
Size: 696288 Color: 4

Bin 3731: 303783 of cap free
Amount of items: 1
Items: 
Size: 696218 Color: 4

Bin 3732: 304072 of cap free
Amount of items: 1
Items: 
Size: 695929 Color: 0

Bin 3733: 304141 of cap free
Amount of items: 1
Items: 
Size: 695860 Color: 2

Bin 3734: 304239 of cap free
Amount of items: 1
Items: 
Size: 695762 Color: 0

Bin 3735: 304264 of cap free
Amount of items: 1
Items: 
Size: 695737 Color: 1

Bin 3736: 304390 of cap free
Amount of items: 1
Items: 
Size: 695611 Color: 4

Bin 3737: 304414 of cap free
Amount of items: 1
Items: 
Size: 695587 Color: 4

Bin 3738: 304462 of cap free
Amount of items: 1
Items: 
Size: 695539 Color: 0

Bin 3739: 304489 of cap free
Amount of items: 1
Items: 
Size: 695512 Color: 4

Bin 3740: 304677 of cap free
Amount of items: 1
Items: 
Size: 695324 Color: 2

Bin 3741: 304759 of cap free
Amount of items: 1
Items: 
Size: 695242 Color: 4

Bin 3742: 304825 of cap free
Amount of items: 1
Items: 
Size: 695176 Color: 3

Bin 3743: 304918 of cap free
Amount of items: 1
Items: 
Size: 695083 Color: 1

Bin 3744: 305001 of cap free
Amount of items: 1
Items: 
Size: 695000 Color: 4

Bin 3745: 305012 of cap free
Amount of items: 1
Items: 
Size: 694989 Color: 3

Bin 3746: 305067 of cap free
Amount of items: 1
Items: 
Size: 694934 Color: 2

Bin 3747: 305098 of cap free
Amount of items: 1
Items: 
Size: 694903 Color: 0

Bin 3748: 305101 of cap free
Amount of items: 1
Items: 
Size: 694900 Color: 3

Bin 3749: 305257 of cap free
Amount of items: 1
Items: 
Size: 694744 Color: 4

Bin 3750: 305424 of cap free
Amount of items: 1
Items: 
Size: 694577 Color: 0

Bin 3751: 305437 of cap free
Amount of items: 1
Items: 
Size: 694564 Color: 1

Bin 3752: 305607 of cap free
Amount of items: 1
Items: 
Size: 694394 Color: 2

Bin 3753: 305652 of cap free
Amount of items: 1
Items: 
Size: 694349 Color: 4

Bin 3754: 305662 of cap free
Amount of items: 1
Items: 
Size: 694339 Color: 2

Bin 3755: 305777 of cap free
Amount of items: 1
Items: 
Size: 694224 Color: 1

Bin 3756: 305802 of cap free
Amount of items: 1
Items: 
Size: 694199 Color: 2

Bin 3757: 305948 of cap free
Amount of items: 1
Items: 
Size: 694053 Color: 2

Bin 3758: 306048 of cap free
Amount of items: 1
Items: 
Size: 693953 Color: 2

Bin 3759: 306284 of cap free
Amount of items: 1
Items: 
Size: 693717 Color: 1

Bin 3760: 306324 of cap free
Amount of items: 1
Items: 
Size: 693677 Color: 2

Bin 3761: 306345 of cap free
Amount of items: 1
Items: 
Size: 693656 Color: 4

Bin 3762: 306372 of cap free
Amount of items: 1
Items: 
Size: 693629 Color: 1

Bin 3763: 306493 of cap free
Amount of items: 1
Items: 
Size: 693508 Color: 2

Bin 3764: 306538 of cap free
Amount of items: 1
Items: 
Size: 693463 Color: 3

Bin 3765: 306545 of cap free
Amount of items: 1
Items: 
Size: 693456 Color: 0

Bin 3766: 306553 of cap free
Amount of items: 1
Items: 
Size: 693448 Color: 3

Bin 3767: 306606 of cap free
Amount of items: 1
Items: 
Size: 693395 Color: 3

Bin 3768: 306824 of cap free
Amount of items: 1
Items: 
Size: 693177 Color: 3

Bin 3769: 306841 of cap free
Amount of items: 1
Items: 
Size: 693160 Color: 0

Bin 3770: 306849 of cap free
Amount of items: 1
Items: 
Size: 693152 Color: 1

Bin 3771: 306850 of cap free
Amount of items: 1
Items: 
Size: 693151 Color: 3

Bin 3772: 306945 of cap free
Amount of items: 1
Items: 
Size: 693056 Color: 3

Bin 3773: 307004 of cap free
Amount of items: 1
Items: 
Size: 692997 Color: 3

Bin 3774: 307012 of cap free
Amount of items: 1
Items: 
Size: 692989 Color: 2

Bin 3775: 307029 of cap free
Amount of items: 1
Items: 
Size: 692972 Color: 1

Bin 3776: 307125 of cap free
Amount of items: 1
Items: 
Size: 692876 Color: 3

Bin 3777: 307277 of cap free
Amount of items: 1
Items: 
Size: 692724 Color: 1

Bin 3778: 307297 of cap free
Amount of items: 1
Items: 
Size: 692704 Color: 0

Bin 3779: 307361 of cap free
Amount of items: 1
Items: 
Size: 692640 Color: 3

Bin 3780: 307468 of cap free
Amount of items: 1
Items: 
Size: 692533 Color: 4

Bin 3781: 307641 of cap free
Amount of items: 1
Items: 
Size: 692360 Color: 2

Bin 3782: 307662 of cap free
Amount of items: 1
Items: 
Size: 692339 Color: 2

Bin 3783: 307692 of cap free
Amount of items: 1
Items: 
Size: 692309 Color: 0

Bin 3784: 307767 of cap free
Amount of items: 1
Items: 
Size: 692234 Color: 4

Bin 3785: 307908 of cap free
Amount of items: 1
Items: 
Size: 692093 Color: 1

Bin 3786: 307918 of cap free
Amount of items: 1
Items: 
Size: 692083 Color: 0

Bin 3787: 307960 of cap free
Amount of items: 1
Items: 
Size: 692041 Color: 2

Bin 3788: 307991 of cap free
Amount of items: 1
Items: 
Size: 692010 Color: 0

Bin 3789: 307992 of cap free
Amount of items: 1
Items: 
Size: 692009 Color: 1

Bin 3790: 308081 of cap free
Amount of items: 1
Items: 
Size: 691920 Color: 2

Bin 3791: 308279 of cap free
Amount of items: 1
Items: 
Size: 691722 Color: 1

Bin 3792: 308345 of cap free
Amount of items: 1
Items: 
Size: 691656 Color: 2

Bin 3793: 308564 of cap free
Amount of items: 1
Items: 
Size: 691437 Color: 2

Bin 3794: 308582 of cap free
Amount of items: 1
Items: 
Size: 691419 Color: 1

Bin 3795: 308690 of cap free
Amount of items: 1
Items: 
Size: 691311 Color: 3

Bin 3796: 308691 of cap free
Amount of items: 1
Items: 
Size: 691310 Color: 4

Bin 3797: 308694 of cap free
Amount of items: 1
Items: 
Size: 691307 Color: 2

Bin 3798: 308717 of cap free
Amount of items: 1
Items: 
Size: 691284 Color: 2

Bin 3799: 308765 of cap free
Amount of items: 1
Items: 
Size: 691236 Color: 2

Bin 3800: 308794 of cap free
Amount of items: 1
Items: 
Size: 691207 Color: 4

Bin 3801: 308806 of cap free
Amount of items: 1
Items: 
Size: 691195 Color: 1

Bin 3802: 308815 of cap free
Amount of items: 1
Items: 
Size: 691186 Color: 2

Bin 3803: 308982 of cap free
Amount of items: 1
Items: 
Size: 691019 Color: 2

Bin 3804: 309006 of cap free
Amount of items: 1
Items: 
Size: 690995 Color: 3

Bin 3805: 309071 of cap free
Amount of items: 1
Items: 
Size: 690930 Color: 1

Bin 3806: 309085 of cap free
Amount of items: 1
Items: 
Size: 690916 Color: 1

Bin 3807: 309174 of cap free
Amount of items: 1
Items: 
Size: 690827 Color: 4

Bin 3808: 309206 of cap free
Amount of items: 1
Items: 
Size: 690795 Color: 0

Bin 3809: 309344 of cap free
Amount of items: 1
Items: 
Size: 690657 Color: 4

Bin 3810: 309406 of cap free
Amount of items: 1
Items: 
Size: 690595 Color: 0

Bin 3811: 309410 of cap free
Amount of items: 1
Items: 
Size: 690591 Color: 3

Bin 3812: 309544 of cap free
Amount of items: 1
Items: 
Size: 690457 Color: 3

Bin 3813: 309604 of cap free
Amount of items: 1
Items: 
Size: 690397 Color: 2

Bin 3814: 309624 of cap free
Amount of items: 1
Items: 
Size: 690377 Color: 3

Bin 3815: 309679 of cap free
Amount of items: 1
Items: 
Size: 690322 Color: 3

Bin 3816: 309772 of cap free
Amount of items: 1
Items: 
Size: 690229 Color: 0

Bin 3817: 309782 of cap free
Amount of items: 1
Items: 
Size: 690219 Color: 0

Bin 3818: 309916 of cap free
Amount of items: 1
Items: 
Size: 690085 Color: 2

Bin 3819: 309985 of cap free
Amount of items: 1
Items: 
Size: 690016 Color: 3

Bin 3820: 310029 of cap free
Amount of items: 1
Items: 
Size: 689972 Color: 2

Bin 3821: 310067 of cap free
Amount of items: 1
Items: 
Size: 689934 Color: 1

Bin 3822: 310092 of cap free
Amount of items: 1
Items: 
Size: 689909 Color: 4

Bin 3823: 310199 of cap free
Amount of items: 1
Items: 
Size: 689802 Color: 1

Bin 3824: 310229 of cap free
Amount of items: 1
Items: 
Size: 689772 Color: 3

Bin 3825: 310236 of cap free
Amount of items: 1
Items: 
Size: 689765 Color: 4

Bin 3826: 310238 of cap free
Amount of items: 1
Items: 
Size: 689763 Color: 3

Bin 3827: 310268 of cap free
Amount of items: 1
Items: 
Size: 689733 Color: 2

Bin 3828: 310412 of cap free
Amount of items: 1
Items: 
Size: 689589 Color: 0

Bin 3829: 310433 of cap free
Amount of items: 1
Items: 
Size: 689568 Color: 2

Bin 3830: 310514 of cap free
Amount of items: 1
Items: 
Size: 689487 Color: 0

Bin 3831: 310519 of cap free
Amount of items: 1
Items: 
Size: 689482 Color: 3

Bin 3832: 310548 of cap free
Amount of items: 1
Items: 
Size: 689453 Color: 1

Bin 3833: 310834 of cap free
Amount of items: 1
Items: 
Size: 689167 Color: 0

Bin 3834: 310865 of cap free
Amount of items: 1
Items: 
Size: 689136 Color: 4

Bin 3835: 310897 of cap free
Amount of items: 1
Items: 
Size: 689104 Color: 2

Bin 3836: 310921 of cap free
Amount of items: 1
Items: 
Size: 689080 Color: 3

Bin 3837: 310978 of cap free
Amount of items: 1
Items: 
Size: 689023 Color: 0

Bin 3838: 311079 of cap free
Amount of items: 1
Items: 
Size: 688922 Color: 3

Bin 3839: 311131 of cap free
Amount of items: 1
Items: 
Size: 688870 Color: 2

Bin 3840: 311264 of cap free
Amount of items: 1
Items: 
Size: 688737 Color: 0

Bin 3841: 311311 of cap free
Amount of items: 1
Items: 
Size: 688690 Color: 0

Bin 3842: 311317 of cap free
Amount of items: 1
Items: 
Size: 688684 Color: 2

Bin 3843: 311335 of cap free
Amount of items: 1
Items: 
Size: 688666 Color: 3

Bin 3844: 311443 of cap free
Amount of items: 1
Items: 
Size: 688558 Color: 1

Bin 3845: 311704 of cap free
Amount of items: 1
Items: 
Size: 688297 Color: 4

Bin 3846: 311736 of cap free
Amount of items: 1
Items: 
Size: 688265 Color: 3

Bin 3847: 311750 of cap free
Amount of items: 1
Items: 
Size: 688251 Color: 4

Bin 3848: 311783 of cap free
Amount of items: 1
Items: 
Size: 688218 Color: 0

Bin 3849: 311848 of cap free
Amount of items: 1
Items: 
Size: 688153 Color: 2

Bin 3850: 312007 of cap free
Amount of items: 1
Items: 
Size: 687994 Color: 0

Bin 3851: 312110 of cap free
Amount of items: 1
Items: 
Size: 687891 Color: 1

Bin 3852: 312124 of cap free
Amount of items: 1
Items: 
Size: 687877 Color: 3

Bin 3853: 312313 of cap free
Amount of items: 1
Items: 
Size: 687688 Color: 3

Bin 3854: 312345 of cap free
Amount of items: 1
Items: 
Size: 687656 Color: 0

Bin 3855: 312368 of cap free
Amount of items: 1
Items: 
Size: 687633 Color: 1

Bin 3856: 312368 of cap free
Amount of items: 1
Items: 
Size: 687633 Color: 3

Bin 3857: 312407 of cap free
Amount of items: 1
Items: 
Size: 687594 Color: 3

Bin 3858: 312523 of cap free
Amount of items: 1
Items: 
Size: 687478 Color: 3

Bin 3859: 312663 of cap free
Amount of items: 1
Items: 
Size: 687338 Color: 3

Bin 3860: 312750 of cap free
Amount of items: 1
Items: 
Size: 687251 Color: 2

Bin 3861: 312958 of cap free
Amount of items: 1
Items: 
Size: 687043 Color: 1

Bin 3862: 313091 of cap free
Amount of items: 1
Items: 
Size: 686910 Color: 1

Bin 3863: 313114 of cap free
Amount of items: 1
Items: 
Size: 686887 Color: 0

Bin 3864: 313206 of cap free
Amount of items: 1
Items: 
Size: 686795 Color: 4

Bin 3865: 313252 of cap free
Amount of items: 1
Items: 
Size: 686749 Color: 4

Bin 3866: 313472 of cap free
Amount of items: 1
Items: 
Size: 686529 Color: 2

Bin 3867: 313535 of cap free
Amount of items: 1
Items: 
Size: 686466 Color: 4

Bin 3868: 313552 of cap free
Amount of items: 1
Items: 
Size: 686449 Color: 4

Bin 3869: 313742 of cap free
Amount of items: 1
Items: 
Size: 686259 Color: 2

Bin 3870: 313768 of cap free
Amount of items: 1
Items: 
Size: 686233 Color: 1

Bin 3871: 313852 of cap free
Amount of items: 1
Items: 
Size: 686149 Color: 4

Bin 3872: 313929 of cap free
Amount of items: 1
Items: 
Size: 686072 Color: 4

Bin 3873: 313970 of cap free
Amount of items: 1
Items: 
Size: 686031 Color: 3

Bin 3874: 314004 of cap free
Amount of items: 1
Items: 
Size: 685997 Color: 1

Bin 3875: 314023 of cap free
Amount of items: 1
Items: 
Size: 685978 Color: 2

Bin 3876: 314029 of cap free
Amount of items: 1
Items: 
Size: 685972 Color: 1

Bin 3877: 314038 of cap free
Amount of items: 1
Items: 
Size: 685963 Color: 3

Bin 3878: 314155 of cap free
Amount of items: 1
Items: 
Size: 685846 Color: 1

Bin 3879: 314172 of cap free
Amount of items: 1
Items: 
Size: 685829 Color: 4

Bin 3880: 314350 of cap free
Amount of items: 1
Items: 
Size: 685651 Color: 0

Bin 3881: 314365 of cap free
Amount of items: 1
Items: 
Size: 685636 Color: 3

Bin 3882: 314369 of cap free
Amount of items: 1
Items: 
Size: 685632 Color: 0

Bin 3883: 314456 of cap free
Amount of items: 1
Items: 
Size: 685545 Color: 1

Bin 3884: 314457 of cap free
Amount of items: 1
Items: 
Size: 685544 Color: 3

Bin 3885: 314481 of cap free
Amount of items: 1
Items: 
Size: 685520 Color: 2

Bin 3886: 314495 of cap free
Amount of items: 1
Items: 
Size: 685506 Color: 4

Bin 3887: 314513 of cap free
Amount of items: 1
Items: 
Size: 685488 Color: 3

Bin 3888: 314522 of cap free
Amount of items: 1
Items: 
Size: 685479 Color: 3

Bin 3889: 314525 of cap free
Amount of items: 1
Items: 
Size: 685476 Color: 2

Bin 3890: 314526 of cap free
Amount of items: 1
Items: 
Size: 685475 Color: 2

Bin 3891: 314729 of cap free
Amount of items: 1
Items: 
Size: 685272 Color: 0

Bin 3892: 314764 of cap free
Amount of items: 1
Items: 
Size: 685237 Color: 0

Bin 3893: 314791 of cap free
Amount of items: 1
Items: 
Size: 685210 Color: 0

Bin 3894: 314889 of cap free
Amount of items: 1
Items: 
Size: 685112 Color: 4

Bin 3895: 314918 of cap free
Amount of items: 1
Items: 
Size: 685083 Color: 0

Bin 3896: 314918 of cap free
Amount of items: 1
Items: 
Size: 685083 Color: 4

Bin 3897: 314954 of cap free
Amount of items: 1
Items: 
Size: 685047 Color: 2

Bin 3898: 314985 of cap free
Amount of items: 1
Items: 
Size: 685016 Color: 3

Bin 3899: 314995 of cap free
Amount of items: 1
Items: 
Size: 685006 Color: 4

Bin 3900: 315233 of cap free
Amount of items: 1
Items: 
Size: 684768 Color: 4

Bin 3901: 315430 of cap free
Amount of items: 1
Items: 
Size: 684571 Color: 2

Bin 3902: 315460 of cap free
Amount of items: 1
Items: 
Size: 684541 Color: 3

Bin 3903: 315471 of cap free
Amount of items: 1
Items: 
Size: 684530 Color: 1

Bin 3904: 315494 of cap free
Amount of items: 1
Items: 
Size: 684507 Color: 4

Bin 3905: 315600 of cap free
Amount of items: 1
Items: 
Size: 684401 Color: 2

Bin 3906: 315764 of cap free
Amount of items: 1
Items: 
Size: 684237 Color: 0

Bin 3907: 316008 of cap free
Amount of items: 1
Items: 
Size: 683993 Color: 2

Bin 3908: 316051 of cap free
Amount of items: 1
Items: 
Size: 683950 Color: 3

Bin 3909: 316058 of cap free
Amount of items: 1
Items: 
Size: 683943 Color: 1

Bin 3910: 316289 of cap free
Amount of items: 1
Items: 
Size: 683712 Color: 4

Bin 3911: 316297 of cap free
Amount of items: 1
Items: 
Size: 683704 Color: 0

Bin 3912: 316319 of cap free
Amount of items: 1
Items: 
Size: 683682 Color: 0

Bin 3913: 316530 of cap free
Amount of items: 1
Items: 
Size: 683471 Color: 0

Bin 3914: 316705 of cap free
Amount of items: 1
Items: 
Size: 683296 Color: 4

Bin 3915: 316723 of cap free
Amount of items: 1
Items: 
Size: 683278 Color: 0

Bin 3916: 316729 of cap free
Amount of items: 1
Items: 
Size: 683272 Color: 3

Bin 3917: 316783 of cap free
Amount of items: 1
Items: 
Size: 683218 Color: 1

Bin 3918: 316874 of cap free
Amount of items: 1
Items: 
Size: 683127 Color: 2

Bin 3919: 316902 of cap free
Amount of items: 1
Items: 
Size: 683099 Color: 0

Bin 3920: 316956 of cap free
Amount of items: 1
Items: 
Size: 683045 Color: 4

Bin 3921: 317001 of cap free
Amount of items: 1
Items: 
Size: 683000 Color: 2

Bin 3922: 317160 of cap free
Amount of items: 1
Items: 
Size: 682841 Color: 1

Bin 3923: 317161 of cap free
Amount of items: 1
Items: 
Size: 682840 Color: 4

Bin 3924: 317223 of cap free
Amount of items: 1
Items: 
Size: 682778 Color: 1

Bin 3925: 317350 of cap free
Amount of items: 1
Items: 
Size: 682651 Color: 2

Bin 3926: 317420 of cap free
Amount of items: 1
Items: 
Size: 682581 Color: 4

Bin 3927: 317451 of cap free
Amount of items: 1
Items: 
Size: 682550 Color: 2

Bin 3928: 317787 of cap free
Amount of items: 1
Items: 
Size: 682214 Color: 1

Bin 3929: 317819 of cap free
Amount of items: 1
Items: 
Size: 682182 Color: 0

Bin 3930: 317879 of cap free
Amount of items: 1
Items: 
Size: 682122 Color: 0

Bin 3931: 317900 of cap free
Amount of items: 1
Items: 
Size: 682101 Color: 2

Bin 3932: 317980 of cap free
Amount of items: 1
Items: 
Size: 682021 Color: 0

Bin 3933: 317998 of cap free
Amount of items: 1
Items: 
Size: 682003 Color: 3

Bin 3934: 318160 of cap free
Amount of items: 1
Items: 
Size: 681841 Color: 4

Bin 3935: 318189 of cap free
Amount of items: 1
Items: 
Size: 681812 Color: 3

Bin 3936: 318196 of cap free
Amount of items: 1
Items: 
Size: 681805 Color: 0

Bin 3937: 318238 of cap free
Amount of items: 1
Items: 
Size: 681763 Color: 3

Bin 3938: 318459 of cap free
Amount of items: 1
Items: 
Size: 681542 Color: 2

Bin 3939: 318510 of cap free
Amount of items: 1
Items: 
Size: 681491 Color: 0

Bin 3940: 318512 of cap free
Amount of items: 1
Items: 
Size: 681489 Color: 3

Bin 3941: 318611 of cap free
Amount of items: 1
Items: 
Size: 681390 Color: 1

Bin 3942: 318798 of cap free
Amount of items: 1
Items: 
Size: 681203 Color: 4

Bin 3943: 318811 of cap free
Amount of items: 1
Items: 
Size: 681190 Color: 0

Bin 3944: 318843 of cap free
Amount of items: 1
Items: 
Size: 681158 Color: 3

Bin 3945: 318936 of cap free
Amount of items: 1
Items: 
Size: 681065 Color: 0

Bin 3946: 318954 of cap free
Amount of items: 1
Items: 
Size: 681047 Color: 4

Bin 3947: 319102 of cap free
Amount of items: 1
Items: 
Size: 680899 Color: 1

Bin 3948: 319162 of cap free
Amount of items: 1
Items: 
Size: 680839 Color: 1

Bin 3949: 319212 of cap free
Amount of items: 1
Items: 
Size: 680789 Color: 1

Bin 3950: 319226 of cap free
Amount of items: 1
Items: 
Size: 680775 Color: 4

Bin 3951: 319249 of cap free
Amount of items: 1
Items: 
Size: 680752 Color: 1

Bin 3952: 319281 of cap free
Amount of items: 1
Items: 
Size: 680720 Color: 4

Bin 3953: 319364 of cap free
Amount of items: 1
Items: 
Size: 680637 Color: 4

Bin 3954: 319460 of cap free
Amount of items: 1
Items: 
Size: 680541 Color: 3

Bin 3955: 319546 of cap free
Amount of items: 1
Items: 
Size: 680455 Color: 2

Bin 3956: 319846 of cap free
Amount of items: 1
Items: 
Size: 680155 Color: 3

Bin 3957: 319930 of cap free
Amount of items: 1
Items: 
Size: 680071 Color: 2

Bin 3958: 319947 of cap free
Amount of items: 1
Items: 
Size: 680054 Color: 2

Bin 3959: 320214 of cap free
Amount of items: 1
Items: 
Size: 679787 Color: 4

Bin 3960: 320309 of cap free
Amount of items: 1
Items: 
Size: 679692 Color: 2

Bin 3961: 320340 of cap free
Amount of items: 1
Items: 
Size: 679661 Color: 2

Bin 3962: 320427 of cap free
Amount of items: 1
Items: 
Size: 679574 Color: 4

Bin 3963: 320619 of cap free
Amount of items: 1
Items: 
Size: 679382 Color: 1

Bin 3964: 320629 of cap free
Amount of items: 1
Items: 
Size: 679372 Color: 2

Bin 3965: 320661 of cap free
Amount of items: 1
Items: 
Size: 679340 Color: 4

Bin 3966: 320677 of cap free
Amount of items: 1
Items: 
Size: 679324 Color: 4

Bin 3967: 320697 of cap free
Amount of items: 1
Items: 
Size: 679304 Color: 2

Bin 3968: 320775 of cap free
Amount of items: 1
Items: 
Size: 679226 Color: 4

Bin 3969: 320864 of cap free
Amount of items: 1
Items: 
Size: 679137 Color: 1

Bin 3970: 320869 of cap free
Amount of items: 1
Items: 
Size: 679132 Color: 4

Bin 3971: 320872 of cap free
Amount of items: 1
Items: 
Size: 679129 Color: 1

Bin 3972: 320940 of cap free
Amount of items: 1
Items: 
Size: 679061 Color: 0

Bin 3973: 320958 of cap free
Amount of items: 1
Items: 
Size: 679043 Color: 4

Bin 3974: 320980 of cap free
Amount of items: 1
Items: 
Size: 679021 Color: 1

Bin 3975: 321017 of cap free
Amount of items: 1
Items: 
Size: 678984 Color: 0

Bin 3976: 321071 of cap free
Amount of items: 1
Items: 
Size: 678930 Color: 0

Bin 3977: 321080 of cap free
Amount of items: 1
Items: 
Size: 678921 Color: 0

Bin 3978: 321091 of cap free
Amount of items: 1
Items: 
Size: 678910 Color: 0

Bin 3979: 321173 of cap free
Amount of items: 1
Items: 
Size: 678828 Color: 4

Bin 3980: 321195 of cap free
Amount of items: 1
Items: 
Size: 678806 Color: 0

Bin 3981: 321343 of cap free
Amount of items: 1
Items: 
Size: 678658 Color: 1

Bin 3982: 321440 of cap free
Amount of items: 1
Items: 
Size: 678561 Color: 2

Bin 3983: 321442 of cap free
Amount of items: 1
Items: 
Size: 678559 Color: 1

Bin 3984: 321616 of cap free
Amount of items: 1
Items: 
Size: 678385 Color: 1

Bin 3985: 321637 of cap free
Amount of items: 1
Items: 
Size: 678364 Color: 0

Bin 3986: 321638 of cap free
Amount of items: 1
Items: 
Size: 678363 Color: 1

Bin 3987: 321658 of cap free
Amount of items: 1
Items: 
Size: 678343 Color: 4

Bin 3988: 321709 of cap free
Amount of items: 1
Items: 
Size: 678292 Color: 2

Bin 3989: 321757 of cap free
Amount of items: 1
Items: 
Size: 678244 Color: 3

Bin 3990: 321779 of cap free
Amount of items: 1
Items: 
Size: 678222 Color: 3

Bin 3991: 321894 of cap free
Amount of items: 1
Items: 
Size: 678107 Color: 2

Bin 3992: 322003 of cap free
Amount of items: 1
Items: 
Size: 677998 Color: 2

Bin 3993: 322029 of cap free
Amount of items: 1
Items: 
Size: 677972 Color: 0

Bin 3994: 322040 of cap free
Amount of items: 1
Items: 
Size: 677961 Color: 0

Bin 3995: 322090 of cap free
Amount of items: 1
Items: 
Size: 677911 Color: 2

Bin 3996: 322320 of cap free
Amount of items: 1
Items: 
Size: 677681 Color: 2

Bin 3997: 322458 of cap free
Amount of items: 1
Items: 
Size: 677543 Color: 3

Bin 3998: 322480 of cap free
Amount of items: 1
Items: 
Size: 677521 Color: 3

Bin 3999: 322484 of cap free
Amount of items: 1
Items: 
Size: 677517 Color: 1

Bin 4000: 322518 of cap free
Amount of items: 1
Items: 
Size: 677483 Color: 1

Bin 4001: 322530 of cap free
Amount of items: 1
Items: 
Size: 677471 Color: 4

Bin 4002: 322632 of cap free
Amount of items: 1
Items: 
Size: 677369 Color: 2

Bin 4003: 322771 of cap free
Amount of items: 1
Items: 
Size: 677230 Color: 1

Bin 4004: 322887 of cap free
Amount of items: 1
Items: 
Size: 677114 Color: 0

Bin 4005: 323136 of cap free
Amount of items: 1
Items: 
Size: 676865 Color: 0

Bin 4006: 323173 of cap free
Amount of items: 1
Items: 
Size: 676828 Color: 2

Bin 4007: 323309 of cap free
Amount of items: 1
Items: 
Size: 676692 Color: 2

Bin 4008: 323325 of cap free
Amount of items: 1
Items: 
Size: 676676 Color: 1

Bin 4009: 323365 of cap free
Amount of items: 1
Items: 
Size: 676636 Color: 1

Bin 4010: 323398 of cap free
Amount of items: 1
Items: 
Size: 676603 Color: 2

Bin 4011: 323433 of cap free
Amount of items: 1
Items: 
Size: 676568 Color: 2

Bin 4012: 323475 of cap free
Amount of items: 1
Items: 
Size: 676526 Color: 0

Bin 4013: 323560 of cap free
Amount of items: 1
Items: 
Size: 676441 Color: 4

Bin 4014: 323589 of cap free
Amount of items: 1
Items: 
Size: 676412 Color: 3

Bin 4015: 323614 of cap free
Amount of items: 1
Items: 
Size: 676387 Color: 3

Bin 4016: 323737 of cap free
Amount of items: 1
Items: 
Size: 676264 Color: 2

Bin 4017: 323814 of cap free
Amount of items: 1
Items: 
Size: 676187 Color: 2

Bin 4018: 323897 of cap free
Amount of items: 1
Items: 
Size: 676104 Color: 1

Bin 4019: 324244 of cap free
Amount of items: 1
Items: 
Size: 675757 Color: 3

Bin 4020: 324248 of cap free
Amount of items: 1
Items: 
Size: 675753 Color: 1

Bin 4021: 324296 of cap free
Amount of items: 1
Items: 
Size: 675705 Color: 0

Bin 4022: 324312 of cap free
Amount of items: 1
Items: 
Size: 675689 Color: 3

Bin 4023: 324329 of cap free
Amount of items: 1
Items: 
Size: 675672 Color: 0

Bin 4024: 324410 of cap free
Amount of items: 1
Items: 
Size: 675591 Color: 4

Bin 4025: 324411 of cap free
Amount of items: 1
Items: 
Size: 675590 Color: 0

Bin 4026: 324511 of cap free
Amount of items: 1
Items: 
Size: 675490 Color: 0

Bin 4027: 324657 of cap free
Amount of items: 1
Items: 
Size: 675344 Color: 3

Bin 4028: 324674 of cap free
Amount of items: 1
Items: 
Size: 675327 Color: 2

Bin 4029: 324695 of cap free
Amount of items: 1
Items: 
Size: 675306 Color: 1

Bin 4030: 324828 of cap free
Amount of items: 1
Items: 
Size: 675173 Color: 4

Bin 4031: 324970 of cap free
Amount of items: 1
Items: 
Size: 675031 Color: 4

Bin 4032: 325069 of cap free
Amount of items: 1
Items: 
Size: 674932 Color: 2

Bin 4033: 325075 of cap free
Amount of items: 1
Items: 
Size: 674926 Color: 4

Bin 4034: 325144 of cap free
Amount of items: 1
Items: 
Size: 674857 Color: 0

Bin 4035: 325155 of cap free
Amount of items: 1
Items: 
Size: 674846 Color: 4

Bin 4036: 325175 of cap free
Amount of items: 1
Items: 
Size: 674826 Color: 0

Bin 4037: 325192 of cap free
Amount of items: 1
Items: 
Size: 674809 Color: 1

Bin 4038: 325745 of cap free
Amount of items: 1
Items: 
Size: 674256 Color: 1

Bin 4039: 325856 of cap free
Amount of items: 1
Items: 
Size: 674145 Color: 2

Bin 4040: 325925 of cap free
Amount of items: 1
Items: 
Size: 674076 Color: 0

Bin 4041: 326006 of cap free
Amount of items: 1
Items: 
Size: 673995 Color: 2

Bin 4042: 326052 of cap free
Amount of items: 1
Items: 
Size: 673949 Color: 0

Bin 4043: 326085 of cap free
Amount of items: 1
Items: 
Size: 673916 Color: 0

Bin 4044: 326106 of cap free
Amount of items: 1
Items: 
Size: 673895 Color: 0

Bin 4045: 326137 of cap free
Amount of items: 1
Items: 
Size: 673864 Color: 2

Bin 4046: 326228 of cap free
Amount of items: 1
Items: 
Size: 673773 Color: 2

Bin 4047: 326309 of cap free
Amount of items: 1
Items: 
Size: 673692 Color: 3

Bin 4048: 326319 of cap free
Amount of items: 1
Items: 
Size: 673682 Color: 0

Bin 4049: 326341 of cap free
Amount of items: 1
Items: 
Size: 673660 Color: 0

Bin 4050: 326406 of cap free
Amount of items: 1
Items: 
Size: 673595 Color: 4

Bin 4051: 326493 of cap free
Amount of items: 1
Items: 
Size: 673508 Color: 2

Bin 4052: 326711 of cap free
Amount of items: 1
Items: 
Size: 673290 Color: 0

Bin 4053: 326714 of cap free
Amount of items: 1
Items: 
Size: 673287 Color: 4

Bin 4054: 326793 of cap free
Amount of items: 1
Items: 
Size: 673208 Color: 1

Bin 4055: 326807 of cap free
Amount of items: 1
Items: 
Size: 673194 Color: 4

Bin 4056: 326883 of cap free
Amount of items: 1
Items: 
Size: 673118 Color: 2

Bin 4057: 326941 of cap free
Amount of items: 1
Items: 
Size: 673060 Color: 1

Bin 4058: 327326 of cap free
Amount of items: 1
Items: 
Size: 672675 Color: 2

Bin 4059: 327371 of cap free
Amount of items: 1
Items: 
Size: 672630 Color: 1

Bin 4060: 327382 of cap free
Amount of items: 1
Items: 
Size: 672619 Color: 2

Bin 4061: 327403 of cap free
Amount of items: 1
Items: 
Size: 672598 Color: 3

Bin 4062: 327443 of cap free
Amount of items: 1
Items: 
Size: 672558 Color: 1

Bin 4063: 327455 of cap free
Amount of items: 1
Items: 
Size: 672546 Color: 1

Bin 4064: 327588 of cap free
Amount of items: 1
Items: 
Size: 672413 Color: 4

Bin 4065: 327589 of cap free
Amount of items: 1
Items: 
Size: 672412 Color: 3

Bin 4066: 327590 of cap free
Amount of items: 1
Items: 
Size: 672411 Color: 3

Bin 4067: 327677 of cap free
Amount of items: 1
Items: 
Size: 672324 Color: 3

Bin 4068: 327801 of cap free
Amount of items: 1
Items: 
Size: 672200 Color: 3

Bin 4069: 327819 of cap free
Amount of items: 1
Items: 
Size: 672182 Color: 3

Bin 4070: 327902 of cap free
Amount of items: 1
Items: 
Size: 672099 Color: 1

Bin 4071: 327953 of cap free
Amount of items: 1
Items: 
Size: 672048 Color: 1

Bin 4072: 327975 of cap free
Amount of items: 1
Items: 
Size: 672026 Color: 2

Bin 4073: 327984 of cap free
Amount of items: 1
Items: 
Size: 672017 Color: 4

Bin 4074: 328022 of cap free
Amount of items: 1
Items: 
Size: 671979 Color: 1

Bin 4075: 328026 of cap free
Amount of items: 1
Items: 
Size: 671975 Color: 2

Bin 4076: 328211 of cap free
Amount of items: 1
Items: 
Size: 671790 Color: 3

Bin 4077: 328387 of cap free
Amount of items: 1
Items: 
Size: 671614 Color: 4

Bin 4078: 328558 of cap free
Amount of items: 1
Items: 
Size: 671443 Color: 2

Bin 4079: 328641 of cap free
Amount of items: 1
Items: 
Size: 671360 Color: 0

Bin 4080: 328663 of cap free
Amount of items: 1
Items: 
Size: 671338 Color: 4

Bin 4081: 328694 of cap free
Amount of items: 1
Items: 
Size: 671307 Color: 3

Bin 4082: 328736 of cap free
Amount of items: 1
Items: 
Size: 671265 Color: 4

Bin 4083: 328740 of cap free
Amount of items: 1
Items: 
Size: 671261 Color: 1

Bin 4084: 328757 of cap free
Amount of items: 1
Items: 
Size: 671244 Color: 0

Bin 4085: 328794 of cap free
Amount of items: 1
Items: 
Size: 671207 Color: 2

Bin 4086: 328868 of cap free
Amount of items: 1
Items: 
Size: 671133 Color: 1

Bin 4087: 328908 of cap free
Amount of items: 1
Items: 
Size: 671093 Color: 3

Bin 4088: 328943 of cap free
Amount of items: 1
Items: 
Size: 671058 Color: 2

Bin 4089: 328948 of cap free
Amount of items: 1
Items: 
Size: 671053 Color: 1

Bin 4090: 329012 of cap free
Amount of items: 1
Items: 
Size: 670989 Color: 3

Bin 4091: 329123 of cap free
Amount of items: 1
Items: 
Size: 670878 Color: 2

Bin 4092: 329245 of cap free
Amount of items: 1
Items: 
Size: 670756 Color: 1

Bin 4093: 329319 of cap free
Amount of items: 1
Items: 
Size: 670682 Color: 4

Bin 4094: 329374 of cap free
Amount of items: 1
Items: 
Size: 670627 Color: 2

Bin 4095: 329381 of cap free
Amount of items: 1
Items: 
Size: 670620 Color: 0

Bin 4096: 329471 of cap free
Amount of items: 1
Items: 
Size: 670530 Color: 0

Bin 4097: 329601 of cap free
Amount of items: 1
Items: 
Size: 670400 Color: 2

Bin 4098: 329718 of cap free
Amount of items: 1
Items: 
Size: 670283 Color: 3

Bin 4099: 329775 of cap free
Amount of items: 1
Items: 
Size: 670226 Color: 2

Bin 4100: 329813 of cap free
Amount of items: 1
Items: 
Size: 670188 Color: 0

Bin 4101: 329843 of cap free
Amount of items: 1
Items: 
Size: 670158 Color: 4

Bin 4102: 329960 of cap free
Amount of items: 1
Items: 
Size: 670041 Color: 2

Bin 4103: 330024 of cap free
Amount of items: 1
Items: 
Size: 669977 Color: 0

Bin 4104: 330047 of cap free
Amount of items: 1
Items: 
Size: 669954 Color: 4

Bin 4105: 330263 of cap free
Amount of items: 1
Items: 
Size: 669738 Color: 0

Bin 4106: 330282 of cap free
Amount of items: 1
Items: 
Size: 669719 Color: 2

Bin 4107: 330445 of cap free
Amount of items: 1
Items: 
Size: 669556 Color: 4

Bin 4108: 330452 of cap free
Amount of items: 1
Items: 
Size: 669549 Color: 1

Bin 4109: 330461 of cap free
Amount of items: 1
Items: 
Size: 669540 Color: 2

Bin 4110: 330502 of cap free
Amount of items: 1
Items: 
Size: 669499 Color: 2

Bin 4111: 330567 of cap free
Amount of items: 1
Items: 
Size: 669434 Color: 3

Bin 4112: 330713 of cap free
Amount of items: 1
Items: 
Size: 669288 Color: 1

Bin 4113: 330715 of cap free
Amount of items: 1
Items: 
Size: 669286 Color: 2

Bin 4114: 330728 of cap free
Amount of items: 1
Items: 
Size: 669273 Color: 4

Bin 4115: 331033 of cap free
Amount of items: 1
Items: 
Size: 668968 Color: 2

Bin 4116: 331177 of cap free
Amount of items: 1
Items: 
Size: 668824 Color: 1

Bin 4117: 331179 of cap free
Amount of items: 1
Items: 
Size: 668822 Color: 3

Bin 4118: 331272 of cap free
Amount of items: 1
Items: 
Size: 668729 Color: 0

Bin 4119: 331276 of cap free
Amount of items: 1
Items: 
Size: 668725 Color: 1

Bin 4120: 331348 of cap free
Amount of items: 1
Items: 
Size: 668653 Color: 2

Bin 4121: 331349 of cap free
Amount of items: 1
Items: 
Size: 668652 Color: 2

Bin 4122: 331371 of cap free
Amount of items: 1
Items: 
Size: 668630 Color: 1

Bin 4123: 331501 of cap free
Amount of items: 1
Items: 
Size: 668500 Color: 4

Bin 4124: 331516 of cap free
Amount of items: 1
Items: 
Size: 668485 Color: 2

Bin 4125: 331524 of cap free
Amount of items: 1
Items: 
Size: 668477 Color: 4

Bin 4126: 331717 of cap free
Amount of items: 1
Items: 
Size: 668284 Color: 0

Bin 4127: 331745 of cap free
Amount of items: 1
Items: 
Size: 668256 Color: 4

Bin 4128: 331781 of cap free
Amount of items: 1
Items: 
Size: 668220 Color: 2

Bin 4129: 331792 of cap free
Amount of items: 1
Items: 
Size: 668209 Color: 3

Bin 4130: 331792 of cap free
Amount of items: 1
Items: 
Size: 668209 Color: 3

Bin 4131: 331798 of cap free
Amount of items: 1
Items: 
Size: 668203 Color: 1

Bin 4132: 331810 of cap free
Amount of items: 1
Items: 
Size: 668191 Color: 4

Bin 4133: 332008 of cap free
Amount of items: 1
Items: 
Size: 667993 Color: 4

Bin 4134: 332124 of cap free
Amount of items: 1
Items: 
Size: 667877 Color: 3

Bin 4135: 332285 of cap free
Amount of items: 1
Items: 
Size: 667716 Color: 0

Bin 4136: 332310 of cap free
Amount of items: 1
Items: 
Size: 667691 Color: 4

Bin 4137: 332356 of cap free
Amount of items: 1
Items: 
Size: 667645 Color: 1

Bin 4138: 332405 of cap free
Amount of items: 1
Items: 
Size: 667596 Color: 0

Bin 4139: 332524 of cap free
Amount of items: 1
Items: 
Size: 667477 Color: 3

Bin 4140: 332589 of cap free
Amount of items: 1
Items: 
Size: 667412 Color: 4

Bin 4141: 332717 of cap free
Amount of items: 1
Items: 
Size: 667284 Color: 0

Bin 4142: 332744 of cap free
Amount of items: 1
Items: 
Size: 667257 Color: 0

Bin 4143: 332764 of cap free
Amount of items: 1
Items: 
Size: 667237 Color: 3

Bin 4144: 332845 of cap free
Amount of items: 1
Items: 
Size: 667156 Color: 4

Bin 4145: 332855 of cap free
Amount of items: 1
Items: 
Size: 667146 Color: 0

Bin 4146: 332928 of cap free
Amount of items: 1
Items: 
Size: 667073 Color: 2

Bin 4147: 332928 of cap free
Amount of items: 1
Items: 
Size: 667073 Color: 4

Bin 4148: 332935 of cap free
Amount of items: 1
Items: 
Size: 667066 Color: 1

Bin 4149: 332960 of cap free
Amount of items: 1
Items: 
Size: 667041 Color: 2

Bin 4150: 332965 of cap free
Amount of items: 1
Items: 
Size: 667036 Color: 3

Bin 4151: 333000 of cap free
Amount of items: 1
Items: 
Size: 667001 Color: 0

Bin 4152: 333137 of cap free
Amount of items: 1
Items: 
Size: 666864 Color: 2

Bin 4153: 333218 of cap free
Amount of items: 1
Items: 
Size: 666783 Color: 1

Bin 4154: 333315 of cap free
Amount of items: 1
Items: 
Size: 666686 Color: 2

Bin 4155: 333328 of cap free
Amount of items: 1
Items: 
Size: 666673 Color: 2

Bin 4156: 333441 of cap free
Amount of items: 1
Items: 
Size: 666560 Color: 3

Bin 4157: 333457 of cap free
Amount of items: 1
Items: 
Size: 666544 Color: 1

Bin 4158: 333472 of cap free
Amount of items: 1
Items: 
Size: 666529 Color: 4

Bin 4159: 333504 of cap free
Amount of items: 1
Items: 
Size: 666497 Color: 3

Bin 4160: 333618 of cap free
Amount of items: 1
Items: 
Size: 666383 Color: 2

Bin 4161: 333647 of cap free
Amount of items: 1
Items: 
Size: 666354 Color: 4

Bin 4162: 333740 of cap free
Amount of items: 1
Items: 
Size: 666261 Color: 1

Bin 4163: 333772 of cap free
Amount of items: 1
Items: 
Size: 666229 Color: 0

Bin 4164: 333776 of cap free
Amount of items: 1
Items: 
Size: 666225 Color: 3

Bin 4165: 333781 of cap free
Amount of items: 1
Items: 
Size: 666220 Color: 2

Bin 4166: 333863 of cap free
Amount of items: 1
Items: 
Size: 666138 Color: 0

Bin 4167: 333942 of cap free
Amount of items: 1
Items: 
Size: 666059 Color: 1

Bin 4168: 333956 of cap free
Amount of items: 1
Items: 
Size: 666045 Color: 1

Bin 4169: 334098 of cap free
Amount of items: 1
Items: 
Size: 665903 Color: 3

Bin 4170: 334118 of cap free
Amount of items: 1
Items: 
Size: 665883 Color: 0

Bin 4171: 334124 of cap free
Amount of items: 1
Items: 
Size: 665877 Color: 1

Bin 4172: 334211 of cap free
Amount of items: 1
Items: 
Size: 665790 Color: 0

Bin 4173: 334257 of cap free
Amount of items: 1
Items: 
Size: 665744 Color: 3

Bin 4174: 334358 of cap free
Amount of items: 1
Items: 
Size: 665643 Color: 2

Bin 4175: 334497 of cap free
Amount of items: 1
Items: 
Size: 665504 Color: 1

Bin 4176: 334534 of cap free
Amount of items: 1
Items: 
Size: 665467 Color: 4

Bin 4177: 334563 of cap free
Amount of items: 1
Items: 
Size: 665438 Color: 4

Bin 4178: 334782 of cap free
Amount of items: 1
Items: 
Size: 665219 Color: 2

Bin 4179: 334785 of cap free
Amount of items: 1
Items: 
Size: 665216 Color: 1

Bin 4180: 334862 of cap free
Amount of items: 1
Items: 
Size: 665139 Color: 1

Bin 4181: 334931 of cap free
Amount of items: 1
Items: 
Size: 665070 Color: 0

Bin 4182: 335010 of cap free
Amount of items: 1
Items: 
Size: 664991 Color: 0

Bin 4183: 335253 of cap free
Amount of items: 1
Items: 
Size: 664748 Color: 4

Bin 4184: 335279 of cap free
Amount of items: 1
Items: 
Size: 664722 Color: 0

Bin 4185: 335298 of cap free
Amount of items: 1
Items: 
Size: 664703 Color: 1

Bin 4186: 335334 of cap free
Amount of items: 1
Items: 
Size: 664667 Color: 3

Bin 4187: 335500 of cap free
Amount of items: 1
Items: 
Size: 664501 Color: 1

Bin 4188: 335512 of cap free
Amount of items: 1
Items: 
Size: 664489 Color: 1

Bin 4189: 335531 of cap free
Amount of items: 1
Items: 
Size: 664470 Color: 0

Bin 4190: 335620 of cap free
Amount of items: 1
Items: 
Size: 664381 Color: 0

Bin 4191: 335637 of cap free
Amount of items: 1
Items: 
Size: 664364 Color: 4

Bin 4192: 335818 of cap free
Amount of items: 1
Items: 
Size: 664183 Color: 4

Bin 4193: 336263 of cap free
Amount of items: 1
Items: 
Size: 663738 Color: 3

Bin 4194: 336276 of cap free
Amount of items: 1
Items: 
Size: 663725 Color: 1

Bin 4195: 336276 of cap free
Amount of items: 1
Items: 
Size: 663725 Color: 2

Bin 4196: 336331 of cap free
Amount of items: 1
Items: 
Size: 663670 Color: 3

Bin 4197: 336387 of cap free
Amount of items: 1
Items: 
Size: 663614 Color: 0

Bin 4198: 336431 of cap free
Amount of items: 1
Items: 
Size: 663570 Color: 2

Bin 4199: 336458 of cap free
Amount of items: 1
Items: 
Size: 663543 Color: 0

Bin 4200: 336470 of cap free
Amount of items: 1
Items: 
Size: 663531 Color: 2

Bin 4201: 336475 of cap free
Amount of items: 1
Items: 
Size: 663526 Color: 0

Bin 4202: 336507 of cap free
Amount of items: 1
Items: 
Size: 663494 Color: 3

Bin 4203: 336565 of cap free
Amount of items: 1
Items: 
Size: 663436 Color: 2

Bin 4204: 336599 of cap free
Amount of items: 1
Items: 
Size: 663402 Color: 4

Bin 4205: 336741 of cap free
Amount of items: 1
Items: 
Size: 663260 Color: 2

Bin 4206: 336841 of cap free
Amount of items: 1
Items: 
Size: 663160 Color: 1

Bin 4207: 336846 of cap free
Amount of items: 1
Items: 
Size: 663155 Color: 4

Bin 4208: 336864 of cap free
Amount of items: 1
Items: 
Size: 663137 Color: 3

Bin 4209: 336974 of cap free
Amount of items: 1
Items: 
Size: 663027 Color: 4

Bin 4210: 337057 of cap free
Amount of items: 1
Items: 
Size: 662944 Color: 2

Bin 4211: 337492 of cap free
Amount of items: 1
Items: 
Size: 662509 Color: 3

Bin 4212: 337736 of cap free
Amount of items: 1
Items: 
Size: 662265 Color: 3

Bin 4213: 337841 of cap free
Amount of items: 1
Items: 
Size: 662160 Color: 4

Bin 4214: 337864 of cap free
Amount of items: 1
Items: 
Size: 662137 Color: 1

Bin 4215: 337866 of cap free
Amount of items: 1
Items: 
Size: 662135 Color: 2

Bin 4216: 337903 of cap free
Amount of items: 1
Items: 
Size: 662098 Color: 2

Bin 4217: 337958 of cap free
Amount of items: 1
Items: 
Size: 662043 Color: 0

Bin 4218: 337965 of cap free
Amount of items: 1
Items: 
Size: 662036 Color: 2

Bin 4219: 338037 of cap free
Amount of items: 1
Items: 
Size: 661964 Color: 3

Bin 4220: 338070 of cap free
Amount of items: 1
Items: 
Size: 661931 Color: 3

Bin 4221: 338292 of cap free
Amount of items: 1
Items: 
Size: 661709 Color: 0

Bin 4222: 338330 of cap free
Amount of items: 1
Items: 
Size: 661671 Color: 4

Bin 4223: 338339 of cap free
Amount of items: 1
Items: 
Size: 661662 Color: 2

Bin 4224: 338485 of cap free
Amount of items: 1
Items: 
Size: 661516 Color: 3

Bin 4225: 338808 of cap free
Amount of items: 1
Items: 
Size: 661193 Color: 1

Bin 4226: 338843 of cap free
Amount of items: 1
Items: 
Size: 661158 Color: 1

Bin 4227: 338854 of cap free
Amount of items: 1
Items: 
Size: 661147 Color: 0

Bin 4228: 338874 of cap free
Amount of items: 1
Items: 
Size: 661127 Color: 3

Bin 4229: 338913 of cap free
Amount of items: 1
Items: 
Size: 661088 Color: 4

Bin 4230: 338995 of cap free
Amount of items: 1
Items: 
Size: 661006 Color: 2

Bin 4231: 339043 of cap free
Amount of items: 1
Items: 
Size: 660958 Color: 3

Bin 4232: 339135 of cap free
Amount of items: 1
Items: 
Size: 660866 Color: 3

Bin 4233: 339138 of cap free
Amount of items: 1
Items: 
Size: 660863 Color: 2

Bin 4234: 339219 of cap free
Amount of items: 1
Items: 
Size: 660782 Color: 4

Bin 4235: 339272 of cap free
Amount of items: 1
Items: 
Size: 660729 Color: 1

Bin 4236: 339276 of cap free
Amount of items: 1
Items: 
Size: 660725 Color: 4

Bin 4237: 339293 of cap free
Amount of items: 1
Items: 
Size: 660708 Color: 0

Bin 4238: 339295 of cap free
Amount of items: 1
Items: 
Size: 660706 Color: 0

Bin 4239: 339333 of cap free
Amount of items: 1
Items: 
Size: 660668 Color: 0

Bin 4240: 339343 of cap free
Amount of items: 1
Items: 
Size: 660658 Color: 0

Bin 4241: 339375 of cap free
Amount of items: 1
Items: 
Size: 660626 Color: 0

Bin 4242: 339404 of cap free
Amount of items: 1
Items: 
Size: 660597 Color: 4

Bin 4243: 339675 of cap free
Amount of items: 1
Items: 
Size: 660326 Color: 3

Bin 4244: 339693 of cap free
Amount of items: 1
Items: 
Size: 660308 Color: 0

Bin 4245: 339745 of cap free
Amount of items: 1
Items: 
Size: 660256 Color: 0

Bin 4246: 339750 of cap free
Amount of items: 1
Items: 
Size: 660251 Color: 2

Bin 4247: 339857 of cap free
Amount of items: 1
Items: 
Size: 660144 Color: 2

Bin 4248: 339865 of cap free
Amount of items: 1
Items: 
Size: 660136 Color: 2

Bin 4249: 340240 of cap free
Amount of items: 1
Items: 
Size: 659761 Color: 3

Bin 4250: 340288 of cap free
Amount of items: 1
Items: 
Size: 659713 Color: 1

Bin 4251: 340289 of cap free
Amount of items: 1
Items: 
Size: 659712 Color: 0

Bin 4252: 340346 of cap free
Amount of items: 1
Items: 
Size: 659655 Color: 1

Bin 4253: 340368 of cap free
Amount of items: 1
Items: 
Size: 659633 Color: 1

Bin 4254: 340383 of cap free
Amount of items: 1
Items: 
Size: 659618 Color: 2

Bin 4255: 340415 of cap free
Amount of items: 1
Items: 
Size: 659586 Color: 2

Bin 4256: 340482 of cap free
Amount of items: 1
Items: 
Size: 659519 Color: 0

Bin 4257: 340552 of cap free
Amount of items: 1
Items: 
Size: 659449 Color: 0

Bin 4258: 340619 of cap free
Amount of items: 1
Items: 
Size: 659382 Color: 1

Bin 4259: 340677 of cap free
Amount of items: 1
Items: 
Size: 659324 Color: 1

Bin 4260: 340748 of cap free
Amount of items: 1
Items: 
Size: 659253 Color: 0

Bin 4261: 341016 of cap free
Amount of items: 1
Items: 
Size: 658985 Color: 3

Bin 4262: 341195 of cap free
Amount of items: 1
Items: 
Size: 658806 Color: 1

Bin 4263: 341276 of cap free
Amount of items: 1
Items: 
Size: 658725 Color: 4

Bin 4264: 341277 of cap free
Amount of items: 1
Items: 
Size: 658724 Color: 2

Bin 4265: 341335 of cap free
Amount of items: 1
Items: 
Size: 658666 Color: 1

Bin 4266: 341352 of cap free
Amount of items: 1
Items: 
Size: 658649 Color: 4

Bin 4267: 341363 of cap free
Amount of items: 1
Items: 
Size: 658638 Color: 0

Bin 4268: 341426 of cap free
Amount of items: 1
Items: 
Size: 658575 Color: 1

Bin 4269: 341519 of cap free
Amount of items: 1
Items: 
Size: 658482 Color: 2

Bin 4270: 341593 of cap free
Amount of items: 1
Items: 
Size: 658408 Color: 2

Bin 4271: 341649 of cap free
Amount of items: 1
Items: 
Size: 658352 Color: 0

Bin 4272: 341660 of cap free
Amount of items: 1
Items: 
Size: 658341 Color: 0

Bin 4273: 341732 of cap free
Amount of items: 1
Items: 
Size: 658269 Color: 3

Bin 4274: 341736 of cap free
Amount of items: 1
Items: 
Size: 658265 Color: 3

Bin 4275: 341917 of cap free
Amount of items: 1
Items: 
Size: 658084 Color: 2

Bin 4276: 341923 of cap free
Amount of items: 1
Items: 
Size: 658078 Color: 0

Bin 4277: 341942 of cap free
Amount of items: 1
Items: 
Size: 658059 Color: 1

Bin 4278: 342027 of cap free
Amount of items: 1
Items: 
Size: 657974 Color: 2

Bin 4279: 342111 of cap free
Amount of items: 1
Items: 
Size: 657890 Color: 1

Bin 4280: 342159 of cap free
Amount of items: 1
Items: 
Size: 657842 Color: 4

Bin 4281: 342231 of cap free
Amount of items: 1
Items: 
Size: 657770 Color: 0

Bin 4282: 342447 of cap free
Amount of items: 1
Items: 
Size: 657554 Color: 3

Bin 4283: 342546 of cap free
Amount of items: 1
Items: 
Size: 657455 Color: 0

Bin 4284: 342605 of cap free
Amount of items: 1
Items: 
Size: 657396 Color: 4

Bin 4285: 342684 of cap free
Amount of items: 1
Items: 
Size: 657317 Color: 3

Bin 4286: 342893 of cap free
Amount of items: 1
Items: 
Size: 657108 Color: 2

Bin 4287: 342914 of cap free
Amount of items: 1
Items: 
Size: 657087 Color: 3

Bin 4288: 342928 of cap free
Amount of items: 1
Items: 
Size: 657073 Color: 4

Bin 4289: 342956 of cap free
Amount of items: 1
Items: 
Size: 657045 Color: 0

Bin 4290: 342958 of cap free
Amount of items: 1
Items: 
Size: 657043 Color: 1

Bin 4291: 343018 of cap free
Amount of items: 1
Items: 
Size: 656983 Color: 1

Bin 4292: 343088 of cap free
Amount of items: 1
Items: 
Size: 656913 Color: 1

Bin 4293: 343111 of cap free
Amount of items: 1
Items: 
Size: 656890 Color: 3

Bin 4294: 343185 of cap free
Amount of items: 1
Items: 
Size: 656816 Color: 1

Bin 4295: 343241 of cap free
Amount of items: 1
Items: 
Size: 656760 Color: 0

Bin 4296: 343285 of cap free
Amount of items: 1
Items: 
Size: 656716 Color: 0

Bin 4297: 343395 of cap free
Amount of items: 1
Items: 
Size: 656606 Color: 0

Bin 4298: 343548 of cap free
Amount of items: 1
Items: 
Size: 656453 Color: 1

Bin 4299: 343582 of cap free
Amount of items: 1
Items: 
Size: 656419 Color: 4

Bin 4300: 343842 of cap free
Amount of items: 1
Items: 
Size: 656159 Color: 0

Bin 4301: 344025 of cap free
Amount of items: 1
Items: 
Size: 655976 Color: 4

Bin 4302: 344189 of cap free
Amount of items: 1
Items: 
Size: 655812 Color: 0

Bin 4303: 344215 of cap free
Amount of items: 1
Items: 
Size: 655786 Color: 2

Bin 4304: 344299 of cap free
Amount of items: 1
Items: 
Size: 655702 Color: 0

Bin 4305: 344315 of cap free
Amount of items: 1
Items: 
Size: 655686 Color: 1

Bin 4306: 344335 of cap free
Amount of items: 1
Items: 
Size: 655666 Color: 3

Bin 4307: 344384 of cap free
Amount of items: 1
Items: 
Size: 655617 Color: 2

Bin 4308: 344507 of cap free
Amount of items: 1
Items: 
Size: 655494 Color: 3

Bin 4309: 344620 of cap free
Amount of items: 1
Items: 
Size: 655381 Color: 0

Bin 4310: 344628 of cap free
Amount of items: 1
Items: 
Size: 655373 Color: 2

Bin 4311: 344644 of cap free
Amount of items: 1
Items: 
Size: 655357 Color: 4

Bin 4312: 344771 of cap free
Amount of items: 1
Items: 
Size: 655230 Color: 2

Bin 4313: 344891 of cap free
Amount of items: 1
Items: 
Size: 655110 Color: 1

Bin 4314: 344908 of cap free
Amount of items: 1
Items: 
Size: 655093 Color: 4

Bin 4315: 345031 of cap free
Amount of items: 1
Items: 
Size: 654970 Color: 0

Bin 4316: 345055 of cap free
Amount of items: 1
Items: 
Size: 654946 Color: 0

Bin 4317: 345076 of cap free
Amount of items: 1
Items: 
Size: 654925 Color: 4

Bin 4318: 345233 of cap free
Amount of items: 1
Items: 
Size: 654768 Color: 0

Bin 4319: 345307 of cap free
Amount of items: 1
Items: 
Size: 654694 Color: 1

Bin 4320: 345312 of cap free
Amount of items: 1
Items: 
Size: 654689 Color: 1

Bin 4321: 345489 of cap free
Amount of items: 1
Items: 
Size: 654512 Color: 3

Bin 4322: 345593 of cap free
Amount of items: 1
Items: 
Size: 654408 Color: 4

Bin 4323: 345608 of cap free
Amount of items: 1
Items: 
Size: 654393 Color: 1

Bin 4324: 345654 of cap free
Amount of items: 1
Items: 
Size: 654347 Color: 1

Bin 4325: 345714 of cap free
Amount of items: 1
Items: 
Size: 654287 Color: 0

Bin 4326: 345871 of cap free
Amount of items: 1
Items: 
Size: 654130 Color: 0

Bin 4327: 345923 of cap free
Amount of items: 1
Items: 
Size: 654078 Color: 1

Bin 4328: 345923 of cap free
Amount of items: 1
Items: 
Size: 654078 Color: 1

Bin 4329: 345957 of cap free
Amount of items: 1
Items: 
Size: 654044 Color: 2

Bin 4330: 346161 of cap free
Amount of items: 1
Items: 
Size: 653840 Color: 3

Bin 4331: 346165 of cap free
Amount of items: 1
Items: 
Size: 653836 Color: 4

Bin 4332: 346185 of cap free
Amount of items: 1
Items: 
Size: 653816 Color: 3

Bin 4333: 346205 of cap free
Amount of items: 1
Items: 
Size: 653796 Color: 1

Bin 4334: 346288 of cap free
Amount of items: 1
Items: 
Size: 653713 Color: 2

Bin 4335: 346325 of cap free
Amount of items: 1
Items: 
Size: 653676 Color: 2

Bin 4336: 346358 of cap free
Amount of items: 1
Items: 
Size: 653643 Color: 0

Bin 4337: 346519 of cap free
Amount of items: 1
Items: 
Size: 653482 Color: 2

Bin 4338: 346552 of cap free
Amount of items: 1
Items: 
Size: 653449 Color: 0

Bin 4339: 346615 of cap free
Amount of items: 1
Items: 
Size: 653386 Color: 2

Bin 4340: 346638 of cap free
Amount of items: 1
Items: 
Size: 653363 Color: 4

Bin 4341: 346672 of cap free
Amount of items: 1
Items: 
Size: 653329 Color: 0

Bin 4342: 346676 of cap free
Amount of items: 1
Items: 
Size: 653325 Color: 3

Bin 4343: 346819 of cap free
Amount of items: 1
Items: 
Size: 653182 Color: 4

Bin 4344: 346836 of cap free
Amount of items: 1
Items: 
Size: 653165 Color: 3

Bin 4345: 346888 of cap free
Amount of items: 1
Items: 
Size: 653113 Color: 3

Bin 4346: 346907 of cap free
Amount of items: 1
Items: 
Size: 653094 Color: 1

Bin 4347: 347106 of cap free
Amount of items: 1
Items: 
Size: 652895 Color: 1

Bin 4348: 347110 of cap free
Amount of items: 1
Items: 
Size: 652891 Color: 2

Bin 4349: 347165 of cap free
Amount of items: 1
Items: 
Size: 652836 Color: 1

Bin 4350: 347169 of cap free
Amount of items: 1
Items: 
Size: 652832 Color: 4

Bin 4351: 347170 of cap free
Amount of items: 1
Items: 
Size: 652831 Color: 2

Bin 4352: 347241 of cap free
Amount of items: 1
Items: 
Size: 652760 Color: 1

Bin 4353: 347276 of cap free
Amount of items: 1
Items: 
Size: 652725 Color: 0

Bin 4354: 347283 of cap free
Amount of items: 1
Items: 
Size: 652718 Color: 4

Bin 4355: 347499 of cap free
Amount of items: 1
Items: 
Size: 652502 Color: 4

Bin 4356: 347621 of cap free
Amount of items: 1
Items: 
Size: 652380 Color: 3

Bin 4357: 347754 of cap free
Amount of items: 1
Items: 
Size: 652247 Color: 3

Bin 4358: 348011 of cap free
Amount of items: 1
Items: 
Size: 651990 Color: 2

Bin 4359: 348097 of cap free
Amount of items: 1
Items: 
Size: 651904 Color: 2

Bin 4360: 348153 of cap free
Amount of items: 1
Items: 
Size: 651848 Color: 2

Bin 4361: 348329 of cap free
Amount of items: 1
Items: 
Size: 651672 Color: 0

Bin 4362: 348385 of cap free
Amount of items: 1
Items: 
Size: 651616 Color: 2

Bin 4363: 348387 of cap free
Amount of items: 1
Items: 
Size: 651614 Color: 4

Bin 4364: 348406 of cap free
Amount of items: 1
Items: 
Size: 651595 Color: 4

Bin 4365: 348411 of cap free
Amount of items: 1
Items: 
Size: 651590 Color: 2

Bin 4366: 348470 of cap free
Amount of items: 1
Items: 
Size: 651531 Color: 1

Bin 4367: 348573 of cap free
Amount of items: 1
Items: 
Size: 651428 Color: 3

Bin 4368: 348782 of cap free
Amount of items: 1
Items: 
Size: 651219 Color: 2

Bin 4369: 348784 of cap free
Amount of items: 1
Items: 
Size: 651217 Color: 2

Bin 4370: 348790 of cap free
Amount of items: 1
Items: 
Size: 651211 Color: 1

Bin 4371: 348805 of cap free
Amount of items: 1
Items: 
Size: 651196 Color: 1

Bin 4372: 348829 of cap free
Amount of items: 1
Items: 
Size: 651172 Color: 1

Bin 4373: 348866 of cap free
Amount of items: 1
Items: 
Size: 651135 Color: 2

Bin 4374: 348921 of cap free
Amount of items: 1
Items: 
Size: 651080 Color: 4

Bin 4375: 348954 of cap free
Amount of items: 1
Items: 
Size: 651047 Color: 2

Bin 4376: 348967 of cap free
Amount of items: 1
Items: 
Size: 651034 Color: 3

Bin 4377: 348985 of cap free
Amount of items: 1
Items: 
Size: 651016 Color: 3

Bin 4378: 349003 of cap free
Amount of items: 1
Items: 
Size: 650998 Color: 0

Bin 4379: 349008 of cap free
Amount of items: 1
Items: 
Size: 650993 Color: 0

Bin 4380: 349042 of cap free
Amount of items: 1
Items: 
Size: 650959 Color: 1

Bin 4381: 349067 of cap free
Amount of items: 1
Items: 
Size: 650934 Color: 4

Bin 4382: 349106 of cap free
Amount of items: 1
Items: 
Size: 650895 Color: 2

Bin 4383: 349137 of cap free
Amount of items: 1
Items: 
Size: 650864 Color: 0

Bin 4384: 349158 of cap free
Amount of items: 1
Items: 
Size: 650843 Color: 4

Bin 4385: 349338 of cap free
Amount of items: 1
Items: 
Size: 650663 Color: 3

Bin 4386: 349376 of cap free
Amount of items: 1
Items: 
Size: 650625 Color: 3

Bin 4387: 349465 of cap free
Amount of items: 1
Items: 
Size: 650536 Color: 3

Bin 4388: 349507 of cap free
Amount of items: 1
Items: 
Size: 650494 Color: 0

Bin 4389: 349533 of cap free
Amount of items: 1
Items: 
Size: 650468 Color: 1

Bin 4390: 349793 of cap free
Amount of items: 1
Items: 
Size: 650208 Color: 4

Bin 4391: 349930 of cap free
Amount of items: 1
Items: 
Size: 650071 Color: 0

Bin 4392: 350022 of cap free
Amount of items: 1
Items: 
Size: 649979 Color: 4

Bin 4393: 350023 of cap free
Amount of items: 1
Items: 
Size: 649978 Color: 2

Bin 4394: 350030 of cap free
Amount of items: 1
Items: 
Size: 649971 Color: 2

Bin 4395: 350061 of cap free
Amount of items: 1
Items: 
Size: 649940 Color: 4

Bin 4396: 350108 of cap free
Amount of items: 1
Items: 
Size: 649893 Color: 2

Bin 4397: 350110 of cap free
Amount of items: 1
Items: 
Size: 649891 Color: 1

Bin 4398: 350137 of cap free
Amount of items: 1
Items: 
Size: 649864 Color: 0

Bin 4399: 350183 of cap free
Amount of items: 1
Items: 
Size: 649818 Color: 1

Bin 4400: 350198 of cap free
Amount of items: 1
Items: 
Size: 649803 Color: 1

Bin 4401: 350287 of cap free
Amount of items: 1
Items: 
Size: 649714 Color: 0

Bin 4402: 350323 of cap free
Amount of items: 1
Items: 
Size: 649678 Color: 3

Bin 4403: 350443 of cap free
Amount of items: 1
Items: 
Size: 649558 Color: 0

Bin 4404: 350471 of cap free
Amount of items: 1
Items: 
Size: 649530 Color: 1

Bin 4405: 350494 of cap free
Amount of items: 1
Items: 
Size: 649507 Color: 0

Bin 4406: 350567 of cap free
Amount of items: 1
Items: 
Size: 649434 Color: 4

Bin 4407: 350882 of cap free
Amount of items: 1
Items: 
Size: 649119 Color: 3

Bin 4408: 350978 of cap free
Amount of items: 1
Items: 
Size: 649023 Color: 3

Bin 4409: 351007 of cap free
Amount of items: 1
Items: 
Size: 648994 Color: 0

Bin 4410: 351156 of cap free
Amount of items: 1
Items: 
Size: 648845 Color: 1

Bin 4411: 351266 of cap free
Amount of items: 1
Items: 
Size: 648735 Color: 4

Bin 4412: 351271 of cap free
Amount of items: 1
Items: 
Size: 648730 Color: 4

Bin 4413: 351347 of cap free
Amount of items: 1
Items: 
Size: 648654 Color: 4

Bin 4414: 351402 of cap free
Amount of items: 1
Items: 
Size: 648599 Color: 1

Bin 4415: 351550 of cap free
Amount of items: 1
Items: 
Size: 648451 Color: 1

Bin 4416: 351693 of cap free
Amount of items: 1
Items: 
Size: 648308 Color: 1

Bin 4417: 351704 of cap free
Amount of items: 1
Items: 
Size: 648297 Color: 3

Bin 4418: 351708 of cap free
Amount of items: 1
Items: 
Size: 648293 Color: 1

Bin 4419: 351738 of cap free
Amount of items: 1
Items: 
Size: 648263 Color: 1

Bin 4420: 351799 of cap free
Amount of items: 1
Items: 
Size: 648202 Color: 3

Bin 4421: 351803 of cap free
Amount of items: 1
Items: 
Size: 648198 Color: 4

Bin 4422: 351821 of cap free
Amount of items: 1
Items: 
Size: 648180 Color: 0

Bin 4423: 351884 of cap free
Amount of items: 1
Items: 
Size: 648117 Color: 3

Bin 4424: 351986 of cap free
Amount of items: 1
Items: 
Size: 648015 Color: 2

Bin 4425: 352014 of cap free
Amount of items: 1
Items: 
Size: 647987 Color: 0

Bin 4426: 352197 of cap free
Amount of items: 1
Items: 
Size: 647804 Color: 4

Bin 4427: 352278 of cap free
Amount of items: 1
Items: 
Size: 647723 Color: 4

Bin 4428: 352287 of cap free
Amount of items: 1
Items: 
Size: 647714 Color: 3

Bin 4429: 352501 of cap free
Amount of items: 1
Items: 
Size: 647500 Color: 4

Bin 4430: 352520 of cap free
Amount of items: 1
Items: 
Size: 647481 Color: 1

Bin 4431: 352672 of cap free
Amount of items: 1
Items: 
Size: 647329 Color: 4

Bin 4432: 352698 of cap free
Amount of items: 1
Items: 
Size: 647303 Color: 2

Bin 4433: 352791 of cap free
Amount of items: 1
Items: 
Size: 647210 Color: 0

Bin 4434: 352831 of cap free
Amount of items: 1
Items: 
Size: 647170 Color: 1

Bin 4435: 352880 of cap free
Amount of items: 1
Items: 
Size: 647121 Color: 1

Bin 4436: 352922 of cap free
Amount of items: 1
Items: 
Size: 647079 Color: 4

Bin 4437: 352929 of cap free
Amount of items: 1
Items: 
Size: 647072 Color: 3

Bin 4438: 352937 of cap free
Amount of items: 1
Items: 
Size: 647064 Color: 3

Bin 4439: 353094 of cap free
Amount of items: 1
Items: 
Size: 646907 Color: 3

Bin 4440: 353172 of cap free
Amount of items: 1
Items: 
Size: 646829 Color: 4

Bin 4441: 353322 of cap free
Amount of items: 1
Items: 
Size: 646679 Color: 4

Bin 4442: 353347 of cap free
Amount of items: 1
Items: 
Size: 646654 Color: 2

Bin 4443: 353413 of cap free
Amount of items: 1
Items: 
Size: 646588 Color: 0

Bin 4444: 353454 of cap free
Amount of items: 1
Items: 
Size: 646547 Color: 3

Bin 4445: 353461 of cap free
Amount of items: 1
Items: 
Size: 646540 Color: 4

Bin 4446: 353503 of cap free
Amount of items: 1
Items: 
Size: 646498 Color: 1

Bin 4447: 353585 of cap free
Amount of items: 1
Items: 
Size: 646416 Color: 4

Bin 4448: 353651 of cap free
Amount of items: 1
Items: 
Size: 646350 Color: 0

Bin 4449: 353694 of cap free
Amount of items: 1
Items: 
Size: 646307 Color: 0

Bin 4450: 353872 of cap free
Amount of items: 1
Items: 
Size: 646129 Color: 3

Bin 4451: 354016 of cap free
Amount of items: 1
Items: 
Size: 645985 Color: 3

Bin 4452: 354035 of cap free
Amount of items: 1
Items: 
Size: 645966 Color: 0

Bin 4453: 354117 of cap free
Amount of items: 1
Items: 
Size: 645884 Color: 0

Bin 4454: 354234 of cap free
Amount of items: 1
Items: 
Size: 645767 Color: 3

Bin 4455: 354298 of cap free
Amount of items: 1
Items: 
Size: 645703 Color: 1

Bin 4456: 354330 of cap free
Amount of items: 1
Items: 
Size: 645671 Color: 4

Bin 4457: 354333 of cap free
Amount of items: 1
Items: 
Size: 645668 Color: 4

Bin 4458: 354375 of cap free
Amount of items: 1
Items: 
Size: 645626 Color: 1

Bin 4459: 354475 of cap free
Amount of items: 1
Items: 
Size: 645526 Color: 0

Bin 4460: 354584 of cap free
Amount of items: 1
Items: 
Size: 645417 Color: 0

Bin 4461: 354638 of cap free
Amount of items: 1
Items: 
Size: 645363 Color: 4

Bin 4462: 354686 of cap free
Amount of items: 1
Items: 
Size: 645315 Color: 4

Bin 4463: 354692 of cap free
Amount of items: 1
Items: 
Size: 645309 Color: 3

Bin 4464: 354745 of cap free
Amount of items: 1
Items: 
Size: 645256 Color: 3

Bin 4465: 354748 of cap free
Amount of items: 1
Items: 
Size: 645253 Color: 3

Bin 4466: 354760 of cap free
Amount of items: 1
Items: 
Size: 645241 Color: 3

Bin 4467: 354822 of cap free
Amount of items: 1
Items: 
Size: 645179 Color: 4

Bin 4468: 354884 of cap free
Amount of items: 1
Items: 
Size: 645117 Color: 2

Bin 4469: 354907 of cap free
Amount of items: 1
Items: 
Size: 645094 Color: 4

Bin 4470: 354944 of cap free
Amount of items: 1
Items: 
Size: 645057 Color: 2

Bin 4471: 354966 of cap free
Amount of items: 1
Items: 
Size: 645035 Color: 1

Bin 4472: 355137 of cap free
Amount of items: 1
Items: 
Size: 644864 Color: 2

Bin 4473: 355320 of cap free
Amount of items: 1
Items: 
Size: 644681 Color: 4

Bin 4474: 355355 of cap free
Amount of items: 1
Items: 
Size: 644646 Color: 2

Bin 4475: 355355 of cap free
Amount of items: 1
Items: 
Size: 644646 Color: 3

Bin 4476: 355461 of cap free
Amount of items: 1
Items: 
Size: 644540 Color: 1

Bin 4477: 355596 of cap free
Amount of items: 1
Items: 
Size: 644405 Color: 3

Bin 4478: 355600 of cap free
Amount of items: 1
Items: 
Size: 644401 Color: 1

Bin 4479: 355641 of cap free
Amount of items: 1
Items: 
Size: 644360 Color: 4

Bin 4480: 355653 of cap free
Amount of items: 1
Items: 
Size: 644348 Color: 2

Bin 4481: 355654 of cap free
Amount of items: 1
Items: 
Size: 644347 Color: 0

Bin 4482: 355671 of cap free
Amount of items: 1
Items: 
Size: 644330 Color: 3

Bin 4483: 355767 of cap free
Amount of items: 1
Items: 
Size: 644234 Color: 2

Bin 4484: 355824 of cap free
Amount of items: 1
Items: 
Size: 644177 Color: 2

Bin 4485: 355921 of cap free
Amount of items: 1
Items: 
Size: 644080 Color: 3

Bin 4486: 356082 of cap free
Amount of items: 1
Items: 
Size: 643919 Color: 0

Bin 4487: 356237 of cap free
Amount of items: 1
Items: 
Size: 643764 Color: 0

Bin 4488: 356243 of cap free
Amount of items: 1
Items: 
Size: 643758 Color: 1

Bin 4489: 356358 of cap free
Amount of items: 1
Items: 
Size: 643643 Color: 4

Bin 4490: 356704 of cap free
Amount of items: 1
Items: 
Size: 643297 Color: 1

Bin 4491: 356711 of cap free
Amount of items: 1
Items: 
Size: 643290 Color: 4

Bin 4492: 356731 of cap free
Amount of items: 1
Items: 
Size: 643270 Color: 4

Bin 4493: 356758 of cap free
Amount of items: 1
Items: 
Size: 643243 Color: 3

Bin 4494: 356780 of cap free
Amount of items: 1
Items: 
Size: 643221 Color: 2

Bin 4495: 356867 of cap free
Amount of items: 1
Items: 
Size: 643134 Color: 0

Bin 4496: 356928 of cap free
Amount of items: 1
Items: 
Size: 643073 Color: 1

Bin 4497: 356935 of cap free
Amount of items: 1
Items: 
Size: 643066 Color: 0

Bin 4498: 356936 of cap free
Amount of items: 1
Items: 
Size: 643065 Color: 2

Bin 4499: 357003 of cap free
Amount of items: 1
Items: 
Size: 642998 Color: 0

Bin 4500: 357328 of cap free
Amount of items: 1
Items: 
Size: 642673 Color: 4

Bin 4501: 357342 of cap free
Amount of items: 1
Items: 
Size: 642659 Color: 3

Bin 4502: 357346 of cap free
Amount of items: 1
Items: 
Size: 642655 Color: 4

Bin 4503: 357352 of cap free
Amount of items: 1
Items: 
Size: 642649 Color: 4

Bin 4504: 357357 of cap free
Amount of items: 1
Items: 
Size: 642644 Color: 1

Bin 4505: 357382 of cap free
Amount of items: 1
Items: 
Size: 642619 Color: 0

Bin 4506: 357406 of cap free
Amount of items: 1
Items: 
Size: 642595 Color: 4

Bin 4507: 357418 of cap free
Amount of items: 1
Items: 
Size: 642583 Color: 3

Bin 4508: 357498 of cap free
Amount of items: 1
Items: 
Size: 642503 Color: 2

Bin 4509: 357516 of cap free
Amount of items: 1
Items: 
Size: 642485 Color: 3

Bin 4510: 357693 of cap free
Amount of items: 1
Items: 
Size: 642308 Color: 2

Bin 4511: 357732 of cap free
Amount of items: 1
Items: 
Size: 642269 Color: 0

Bin 4512: 357746 of cap free
Amount of items: 1
Items: 
Size: 642255 Color: 1

Bin 4513: 357905 of cap free
Amount of items: 1
Items: 
Size: 642096 Color: 2

Bin 4514: 357943 of cap free
Amount of items: 1
Items: 
Size: 642058 Color: 2

Bin 4515: 357961 of cap free
Amount of items: 1
Items: 
Size: 642040 Color: 3

Bin 4516: 357996 of cap free
Amount of items: 1
Items: 
Size: 642005 Color: 0

Bin 4517: 358075 of cap free
Amount of items: 1
Items: 
Size: 641926 Color: 4

Bin 4518: 358133 of cap free
Amount of items: 1
Items: 
Size: 641868 Color: 1

Bin 4519: 358153 of cap free
Amount of items: 1
Items: 
Size: 641848 Color: 4

Bin 4520: 358219 of cap free
Amount of items: 1
Items: 
Size: 641782 Color: 3

Bin 4521: 358238 of cap free
Amount of items: 1
Items: 
Size: 641763 Color: 0

Bin 4522: 358356 of cap free
Amount of items: 1
Items: 
Size: 641645 Color: 1

Bin 4523: 358411 of cap free
Amount of items: 1
Items: 
Size: 641590 Color: 2

Bin 4524: 358425 of cap free
Amount of items: 1
Items: 
Size: 641576 Color: 0

Bin 4525: 358456 of cap free
Amount of items: 1
Items: 
Size: 641545 Color: 0

Bin 4526: 358475 of cap free
Amount of items: 1
Items: 
Size: 641526 Color: 1

Bin 4527: 358706 of cap free
Amount of items: 1
Items: 
Size: 641295 Color: 2

Bin 4528: 358767 of cap free
Amount of items: 1
Items: 
Size: 641234 Color: 2

Bin 4529: 358819 of cap free
Amount of items: 1
Items: 
Size: 641182 Color: 2

Bin 4530: 358844 of cap free
Amount of items: 1
Items: 
Size: 641157 Color: 3

Bin 4531: 358860 of cap free
Amount of items: 1
Items: 
Size: 641141 Color: 1

Bin 4532: 358883 of cap free
Amount of items: 1
Items: 
Size: 641118 Color: 0

Bin 4533: 358960 of cap free
Amount of items: 1
Items: 
Size: 641041 Color: 4

Bin 4534: 359027 of cap free
Amount of items: 1
Items: 
Size: 640974 Color: 3

Bin 4535: 359322 of cap free
Amount of items: 1
Items: 
Size: 640679 Color: 2

Bin 4536: 359362 of cap free
Amount of items: 1
Items: 
Size: 640639 Color: 0

Bin 4537: 359368 of cap free
Amount of items: 1
Items: 
Size: 640633 Color: 3

Bin 4538: 359559 of cap free
Amount of items: 1
Items: 
Size: 640442 Color: 4

Bin 4539: 359596 of cap free
Amount of items: 1
Items: 
Size: 640405 Color: 1

Bin 4540: 359618 of cap free
Amount of items: 1
Items: 
Size: 640383 Color: 3

Bin 4541: 359622 of cap free
Amount of items: 1
Items: 
Size: 640379 Color: 1

Bin 4542: 359724 of cap free
Amount of items: 1
Items: 
Size: 640277 Color: 1

Bin 4543: 359724 of cap free
Amount of items: 1
Items: 
Size: 640277 Color: 1

Bin 4544: 359783 of cap free
Amount of items: 1
Items: 
Size: 640218 Color: 4

Bin 4545: 359884 of cap free
Amount of items: 1
Items: 
Size: 640117 Color: 2

Bin 4546: 360002 of cap free
Amount of items: 1
Items: 
Size: 639999 Color: 1

Bin 4547: 360038 of cap free
Amount of items: 1
Items: 
Size: 639963 Color: 1

Bin 4548: 360161 of cap free
Amount of items: 1
Items: 
Size: 639840 Color: 3

Bin 4549: 360221 of cap free
Amount of items: 1
Items: 
Size: 639780 Color: 0

Bin 4550: 360262 of cap free
Amount of items: 1
Items: 
Size: 639739 Color: 0

Bin 4551: 360274 of cap free
Amount of items: 1
Items: 
Size: 639727 Color: 4

Bin 4552: 360277 of cap free
Amount of items: 1
Items: 
Size: 639724 Color: 2

Bin 4553: 360317 of cap free
Amount of items: 1
Items: 
Size: 639684 Color: 1

Bin 4554: 360375 of cap free
Amount of items: 1
Items: 
Size: 639626 Color: 3

Bin 4555: 360421 of cap free
Amount of items: 1
Items: 
Size: 639580 Color: 2

Bin 4556: 360499 of cap free
Amount of items: 1
Items: 
Size: 639502 Color: 0

Bin 4557: 360507 of cap free
Amount of items: 1
Items: 
Size: 639494 Color: 4

Bin 4558: 360655 of cap free
Amount of items: 1
Items: 
Size: 639346 Color: 0

Bin 4559: 360716 of cap free
Amount of items: 1
Items: 
Size: 639285 Color: 0

Bin 4560: 360774 of cap free
Amount of items: 1
Items: 
Size: 639227 Color: 4

Bin 4561: 360807 of cap free
Amount of items: 1
Items: 
Size: 639194 Color: 2

Bin 4562: 360838 of cap free
Amount of items: 1
Items: 
Size: 639163 Color: 1

Bin 4563: 360958 of cap free
Amount of items: 1
Items: 
Size: 639043 Color: 4

Bin 4564: 360986 of cap free
Amount of items: 1
Items: 
Size: 639015 Color: 0

Bin 4565: 361110 of cap free
Amount of items: 1
Items: 
Size: 638891 Color: 1

Bin 4566: 361192 of cap free
Amount of items: 1
Items: 
Size: 638809 Color: 2

Bin 4567: 361239 of cap free
Amount of items: 1
Items: 
Size: 638762 Color: 2

Bin 4568: 361261 of cap free
Amount of items: 1
Items: 
Size: 638740 Color: 4

Bin 4569: 361295 of cap free
Amount of items: 1
Items: 
Size: 638706 Color: 1

Bin 4570: 361355 of cap free
Amount of items: 1
Items: 
Size: 638646 Color: 1

Bin 4571: 361361 of cap free
Amount of items: 1
Items: 
Size: 638640 Color: 2

Bin 4572: 361380 of cap free
Amount of items: 1
Items: 
Size: 638621 Color: 4

Bin 4573: 361417 of cap free
Amount of items: 1
Items: 
Size: 638584 Color: 0

Bin 4574: 361419 of cap free
Amount of items: 1
Items: 
Size: 638582 Color: 1

Bin 4575: 361471 of cap free
Amount of items: 1
Items: 
Size: 638530 Color: 1

Bin 4576: 361480 of cap free
Amount of items: 1
Items: 
Size: 638521 Color: 1

Bin 4577: 361504 of cap free
Amount of items: 1
Items: 
Size: 638497 Color: 1

Bin 4578: 361555 of cap free
Amount of items: 1
Items: 
Size: 638446 Color: 4

Bin 4579: 361635 of cap free
Amount of items: 1
Items: 
Size: 638366 Color: 0

Bin 4580: 361735 of cap free
Amount of items: 1
Items: 
Size: 638266 Color: 3

Bin 4581: 361854 of cap free
Amount of items: 1
Items: 
Size: 638147 Color: 2

Bin 4582: 361979 of cap free
Amount of items: 1
Items: 
Size: 638022 Color: 3

Bin 4583: 362029 of cap free
Amount of items: 1
Items: 
Size: 637972 Color: 0

Bin 4584: 362061 of cap free
Amount of items: 1
Items: 
Size: 637940 Color: 4

Bin 4585: 362076 of cap free
Amount of items: 1
Items: 
Size: 637925 Color: 3

Bin 4586: 362138 of cap free
Amount of items: 1
Items: 
Size: 637863 Color: 1

Bin 4587: 362140 of cap free
Amount of items: 1
Items: 
Size: 637861 Color: 3

Bin 4588: 362202 of cap free
Amount of items: 1
Items: 
Size: 637799 Color: 0

Bin 4589: 362214 of cap free
Amount of items: 1
Items: 
Size: 637787 Color: 3

Bin 4590: 362483 of cap free
Amount of items: 1
Items: 
Size: 637518 Color: 2

Bin 4591: 362648 of cap free
Amount of items: 1
Items: 
Size: 637353 Color: 4

Bin 4592: 362818 of cap free
Amount of items: 1
Items: 
Size: 637183 Color: 4

Bin 4593: 362992 of cap free
Amount of items: 1
Items: 
Size: 637009 Color: 4

Bin 4594: 363017 of cap free
Amount of items: 1
Items: 
Size: 636984 Color: 4

Bin 4595: 363031 of cap free
Amount of items: 1
Items: 
Size: 636970 Color: 3

Bin 4596: 363112 of cap free
Amount of items: 1
Items: 
Size: 636889 Color: 4

Bin 4597: 363170 of cap free
Amount of items: 1
Items: 
Size: 636831 Color: 1

Bin 4598: 363334 of cap free
Amount of items: 1
Items: 
Size: 636667 Color: 2

Bin 4599: 363429 of cap free
Amount of items: 1
Items: 
Size: 636572 Color: 4

Bin 4600: 363436 of cap free
Amount of items: 1
Items: 
Size: 636565 Color: 1

Bin 4601: 363452 of cap free
Amount of items: 1
Items: 
Size: 636549 Color: 1

Bin 4602: 363620 of cap free
Amount of items: 1
Items: 
Size: 636381 Color: 2

Bin 4603: 363661 of cap free
Amount of items: 1
Items: 
Size: 636340 Color: 1

Bin 4604: 363665 of cap free
Amount of items: 1
Items: 
Size: 636336 Color: 1

Bin 4605: 363669 of cap free
Amount of items: 1
Items: 
Size: 636332 Color: 1

Bin 4606: 363686 of cap free
Amount of items: 1
Items: 
Size: 636315 Color: 1

Bin 4607: 363716 of cap free
Amount of items: 1
Items: 
Size: 636285 Color: 4

Bin 4608: 363723 of cap free
Amount of items: 1
Items: 
Size: 636278 Color: 0

Bin 4609: 363785 of cap free
Amount of items: 1
Items: 
Size: 636216 Color: 2

Bin 4610: 363857 of cap free
Amount of items: 1
Items: 
Size: 636144 Color: 2

Bin 4611: 363920 of cap free
Amount of items: 1
Items: 
Size: 636081 Color: 3

Bin 4612: 363924 of cap free
Amount of items: 1
Items: 
Size: 636077 Color: 4

Bin 4613: 363929 of cap free
Amount of items: 1
Items: 
Size: 636072 Color: 4

Bin 4614: 363968 of cap free
Amount of items: 1
Items: 
Size: 636033 Color: 0

Bin 4615: 364065 of cap free
Amount of items: 1
Items: 
Size: 635936 Color: 4

Bin 4616: 364066 of cap free
Amount of items: 1
Items: 
Size: 635935 Color: 0

Bin 4617: 364096 of cap free
Amount of items: 1
Items: 
Size: 635905 Color: 2

Bin 4618: 364152 of cap free
Amount of items: 1
Items: 
Size: 635849 Color: 2

Bin 4619: 364212 of cap free
Amount of items: 1
Items: 
Size: 635789 Color: 2

Bin 4620: 364225 of cap free
Amount of items: 1
Items: 
Size: 635776 Color: 0

Bin 4621: 364407 of cap free
Amount of items: 1
Items: 
Size: 635594 Color: 4

Bin 4622: 364438 of cap free
Amount of items: 1
Items: 
Size: 635563 Color: 1

Bin 4623: 364649 of cap free
Amount of items: 1
Items: 
Size: 635352 Color: 4

Bin 4624: 364730 of cap free
Amount of items: 1
Items: 
Size: 635271 Color: 0

Bin 4625: 364804 of cap free
Amount of items: 1
Items: 
Size: 635197 Color: 1

Bin 4626: 364843 of cap free
Amount of items: 1
Items: 
Size: 635158 Color: 2

Bin 4627: 364845 of cap free
Amount of items: 1
Items: 
Size: 635156 Color: 4

Bin 4628: 364910 of cap free
Amount of items: 1
Items: 
Size: 635091 Color: 0

Bin 4629: 365052 of cap free
Amount of items: 1
Items: 
Size: 634949 Color: 2

Bin 4630: 365140 of cap free
Amount of items: 1
Items: 
Size: 634861 Color: 2

Bin 4631: 365147 of cap free
Amount of items: 1
Items: 
Size: 634854 Color: 4

Bin 4632: 365201 of cap free
Amount of items: 1
Items: 
Size: 634800 Color: 0

Bin 4633: 365212 of cap free
Amount of items: 1
Items: 
Size: 634789 Color: 0

Bin 4634: 365238 of cap free
Amount of items: 1
Items: 
Size: 634763 Color: 0

Bin 4635: 365241 of cap free
Amount of items: 1
Items: 
Size: 634760 Color: 4

Bin 4636: 365263 of cap free
Amount of items: 1
Items: 
Size: 634738 Color: 0

Bin 4637: 365303 of cap free
Amount of items: 1
Items: 
Size: 634698 Color: 0

Bin 4638: 365322 of cap free
Amount of items: 1
Items: 
Size: 634679 Color: 1

Bin 4639: 365356 of cap free
Amount of items: 1
Items: 
Size: 634645 Color: 1

Bin 4640: 365474 of cap free
Amount of items: 1
Items: 
Size: 634527 Color: 2

Bin 4641: 365582 of cap free
Amount of items: 1
Items: 
Size: 634419 Color: 1

Bin 4642: 365651 of cap free
Amount of items: 1
Items: 
Size: 634350 Color: 2

Bin 4643: 365746 of cap free
Amount of items: 1
Items: 
Size: 634255 Color: 3

Bin 4644: 365789 of cap free
Amount of items: 1
Items: 
Size: 634212 Color: 3

Bin 4645: 365815 of cap free
Amount of items: 1
Items: 
Size: 634186 Color: 1

Bin 4646: 365966 of cap free
Amount of items: 1
Items: 
Size: 634035 Color: 1

Bin 4647: 366042 of cap free
Amount of items: 1
Items: 
Size: 633959 Color: 3

Bin 4648: 366103 of cap free
Amount of items: 1
Items: 
Size: 633898 Color: 0

Bin 4649: 366166 of cap free
Amount of items: 1
Items: 
Size: 633835 Color: 0

Bin 4650: 366217 of cap free
Amount of items: 1
Items: 
Size: 633784 Color: 2

Bin 4651: 366282 of cap free
Amount of items: 1
Items: 
Size: 633719 Color: 0

Bin 4652: 366298 of cap free
Amount of items: 1
Items: 
Size: 633703 Color: 0

Bin 4653: 366373 of cap free
Amount of items: 1
Items: 
Size: 633628 Color: 1

Bin 4654: 366439 of cap free
Amount of items: 1
Items: 
Size: 633562 Color: 0

Bin 4655: 366485 of cap free
Amount of items: 1
Items: 
Size: 633516 Color: 1

Bin 4656: 366519 of cap free
Amount of items: 1
Items: 
Size: 633482 Color: 3

Bin 4657: 366529 of cap free
Amount of items: 1
Items: 
Size: 633472 Color: 0

Bin 4658: 366561 of cap free
Amount of items: 1
Items: 
Size: 633440 Color: 1

Bin 4659: 366640 of cap free
Amount of items: 1
Items: 
Size: 633361 Color: 1

Bin 4660: 366766 of cap free
Amount of items: 1
Items: 
Size: 633235 Color: 2

Bin 4661: 366934 of cap free
Amount of items: 1
Items: 
Size: 633067 Color: 2

Bin 4662: 366962 of cap free
Amount of items: 1
Items: 
Size: 633039 Color: 2

Bin 4663: 366971 of cap free
Amount of items: 1
Items: 
Size: 633030 Color: 0

Bin 4664: 367080 of cap free
Amount of items: 1
Items: 
Size: 632921 Color: 1

Bin 4665: 367177 of cap free
Amount of items: 1
Items: 
Size: 632824 Color: 3

Bin 4666: 367203 of cap free
Amount of items: 1
Items: 
Size: 632798 Color: 1

Bin 4667: 367303 of cap free
Amount of items: 1
Items: 
Size: 632698 Color: 0

Bin 4668: 367446 of cap free
Amount of items: 1
Items: 
Size: 632555 Color: 4

Bin 4669: 367483 of cap free
Amount of items: 1
Items: 
Size: 632518 Color: 0

Bin 4670: 367511 of cap free
Amount of items: 1
Items: 
Size: 632490 Color: 4

Bin 4671: 367548 of cap free
Amount of items: 1
Items: 
Size: 632453 Color: 0

Bin 4672: 367642 of cap free
Amount of items: 1
Items: 
Size: 632359 Color: 0

Bin 4673: 367825 of cap free
Amount of items: 1
Items: 
Size: 632176 Color: 4

Bin 4674: 367865 of cap free
Amount of items: 1
Items: 
Size: 632136 Color: 3

Bin 4675: 367925 of cap free
Amount of items: 1
Items: 
Size: 632076 Color: 0

Bin 4676: 368154 of cap free
Amount of items: 1
Items: 
Size: 631847 Color: 4

Bin 4677: 368196 of cap free
Amount of items: 1
Items: 
Size: 631805 Color: 1

Bin 4678: 368291 of cap free
Amount of items: 1
Items: 
Size: 631710 Color: 0

Bin 4679: 368374 of cap free
Amount of items: 1
Items: 
Size: 631627 Color: 4

Bin 4680: 368479 of cap free
Amount of items: 1
Items: 
Size: 631522 Color: 3

Bin 4681: 368486 of cap free
Amount of items: 1
Items: 
Size: 631515 Color: 2

Bin 4682: 368507 of cap free
Amount of items: 1
Items: 
Size: 631494 Color: 4

Bin 4683: 368542 of cap free
Amount of items: 1
Items: 
Size: 631459 Color: 2

Bin 4684: 368741 of cap free
Amount of items: 1
Items: 
Size: 631260 Color: 4

Bin 4685: 368847 of cap free
Amount of items: 1
Items: 
Size: 631154 Color: 4

Bin 4686: 368860 of cap free
Amount of items: 1
Items: 
Size: 631141 Color: 0

Bin 4687: 368952 of cap free
Amount of items: 1
Items: 
Size: 631049 Color: 3

Bin 4688: 369144 of cap free
Amount of items: 1
Items: 
Size: 630857 Color: 0

Bin 4689: 369294 of cap free
Amount of items: 1
Items: 
Size: 630707 Color: 4

Bin 4690: 369325 of cap free
Amount of items: 1
Items: 
Size: 630676 Color: 4

Bin 4691: 369338 of cap free
Amount of items: 1
Items: 
Size: 630663 Color: 3

Bin 4692: 369340 of cap free
Amount of items: 1
Items: 
Size: 630661 Color: 0

Bin 4693: 369351 of cap free
Amount of items: 1
Items: 
Size: 630650 Color: 0

Bin 4694: 369367 of cap free
Amount of items: 1
Items: 
Size: 630634 Color: 1

Bin 4695: 369504 of cap free
Amount of items: 1
Items: 
Size: 630497 Color: 1

Bin 4696: 369654 of cap free
Amount of items: 1
Items: 
Size: 630347 Color: 2

Bin 4697: 369698 of cap free
Amount of items: 1
Items: 
Size: 630303 Color: 4

Bin 4698: 369707 of cap free
Amount of items: 1
Items: 
Size: 630294 Color: 4

Bin 4699: 369734 of cap free
Amount of items: 1
Items: 
Size: 630267 Color: 3

Bin 4700: 369784 of cap free
Amount of items: 1
Items: 
Size: 630217 Color: 1

Bin 4701: 369867 of cap free
Amount of items: 1
Items: 
Size: 630134 Color: 0

Bin 4702: 369964 of cap free
Amount of items: 1
Items: 
Size: 630037 Color: 0

Bin 4703: 370100 of cap free
Amount of items: 1
Items: 
Size: 629901 Color: 2

Bin 4704: 370191 of cap free
Amount of items: 1
Items: 
Size: 629810 Color: 3

Bin 4705: 370277 of cap free
Amount of items: 1
Items: 
Size: 629724 Color: 2

Bin 4706: 370567 of cap free
Amount of items: 1
Items: 
Size: 629434 Color: 4

Bin 4707: 370600 of cap free
Amount of items: 1
Items: 
Size: 629401 Color: 0

Bin 4708: 370618 of cap free
Amount of items: 1
Items: 
Size: 629383 Color: 4

Bin 4709: 370725 of cap free
Amount of items: 1
Items: 
Size: 629276 Color: 0

Bin 4710: 370762 of cap free
Amount of items: 1
Items: 
Size: 629239 Color: 1

Bin 4711: 370823 of cap free
Amount of items: 1
Items: 
Size: 629178 Color: 3

Bin 4712: 370847 of cap free
Amount of items: 1
Items: 
Size: 629154 Color: 4

Bin 4713: 370864 of cap free
Amount of items: 1
Items: 
Size: 629137 Color: 0

Bin 4714: 370955 of cap free
Amount of items: 1
Items: 
Size: 629046 Color: 1

Bin 4715: 370962 of cap free
Amount of items: 1
Items: 
Size: 629039 Color: 3

Bin 4716: 371198 of cap free
Amount of items: 1
Items: 
Size: 628803 Color: 0

Bin 4717: 371225 of cap free
Amount of items: 1
Items: 
Size: 628776 Color: 4

Bin 4718: 371233 of cap free
Amount of items: 1
Items: 
Size: 628768 Color: 2

Bin 4719: 371271 of cap free
Amount of items: 1
Items: 
Size: 628730 Color: 3

Bin 4720: 371278 of cap free
Amount of items: 1
Items: 
Size: 628723 Color: 2

Bin 4721: 371317 of cap free
Amount of items: 1
Items: 
Size: 628684 Color: 3

Bin 4722: 371338 of cap free
Amount of items: 1
Items: 
Size: 628663 Color: 3

Bin 4723: 371419 of cap free
Amount of items: 1
Items: 
Size: 628582 Color: 1

Bin 4724: 371457 of cap free
Amount of items: 1
Items: 
Size: 628544 Color: 2

Bin 4725: 371546 of cap free
Amount of items: 1
Items: 
Size: 628455 Color: 1

Bin 4726: 371589 of cap free
Amount of items: 1
Items: 
Size: 628412 Color: 2

Bin 4727: 371595 of cap free
Amount of items: 1
Items: 
Size: 628406 Color: 0

Bin 4728: 372044 of cap free
Amount of items: 1
Items: 
Size: 627957 Color: 0

Bin 4729: 372071 of cap free
Amount of items: 1
Items: 
Size: 627930 Color: 0

Bin 4730: 372074 of cap free
Amount of items: 1
Items: 
Size: 627927 Color: 3

Bin 4731: 372090 of cap free
Amount of items: 1
Items: 
Size: 627911 Color: 1

Bin 4732: 372115 of cap free
Amount of items: 1
Items: 
Size: 627886 Color: 3

Bin 4733: 372127 of cap free
Amount of items: 1
Items: 
Size: 627874 Color: 4

Bin 4734: 372179 of cap free
Amount of items: 1
Items: 
Size: 627822 Color: 1

Bin 4735: 372254 of cap free
Amount of items: 1
Items: 
Size: 627747 Color: 3

Bin 4736: 372266 of cap free
Amount of items: 1
Items: 
Size: 627735 Color: 2

Bin 4737: 372339 of cap free
Amount of items: 1
Items: 
Size: 627662 Color: 3

Bin 4738: 372384 of cap free
Amount of items: 1
Items: 
Size: 627617 Color: 2

Bin 4739: 372500 of cap free
Amount of items: 1
Items: 
Size: 627501 Color: 1

Bin 4740: 372555 of cap free
Amount of items: 1
Items: 
Size: 627446 Color: 0

Bin 4741: 372592 of cap free
Amount of items: 1
Items: 
Size: 627409 Color: 0

Bin 4742: 372593 of cap free
Amount of items: 1
Items: 
Size: 627408 Color: 1

Bin 4743: 372668 of cap free
Amount of items: 1
Items: 
Size: 627333 Color: 3

Bin 4744: 372887 of cap free
Amount of items: 1
Items: 
Size: 627114 Color: 3

Bin 4745: 372966 of cap free
Amount of items: 1
Items: 
Size: 627035 Color: 2

Bin 4746: 372986 of cap free
Amount of items: 1
Items: 
Size: 627015 Color: 2

Bin 4747: 373027 of cap free
Amount of items: 1
Items: 
Size: 626974 Color: 3

Bin 4748: 373036 of cap free
Amount of items: 1
Items: 
Size: 626965 Color: 2

Bin 4749: 373088 of cap free
Amount of items: 1
Items: 
Size: 626913 Color: 0

Bin 4750: 373105 of cap free
Amount of items: 1
Items: 
Size: 626896 Color: 0

Bin 4751: 373566 of cap free
Amount of items: 1
Items: 
Size: 626435 Color: 0

Bin 4752: 373760 of cap free
Amount of items: 1
Items: 
Size: 626241 Color: 1

Bin 4753: 373810 of cap free
Amount of items: 1
Items: 
Size: 626191 Color: 1

Bin 4754: 373851 of cap free
Amount of items: 1
Items: 
Size: 626150 Color: 3

Bin 4755: 373882 of cap free
Amount of items: 1
Items: 
Size: 626119 Color: 1

Bin 4756: 373949 of cap free
Amount of items: 1
Items: 
Size: 626052 Color: 3

Bin 4757: 373968 of cap free
Amount of items: 1
Items: 
Size: 626033 Color: 2

Bin 4758: 373973 of cap free
Amount of items: 1
Items: 
Size: 626028 Color: 3

Bin 4759: 374087 of cap free
Amount of items: 1
Items: 
Size: 625914 Color: 2

Bin 4760: 374211 of cap free
Amount of items: 1
Items: 
Size: 625790 Color: 4

Bin 4761: 374246 of cap free
Amount of items: 1
Items: 
Size: 625755 Color: 4

Bin 4762: 374269 of cap free
Amount of items: 1
Items: 
Size: 625732 Color: 1

Bin 4763: 374358 of cap free
Amount of items: 1
Items: 
Size: 625643 Color: 1

Bin 4764: 374382 of cap free
Amount of items: 1
Items: 
Size: 625619 Color: 0

Bin 4765: 374453 of cap free
Amount of items: 1
Items: 
Size: 625548 Color: 3

Bin 4766: 374555 of cap free
Amount of items: 1
Items: 
Size: 625446 Color: 1

Bin 4767: 374584 of cap free
Amount of items: 1
Items: 
Size: 625417 Color: 3

Bin 4768: 374614 of cap free
Amount of items: 1
Items: 
Size: 625387 Color: 3

Bin 4769: 374730 of cap free
Amount of items: 1
Items: 
Size: 625271 Color: 0

Bin 4770: 374755 of cap free
Amount of items: 1
Items: 
Size: 625246 Color: 2

Bin 4771: 374872 of cap free
Amount of items: 1
Items: 
Size: 625129 Color: 3

Bin 4772: 374955 of cap free
Amount of items: 1
Items: 
Size: 625046 Color: 0

Bin 4773: 375005 of cap free
Amount of items: 1
Items: 
Size: 624996 Color: 3

Bin 4774: 375013 of cap free
Amount of items: 1
Items: 
Size: 624988 Color: 4

Bin 4775: 375049 of cap free
Amount of items: 1
Items: 
Size: 624952 Color: 1

Bin 4776: 375220 of cap free
Amount of items: 1
Items: 
Size: 624781 Color: 0

Bin 4777: 375234 of cap free
Amount of items: 1
Items: 
Size: 624767 Color: 3

Bin 4778: 375294 of cap free
Amount of items: 1
Items: 
Size: 624707 Color: 2

Bin 4779: 375440 of cap free
Amount of items: 1
Items: 
Size: 624561 Color: 0

Bin 4780: 375454 of cap free
Amount of items: 1
Items: 
Size: 624547 Color: 3

Bin 4781: 375514 of cap free
Amount of items: 1
Items: 
Size: 624487 Color: 0

Bin 4782: 375515 of cap free
Amount of items: 1
Items: 
Size: 624486 Color: 2

Bin 4783: 375564 of cap free
Amount of items: 1
Items: 
Size: 624437 Color: 3

Bin 4784: 375647 of cap free
Amount of items: 1
Items: 
Size: 624354 Color: 0

Bin 4785: 375737 of cap free
Amount of items: 1
Items: 
Size: 624264 Color: 3

Bin 4786: 376040 of cap free
Amount of items: 1
Items: 
Size: 623961 Color: 0

Bin 4787: 376160 of cap free
Amount of items: 1
Items: 
Size: 623841 Color: 0

Bin 4788: 376216 of cap free
Amount of items: 1
Items: 
Size: 623785 Color: 0

Bin 4789: 376265 of cap free
Amount of items: 1
Items: 
Size: 623736 Color: 1

Bin 4790: 376391 of cap free
Amount of items: 1
Items: 
Size: 623610 Color: 0

Bin 4791: 376414 of cap free
Amount of items: 1
Items: 
Size: 623587 Color: 1

Bin 4792: 376422 of cap free
Amount of items: 1
Items: 
Size: 623579 Color: 1

Bin 4793: 376503 of cap free
Amount of items: 1
Items: 
Size: 623498 Color: 3

Bin 4794: 376677 of cap free
Amount of items: 1
Items: 
Size: 623324 Color: 0

Bin 4795: 376814 of cap free
Amount of items: 1
Items: 
Size: 623187 Color: 1

Bin 4796: 376885 of cap free
Amount of items: 1
Items: 
Size: 623116 Color: 2

Bin 4797: 376991 of cap free
Amount of items: 1
Items: 
Size: 623010 Color: 1

Bin 4798: 377104 of cap free
Amount of items: 1
Items: 
Size: 622897 Color: 4

Bin 4799: 377137 of cap free
Amount of items: 1
Items: 
Size: 622864 Color: 3

Bin 4800: 377217 of cap free
Amount of items: 1
Items: 
Size: 622784 Color: 2

Bin 4801: 377249 of cap free
Amount of items: 1
Items: 
Size: 622752 Color: 3

Bin 4802: 377249 of cap free
Amount of items: 1
Items: 
Size: 622752 Color: 4

Bin 4803: 377296 of cap free
Amount of items: 1
Items: 
Size: 622705 Color: 0

Bin 4804: 377344 of cap free
Amount of items: 1
Items: 
Size: 622657 Color: 4

Bin 4805: 377364 of cap free
Amount of items: 1
Items: 
Size: 622637 Color: 2

Bin 4806: 377455 of cap free
Amount of items: 1
Items: 
Size: 622546 Color: 0

Bin 4807: 377463 of cap free
Amount of items: 1
Items: 
Size: 622538 Color: 1

Bin 4808: 377521 of cap free
Amount of items: 1
Items: 
Size: 622480 Color: 1

Bin 4809: 377717 of cap free
Amount of items: 1
Items: 
Size: 622284 Color: 0

Bin 4810: 377724 of cap free
Amount of items: 1
Items: 
Size: 622277 Color: 2

Bin 4811: 377773 of cap free
Amount of items: 1
Items: 
Size: 622228 Color: 2

Bin 4812: 377883 of cap free
Amount of items: 1
Items: 
Size: 622118 Color: 3

Bin 4813: 377910 of cap free
Amount of items: 1
Items: 
Size: 622091 Color: 1

Bin 4814: 377925 of cap free
Amount of items: 1
Items: 
Size: 622076 Color: 0

Bin 4815: 377951 of cap free
Amount of items: 1
Items: 
Size: 622050 Color: 3

Bin 4816: 378020 of cap free
Amount of items: 1
Items: 
Size: 621981 Color: 1

Bin 4817: 378070 of cap free
Amount of items: 1
Items: 
Size: 621931 Color: 0

Bin 4818: 378109 of cap free
Amount of items: 1
Items: 
Size: 621892 Color: 3

Bin 4819: 378117 of cap free
Amount of items: 1
Items: 
Size: 621884 Color: 2

Bin 4820: 378172 of cap free
Amount of items: 1
Items: 
Size: 621829 Color: 4

Bin 4821: 378178 of cap free
Amount of items: 1
Items: 
Size: 621823 Color: 4

Bin 4822: 378183 of cap free
Amount of items: 1
Items: 
Size: 621818 Color: 3

Bin 4823: 378336 of cap free
Amount of items: 1
Items: 
Size: 621665 Color: 4

Bin 4824: 378387 of cap free
Amount of items: 1
Items: 
Size: 621614 Color: 3

Bin 4825: 378458 of cap free
Amount of items: 1
Items: 
Size: 621543 Color: 3

Bin 4826: 378508 of cap free
Amount of items: 1
Items: 
Size: 621493 Color: 4

Bin 4827: 378553 of cap free
Amount of items: 1
Items: 
Size: 621448 Color: 4

Bin 4828: 378564 of cap free
Amount of items: 1
Items: 
Size: 621437 Color: 1

Bin 4829: 378564 of cap free
Amount of items: 1
Items: 
Size: 621437 Color: 2

Bin 4830: 378583 of cap free
Amount of items: 1
Items: 
Size: 621418 Color: 2

Bin 4831: 378619 of cap free
Amount of items: 1
Items: 
Size: 621382 Color: 3

Bin 4832: 378637 of cap free
Amount of items: 1
Items: 
Size: 621364 Color: 4

Bin 4833: 378656 of cap free
Amount of items: 1
Items: 
Size: 621345 Color: 4

Bin 4834: 378699 of cap free
Amount of items: 1
Items: 
Size: 621302 Color: 3

Bin 4835: 378798 of cap free
Amount of items: 1
Items: 
Size: 621203 Color: 0

Bin 4836: 378918 of cap free
Amount of items: 1
Items: 
Size: 621083 Color: 3

Bin 4837: 378964 of cap free
Amount of items: 1
Items: 
Size: 621037 Color: 2

Bin 4838: 379078 of cap free
Amount of items: 1
Items: 
Size: 620923 Color: 0

Bin 4839: 379206 of cap free
Amount of items: 1
Items: 
Size: 620795 Color: 2

Bin 4840: 379406 of cap free
Amount of items: 1
Items: 
Size: 620595 Color: 2

Bin 4841: 379628 of cap free
Amount of items: 1
Items: 
Size: 620373 Color: 0

Bin 4842: 379637 of cap free
Amount of items: 1
Items: 
Size: 620364 Color: 1

Bin 4843: 379680 of cap free
Amount of items: 1
Items: 
Size: 620321 Color: 2

Bin 4844: 379714 of cap free
Amount of items: 1
Items: 
Size: 620287 Color: 2

Bin 4845: 379737 of cap free
Amount of items: 1
Items: 
Size: 620264 Color: 0

Bin 4846: 379797 of cap free
Amount of items: 1
Items: 
Size: 620204 Color: 1

Bin 4847: 379829 of cap free
Amount of items: 1
Items: 
Size: 620172 Color: 1

Bin 4848: 379836 of cap free
Amount of items: 1
Items: 
Size: 620165 Color: 0

Bin 4849: 379861 of cap free
Amount of items: 1
Items: 
Size: 620140 Color: 3

Bin 4850: 380042 of cap free
Amount of items: 1
Items: 
Size: 619959 Color: 1

Bin 4851: 380047 of cap free
Amount of items: 1
Items: 
Size: 619954 Color: 1

Bin 4852: 380057 of cap free
Amount of items: 1
Items: 
Size: 619944 Color: 1

Bin 4853: 380060 of cap free
Amount of items: 1
Items: 
Size: 619941 Color: 0

Bin 4854: 380118 of cap free
Amount of items: 1
Items: 
Size: 619883 Color: 4

Bin 4855: 380252 of cap free
Amount of items: 1
Items: 
Size: 619749 Color: 2

Bin 4856: 380280 of cap free
Amount of items: 1
Items: 
Size: 619721 Color: 2

Bin 4857: 380314 of cap free
Amount of items: 1
Items: 
Size: 619687 Color: 3

Bin 4858: 380441 of cap free
Amount of items: 1
Items: 
Size: 619560 Color: 2

Bin 4859: 380485 of cap free
Amount of items: 1
Items: 
Size: 619516 Color: 2

Bin 4860: 380559 of cap free
Amount of items: 1
Items: 
Size: 619442 Color: 2

Bin 4861: 380642 of cap free
Amount of items: 1
Items: 
Size: 619359 Color: 0

Bin 4862: 380773 of cap free
Amount of items: 1
Items: 
Size: 619228 Color: 1

Bin 4863: 380775 of cap free
Amount of items: 1
Items: 
Size: 619226 Color: 1

Bin 4864: 380792 of cap free
Amount of items: 1
Items: 
Size: 619209 Color: 0

Bin 4865: 380798 of cap free
Amount of items: 1
Items: 
Size: 619203 Color: 3

Bin 4866: 380832 of cap free
Amount of items: 1
Items: 
Size: 619169 Color: 1

Bin 4867: 380920 of cap free
Amount of items: 1
Items: 
Size: 619081 Color: 3

Bin 4868: 381042 of cap free
Amount of items: 1
Items: 
Size: 618959 Color: 0

Bin 4869: 381196 of cap free
Amount of items: 1
Items: 
Size: 618805 Color: 3

Bin 4870: 381316 of cap free
Amount of items: 1
Items: 
Size: 618685 Color: 1

Bin 4871: 381325 of cap free
Amount of items: 1
Items: 
Size: 618676 Color: 1

Bin 4872: 381495 of cap free
Amount of items: 1
Items: 
Size: 618506 Color: 4

Bin 4873: 381550 of cap free
Amount of items: 1
Items: 
Size: 618451 Color: 4

Bin 4874: 381607 of cap free
Amount of items: 1
Items: 
Size: 618394 Color: 4

Bin 4875: 381690 of cap free
Amount of items: 1
Items: 
Size: 618311 Color: 3

Bin 4876: 381765 of cap free
Amount of items: 1
Items: 
Size: 618236 Color: 3

Bin 4877: 381813 of cap free
Amount of items: 1
Items: 
Size: 618188 Color: 2

Bin 4878: 382078 of cap free
Amount of items: 1
Items: 
Size: 617923 Color: 3

Bin 4879: 382190 of cap free
Amount of items: 1
Items: 
Size: 617811 Color: 0

Bin 4880: 382194 of cap free
Amount of items: 1
Items: 
Size: 617807 Color: 0

Bin 4881: 382284 of cap free
Amount of items: 1
Items: 
Size: 617717 Color: 0

Bin 4882: 382362 of cap free
Amount of items: 1
Items: 
Size: 617639 Color: 4

Bin 4883: 382566 of cap free
Amount of items: 1
Items: 
Size: 617435 Color: 4

Bin 4884: 382591 of cap free
Amount of items: 1
Items: 
Size: 617410 Color: 0

Bin 4885: 382608 of cap free
Amount of items: 1
Items: 
Size: 617393 Color: 3

Bin 4886: 382663 of cap free
Amount of items: 1
Items: 
Size: 617338 Color: 1

Bin 4887: 382705 of cap free
Amount of items: 1
Items: 
Size: 617296 Color: 2

Bin 4888: 382738 of cap free
Amount of items: 1
Items: 
Size: 617263 Color: 4

Bin 4889: 382806 of cap free
Amount of items: 1
Items: 
Size: 617195 Color: 4

Bin 4890: 382879 of cap free
Amount of items: 1
Items: 
Size: 617122 Color: 0

Bin 4891: 382889 of cap free
Amount of items: 1
Items: 
Size: 617112 Color: 2

Bin 4892: 382933 of cap free
Amount of items: 1
Items: 
Size: 617068 Color: 3

Bin 4893: 383084 of cap free
Amount of items: 1
Items: 
Size: 616917 Color: 1

Bin 4894: 383160 of cap free
Amount of items: 1
Items: 
Size: 616841 Color: 3

Bin 4895: 383193 of cap free
Amount of items: 1
Items: 
Size: 616808 Color: 0

Bin 4896: 383373 of cap free
Amount of items: 1
Items: 
Size: 616628 Color: 2

Bin 4897: 383423 of cap free
Amount of items: 1
Items: 
Size: 616578 Color: 2

Bin 4898: 383423 of cap free
Amount of items: 1
Items: 
Size: 616578 Color: 2

Bin 4899: 383461 of cap free
Amount of items: 1
Items: 
Size: 616540 Color: 4

Bin 4900: 383572 of cap free
Amount of items: 1
Items: 
Size: 616429 Color: 3

Bin 4901: 383638 of cap free
Amount of items: 1
Items: 
Size: 616363 Color: 0

Bin 4902: 383671 of cap free
Amount of items: 1
Items: 
Size: 616330 Color: 1

Bin 4903: 383741 of cap free
Amount of items: 1
Items: 
Size: 616260 Color: 3

Bin 4904: 383769 of cap free
Amount of items: 1
Items: 
Size: 616232 Color: 0

Bin 4905: 383956 of cap free
Amount of items: 1
Items: 
Size: 616045 Color: 4

Bin 4906: 383963 of cap free
Amount of items: 1
Items: 
Size: 616038 Color: 1

Bin 4907: 384064 of cap free
Amount of items: 1
Items: 
Size: 615937 Color: 3

Bin 4908: 384146 of cap free
Amount of items: 1
Items: 
Size: 615855 Color: 2

Bin 4909: 384161 of cap free
Amount of items: 1
Items: 
Size: 615840 Color: 3

Bin 4910: 384167 of cap free
Amount of items: 1
Items: 
Size: 615834 Color: 4

Bin 4911: 384194 of cap free
Amount of items: 1
Items: 
Size: 615807 Color: 1

Bin 4912: 384306 of cap free
Amount of items: 1
Items: 
Size: 615695 Color: 4

Bin 4913: 384512 of cap free
Amount of items: 1
Items: 
Size: 615489 Color: 3

Bin 4914: 384702 of cap free
Amount of items: 1
Items: 
Size: 615299 Color: 3

Bin 4915: 384823 of cap free
Amount of items: 1
Items: 
Size: 615178 Color: 3

Bin 4916: 384851 of cap free
Amount of items: 1
Items: 
Size: 615150 Color: 1

Bin 4917: 384860 of cap free
Amount of items: 1
Items: 
Size: 615141 Color: 3

Bin 4918: 384928 of cap free
Amount of items: 1
Items: 
Size: 615073 Color: 1

Bin 4919: 384943 of cap free
Amount of items: 1
Items: 
Size: 615058 Color: 2

Bin 4920: 385189 of cap free
Amount of items: 1
Items: 
Size: 614812 Color: 2

Bin 4921: 385206 of cap free
Amount of items: 1
Items: 
Size: 614795 Color: 0

Bin 4922: 385233 of cap free
Amount of items: 1
Items: 
Size: 614768 Color: 0

Bin 4923: 385267 of cap free
Amount of items: 1
Items: 
Size: 614734 Color: 1

Bin 4924: 385490 of cap free
Amount of items: 1
Items: 
Size: 614511 Color: 3

Bin 4925: 385670 of cap free
Amount of items: 1
Items: 
Size: 614331 Color: 2

Bin 4926: 385844 of cap free
Amount of items: 1
Items: 
Size: 614157 Color: 1

Bin 4927: 385846 of cap free
Amount of items: 1
Items: 
Size: 614155 Color: 4

Bin 4928: 385956 of cap free
Amount of items: 1
Items: 
Size: 614045 Color: 1

Bin 4929: 386024 of cap free
Amount of items: 1
Items: 
Size: 613977 Color: 0

Bin 4930: 386191 of cap free
Amount of items: 1
Items: 
Size: 613810 Color: 1

Bin 4931: 386238 of cap free
Amount of items: 1
Items: 
Size: 613763 Color: 0

Bin 4932: 386291 of cap free
Amount of items: 1
Items: 
Size: 613710 Color: 3

Bin 4933: 386400 of cap free
Amount of items: 1
Items: 
Size: 613601 Color: 2

Bin 4934: 386411 of cap free
Amount of items: 1
Items: 
Size: 613590 Color: 4

Bin 4935: 386431 of cap free
Amount of items: 1
Items: 
Size: 613570 Color: 4

Bin 4936: 386500 of cap free
Amount of items: 1
Items: 
Size: 613501 Color: 0

Bin 4937: 386598 of cap free
Amount of items: 1
Items: 
Size: 613403 Color: 1

Bin 4938: 386673 of cap free
Amount of items: 1
Items: 
Size: 613328 Color: 2

Bin 4939: 386699 of cap free
Amount of items: 1
Items: 
Size: 613302 Color: 0

Bin 4940: 386732 of cap free
Amount of items: 1
Items: 
Size: 613269 Color: 1

Bin 4941: 386787 of cap free
Amount of items: 1
Items: 
Size: 613214 Color: 4

Bin 4942: 386963 of cap free
Amount of items: 1
Items: 
Size: 613038 Color: 1

Bin 4943: 387008 of cap free
Amount of items: 1
Items: 
Size: 612993 Color: 2

Bin 4944: 387078 of cap free
Amount of items: 1
Items: 
Size: 612923 Color: 2

Bin 4945: 387096 of cap free
Amount of items: 1
Items: 
Size: 612905 Color: 4

Bin 4946: 387201 of cap free
Amount of items: 1
Items: 
Size: 612800 Color: 2

Bin 4947: 387237 of cap free
Amount of items: 1
Items: 
Size: 612764 Color: 3

Bin 4948: 387391 of cap free
Amount of items: 1
Items: 
Size: 612610 Color: 4

Bin 4949: 387423 of cap free
Amount of items: 1
Items: 
Size: 612578 Color: 3

Bin 4950: 387433 of cap free
Amount of items: 1
Items: 
Size: 612568 Color: 3

Bin 4951: 387671 of cap free
Amount of items: 1
Items: 
Size: 612330 Color: 3

Bin 4952: 387686 of cap free
Amount of items: 1
Items: 
Size: 612315 Color: 4

Bin 4953: 387722 of cap free
Amount of items: 1
Items: 
Size: 612279 Color: 4

Bin 4954: 387927 of cap free
Amount of items: 1
Items: 
Size: 612074 Color: 4

Bin 4955: 387947 of cap free
Amount of items: 1
Items: 
Size: 612054 Color: 2

Bin 4956: 387960 of cap free
Amount of items: 1
Items: 
Size: 612041 Color: 4

Bin 4957: 387981 of cap free
Amount of items: 1
Items: 
Size: 612020 Color: 4

Bin 4958: 388008 of cap free
Amount of items: 1
Items: 
Size: 611993 Color: 2

Bin 4959: 388083 of cap free
Amount of items: 1
Items: 
Size: 611918 Color: 4

Bin 4960: 388231 of cap free
Amount of items: 1
Items: 
Size: 611770 Color: 3

Bin 4961: 388239 of cap free
Amount of items: 1
Items: 
Size: 611762 Color: 2

Bin 4962: 388294 of cap free
Amount of items: 1
Items: 
Size: 611707 Color: 4

Bin 4963: 388375 of cap free
Amount of items: 1
Items: 
Size: 611626 Color: 4

Bin 4964: 388523 of cap free
Amount of items: 1
Items: 
Size: 611478 Color: 2

Bin 4965: 388584 of cap free
Amount of items: 1
Items: 
Size: 611417 Color: 2

Bin 4966: 388632 of cap free
Amount of items: 1
Items: 
Size: 611369 Color: 4

Bin 4967: 388729 of cap free
Amount of items: 1
Items: 
Size: 611272 Color: 2

Bin 4968: 388882 of cap free
Amount of items: 1
Items: 
Size: 611119 Color: 0

Bin 4969: 388892 of cap free
Amount of items: 1
Items: 
Size: 611109 Color: 4

Bin 4970: 388939 of cap free
Amount of items: 1
Items: 
Size: 611062 Color: 2

Bin 4971: 388950 of cap free
Amount of items: 1
Items: 
Size: 611051 Color: 0

Bin 4972: 388960 of cap free
Amount of items: 1
Items: 
Size: 611041 Color: 2

Bin 4973: 389220 of cap free
Amount of items: 1
Items: 
Size: 610781 Color: 0

Bin 4974: 389267 of cap free
Amount of items: 1
Items: 
Size: 610734 Color: 2

Bin 4975: 389279 of cap free
Amount of items: 1
Items: 
Size: 610722 Color: 3

Bin 4976: 389338 of cap free
Amount of items: 1
Items: 
Size: 610663 Color: 0

Bin 4977: 389527 of cap free
Amount of items: 1
Items: 
Size: 610474 Color: 4

Bin 4978: 389587 of cap free
Amount of items: 1
Items: 
Size: 610414 Color: 4

Bin 4979: 389616 of cap free
Amount of items: 1
Items: 
Size: 610385 Color: 0

Bin 4980: 389644 of cap free
Amount of items: 1
Items: 
Size: 610357 Color: 0

Bin 4981: 389729 of cap free
Amount of items: 1
Items: 
Size: 610272 Color: 4

Bin 4982: 389738 of cap free
Amount of items: 1
Items: 
Size: 610263 Color: 2

Bin 4983: 389740 of cap free
Amount of items: 1
Items: 
Size: 610261 Color: 3

Bin 4984: 389836 of cap free
Amount of items: 1
Items: 
Size: 610165 Color: 4

Bin 4985: 389861 of cap free
Amount of items: 1
Items: 
Size: 610140 Color: 3

Bin 4986: 390008 of cap free
Amount of items: 1
Items: 
Size: 609993 Color: 1

Bin 4987: 390165 of cap free
Amount of items: 1
Items: 
Size: 609836 Color: 0

Bin 4988: 390186 of cap free
Amount of items: 1
Items: 
Size: 609815 Color: 3

Bin 4989: 390256 of cap free
Amount of items: 1
Items: 
Size: 609745 Color: 1

Bin 4990: 390389 of cap free
Amount of items: 1
Items: 
Size: 609612 Color: 1

Bin 4991: 390453 of cap free
Amount of items: 1
Items: 
Size: 609548 Color: 1

Bin 4992: 390469 of cap free
Amount of items: 1
Items: 
Size: 609532 Color: 0

Bin 4993: 390491 of cap free
Amount of items: 1
Items: 
Size: 609510 Color: 2

Bin 4994: 390674 of cap free
Amount of items: 1
Items: 
Size: 609327 Color: 1

Bin 4995: 390706 of cap free
Amount of items: 1
Items: 
Size: 609295 Color: 2

Bin 4996: 390720 of cap free
Amount of items: 1
Items: 
Size: 609281 Color: 3

Bin 4997: 390729 of cap free
Amount of items: 1
Items: 
Size: 609272 Color: 2

Bin 4998: 390761 of cap free
Amount of items: 1
Items: 
Size: 609240 Color: 2

Bin 4999: 390854 of cap free
Amount of items: 1
Items: 
Size: 609147 Color: 0

Bin 5000: 390942 of cap free
Amount of items: 1
Items: 
Size: 609059 Color: 0

Bin 5001: 391179 of cap free
Amount of items: 1
Items: 
Size: 608822 Color: 1

Bin 5002: 391220 of cap free
Amount of items: 1
Items: 
Size: 608781 Color: 0

Bin 5003: 391419 of cap free
Amount of items: 1
Items: 
Size: 608582 Color: 1

Bin 5004: 391433 of cap free
Amount of items: 1
Items: 
Size: 608568 Color: 0

Bin 5005: 391545 of cap free
Amount of items: 1
Items: 
Size: 608456 Color: 2

Bin 5006: 391573 of cap free
Amount of items: 1
Items: 
Size: 608428 Color: 3

Bin 5007: 391617 of cap free
Amount of items: 1
Items: 
Size: 608384 Color: 1

Bin 5008: 391645 of cap free
Amount of items: 1
Items: 
Size: 608356 Color: 0

Bin 5009: 391678 of cap free
Amount of items: 1
Items: 
Size: 608323 Color: 1

Bin 5010: 391733 of cap free
Amount of items: 1
Items: 
Size: 608268 Color: 3

Bin 5011: 391820 of cap free
Amount of items: 1
Items: 
Size: 608181 Color: 2

Bin 5012: 391834 of cap free
Amount of items: 1
Items: 
Size: 608167 Color: 0

Bin 5013: 391885 of cap free
Amount of items: 1
Items: 
Size: 608116 Color: 1

Bin 5014: 391888 of cap free
Amount of items: 1
Items: 
Size: 608113 Color: 4

Bin 5015: 391980 of cap free
Amount of items: 1
Items: 
Size: 608021 Color: 0

Bin 5016: 391998 of cap free
Amount of items: 1
Items: 
Size: 608003 Color: 3

Bin 5017: 392009 of cap free
Amount of items: 1
Items: 
Size: 607992 Color: 1

Bin 5018: 392030 of cap free
Amount of items: 1
Items: 
Size: 607971 Color: 2

Bin 5019: 392233 of cap free
Amount of items: 1
Items: 
Size: 607768 Color: 1

Bin 5020: 392255 of cap free
Amount of items: 1
Items: 
Size: 607746 Color: 1

Bin 5021: 392271 of cap free
Amount of items: 1
Items: 
Size: 607730 Color: 0

Bin 5022: 392280 of cap free
Amount of items: 1
Items: 
Size: 607721 Color: 1

Bin 5023: 392337 of cap free
Amount of items: 1
Items: 
Size: 607664 Color: 2

Bin 5024: 392391 of cap free
Amount of items: 1
Items: 
Size: 607610 Color: 4

Bin 5025: 392524 of cap free
Amount of items: 1
Items: 
Size: 607477 Color: 4

Bin 5026: 392625 of cap free
Amount of items: 1
Items: 
Size: 607376 Color: 4

Bin 5027: 392723 of cap free
Amount of items: 1
Items: 
Size: 607278 Color: 2

Bin 5028: 392780 of cap free
Amount of items: 1
Items: 
Size: 607221 Color: 3

Bin 5029: 392870 of cap free
Amount of items: 1
Items: 
Size: 607131 Color: 1

Bin 5030: 393025 of cap free
Amount of items: 1
Items: 
Size: 606976 Color: 0

Bin 5031: 393038 of cap free
Amount of items: 1
Items: 
Size: 606963 Color: 0

Bin 5032: 393094 of cap free
Amount of items: 1
Items: 
Size: 606907 Color: 0

Bin 5033: 393172 of cap free
Amount of items: 1
Items: 
Size: 606829 Color: 1

Bin 5034: 393185 of cap free
Amount of items: 1
Items: 
Size: 606816 Color: 4

Bin 5035: 393234 of cap free
Amount of items: 1
Items: 
Size: 606767 Color: 4

Bin 5036: 393236 of cap free
Amount of items: 1
Items: 
Size: 606765 Color: 2

Bin 5037: 393240 of cap free
Amount of items: 1
Items: 
Size: 606761 Color: 4

Bin 5038: 393340 of cap free
Amount of items: 1
Items: 
Size: 606661 Color: 2

Bin 5039: 393379 of cap free
Amount of items: 1
Items: 
Size: 606622 Color: 2

Bin 5040: 393383 of cap free
Amount of items: 1
Items: 
Size: 606618 Color: 4

Bin 5041: 393481 of cap free
Amount of items: 1
Items: 
Size: 606520 Color: 3

Bin 5042: 393529 of cap free
Amount of items: 1
Items: 
Size: 606472 Color: 4

Bin 5043: 393544 of cap free
Amount of items: 1
Items: 
Size: 606457 Color: 3

Bin 5044: 393640 of cap free
Amount of items: 1
Items: 
Size: 606361 Color: 1

Bin 5045: 393750 of cap free
Amount of items: 1
Items: 
Size: 606251 Color: 3

Bin 5046: 393793 of cap free
Amount of items: 1
Items: 
Size: 606208 Color: 2

Bin 5047: 393815 of cap free
Amount of items: 1
Items: 
Size: 606186 Color: 0

Bin 5048: 393864 of cap free
Amount of items: 1
Items: 
Size: 606137 Color: 1

Bin 5049: 393876 of cap free
Amount of items: 1
Items: 
Size: 606125 Color: 4

Bin 5050: 393937 of cap free
Amount of items: 1
Items: 
Size: 606064 Color: 2

Bin 5051: 393975 of cap free
Amount of items: 1
Items: 
Size: 606026 Color: 1

Bin 5052: 394058 of cap free
Amount of items: 1
Items: 
Size: 605943 Color: 2

Bin 5053: 394089 of cap free
Amount of items: 1
Items: 
Size: 605912 Color: 4

Bin 5054: 394094 of cap free
Amount of items: 1
Items: 
Size: 605907 Color: 4

Bin 5055: 394468 of cap free
Amount of items: 1
Items: 
Size: 605533 Color: 4

Bin 5056: 394591 of cap free
Amount of items: 1
Items: 
Size: 605410 Color: 4

Bin 5057: 394609 of cap free
Amount of items: 1
Items: 
Size: 605392 Color: 3

Bin 5058: 394674 of cap free
Amount of items: 1
Items: 
Size: 605327 Color: 0

Bin 5059: 394725 of cap free
Amount of items: 1
Items: 
Size: 605276 Color: 1

Bin 5060: 394822 of cap free
Amount of items: 1
Items: 
Size: 605179 Color: 3

Bin 5061: 394827 of cap free
Amount of items: 1
Items: 
Size: 605174 Color: 1

Bin 5062: 394930 of cap free
Amount of items: 1
Items: 
Size: 605071 Color: 0

Bin 5063: 395022 of cap free
Amount of items: 1
Items: 
Size: 604979 Color: 0

Bin 5064: 395022 of cap free
Amount of items: 1
Items: 
Size: 604979 Color: 4

Bin 5065: 395056 of cap free
Amount of items: 1
Items: 
Size: 604945 Color: 1

Bin 5066: 395071 of cap free
Amount of items: 1
Items: 
Size: 604930 Color: 3

Bin 5067: 395209 of cap free
Amount of items: 1
Items: 
Size: 604792 Color: 2

Bin 5068: 395320 of cap free
Amount of items: 1
Items: 
Size: 604681 Color: 0

Bin 5069: 395401 of cap free
Amount of items: 1
Items: 
Size: 604600 Color: 4

Bin 5070: 395475 of cap free
Amount of items: 1
Items: 
Size: 604526 Color: 4

Bin 5071: 395525 of cap free
Amount of items: 1
Items: 
Size: 604476 Color: 4

Bin 5072: 395652 of cap free
Amount of items: 1
Items: 
Size: 604349 Color: 2

Bin 5073: 395711 of cap free
Amount of items: 1
Items: 
Size: 604290 Color: 0

Bin 5074: 395747 of cap free
Amount of items: 1
Items: 
Size: 604254 Color: 0

Bin 5075: 395785 of cap free
Amount of items: 1
Items: 
Size: 604216 Color: 3

Bin 5076: 395819 of cap free
Amount of items: 1
Items: 
Size: 604182 Color: 2

Bin 5077: 395959 of cap free
Amount of items: 1
Items: 
Size: 604042 Color: 0

Bin 5078: 396105 of cap free
Amount of items: 1
Items: 
Size: 603896 Color: 4

Bin 5079: 396253 of cap free
Amount of items: 1
Items: 
Size: 603748 Color: 3

Bin 5080: 396294 of cap free
Amount of items: 1
Items: 
Size: 603707 Color: 3

Bin 5081: 396336 of cap free
Amount of items: 1
Items: 
Size: 603665 Color: 4

Bin 5082: 396369 of cap free
Amount of items: 1
Items: 
Size: 603632 Color: 4

Bin 5083: 396426 of cap free
Amount of items: 1
Items: 
Size: 603575 Color: 2

Bin 5084: 396494 of cap free
Amount of items: 1
Items: 
Size: 603507 Color: 0

Bin 5085: 396558 of cap free
Amount of items: 1
Items: 
Size: 603443 Color: 4

Bin 5086: 396620 of cap free
Amount of items: 1
Items: 
Size: 603381 Color: 3

Bin 5087: 396629 of cap free
Amount of items: 1
Items: 
Size: 603372 Color: 1

Bin 5088: 396800 of cap free
Amount of items: 1
Items: 
Size: 603201 Color: 1

Bin 5089: 396938 of cap free
Amount of items: 1
Items: 
Size: 603063 Color: 4

Bin 5090: 397034 of cap free
Amount of items: 1
Items: 
Size: 602967 Color: 4

Bin 5091: 397149 of cap free
Amount of items: 1
Items: 
Size: 602852 Color: 3

Bin 5092: 397197 of cap free
Amount of items: 1
Items: 
Size: 602804 Color: 1

Bin 5093: 397261 of cap free
Amount of items: 1
Items: 
Size: 602740 Color: 0

Bin 5094: 397287 of cap free
Amount of items: 1
Items: 
Size: 602714 Color: 4

Bin 5095: 397302 of cap free
Amount of items: 1
Items: 
Size: 602699 Color: 0

Bin 5096: 397302 of cap free
Amount of items: 1
Items: 
Size: 602699 Color: 2

Bin 5097: 397351 of cap free
Amount of items: 1
Items: 
Size: 602650 Color: 3

Bin 5098: 397374 of cap free
Amount of items: 1
Items: 
Size: 602627 Color: 2

Bin 5099: 397390 of cap free
Amount of items: 1
Items: 
Size: 602611 Color: 1

Bin 5100: 397410 of cap free
Amount of items: 1
Items: 
Size: 602591 Color: 3

Bin 5101: 397508 of cap free
Amount of items: 1
Items: 
Size: 602493 Color: 1

Bin 5102: 397589 of cap free
Amount of items: 1
Items: 
Size: 602412 Color: 3

Bin 5103: 397594 of cap free
Amount of items: 1
Items: 
Size: 602407 Color: 4

Bin 5104: 397717 of cap free
Amount of items: 1
Items: 
Size: 602284 Color: 1

Bin 5105: 397792 of cap free
Amount of items: 1
Items: 
Size: 602209 Color: 1

Bin 5106: 397851 of cap free
Amount of items: 1
Items: 
Size: 602150 Color: 0

Bin 5107: 397933 of cap free
Amount of items: 1
Items: 
Size: 602068 Color: 4

Bin 5108: 398000 of cap free
Amount of items: 1
Items: 
Size: 602001 Color: 3

Bin 5109: 398007 of cap free
Amount of items: 1
Items: 
Size: 601994 Color: 0

Bin 5110: 398162 of cap free
Amount of items: 1
Items: 
Size: 601839 Color: 1

Bin 5111: 398250 of cap free
Amount of items: 1
Items: 
Size: 601751 Color: 2

Bin 5112: 398449 of cap free
Amount of items: 1
Items: 
Size: 601552 Color: 3

Bin 5113: 398810 of cap free
Amount of items: 1
Items: 
Size: 601191 Color: 1

Bin 5114: 398845 of cap free
Amount of items: 1
Items: 
Size: 601156 Color: 3

Bin 5115: 398876 of cap free
Amount of items: 1
Items: 
Size: 601125 Color: 1

Bin 5116: 398935 of cap free
Amount of items: 1
Items: 
Size: 601066 Color: 1

Bin 5117: 399054 of cap free
Amount of items: 1
Items: 
Size: 600947 Color: 4

Bin 5118: 399146 of cap free
Amount of items: 1
Items: 
Size: 600855 Color: 4

Bin 5119: 399208 of cap free
Amount of items: 1
Items: 
Size: 600793 Color: 4

Bin 5120: 399285 of cap free
Amount of items: 1
Items: 
Size: 600716 Color: 4

Bin 5121: 399372 of cap free
Amount of items: 1
Items: 
Size: 600629 Color: 0

Bin 5122: 399447 of cap free
Amount of items: 1
Items: 
Size: 600554 Color: 4

Bin 5123: 399512 of cap free
Amount of items: 1
Items: 
Size: 600489 Color: 0

Bin 5124: 399595 of cap free
Amount of items: 1
Items: 
Size: 600406 Color: 0

Bin 5125: 399612 of cap free
Amount of items: 1
Items: 
Size: 600389 Color: 4

Bin 5126: 399652 of cap free
Amount of items: 1
Items: 
Size: 600349 Color: 1

Bin 5127: 399870 of cap free
Amount of items: 1
Items: 
Size: 600131 Color: 2

Bin 5128: 399976 of cap free
Amount of items: 1
Items: 
Size: 600025 Color: 2

Bin 5129: 400231 of cap free
Amount of items: 1
Items: 
Size: 599770 Color: 4

Bin 5130: 400403 of cap free
Amount of items: 1
Items: 
Size: 599598 Color: 2

Bin 5131: 400484 of cap free
Amount of items: 1
Items: 
Size: 599517 Color: 4

Bin 5132: 400486 of cap free
Amount of items: 1
Items: 
Size: 599515 Color: 4

Bin 5133: 400662 of cap free
Amount of items: 1
Items: 
Size: 599339 Color: 2

Bin 5134: 400662 of cap free
Amount of items: 1
Items: 
Size: 599339 Color: 3

Bin 5135: 400983 of cap free
Amount of items: 1
Items: 
Size: 599018 Color: 2

Bin 5136: 401026 of cap free
Amount of items: 1
Items: 
Size: 598975 Color: 2

Bin 5137: 401036 of cap free
Amount of items: 1
Items: 
Size: 598965 Color: 2

Bin 5138: 401052 of cap free
Amount of items: 1
Items: 
Size: 598949 Color: 2

Bin 5139: 401135 of cap free
Amount of items: 1
Items: 
Size: 598866 Color: 2

Bin 5140: 401198 of cap free
Amount of items: 1
Items: 
Size: 598803 Color: 4

Bin 5141: 401301 of cap free
Amount of items: 1
Items: 
Size: 598700 Color: 2

Bin 5142: 401343 of cap free
Amount of items: 1
Items: 
Size: 598658 Color: 3

Bin 5143: 401363 of cap free
Amount of items: 1
Items: 
Size: 598638 Color: 2

Bin 5144: 401415 of cap free
Amount of items: 1
Items: 
Size: 598586 Color: 1

Bin 5145: 401541 of cap free
Amount of items: 1
Items: 
Size: 598460 Color: 3

Bin 5146: 401591 of cap free
Amount of items: 1
Items: 
Size: 598410 Color: 1

Bin 5147: 401660 of cap free
Amount of items: 1
Items: 
Size: 598341 Color: 1

Bin 5148: 401746 of cap free
Amount of items: 1
Items: 
Size: 598255 Color: 0

Bin 5149: 401763 of cap free
Amount of items: 1
Items: 
Size: 598238 Color: 0

Bin 5150: 401864 of cap free
Amount of items: 1
Items: 
Size: 598137 Color: 1

Bin 5151: 401901 of cap free
Amount of items: 1
Items: 
Size: 598100 Color: 2

Bin 5152: 401917 of cap free
Amount of items: 1
Items: 
Size: 598084 Color: 4

Bin 5153: 401918 of cap free
Amount of items: 1
Items: 
Size: 598083 Color: 3

Bin 5154: 402017 of cap free
Amount of items: 1
Items: 
Size: 597984 Color: 3

Bin 5155: 402117 of cap free
Amount of items: 1
Items: 
Size: 597884 Color: 0

Bin 5156: 402362 of cap free
Amount of items: 1
Items: 
Size: 597639 Color: 3

Bin 5157: 402386 of cap free
Amount of items: 1
Items: 
Size: 597615 Color: 2

Bin 5158: 402652 of cap free
Amount of items: 1
Items: 
Size: 597349 Color: 2

Bin 5159: 402661 of cap free
Amount of items: 1
Items: 
Size: 597340 Color: 2

Bin 5160: 402776 of cap free
Amount of items: 1
Items: 
Size: 597225 Color: 4

Bin 5161: 402819 of cap free
Amount of items: 1
Items: 
Size: 597182 Color: 2

Bin 5162: 402865 of cap free
Amount of items: 1
Items: 
Size: 597136 Color: 0

Bin 5163: 402998 of cap free
Amount of items: 1
Items: 
Size: 597003 Color: 2

Bin 5164: 403090 of cap free
Amount of items: 1
Items: 
Size: 596911 Color: 2

Bin 5165: 403188 of cap free
Amount of items: 1
Items: 
Size: 596813 Color: 2

Bin 5166: 403392 of cap free
Amount of items: 1
Items: 
Size: 596609 Color: 2

Bin 5167: 403399 of cap free
Amount of items: 1
Items: 
Size: 596602 Color: 4

Bin 5168: 403458 of cap free
Amount of items: 1
Items: 
Size: 596543 Color: 3

Bin 5169: 403479 of cap free
Amount of items: 1
Items: 
Size: 596522 Color: 3

Bin 5170: 403485 of cap free
Amount of items: 1
Items: 
Size: 596516 Color: 0

Bin 5171: 403652 of cap free
Amount of items: 1
Items: 
Size: 596349 Color: 0

Bin 5172: 403895 of cap free
Amount of items: 1
Items: 
Size: 596106 Color: 3

Bin 5173: 404096 of cap free
Amount of items: 1
Items: 
Size: 595905 Color: 4

Bin 5174: 404168 of cap free
Amount of items: 1
Items: 
Size: 595833 Color: 4

Bin 5175: 404245 of cap free
Amount of items: 1
Items: 
Size: 595756 Color: 1

Bin 5176: 404287 of cap free
Amount of items: 1
Items: 
Size: 595714 Color: 3

Bin 5177: 404312 of cap free
Amount of items: 1
Items: 
Size: 595689 Color: 3

Bin 5178: 404341 of cap free
Amount of items: 1
Items: 
Size: 595660 Color: 3

Bin 5179: 404363 of cap free
Amount of items: 1
Items: 
Size: 595638 Color: 3

Bin 5180: 404410 of cap free
Amount of items: 1
Items: 
Size: 595591 Color: 4

Bin 5181: 404449 of cap free
Amount of items: 1
Items: 
Size: 595552 Color: 1

Bin 5182: 404508 of cap free
Amount of items: 1
Items: 
Size: 595493 Color: 4

Bin 5183: 404568 of cap free
Amount of items: 1
Items: 
Size: 595433 Color: 4

Bin 5184: 404644 of cap free
Amount of items: 1
Items: 
Size: 595357 Color: 3

Bin 5185: 404657 of cap free
Amount of items: 1
Items: 
Size: 595344 Color: 4

Bin 5186: 404726 of cap free
Amount of items: 1
Items: 
Size: 595275 Color: 3

Bin 5187: 404873 of cap free
Amount of items: 1
Items: 
Size: 595128 Color: 0

Bin 5188: 404973 of cap free
Amount of items: 1
Items: 
Size: 595028 Color: 1

Bin 5189: 404990 of cap free
Amount of items: 1
Items: 
Size: 595011 Color: 1

Bin 5190: 405015 of cap free
Amount of items: 1
Items: 
Size: 594986 Color: 1

Bin 5191: 405016 of cap free
Amount of items: 1
Items: 
Size: 594985 Color: 0

Bin 5192: 405071 of cap free
Amount of items: 1
Items: 
Size: 594930 Color: 1

Bin 5193: 405211 of cap free
Amount of items: 1
Items: 
Size: 594790 Color: 0

Bin 5194: 405257 of cap free
Amount of items: 1
Items: 
Size: 594744 Color: 3

Bin 5195: 405282 of cap free
Amount of items: 1
Items: 
Size: 594719 Color: 0

Bin 5196: 405410 of cap free
Amount of items: 1
Items: 
Size: 594591 Color: 4

Bin 5197: 405455 of cap free
Amount of items: 1
Items: 
Size: 594546 Color: 1

Bin 5198: 405514 of cap free
Amount of items: 1
Items: 
Size: 594487 Color: 4

Bin 5199: 405558 of cap free
Amount of items: 1
Items: 
Size: 594443 Color: 4

Bin 5200: 405624 of cap free
Amount of items: 1
Items: 
Size: 594377 Color: 4

Bin 5201: 405702 of cap free
Amount of items: 1
Items: 
Size: 594299 Color: 1

Bin 5202: 405718 of cap free
Amount of items: 1
Items: 
Size: 594283 Color: 4

Bin 5203: 405787 of cap free
Amount of items: 1
Items: 
Size: 594214 Color: 2

Bin 5204: 405811 of cap free
Amount of items: 1
Items: 
Size: 594190 Color: 3

Bin 5205: 405839 of cap free
Amount of items: 1
Items: 
Size: 594162 Color: 1

Bin 5206: 405844 of cap free
Amount of items: 1
Items: 
Size: 594157 Color: 4

Bin 5207: 405850 of cap free
Amount of items: 1
Items: 
Size: 594151 Color: 3

Bin 5208: 405935 of cap free
Amount of items: 1
Items: 
Size: 594066 Color: 4

Bin 5209: 405944 of cap free
Amount of items: 1
Items: 
Size: 594057 Color: 1

Bin 5210: 406059 of cap free
Amount of items: 1
Items: 
Size: 593942 Color: 0

Bin 5211: 406074 of cap free
Amount of items: 1
Items: 
Size: 593927 Color: 3

Bin 5212: 406117 of cap free
Amount of items: 1
Items: 
Size: 593884 Color: 0

Bin 5213: 406146 of cap free
Amount of items: 1
Items: 
Size: 593855 Color: 2

Bin 5214: 406191 of cap free
Amount of items: 1
Items: 
Size: 593810 Color: 1

Bin 5215: 406286 of cap free
Amount of items: 1
Items: 
Size: 593715 Color: 4

Bin 5216: 406317 of cap free
Amount of items: 1
Items: 
Size: 593684 Color: 4

Bin 5217: 406368 of cap free
Amount of items: 1
Items: 
Size: 593633 Color: 4

Bin 5218: 406437 of cap free
Amount of items: 1
Items: 
Size: 593564 Color: 0

Bin 5219: 406522 of cap free
Amount of items: 1
Items: 
Size: 593479 Color: 2

Bin 5220: 406571 of cap free
Amount of items: 1
Items: 
Size: 593430 Color: 2

Bin 5221: 406579 of cap free
Amount of items: 1
Items: 
Size: 593422 Color: 2

Bin 5222: 406626 of cap free
Amount of items: 1
Items: 
Size: 593375 Color: 1

Bin 5223: 406719 of cap free
Amount of items: 1
Items: 
Size: 593282 Color: 4

Bin 5224: 406720 of cap free
Amount of items: 1
Items: 
Size: 593281 Color: 4

Bin 5225: 406738 of cap free
Amount of items: 1
Items: 
Size: 593263 Color: 4

Bin 5226: 406814 of cap free
Amount of items: 1
Items: 
Size: 593187 Color: 0

Bin 5227: 406848 of cap free
Amount of items: 1
Items: 
Size: 593153 Color: 2

Bin 5228: 406951 of cap free
Amount of items: 1
Items: 
Size: 593050 Color: 3

Bin 5229: 407015 of cap free
Amount of items: 1
Items: 
Size: 592986 Color: 0

Bin 5230: 407144 of cap free
Amount of items: 1
Items: 
Size: 592857 Color: 1

Bin 5231: 407411 of cap free
Amount of items: 1
Items: 
Size: 592590 Color: 1

Bin 5232: 407751 of cap free
Amount of items: 1
Items: 
Size: 592250 Color: 3

Bin 5233: 407791 of cap free
Amount of items: 1
Items: 
Size: 592210 Color: 1

Bin 5234: 407977 of cap free
Amount of items: 1
Items: 
Size: 592024 Color: 0

Bin 5235: 408055 of cap free
Amount of items: 1
Items: 
Size: 591946 Color: 3

Bin 5236: 408071 of cap free
Amount of items: 1
Items: 
Size: 591930 Color: 3

Bin 5237: 408082 of cap free
Amount of items: 1
Items: 
Size: 591919 Color: 0

Bin 5238: 408139 of cap free
Amount of items: 1
Items: 
Size: 591862 Color: 2

Bin 5239: 408177 of cap free
Amount of items: 1
Items: 
Size: 591824 Color: 0

Bin 5240: 408202 of cap free
Amount of items: 1
Items: 
Size: 591799 Color: 0

Bin 5241: 408245 of cap free
Amount of items: 1
Items: 
Size: 591756 Color: 0

Bin 5242: 408322 of cap free
Amount of items: 1
Items: 
Size: 591679 Color: 0

Bin 5243: 408327 of cap free
Amount of items: 1
Items: 
Size: 591674 Color: 2

Bin 5244: 408331 of cap free
Amount of items: 1
Items: 
Size: 591670 Color: 0

Bin 5245: 408427 of cap free
Amount of items: 1
Items: 
Size: 591574 Color: 2

Bin 5246: 408577 of cap free
Amount of items: 1
Items: 
Size: 591424 Color: 2

Bin 5247: 408627 of cap free
Amount of items: 1
Items: 
Size: 591374 Color: 2

Bin 5248: 408635 of cap free
Amount of items: 1
Items: 
Size: 591366 Color: 4

Bin 5249: 408717 of cap free
Amount of items: 1
Items: 
Size: 591284 Color: 2

Bin 5250: 408733 of cap free
Amount of items: 1
Items: 
Size: 591268 Color: 0

Bin 5251: 408802 of cap free
Amount of items: 1
Items: 
Size: 591199 Color: 0

Bin 5252: 408977 of cap free
Amount of items: 1
Items: 
Size: 591024 Color: 3

Bin 5253: 408991 of cap free
Amount of items: 1
Items: 
Size: 591010 Color: 0

Bin 5254: 409054 of cap free
Amount of items: 1
Items: 
Size: 590947 Color: 0

Bin 5255: 409104 of cap free
Amount of items: 1
Items: 
Size: 590897 Color: 3

Bin 5256: 409139 of cap free
Amount of items: 1
Items: 
Size: 590862 Color: 0

Bin 5257: 409220 of cap free
Amount of items: 1
Items: 
Size: 590781 Color: 3

Bin 5258: 409326 of cap free
Amount of items: 1
Items: 
Size: 590675 Color: 3

Bin 5259: 409433 of cap free
Amount of items: 1
Items: 
Size: 590568 Color: 1

Bin 5260: 409489 of cap free
Amount of items: 1
Items: 
Size: 590512 Color: 0

Bin 5261: 409492 of cap free
Amount of items: 1
Items: 
Size: 590509 Color: 1

Bin 5262: 409538 of cap free
Amount of items: 1
Items: 
Size: 590463 Color: 3

Bin 5263: 409569 of cap free
Amount of items: 1
Items: 
Size: 590432 Color: 1

Bin 5264: 409597 of cap free
Amount of items: 1
Items: 
Size: 590404 Color: 2

Bin 5265: 409679 of cap free
Amount of items: 1
Items: 
Size: 590322 Color: 2

Bin 5266: 409683 of cap free
Amount of items: 1
Items: 
Size: 590318 Color: 0

Bin 5267: 409722 of cap free
Amount of items: 1
Items: 
Size: 590279 Color: 3

Bin 5268: 409797 of cap free
Amount of items: 1
Items: 
Size: 590204 Color: 3

Bin 5269: 409859 of cap free
Amount of items: 1
Items: 
Size: 590142 Color: 2

Bin 5270: 410020 of cap free
Amount of items: 1
Items: 
Size: 589981 Color: 2

Bin 5271: 410038 of cap free
Amount of items: 1
Items: 
Size: 589963 Color: 3

Bin 5272: 410255 of cap free
Amount of items: 1
Items: 
Size: 589746 Color: 3

Bin 5273: 410277 of cap free
Amount of items: 1
Items: 
Size: 589724 Color: 2

Bin 5274: 410374 of cap free
Amount of items: 1
Items: 
Size: 589627 Color: 4

Bin 5275: 410464 of cap free
Amount of items: 1
Items: 
Size: 589537 Color: 2

Bin 5276: 410483 of cap free
Amount of items: 1
Items: 
Size: 589518 Color: 4

Bin 5277: 410795 of cap free
Amount of items: 1
Items: 
Size: 589206 Color: 2

Bin 5278: 411031 of cap free
Amount of items: 1
Items: 
Size: 588970 Color: 0

Bin 5279: 411097 of cap free
Amount of items: 1
Items: 
Size: 588904 Color: 3

Bin 5280: 411111 of cap free
Amount of items: 1
Items: 
Size: 588890 Color: 3

Bin 5281: 411179 of cap free
Amount of items: 1
Items: 
Size: 588822 Color: 0

Bin 5282: 411190 of cap free
Amount of items: 1
Items: 
Size: 588811 Color: 3

Bin 5283: 411291 of cap free
Amount of items: 1
Items: 
Size: 588710 Color: 0

Bin 5284: 411431 of cap free
Amount of items: 1
Items: 
Size: 588570 Color: 2

Bin 5285: 411448 of cap free
Amount of items: 1
Items: 
Size: 588553 Color: 4

Bin 5286: 411482 of cap free
Amount of items: 1
Items: 
Size: 588519 Color: 1

Bin 5287: 411503 of cap free
Amount of items: 1
Items: 
Size: 588498 Color: 4

Bin 5288: 411597 of cap free
Amount of items: 1
Items: 
Size: 588404 Color: 3

Bin 5289: 411728 of cap free
Amount of items: 1
Items: 
Size: 588273 Color: 2

Bin 5290: 411781 of cap free
Amount of items: 1
Items: 
Size: 588220 Color: 0

Bin 5291: 411863 of cap free
Amount of items: 1
Items: 
Size: 588138 Color: 4

Bin 5292: 412016 of cap free
Amount of items: 1
Items: 
Size: 587985 Color: 3

Bin 5293: 412136 of cap free
Amount of items: 1
Items: 
Size: 587865 Color: 2

Bin 5294: 412145 of cap free
Amount of items: 1
Items: 
Size: 587856 Color: 1

Bin 5295: 412151 of cap free
Amount of items: 1
Items: 
Size: 587850 Color: 3

Bin 5296: 412383 of cap free
Amount of items: 1
Items: 
Size: 587618 Color: 1

Bin 5297: 412578 of cap free
Amount of items: 1
Items: 
Size: 587423 Color: 0

Bin 5298: 412665 of cap free
Amount of items: 1
Items: 
Size: 587336 Color: 0

Bin 5299: 412839 of cap free
Amount of items: 1
Items: 
Size: 587162 Color: 3

Bin 5300: 412941 of cap free
Amount of items: 1
Items: 
Size: 587060 Color: 2

Bin 5301: 412988 of cap free
Amount of items: 1
Items: 
Size: 587013 Color: 4

Bin 5302: 413036 of cap free
Amount of items: 1
Items: 
Size: 586965 Color: 4

Bin 5303: 413057 of cap free
Amount of items: 1
Items: 
Size: 586944 Color: 3

Bin 5304: 413096 of cap free
Amount of items: 1
Items: 
Size: 586905 Color: 4

Bin 5305: 413163 of cap free
Amount of items: 1
Items: 
Size: 586838 Color: 1

Bin 5306: 413212 of cap free
Amount of items: 1
Items: 
Size: 586789 Color: 1

Bin 5307: 413222 of cap free
Amount of items: 1
Items: 
Size: 586779 Color: 0

Bin 5308: 413298 of cap free
Amount of items: 1
Items: 
Size: 586703 Color: 1

Bin 5309: 413345 of cap free
Amount of items: 1
Items: 
Size: 586656 Color: 0

Bin 5310: 413422 of cap free
Amount of items: 1
Items: 
Size: 586579 Color: 2

Bin 5311: 413469 of cap free
Amount of items: 1
Items: 
Size: 586532 Color: 2

Bin 5312: 413479 of cap free
Amount of items: 1
Items: 
Size: 586522 Color: 0

Bin 5313: 413585 of cap free
Amount of items: 1
Items: 
Size: 586416 Color: 0

Bin 5314: 413598 of cap free
Amount of items: 1
Items: 
Size: 586403 Color: 2

Bin 5315: 413673 of cap free
Amount of items: 1
Items: 
Size: 586328 Color: 0

Bin 5316: 413709 of cap free
Amount of items: 1
Items: 
Size: 586292 Color: 3

Bin 5317: 413746 of cap free
Amount of items: 1
Items: 
Size: 586255 Color: 3

Bin 5318: 413759 of cap free
Amount of items: 1
Items: 
Size: 586242 Color: 2

Bin 5319: 413770 of cap free
Amount of items: 1
Items: 
Size: 586231 Color: 1

Bin 5320: 413863 of cap free
Amount of items: 1
Items: 
Size: 586138 Color: 2

Bin 5321: 413924 of cap free
Amount of items: 1
Items: 
Size: 586077 Color: 4

Bin 5322: 413945 of cap free
Amount of items: 1
Items: 
Size: 586056 Color: 2

Bin 5323: 413958 of cap free
Amount of items: 1
Items: 
Size: 586043 Color: 2

Bin 5324: 413994 of cap free
Amount of items: 1
Items: 
Size: 586007 Color: 4

Bin 5325: 414063 of cap free
Amount of items: 1
Items: 
Size: 585938 Color: 0

Bin 5326: 414073 of cap free
Amount of items: 1
Items: 
Size: 585928 Color: 4

Bin 5327: 414093 of cap free
Amount of items: 1
Items: 
Size: 585908 Color: 4

Bin 5328: 414148 of cap free
Amount of items: 1
Items: 
Size: 585853 Color: 1

Bin 5329: 414165 of cap free
Amount of items: 1
Items: 
Size: 585836 Color: 1

Bin 5330: 414202 of cap free
Amount of items: 1
Items: 
Size: 585799 Color: 1

Bin 5331: 414247 of cap free
Amount of items: 1
Items: 
Size: 585754 Color: 3

Bin 5332: 414398 of cap free
Amount of items: 1
Items: 
Size: 585603 Color: 2

Bin 5333: 414598 of cap free
Amount of items: 1
Items: 
Size: 585403 Color: 4

Bin 5334: 414600 of cap free
Amount of items: 1
Items: 
Size: 585401 Color: 0

Bin 5335: 414620 of cap free
Amount of items: 1
Items: 
Size: 585381 Color: 2

Bin 5336: 414737 of cap free
Amount of items: 1
Items: 
Size: 585264 Color: 3

Bin 5337: 414773 of cap free
Amount of items: 1
Items: 
Size: 585228 Color: 3

Bin 5338: 414875 of cap free
Amount of items: 1
Items: 
Size: 585126 Color: 1

Bin 5339: 414907 of cap free
Amount of items: 1
Items: 
Size: 585094 Color: 3

Bin 5340: 414942 of cap free
Amount of items: 1
Items: 
Size: 585059 Color: 3

Bin 5341: 414943 of cap free
Amount of items: 1
Items: 
Size: 585058 Color: 2

Bin 5342: 414989 of cap free
Amount of items: 1
Items: 
Size: 585012 Color: 1

Bin 5343: 415054 of cap free
Amount of items: 1
Items: 
Size: 584947 Color: 0

Bin 5344: 415103 of cap free
Amount of items: 1
Items: 
Size: 584898 Color: 4

Bin 5345: 415174 of cap free
Amount of items: 1
Items: 
Size: 584827 Color: 2

Bin 5346: 415232 of cap free
Amount of items: 1
Items: 
Size: 584769 Color: 1

Bin 5347: 415330 of cap free
Amount of items: 1
Items: 
Size: 584671 Color: 3

Bin 5348: 415346 of cap free
Amount of items: 1
Items: 
Size: 584655 Color: 4

Bin 5349: 415420 of cap free
Amount of items: 1
Items: 
Size: 584581 Color: 0

Bin 5350: 415457 of cap free
Amount of items: 1
Items: 
Size: 584544 Color: 0

Bin 5351: 415499 of cap free
Amount of items: 1
Items: 
Size: 584502 Color: 2

Bin 5352: 415543 of cap free
Amount of items: 1
Items: 
Size: 584458 Color: 3

Bin 5353: 415610 of cap free
Amount of items: 1
Items: 
Size: 584391 Color: 3

Bin 5354: 415680 of cap free
Amount of items: 1
Items: 
Size: 584321 Color: 1

Bin 5355: 415781 of cap free
Amount of items: 1
Items: 
Size: 584220 Color: 3

Bin 5356: 415817 of cap free
Amount of items: 1
Items: 
Size: 584184 Color: 0

Bin 5357: 415835 of cap free
Amount of items: 1
Items: 
Size: 584166 Color: 1

Bin 5358: 415838 of cap free
Amount of items: 1
Items: 
Size: 584163 Color: 1

Bin 5359: 415840 of cap free
Amount of items: 1
Items: 
Size: 584161 Color: 1

Bin 5360: 415860 of cap free
Amount of items: 1
Items: 
Size: 584141 Color: 2

Bin 5361: 415917 of cap free
Amount of items: 1
Items: 
Size: 584084 Color: 2

Bin 5362: 415944 of cap free
Amount of items: 1
Items: 
Size: 584057 Color: 1

Bin 5363: 415952 of cap free
Amount of items: 1
Items: 
Size: 584049 Color: 3

Bin 5364: 415985 of cap free
Amount of items: 1
Items: 
Size: 584016 Color: 2

Bin 5365: 415991 of cap free
Amount of items: 1
Items: 
Size: 584010 Color: 4

Bin 5366: 416037 of cap free
Amount of items: 1
Items: 
Size: 583964 Color: 1

Bin 5367: 416099 of cap free
Amount of items: 1
Items: 
Size: 583902 Color: 3

Bin 5368: 416108 of cap free
Amount of items: 1
Items: 
Size: 583893 Color: 0

Bin 5369: 416187 of cap free
Amount of items: 1
Items: 
Size: 583814 Color: 1

Bin 5370: 416210 of cap free
Amount of items: 1
Items: 
Size: 583791 Color: 0

Bin 5371: 416228 of cap free
Amount of items: 1
Items: 
Size: 583773 Color: 1

Bin 5372: 416434 of cap free
Amount of items: 1
Items: 
Size: 583567 Color: 2

Bin 5373: 416947 of cap free
Amount of items: 1
Items: 
Size: 583054 Color: 0

Bin 5374: 417107 of cap free
Amount of items: 1
Items: 
Size: 582894 Color: 0

Bin 5375: 417218 of cap free
Amount of items: 1
Items: 
Size: 582783 Color: 0

Bin 5376: 417363 of cap free
Amount of items: 1
Items: 
Size: 582638 Color: 2

Bin 5377: 417441 of cap free
Amount of items: 1
Items: 
Size: 582560 Color: 3

Bin 5378: 417477 of cap free
Amount of items: 1
Items: 
Size: 582524 Color: 4

Bin 5379: 417537 of cap free
Amount of items: 1
Items: 
Size: 582464 Color: 0

Bin 5380: 417554 of cap free
Amount of items: 1
Items: 
Size: 582447 Color: 1

Bin 5381: 417576 of cap free
Amount of items: 1
Items: 
Size: 582425 Color: 4

Bin 5382: 417639 of cap free
Amount of items: 1
Items: 
Size: 582362 Color: 4

Bin 5383: 417704 of cap free
Amount of items: 1
Items: 
Size: 582297 Color: 2

Bin 5384: 417709 of cap free
Amount of items: 1
Items: 
Size: 582292 Color: 3

Bin 5385: 417778 of cap free
Amount of items: 1
Items: 
Size: 582223 Color: 1

Bin 5386: 418047 of cap free
Amount of items: 1
Items: 
Size: 581954 Color: 4

Bin 5387: 418140 of cap free
Amount of items: 1
Items: 
Size: 581861 Color: 0

Bin 5388: 418294 of cap free
Amount of items: 1
Items: 
Size: 581707 Color: 0

Bin 5389: 418321 of cap free
Amount of items: 1
Items: 
Size: 581680 Color: 1

Bin 5390: 418438 of cap free
Amount of items: 1
Items: 
Size: 581563 Color: 1

Bin 5391: 418457 of cap free
Amount of items: 1
Items: 
Size: 581544 Color: 2

Bin 5392: 418488 of cap free
Amount of items: 1
Items: 
Size: 581513 Color: 4

Bin 5393: 418525 of cap free
Amount of items: 1
Items: 
Size: 581476 Color: 0

Bin 5394: 418554 of cap free
Amount of items: 1
Items: 
Size: 581447 Color: 3

Bin 5395: 418694 of cap free
Amount of items: 1
Items: 
Size: 581307 Color: 2

Bin 5396: 418702 of cap free
Amount of items: 1
Items: 
Size: 581299 Color: 4

Bin 5397: 418823 of cap free
Amount of items: 1
Items: 
Size: 581178 Color: 3

Bin 5398: 418906 of cap free
Amount of items: 1
Items: 
Size: 581095 Color: 2

Bin 5399: 418952 of cap free
Amount of items: 1
Items: 
Size: 581049 Color: 0

Bin 5400: 418977 of cap free
Amount of items: 1
Items: 
Size: 581024 Color: 4

Bin 5401: 419137 of cap free
Amount of items: 1
Items: 
Size: 580864 Color: 3

Bin 5402: 419166 of cap free
Amount of items: 1
Items: 
Size: 580835 Color: 1

Bin 5403: 419268 of cap free
Amount of items: 1
Items: 
Size: 580733 Color: 4

Bin 5404: 419358 of cap free
Amount of items: 1
Items: 
Size: 580643 Color: 1

Bin 5405: 419394 of cap free
Amount of items: 1
Items: 
Size: 580607 Color: 3

Bin 5406: 419445 of cap free
Amount of items: 1
Items: 
Size: 580556 Color: 0

Bin 5407: 419467 of cap free
Amount of items: 1
Items: 
Size: 580534 Color: 2

Bin 5408: 419573 of cap free
Amount of items: 1
Items: 
Size: 580428 Color: 0

Bin 5409: 419621 of cap free
Amount of items: 1
Items: 
Size: 580380 Color: 3

Bin 5410: 419770 of cap free
Amount of items: 1
Items: 
Size: 580231 Color: 4

Bin 5411: 419788 of cap free
Amount of items: 1
Items: 
Size: 580213 Color: 1

Bin 5412: 419986 of cap free
Amount of items: 1
Items: 
Size: 580015 Color: 1

Bin 5413: 420127 of cap free
Amount of items: 1
Items: 
Size: 579874 Color: 3

Bin 5414: 420200 of cap free
Amount of items: 1
Items: 
Size: 579801 Color: 4

Bin 5415: 420298 of cap free
Amount of items: 1
Items: 
Size: 579703 Color: 0

Bin 5416: 420339 of cap free
Amount of items: 1
Items: 
Size: 579662 Color: 2

Bin 5417: 420394 of cap free
Amount of items: 1
Items: 
Size: 579607 Color: 0

Bin 5418: 420395 of cap free
Amount of items: 1
Items: 
Size: 579606 Color: 1

Bin 5419: 420409 of cap free
Amount of items: 1
Items: 
Size: 579592 Color: 0

Bin 5420: 420433 of cap free
Amount of items: 1
Items: 
Size: 579568 Color: 3

Bin 5421: 420474 of cap free
Amount of items: 1
Items: 
Size: 579527 Color: 2

Bin 5422: 420585 of cap free
Amount of items: 1
Items: 
Size: 579416 Color: 0

Bin 5423: 420689 of cap free
Amount of items: 1
Items: 
Size: 579312 Color: 3

Bin 5424: 420763 of cap free
Amount of items: 1
Items: 
Size: 579238 Color: 3

Bin 5425: 420819 of cap free
Amount of items: 1
Items: 
Size: 579182 Color: 0

Bin 5426: 420844 of cap free
Amount of items: 1
Items: 
Size: 579157 Color: 3

Bin 5427: 420896 of cap free
Amount of items: 1
Items: 
Size: 579105 Color: 3

Bin 5428: 420965 of cap free
Amount of items: 1
Items: 
Size: 579036 Color: 2

Bin 5429: 420976 of cap free
Amount of items: 1
Items: 
Size: 579025 Color: 2

Bin 5430: 420994 of cap free
Amount of items: 1
Items: 
Size: 579007 Color: 1

Bin 5431: 421021 of cap free
Amount of items: 1
Items: 
Size: 578980 Color: 1

Bin 5432: 421081 of cap free
Amount of items: 1
Items: 
Size: 578920 Color: 3

Bin 5433: 421093 of cap free
Amount of items: 1
Items: 
Size: 578908 Color: 3

Bin 5434: 421169 of cap free
Amount of items: 1
Items: 
Size: 578832 Color: 1

Bin 5435: 421243 of cap free
Amount of items: 1
Items: 
Size: 578758 Color: 3

Bin 5436: 421362 of cap free
Amount of items: 1
Items: 
Size: 578639 Color: 0

Bin 5437: 421385 of cap free
Amount of items: 1
Items: 
Size: 578616 Color: 1

Bin 5438: 421420 of cap free
Amount of items: 1
Items: 
Size: 578581 Color: 1

Bin 5439: 421909 of cap free
Amount of items: 1
Items: 
Size: 578092 Color: 3

Bin 5440: 421933 of cap free
Amount of items: 1
Items: 
Size: 578068 Color: 2

Bin 5441: 422180 of cap free
Amount of items: 1
Items: 
Size: 577821 Color: 0

Bin 5442: 422368 of cap free
Amount of items: 1
Items: 
Size: 577633 Color: 4

Bin 5443: 422389 of cap free
Amount of items: 1
Items: 
Size: 577612 Color: 4

Bin 5444: 422630 of cap free
Amount of items: 1
Items: 
Size: 577371 Color: 4

Bin 5445: 422632 of cap free
Amount of items: 1
Items: 
Size: 577369 Color: 4

Bin 5446: 422652 of cap free
Amount of items: 1
Items: 
Size: 577349 Color: 4

Bin 5447: 422669 of cap free
Amount of items: 1
Items: 
Size: 577332 Color: 4

Bin 5448: 422873 of cap free
Amount of items: 1
Items: 
Size: 577128 Color: 4

Bin 5449: 422875 of cap free
Amount of items: 1
Items: 
Size: 577126 Color: 1

Bin 5450: 422882 of cap free
Amount of items: 1
Items: 
Size: 577119 Color: 4

Bin 5451: 422926 of cap free
Amount of items: 1
Items: 
Size: 577075 Color: 2

Bin 5452: 423555 of cap free
Amount of items: 1
Items: 
Size: 576446 Color: 4

Bin 5453: 423556 of cap free
Amount of items: 1
Items: 
Size: 576445 Color: 4

Bin 5454: 423606 of cap free
Amount of items: 1
Items: 
Size: 576395 Color: 1

Bin 5455: 423695 of cap free
Amount of items: 1
Items: 
Size: 576306 Color: 1

Bin 5456: 423782 of cap free
Amount of items: 1
Items: 
Size: 576219 Color: 0

Bin 5457: 423852 of cap free
Amount of items: 1
Items: 
Size: 576149 Color: 2

Bin 5458: 423871 of cap free
Amount of items: 1
Items: 
Size: 576130 Color: 1

Bin 5459: 423907 of cap free
Amount of items: 1
Items: 
Size: 576094 Color: 3

Bin 5460: 424043 of cap free
Amount of items: 1
Items: 
Size: 575958 Color: 4

Bin 5461: 424079 of cap free
Amount of items: 1
Items: 
Size: 575922 Color: 1

Bin 5462: 424082 of cap free
Amount of items: 1
Items: 
Size: 575919 Color: 0

Bin 5463: 424084 of cap free
Amount of items: 1
Items: 
Size: 575917 Color: 4

Bin 5464: 424466 of cap free
Amount of items: 1
Items: 
Size: 575535 Color: 4

Bin 5465: 424503 of cap free
Amount of items: 1
Items: 
Size: 575498 Color: 2

Bin 5466: 424508 of cap free
Amount of items: 1
Items: 
Size: 575493 Color: 1

Bin 5467: 424605 of cap free
Amount of items: 1
Items: 
Size: 575396 Color: 0

Bin 5468: 424685 of cap free
Amount of items: 1
Items: 
Size: 575316 Color: 2

Bin 5469: 424924 of cap free
Amount of items: 1
Items: 
Size: 575077 Color: 3

Bin 5470: 424947 of cap free
Amount of items: 1
Items: 
Size: 575054 Color: 3

Bin 5471: 424948 of cap free
Amount of items: 1
Items: 
Size: 575053 Color: 4

Bin 5472: 424959 of cap free
Amount of items: 1
Items: 
Size: 575042 Color: 1

Bin 5473: 424960 of cap free
Amount of items: 1
Items: 
Size: 575041 Color: 1

Bin 5474: 424966 of cap free
Amount of items: 1
Items: 
Size: 575035 Color: 2

Bin 5475: 424979 of cap free
Amount of items: 1
Items: 
Size: 575022 Color: 3

Bin 5476: 425134 of cap free
Amount of items: 1
Items: 
Size: 574867 Color: 3

Bin 5477: 425177 of cap free
Amount of items: 1
Items: 
Size: 574824 Color: 0

Bin 5478: 425219 of cap free
Amount of items: 1
Items: 
Size: 574782 Color: 4

Bin 5479: 425310 of cap free
Amount of items: 1
Items: 
Size: 574691 Color: 3

Bin 5480: 425345 of cap free
Amount of items: 1
Items: 
Size: 574656 Color: 1

Bin 5481: 425358 of cap free
Amount of items: 1
Items: 
Size: 574643 Color: 2

Bin 5482: 425515 of cap free
Amount of items: 1
Items: 
Size: 574486 Color: 1

Bin 5483: 425630 of cap free
Amount of items: 1
Items: 
Size: 574371 Color: 1

Bin 5484: 425662 of cap free
Amount of items: 1
Items: 
Size: 574339 Color: 3

Bin 5485: 425680 of cap free
Amount of items: 1
Items: 
Size: 574321 Color: 3

Bin 5486: 425724 of cap free
Amount of items: 1
Items: 
Size: 574277 Color: 2

Bin 5487: 425733 of cap free
Amount of items: 1
Items: 
Size: 574268 Color: 2

Bin 5488: 425778 of cap free
Amount of items: 1
Items: 
Size: 574223 Color: 0

Bin 5489: 425793 of cap free
Amount of items: 1
Items: 
Size: 574208 Color: 2

Bin 5490: 425793 of cap free
Amount of items: 1
Items: 
Size: 574208 Color: 4

Bin 5491: 425942 of cap free
Amount of items: 1
Items: 
Size: 574059 Color: 0

Bin 5492: 426067 of cap free
Amount of items: 1
Items: 
Size: 573934 Color: 4

Bin 5493: 426133 of cap free
Amount of items: 1
Items: 
Size: 573868 Color: 4

Bin 5494: 426149 of cap free
Amount of items: 1
Items: 
Size: 573852 Color: 3

Bin 5495: 426276 of cap free
Amount of items: 1
Items: 
Size: 573725 Color: 2

Bin 5496: 426437 of cap free
Amount of items: 1
Items: 
Size: 573564 Color: 0

Bin 5497: 426544 of cap free
Amount of items: 1
Items: 
Size: 573457 Color: 1

Bin 5498: 426636 of cap free
Amount of items: 1
Items: 
Size: 573365 Color: 4

Bin 5499: 426869 of cap free
Amount of items: 1
Items: 
Size: 573132 Color: 2

Bin 5500: 427087 of cap free
Amount of items: 1
Items: 
Size: 572914 Color: 2

Bin 5501: 427166 of cap free
Amount of items: 1
Items: 
Size: 572835 Color: 3

Bin 5502: 427289 of cap free
Amount of items: 1
Items: 
Size: 572712 Color: 1

Bin 5503: 427316 of cap free
Amount of items: 1
Items: 
Size: 572685 Color: 0

Bin 5504: 427451 of cap free
Amount of items: 1
Items: 
Size: 572550 Color: 2

Bin 5505: 427562 of cap free
Amount of items: 1
Items: 
Size: 572439 Color: 4

Bin 5506: 427716 of cap free
Amount of items: 1
Items: 
Size: 572285 Color: 4

Bin 5507: 427782 of cap free
Amount of items: 1
Items: 
Size: 572219 Color: 3

Bin 5508: 427783 of cap free
Amount of items: 1
Items: 
Size: 572218 Color: 1

Bin 5509: 427800 of cap free
Amount of items: 1
Items: 
Size: 572201 Color: 2

Bin 5510: 427827 of cap free
Amount of items: 1
Items: 
Size: 572174 Color: 1

Bin 5511: 427855 of cap free
Amount of items: 1
Items: 
Size: 572146 Color: 3

Bin 5512: 427951 of cap free
Amount of items: 1
Items: 
Size: 572050 Color: 3

Bin 5513: 427967 of cap free
Amount of items: 1
Items: 
Size: 572034 Color: 0

Bin 5514: 427986 of cap free
Amount of items: 1
Items: 
Size: 572015 Color: 4

Bin 5515: 428003 of cap free
Amount of items: 1
Items: 
Size: 571998 Color: 2

Bin 5516: 428128 of cap free
Amount of items: 1
Items: 
Size: 571873 Color: 3

Bin 5517: 428240 of cap free
Amount of items: 1
Items: 
Size: 571761 Color: 4

Bin 5518: 428504 of cap free
Amount of items: 1
Items: 
Size: 571497 Color: 3

Bin 5519: 428517 of cap free
Amount of items: 1
Items: 
Size: 571484 Color: 0

Bin 5520: 428578 of cap free
Amount of items: 1
Items: 
Size: 571423 Color: 1

Bin 5521: 428588 of cap free
Amount of items: 1
Items: 
Size: 571413 Color: 2

Bin 5522: 428737 of cap free
Amount of items: 1
Items: 
Size: 571264 Color: 2

Bin 5523: 428742 of cap free
Amount of items: 1
Items: 
Size: 571259 Color: 1

Bin 5524: 428893 of cap free
Amount of items: 1
Items: 
Size: 571108 Color: 4

Bin 5525: 429021 of cap free
Amount of items: 1
Items: 
Size: 570980 Color: 1

Bin 5526: 429113 of cap free
Amount of items: 1
Items: 
Size: 570888 Color: 0

Bin 5527: 429276 of cap free
Amount of items: 1
Items: 
Size: 570725 Color: 4

Bin 5528: 429284 of cap free
Amount of items: 1
Items: 
Size: 570717 Color: 1

Bin 5529: 429315 of cap free
Amount of items: 1
Items: 
Size: 570686 Color: 3

Bin 5530: 429434 of cap free
Amount of items: 1
Items: 
Size: 570567 Color: 4

Bin 5531: 429461 of cap free
Amount of items: 1
Items: 
Size: 570540 Color: 2

Bin 5532: 429526 of cap free
Amount of items: 1
Items: 
Size: 570475 Color: 2

Bin 5533: 429536 of cap free
Amount of items: 1
Items: 
Size: 570465 Color: 3

Bin 5534: 429557 of cap free
Amount of items: 1
Items: 
Size: 570444 Color: 3

Bin 5535: 429734 of cap free
Amount of items: 1
Items: 
Size: 570267 Color: 3

Bin 5536: 429845 of cap free
Amount of items: 1
Items: 
Size: 570156 Color: 0

Bin 5537: 429919 of cap free
Amount of items: 1
Items: 
Size: 570082 Color: 0

Bin 5538: 429944 of cap free
Amount of items: 1
Items: 
Size: 570057 Color: 3

Bin 5539: 430036 of cap free
Amount of items: 1
Items: 
Size: 569965 Color: 4

Bin 5540: 430043 of cap free
Amount of items: 1
Items: 
Size: 569958 Color: 3

Bin 5541: 430184 of cap free
Amount of items: 1
Items: 
Size: 569817 Color: 1

Bin 5542: 430259 of cap free
Amount of items: 1
Items: 
Size: 569742 Color: 0

Bin 5543: 430305 of cap free
Amount of items: 1
Items: 
Size: 569696 Color: 3

Bin 5544: 430327 of cap free
Amount of items: 1
Items: 
Size: 569674 Color: 4

Bin 5545: 430517 of cap free
Amount of items: 1
Items: 
Size: 569484 Color: 3

Bin 5546: 430542 of cap free
Amount of items: 1
Items: 
Size: 569459 Color: 4

Bin 5547: 430560 of cap free
Amount of items: 1
Items: 
Size: 569441 Color: 2

Bin 5548: 430647 of cap free
Amount of items: 1
Items: 
Size: 569354 Color: 2

Bin 5549: 430661 of cap free
Amount of items: 1
Items: 
Size: 569340 Color: 2

Bin 5550: 430678 of cap free
Amount of items: 1
Items: 
Size: 569323 Color: 2

Bin 5551: 430883 of cap free
Amount of items: 1
Items: 
Size: 569118 Color: 4

Bin 5552: 430942 of cap free
Amount of items: 1
Items: 
Size: 569059 Color: 3

Bin 5553: 431135 of cap free
Amount of items: 1
Items: 
Size: 568866 Color: 2

Bin 5554: 431144 of cap free
Amount of items: 1
Items: 
Size: 568857 Color: 2

Bin 5555: 431166 of cap free
Amount of items: 1
Items: 
Size: 568835 Color: 3

Bin 5556: 431179 of cap free
Amount of items: 1
Items: 
Size: 568822 Color: 1

Bin 5557: 431246 of cap free
Amount of items: 1
Items: 
Size: 568755 Color: 2

Bin 5558: 431258 of cap free
Amount of items: 1
Items: 
Size: 568743 Color: 1

Bin 5559: 431282 of cap free
Amount of items: 1
Items: 
Size: 568719 Color: 1

Bin 5560: 431295 of cap free
Amount of items: 1
Items: 
Size: 568706 Color: 3

Bin 5561: 431368 of cap free
Amount of items: 1
Items: 
Size: 568633 Color: 1

Bin 5562: 431387 of cap free
Amount of items: 1
Items: 
Size: 568614 Color: 2

Bin 5563: 431421 of cap free
Amount of items: 1
Items: 
Size: 568580 Color: 4

Bin 5564: 431452 of cap free
Amount of items: 1
Items: 
Size: 568549 Color: 4

Bin 5565: 431464 of cap free
Amount of items: 1
Items: 
Size: 568537 Color: 3

Bin 5566: 431559 of cap free
Amount of items: 1
Items: 
Size: 568442 Color: 0

Bin 5567: 431611 of cap free
Amount of items: 1
Items: 
Size: 568390 Color: 4

Bin 5568: 431670 of cap free
Amount of items: 1
Items: 
Size: 568331 Color: 0

Bin 5569: 431677 of cap free
Amount of items: 1
Items: 
Size: 568324 Color: 3

Bin 5570: 431698 of cap free
Amount of items: 1
Items: 
Size: 568303 Color: 4

Bin 5571: 431700 of cap free
Amount of items: 1
Items: 
Size: 568301 Color: 3

Bin 5572: 431812 of cap free
Amount of items: 1
Items: 
Size: 568189 Color: 1

Bin 5573: 431841 of cap free
Amount of items: 1
Items: 
Size: 568160 Color: 3

Bin 5574: 431939 of cap free
Amount of items: 1
Items: 
Size: 568062 Color: 1

Bin 5575: 431958 of cap free
Amount of items: 1
Items: 
Size: 568043 Color: 4

Bin 5576: 431966 of cap free
Amount of items: 1
Items: 
Size: 568035 Color: 2

Bin 5577: 432035 of cap free
Amount of items: 1
Items: 
Size: 567966 Color: 1

Bin 5578: 432094 of cap free
Amount of items: 1
Items: 
Size: 567907 Color: 3

Bin 5579: 432137 of cap free
Amount of items: 1
Items: 
Size: 567864 Color: 4

Bin 5580: 432139 of cap free
Amount of items: 1
Items: 
Size: 567862 Color: 4

Bin 5581: 432161 of cap free
Amount of items: 1
Items: 
Size: 567840 Color: 0

Bin 5582: 432169 of cap free
Amount of items: 1
Items: 
Size: 567832 Color: 0

Bin 5583: 432177 of cap free
Amount of items: 1
Items: 
Size: 567824 Color: 2

Bin 5584: 432338 of cap free
Amount of items: 1
Items: 
Size: 567663 Color: 3

Bin 5585: 432482 of cap free
Amount of items: 1
Items: 
Size: 567519 Color: 0

Bin 5586: 432508 of cap free
Amount of items: 1
Items: 
Size: 567493 Color: 3

Bin 5587: 432617 of cap free
Amount of items: 1
Items: 
Size: 567384 Color: 2

Bin 5588: 432638 of cap free
Amount of items: 1
Items: 
Size: 567363 Color: 2

Bin 5589: 432668 of cap free
Amount of items: 1
Items: 
Size: 567333 Color: 3

Bin 5590: 432703 of cap free
Amount of items: 1
Items: 
Size: 567298 Color: 4

Bin 5591: 432756 of cap free
Amount of items: 1
Items: 
Size: 567245 Color: 1

Bin 5592: 432765 of cap free
Amount of items: 1
Items: 
Size: 567236 Color: 4

Bin 5593: 432795 of cap free
Amount of items: 1
Items: 
Size: 567206 Color: 4

Bin 5594: 432949 of cap free
Amount of items: 1
Items: 
Size: 567052 Color: 4

Bin 5595: 432986 of cap free
Amount of items: 1
Items: 
Size: 567015 Color: 0

Bin 5596: 433074 of cap free
Amount of items: 1
Items: 
Size: 566927 Color: 3

Bin 5597: 433130 of cap free
Amount of items: 1
Items: 
Size: 566871 Color: 1

Bin 5598: 433210 of cap free
Amount of items: 1
Items: 
Size: 566791 Color: 3

Bin 5599: 433224 of cap free
Amount of items: 1
Items: 
Size: 566777 Color: 1

Bin 5600: 433372 of cap free
Amount of items: 1
Items: 
Size: 566629 Color: 3

Bin 5601: 433495 of cap free
Amount of items: 1
Items: 
Size: 566506 Color: 3

Bin 5602: 433723 of cap free
Amount of items: 1
Items: 
Size: 566278 Color: 2

Bin 5603: 433728 of cap free
Amount of items: 1
Items: 
Size: 566273 Color: 2

Bin 5604: 433765 of cap free
Amount of items: 1
Items: 
Size: 566236 Color: 4

Bin 5605: 433920 of cap free
Amount of items: 1
Items: 
Size: 566081 Color: 4

Bin 5606: 434117 of cap free
Amount of items: 1
Items: 
Size: 565884 Color: 0

Bin 5607: 434254 of cap free
Amount of items: 1
Items: 
Size: 565747 Color: 1

Bin 5608: 434287 of cap free
Amount of items: 1
Items: 
Size: 565714 Color: 4

Bin 5609: 434386 of cap free
Amount of items: 1
Items: 
Size: 565615 Color: 0

Bin 5610: 434414 of cap free
Amount of items: 1
Items: 
Size: 565587 Color: 3

Bin 5611: 434521 of cap free
Amount of items: 1
Items: 
Size: 565480 Color: 3

Bin 5612: 434603 of cap free
Amount of items: 1
Items: 
Size: 565398 Color: 1

Bin 5613: 434690 of cap free
Amount of items: 1
Items: 
Size: 565311 Color: 2

Bin 5614: 434780 of cap free
Amount of items: 1
Items: 
Size: 565221 Color: 0

Bin 5615: 434827 of cap free
Amount of items: 1
Items: 
Size: 565174 Color: 4

Bin 5616: 434868 of cap free
Amount of items: 1
Items: 
Size: 565133 Color: 1

Bin 5617: 435006 of cap free
Amount of items: 1
Items: 
Size: 564995 Color: 3

Bin 5618: 435140 of cap free
Amount of items: 1
Items: 
Size: 564861 Color: 1

Bin 5619: 435161 of cap free
Amount of items: 1
Items: 
Size: 564840 Color: 1

Bin 5620: 435281 of cap free
Amount of items: 1
Items: 
Size: 564720 Color: 1

Bin 5621: 435456 of cap free
Amount of items: 1
Items: 
Size: 564545 Color: 2

Bin 5622: 435675 of cap free
Amount of items: 1
Items: 
Size: 564326 Color: 1

Bin 5623: 435724 of cap free
Amount of items: 1
Items: 
Size: 564277 Color: 3

Bin 5624: 435729 of cap free
Amount of items: 1
Items: 
Size: 564272 Color: 0

Bin 5625: 435745 of cap free
Amount of items: 1
Items: 
Size: 564256 Color: 0

Bin 5626: 435857 of cap free
Amount of items: 1
Items: 
Size: 564144 Color: 0

Bin 5627: 435924 of cap free
Amount of items: 1
Items: 
Size: 564077 Color: 4

Bin 5628: 435940 of cap free
Amount of items: 1
Items: 
Size: 564061 Color: 4

Bin 5629: 436047 of cap free
Amount of items: 1
Items: 
Size: 563954 Color: 3

Bin 5630: 436081 of cap free
Amount of items: 1
Items: 
Size: 563920 Color: 3

Bin 5631: 436192 of cap free
Amount of items: 1
Items: 
Size: 563809 Color: 3

Bin 5632: 436553 of cap free
Amount of items: 1
Items: 
Size: 563448 Color: 3

Bin 5633: 436561 of cap free
Amount of items: 1
Items: 
Size: 563440 Color: 4

Bin 5634: 436690 of cap free
Amount of items: 1
Items: 
Size: 563311 Color: 4

Bin 5635: 436726 of cap free
Amount of items: 1
Items: 
Size: 563275 Color: 2

Bin 5636: 436802 of cap free
Amount of items: 1
Items: 
Size: 563199 Color: 1

Bin 5637: 436855 of cap free
Amount of items: 1
Items: 
Size: 563146 Color: 2

Bin 5638: 436927 of cap free
Amount of items: 1
Items: 
Size: 563074 Color: 3

Bin 5639: 436950 of cap free
Amount of items: 1
Items: 
Size: 563051 Color: 4

Bin 5640: 436986 of cap free
Amount of items: 1
Items: 
Size: 563015 Color: 1

Bin 5641: 437116 of cap free
Amount of items: 1
Items: 
Size: 562885 Color: 4

Bin 5642: 437216 of cap free
Amount of items: 1
Items: 
Size: 562785 Color: 1

Bin 5643: 437257 of cap free
Amount of items: 1
Items: 
Size: 562744 Color: 0

Bin 5644: 437292 of cap free
Amount of items: 1
Items: 
Size: 562709 Color: 1

Bin 5645: 437331 of cap free
Amount of items: 1
Items: 
Size: 562670 Color: 3

Bin 5646: 437351 of cap free
Amount of items: 1
Items: 
Size: 562650 Color: 2

Bin 5647: 437393 of cap free
Amount of items: 1
Items: 
Size: 562608 Color: 1

Bin 5648: 437597 of cap free
Amount of items: 1
Items: 
Size: 562404 Color: 0

Bin 5649: 437618 of cap free
Amount of items: 1
Items: 
Size: 562383 Color: 1

Bin 5650: 437693 of cap free
Amount of items: 1
Items: 
Size: 562308 Color: 1

Bin 5651: 437696 of cap free
Amount of items: 1
Items: 
Size: 562305 Color: 4

Bin 5652: 438034 of cap free
Amount of items: 1
Items: 
Size: 561967 Color: 1

Bin 5653: 438538 of cap free
Amount of items: 1
Items: 
Size: 561463 Color: 4

Bin 5654: 438540 of cap free
Amount of items: 1
Items: 
Size: 561461 Color: 3

Bin 5655: 438553 of cap free
Amount of items: 1
Items: 
Size: 561448 Color: 0

Bin 5656: 438671 of cap free
Amount of items: 1
Items: 
Size: 561330 Color: 4

Bin 5657: 438809 of cap free
Amount of items: 1
Items: 
Size: 561192 Color: 2

Bin 5658: 438883 of cap free
Amount of items: 1
Items: 
Size: 561118 Color: 2

Bin 5659: 439077 of cap free
Amount of items: 1
Items: 
Size: 560924 Color: 1

Bin 5660: 439172 of cap free
Amount of items: 1
Items: 
Size: 560829 Color: 0

Bin 5661: 439225 of cap free
Amount of items: 1
Items: 
Size: 560776 Color: 2

Bin 5662: 439428 of cap free
Amount of items: 1
Items: 
Size: 560573 Color: 4

Bin 5663: 439452 of cap free
Amount of items: 1
Items: 
Size: 560549 Color: 2

Bin 5664: 439596 of cap free
Amount of items: 1
Items: 
Size: 560405 Color: 4

Bin 5665: 439620 of cap free
Amount of items: 1
Items: 
Size: 560381 Color: 0

Bin 5666: 439629 of cap free
Amount of items: 1
Items: 
Size: 560372 Color: 4

Bin 5667: 439647 of cap free
Amount of items: 1
Items: 
Size: 560354 Color: 1

Bin 5668: 439779 of cap free
Amount of items: 1
Items: 
Size: 560222 Color: 1

Bin 5669: 439805 of cap free
Amount of items: 1
Items: 
Size: 560196 Color: 4

Bin 5670: 439848 of cap free
Amount of items: 1
Items: 
Size: 560153 Color: 1

Bin 5671: 439867 of cap free
Amount of items: 1
Items: 
Size: 560134 Color: 0

Bin 5672: 439983 of cap free
Amount of items: 1
Items: 
Size: 560018 Color: 3

Bin 5673: 440033 of cap free
Amount of items: 1
Items: 
Size: 559968 Color: 3

Bin 5674: 440125 of cap free
Amount of items: 1
Items: 
Size: 559876 Color: 1

Bin 5675: 440141 of cap free
Amount of items: 1
Items: 
Size: 559860 Color: 3

Bin 5676: 440205 of cap free
Amount of items: 1
Items: 
Size: 559796 Color: 3

Bin 5677: 440648 of cap free
Amount of items: 1
Items: 
Size: 559353 Color: 0

Bin 5678: 440731 of cap free
Amount of items: 1
Items: 
Size: 559270 Color: 0

Bin 5679: 440738 of cap free
Amount of items: 1
Items: 
Size: 559263 Color: 0

Bin 5680: 440740 of cap free
Amount of items: 1
Items: 
Size: 559261 Color: 3

Bin 5681: 440755 of cap free
Amount of items: 1
Items: 
Size: 559246 Color: 3

Bin 5682: 440782 of cap free
Amount of items: 1
Items: 
Size: 559219 Color: 4

Bin 5683: 440793 of cap free
Amount of items: 1
Items: 
Size: 559208 Color: 4

Bin 5684: 440823 of cap free
Amount of items: 1
Items: 
Size: 559178 Color: 0

Bin 5685: 440829 of cap free
Amount of items: 1
Items: 
Size: 559172 Color: 3

Bin 5686: 440837 of cap free
Amount of items: 1
Items: 
Size: 559164 Color: 3

Bin 5687: 440855 of cap free
Amount of items: 1
Items: 
Size: 559146 Color: 2

Bin 5688: 440871 of cap free
Amount of items: 1
Items: 
Size: 559130 Color: 4

Bin 5689: 440914 of cap free
Amount of items: 1
Items: 
Size: 559087 Color: 4

Bin 5690: 440925 of cap free
Amount of items: 1
Items: 
Size: 559076 Color: 4

Bin 5691: 441004 of cap free
Amount of items: 1
Items: 
Size: 558997 Color: 4

Bin 5692: 441128 of cap free
Amount of items: 1
Items: 
Size: 558873 Color: 3

Bin 5693: 441358 of cap free
Amount of items: 1
Items: 
Size: 558643 Color: 2

Bin 5694: 441441 of cap free
Amount of items: 1
Items: 
Size: 558560 Color: 1

Bin 5695: 441445 of cap free
Amount of items: 1
Items: 
Size: 558556 Color: 4

Bin 5696: 441484 of cap free
Amount of items: 1
Items: 
Size: 558517 Color: 3

Bin 5697: 441500 of cap free
Amount of items: 1
Items: 
Size: 558501 Color: 2

Bin 5698: 441561 of cap free
Amount of items: 1
Items: 
Size: 558440 Color: 3

Bin 5699: 441572 of cap free
Amount of items: 1
Items: 
Size: 558429 Color: 0

Bin 5700: 441593 of cap free
Amount of items: 1
Items: 
Size: 558408 Color: 2

Bin 5701: 441668 of cap free
Amount of items: 1
Items: 
Size: 558333 Color: 3

Bin 5702: 441728 of cap free
Amount of items: 1
Items: 
Size: 558273 Color: 1

Bin 5703: 441734 of cap free
Amount of items: 1
Items: 
Size: 558267 Color: 2

Bin 5704: 604444 of cap free
Amount of items: 1
Items: 
Size: 395557 Color: 0

Total size: 4532441801
Total free space: 1171563903

