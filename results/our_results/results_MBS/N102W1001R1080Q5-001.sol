Capicity Bin: 1001
Lower Bound: 44

Bins used: 45
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 488 Color: 4

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 455 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 0
Size: 197 Color: 4
Size: 156 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 1
Size: 162 Color: 3
Size: 148 Color: 4

Bin 6: 1 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 323 Color: 1
Size: 238 Color: 1

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 1
Size: 487 Color: 3

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 476 Color: 2

Bin 9: 1 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 468 Color: 2

Bin 10: 1 of cap free
Amount of items: 3
Items: 
Size: 647 Color: 0
Size: 182 Color: 2
Size: 171 Color: 0

Bin 11: 1 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 202 Color: 4

Bin 12: 2 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 472 Color: 0

Bin 13: 2 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 0
Size: 192 Color: 4
Size: 184 Color: 3

Bin 14: 2 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 336 Color: 1

Bin 15: 2 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 3
Size: 293 Color: 4

Bin 16: 2 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 235 Color: 1

Bin 17: 2 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 1
Size: 117 Color: 0
Size: 114 Color: 1

Bin 18: 3 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 1
Size: 194 Color: 0
Size: 193 Color: 0

Bin 19: 3 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 311 Color: 1

Bin 20: 3 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 2
Size: 141 Color: 0
Size: 122 Color: 1

Bin 21: 3 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 240 Color: 4

Bin 22: 3 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 2
Size: 214 Color: 1

Bin 23: 4 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 3
Size: 467 Color: 0

Bin 24: 4 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 4
Size: 212 Color: 1
Size: 159 Color: 2

Bin 25: 5 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 3
Size: 139 Color: 1
Size: 138 Color: 3

Bin 26: 6 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 1
Size: 308 Color: 4

Bin 27: 6 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 2
Size: 149 Color: 4
Size: 136 Color: 4

Bin 28: 9 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 485 Color: 1

Bin 29: 9 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 225 Color: 2

Bin 30: 11 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 386 Color: 1

Bin 31: 14 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 252 Color: 0

Bin 32: 16 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 442 Color: 0

Bin 33: 17 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 250 Color: 3

Bin 34: 19 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 4
Size: 274 Color: 2
Size: 272 Color: 3

Bin 35: 19 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 283 Color: 3

Bin 36: 24 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 2
Size: 316 Color: 0
Size: 315 Color: 2

Bin 37: 26 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 348 Color: 4
Size: 272 Color: 3

Bin 38: 26 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 435 Color: 0

Bin 39: 26 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 1
Size: 222 Color: 0

Bin 40: 28 of cap free
Amount of items: 2
Items: 
Size: 496 Color: 4
Size: 477 Color: 1

Bin 41: 55 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 3
Size: 363 Color: 0

Bin 42: 58 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 359 Color: 4

Bin 43: 156 of cap free
Amount of items: 2
Items: 
Size: 425 Color: 2
Size: 420 Color: 4

Bin 44: 658 of cap free
Amount of items: 1
Items: 
Size: 343 Color: 2

Bin 45: 669 of cap free
Amount of items: 1
Items: 
Size: 332 Color: 2

Total size: 43147
Total free space: 1898

