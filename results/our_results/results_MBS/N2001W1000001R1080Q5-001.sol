Capicity Bin: 1000001
Lower Bound: 906

Bins used: 917
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 515163 Color: 1
Size: 484838 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 700658 Color: 4
Size: 151102 Color: 3
Size: 148241 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 731561 Color: 2
Size: 134838 Color: 3
Size: 133602 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 775911 Color: 1
Size: 113198 Color: 0
Size: 110892 Color: 4

Bin 5: 1 of cap free
Amount of items: 2
Items: 
Size: 696545 Color: 1
Size: 303455 Color: 3

Bin 6: 1 of cap free
Amount of items: 3
Items: 
Size: 727942 Color: 4
Size: 136035 Color: 3
Size: 136023 Color: 2

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 759872 Color: 4
Size: 240128 Color: 0

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 776338 Color: 2
Size: 223662 Color: 3

Bin 9: 4 of cap free
Amount of items: 3
Items: 
Size: 744716 Color: 2
Size: 127898 Color: 3
Size: 127383 Color: 4

Bin 10: 5 of cap free
Amount of items: 2
Items: 
Size: 560654 Color: 3
Size: 439342 Color: 0

Bin 11: 5 of cap free
Amount of items: 2
Items: 
Size: 745354 Color: 3
Size: 254642 Color: 4

Bin 12: 5 of cap free
Amount of items: 2
Items: 
Size: 751826 Color: 1
Size: 248170 Color: 2

Bin 13: 5 of cap free
Amount of items: 2
Items: 
Size: 762309 Color: 1
Size: 237687 Color: 4

Bin 14: 5 of cap free
Amount of items: 3
Items: 
Size: 775780 Color: 0
Size: 112572 Color: 1
Size: 111644 Color: 3

Bin 15: 7 of cap free
Amount of items: 2
Items: 
Size: 682961 Color: 3
Size: 317033 Color: 2

Bin 16: 7 of cap free
Amount of items: 2
Items: 
Size: 721266 Color: 0
Size: 278728 Color: 2

Bin 17: 7 of cap free
Amount of items: 2
Items: 
Size: 747256 Color: 2
Size: 252738 Color: 4

Bin 18: 7 of cap free
Amount of items: 3
Items: 
Size: 774801 Color: 2
Size: 113335 Color: 1
Size: 111858 Color: 2

Bin 19: 8 of cap free
Amount of items: 2
Items: 
Size: 665426 Color: 3
Size: 334567 Color: 1

Bin 20: 8 of cap free
Amount of items: 3
Items: 
Size: 677419 Color: 1
Size: 162111 Color: 3
Size: 160463 Color: 0

Bin 21: 8 of cap free
Amount of items: 2
Items: 
Size: 678517 Color: 0
Size: 321476 Color: 1

Bin 22: 8 of cap free
Amount of items: 2
Items: 
Size: 763956 Color: 4
Size: 236037 Color: 2

Bin 23: 8 of cap free
Amount of items: 3
Items: 
Size: 796492 Color: 3
Size: 101835 Color: 1
Size: 101666 Color: 0

Bin 24: 9 of cap free
Amount of items: 2
Items: 
Size: 757403 Color: 2
Size: 242589 Color: 1

Bin 25: 10 of cap free
Amount of items: 2
Items: 
Size: 526352 Color: 1
Size: 473639 Color: 2

Bin 26: 10 of cap free
Amount of items: 2
Items: 
Size: 741210 Color: 1
Size: 258781 Color: 0

Bin 27: 10 of cap free
Amount of items: 2
Items: 
Size: 767549 Color: 2
Size: 232442 Color: 3

Bin 28: 11 of cap free
Amount of items: 2
Items: 
Size: 658169 Color: 2
Size: 341821 Color: 4

Bin 29: 11 of cap free
Amount of items: 3
Items: 
Size: 697430 Color: 0
Size: 151729 Color: 4
Size: 150831 Color: 2

Bin 30: 11 of cap free
Amount of items: 2
Items: 
Size: 751648 Color: 3
Size: 248342 Color: 2

Bin 31: 12 of cap free
Amount of items: 2
Items: 
Size: 578416 Color: 3
Size: 421573 Color: 0

Bin 32: 12 of cap free
Amount of items: 2
Items: 
Size: 702042 Color: 0
Size: 297947 Color: 2

Bin 33: 13 of cap free
Amount of items: 3
Items: 
Size: 685825 Color: 1
Size: 157566 Color: 3
Size: 156597 Color: 4

Bin 34: 13 of cap free
Amount of items: 2
Items: 
Size: 737271 Color: 2
Size: 262717 Color: 3

Bin 35: 14 of cap free
Amount of items: 2
Items: 
Size: 501471 Color: 3
Size: 498516 Color: 0

Bin 36: 14 of cap free
Amount of items: 3
Items: 
Size: 535650 Color: 0
Size: 232173 Color: 3
Size: 232164 Color: 2

Bin 37: 14 of cap free
Amount of items: 3
Items: 
Size: 545701 Color: 4
Size: 229012 Color: 1
Size: 225274 Color: 0

Bin 38: 14 of cap free
Amount of items: 3
Items: 
Size: 681251 Color: 1
Size: 160044 Color: 3
Size: 158692 Color: 2

Bin 39: 15 of cap free
Amount of items: 3
Items: 
Size: 577637 Color: 4
Size: 212778 Color: 3
Size: 209571 Color: 1

Bin 40: 15 of cap free
Amount of items: 2
Items: 
Size: 622574 Color: 1
Size: 377412 Color: 3

Bin 41: 15 of cap free
Amount of items: 3
Items: 
Size: 632149 Color: 1
Size: 184330 Color: 0
Size: 183507 Color: 3

Bin 42: 16 of cap free
Amount of items: 2
Items: 
Size: 726571 Color: 0
Size: 273414 Color: 2

Bin 43: 18 of cap free
Amount of items: 2
Items: 
Size: 563946 Color: 3
Size: 436037 Color: 4

Bin 44: 18 of cap free
Amount of items: 2
Items: 
Size: 677563 Color: 3
Size: 322420 Color: 4

Bin 45: 19 of cap free
Amount of items: 2
Items: 
Size: 765325 Color: 4
Size: 234657 Color: 1

Bin 46: 20 of cap free
Amount of items: 2
Items: 
Size: 574220 Color: 1
Size: 425761 Color: 0

Bin 47: 20 of cap free
Amount of items: 2
Items: 
Size: 579829 Color: 4
Size: 420152 Color: 2

Bin 48: 20 of cap free
Amount of items: 2
Items: 
Size: 728671 Color: 4
Size: 271310 Color: 2

Bin 49: 20 of cap free
Amount of items: 2
Items: 
Size: 741353 Color: 2
Size: 258628 Color: 1

Bin 50: 20 of cap free
Amount of items: 3
Items: 
Size: 773867 Color: 2
Size: 113114 Color: 4
Size: 113000 Color: 3

Bin 51: 21 of cap free
Amount of items: 3
Items: 
Size: 696581 Color: 4
Size: 152212 Color: 2
Size: 151187 Color: 0

Bin 52: 21 of cap free
Amount of items: 2
Items: 
Size: 750997 Color: 0
Size: 248983 Color: 4

Bin 53: 21 of cap free
Amount of items: 3
Items: 
Size: 768111 Color: 1
Size: 117261 Color: 3
Size: 114608 Color: 4

Bin 54: 22 of cap free
Amount of items: 2
Items: 
Size: 685172 Color: 1
Size: 314807 Color: 3

Bin 55: 22 of cap free
Amount of items: 3
Items: 
Size: 694449 Color: 4
Size: 152904 Color: 1
Size: 152626 Color: 4

Bin 56: 23 of cap free
Amount of items: 2
Items: 
Size: 556472 Color: 1
Size: 443506 Color: 4

Bin 57: 23 of cap free
Amount of items: 2
Items: 
Size: 755308 Color: 0
Size: 244670 Color: 1

Bin 58: 24 of cap free
Amount of items: 2
Items: 
Size: 581833 Color: 3
Size: 418144 Color: 4

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 692425 Color: 0
Size: 307552 Color: 4

Bin 60: 24 of cap free
Amount of items: 3
Items: 
Size: 708706 Color: 0
Size: 146226 Color: 1
Size: 145045 Color: 0

Bin 61: 24 of cap free
Amount of items: 2
Items: 
Size: 729192 Color: 1
Size: 270785 Color: 4

Bin 62: 24 of cap free
Amount of items: 2
Items: 
Size: 762403 Color: 2
Size: 237574 Color: 3

Bin 63: 24 of cap free
Amount of items: 2
Items: 
Size: 777331 Color: 1
Size: 222646 Color: 3

Bin 64: 24 of cap free
Amount of items: 3
Items: 
Size: 781521 Color: 1
Size: 109356 Color: 4
Size: 109100 Color: 2

Bin 65: 25 of cap free
Amount of items: 2
Items: 
Size: 565682 Color: 1
Size: 434294 Color: 4

Bin 66: 26 of cap free
Amount of items: 3
Items: 
Size: 563508 Color: 0
Size: 220106 Color: 3
Size: 216361 Color: 0

Bin 67: 26 of cap free
Amount of items: 3
Items: 
Size: 632996 Color: 0
Size: 183975 Color: 1
Size: 183004 Color: 0

Bin 68: 27 of cap free
Amount of items: 2
Items: 
Size: 560220 Color: 1
Size: 439754 Color: 2

Bin 69: 27 of cap free
Amount of items: 3
Items: 
Size: 708911 Color: 1
Size: 146171 Color: 3
Size: 144892 Color: 0

Bin 70: 27 of cap free
Amount of items: 3
Items: 
Size: 765959 Color: 1
Size: 119640 Color: 0
Size: 114375 Color: 0

Bin 71: 28 of cap free
Amount of items: 3
Items: 
Size: 609954 Color: 4
Size: 195793 Color: 0
Size: 194226 Color: 4

Bin 72: 28 of cap free
Amount of items: 3
Items: 
Size: 644515 Color: 1
Size: 179165 Color: 0
Size: 176293 Color: 0

Bin 73: 28 of cap free
Amount of items: 2
Items: 
Size: 782058 Color: 3
Size: 217915 Color: 1

Bin 74: 28 of cap free
Amount of items: 2
Items: 
Size: 787994 Color: 0
Size: 211979 Color: 4

Bin 75: 29 of cap free
Amount of items: 2
Items: 
Size: 654610 Color: 4
Size: 345362 Color: 2

Bin 76: 30 of cap free
Amount of items: 3
Items: 
Size: 574776 Color: 3
Size: 212772 Color: 0
Size: 212423 Color: 1

Bin 77: 31 of cap free
Amount of items: 3
Items: 
Size: 671670 Color: 0
Size: 164471 Color: 3
Size: 163829 Color: 1

Bin 78: 31 of cap free
Amount of items: 3
Items: 
Size: 756096 Color: 3
Size: 122240 Color: 2
Size: 121634 Color: 0

Bin 79: 31 of cap free
Amount of items: 3
Items: 
Size: 782595 Color: 1
Size: 109019 Color: 4
Size: 108356 Color: 4

Bin 80: 32 of cap free
Amount of items: 3
Items: 
Size: 556135 Color: 3
Size: 222937 Color: 2
Size: 220897 Color: 3

Bin 81: 33 of cap free
Amount of items: 3
Items: 
Size: 630509 Color: 2
Size: 186401 Color: 3
Size: 183058 Color: 1

Bin 82: 33 of cap free
Amount of items: 3
Items: 
Size: 718952 Color: 1
Size: 140989 Color: 3
Size: 140027 Color: 3

Bin 83: 33 of cap free
Amount of items: 2
Items: 
Size: 762516 Color: 2
Size: 237452 Color: 1

Bin 84: 34 of cap free
Amount of items: 2
Items: 
Size: 761777 Color: 2
Size: 238190 Color: 1

Bin 85: 35 of cap free
Amount of items: 2
Items: 
Size: 522226 Color: 3
Size: 477740 Color: 0

Bin 86: 35 of cap free
Amount of items: 3
Items: 
Size: 606794 Color: 3
Size: 197031 Color: 1
Size: 196141 Color: 2

Bin 87: 35 of cap free
Amount of items: 2
Items: 
Size: 766579 Color: 2
Size: 233387 Color: 3

Bin 88: 35 of cap free
Amount of items: 3
Items: 
Size: 771079 Color: 3
Size: 114494 Color: 1
Size: 114393 Color: 1

Bin 89: 36 of cap free
Amount of items: 3
Items: 
Size: 710107 Color: 1
Size: 145929 Color: 3
Size: 143929 Color: 2

Bin 90: 36 of cap free
Amount of items: 3
Items: 
Size: 755997 Color: 4
Size: 122098 Color: 0
Size: 121870 Color: 1

Bin 91: 37 of cap free
Amount of items: 3
Items: 
Size: 601401 Color: 0
Size: 201110 Color: 4
Size: 197453 Color: 0

Bin 92: 37 of cap free
Amount of items: 3
Items: 
Size: 618453 Color: 3
Size: 191257 Color: 1
Size: 190254 Color: 4

Bin 93: 37 of cap free
Amount of items: 3
Items: 
Size: 628702 Color: 3
Size: 186981 Color: 2
Size: 184281 Color: 3

Bin 94: 37 of cap free
Amount of items: 3
Items: 
Size: 628709 Color: 2
Size: 187854 Color: 1
Size: 183401 Color: 3

Bin 95: 38 of cap free
Amount of items: 2
Items: 
Size: 538049 Color: 1
Size: 461914 Color: 4

Bin 96: 38 of cap free
Amount of items: 2
Items: 
Size: 563743 Color: 4
Size: 436220 Color: 0

Bin 97: 39 of cap free
Amount of items: 3
Items: 
Size: 531941 Color: 1
Size: 234919 Color: 0
Size: 233102 Color: 0

Bin 98: 39 of cap free
Amount of items: 3
Items: 
Size: 636323 Color: 2
Size: 182167 Color: 0
Size: 181472 Color: 1

Bin 99: 39 of cap free
Amount of items: 3
Items: 
Size: 670980 Color: 0
Size: 165316 Color: 1
Size: 163666 Color: 1

Bin 100: 39 of cap free
Amount of items: 3
Items: 
Size: 737172 Color: 0
Size: 131630 Color: 1
Size: 131160 Color: 3

Bin 101: 40 of cap free
Amount of items: 2
Items: 
Size: 508556 Color: 3
Size: 491405 Color: 0

Bin 102: 40 of cap free
Amount of items: 3
Items: 
Size: 537278 Color: 2
Size: 231482 Color: 0
Size: 231201 Color: 0

Bin 103: 40 of cap free
Amount of items: 2
Items: 
Size: 604152 Color: 2
Size: 395809 Color: 4

Bin 104: 40 of cap free
Amount of items: 3
Items: 
Size: 775513 Color: 1
Size: 112555 Color: 4
Size: 111893 Color: 1

Bin 105: 40 of cap free
Amount of items: 3
Items: 
Size: 798707 Color: 4
Size: 100858 Color: 2
Size: 100396 Color: 0

Bin 106: 41 of cap free
Amount of items: 2
Items: 
Size: 568628 Color: 2
Size: 431332 Color: 3

Bin 107: 41 of cap free
Amount of items: 3
Items: 
Size: 664667 Color: 3
Size: 167924 Color: 1
Size: 167369 Color: 0

Bin 108: 41 of cap free
Amount of items: 2
Items: 
Size: 729502 Color: 0
Size: 270458 Color: 2

Bin 109: 42 of cap free
Amount of items: 2
Items: 
Size: 519314 Color: 3
Size: 480645 Color: 1

Bin 110: 42 of cap free
Amount of items: 2
Items: 
Size: 568312 Color: 4
Size: 431647 Color: 3

Bin 111: 42 of cap free
Amount of items: 2
Items: 
Size: 592622 Color: 4
Size: 407337 Color: 3

Bin 112: 42 of cap free
Amount of items: 2
Items: 
Size: 648816 Color: 3
Size: 351143 Color: 4

Bin 113: 43 of cap free
Amount of items: 2
Items: 
Size: 538900 Color: 4
Size: 461058 Color: 0

Bin 114: 43 of cap free
Amount of items: 2
Items: 
Size: 602524 Color: 0
Size: 397434 Color: 3

Bin 115: 43 of cap free
Amount of items: 3
Items: 
Size: 766386 Color: 2
Size: 117064 Color: 3
Size: 116508 Color: 3

Bin 116: 44 of cap free
Amount of items: 3
Items: 
Size: 656204 Color: 2
Size: 172110 Color: 0
Size: 171643 Color: 3

Bin 117: 44 of cap free
Amount of items: 2
Items: 
Size: 656669 Color: 1
Size: 343288 Color: 2

Bin 118: 45 of cap free
Amount of items: 2
Items: 
Size: 746458 Color: 0
Size: 253498 Color: 3

Bin 119: 45 of cap free
Amount of items: 3
Items: 
Size: 749522 Color: 2
Size: 125256 Color: 3
Size: 125178 Color: 4

Bin 120: 46 of cap free
Amount of items: 2
Items: 
Size: 664939 Color: 2
Size: 335016 Color: 1

Bin 121: 46 of cap free
Amount of items: 3
Items: 
Size: 693523 Color: 1
Size: 153468 Color: 2
Size: 152964 Color: 3

Bin 122: 46 of cap free
Amount of items: 3
Items: 
Size: 729116 Color: 4
Size: 135636 Color: 1
Size: 135203 Color: 2

Bin 123: 47 of cap free
Amount of items: 2
Items: 
Size: 728372 Color: 1
Size: 271582 Color: 3

Bin 124: 48 of cap free
Amount of items: 2
Items: 
Size: 735892 Color: 1
Size: 264061 Color: 0

Bin 125: 49 of cap free
Amount of items: 3
Items: 
Size: 529761 Color: 2
Size: 235233 Color: 3
Size: 234958 Color: 1

Bin 126: 49 of cap free
Amount of items: 2
Items: 
Size: 722111 Color: 1
Size: 277841 Color: 4

Bin 127: 49 of cap free
Amount of items: 2
Items: 
Size: 746810 Color: 4
Size: 253142 Color: 1

Bin 128: 51 of cap free
Amount of items: 3
Items: 
Size: 550107 Color: 4
Size: 225130 Color: 1
Size: 224713 Color: 1

Bin 129: 51 of cap free
Amount of items: 2
Items: 
Size: 575974 Color: 1
Size: 423976 Color: 0

Bin 130: 51 of cap free
Amount of items: 2
Items: 
Size: 587739 Color: 0
Size: 412211 Color: 3

Bin 131: 51 of cap free
Amount of items: 3
Items: 
Size: 770588 Color: 0
Size: 114723 Color: 2
Size: 114639 Color: 2

Bin 132: 52 of cap free
Amount of items: 2
Items: 
Size: 549220 Color: 1
Size: 450729 Color: 4

Bin 133: 52 of cap free
Amount of items: 2
Items: 
Size: 552104 Color: 0
Size: 447845 Color: 1

Bin 134: 52 of cap free
Amount of items: 2
Items: 
Size: 621687 Color: 3
Size: 378262 Color: 4

Bin 135: 52 of cap free
Amount of items: 3
Items: 
Size: 631698 Color: 4
Size: 185187 Color: 3
Size: 183064 Color: 3

Bin 136: 52 of cap free
Amount of items: 2
Items: 
Size: 669621 Color: 4
Size: 330328 Color: 3

Bin 137: 53 of cap free
Amount of items: 3
Items: 
Size: 617770 Color: 1
Size: 191243 Color: 3
Size: 190935 Color: 4

Bin 138: 53 of cap free
Amount of items: 2
Items: 
Size: 781736 Color: 3
Size: 218212 Color: 4

Bin 139: 54 of cap free
Amount of items: 3
Items: 
Size: 619915 Color: 1
Size: 190416 Color: 2
Size: 189616 Color: 1

Bin 140: 54 of cap free
Amount of items: 2
Items: 
Size: 626501 Color: 2
Size: 373446 Color: 0

Bin 141: 54 of cap free
Amount of items: 2
Items: 
Size: 758504 Color: 4
Size: 241443 Color: 0

Bin 142: 55 of cap free
Amount of items: 2
Items: 
Size: 602057 Color: 2
Size: 397889 Color: 0

Bin 143: 55 of cap free
Amount of items: 2
Items: 
Size: 608688 Color: 2
Size: 391258 Color: 4

Bin 144: 55 of cap free
Amount of items: 2
Items: 
Size: 708251 Color: 0
Size: 291695 Color: 4

Bin 145: 55 of cap free
Amount of items: 3
Items: 
Size: 713878 Color: 2
Size: 143035 Color: 1
Size: 143033 Color: 1

Bin 146: 57 of cap free
Amount of items: 2
Items: 
Size: 560931 Color: 2
Size: 439013 Color: 1

Bin 147: 57 of cap free
Amount of items: 2
Items: 
Size: 612001 Color: 4
Size: 387943 Color: 0

Bin 148: 58 of cap free
Amount of items: 2
Items: 
Size: 515435 Color: 1
Size: 484508 Color: 4

Bin 149: 58 of cap free
Amount of items: 3
Items: 
Size: 644816 Color: 0
Size: 178987 Color: 2
Size: 176140 Color: 4

Bin 150: 58 of cap free
Amount of items: 2
Items: 
Size: 727501 Color: 1
Size: 272442 Color: 3

Bin 151: 59 of cap free
Amount of items: 2
Items: 
Size: 550336 Color: 0
Size: 449606 Color: 2

Bin 152: 59 of cap free
Amount of items: 2
Items: 
Size: 702687 Color: 1
Size: 297255 Color: 2

Bin 153: 59 of cap free
Amount of items: 2
Items: 
Size: 720865 Color: 0
Size: 279077 Color: 2

Bin 154: 59 of cap free
Amount of items: 2
Items: 
Size: 758654 Color: 0
Size: 241288 Color: 2

Bin 155: 60 of cap free
Amount of items: 3
Items: 
Size: 573367 Color: 0
Size: 213948 Color: 1
Size: 212626 Color: 3

Bin 156: 61 of cap free
Amount of items: 2
Items: 
Size: 502538 Color: 2
Size: 497402 Color: 0

Bin 157: 61 of cap free
Amount of items: 2
Items: 
Size: 744911 Color: 2
Size: 255029 Color: 3

Bin 158: 62 of cap free
Amount of items: 2
Items: 
Size: 592416 Color: 3
Size: 407523 Color: 0

Bin 159: 62 of cap free
Amount of items: 2
Items: 
Size: 695714 Color: 2
Size: 304225 Color: 3

Bin 160: 62 of cap free
Amount of items: 2
Items: 
Size: 793120 Color: 4
Size: 206819 Color: 0

Bin 161: 63 of cap free
Amount of items: 3
Items: 
Size: 629516 Color: 1
Size: 186738 Color: 4
Size: 183684 Color: 4

Bin 162: 64 of cap free
Amount of items: 3
Items: 
Size: 732247 Color: 2
Size: 134381 Color: 4
Size: 133309 Color: 4

Bin 163: 65 of cap free
Amount of items: 2
Items: 
Size: 637469 Color: 2
Size: 362467 Color: 4

Bin 164: 66 of cap free
Amount of items: 2
Items: 
Size: 501659 Color: 3
Size: 498276 Color: 4

Bin 165: 66 of cap free
Amount of items: 2
Items: 
Size: 605572 Color: 4
Size: 394363 Color: 3

Bin 166: 66 of cap free
Amount of items: 3
Items: 
Size: 609227 Color: 1
Size: 196327 Color: 4
Size: 194381 Color: 4

Bin 167: 66 of cap free
Amount of items: 2
Items: 
Size: 690491 Color: 3
Size: 309444 Color: 2

Bin 168: 66 of cap free
Amount of items: 2
Items: 
Size: 798268 Color: 1
Size: 201667 Color: 4

Bin 169: 68 of cap free
Amount of items: 3
Items: 
Size: 683972 Color: 4
Size: 158493 Color: 2
Size: 157468 Color: 2

Bin 170: 69 of cap free
Amount of items: 2
Items: 
Size: 502365 Color: 4
Size: 497567 Color: 0

Bin 171: 69 of cap free
Amount of items: 2
Items: 
Size: 593857 Color: 1
Size: 406075 Color: 0

Bin 172: 69 of cap free
Amount of items: 3
Items: 
Size: 715231 Color: 4
Size: 142431 Color: 3
Size: 142270 Color: 3

Bin 173: 69 of cap free
Amount of items: 2
Items: 
Size: 734090 Color: 3
Size: 265842 Color: 4

Bin 174: 70 of cap free
Amount of items: 2
Items: 
Size: 563736 Color: 2
Size: 436195 Color: 1

Bin 175: 70 of cap free
Amount of items: 2
Items: 
Size: 740162 Color: 3
Size: 259769 Color: 2

Bin 176: 70 of cap free
Amount of items: 3
Items: 
Size: 760984 Color: 2
Size: 120611 Color: 0
Size: 118336 Color: 0

Bin 177: 70 of cap free
Amount of items: 3
Items: 
Size: 772591 Color: 0
Size: 116090 Color: 1
Size: 111250 Color: 0

Bin 178: 71 of cap free
Amount of items: 3
Items: 
Size: 686761 Color: 1
Size: 156680 Color: 0
Size: 156489 Color: 3

Bin 179: 71 of cap free
Amount of items: 2
Items: 
Size: 760375 Color: 1
Size: 239555 Color: 0

Bin 180: 72 of cap free
Amount of items: 2
Items: 
Size: 672660 Color: 2
Size: 327269 Color: 4

Bin 181: 72 of cap free
Amount of items: 2
Items: 
Size: 682698 Color: 3
Size: 317231 Color: 4

Bin 182: 73 of cap free
Amount of items: 3
Items: 
Size: 623483 Color: 4
Size: 188303 Color: 3
Size: 188142 Color: 3

Bin 183: 73 of cap free
Amount of items: 3
Items: 
Size: 632922 Color: 0
Size: 183732 Color: 4
Size: 183274 Color: 2

Bin 184: 74 of cap free
Amount of items: 3
Items: 
Size: 690494 Color: 2
Size: 155030 Color: 0
Size: 154403 Color: 4

Bin 185: 74 of cap free
Amount of items: 3
Items: 
Size: 729003 Color: 2
Size: 135809 Color: 4
Size: 135115 Color: 3

Bin 186: 74 of cap free
Amount of items: 3
Items: 
Size: 784467 Color: 2
Size: 108483 Color: 0
Size: 106977 Color: 4

Bin 187: 75 of cap free
Amount of items: 2
Items: 
Size: 525840 Color: 0
Size: 474086 Color: 2

Bin 188: 76 of cap free
Amount of items: 2
Items: 
Size: 609107 Color: 4
Size: 390818 Color: 3

Bin 189: 76 of cap free
Amount of items: 2
Items: 
Size: 628570 Color: 1
Size: 371355 Color: 0

Bin 190: 77 of cap free
Amount of items: 2
Items: 
Size: 536486 Color: 1
Size: 463438 Color: 0

Bin 191: 78 of cap free
Amount of items: 3
Items: 
Size: 624404 Color: 3
Size: 187761 Color: 2
Size: 187758 Color: 1

Bin 192: 79 of cap free
Amount of items: 2
Items: 
Size: 505328 Color: 1
Size: 494594 Color: 2

Bin 193: 79 of cap free
Amount of items: 2
Items: 
Size: 579475 Color: 4
Size: 420447 Color: 0

Bin 194: 79 of cap free
Amount of items: 2
Items: 
Size: 625469 Color: 0
Size: 374453 Color: 2

Bin 195: 80 of cap free
Amount of items: 2
Items: 
Size: 640914 Color: 3
Size: 359007 Color: 1

Bin 196: 80 of cap free
Amount of items: 3
Items: 
Size: 659659 Color: 2
Size: 170862 Color: 0
Size: 169400 Color: 4

Bin 197: 81 of cap free
Amount of items: 2
Items: 
Size: 572469 Color: 3
Size: 427451 Color: 4

Bin 198: 81 of cap free
Amount of items: 2
Items: 
Size: 591665 Color: 2
Size: 408255 Color: 3

Bin 199: 82 of cap free
Amount of items: 2
Items: 
Size: 636982 Color: 0
Size: 362937 Color: 4

Bin 200: 83 of cap free
Amount of items: 2
Items: 
Size: 526877 Color: 0
Size: 473041 Color: 2

Bin 201: 83 of cap free
Amount of items: 2
Items: 
Size: 619972 Color: 0
Size: 379946 Color: 3

Bin 202: 84 of cap free
Amount of items: 2
Items: 
Size: 685466 Color: 2
Size: 314451 Color: 1

Bin 203: 85 of cap free
Amount of items: 2
Items: 
Size: 550332 Color: 4
Size: 449584 Color: 2

Bin 204: 87 of cap free
Amount of items: 2
Items: 
Size: 539452 Color: 2
Size: 460462 Color: 3

Bin 205: 87 of cap free
Amount of items: 3
Items: 
Size: 726063 Color: 4
Size: 138152 Color: 0
Size: 135699 Color: 2

Bin 206: 88 of cap free
Amount of items: 2
Items: 
Size: 614013 Color: 2
Size: 385900 Color: 0

Bin 207: 88 of cap free
Amount of items: 2
Items: 
Size: 718362 Color: 2
Size: 281551 Color: 0

Bin 208: 90 of cap free
Amount of items: 2
Items: 
Size: 522591 Color: 1
Size: 477320 Color: 4

Bin 209: 90 of cap free
Amount of items: 2
Items: 
Size: 582761 Color: 3
Size: 417150 Color: 1

Bin 210: 90 of cap free
Amount of items: 2
Items: 
Size: 614656 Color: 3
Size: 385255 Color: 0

Bin 211: 90 of cap free
Amount of items: 2
Items: 
Size: 618272 Color: 3
Size: 381639 Color: 2

Bin 212: 90 of cap free
Amount of items: 2
Items: 
Size: 694432 Color: 4
Size: 305479 Color: 0

Bin 213: 91 of cap free
Amount of items: 2
Items: 
Size: 621759 Color: 1
Size: 378151 Color: 4

Bin 214: 91 of cap free
Amount of items: 3
Items: 
Size: 703378 Color: 3
Size: 148396 Color: 1
Size: 148136 Color: 2

Bin 215: 91 of cap free
Amount of items: 3
Items: 
Size: 772499 Color: 3
Size: 116249 Color: 4
Size: 111162 Color: 0

Bin 216: 92 of cap free
Amount of items: 2
Items: 
Size: 708816 Color: 2
Size: 291093 Color: 0

Bin 217: 92 of cap free
Amount of items: 3
Items: 
Size: 714306 Color: 3
Size: 143496 Color: 1
Size: 142107 Color: 1

Bin 218: 92 of cap free
Amount of items: 2
Items: 
Size: 747678 Color: 3
Size: 252231 Color: 2

Bin 219: 94 of cap free
Amount of items: 2
Items: 
Size: 711592 Color: 4
Size: 288315 Color: 0

Bin 220: 95 of cap free
Amount of items: 2
Items: 
Size: 732638 Color: 2
Size: 267268 Color: 1

Bin 221: 96 of cap free
Amount of items: 2
Items: 
Size: 526776 Color: 1
Size: 473129 Color: 3

Bin 222: 96 of cap free
Amount of items: 2
Items: 
Size: 617828 Color: 2
Size: 382077 Color: 3

Bin 223: 96 of cap free
Amount of items: 3
Items: 
Size: 709959 Color: 1
Size: 145768 Color: 2
Size: 144178 Color: 0

Bin 224: 97 of cap free
Amount of items: 2
Items: 
Size: 764251 Color: 3
Size: 235653 Color: 1

Bin 225: 98 of cap free
Amount of items: 2
Items: 
Size: 642360 Color: 4
Size: 357543 Color: 1

Bin 226: 98 of cap free
Amount of items: 3
Items: 
Size: 701842 Color: 3
Size: 149954 Color: 4
Size: 148107 Color: 0

Bin 227: 98 of cap free
Amount of items: 2
Items: 
Size: 704175 Color: 3
Size: 295728 Color: 4

Bin 228: 98 of cap free
Amount of items: 3
Items: 
Size: 726630 Color: 0
Size: 136931 Color: 4
Size: 136342 Color: 4

Bin 229: 99 of cap free
Amount of items: 2
Items: 
Size: 506729 Color: 3
Size: 493173 Color: 1

Bin 230: 100 of cap free
Amount of items: 3
Items: 
Size: 634762 Color: 0
Size: 182583 Color: 3
Size: 182556 Color: 2

Bin 231: 100 of cap free
Amount of items: 2
Items: 
Size: 648197 Color: 3
Size: 351704 Color: 1

Bin 232: 100 of cap free
Amount of items: 2
Items: 
Size: 725082 Color: 3
Size: 274819 Color: 1

Bin 233: 101 of cap free
Amount of items: 3
Items: 
Size: 669994 Color: 1
Size: 166690 Color: 2
Size: 163216 Color: 1

Bin 234: 101 of cap free
Amount of items: 2
Items: 
Size: 702248 Color: 0
Size: 297652 Color: 3

Bin 235: 103 of cap free
Amount of items: 2
Items: 
Size: 557277 Color: 0
Size: 442621 Color: 4

Bin 236: 103 of cap free
Amount of items: 2
Items: 
Size: 660790 Color: 0
Size: 339108 Color: 1

Bin 237: 103 of cap free
Amount of items: 2
Items: 
Size: 672873 Color: 1
Size: 327025 Color: 2

Bin 238: 104 of cap free
Amount of items: 2
Items: 
Size: 516415 Color: 4
Size: 483482 Color: 3

Bin 239: 104 of cap free
Amount of items: 2
Items: 
Size: 698893 Color: 1
Size: 301004 Color: 0

Bin 240: 105 of cap free
Amount of items: 3
Items: 
Size: 777714 Color: 0
Size: 111536 Color: 2
Size: 110646 Color: 4

Bin 241: 106 of cap free
Amount of items: 2
Items: 
Size: 584527 Color: 1
Size: 415368 Color: 2

Bin 242: 107 of cap free
Amount of items: 2
Items: 
Size: 799826 Color: 4
Size: 200068 Color: 0

Bin 243: 109 of cap free
Amount of items: 2
Items: 
Size: 563314 Color: 3
Size: 436578 Color: 2

Bin 244: 110 of cap free
Amount of items: 2
Items: 
Size: 646800 Color: 3
Size: 353091 Color: 4

Bin 245: 111 of cap free
Amount of items: 2
Items: 
Size: 508206 Color: 4
Size: 491684 Color: 0

Bin 246: 111 of cap free
Amount of items: 2
Items: 
Size: 531352 Color: 4
Size: 468538 Color: 3

Bin 247: 111 of cap free
Amount of items: 2
Items: 
Size: 641619 Color: 4
Size: 358271 Color: 0

Bin 248: 111 of cap free
Amount of items: 2
Items: 
Size: 794651 Color: 0
Size: 205239 Color: 4

Bin 249: 112 of cap free
Amount of items: 2
Items: 
Size: 684575 Color: 2
Size: 315314 Color: 4

Bin 250: 113 of cap free
Amount of items: 2
Items: 
Size: 745547 Color: 1
Size: 254341 Color: 4

Bin 251: 113 of cap free
Amount of items: 2
Items: 
Size: 796099 Color: 1
Size: 203789 Color: 4

Bin 252: 114 of cap free
Amount of items: 2
Items: 
Size: 529417 Color: 3
Size: 470470 Color: 4

Bin 253: 114 of cap free
Amount of items: 3
Items: 
Size: 556353 Color: 0
Size: 221936 Color: 2
Size: 221598 Color: 4

Bin 254: 114 of cap free
Amount of items: 3
Items: 
Size: 624243 Color: 0
Size: 188436 Color: 2
Size: 187208 Color: 1

Bin 255: 114 of cap free
Amount of items: 2
Items: 
Size: 760565 Color: 2
Size: 239322 Color: 4

Bin 256: 115 of cap free
Amount of items: 2
Items: 
Size: 571926 Color: 3
Size: 427960 Color: 4

Bin 257: 116 of cap free
Amount of items: 2
Items: 
Size: 520122 Color: 0
Size: 479763 Color: 3

Bin 258: 116 of cap free
Amount of items: 2
Items: 
Size: 689147 Color: 0
Size: 310738 Color: 1

Bin 259: 117 of cap free
Amount of items: 2
Items: 
Size: 514598 Color: 1
Size: 485286 Color: 3

Bin 260: 118 of cap free
Amount of items: 2
Items: 
Size: 584013 Color: 4
Size: 415870 Color: 0

Bin 261: 118 of cap free
Amount of items: 3
Items: 
Size: 742856 Color: 4
Size: 128733 Color: 3
Size: 128294 Color: 2

Bin 262: 119 of cap free
Amount of items: 3
Items: 
Size: 639518 Color: 1
Size: 180930 Color: 4
Size: 179434 Color: 0

Bin 263: 120 of cap free
Amount of items: 2
Items: 
Size: 561197 Color: 2
Size: 438684 Color: 4

Bin 264: 120 of cap free
Amount of items: 2
Items: 
Size: 620781 Color: 4
Size: 379100 Color: 2

Bin 265: 121 of cap free
Amount of items: 2
Items: 
Size: 544902 Color: 3
Size: 454978 Color: 2

Bin 266: 123 of cap free
Amount of items: 2
Items: 
Size: 693958 Color: 0
Size: 305920 Color: 2

Bin 267: 124 of cap free
Amount of items: 2
Items: 
Size: 534551 Color: 1
Size: 465326 Color: 2

Bin 268: 125 of cap free
Amount of items: 2
Items: 
Size: 576327 Color: 4
Size: 423549 Color: 3

Bin 269: 125 of cap free
Amount of items: 2
Items: 
Size: 584681 Color: 3
Size: 415195 Color: 4

Bin 270: 125 of cap free
Amount of items: 2
Items: 
Size: 746224 Color: 1
Size: 253652 Color: 4

Bin 271: 126 of cap free
Amount of items: 2
Items: 
Size: 602594 Color: 0
Size: 397281 Color: 2

Bin 272: 127 of cap free
Amount of items: 2
Items: 
Size: 618828 Color: 2
Size: 381046 Color: 4

Bin 273: 128 of cap free
Amount of items: 2
Items: 
Size: 528427 Color: 2
Size: 471446 Color: 4

Bin 274: 128 of cap free
Amount of items: 2
Items: 
Size: 575003 Color: 4
Size: 424870 Color: 3

Bin 275: 129 of cap free
Amount of items: 2
Items: 
Size: 570297 Color: 1
Size: 429575 Color: 3

Bin 276: 132 of cap free
Amount of items: 2
Items: 
Size: 538638 Color: 0
Size: 461231 Color: 4

Bin 277: 133 of cap free
Amount of items: 3
Items: 
Size: 683546 Color: 4
Size: 158213 Color: 2
Size: 158109 Color: 3

Bin 278: 134 of cap free
Amount of items: 2
Items: 
Size: 582236 Color: 0
Size: 417631 Color: 4

Bin 279: 136 of cap free
Amount of items: 2
Items: 
Size: 706483 Color: 4
Size: 293382 Color: 2

Bin 280: 137 of cap free
Amount of items: 2
Items: 
Size: 519680 Color: 1
Size: 480184 Color: 0

Bin 281: 137 of cap free
Amount of items: 2
Items: 
Size: 651801 Color: 0
Size: 348063 Color: 1

Bin 282: 137 of cap free
Amount of items: 2
Items: 
Size: 760136 Color: 4
Size: 239728 Color: 2

Bin 283: 138 of cap free
Amount of items: 2
Items: 
Size: 529848 Color: 2
Size: 470015 Color: 3

Bin 284: 138 of cap free
Amount of items: 2
Items: 
Size: 557375 Color: 4
Size: 442488 Color: 1

Bin 285: 138 of cap free
Amount of items: 3
Items: 
Size: 733787 Color: 3
Size: 133638 Color: 2
Size: 132438 Color: 2

Bin 286: 140 of cap free
Amount of items: 3
Items: 
Size: 620536 Color: 0
Size: 189861 Color: 2
Size: 189464 Color: 2

Bin 287: 142 of cap free
Amount of items: 2
Items: 
Size: 640301 Color: 3
Size: 359558 Color: 1

Bin 288: 142 of cap free
Amount of items: 2
Items: 
Size: 680106 Color: 3
Size: 319753 Color: 0

Bin 289: 143 of cap free
Amount of items: 2
Items: 
Size: 682796 Color: 4
Size: 317062 Color: 2

Bin 290: 144 of cap free
Amount of items: 2
Items: 
Size: 542556 Color: 2
Size: 457301 Color: 1

Bin 291: 144 of cap free
Amount of items: 3
Items: 
Size: 669974 Color: 4
Size: 165270 Color: 0
Size: 164613 Color: 4

Bin 292: 144 of cap free
Amount of items: 3
Items: 
Size: 792846 Color: 1
Size: 103737 Color: 4
Size: 103274 Color: 2

Bin 293: 145 of cap free
Amount of items: 3
Items: 
Size: 717789 Color: 2
Size: 141204 Color: 3
Size: 140863 Color: 2

Bin 294: 149 of cap free
Amount of items: 2
Items: 
Size: 550088 Color: 1
Size: 449764 Color: 0

Bin 295: 149 of cap free
Amount of items: 2
Items: 
Size: 591949 Color: 3
Size: 407903 Color: 4

Bin 296: 149 of cap free
Amount of items: 3
Items: 
Size: 782160 Color: 2
Size: 109623 Color: 4
Size: 108069 Color: 0

Bin 297: 152 of cap free
Amount of items: 2
Items: 
Size: 575576 Color: 1
Size: 424273 Color: 4

Bin 298: 152 of cap free
Amount of items: 3
Items: 
Size: 674648 Color: 1
Size: 162715 Color: 0
Size: 162486 Color: 4

Bin 299: 153 of cap free
Amount of items: 3
Items: 
Size: 709770 Color: 3
Size: 145498 Color: 0
Size: 144580 Color: 1

Bin 300: 153 of cap free
Amount of items: 2
Items: 
Size: 765529 Color: 0
Size: 234319 Color: 3

Bin 301: 154 of cap free
Amount of items: 2
Items: 
Size: 502954 Color: 2
Size: 496893 Color: 0

Bin 302: 154 of cap free
Amount of items: 2
Items: 
Size: 592612 Color: 1
Size: 407235 Color: 3

Bin 303: 155 of cap free
Amount of items: 2
Items: 
Size: 504610 Color: 3
Size: 495236 Color: 1

Bin 304: 156 of cap free
Amount of items: 2
Items: 
Size: 599080 Color: 0
Size: 400765 Color: 4

Bin 305: 156 of cap free
Amount of items: 2
Items: 
Size: 747508 Color: 1
Size: 252337 Color: 2

Bin 306: 157 of cap free
Amount of items: 2
Items: 
Size: 521906 Color: 4
Size: 477938 Color: 2

Bin 307: 157 of cap free
Amount of items: 2
Items: 
Size: 571919 Color: 2
Size: 427925 Color: 0

Bin 308: 158 of cap free
Amount of items: 2
Items: 
Size: 692369 Color: 2
Size: 307474 Color: 3

Bin 309: 159 of cap free
Amount of items: 2
Items: 
Size: 547635 Color: 0
Size: 452207 Color: 4

Bin 310: 160 of cap free
Amount of items: 2
Items: 
Size: 719526 Color: 1
Size: 280315 Color: 2

Bin 311: 163 of cap free
Amount of items: 2
Items: 
Size: 646939 Color: 3
Size: 352899 Color: 2

Bin 312: 163 of cap free
Amount of items: 3
Items: 
Size: 726196 Color: 4
Size: 137563 Color: 2
Size: 136079 Color: 1

Bin 313: 164 of cap free
Amount of items: 3
Items: 
Size: 562845 Color: 4
Size: 219308 Color: 0
Size: 217684 Color: 1

Bin 314: 165 of cap free
Amount of items: 2
Items: 
Size: 521157 Color: 0
Size: 478679 Color: 3

Bin 315: 165 of cap free
Amount of items: 2
Items: 
Size: 715826 Color: 0
Size: 284010 Color: 4

Bin 316: 165 of cap free
Amount of items: 2
Items: 
Size: 723226 Color: 0
Size: 276610 Color: 1

Bin 317: 165 of cap free
Amount of items: 2
Items: 
Size: 735543 Color: 2
Size: 264293 Color: 4

Bin 318: 170 of cap free
Amount of items: 3
Items: 
Size: 652627 Color: 2
Size: 175482 Color: 3
Size: 171722 Color: 4

Bin 319: 171 of cap free
Amount of items: 2
Items: 
Size: 627664 Color: 4
Size: 372166 Color: 1

Bin 320: 171 of cap free
Amount of items: 2
Items: 
Size: 693608 Color: 0
Size: 306222 Color: 3

Bin 321: 172 of cap free
Amount of items: 2
Items: 
Size: 657810 Color: 3
Size: 342019 Color: 0

Bin 322: 173 of cap free
Amount of items: 2
Items: 
Size: 585389 Color: 0
Size: 414439 Color: 4

Bin 323: 173 of cap free
Amount of items: 2
Items: 
Size: 796214 Color: 0
Size: 203614 Color: 2

Bin 324: 174 of cap free
Amount of items: 2
Items: 
Size: 548446 Color: 1
Size: 451381 Color: 2

Bin 325: 174 of cap free
Amount of items: 3
Items: 
Size: 640577 Color: 3
Size: 179777 Color: 4
Size: 179473 Color: 2

Bin 326: 174 of cap free
Amount of items: 3
Items: 
Size: 682162 Color: 4
Size: 160064 Color: 1
Size: 157601 Color: 0

Bin 327: 175 of cap free
Amount of items: 2
Items: 
Size: 663404 Color: 3
Size: 336422 Color: 4

Bin 328: 175 of cap free
Amount of items: 3
Items: 
Size: 747883 Color: 3
Size: 126823 Color: 4
Size: 125120 Color: 2

Bin 329: 175 of cap free
Amount of items: 2
Items: 
Size: 764008 Color: 4
Size: 235818 Color: 3

Bin 330: 176 of cap free
Amount of items: 2
Items: 
Size: 586943 Color: 4
Size: 412882 Color: 1

Bin 331: 176 of cap free
Amount of items: 2
Items: 
Size: 592262 Color: 2
Size: 407563 Color: 3

Bin 332: 176 of cap free
Amount of items: 2
Items: 
Size: 676392 Color: 2
Size: 323433 Color: 1

Bin 333: 176 of cap free
Amount of items: 3
Items: 
Size: 681199 Color: 1
Size: 159323 Color: 0
Size: 159303 Color: 4

Bin 334: 176 of cap free
Amount of items: 2
Items: 
Size: 783289 Color: 2
Size: 216536 Color: 0

Bin 335: 177 of cap free
Amount of items: 2
Items: 
Size: 750895 Color: 0
Size: 248929 Color: 3

Bin 336: 178 of cap free
Amount of items: 3
Items: 
Size: 712609 Color: 2
Size: 143738 Color: 4
Size: 143476 Color: 4

Bin 337: 178 of cap free
Amount of items: 2
Items: 
Size: 738199 Color: 2
Size: 261624 Color: 3

Bin 338: 179 of cap free
Amount of items: 2
Items: 
Size: 550710 Color: 2
Size: 449112 Color: 0

Bin 339: 180 of cap free
Amount of items: 2
Items: 
Size: 519425 Color: 1
Size: 480396 Color: 4

Bin 340: 182 of cap free
Amount of items: 2
Items: 
Size: 689344 Color: 1
Size: 310475 Color: 4

Bin 341: 184 of cap free
Amount of items: 3
Items: 
Size: 558791 Color: 2
Size: 220523 Color: 0
Size: 220503 Color: 2

Bin 342: 184 of cap free
Amount of items: 3
Items: 
Size: 565627 Color: 4
Size: 218099 Color: 1
Size: 216091 Color: 3

Bin 343: 185 of cap free
Amount of items: 3
Items: 
Size: 541216 Color: 1
Size: 232817 Color: 2
Size: 225783 Color: 4

Bin 344: 185 of cap free
Amount of items: 2
Items: 
Size: 627275 Color: 2
Size: 372541 Color: 0

Bin 345: 185 of cap free
Amount of items: 2
Items: 
Size: 658060 Color: 2
Size: 341756 Color: 0

Bin 346: 186 of cap free
Amount of items: 2
Items: 
Size: 579101 Color: 1
Size: 420714 Color: 0

Bin 347: 187 of cap free
Amount of items: 2
Items: 
Size: 628679 Color: 4
Size: 371135 Color: 3

Bin 348: 190 of cap free
Amount of items: 2
Items: 
Size: 533076 Color: 4
Size: 466735 Color: 3

Bin 349: 190 of cap free
Amount of items: 2
Items: 
Size: 658097 Color: 0
Size: 341714 Color: 1

Bin 350: 195 of cap free
Amount of items: 3
Items: 
Size: 611104 Color: 1
Size: 194802 Color: 0
Size: 193900 Color: 3

Bin 351: 195 of cap free
Amount of items: 2
Items: 
Size: 717777 Color: 0
Size: 282029 Color: 3

Bin 352: 197 of cap free
Amount of items: 2
Items: 
Size: 530294 Color: 0
Size: 469510 Color: 4

Bin 353: 197 of cap free
Amount of items: 2
Items: 
Size: 737507 Color: 4
Size: 262297 Color: 0

Bin 354: 200 of cap free
Amount of items: 2
Items: 
Size: 641900 Color: 4
Size: 357901 Color: 1

Bin 355: 201 of cap free
Amount of items: 2
Items: 
Size: 539442 Color: 4
Size: 460358 Color: 1

Bin 356: 203 of cap free
Amount of items: 2
Items: 
Size: 578581 Color: 0
Size: 421217 Color: 4

Bin 357: 206 of cap free
Amount of items: 2
Items: 
Size: 688300 Color: 1
Size: 311495 Color: 2

Bin 358: 210 of cap free
Amount of items: 3
Items: 
Size: 738299 Color: 3
Size: 131014 Color: 0
Size: 130478 Color: 3

Bin 359: 212 of cap free
Amount of items: 2
Items: 
Size: 576739 Color: 0
Size: 423050 Color: 1

Bin 360: 213 of cap free
Amount of items: 2
Items: 
Size: 789138 Color: 1
Size: 210650 Color: 4

Bin 361: 214 of cap free
Amount of items: 2
Items: 
Size: 724313 Color: 1
Size: 275474 Color: 0

Bin 362: 214 of cap free
Amount of items: 2
Items: 
Size: 783834 Color: 3
Size: 215953 Color: 1

Bin 363: 215 of cap free
Amount of items: 2
Items: 
Size: 598741 Color: 0
Size: 401045 Color: 4

Bin 364: 215 of cap free
Amount of items: 2
Items: 
Size: 762897 Color: 3
Size: 236889 Color: 0

Bin 365: 219 of cap free
Amount of items: 2
Items: 
Size: 692611 Color: 0
Size: 307171 Color: 1

Bin 366: 220 of cap free
Amount of items: 2
Items: 
Size: 515826 Color: 3
Size: 483955 Color: 4

Bin 367: 223 of cap free
Amount of items: 3
Items: 
Size: 615610 Color: 1
Size: 192452 Color: 4
Size: 191716 Color: 2

Bin 368: 224 of cap free
Amount of items: 2
Items: 
Size: 753703 Color: 0
Size: 246074 Color: 4

Bin 369: 225 of cap free
Amount of items: 3
Items: 
Size: 796879 Color: 4
Size: 102178 Color: 3
Size: 100719 Color: 2

Bin 370: 226 of cap free
Amount of items: 3
Items: 
Size: 567025 Color: 4
Size: 216386 Color: 1
Size: 216364 Color: 1

Bin 371: 227 of cap free
Amount of items: 3
Items: 
Size: 570966 Color: 1
Size: 215219 Color: 2
Size: 213589 Color: 2

Bin 372: 227 of cap free
Amount of items: 2
Items: 
Size: 707075 Color: 4
Size: 292699 Color: 3

Bin 373: 228 of cap free
Amount of items: 2
Items: 
Size: 797863 Color: 4
Size: 201910 Color: 2

Bin 374: 229 of cap free
Amount of items: 2
Items: 
Size: 780211 Color: 3
Size: 219561 Color: 2

Bin 375: 231 of cap free
Amount of items: 2
Items: 
Size: 704812 Color: 0
Size: 294958 Color: 4

Bin 376: 232 of cap free
Amount of items: 2
Items: 
Size: 661007 Color: 4
Size: 338762 Color: 2

Bin 377: 233 of cap free
Amount of items: 2
Items: 
Size: 714673 Color: 1
Size: 285095 Color: 4

Bin 378: 234 of cap free
Amount of items: 3
Items: 
Size: 642479 Color: 4
Size: 178796 Color: 2
Size: 178492 Color: 3

Bin 379: 234 of cap free
Amount of items: 2
Items: 
Size: 681308 Color: 0
Size: 318459 Color: 2

Bin 380: 234 of cap free
Amount of items: 2
Items: 
Size: 759430 Color: 3
Size: 240337 Color: 4

Bin 381: 235 of cap free
Amount of items: 2
Items: 
Size: 626028 Color: 2
Size: 373738 Color: 0

Bin 382: 237 of cap free
Amount of items: 2
Items: 
Size: 518274 Color: 3
Size: 481490 Color: 1

Bin 383: 237 of cap free
Amount of items: 2
Items: 
Size: 519650 Color: 2
Size: 480114 Color: 4

Bin 384: 237 of cap free
Amount of items: 2
Items: 
Size: 572948 Color: 2
Size: 426816 Color: 1

Bin 385: 238 of cap free
Amount of items: 2
Items: 
Size: 507858 Color: 3
Size: 491905 Color: 4

Bin 386: 239 of cap free
Amount of items: 2
Items: 
Size: 510797 Color: 4
Size: 488965 Color: 2

Bin 387: 239 of cap free
Amount of items: 2
Items: 
Size: 735949 Color: 1
Size: 263813 Color: 3

Bin 388: 240 of cap free
Amount of items: 2
Items: 
Size: 754273 Color: 3
Size: 245488 Color: 2

Bin 389: 243 of cap free
Amount of items: 3
Items: 
Size: 785651 Color: 2
Size: 107494 Color: 0
Size: 106613 Color: 1

Bin 390: 244 of cap free
Amount of items: 2
Items: 
Size: 662964 Color: 4
Size: 336793 Color: 0

Bin 391: 246 of cap free
Amount of items: 2
Items: 
Size: 650597 Color: 1
Size: 349158 Color: 3

Bin 392: 246 of cap free
Amount of items: 3
Items: 
Size: 670618 Color: 4
Size: 165816 Color: 1
Size: 163321 Color: 4

Bin 393: 247 of cap free
Amount of items: 2
Items: 
Size: 621937 Color: 1
Size: 377817 Color: 4

Bin 394: 248 of cap free
Amount of items: 2
Items: 
Size: 646142 Color: 4
Size: 353611 Color: 3

Bin 395: 249 of cap free
Amount of items: 2
Items: 
Size: 590820 Color: 3
Size: 408932 Color: 0

Bin 396: 252 of cap free
Amount of items: 2
Items: 
Size: 649527 Color: 4
Size: 350222 Color: 0

Bin 397: 254 of cap free
Amount of items: 3
Items: 
Size: 679572 Color: 4
Size: 160610 Color: 3
Size: 159565 Color: 4

Bin 398: 254 of cap free
Amount of items: 2
Items: 
Size: 751522 Color: 0
Size: 248225 Color: 4

Bin 399: 255 of cap free
Amount of items: 2
Items: 
Size: 639068 Color: 0
Size: 360678 Color: 3

Bin 400: 258 of cap free
Amount of items: 3
Items: 
Size: 677473 Color: 4
Size: 161725 Color: 2
Size: 160545 Color: 1

Bin 401: 259 of cap free
Amount of items: 2
Items: 
Size: 566410 Color: 2
Size: 433332 Color: 1

Bin 402: 259 of cap free
Amount of items: 2
Items: 
Size: 596543 Color: 3
Size: 403199 Color: 1

Bin 403: 260 of cap free
Amount of items: 2
Items: 
Size: 769509 Color: 1
Size: 230232 Color: 3

Bin 404: 261 of cap free
Amount of items: 2
Items: 
Size: 524422 Color: 2
Size: 475318 Color: 4

Bin 405: 262 of cap free
Amount of items: 2
Items: 
Size: 758924 Color: 1
Size: 240815 Color: 2

Bin 406: 263 of cap free
Amount of items: 2
Items: 
Size: 635300 Color: 1
Size: 364438 Color: 3

Bin 407: 263 of cap free
Amount of items: 3
Items: 
Size: 696490 Color: 3
Size: 152036 Color: 1
Size: 151212 Color: 1

Bin 408: 264 of cap free
Amount of items: 2
Items: 
Size: 781180 Color: 0
Size: 218557 Color: 3

Bin 409: 265 of cap free
Amount of items: 2
Items: 
Size: 539652 Color: 4
Size: 460084 Color: 2

Bin 410: 265 of cap free
Amount of items: 3
Items: 
Size: 646201 Color: 3
Size: 178373 Color: 0
Size: 175162 Color: 0

Bin 411: 266 of cap free
Amount of items: 2
Items: 
Size: 572626 Color: 0
Size: 427109 Color: 2

Bin 412: 266 of cap free
Amount of items: 2
Items: 
Size: 676621 Color: 3
Size: 323114 Color: 1

Bin 413: 267 of cap free
Amount of items: 2
Items: 
Size: 555739 Color: 0
Size: 443995 Color: 4

Bin 414: 269 of cap free
Amount of items: 2
Items: 
Size: 561780 Color: 1
Size: 437952 Color: 3

Bin 415: 269 of cap free
Amount of items: 3
Items: 
Size: 604451 Color: 2
Size: 198375 Color: 4
Size: 196906 Color: 2

Bin 416: 272 of cap free
Amount of items: 3
Items: 
Size: 628481 Color: 0
Size: 187464 Color: 4
Size: 183784 Color: 0

Bin 417: 274 of cap free
Amount of items: 2
Items: 
Size: 601335 Color: 0
Size: 398392 Color: 4

Bin 418: 277 of cap free
Amount of items: 2
Items: 
Size: 666535 Color: 3
Size: 333189 Color: 4

Bin 419: 277 of cap free
Amount of items: 2
Items: 
Size: 668846 Color: 3
Size: 330878 Color: 4

Bin 420: 278 of cap free
Amount of items: 2
Items: 
Size: 732514 Color: 3
Size: 267209 Color: 2

Bin 421: 281 of cap free
Amount of items: 2
Items: 
Size: 642844 Color: 1
Size: 356876 Color: 2

Bin 422: 281 of cap free
Amount of items: 2
Items: 
Size: 748941 Color: 2
Size: 250779 Color: 1

Bin 423: 285 of cap free
Amount of items: 3
Items: 
Size: 750227 Color: 1
Size: 125989 Color: 0
Size: 123500 Color: 2

Bin 424: 286 of cap free
Amount of items: 2
Items: 
Size: 763004 Color: 0
Size: 236711 Color: 3

Bin 425: 287 of cap free
Amount of items: 3
Items: 
Size: 659558 Color: 3
Size: 170494 Color: 1
Size: 169662 Color: 0

Bin 426: 289 of cap free
Amount of items: 2
Items: 
Size: 504189 Color: 1
Size: 495523 Color: 2

Bin 427: 289 of cap free
Amount of items: 2
Items: 
Size: 594847 Color: 0
Size: 404865 Color: 2

Bin 428: 290 of cap free
Amount of items: 2
Items: 
Size: 696933 Color: 1
Size: 302778 Color: 4

Bin 429: 294 of cap free
Amount of items: 2
Items: 
Size: 773289 Color: 2
Size: 226418 Color: 1

Bin 430: 295 of cap free
Amount of items: 3
Items: 
Size: 789983 Color: 3
Size: 106640 Color: 2
Size: 103083 Color: 0

Bin 431: 298 of cap free
Amount of items: 3
Items: 
Size: 762138 Color: 0
Size: 119652 Color: 1
Size: 117913 Color: 1

Bin 432: 301 of cap free
Amount of items: 2
Items: 
Size: 650944 Color: 2
Size: 348756 Color: 0

Bin 433: 306 of cap free
Amount of items: 2
Items: 
Size: 613861 Color: 1
Size: 385834 Color: 2

Bin 434: 306 of cap free
Amount of items: 2
Items: 
Size: 622340 Color: 3
Size: 377355 Color: 2

Bin 435: 307 of cap free
Amount of items: 2
Items: 
Size: 516858 Color: 2
Size: 482836 Color: 0

Bin 436: 307 of cap free
Amount of items: 2
Items: 
Size: 542126 Color: 2
Size: 457568 Color: 4

Bin 437: 307 of cap free
Amount of items: 2
Items: 
Size: 545387 Color: 4
Size: 454307 Color: 2

Bin 438: 307 of cap free
Amount of items: 2
Items: 
Size: 605687 Color: 0
Size: 394007 Color: 2

Bin 439: 308 of cap free
Amount of items: 2
Items: 
Size: 546800 Color: 4
Size: 452893 Color: 3

Bin 440: 309 of cap free
Amount of items: 2
Items: 
Size: 626317 Color: 1
Size: 373375 Color: 2

Bin 441: 309 of cap free
Amount of items: 3
Items: 
Size: 647577 Color: 1
Size: 177571 Color: 4
Size: 174544 Color: 3

Bin 442: 309 of cap free
Amount of items: 2
Items: 
Size: 762073 Color: 4
Size: 237619 Color: 0

Bin 443: 311 of cap free
Amount of items: 3
Items: 
Size: 535610 Color: 4
Size: 232517 Color: 0
Size: 231563 Color: 2

Bin 444: 313 of cap free
Amount of items: 2
Items: 
Size: 602557 Color: 3
Size: 397131 Color: 1

Bin 445: 314 of cap free
Amount of items: 2
Items: 
Size: 528744 Color: 1
Size: 470943 Color: 2

Bin 446: 314 of cap free
Amount of items: 2
Items: 
Size: 551910 Color: 3
Size: 447777 Color: 2

Bin 447: 315 of cap free
Amount of items: 3
Items: 
Size: 630651 Color: 2
Size: 185214 Color: 4
Size: 183821 Color: 1

Bin 448: 316 of cap free
Amount of items: 3
Items: 
Size: 635543 Color: 3
Size: 182078 Color: 4
Size: 182064 Color: 4

Bin 449: 317 of cap free
Amount of items: 2
Items: 
Size: 703641 Color: 2
Size: 296043 Color: 4

Bin 450: 320 of cap free
Amount of items: 2
Items: 
Size: 713506 Color: 3
Size: 286175 Color: 2

Bin 451: 320 of cap free
Amount of items: 2
Items: 
Size: 717089 Color: 0
Size: 282592 Color: 4

Bin 452: 321 of cap free
Amount of items: 2
Items: 
Size: 521764 Color: 0
Size: 477916 Color: 1

Bin 453: 323 of cap free
Amount of items: 3
Items: 
Size: 613161 Color: 3
Size: 193800 Color: 4
Size: 192717 Color: 1

Bin 454: 325 of cap free
Amount of items: 2
Items: 
Size: 533409 Color: 0
Size: 466267 Color: 1

Bin 455: 329 of cap free
Amount of items: 2
Items: 
Size: 763614 Color: 0
Size: 236058 Color: 4

Bin 456: 329 of cap free
Amount of items: 2
Items: 
Size: 783230 Color: 4
Size: 216442 Color: 0

Bin 457: 331 of cap free
Amount of items: 2
Items: 
Size: 538813 Color: 3
Size: 460857 Color: 2

Bin 458: 331 of cap free
Amount of items: 3
Items: 
Size: 788098 Color: 0
Size: 108012 Color: 2
Size: 103560 Color: 4

Bin 459: 336 of cap free
Amount of items: 2
Items: 
Size: 548972 Color: 2
Size: 450693 Color: 0

Bin 460: 336 of cap free
Amount of items: 3
Items: 
Size: 603479 Color: 1
Size: 199547 Color: 4
Size: 196639 Color: 3

Bin 461: 337 of cap free
Amount of items: 3
Items: 
Size: 614472 Color: 3
Size: 192970 Color: 4
Size: 192222 Color: 1

Bin 462: 337 of cap free
Amount of items: 2
Items: 
Size: 637331 Color: 0
Size: 362333 Color: 3

Bin 463: 339 of cap free
Amount of items: 2
Items: 
Size: 729902 Color: 4
Size: 269760 Color: 3

Bin 464: 340 of cap free
Amount of items: 2
Items: 
Size: 550309 Color: 0
Size: 449352 Color: 3

Bin 465: 341 of cap free
Amount of items: 2
Items: 
Size: 502339 Color: 2
Size: 497321 Color: 1

Bin 466: 342 of cap free
Amount of items: 3
Items: 
Size: 662942 Color: 2
Size: 168586 Color: 1
Size: 168131 Color: 4

Bin 467: 344 of cap free
Amount of items: 2
Items: 
Size: 513967 Color: 1
Size: 485690 Color: 4

Bin 468: 347 of cap free
Amount of items: 2
Items: 
Size: 535027 Color: 2
Size: 464627 Color: 3

Bin 469: 352 of cap free
Amount of items: 2
Items: 
Size: 559326 Color: 3
Size: 440323 Color: 1

Bin 470: 352 of cap free
Amount of items: 2
Items: 
Size: 765111 Color: 1
Size: 234538 Color: 4

Bin 471: 354 of cap free
Amount of items: 2
Items: 
Size: 613145 Color: 4
Size: 386502 Color: 0

Bin 472: 355 of cap free
Amount of items: 2
Items: 
Size: 535419 Color: 0
Size: 464227 Color: 4

Bin 473: 355 of cap free
Amount of items: 2
Items: 
Size: 613842 Color: 3
Size: 385804 Color: 0

Bin 474: 355 of cap free
Amount of items: 2
Items: 
Size: 757293 Color: 3
Size: 242353 Color: 1

Bin 475: 356 of cap free
Amount of items: 3
Items: 
Size: 554381 Color: 3
Size: 223715 Color: 0
Size: 221549 Color: 2

Bin 476: 357 of cap free
Amount of items: 2
Items: 
Size: 604367 Color: 3
Size: 395277 Color: 0

Bin 477: 360 of cap free
Amount of items: 3
Items: 
Size: 569466 Color: 3
Size: 215189 Color: 4
Size: 214986 Color: 0

Bin 478: 362 of cap free
Amount of items: 2
Items: 
Size: 527326 Color: 4
Size: 472313 Color: 3

Bin 479: 365 of cap free
Amount of items: 2
Items: 
Size: 547458 Color: 2
Size: 452178 Color: 4

Bin 480: 369 of cap free
Amount of items: 2
Items: 
Size: 695580 Color: 0
Size: 304052 Color: 3

Bin 481: 374 of cap free
Amount of items: 2
Items: 
Size: 716504 Color: 1
Size: 283123 Color: 2

Bin 482: 375 of cap free
Amount of items: 2
Items: 
Size: 522500 Color: 3
Size: 477126 Color: 2

Bin 483: 375 of cap free
Amount of items: 3
Items: 
Size: 637283 Color: 2
Size: 181669 Color: 0
Size: 180674 Color: 0

Bin 484: 376 of cap free
Amount of items: 2
Items: 
Size: 665286 Color: 4
Size: 334339 Color: 1

Bin 485: 376 of cap free
Amount of items: 2
Items: 
Size: 700364 Color: 1
Size: 299261 Color: 0

Bin 486: 376 of cap free
Amount of items: 2
Items: 
Size: 748017 Color: 4
Size: 251608 Color: 2

Bin 487: 377 of cap free
Amount of items: 2
Items: 
Size: 771331 Color: 2
Size: 228293 Color: 3

Bin 488: 378 of cap free
Amount of items: 2
Items: 
Size: 786826 Color: 1
Size: 212797 Color: 2

Bin 489: 380 of cap free
Amount of items: 2
Items: 
Size: 678345 Color: 0
Size: 321276 Color: 4

Bin 490: 382 of cap free
Amount of items: 2
Items: 
Size: 634296 Color: 3
Size: 365323 Color: 0

Bin 491: 384 of cap free
Amount of items: 2
Items: 
Size: 501037 Color: 2
Size: 498580 Color: 3

Bin 492: 385 of cap free
Amount of items: 3
Items: 
Size: 681192 Color: 1
Size: 159733 Color: 3
Size: 158691 Color: 3

Bin 493: 387 of cap free
Amount of items: 2
Items: 
Size: 667137 Color: 1
Size: 332477 Color: 4

Bin 494: 390 of cap free
Amount of items: 2
Items: 
Size: 606600 Color: 0
Size: 393011 Color: 1

Bin 495: 400 of cap free
Amount of items: 3
Items: 
Size: 725071 Color: 2
Size: 137922 Color: 4
Size: 136608 Color: 1

Bin 496: 401 of cap free
Amount of items: 2
Items: 
Size: 753092 Color: 1
Size: 246508 Color: 3

Bin 497: 404 of cap free
Amount of items: 2
Items: 
Size: 745817 Color: 0
Size: 253780 Color: 3

Bin 498: 407 of cap free
Amount of items: 2
Items: 
Size: 582730 Color: 3
Size: 416864 Color: 2

Bin 499: 408 of cap free
Amount of items: 2
Items: 
Size: 641785 Color: 2
Size: 357808 Color: 0

Bin 500: 410 of cap free
Amount of items: 3
Items: 
Size: 735088 Color: 3
Size: 132652 Color: 0
Size: 131851 Color: 2

Bin 501: 411 of cap free
Amount of items: 2
Items: 
Size: 622776 Color: 0
Size: 376814 Color: 2

Bin 502: 414 of cap free
Amount of items: 2
Items: 
Size: 540717 Color: 1
Size: 458870 Color: 2

Bin 503: 414 of cap free
Amount of items: 2
Items: 
Size: 597652 Color: 0
Size: 401935 Color: 4

Bin 504: 421 of cap free
Amount of items: 2
Items: 
Size: 733999 Color: 4
Size: 265581 Color: 3

Bin 505: 423 of cap free
Amount of items: 2
Items: 
Size: 699006 Color: 1
Size: 300572 Color: 4

Bin 506: 428 of cap free
Amount of items: 3
Items: 
Size: 706941 Color: 1
Size: 146528 Color: 4
Size: 146104 Color: 2

Bin 507: 432 of cap free
Amount of items: 2
Items: 
Size: 571739 Color: 1
Size: 427830 Color: 4

Bin 508: 433 of cap free
Amount of items: 2
Items: 
Size: 586746 Color: 2
Size: 412822 Color: 3

Bin 509: 434 of cap free
Amount of items: 2
Items: 
Size: 543187 Color: 3
Size: 456380 Color: 1

Bin 510: 437 of cap free
Amount of items: 3
Items: 
Size: 742566 Color: 3
Size: 128931 Color: 4
Size: 128067 Color: 3

Bin 511: 440 of cap free
Amount of items: 2
Items: 
Size: 524802 Color: 1
Size: 474759 Color: 2

Bin 512: 443 of cap free
Amount of items: 2
Items: 
Size: 520825 Color: 2
Size: 478733 Color: 0

Bin 513: 444 of cap free
Amount of items: 2
Items: 
Size: 754973 Color: 1
Size: 244584 Color: 4

Bin 514: 449 of cap free
Amount of items: 3
Items: 
Size: 755448 Color: 4
Size: 122775 Color: 1
Size: 121329 Color: 3

Bin 515: 452 of cap free
Amount of items: 2
Items: 
Size: 574968 Color: 2
Size: 424581 Color: 3

Bin 516: 456 of cap free
Amount of items: 2
Items: 
Size: 587922 Color: 0
Size: 411623 Color: 4

Bin 517: 456 of cap free
Amount of items: 2
Items: 
Size: 693621 Color: 3
Size: 305924 Color: 0

Bin 518: 466 of cap free
Amount of items: 2
Items: 
Size: 725199 Color: 4
Size: 274336 Color: 2

Bin 519: 472 of cap free
Amount of items: 2
Items: 
Size: 723137 Color: 0
Size: 276392 Color: 4

Bin 520: 473 of cap free
Amount of items: 2
Items: 
Size: 799745 Color: 1
Size: 199783 Color: 4

Bin 521: 476 of cap free
Amount of items: 2
Items: 
Size: 750634 Color: 3
Size: 248891 Color: 1

Bin 522: 477 of cap free
Amount of items: 2
Items: 
Size: 665227 Color: 0
Size: 334297 Color: 1

Bin 523: 483 of cap free
Amount of items: 2
Items: 
Size: 563332 Color: 2
Size: 436186 Color: 4

Bin 524: 484 of cap free
Amount of items: 2
Items: 
Size: 754261 Color: 1
Size: 245256 Color: 4

Bin 525: 485 of cap free
Amount of items: 3
Items: 
Size: 618189 Color: 4
Size: 190677 Color: 2
Size: 190650 Color: 4

Bin 526: 486 of cap free
Amount of items: 2
Items: 
Size: 558743 Color: 3
Size: 440772 Color: 1

Bin 527: 486 of cap free
Amount of items: 2
Items: 
Size: 780164 Color: 2
Size: 219351 Color: 0

Bin 528: 487 of cap free
Amount of items: 2
Items: 
Size: 743711 Color: 3
Size: 255803 Color: 2

Bin 529: 493 of cap free
Amount of items: 2
Items: 
Size: 579807 Color: 3
Size: 419701 Color: 2

Bin 530: 496 of cap free
Amount of items: 3
Items: 
Size: 704697 Color: 3
Size: 147509 Color: 1
Size: 147299 Color: 1

Bin 531: 499 of cap free
Amount of items: 2
Items: 
Size: 574211 Color: 3
Size: 425291 Color: 4

Bin 532: 504 of cap free
Amount of items: 3
Items: 
Size: 779940 Color: 4
Size: 110285 Color: 2
Size: 109272 Color: 3

Bin 533: 507 of cap free
Amount of items: 2
Items: 
Size: 644496 Color: 2
Size: 354998 Color: 4

Bin 534: 507 of cap free
Amount of items: 3
Items: 
Size: 695482 Color: 2
Size: 152016 Color: 3
Size: 151996 Color: 4

Bin 535: 507 of cap free
Amount of items: 2
Items: 
Size: 787923 Color: 2
Size: 211571 Color: 1

Bin 536: 513 of cap free
Amount of items: 3
Items: 
Size: 655309 Color: 3
Size: 172314 Color: 4
Size: 171865 Color: 3

Bin 537: 515 of cap free
Amount of items: 2
Items: 
Size: 670601 Color: 1
Size: 328885 Color: 2

Bin 538: 518 of cap free
Amount of items: 2
Items: 
Size: 528560 Color: 2
Size: 470923 Color: 3

Bin 539: 518 of cap free
Amount of items: 2
Items: 
Size: 585977 Color: 3
Size: 413506 Color: 2

Bin 540: 521 of cap free
Amount of items: 2
Items: 
Size: 659972 Color: 1
Size: 339508 Color: 3

Bin 541: 525 of cap free
Amount of items: 2
Items: 
Size: 652332 Color: 2
Size: 347144 Color: 3

Bin 542: 525 of cap free
Amount of items: 2
Items: 
Size: 704166 Color: 4
Size: 295310 Color: 1

Bin 543: 532 of cap free
Amount of items: 2
Items: 
Size: 566455 Color: 1
Size: 433014 Color: 4

Bin 544: 533 of cap free
Amount of items: 2
Items: 
Size: 500258 Color: 0
Size: 499210 Color: 2

Bin 545: 533 of cap free
Amount of items: 2
Items: 
Size: 562079 Color: 2
Size: 437389 Color: 3

Bin 546: 533 of cap free
Amount of items: 2
Items: 
Size: 773065 Color: 3
Size: 226403 Color: 0

Bin 547: 534 of cap free
Amount of items: 2
Items: 
Size: 579366 Color: 2
Size: 420101 Color: 3

Bin 548: 534 of cap free
Amount of items: 2
Items: 
Size: 681091 Color: 1
Size: 318376 Color: 0

Bin 549: 543 of cap free
Amount of items: 2
Items: 
Size: 518570 Color: 2
Size: 480888 Color: 4

Bin 550: 545 of cap free
Amount of items: 3
Items: 
Size: 606013 Color: 2
Size: 197515 Color: 0
Size: 195928 Color: 3

Bin 551: 548 of cap free
Amount of items: 2
Items: 
Size: 602479 Color: 2
Size: 396974 Color: 3

Bin 552: 548 of cap free
Amount of items: 3
Items: 
Size: 791214 Color: 3
Size: 104620 Color: 2
Size: 103619 Color: 4

Bin 553: 560 of cap free
Amount of items: 2
Items: 
Size: 709370 Color: 3
Size: 290071 Color: 0

Bin 554: 564 of cap free
Amount of items: 3
Items: 
Size: 538689 Color: 4
Size: 230579 Color: 0
Size: 230169 Color: 2

Bin 555: 564 of cap free
Amount of items: 2
Items: 
Size: 728813 Color: 2
Size: 270624 Color: 1

Bin 556: 569 of cap free
Amount of items: 2
Items: 
Size: 725903 Color: 0
Size: 273529 Color: 2

Bin 557: 572 of cap free
Amount of items: 2
Items: 
Size: 655266 Color: 3
Size: 344163 Color: 4

Bin 558: 574 of cap free
Amount of items: 2
Items: 
Size: 694267 Color: 3
Size: 305160 Color: 0

Bin 559: 578 of cap free
Amount of items: 2
Items: 
Size: 748766 Color: 2
Size: 250657 Color: 1

Bin 560: 580 of cap free
Amount of items: 2
Items: 
Size: 545908 Color: 1
Size: 453513 Color: 3

Bin 561: 580 of cap free
Amount of items: 2
Items: 
Size: 686396 Color: 0
Size: 313025 Color: 4

Bin 562: 583 of cap free
Amount of items: 2
Items: 
Size: 516675 Color: 3
Size: 482743 Color: 1

Bin 563: 584 of cap free
Amount of items: 2
Items: 
Size: 646633 Color: 1
Size: 352784 Color: 4

Bin 564: 585 of cap free
Amount of items: 2
Items: 
Size: 607211 Color: 3
Size: 392205 Color: 0

Bin 565: 590 of cap free
Amount of items: 2
Items: 
Size: 730757 Color: 4
Size: 268654 Color: 1

Bin 566: 598 of cap free
Amount of items: 2
Items: 
Size: 595892 Color: 3
Size: 403511 Color: 1

Bin 567: 598 of cap free
Amount of items: 2
Items: 
Size: 646626 Color: 3
Size: 352777 Color: 2

Bin 568: 599 of cap free
Amount of items: 3
Items: 
Size: 527260 Color: 3
Size: 236468 Color: 4
Size: 235674 Color: 3

Bin 569: 604 of cap free
Amount of items: 2
Items: 
Size: 583897 Color: 2
Size: 415500 Color: 0

Bin 570: 606 of cap free
Amount of items: 3
Items: 
Size: 668011 Color: 2
Size: 166215 Color: 4
Size: 165169 Color: 4

Bin 571: 614 of cap free
Amount of items: 2
Items: 
Size: 616402 Color: 4
Size: 382985 Color: 3

Bin 572: 618 of cap free
Amount of items: 3
Items: 
Size: 476903 Color: 0
Size: 284427 Color: 4
Size: 238053 Color: 4

Bin 573: 619 of cap free
Amount of items: 2
Items: 
Size: 619535 Color: 3
Size: 379847 Color: 2

Bin 574: 623 of cap free
Amount of items: 2
Items: 
Size: 514114 Color: 4
Size: 485264 Color: 1

Bin 575: 628 of cap free
Amount of items: 2
Items: 
Size: 785363 Color: 4
Size: 214010 Color: 3

Bin 576: 636 of cap free
Amount of items: 2
Items: 
Size: 545887 Color: 1
Size: 453478 Color: 4

Bin 577: 639 of cap free
Amount of items: 2
Items: 
Size: 712545 Color: 1
Size: 286817 Color: 0

Bin 578: 651 of cap free
Amount of items: 3
Items: 
Size: 739665 Color: 3
Size: 129996 Color: 0
Size: 129689 Color: 2

Bin 579: 658 of cap free
Amount of items: 2
Items: 
Size: 593833 Color: 2
Size: 405510 Color: 1

Bin 580: 662 of cap free
Amount of items: 2
Items: 
Size: 740515 Color: 3
Size: 258824 Color: 0

Bin 581: 667 of cap free
Amount of items: 2
Items: 
Size: 722505 Color: 4
Size: 276829 Color: 0

Bin 582: 672 of cap free
Amount of items: 2
Items: 
Size: 784105 Color: 4
Size: 215224 Color: 0

Bin 583: 673 of cap free
Amount of items: 2
Items: 
Size: 747078 Color: 2
Size: 252250 Color: 3

Bin 584: 678 of cap free
Amount of items: 2
Items: 
Size: 577132 Color: 3
Size: 422191 Color: 0

Bin 585: 679 of cap free
Amount of items: 2
Items: 
Size: 607207 Color: 3
Size: 392115 Color: 4

Bin 586: 682 of cap free
Amount of items: 2
Items: 
Size: 638665 Color: 0
Size: 360654 Color: 3

Bin 587: 688 of cap free
Amount of items: 2
Items: 
Size: 521465 Color: 2
Size: 477848 Color: 3

Bin 588: 689 of cap free
Amount of items: 2
Items: 
Size: 511634 Color: 1
Size: 487678 Color: 0

Bin 589: 689 of cap free
Amount of items: 2
Items: 
Size: 662605 Color: 4
Size: 336707 Color: 2

Bin 590: 690 of cap free
Amount of items: 2
Items: 
Size: 512739 Color: 3
Size: 486572 Color: 4

Bin 591: 711 of cap free
Amount of items: 2
Items: 
Size: 500832 Color: 2
Size: 498458 Color: 4

Bin 592: 729 of cap free
Amount of items: 2
Items: 
Size: 531475 Color: 2
Size: 467797 Color: 4

Bin 593: 733 of cap free
Amount of items: 2
Items: 
Size: 590743 Color: 0
Size: 408525 Color: 3

Bin 594: 741 of cap free
Amount of items: 2
Items: 
Size: 643877 Color: 3
Size: 355383 Color: 2

Bin 595: 741 of cap free
Amount of items: 2
Items: 
Size: 724039 Color: 0
Size: 275221 Color: 4

Bin 596: 753 of cap free
Amount of items: 2
Items: 
Size: 626056 Color: 0
Size: 373192 Color: 4

Bin 597: 756 of cap free
Amount of items: 2
Items: 
Size: 537167 Color: 2
Size: 462078 Color: 0

Bin 598: 760 of cap free
Amount of items: 2
Items: 
Size: 612739 Color: 0
Size: 386502 Color: 4

Bin 599: 770 of cap free
Amount of items: 2
Items: 
Size: 505185 Color: 4
Size: 494046 Color: 3

Bin 600: 772 of cap free
Amount of items: 2
Items: 
Size: 696395 Color: 2
Size: 302834 Color: 1

Bin 601: 774 of cap free
Amount of items: 2
Items: 
Size: 604222 Color: 4
Size: 395005 Color: 2

Bin 602: 778 of cap free
Amount of items: 2
Items: 
Size: 678012 Color: 0
Size: 321211 Color: 1

Bin 603: 779 of cap free
Amount of items: 3
Items: 
Size: 545560 Color: 2
Size: 227561 Color: 1
Size: 226101 Color: 3

Bin 604: 779 of cap free
Amount of items: 2
Items: 
Size: 724150 Color: 4
Size: 275072 Color: 0

Bin 605: 780 of cap free
Amount of items: 2
Items: 
Size: 790780 Color: 1
Size: 208441 Color: 3

Bin 606: 786 of cap free
Amount of items: 2
Items: 
Size: 530440 Color: 4
Size: 468775 Color: 0

Bin 607: 786 of cap free
Amount of items: 2
Items: 
Size: 664110 Color: 4
Size: 335105 Color: 1

Bin 608: 787 of cap free
Amount of items: 2
Items: 
Size: 559752 Color: 3
Size: 439462 Color: 0

Bin 609: 789 of cap free
Amount of items: 2
Items: 
Size: 578545 Color: 3
Size: 420667 Color: 2

Bin 610: 791 of cap free
Amount of items: 2
Items: 
Size: 585092 Color: 4
Size: 414118 Color: 3

Bin 611: 792 of cap free
Amount of items: 2
Items: 
Size: 507744 Color: 4
Size: 491465 Color: 3

Bin 612: 798 of cap free
Amount of items: 3
Items: 
Size: 597577 Color: 0
Size: 201270 Color: 4
Size: 200356 Color: 4

Bin 613: 801 of cap free
Amount of items: 2
Items: 
Size: 578236 Color: 0
Size: 420964 Color: 3

Bin 614: 809 of cap free
Amount of items: 2
Items: 
Size: 582400 Color: 2
Size: 416792 Color: 3

Bin 615: 811 of cap free
Amount of items: 2
Items: 
Size: 552852 Color: 0
Size: 446338 Color: 3

Bin 616: 814 of cap free
Amount of items: 2
Items: 
Size: 651792 Color: 1
Size: 347395 Color: 2

Bin 617: 821 of cap free
Amount of items: 3
Items: 
Size: 653526 Color: 3
Size: 173885 Color: 1
Size: 171769 Color: 1

Bin 618: 828 of cap free
Amount of items: 2
Items: 
Size: 519263 Color: 1
Size: 479910 Color: 2

Bin 619: 829 of cap free
Amount of items: 2
Items: 
Size: 700006 Color: 0
Size: 299166 Color: 1

Bin 620: 832 of cap free
Amount of items: 2
Items: 
Size: 588005 Color: 4
Size: 411164 Color: 2

Bin 621: 865 of cap free
Amount of items: 2
Items: 
Size: 505551 Color: 3
Size: 493585 Color: 2

Bin 622: 878 of cap free
Amount of items: 2
Items: 
Size: 521362 Color: 4
Size: 477761 Color: 3

Bin 623: 880 of cap free
Amount of items: 2
Items: 
Size: 650589 Color: 4
Size: 348532 Color: 1

Bin 624: 888 of cap free
Amount of items: 2
Items: 
Size: 551523 Color: 0
Size: 447590 Color: 2

Bin 625: 891 of cap free
Amount of items: 2
Items: 
Size: 578525 Color: 3
Size: 420585 Color: 2

Bin 626: 905 of cap free
Amount of items: 2
Items: 
Size: 796536 Color: 2
Size: 202560 Color: 1

Bin 627: 912 of cap free
Amount of items: 2
Items: 
Size: 686969 Color: 4
Size: 312120 Color: 1

Bin 628: 913 of cap free
Amount of items: 2
Items: 
Size: 511603 Color: 4
Size: 487485 Color: 2

Bin 629: 919 of cap free
Amount of items: 2
Items: 
Size: 712438 Color: 2
Size: 286644 Color: 0

Bin 630: 939 of cap free
Amount of items: 2
Items: 
Size: 780885 Color: 1
Size: 218177 Color: 2

Bin 631: 941 of cap free
Amount of items: 2
Items: 
Size: 731672 Color: 0
Size: 267388 Color: 3

Bin 632: 947 of cap free
Amount of items: 2
Items: 
Size: 752129 Color: 2
Size: 246925 Color: 0

Bin 633: 968 of cap free
Amount of items: 2
Items: 
Size: 757129 Color: 4
Size: 241904 Color: 2

Bin 634: 997 of cap free
Amount of items: 2
Items: 
Size: 567817 Color: 1
Size: 431187 Color: 0

Bin 635: 999 of cap free
Amount of items: 2
Items: 
Size: 562070 Color: 1
Size: 436932 Color: 0

Bin 636: 1021 of cap free
Amount of items: 2
Items: 
Size: 785279 Color: 1
Size: 213701 Color: 4

Bin 637: 1050 of cap free
Amount of items: 2
Items: 
Size: 659636 Color: 1
Size: 339315 Color: 3

Bin 638: 1052 of cap free
Amount of items: 2
Items: 
Size: 693101 Color: 1
Size: 305848 Color: 4

Bin 639: 1056 of cap free
Amount of items: 2
Items: 
Size: 516655 Color: 3
Size: 482290 Color: 1

Bin 640: 1064 of cap free
Amount of items: 2
Items: 
Size: 677830 Color: 2
Size: 321107 Color: 0

Bin 641: 1074 of cap free
Amount of items: 2
Items: 
Size: 720368 Color: 0
Size: 278559 Color: 3

Bin 642: 1075 of cap free
Amount of items: 2
Items: 
Size: 755565 Color: 1
Size: 243361 Color: 4

Bin 643: 1084 of cap free
Amount of items: 2
Items: 
Size: 604258 Color: 2
Size: 394659 Color: 3

Bin 644: 1101 of cap free
Amount of items: 3
Items: 
Size: 554075 Color: 3
Size: 223449 Color: 4
Size: 221376 Color: 4

Bin 645: 1106 of cap free
Amount of items: 2
Items: 
Size: 668019 Color: 4
Size: 330876 Color: 0

Bin 646: 1115 of cap free
Amount of items: 2
Items: 
Size: 730687 Color: 4
Size: 268199 Color: 0

Bin 647: 1131 of cap free
Amount of items: 3
Items: 
Size: 532492 Color: 1
Size: 233231 Color: 0
Size: 233147 Color: 3

Bin 648: 1149 of cap free
Amount of items: 2
Items: 
Size: 519147 Color: 4
Size: 479705 Color: 3

Bin 649: 1154 of cap free
Amount of items: 2
Items: 
Size: 772484 Color: 3
Size: 226363 Color: 0

Bin 650: 1165 of cap free
Amount of items: 2
Items: 
Size: 584955 Color: 0
Size: 413881 Color: 3

Bin 651: 1167 of cap free
Amount of items: 2
Items: 
Size: 504975 Color: 2
Size: 493859 Color: 3

Bin 652: 1177 of cap free
Amount of items: 2
Items: 
Size: 704157 Color: 1
Size: 294667 Color: 2

Bin 653: 1177 of cap free
Amount of items: 2
Items: 
Size: 765470 Color: 1
Size: 233354 Color: 3

Bin 654: 1184 of cap free
Amount of items: 2
Items: 
Size: 500827 Color: 2
Size: 497990 Color: 1

Bin 655: 1185 of cap free
Amount of items: 2
Items: 
Size: 696037 Color: 4
Size: 302779 Color: 1

Bin 656: 1195 of cap free
Amount of items: 2
Items: 
Size: 798186 Color: 4
Size: 200620 Color: 1

Bin 657: 1202 of cap free
Amount of items: 2
Items: 
Size: 705638 Color: 0
Size: 293161 Color: 4

Bin 658: 1206 of cap free
Amount of items: 2
Items: 
Size: 694230 Color: 1
Size: 304565 Color: 2

Bin 659: 1207 of cap free
Amount of items: 2
Items: 
Size: 742312 Color: 0
Size: 256482 Color: 2

Bin 660: 1212 of cap free
Amount of items: 2
Items: 
Size: 509406 Color: 1
Size: 489383 Color: 3

Bin 661: 1219 of cap free
Amount of items: 2
Items: 
Size: 507441 Color: 2
Size: 491341 Color: 1

Bin 662: 1231 of cap free
Amount of items: 2
Items: 
Size: 625636 Color: 0
Size: 373134 Color: 2

Bin 663: 1242 of cap free
Amount of items: 2
Items: 
Size: 567805 Color: 1
Size: 430954 Color: 2

Bin 664: 1263 of cap free
Amount of items: 2
Items: 
Size: 601138 Color: 0
Size: 397600 Color: 1

Bin 665: 1268 of cap free
Amount of items: 2
Items: 
Size: 638543 Color: 3
Size: 360190 Color: 2

Bin 666: 1269 of cap free
Amount of items: 2
Items: 
Size: 720353 Color: 2
Size: 278379 Color: 3

Bin 667: 1273 of cap free
Amount of items: 2
Items: 
Size: 536988 Color: 4
Size: 461740 Color: 3

Bin 668: 1274 of cap free
Amount of items: 2
Items: 
Size: 738138 Color: 4
Size: 260589 Color: 3

Bin 669: 1280 of cap free
Amount of items: 2
Items: 
Size: 575836 Color: 2
Size: 422885 Color: 3

Bin 670: 1280 of cap free
Amount of items: 2
Items: 
Size: 603916 Color: 1
Size: 394805 Color: 2

Bin 671: 1283 of cap free
Amount of items: 2
Items: 
Size: 699718 Color: 2
Size: 299000 Color: 1

Bin 672: 1293 of cap free
Amount of items: 2
Items: 
Size: 585272 Color: 3
Size: 413436 Color: 1

Bin 673: 1302 of cap free
Amount of items: 2
Items: 
Size: 745732 Color: 0
Size: 252967 Color: 1

Bin 674: 1304 of cap free
Amount of items: 2
Items: 
Size: 646041 Color: 0
Size: 352656 Color: 3

Bin 675: 1316 of cap free
Amount of items: 2
Items: 
Size: 566298 Color: 0
Size: 432387 Color: 3

Bin 676: 1328 of cap free
Amount of items: 2
Items: 
Size: 758630 Color: 1
Size: 240043 Color: 0

Bin 677: 1361 of cap free
Amount of items: 2
Items: 
Size: 587704 Color: 1
Size: 410936 Color: 4

Bin 678: 1388 of cap free
Amount of items: 2
Items: 
Size: 643487 Color: 3
Size: 355126 Color: 2

Bin 679: 1396 of cap free
Amount of items: 2
Items: 
Size: 712067 Color: 4
Size: 286538 Color: 2

Bin 680: 1406 of cap free
Amount of items: 2
Items: 
Size: 555548 Color: 0
Size: 443047 Color: 3

Bin 681: 1415 of cap free
Amount of items: 2
Items: 
Size: 790564 Color: 4
Size: 208022 Color: 3

Bin 682: 1421 of cap free
Amount of items: 2
Items: 
Size: 749731 Color: 3
Size: 248849 Color: 4

Bin 683: 1434 of cap free
Amount of items: 2
Items: 
Size: 749845 Color: 4
Size: 248722 Color: 3

Bin 684: 1436 of cap free
Amount of items: 2
Items: 
Size: 701478 Color: 4
Size: 297087 Color: 1

Bin 685: 1438 of cap free
Amount of items: 2
Items: 
Size: 662413 Color: 0
Size: 336150 Color: 4

Bin 686: 1439 of cap free
Amount of items: 2
Items: 
Size: 669809 Color: 3
Size: 328753 Color: 4

Bin 687: 1442 of cap free
Amount of items: 2
Items: 
Size: 716514 Color: 2
Size: 282045 Color: 0

Bin 688: 1450 of cap free
Amount of items: 2
Items: 
Size: 559395 Color: 1
Size: 439156 Color: 4

Bin 689: 1450 of cap free
Amount of items: 2
Items: 
Size: 608969 Color: 3
Size: 389582 Color: 1

Bin 690: 1452 of cap free
Amount of items: 2
Items: 
Size: 595213 Color: 2
Size: 403336 Color: 3

Bin 691: 1456 of cap free
Amount of items: 2
Items: 
Size: 691483 Color: 1
Size: 307062 Color: 0

Bin 692: 1465 of cap free
Amount of items: 2
Items: 
Size: 738051 Color: 3
Size: 260485 Color: 2

Bin 693: 1483 of cap free
Amount of items: 2
Items: 
Size: 590661 Color: 3
Size: 407857 Color: 1

Bin 694: 1486 of cap free
Amount of items: 2
Items: 
Size: 694865 Color: 2
Size: 303650 Color: 4

Bin 695: 1506 of cap free
Amount of items: 2
Items: 
Size: 526481 Color: 1
Size: 472014 Color: 3

Bin 696: 1517 of cap free
Amount of items: 2
Items: 
Size: 552789 Color: 0
Size: 445695 Color: 3

Bin 697: 1522 of cap free
Amount of items: 2
Items: 
Size: 581803 Color: 2
Size: 416676 Color: 0

Bin 698: 1561 of cap free
Amount of items: 2
Items: 
Size: 540500 Color: 0
Size: 457940 Color: 3

Bin 699: 1564 of cap free
Amount of items: 2
Items: 
Size: 572936 Color: 2
Size: 425501 Color: 3

Bin 700: 1564 of cap free
Amount of items: 2
Items: 
Size: 593001 Color: 1
Size: 405436 Color: 3

Bin 701: 1585 of cap free
Amount of items: 2
Items: 
Size: 645937 Color: 1
Size: 352479 Color: 3

Bin 702: 1605 of cap free
Amount of items: 2
Items: 
Size: 720132 Color: 2
Size: 278264 Color: 3

Bin 703: 1637 of cap free
Amount of items: 2
Items: 
Size: 795643 Color: 4
Size: 202721 Color: 2

Bin 704: 1650 of cap free
Amount of items: 2
Items: 
Size: 524023 Color: 4
Size: 474328 Color: 0

Bin 705: 1660 of cap free
Amount of items: 2
Items: 
Size: 638304 Color: 1
Size: 360037 Color: 4

Bin 706: 1669 of cap free
Amount of items: 2
Items: 
Size: 571462 Color: 3
Size: 426870 Color: 2

Bin 707: 1671 of cap free
Amount of items: 2
Items: 
Size: 725787 Color: 4
Size: 272543 Color: 1

Bin 708: 1674 of cap free
Amount of items: 2
Items: 
Size: 676088 Color: 0
Size: 322239 Color: 2

Bin 709: 1710 of cap free
Amount of items: 2
Items: 
Size: 623448 Color: 2
Size: 374843 Color: 3

Bin 710: 1733 of cap free
Amount of items: 2
Items: 
Size: 534272 Color: 4
Size: 463996 Color: 2

Bin 711: 1746 of cap free
Amount of items: 2
Items: 
Size: 790912 Color: 3
Size: 207343 Color: 4

Bin 712: 1750 of cap free
Amount of items: 2
Items: 
Size: 669677 Color: 3
Size: 328574 Color: 2

Bin 713: 1754 of cap free
Amount of items: 2
Items: 
Size: 790376 Color: 1
Size: 207871 Color: 3

Bin 714: 1756 of cap free
Amount of items: 2
Items: 
Size: 701404 Color: 4
Size: 296841 Color: 0

Bin 715: 1787 of cap free
Amount of items: 2
Items: 
Size: 526310 Color: 4
Size: 471904 Color: 0

Bin 716: 1792 of cap free
Amount of items: 2
Items: 
Size: 641619 Color: 0
Size: 356590 Color: 2

Bin 717: 1827 of cap free
Amount of items: 2
Items: 
Size: 795618 Color: 1
Size: 202556 Color: 2

Bin 718: 1841 of cap free
Amount of items: 2
Items: 
Size: 597547 Color: 2
Size: 400613 Color: 4

Bin 719: 1851 of cap free
Amount of items: 2
Items: 
Size: 544759 Color: 0
Size: 453391 Color: 2

Bin 720: 1866 of cap free
Amount of items: 2
Items: 
Size: 760928 Color: 4
Size: 237207 Color: 0

Bin 721: 1908 of cap free
Amount of items: 2
Items: 
Size: 664955 Color: 1
Size: 333138 Color: 4

Bin 722: 1930 of cap free
Amount of items: 2
Items: 
Size: 685705 Color: 1
Size: 312366 Color: 4

Bin 723: 1946 of cap free
Amount of items: 2
Items: 
Size: 714272 Color: 4
Size: 283783 Color: 1

Bin 724: 1949 of cap free
Amount of items: 2
Items: 
Size: 578223 Color: 2
Size: 419829 Color: 3

Bin 725: 2021 of cap free
Amount of items: 2
Items: 
Size: 506592 Color: 0
Size: 491388 Color: 2

Bin 726: 2033 of cap free
Amount of items: 2
Items: 
Size: 795473 Color: 0
Size: 202495 Color: 2

Bin 727: 2035 of cap free
Amount of items: 2
Items: 
Size: 645872 Color: 0
Size: 352094 Color: 3

Bin 728: 2037 of cap free
Amount of items: 2
Items: 
Size: 621229 Color: 2
Size: 376735 Color: 1

Bin 729: 2049 of cap free
Amount of items: 2
Items: 
Size: 701314 Color: 3
Size: 296638 Color: 0

Bin 730: 2064 of cap free
Amount of items: 2
Items: 
Size: 637929 Color: 2
Size: 360008 Color: 1

Bin 731: 2079 of cap free
Amount of items: 2
Items: 
Size: 526062 Color: 2
Size: 471860 Color: 1

Bin 732: 2111 of cap free
Amount of items: 2
Items: 
Size: 641334 Color: 4
Size: 356556 Color: 3

Bin 733: 2165 of cap free
Amount of items: 2
Items: 
Size: 731391 Color: 0
Size: 266445 Color: 3

Bin 734: 2209 of cap free
Amount of items: 2
Items: 
Size: 534270 Color: 0
Size: 463522 Color: 4

Bin 735: 2238 of cap free
Amount of items: 2
Items: 
Size: 539971 Color: 4
Size: 457792 Color: 2

Bin 736: 2257 of cap free
Amount of items: 2
Items: 
Size: 561424 Color: 4
Size: 436320 Color: 2

Bin 737: 2283 of cap free
Amount of items: 2
Items: 
Size: 625192 Color: 0
Size: 372526 Color: 3

Bin 738: 2350 of cap free
Amount of items: 2
Items: 
Size: 612622 Color: 1
Size: 385029 Color: 2

Bin 739: 2384 of cap free
Amount of items: 2
Items: 
Size: 570947 Color: 1
Size: 426670 Color: 2

Bin 740: 2387 of cap free
Amount of items: 2
Items: 
Size: 583813 Color: 4
Size: 413801 Color: 3

Bin 741: 2410 of cap free
Amount of items: 2
Items: 
Size: 625080 Color: 3
Size: 372511 Color: 0

Bin 742: 2448 of cap free
Amount of items: 2
Items: 
Size: 534152 Color: 3
Size: 463401 Color: 1

Bin 743: 2458 of cap free
Amount of items: 2
Items: 
Size: 578175 Color: 3
Size: 419368 Color: 1

Bin 744: 2510 of cap free
Amount of items: 2
Items: 
Size: 559211 Color: 1
Size: 438280 Color: 4

Bin 745: 2598 of cap free
Amount of items: 2
Items: 
Size: 500773 Color: 2
Size: 496630 Color: 0

Bin 746: 2643 of cap free
Amount of items: 2
Items: 
Size: 534018 Color: 2
Size: 463340 Color: 1

Bin 747: 2674 of cap free
Amount of items: 2
Items: 
Size: 520399 Color: 4
Size: 476928 Color: 2

Bin 748: 2707 of cap free
Amount of items: 2
Items: 
Size: 525621 Color: 0
Size: 471673 Color: 3

Bin 749: 2770 of cap free
Amount of items: 2
Items: 
Size: 500076 Color: 0
Size: 497155 Color: 2

Bin 750: 2841 of cap free
Amount of items: 2
Items: 
Size: 742418 Color: 2
Size: 254742 Color: 4

Bin 751: 2908 of cap free
Amount of items: 2
Items: 
Size: 637210 Color: 0
Size: 359883 Color: 2

Bin 752: 2936 of cap free
Amount of items: 2
Items: 
Size: 719485 Color: 4
Size: 277580 Color: 3

Bin 753: 2963 of cap free
Amount of items: 2
Items: 
Size: 511037 Color: 4
Size: 486001 Color: 1

Bin 754: 3003 of cap free
Amount of items: 2
Items: 
Size: 705419 Color: 3
Size: 291579 Color: 1

Bin 755: 3016 of cap free
Amount of items: 2
Items: 
Size: 664058 Color: 2
Size: 332927 Color: 0

Bin 756: 3062 of cap free
Amount of items: 2
Items: 
Size: 741481 Color: 3
Size: 255458 Color: 2

Bin 757: 3069 of cap free
Amount of items: 2
Items: 
Size: 499967 Color: 1
Size: 496965 Color: 2

Bin 758: 3157 of cap free
Amount of items: 2
Items: 
Size: 640509 Color: 4
Size: 356335 Color: 3

Bin 759: 3162 of cap free
Amount of items: 2
Items: 
Size: 602494 Color: 3
Size: 394345 Color: 2

Bin 760: 3244 of cap free
Amount of items: 2
Items: 
Size: 794441 Color: 0
Size: 202316 Color: 2

Bin 761: 3367 of cap free
Amount of items: 2
Items: 
Size: 500222 Color: 2
Size: 496412 Color: 3

Bin 762: 3395 of cap free
Amount of items: 2
Items: 
Size: 586683 Color: 0
Size: 409923 Color: 1

Bin 763: 3402 of cap free
Amount of items: 2
Items: 
Size: 675411 Color: 1
Size: 321188 Color: 2

Bin 764: 3429 of cap free
Amount of items: 2
Items: 
Size: 586671 Color: 3
Size: 409901 Color: 4

Bin 765: 3454 of cap free
Amount of items: 2
Items: 
Size: 729836 Color: 4
Size: 266711 Color: 0

Bin 766: 3462 of cap free
Amount of items: 2
Items: 
Size: 794294 Color: 3
Size: 202245 Color: 0

Bin 767: 3488 of cap free
Amount of items: 2
Items: 
Size: 737010 Color: 1
Size: 259503 Color: 2

Bin 768: 3557 of cap free
Amount of items: 2
Items: 
Size: 741946 Color: 2
Size: 254498 Color: 0

Bin 769: 3667 of cap free
Amount of items: 2
Items: 
Size: 719381 Color: 1
Size: 276953 Color: 4

Bin 770: 3686 of cap free
Amount of items: 2
Items: 
Size: 675352 Color: 4
Size: 320963 Color: 1

Bin 771: 3813 of cap free
Amount of items: 2
Items: 
Size: 723836 Color: 0
Size: 272352 Color: 4

Bin 772: 3850 of cap free
Amount of items: 2
Items: 
Size: 540224 Color: 2
Size: 455927 Color: 4

Bin 773: 3977 of cap free
Amount of items: 2
Items: 
Size: 573948 Color: 3
Size: 422076 Color: 1

Bin 774: 3990 of cap free
Amount of items: 2
Items: 
Size: 723784 Color: 3
Size: 272227 Color: 2

Bin 775: 4003 of cap free
Amount of items: 2
Items: 
Size: 674900 Color: 1
Size: 321098 Color: 4

Bin 776: 4083 of cap free
Amount of items: 2
Items: 
Size: 693222 Color: 4
Size: 302696 Color: 2

Bin 777: 4118 of cap free
Amount of items: 2
Items: 
Size: 549348 Color: 1
Size: 446535 Color: 0

Bin 778: 4121 of cap free
Amount of items: 2
Items: 
Size: 510653 Color: 4
Size: 485227 Color: 2

Bin 779: 4188 of cap free
Amount of items: 2
Items: 
Size: 510630 Color: 3
Size: 485183 Color: 0

Bin 780: 4347 of cap free
Amount of items: 2
Items: 
Size: 605932 Color: 2
Size: 389722 Color: 3

Bin 781: 4359 of cap free
Amount of items: 2
Items: 
Size: 699051 Color: 4
Size: 296591 Color: 3

Bin 782: 4638 of cap free
Amount of items: 2
Items: 
Size: 623725 Color: 3
Size: 371638 Color: 4

Bin 783: 4682 of cap free
Amount of items: 2
Items: 
Size: 623346 Color: 1
Size: 371973 Color: 3

Bin 784: 4692 of cap free
Amount of items: 2
Items: 
Size: 662381 Color: 3
Size: 332928 Color: 2

Bin 785: 4739 of cap free
Amount of items: 2
Items: 
Size: 674505 Color: 0
Size: 320757 Color: 1

Bin 786: 4804 of cap free
Amount of items: 2
Items: 
Size: 674280 Color: 1
Size: 320917 Color: 0

Bin 787: 4923 of cap free
Amount of items: 2
Items: 
Size: 723758 Color: 3
Size: 271320 Color: 4

Bin 788: 4949 of cap free
Amount of items: 2
Items: 
Size: 524344 Color: 0
Size: 470708 Color: 3

Bin 789: 4966 of cap free
Amount of items: 2
Items: 
Size: 692961 Color: 4
Size: 302074 Color: 2

Bin 790: 5004 of cap free
Amount of items: 2
Items: 
Size: 698590 Color: 3
Size: 296407 Color: 1

Bin 791: 5024 of cap free
Amount of items: 2
Items: 
Size: 741421 Color: 3
Size: 253556 Color: 0

Bin 792: 5097 of cap free
Amount of items: 2
Items: 
Size: 667994 Color: 4
Size: 326910 Color: 1

Bin 793: 5097 of cap free
Amount of items: 2
Items: 
Size: 735793 Color: 2
Size: 259111 Color: 3

Bin 794: 5209 of cap free
Amount of items: 2
Items: 
Size: 585076 Color: 3
Size: 409716 Color: 0

Bin 795: 5256 of cap free
Amount of items: 2
Items: 
Size: 558681 Color: 3
Size: 436064 Color: 0

Bin 796: 5351 of cap free
Amount of items: 2
Items: 
Size: 558658 Color: 4
Size: 435992 Color: 3

Bin 797: 5582 of cap free
Amount of items: 2
Items: 
Size: 558461 Color: 1
Size: 435958 Color: 4

Bin 798: 5615 of cap free
Amount of items: 2
Items: 
Size: 509221 Color: 1
Size: 485165 Color: 3

Bin 799: 5886 of cap free
Amount of items: 2
Items: 
Size: 673816 Color: 1
Size: 320299 Color: 4

Bin 800: 6437 of cap free
Amount of items: 2
Items: 
Size: 673476 Color: 2
Size: 320088 Color: 4

Bin 801: 7283 of cap free
Amount of items: 2
Items: 
Size: 698065 Color: 3
Size: 294653 Color: 0

Bin 802: 7320 of cap free
Amount of items: 2
Items: 
Size: 749390 Color: 3
Size: 243291 Color: 0

Bin 803: 7617 of cap free
Amount of items: 2
Items: 
Size: 496300 Color: 3
Size: 496084 Color: 1

Bin 804: 7737 of cap free
Amount of items: 2
Items: 
Size: 620339 Color: 2
Size: 371925 Color: 3

Bin 805: 8118 of cap free
Amount of items: 2
Items: 
Size: 620188 Color: 2
Size: 371695 Color: 3

Bin 806: 8188 of cap free
Amount of items: 2
Items: 
Size: 794191 Color: 4
Size: 197622 Color: 3

Bin 807: 8435 of cap free
Amount of items: 2
Items: 
Size: 581862 Color: 0
Size: 409704 Color: 1

Bin 808: 9084 of cap free
Amount of items: 2
Items: 
Size: 719381 Color: 4
Size: 271536 Color: 3

Bin 809: 9097 of cap free
Amount of items: 2
Items: 
Size: 495479 Color: 2
Size: 495425 Color: 3

Bin 810: 9483 of cap free
Amount of items: 2
Items: 
Size: 570744 Color: 4
Size: 419774 Color: 3

Bin 811: 9485 of cap free
Amount of items: 2
Items: 
Size: 600845 Color: 4
Size: 389671 Color: 3

Bin 812: 9637 of cap free
Amount of items: 2
Items: 
Size: 495299 Color: 3
Size: 495065 Color: 0

Bin 813: 10127 of cap free
Amount of items: 2
Items: 
Size: 600297 Color: 4
Size: 389577 Color: 2

Bin 814: 10208 of cap free
Amount of items: 2
Items: 
Size: 719356 Color: 4
Size: 270437 Color: 2

Bin 815: 10328 of cap free
Amount of items: 2
Items: 
Size: 581239 Color: 1
Size: 408434 Color: 3

Bin 816: 10376 of cap free
Amount of items: 2
Items: 
Size: 600104 Color: 4
Size: 389521 Color: 2

Bin 817: 10401 of cap free
Amount of items: 2
Items: 
Size: 618005 Color: 3
Size: 371595 Color: 0

Bin 818: 10715 of cap free
Amount of items: 2
Items: 
Size: 599943 Color: 4
Size: 389343 Color: 0

Bin 819: 10810 of cap free
Amount of items: 2
Items: 
Size: 662352 Color: 3
Size: 326839 Color: 1

Bin 820: 11097 of cap free
Amount of items: 2
Items: 
Size: 662325 Color: 0
Size: 326579 Color: 4

Bin 821: 11417 of cap free
Amount of items: 2
Items: 
Size: 617926 Color: 4
Size: 370658 Color: 3

Bin 822: 11599 of cap free
Amount of items: 2
Items: 
Size: 599837 Color: 2
Size: 388565 Color: 4

Bin 823: 11941 of cap free
Amount of items: 2
Items: 
Size: 495036 Color: 4
Size: 493024 Color: 0

Bin 824: 12095 of cap free
Amount of items: 2
Items: 
Size: 719221 Color: 3
Size: 268685 Color: 4

Bin 825: 12646 of cap free
Amount of items: 2
Items: 
Size: 617597 Color: 2
Size: 369758 Color: 4

Bin 826: 12877 of cap free
Amount of items: 2
Items: 
Size: 597920 Color: 4
Size: 389204 Color: 2

Bin 827: 14502 of cap free
Amount of items: 2
Items: 
Size: 597411 Color: 2
Size: 388088 Color: 1

Bin 828: 15680 of cap free
Amount of items: 2
Items: 
Size: 741084 Color: 0
Size: 243237 Color: 3

Bin 829: 16398 of cap free
Amount of items: 2
Items: 
Size: 691025 Color: 2
Size: 292578 Color: 3

Bin 830: 16991 of cap free
Amount of items: 2
Items: 
Size: 690796 Color: 0
Size: 292214 Color: 3

Bin 831: 17571 of cap free
Amount of items: 2
Items: 
Size: 597365 Color: 2
Size: 385065 Color: 1

Bin 832: 17895 of cap free
Amount of items: 2
Items: 
Size: 691113 Color: 3
Size: 290993 Color: 2

Bin 833: 18540 of cap free
Amount of items: 2
Items: 
Size: 662287 Color: 3
Size: 319174 Color: 4

Bin 834: 18841 of cap free
Amount of items: 2
Items: 
Size: 662034 Color: 3
Size: 319126 Color: 0

Bin 835: 19369 of cap free
Amount of items: 2
Items: 
Size: 661661 Color: 1
Size: 318971 Color: 0

Bin 836: 19415 of cap free
Amount of items: 2
Items: 
Size: 690763 Color: 4
Size: 289823 Color: 3

Bin 837: 19816 of cap free
Amount of items: 2
Items: 
Size: 690629 Color: 1
Size: 289556 Color: 0

Bin 838: 20176 of cap free
Amount of items: 2
Items: 
Size: 690478 Color: 0
Size: 289347 Color: 4

Bin 839: 20647 of cap free
Amount of items: 2
Items: 
Size: 736184 Color: 3
Size: 243170 Color: 2

Bin 840: 21034 of cap free
Amount of items: 2
Items: 
Size: 595095 Color: 2
Size: 383872 Color: 4

Bin 841: 21250 of cap free
Amount of items: 2
Items: 
Size: 689843 Color: 4
Size: 288908 Color: 0

Bin 842: 21369 of cap free
Amount of items: 2
Items: 
Size: 594766 Color: 2
Size: 383866 Color: 3

Bin 843: 22041 of cap free
Amount of items: 2
Items: 
Size: 734957 Color: 4
Size: 243003 Color: 1

Bin 844: 22137 of cap free
Amount of items: 2
Items: 
Size: 689014 Color: 1
Size: 288850 Color: 0

Bin 845: 22144 of cap free
Amount of items: 2
Items: 
Size: 570629 Color: 3
Size: 407228 Color: 4

Bin 846: 22146 of cap free
Amount of items: 2
Items: 
Size: 734909 Color: 0
Size: 242946 Color: 1

Bin 847: 22235 of cap free
Amount of items: 2
Items: 
Size: 570560 Color: 1
Size: 407206 Color: 4

Bin 848: 22266 of cap free
Amount of items: 2
Items: 
Size: 688923 Color: 4
Size: 288812 Color: 2

Bin 849: 22306 of cap free
Amount of items: 2
Items: 
Size: 659358 Color: 2
Size: 318337 Color: 3

Bin 850: 22517 of cap free
Amount of items: 2
Items: 
Size: 688912 Color: 2
Size: 288572 Color: 0

Bin 851: 22679 of cap free
Amount of items: 2
Items: 
Size: 659280 Color: 0
Size: 318042 Color: 1

Bin 852: 23043 of cap free
Amount of items: 2
Items: 
Size: 688768 Color: 4
Size: 288190 Color: 2

Bin 853: 23351 of cap free
Amount of items: 2
Items: 
Size: 734496 Color: 0
Size: 242154 Color: 4

Bin 854: 23631 of cap free
Amount of items: 2
Items: 
Size: 658394 Color: 2
Size: 317976 Color: 4

Bin 855: 23647 of cap free
Amount of items: 2
Items: 
Size: 688235 Color: 2
Size: 288119 Color: 4

Bin 856: 24012 of cap free
Amount of items: 2
Items: 
Size: 687985 Color: 2
Size: 288004 Color: 0

Bin 857: 24071 of cap free
Amount of items: 2
Items: 
Size: 490978 Color: 3
Size: 484952 Color: 0

Bin 858: 24107 of cap free
Amount of items: 2
Items: 
Size: 658276 Color: 1
Size: 317618 Color: 2

Bin 859: 24659 of cap free
Amount of items: 2
Items: 
Size: 733772 Color: 0
Size: 241570 Color: 4

Bin 860: 24732 of cap free
Amount of items: 2
Items: 
Size: 733585 Color: 2
Size: 241684 Color: 0

Bin 861: 24860 of cap free
Amount of items: 2
Items: 
Size: 658340 Color: 2
Size: 316801 Color: 4

Bin 862: 25705 of cap free
Amount of items: 2
Items: 
Size: 570037 Color: 4
Size: 404259 Color: 2

Bin 863: 26431 of cap free
Amount of items: 2
Items: 
Size: 657654 Color: 2
Size: 315916 Color: 4

Bin 864: 32100 of cap free
Amount of items: 2
Items: 
Size: 484208 Color: 0
Size: 483693 Color: 4

Bin 865: 36003 of cap free
Amount of items: 2
Items: 
Size: 482148 Color: 1
Size: 481850 Color: 0

Bin 866: 40183 of cap free
Amount of items: 2
Items: 
Size: 718815 Color: 0
Size: 241003 Color: 1

Bin 867: 40642 of cap free
Amount of items: 2
Items: 
Size: 718681 Color: 2
Size: 240678 Color: 1

Bin 868: 41562 of cap free
Amount of items: 2
Items: 
Size: 569250 Color: 1
Size: 389189 Color: 2

Bin 869: 42363 of cap free
Amount of items: 2
Items: 
Size: 718005 Color: 4
Size: 239633 Color: 1

Bin 870: 43486 of cap free
Amount of items: 2
Items: 
Size: 718066 Color: 1
Size: 238449 Color: 4

Bin 871: 48049 of cap free
Amount of items: 2
Items: 
Size: 476227 Color: 1
Size: 475725 Color: 4

Bin 872: 50382 of cap free
Amount of items: 3
Items: 
Size: 383730 Color: 1
Size: 283537 Color: 2
Size: 282352 Color: 2

Bin 873: 57529 of cap free
Amount of items: 2
Items: 
Size: 657521 Color: 1
Size: 284951 Color: 4

Bin 874: 58241 of cap free
Amount of items: 2
Items: 
Size: 657317 Color: 2
Size: 284443 Color: 4

Bin 875: 61026 of cap free
Amount of items: 2
Items: 
Size: 470261 Color: 3
Size: 468714 Color: 2

Bin 876: 67812 of cap free
Amount of items: 2
Items: 
Size: 466116 Color: 2
Size: 466073 Color: 4

Bin 877: 73423 of cap free
Amount of items: 2
Items: 
Size: 463335 Color: 1
Size: 463243 Color: 3

Bin 878: 75543 of cap free
Amount of items: 2
Items: 
Size: 462883 Color: 4
Size: 461575 Color: 3

Bin 879: 81797 of cap free
Amount of items: 2
Items: 
Size: 460715 Color: 0
Size: 457489 Color: 2

Bin 880: 88184 of cap free
Amount of items: 2
Items: 
Size: 456064 Color: 2
Size: 455753 Color: 1

Bin 881: 92462 of cap free
Amount of items: 2
Items: 
Size: 454705 Color: 0
Size: 452834 Color: 1

Bin 882: 93352 of cap free
Amount of items: 2
Items: 
Size: 454691 Color: 0
Size: 451958 Color: 4

Bin 883: 97808 of cap free
Amount of items: 2
Items: 
Size: 451128 Color: 3
Size: 451065 Color: 0

Bin 884: 105294 of cap free
Amount of items: 2
Items: 
Size: 449906 Color: 1
Size: 444801 Color: 0

Bin 885: 113506 of cap free
Amount of items: 2
Items: 
Size: 443999 Color: 0
Size: 442496 Color: 4

Bin 886: 113794 of cap free
Amount of items: 2
Items: 
Size: 443744 Color: 0
Size: 442463 Color: 4

Bin 887: 116302 of cap free
Amount of items: 2
Items: 
Size: 442061 Color: 0
Size: 441638 Color: 3

Bin 888: 129434 of cap free
Amount of items: 2
Items: 
Size: 435429 Color: 4
Size: 435138 Color: 2

Bin 889: 130079 of cap free
Amount of items: 2
Items: 
Size: 435078 Color: 1
Size: 434844 Color: 0

Bin 890: 132896 of cap free
Amount of items: 2
Items: 
Size: 434770 Color: 2
Size: 432335 Color: 0

Bin 891: 136229 of cap free
Amount of items: 2
Items: 
Size: 432259 Color: 2
Size: 431513 Color: 1

Bin 892: 234537 of cap free
Amount of items: 2
Items: 
Size: 382846 Color: 1
Size: 382618 Color: 0

Bin 893: 248024 of cap free
Amount of items: 2
Items: 
Size: 382384 Color: 2
Size: 369593 Color: 4

Bin 894: 260973 of cap free
Amount of items: 2
Items: 
Size: 370209 Color: 2
Size: 368819 Color: 4

Bin 895: 264017 of cap free
Amount of items: 2
Items: 
Size: 368458 Color: 0
Size: 367526 Color: 1

Bin 896: 264239 of cap free
Amount of items: 2
Items: 
Size: 368358 Color: 0
Size: 367404 Color: 1

Bin 897: 266864 of cap free
Amount of items: 2
Items: 
Size: 366758 Color: 1
Size: 366379 Color: 3

Bin 898: 267929 of cap free
Amount of items: 2
Items: 
Size: 366073 Color: 2
Size: 365999 Color: 4

Bin 899: 268145 of cap free
Amount of items: 2
Items: 
Size: 365989 Color: 4
Size: 365867 Color: 1

Bin 900: 269894 of cap free
Amount of items: 2
Items: 
Size: 365244 Color: 1
Size: 364863 Color: 0

Bin 901: 272116 of cap free
Amount of items: 2
Items: 
Size: 363951 Color: 1
Size: 363934 Color: 3

Bin 902: 273080 of cap free
Amount of items: 2
Items: 
Size: 363882 Color: 1
Size: 363039 Color: 3

Bin 903: 273431 of cap free
Amount of items: 2
Items: 
Size: 363750 Color: 1
Size: 362820 Color: 2

Bin 904: 284233 of cap free
Amount of items: 2
Items: 
Size: 360008 Color: 0
Size: 355760 Color: 3

Bin 905: 289565 of cap free
Amount of items: 2
Items: 
Size: 355622 Color: 3
Size: 354814 Color: 0

Bin 906: 293856 of cap free
Amount of items: 2
Items: 
Size: 354277 Color: 1
Size: 351868 Color: 0

Bin 907: 296634 of cap free
Amount of items: 2
Items: 
Size: 351833 Color: 1
Size: 351534 Color: 4

Bin 908: 298820 of cap free
Amount of items: 2
Items: 
Size: 351242 Color: 2
Size: 349939 Color: 3

Bin 909: 303252 of cap free
Amount of items: 2
Items: 
Size: 349867 Color: 4
Size: 346882 Color: 2

Bin 910: 306608 of cap free
Amount of items: 2
Items: 
Size: 346851 Color: 0
Size: 346542 Color: 1

Bin 911: 307594 of cap free
Amount of items: 2
Items: 
Size: 346840 Color: 0
Size: 345567 Color: 4

Bin 912: 309237 of cap free
Amount of items: 2
Items: 
Size: 345477 Color: 4
Size: 345287 Color: 3

Bin 913: 309857 of cap free
Amount of items: 2
Items: 
Size: 345236 Color: 4
Size: 344908 Color: 3

Bin 914: 310757 of cap free
Amount of items: 2
Items: 
Size: 345068 Color: 4
Size: 344176 Color: 3

Bin 915: 312942 of cap free
Amount of items: 2
Items: 
Size: 343651 Color: 0
Size: 343408 Color: 4

Bin 916: 657131 of cap free
Amount of items: 1
Items: 
Size: 342870 Color: 3

Bin 917: 657258 of cap free
Amount of items: 1
Items: 
Size: 342743 Color: 3

Total size: 905097039
Total free space: 11903878

