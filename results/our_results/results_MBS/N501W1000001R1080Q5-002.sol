Capicity Bin: 1000001
Lower Bound: 227

Bins used: 229
Amount of Colors: 5

Bin 1: 2 of cap free
Amount of items: 2
Items: 
Size: 797120 Color: 4
Size: 202879 Color: 3

Bin 2: 7 of cap free
Amount of items: 3
Items: 
Size: 764330 Color: 1
Size: 121689 Color: 4
Size: 113975 Color: 3

Bin 3: 9 of cap free
Amount of items: 2
Items: 
Size: 742368 Color: 1
Size: 257624 Color: 3

Bin 4: 15 of cap free
Amount of items: 3
Items: 
Size: 790001 Color: 4
Size: 106410 Color: 1
Size: 103575 Color: 1

Bin 5: 23 of cap free
Amount of items: 2
Items: 
Size: 632941 Color: 4
Size: 367037 Color: 2

Bin 6: 30 of cap free
Amount of items: 2
Items: 
Size: 713246 Color: 1
Size: 286725 Color: 3

Bin 7: 34 of cap free
Amount of items: 3
Items: 
Size: 607315 Color: 1
Size: 202097 Color: 2
Size: 190555 Color: 2

Bin 8: 41 of cap free
Amount of items: 2
Items: 
Size: 548131 Color: 1
Size: 451829 Color: 3

Bin 9: 45 of cap free
Amount of items: 3
Items: 
Size: 672735 Color: 3
Size: 166309 Color: 2
Size: 160912 Color: 2

Bin 10: 54 of cap free
Amount of items: 2
Items: 
Size: 588452 Color: 3
Size: 411495 Color: 2

Bin 11: 55 of cap free
Amount of items: 2
Items: 
Size: 609957 Color: 4
Size: 389989 Color: 0

Bin 12: 55 of cap free
Amount of items: 2
Items: 
Size: 758303 Color: 0
Size: 241643 Color: 1

Bin 13: 61 of cap free
Amount of items: 2
Items: 
Size: 699750 Color: 1
Size: 300190 Color: 0

Bin 14: 62 of cap free
Amount of items: 2
Items: 
Size: 549850 Color: 4
Size: 450089 Color: 2

Bin 15: 63 of cap free
Amount of items: 3
Items: 
Size: 613315 Color: 1
Size: 194685 Color: 3
Size: 191938 Color: 4

Bin 16: 65 of cap free
Amount of items: 2
Items: 
Size: 792981 Color: 1
Size: 206955 Color: 4

Bin 17: 72 of cap free
Amount of items: 3
Items: 
Size: 627845 Color: 3
Size: 189217 Color: 4
Size: 182867 Color: 3

Bin 18: 75 of cap free
Amount of items: 2
Items: 
Size: 527916 Color: 1
Size: 472010 Color: 3

Bin 19: 79 of cap free
Amount of items: 3
Items: 
Size: 693163 Color: 1
Size: 155535 Color: 3
Size: 151224 Color: 2

Bin 20: 93 of cap free
Amount of items: 2
Items: 
Size: 637997 Color: 2
Size: 361911 Color: 4

Bin 21: 109 of cap free
Amount of items: 3
Items: 
Size: 577917 Color: 0
Size: 213140 Color: 2
Size: 208835 Color: 1

Bin 22: 114 of cap free
Amount of items: 3
Items: 
Size: 685843 Color: 2
Size: 160074 Color: 1
Size: 153970 Color: 3

Bin 23: 122 of cap free
Amount of items: 2
Items: 
Size: 571714 Color: 3
Size: 428165 Color: 1

Bin 24: 133 of cap free
Amount of items: 2
Items: 
Size: 799932 Color: 0
Size: 199936 Color: 2

Bin 25: 136 of cap free
Amount of items: 3
Items: 
Size: 684806 Color: 4
Size: 165496 Color: 2
Size: 149563 Color: 2

Bin 26: 137 of cap free
Amount of items: 3
Items: 
Size: 742636 Color: 1
Size: 130570 Color: 4
Size: 126658 Color: 2

Bin 27: 146 of cap free
Amount of items: 2
Items: 
Size: 518464 Color: 2
Size: 481391 Color: 4

Bin 28: 147 of cap free
Amount of items: 2
Items: 
Size: 677965 Color: 0
Size: 321889 Color: 3

Bin 29: 153 of cap free
Amount of items: 2
Items: 
Size: 509202 Color: 3
Size: 490646 Color: 2

Bin 30: 169 of cap free
Amount of items: 2
Items: 
Size: 656416 Color: 4
Size: 343416 Color: 1

Bin 31: 171 of cap free
Amount of items: 2
Items: 
Size: 501885 Color: 2
Size: 497945 Color: 0

Bin 32: 181 of cap free
Amount of items: 2
Items: 
Size: 700743 Color: 2
Size: 299077 Color: 4

Bin 33: 182 of cap free
Amount of items: 3
Items: 
Size: 638359 Color: 2
Size: 182546 Color: 4
Size: 178914 Color: 3

Bin 34: 191 of cap free
Amount of items: 2
Items: 
Size: 725445 Color: 4
Size: 274365 Color: 1

Bin 35: 193 of cap free
Amount of items: 2
Items: 
Size: 726549 Color: 2
Size: 273259 Color: 0

Bin 36: 195 of cap free
Amount of items: 3
Items: 
Size: 637098 Color: 4
Size: 182359 Color: 2
Size: 180349 Color: 4

Bin 37: 196 of cap free
Amount of items: 2
Items: 
Size: 594985 Color: 0
Size: 404820 Color: 1

Bin 38: 198 of cap free
Amount of items: 3
Items: 
Size: 765728 Color: 2
Size: 119340 Color: 3
Size: 114735 Color: 2

Bin 39: 204 of cap free
Amount of items: 2
Items: 
Size: 653833 Color: 1
Size: 345964 Color: 3

Bin 40: 207 of cap free
Amount of items: 3
Items: 
Size: 686333 Color: 4
Size: 160171 Color: 2
Size: 153290 Color: 2

Bin 41: 223 of cap free
Amount of items: 3
Items: 
Size: 774636 Color: 1
Size: 113653 Color: 4
Size: 111489 Color: 3

Bin 42: 233 of cap free
Amount of items: 2
Items: 
Size: 704521 Color: 3
Size: 295247 Color: 2

Bin 43: 234 of cap free
Amount of items: 2
Items: 
Size: 562036 Color: 2
Size: 437731 Color: 0

Bin 44: 237 of cap free
Amount of items: 2
Items: 
Size: 543834 Color: 4
Size: 455930 Color: 0

Bin 45: 248 of cap free
Amount of items: 3
Items: 
Size: 651203 Color: 4
Size: 177559 Color: 3
Size: 170991 Color: 2

Bin 46: 250 of cap free
Amount of items: 2
Items: 
Size: 609091 Color: 4
Size: 390660 Color: 1

Bin 47: 260 of cap free
Amount of items: 2
Items: 
Size: 541246 Color: 2
Size: 458495 Color: 3

Bin 48: 267 of cap free
Amount of items: 3
Items: 
Size: 736297 Color: 3
Size: 136969 Color: 2
Size: 126468 Color: 3

Bin 49: 273 of cap free
Amount of items: 2
Items: 
Size: 678609 Color: 0
Size: 321119 Color: 1

Bin 50: 276 of cap free
Amount of items: 3
Items: 
Size: 738193 Color: 1
Size: 138046 Color: 2
Size: 123486 Color: 4

Bin 51: 300 of cap free
Amount of items: 2
Items: 
Size: 509659 Color: 3
Size: 490042 Color: 4

Bin 52: 301 of cap free
Amount of items: 2
Items: 
Size: 545329 Color: 3
Size: 454371 Color: 4

Bin 53: 306 of cap free
Amount of items: 2
Items: 
Size: 792086 Color: 1
Size: 207609 Color: 0

Bin 54: 309 of cap free
Amount of items: 3
Items: 
Size: 648983 Color: 4
Size: 179179 Color: 1
Size: 171530 Color: 1

Bin 55: 346 of cap free
Amount of items: 2
Items: 
Size: 673095 Color: 0
Size: 326560 Color: 1

Bin 56: 351 of cap free
Amount of items: 2
Items: 
Size: 677926 Color: 2
Size: 321724 Color: 3

Bin 57: 352 of cap free
Amount of items: 2
Items: 
Size: 680541 Color: 4
Size: 319108 Color: 3

Bin 58: 357 of cap free
Amount of items: 2
Items: 
Size: 652696 Color: 3
Size: 346948 Color: 2

Bin 59: 361 of cap free
Amount of items: 2
Items: 
Size: 767991 Color: 2
Size: 231649 Color: 3

Bin 60: 363 of cap free
Amount of items: 2
Items: 
Size: 549030 Color: 3
Size: 450608 Color: 0

Bin 61: 363 of cap free
Amount of items: 2
Items: 
Size: 605982 Color: 4
Size: 393656 Color: 0

Bin 62: 371 of cap free
Amount of items: 2
Items: 
Size: 540313 Color: 0
Size: 459317 Color: 1

Bin 63: 422 of cap free
Amount of items: 3
Items: 
Size: 694171 Color: 3
Size: 154730 Color: 0
Size: 150678 Color: 1

Bin 64: 426 of cap free
Amount of items: 2
Items: 
Size: 759502 Color: 1
Size: 240073 Color: 3

Bin 65: 433 of cap free
Amount of items: 3
Items: 
Size: 690855 Color: 1
Size: 156738 Color: 4
Size: 151975 Color: 2

Bin 66: 434 of cap free
Amount of items: 2
Items: 
Size: 519338 Color: 0
Size: 480229 Color: 4

Bin 67: 436 of cap free
Amount of items: 2
Items: 
Size: 698971 Color: 2
Size: 300594 Color: 1

Bin 68: 462 of cap free
Amount of items: 3
Items: 
Size: 639435 Color: 3
Size: 181119 Color: 4
Size: 178985 Color: 0

Bin 69: 472 of cap free
Amount of items: 2
Items: 
Size: 514740 Color: 0
Size: 484789 Color: 4

Bin 70: 472 of cap free
Amount of items: 3
Items: 
Size: 771913 Color: 1
Size: 114926 Color: 3
Size: 112690 Color: 0

Bin 71: 483 of cap free
Amount of items: 2
Items: 
Size: 682049 Color: 3
Size: 317469 Color: 1

Bin 72: 491 of cap free
Amount of items: 2
Items: 
Size: 573849 Color: 3
Size: 425661 Color: 2

Bin 73: 496 of cap free
Amount of items: 2
Items: 
Size: 779215 Color: 2
Size: 220290 Color: 3

Bin 74: 508 of cap free
Amount of items: 2
Items: 
Size: 783543 Color: 1
Size: 215950 Color: 3

Bin 75: 522 of cap free
Amount of items: 2
Items: 
Size: 671498 Color: 0
Size: 327981 Color: 1

Bin 76: 525 of cap free
Amount of items: 2
Items: 
Size: 524850 Color: 1
Size: 474626 Color: 3

Bin 77: 549 of cap free
Amount of items: 3
Items: 
Size: 757457 Color: 2
Size: 122760 Color: 0
Size: 119235 Color: 1

Bin 78: 566 of cap free
Amount of items: 3
Items: 
Size: 664907 Color: 0
Size: 167701 Color: 2
Size: 166827 Color: 0

Bin 79: 568 of cap free
Amount of items: 2
Items: 
Size: 597626 Color: 4
Size: 401807 Color: 3

Bin 80: 579 of cap free
Amount of items: 2
Items: 
Size: 547812 Color: 3
Size: 451610 Color: 2

Bin 81: 590 of cap free
Amount of items: 2
Items: 
Size: 794113 Color: 4
Size: 205298 Color: 0

Bin 82: 597 of cap free
Amount of items: 2
Items: 
Size: 636722 Color: 1
Size: 362682 Color: 3

Bin 83: 622 of cap free
Amount of items: 2
Items: 
Size: 603798 Color: 2
Size: 395581 Color: 4

Bin 84: 640 of cap free
Amount of items: 2
Items: 
Size: 728524 Color: 1
Size: 270837 Color: 3

Bin 85: 644 of cap free
Amount of items: 2
Items: 
Size: 653450 Color: 2
Size: 345907 Color: 0

Bin 86: 650 of cap free
Amount of items: 3
Items: 
Size: 783291 Color: 3
Size: 111705 Color: 1
Size: 104355 Color: 0

Bin 87: 659 of cap free
Amount of items: 2
Items: 
Size: 604994 Color: 1
Size: 394348 Color: 3

Bin 88: 693 of cap free
Amount of items: 2
Items: 
Size: 669686 Color: 3
Size: 329622 Color: 2

Bin 89: 703 of cap free
Amount of items: 3
Items: 
Size: 508257 Color: 1
Size: 248662 Color: 0
Size: 242379 Color: 2

Bin 90: 715 of cap free
Amount of items: 2
Items: 
Size: 730750 Color: 2
Size: 268536 Color: 3

Bin 91: 715 of cap free
Amount of items: 2
Items: 
Size: 782089 Color: 1
Size: 217197 Color: 2

Bin 92: 729 of cap free
Amount of items: 3
Items: 
Size: 748580 Color: 3
Size: 125789 Color: 4
Size: 124903 Color: 0

Bin 93: 739 of cap free
Amount of items: 2
Items: 
Size: 659502 Color: 1
Size: 339760 Color: 0

Bin 94: 753 of cap free
Amount of items: 2
Items: 
Size: 549020 Color: 2
Size: 450228 Color: 1

Bin 95: 754 of cap free
Amount of items: 2
Items: 
Size: 701372 Color: 1
Size: 297875 Color: 4

Bin 96: 755 of cap free
Amount of items: 3
Items: 
Size: 581318 Color: 4
Size: 214493 Color: 3
Size: 203435 Color: 2

Bin 97: 789 of cap free
Amount of items: 2
Items: 
Size: 741161 Color: 0
Size: 258051 Color: 3

Bin 98: 819 of cap free
Amount of items: 2
Items: 
Size: 602763 Color: 3
Size: 396419 Color: 0

Bin 99: 822 of cap free
Amount of items: 3
Items: 
Size: 512544 Color: 1
Size: 243535 Color: 0
Size: 243100 Color: 3

Bin 100: 824 of cap free
Amount of items: 3
Items: 
Size: 763471 Color: 1
Size: 121000 Color: 2
Size: 114706 Color: 2

Bin 101: 843 of cap free
Amount of items: 2
Items: 
Size: 502940 Color: 4
Size: 496218 Color: 2

Bin 102: 850 of cap free
Amount of items: 2
Items: 
Size: 606173 Color: 0
Size: 392978 Color: 3

Bin 103: 850 of cap free
Amount of items: 2
Items: 
Size: 722282 Color: 2
Size: 276869 Color: 4

Bin 104: 867 of cap free
Amount of items: 3
Items: 
Size: 604868 Color: 4
Size: 197719 Color: 3
Size: 196547 Color: 0

Bin 105: 870 of cap free
Amount of items: 2
Items: 
Size: 557859 Color: 3
Size: 441272 Color: 0

Bin 106: 928 of cap free
Amount of items: 2
Items: 
Size: 506152 Color: 1
Size: 492921 Color: 3

Bin 107: 968 of cap free
Amount of items: 2
Items: 
Size: 793053 Color: 3
Size: 205980 Color: 0

Bin 108: 980 of cap free
Amount of items: 2
Items: 
Size: 623830 Color: 2
Size: 375191 Color: 4

Bin 109: 992 of cap free
Amount of items: 2
Items: 
Size: 795448 Color: 2
Size: 203561 Color: 4

Bin 110: 1002 of cap free
Amount of items: 2
Items: 
Size: 792096 Color: 0
Size: 206903 Color: 3

Bin 111: 1004 of cap free
Amount of items: 2
Items: 
Size: 510741 Color: 4
Size: 488256 Color: 3

Bin 112: 1039 of cap free
Amount of items: 3
Items: 
Size: 666146 Color: 0
Size: 166466 Color: 3
Size: 166350 Color: 3

Bin 113: 1066 of cap free
Amount of items: 2
Items: 
Size: 657609 Color: 0
Size: 341326 Color: 1

Bin 114: 1102 of cap free
Amount of items: 3
Items: 
Size: 618769 Color: 2
Size: 193138 Color: 1
Size: 186992 Color: 1

Bin 115: 1119 of cap free
Amount of items: 2
Items: 
Size: 550042 Color: 3
Size: 448840 Color: 4

Bin 116: 1125 of cap free
Amount of items: 2
Items: 
Size: 644475 Color: 1
Size: 354401 Color: 2

Bin 117: 1130 of cap free
Amount of items: 3
Items: 
Size: 762231 Color: 3
Size: 120879 Color: 0
Size: 115761 Color: 3

Bin 118: 1137 of cap free
Amount of items: 2
Items: 
Size: 787553 Color: 4
Size: 211311 Color: 2

Bin 119: 1141 of cap free
Amount of items: 2
Items: 
Size: 706870 Color: 3
Size: 291990 Color: 4

Bin 120: 1201 of cap free
Amount of items: 2
Items: 
Size: 508701 Color: 0
Size: 490099 Color: 3

Bin 121: 1210 of cap free
Amount of items: 3
Items: 
Size: 716233 Color: 4
Size: 144558 Color: 3
Size: 138000 Color: 2

Bin 122: 1216 of cap free
Amount of items: 2
Items: 
Size: 627345 Color: 0
Size: 371440 Color: 2

Bin 123: 1257 of cap free
Amount of items: 2
Items: 
Size: 742503 Color: 1
Size: 256241 Color: 4

Bin 124: 1385 of cap free
Amount of items: 2
Items: 
Size: 720050 Color: 4
Size: 278566 Color: 0

Bin 125: 1413 of cap free
Amount of items: 2
Items: 
Size: 588794 Color: 4
Size: 409794 Color: 3

Bin 126: 1432 of cap free
Amount of items: 2
Items: 
Size: 544623 Color: 1
Size: 453946 Color: 2

Bin 127: 1485 of cap free
Amount of items: 3
Items: 
Size: 598262 Color: 4
Size: 200555 Color: 3
Size: 199699 Color: 1

Bin 128: 1498 of cap free
Amount of items: 2
Items: 
Size: 591283 Color: 2
Size: 407220 Color: 3

Bin 129: 1521 of cap free
Amount of items: 3
Items: 
Size: 712268 Color: 2
Size: 146410 Color: 4
Size: 139802 Color: 4

Bin 130: 1529 of cap free
Amount of items: 2
Items: 
Size: 708211 Color: 3
Size: 290261 Color: 2

Bin 131: 1529 of cap free
Amount of items: 2
Items: 
Size: 726010 Color: 1
Size: 272462 Color: 2

Bin 132: 1562 of cap free
Amount of items: 2
Items: 
Size: 752441 Color: 4
Size: 245998 Color: 0

Bin 133: 1572 of cap free
Amount of items: 2
Items: 
Size: 686173 Color: 2
Size: 312256 Color: 0

Bin 134: 1578 of cap free
Amount of items: 2
Items: 
Size: 634658 Color: 4
Size: 363765 Color: 1

Bin 135: 1652 of cap free
Amount of items: 2
Items: 
Size: 641155 Color: 0
Size: 357194 Color: 3

Bin 136: 1678 of cap free
Amount of items: 2
Items: 
Size: 682366 Color: 1
Size: 315957 Color: 4

Bin 137: 1827 of cap free
Amount of items: 2
Items: 
Size: 500856 Color: 4
Size: 497318 Color: 1

Bin 138: 1856 of cap free
Amount of items: 2
Items: 
Size: 674747 Color: 3
Size: 323398 Color: 0

Bin 139: 1882 of cap free
Amount of items: 2
Items: 
Size: 778893 Color: 1
Size: 219226 Color: 0

Bin 140: 1898 of cap free
Amount of items: 2
Items: 
Size: 588389 Color: 2
Size: 409714 Color: 3

Bin 141: 2097 of cap free
Amount of items: 3
Items: 
Size: 644398 Color: 4
Size: 176805 Color: 1
Size: 176701 Color: 2

Bin 142: 2179 of cap free
Amount of items: 2
Items: 
Size: 731785 Color: 1
Size: 266037 Color: 2

Bin 143: 2193 of cap free
Amount of items: 2
Items: 
Size: 663622 Color: 0
Size: 334186 Color: 2

Bin 144: 2305 of cap free
Amount of items: 2
Items: 
Size: 505165 Color: 4
Size: 492531 Color: 0

Bin 145: 2438 of cap free
Amount of items: 2
Items: 
Size: 702453 Color: 4
Size: 295110 Color: 3

Bin 146: 2445 of cap free
Amount of items: 2
Items: 
Size: 580105 Color: 1
Size: 417451 Color: 2

Bin 147: 2484 of cap free
Amount of items: 2
Items: 
Size: 625867 Color: 1
Size: 371650 Color: 0

Bin 148: 2562 of cap free
Amount of items: 2
Items: 
Size: 605940 Color: 0
Size: 391499 Color: 1

Bin 149: 2570 of cap free
Amount of items: 2
Items: 
Size: 617391 Color: 4
Size: 380040 Color: 0

Bin 150: 2603 of cap free
Amount of items: 2
Items: 
Size: 674424 Color: 4
Size: 322974 Color: 3

Bin 151: 2824 of cap free
Amount of items: 2
Items: 
Size: 631693 Color: 3
Size: 365484 Color: 1

Bin 152: 2834 of cap free
Amount of items: 2
Items: 
Size: 717792 Color: 1
Size: 279375 Color: 4

Bin 153: 2841 of cap free
Amount of items: 2
Items: 
Size: 653436 Color: 3
Size: 343724 Color: 4

Bin 154: 2857 of cap free
Amount of items: 2
Items: 
Size: 523463 Color: 0
Size: 473681 Color: 1

Bin 155: 3012 of cap free
Amount of items: 2
Items: 
Size: 771353 Color: 1
Size: 225636 Color: 3

Bin 156: 3090 of cap free
Amount of items: 3
Items: 
Size: 740799 Color: 1
Size: 128915 Color: 2
Size: 127197 Color: 1

Bin 157: 3244 of cap free
Amount of items: 2
Items: 
Size: 583916 Color: 2
Size: 412841 Color: 4

Bin 158: 3258 of cap free
Amount of items: 2
Items: 
Size: 568702 Color: 4
Size: 428041 Color: 2

Bin 159: 3367 of cap free
Amount of items: 2
Items: 
Size: 588320 Color: 0
Size: 408314 Color: 2

Bin 160: 3592 of cap free
Amount of items: 2
Items: 
Size: 653448 Color: 4
Size: 342961 Color: 3

Bin 161: 3613 of cap free
Amount of items: 2
Items: 
Size: 625617 Color: 2
Size: 370771 Color: 0

Bin 162: 3697 of cap free
Amount of items: 2
Items: 
Size: 706335 Color: 2
Size: 289969 Color: 3

Bin 163: 3834 of cap free
Amount of items: 2
Items: 
Size: 674087 Color: 4
Size: 322080 Color: 0

Bin 164: 3837 of cap free
Amount of items: 2
Items: 
Size: 625387 Color: 0
Size: 370777 Color: 2

Bin 165: 3986 of cap free
Amount of items: 2
Items: 
Size: 724008 Color: 2
Size: 272007 Color: 3

Bin 166: 4324 of cap free
Amount of items: 2
Items: 
Size: 523325 Color: 3
Size: 472352 Color: 1

Bin 167: 4505 of cap free
Amount of items: 2
Items: 
Size: 648694 Color: 1
Size: 346802 Color: 0

Bin 168: 4546 of cap free
Amount of items: 2
Items: 
Size: 737717 Color: 0
Size: 257738 Color: 1

Bin 169: 4675 of cap free
Amount of items: 2
Items: 
Size: 524032 Color: 1
Size: 471294 Color: 3

Bin 170: 4983 of cap free
Amount of items: 2
Items: 
Size: 705760 Color: 0
Size: 289258 Color: 2

Bin 171: 5596 of cap free
Amount of items: 2
Items: 
Size: 663910 Color: 2
Size: 330495 Color: 1

Bin 172: 6369 of cap free
Amount of items: 2
Items: 
Size: 602156 Color: 0
Size: 391476 Color: 1

Bin 173: 6387 of cap free
Amount of items: 2
Items: 
Size: 647009 Color: 2
Size: 346605 Color: 4

Bin 174: 6413 of cap free
Amount of items: 2
Items: 
Size: 778206 Color: 1
Size: 215382 Color: 2

Bin 175: 6578 of cap free
Amount of items: 2
Items: 
Size: 522325 Color: 1
Size: 471098 Color: 2

Bin 176: 6795 of cap free
Amount of items: 3
Items: 
Size: 555569 Color: 2
Size: 223526 Color: 4
Size: 214111 Color: 4

Bin 177: 7183 of cap free
Amount of items: 2
Items: 
Size: 546640 Color: 0
Size: 446178 Color: 1

Bin 178: 7409 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 0
Size: 379852 Color: 2

Bin 179: 7527 of cap free
Amount of items: 2
Items: 
Size: 672936 Color: 4
Size: 319538 Color: 0

Bin 180: 7900 of cap free
Amount of items: 2
Items: 
Size: 706164 Color: 2
Size: 285937 Color: 3

Bin 181: 7936 of cap free
Amount of items: 2
Items: 
Size: 601125 Color: 0
Size: 390940 Color: 1

Bin 182: 8048 of cap free
Amount of items: 2
Items: 
Size: 736183 Color: 1
Size: 255770 Color: 4

Bin 183: 8305 of cap free
Amount of items: 2
Items: 
Size: 776740 Color: 4
Size: 214956 Color: 0

Bin 184: 8653 of cap free
Amount of items: 2
Items: 
Size: 587418 Color: 1
Size: 403930 Color: 4

Bin 185: 8684 of cap free
Amount of items: 2
Items: 
Size: 533880 Color: 3
Size: 457437 Color: 1

Bin 186: 8755 of cap free
Amount of items: 2
Items: 
Size: 736061 Color: 1
Size: 255185 Color: 0

Bin 187: 8794 of cap free
Amount of items: 2
Items: 
Size: 761119 Color: 3
Size: 230088 Color: 2

Bin 188: 9383 of cap free
Amount of items: 2
Items: 
Size: 671465 Color: 1
Size: 319153 Color: 4

Bin 189: 9484 of cap free
Amount of items: 2
Items: 
Size: 520444 Color: 1
Size: 470073 Color: 4

Bin 190: 9875 of cap free
Amount of items: 2
Items: 
Size: 601102 Color: 4
Size: 389024 Color: 3

Bin 191: 10371 of cap free
Amount of items: 2
Items: 
Size: 544134 Color: 3
Size: 445496 Color: 0

Bin 192: 10716 of cap free
Amount of items: 2
Items: 
Size: 761061 Color: 4
Size: 228224 Color: 1

Bin 193: 11150 of cap free
Amount of items: 2
Items: 
Size: 735008 Color: 3
Size: 253843 Color: 0

Bin 194: 11627 of cap free
Amount of items: 2
Items: 
Size: 759027 Color: 1
Size: 229347 Color: 4

Bin 195: 11777 of cap free
Amount of items: 2
Items: 
Size: 531520 Color: 4
Size: 456704 Color: 2

Bin 196: 12726 of cap free
Amount of items: 2
Items: 
Size: 531099 Color: 0
Size: 456176 Color: 1

Bin 197: 12763 of cap free
Amount of items: 2
Items: 
Size: 669103 Color: 1
Size: 318135 Color: 0

Bin 198: 12944 of cap free
Amount of items: 2
Items: 
Size: 612391 Color: 0
Size: 374666 Color: 3

Bin 199: 15204 of cap free
Amount of items: 2
Items: 
Size: 671412 Color: 0
Size: 313385 Color: 2

Bin 200: 15797 of cap free
Amount of items: 2
Items: 
Size: 495986 Color: 3
Size: 488218 Color: 1

Bin 201: 16203 of cap free
Amount of items: 2
Items: 
Size: 579308 Color: 4
Size: 404490 Color: 1

Bin 202: 16640 of cap free
Amount of items: 2
Items: 
Size: 579582 Color: 1
Size: 403779 Color: 0

Bin 203: 17895 of cap free
Amount of items: 2
Items: 
Size: 728242 Color: 4
Size: 253864 Color: 3

Bin 204: 18243 of cap free
Amount of items: 2
Items: 
Size: 578654 Color: 4
Size: 403104 Color: 1

Bin 205: 19620 of cap free
Amount of items: 2
Items: 
Size: 637647 Color: 3
Size: 342734 Color: 1

Bin 206: 20324 of cap free
Amount of items: 2
Items: 
Size: 577838 Color: 2
Size: 401839 Color: 4

Bin 207: 23038 of cap free
Amount of items: 2
Items: 
Size: 728239 Color: 4
Size: 248724 Color: 3

Bin 208: 23138 of cap free
Amount of items: 2
Items: 
Size: 723688 Color: 0
Size: 253175 Color: 4

Bin 209: 24302 of cap free
Amount of items: 2
Items: 
Size: 487984 Color: 1
Size: 487715 Color: 4

Bin 210: 24532 of cap free
Amount of items: 2
Items: 
Size: 575477 Color: 4
Size: 399992 Color: 3

Bin 211: 25293 of cap free
Amount of items: 2
Items: 
Size: 663822 Color: 2
Size: 310886 Color: 0

Bin 212: 25479 of cap free
Amount of items: 2
Items: 
Size: 662002 Color: 3
Size: 312520 Color: 2

Bin 213: 26055 of cap free
Amount of items: 2
Items: 
Size: 574450 Color: 0
Size: 399496 Color: 3

Bin 214: 32929 of cap free
Amount of items: 2
Items: 
Size: 658823 Color: 2
Size: 308249 Color: 1

Bin 215: 34045 of cap free
Amount of items: 2
Items: 
Size: 658785 Color: 2
Size: 307171 Color: 4

Bin 216: 38470 of cap free
Amount of items: 2
Items: 
Size: 483097 Color: 0
Size: 478434 Color: 1

Bin 217: 38880 of cap free
Amount of items: 2
Items: 
Size: 657745 Color: 1
Size: 303376 Color: 0

Bin 218: 49007 of cap free
Amount of items: 2
Items: 
Size: 481325 Color: 0
Size: 469669 Color: 3

Bin 219: 50082 of cap free
Amount of items: 2
Items: 
Size: 480464 Color: 0
Size: 469455 Color: 2

Bin 220: 57643 of cap free
Amount of items: 2
Items: 
Size: 573872 Color: 2
Size: 368486 Color: 3

Bin 221: 68151 of cap free
Amount of items: 2
Items: 
Size: 568522 Color: 1
Size: 363328 Color: 3

Bin 222: 73553 of cap free
Amount of items: 2
Items: 
Size: 564018 Color: 2
Size: 362430 Color: 1

Bin 223: 129710 of cap free
Amount of items: 2
Items: 
Size: 567050 Color: 1
Size: 303241 Color: 3

Bin 224: 138454 of cap free
Amount of items: 2
Items: 
Size: 563559 Color: 4
Size: 297988 Color: 1

Bin 225: 148033 of cap free
Amount of items: 2
Items: 
Size: 555189 Color: 2
Size: 296779 Color: 4

Bin 226: 157005 of cap free
Amount of items: 2
Items: 
Size: 556063 Color: 4
Size: 286933 Color: 2

Bin 227: 184899 of cap free
Amount of items: 2
Items: 
Size: 530959 Color: 3
Size: 284143 Color: 4

Bin 228: 185095 of cap free
Amount of items: 2
Items: 
Size: 530782 Color: 2
Size: 284124 Color: 4

Bin 229: 197254 of cap free
Amount of items: 3
Items: 
Size: 281758 Color: 1
Size: 276398 Color: 2
Size: 244591 Color: 1

Total size: 226673258
Total free space: 2326971

