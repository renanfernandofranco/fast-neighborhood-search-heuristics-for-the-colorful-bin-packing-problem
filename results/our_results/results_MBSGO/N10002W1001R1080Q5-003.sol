Capicity Bin: 1001
Lower Bound: 4485

Bins used: 4618
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 409 Color: 0
Size: 181 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 409 Color: 0
Size: 177 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 408 Color: 1
Size: 177 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 400 Color: 0
Size: 183 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 404 Color: 0
Size: 179 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 402 Color: 1
Size: 181 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 404 Color: 0
Size: 179 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 404 Color: 0
Size: 179 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 404 Color: 0
Size: 179 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 403 Color: 2
Size: 179 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 405 Color: 1
Size: 177 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 405 Color: 1
Size: 177 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 405 Color: 1
Size: 177 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 396 Color: 0
Size: 182 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 393 Color: 0
Size: 183 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 396 Color: 0
Size: 180 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 398 Color: 0
Size: 178 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 398 Color: 0
Size: 178 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 398 Color: 0
Size: 178 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 397 Color: 1
Size: 178 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 398 Color: 1
Size: 177 Color: 2

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 428 Color: 0
Size: 191 Color: 3
Size: 191 Color: 2
Size: 191 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 391 Color: 1
Size: 181 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 391 Color: 1
Size: 181 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 394 Color: 0
Size: 178 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 393 Color: 2
Size: 179 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 394 Color: 0
Size: 178 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 393 Color: 1
Size: 179 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 393 Color: 1
Size: 176 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 389 Color: 0
Size: 176 Color: 1

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 436 Color: 0
Size: 191 Color: 3
Size: 191 Color: 2
Size: 183 Color: 1

Bin 32: 0 of cap free
Amount of items: 4
Items: 
Size: 437 Color: 0
Size: 191 Color: 3
Size: 191 Color: 2
Size: 182 Color: 3

Bin 33: 0 of cap free
Amount of items: 4
Items: 
Size: 437 Color: 0
Size: 192 Color: 3
Size: 192 Color: 1
Size: 180 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 386 Color: 1
Size: 178 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 386 Color: 2
Size: 178 Color: 2

Bin 36: 0 of cap free
Amount of items: 4
Items: 
Size: 440 Color: 0
Size: 192 Color: 3
Size: 192 Color: 1
Size: 177 Color: 3

Bin 37: 0 of cap free
Amount of items: 4
Items: 
Size: 440 Color: 0
Size: 192 Color: 3
Size: 192 Color: 1
Size: 177 Color: 2

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 440 Color: 0
Size: 192 Color: 3
Size: 192 Color: 1
Size: 177 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 385 Color: 1
Size: 176 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 385 Color: 2
Size: 176 Color: 0

Bin 41: 0 of cap free
Amount of items: 4
Items: 
Size: 441 Color: 0
Size: 192 Color: 3
Size: 192 Color: 2
Size: 176 Color: 1

Bin 42: 0 of cap free
Amount of items: 4
Items: 
Size: 441 Color: 0
Size: 192 Color: 3
Size: 192 Color: 2
Size: 176 Color: 0

Bin 43: 0 of cap free
Amount of items: 4
Items: 
Size: 442 Color: 0
Size: 193 Color: 1
Size: 190 Color: 0
Size: 176 Color: 1

Bin 44: 0 of cap free
Amount of items: 4
Items: 
Size: 442 Color: 0
Size: 193 Color: 1
Size: 190 Color: 0
Size: 176 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 369 Color: 0
Size: 190 Color: 1

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 442 Color: 0
Size: 193 Color: 3
Size: 191 Color: 4
Size: 175 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 370 Color: 0
Size: 189 Color: 3

Bin 48: 0 of cap free
Amount of items: 4
Items: 
Size: 442 Color: 0
Size: 193 Color: 1
Size: 191 Color: 3
Size: 175 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 370 Color: 0
Size: 189 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 372 Color: 0
Size: 187 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 372 Color: 0
Size: 187 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 372 Color: 0
Size: 187 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 372 Color: 0
Size: 187 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 384 Color: 0
Size: 175 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 384 Color: 0
Size: 175 Color: 2

Bin 56: 0 of cap free
Amount of items: 4
Items: 
Size: 443 Color: 0
Size: 193 Color: 3
Size: 190 Color: 2
Size: 175 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 375 Color: 0
Size: 183 Color: 2

Bin 58: 0 of cap free
Amount of items: 4
Items: 
Size: 443 Color: 0
Size: 193 Color: 2
Size: 190 Color: 1
Size: 175 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 375 Color: 0
Size: 183 Color: 1

Bin 60: 0 of cap free
Amount of items: 4
Items: 
Size: 443 Color: 0
Size: 193 Color: 3
Size: 190 Color: 2
Size: 175 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 376 Color: 0
Size: 182 Color: 2

Bin 62: 0 of cap free
Amount of items: 4
Items: 
Size: 443 Color: 0
Size: 193 Color: 2
Size: 190 Color: 3
Size: 175 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 376 Color: 0
Size: 182 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 2
Size: 384 Color: 0
Size: 174 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 376 Color: 0
Size: 181 Color: 1

Bin 66: 0 of cap free
Amount of items: 4
Items: 
Size: 444 Color: 0
Size: 194 Color: 1
Size: 189 Color: 3
Size: 174 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 381 Color: 0
Size: 176 Color: 3

Bin 68: 0 of cap free
Amount of items: 4
Items: 
Size: 444 Color: 0
Size: 194 Color: 1
Size: 189 Color: 1
Size: 174 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 382 Color: 0
Size: 175 Color: 1

Bin 70: 0 of cap free
Amount of items: 4
Items: 
Size: 444 Color: 0
Size: 194 Color: 3
Size: 189 Color: 3
Size: 174 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 383 Color: 0
Size: 174 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 383 Color: 0
Size: 174 Color: 1

Bin 73: 0 of cap free
Amount of items: 4
Items: 
Size: 445 Color: 0
Size: 195 Color: 1
Size: 187 Color: 4
Size: 174 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 0
Size: 374 Color: 1
Size: 182 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 0
Size: 374 Color: 1
Size: 182 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 382 Color: 0
Size: 174 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 374 Color: 1
Size: 180 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 376 Color: 1
Size: 174 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 376 Color: 1
Size: 174 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 376 Color: 1
Size: 174 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 375 Color: 2
Size: 173 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 375 Color: 0
Size: 173 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 375 Color: 2
Size: 173 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 375 Color: 0
Size: 173 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 375 Color: 2
Size: 173 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 374 Color: 1
Size: 173 Color: 0

Bin 87: 0 of cap free
Amount of items: 4
Items: 
Size: 455 Color: 0
Size: 195 Color: 1
Size: 178 Color: 3
Size: 173 Color: 1

Bin 88: 0 of cap free
Amount of items: 4
Items: 
Size: 455 Color: 0
Size: 195 Color: 2
Size: 178 Color: 4
Size: 173 Color: 4

Bin 89: 0 of cap free
Amount of items: 4
Items: 
Size: 455 Color: 0
Size: 195 Color: 1
Size: 178 Color: 4
Size: 173 Color: 1

Bin 90: 0 of cap free
Amount of items: 4
Items: 
Size: 455 Color: 0
Size: 195 Color: 2
Size: 178 Color: 4
Size: 173 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 365 Color: 2
Size: 180 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 365 Color: 2
Size: 180 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 366 Color: 1
Size: 179 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 372 Color: 2
Size: 173 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 367 Color: 1
Size: 177 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 367 Color: 1
Size: 177 Color: 4

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 367 Color: 1
Size: 177 Color: 4

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 370 Color: 0
Size: 174 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 367 Color: 1
Size: 172 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 367 Color: 1
Size: 172 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 366 Color: 1
Size: 172 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 366 Color: 1
Size: 172 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 365 Color: 1
Size: 171 Color: 1

Bin 104: 0 of cap free
Amount of items: 4
Items: 
Size: 466 Color: 0
Size: 193 Color: 3
Size: 171 Color: 1
Size: 171 Color: 0

Bin 105: 0 of cap free
Amount of items: 4
Items: 
Size: 466 Color: 0
Size: 193 Color: 2
Size: 171 Color: 1
Size: 171 Color: 3

Bin 106: 0 of cap free
Amount of items: 4
Items: 
Size: 468 Color: 0
Size: 191 Color: 4
Size: 171 Color: 1
Size: 171 Color: 3

Bin 107: 0 of cap free
Amount of items: 4
Items: 
Size: 468 Color: 0
Size: 191 Color: 4
Size: 171 Color: 1
Size: 171 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 353 Color: 2
Size: 180 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 358 Color: 1
Size: 175 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 358 Color: 1
Size: 172 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 358 Color: 1
Size: 172 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 359 Color: 1
Size: 171 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 359 Color: 1
Size: 171 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 353 Color: 2
Size: 173 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 353 Color: 1
Size: 173 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 353 Color: 2
Size: 173 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 353 Color: 3
Size: 173 Color: 3

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 0
Size: 354 Color: 1
Size: 171 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 351 Color: 1
Size: 170 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 349 Color: 1
Size: 170 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 347 Color: 1
Size: 170 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 347 Color: 2
Size: 170 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 347 Color: 2
Size: 170 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 347 Color: 2
Size: 170 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 347 Color: 2
Size: 170 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 347 Color: 2
Size: 170 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 343 Color: 1
Size: 173 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 342 Color: 1
Size: 168 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 342 Color: 1
Size: 168 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 343 Color: 2
Size: 167 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 343 Color: 4
Size: 167 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 343 Color: 2
Size: 167 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 343 Color: 4
Size: 167 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 341 Color: 0
Size: 167 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 341 Color: 1
Size: 167 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 341 Color: 1
Size: 167 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 341 Color: 1
Size: 166 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 341 Color: 2
Size: 166 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 341 Color: 1
Size: 166 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 341 Color: 2
Size: 166 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 337 Color: 1
Size: 168 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 337 Color: 2
Size: 166 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 337 Color: 2
Size: 166 Color: 2

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 337 Color: 2
Size: 166 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 335 Color: 2
Size: 167 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 336 Color: 2
Size: 166 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 335 Color: 1
Size: 167 Color: 2

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 2

Bin 149: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 0

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 2

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 0

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 2

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 0

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 2

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 2

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 3
Size: 500 Color: 1

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 2

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 3
Size: 500 Color: 1

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 2
Size: 500 Color: 4

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 3
Size: 500 Color: 4

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 4
Size: 500 Color: 2

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 2
Size: 499 Color: 1

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 4
Size: 500 Color: 3

Bin 164: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 2
Size: 499 Color: 1

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 499 Color: 2

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 2
Size: 499 Color: 1

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 499 Color: 4

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 2
Size: 499 Color: 4

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 499 Color: 4

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 4
Size: 499 Color: 2

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 499 Color: 4

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 4
Size: 499 Color: 3

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 499 Color: 4

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 4
Size: 499 Color: 3

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 4
Size: 499 Color: 3

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 503 Color: 0
Size: 332 Color: 1
Size: 166 Color: 3

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 0

Bin 178: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 0
Size: 498 Color: 1

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 0

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 2
Size: 498 Color: 1

Bin 181: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 3

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 2
Size: 498 Color: 3

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 498 Color: 2

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 2
Size: 498 Color: 3

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 498 Color: 2

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 2
Size: 498 Color: 4

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 498 Color: 4

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 4
Size: 498 Color: 2

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 1
Size: 331 Color: 0
Size: 166 Color: 2

Bin 190: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 4
Size: 498 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 1
Size: 331 Color: 0
Size: 166 Color: 3

Bin 192: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 1
Size: 497 Color: 0

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 497 Color: 1

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 1
Size: 497 Color: 0

Bin 195: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 497 Color: 1

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 1
Size: 497 Color: 2

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 2
Size: 497 Color: 4

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 497 Color: 4

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 2
Size: 497 Color: 4

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 497 Color: 4

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 505 Color: 0
Size: 330 Color: 1
Size: 166 Color: 2

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 497 Color: 2

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 505 Color: 0
Size: 330 Color: 1
Size: 166 Color: 3

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 497 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 505 Color: 0
Size: 330 Color: 2
Size: 166 Color: 3

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 497 Color: 2

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 496 Color: 1

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 4
Size: 497 Color: 3

Bin 209: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 496 Color: 1

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 1

Bin 211: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 3

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 2

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 496 Color: 4

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 3

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 496 Color: 4

Bin 216: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 2

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 3
Size: 496 Color: 4

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 3

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 3
Size: 496 Color: 4

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 3

Bin 221: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 3

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 495 Color: 1

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 1

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 0

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 1

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 0

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 1

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 0

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 0

Bin 230: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 494 Color: 3

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 1

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 494 Color: 2

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 3
Size: 495 Color: 2

Bin 234: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 494 Color: 3

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 3
Size: 495 Color: 4

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 494 Color: 4

Bin 237: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 4
Size: 495 Color: 3

Bin 238: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 494 Color: 3

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 494 Color: 3

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 4

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 2
Size: 494 Color: 4

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 4

Bin 243: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 493 Color: 1

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 4
Size: 494 Color: 3

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 493 Color: 1

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 493 Color: 0

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 2
Size: 493 Color: 1

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 493 Color: 2

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 4
Size: 493 Color: 3

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 2
Size: 325 Color: 0
Size: 167 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 1
Size: 328 Color: 0
Size: 164 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 2
Size: 328 Color: 0
Size: 164 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 1
Size: 330 Color: 2
Size: 162 Color: 0

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 2
Size: 492 Color: 0

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 492 Color: 1

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 2
Size: 492 Color: 0

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 492 Color: 1

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 2
Size: 492 Color: 0

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 492 Color: 1

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 3

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 492 Color: 2

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 2

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 492 Color: 4

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 3

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 2

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 3

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 491 Color: 1

Bin 268: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 491 Color: 0

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 491 Color: 2

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 491 Color: 1

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 491 Color: 2

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 491 Color: 1

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 491 Color: 2

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 491 Color: 4

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 491 Color: 4

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 491 Color: 4

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 0
Size: 328 Color: 1
Size: 162 Color: 0

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 4
Size: 491 Color: 3

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 0
Size: 490 Color: 1

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 2

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 0
Size: 490 Color: 1

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 1

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 1
Size: 490 Color: 2

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 2

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 2
Size: 490 Color: 1

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 4

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 490 Color: 2

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 4

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 490 Color: 3

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 4

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 490 Color: 3

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 1

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 490 Color: 3

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 2

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 2
Size: 489 Color: 4

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 2

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 2
Size: 489 Color: 4

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 489 Color: 4

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 2
Size: 489 Color: 4

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 3
Size: 489 Color: 4

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 489 Color: 3

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 3
Size: 489 Color: 4

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 489 Color: 3

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 1
Size: 323 Color: 0
Size: 165 Color: 0

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 489 Color: 3

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 1
Size: 324 Color: 0
Size: 164 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 1
Size: 325 Color: 0
Size: 163 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 0
Size: 327 Color: 1
Size: 161 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 1
Size: 325 Color: 0
Size: 163 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 3
Size: 327 Color: 1
Size: 161 Color: 0

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 488 Color: 0

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 3
Size: 488 Color: 0

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 488 Color: 1

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 3
Size: 488 Color: 2

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 488 Color: 4

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 3
Size: 488 Color: 4

Bin 317: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 4
Size: 488 Color: 1

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 4
Size: 488 Color: 2

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 4
Size: 488 Color: 3

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 514 Color: 0
Size: 327 Color: 1
Size: 160 Color: 0

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 4
Size: 488 Color: 2

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 514 Color: 0
Size: 327 Color: 1
Size: 160 Color: 0

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 4
Size: 488 Color: 3

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 514 Color: 0
Size: 327 Color: 1
Size: 160 Color: 0

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 487 Color: 0

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 487 Color: 1

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 487 Color: 2

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 487 Color: 3

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 487 Color: 2

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 3

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 3
Size: 487 Color: 2

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 3

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 3
Size: 487 Color: 4

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 515 Color: 1
Size: 326 Color: 0
Size: 160 Color: 0

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 3
Size: 487 Color: 4

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 515 Color: 1
Size: 326 Color: 0
Size: 160 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 515 Color: 1
Size: 327 Color: 0
Size: 159 Color: 0

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 2
Size: 486 Color: 1

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 486 Color: 4

Bin 340: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 2
Size: 486 Color: 1

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 486 Color: 4

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 2
Size: 486 Color: 1

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 3
Size: 486 Color: 4

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 2
Size: 486 Color: 1

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 3
Size: 486 Color: 4

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 3
Size: 486 Color: 4

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 0
Size: 328 Color: 1
Size: 157 Color: 0

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 3
Size: 486 Color: 4

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 0
Size: 328 Color: 1
Size: 157 Color: 0

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 486 Color: 3

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 1

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 486 Color: 3

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 1

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 1
Size: 485 Color: 0

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 2

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 1
Size: 485 Color: 3

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 2
Size: 485 Color: 3

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 3
Size: 485 Color: 2

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 2
Size: 485 Color: 4

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 3
Size: 485 Color: 4

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 4
Size: 485 Color: 3

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 0
Size: 484 Color: 1

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 484 Color: 2

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 3
Size: 484 Color: 1

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 484 Color: 2

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 3
Size: 484 Color: 2

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 484 Color: 4

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 3
Size: 484 Color: 4

Bin 369: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 2
Size: 484 Color: 4

Bin 370: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 3
Size: 484 Color: 4

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 2
Size: 484 Color: 4

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 3
Size: 484 Color: 4

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 4
Size: 484 Color: 2

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 483 Color: 1

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 483 Color: 2

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 483 Color: 1

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 483 Color: 2

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 2
Size: 483 Color: 3

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 3
Size: 483 Color: 4

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 3
Size: 483 Color: 4

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 4
Size: 483 Color: 3

Bin 382: 0 of cap free
Amount of items: 4
Items: 
Size: 519 Color: 0
Size: 166 Color: 3
Size: 159 Color: 0
Size: 157 Color: 2

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 4
Size: 483 Color: 3

Bin 384: 0 of cap free
Amount of items: 4
Items: 
Size: 519 Color: 0
Size: 167 Color: 2
Size: 159 Color: 0
Size: 156 Color: 1

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 0

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 482 Color: 3

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 1

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 482 Color: 3

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 2

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 482 Color: 3

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 4

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 2
Size: 482 Color: 4

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 4

Bin 394: 0 of cap free
Amount of items: 4
Items: 
Size: 520 Color: 0
Size: 167 Color: 1
Size: 158 Color: 0
Size: 156 Color: 1

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 4
Size: 482 Color: 3

Bin 396: 0 of cap free
Amount of items: 4
Items: 
Size: 520 Color: 0
Size: 167 Color: 2
Size: 158 Color: 0
Size: 156 Color: 1

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 1
Size: 481 Color: 0

Bin 398: 0 of cap free
Amount of items: 4
Items: 
Size: 520 Color: 0
Size: 167 Color: 3
Size: 158 Color: 1
Size: 156 Color: 1

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 1
Size: 481 Color: 0

Bin 400: 0 of cap free
Amount of items: 4
Items: 
Size: 520 Color: 0
Size: 170 Color: 3
Size: 156 Color: 1
Size: 155 Color: 0

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 1
Size: 481 Color: 0

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 2
Size: 481 Color: 0

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 481 Color: 2

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 0

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 481 Color: 2

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 2

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 481 Color: 4

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 2

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 0
Size: 321 Color: 2
Size: 159 Color: 1

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 3

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 0
Size: 325 Color: 1
Size: 155 Color: 1

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 1

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 1
Size: 480 Color: 3

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 2

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 1
Size: 480 Color: 3

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 4

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 480 Color: 2

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 4

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 480 Color: 3

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 4

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 480 Color: 2

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 4

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 480 Color: 3

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 0
Size: 479 Color: 1

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 2

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 3

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 2

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 3

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 2

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 3

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 4

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 0

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 4

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 0

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 4
Size: 479 Color: 2

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 0

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 4
Size: 479 Color: 3

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 2

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 0
Size: 478 Color: 1

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 2

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 1

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 2

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 2

Bin 444: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 2
Size: 478 Color: 1

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 2

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 2
Size: 478 Color: 4

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 4

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 0
Size: 321 Color: 2
Size: 156 Color: 1

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 4
Size: 478 Color: 2

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 0
Size: 321 Color: 1
Size: 156 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 1
Size: 322 Color: 0
Size: 155 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 0
Size: 322 Color: 1
Size: 155 Color: 1

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 0
Size: 322 Color: 1
Size: 155 Color: 2

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 0
Size: 322 Color: 1
Size: 155 Color: 1

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 0

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 0

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 0

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 1

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 477 Color: 2

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 1

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 477 Color: 2

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 1

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 477 Color: 2

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 3

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 477 Color: 2

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 3

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 477 Color: 4

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 477 Color: 3

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 0
Size: 321 Color: 2
Size: 155 Color: 2

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 477 Color: 3

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 0
Size: 321 Color: 1
Size: 155 Color: 3

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 0

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 1

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 0

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 1

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 0

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 4

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 4

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 4

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 4

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 4

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 476 Color: 2

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 3
Size: 476 Color: 4

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 476 Color: 3

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 3
Size: 476 Color: 4

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 476 Color: 3

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 1
Size: 475 Color: 2

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 475 Color: 3

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 3
Size: 475 Color: 2

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 475 Color: 4

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 3
Size: 475 Color: 4

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 475 Color: 3

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 3
Size: 475 Color: 4

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 475 Color: 2

Bin 497: 0 of cap free
Amount of items: 4
Items: 
Size: 527 Color: 0
Size: 159 Color: 1
Size: 159 Color: 3
Size: 156 Color: 1

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 475 Color: 3

Bin 499: 0 of cap free
Amount of items: 4
Items: 
Size: 527 Color: 0
Size: 160 Color: 1
Size: 159 Color: 3
Size: 155 Color: 2

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 474 Color: 1

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 1
Size: 319 Color: 0
Size: 155 Color: 3

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 474 Color: 1

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 474 Color: 4

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 474 Color: 4

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 474 Color: 4

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 4
Size: 474 Color: 0

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 474 Color: 4

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 4
Size: 474 Color: 1

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 474 Color: 4

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 4
Size: 474 Color: 2

Bin 511: 0 of cap free
Amount of items: 4
Items: 
Size: 528 Color: 0
Size: 160 Color: 1
Size: 158 Color: 0
Size: 155 Color: 2

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 1
Size: 473 Color: 0

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 1

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 1
Size: 473 Color: 0

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 1

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 1
Size: 473 Color: 0

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 0

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 2
Size: 473 Color: 1

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 3

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 2
Size: 473 Color: 4

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 2

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 3
Size: 473 Color: 4

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 3

Bin 524: 0 of cap free
Amount of items: 4
Items: 
Size: 529 Color: 0
Size: 160 Color: 2
Size: 158 Color: 1
Size: 154 Color: 0

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 2

Bin 526: 0 of cap free
Amount of items: 4
Items: 
Size: 529 Color: 0
Size: 160 Color: 2
Size: 160 Color: 1
Size: 152 Color: 0

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 3

Bin 528: 0 of cap free
Amount of items: 4
Items: 
Size: 529 Color: 0
Size: 160 Color: 2
Size: 160 Color: 1
Size: 152 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 529 Color: 3
Size: 317 Color: 0
Size: 155 Color: 3

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 529 Color: 1
Size: 317 Color: 0
Size: 155 Color: 2

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 529 Color: 3
Size: 317 Color: 0
Size: 155 Color: 3

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 1
Size: 472 Color: 0

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 0

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 1
Size: 472 Color: 0

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 0

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 472 Color: 3

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 1

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 472 Color: 3

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 2

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 472 Color: 3

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 4

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 472 Color: 4

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 4

Bin 544: 0 of cap free
Amount of items: 4
Items: 
Size: 530 Color: 0
Size: 167 Color: 4
Size: 152 Color: 2
Size: 152 Color: 0

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 4

Bin 546: 0 of cap free
Amount of items: 4
Items: 
Size: 530 Color: 0
Size: 167 Color: 3
Size: 152 Color: 2
Size: 152 Color: 0

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 4
Size: 472 Color: 3

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 0
Size: 471 Color: 1

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 471 Color: 0

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 471 Color: 2

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 471 Color: 1

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 471 Color: 2

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 471 Color: 1

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 471 Color: 4

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 471 Color: 4

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 471 Color: 4

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 471 Color: 4

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 4
Size: 471 Color: 3

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 3
Size: 471 Color: 4

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 4
Size: 471 Color: 3

Bin 561: 0 of cap free
Amount of items: 4
Items: 
Size: 531 Color: 0
Size: 166 Color: 4
Size: 152 Color: 2
Size: 152 Color: 1

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 1
Size: 314 Color: 0
Size: 156 Color: 2

Bin 563: 0 of cap free
Amount of items: 4
Items: 
Size: 531 Color: 0
Size: 166 Color: 4
Size: 152 Color: 2
Size: 152 Color: 1

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 0
Size: 318 Color: 2
Size: 152 Color: 3

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 0

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 470 Color: 1

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 2

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 3

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 3

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 3

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 3
Size: 470 Color: 2

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 4

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 3
Size: 470 Color: 4

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 470 Color: 3

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 469 Color: 1

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 469 Color: 0

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 469 Color: 1

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 469 Color: 0

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 469 Color: 1

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 469 Color: 2

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 469 Color: 1

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 469 Color: 2

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 469 Color: 3

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 3
Size: 469 Color: 2

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 469 Color: 3

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 3
Size: 469 Color: 4

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 469 Color: 4

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 3
Size: 469 Color: 4

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 4
Size: 469 Color: 2

Bin 591: 0 of cap free
Amount of items: 4
Items: 
Size: 533 Color: 0
Size: 160 Color: 3
Size: 157 Color: 2
Size: 151 Color: 1

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 4
Size: 469 Color: 3

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 0
Size: 317 Color: 1
Size: 151 Color: 1

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 4
Size: 469 Color: 3

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 468 Color: 1

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 1

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 3

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 1

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 3

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 1

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 3

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 2

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 2
Size: 468 Color: 3

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 4

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 4
Size: 468 Color: 3

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 4

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 4
Size: 468 Color: 2

Bin 608: 0 of cap free
Amount of items: 4
Items: 
Size: 534 Color: 0
Size: 160 Color: 2
Size: 156 Color: 3
Size: 151 Color: 1

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 4
Size: 468 Color: 3

Bin 610: 0 of cap free
Amount of items: 4
Items: 
Size: 534 Color: 0
Size: 160 Color: 3
Size: 156 Color: 2
Size: 151 Color: 0

Bin 611: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 4
Size: 468 Color: 3

Bin 612: 0 of cap free
Amount of items: 4
Items: 
Size: 534 Color: 0
Size: 160 Color: 2
Size: 156 Color: 3
Size: 151 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 1
Size: 272 Color: 0
Size: 195 Color: 1

Bin 614: 0 of cap free
Amount of items: 4
Items: 
Size: 534 Color: 0
Size: 160 Color: 3
Size: 156 Color: 2
Size: 151 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 1
Size: 314 Color: 0
Size: 153 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 2
Size: 317 Color: 0
Size: 150 Color: 0

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 3
Size: 467 Color: 2

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 2
Size: 467 Color: 0

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 3
Size: 467 Color: 2

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 2
Size: 467 Color: 3

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 3
Size: 467 Color: 0

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 2
Size: 467 Color: 3

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 3
Size: 467 Color: 2

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 4
Size: 467 Color: 3

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 466 Color: 4

Bin 626: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 4
Size: 467 Color: 3

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 466 Color: 1

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 4

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 466 Color: 2

Bin 630: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 4

Bin 631: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 466 Color: 3

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 4

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 466 Color: 4

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 4
Size: 466 Color: 3

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 3
Size: 466 Color: 4

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 4
Size: 466 Color: 3

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 1

Bin 638: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 0

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 1

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 0

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 1

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 0

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 1

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 2

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 1

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 2

Bin 647: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 2

Bin 648: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 2
Size: 465 Color: 3

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 4

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 2
Size: 465 Color: 4

Bin 651: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 4

Bin 652: 0 of cap free
Amount of items: 4
Items: 
Size: 537 Color: 0
Size: 160 Color: 4
Size: 153 Color: 0
Size: 151 Color: 1

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 4

Bin 654: 0 of cap free
Amount of items: 4
Items: 
Size: 537 Color: 0
Size: 161 Color: 1
Size: 153 Color: 1
Size: 150 Color: 0

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 4
Size: 465 Color: 3

Bin 656: 0 of cap free
Amount of items: 4
Items: 
Size: 537 Color: 0
Size: 160 Color: 4
Size: 153 Color: 0
Size: 151 Color: 1

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 464 Color: 0

Bin 658: 0 of cap free
Amount of items: 4
Items: 
Size: 537 Color: 0
Size: 161 Color: 1
Size: 153 Color: 1
Size: 150 Color: 0

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 464 Color: 0

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 0

Bin 661: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 3
Size: 464 Color: 0

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 1

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 3
Size: 464 Color: 4

Bin 664: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 4

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 3
Size: 464 Color: 4

Bin 666: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 4

Bin 667: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 3
Size: 464 Color: 4

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 464 Color: 2

Bin 669: 0 of cap free
Amount of items: 4
Items: 
Size: 538 Color: 0
Size: 161 Color: 1
Size: 153 Color: 2
Size: 149 Color: 0

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 464 Color: 2

Bin 671: 0 of cap free
Amount of items: 4
Items: 
Size: 538 Color: 0
Size: 161 Color: 1
Size: 153 Color: 1
Size: 149 Color: 0

Bin 672: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 464 Color: 2

Bin 673: 0 of cap free
Amount of items: 4
Items: 
Size: 538 Color: 0
Size: 161 Color: 2
Size: 153 Color: 2
Size: 149 Color: 1

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 464 Color: 2

Bin 675: 0 of cap free
Amount of items: 4
Items: 
Size: 538 Color: 0
Size: 161 Color: 3
Size: 153 Color: 3
Size: 149 Color: 0

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 463 Color: 0

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 463 Color: 2

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 463 Color: 1

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 463 Color: 2

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 463 Color: 3

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 463 Color: 2

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 463 Color: 3

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 463 Color: 4

Bin 684: 0 of cap free
Amount of items: 4
Items: 
Size: 539 Color: 0
Size: 161 Color: 2
Size: 152 Color: 2
Size: 149 Color: 1

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 463 Color: 4

Bin 686: 0 of cap free
Amount of items: 4
Items: 
Size: 539 Color: 0
Size: 161 Color: 3
Size: 152 Color: 3
Size: 149 Color: 2

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 4
Size: 463 Color: 3

Bin 688: 0 of cap free
Amount of items: 4
Items: 
Size: 539 Color: 0
Size: 161 Color: 4
Size: 152 Color: 4
Size: 149 Color: 3

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 539 Color: 0
Size: 314 Color: 1
Size: 148 Color: 2

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 1

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 462 Color: 0

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 2

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 2
Size: 462 Color: 1

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 2

Bin 696: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 2
Size: 462 Color: 1

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 4

Bin 698: 0 of cap free
Amount of items: 4
Items: 
Size: 540 Color: 0
Size: 162 Color: 1
Size: 151 Color: 1
Size: 148 Color: 0

Bin 699: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 462 Color: 1

Bin 700: 0 of cap free
Amount of items: 4
Items: 
Size: 540 Color: 0
Size: 163 Color: 1
Size: 150 Color: 1
Size: 148 Color: 0

Bin 701: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 462 Color: 2

Bin 702: 0 of cap free
Amount of items: 4
Items: 
Size: 540 Color: 0
Size: 163 Color: 1
Size: 150 Color: 0
Size: 148 Color: 2

Bin 703: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 462 Color: 3

Bin 704: 0 of cap free
Amount of items: 4
Items: 
Size: 540 Color: 0
Size: 163 Color: 2
Size: 150 Color: 1
Size: 148 Color: 2

Bin 705: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 462 Color: 3

Bin 706: 0 of cap free
Amount of items: 4
Items: 
Size: 540 Color: 0
Size: 163 Color: 1
Size: 150 Color: 2
Size: 148 Color: 1

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 2
Size: 273 Color: 0
Size: 188 Color: 2

Bin 708: 0 of cap free
Amount of items: 4
Items: 
Size: 540 Color: 0
Size: 163 Color: 2
Size: 150 Color: 3
Size: 148 Color: 2

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 2
Size: 273 Color: 0
Size: 188 Color: 3

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 1
Size: 273 Color: 0
Size: 188 Color: 2

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 2
Size: 273 Color: 0
Size: 188 Color: 3

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 1
Size: 273 Color: 0
Size: 188 Color: 2

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 2
Size: 273 Color: 0
Size: 188 Color: 2

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 1
Size: 277 Color: 0
Size: 184 Color: 2

Bin 715: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 2
Size: 461 Color: 1

Bin 716: 0 of cap free
Amount of items: 4
Items: 
Size: 541 Color: 0
Size: 163 Color: 3
Size: 149 Color: 2
Size: 148 Color: 1

Bin 717: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 2
Size: 461 Color: 1

Bin 718: 0 of cap free
Amount of items: 4
Items: 
Size: 541 Color: 0
Size: 163 Color: 4
Size: 149 Color: 3
Size: 148 Color: 2

Bin 719: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 3
Size: 461 Color: 2

Bin 720: 0 of cap free
Amount of items: 4
Items: 
Size: 541 Color: 0
Size: 164 Color: 1
Size: 148 Color: 2
Size: 148 Color: 1

Bin 721: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 3
Size: 461 Color: 4

Bin 722: 0 of cap free
Amount of items: 4
Items: 
Size: 541 Color: 0
Size: 164 Color: 1
Size: 149 Color: 4
Size: 147 Color: 0

Bin 723: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 461 Color: 3

Bin 724: 0 of cap free
Amount of items: 4
Items: 
Size: 541 Color: 0
Size: 164 Color: 1
Size: 149 Color: 3
Size: 147 Color: 0

Bin 725: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 461 Color: 3

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 541 Color: 0
Size: 314 Color: 1
Size: 146 Color: 0

Bin 727: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 2
Size: 460 Color: 0

Bin 728: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 1
Size: 460 Color: 2

Bin 729: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 2
Size: 460 Color: 0

Bin 730: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 0

Bin 731: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 2

Bin 732: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 1

Bin 733: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 2

Bin 734: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 4

Bin 735: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 1

Bin 736: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 4

Bin 737: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 2

Bin 738: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 4

Bin 739: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 3

Bin 740: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 4

Bin 741: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 3

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 1
Size: 284 Color: 0
Size: 175 Color: 1

Bin 743: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 460 Color: 3

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 1
Size: 287 Color: 0
Size: 172 Color: 2

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 2
Size: 309 Color: 0
Size: 150 Color: 4

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 1
Size: 309 Color: 0
Size: 150 Color: 3

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 2
Size: 310 Color: 0
Size: 149 Color: 4

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 1
Size: 314 Color: 0
Size: 145 Color: 0

Bin 749: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 2
Size: 459 Color: 0

Bin 750: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 459 Color: 2

Bin 751: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 3
Size: 459 Color: 1

Bin 752: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 459 Color: 2

Bin 753: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 3
Size: 459 Color: 1

Bin 754: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 459 Color: 2

Bin 755: 0 of cap free
Amount of items: 4
Items: 
Size: 543 Color: 0
Size: 166 Color: 4
Size: 147 Color: 1
Size: 145 Color: 0

Bin 756: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 459 Color: 3

Bin 757: 0 of cap free
Amount of items: 4
Items: 
Size: 543 Color: 0
Size: 166 Color: 4
Size: 147 Color: 1
Size: 145 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 1
Size: 310 Color: 0
Size: 148 Color: 3

Bin 759: 0 of cap free
Amount of items: 4
Items: 
Size: 543 Color: 0
Size: 166 Color: 4
Size: 147 Color: 1
Size: 145 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 1
Size: 310 Color: 0
Size: 148 Color: 4

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 2
Size: 310 Color: 0
Size: 148 Color: 3

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 1
Size: 311 Color: 0
Size: 147 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 2
Size: 314 Color: 1
Size: 144 Color: 0

Bin 764: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 1
Size: 458 Color: 2

Bin 765: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 2
Size: 458 Color: 0

Bin 766: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 4
Size: 458 Color: 0

Bin 767: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 2

Bin 768: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 4
Size: 458 Color: 1

Bin 769: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 4

Bin 770: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 4
Size: 458 Color: 2

Bin 771: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 4

Bin 772: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 4
Size: 458 Color: 3

Bin 773: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 4

Bin 774: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 4
Size: 458 Color: 3

Bin 775: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 0
Size: 457 Color: 3

Bin 776: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 1

Bin 777: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 1
Size: 457 Color: 3

Bin 778: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 2

Bin 779: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 2
Size: 457 Color: 3

Bin 780: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 2

Bin 781: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 2
Size: 457 Color: 4

Bin 782: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 4

Bin 783: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 4
Size: 457 Color: 3

Bin 784: 0 of cap free
Amount of items: 4
Items: 
Size: 545 Color: 0
Size: 164 Color: 1
Size: 148 Color: 4
Size: 144 Color: 0

Bin 785: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 456 Color: 0

Bin 786: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 0
Size: 456 Color: 2

Bin 787: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 456 Color: 0

Bin 788: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 1
Size: 456 Color: 2

Bin 789: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 456 Color: 1

Bin 790: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 1
Size: 456 Color: 2

Bin 791: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 456 Color: 3

Bin 792: 0 of cap free
Amount of items: 4
Items: 
Size: 546 Color: 0
Size: 167 Color: 4
Size: 144 Color: 1
Size: 144 Color: 0

Bin 793: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 3
Size: 456 Color: 2

Bin 794: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 455 Color: 2

Bin 795: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 455 Color: 0

Bin 796: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 455 Color: 2

Bin 797: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 455 Color: 3

Bin 798: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 3
Size: 455 Color: 2

Bin 799: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 2
Size: 455 Color: 3

Bin 800: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 3
Size: 455 Color: 4

Bin 801: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 2
Size: 455 Color: 4

Bin 802: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 3
Size: 455 Color: 4

Bin 803: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 454 Color: 2

Bin 804: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 455 Color: 3

Bin 805: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 454 Color: 2

Bin 806: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 454 Color: 2

Bin 807: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 0

Bin 808: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 454 Color: 2

Bin 809: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 1

Bin 810: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 4
Size: 454 Color: 2

Bin 811: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 3

Bin 812: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 4
Size: 454 Color: 2

Bin 813: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 4

Bin 814: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 4
Size: 454 Color: 3

Bin 815: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 3
Size: 454 Color: 4

Bin 816: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 4
Size: 454 Color: 3

Bin 817: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 0
Size: 453 Color: 1

Bin 818: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 3
Size: 453 Color: 2

Bin 819: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 0
Size: 453 Color: 1

Bin 820: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 3
Size: 453 Color: 1

Bin 821: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 1
Size: 453 Color: 2

Bin 822: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 3
Size: 453 Color: 2

Bin 823: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 1
Size: 453 Color: 4

Bin 824: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 3
Size: 453 Color: 4

Bin 825: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 2
Size: 453 Color: 4

Bin 826: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 3
Size: 453 Color: 4

Bin 827: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 4
Size: 453 Color: 3

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 1
Size: 287 Color: 0
Size: 165 Color: 0

Bin 829: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 4
Size: 453 Color: 3

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 1
Size: 287 Color: 0
Size: 165 Color: 0

Bin 831: 0 of cap free
Amount of items: 4
Items: 
Size: 549 Color: 0
Size: 164 Color: 2
Size: 144 Color: 1
Size: 144 Color: 3

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 1
Size: 287 Color: 0
Size: 165 Color: 1

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 0
Size: 308 Color: 2
Size: 144 Color: 3

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 1
Size: 309 Color: 0
Size: 143 Color: 0

Bin 835: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 0
Size: 452 Color: 1

Bin 836: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 1
Size: 452 Color: 0

Bin 837: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 1

Bin 838: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 1
Size: 452 Color: 0

Bin 839: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 1

Bin 840: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 452 Color: 2

Bin 841: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 3

Bin 842: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 452 Color: 4

Bin 843: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 3

Bin 844: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 452 Color: 4

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 0
Size: 308 Color: 2
Size: 143 Color: 0

Bin 846: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 451 Color: 0

Bin 847: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 0
Size: 451 Color: 2

Bin 848: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 451 Color: 1

Bin 849: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 0
Size: 451 Color: 2

Bin 850: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 451 Color: 1

Bin 851: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 451 Color: 2

Bin 852: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 451 Color: 1

Bin 853: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 451 Color: 2

Bin 854: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 451 Color: 1

Bin 855: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 4

Bin 856: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 4
Size: 451 Color: 3

Bin 857: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 4

Bin 858: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 4
Size: 451 Color: 3

Bin 859: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 4

Bin 860: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 4
Size: 451 Color: 3

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 1
Size: 288 Color: 0
Size: 162 Color: 1

Bin 862: 0 of cap free
Amount of items: 4
Items: 
Size: 551 Color: 0
Size: 164 Color: 3
Size: 143 Color: 1
Size: 143 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 1
Size: 296 Color: 0
Size: 154 Color: 0

Bin 864: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 2

Bin 865: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 450 Color: 0

Bin 866: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 1

Bin 867: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 450 Color: 2

Bin 868: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 1

Bin 869: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 450 Color: 2

Bin 870: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 2
Size: 450 Color: 1

Bin 871: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 450 Color: 2

Bin 872: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 2
Size: 450 Color: 4

Bin 873: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 3
Size: 450 Color: 4

Bin 874: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 4
Size: 450 Color: 2

Bin 875: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 3
Size: 450 Color: 4

Bin 876: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 4
Size: 450 Color: 3

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 1
Size: 296 Color: 0
Size: 153 Color: 2

Bin 878: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 4
Size: 450 Color: 2

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 1
Size: 296 Color: 0
Size: 153 Color: 3

Bin 880: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 4
Size: 450 Color: 3

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 1
Size: 298 Color: 0
Size: 151 Color: 2

Bin 882: 0 of cap free
Amount of items: 4
Items: 
Size: 552 Color: 0
Size: 163 Color: 3
Size: 143 Color: 1
Size: 143 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 1
Size: 298 Color: 0
Size: 151 Color: 3

Bin 884: 0 of cap free
Amount of items: 4
Items: 
Size: 552 Color: 0
Size: 163 Color: 4
Size: 143 Color: 1
Size: 143 Color: 3

Bin 885: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 449 Color: 0

Bin 886: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 0
Size: 449 Color: 1

Bin 887: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 449 Color: 0

Bin 888: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 449 Color: 0

Bin 889: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 3
Size: 449 Color: 1

Bin 890: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 449 Color: 4

Bin 891: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 3
Size: 449 Color: 1

Bin 892: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 449 Color: 4

Bin 893: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 3
Size: 449 Color: 4

Bin 894: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 4
Size: 449 Color: 2

Bin 895: 0 of cap free
Amount of items: 4
Items: 
Size: 553 Color: 0
Size: 162 Color: 4
Size: 143 Color: 1
Size: 143 Color: 3

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 553 Color: 1
Size: 300 Color: 0
Size: 148 Color: 3

Bin 897: 0 of cap free
Amount of items: 4
Items: 
Size: 553 Color: 0
Size: 162 Color: 1
Size: 143 Color: 1
Size: 143 Color: 3

Bin 898: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 448 Color: 0

Bin 899: 0 of cap free
Amount of items: 4
Items: 
Size: 553 Color: 0
Size: 162 Color: 4
Size: 143 Color: 2
Size: 143 Color: 3

Bin 900: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 448 Color: 0

Bin 901: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 2
Size: 448 Color: 1

Bin 902: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 448 Color: 0

Bin 903: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 2
Size: 448 Color: 3

Bin 904: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 3
Size: 448 Color: 4

Bin 905: 0 of cap free
Amount of items: 4
Items: 
Size: 554 Color: 0
Size: 162 Color: 1
Size: 143 Color: 3
Size: 142 Color: 1

Bin 906: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 3
Size: 448 Color: 4

Bin 907: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 1

Bin 908: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 3
Size: 448 Color: 4

Bin 909: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 1

Bin 910: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 4
Size: 448 Color: 2

Bin 911: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 1

Bin 912: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 4
Size: 448 Color: 3

Bin 913: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 2

Bin 914: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 4
Size: 448 Color: 3

Bin 915: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 1

Bin 916: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 1
Size: 447 Color: 3

Bin 917: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 2

Bin 918: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 2
Size: 447 Color: 3

Bin 919: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 2

Bin 920: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 2
Size: 447 Color: 4

Bin 921: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 4

Bin 922: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 4
Size: 447 Color: 2

Bin 923: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 4

Bin 924: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 4
Size: 447 Color: 3

Bin 925: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 4

Bin 926: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 4
Size: 447 Color: 3

Bin 927: 0 of cap free
Amount of items: 4
Items: 
Size: 555 Color: 0
Size: 162 Color: 4
Size: 142 Color: 1
Size: 142 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 2
Size: 302 Color: 0
Size: 144 Color: 1

Bin 929: 0 of cap free
Amount of items: 4
Items: 
Size: 555 Color: 0
Size: 162 Color: 2
Size: 142 Color: 1
Size: 142 Color: 2

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 2
Size: 304 Color: 0
Size: 142 Color: 2

Bin 931: 0 of cap free
Amount of items: 4
Items: 
Size: 555 Color: 0
Size: 162 Color: 4
Size: 142 Color: 2
Size: 142 Color: 1

Bin 932: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 2
Size: 446 Color: 0

Bin 933: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 1
Size: 446 Color: 2

Bin 934: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 2
Size: 446 Color: 0

Bin 935: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 446 Color: 2

Bin 936: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 446 Color: 4

Bin 937: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 446 Color: 1

Bin 938: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 446 Color: 4

Bin 939: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 446 Color: 2

Bin 940: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 0
Size: 445 Color: 2

Bin 941: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 3

Bin 942: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 0
Size: 445 Color: 2

Bin 943: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 3

Bin 944: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 4
Size: 445 Color: 2

Bin 945: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 4

Bin 946: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 4
Size: 445 Color: 3

Bin 947: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 2
Size: 445 Color: 4

Bin 948: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 4
Size: 445 Color: 3

Bin 949: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 2
Size: 445 Color: 4

Bin 950: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 4
Size: 445 Color: 3

Bin 951: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 2
Size: 445 Color: 4

Bin 952: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 4
Size: 445 Color: 3

Bin 953: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 444 Color: 0

Bin 954: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 1

Bin 955: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 444 Color: 0

Bin 956: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 3

Bin 957: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 444 Color: 2

Bin 958: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 3

Bin 959: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 444 Color: 2

Bin 960: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 3
Size: 444 Color: 4

Bin 961: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 2
Size: 444 Color: 4

Bin 962: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 3
Size: 444 Color: 4

Bin 963: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 1

Bin 964: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 4
Size: 444 Color: 3

Bin 965: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 1

Bin 966: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 1
Size: 443 Color: 2

Bin 967: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 1

Bin 968: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 1
Size: 443 Color: 2

Bin 969: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 2

Bin 970: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 1
Size: 443 Color: 4

Bin 971: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 2

Bin 972: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 1
Size: 443 Color: 4

Bin 973: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 2

Bin 974: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 1
Size: 443 Color: 2

Bin 975: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 2
Size: 443 Color: 4

Bin 976: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 3
Size: 443 Color: 4

Bin 977: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 2
Size: 443 Color: 3

Bin 978: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 3
Size: 443 Color: 4

Bin 979: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 2
Size: 443 Color: 3

Bin 980: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 3
Size: 443 Color: 4

Bin 981: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 442 Color: 3

Bin 982: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 3
Size: 442 Color: 4

Bin 983: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 442 Color: 3

Bin 984: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 3
Size: 442 Color: 4

Bin 985: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 2
Size: 442 Color: 4

Bin 986: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 3
Size: 442 Color: 4

Bin 987: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 4
Size: 442 Color: 3

Bin 988: 0 of cap free
Amount of items: 4
Items: 
Size: 560 Color: 0
Size: 157 Color: 4
Size: 142 Color: 2
Size: 142 Color: 3

Bin 989: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 441 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 0
Size: 300 Color: 3
Size: 141 Color: 0

Bin 991: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 441 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 0
Size: 300 Color: 3
Size: 141 Color: 0

Bin 993: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 441 Color: 2

Bin 994: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 2
Size: 441 Color: 1

Bin 995: 0 of cap free
Amount of items: 4
Items: 
Size: 561 Color: 0
Size: 158 Color: 2
Size: 141 Color: 2
Size: 141 Color: 0

Bin 996: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 2
Size: 441 Color: 3

Bin 997: 0 of cap free
Amount of items: 4
Items: 
Size: 561 Color: 0
Size: 158 Color: 1
Size: 141 Color: 2
Size: 141 Color: 1

Bin 998: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 3
Size: 441 Color: 4

Bin 999: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 440 Color: 1

Bin 1000: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 3
Size: 441 Color: 4

Bin 1001: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 440 Color: 4

Bin 1002: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 4
Size: 441 Color: 2

Bin 1003: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 440 Color: 4

Bin 1004: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 4
Size: 441 Color: 3

Bin 1005: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 440 Color: 4

Bin 1006: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 1
Size: 440 Color: 4

Bin 1007: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 2
Size: 440 Color: 4

Bin 1008: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 4
Size: 440 Color: 2

Bin 1009: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 440 Color: 4

Bin 1010: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 4
Size: 440 Color: 3

Bin 1011: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 440 Color: 4

Bin 1012: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 4
Size: 440 Color: 3

Bin 1013: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 0

Bin 1014: 0 of cap free
Amount of items: 4
Items: 
Size: 562 Color: 0
Size: 157 Color: 3
Size: 141 Color: 2
Size: 141 Color: 1

Bin 1015: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 0

Bin 1016: 0 of cap free
Amount of items: 4
Items: 
Size: 562 Color: 0
Size: 157 Color: 4
Size: 141 Color: 2
Size: 141 Color: 1

Bin 1017: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 2

Bin 1018: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 439 Color: 1

Bin 1019: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 2

Bin 1020: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 439 Color: 1

Bin 1021: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 2

Bin 1022: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 2
Size: 439 Color: 1

Bin 1023: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 4

Bin 1024: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 2
Size: 439 Color: 4

Bin 1025: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 3
Size: 439 Color: 4

Bin 1026: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 4
Size: 439 Color: 2

Bin 1027: 0 of cap free
Amount of items: 4
Items: 
Size: 563 Color: 0
Size: 156 Color: 3
Size: 141 Color: 2
Size: 141 Color: 3

Bin 1028: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 4
Size: 439 Color: 3

Bin 1029: 0 of cap free
Amount of items: 4
Items: 
Size: 563 Color: 0
Size: 158 Color: 2
Size: 140 Color: 1
Size: 140 Color: 1

Bin 1030: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 4
Size: 439 Color: 3

Bin 1031: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 438 Color: 1

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 1
Size: 298 Color: 0
Size: 140 Color: 1

Bin 1033: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 438 Color: 1

Bin 1034: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 1
Size: 438 Color: 0

Bin 1035: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 2
Size: 438 Color: 1

Bin 1036: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 0

Bin 1037: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 3
Size: 438 Color: 1

Bin 1038: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 0

Bin 1039: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 3
Size: 438 Color: 1

Bin 1040: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 2

Bin 1041: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 3
Size: 438 Color: 1

Bin 1042: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 3

Bin 1043: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 3
Size: 438 Color: 2

Bin 1044: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 2

Bin 1045: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 3
Size: 438 Color: 4

Bin 1046: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 3

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 564 Color: 0
Size: 297 Color: 1
Size: 140 Color: 1

Bin 1048: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 2

Bin 1049: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 0
Size: 437 Color: 1

Bin 1050: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 3

Bin 1051: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 0
Size: 437 Color: 1

Bin 1052: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 2

Bin 1053: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 437 Color: 1

Bin 1054: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 2

Bin 1055: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 437 Color: 1

Bin 1056: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 2

Bin 1057: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 437 Color: 1

Bin 1058: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 2

Bin 1059: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 437 Color: 3

Bin 1060: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 437 Color: 2

Bin 1061: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 4
Size: 437 Color: 3

Bin 1062: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 437 Color: 4

Bin 1063: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 4
Size: 437 Color: 2

Bin 1064: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 437 Color: 4

Bin 1065: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 4
Size: 437 Color: 3

Bin 1066: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 437 Color: 4

Bin 1067: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 4
Size: 437 Color: 3

Bin 1068: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 0
Size: 436 Color: 2

Bin 1069: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 2

Bin 1070: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 2
Size: 436 Color: 1

Bin 1071: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 4

Bin 1072: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 2
Size: 436 Color: 4

Bin 1073: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 3
Size: 436 Color: 4

Bin 1074: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 2
Size: 436 Color: 4

Bin 1075: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 3
Size: 436 Color: 4

Bin 1076: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 1

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 2
Size: 288 Color: 0
Size: 147 Color: 1

Bin 1078: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 2

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 2
Size: 288 Color: 0
Size: 147 Color: 0

Bin 1080: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 2

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 2
Size: 295 Color: 0
Size: 140 Color: 0

Bin 1082: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 3

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 2
Size: 295 Color: 0
Size: 140 Color: 1

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 1
Size: 295 Color: 2
Size: 140 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 2
Size: 295 Color: 0
Size: 140 Color: 1

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 1
Size: 295 Color: 2
Size: 140 Color: 0

Bin 1087: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 2
Size: 435 Color: 0

Bin 1088: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 1
Size: 435 Color: 0

Bin 1089: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 2
Size: 435 Color: 1

Bin 1090: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 435 Color: 2

Bin 1091: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 2
Size: 435 Color: 1

Bin 1092: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 435 Color: 2

Bin 1093: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 2
Size: 435 Color: 1

Bin 1094: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 435 Color: 2

Bin 1095: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 4
Size: 435 Color: 3

Bin 1096: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 435 Color: 4

Bin 1097: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 4
Size: 435 Color: 2

Bin 1098: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 435 Color: 4

Bin 1099: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 4
Size: 435 Color: 3

Bin 1100: 0 of cap free
Amount of items: 4
Items: 
Size: 567 Color: 0
Size: 153 Color: 2
Size: 141 Color: 3
Size: 140 Color: 1

Bin 1101: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 4
Size: 435 Color: 3

Bin 1102: 0 of cap free
Amount of items: 4
Items: 
Size: 567 Color: 0
Size: 153 Color: 3
Size: 141 Color: 4
Size: 140 Color: 0

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 1
Size: 288 Color: 0
Size: 146 Color: 0

Bin 1104: 0 of cap free
Amount of items: 4
Items: 
Size: 567 Color: 0
Size: 153 Color: 4
Size: 141 Color: 3
Size: 140 Color: 1

Bin 1105: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 1
Size: 434 Color: 0

Bin 1106: 0 of cap free
Amount of items: 4
Items: 
Size: 567 Color: 0
Size: 153 Color: 3
Size: 141 Color: 4
Size: 140 Color: 2

Bin 1107: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 1
Size: 434 Color: 2

Bin 1108: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 434 Color: 0

Bin 1109: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 1
Size: 434 Color: 2

Bin 1110: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 434 Color: 0

Bin 1111: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 1
Size: 434 Color: 2

Bin 1112: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 434 Color: 1

Bin 1113: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 3
Size: 434 Color: 4

Bin 1114: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 2

Bin 1115: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 3
Size: 434 Color: 4

Bin 1116: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 3

Bin 1117: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 3
Size: 434 Color: 4

Bin 1118: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 2

Bin 1119: 0 of cap free
Amount of items: 4
Items: 
Size: 568 Color: 0
Size: 153 Color: 4
Size: 141 Color: 3
Size: 139 Color: 0

Bin 1120: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 3

Bin 1121: 0 of cap free
Amount of items: 4
Items: 
Size: 568 Color: 0
Size: 154 Color: 1
Size: 140 Color: 1
Size: 139 Color: 0

Bin 1122: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 3

Bin 1123: 0 of cap free
Amount of items: 4
Items: 
Size: 568 Color: 0
Size: 154 Color: 2
Size: 140 Color: 2
Size: 139 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 289 Color: 0
Size: 144 Color: 3

Bin 1125: 0 of cap free
Amount of items: 4
Items: 
Size: 568 Color: 0
Size: 155 Color: 2
Size: 139 Color: 3
Size: 139 Color: 1

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 289 Color: 0
Size: 144 Color: 1

Bin 1127: 0 of cap free
Amount of items: 4
Items: 
Size: 568 Color: 0
Size: 155 Color: 3
Size: 139 Color: 3
Size: 139 Color: 1

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 290 Color: 0
Size: 143 Color: 4

Bin 1129: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 1

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 290 Color: 0
Size: 143 Color: 4

Bin 1131: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 1

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 295 Color: 4
Size: 138 Color: 0

Bin 1133: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 1

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 295 Color: 2
Size: 138 Color: 1

Bin 1135: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 1

Bin 1136: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 3
Size: 433 Color: 1

Bin 1137: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 3

Bin 1138: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 3
Size: 433 Color: 2

Bin 1139: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 4
Size: 433 Color: 2

Bin 1140: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 3
Size: 433 Color: 4

Bin 1141: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 4
Size: 433 Color: 3

Bin 1142: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 3
Size: 433 Color: 4

Bin 1143: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 4
Size: 433 Color: 2

Bin 1144: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 1
Size: 432 Color: 0

Bin 1145: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 4
Size: 433 Color: 3

Bin 1146: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 1
Size: 432 Color: 0

Bin 1147: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 4
Size: 433 Color: 3

Bin 1148: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 1
Size: 432 Color: 2

Bin 1149: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 0
Size: 432 Color: 1

Bin 1150: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 1
Size: 432 Color: 3

Bin 1151: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 3
Size: 432 Color: 2

Bin 1152: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 2
Size: 432 Color: 4

Bin 1153: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 3
Size: 432 Color: 4

Bin 1154: 0 of cap free
Amount of items: 4
Items: 
Size: 570 Color: 0
Size: 155 Color: 3
Size: 138 Color: 1
Size: 138 Color: 0

Bin 1155: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 4
Size: 432 Color: 2

Bin 1156: 0 of cap free
Amount of items: 4
Items: 
Size: 570 Color: 0
Size: 155 Color: 3
Size: 138 Color: 1
Size: 138 Color: 0

Bin 1157: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 4
Size: 432 Color: 3

Bin 1158: 0 of cap free
Amount of items: 4
Items: 
Size: 570 Color: 0
Size: 155 Color: 4
Size: 138 Color: 3
Size: 138 Color: 2

Bin 1159: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 4
Size: 432 Color: 3

Bin 1160: 0 of cap free
Amount of items: 4
Items: 
Size: 570 Color: 0
Size: 156 Color: 2
Size: 138 Color: 2
Size: 137 Color: 0

Bin 1161: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 4
Size: 432 Color: 3

Bin 1162: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 431 Color: 1

Bin 1163: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 431 Color: 0

Bin 1164: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 431 Color: 1

Bin 1165: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 431 Color: 0

Bin 1166: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 431 Color: 1

Bin 1167: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 431 Color: 2

Bin 1168: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 431 Color: 4

Bin 1169: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 431 Color: 4

Bin 1170: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 431 Color: 4

Bin 1171: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 431 Color: 3

Bin 1172: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 431 Color: 4

Bin 1173: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 431 Color: 2

Bin 1174: 0 of cap free
Amount of items: 4
Items: 
Size: 571 Color: 0
Size: 156 Color: 3
Size: 137 Color: 4
Size: 137 Color: 1

Bin 1175: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 431 Color: 3

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 0
Size: 288 Color: 1
Size: 142 Color: 3

Bin 1177: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 431 Color: 3

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 0
Size: 291 Color: 1
Size: 139 Color: 2

Bin 1179: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 1
Size: 430 Color: 0

Bin 1180: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 430 Color: 1

Bin 1181: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 430 Color: 0

Bin 1182: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 430 Color: 1

Bin 1183: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 430 Color: 0

Bin 1184: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 430 Color: 1

Bin 1185: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 430 Color: 3

Bin 1186: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 430 Color: 2

Bin 1187: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 430 Color: 3

Bin 1188: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 430 Color: 2

Bin 1189: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 4

Bin 1190: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 430 Color: 3

Bin 1191: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 429 Color: 1

Bin 1192: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 1
Size: 429 Color: 2

Bin 1193: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 429 Color: 1

Bin 1194: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 1
Size: 429 Color: 2

Bin 1195: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 429 Color: 2

Bin 1196: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 1
Size: 429 Color: 4

Bin 1197: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 4
Size: 429 Color: 3

Bin 1198: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 3
Size: 429 Color: 4

Bin 1199: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 4
Size: 429 Color: 3

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 0
Size: 291 Color: 1
Size: 137 Color: 1

Bin 1201: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 4

Bin 1202: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 428 Color: 0

Bin 1203: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 1

Bin 1204: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 428 Color: 4

Bin 1205: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 1

Bin 1206: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 428 Color: 4

Bin 1207: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 4

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 574 Color: 1
Size: 290 Color: 0
Size: 137 Color: 4

Bin 1209: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 4

Bin 1210: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 0

Bin 1211: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 3
Size: 428 Color: 4

Bin 1212: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 0

Bin 1213: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 3
Size: 428 Color: 4

Bin 1214: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 0

Bin 1215: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 4
Size: 428 Color: 3

Bin 1216: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 0

Bin 1217: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 3
Size: 427 Color: 0

Bin 1218: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 0

Bin 1219: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 3
Size: 427 Color: 1

Bin 1220: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 3

Bin 1221: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 3
Size: 427 Color: 2

Bin 1222: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 2

Bin 1223: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 3
Size: 427 Color: 4

Bin 1224: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 2
Size: 427 Color: 4

Bin 1225: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 3
Size: 427 Color: 4

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 0
Size: 288 Color: 1
Size: 138 Color: 3

Bin 1227: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 4
Size: 427 Color: 3

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 0
Size: 289 Color: 1
Size: 137 Color: 1

Bin 1229: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 4
Size: 427 Color: 3

Bin 1230: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 0
Size: 426 Color: 1

Bin 1231: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 2
Size: 426 Color: 1

Bin 1232: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 0
Size: 426 Color: 3

Bin 1233: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 2
Size: 426 Color: 1

Bin 1234: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 426 Color: 4

Bin 1235: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 2
Size: 426 Color: 4

Bin 1236: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 426 Color: 4

Bin 1237: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 4
Size: 426 Color: 3

Bin 1238: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 426 Color: 4

Bin 1239: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 4
Size: 426 Color: 1

Bin 1240: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 426 Color: 4

Bin 1241: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 4
Size: 426 Color: 3

Bin 1242: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 2
Size: 425 Color: 3

Bin 1243: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 0
Size: 425 Color: 2

Bin 1244: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 2
Size: 425 Color: 3

Bin 1245: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 3
Size: 425 Color: 2

Bin 1246: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 2
Size: 425 Color: 4

Bin 1247: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 3
Size: 425 Color: 4

Bin 1248: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 4
Size: 425 Color: 3

Bin 1249: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 0

Bin 1250: 0 of cap free
Amount of items: 4
Items: 
Size: 577 Color: 0
Size: 150 Color: 4
Size: 137 Color: 2
Size: 137 Color: 4

Bin 1251: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 0

Bin 1252: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 424 Color: 1

Bin 1253: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 0

Bin 1254: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 3
Size: 424 Color: 1

Bin 1255: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 2

Bin 1256: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 3
Size: 424 Color: 2

Bin 1257: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 3

Bin 1258: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 3
Size: 424 Color: 2

Bin 1259: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 2
Size: 424 Color: 4

Bin 1260: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 3
Size: 424 Color: 4

Bin 1261: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 4
Size: 424 Color: 3

Bin 1262: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 0
Size: 423 Color: 1

Bin 1263: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 4
Size: 424 Color: 3

Bin 1264: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 0
Size: 423 Color: 1

Bin 1265: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 1

Bin 1266: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 423 Color: 0

Bin 1267: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 0

Bin 1268: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 423 Color: 3

Bin 1269: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 1

Bin 1270: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 423 Color: 3

Bin 1271: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 2

Bin 1272: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 423 Color: 2

Bin 1273: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 4

Bin 1274: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 2
Size: 423 Color: 4

Bin 1275: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 4

Bin 1276: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 4
Size: 423 Color: 3

Bin 1277: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 4

Bin 1278: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 4
Size: 423 Color: 3

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 579 Color: 1
Size: 284 Color: 0
Size: 138 Color: 2

Bin 1280: 0 of cap free
Amount of items: 4
Items: 
Size: 579 Color: 0
Size: 149 Color: 4
Size: 137 Color: 4
Size: 136 Color: 0

Bin 1281: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 0

Bin 1282: 0 of cap free
Amount of items: 4
Items: 
Size: 579 Color: 0
Size: 150 Color: 4
Size: 136 Color: 1
Size: 136 Color: 0

Bin 1283: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 3

Bin 1284: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 0

Bin 1285: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 3

Bin 1286: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 1

Bin 1287: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 2

Bin 1288: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 3

Bin 1289: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 3

Bin 1290: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 3

Bin 1291: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 3
Size: 422 Color: 2

Bin 1292: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 3

Bin 1293: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 3
Size: 422 Color: 2

Bin 1294: 0 of cap free
Amount of items: 4
Items: 
Size: 580 Color: 0
Size: 149 Color: 4
Size: 136 Color: 2
Size: 136 Color: 4

Bin 1295: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 3
Size: 422 Color: 4

Bin 1296: 0 of cap free
Amount of items: 4
Items: 
Size: 580 Color: 0
Size: 150 Color: 4
Size: 136 Color: 4
Size: 135 Color: 2

Bin 1297: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 4
Size: 422 Color: 3

Bin 1298: 0 of cap free
Amount of items: 4
Items: 
Size: 580 Color: 0
Size: 151 Color: 3
Size: 135 Color: 2
Size: 135 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 580 Color: 1
Size: 286 Color: 0
Size: 135 Color: 1

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 580 Color: 0
Size: 284 Color: 1
Size: 137 Color: 2

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 580 Color: 1
Size: 287 Color: 0
Size: 134 Color: 0

Bin 1302: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 0

Bin 1303: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 421 Color: 2

Bin 1304: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 1

Bin 1305: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 421 Color: 2

Bin 1306: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 3

Bin 1307: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 421 Color: 3

Bin 1308: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 4

Bin 1309: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 4
Size: 421 Color: 2

Bin 1310: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 3
Size: 421 Color: 4

Bin 1311: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 4
Size: 421 Color: 3

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 581 Color: 1
Size: 286 Color: 0
Size: 134 Color: 0

Bin 1313: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 4
Size: 421 Color: 3

Bin 1314: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 1
Size: 420 Color: 0

Bin 1315: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 0
Size: 420 Color: 1

Bin 1316: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 1
Size: 420 Color: 0

Bin 1317: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 3
Size: 420 Color: 4

Bin 1318: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 1

Bin 1319: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 4
Size: 420 Color: 2

Bin 1320: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 1

Bin 1321: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 4
Size: 420 Color: 3

Bin 1322: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 2

Bin 1323: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 419 Color: 1

Bin 1324: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 419 Color: 2

Bin 1325: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 419 Color: 1

Bin 1326: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 3
Size: 419 Color: 2

Bin 1327: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 419 Color: 4

Bin 1328: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 3
Size: 419 Color: 4

Bin 1329: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 418 Color: 2

Bin 1330: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 4
Size: 419 Color: 3

Bin 1331: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 418 Color: 4

Bin 1332: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 418 Color: 4

Bin 1333: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 418 Color: 4

Bin 1334: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 4

Bin 1335: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 4
Size: 418 Color: 3

Bin 1336: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 4

Bin 1337: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 4
Size: 418 Color: 3

Bin 1338: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 4

Bin 1339: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 4
Size: 418 Color: 3

Bin 1340: 0 of cap free
Amount of items: 4
Items: 
Size: 584 Color: 0
Size: 148 Color: 4
Size: 135 Color: 2
Size: 134 Color: 0

Bin 1341: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 4
Size: 418 Color: 3

Bin 1342: 0 of cap free
Amount of items: 4
Items: 
Size: 584 Color: 0
Size: 148 Color: 4
Size: 135 Color: 1
Size: 134 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 2
Size: 277 Color: 0
Size: 140 Color: 3

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 0
Size: 282 Color: 1
Size: 135 Color: 2

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 2
Size: 284 Color: 0
Size: 133 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 3
Size: 284 Color: 1
Size: 133 Color: 0

Bin 1347: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 417 Color: 1

Bin 1348: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 417 Color: 1

Bin 1349: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 417 Color: 1

Bin 1350: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 417 Color: 0

Bin 1351: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 417 Color: 1

Bin 1352: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 417 Color: 0

Bin 1353: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 417 Color: 4

Bin 1354: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 417 Color: 4

Bin 1355: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 417 Color: 1

Bin 1356: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 417 Color: 4

Bin 1357: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 417 Color: 3

Bin 1358: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 416 Color: 1

Bin 1359: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 417 Color: 3

Bin 1360: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 416 Color: 1

Bin 1361: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 1
Size: 416 Color: 0

Bin 1362: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 2
Size: 416 Color: 0

Bin 1363: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 416 Color: 4

Bin 1364: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 2
Size: 416 Color: 4

Bin 1365: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 416 Color: 4

Bin 1366: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 4
Size: 416 Color: 1

Bin 1367: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 416 Color: 4

Bin 1368: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 4
Size: 416 Color: 3

Bin 1369: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 415 Color: 0

Bin 1370: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 0
Size: 415 Color: 1

Bin 1371: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 415 Color: 2

Bin 1372: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 1

Bin 1373: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 415 Color: 2

Bin 1374: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 4

Bin 1375: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 415 Color: 4

Bin 1376: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 4

Bin 1377: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 3
Size: 415 Color: 4

Bin 1378: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 2

Bin 1379: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 3
Size: 415 Color: 4

Bin 1380: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 3

Bin 1381: 0 of cap free
Amount of items: 4
Items: 
Size: 587 Color: 0
Size: 146 Color: 2
Size: 135 Color: 4
Size: 133 Color: 0

Bin 1382: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 3

Bin 1383: 0 of cap free
Amount of items: 4
Items: 
Size: 587 Color: 0
Size: 146 Color: 2
Size: 135 Color: 2
Size: 133 Color: 0

Bin 1384: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 3

Bin 1385: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 0
Size: 414 Color: 1

Bin 1386: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 3

Bin 1387: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 0
Size: 414 Color: 1

Bin 1388: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 3

Bin 1389: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 3
Size: 414 Color: 1

Bin 1390: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 3

Bin 1391: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 3
Size: 414 Color: 2

Bin 1392: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 2
Size: 414 Color: 3

Bin 1393: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 3
Size: 414 Color: 4

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 1
Size: 278 Color: 0
Size: 135 Color: 4

Bin 1395: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 3
Size: 414 Color: 4

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 1
Size: 279 Color: 0
Size: 134 Color: 0

Bin 1397: 0 of cap free
Amount of items: 4
Items: 
Size: 588 Color: 0
Size: 147 Color: 1
Size: 133 Color: 3
Size: 133 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 1
Size: 279 Color: 0
Size: 134 Color: 0

Bin 1399: 0 of cap free
Amount of items: 4
Items: 
Size: 588 Color: 0
Size: 147 Color: 4
Size: 133 Color: 3
Size: 133 Color: 1

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 1
Size: 279 Color: 0
Size: 134 Color: 1

Bin 1401: 0 of cap free
Amount of items: 4
Items: 
Size: 588 Color: 0
Size: 147 Color: 2
Size: 133 Color: 3
Size: 133 Color: 2

Bin 1402: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 1
Size: 413 Color: 0

Bin 1403: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 2
Size: 413 Color: 0

Bin 1404: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 1
Size: 413 Color: 0

Bin 1405: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 2
Size: 413 Color: 1

Bin 1406: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 413 Color: 2

Bin 1407: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 2
Size: 413 Color: 1

Bin 1408: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 413 Color: 2

Bin 1409: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 3

Bin 1410: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 413 Color: 2

Bin 1411: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 3

Bin 1412: 0 of cap free
Amount of items: 4
Items: 
Size: 589 Color: 0
Size: 146 Color: 1
Size: 133 Color: 3
Size: 133 Color: 2

Bin 1413: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 2

Bin 1414: 0 of cap free
Amount of items: 4
Items: 
Size: 589 Color: 0
Size: 146 Color: 2
Size: 133 Color: 3
Size: 133 Color: 2

Bin 1415: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 3

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 0
Size: 278 Color: 1
Size: 134 Color: 3

Bin 1417: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 1
Size: 412 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 0
Size: 278 Color: 1
Size: 134 Color: 1

Bin 1419: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 1
Size: 412 Color: 0

Bin 1420: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 0
Size: 412 Color: 1

Bin 1421: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 1
Size: 412 Color: 0

Bin 1422: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 0
Size: 412 Color: 1

Bin 1423: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 1
Size: 412 Color: 2

Bin 1424: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 3
Size: 412 Color: 4

Bin 1425: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 2
Size: 412 Color: 4

Bin 1426: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 3
Size: 412 Color: 4

Bin 1427: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 0

Bin 1428: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 3
Size: 412 Color: 4

Bin 1429: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 0

Bin 1430: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 4
Size: 412 Color: 2

Bin 1431: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 0

Bin 1432: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 4
Size: 412 Color: 3

Bin 1433: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 0

Bin 1434: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 4
Size: 412 Color: 3

Bin 1435: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 2

Bin 1436: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 411 Color: 1

Bin 1437: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 2

Bin 1438: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 411 Color: 2

Bin 1439: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 2

Bin 1440: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 411 Color: 2

Bin 1441: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 3

Bin 1442: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 3
Size: 411 Color: 4

Bin 1443: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 2
Size: 411 Color: 4

Bin 1444: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 3
Size: 411 Color: 4

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 0
Size: 270 Color: 1
Size: 140 Color: 4

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 1
Size: 277 Color: 0
Size: 133 Color: 2

Bin 1447: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 410 Color: 1

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 1
Size: 277 Color: 0
Size: 133 Color: 3

Bin 1449: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 410 Color: 1

Bin 1450: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 410 Color: 0

Bin 1451: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 410 Color: 1

Bin 1452: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 410 Color: 0

Bin 1453: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 410 Color: 1

Bin 1454: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 410 Color: 4

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 592 Color: 0
Size: 277 Color: 1
Size: 132 Color: 2

Bin 1456: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 4
Size: 410 Color: 2

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 592 Color: 0
Size: 277 Color: 2
Size: 132 Color: 2

Bin 1458: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 4
Size: 410 Color: 3

Bin 1459: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 0
Size: 409 Color: 1

Bin 1460: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 409 Color: 2

Bin 1461: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 409 Color: 1

Bin 1462: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 409 Color: 2

Bin 1463: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 409 Color: 1

Bin 1464: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 409 Color: 3

Bin 1465: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 3
Size: 409 Color: 4

Bin 1466: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 409 Color: 2

Bin 1467: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 3
Size: 409 Color: 4

Bin 1468: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 409 Color: 3

Bin 1469: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 3
Size: 409 Color: 4

Bin 1470: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 409 Color: 3

Bin 1471: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 3
Size: 409 Color: 4

Bin 1472: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 409 Color: 3

Bin 1473: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 0
Size: 408 Color: 1

Bin 1474: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 2

Bin 1475: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 0
Size: 408 Color: 3

Bin 1476: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 2

Bin 1477: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 408 Color: 3

Bin 1478: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 2

Bin 1479: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 2
Size: 408 Color: 3

Bin 1480: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 2

Bin 1481: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 3
Size: 408 Color: 4

Bin 1482: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 3

Bin 1483: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 3
Size: 408 Color: 4

Bin 1484: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 3

Bin 1485: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 2
Size: 407 Color: 0

Bin 1486: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 0
Size: 407 Color: 2

Bin 1487: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 2
Size: 407 Color: 1

Bin 1488: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 407 Color: 2

Bin 1489: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 2
Size: 407 Color: 1

Bin 1490: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 407 Color: 2

Bin 1491: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 2
Size: 407 Color: 1

Bin 1492: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 407 Color: 2

Bin 1493: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 2
Size: 407 Color: 3

Bin 1494: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 407 Color: 2

Bin 1495: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 2
Size: 407 Color: 3

Bin 1496: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 3
Size: 407 Color: 4

Bin 1497: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 407 Color: 2

Bin 1498: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 3
Size: 407 Color: 4

Bin 1499: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 407 Color: 3

Bin 1500: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 3
Size: 407 Color: 4

Bin 1501: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 407 Color: 3

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 0
Size: 270 Color: 1
Size: 136 Color: 2

Bin 1503: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 407 Color: 3

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 0
Size: 270 Color: 1
Size: 136 Color: 4

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 1
Size: 275 Color: 0
Size: 131 Color: 0

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 0
Size: 274 Color: 1
Size: 132 Color: 2

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 1
Size: 275 Color: 0
Size: 131 Color: 0

Bin 1508: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 0

Bin 1509: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 0

Bin 1510: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 0

Bin 1511: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 0

Bin 1512: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 3

Bin 1513: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 1

Bin 1514: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 3

Bin 1515: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 2

Bin 1516: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 3

Bin 1517: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 4

Bin 1518: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 3

Bin 1519: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 4

Bin 1520: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 1
Size: 405 Color: 0

Bin 1521: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 0
Size: 405 Color: 1

Bin 1522: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 1
Size: 405 Color: 0

Bin 1523: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 0
Size: 405 Color: 1

Bin 1524: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 1
Size: 405 Color: 3

Bin 1525: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 2

Bin 1526: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 1
Size: 405 Color: 2

Bin 1527: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 2

Bin 1528: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 405 Color: 3

Bin 1529: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 4

Bin 1530: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 405 Color: 4

Bin 1531: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 4

Bin 1532: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 4
Size: 405 Color: 3

Bin 1533: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 4

Bin 1534: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 4
Size: 405 Color: 2

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 0
Size: 273 Color: 1
Size: 131 Color: 0

Bin 1536: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 4
Size: 405 Color: 3

Bin 1537: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 404 Color: 1

Bin 1538: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 0

Bin 1539: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 1
Size: 404 Color: 3

Bin 1540: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 1

Bin 1541: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 1
Size: 404 Color: 3

Bin 1542: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 2

Bin 1543: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 2
Size: 404 Color: 3

Bin 1544: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 2

Bin 1545: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 2
Size: 404 Color: 3

Bin 1546: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 4

Bin 1547: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 2
Size: 404 Color: 4

Bin 1548: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 4

Bin 1549: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 4
Size: 404 Color: 3

Bin 1550: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 0
Size: 403 Color: 2

Bin 1551: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 403 Color: 1

Bin 1552: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 0
Size: 403 Color: 2

Bin 1553: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 403 Color: 3

Bin 1554: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 0
Size: 403 Color: 2

Bin 1555: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 403 Color: 3

Bin 1556: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 4

Bin 1557: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 403 Color: 4

Bin 1558: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 4

Bin 1559: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 403 Color: 4

Bin 1560: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 4
Size: 403 Color: 3

Bin 1561: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 3
Size: 403 Color: 4

Bin 1562: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 4
Size: 403 Color: 3

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 1
Size: 270 Color: 0
Size: 132 Color: 0

Bin 1564: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 402 Color: 1

Bin 1565: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 402 Color: 0

Bin 1566: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 402 Color: 2

Bin 1567: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 402 Color: 2

Bin 1568: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 402 Color: 1

Bin 1569: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 402 Color: 4

Bin 1570: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 402 Color: 4

Bin 1571: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 4
Size: 402 Color: 3

Bin 1572: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 3
Size: 402 Color: 4

Bin 1573: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 4
Size: 402 Color: 3

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 600 Color: 0
Size: 270 Color: 1
Size: 131 Color: 0

Bin 1575: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 1

Bin 1576: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 401 Color: 1

Bin 1577: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 1

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 600 Color: 1
Size: 270 Color: 0
Size: 131 Color: 1

Bin 1579: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 0

Bin 1580: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 3
Size: 401 Color: 1

Bin 1581: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 0

Bin 1582: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 3
Size: 401 Color: 4

Bin 1583: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 1

Bin 1584: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 400 Color: 3

Bin 1585: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 0

Bin 1586: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 400 Color: 1

Bin 1587: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 1

Bin 1588: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 400 Color: 3

Bin 1589: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 1

Bin 1590: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 1
Size: 400 Color: 2

Bin 1591: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 3

Bin 1592: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 1
Size: 400 Color: 3

Bin 1593: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 4

Bin 1594: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 4
Size: 400 Color: 2

Bin 1595: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 3
Size: 400 Color: 4

Bin 1596: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 4
Size: 400 Color: 3

Bin 1597: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 3
Size: 400 Color: 4

Bin 1598: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 4
Size: 400 Color: 2

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 1
Size: 264 Color: 0
Size: 135 Color: 3

Bin 1600: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 4
Size: 400 Color: 3

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 1
Size: 264 Color: 0
Size: 135 Color: 4

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 0
Size: 257 Color: 1
Size: 142 Color: 4

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 1
Size: 265 Color: 0
Size: 134 Color: 3

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 0
Size: 257 Color: 1
Size: 142 Color: 4

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 1
Size: 268 Color: 0
Size: 131 Color: 2

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 0
Size: 260 Color: 1
Size: 139 Color: 3

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 1
Size: 268 Color: 0
Size: 131 Color: 1

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 0
Size: 260 Color: 1
Size: 139 Color: 2

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 1
Size: 270 Color: 0
Size: 129 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 0
Size: 270 Color: 2
Size: 129 Color: 0

Bin 1611: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 399 Color: 2

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 2
Size: 270 Color: 1
Size: 129 Color: 0

Bin 1613: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 3
Size: 399 Color: 2

Bin 1614: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 2
Size: 399 Color: 0

Bin 1615: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 3
Size: 399 Color: 2

Bin 1616: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 2
Size: 399 Color: 1

Bin 1617: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 3
Size: 399 Color: 4

Bin 1618: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 4
Size: 399 Color: 3

Bin 1619: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 3
Size: 399 Color: 4

Bin 1620: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 4
Size: 399 Color: 3

Bin 1621: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 0
Size: 398 Color: 1

Bin 1622: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 0

Bin 1623: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 1

Bin 1624: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 3

Bin 1625: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 3

Bin 1626: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 3
Size: 398 Color: 2

Bin 1627: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 3

Bin 1628: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 3
Size: 398 Color: 4

Bin 1629: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 4

Bin 1630: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 3
Size: 398 Color: 4

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 1
Size: 268 Color: 0
Size: 129 Color: 0

Bin 1632: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 398 Color: 3

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 1
Size: 268 Color: 0
Size: 129 Color: 0

Bin 1634: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 1

Bin 1635: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 397 Color: 0

Bin 1636: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 1

Bin 1637: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 397 Color: 0

Bin 1638: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 3

Bin 1639: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 397 Color: 2

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 0
Size: 260 Color: 1
Size: 136 Color: 2

Bin 1641: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 2
Size: 397 Color: 3

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 0
Size: 260 Color: 2
Size: 136 Color: 4

Bin 1643: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 397 Color: 2

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 0
Size: 264 Color: 3
Size: 132 Color: 2

Bin 1645: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 397 Color: 4

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 0
Size: 265 Color: 1
Size: 131 Color: 2

Bin 1647: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 397 Color: 4

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 0
Size: 265 Color: 1
Size: 131 Color: 3

Bin 1649: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 4
Size: 397 Color: 3

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 0
Size: 265 Color: 2
Size: 131 Color: 2

Bin 1651: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 1
Size: 396 Color: 0

Bin 1652: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 2
Size: 396 Color: 1

Bin 1653: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 1
Size: 396 Color: 0

Bin 1654: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 2
Size: 396 Color: 0

Bin 1655: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 3
Size: 396 Color: 2

Bin 1656: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 2
Size: 396 Color: 1

Bin 1657: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 3
Size: 396 Color: 1

Bin 1658: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 2
Size: 396 Color: 3

Bin 1659: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 3
Size: 396 Color: 2

Bin 1660: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 4
Size: 396 Color: 3

Bin 1661: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 3
Size: 396 Color: 2

Bin 1662: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 4
Size: 396 Color: 2

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 606 Color: 0
Size: 266 Color: 1
Size: 129 Color: 2

Bin 1664: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 4
Size: 396 Color: 3

Bin 1665: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 395 Color: 2

Bin 1666: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 0

Bin 1667: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 2

Bin 1668: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 0

Bin 1669: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 2

Bin 1670: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 0

Bin 1671: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 2

Bin 1672: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 1

Bin 1673: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 395 Color: 4

Bin 1674: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 2

Bin 1675: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 395 Color: 4

Bin 1676: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 1

Bin 1677: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 395 Color: 4

Bin 1678: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 2

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 0
Size: 265 Color: 1
Size: 129 Color: 1

Bin 1680: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 1

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 0
Size: 265 Color: 2
Size: 129 Color: 2

Bin 1682: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 2

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 0
Size: 265 Color: 1
Size: 129 Color: 1

Bin 1684: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 3

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 0
Size: 265 Color: 2
Size: 129 Color: 2

Bin 1686: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 0

Bin 1687: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 0
Size: 394 Color: 1

Bin 1688: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 3

Bin 1689: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 0
Size: 394 Color: 2

Bin 1690: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 3

Bin 1691: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 3
Size: 394 Color: 2

Bin 1692: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 3

Bin 1693: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 3
Size: 394 Color: 4

Bin 1694: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 2

Bin 1695: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 3
Size: 394 Color: 4

Bin 1696: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 3

Bin 1697: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 3
Size: 394 Color: 4

Bin 1698: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 3

Bin 1699: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 393 Color: 2

Bin 1700: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 3

Bin 1701: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 393 Color: 2

Bin 1702: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 393 Color: 2

Bin 1703: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 393 Color: 2

Bin 1704: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 393 Color: 1

Bin 1705: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 393 Color: 2

Bin 1706: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 393 Color: 1

Bin 1707: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 2

Bin 1708: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 393 Color: 3

Bin 1709: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 2

Bin 1710: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 393 Color: 3

Bin 1711: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 4

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 0
Size: 260 Color: 1
Size: 132 Color: 1

Bin 1713: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 4

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 0
Size: 260 Color: 2
Size: 132 Color: 2

Bin 1715: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 4

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 0
Size: 261 Color: 1
Size: 131 Color: 3

Bin 1717: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 4
Size: 393 Color: 3

Bin 1718: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 392 Color: 1

Bin 1719: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 0

Bin 1720: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 392 Color: 1

Bin 1721: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 0

Bin 1722: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 1

Bin 1723: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 0

Bin 1724: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 1

Bin 1725: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 4

Bin 1726: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 2

Bin 1727: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 4

Bin 1728: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 2

Bin 1729: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 3

Bin 1730: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 4

Bin 1731: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 4

Bin 1732: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 4

Bin 1733: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 0
Size: 391 Color: 1

Bin 1734: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 4

Bin 1735: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 0
Size: 391 Color: 1

Bin 1736: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 391 Color: 1

Bin 1737: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 1
Size: 391 Color: 2

Bin 1738: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 391 Color: 3

Bin 1739: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 3
Size: 391 Color: 2

Bin 1740: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 391 Color: 3

Bin 1741: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 3
Size: 391 Color: 2

Bin 1742: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 391 Color: 3

Bin 1743: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 3
Size: 391 Color: 2

Bin 1744: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 4
Size: 391 Color: 3

Bin 1745: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 0
Size: 261 Color: 1
Size: 129 Color: 3

Bin 1747: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 0

Bin 1748: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 390 Color: 1

Bin 1749: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 0

Bin 1750: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 390 Color: 1

Bin 1751: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 2

Bin 1752: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 390 Color: 1

Bin 1753: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 2

Bin 1754: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 390 Color: 1

Bin 1755: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 3
Size: 390 Color: 2

Bin 1756: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 390 Color: 1

Bin 1757: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 3
Size: 390 Color: 4

Bin 1758: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 390 Color: 2

Bin 1759: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 3
Size: 390 Color: 4

Bin 1760: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 390 Color: 1

Bin 1761: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 389 Color: 1

Bin 1762: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 390 Color: 2

Bin 1763: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 389 Color: 1

Bin 1764: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 390 Color: 3

Bin 1765: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 389 Color: 1

Bin 1766: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 1
Size: 389 Color: 2

Bin 1767: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 389 Color: 1

Bin 1768: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 1
Size: 389 Color: 2

Bin 1769: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 1

Bin 1770: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 1
Size: 389 Color: 2

Bin 1771: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 4

Bin 1772: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 4
Size: 389 Color: 2

Bin 1773: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 4

Bin 1774: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 4
Size: 389 Color: 3

Bin 1775: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 3
Size: 389 Color: 4

Bin 1776: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 4
Size: 389 Color: 2

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 2
Size: 257 Color: 0
Size: 131 Color: 2

Bin 1778: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 4
Size: 389 Color: 3

Bin 1779: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 2
Size: 388 Color: 0

Bin 1780: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 1
Size: 388 Color: 2

Bin 1781: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 2
Size: 388 Color: 1

Bin 1782: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 3
Size: 388 Color: 2

Bin 1783: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 2
Size: 388 Color: 4

Bin 1784: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 3
Size: 388 Color: 4

Bin 1785: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 4
Size: 388 Color: 1

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 0
Size: 254 Color: 1
Size: 133 Color: 4

Bin 1787: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 4
Size: 388 Color: 2

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 0
Size: 254 Color: 1
Size: 133 Color: 4

Bin 1789: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 4
Size: 388 Color: 3

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 0
Size: 258 Color: 1
Size: 129 Color: 2

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 3
Size: 259 Color: 0
Size: 128 Color: 0

Bin 1792: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 387 Color: 0

Bin 1793: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 387 Color: 1

Bin 1794: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 2
Size: 387 Color: 0

Bin 1795: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 387 Color: 1

Bin 1796: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 2
Size: 387 Color: 0

Bin 1797: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 387 Color: 1

Bin 1798: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 387 Color: 3

Bin 1799: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 387 Color: 2

Bin 1800: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 387 Color: 3

Bin 1801: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 387 Color: 2

Bin 1802: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 387 Color: 3

Bin 1803: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 0
Size: 386 Color: 1

Bin 1804: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 2

Bin 1805: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 0
Size: 386 Color: 1

Bin 1806: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 2

Bin 1807: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 386 Color: 1

Bin 1808: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 3

Bin 1809: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 386 Color: 2

Bin 1810: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 2
Size: 386 Color: 4

Bin 1811: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 386 Color: 4

Bin 1812: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 4
Size: 386 Color: 3

Bin 1813: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 386 Color: 4

Bin 1814: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 4
Size: 386 Color: 3

Bin 1815: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 0
Size: 385 Color: 2

Bin 1816: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 385 Color: 1

Bin 1817: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 0
Size: 385 Color: 2

Bin 1818: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 385 Color: 1

Bin 1819: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 0
Size: 385 Color: 2

Bin 1820: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 385 Color: 1

Bin 1821: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 4
Size: 385 Color: 1

Bin 1822: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 3
Size: 385 Color: 4

Bin 1823: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 4
Size: 385 Color: 2

Bin 1824: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 3
Size: 385 Color: 4

Bin 1825: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 4
Size: 385 Color: 3

Bin 1826: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 384 Color: 1

Bin 1827: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 1
Size: 384 Color: 0

Bin 1828: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 384 Color: 1

Bin 1829: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 1
Size: 384 Color: 2

Bin 1830: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 2
Size: 384 Color: 4

Bin 1831: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 384 Color: 2

Bin 1832: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 2
Size: 384 Color: 4

Bin 1833: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 384 Color: 4

Bin 1834: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 4
Size: 384 Color: 2

Bin 1835: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 3
Size: 384 Color: 4

Bin 1836: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 4
Size: 384 Color: 3

Bin 1837: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 3

Bin 1838: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 0
Size: 383 Color: 1

Bin 1839: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 3

Bin 1840: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 0
Size: 383 Color: 1

Bin 1841: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 3

Bin 1842: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 2

Bin 1843: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 3

Bin 1844: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 2

Bin 1845: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 2
Size: 383 Color: 4

Bin 1846: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 4

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 0
Size: 254 Color: 1
Size: 128 Color: 0

Bin 1848: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 4

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 0
Size: 254 Color: 3
Size: 128 Color: 0

Bin 1850: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 4
Size: 383 Color: 3

Bin 1851: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 382 Color: 1

Bin 1852: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 382 Color: 0

Bin 1853: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 382 Color: 1

Bin 1854: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 382 Color: 3

Bin 1855: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 1

Bin 1856: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 2
Size: 382 Color: 3

Bin 1857: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 2

Bin 1858: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 2
Size: 382 Color: 4

Bin 1859: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 4

Bin 1860: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 382 Color: 2

Bin 1861: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 4

Bin 1862: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 382 Color: 3

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 0
Size: 254 Color: 1
Size: 127 Color: 0

Bin 1864: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 382 Color: 3

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 0
Size: 254 Color: 3
Size: 127 Color: 0

Bin 1866: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 1
Size: 381 Color: 2

Bin 1867: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 2

Bin 1868: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 2
Size: 381 Color: 1

Bin 1869: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 2

Bin 1870: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 2
Size: 381 Color: 1

Bin 1871: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 1

Bin 1872: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 2
Size: 381 Color: 3

Bin 1873: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 2

Bin 1874: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 2
Size: 381 Color: 4

Bin 1875: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 4

Bin 1876: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 4
Size: 381 Color: 3

Bin 1877: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 4

Bin 1878: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 4
Size: 381 Color: 3

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 0
Size: 196 Color: 1
Size: 184 Color: 4

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 1
Size: 245 Color: 0
Size: 135 Color: 4

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 0
Size: 196 Color: 1
Size: 184 Color: 3

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 1
Size: 246 Color: 0
Size: 134 Color: 2

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 0
Size: 196 Color: 1
Size: 184 Color: 4

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 1
Size: 246 Color: 0
Size: 134 Color: 3

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 2
Size: 246 Color: 0
Size: 134 Color: 4

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 4
Size: 247 Color: 0
Size: 133 Color: 4

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 3
Size: 247 Color: 0
Size: 133 Color: 4

Bin 1888: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 1

Bin 1889: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 3
Size: 380 Color: 4

Bin 1890: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 0

Bin 1891: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 3
Size: 380 Color: 4

Bin 1892: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 1

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 0
Size: 196 Color: 1
Size: 183 Color: 2

Bin 1894: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 3

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 0
Size: 196 Color: 1
Size: 183 Color: 4

Bin 1896: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 2

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 0
Size: 196 Color: 1
Size: 183 Color: 4

Bin 1898: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 3

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 0
Size: 196 Color: 4
Size: 183 Color: 4

Bin 1900: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 3

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 0
Size: 196 Color: 2
Size: 183 Color: 4

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 2
Size: 247 Color: 0
Size: 132 Color: 1

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 1
Size: 248 Color: 0
Size: 131 Color: 3

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 2
Size: 248 Color: 0
Size: 131 Color: 4

Bin 1905: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 1
Size: 379 Color: 0

Bin 1906: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 2
Size: 379 Color: 0

Bin 1907: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 1
Size: 379 Color: 0

Bin 1908: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 2
Size: 379 Color: 1

Bin 1909: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 1
Size: 379 Color: 0

Bin 1910: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 2
Size: 379 Color: 3

Bin 1911: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 1

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 0
Size: 197 Color: 2
Size: 181 Color: 2

Bin 1913: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 2

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 0
Size: 197 Color: 1
Size: 181 Color: 3

Bin 1915: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 4

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 0
Size: 197 Color: 2
Size: 181 Color: 2

Bin 1917: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 379 Color: 3

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 0
Size: 197 Color: 3
Size: 181 Color: 3

Bin 1919: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 379 Color: 2

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 0
Size: 197 Color: 2
Size: 181 Color: 4

Bin 1921: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 379 Color: 3

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 0
Size: 197 Color: 3
Size: 181 Color: 3

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 1
Size: 248 Color: 0
Size: 130 Color: 0

Bin 1924: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 2
Size: 378 Color: 1

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 1
Size: 251 Color: 0
Size: 127 Color: 0

Bin 1926: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 2
Size: 378 Color: 1

Bin 1927: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 1
Size: 378 Color: 2

Bin 1928: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 2
Size: 378 Color: 1

Bin 1929: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 1
Size: 378 Color: 2

Bin 1930: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 2
Size: 378 Color: 1

Bin 1931: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 1
Size: 378 Color: 2

Bin 1932: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 2
Size: 378 Color: 3

Bin 1933: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 3
Size: 378 Color: 4

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 197 Color: 2
Size: 180 Color: 3

Bin 1935: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 4
Size: 378 Color: 2

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 239 Color: 2
Size: 138 Color: 3

Bin 1937: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 4
Size: 378 Color: 3

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 239 Color: 2
Size: 138 Color: 3

Bin 1939: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 4
Size: 378 Color: 3

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 239 Color: 2
Size: 138 Color: 4

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 1
Size: 249 Color: 0
Size: 128 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 250 Color: 1
Size: 127 Color: 1

Bin 1943: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 377 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 250 Color: 1
Size: 127 Color: 0

Bin 1945: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 377 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 251 Color: 1
Size: 126 Color: 0

Bin 1947: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 377 Color: 0

Bin 1948: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 1

Bin 1949: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 377 Color: 0

Bin 1950: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 0

Bin 1951: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 377 Color: 4

Bin 1952: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 4

Bin 1953: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 1
Size: 376 Color: 2

Bin 1954: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 4

Bin 1955: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 1
Size: 376 Color: 2

Bin 1956: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 4
Size: 377 Color: 1

Bin 1957: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 1
Size: 376 Color: 2

Bin 1958: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 4
Size: 377 Color: 2

Bin 1959: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 1
Size: 376 Color: 2

Bin 1960: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 0
Size: 376 Color: 4

Bin 1961: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 1
Size: 376 Color: 4

Bin 1962: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 376 Color: 4

Bin 1963: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 2

Bin 1964: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 3

Bin 1965: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 2

Bin 1966: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 2

Bin 1967: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 1

Bin 1968: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 3

Bin 1969: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 2

Bin 1970: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 1
Size: 375 Color: 2

Bin 1971: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 2
Size: 375 Color: 1

Bin 1972: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 1
Size: 375 Color: 2

Bin 1973: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 2
Size: 375 Color: 1

Bin 1974: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 4
Size: 375 Color: 3

Bin 1975: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 3
Size: 375 Color: 4

Bin 1976: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 4
Size: 375 Color: 3

Bin 1977: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 374 Color: 0

Bin 1978: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 374 Color: 1

Bin 1979: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 374 Color: 2

Bin 1980: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 374 Color: 2

Bin 1981: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 374 Color: 2

Bin 1982: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 2
Size: 374 Color: 3

Bin 1983: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 3
Size: 374 Color: 4

Bin 1984: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 2
Size: 374 Color: 4

Bin 1985: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 3
Size: 374 Color: 4

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 0
Size: 247 Color: 1
Size: 126 Color: 1

Bin 1987: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 4
Size: 374 Color: 3

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 0
Size: 247 Color: 2
Size: 126 Color: 2

Bin 1989: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 4
Size: 374 Color: 3

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 0
Size: 247 Color: 1
Size: 126 Color: 1

Bin 1991: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 4
Size: 374 Color: 3

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 0
Size: 247 Color: 2
Size: 126 Color: 2

Bin 1993: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 2
Size: 373 Color: 0

Bin 1994: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 373 Color: 1

Bin 1995: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 2
Size: 373 Color: 1

Bin 1996: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 1
Size: 373 Color: 0

Bin 1997: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 2
Size: 373 Color: 1

Bin 1998: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 1
Size: 373 Color: 2

Bin 1999: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 2
Size: 373 Color: 4

Bin 2000: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 4
Size: 373 Color: 1

Bin 2001: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 3
Size: 373 Color: 4

Bin 2002: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 4
Size: 373 Color: 2

Bin 2003: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 3
Size: 373 Color: 4

Bin 2004: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 4
Size: 373 Color: 3

Bin 2005: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 3
Size: 373 Color: 4

Bin 2006: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 4
Size: 373 Color: 3

Bin 2007: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 0
Size: 372 Color: 1

Bin 2008: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 2

Bin 2009: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 1
Size: 372 Color: 4

Bin 2010: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 2

Bin 2011: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 2
Size: 372 Color: 4

Bin 2012: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 4

Bin 2013: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 372 Color: 2

Bin 2014: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 4

Bin 2015: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 372 Color: 3

Bin 2016: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 371 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 630 Color: 0
Size: 240 Color: 2
Size: 131 Color: 4

Bin 2018: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 371 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 630 Color: 0
Size: 244 Color: 2
Size: 127 Color: 1

Bin 2020: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 371 Color: 0

Bin 2021: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 2
Size: 371 Color: 3

Bin 2022: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 3

Bin 2023: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 371 Color: 0

Bin 2024: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 3

Bin 2025: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 371 Color: 4

Bin 2026: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 3

Bin 2027: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 0
Size: 370 Color: 2

Bin 2028: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 2

Bin 2029: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 2
Size: 370 Color: 1

Bin 2030: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 2

Bin 2031: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 2
Size: 370 Color: 3

Bin 2032: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 3

Bin 2033: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 2
Size: 370 Color: 4

Bin 2034: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 1

Bin 2035: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 3
Size: 370 Color: 4

Bin 2036: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 3

Bin 2037: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 3
Size: 370 Color: 4

Bin 2038: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 2

Bin 2039: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 3
Size: 370 Color: 4

Bin 2040: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 3

Bin 2041: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 4
Size: 370 Color: 3

Bin 2042: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 2

Bin 2043: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 2
Size: 369 Color: 4

Bin 2044: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 369 Color: 4

Bin 2045: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 2
Size: 369 Color: 4

Bin 2046: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 3
Size: 369 Color: 4

Bin 2047: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 3

Bin 2048: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 3
Size: 369 Color: 4

Bin 2049: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 3

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 0
Size: 240 Color: 2
Size: 128 Color: 1

Bin 2051: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 3

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 0
Size: 240 Color: 1
Size: 128 Color: 2

Bin 2053: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 3

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 0
Size: 240 Color: 1
Size: 128 Color: 1

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 2
Size: 241 Color: 0
Size: 127 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 0
Size: 241 Color: 1
Size: 127 Color: 1

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 2
Size: 241 Color: 0
Size: 127 Color: 2

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 0
Size: 241 Color: 1
Size: 127 Color: 1

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 2
Size: 241 Color: 0
Size: 127 Color: 2

Bin 2060: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 1
Size: 368 Color: 0

Bin 2061: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 1

Bin 2062: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 1
Size: 368 Color: 0

Bin 2063: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 3

Bin 2064: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 368 Color: 2

Bin 2065: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 368 Color: 4

Bin 2066: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 368 Color: 3

Bin 2067: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 368 Color: 4

Bin 2068: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 368 Color: 2

Bin 2069: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 367 Color: 2

Bin 2070: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 368 Color: 3

Bin 2071: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 367 Color: 2

Bin 2072: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 367 Color: 4

Bin 2073: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 367 Color: 4

Bin 2074: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 367 Color: 4

Bin 2075: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 4
Size: 367 Color: 2

Bin 2076: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 3
Size: 367 Color: 4

Bin 2077: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 4
Size: 367 Color: 2

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 1
Size: 240 Color: 2
Size: 126 Color: 4

Bin 2079: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 0
Size: 366 Color: 1

Bin 2080: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 366 Color: 0

Bin 2081: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 366 Color: 1

Bin 2082: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 366 Color: 0

Bin 2083: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 366 Color: 1

Bin 2084: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 366 Color: 2

Bin 2085: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 366 Color: 4

Bin 2086: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 366 Color: 4

Bin 2087: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 366 Color: 4

Bin 2088: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 366 Color: 4

Bin 2089: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 366 Color: 3

Bin 2090: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 2

Bin 2091: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 366 Color: 3

Bin 2092: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 4

Bin 2093: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 366 Color: 3

Bin 2094: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 4

Bin 2095: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 2
Size: 365 Color: 4

Bin 2096: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 4

Bin 2097: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 2
Size: 365 Color: 4

Bin 2098: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 1
Size: 364 Color: 0

Bin 2099: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 4
Size: 365 Color: 3

Bin 2100: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 1
Size: 364 Color: 0

Bin 2101: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 364 Color: 0

Bin 2102: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 1
Size: 364 Color: 0

Bin 2103: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 364 Color: 1

Bin 2104: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 2

Bin 2105: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 364 Color: 3

Bin 2106: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 4

Bin 2107: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 3

Bin 2108: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 4

Bin 2109: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 2

Bin 2110: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 4

Bin 2111: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 3

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 0
Size: 237 Color: 1
Size: 126 Color: 3

Bin 2113: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 3

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 0
Size: 237 Color: 1
Size: 126 Color: 4

Bin 2115: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 1
Size: 363 Color: 0

Bin 2116: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 0

Bin 2117: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 1
Size: 363 Color: 0

Bin 2118: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 0

Bin 2119: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 1
Size: 363 Color: 2

Bin 2120: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 0

Bin 2121: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 2

Bin 2122: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 1

Bin 2123: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 2

Bin 2124: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 1

Bin 2125: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 2

Bin 2126: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 1

Bin 2127: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 4

Bin 2128: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 4
Size: 363 Color: 2

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 0
Size: 223 Color: 1
Size: 139 Color: 3

Bin 2130: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 4
Size: 363 Color: 3

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 0
Size: 223 Color: 1
Size: 139 Color: 3

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 1
Size: 233 Color: 0
Size: 129 Color: 3

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 0
Size: 237 Color: 1
Size: 125 Color: 0

Bin 2134: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 362 Color: 0

Bin 2135: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 0

Bin 2136: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 362 Color: 2

Bin 2137: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 2

Bin 2138: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 3

Bin 2139: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 3

Bin 2140: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 4

Bin 2141: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 2

Bin 2142: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 3
Size: 362 Color: 4

Bin 2143: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 3

Bin 2144: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 3
Size: 362 Color: 4

Bin 2145: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 3

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 0
Size: 233 Color: 2
Size: 128 Color: 2

Bin 2147: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 3

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 0
Size: 237 Color: 1
Size: 124 Color: 1

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 1
Size: 237 Color: 0
Size: 124 Color: 1

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 0
Size: 237 Color: 1
Size: 124 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 1
Size: 237 Color: 2
Size: 124 Color: 1

Bin 2152: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 3
Size: 361 Color: 0

Bin 2153: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 1
Size: 361 Color: 0

Bin 2154: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 3
Size: 361 Color: 1

Bin 2155: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 2
Size: 361 Color: 3

Bin 2156: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 3
Size: 361 Color: 1

Bin 2157: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 2
Size: 361 Color: 3

Bin 2158: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 3
Size: 361 Color: 1

Bin 2159: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 2
Size: 361 Color: 3

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 3
Size: 233 Color: 0
Size: 127 Color: 3

Bin 2161: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 2
Size: 361 Color: 4

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 3
Size: 233 Color: 0
Size: 127 Color: 2

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 0
Size: 233 Color: 2
Size: 127 Color: 3

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 3
Size: 233 Color: 1
Size: 127 Color: 2

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 2
Size: 235 Color: 0
Size: 125 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 3
Size: 235 Color: 0
Size: 125 Color: 1

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 2
Size: 235 Color: 0
Size: 125 Color: 0

Bin 2168: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 1

Bin 2169: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 2
Size: 360 Color: 1

Bin 2170: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 1

Bin 2171: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 4
Size: 360 Color: 3

Bin 2172: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 2

Bin 2173: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 4
Size: 360 Color: 2

Bin 2174: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 2

Bin 2175: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 4
Size: 360 Color: 3

Bin 2176: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 3

Bin 2177: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 4
Size: 360 Color: 3

Bin 2178: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 2

Bin 2179: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 0
Size: 359 Color: 3

Bin 2180: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 3

Bin 2181: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 3
Size: 359 Color: 2

Bin 2182: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 2
Size: 359 Color: 4

Bin 2183: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 3
Size: 359 Color: 4

Bin 2184: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 359 Color: 2

Bin 2185: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 3
Size: 359 Color: 4

Bin 2186: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 359 Color: 3

Bin 2187: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 0
Size: 358 Color: 1

Bin 2188: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 2
Size: 358 Color: 1

Bin 2189: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 1
Size: 358 Color: 0

Bin 2190: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 2
Size: 358 Color: 3

Bin 2191: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 1
Size: 358 Color: 2

Bin 2192: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 2
Size: 358 Color: 4

Bin 2193: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 4
Size: 358 Color: 3

Bin 2194: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 358 Color: 4

Bin 2195: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 4
Size: 358 Color: 2

Bin 2196: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 358 Color: 4

Bin 2197: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 4
Size: 358 Color: 3

Bin 2198: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 0
Size: 357 Color: 2

Bin 2199: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 2
Size: 357 Color: 1

Bin 2200: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 0
Size: 357 Color: 2

Bin 2201: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 2
Size: 357 Color: 1

Bin 2202: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 0
Size: 357 Color: 2

Bin 2203: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 2
Size: 357 Color: 4

Bin 2204: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 0
Size: 357 Color: 2

Bin 2205: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 2
Size: 357 Color: 4

Bin 2206: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 357 Color: 2

Bin 2207: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 2
Size: 357 Color: 3

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 0
Size: 223 Color: 3
Size: 133 Color: 4

Bin 2209: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 3
Size: 357 Color: 4

Bin 2210: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 0
Size: 356 Color: 2

Bin 2211: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 3
Size: 357 Color: 4

Bin 2212: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 0
Size: 356 Color: 1

Bin 2213: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 3
Size: 357 Color: 4

Bin 2214: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 0
Size: 356 Color: 2

Bin 2215: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 4
Size: 357 Color: 3

Bin 2216: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 0
Size: 356 Color: 1

Bin 2217: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 2

Bin 2218: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 2
Size: 356 Color: 1

Bin 2219: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 4

Bin 2220: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 2
Size: 356 Color: 4

Bin 2221: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 4

Bin 2222: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 356 Color: 2

Bin 2223: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 4

Bin 2224: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 356 Color: 3

Bin 2225: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 4

Bin 2226: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 356 Color: 3

Bin 2227: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 4

Bin 2228: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 356 Color: 3

Bin 2229: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 355 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 0
Size: 223 Color: 2
Size: 132 Color: 2

Bin 2231: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 355 Color: 0

Bin 2232: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 0
Size: 355 Color: 4

Bin 2233: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 355 Color: 4

Bin 2234: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 355 Color: 1

Bin 2235: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 355 Color: 4

Bin 2236: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 355 Color: 4

Bin 2237: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 3
Size: 355 Color: 4

Bin 2238: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 4
Size: 355 Color: 1

Bin 2239: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 0
Size: 354 Color: 2

Bin 2240: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 4
Size: 355 Color: 2

Bin 2241: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 0
Size: 354 Color: 1

Bin 2242: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 3
Size: 354 Color: 1

Bin 2243: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 354 Color: 2

Bin 2244: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 3
Size: 354 Color: 2

Bin 2245: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 354 Color: 3

Bin 2246: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 3
Size: 354 Color: 2

Bin 2247: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 2
Size: 354 Color: 4

Bin 2248: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 3
Size: 354 Color: 4

Bin 2249: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 4
Size: 354 Color: 3

Bin 2250: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 2

Bin 2251: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 353 Color: 3

Bin 2252: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 2

Bin 2253: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 353 Color: 3

Bin 2254: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 4

Bin 2255: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 2
Size: 353 Color: 4

Bin 2256: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 4

Bin 2257: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 2
Size: 353 Color: 4

Bin 2258: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 3
Size: 353 Color: 4

Bin 2259: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 4
Size: 353 Color: 3

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 0
Size: 223 Color: 3
Size: 129 Color: 4

Bin 2261: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 0
Size: 224 Color: 1
Size: 128 Color: 3

Bin 2263: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 2

Bin 2264: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 2
Size: 352 Color: 0

Bin 2265: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 2

Bin 2266: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 2
Size: 352 Color: 1

Bin 2267: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 2

Bin 2268: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 2
Size: 352 Color: 1

Bin 2269: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 4
Size: 352 Color: 1

Bin 2270: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 2

Bin 2271: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 4
Size: 352 Color: 1

Bin 2272: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 4

Bin 2273: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 4
Size: 352 Color: 2

Bin 2274: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 4

Bin 2275: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 4
Size: 352 Color: 3

Bin 2276: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 351 Color: 1

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 1
Size: 227 Color: 0
Size: 124 Color: 2

Bin 2278: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 351 Color: 1

Bin 2279: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 351 Color: 0

Bin 2280: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 351 Color: 1

Bin 2281: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 351 Color: 2

Bin 2282: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 351 Color: 2

Bin 2283: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 351 Color: 2

Bin 2284: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 2
Size: 351 Color: 1

Bin 2285: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 351 Color: 2

Bin 2286: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 2
Size: 351 Color: 3

Bin 2287: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 351 Color: 4

Bin 2288: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 2
Size: 351 Color: 4

Bin 2289: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 351 Color: 4

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 0
Size: 224 Color: 1
Size: 126 Color: 3

Bin 2291: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 4
Size: 351 Color: 3

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 0
Size: 224 Color: 1
Size: 126 Color: 4

Bin 2293: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 4
Size: 351 Color: 3

Bin 2294: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 0
Size: 350 Color: 1

Bin 2295: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 1
Size: 350 Color: 0

Bin 2296: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 350 Color: 4

Bin 2297: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 4
Size: 350 Color: 1

Bin 2298: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 350 Color: 4

Bin 2299: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 4
Size: 350 Color: 2

Bin 2300: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 350 Color: 4

Bin 2301: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 4
Size: 350 Color: 2

Bin 2302: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 350 Color: 4

Bin 2303: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 4
Size: 350 Color: 2

Bin 2304: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 350 Color: 4

Bin 2305: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 4
Size: 350 Color: 3

Bin 2306: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 349 Color: 0

Bin 2307: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 349 Color: 1

Bin 2308: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 349 Color: 0

Bin 2309: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 349 Color: 1

Bin 2310: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 349 Color: 2

Bin 2311: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 349 Color: 3

Bin 2312: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 3
Size: 349 Color: 2

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 0
Size: 224 Color: 1
Size: 124 Color: 3

Bin 2314: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 3
Size: 349 Color: 4

Bin 2315: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 0
Size: 348 Color: 1

Bin 2316: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 4
Size: 349 Color: 3

Bin 2317: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 0
Size: 348 Color: 2

Bin 2318: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 1
Size: 348 Color: 2

Bin 2319: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 2
Size: 348 Color: 1

Bin 2320: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 3
Size: 348 Color: 1

Bin 2321: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 2
Size: 348 Color: 3

Bin 2322: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 3
Size: 348 Color: 2

Bin 2323: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 347 Color: 1

Bin 2324: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 3
Size: 348 Color: 4

Bin 2325: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 347 Color: 1

Bin 2326: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 3
Size: 348 Color: 4

Bin 2327: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 347 Color: 1

Bin 2328: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 4
Size: 348 Color: 3

Bin 2329: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 347 Color: 1

Bin 2330: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 1
Size: 347 Color: 4

Bin 2331: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 347 Color: 4

Bin 2332: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 3
Size: 347 Color: 4

Bin 2333: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 347 Color: 1

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 0
Size: 216 Color: 2
Size: 130 Color: 0

Bin 2335: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 347 Color: 2

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 0
Size: 217 Color: 2
Size: 129 Color: 3

Bin 2337: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 347 Color: 3

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 0
Size: 218 Color: 1
Size: 128 Color: 2

Bin 2339: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 0

Bin 2340: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 0
Size: 346 Color: 1

Bin 2341: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 1

Bin 2342: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 1
Size: 346 Color: 0

Bin 2343: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 1

Bin 2344: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 2
Size: 346 Color: 3

Bin 2345: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 2

Bin 2346: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 2
Size: 346 Color: 3

Bin 2347: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 2

Bin 2348: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 4
Size: 346 Color: 3

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 0
Size: 218 Color: 1
Size: 127 Color: 3

Bin 2350: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 4
Size: 346 Color: 3

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 0
Size: 218 Color: 1
Size: 127 Color: 2

Bin 2352: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 4
Size: 346 Color: 3

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 0
Size: 221 Color: 1
Size: 124 Color: 4

Bin 2354: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 345 Color: 0

Bin 2355: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 2
Size: 345 Color: 1

Bin 2356: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 345 Color: 3

Bin 2357: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 2
Size: 345 Color: 4

Bin 2358: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 3
Size: 345 Color: 4

Bin 2359: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 4
Size: 345 Color: 3

Bin 2360: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 3
Size: 345 Color: 4

Bin 2361: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 4
Size: 345 Color: 3

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 1
Size: 219 Color: 0
Size: 125 Color: 1

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 2
Size: 221 Color: 1
Size: 123 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 1
Size: 219 Color: 2
Size: 125 Color: 2

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 2
Size: 221 Color: 1
Size: 123 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 1
Size: 221 Color: 0
Size: 123 Color: 0

Bin 2367: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 2
Size: 344 Color: 0

Bin 2368: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 1
Size: 344 Color: 0

Bin 2369: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 2
Size: 344 Color: 0

Bin 2370: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 4
Size: 344 Color: 1

Bin 2371: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 344 Color: 4

Bin 2372: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 4
Size: 344 Color: 3

Bin 2373: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 344 Color: 4

Bin 2374: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 4
Size: 344 Color: 1

Bin 2375: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 344 Color: 4

Bin 2376: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 4
Size: 344 Color: 3

Bin 2377: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 2

Bin 2378: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 0
Size: 343 Color: 4

Bin 2379: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 2

Bin 2380: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 3
Size: 343 Color: 4

Bin 2381: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 2
Size: 343 Color: 4

Bin 2382: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 3
Size: 343 Color: 4

Bin 2383: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 342 Color: 0

Bin 2384: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 3
Size: 343 Color: 4

Bin 2385: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 342 Color: 0

Bin 2386: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 4
Size: 343 Color: 3

Bin 2387: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 342 Color: 3

Bin 2388: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 2
Size: 342 Color: 1

Bin 2389: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 342 Color: 3

Bin 2390: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 2
Size: 342 Color: 3

Bin 2391: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 3
Size: 342 Color: 2

Bin 2392: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 4
Size: 342 Color: 3

Bin 2393: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 3
Size: 342 Color: 2

Bin 2394: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 4
Size: 342 Color: 2

Bin 2395: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 3
Size: 342 Color: 4

Bin 2396: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 4
Size: 342 Color: 3

Bin 2397: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 1

Bin 2398: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 341 Color: 2

Bin 2399: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 4

Bin 2400: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 341 Color: 4

Bin 2401: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 2
Size: 341 Color: 4

Bin 2402: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 3
Size: 341 Color: 4

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 2
Size: 217 Color: 0
Size: 123 Color: 0

Bin 2404: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 341 Color: 3

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 2
Size: 217 Color: 0
Size: 123 Color: 1

Bin 2406: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 341 Color: 3

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 2
Size: 217 Color: 0
Size: 123 Color: 3

Bin 2408: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 341 Color: 3

Bin 2409: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 3

Bin 2410: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 0
Size: 340 Color: 3

Bin 2411: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 1

Bin 2412: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 0
Size: 340 Color: 3

Bin 2413: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 1

Bin 2414: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 0
Size: 340 Color: 3

Bin 2415: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 1

Bin 2416: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 340 Color: 2

Bin 2417: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 3

Bin 2418: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 340 Color: 2

Bin 2419: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 3

Bin 2420: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 340 Color: 3

Bin 2421: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 3
Size: 340 Color: 4

Bin 2422: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 340 Color: 2

Bin 2423: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 3
Size: 340 Color: 4

Bin 2424: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 340 Color: 3

Bin 2425: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 0

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 662 Color: 0
Size: 216 Color: 1
Size: 123 Color: 1

Bin 2427: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 662 Color: 0
Size: 216 Color: 2
Size: 123 Color: 3

Bin 2429: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 0

Bin 2430: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 339 Color: 1

Bin 2431: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 1

Bin 2432: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 339 Color: 4

Bin 2433: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 4

Bin 2434: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 339 Color: 4

Bin 2435: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 4

Bin 2436: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 339 Color: 4

Bin 2437: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 4
Size: 339 Color: 2

Bin 2438: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 339 Color: 4

Bin 2439: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 4
Size: 339 Color: 2

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 0
Size: 197 Color: 3
Size: 141 Color: 4

Bin 2441: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 4
Size: 339 Color: 3

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 0
Size: 197 Color: 4
Size: 141 Color: 3

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 2
Size: 211 Color: 0
Size: 127 Color: 3

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 0
Size: 197 Color: 3
Size: 141 Color: 4

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 2
Size: 212 Color: 0
Size: 126 Color: 4

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 0
Size: 202 Color: 2
Size: 136 Color: 2

Bin 2447: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 1

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 1
Size: 212 Color: 0
Size: 126 Color: 4

Bin 2449: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 1

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 1
Size: 213 Color: 0
Size: 125 Color: 1

Bin 2451: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 1

Bin 2452: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 338 Color: 3

Bin 2453: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 1

Bin 2454: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 338 Color: 3

Bin 2455: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 1

Bin 2456: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 338 Color: 3

Bin 2457: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 3

Bin 2458: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 3
Size: 338 Color: 2

Bin 2459: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 1

Bin 2460: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 3
Size: 338 Color: 4

Bin 2461: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 1

Bin 2462: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 3
Size: 338 Color: 4

Bin 2463: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 1

Bin 2464: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 4
Size: 338 Color: 3

Bin 2465: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 1

Bin 2466: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 0
Size: 337 Color: 2

Bin 2467: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 3

Bin 2468: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 0
Size: 337 Color: 2

Bin 2469: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 4

Bin 2470: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 337 Color: 3

Bin 2471: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 3
Size: 337 Color: 4

Bin 2472: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 337 Color: 2

Bin 2473: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 3
Size: 337 Color: 4

Bin 2474: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 337 Color: 3

Bin 2475: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 2

Bin 2476: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 336 Color: 2

Bin 2477: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 0

Bin 2478: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 336 Color: 1

Bin 2479: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 2

Bin 2480: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 2
Size: 336 Color: 4

Bin 2481: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 4
Size: 336 Color: 2

Bin 2482: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 3
Size: 336 Color: 4

Bin 2483: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 4
Size: 336 Color: 3

Bin 2484: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 0
Size: 335 Color: 1

Bin 2485: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 4
Size: 336 Color: 2

Bin 2486: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 0
Size: 335 Color: 2

Bin 2487: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 4
Size: 336 Color: 3

Bin 2488: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 0
Size: 335 Color: 1

Bin 2489: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 335 Color: 2

Bin 2490: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 2
Size: 335 Color: 1

Bin 2491: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 335 Color: 2

Bin 2492: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 2
Size: 335 Color: 1

Bin 2493: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 335 Color: 2

Bin 2494: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 2
Size: 335 Color: 3

Bin 2495: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 335 Color: 4

Bin 2496: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 2
Size: 335 Color: 4

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 0
Size: 202 Color: 2
Size: 132 Color: 3

Bin 2498: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 3
Size: 335 Color: 4

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 0
Size: 202 Color: 1
Size: 132 Color: 3

Bin 2500: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 3
Size: 335 Color: 4

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 0
Size: 204 Color: 1
Size: 130 Color: 0

Bin 2502: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 4
Size: 335 Color: 3

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 0
Size: 209 Color: 1
Size: 125 Color: 2

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 1
Size: 213 Color: 0
Size: 121 Color: 1

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 0
Size: 210 Color: 1
Size: 124 Color: 3

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 1
Size: 213 Color: 0
Size: 121 Color: 1

Bin 2507: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 0
Size: 334 Color: 1

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 1
Size: 213 Color: 0
Size: 121 Color: 1

Bin 2509: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 3
Size: 334 Color: 1

Bin 2510: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 2
Size: 334 Color: 1

Bin 2511: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 3
Size: 334 Color: 0

Bin 2512: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 2
Size: 334 Color: 4

Bin 2513: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 3
Size: 334 Color: 4

Bin 2514: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 4
Size: 334 Color: 1

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 0
Size: 210 Color: 1
Size: 123 Color: 2

Bin 2516: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 4
Size: 334 Color: 2

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 0
Size: 212 Color: 2
Size: 121 Color: 1

Bin 2518: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 4
Size: 334 Color: 2

Bin 2519: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 333 Color: 1

Bin 2520: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 1
Size: 333 Color: 2

Bin 2521: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 333 Color: 1

Bin 2522: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 1
Size: 333 Color: 2

Bin 2523: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 2

Bin 2524: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 2
Size: 333 Color: 1

Bin 2525: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 4

Bin 2526: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 333 Color: 1

Bin 2527: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 4

Bin 2528: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 333 Color: 2

Bin 2529: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 1

Bin 2530: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 333 Color: 3

Bin 2531: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 1

Bin 2532: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 332 Color: 2

Bin 2533: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 3

Bin 2534: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 332 Color: 2

Bin 2535: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 3

Bin 2536: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 332 Color: 2

Bin 2537: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 2
Size: 332 Color: 4

Bin 2538: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 332 Color: 3

Bin 2539: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 2
Size: 332 Color: 4

Bin 2540: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 332 Color: 3

Bin 2541: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 3
Size: 332 Color: 4

Bin 2542: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 332 Color: 3

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 0
Size: 210 Color: 1
Size: 121 Color: 1

Bin 2544: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 1
Size: 331 Color: 2

Bin 2545: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 0
Size: 331 Color: 1

Bin 2546: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 1
Size: 331 Color: 2

Bin 2547: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 331 Color: 2

Bin 2548: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 2
Size: 331 Color: 1

Bin 2549: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 331 Color: 2

Bin 2550: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 2
Size: 331 Color: 1

Bin 2551: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 331 Color: 2

Bin 2552: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 2
Size: 331 Color: 1

Bin 2553: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 331 Color: 1

Bin 2554: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 3
Size: 331 Color: 2

Bin 2555: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 331 Color: 3

Bin 2556: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 1

Bin 2557: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 0
Size: 330 Color: 2

Bin 2558: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 1

Bin 2559: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 0
Size: 330 Color: 3

Bin 2560: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 4

Bin 2561: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 0
Size: 330 Color: 4

Bin 2562: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 4

Bin 2563: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 3

Bin 2564: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 4

Bin 2565: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 3

Bin 2566: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 4

Bin 2567: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 3

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 0
Size: 206 Color: 2
Size: 123 Color: 3

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 1
Size: 208 Color: 0
Size: 121 Color: 1

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 0
Size: 208 Color: 1
Size: 121 Color: 0

Bin 2571: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 329 Color: 0

Bin 2572: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 1

Bin 2573: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 329 Color: 0

Bin 2574: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 1

Bin 2575: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 329 Color: 4

Bin 2576: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 4

Bin 2577: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 2

Bin 2578: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 4

Bin 2579: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 1

Bin 2580: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 329 Color: 4

Bin 2581: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 2

Bin 2582: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 1

Bin 2583: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 3

Bin 2584: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 2

Bin 2585: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 3

Bin 2586: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 3

Bin 2587: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 3

Bin 2588: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 2

Bin 2589: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 328 Color: 2

Bin 2590: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 2
Size: 328 Color: 3

Bin 2591: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 328 Color: 3

Bin 2592: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 2
Size: 328 Color: 4

Bin 2593: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 328 Color: 3

Bin 2594: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 2
Size: 328 Color: 4

Bin 2595: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 3
Size: 328 Color: 4

Bin 2596: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 4
Size: 328 Color: 3

Bin 2597: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 3
Size: 328 Color: 4

Bin 2598: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 4
Size: 328 Color: 3

Bin 2599: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 1
Size: 327 Color: 2

Bin 2600: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 1

Bin 2601: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 1
Size: 327 Color: 2

Bin 2602: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 2

Bin 2603: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 1
Size: 327 Color: 4

Bin 2604: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 3

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 0
Size: 204 Color: 1
Size: 122 Color: 0

Bin 2606: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 3
Size: 327 Color: 4

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 0
Size: 204 Color: 3
Size: 122 Color: 2

Bin 2608: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 3
Size: 327 Color: 4

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 0
Size: 204 Color: 2
Size: 122 Color: 1

Bin 2610: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 3
Size: 327 Color: 4

Bin 2611: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 0
Size: 326 Color: 2

Bin 2612: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 4
Size: 327 Color: 3

Bin 2613: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 0
Size: 326 Color: 2

Bin 2614: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 1
Size: 326 Color: 2

Bin 2615: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 0

Bin 2616: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 1
Size: 326 Color: 2

Bin 2617: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 1

Bin 2618: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 4
Size: 326 Color: 2

Bin 2619: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 1

Bin 2620: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 4
Size: 326 Color: 3

Bin 2621: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 3
Size: 326 Color: 2

Bin 2622: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 4
Size: 326 Color: 3

Bin 2623: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 3
Size: 326 Color: 4

Bin 2624: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 4
Size: 326 Color: 3

Bin 2625: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 0
Size: 325 Color: 1

Bin 2626: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 1
Size: 325 Color: 2

Bin 2627: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 2
Size: 325 Color: 4

Bin 2628: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 325 Color: 1

Bin 2629: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 325 Color: 4

Bin 2630: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 325 Color: 2

Bin 2631: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 0

Bin 2632: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 325 Color: 3

Bin 2633: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 0

Bin 2634: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 325 Color: 3

Bin 2635: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 0
Size: 202 Color: 2
Size: 122 Color: 2

Bin 2637: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 0

Bin 2638: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 0
Size: 324 Color: 1

Bin 2639: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 4

Bin 2640: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 2
Size: 324 Color: 4

Bin 2641: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 4
Size: 324 Color: 2

Bin 2642: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 2
Size: 324 Color: 4

Bin 2643: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 4
Size: 324 Color: 1

Bin 2644: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 2
Size: 324 Color: 4

Bin 2645: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 4
Size: 324 Color: 2

Bin 2646: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 2
Size: 324 Color: 4

Bin 2647: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 4
Size: 324 Color: 3

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 0
Size: 202 Color: 1
Size: 121 Color: 1

Bin 2649: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 2
Size: 323 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 0
Size: 202 Color: 2
Size: 121 Color: 4

Bin 2651: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 2
Size: 323 Color: 0

Bin 2652: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 1
Size: 323 Color: 2

Bin 2653: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 2
Size: 323 Color: 0

Bin 2654: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 3
Size: 323 Color: 2

Bin 2655: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 2
Size: 323 Color: 1

Bin 2656: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 3
Size: 323 Color: 4

Bin 2657: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 4
Size: 323 Color: 2

Bin 2658: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 3
Size: 323 Color: 4

Bin 2659: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 4
Size: 323 Color: 2

Bin 2660: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 0
Size: 322 Color: 2

Bin 2661: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 4
Size: 323 Color: 3

Bin 2662: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 0
Size: 322 Color: 2

Bin 2663: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 2
Size: 322 Color: 1

Bin 2664: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 322 Color: 2

Bin 2665: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 2
Size: 322 Color: 3

Bin 2666: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 2

Bin 2667: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 2
Size: 322 Color: 3

Bin 2668: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 2

Bin 2669: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 2
Size: 322 Color: 3

Bin 2670: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 3

Bin 2671: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 3
Size: 322 Color: 4

Bin 2672: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 2

Bin 2673: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 3
Size: 322 Color: 4

Bin 2674: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 3

Bin 2675: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 321 Color: 2

Bin 2676: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 4

Bin 2677: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 3

Bin 2678: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 4

Bin 2679: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 3

Bin 2680: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 4

Bin 2681: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 3

Bin 2682: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 4

Bin 2683: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 3

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 0
Size: 197 Color: 4
Size: 123 Color: 2

Bin 2685: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 3
Size: 321 Color: 4

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 0
Size: 197 Color: 3
Size: 123 Color: 3

Bin 2687: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 3
Size: 321 Color: 4

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 0
Size: 197 Color: 4
Size: 123 Color: 4

Bin 2689: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 4
Size: 321 Color: 3

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 0
Size: 197 Color: 4
Size: 123 Color: 4

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 1
Size: 198 Color: 0
Size: 122 Color: 1

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 0
Size: 198 Color: 1
Size: 122 Color: 2

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 1
Size: 198 Color: 3
Size: 122 Color: 3

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 3
Size: 198 Color: 1
Size: 122 Color: 3

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 1
Size: 199 Color: 3
Size: 121 Color: 2

Bin 2696: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 1

Bin 2697: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 1
Size: 320 Color: 0

Bin 2698: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 1

Bin 2699: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 2
Size: 320 Color: 3

Bin 2700: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 1

Bin 2701: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 2
Size: 320 Color: 3

Bin 2702: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 2

Bin 2703: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 2
Size: 320 Color: 3

Bin 2704: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 4

Bin 2705: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 4
Size: 320 Color: 2

Bin 2706: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 4

Bin 2707: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 4
Size: 320 Color: 3

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 0
Size: 198 Color: 1
Size: 121 Color: 4

Bin 2709: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 4
Size: 320 Color: 2

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 0
Size: 198 Color: 3
Size: 121 Color: 2

Bin 2711: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 4
Size: 320 Color: 3

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 0
Size: 198 Color: 1
Size: 121 Color: 4

Bin 2713: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 319 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 0
Size: 198 Color: 2
Size: 121 Color: 2

Bin 2715: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 319 Color: 0

Bin 2716: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 2
Size: 319 Color: 1

Bin 2717: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 319 Color: 0

Bin 2718: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 2
Size: 319 Color: 1

Bin 2719: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 2

Bin 2720: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 2
Size: 319 Color: 3

Bin 2721: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 4

Bin 2722: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 4
Size: 319 Color: 3

Bin 2723: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 4

Bin 2724: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 4
Size: 319 Color: 2

Bin 2725: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 0
Size: 318 Color: 1

Bin 2726: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 4
Size: 319 Color: 3

Bin 2727: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 0
Size: 318 Color: 2

Bin 2728: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 4
Size: 319 Color: 3

Bin 2729: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 0
Size: 318 Color: 2

Bin 2730: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 1

Bin 2731: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 318 Color: 2

Bin 2732: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 3

Bin 2733: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 318 Color: 4

Bin 2734: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 4

Bin 2735: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 318 Color: 4

Bin 2736: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 4

Bin 2737: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 3
Size: 318 Color: 4

Bin 2738: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 318 Color: 3

Bin 2739: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 3
Size: 318 Color: 4

Bin 2740: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 318 Color: 3

Bin 2741: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 0
Size: 317 Color: 1

Bin 2742: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 3

Bin 2743: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 0
Size: 317 Color: 1

Bin 2744: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 3

Bin 2745: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 0
Size: 317 Color: 1

Bin 2746: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 2

Bin 2747: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 317 Color: 3

Bin 2748: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 3

Bin 2749: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 317 Color: 4

Bin 2750: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 317 Color: 2

Bin 2751: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 3
Size: 317 Color: 4

Bin 2752: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 317 Color: 3

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 0
Size: 195 Color: 2
Size: 121 Color: 4

Bin 2754: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 317 Color: 3

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 0
Size: 195 Color: 3
Size: 121 Color: 3

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 1
Size: 196 Color: 4
Size: 120 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 0
Size: 196 Color: 2
Size: 120 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 1
Size: 196 Color: 4
Size: 120 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 2
Size: 196 Color: 4
Size: 120 Color: 1

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 1
Size: 196 Color: 4
Size: 120 Color: 2

Bin 2761: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 2
Size: 316 Color: 3

Bin 2762: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 3
Size: 316 Color: 0

Bin 2763: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 4
Size: 316 Color: 3

Bin 2764: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 3
Size: 316 Color: 0

Bin 2765: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 4
Size: 316 Color: 3

Bin 2766: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 3
Size: 316 Color: 1

Bin 2767: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 4
Size: 316 Color: 3

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 0
Size: 195 Color: 3
Size: 120 Color: 1

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 3
Size: 195 Color: 1
Size: 120 Color: 2

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 1
Size: 195 Color: 3
Size: 120 Color: 3

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 3
Size: 195 Color: 2
Size: 120 Color: 3

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 1
Size: 195 Color: 4
Size: 120 Color: 3

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 3
Size: 195 Color: 4
Size: 120 Color: 4

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 1
Size: 195 Color: 4
Size: 120 Color: 4

Bin 2775: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 315 Color: 0

Bin 2776: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 2
Size: 315 Color: 0

Bin 2777: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 315 Color: 0

Bin 2778: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 4
Size: 315 Color: 0

Bin 2779: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 315 Color: 0

Bin 2780: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 4
Size: 315 Color: 0

Bin 2781: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 315 Color: 2

Bin 2782: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 4
Size: 315 Color: 1

Bin 2783: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 315 Color: 2

Bin 2784: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 4
Size: 315 Color: 3

Bin 2785: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 314 Color: 2

Bin 2786: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 0
Size: 314 Color: 3

Bin 2787: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 314 Color: 1

Bin 2788: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 2
Size: 314 Color: 3

Bin 2789: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 314 Color: 2

Bin 2790: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 2
Size: 314 Color: 4

Bin 2791: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 314 Color: 4

Bin 2792: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 4
Size: 314 Color: 3

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 0
Size: 193 Color: 3
Size: 120 Color: 4

Bin 2794: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 4
Size: 314 Color: 3

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 0
Size: 194 Color: 1
Size: 119 Color: 3

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 1
Size: 194 Color: 3
Size: 119 Color: 1

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 0
Size: 194 Color: 2
Size: 119 Color: 3

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 1
Size: 194 Color: 3
Size: 119 Color: 1

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 2
Size: 194 Color: 4
Size: 119 Color: 3

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 1
Size: 194 Color: 3
Size: 119 Color: 2

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 2
Size: 194 Color: 4
Size: 119 Color: 3

Bin 2802: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 0

Bin 2803: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 2
Size: 313 Color: 0

Bin 2804: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 3
Size: 313 Color: 4

Bin 2805: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 0

Bin 2806: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 3
Size: 313 Color: 4

Bin 2807: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 3

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 1
Size: 193 Color: 4
Size: 119 Color: 2

Bin 2809: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 2

Bin 2810: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 312 Color: 0

Bin 2811: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 3

Bin 2812: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 312 Color: 0

Bin 2813: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 3

Bin 2814: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 312 Color: 0

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 0
Size: 193 Color: 4
Size: 119 Color: 3

Bin 2816: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 312 Color: 0

Bin 2817: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 0
Size: 312 Color: 2

Bin 2818: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 312 Color: 2

Bin 2819: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 312 Color: 0

Bin 2820: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 3
Size: 312 Color: 2

Bin 2821: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 312 Color: 1

Bin 2822: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 3
Size: 312 Color: 2

Bin 2823: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 312 Color: 1

Bin 2824: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 3
Size: 312 Color: 2

Bin 2825: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 312 Color: 3

Bin 2826: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 3
Size: 312 Color: 4

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 690 Color: 0
Size: 192 Color: 4
Size: 119 Color: 2

Bin 2828: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 3
Size: 312 Color: 4

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 690 Color: 0
Size: 192 Color: 4
Size: 119 Color: 3

Bin 2830: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 4
Size: 312 Color: 3

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 690 Color: 0
Size: 192 Color: 4
Size: 119 Color: 4

Bin 2832: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 311 Color: 0

Bin 2833: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 311 Color: 1

Bin 2834: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 311 Color: 3

Bin 2835: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 311 Color: 1

Bin 2836: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 311 Color: 3

Bin 2837: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 1

Bin 2838: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 311 Color: 3

Bin 2839: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 3

Bin 2840: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 3
Size: 311 Color: 1

Bin 2841: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 1

Bin 2842: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 3
Size: 311 Color: 4

Bin 2843: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 4

Bin 2844: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 3
Size: 311 Color: 4

Bin 2845: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 4
Size: 311 Color: 3

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 0
Size: 191 Color: 4
Size: 119 Color: 3

Bin 2847: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 0

Bin 2848: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 0
Size: 310 Color: 1

Bin 2849: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 0

Bin 2850: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 0
Size: 310 Color: 2

Bin 2851: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 1

Bin 2852: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 1
Size: 310 Color: 2

Bin 2853: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 1

Bin 2854: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 1
Size: 310 Color: 4

Bin 2855: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 4

Bin 2856: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 1

Bin 2857: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 3
Size: 310 Color: 4

Bin 2858: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 1

Bin 2859: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 1

Bin 2860: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 3

Bin 2861: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 2

Bin 2862: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 1

Bin 2863: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 3

Bin 2864: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 4

Bin 2865: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 3

Bin 2866: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 4

Bin 2867: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 2
Size: 309 Color: 4

Bin 2868: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 3
Size: 309 Color: 4

Bin 2869: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 4
Size: 309 Color: 3

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 2
Size: 190 Color: 3
Size: 118 Color: 1

Bin 2871: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 4
Size: 309 Color: 2

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 2
Size: 190 Color: 3
Size: 118 Color: 0

Bin 2873: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 4
Size: 309 Color: 3

Bin 2874: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 2
Size: 308 Color: 0

Bin 2875: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 308 Color: 2

Bin 2876: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 2
Size: 308 Color: 1

Bin 2877: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 308 Color: 4

Bin 2878: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 2
Size: 308 Color: 4

Bin 2879: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 3
Size: 308 Color: 4

Bin 2880: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 308 Color: 2

Bin 2881: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 3
Size: 308 Color: 4

Bin 2882: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 308 Color: 2

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 1
Size: 189 Color: 2
Size: 118 Color: 1

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 0
Size: 189 Color: 3
Size: 118 Color: 0

Bin 2885: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 1
Size: 307 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 0
Size: 189 Color: 4
Size: 118 Color: 1

Bin 2887: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 1
Size: 307 Color: 0

Bin 2888: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 0
Size: 307 Color: 1

Bin 2889: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 1
Size: 307 Color: 2

Bin 2890: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 3
Size: 307 Color: 2

Bin 2891: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 307 Color: 1

Bin 2892: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 3
Size: 307 Color: 2

Bin 2893: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 307 Color: 4

Bin 2894: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 3
Size: 307 Color: 4

Bin 2895: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 307 Color: 4

Bin 2896: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 3
Size: 307 Color: 4

Bin 2897: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 4
Size: 307 Color: 3

Bin 2898: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 3
Size: 307 Color: 4

Bin 2899: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 4
Size: 307 Color: 3

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 0
Size: 188 Color: 4
Size: 118 Color: 3

Bin 2901: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 4
Size: 307 Color: 3

Bin 2902: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 1

Bin 2903: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 4
Size: 307 Color: 3

Bin 2904: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 1

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 1
Size: 188 Color: 4
Size: 118 Color: 1

Bin 2906: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 1

Bin 2907: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 1
Size: 306 Color: 0

Bin 2908: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 1

Bin 2909: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 1
Size: 306 Color: 3

Bin 2910: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 306 Color: 2

Bin 2911: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 1
Size: 306 Color: 3

Bin 2912: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 306 Color: 2

Bin 2913: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 306 Color: 4

Bin 2914: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 306 Color: 4

Bin 2915: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 4
Size: 306 Color: 2

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 0
Size: 187 Color: 3
Size: 118 Color: 3

Bin 2917: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 4
Size: 306 Color: 3

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 0
Size: 187 Color: 4
Size: 118 Color: 1

Bin 2919: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 1
Size: 305 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 0
Size: 187 Color: 4
Size: 118 Color: 3

Bin 2921: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 1
Size: 305 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 0
Size: 187 Color: 4
Size: 118 Color: 1

Bin 2923: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 1
Size: 305 Color: 0

Bin 2924: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 2
Size: 305 Color: 3

Bin 2925: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 3
Size: 305 Color: 1

Bin 2926: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 2
Size: 305 Color: 1

Bin 2927: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 3
Size: 305 Color: 4

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 0
Size: 186 Color: 4
Size: 118 Color: 3

Bin 2929: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 3
Size: 305 Color: 4

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 0
Size: 186 Color: 3
Size: 118 Color: 2

Bin 2931: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 4
Size: 305 Color: 3

Bin 2932: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 304 Color: 1

Bin 2933: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 4
Size: 305 Color: 1

Bin 2934: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 304 Color: 1

Bin 2935: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 4
Size: 305 Color: 3

Bin 2936: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 304 Color: 1

Bin 2937: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 0

Bin 2938: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 304 Color: 2

Bin 2939: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 1

Bin 2940: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 2

Bin 2941: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 1

Bin 2942: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 2

Bin 2943: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 3

Bin 2944: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 3

Bin 2945: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 3

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 0
Size: 185 Color: 3
Size: 118 Color: 3

Bin 2947: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 304 Color: 2

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 0
Size: 185 Color: 4
Size: 118 Color: 4

Bin 2949: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 304 Color: 4

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 0
Size: 185 Color: 3
Size: 118 Color: 3

Bin 2951: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 304 Color: 4

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 0
Size: 185 Color: 4
Size: 118 Color: 4

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 186 Color: 4
Size: 117 Color: 1

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 0
Size: 186 Color: 3
Size: 117 Color: 1

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 186 Color: 4
Size: 117 Color: 0

Bin 2956: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 303 Color: 1

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 186 Color: 4
Size: 117 Color: 1

Bin 2958: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 2
Size: 303 Color: 1

Bin 2959: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 303 Color: 0

Bin 2960: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 3
Size: 303 Color: 1

Bin 2961: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 303 Color: 0

Bin 2962: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 3
Size: 303 Color: 4

Bin 2963: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 303 Color: 1

Bin 2964: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 3
Size: 303 Color: 4

Bin 2965: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 303 Color: 2

Bin 2966: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 302 Color: 3

Bin 2967: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 303 Color: 3

Bin 2968: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 302 Color: 0

Bin 2969: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 302 Color: 3

Bin 2970: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 302 Color: 0

Bin 2971: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 302 Color: 3

Bin 2972: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 302 Color: 0

Bin 2973: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 2
Size: 302 Color: 3

Bin 2974: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 3
Size: 302 Color: 1

Bin 2975: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 2
Size: 302 Color: 4

Bin 2976: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 3
Size: 302 Color: 4

Bin 2977: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 4
Size: 302 Color: 3

Bin 2978: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 301 Color: 0

Bin 2979: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 4
Size: 302 Color: 2

Bin 2980: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 301 Color: 0

Bin 2981: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 4
Size: 302 Color: 3

Bin 2982: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 301 Color: 0

Bin 2983: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 301 Color: 1

Bin 2984: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 301 Color: 0

Bin 2985: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 301 Color: 1

Bin 2986: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 301 Color: 2

Bin 2987: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 2
Size: 301 Color: 4

Bin 2988: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 301 Color: 2

Bin 2989: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 2
Size: 301 Color: 4

Bin 2990: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 301 Color: 2

Bin 2991: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 2
Size: 301 Color: 3

Bin 2992: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 301 Color: 4

Bin 2993: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 2
Size: 301 Color: 4

Bin 2994: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 301 Color: 4

Bin 2995: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 3

Bin 2996: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 301 Color: 4

Bin 2997: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 3

Bin 2998: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 4
Size: 301 Color: 3

Bin 2999: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 3

Bin 3000: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 4
Size: 301 Color: 3

Bin 3001: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 3

Bin 3002: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 1
Size: 300 Color: 0

Bin 3003: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 2
Size: 300 Color: 3

Bin 3004: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 1
Size: 300 Color: 2

Bin 3005: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 2
Size: 300 Color: 3

Bin 3006: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 3
Size: 300 Color: 2

Bin 3007: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 2
Size: 300 Color: 4

Bin 3008: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 3
Size: 300 Color: 4

Bin 3009: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 3

Bin 3010: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 3
Size: 300 Color: 4

Bin 3011: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 2

Bin 3012: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 3
Size: 300 Color: 4

Bin 3013: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 3

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 0
Size: 182 Color: 4
Size: 117 Color: 0

Bin 3015: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 3

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 0
Size: 182 Color: 4
Size: 117 Color: 1

Bin 3017: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 1

Bin 3018: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 1
Size: 299 Color: 0

Bin 3019: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 1

Bin 3020: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 1
Size: 299 Color: 0

Bin 3021: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 1

Bin 3022: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 1
Size: 299 Color: 0

Bin 3023: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 1

Bin 3024: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 2

Bin 3025: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 1

Bin 3026: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 4

Bin 3027: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 4

Bin 3028: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 4

Bin 3029: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 4

Bin 3030: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 4

Bin 3031: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 299 Color: 2

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 0
Size: 181 Color: 4
Size: 117 Color: 2

Bin 3033: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 299 Color: 3

Bin 3034: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 3

Bin 3035: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 299 Color: 2

Bin 3036: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 1

Bin 3037: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 299 Color: 3

Bin 3038: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 2

Bin 3039: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 2
Size: 298 Color: 3

Bin 3040: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 2

Bin 3041: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 2
Size: 298 Color: 3

Bin 3042: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 2

Bin 3043: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 2
Size: 298 Color: 3

Bin 3044: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 3

Bin 3045: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 2
Size: 298 Color: 4

Bin 3046: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 2

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 1
Size: 180 Color: 4
Size: 117 Color: 1

Bin 3048: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 3

Bin 3049: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 0

Bin 3050: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 1

Bin 3051: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 3

Bin 3052: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 1

Bin 3053: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 3

Bin 3054: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 3
Size: 297 Color: 1

Bin 3055: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 2
Size: 297 Color: 3

Bin 3056: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 3
Size: 297 Color: 4

Bin 3057: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 2
Size: 297 Color: 4

Bin 3058: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 3
Size: 297 Color: 4

Bin 3059: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 297 Color: 2

Bin 3060: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 296 Color: 1

Bin 3061: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 297 Color: 3

Bin 3062: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 296 Color: 1

Bin 3063: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 296 Color: 4

Bin 3064: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 296 Color: 2

Bin 3065: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 296 Color: 4

Bin 3066: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 296 Color: 4

Bin 3067: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 4
Size: 296 Color: 3

Bin 3068: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 4

Bin 3069: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 2
Size: 295 Color: 3

Bin 3070: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 4

Bin 3071: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 2
Size: 295 Color: 3

Bin 3072: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 295 Color: 4

Bin 3073: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 2
Size: 295 Color: 4

Bin 3074: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 3
Size: 295 Color: 4

Bin 3075: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 4
Size: 295 Color: 3

Bin 3076: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 0
Size: 294 Color: 1

Bin 3077: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 2
Size: 294 Color: 1

Bin 3078: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 0
Size: 294 Color: 1

Bin 3079: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 2
Size: 294 Color: 1

Bin 3080: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 1
Size: 294 Color: 0

Bin 3081: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 2
Size: 294 Color: 4

Bin 3082: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 3
Size: 294 Color: 4

Bin 3083: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 294 Color: 2

Bin 3084: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 3
Size: 294 Color: 4

Bin 3085: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 294 Color: 3

Bin 3086: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 1

Bin 3087: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 294 Color: 3

Bin 3088: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 1

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 1
Size: 176 Color: 2
Size: 117 Color: 2

Bin 3090: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 1

Bin 3091: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 1
Size: 293 Color: 3

Bin 3092: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 2
Size: 293 Color: 4

Bin 3093: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 1
Size: 293 Color: 4

Bin 3094: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 2
Size: 293 Color: 4

Bin 3095: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 293 Color: 4

Bin 3096: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 2

Bin 3097: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 293 Color: 4

Bin 3098: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 3

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 0
Size: 175 Color: 2
Size: 117 Color: 3

Bin 3100: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 3

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 0
Size: 175 Color: 4
Size: 117 Color: 2

Bin 3102: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 3

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 0
Size: 175 Color: 2
Size: 117 Color: 3

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 1
Size: 175 Color: 4
Size: 117 Color: 2

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 0
Size: 175 Color: 2
Size: 117 Color: 3

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 1
Size: 175 Color: 4
Size: 117 Color: 4

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 0
Size: 176 Color: 3
Size: 116 Color: 0

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 1
Size: 176 Color: 2
Size: 116 Color: 0

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 0
Size: 176 Color: 3
Size: 116 Color: 0

Bin 3110: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 1
Size: 292 Color: 0

Bin 3111: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 2
Size: 292 Color: 0

Bin 3112: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 1
Size: 292 Color: 0

Bin 3113: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 2
Size: 292 Color: 3

Bin 3114: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 1

Bin 3115: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 2
Size: 292 Color: 4

Bin 3116: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 3

Bin 3117: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 3
Size: 292 Color: 4

Bin 3118: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 2

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 175 Color: 3
Size: 116 Color: 0

Bin 3120: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 3

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 175 Color: 4
Size: 116 Color: 0

Bin 3122: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 2

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 175 Color: 3
Size: 116 Color: 1

Bin 3124: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 3

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 175 Color: 4
Size: 116 Color: 3

Bin 3126: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 291 Color: 1

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 175 Color: 3
Size: 116 Color: 1

Bin 3128: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 291 Color: 1

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 175 Color: 4
Size: 116 Color: 3

Bin 3130: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 291 Color: 1

Bin 3131: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 2
Size: 291 Color: 3

Bin 3132: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 291 Color: 1

Bin 3133: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 2
Size: 291 Color: 3

Bin 3134: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 291 Color: 2

Bin 3135: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 2
Size: 291 Color: 4

Bin 3136: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 291 Color: 4

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 0
Size: 174 Color: 4
Size: 116 Color: 1

Bin 3138: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 291 Color: 4

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 0
Size: 174 Color: 4
Size: 116 Color: 3

Bin 3140: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 3

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 0
Size: 174 Color: 4
Size: 116 Color: 2

Bin 3142: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 2

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 0
Size: 175 Color: 4
Size: 115 Color: 0

Bin 3144: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 3

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 0
Size: 175 Color: 4
Size: 115 Color: 1

Bin 3146: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 2

Bin 3147: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 2
Size: 290 Color: 0

Bin 3148: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 2

Bin 3149: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 2
Size: 290 Color: 1

Bin 3150: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 2

Bin 3151: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 2
Size: 290 Color: 4

Bin 3152: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 3

Bin 3153: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 3
Size: 290 Color: 4

Bin 3154: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 3

Bin 3155: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 1
Size: 289 Color: 0

Bin 3156: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 289 Color: 1

Bin 3157: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 1
Size: 289 Color: 0

Bin 3158: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 289 Color: 1

Bin 3159: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 1
Size: 289 Color: 0

Bin 3160: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 289 Color: 1

Bin 3161: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 3
Size: 289 Color: 2

Bin 3162: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 289 Color: 1

Bin 3163: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 3
Size: 289 Color: 2

Bin 3164: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 289 Color: 4

Bin 3165: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 3
Size: 289 Color: 4

Bin 3166: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 4
Size: 289 Color: 3

Bin 3167: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 3
Size: 289 Color: 4

Bin 3168: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 4
Size: 289 Color: 2

Bin 3169: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 1

Bin 3170: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 4
Size: 289 Color: 3

Bin 3171: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 1

Bin 3172: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 1

Bin 3173: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 2

Bin 3174: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 4

Bin 3175: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 2

Bin 3176: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 4

Bin 3177: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 2

Bin 3178: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 4

Bin 3179: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 1
Size: 288 Color: 2

Bin 3180: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 4

Bin 3181: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 1
Size: 288 Color: 4

Bin 3182: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 4

Bin 3183: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 3
Size: 288 Color: 4

Bin 3184: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 0
Size: 172 Color: 3
Size: 115 Color: 2

Bin 3185: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 4
Size: 288 Color: 2

Bin 3186: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 0
Size: 172 Color: 4
Size: 115 Color: 1

Bin 3187: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 287 Color: 0

Bin 3188: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 1

Bin 3189: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 287 Color: 0

Bin 3190: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 1

Bin 3191: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 287 Color: 0

Bin 3192: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 2

Bin 3193: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 2
Size: 287 Color: 4

Bin 3194: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 4

Bin 3195: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 2
Size: 287 Color: 4

Bin 3196: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 4

Bin 3197: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 0
Size: 171 Color: 2
Size: 115 Color: 2

Bin 3198: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 4
Size: 287 Color: 2

Bin 3199: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 0
Size: 171 Color: 3
Size: 115 Color: 1

Bin 3200: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 4
Size: 287 Color: 3

Bin 3201: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 0
Size: 172 Color: 4
Size: 114 Color: 0

Bin 3202: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 286 Color: 0

Bin 3203: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 286 Color: 1

Bin 3204: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 286 Color: 2

Bin 3205: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 286 Color: 1

Bin 3206: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 286 Color: 2

Bin 3207: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 2
Size: 286 Color: 1

Bin 3208: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 286 Color: 2

Bin 3209: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 2
Size: 286 Color: 3

Bin 3210: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 286 Color: 3

Bin 3211: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 2
Size: 286 Color: 3

Bin 3212: 0 of cap free
Amount of items: 3
Items: 
Size: 716 Color: 0
Size: 171 Color: 4
Size: 114 Color: 0

Bin 3213: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 3
Size: 286 Color: 2

Bin 3214: 0 of cap free
Amount of items: 3
Items: 
Size: 716 Color: 0
Size: 171 Color: 4
Size: 114 Color: 0

Bin 3215: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 3
Size: 286 Color: 4

Bin 3216: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 285 Color: 1

Bin 3217: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 4
Size: 286 Color: 3

Bin 3218: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 285 Color: 1

Bin 3219: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 2
Size: 285 Color: 0

Bin 3220: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 4
Size: 285 Color: 1

Bin 3221: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 285 Color: 4

Bin 3222: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 4
Size: 285 Color: 2

Bin 3223: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 284 Color: 2

Bin 3224: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 4
Size: 285 Color: 3

Bin 3225: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 284 Color: 1

Bin 3226: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 3
Size: 284 Color: 2

Bin 3227: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 284 Color: 4

Bin 3228: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 3
Size: 284 Color: 4

Bin 3229: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 284 Color: 4

Bin 3230: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 3
Size: 284 Color: 4

Bin 3231: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 3

Bin 3232: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 0
Size: 167 Color: 3
Size: 116 Color: 3

Bin 3233: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 2

Bin 3234: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 0
Size: 167 Color: 4
Size: 116 Color: 3

Bin 3235: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 3

Bin 3236: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 0
Size: 168 Color: 1
Size: 115 Color: 2

Bin 3237: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 3

Bin 3238: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 0
Size: 168 Color: 1
Size: 115 Color: 3

Bin 3239: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 3

Bin 3240: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 0
Size: 168 Color: 1
Size: 115 Color: 3

Bin 3241: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 2
Size: 168 Color: 4
Size: 115 Color: 3

Bin 3242: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 1
Size: 168 Color: 2
Size: 115 Color: 4

Bin 3243: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 2
Size: 169 Color: 1
Size: 114 Color: 3

Bin 3244: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 1
Size: 169 Color: 0
Size: 114 Color: 1

Bin 3245: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 2
Size: 169 Color: 1
Size: 114 Color: 3

Bin 3246: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 1
Size: 169 Color: 2
Size: 114 Color: 1

Bin 3247: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 2
Size: 169 Color: 1
Size: 114 Color: 3

Bin 3248: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 1
Size: 169 Color: 2
Size: 114 Color: 4

Bin 3249: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 283 Color: 1

Bin 3250: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 1
Size: 170 Color: 4
Size: 113 Color: 0

Bin 3251: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 283 Color: 1

Bin 3252: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 1

Bin 3253: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 283 Color: 1

Bin 3254: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 1

Bin 3255: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 283 Color: 1

Bin 3256: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 0

Bin 3257: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 283 Color: 1

Bin 3258: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 2

Bin 3259: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 283 Color: 4

Bin 3260: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 3

Bin 3261: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 282 Color: 1

Bin 3262: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 1
Size: 282 Color: 0

Bin 3263: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 282 Color: 1

Bin 3264: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 282 Color: 0

Bin 3265: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 282 Color: 1

Bin 3266: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 282 Color: 0

Bin 3267: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 282 Color: 0

Bin 3268: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 282 Color: 3

Bin 3269: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 282 Color: 0

Bin 3270: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 168 Color: 4
Size: 113 Color: 0

Bin 3271: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 282 Color: 4

Bin 3272: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 168 Color: 2
Size: 113 Color: 3

Bin 3273: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 4
Size: 282 Color: 3

Bin 3274: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 168 Color: 4
Size: 113 Color: 0

Bin 3275: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 1
Size: 168 Color: 3
Size: 113 Color: 3

Bin 3276: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 168 Color: 4
Size: 113 Color: 1

Bin 3277: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 1
Size: 168 Color: 3
Size: 113 Color: 3

Bin 3278: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 168 Color: 4
Size: 113 Color: 4

Bin 3279: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 1
Size: 169 Color: 3
Size: 112 Color: 0

Bin 3280: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 169 Color: 4
Size: 112 Color: 1

Bin 3281: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 1
Size: 169 Color: 4
Size: 112 Color: 2

Bin 3282: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 2
Size: 281 Color: 3

Bin 3283: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 3
Size: 281 Color: 1

Bin 3284: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 2
Size: 281 Color: 3

Bin 3285: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 3
Size: 281 Color: 2

Bin 3286: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 4
Size: 281 Color: 2

Bin 3287: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 3
Size: 281 Color: 4

Bin 3288: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 4
Size: 281 Color: 3

Bin 3289: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 2
Size: 168 Color: 3
Size: 112 Color: 1

Bin 3290: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 0
Size: 168 Color: 4
Size: 112 Color: 2

Bin 3291: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 2
Size: 168 Color: 3
Size: 112 Color: 1

Bin 3292: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 1
Size: 280 Color: 2

Bin 3293: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 2
Size: 168 Color: 4
Size: 112 Color: 2

Bin 3294: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 1
Size: 280 Color: 2

Bin 3295: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 2
Size: 280 Color: 0

Bin 3296: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 1
Size: 280 Color: 2

Bin 3297: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 2
Size: 280 Color: 0

Bin 3298: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 0
Size: 167 Color: 4
Size: 112 Color: 3

Bin 3299: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 3
Size: 280 Color: 2

Bin 3300: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 2

Bin 3301: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 3
Size: 280 Color: 4

Bin 3302: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 2

Bin 3303: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 3
Size: 280 Color: 4

Bin 3304: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 2

Bin 3305: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 280 Color: 3

Bin 3306: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 1

Bin 3307: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 1
Size: 279 Color: 2

Bin 3308: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 1

Bin 3309: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 1
Size: 279 Color: 2

Bin 3310: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 1

Bin 3311: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 1
Size: 279 Color: 2

Bin 3312: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 3

Bin 3313: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 1
Size: 279 Color: 3

Bin 3314: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 279 Color: 2

Bin 3315: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 279 Color: 3

Bin 3316: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 279 Color: 4

Bin 3317: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 279 Color: 4

Bin 3318: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 279 Color: 4

Bin 3319: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 278 Color: 1

Bin 3320: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 1
Size: 278 Color: 0

Bin 3321: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 278 Color: 1

Bin 3322: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 1
Size: 278 Color: 0

Bin 3323: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 2
Size: 278 Color: 0

Bin 3324: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 1
Size: 278 Color: 2

Bin 3325: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 2
Size: 278 Color: 1

Bin 3326: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 1
Size: 278 Color: 3

Bin 3327: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 2
Size: 278 Color: 3

Bin 3328: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 3
Size: 278 Color: 2

Bin 3329: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 2
Size: 278 Color: 3

Bin 3330: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 3
Size: 278 Color: 4

Bin 3331: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 2
Size: 278 Color: 4

Bin 3332: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 3
Size: 278 Color: 4

Bin 3333: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 277 Color: 1

Bin 3334: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 3
Size: 278 Color: 4

Bin 3335: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 277 Color: 2

Bin 3336: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 4
Size: 278 Color: 3

Bin 3337: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 277 Color: 1

Bin 3338: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 4
Size: 278 Color: 3

Bin 3339: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 277 Color: 2

Bin 3340: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 277 Color: 1

Bin 3341: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 1
Size: 277 Color: 2

Bin 3342: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 277 Color: 2

Bin 3343: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 2
Size: 277 Color: 4

Bin 3344: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 277 Color: 4

Bin 3345: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 2
Size: 277 Color: 3

Bin 3346: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 277 Color: 4

Bin 3347: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 2
Size: 277 Color: 4

Bin 3348: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 277 Color: 4

Bin 3349: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 4
Size: 277 Color: 3

Bin 3350: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 277 Color: 4

Bin 3351: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 4
Size: 277 Color: 3

Bin 3352: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 0
Size: 164 Color: 2
Size: 112 Color: 2

Bin 3353: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 4
Size: 277 Color: 3

Bin 3354: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 0
Size: 164 Color: 3
Size: 112 Color: 3

Bin 3355: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 1
Size: 165 Color: 2
Size: 111 Color: 0

Bin 3356: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 3
Size: 165 Color: 1
Size: 111 Color: 0

Bin 3357: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 1
Size: 165 Color: 2
Size: 111 Color: 0

Bin 3358: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 3
Size: 165 Color: 2
Size: 111 Color: 1

Bin 3359: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 2
Size: 276 Color: 0

Bin 3360: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 3
Size: 276 Color: 0

Bin 3361: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 2
Size: 276 Color: 4

Bin 3362: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 3
Size: 276 Color: 4

Bin 3363: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 2
Size: 276 Color: 4

Bin 3364: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 3
Size: 276 Color: 4

Bin 3365: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 2

Bin 3366: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 3
Size: 276 Color: 4

Bin 3367: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 1

Bin 3368: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 164 Color: 4
Size: 111 Color: 2

Bin 3369: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 2

Bin 3370: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 164 Color: 3
Size: 111 Color: 1

Bin 3371: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 3

Bin 3372: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 164 Color: 4
Size: 111 Color: 2

Bin 3373: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 3

Bin 3374: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 165 Color: 3
Size: 110 Color: 0

Bin 3375: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 275 Color: 0

Bin 3376: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 165 Color: 4
Size: 110 Color: 0

Bin 3377: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 275 Color: 3

Bin 3378: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 1

Bin 3379: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 275 Color: 4

Bin 3380: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 4

Bin 3381: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 275 Color: 4

Bin 3382: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 4

Bin 3383: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 1
Size: 164 Color: 4
Size: 110 Color: 0

Bin 3384: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 1

Bin 3385: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 1
Size: 164 Color: 4
Size: 110 Color: 0

Bin 3386: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 3

Bin 3387: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 1
Size: 274 Color: 0

Bin 3388: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 3

Bin 3389: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 1
Size: 274 Color: 0

Bin 3390: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 3

Bin 3391: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 1
Size: 274 Color: 3

Bin 3392: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 0
Size: 274 Color: 3

Bin 3393: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 1
Size: 274 Color: 2

Bin 3394: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 274 Color: 3

Bin 3395: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 3
Size: 274 Color: 4

Bin 3396: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 4
Size: 274 Color: 2

Bin 3397: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 3
Size: 274 Color: 4

Bin 3398: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 4
Size: 274 Color: 3

Bin 3399: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 1
Size: 273 Color: 3

Bin 3400: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 0
Size: 273 Color: 3

Bin 3401: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 1
Size: 273 Color: 3

Bin 3402: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 3
Size: 273 Color: 1

Bin 3403: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 2
Size: 273 Color: 1

Bin 3404: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 3
Size: 273 Color: 4

Bin 3405: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 4
Size: 273 Color: 3

Bin 3406: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 0
Size: 162 Color: 2
Size: 110 Color: 2

Bin 3407: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 4
Size: 273 Color: 2

Bin 3408: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 0
Size: 162 Color: 4
Size: 110 Color: 1

Bin 3409: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 4
Size: 273 Color: 3

Bin 3410: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 0
Size: 272 Color: 1

Bin 3411: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 3

Bin 3412: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 0
Size: 272 Color: 1

Bin 3413: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 3

Bin 3414: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 1

Bin 3415: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 3

Bin 3416: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 1

Bin 3417: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 4

Bin 3418: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 4

Bin 3419: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 2

Bin 3420: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 4

Bin 3421: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 2

Bin 3422: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 272 Color: 3

Bin 3423: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 2

Bin 3424: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 272 Color: 3

Bin 3425: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 1

Bin 3426: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 272 Color: 3

Bin 3427: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 1

Bin 3428: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 271 Color: 2

Bin 3429: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 2

Bin 3430: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 271 Color: 3

Bin 3431: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 4

Bin 3432: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 271 Color: 3

Bin 3433: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 4

Bin 3434: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 2
Size: 271 Color: 4

Bin 3435: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 4

Bin 3436: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 4
Size: 271 Color: 3

Bin 3437: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 270 Color: 2

Bin 3438: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 2
Size: 270 Color: 1

Bin 3439: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 4
Size: 270 Color: 1

Bin 3440: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 3
Size: 270 Color: 4

Bin 3441: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 4
Size: 270 Color: 2

Bin 3442: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 3
Size: 270 Color: 4

Bin 3443: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 4
Size: 270 Color: 3

Bin 3444: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 1
Size: 159 Color: 2
Size: 110 Color: 2

Bin 3445: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 0
Size: 159 Color: 3
Size: 110 Color: 1

Bin 3446: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 1
Size: 159 Color: 4
Size: 110 Color: 2

Bin 3447: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 0
Size: 159 Color: 4
Size: 110 Color: 3

Bin 3448: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 1
Size: 159 Color: 4
Size: 110 Color: 2

Bin 3449: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 0

Bin 3450: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 269 Color: 1

Bin 3451: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 2

Bin 3452: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 269 Color: 1

Bin 3453: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 2

Bin 3454: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 269 Color: 1

Bin 3455: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 2

Bin 3456: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 269 Color: 1

Bin 3457: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 4

Bin 3458: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 2

Bin 3459: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 4

Bin 3460: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 3

Bin 3461: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 4

Bin 3462: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 3

Bin 3463: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 0
Size: 158 Color: 3
Size: 110 Color: 3

Bin 3464: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 3

Bin 3465: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 0
Size: 268 Color: 2

Bin 3466: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 1
Size: 268 Color: 0

Bin 3467: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 0
Size: 268 Color: 2

Bin 3468: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 1
Size: 268 Color: 2

Bin 3469: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 3
Size: 268 Color: 2

Bin 3470: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 2
Size: 268 Color: 1

Bin 3471: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 3
Size: 268 Color: 2

Bin 3472: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 2
Size: 268 Color: 3

Bin 3473: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 3
Size: 268 Color: 4

Bin 3474: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 2
Size: 268 Color: 4

Bin 3475: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 3
Size: 268 Color: 4

Bin 3476: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 4
Size: 268 Color: 3

Bin 3477: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 0
Size: 157 Color: 4
Size: 110 Color: 2

Bin 3478: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 4
Size: 268 Color: 3

Bin 3479: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 0
Size: 158 Color: 3
Size: 109 Color: 0

Bin 3480: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 4
Size: 268 Color: 3

Bin 3481: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 0
Size: 158 Color: 4
Size: 109 Color: 0

Bin 3482: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 2
Size: 267 Color: 3

Bin 3483: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 1
Size: 267 Color: 0

Bin 3484: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 2
Size: 267 Color: 1

Bin 3485: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 1
Size: 267 Color: 3

Bin 3486: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 2
Size: 267 Color: 1

Bin 3487: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 1
Size: 267 Color: 3

Bin 3488: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 2
Size: 267 Color: 4

Bin 3489: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 267 Color: 4

Bin 3490: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 0
Size: 266 Color: 1

Bin 3491: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 4
Size: 267 Color: 3

Bin 3492: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 0
Size: 266 Color: 1

Bin 3493: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 0

Bin 3494: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 1

Bin 3495: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 0

Bin 3496: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 1

Bin 3497: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 0

Bin 3498: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 1

Bin 3499: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 0

Bin 3500: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 1

Bin 3501: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 266 Color: 2

Bin 3502: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 4

Bin 3503: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 266 Color: 4

Bin 3504: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 4
Size: 266 Color: 3

Bin 3505: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 266 Color: 4

Bin 3506: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 4
Size: 266 Color: 3

Bin 3507: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 265 Color: 3

Bin 3508: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 4
Size: 266 Color: 3

Bin 3509: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 265 Color: 2

Bin 3510: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 2
Size: 265 Color: 3

Bin 3511: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 1
Size: 265 Color: 2

Bin 3512: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 2
Size: 265 Color: 3

Bin 3513: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 2

Bin 3514: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 2
Size: 265 Color: 4

Bin 3515: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 4

Bin 3516: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 1

Bin 3517: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 4

Bin 3518: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 3

Bin 3519: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 4
Size: 265 Color: 3

Bin 3520: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 1

Bin 3521: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 264 Color: 3

Bin 3522: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 2

Bin 3523: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 264 Color: 3

Bin 3524: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 2

Bin 3525: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 264 Color: 3

Bin 3526: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 2
Size: 264 Color: 4

Bin 3527: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 3
Size: 264 Color: 4

Bin 3528: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 4
Size: 264 Color: 3

Bin 3529: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 3
Size: 264 Color: 4

Bin 3530: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 4
Size: 264 Color: 3

Bin 3531: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 3
Size: 264 Color: 4

Bin 3532: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 4
Size: 264 Color: 3

Bin 3533: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 1
Size: 154 Color: 3
Size: 109 Color: 0

Bin 3534: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 2
Size: 154 Color: 4
Size: 109 Color: 2

Bin 3535: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 1
Size: 154 Color: 3
Size: 109 Color: 1

Bin 3536: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 2
Size: 155 Color: 4
Size: 108 Color: 0

Bin 3537: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 1
Size: 155 Color: 4
Size: 108 Color: 0

Bin 3538: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 2
Size: 263 Color: 1

Bin 3539: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 1
Size: 263 Color: 0

Bin 3540: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 2
Size: 263 Color: 0

Bin 3541: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 1

Bin 3542: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 263 Color: 1

Bin 3543: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 3

Bin 3544: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 263 Color: 4

Bin 3545: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 3

Bin 3546: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 0
Size: 154 Color: 4
Size: 108 Color: 1

Bin 3547: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 2

Bin 3548: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 2
Size: 262 Color: 0

Bin 3549: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 2

Bin 3550: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 2
Size: 262 Color: 1

Bin 3551: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 2

Bin 3552: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 2
Size: 262 Color: 4

Bin 3553: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 4

Bin 3554: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 2
Size: 262 Color: 4

Bin 3555: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 3
Size: 262 Color: 4

Bin 3556: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 262 Color: 2

Bin 3557: 0 of cap free
Amount of items: 3
Items: 
Size: 740 Color: 1
Size: 153 Color: 4
Size: 108 Color: 2

Bin 3558: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 262 Color: 3

Bin 3559: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 0

Bin 3560: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 262 Color: 3

Bin 3561: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 0

Bin 3562: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 261 Color: 1

Bin 3563: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 0

Bin 3564: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 261 Color: 1

Bin 3565: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 2

Bin 3566: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 4

Bin 3567: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 2

Bin 3568: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 4

Bin 3569: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 260 Color: 4

Bin 3570: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 4

Bin 3571: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 260 Color: 2

Bin 3572: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 4
Size: 261 Color: 3

Bin 3573: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 260 Color: 4

Bin 3574: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 260 Color: 4

Bin 3575: 0 of cap free
Amount of items: 3
Items: 
Size: 742 Color: 0
Size: 151 Color: 4
Size: 108 Color: 4

Bin 3576: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 3
Size: 260 Color: 4

Bin 3577: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 259 Color: 1

Bin 3578: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 4
Size: 260 Color: 2

Bin 3579: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 259 Color: 1

Bin 3580: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 4
Size: 260 Color: 3

Bin 3581: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 259 Color: 1

Bin 3582: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 1
Size: 259 Color: 0

Bin 3583: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 259 Color: 3

Bin 3584: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 1
Size: 259 Color: 2

Bin 3585: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 259 Color: 4

Bin 3586: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 0
Size: 258 Color: 1

Bin 3587: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 259 Color: 4

Bin 3588: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 0
Size: 258 Color: 1

Bin 3589: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 4
Size: 259 Color: 3

Bin 3590: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 0
Size: 258 Color: 3

Bin 3591: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 258 Color: 2

Bin 3592: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 3
Size: 258 Color: 2

Bin 3593: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 2
Size: 258 Color: 3

Bin 3594: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 3
Size: 258 Color: 2

Bin 3595: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 2
Size: 258 Color: 3

Bin 3596: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 3
Size: 258 Color: 4

Bin 3597: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 258 Color: 3

Bin 3598: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 257 Color: 0

Bin 3599: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 257 Color: 1

Bin 3600: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 257 Color: 0

Bin 3601: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 257 Color: 1

Bin 3602: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 257 Color: 0

Bin 3603: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 257 Color: 1

Bin 3604: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 257 Color: 2

Bin 3605: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 2
Size: 257 Color: 1

Bin 3606: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 257 Color: 2

Bin 3607: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 2
Size: 257 Color: 1

Bin 3608: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 257 Color: 3

Bin 3609: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 3
Size: 257 Color: 4

Bin 3610: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 257 Color: 3

Bin 3611: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 1

Bin 3612: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 2

Bin 3613: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 2

Bin 3614: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 2

Bin 3615: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 2
Size: 256 Color: 1

Bin 3616: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 3
Size: 256 Color: 1

Bin 3617: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 0
Size: 147 Color: 4
Size: 108 Color: 3

Bin 3618: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 3
Size: 256 Color: 4

Bin 3619: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 0
Size: 147 Color: 3
Size: 108 Color: 4

Bin 3620: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 256 Color: 2

Bin 3621: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 0
Size: 147 Color: 4
Size: 108 Color: 3

Bin 3622: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 256 Color: 3

Bin 3623: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 0
Size: 255 Color: 3

Bin 3624: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 1

Bin 3625: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 0
Size: 255 Color: 3

Bin 3626: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 1

Bin 3627: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 3

Bin 3628: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 4

Bin 3629: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 4

Bin 3630: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 4

Bin 3631: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 4
Size: 255 Color: 2

Bin 3632: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 4

Bin 3633: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 4
Size: 255 Color: 3

Bin 3634: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 254 Color: 2

Bin 3635: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 1
Size: 254 Color: 3

Bin 3636: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 3
Size: 254 Color: 2

Bin 3637: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 1
Size: 254 Color: 4

Bin 3638: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 3
Size: 254 Color: 4

Bin 3639: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 144 Color: 3
Size: 109 Color: 2

Bin 3640: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 3
Size: 254 Color: 4

Bin 3641: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 144 Color: 1
Size: 109 Color: 3

Bin 3642: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 4
Size: 254 Color: 3

Bin 3643: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 144 Color: 3
Size: 109 Color: 3

Bin 3644: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 4
Size: 254 Color: 2

Bin 3645: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 144 Color: 4
Size: 109 Color: 3

Bin 3646: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 4
Size: 254 Color: 3

Bin 3647: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 145 Color: 2
Size: 108 Color: 4

Bin 3648: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 1
Size: 145 Color: 2
Size: 108 Color: 4

Bin 3649: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 146 Color: 4
Size: 107 Color: 0

Bin 3650: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 1
Size: 146 Color: 2
Size: 107 Color: 1

Bin 3651: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 3
Size: 146 Color: 4
Size: 107 Color: 0

Bin 3652: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 2
Size: 146 Color: 3
Size: 107 Color: 1

Bin 3653: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 3
Size: 146 Color: 4
Size: 107 Color: 0

Bin 3654: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 4
Size: 253 Color: 0

Bin 3655: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 3
Size: 253 Color: 0

Bin 3656: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 4
Size: 253 Color: 3

Bin 3657: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 3
Size: 253 Color: 4

Bin 3658: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 4
Size: 253 Color: 0

Bin 3659: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 3
Size: 253 Color: 4

Bin 3660: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 4
Size: 253 Color: 3

Bin 3661: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 0
Size: 145 Color: 1
Size: 107 Color: 1

Bin 3662: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 4
Size: 253 Color: 3

Bin 3663: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 0
Size: 145 Color: 2
Size: 107 Color: 2

Bin 3664: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 1
Size: 145 Color: 3
Size: 107 Color: 1

Bin 3665: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 0
Size: 145 Color: 2
Size: 107 Color: 2

Bin 3666: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 1
Size: 145 Color: 3
Size: 107 Color: 1

Bin 3667: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 2
Size: 252 Color: 0

Bin 3668: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 252 Color: 2

Bin 3669: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 2
Size: 252 Color: 0

Bin 3670: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 252 Color: 2

Bin 3671: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 2
Size: 252 Color: 0

Bin 3672: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 252 Color: 2

Bin 3673: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 2
Size: 252 Color: 1

Bin 3674: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 3

Bin 3675: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 3
Size: 252 Color: 2

Bin 3676: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 1

Bin 3677: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 3
Size: 252 Color: 4

Bin 3678: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 3

Bin 3679: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 3
Size: 252 Color: 4

Bin 3680: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 2

Bin 3681: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 1

Bin 3682: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 3

Bin 3683: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 2

Bin 3684: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 2

Bin 3685: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 3

Bin 3686: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 3

Bin 3687: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 251 Color: 2

Bin 3688: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 3

Bin 3689: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 1
Size: 144 Color: 4
Size: 106 Color: 0

Bin 3690: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 2
Size: 251 Color: 4

Bin 3691: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 0

Bin 3692: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 4
Size: 251 Color: 2

Bin 3693: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 0

Bin 3694: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 4
Size: 251 Color: 3

Bin 3695: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 2

Bin 3696: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 0
Size: 250 Color: 1

Bin 3697: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 2

Bin 3698: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 0
Size: 250 Color: 2

Bin 3699: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 2

Bin 3700: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 3

Bin 3701: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 3

Bin 3702: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 3

Bin 3703: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 3
Size: 250 Color: 2

Bin 3704: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 3

Bin 3705: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 3
Size: 250 Color: 4

Bin 3706: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 4

Bin 3707: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 3
Size: 250 Color: 4

Bin 3708: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 0
Size: 143 Color: 4
Size: 106 Color: 0

Bin 3709: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 4
Size: 250 Color: 3

Bin 3710: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 249 Color: 1

Bin 3711: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 249 Color: 1

Bin 3712: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 249 Color: 3

Bin 3713: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 249 Color: 1

Bin 3714: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 2
Size: 249 Color: 3

Bin 3715: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 249 Color: 2

Bin 3716: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 2
Size: 249 Color: 4

Bin 3717: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 249 Color: 4

Bin 3718: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 3

Bin 3719: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 249 Color: 4

Bin 3720: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 2

Bin 3721: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 0
Size: 142 Color: 4
Size: 106 Color: 1

Bin 3722: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 3

Bin 3723: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 248 Color: 1

Bin 3724: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 3

Bin 3725: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 248 Color: 2

Bin 3726: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 1

Bin 3727: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 248 Color: 2

Bin 3728: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 4

Bin 3729: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 248 Color: 4

Bin 3730: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 4

Bin 3731: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 3
Size: 248 Color: 4

Bin 3732: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 248 Color: 2

Bin 3733: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 3
Size: 248 Color: 4

Bin 3734: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 248 Color: 2

Bin 3735: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 3
Size: 248 Color: 4

Bin 3736: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 248 Color: 3

Bin 3737: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 247 Color: 1

Bin 3738: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 248 Color: 3

Bin 3739: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 247 Color: 3

Bin 3740: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 247 Color: 3

Bin 3741: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 247 Color: 2

Bin 3742: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 0
Size: 140 Color: 4
Size: 106 Color: 0

Bin 3743: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 247 Color: 2

Bin 3744: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 2

Bin 3745: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 247 Color: 2

Bin 3746: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 2

Bin 3747: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 247 Color: 4

Bin 3748: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 1

Bin 3749: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 4
Size: 247 Color: 3

Bin 3750: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 2

Bin 3751: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 1

Bin 3752: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 1
Size: 246 Color: 2

Bin 3753: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 3

Bin 3754: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 1
Size: 246 Color: 2

Bin 3755: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 2

Bin 3756: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 2
Size: 246 Color: 3

Bin 3757: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 3

Bin 3758: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 2
Size: 246 Color: 3

Bin 3759: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 3

Bin 3760: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 0

Bin 3761: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 0
Size: 139 Color: 4
Size: 106 Color: 1

Bin 3762: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 0

Bin 3763: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 0
Size: 139 Color: 4
Size: 106 Color: 0

Bin 3764: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 0

Bin 3765: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 0
Size: 245 Color: 1

Bin 3766: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 0

Bin 3767: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 1

Bin 3768: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 3

Bin 3769: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 3

Bin 3770: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 245 Color: 1

Bin 3771: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 3

Bin 3772: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 245 Color: 4

Bin 3773: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 2

Bin 3774: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 245 Color: 4

Bin 3775: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 3

Bin 3776: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 245 Color: 4

Bin 3777: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 3

Bin 3778: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 245 Color: 4

Bin 3779: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 3

Bin 3780: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 245 Color: 4

Bin 3781: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 3

Bin 3782: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 245 Color: 4

Bin 3783: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 4
Size: 245 Color: 3

Bin 3784: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 244 Color: 0

Bin 3785: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 0
Size: 244 Color: 2

Bin 3786: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 244 Color: 0

Bin 3787: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 2

Bin 3788: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 244 Color: 0

Bin 3789: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 4

Bin 3790: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 244 Color: 0

Bin 3791: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 4

Bin 3792: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 244 Color: 2

Bin 3793: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 4

Bin 3794: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 244 Color: 1

Bin 3795: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 0
Size: 137 Color: 4
Size: 106 Color: 1

Bin 3796: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 244 Color: 2

Bin 3797: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 0
Size: 137 Color: 3
Size: 106 Color: 0

Bin 3798: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 244 Color: 3

Bin 3799: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 0
Size: 137 Color: 4
Size: 106 Color: 1

Bin 3800: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 244 Color: 3

Bin 3801: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 0
Size: 243 Color: 1

Bin 3802: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 243 Color: 1

Bin 3803: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 243 Color: 2

Bin 3804: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 243 Color: 1

Bin 3805: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 3
Size: 243 Color: 2

Bin 3806: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 243 Color: 3

Bin 3807: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 3
Size: 243 Color: 1

Bin 3808: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 0
Size: 136 Color: 4
Size: 106 Color: 2

Bin 3809: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 3
Size: 243 Color: 2

Bin 3810: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 0
Size: 242 Color: 1

Bin 3811: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 3
Size: 243 Color: 4

Bin 3812: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 0
Size: 242 Color: 1

Bin 3813: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 2

Bin 3814: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 0
Size: 242 Color: 4

Bin 3815: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 3

Bin 3816: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 0
Size: 242 Color: 4

Bin 3817: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 242 Color: 2

Bin 3818: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 2
Size: 242 Color: 4

Bin 3819: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 242 Color: 1

Bin 3820: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 4

Bin 3821: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 242 Color: 2

Bin 3822: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 4

Bin 3823: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 242 Color: 3

Bin 3824: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 0
Size: 241 Color: 1

Bin 3825: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 242 Color: 3

Bin 3826: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 0
Size: 241 Color: 1

Bin 3827: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 1
Size: 241 Color: 0

Bin 3828: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 241 Color: 1

Bin 3829: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 1
Size: 241 Color: 3

Bin 3830: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 241 Color: 1

Bin 3831: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 1
Size: 241 Color: 3

Bin 3832: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 241 Color: 3

Bin 3833: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 3
Size: 241 Color: 4

Bin 3834: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 241 Color: 3

Bin 3835: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 3
Size: 241 Color: 4

Bin 3836: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 241 Color: 4

Bin 3837: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 3
Size: 241 Color: 4

Bin 3838: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 4
Size: 241 Color: 3

Bin 3839: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 2

Bin 3840: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 240 Color: 2

Bin 3841: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 3

Bin 3842: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 240 Color: 4

Bin 3843: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 3

Bin 3844: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 2
Size: 240 Color: 4

Bin 3845: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 4

Bin 3846: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 0

Bin 3847: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 4

Bin 3848: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 2

Bin 3849: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 4
Size: 240 Color: 3

Bin 3850: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 0

Bin 3851: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 4
Size: 240 Color: 3

Bin 3852: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 2

Bin 3853: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 0
Size: 239 Color: 1

Bin 3854: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 2

Bin 3855: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 239 Color: 4

Bin 3856: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 4
Size: 239 Color: 1

Bin 3857: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 239 Color: 4

Bin 3858: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 4
Size: 239 Color: 2

Bin 3859: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 239 Color: 4

Bin 3860: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 4
Size: 239 Color: 3

Bin 3861: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 239 Color: 4

Bin 3862: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 4
Size: 239 Color: 3

Bin 3863: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 0
Size: 132 Color: 3
Size: 106 Color: 3

Bin 3864: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 1
Size: 238 Color: 0

Bin 3865: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 0
Size: 132 Color: 4
Size: 106 Color: 2

Bin 3866: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 1
Size: 238 Color: 0

Bin 3867: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 0
Size: 132 Color: 4
Size: 106 Color: 3

Bin 3868: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 1
Size: 238 Color: 0

Bin 3869: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 0
Size: 238 Color: 1

Bin 3870: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 1
Size: 238 Color: 0

Bin 3871: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 2
Size: 238 Color: 0

Bin 3872: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 3
Size: 238 Color: 2

Bin 3873: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 2
Size: 238 Color: 1

Bin 3874: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 3
Size: 238 Color: 4

Bin 3875: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 2
Size: 238 Color: 4

Bin 3876: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 3
Size: 238 Color: 4

Bin 3877: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 2

Bin 3878: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 3
Size: 238 Color: 4

Bin 3879: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 1

Bin 3880: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 4
Size: 238 Color: 3

Bin 3881: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 2

Bin 3882: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 4
Size: 238 Color: 3

Bin 3883: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 3

Bin 3884: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 237 Color: 4

Bin 3885: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 237 Color: 4

Bin 3886: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 237 Color: 4

Bin 3887: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 237 Color: 4

Bin 3888: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 4
Size: 237 Color: 2

Bin 3889: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 0
Size: 129 Color: 4
Size: 107 Color: 2

Bin 3890: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 4
Size: 237 Color: 3

Bin 3891: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 0
Size: 129 Color: 4
Size: 107 Color: 3

Bin 3892: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 4
Size: 237 Color: 2

Bin 3893: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 0
Size: 129 Color: 4
Size: 107 Color: 4

Bin 3894: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 4
Size: 237 Color: 3

Bin 3895: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 0
Size: 130 Color: 1
Size: 106 Color: 2

Bin 3896: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 2
Size: 236 Color: 0

Bin 3897: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 0
Size: 131 Color: 4
Size: 105 Color: 0

Bin 3898: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 2
Size: 236 Color: 0

Bin 3899: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 1
Size: 236 Color: 0

Bin 3900: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 2
Size: 236 Color: 1

Bin 3901: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 0
Size: 130 Color: 2
Size: 105 Color: 1

Bin 3902: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 3
Size: 236 Color: 4

Bin 3903: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 0
Size: 130 Color: 1
Size: 105 Color: 0

Bin 3904: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 4
Size: 236 Color: 2

Bin 3905: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 0
Size: 130 Color: 2
Size: 105 Color: 1

Bin 3906: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 4
Size: 236 Color: 3

Bin 3907: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 0
Size: 130 Color: 3
Size: 105 Color: 2

Bin 3908: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 4
Size: 236 Color: 2

Bin 3909: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 0
Size: 130 Color: 4
Size: 105 Color: 3

Bin 3910: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 4
Size: 236 Color: 3

Bin 3911: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 0
Size: 130 Color: 4
Size: 105 Color: 4

Bin 3912: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 1
Size: 235 Color: 2

Bin 3913: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 2
Size: 235 Color: 4

Bin 3914: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 1
Size: 235 Color: 3

Bin 3915: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 2
Size: 235 Color: 4

Bin 3916: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 1
Size: 235 Color: 4

Bin 3917: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 2
Size: 235 Color: 4

Bin 3918: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 3
Size: 235 Color: 4

Bin 3919: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 234 Color: 3

Bin 3920: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 4
Size: 235 Color: 3

Bin 3921: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 234 Color: 0

Bin 3922: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 234 Color: 3

Bin 3923: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 234 Color: 0

Bin 3924: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 234 Color: 1

Bin 3925: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 234 Color: 3

Bin 3926: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 234 Color: 1

Bin 3927: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 234 Color: 3

Bin 3928: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 2
Size: 234 Color: 3

Bin 3929: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 3
Size: 234 Color: 1

Bin 3930: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 2
Size: 234 Color: 3

Bin 3931: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 3
Size: 234 Color: 1

Bin 3932: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 2
Size: 234 Color: 3

Bin 3933: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 3
Size: 234 Color: 4

Bin 3934: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 4
Size: 234 Color: 3

Bin 3935: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 1
Size: 233 Color: 2

Bin 3936: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 0
Size: 233 Color: 1

Bin 3937: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 1
Size: 233 Color: 2

Bin 3938: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 0
Size: 233 Color: 2

Bin 3939: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 1
Size: 233 Color: 3

Bin 3940: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 4

Bin 3941: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 2
Size: 233 Color: 4

Bin 3942: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 4

Bin 3943: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 2
Size: 233 Color: 4

Bin 3944: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 4

Bin 3945: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 0
Size: 127 Color: 3
Size: 105 Color: 3

Bin 3946: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 4
Size: 233 Color: 3

Bin 3947: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 0
Size: 127 Color: 4
Size: 105 Color: 4

Bin 3948: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 4
Size: 233 Color: 3

Bin 3949: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 0
Size: 127 Color: 4
Size: 105 Color: 4

Bin 3950: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 4
Size: 233 Color: 3

Bin 3951: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 0
Size: 128 Color: 3
Size: 104 Color: 0

Bin 3952: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 1
Size: 128 Color: 3
Size: 104 Color: 0

Bin 3953: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 0
Size: 128 Color: 4
Size: 104 Color: 1

Bin 3954: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 1
Size: 128 Color: 4
Size: 104 Color: 0

Bin 3955: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 2
Size: 128 Color: 4
Size: 104 Color: 1

Bin 3956: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 1

Bin 3957: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 2
Size: 232 Color: 0

Bin 3958: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 1

Bin 3959: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 2
Size: 232 Color: 4

Bin 3960: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 4

Bin 3961: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 232 Color: 0

Bin 3962: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 4

Bin 3963: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 232 Color: 1

Bin 3964: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 231 Color: 1

Bin 3965: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 232 Color: 3

Bin 3966: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 231 Color: 2

Bin 3967: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 232 Color: 3

Bin 3968: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 231 Color: 1

Bin 3969: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 1
Size: 231 Color: 2

Bin 3970: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 231 Color: 1

Bin 3971: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 1
Size: 231 Color: 2

Bin 3972: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 2
Size: 231 Color: 4

Bin 3973: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 3
Size: 231 Color: 2

Bin 3974: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 2
Size: 231 Color: 3

Bin 3975: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 3
Size: 231 Color: 4

Bin 3976: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 1

Bin 3977: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 3
Size: 231 Color: 4

Bin 3978: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 1

Bin 3979: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 230 Color: 0

Bin 3980: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 2
Size: 230 Color: 1

Bin 3981: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 230 Color: 2

Bin 3982: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 2
Size: 230 Color: 1

Bin 3983: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 230 Color: 2

Bin 3984: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 4

Bin 3985: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 230 Color: 3

Bin 3986: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 4

Bin 3987: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 230 Color: 3

Bin 3988: 0 of cap free
Amount of items: 3
Items: 
Size: 772 Color: 0
Size: 125 Color: 1
Size: 104 Color: 4

Bin 3989: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 229 Color: 0

Bin 3990: 0 of cap free
Amount of items: 3
Items: 
Size: 772 Color: 0
Size: 125 Color: 2
Size: 104 Color: 2

Bin 3991: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 229 Color: 4

Bin 3992: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 0
Size: 229 Color: 4

Bin 3993: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 229 Color: 4

Bin 3994: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 2
Size: 229 Color: 4

Bin 3995: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 3
Size: 229 Color: 4

Bin 3996: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 124 Color: 4
Size: 104 Color: 4

Bin 3997: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 0

Bin 3998: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 124 Color: 3
Size: 104 Color: 2

Bin 3999: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 1

Bin 4000: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 124 Color: 4
Size: 104 Color: 4

Bin 4001: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 0

Bin 4002: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 124 Color: 3
Size: 104 Color: 3

Bin 4003: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 1

Bin 4004: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 124 Color: 4
Size: 104 Color: 4

Bin 4005: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 2

Bin 4006: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 125 Color: 1
Size: 103 Color: 1

Bin 4007: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 1

Bin 4008: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 125 Color: 2
Size: 103 Color: 1

Bin 4009: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 2

Bin 4010: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 125 Color: 1
Size: 103 Color: 0

Bin 4011: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 3

Bin 4012: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 0
Size: 125 Color: 2
Size: 103 Color: 1

Bin 4013: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 2
Size: 125 Color: 4
Size: 103 Color: 3

Bin 4014: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 1
Size: 125 Color: 3
Size: 103 Color: 1

Bin 4015: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 2
Size: 228 Color: 1

Bin 4016: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 1
Size: 125 Color: 4
Size: 103 Color: 3

Bin 4017: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 2
Size: 228 Color: 1

Bin 4018: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 1
Size: 228 Color: 0

Bin 4019: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 2
Size: 228 Color: 0

Bin 4020: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 3
Size: 228 Color: 1

Bin 4021: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 228 Color: 3

Bin 4022: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 3
Size: 228 Color: 4

Bin 4023: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 228 Color: 2

Bin 4024: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 3
Size: 228 Color: 4

Bin 4025: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 228 Color: 3

Bin 4026: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 0
Size: 124 Color: 4
Size: 103 Color: 2

Bin 4027: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 1

Bin 4028: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 227 Color: 0

Bin 4029: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 1

Bin 4030: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 227 Color: 0

Bin 4031: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 0

Bin 4032: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 227 Color: 3

Bin 4033: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 1

Bin 4034: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 227 Color: 3

Bin 4035: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 2

Bin 4036: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 2
Size: 227 Color: 3

Bin 4037: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 2

Bin 4038: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 226 Color: 1

Bin 4039: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 226 Color: 0

Bin 4040: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 226 Color: 1

Bin 4041: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 226 Color: 3

Bin 4042: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 226 Color: 2

Bin 4043: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 226 Color: 3

Bin 4044: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 1
Size: 226 Color: 2

Bin 4045: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 226 Color: 4

Bin 4046: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 3
Size: 226 Color: 4

Bin 4047: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 4
Size: 226 Color: 3

Bin 4048: 0 of cap free
Amount of items: 3
Items: 
Size: 776 Color: 0
Size: 122 Color: 4
Size: 103 Color: 3

Bin 4049: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 2
Size: 225 Color: 0

Bin 4050: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 1
Size: 225 Color: 0

Bin 4051: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 2
Size: 225 Color: 0

Bin 4052: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 1
Size: 225 Color: 2

Bin 4053: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 2
Size: 225 Color: 0

Bin 4054: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 3
Size: 225 Color: 1

Bin 4055: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 4
Size: 225 Color: 2

Bin 4056: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 3
Size: 225 Color: 1

Bin 4057: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 4
Size: 225 Color: 2

Bin 4058: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 3
Size: 225 Color: 4

Bin 4059: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 4
Size: 225 Color: 3

Bin 4060: 0 of cap free
Amount of items: 3
Items: 
Size: 777 Color: 1
Size: 121 Color: 4
Size: 103 Color: 2

Bin 4061: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 0
Size: 224 Color: 1

Bin 4062: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 1
Size: 224 Color: 0

Bin 4063: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 2
Size: 224 Color: 3

Bin 4064: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 2

Bin 4065: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 3
Size: 224 Color: 2

Bin 4066: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 3

Bin 4067: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 3
Size: 224 Color: 4

Bin 4068: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 2

Bin 4069: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 3
Size: 224 Color: 4

Bin 4070: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 3

Bin 4071: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 3
Size: 224 Color: 4

Bin 4072: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 2

Bin 4073: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 223 Color: 2

Bin 4074: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 2
Size: 223 Color: 3

Bin 4075: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 223 Color: 4

Bin 4076: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 2
Size: 223 Color: 4

Bin 4077: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 3
Size: 223 Color: 4

Bin 4078: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 4
Size: 223 Color: 3

Bin 4079: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 0
Size: 119 Color: 4
Size: 103 Color: 3

Bin 4080: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 0

Bin 4081: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 0
Size: 119 Color: 4
Size: 103 Color: 4

Bin 4082: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 0

Bin 4083: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 0
Size: 119 Color: 4
Size: 103 Color: 4

Bin 4084: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 2

Bin 4085: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 222 Color: 1

Bin 4086: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 2

Bin 4087: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 222 Color: 3

Bin 4088: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 2

Bin 4089: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 222 Color: 3

Bin 4090: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 2

Bin 4091: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 2
Size: 222 Color: 3

Bin 4092: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 4

Bin 4093: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 2
Size: 222 Color: 4

Bin 4094: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 222 Color: 3

Bin 4095: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 3
Size: 222 Color: 4

Bin 4096: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 222 Color: 3

Bin 4097: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 1
Size: 221 Color: 2

Bin 4098: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 0
Size: 221 Color: 2

Bin 4099: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 1
Size: 221 Color: 2

Bin 4100: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 2
Size: 221 Color: 3

Bin 4101: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 3
Size: 221 Color: 4

Bin 4102: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 2
Size: 221 Color: 4

Bin 4103: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 3
Size: 221 Color: 4

Bin 4104: 0 of cap free
Amount of items: 3
Items: 
Size: 781 Color: 0
Size: 118 Color: 3
Size: 102 Color: 0

Bin 4105: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 3
Size: 221 Color: 4

Bin 4106: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 1

Bin 4107: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 221 Color: 3

Bin 4108: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 1

Bin 4109: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 221 Color: 3

Bin 4110: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 1

Bin 4111: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 221 Color: 3

Bin 4112: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 1

Bin 4113: 0 of cap free
Amount of items: 3
Items: 
Size: 781 Color: 1
Size: 118 Color: 4
Size: 102 Color: 0

Bin 4114: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 1

Bin 4115: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 220 Color: 0

Bin 4116: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 1

Bin 4117: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 220 Color: 2

Bin 4118: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 4

Bin 4119: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 4
Size: 220 Color: 3

Bin 4120: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 4

Bin 4121: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 4
Size: 220 Color: 3

Bin 4122: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 3
Size: 220 Color: 4

Bin 4123: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 4
Size: 220 Color: 3

Bin 4124: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 0

Bin 4125: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 0
Size: 219 Color: 1

Bin 4126: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 2

Bin 4127: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 0
Size: 219 Color: 1

Bin 4128: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 2

Bin 4129: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 0
Size: 219 Color: 3

Bin 4130: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 3

Bin 4131: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 3
Size: 219 Color: 2

Bin 4132: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 3

Bin 4133: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 3
Size: 219 Color: 2

Bin 4134: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 2
Size: 219 Color: 4

Bin 4135: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 3
Size: 219 Color: 4

Bin 4136: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 4
Size: 219 Color: 3

Bin 4137: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 218 Color: 2

Bin 4138: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 3

Bin 4139: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 218 Color: 2

Bin 4140: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 3

Bin 4141: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 1
Size: 218 Color: 4

Bin 4142: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 4

Bin 4143: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 3
Size: 218 Color: 4

Bin 4144: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 2

Bin 4145: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 4
Size: 218 Color: 3

Bin 4146: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 4

Bin 4147: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 2

Bin 4148: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 4

Bin 4149: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 1

Bin 4150: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 2
Size: 217 Color: 4

Bin 4151: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 2

Bin 4152: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 2
Size: 217 Color: 4

Bin 4153: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 3

Bin 4154: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 2
Size: 217 Color: 4

Bin 4155: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 3

Bin 4156: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 1

Bin 4157: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 3

Bin 4158: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 2

Bin 4159: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 2
Size: 216 Color: 3

Bin 4160: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 1
Size: 216 Color: 4

Bin 4161: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 2
Size: 216 Color: 3

Bin 4162: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 3
Size: 216 Color: 4

Bin 4163: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 2
Size: 216 Color: 4

Bin 4164: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 3
Size: 216 Color: 4

Bin 4165: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 4
Size: 216 Color: 3

Bin 4166: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 0
Size: 113 Color: 4
Size: 102 Color: 0

Bin 4167: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 4
Size: 216 Color: 3

Bin 4168: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 0
Size: 113 Color: 4
Size: 102 Color: 2

Bin 4169: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 2
Size: 215 Color: 0

Bin 4170: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 215 Color: 1

Bin 4171: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 2
Size: 215 Color: 0

Bin 4172: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 2

Bin 4173: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 2
Size: 215 Color: 1

Bin 4174: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 2

Bin 4175: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 2
Size: 215 Color: 1

Bin 4176: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 215 Color: 4

Bin 4177: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 0
Size: 112 Color: 4
Size: 102 Color: 0

Bin 4178: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 215 Color: 4

Bin 4179: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 0
Size: 112 Color: 3
Size: 102 Color: 2

Bin 4180: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 215 Color: 4

Bin 4181: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 0
Size: 112 Color: 4
Size: 102 Color: 1

Bin 4182: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 215 Color: 3

Bin 4183: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 0
Size: 112 Color: 4
Size: 102 Color: 2

Bin 4184: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 215 Color: 3

Bin 4185: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 0
Size: 214 Color: 1

Bin 4186: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 1
Size: 214 Color: 0

Bin 4187: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 3
Size: 214 Color: 1

Bin 4188: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 1
Size: 214 Color: 0

Bin 4189: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 3
Size: 214 Color: 1

Bin 4190: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 214 Color: 4

Bin 4191: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 3
Size: 214 Color: 2

Bin 4192: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 214 Color: 4

Bin 4193: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 3
Size: 214 Color: 4

Bin 4194: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 0
Size: 111 Color: 3
Size: 102 Color: 1

Bin 4195: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 4
Size: 214 Color: 2

Bin 4196: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 0
Size: 213 Color: 1

Bin 4197: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 3

Bin 4198: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 0
Size: 213 Color: 1

Bin 4199: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 3

Bin 4200: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 0
Size: 213 Color: 1

Bin 4201: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 3

Bin 4202: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 2

Bin 4203: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 2
Size: 213 Color: 3

Bin 4204: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 3

Bin 4205: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 3
Size: 213 Color: 4

Bin 4206: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 2

Bin 4207: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 3
Size: 213 Color: 4

Bin 4208: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 3

Bin 4209: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 0

Bin 4210: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 3

Bin 4211: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 2

Bin 4212: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 2
Size: 212 Color: 1

Bin 4213: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 2

Bin 4214: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 2
Size: 212 Color: 1

Bin 4215: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 3
Size: 212 Color: 4

Bin 4216: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 2

Bin 4217: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 3
Size: 212 Color: 4

Bin 4218: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 1

Bin 4219: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 0
Size: 110 Color: 3
Size: 101 Color: 0

Bin 4220: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 2

Bin 4221: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 0
Size: 110 Color: 4
Size: 101 Color: 1

Bin 4222: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 3

Bin 4223: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 0
Size: 110 Color: 4
Size: 101 Color: 0

Bin 4224: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 3

Bin 4225: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 0
Size: 110 Color: 4
Size: 101 Color: 1

Bin 4226: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 211 Color: 0

Bin 4227: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 1
Size: 211 Color: 2

Bin 4228: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 211 Color: 0

Bin 4229: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 1
Size: 211 Color: 2

Bin 4230: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 211 Color: 1

Bin 4231: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 211 Color: 2

Bin 4232: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 211 Color: 4

Bin 4233: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 211 Color: 3

Bin 4234: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 210 Color: 2

Bin 4235: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 1
Size: 210 Color: 3

Bin 4236: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 210 Color: 2

Bin 4237: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 1
Size: 210 Color: 4

Bin 4238: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 2
Size: 210 Color: 4

Bin 4239: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 3
Size: 210 Color: 4

Bin 4240: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 1
Size: 108 Color: 4
Size: 101 Color: 0

Bin 4241: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 4
Size: 210 Color: 3

Bin 4242: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 209 Color: 0

Bin 4243: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 4
Size: 210 Color: 3

Bin 4244: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 209 Color: 0

Bin 4245: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 1

Bin 4246: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 2
Size: 209 Color: 0

Bin 4247: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 1

Bin 4248: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 2
Size: 209 Color: 0

Bin 4249: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 4

Bin 4250: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 4
Size: 209 Color: 1

Bin 4251: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 4

Bin 4252: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 4
Size: 209 Color: 1

Bin 4253: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 4

Bin 4254: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 4
Size: 209 Color: 2

Bin 4255: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 4

Bin 4256: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 4
Size: 209 Color: 2

Bin 4257: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 0
Size: 208 Color: 1

Bin 4258: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 2
Size: 208 Color: 1

Bin 4259: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 208 Color: 0

Bin 4260: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 2
Size: 208 Color: 3

Bin 4261: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 208 Color: 2

Bin 4262: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 2
Size: 208 Color: 4

Bin 4263: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 208 Color: 4

Bin 4264: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 3

Bin 4265: 0 of cap free
Amount of items: 3
Items: 
Size: 794 Color: 0
Size: 106 Color: 3
Size: 101 Color: 1

Bin 4266: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 3

Bin 4267: 0 of cap free
Amount of items: 3
Items: 
Size: 794 Color: 0
Size: 106 Color: 4
Size: 101 Color: 0

Bin 4268: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 207 Color: 0

Bin 4269: 0 of cap free
Amount of items: 3
Items: 
Size: 794 Color: 0
Size: 106 Color: 3
Size: 101 Color: 1

Bin 4270: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 207 Color: 0

Bin 4271: 0 of cap free
Amount of items: 3
Items: 
Size: 794 Color: 0
Size: 106 Color: 4
Size: 101 Color: 3

Bin 4272: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 207 Color: 2

Bin 4273: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 2
Size: 207 Color: 1

Bin 4274: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 207 Color: 2

Bin 4275: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 2
Size: 207 Color: 1

Bin 4276: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 3
Size: 207 Color: 2

Bin 4277: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 2
Size: 207 Color: 1

Bin 4278: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 3
Size: 207 Color: 4

Bin 4279: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 4
Size: 207 Color: 3

Bin 4280: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 2

Bin 4281: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 0
Size: 206 Color: 1

Bin 4282: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 2

Bin 4283: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 0
Size: 206 Color: 1

Bin 4284: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 2

Bin 4285: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 2
Size: 206 Color: 1

Bin 4286: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 2

Bin 4287: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 2
Size: 206 Color: 4

Bin 4288: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 4

Bin 4289: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 2
Size: 206 Color: 4

Bin 4290: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 4
Size: 206 Color: 1

Bin 4291: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 3
Size: 206 Color: 4

Bin 4292: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 4
Size: 206 Color: 2

Bin 4293: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 205 Color: 0

Bin 4294: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 0
Size: 205 Color: 1

Bin 4295: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 205 Color: 0

Bin 4296: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 2
Size: 205 Color: 0

Bin 4297: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 205 Color: 2

Bin 4298: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 2
Size: 205 Color: 1

Bin 4299: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 3
Size: 205 Color: 1

Bin 4300: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 2
Size: 205 Color: 4

Bin 4301: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 3
Size: 205 Color: 4

Bin 4302: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 204 Color: 3

Bin 4303: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 4
Size: 205 Color: 2

Bin 4304: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 204 Color: 2

Bin 4305: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 4
Size: 205 Color: 3

Bin 4306: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 204 Color: 3

Bin 4307: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 0
Size: 204 Color: 2

Bin 4308: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 204 Color: 4

Bin 4309: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 2
Size: 204 Color: 4

Bin 4310: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 4

Bin 4311: 0 of cap free
Amount of items: 3
Items: 
Size: 798 Color: 0
Size: 102 Color: 2
Size: 101 Color: 2

Bin 4312: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 4

Bin 4313: 0 of cap free
Amount of items: 3
Items: 
Size: 798 Color: 0
Size: 102 Color: 3
Size: 101 Color: 3

Bin 4314: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 4
Size: 204 Color: 3

Bin 4315: 0 of cap free
Amount of items: 3
Items: 
Size: 798 Color: 0
Size: 102 Color: 4
Size: 101 Color: 3

Bin 4316: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 0

Bin 4317: 0 of cap free
Amount of items: 3
Items: 
Size: 798 Color: 0
Size: 102 Color: 4
Size: 101 Color: 4

Bin 4318: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 0

Bin 4319: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 203 Color: 3

Bin 4320: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 0

Bin 4321: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 203 Color: 3

Bin 4322: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 1

Bin 4323: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 4
Size: 203 Color: 2

Bin 4324: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 4

Bin 4325: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 4
Size: 203 Color: 3

Bin 4326: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 0
Size: 202 Color: 1

Bin 4327: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 2

Bin 4328: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 0
Size: 202 Color: 3

Bin 4329: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 4

Bin 4330: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 0
Size: 202 Color: 4

Bin 4331: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 4

Bin 4332: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 2
Size: 202 Color: 4

Bin 4333: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 3
Size: 202 Color: 4

Bin 4334: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 4
Size: 202 Color: 3

Bin 4335: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 0

Bin 4336: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 201 Color: 0

Bin 4337: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 1

Bin 4338: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 201 Color: 0

Bin 4339: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 1

Bin 4340: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 201 Color: 3

Bin 4341: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 2

Bin 4342: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 4
Size: 201 Color: 3

Bin 4343: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 2

Bin 4344: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 4
Size: 201 Color: 3

Bin 4345: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 2

Bin 4346: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 4
Size: 201 Color: 3

Bin 4347: 0 of cap free
Amount of items: 3
Items: 
Size: 801 Color: 4
Size: 100 Color: 3
Size: 100 Color: 0

Bin 4348: 0 of cap free
Amount of items: 3
Items: 
Size: 801 Color: 2
Size: 100 Color: 4
Size: 100 Color: 0

Bin 4349: 0 of cap free
Amount of items: 3
Items: 
Size: 801 Color: 4
Size: 100 Color: 3
Size: 100 Color: 2

Bin 4350: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 2
Size: 200 Color: 0

Bin 4351: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 0

Bin 4352: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 2
Size: 200 Color: 1

Bin 4353: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 0

Bin 4354: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 2
Size: 200 Color: 1

Bin 4355: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 2

Bin 4356: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 4

Bin 4357: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 1

Bin 4358: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 4

Bin 4359: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 2

Bin 4360: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 4

Bin 4361: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 3

Bin 4362: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 4

Bin 4363: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 3

Bin 4364: 1 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 411 Color: 1
Size: 177 Color: 0

Bin 4365: 1 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 400 Color: 3
Size: 177 Color: 1

Bin 4366: 1 of cap free
Amount of items: 4
Items: 
Size: 429 Color: 0
Size: 191 Color: 3
Size: 191 Color: 2
Size: 189 Color: 1

Bin 4367: 1 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 343 Color: 1
Size: 168 Color: 0

Bin 4368: 1 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 494 Color: 2

Bin 4369: 1 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 0
Size: 489 Color: 4

Bin 4370: 1 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 484 Color: 1

Bin 4371: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 199 Color: 3

Bin 4372: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 1
Size: 199 Color: 2

Bin 4373: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 199 Color: 3

Bin 4374: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 1
Size: 199 Color: 4

Bin 4375: 2 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 489 Color: 1

Bin 4376: 2 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 484 Color: 2

Bin 4377: 2 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 199 Color: 3

Bin 4378: 2 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 199 Color: 3

Bin 4379: 2 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 199 Color: 3

Bin 4380: 2 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 199 Color: 2

Bin 4381: 3 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 494 Color: 3

Bin 4382: 3 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 484 Color: 2

Bin 4383: 3 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 484 Color: 1

Bin 4384: 3 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 479 Color: 1

Bin 4385: 3 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 479 Color: 1

Bin 4386: 3 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 479 Color: 1

Bin 4387: 3 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 198 Color: 4

Bin 4388: 3 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 198 Color: 4

Bin 4389: 4 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 0
Size: 494 Color: 2

Bin 4390: 5 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 0
Size: 483 Color: 2

Bin 4391: 7 of cap free
Amount of items: 4
Items: 
Size: 428 Color: 0
Size: 189 Color: 3
Size: 189 Color: 3
Size: 188 Color: 1

Bin 4392: 8 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 0
Size: 494 Color: 2

Bin 4393: 8 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 0
Size: 494 Color: 1

Bin 4394: 8 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 483 Color: 1

Bin 4395: 9 of cap free
Amount of items: 4
Items: 
Size: 426 Color: 0
Size: 189 Color: 3
Size: 189 Color: 3
Size: 188 Color: 0

Bin 4396: 9 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 0
Size: 483 Color: 2

Bin 4397: 14 of cap free
Amount of items: 2
Items: 
Size: 494 Color: 0
Size: 493 Color: 1

Bin 4398: 15 of cap free
Amount of items: 4
Items: 
Size: 425 Color: 0
Size: 187 Color: 3
Size: 187 Color: 2
Size: 187 Color: 0

Bin 4399: 16 of cap free
Amount of items: 4
Items: 
Size: 425 Color: 0
Size: 187 Color: 2
Size: 187 Color: 2
Size: 186 Color: 3

Bin 4400: 18 of cap free
Amount of items: 4
Items: 
Size: 425 Color: 0
Size: 186 Color: 4
Size: 186 Color: 2
Size: 186 Color: 1

Bin 4401: 20 of cap free
Amount of items: 4
Items: 
Size: 423 Color: 0
Size: 186 Color: 2
Size: 186 Color: 0
Size: 186 Color: 2

Bin 4402: 20 of cap free
Amount of items: 4
Items: 
Size: 423 Color: 0
Size: 186 Color: 2
Size: 186 Color: 1
Size: 186 Color: 2

Bin 4403: 23 of cap free
Amount of items: 4
Items: 
Size: 423 Color: 0
Size: 185 Color: 2
Size: 185 Color: 4
Size: 185 Color: 2

Bin 4404: 23 of cap free
Amount of items: 4
Items: 
Size: 423 Color: 0
Size: 185 Color: 4
Size: 185 Color: 2
Size: 185 Color: 4

Bin 4405: 28 of cap free
Amount of items: 4
Items: 
Size: 418 Color: 0
Size: 185 Color: 2
Size: 185 Color: 1
Size: 185 Color: 0

Bin 4406: 28 of cap free
Amount of items: 4
Items: 
Size: 418 Color: 0
Size: 185 Color: 1
Size: 185 Color: 2
Size: 185 Color: 1

Bin 4407: 29 of cap free
Amount of items: 4
Items: 
Size: 418 Color: 0
Size: 185 Color: 1
Size: 185 Color: 0
Size: 184 Color: 4

Bin 4408: 29 of cap free
Amount of items: 2
Items: 
Size: 489 Color: 0
Size: 483 Color: 2

Bin 4409: 29 of cap free
Amount of items: 2
Items: 
Size: 489 Color: 0
Size: 483 Color: 1

Bin 4410: 33 of cap free
Amount of items: 2
Items: 
Size: 485 Color: 0
Size: 483 Color: 1

Bin 4411: 34 of cap free
Amount of items: 4
Items: 
Size: 415 Color: 0
Size: 184 Color: 2
Size: 184 Color: 1
Size: 184 Color: 2

Bin 4412: 35 of cap free
Amount of items: 4
Items: 
Size: 415 Color: 0
Size: 184 Color: 1
Size: 184 Color: 0
Size: 183 Color: 1

Bin 4413: 38 of cap free
Amount of items: 2
Items: 
Size: 485 Color: 0
Size: 478 Color: 1

Bin 4414: 40 of cap free
Amount of items: 4
Items: 
Size: 415 Color: 0
Size: 182 Color: 3
Size: 182 Color: 2
Size: 182 Color: 3

Bin 4415: 41 of cap free
Amount of items: 4
Items: 
Size: 414 Color: 0
Size: 182 Color: 2
Size: 182 Color: 1
Size: 182 Color: 2

Bin 4416: 41 of cap free
Amount of items: 2
Items: 
Size: 485 Color: 0
Size: 475 Color: 2

Bin 4417: 41 of cap free
Amount of items: 2
Items: 
Size: 485 Color: 0
Size: 475 Color: 3

Bin 4418: 43 of cap free
Amount of items: 4
Items: 
Size: 412 Color: 0
Size: 182 Color: 1
Size: 182 Color: 0
Size: 182 Color: 1

Bin 4419: 43 of cap free
Amount of items: 4
Items: 
Size: 412 Color: 0
Size: 182 Color: 1
Size: 182 Color: 2
Size: 182 Color: 1

Bin 4420: 43 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 0
Size: 475 Color: 1

Bin 4421: 43 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 0
Size: 475 Color: 3

Bin 4422: 43 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 0
Size: 475 Color: 1

Bin 4423: 43 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 0
Size: 475 Color: 3

Bin 4424: 52 of cap free
Amount of items: 4
Items: 
Size: 412 Color: 0
Size: 179 Color: 2
Size: 179 Color: 1
Size: 179 Color: 1

Bin 4425: 52 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 0
Size: 466 Color: 1

Bin 4426: 53 of cap free
Amount of items: 2
Items: 
Size: 482 Color: 0
Size: 466 Color: 1

Bin 4427: 53 of cap free
Amount of items: 2
Items: 
Size: 482 Color: 0
Size: 466 Color: 1

Bin 4428: 53 of cap free
Amount of items: 2
Items: 
Size: 482 Color: 0
Size: 466 Color: 1

Bin 4429: 56 of cap free
Amount of items: 2
Items: 
Size: 480 Color: 0
Size: 465 Color: 1

Bin 4430: 57 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 0
Size: 465 Color: 1

Bin 4431: 57 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 0
Size: 465 Color: 1

Bin 4432: 65 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 0
Size: 457 Color: 3

Bin 4433: 66 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 0
Size: 457 Color: 1

Bin 4434: 235 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 179 Color: 1
Size: 179 Color: 1

Bin 4435: 596 of cap free
Amount of items: 1
Items: 
Size: 405 Color: 0

Bin 4436: 596 of cap free
Amount of items: 1
Items: 
Size: 405 Color: 0

Bin 4437: 598 of cap free
Amount of items: 1
Items: 
Size: 403 Color: 0

Bin 4438: 601 of cap free
Amount of items: 1
Items: 
Size: 400 Color: 0

Bin 4439: 601 of cap free
Amount of items: 1
Items: 
Size: 400 Color: 0

Bin 4440: 601 of cap free
Amount of items: 1
Items: 
Size: 400 Color: 0

Bin 4441: 601 of cap free
Amount of items: 1
Items: 
Size: 400 Color: 0

Bin 4442: 605 of cap free
Amount of items: 1
Items: 
Size: 396 Color: 0

Bin 4443: 608 of cap free
Amount of items: 1
Items: 
Size: 393 Color: 0

Bin 4444: 608 of cap free
Amount of items: 1
Items: 
Size: 393 Color: 0

Bin 4445: 608 of cap free
Amount of items: 1
Items: 
Size: 393 Color: 0

Bin 4446: 609 of cap free
Amount of items: 1
Items: 
Size: 392 Color: 0

Bin 4447: 610 of cap free
Amount of items: 1
Items: 
Size: 391 Color: 0

Bin 4448: 610 of cap free
Amount of items: 1
Items: 
Size: 391 Color: 0

Bin 4449: 611 of cap free
Amount of items: 1
Items: 
Size: 390 Color: 0

Bin 4450: 612 of cap free
Amount of items: 1
Items: 
Size: 389 Color: 0

Bin 4451: 612 of cap free
Amount of items: 1
Items: 
Size: 389 Color: 0

Bin 4452: 615 of cap free
Amount of items: 1
Items: 
Size: 386 Color: 0

Bin 4453: 615 of cap free
Amount of items: 1
Items: 
Size: 386 Color: 0

Bin 4454: 615 of cap free
Amount of items: 1
Items: 
Size: 386 Color: 0

Bin 4455: 616 of cap free
Amount of items: 1
Items: 
Size: 385 Color: 0

Bin 4456: 632 of cap free
Amount of items: 1
Items: 
Size: 369 Color: 0

Bin 4457: 632 of cap free
Amount of items: 1
Items: 
Size: 369 Color: 0

Bin 4458: 634 of cap free
Amount of items: 1
Items: 
Size: 367 Color: 0

Bin 4459: 634 of cap free
Amount of items: 1
Items: 
Size: 367 Color: 0

Bin 4460: 634 of cap free
Amount of items: 1
Items: 
Size: 367 Color: 0

Bin 4461: 637 of cap free
Amount of items: 1
Items: 
Size: 364 Color: 0

Bin 4462: 638 of cap free
Amount of items: 1
Items: 
Size: 363 Color: 0

Bin 4463: 642 of cap free
Amount of items: 1
Items: 
Size: 359 Color: 0

Bin 4464: 642 of cap free
Amount of items: 1
Items: 
Size: 359 Color: 0

Bin 4465: 642 of cap free
Amount of items: 1
Items: 
Size: 359 Color: 0

Bin 4466: 642 of cap free
Amount of items: 1
Items: 
Size: 359 Color: 0

Bin 4467: 643 of cap free
Amount of items: 1
Items: 
Size: 358 Color: 0

Bin 4468: 643 of cap free
Amount of items: 1
Items: 
Size: 358 Color: 0

Bin 4469: 643 of cap free
Amount of items: 1
Items: 
Size: 358 Color: 0

Bin 4470: 643 of cap free
Amount of items: 1
Items: 
Size: 358 Color: 0

Bin 4471: 644 of cap free
Amount of items: 1
Items: 
Size: 357 Color: 0

Bin 4472: 644 of cap free
Amount of items: 1
Items: 
Size: 357 Color: 0

Bin 4473: 644 of cap free
Amount of items: 1
Items: 
Size: 357 Color: 0

Bin 4474: 644 of cap free
Amount of items: 1
Items: 
Size: 357 Color: 0

Bin 4475: 646 of cap free
Amount of items: 1
Items: 
Size: 355 Color: 0

Bin 4476: 647 of cap free
Amount of items: 1
Items: 
Size: 354 Color: 0

Bin 4477: 647 of cap free
Amount of items: 1
Items: 
Size: 354 Color: 0

Bin 4478: 647 of cap free
Amount of items: 1
Items: 
Size: 354 Color: 0

Bin 4479: 647 of cap free
Amount of items: 1
Items: 
Size: 354 Color: 0

Bin 4480: 648 of cap free
Amount of items: 1
Items: 
Size: 353 Color: 0

Bin 4481: 648 of cap free
Amount of items: 1
Items: 
Size: 353 Color: 0

Bin 4482: 648 of cap free
Amount of items: 1
Items: 
Size: 353 Color: 0

Bin 4483: 651 of cap free
Amount of items: 1
Items: 
Size: 350 Color: 0

Bin 4484: 651 of cap free
Amount of items: 1
Items: 
Size: 350 Color: 0

Bin 4485: 651 of cap free
Amount of items: 1
Items: 
Size: 350 Color: 0

Bin 4486: 652 of cap free
Amount of items: 1
Items: 
Size: 349 Color: 0

Bin 4487: 652 of cap free
Amount of items: 1
Items: 
Size: 349 Color: 0

Bin 4488: 652 of cap free
Amount of items: 1
Items: 
Size: 349 Color: 0

Bin 4489: 653 of cap free
Amount of items: 1
Items: 
Size: 348 Color: 0

Bin 4490: 653 of cap free
Amount of items: 1
Items: 
Size: 348 Color: 0

Bin 4491: 654 of cap free
Amount of items: 1
Items: 
Size: 347 Color: 0

Bin 4492: 654 of cap free
Amount of items: 1
Items: 
Size: 347 Color: 0

Bin 4493: 654 of cap free
Amount of items: 1
Items: 
Size: 347 Color: 0

Bin 4494: 655 of cap free
Amount of items: 1
Items: 
Size: 346 Color: 0

Bin 4495: 655 of cap free
Amount of items: 1
Items: 
Size: 346 Color: 0

Bin 4496: 656 of cap free
Amount of items: 1
Items: 
Size: 345 Color: 0

Bin 4497: 656 of cap free
Amount of items: 1
Items: 
Size: 345 Color: 0

Bin 4498: 658 of cap free
Amount of items: 1
Items: 
Size: 343 Color: 0

Bin 4499: 658 of cap free
Amount of items: 1
Items: 
Size: 343 Color: 0

Bin 4500: 658 of cap free
Amount of items: 1
Items: 
Size: 343 Color: 0

Bin 4501: 662 of cap free
Amount of items: 1
Items: 
Size: 339 Color: 0

Bin 4502: 664 of cap free
Amount of items: 1
Items: 
Size: 337 Color: 0

Bin 4503: 664 of cap free
Amount of items: 1
Items: 
Size: 337 Color: 0

Bin 4504: 664 of cap free
Amount of items: 1
Items: 
Size: 337 Color: 0

Bin 4505: 666 of cap free
Amount of items: 1
Items: 
Size: 335 Color: 0

Bin 4506: 666 of cap free
Amount of items: 1
Items: 
Size: 335 Color: 0

Bin 4507: 666 of cap free
Amount of items: 1
Items: 
Size: 335 Color: 0

Bin 4508: 666 of cap free
Amount of items: 1
Items: 
Size: 335 Color: 0

Bin 4509: 669 of cap free
Amount of items: 1
Items: 
Size: 332 Color: 0

Bin 4510: 669 of cap free
Amount of items: 1
Items: 
Size: 332 Color: 0

Bin 4511: 671 of cap free
Amount of items: 1
Items: 
Size: 330 Color: 0

Bin 4512: 671 of cap free
Amount of items: 1
Items: 
Size: 330 Color: 0

Bin 4513: 676 of cap free
Amount of items: 1
Items: 
Size: 325 Color: 0

Bin 4514: 676 of cap free
Amount of items: 1
Items: 
Size: 325 Color: 0

Bin 4515: 679 of cap free
Amount of items: 1
Items: 
Size: 322 Color: 0

Bin 4516: 680 of cap free
Amount of items: 1
Items: 
Size: 321 Color: 0

Bin 4517: 680 of cap free
Amount of items: 1
Items: 
Size: 321 Color: 0

Bin 4518: 680 of cap free
Amount of items: 1
Items: 
Size: 321 Color: 0

Bin 4519: 682 of cap free
Amount of items: 1
Items: 
Size: 319 Color: 0

Bin 4520: 682 of cap free
Amount of items: 1
Items: 
Size: 319 Color: 0

Bin 4521: 729 of cap free
Amount of items: 1
Items: 
Size: 272 Color: 0

Bin 4522: 731 of cap free
Amount of items: 1
Items: 
Size: 270 Color: 0

Bin 4523: 737 of cap free
Amount of items: 1
Items: 
Size: 264 Color: 0

Bin 4524: 737 of cap free
Amount of items: 1
Items: 
Size: 264 Color: 0

Bin 4525: 737 of cap free
Amount of items: 1
Items: 
Size: 264 Color: 0

Bin 4526: 739 of cap free
Amount of items: 1
Items: 
Size: 262 Color: 0

Bin 4527: 741 of cap free
Amount of items: 1
Items: 
Size: 260 Color: 0

Bin 4528: 744 of cap free
Amount of items: 1
Items: 
Size: 257 Color: 0

Bin 4529: 745 of cap free
Amount of items: 1
Items: 
Size: 256 Color: 0

Bin 4530: 745 of cap free
Amount of items: 1
Items: 
Size: 256 Color: 0

Bin 4531: 745 of cap free
Amount of items: 1
Items: 
Size: 256 Color: 0

Bin 4532: 745 of cap free
Amount of items: 1
Items: 
Size: 256 Color: 0

Bin 4533: 746 of cap free
Amount of items: 1
Items: 
Size: 255 Color: 0

Bin 4534: 746 of cap free
Amount of items: 1
Items: 
Size: 255 Color: 0

Bin 4535: 746 of cap free
Amount of items: 1
Items: 
Size: 255 Color: 0

Bin 4536: 746 of cap free
Amount of items: 1
Items: 
Size: 255 Color: 0

Bin 4537: 747 of cap free
Amount of items: 1
Items: 
Size: 254 Color: 0

Bin 4538: 747 of cap free
Amount of items: 1
Items: 
Size: 254 Color: 0

Bin 4539: 758 of cap free
Amount of items: 1
Items: 
Size: 243 Color: 0

Bin 4540: 758 of cap free
Amount of items: 1
Items: 
Size: 243 Color: 0

Bin 4541: 758 of cap free
Amount of items: 1
Items: 
Size: 243 Color: 0

Bin 4542: 758 of cap free
Amount of items: 1
Items: 
Size: 243 Color: 0

Bin 4543: 758 of cap free
Amount of items: 1
Items: 
Size: 243 Color: 0

Bin 4544: 761 of cap free
Amount of items: 1
Items: 
Size: 240 Color: 0

Bin 4545: 764 of cap free
Amount of items: 1
Items: 
Size: 237 Color: 0

Bin 4546: 764 of cap free
Amount of items: 1
Items: 
Size: 237 Color: 0

Bin 4547: 770 of cap free
Amount of items: 1
Items: 
Size: 231 Color: 0

Bin 4548: 771 of cap free
Amount of items: 1
Items: 
Size: 230 Color: 0

Bin 4549: 771 of cap free
Amount of items: 1
Items: 
Size: 230 Color: 0

Bin 4550: 771 of cap free
Amount of items: 1
Items: 
Size: 230 Color: 0

Bin 4551: 771 of cap free
Amount of items: 1
Items: 
Size: 230 Color: 0

Bin 4552: 771 of cap free
Amount of items: 1
Items: 
Size: 230 Color: 0

Bin 4553: 775 of cap free
Amount of items: 1
Items: 
Size: 226 Color: 0

Bin 4554: 778 of cap free
Amount of items: 1
Items: 
Size: 223 Color: 0

Bin 4555: 778 of cap free
Amount of items: 1
Items: 
Size: 223 Color: 0

Bin 4556: 782 of cap free
Amount of items: 1
Items: 
Size: 219 Color: 0

Bin 4557: 783 of cap free
Amount of items: 1
Items: 
Size: 218 Color: 0

Bin 4558: 783 of cap free
Amount of items: 1
Items: 
Size: 218 Color: 0

Bin 4559: 783 of cap free
Amount of items: 1
Items: 
Size: 218 Color: 0

Bin 4560: 783 of cap free
Amount of items: 1
Items: 
Size: 218 Color: 0

Bin 4561: 783 of cap free
Amount of items: 1
Items: 
Size: 218 Color: 0

Bin 4562: 785 of cap free
Amount of items: 1
Items: 
Size: 216 Color: 0

Bin 4563: 785 of cap free
Amount of items: 1
Items: 
Size: 216 Color: 0

Bin 4564: 791 of cap free
Amount of items: 1
Items: 
Size: 210 Color: 0

Bin 4565: 791 of cap free
Amount of items: 1
Items: 
Size: 210 Color: 0

Bin 4566: 791 of cap free
Amount of items: 1
Items: 
Size: 210 Color: 0

Bin 4567: 794 of cap free
Amount of items: 1
Items: 
Size: 207 Color: 0

Bin 4568: 794 of cap free
Amount of items: 1
Items: 
Size: 207 Color: 0

Bin 4569: 795 of cap free
Amount of items: 1
Items: 
Size: 206 Color: 0

Bin 4570: 795 of cap free
Amount of items: 1
Items: 
Size: 206 Color: 0

Bin 4571: 795 of cap free
Amount of items: 1
Items: 
Size: 206 Color: 0

Bin 4572: 795 of cap free
Amount of items: 1
Items: 
Size: 206 Color: 0

Bin 4573: 796 of cap free
Amount of items: 1
Items: 
Size: 205 Color: 0

Bin 4574: 796 of cap free
Amount of items: 1
Items: 
Size: 205 Color: 0

Bin 4575: 797 of cap free
Amount of items: 1
Items: 
Size: 204 Color: 0

Bin 4576: 797 of cap free
Amount of items: 1
Items: 
Size: 204 Color: 0

Bin 4577: 797 of cap free
Amount of items: 1
Items: 
Size: 204 Color: 0

Bin 4578: 798 of cap free
Amount of items: 1
Items: 
Size: 203 Color: 0

Bin 4579: 799 of cap free
Amount of items: 1
Items: 
Size: 202 Color: 0

Bin 4580: 799 of cap free
Amount of items: 1
Items: 
Size: 202 Color: 0

Bin 4581: 799 of cap free
Amount of items: 1
Items: 
Size: 202 Color: 0

Bin 4582: 803 of cap free
Amount of items: 1
Items: 
Size: 198 Color: 0

Bin 4583: 803 of cap free
Amount of items: 1
Items: 
Size: 198 Color: 0

Bin 4584: 804 of cap free
Amount of items: 1
Items: 
Size: 197 Color: 0

Bin 4585: 804 of cap free
Amount of items: 1
Items: 
Size: 197 Color: 0

Bin 4586: 804 of cap free
Amount of items: 1
Items: 
Size: 197 Color: 0

Bin 4587: 806 of cap free
Amount of items: 1
Items: 
Size: 195 Color: 0

Bin 4588: 806 of cap free
Amount of items: 1
Items: 
Size: 195 Color: 0

Bin 4589: 807 of cap free
Amount of items: 1
Items: 
Size: 194 Color: 0

Bin 4590: 807 of cap free
Amount of items: 1
Items: 
Size: 194 Color: 0

Bin 4591: 807 of cap free
Amount of items: 1
Items: 
Size: 194 Color: 0

Bin 4592: 807 of cap free
Amount of items: 1
Items: 
Size: 194 Color: 0

Bin 4593: 808 of cap free
Amount of items: 1
Items: 
Size: 193 Color: 0

Bin 4594: 808 of cap free
Amount of items: 1
Items: 
Size: 193 Color: 0

Bin 4595: 808 of cap free
Amount of items: 1
Items: 
Size: 193 Color: 0

Bin 4596: 808 of cap free
Amount of items: 1
Items: 
Size: 193 Color: 0

Bin 4597: 808 of cap free
Amount of items: 1
Items: 
Size: 193 Color: 0

Bin 4598: 809 of cap free
Amount of items: 1
Items: 
Size: 192 Color: 0

Bin 4599: 809 of cap free
Amount of items: 1
Items: 
Size: 192 Color: 0

Bin 4600: 809 of cap free
Amount of items: 1
Items: 
Size: 192 Color: 0

Bin 4601: 810 of cap free
Amount of items: 1
Items: 
Size: 191 Color: 0

Bin 4602: 810 of cap free
Amount of items: 1
Items: 
Size: 191 Color: 0

Bin 4603: 810 of cap free
Amount of items: 1
Items: 
Size: 191 Color: 0

Bin 4604: 813 of cap free
Amount of items: 1
Items: 
Size: 188 Color: 0

Bin 4605: 813 of cap free
Amount of items: 1
Items: 
Size: 188 Color: 0

Bin 4606: 815 of cap free
Amount of items: 1
Items: 
Size: 186 Color: 0

Bin 4607: 815 of cap free
Amount of items: 1
Items: 
Size: 186 Color: 0

Bin 4608: 817 of cap free
Amount of items: 1
Items: 
Size: 184 Color: 0

Bin 4609: 817 of cap free
Amount of items: 1
Items: 
Size: 184 Color: 0

Bin 4610: 817 of cap free
Amount of items: 1
Items: 
Size: 184 Color: 0

Bin 4611: 818 of cap free
Amount of items: 1
Items: 
Size: 183 Color: 0

Bin 4612: 818 of cap free
Amount of items: 1
Items: 
Size: 183 Color: 0

Bin 4613: 818 of cap free
Amount of items: 1
Items: 
Size: 183 Color: 0

Bin 4614: 818 of cap free
Amount of items: 1
Items: 
Size: 183 Color: 0

Bin 4615: 818 of cap free
Amount of items: 1
Items: 
Size: 183 Color: 0

Bin 4616: 818 of cap free
Amount of items: 1
Items: 
Size: 183 Color: 0

Bin 4617: 819 of cap free
Amount of items: 1
Items: 
Size: 182 Color: 0

Bin 4618: 820 of cap free
Amount of items: 1
Items: 
Size: 181 Color: 0

Total size: 4488589
Total free space: 134029

