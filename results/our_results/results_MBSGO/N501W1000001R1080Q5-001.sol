Capicity Bin: 1000001
Lower Bound: 227

Bins used: 229
Amount of Colors: 5

Bin 1: 13 of cap free
Amount of items: 2
Items: 
Size: 645616 Color: 2
Size: 354372 Color: 4

Bin 2: 23 of cap free
Amount of items: 2
Items: 
Size: 699276 Color: 0
Size: 300702 Color: 4

Bin 3: 54 of cap free
Amount of items: 2
Items: 
Size: 634001 Color: 3
Size: 365946 Color: 1

Bin 4: 60 of cap free
Amount of items: 2
Items: 
Size: 554445 Color: 4
Size: 445496 Color: 1

Bin 5: 74 of cap free
Amount of items: 3
Items: 
Size: 670079 Color: 1
Size: 170001 Color: 4
Size: 159847 Color: 2

Bin 6: 83 of cap free
Amount of items: 2
Items: 
Size: 518629 Color: 0
Size: 481289 Color: 2

Bin 7: 90 of cap free
Amount of items: 2
Items: 
Size: 533738 Color: 1
Size: 466173 Color: 3

Bin 8: 91 of cap free
Amount of items: 2
Items: 
Size: 643670 Color: 1
Size: 356240 Color: 3

Bin 9: 107 of cap free
Amount of items: 3
Items: 
Size: 774705 Color: 4
Size: 114933 Color: 2
Size: 110256 Color: 2

Bin 10: 112 of cap free
Amount of items: 2
Items: 
Size: 589839 Color: 3
Size: 410050 Color: 1

Bin 11: 119 of cap free
Amount of items: 3
Items: 
Size: 731705 Color: 1
Size: 134665 Color: 2
Size: 133512 Color: 1

Bin 12: 122 of cap free
Amount of items: 2
Items: 
Size: 738388 Color: 0
Size: 261491 Color: 3

Bin 13: 141 of cap free
Amount of items: 3
Items: 
Size: 676900 Color: 2
Size: 167278 Color: 0
Size: 155682 Color: 3

Bin 14: 145 of cap free
Amount of items: 3
Items: 
Size: 652688 Color: 1
Size: 175921 Color: 3
Size: 171247 Color: 2

Bin 15: 156 of cap free
Amount of items: 2
Items: 
Size: 711778 Color: 3
Size: 288067 Color: 1

Bin 16: 176 of cap free
Amount of items: 2
Items: 
Size: 532279 Color: 0
Size: 467546 Color: 3

Bin 17: 176 of cap free
Amount of items: 3
Items: 
Size: 646120 Color: 4
Size: 177233 Color: 0
Size: 176472 Color: 3

Bin 18: 176 of cap free
Amount of items: 3
Items: 
Size: 723980 Color: 4
Size: 138220 Color: 3
Size: 137625 Color: 3

Bin 19: 198 of cap free
Amount of items: 3
Items: 
Size: 675189 Color: 4
Size: 166605 Color: 1
Size: 158009 Color: 3

Bin 20: 201 of cap free
Amount of items: 2
Items: 
Size: 558564 Color: 3
Size: 441236 Color: 0

Bin 21: 205 of cap free
Amount of items: 2
Items: 
Size: 752736 Color: 0
Size: 247060 Color: 1

Bin 22: 213 of cap free
Amount of items: 2
Items: 
Size: 699458 Color: 1
Size: 300330 Color: 4

Bin 23: 216 of cap free
Amount of items: 2
Items: 
Size: 574593 Color: 4
Size: 425192 Color: 1

Bin 24: 231 of cap free
Amount of items: 3
Items: 
Size: 722143 Color: 2
Size: 141315 Color: 4
Size: 136312 Color: 2

Bin 25: 242 of cap free
Amount of items: 2
Items: 
Size: 612608 Color: 3
Size: 387151 Color: 2

Bin 26: 247 of cap free
Amount of items: 2
Items: 
Size: 726100 Color: 1
Size: 273654 Color: 0

Bin 27: 253 of cap free
Amount of items: 3
Items: 
Size: 711706 Color: 2
Size: 144472 Color: 0
Size: 143570 Color: 4

Bin 28: 255 of cap free
Amount of items: 2
Items: 
Size: 736748 Color: 0
Size: 262998 Color: 4

Bin 29: 281 of cap free
Amount of items: 2
Items: 
Size: 556820 Color: 1
Size: 442900 Color: 2

Bin 30: 287 of cap free
Amount of items: 3
Items: 
Size: 767390 Color: 2
Size: 116954 Color: 4
Size: 115370 Color: 4

Bin 31: 291 of cap free
Amount of items: 3
Items: 
Size: 706136 Color: 4
Size: 149190 Color: 2
Size: 144384 Color: 4

Bin 32: 299 of cap free
Amount of items: 3
Items: 
Size: 672948 Color: 0
Size: 171598 Color: 2
Size: 155156 Color: 1

Bin 33: 303 of cap free
Amount of items: 2
Items: 
Size: 746824 Color: 4
Size: 252874 Color: 2

Bin 34: 304 of cap free
Amount of items: 3
Items: 
Size: 742651 Color: 2
Size: 132180 Color: 1
Size: 124866 Color: 2

Bin 35: 327 of cap free
Amount of items: 3
Items: 
Size: 784352 Color: 0
Size: 108376 Color: 4
Size: 106946 Color: 0

Bin 36: 350 of cap free
Amount of items: 2
Items: 
Size: 796859 Color: 4
Size: 202792 Color: 1

Bin 37: 354 of cap free
Amount of items: 3
Items: 
Size: 675704 Color: 2
Size: 168337 Color: 0
Size: 155606 Color: 1

Bin 38: 365 of cap free
Amount of items: 2
Items: 
Size: 609153 Color: 4
Size: 390483 Color: 0

Bin 39: 371 of cap free
Amount of items: 3
Items: 
Size: 641912 Color: 1
Size: 180563 Color: 3
Size: 177155 Color: 0

Bin 40: 375 of cap free
Amount of items: 3
Items: 
Size: 782863 Color: 2
Size: 109972 Color: 0
Size: 106791 Color: 2

Bin 41: 380 of cap free
Amount of items: 2
Items: 
Size: 641411 Color: 4
Size: 358210 Color: 0

Bin 42: 402 of cap free
Amount of items: 2
Items: 
Size: 569423 Color: 4
Size: 430176 Color: 2

Bin 43: 407 of cap free
Amount of items: 3
Items: 
Size: 723138 Color: 4
Size: 141480 Color: 0
Size: 134976 Color: 3

Bin 44: 434 of cap free
Amount of items: 2
Items: 
Size: 504542 Color: 4
Size: 495025 Color: 1

Bin 45: 439 of cap free
Amount of items: 2
Items: 
Size: 739414 Color: 3
Size: 260148 Color: 4

Bin 46: 440 of cap free
Amount of items: 2
Items: 
Size: 643500 Color: 4
Size: 356061 Color: 0

Bin 47: 440 of cap free
Amount of items: 2
Items: 
Size: 765931 Color: 2
Size: 233630 Color: 3

Bin 48: 464 of cap free
Amount of items: 2
Items: 
Size: 718527 Color: 1
Size: 281010 Color: 2

Bin 49: 483 of cap free
Amount of items: 2
Items: 
Size: 613474 Color: 1
Size: 386044 Color: 4

Bin 50: 486 of cap free
Amount of items: 2
Items: 
Size: 691725 Color: 1
Size: 307790 Color: 3

Bin 51: 487 of cap free
Amount of items: 2
Items: 
Size: 542353 Color: 1
Size: 457161 Color: 3

Bin 52: 495 of cap free
Amount of items: 2
Items: 
Size: 597672 Color: 3
Size: 401834 Color: 1

Bin 53: 501 of cap free
Amount of items: 2
Items: 
Size: 527825 Color: 3
Size: 471675 Color: 0

Bin 54: 501 of cap free
Amount of items: 2
Items: 
Size: 662865 Color: 0
Size: 336635 Color: 3

Bin 55: 514 of cap free
Amount of items: 2
Items: 
Size: 529442 Color: 4
Size: 470045 Color: 2

Bin 56: 515 of cap free
Amount of items: 2
Items: 
Size: 616226 Color: 3
Size: 383260 Color: 1

Bin 57: 520 of cap free
Amount of items: 2
Items: 
Size: 772597 Color: 3
Size: 226884 Color: 2

Bin 58: 522 of cap free
Amount of items: 2
Items: 
Size: 596659 Color: 4
Size: 402820 Color: 0

Bin 59: 531 of cap free
Amount of items: 2
Items: 
Size: 728101 Color: 1
Size: 271369 Color: 2

Bin 60: 536 of cap free
Amount of items: 3
Items: 
Size: 759929 Color: 3
Size: 125397 Color: 0
Size: 114139 Color: 3

Bin 61: 542 of cap free
Amount of items: 2
Items: 
Size: 517954 Color: 2
Size: 481505 Color: 0

Bin 62: 549 of cap free
Amount of items: 3
Items: 
Size: 704371 Color: 0
Size: 150804 Color: 3
Size: 144277 Color: 2

Bin 63: 553 of cap free
Amount of items: 2
Items: 
Size: 683461 Color: 1
Size: 315987 Color: 2

Bin 64: 562 of cap free
Amount of items: 2
Items: 
Size: 534429 Color: 0
Size: 465010 Color: 2

Bin 65: 565 of cap free
Amount of items: 2
Items: 
Size: 563710 Color: 1
Size: 435726 Color: 4

Bin 66: 587 of cap free
Amount of items: 2
Items: 
Size: 633347 Color: 4
Size: 366067 Color: 3

Bin 67: 588 of cap free
Amount of items: 2
Items: 
Size: 514721 Color: 3
Size: 484692 Color: 0

Bin 68: 588 of cap free
Amount of items: 2
Items: 
Size: 735444 Color: 1
Size: 263969 Color: 4

Bin 69: 595 of cap free
Amount of items: 2
Items: 
Size: 780658 Color: 1
Size: 218748 Color: 3

Bin 70: 597 of cap free
Amount of items: 2
Items: 
Size: 638146 Color: 1
Size: 361258 Color: 3

Bin 71: 616 of cap free
Amount of items: 2
Items: 
Size: 694995 Color: 1
Size: 304390 Color: 3

Bin 72: 632 of cap free
Amount of items: 2
Items: 
Size: 790427 Color: 4
Size: 208942 Color: 3

Bin 73: 650 of cap free
Amount of items: 2
Items: 
Size: 765011 Color: 2
Size: 234340 Color: 1

Bin 74: 669 of cap free
Amount of items: 3
Items: 
Size: 762913 Color: 3
Size: 120179 Color: 1
Size: 116240 Color: 3

Bin 75: 696 of cap free
Amount of items: 3
Items: 
Size: 689424 Color: 4
Size: 156025 Color: 2
Size: 153856 Color: 3

Bin 76: 698 of cap free
Amount of items: 3
Items: 
Size: 393235 Color: 3
Size: 317449 Color: 1
Size: 288619 Color: 1

Bin 77: 719 of cap free
Amount of items: 2
Items: 
Size: 665993 Color: 0
Size: 333289 Color: 3

Bin 78: 733 of cap free
Amount of items: 3
Items: 
Size: 664596 Color: 2
Size: 170515 Color: 1
Size: 164157 Color: 0

Bin 79: 738 of cap free
Amount of items: 2
Items: 
Size: 628750 Color: 2
Size: 370513 Color: 4

Bin 80: 754 of cap free
Amount of items: 2
Items: 
Size: 704202 Color: 4
Size: 295045 Color: 0

Bin 81: 779 of cap free
Amount of items: 3
Items: 
Size: 755056 Color: 2
Size: 122233 Color: 4
Size: 121933 Color: 4

Bin 82: 795 of cap free
Amount of items: 2
Items: 
Size: 613289 Color: 0
Size: 385917 Color: 3

Bin 83: 799 of cap free
Amount of items: 2
Items: 
Size: 582606 Color: 0
Size: 416596 Color: 3

Bin 84: 804 of cap free
Amount of items: 2
Items: 
Size: 602587 Color: 4
Size: 396610 Color: 3

Bin 85: 809 of cap free
Amount of items: 2
Items: 
Size: 787701 Color: 0
Size: 211491 Color: 2

Bin 86: 833 of cap free
Amount of items: 2
Items: 
Size: 743954 Color: 1
Size: 255214 Color: 0

Bin 87: 841 of cap free
Amount of items: 2
Items: 
Size: 542059 Color: 1
Size: 457101 Color: 4

Bin 88: 864 of cap free
Amount of items: 2
Items: 
Size: 525106 Color: 2
Size: 474031 Color: 4

Bin 89: 868 of cap free
Amount of items: 2
Items: 
Size: 750724 Color: 2
Size: 248409 Color: 3

Bin 90: 876 of cap free
Amount of items: 3
Items: 
Size: 714098 Color: 3
Size: 145072 Color: 0
Size: 139955 Color: 3

Bin 91: 880 of cap free
Amount of items: 2
Items: 
Size: 589368 Color: 2
Size: 409753 Color: 4

Bin 92: 880 of cap free
Amount of items: 3
Items: 
Size: 773357 Color: 0
Size: 114149 Color: 4
Size: 111615 Color: 2

Bin 93: 886 of cap free
Amount of items: 2
Items: 
Size: 571639 Color: 3
Size: 427476 Color: 0

Bin 94: 899 of cap free
Amount of items: 2
Items: 
Size: 599582 Color: 2
Size: 399520 Color: 4

Bin 95: 900 of cap free
Amount of items: 2
Items: 
Size: 516057 Color: 4
Size: 483044 Color: 0

Bin 96: 912 of cap free
Amount of items: 2
Items: 
Size: 549844 Color: 2
Size: 449245 Color: 0

Bin 97: 930 of cap free
Amount of items: 2
Items: 
Size: 518443 Color: 1
Size: 480628 Color: 3

Bin 98: 930 of cap free
Amount of items: 3
Items: 
Size: 531177 Color: 1
Size: 287578 Color: 4
Size: 180316 Color: 3

Bin 99: 939 of cap free
Amount of items: 2
Items: 
Size: 547530 Color: 1
Size: 451532 Color: 3

Bin 100: 951 of cap free
Amount of items: 2
Items: 
Size: 712678 Color: 1
Size: 286372 Color: 4

Bin 101: 981 of cap free
Amount of items: 3
Items: 
Size: 699147 Color: 3
Size: 152913 Color: 0
Size: 146960 Color: 4

Bin 102: 983 of cap free
Amount of items: 2
Items: 
Size: 500259 Color: 0
Size: 498759 Color: 1

Bin 103: 1028 of cap free
Amount of items: 2
Items: 
Size: 741402 Color: 1
Size: 257571 Color: 3

Bin 104: 1057 of cap free
Amount of items: 3
Items: 
Size: 703017 Color: 3
Size: 150647 Color: 0
Size: 145280 Color: 3

Bin 105: 1097 of cap free
Amount of items: 2
Items: 
Size: 579272 Color: 1
Size: 419632 Color: 4

Bin 106: 1198 of cap free
Amount of items: 2
Items: 
Size: 754663 Color: 1
Size: 244140 Color: 0

Bin 107: 1228 of cap free
Amount of items: 2
Items: 
Size: 553653 Color: 2
Size: 445120 Color: 3

Bin 108: 1235 of cap free
Amount of items: 2
Items: 
Size: 611715 Color: 2
Size: 387051 Color: 3

Bin 109: 1236 of cap free
Amount of items: 2
Items: 
Size: 732920 Color: 2
Size: 265845 Color: 3

Bin 110: 1245 of cap free
Amount of items: 2
Items: 
Size: 694745 Color: 2
Size: 304011 Color: 3

Bin 111: 1282 of cap free
Amount of items: 3
Items: 
Size: 716987 Color: 2
Size: 141068 Color: 1
Size: 140664 Color: 1

Bin 112: 1305 of cap free
Amount of items: 3
Items: 
Size: 793478 Color: 3
Size: 102919 Color: 1
Size: 102299 Color: 4

Bin 113: 1342 of cap free
Amount of items: 2
Items: 
Size: 574365 Color: 0
Size: 424294 Color: 1

Bin 114: 1356 of cap free
Amount of items: 2
Items: 
Size: 504985 Color: 2
Size: 493660 Color: 0

Bin 115: 1358 of cap free
Amount of items: 3
Items: 
Size: 646993 Color: 0
Size: 177528 Color: 1
Size: 174122 Color: 4

Bin 116: 1365 of cap free
Amount of items: 2
Items: 
Size: 721572 Color: 4
Size: 277064 Color: 1

Bin 117: 1422 of cap free
Amount of items: 2
Items: 
Size: 520459 Color: 1
Size: 478120 Color: 4

Bin 118: 1440 of cap free
Amount of items: 2
Items: 
Size: 567076 Color: 3
Size: 431485 Color: 2

Bin 119: 1548 of cap free
Amount of items: 2
Items: 
Size: 502891 Color: 0
Size: 495562 Color: 1

Bin 120: 1568 of cap free
Amount of items: 2
Items: 
Size: 630303 Color: 2
Size: 368130 Color: 4

Bin 121: 1605 of cap free
Amount of items: 2
Items: 
Size: 691333 Color: 3
Size: 307063 Color: 1

Bin 122: 1613 of cap free
Amount of items: 2
Items: 
Size: 669534 Color: 3
Size: 328854 Color: 0

Bin 123: 1638 of cap free
Amount of items: 2
Items: 
Size: 627036 Color: 1
Size: 371327 Color: 3

Bin 124: 1644 of cap free
Amount of items: 2
Items: 
Size: 567707 Color: 4
Size: 430650 Color: 2

Bin 125: 1679 of cap free
Amount of items: 2
Items: 
Size: 527464 Color: 1
Size: 470858 Color: 2

Bin 126: 1722 of cap free
Amount of items: 2
Items: 
Size: 756106 Color: 0
Size: 242173 Color: 2

Bin 127: 1831 of cap free
Amount of items: 2
Items: 
Size: 600680 Color: 0
Size: 397490 Color: 2

Bin 128: 1869 of cap free
Amount of items: 3
Items: 
Size: 745921 Color: 2
Size: 126983 Color: 3
Size: 125228 Color: 1

Bin 129: 1890 of cap free
Amount of items: 3
Items: 
Size: 391212 Color: 1
Size: 318304 Color: 4
Size: 288595 Color: 0

Bin 130: 1943 of cap free
Amount of items: 2
Items: 
Size: 639139 Color: 1
Size: 358919 Color: 0

Bin 131: 1982 of cap free
Amount of items: 2
Items: 
Size: 517672 Color: 2
Size: 480347 Color: 0

Bin 132: 2031 of cap free
Amount of items: 2
Items: 
Size: 600508 Color: 0
Size: 397462 Color: 2

Bin 133: 2058 of cap free
Amount of items: 2
Items: 
Size: 591427 Color: 2
Size: 406516 Color: 0

Bin 134: 2120 of cap free
Amount of items: 2
Items: 
Size: 655379 Color: 2
Size: 342502 Color: 3

Bin 135: 2162 of cap free
Amount of items: 2
Items: 
Size: 594494 Color: 4
Size: 403345 Color: 2

Bin 136: 2225 of cap free
Amount of items: 2
Items: 
Size: 642655 Color: 2
Size: 355121 Color: 3

Bin 137: 2444 of cap free
Amount of items: 2
Items: 
Size: 790502 Color: 3
Size: 207055 Color: 1

Bin 138: 2464 of cap free
Amount of items: 2
Items: 
Size: 588117 Color: 3
Size: 409420 Color: 1

Bin 139: 2486 of cap free
Amount of items: 2
Items: 
Size: 541030 Color: 3
Size: 456485 Color: 0

Bin 140: 2496 of cap free
Amount of items: 2
Items: 
Size: 579318 Color: 0
Size: 418187 Color: 4

Bin 141: 2645 of cap free
Amount of items: 2
Items: 
Size: 614416 Color: 2
Size: 382940 Color: 1

Bin 142: 2804 of cap free
Amount of items: 2
Items: 
Size: 695039 Color: 3
Size: 302158 Color: 2

Bin 143: 2830 of cap free
Amount of items: 2
Items: 
Size: 668727 Color: 1
Size: 328444 Color: 2

Bin 144: 2909 of cap free
Amount of items: 2
Items: 
Size: 714898 Color: 1
Size: 282194 Color: 2

Bin 145: 3143 of cap free
Amount of items: 2
Items: 
Size: 772547 Color: 2
Size: 224311 Color: 1

Bin 146: 3163 of cap free
Amount of items: 2
Items: 
Size: 504472 Color: 4
Size: 492366 Color: 0

Bin 147: 3450 of cap free
Amount of items: 3
Items: 
Size: 792718 Color: 1
Size: 102147 Color: 0
Size: 101686 Color: 3

Bin 148: 3544 of cap free
Amount of items: 2
Items: 
Size: 673246 Color: 2
Size: 323211 Color: 3

Bin 149: 3733 of cap free
Amount of items: 2
Items: 
Size: 505916 Color: 0
Size: 490352 Color: 4

Bin 150: 3907 of cap free
Amount of items: 2
Items: 
Size: 534881 Color: 2
Size: 461213 Color: 3

Bin 151: 4024 of cap free
Amount of items: 2
Items: 
Size: 655322 Color: 3
Size: 340655 Color: 2

Bin 152: 4067 of cap free
Amount of items: 3
Items: 
Size: 380428 Color: 4
Size: 309230 Color: 0
Size: 306276 Color: 0

Bin 153: 4203 of cap free
Amount of items: 2
Items: 
Size: 672694 Color: 2
Size: 323104 Color: 4

Bin 154: 4401 of cap free
Amount of items: 2
Items: 
Size: 567034 Color: 3
Size: 428566 Color: 1

Bin 155: 4758 of cap free
Amount of items: 2
Items: 
Size: 775329 Color: 1
Size: 219914 Color: 0

Bin 156: 4839 of cap free
Amount of items: 2
Items: 
Size: 505255 Color: 0
Size: 489907 Color: 3

Bin 157: 4917 of cap free
Amount of items: 3
Items: 
Size: 394943 Color: 1
Size: 311459 Color: 0
Size: 288682 Color: 1

Bin 158: 4926 of cap free
Amount of items: 2
Items: 
Size: 629773 Color: 1
Size: 365302 Color: 3

Bin 159: 5251 of cap free
Amount of items: 2
Items: 
Size: 510800 Color: 3
Size: 483950 Color: 0

Bin 160: 5343 of cap free
Amount of items: 2
Items: 
Size: 525672 Color: 1
Size: 468986 Color: 4

Bin 161: 5364 of cap free
Amount of items: 2
Items: 
Size: 693465 Color: 0
Size: 301172 Color: 4

Bin 162: 5370 of cap free
Amount of items: 2
Items: 
Size: 549687 Color: 2
Size: 444944 Color: 0

Bin 163: 5508 of cap free
Amount of items: 2
Items: 
Size: 646519 Color: 2
Size: 347974 Color: 1

Bin 164: 5529 of cap free
Amount of items: 2
Items: 
Size: 598804 Color: 2
Size: 395668 Color: 3

Bin 165: 5612 of cap free
Amount of items: 2
Items: 
Size: 652481 Color: 1
Size: 341908 Color: 3

Bin 166: 5689 of cap free
Amount of items: 2
Items: 
Size: 789719 Color: 3
Size: 204593 Color: 1

Bin 167: 6359 of cap free
Amount of items: 2
Items: 
Size: 551423 Color: 0
Size: 442219 Color: 2

Bin 168: 6408 of cap free
Amount of items: 2
Items: 
Size: 799777 Color: 0
Size: 193816 Color: 1

Bin 169: 6476 of cap free
Amount of items: 2
Items: 
Size: 653165 Color: 3
Size: 340360 Color: 1

Bin 170: 6553 of cap free
Amount of items: 2
Items: 
Size: 653164 Color: 3
Size: 340284 Color: 2

Bin 171: 6886 of cap free
Amount of items: 2
Items: 
Size: 653125 Color: 3
Size: 339990 Color: 4

Bin 172: 7113 of cap free
Amount of items: 2
Items: 
Size: 759847 Color: 1
Size: 233041 Color: 2

Bin 173: 7186 of cap free
Amount of items: 2
Items: 
Size: 782133 Color: 1
Size: 210682 Color: 3

Bin 174: 7510 of cap free
Amount of items: 2
Items: 
Size: 700659 Color: 2
Size: 291832 Color: 1

Bin 175: 7528 of cap free
Amount of items: 2
Items: 
Size: 771555 Color: 0
Size: 220918 Color: 1

Bin 176: 7752 of cap free
Amount of items: 2
Items: 
Size: 641699 Color: 0
Size: 350550 Color: 2

Bin 177: 7939 of cap free
Amount of items: 2
Items: 
Size: 578152 Color: 0
Size: 413910 Color: 1

Bin 178: 8032 of cap free
Amount of items: 2
Items: 
Size: 577073 Color: 2
Size: 414896 Color: 0

Bin 179: 8154 of cap free
Amount of items: 2
Items: 
Size: 730120 Color: 3
Size: 261727 Color: 0

Bin 180: 8206 of cap free
Amount of items: 3
Items: 
Size: 362599 Color: 1
Size: 347613 Color: 2
Size: 281583 Color: 2

Bin 181: 8385 of cap free
Amount of items: 2
Items: 
Size: 741393 Color: 3
Size: 250223 Color: 1

Bin 182: 8842 of cap free
Amount of items: 2
Items: 
Size: 729405 Color: 4
Size: 261754 Color: 3

Bin 183: 9119 of cap free
Amount of items: 2
Items: 
Size: 550412 Color: 0
Size: 440470 Color: 4

Bin 184: 9302 of cap free
Amount of items: 2
Items: 
Size: 563545 Color: 0
Size: 427154 Color: 1

Bin 185: 10224 of cap free
Amount of items: 2
Items: 
Size: 788733 Color: 3
Size: 201044 Color: 1

Bin 186: 10483 of cap free
Amount of items: 2
Items: 
Size: 662008 Color: 4
Size: 327510 Color: 1

Bin 187: 10534 of cap free
Amount of items: 2
Items: 
Size: 759329 Color: 2
Size: 230138 Color: 3

Bin 188: 10634 of cap free
Amount of items: 2
Items: 
Size: 729699 Color: 3
Size: 259668 Color: 2

Bin 189: 10976 of cap free
Amount of items: 2
Items: 
Size: 699174 Color: 0
Size: 289851 Color: 2

Bin 190: 11116 of cap free
Amount of items: 2
Items: 
Size: 622950 Color: 4
Size: 365935 Color: 1

Bin 191: 11127 of cap free
Amount of items: 2
Items: 
Size: 549051 Color: 2
Size: 439823 Color: 3

Bin 192: 11181 of cap free
Amount of items: 2
Items: 
Size: 740866 Color: 1
Size: 247954 Color: 4

Bin 193: 11739 of cap free
Amount of items: 2
Items: 
Size: 759145 Color: 1
Size: 229117 Color: 0

Bin 194: 12343 of cap free
Amount of items: 2
Items: 
Size: 622204 Color: 4
Size: 365454 Color: 1

Bin 195: 12364 of cap free
Amount of items: 2
Items: 
Size: 510164 Color: 0
Size: 477473 Color: 2

Bin 196: 13236 of cap free
Amount of items: 3
Items: 
Size: 622574 Color: 1
Size: 182437 Color: 0
Size: 181754 Color: 0

Bin 197: 13980 of cap free
Amount of items: 2
Items: 
Size: 661188 Color: 4
Size: 324833 Color: 2

Bin 198: 14016 of cap free
Amount of items: 2
Items: 
Size: 739061 Color: 2
Size: 246924 Color: 0

Bin 199: 14106 of cap free
Amount of items: 2
Items: 
Size: 786684 Color: 3
Size: 199211 Color: 0

Bin 200: 14384 of cap free
Amount of items: 2
Items: 
Size: 548452 Color: 2
Size: 437165 Color: 0

Bin 201: 15463 of cap free
Amount of items: 2
Items: 
Size: 620598 Color: 0
Size: 363940 Color: 4

Bin 202: 17062 of cap free
Amount of items: 2
Items: 
Size: 619674 Color: 1
Size: 363265 Color: 2

Bin 203: 17951 of cap free
Amount of items: 2
Items: 
Size: 505159 Color: 0
Size: 476891 Color: 3

Bin 204: 20638 of cap free
Amount of items: 2
Items: 
Size: 503014 Color: 4
Size: 476349 Color: 1

Bin 205: 21551 of cap free
Amount of items: 2
Items: 
Size: 758627 Color: 1
Size: 219823 Color: 2

Bin 206: 21830 of cap free
Amount of items: 2
Items: 
Size: 502007 Color: 3
Size: 476164 Color: 2

Bin 207: 22700 of cap free
Amount of items: 2
Items: 
Size: 502207 Color: 4
Size: 475094 Color: 0

Bin 208: 23698 of cap free
Amount of items: 2
Items: 
Size: 781933 Color: 3
Size: 194370 Color: 0

Bin 209: 26215 of cap free
Amount of items: 2
Items: 
Size: 501428 Color: 2
Size: 472358 Color: 1

Bin 210: 28195 of cap free
Amount of items: 2
Items: 
Size: 563213 Color: 3
Size: 408593 Color: 1

Bin 211: 32519 of cap free
Amount of items: 2
Items: 
Size: 758888 Color: 2
Size: 208594 Color: 3

Bin 212: 35449 of cap free
Amount of items: 2
Items: 
Size: 641881 Color: 2
Size: 322671 Color: 4

Bin 213: 40088 of cap free
Amount of items: 2
Items: 
Size: 641166 Color: 2
Size: 318747 Color: 0

Bin 214: 42623 of cap free
Amount of items: 2
Items: 
Size: 638962 Color: 2
Size: 318416 Color: 4

Bin 215: 47935 of cap free
Amount of items: 2
Items: 
Size: 758011 Color: 3
Size: 194055 Color: 0

Bin 216: 60928 of cap free
Amount of items: 2
Items: 
Size: 738407 Color: 2
Size: 200666 Color: 3

Bin 217: 76174 of cap free
Amount of items: 2
Items: 
Size: 464438 Color: 2
Size: 459389 Color: 3

Bin 218: 76889 of cap free
Amount of items: 3
Items: 
Size: 361856 Color: 4
Size: 279702 Color: 1
Size: 281554 Color: 2

Bin 219: 77517 of cap free
Amount of items: 2
Items: 
Size: 729323 Color: 1
Size: 193161 Color: 2

Bin 220: 82596 of cap free
Amount of items: 2
Items: 
Size: 727943 Color: 1
Size: 189462 Color: 3

Bin 221: 84020 of cap free
Amount of items: 2
Items: 
Size: 727124 Color: 1
Size: 188857 Color: 3

Bin 222: 84173 of cap free
Amount of items: 2
Items: 
Size: 459105 Color: 4
Size: 456723 Color: 3

Bin 223: 89710 of cap free
Amount of items: 2
Items: 
Size: 455727 Color: 0
Size: 454564 Color: 1

Bin 224: 90830 of cap free
Amount of items: 2
Items: 
Size: 455210 Color: 0
Size: 453961 Color: 4

Bin 225: 92262 of cap free
Amount of items: 2
Items: 
Size: 454424 Color: 0
Size: 453315 Color: 3

Bin 226: 93755 of cap free
Amount of items: 2
Items: 
Size: 723645 Color: 1
Size: 182601 Color: 4

Bin 227: 94847 of cap free
Amount of items: 2
Items: 
Size: 721417 Color: 0
Size: 183737 Color: 1

Bin 228: 107077 of cap free
Amount of items: 2
Items: 
Size: 453071 Color: 4
Size: 439853 Color: 2

Bin 229: 132912 of cap free
Amount of items: 3
Items: 
Size: 308970 Color: 4
Size: 279324 Color: 0
Size: 278795 Color: 3

Total size: 226787735
Total free space: 2212494

