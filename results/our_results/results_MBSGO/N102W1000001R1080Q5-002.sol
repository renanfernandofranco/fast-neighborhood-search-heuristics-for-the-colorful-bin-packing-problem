Capicity Bin: 1000001
Lower Bound: 45

Bins used: 46
Amount of Colors: 5

Bin 1: 21 of cap free
Amount of items: 2
Items: 
Size: 699659 Color: 1
Size: 300321 Color: 2

Bin 2: 497 of cap free
Amount of items: 3
Items: 
Size: 711858 Color: 0
Size: 176528 Color: 1
Size: 111118 Color: 2

Bin 3: 657 of cap free
Amount of items: 2
Items: 
Size: 538903 Color: 1
Size: 460441 Color: 0

Bin 4: 685 of cap free
Amount of items: 3
Items: 
Size: 652734 Color: 3
Size: 176489 Color: 0
Size: 170093 Color: 0

Bin 5: 693 of cap free
Amount of items: 2
Items: 
Size: 705130 Color: 0
Size: 294178 Color: 2

Bin 6: 781 of cap free
Amount of items: 2
Items: 
Size: 754790 Color: 2
Size: 244430 Color: 1

Bin 7: 861 of cap free
Amount of items: 3
Items: 
Size: 743798 Color: 0
Size: 145613 Color: 2
Size: 109729 Color: 0

Bin 8: 972 of cap free
Amount of items: 2
Items: 
Size: 702610 Color: 1
Size: 296419 Color: 0

Bin 9: 1753 of cap free
Amount of items: 3
Items: 
Size: 668819 Color: 2
Size: 167749 Color: 3
Size: 161680 Color: 1

Bin 10: 1784 of cap free
Amount of items: 2
Items: 
Size: 594287 Color: 3
Size: 403930 Color: 0

Bin 11: 1975 of cap free
Amount of items: 2
Items: 
Size: 570667 Color: 4
Size: 427359 Color: 2

Bin 12: 2107 of cap free
Amount of items: 2
Items: 
Size: 561816 Color: 2
Size: 436078 Color: 3

Bin 13: 2109 of cap free
Amount of items: 3
Items: 
Size: 522279 Color: 2
Size: 265507 Color: 0
Size: 210106 Color: 2

Bin 14: 2151 of cap free
Amount of items: 2
Items: 
Size: 517481 Color: 3
Size: 480369 Color: 2

Bin 15: 2611 of cap free
Amount of items: 3
Items: 
Size: 774118 Color: 4
Size: 117940 Color: 3
Size: 105332 Color: 2

Bin 16: 2616 of cap free
Amount of items: 2
Items: 
Size: 742882 Color: 2
Size: 254503 Color: 0

Bin 17: 2690 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 3
Size: 384571 Color: 2

Bin 18: 2841 of cap free
Amount of items: 2
Items: 
Size: 578287 Color: 3
Size: 418873 Color: 1

Bin 19: 3337 of cap free
Amount of items: 3
Items: 
Size: 642109 Color: 4
Size: 184903 Color: 0
Size: 169652 Color: 1

Bin 20: 3341 of cap free
Amount of items: 2
Items: 
Size: 585486 Color: 1
Size: 411174 Color: 4

Bin 21: 3473 of cap free
Amount of items: 2
Items: 
Size: 794565 Color: 3
Size: 201963 Color: 4

Bin 22: 3782 of cap free
Amount of items: 2
Items: 
Size: 509052 Color: 4
Size: 487167 Color: 3

Bin 23: 4174 of cap free
Amount of items: 2
Items: 
Size: 555241 Color: 0
Size: 440586 Color: 2

Bin 24: 4518 of cap free
Amount of items: 3
Items: 
Size: 583003 Color: 3
Size: 215458 Color: 1
Size: 197022 Color: 3

Bin 25: 5211 of cap free
Amount of items: 3
Items: 
Size: 756218 Color: 1
Size: 136047 Color: 4
Size: 102525 Color: 4

Bin 26: 5726 of cap free
Amount of items: 3
Items: 
Size: 672860 Color: 0
Size: 185600 Color: 1
Size: 135815 Color: 0

Bin 27: 7457 of cap free
Amount of items: 2
Items: 
Size: 742672 Color: 4
Size: 249872 Color: 0

Bin 28: 10347 of cap free
Amount of items: 2
Items: 
Size: 630287 Color: 2
Size: 359367 Color: 0

Bin 29: 12346 of cap free
Amount of items: 2
Items: 
Size: 582035 Color: 0
Size: 405620 Color: 3

Bin 30: 12924 of cap free
Amount of items: 2
Items: 
Size: 709800 Color: 2
Size: 277277 Color: 3

Bin 31: 16496 of cap free
Amount of items: 2
Items: 
Size: 551363 Color: 4
Size: 432142 Color: 0

Bin 32: 17450 of cap free
Amount of items: 2
Items: 
Size: 508596 Color: 3
Size: 473955 Color: 0

Bin 33: 20978 of cap free
Amount of items: 2
Items: 
Size: 508014 Color: 4
Size: 471009 Color: 0

Bin 34: 21329 of cap free
Amount of items: 2
Items: 
Size: 602658 Color: 2
Size: 376014 Color: 0

Bin 35: 25368 of cap free
Amount of items: 2
Items: 
Size: 507242 Color: 2
Size: 467391 Color: 1

Bin 36: 27795 of cap free
Amount of items: 2
Items: 
Size: 602306 Color: 3
Size: 369900 Color: 2

Bin 37: 31332 of cap free
Amount of items: 2
Items: 
Size: 709552 Color: 0
Size: 259117 Color: 3

Bin 38: 32368 of cap free
Amount of items: 2
Items: 
Size: 566072 Color: 0
Size: 401561 Color: 2

Bin 39: 35659 of cap free
Amount of items: 2
Items: 
Size: 741369 Color: 4
Size: 222973 Color: 3

Bin 40: 49119 of cap free
Amount of items: 2
Items: 
Size: 600740 Color: 2
Size: 350142 Color: 0

Bin 41: 97936 of cap free
Amount of items: 2
Items: 
Size: 597025 Color: 2
Size: 305040 Color: 3

Bin 42: 104051 of cap free
Amount of items: 2
Items: 
Size: 550157 Color: 1
Size: 345793 Color: 2

Bin 43: 121653 of cap free
Amount of items: 2
Items: 
Size: 545417 Color: 4
Size: 332931 Color: 2

Bin 44: 168317 of cap free
Amount of items: 2
Items: 
Size: 533747 Color: 4
Size: 297937 Color: 2

Bin 45: 197876 of cap free
Amount of items: 2
Items: 
Size: 498822 Color: 2
Size: 303303 Color: 4

Bin 46: 447852 of cap free
Amount of items: 2
Items: 
Size: 297711 Color: 2
Size: 254438 Color: 4

Total size: 44480027
Total free space: 1520019

