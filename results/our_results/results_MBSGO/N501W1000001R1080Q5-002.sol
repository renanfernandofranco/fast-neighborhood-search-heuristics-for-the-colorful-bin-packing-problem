Capicity Bin: 1000001
Lower Bound: 227

Bins used: 230
Amount of Colors: 5

Bin 1: 2 of cap free
Amount of items: 2
Items: 
Size: 797120 Color: 4
Size: 202879 Color: 3

Bin 2: 4 of cap free
Amount of items: 3
Items: 
Size: 761119 Color: 3
Size: 124903 Color: 0
Size: 113975 Color: 3

Bin 3: 7 of cap free
Amount of items: 3
Items: 
Size: 765728 Color: 2
Size: 119340 Color: 3
Size: 114926 Color: 3

Bin 4: 9 of cap free
Amount of items: 2
Items: 
Size: 742368 Color: 1
Size: 257624 Color: 3

Bin 5: 15 of cap free
Amount of items: 3
Items: 
Size: 790001 Color: 4
Size: 106410 Color: 1
Size: 103575 Color: 1

Bin 6: 29 of cap free
Amount of items: 3
Items: 
Size: 736183 Color: 1
Size: 138000 Color: 2
Size: 125789 Color: 4

Bin 7: 30 of cap free
Amount of items: 2
Items: 
Size: 713246 Color: 1
Size: 286725 Color: 3

Bin 8: 41 of cap free
Amount of items: 2
Items: 
Size: 548131 Color: 1
Size: 451829 Color: 3

Bin 9: 45 of cap free
Amount of items: 3
Items: 
Size: 672735 Color: 3
Size: 166309 Color: 2
Size: 160912 Color: 2

Bin 10: 53 of cap free
Amount of items: 3
Items: 
Size: 609091 Color: 4
Size: 197719 Color: 3
Size: 193138 Color: 1

Bin 11: 54 of cap free
Amount of items: 2
Items: 
Size: 588452 Color: 3
Size: 411495 Color: 2

Bin 12: 55 of cap free
Amount of items: 2
Items: 
Size: 609957 Color: 4
Size: 389989 Color: 0

Bin 13: 55 of cap free
Amount of items: 2
Items: 
Size: 758303 Color: 0
Size: 241643 Color: 1

Bin 14: 61 of cap free
Amount of items: 2
Items: 
Size: 699750 Color: 1
Size: 300190 Color: 0

Bin 15: 62 of cap free
Amount of items: 2
Items: 
Size: 549850 Color: 4
Size: 450089 Color: 2

Bin 16: 65 of cap free
Amount of items: 2
Items: 
Size: 792981 Color: 1
Size: 206955 Color: 4

Bin 17: 72 of cap free
Amount of items: 3
Items: 
Size: 627845 Color: 3
Size: 189217 Color: 4
Size: 182867 Color: 3

Bin 18: 75 of cap free
Amount of items: 2
Items: 
Size: 527916 Color: 1
Size: 472010 Color: 3

Bin 19: 79 of cap free
Amount of items: 3
Items: 
Size: 693163 Color: 1
Size: 155535 Color: 3
Size: 151224 Color: 2

Bin 20: 85 of cap free
Amount of items: 3
Items: 
Size: 767991 Color: 2
Size: 119235 Color: 1
Size: 112690 Color: 0

Bin 21: 86 of cap free
Amount of items: 3
Items: 
Size: 764330 Color: 1
Size: 120879 Color: 0
Size: 114706 Color: 2

Bin 22: 114 of cap free
Amount of items: 3
Items: 
Size: 685843 Color: 2
Size: 160074 Color: 1
Size: 153970 Color: 3

Bin 23: 133 of cap free
Amount of items: 2
Items: 
Size: 799932 Color: 0
Size: 199936 Color: 2

Bin 24: 136 of cap free
Amount of items: 3
Items: 
Size: 684806 Color: 4
Size: 165496 Color: 2
Size: 149563 Color: 2

Bin 25: 137 of cap free
Amount of items: 3
Items: 
Size: 742636 Color: 1
Size: 130570 Color: 4
Size: 126658 Color: 2

Bin 26: 146 of cap free
Amount of items: 2
Items: 
Size: 518464 Color: 2
Size: 481391 Color: 4

Bin 27: 153 of cap free
Amount of items: 2
Items: 
Size: 509202 Color: 3
Size: 490646 Color: 2

Bin 28: 169 of cap free
Amount of items: 2
Items: 
Size: 656416 Color: 4
Size: 343416 Color: 1

Bin 29: 171 of cap free
Amount of items: 2
Items: 
Size: 501885 Color: 2
Size: 497945 Color: 0

Bin 30: 181 of cap free
Amount of items: 2
Items: 
Size: 700743 Color: 2
Size: 299077 Color: 4

Bin 31: 182 of cap free
Amount of items: 3
Items: 
Size: 638359 Color: 2
Size: 182546 Color: 4
Size: 178914 Color: 3

Bin 32: 182 of cap free
Amount of items: 3
Items: 
Size: 741161 Color: 0
Size: 136969 Color: 2
Size: 121689 Color: 4

Bin 33: 184 of cap free
Amount of items: 3
Items: 
Size: 591283 Color: 2
Size: 208835 Color: 1
Size: 199699 Color: 1

Bin 34: 186 of cap free
Amount of items: 2
Items: 
Size: 677926 Color: 2
Size: 321889 Color: 3

Bin 35: 191 of cap free
Amount of items: 2
Items: 
Size: 725445 Color: 4
Size: 274365 Color: 1

Bin 36: 193 of cap free
Amount of items: 2
Items: 
Size: 726549 Color: 2
Size: 273259 Color: 0

Bin 37: 195 of cap free
Amount of items: 3
Items: 
Size: 637098 Color: 4
Size: 182359 Color: 2
Size: 180349 Color: 4

Bin 38: 196 of cap free
Amount of items: 2
Items: 
Size: 594985 Color: 0
Size: 404820 Color: 1

Bin 39: 197 of cap free
Amount of items: 3
Items: 
Size: 508701 Color: 0
Size: 248724 Color: 3
Size: 242379 Color: 2

Bin 40: 204 of cap free
Amount of items: 2
Items: 
Size: 653833 Color: 1
Size: 345964 Color: 3

Bin 41: 207 of cap free
Amount of items: 3
Items: 
Size: 686333 Color: 4
Size: 160171 Color: 2
Size: 153290 Color: 2

Bin 42: 223 of cap free
Amount of items: 3
Items: 
Size: 774636 Color: 1
Size: 113653 Color: 4
Size: 111489 Color: 3

Bin 43: 233 of cap free
Amount of items: 2
Items: 
Size: 704521 Color: 3
Size: 295247 Color: 2

Bin 44: 234 of cap free
Amount of items: 2
Items: 
Size: 562036 Color: 2
Size: 437731 Color: 0

Bin 45: 237 of cap free
Amount of items: 2
Items: 
Size: 543834 Color: 4
Size: 455930 Color: 0

Bin 46: 246 of cap free
Amount of items: 2
Items: 
Size: 571714 Color: 3
Size: 428041 Color: 2

Bin 47: 248 of cap free
Amount of items: 3
Items: 
Size: 651203 Color: 4
Size: 177559 Color: 3
Size: 170991 Color: 2

Bin 48: 260 of cap free
Amount of items: 2
Items: 
Size: 541246 Color: 2
Size: 458495 Color: 3

Bin 49: 273 of cap free
Amount of items: 2
Items: 
Size: 678609 Color: 0
Size: 321119 Color: 1

Bin 50: 275 of cap free
Amount of items: 3
Items: 
Size: 762231 Color: 3
Size: 122760 Color: 0
Size: 114735 Color: 2

Bin 51: 300 of cap free
Amount of items: 2
Items: 
Size: 509659 Color: 3
Size: 490042 Color: 4

Bin 52: 301 of cap free
Amount of items: 2
Items: 
Size: 545329 Color: 3
Size: 454371 Color: 4

Bin 53: 306 of cap free
Amount of items: 2
Items: 
Size: 792086 Color: 1
Size: 207609 Color: 0

Bin 54: 309 of cap free
Amount of items: 3
Items: 
Size: 648983 Color: 4
Size: 179179 Color: 1
Size: 171530 Color: 1

Bin 55: 312 of cap free
Amount of items: 2
Items: 
Size: 677965 Color: 0
Size: 321724 Color: 3

Bin 56: 346 of cap free
Amount of items: 2
Items: 
Size: 673095 Color: 0
Size: 326560 Color: 1

Bin 57: 352 of cap free
Amount of items: 2
Items: 
Size: 680541 Color: 4
Size: 319108 Color: 3

Bin 58: 357 of cap free
Amount of items: 2
Items: 
Size: 652696 Color: 3
Size: 346948 Color: 2

Bin 59: 363 of cap free
Amount of items: 2
Items: 
Size: 549030 Color: 3
Size: 450608 Color: 0

Bin 60: 363 of cap free
Amount of items: 2
Items: 
Size: 605982 Color: 4
Size: 393656 Color: 0

Bin 61: 371 of cap free
Amount of items: 2
Items: 
Size: 540313 Color: 0
Size: 459317 Color: 1

Bin 62: 422 of cap free
Amount of items: 3
Items: 
Size: 694171 Color: 3
Size: 154730 Color: 0
Size: 150678 Color: 1

Bin 63: 426 of cap free
Amount of items: 2
Items: 
Size: 759502 Color: 1
Size: 240073 Color: 3

Bin 64: 433 of cap free
Amount of items: 3
Items: 
Size: 690855 Color: 1
Size: 156738 Color: 4
Size: 151975 Color: 2

Bin 65: 434 of cap free
Amount of items: 2
Items: 
Size: 519338 Color: 0
Size: 480229 Color: 4

Bin 66: 436 of cap free
Amount of items: 2
Items: 
Size: 698971 Color: 2
Size: 300594 Color: 1

Bin 67: 443 of cap free
Amount of items: 2
Items: 
Size: 637647 Color: 3
Size: 361911 Color: 4

Bin 68: 462 of cap free
Amount of items: 3
Items: 
Size: 639435 Color: 3
Size: 181119 Color: 4
Size: 178985 Color: 0

Bin 69: 472 of cap free
Amount of items: 2
Items: 
Size: 514740 Color: 0
Size: 484789 Color: 4

Bin 70: 479 of cap free
Amount of items: 3
Items: 
Size: 735008 Color: 3
Size: 138046 Color: 2
Size: 126468 Color: 3

Bin 71: 483 of cap free
Amount of items: 2
Items: 
Size: 682049 Color: 3
Size: 317469 Color: 1

Bin 72: 491 of cap free
Amount of items: 2
Items: 
Size: 573849 Color: 3
Size: 425661 Color: 2

Bin 73: 496 of cap free
Amount of items: 2
Items: 
Size: 779215 Color: 2
Size: 220290 Color: 3

Bin 74: 508 of cap free
Amount of items: 2
Items: 
Size: 783543 Color: 1
Size: 215950 Color: 3

Bin 75: 522 of cap free
Amount of items: 2
Items: 
Size: 671498 Color: 0
Size: 327981 Color: 1

Bin 76: 534 of cap free
Amount of items: 3
Items: 
Size: 730750 Color: 2
Size: 139802 Color: 4
Size: 128915 Color: 2

Bin 77: 568 of cap free
Amount of items: 2
Items: 
Size: 597626 Color: 4
Size: 401807 Color: 3

Bin 78: 579 of cap free
Amount of items: 2
Items: 
Size: 547812 Color: 3
Size: 451610 Color: 2

Bin 79: 590 of cap free
Amount of items: 2
Items: 
Size: 794113 Color: 4
Size: 205298 Color: 0

Bin 80: 597 of cap free
Amount of items: 2
Items: 
Size: 636722 Color: 1
Size: 362682 Color: 3

Bin 81: 622 of cap free
Amount of items: 2
Items: 
Size: 603798 Color: 2
Size: 395581 Color: 4

Bin 82: 644 of cap free
Amount of items: 2
Items: 
Size: 653450 Color: 2
Size: 345907 Color: 0

Bin 83: 650 of cap free
Amount of items: 3
Items: 
Size: 783291 Color: 3
Size: 111705 Color: 1
Size: 104355 Color: 0

Bin 84: 659 of cap free
Amount of items: 2
Items: 
Size: 604994 Color: 1
Size: 394348 Color: 3

Bin 85: 693 of cap free
Amount of items: 2
Items: 
Size: 669686 Color: 3
Size: 329622 Color: 2

Bin 86: 715 of cap free
Amount of items: 2
Items: 
Size: 782089 Color: 1
Size: 217197 Color: 2

Bin 87: 738 of cap free
Amount of items: 3
Items: 
Size: 748580 Color: 3
Size: 127197 Color: 1
Size: 123486 Color: 4

Bin 88: 739 of cap free
Amount of items: 2
Items: 
Size: 659502 Color: 1
Size: 339760 Color: 0

Bin 89: 743 of cap free
Amount of items: 3
Items: 
Size: 602156 Color: 0
Size: 200555 Color: 3
Size: 196547 Color: 0

Bin 90: 753 of cap free
Amount of items: 2
Items: 
Size: 549020 Color: 2
Size: 450228 Color: 1

Bin 91: 754 of cap free
Amount of items: 2
Items: 
Size: 701372 Color: 1
Size: 297875 Color: 4

Bin 92: 755 of cap free
Amount of items: 3
Items: 
Size: 581318 Color: 4
Size: 214493 Color: 3
Size: 203435 Color: 2

Bin 93: 819 of cap free
Amount of items: 2
Items: 
Size: 602763 Color: 3
Size: 396419 Color: 0

Bin 94: 822 of cap free
Amount of items: 3
Items: 
Size: 512544 Color: 1
Size: 243100 Color: 3
Size: 243535 Color: 0

Bin 95: 822 of cap free
Amount of items: 3
Items: 
Size: 708211 Color: 3
Size: 146410 Color: 4
Size: 144558 Color: 3

Bin 96: 843 of cap free
Amount of items: 2
Items: 
Size: 502940 Color: 4
Size: 496218 Color: 2

Bin 97: 850 of cap free
Amount of items: 2
Items: 
Size: 606173 Color: 0
Size: 392978 Color: 3

Bin 98: 850 of cap free
Amount of items: 2
Items: 
Size: 722282 Color: 2
Size: 276869 Color: 4

Bin 99: 870 of cap free
Amount of items: 2
Items: 
Size: 557859 Color: 3
Size: 441272 Color: 0

Bin 100: 908 of cap free
Amount of items: 2
Items: 
Size: 664907 Color: 0
Size: 334186 Color: 2

Bin 101: 922 of cap free
Amount of items: 2
Items: 
Size: 728242 Color: 4
Size: 270837 Color: 3

Bin 102: 928 of cap free
Amount of items: 2
Items: 
Size: 506152 Color: 1
Size: 492921 Color: 3

Bin 103: 933 of cap free
Amount of items: 3
Items: 
Size: 617391 Color: 4
Size: 194685 Color: 3
Size: 186992 Color: 1

Bin 104: 968 of cap free
Amount of items: 2
Items: 
Size: 793053 Color: 3
Size: 205980 Color: 0

Bin 105: 980 of cap free
Amount of items: 2
Items: 
Size: 623830 Color: 2
Size: 375191 Color: 4

Bin 106: 992 of cap free
Amount of items: 2
Items: 
Size: 795448 Color: 2
Size: 203561 Color: 4

Bin 107: 1002 of cap free
Amount of items: 2
Items: 
Size: 792096 Color: 0
Size: 206903 Color: 3

Bin 108: 1004 of cap free
Amount of items: 2
Items: 
Size: 510741 Color: 4
Size: 488256 Color: 3

Bin 109: 1039 of cap free
Amount of items: 3
Items: 
Size: 666146 Color: 0
Size: 166466 Color: 3
Size: 166350 Color: 3

Bin 110: 1066 of cap free
Amount of items: 2
Items: 
Size: 657609 Color: 0
Size: 341326 Color: 1

Bin 111: 1119 of cap free
Amount of items: 2
Items: 
Size: 550042 Color: 3
Size: 448840 Color: 4

Bin 112: 1125 of cap free
Amount of items: 2
Items: 
Size: 644475 Color: 1
Size: 354401 Color: 2

Bin 113: 1137 of cap free
Amount of items: 2
Items: 
Size: 787553 Color: 4
Size: 211311 Color: 2

Bin 114: 1192 of cap free
Amount of items: 2
Items: 
Size: 618769 Color: 2
Size: 380040 Color: 0

Bin 115: 1216 of cap free
Amount of items: 2
Items: 
Size: 627345 Color: 0
Size: 371440 Color: 2

Bin 116: 1257 of cap free
Amount of items: 2
Items: 
Size: 742503 Color: 1
Size: 256241 Color: 4

Bin 117: 1271 of cap free
Amount of items: 2
Items: 
Size: 631693 Color: 3
Size: 367037 Color: 2

Bin 118: 1385 of cap free
Amount of items: 2
Items: 
Size: 720050 Color: 4
Size: 278566 Color: 0

Bin 119: 1413 of cap free
Amount of items: 2
Items: 
Size: 588794 Color: 4
Size: 409794 Color: 3

Bin 120: 1432 of cap free
Amount of items: 2
Items: 
Size: 544623 Color: 1
Size: 453946 Color: 2

Bin 121: 1529 of cap free
Amount of items: 2
Items: 
Size: 726010 Color: 1
Size: 272462 Color: 2

Bin 122: 1562 of cap free
Amount of items: 2
Items: 
Size: 752441 Color: 4
Size: 245998 Color: 0

Bin 123: 1563 of cap free
Amount of items: 3
Items: 
Size: 663910 Color: 2
Size: 166827 Color: 0
Size: 167701 Color: 2

Bin 124: 1572 of cap free
Amount of items: 2
Items: 
Size: 686173 Color: 2
Size: 312256 Color: 0

Bin 125: 1576 of cap free
Amount of items: 2
Items: 
Size: 632941 Color: 4
Size: 365484 Color: 1

Bin 126: 1578 of cap free
Amount of items: 2
Items: 
Size: 634658 Color: 4
Size: 363765 Color: 1

Bin 127: 1583 of cap free
Amount of items: 3
Items: 
Size: 505165 Color: 4
Size: 248662 Color: 0
Size: 244591 Color: 1

Bin 128: 1603 of cap free
Amount of items: 3
Items: 
Size: 556063 Color: 4
Size: 228224 Color: 1
Size: 214111 Color: 4

Bin 129: 1645 of cap free
Amount of items: 2
Items: 
Size: 508257 Color: 1
Size: 490099 Color: 3

Bin 130: 1652 of cap free
Amount of items: 2
Items: 
Size: 641155 Color: 0
Size: 357194 Color: 3

Bin 131: 1676 of cap free
Amount of items: 2
Items: 
Size: 706335 Color: 2
Size: 291990 Color: 4

Bin 132: 1678 of cap free
Amount of items: 2
Items: 
Size: 682366 Color: 1
Size: 315957 Color: 4

Bin 133: 1747 of cap free
Amount of items: 2
Items: 
Size: 598262 Color: 4
Size: 399992 Color: 3

Bin 134: 1796 of cap free
Amount of items: 2
Items: 
Size: 712268 Color: 2
Size: 285937 Color: 3

Bin 135: 1827 of cap free
Amount of items: 2
Items: 
Size: 500856 Color: 4
Size: 497318 Color: 1

Bin 136: 1856 of cap free
Amount of items: 2
Items: 
Size: 674747 Color: 3
Size: 323398 Color: 0

Bin 137: 1882 of cap free
Amount of items: 2
Items: 
Size: 778893 Color: 1
Size: 219226 Color: 0

Bin 138: 1898 of cap free
Amount of items: 2
Items: 
Size: 588389 Color: 2
Size: 409714 Color: 3

Bin 139: 1912 of cap free
Amount of items: 2
Items: 
Size: 523463 Color: 0
Size: 474626 Color: 3

Bin 140: 2010 of cap free
Amount of items: 2
Items: 
Size: 716233 Color: 4
Size: 281758 Color: 1

Bin 141: 2097 of cap free
Amount of items: 3
Items: 
Size: 644398 Color: 4
Size: 176805 Color: 1
Size: 176701 Color: 2

Bin 142: 2179 of cap free
Amount of items: 2
Items: 
Size: 731785 Color: 1
Size: 266037 Color: 2

Bin 143: 2179 of cap free
Amount of items: 3
Items: 
Size: 761061 Color: 4
Size: 121000 Color: 2
Size: 115761 Color: 3

Bin 144: 2438 of cap free
Amount of items: 2
Items: 
Size: 702453 Color: 4
Size: 295110 Color: 3

Bin 145: 2445 of cap free
Amount of items: 2
Items: 
Size: 580105 Color: 1
Size: 417451 Color: 2

Bin 146: 2452 of cap free
Amount of items: 2
Items: 
Size: 771913 Color: 1
Size: 225636 Color: 3

Bin 147: 2484 of cap free
Amount of items: 2
Items: 
Size: 625867 Color: 1
Size: 371650 Color: 0

Bin 148: 2562 of cap free
Amount of items: 2
Items: 
Size: 605940 Color: 0
Size: 391499 Color: 1

Bin 149: 2603 of cap free
Amount of items: 2
Items: 
Size: 674424 Color: 4
Size: 322974 Color: 3

Bin 150: 2834 of cap free
Amount of items: 2
Items: 
Size: 717792 Color: 1
Size: 279375 Color: 4

Bin 151: 2841 of cap free
Amount of items: 2
Items: 
Size: 653436 Color: 3
Size: 343724 Color: 4

Bin 152: 2870 of cap free
Amount of items: 2
Items: 
Size: 706870 Color: 3
Size: 290261 Color: 2

Bin 153: 2995 of cap free
Amount of items: 2
Items: 
Size: 523325 Color: 3
Size: 473681 Color: 1

Bin 154: 3134 of cap free
Amount of items: 2
Items: 
Size: 568702 Color: 4
Size: 428165 Color: 1

Bin 155: 3226 of cap free
Amount of items: 2
Items: 
Size: 728239 Color: 4
Size: 268536 Color: 3

Bin 156: 3244 of cap free
Amount of items: 2
Items: 
Size: 583916 Color: 2
Size: 412841 Color: 4

Bin 157: 3367 of cap free
Amount of items: 2
Items: 
Size: 588320 Color: 0
Size: 408314 Color: 2

Bin 158: 3432 of cap free
Amount of items: 2
Items: 
Size: 740799 Color: 1
Size: 255770 Color: 4

Bin 159: 3592 of cap free
Amount of items: 2
Items: 
Size: 653448 Color: 4
Size: 342961 Color: 3

Bin 160: 3613 of cap free
Amount of items: 2
Items: 
Size: 625617 Color: 2
Size: 370771 Color: 0

Bin 161: 3657 of cap free
Amount of items: 2
Items: 
Size: 604868 Color: 4
Size: 391476 Color: 1

Bin 162: 3662 of cap free
Amount of items: 2
Items: 
Size: 607315 Color: 1
Size: 389024 Color: 3

Bin 163: 3834 of cap free
Amount of items: 2
Items: 
Size: 674087 Color: 4
Size: 322080 Color: 0

Bin 164: 3837 of cap free
Amount of items: 2
Items: 
Size: 625387 Color: 0
Size: 370777 Color: 2

Bin 165: 3857 of cap free
Amount of items: 2
Items: 
Size: 524850 Color: 1
Size: 471294 Color: 3

Bin 166: 3868 of cap free
Amount of items: 2
Items: 
Size: 706164 Color: 2
Size: 289969 Color: 3

Bin 167: 3986 of cap free
Amount of items: 2
Items: 
Size: 724008 Color: 2
Size: 272007 Color: 3

Bin 168: 4193 of cap free
Amount of items: 3
Items: 
Size: 613315 Color: 1
Size: 191938 Color: 4
Size: 190555 Color: 2

Bin 169: 4233 of cap free
Amount of items: 2
Items: 
Size: 737717 Color: 0
Size: 258051 Color: 3

Bin 170: 4505 of cap free
Amount of items: 2
Items: 
Size: 648694 Color: 1
Size: 346802 Color: 0

Bin 171: 4871 of cap free
Amount of items: 2
Items: 
Size: 524032 Color: 1
Size: 471098 Color: 2

Bin 172: 4881 of cap free
Amount of items: 2
Items: 
Size: 763471 Color: 1
Size: 231649 Color: 3

Bin 173: 4983 of cap free
Amount of items: 2
Items: 
Size: 705760 Color: 0
Size: 289258 Color: 2

Bin 174: 5122 of cap free
Amount of items: 2
Items: 
Size: 771353 Color: 1
Size: 223526 Color: 4

Bin 175: 5363 of cap free
Amount of items: 2
Items: 
Size: 587418 Color: 1
Size: 407220 Color: 3

Bin 176: 5456 of cap free
Amount of items: 3
Items: 
Size: 579308 Color: 4
Size: 213140 Color: 2
Size: 202097 Color: 2

Bin 177: 5684 of cap free
Amount of items: 2
Items: 
Size: 663822 Color: 2
Size: 330495 Color: 1

Bin 178: 5966 of cap free
Amount of items: 2
Items: 
Size: 736297 Color: 3
Size: 257738 Color: 1

Bin 179: 6387 of cap free
Amount of items: 2
Items: 
Size: 647009 Color: 2
Size: 346605 Color: 4

Bin 180: 6623 of cap free
Amount of items: 2
Items: 
Size: 738193 Color: 1
Size: 255185 Color: 0

Bin 181: 6839 of cap free
Amount of items: 2
Items: 
Size: 778206 Color: 1
Size: 214956 Color: 0

Bin 182: 7183 of cap free
Amount of items: 2
Items: 
Size: 546640 Color: 0
Size: 446178 Color: 1

Bin 183: 7409 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 0
Size: 379852 Color: 2

Bin 184: 7527 of cap free
Amount of items: 2
Items: 
Size: 672936 Color: 4
Size: 319538 Color: 0

Bin 185: 7603 of cap free
Amount of items: 2
Items: 
Size: 522325 Color: 1
Size: 470073 Color: 4

Bin 186: 7879 of cap free
Amount of items: 2
Items: 
Size: 776740 Color: 4
Size: 215382 Color: 2

Bin 187: 7959 of cap free
Amount of items: 2
Items: 
Size: 601102 Color: 4
Size: 390940 Color: 1

Bin 188: 8216 of cap free
Amount of items: 2
Items: 
Size: 601125 Color: 0
Size: 390660 Color: 1

Bin 189: 8684 of cap free
Amount of items: 2
Items: 
Size: 533880 Color: 3
Size: 457437 Color: 1

Bin 190: 9383 of cap free
Amount of items: 2
Items: 
Size: 671465 Color: 1
Size: 319153 Color: 4

Bin 191: 9888 of cap free
Amount of items: 2
Items: 
Size: 520444 Color: 1
Size: 469669 Color: 3

Bin 192: 10076 of cap free
Amount of items: 2
Items: 
Size: 736061 Color: 1
Size: 253864 Color: 3

Bin 193: 10371 of cap free
Amount of items: 2
Items: 
Size: 544134 Color: 3
Size: 445496 Color: 0

Bin 194: 10886 of cap free
Amount of items: 2
Items: 
Size: 759027 Color: 1
Size: 230088 Color: 2

Bin 195: 11484 of cap free
Amount of items: 2
Items: 
Size: 495986 Color: 3
Size: 492531 Color: 0

Bin 196: 11777 of cap free
Amount of items: 2
Items: 
Size: 531520 Color: 4
Size: 456704 Color: 2

Bin 197: 12726 of cap free
Amount of items: 2
Items: 
Size: 531099 Color: 0
Size: 456176 Color: 1

Bin 198: 12763 of cap free
Amount of items: 2
Items: 
Size: 669103 Color: 1
Size: 318135 Color: 0

Bin 199: 12944 of cap free
Amount of items: 2
Items: 
Size: 612391 Color: 0
Size: 374666 Color: 3

Bin 200: 13197 of cap free
Amount of items: 2
Items: 
Size: 757457 Color: 2
Size: 229347 Color: 4

Bin 201: 15204 of cap free
Amount of items: 2
Items: 
Size: 671412 Color: 0
Size: 313385 Color: 2

Bin 202: 16489 of cap free
Amount of items: 2
Items: 
Size: 579582 Color: 1
Size: 403930 Color: 4

Bin 203: 16857 of cap free
Amount of items: 2
Items: 
Size: 578654 Color: 4
Size: 404490 Color: 1

Bin 204: 17634 of cap free
Amount of items: 2
Items: 
Size: 728524 Color: 1
Size: 253843 Color: 0

Bin 205: 18384 of cap free
Amount of items: 2
Items: 
Size: 577838 Color: 2
Size: 403779 Color: 0

Bin 206: 18980 of cap free
Amount of items: 2
Items: 
Size: 577917 Color: 0
Size: 403104 Color: 1

Bin 207: 19270 of cap free
Amount of items: 2
Items: 
Size: 637997 Color: 2
Size: 342734 Color: 1

Bin 208: 23138 of cap free
Amount of items: 2
Items: 
Size: 723688 Color: 0
Size: 253175 Color: 4

Bin 209: 23712 of cap free
Amount of items: 2
Items: 
Size: 574450 Color: 0
Size: 401839 Color: 4

Bin 210: 23859 of cap free
Amount of items: 2
Items: 
Size: 663622 Color: 0
Size: 312520 Color: 2

Bin 211: 24068 of cap free
Amount of items: 2
Items: 
Size: 488218 Color: 1
Size: 487715 Color: 4

Bin 212: 25028 of cap free
Amount of items: 2
Items: 
Size: 575477 Color: 4
Size: 399496 Color: 3

Bin 213: 27113 of cap free
Amount of items: 2
Items: 
Size: 662002 Color: 3
Size: 310886 Color: 0

Bin 214: 28920 of cap free
Amount of items: 2
Items: 
Size: 487984 Color: 1
Size: 483097 Color: 0

Bin 215: 32929 of cap free
Amount of items: 2
Items: 
Size: 658823 Color: 2
Size: 308249 Color: 1

Bin 216: 35085 of cap free
Amount of items: 2
Items: 
Size: 657745 Color: 1
Size: 307171 Color: 4

Bin 217: 37840 of cap free
Amount of items: 2
Items: 
Size: 658785 Color: 2
Size: 303376 Color: 0

Bin 218: 40242 of cap free
Amount of items: 2
Items: 
Size: 481325 Color: 0
Size: 478434 Color: 1

Bin 219: 47185 of cap free
Amount of items: 2
Items: 
Size: 480464 Color: 0
Size: 472352 Color: 1

Bin 220: 57643 of cap free
Amount of items: 2
Items: 
Size: 573872 Color: 2
Size: 368486 Color: 3

Bin 221: 68151 of cap free
Amount of items: 2
Items: 
Size: 568522 Color: 1
Size: 363328 Color: 3

Bin 222: 73553 of cap free
Amount of items: 2
Items: 
Size: 564018 Color: 2
Size: 362430 Color: 1

Bin 223: 129710 of cap free
Amount of items: 2
Items: 
Size: 567050 Color: 1
Size: 303241 Color: 3

Bin 224: 138454 of cap free
Amount of items: 2
Items: 
Size: 563559 Color: 4
Size: 297988 Color: 1

Bin 225: 147653 of cap free
Amount of items: 2
Items: 
Size: 555569 Color: 2
Size: 296779 Color: 4

Bin 226: 160669 of cap free
Amount of items: 2
Items: 
Size: 555189 Color: 2
Size: 284143 Color: 4

Bin 227: 182109 of cap free
Amount of items: 2
Items: 
Size: 530959 Color: 3
Size: 286933 Color: 2

Bin 228: 185095 of cap free
Amount of items: 2
Items: 
Size: 530782 Color: 2
Size: 284124 Color: 4

Bin 229: 530546 of cap free
Amount of items: 1
Items: 
Size: 469455 Color: 2

Bin 230: 723603 of cap free
Amount of items: 1
Items: 
Size: 276398 Color: 2

Total size: 226673258
Total free space: 3326972

