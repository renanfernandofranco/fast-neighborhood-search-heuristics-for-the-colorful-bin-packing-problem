Capicity Bin: 1001
Lower Bound: 225

Bins used: 227
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 3
Size: 328 Color: 2
Size: 322 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 277 Color: 4
Size: 277 Color: 4

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 0

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 3
Size: 488 Color: 0

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 486 Color: 3

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 1

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 1
Size: 480 Color: 0

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 1
Size: 473 Color: 0

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 4

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 2
Size: 448 Color: 1

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 446 Color: 2

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 442 Color: 2

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 4
Size: 443 Color: 3

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 3
Size: 264 Color: 4
Size: 169 Color: 0

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 4
Size: 432 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 1
Size: 263 Color: 4
Size: 168 Color: 3

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 431 Color: 2

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 431 Color: 3

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 4

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 0
Size: 420 Color: 4

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 4

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 4
Size: 414 Color: 2

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 2
Size: 411 Color: 3

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 2

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 1

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 395 Color: 1

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 2

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 1
Size: 385 Color: 3

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 381 Color: 2

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 382 Color: 4

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 2
Size: 380 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 625 Color: 2
Size: 189 Color: 3
Size: 187 Color: 2

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 373 Color: 4

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 4

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 0

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 367 Color: 4

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 4

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 2

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 2
Size: 356 Color: 3

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 2
Size: 354 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 4
Size: 181 Color: 2
Size: 169 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 4
Size: 184 Color: 3
Size: 165 Color: 2

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 3
Size: 348 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 0
Size: 177 Color: 4
Size: 170 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 2
Size: 180 Color: 4
Size: 167 Color: 4

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 344 Color: 4

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 1
Size: 344 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 2
Size: 174 Color: 1
Size: 164 Color: 0

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 341 Color: 1

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 2
Size: 335 Color: 3

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 340 Color: 3

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 3

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 0
Size: 160 Color: 4
Size: 160 Color: 4

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 318 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 3
Size: 159 Color: 0
Size: 156 Color: 0

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 1
Size: 310 Color: 0

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 308 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 1
Size: 155 Color: 2
Size: 149 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 0
Size: 155 Color: 3
Size: 148 Color: 0

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 701 Color: 1
Size: 153 Color: 0
Size: 147 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 0
Size: 149 Color: 1
Size: 139 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 3
Size: 152 Color: 0
Size: 139 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 712 Color: 4
Size: 144 Color: 2
Size: 145 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 0
Size: 146 Color: 2
Size: 134 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 139 Color: 3
Size: 136 Color: 4

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 279 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 1
Size: 145 Color: 2
Size: 133 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 0
Size: 141 Color: 4
Size: 131 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 1
Size: 137 Color: 2
Size: 132 Color: 2

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 2
Size: 268 Color: 0

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 256 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 3
Size: 128 Color: 0
Size: 126 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 2
Size: 126 Color: 4
Size: 124 Color: 3

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 3
Size: 246 Color: 4

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 3
Size: 121 Color: 4
Size: 116 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 4
Size: 235 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 778 Color: 3
Size: 113 Color: 0
Size: 110 Color: 0

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 0
Size: 225 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 4
Size: 115 Color: 3
Size: 106 Color: 1

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 2
Size: 219 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 783 Color: 0
Size: 110 Color: 1
Size: 108 Color: 2

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 1

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 1

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 2
Size: 216 Color: 3

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 3
Size: 216 Color: 1

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 4
Size: 207 Color: 0

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 2
Size: 208 Color: 3

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 4
Size: 207 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 4
Size: 103 Color: 2
Size: 102 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 800 Color: 4
Size: 101 Color: 0
Size: 100 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 331 Color: 3
Size: 282 Color: 0

Bin 96: 1 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 497 Color: 4

Bin 97: 1 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 486 Color: 0

Bin 98: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 483 Color: 2

Bin 99: 1 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 1
Size: 479 Color: 2

Bin 100: 1 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 2
Size: 477 Color: 0

Bin 101: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 467 Color: 1

Bin 102: 1 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 446 Color: 2

Bin 103: 1 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 425 Color: 2

Bin 104: 1 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 423 Color: 2

Bin 105: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 0
Size: 404 Color: 3

Bin 106: 1 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 397 Color: 2

Bin 107: 1 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 394 Color: 0

Bin 108: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 392 Color: 2

Bin 109: 1 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 3
Size: 388 Color: 0

Bin 110: 1 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 375 Color: 2

Bin 111: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 4
Size: 373 Color: 0

Bin 112: 1 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 3
Size: 184 Color: 4
Size: 184 Color: 2

Bin 113: 1 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 361 Color: 1

Bin 114: 1 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 3
Size: 358 Color: 1

Bin 115: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 326 Color: 1

Bin 116: 1 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 2
Size: 313 Color: 1

Bin 117: 1 of cap free
Amount of items: 3
Items: 
Size: 690 Color: 0
Size: 155 Color: 1
Size: 155 Color: 0

Bin 118: 1 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 1
Size: 148 Color: 0
Size: 143 Color: 2

Bin 119: 1 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 2
Size: 145 Color: 1
Size: 134 Color: 3

Bin 120: 1 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 0
Size: 273 Color: 1

Bin 121: 1 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 255 Color: 4

Bin 122: 1 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 251 Color: 1

Bin 123: 1 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 247 Color: 1

Bin 124: 1 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 4
Size: 240 Color: 0

Bin 125: 1 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 228 Color: 2

Bin 126: 1 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 2
Size: 114 Color: 3
Size: 112 Color: 0

Bin 127: 1 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 4
Size: 114 Color: 2
Size: 112 Color: 0

Bin 128: 1 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 3
Size: 220 Color: 0

Bin 129: 1 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 214 Color: 3

Bin 130: 1 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 3
Size: 212 Color: 4

Bin 131: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 199 Color: 4

Bin 132: 2 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 2
Size: 331 Color: 0
Size: 333 Color: 3

Bin 133: 2 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 1
Size: 499 Color: 4

Bin 134: 2 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 483 Color: 2

Bin 135: 2 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 457 Color: 1

Bin 136: 2 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 457 Color: 4

Bin 137: 2 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 4
Size: 458 Color: 2

Bin 138: 2 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 4
Size: 448 Color: 2

Bin 139: 2 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 447 Color: 3

Bin 140: 2 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 435 Color: 3

Bin 141: 2 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 4
Size: 425 Color: 1

Bin 142: 2 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 409 Color: 4

Bin 143: 2 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 403 Color: 1

Bin 144: 2 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 2
Size: 373 Color: 0

Bin 145: 2 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 0
Size: 177 Color: 2
Size: 171 Color: 3

Bin 146: 2 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 299 Color: 2

Bin 147: 2 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 299 Color: 4

Bin 148: 2 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 1
Size: 246 Color: 0

Bin 149: 3 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 4
Size: 288 Color: 0
Size: 267 Color: 0

Bin 150: 3 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 1
Size: 485 Color: 0

Bin 151: 3 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 0
Size: 468 Color: 2

Bin 152: 3 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 2
Size: 424 Color: 0

Bin 153: 3 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 4
Size: 419 Color: 2

Bin 154: 3 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 1
Size: 323 Color: 3

Bin 155: 3 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 278 Color: 1

Bin 156: 3 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 1
Size: 251 Color: 4

Bin 157: 3 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 246 Color: 0

Bin 158: 3 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 1
Size: 239 Color: 2

Bin 159: 3 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 4
Size: 235 Color: 1

Bin 160: 3 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 3
Size: 203 Color: 2

Bin 161: 4 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 3
Size: 337 Color: 1
Size: 306 Color: 1

Bin 162: 4 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 3
Size: 472 Color: 2

Bin 163: 4 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 397 Color: 0

Bin 164: 4 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 365 Color: 4

Bin 165: 4 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 345 Color: 4

Bin 166: 4 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 2
Size: 298 Color: 4

Bin 167: 4 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 258 Color: 0

Bin 168: 5 of cap free
Amount of items: 3
Items: 
Size: 539 Color: 1
Size: 267 Color: 4
Size: 190 Color: 3

Bin 169: 5 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 402 Color: 0

Bin 170: 5 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 390 Color: 3

Bin 171: 5 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 2
Size: 172 Color: 1
Size: 172 Color: 2

Bin 172: 5 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 0
Size: 287 Color: 2

Bin 173: 5 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 234 Color: 0

Bin 174: 6 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 284 Color: 4
Size: 267 Color: 4

Bin 175: 6 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 2
Size: 496 Color: 4

Bin 176: 6 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 456 Color: 2

Bin 177: 6 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 433 Color: 3

Bin 178: 6 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 429 Color: 4

Bin 179: 6 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 3
Size: 409 Color: 2

Bin 180: 6 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 364 Color: 0

Bin 181: 6 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 3
Size: 174 Color: 2
Size: 171 Color: 1

Bin 182: 6 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 2
Size: 322 Color: 1

Bin 183: 6 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 3
Size: 219 Color: 2

Bin 184: 6 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 202 Color: 2

Bin 185: 7 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 456 Color: 0

Bin 186: 7 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 3
Size: 408 Color: 4

Bin 187: 7 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 2
Size: 312 Color: 0

Bin 188: 7 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 223 Color: 3

Bin 189: 7 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 2
Size: 198 Color: 0

Bin 190: 8 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 409 Color: 1

Bin 191: 8 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 297 Color: 3

Bin 192: 9 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 0
Size: 467 Color: 3

Bin 193: 9 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 453 Color: 1

Bin 194: 9 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 393 Color: 1

Bin 195: 9 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 312 Color: 0

Bin 196: 10 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 312 Color: 2

Bin 197: 10 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 233 Color: 4

Bin 198: 10 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 232 Color: 2

Bin 199: 11 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 481 Color: 1

Bin 200: 12 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 482 Color: 4

Bin 201: 12 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 0
Size: 232 Color: 4

Bin 202: 12 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 218 Color: 0

Bin 203: 13 of cap free
Amount of items: 3
Items: 
Size: 330 Color: 4
Size: 329 Color: 0
Size: 329 Color: 3

Bin 204: 14 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 216 Color: 3

Bin 205: 14 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 2
Size: 195 Color: 4

Bin 206: 16 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 282 Color: 0
Size: 263 Color: 3

Bin 207: 17 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 0
Size: 363 Color: 4

Bin 208: 17 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 1
Size: 214 Color: 4

Bin 209: 19 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 360 Color: 1

Bin 210: 19 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 4
Size: 212 Color: 1

Bin 211: 26 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 390 Color: 0

Bin 212: 26 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 391 Color: 1

Bin 213: 28 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 452 Color: 4

Bin 214: 29 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 451 Color: 0

Bin 215: 29 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 387 Color: 4

Bin 216: 30 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 265 Color: 3
Size: 264 Color: 0

Bin 217: 32 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 211 Color: 4

Bin 218: 33 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 211 Color: 4

Bin 219: 40 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 0
Size: 210 Color: 1

Bin 220: 47 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 210 Color: 1

Bin 221: 59 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 197 Color: 2

Bin 222: 64 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 193 Color: 4

Bin 223: 141 of cap free
Amount of items: 2
Items: 
Size: 438 Color: 1
Size: 422 Color: 4

Bin 224: 198 of cap free
Amount of items: 2
Items: 
Size: 419 Color: 0
Size: 384 Color: 4

Bin 225: 264 of cap free
Amount of items: 2
Items: 
Size: 380 Color: 0
Size: 357 Color: 3

Bin 226: 672 of cap free
Amount of items: 1
Items: 
Size: 329 Color: 0

Bin 227: 672 of cap free
Amount of items: 1
Items: 
Size: 329 Color: 0

Total size: 224313
Total free space: 2914

