Capicity Bin: 1000001
Lower Bound: 50

Bins used: 55
Amount of Colors: 5

Bin 1: 140 of cap free
Amount of items: 2
Items: 
Size: 707712 Color: 0
Size: 292149 Color: 2

Bin 2: 601 of cap free
Amount of items: 2
Items: 
Size: 704949 Color: 4
Size: 294451 Color: 1

Bin 3: 1080 of cap free
Amount of items: 2
Items: 
Size: 611384 Color: 0
Size: 387537 Color: 1

Bin 4: 1496 of cap free
Amount of items: 2
Items: 
Size: 568920 Color: 3
Size: 429585 Color: 2

Bin 5: 1517 of cap free
Amount of items: 2
Items: 
Size: 704315 Color: 3
Size: 294169 Color: 1

Bin 6: 1706 of cap free
Amount of items: 3
Items: 
Size: 738351 Color: 1
Size: 116370 Color: 4
Size: 143574 Color: 2

Bin 7: 1727 of cap free
Amount of items: 2
Items: 
Size: 562217 Color: 3
Size: 436057 Color: 0

Bin 8: 1957 of cap free
Amount of items: 2
Items: 
Size: 527651 Color: 0
Size: 470393 Color: 1

Bin 9: 2382 of cap free
Amount of items: 2
Items: 
Size: 744169 Color: 4
Size: 253450 Color: 2

Bin 10: 3123 of cap free
Amount of items: 2
Items: 
Size: 749451 Color: 2
Size: 247427 Color: 3

Bin 11: 3446 of cap free
Amount of items: 2
Items: 
Size: 573703 Color: 0
Size: 422852 Color: 3

Bin 12: 3502 of cap free
Amount of items: 2
Items: 
Size: 661811 Color: 4
Size: 334688 Color: 0

Bin 13: 3957 of cap free
Amount of items: 2
Items: 
Size: 789120 Color: 4
Size: 206924 Color: 0

Bin 14: 4152 of cap free
Amount of items: 2
Items: 
Size: 605454 Color: 1
Size: 390395 Color: 3

Bin 15: 4739 of cap free
Amount of items: 2
Items: 
Size: 498057 Color: 0
Size: 497205 Color: 3

Bin 16: 4992 of cap free
Amount of items: 2
Items: 
Size: 719951 Color: 2
Size: 275058 Color: 4

Bin 17: 5372 of cap free
Amount of items: 2
Items: 
Size: 548661 Color: 3
Size: 445968 Color: 0

Bin 18: 5513 of cap free
Amount of items: 2
Items: 
Size: 611905 Color: 4
Size: 382583 Color: 3

Bin 19: 5578 of cap free
Amount of items: 2
Items: 
Size: 597420 Color: 4
Size: 397003 Color: 2

Bin 20: 6401 of cap free
Amount of items: 2
Items: 
Size: 760657 Color: 4
Size: 232943 Color: 0

Bin 21: 6928 of cap free
Amount of items: 2
Items: 
Size: 559486 Color: 2
Size: 433587 Color: 3

Bin 22: 6984 of cap free
Amount of items: 2
Items: 
Size: 721163 Color: 4
Size: 271854 Color: 0

Bin 23: 7560 of cap free
Amount of items: 2
Items: 
Size: 709485 Color: 1
Size: 282956 Color: 2

Bin 24: 7625 of cap free
Amount of items: 3
Items: 
Size: 681106 Color: 3
Size: 158736 Color: 0
Size: 152534 Color: 0

Bin 25: 7933 of cap free
Amount of items: 2
Items: 
Size: 573013 Color: 0
Size: 419055 Color: 2

Bin 26: 10924 of cap free
Amount of items: 2
Items: 
Size: 769960 Color: 2
Size: 219117 Color: 3

Bin 27: 11141 of cap free
Amount of items: 2
Items: 
Size: 593017 Color: 3
Size: 395843 Color: 2

Bin 28: 13136 of cap free
Amount of items: 2
Items: 
Size: 507675 Color: 4
Size: 479190 Color: 0

Bin 29: 20302 of cap free
Amount of items: 2
Items: 
Size: 767211 Color: 1
Size: 212488 Color: 3

Bin 30: 20575 of cap free
Amount of items: 2
Items: 
Size: 568622 Color: 3
Size: 410804 Color: 4

Bin 31: 22215 of cap free
Amount of items: 2
Items: 
Size: 707605 Color: 2
Size: 270181 Color: 0

Bin 32: 22224 of cap free
Amount of items: 2
Items: 
Size: 567329 Color: 0
Size: 410448 Color: 3

Bin 33: 23443 of cap free
Amount of items: 2
Items: 
Size: 671853 Color: 2
Size: 304705 Color: 1

Bin 34: 31452 of cap free
Amount of items: 2
Items: 
Size: 638122 Color: 0
Size: 330427 Color: 1

Bin 35: 36893 of cap free
Amount of items: 2
Items: 
Size: 489743 Color: 4
Size: 473365 Color: 1

Bin 36: 53472 of cap free
Amount of items: 2
Items: 
Size: 737308 Color: 2
Size: 209221 Color: 4

Bin 37: 59586 of cap free
Amount of items: 2
Items: 
Size: 731441 Color: 2
Size: 208974 Color: 4

Bin 38: 64186 of cap free
Amount of items: 2
Items: 
Size: 640123 Color: 4
Size: 295692 Color: 1

Bin 39: 66777 of cap free
Amount of items: 2
Items: 
Size: 467105 Color: 2
Size: 466119 Color: 3

Bin 40: 74527 of cap free
Amount of items: 2
Items: 
Size: 732007 Color: 4
Size: 193467 Color: 1

Bin 41: 74748 of cap free
Amount of items: 2
Items: 
Size: 731889 Color: 4
Size: 193364 Color: 3

Bin 42: 79795 of cap free
Amount of items: 2
Items: 
Size: 730757 Color: 1
Size: 189449 Color: 2

Bin 43: 91978 of cap free
Amount of items: 2
Items: 
Size: 465505 Color: 3
Size: 442518 Color: 0

Bin 44: 139424 of cap free
Amount of items: 2
Items: 
Size: 690842 Color: 1
Size: 169735 Color: 2

Bin 45: 140893 of cap free
Amount of items: 2
Items: 
Size: 690401 Color: 1
Size: 168707 Color: 2

Bin 46: 326144 of cap free
Amount of items: 1
Items: 
Size: 673857 Color: 1

Bin 47: 364074 of cap free
Amount of items: 1
Items: 
Size: 635927 Color: 2

Bin 48: 371339 of cap free
Amount of items: 1
Items: 
Size: 628662 Color: 0

Bin 49: 371425 of cap free
Amount of items: 1
Items: 
Size: 628576 Color: 4

Bin 50: 378541 of cap free
Amount of items: 1
Items: 
Size: 621460 Color: 4

Bin 51: 382105 of cap free
Amount of items: 1
Items: 
Size: 617896 Color: 1

Bin 52: 389563 of cap free
Amount of items: 1
Items: 
Size: 610438 Color: 2

Bin 53: 399640 of cap free
Amount of items: 1
Items: 
Size: 600361 Color: 2

Bin 54: 438855 of cap free
Amount of items: 1
Items: 
Size: 561146 Color: 3

Bin 55: 542540 of cap free
Amount of items: 1
Items: 
Size: 457461 Color: 3

Total size: 49877727
Total free space: 5122328

