Capicity Bin: 1000001
Lower Bound: 44

Bins used: 45
Amount of Colors: 5

Bin 1: 2 of cap free
Amount of items: 2
Items: 
Size: 657970 Color: 1
Size: 342029 Color: 0

Bin 2: 55 of cap free
Amount of items: 3
Items: 
Size: 706899 Color: 1
Size: 191634 Color: 0
Size: 101413 Color: 4

Bin 3: 185 of cap free
Amount of items: 3
Items: 
Size: 564731 Color: 1
Size: 251715 Color: 2
Size: 183370 Color: 0

Bin 4: 214 of cap free
Amount of items: 2
Items: 
Size: 608745 Color: 0
Size: 391042 Color: 4

Bin 5: 338 of cap free
Amount of items: 2
Items: 
Size: 575367 Color: 4
Size: 424296 Color: 3

Bin 6: 437 of cap free
Amount of items: 2
Items: 
Size: 539215 Color: 1
Size: 460349 Color: 4

Bin 7: 567 of cap free
Amount of items: 3
Items: 
Size: 754193 Color: 3
Size: 126396 Color: 4
Size: 118845 Color: 4

Bin 8: 571 of cap free
Amount of items: 3
Items: 
Size: 698250 Color: 3
Size: 154149 Color: 2
Size: 147031 Color: 0

Bin 9: 599 of cap free
Amount of items: 3
Items: 
Size: 701862 Color: 2
Size: 149551 Color: 3
Size: 147989 Color: 1

Bin 10: 716 of cap free
Amount of items: 2
Items: 
Size: 783990 Color: 4
Size: 215295 Color: 2

Bin 11: 759 of cap free
Amount of items: 2
Items: 
Size: 712025 Color: 2
Size: 287217 Color: 1

Bin 12: 860 of cap free
Amount of items: 3
Items: 
Size: 776927 Color: 2
Size: 112501 Color: 0
Size: 109713 Color: 4

Bin 13: 1278 of cap free
Amount of items: 2
Items: 
Size: 672290 Color: 1
Size: 326433 Color: 2

Bin 14: 1294 of cap free
Amount of items: 3
Items: 
Size: 644538 Color: 1
Size: 186930 Color: 3
Size: 167239 Color: 2

Bin 15: 1366 of cap free
Amount of items: 3
Items: 
Size: 736370 Color: 1
Size: 133223 Color: 3
Size: 129042 Color: 4

Bin 16: 1595 of cap free
Amount of items: 3
Items: 
Size: 719092 Color: 0
Size: 146257 Color: 1
Size: 133057 Color: 0

Bin 17: 1708 of cap free
Amount of items: 2
Items: 
Size: 695780 Color: 1
Size: 302513 Color: 4

Bin 18: 1946 of cap free
Amount of items: 2
Items: 
Size: 507224 Color: 1
Size: 490831 Color: 3

Bin 19: 2361 of cap free
Amount of items: 2
Items: 
Size: 547495 Color: 0
Size: 450145 Color: 2

Bin 20: 2693 of cap free
Amount of items: 2
Items: 
Size: 778569 Color: 4
Size: 218739 Color: 3

Bin 21: 2705 of cap free
Amount of items: 2
Items: 
Size: 635483 Color: 2
Size: 361813 Color: 3

Bin 22: 2923 of cap free
Amount of items: 2
Items: 
Size: 601715 Color: 4
Size: 395363 Color: 1

Bin 23: 3032 of cap free
Amount of items: 2
Items: 
Size: 558757 Color: 3
Size: 438212 Color: 1

Bin 24: 4023 of cap free
Amount of items: 2
Items: 
Size: 627082 Color: 0
Size: 368896 Color: 1

Bin 25: 4567 of cap free
Amount of items: 2
Items: 
Size: 512541 Color: 4
Size: 482893 Color: 0

Bin 26: 4625 of cap free
Amount of items: 2
Items: 
Size: 654993 Color: 2
Size: 340383 Color: 4

Bin 27: 4732 of cap free
Amount of items: 3
Items: 
Size: 791019 Color: 0
Size: 102688 Color: 2
Size: 101562 Color: 0

Bin 28: 7666 of cap free
Amount of items: 3
Items: 
Size: 687811 Color: 0
Size: 151905 Color: 4
Size: 152619 Color: 2

Bin 29: 9254 of cap free
Amount of items: 2
Items: 
Size: 558464 Color: 4
Size: 432283 Color: 1

Bin 30: 9650 of cap free
Amount of items: 2
Items: 
Size: 705431 Color: 1
Size: 284920 Color: 0

Bin 31: 11822 of cap free
Amount of items: 2
Items: 
Size: 526656 Color: 2
Size: 461523 Color: 0

Bin 32: 13022 of cap free
Amount of items: 2
Items: 
Size: 500653 Color: 1
Size: 486326 Color: 4

Bin 33: 14302 of cap free
Amount of items: 2
Items: 
Size: 597984 Color: 3
Size: 387715 Color: 0

Bin 34: 15087 of cap free
Amount of items: 2
Items: 
Size: 603262 Color: 0
Size: 381652 Color: 3

Bin 35: 18169 of cap free
Amount of items: 2
Items: 
Size: 518293 Color: 3
Size: 463539 Color: 2

Bin 36: 22011 of cap free
Amount of items: 2
Items: 
Size: 519131 Color: 2
Size: 458859 Color: 0

Bin 37: 24489 of cap free
Amount of items: 2
Items: 
Size: 545585 Color: 1
Size: 429927 Color: 3

Bin 38: 24762 of cap free
Amount of items: 3
Items: 
Size: 596500 Color: 2
Size: 195029 Color: 4
Size: 183710 Color: 4

Bin 39: 33458 of cap free
Amount of items: 2
Items: 
Size: 686068 Color: 3
Size: 280475 Color: 0

Bin 40: 67037 of cap free
Amount of items: 2
Items: 
Size: 518019 Color: 0
Size: 414945 Color: 1

Bin 41: 82636 of cap free
Amount of items: 2
Items: 
Size: 664875 Color: 0
Size: 252490 Color: 2

Bin 42: 91537 of cap free
Amount of items: 2
Items: 
Size: 678802 Color: 2
Size: 229662 Color: 0

Bin 43: 94995 of cap free
Amount of items: 2
Items: 
Size: 538780 Color: 1
Size: 366226 Color: 4

Bin 44: 132290 of cap free
Amount of items: 2
Items: 
Size: 531195 Color: 1
Size: 336516 Color: 0

Bin 45: 362818 of cap free
Amount of items: 2
Items: 
Size: 412730 Color: 1
Size: 224453 Color: 0

Total size: 43918889
Total free space: 1081156

