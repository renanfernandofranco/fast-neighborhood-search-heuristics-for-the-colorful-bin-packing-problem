Capicity Bin: 1000001
Lower Bound: 220

Bins used: 223
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 778597 Color: 3
Size: 221404 Color: 1

Bin 2: 9 of cap free
Amount of items: 3
Items: 
Size: 554285 Color: 1
Size: 224533 Color: 0
Size: 221174 Color: 2

Bin 3: 12 of cap free
Amount of items: 3
Items: 
Size: 758072 Color: 3
Size: 122790 Color: 4
Size: 119127 Color: 0

Bin 4: 14 of cap free
Amount of items: 3
Items: 
Size: 704220 Color: 4
Size: 148226 Color: 2
Size: 147541 Color: 4

Bin 5: 20 of cap free
Amount of items: 2
Items: 
Size: 680038 Color: 1
Size: 319943 Color: 3

Bin 6: 24 of cap free
Amount of items: 2
Items: 
Size: 576757 Color: 2
Size: 423220 Color: 4

Bin 7: 25 of cap free
Amount of items: 2
Items: 
Size: 576655 Color: 1
Size: 423321 Color: 0

Bin 8: 30 of cap free
Amount of items: 3
Items: 
Size: 791611 Color: 3
Size: 105571 Color: 4
Size: 102789 Color: 4

Bin 9: 46 of cap free
Amount of items: 3
Items: 
Size: 736209 Color: 4
Size: 134931 Color: 1
Size: 128815 Color: 0

Bin 10: 47 of cap free
Amount of items: 2
Items: 
Size: 574876 Color: 4
Size: 425078 Color: 2

Bin 11: 48 of cap free
Amount of items: 2
Items: 
Size: 797549 Color: 1
Size: 202404 Color: 3

Bin 12: 55 of cap free
Amount of items: 3
Items: 
Size: 706968 Color: 2
Size: 148213 Color: 0
Size: 144765 Color: 0

Bin 13: 59 of cap free
Amount of items: 2
Items: 
Size: 729031 Color: 0
Size: 270911 Color: 1

Bin 14: 74 of cap free
Amount of items: 3
Items: 
Size: 348497 Color: 1
Size: 329551 Color: 3
Size: 321879 Color: 3

Bin 15: 75 of cap free
Amount of items: 3
Items: 
Size: 678991 Color: 0
Size: 163528 Color: 4
Size: 157407 Color: 4

Bin 16: 81 of cap free
Amount of items: 3
Items: 
Size: 667007 Color: 1
Size: 171136 Color: 0
Size: 161777 Color: 0

Bin 17: 83 of cap free
Amount of items: 3
Items: 
Size: 706876 Color: 4
Size: 149540 Color: 0
Size: 143502 Color: 3

Bin 18: 89 of cap free
Amount of items: 3
Items: 
Size: 766456 Color: 0
Size: 118992 Color: 2
Size: 114464 Color: 4

Bin 19: 96 of cap free
Amount of items: 3
Items: 
Size: 347835 Color: 0
Size: 329063 Color: 1
Size: 323007 Color: 4

Bin 20: 97 of cap free
Amount of items: 3
Items: 
Size: 612001 Color: 2
Size: 201576 Color: 0
Size: 186327 Color: 4

Bin 21: 98 of cap free
Amount of items: 2
Items: 
Size: 651896 Color: 4
Size: 348007 Color: 2

Bin 22: 100 of cap free
Amount of items: 2
Items: 
Size: 574171 Color: 4
Size: 425730 Color: 1

Bin 23: 103 of cap free
Amount of items: 3
Items: 
Size: 595451 Color: 4
Size: 218163 Color: 2
Size: 186284 Color: 3

Bin 24: 115 of cap free
Amount of items: 2
Items: 
Size: 538795 Color: 4
Size: 461091 Color: 1

Bin 25: 125 of cap free
Amount of items: 2
Items: 
Size: 584266 Color: 2
Size: 415610 Color: 3

Bin 26: 130 of cap free
Amount of items: 3
Items: 
Size: 785112 Color: 0
Size: 109085 Color: 4
Size: 105674 Color: 0

Bin 27: 132 of cap free
Amount of items: 3
Items: 
Size: 701080 Color: 2
Size: 153753 Color: 1
Size: 145036 Color: 1

Bin 28: 132 of cap free
Amount of items: 2
Items: 
Size: 731395 Color: 1
Size: 268474 Color: 3

Bin 29: 135 of cap free
Amount of items: 2
Items: 
Size: 608423 Color: 3
Size: 391443 Color: 1

Bin 30: 135 of cap free
Amount of items: 2
Items: 
Size: 791829 Color: 0
Size: 208037 Color: 2

Bin 31: 146 of cap free
Amount of items: 2
Items: 
Size: 514580 Color: 4
Size: 485275 Color: 3

Bin 32: 152 of cap free
Amount of items: 3
Items: 
Size: 721380 Color: 0
Size: 142851 Color: 1
Size: 135618 Color: 2

Bin 33: 161 of cap free
Amount of items: 3
Items: 
Size: 701281 Color: 0
Size: 154941 Color: 2
Size: 143618 Color: 1

Bin 34: 178 of cap free
Amount of items: 2
Items: 
Size: 682000 Color: 4
Size: 317823 Color: 3

Bin 35: 188 of cap free
Amount of items: 2
Items: 
Size: 720082 Color: 3
Size: 279731 Color: 1

Bin 36: 189 of cap free
Amount of items: 2
Items: 
Size: 741940 Color: 1
Size: 257872 Color: 4

Bin 37: 194 of cap free
Amount of items: 2
Items: 
Size: 767321 Color: 1
Size: 232486 Color: 2

Bin 38: 197 of cap free
Amount of items: 2
Items: 
Size: 797465 Color: 1
Size: 202339 Color: 3

Bin 39: 210 of cap free
Amount of items: 2
Items: 
Size: 649862 Color: 3
Size: 349929 Color: 0

Bin 40: 213 of cap free
Amount of items: 2
Items: 
Size: 759838 Color: 3
Size: 239950 Color: 1

Bin 41: 214 of cap free
Amount of items: 2
Items: 
Size: 526754 Color: 0
Size: 473033 Color: 4

Bin 42: 215 of cap free
Amount of items: 2
Items: 
Size: 766629 Color: 2
Size: 233157 Color: 1

Bin 43: 221 of cap free
Amount of items: 3
Items: 
Size: 644599 Color: 2
Size: 180293 Color: 1
Size: 174888 Color: 4

Bin 44: 225 of cap free
Amount of items: 2
Items: 
Size: 605138 Color: 3
Size: 394638 Color: 4

Bin 45: 227 of cap free
Amount of items: 2
Items: 
Size: 522404 Color: 0
Size: 477370 Color: 2

Bin 46: 234 of cap free
Amount of items: 2
Items: 
Size: 690684 Color: 4
Size: 309083 Color: 1

Bin 47: 236 of cap free
Amount of items: 2
Items: 
Size: 780022 Color: 2
Size: 219743 Color: 3

Bin 48: 240 of cap free
Amount of items: 3
Items: 
Size: 541201 Color: 4
Size: 234097 Color: 2
Size: 224463 Color: 3

Bin 49: 246 of cap free
Amount of items: 3
Items: 
Size: 748978 Color: 0
Size: 125649 Color: 3
Size: 125128 Color: 1

Bin 50: 247 of cap free
Amount of items: 3
Items: 
Size: 794827 Color: 4
Size: 102819 Color: 0
Size: 102108 Color: 3

Bin 51: 253 of cap free
Amount of items: 2
Items: 
Size: 719723 Color: 2
Size: 280025 Color: 1

Bin 52: 266 of cap free
Amount of items: 2
Items: 
Size: 585744 Color: 1
Size: 413991 Color: 0

Bin 53: 267 of cap free
Amount of items: 3
Items: 
Size: 737832 Color: 2
Size: 135814 Color: 3
Size: 126088 Color: 0

Bin 54: 288 of cap free
Amount of items: 3
Items: 
Size: 721014 Color: 1
Size: 141125 Color: 4
Size: 137574 Color: 0

Bin 55: 309 of cap free
Amount of items: 2
Items: 
Size: 706282 Color: 0
Size: 293410 Color: 2

Bin 56: 310 of cap free
Amount of items: 3
Items: 
Size: 702591 Color: 4
Size: 152443 Color: 3
Size: 144657 Color: 1

Bin 57: 339 of cap free
Amount of items: 2
Items: 
Size: 645307 Color: 2
Size: 354355 Color: 4

Bin 58: 346 of cap free
Amount of items: 2
Items: 
Size: 777467 Color: 2
Size: 222188 Color: 4

Bin 59: 355 of cap free
Amount of items: 2
Items: 
Size: 661164 Color: 0
Size: 338482 Color: 3

Bin 60: 365 of cap free
Amount of items: 2
Items: 
Size: 771554 Color: 3
Size: 228082 Color: 4

Bin 61: 369 of cap free
Amount of items: 2
Items: 
Size: 561222 Color: 1
Size: 438410 Color: 3

Bin 62: 371 of cap free
Amount of items: 2
Items: 
Size: 759196 Color: 4
Size: 240434 Color: 3

Bin 63: 378 of cap free
Amount of items: 3
Items: 
Size: 346730 Color: 3
Size: 331073 Color: 0
Size: 321820 Color: 4

Bin 64: 389 of cap free
Amount of items: 2
Items: 
Size: 629656 Color: 4
Size: 369956 Color: 1

Bin 65: 398 of cap free
Amount of items: 2
Items: 
Size: 560011 Color: 2
Size: 439592 Color: 4

Bin 66: 402 of cap free
Amount of items: 2
Items: 
Size: 519306 Color: 4
Size: 480293 Color: 1

Bin 67: 406 of cap free
Amount of items: 2
Items: 
Size: 612384 Color: 3
Size: 387211 Color: 4

Bin 68: 445 of cap free
Amount of items: 2
Items: 
Size: 582593 Color: 0
Size: 416963 Color: 1

Bin 69: 452 of cap free
Amount of items: 2
Items: 
Size: 572275 Color: 3
Size: 427274 Color: 0

Bin 70: 460 of cap free
Amount of items: 2
Items: 
Size: 761631 Color: 3
Size: 237910 Color: 2

Bin 71: 479 of cap free
Amount of items: 3
Items: 
Size: 551620 Color: 2
Size: 224688 Color: 4
Size: 223214 Color: 0

Bin 72: 488 of cap free
Amount of items: 2
Items: 
Size: 594479 Color: 1
Size: 405034 Color: 4

Bin 73: 493 of cap free
Amount of items: 2
Items: 
Size: 536215 Color: 0
Size: 463293 Color: 1

Bin 74: 498 of cap free
Amount of items: 3
Items: 
Size: 605993 Color: 2
Size: 202762 Color: 3
Size: 190748 Color: 3

Bin 75: 515 of cap free
Amount of items: 2
Items: 
Size: 778121 Color: 1
Size: 221365 Color: 0

Bin 76: 522 of cap free
Amount of items: 3
Items: 
Size: 765060 Color: 1
Size: 124122 Color: 2
Size: 110297 Color: 2

Bin 77: 552 of cap free
Amount of items: 2
Items: 
Size: 685168 Color: 1
Size: 314281 Color: 3

Bin 78: 552 of cap free
Amount of items: 3
Items: 
Size: 736529 Color: 1
Size: 130301 Color: 2
Size: 132619 Color: 1

Bin 79: 567 of cap free
Amount of items: 3
Items: 
Size: 658585 Color: 1
Size: 174122 Color: 2
Size: 166727 Color: 2

Bin 80: 576 of cap free
Amount of items: 2
Items: 
Size: 608179 Color: 2
Size: 391246 Color: 3

Bin 81: 593 of cap free
Amount of items: 2
Items: 
Size: 762999 Color: 1
Size: 236409 Color: 3

Bin 82: 600 of cap free
Amount of items: 3
Items: 
Size: 597495 Color: 3
Size: 201568 Color: 2
Size: 200338 Color: 1

Bin 83: 618 of cap free
Amount of items: 3
Items: 
Size: 645012 Color: 4
Size: 178460 Color: 2
Size: 175911 Color: 2

Bin 84: 632 of cap free
Amount of items: 2
Items: 
Size: 767760 Color: 3
Size: 231609 Color: 4

Bin 85: 644 of cap free
Amount of items: 2
Items: 
Size: 601124 Color: 2
Size: 398233 Color: 1

Bin 86: 656 of cap free
Amount of items: 2
Items: 
Size: 738515 Color: 1
Size: 260830 Color: 2

Bin 87: 663 of cap free
Amount of items: 2
Items: 
Size: 662107 Color: 3
Size: 337231 Color: 1

Bin 88: 691 of cap free
Amount of items: 2
Items: 
Size: 590542 Color: 4
Size: 408768 Color: 0

Bin 89: 692 of cap free
Amount of items: 2
Items: 
Size: 581336 Color: 4
Size: 417973 Color: 1

Bin 90: 729 of cap free
Amount of items: 2
Items: 
Size: 798811 Color: 2
Size: 200461 Color: 4

Bin 91: 750 of cap free
Amount of items: 2
Items: 
Size: 720481 Color: 3
Size: 278770 Color: 1

Bin 92: 751 of cap free
Amount of items: 2
Items: 
Size: 713617 Color: 3
Size: 285633 Color: 0

Bin 93: 757 of cap free
Amount of items: 2
Items: 
Size: 694840 Color: 4
Size: 304404 Color: 3

Bin 94: 788 of cap free
Amount of items: 2
Items: 
Size: 530602 Color: 1
Size: 468611 Color: 3

Bin 95: 788 of cap free
Amount of items: 2
Items: 
Size: 662916 Color: 3
Size: 336297 Color: 1

Bin 96: 843 of cap free
Amount of items: 2
Items: 
Size: 529409 Color: 2
Size: 469749 Color: 0

Bin 97: 844 of cap free
Amount of items: 2
Items: 
Size: 534725 Color: 1
Size: 464432 Color: 3

Bin 98: 860 of cap free
Amount of items: 2
Items: 
Size: 733580 Color: 4
Size: 265561 Color: 0

Bin 99: 893 of cap free
Amount of items: 3
Items: 
Size: 734794 Color: 2
Size: 136276 Color: 1
Size: 128038 Color: 0

Bin 100: 893 of cap free
Amount of items: 2
Items: 
Size: 755944 Color: 0
Size: 243164 Color: 4

Bin 101: 913 of cap free
Amount of items: 3
Items: 
Size: 371068 Color: 1
Size: 332316 Color: 0
Size: 295704 Color: 4

Bin 102: 946 of cap free
Amount of items: 3
Items: 
Size: 642966 Color: 0
Size: 182536 Color: 2
Size: 173553 Color: 4

Bin 103: 951 of cap free
Amount of items: 2
Items: 
Size: 717920 Color: 2
Size: 281130 Color: 4

Bin 104: 959 of cap free
Amount of items: 2
Items: 
Size: 731391 Color: 1
Size: 267651 Color: 3

Bin 105: 991 of cap free
Amount of items: 3
Items: 
Size: 460303 Color: 0
Size: 269077 Color: 1
Size: 269630 Color: 3

Bin 106: 1003 of cap free
Amount of items: 3
Items: 
Size: 604307 Color: 1
Size: 201854 Color: 3
Size: 192837 Color: 4

Bin 107: 1010 of cap free
Amount of items: 2
Items: 
Size: 758358 Color: 0
Size: 240633 Color: 4

Bin 108: 1030 of cap free
Amount of items: 2
Items: 
Size: 567106 Color: 0
Size: 431865 Color: 2

Bin 109: 1033 of cap free
Amount of items: 3
Items: 
Size: 618542 Color: 2
Size: 191081 Color: 4
Size: 189345 Color: 1

Bin 110: 1046 of cap free
Amount of items: 2
Items: 
Size: 794145 Color: 1
Size: 204810 Color: 2

Bin 111: 1131 of cap free
Amount of items: 3
Items: 
Size: 779711 Color: 4
Size: 110811 Color: 0
Size: 108348 Color: 2

Bin 112: 1175 of cap free
Amount of items: 2
Items: 
Size: 574493 Color: 2
Size: 424333 Color: 3

Bin 113: 1176 of cap free
Amount of items: 2
Items: 
Size: 600708 Color: 3
Size: 398117 Color: 2

Bin 114: 1189 of cap free
Amount of items: 2
Items: 
Size: 530194 Color: 0
Size: 468618 Color: 1

Bin 115: 1194 of cap free
Amount of items: 2
Items: 
Size: 627986 Color: 1
Size: 370821 Color: 0

Bin 116: 1239 of cap free
Amount of items: 2
Items: 
Size: 598684 Color: 2
Size: 400078 Color: 1

Bin 117: 1252 of cap free
Amount of items: 2
Items: 
Size: 575751 Color: 0
Size: 422998 Color: 4

Bin 118: 1284 of cap free
Amount of items: 2
Items: 
Size: 568819 Color: 2
Size: 429898 Color: 0

Bin 119: 1330 of cap free
Amount of items: 2
Items: 
Size: 596692 Color: 0
Size: 401979 Color: 3

Bin 120: 1370 of cap free
Amount of items: 3
Items: 
Size: 715794 Color: 3
Size: 142216 Color: 2
Size: 140621 Color: 0

Bin 121: 1386 of cap free
Amount of items: 3
Items: 
Size: 685799 Color: 4
Size: 156469 Color: 0
Size: 156347 Color: 3

Bin 122: 1389 of cap free
Amount of items: 3
Items: 
Size: 518728 Color: 3
Size: 253653 Color: 0
Size: 226231 Color: 4

Bin 123: 1453 of cap free
Amount of items: 2
Items: 
Size: 752643 Color: 1
Size: 245905 Color: 0

Bin 124: 1568 of cap free
Amount of items: 2
Items: 
Size: 625218 Color: 4
Size: 373215 Color: 3

Bin 125: 1581 of cap free
Amount of items: 3
Items: 
Size: 664277 Color: 0
Size: 169749 Color: 4
Size: 164394 Color: 1

Bin 126: 1648 of cap free
Amount of items: 2
Items: 
Size: 516054 Color: 1
Size: 482299 Color: 4

Bin 127: 1669 of cap free
Amount of items: 2
Items: 
Size: 617114 Color: 1
Size: 381218 Color: 2

Bin 128: 1688 of cap free
Amount of items: 2
Items: 
Size: 727459 Color: 0
Size: 270854 Color: 3

Bin 129: 1700 of cap free
Amount of items: 3
Items: 
Size: 504255 Color: 4
Size: 259576 Color: 2
Size: 234470 Color: 1

Bin 130: 1738 of cap free
Amount of items: 2
Items: 
Size: 755119 Color: 1
Size: 243144 Color: 2

Bin 131: 1765 of cap free
Amount of items: 2
Items: 
Size: 692569 Color: 0
Size: 305667 Color: 4

Bin 132: 1791 of cap free
Amount of items: 3
Items: 
Size: 451853 Color: 4
Size: 275718 Color: 1
Size: 270639 Color: 3

Bin 133: 1813 of cap free
Amount of items: 2
Items: 
Size: 550318 Color: 1
Size: 447870 Color: 4

Bin 134: 1887 of cap free
Amount of items: 2
Items: 
Size: 532544 Color: 4
Size: 465570 Color: 0

Bin 135: 1894 of cap free
Amount of items: 2
Items: 
Size: 724338 Color: 4
Size: 273769 Color: 2

Bin 136: 2038 of cap free
Amount of items: 2
Items: 
Size: 749806 Color: 3
Size: 248157 Color: 2

Bin 137: 2041 of cap free
Amount of items: 2
Items: 
Size: 688593 Color: 3
Size: 309367 Color: 4

Bin 138: 2058 of cap free
Amount of items: 2
Items: 
Size: 595118 Color: 3
Size: 402825 Color: 0

Bin 139: 2080 of cap free
Amount of items: 2
Items: 
Size: 553495 Color: 3
Size: 444426 Color: 1

Bin 140: 2083 of cap free
Amount of items: 2
Items: 
Size: 557592 Color: 4
Size: 440326 Color: 3

Bin 141: 2166 of cap free
Amount of items: 2
Items: 
Size: 656201 Color: 1
Size: 341634 Color: 4

Bin 142: 2225 of cap free
Amount of items: 3
Items: 
Size: 487737 Color: 4
Size: 256835 Color: 1
Size: 253204 Color: 0

Bin 143: 2276 of cap free
Amount of items: 2
Items: 
Size: 679409 Color: 0
Size: 318316 Color: 1

Bin 144: 2316 of cap free
Amount of items: 2
Items: 
Size: 642294 Color: 1
Size: 355391 Color: 2

Bin 145: 2332 of cap free
Amount of items: 3
Items: 
Size: 701077 Color: 1
Size: 150758 Color: 3
Size: 145834 Color: 2

Bin 146: 2362 of cap free
Amount of items: 2
Items: 
Size: 514904 Color: 4
Size: 482735 Color: 1

Bin 147: 2463 of cap free
Amount of items: 3
Items: 
Size: 566297 Color: 0
Size: 221440 Color: 1
Size: 209801 Color: 4

Bin 148: 2467 of cap free
Amount of items: 2
Items: 
Size: 783690 Color: 3
Size: 213844 Color: 4

Bin 149: 2474 of cap free
Amount of items: 2
Items: 
Size: 532389 Color: 4
Size: 465138 Color: 1

Bin 150: 2502 of cap free
Amount of items: 2
Items: 
Size: 540761 Color: 3
Size: 456738 Color: 2

Bin 151: 2597 of cap free
Amount of items: 2
Items: 
Size: 790667 Color: 3
Size: 206737 Color: 0

Bin 152: 2609 of cap free
Amount of items: 2
Items: 
Size: 733766 Color: 0
Size: 263626 Color: 4

Bin 153: 2725 of cap free
Amount of items: 2
Items: 
Size: 693680 Color: 4
Size: 303596 Color: 3

Bin 154: 2887 of cap free
Amount of items: 2
Items: 
Size: 503941 Color: 2
Size: 493173 Color: 1

Bin 155: 2954 of cap free
Amount of items: 2
Items: 
Size: 696739 Color: 4
Size: 300308 Color: 2

Bin 156: 2956 of cap free
Amount of items: 3
Items: 
Size: 533281 Color: 1
Size: 233552 Color: 0
Size: 230212 Color: 0

Bin 157: 3012 of cap free
Amount of items: 2
Items: 
Size: 662716 Color: 1
Size: 334273 Color: 0

Bin 158: 3033 of cap free
Amount of items: 2
Items: 
Size: 678917 Color: 4
Size: 318051 Color: 3

Bin 159: 3037 of cap free
Amount of items: 2
Items: 
Size: 524836 Color: 0
Size: 472128 Color: 3

Bin 160: 3176 of cap free
Amount of items: 2
Items: 
Size: 503480 Color: 4
Size: 493345 Color: 2

Bin 161: 3257 of cap free
Amount of items: 2
Items: 
Size: 550012 Color: 2
Size: 446732 Color: 3

Bin 162: 3416 of cap free
Amount of items: 2
Items: 
Size: 693017 Color: 4
Size: 303568 Color: 2

Bin 163: 3541 of cap free
Amount of items: 2
Items: 
Size: 616696 Color: 0
Size: 379764 Color: 2

Bin 164: 3647 of cap free
Amount of items: 2
Items: 
Size: 718132 Color: 3
Size: 278222 Color: 2

Bin 165: 3708 of cap free
Amount of items: 2
Items: 
Size: 798506 Color: 1
Size: 197787 Color: 3

Bin 166: 3779 of cap free
Amount of items: 3
Items: 
Size: 489373 Color: 1
Size: 253479 Color: 2
Size: 253370 Color: 3

Bin 167: 3990 of cap free
Amount of items: 2
Items: 
Size: 791308 Color: 0
Size: 204703 Color: 1

Bin 168: 4045 of cap free
Amount of items: 3
Items: 
Size: 347640 Color: 4
Size: 324173 Color: 3
Size: 324143 Color: 1

Bin 169: 4141 of cap free
Amount of items: 2
Items: 
Size: 623947 Color: 4
Size: 371913 Color: 1

Bin 170: 4193 of cap free
Amount of items: 2
Items: 
Size: 528037 Color: 3
Size: 467771 Color: 0

Bin 171: 4217 of cap free
Amount of items: 2
Items: 
Size: 699727 Color: 1
Size: 296057 Color: 3

Bin 172: 4487 of cap free
Amount of items: 2
Items: 
Size: 502529 Color: 2
Size: 492985 Color: 0

Bin 173: 4514 of cap free
Amount of items: 3
Items: 
Size: 345865 Color: 1
Size: 325639 Color: 4
Size: 323983 Color: 0

Bin 174: 4515 of cap free
Amount of items: 2
Items: 
Size: 575261 Color: 3
Size: 420225 Color: 1

Bin 175: 4563 of cap free
Amount of items: 2
Items: 
Size: 732983 Color: 0
Size: 262455 Color: 2

Bin 176: 4767 of cap free
Amount of items: 3
Items: 
Size: 444203 Color: 2
Size: 278424 Color: 3
Size: 272607 Color: 2

Bin 177: 4768 of cap free
Amount of items: 2
Items: 
Size: 781875 Color: 3
Size: 213358 Color: 0

Bin 178: 5206 of cap free
Amount of items: 2
Items: 
Size: 594157 Color: 3
Size: 400638 Color: 2

Bin 179: 5372 of cap free
Amount of items: 2
Items: 
Size: 678910 Color: 3
Size: 315719 Color: 2

Bin 180: 5651 of cap free
Amount of items: 2
Items: 
Size: 655606 Color: 2
Size: 338744 Color: 0

Bin 181: 5698 of cap free
Amount of items: 2
Items: 
Size: 500644 Color: 1
Size: 493659 Color: 2

Bin 182: 6285 of cap free
Amount of items: 2
Items: 
Size: 790661 Color: 4
Size: 203055 Color: 3

Bin 183: 7397 of cap free
Amount of items: 2
Items: 
Size: 565779 Color: 4
Size: 426825 Color: 2

Bin 184: 7577 of cap free
Amount of items: 2
Items: 
Size: 752575 Color: 0
Size: 239849 Color: 4

Bin 185: 7653 of cap free
Amount of items: 3
Items: 
Size: 638563 Color: 0
Size: 175881 Color: 4
Size: 177904 Color: 2

Bin 186: 8239 of cap free
Amount of items: 2
Items: 
Size: 638374 Color: 2
Size: 353388 Color: 3

Bin 187: 8379 of cap free
Amount of items: 2
Items: 
Size: 661033 Color: 0
Size: 330589 Color: 2

Bin 188: 9468 of cap free
Amount of items: 2
Items: 
Size: 732914 Color: 2
Size: 257619 Color: 4

Bin 189: 9625 of cap free
Amount of items: 2
Items: 
Size: 637219 Color: 4
Size: 353157 Color: 2

Bin 190: 9839 of cap free
Amount of items: 2
Items: 
Size: 678186 Color: 3
Size: 311976 Color: 4

Bin 191: 10007 of cap free
Amount of items: 2
Items: 
Size: 754919 Color: 4
Size: 235075 Color: 0

Bin 192: 10366 of cap free
Amount of items: 2
Items: 
Size: 636979 Color: 1
Size: 352656 Color: 0

Bin 193: 10459 of cap free
Amount of items: 2
Items: 
Size: 593070 Color: 4
Size: 396472 Color: 1

Bin 194: 10638 of cap free
Amount of items: 2
Items: 
Size: 713094 Color: 3
Size: 276269 Color: 2

Bin 195: 11308 of cap free
Amount of items: 2
Items: 
Size: 524696 Color: 0
Size: 463997 Color: 1

Bin 196: 11894 of cap free
Amount of items: 2
Items: 
Size: 635788 Color: 0
Size: 352319 Color: 1

Bin 197: 14521 of cap free
Amount of items: 2
Items: 
Size: 593352 Color: 1
Size: 392128 Color: 3

Bin 198: 16409 of cap free
Amount of items: 2
Items: 
Size: 631592 Color: 3
Size: 352000 Color: 2

Bin 199: 16446 of cap free
Amount of items: 2
Items: 
Size: 748779 Color: 3
Size: 234776 Color: 1

Bin 200: 17411 of cap free
Amount of items: 2
Items: 
Size: 630716 Color: 3
Size: 351874 Color: 2

Bin 201: 17775 of cap free
Amount of items: 2
Items: 
Size: 593193 Color: 1
Size: 389033 Color: 3

Bin 202: 18599 of cap free
Amount of items: 3
Items: 
Size: 345771 Color: 3
Size: 323926 Color: 1
Size: 311705 Color: 2

Bin 203: 19513 of cap free
Amount of items: 2
Items: 
Size: 491569 Color: 0
Size: 488919 Color: 3

Bin 204: 23472 of cap free
Amount of items: 2
Items: 
Size: 488558 Color: 1
Size: 487971 Color: 3

Bin 205: 25687 of cap free
Amount of items: 2
Items: 
Size: 589550 Color: 3
Size: 384764 Color: 2

Bin 206: 28819 of cap free
Amount of items: 2
Items: 
Size: 486539 Color: 0
Size: 484643 Color: 4

Bin 207: 29155 of cap free
Amount of items: 2
Items: 
Size: 587000 Color: 4
Size: 383846 Color: 0

Bin 208: 34515 of cap free
Amount of items: 2
Items: 
Size: 587052 Color: 0
Size: 378434 Color: 1

Bin 209: 36789 of cap free
Amount of items: 2
Items: 
Size: 585298 Color: 2
Size: 377914 Color: 0

Bin 210: 48838 of cap free
Amount of items: 2
Items: 
Size: 574019 Color: 3
Size: 377144 Color: 2

Bin 211: 53016 of cap free
Amount of items: 2
Items: 
Size: 570740 Color: 2
Size: 376245 Color: 1

Bin 212: 54266 of cap free
Amount of items: 2
Items: 
Size: 482447 Color: 1
Size: 463288 Color: 0

Bin 213: 58353 of cap free
Amount of items: 2
Items: 
Size: 479607 Color: 1
Size: 462041 Color: 4

Bin 214: 75636 of cap free
Amount of items: 2
Items: 
Size: 573338 Color: 1
Size: 351027 Color: 0

Bin 215: 76696 of cap free
Amount of items: 2
Items: 
Size: 461751 Color: 2
Size: 461554 Color: 4

Bin 216: 85019 of cap free
Amount of items: 2
Items: 
Size: 460202 Color: 2
Size: 454780 Color: 4

Bin 217: 85896 of cap free
Amount of items: 2
Items: 
Size: 564208 Color: 1
Size: 349897 Color: 3

Bin 218: 96718 of cap free
Amount of items: 2
Items: 
Size: 451488 Color: 3
Size: 451795 Color: 4

Bin 219: 113356 of cap free
Amount of items: 2
Items: 
Size: 443926 Color: 3
Size: 442719 Color: 2

Bin 220: 113659 of cap free
Amount of items: 3
Items: 
Size: 308757 Color: 3
Size: 289297 Color: 2
Size: 288288 Color: 1

Bin 221: 125762 of cap free
Amount of items: 2
Items: 
Size: 439647 Color: 2
Size: 434592 Color: 1

Bin 222: 709018 of cap free
Amount of items: 1
Items: 
Size: 290983 Color: 3

Bin 223: 711968 of cap free
Amount of items: 1
Items: 
Size: 288033 Color: 3

Total size: 219918824
Total free space: 3081399

