Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 50
Size: 319 Color: 34
Size: 256 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 55
Size: 258 Color: 12
Size: 264 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 52
Size: 284 Color: 22
Size: 282 Color: 20

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 54
Size: 276 Color: 16
Size: 254 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 49
Size: 296 Color: 28
Size: 286 Color: 24

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 46
Size: 304 Color: 30
Size: 287 Color: 25

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 47
Size: 310 Color: 31
Size: 279 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 48
Size: 336 Color: 37
Size: 250 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 44
Size: 346 Color: 38
Size: 252 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 40
Size: 326 Color: 35
Size: 296 Color: 29

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 42
Size: 328 Color: 36
Size: 293 Color: 27

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 43
Size: 315 Color: 33
Size: 284 Color: 23

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 41
Size: 357 Color: 39
Size: 264 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 45
Size: 314 Color: 32
Size: 283 Color: 21

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 51
Size: 291 Color: 26
Size: 281 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 53
Size: 281 Color: 19
Size: 264 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 56
Size: 256 Color: 11
Size: 253 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 57
Size: 253 Color: 8
Size: 253 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 58
Size: 252 Color: 5
Size: 252 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 59
Size: 251 Color: 2
Size: 251 Color: 1

Total size: 20000
Total free space: 0

