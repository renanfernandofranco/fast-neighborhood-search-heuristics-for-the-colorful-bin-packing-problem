Capicity Bin: 2032
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1130 Color: 1
Size: 846 Color: 1
Size: 52 Color: 0
Size: 4 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1379 Color: 1
Size: 541 Color: 1
Size: 60 Color: 0
Size: 36 Color: 0
Size: 16 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 1
Size: 559 Color: 1
Size: 90 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1501 Color: 1
Size: 391 Color: 1
Size: 76 Color: 0
Size: 64 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1702 Color: 1
Size: 210 Color: 1
Size: 80 Color: 0
Size: 40 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 820 Color: 1
Size: 662 Color: 1
Size: 472 Color: 0
Size: 78 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 314 Color: 1
Size: 60 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 1
Size: 411 Color: 1
Size: 82 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 1
Size: 325 Color: 1
Size: 64 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 1
Size: 206 Color: 1
Size: 40 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 128 Color: 0
Size: 94 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 1
Size: 461 Color: 1
Size: 8 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 1
Size: 317 Color: 1
Size: 62 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 1
Size: 255 Color: 1
Size: 50 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 1
Size: 843 Color: 1
Size: 168 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1079 Color: 1
Size: 795 Color: 1
Size: 158 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 1
Size: 291 Color: 1
Size: 56 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 1
Size: 369 Color: 1
Size: 72 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 1
Size: 721 Color: 1
Size: 144 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 1
Size: 293 Color: 1
Size: 58 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 1
Size: 338 Color: 1
Size: 64 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 1
Size: 219 Color: 1
Size: 42 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 1
Size: 276 Color: 1
Size: 48 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 1
Size: 271 Color: 1
Size: 52 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 1
Size: 846 Color: 1
Size: 168 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1744 Color: 1
Size: 240 Color: 1
Size: 48 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 1
Size: 251 Color: 1
Size: 50 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 754 Color: 1
Size: 36 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 1
Size: 420 Color: 1
Size: 278 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 1
Size: 382 Color: 1
Size: 116 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 1
Size: 462 Color: 1
Size: 92 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 206 Color: 1
Size: 44 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1126 Color: 1
Size: 758 Color: 1
Size: 148 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 1
Size: 582 Color: 1
Size: 228 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 1
Size: 481 Color: 1
Size: 94 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 1
Size: 680 Color: 1
Size: 72 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 1
Size: 443 Color: 1
Size: 108 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 294 Color: 1
Size: 56 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 230 Color: 1
Size: 44 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 1
Size: 673 Color: 1
Size: 78 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 1
Size: 418 Color: 1
Size: 36 Color: 0

Bin 43: 0 of cap free
Amount of items: 4
Items: 
Size: 1400 Color: 1
Size: 316 Color: 1
Size: 228 Color: 0
Size: 88 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 1
Size: 190 Color: 1
Size: 52 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 1
Size: 186 Color: 1
Size: 40 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1747 Color: 1
Size: 239 Color: 1
Size: 46 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 1
Size: 518 Color: 1
Size: 100 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1127 Color: 1
Size: 755 Color: 1
Size: 150 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 1
Size: 193 Color: 1
Size: 38 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1265 Color: 1
Size: 641 Color: 1
Size: 126 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 1
Size: 390 Color: 1
Size: 24 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 1
Size: 363 Color: 1
Size: 40 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 1
Size: 197 Color: 1
Size: 38 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 1
Size: 611 Color: 1
Size: 120 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1245 Color: 1
Size: 657 Color: 1
Size: 130 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1441 Color: 1
Size: 553 Color: 1
Size: 38 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 1
Size: 209 Color: 1
Size: 40 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 1
Size: 371 Color: 1
Size: 74 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1759 Color: 1
Size: 229 Color: 1
Size: 44 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 1
Size: 321 Color: 1
Size: 62 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1825 Color: 1
Size: 173 Color: 1
Size: 34 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 1
Size: 191 Color: 1
Size: 36 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 1
Size: 177 Color: 1
Size: 34 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 1
Size: 937 Color: 1
Size: 78 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 1
Size: 201 Color: 1
Size: 40 Color: 0

Total size: 132080
Total free space: 0

