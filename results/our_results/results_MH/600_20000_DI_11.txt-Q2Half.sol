Capicity Bin: 14480
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10996 Color: 1
Size: 2924 Color: 1
Size: 560 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9038 Color: 1
Size: 4588 Color: 1
Size: 854 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7245 Color: 1
Size: 6031 Color: 1
Size: 1204 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8312 Color: 1
Size: 5144 Color: 1
Size: 1024 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 9732 Color: 1
Size: 2508 Color: 1
Size: 1792 Color: 0
Size: 448 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 7136 Color: 1
Size: 5704 Color: 1
Size: 1200 Color: 0
Size: 440 Color: 0

Bin 7: 0 of cap free
Amount of items: 5
Items: 
Size: 7241 Color: 1
Size: 3804 Color: 1
Size: 2275 Color: 1
Size: 680 Color: 0
Size: 480 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7416 Color: 1
Size: 4296 Color: 1
Size: 2768 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7242 Color: 1
Size: 6034 Color: 1
Size: 1204 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 1
Size: 6026 Color: 1
Size: 1204 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 1
Size: 6028 Color: 1
Size: 1200 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7256 Color: 1
Size: 6024 Color: 1
Size: 1200 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 1
Size: 5218 Color: 1
Size: 1040 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 1
Size: 5212 Color: 1
Size: 1040 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8632 Color: 1
Size: 5254 Color: 1
Size: 594 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 1
Size: 4654 Color: 1
Size: 928 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9036 Color: 1
Size: 4540 Color: 1
Size: 904 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9052 Color: 1
Size: 4500 Color: 1
Size: 928 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 9084 Color: 1
Size: 4538 Color: 1
Size: 858 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9283 Color: 1
Size: 4331 Color: 1
Size: 866 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9359 Color: 1
Size: 4269 Color: 1
Size: 852 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 9706 Color: 1
Size: 3982 Color: 1
Size: 792 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 1
Size: 4058 Color: 1
Size: 670 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 9828 Color: 1
Size: 4212 Color: 1
Size: 440 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 9919 Color: 1
Size: 3801 Color: 1
Size: 760 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 9924 Color: 1
Size: 3502 Color: 1
Size: 1054 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10170 Color: 1
Size: 3594 Color: 1
Size: 716 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 10274 Color: 1
Size: 3506 Color: 1
Size: 700 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 10312 Color: 1
Size: 3704 Color: 1
Size: 464 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 10435 Color: 1
Size: 3345 Color: 1
Size: 700 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 10459 Color: 1
Size: 2983 Color: 1
Size: 1038 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 10476 Color: 1
Size: 3220 Color: 1
Size: 784 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 10600 Color: 1
Size: 3480 Color: 1
Size: 400 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 1
Size: 3436 Color: 1
Size: 424 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 10650 Color: 1
Size: 3194 Color: 1
Size: 636 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 1
Size: 3208 Color: 1
Size: 400 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 10891 Color: 1
Size: 2921 Color: 1
Size: 668 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 10915 Color: 1
Size: 3031 Color: 1
Size: 534 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 10919 Color: 1
Size: 2969 Color: 1
Size: 592 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11036 Color: 1
Size: 2796 Color: 1
Size: 648 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11052 Color: 1
Size: 3102 Color: 1
Size: 326 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 1
Size: 3016 Color: 1
Size: 400 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11150 Color: 1
Size: 2778 Color: 1
Size: 552 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 1
Size: 2774 Color: 1
Size: 552 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 1
Size: 2776 Color: 1
Size: 544 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 1
Size: 2476 Color: 1
Size: 808 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 11219 Color: 1
Size: 2719 Color: 1
Size: 542 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 1
Size: 2502 Color: 1
Size: 674 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 11379 Color: 1
Size: 2485 Color: 1
Size: 616 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 11478 Color: 1
Size: 2922 Color: 1
Size: 80 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 11482 Color: 1
Size: 2262 Color: 1
Size: 736 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 11486 Color: 1
Size: 2266 Color: 1
Size: 728 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 11516 Color: 1
Size: 2212 Color: 1
Size: 752 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 11568 Color: 1
Size: 2648 Color: 1
Size: 264 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 11593 Color: 1
Size: 2407 Color: 1
Size: 480 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 11596 Color: 1
Size: 2244 Color: 1
Size: 640 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 1
Size: 2312 Color: 1
Size: 496 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 1
Size: 2192 Color: 1
Size: 568 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 11734 Color: 1
Size: 2378 Color: 1
Size: 368 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 11754 Color: 1
Size: 2274 Color: 1
Size: 452 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 1
Size: 2264 Color: 1
Size: 454 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 11767 Color: 1
Size: 2261 Color: 1
Size: 452 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 11768 Color: 1
Size: 2440 Color: 1
Size: 272 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 11770 Color: 1
Size: 2070 Color: 1
Size: 640 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 1
Size: 2404 Color: 1
Size: 280 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 1
Size: 2344 Color: 1
Size: 308 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 11860 Color: 1
Size: 2188 Color: 1
Size: 432 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 11927 Color: 1
Size: 2081 Color: 1
Size: 472 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 11985 Color: 1
Size: 2231 Color: 1
Size: 264 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 11989 Color: 1
Size: 2077 Color: 1
Size: 414 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 11990 Color: 1
Size: 2078 Color: 1
Size: 412 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 1
Size: 1816 Color: 1
Size: 672 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 11998 Color: 1
Size: 1574 Color: 1
Size: 908 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 12021 Color: 1
Size: 2051 Color: 1
Size: 408 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 1
Size: 1868 Color: 1
Size: 552 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 12062 Color: 1
Size: 2018 Color: 1
Size: 400 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 12068 Color: 1
Size: 2000 Color: 1
Size: 412 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 12088 Color: 1
Size: 1864 Color: 1
Size: 528 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 12183 Color: 1
Size: 1883 Color: 1
Size: 414 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 12186 Color: 1
Size: 1778 Color: 1
Size: 516 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 1
Size: 1906 Color: 1
Size: 380 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 12201 Color: 1
Size: 1901 Color: 1
Size: 378 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 12202 Color: 1
Size: 1902 Color: 1
Size: 376 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 12221 Color: 1
Size: 1571 Color: 1
Size: 688 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 1
Size: 1784 Color: 1
Size: 452 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 1
Size: 1192 Color: 0
Size: 1040 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 1
Size: 1860 Color: 1
Size: 368 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 12305 Color: 1
Size: 1813 Color: 1
Size: 362 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 1
Size: 2008 Color: 1
Size: 160 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 1530 Color: 1
Size: 606 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 12350 Color: 1
Size: 1708 Color: 1
Size: 422 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 12354 Color: 1
Size: 1774 Color: 1
Size: 352 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 12381 Color: 1
Size: 1479 Color: 1
Size: 620 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 12388 Color: 1
Size: 2012 Color: 1
Size: 80 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 12404 Color: 1
Size: 1732 Color: 1
Size: 344 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 1
Size: 1748 Color: 1
Size: 296 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 12443 Color: 1
Size: 1699 Color: 1
Size: 338 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 12490 Color: 1
Size: 1662 Color: 1
Size: 328 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 12502 Color: 1
Size: 1650 Color: 1
Size: 328 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 1
Size: 1592 Color: 1
Size: 352 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 1
Size: 1624 Color: 1
Size: 308 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 12563 Color: 1
Size: 1599 Color: 1
Size: 318 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 1
Size: 1448 Color: 1
Size: 448 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 1
Size: 1542 Color: 1
Size: 344 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 12604 Color: 1
Size: 1564 Color: 1
Size: 312 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 12614 Color: 1
Size: 1558 Color: 1
Size: 308 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 12630 Color: 1
Size: 1502 Color: 1
Size: 348 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 1
Size: 1612 Color: 1
Size: 200 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 12680 Color: 1
Size: 1304 Color: 1
Size: 496 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 12691 Color: 1
Size: 1491 Color: 1
Size: 298 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 1
Size: 1500 Color: 1
Size: 280 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 12716 Color: 1
Size: 1384 Color: 1
Size: 380 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 12726 Color: 1
Size: 1514 Color: 1
Size: 240 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 12727 Color: 1
Size: 1461 Color: 1
Size: 292 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 12742 Color: 1
Size: 1450 Color: 1
Size: 288 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 1
Size: 1476 Color: 1
Size: 260 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 12754 Color: 1
Size: 1512 Color: 1
Size: 214 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 12768 Color: 1
Size: 1456 Color: 1
Size: 256 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 12770 Color: 1
Size: 1506 Color: 1
Size: 204 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 1
Size: 1380 Color: 1
Size: 320 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 12786 Color: 1
Size: 1406 Color: 1
Size: 288 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 12794 Color: 1
Size: 1382 Color: 1
Size: 304 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 1
Size: 1420 Color: 1
Size: 264 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 12822 Color: 1
Size: 1364 Color: 1
Size: 294 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 1
Size: 1342 Color: 1
Size: 314 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 12828 Color: 1
Size: 1404 Color: 1
Size: 248 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 1
Size: 1324 Color: 1
Size: 304 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 12874 Color: 1
Size: 1318 Color: 1
Size: 288 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 12892 Color: 1
Size: 1292 Color: 1
Size: 296 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 1
Size: 1302 Color: 1
Size: 276 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 12910 Color: 1
Size: 1310 Color: 1
Size: 260 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 12918 Color: 1
Size: 896 Color: 1
Size: 666 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 1
Size: 1224 Color: 1
Size: 336 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 12926 Color: 1
Size: 1478 Color: 1
Size: 76 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 12932 Color: 1
Size: 1288 Color: 1
Size: 260 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 1
Size: 1244 Color: 1
Size: 284 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 1
Size: 1442 Color: 1
Size: 66 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 1
Size: 1260 Color: 1
Size: 232 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 1
Size: 1228 Color: 1
Size: 240 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 1152 Color: 1
Size: 312 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 1
Size: 1214 Color: 1
Size: 240 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 8595 Color: 1
Size: 5220 Color: 1
Size: 664 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 9030 Color: 1
Size: 4905 Color: 1
Size: 544 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 9614 Color: 1
Size: 4271 Color: 1
Size: 594 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 9756 Color: 1
Size: 4291 Color: 1
Size: 432 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 9812 Color: 1
Size: 4571 Color: 1
Size: 96 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 10364 Color: 1
Size: 3763 Color: 1
Size: 352 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 10632 Color: 1
Size: 3359 Color: 1
Size: 488 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 10843 Color: 1
Size: 2740 Color: 1
Size: 896 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 10907 Color: 1
Size: 2876 Color: 1
Size: 696 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 11132 Color: 1
Size: 2979 Color: 1
Size: 368 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 11359 Color: 1
Size: 1914 Color: 1
Size: 1206 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 11363 Color: 1
Size: 2860 Color: 1
Size: 256 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 1
Size: 2679 Color: 1
Size: 240 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 1
Size: 2522 Color: 1
Size: 384 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 11751 Color: 1
Size: 2088 Color: 1
Size: 640 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 1
Size: 1751 Color: 1
Size: 240 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 12519 Color: 1
Size: 1672 Color: 1
Size: 288 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 12627 Color: 1
Size: 1484 Color: 1
Size: 368 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 1
Size: 1545 Color: 1
Size: 288 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 12707 Color: 1
Size: 1516 Color: 1
Size: 256 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 8934 Color: 1
Size: 4408 Color: 1
Size: 1136 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 9192 Color: 1
Size: 4622 Color: 1
Size: 664 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 1
Size: 4542 Color: 1
Size: 280 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 10040 Color: 1
Size: 3514 Color: 1
Size: 924 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 10460 Color: 1
Size: 3106 Color: 1
Size: 912 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 10556 Color: 1
Size: 3240 Color: 1
Size: 682 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 10746 Color: 1
Size: 3356 Color: 1
Size: 376 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 1
Size: 3340 Color: 1
Size: 384 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 11267 Color: 1
Size: 2971 Color: 1
Size: 240 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 12595 Color: 1
Size: 1635 Color: 1
Size: 248 Color: 0

Bin 172: 2 of cap free
Amount of items: 3
Items: 
Size: 10762 Color: 1
Size: 3276 Color: 1
Size: 440 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 9355 Color: 1
Size: 4524 Color: 1
Size: 598 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 10266 Color: 1
Size: 3411 Color: 1
Size: 800 Color: 0

Bin 175: 3 of cap free
Amount of items: 3
Items: 
Size: 10467 Color: 1
Size: 3658 Color: 1
Size: 352 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 10094 Color: 1
Size: 3502 Color: 1
Size: 880 Color: 0

Bin 177: 4 of cap free
Amount of items: 3
Items: 
Size: 11476 Color: 1
Size: 2020 Color: 1
Size: 980 Color: 0

Bin 178: 6 of cap free
Amount of items: 3
Items: 
Size: 10451 Color: 1
Size: 2975 Color: 1
Size: 1048 Color: 0

Bin 179: 6 of cap free
Amount of items: 3
Items: 
Size: 8178 Color: 1
Size: 5464 Color: 1
Size: 832 Color: 0

Bin 180: 6 of cap free
Amount of items: 3
Items: 
Size: 10911 Color: 1
Size: 3371 Color: 1
Size: 192 Color: 0

Bin 181: 6 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 1
Size: 6934 Color: 1
Size: 296 Color: 0

Bin 182: 7 of cap free
Amount of items: 3
Items: 
Size: 9965 Color: 1
Size: 3940 Color: 1
Size: 568 Color: 0

Bin 183: 9 of cap free
Amount of items: 3
Items: 
Size: 10387 Color: 1
Size: 3892 Color: 1
Size: 192 Color: 0

Bin 184: 13 of cap free
Amount of items: 3
Items: 
Size: 8251 Color: 1
Size: 5896 Color: 1
Size: 320 Color: 0

Bin 185: 18 of cap free
Amount of items: 3
Items: 
Size: 7258 Color: 1
Size: 6036 Color: 1
Size: 1168 Color: 0

Bin 186: 18 of cap free
Amount of items: 3
Items: 
Size: 11283 Color: 1
Size: 2585 Color: 1
Size: 594 Color: 0

Bin 187: 20 of cap free
Amount of items: 3
Items: 
Size: 8997 Color: 1
Size: 5271 Color: 1
Size: 192 Color: 0

Bin 188: 21 of cap free
Amount of items: 3
Items: 
Size: 8331 Color: 1
Size: 5376 Color: 1
Size: 752 Color: 0

Bin 189: 24 of cap free
Amount of items: 3
Items: 
Size: 7640 Color: 1
Size: 6336 Color: 1
Size: 480 Color: 0

Bin 190: 24 of cap free
Amount of items: 3
Items: 
Size: 8872 Color: 1
Size: 4680 Color: 1
Size: 904 Color: 0

Bin 191: 26 of cap free
Amount of items: 5
Items: 
Size: 8220 Color: 1
Size: 3037 Color: 1
Size: 2129 Color: 1
Size: 560 Color: 0
Size: 508 Color: 0

Bin 192: 31 of cap free
Amount of items: 3
Items: 
Size: 8155 Color: 1
Size: 5974 Color: 1
Size: 320 Color: 0

Bin 193: 44 of cap free
Amount of items: 3
Items: 
Size: 8203 Color: 1
Size: 5641 Color: 1
Size: 592 Color: 0

Bin 194: 71 of cap free
Amount of items: 3
Items: 
Size: 10282 Color: 1
Size: 3351 Color: 1
Size: 776 Color: 0

Bin 195: 221 of cap free
Amount of items: 3
Items: 
Size: 9331 Color: 1
Size: 4024 Color: 1
Size: 904 Color: 0

Bin 196: 463 of cap free
Amount of items: 3
Items: 
Size: 7314 Color: 1
Size: 6033 Color: 1
Size: 670 Color: 0

Bin 197: 1796 of cap free
Amount of items: 1
Items: 
Size: 12684 Color: 1

Bin 198: 3230 of cap free
Amount of items: 3
Items: 
Size: 5611 Color: 1
Size: 5191 Color: 1
Size: 448 Color: 0

Bin 199: 8361 of cap free
Amount of items: 3
Items: 
Size: 2991 Color: 1
Size: 2856 Color: 1
Size: 272 Color: 0

Total size: 2867040
Total free space: 14480

