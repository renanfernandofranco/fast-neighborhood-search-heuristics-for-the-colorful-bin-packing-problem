Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 221
Size: 276 Color: 94
Size: 255 Color: 39

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 242
Size: 254 Color: 32
Size: 251 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 197
Size: 312 Color: 131
Size: 274 Color: 88

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 193
Size: 336 Color: 144
Size: 258 Color: 43

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 178
Size: 347 Color: 149
Size: 283 Color: 104

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 219
Size: 281 Color: 101
Size: 255 Color: 35

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 172
Size: 357 Color: 161
Size: 277 Color: 97

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 233
Size: 259 Color: 50
Size: 256 Color: 41

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 224
Size: 274 Color: 90
Size: 254 Color: 30

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 246
Size: 253 Color: 27
Size: 250 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 213
Size: 281 Color: 100
Size: 274 Color: 87

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 236
Size: 262 Color: 59
Size: 253 Color: 28

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 198
Size: 318 Color: 136
Size: 267 Color: 70

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 187
Size: 357 Color: 163
Size: 250 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 160
Size: 350 Color: 153
Size: 294 Color: 114

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 231
Size: 269 Color: 74
Size: 251 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 239
Size: 259 Color: 52
Size: 250 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 212
Size: 280 Color: 99
Size: 276 Color: 95

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 196
Size: 329 Color: 140
Size: 259 Color: 53

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 182
Size: 318 Color: 137
Size: 297 Color: 118

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 217
Size: 298 Color: 121
Size: 252 Color: 20

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 248
Size: 252 Color: 16
Size: 250 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 243
Size: 253 Color: 26
Size: 252 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 235
Size: 263 Color: 63
Size: 252 Color: 21

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 180
Size: 365 Color: 169
Size: 259 Color: 48

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 244
Size: 253 Color: 24
Size: 251 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 237
Size: 262 Color: 60
Size: 250 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 175
Size: 351 Color: 155
Size: 282 Color: 103

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 205
Size: 287 Color: 107
Size: 287 Color: 106

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 186
Size: 314 Color: 133
Size: 294 Color: 116

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 241
Size: 257 Color: 42
Size: 251 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 199
Size: 310 Color: 130
Size: 271 Color: 77

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 162
Size: 356 Color: 159
Size: 287 Color: 105

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 192
Size: 305 Color: 126
Size: 290 Color: 109

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 167
Size: 362 Color: 166
Size: 275 Color: 93

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 208
Size: 294 Color: 115
Size: 269 Color: 73

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 223
Size: 271 Color: 78
Size: 258 Color: 45

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 234
Size: 265 Color: 68
Size: 250 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 176
Size: 351 Color: 154
Size: 282 Color: 102

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 222
Size: 275 Color: 91
Size: 255 Color: 40

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 229
Size: 267 Color: 71
Size: 254 Color: 33

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 228
Size: 264 Color: 66
Size: 258 Color: 44

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 220
Size: 274 Color: 89
Size: 260 Color: 54

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 226
Size: 272 Color: 82
Size: 255 Color: 38

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 158
Size: 350 Color: 152
Size: 295 Color: 117

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 215
Size: 299 Color: 122
Size: 255 Color: 37

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 238
Size: 260 Color: 55
Size: 250 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 225
Size: 269 Color: 75
Size: 258 Color: 47

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 174
Size: 359 Color: 164
Size: 275 Color: 92

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 171
Size: 363 Color: 168
Size: 271 Color: 80

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 181
Size: 366 Color: 173
Size: 252 Color: 22

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 207
Size: 313 Color: 132
Size: 253 Color: 25

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 204
Size: 320 Color: 138
Size: 254 Color: 34

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 188
Size: 344 Color: 147
Size: 261 Color: 57

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 247
Size: 252 Color: 17
Size: 251 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 211
Size: 308 Color: 128
Size: 251 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 210
Size: 301 Color: 123
Size: 258 Color: 46

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 218
Size: 276 Color: 96
Size: 262 Color: 61

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 170
Size: 346 Color: 148
Size: 288 Color: 108

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 191
Size: 303 Color: 125
Size: 293 Color: 112

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 214
Size: 291 Color: 110
Size: 264 Color: 65

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 189
Size: 350 Color: 150
Size: 253 Color: 29

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 190
Size: 350 Color: 151
Size: 250 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 195
Size: 298 Color: 119
Size: 292 Color: 111

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 194
Size: 333 Color: 142
Size: 260 Color: 56

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 216
Size: 302 Color: 124
Size: 252 Color: 19

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 177
Size: 361 Color: 165
Size: 269 Color: 76

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 184
Size: 330 Color: 141
Size: 278 Color: 98

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 202
Size: 325 Color: 139
Size: 254 Color: 31

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 156
Size: 333 Color: 143
Size: 315 Color: 135

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 227
Size: 263 Color: 64
Size: 263 Color: 62

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 201
Size: 314 Color: 134
Size: 266 Color: 69

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 185
Size: 337 Color: 145
Size: 271 Color: 79

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 203
Size: 307 Color: 127
Size: 271 Color: 81

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 200
Size: 308 Color: 129
Size: 273 Color: 85

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 206
Size: 298 Color: 120
Size: 272 Color: 83

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 209
Size: 293 Color: 113
Size: 268 Color: 72

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 230
Size: 261 Color: 58
Size: 259 Color: 51

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 245
Size: 253 Color: 23
Size: 250 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 183
Size: 342 Color: 146
Size: 272 Color: 84

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 179
Size: 355 Color: 157
Size: 273 Color: 86

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 232
Size: 264 Color: 67
Size: 255 Color: 36

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 240
Size: 259 Color: 49
Size: 250 Color: 1

Total size: 83000
Total free space: 0

