Capicity Bin: 1996
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1137 Color: 0
Size: 511 Color: 1
Size: 164 Color: 0
Size: 108 Color: 1
Size: 76 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1145 Color: 1
Size: 767 Color: 1
Size: 84 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1162 Color: 0
Size: 778 Color: 1
Size: 56 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 0
Size: 663 Color: 0
Size: 46 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1346 Color: 1
Size: 542 Color: 1
Size: 108 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 1
Size: 393 Color: 0
Size: 144 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 1
Size: 387 Color: 1
Size: 84 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 0
Size: 308 Color: 0
Size: 78 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 262 Color: 1
Size: 88 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 0
Size: 325 Color: 1
Size: 38 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 0
Size: 190 Color: 0
Size: 100 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 0
Size: 186 Color: 0
Size: 88 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 0
Size: 178 Color: 1
Size: 56 Color: 0

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1002 Color: 1
Size: 829 Color: 0
Size: 164 Color: 1

Bin 15: 1 of cap free
Amount of items: 4
Items: 
Size: 1198 Color: 1
Size: 713 Color: 0
Size: 44 Color: 1
Size: 40 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1380 Color: 1
Size: 515 Color: 1
Size: 100 Color: 0

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 0
Size: 506 Color: 1
Size: 32 Color: 0

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1486 Color: 1
Size: 509 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 0
Size: 426 Color: 0
Size: 52 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 0
Size: 401 Color: 1
Size: 36 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 0
Size: 189 Color: 1
Size: 152 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 1
Size: 182 Color: 0
Size: 102 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 239 Color: 0
Size: 42 Color: 1

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 1770 Color: 0
Size: 197 Color: 1
Size: 20 Color: 1
Size: 8 Color: 0

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1774 Color: 1
Size: 220 Color: 0

Bin 26: 2 of cap free
Amount of items: 4
Items: 
Size: 1778 Color: 0
Size: 198 Color: 1
Size: 10 Color: 0
Size: 8 Color: 1

Bin 27: 3 of cap free
Amount of items: 3
Items: 
Size: 1255 Color: 0
Size: 666 Color: 0
Size: 72 Color: 1

Bin 28: 3 of cap free
Amount of items: 2
Items: 
Size: 1259 Color: 1
Size: 734 Color: 0

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 0
Size: 619 Color: 1
Size: 36 Color: 0

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 0
Size: 550 Color: 1
Size: 64 Color: 0

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 0
Size: 610 Color: 1

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1751 Color: 1
Size: 242 Color: 0

Bin 33: 4 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 1
Size: 478 Color: 0
Size: 52 Color: 1

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 0
Size: 390 Color: 1

Bin 35: 5 of cap free
Amount of items: 2
Items: 
Size: 1575 Color: 0
Size: 416 Color: 1

Bin 36: 5 of cap free
Amount of items: 2
Items: 
Size: 1761 Color: 1
Size: 230 Color: 0

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1771 Color: 0
Size: 220 Color: 1

Bin 38: 5 of cap free
Amount of items: 2
Items: 
Size: 1786 Color: 0
Size: 205 Color: 1

Bin 39: 6 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 1
Size: 717 Color: 0
Size: 72 Color: 1

Bin 40: 6 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 0
Size: 676 Color: 1
Size: 48 Color: 1

Bin 41: 7 of cap free
Amount of items: 3
Items: 
Size: 1252 Color: 0
Size: 615 Color: 0
Size: 122 Color: 1

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 1291 Color: 1
Size: 698 Color: 0

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1695 Color: 1
Size: 294 Color: 0

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1743 Color: 1
Size: 246 Color: 0

Bin 45: 8 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 0
Size: 830 Color: 0
Size: 92 Color: 1

Bin 46: 8 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 1
Size: 351 Color: 0
Size: 211 Color: 0

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 1
Size: 369 Color: 0

Bin 48: 11 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 1
Size: 831 Color: 0
Size: 36 Color: 1

Bin 49: 11 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 1
Size: 397 Color: 0

Bin 50: 11 of cap free
Amount of items: 2
Items: 
Size: 1682 Color: 0
Size: 303 Color: 1

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1533 Color: 0
Size: 451 Color: 1

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 0
Size: 410 Color: 1

Bin 53: 13 of cap free
Amount of items: 6
Items: 
Size: 1003 Color: 0
Size: 340 Color: 1
Size: 330 Color: 1
Size: 142 Color: 0
Size: 132 Color: 0
Size: 36 Color: 1

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1617 Color: 1
Size: 366 Color: 0

Bin 55: 13 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 0
Size: 286 Color: 1
Size: 16 Color: 1

Bin 56: 14 of cap free
Amount of items: 6
Items: 
Size: 999 Color: 0
Size: 318 Color: 1
Size: 281 Color: 1
Size: 142 Color: 1
Size: 122 Color: 0
Size: 120 Color: 0

Bin 57: 15 of cap free
Amount of items: 3
Items: 
Size: 1141 Color: 0
Size: 804 Color: 0
Size: 36 Color: 1

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1390 Color: 0
Size: 591 Color: 1

Bin 59: 15 of cap free
Amount of items: 2
Items: 
Size: 1521 Color: 1
Size: 460 Color: 0

Bin 60: 15 of cap free
Amount of items: 2
Items: 
Size: 1659 Color: 1
Size: 322 Color: 0

Bin 61: 17 of cap free
Amount of items: 2
Items: 
Size: 1530 Color: 0
Size: 449 Color: 1

Bin 62: 20 of cap free
Amount of items: 2
Items: 
Size: 1387 Color: 1
Size: 589 Color: 0

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 1522 Color: 0
Size: 446 Color: 1

Bin 64: 35 of cap free
Amount of items: 2
Items: 
Size: 1607 Color: 1
Size: 354 Color: 0

Bin 65: 46 of cap free
Amount of items: 17
Items: 
Size: 263 Color: 1
Size: 251 Color: 1
Size: 166 Color: 1
Size: 136 Color: 1
Size: 132 Color: 1
Size: 118 Color: 0
Size: 116 Color: 1
Size: 102 Color: 0
Size: 88 Color: 0
Size: 78 Color: 0
Size: 78 Color: 0
Size: 76 Color: 0
Size: 76 Color: 0
Size: 70 Color: 1
Size: 68 Color: 1
Size: 68 Color: 0
Size: 64 Color: 0

Bin 66: 1554 of cap free
Amount of items: 8
Items: 
Size: 64 Color: 1
Size: 64 Color: 1
Size: 60 Color: 1
Size: 60 Color: 1
Size: 56 Color: 0
Size: 52 Color: 0
Size: 50 Color: 0
Size: 36 Color: 0

Total size: 129740
Total free space: 1996

