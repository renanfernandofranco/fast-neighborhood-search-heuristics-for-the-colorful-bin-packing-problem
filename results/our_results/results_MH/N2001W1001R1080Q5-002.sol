Capicity Bin: 1001
Lower Bound: 911

Bins used: 911
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 3
Size: 360 Color: 0
Size: 125 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 514 Color: 1
Size: 301 Color: 0
Size: 186 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 574 Color: 2
Size: 271 Color: 3
Size: 156 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 3
Size: 242 Color: 4
Size: 135 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 3
Size: 179 Color: 4
Size: 170 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 3
Size: 172 Color: 0
Size: 168 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 1
Size: 139 Color: 3
Size: 117 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 4
Size: 310 Color: 1
Size: 175 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 3
Size: 246 Color: 4
Size: 126 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 0
Size: 135 Color: 3
Size: 134 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 1
Size: 294 Color: 3
Size: 147 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 2
Size: 190 Color: 2
Size: 176 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 3
Size: 168 Color: 1
Size: 167 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 359 Color: 1
Size: 191 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 794 Color: 0
Size: 105 Color: 3
Size: 102 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 4
Size: 105 Color: 0
Size: 104 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 797 Color: 2
Size: 102 Color: 2
Size: 102 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 0
Size: 257 Color: 0
Size: 184 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 301 Color: 2
Size: 234 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 4
Size: 227 Color: 2
Size: 167 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 3
Size: 315 Color: 2
Size: 170 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 1
Size: 138 Color: 3
Size: 125 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 4
Size: 346 Color: 4
Size: 144 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 272 Color: 4
Size: 272 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 2
Size: 107 Color: 4
Size: 101 Color: 2

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 4
Size: 499 Color: 1

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 4
Size: 165 Color: 3
Size: 132 Color: 2

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 2
Size: 206 Color: 4

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 1

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 3

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 1
Size: 255 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 3
Size: 195 Color: 0
Size: 147 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 3
Size: 143 Color: 3
Size: 112 Color: 1

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 2
Size: 263 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 2
Size: 190 Color: 1
Size: 187 Color: 4

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 1
Size: 244 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 1
Size: 144 Color: 2
Size: 135 Color: 2

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 279 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 0
Size: 257 Color: 0
Size: 120 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 301 Color: 1
Size: 257 Color: 1

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 2
Size: 247 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 4
Size: 311 Color: 2
Size: 246 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 403 Color: 0
Size: 173 Color: 3

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 0

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 479 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 1
Size: 336 Color: 0
Size: 143 Color: 3

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 474 Color: 1

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 0

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 4

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 4
Size: 307 Color: 2
Size: 169 Color: 1

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 4
Size: 236 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 4
Size: 119 Color: 4
Size: 117 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 3
Size: 312 Color: 4
Size: 119 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 514 Color: 2
Size: 312 Color: 4
Size: 175 Color: 0

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 0
Size: 479 Color: 1

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 2
Size: 381 Color: 3

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 0
Size: 346 Color: 4

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 3

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 256 Color: 3

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 0

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 2
Size: 314 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 3
Size: 300 Color: 1
Size: 131 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 3
Size: 313 Color: 0
Size: 172 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 1
Size: 310 Color: 1
Size: 169 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 736 Color: 1
Size: 133 Color: 3
Size: 132 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 3

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 4

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 2
Size: 297 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 0
Size: 165 Color: 1
Size: 132 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 0
Size: 266 Color: 3
Size: 165 Color: 0

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 4
Size: 438 Color: 3

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 4
Size: 271 Color: 1
Size: 168 Color: 4

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 487 Color: 2

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 2
Size: 446 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 373 Color: 3
Size: 182 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 1
Size: 226 Color: 4
Size: 171 Color: 2

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 2
Size: 444 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 4

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 4
Size: 261 Color: 2

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 4

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 261 Color: 4

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 269 Color: 1

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 4
Size: 261 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 1
Size: 147 Color: 3
Size: 131 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 1
Size: 141 Color: 0
Size: 115 Color: 0

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 3
Size: 261 Color: 4

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 463 Color: 4

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 0

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 463 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 660 Color: 1
Size: 188 Color: 4
Size: 153 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 399 Color: 3
Size: 144 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 0
Size: 226 Color: 4
Size: 168 Color: 1

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 394 Color: 0

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 4
Size: 310 Color: 2
Size: 223 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 2
Size: 181 Color: 4
Size: 168 Color: 0

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 349 Color: 3

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 4

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 421 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 0
Size: 290 Color: 0
Size: 186 Color: 2

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 0
Size: 398 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 1
Size: 301 Color: 0
Size: 111 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 3
Size: 227 Color: 3
Size: 171 Color: 4

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 0
Size: 442 Color: 2

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 4
Size: 442 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 2
Size: 224 Color: 2
Size: 146 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 413 Color: 1
Size: 146 Color: 0

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 4
Size: 467 Color: 0

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 257 Color: 3

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 647 Color: 2
Size: 178 Color: 3
Size: 176 Color: 4

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 4
Size: 435 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 4
Size: 257 Color: 0
Size: 178 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 358 Color: 4
Size: 176 Color: 4

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 1
Size: 281 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 4
Size: 142 Color: 0
Size: 114 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 4
Size: 138 Color: 2
Size: 118 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 3
Size: 167 Color: 4
Size: 114 Color: 4

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 1
Size: 350 Color: 2

Bin 121: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 350 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 3
Size: 122 Color: 4
Size: 118 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 1
Size: 257 Color: 0
Size: 120 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 4
Size: 173 Color: 2
Size: 172 Color: 1

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 2

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 430 Color: 0

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 257 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 3
Size: 272 Color: 4
Size: 158 Color: 2

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 1

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 3

Bin 131: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 3
Size: 246 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 2
Size: 273 Color: 2
Size: 159 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 660 Color: 2
Size: 190 Color: 4
Size: 151 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 3
Size: 179 Color: 4
Size: 170 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 3
Size: 188 Color: 1
Size: 180 Color: 3

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 297 Color: 2

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 297 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 1
Size: 148 Color: 2
Size: 132 Color: 4

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 4

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 0
Size: 403 Color: 4

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 2
Size: 365 Color: 4

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 403 Color: 3
Size: 183 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 403 Color: 0
Size: 183 Color: 2

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 2
Size: 177 Color: 0
Size: 121 Color: 2

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 3
Size: 288 Color: 2
Size: 177 Color: 3

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 1
Size: 129 Color: 4
Size: 112 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 0
Size: 112 Color: 4
Size: 109 Color: 4

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 4
Size: 271 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 404 Color: 2
Size: 176 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 4
Size: 299 Color: 3
Size: 177 Color: 2

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 205 Color: 0

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 4
Size: 207 Color: 1

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 0
Size: 209 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 791 Color: 3
Size: 110 Color: 2
Size: 100 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 3
Size: 105 Color: 2
Size: 100 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 2
Size: 108 Color: 0
Size: 103 Color: 2

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 0
Size: 241 Color: 3

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 0

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 211 Color: 3

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 0
Size: 209 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 1
Size: 109 Color: 0
Size: 102 Color: 2

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 203 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 0
Size: 106 Color: 3
Size: 105 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 0
Size: 108 Color: 0
Size: 103 Color: 3

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 0
Size: 242 Color: 3

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 4
Size: 254 Color: 3

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 4
Size: 288 Color: 3

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 1
Size: 288 Color: 2
Size: 151 Color: 2

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 2
Size: 141 Color: 4
Size: 136 Color: 3

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 1
Size: 167 Color: 4
Size: 162 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 3
Size: 159 Color: 2
Size: 134 Color: 2

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 2

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 2
Size: 467 Color: 4

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 2
Size: 358 Color: 3

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 3
Size: 422 Color: 4

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 0
Size: 457 Color: 2

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 422 Color: 0
Size: 122 Color: 2

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 4
Size: 320 Color: 2

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 0

Bin 181: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 3
Size: 399 Color: 0

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 0

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 0
Size: 448 Color: 1

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 4
Size: 449 Color: 1

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 4
Size: 472 Color: 3

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 2
Size: 383 Color: 1

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 0
Size: 456 Color: 2

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 3
Size: 401 Color: 2

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 4
Size: 320 Color: 2

Bin 190: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 0

Bin 191: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 0
Size: 472 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 400 Color: 4
Size: 153 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 2
Size: 336 Color: 1
Size: 154 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 379 Color: 1
Size: 169 Color: 2

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 545 Color: 1
Size: 320 Color: 4
Size: 136 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 545 Color: 4
Size: 320 Color: 0
Size: 136 Color: 3

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 3
Size: 412 Color: 4

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 227 Color: 2

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 366 Color: 0

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 4

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 411 Color: 2

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 4
Size: 384 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 3
Size: 231 Color: 1
Size: 181 Color: 0

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 4
Size: 227 Color: 2

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 0
Size: 412 Color: 3

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 3
Size: 184 Color: 0
Size: 182 Color: 3

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 4
Size: 181 Color: 2
Size: 161 Color: 4

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 4
Size: 194 Color: 3
Size: 190 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 0
Size: 227 Color: 0
Size: 157 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 1
Size: 227 Color: 2
Size: 185 Color: 2

Bin 211: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 0
Size: 370 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 4
Size: 188 Color: 4
Size: 182 Color: 3

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 4
Size: 257 Color: 2
Size: 136 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 309 Color: 4
Size: 226 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 1
Size: 173 Color: 4
Size: 158 Color: 2

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 2
Size: 272 Color: 4
Size: 126 Color: 3

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 4
Size: 128 Color: 1
Size: 113 Color: 0

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 1

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 3

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 0
Size: 338 Color: 4
Size: 147 Color: 2

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 2

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 3
Size: 246 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 4
Size: 272 Color: 3
Size: 162 Color: 4

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 451 Color: 0

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 374 Color: 3

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 4
Size: 361 Color: 3

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 3
Size: 360 Color: 2
Size: 130 Color: 1

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 3
Size: 403 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 2
Size: 267 Color: 1
Size: 166 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 1
Size: 220 Color: 0
Size: 167 Color: 2

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 239 Color: 4

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 239 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 2
Size: 128 Color: 0
Size: 113 Color: 1

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 299 Color: 2
Size: 259 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 2
Size: 171 Color: 2
Size: 171 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 4
Size: 179 Color: 4
Size: 163 Color: 0

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 2
Size: 246 Color: 1

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 269 Color: 3

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 3
Size: 268 Color: 1

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 293 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 4
Size: 159 Color: 0
Size: 158 Color: 1

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 4
Size: 206 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 3
Size: 107 Color: 3
Size: 101 Color: 1

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 2
Size: 280 Color: 0

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 2
Size: 331 Color: 4

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 3
Size: 158 Color: 2
Size: 122 Color: 0

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 1
Size: 434 Color: 3

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 1
Size: 221 Color: 0

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 2
Size: 348 Color: 4

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 3
Size: 348 Color: 0

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 366 Color: 3

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 4
Size: 166 Color: 4
Size: 157 Color: 3

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 3

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 1
Size: 271 Color: 3
Size: 163 Color: 1

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 2
Size: 427 Color: 3

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 2
Size: 273 Color: 1

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 2
Size: 190 Color: 4
Size: 128 Color: 4

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 352 Color: 3

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 3
Size: 185 Color: 2
Size: 146 Color: 2

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 4
Size: 350 Color: 3

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 3
Size: 488 Color: 2

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 3
Size: 350 Color: 2
Size: 138 Color: 4

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 295 Color: 3

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 2
Size: 131 Color: 3
Size: 123 Color: 4

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 4
Size: 264 Color: 0
Size: 129 Color: 4

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 3

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 0
Size: 133 Color: 2
Size: 121 Color: 2

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 397 Color: 0

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 3

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 1

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 3

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 2
Size: 384 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 305 Color: 2
Size: 250 Color: 4

Bin 277: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 4
Size: 377 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 3
Size: 273 Color: 4
Size: 161 Color: 0

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 4
Size: 485 Color: 0

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 345 Color: 4

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 3
Size: 345 Color: 2

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 0
Size: 257 Color: 4
Size: 178 Color: 2

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 431 Color: 3

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 4

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 431 Color: 2

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 4
Size: 388 Color: 2

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 1
Size: 388 Color: 3

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 0

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 3
Size: 223 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 3
Size: 231 Color: 1
Size: 159 Color: 0

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 2

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 466 Color: 1

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 1
Size: 372 Color: 4

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 4
Size: 191 Color: 4
Size: 181 Color: 3

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 3

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 0

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 293 Color: 4

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 4

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 0
Size: 250 Color: 1

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 296 Color: 4

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 3
Size: 250 Color: 0

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 2
Size: 387 Color: 1

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 0
Size: 253 Color: 2

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 581 Color: 4
Size: 270 Color: 1
Size: 150 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 561 Color: 4
Size: 250 Color: 4
Size: 190 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 4
Size: 116 Color: 4
Size: 114 Color: 1

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 4
Size: 420 Color: 2

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 2
Size: 309 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 581 Color: 0
Size: 270 Color: 1
Size: 150 Color: 3

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 2
Size: 127 Color: 0
Size: 111 Color: 3

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 404 Color: 3

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 2
Size: 404 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 4
Size: 346 Color: 2
Size: 139 Color: 4

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 4
Size: 391 Color: 3

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 2
Size: 138 Color: 0
Size: 124 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 0
Size: 136 Color: 4
Size: 126 Color: 4

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 2
Size: 308 Color: 3

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 4
Size: 421 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 1
Size: 308 Color: 1
Size: 177 Color: 3

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 3
Size: 220 Color: 0

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 200 Color: 3

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 201 Color: 0

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 2
Size: 200 Color: 0

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 0

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 491 Color: 2

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 271 Color: 2

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 510 Color: 0
Size: 312 Color: 1
Size: 179 Color: 2

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 3
Size: 429 Color: 1

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 4
Size: 277 Color: 1

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 1
Size: 277 Color: 4

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 0
Size: 140 Color: 0
Size: 137 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 2
Size: 158 Color: 4
Size: 119 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 0
Size: 152 Color: 4
Size: 134 Color: 3

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 3
Size: 312 Color: 2
Size: 153 Color: 3

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 523 Color: 4
Size: 334 Color: 3
Size: 144 Color: 3

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 4
Size: 136 Color: 3
Size: 126 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 669 Color: 4
Size: 175 Color: 3
Size: 157 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 3
Size: 108 Color: 3
Size: 108 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 321 Color: 4
Size: 188 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 3
Size: 358 Color: 1
Size: 191 Color: 2

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 4
Size: 127 Color: 3
Size: 114 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 0
Size: 258 Color: 1
Size: 154 Color: 1

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 4
Size: 302 Color: 1

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 475 Color: 3

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 4

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 4
Size: 463 Color: 2

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 344 Color: 3

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 4

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 3

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 4
Size: 110 Color: 3
Size: 109 Color: 4

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 0
Size: 213 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 1
Size: 140 Color: 4
Size: 137 Color: 4

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 3
Size: 135 Color: 2
Size: 121 Color: 2

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 215 Color: 0

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 3

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 4

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 3
Size: 274 Color: 1

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 417 Color: 2

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 3
Size: 234 Color: 2

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 3
Size: 234 Color: 0

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 337 Color: 0

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 286 Color: 1

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 4

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 4

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 249 Color: 1

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 249 Color: 3

Bin 369: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 419 Color: 3

Bin 370: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 2

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 3
Size: 495 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 273 Color: 4
Size: 271 Color: 0

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 1
Size: 235 Color: 2

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 4
Size: 289 Color: 2

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 4
Size: 233 Color: 0

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 4

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 249 Color: 3

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 4
Size: 274 Color: 3

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 329 Color: 3

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 209 Color: 3

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 3
Size: 214 Color: 1

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 1

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 413 Color: 2

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 3
Size: 378 Color: 1

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 2
Size: 465 Color: 3

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 1

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 1
Size: 274 Color: 0

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 228 Color: 1

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 4

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 2

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 252 Color: 3

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 0

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 1
Size: 224 Color: 2

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 0
Size: 323 Color: 1

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 406 Color: 0

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 249 Color: 2

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 239 Color: 4

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 218 Color: 2

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 210 Color: 1

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 274 Color: 4

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 1

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 4
Size: 202 Color: 0

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 2

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 2

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 4

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 1
Size: 150 Color: 0
Size: 148 Color: 2

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 260 Color: 4

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 2

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 0

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 3

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 4

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 2
Size: 230 Color: 4

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 0
Size: 238 Color: 3

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 283 Color: 1

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 3
Size: 466 Color: 4

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 377 Color: 4

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 791 Color: 0
Size: 110 Color: 3
Size: 100 Color: 1

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 2

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 4
Size: 485 Color: 0

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 4
Size: 400 Color: 2

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 390 Color: 1

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 2
Size: 458 Color: 1

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 499 Color: 4

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 482 Color: 2

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 2
Size: 216 Color: 3

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 2
Size: 276 Color: 3

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 0
Size: 415 Color: 3

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 2

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 3
Size: 390 Color: 2

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 3

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 203 Color: 0

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 2

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 2
Size: 246 Color: 3

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 286 Color: 4

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 0

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 405 Color: 0

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 371 Color: 1

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 371 Color: 1

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 3
Size: 217 Color: 1

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 0
Size: 225 Color: 2

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 474 Color: 3

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 385 Color: 1

Bin 444: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 2

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 2
Size: 315 Color: 4

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 3
Size: 257 Color: 1

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 4

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 2

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 1
Size: 216 Color: 0

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 218 Color: 3

Bin 451: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 276 Color: 1

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 4

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 4
Size: 452 Color: 3

Bin 454: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 1
Size: 433 Color: 2

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 408 Color: 4

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 3
Size: 322 Color: 4

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 789 Color: 2
Size: 106 Color: 2
Size: 106 Color: 1

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 3
Size: 408 Color: 0

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 496 Color: 2

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 0

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 413 Color: 1

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 3
Size: 317 Color: 4

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 0
Size: 238 Color: 1

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 493 Color: 2

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 4
Size: 493 Color: 2

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 237 Color: 2

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 2
Size: 432 Color: 3

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 0

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 2
Size: 413 Color: 0

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 2
Size: 345 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 1
Size: 164 Color: 2
Size: 113 Color: 0

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 332 Color: 0

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 2
Size: 323 Color: 1

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 4
Size: 323 Color: 0

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 0

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 1
Size: 233 Color: 4

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 4
Size: 402 Color: 2

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 248 Color: 3

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 369 Color: 3

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 3
Size: 486 Color: 2

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 3
Size: 362 Color: 2

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 2

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 1
Size: 372 Color: 3

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 1

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 214 Color: 0

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 1
Size: 325 Color: 4

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 0

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 482 Color: 2

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 3

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 2

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 0
Size: 406 Color: 2

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 2
Size: 290 Color: 4

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 1

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 1

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 4

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 2
Size: 461 Color: 3

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 339 Color: 4

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 416 Color: 4

Bin 499: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 4

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 4
Size: 345 Color: 2

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 1

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 0

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 0
Size: 258 Color: 4

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 4

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 486 Color: 3

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 3
Size: 489 Color: 2

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 407 Color: 0

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 407 Color: 0

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 321 Color: 1

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 3

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 1
Size: 490 Color: 3

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 4
Size: 305 Color: 3

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 4
Size: 404 Color: 1

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 2

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 308 Color: 2

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 308 Color: 0

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 4
Size: 351 Color: 1

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 249 Color: 3

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 2
Size: 401 Color: 0

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 284 Color: 4

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 0
Size: 324 Color: 4

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 4

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 303 Color: 3

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 4
Size: 447 Color: 1

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 0

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 497 Color: 4

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 497 Color: 2

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 3
Size: 444 Color: 0

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 2
Size: 445 Color: 3

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 3
Size: 284 Color: 2

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 4
Size: 433 Color: 2

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 4
Size: 210 Color: 2

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 376 Color: 2

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 3
Size: 495 Color: 2

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 486 Color: 3

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 4

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 351 Color: 3

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 3

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 2
Size: 324 Color: 4

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 3

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 2

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 325 Color: 2

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 0
Size: 406 Color: 3

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 325 Color: 1

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 4

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 0

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 395 Color: 4

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 1

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 416 Color: 3

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 496 Color: 4

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 3
Size: 353 Color: 0

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 3
Size: 353 Color: 2

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 355 Color: 1

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 0
Size: 480 Color: 1

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 477 Color: 1

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 3
Size: 355 Color: 2

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 3
Size: 258 Color: 2

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 2

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 314 Color: 1

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 362 Color: 1

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 329 Color: 3

Bin 562: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 2

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 4

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 3

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 3
Size: 225 Color: 4

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 222 Color: 4

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 4

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 2

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 4

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 3
Size: 411 Color: 0

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 491 Color: 3

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 0

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 4
Size: 343 Color: 1

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 0
Size: 337 Color: 1

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 4
Size: 343 Color: 0

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 1
Size: 270 Color: 3

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 3
Size: 355 Color: 2

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 3
Size: 318 Color: 2

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 2

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 3
Size: 453 Color: 4

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 0
Size: 408 Color: 4

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 4
Size: 296 Color: 3

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 3
Size: 316 Color: 1

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 1

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 3
Size: 289 Color: 2

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 4
Size: 287 Color: 0

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 287 Color: 2

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 2

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 0
Size: 409 Color: 3

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 409 Color: 2

Bin 591: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 284 Color: 0

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 3
Size: 284 Color: 4

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 2

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 2
Size: 263 Color: 0

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 0

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 248 Color: 2

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 3
Size: 248 Color: 1

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 2
Size: 429 Color: 1

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 214 Color: 1

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 4
Size: 259 Color: 2

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 258 Color: 1

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 1
Size: 214 Color: 3

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 3
Size: 438 Color: 4

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 496 Color: 4

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 3

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 4

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 2
Size: 219 Color: 3

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 1
Size: 232 Color: 0

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 3

Bin 610: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 0
Size: 337 Color: 1

Bin 611: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 335 Color: 3

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 1
Size: 385 Color: 2

Bin 613: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 2

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 237 Color: 0

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 1
Size: 231 Color: 3

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 0
Size: 330 Color: 3

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 0
Size: 334 Color: 3

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 410 Color: 4

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 3
Size: 354 Color: 2

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 3

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 2
Size: 499 Color: 1

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 3

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 498 Color: 4

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 3

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 494 Color: 0

Bin 626: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 4
Size: 494 Color: 3

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 491 Color: 0

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 4
Size: 484 Color: 2

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 4
Size: 482 Color: 0

Bin 630: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 3

Bin 631: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 481 Color: 1

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 480 Color: 4

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 2
Size: 478 Color: 4

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 0
Size: 478 Color: 4

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 4
Size: 468 Color: 1

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 464 Color: 3

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 2
Size: 462 Color: 3

Bin 638: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 455 Color: 4

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 451 Color: 3

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 3

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 440 Color: 4

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 1

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 1

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 1

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 428 Color: 0

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 2
Size: 426 Color: 0

Bin 647: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 0
Size: 426 Color: 4

Bin 648: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 2
Size: 424 Color: 3

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 425 Color: 3

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 4
Size: 425 Color: 0

Bin 651: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 4

Bin 652: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 4

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 4
Size: 418 Color: 0

Bin 654: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 0

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 417 Color: 4

Bin 656: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 0
Size: 414 Color: 1

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 2
Size: 411 Color: 3

Bin 658: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 410 Color: 0

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 409 Color: 0

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 2

Bin 661: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 1

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 2
Size: 399 Color: 3

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 0
Size: 398 Color: 2

Bin 664: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 0

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 1
Size: 391 Color: 4

Bin 666: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 386 Color: 2

Bin 667: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 4
Size: 386 Color: 3

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 3

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 4
Size: 378 Color: 3

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 362 Color: 3

Bin 671: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 3
Size: 361 Color: 0

Bin 672: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 4

Bin 673: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 354 Color: 3

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 3
Size: 343 Color: 0

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 340 Color: 0

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 339 Color: 4

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 3

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 3
Size: 334 Color: 2

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 333 Color: 2

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 332 Color: 4

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 4

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 3
Size: 328 Color: 4

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 0
Size: 326 Color: 4

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 0

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 2

Bin 686: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 2

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 2
Size: 320 Color: 4

Bin 688: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 2
Size: 320 Color: 0

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 319 Color: 0

Bin 690: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 2
Size: 298 Color: 1

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 1
Size: 292 Color: 4

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 0

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 1

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 285 Color: 1

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 1

Bin 696: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 3
Size: 281 Color: 1

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 0

Bin 698: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 4

Bin 699: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 2
Size: 251 Color: 0

Bin 700: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 2

Bin 701: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 243 Color: 4

Bin 702: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 0
Size: 243 Color: 2

Bin 703: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 0
Size: 236 Color: 4

Bin 704: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 2
Size: 235 Color: 0

Bin 705: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 2
Size: 230 Color: 0

Bin 706: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 2

Bin 707: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 4
Size: 226 Color: 1

Bin 708: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 1
Size: 218 Color: 4

Bin 709: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 3

Bin 710: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 1
Size: 214 Color: 3

Bin 711: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 2

Bin 712: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 2
Size: 212 Color: 1

Bin 713: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 2
Size: 210 Color: 1

Bin 714: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 4
Size: 205 Color: 3

Bin 715: 1 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 2
Size: 273 Color: 3
Size: 167 Color: 0

Bin 716: 1 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 1
Size: 188 Color: 0
Size: 176 Color: 3

Bin 717: 1 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 2
Size: 131 Color: 2
Size: 130 Color: 3

Bin 718: 1 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 239 Color: 3

Bin 719: 1 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 294 Color: 4

Bin 720: 1 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 246 Color: 3

Bin 721: 1 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 295 Color: 0

Bin 722: 1 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 0
Size: 448 Color: 2

Bin 723: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 2
Size: 381 Color: 4

Bin 724: 1 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 4
Size: 313 Color: 3

Bin 725: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 373 Color: 4

Bin 726: 1 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 1
Size: 446 Color: 0

Bin 727: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 2
Size: 396 Color: 0

Bin 728: 1 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 3
Size: 261 Color: 2

Bin 729: 1 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 3
Size: 458 Color: 4

Bin 730: 1 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 1
Size: 347 Color: 0

Bin 731: 1 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 2
Size: 347 Color: 0

Bin 732: 1 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 350 Color: 4

Bin 733: 1 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 0
Size: 348 Color: 2

Bin 734: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 367 Color: 1

Bin 735: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 367 Color: 1

Bin 736: 1 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 415 Color: 4

Bin 737: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 203 Color: 1

Bin 738: 1 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 328 Color: 4

Bin 739: 1 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 472 Color: 1

Bin 740: 1 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 2
Size: 356 Color: 3

Bin 741: 1 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 3
Size: 400 Color: 0

Bin 742: 1 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 448 Color: 0

Bin 743: 1 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 471 Color: 2

Bin 744: 1 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 4
Size: 411 Color: 2

Bin 745: 1 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 0
Size: 411 Color: 3

Bin 746: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 373 Color: 0

Bin 747: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 268 Color: 4

Bin 748: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 268 Color: 4

Bin 749: 1 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 220 Color: 0

Bin 750: 1 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 294 Color: 3

Bin 751: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 396 Color: 0

Bin 752: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 392 Color: 2

Bin 753: 1 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 2
Size: 223 Color: 0

Bin 754: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 386 Color: 0

Bin 755: 1 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 285 Color: 0

Bin 756: 1 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 0
Size: 200 Color: 2

Bin 757: 1 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 0
Size: 429 Color: 1

Bin 758: 1 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 3
Size: 133 Color: 0
Size: 104 Color: 3

Bin 759: 1 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 303 Color: 4

Bin 760: 1 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 2
Size: 252 Color: 3

Bin 761: 1 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 462 Color: 1

Bin 762: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 4
Size: 305 Color: 1

Bin 763: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 4
Size: 259 Color: 3

Bin 764: 1 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 218 Color: 3

Bin 765: 1 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 338 Color: 2

Bin 766: 1 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 3
Size: 228 Color: 4

Bin 767: 1 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 494 Color: 3

Bin 768: 1 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 3
Size: 228 Color: 0

Bin 769: 1 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 2
Size: 411 Color: 1

Bin 770: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 492 Color: 1

Bin 771: 1 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 242 Color: 2

Bin 772: 1 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 2
Size: 415 Color: 3

Bin 773: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 290 Color: 0

Bin 774: 1 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 3
Size: 285 Color: 0

Bin 775: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 0
Size: 483 Color: 3

Bin 776: 1 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 454 Color: 3

Bin 777: 1 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 343 Color: 4

Bin 778: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 488 Color: 3

Bin 779: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 392 Color: 3

Bin 780: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 492 Color: 1

Bin 781: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 1
Size: 305 Color: 3

Bin 782: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 474 Color: 3

Bin 783: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 2
Size: 492 Color: 4

Bin 784: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 2
Size: 290 Color: 3

Bin 785: 1 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 3
Size: 315 Color: 0

Bin 786: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 392 Color: 4

Bin 787: 1 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 214 Color: 1

Bin 788: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 305 Color: 2

Bin 789: 1 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 3
Size: 500 Color: 0

Bin 790: 1 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 0
Size: 389 Color: 3

Bin 791: 1 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 389 Color: 1

Bin 792: 1 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 1
Size: 432 Color: 2

Bin 793: 1 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 1
Size: 400 Color: 3

Bin 794: 1 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 356 Color: 2

Bin 795: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 3
Size: 444 Color: 0

Bin 796: 1 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 0
Size: 318 Color: 2

Bin 797: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 392 Color: 4

Bin 798: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 381 Color: 0

Bin 799: 1 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 480 Color: 2

Bin 800: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 290 Color: 2

Bin 801: 1 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 214 Color: 0

Bin 802: 1 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 337 Color: 0

Bin 803: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 492 Color: 4

Bin 804: 1 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 3
Size: 400 Color: 2

Bin 805: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 0
Size: 483 Color: 2

Bin 806: 1 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 418 Color: 2

Bin 807: 1 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 2
Size: 497 Color: 3

Bin 808: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 488 Color: 3

Bin 809: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 482 Color: 1

Bin 810: 1 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 2
Size: 477 Color: 1

Bin 811: 1 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 454 Color: 0

Bin 812: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 2
Size: 444 Color: 1

Bin 813: 1 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 422 Color: 2

Bin 814: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 396 Color: 2

Bin 815: 1 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 285 Color: 1

Bin 816: 1 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 3
Size: 228 Color: 4

Bin 817: 1 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 227 Color: 3

Bin 818: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 4
Size: 225 Color: 0

Bin 819: 1 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 214 Color: 1

Bin 820: 2 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 356 Color: 1
Size: 144 Color: 0

Bin 821: 2 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 285 Color: 0

Bin 822: 2 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 242 Color: 2

Bin 823: 2 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 0
Size: 423 Color: 3

Bin 824: 2 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 4
Size: 424 Color: 2

Bin 825: 2 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 3
Size: 437 Color: 1

Bin 826: 2 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 315 Color: 1

Bin 827: 2 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 1
Size: 457 Color: 2

Bin 828: 2 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 0
Size: 347 Color: 3

Bin 829: 2 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 2
Size: 350 Color: 4

Bin 830: 2 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 2
Size: 294 Color: 3

Bin 831: 2 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 242 Color: 4

Bin 832: 2 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 2
Size: 381 Color: 4

Bin 833: 2 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 399 Color: 1

Bin 834: 2 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 338 Color: 0

Bin 835: 2 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 360 Color: 3

Bin 836: 2 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 457 Color: 2

Bin 837: 2 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 2
Size: 350 Color: 3

Bin 838: 2 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 227 Color: 2

Bin 839: 2 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 4
Size: 375 Color: 1

Bin 840: 2 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 448 Color: 0

Bin 841: 2 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 3
Size: 437 Color: 4

Bin 842: 2 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 388 Color: 2

Bin 843: 2 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 315 Color: 0

Bin 844: 2 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 294 Color: 1

Bin 845: 2 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 391 Color: 1

Bin 846: 2 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 290 Color: 0

Bin 847: 2 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 428 Color: 1

Bin 848: 2 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 428 Color: 2

Bin 849: 2 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 417 Color: 1

Bin 850: 2 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 302 Color: 1

Bin 851: 2 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 4
Size: 499 Color: 2

Bin 852: 2 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 4
Size: 258 Color: 3

Bin 853: 2 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 242 Color: 4

Bin 854: 2 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 417 Color: 2

Bin 855: 2 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 2
Size: 482 Color: 1

Bin 856: 2 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 453 Color: 4

Bin 857: 2 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 315 Color: 2

Bin 858: 2 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 2
Size: 385 Color: 1

Bin 859: 2 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 428 Color: 1

Bin 860: 2 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 400 Color: 2

Bin 861: 2 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 496 Color: 0

Bin 862: 2 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 406 Color: 0

Bin 863: 2 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 4
Size: 496 Color: 3

Bin 864: 2 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 1
Size: 328 Color: 0

Bin 865: 2 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 305 Color: 1

Bin 866: 2 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 4
Size: 217 Color: 0

Bin 867: 3 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 2
Size: 120 Color: 3
Size: 118 Color: 1

Bin 868: 3 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 4
Size: 346 Color: 2

Bin 869: 3 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 470 Color: 1

Bin 870: 3 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 2
Size: 470 Color: 0

Bin 871: 3 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 3
Size: 273 Color: 1

Bin 872: 3 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 461 Color: 1

Bin 873: 3 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 461 Color: 1

Bin 874: 3 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 460 Color: 0

Bin 875: 3 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 0
Size: 404 Color: 2

Bin 876: 3 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 379 Color: 2

Bin 877: 3 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 198 Color: 0

Bin 878: 3 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 1
Size: 250 Color: 0

Bin 879: 3 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 379 Color: 2

Bin 880: 3 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 492 Color: 2

Bin 881: 3 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 492 Color: 4

Bin 882: 3 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 1
Size: 213 Color: 0

Bin 883: 4 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 444 Color: 3
Size: 105 Color: 4

Bin 884: 4 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 336 Color: 0

Bin 885: 4 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 336 Color: 4

Bin 886: 4 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 4
Size: 197 Color: 3

Bin 887: 4 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 405 Color: 0

Bin 888: 5 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 4
Size: 257 Color: 3
Size: 169 Color: 1

Bin 889: 5 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 469 Color: 0

Bin 890: 5 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 3
Size: 197 Color: 1

Bin 891: 6 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 458 Color: 2

Bin 892: 6 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 1
Size: 359 Color: 2

Bin 893: 6 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 1
Size: 414 Color: 0

Bin 894: 6 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 469 Color: 1

Bin 895: 6 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 0
Size: 198 Color: 4

Bin 896: 7 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 0
Size: 132 Color: 3
Size: 125 Color: 4

Bin 897: 7 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 469 Color: 3

Bin 898: 7 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 468 Color: 3

Bin 899: 7 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 4
Size: 195 Color: 1

Bin 900: 8 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 4
Size: 196 Color: 3

Bin 901: 9 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 0
Size: 195 Color: 2

Bin 902: 10 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 2
Size: 198 Color: 0

Bin 903: 11 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 197 Color: 1

Bin 904: 13 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 2
Size: 193 Color: 3

Bin 905: 16 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 3
Size: 189 Color: 0

Bin 906: 20 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 3
Size: 421 Color: 4

Bin 907: 21 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 4
Size: 458 Color: 3

Bin 908: 79 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 2
Size: 197 Color: 4
Size: 189 Color: 3

Bin 909: 89 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 348 Color: 2
Size: 183 Color: 0

Bin 910: 98 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 3
Size: 403 Color: 2

Bin 911: 130 of cap free
Amount of items: 4
Items: 
Size: 311 Color: 0
Size: 191 Color: 2
Size: 187 Color: 3
Size: 182 Color: 1

Total size: 911067
Total free space: 844

