Capicity Bin: 1000001
Lower Bound: 906

Bins used: 907
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 763858 Color: 0
Size: 132533 Color: 0
Size: 103610 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 737778 Color: 1
Size: 139858 Color: 1
Size: 122365 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 510667 Color: 0
Size: 380937 Color: 1
Size: 108397 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 378282 Color: 1
Size: 311122 Color: 0
Size: 310597 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 736672 Color: 1
Size: 147264 Color: 1
Size: 116065 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 714407 Color: 0
Size: 175291 Color: 1
Size: 110303 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 718293 Color: 0
Size: 142730 Color: 1
Size: 138978 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 589521 Color: 1
Size: 217999 Color: 0
Size: 192481 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 575258 Color: 0
Size: 320114 Color: 1
Size: 104629 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 703675 Color: 1
Size: 161725 Color: 0
Size: 134601 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 715048 Color: 0
Size: 184868 Color: 0
Size: 100085 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 466186 Color: 0
Size: 380642 Color: 1
Size: 153173 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 467055 Color: 0
Size: 267857 Color: 0
Size: 265089 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 502846 Color: 0
Size: 324891 Color: 0
Size: 172264 Color: 1

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 579062 Color: 0
Size: 420939 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 456325 Color: 1
Size: 435586 Color: 1
Size: 108090 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 709385 Color: 0
Size: 145974 Color: 0
Size: 144642 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 770767 Color: 0
Size: 121799 Color: 1
Size: 107435 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 658969 Color: 1
Size: 193176 Color: 1
Size: 147855 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 702874 Color: 0
Size: 196802 Color: 1
Size: 100324 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 642193 Color: 0
Size: 182729 Color: 1
Size: 175078 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 734319 Color: 0
Size: 135435 Color: 1
Size: 130246 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 566061 Color: 0
Size: 291299 Color: 1
Size: 142640 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 753627 Color: 0
Size: 141901 Color: 1
Size: 104472 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 658408 Color: 1
Size: 174324 Color: 1
Size: 167268 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 500688 Color: 0
Size: 372195 Color: 1
Size: 127117 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 686262 Color: 0
Size: 164334 Color: 1
Size: 149404 Color: 0

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 682817 Color: 0
Size: 317183 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 732288 Color: 0
Size: 134328 Color: 0
Size: 133384 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 469203 Color: 0
Size: 380222 Color: 1
Size: 150575 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 466880 Color: 0
Size: 378554 Color: 1
Size: 154566 Color: 0

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 681481 Color: 1
Size: 190114 Color: 1
Size: 128405 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 797057 Color: 0
Size: 202943 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 502343 Color: 1
Size: 311295 Color: 0
Size: 186361 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 449444 Color: 1
Size: 447143 Color: 0
Size: 103412 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 609081 Color: 0
Size: 255828 Color: 1
Size: 135090 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 411263 Color: 0
Size: 340427 Color: 1
Size: 248309 Color: 0

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 583145 Color: 1
Size: 416854 Color: 0

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 703411 Color: 1
Size: 171702 Color: 1
Size: 124886 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 753226 Color: 1
Size: 144558 Color: 0
Size: 102215 Color: 0

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 590092 Color: 0
Size: 250164 Color: 1
Size: 159742 Color: 0

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 440722 Color: 1
Size: 373700 Color: 0
Size: 185576 Color: 1

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 519247 Color: 0
Size: 378824 Color: 1
Size: 101927 Color: 0

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 490844 Color: 1
Size: 310672 Color: 0
Size: 198482 Color: 1

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 728771 Color: 0
Size: 164743 Color: 1
Size: 106484 Color: 0

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 442263 Color: 1
Size: 362350 Color: 0
Size: 195385 Color: 1

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 532285 Color: 0
Size: 272338 Color: 0
Size: 195375 Color: 1

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 553113 Color: 0
Size: 446885 Color: 1

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 723280 Color: 0
Size: 142075 Color: 1
Size: 134643 Color: 1

Bin 50: 3 of cap free
Amount of items: 2
Items: 
Size: 776475 Color: 1
Size: 223523 Color: 0

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 578192 Color: 1
Size: 248584 Color: 0
Size: 173221 Color: 0

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 503162 Color: 1
Size: 357968 Color: 0
Size: 138867 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 596660 Color: 1
Size: 252236 Color: 0
Size: 151101 Color: 0

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 682323 Color: 1
Size: 166627 Color: 0
Size: 151047 Color: 0

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 438589 Color: 0
Size: 363995 Color: 0
Size: 197413 Color: 1

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 614221 Color: 1
Size: 193476 Color: 1
Size: 192300 Color: 0

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 449424 Color: 1
Size: 301282 Color: 1
Size: 249291 Color: 0

Bin 58: 5 of cap free
Amount of items: 3
Items: 
Size: 685974 Color: 0
Size: 195163 Color: 1
Size: 118859 Color: 0

Bin 59: 5 of cap free
Amount of items: 3
Items: 
Size: 468292 Color: 1
Size: 366306 Color: 0
Size: 165398 Color: 0

Bin 60: 5 of cap free
Amount of items: 3
Items: 
Size: 601549 Color: 1
Size: 249652 Color: 0
Size: 148795 Color: 0

Bin 61: 5 of cap free
Amount of items: 2
Items: 
Size: 744052 Color: 0
Size: 255944 Color: 1

Bin 62: 6 of cap free
Amount of items: 3
Items: 
Size: 753386 Color: 1
Size: 136899 Color: 0
Size: 109710 Color: 1

Bin 63: 6 of cap free
Amount of items: 3
Items: 
Size: 508596 Color: 1
Size: 347657 Color: 0
Size: 143742 Color: 1

Bin 64: 6 of cap free
Amount of items: 2
Items: 
Size: 506068 Color: 0
Size: 493927 Color: 1

Bin 65: 6 of cap free
Amount of items: 2
Items: 
Size: 740498 Color: 1
Size: 259497 Color: 0

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 652519 Color: 0
Size: 347476 Color: 1

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 664157 Color: 0
Size: 335838 Color: 1

Bin 68: 7 of cap free
Amount of items: 3
Items: 
Size: 511955 Color: 0
Size: 374011 Color: 1
Size: 114028 Color: 1

Bin 69: 7 of cap free
Amount of items: 3
Items: 
Size: 712267 Color: 1
Size: 158227 Color: 1
Size: 129500 Color: 0

Bin 70: 8 of cap free
Amount of items: 3
Items: 
Size: 709810 Color: 0
Size: 172159 Color: 1
Size: 118024 Color: 1

Bin 71: 8 of cap free
Amount of items: 3
Items: 
Size: 776345 Color: 0
Size: 115528 Color: 1
Size: 108120 Color: 0

Bin 72: 8 of cap free
Amount of items: 3
Items: 
Size: 660836 Color: 1
Size: 177944 Color: 0
Size: 161213 Color: 1

Bin 73: 8 of cap free
Amount of items: 2
Items: 
Size: 625462 Color: 1
Size: 374531 Color: 0

Bin 74: 9 of cap free
Amount of items: 2
Items: 
Size: 565282 Color: 0
Size: 434710 Color: 1

Bin 75: 10 of cap free
Amount of items: 3
Items: 
Size: 703167 Color: 1
Size: 174155 Color: 0
Size: 122669 Color: 1

Bin 76: 11 of cap free
Amount of items: 2
Items: 
Size: 502372 Color: 0
Size: 497618 Color: 1

Bin 77: 11 of cap free
Amount of items: 3
Items: 
Size: 660847 Color: 1
Size: 173222 Color: 0
Size: 165921 Color: 0

Bin 78: 11 of cap free
Amount of items: 3
Items: 
Size: 589436 Color: 0
Size: 249032 Color: 0
Size: 161522 Color: 1

Bin 79: 11 of cap free
Amount of items: 2
Items: 
Size: 771155 Color: 0
Size: 228835 Color: 1

Bin 80: 12 of cap free
Amount of items: 3
Items: 
Size: 709929 Color: 0
Size: 187472 Color: 0
Size: 102588 Color: 1

Bin 81: 12 of cap free
Amount of items: 3
Items: 
Size: 569746 Color: 0
Size: 252018 Color: 0
Size: 178225 Color: 1

Bin 82: 13 of cap free
Amount of items: 2
Items: 
Size: 739255 Color: 0
Size: 260733 Color: 1

Bin 83: 13 of cap free
Amount of items: 3
Items: 
Size: 502589 Color: 1
Size: 353549 Color: 0
Size: 143850 Color: 1

Bin 84: 13 of cap free
Amount of items: 2
Items: 
Size: 736623 Color: 1
Size: 263365 Color: 0

Bin 85: 13 of cap free
Amount of items: 2
Items: 
Size: 787529 Color: 1
Size: 212459 Color: 0

Bin 86: 14 of cap free
Amount of items: 2
Items: 
Size: 781966 Color: 1
Size: 218021 Color: 0

Bin 87: 15 of cap free
Amount of items: 3
Items: 
Size: 499049 Color: 0
Size: 395286 Color: 1
Size: 105651 Color: 1

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 781759 Color: 0
Size: 218227 Color: 1

Bin 89: 16 of cap free
Amount of items: 3
Items: 
Size: 606983 Color: 0
Size: 237784 Color: 0
Size: 155218 Color: 1

Bin 90: 16 of cap free
Amount of items: 3
Items: 
Size: 467345 Color: 0
Size: 356920 Color: 0
Size: 175720 Color: 1

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 501280 Color: 0
Size: 498705 Color: 1

Bin 92: 16 of cap free
Amount of items: 2
Items: 
Size: 762397 Color: 1
Size: 237588 Color: 0

Bin 93: 17 of cap free
Amount of items: 3
Items: 
Size: 605725 Color: 0
Size: 204687 Color: 0
Size: 189572 Color: 1

Bin 94: 17 of cap free
Amount of items: 3
Items: 
Size: 583090 Color: 0
Size: 250332 Color: 1
Size: 166562 Color: 0

Bin 95: 17 of cap free
Amount of items: 2
Items: 
Size: 680504 Color: 1
Size: 319480 Color: 0

Bin 96: 18 of cap free
Amount of items: 3
Items: 
Size: 574569 Color: 0
Size: 248862 Color: 0
Size: 176552 Color: 1

Bin 97: 18 of cap free
Amount of items: 3
Items: 
Size: 568804 Color: 0
Size: 300819 Color: 1
Size: 130360 Color: 0

Bin 98: 18 of cap free
Amount of items: 3
Items: 
Size: 680891 Color: 1
Size: 184221 Color: 0
Size: 134871 Color: 1

Bin 99: 18 of cap free
Amount of items: 3
Items: 
Size: 702473 Color: 1
Size: 149525 Color: 0
Size: 147985 Color: 1

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 638031 Color: 1
Size: 361952 Color: 0

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 556510 Color: 0
Size: 443473 Color: 1

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 596688 Color: 1
Size: 403295 Color: 0

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 797040 Color: 1
Size: 202943 Color: 0

Bin 104: 19 of cap free
Amount of items: 3
Items: 
Size: 503160 Color: 1
Size: 310880 Color: 0
Size: 185942 Color: 0

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 663107 Color: 1
Size: 336875 Color: 0

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 702555 Color: 1
Size: 157976 Color: 1
Size: 139450 Color: 0

Bin 107: 20 of cap free
Amount of items: 3
Items: 
Size: 568701 Color: 0
Size: 325640 Color: 0
Size: 105640 Color: 1

Bin 108: 20 of cap free
Amount of items: 3
Items: 
Size: 764737 Color: 0
Size: 123868 Color: 0
Size: 111376 Color: 1

Bin 109: 21 of cap free
Amount of items: 2
Items: 
Size: 719079 Color: 1
Size: 280901 Color: 0

Bin 110: 21 of cap free
Amount of items: 3
Items: 
Size: 425818 Color: 0
Size: 395924 Color: 1
Size: 178238 Color: 0

Bin 111: 21 of cap free
Amount of items: 2
Items: 
Size: 646934 Color: 1
Size: 353046 Color: 0

Bin 112: 22 of cap free
Amount of items: 3
Items: 
Size: 608957 Color: 0
Size: 199370 Color: 1
Size: 191652 Color: 0

Bin 113: 22 of cap free
Amount of items: 3
Items: 
Size: 683389 Color: 0
Size: 198268 Color: 0
Size: 118322 Color: 1

Bin 114: 22 of cap free
Amount of items: 2
Items: 
Size: 684881 Color: 0
Size: 315098 Color: 1

Bin 115: 23 of cap free
Amount of items: 3
Items: 
Size: 681452 Color: 1
Size: 170598 Color: 0
Size: 147928 Color: 0

Bin 116: 23 of cap free
Amount of items: 3
Items: 
Size: 673222 Color: 0
Size: 173414 Color: 1
Size: 153342 Color: 0

Bin 117: 23 of cap free
Amount of items: 2
Items: 
Size: 689856 Color: 1
Size: 310122 Color: 0

Bin 118: 23 of cap free
Amount of items: 2
Items: 
Size: 776805 Color: 1
Size: 223173 Color: 0

Bin 119: 25 of cap free
Amount of items: 2
Items: 
Size: 769704 Color: 1
Size: 230272 Color: 0

Bin 120: 26 of cap free
Amount of items: 3
Items: 
Size: 642518 Color: 0
Size: 182455 Color: 0
Size: 175002 Color: 1

Bin 121: 27 of cap free
Amount of items: 2
Items: 
Size: 746926 Color: 0
Size: 253048 Color: 1

Bin 122: 28 of cap free
Amount of items: 2
Items: 
Size: 556094 Color: 0
Size: 443879 Color: 1

Bin 123: 28 of cap free
Amount of items: 2
Items: 
Size: 646406 Color: 0
Size: 353567 Color: 1

Bin 124: 29 of cap free
Amount of items: 3
Items: 
Size: 640709 Color: 1
Size: 251693 Color: 0
Size: 107570 Color: 0

Bin 125: 29 of cap free
Amount of items: 3
Items: 
Size: 640737 Color: 0
Size: 197133 Color: 0
Size: 162102 Color: 1

Bin 126: 29 of cap free
Amount of items: 3
Items: 
Size: 703515 Color: 1
Size: 164038 Color: 0
Size: 132419 Color: 0

Bin 127: 29 of cap free
Amount of items: 2
Items: 
Size: 626443 Color: 0
Size: 373529 Color: 1

Bin 128: 30 of cap free
Amount of items: 2
Items: 
Size: 725832 Color: 1
Size: 274139 Color: 0

Bin 129: 33 of cap free
Amount of items: 3
Items: 
Size: 764648 Color: 0
Size: 119703 Color: 0
Size: 115617 Color: 1

Bin 130: 33 of cap free
Amount of items: 2
Items: 
Size: 571604 Color: 0
Size: 428364 Color: 1

Bin 131: 33 of cap free
Amount of items: 2
Items: 
Size: 751678 Color: 0
Size: 248290 Color: 1

Bin 132: 36 of cap free
Amount of items: 3
Items: 
Size: 658750 Color: 1
Size: 193417 Color: 0
Size: 147798 Color: 1

Bin 133: 36 of cap free
Amount of items: 2
Items: 
Size: 607233 Color: 1
Size: 392732 Color: 0

Bin 134: 37 of cap free
Amount of items: 2
Items: 
Size: 766538 Color: 1
Size: 233426 Color: 0

Bin 135: 38 of cap free
Amount of items: 3
Items: 
Size: 714728 Color: 0
Size: 147445 Color: 0
Size: 137790 Color: 1

Bin 136: 38 of cap free
Amount of items: 2
Items: 
Size: 762181 Color: 0
Size: 237782 Color: 1

Bin 137: 39 of cap free
Amount of items: 3
Items: 
Size: 768662 Color: 0
Size: 122086 Color: 1
Size: 109214 Color: 1

Bin 138: 39 of cap free
Amount of items: 2
Items: 
Size: 605480 Color: 0
Size: 394482 Color: 1

Bin 139: 40 of cap free
Amount of items: 3
Items: 
Size: 701277 Color: 1
Size: 190454 Color: 1
Size: 108230 Color: 0

Bin 140: 40 of cap free
Amount of items: 2
Items: 
Size: 662094 Color: 0
Size: 337867 Color: 1

Bin 141: 41 of cap free
Amount of items: 3
Items: 
Size: 606090 Color: 0
Size: 198817 Color: 0
Size: 195053 Color: 1

Bin 142: 41 of cap free
Amount of items: 3
Items: 
Size: 737695 Color: 1
Size: 148557 Color: 0
Size: 113708 Color: 1

Bin 143: 43 of cap free
Amount of items: 2
Items: 
Size: 649090 Color: 0
Size: 350868 Color: 1

Bin 144: 43 of cap free
Amount of items: 2
Items: 
Size: 747363 Color: 0
Size: 252595 Color: 1

Bin 145: 44 of cap free
Amount of items: 3
Items: 
Size: 639769 Color: 1
Size: 206252 Color: 1
Size: 153936 Color: 0

Bin 146: 44 of cap free
Amount of items: 2
Items: 
Size: 556885 Color: 0
Size: 443072 Color: 1

Bin 147: 44 of cap free
Amount of items: 2
Items: 
Size: 669009 Color: 1
Size: 330948 Color: 0

Bin 148: 45 of cap free
Amount of items: 2
Items: 
Size: 547752 Color: 1
Size: 452204 Color: 0

Bin 149: 45 of cap free
Amount of items: 2
Items: 
Size: 602139 Color: 0
Size: 397817 Color: 1

Bin 150: 46 of cap free
Amount of items: 3
Items: 
Size: 581963 Color: 0
Size: 252460 Color: 1
Size: 165532 Color: 0

Bin 151: 46 of cap free
Amount of items: 2
Items: 
Size: 502397 Color: 0
Size: 497558 Color: 1

Bin 152: 47 of cap free
Amount of items: 2
Items: 
Size: 540008 Color: 0
Size: 459946 Color: 1

Bin 153: 48 of cap free
Amount of items: 2
Items: 
Size: 587808 Color: 1
Size: 412145 Color: 0

Bin 154: 48 of cap free
Amount of items: 2
Items: 
Size: 504046 Color: 0
Size: 495907 Color: 1

Bin 155: 49 of cap free
Amount of items: 2
Items: 
Size: 643769 Color: 0
Size: 356183 Color: 1

Bin 156: 50 of cap free
Amount of items: 2
Items: 
Size: 523527 Color: 1
Size: 476424 Color: 0

Bin 157: 51 of cap free
Amount of items: 2
Items: 
Size: 521598 Color: 1
Size: 478352 Color: 0

Bin 158: 52 of cap free
Amount of items: 3
Items: 
Size: 702551 Color: 1
Size: 171076 Color: 0
Size: 126322 Color: 0

Bin 159: 52 of cap free
Amount of items: 3
Items: 
Size: 405583 Color: 0
Size: 372920 Color: 1
Size: 221446 Color: 0

Bin 160: 52 of cap free
Amount of items: 2
Items: 
Size: 760595 Color: 0
Size: 239354 Color: 1

Bin 161: 53 of cap free
Amount of items: 3
Items: 
Size: 490490 Color: 1
Size: 361404 Color: 1
Size: 148054 Color: 0

Bin 162: 53 of cap free
Amount of items: 2
Items: 
Size: 707396 Color: 0
Size: 292552 Color: 1

Bin 163: 54 of cap free
Amount of items: 3
Items: 
Size: 752537 Color: 1
Size: 137635 Color: 1
Size: 109775 Color: 0

Bin 164: 55 of cap free
Amount of items: 3
Items: 
Size: 475035 Color: 1
Size: 388264 Color: 0
Size: 136647 Color: 0

Bin 165: 55 of cap free
Amount of items: 3
Items: 
Size: 762713 Color: 0
Size: 128700 Color: 1
Size: 108533 Color: 1

Bin 166: 55 of cap free
Amount of items: 3
Items: 
Size: 472884 Color: 0
Size: 363051 Color: 0
Size: 164011 Color: 1

Bin 167: 56 of cap free
Amount of items: 2
Items: 
Size: 564151 Color: 0
Size: 435794 Color: 1

Bin 168: 57 of cap free
Amount of items: 3
Items: 
Size: 768587 Color: 0
Size: 130466 Color: 1
Size: 100891 Color: 0

Bin 169: 58 of cap free
Amount of items: 3
Items: 
Size: 701970 Color: 1
Size: 196029 Color: 1
Size: 101944 Color: 0

Bin 170: 58 of cap free
Amount of items: 2
Items: 
Size: 764364 Color: 1
Size: 235579 Color: 0

Bin 171: 59 of cap free
Amount of items: 2
Items: 
Size: 591357 Color: 1
Size: 408585 Color: 0

Bin 172: 61 of cap free
Amount of items: 2
Items: 
Size: 730399 Color: 0
Size: 269541 Color: 1

Bin 173: 62 of cap free
Amount of items: 3
Items: 
Size: 448335 Color: 0
Size: 373299 Color: 0
Size: 178305 Color: 1

Bin 174: 62 of cap free
Amount of items: 3
Items: 
Size: 579895 Color: 1
Size: 236898 Color: 0
Size: 183146 Color: 0

Bin 175: 63 of cap free
Amount of items: 3
Items: 
Size: 681559 Color: 0
Size: 181322 Color: 0
Size: 137057 Color: 1

Bin 176: 63 of cap free
Amount of items: 2
Items: 
Size: 665777 Color: 1
Size: 334161 Color: 0

Bin 177: 64 of cap free
Amount of items: 3
Items: 
Size: 683106 Color: 0
Size: 175718 Color: 0
Size: 141113 Color: 1

Bin 178: 64 of cap free
Amount of items: 3
Items: 
Size: 660804 Color: 1
Size: 173462 Color: 1
Size: 165671 Color: 0

Bin 179: 64 of cap free
Amount of items: 3
Items: 
Size: 721848 Color: 0
Size: 139486 Color: 1
Size: 138603 Color: 0

Bin 180: 66 of cap free
Amount of items: 2
Items: 
Size: 666714 Color: 1
Size: 333221 Color: 0

Bin 181: 67 of cap free
Amount of items: 2
Items: 
Size: 611441 Color: 0
Size: 388493 Color: 1

Bin 182: 68 of cap free
Amount of items: 2
Items: 
Size: 779674 Color: 1
Size: 220259 Color: 0

Bin 183: 69 of cap free
Amount of items: 3
Items: 
Size: 680579 Color: 1
Size: 192272 Color: 1
Size: 127081 Color: 0

Bin 184: 71 of cap free
Amount of items: 3
Items: 
Size: 684781 Color: 0
Size: 180934 Color: 1
Size: 134215 Color: 0

Bin 185: 74 of cap free
Amount of items: 2
Items: 
Size: 569811 Color: 0
Size: 430116 Color: 1

Bin 186: 74 of cap free
Amount of items: 2
Items: 
Size: 740385 Color: 0
Size: 259542 Color: 1

Bin 187: 74 of cap free
Amount of items: 2
Items: 
Size: 758155 Color: 0
Size: 241772 Color: 1

Bin 188: 75 of cap free
Amount of items: 2
Items: 
Size: 526358 Color: 0
Size: 473568 Color: 1

Bin 189: 75 of cap free
Amount of items: 2
Items: 
Size: 735601 Color: 1
Size: 264325 Color: 0

Bin 190: 76 of cap free
Amount of items: 2
Items: 
Size: 508654 Color: 1
Size: 491271 Color: 0

Bin 191: 76 of cap free
Amount of items: 2
Items: 
Size: 538470 Color: 1
Size: 461455 Color: 0

Bin 192: 76 of cap free
Amount of items: 2
Items: 
Size: 780243 Color: 1
Size: 219682 Color: 0

Bin 193: 76 of cap free
Amount of items: 2
Items: 
Size: 781728 Color: 0
Size: 218197 Color: 1

Bin 194: 77 of cap free
Amount of items: 3
Items: 
Size: 764288 Color: 0
Size: 126359 Color: 1
Size: 109277 Color: 0

Bin 195: 77 of cap free
Amount of items: 3
Items: 
Size: 639985 Color: 1
Size: 217261 Color: 0
Size: 142678 Color: 0

Bin 196: 77 of cap free
Amount of items: 2
Items: 
Size: 531774 Color: 0
Size: 468150 Color: 1

Bin 197: 77 of cap free
Amount of items: 2
Items: 
Size: 648909 Color: 0
Size: 351015 Color: 1

Bin 198: 78 of cap free
Amount of items: 3
Items: 
Size: 680150 Color: 0
Size: 175814 Color: 1
Size: 143959 Color: 1

Bin 199: 78 of cap free
Amount of items: 2
Items: 
Size: 504713 Color: 0
Size: 495210 Color: 1

Bin 200: 80 of cap free
Amount of items: 3
Items: 
Size: 502276 Color: 1
Size: 353145 Color: 0
Size: 144500 Color: 0

Bin 201: 81 of cap free
Amount of items: 2
Items: 
Size: 732332 Color: 1
Size: 267588 Color: 0

Bin 202: 82 of cap free
Amount of items: 2
Items: 
Size: 701672 Color: 1
Size: 298247 Color: 0

Bin 203: 82 of cap free
Amount of items: 2
Items: 
Size: 781299 Color: 1
Size: 218620 Color: 0

Bin 204: 83 of cap free
Amount of items: 2
Items: 
Size: 672816 Color: 0
Size: 327102 Color: 1

Bin 205: 84 of cap free
Amount of items: 2
Items: 
Size: 596809 Color: 0
Size: 403108 Color: 1

Bin 206: 86 of cap free
Amount of items: 2
Items: 
Size: 745554 Color: 0
Size: 254361 Color: 1

Bin 207: 87 of cap free
Amount of items: 3
Items: 
Size: 753280 Color: 1
Size: 127784 Color: 0
Size: 118850 Color: 1

Bin 208: 87 of cap free
Amount of items: 2
Items: 
Size: 649296 Color: 0
Size: 350618 Color: 1

Bin 209: 88 of cap free
Amount of items: 2
Items: 
Size: 719562 Color: 1
Size: 280351 Color: 0

Bin 210: 89 of cap free
Amount of items: 2
Items: 
Size: 578985 Color: 0
Size: 420927 Color: 1

Bin 211: 89 of cap free
Amount of items: 2
Items: 
Size: 784384 Color: 0
Size: 215528 Color: 1

Bin 212: 90 of cap free
Amount of items: 2
Items: 
Size: 616446 Color: 1
Size: 383465 Color: 0

Bin 213: 91 of cap free
Amount of items: 2
Items: 
Size: 556719 Color: 1
Size: 443191 Color: 0

Bin 214: 91 of cap free
Amount of items: 2
Items: 
Size: 572790 Color: 1
Size: 427120 Color: 0

Bin 215: 92 of cap free
Amount of items: 2
Items: 
Size: 673070 Color: 1
Size: 326839 Color: 0

Bin 216: 94 of cap free
Amount of items: 3
Items: 
Size: 370213 Color: 1
Size: 328065 Color: 0
Size: 301629 Color: 1

Bin 217: 94 of cap free
Amount of items: 2
Items: 
Size: 648966 Color: 1
Size: 350941 Color: 0

Bin 218: 96 of cap free
Amount of items: 2
Items: 
Size: 549904 Color: 0
Size: 450001 Color: 1

Bin 219: 96 of cap free
Amount of items: 2
Items: 
Size: 698967 Color: 1
Size: 300938 Color: 0

Bin 220: 96 of cap free
Amount of items: 2
Items: 
Size: 744037 Color: 1
Size: 255868 Color: 0

Bin 221: 96 of cap free
Amount of items: 2
Items: 
Size: 765665 Color: 1
Size: 234240 Color: 0

Bin 222: 97 of cap free
Amount of items: 2
Items: 
Size: 535207 Color: 0
Size: 464697 Color: 1

Bin 223: 98 of cap free
Amount of items: 3
Items: 
Size: 764906 Color: 1
Size: 118351 Color: 0
Size: 116646 Color: 1

Bin 224: 98 of cap free
Amount of items: 2
Items: 
Size: 580597 Color: 0
Size: 419306 Color: 1

Bin 225: 98 of cap free
Amount of items: 2
Items: 
Size: 617974 Color: 1
Size: 381929 Color: 0

Bin 226: 99 of cap free
Amount of items: 2
Items: 
Size: 530167 Color: 1
Size: 469735 Color: 0

Bin 227: 100 of cap free
Amount of items: 3
Items: 
Size: 784345 Color: 1
Size: 110341 Color: 0
Size: 105215 Color: 1

Bin 228: 100 of cap free
Amount of items: 2
Items: 
Size: 758520 Color: 1
Size: 241381 Color: 0

Bin 229: 100 of cap free
Amount of items: 2
Items: 
Size: 669167 Color: 0
Size: 330734 Color: 1

Bin 230: 100 of cap free
Amount of items: 2
Items: 
Size: 598036 Color: 0
Size: 401865 Color: 1

Bin 231: 103 of cap free
Amount of items: 3
Items: 
Size: 565110 Color: 0
Size: 248834 Color: 1
Size: 185954 Color: 1

Bin 232: 104 of cap free
Amount of items: 2
Items: 
Size: 652727 Color: 0
Size: 347170 Color: 1

Bin 233: 104 of cap free
Amount of items: 2
Items: 
Size: 654164 Color: 1
Size: 345733 Color: 0

Bin 234: 106 of cap free
Amount of items: 3
Items: 
Size: 568762 Color: 0
Size: 247764 Color: 0
Size: 183369 Color: 1

Bin 235: 108 of cap free
Amount of items: 2
Items: 
Size: 781806 Color: 0
Size: 218087 Color: 1

Bin 236: 109 of cap free
Amount of items: 2
Items: 
Size: 589806 Color: 0
Size: 410086 Color: 1

Bin 237: 111 of cap free
Amount of items: 2
Items: 
Size: 674385 Color: 0
Size: 325505 Color: 1

Bin 238: 114 of cap free
Amount of items: 2
Items: 
Size: 743335 Color: 0
Size: 256552 Color: 1

Bin 239: 115 of cap free
Amount of items: 2
Items: 
Size: 723859 Color: 1
Size: 276027 Color: 0

Bin 240: 115 of cap free
Amount of items: 2
Items: 
Size: 744747 Color: 1
Size: 255139 Color: 0

Bin 241: 116 of cap free
Amount of items: 3
Items: 
Size: 681428 Color: 0
Size: 194255 Color: 1
Size: 124202 Color: 0

Bin 242: 116 of cap free
Amount of items: 2
Items: 
Size: 754063 Color: 0
Size: 245822 Color: 1

Bin 243: 117 of cap free
Amount of items: 3
Items: 
Size: 614000 Color: 1
Size: 220979 Color: 0
Size: 164905 Color: 0

Bin 244: 118 of cap free
Amount of items: 2
Items: 
Size: 780094 Color: 0
Size: 219789 Color: 1

Bin 245: 123 of cap free
Amount of items: 2
Items: 
Size: 601719 Color: 1
Size: 398159 Color: 0

Bin 246: 124 of cap free
Amount of items: 2
Items: 
Size: 679612 Color: 0
Size: 320265 Color: 1

Bin 247: 124 of cap free
Amount of items: 2
Items: 
Size: 746012 Color: 1
Size: 253865 Color: 0

Bin 248: 124 of cap free
Amount of items: 2
Items: 
Size: 785854 Color: 0
Size: 214023 Color: 1

Bin 249: 126 of cap free
Amount of items: 2
Items: 
Size: 524180 Color: 0
Size: 475695 Color: 1

Bin 250: 127 of cap free
Amount of items: 2
Items: 
Size: 592523 Color: 1
Size: 407351 Color: 0

Bin 251: 128 of cap free
Amount of items: 2
Items: 
Size: 572638 Color: 1
Size: 427235 Color: 0

Bin 252: 129 of cap free
Amount of items: 3
Items: 
Size: 440731 Color: 1
Size: 308372 Color: 0
Size: 250769 Color: 0

Bin 253: 130 of cap free
Amount of items: 2
Items: 
Size: 673972 Color: 0
Size: 325899 Color: 1

Bin 254: 131 of cap free
Amount of items: 3
Items: 
Size: 450680 Color: 0
Size: 370963 Color: 1
Size: 178227 Color: 0

Bin 255: 132 of cap free
Amount of items: 2
Items: 
Size: 755645 Color: 0
Size: 244224 Color: 1

Bin 256: 134 of cap free
Amount of items: 2
Items: 
Size: 695980 Color: 0
Size: 303887 Color: 1

Bin 257: 135 of cap free
Amount of items: 2
Items: 
Size: 565587 Color: 0
Size: 434279 Color: 1

Bin 258: 135 of cap free
Amount of items: 2
Items: 
Size: 503548 Color: 0
Size: 496318 Color: 1

Bin 259: 138 of cap free
Amount of items: 2
Items: 
Size: 583715 Color: 0
Size: 416148 Color: 1

Bin 260: 139 of cap free
Amount of items: 3
Items: 
Size: 445578 Color: 0
Size: 405781 Color: 0
Size: 148503 Color: 1

Bin 261: 141 of cap free
Amount of items: 3
Items: 
Size: 761788 Color: 0
Size: 128133 Color: 1
Size: 109939 Color: 0

Bin 262: 142 of cap free
Amount of items: 2
Items: 
Size: 681842 Color: 0
Size: 318017 Color: 1

Bin 263: 142 of cap free
Amount of items: 2
Items: 
Size: 711114 Color: 1
Size: 288745 Color: 0

Bin 264: 143 of cap free
Amount of items: 2
Items: 
Size: 722456 Color: 0
Size: 277402 Color: 1

Bin 265: 143 of cap free
Amount of items: 2
Items: 
Size: 652085 Color: 1
Size: 347773 Color: 0

Bin 266: 143 of cap free
Amount of items: 2
Items: 
Size: 755343 Color: 1
Size: 244515 Color: 0

Bin 267: 143 of cap free
Amount of items: 2
Items: 
Size: 772402 Color: 0
Size: 227456 Color: 1

Bin 268: 144 of cap free
Amount of items: 3
Items: 
Size: 584235 Color: 0
Size: 249092 Color: 1
Size: 166530 Color: 0

Bin 269: 145 of cap free
Amount of items: 3
Items: 
Size: 658957 Color: 1
Size: 195087 Color: 0
Size: 145812 Color: 0

Bin 270: 146 of cap free
Amount of items: 2
Items: 
Size: 782763 Color: 0
Size: 217092 Color: 1

Bin 271: 147 of cap free
Amount of items: 2
Items: 
Size: 519807 Color: 1
Size: 480047 Color: 0

Bin 272: 149 of cap free
Amount of items: 2
Items: 
Size: 538044 Color: 1
Size: 461808 Color: 0

Bin 273: 150 of cap free
Amount of items: 2
Items: 
Size: 554199 Color: 1
Size: 445652 Color: 0

Bin 274: 151 of cap free
Amount of items: 2
Items: 
Size: 561483 Color: 1
Size: 438367 Color: 0

Bin 275: 154 of cap free
Amount of items: 2
Items: 
Size: 530992 Color: 1
Size: 468855 Color: 0

Bin 276: 155 of cap free
Amount of items: 3
Items: 
Size: 641669 Color: 0
Size: 181271 Color: 0
Size: 176906 Color: 1

Bin 277: 156 of cap free
Amount of items: 2
Items: 
Size: 521355 Color: 1
Size: 478490 Color: 0

Bin 278: 158 of cap free
Amount of items: 2
Items: 
Size: 693965 Color: 1
Size: 305878 Color: 0

Bin 279: 158 of cap free
Amount of items: 2
Items: 
Size: 722100 Color: 0
Size: 277743 Color: 1

Bin 280: 159 of cap free
Amount of items: 2
Items: 
Size: 699277 Color: 1
Size: 300565 Color: 0

Bin 281: 161 of cap free
Amount of items: 2
Items: 
Size: 779733 Color: 0
Size: 220107 Color: 1

Bin 282: 163 of cap free
Amount of items: 2
Items: 
Size: 585575 Color: 0
Size: 414263 Color: 1

Bin 283: 164 of cap free
Amount of items: 2
Items: 
Size: 693308 Color: 0
Size: 306529 Color: 1

Bin 284: 165 of cap free
Amount of items: 2
Items: 
Size: 613065 Color: 0
Size: 386771 Color: 1

Bin 285: 165 of cap free
Amount of items: 2
Items: 
Size: 552277 Color: 1
Size: 447559 Color: 0

Bin 286: 166 of cap free
Amount of items: 3
Items: 
Size: 506958 Color: 1
Size: 318789 Color: 0
Size: 174088 Color: 1

Bin 287: 166 of cap free
Amount of items: 2
Items: 
Size: 563278 Color: 1
Size: 436557 Color: 0

Bin 288: 166 of cap free
Amount of items: 2
Items: 
Size: 613666 Color: 0
Size: 386169 Color: 1

Bin 289: 171 of cap free
Amount of items: 2
Items: 
Size: 595594 Color: 0
Size: 404236 Color: 1

Bin 290: 171 of cap free
Amount of items: 2
Items: 
Size: 664990 Color: 0
Size: 334840 Color: 1

Bin 291: 172 of cap free
Amount of items: 2
Items: 
Size: 547624 Color: 0
Size: 452205 Color: 1

Bin 292: 172 of cap free
Amount of items: 2
Items: 
Size: 708030 Color: 0
Size: 291799 Color: 1

Bin 293: 173 of cap free
Amount of items: 2
Items: 
Size: 504665 Color: 1
Size: 495163 Color: 0

Bin 294: 175 of cap free
Amount of items: 2
Items: 
Size: 525232 Color: 0
Size: 474594 Color: 1

Bin 295: 176 of cap free
Amount of items: 2
Items: 
Size: 543541 Color: 1
Size: 456284 Color: 0

Bin 296: 176 of cap free
Amount of items: 2
Items: 
Size: 689437 Color: 0
Size: 310388 Color: 1

Bin 297: 178 of cap free
Amount of items: 2
Items: 
Size: 634387 Color: 0
Size: 365436 Color: 1

Bin 298: 178 of cap free
Amount of items: 2
Items: 
Size: 671934 Color: 0
Size: 327889 Color: 1

Bin 299: 183 of cap free
Amount of items: 3
Items: 
Size: 607710 Color: 0
Size: 226068 Color: 0
Size: 166040 Color: 1

Bin 300: 187 of cap free
Amount of items: 2
Items: 
Size: 545309 Color: 0
Size: 454505 Color: 1

Bin 301: 188 of cap free
Amount of items: 3
Items: 
Size: 436856 Color: 0
Size: 339379 Color: 1
Size: 223578 Color: 0

Bin 302: 191 of cap free
Amount of items: 3
Items: 
Size: 658485 Color: 1
Size: 184399 Color: 0
Size: 156926 Color: 0

Bin 303: 193 of cap free
Amount of items: 2
Items: 
Size: 731580 Color: 1
Size: 268228 Color: 0

Bin 304: 193 of cap free
Amount of items: 3
Items: 
Size: 580077 Color: 0
Size: 248573 Color: 1
Size: 171158 Color: 1

Bin 305: 196 of cap free
Amount of items: 2
Items: 
Size: 552339 Color: 0
Size: 447466 Color: 1

Bin 306: 199 of cap free
Amount of items: 2
Items: 
Size: 716600 Color: 0
Size: 283202 Color: 1

Bin 307: 199 of cap free
Amount of items: 2
Items: 
Size: 516325 Color: 1
Size: 483477 Color: 0

Bin 308: 200 of cap free
Amount of items: 3
Items: 
Size: 504538 Color: 1
Size: 308641 Color: 0
Size: 186622 Color: 0

Bin 309: 201 of cap free
Amount of items: 3
Items: 
Size: 737230 Color: 1
Size: 152441 Color: 0
Size: 110129 Color: 0

Bin 310: 201 of cap free
Amount of items: 2
Items: 
Size: 723414 Color: 1
Size: 276386 Color: 0

Bin 311: 205 of cap free
Amount of items: 2
Items: 
Size: 759807 Color: 1
Size: 239989 Color: 0

Bin 312: 205 of cap free
Amount of items: 2
Items: 
Size: 786782 Color: 1
Size: 213014 Color: 0

Bin 313: 206 of cap free
Amount of items: 2
Items: 
Size: 684868 Color: 0
Size: 314927 Color: 1

Bin 314: 206 of cap free
Amount of items: 2
Items: 
Size: 603005 Color: 1
Size: 396790 Color: 0

Bin 315: 207 of cap free
Amount of items: 2
Items: 
Size: 628106 Color: 0
Size: 371688 Color: 1

Bin 316: 208 of cap free
Amount of items: 2
Items: 
Size: 593919 Color: 0
Size: 405874 Color: 1

Bin 317: 208 of cap free
Amount of items: 2
Items: 
Size: 657928 Color: 1
Size: 341865 Color: 0

Bin 318: 209 of cap free
Amount of items: 2
Items: 
Size: 632382 Color: 1
Size: 367410 Color: 0

Bin 319: 210 of cap free
Amount of items: 2
Items: 
Size: 653219 Color: 0
Size: 346572 Color: 1

Bin 320: 210 of cap free
Amount of items: 2
Items: 
Size: 678836 Color: 1
Size: 320955 Color: 0

Bin 321: 212 of cap free
Amount of items: 2
Items: 
Size: 750051 Color: 1
Size: 249738 Color: 0

Bin 322: 213 of cap free
Amount of items: 2
Items: 
Size: 704240 Color: 0
Size: 295548 Color: 1

Bin 323: 215 of cap free
Amount of items: 2
Items: 
Size: 708431 Color: 1
Size: 291355 Color: 0

Bin 324: 216 of cap free
Amount of items: 3
Items: 
Size: 603460 Color: 1
Size: 204068 Color: 0
Size: 192257 Color: 1

Bin 325: 216 of cap free
Amount of items: 2
Items: 
Size: 799088 Color: 0
Size: 200697 Color: 1

Bin 326: 217 of cap free
Amount of items: 3
Items: 
Size: 441742 Color: 1
Size: 425133 Color: 0
Size: 132909 Color: 1

Bin 327: 217 of cap free
Amount of items: 2
Items: 
Size: 573598 Color: 0
Size: 426186 Color: 1

Bin 328: 219 of cap free
Amount of items: 2
Items: 
Size: 674327 Color: 1
Size: 325455 Color: 0

Bin 329: 219 of cap free
Amount of items: 2
Items: 
Size: 604227 Color: 0
Size: 395555 Color: 1

Bin 330: 219 of cap free
Amount of items: 2
Items: 
Size: 776762 Color: 1
Size: 223020 Color: 0

Bin 331: 222 of cap free
Amount of items: 2
Items: 
Size: 552747 Color: 0
Size: 447032 Color: 1

Bin 332: 222 of cap free
Amount of items: 2
Items: 
Size: 610442 Color: 1
Size: 389337 Color: 0

Bin 333: 224 of cap free
Amount of items: 3
Items: 
Size: 569860 Color: 0
Size: 251657 Color: 1
Size: 178260 Color: 1

Bin 334: 224 of cap free
Amount of items: 3
Items: 
Size: 405095 Color: 0
Size: 369755 Color: 1
Size: 224927 Color: 0

Bin 335: 224 of cap free
Amount of items: 3
Items: 
Size: 370172 Color: 1
Size: 330657 Color: 0
Size: 298948 Color: 0

Bin 336: 225 of cap free
Amount of items: 2
Items: 
Size: 740199 Color: 1
Size: 259577 Color: 0

Bin 337: 225 of cap free
Amount of items: 2
Items: 
Size: 761939 Color: 0
Size: 237837 Color: 1

Bin 338: 226 of cap free
Amount of items: 2
Items: 
Size: 639149 Color: 0
Size: 360626 Color: 1

Bin 339: 227 of cap free
Amount of items: 2
Items: 
Size: 566033 Color: 1
Size: 433741 Color: 0

Bin 340: 230 of cap free
Amount of items: 2
Items: 
Size: 749324 Color: 0
Size: 250447 Color: 1

Bin 341: 235 of cap free
Amount of items: 3
Items: 
Size: 491385 Color: 1
Size: 320227 Color: 0
Size: 188154 Color: 0

Bin 342: 235 of cap free
Amount of items: 2
Items: 
Size: 757314 Color: 0
Size: 242452 Color: 1

Bin 343: 235 of cap free
Amount of items: 2
Items: 
Size: 763895 Color: 1
Size: 235871 Color: 0

Bin 344: 236 of cap free
Amount of items: 3
Items: 
Size: 440354 Color: 1
Size: 368032 Color: 0
Size: 191379 Color: 1

Bin 345: 236 of cap free
Amount of items: 2
Items: 
Size: 727316 Color: 1
Size: 272449 Color: 0

Bin 346: 236 of cap free
Amount of items: 2
Items: 
Size: 753366 Color: 0
Size: 246399 Color: 1

Bin 347: 238 of cap free
Amount of items: 2
Items: 
Size: 782342 Color: 0
Size: 217421 Color: 1

Bin 348: 239 of cap free
Amount of items: 2
Items: 
Size: 584080 Color: 1
Size: 415682 Color: 0

Bin 349: 239 of cap free
Amount of items: 2
Items: 
Size: 711704 Color: 0
Size: 288058 Color: 1

Bin 350: 239 of cap free
Amount of items: 2
Items: 
Size: 511315 Color: 1
Size: 488447 Color: 0

Bin 351: 240 of cap free
Amount of items: 2
Items: 
Size: 754303 Color: 0
Size: 245458 Color: 1

Bin 352: 241 of cap free
Amount of items: 2
Items: 
Size: 583862 Color: 0
Size: 415898 Color: 1

Bin 353: 241 of cap free
Amount of items: 2
Items: 
Size: 651394 Color: 1
Size: 348366 Color: 0

Bin 354: 244 of cap free
Amount of items: 2
Items: 
Size: 709304 Color: 0
Size: 290453 Color: 1

Bin 355: 244 of cap free
Amount of items: 2
Items: 
Size: 719192 Color: 0
Size: 280565 Color: 1

Bin 356: 245 of cap free
Amount of items: 2
Items: 
Size: 779174 Color: 1
Size: 220582 Color: 0

Bin 357: 245 of cap free
Amount of items: 2
Items: 
Size: 537662 Color: 1
Size: 462094 Color: 0

Bin 358: 246 of cap free
Amount of items: 2
Items: 
Size: 519275 Color: 1
Size: 480480 Color: 0

Bin 359: 246 of cap free
Amount of items: 2
Items: 
Size: 635358 Color: 1
Size: 364397 Color: 0

Bin 360: 248 of cap free
Amount of items: 2
Items: 
Size: 638473 Color: 0
Size: 361280 Color: 1

Bin 361: 248 of cap free
Amount of items: 2
Items: 
Size: 530086 Color: 1
Size: 469667 Color: 0

Bin 362: 252 of cap free
Amount of items: 2
Items: 
Size: 717936 Color: 1
Size: 281813 Color: 0

Bin 363: 252 of cap free
Amount of items: 2
Items: 
Size: 518043 Color: 1
Size: 481706 Color: 0

Bin 364: 252 of cap free
Amount of items: 2
Items: 
Size: 592426 Color: 0
Size: 407323 Color: 1

Bin 365: 252 of cap free
Amount of items: 2
Items: 
Size: 749026 Color: 0
Size: 250723 Color: 1

Bin 366: 255 of cap free
Amount of items: 2
Items: 
Size: 582059 Color: 1
Size: 417687 Color: 0

Bin 367: 255 of cap free
Amount of items: 2
Items: 
Size: 756196 Color: 1
Size: 243550 Color: 0

Bin 368: 259 of cap free
Amount of items: 2
Items: 
Size: 646351 Color: 1
Size: 353391 Color: 0

Bin 369: 259 of cap free
Amount of items: 2
Items: 
Size: 570351 Color: 1
Size: 429391 Color: 0

Bin 370: 261 of cap free
Amount of items: 2
Items: 
Size: 673683 Color: 1
Size: 326057 Color: 0

Bin 371: 261 of cap free
Amount of items: 2
Items: 
Size: 759717 Color: 0
Size: 240023 Color: 1

Bin 372: 263 of cap free
Amount of items: 2
Items: 
Size: 777498 Color: 0
Size: 222240 Color: 1

Bin 373: 264 of cap free
Amount of items: 2
Items: 
Size: 690418 Color: 0
Size: 309319 Color: 1

Bin 374: 264 of cap free
Amount of items: 2
Items: 
Size: 761393 Color: 1
Size: 238344 Color: 0

Bin 375: 265 of cap free
Amount of items: 2
Items: 
Size: 511115 Color: 0
Size: 488621 Color: 1

Bin 376: 267 of cap free
Amount of items: 2
Items: 
Size: 725657 Color: 1
Size: 274077 Color: 0

Bin 377: 269 of cap free
Amount of items: 2
Items: 
Size: 778077 Color: 0
Size: 221655 Color: 1

Bin 378: 270 of cap free
Amount of items: 3
Items: 
Size: 502250 Color: 1
Size: 307467 Color: 0
Size: 190014 Color: 0

Bin 379: 270 of cap free
Amount of items: 2
Items: 
Size: 623851 Color: 0
Size: 375880 Color: 1

Bin 380: 272 of cap free
Amount of items: 2
Items: 
Size: 789967 Color: 1
Size: 209762 Color: 0

Bin 381: 273 of cap free
Amount of items: 2
Items: 
Size: 664694 Color: 0
Size: 335034 Color: 1

Bin 382: 274 of cap free
Amount of items: 2
Items: 
Size: 532538 Color: 1
Size: 467189 Color: 0

Bin 383: 274 of cap free
Amount of items: 2
Items: 
Size: 517806 Color: 0
Size: 481921 Color: 1

Bin 384: 274 of cap free
Amount of items: 2
Items: 
Size: 614194 Color: 0
Size: 385533 Color: 1

Bin 385: 278 of cap free
Amount of items: 2
Items: 
Size: 529078 Color: 1
Size: 470645 Color: 0

Bin 386: 280 of cap free
Amount of items: 2
Items: 
Size: 649980 Color: 0
Size: 349741 Color: 1

Bin 387: 280 of cap free
Amount of items: 2
Items: 
Size: 666909 Color: 0
Size: 332812 Color: 1

Bin 388: 282 of cap free
Amount of items: 2
Items: 
Size: 589048 Color: 1
Size: 410671 Color: 0

Bin 389: 282 of cap free
Amount of items: 2
Items: 
Size: 512030 Color: 0
Size: 487689 Color: 1

Bin 390: 283 of cap free
Amount of items: 2
Items: 
Size: 585665 Color: 1
Size: 414053 Color: 0

Bin 391: 283 of cap free
Amount of items: 2
Items: 
Size: 651739 Color: 0
Size: 347979 Color: 1

Bin 392: 283 of cap free
Amount of items: 2
Items: 
Size: 660174 Color: 1
Size: 339544 Color: 0

Bin 393: 284 of cap free
Amount of items: 3
Items: 
Size: 369092 Color: 1
Size: 366758 Color: 1
Size: 263867 Color: 0

Bin 394: 284 of cap free
Amount of items: 2
Items: 
Size: 774210 Color: 1
Size: 225507 Color: 0

Bin 395: 289 of cap free
Amount of items: 2
Items: 
Size: 505754 Color: 0
Size: 493958 Color: 1

Bin 396: 289 of cap free
Amount of items: 2
Items: 
Size: 696611 Color: 1
Size: 303101 Color: 0

Bin 397: 290 of cap free
Amount of items: 2
Items: 
Size: 600827 Color: 1
Size: 398884 Color: 0

Bin 398: 293 of cap free
Amount of items: 2
Items: 
Size: 542455 Color: 0
Size: 457253 Color: 1

Bin 399: 295 of cap free
Amount of items: 2
Items: 
Size: 500552 Color: 1
Size: 499154 Color: 0

Bin 400: 297 of cap free
Amount of items: 3
Items: 
Size: 468425 Color: 0
Size: 405435 Color: 0
Size: 125844 Color: 1

Bin 401: 297 of cap free
Amount of items: 2
Items: 
Size: 562529 Color: 0
Size: 437175 Color: 1

Bin 402: 298 of cap free
Amount of items: 3
Items: 
Size: 448753 Color: 1
Size: 371746 Color: 0
Size: 179204 Color: 1

Bin 403: 298 of cap free
Amount of items: 2
Items: 
Size: 573180 Color: 0
Size: 426523 Color: 1

Bin 404: 299 of cap free
Amount of items: 2
Items: 
Size: 518805 Color: 1
Size: 480897 Color: 0

Bin 405: 300 of cap free
Amount of items: 2
Items: 
Size: 656191 Color: 1
Size: 343510 Color: 0

Bin 406: 302 of cap free
Amount of items: 2
Items: 
Size: 539300 Color: 0
Size: 460399 Color: 1

Bin 407: 306 of cap free
Amount of items: 2
Items: 
Size: 567774 Color: 0
Size: 431921 Color: 1

Bin 408: 309 of cap free
Amount of items: 2
Items: 
Size: 646844 Color: 0
Size: 352848 Color: 1

Bin 409: 311 of cap free
Amount of items: 2
Items: 
Size: 716502 Color: 1
Size: 283188 Color: 0

Bin 410: 312 of cap free
Amount of items: 2
Items: 
Size: 787171 Color: 1
Size: 212518 Color: 0

Bin 411: 313 of cap free
Amount of items: 2
Items: 
Size: 624770 Color: 1
Size: 374918 Color: 0

Bin 412: 313 of cap free
Amount of items: 2
Items: 
Size: 663949 Color: 0
Size: 335739 Color: 1

Bin 413: 314 of cap free
Amount of items: 2
Items: 
Size: 664123 Color: 1
Size: 335564 Color: 0

Bin 414: 316 of cap free
Amount of items: 2
Items: 
Size: 727631 Color: 0
Size: 272054 Color: 1

Bin 415: 317 of cap free
Amount of items: 2
Items: 
Size: 743176 Color: 0
Size: 256508 Color: 1

Bin 416: 319 of cap free
Amount of items: 2
Items: 
Size: 509434 Color: 1
Size: 490248 Color: 0

Bin 417: 320 of cap free
Amount of items: 2
Items: 
Size: 507902 Color: 1
Size: 491779 Color: 0

Bin 418: 320 of cap free
Amount of items: 2
Items: 
Size: 548687 Color: 0
Size: 450994 Color: 1

Bin 419: 328 of cap free
Amount of items: 2
Items: 
Size: 748643 Color: 0
Size: 251030 Color: 1

Bin 420: 333 of cap free
Amount of items: 2
Items: 
Size: 570323 Color: 1
Size: 429345 Color: 0

Bin 421: 333 of cap free
Amount of items: 2
Items: 
Size: 597538 Color: 0
Size: 402130 Color: 1

Bin 422: 334 of cap free
Amount of items: 2
Items: 
Size: 573941 Color: 1
Size: 425726 Color: 0

Bin 423: 334 of cap free
Amount of items: 2
Items: 
Size: 536385 Color: 1
Size: 463282 Color: 0

Bin 424: 339 of cap free
Amount of items: 2
Items: 
Size: 765578 Color: 1
Size: 234084 Color: 0

Bin 425: 341 of cap free
Amount of items: 2
Items: 
Size: 730872 Color: 1
Size: 268788 Color: 0

Bin 426: 343 of cap free
Amount of items: 2
Items: 
Size: 747713 Color: 0
Size: 251945 Color: 1

Bin 427: 343 of cap free
Amount of items: 2
Items: 
Size: 625163 Color: 0
Size: 374495 Color: 1

Bin 428: 344 of cap free
Amount of items: 3
Items: 
Size: 580880 Color: 0
Size: 276963 Color: 0
Size: 141814 Color: 1

Bin 429: 348 of cap free
Amount of items: 2
Items: 
Size: 533250 Color: 1
Size: 466403 Color: 0

Bin 430: 348 of cap free
Amount of items: 2
Items: 
Size: 742318 Color: 1
Size: 257335 Color: 0

Bin 431: 356 of cap free
Amount of items: 3
Items: 
Size: 503567 Color: 1
Size: 371852 Color: 0
Size: 124226 Color: 0

Bin 432: 356 of cap free
Amount of items: 2
Items: 
Size: 674935 Color: 1
Size: 324710 Color: 0

Bin 433: 361 of cap free
Amount of items: 2
Items: 
Size: 625962 Color: 0
Size: 373678 Color: 1

Bin 434: 362 of cap free
Amount of items: 2
Items: 
Size: 523266 Color: 1
Size: 476373 Color: 0

Bin 435: 365 of cap free
Amount of items: 2
Items: 
Size: 550039 Color: 1
Size: 449597 Color: 0

Bin 436: 365 of cap free
Amount of items: 2
Items: 
Size: 764268 Color: 1
Size: 235368 Color: 0

Bin 437: 369 of cap free
Amount of items: 2
Items: 
Size: 706091 Color: 0
Size: 293541 Color: 1

Bin 438: 374 of cap free
Amount of items: 2
Items: 
Size: 564773 Color: 1
Size: 434854 Color: 0

Bin 439: 374 of cap free
Amount of items: 2
Items: 
Size: 771793 Color: 1
Size: 227834 Color: 0

Bin 440: 374 of cap free
Amount of items: 2
Items: 
Size: 794046 Color: 1
Size: 205581 Color: 0

Bin 441: 377 of cap free
Amount of items: 2
Items: 
Size: 783034 Color: 0
Size: 216590 Color: 1

Bin 442: 381 of cap free
Amount of items: 2
Items: 
Size: 766104 Color: 1
Size: 233516 Color: 0

Bin 443: 386 of cap free
Amount of items: 3
Items: 
Size: 564912 Color: 0
Size: 306755 Color: 0
Size: 127948 Color: 1

Bin 444: 392 of cap free
Amount of items: 2
Items: 
Size: 572784 Color: 1
Size: 426825 Color: 0

Bin 445: 402 of cap free
Amount of items: 2
Items: 
Size: 584605 Color: 1
Size: 414994 Color: 0

Bin 446: 402 of cap free
Amount of items: 2
Items: 
Size: 645064 Color: 0
Size: 354535 Color: 1

Bin 447: 403 of cap free
Amount of items: 2
Items: 
Size: 599127 Color: 0
Size: 400471 Color: 1

Bin 448: 405 of cap free
Amount of items: 2
Items: 
Size: 570903 Color: 1
Size: 428693 Color: 0

Bin 449: 405 of cap free
Amount of items: 2
Items: 
Size: 633951 Color: 1
Size: 365645 Color: 0

Bin 450: 406 of cap free
Amount of items: 2
Items: 
Size: 507191 Color: 0
Size: 492404 Color: 1

Bin 451: 407 of cap free
Amount of items: 2
Items: 
Size: 743661 Color: 0
Size: 255933 Color: 1

Bin 452: 415 of cap free
Amount of items: 2
Items: 
Size: 521954 Color: 0
Size: 477632 Color: 1

Bin 453: 419 of cap free
Amount of items: 3
Items: 
Size: 473032 Color: 1
Size: 342421 Color: 0
Size: 184129 Color: 0

Bin 454: 421 of cap free
Amount of items: 2
Items: 
Size: 674913 Color: 0
Size: 324667 Color: 1

Bin 455: 422 of cap free
Amount of items: 2
Items: 
Size: 637438 Color: 0
Size: 362141 Color: 1

Bin 456: 429 of cap free
Amount of items: 2
Items: 
Size: 624767 Color: 1
Size: 374805 Color: 0

Bin 457: 430 of cap free
Amount of items: 2
Items: 
Size: 635838 Color: 0
Size: 363733 Color: 1

Bin 458: 430 of cap free
Amount of items: 2
Items: 
Size: 723342 Color: 0
Size: 276229 Color: 1

Bin 459: 433 of cap free
Amount of items: 2
Items: 
Size: 516875 Color: 0
Size: 482693 Color: 1

Bin 460: 434 of cap free
Amount of items: 2
Items: 
Size: 586854 Color: 1
Size: 412713 Color: 0

Bin 461: 440 of cap free
Amount of items: 2
Items: 
Size: 554269 Color: 0
Size: 445292 Color: 1

Bin 462: 441 of cap free
Amount of items: 3
Items: 
Size: 736452 Color: 1
Size: 140417 Color: 1
Size: 122691 Color: 0

Bin 463: 442 of cap free
Amount of items: 2
Items: 
Size: 556765 Color: 0
Size: 442794 Color: 1

Bin 464: 443 of cap free
Amount of items: 2
Items: 
Size: 537039 Color: 1
Size: 462519 Color: 0

Bin 465: 446 of cap free
Amount of items: 3
Items: 
Size: 638333 Color: 1
Size: 182125 Color: 0
Size: 179097 Color: 1

Bin 466: 451 of cap free
Amount of items: 2
Items: 
Size: 752315 Color: 0
Size: 247235 Color: 1

Bin 467: 452 of cap free
Amount of items: 3
Items: 
Size: 762980 Color: 0
Size: 133132 Color: 0
Size: 103437 Color: 1

Bin 468: 456 of cap free
Amount of items: 2
Items: 
Size: 501294 Color: 1
Size: 498251 Color: 0

Bin 469: 457 of cap free
Amount of items: 2
Items: 
Size: 579804 Color: 1
Size: 419740 Color: 0

Bin 470: 457 of cap free
Amount of items: 2
Items: 
Size: 523989 Color: 1
Size: 475555 Color: 0

Bin 471: 459 of cap free
Amount of items: 2
Items: 
Size: 653448 Color: 1
Size: 346094 Color: 0

Bin 472: 463 of cap free
Amount of items: 2
Items: 
Size: 675854 Color: 0
Size: 323684 Color: 1

Bin 473: 464 of cap free
Amount of items: 3
Items: 
Size: 660915 Color: 1
Size: 205226 Color: 1
Size: 133396 Color: 0

Bin 474: 466 of cap free
Amount of items: 2
Items: 
Size: 710363 Color: 1
Size: 289172 Color: 0

Bin 475: 468 of cap free
Amount of items: 2
Items: 
Size: 557883 Color: 1
Size: 441650 Color: 0

Bin 476: 469 of cap free
Amount of items: 2
Items: 
Size: 524915 Color: 1
Size: 474617 Color: 0

Bin 477: 485 of cap free
Amount of items: 3
Items: 
Size: 473652 Color: 1
Size: 337837 Color: 1
Size: 188027 Color: 0

Bin 478: 486 of cap free
Amount of items: 2
Items: 
Size: 607856 Color: 1
Size: 391659 Color: 0

Bin 479: 486 of cap free
Amount of items: 2
Items: 
Size: 677499 Color: 1
Size: 322016 Color: 0

Bin 480: 492 of cap free
Amount of items: 2
Items: 
Size: 715329 Color: 1
Size: 284180 Color: 0

Bin 481: 492 of cap free
Amount of items: 2
Items: 
Size: 778453 Color: 0
Size: 221056 Color: 1

Bin 482: 498 of cap free
Amount of items: 2
Items: 
Size: 639395 Color: 1
Size: 360108 Color: 0

Bin 483: 499 of cap free
Amount of items: 2
Items: 
Size: 519770 Color: 0
Size: 479732 Color: 1

Bin 484: 500 of cap free
Amount of items: 2
Items: 
Size: 680109 Color: 0
Size: 319392 Color: 1

Bin 485: 501 of cap free
Amount of items: 3
Items: 
Size: 467661 Color: 0
Size: 326555 Color: 0
Size: 205284 Color: 1

Bin 486: 505 of cap free
Amount of items: 2
Items: 
Size: 549836 Color: 0
Size: 449660 Color: 1

Bin 487: 505 of cap free
Amount of items: 2
Items: 
Size: 737025 Color: 0
Size: 262471 Color: 1

Bin 488: 507 of cap free
Amount of items: 2
Items: 
Size: 711153 Color: 0
Size: 288341 Color: 1

Bin 489: 508 of cap free
Amount of items: 2
Items: 
Size: 792301 Color: 1
Size: 207192 Color: 0

Bin 490: 511 of cap free
Amount of items: 2
Items: 
Size: 608277 Color: 0
Size: 391213 Color: 1

Bin 491: 511 of cap free
Amount of items: 2
Items: 
Size: 690842 Color: 0
Size: 308648 Color: 1

Bin 492: 514 of cap free
Amount of items: 2
Items: 
Size: 526999 Color: 0
Size: 472488 Color: 1

Bin 493: 515 of cap free
Amount of items: 2
Items: 
Size: 601396 Color: 1
Size: 398090 Color: 0

Bin 494: 516 of cap free
Amount of items: 2
Items: 
Size: 507838 Color: 0
Size: 491647 Color: 1

Bin 495: 517 of cap free
Amount of items: 2
Items: 
Size: 539474 Color: 1
Size: 460010 Color: 0

Bin 496: 520 of cap free
Amount of items: 2
Items: 
Size: 647708 Color: 0
Size: 351773 Color: 1

Bin 497: 521 of cap free
Amount of items: 2
Items: 
Size: 717147 Color: 1
Size: 282333 Color: 0

Bin 498: 522 of cap free
Amount of items: 2
Items: 
Size: 698615 Color: 1
Size: 300864 Color: 0

Bin 499: 525 of cap free
Amount of items: 2
Items: 
Size: 762872 Color: 1
Size: 236604 Color: 0

Bin 500: 528 of cap free
Amount of items: 2
Items: 
Size: 620498 Color: 1
Size: 378975 Color: 0

Bin 501: 532 of cap free
Amount of items: 2
Items: 
Size: 574295 Color: 0
Size: 425174 Color: 1

Bin 502: 540 of cap free
Amount of items: 2
Items: 
Size: 709109 Color: 0
Size: 290352 Color: 1

Bin 503: 543 of cap free
Amount of items: 2
Items: 
Size: 658728 Color: 0
Size: 340730 Color: 1

Bin 504: 543 of cap free
Amount of items: 2
Items: 
Size: 677497 Color: 0
Size: 321961 Color: 1

Bin 505: 544 of cap free
Amount of items: 2
Items: 
Size: 499741 Color: 1
Size: 499716 Color: 0

Bin 506: 548 of cap free
Amount of items: 3
Items: 
Size: 683994 Color: 0
Size: 172971 Color: 0
Size: 142488 Color: 1

Bin 507: 548 of cap free
Amount of items: 2
Items: 
Size: 714404 Color: 0
Size: 285049 Color: 1

Bin 508: 548 of cap free
Amount of items: 2
Items: 
Size: 732171 Color: 0
Size: 267282 Color: 1

Bin 509: 560 of cap free
Amount of items: 3
Items: 
Size: 505415 Color: 1
Size: 308955 Color: 0
Size: 185071 Color: 1

Bin 510: 560 of cap free
Amount of items: 2
Items: 
Size: 559030 Color: 1
Size: 440411 Color: 0

Bin 511: 563 of cap free
Amount of items: 2
Items: 
Size: 569101 Color: 0
Size: 430337 Color: 1

Bin 512: 570 of cap free
Amount of items: 2
Items: 
Size: 637346 Color: 0
Size: 362085 Color: 1

Bin 513: 574 of cap free
Amount of items: 2
Items: 
Size: 598329 Color: 1
Size: 401098 Color: 0

Bin 514: 581 of cap free
Amount of items: 2
Items: 
Size: 519564 Color: 1
Size: 479856 Color: 0

Bin 515: 584 of cap free
Amount of items: 2
Items: 
Size: 511205 Color: 1
Size: 488212 Color: 0

Bin 516: 586 of cap free
Amount of items: 2
Items: 
Size: 528849 Color: 1
Size: 470566 Color: 0

Bin 517: 590 of cap free
Amount of items: 2
Items: 
Size: 714297 Color: 1
Size: 285114 Color: 0

Bin 518: 590 of cap free
Amount of items: 2
Items: 
Size: 797948 Color: 0
Size: 201463 Color: 1

Bin 519: 593 of cap free
Amount of items: 2
Items: 
Size: 543855 Color: 1
Size: 455553 Color: 0

Bin 520: 602 of cap free
Amount of items: 2
Items: 
Size: 594278 Color: 1
Size: 405121 Color: 0

Bin 521: 602 of cap free
Amount of items: 2
Items: 
Size: 657799 Color: 0
Size: 341600 Color: 1

Bin 522: 605 of cap free
Amount of items: 3
Items: 
Size: 663688 Color: 0
Size: 181440 Color: 0
Size: 154268 Color: 1

Bin 523: 605 of cap free
Amount of items: 2
Items: 
Size: 523981 Color: 1
Size: 475415 Color: 0

Bin 524: 611 of cap free
Amount of items: 3
Items: 
Size: 565401 Color: 0
Size: 305690 Color: 1
Size: 128299 Color: 1

Bin 525: 623 of cap free
Amount of items: 2
Items: 
Size: 573250 Color: 1
Size: 426128 Color: 0

Bin 526: 627 of cap free
Amount of items: 2
Items: 
Size: 709460 Color: 1
Size: 289914 Color: 0

Bin 527: 630 of cap free
Amount of items: 2
Items: 
Size: 770523 Color: 1
Size: 228848 Color: 0

Bin 528: 630 of cap free
Amount of items: 2
Items: 
Size: 758746 Color: 1
Size: 240625 Color: 0

Bin 529: 635 of cap free
Amount of items: 2
Items: 
Size: 788265 Color: 1
Size: 211101 Color: 0

Bin 530: 636 of cap free
Amount of items: 2
Items: 
Size: 568602 Color: 1
Size: 430763 Color: 0

Bin 531: 639 of cap free
Amount of items: 2
Items: 
Size: 523145 Color: 1
Size: 476217 Color: 0

Bin 532: 640 of cap free
Amount of items: 2
Items: 
Size: 705861 Color: 0
Size: 293500 Color: 1

Bin 533: 644 of cap free
Amount of items: 2
Items: 
Size: 647678 Color: 0
Size: 351679 Color: 1

Bin 534: 651 of cap free
Amount of items: 2
Items: 
Size: 686744 Color: 1
Size: 312606 Color: 0

Bin 535: 653 of cap free
Amount of items: 3
Items: 
Size: 566283 Color: 0
Size: 259898 Color: 1
Size: 173167 Color: 0

Bin 536: 653 of cap free
Amount of items: 2
Items: 
Size: 566394 Color: 1
Size: 432954 Color: 0

Bin 537: 653 of cap free
Amount of items: 2
Items: 
Size: 513507 Color: 1
Size: 485841 Color: 0

Bin 538: 659 of cap free
Amount of items: 2
Items: 
Size: 530935 Color: 0
Size: 468407 Color: 1

Bin 539: 668 of cap free
Amount of items: 3
Items: 
Size: 740602 Color: 1
Size: 129948 Color: 0
Size: 128783 Color: 1

Bin 540: 668 of cap free
Amount of items: 2
Items: 
Size: 762850 Color: 0
Size: 236483 Color: 1

Bin 541: 670 of cap free
Amount of items: 2
Items: 
Size: 772867 Color: 1
Size: 226464 Color: 0

Bin 542: 671 of cap free
Amount of items: 2
Items: 
Size: 708044 Color: 1
Size: 291286 Color: 0

Bin 543: 683 of cap free
Amount of items: 2
Items: 
Size: 682314 Color: 0
Size: 317004 Color: 1

Bin 544: 684 of cap free
Amount of items: 2
Items: 
Size: 537489 Color: 0
Size: 461828 Color: 1

Bin 545: 685 of cap free
Amount of items: 2
Items: 
Size: 543832 Color: 1
Size: 455484 Color: 0

Bin 546: 685 of cap free
Amount of items: 2
Items: 
Size: 706605 Color: 1
Size: 292711 Color: 0

Bin 547: 687 of cap free
Amount of items: 2
Items: 
Size: 600277 Color: 0
Size: 399037 Color: 1

Bin 548: 689 of cap free
Amount of items: 2
Items: 
Size: 642638 Color: 0
Size: 356674 Color: 1

Bin 549: 693 of cap free
Amount of items: 2
Items: 
Size: 594262 Color: 1
Size: 405046 Color: 0

Bin 550: 717 of cap free
Amount of items: 2
Items: 
Size: 784131 Color: 1
Size: 215153 Color: 0

Bin 551: 718 of cap free
Amount of items: 2
Items: 
Size: 554854 Color: 1
Size: 444429 Color: 0

Bin 552: 722 of cap free
Amount of items: 2
Items: 
Size: 662935 Color: 1
Size: 336344 Color: 0

Bin 553: 728 of cap free
Amount of items: 2
Items: 
Size: 610420 Color: 0
Size: 388853 Color: 1

Bin 554: 734 of cap free
Amount of items: 2
Items: 
Size: 738592 Color: 1
Size: 260675 Color: 0

Bin 555: 738 of cap free
Amount of items: 2
Items: 
Size: 529729 Color: 1
Size: 469534 Color: 0

Bin 556: 739 of cap free
Amount of items: 2
Items: 
Size: 636516 Color: 1
Size: 362746 Color: 0

Bin 557: 739 of cap free
Amount of items: 2
Items: 
Size: 536075 Color: 1
Size: 463187 Color: 0

Bin 558: 745 of cap free
Amount of items: 2
Items: 
Size: 754852 Color: 1
Size: 244404 Color: 0

Bin 559: 748 of cap free
Amount of items: 2
Items: 
Size: 736930 Color: 0
Size: 262323 Color: 1

Bin 560: 754 of cap free
Amount of items: 2
Items: 
Size: 607606 Color: 1
Size: 391641 Color: 0

Bin 561: 756 of cap free
Amount of items: 2
Items: 
Size: 520798 Color: 1
Size: 478447 Color: 0

Bin 562: 760 of cap free
Amount of items: 3
Items: 
Size: 704885 Color: 0
Size: 172515 Color: 0
Size: 121841 Color: 1

Bin 563: 760 of cap free
Amount of items: 2
Items: 
Size: 631571 Color: 0
Size: 367670 Color: 1

Bin 564: 775 of cap free
Amount of items: 2
Items: 
Size: 560361 Color: 1
Size: 438865 Color: 0

Bin 565: 779 of cap free
Amount of items: 2
Items: 
Size: 602108 Color: 1
Size: 397114 Color: 0

Bin 566: 782 of cap free
Amount of items: 2
Items: 
Size: 749725 Color: 1
Size: 249494 Color: 0

Bin 567: 785 of cap free
Amount of items: 2
Items: 
Size: 528743 Color: 0
Size: 470473 Color: 1

Bin 568: 798 of cap free
Amount of items: 2
Items: 
Size: 788608 Color: 0
Size: 210595 Color: 1

Bin 569: 799 of cap free
Amount of items: 2
Items: 
Size: 709469 Color: 1
Size: 289733 Color: 0

Bin 570: 803 of cap free
Amount of items: 2
Items: 
Size: 556850 Color: 1
Size: 442348 Color: 0

Bin 571: 806 of cap free
Amount of items: 2
Items: 
Size: 735872 Color: 0
Size: 263323 Color: 1

Bin 572: 809 of cap free
Amount of items: 3
Items: 
Size: 444418 Color: 0
Size: 406552 Color: 0
Size: 148222 Color: 1

Bin 573: 811 of cap free
Amount of items: 2
Items: 
Size: 675641 Color: 1
Size: 323549 Color: 0

Bin 574: 812 of cap free
Amount of items: 2
Items: 
Size: 508227 Color: 1
Size: 490962 Color: 0

Bin 575: 813 of cap free
Amount of items: 2
Items: 
Size: 725120 Color: 1
Size: 274068 Color: 0

Bin 576: 836 of cap free
Amount of items: 2
Items: 
Size: 748502 Color: 0
Size: 250663 Color: 1

Bin 577: 836 of cap free
Amount of items: 2
Items: 
Size: 635277 Color: 1
Size: 363888 Color: 0

Bin 578: 838 of cap free
Amount of items: 2
Items: 
Size: 515832 Color: 0
Size: 483331 Color: 1

Bin 579: 839 of cap free
Amount of items: 2
Items: 
Size: 757985 Color: 0
Size: 241177 Color: 1

Bin 580: 842 of cap free
Amount of items: 2
Items: 
Size: 558465 Color: 0
Size: 440694 Color: 1

Bin 581: 844 of cap free
Amount of items: 2
Items: 
Size: 595313 Color: 1
Size: 403844 Color: 0

Bin 582: 858 of cap free
Amount of items: 2
Items: 
Size: 668976 Color: 0
Size: 330167 Color: 1

Bin 583: 864 of cap free
Amount of items: 2
Items: 
Size: 518330 Color: 0
Size: 480807 Color: 1

Bin 584: 868 of cap free
Amount of items: 2
Items: 
Size: 530903 Color: 0
Size: 468230 Color: 1

Bin 585: 870 of cap free
Amount of items: 3
Items: 
Size: 560610 Color: 0
Size: 247212 Color: 0
Size: 191309 Color: 1

Bin 586: 872 of cap free
Amount of items: 2
Items: 
Size: 703985 Color: 1
Size: 295144 Color: 0

Bin 587: 878 of cap free
Amount of items: 2
Items: 
Size: 562137 Color: 1
Size: 436986 Color: 0

Bin 588: 885 of cap free
Amount of items: 2
Items: 
Size: 799169 Color: 1
Size: 199947 Color: 0

Bin 589: 887 of cap free
Amount of items: 4
Items: 
Size: 423896 Color: 0
Size: 266439 Color: 0
Size: 188903 Color: 1
Size: 119876 Color: 1

Bin 590: 887 of cap free
Amount of items: 2
Items: 
Size: 784439 Color: 0
Size: 214675 Color: 1

Bin 591: 893 of cap free
Amount of items: 2
Items: 
Size: 659433 Color: 0
Size: 339675 Color: 1

Bin 592: 903 of cap free
Amount of items: 2
Items: 
Size: 696086 Color: 1
Size: 303012 Color: 0

Bin 593: 903 of cap free
Amount of items: 2
Items: 
Size: 771658 Color: 1
Size: 227440 Color: 0

Bin 594: 907 of cap free
Amount of items: 2
Items: 
Size: 719704 Color: 0
Size: 279390 Color: 1

Bin 595: 916 of cap free
Amount of items: 2
Items: 
Size: 547052 Color: 1
Size: 452033 Color: 0

Bin 596: 918 of cap free
Amount of items: 2
Items: 
Size: 774148 Color: 0
Size: 224935 Color: 1

Bin 597: 929 of cap free
Amount of items: 2
Items: 
Size: 545530 Color: 0
Size: 453542 Color: 1

Bin 598: 934 of cap free
Amount of items: 2
Items: 
Size: 656377 Color: 0
Size: 342690 Color: 1

Bin 599: 936 of cap free
Amount of items: 2
Items: 
Size: 561172 Color: 1
Size: 437893 Color: 0

Bin 600: 936 of cap free
Amount of items: 2
Items: 
Size: 725099 Color: 0
Size: 273966 Color: 1

Bin 601: 938 of cap free
Amount of items: 2
Items: 
Size: 586404 Color: 1
Size: 412659 Color: 0

Bin 602: 947 of cap free
Amount of items: 2
Items: 
Size: 797602 Color: 0
Size: 201452 Color: 1

Bin 603: 948 of cap free
Amount of items: 2
Items: 
Size: 555260 Color: 0
Size: 443793 Color: 1

Bin 604: 953 of cap free
Amount of items: 2
Items: 
Size: 525385 Color: 1
Size: 473663 Color: 0

Bin 605: 962 of cap free
Amount of items: 2
Items: 
Size: 638254 Color: 1
Size: 360785 Color: 0

Bin 606: 964 of cap free
Amount of items: 2
Items: 
Size: 670215 Color: 0
Size: 328822 Color: 1

Bin 607: 967 of cap free
Amount of items: 2
Items: 
Size: 769440 Color: 0
Size: 229594 Color: 1

Bin 608: 969 of cap free
Amount of items: 2
Items: 
Size: 610824 Color: 1
Size: 388208 Color: 0

Bin 609: 974 of cap free
Amount of items: 2
Items: 
Size: 558996 Color: 1
Size: 440031 Color: 0

Bin 610: 993 of cap free
Amount of items: 2
Items: 
Size: 530915 Color: 0
Size: 468093 Color: 1

Bin 611: 1017 of cap free
Amount of items: 2
Items: 
Size: 570859 Color: 1
Size: 428125 Color: 0

Bin 612: 1017 of cap free
Amount of items: 2
Items: 
Size: 664687 Color: 0
Size: 334297 Color: 1

Bin 613: 1022 of cap free
Amount of items: 2
Items: 
Size: 576757 Color: 0
Size: 422222 Color: 1

Bin 614: 1029 of cap free
Amount of items: 2
Items: 
Size: 644845 Color: 0
Size: 354127 Color: 1

Bin 615: 1032 of cap free
Amount of items: 2
Items: 
Size: 796619 Color: 1
Size: 202350 Color: 0

Bin 616: 1042 of cap free
Amount of items: 2
Items: 
Size: 515184 Color: 1
Size: 483775 Color: 0

Bin 617: 1044 of cap free
Amount of items: 2
Items: 
Size: 766697 Color: 0
Size: 232260 Color: 1

Bin 618: 1051 of cap free
Amount of items: 2
Items: 
Size: 777758 Color: 1
Size: 221192 Color: 0

Bin 619: 1052 of cap free
Amount of items: 2
Items: 
Size: 772638 Color: 0
Size: 226311 Color: 1

Bin 620: 1059 of cap free
Amount of items: 2
Items: 
Size: 590472 Color: 1
Size: 408470 Color: 0

Bin 621: 1068 of cap free
Amount of items: 2
Items: 
Size: 620114 Color: 0
Size: 378819 Color: 1

Bin 622: 1082 of cap free
Amount of items: 2
Items: 
Size: 733624 Color: 1
Size: 265295 Color: 0

Bin 623: 1082 of cap free
Amount of items: 2
Items: 
Size: 617533 Color: 0
Size: 381386 Color: 1

Bin 624: 1092 of cap free
Amount of items: 2
Items: 
Size: 593260 Color: 0
Size: 405649 Color: 1

Bin 625: 1112 of cap free
Amount of items: 2
Items: 
Size: 659474 Color: 1
Size: 339415 Color: 0

Bin 626: 1113 of cap free
Amount of items: 2
Items: 
Size: 508818 Color: 0
Size: 490070 Color: 1

Bin 627: 1116 of cap free
Amount of items: 2
Items: 
Size: 729667 Color: 0
Size: 269218 Color: 1

Bin 628: 1118 of cap free
Amount of items: 2
Items: 
Size: 707272 Color: 0
Size: 291611 Color: 1

Bin 629: 1124 of cap free
Amount of items: 2
Items: 
Size: 712682 Color: 1
Size: 286195 Color: 0

Bin 630: 1131 of cap free
Amount of items: 2
Items: 
Size: 578087 Color: 0
Size: 420783 Color: 1

Bin 631: 1142 of cap free
Amount of items: 2
Items: 
Size: 684663 Color: 0
Size: 314196 Color: 1

Bin 632: 1142 of cap free
Amount of items: 2
Items: 
Size: 664082 Color: 1
Size: 334777 Color: 0

Bin 633: 1155 of cap free
Amount of items: 2
Items: 
Size: 784317 Color: 0
Size: 214529 Color: 1

Bin 634: 1159 of cap free
Amount of items: 2
Items: 
Size: 529586 Color: 0
Size: 469256 Color: 1

Bin 635: 1167 of cap free
Amount of items: 2
Items: 
Size: 606393 Color: 1
Size: 392441 Color: 0

Bin 636: 1176 of cap free
Amount of items: 2
Items: 
Size: 702772 Color: 0
Size: 296053 Color: 1

Bin 637: 1190 of cap free
Amount of items: 2
Items: 
Size: 687453 Color: 1
Size: 311358 Color: 0

Bin 638: 1193 of cap free
Amount of items: 2
Items: 
Size: 641327 Color: 1
Size: 357481 Color: 0

Bin 639: 1196 of cap free
Amount of items: 2
Items: 
Size: 576455 Color: 1
Size: 422350 Color: 0

Bin 640: 1203 of cap free
Amount of items: 2
Items: 
Size: 776500 Color: 1
Size: 222298 Color: 0

Bin 641: 1204 of cap free
Amount of items: 2
Items: 
Size: 623782 Color: 0
Size: 375015 Color: 1

Bin 642: 1214 of cap free
Amount of items: 2
Items: 
Size: 706114 Color: 1
Size: 292673 Color: 0

Bin 643: 1221 of cap free
Amount of items: 2
Items: 
Size: 782315 Color: 0
Size: 216465 Color: 1

Bin 644: 1222 of cap free
Amount of items: 2
Items: 
Size: 703681 Color: 1
Size: 295098 Color: 0

Bin 645: 1225 of cap free
Amount of items: 2
Items: 
Size: 759277 Color: 0
Size: 239499 Color: 1

Bin 646: 1230 of cap free
Amount of items: 2
Items: 
Size: 724911 Color: 0
Size: 273860 Color: 1

Bin 647: 1261 of cap free
Amount of items: 2
Items: 
Size: 696727 Color: 0
Size: 302013 Color: 1

Bin 648: 1278 of cap free
Amount of items: 2
Items: 
Size: 765450 Color: 1
Size: 233273 Color: 0

Bin 649: 1279 of cap free
Amount of items: 2
Items: 
Size: 675372 Color: 1
Size: 323350 Color: 0

Bin 650: 1283 of cap free
Amount of items: 2
Items: 
Size: 552896 Color: 1
Size: 445822 Color: 0

Bin 651: 1284 of cap free
Amount of items: 2
Items: 
Size: 672164 Color: 1
Size: 326553 Color: 0

Bin 652: 1284 of cap free
Amount of items: 2
Items: 
Size: 576752 Color: 0
Size: 421965 Color: 1

Bin 653: 1285 of cap free
Amount of items: 2
Items: 
Size: 780229 Color: 1
Size: 218487 Color: 0

Bin 654: 1293 of cap free
Amount of items: 2
Items: 
Size: 523876 Color: 0
Size: 474832 Color: 1

Bin 655: 1296 of cap free
Amount of items: 2
Items: 
Size: 664610 Color: 0
Size: 334095 Color: 1

Bin 656: 1297 of cap free
Amount of items: 2
Items: 
Size: 651101 Color: 0
Size: 347603 Color: 1

Bin 657: 1300 of cap free
Amount of items: 2
Items: 
Size: 501425 Color: 0
Size: 497276 Color: 1

Bin 658: 1307 of cap free
Amount of items: 2
Items: 
Size: 613919 Color: 0
Size: 384775 Color: 1

Bin 659: 1311 of cap free
Amount of items: 2
Items: 
Size: 597944 Color: 1
Size: 400746 Color: 0

Bin 660: 1311 of cap free
Amount of items: 2
Items: 
Size: 624215 Color: 1
Size: 374475 Color: 0

Bin 661: 1313 of cap free
Amount of items: 2
Items: 
Size: 666133 Color: 1
Size: 332555 Color: 0

Bin 662: 1332 of cap free
Amount of items: 2
Items: 
Size: 658719 Color: 0
Size: 339950 Color: 1

Bin 663: 1332 of cap free
Amount of items: 2
Items: 
Size: 565980 Color: 0
Size: 432689 Color: 1

Bin 664: 1340 of cap free
Amount of items: 2
Items: 
Size: 549335 Color: 1
Size: 449326 Color: 0

Bin 665: 1351 of cap free
Amount of items: 2
Items: 
Size: 710960 Color: 0
Size: 287690 Color: 1

Bin 666: 1351 of cap free
Amount of items: 2
Items: 
Size: 513931 Color: 0
Size: 484719 Color: 1

Bin 667: 1364 of cap free
Amount of items: 2
Items: 
Size: 542436 Color: 1
Size: 456201 Color: 0

Bin 668: 1367 of cap free
Amount of items: 2
Items: 
Size: 578153 Color: 1
Size: 420481 Color: 0

Bin 669: 1373 of cap free
Amount of items: 2
Items: 
Size: 574468 Color: 1
Size: 424160 Color: 0

Bin 670: 1379 of cap free
Amount of items: 2
Items: 
Size: 796486 Color: 1
Size: 202136 Color: 0

Bin 671: 1389 of cap free
Amount of items: 2
Items: 
Size: 615425 Color: 0
Size: 383187 Color: 1

Bin 672: 1392 of cap free
Amount of items: 2
Items: 
Size: 570748 Color: 1
Size: 427861 Color: 0

Bin 673: 1401 of cap free
Amount of items: 2
Items: 
Size: 590545 Color: 0
Size: 408055 Color: 1

Bin 674: 1405 of cap free
Amount of items: 2
Items: 
Size: 694982 Color: 0
Size: 303614 Color: 1

Bin 675: 1418 of cap free
Amount of items: 2
Items: 
Size: 571152 Color: 0
Size: 427431 Color: 1

Bin 676: 1418 of cap free
Amount of items: 2
Items: 
Size: 602415 Color: 0
Size: 396168 Color: 1

Bin 677: 1419 of cap free
Amount of items: 2
Items: 
Size: 609733 Color: 0
Size: 388849 Color: 1

Bin 678: 1423 of cap free
Amount of items: 2
Items: 
Size: 593015 Color: 0
Size: 405563 Color: 1

Bin 679: 1424 of cap free
Amount of items: 2
Items: 
Size: 757655 Color: 0
Size: 240922 Color: 1

Bin 680: 1428 of cap free
Amount of items: 2
Items: 
Size: 766374 Color: 0
Size: 232199 Color: 1

Bin 681: 1431 of cap free
Amount of items: 2
Items: 
Size: 619899 Color: 0
Size: 378671 Color: 1

Bin 682: 1442 of cap free
Amount of items: 2
Items: 
Size: 762229 Color: 1
Size: 236330 Color: 0

Bin 683: 1455 of cap free
Amount of items: 2
Items: 
Size: 671617 Color: 0
Size: 326929 Color: 1

Bin 684: 1460 of cap free
Amount of items: 3
Items: 
Size: 639476 Color: 1
Size: 216815 Color: 0
Size: 142250 Color: 0

Bin 685: 1478 of cap free
Amount of items: 7
Items: 
Size: 230065 Color: 0
Size: 177339 Color: 1
Size: 163325 Color: 1
Size: 121308 Color: 1
Size: 103002 Color: 0
Size: 102460 Color: 0
Size: 101024 Color: 1

Bin 686: 1514 of cap free
Amount of items: 2
Items: 
Size: 632312 Color: 1
Size: 366175 Color: 0

Bin 687: 1515 of cap free
Amount of items: 2
Items: 
Size: 620106 Color: 1
Size: 378380 Color: 0

Bin 688: 1518 of cap free
Amount of items: 2
Items: 
Size: 766346 Color: 0
Size: 232137 Color: 1

Bin 689: 1537 of cap free
Amount of items: 2
Items: 
Size: 678254 Color: 1
Size: 320210 Color: 0

Bin 690: 1537 of cap free
Amount of items: 2
Items: 
Size: 644454 Color: 0
Size: 354010 Color: 1

Bin 691: 1538 of cap free
Amount of items: 2
Items: 
Size: 504578 Color: 0
Size: 493885 Color: 1

Bin 692: 1539 of cap free
Amount of items: 2
Items: 
Size: 590075 Color: 1
Size: 408387 Color: 0

Bin 693: 1552 of cap free
Amount of items: 2
Items: 
Size: 581574 Color: 1
Size: 416875 Color: 0

Bin 694: 1557 of cap free
Amount of items: 2
Items: 
Size: 540552 Color: 0
Size: 457892 Color: 1

Bin 695: 1567 of cap free
Amount of items: 2
Items: 
Size: 629322 Color: 1
Size: 369112 Color: 0

Bin 696: 1573 of cap free
Amount of items: 2
Items: 
Size: 683743 Color: 1
Size: 314685 Color: 0

Bin 697: 1606 of cap free
Amount of items: 2
Items: 
Size: 648866 Color: 1
Size: 349529 Color: 0

Bin 698: 1614 of cap free
Amount of items: 2
Items: 
Size: 617050 Color: 0
Size: 381337 Color: 1

Bin 699: 1619 of cap free
Amount of items: 2
Items: 
Size: 534110 Color: 0
Size: 464272 Color: 1

Bin 700: 1625 of cap free
Amount of items: 2
Items: 
Size: 521258 Color: 0
Size: 477118 Color: 1

Bin 701: 1631 of cap free
Amount of items: 2
Items: 
Size: 640934 Color: 1
Size: 357436 Color: 0

Bin 702: 1644 of cap free
Amount of items: 2
Items: 
Size: 710543 Color: 0
Size: 287814 Color: 1

Bin 703: 1653 of cap free
Amount of items: 2
Items: 
Size: 727051 Color: 0
Size: 271297 Color: 1

Bin 704: 1673 of cap free
Amount of items: 2
Items: 
Size: 755197 Color: 0
Size: 243131 Color: 1

Bin 705: 1689 of cap free
Amount of items: 2
Items: 
Size: 583641 Color: 1
Size: 414671 Color: 0

Bin 706: 1693 of cap free
Amount of items: 2
Items: 
Size: 613601 Color: 0
Size: 384707 Color: 1

Bin 707: 1701 of cap free
Amount of items: 2
Items: 
Size: 717031 Color: 1
Size: 281269 Color: 0

Bin 708: 1716 of cap free
Amount of items: 2
Items: 
Size: 645154 Color: 1
Size: 353131 Color: 0

Bin 709: 1719 of cap free
Amount of items: 2
Items: 
Size: 720280 Color: 1
Size: 278002 Color: 0

Bin 710: 1720 of cap free
Amount of items: 2
Items: 
Size: 742418 Color: 0
Size: 255863 Color: 1

Bin 711: 1729 of cap free
Amount of items: 2
Items: 
Size: 511892 Color: 0
Size: 486380 Color: 1

Bin 712: 1744 of cap free
Amount of items: 2
Items: 
Size: 602118 Color: 0
Size: 396139 Color: 1

Bin 713: 1763 of cap free
Amount of items: 2
Items: 
Size: 718849 Color: 0
Size: 279389 Color: 1

Bin 714: 1767 of cap free
Amount of items: 2
Items: 
Size: 732035 Color: 0
Size: 266199 Color: 1

Bin 715: 1787 of cap free
Amount of items: 2
Items: 
Size: 565671 Color: 1
Size: 432543 Color: 0

Bin 716: 1799 of cap free
Amount of items: 2
Items: 
Size: 700182 Color: 0
Size: 298020 Color: 1

Bin 717: 1799 of cap free
Amount of items: 2
Items: 
Size: 546496 Color: 0
Size: 451706 Color: 1

Bin 718: 1810 of cap free
Amount of items: 2
Items: 
Size: 717709 Color: 1
Size: 280482 Color: 0

Bin 719: 1833 of cap free
Amount of items: 2
Items: 
Size: 767542 Color: 1
Size: 230626 Color: 0

Bin 720: 1847 of cap free
Amount of items: 2
Items: 
Size: 585653 Color: 1
Size: 412501 Color: 0

Bin 721: 1851 of cap free
Amount of items: 2
Items: 
Size: 715199 Color: 0
Size: 282951 Color: 1

Bin 722: 1861 of cap free
Amount of items: 2
Items: 
Size: 767219 Color: 1
Size: 230921 Color: 0

Bin 723: 1866 of cap free
Amount of items: 2
Items: 
Size: 690382 Color: 1
Size: 307753 Color: 0

Bin 724: 1878 of cap free
Amount of items: 2
Items: 
Size: 609316 Color: 0
Size: 388807 Color: 1

Bin 725: 1911 of cap free
Amount of items: 2
Items: 
Size: 644104 Color: 0
Size: 353986 Color: 1

Bin 726: 1920 of cap free
Amount of items: 2
Items: 
Size: 523588 Color: 0
Size: 474493 Color: 1

Bin 727: 1932 of cap free
Amount of items: 3
Items: 
Size: 737797 Color: 1
Size: 159758 Color: 0
Size: 100514 Color: 0

Bin 728: 1949 of cap free
Amount of items: 2
Items: 
Size: 692609 Color: 0
Size: 305443 Color: 1

Bin 729: 1952 of cap free
Amount of items: 2
Items: 
Size: 617251 Color: 1
Size: 380798 Color: 0

Bin 730: 1958 of cap free
Amount of items: 2
Items: 
Size: 590108 Color: 0
Size: 407935 Color: 1

Bin 731: 1963 of cap free
Amount of items: 2
Items: 
Size: 533810 Color: 0
Size: 464228 Color: 1

Bin 732: 1969 of cap free
Amount of items: 2
Items: 
Size: 675372 Color: 0
Size: 322660 Color: 1

Bin 733: 1973 of cap free
Amount of items: 2
Items: 
Size: 742694 Color: 1
Size: 255334 Color: 0

Bin 734: 2020 of cap free
Amount of items: 2
Items: 
Size: 623895 Color: 1
Size: 374086 Color: 0

Bin 735: 2024 of cap free
Amount of items: 2
Items: 
Size: 759230 Color: 0
Size: 238747 Color: 1

Bin 736: 2033 of cap free
Amount of items: 2
Items: 
Size: 597845 Color: 1
Size: 400123 Color: 0

Bin 737: 2053 of cap free
Amount of items: 2
Items: 
Size: 636769 Color: 0
Size: 361179 Color: 1

Bin 738: 2074 of cap free
Amount of items: 2
Items: 
Size: 766275 Color: 0
Size: 231652 Color: 1

Bin 739: 2084 of cap free
Amount of items: 2
Items: 
Size: 645285 Color: 1
Size: 352632 Color: 0

Bin 740: 2091 of cap free
Amount of items: 3
Items: 
Size: 514258 Color: 0
Size: 339826 Color: 1
Size: 143826 Color: 1

Bin 741: 2092 of cap free
Amount of items: 2
Items: 
Size: 596542 Color: 0
Size: 401367 Color: 1

Bin 742: 2109 of cap free
Amount of items: 2
Items: 
Size: 648448 Color: 1
Size: 349444 Color: 0

Bin 743: 2172 of cap free
Amount of items: 2
Items: 
Size: 565000 Color: 1
Size: 432829 Color: 0

Bin 744: 2179 of cap free
Amount of items: 2
Items: 
Size: 554125 Color: 0
Size: 443697 Color: 1

Bin 745: 2202 of cap free
Amount of items: 2
Items: 
Size: 504458 Color: 0
Size: 493341 Color: 1

Bin 746: 2214 of cap free
Amount of items: 2
Items: 
Size: 507149 Color: 1
Size: 490638 Color: 0

Bin 747: 2250 of cap free
Amount of items: 2
Items: 
Size: 520689 Color: 1
Size: 477062 Color: 0

Bin 748: 2278 of cap free
Amount of items: 2
Items: 
Size: 551455 Color: 0
Size: 446268 Color: 1

Bin 749: 2307 of cap free
Amount of items: 2
Items: 
Size: 718478 Color: 0
Size: 279216 Color: 1

Bin 750: 2331 of cap free
Amount of items: 2
Items: 
Size: 796495 Color: 0
Size: 201175 Color: 1

Bin 751: 2355 of cap free
Amount of items: 2
Items: 
Size: 617073 Color: 1
Size: 380573 Color: 0

Bin 752: 2364 of cap free
Amount of items: 2
Items: 
Size: 724174 Color: 1
Size: 273463 Color: 0

Bin 753: 2380 of cap free
Amount of items: 2
Items: 
Size: 663627 Color: 0
Size: 333994 Color: 1

Bin 754: 2389 of cap free
Amount of items: 2
Items: 
Size: 514393 Color: 1
Size: 483219 Color: 0

Bin 755: 2405 of cap free
Amount of items: 2
Items: 
Size: 504279 Color: 0
Size: 493317 Color: 1

Bin 756: 2414 of cap free
Amount of items: 2
Items: 
Size: 657754 Color: 0
Size: 339833 Color: 1

Bin 757: 2422 of cap free
Amount of items: 2
Items: 
Size: 561432 Color: 0
Size: 436147 Color: 1

Bin 758: 2432 of cap free
Amount of items: 2
Items: 
Size: 654795 Color: 1
Size: 342774 Color: 0

Bin 759: 2444 of cap free
Amount of items: 2
Items: 
Size: 510071 Color: 1
Size: 487486 Color: 0

Bin 760: 2496 of cap free
Amount of items: 2
Items: 
Size: 523378 Color: 0
Size: 474127 Color: 1

Bin 761: 2518 of cap free
Amount of items: 2
Items: 
Size: 643593 Color: 0
Size: 353890 Color: 1

Bin 762: 2541 of cap free
Amount of items: 2
Items: 
Size: 628797 Color: 1
Size: 368663 Color: 0

Bin 763: 2542 of cap free
Amount of items: 2
Items: 
Size: 640926 Color: 1
Size: 356533 Color: 0

Bin 764: 2558 of cap free
Amount of items: 2
Items: 
Size: 592854 Color: 0
Size: 404589 Color: 1

Bin 765: 2564 of cap free
Amount of items: 2
Items: 
Size: 577967 Color: 1
Size: 419470 Color: 0

Bin 766: 2577 of cap free
Amount of items: 2
Items: 
Size: 654656 Color: 1
Size: 342768 Color: 0

Bin 767: 2579 of cap free
Amount of items: 2
Items: 
Size: 772663 Color: 1
Size: 224759 Color: 0

Bin 768: 2673 of cap free
Amount of items: 2
Items: 
Size: 777268 Color: 0
Size: 220060 Color: 1

Bin 769: 2676 of cap free
Amount of items: 2
Items: 
Size: 551448 Color: 0
Size: 445877 Color: 1

Bin 770: 2697 of cap free
Amount of items: 2
Items: 
Size: 731460 Color: 0
Size: 265844 Color: 1

Bin 771: 2697 of cap free
Amount of items: 2
Items: 
Size: 791757 Color: 1
Size: 205547 Color: 0

Bin 772: 2726 of cap free
Amount of items: 2
Items: 
Size: 667459 Color: 0
Size: 329816 Color: 1

Bin 773: 2728 of cap free
Amount of items: 2
Items: 
Size: 724112 Color: 1
Size: 273161 Color: 0

Bin 774: 2749 of cap free
Amount of items: 2
Items: 
Size: 785406 Color: 1
Size: 211846 Color: 0

Bin 775: 2774 of cap free
Amount of items: 2
Items: 
Size: 706019 Color: 1
Size: 291208 Color: 0

Bin 776: 2792 of cap free
Amount of items: 2
Items: 
Size: 523205 Color: 0
Size: 474004 Color: 1

Bin 777: 2793 of cap free
Amount of items: 2
Items: 
Size: 724284 Color: 1
Size: 272924 Color: 0

Bin 778: 2794 of cap free
Amount of items: 2
Items: 
Size: 705796 Color: 0
Size: 291411 Color: 1

Bin 779: 2829 of cap free
Amount of items: 2
Items: 
Size: 682850 Color: 1
Size: 314322 Color: 0

Bin 780: 2841 of cap free
Amount of items: 2
Items: 
Size: 654554 Color: 1
Size: 342606 Color: 0

Bin 781: 2845 of cap free
Amount of items: 2
Items: 
Size: 503927 Color: 0
Size: 493229 Color: 1

Bin 782: 2848 of cap free
Amount of items: 2
Items: 
Size: 715001 Color: 0
Size: 282152 Color: 1

Bin 783: 2890 of cap free
Amount of items: 2
Items: 
Size: 623377 Color: 1
Size: 373734 Color: 0

Bin 784: 2979 of cap free
Amount of items: 2
Items: 
Size: 781269 Color: 0
Size: 215753 Color: 1

Bin 785: 2988 of cap free
Amount of items: 2
Items: 
Size: 771576 Color: 1
Size: 225437 Color: 0

Bin 786: 2998 of cap free
Amount of items: 2
Items: 
Size: 710002 Color: 0
Size: 287001 Color: 1

Bin 787: 3027 of cap free
Amount of items: 2
Items: 
Size: 616444 Color: 1
Size: 380530 Color: 0

Bin 788: 3033 of cap free
Amount of items: 2
Items: 
Size: 757983 Color: 1
Size: 238985 Color: 0

Bin 789: 3059 of cap free
Amount of items: 2
Items: 
Size: 541505 Color: 1
Size: 455437 Color: 0

Bin 790: 3061 of cap free
Amount of items: 3
Items: 
Size: 611549 Color: 0
Size: 205112 Color: 1
Size: 180279 Color: 0

Bin 791: 3073 of cap free
Amount of items: 2
Items: 
Size: 609073 Color: 1
Size: 387855 Color: 0

Bin 792: 3082 of cap free
Amount of items: 2
Items: 
Size: 564451 Color: 1
Size: 432468 Color: 0

Bin 793: 3114 of cap free
Amount of items: 2
Items: 
Size: 644367 Color: 1
Size: 352520 Color: 0

Bin 794: 3117 of cap free
Amount of items: 2
Items: 
Size: 663547 Color: 0
Size: 333337 Color: 1

Bin 795: 3120 of cap free
Amount of items: 2
Items: 
Size: 561136 Color: 0
Size: 435745 Color: 1

Bin 796: 3159 of cap free
Amount of items: 2
Items: 
Size: 564601 Color: 0
Size: 432241 Color: 1

Bin 797: 3167 of cap free
Amount of items: 2
Items: 
Size: 597274 Color: 1
Size: 399560 Color: 0

Bin 798: 3168 of cap free
Amount of items: 2
Items: 
Size: 576473 Color: 0
Size: 420360 Color: 1

Bin 799: 3168 of cap free
Amount of items: 2
Items: 
Size: 757981 Color: 1
Size: 238852 Color: 0

Bin 800: 3206 of cap free
Amount of items: 2
Items: 
Size: 596933 Color: 1
Size: 399862 Color: 0

Bin 801: 3216 of cap free
Amount of items: 2
Items: 
Size: 771946 Color: 0
Size: 224839 Color: 1

Bin 802: 3219 of cap free
Amount of items: 2
Items: 
Size: 532700 Color: 0
Size: 464082 Color: 1

Bin 803: 3246 of cap free
Amount of items: 2
Items: 
Size: 643460 Color: 0
Size: 353295 Color: 1

Bin 804: 3257 of cap free
Amount of items: 2
Items: 
Size: 694962 Color: 0
Size: 301782 Color: 1

Bin 805: 3308 of cap free
Amount of items: 2
Items: 
Size: 623475 Color: 0
Size: 373218 Color: 1

Bin 806: 3323 of cap free
Amount of items: 2
Items: 
Size: 723254 Color: 1
Size: 273424 Color: 0

Bin 807: 3362 of cap free
Amount of items: 2
Items: 
Size: 791743 Color: 1
Size: 204896 Color: 0

Bin 808: 3387 of cap free
Amount of items: 2
Items: 
Size: 628583 Color: 1
Size: 368031 Color: 0

Bin 809: 3391 of cap free
Amount of items: 2
Items: 
Size: 570666 Color: 0
Size: 425944 Color: 1

Bin 810: 3394 of cap free
Amount of items: 2
Items: 
Size: 667568 Color: 0
Size: 329039 Color: 1

Bin 811: 3417 of cap free
Amount of items: 2
Items: 
Size: 796192 Color: 0
Size: 200392 Color: 1

Bin 812: 3450 of cap free
Amount of items: 2
Items: 
Size: 688499 Color: 0
Size: 308052 Color: 1

Bin 813: 3505 of cap free
Amount of items: 2
Items: 
Size: 742263 Color: 1
Size: 254233 Color: 0

Bin 814: 3510 of cap free
Amount of items: 2
Items: 
Size: 711706 Color: 1
Size: 284785 Color: 0

Bin 815: 3520 of cap free
Amount of items: 2
Items: 
Size: 635533 Color: 0
Size: 360948 Color: 1

Bin 816: 3561 of cap free
Amount of items: 2
Items: 
Size: 623264 Color: 0
Size: 373176 Color: 1

Bin 817: 3619 of cap free
Amount of items: 2
Items: 
Size: 528558 Color: 1
Size: 467824 Color: 0

Bin 818: 3680 of cap free
Amount of items: 2
Items: 
Size: 688427 Color: 0
Size: 307894 Color: 1

Bin 819: 3706 of cap free
Amount of items: 2
Items: 
Size: 682084 Color: 1
Size: 314211 Color: 0

Bin 820: 3712 of cap free
Amount of items: 2
Items: 
Size: 740593 Color: 0
Size: 255696 Color: 1

Bin 821: 3820 of cap free
Amount of items: 2
Items: 
Size: 528251 Color: 0
Size: 467930 Color: 1

Bin 822: 3830 of cap free
Amount of items: 2
Items: 
Size: 776127 Color: 0
Size: 220044 Color: 1

Bin 823: 3857 of cap free
Amount of items: 2
Items: 
Size: 667251 Color: 0
Size: 328893 Color: 1

Bin 824: 3857 of cap free
Amount of items: 2
Items: 
Size: 650378 Color: 0
Size: 345766 Color: 1

Bin 825: 3869 of cap free
Amount of items: 2
Items: 
Size: 556488 Color: 1
Size: 439644 Color: 0

Bin 826: 3911 of cap free
Amount of items: 2
Items: 
Size: 728747 Color: 1
Size: 267343 Color: 0

Bin 827: 3912 of cap free
Amount of items: 2
Items: 
Size: 665613 Color: 1
Size: 330476 Color: 0

Bin 828: 3938 of cap free
Amount of items: 2
Items: 
Size: 753249 Color: 1
Size: 242814 Color: 0

Bin 829: 3986 of cap free
Amount of items: 2
Items: 
Size: 575833 Color: 0
Size: 420182 Color: 1

Bin 830: 4079 of cap free
Amount of items: 2
Items: 
Size: 788123 Color: 0
Size: 207799 Color: 1

Bin 831: 4095 of cap free
Amount of items: 2
Items: 
Size: 544973 Color: 0
Size: 450933 Color: 1

Bin 832: 4113 of cap free
Amount of items: 2
Items: 
Size: 502862 Color: 0
Size: 493026 Color: 1

Bin 833: 4145 of cap free
Amount of items: 2
Items: 
Size: 665545 Color: 1
Size: 330311 Color: 0

Bin 834: 4165 of cap free
Amount of items: 2
Items: 
Size: 742102 Color: 0
Size: 253734 Color: 1

Bin 835: 4177 of cap free
Amount of items: 3
Items: 
Size: 682275 Color: 0
Size: 158737 Color: 1
Size: 154812 Color: 1

Bin 836: 4234 of cap free
Amount of items: 2
Items: 
Size: 527899 Color: 0
Size: 467868 Color: 1

Bin 837: 4240 of cap free
Amount of items: 2
Items: 
Size: 541349 Color: 1
Size: 454412 Color: 0

Bin 838: 4319 of cap free
Amount of items: 2
Items: 
Size: 513446 Color: 1
Size: 482236 Color: 0

Bin 839: 4353 of cap free
Amount of items: 2
Items: 
Size: 502780 Color: 0
Size: 492868 Color: 1

Bin 840: 4395 of cap free
Amount of items: 2
Items: 
Size: 681811 Color: 1
Size: 313795 Color: 0

Bin 841: 4399 of cap free
Amount of items: 2
Items: 
Size: 728308 Color: 1
Size: 267294 Color: 0

Bin 842: 4441 of cap free
Amount of items: 3
Items: 
Size: 570742 Color: 0
Size: 244132 Color: 0
Size: 180686 Color: 1

Bin 843: 4442 of cap free
Amount of items: 2
Items: 
Size: 687930 Color: 0
Size: 307629 Color: 1

Bin 844: 4469 of cap free
Amount of items: 2
Items: 
Size: 650408 Color: 0
Size: 345124 Color: 1

Bin 845: 4515 of cap free
Amount of items: 2
Items: 
Size: 532429 Color: 0
Size: 463057 Color: 1

Bin 846: 4583 of cap free
Amount of items: 2
Items: 
Size: 604197 Color: 1
Size: 391221 Color: 0

Bin 847: 4659 of cap free
Amount of items: 2
Items: 
Size: 513203 Color: 1
Size: 482139 Color: 0

Bin 848: 4705 of cap free
Amount of items: 2
Items: 
Size: 795340 Color: 0
Size: 199956 Color: 1

Bin 849: 4760 of cap free
Amount of items: 2
Items: 
Size: 681764 Color: 1
Size: 313477 Color: 0

Bin 850: 4781 of cap free
Amount of items: 2
Items: 
Size: 527613 Color: 1
Size: 467607 Color: 0

Bin 851: 4798 of cap free
Amount of items: 2
Items: 
Size: 687799 Color: 0
Size: 307404 Color: 1

Bin 852: 4814 of cap free
Amount of items: 2
Items: 
Size: 655823 Color: 0
Size: 339364 Color: 1

Bin 853: 4835 of cap free
Amount of items: 2
Items: 
Size: 652729 Color: 1
Size: 342437 Color: 0

Bin 854: 4846 of cap free
Amount of items: 2
Items: 
Size: 795314 Color: 0
Size: 199841 Color: 1

Bin 855: 4904 of cap free
Amount of items: 2
Items: 
Size: 796379 Color: 0
Size: 198718 Color: 1

Bin 856: 5076 of cap free
Amount of items: 2
Items: 
Size: 647625 Color: 1
Size: 347300 Color: 0

Bin 857: 5102 of cap free
Amount of items: 2
Items: 
Size: 511686 Color: 0
Size: 483213 Color: 1

Bin 858: 5103 of cap free
Amount of items: 2
Items: 
Size: 785046 Color: 1
Size: 209852 Color: 0

Bin 859: 5112 of cap free
Amount of items: 2
Items: 
Size: 796220 Color: 0
Size: 198669 Color: 1

Bin 860: 5122 of cap free
Amount of items: 2
Items: 
Size: 501928 Color: 0
Size: 492951 Color: 1

Bin 861: 5126 of cap free
Amount of items: 2
Items: 
Size: 538312 Color: 0
Size: 456563 Color: 1

Bin 862: 5160 of cap free
Amount of items: 2
Items: 
Size: 674644 Color: 0
Size: 320197 Color: 1

Bin 863: 5278 of cap free
Amount of items: 2
Items: 
Size: 611727 Color: 0
Size: 382996 Color: 1

Bin 864: 5376 of cap free
Amount of items: 2
Items: 
Size: 741611 Color: 1
Size: 253014 Color: 0

Bin 865: 5379 of cap free
Amount of items: 2
Items: 
Size: 786989 Color: 0
Size: 207633 Color: 1

Bin 866: 5384 of cap free
Amount of items: 2
Items: 
Size: 649028 Color: 0
Size: 345589 Color: 1

Bin 867: 5390 of cap free
Amount of items: 3
Items: 
Size: 433282 Color: 0
Size: 367689 Color: 1
Size: 193640 Color: 0

Bin 868: 5446 of cap free
Amount of items: 2
Items: 
Size: 574444 Color: 0
Size: 420111 Color: 1

Bin 869: 5613 of cap free
Amount of items: 2
Items: 
Size: 655526 Color: 0
Size: 338862 Color: 1

Bin 870: 5675 of cap free
Amount of items: 2
Items: 
Size: 544701 Color: 0
Size: 449625 Color: 1

Bin 871: 5684 of cap free
Amount of items: 2
Items: 
Size: 649187 Color: 0
Size: 345130 Color: 1

Bin 872: 5934 of cap free
Amount of items: 2
Items: 
Size: 663902 Color: 1
Size: 330165 Color: 0

Bin 873: 6056 of cap free
Amount of items: 2
Items: 
Size: 501591 Color: 0
Size: 492354 Color: 1

Bin 874: 6056 of cap free
Amount of items: 2
Items: 
Size: 544624 Color: 0
Size: 449321 Color: 1

Bin 875: 6158 of cap free
Amount of items: 2
Items: 
Size: 785061 Color: 1
Size: 208782 Color: 0

Bin 876: 6198 of cap free
Amount of items: 2
Items: 
Size: 510613 Color: 0
Size: 483190 Color: 1

Bin 877: 6268 of cap free
Amount of items: 2
Items: 
Size: 686455 Color: 0
Size: 307278 Color: 1

Bin 878: 6375 of cap free
Amount of items: 2
Items: 
Size: 583133 Color: 1
Size: 410493 Color: 0

Bin 879: 6499 of cap free
Amount of items: 2
Items: 
Size: 648373 Color: 0
Size: 345129 Color: 1

Bin 880: 6503 of cap free
Amount of items: 2
Items: 
Size: 537264 Color: 0
Size: 456234 Color: 1

Bin 881: 6724 of cap free
Amount of items: 2
Items: 
Size: 612587 Color: 0
Size: 380690 Color: 1

Bin 882: 7012 of cap free
Amount of items: 2
Items: 
Size: 796111 Color: 1
Size: 196878 Color: 0

Bin 883: 7095 of cap free
Amount of items: 2
Items: 
Size: 611801 Color: 0
Size: 381105 Color: 1

Bin 884: 7185 of cap free
Amount of items: 2
Items: 
Size: 765553 Color: 1
Size: 227263 Color: 0

Bin 885: 7214 of cap free
Amount of items: 2
Items: 
Size: 786903 Color: 0
Size: 205884 Color: 1

Bin 886: 7309 of cap free
Amount of items: 2
Items: 
Size: 500373 Color: 0
Size: 492319 Color: 1

Bin 887: 7575 of cap free
Amount of items: 2
Items: 
Size: 614608 Color: 1
Size: 377818 Color: 0

Bin 888: 7768 of cap free
Amount of items: 2
Items: 
Size: 540679 Color: 1
Size: 451554 Color: 0

Bin 889: 8154 of cap free
Amount of items: 2
Items: 
Size: 731023 Color: 0
Size: 260824 Color: 1

Bin 890: 8164 of cap free
Amount of items: 2
Items: 
Size: 647284 Color: 0
Size: 344553 Color: 1

Bin 891: 8404 of cap free
Amount of items: 2
Items: 
Size: 614260 Color: 1
Size: 377337 Color: 0

Bin 892: 8756 of cap free
Amount of items: 2
Items: 
Size: 523001 Color: 1
Size: 468244 Color: 0

Bin 893: 8812 of cap free
Amount of items: 3
Items: 
Size: 484488 Color: 1
Size: 364875 Color: 0
Size: 141826 Color: 1

Bin 894: 9432 of cap free
Amount of items: 2
Items: 
Size: 737767 Color: 1
Size: 252802 Color: 0

Bin 895: 9947 of cap free
Amount of items: 2
Items: 
Size: 775841 Color: 0
Size: 214213 Color: 1

Bin 896: 10712 of cap free
Amount of items: 2
Items: 
Size: 737594 Color: 1
Size: 251695 Color: 0

Bin 897: 12590 of cap free
Amount of items: 2
Items: 
Size: 539191 Color: 1
Size: 448220 Color: 0

Bin 898: 13537 of cap free
Amount of items: 3
Items: 
Size: 741261 Color: 1
Size: 139253 Color: 0
Size: 105950 Color: 0

Bin 899: 13558 of cap free
Amount of items: 2
Items: 
Size: 506854 Color: 1
Size: 479589 Color: 0

Bin 900: 14241 of cap free
Amount of items: 2
Items: 
Size: 506112 Color: 1
Size: 479648 Color: 0

Bin 901: 17172 of cap free
Amount of items: 2
Items: 
Size: 584243 Color: 0
Size: 398586 Color: 1

Bin 902: 17777 of cap free
Amount of items: 2
Items: 
Size: 505204 Color: 1
Size: 477020 Color: 0

Bin 903: 18372 of cap free
Amount of items: 2
Items: 
Size: 776521 Color: 0
Size: 205108 Color: 1

Bin 904: 21151 of cap free
Amount of items: 2
Items: 
Size: 798989 Color: 1
Size: 179861 Color: 0

Bin 905: 21779 of cap free
Amount of items: 2
Items: 
Size: 758405 Color: 1
Size: 219817 Color: 0

Bin 906: 32531 of cap free
Amount of items: 2
Items: 
Size: 693576 Color: 1
Size: 273894 Color: 0

Bin 907: 35717 of cap free
Amount of items: 2
Items: 
Size: 601945 Color: 1
Size: 362339 Color: 0

Total size: 905774311
Total free space: 1226596

