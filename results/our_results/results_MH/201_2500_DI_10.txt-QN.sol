Capicity Bin: 2012
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 684 Color: 127
Size: 464 Color: 114
Size: 420 Color: 110
Size: 148 Color: 64
Size: 136 Color: 62
Size: 72 Color: 40
Size: 52 Color: 26
Size: 36 Color: 7

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1502 Color: 163
Size: 426 Color: 111
Size: 76 Color: 43
Size: 8 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1618 Color: 172
Size: 330 Color: 101
Size: 56 Color: 29
Size: 8 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 188
Size: 218 Color: 82
Size: 40 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 168
Size: 366 Color: 105
Size: 72 Color: 39

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 191
Size: 208 Color: 79
Size: 40 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 187
Size: 221 Color: 84
Size: 42 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1110 Color: 142
Size: 682 Color: 126
Size: 220 Color: 83

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 176
Size: 293 Color: 96
Size: 58 Color: 32

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1159 Color: 145
Size: 711 Color: 128
Size: 142 Color: 63

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 198
Size: 181 Color: 70
Size: 36 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 180
Size: 262 Color: 92
Size: 52 Color: 27

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 170
Size: 341 Color: 103
Size: 68 Color: 37

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 164
Size: 411 Color: 109
Size: 80 Color: 45

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1011 Color: 140
Size: 919 Color: 137
Size: 82 Color: 47

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 181
Size: 261 Color: 91
Size: 50 Color: 24

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 155
Size: 553 Color: 119
Size: 110 Color: 56

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 185
Size: 230 Color: 87
Size: 44 Color: 20

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 197
Size: 186 Color: 72
Size: 36 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 169
Size: 362 Color: 104
Size: 68 Color: 38

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 160
Size: 478 Color: 115
Size: 92 Color: 51

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1279 Color: 152
Size: 643 Color: 124
Size: 90 Color: 50

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 183
Size: 241 Color: 89
Size: 48 Color: 23

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 171
Size: 331 Color: 102
Size: 64 Color: 35

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 175
Size: 302 Color: 97
Size: 56 Color: 31

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 196
Size: 191 Color: 74
Size: 36 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 190
Size: 210 Color: 80
Size: 40 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1194 Color: 146
Size: 754 Color: 132
Size: 64 Color: 34

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 193
Size: 202 Color: 76
Size: 40 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 199
Size: 182 Color: 71
Size: 32 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 154
Size: 574 Color: 120
Size: 112 Color: 57

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1010 Color: 139
Size: 838 Color: 135
Size: 164 Color: 67

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 189
Size: 213 Color: 81
Size: 42 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1007 Color: 138
Size: 839 Color: 136
Size: 166 Color: 68

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 201
Size: 189 Color: 73
Size: 16 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1207 Color: 148
Size: 671 Color: 125
Size: 134 Color: 61

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 200
Size: 174 Color: 69
Size: 32 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 158
Size: 506 Color: 117
Size: 100 Color: 54

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 1260 Color: 150
Size: 752 Color: 131

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 147
Size: 747 Color: 129
Size: 66 Color: 36

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 143
Size: 751 Color: 130
Size: 150 Color: 65

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1151 Color: 144
Size: 787 Color: 133
Size: 74 Color: 42

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 182
Size: 246 Color: 90
Size: 48 Color: 22

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 165
Size: 410 Color: 108
Size: 80 Color: 46

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 179
Size: 269 Color: 93
Size: 52 Color: 28

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 174
Size: 329 Color: 100
Size: 30 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 178
Size: 282 Color: 94
Size: 52 Color: 25

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 167
Size: 367 Color: 106
Size: 72 Color: 41

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1431 Color: 159
Size: 485 Color: 116
Size: 96 Color: 53

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 161
Size: 463 Color: 113
Size: 92 Color: 52

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1247 Color: 149
Size: 639 Color: 123
Size: 126 Color: 60

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 194
Size: 203 Color: 77
Size: 32 Color: 5

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1381 Color: 156
Size: 527 Color: 118
Size: 104 Color: 55

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 173
Size: 309 Color: 98
Size: 60 Color: 33

Bin 55: 0 of cap free
Amount of items: 4
Items: 
Size: 1392 Color: 157
Size: 312 Color: 99
Size: 224 Color: 86
Size: 84 Color: 49

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 177
Size: 287 Color: 95
Size: 56 Color: 30

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 192
Size: 207 Color: 78
Size: 40 Color: 12

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 153
Size: 585 Color: 121
Size: 116 Color: 58

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1063 Color: 141
Size: 791 Color: 134
Size: 158 Color: 66

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 151
Size: 622 Color: 122
Size: 124 Color: 59

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1747 Color: 186
Size: 221 Color: 85
Size: 44 Color: 19

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 162
Size: 429 Color: 112
Size: 84 Color: 48

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 166
Size: 381 Color: 107
Size: 76 Color: 44

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 195
Size: 193 Color: 75
Size: 38 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 184
Size: 237 Color: 88
Size: 46 Color: 21

Total size: 130780
Total free space: 0

