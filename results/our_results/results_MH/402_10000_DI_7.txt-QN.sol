Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 322
Size: 1874 Color: 226
Size: 228 Color: 59

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 324
Size: 1914 Color: 229
Size: 106 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6266 Color: 328
Size: 988 Color: 166
Size: 882 Color: 156

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 332
Size: 1524 Color: 208
Size: 304 Color: 82

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 336
Size: 1398 Color: 201
Size: 334 Color: 88

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6421 Color: 337
Size: 1431 Color: 203
Size: 284 Color: 77

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6501 Color: 342
Size: 1211 Color: 189
Size: 424 Color: 108

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6502 Color: 343
Size: 854 Color: 152
Size: 780 Color: 145

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 349
Size: 1233 Color: 191
Size: 246 Color: 65

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 350
Size: 892 Color: 157
Size: 584 Color: 124

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6673 Color: 351
Size: 1221 Color: 190
Size: 242 Color: 63

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 355
Size: 1236 Color: 192
Size: 144 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 356
Size: 1362 Color: 198
Size: 16 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6782 Color: 359
Size: 982 Color: 165
Size: 372 Color: 98

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 361
Size: 1140 Color: 184
Size: 176 Color: 32

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6943 Color: 369
Size: 995 Color: 168
Size: 198 Color: 43

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 371
Size: 600 Color: 126
Size: 584 Color: 125

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 373
Size: 1000 Color: 170
Size: 180 Color: 34

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 374
Size: 948 Color: 164
Size: 226 Color: 57

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7017 Color: 376
Size: 933 Color: 163
Size: 186 Color: 39

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7079 Color: 383
Size: 881 Color: 154
Size: 176 Color: 31

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7081 Color: 384
Size: 881 Color: 155
Size: 174 Color: 29

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7089 Color: 386
Size: 997 Color: 169
Size: 50 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7114 Color: 388
Size: 830 Color: 150
Size: 192 Color: 40

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 391
Size: 852 Color: 151
Size: 128 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 392
Size: 708 Color: 140
Size: 256 Color: 69

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 393
Size: 922 Color: 160
Size: 40 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 394
Size: 734 Color: 143
Size: 208 Color: 45

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7268 Color: 398
Size: 672 Color: 132
Size: 196 Color: 42

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 399
Size: 706 Color: 139
Size: 160 Color: 21

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 400
Size: 626 Color: 129
Size: 220 Color: 53

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 401
Size: 548 Color: 122
Size: 296 Color: 80

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7322 Color: 402
Size: 630 Color: 130
Size: 184 Color: 38

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6121 Color: 325
Size: 1742 Color: 218
Size: 272 Color: 72

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6269 Color: 329
Size: 1866 Color: 225

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 6346 Color: 334
Size: 1789 Color: 222

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6454 Color: 339
Size: 1681 Color: 216

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 357
Size: 1363 Color: 199

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6777 Color: 358
Size: 682 Color: 138
Size: 676 Color: 137

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6855 Color: 363
Size: 1144 Color: 185
Size: 136 Color: 10

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6877 Color: 366
Size: 930 Color: 161
Size: 328 Color: 87

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 368
Size: 786 Color: 146
Size: 412 Color: 107

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6951 Color: 370
Size: 672 Color: 134
Size: 512 Color: 121

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6954 Color: 372
Size: 1181 Color: 188

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 395
Size: 931 Color: 162

Bin 46: 2 of cap free
Amount of items: 5
Items: 
Size: 4078 Color: 277
Size: 3372 Color: 262
Size: 264 Color: 70
Size: 212 Color: 49
Size: 208 Color: 48

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 5991 Color: 319
Size: 1891 Color: 227
Size: 252 Color: 68

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 346
Size: 1554 Color: 209

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 365
Size: 1266 Color: 195

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 7004 Color: 375
Size: 1130 Color: 182

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 7034 Color: 379
Size: 1100 Color: 179

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 7042 Color: 380
Size: 1092 Color: 178

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 5497 Color: 303
Size: 2636 Color: 248

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 5842 Color: 311
Size: 1158 Color: 187
Size: 1133 Color: 183

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 344
Size: 1613 Color: 213

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 6721 Color: 353
Size: 1412 Color: 202

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 7022 Color: 378
Size: 1111 Color: 181

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 385
Size: 1051 Color: 174

Bin 59: 4 of cap free
Amount of items: 5
Items: 
Size: 4086 Color: 278
Size: 3378 Color: 263
Size: 252 Color: 67
Size: 208 Color: 47
Size: 208 Color: 46

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 5890 Color: 315
Size: 1906 Color: 228
Size: 336 Color: 89

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 387
Size: 1042 Color: 173

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 7116 Color: 389
Size: 1016 Color: 171

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 7218 Color: 396
Size: 914 Color: 159

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 7258 Color: 397
Size: 874 Color: 153

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6750 Color: 354
Size: 1381 Color: 200

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 7142 Color: 390
Size: 989 Color: 167

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 367
Size: 1244 Color: 193

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 382
Size: 1062 Color: 176

Bin 69: 7 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 347
Size: 1511 Color: 207

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 6685 Color: 352
Size: 1444 Color: 204

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 7060 Color: 381
Size: 1069 Color: 177

Bin 72: 8 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 338
Size: 1684 Color: 217

Bin 73: 8 of cap free
Amount of items: 2
Items: 
Size: 6828 Color: 362
Size: 1300 Color: 196

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 364
Size: 1262 Color: 194

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 7021 Color: 377
Size: 1107 Color: 180

Bin 76: 9 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 314
Size: 2164 Color: 235
Size: 96 Color: 4

Bin 77: 10 of cap free
Amount of items: 3
Items: 
Size: 4299 Color: 280
Size: 3643 Color: 271
Size: 184 Color: 37

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 5850 Color: 313
Size: 2276 Color: 240

Bin 79: 10 of cap free
Amount of items: 2
Items: 
Size: 6282 Color: 331
Size: 1844 Color: 224

Bin 80: 10 of cap free
Amount of items: 2
Items: 
Size: 6364 Color: 335
Size: 1762 Color: 220

Bin 81: 10 of cap free
Amount of items: 2
Items: 
Size: 6805 Color: 360
Size: 1321 Color: 197

Bin 82: 11 of cap free
Amount of items: 4
Items: 
Size: 4092 Color: 279
Size: 3633 Color: 270
Size: 208 Color: 44
Size: 192 Color: 41

Bin 83: 11 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 297
Size: 2749 Color: 249
Size: 140 Color: 12

Bin 84: 12 of cap free
Amount of items: 2
Items: 
Size: 5898 Color: 316
Size: 2226 Color: 238

Bin 85: 13 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 327
Size: 1922 Color: 230

Bin 86: 14 of cap free
Amount of items: 3
Items: 
Size: 4839 Color: 292
Size: 3131 Color: 256
Size: 152 Color: 16

Bin 87: 14 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 310
Size: 1484 Color: 205
Size: 804 Color: 148

Bin 88: 14 of cap free
Amount of items: 2
Items: 
Size: 6462 Color: 340
Size: 1660 Color: 215

Bin 89: 15 of cap free
Amount of items: 2
Items: 
Size: 5994 Color: 320
Size: 2127 Color: 234

Bin 90: 15 of cap free
Amount of items: 2
Items: 
Size: 6479 Color: 341
Size: 1642 Color: 214

Bin 91: 16 of cap free
Amount of items: 19
Items: 
Size: 648 Color: 131
Size: 624 Color: 128
Size: 602 Color: 127
Size: 508 Color: 120
Size: 508 Color: 119
Size: 504 Color: 118
Size: 480 Color: 117
Size: 476 Color: 116
Size: 464 Color: 115
Size: 448 Color: 114
Size: 448 Color: 113
Size: 444 Color: 112
Size: 432 Color: 111
Size: 400 Color: 106
Size: 234 Color: 60
Size: 228 Color: 58
Size: 224 Color: 56
Size: 224 Color: 55
Size: 224 Color: 54

Bin 92: 16 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 348
Size: 1494 Color: 206

Bin 93: 17 of cap free
Amount of items: 3
Items: 
Size: 5796 Color: 309
Size: 1557 Color: 211
Size: 766 Color: 144

Bin 94: 17 of cap free
Amount of items: 2
Items: 
Size: 5845 Color: 312
Size: 2274 Color: 239

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 5082 Color: 295
Size: 3036 Color: 255

Bin 96: 20 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 330
Size: 1842 Color: 223

Bin 97: 21 of cap free
Amount of items: 3
Items: 
Size: 4387 Color: 284
Size: 3560 Color: 269
Size: 168 Color: 26

Bin 98: 21 of cap free
Amount of items: 2
Items: 
Size: 6050 Color: 323
Size: 2065 Color: 233

Bin 99: 23 of cap free
Amount of items: 4
Items: 
Size: 4379 Color: 283
Size: 3390 Color: 266
Size: 172 Color: 28
Size: 172 Color: 27

Bin 100: 23 of cap free
Amount of items: 2
Items: 
Size: 6551 Color: 345
Size: 1562 Color: 212

Bin 101: 25 of cap free
Amount of items: 2
Items: 
Size: 6325 Color: 333
Size: 1786 Color: 221

Bin 102: 26 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 326
Size: 1962 Color: 232

Bin 103: 28 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 291
Size: 3160 Color: 259
Size: 152 Color: 17

Bin 104: 30 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 293
Size: 2926 Color: 253
Size: 144 Color: 14

Bin 105: 30 of cap free
Amount of items: 2
Items: 
Size: 5932 Color: 318
Size: 2174 Color: 237

Bin 106: 34 of cap free
Amount of items: 4
Items: 
Size: 4352 Color: 281
Size: 3382 Color: 264
Size: 184 Color: 36
Size: 184 Color: 35

Bin 107: 35 of cap free
Amount of items: 2
Items: 
Size: 5077 Color: 294
Size: 3024 Color: 254

Bin 108: 37 of cap free
Amount of items: 4
Items: 
Size: 4355 Color: 282
Size: 3388 Color: 265
Size: 180 Color: 33
Size: 176 Color: 30

Bin 109: 37 of cap free
Amount of items: 4
Items: 
Size: 4626 Color: 287
Size: 3149 Color: 257
Size: 164 Color: 23
Size: 160 Color: 22

Bin 110: 39 of cap free
Amount of items: 2
Items: 
Size: 5930 Color: 317
Size: 2167 Color: 236

Bin 111: 44 of cap free
Amount of items: 2
Items: 
Size: 4076 Color: 276
Size: 4016 Color: 272

Bin 112: 44 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 286
Size: 3428 Color: 268
Size: 164 Color: 24

Bin 113: 45 of cap free
Amount of items: 3
Items: 
Size: 4700 Color: 289
Size: 3235 Color: 261
Size: 156 Color: 19

Bin 114: 46 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 299
Size: 2542 Color: 245
Size: 136 Color: 11

Bin 115: 48 of cap free
Amount of items: 3
Items: 
Size: 5474 Color: 302
Size: 1554 Color: 210
Size: 1060 Color: 175

Bin 116: 48 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 308
Size: 1754 Color: 219
Size: 552 Color: 123

Bin 117: 50 of cap free
Amount of items: 3
Items: 
Size: 4779 Color: 290
Size: 3151 Color: 258
Size: 156 Color: 18

Bin 118: 57 of cap free
Amount of items: 2
Items: 
Size: 5659 Color: 307
Size: 2420 Color: 244

Bin 119: 62 of cap free
Amount of items: 3
Items: 
Size: 6026 Color: 321
Size: 1956 Color: 231
Size: 92 Color: 3

Bin 120: 72 of cap free
Amount of items: 3
Items: 
Size: 5537 Color: 305
Size: 2395 Color: 242
Size: 132 Color: 8

Bin 121: 73 of cap free
Amount of items: 2
Items: 
Size: 5275 Color: 298
Size: 2788 Color: 251

Bin 122: 74 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 306
Size: 2410 Color: 243
Size: 104 Color: 5

Bin 123: 85 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 304
Size: 2385 Color: 241
Size: 136 Color: 9

Bin 124: 117 of cap free
Amount of items: 3
Items: 
Size: 4071 Color: 275
Size: 2922 Color: 252
Size: 1026 Color: 172

Bin 125: 119 of cap free
Amount of items: 2
Items: 
Size: 5466 Color: 301
Size: 2551 Color: 247

Bin 126: 126 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 296
Size: 2776 Color: 250
Size: 144 Color: 13

Bin 127: 128 of cap free
Amount of items: 2
Items: 
Size: 5462 Color: 300
Size: 2546 Color: 246

Bin 128: 134 of cap free
Amount of items: 3
Items: 
Size: 4630 Color: 288
Size: 3212 Color: 260
Size: 160 Color: 20

Bin 129: 141 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 285
Size: 3391 Color: 267
Size: 168 Color: 25

Bin 130: 165 of cap free
Amount of items: 8
Items: 
Size: 4069 Color: 273
Size: 724 Color: 142
Size: 722 Color: 141
Size: 676 Color: 136
Size: 676 Color: 135
Size: 672 Color: 133
Size: 216 Color: 52
Size: 216 Color: 51

Bin 131: 182 of cap free
Amount of items: 6
Items: 
Size: 4070 Color: 274
Size: 1150 Color: 186
Size: 900 Color: 158
Size: 820 Color: 149
Size: 802 Color: 147
Size: 212 Color: 50

Bin 132: 246 of cap free
Amount of items: 24
Items: 
Size: 432 Color: 110
Size: 432 Color: 109
Size: 392 Color: 105
Size: 384 Color: 104
Size: 380 Color: 103
Size: 380 Color: 102
Size: 380 Color: 101
Size: 378 Color: 100
Size: 372 Color: 99
Size: 364 Color: 97
Size: 360 Color: 96
Size: 356 Color: 95
Size: 356 Color: 94
Size: 288 Color: 79
Size: 288 Color: 78
Size: 280 Color: 76
Size: 276 Color: 75
Size: 276 Color: 74
Size: 272 Color: 73
Size: 272 Color: 71
Size: 248 Color: 66
Size: 244 Color: 64
Size: 240 Color: 62
Size: 240 Color: 61

Bin 133: 5196 of cap free
Amount of items: 9
Items: 
Size: 352 Color: 93
Size: 348 Color: 92
Size: 348 Color: 91
Size: 344 Color: 90
Size: 322 Color: 86
Size: 310 Color: 85
Size: 308 Color: 84
Size: 308 Color: 83
Size: 300 Color: 81

Total size: 1073952
Total free space: 8136

