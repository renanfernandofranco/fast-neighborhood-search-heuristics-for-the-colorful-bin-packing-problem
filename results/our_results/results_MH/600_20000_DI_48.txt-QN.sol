Capicity Bin: 15808
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10664 Color: 455
Size: 4872 Color: 365
Size: 272 Color: 23

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11728 Color: 478
Size: 2156 Color: 264
Size: 1924 Color: 254

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 479
Size: 3408 Color: 327
Size: 668 Color: 137

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11836 Color: 482
Size: 3788 Color: 338
Size: 184 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 484
Size: 2950 Color: 306
Size: 1010 Color: 178

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12093 Color: 489
Size: 3097 Color: 313
Size: 618 Color: 130

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12114 Color: 490
Size: 1868 Color: 250
Size: 1826 Color: 246

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12192 Color: 493
Size: 3248 Color: 319
Size: 368 Color: 62

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 497
Size: 3040 Color: 309
Size: 440 Color: 85

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 501
Size: 1912 Color: 252
Size: 1432 Color: 212

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12516 Color: 505
Size: 2904 Color: 304
Size: 388 Color: 72

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 509
Size: 2704 Color: 294
Size: 512 Color: 107

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 516
Size: 2248 Color: 269
Size: 832 Color: 157

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12806 Color: 519
Size: 2250 Color: 271
Size: 752 Color: 148

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 520
Size: 2456 Color: 282
Size: 544 Color: 115

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 522
Size: 2424 Color: 279
Size: 512 Color: 106

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 524
Size: 2568 Color: 289
Size: 252 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13066 Color: 528
Size: 2284 Color: 273
Size: 458 Color: 90

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13110 Color: 531
Size: 2256 Color: 272
Size: 442 Color: 86

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13164 Color: 535
Size: 1508 Color: 218
Size: 1136 Color: 185

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 537
Size: 2248 Color: 270
Size: 368 Color: 64

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 538
Size: 2128 Color: 263
Size: 480 Color: 94

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 540
Size: 2008 Color: 258
Size: 544 Color: 114

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 546
Size: 1720 Color: 236
Size: 672 Color: 139

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13420 Color: 547
Size: 1636 Color: 226
Size: 752 Color: 149

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 552
Size: 1780 Color: 243
Size: 500 Color: 104

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 553
Size: 1404 Color: 208
Size: 832 Color: 156

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 558
Size: 1652 Color: 228
Size: 532 Color: 111

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 559
Size: 1582 Color: 221
Size: 600 Color: 125

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 560
Size: 1460 Color: 215
Size: 688 Color: 141

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 563
Size: 1452 Color: 213
Size: 640 Color: 133

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 564
Size: 1458 Color: 214
Size: 608 Color: 127

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 565
Size: 1312 Color: 195
Size: 752 Color: 150

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13746 Color: 566
Size: 1510 Color: 219
Size: 552 Color: 120

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 567
Size: 1312 Color: 196
Size: 744 Color: 145

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13754 Color: 568
Size: 1462 Color: 216
Size: 592 Color: 124

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13762 Color: 569
Size: 1410 Color: 209
Size: 636 Color: 132

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 570
Size: 1168 Color: 190
Size: 864 Color: 166

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 571
Size: 1728 Color: 239
Size: 300 Color: 32

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 573
Size: 1384 Color: 205
Size: 608 Color: 128

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13818 Color: 574
Size: 1662 Color: 229
Size: 328 Color: 45

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13864 Color: 576
Size: 1080 Color: 182
Size: 864 Color: 167

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 577
Size: 1166 Color: 189
Size: 736 Color: 144

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 578
Size: 1632 Color: 225
Size: 268 Color: 20

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13998 Color: 582
Size: 1316 Color: 199
Size: 494 Color: 98

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 586
Size: 1316 Color: 201
Size: 424 Color: 82

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 587
Size: 1336 Color: 202
Size: 368 Color: 63

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 589
Size: 1414 Color: 210
Size: 276 Color: 25

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 590
Size: 1312 Color: 198
Size: 372 Color: 65

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14128 Color: 591
Size: 1424 Color: 211
Size: 256 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14160 Color: 595
Size: 1216 Color: 192
Size: 432 Color: 83

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 597
Size: 1154 Color: 188
Size: 468 Color: 93

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 598
Size: 984 Color: 174
Size: 628 Color: 131

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 599
Size: 1144 Color: 187
Size: 464 Color: 91

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 600
Size: 1232 Color: 193
Size: 360 Color: 58

Bin 56: 1 of cap free
Amount of items: 7
Items: 
Size: 7920 Color: 411
Size: 2347 Color: 277
Size: 2100 Color: 260
Size: 2096 Color: 259
Size: 576 Color: 123
Size: 384 Color: 67
Size: 384 Color: 66

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 11671 Color: 477
Size: 3880 Color: 342
Size: 256 Color: 17

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 481
Size: 3782 Color: 337
Size: 192 Color: 10

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11845 Color: 483
Size: 2762 Color: 298
Size: 1200 Color: 191

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11970 Color: 486
Size: 2445 Color: 281
Size: 1392 Color: 206

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 12403 Color: 499
Size: 3404 Color: 326

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 12605 Color: 510
Size: 3202 Color: 317

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12618 Color: 511
Size: 2893 Color: 303
Size: 296 Color: 31

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 12725 Color: 515
Size: 3082 Color: 311

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12771 Color: 517
Size: 2884 Color: 302
Size: 152 Color: 4

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 13136 Color: 533
Size: 2671 Color: 293

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13183 Color: 536
Size: 2432 Color: 280
Size: 192 Color: 11

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 13618 Color: 557
Size: 2189 Color: 265

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 9878 Color: 435
Size: 5608 Color: 380
Size: 320 Color: 39

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 10642 Color: 452
Size: 3848 Color: 339
Size: 1316 Color: 200

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 10864 Color: 460
Size: 4942 Color: 369

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 13070 Color: 529
Size: 2736 Color: 296

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13282 Color: 542
Size: 2524 Color: 287

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13462 Color: 549
Size: 2344 Color: 276

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13700 Color: 562
Size: 2106 Color: 262

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13810 Color: 572
Size: 1996 Color: 257

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13972 Color: 581
Size: 1834 Color: 248

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 585
Size: 1744 Color: 240

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 596
Size: 1628 Color: 224

Bin 80: 3 of cap free
Amount of items: 11
Items: 
Size: 7907 Color: 406
Size: 1092 Color: 183
Size: 1056 Color: 181
Size: 1032 Color: 180
Size: 1024 Color: 179
Size: 992 Color: 177
Size: 992 Color: 176
Size: 528 Color: 110
Size: 400 Color: 75
Size: 392 Color: 74
Size: 390 Color: 73

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 12157 Color: 492
Size: 3648 Color: 332

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 13592 Color: 554
Size: 2213 Color: 268

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 548
Size: 2356 Color: 278

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 13600 Color: 555
Size: 2204 Color: 267

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 575
Size: 1976 Color: 256

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 13956 Color: 580
Size: 1848 Color: 249

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 14132 Color: 592
Size: 1672 Color: 231

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 14138 Color: 593
Size: 1666 Color: 230

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 14152 Color: 594
Size: 1652 Color: 227

Bin 90: 5 of cap free
Amount of items: 2
Items: 
Size: 12072 Color: 488
Size: 3731 Color: 335

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 13502 Color: 551
Size: 2301 Color: 275

Bin 92: 6 of cap free
Amount of items: 3
Items: 
Size: 8873 Color: 418
Size: 6585 Color: 398
Size: 344 Color: 52

Bin 93: 6 of cap free
Amount of items: 3
Items: 
Size: 10634 Color: 451
Size: 3856 Color: 340
Size: 1312 Color: 197

Bin 94: 6 of cap free
Amount of items: 2
Items: 
Size: 12486 Color: 503
Size: 3316 Color: 324

Bin 95: 6 of cap free
Amount of items: 2
Items: 
Size: 13049 Color: 527
Size: 2753 Color: 297

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 13610 Color: 556
Size: 2192 Color: 266

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 13914 Color: 579
Size: 1888 Color: 251

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 14054 Color: 584
Size: 1748 Color: 241

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 588
Size: 1692 Color: 232

Bin 100: 7 of cap free
Amount of items: 5
Items: 
Size: 7944 Color: 412
Size: 3084 Color: 312
Size: 3043 Color: 310
Size: 1362 Color: 204
Size: 368 Color: 61

Bin 101: 7 of cap free
Amount of items: 3
Items: 
Size: 10626 Color: 450
Size: 4895 Color: 366
Size: 280 Color: 27

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 13153 Color: 534
Size: 2648 Color: 291

Bin 103: 8 of cap free
Amount of items: 7
Items: 
Size: 7914 Color: 409
Size: 1768 Color: 242
Size: 1722 Color: 238
Size: 1722 Color: 237
Size: 1714 Color: 235
Size: 576 Color: 122
Size: 384 Color: 68

Bin 104: 8 of cap free
Amount of items: 3
Items: 
Size: 11592 Color: 476
Size: 4144 Color: 345
Size: 64 Color: 0

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 11920 Color: 485
Size: 3880 Color: 343

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 12116 Color: 491
Size: 3684 Color: 333

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 13000 Color: 526
Size: 2800 Color: 301

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 583
Size: 1796 Color: 244

Bin 109: 9 of cap free
Amount of items: 2
Items: 
Size: 13228 Color: 539
Size: 2571 Color: 290

Bin 110: 10 of cap free
Amount of items: 2
Items: 
Size: 12494 Color: 504
Size: 3304 Color: 323

Bin 111: 11 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 445
Size: 3879 Color: 341
Size: 1394 Color: 207

Bin 112: 11 of cap free
Amount of items: 2
Items: 
Size: 12348 Color: 498
Size: 3449 Color: 328

Bin 113: 12 of cap free
Amount of items: 2
Items: 
Size: 12234 Color: 495
Size: 3562 Color: 330

Bin 114: 12 of cap free
Amount of items: 2
Items: 
Size: 12268 Color: 496
Size: 3528 Color: 329

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 12814 Color: 521
Size: 2982 Color: 307

Bin 116: 12 of cap free
Amount of items: 2
Items: 
Size: 13292 Color: 544
Size: 2504 Color: 286

Bin 117: 13 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 541
Size: 2531 Color: 288

Bin 118: 15 of cap free
Amount of items: 2
Items: 
Size: 12529 Color: 507
Size: 3264 Color: 321

Bin 119: 16 of cap free
Amount of items: 3
Items: 
Size: 9296 Color: 428
Size: 6160 Color: 388
Size: 336 Color: 47

Bin 120: 16 of cap free
Amount of items: 3
Items: 
Size: 10588 Color: 449
Size: 4916 Color: 367
Size: 288 Color: 28

Bin 121: 16 of cap free
Amount of items: 3
Items: 
Size: 10728 Color: 457
Size: 4792 Color: 362
Size: 272 Color: 21

Bin 122: 16 of cap free
Amount of items: 2
Items: 
Size: 12664 Color: 513
Size: 3128 Color: 315

Bin 123: 16 of cap free
Amount of items: 2
Items: 
Size: 12692 Color: 514
Size: 3100 Color: 314

Bin 124: 16 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 518
Size: 3004 Color: 308

Bin 125: 16 of cap free
Amount of items: 2
Items: 
Size: 13290 Color: 543
Size: 2502 Color: 285

Bin 126: 17 of cap free
Amount of items: 3
Items: 
Size: 10480 Color: 444
Size: 3725 Color: 334
Size: 1586 Color: 222

Bin 127: 17 of cap free
Amount of items: 3
Items: 
Size: 10565 Color: 447
Size: 4938 Color: 368
Size: 288 Color: 29

Bin 128: 17 of cap free
Amount of items: 2
Items: 
Size: 12528 Color: 506
Size: 3263 Color: 320

Bin 129: 17 of cap free
Amount of items: 2
Items: 
Size: 12875 Color: 523
Size: 2916 Color: 305

Bin 130: 18 of cap free
Amount of items: 2
Items: 
Size: 13128 Color: 532
Size: 2662 Color: 292

Bin 131: 18 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 561
Size: 2102 Color: 261

Bin 132: 19 of cap free
Amount of items: 3
Items: 
Size: 11323 Color: 470
Size: 4306 Color: 353
Size: 160 Color: 5

Bin 133: 19 of cap free
Amount of items: 3
Items: 
Size: 11339 Color: 472
Size: 4322 Color: 355
Size: 128 Color: 2

Bin 134: 19 of cap free
Amount of items: 2
Items: 
Size: 12204 Color: 494
Size: 3585 Color: 331

Bin 135: 21 of cap free
Amount of items: 3
Items: 
Size: 9743 Color: 432
Size: 5724 Color: 384
Size: 320 Color: 41

Bin 136: 21 of cap free
Amount of items: 3
Items: 
Size: 11208 Color: 466
Size: 4371 Color: 357
Size: 208 Color: 12

Bin 137: 21 of cap free
Amount of items: 3
Items: 
Size: 11331 Color: 471
Size: 4314 Color: 354
Size: 142 Color: 3

Bin 138: 22 of cap free
Amount of items: 2
Items: 
Size: 13328 Color: 545
Size: 2458 Color: 283

Bin 139: 22 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 550
Size: 2286 Color: 274

Bin 140: 23 of cap free
Amount of items: 2
Items: 
Size: 12993 Color: 525
Size: 2792 Color: 300

Bin 141: 24 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 425
Size: 6348 Color: 390
Size: 340 Color: 49

Bin 142: 24 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 508
Size: 3220 Color: 318

Bin 143: 24 of cap free
Amount of items: 2
Items: 
Size: 13076 Color: 530
Size: 2708 Color: 295

Bin 144: 26 of cap free
Amount of items: 2
Items: 
Size: 11534 Color: 475
Size: 4248 Color: 348

Bin 145: 27 of cap free
Amount of items: 13
Items: 
Size: 7905 Color: 404
Size: 860 Color: 164
Size: 860 Color: 163
Size: 856 Color: 162
Size: 856 Color: 161
Size: 848 Color: 160
Size: 848 Color: 159
Size: 612 Color: 129
Size: 448 Color: 87
Size: 436 Color: 84
Size: 420 Color: 81
Size: 416 Color: 80
Size: 416 Color: 79

Bin 146: 27 of cap free
Amount of items: 2
Items: 
Size: 12427 Color: 500
Size: 3354 Color: 325

Bin 147: 28 of cap free
Amount of items: 3
Items: 
Size: 9836 Color: 434
Size: 5624 Color: 381
Size: 320 Color: 40

Bin 148: 28 of cap free
Amount of items: 2
Items: 
Size: 10584 Color: 448
Size: 5196 Color: 374

Bin 149: 28 of cap free
Amount of items: 2
Items: 
Size: 10796 Color: 459
Size: 4984 Color: 371

Bin 150: 28 of cap free
Amount of items: 3
Items: 
Size: 10944 Color: 461
Size: 4572 Color: 359
Size: 264 Color: 19

Bin 151: 30 of cap free
Amount of items: 3
Items: 
Size: 10658 Color: 454
Size: 4848 Color: 364
Size: 272 Color: 24

Bin 152: 32 of cap free
Amount of items: 9
Items: 
Size: 7908 Color: 407
Size: 1500 Color: 217
Size: 1352 Color: 203
Size: 1296 Color: 194
Size: 1136 Color: 186
Size: 1104 Color: 184
Size: 712 Color: 143
Size: 384 Color: 71
Size: 384 Color: 70

Bin 153: 32 of cap free
Amount of items: 2
Items: 
Size: 10544 Color: 446
Size: 5232 Color: 375

Bin 154: 33 of cap free
Amount of items: 2
Items: 
Size: 12472 Color: 502
Size: 3303 Color: 322

Bin 155: 34 of cap free
Amount of items: 2
Items: 
Size: 12035 Color: 487
Size: 3739 Color: 336

Bin 156: 35 of cap free
Amount of items: 2
Items: 
Size: 11786 Color: 480
Size: 3987 Color: 344

Bin 157: 35 of cap free
Amount of items: 2
Items: 
Size: 12628 Color: 512
Size: 3145 Color: 316

Bin 158: 36 of cap free
Amount of items: 7
Items: 
Size: 7912 Color: 408
Size: 1712 Color: 234
Size: 1706 Color: 233
Size: 1624 Color: 223
Size: 1522 Color: 220
Size: 912 Color: 170
Size: 384 Color: 69

Bin 159: 36 of cap free
Amount of items: 3
Items: 
Size: 10719 Color: 456
Size: 4781 Color: 361
Size: 272 Color: 22

Bin 160: 39 of cap free
Amount of items: 3
Items: 
Size: 9249 Color: 426
Size: 6184 Color: 389
Size: 336 Color: 48

Bin 161: 39 of cap free
Amount of items: 2
Items: 
Size: 11528 Color: 474
Size: 4241 Color: 347

Bin 162: 40 of cap free
Amount of items: 3
Items: 
Size: 10652 Color: 453
Size: 4836 Color: 363
Size: 280 Color: 26

Bin 163: 45 of cap free
Amount of items: 3
Items: 
Size: 9976 Color: 439
Size: 5467 Color: 379
Size: 320 Color: 36

Bin 164: 48 of cap free
Amount of items: 2
Items: 
Size: 10780 Color: 458
Size: 4980 Color: 370

Bin 165: 48 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 465
Size: 4360 Color: 356
Size: 216 Color: 13

Bin 166: 54 of cap free
Amount of items: 4
Items: 
Size: 10071 Color: 440
Size: 5055 Color: 372
Size: 316 Color: 35
Size: 312 Color: 34

Bin 167: 58 of cap free
Amount of items: 2
Items: 
Size: 9886 Color: 436
Size: 5864 Color: 387

Bin 168: 58 of cap free
Amount of items: 3
Items: 
Size: 11274 Color: 469
Size: 4300 Color: 352
Size: 176 Color: 6

Bin 169: 60 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 467
Size: 4294 Color: 350
Size: 192 Color: 9

Bin 170: 60 of cap free
Amount of items: 3
Items: 
Size: 11268 Color: 468
Size: 4296 Color: 351
Size: 184 Color: 8

Bin 171: 63 of cap free
Amount of items: 3
Items: 
Size: 11025 Color: 462
Size: 4464 Color: 358
Size: 256 Color: 16

Bin 172: 64 of cap free
Amount of items: 3
Items: 
Size: 10096 Color: 441
Size: 5344 Color: 376
Size: 304 Color: 33

Bin 173: 65 of cap free
Amount of items: 2
Items: 
Size: 11155 Color: 463
Size: 4588 Color: 360

Bin 174: 66 of cap free
Amount of items: 3
Items: 
Size: 10472 Color: 443
Size: 2772 Color: 299
Size: 2498 Color: 284

Bin 175: 73 of cap free
Amount of items: 3
Items: 
Size: 8809 Color: 417
Size: 6582 Color: 397
Size: 344 Color: 53

Bin 176: 82 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 424
Size: 6400 Color: 392
Size: 340 Color: 50

Bin 177: 90 of cap free
Amount of items: 3
Items: 
Size: 8982 Color: 423
Size: 6396 Color: 391
Size: 340 Color: 51

Bin 178: 96 of cap free
Amount of items: 4
Items: 
Size: 8432 Color: 415
Size: 6576 Color: 395
Size: 352 Color: 56
Size: 352 Color: 55

Bin 179: 97 of cap free
Amount of items: 3
Items: 
Size: 9935 Color: 438
Size: 5456 Color: 378
Size: 320 Color: 37

Bin 180: 98 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 464
Size: 4294 Color: 349
Size: 256 Color: 15

Bin 181: 99 of cap free
Amount of items: 2
Items: 
Size: 9292 Color: 427
Size: 6417 Color: 393

Bin 182: 108 of cap free
Amount of items: 3
Items: 
Size: 8776 Color: 416
Size: 6580 Color: 396
Size: 344 Color: 54

Bin 183: 110 of cap free
Amount of items: 4
Items: 
Size: 9352 Color: 429
Size: 5686 Color: 382
Size: 332 Color: 46
Size: 328 Color: 44

Bin 184: 112 of cap free
Amount of items: 3
Items: 
Size: 11388 Color: 473
Size: 4180 Color: 346
Size: 128 Color: 1

Bin 185: 120 of cap free
Amount of items: 3
Items: 
Size: 10324 Color: 442
Size: 5072 Color: 373
Size: 292 Color: 30

Bin 186: 127 of cap free
Amount of items: 3
Items: 
Size: 9580 Color: 431
Size: 5781 Color: 385
Size: 320 Color: 42

Bin 187: 132 of cap free
Amount of items: 3
Items: 
Size: 8008 Color: 413
Size: 7304 Color: 403
Size: 364 Color: 60

Bin 188: 132 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 414
Size: 6568 Color: 394
Size: 364 Color: 59
Size: 352 Color: 57

Bin 189: 143 of cap free
Amount of items: 2
Items: 
Size: 9832 Color: 433
Size: 5833 Color: 386

Bin 190: 164 of cap free
Amount of items: 3
Items: 
Size: 9888 Color: 437
Size: 5436 Color: 377
Size: 320 Color: 38

Bin 191: 172 of cap free
Amount of items: 2
Items: 
Size: 8940 Color: 422
Size: 6696 Color: 402

Bin 192: 176 of cap free
Amount of items: 11
Items: 
Size: 7906 Color: 405
Size: 988 Color: 175
Size: 978 Color: 173
Size: 960 Color: 172
Size: 956 Color: 171
Size: 872 Color: 169
Size: 864 Color: 168
Size: 860 Color: 165
Size: 416 Color: 78
Size: 416 Color: 77
Size: 416 Color: 76

Bin 193: 238 of cap free
Amount of items: 25
Items: 
Size: 832 Color: 158
Size: 800 Color: 155
Size: 796 Color: 154
Size: 774 Color: 153
Size: 768 Color: 152
Size: 768 Color: 151
Size: 746 Color: 147
Size: 746 Color: 146
Size: 696 Color: 142
Size: 688 Color: 140
Size: 672 Color: 138
Size: 660 Color: 136
Size: 656 Color: 135
Size: 656 Color: 134
Size: 496 Color: 103
Size: 496 Color: 102
Size: 496 Color: 101
Size: 496 Color: 100
Size: 496 Color: 99
Size: 488 Color: 97
Size: 488 Color: 96
Size: 488 Color: 95
Size: 464 Color: 92
Size: 456 Color: 89
Size: 448 Color: 88

Bin 194: 238 of cap free
Amount of items: 3
Items: 
Size: 9552 Color: 430
Size: 5690 Color: 383
Size: 328 Color: 43

Bin 195: 296 of cap free
Amount of items: 2
Items: 
Size: 8924 Color: 421
Size: 6588 Color: 401

Bin 196: 305 of cap free
Amount of items: 2
Items: 
Size: 8916 Color: 420
Size: 6587 Color: 400

Bin 197: 325 of cap free
Amount of items: 2
Items: 
Size: 8897 Color: 419
Size: 6586 Color: 399

Bin 198: 358 of cap free
Amount of items: 5
Items: 
Size: 7916 Color: 410
Size: 1958 Color: 255
Size: 1922 Color: 253
Size: 1832 Color: 247
Size: 1822 Color: 245

Bin 199: 9824 of cap free
Amount of items: 11
Items: 
Size: 608 Color: 126
Size: 576 Color: 121
Size: 544 Color: 119
Size: 544 Color: 118
Size: 544 Color: 117
Size: 544 Color: 116
Size: 544 Color: 113
Size: 536 Color: 112
Size: 526 Color: 109
Size: 512 Color: 108
Size: 506 Color: 105

Total size: 3129984
Total free space: 15808

