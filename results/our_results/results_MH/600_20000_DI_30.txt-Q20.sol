Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8152 Color: 16
Size: 2605 Color: 15
Size: 2497 Color: 13
Size: 2490 Color: 13
Size: 544 Color: 3

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8164 Color: 2
Size: 2734 Color: 2
Size: 2617 Color: 12
Size: 2051 Color: 14
Size: 722 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 11
Size: 5764 Color: 12
Size: 772 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 9
Size: 6046 Color: 19
Size: 476 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10520 Color: 9
Size: 5640 Color: 0
Size: 128 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11069 Color: 5
Size: 4351 Color: 17
Size: 868 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11346 Color: 15
Size: 4570 Color: 7
Size: 372 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11442 Color: 13
Size: 4146 Color: 0
Size: 700 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11914 Color: 18
Size: 4036 Color: 9
Size: 338 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 3
Size: 2411 Color: 16
Size: 1443 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 15
Size: 2916 Color: 5
Size: 880 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12520 Color: 19
Size: 3432 Color: 12
Size: 336 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12730 Color: 8
Size: 2414 Color: 0
Size: 1144 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12797 Color: 14
Size: 2911 Color: 8
Size: 580 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 14
Size: 2224 Color: 14
Size: 1160 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 9
Size: 2396 Color: 14
Size: 882 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 5
Size: 2568 Color: 4
Size: 704 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13040 Color: 10
Size: 2888 Color: 7
Size: 360 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 19
Size: 1964 Color: 4
Size: 1200 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 17
Size: 2824 Color: 6
Size: 324 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 18
Size: 2644 Color: 3
Size: 408 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13360 Color: 18
Size: 2564 Color: 15
Size: 364 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 12
Size: 1820 Color: 7
Size: 1084 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 15
Size: 2086 Color: 9
Size: 686 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 9
Size: 1965 Color: 4
Size: 696 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 5
Size: 2201 Color: 14
Size: 440 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13654 Color: 18
Size: 1940 Color: 2
Size: 694 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 6
Size: 2002 Color: 14
Size: 544 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 2
Size: 2122 Color: 0
Size: 480 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 1
Size: 2060 Color: 11
Size: 496 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 16
Size: 2076 Color: 6
Size: 464 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13829 Color: 7
Size: 2035 Color: 13
Size: 424 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 9
Size: 2044 Color: 19
Size: 408 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13890 Color: 9
Size: 1624 Color: 8
Size: 774 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 11
Size: 1404 Color: 18
Size: 984 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 6
Size: 1532 Color: 7
Size: 840 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13960 Color: 11
Size: 1464 Color: 18
Size: 864 Color: 17

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 11
Size: 1796 Color: 3
Size: 528 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 1
Size: 1498 Color: 0
Size: 804 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14083 Color: 14
Size: 1549 Color: 12
Size: 656 Color: 5

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 12
Size: 1352 Color: 18
Size: 902 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14082 Color: 5
Size: 1986 Color: 13
Size: 220 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 13
Size: 1160 Color: 13
Size: 1024 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 14
Size: 1974 Color: 8
Size: 120 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 0
Size: 1414 Color: 7
Size: 760 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 10
Size: 1804 Color: 9
Size: 360 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 3
Size: 1692 Color: 2
Size: 456 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 14
Size: 1356 Color: 15
Size: 672 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14165 Color: 13
Size: 1491 Color: 7
Size: 632 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14234 Color: 11
Size: 1612 Color: 18
Size: 442 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 8
Size: 1524 Color: 12
Size: 516 Color: 14

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14287 Color: 10
Size: 1669 Color: 4
Size: 332 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 6
Size: 1606 Color: 18
Size: 360 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 7
Size: 1144 Color: 15
Size: 800 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 3
Size: 1208 Color: 13
Size: 704 Color: 5

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 14
Size: 1544 Color: 1
Size: 304 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 16
Size: 1440 Color: 11
Size: 396 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14460 Color: 8
Size: 1280 Color: 5
Size: 548 Color: 17

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 16
Size: 1372 Color: 9
Size: 384 Color: 7

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 5
Size: 1160 Color: 16
Size: 592 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 6
Size: 1144 Color: 15
Size: 532 Color: 11

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14620 Color: 12
Size: 1364 Color: 13
Size: 304 Color: 11

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 13
Size: 1628 Color: 3
Size: 8 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 16
Size: 4948 Color: 14
Size: 302 Color: 10

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 10
Size: 3879 Color: 12
Size: 496 Color: 17

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 5
Size: 3866 Color: 13
Size: 304 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12334 Color: 14
Size: 3441 Color: 7
Size: 512 Color: 9

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 11
Size: 2219 Color: 1
Size: 784 Color: 18

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 10
Size: 2278 Color: 6
Size: 612 Color: 5

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13551 Color: 4
Size: 1704 Color: 6
Size: 1032 Color: 5

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 14592 Color: 5
Size: 1695 Color: 2

Bin 72: 2 of cap free
Amount of items: 7
Items: 
Size: 8148 Color: 0
Size: 2066 Color: 4
Size: 1826 Color: 3
Size: 1814 Color: 16
Size: 1604 Color: 6
Size: 504 Color: 5
Size: 324 Color: 1

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 9030 Color: 0
Size: 6764 Color: 15
Size: 492 Color: 16

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9798 Color: 7
Size: 6000 Color: 2
Size: 488 Color: 6

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 10865 Color: 19
Size: 4413 Color: 13
Size: 1008 Color: 4

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 11682 Color: 3
Size: 4604 Color: 5

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 9
Size: 3880 Color: 18
Size: 296 Color: 15

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13114 Color: 1
Size: 2548 Color: 18
Size: 624 Color: 9

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13346 Color: 14
Size: 2940 Color: 12

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 13558 Color: 9
Size: 2728 Color: 4

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 14
Size: 2482 Color: 13

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14162 Color: 6
Size: 2124 Color: 3

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 14364 Color: 15
Size: 1922 Color: 4

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 2
Size: 1396 Color: 14
Size: 264 Color: 16

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 14644 Color: 12
Size: 1642 Color: 5

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 9209 Color: 7
Size: 6772 Color: 13
Size: 304 Color: 4

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 10095 Color: 15
Size: 6050 Color: 16
Size: 140 Color: 16

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 10835 Color: 16
Size: 3138 Color: 18
Size: 2312 Color: 11

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 10993 Color: 8
Size: 5292 Color: 15

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 11436 Color: 13
Size: 4529 Color: 2
Size: 320 Color: 13

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 12004 Color: 1
Size: 3469 Color: 4
Size: 812 Color: 0

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 14469 Color: 17
Size: 1416 Color: 14
Size: 400 Color: 9

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 5
Size: 1152 Color: 17
Size: 576 Color: 14

Bin 94: 4 of cap free
Amount of items: 5
Items: 
Size: 8376 Color: 1
Size: 5828 Color: 3
Size: 1344 Color: 5
Size: 464 Color: 17
Size: 272 Color: 6

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 9
Size: 5832 Color: 16
Size: 1136 Color: 2

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 10
Size: 5732 Color: 9
Size: 480 Color: 1

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 0
Size: 2628 Color: 4

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 12
Size: 1714 Color: 5
Size: 76 Color: 4

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 3
Size: 1684 Color: 5

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 11458 Color: 1
Size: 4521 Color: 9
Size: 304 Color: 16

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 12161 Color: 9
Size: 4122 Color: 3

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 13163 Color: 11
Size: 3120 Color: 9

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 10438 Color: 0
Size: 5844 Color: 8

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 9
Size: 4166 Color: 13
Size: 96 Color: 6

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13866 Color: 18
Size: 2416 Color: 2

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 14
Size: 2402 Color: 15

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 12476 Color: 8
Size: 3805 Color: 3

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 12745 Color: 2
Size: 3144 Color: 3
Size: 392 Color: 1

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13847 Color: 14
Size: 2434 Color: 0

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 14449 Color: 15
Size: 1832 Color: 3

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 10806 Color: 11
Size: 5474 Color: 3

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 11410 Color: 6
Size: 4502 Color: 5
Size: 368 Color: 16

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 11635 Color: 3
Size: 3467 Color: 2
Size: 1178 Color: 9

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 5
Size: 4044 Color: 1
Size: 596 Color: 14

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 17
Size: 4202 Color: 3

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 12634 Color: 14
Size: 3646 Color: 15

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13314 Color: 0
Size: 2966 Color: 3

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 14279 Color: 7
Size: 2000 Color: 11

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 11314 Color: 2
Size: 4964 Color: 12

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 12796 Color: 7
Size: 3482 Color: 14

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 12994 Color: 14
Size: 3284 Color: 12

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 14108 Color: 19
Size: 2170 Color: 7

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 12979 Color: 16
Size: 3298 Color: 18

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 14255 Color: 10
Size: 2022 Color: 8

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 8180 Color: 9
Size: 8096 Color: 15

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 0
Size: 2344 Color: 11

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 14431 Color: 1
Size: 1845 Color: 10

Bin 128: 14 of cap free
Amount of items: 3
Items: 
Size: 10909 Color: 16
Size: 5131 Color: 18
Size: 234 Color: 7

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 12764 Color: 10
Size: 3510 Color: 2

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 13820 Color: 13
Size: 2454 Color: 10

Bin 131: 15 of cap free
Amount of items: 3
Items: 
Size: 9253 Color: 7
Size: 5812 Color: 18
Size: 1208 Color: 11

Bin 132: 15 of cap free
Amount of items: 4
Items: 
Size: 9276 Color: 8
Size: 5901 Color: 15
Size: 840 Color: 0
Size: 256 Color: 0

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 9
Size: 3411 Color: 2
Size: 276 Color: 1

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 5
Size: 3048 Color: 9

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 14430 Color: 16
Size: 1842 Color: 10

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 14501 Color: 8
Size: 1771 Color: 5

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 7
Size: 3572 Color: 11

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 13596 Color: 8
Size: 2674 Color: 1

Bin 139: 18 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 5
Size: 1882 Color: 4
Size: 32 Color: 11

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 10406 Color: 12
Size: 5863 Color: 3

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 13055 Color: 0
Size: 3214 Color: 6

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 14594 Color: 2
Size: 1675 Color: 1

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 11723 Color: 7
Size: 4545 Color: 17

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 11
Size: 2244 Color: 19

Bin 145: 21 of cap free
Amount of items: 3
Items: 
Size: 10378 Color: 2
Size: 5161 Color: 16
Size: 728 Color: 6

Bin 146: 22 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 5
Size: 5410 Color: 15
Size: 352 Color: 1

Bin 147: 22 of cap free
Amount of items: 2
Items: 
Size: 11256 Color: 7
Size: 5010 Color: 10

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 13410 Color: 12
Size: 2856 Color: 18

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 13931 Color: 15
Size: 2334 Color: 3

Bin 150: 24 of cap free
Amount of items: 21
Items: 
Size: 1080 Color: 16
Size: 984 Color: 19
Size: 984 Color: 13
Size: 920 Color: 7
Size: 912 Color: 16
Size: 912 Color: 12
Size: 832 Color: 3
Size: 828 Color: 9
Size: 808 Color: 11
Size: 800 Color: 5
Size: 768 Color: 14
Size: 720 Color: 17
Size: 712 Color: 11
Size: 680 Color: 7
Size: 664 Color: 13
Size: 644 Color: 11
Size: 640 Color: 18
Size: 624 Color: 14
Size: 592 Color: 0
Size: 584 Color: 11
Size: 576 Color: 1

Bin 151: 24 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 19
Size: 5748 Color: 2
Size: 160 Color: 17

Bin 152: 24 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 17
Size: 1996 Color: 9

Bin 153: 25 of cap free
Amount of items: 2
Items: 
Size: 12197 Color: 11
Size: 4066 Color: 8

Bin 154: 25 of cap free
Amount of items: 3
Items: 
Size: 12607 Color: 13
Size: 3560 Color: 6
Size: 96 Color: 6

Bin 155: 26 of cap free
Amount of items: 4
Items: 
Size: 8154 Color: 6
Size: 3433 Color: 11
Size: 2695 Color: 5
Size: 1980 Color: 14

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 11650 Color: 18
Size: 4612 Color: 17

Bin 157: 26 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 13
Size: 3180 Color: 14

Bin 158: 28 of cap free
Amount of items: 2
Items: 
Size: 11452 Color: 13
Size: 4808 Color: 19

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 13490 Color: 10
Size: 2770 Color: 19

Bin 160: 28 of cap free
Amount of items: 2
Items: 
Size: 13752 Color: 15
Size: 2508 Color: 8

Bin 161: 30 of cap free
Amount of items: 2
Items: 
Size: 13810 Color: 16
Size: 2448 Color: 1

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 14362 Color: 1
Size: 1896 Color: 6

Bin 163: 31 of cap free
Amount of items: 2
Items: 
Size: 12780 Color: 7
Size: 3477 Color: 17

Bin 164: 35 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 1
Size: 5055 Color: 3
Size: 308 Color: 2

Bin 165: 36 of cap free
Amount of items: 3
Items: 
Size: 9396 Color: 12
Size: 6600 Color: 5
Size: 256 Color: 0

Bin 166: 36 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 6
Size: 2424 Color: 15
Size: 908 Color: 1

Bin 167: 40 of cap free
Amount of items: 7
Items: 
Size: 8146 Color: 5
Size: 1533 Color: 9
Size: 1517 Color: 0
Size: 1356 Color: 6
Size: 1356 Color: 4
Size: 1172 Color: 6
Size: 1168 Color: 19

Bin 168: 42 of cap free
Amount of items: 2
Items: 
Size: 11246 Color: 11
Size: 5000 Color: 0

Bin 169: 42 of cap free
Amount of items: 2
Items: 
Size: 13487 Color: 2
Size: 2759 Color: 8

Bin 170: 43 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 18
Size: 3069 Color: 13

Bin 171: 44 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 17
Size: 4328 Color: 6
Size: 820 Color: 1

Bin 172: 48 of cap free
Amount of items: 3
Items: 
Size: 10372 Color: 13
Size: 5708 Color: 1
Size: 160 Color: 17

Bin 173: 50 of cap free
Amount of items: 2
Items: 
Size: 13922 Color: 3
Size: 2316 Color: 7

Bin 174: 52 of cap free
Amount of items: 2
Items: 
Size: 9444 Color: 14
Size: 6792 Color: 10

Bin 175: 52 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 5
Size: 2646 Color: 0

Bin 176: 54 of cap free
Amount of items: 3
Items: 
Size: 12402 Color: 4
Size: 3656 Color: 1
Size: 176 Color: 17

Bin 177: 58 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 6
Size: 4046 Color: 2

Bin 178: 59 of cap free
Amount of items: 2
Items: 
Size: 10133 Color: 2
Size: 6096 Color: 14

Bin 179: 61 of cap free
Amount of items: 5
Items: 
Size: 8147 Color: 10
Size: 2200 Color: 3
Size: 2152 Color: 5
Size: 2120 Color: 14
Size: 1608 Color: 19

Bin 180: 62 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 7
Size: 4926 Color: 0
Size: 960 Color: 6

Bin 181: 62 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 6
Size: 2924 Color: 14

Bin 182: 62 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 12
Size: 2746 Color: 3

Bin 183: 64 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 13
Size: 4200 Color: 2

Bin 184: 66 of cap free
Amount of items: 3
Items: 
Size: 8992 Color: 16
Size: 5844 Color: 1
Size: 1386 Color: 10

Bin 185: 68 of cap free
Amount of items: 2
Items: 
Size: 10772 Color: 4
Size: 5448 Color: 11

Bin 186: 74 of cap free
Amount of items: 2
Items: 
Size: 13220 Color: 1
Size: 2994 Color: 7

Bin 187: 76 of cap free
Amount of items: 2
Items: 
Size: 12648 Color: 5
Size: 3564 Color: 7

Bin 188: 77 of cap free
Amount of items: 2
Items: 
Size: 12169 Color: 6
Size: 4042 Color: 10

Bin 189: 80 of cap free
Amount of items: 2
Items: 
Size: 12966 Color: 2
Size: 3242 Color: 16

Bin 190: 86 of cap free
Amount of items: 2
Items: 
Size: 9034 Color: 18
Size: 7168 Color: 7

Bin 191: 88 of cap free
Amount of items: 2
Items: 
Size: 9412 Color: 5
Size: 6788 Color: 9

Bin 192: 94 of cap free
Amount of items: 2
Items: 
Size: 10756 Color: 17
Size: 5438 Color: 14

Bin 193: 113 of cap free
Amount of items: 8
Items: 
Size: 8145 Color: 2
Size: 1352 Color: 2
Size: 1352 Color: 0
Size: 1344 Color: 17
Size: 1312 Color: 4
Size: 1088 Color: 17
Size: 896 Color: 9
Size: 686 Color: 1

Bin 194: 123 of cap free
Amount of items: 2
Items: 
Size: 9380 Color: 4
Size: 6785 Color: 6

Bin 195: 162 of cap free
Amount of items: 37
Items: 
Size: 584 Color: 5
Size: 560 Color: 17
Size: 560 Color: 15
Size: 552 Color: 4
Size: 550 Color: 10
Size: 538 Color: 2
Size: 520 Color: 5
Size: 520 Color: 4
Size: 520 Color: 1
Size: 504 Color: 12
Size: 496 Color: 13
Size: 480 Color: 7
Size: 480 Color: 7
Size: 452 Color: 3
Size: 448 Color: 16
Size: 432 Color: 11
Size: 432 Color: 9
Size: 416 Color: 10
Size: 416 Color: 2
Size: 412 Color: 2
Size: 408 Color: 10
Size: 408 Color: 2
Size: 408 Color: 0
Size: 406 Color: 18
Size: 392 Color: 14
Size: 392 Color: 6
Size: 392 Color: 6
Size: 392 Color: 0
Size: 380 Color: 6
Size: 360 Color: 6
Size: 352 Color: 4
Size: 352 Color: 1
Size: 340 Color: 17
Size: 336 Color: 9
Size: 334 Color: 9
Size: 306 Color: 1
Size: 296 Color: 15

Bin 196: 197 of cap free
Amount of items: 2
Items: 
Size: 9304 Color: 11
Size: 6787 Color: 14

Bin 197: 202 of cap free
Amount of items: 2
Items: 
Size: 9300 Color: 6
Size: 6786 Color: 4

Bin 198: 222 of cap free
Amount of items: 2
Items: 
Size: 9284 Color: 1
Size: 6782 Color: 14

Bin 199: 12472 of cap free
Amount of items: 13
Items: 
Size: 336 Color: 11
Size: 320 Color: 18
Size: 320 Color: 4
Size: 320 Color: 0
Size: 304 Color: 17
Size: 288 Color: 16
Size: 288 Color: 15
Size: 280 Color: 8
Size: 272 Color: 16
Size: 272 Color: 15
Size: 272 Color: 9
Size: 272 Color: 5
Size: 272 Color: 1

Total size: 3225024
Total free space: 16288

