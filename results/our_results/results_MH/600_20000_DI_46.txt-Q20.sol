Capicity Bin: 13904
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 6974 Color: 3
Size: 1152 Color: 18
Size: 1152 Color: 15
Size: 1152 Color: 0
Size: 1014 Color: 19
Size: 1012 Color: 12
Size: 976 Color: 8
Size: 304 Color: 9
Size: 168 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 16
Size: 6180 Color: 11
Size: 564 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7320 Color: 10
Size: 6144 Color: 18
Size: 440 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7771 Color: 3
Size: 5111 Color: 1
Size: 1022 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7843 Color: 2
Size: 5791 Color: 7
Size: 270 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7846 Color: 18
Size: 5782 Color: 9
Size: 276 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7992 Color: 11
Size: 5624 Color: 11
Size: 288 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 5
Size: 4996 Color: 18
Size: 824 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 3
Size: 4748 Color: 16
Size: 944 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 9
Size: 5496 Color: 18
Size: 240 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 13
Size: 4792 Color: 4
Size: 592 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 6
Size: 5078 Color: 4
Size: 284 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8579 Color: 1
Size: 5031 Color: 5
Size: 294 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8667 Color: 9
Size: 4759 Color: 0
Size: 478 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9556 Color: 15
Size: 3628 Color: 4
Size: 720 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 9
Size: 3892 Color: 7
Size: 416 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 7
Size: 3816 Color: 11
Size: 416 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9702 Color: 16
Size: 3818 Color: 11
Size: 384 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 9820 Color: 11
Size: 3730 Color: 1
Size: 354 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 11
Size: 3704 Color: 0
Size: 352 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9871 Color: 11
Size: 3361 Color: 16
Size: 672 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 18
Size: 3276 Color: 17
Size: 396 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 10269 Color: 9
Size: 2333 Color: 15
Size: 1302 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10302 Color: 17
Size: 3290 Color: 17
Size: 312 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 1
Size: 3364 Color: 13
Size: 232 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10348 Color: 11
Size: 3256 Color: 9
Size: 300 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10410 Color: 12
Size: 3122 Color: 12
Size: 372 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 10600 Color: 8
Size: 3064 Color: 2
Size: 240 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 16
Size: 2989 Color: 10
Size: 206 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 2
Size: 2892 Color: 16
Size: 264 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 10814 Color: 19
Size: 2624 Color: 0
Size: 466 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 10904 Color: 8
Size: 2394 Color: 6
Size: 606 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11025 Color: 5
Size: 1791 Color: 10
Size: 1088 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 15
Size: 2622 Color: 6
Size: 248 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11076 Color: 6
Size: 2578 Color: 7
Size: 250 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 12
Size: 2360 Color: 7
Size: 464 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11256 Color: 15
Size: 2032 Color: 12
Size: 616 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11348 Color: 3
Size: 2324 Color: 2
Size: 232 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11454 Color: 9
Size: 1262 Color: 4
Size: 1188 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11461 Color: 7
Size: 1287 Color: 12
Size: 1156 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 0
Size: 2040 Color: 13
Size: 400 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 11
Size: 2026 Color: 13
Size: 348 Color: 14

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 5
Size: 1384 Color: 19
Size: 944 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11623 Color: 0
Size: 2037 Color: 1
Size: 244 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 11674 Color: 9
Size: 1430 Color: 16
Size: 800 Color: 14

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 18
Size: 1176 Color: 2
Size: 1008 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 11730 Color: 2
Size: 1582 Color: 6
Size: 592 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11757 Color: 0
Size: 1677 Color: 5
Size: 470 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 11773 Color: 6
Size: 1445 Color: 17
Size: 686 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 11
Size: 1156 Color: 14
Size: 896 Color: 15

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 11894 Color: 9
Size: 1422 Color: 16
Size: 588 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 4
Size: 992 Color: 19
Size: 968 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 11950 Color: 15
Size: 1378 Color: 14
Size: 576 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 11990 Color: 0
Size: 1522 Color: 12
Size: 392 Color: 11

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12025 Color: 2
Size: 1601 Color: 18
Size: 278 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 16
Size: 1396 Color: 9
Size: 464 Color: 8

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12084 Color: 17
Size: 1292 Color: 18
Size: 528 Color: 16

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12147 Color: 9
Size: 1353 Color: 0
Size: 404 Color: 7

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 0
Size: 1320 Color: 12
Size: 432 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12189 Color: 9
Size: 1431 Color: 15
Size: 284 Color: 6

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 16
Size: 952 Color: 15
Size: 752 Color: 19

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12228 Color: 18
Size: 1008 Color: 12
Size: 668 Color: 9

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 12238 Color: 14
Size: 1332 Color: 13
Size: 334 Color: 19

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 12281 Color: 9
Size: 1255 Color: 8
Size: 368 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 2
Size: 1120 Color: 8
Size: 476 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 9
Size: 1272 Color: 10
Size: 276 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 12334 Color: 12
Size: 1278 Color: 17
Size: 292 Color: 7

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 9
Size: 1214 Color: 11
Size: 292 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 12381 Color: 17
Size: 1271 Color: 3
Size: 252 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 12407 Color: 9
Size: 1249 Color: 17
Size: 248 Color: 18

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 12421 Color: 2
Size: 1223 Color: 13
Size: 260 Color: 19

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 12454 Color: 9
Size: 1204 Color: 12
Size: 246 Color: 7

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 12484 Color: 1
Size: 1004 Color: 19
Size: 416 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 12502 Color: 13
Size: 1162 Color: 16
Size: 240 Color: 9

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 7814 Color: 10
Size: 5793 Color: 10
Size: 296 Color: 11

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 7867 Color: 6
Size: 5784 Color: 9
Size: 252 Color: 18

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 8539 Color: 19
Size: 4788 Color: 7
Size: 576 Color: 10

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 8606 Color: 9
Size: 5025 Color: 10
Size: 272 Color: 12

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 2
Size: 3903 Color: 11
Size: 424 Color: 19

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 19
Size: 3085 Color: 7
Size: 1237 Color: 10

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 9787 Color: 10
Size: 4116 Color: 15

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 9894 Color: 9
Size: 3603 Color: 13
Size: 406 Color: 15

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 10203 Color: 0
Size: 3342 Color: 15
Size: 358 Color: 19

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 8
Size: 3031 Color: 18
Size: 544 Color: 1

Bin 85: 1 of cap free
Amount of items: 2
Items: 
Size: 10519 Color: 17
Size: 3384 Color: 14

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 11088 Color: 9
Size: 2455 Color: 18
Size: 360 Color: 1

Bin 87: 1 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 14
Size: 2357 Color: 9
Size: 284 Color: 12

Bin 88: 1 of cap free
Amount of items: 3
Items: 
Size: 11351 Color: 8
Size: 1982 Color: 19
Size: 570 Color: 11

Bin 89: 1 of cap free
Amount of items: 3
Items: 
Size: 11998 Color: 12
Size: 1809 Color: 9
Size: 96 Color: 3

Bin 90: 1 of cap free
Amount of items: 2
Items: 
Size: 12374 Color: 11
Size: 1529 Color: 16

Bin 91: 2 of cap free
Amount of items: 36
Items: 
Size: 580 Color: 13
Size: 526 Color: 7
Size: 520 Color: 5
Size: 512 Color: 4
Size: 512 Color: 2
Size: 504 Color: 11
Size: 496 Color: 18
Size: 472 Color: 16
Size: 464 Color: 1
Size: 444 Color: 19
Size: 432 Color: 9
Size: 428 Color: 16
Size: 424 Color: 8
Size: 408 Color: 17
Size: 400 Color: 12
Size: 380 Color: 11
Size: 376 Color: 15
Size: 368 Color: 3
Size: 356 Color: 8
Size: 356 Color: 3
Size: 352 Color: 15
Size: 336 Color: 15
Size: 332 Color: 5
Size: 328 Color: 12
Size: 324 Color: 14
Size: 324 Color: 10
Size: 320 Color: 13
Size: 320 Color: 9
Size: 320 Color: 7
Size: 316 Color: 6
Size: 304 Color: 14
Size: 288 Color: 14
Size: 272 Color: 14
Size: 272 Color: 6
Size: 272 Color: 6
Size: 264 Color: 13

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 7916 Color: 3
Size: 5778 Color: 7
Size: 208 Color: 17

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 8824 Color: 0
Size: 4470 Color: 12
Size: 608 Color: 9

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 9064 Color: 13
Size: 4494 Color: 2
Size: 344 Color: 11

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 9199 Color: 10
Size: 2906 Color: 7
Size: 1797 Color: 17

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 10436 Color: 9
Size: 2946 Color: 4
Size: 520 Color: 1

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 19
Size: 2894 Color: 17
Size: 312 Color: 0

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 10743 Color: 19
Size: 2855 Color: 14
Size: 304 Color: 7

Bin 99: 2 of cap free
Amount of items: 3
Items: 
Size: 10871 Color: 9
Size: 2635 Color: 16
Size: 396 Color: 17

Bin 100: 2 of cap free
Amount of items: 3
Items: 
Size: 10762 Color: 1
Size: 2964 Color: 17
Size: 176 Color: 13

Bin 101: 2 of cap free
Amount of items: 3
Items: 
Size: 11066 Color: 3
Size: 1578 Color: 5
Size: 1258 Color: 9

Bin 102: 2 of cap free
Amount of items: 3
Items: 
Size: 11229 Color: 15
Size: 1777 Color: 6
Size: 896 Color: 6

Bin 103: 2 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 19
Size: 1310 Color: 9
Size: 1120 Color: 16

Bin 104: 2 of cap free
Amount of items: 2
Items: 
Size: 12006 Color: 15
Size: 1896 Color: 4

Bin 105: 2 of cap free
Amount of items: 2
Items: 
Size: 12134 Color: 11
Size: 1768 Color: 5

Bin 106: 2 of cap free
Amount of items: 2
Items: 
Size: 12315 Color: 6
Size: 1587 Color: 0

Bin 107: 2 of cap free
Amount of items: 2
Items: 
Size: 12437 Color: 17
Size: 1465 Color: 10

Bin 108: 3 of cap free
Amount of items: 7
Items: 
Size: 6956 Color: 0
Size: 1908 Color: 9
Size: 1743 Color: 3
Size: 1404 Color: 17
Size: 1382 Color: 16
Size: 256 Color: 17
Size: 252 Color: 9

Bin 109: 3 of cap free
Amount of items: 5
Items: 
Size: 6957 Color: 10
Size: 3350 Color: 0
Size: 3018 Color: 10
Size: 320 Color: 16
Size: 256 Color: 19

Bin 110: 3 of cap free
Amount of items: 3
Items: 
Size: 9125 Color: 3
Size: 4418 Color: 10
Size: 358 Color: 2

Bin 111: 3 of cap free
Amount of items: 3
Items: 
Size: 9238 Color: 1
Size: 4439 Color: 11
Size: 224 Color: 18

Bin 112: 3 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 6
Size: 2636 Color: 15
Size: 160 Color: 9

Bin 113: 3 of cap free
Amount of items: 2
Items: 
Size: 12069 Color: 19
Size: 1832 Color: 7

Bin 114: 3 of cap free
Amount of items: 2
Items: 
Size: 12139 Color: 13
Size: 1762 Color: 3

Bin 115: 3 of cap free
Amount of items: 2
Items: 
Size: 12504 Color: 15
Size: 1397 Color: 18

Bin 116: 4 of cap free
Amount of items: 2
Items: 
Size: 9958 Color: 5
Size: 3942 Color: 0

Bin 117: 4 of cap free
Amount of items: 3
Items: 
Size: 10796 Color: 9
Size: 2984 Color: 5
Size: 120 Color: 7

Bin 118: 4 of cap free
Amount of items: 2
Items: 
Size: 11396 Color: 0
Size: 2504 Color: 7

Bin 119: 4 of cap free
Amount of items: 2
Items: 
Size: 11790 Color: 0
Size: 2110 Color: 16

Bin 120: 4 of cap free
Amount of items: 2
Items: 
Size: 11956 Color: 1
Size: 1944 Color: 19

Bin 121: 4 of cap free
Amount of items: 2
Items: 
Size: 12468 Color: 11
Size: 1432 Color: 13

Bin 122: 4 of cap free
Amount of items: 2
Items: 
Size: 12510 Color: 5
Size: 1390 Color: 13

Bin 123: 5 of cap free
Amount of items: 9
Items: 
Size: 6953 Color: 12
Size: 1010 Color: 13
Size: 1006 Color: 4
Size: 892 Color: 17
Size: 886 Color: 10
Size: 880 Color: 4
Size: 872 Color: 2
Size: 720 Color: 6
Size: 680 Color: 9

Bin 124: 5 of cap free
Amount of items: 3
Items: 
Size: 8164 Color: 0
Size: 5079 Color: 5
Size: 656 Color: 10

Bin 125: 5 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 3
Size: 4365 Color: 9
Size: 360 Color: 19

Bin 126: 5 of cap free
Amount of items: 2
Items: 
Size: 11668 Color: 18
Size: 2231 Color: 15

Bin 127: 5 of cap free
Amount of items: 2
Items: 
Size: 12309 Color: 4
Size: 1590 Color: 12

Bin 128: 6 of cap free
Amount of items: 20
Items: 
Size: 832 Color: 8
Size: 816 Color: 12
Size: 816 Color: 8
Size: 796 Color: 3
Size: 788 Color: 16
Size: 784 Color: 2
Size: 780 Color: 1
Size: 776 Color: 9
Size: 720 Color: 13
Size: 698 Color: 9
Size: 692 Color: 4
Size: 676 Color: 0
Size: 672 Color: 11
Size: 672 Color: 8
Size: 672 Color: 3
Size: 668 Color: 10
Size: 640 Color: 15
Size: 604 Color: 11
Size: 476 Color: 7
Size: 320 Color: 6

Bin 129: 6 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 11
Size: 4488 Color: 3
Size: 896 Color: 13

Bin 130: 6 of cap free
Amount of items: 3
Items: 
Size: 8574 Color: 19
Size: 5136 Color: 3
Size: 188 Color: 4

Bin 131: 6 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 12
Size: 4034 Color: 9
Size: 528 Color: 16

Bin 132: 6 of cap free
Amount of items: 2
Items: 
Size: 10008 Color: 7
Size: 3890 Color: 13

Bin 133: 6 of cap free
Amount of items: 2
Items: 
Size: 11077 Color: 15
Size: 2821 Color: 10

Bin 134: 7 of cap free
Amount of items: 2
Items: 
Size: 12254 Color: 18
Size: 1643 Color: 2

Bin 135: 8 of cap free
Amount of items: 2
Items: 
Size: 10418 Color: 15
Size: 3478 Color: 4

Bin 136: 8 of cap free
Amount of items: 2
Items: 
Size: 11804 Color: 2
Size: 2092 Color: 0

Bin 137: 9 of cap free
Amount of items: 3
Items: 
Size: 7811 Color: 14
Size: 5796 Color: 12
Size: 288 Color: 3

Bin 138: 9 of cap free
Amount of items: 2
Items: 
Size: 11893 Color: 16
Size: 2002 Color: 4

Bin 139: 9 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 10
Size: 1567 Color: 4

Bin 140: 10 of cap free
Amount of items: 7
Items: 
Size: 6954 Color: 13
Size: 1331 Color: 4
Size: 1325 Color: 7
Size: 1210 Color: 3
Size: 1158 Color: 12
Size: 1156 Color: 3
Size: 760 Color: 12

Bin 141: 10 of cap free
Amount of items: 3
Items: 
Size: 11958 Color: 19
Size: 1856 Color: 0
Size: 80 Color: 12

Bin 142: 10 of cap free
Amount of items: 2
Items: 
Size: 11983 Color: 2
Size: 1911 Color: 3

Bin 143: 10 of cap free
Amount of items: 2
Items: 
Size: 12198 Color: 0
Size: 1696 Color: 8

Bin 144: 11 of cap free
Amount of items: 3
Items: 
Size: 11613 Color: 14
Size: 2176 Color: 1
Size: 104 Color: 9

Bin 145: 11 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 0
Size: 1869 Color: 12

Bin 146: 12 of cap free
Amount of items: 2
Items: 
Size: 11749 Color: 12
Size: 2143 Color: 19

Bin 147: 12 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 17
Size: 1814 Color: 6

Bin 148: 12 of cap free
Amount of items: 2
Items: 
Size: 12361 Color: 4
Size: 1531 Color: 11

Bin 149: 12 of cap free
Amount of items: 2
Items: 
Size: 12428 Color: 3
Size: 1464 Color: 10

Bin 150: 13 of cap free
Amount of items: 2
Items: 
Size: 9643 Color: 1
Size: 4248 Color: 15

Bin 151: 14 of cap free
Amount of items: 2
Items: 
Size: 9734 Color: 11
Size: 4156 Color: 6

Bin 152: 14 of cap free
Amount of items: 3
Items: 
Size: 9902 Color: 5
Size: 3608 Color: 4
Size: 380 Color: 9

Bin 153: 15 of cap free
Amount of items: 3
Items: 
Size: 9715 Color: 5
Size: 3918 Color: 2
Size: 256 Color: 9

Bin 154: 16 of cap free
Amount of items: 2
Items: 
Size: 12132 Color: 8
Size: 1756 Color: 18

Bin 155: 16 of cap free
Amount of items: 2
Items: 
Size: 12248 Color: 19
Size: 1640 Color: 11

Bin 156: 17 of cap free
Amount of items: 2
Items: 
Size: 12171 Color: 15
Size: 1716 Color: 14

Bin 157: 18 of cap free
Amount of items: 2
Items: 
Size: 11290 Color: 15
Size: 2596 Color: 5

Bin 158: 21 of cap free
Amount of items: 2
Items: 
Size: 10479 Color: 14
Size: 3404 Color: 7

Bin 159: 21 of cap free
Amount of items: 2
Items: 
Size: 12399 Color: 10
Size: 1484 Color: 16

Bin 160: 22 of cap free
Amount of items: 2
Items: 
Size: 11750 Color: 19
Size: 2132 Color: 5

Bin 161: 22 of cap free
Amount of items: 2
Items: 
Size: 12014 Color: 18
Size: 1868 Color: 12

Bin 162: 23 of cap free
Amount of items: 2
Items: 
Size: 11517 Color: 3
Size: 2364 Color: 4

Bin 163: 25 of cap free
Amount of items: 2
Items: 
Size: 11663 Color: 18
Size: 2216 Color: 7

Bin 164: 28 of cap free
Amount of items: 2
Items: 
Size: 11116 Color: 8
Size: 2760 Color: 16

Bin 165: 28 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 17
Size: 1630 Color: 8

Bin 166: 29 of cap free
Amount of items: 2
Items: 
Size: 11474 Color: 13
Size: 2401 Color: 16

Bin 167: 31 of cap free
Amount of items: 2
Items: 
Size: 12075 Color: 11
Size: 1798 Color: 15

Bin 168: 32 of cap free
Amount of items: 2
Items: 
Size: 11506 Color: 6
Size: 2366 Color: 3

Bin 169: 33 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 2
Size: 1991 Color: 8

Bin 170: 34 of cap free
Amount of items: 2
Items: 
Size: 12392 Color: 3
Size: 1478 Color: 18

Bin 171: 35 of cap free
Amount of items: 2
Items: 
Size: 9886 Color: 0
Size: 3983 Color: 11

Bin 172: 36 of cap free
Amount of items: 2
Items: 
Size: 10370 Color: 4
Size: 3498 Color: 14

Bin 173: 36 of cap free
Amount of items: 2
Items: 
Size: 12190 Color: 0
Size: 1678 Color: 3

Bin 174: 38 of cap free
Amount of items: 4
Items: 
Size: 6958 Color: 11
Size: 5050 Color: 5
Size: 1598 Color: 7
Size: 260 Color: 3

Bin 175: 38 of cap free
Amount of items: 2
Items: 
Size: 12342 Color: 4
Size: 1524 Color: 17

Bin 176: 39 of cap free
Amount of items: 2
Items: 
Size: 10434 Color: 17
Size: 3431 Color: 15

Bin 177: 39 of cap free
Amount of items: 2
Items: 
Size: 12237 Color: 14
Size: 1628 Color: 19

Bin 178: 40 of cap free
Amount of items: 2
Items: 
Size: 11735 Color: 14
Size: 2129 Color: 18

Bin 179: 42 of cap free
Amount of items: 2
Items: 
Size: 11333 Color: 5
Size: 2529 Color: 0

Bin 180: 43 of cap free
Amount of items: 2
Items: 
Size: 12390 Color: 14
Size: 1471 Color: 3

Bin 181: 44 of cap free
Amount of items: 2
Items: 
Size: 8924 Color: 2
Size: 4936 Color: 5

Bin 182: 44 of cap free
Amount of items: 2
Items: 
Size: 9206 Color: 1
Size: 4654 Color: 2

Bin 183: 44 of cap free
Amount of items: 2
Items: 
Size: 12236 Color: 10
Size: 1624 Color: 4

Bin 184: 47 of cap free
Amount of items: 2
Items: 
Size: 10366 Color: 14
Size: 3491 Color: 10

Bin 185: 49 of cap free
Amount of items: 2
Items: 
Size: 11813 Color: 7
Size: 2042 Color: 2

Bin 186: 53 of cap free
Amount of items: 2
Items: 
Size: 12229 Color: 3
Size: 1622 Color: 11

Bin 187: 58 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 4
Size: 5790 Color: 19
Size: 1088 Color: 11

Bin 188: 60 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 0
Size: 6880 Color: 7

Bin 189: 63 of cap free
Amount of items: 2
Items: 
Size: 11161 Color: 16
Size: 2680 Color: 12

Bin 190: 70 of cap free
Amount of items: 2
Items: 
Size: 11933 Color: 4
Size: 1901 Color: 19

Bin 191: 74 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 5
Size: 5628 Color: 0
Size: 1236 Color: 9

Bin 192: 80 of cap free
Amount of items: 2
Items: 
Size: 8972 Color: 14
Size: 4852 Color: 11

Bin 193: 82 of cap free
Amount of items: 2
Items: 
Size: 11620 Color: 17
Size: 2202 Color: 0

Bin 194: 82 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 0
Size: 2182 Color: 13

Bin 195: 87 of cap free
Amount of items: 2
Items: 
Size: 7875 Color: 4
Size: 5942 Color: 2

Bin 196: 98 of cap free
Amount of items: 2
Items: 
Size: 9766 Color: 18
Size: 4040 Color: 12

Bin 197: 115 of cap free
Amount of items: 2
Items: 
Size: 9868 Color: 7
Size: 3921 Color: 5

Bin 198: 141 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 2
Size: 5051 Color: 1
Size: 1556 Color: 9

Bin 199: 11432 of cap free
Amount of items: 9
Items: 
Size: 316 Color: 19
Size: 316 Color: 10
Size: 288 Color: 4
Size: 272 Color: 16
Size: 272 Color: 8
Size: 264 Color: 6
Size: 256 Color: 13
Size: 256 Color: 2
Size: 232 Color: 0

Total size: 2752992
Total free space: 13904

