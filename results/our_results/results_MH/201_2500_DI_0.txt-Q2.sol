Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 0
Size: 568 Color: 0
Size: 284 Color: 1
Size: 200 Color: 0
Size: 174 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 1
Size: 585 Color: 0
Size: 356 Color: 1
Size: 220 Color: 1
Size: 64 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 1018 Color: 0
Size: 200 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 0
Size: 988 Color: 1
Size: 192 Color: 1

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1719 Color: 1
Size: 631 Color: 0
Size: 80 Color: 1
Size: 26 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 0
Size: 668 Color: 1
Size: 56 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 1
Size: 609 Color: 1
Size: 120 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1830 Color: 0
Size: 458 Color: 0
Size: 168 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 1
Size: 526 Color: 1
Size: 104 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 0
Size: 564 Color: 1
Size: 40 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 0
Size: 438 Color: 0
Size: 48 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 0
Size: 406 Color: 1
Size: 36 Color: 1

Bin 13: 1 of cap free
Amount of items: 7
Items: 
Size: 1229 Color: 1
Size: 436 Color: 0
Size: 204 Color: 1
Size: 204 Color: 0
Size: 160 Color: 1
Size: 128 Color: 0
Size: 94 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 1
Size: 840 Color: 1
Size: 56 Color: 0

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 1581 Color: 1
Size: 874 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 0
Size: 462 Color: 1
Size: 144 Color: 0

Bin 17: 2 of cap free
Amount of items: 5
Items: 
Size: 1536 Color: 1
Size: 602 Color: 0
Size: 228 Color: 0
Size: 44 Color: 1
Size: 44 Color: 0

Bin 18: 2 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 1
Size: 738 Color: 0
Size: 144 Color: 0

Bin 19: 2 of cap free
Amount of items: 4
Items: 
Size: 1574 Color: 1
Size: 812 Color: 0
Size: 40 Color: 1
Size: 28 Color: 0

Bin 20: 2 of cap free
Amount of items: 3
Items: 
Size: 1660 Color: 0
Size: 646 Color: 1
Size: 148 Color: 1

Bin 21: 2 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 0
Size: 332 Color: 0
Size: 64 Color: 1

Bin 22: 2 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 1
Size: 276 Color: 0
Size: 64 Color: 0

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 2132 Color: 1
Size: 322 Color: 0

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 1
Size: 262 Color: 0

Bin 25: 2 of cap free
Amount of items: 4
Items: 
Size: 2204 Color: 1
Size: 238 Color: 0
Size: 8 Color: 0
Size: 4 Color: 1

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 2206 Color: 1
Size: 248 Color: 0

Bin 27: 3 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 0
Size: 980 Color: 0
Size: 64 Color: 1

Bin 28: 3 of cap free
Amount of items: 2
Items: 
Size: 1723 Color: 1
Size: 730 Color: 0

Bin 29: 4 of cap free
Amount of items: 3
Items: 
Size: 1833 Color: 1
Size: 507 Color: 1
Size: 112 Color: 0

Bin 30: 4 of cap free
Amount of items: 2
Items: 
Size: 2198 Color: 1
Size: 254 Color: 0

Bin 31: 5 of cap free
Amount of items: 2
Items: 
Size: 1710 Color: 1
Size: 741 Color: 0

Bin 32: 5 of cap free
Amount of items: 4
Items: 
Size: 1891 Color: 1
Size: 512 Color: 0
Size: 32 Color: 1
Size: 16 Color: 0

Bin 33: 5 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 0
Size: 541 Color: 1

Bin 34: 6 of cap free
Amount of items: 4
Items: 
Size: 2146 Color: 1
Size: 280 Color: 0
Size: 16 Color: 1
Size: 8 Color: 0

Bin 35: 6 of cap free
Amount of items: 2
Items: 
Size: 2188 Color: 0
Size: 262 Color: 1

Bin 36: 7 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 1
Size: 1023 Color: 0

Bin 37: 8 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 0
Size: 508 Color: 1

Bin 38: 8 of cap free
Amount of items: 2
Items: 
Size: 2036 Color: 0
Size: 412 Color: 1

Bin 39: 8 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 0
Size: 370 Color: 1
Size: 16 Color: 1

Bin 40: 9 of cap free
Amount of items: 2
Items: 
Size: 1964 Color: 0
Size: 483 Color: 1

Bin 41: 10 of cap free
Amount of items: 3
Items: 
Size: 1484 Color: 1
Size: 862 Color: 1
Size: 100 Color: 0

Bin 42: 10 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 0
Size: 750 Color: 1

Bin 43: 10 of cap free
Amount of items: 2
Items: 
Size: 2116 Color: 0
Size: 330 Color: 1

Bin 44: 10 of cap free
Amount of items: 2
Items: 
Size: 2174 Color: 0
Size: 272 Color: 1

Bin 45: 11 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 0
Size: 731 Color: 1
Size: 32 Color: 0

Bin 46: 11 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 1
Size: 471 Color: 0

Bin 47: 12 of cap free
Amount of items: 2
Items: 
Size: 2074 Color: 0
Size: 370 Color: 1

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 2060 Color: 1
Size: 384 Color: 0

Bin 49: 12 of cap free
Amount of items: 2
Items: 
Size: 2158 Color: 1
Size: 286 Color: 0

Bin 50: 13 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 1
Size: 1022 Color: 0

Bin 51: 13 of cap free
Amount of items: 2
Items: 
Size: 1694 Color: 0
Size: 749 Color: 1

Bin 52: 14 of cap free
Amount of items: 20
Items: 
Size: 204 Color: 0
Size: 172 Color: 0
Size: 172 Color: 0
Size: 148 Color: 0
Size: 146 Color: 1
Size: 144 Color: 0
Size: 128 Color: 1
Size: 124 Color: 1
Size: 124 Color: 1
Size: 122 Color: 0
Size: 122 Color: 0
Size: 120 Color: 1
Size: 104 Color: 1
Size: 104 Color: 1
Size: 96 Color: 1
Size: 88 Color: 1
Size: 88 Color: 0
Size: 88 Color: 0
Size: 88 Color: 0
Size: 60 Color: 1

Bin 53: 14 of cap free
Amount of items: 2
Items: 
Size: 1569 Color: 0
Size: 873 Color: 1

Bin 54: 15 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 1
Size: 740 Color: 0

Bin 55: 17 of cap free
Amount of items: 2
Items: 
Size: 1907 Color: 0
Size: 532 Color: 1

Bin 56: 18 of cap free
Amount of items: 2
Items: 
Size: 1823 Color: 0
Size: 615 Color: 1

Bin 57: 24 of cap free
Amount of items: 2
Items: 
Size: 1947 Color: 1
Size: 485 Color: 0

Bin 58: 25 of cap free
Amount of items: 2
Items: 
Size: 1820 Color: 0
Size: 611 Color: 1

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 1
Size: 1020 Color: 0

Bin 60: 28 of cap free
Amount of items: 2
Items: 
Size: 1906 Color: 1
Size: 522 Color: 0

Bin 61: 30 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 1
Size: 692 Color: 0

Bin 62: 34 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 976 Color: 0
Size: 210 Color: 0

Bin 63: 35 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 0
Size: 863 Color: 1

Bin 64: 38 of cap free
Amount of items: 2
Items: 
Size: 1780 Color: 1
Size: 638 Color: 0

Bin 65: 39 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 0
Size: 1021 Color: 1

Bin 66: 1870 of cap free
Amount of items: 9
Items: 
Size: 82 Color: 0
Size: 80 Color: 1
Size: 80 Color: 0
Size: 80 Color: 0
Size: 72 Color: 0
Size: 48 Color: 1
Size: 48 Color: 1
Size: 48 Color: 1
Size: 48 Color: 0

Total size: 159640
Total free space: 2456

