Capicity Bin: 13904
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 6966 Color: 411
Size: 5778 Color: 394
Size: 404 Color: 99
Size: 384 Color: 93
Size: 372 Color: 89

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9734 Color: 456
Size: 3918 Color: 360
Size: 252 Color: 22

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 460
Size: 3816 Color: 355
Size: 240 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10302 Color: 471
Size: 2946 Color: 328
Size: 656 Color: 144

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 473
Size: 2624 Color: 318
Size: 952 Color: 180

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 479
Size: 3350 Color: 341
Size: 120 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 484
Size: 2680 Color: 321
Size: 528 Color: 129

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 487
Size: 2892 Color: 325
Size: 264 Color: 34

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10796 Color: 489
Size: 1678 Color: 264
Size: 1430 Color: 236

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11076 Color: 496
Size: 2040 Color: 292
Size: 788 Color: 164

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11088 Color: 499
Size: 1628 Color: 259
Size: 1188 Color: 205

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 500
Size: 2333 Color: 305
Size: 466 Color: 115

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 502
Size: 2455 Color: 312
Size: 288 Color: 49

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11348 Color: 508
Size: 1404 Color: 234
Size: 1152 Color: 198

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11454 Color: 511
Size: 1236 Color: 210
Size: 1214 Color: 208

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11461 Color: 512
Size: 2037 Color: 291
Size: 406 Color: 100

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 513
Size: 1624 Color: 258
Size: 816 Color: 167

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 514
Size: 2032 Color: 290
Size: 400 Color: 98

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11613 Color: 520
Size: 1531 Color: 248
Size: 760 Color: 160

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11620 Color: 521
Size: 1272 Color: 217
Size: 1012 Color: 189

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 523
Size: 1578 Color: 251
Size: 686 Color: 153

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11663 Color: 524
Size: 1601 Color: 256
Size: 640 Color: 143

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11730 Color: 528
Size: 1582 Color: 252
Size: 592 Color: 138

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11735 Color: 529
Size: 1777 Color: 271
Size: 392 Color: 94

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11749 Color: 530
Size: 1797 Color: 273
Size: 358 Color: 83

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11813 Color: 536
Size: 1791 Color: 272
Size: 300 Color: 56

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 538
Size: 1332 Color: 226
Size: 692 Color: 154

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11933 Color: 541
Size: 1677 Color: 263
Size: 294 Color: 54

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 542
Size: 1640 Color: 261
Size: 320 Color: 65

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11950 Color: 543
Size: 1630 Color: 260
Size: 324 Color: 70

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11956 Color: 544
Size: 1484 Color: 244
Size: 464 Color: 114

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12014 Color: 550
Size: 1598 Color: 255
Size: 292 Color: 53

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12075 Color: 555
Size: 1223 Color: 209
Size: 606 Color: 140

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 558
Size: 1396 Color: 232
Size: 376 Color: 90

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 563
Size: 1325 Color: 224
Size: 408 Color: 101

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12189 Color: 564
Size: 1237 Color: 211
Size: 478 Color: 120

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12190 Color: 565
Size: 1210 Color: 207
Size: 504 Color: 122

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12198 Color: 566
Size: 1390 Color: 231
Size: 316 Color: 64

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12228 Color: 568
Size: 892 Color: 174
Size: 784 Color: 163

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12236 Color: 570
Size: 872 Color: 171
Size: 796 Color: 165

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12237 Color: 571
Size: 1331 Color: 225
Size: 336 Color: 75

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12238 Color: 572
Size: 1310 Color: 222
Size: 356 Color: 82

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12246 Color: 573
Size: 1088 Color: 193
Size: 570 Color: 132

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 574
Size: 1384 Color: 230
Size: 272 Color: 41

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 577
Size: 1008 Color: 187
Size: 588 Color: 136

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 580
Size: 1320 Color: 223
Size: 256 Color: 27

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 583
Size: 1292 Color: 220
Size: 256 Color: 28

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12361 Color: 584
Size: 1271 Color: 216
Size: 272 Color: 36

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12390 Color: 587
Size: 1162 Color: 203
Size: 352 Color: 78

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 588
Size: 1156 Color: 201
Size: 356 Color: 81

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 589
Size: 1262 Color: 215
Size: 244 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12399 Color: 590
Size: 1255 Color: 213
Size: 250 Color: 21

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12407 Color: 591
Size: 1249 Color: 212
Size: 248 Color: 19

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12484 Color: 597
Size: 992 Color: 183
Size: 428 Color: 107

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12502 Color: 598
Size: 1022 Color: 191
Size: 380 Color: 91

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12510 Color: 600
Size: 1014 Color: 190
Size: 380 Color: 92

Bin 57: 1 of cap free
Amount of items: 9
Items: 
Size: 6953 Color: 405
Size: 1152 Color: 196
Size: 1120 Color: 195
Size: 1120 Color: 194
Size: 1088 Color: 192
Size: 1010 Color: 188
Size: 604 Color: 139
Size: 432 Color: 108
Size: 424 Color: 106

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 9787 Color: 458
Size: 4116 Color: 366

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 10519 Color: 482
Size: 3384 Color: 344

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 485
Size: 2906 Color: 327
Size: 288 Color: 51

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 10743 Color: 486
Size: 3064 Color: 334
Size: 96 Color: 1

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11025 Color: 493
Size: 1982 Color: 286
Size: 896 Color: 177

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11077 Color: 497
Size: 2578 Color: 315
Size: 248 Color: 20

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11229 Color: 503
Size: 2202 Color: 301
Size: 472 Color: 117

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11333 Color: 507
Size: 2026 Color: 289
Size: 544 Color: 130

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11623 Color: 522
Size: 2176 Color: 299
Size: 104 Color: 2

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 12147 Color: 561
Size: 1756 Color: 268

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12229 Color: 569
Size: 1204 Color: 206
Size: 470 Color: 116

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 12281 Color: 576
Size: 1622 Color: 257

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 12374 Color: 585
Size: 1529 Color: 247

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 12381 Color: 586
Size: 1522 Color: 245

Bin 72: 2 of cap free
Amount of items: 5
Items: 
Size: 6968 Color: 412
Size: 5782 Color: 395
Size: 416 Color: 104
Size: 368 Color: 88
Size: 368 Color: 87

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 8972 Color: 440
Size: 4654 Color: 375
Size: 276 Color: 42

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9556 Color: 448
Size: 2964 Color: 329
Size: 1382 Color: 229

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 457
Size: 2984 Color: 330
Size: 1152 Color: 197

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 9868 Color: 461
Size: 4034 Color: 364

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 10203 Color: 468
Size: 3491 Color: 348
Size: 208 Color: 9

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 472
Size: 3018 Color: 332
Size: 576 Color: 134

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 10871 Color: 491
Size: 3031 Color: 333

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11066 Color: 495
Size: 2360 Color: 307
Size: 476 Color: 119

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 527
Size: 2182 Color: 300

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 11773 Color: 533
Size: 2129 Color: 296

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 11958 Color: 545
Size: 1944 Color: 285

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 12006 Color: 549
Size: 1896 Color: 281

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 12134 Color: 559
Size: 1768 Color: 270

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 12315 Color: 579
Size: 1587 Color: 253

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 12437 Color: 594
Size: 1465 Color: 241

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 449
Size: 2894 Color: 326
Size: 1431 Color: 237

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 11080 Color: 498
Size: 2821 Color: 323

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 11990 Color: 547
Size: 1911 Color: 284

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 12069 Color: 554
Size: 1832 Color: 277

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 12139 Color: 560
Size: 1762 Color: 269

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 12334 Color: 581
Size: 1567 Color: 250

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 12504 Color: 599
Size: 1397 Color: 233

Bin 95: 4 of cap free
Amount of items: 5
Items: 
Size: 6974 Color: 413
Size: 5790 Color: 397
Size: 416 Color: 103
Size: 360 Color: 86
Size: 360 Color: 85

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 11396 Color: 510
Size: 2504 Color: 313

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 11506 Color: 516
Size: 2394 Color: 310

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 11576 Color: 519
Size: 2324 Color: 304

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 11757 Color: 532
Size: 2143 Color: 298

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 11790 Color: 534
Size: 2110 Color: 295

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 12044 Color: 553
Size: 1856 Color: 278

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 12468 Color: 596
Size: 1432 Color: 238

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 7771 Color: 417
Size: 5784 Color: 396
Size: 344 Color: 76

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 10814 Color: 490
Size: 3085 Color: 335

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 11668 Color: 525
Size: 2231 Color: 303

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 11998 Color: 548
Size: 1901 Color: 282

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 12309 Color: 578
Size: 1590 Color: 254

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 12421 Color: 592
Size: 1478 Color: 243

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 12428 Color: 593
Size: 1471 Color: 242

Bin 110: 5 of cap free
Amount of items: 2
Items: 
Size: 12454 Color: 595
Size: 1445 Color: 239

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 11262 Color: 505
Size: 2636 Color: 320

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 12084 Color: 557
Size: 1814 Color: 276

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 12342 Color: 582
Size: 1556 Color: 249

Bin 114: 7 of cap free
Amount of items: 4
Items: 
Size: 9199 Color: 444
Size: 4156 Color: 367
Size: 272 Color: 37
Size: 270 Color: 35

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 10269 Color: 470
Size: 3628 Color: 352

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 10348 Color: 474
Size: 3361 Color: 342
Size: 188 Color: 7

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 12254 Color: 575
Size: 1643 Color: 262

Bin 118: 8 of cap free
Amount of items: 5
Items: 
Size: 7992 Color: 425
Size: 4788 Color: 378
Size: 476 Color: 118
Size: 320 Color: 67
Size: 320 Color: 66

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 426
Size: 5496 Color: 391
Size: 316 Color: 63

Bin 120: 8 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 430
Size: 5078 Color: 387
Size: 304 Color: 57

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 9702 Color: 454
Size: 3942 Color: 362
Size: 252 Color: 24

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 11530 Color: 518
Size: 2366 Color: 309

Bin 123: 8 of cap free
Amount of items: 2
Items: 
Size: 11804 Color: 535
Size: 2092 Color: 294

Bin 124: 8 of cap free
Amount of items: 2
Items: 
Size: 11894 Color: 540
Size: 2002 Color: 288

Bin 125: 8 of cap free
Amount of items: 2
Items: 
Size: 12200 Color: 567
Size: 1696 Color: 265

Bin 126: 9 of cap free
Amount of items: 4
Items: 
Size: 8168 Color: 428
Size: 5111 Color: 389
Size: 312 Color: 60
Size: 304 Color: 59

Bin 127: 9 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 431
Size: 5079 Color: 388
Size: 296 Color: 55

Bin 128: 9 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 562
Size: 1743 Color: 267

Bin 129: 10 of cap free
Amount of items: 5
Items: 
Size: 7867 Color: 422
Size: 4759 Color: 377
Size: 616 Color: 142
Size: 328 Color: 72
Size: 324 Color: 71

Bin 130: 10 of cap free
Amount of items: 3
Items: 
Size: 8579 Color: 435
Size: 5031 Color: 384
Size: 284 Color: 47

Bin 131: 10 of cap free
Amount of items: 3
Items: 
Size: 8824 Color: 438
Size: 4792 Color: 379
Size: 278 Color: 44

Bin 132: 10 of cap free
Amount of items: 3
Items: 
Size: 9958 Color: 466
Size: 3704 Color: 353
Size: 232 Color: 11

Bin 133: 10 of cap free
Amount of items: 2
Items: 
Size: 11852 Color: 537
Size: 2042 Color: 293

Bin 134: 10 of cap free
Amount of items: 2
Items: 
Size: 12025 Color: 552
Size: 1869 Color: 280

Bin 135: 11 of cap free
Amount of items: 2
Items: 
Size: 10904 Color: 492
Size: 2989 Color: 331

Bin 136: 12 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 451
Size: 4040 Color: 365
Size: 256 Color: 29

Bin 137: 12 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 551
Size: 1868 Color: 279

Bin 138: 13 of cap free
Amount of items: 3
Items: 
Size: 9125 Color: 442
Size: 4494 Color: 374
Size: 272 Color: 39

Bin 139: 13 of cap free
Amount of items: 2
Items: 
Size: 11256 Color: 504
Size: 2635 Color: 319

Bin 140: 13 of cap free
Amount of items: 2
Items: 
Size: 11983 Color: 546
Size: 1908 Color: 283

Bin 141: 14 of cap free
Amount of items: 2
Items: 
Size: 10600 Color: 483
Size: 3290 Color: 339

Bin 142: 14 of cap free
Amount of items: 2
Items: 
Size: 11674 Color: 526
Size: 2216 Color: 302

Bin 143: 15 of cap free
Amount of items: 2
Items: 
Size: 11034 Color: 494
Size: 2855 Color: 324

Bin 144: 16 of cap free
Amount of items: 3
Items: 
Size: 9206 Color: 445
Size: 4418 Color: 370
Size: 264 Color: 33

Bin 145: 16 of cap free
Amount of items: 3
Items: 
Size: 9715 Color: 455
Size: 3921 Color: 361
Size: 252 Color: 23

Bin 146: 16 of cap free
Amount of items: 3
Items: 
Size: 10370 Color: 476
Size: 3342 Color: 340
Size: 176 Color: 6

Bin 147: 17 of cap free
Amount of items: 3
Items: 
Size: 8574 Color: 434
Size: 5025 Color: 383
Size: 288 Color: 48

Bin 148: 17 of cap free
Amount of items: 3
Items: 
Size: 8667 Color: 437
Size: 4936 Color: 381
Size: 284 Color: 45

Bin 149: 17 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 556
Size: 1809 Color: 275

Bin 150: 18 of cap free
Amount of items: 3
Items: 
Size: 8606 Color: 436
Size: 4996 Color: 382
Size: 284 Color: 46

Bin 151: 18 of cap free
Amount of items: 2
Items: 
Size: 11290 Color: 506
Size: 2596 Color: 316

Bin 152: 19 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 443
Size: 4439 Color: 371
Size: 272 Color: 38

Bin 153: 20 of cap free
Amount of items: 3
Items: 
Size: 9820 Color: 459
Size: 3818 Color: 356
Size: 246 Color: 18

Bin 154: 20 of cap free
Amount of items: 2
Items: 
Size: 10762 Color: 488
Size: 3122 Color: 336

Bin 155: 20 of cap free
Amount of items: 2
Items: 
Size: 11893 Color: 539
Size: 1991 Color: 287

Bin 156: 21 of cap free
Amount of items: 2
Items: 
Size: 10479 Color: 481
Size: 3404 Color: 345

Bin 157: 22 of cap free
Amount of items: 2
Items: 
Size: 11750 Color: 531
Size: 2132 Color: 297

Bin 158: 23 of cap free
Amount of items: 3
Items: 
Size: 8539 Color: 432
Size: 5050 Color: 385
Size: 292 Color: 52

Bin 159: 23 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 433
Size: 5051 Color: 386
Size: 288 Color: 50

Bin 160: 23 of cap free
Amount of items: 2
Items: 
Size: 11517 Color: 517
Size: 2364 Color: 308

Bin 161: 24 of cap free
Amount of items: 3
Items: 
Size: 10436 Color: 480
Size: 3364 Color: 343
Size: 80 Color: 0

Bin 162: 24 of cap free
Amount of items: 2
Items: 
Size: 11351 Color: 509
Size: 2529 Color: 314

Bin 163: 28 of cap free
Amount of items: 2
Items: 
Size: 11116 Color: 501
Size: 2760 Color: 322

Bin 164: 29 of cap free
Amount of items: 2
Items: 
Size: 11474 Color: 515
Size: 2401 Color: 311

Bin 165: 35 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 469
Size: 3431 Color: 346
Size: 206 Color: 8

Bin 166: 36 of cap free
Amount of items: 5
Items: 
Size: 7846 Color: 421
Size: 4748 Color: 376
Size: 608 Color: 141
Size: 334 Color: 74
Size: 332 Color: 73

Bin 167: 37 of cap free
Amount of items: 3
Items: 
Size: 9238 Color: 446
Size: 4365 Color: 369
Size: 264 Color: 32

Bin 168: 40 of cap free
Amount of items: 3
Items: 
Size: 7916 Color: 424
Size: 5628 Color: 393
Size: 320 Color: 68

Bin 169: 40 of cap free
Amount of items: 3
Items: 
Size: 9902 Color: 465
Size: 3730 Color: 354
Size: 232 Color: 12

Bin 170: 40 of cap free
Amount of items: 2
Items: 
Size: 10366 Color: 475
Size: 3498 Color: 349

Bin 171: 47 of cap free
Amount of items: 5
Items: 
Size: 6958 Color: 409
Size: 2622 Color: 317
Size: 2357 Color: 306
Size: 1524 Color: 246
Size: 396 Color: 95

Bin 172: 50 of cap free
Amount of items: 2
Items: 
Size: 9871 Color: 462
Size: 3983 Color: 363

Bin 173: 50 of cap free
Amount of items: 3
Items: 
Size: 10418 Color: 478
Size: 3276 Color: 338
Size: 160 Color: 4

Bin 174: 52 of cap free
Amount of items: 7
Items: 
Size: 6956 Color: 407
Size: 1378 Color: 228
Size: 1353 Color: 227
Size: 1302 Color: 221
Size: 1287 Color: 219
Size: 1176 Color: 204
Size: 400 Color: 97

Bin 175: 56 of cap free
Amount of items: 3
Items: 
Size: 7320 Color: 416
Size: 6180 Color: 403
Size: 348 Color: 77

Bin 176: 60 of cap free
Amount of items: 17
Items: 
Size: 1008 Color: 186
Size: 1006 Color: 185
Size: 1004 Color: 184
Size: 976 Color: 182
Size: 968 Color: 181
Size: 944 Color: 179
Size: 944 Color: 178
Size: 896 Color: 176
Size: 896 Color: 175
Size: 886 Color: 173
Size: 880 Color: 172
Size: 832 Color: 170
Size: 824 Color: 169
Size: 464 Color: 112
Size: 444 Color: 111
Size: 440 Color: 110
Size: 432 Color: 109

Bin 177: 60 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 410
Size: 6880 Color: 404

Bin 178: 60 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 447
Size: 4248 Color: 368
Size: 260 Color: 31

Bin 179: 60 of cap free
Amount of items: 4
Items: 
Size: 9886 Color: 463
Size: 3478 Color: 347
Size: 240 Color: 15
Size: 240 Color: 14

Bin 180: 64 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 467
Size: 3608 Color: 351
Size: 224 Color: 10

Bin 181: 70 of cap free
Amount of items: 3
Items: 
Size: 10410 Color: 477
Size: 3256 Color: 337
Size: 168 Color: 5

Bin 182: 73 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 453
Size: 3903 Color: 359
Size: 256 Color: 25

Bin 183: 80 of cap free
Amount of items: 3
Items: 
Size: 9064 Color: 441
Size: 4488 Color: 373
Size: 272 Color: 40

Bin 184: 85 of cap free
Amount of items: 3
Items: 
Size: 7875 Color: 423
Size: 5624 Color: 392
Size: 320 Color: 69

Bin 185: 104 of cap free
Amount of items: 8
Items: 
Size: 6954 Color: 406
Size: 1278 Color: 218
Size: 1258 Color: 214
Size: 1158 Color: 202
Size: 1156 Color: 200
Size: 1156 Color: 199
Size: 424 Color: 105
Size: 416 Color: 102

Bin 186: 113 of cap free
Amount of items: 3
Items: 
Size: 9643 Color: 452
Size: 3892 Color: 358
Size: 256 Color: 26

Bin 187: 119 of cap free
Amount of items: 2
Items: 
Size: 7843 Color: 420
Size: 5942 Color: 401

Bin 188: 151 of cap free
Amount of items: 6
Items: 
Size: 6957 Color: 408
Size: 1798 Color: 274
Size: 1716 Color: 266
Size: 1464 Color: 240
Size: 1422 Color: 235
Size: 396 Color: 96

Bin 189: 173 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 450
Size: 3890 Color: 357
Size: 260 Color: 30

Bin 190: 175 of cap free
Amount of items: 3
Items: 
Size: 9894 Color: 464
Size: 3603 Color: 350
Size: 232 Color: 13

Bin 191: 234 of cap free
Amount of items: 3
Items: 
Size: 8924 Color: 439
Size: 4470 Color: 372
Size: 276 Color: 43

Bin 192: 245 of cap free
Amount of items: 4
Items: 
Size: 7156 Color: 414
Size: 5791 Color: 398
Size: 358 Color: 84
Size: 354 Color: 80

Bin 193: 248 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 415
Size: 6144 Color: 402
Size: 352 Color: 79

Bin 194: 252 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 429
Size: 5136 Color: 390
Size: 304 Color: 58

Bin 195: 260 of cap free
Amount of items: 4
Items: 
Size: 8164 Color: 427
Size: 4852 Color: 380
Size: 316 Color: 62
Size: 312 Color: 61

Bin 196: 294 of cap free
Amount of items: 2
Items: 
Size: 7814 Color: 419
Size: 5796 Color: 400

Bin 197: 300 of cap free
Amount of items: 2
Items: 
Size: 7811 Color: 418
Size: 5793 Color: 399

Bin 198: 452 of cap free
Amount of items: 21
Items: 
Size: 816 Color: 168
Size: 800 Color: 166
Size: 780 Color: 162
Size: 776 Color: 161
Size: 752 Color: 159
Size: 720 Color: 158
Size: 720 Color: 157
Size: 720 Color: 156
Size: 698 Color: 155
Size: 680 Color: 152
Size: 676 Color: 151
Size: 672 Color: 150
Size: 564 Color: 131
Size: 528 Color: 128
Size: 526 Color: 127
Size: 520 Color: 126
Size: 520 Color: 125
Size: 512 Color: 124
Size: 512 Color: 123
Size: 496 Color: 121
Size: 464 Color: 113

Bin 199: 8804 of cap free
Amount of items: 8
Items: 
Size: 672 Color: 149
Size: 672 Color: 148
Size: 672 Color: 147
Size: 668 Color: 146
Size: 668 Color: 145
Size: 592 Color: 137
Size: 580 Color: 135
Size: 576 Color: 133

Total size: 2752992
Total free space: 13904

