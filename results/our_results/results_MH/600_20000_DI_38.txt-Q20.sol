Capicity Bin: 15296
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 7656 Color: 1
Size: 4121 Color: 8
Size: 2151 Color: 5
Size: 824 Color: 6
Size: 544 Color: 14

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 7650 Color: 8
Size: 6372 Color: 3
Size: 866 Color: 19
Size: 408 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8626 Color: 16
Size: 6370 Color: 4
Size: 300 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8700 Color: 4
Size: 6328 Color: 19
Size: 268 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8716 Color: 14
Size: 5484 Color: 4
Size: 1096 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 10
Size: 5464 Color: 2
Size: 848 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9588 Color: 17
Size: 5444 Color: 4
Size: 264 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10033 Color: 15
Size: 4175 Color: 10
Size: 1088 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10672 Color: 15
Size: 4016 Color: 16
Size: 608 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11281 Color: 14
Size: 2907 Color: 9
Size: 1108 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11366 Color: 3
Size: 3578 Color: 8
Size: 352 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11370 Color: 14
Size: 2902 Color: 5
Size: 1024 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11532 Color: 11
Size: 2888 Color: 4
Size: 876 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11798 Color: 13
Size: 2918 Color: 3
Size: 580 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11816 Color: 2
Size: 2904 Color: 9
Size: 576 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11822 Color: 11
Size: 2812 Color: 14
Size: 662 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11832 Color: 1
Size: 2616 Color: 0
Size: 848 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12102 Color: 14
Size: 1922 Color: 8
Size: 1272 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 11
Size: 2476 Color: 7
Size: 652 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 2
Size: 2600 Color: 4
Size: 512 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12276 Color: 3
Size: 2756 Color: 14
Size: 264 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 10
Size: 2864 Color: 3
Size: 100 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 0
Size: 1848 Color: 3
Size: 992 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 3
Size: 1644 Color: 5
Size: 1272 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12390 Color: 19
Size: 1912 Color: 17
Size: 994 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12400 Color: 3
Size: 2640 Color: 7
Size: 256 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12520 Color: 0
Size: 2488 Color: 19
Size: 288 Color: 7

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12439 Color: 5
Size: 2129 Color: 18
Size: 728 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12449 Color: 9
Size: 2373 Color: 10
Size: 474 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12496 Color: 11
Size: 2328 Color: 15
Size: 472 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 0
Size: 2352 Color: 3
Size: 332 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 1
Size: 2326 Color: 2
Size: 392 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 19
Size: 2368 Color: 15
Size: 344 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12715 Color: 15
Size: 1991 Color: 2
Size: 590 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 15
Size: 1936 Color: 7
Size: 640 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12743 Color: 0
Size: 1961 Color: 11
Size: 592 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 4
Size: 1364 Color: 12
Size: 1088 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12848 Color: 1
Size: 1348 Color: 3
Size: 1100 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 18
Size: 2152 Color: 0
Size: 272 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 16
Size: 1712 Color: 5
Size: 648 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12940 Color: 6
Size: 1916 Color: 9
Size: 440 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 5
Size: 1584 Color: 6
Size: 736 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12992 Color: 12
Size: 1264 Color: 14
Size: 1040 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 6
Size: 1900 Color: 4
Size: 392 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 0
Size: 1424 Color: 1
Size: 852 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13127 Color: 11
Size: 1721 Color: 14
Size: 448 Color: 18

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13138 Color: 9
Size: 1636 Color: 19
Size: 522 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 12
Size: 1696 Color: 19
Size: 392 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13210 Color: 17
Size: 1566 Color: 12
Size: 520 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 0
Size: 1724 Color: 4
Size: 344 Color: 7

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 2
Size: 1264 Color: 5
Size: 768 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13269 Color: 8
Size: 1691 Color: 15
Size: 336 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 1
Size: 1002 Color: 0
Size: 992 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13340 Color: 5
Size: 1308 Color: 4
Size: 648 Color: 19

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13402 Color: 15
Size: 1582 Color: 7
Size: 312 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13478 Color: 4
Size: 994 Color: 13
Size: 824 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 10
Size: 1496 Color: 5
Size: 312 Color: 5

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 13
Size: 1352 Color: 0
Size: 432 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 15
Size: 1040 Color: 4
Size: 680 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13590 Color: 15
Size: 1354 Color: 18
Size: 352 Color: 16

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 16
Size: 1316 Color: 17
Size: 376 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13684 Color: 0
Size: 1248 Color: 19
Size: 364 Color: 7

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13620 Color: 10
Size: 1096 Color: 19
Size: 580 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 4
Size: 1384 Color: 16
Size: 272 Color: 9

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13654 Color: 9
Size: 1274 Color: 11
Size: 368 Color: 11

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 5
Size: 1286 Color: 3
Size: 342 Color: 17

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 4
Size: 1132 Color: 9
Size: 476 Color: 8

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13718 Color: 14
Size: 1318 Color: 9
Size: 260 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13724 Color: 15
Size: 964 Color: 0
Size: 608 Color: 14

Bin 70: 1 of cap free
Amount of items: 9
Items: 
Size: 7653 Color: 10
Size: 1448 Color: 18
Size: 1370 Color: 3
Size: 1040 Color: 1
Size: 1028 Color: 4
Size: 952 Color: 9
Size: 768 Color: 6
Size: 652 Color: 4
Size: 384 Color: 5

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 9257 Color: 0
Size: 4520 Color: 16
Size: 1518 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 10512 Color: 5
Size: 4159 Color: 8
Size: 624 Color: 18

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 1
Size: 3168 Color: 0
Size: 1248 Color: 7

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 15
Size: 3699 Color: 11
Size: 384 Color: 0

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 11299 Color: 10
Size: 3260 Color: 14
Size: 736 Color: 3

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 11319 Color: 3
Size: 3304 Color: 1
Size: 672 Color: 7

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 11363 Color: 2
Size: 3266 Color: 2
Size: 666 Color: 3

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 11371 Color: 1
Size: 3256 Color: 15
Size: 668 Color: 8

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 11568 Color: 13
Size: 3315 Color: 5
Size: 412 Color: 17

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 11729 Color: 14
Size: 2910 Color: 10
Size: 656 Color: 17

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 11793 Color: 12
Size: 2898 Color: 17
Size: 604 Color: 17

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 12461 Color: 3
Size: 1422 Color: 15
Size: 1412 Color: 13

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 5
Size: 2621 Color: 10

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 12943 Color: 14
Size: 1640 Color: 1
Size: 712 Color: 0

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 13375 Color: 16
Size: 1456 Color: 5
Size: 464 Color: 13

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 13674 Color: 1
Size: 1601 Color: 10
Size: 20 Color: 0

Bin 87: 2 of cap free
Amount of items: 5
Items: 
Size: 7664 Color: 13
Size: 3596 Color: 18
Size: 2610 Color: 4
Size: 1096 Color: 6
Size: 328 Color: 1

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 8744 Color: 3
Size: 6550 Color: 19

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 6
Size: 2966 Color: 11
Size: 1264 Color: 9

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 12
Size: 3262 Color: 12
Size: 848 Color: 8

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 11378 Color: 10
Size: 3404 Color: 0
Size: 512 Color: 12

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 12153 Color: 4
Size: 2957 Color: 0
Size: 184 Color: 10

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 12165 Color: 13
Size: 2973 Color: 5
Size: 156 Color: 18

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 12654 Color: 10
Size: 2204 Color: 0
Size: 436 Color: 8

Bin 95: 2 of cap free
Amount of items: 2
Items: 
Size: 12931 Color: 14
Size: 2363 Color: 16

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 12
Size: 2070 Color: 13

Bin 97: 2 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 17
Size: 1742 Color: 1

Bin 98: 2 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 15
Size: 1540 Color: 12

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 10099 Color: 10
Size: 4834 Color: 0
Size: 360 Color: 7

Bin 100: 3 of cap free
Amount of items: 2
Items: 
Size: 13233 Color: 8
Size: 2060 Color: 11

Bin 101: 4 of cap free
Amount of items: 7
Items: 
Size: 7649 Color: 5
Size: 2394 Color: 19
Size: 1976 Color: 11
Size: 1809 Color: 12
Size: 560 Color: 7
Size: 520 Color: 10
Size: 384 Color: 15

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 8498 Color: 0
Size: 6364 Color: 12
Size: 430 Color: 8

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 10184 Color: 8
Size: 4764 Color: 17
Size: 344 Color: 1

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 3
Size: 2381 Color: 10

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13105 Color: 14
Size: 2186 Color: 9

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 10858 Color: 10
Size: 4148 Color: 14
Size: 284 Color: 8

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 12144 Color: 14
Size: 2662 Color: 11
Size: 484 Color: 1

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 9329 Color: 8
Size: 4264 Color: 6
Size: 1696 Color: 0

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 10324 Color: 3
Size: 4693 Color: 19
Size: 272 Color: 0

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 10859 Color: 2
Size: 3694 Color: 16
Size: 736 Color: 5

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 3
Size: 3681 Color: 7
Size: 272 Color: 0

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 11
Size: 1971 Color: 2

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 8385 Color: 13
Size: 6615 Color: 5
Size: 288 Color: 7

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 8562 Color: 17
Size: 6374 Color: 0
Size: 352 Color: 6

Bin 115: 8 of cap free
Amount of items: 4
Items: 
Size: 8764 Color: 5
Size: 4278 Color: 12
Size: 1990 Color: 11
Size: 256 Color: 15

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 10152 Color: 8
Size: 5136 Color: 1

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 2
Size: 1964 Color: 1

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 16
Size: 1952 Color: 7

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13418 Color: 9
Size: 1870 Color: 14

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 10782 Color: 7
Size: 4328 Color: 1
Size: 176 Color: 9

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 12166 Color: 18
Size: 3120 Color: 19

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 9562 Color: 1
Size: 5723 Color: 2

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 10013 Color: 7
Size: 5272 Color: 15

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 12909 Color: 16
Size: 2376 Color: 11

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 10560 Color: 18
Size: 4724 Color: 13

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 10988 Color: 2
Size: 4296 Color: 7

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 11
Size: 4248 Color: 12

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 2
Size: 2524 Color: 1

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 14
Size: 1860 Color: 15

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 18
Size: 1520 Color: 11
Size: 32 Color: 7

Bin 131: 14 of cap free
Amount of items: 5
Items: 
Size: 7668 Color: 9
Size: 4108 Color: 13
Size: 1776 Color: 7
Size: 1150 Color: 8
Size: 580 Color: 19

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 13542 Color: 6
Size: 1740 Color: 3

Bin 133: 17 of cap free
Amount of items: 2
Items: 
Size: 13452 Color: 2
Size: 1827 Color: 8

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 11298 Color: 12
Size: 3980 Color: 3

Bin 135: 18 of cap free
Amount of items: 2
Items: 
Size: 13111 Color: 13
Size: 2167 Color: 19

Bin 136: 18 of cap free
Amount of items: 2
Items: 
Size: 13616 Color: 3
Size: 1662 Color: 14

Bin 137: 19 of cap free
Amount of items: 3
Items: 
Size: 10107 Color: 5
Size: 4164 Color: 15
Size: 1006 Color: 4

Bin 138: 19 of cap free
Amount of items: 3
Items: 
Size: 10307 Color: 13
Size: 4218 Color: 16
Size: 752 Color: 5

Bin 139: 19 of cap free
Amount of items: 2
Items: 
Size: 11749 Color: 8
Size: 3528 Color: 1

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 12338 Color: 18
Size: 2938 Color: 8

Bin 141: 20 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 14
Size: 2044 Color: 7
Size: 32 Color: 2

Bin 142: 20 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 15
Size: 2064 Color: 9

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 12
Size: 1736 Color: 5

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 9498 Color: 18
Size: 5777 Color: 2

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 10120 Color: 7
Size: 5152 Color: 4

Bin 146: 25 of cap free
Amount of items: 3
Items: 
Size: 8429 Color: 0
Size: 3702 Color: 15
Size: 3140 Color: 2

Bin 147: 25 of cap free
Amount of items: 3
Items: 
Size: 9600 Color: 8
Size: 4967 Color: 3
Size: 704 Color: 6

Bin 148: 25 of cap free
Amount of items: 2
Items: 
Size: 10238 Color: 4
Size: 5033 Color: 16

Bin 149: 25 of cap free
Amount of items: 2
Items: 
Size: 11924 Color: 1
Size: 3347 Color: 11

Bin 150: 25 of cap free
Amount of items: 2
Items: 
Size: 12660 Color: 13
Size: 2611 Color: 9

Bin 151: 25 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 3
Size: 2029 Color: 13
Size: 68 Color: 0

Bin 152: 26 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 19
Size: 4222 Color: 18
Size: 740 Color: 1

Bin 153: 26 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 18
Size: 3274 Color: 4

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 10287 Color: 8
Size: 4981 Color: 10

Bin 155: 28 of cap free
Amount of items: 3
Items: 
Size: 11663 Color: 11
Size: 3029 Color: 13
Size: 576 Color: 0

Bin 156: 32 of cap free
Amount of items: 3
Items: 
Size: 9168 Color: 11
Size: 5502 Color: 5
Size: 594 Color: 1

Bin 157: 32 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 9
Size: 3644 Color: 0
Size: 1404 Color: 2

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 12828 Color: 4
Size: 2436 Color: 9

Bin 159: 33 of cap free
Amount of items: 2
Items: 
Size: 10924 Color: 19
Size: 4339 Color: 17

Bin 160: 38 of cap free
Amount of items: 2
Items: 
Size: 11738 Color: 3
Size: 3520 Color: 11

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 7
Size: 2160 Color: 10

Bin 162: 42 of cap free
Amount of items: 2
Items: 
Size: 11814 Color: 3
Size: 3440 Color: 17

Bin 163: 45 of cap free
Amount of items: 3
Items: 
Size: 9337 Color: 14
Size: 5506 Color: 7
Size: 408 Color: 0

Bin 164: 45 of cap free
Amount of items: 3
Items: 
Size: 11806 Color: 18
Size: 3317 Color: 2
Size: 128 Color: 13

Bin 165: 49 of cap free
Amount of items: 2
Items: 
Size: 10230 Color: 6
Size: 5017 Color: 16

Bin 166: 52 of cap free
Amount of items: 3
Items: 
Size: 9628 Color: 5
Size: 5192 Color: 14
Size: 424 Color: 0

Bin 167: 52 of cap free
Amount of items: 2
Items: 
Size: 11388 Color: 4
Size: 3856 Color: 15

Bin 168: 52 of cap free
Amount of items: 2
Items: 
Size: 11809 Color: 5
Size: 3435 Color: 11

Bin 169: 55 of cap free
Amount of items: 3
Items: 
Size: 9048 Color: 1
Size: 5761 Color: 9
Size: 432 Color: 19

Bin 170: 55 of cap free
Amount of items: 2
Items: 
Size: 12697 Color: 9
Size: 2544 Color: 18

Bin 171: 59 of cap free
Amount of items: 2
Items: 
Size: 10850 Color: 2
Size: 4387 Color: 4

Bin 172: 60 of cap free
Amount of items: 2
Items: 
Size: 12814 Color: 7
Size: 2422 Color: 10

Bin 173: 62 of cap free
Amount of items: 2
Items: 
Size: 12990 Color: 18
Size: 2244 Color: 1

Bin 174: 63 of cap free
Amount of items: 2
Items: 
Size: 11400 Color: 15
Size: 3833 Color: 11

Bin 175: 76 of cap free
Amount of items: 2
Items: 
Size: 13068 Color: 3
Size: 2152 Color: 2

Bin 176: 77 of cap free
Amount of items: 2
Items: 
Size: 11888 Color: 7
Size: 3331 Color: 15

Bin 177: 78 of cap free
Amount of items: 3
Items: 
Size: 8365 Color: 0
Size: 6373 Color: 12
Size: 480 Color: 1

Bin 178: 78 of cap free
Amount of items: 2
Items: 
Size: 9552 Color: 14
Size: 5666 Color: 15

Bin 179: 80 of cap free
Amount of items: 3
Items: 
Size: 7720 Color: 2
Size: 6344 Color: 9
Size: 1152 Color: 17

Bin 180: 89 of cap free
Amount of items: 3
Items: 
Size: 9080 Color: 18
Size: 4973 Color: 6
Size: 1154 Color: 10

Bin 181: 103 of cap free
Amount of items: 2
Items: 
Size: 10166 Color: 8
Size: 5027 Color: 6

Bin 182: 104 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 3
Size: 6176 Color: 13

Bin 183: 108 of cap free
Amount of items: 2
Items: 
Size: 10372 Color: 12
Size: 4816 Color: 6

Bin 184: 121 of cap free
Amount of items: 3
Items: 
Size: 9321 Color: 8
Size: 5562 Color: 7
Size: 292 Color: 11

Bin 185: 141 of cap free
Amount of items: 2
Items: 
Size: 10824 Color: 3
Size: 4331 Color: 19

Bin 186: 142 of cap free
Amount of items: 11
Items: 
Size: 7652 Color: 11
Size: 944 Color: 15
Size: 944 Color: 12
Size: 928 Color: 13
Size: 896 Color: 6
Size: 896 Color: 1
Size: 866 Color: 13
Size: 576 Color: 19
Size: 544 Color: 7
Size: 532 Color: 7
Size: 376 Color: 5

Bin 187: 148 of cap free
Amount of items: 6
Items: 
Size: 7654 Color: 18
Size: 1770 Color: 14
Size: 1650 Color: 2
Size: 1468 Color: 1
Size: 1462 Color: 15
Size: 1144 Color: 18

Bin 188: 148 of cap free
Amount of items: 2
Items: 
Size: 11386 Color: 2
Size: 3762 Color: 5

Bin 189: 163 of cap free
Amount of items: 2
Items: 
Size: 10351 Color: 2
Size: 4782 Color: 13

Bin 190: 165 of cap free
Amount of items: 3
Items: 
Size: 10091 Color: 10
Size: 4528 Color: 14
Size: 512 Color: 5

Bin 191: 166 of cap free
Amount of items: 3
Items: 
Size: 7688 Color: 15
Size: 3736 Color: 0
Size: 3706 Color: 12

Bin 192: 176 of cap free
Amount of items: 2
Items: 
Size: 9880 Color: 12
Size: 5240 Color: 2

Bin 193: 179 of cap free
Amount of items: 2
Items: 
Size: 9277 Color: 0
Size: 5840 Color: 12

Bin 194: 214 of cap free
Amount of items: 2
Items: 
Size: 8698 Color: 5
Size: 6384 Color: 10

Bin 195: 216 of cap free
Amount of items: 2
Items: 
Size: 9872 Color: 15
Size: 5208 Color: 1

Bin 196: 220 of cap free
Amount of items: 3
Items: 
Size: 8304 Color: 13
Size: 5500 Color: 15
Size: 1272 Color: 6

Bin 197: 228 of cap free
Amount of items: 29
Items: 
Size: 844 Color: 10
Size: 840 Color: 1
Size: 834 Color: 0
Size: 832 Color: 15
Size: 830 Color: 6
Size: 824 Color: 0
Size: 816 Color: 4
Size: 738 Color: 16
Size: 580 Color: 9
Size: 576 Color: 14
Size: 512 Color: 7
Size: 496 Color: 16
Size: 496 Color: 3
Size: 488 Color: 13
Size: 416 Color: 19
Size: 396 Color: 8
Size: 396 Color: 7
Size: 394 Color: 8
Size: 384 Color: 15
Size: 384 Color: 15
Size: 384 Color: 2
Size: 352 Color: 13
Size: 352 Color: 8
Size: 352 Color: 5
Size: 336 Color: 6
Size: 328 Color: 19
Size: 320 Color: 5
Size: 288 Color: 11
Size: 280 Color: 11

Bin 198: 230 of cap free
Amount of items: 2
Items: 
Size: 8690 Color: 0
Size: 6376 Color: 11

Bin 199: 10112 of cap free
Amount of items: 18
Items: 
Size: 320 Color: 19
Size: 320 Color: 17
Size: 320 Color: 10
Size: 320 Color: 6
Size: 320 Color: 5
Size: 304 Color: 19
Size: 304 Color: 8
Size: 288 Color: 17
Size: 288 Color: 16
Size: 288 Color: 3
Size: 288 Color: 1
Size: 280 Color: 11
Size: 272 Color: 7
Size: 256 Color: 14
Size: 256 Color: 11
Size: 256 Color: 9
Size: 256 Color: 4
Size: 248 Color: 10

Total size: 3028608
Total free space: 15296

