Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 444622 Color: 0
Size: 288627 Color: 1
Size: 266752 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 357306 Color: 1
Size: 353046 Color: 1
Size: 289649 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 436364 Color: 0
Size: 300533 Color: 1
Size: 263104 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 462058 Color: 0
Size: 281699 Color: 0
Size: 256244 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 473737 Color: 0
Size: 269153 Color: 0
Size: 257111 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 408550 Color: 1
Size: 320733 Color: 0
Size: 270718 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 396866 Color: 0
Size: 320841 Color: 0
Size: 282294 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 400829 Color: 0
Size: 311447 Color: 0
Size: 287725 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 466775 Color: 0
Size: 269504 Color: 1
Size: 263722 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 377277 Color: 1
Size: 357255 Color: 1
Size: 265469 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 378043 Color: 1
Size: 360338 Color: 0
Size: 261620 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 368820 Color: 0
Size: 325159 Color: 0
Size: 306022 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 414886 Color: 0
Size: 330640 Color: 1
Size: 254475 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 370718 Color: 0
Size: 328506 Color: 1
Size: 300777 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 452880 Color: 1
Size: 283848 Color: 1
Size: 263273 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 388583 Color: 1
Size: 309380 Color: 0
Size: 302038 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 369142 Color: 1
Size: 318207 Color: 0
Size: 312652 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 452406 Color: 1
Size: 275948 Color: 0
Size: 271647 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 499582 Color: 1
Size: 250305 Color: 0
Size: 250114 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 458296 Color: 0
Size: 283913 Color: 1
Size: 257792 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 475133 Color: 1
Size: 267083 Color: 0
Size: 257785 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 406475 Color: 1
Size: 326803 Color: 0
Size: 266723 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 499081 Color: 0
Size: 250853 Color: 0
Size: 250067 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 347807 Color: 0
Size: 337097 Color: 1
Size: 315097 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 406639 Color: 0
Size: 324948 Color: 1
Size: 268414 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 460899 Color: 1
Size: 281870 Color: 1
Size: 257232 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 430235 Color: 1
Size: 319180 Color: 0
Size: 250586 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 460568 Color: 0
Size: 274054 Color: 1
Size: 265379 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 408848 Color: 0
Size: 338496 Color: 0
Size: 252657 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 491245 Color: 0
Size: 257864 Color: 1
Size: 250892 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 362934 Color: 0
Size: 318638 Color: 1
Size: 318429 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 430813 Color: 0
Size: 309201 Color: 1
Size: 259987 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 385217 Color: 1
Size: 354481 Color: 0
Size: 260303 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 458250 Color: 0
Size: 278526 Color: 1
Size: 263225 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 365756 Color: 0
Size: 324826 Color: 1
Size: 309419 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 376297 Color: 0
Size: 373032 Color: 1
Size: 250672 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 406293 Color: 0
Size: 329686 Color: 1
Size: 264022 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 393046 Color: 1
Size: 326573 Color: 1
Size: 280382 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 390896 Color: 0
Size: 327442 Color: 0
Size: 281663 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 481094 Color: 0
Size: 264209 Color: 0
Size: 254698 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 398552 Color: 1
Size: 306422 Color: 1
Size: 295027 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 474993 Color: 1
Size: 265041 Color: 1
Size: 259967 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 382798 Color: 1
Size: 343702 Color: 0
Size: 273501 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 347248 Color: 1
Size: 341839 Color: 0
Size: 310914 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 431110 Color: 1
Size: 301944 Color: 0
Size: 266947 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 429456 Color: 0
Size: 285434 Color: 0
Size: 285111 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 396999 Color: 1
Size: 312732 Color: 1
Size: 290270 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 374753 Color: 1
Size: 331447 Color: 0
Size: 293801 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 440836 Color: 1
Size: 295611 Color: 0
Size: 263554 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 387384 Color: 1
Size: 314046 Color: 1
Size: 298571 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 440174 Color: 1
Size: 280269 Color: 1
Size: 279558 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 429129 Color: 0
Size: 300672 Color: 0
Size: 270200 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 380083 Color: 1
Size: 310757 Color: 1
Size: 309161 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 434439 Color: 1
Size: 291099 Color: 0
Size: 274463 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 473974 Color: 0
Size: 270053 Color: 1
Size: 255974 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 389521 Color: 1
Size: 359727 Color: 0
Size: 250753 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 386406 Color: 0
Size: 328938 Color: 1
Size: 284657 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 401093 Color: 0
Size: 346890 Color: 0
Size: 252018 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 474131 Color: 1
Size: 268836 Color: 0
Size: 257034 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 373117 Color: 1
Size: 355168 Color: 0
Size: 271716 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 391023 Color: 0
Size: 341410 Color: 1
Size: 267568 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 385338 Color: 0
Size: 320200 Color: 1
Size: 294463 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 344395 Color: 1
Size: 328957 Color: 1
Size: 326649 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 426506 Color: 1
Size: 316321 Color: 1
Size: 257174 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 378325 Color: 1
Size: 355198 Color: 0
Size: 266478 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 382507 Color: 0
Size: 332208 Color: 0
Size: 285286 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 425942 Color: 0
Size: 296659 Color: 0
Size: 277400 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 419015 Color: 0
Size: 298488 Color: 0
Size: 282498 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 480420 Color: 0
Size: 261449 Color: 0
Size: 258132 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 369938 Color: 1
Size: 346222 Color: 0
Size: 283841 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 373195 Color: 0
Size: 339575 Color: 1
Size: 287231 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 364149 Color: 0
Size: 336011 Color: 1
Size: 299841 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 456218 Color: 1
Size: 290110 Color: 0
Size: 253673 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 400557 Color: 0
Size: 348171 Color: 1
Size: 251273 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 433186 Color: 0
Size: 289449 Color: 1
Size: 277366 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 416526 Color: 0
Size: 332487 Color: 0
Size: 250988 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 433437 Color: 1
Size: 301933 Color: 1
Size: 264631 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 360750 Color: 1
Size: 348387 Color: 0
Size: 290864 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 412388 Color: 0
Size: 302826 Color: 0
Size: 284787 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 395184 Color: 0
Size: 333935 Color: 1
Size: 270882 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 429060 Color: 1
Size: 310408 Color: 0
Size: 260533 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 438019 Color: 0
Size: 286065 Color: 0
Size: 275917 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 496323 Color: 1
Size: 252875 Color: 0
Size: 250803 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 493852 Color: 1
Size: 255969 Color: 0
Size: 250180 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 457999 Color: 1
Size: 289936 Color: 0
Size: 252066 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 392489 Color: 1
Size: 348681 Color: 0
Size: 258831 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 453724 Color: 1
Size: 289226 Color: 0
Size: 257051 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 397111 Color: 0
Size: 343572 Color: 1
Size: 259318 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 468934 Color: 1
Size: 276286 Color: 0
Size: 254781 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 470100 Color: 1
Size: 271866 Color: 0
Size: 258035 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 403944 Color: 1
Size: 314541 Color: 1
Size: 281516 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 405689 Color: 1
Size: 323363 Color: 1
Size: 270949 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 467262 Color: 1
Size: 276241 Color: 0
Size: 256498 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 374046 Color: 1
Size: 356947 Color: 0
Size: 269008 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 451623 Color: 0
Size: 276542 Color: 1
Size: 271836 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 387131 Color: 0
Size: 346647 Color: 0
Size: 266223 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 431938 Color: 0
Size: 310258 Color: 1
Size: 257805 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 470176 Color: 1
Size: 278780 Color: 0
Size: 251045 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 393831 Color: 0
Size: 333637 Color: 1
Size: 272533 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 426401 Color: 0
Size: 321013 Color: 1
Size: 252587 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 397931 Color: 0
Size: 318729 Color: 1
Size: 283341 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 430712 Color: 1
Size: 317907 Color: 0
Size: 251382 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 370626 Color: 0
Size: 321882 Color: 1
Size: 307493 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 375967 Color: 1
Size: 352564 Color: 0
Size: 271470 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 475333 Color: 1
Size: 270220 Color: 0
Size: 254448 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 465455 Color: 0
Size: 278030 Color: 1
Size: 256516 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 430081 Color: 1
Size: 291988 Color: 0
Size: 277932 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 430247 Color: 0
Size: 298407 Color: 0
Size: 271347 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 391482 Color: 1
Size: 334072 Color: 0
Size: 274447 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 489535 Color: 1
Size: 256830 Color: 1
Size: 253636 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 406749 Color: 0
Size: 321914 Color: 0
Size: 271338 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 0
Size: 308824 Color: 1
Size: 268980 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 435617 Color: 1
Size: 287830 Color: 0
Size: 276554 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 448165 Color: 1
Size: 297004 Color: 1
Size: 254832 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 380201 Color: 1
Size: 333616 Color: 0
Size: 286184 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 483872 Color: 1
Size: 260409 Color: 1
Size: 255720 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 399026 Color: 1
Size: 313875 Color: 1
Size: 287100 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 468331 Color: 1
Size: 267546 Color: 0
Size: 264124 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 411807 Color: 1
Size: 332100 Color: 0
Size: 256094 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 383114 Color: 1
Size: 324487 Color: 1
Size: 292400 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 448606 Color: 0
Size: 289140 Color: 0
Size: 262255 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 389026 Color: 0
Size: 331217 Color: 1
Size: 279758 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 361470 Color: 1
Size: 348417 Color: 1
Size: 290114 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 431808 Color: 0
Size: 306616 Color: 0
Size: 261577 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 461504 Color: 0
Size: 275635 Color: 0
Size: 262862 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 438191 Color: 0
Size: 291002 Color: 1
Size: 270808 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 486459 Color: 0
Size: 260420 Color: 0
Size: 253122 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 410288 Color: 0
Size: 328091 Color: 0
Size: 261622 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 459148 Color: 1
Size: 272113 Color: 1
Size: 268740 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 462871 Color: 1
Size: 284767 Color: 0
Size: 252363 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 431544 Color: 1
Size: 317839 Color: 1
Size: 250618 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 381489 Color: 1
Size: 352050 Color: 0
Size: 266462 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 480572 Color: 0
Size: 264377 Color: 1
Size: 255052 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 418540 Color: 0
Size: 291560 Color: 1
Size: 289901 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 365572 Color: 0
Size: 337474 Color: 1
Size: 296955 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 481157 Color: 1
Size: 259934 Color: 0
Size: 258910 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 435665 Color: 1
Size: 286798 Color: 0
Size: 277538 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 490137 Color: 0
Size: 258819 Color: 1
Size: 251045 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 340700 Color: 1
Size: 336201 Color: 0
Size: 323100 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 408773 Color: 1
Size: 335360 Color: 1
Size: 255868 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 373085 Color: 1
Size: 363092 Color: 1
Size: 263824 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 402259 Color: 0
Size: 301766 Color: 0
Size: 295976 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 350843 Color: 0
Size: 350780 Color: 1
Size: 298378 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 421104 Color: 1
Size: 306158 Color: 0
Size: 272739 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 378941 Color: 1
Size: 357026 Color: 1
Size: 264034 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 406394 Color: 1
Size: 301376 Color: 0
Size: 292231 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 360362 Color: 1
Size: 336499 Color: 1
Size: 303140 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 443100 Color: 1
Size: 279026 Color: 1
Size: 277875 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 398385 Color: 1
Size: 324057 Color: 0
Size: 277559 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 462611 Color: 1
Size: 278128 Color: 1
Size: 259262 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 387322 Color: 0
Size: 335547 Color: 0
Size: 277132 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 477896 Color: 1
Size: 270105 Color: 1
Size: 252000 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 410351 Color: 1
Size: 311038 Color: 0
Size: 278612 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 478342 Color: 1
Size: 263125 Color: 0
Size: 258534 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 489267 Color: 1
Size: 257045 Color: 0
Size: 253689 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 448898 Color: 0
Size: 285985 Color: 0
Size: 265118 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 376576 Color: 1
Size: 335446 Color: 0
Size: 287979 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 470972 Color: 0
Size: 277645 Color: 0
Size: 251384 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 493914 Color: 1
Size: 255595 Color: 0
Size: 250492 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 440069 Color: 0
Size: 309123 Color: 1
Size: 250809 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 394387 Color: 1
Size: 307168 Color: 1
Size: 298446 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 425931 Color: 0
Size: 303871 Color: 0
Size: 270199 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 348875 Color: 0
Size: 342181 Color: 0
Size: 308945 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 376012 Color: 1
Size: 361219 Color: 1
Size: 262770 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 427128 Color: 1
Size: 311919 Color: 0
Size: 260954 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 372066 Color: 0
Size: 325965 Color: 1
Size: 301970 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 353706 Color: 1
Size: 343875 Color: 1
Size: 302420 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 396677 Color: 0
Size: 302751 Color: 0
Size: 300573 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 373122 Color: 1
Size: 315874 Color: 0
Size: 311005 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 480159 Color: 1
Size: 268288 Color: 1
Size: 251554 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 417291 Color: 1
Size: 320718 Color: 0
Size: 261992 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 440084 Color: 1
Size: 280496 Color: 0
Size: 279421 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 458638 Color: 1
Size: 283937 Color: 1
Size: 257426 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 356475 Color: 1
Size: 354314 Color: 0
Size: 289212 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 443369 Color: 1
Size: 285422 Color: 1
Size: 271210 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 423352 Color: 1
Size: 288884 Color: 1
Size: 287765 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 478334 Color: 1
Size: 263812 Color: 0
Size: 257855 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 450719 Color: 1
Size: 292784 Color: 0
Size: 256498 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 346137 Color: 1
Size: 342857 Color: 0
Size: 311007 Color: 1

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 407629 Color: 0
Size: 334436 Color: 1
Size: 257936 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 441122 Color: 1
Size: 305950 Color: 1
Size: 252929 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 467450 Color: 1
Size: 270015 Color: 1
Size: 262536 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 352879 Color: 1
Size: 351206 Color: 1
Size: 295916 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 373748 Color: 0
Size: 327059 Color: 1
Size: 299194 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 364379 Color: 0
Size: 327814 Color: 0
Size: 307808 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 458440 Color: 0
Size: 270808 Color: 1
Size: 270753 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 391123 Color: 1
Size: 345530 Color: 0
Size: 263348 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 452146 Color: 1
Size: 292962 Color: 0
Size: 254893 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 391723 Color: 0
Size: 339276 Color: 1
Size: 269002 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 405314 Color: 1
Size: 303951 Color: 1
Size: 290736 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 373255 Color: 1
Size: 360859 Color: 0
Size: 265887 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 395364 Color: 0
Size: 321576 Color: 1
Size: 283061 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 381411 Color: 1
Size: 351211 Color: 0
Size: 267379 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 364796 Color: 1
Size: 335843 Color: 0
Size: 299362 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 356979 Color: 1
Size: 344644 Color: 1
Size: 298378 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 437679 Color: 1
Size: 290078 Color: 1
Size: 272244 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 490794 Color: 0
Size: 257531 Color: 1
Size: 251676 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 391811 Color: 1
Size: 322068 Color: 0
Size: 286122 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 469182 Color: 0
Size: 267679 Color: 0
Size: 263140 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 409685 Color: 0
Size: 303470 Color: 0
Size: 286846 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 471371 Color: 0
Size: 276868 Color: 1
Size: 251762 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 453519 Color: 0
Size: 289366 Color: 1
Size: 257116 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 474973 Color: 1
Size: 268485 Color: 0
Size: 256543 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 421842 Color: 0
Size: 289918 Color: 0
Size: 288241 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 457202 Color: 1
Size: 285199 Color: 0
Size: 257600 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 358648 Color: 0
Size: 322807 Color: 1
Size: 318546 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 373351 Color: 0
Size: 365631 Color: 1
Size: 261019 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 388674 Color: 1
Size: 336327 Color: 0
Size: 275000 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 444270 Color: 0
Size: 279597 Color: 1
Size: 276134 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 369555 Color: 0
Size: 365314 Color: 1
Size: 265132 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 406646 Color: 1
Size: 298470 Color: 1
Size: 294885 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 453104 Color: 1
Size: 292931 Color: 0
Size: 253966 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 451872 Color: 1
Size: 281744 Color: 1
Size: 266385 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 396144 Color: 1
Size: 303792 Color: 0
Size: 300065 Color: 1

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 446912 Color: 0
Size: 281477 Color: 0
Size: 271612 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 400313 Color: 0
Size: 314990 Color: 0
Size: 284698 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 406133 Color: 0
Size: 329142 Color: 0
Size: 264726 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 480139 Color: 1
Size: 261538 Color: 0
Size: 258324 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 436468 Color: 1
Size: 303028 Color: 1
Size: 260505 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 495290 Color: 0
Size: 254404 Color: 1
Size: 250307 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 457004 Color: 0
Size: 276601 Color: 0
Size: 266396 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 379846 Color: 1
Size: 352550 Color: 1
Size: 267605 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 453092 Color: 1
Size: 290381 Color: 0
Size: 256528 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 463207 Color: 1
Size: 281016 Color: 0
Size: 255778 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 396464 Color: 1
Size: 308941 Color: 0
Size: 294596 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 442715 Color: 0
Size: 286734 Color: 1
Size: 270552 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 495935 Color: 0
Size: 254033 Color: 1
Size: 250033 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 462400 Color: 1
Size: 284475 Color: 1
Size: 253126 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 441243 Color: 1
Size: 304265 Color: 0
Size: 254493 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 471466 Color: 1
Size: 274347 Color: 0
Size: 254188 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 432005 Color: 1
Size: 308554 Color: 1
Size: 259442 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 385269 Color: 1
Size: 313182 Color: 0
Size: 301550 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 375799 Color: 0
Size: 368637 Color: 0
Size: 255565 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 420050 Color: 0
Size: 311350 Color: 1
Size: 268601 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 471058 Color: 1
Size: 276062 Color: 0
Size: 252881 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 482672 Color: 1
Size: 263930 Color: 1
Size: 253399 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 490562 Color: 1
Size: 257694 Color: 0
Size: 251745 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 377856 Color: 0
Size: 361864 Color: 0
Size: 260281 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 491851 Color: 1
Size: 254127 Color: 0
Size: 254023 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 477727 Color: 1
Size: 266733 Color: 0
Size: 255541 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 420301 Color: 0
Size: 319535 Color: 1
Size: 260165 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 377739 Color: 0
Size: 341188 Color: 0
Size: 281074 Color: 1

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 471533 Color: 1
Size: 273294 Color: 0
Size: 255174 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 380431 Color: 1
Size: 358505 Color: 0
Size: 261065 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 395891 Color: 1
Size: 337983 Color: 1
Size: 266127 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 426924 Color: 1
Size: 322540 Color: 0
Size: 250537 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 423414 Color: 1
Size: 309109 Color: 0
Size: 267478 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 477401 Color: 0
Size: 262358 Color: 1
Size: 260242 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 362996 Color: 0
Size: 342798 Color: 0
Size: 294207 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 430348 Color: 1
Size: 286409 Color: 0
Size: 283244 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 393982 Color: 0
Size: 322810 Color: 0
Size: 283209 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 455944 Color: 0
Size: 276138 Color: 1
Size: 267919 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 496570 Color: 1
Size: 251764 Color: 0
Size: 251667 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 480890 Color: 0
Size: 262292 Color: 1
Size: 256819 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 405577 Color: 0
Size: 319284 Color: 0
Size: 275140 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 445458 Color: 0
Size: 284018 Color: 1
Size: 270525 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 372907 Color: 1
Size: 332785 Color: 1
Size: 294309 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 399752 Color: 0
Size: 334400 Color: 0
Size: 265849 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 451532 Color: 0
Size: 274509 Color: 1
Size: 273960 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 447159 Color: 0
Size: 291993 Color: 1
Size: 260849 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 440359 Color: 1
Size: 291378 Color: 0
Size: 268264 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 376403 Color: 0
Size: 350253 Color: 0
Size: 273345 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 451045 Color: 0
Size: 293139 Color: 1
Size: 255817 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 492174 Color: 0
Size: 254023 Color: 1
Size: 253804 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 383660 Color: 0
Size: 309152 Color: 0
Size: 307189 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 399265 Color: 0
Size: 321669 Color: 1
Size: 279067 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 480899 Color: 1
Size: 260798 Color: 1
Size: 258304 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 489976 Color: 1
Size: 255124 Color: 0
Size: 254901 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 398929 Color: 1
Size: 333679 Color: 0
Size: 267393 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 402802 Color: 1
Size: 344387 Color: 0
Size: 252812 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 365515 Color: 0
Size: 347453 Color: 1
Size: 287033 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 472915 Color: 0
Size: 267333 Color: 0
Size: 259753 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 368646 Color: 1
Size: 352936 Color: 0
Size: 278419 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 461964 Color: 1
Size: 281980 Color: 0
Size: 256057 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 464919 Color: 1
Size: 271962 Color: 0
Size: 263120 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 429834 Color: 1
Size: 302983 Color: 1
Size: 267184 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 364685 Color: 0
Size: 328874 Color: 1
Size: 306442 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 383680 Color: 0
Size: 331574 Color: 0
Size: 284747 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 451715 Color: 0
Size: 295915 Color: 1
Size: 252371 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 381900 Color: 1
Size: 316538 Color: 0
Size: 301563 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 406660 Color: 1
Size: 325417 Color: 1
Size: 267924 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 454315 Color: 0
Size: 273862 Color: 1
Size: 271824 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 485811 Color: 1
Size: 257894 Color: 0
Size: 256296 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 481262 Color: 0
Size: 261043 Color: 1
Size: 257696 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 407396 Color: 1
Size: 308073 Color: 1
Size: 284532 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 440525 Color: 0
Size: 299141 Color: 0
Size: 260335 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 475668 Color: 0
Size: 270768 Color: 1
Size: 253565 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 385923 Color: 1
Size: 330379 Color: 1
Size: 283699 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 374516 Color: 1
Size: 350317 Color: 1
Size: 275168 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 419395 Color: 1
Size: 321718 Color: 1
Size: 258888 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 441084 Color: 0
Size: 286693 Color: 1
Size: 272224 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 368857 Color: 1
Size: 362342 Color: 0
Size: 268802 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 473622 Color: 1
Size: 274776 Color: 0
Size: 251603 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 447902 Color: 0
Size: 285475 Color: 1
Size: 266624 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 478226 Color: 0
Size: 266271 Color: 0
Size: 255504 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 421532 Color: 0
Size: 323023 Color: 0
Size: 255446 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 396217 Color: 1
Size: 310405 Color: 0
Size: 293379 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 431266 Color: 1
Size: 288053 Color: 0
Size: 280682 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 364657 Color: 0
Size: 341947 Color: 0
Size: 293397 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 362527 Color: 0
Size: 354301 Color: 0
Size: 283173 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 418485 Color: 1
Size: 294161 Color: 1
Size: 287355 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 426579 Color: 0
Size: 321238 Color: 1
Size: 252184 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 357121 Color: 0
Size: 343916 Color: 1
Size: 298964 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 478571 Color: 1
Size: 270889 Color: 0
Size: 250541 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 367982 Color: 0
Size: 346965 Color: 1
Size: 285054 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 419909 Color: 0
Size: 303534 Color: 0
Size: 276558 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 406226 Color: 0
Size: 309809 Color: 0
Size: 283966 Color: 1

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 338993 Color: 1
Size: 334060 Color: 0
Size: 326948 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 477166 Color: 1
Size: 272006 Color: 1
Size: 250829 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 465399 Color: 1
Size: 277568 Color: 0
Size: 257034 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 375598 Color: 1
Size: 334047 Color: 0
Size: 290356 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 403495 Color: 0
Size: 325604 Color: 1
Size: 270902 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 354951 Color: 1
Size: 326416 Color: 1
Size: 318634 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 368786 Color: 0
Size: 334106 Color: 1
Size: 297109 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 378288 Color: 1
Size: 322076 Color: 1
Size: 299637 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 397535 Color: 0
Size: 321487 Color: 1
Size: 280979 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 376462 Color: 1
Size: 349692 Color: 0
Size: 273847 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 423862 Color: 1
Size: 298683 Color: 1
Size: 277456 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 372342 Color: 1
Size: 371053 Color: 0
Size: 256606 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 438376 Color: 0
Size: 296202 Color: 1
Size: 265423 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 406967 Color: 1
Size: 326938 Color: 0
Size: 266096 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 371060 Color: 1
Size: 349412 Color: 0
Size: 279529 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 384999 Color: 0
Size: 307932 Color: 1
Size: 307070 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 461476 Color: 0
Size: 282818 Color: 1
Size: 255707 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 372378 Color: 1
Size: 353889 Color: 1
Size: 273734 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 409386 Color: 1
Size: 333907 Color: 0
Size: 256708 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 374323 Color: 0
Size: 336979 Color: 0
Size: 288699 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 426827 Color: 0
Size: 305412 Color: 0
Size: 267762 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 427960 Color: 1
Size: 312639 Color: 0
Size: 259402 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 459824 Color: 1
Size: 270604 Color: 0
Size: 269573 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 476527 Color: 0
Size: 265346 Color: 0
Size: 258128 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 432732 Color: 1
Size: 288146 Color: 1
Size: 279123 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 435070 Color: 0
Size: 291465 Color: 0
Size: 273466 Color: 1

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 387493 Color: 1
Size: 346370 Color: 0
Size: 266138 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 491158 Color: 0
Size: 255463 Color: 0
Size: 253380 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 442245 Color: 0
Size: 293316 Color: 1
Size: 264440 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 443266 Color: 0
Size: 288212 Color: 0
Size: 268523 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 471902 Color: 0
Size: 277726 Color: 0
Size: 250373 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 476142 Color: 0
Size: 268976 Color: 1
Size: 254883 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 383621 Color: 0
Size: 345129 Color: 1
Size: 271251 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 363706 Color: 0
Size: 320791 Color: 1
Size: 315504 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 448073 Color: 0
Size: 298860 Color: 0
Size: 253068 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 377867 Color: 0
Size: 326473 Color: 1
Size: 295661 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 377928 Color: 1
Size: 353146 Color: 0
Size: 268927 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 371739 Color: 0
Size: 348615 Color: 0
Size: 279647 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 397847 Color: 1
Size: 326489 Color: 1
Size: 275665 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 404863 Color: 1
Size: 326349 Color: 0
Size: 268789 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 429496 Color: 0
Size: 308890 Color: 1
Size: 261615 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 459774 Color: 1
Size: 284354 Color: 0
Size: 255873 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 395859 Color: 1
Size: 317812 Color: 1
Size: 286330 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 488963 Color: 1
Size: 258416 Color: 0
Size: 252622 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 396150 Color: 0
Size: 321434 Color: 0
Size: 282417 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 406704 Color: 0
Size: 330375 Color: 0
Size: 262922 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 394625 Color: 0
Size: 351662 Color: 0
Size: 253714 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 466477 Color: 0
Size: 273552 Color: 1
Size: 259972 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 466311 Color: 0
Size: 278775 Color: 0
Size: 254915 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 380010 Color: 1
Size: 324261 Color: 0
Size: 295730 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 361215 Color: 1
Size: 352731 Color: 1
Size: 286055 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 419387 Color: 1
Size: 300893 Color: 0
Size: 279721 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 378363 Color: 1
Size: 317252 Color: 0
Size: 304386 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 491129 Color: 0
Size: 258175 Color: 1
Size: 250697 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 385414 Color: 1
Size: 311709 Color: 1
Size: 302878 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 409475 Color: 0
Size: 327171 Color: 1
Size: 263355 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 391984 Color: 1
Size: 306108 Color: 0
Size: 301909 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 461858 Color: 0
Size: 283938 Color: 1
Size: 254205 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 413517 Color: 1
Size: 329892 Color: 0
Size: 256592 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 372153 Color: 1
Size: 341614 Color: 0
Size: 286234 Color: 1

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 433120 Color: 0
Size: 286576 Color: 1
Size: 280305 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 347754 Color: 1
Size: 337708 Color: 0
Size: 314539 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 367997 Color: 0
Size: 352738 Color: 1
Size: 279266 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 440266 Color: 1
Size: 303338 Color: 1
Size: 256397 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 413245 Color: 1
Size: 326188 Color: 1
Size: 260568 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 389594 Color: 0
Size: 351267 Color: 1
Size: 259140 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 471493 Color: 0
Size: 267796 Color: 0
Size: 260712 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 438440 Color: 1
Size: 304689 Color: 0
Size: 256872 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 461388 Color: 0
Size: 285858 Color: 1
Size: 252755 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 370444 Color: 0
Size: 340756 Color: 1
Size: 288801 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 345798 Color: 1
Size: 331429 Color: 0
Size: 322774 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 387617 Color: 1
Size: 358127 Color: 1
Size: 254257 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 363052 Color: 1
Size: 344813 Color: 0
Size: 292136 Color: 1

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 492437 Color: 1
Size: 256538 Color: 1
Size: 251026 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 468682 Color: 0
Size: 275162 Color: 1
Size: 256157 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 404218 Color: 1
Size: 325255 Color: 0
Size: 270528 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 387670 Color: 1
Size: 344280 Color: 1
Size: 268051 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 374552 Color: 1
Size: 352727 Color: 0
Size: 272722 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 398975 Color: 0
Size: 337331 Color: 1
Size: 263695 Color: 1

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 376726 Color: 0
Size: 333226 Color: 0
Size: 290049 Color: 1

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 350943 Color: 1
Size: 338188 Color: 1
Size: 310870 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 490789 Color: 0
Size: 255865 Color: 1
Size: 253347 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 412362 Color: 1
Size: 333961 Color: 0
Size: 253678 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 391091 Color: 0
Size: 337772 Color: 1
Size: 271138 Color: 1

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 429422 Color: 1
Size: 314417 Color: 0
Size: 256162 Color: 1

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 430685 Color: 1
Size: 287123 Color: 1
Size: 282193 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 421957 Color: 1
Size: 300003 Color: 1
Size: 278041 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 395120 Color: 1
Size: 312695 Color: 1
Size: 292186 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 388755 Color: 0
Size: 357940 Color: 1
Size: 253306 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 366191 Color: 0
Size: 329874 Color: 1
Size: 303936 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 353933 Color: 0
Size: 343142 Color: 1
Size: 302926 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 412073 Color: 0
Size: 315538 Color: 1
Size: 272390 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 401189 Color: 0
Size: 328074 Color: 1
Size: 270738 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 348315 Color: 0
Size: 339406 Color: 1
Size: 312280 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 397704 Color: 0
Size: 318160 Color: 1
Size: 284137 Color: 1

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 442922 Color: 0
Size: 305162 Color: 1
Size: 251917 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 442524 Color: 1
Size: 283124 Color: 1
Size: 274353 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 376627 Color: 0
Size: 340640 Color: 1
Size: 282734 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 463047 Color: 0
Size: 283881 Color: 0
Size: 253073 Color: 1

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 449463 Color: 1
Size: 284198 Color: 0
Size: 266340 Color: 1

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 390530 Color: 1
Size: 335233 Color: 1
Size: 274238 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 434994 Color: 0
Size: 306192 Color: 0
Size: 258815 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 407041 Color: 1
Size: 315072 Color: 0
Size: 277888 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 377768 Color: 1
Size: 320845 Color: 0
Size: 301388 Color: 1

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 377554 Color: 0
Size: 340905 Color: 0
Size: 281542 Color: 1

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 372485 Color: 0
Size: 364754 Color: 0
Size: 262762 Color: 1

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 392655 Color: 1
Size: 342100 Color: 0
Size: 265246 Color: 1

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 437443 Color: 0
Size: 290297 Color: 0
Size: 272261 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 463124 Color: 0
Size: 282709 Color: 0
Size: 254168 Color: 1

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 400105 Color: 0
Size: 349746 Color: 1
Size: 250150 Color: 1

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 400766 Color: 1
Size: 341257 Color: 0
Size: 257978 Color: 1

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 430234 Color: 0
Size: 308436 Color: 1
Size: 261331 Color: 1

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 490562 Color: 1
Size: 254862 Color: 0
Size: 254577 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 454871 Color: 0
Size: 294960 Color: 1
Size: 250170 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 373961 Color: 1
Size: 329444 Color: 0
Size: 296596 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 390646 Color: 1
Size: 337538 Color: 1
Size: 271817 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 417378 Color: 1
Size: 325837 Color: 0
Size: 256786 Color: 1

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 486314 Color: 0
Size: 259549 Color: 0
Size: 254138 Color: 1

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 436435 Color: 0
Size: 300097 Color: 1
Size: 263469 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 435114 Color: 1
Size: 298127 Color: 1
Size: 266760 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 495714 Color: 1
Size: 253715 Color: 1
Size: 250572 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 458285 Color: 1
Size: 283456 Color: 0
Size: 258260 Color: 1

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 421760 Color: 1
Size: 298203 Color: 1
Size: 280038 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 395042 Color: 1
Size: 341939 Color: 0
Size: 263020 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 450770 Color: 0
Size: 279540 Color: 0
Size: 269691 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 369358 Color: 0
Size: 322501 Color: 1
Size: 308142 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 395513 Color: 0
Size: 335342 Color: 1
Size: 269146 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 490092 Color: 0
Size: 255209 Color: 1
Size: 254700 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 424070 Color: 1
Size: 295991 Color: 0
Size: 279940 Color: 1

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 464302 Color: 1
Size: 275404 Color: 0
Size: 260295 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 359889 Color: 1
Size: 335874 Color: 0
Size: 304238 Color: 1

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 444199 Color: 1
Size: 278403 Color: 1
Size: 277399 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 442605 Color: 1
Size: 280370 Color: 1
Size: 277026 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 460069 Color: 1
Size: 284604 Color: 1
Size: 255328 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 435574 Color: 1
Size: 294141 Color: 1
Size: 270286 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 472811 Color: 0
Size: 270286 Color: 1
Size: 256904 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 368961 Color: 1
Size: 337839 Color: 0
Size: 293201 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 472453 Color: 0
Size: 265735 Color: 1
Size: 261813 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 366388 Color: 0
Size: 347846 Color: 0
Size: 285767 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 490911 Color: 0
Size: 256148 Color: 1
Size: 252942 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 422627 Color: 0
Size: 305738 Color: 0
Size: 271636 Color: 1

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 496194 Color: 1
Size: 252354 Color: 0
Size: 251453 Color: 1

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 401995 Color: 0
Size: 314037 Color: 0
Size: 283969 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 400258 Color: 0
Size: 328377 Color: 0
Size: 271366 Color: 1

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 361772 Color: 0
Size: 339746 Color: 1
Size: 298483 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 493132 Color: 1
Size: 255787 Color: 0
Size: 251082 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 452490 Color: 1
Size: 280271 Color: 0
Size: 267240 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 477836 Color: 0
Size: 270208 Color: 1
Size: 251957 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 430804 Color: 0
Size: 292565 Color: 0
Size: 276632 Color: 1

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 386613 Color: 0
Size: 311060 Color: 1
Size: 302328 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 381913 Color: 0
Size: 322434 Color: 1
Size: 295654 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 488719 Color: 0
Size: 260422 Color: 1
Size: 250860 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 477263 Color: 1
Size: 266845 Color: 0
Size: 255893 Color: 1

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 390065 Color: 1
Size: 307039 Color: 1
Size: 302897 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 409523 Color: 0
Size: 305302 Color: 1
Size: 285176 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 476555 Color: 1
Size: 267105 Color: 1
Size: 256341 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 440977 Color: 1
Size: 305792 Color: 0
Size: 253232 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 418923 Color: 1
Size: 295532 Color: 0
Size: 285546 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 380545 Color: 1
Size: 349135 Color: 0
Size: 270321 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 492214 Color: 0
Size: 256768 Color: 1
Size: 251019 Color: 1

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 446316 Color: 1
Size: 302035 Color: 0
Size: 251650 Color: 1

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 464495 Color: 0
Size: 279558 Color: 0
Size: 255948 Color: 1

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 357550 Color: 0
Size: 343368 Color: 1
Size: 299083 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 449288 Color: 1
Size: 281668 Color: 1
Size: 269045 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 479897 Color: 0
Size: 268897 Color: 1
Size: 251207 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 387350 Color: 1
Size: 352589 Color: 1
Size: 260062 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 389848 Color: 0
Size: 310408 Color: 1
Size: 299745 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 436052 Color: 1
Size: 298875 Color: 0
Size: 265074 Color: 1

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 394994 Color: 1
Size: 335773 Color: 1
Size: 269234 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 408832 Color: 0
Size: 304623 Color: 0
Size: 286546 Color: 1

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 402563 Color: 1
Size: 299733 Color: 1
Size: 297705 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 477946 Color: 0
Size: 261561 Color: 1
Size: 260494 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 410658 Color: 0
Size: 309380 Color: 1
Size: 279963 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 448342 Color: 1
Size: 281383 Color: 0
Size: 270276 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 477210 Color: 1
Size: 269600 Color: 0
Size: 253191 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 412085 Color: 0
Size: 314957 Color: 1
Size: 272959 Color: 1

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 485927 Color: 1
Size: 263453 Color: 0
Size: 250621 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 372341 Color: 1
Size: 339157 Color: 0
Size: 288503 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 376381 Color: 1
Size: 316590 Color: 0
Size: 307030 Color: 1

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 378474 Color: 0
Size: 327507 Color: 0
Size: 294020 Color: 1

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 367098 Color: 0
Size: 356786 Color: 1
Size: 276117 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 469399 Color: 1
Size: 275297 Color: 1
Size: 255305 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 423104 Color: 1
Size: 310487 Color: 0
Size: 266410 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 403560 Color: 1
Size: 319149 Color: 0
Size: 277292 Color: 1

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 403902 Color: 0
Size: 335664 Color: 1
Size: 260435 Color: 1

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 425926 Color: 1
Size: 321496 Color: 1
Size: 252579 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 482290 Color: 1
Size: 259515 Color: 0
Size: 258196 Color: 1

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 471238 Color: 0
Size: 275366 Color: 1
Size: 253397 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 395444 Color: 0
Size: 329627 Color: 0
Size: 274930 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 423302 Color: 1
Size: 323442 Color: 1
Size: 253257 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 430120 Color: 0
Size: 305753 Color: 1
Size: 264128 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 446095 Color: 0
Size: 281375 Color: 1
Size: 272531 Color: 1

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 381220 Color: 1
Size: 338437 Color: 0
Size: 280344 Color: 1

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 394590 Color: 0
Size: 313896 Color: 1
Size: 291515 Color: 1

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 405445 Color: 1
Size: 322784 Color: 0
Size: 271772 Color: 1

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 436994 Color: 1
Size: 301033 Color: 0
Size: 261974 Color: 1

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 442810 Color: 1
Size: 290042 Color: 0
Size: 267149 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 455504 Color: 1
Size: 293196 Color: 0
Size: 251301 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 365443 Color: 1
Size: 332383 Color: 0
Size: 302175 Color: 1

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 362816 Color: 1
Size: 329251 Color: 0
Size: 307934 Color: 1

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 378792 Color: 0
Size: 312700 Color: 0
Size: 308509 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 383101 Color: 1
Size: 313898 Color: 0
Size: 303002 Color: 1

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 484554 Color: 1
Size: 264815 Color: 0
Size: 250632 Color: 1

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 446497 Color: 0
Size: 291809 Color: 0
Size: 261695 Color: 1

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 447501 Color: 1
Size: 292401 Color: 1
Size: 260099 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 415985 Color: 0
Size: 297086 Color: 1
Size: 286930 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 438939 Color: 1
Size: 306404 Color: 0
Size: 254658 Color: 1

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 453972 Color: 1
Size: 289246 Color: 0
Size: 256783 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 429059 Color: 1
Size: 317736 Color: 0
Size: 253206 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 489920 Color: 0
Size: 257516 Color: 0
Size: 252565 Color: 1

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 368982 Color: 0
Size: 349059 Color: 1
Size: 281960 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 462077 Color: 1
Size: 271015 Color: 1
Size: 266909 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 404834 Color: 1
Size: 320904 Color: 1
Size: 274263 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 385644 Color: 1
Size: 360923 Color: 0
Size: 253434 Color: 1

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 449765 Color: 1
Size: 294720 Color: 0
Size: 255516 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 373181 Color: 0
Size: 317633 Color: 0
Size: 309187 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 394642 Color: 1
Size: 330454 Color: 1
Size: 274905 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 493409 Color: 0
Size: 253490 Color: 1
Size: 253102 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 480100 Color: 0
Size: 260032 Color: 0
Size: 259869 Color: 1

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 380947 Color: 0
Size: 337664 Color: 1
Size: 281390 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 375529 Color: 1
Size: 331945 Color: 0
Size: 292527 Color: 1

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 399548 Color: 1
Size: 326313 Color: 0
Size: 274140 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 470667 Color: 1
Size: 274963 Color: 1
Size: 254371 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 389838 Color: 1
Size: 316548 Color: 1
Size: 293615 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 471404 Color: 1
Size: 270826 Color: 1
Size: 257771 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 388163 Color: 0
Size: 345029 Color: 1
Size: 266809 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 473161 Color: 1
Size: 274856 Color: 1
Size: 251984 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 479733 Color: 0
Size: 260874 Color: 1
Size: 259394 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 364558 Color: 1
Size: 353175 Color: 0
Size: 282268 Color: 1

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 426608 Color: 0
Size: 304301 Color: 0
Size: 269092 Color: 1

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 439423 Color: 0
Size: 304195 Color: 0
Size: 256383 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 457907 Color: 1
Size: 290752 Color: 1
Size: 251342 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 452814 Color: 1
Size: 296610 Color: 0
Size: 250577 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 448287 Color: 0
Size: 287548 Color: 1
Size: 264166 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 379922 Color: 1
Size: 338688 Color: 0
Size: 281391 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 460999 Color: 0
Size: 287497 Color: 0
Size: 251505 Color: 1

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 439257 Color: 0
Size: 281936 Color: 1
Size: 278808 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 393470 Color: 0
Size: 311940 Color: 0
Size: 294591 Color: 1

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 427650 Color: 1
Size: 315341 Color: 1
Size: 257010 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 449900 Color: 0
Size: 284419 Color: 0
Size: 265682 Color: 1

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 393666 Color: 0
Size: 329802 Color: 1
Size: 276533 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 366326 Color: 1
Size: 365224 Color: 1
Size: 268451 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 419316 Color: 0
Size: 317323 Color: 1
Size: 263362 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 416078 Color: 1
Size: 298249 Color: 1
Size: 285674 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 492983 Color: 0
Size: 256885 Color: 0
Size: 250133 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 447281 Color: 0
Size: 280103 Color: 1
Size: 272617 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 465054 Color: 1
Size: 279375 Color: 1
Size: 255572 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 376973 Color: 1
Size: 356851 Color: 1
Size: 266177 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 462994 Color: 1
Size: 279046 Color: 0
Size: 257961 Color: 1

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 355580 Color: 0
Size: 334998 Color: 0
Size: 309423 Color: 1

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 362577 Color: 1
Size: 323229 Color: 0
Size: 314195 Color: 1

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 476716 Color: 1
Size: 268377 Color: 0
Size: 254908 Color: 1

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 434474 Color: 1
Size: 283507 Color: 1
Size: 282020 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 459018 Color: 1
Size: 286614 Color: 0
Size: 254369 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 401540 Color: 1
Size: 306703 Color: 1
Size: 291758 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 382477 Color: 0
Size: 352045 Color: 1
Size: 265479 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 498431 Color: 0
Size: 251391 Color: 1
Size: 250179 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 418004 Color: 1
Size: 327220 Color: 0
Size: 254777 Color: 1

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 472397 Color: 1
Size: 271453 Color: 0
Size: 256151 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 380740 Color: 0
Size: 346727 Color: 1
Size: 272534 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 413406 Color: 0
Size: 329348 Color: 1
Size: 257247 Color: 1

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 424918 Color: 0
Size: 291389 Color: 0
Size: 283694 Color: 1

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 407455 Color: 1
Size: 319396 Color: 0
Size: 273150 Color: 1

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 426080 Color: 0
Size: 314959 Color: 1
Size: 258962 Color: 1

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 350756 Color: 0
Size: 336983 Color: 1
Size: 312262 Color: 1

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 382424 Color: 0
Size: 323949 Color: 0
Size: 293628 Color: 1

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 369908 Color: 0
Size: 335207 Color: 1
Size: 294886 Color: 1

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 422932 Color: 1
Size: 318152 Color: 1
Size: 258917 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 457468 Color: 1
Size: 274422 Color: 1
Size: 268111 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 463621 Color: 1
Size: 284822 Color: 0
Size: 251558 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 467095 Color: 0
Size: 271164 Color: 0
Size: 261742 Color: 1

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 433966 Color: 0
Size: 299654 Color: 0
Size: 266381 Color: 1

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 406742 Color: 1
Size: 334531 Color: 1
Size: 258728 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 392328 Color: 0
Size: 326550 Color: 0
Size: 281123 Color: 1

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 432233 Color: 1
Size: 312965 Color: 0
Size: 254803 Color: 1

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 373938 Color: 1
Size: 363236 Color: 1
Size: 262827 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 448272 Color: 0
Size: 299059 Color: 0
Size: 252670 Color: 1

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 472325 Color: 0
Size: 275650 Color: 1
Size: 252026 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 472907 Color: 0
Size: 272222 Color: 0
Size: 254872 Color: 1

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 391570 Color: 0
Size: 340763 Color: 1
Size: 267668 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 386131 Color: 1
Size: 359849 Color: 1
Size: 254021 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 388508 Color: 0
Size: 343355 Color: 1
Size: 268138 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 451195 Color: 0
Size: 276973 Color: 0
Size: 271833 Color: 1

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 379423 Color: 0
Size: 346206 Color: 1
Size: 274372 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 457619 Color: 0
Size: 284403 Color: 0
Size: 257979 Color: 1

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 385768 Color: 1
Size: 358268 Color: 1
Size: 255965 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 393111 Color: 0
Size: 345458 Color: 1
Size: 261432 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 386083 Color: 0
Size: 321252 Color: 1
Size: 292666 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 462816 Color: 1
Size: 280778 Color: 0
Size: 256407 Color: 1

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 431053 Color: 0
Size: 304000 Color: 1
Size: 264948 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 389954 Color: 1
Size: 346212 Color: 1
Size: 263835 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 437522 Color: 1
Size: 305669 Color: 1
Size: 256810 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 354782 Color: 0
Size: 325167 Color: 0
Size: 320052 Color: 1

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 358338 Color: 1
Size: 325376 Color: 1
Size: 316287 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 400393 Color: 1
Size: 323059 Color: 0
Size: 276549 Color: 1

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 494564 Color: 1
Size: 254572 Color: 0
Size: 250865 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 483073 Color: 1
Size: 263392 Color: 0
Size: 253536 Color: 1

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 437691 Color: 1
Size: 301000 Color: 1
Size: 261310 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 368471 Color: 1
Size: 337562 Color: 1
Size: 293968 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 385760 Color: 1
Size: 314447 Color: 0
Size: 299794 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 408031 Color: 0
Size: 310138 Color: 1
Size: 281832 Color: 1

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 435114 Color: 0
Size: 303858 Color: 0
Size: 261029 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 387322 Color: 0
Size: 353778 Color: 1
Size: 258901 Color: 1

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 489155 Color: 0
Size: 259862 Color: 1
Size: 250984 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 404306 Color: 0
Size: 323356 Color: 1
Size: 272339 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 407930 Color: 1
Size: 338517 Color: 1
Size: 253554 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 388776 Color: 0
Size: 311068 Color: 0
Size: 300157 Color: 1

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 383327 Color: 1
Size: 342425 Color: 0
Size: 274249 Color: 1

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 409091 Color: 0
Size: 324011 Color: 1
Size: 266899 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 470018 Color: 1
Size: 269307 Color: 0
Size: 260676 Color: 1

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 411661 Color: 0
Size: 328717 Color: 1
Size: 259623 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 443087 Color: 1
Size: 304102 Color: 0
Size: 252812 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 348441 Color: 0
Size: 330300 Color: 1
Size: 321260 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 434056 Color: 0
Size: 295978 Color: 1
Size: 269967 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 406689 Color: 0
Size: 311266 Color: 1
Size: 282046 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 383087 Color: 0
Size: 336430 Color: 0
Size: 280484 Color: 1

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 368476 Color: 0
Size: 351649 Color: 1
Size: 279876 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 436286 Color: 1
Size: 290168 Color: 1
Size: 273547 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 490831 Color: 1
Size: 255936 Color: 0
Size: 253234 Color: 1

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 401009 Color: 1
Size: 311848 Color: 0
Size: 287144 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 386076 Color: 0
Size: 361827 Color: 1
Size: 252098 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 453014 Color: 1
Size: 289754 Color: 1
Size: 257233 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 496874 Color: 1
Size: 251617 Color: 0
Size: 251510 Color: 1

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 437666 Color: 1
Size: 307328 Color: 1
Size: 255007 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 430399 Color: 0
Size: 301129 Color: 0
Size: 268473 Color: 1

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 377515 Color: 1
Size: 339061 Color: 0
Size: 283425 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 466297 Color: 1
Size: 273894 Color: 1
Size: 259810 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 412227 Color: 1
Size: 326389 Color: 0
Size: 261385 Color: 0

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 459112 Color: 1
Size: 279544 Color: 0
Size: 261345 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 406809 Color: 1
Size: 342571 Color: 1
Size: 250621 Color: 0

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 418889 Color: 1
Size: 327750 Color: 0
Size: 253362 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 362444 Color: 0
Size: 332037 Color: 1
Size: 305520 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 376425 Color: 0
Size: 354264 Color: 0
Size: 269312 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 356101 Color: 1
Size: 330809 Color: 1
Size: 313091 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 374566 Color: 0
Size: 352805 Color: 0
Size: 272630 Color: 1

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 392881 Color: 0
Size: 353408 Color: 0
Size: 253712 Color: 1

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 416455 Color: 1
Size: 313108 Color: 0
Size: 270438 Color: 1

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 466071 Color: 0
Size: 269932 Color: 0
Size: 263998 Color: 1

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 379274 Color: 1
Size: 340382 Color: 0
Size: 280345 Color: 1

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 386063 Color: 1
Size: 307923 Color: 1
Size: 306015 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 484579 Color: 0
Size: 261826 Color: 0
Size: 253596 Color: 1

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 379206 Color: 1
Size: 348258 Color: 0
Size: 272537 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 422014 Color: 0
Size: 305395 Color: 1
Size: 272592 Color: 0

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 493622 Color: 1
Size: 255102 Color: 0
Size: 251277 Color: 1

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 400056 Color: 1
Size: 303054 Color: 1
Size: 296891 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 453335 Color: 0
Size: 286665 Color: 1
Size: 260001 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 365259 Color: 1
Size: 332897 Color: 0
Size: 301845 Color: 1

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 424357 Color: 1
Size: 317935 Color: 0
Size: 257709 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 460120 Color: 0
Size: 284133 Color: 1
Size: 255748 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 391171 Color: 0
Size: 313621 Color: 0
Size: 295209 Color: 1

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 452132 Color: 1
Size: 281707 Color: 1
Size: 266162 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 358951 Color: 0
Size: 342630 Color: 0
Size: 298420 Color: 1

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 391020 Color: 1
Size: 322804 Color: 0
Size: 286177 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 405993 Color: 0
Size: 339354 Color: 1
Size: 254654 Color: 1

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 439391 Color: 0
Size: 282210 Color: 1
Size: 278400 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 468850 Color: 0
Size: 269743 Color: 1
Size: 261408 Color: 1

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 476025 Color: 0
Size: 273655 Color: 1
Size: 250321 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 402891 Color: 0
Size: 310900 Color: 1
Size: 286210 Color: 0

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 420340 Color: 0
Size: 293518 Color: 1
Size: 286143 Color: 1

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 454506 Color: 1
Size: 284531 Color: 1
Size: 260964 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 1
Size: 319909 Color: 0
Size: 316610 Color: 1

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 401181 Color: 1
Size: 324104 Color: 1
Size: 274716 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 444863 Color: 1
Size: 298826 Color: 0
Size: 256312 Color: 1

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 347499 Color: 1
Size: 328986 Color: 0
Size: 323516 Color: 1

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 349846 Color: 0
Size: 338245 Color: 1
Size: 311910 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 369638 Color: 1
Size: 325416 Color: 0
Size: 304947 Color: 1

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 473930 Color: 0
Size: 264292 Color: 0
Size: 261779 Color: 1

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 468948 Color: 1
Size: 278443 Color: 0
Size: 252610 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 392423 Color: 0
Size: 350094 Color: 1
Size: 257484 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 464585 Color: 1
Size: 283032 Color: 1
Size: 252384 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 366747 Color: 0
Size: 365027 Color: 0
Size: 268227 Color: 1

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 488866 Color: 0
Size: 260989 Color: 1
Size: 250146 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 381825 Color: 1
Size: 334099 Color: 0
Size: 284077 Color: 1

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 391780 Color: 0
Size: 310315 Color: 0
Size: 297906 Color: 1

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 441711 Color: 0
Size: 295759 Color: 0
Size: 262531 Color: 1

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 414289 Color: 1
Size: 319501 Color: 0
Size: 266211 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 489118 Color: 0
Size: 260878 Color: 0
Size: 250005 Color: 1

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 423527 Color: 1
Size: 317050 Color: 0
Size: 259424 Color: 1

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 430446 Color: 1
Size: 291870 Color: 1
Size: 277685 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 385683 Color: 0
Size: 316640 Color: 1
Size: 297678 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 402252 Color: 0
Size: 339450 Color: 1
Size: 258299 Color: 1

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 421758 Color: 0
Size: 321591 Color: 1
Size: 256652 Color: 1

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 400752 Color: 1
Size: 301138 Color: 1
Size: 298111 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 405059 Color: 0
Size: 337555 Color: 0
Size: 257387 Color: 1

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 426715 Color: 0
Size: 296548 Color: 1
Size: 276738 Color: 1

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 363773 Color: 0
Size: 333014 Color: 1
Size: 303214 Color: 1

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 387768 Color: 0
Size: 319401 Color: 1
Size: 292832 Color: 1

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 346782 Color: 1
Size: 328233 Color: 0
Size: 324986 Color: 1

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 469398 Color: 1
Size: 274027 Color: 0
Size: 256576 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 372522 Color: 0
Size: 353379 Color: 1
Size: 274100 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 497670 Color: 1
Size: 251725 Color: 0
Size: 250606 Color: 1

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 431984 Color: 1
Size: 290267 Color: 1
Size: 277750 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 472725 Color: 0
Size: 268406 Color: 0
Size: 258870 Color: 1

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 461994 Color: 0
Size: 285079 Color: 0
Size: 252928 Color: 1

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 389886 Color: 0
Size: 325567 Color: 1
Size: 284548 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 477789 Color: 0
Size: 268890 Color: 0
Size: 253322 Color: 1

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 411852 Color: 0
Size: 298954 Color: 0
Size: 289195 Color: 1

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 353267 Color: 0
Size: 352379 Color: 1
Size: 294355 Color: 1

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 365206 Color: 1
Size: 332450 Color: 1
Size: 302345 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 398258 Color: 1
Size: 311906 Color: 0
Size: 289837 Color: 1

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 366791 Color: 0
Size: 330848 Color: 1
Size: 302362 Color: 0

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 441275 Color: 1
Size: 300182 Color: 1
Size: 258544 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 434111 Color: 1
Size: 307655 Color: 0
Size: 258235 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 451719 Color: 0
Size: 277896 Color: 1
Size: 270386 Color: 1

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 365583 Color: 1
Size: 348141 Color: 1
Size: 286277 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 467259 Color: 0
Size: 272838 Color: 1
Size: 259904 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 419500 Color: 0
Size: 316909 Color: 0
Size: 263592 Color: 1

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 0
Size: 343069 Color: 1
Size: 272703 Color: 1

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 423503 Color: 0
Size: 325246 Color: 0
Size: 251252 Color: 1

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 478297 Color: 1
Size: 266386 Color: 0
Size: 255318 Color: 1

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 381633 Color: 0
Size: 338153 Color: 1
Size: 280215 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 362795 Color: 0
Size: 336733 Color: 1
Size: 300473 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 399432 Color: 0
Size: 323126 Color: 0
Size: 277443 Color: 1

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 364826 Color: 1
Size: 325916 Color: 0
Size: 309259 Color: 1

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 440315 Color: 0
Size: 282419 Color: 1
Size: 277267 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 485178 Color: 0
Size: 259253 Color: 0
Size: 255570 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 440668 Color: 0
Size: 302698 Color: 1
Size: 256635 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 386151 Color: 1
Size: 348985 Color: 0
Size: 264865 Color: 1

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 420417 Color: 0
Size: 311556 Color: 1
Size: 268028 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 488134 Color: 1
Size: 256828 Color: 0
Size: 255039 Color: 1

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 435531 Color: 0
Size: 312861 Color: 1
Size: 251609 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 441815 Color: 1
Size: 289582 Color: 0
Size: 268604 Color: 1

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 375703 Color: 0
Size: 335579 Color: 0
Size: 288719 Color: 1

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 485028 Color: 1
Size: 260909 Color: 1
Size: 254064 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 430030 Color: 0
Size: 317308 Color: 1
Size: 252663 Color: 1

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 378381 Color: 0
Size: 332457 Color: 0
Size: 289163 Color: 1

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 409179 Color: 1
Size: 318425 Color: 1
Size: 272397 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 383569 Color: 0
Size: 362148 Color: 1
Size: 254284 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 392456 Color: 0
Size: 349987 Color: 1
Size: 257558 Color: 1

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 444018 Color: 0
Size: 291680 Color: 1
Size: 264303 Color: 1

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 399668 Color: 1
Size: 323271 Color: 1
Size: 277062 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 459458 Color: 1
Size: 284318 Color: 0
Size: 256225 Color: 1

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 430658 Color: 0
Size: 318110 Color: 1
Size: 251233 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 480433 Color: 1
Size: 263893 Color: 1
Size: 255675 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 364314 Color: 1
Size: 358492 Color: 1
Size: 277195 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 466996 Color: 1
Size: 280005 Color: 1
Size: 253000 Color: 0

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 411809 Color: 0
Size: 327583 Color: 1
Size: 260609 Color: 1

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 418278 Color: 1
Size: 315857 Color: 0
Size: 265866 Color: 1

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 497224 Color: 0
Size: 251666 Color: 1
Size: 251111 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 450606 Color: 0
Size: 297065 Color: 1
Size: 252330 Color: 1

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 408522 Color: 0
Size: 304801 Color: 1
Size: 286678 Color: 1

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 406265 Color: 0
Size: 328901 Color: 1
Size: 264835 Color: 1

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 480376 Color: 1
Size: 261361 Color: 0
Size: 258264 Color: 1

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 371235 Color: 1
Size: 355299 Color: 1
Size: 273467 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 426192 Color: 0
Size: 319855 Color: 0
Size: 253954 Color: 1

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 400288 Color: 1
Size: 340113 Color: 0
Size: 259600 Color: 1

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 354716 Color: 0
Size: 336326 Color: 0
Size: 308959 Color: 1

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 410243 Color: 0
Size: 327182 Color: 1
Size: 262576 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 407121 Color: 1
Size: 327161 Color: 1
Size: 265719 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 376419 Color: 0
Size: 353503 Color: 0
Size: 270079 Color: 1

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 417809 Color: 1
Size: 305435 Color: 0
Size: 276757 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 349954 Color: 0
Size: 333112 Color: 1
Size: 316935 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 382239 Color: 1
Size: 321978 Color: 0
Size: 295784 Color: 1

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 373676 Color: 0
Size: 351414 Color: 0
Size: 274911 Color: 1

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 363060 Color: 1
Size: 332264 Color: 0
Size: 304677 Color: 1

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 363091 Color: 1
Size: 357719 Color: 0
Size: 279191 Color: 1

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 457606 Color: 1
Size: 286064 Color: 1
Size: 256331 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 398350 Color: 0
Size: 322070 Color: 1
Size: 279581 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 370801 Color: 0
Size: 356047 Color: 0
Size: 273153 Color: 1

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 362504 Color: 0
Size: 323202 Color: 1
Size: 314295 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 454985 Color: 0
Size: 285900 Color: 1
Size: 259116 Color: 1

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 445766 Color: 0
Size: 281657 Color: 0
Size: 272578 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 363534 Color: 0
Size: 320484 Color: 0
Size: 315983 Color: 1

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 442780 Color: 0
Size: 298479 Color: 1
Size: 258742 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 401145 Color: 1
Size: 309405 Color: 1
Size: 289451 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 440513 Color: 1
Size: 285012 Color: 0
Size: 274476 Color: 1

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 402696 Color: 1
Size: 300048 Color: 0
Size: 297257 Color: 1

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 408997 Color: 1
Size: 323254 Color: 0
Size: 267750 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 399951 Color: 0
Size: 334363 Color: 1
Size: 265687 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 369737 Color: 1
Size: 324138 Color: 0
Size: 306126 Color: 1

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 395395 Color: 0
Size: 351608 Color: 1
Size: 252998 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 495327 Color: 0
Size: 253818 Color: 0
Size: 250856 Color: 1

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 377701 Color: 0
Size: 320021 Color: 1
Size: 302279 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 487178 Color: 0
Size: 258212 Color: 0
Size: 254611 Color: 1

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 477995 Color: 0
Size: 268837 Color: 0
Size: 253169 Color: 1

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 388528 Color: 1
Size: 323810 Color: 0
Size: 287663 Color: 1

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 431130 Color: 0
Size: 285992 Color: 0
Size: 282879 Color: 1

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 381195 Color: 1
Size: 347907 Color: 1
Size: 270899 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 364435 Color: 1
Size: 343450 Color: 0
Size: 292116 Color: 1

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 461084 Color: 1
Size: 276273 Color: 0
Size: 262644 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 384068 Color: 1
Size: 345984 Color: 0
Size: 269949 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 350684 Color: 1
Size: 332050 Color: 0
Size: 317267 Color: 1

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 423375 Color: 0
Size: 320274 Color: 1
Size: 256352 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 371184 Color: 0
Size: 339059 Color: 0
Size: 289758 Color: 1

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 358587 Color: 1
Size: 331666 Color: 0
Size: 309748 Color: 1

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 388476 Color: 0
Size: 318411 Color: 0
Size: 293114 Color: 1

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 383429 Color: 1
Size: 359725 Color: 0
Size: 256847 Color: 1

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 376228 Color: 0
Size: 366678 Color: 0
Size: 257095 Color: 1

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 421043 Color: 1
Size: 311558 Color: 0
Size: 267400 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 424203 Color: 0
Size: 304090 Color: 0
Size: 271708 Color: 1

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 425349 Color: 0
Size: 314755 Color: 1
Size: 259897 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 351007 Color: 0
Size: 338001 Color: 1
Size: 310993 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 367928 Color: 1
Size: 334220 Color: 0
Size: 297853 Color: 1

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 398077 Color: 1
Size: 320182 Color: 0
Size: 281742 Color: 0

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 379313 Color: 1
Size: 347043 Color: 0
Size: 273645 Color: 1

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 396547 Color: 1
Size: 337106 Color: 0
Size: 266348 Color: 1

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 345442 Color: 1
Size: 336646 Color: 0
Size: 317913 Color: 1

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 423899 Color: 1
Size: 290940 Color: 1
Size: 285162 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 491033 Color: 0
Size: 254631 Color: 0
Size: 254337 Color: 1

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 455734 Color: 0
Size: 285302 Color: 0
Size: 258965 Color: 1

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 359935 Color: 1
Size: 321711 Color: 0
Size: 318355 Color: 1

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 491016 Color: 0
Size: 258146 Color: 0
Size: 250839 Color: 1

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 378617 Color: 1
Size: 318162 Color: 0
Size: 303222 Color: 1

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 404959 Color: 0
Size: 325634 Color: 0
Size: 269408 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 426271 Color: 1
Size: 300757 Color: 1
Size: 272973 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 378869 Color: 0
Size: 311930 Color: 1
Size: 309202 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 382738 Color: 1
Size: 322801 Color: 0
Size: 294462 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 458619 Color: 1
Size: 276557 Color: 0
Size: 264825 Color: 1

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 449734 Color: 0
Size: 277052 Color: 0
Size: 273215 Color: 1

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 439729 Color: 0
Size: 290010 Color: 0
Size: 270262 Color: 1

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 434884 Color: 1
Size: 282735 Color: 0
Size: 282382 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 481752 Color: 0
Size: 263052 Color: 1
Size: 255197 Color: 1

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 401733 Color: 0
Size: 321404 Color: 1
Size: 276864 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 446066 Color: 1
Size: 277671 Color: 0
Size: 276264 Color: 1

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 376063 Color: 0
Size: 332891 Color: 0
Size: 291047 Color: 1

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 388202 Color: 0
Size: 350039 Color: 1
Size: 261760 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 463489 Color: 0
Size: 270405 Color: 1
Size: 266107 Color: 1

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 470392 Color: 0
Size: 276408 Color: 0
Size: 253201 Color: 1

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 384421 Color: 0
Size: 313561 Color: 0
Size: 302019 Color: 1

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 365222 Color: 1
Size: 358203 Color: 0
Size: 276576 Color: 1

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 364794 Color: 0
Size: 319231 Color: 1
Size: 315976 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 485461 Color: 1
Size: 260002 Color: 1
Size: 254538 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 382424 Color: 0
Size: 357223 Color: 1
Size: 260354 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 469440 Color: 0
Size: 276604 Color: 1
Size: 253957 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 465937 Color: 1
Size: 279647 Color: 0
Size: 254417 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 432165 Color: 0
Size: 284964 Color: 0
Size: 282872 Color: 1

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 453587 Color: 0
Size: 279920 Color: 1
Size: 266494 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 358878 Color: 1
Size: 335135 Color: 0
Size: 305988 Color: 1

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 369528 Color: 1
Size: 340108 Color: 0
Size: 290365 Color: 1

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 449443 Color: 1
Size: 280720 Color: 0
Size: 269838 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 445941 Color: 0
Size: 285102 Color: 1
Size: 268958 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 345462 Color: 0
Size: 339458 Color: 1
Size: 315081 Color: 1

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 378983 Color: 1
Size: 343171 Color: 1
Size: 277847 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 468495 Color: 1
Size: 270858 Color: 0
Size: 260648 Color: 1

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 376974 Color: 0
Size: 353405 Color: 1
Size: 269622 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 461269 Color: 0
Size: 283434 Color: 1
Size: 255298 Color: 1

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 437908 Color: 0
Size: 306067 Color: 0
Size: 256026 Color: 1

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 417431 Color: 0
Size: 304818 Color: 0
Size: 277752 Color: 1

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 482647 Color: 0
Size: 266793 Color: 1
Size: 250561 Color: 1

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 451633 Color: 1
Size: 294184 Color: 0
Size: 254184 Color: 1

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 498815 Color: 0
Size: 251073 Color: 0
Size: 250113 Color: 1

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 365861 Color: 0
Size: 336505 Color: 1
Size: 297635 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 371267 Color: 1
Size: 338282 Color: 1
Size: 290452 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 368904 Color: 1
Size: 347313 Color: 0
Size: 283784 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 390614 Color: 1
Size: 346233 Color: 1
Size: 263154 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 422418 Color: 0
Size: 312020 Color: 0
Size: 265563 Color: 1

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 423463 Color: 0
Size: 307017 Color: 1
Size: 269521 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 412835 Color: 0
Size: 328105 Color: 1
Size: 259061 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 404183 Color: 0
Size: 299924 Color: 0
Size: 295894 Color: 1

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 390872 Color: 1
Size: 335036 Color: 0
Size: 274093 Color: 1

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 373766 Color: 0
Size: 350754 Color: 1
Size: 275481 Color: 0

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 377221 Color: 1
Size: 363206 Color: 0
Size: 259574 Color: 1

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 463209 Color: 0
Size: 277085 Color: 1
Size: 259707 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 490080 Color: 1
Size: 259560 Color: 0
Size: 250361 Color: 1

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 446502 Color: 1
Size: 280291 Color: 1
Size: 273208 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 434018 Color: 1
Size: 315862 Color: 1
Size: 250121 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 431257 Color: 0
Size: 286206 Color: 1
Size: 282538 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 390203 Color: 0
Size: 308883 Color: 0
Size: 300915 Color: 1

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 353821 Color: 1
Size: 336225 Color: 0
Size: 309955 Color: 1

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 370513 Color: 1
Size: 354209 Color: 0
Size: 275279 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 409811 Color: 0
Size: 329630 Color: 1
Size: 260560 Color: 1

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 453124 Color: 0
Size: 288385 Color: 1
Size: 258492 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 420631 Color: 1
Size: 306622 Color: 1
Size: 272748 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 369154 Color: 1
Size: 330321 Color: 1
Size: 300526 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 377457 Color: 0
Size: 350734 Color: 0
Size: 271810 Color: 1

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 388766 Color: 0
Size: 326469 Color: 1
Size: 284766 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 406413 Color: 0
Size: 331840 Color: 0
Size: 261748 Color: 1

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 371821 Color: 1
Size: 331883 Color: 0
Size: 296297 Color: 1

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 372167 Color: 0
Size: 356564 Color: 0
Size: 271270 Color: 1

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 368359 Color: 1
Size: 338866 Color: 0
Size: 292776 Color: 1

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 407671 Color: 1
Size: 311886 Color: 0
Size: 280444 Color: 1

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 432467 Color: 0
Size: 297710 Color: 0
Size: 269824 Color: 1

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 374627 Color: 1
Size: 370684 Color: 1
Size: 254690 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 413149 Color: 1
Size: 330794 Color: 0
Size: 256058 Color: 1

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 418312 Color: 0
Size: 302309 Color: 0
Size: 279380 Color: 1

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 484724 Color: 0
Size: 260613 Color: 1
Size: 254664 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 421541 Color: 0
Size: 310389 Color: 1
Size: 268071 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 371920 Color: 0
Size: 349065 Color: 0
Size: 279016 Color: 1

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 345940 Color: 1
Size: 335541 Color: 1
Size: 318520 Color: 0

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 425442 Color: 1
Size: 294579 Color: 0
Size: 279980 Color: 1

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 454689 Color: 1
Size: 286668 Color: 1
Size: 258644 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 386419 Color: 1
Size: 312966 Color: 0
Size: 300616 Color: 1

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 354687 Color: 0
Size: 326934 Color: 1
Size: 318380 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 380264 Color: 0
Size: 353202 Color: 1
Size: 266535 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 413413 Color: 0
Size: 295219 Color: 1
Size: 291369 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 433356 Color: 1
Size: 300878 Color: 0
Size: 265767 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 362931 Color: 0
Size: 323945 Color: 0
Size: 313125 Color: 1

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 445960 Color: 1
Size: 299639 Color: 0
Size: 254402 Color: 0

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 354045 Color: 1
Size: 335419 Color: 0
Size: 310537 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 420317 Color: 0
Size: 326358 Color: 1
Size: 253326 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 351847 Color: 0
Size: 332045 Color: 1
Size: 316109 Color: 1

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 343121 Color: 1
Size: 338259 Color: 0
Size: 318621 Color: 1

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 393722 Color: 0
Size: 355432 Color: 1
Size: 250847 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 400352 Color: 0
Size: 330538 Color: 1
Size: 269111 Color: 1

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 413028 Color: 1
Size: 313471 Color: 1
Size: 273502 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 419321 Color: 1
Size: 312738 Color: 0
Size: 267942 Color: 1

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 368089 Color: 0
Size: 367524 Color: 1
Size: 264388 Color: 1

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 347227 Color: 1
Size: 332598 Color: 1
Size: 320176 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 433740 Color: 1
Size: 285002 Color: 0
Size: 281259 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 349869 Color: 0
Size: 348382 Color: 1
Size: 301750 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 345863 Color: 1
Size: 332218 Color: 1
Size: 321920 Color: 0

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 360485 Color: 1
Size: 336746 Color: 1
Size: 302770 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 443142 Color: 0
Size: 295785 Color: 1
Size: 261074 Color: 1

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 410645 Color: 1
Size: 310773 Color: 0
Size: 278583 Color: 1

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 499058 Color: 0
Size: 250780 Color: 1
Size: 250163 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 499196 Color: 0
Size: 250727 Color: 1
Size: 250078 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 350706 Color: 1
Size: 350112 Color: 0
Size: 299183 Color: 1

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 414975 Color: 0
Size: 304736 Color: 0
Size: 280290 Color: 1

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 375258 Color: 1
Size: 356473 Color: 1
Size: 268270 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 417244 Color: 0
Size: 306400 Color: 0
Size: 276357 Color: 1

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 462952 Color: 1
Size: 275322 Color: 1
Size: 261727 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 425844 Color: 0
Size: 293884 Color: 1
Size: 280273 Color: 1

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 362385 Color: 0
Size: 349236 Color: 0
Size: 288380 Color: 1

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 378467 Color: 1
Size: 363959 Color: 0
Size: 257575 Color: 1

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 458964 Color: 1
Size: 271157 Color: 1
Size: 269880 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 363565 Color: 1
Size: 320688 Color: 0
Size: 315748 Color: 1

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 374249 Color: 0
Size: 352017 Color: 0
Size: 273735 Color: 1

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 470127 Color: 1
Size: 265161 Color: 1
Size: 264713 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 335646 Color: 0
Size: 333474 Color: 1
Size: 330881 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 350933 Color: 1
Size: 345417 Color: 0
Size: 303651 Color: 1

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 429629 Color: 0
Size: 307308 Color: 1
Size: 263064 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 357248 Color: 0
Size: 351156 Color: 0
Size: 291597 Color: 1

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 362539 Color: 1
Size: 328359 Color: 1
Size: 309103 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 352214 Color: 0
Size: 326218 Color: 1
Size: 321569 Color: 1

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 427057 Color: 1
Size: 321749 Color: 1
Size: 251195 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 444742 Color: 1
Size: 291815 Color: 0
Size: 263444 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 359630 Color: 0
Size: 321329 Color: 1
Size: 319042 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 407941 Color: 0
Size: 331572 Color: 1
Size: 260488 Color: 1

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 413895 Color: 1
Size: 300808 Color: 0
Size: 285298 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 421820 Color: 0
Size: 309982 Color: 1
Size: 268199 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 351422 Color: 1
Size: 346547 Color: 0
Size: 302032 Color: 1

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 362055 Color: 0
Size: 352907 Color: 1
Size: 285039 Color: 1

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 361984 Color: 0
Size: 344994 Color: 1
Size: 293023 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 366614 Color: 1
Size: 348927 Color: 1
Size: 284460 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 358348 Color: 1
Size: 352042 Color: 0
Size: 289611 Color: 1

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 404943 Color: 0
Size: 312831 Color: 1
Size: 282227 Color: 1

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 421733 Color: 1
Size: 309326 Color: 1
Size: 268942 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 350248 Color: 0
Size: 348612 Color: 1
Size: 301141 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 375566 Color: 0
Size: 327430 Color: 1
Size: 297005 Color: 1

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 416966 Color: 0
Size: 309218 Color: 1
Size: 273817 Color: 1

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 385692 Color: 0
Size: 350747 Color: 0
Size: 263562 Color: 1

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 458609 Color: 1
Size: 283111 Color: 1
Size: 258281 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 498407 Color: 1
Size: 250865 Color: 1
Size: 250729 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 399639 Color: 1
Size: 335430 Color: 0
Size: 264932 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 478038 Color: 1
Size: 271221 Color: 0
Size: 250742 Color: 1

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 350672 Color: 1
Size: 340236 Color: 0
Size: 309093 Color: 1

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 472407 Color: 1
Size: 272776 Color: 1
Size: 254818 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 346676 Color: 1
Size: 346156 Color: 0
Size: 307169 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 490376 Color: 0
Size: 258338 Color: 0
Size: 251287 Color: 1

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 468582 Color: 0
Size: 271213 Color: 1
Size: 260206 Color: 1

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 495682 Color: 1
Size: 253284 Color: 0
Size: 251035 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 464496 Color: 1
Size: 281938 Color: 0
Size: 253567 Color: 1

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 374995 Color: 1
Size: 314402 Color: 0
Size: 310604 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 396446 Color: 1
Size: 351373 Color: 1
Size: 252182 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 486152 Color: 1
Size: 258522 Color: 0
Size: 255327 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 376536 Color: 1
Size: 339802 Color: 1
Size: 283663 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 359283 Color: 1
Size: 320965 Color: 0
Size: 319753 Color: 1

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 426312 Color: 1
Size: 316318 Color: 0
Size: 257371 Color: 1

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 462605 Color: 0
Size: 284600 Color: 0
Size: 252796 Color: 1

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 368314 Color: 1
Size: 355386 Color: 0
Size: 276301 Color: 1

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 357531 Color: 0
Size: 346718 Color: 0
Size: 295752 Color: 1

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 497792 Color: 1
Size: 251361 Color: 0
Size: 250848 Color: 1

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 363488 Color: 1
Size: 353592 Color: 0
Size: 282921 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 398708 Color: 1
Size: 309261 Color: 1
Size: 292032 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 344375 Color: 1
Size: 330311 Color: 0
Size: 325315 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 343546 Color: 1
Size: 330596 Color: 1
Size: 325859 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 494557 Color: 0
Size: 254442 Color: 0
Size: 251002 Color: 1

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 471731 Color: 0
Size: 275624 Color: 0
Size: 252646 Color: 1

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 433604 Color: 0
Size: 298647 Color: 1
Size: 267750 Color: 1

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 388427 Color: 0
Size: 337820 Color: 0
Size: 273754 Color: 1

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 477995 Color: 1
Size: 262540 Color: 1
Size: 259466 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 393037 Color: 0
Size: 346248 Color: 0
Size: 260716 Color: 1

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 463222 Color: 1
Size: 284451 Color: 0
Size: 252328 Color: 1

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 388310 Color: 1
Size: 308731 Color: 1
Size: 302960 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 423236 Color: 1
Size: 325748 Color: 0
Size: 251017 Color: 1

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 465441 Color: 1
Size: 272486 Color: 0
Size: 262074 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 454644 Color: 0
Size: 291425 Color: 1
Size: 253932 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 470694 Color: 1
Size: 266396 Color: 1
Size: 262911 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 459820 Color: 0
Size: 287979 Color: 1
Size: 252202 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 486830 Color: 1
Size: 257995 Color: 0
Size: 255176 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 465925 Color: 1
Size: 281149 Color: 0
Size: 252927 Color: 1

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 438702 Color: 1
Size: 285752 Color: 0
Size: 275547 Color: 1

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 418430 Color: 0
Size: 312695 Color: 1
Size: 268876 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 450936 Color: 1
Size: 295567 Color: 0
Size: 253498 Color: 1

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 356828 Color: 0
Size: 346160 Color: 1
Size: 297013 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 416769 Color: 0
Size: 325525 Color: 1
Size: 257707 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 464972 Color: 1
Size: 281950 Color: 0
Size: 253079 Color: 1

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 397518 Color: 0
Size: 310697 Color: 1
Size: 291786 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 410843 Color: 0
Size: 333716 Color: 0
Size: 255442 Color: 1

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 408627 Color: 0
Size: 302944 Color: 0
Size: 288430 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 409612 Color: 0
Size: 317636 Color: 0
Size: 272753 Color: 1

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 355833 Color: 0
Size: 341429 Color: 1
Size: 302739 Color: 1

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 412463 Color: 0
Size: 296150 Color: 1
Size: 291388 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 494245 Color: 0
Size: 255664 Color: 0
Size: 250092 Color: 1

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 467473 Color: 0
Size: 275969 Color: 1
Size: 256559 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 370272 Color: 1
Size: 355220 Color: 0
Size: 274509 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 447230 Color: 0
Size: 289320 Color: 1
Size: 263451 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 443373 Color: 1
Size: 278549 Color: 0
Size: 278079 Color: 1

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 481583 Color: 1
Size: 263992 Color: 1
Size: 254426 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 397447 Color: 0
Size: 305487 Color: 0
Size: 297067 Color: 1

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 478858 Color: 0
Size: 265599 Color: 1
Size: 255544 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 446276 Color: 0
Size: 291974 Color: 0
Size: 261751 Color: 1

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 467089 Color: 1
Size: 280443 Color: 1
Size: 252469 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 390382 Color: 1
Size: 354027 Color: 0
Size: 255592 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 405225 Color: 1
Size: 341594 Color: 0
Size: 253182 Color: 1

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 495282 Color: 1
Size: 254291 Color: 1
Size: 250428 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 358132 Color: 0
Size: 346256 Color: 1
Size: 295613 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 498809 Color: 1
Size: 250802 Color: 1
Size: 250390 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 425396 Color: 1
Size: 308850 Color: 1
Size: 265755 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 455394 Color: 1
Size: 275055 Color: 0
Size: 269552 Color: 1

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 356861 Color: 0
Size: 333493 Color: 1
Size: 309647 Color: 1

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 360113 Color: 1
Size: 345453 Color: 0
Size: 294435 Color: 0

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 354241 Color: 0
Size: 325940 Color: 1
Size: 319820 Color: 1

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 353513 Color: 1
Size: 332289 Color: 1
Size: 314199 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 362812 Color: 1
Size: 332446 Color: 0
Size: 304743 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 351895 Color: 0
Size: 326330 Color: 1
Size: 321776 Color: 1

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 347902 Color: 0
Size: 340901 Color: 1
Size: 311198 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 353651 Color: 0
Size: 331269 Color: 1
Size: 315081 Color: 1

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 351521 Color: 0
Size: 336531 Color: 1
Size: 311949 Color: 1

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 354019 Color: 0
Size: 332504 Color: 1
Size: 313478 Color: 1

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 353042 Color: 0
Size: 325115 Color: 1
Size: 321844 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 360303 Color: 1
Size: 322338 Color: 1
Size: 317360 Color: 0

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 360653 Color: 1
Size: 343729 Color: 0
Size: 295619 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 358140 Color: 0
Size: 337184 Color: 1
Size: 304677 Color: 1

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 357215 Color: 0
Size: 355501 Color: 1
Size: 287285 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 385128 Color: 1
Size: 359985 Color: 1
Size: 254888 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 356519 Color: 1
Size: 325764 Color: 0
Size: 317718 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 349143 Color: 0
Size: 343346 Color: 1
Size: 307512 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 351430 Color: 1
Size: 328763 Color: 0
Size: 319808 Color: 1

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 358822 Color: 1
Size: 341719 Color: 0
Size: 299460 Color: 1

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 353545 Color: 1
Size: 351095 Color: 1
Size: 295361 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 342060 Color: 1
Size: 339882 Color: 0
Size: 318059 Color: 1

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 361712 Color: 0
Size: 329562 Color: 1
Size: 308727 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 351769 Color: 0
Size: 333835 Color: 1
Size: 314397 Color: 1

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 352737 Color: 0
Size: 337143 Color: 0
Size: 310121 Color: 1

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 356753 Color: 1
Size: 335481 Color: 0
Size: 307767 Color: 0

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 352989 Color: 1
Size: 352183 Color: 0
Size: 294829 Color: 1

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 342168 Color: 1
Size: 336055 Color: 0
Size: 321778 Color: 1

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 346913 Color: 0
Size: 327522 Color: 1
Size: 325566 Color: 1

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 350216 Color: 1
Size: 337127 Color: 0
Size: 312658 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 351851 Color: 0
Size: 346841 Color: 0
Size: 301309 Color: 1

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 345751 Color: 1
Size: 329658 Color: 1
Size: 324592 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 345310 Color: 1
Size: 339606 Color: 0
Size: 315085 Color: 1

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 352483 Color: 0
Size: 352269 Color: 1
Size: 295249 Color: 1

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 349274 Color: 0
Size: 348568 Color: 1
Size: 302159 Color: 1

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 348810 Color: 1
Size: 341579 Color: 0
Size: 309612 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 349199 Color: 1
Size: 347977 Color: 0
Size: 302825 Color: 1

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 345654 Color: 1
Size: 331235 Color: 0
Size: 323112 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 349180 Color: 1
Size: 346924 Color: 0
Size: 303897 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 360223 Color: 0
Size: 333957 Color: 1
Size: 305821 Color: 1

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 343296 Color: 0
Size: 331717 Color: 1
Size: 324988 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 349337 Color: 1
Size: 342086 Color: 1
Size: 308578 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 348479 Color: 1
Size: 342567 Color: 0
Size: 308955 Color: 1

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 351417 Color: 1
Size: 341767 Color: 0
Size: 306817 Color: 1

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 348351 Color: 1
Size: 341094 Color: 0
Size: 310556 Color: 1

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 348777 Color: 0
Size: 336325 Color: 1
Size: 314899 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 344031 Color: 0
Size: 335156 Color: 1
Size: 320814 Color: 1

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 340818 Color: 0
Size: 339711 Color: 1
Size: 319472 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 343604 Color: 1
Size: 339849 Color: 0
Size: 316548 Color: 1

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 338940 Color: 0
Size: 337605 Color: 1
Size: 323456 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 347866 Color: 1
Size: 337651 Color: 0
Size: 314484 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 350232 Color: 1
Size: 335386 Color: 1
Size: 314383 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 345359 Color: 0
Size: 332312 Color: 1
Size: 322330 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 354477 Color: 1
Size: 338585 Color: 1
Size: 306939 Color: 0

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 356455 Color: 1
Size: 335810 Color: 1
Size: 307736 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 356239 Color: 0
Size: 329053 Color: 0
Size: 314709 Color: 1

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 356330 Color: 0
Size: 335630 Color: 1
Size: 308041 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 356698 Color: 0
Size: 335370 Color: 0
Size: 307933 Color: 1

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 356740 Color: 0
Size: 327888 Color: 0
Size: 315373 Color: 1

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 357153 Color: 1
Size: 344848 Color: 0
Size: 298000 Color: 1

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 357367 Color: 1
Size: 344669 Color: 0
Size: 297965 Color: 1

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 357774 Color: 1
Size: 335143 Color: 1
Size: 307084 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 358477 Color: 0
Size: 352718 Color: 1
Size: 288806 Color: 1

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 358928 Color: 1
Size: 324751 Color: 0
Size: 316322 Color: 1

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 358723 Color: 0
Size: 322739 Color: 1
Size: 318539 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 359528 Color: 1
Size: 341097 Color: 0
Size: 299376 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 359572 Color: 1
Size: 327999 Color: 1
Size: 312430 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 359776 Color: 1
Size: 320486 Color: 1
Size: 319739 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 359266 Color: 0
Size: 328916 Color: 1
Size: 311819 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 359552 Color: 0
Size: 338448 Color: 1
Size: 302001 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 359903 Color: 1
Size: 354893 Color: 0
Size: 285205 Color: 1

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 360102 Color: 0
Size: 335375 Color: 1
Size: 304524 Color: 1

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 360519 Color: 0
Size: 337474 Color: 0
Size: 302008 Color: 1

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 360868 Color: 1
Size: 329070 Color: 1
Size: 310063 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 360885 Color: 1
Size: 320206 Color: 0
Size: 318910 Color: 1

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 360887 Color: 1
Size: 330205 Color: 0
Size: 308909 Color: 1

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 360774 Color: 0
Size: 333643 Color: 1
Size: 305584 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 360970 Color: 1
Size: 360698 Color: 0
Size: 278333 Color: 1

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 361045 Color: 0
Size: 343852 Color: 1
Size: 295104 Color: 0

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 361355 Color: 1
Size: 328019 Color: 0
Size: 310627 Color: 1

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 361438 Color: 1
Size: 329400 Color: 0
Size: 309163 Color: 1

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 361324 Color: 0
Size: 334274 Color: 0
Size: 304403 Color: 1

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 361443 Color: 1
Size: 336143 Color: 1
Size: 302415 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 361418 Color: 0
Size: 351995 Color: 0
Size: 286588 Color: 1

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 361572 Color: 1
Size: 336032 Color: 0
Size: 302397 Color: 1

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 361582 Color: 1
Size: 332082 Color: 1
Size: 306337 Color: 0

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 361885 Color: 1
Size: 331511 Color: 0
Size: 306605 Color: 1

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 361941 Color: 1
Size: 330482 Color: 1
Size: 307578 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 362170 Color: 0
Size: 324475 Color: 1
Size: 313356 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 362224 Color: 1
Size: 353273 Color: 0
Size: 284504 Color: 1

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 362191 Color: 0
Size: 360635 Color: 0
Size: 277175 Color: 1

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 362286 Color: 1
Size: 349674 Color: 0
Size: 288041 Color: 1

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 362647 Color: 1
Size: 357349 Color: 0
Size: 280005 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 362529 Color: 0
Size: 320908 Color: 0
Size: 316564 Color: 1

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 362725 Color: 1
Size: 351480 Color: 1
Size: 285796 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 362718 Color: 0
Size: 325838 Color: 0
Size: 311445 Color: 1

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 362726 Color: 1
Size: 344057 Color: 0
Size: 293218 Color: 1

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 362816 Color: 1
Size: 360069 Color: 1
Size: 277116 Color: 0

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 363210 Color: 0
Size: 340039 Color: 0
Size: 296752 Color: 1

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 363248 Color: 0
Size: 328236 Color: 1
Size: 308517 Color: 0

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 363271 Color: 0
Size: 355535 Color: 1
Size: 281195 Color: 0

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 363307 Color: 0
Size: 331597 Color: 1
Size: 305097 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 363525 Color: 1
Size: 329897 Color: 1
Size: 306579 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 363785 Color: 0
Size: 361148 Color: 1
Size: 275068 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 363969 Color: 0
Size: 343962 Color: 1
Size: 292070 Color: 1

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 364110 Color: 0
Size: 337259 Color: 1
Size: 298632 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 363891 Color: 1
Size: 323045 Color: 0
Size: 313065 Color: 1

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 364229 Color: 0
Size: 319609 Color: 1
Size: 316163 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 364556 Color: 0
Size: 347891 Color: 0
Size: 287554 Color: 1

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 364149 Color: 1
Size: 340909 Color: 0
Size: 294943 Color: 1

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 364598 Color: 0
Size: 339737 Color: 0
Size: 295666 Color: 1

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 364621 Color: 0
Size: 324786 Color: 0
Size: 310594 Color: 1

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 364584 Color: 1
Size: 330147 Color: 0
Size: 305270 Color: 1

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 364906 Color: 1
Size: 357749 Color: 1
Size: 277346 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 364946 Color: 0
Size: 323247 Color: 1
Size: 311808 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 365031 Color: 1
Size: 332335 Color: 1
Size: 302635 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 365170 Color: 0
Size: 357922 Color: 0
Size: 276909 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 365128 Color: 1
Size: 336095 Color: 0
Size: 298778 Color: 1

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 365181 Color: 0
Size: 345668 Color: 1
Size: 289152 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 365198 Color: 1
Size: 358062 Color: 1
Size: 276741 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 365230 Color: 0
Size: 334284 Color: 1
Size: 300487 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 365207 Color: 1
Size: 318923 Color: 0
Size: 315871 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 365295 Color: 0
Size: 357208 Color: 1
Size: 277498 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 365208 Color: 1
Size: 325348 Color: 1
Size: 309445 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 365246 Color: 1
Size: 351969 Color: 0
Size: 282786 Color: 1

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 365388 Color: 0
Size: 361127 Color: 1
Size: 273486 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 365404 Color: 0
Size: 320592 Color: 1
Size: 314005 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 365340 Color: 1
Size: 326727 Color: 0
Size: 307934 Color: 1

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 365522 Color: 1
Size: 341737 Color: 0
Size: 292742 Color: 1

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 365784 Color: 0
Size: 357658 Color: 0
Size: 276559 Color: 1

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 365923 Color: 0
Size: 345485 Color: 1
Size: 288593 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 365874 Color: 1
Size: 351372 Color: 1
Size: 282755 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 365973 Color: 0
Size: 360784 Color: 1
Size: 273244 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 366105 Color: 0
Size: 330731 Color: 0
Size: 303165 Color: 1

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 366128 Color: 0
Size: 324462 Color: 1
Size: 309411 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 366379 Color: 0
Size: 352395 Color: 1
Size: 281227 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 366380 Color: 0
Size: 321869 Color: 1
Size: 311752 Color: 1

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 366238 Color: 1
Size: 338182 Color: 1
Size: 295581 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 366494 Color: 0
Size: 351778 Color: 0
Size: 281729 Color: 1

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 366524 Color: 0
Size: 351141 Color: 0
Size: 282336 Color: 1

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 366553 Color: 1
Size: 350098 Color: 1
Size: 283350 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 366613 Color: 0
Size: 348822 Color: 0
Size: 284566 Color: 1

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 366763 Color: 1
Size: 335338 Color: 1
Size: 297900 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 366618 Color: 0
Size: 337681 Color: 0
Size: 295702 Color: 1

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 366882 Color: 1
Size: 350925 Color: 1
Size: 282194 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 366915 Color: 1
Size: 322873 Color: 0
Size: 310213 Color: 1

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 366969 Color: 1
Size: 355479 Color: 0
Size: 277553 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 367054 Color: 1
Size: 319903 Color: 0
Size: 313044 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 367094 Color: 1
Size: 340175 Color: 1
Size: 292732 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 367094 Color: 1
Size: 326247 Color: 1
Size: 306660 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 367146 Color: 1
Size: 340260 Color: 1
Size: 292595 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 367056 Color: 0
Size: 339360 Color: 0
Size: 293585 Color: 1

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 367233 Color: 1
Size: 351777 Color: 0
Size: 280991 Color: 1

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 367255 Color: 1
Size: 352427 Color: 0
Size: 280319 Color: 1

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 367124 Color: 0
Size: 327275 Color: 1
Size: 305602 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 367300 Color: 1
Size: 316455 Color: 0
Size: 316246 Color: 1

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 367381 Color: 0
Size: 336212 Color: 0
Size: 296408 Color: 1

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 367474 Color: 1
Size: 328220 Color: 1
Size: 304307 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 367426 Color: 0
Size: 322099 Color: 0
Size: 310476 Color: 1

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 367811 Color: 1
Size: 338548 Color: 0
Size: 293642 Color: 1

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 367789 Color: 0
Size: 361044 Color: 1
Size: 271168 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 367853 Color: 1
Size: 332282 Color: 1
Size: 299866 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 367834 Color: 0
Size: 350602 Color: 0
Size: 281565 Color: 1

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 367839 Color: 0
Size: 339478 Color: 1
Size: 292684 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 368091 Color: 1
Size: 357104 Color: 0
Size: 274806 Color: 1

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 368139 Color: 1
Size: 339901 Color: 0
Size: 291961 Color: 1

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 368153 Color: 1
Size: 336986 Color: 1
Size: 294862 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 368054 Color: 0
Size: 316349 Color: 0
Size: 315598 Color: 1

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 368192 Color: 1
Size: 355310 Color: 1
Size: 276499 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 368086 Color: 0
Size: 333544 Color: 0
Size: 298371 Color: 1

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 368332 Color: 1
Size: 337344 Color: 1
Size: 294325 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 368291 Color: 0
Size: 354031 Color: 0
Size: 277679 Color: 1

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 368396 Color: 1
Size: 316211 Color: 0
Size: 315394 Color: 1

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 368351 Color: 0
Size: 328724 Color: 1
Size: 302926 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 368352 Color: 0
Size: 347285 Color: 1
Size: 284364 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 368474 Color: 1
Size: 326059 Color: 0
Size: 305468 Color: 1

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 368494 Color: 0
Size: 337838 Color: 1
Size: 293669 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 368605 Color: 0
Size: 368424 Color: 1
Size: 262972 Color: 1

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 368681 Color: 0
Size: 326852 Color: 1
Size: 304468 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 368738 Color: 0
Size: 319908 Color: 1
Size: 311355 Color: 1

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 368747 Color: 1
Size: 317247 Color: 1
Size: 314007 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 368865 Color: 1
Size: 343345 Color: 1
Size: 287791 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 368994 Color: 1
Size: 320888 Color: 0
Size: 310119 Color: 1

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 369141 Color: 1
Size: 367916 Color: 1
Size: 262944 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 369184 Color: 1
Size: 328314 Color: 0
Size: 302503 Color: 1

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 369059 Color: 0
Size: 349338 Color: 0
Size: 281604 Color: 1

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 369282 Color: 1
Size: 324907 Color: 0
Size: 305812 Color: 1

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 369421 Color: 1
Size: 357781 Color: 1
Size: 272799 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 369484 Color: 1
Size: 317256 Color: 0
Size: 313261 Color: 1

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 369520 Color: 0
Size: 317052 Color: 1
Size: 313429 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 369740 Color: 1
Size: 346972 Color: 1
Size: 283289 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 369931 Color: 0
Size: 351017 Color: 0
Size: 279053 Color: 1

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 369905 Color: 1
Size: 326547 Color: 0
Size: 303549 Color: 1

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 369989 Color: 0
Size: 335811 Color: 1
Size: 294201 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 370159 Color: 1
Size: 322414 Color: 0
Size: 307428 Color: 1

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 370090 Color: 0
Size: 351295 Color: 1
Size: 278616 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 370188 Color: 1
Size: 318430 Color: 1
Size: 311383 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 370341 Color: 0
Size: 355453 Color: 0
Size: 274207 Color: 1

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 370199 Color: 1
Size: 366212 Color: 1
Size: 263590 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 370425 Color: 0
Size: 317278 Color: 1
Size: 312298 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 370306 Color: 1
Size: 355733 Color: 1
Size: 273962 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 370464 Color: 0
Size: 359609 Color: 0
Size: 269928 Color: 1

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 370407 Color: 1
Size: 365571 Color: 1
Size: 264023 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 370555 Color: 0
Size: 330616 Color: 1
Size: 298830 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 370657 Color: 0
Size: 338994 Color: 1
Size: 290350 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 370924 Color: 0
Size: 347115 Color: 0
Size: 281962 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 370583 Color: 1
Size: 346686 Color: 0
Size: 282732 Color: 1

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 370663 Color: 1
Size: 319855 Color: 0
Size: 309483 Color: 1

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 370752 Color: 0
Size: 315940 Color: 0
Size: 313309 Color: 1

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 370789 Color: 1
Size: 364557 Color: 0
Size: 264655 Color: 1

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 370834 Color: 0
Size: 314751 Color: 1
Size: 314416 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 371010 Color: 0
Size: 336031 Color: 1
Size: 292960 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 370830 Color: 1
Size: 319891 Color: 0
Size: 309280 Color: 1

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 371089 Color: 1
Size: 337509 Color: 0
Size: 291403 Color: 1

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 371112 Color: 0
Size: 322297 Color: 0
Size: 306592 Color: 1

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 371157 Color: 1
Size: 344276 Color: 0
Size: 284568 Color: 1

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 371276 Color: 0
Size: 321026 Color: 0
Size: 307699 Color: 1

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 371207 Color: 1
Size: 362871 Color: 1
Size: 265923 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 371327 Color: 0
Size: 330140 Color: 0
Size: 298534 Color: 1

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 371477 Color: 0
Size: 323007 Color: 1
Size: 305517 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 371516 Color: 0
Size: 324236 Color: 0
Size: 304249 Color: 1

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 371497 Color: 1
Size: 341974 Color: 1
Size: 286530 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 371522 Color: 0
Size: 354080 Color: 0
Size: 274399 Color: 1

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 371500 Color: 1
Size: 365997 Color: 0
Size: 262504 Color: 1

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 371604 Color: 0
Size: 346931 Color: 0
Size: 281466 Color: 1

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 371670 Color: 1
Size: 345973 Color: 1
Size: 282358 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 371735 Color: 0
Size: 344729 Color: 0
Size: 283537 Color: 1

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 371820 Color: 1
Size: 325691 Color: 1
Size: 302490 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 371825 Color: 0
Size: 345286 Color: 1
Size: 282890 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 371837 Color: 0
Size: 362981 Color: 1
Size: 265183 Color: 0

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 371891 Color: 1
Size: 355989 Color: 1
Size: 272121 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 371878 Color: 0
Size: 361541 Color: 1
Size: 266582 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 371901 Color: 1
Size: 341226 Color: 0
Size: 286874 Color: 1

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 371929 Color: 0
Size: 350823 Color: 1
Size: 277249 Color: 1

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 371903 Color: 1
Size: 333219 Color: 0
Size: 294879 Color: 1

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 371971 Color: 0
Size: 344095 Color: 1
Size: 283935 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 371995 Color: 1
Size: 338969 Color: 0
Size: 289037 Color: 1

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 372035 Color: 1
Size: 324787 Color: 0
Size: 303179 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 372127 Color: 1
Size: 340576 Color: 1
Size: 287298 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 372317 Color: 1
Size: 348494 Color: 0
Size: 279190 Color: 1

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 372201 Color: 0
Size: 334195 Color: 1
Size: 293605 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 372418 Color: 1
Size: 362438 Color: 0
Size: 265145 Color: 1

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 372244 Color: 0
Size: 350962 Color: 1
Size: 276795 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 372654 Color: 1
Size: 350217 Color: 0
Size: 277130 Color: 1

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 372697 Color: 1
Size: 330447 Color: 0
Size: 296857 Color: 1

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 372692 Color: 0
Size: 358746 Color: 0
Size: 268563 Color: 1

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 373165 Color: 1
Size: 324811 Color: 1
Size: 302025 Color: 0

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 373507 Color: 0
Size: 328463 Color: 0
Size: 298031 Color: 1

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 373581 Color: 0
Size: 330129 Color: 1
Size: 296291 Color: 1

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 373606 Color: 0
Size: 350859 Color: 1
Size: 275536 Color: 1

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 373546 Color: 1
Size: 316443 Color: 1
Size: 310012 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 373613 Color: 1
Size: 314440 Color: 0
Size: 311948 Color: 1

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 373746 Color: 0
Size: 316211 Color: 0
Size: 310044 Color: 1

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 373731 Color: 1
Size: 343401 Color: 1
Size: 282869 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 373893 Color: 1
Size: 317574 Color: 0
Size: 308534 Color: 1

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 373757 Color: 0
Size: 362565 Color: 0
Size: 263679 Color: 1

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 374046 Color: 1
Size: 331962 Color: 0
Size: 293993 Color: 1

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 374002 Color: 0
Size: 336317 Color: 0
Size: 289682 Color: 1

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 374138 Color: 0
Size: 358861 Color: 1
Size: 267002 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 374089 Color: 1
Size: 318669 Color: 0
Size: 307243 Color: 1

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 374145 Color: 1
Size: 351156 Color: 1
Size: 274700 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 374180 Color: 1
Size: 342239 Color: 0
Size: 283582 Color: 1

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 374291 Color: 0
Size: 358762 Color: 1
Size: 266948 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 374328 Color: 1
Size: 352209 Color: 1
Size: 273464 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 374447 Color: 0
Size: 364787 Color: 0
Size: 260767 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 374468 Color: 1
Size: 355857 Color: 0
Size: 269676 Color: 1

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 374557 Color: 0
Size: 346582 Color: 0
Size: 278862 Color: 1

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 374559 Color: 0
Size: 316264 Color: 0
Size: 309178 Color: 1

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 374611 Color: 1
Size: 337893 Color: 0
Size: 287497 Color: 1

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 374590 Color: 0
Size: 360288 Color: 0
Size: 265123 Color: 1

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 374613 Color: 0
Size: 320225 Color: 1
Size: 305163 Color: 0

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 374656 Color: 0
Size: 329981 Color: 1
Size: 295364 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 374994 Color: 1
Size: 330015 Color: 1
Size: 294992 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 375058 Color: 1
Size: 360494 Color: 1
Size: 264449 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 374923 Color: 0
Size: 321864 Color: 0
Size: 303214 Color: 1

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 375089 Color: 1
Size: 372064 Color: 0
Size: 252848 Color: 1

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 375126 Color: 0
Size: 371979 Color: 0
Size: 252896 Color: 1

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 375233 Color: 0
Size: 364034 Color: 0
Size: 260734 Color: 1

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 375312 Color: 1
Size: 314201 Color: 1
Size: 310488 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 375287 Color: 0
Size: 346487 Color: 0
Size: 278227 Color: 1

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 375320 Color: 1
Size: 312981 Color: 1
Size: 311700 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 375342 Color: 0
Size: 358718 Color: 1
Size: 265941 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 375448 Color: 1
Size: 361578 Color: 1
Size: 262975 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 375401 Color: 0
Size: 343805 Color: 1
Size: 280795 Color: 0

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 375477 Color: 1
Size: 322377 Color: 0
Size: 302147 Color: 1

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 375471 Color: 0
Size: 353587 Color: 1
Size: 270943 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 375523 Color: 0
Size: 351527 Color: 1
Size: 272951 Color: 1

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 375560 Color: 0
Size: 343630 Color: 1
Size: 280811 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 375777 Color: 1
Size: 343094 Color: 0
Size: 281130 Color: 1

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 375713 Color: 0
Size: 315811 Color: 0
Size: 308477 Color: 1

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 375813 Color: 1
Size: 356579 Color: 1
Size: 267609 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 375926 Color: 1
Size: 330990 Color: 0
Size: 293085 Color: 1

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 375973 Color: 0
Size: 363886 Color: 0
Size: 260142 Color: 1

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 375992 Color: 0
Size: 332551 Color: 1
Size: 291458 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 376142 Color: 0
Size: 339583 Color: 0
Size: 284276 Color: 1

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 376128 Color: 1
Size: 339299 Color: 0
Size: 284574 Color: 1

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 376188 Color: 0
Size: 320145 Color: 0
Size: 303668 Color: 1

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 376171 Color: 1
Size: 352377 Color: 0
Size: 271453 Color: 1

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 376301 Color: 1
Size: 327344 Color: 1
Size: 296356 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 376370 Color: 1
Size: 318873 Color: 0
Size: 304758 Color: 1

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 376362 Color: 0
Size: 323955 Color: 1
Size: 299684 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 376422 Color: 1
Size: 339343 Color: 1
Size: 284236 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 376544 Color: 0
Size: 337741 Color: 1
Size: 285716 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 376511 Color: 1
Size: 351336 Color: 1
Size: 272154 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 376526 Color: 1
Size: 368344 Color: 0
Size: 255131 Color: 1

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 376753 Color: 0
Size: 367072 Color: 0
Size: 256176 Color: 1

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 376633 Color: 1
Size: 351293 Color: 1
Size: 272075 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 376919 Color: 0
Size: 314201 Color: 0
Size: 308881 Color: 1

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 376888 Color: 1
Size: 357913 Color: 1
Size: 265200 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 377128 Color: 1
Size: 330927 Color: 1
Size: 291946 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 377208 Color: 1
Size: 350253 Color: 1
Size: 272540 Color: 0

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 377189 Color: 0
Size: 314628 Color: 0
Size: 308184 Color: 1

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 377354 Color: 0
Size: 363889 Color: 1
Size: 258758 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 377360 Color: 1
Size: 366329 Color: 0
Size: 256312 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 377439 Color: 0
Size: 338957 Color: 1
Size: 283605 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 377505 Color: 1
Size: 318005 Color: 1
Size: 304491 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 377488 Color: 0
Size: 329629 Color: 1
Size: 292884 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 377643 Color: 1
Size: 360304 Color: 0
Size: 262054 Color: 1

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 377666 Color: 1
Size: 347599 Color: 1
Size: 274736 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 377677 Color: 1
Size: 340135 Color: 1
Size: 282189 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 377769 Color: 0
Size: 365947 Color: 0
Size: 256285 Color: 1

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 377794 Color: 0
Size: 369309 Color: 0
Size: 252898 Color: 1

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 377803 Color: 1
Size: 318608 Color: 0
Size: 303590 Color: 1

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 377986 Color: 0
Size: 336990 Color: 1
Size: 285025 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 378050 Color: 1
Size: 320062 Color: 1
Size: 301889 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 378008 Color: 0
Size: 355530 Color: 0
Size: 266463 Color: 1

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 378093 Color: 1
Size: 324885 Color: 0
Size: 297023 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 378241 Color: 1
Size: 313746 Color: 0
Size: 308014 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 378333 Color: 1
Size: 370162 Color: 1
Size: 251506 Color: 0

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 378383 Color: 1
Size: 357244 Color: 1
Size: 264374 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 378146 Color: 0
Size: 365780 Color: 1
Size: 256075 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 378386 Color: 1
Size: 343294 Color: 1
Size: 278321 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 378420 Color: 1
Size: 331081 Color: 0
Size: 290500 Color: 1

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 378449 Color: 0
Size: 346354 Color: 0
Size: 275198 Color: 1

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 378451 Color: 1
Size: 318154 Color: 0
Size: 303396 Color: 1

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 378541 Color: 1
Size: 316778 Color: 0
Size: 304682 Color: 1

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 378548 Color: 1
Size: 311989 Color: 0
Size: 309464 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 378690 Color: 0
Size: 332565 Color: 1
Size: 288746 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 381105 Color: 0
Size: 328357 Color: 0
Size: 290539 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 378750 Color: 1
Size: 315947 Color: 0
Size: 305304 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 378840 Color: 1
Size: 321825 Color: 1
Size: 299336 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 378857 Color: 1
Size: 368293 Color: 0
Size: 252851 Color: 1

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 378797 Color: 0
Size: 318341 Color: 0
Size: 302863 Color: 1

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 378859 Color: 0
Size: 335999 Color: 0
Size: 285143 Color: 1

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 378981 Color: 1
Size: 369293 Color: 0
Size: 251727 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 378917 Color: 0
Size: 355509 Color: 1
Size: 265575 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 379014 Color: 1
Size: 368435 Color: 0
Size: 252552 Color: 1

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 378962 Color: 0
Size: 343634 Color: 0
Size: 277405 Color: 1

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 379147 Color: 1
Size: 366118 Color: 0
Size: 254736 Color: 1

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 379252 Color: 1
Size: 361366 Color: 0
Size: 259383 Color: 1

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 379256 Color: 1
Size: 351517 Color: 0
Size: 269228 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 379264 Color: 1
Size: 324862 Color: 1
Size: 295875 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 379078 Color: 0
Size: 349838 Color: 1
Size: 271085 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 379174 Color: 0
Size: 319534 Color: 1
Size: 301293 Color: 0

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 379277 Color: 1
Size: 347663 Color: 1
Size: 273061 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 379318 Color: 0
Size: 313934 Color: 0
Size: 306749 Color: 1

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 379294 Color: 1
Size: 341926 Color: 0
Size: 278781 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 379459 Color: 0
Size: 350592 Color: 1
Size: 269950 Color: 1

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 379490 Color: 0
Size: 333037 Color: 1
Size: 287474 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 379357 Color: 1
Size: 336680 Color: 1
Size: 283964 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 379596 Color: 0
Size: 334727 Color: 1
Size: 285678 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 379533 Color: 1
Size: 342708 Color: 1
Size: 277760 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 379607 Color: 0
Size: 370111 Color: 1
Size: 250283 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 379658 Color: 1
Size: 333289 Color: 1
Size: 287054 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 379702 Color: 1
Size: 325480 Color: 0
Size: 294819 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 379706 Color: 1
Size: 339999 Color: 0
Size: 280296 Color: 1

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 379857 Color: 1
Size: 366070 Color: 0
Size: 254074 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 379874 Color: 1
Size: 353543 Color: 0
Size: 266584 Color: 1

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 379759 Color: 0
Size: 318825 Color: 0
Size: 301417 Color: 1

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 379938 Color: 1
Size: 337647 Color: 0
Size: 282416 Color: 1

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 380037 Color: 1
Size: 349684 Color: 0
Size: 270280 Color: 1

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 380051 Color: 1
Size: 321830 Color: 0
Size: 298120 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 380066 Color: 1
Size: 315836 Color: 0
Size: 304099 Color: 1

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 380075 Color: 1
Size: 350645 Color: 0
Size: 269281 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 380146 Color: 1
Size: 324632 Color: 0
Size: 295223 Color: 1

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 379887 Color: 0
Size: 351756 Color: 1
Size: 268358 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 380164 Color: 1
Size: 326182 Color: 1
Size: 293655 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 380094 Color: 0
Size: 334688 Color: 0
Size: 285219 Color: 1

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 380217 Color: 0
Size: 348595 Color: 1
Size: 271189 Color: 1

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 380322 Color: 0
Size: 320546 Color: 1
Size: 299133 Color: 1

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 380340 Color: 1
Size: 368809 Color: 0
Size: 250852 Color: 1

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 380439 Color: 0
Size: 319670 Color: 0
Size: 299892 Color: 1

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 380561 Color: 0
Size: 353887 Color: 0
Size: 265553 Color: 1

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 380438 Color: 1
Size: 341662 Color: 1
Size: 277901 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 380684 Color: 0
Size: 311004 Color: 1
Size: 308313 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 380706 Color: 0
Size: 344259 Color: 0
Size: 275036 Color: 1

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 380596 Color: 1
Size: 324746 Color: 0
Size: 294659 Color: 1

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 380721 Color: 0
Size: 315683 Color: 0
Size: 303597 Color: 1

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 380619 Color: 1
Size: 346037 Color: 0
Size: 273345 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 380672 Color: 1
Size: 335853 Color: 0
Size: 283476 Color: 1

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 380767 Color: 0
Size: 348250 Color: 0
Size: 270984 Color: 1

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 380899 Color: 0
Size: 310091 Color: 1
Size: 309011 Color: 1

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 380852 Color: 1
Size: 332597 Color: 1
Size: 286552 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 380975 Color: 0
Size: 357644 Color: 0
Size: 261382 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 380993 Color: 0
Size: 355697 Color: 1
Size: 263311 Color: 1

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 381085 Color: 0
Size: 312347 Color: 1
Size: 306569 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 381105 Color: 1
Size: 316328 Color: 1
Size: 302568 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 381139 Color: 0
Size: 341041 Color: 0
Size: 277821 Color: 1

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 381122 Color: 1
Size: 317317 Color: 1
Size: 301562 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 381168 Color: 0
Size: 338489 Color: 0
Size: 280344 Color: 1

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 381170 Color: 1
Size: 329226 Color: 0
Size: 289605 Color: 1

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 381174 Color: 0
Size: 359054 Color: 1
Size: 259773 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 381192 Color: 0
Size: 312230 Color: 1
Size: 306579 Color: 1

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 381350 Color: 0
Size: 327389 Color: 0
Size: 291262 Color: 1

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 381256 Color: 1
Size: 311630 Color: 1
Size: 307115 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 381369 Color: 0
Size: 343838 Color: 0
Size: 274794 Color: 1

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 381339 Color: 1
Size: 317657 Color: 1
Size: 301005 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 381474 Color: 0
Size: 326018 Color: 1
Size: 292509 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 381483 Color: 0
Size: 345647 Color: 0
Size: 272871 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 381529 Color: 1
Size: 327872 Color: 0
Size: 290600 Color: 1

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 381630 Color: 1
Size: 321690 Color: 1
Size: 296681 Color: 0

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 381742 Color: 1
Size: 358482 Color: 1
Size: 259777 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 381790 Color: 0
Size: 333953 Color: 0
Size: 284258 Color: 1

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 381816 Color: 0
Size: 337477 Color: 1
Size: 280708 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 381887 Color: 1
Size: 350659 Color: 1
Size: 267455 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 381870 Color: 0
Size: 352728 Color: 0
Size: 265403 Color: 1

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 381913 Color: 1
Size: 316402 Color: 0
Size: 301686 Color: 1

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 382115 Color: 0
Size: 359613 Color: 0
Size: 258273 Color: 1

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 382160 Color: 0
Size: 330350 Color: 1
Size: 287491 Color: 1

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 382255 Color: 0
Size: 333886 Color: 0
Size: 283860 Color: 1

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 382324 Color: 0
Size: 365086 Color: 1
Size: 252591 Color: 1

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 382367 Color: 1
Size: 326555 Color: 1
Size: 291079 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 382605 Color: 0
Size: 337773 Color: 0
Size: 279623 Color: 1

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 382716 Color: 0
Size: 355767 Color: 1
Size: 261518 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 382803 Color: 1
Size: 324439 Color: 1
Size: 292759 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 383052 Color: 1
Size: 335488 Color: 0
Size: 281461 Color: 0

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 383064 Color: 1
Size: 364543 Color: 0
Size: 252394 Color: 1

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 383124 Color: 0
Size: 319157 Color: 0
Size: 297720 Color: 1

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 383193 Color: 0
Size: 319246 Color: 1
Size: 297562 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 383217 Color: 1
Size: 315634 Color: 0
Size: 301150 Color: 1

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 383201 Color: 0
Size: 333077 Color: 1
Size: 283723 Color: 0

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 383312 Color: 1
Size: 326486 Color: 1
Size: 290203 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 383216 Color: 0
Size: 312528 Color: 0
Size: 304257 Color: 1

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 383375 Color: 0
Size: 317421 Color: 0
Size: 299205 Color: 1

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 383409 Color: 0
Size: 333724 Color: 1
Size: 282868 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 383443 Color: 0
Size: 325271 Color: 1
Size: 291287 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 383567 Color: 0
Size: 332133 Color: 1
Size: 284301 Color: 1

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 383366 Color: 1
Size: 319558 Color: 1
Size: 297077 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 383618 Color: 0
Size: 319931 Color: 1
Size: 296452 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 383459 Color: 1
Size: 320564 Color: 0
Size: 295978 Color: 1

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 383710 Color: 1
Size: 329207 Color: 1
Size: 287084 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 383634 Color: 0
Size: 341420 Color: 1
Size: 274947 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 383712 Color: 1
Size: 322663 Color: 1
Size: 293626 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 383737 Color: 1
Size: 339882 Color: 0
Size: 276382 Color: 1

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 383806 Color: 1
Size: 336047 Color: 0
Size: 280148 Color: 1

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 383765 Color: 0
Size: 348813 Color: 1
Size: 267423 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 383931 Color: 1
Size: 332482 Color: 0
Size: 283588 Color: 1

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 383966 Color: 1
Size: 340500 Color: 0
Size: 275535 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 384075 Color: 0
Size: 358978 Color: 1
Size: 256948 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 384087 Color: 1
Size: 317269 Color: 1
Size: 298645 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 384149 Color: 0
Size: 346794 Color: 1
Size: 269058 Color: 0

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 384156 Color: 1
Size: 347693 Color: 1
Size: 268152 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 384242 Color: 0
Size: 332636 Color: 0
Size: 283123 Color: 1

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 384410 Color: 0
Size: 345521 Color: 1
Size: 270070 Color: 1

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 384447 Color: 0
Size: 316229 Color: 1
Size: 299325 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 384523 Color: 0
Size: 348341 Color: 0
Size: 267137 Color: 1

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 384273 Color: 1
Size: 348791 Color: 1
Size: 266937 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 384573 Color: 0
Size: 331912 Color: 0
Size: 283516 Color: 1

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 384649 Color: 1
Size: 357090 Color: 0
Size: 258262 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 384745 Color: 1
Size: 343064 Color: 1
Size: 272192 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 384860 Color: 0
Size: 309639 Color: 1
Size: 305502 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 384759 Color: 1
Size: 338391 Color: 0
Size: 276851 Color: 1

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 384917 Color: 0
Size: 355936 Color: 1
Size: 259148 Color: 0

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 385313 Color: 1
Size: 351705 Color: 0
Size: 262983 Color: 1

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 385315 Color: 1
Size: 335828 Color: 0
Size: 278858 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 385330 Color: 1
Size: 342342 Color: 0
Size: 272329 Color: 1

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 385334 Color: 1
Size: 317023 Color: 0
Size: 297644 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 385434 Color: 1
Size: 362360 Color: 0
Size: 252207 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 385480 Color: 1
Size: 332979 Color: 0
Size: 281542 Color: 1

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 385500 Color: 1
Size: 318117 Color: 1
Size: 296384 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 385572 Color: 1
Size: 307678 Color: 0
Size: 306751 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 385622 Color: 1
Size: 358093 Color: 1
Size: 256286 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 385485 Color: 0
Size: 348245 Color: 0
Size: 266271 Color: 1

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 385661 Color: 1
Size: 335280 Color: 0
Size: 279060 Color: 1

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 385671 Color: 1
Size: 338658 Color: 1
Size: 275672 Color: 0

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 385718 Color: 0
Size: 341400 Color: 1
Size: 272883 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 385729 Color: 0
Size: 330426 Color: 1
Size: 283846 Color: 1

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 385710 Color: 1
Size: 308479 Color: 0
Size: 305812 Color: 1

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 385874 Color: 0
Size: 355138 Color: 0
Size: 258989 Color: 1

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 385925 Color: 0
Size: 310447 Color: 0
Size: 303629 Color: 1

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 386052 Color: 1
Size: 362560 Color: 0
Size: 251389 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 386072 Color: 1
Size: 351240 Color: 0
Size: 262689 Color: 0

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 386226 Color: 1
Size: 335454 Color: 0
Size: 278321 Color: 0

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 386376 Color: 1
Size: 353168 Color: 0
Size: 260457 Color: 1

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 386235 Color: 0
Size: 347044 Color: 0
Size: 266722 Color: 1

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 386472 Color: 1
Size: 307283 Color: 0
Size: 306246 Color: 1

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 386699 Color: 1
Size: 349028 Color: 0
Size: 264274 Color: 1

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 386570 Color: 0
Size: 348249 Color: 0
Size: 265182 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 386702 Color: 1
Size: 349066 Color: 1
Size: 264233 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 386718 Color: 1
Size: 359358 Color: 0
Size: 253925 Color: 1

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 386694 Color: 0
Size: 362889 Color: 0
Size: 250418 Color: 1

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 386767 Color: 0
Size: 317965 Color: 1
Size: 295269 Color: 1

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 386792 Color: 0
Size: 348967 Color: 0
Size: 264242 Color: 1

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 387008 Color: 0
Size: 328598 Color: 1
Size: 284395 Color: 1

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 340469 Color: 0
Size: 334596 Color: 1
Size: 324936 Color: 1

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 387032 Color: 1
Size: 312695 Color: 1
Size: 300274 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 387225 Color: 0
Size: 322755 Color: 1
Size: 290021 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 387059 Color: 1
Size: 360518 Color: 0
Size: 252424 Color: 1

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 387320 Color: 1
Size: 310549 Color: 0
Size: 302132 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 387306 Color: 0
Size: 336222 Color: 1
Size: 276473 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 387350 Color: 1
Size: 319255 Color: 0
Size: 293396 Color: 1

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 387424 Color: 0
Size: 321015 Color: 0
Size: 291562 Color: 1

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 387508 Color: 1
Size: 332107 Color: 1
Size: 280386 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 387575 Color: 0
Size: 358790 Color: 1
Size: 253636 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 387635 Color: 1
Size: 345817 Color: 0
Size: 266549 Color: 1

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 387615 Color: 0
Size: 318426 Color: 0
Size: 293960 Color: 1

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 387782 Color: 0
Size: 331640 Color: 0
Size: 280579 Color: 1

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 387790 Color: 0
Size: 362086 Color: 1
Size: 250125 Color: 1

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 387858 Color: 0
Size: 358027 Color: 1
Size: 254116 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 387875 Color: 0
Size: 335174 Color: 1
Size: 276952 Color: 1

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 387957 Color: 0
Size: 316212 Color: 0
Size: 295832 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 387979 Color: 1
Size: 337581 Color: 1
Size: 274441 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 388097 Color: 0
Size: 346356 Color: 1
Size: 265548 Color: 1

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 387994 Color: 1
Size: 340931 Color: 1
Size: 271076 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 388178 Color: 1
Size: 328215 Color: 1
Size: 283608 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 388356 Color: 1
Size: 346286 Color: 0
Size: 265359 Color: 1

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 388373 Color: 0
Size: 315958 Color: 0
Size: 295670 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 388479 Color: 1
Size: 306283 Color: 1
Size: 305239 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 388398 Color: 0
Size: 322086 Color: 0
Size: 289517 Color: 1

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 388480 Color: 1
Size: 342645 Color: 1
Size: 268876 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 388475 Color: 0
Size: 332550 Color: 1
Size: 278976 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 388633 Color: 1
Size: 338252 Color: 1
Size: 273116 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 388491 Color: 0
Size: 315999 Color: 0
Size: 295511 Color: 1

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 388762 Color: 1
Size: 325660 Color: 1
Size: 285579 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 388770 Color: 1
Size: 313711 Color: 0
Size: 297520 Color: 1

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 388820 Color: 0
Size: 332314 Color: 0
Size: 278867 Color: 1

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 388856 Color: 0
Size: 330865 Color: 1
Size: 280280 Color: 1

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 388864 Color: 0
Size: 358420 Color: 1
Size: 252717 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 388814 Color: 1
Size: 344145 Color: 1
Size: 267042 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 389066 Color: 0
Size: 357892 Color: 1
Size: 253043 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 388842 Color: 1
Size: 315839 Color: 1
Size: 295320 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 389067 Color: 0
Size: 320064 Color: 1
Size: 290870 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 389025 Color: 1
Size: 356710 Color: 1
Size: 254266 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 389140 Color: 1
Size: 352811 Color: 0
Size: 258050 Color: 0

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 389117 Color: 0
Size: 319146 Color: 1
Size: 291738 Color: 0

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 389278 Color: 1
Size: 356798 Color: 0
Size: 253925 Color: 1

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 389283 Color: 0
Size: 350904 Color: 0
Size: 259814 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 389323 Color: 1
Size: 327524 Color: 1
Size: 283154 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 389590 Color: 1
Size: 338448 Color: 0
Size: 271963 Color: 1

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 389799 Color: 1
Size: 317834 Color: 0
Size: 292368 Color: 1

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 389934 Color: 0
Size: 358349 Color: 1
Size: 251718 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 390001 Color: 0
Size: 310738 Color: 1
Size: 299262 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 390018 Color: 0
Size: 317973 Color: 1
Size: 292010 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 390115 Color: 0
Size: 347753 Color: 1
Size: 262133 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 390143 Color: 0
Size: 320113 Color: 1
Size: 289745 Color: 1

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 390241 Color: 1
Size: 356036 Color: 0
Size: 253724 Color: 1

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 390210 Color: 0
Size: 320544 Color: 1
Size: 289247 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 390260 Color: 0
Size: 335867 Color: 1
Size: 273874 Color: 1

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 390280 Color: 1
Size: 325487 Color: 0
Size: 284234 Color: 1

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 390383 Color: 1
Size: 327107 Color: 1
Size: 282511 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 390389 Color: 1
Size: 324386 Color: 0
Size: 285226 Color: 0

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 390570 Color: 1
Size: 330436 Color: 0
Size: 278995 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 390700 Color: 0
Size: 346873 Color: 1
Size: 262428 Color: 0

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 390627 Color: 1
Size: 314664 Color: 0
Size: 294710 Color: 1

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 390715 Color: 0
Size: 332177 Color: 1
Size: 277109 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 390725 Color: 0
Size: 307557 Color: 0
Size: 301719 Color: 1

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 390668 Color: 1
Size: 351595 Color: 0
Size: 257738 Color: 1

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 390744 Color: 0
Size: 329871 Color: 1
Size: 279386 Color: 0

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 390704 Color: 1
Size: 345250 Color: 0
Size: 264047 Color: 1

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 390756 Color: 0
Size: 322963 Color: 1
Size: 286282 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 390802 Color: 0
Size: 309818 Color: 0
Size: 299381 Color: 1

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 390777 Color: 0
Size: 331045 Color: 1
Size: 278179 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 390792 Color: 0
Size: 337746 Color: 1
Size: 271463 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 390886 Color: 1
Size: 326340 Color: 0
Size: 282775 Color: 1

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 390802 Color: 1
Size: 355856 Color: 1
Size: 253343 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 390923 Color: 1
Size: 317860 Color: 0
Size: 291218 Color: 1

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 390988 Color: 1
Size: 311035 Color: 0
Size: 297978 Color: 1

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 390963 Color: 0
Size: 321928 Color: 0
Size: 287110 Color: 1

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 391146 Color: 1
Size: 320272 Color: 0
Size: 288583 Color: 1

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 391168 Color: 0
Size: 310621 Color: 0
Size: 298212 Color: 1

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 391148 Color: 1
Size: 340785 Color: 0
Size: 268068 Color: 1

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 391172 Color: 1
Size: 329214 Color: 0
Size: 279615 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 391181 Color: 0
Size: 332904 Color: 1
Size: 275916 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 391392 Color: 0
Size: 304712 Color: 1
Size: 303897 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 391387 Color: 1
Size: 322168 Color: 1
Size: 286446 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 391619 Color: 0
Size: 347107 Color: 0
Size: 261275 Color: 1

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 391610 Color: 1
Size: 321094 Color: 1
Size: 287297 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 391627 Color: 0
Size: 312527 Color: 1
Size: 295847 Color: 0

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 391611 Color: 1
Size: 317641 Color: 1
Size: 290749 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 391874 Color: 1
Size: 338272 Color: 0
Size: 269855 Color: 1

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 391898 Color: 1
Size: 342493 Color: 1
Size: 265610 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 391943 Color: 1
Size: 325360 Color: 0
Size: 282698 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 391946 Color: 1
Size: 316423 Color: 1
Size: 291632 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 391764 Color: 0
Size: 326784 Color: 0
Size: 281453 Color: 1

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 391850 Color: 0
Size: 356834 Color: 0
Size: 251317 Color: 1

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 392020 Color: 1
Size: 335071 Color: 1
Size: 272910 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 392070 Color: 0
Size: 313839 Color: 1
Size: 294092 Color: 1

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 392056 Color: 1
Size: 343098 Color: 1
Size: 264847 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 0
Size: 312025 Color: 1
Size: 295822 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 392066 Color: 1
Size: 321595 Color: 0
Size: 286340 Color: 1

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 392191 Color: 0
Size: 307737 Color: 1
Size: 300073 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 392106 Color: 1
Size: 315091 Color: 0
Size: 292804 Color: 1

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 392255 Color: 0
Size: 312739 Color: 0
Size: 295007 Color: 1

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 392359 Color: 1
Size: 318377 Color: 1
Size: 289265 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 392328 Color: 0
Size: 325902 Color: 1
Size: 281771 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 392401 Color: 1
Size: 350549 Color: 1
Size: 257051 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 392392 Color: 0
Size: 329394 Color: 1
Size: 278215 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 392450 Color: 1
Size: 340490 Color: 0
Size: 267061 Color: 1

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 392495 Color: 0
Size: 321299 Color: 0
Size: 286207 Color: 1

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 392510 Color: 0
Size: 343479 Color: 0
Size: 264012 Color: 1

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 392691 Color: 1
Size: 307363 Color: 1
Size: 299947 Color: 0

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 392578 Color: 0
Size: 355549 Color: 0
Size: 251874 Color: 1

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 392738 Color: 1
Size: 339177 Color: 1
Size: 268086 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 392677 Color: 0
Size: 346945 Color: 0
Size: 260379 Color: 1

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 392746 Color: 1
Size: 347109 Color: 1
Size: 260146 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 392685 Color: 0
Size: 341036 Color: 1
Size: 266280 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 392755 Color: 1
Size: 330814 Color: 1
Size: 276432 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 392834 Color: 0
Size: 337453 Color: 0
Size: 269714 Color: 1

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 392882 Color: 1
Size: 328500 Color: 1
Size: 278619 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 392915 Color: 1
Size: 342773 Color: 1
Size: 264313 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 393001 Color: 1
Size: 320520 Color: 1
Size: 286480 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 393024 Color: 0
Size: 318314 Color: 0
Size: 288663 Color: 1

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 393031 Color: 1
Size: 342371 Color: 0
Size: 264599 Color: 1

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 393089 Color: 0
Size: 305981 Color: 1
Size: 300931 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 393067 Color: 1
Size: 353186 Color: 0
Size: 253748 Color: 1

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 393075 Color: 1
Size: 338951 Color: 1
Size: 267975 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 393211 Color: 1
Size: 340917 Color: 0
Size: 265873 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 393303 Color: 1
Size: 350540 Color: 0
Size: 256158 Color: 1

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 393277 Color: 0
Size: 338141 Color: 1
Size: 268583 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 393339 Color: 1
Size: 348999 Color: 0
Size: 257663 Color: 1

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 393432 Color: 1
Size: 339878 Color: 0
Size: 266691 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 393445 Color: 1
Size: 354531 Color: 1
Size: 252025 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 393511 Color: 1
Size: 314092 Color: 0
Size: 292398 Color: 1

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 393396 Color: 0
Size: 324878 Color: 0
Size: 281727 Color: 1

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 393576 Color: 1
Size: 353505 Color: 0
Size: 252920 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 393596 Color: 1
Size: 303561 Color: 0
Size: 302844 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 393603 Color: 1
Size: 344061 Color: 0
Size: 262337 Color: 1

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 393640 Color: 1
Size: 304997 Color: 1
Size: 301364 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 393716 Color: 0
Size: 327564 Color: 1
Size: 278721 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 393790 Color: 0
Size: 331362 Color: 0
Size: 274849 Color: 1

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 393719 Color: 1
Size: 340002 Color: 0
Size: 266280 Color: 1

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 393809 Color: 1
Size: 323945 Color: 1
Size: 282247 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 394041 Color: 1
Size: 352330 Color: 0
Size: 253630 Color: 1

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 394110 Color: 1
Size: 322067 Color: 0
Size: 283824 Color: 1

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 394195 Color: 1
Size: 319382 Color: 0
Size: 286424 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 394233 Color: 1
Size: 312182 Color: 0
Size: 293586 Color: 1

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 394263 Color: 1
Size: 339784 Color: 0
Size: 265954 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 394280 Color: 1
Size: 316801 Color: 0
Size: 288920 Color: 1

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 394246 Color: 0
Size: 340506 Color: 0
Size: 265249 Color: 1

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 394409 Color: 0
Size: 342468 Color: 0
Size: 263124 Color: 1

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 394500 Color: 0
Size: 310457 Color: 1
Size: 295044 Color: 1

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 394552 Color: 0
Size: 344611 Color: 1
Size: 260838 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 394600 Color: 0
Size: 314070 Color: 1
Size: 291331 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 394776 Color: 1
Size: 345996 Color: 1
Size: 259229 Color: 0

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 394786 Color: 1
Size: 315946 Color: 0
Size: 289269 Color: 1

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 394770 Color: 0
Size: 336165 Color: 0
Size: 269066 Color: 1

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 394793 Color: 1
Size: 335734 Color: 0
Size: 269474 Color: 1

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 394787 Color: 0
Size: 342317 Color: 1
Size: 262897 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 394932 Color: 1
Size: 304385 Color: 0
Size: 300684 Color: 1

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 394992 Color: 0
Size: 337878 Color: 1
Size: 267131 Color: 1

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 395130 Color: 1
Size: 350046 Color: 1
Size: 254825 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 395239 Color: 0
Size: 332841 Color: 1
Size: 271921 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 395198 Color: 1
Size: 344537 Color: 1
Size: 260266 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 395253 Color: 0
Size: 306068 Color: 0
Size: 298680 Color: 1

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 395362 Color: 1
Size: 325967 Color: 1
Size: 278672 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 395424 Color: 0
Size: 322196 Color: 1
Size: 282381 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 395453 Color: 1
Size: 315739 Color: 1
Size: 288809 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 395534 Color: 1
Size: 336186 Color: 1
Size: 268281 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 395611 Color: 1
Size: 331262 Color: 1
Size: 273128 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 395606 Color: 0
Size: 330660 Color: 1
Size: 273735 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 395791 Color: 1
Size: 315942 Color: 0
Size: 288268 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 395792 Color: 1
Size: 346685 Color: 1
Size: 257524 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 395809 Color: 1
Size: 320530 Color: 0
Size: 283662 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 395862 Color: 1
Size: 336091 Color: 0
Size: 268048 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 395908 Color: 0
Size: 347687 Color: 0
Size: 256406 Color: 1

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 396054 Color: 1
Size: 333847 Color: 1
Size: 270100 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 396263 Color: 0
Size: 318634 Color: 0
Size: 285104 Color: 1

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 396348 Color: 0
Size: 349629 Color: 1
Size: 254024 Color: 1

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 396394 Color: 1
Size: 305236 Color: 0
Size: 298371 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 396663 Color: 0
Size: 311587 Color: 1
Size: 291751 Color: 1

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 396786 Color: 0
Size: 305896 Color: 0
Size: 297319 Color: 1

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 396798 Color: 0
Size: 352260 Color: 1
Size: 250943 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 396822 Color: 0
Size: 347530 Color: 0
Size: 255649 Color: 1

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 396849 Color: 0
Size: 311266 Color: 1
Size: 291886 Color: 1

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 396879 Color: 1
Size: 328219 Color: 0
Size: 274903 Color: 1

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 396897 Color: 0
Size: 348952 Color: 1
Size: 254152 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 397032 Color: 0
Size: 351472 Color: 1
Size: 251497 Color: 1

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 397064 Color: 0
Size: 337647 Color: 0
Size: 265290 Color: 1

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 397082 Color: 1
Size: 334837 Color: 0
Size: 268082 Color: 1

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 397265 Color: 0
Size: 328156 Color: 1
Size: 274580 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 397438 Color: 0
Size: 352387 Color: 1
Size: 250176 Color: 1

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 397468 Color: 1
Size: 303526 Color: 0
Size: 299007 Color: 1

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 397499 Color: 1
Size: 346936 Color: 1
Size: 255566 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 397545 Color: 1
Size: 305595 Color: 1
Size: 296861 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 397705 Color: 0
Size: 331138 Color: 0
Size: 271158 Color: 1

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 397739 Color: 0
Size: 308516 Color: 1
Size: 293746 Color: 1

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 397623 Color: 1
Size: 317999 Color: 1
Size: 284379 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 397757 Color: 0
Size: 351981 Color: 0
Size: 250263 Color: 1

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 397744 Color: 1
Size: 351490 Color: 1
Size: 250767 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 397802 Color: 0
Size: 336214 Color: 0
Size: 265985 Color: 1

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 397891 Color: 1
Size: 312147 Color: 0
Size: 289963 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 397958 Color: 1
Size: 324466 Color: 0
Size: 277577 Color: 1

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 398019 Color: 1
Size: 315988 Color: 1
Size: 285994 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 398115 Color: 1
Size: 314239 Color: 0
Size: 287647 Color: 1

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 398119 Color: 1
Size: 322776 Color: 0
Size: 279106 Color: 0

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 398260 Color: 1
Size: 334074 Color: 0
Size: 267667 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 398305 Color: 1
Size: 343015 Color: 1
Size: 258681 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 398476 Color: 1
Size: 304437 Color: 0
Size: 297088 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 398440 Color: 0
Size: 316666 Color: 0
Size: 284895 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 398556 Color: 1
Size: 306564 Color: 1
Size: 294881 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 398475 Color: 0
Size: 340143 Color: 1
Size: 261383 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 398665 Color: 1
Size: 304946 Color: 0
Size: 296390 Color: 1

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 398670 Color: 1
Size: 308851 Color: 0
Size: 292480 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 398772 Color: 0
Size: 347094 Color: 1
Size: 254135 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 398833 Color: 0
Size: 340579 Color: 1
Size: 260589 Color: 1

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 398847 Color: 0
Size: 349684 Color: 1
Size: 251470 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 398864 Color: 0
Size: 303100 Color: 1
Size: 298037 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 398871 Color: 0
Size: 336764 Color: 1
Size: 264366 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 398992 Color: 0
Size: 313621 Color: 0
Size: 287388 Color: 1

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 399021 Color: 1
Size: 333875 Color: 1
Size: 267105 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 399165 Color: 0
Size: 309439 Color: 1
Size: 291397 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 399168 Color: 0
Size: 344477 Color: 0
Size: 256356 Color: 1

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 399126 Color: 1
Size: 320998 Color: 0
Size: 279877 Color: 1

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 399216 Color: 0
Size: 305375 Color: 0
Size: 295410 Color: 1

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 399140 Color: 1
Size: 327693 Color: 0
Size: 273168 Color: 1

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 399314 Color: 0
Size: 328877 Color: 1
Size: 271810 Color: 1

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 399431 Color: 1
Size: 337425 Color: 1
Size: 263145 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 399584 Color: 1
Size: 309772 Color: 1
Size: 290645 Color: 0

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 399681 Color: 1
Size: 306666 Color: 0
Size: 293654 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 399661 Color: 0
Size: 320652 Color: 0
Size: 279688 Color: 1

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 399772 Color: 1
Size: 300191 Color: 0
Size: 300038 Color: 1

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 399843 Color: 1
Size: 338928 Color: 1
Size: 261230 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 400078 Color: 1
Size: 304134 Color: 0
Size: 295789 Color: 1

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 400097 Color: 0
Size: 323019 Color: 1
Size: 276885 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 400187 Color: 1
Size: 334678 Color: 1
Size: 265136 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 400252 Color: 0
Size: 301947 Color: 1
Size: 297802 Color: 1

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 400188 Color: 1
Size: 322746 Color: 0
Size: 277067 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 400286 Color: 0
Size: 303906 Color: 1
Size: 295809 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 400342 Color: 1
Size: 340783 Color: 0
Size: 258876 Color: 1

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 400354 Color: 1
Size: 319626 Color: 0
Size: 280021 Color: 1

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 400633 Color: 1
Size: 299956 Color: 1
Size: 299412 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 400620 Color: 0
Size: 331524 Color: 1
Size: 267857 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 400719 Color: 0
Size: 319579 Color: 1
Size: 279703 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 400787 Color: 1
Size: 309981 Color: 0
Size: 289233 Color: 1

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 400876 Color: 0
Size: 315578 Color: 1
Size: 283547 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 400937 Color: 1
Size: 304434 Color: 0
Size: 294630 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 400885 Color: 0
Size: 344763 Color: 0
Size: 254353 Color: 1

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 401141 Color: 1
Size: 343346 Color: 0
Size: 255514 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 401209 Color: 1
Size: 322191 Color: 1
Size: 276601 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 401386 Color: 1
Size: 341573 Color: 0
Size: 257042 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 401459 Color: 1
Size: 309922 Color: 0
Size: 288620 Color: 1

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 401404 Color: 0
Size: 328092 Color: 1
Size: 270505 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 401463 Color: 1
Size: 313326 Color: 1
Size: 285212 Color: 0

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 401468 Color: 1
Size: 327452 Color: 0
Size: 271081 Color: 1

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 401480 Color: 0
Size: 300354 Color: 1
Size: 298167 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 401630 Color: 0
Size: 327441 Color: 0
Size: 270930 Color: 1

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 401547 Color: 1
Size: 335904 Color: 1
Size: 262550 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 401667 Color: 1
Size: 342274 Color: 0
Size: 256060 Color: 1

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 401759 Color: 0
Size: 335624 Color: 0
Size: 262618 Color: 1

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 401825 Color: 1
Size: 309017 Color: 1
Size: 289159 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 401916 Color: 0
Size: 315867 Color: 0
Size: 282218 Color: 1

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 401924 Color: 1
Size: 327972 Color: 0
Size: 270105 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 401970 Color: 1
Size: 315797 Color: 0
Size: 282234 Color: 1

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 402042 Color: 0
Size: 306477 Color: 0
Size: 291482 Color: 1

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 402082 Color: 0
Size: 304765 Color: 1
Size: 293154 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 402050 Color: 1
Size: 319627 Color: 1
Size: 278324 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 402095 Color: 0
Size: 316366 Color: 0
Size: 281540 Color: 1

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 402137 Color: 0
Size: 319423 Color: 1
Size: 278441 Color: 1

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 402213 Color: 0
Size: 327693 Color: 0
Size: 270095 Color: 1

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 402327 Color: 1
Size: 328066 Color: 1
Size: 269608 Color: 0

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 402311 Color: 0
Size: 320485 Color: 1
Size: 277205 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 402468 Color: 1
Size: 301816 Color: 0
Size: 295717 Color: 1

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 402516 Color: 1
Size: 311361 Color: 1
Size: 286124 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 402387 Color: 0
Size: 317256 Color: 1
Size: 280358 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 402473 Color: 0
Size: 310802 Color: 0
Size: 286726 Color: 1

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 402607 Color: 0
Size: 301620 Color: 1
Size: 295774 Color: 1

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 402610 Color: 1
Size: 337303 Color: 0
Size: 260088 Color: 1

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 402748 Color: 0
Size: 332676 Color: 1
Size: 264577 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 402719 Color: 1
Size: 319375 Color: 0
Size: 277907 Color: 1

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 402841 Color: 0
Size: 321215 Color: 1
Size: 275945 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 402854 Color: 1
Size: 310035 Color: 0
Size: 287112 Color: 1

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 403026 Color: 1
Size: 300957 Color: 0
Size: 296018 Color: 1

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 403113 Color: 0
Size: 302535 Color: 0
Size: 294353 Color: 1

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 403168 Color: 1
Size: 341725 Color: 0
Size: 255108 Color: 1

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 403187 Color: 0
Size: 342703 Color: 0
Size: 254111 Color: 1

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 403208 Color: 1
Size: 299321 Color: 0
Size: 297472 Color: 1

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 403536 Color: 1
Size: 305753 Color: 0
Size: 290712 Color: 1

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 403571 Color: 0
Size: 309670 Color: 1
Size: 286760 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 403666 Color: 0
Size: 340808 Color: 1
Size: 255527 Color: 1

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 403753 Color: 0
Size: 332836 Color: 0
Size: 263412 Color: 1

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 403752 Color: 1
Size: 308454 Color: 0
Size: 287795 Color: 1

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 403834 Color: 0
Size: 322596 Color: 1
Size: 273571 Color: 1

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 403869 Color: 0
Size: 301632 Color: 0
Size: 294500 Color: 1

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 403898 Color: 0
Size: 333092 Color: 1
Size: 263011 Color: 1

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 403900 Color: 0
Size: 310333 Color: 0
Size: 285768 Color: 1

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 403857 Color: 1
Size: 342997 Color: 1
Size: 253147 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 403938 Color: 1
Size: 317938 Color: 0
Size: 278125 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 404018 Color: 0
Size: 319380 Color: 0
Size: 276603 Color: 1

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 403959 Color: 1
Size: 323978 Color: 0
Size: 272064 Color: 1

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 404153 Color: 1
Size: 327206 Color: 0
Size: 268642 Color: 1

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 404309 Color: 0
Size: 332481 Color: 0
Size: 263211 Color: 1

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 404283 Color: 1
Size: 334983 Color: 0
Size: 260735 Color: 1

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 404319 Color: 0
Size: 337856 Color: 1
Size: 257826 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 404329 Color: 1
Size: 322608 Color: 0
Size: 273064 Color: 1

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 404446 Color: 0
Size: 320192 Color: 1
Size: 275363 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 404379 Color: 1
Size: 342927 Color: 0
Size: 252695 Color: 1

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 404463 Color: 0
Size: 336676 Color: 1
Size: 258862 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 404459 Color: 1
Size: 333954 Color: 1
Size: 261588 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 404608 Color: 1
Size: 303717 Color: 0
Size: 291676 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 404654 Color: 1
Size: 309117 Color: 0
Size: 286230 Color: 1

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 404687 Color: 1
Size: 333146 Color: 0
Size: 262168 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 404697 Color: 1
Size: 327569 Color: 1
Size: 267735 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 404724 Color: 1
Size: 338512 Color: 0
Size: 256765 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 404954 Color: 1
Size: 316150 Color: 1
Size: 278897 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 405113 Color: 0
Size: 315069 Color: 1
Size: 279819 Color: 1

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 405130 Color: 0
Size: 336312 Color: 0
Size: 258559 Color: 1

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 405212 Color: 0
Size: 298015 Color: 1
Size: 296774 Color: 1

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 405042 Color: 1
Size: 337546 Color: 1
Size: 257413 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 405214 Color: 0
Size: 318569 Color: 0
Size: 276218 Color: 1

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 405188 Color: 1
Size: 339868 Color: 0
Size: 254945 Color: 1

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 405382 Color: 0
Size: 333047 Color: 0
Size: 261572 Color: 1

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 405403 Color: 0
Size: 342416 Color: 1
Size: 252182 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 405542 Color: 0
Size: 308279 Color: 0
Size: 286180 Color: 1

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 405436 Color: 1
Size: 321808 Color: 1
Size: 272757 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 405557 Color: 0
Size: 315098 Color: 0
Size: 279346 Color: 1

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 405474 Color: 1
Size: 326716 Color: 1
Size: 267811 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 405586 Color: 0
Size: 299159 Color: 1
Size: 295256 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 405623 Color: 0
Size: 340470 Color: 0
Size: 253908 Color: 1

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 405734 Color: 0
Size: 316696 Color: 1
Size: 277571 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 405880 Color: 0
Size: 317090 Color: 0
Size: 277031 Color: 1

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 405888 Color: 1
Size: 343624 Color: 1
Size: 250489 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 405887 Color: 0
Size: 328392 Color: 1
Size: 265722 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 405902 Color: 1
Size: 315344 Color: 0
Size: 278755 Color: 1

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 405902 Color: 0
Size: 304959 Color: 1
Size: 289140 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 405946 Color: 1
Size: 342065 Color: 0
Size: 251990 Color: 1

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 405927 Color: 0
Size: 323560 Color: 1
Size: 270514 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 406147 Color: 0
Size: 308087 Color: 1
Size: 285767 Color: 1

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 406306 Color: 1
Size: 321679 Color: 0
Size: 272016 Color: 1

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 406422 Color: 1
Size: 307436 Color: 0
Size: 286143 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 406441 Color: 1
Size: 341748 Color: 0
Size: 251812 Color: 1

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 406435 Color: 0
Size: 339614 Color: 1
Size: 253952 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 406450 Color: 0
Size: 305972 Color: 1
Size: 287579 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 406517 Color: 1
Size: 339621 Color: 0
Size: 253863 Color: 1

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 406546 Color: 1
Size: 311731 Color: 0
Size: 281724 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 406559 Color: 1
Size: 323790 Color: 0
Size: 269652 Color: 1

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 406653 Color: 0
Size: 316801 Color: 0
Size: 276547 Color: 1

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 406672 Color: 0
Size: 330123 Color: 0
Size: 263206 Color: 1

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 406671 Color: 1
Size: 320745 Color: 1
Size: 272585 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 406760 Color: 1
Size: 330604 Color: 0
Size: 262637 Color: 1

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 406799 Color: 0
Size: 323106 Color: 1
Size: 270096 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 406850 Color: 0
Size: 314259 Color: 1
Size: 278892 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 406890 Color: 1
Size: 298657 Color: 0
Size: 294454 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 406857 Color: 0
Size: 322511 Color: 1
Size: 270633 Color: 0

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 407040 Color: 1
Size: 324467 Color: 0
Size: 268494 Color: 1

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 407111 Color: 1
Size: 302488 Color: 0
Size: 290402 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 407180 Color: 0
Size: 325041 Color: 1
Size: 267780 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 407131 Color: 1
Size: 333676 Color: 1
Size: 259194 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 407224 Color: 0
Size: 315908 Color: 1
Size: 276869 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 407205 Color: 1
Size: 302095 Color: 0
Size: 290701 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 407337 Color: 0
Size: 341989 Color: 1
Size: 250675 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 407395 Color: 1
Size: 326184 Color: 0
Size: 266422 Color: 1

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 407439 Color: 0
Size: 331538 Color: 1
Size: 261024 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 407469 Color: 0
Size: 306808 Color: 1
Size: 285724 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 407539 Color: 0
Size: 321639 Color: 0
Size: 270823 Color: 1

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 407672 Color: 0
Size: 303996 Color: 1
Size: 288333 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 407805 Color: 1
Size: 303021 Color: 0
Size: 289175 Color: 1

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 407853 Color: 0
Size: 335224 Color: 1
Size: 256924 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 407916 Color: 1
Size: 329166 Color: 1
Size: 262919 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 407928 Color: 0
Size: 297746 Color: 0
Size: 294327 Color: 1

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 408143 Color: 0
Size: 303292 Color: 1
Size: 288566 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 408284 Color: 1
Size: 315366 Color: 1
Size: 276351 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 408181 Color: 0
Size: 297041 Color: 1
Size: 294779 Color: 0

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 408317 Color: 0
Size: 334542 Color: 1
Size: 257142 Color: 1

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 408596 Color: 1
Size: 340799 Color: 1
Size: 250606 Color: 0

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 408600 Color: 1
Size: 341225 Color: 0
Size: 250176 Color: 1

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 408683 Color: 0
Size: 337841 Color: 1
Size: 253477 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 408805 Color: 0
Size: 306372 Color: 1
Size: 284824 Color: 1

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 408759 Color: 1
Size: 336033 Color: 1
Size: 255209 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 409022 Color: 0
Size: 338251 Color: 0
Size: 252728 Color: 1

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 409160 Color: 0
Size: 309820 Color: 0
Size: 281021 Color: 1

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 409365 Color: 1
Size: 296277 Color: 0
Size: 294359 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 409370 Color: 1
Size: 306397 Color: 0
Size: 284234 Color: 1

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 409417 Color: 1
Size: 338008 Color: 1
Size: 252576 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 409462 Color: 0
Size: 304873 Color: 0
Size: 285666 Color: 1

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 409612 Color: 1
Size: 330209 Color: 1
Size: 260180 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 409644 Color: 0
Size: 339423 Color: 1
Size: 250934 Color: 1

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 409720 Color: 0
Size: 325392 Color: 1
Size: 264889 Color: 1

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 409876 Color: 0
Size: 321080 Color: 1
Size: 269045 Color: 1

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 409736 Color: 1
Size: 325844 Color: 0
Size: 264421 Color: 1

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 409911 Color: 0
Size: 325898 Color: 0
Size: 264192 Color: 1

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 409876 Color: 1
Size: 303060 Color: 1
Size: 287065 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 409971 Color: 1
Size: 322800 Color: 0
Size: 267230 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 410033 Color: 1
Size: 329736 Color: 0
Size: 260232 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 409986 Color: 0
Size: 339675 Color: 0
Size: 250340 Color: 1

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 410129 Color: 1
Size: 312638 Color: 0
Size: 277234 Color: 1

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 410159 Color: 0
Size: 327094 Color: 0
Size: 262748 Color: 1

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 410247 Color: 0
Size: 305816 Color: 0
Size: 283938 Color: 1

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 410264 Color: 0
Size: 312176 Color: 1
Size: 277561 Color: 1

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 410312 Color: 0
Size: 314398 Color: 1
Size: 275291 Color: 1

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 410383 Color: 0
Size: 313262 Color: 1
Size: 276356 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 410349 Color: 1
Size: 327172 Color: 0
Size: 262480 Color: 1

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 410443 Color: 0
Size: 320363 Color: 0
Size: 269195 Color: 1

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 410468 Color: 1
Size: 320723 Color: 0
Size: 268810 Color: 1

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 410570 Color: 1
Size: 324804 Color: 0
Size: 264627 Color: 1

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 410649 Color: 0
Size: 327231 Color: 1
Size: 262121 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 349230 Color: 1
Size: 336225 Color: 1
Size: 314546 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 410781 Color: 1
Size: 294893 Color: 0
Size: 294327 Color: 1

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 410901 Color: 0
Size: 299017 Color: 0
Size: 290083 Color: 1

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 410935 Color: 1
Size: 329290 Color: 1
Size: 259776 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 410998 Color: 1
Size: 316138 Color: 1
Size: 272865 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 411042 Color: 0
Size: 305265 Color: 0
Size: 283694 Color: 1

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 411048 Color: 1
Size: 308523 Color: 0
Size: 280430 Color: 1

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 411113 Color: 0
Size: 297753 Color: 0
Size: 291135 Color: 1

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 411096 Color: 1
Size: 303698 Color: 0
Size: 285207 Color: 1

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 411220 Color: 0
Size: 309811 Color: 0
Size: 278970 Color: 1

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 411142 Color: 1
Size: 323293 Color: 1
Size: 265566 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 411229 Color: 0
Size: 297847 Color: 0
Size: 290925 Color: 1

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 411190 Color: 1
Size: 318272 Color: 0
Size: 270539 Color: 1

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 411402 Color: 1
Size: 322300 Color: 0
Size: 266299 Color: 1

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 411569 Color: 1
Size: 330354 Color: 0
Size: 258078 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 411598 Color: 1
Size: 297246 Color: 1
Size: 291157 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 411549 Color: 0
Size: 317964 Color: 1
Size: 270488 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 411700 Color: 1
Size: 300936 Color: 1
Size: 287365 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 411708 Color: 1
Size: 308762 Color: 0
Size: 279531 Color: 1

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 411743 Color: 0
Size: 322464 Color: 0
Size: 265794 Color: 1

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 411827 Color: 0
Size: 320758 Color: 0
Size: 267416 Color: 1

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 412003 Color: 1
Size: 336367 Color: 0
Size: 251631 Color: 1

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 411957 Color: 0
Size: 305781 Color: 1
Size: 282263 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 412055 Color: 0
Size: 328685 Color: 1
Size: 259261 Color: 1

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 412200 Color: 1
Size: 306544 Color: 1
Size: 281257 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 412294 Color: 1
Size: 308064 Color: 1
Size: 279643 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 412324 Color: 1
Size: 311650 Color: 0
Size: 276027 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 412382 Color: 0
Size: 317016 Color: 0
Size: 270603 Color: 1

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 412421 Color: 1
Size: 328883 Color: 0
Size: 258697 Color: 1

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 412457 Color: 1
Size: 316328 Color: 0
Size: 271216 Color: 1

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 412427 Color: 0
Size: 318684 Color: 1
Size: 268890 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 412508 Color: 1
Size: 302090 Color: 0
Size: 285403 Color: 1

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 412628 Color: 1
Size: 295724 Color: 0
Size: 291649 Color: 1

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 412734 Color: 0
Size: 322642 Color: 1
Size: 264625 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 412680 Color: 1
Size: 325761 Color: 1
Size: 261560 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 412781 Color: 0
Size: 336047 Color: 0
Size: 251173 Color: 1

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 412813 Color: 0
Size: 295505 Color: 1
Size: 291683 Color: 1

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 412823 Color: 0
Size: 320745 Color: 0
Size: 266433 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 412814 Color: 1
Size: 333535 Color: 0
Size: 253652 Color: 1

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 412976 Color: 1
Size: 294631 Color: 0
Size: 292394 Color: 1

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 412862 Color: 0
Size: 296128 Color: 1
Size: 291011 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 413015 Color: 1
Size: 328297 Color: 0
Size: 258689 Color: 1

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 413020 Color: 1
Size: 331629 Color: 0
Size: 255352 Color: 0

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 412981 Color: 0
Size: 334179 Color: 1
Size: 252841 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 413070 Color: 1
Size: 332139 Color: 0
Size: 254792 Color: 1

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 413248 Color: 0
Size: 325937 Color: 0
Size: 260816 Color: 1

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 413255 Color: 0
Size: 324564 Color: 1
Size: 262182 Color: 1

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 413257 Color: 0
Size: 328117 Color: 0
Size: 258627 Color: 1

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 413187 Color: 1
Size: 327915 Color: 0
Size: 258899 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 413315 Color: 0
Size: 327724 Color: 1
Size: 258962 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 413376 Color: 0
Size: 309276 Color: 0
Size: 277349 Color: 1

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 413295 Color: 1
Size: 321932 Color: 0
Size: 264774 Color: 1

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 413418 Color: 0
Size: 318430 Color: 0
Size: 268153 Color: 1

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 413465 Color: 0
Size: 313044 Color: 1
Size: 273492 Color: 1

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 413465 Color: 1
Size: 323489 Color: 1
Size: 263047 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 413566 Color: 1
Size: 304461 Color: 0
Size: 281974 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 413569 Color: 1
Size: 299383 Color: 0
Size: 287049 Color: 1

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 413545 Color: 0
Size: 321142 Color: 0
Size: 265314 Color: 1

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 413570 Color: 1
Size: 318511 Color: 0
Size: 267920 Color: 1

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 413612 Color: 0
Size: 296510 Color: 1
Size: 289879 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 413680 Color: 1
Size: 295068 Color: 0
Size: 291253 Color: 1

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 413746 Color: 1
Size: 304333 Color: 0
Size: 281922 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 413869 Color: 1
Size: 325597 Color: 1
Size: 260535 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 413807 Color: 0
Size: 303196 Color: 1
Size: 282998 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 413938 Color: 0
Size: 311690 Color: 1
Size: 274373 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 414032 Color: 1
Size: 319680 Color: 0
Size: 266289 Color: 1

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 413971 Color: 0
Size: 329600 Color: 1
Size: 256430 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 414051 Color: 1
Size: 313751 Color: 1
Size: 272199 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 414185 Color: 1
Size: 294637 Color: 0
Size: 291179 Color: 1

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 414199 Color: 1
Size: 299082 Color: 0
Size: 286720 Color: 1

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 414502 Color: 1
Size: 322879 Color: 1
Size: 262620 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 414730 Color: 1
Size: 328176 Color: 0
Size: 257095 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 414955 Color: 0
Size: 319480 Color: 1
Size: 265566 Color: 1

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 414992 Color: 1
Size: 313649 Color: 0
Size: 271360 Color: 1

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 415131 Color: 0
Size: 305157 Color: 1
Size: 279713 Color: 1

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 415199 Color: 1
Size: 331921 Color: 0
Size: 252881 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 415208 Color: 1
Size: 293788 Color: 0
Size: 291005 Color: 1

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 415302 Color: 0
Size: 300022 Color: 1
Size: 284677 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 415308 Color: 1
Size: 306954 Color: 1
Size: 277739 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 415311 Color: 0
Size: 333096 Color: 0
Size: 251594 Color: 1

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 415373 Color: 1
Size: 312933 Color: 0
Size: 271695 Color: 1

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 415481 Color: 1
Size: 319606 Color: 1
Size: 264914 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 415508 Color: 1
Size: 322323 Color: 0
Size: 262170 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 415798 Color: 1
Size: 325382 Color: 1
Size: 258821 Color: 0

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 415804 Color: 1
Size: 306901 Color: 0
Size: 277296 Color: 1

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 416036 Color: 1
Size: 295817 Color: 0
Size: 288148 Color: 1

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 415934 Color: 0
Size: 316635 Color: 0
Size: 267432 Color: 1

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 416049 Color: 1
Size: 310220 Color: 0
Size: 273732 Color: 1

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 416076 Color: 1
Size: 303769 Color: 1
Size: 280156 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 416091 Color: 0
Size: 307733 Color: 1
Size: 276177 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 416168 Color: 0
Size: 321935 Color: 1
Size: 261898 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 416288 Color: 0
Size: 309031 Color: 1
Size: 274682 Color: 1

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 416349 Color: 1
Size: 301375 Color: 1
Size: 282277 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 416362 Color: 1
Size: 319057 Color: 0
Size: 264582 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 416442 Color: 0
Size: 320537 Color: 0
Size: 263022 Color: 1

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 416447 Color: 0
Size: 314315 Color: 1
Size: 269239 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 416471 Color: 1
Size: 300175 Color: 0
Size: 283355 Color: 1

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 416779 Color: 1
Size: 322728 Color: 0
Size: 260494 Color: 1

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 416817 Color: 1
Size: 322690 Color: 0
Size: 260494 Color: 1

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 416958 Color: 0
Size: 331508 Color: 1
Size: 251535 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 416975 Color: 1
Size: 329842 Color: 0
Size: 253184 Color: 1

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 417001 Color: 1
Size: 321710 Color: 0
Size: 261290 Color: 1

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 417187 Color: 1
Size: 315306 Color: 0
Size: 267508 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 417211 Color: 1
Size: 302588 Color: 1
Size: 280202 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 417246 Color: 1
Size: 317511 Color: 1
Size: 265244 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 417336 Color: 1
Size: 300949 Color: 0
Size: 281716 Color: 1

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 417319 Color: 0
Size: 311104 Color: 1
Size: 271578 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 417390 Color: 0
Size: 302981 Color: 0
Size: 279630 Color: 1

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 417399 Color: 1
Size: 303144 Color: 1
Size: 279458 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 417738 Color: 0
Size: 322513 Color: 1
Size: 259750 Color: 1

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 417683 Color: 1
Size: 291646 Color: 1
Size: 290672 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 417782 Color: 0
Size: 330929 Color: 1
Size: 251290 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 417804 Color: 1
Size: 324741 Color: 0
Size: 257456 Color: 1

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 417906 Color: 1
Size: 303997 Color: 0
Size: 278098 Color: 1

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 417964 Color: 0
Size: 314043 Color: 0
Size: 267994 Color: 1

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 417919 Color: 1
Size: 325341 Color: 1
Size: 256741 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 418114 Color: 0
Size: 295089 Color: 1
Size: 286798 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 418303 Color: 0
Size: 309710 Color: 1
Size: 271988 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 418299 Color: 1
Size: 305541 Color: 0
Size: 276161 Color: 1

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 418375 Color: 1
Size: 309220 Color: 1
Size: 272406 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 418395 Color: 0
Size: 324489 Color: 1
Size: 257117 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 418534 Color: 0
Size: 301331 Color: 1
Size: 280136 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 418690 Color: 1
Size: 326768 Color: 1
Size: 254543 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 418868 Color: 1
Size: 314807 Color: 0
Size: 266326 Color: 0

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 419017 Color: 0
Size: 308847 Color: 0
Size: 272137 Color: 1

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 419019 Color: 0
Size: 318887 Color: 1
Size: 262095 Color: 1

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 418989 Color: 1
Size: 296707 Color: 0
Size: 284305 Color: 1

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 362184 Color: 1
Size: 325060 Color: 0
Size: 312757 Color: 1

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 419129 Color: 1
Size: 326761 Color: 1
Size: 254111 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 419202 Color: 0
Size: 293049 Color: 1
Size: 287750 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 419259 Color: 1
Size: 313528 Color: 1
Size: 267214 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 419300 Color: 1
Size: 313347 Color: 0
Size: 267354 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 419349 Color: 1
Size: 312957 Color: 0
Size: 267695 Color: 1

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 419492 Color: 1
Size: 323956 Color: 0
Size: 256553 Color: 1

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 419769 Color: 0
Size: 300215 Color: 1
Size: 280017 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 419844 Color: 1
Size: 299446 Color: 1
Size: 280711 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 419787 Color: 0
Size: 309659 Color: 0
Size: 270555 Color: 1

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 419847 Color: 1
Size: 324105 Color: 0
Size: 256049 Color: 1

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 419992 Color: 0
Size: 291836 Color: 1
Size: 288173 Color: 1

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 420027 Color: 1
Size: 297170 Color: 1
Size: 282804 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 420148 Color: 0
Size: 296621 Color: 0
Size: 283232 Color: 1

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 420305 Color: 1
Size: 296795 Color: 1
Size: 282901 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 420254 Color: 1
Size: 307775 Color: 1
Size: 271972 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 420325 Color: 1
Size: 329293 Color: 0
Size: 250383 Color: 1

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 420368 Color: 1
Size: 300692 Color: 0
Size: 278941 Color: 1

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 420426 Color: 0
Size: 323748 Color: 1
Size: 255827 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 420527 Color: 0
Size: 321186 Color: 1
Size: 258288 Color: 0

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 420533 Color: 1
Size: 305116 Color: 1
Size: 274352 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 420572 Color: 0
Size: 303294 Color: 0
Size: 276135 Color: 1

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 420698 Color: 0
Size: 325610 Color: 1
Size: 253693 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 420702 Color: 1
Size: 292605 Color: 1
Size: 286694 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 420824 Color: 0
Size: 317629 Color: 1
Size: 261548 Color: 1

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 420846 Color: 0
Size: 318990 Color: 0
Size: 260165 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 421051 Color: 0
Size: 311893 Color: 0
Size: 267057 Color: 1

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 421164 Color: 1
Size: 296390 Color: 1
Size: 282447 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 421250 Color: 1
Size: 296435 Color: 0
Size: 282316 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 421302 Color: 1
Size: 322577 Color: 0
Size: 256122 Color: 1

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 421376 Color: 1
Size: 302484 Color: 0
Size: 276141 Color: 1

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 421354 Color: 0
Size: 305890 Color: 1
Size: 272757 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 421424 Color: 0
Size: 302713 Color: 1
Size: 275864 Color: 1

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 421504 Color: 1
Size: 298869 Color: 1
Size: 279628 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 421523 Color: 1
Size: 317377 Color: 1
Size: 261101 Color: 0

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 421615 Color: 0
Size: 298624 Color: 1
Size: 279762 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 421744 Color: 0
Size: 303173 Color: 1
Size: 275084 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 421762 Color: 0
Size: 326493 Color: 1
Size: 251746 Color: 1

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 421768 Color: 0
Size: 323207 Color: 0
Size: 255026 Color: 1

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 421755 Color: 1
Size: 317973 Color: 1
Size: 260273 Color: 0

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 421954 Color: 1
Size: 294740 Color: 0
Size: 283307 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 422013 Color: 0
Size: 300933 Color: 1
Size: 277055 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 422084 Color: 1
Size: 317214 Color: 0
Size: 260703 Color: 1

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 422030 Color: 0
Size: 291182 Color: 0
Size: 286789 Color: 1

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 422244 Color: 0
Size: 326909 Color: 1
Size: 250848 Color: 1

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 422266 Color: 0
Size: 303804 Color: 1
Size: 273931 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 422403 Color: 1
Size: 322025 Color: 0
Size: 255573 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 422331 Color: 0
Size: 313041 Color: 0
Size: 264629 Color: 1

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 422501 Color: 1
Size: 305032 Color: 1
Size: 272468 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 422525 Color: 1
Size: 315011 Color: 1
Size: 262465 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 422768 Color: 1
Size: 309189 Color: 1
Size: 268044 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 422773 Color: 1
Size: 311648 Color: 1
Size: 265580 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 422884 Color: 1
Size: 301829 Color: 1
Size: 275288 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 422828 Color: 0
Size: 313027 Color: 1
Size: 264146 Color: 0

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 422910 Color: 1
Size: 314326 Color: 1
Size: 262765 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 423045 Color: 0
Size: 326193 Color: 0
Size: 250763 Color: 1

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 423066 Color: 1
Size: 319739 Color: 0
Size: 257196 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 423078 Color: 1
Size: 309154 Color: 1
Size: 267769 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 423266 Color: 1
Size: 306224 Color: 0
Size: 270511 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 423352 Color: 1
Size: 293390 Color: 0
Size: 283259 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 423437 Color: 1
Size: 319716 Color: 0
Size: 256848 Color: 1

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 423513 Color: 1
Size: 296562 Color: 1
Size: 279926 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 423609 Color: 1
Size: 297997 Color: 0
Size: 278395 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 423636 Color: 1
Size: 314385 Color: 1
Size: 261980 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 423791 Color: 1
Size: 300446 Color: 0
Size: 275764 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 423792 Color: 1
Size: 315640 Color: 0
Size: 260569 Color: 1

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 423727 Color: 0
Size: 308577 Color: 1
Size: 267697 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 423887 Color: 1
Size: 303810 Color: 0
Size: 272304 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 423889 Color: 1
Size: 301776 Color: 0
Size: 274336 Color: 1

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 423765 Color: 0
Size: 295151 Color: 0
Size: 281085 Color: 1

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 423920 Color: 1
Size: 316380 Color: 0
Size: 259701 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 424078 Color: 0
Size: 301723 Color: 1
Size: 274200 Color: 1

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 424182 Color: 0
Size: 316277 Color: 0
Size: 259542 Color: 1

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 424142 Color: 1
Size: 319119 Color: 1
Size: 256740 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 424212 Color: 0
Size: 314375 Color: 0
Size: 261414 Color: 1

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 424347 Color: 1
Size: 297315 Color: 1
Size: 278339 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 424498 Color: 1
Size: 311561 Color: 1
Size: 263942 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 424601 Color: 0
Size: 315313 Color: 1
Size: 260087 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 424561 Color: 1
Size: 318517 Color: 0
Size: 256923 Color: 1

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 424587 Color: 1
Size: 301532 Color: 1
Size: 273882 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 424690 Color: 0
Size: 315674 Color: 1
Size: 259637 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 424620 Color: 1
Size: 321407 Color: 0
Size: 253974 Color: 1

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 424699 Color: 0
Size: 322975 Color: 0
Size: 252327 Color: 1

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 424707 Color: 0
Size: 306494 Color: 1
Size: 268800 Color: 1

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 424882 Color: 1
Size: 302763 Color: 0
Size: 272356 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 424939 Color: 1
Size: 295883 Color: 0
Size: 279179 Color: 1

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 425022 Color: 1
Size: 314973 Color: 0
Size: 260006 Color: 1

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 425028 Color: 1
Size: 295539 Color: 0
Size: 279434 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 425070 Color: 1
Size: 312624 Color: 1
Size: 262307 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 424990 Color: 0
Size: 291298 Color: 0
Size: 283713 Color: 1

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 425186 Color: 0
Size: 292318 Color: 1
Size: 282497 Color: 1

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 425315 Color: 0
Size: 287624 Color: 0
Size: 287062 Color: 1

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 425497 Color: 0
Size: 302086 Color: 0
Size: 272418 Color: 1

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 425502 Color: 0
Size: 318444 Color: 1
Size: 256055 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 425525 Color: 0
Size: 305739 Color: 1
Size: 268737 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 425428 Color: 1
Size: 296262 Color: 1
Size: 278311 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 425541 Color: 0
Size: 313620 Color: 0
Size: 260840 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 425607 Color: 0
Size: 318926 Color: 1
Size: 255468 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 425516 Color: 1
Size: 309344 Color: 1
Size: 265141 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 425557 Color: 1
Size: 288163 Color: 0
Size: 286281 Color: 1

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 425706 Color: 1
Size: 294224 Color: 0
Size: 280071 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 425918 Color: 0
Size: 323849 Color: 0
Size: 250234 Color: 1

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 425918 Color: 0
Size: 323635 Color: 1
Size: 250448 Color: 1

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 426069 Color: 0
Size: 315122 Color: 1
Size: 258810 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 426222 Color: 0
Size: 321858 Color: 0
Size: 251921 Color: 1

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 426521 Color: 0
Size: 297178 Color: 1
Size: 276302 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 426570 Color: 1
Size: 292214 Color: 0
Size: 281217 Color: 0

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 426555 Color: 0
Size: 321883 Color: 0
Size: 251563 Color: 1

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 426576 Color: 1
Size: 309437 Color: 0
Size: 263988 Color: 1

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 426774 Color: 0
Size: 289615 Color: 0
Size: 283612 Color: 1

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 426743 Color: 1
Size: 318340 Color: 1
Size: 254918 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 426765 Color: 1
Size: 311362 Color: 0
Size: 261874 Color: 1

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 426828 Color: 0
Size: 311379 Color: 0
Size: 261794 Color: 1

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 426926 Color: 1
Size: 309393 Color: 1
Size: 263682 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 426954 Color: 1
Size: 308357 Color: 0
Size: 264690 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 427148 Color: 1
Size: 291798 Color: 0
Size: 281055 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 427219 Color: 1
Size: 286745 Color: 0
Size: 286037 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 427414 Color: 1
Size: 288933 Color: 0
Size: 283654 Color: 1

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 427441 Color: 1
Size: 289730 Color: 0
Size: 282830 Color: 1

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 427499 Color: 0
Size: 319385 Color: 0
Size: 253117 Color: 1

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 427451 Color: 1
Size: 304834 Color: 1
Size: 267716 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 427500 Color: 0
Size: 312056 Color: 0
Size: 260445 Color: 1

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 427505 Color: 1
Size: 312851 Color: 1
Size: 259645 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 427560 Color: 0
Size: 298539 Color: 0
Size: 273902 Color: 1

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 427635 Color: 1
Size: 287444 Color: 0
Size: 284922 Color: 1

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 427627 Color: 0
Size: 287010 Color: 1
Size: 285364 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 427634 Color: 0
Size: 295728 Color: 0
Size: 276639 Color: 1

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 427735 Color: 0
Size: 287299 Color: 1
Size: 284967 Color: 1

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 427762 Color: 0
Size: 304787 Color: 0
Size: 267452 Color: 1

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 427836 Color: 1
Size: 313718 Color: 0
Size: 258447 Color: 1

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 427770 Color: 0
Size: 308035 Color: 0
Size: 264196 Color: 1

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 427858 Color: 1
Size: 292341 Color: 0
Size: 279802 Color: 1

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 428000 Color: 0
Size: 302213 Color: 1
Size: 269788 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 428228 Color: 1
Size: 318117 Color: 0
Size: 253656 Color: 1

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 428284 Color: 0
Size: 319473 Color: 0
Size: 252244 Color: 1

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 428557 Color: 0
Size: 294466 Color: 1
Size: 276978 Color: 1

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 428707 Color: 1
Size: 302894 Color: 0
Size: 268400 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 428939 Color: 0
Size: 298968 Color: 1
Size: 272094 Color: 1

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 428993 Color: 0
Size: 302811 Color: 1
Size: 268197 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 428995 Color: 1
Size: 299936 Color: 0
Size: 271070 Color: 1

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 429060 Color: 1
Size: 299524 Color: 0
Size: 271417 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 429160 Color: 1
Size: 304197 Color: 1
Size: 266644 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 429247 Color: 0
Size: 314461 Color: 0
Size: 256293 Color: 1

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 429322 Color: 1
Size: 307807 Color: 0
Size: 262872 Color: 1

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 429497 Color: 1
Size: 294717 Color: 1
Size: 275787 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 429526 Color: 1
Size: 293612 Color: 1
Size: 276863 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 429642 Color: 0
Size: 290373 Color: 0
Size: 279986 Color: 1

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 429792 Color: 0
Size: 290014 Color: 0
Size: 280195 Color: 1

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 430182 Color: 0
Size: 295629 Color: 1
Size: 274190 Color: 1

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 430200 Color: 0
Size: 302708 Color: 0
Size: 267093 Color: 1

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 430291 Color: 0
Size: 310862 Color: 0
Size: 258848 Color: 1

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 430516 Color: 0
Size: 286521 Color: 0
Size: 282964 Color: 1

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 430482 Color: 1
Size: 284898 Color: 1
Size: 284621 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 430626 Color: 0
Size: 301665 Color: 1
Size: 267710 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 430630 Color: 1
Size: 291852 Color: 0
Size: 277519 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 430646 Color: 0
Size: 310428 Color: 0
Size: 258927 Color: 1

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 430738 Color: 0
Size: 309803 Color: 1
Size: 259460 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 430766 Color: 0
Size: 315142 Color: 1
Size: 254093 Color: 1

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 430823 Color: 1
Size: 296529 Color: 1
Size: 272649 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 430852 Color: 1
Size: 293507 Color: 0
Size: 275642 Color: 1

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 430815 Color: 0
Size: 293943 Color: 1
Size: 275243 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 430922 Color: 1
Size: 296481 Color: 0
Size: 272598 Color: 1

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 430899 Color: 0
Size: 309882 Color: 0
Size: 259220 Color: 1

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 430939 Color: 1
Size: 302918 Color: 1
Size: 266144 Color: 0

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 430967 Color: 0
Size: 317715 Color: 1
Size: 251319 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 430984 Color: 0
Size: 297023 Color: 1
Size: 271994 Color: 1

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 431069 Color: 0
Size: 314692 Color: 1
Size: 254240 Color: 1

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 431105 Color: 0
Size: 317975 Color: 0
Size: 250921 Color: 1

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 431127 Color: 1
Size: 299127 Color: 1
Size: 269747 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 431244 Color: 0
Size: 296871 Color: 1
Size: 271886 Color: 1

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 431357 Color: 0
Size: 315940 Color: 0
Size: 252704 Color: 1

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 431319 Color: 1
Size: 304811 Color: 0
Size: 263871 Color: 1

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 431465 Color: 0
Size: 309159 Color: 0
Size: 259377 Color: 1

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 431386 Color: 1
Size: 312902 Color: 0
Size: 255713 Color: 1

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 431482 Color: 0
Size: 288963 Color: 0
Size: 279556 Color: 1

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 431519 Color: 0
Size: 317499 Color: 1
Size: 250983 Color: 1

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 431581 Color: 0
Size: 290853 Color: 1
Size: 277567 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 431603 Color: 0
Size: 318398 Color: 0
Size: 250000 Color: 1

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 431635 Color: 0
Size: 317473 Color: 1
Size: 250893 Color: 1

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 431734 Color: 0
Size: 310405 Color: 1
Size: 257862 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 431704 Color: 1
Size: 300778 Color: 1
Size: 267519 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 431788 Color: 0
Size: 294401 Color: 1
Size: 273812 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 431798 Color: 1
Size: 308711 Color: 0
Size: 259492 Color: 1

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 431875 Color: 0
Size: 297274 Color: 1
Size: 270852 Color: 1

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 431876 Color: 0
Size: 300974 Color: 0
Size: 267151 Color: 1

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 432083 Color: 0
Size: 291995 Color: 1
Size: 275923 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 432106 Color: 0
Size: 301138 Color: 1
Size: 266757 Color: 1

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 432172 Color: 1
Size: 285642 Color: 1
Size: 282187 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 432239 Color: 0
Size: 298119 Color: 1
Size: 269643 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 432248 Color: 1
Size: 298760 Color: 1
Size: 268993 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 432249 Color: 0
Size: 308021 Color: 0
Size: 259731 Color: 1

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 432398 Color: 1
Size: 302949 Color: 0
Size: 264654 Color: 1

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 432437 Color: 0
Size: 300273 Color: 0
Size: 267291 Color: 1

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 432399 Color: 1
Size: 288766 Color: 0
Size: 278836 Color: 1

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 432457 Color: 1
Size: 293002 Color: 0
Size: 274542 Color: 1

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 432507 Color: 0
Size: 308007 Color: 0
Size: 259487 Color: 1

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 432481 Color: 1
Size: 302253 Color: 1
Size: 265267 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 432536 Color: 0
Size: 289262 Color: 0
Size: 278203 Color: 1

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 432545 Color: 1
Size: 313248 Color: 1
Size: 254208 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 432574 Color: 0
Size: 289448 Color: 1
Size: 277979 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 432586 Color: 1
Size: 284981 Color: 1
Size: 282434 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 432874 Color: 1
Size: 287999 Color: 0
Size: 279128 Color: 1

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 432898 Color: 0
Size: 295027 Color: 0
Size: 272076 Color: 1

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 432893 Color: 1
Size: 294326 Color: 0
Size: 272782 Color: 1

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 432902 Color: 0
Size: 299117 Color: 1
Size: 267982 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 432999 Color: 1
Size: 285338 Color: 0
Size: 281664 Color: 1

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 433014 Color: 0
Size: 298070 Color: 1
Size: 268917 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 433058 Color: 1
Size: 292932 Color: 0
Size: 274011 Color: 1

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 433239 Color: 0
Size: 283882 Color: 0
Size: 282880 Color: 1

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 433263 Color: 0
Size: 298238 Color: 1
Size: 268500 Color: 1

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 433316 Color: 0
Size: 305083 Color: 1
Size: 261602 Color: 1

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 433444 Color: 1
Size: 299325 Color: 1
Size: 267232 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 433464 Color: 1
Size: 314348 Color: 0
Size: 252189 Color: 1

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 433569 Color: 0
Size: 285605 Color: 1
Size: 280827 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 433584 Color: 1
Size: 287738 Color: 0
Size: 278679 Color: 1

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 433748 Color: 0
Size: 300353 Color: 0
Size: 265900 Color: 1

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 433753 Color: 1
Size: 311648 Color: 0
Size: 254600 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 433864 Color: 0
Size: 307807 Color: 1
Size: 258330 Color: 1

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 433952 Color: 1
Size: 304310 Color: 0
Size: 261739 Color: 1

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 434038 Color: 0
Size: 309491 Color: 0
Size: 256472 Color: 1

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 434104 Color: 0
Size: 315700 Color: 1
Size: 250197 Color: 1

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 434192 Color: 0
Size: 293798 Color: 1
Size: 272011 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 434303 Color: 1
Size: 297457 Color: 1
Size: 268241 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 434336 Color: 1
Size: 292627 Color: 0
Size: 273038 Color: 0

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 434343 Color: 1
Size: 301540 Color: 0
Size: 264118 Color: 1

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 434408 Color: 0
Size: 283750 Color: 0
Size: 281843 Color: 1

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 434565 Color: 0
Size: 291156 Color: 0
Size: 274280 Color: 1

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 434549 Color: 1
Size: 299722 Color: 0
Size: 265730 Color: 1

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 434681 Color: 0
Size: 308936 Color: 1
Size: 256384 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 434764 Color: 1
Size: 286730 Color: 0
Size: 278507 Color: 1

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 434757 Color: 0
Size: 312238 Color: 1
Size: 253006 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 434770 Color: 1
Size: 308209 Color: 1
Size: 257022 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 434855 Color: 1
Size: 308503 Color: 0
Size: 256643 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 434862 Color: 1
Size: 309081 Color: 0
Size: 256058 Color: 1

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 434946 Color: 1
Size: 289240 Color: 0
Size: 275815 Color: 1

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 434960 Color: 1
Size: 304055 Color: 0
Size: 260986 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 435107 Color: 1
Size: 295509 Color: 0
Size: 269385 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 435148 Color: 1
Size: 285763 Color: 0
Size: 279090 Color: 1

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 435296 Color: 1
Size: 291674 Color: 0
Size: 273031 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 435382 Color: 0
Size: 291726 Color: 1
Size: 272893 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 435383 Color: 1
Size: 298102 Color: 0
Size: 266516 Color: 1

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 435520 Color: 0
Size: 303043 Color: 0
Size: 261438 Color: 1

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 435550 Color: 1
Size: 282549 Color: 1
Size: 281902 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 435522 Color: 0
Size: 284332 Color: 1
Size: 280147 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 435780 Color: 0
Size: 304942 Color: 1
Size: 259279 Color: 1

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 436129 Color: 0
Size: 287399 Color: 1
Size: 276473 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 436056 Color: 0
Size: 301299 Color: 0
Size: 262646 Color: 1

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 436163 Color: 1
Size: 303764 Color: 0
Size: 260074 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 436179 Color: 1
Size: 305090 Color: 0
Size: 258732 Color: 1

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 436310 Color: 0
Size: 308451 Color: 0
Size: 255240 Color: 1

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 436221 Color: 1
Size: 311250 Color: 0
Size: 252530 Color: 1

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 436375 Color: 0
Size: 283629 Color: 1
Size: 279997 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 436370 Color: 1
Size: 293904 Color: 1
Size: 269727 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 436448 Color: 1
Size: 296065 Color: 0
Size: 267488 Color: 1

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 436715 Color: 1
Size: 295928 Color: 0
Size: 267358 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 436691 Color: 0
Size: 297509 Color: 0
Size: 265801 Color: 1

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 436752 Color: 1
Size: 309459 Color: 1
Size: 253790 Color: 0

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 436697 Color: 0
Size: 307491 Color: 1
Size: 255813 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 436932 Color: 1
Size: 290211 Color: 0
Size: 272858 Color: 1

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 436952 Color: 1
Size: 289815 Color: 0
Size: 273234 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 436960 Color: 1
Size: 284385 Color: 1
Size: 278656 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 436902 Color: 0
Size: 299895 Color: 1
Size: 263204 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 436981 Color: 0
Size: 303741 Color: 1
Size: 259279 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 437019 Color: 0
Size: 290310 Color: 1
Size: 272672 Color: 1

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 437019 Color: 1
Size: 308651 Color: 0
Size: 254331 Color: 1

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 437235 Color: 0
Size: 284801 Color: 0
Size: 277965 Color: 1

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 437131 Color: 1
Size: 310230 Color: 0
Size: 252640 Color: 1

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 437284 Color: 0
Size: 307973 Color: 1
Size: 254744 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 437335 Color: 0
Size: 291296 Color: 1
Size: 271370 Color: 1

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 437356 Color: 0
Size: 287652 Color: 0
Size: 274993 Color: 1

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 437397 Color: 1
Size: 303289 Color: 1
Size: 259315 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 437421 Color: 0
Size: 299428 Color: 0
Size: 263152 Color: 1

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 437452 Color: 1
Size: 301375 Color: 0
Size: 261174 Color: 1

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 437441 Color: 0
Size: 297121 Color: 0
Size: 265439 Color: 1

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 437470 Color: 1
Size: 299277 Color: 0
Size: 263254 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 437565 Color: 1
Size: 302176 Color: 1
Size: 260260 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 437603 Color: 0
Size: 291704 Color: 0
Size: 270694 Color: 1

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 437622 Color: 0
Size: 298907 Color: 1
Size: 263472 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 437802 Color: 0
Size: 300254 Color: 1
Size: 261945 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 437901 Color: 1
Size: 309577 Color: 1
Size: 252523 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 437941 Color: 1
Size: 296649 Color: 1
Size: 265411 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 438054 Color: 1
Size: 283733 Color: 1
Size: 278214 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 438091 Color: 0
Size: 304475 Color: 1
Size: 257435 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 438377 Color: 0
Size: 300612 Color: 1
Size: 261012 Color: 1

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 438407 Color: 1
Size: 293561 Color: 0
Size: 268033 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 438418 Color: 1
Size: 309436 Color: 1
Size: 252147 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 438450 Color: 1
Size: 285834 Color: 0
Size: 275717 Color: 1

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 438657 Color: 0
Size: 286219 Color: 0
Size: 275125 Color: 1

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 438680 Color: 0
Size: 285471 Color: 1
Size: 275850 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 438840 Color: 1
Size: 288616 Color: 0
Size: 272545 Color: 1

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 439148 Color: 0
Size: 309727 Color: 0
Size: 251126 Color: 1

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 439182 Color: 0
Size: 296446 Color: 1
Size: 264373 Color: 1

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 439189 Color: 0
Size: 302929 Color: 1
Size: 257883 Color: 0

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 439267 Color: 1
Size: 305023 Color: 0
Size: 255711 Color: 1

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 439341 Color: 1
Size: 305868 Color: 0
Size: 254792 Color: 1

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 439392 Color: 1
Size: 291571 Color: 1
Size: 269038 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 439401 Color: 1
Size: 284886 Color: 0
Size: 275714 Color: 1

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 439429 Color: 0
Size: 284748 Color: 1
Size: 275824 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 439492 Color: 0
Size: 296011 Color: 1
Size: 264498 Color: 1

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 439518 Color: 0
Size: 288994 Color: 0
Size: 271489 Color: 1

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 439570 Color: 1
Size: 298643 Color: 0
Size: 261788 Color: 1

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 439731 Color: 1
Size: 303664 Color: 0
Size: 256606 Color: 1

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 439860 Color: 1
Size: 296591 Color: 0
Size: 263550 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 439974 Color: 0
Size: 280397 Color: 1
Size: 279630 Color: 1

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 439939 Color: 1
Size: 281650 Color: 0
Size: 278412 Color: 1

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 440042 Color: 0
Size: 281052 Color: 0
Size: 278907 Color: 1

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 440154 Color: 0
Size: 305881 Color: 1
Size: 253966 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 440226 Color: 1
Size: 298384 Color: 1
Size: 261391 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 440239 Color: 1
Size: 290519 Color: 0
Size: 269243 Color: 0

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 440419 Color: 1
Size: 306769 Color: 0
Size: 252813 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 440425 Color: 1
Size: 281063 Color: 1
Size: 278513 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 440416 Color: 0
Size: 292338 Color: 0
Size: 267247 Color: 1

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 440535 Color: 1
Size: 285161 Color: 1
Size: 274305 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 440623 Color: 1
Size: 300173 Color: 0
Size: 259205 Color: 1

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 440636 Color: 1
Size: 302219 Color: 1
Size: 257146 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 440722 Color: 1
Size: 296370 Color: 0
Size: 262909 Color: 1

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 440775 Color: 0
Size: 299973 Color: 1
Size: 259253 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 440822 Color: 0
Size: 299823 Color: 0
Size: 259356 Color: 1

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 440916 Color: 1
Size: 308625 Color: 0
Size: 250460 Color: 1

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 440959 Color: 0
Size: 288558 Color: 1
Size: 270484 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 441069 Color: 1
Size: 291301 Color: 1
Size: 267631 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 441196 Color: 0
Size: 299315 Color: 1
Size: 259490 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 441329 Color: 0
Size: 286772 Color: 1
Size: 271900 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 441424 Color: 1
Size: 284632 Color: 0
Size: 273945 Color: 1

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 441435 Color: 1
Size: 300901 Color: 0
Size: 257665 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 441460 Color: 1
Size: 286419 Color: 1
Size: 272122 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 441547 Color: 1
Size: 287432 Color: 0
Size: 271022 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 441586 Color: 1
Size: 303877 Color: 1
Size: 254538 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 441606 Color: 1
Size: 307234 Color: 0
Size: 251161 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 441651 Color: 1
Size: 286473 Color: 1
Size: 271877 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 441635 Color: 0
Size: 284408 Color: 0
Size: 273958 Color: 1

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 441703 Color: 1
Size: 295123 Color: 1
Size: 263175 Color: 0

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 441769 Color: 1
Size: 306436 Color: 0
Size: 251796 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 441769 Color: 1
Size: 292063 Color: 0
Size: 266169 Color: 1

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 441771 Color: 1
Size: 304168 Color: 0
Size: 254062 Color: 1

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 441787 Color: 0
Size: 304213 Color: 1
Size: 254001 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 441935 Color: 0
Size: 294504 Color: 1
Size: 263562 Color: 0

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 441972 Color: 0
Size: 280733 Color: 1
Size: 277296 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 441988 Color: 0
Size: 301741 Color: 0
Size: 256272 Color: 1

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 441967 Color: 1
Size: 305846 Color: 0
Size: 252188 Color: 1

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 442038 Color: 0
Size: 303838 Color: 1
Size: 254125 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 442108 Color: 0
Size: 291900 Color: 1
Size: 265993 Color: 1

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 442151 Color: 0
Size: 294687 Color: 1
Size: 263163 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 442174 Color: 0
Size: 307622 Color: 1
Size: 250205 Color: 1

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 442226 Color: 0
Size: 295066 Color: 1
Size: 262709 Color: 1

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 442227 Color: 1
Size: 304898 Color: 0
Size: 252876 Color: 1

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 442353 Color: 1
Size: 284163 Color: 0
Size: 273485 Color: 1

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 442400 Color: 1
Size: 279685 Color: 0
Size: 277916 Color: 1

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 442583 Color: 0
Size: 292507 Color: 1
Size: 264911 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 442589 Color: 1
Size: 304154 Color: 0
Size: 253258 Color: 1

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 442681 Color: 0
Size: 284083 Color: 1
Size: 273237 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 442642 Color: 1
Size: 293760 Color: 0
Size: 263599 Color: 1

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 442823 Color: 1
Size: 305252 Color: 1
Size: 251926 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 442850 Color: 1
Size: 289864 Color: 1
Size: 267287 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 442793 Color: 0
Size: 281410 Color: 1
Size: 275798 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 442951 Color: 1
Size: 296467 Color: 0
Size: 260583 Color: 1

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 410647 Color: 1
Size: 297771 Color: 0
Size: 291583 Color: 1

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 443016 Color: 0
Size: 292422 Color: 0
Size: 264563 Color: 1

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 443017 Color: 0
Size: 302506 Color: 1
Size: 254478 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 443089 Color: 0
Size: 283922 Color: 0
Size: 272990 Color: 1

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 443144 Color: 0
Size: 279019 Color: 0
Size: 277838 Color: 1

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 443244 Color: 0
Size: 290471 Color: 1
Size: 266286 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 443412 Color: 0
Size: 303011 Color: 1
Size: 253578 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 443620 Color: 0
Size: 281152 Color: 1
Size: 275229 Color: 1

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 443638 Color: 0
Size: 285373 Color: 0
Size: 270990 Color: 1

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 443602 Color: 1
Size: 293930 Color: 1
Size: 262469 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 443693 Color: 0
Size: 302531 Color: 0
Size: 253777 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 443695 Color: 0
Size: 301594 Color: 0
Size: 254712 Color: 1

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 443772 Color: 0
Size: 296205 Color: 1
Size: 260024 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 443784 Color: 1
Size: 292019 Color: 1
Size: 264198 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 443830 Color: 0
Size: 304744 Color: 1
Size: 251427 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 443810 Color: 1
Size: 280174 Color: 1
Size: 276017 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 443846 Color: 0
Size: 299597 Color: 0
Size: 256558 Color: 1

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 443881 Color: 0
Size: 280503 Color: 1
Size: 275617 Color: 1

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 443913 Color: 0
Size: 294883 Color: 1
Size: 261205 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 444080 Color: 0
Size: 303076 Color: 1
Size: 252845 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 444228 Color: 1
Size: 289535 Color: 0
Size: 266238 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 444349 Color: 0
Size: 290944 Color: 0
Size: 264708 Color: 1

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 444344 Color: 1
Size: 300528 Color: 0
Size: 255129 Color: 1

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 444466 Color: 1
Size: 278705 Color: 0
Size: 276830 Color: 0

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 444672 Color: 1
Size: 303130 Color: 0
Size: 252199 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 444698 Color: 1
Size: 283239 Color: 1
Size: 272064 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 444822 Color: 1
Size: 287835 Color: 0
Size: 267344 Color: 1

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 444845 Color: 1
Size: 298363 Color: 0
Size: 256793 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 444857 Color: 1
Size: 299929 Color: 0
Size: 255215 Color: 1

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 445153 Color: 0
Size: 303823 Color: 1
Size: 251025 Color: 0

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 445206 Color: 1
Size: 300820 Color: 0
Size: 253975 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 445225 Color: 1
Size: 284185 Color: 0
Size: 270591 Color: 1

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 445308 Color: 0
Size: 303877 Color: 1
Size: 250816 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 445345 Color: 0
Size: 293360 Color: 1
Size: 261296 Color: 1

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 445371 Color: 0
Size: 302190 Color: 1
Size: 252440 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 445486 Color: 0
Size: 281824 Color: 0
Size: 272691 Color: 1

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 445436 Color: 1
Size: 280760 Color: 0
Size: 273805 Color: 1

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 445501 Color: 0
Size: 291106 Color: 1
Size: 263394 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 445478 Color: 1
Size: 298366 Color: 1
Size: 256157 Color: 0

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 445663 Color: 1
Size: 280134 Color: 0
Size: 274204 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 445716 Color: 0
Size: 302995 Color: 1
Size: 251290 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 445746 Color: 1
Size: 279361 Color: 0
Size: 274894 Color: 1

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 445724 Color: 0
Size: 281801 Color: 0
Size: 272476 Color: 1

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 445746 Color: 1
Size: 280940 Color: 0
Size: 273315 Color: 1

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 446034 Color: 1
Size: 291106 Color: 1
Size: 262861 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 446093 Color: 0
Size: 282213 Color: 0
Size: 271695 Color: 1

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 446264 Color: 1
Size: 277245 Color: 0
Size: 276492 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 446452 Color: 1
Size: 292819 Color: 0
Size: 260730 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 446657 Color: 0
Size: 277574 Color: 1
Size: 275770 Color: 1

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 446737 Color: 0
Size: 290708 Color: 0
Size: 262556 Color: 1

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 446780 Color: 1
Size: 295680 Color: 1
Size: 257541 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 446831 Color: 0
Size: 303080 Color: 0
Size: 250090 Color: 1

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 446794 Color: 1
Size: 287914 Color: 0
Size: 265293 Color: 1

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 447037 Color: 0
Size: 296219 Color: 1
Size: 256745 Color: 1

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 447059 Color: 0
Size: 288612 Color: 0
Size: 264330 Color: 1

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 447088 Color: 1
Size: 290231 Color: 1
Size: 262682 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 447314 Color: 0
Size: 295683 Color: 1
Size: 257004 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 447291 Color: 1
Size: 298113 Color: 1
Size: 254597 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 447452 Color: 1
Size: 284474 Color: 0
Size: 268075 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 447628 Color: 0
Size: 279636 Color: 1
Size: 272737 Color: 1

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 447958 Color: 0
Size: 298661 Color: 1
Size: 253382 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 448158 Color: 1
Size: 282487 Color: 1
Size: 269356 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 448201 Color: 0
Size: 301148 Color: 0
Size: 250652 Color: 1

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 448218 Color: 0
Size: 296117 Color: 1
Size: 255666 Color: 1

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 448161 Color: 1
Size: 293716 Color: 1
Size: 258124 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 448229 Color: 1
Size: 281359 Color: 1
Size: 270413 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 448401 Color: 0
Size: 279745 Color: 0
Size: 271855 Color: 1

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 410384 Color: 0
Size: 305559 Color: 0
Size: 284058 Color: 1

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 448555 Color: 1
Size: 282190 Color: 0
Size: 269256 Color: 1

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 448707 Color: 0
Size: 289541 Color: 1
Size: 261753 Color: 1

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 448871 Color: 0
Size: 291228 Color: 1
Size: 259902 Color: 0

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 448891 Color: 1
Size: 279822 Color: 1
Size: 271288 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 448897 Color: 0
Size: 298689 Color: 0
Size: 252415 Color: 1

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 448934 Color: 1
Size: 286245 Color: 1
Size: 264822 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 449465 Color: 0
Size: 290723 Color: 0
Size: 259813 Color: 1

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 449514 Color: 1
Size: 281697 Color: 0
Size: 268790 Color: 1

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 449775 Color: 1
Size: 297202 Color: 0
Size: 253024 Color: 1

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 449854 Color: 0
Size: 285840 Color: 1
Size: 264307 Color: 1

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 450013 Color: 1
Size: 289008 Color: 1
Size: 260980 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 450006 Color: 0
Size: 296101 Color: 1
Size: 253894 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 450107 Color: 0
Size: 298750 Color: 1
Size: 251144 Color: 1

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 450027 Color: 1
Size: 284412 Color: 1
Size: 265562 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 450136 Color: 0
Size: 278729 Color: 0
Size: 271136 Color: 1

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 450035 Color: 1
Size: 275113 Color: 0
Size: 274853 Color: 1

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 450353 Color: 1
Size: 291496 Color: 0
Size: 258152 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 450776 Color: 1
Size: 279301 Color: 1
Size: 269924 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 450872 Color: 0
Size: 294343 Color: 0
Size: 254786 Color: 1

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 450886 Color: 1
Size: 285654 Color: 1
Size: 263461 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 450972 Color: 0
Size: 285154 Color: 1
Size: 263875 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 451116 Color: 0
Size: 290157 Color: 1
Size: 258728 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 451112 Color: 1
Size: 288606 Color: 0
Size: 260283 Color: 1

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 451294 Color: 1
Size: 275230 Color: 1
Size: 273477 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 451404 Color: 0
Size: 279063 Color: 1
Size: 269534 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 451416 Color: 1
Size: 297590 Color: 0
Size: 250995 Color: 1

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 451608 Color: 0
Size: 289721 Color: 1
Size: 258672 Color: 1

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 451686 Color: 0
Size: 288394 Color: 1
Size: 259921 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 451971 Color: 0
Size: 295126 Color: 1
Size: 252904 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 452010 Color: 1
Size: 274778 Color: 1
Size: 273213 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 452017 Color: 1
Size: 276947 Color: 0
Size: 271037 Color: 1

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 452085 Color: 1
Size: 295793 Color: 1
Size: 252123 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 452120 Color: 1
Size: 274901 Color: 0
Size: 272980 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 452133 Color: 1
Size: 294220 Color: 0
Size: 253648 Color: 0

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 452250 Color: 1
Size: 283654 Color: 0
Size: 264097 Color: 1

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 452470 Color: 1
Size: 288338 Color: 1
Size: 259193 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 452535 Color: 1
Size: 283975 Color: 0
Size: 263491 Color: 1

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 452550 Color: 1
Size: 291010 Color: 1
Size: 256441 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 452974 Color: 1
Size: 290755 Color: 0
Size: 256272 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 453090 Color: 1
Size: 274283 Color: 0
Size: 272628 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 453132 Color: 0
Size: 284524 Color: 1
Size: 262345 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 453181 Color: 1
Size: 278812 Color: 0
Size: 268008 Color: 1

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 453246 Color: 1
Size: 288756 Color: 1
Size: 257999 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 453424 Color: 1
Size: 274132 Color: 0
Size: 272445 Color: 1

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 453444 Color: 0
Size: 276869 Color: 0
Size: 269688 Color: 1

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 453569 Color: 1
Size: 282573 Color: 1
Size: 263859 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 453759 Color: 0
Size: 287003 Color: 1
Size: 259239 Color: 1

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 453786 Color: 0
Size: 275897 Color: 0
Size: 270318 Color: 1

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 453955 Color: 1
Size: 283290 Color: 0
Size: 262756 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 453992 Color: 1
Size: 292905 Color: 0
Size: 253104 Color: 0

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 454330 Color: 0
Size: 285643 Color: 0
Size: 260028 Color: 1

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 454464 Color: 0
Size: 292320 Color: 1
Size: 253217 Color: 1

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 454508 Color: 0
Size: 273776 Color: 0
Size: 271717 Color: 1

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 454702 Color: 0
Size: 280115 Color: 1
Size: 265184 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 454734 Color: 1
Size: 278842 Color: 1
Size: 266425 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 454725 Color: 0
Size: 275183 Color: 0
Size: 270093 Color: 1

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 454804 Color: 1
Size: 281957 Color: 0
Size: 263240 Color: 1

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 454814 Color: 1
Size: 283405 Color: 0
Size: 261782 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 454876 Color: 0
Size: 278050 Color: 0
Size: 267075 Color: 1

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 454906 Color: 1
Size: 287888 Color: 0
Size: 257207 Color: 1

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 454901 Color: 0
Size: 281807 Color: 1
Size: 263293 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 454943 Color: 1
Size: 276431 Color: 1
Size: 268627 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 455063 Color: 0
Size: 273588 Color: 0
Size: 271350 Color: 1

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 455145 Color: 0
Size: 285134 Color: 1
Size: 259722 Color: 1

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 455033 Color: 1
Size: 285983 Color: 0
Size: 258985 Color: 1

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 455162 Color: 0
Size: 277774 Color: 1
Size: 267065 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 455141 Color: 1
Size: 287940 Color: 0
Size: 256920 Color: 1

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 455171 Color: 0
Size: 289203 Color: 0
Size: 255627 Color: 1

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 455393 Color: 1
Size: 277863 Color: 0
Size: 266745 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 455493 Color: 1
Size: 280013 Color: 0
Size: 264495 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 455497 Color: 1
Size: 288466 Color: 1
Size: 256038 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 455514 Color: 1
Size: 276755 Color: 0
Size: 267732 Color: 1

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 455652 Color: 0
Size: 290835 Color: 0
Size: 253514 Color: 1

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 455662 Color: 1
Size: 275206 Color: 0
Size: 269133 Color: 1

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 455744 Color: 1
Size: 276670 Color: 1
Size: 267587 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 455798 Color: 0
Size: 272556 Color: 0
Size: 271647 Color: 1

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 455898 Color: 1
Size: 287360 Color: 0
Size: 256743 Color: 1

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 455899 Color: 1
Size: 280035 Color: 0
Size: 264067 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 456164 Color: 1
Size: 288147 Color: 0
Size: 255690 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 456250 Color: 1
Size: 292496 Color: 1
Size: 251255 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 456337 Color: 1
Size: 288157 Color: 0
Size: 255507 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 456376 Color: 1
Size: 271892 Color: 0
Size: 271733 Color: 1

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 456540 Color: 0
Size: 285102 Color: 1
Size: 258359 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 456526 Color: 1
Size: 288379 Color: 1
Size: 255096 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 456671 Color: 1
Size: 282994 Color: 1
Size: 260336 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 456723 Color: 0
Size: 291079 Color: 0
Size: 252199 Color: 1

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 456763 Color: 1
Size: 283968 Color: 0
Size: 259270 Color: 1

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 456764 Color: 1
Size: 286854 Color: 1
Size: 256383 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 456805 Color: 0
Size: 275827 Color: 0
Size: 267369 Color: 1

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 456781 Color: 1
Size: 273804 Color: 0
Size: 269416 Color: 1

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 456895 Color: 0
Size: 272191 Color: 0
Size: 270915 Color: 1

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 456850 Color: 1
Size: 282172 Color: 0
Size: 260979 Color: 1

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 456960 Color: 0
Size: 278974 Color: 1
Size: 264067 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 457006 Color: 1
Size: 279525 Color: 0
Size: 263470 Color: 1

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 457039 Color: 1
Size: 289387 Color: 0
Size: 253575 Color: 1

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 457051 Color: 0
Size: 283286 Color: 0
Size: 259664 Color: 1

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 457141 Color: 1
Size: 278066 Color: 1
Size: 264794 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 457236 Color: 1
Size: 289400 Color: 1
Size: 253365 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 457257 Color: 0
Size: 277936 Color: 1
Size: 264808 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 457367 Color: 1
Size: 290780 Color: 0
Size: 251854 Color: 1

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 457367 Color: 1
Size: 282541 Color: 0
Size: 260093 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 457403 Color: 1
Size: 279034 Color: 1
Size: 263564 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 457580 Color: 1
Size: 290326 Color: 0
Size: 252095 Color: 1

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 457733 Color: 1
Size: 288413 Color: 0
Size: 253855 Color: 0

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 457872 Color: 0
Size: 291488 Color: 1
Size: 250641 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 458155 Color: 0
Size: 276349 Color: 1
Size: 265497 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 458279 Color: 0
Size: 284513 Color: 1
Size: 257209 Color: 1

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 458309 Color: 0
Size: 286557 Color: 0
Size: 255135 Color: 1

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 458272 Color: 1
Size: 281013 Color: 1
Size: 260716 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 458319 Color: 0
Size: 290642 Color: 0
Size: 251040 Color: 1

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 458368 Color: 0
Size: 275212 Color: 0
Size: 266421 Color: 1

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 458498 Color: 1
Size: 276524 Color: 0
Size: 264979 Color: 1

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 458534 Color: 0
Size: 281116 Color: 1
Size: 260351 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 458585 Color: 0
Size: 283541 Color: 0
Size: 257875 Color: 1

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 458590 Color: 0
Size: 284185 Color: 1
Size: 257226 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 458659 Color: 0
Size: 280573 Color: 1
Size: 260769 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 458678 Color: 1
Size: 283533 Color: 0
Size: 257790 Color: 1

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 458664 Color: 0
Size: 279891 Color: 0
Size: 261446 Color: 1

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 458831 Color: 1
Size: 283577 Color: 0
Size: 257593 Color: 1

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 458774 Color: 0
Size: 275585 Color: 0
Size: 265642 Color: 1

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 458847 Color: 1
Size: 281287 Color: 1
Size: 259867 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 458923 Color: 0
Size: 286490 Color: 0
Size: 254588 Color: 1

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 459097 Color: 1
Size: 272748 Color: 0
Size: 268156 Color: 1

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 459147 Color: 1
Size: 284470 Color: 0
Size: 256384 Color: 1

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 459060 Color: 0
Size: 286665 Color: 0
Size: 254276 Color: 1

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 459483 Color: 1
Size: 286605 Color: 0
Size: 253913 Color: 1

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 409784 Color: 0
Size: 333498 Color: 1
Size: 256719 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 459808 Color: 1
Size: 281796 Color: 0
Size: 258397 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 459827 Color: 1
Size: 275374 Color: 1
Size: 264800 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 459866 Color: 1
Size: 278488 Color: 0
Size: 261647 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 460010 Color: 0
Size: 280504 Color: 1
Size: 259487 Color: 1

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 460055 Color: 0
Size: 280987 Color: 1
Size: 258959 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 460119 Color: 1
Size: 281608 Color: 0
Size: 258274 Color: 1

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 460272 Color: 0
Size: 281775 Color: 1
Size: 257954 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 460308 Color: 0
Size: 280965 Color: 1
Size: 258728 Color: 1

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 460420 Color: 0
Size: 278772 Color: 1
Size: 260809 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 460388 Color: 1
Size: 275669 Color: 0
Size: 263944 Color: 1

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 460517 Color: 1
Size: 284261 Color: 0
Size: 255223 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 460570 Color: 0
Size: 273995 Color: 1
Size: 265436 Color: 0

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 460659 Color: 1
Size: 274913 Color: 1
Size: 264429 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 460636 Color: 0
Size: 287393 Color: 1
Size: 251972 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 460759 Color: 0
Size: 275203 Color: 0
Size: 264039 Color: 1

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 461082 Color: 1
Size: 287854 Color: 0
Size: 251065 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 461189 Color: 0
Size: 271620 Color: 1
Size: 267192 Color: 1

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 461195 Color: 0
Size: 275081 Color: 0
Size: 263725 Color: 1

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 461457 Color: 0
Size: 286908 Color: 1
Size: 251636 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 461494 Color: 0
Size: 279751 Color: 1
Size: 258756 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 461451 Color: 1
Size: 277612 Color: 0
Size: 260938 Color: 1

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 461719 Color: 0
Size: 274670 Color: 1
Size: 263612 Color: 1

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 461683 Color: 1
Size: 280040 Color: 1
Size: 258278 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 461772 Color: 0
Size: 269213 Color: 0
Size: 269016 Color: 1

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 461967 Color: 0
Size: 279065 Color: 0
Size: 258969 Color: 1

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 462042 Color: 0
Size: 273635 Color: 1
Size: 264324 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 462094 Color: 1
Size: 281218 Color: 1
Size: 256689 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 462199 Color: 1
Size: 286938 Color: 1
Size: 250864 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 462130 Color: 0
Size: 271921 Color: 1
Size: 265950 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 462305 Color: 1
Size: 274541 Color: 1
Size: 263155 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 462236 Color: 0
Size: 276730 Color: 0
Size: 261035 Color: 1

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 462452 Color: 1
Size: 276251 Color: 0
Size: 261298 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 462689 Color: 1
Size: 277063 Color: 0
Size: 260249 Color: 1

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 462610 Color: 0
Size: 279717 Color: 1
Size: 257674 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 462755 Color: 0
Size: 279322 Color: 1
Size: 257924 Color: 1

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 462839 Color: 0
Size: 283629 Color: 0
Size: 253533 Color: 1

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 462848 Color: 1
Size: 274170 Color: 0
Size: 262983 Color: 1

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 462874 Color: 1
Size: 268740 Color: 1
Size: 268387 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 463112 Color: 1
Size: 275766 Color: 1
Size: 261123 Color: 0

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 463145 Color: 0
Size: 271045 Color: 0
Size: 265811 Color: 1

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 463160 Color: 1
Size: 279283 Color: 0
Size: 257558 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 463204 Color: 1
Size: 273823 Color: 0
Size: 262974 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 463212 Color: 1
Size: 271816 Color: 0
Size: 264973 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 463249 Color: 1
Size: 273928 Color: 1
Size: 262824 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 463384 Color: 1
Size: 271583 Color: 1
Size: 265034 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 463433 Color: 0
Size: 283712 Color: 0
Size: 252856 Color: 1

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 463460 Color: 0
Size: 270070 Color: 1
Size: 266471 Color: 1

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 463464 Color: 0
Size: 276458 Color: 0
Size: 260079 Color: 1

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 463687 Color: 1
Size: 286312 Color: 1
Size: 250002 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 463642 Color: 0
Size: 282197 Color: 1
Size: 254162 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 463700 Color: 1
Size: 279392 Color: 0
Size: 256909 Color: 1

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 463752 Color: 0
Size: 275968 Color: 0
Size: 260281 Color: 1

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 463879 Color: 0
Size: 275775 Color: 1
Size: 260347 Color: 1

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 464054 Color: 0
Size: 283200 Color: 0
Size: 252747 Color: 1

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 464196 Color: 0
Size: 284207 Color: 1
Size: 251598 Color: 1

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 464224 Color: 0
Size: 281766 Color: 1
Size: 254011 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 464257 Color: 1
Size: 275530 Color: 0
Size: 260214 Color: 1

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 464294 Color: 0
Size: 277151 Color: 0
Size: 258556 Color: 1

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 464479 Color: 0
Size: 275434 Color: 0
Size: 260088 Color: 1

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 464412 Color: 1
Size: 268211 Color: 0
Size: 267378 Color: 1

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 464434 Color: 1
Size: 279485 Color: 1
Size: 256082 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 464519 Color: 0
Size: 272770 Color: 0
Size: 262712 Color: 1

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 464581 Color: 1
Size: 277370 Color: 0
Size: 258050 Color: 1

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 464610 Color: 1
Size: 278458 Color: 0
Size: 256933 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 464816 Color: 0
Size: 284315 Color: 0
Size: 250870 Color: 1

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 464804 Color: 1
Size: 277619 Color: 0
Size: 257578 Color: 1

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 464825 Color: 0
Size: 276856 Color: 1
Size: 258320 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 464851 Color: 1
Size: 271796 Color: 0
Size: 263354 Color: 1

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 464990 Color: 1
Size: 271388 Color: 0
Size: 263623 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 464999 Color: 0
Size: 284913 Color: 0
Size: 250089 Color: 1

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 465480 Color: 1
Size: 268738 Color: 1
Size: 265783 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 465710 Color: 0
Size: 283618 Color: 1
Size: 250673 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 465678 Color: 1
Size: 282642 Color: 0
Size: 251681 Color: 1

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 465867 Color: 0
Size: 271161 Color: 1
Size: 262973 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 466149 Color: 1
Size: 267297 Color: 0
Size: 266555 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 466310 Color: 0
Size: 282469 Color: 1
Size: 251222 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 466332 Color: 1
Size: 282171 Color: 1
Size: 251498 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 466448 Color: 0
Size: 279201 Color: 0
Size: 254352 Color: 1

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 466506 Color: 0
Size: 270334 Color: 0
Size: 263161 Color: 1

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 466511 Color: 1
Size: 280420 Color: 0
Size: 253070 Color: 1

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 466586 Color: 0
Size: 270455 Color: 0
Size: 262960 Color: 1

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 466693 Color: 1
Size: 272788 Color: 1
Size: 260520 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 466702 Color: 0
Size: 266678 Color: 1
Size: 266621 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 466869 Color: 0
Size: 276295 Color: 0
Size: 256837 Color: 1

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 466868 Color: 1
Size: 274423 Color: 1
Size: 258710 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 467023 Color: 1
Size: 273094 Color: 0
Size: 259884 Color: 1

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 467043 Color: 0
Size: 281393 Color: 0
Size: 251565 Color: 1

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 467223 Color: 1
Size: 276639 Color: 1
Size: 256139 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 467247 Color: 1
Size: 266652 Color: 0
Size: 266102 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 467332 Color: 1
Size: 282116 Color: 1
Size: 250553 Color: 0

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 467478 Color: 0
Size: 272909 Color: 1
Size: 259614 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 467671 Color: 1
Size: 276463 Color: 0
Size: 255867 Color: 1

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 467762 Color: 1
Size: 276119 Color: 0
Size: 256120 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 467827 Color: 1
Size: 274052 Color: 1
Size: 258122 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 467897 Color: 1
Size: 278019 Color: 0
Size: 254085 Color: 1

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 468067 Color: 0
Size: 272213 Color: 1
Size: 259721 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 468104 Color: 1
Size: 269757 Color: 0
Size: 262140 Color: 1

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 468139 Color: 0
Size: 275879 Color: 1
Size: 255983 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 468110 Color: 1
Size: 281803 Color: 0
Size: 250088 Color: 1

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 468416 Color: 0
Size: 266329 Color: 1
Size: 265256 Color: 1

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 468492 Color: 1
Size: 273187 Color: 0
Size: 258322 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 468633 Color: 0
Size: 266922 Color: 0
Size: 264446 Color: 1

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 468645 Color: 0
Size: 277157 Color: 1
Size: 254199 Color: 1

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 468739 Color: 0
Size: 267419 Color: 1
Size: 263843 Color: 1

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 469007 Color: 1
Size: 278491 Color: 0
Size: 252503 Color: 1

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 469235 Color: 1
Size: 272366 Color: 0
Size: 258400 Color: 1

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 469311 Color: 0
Size: 278906 Color: 1
Size: 251784 Color: 1

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 469373 Color: 1
Size: 271929 Color: 0
Size: 258699 Color: 1

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 469409 Color: 1
Size: 267378 Color: 1
Size: 263214 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 469517 Color: 0
Size: 279157 Color: 0
Size: 251327 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 469480 Color: 1
Size: 271860 Color: 0
Size: 258661 Color: 1

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 469586 Color: 1
Size: 270514 Color: 0
Size: 259901 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 469749 Color: 0
Size: 272386 Color: 1
Size: 257866 Color: 1

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 469775 Color: 0
Size: 275235 Color: 0
Size: 254991 Color: 1

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 469963 Color: 1
Size: 267238 Color: 0
Size: 262800 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 469975 Color: 1
Size: 270478 Color: 0
Size: 259548 Color: 1

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 470061 Color: 0
Size: 266313 Color: 0
Size: 263627 Color: 1

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 470373 Color: 0
Size: 265362 Color: 1
Size: 264266 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 470347 Color: 1
Size: 276051 Color: 1
Size: 253603 Color: 0

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 470366 Color: 1
Size: 271488 Color: 1
Size: 258147 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 470471 Color: 1
Size: 276025 Color: 1
Size: 253505 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 470438 Color: 0
Size: 274925 Color: 0
Size: 254638 Color: 1

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 470477 Color: 1
Size: 269994 Color: 0
Size: 259530 Color: 1

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 470486 Color: 0
Size: 266521 Color: 0
Size: 262994 Color: 1

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 470622 Color: 1
Size: 274128 Color: 1
Size: 255251 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 470558 Color: 0
Size: 275596 Color: 0
Size: 253847 Color: 1

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 470657 Color: 1
Size: 274308 Color: 1
Size: 255036 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 470706 Color: 0
Size: 273941 Color: 0
Size: 255354 Color: 1

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 470955 Color: 0
Size: 278744 Color: 0
Size: 250302 Color: 1

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 470955 Color: 0
Size: 270391 Color: 1
Size: 258655 Color: 1

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 470975 Color: 0
Size: 273606 Color: 1
Size: 255420 Color: 1

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 471071 Color: 1
Size: 273425 Color: 1
Size: 255505 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 471076 Color: 1
Size: 270082 Color: 0
Size: 258843 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 471196 Color: 1
Size: 266043 Color: 1
Size: 262762 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 471243 Color: 1
Size: 266213 Color: 0
Size: 262545 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 471388 Color: 0
Size: 273717 Color: 0
Size: 254896 Color: 1

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 471507 Color: 1
Size: 278463 Color: 0
Size: 250031 Color: 1

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 471475 Color: 0
Size: 268234 Color: 0
Size: 260292 Color: 1

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 471595 Color: 1
Size: 273124 Color: 1
Size: 255282 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 471844 Color: 1
Size: 277987 Color: 0
Size: 250170 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 471849 Color: 1
Size: 264101 Color: 0
Size: 264051 Color: 1

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 471904 Color: 1
Size: 277794 Color: 1
Size: 250303 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 472102 Color: 1
Size: 269330 Color: 0
Size: 258569 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 472210 Color: 0
Size: 264500 Color: 1
Size: 263291 Color: 1

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 472359 Color: 0
Size: 264947 Color: 0
Size: 262695 Color: 1

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 472365 Color: 0
Size: 272881 Color: 1
Size: 254755 Color: 1

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 472372 Color: 0
Size: 274212 Color: 1
Size: 253417 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 472395 Color: 1
Size: 274384 Color: 0
Size: 253222 Color: 1

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 472443 Color: 0
Size: 273582 Color: 0
Size: 253976 Color: 1

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 472521 Color: 1
Size: 270232 Color: 0
Size: 257248 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 472617 Color: 0
Size: 273498 Color: 0
Size: 253886 Color: 1

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 472661 Color: 1
Size: 267479 Color: 1
Size: 259861 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 472726 Color: 0
Size: 269029 Color: 1
Size: 258246 Color: 1

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 472816 Color: 0
Size: 274229 Color: 1
Size: 252956 Color: 1

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 472980 Color: 1
Size: 272529 Color: 1
Size: 254492 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 473444 Color: 0
Size: 263862 Color: 0
Size: 262695 Color: 1

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 473751 Color: 0
Size: 271139 Color: 1
Size: 255111 Color: 1

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 473760 Color: 0
Size: 275768 Color: 1
Size: 250473 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 473788 Color: 0
Size: 272125 Color: 1
Size: 254088 Color: 1

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 473887 Color: 0
Size: 273812 Color: 1
Size: 252302 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 474063 Color: 0
Size: 263848 Color: 1
Size: 262090 Color: 1

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 474252 Color: 1
Size: 270763 Color: 0
Size: 254986 Color: 1

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 474439 Color: 0
Size: 274868 Color: 0
Size: 250694 Color: 1

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 474485 Color: 0
Size: 273118 Color: 1
Size: 252398 Color: 1

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 474552 Color: 1
Size: 266184 Color: 0
Size: 259265 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 474802 Color: 1
Size: 265305 Color: 1
Size: 259894 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 475193 Color: 1
Size: 274143 Color: 0
Size: 250665 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 475445 Color: 1
Size: 269547 Color: 0
Size: 255009 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 475699 Color: 0
Size: 270732 Color: 1
Size: 253570 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 476033 Color: 0
Size: 271460 Color: 1
Size: 252508 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 476291 Color: 1
Size: 267273 Color: 1
Size: 256437 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 476527 Color: 0
Size: 262089 Color: 0
Size: 261385 Color: 1

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 476623 Color: 1
Size: 268434 Color: 0
Size: 254944 Color: 1

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 476652 Color: 1
Size: 262187 Color: 0
Size: 261162 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 476717 Color: 1
Size: 264652 Color: 0
Size: 258632 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 476797 Color: 1
Size: 264619 Color: 1
Size: 258585 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 476846 Color: 1
Size: 261823 Color: 0
Size: 261332 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 476860 Color: 0
Size: 265889 Color: 1
Size: 257252 Color: 1

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 477017 Color: 0
Size: 263792 Color: 1
Size: 259192 Color: 1

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 477091 Color: 0
Size: 271674 Color: 0
Size: 251236 Color: 1

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 477319 Color: 1
Size: 263105 Color: 0
Size: 259577 Color: 1

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 477427 Color: 0
Size: 268348 Color: 1
Size: 254226 Color: 1

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 477514 Color: 0
Size: 264173 Color: 1
Size: 258314 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 477553 Color: 1
Size: 270327 Color: 0
Size: 252121 Color: 1

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 477579 Color: 0
Size: 261343 Color: 1
Size: 261079 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 477867 Color: 1
Size: 264072 Color: 1
Size: 258062 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 477887 Color: 0
Size: 266424 Color: 0
Size: 255690 Color: 1

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 477883 Color: 1
Size: 266949 Color: 0
Size: 255169 Color: 1

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 477894 Color: 0
Size: 268613 Color: 1
Size: 253494 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 478106 Color: 1
Size: 263383 Color: 0
Size: 258512 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 478432 Color: 0
Size: 266055 Color: 1
Size: 255514 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 478594 Color: 0
Size: 265244 Color: 0
Size: 256163 Color: 1

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 478719 Color: 1
Size: 271158 Color: 0
Size: 250124 Color: 1

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 478762 Color: 0
Size: 262394 Color: 0
Size: 258845 Color: 1

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 478819 Color: 0
Size: 270466 Color: 1
Size: 250716 Color: 1

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 478925 Color: 1
Size: 270135 Color: 0
Size: 250941 Color: 1

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 478925 Color: 0
Size: 261563 Color: 0
Size: 259513 Color: 1

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 479146 Color: 1
Size: 264841 Color: 1
Size: 256014 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 479213 Color: 0
Size: 260467 Color: 0
Size: 260321 Color: 1

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 479395 Color: 0
Size: 270365 Color: 0
Size: 250241 Color: 1

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 479654 Color: 0
Size: 262886 Color: 1
Size: 257461 Color: 1

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 479660 Color: 0
Size: 261532 Color: 0
Size: 258809 Color: 1

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 479721 Color: 0
Size: 267594 Color: 1
Size: 252686 Color: 1

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 479742 Color: 1
Size: 268846 Color: 1
Size: 251413 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 480085 Color: 1
Size: 262997 Color: 0
Size: 256919 Color: 1

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 480400 Color: 0
Size: 260181 Color: 0
Size: 259420 Color: 1

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 480487 Color: 1
Size: 267284 Color: 1
Size: 252230 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 480442 Color: 0
Size: 261848 Color: 0
Size: 257711 Color: 1

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 480534 Color: 1
Size: 262765 Color: 1
Size: 256702 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 480819 Color: 0
Size: 265446 Color: 0
Size: 253736 Color: 1

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 480873 Color: 1
Size: 265096 Color: 0
Size: 254032 Color: 1

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 481282 Color: 1
Size: 261145 Color: 0
Size: 257574 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 481284 Color: 1
Size: 259906 Color: 0
Size: 258811 Color: 1

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 481435 Color: 0
Size: 260726 Color: 1
Size: 257840 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 481565 Color: 1
Size: 260043 Color: 0
Size: 258393 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 481576 Color: 0
Size: 260027 Color: 0
Size: 258398 Color: 1

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 481801 Color: 0
Size: 260368 Color: 0
Size: 257832 Color: 1

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 481903 Color: 0
Size: 267338 Color: 1
Size: 250760 Color: 1

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 482015 Color: 0
Size: 266115 Color: 1
Size: 251871 Color: 0

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 482020 Color: 0
Size: 261104 Color: 1
Size: 256877 Color: 1

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 482115 Color: 0
Size: 263981 Color: 1
Size: 253905 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 482291 Color: 0
Size: 259174 Color: 1
Size: 258536 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 482335 Color: 0
Size: 266970 Color: 1
Size: 250696 Color: 1

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 482369 Color: 1
Size: 258978 Color: 0
Size: 258654 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 482412 Color: 1
Size: 264356 Color: 0
Size: 253233 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 482528 Color: 1
Size: 261231 Color: 0
Size: 256242 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 482600 Color: 1
Size: 267014 Color: 1
Size: 250387 Color: 0

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 482664 Color: 1
Size: 261822 Color: 0
Size: 255515 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 482879 Color: 1
Size: 263237 Color: 0
Size: 253885 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 483381 Color: 0
Size: 259776 Color: 0
Size: 256844 Color: 1

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 483360 Color: 1
Size: 261544 Color: 1
Size: 255097 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 483577 Color: 1
Size: 262699 Color: 0
Size: 253725 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 483578 Color: 1
Size: 265560 Color: 0
Size: 250863 Color: 1

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 483685 Color: 0
Size: 264910 Color: 1
Size: 251406 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 483730 Color: 1
Size: 261662 Color: 1
Size: 254609 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 483931 Color: 0
Size: 263244 Color: 0
Size: 252826 Color: 1

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 484108 Color: 1
Size: 258241 Color: 1
Size: 257652 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 484235 Color: 0
Size: 261388 Color: 1
Size: 254378 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 484441 Color: 1
Size: 264424 Color: 1
Size: 251136 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 484954 Color: 1
Size: 263173 Color: 0
Size: 251874 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 485940 Color: 1
Size: 257551 Color: 0
Size: 256510 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 486031 Color: 0
Size: 262951 Color: 1
Size: 251019 Color: 1

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 485997 Color: 1
Size: 263467 Color: 0
Size: 250537 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 486270 Color: 0
Size: 260745 Color: 1
Size: 252986 Color: 1

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 486311 Color: 1
Size: 259222 Color: 0
Size: 254468 Color: 1

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 486348 Color: 1
Size: 263267 Color: 0
Size: 250386 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 486614 Color: 0
Size: 261698 Color: 0
Size: 251689 Color: 1

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 486662 Color: 0
Size: 262795 Color: 1
Size: 250544 Color: 1

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 486899 Color: 1
Size: 260370 Color: 1
Size: 252732 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 486939 Color: 0
Size: 261118 Color: 0
Size: 251944 Color: 1

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 486953 Color: 1
Size: 260711 Color: 0
Size: 252337 Color: 1

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 487079 Color: 1
Size: 258052 Color: 0
Size: 254870 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 487104 Color: 1
Size: 261841 Color: 0
Size: 251056 Color: 1

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 487133 Color: 1
Size: 258646 Color: 1
Size: 254222 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 487235 Color: 1
Size: 257586 Color: 0
Size: 255180 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 487628 Color: 0
Size: 259185 Color: 1
Size: 253188 Color: 0

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 487803 Color: 0
Size: 256799 Color: 1
Size: 255399 Color: 1

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 488132 Color: 1
Size: 260559 Color: 0
Size: 251310 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 488233 Color: 0
Size: 256766 Color: 0
Size: 255002 Color: 1

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 488331 Color: 0
Size: 260981 Color: 1
Size: 250689 Color: 1

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 488389 Color: 0
Size: 256795 Color: 1
Size: 254817 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 488787 Color: 1
Size: 260104 Color: 1
Size: 251110 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 488833 Color: 1
Size: 258327 Color: 0
Size: 252841 Color: 1

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 488926 Color: 1
Size: 260174 Color: 1
Size: 250901 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 488931 Color: 1
Size: 258261 Color: 1
Size: 252809 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 488955 Color: 0
Size: 258997 Color: 1
Size: 252049 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 489087 Color: 0
Size: 256665 Color: 1
Size: 254249 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 489115 Color: 1
Size: 257162 Color: 1
Size: 253724 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 489172 Color: 1
Size: 259558 Color: 1
Size: 251271 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 489205 Color: 1
Size: 258030 Color: 0
Size: 252766 Color: 1

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 489266 Color: 0
Size: 260054 Color: 1
Size: 250681 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 489602 Color: 1
Size: 259473 Color: 0
Size: 250926 Color: 1

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 489622 Color: 0
Size: 260231 Color: 0
Size: 250148 Color: 1

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 489658 Color: 1
Size: 255444 Color: 0
Size: 254899 Color: 1

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 489677 Color: 0
Size: 259961 Color: 1
Size: 250363 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 489843 Color: 1
Size: 258188 Color: 1
Size: 251970 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 490092 Color: 0
Size: 259648 Color: 0
Size: 250261 Color: 1

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 490287 Color: 0
Size: 257393 Color: 1
Size: 252321 Color: 1

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 490594 Color: 1
Size: 258399 Color: 0
Size: 251008 Color: 1

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 490597 Color: 1
Size: 256137 Color: 0
Size: 253267 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 490767 Color: 0
Size: 254819 Color: 1
Size: 254415 Color: 1

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 490676 Color: 1
Size: 258207 Color: 0
Size: 251118 Color: 1

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 490966 Color: 1
Size: 255898 Color: 1
Size: 253137 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 491129 Color: 1
Size: 255574 Color: 1
Size: 253298 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 491219 Color: 0
Size: 254959 Color: 0
Size: 253823 Color: 1

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 491303 Color: 0
Size: 255220 Color: 0
Size: 253478 Color: 1

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 491307 Color: 0
Size: 257429 Color: 1
Size: 251265 Color: 1

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 491331 Color: 0
Size: 258635 Color: 1
Size: 250035 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 491528 Color: 0
Size: 256581 Color: 1
Size: 251892 Color: 1

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 491636 Color: 0
Size: 254348 Color: 1
Size: 254017 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 491668 Color: 0
Size: 255941 Color: 1
Size: 252392 Color: 1

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 491742 Color: 0
Size: 255529 Color: 0
Size: 252730 Color: 1

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 491758 Color: 1
Size: 257186 Color: 0
Size: 251057 Color: 1

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 491840 Color: 1
Size: 255533 Color: 0
Size: 252628 Color: 0

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 491955 Color: 0
Size: 255809 Color: 1
Size: 252237 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 492187 Color: 1
Size: 257009 Color: 1
Size: 250805 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 492193 Color: 0
Size: 255648 Color: 0
Size: 252160 Color: 1

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 492386 Color: 0
Size: 254845 Color: 0
Size: 252770 Color: 1

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 492498 Color: 1
Size: 256277 Color: 0
Size: 251226 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 492617 Color: 0
Size: 254256 Color: 1
Size: 253128 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 492681 Color: 1
Size: 254926 Color: 0
Size: 252394 Color: 1

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 492756 Color: 1
Size: 256019 Color: 0
Size: 251226 Color: 1

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 492942 Color: 1
Size: 255328 Color: 0
Size: 251731 Color: 1

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 493219 Color: 0
Size: 255992 Color: 1
Size: 250790 Color: 1

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 493325 Color: 0
Size: 254457 Color: 1
Size: 252219 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 493279 Color: 1
Size: 256167 Color: 1
Size: 250555 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 493430 Color: 1
Size: 255167 Color: 1
Size: 251404 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 493601 Color: 1
Size: 253233 Color: 1
Size: 253167 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 493578 Color: 0
Size: 254814 Color: 1
Size: 251609 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 493715 Color: 1
Size: 253867 Color: 0
Size: 252419 Color: 1

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 493689 Color: 0
Size: 255084 Color: 1
Size: 251228 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 493871 Color: 0
Size: 255855 Color: 0
Size: 250275 Color: 1

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 493978 Color: 1
Size: 255488 Color: 0
Size: 250535 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 494081 Color: 0
Size: 255268 Color: 1
Size: 250652 Color: 1

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 494144 Color: 0
Size: 254193 Color: 0
Size: 251664 Color: 1

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 494421 Color: 0
Size: 253681 Color: 1
Size: 251899 Color: 1

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 494475 Color: 1
Size: 254674 Color: 0
Size: 250852 Color: 1

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 494511 Color: 1
Size: 252911 Color: 0
Size: 252579 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 494838 Color: 1
Size: 254151 Color: 0
Size: 251012 Color: 1

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 494890 Color: 0
Size: 254218 Color: 0
Size: 250893 Color: 1

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 495047 Color: 0
Size: 254064 Color: 1
Size: 250890 Color: 1

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 495052 Color: 0
Size: 254135 Color: 1
Size: 250814 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 495070 Color: 1
Size: 254581 Color: 1
Size: 250350 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 495083 Color: 1
Size: 253146 Color: 1
Size: 251772 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 495181 Color: 1
Size: 254115 Color: 0
Size: 250705 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 495420 Color: 0
Size: 253010 Color: 0
Size: 251571 Color: 1

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 495473 Color: 0
Size: 254075 Color: 1
Size: 250453 Color: 1

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 495636 Color: 1
Size: 252369 Color: 0
Size: 251996 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 495643 Color: 1
Size: 253709 Color: 0
Size: 250649 Color: 1

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 495677 Color: 1
Size: 253056 Color: 0
Size: 251268 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 495736 Color: 0
Size: 253716 Color: 1
Size: 250549 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 339959 Color: 0
Size: 336956 Color: 1
Size: 323086 Color: 0

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 495766 Color: 0
Size: 252253 Color: 0
Size: 251982 Color: 1

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 495805 Color: 0
Size: 253234 Color: 1
Size: 250962 Color: 1

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 497035 Color: 0
Size: 251828 Color: 1
Size: 251138 Color: 1

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 496092 Color: 0
Size: 252026 Color: 1
Size: 251883 Color: 1

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 496273 Color: 0
Size: 253298 Color: 0
Size: 250430 Color: 1

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 496337 Color: 0
Size: 252339 Color: 1
Size: 251325 Color: 1

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 496462 Color: 1
Size: 253332 Color: 0
Size: 250207 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 496661 Color: 0
Size: 251840 Color: 0
Size: 251500 Color: 1

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 496773 Color: 0
Size: 253027 Color: 1
Size: 250201 Color: 1

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 402347 Color: 0
Size: 342122 Color: 0
Size: 255532 Color: 1

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 496900 Color: 0
Size: 252954 Color: 0
Size: 250147 Color: 1

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 496941 Color: 1
Size: 252742 Color: 1
Size: 250318 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 496978 Color: 0
Size: 252537 Color: 0
Size: 250486 Color: 1

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 497124 Color: 0
Size: 252876 Color: 0
Size: 250001 Color: 1

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 497170 Color: 0
Size: 251943 Color: 1
Size: 250888 Color: 1

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 497186 Color: 0
Size: 252787 Color: 1
Size: 250028 Color: 0

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 497199 Color: 0
Size: 252105 Color: 1
Size: 250697 Color: 1

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 497320 Color: 1
Size: 252505 Color: 0
Size: 250176 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 497539 Color: 1
Size: 251519 Color: 0
Size: 250943 Color: 1

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 497563 Color: 1
Size: 251323 Color: 1
Size: 251115 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 497667 Color: 1
Size: 251877 Color: 0
Size: 250457 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 497711 Color: 0
Size: 251420 Color: 1
Size: 250870 Color: 0

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 497796 Color: 1
Size: 251370 Color: 0
Size: 250835 Color: 0

Bin 2946: 1 of cap free
Amount of items: 3
Items: 
Size: 387610 Color: 0
Size: 318784 Color: 1
Size: 293606 Color: 1

Bin 2947: 1 of cap free
Amount of items: 3
Items: 
Size: 381066 Color: 1
Size: 365134 Color: 0
Size: 253800 Color: 1

Bin 2948: 1 of cap free
Amount of items: 3
Items: 
Size: 375617 Color: 0
Size: 327026 Color: 1
Size: 297357 Color: 1

Bin 2949: 1 of cap free
Amount of items: 3
Items: 
Size: 359407 Color: 1
Size: 327198 Color: 0
Size: 313395 Color: 1

Bin 2950: 1 of cap free
Amount of items: 3
Items: 
Size: 351156 Color: 0
Size: 333253 Color: 0
Size: 315591 Color: 1

Bin 2951: 1 of cap free
Amount of items: 3
Items: 
Size: 353175 Color: 1
Size: 339206 Color: 0
Size: 307619 Color: 0

Bin 2952: 1 of cap free
Amount of items: 3
Items: 
Size: 346182 Color: 1
Size: 327643 Color: 0
Size: 326175 Color: 0

Bin 2953: 1 of cap free
Amount of items: 3
Items: 
Size: 473899 Color: 0
Size: 264173 Color: 1
Size: 261928 Color: 1

Bin 2954: 1 of cap free
Amount of items: 3
Items: 
Size: 360242 Color: 0
Size: 327772 Color: 0
Size: 311986 Color: 1

Bin 2955: 1 of cap free
Amount of items: 3
Items: 
Size: 381402 Color: 0
Size: 346364 Color: 1
Size: 272234 Color: 1

Bin 2956: 1 of cap free
Amount of items: 3
Items: 
Size: 482335 Color: 0
Size: 259233 Color: 0
Size: 258432 Color: 1

Bin 2957: 1 of cap free
Amount of items: 3
Items: 
Size: 409886 Color: 0
Size: 326205 Color: 0
Size: 263909 Color: 1

Bin 2958: 1 of cap free
Amount of items: 3
Items: 
Size: 338720 Color: 1
Size: 333204 Color: 0
Size: 328076 Color: 0

Bin 2959: 1 of cap free
Amount of items: 3
Items: 
Size: 389710 Color: 0
Size: 345024 Color: 0
Size: 265266 Color: 1

Bin 2960: 1 of cap free
Amount of items: 3
Items: 
Size: 386932 Color: 1
Size: 352131 Color: 0
Size: 260937 Color: 1

Bin 2961: 1 of cap free
Amount of items: 3
Items: 
Size: 349914 Color: 0
Size: 334281 Color: 1
Size: 315805 Color: 0

Bin 2962: 1 of cap free
Amount of items: 3
Items: 
Size: 353025 Color: 0
Size: 329490 Color: 1
Size: 317485 Color: 0

Bin 2963: 1 of cap free
Amount of items: 3
Items: 
Size: 354241 Color: 0
Size: 345312 Color: 1
Size: 300447 Color: 0

Bin 2964: 1 of cap free
Amount of items: 3
Items: 
Size: 376928 Color: 0
Size: 347123 Color: 1
Size: 275949 Color: 0

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 493985 Color: 0
Size: 255846 Color: 0
Size: 250169 Color: 1

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 401972 Color: 1
Size: 335088 Color: 1
Size: 262940 Color: 0

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 341169 Color: 1
Size: 337927 Color: 1
Size: 320904 Color: 0

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 350543 Color: 0
Size: 342793 Color: 0
Size: 306664 Color: 1

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 394071 Color: 0
Size: 355483 Color: 1
Size: 250446 Color: 1

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 378619 Color: 1
Size: 357637 Color: 1
Size: 263744 Color: 0

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 356824 Color: 0
Size: 333399 Color: 0
Size: 309777 Color: 1

Bin 2972: 1 of cap free
Amount of items: 3
Items: 
Size: 363911 Color: 1
Size: 348887 Color: 0
Size: 287202 Color: 1

Bin 2973: 1 of cap free
Amount of items: 3
Items: 
Size: 351670 Color: 0
Size: 350869 Color: 1
Size: 297461 Color: 0

Bin 2974: 1 of cap free
Amount of items: 3
Items: 
Size: 425893 Color: 1
Size: 320358 Color: 0
Size: 253749 Color: 0

Bin 2975: 1 of cap free
Amount of items: 3
Items: 
Size: 421616 Color: 0
Size: 300051 Color: 1
Size: 278333 Color: 1

Bin 2976: 1 of cap free
Amount of items: 3
Items: 
Size: 372312 Color: 1
Size: 362206 Color: 0
Size: 265482 Color: 0

Bin 2977: 1 of cap free
Amount of items: 3
Items: 
Size: 358291 Color: 1
Size: 331266 Color: 0
Size: 310443 Color: 0

Bin 2978: 1 of cap free
Amount of items: 3
Items: 
Size: 402961 Color: 0
Size: 346323 Color: 1
Size: 250716 Color: 1

Bin 2979: 1 of cap free
Amount of items: 3
Items: 
Size: 479393 Color: 1
Size: 266070 Color: 1
Size: 254537 Color: 0

Bin 2980: 1 of cap free
Amount of items: 3
Items: 
Size: 355370 Color: 0
Size: 338136 Color: 1
Size: 306494 Color: 1

Bin 2981: 1 of cap free
Amount of items: 3
Items: 
Size: 459737 Color: 1
Size: 271380 Color: 0
Size: 268883 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 350876 Color: 0
Size: 333201 Color: 1
Size: 315923 Color: 0

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 363101 Color: 0
Size: 339568 Color: 0
Size: 297331 Color: 1

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 392455 Color: 1
Size: 343924 Color: 0
Size: 263621 Color: 0

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 395682 Color: 1
Size: 319659 Color: 0
Size: 284659 Color: 0

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 354074 Color: 1
Size: 329295 Color: 0
Size: 316631 Color: 1

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 386469 Color: 1
Size: 310536 Color: 1
Size: 302995 Color: 0

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 354287 Color: 1
Size: 349004 Color: 1
Size: 296709 Color: 0

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 490261 Color: 0
Size: 255053 Color: 0
Size: 254686 Color: 1

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 362261 Color: 1
Size: 361546 Color: 0
Size: 276193 Color: 1

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 369897 Color: 0
Size: 360654 Color: 0
Size: 269449 Color: 1

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 373944 Color: 0
Size: 352723 Color: 1
Size: 273333 Color: 1

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 349020 Color: 1
Size: 339648 Color: 0
Size: 311332 Color: 1

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 352409 Color: 1
Size: 347725 Color: 0
Size: 299866 Color: 0

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 363245 Color: 1
Size: 319350 Color: 0
Size: 317405 Color: 1

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 349618 Color: 1
Size: 331999 Color: 0
Size: 318383 Color: 0

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 374767 Color: 1
Size: 343820 Color: 1
Size: 281413 Color: 0

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 359566 Color: 0
Size: 328336 Color: 1
Size: 312098 Color: 1

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 450876 Color: 0
Size: 275346 Color: 0
Size: 273778 Color: 1

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 361307 Color: 0
Size: 343792 Color: 0
Size: 294901 Color: 1

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 397525 Color: 0
Size: 345760 Color: 0
Size: 256715 Color: 1

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 348664 Color: 1
Size: 341054 Color: 1
Size: 310282 Color: 0

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 345126 Color: 0
Size: 332747 Color: 1
Size: 322127 Color: 1

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 364152 Color: 1
Size: 354937 Color: 0
Size: 280911 Color: 0

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 353860 Color: 1
Size: 353655 Color: 1
Size: 292485 Color: 0

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 351517 Color: 0
Size: 329317 Color: 0
Size: 319166 Color: 1

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 362731 Color: 1
Size: 342725 Color: 0
Size: 294544 Color: 1

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 365369 Color: 0
Size: 360665 Color: 0
Size: 273966 Color: 1

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 366912 Color: 1
Size: 320762 Color: 0
Size: 312326 Color: 0

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 349929 Color: 1
Size: 327061 Color: 0
Size: 323010 Color: 1

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 358254 Color: 0
Size: 352573 Color: 1
Size: 289173 Color: 1

Bin 3012: 1 of cap free
Amount of items: 3
Items: 
Size: 435766 Color: 1
Size: 283193 Color: 0
Size: 281041 Color: 1

Bin 3013: 1 of cap free
Amount of items: 3
Items: 
Size: 352159 Color: 0
Size: 332968 Color: 1
Size: 314873 Color: 1

Bin 3014: 1 of cap free
Amount of items: 3
Items: 
Size: 421328 Color: 0
Size: 309280 Color: 0
Size: 269392 Color: 1

Bin 3015: 1 of cap free
Amount of items: 3
Items: 
Size: 406327 Color: 0
Size: 335286 Color: 1
Size: 258387 Color: 0

Bin 3016: 1 of cap free
Amount of items: 3
Items: 
Size: 399687 Color: 1
Size: 348611 Color: 1
Size: 251702 Color: 0

Bin 3017: 1 of cap free
Amount of items: 3
Items: 
Size: 353201 Color: 0
Size: 341728 Color: 0
Size: 305071 Color: 1

Bin 3018: 1 of cap free
Amount of items: 3
Items: 
Size: 410568 Color: 0
Size: 316163 Color: 1
Size: 273269 Color: 0

Bin 3019: 1 of cap free
Amount of items: 3
Items: 
Size: 358351 Color: 1
Size: 352943 Color: 1
Size: 288706 Color: 0

Bin 3020: 1 of cap free
Amount of items: 3
Items: 
Size: 407611 Color: 0
Size: 326801 Color: 0
Size: 265588 Color: 1

Bin 3021: 1 of cap free
Amount of items: 3
Items: 
Size: 381836 Color: 0
Size: 360661 Color: 0
Size: 257503 Color: 1

Bin 3022: 2 of cap free
Amount of items: 3
Items: 
Size: 402741 Color: 0
Size: 317320 Color: 1
Size: 279938 Color: 0

Bin 3023: 2 of cap free
Amount of items: 3
Items: 
Size: 354772 Color: 0
Size: 326704 Color: 1
Size: 318523 Color: 0

Bin 3024: 2 of cap free
Amount of items: 3
Items: 
Size: 494324 Color: 1
Size: 253807 Color: 0
Size: 251868 Color: 0

Bin 3025: 2 of cap free
Amount of items: 3
Items: 
Size: 457570 Color: 0
Size: 271795 Color: 1
Size: 270634 Color: 0

Bin 3026: 2 of cap free
Amount of items: 3
Items: 
Size: 499201 Color: 1
Size: 250616 Color: 1
Size: 250182 Color: 0

Bin 3027: 2 of cap free
Amount of items: 3
Items: 
Size: 353275 Color: 1
Size: 330489 Color: 1
Size: 316235 Color: 0

Bin 3028: 2 of cap free
Amount of items: 3
Items: 
Size: 342323 Color: 0
Size: 339887 Color: 0
Size: 317789 Color: 1

Bin 3029: 2 of cap free
Amount of items: 3
Items: 
Size: 493750 Color: 1
Size: 253260 Color: 0
Size: 252989 Color: 0

Bin 3030: 2 of cap free
Amount of items: 3
Items: 
Size: 454943 Color: 0
Size: 291950 Color: 1
Size: 253106 Color: 1

Bin 3031: 2 of cap free
Amount of items: 3
Items: 
Size: 379813 Color: 1
Size: 329725 Color: 0
Size: 290461 Color: 0

Bin 3032: 2 of cap free
Amount of items: 3
Items: 
Size: 443041 Color: 1
Size: 282583 Color: 0
Size: 274375 Color: 1

Bin 3033: 2 of cap free
Amount of items: 3
Items: 
Size: 356600 Color: 1
Size: 349514 Color: 1
Size: 293885 Color: 0

Bin 3034: 2 of cap free
Amount of items: 3
Items: 
Size: 424200 Color: 1
Size: 319143 Color: 1
Size: 256656 Color: 0

Bin 3035: 2 of cap free
Amount of items: 3
Items: 
Size: 364795 Color: 0
Size: 319830 Color: 0
Size: 315374 Color: 1

Bin 3036: 2 of cap free
Amount of items: 3
Items: 
Size: 476110 Color: 0
Size: 271830 Color: 1
Size: 252059 Color: 1

Bin 3037: 2 of cap free
Amount of items: 3
Items: 
Size: 345624 Color: 1
Size: 333168 Color: 0
Size: 321207 Color: 0

Bin 3038: 2 of cap free
Amount of items: 3
Items: 
Size: 418464 Color: 1
Size: 310251 Color: 0
Size: 271284 Color: 1

Bin 3039: 2 of cap free
Amount of items: 3
Items: 
Size: 349838 Color: 1
Size: 345721 Color: 1
Size: 304440 Color: 0

Bin 3040: 2 of cap free
Amount of items: 3
Items: 
Size: 411654 Color: 0
Size: 338179 Color: 1
Size: 250166 Color: 1

Bin 3041: 2 of cap free
Amount of items: 3
Items: 
Size: 484873 Color: 1
Size: 261295 Color: 1
Size: 253831 Color: 0

Bin 3042: 2 of cap free
Amount of items: 3
Items: 
Size: 359161 Color: 0
Size: 352969 Color: 0
Size: 287869 Color: 1

Bin 3043: 2 of cap free
Amount of items: 3
Items: 
Size: 364651 Color: 0
Size: 340210 Color: 1
Size: 295138 Color: 0

Bin 3044: 2 of cap free
Amount of items: 3
Items: 
Size: 461742 Color: 0
Size: 273204 Color: 1
Size: 265053 Color: 1

Bin 3045: 2 of cap free
Amount of items: 3
Items: 
Size: 455340 Color: 1
Size: 294433 Color: 0
Size: 250226 Color: 0

Bin 3046: 2 of cap free
Amount of items: 3
Items: 
Size: 388279 Color: 1
Size: 358149 Color: 0
Size: 253571 Color: 1

Bin 3047: 2 of cap free
Amount of items: 3
Items: 
Size: 415432 Color: 1
Size: 292614 Color: 0
Size: 291953 Color: 0

Bin 3048: 2 of cap free
Amount of items: 3
Items: 
Size: 443685 Color: 1
Size: 291393 Color: 0
Size: 264921 Color: 1

Bin 3049: 2 of cap free
Amount of items: 3
Items: 
Size: 362524 Color: 1
Size: 360300 Color: 0
Size: 277175 Color: 1

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 444451 Color: 0
Size: 300906 Color: 0
Size: 254642 Color: 1

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 383602 Color: 0
Size: 338490 Color: 1
Size: 277907 Color: 1

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 360568 Color: 1
Size: 335949 Color: 0
Size: 303482 Color: 1

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 353508 Color: 1
Size: 329757 Color: 0
Size: 316734 Color: 1

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 363967 Color: 1
Size: 363083 Color: 0
Size: 272949 Color: 1

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 346248 Color: 0
Size: 338039 Color: 1
Size: 315712 Color: 0

Bin 3056: 2 of cap free
Amount of items: 3
Items: 
Size: 462919 Color: 0
Size: 269093 Color: 1
Size: 267987 Color: 1

Bin 3057: 2 of cap free
Amount of items: 3
Items: 
Size: 499381 Color: 1
Size: 250560 Color: 0
Size: 250058 Color: 0

Bin 3058: 2 of cap free
Amount of items: 3
Items: 
Size: 479875 Color: 0
Size: 266000 Color: 1
Size: 254124 Color: 0

Bin 3059: 2 of cap free
Amount of items: 3
Items: 
Size: 358209 Color: 1
Size: 321784 Color: 0
Size: 320006 Color: 0

Bin 3060: 2 of cap free
Amount of items: 3
Items: 
Size: 357907 Color: 0
Size: 353994 Color: 1
Size: 288098 Color: 0

Bin 3061: 2 of cap free
Amount of items: 3
Items: 
Size: 349852 Color: 0
Size: 336075 Color: 1
Size: 314072 Color: 0

Bin 3062: 2 of cap free
Amount of items: 3
Items: 
Size: 364324 Color: 0
Size: 363828 Color: 1
Size: 271847 Color: 1

Bin 3063: 2 of cap free
Amount of items: 3
Items: 
Size: 347595 Color: 1
Size: 332222 Color: 0
Size: 320182 Color: 1

Bin 3064: 2 of cap free
Amount of items: 3
Items: 
Size: 373623 Color: 0
Size: 314944 Color: 0
Size: 311432 Color: 1

Bin 3065: 2 of cap free
Amount of items: 3
Items: 
Size: 350763 Color: 1
Size: 328126 Color: 0
Size: 321110 Color: 1

Bin 3066: 2 of cap free
Amount of items: 3
Items: 
Size: 403787 Color: 0
Size: 323874 Color: 0
Size: 272338 Color: 1

Bin 3067: 2 of cap free
Amount of items: 3
Items: 
Size: 435728 Color: 0
Size: 314159 Color: 1
Size: 250112 Color: 0

Bin 3068: 2 of cap free
Amount of items: 3
Items: 
Size: 436422 Color: 0
Size: 289488 Color: 1
Size: 274089 Color: 1

Bin 3069: 2 of cap free
Amount of items: 3
Items: 
Size: 441025 Color: 1
Size: 289959 Color: 0
Size: 269015 Color: 1

Bin 3070: 3 of cap free
Amount of items: 3
Items: 
Size: 369139 Color: 0
Size: 343736 Color: 1
Size: 287123 Color: 0

Bin 3071: 3 of cap free
Amount of items: 3
Items: 
Size: 475176 Color: 1
Size: 267387 Color: 0
Size: 257435 Color: 0

Bin 3072: 3 of cap free
Amount of items: 3
Items: 
Size: 429371 Color: 0
Size: 314754 Color: 0
Size: 255873 Color: 1

Bin 3073: 3 of cap free
Amount of items: 3
Items: 
Size: 342744 Color: 1
Size: 328932 Color: 1
Size: 328322 Color: 0

Bin 3074: 3 of cap free
Amount of items: 3
Items: 
Size: 336908 Color: 1
Size: 332190 Color: 1
Size: 330900 Color: 0

Bin 3075: 3 of cap free
Amount of items: 3
Items: 
Size: 429077 Color: 1
Size: 320005 Color: 0
Size: 250916 Color: 0

Bin 3076: 3 of cap free
Amount of items: 3
Items: 
Size: 345257 Color: 1
Size: 333508 Color: 0
Size: 321233 Color: 0

Bin 3077: 3 of cap free
Amount of items: 3
Items: 
Size: 407052 Color: 1
Size: 329520 Color: 1
Size: 263426 Color: 0

Bin 3078: 3 of cap free
Amount of items: 3
Items: 
Size: 366013 Color: 1
Size: 318387 Color: 0
Size: 315598 Color: 1

Bin 3079: 3 of cap free
Amount of items: 3
Items: 
Size: 357847 Color: 1
Size: 331901 Color: 0
Size: 310250 Color: 0

Bin 3080: 3 of cap free
Amount of items: 3
Items: 
Size: 405530 Color: 1
Size: 340707 Color: 1
Size: 253761 Color: 0

Bin 3081: 3 of cap free
Amount of items: 3
Items: 
Size: 360944 Color: 0
Size: 350163 Color: 1
Size: 288891 Color: 0

Bin 3082: 3 of cap free
Amount of items: 3
Items: 
Size: 416740 Color: 1
Size: 318282 Color: 0
Size: 264976 Color: 1

Bin 3083: 3 of cap free
Amount of items: 3
Items: 
Size: 366103 Color: 1
Size: 346825 Color: 0
Size: 287070 Color: 0

Bin 3084: 3 of cap free
Amount of items: 3
Items: 
Size: 381308 Color: 0
Size: 316963 Color: 0
Size: 301727 Color: 1

Bin 3085: 3 of cap free
Amount of items: 3
Items: 
Size: 348082 Color: 0
Size: 345135 Color: 1
Size: 306781 Color: 1

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 379893 Color: 1
Size: 334433 Color: 0
Size: 285672 Color: 1

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 433321 Color: 0
Size: 306260 Color: 1
Size: 260417 Color: 0

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 413522 Color: 1
Size: 327826 Color: 0
Size: 258650 Color: 1

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 482366 Color: 0
Size: 259699 Color: 0
Size: 257933 Color: 1

Bin 3090: 4 of cap free
Amount of items: 3
Items: 
Size: 346158 Color: 1
Size: 339984 Color: 1
Size: 313855 Color: 0

Bin 3091: 4 of cap free
Amount of items: 3
Items: 
Size: 410687 Color: 0
Size: 325357 Color: 1
Size: 263953 Color: 0

Bin 3092: 4 of cap free
Amount of items: 3
Items: 
Size: 432733 Color: 1
Size: 285359 Color: 0
Size: 281905 Color: 1

Bin 3093: 4 of cap free
Amount of items: 3
Items: 
Size: 485809 Color: 1
Size: 262938 Color: 0
Size: 251250 Color: 1

Bin 3094: 4 of cap free
Amount of items: 3
Items: 
Size: 445687 Color: 1
Size: 301779 Color: 1
Size: 252531 Color: 0

Bin 3095: 4 of cap free
Amount of items: 3
Items: 
Size: 422023 Color: 1
Size: 321909 Color: 0
Size: 256065 Color: 1

Bin 3096: 4 of cap free
Amount of items: 3
Items: 
Size: 367140 Color: 1
Size: 348299 Color: 0
Size: 284558 Color: 0

Bin 3097: 4 of cap free
Amount of items: 3
Items: 
Size: 381916 Color: 1
Size: 356252 Color: 0
Size: 261829 Color: 1

Bin 3098: 4 of cap free
Amount of items: 3
Items: 
Size: 345206 Color: 1
Size: 331929 Color: 1
Size: 322862 Color: 0

Bin 3099: 4 of cap free
Amount of items: 3
Items: 
Size: 352862 Color: 1
Size: 336324 Color: 0
Size: 310811 Color: 1

Bin 3100: 4 of cap free
Amount of items: 3
Items: 
Size: 360767 Color: 1
Size: 349167 Color: 0
Size: 290063 Color: 1

Bin 3101: 4 of cap free
Amount of items: 3
Items: 
Size: 344710 Color: 0
Size: 339766 Color: 0
Size: 315521 Color: 1

Bin 3102: 4 of cap free
Amount of items: 3
Items: 
Size: 354978 Color: 0
Size: 350802 Color: 0
Size: 294217 Color: 1

Bin 3103: 4 of cap free
Amount of items: 3
Items: 
Size: 355389 Color: 1
Size: 323432 Color: 0
Size: 321176 Color: 0

Bin 3104: 4 of cap free
Amount of items: 3
Items: 
Size: 375678 Color: 0
Size: 341013 Color: 1
Size: 283306 Color: 0

Bin 3105: 4 of cap free
Amount of items: 3
Items: 
Size: 362585 Color: 0
Size: 350797 Color: 1
Size: 286615 Color: 1

Bin 3106: 4 of cap free
Amount of items: 3
Items: 
Size: 386144 Color: 1
Size: 341944 Color: 0
Size: 271909 Color: 0

Bin 3107: 4 of cap free
Amount of items: 3
Items: 
Size: 380424 Color: 0
Size: 362726 Color: 1
Size: 256847 Color: 1

Bin 3108: 4 of cap free
Amount of items: 3
Items: 
Size: 356374 Color: 0
Size: 347018 Color: 0
Size: 296605 Color: 1

Bin 3109: 4 of cap free
Amount of items: 3
Items: 
Size: 414097 Color: 0
Size: 295055 Color: 1
Size: 290845 Color: 0

Bin 3110: 4 of cap free
Amount of items: 3
Items: 
Size: 413510 Color: 0
Size: 309118 Color: 0
Size: 277369 Color: 1

Bin 3111: 4 of cap free
Amount of items: 3
Items: 
Size: 417166 Color: 0
Size: 320172 Color: 0
Size: 262659 Color: 1

Bin 3112: 5 of cap free
Amount of items: 3
Items: 
Size: 492891 Color: 1
Size: 255444 Color: 0
Size: 251661 Color: 0

Bin 3113: 5 of cap free
Amount of items: 3
Items: 
Size: 442198 Color: 0
Size: 284783 Color: 1
Size: 273015 Color: 1

Bin 3114: 5 of cap free
Amount of items: 3
Items: 
Size: 460791 Color: 1
Size: 286624 Color: 1
Size: 252581 Color: 0

Bin 3115: 5 of cap free
Amount of items: 3
Items: 
Size: 403571 Color: 1
Size: 313302 Color: 1
Size: 283123 Color: 0

Bin 3116: 5 of cap free
Amount of items: 3
Items: 
Size: 364810 Color: 0
Size: 349830 Color: 0
Size: 285356 Color: 1

Bin 3117: 5 of cap free
Amount of items: 3
Items: 
Size: 463166 Color: 0
Size: 272897 Color: 0
Size: 263933 Color: 1

Bin 3118: 5 of cap free
Amount of items: 3
Items: 
Size: 449599 Color: 1
Size: 279533 Color: 1
Size: 270864 Color: 0

Bin 3119: 5 of cap free
Amount of items: 3
Items: 
Size: 447490 Color: 0
Size: 283724 Color: 1
Size: 268782 Color: 0

Bin 3120: 5 of cap free
Amount of items: 3
Items: 
Size: 466351 Color: 1
Size: 279094 Color: 0
Size: 254551 Color: 0

Bin 3121: 5 of cap free
Amount of items: 3
Items: 
Size: 403602 Color: 0
Size: 324626 Color: 1
Size: 271768 Color: 1

Bin 3122: 5 of cap free
Amount of items: 3
Items: 
Size: 381797 Color: 1
Size: 326467 Color: 1
Size: 291732 Color: 0

Bin 3123: 5 of cap free
Amount of items: 3
Items: 
Size: 342200 Color: 0
Size: 340219 Color: 0
Size: 317577 Color: 1

Bin 3124: 5 of cap free
Amount of items: 3
Items: 
Size: 438027 Color: 1
Size: 309534 Color: 0
Size: 252435 Color: 1

Bin 3125: 5 of cap free
Amount of items: 3
Items: 
Size: 355762 Color: 0
Size: 344034 Color: 0
Size: 300200 Color: 1

Bin 3126: 5 of cap free
Amount of items: 3
Items: 
Size: 359920 Color: 0
Size: 348574 Color: 0
Size: 291502 Color: 1

Bin 3127: 5 of cap free
Amount of items: 3
Items: 
Size: 393454 Color: 1
Size: 343090 Color: 0
Size: 263452 Color: 0

Bin 3128: 5 of cap free
Amount of items: 3
Items: 
Size: 358982 Color: 1
Size: 344969 Color: 0
Size: 296045 Color: 1

Bin 3129: 5 of cap free
Amount of items: 3
Items: 
Size: 358537 Color: 0
Size: 347588 Color: 1
Size: 293871 Color: 0

Bin 3130: 5 of cap free
Amount of items: 3
Items: 
Size: 369395 Color: 0
Size: 343480 Color: 1
Size: 287121 Color: 0

Bin 3131: 6 of cap free
Amount of items: 3
Items: 
Size: 452246 Color: 1
Size: 284629 Color: 0
Size: 263120 Color: 1

Bin 3132: 6 of cap free
Amount of items: 3
Items: 
Size: 392365 Color: 1
Size: 335125 Color: 0
Size: 272505 Color: 1

Bin 3133: 6 of cap free
Amount of items: 3
Items: 
Size: 436492 Color: 0
Size: 300853 Color: 0
Size: 262650 Color: 1

Bin 3134: 6 of cap free
Amount of items: 3
Items: 
Size: 467295 Color: 1
Size: 277974 Color: 0
Size: 254726 Color: 1

Bin 3135: 6 of cap free
Amount of items: 3
Items: 
Size: 452062 Color: 1
Size: 275742 Color: 1
Size: 272191 Color: 0

Bin 3136: 6 of cap free
Amount of items: 3
Items: 
Size: 391770 Color: 1
Size: 335681 Color: 1
Size: 272544 Color: 0

Bin 3137: 6 of cap free
Amount of items: 3
Items: 
Size: 419581 Color: 0
Size: 297444 Color: 0
Size: 282970 Color: 1

Bin 3138: 6 of cap free
Amount of items: 3
Items: 
Size: 394981 Color: 0
Size: 318339 Color: 0
Size: 286675 Color: 1

Bin 3139: 6 of cap free
Amount of items: 3
Items: 
Size: 384600 Color: 0
Size: 350405 Color: 1
Size: 264990 Color: 1

Bin 3140: 6 of cap free
Amount of items: 3
Items: 
Size: 464676 Color: 0
Size: 278472 Color: 0
Size: 256847 Color: 1

Bin 3141: 6 of cap free
Amount of items: 3
Items: 
Size: 387509 Color: 0
Size: 350240 Color: 1
Size: 262246 Color: 0

Bin 3142: 6 of cap free
Amount of items: 3
Items: 
Size: 393898 Color: 0
Size: 347248 Color: 1
Size: 258849 Color: 0

Bin 3143: 6 of cap free
Amount of items: 3
Items: 
Size: 361871 Color: 1
Size: 344955 Color: 0
Size: 293169 Color: 0

Bin 3144: 6 of cap free
Amount of items: 3
Items: 
Size: 344121 Color: 0
Size: 336914 Color: 1
Size: 318960 Color: 1

Bin 3145: 6 of cap free
Amount of items: 3
Items: 
Size: 358047 Color: 1
Size: 341803 Color: 0
Size: 300145 Color: 0

Bin 3146: 6 of cap free
Amount of items: 3
Items: 
Size: 350928 Color: 1
Size: 341372 Color: 0
Size: 307695 Color: 0

Bin 3147: 6 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 0
Size: 333520 Color: 1
Size: 282246 Color: 1

Bin 3148: 7 of cap free
Amount of items: 3
Items: 
Size: 442519 Color: 0
Size: 282183 Color: 1
Size: 275292 Color: 0

Bin 3149: 7 of cap free
Amount of items: 3
Items: 
Size: 355773 Color: 0
Size: 342521 Color: 1
Size: 301700 Color: 0

Bin 3150: 7 of cap free
Amount of items: 3
Items: 
Size: 498044 Color: 1
Size: 251104 Color: 0
Size: 250846 Color: 0

Bin 3151: 7 of cap free
Amount of items: 3
Items: 
Size: 394051 Color: 0
Size: 349778 Color: 1
Size: 256165 Color: 1

Bin 3152: 7 of cap free
Amount of items: 3
Items: 
Size: 425648 Color: 0
Size: 316207 Color: 0
Size: 258139 Color: 1

Bin 3153: 7 of cap free
Amount of items: 3
Items: 
Size: 337359 Color: 1
Size: 337308 Color: 0
Size: 325327 Color: 1

Bin 3154: 7 of cap free
Amount of items: 3
Items: 
Size: 458149 Color: 1
Size: 271540 Color: 1
Size: 270305 Color: 0

Bin 3155: 7 of cap free
Amount of items: 3
Items: 
Size: 350000 Color: 1
Size: 333230 Color: 0
Size: 316764 Color: 0

Bin 3156: 7 of cap free
Amount of items: 3
Items: 
Size: 389804 Color: 1
Size: 330379 Color: 1
Size: 279811 Color: 0

Bin 3157: 8 of cap free
Amount of items: 3
Items: 
Size: 421810 Color: 0
Size: 296484 Color: 1
Size: 281699 Color: 0

Bin 3158: 8 of cap free
Amount of items: 3
Items: 
Size: 371108 Color: 1
Size: 337226 Color: 1
Size: 291659 Color: 0

Bin 3159: 8 of cap free
Amount of items: 3
Items: 
Size: 348772 Color: 0
Size: 342984 Color: 1
Size: 308237 Color: 1

Bin 3160: 8 of cap free
Amount of items: 3
Items: 
Size: 499034 Color: 0
Size: 250748 Color: 0
Size: 250211 Color: 1

Bin 3161: 8 of cap free
Amount of items: 3
Items: 
Size: 418207 Color: 0
Size: 331445 Color: 1
Size: 250341 Color: 1

Bin 3162: 8 of cap free
Amount of items: 3
Items: 
Size: 389115 Color: 0
Size: 349892 Color: 0
Size: 260986 Color: 1

Bin 3163: 8 of cap free
Amount of items: 3
Items: 
Size: 405672 Color: 0
Size: 298248 Color: 1
Size: 296073 Color: 0

Bin 3164: 8 of cap free
Amount of items: 3
Items: 
Size: 470436 Color: 0
Size: 265937 Color: 0
Size: 263620 Color: 1

Bin 3165: 8 of cap free
Amount of items: 3
Items: 
Size: 438574 Color: 0
Size: 308171 Color: 0
Size: 253248 Color: 1

Bin 3166: 8 of cap free
Amount of items: 3
Items: 
Size: 394715 Color: 0
Size: 349673 Color: 1
Size: 255605 Color: 1

Bin 3167: 8 of cap free
Amount of items: 3
Items: 
Size: 388236 Color: 0
Size: 349531 Color: 0
Size: 262226 Color: 1

Bin 3168: 9 of cap free
Amount of items: 3
Items: 
Size: 354357 Color: 0
Size: 329931 Color: 0
Size: 315704 Color: 1

Bin 3169: 9 of cap free
Amount of items: 3
Items: 
Size: 361431 Color: 0
Size: 349759 Color: 1
Size: 288802 Color: 1

Bin 3170: 9 of cap free
Amount of items: 3
Items: 
Size: 450886 Color: 1
Size: 279948 Color: 0
Size: 269158 Color: 0

Bin 3171: 9 of cap free
Amount of items: 3
Items: 
Size: 467899 Color: 0
Size: 268844 Color: 1
Size: 263249 Color: 0

Bin 3172: 9 of cap free
Amount of items: 3
Items: 
Size: 349744 Color: 1
Size: 348344 Color: 0
Size: 301904 Color: 1

Bin 3173: 9 of cap free
Amount of items: 3
Items: 
Size: 346389 Color: 0
Size: 339518 Color: 1
Size: 314085 Color: 0

Bin 3174: 10 of cap free
Amount of items: 3
Items: 
Size: 495093 Color: 0
Size: 252596 Color: 0
Size: 252302 Color: 1

Bin 3175: 10 of cap free
Amount of items: 3
Items: 
Size: 376750 Color: 0
Size: 365324 Color: 0
Size: 257917 Color: 1

Bin 3176: 10 of cap free
Amount of items: 3
Items: 
Size: 485229 Color: 1
Size: 260889 Color: 0
Size: 253873 Color: 0

Bin 3177: 10 of cap free
Amount of items: 3
Items: 
Size: 497832 Color: 1
Size: 252009 Color: 0
Size: 250150 Color: 1

Bin 3178: 10 of cap free
Amount of items: 3
Items: 
Size: 385926 Color: 1
Size: 359482 Color: 0
Size: 254583 Color: 1

Bin 3179: 10 of cap free
Amount of items: 3
Items: 
Size: 424668 Color: 0
Size: 318678 Color: 0
Size: 256645 Color: 1

Bin 3180: 10 of cap free
Amount of items: 3
Items: 
Size: 453222 Color: 0
Size: 280345 Color: 1
Size: 266424 Color: 1

Bin 3181: 10 of cap free
Amount of items: 3
Items: 
Size: 393694 Color: 0
Size: 345431 Color: 1
Size: 260866 Color: 1

Bin 3182: 10 of cap free
Amount of items: 3
Items: 
Size: 420473 Color: 1
Size: 325120 Color: 1
Size: 254398 Color: 0

Bin 3183: 10 of cap free
Amount of items: 3
Items: 
Size: 365569 Color: 1
Size: 348080 Color: 0
Size: 286342 Color: 1

Bin 3184: 10 of cap free
Amount of items: 3
Items: 
Size: 356581 Color: 0
Size: 333986 Color: 1
Size: 309424 Color: 0

Bin 3185: 10 of cap free
Amount of items: 3
Items: 
Size: 372419 Color: 0
Size: 351537 Color: 0
Size: 276035 Color: 1

Bin 3186: 10 of cap free
Amount of items: 3
Items: 
Size: 471833 Color: 1
Size: 270819 Color: 0
Size: 257339 Color: 1

Bin 3187: 11 of cap free
Amount of items: 3
Items: 
Size: 445083 Color: 1
Size: 302218 Color: 0
Size: 252689 Color: 0

Bin 3188: 11 of cap free
Amount of items: 3
Items: 
Size: 399798 Color: 0
Size: 338802 Color: 0
Size: 261390 Color: 1

Bin 3189: 11 of cap free
Amount of items: 3
Items: 
Size: 371847 Color: 1
Size: 333683 Color: 0
Size: 294460 Color: 1

Bin 3190: 11 of cap free
Amount of items: 3
Items: 
Size: 382438 Color: 1
Size: 335233 Color: 0
Size: 282319 Color: 1

Bin 3191: 11 of cap free
Amount of items: 3
Items: 
Size: 452489 Color: 0
Size: 288567 Color: 0
Size: 258934 Color: 1

Bin 3192: 11 of cap free
Amount of items: 3
Items: 
Size: 387960 Color: 0
Size: 321127 Color: 1
Size: 290903 Color: 1

Bin 3193: 11 of cap free
Amount of items: 3
Items: 
Size: 471246 Color: 1
Size: 276697 Color: 0
Size: 252047 Color: 1

Bin 3194: 12 of cap free
Amount of items: 3
Items: 
Size: 470327 Color: 0
Size: 270321 Color: 1
Size: 259341 Color: 0

Bin 3195: 12 of cap free
Amount of items: 3
Items: 
Size: 373712 Color: 0
Size: 352207 Color: 1
Size: 274070 Color: 1

Bin 3196: 12 of cap free
Amount of items: 3
Items: 
Size: 367552 Color: 0
Size: 343055 Color: 1
Size: 289382 Color: 0

Bin 3197: 12 of cap free
Amount of items: 3
Items: 
Size: 425388 Color: 1
Size: 319314 Color: 0
Size: 255287 Color: 0

Bin 3198: 12 of cap free
Amount of items: 3
Items: 
Size: 381694 Color: 0
Size: 339802 Color: 0
Size: 278493 Color: 1

Bin 3199: 12 of cap free
Amount of items: 3
Items: 
Size: 347638 Color: 0
Size: 345387 Color: 1
Size: 306964 Color: 0

Bin 3200: 12 of cap free
Amount of items: 3
Items: 
Size: 438298 Color: 0
Size: 308067 Color: 1
Size: 253624 Color: 0

Bin 3201: 13 of cap free
Amount of items: 3
Items: 
Size: 391096 Color: 0
Size: 309874 Color: 1
Size: 299018 Color: 1

Bin 3202: 13 of cap free
Amount of items: 3
Items: 
Size: 421893 Color: 0
Size: 319363 Color: 0
Size: 258732 Color: 1

Bin 3203: 13 of cap free
Amount of items: 3
Items: 
Size: 479998 Color: 0
Size: 266258 Color: 0
Size: 253732 Color: 1

Bin 3204: 14 of cap free
Amount of items: 3
Items: 
Size: 347263 Color: 0
Size: 340408 Color: 1
Size: 312316 Color: 0

Bin 3205: 14 of cap free
Amount of items: 3
Items: 
Size: 369718 Color: 0
Size: 320666 Color: 0
Size: 309603 Color: 1

Bin 3206: 14 of cap free
Amount of items: 3
Items: 
Size: 441243 Color: 0
Size: 285707 Color: 1
Size: 273037 Color: 0

Bin 3207: 14 of cap free
Amount of items: 3
Items: 
Size: 357027 Color: 1
Size: 347299 Color: 0
Size: 295661 Color: 1

Bin 3208: 14 of cap free
Amount of items: 3
Items: 
Size: 348988 Color: 1
Size: 331279 Color: 1
Size: 319720 Color: 0

Bin 3209: 15 of cap free
Amount of items: 3
Items: 
Size: 469823 Color: 0
Size: 277672 Color: 1
Size: 252491 Color: 0

Bin 3210: 15 of cap free
Amount of items: 3
Items: 
Size: 346523 Color: 0
Size: 340902 Color: 1
Size: 312561 Color: 1

Bin 3211: 15 of cap free
Amount of items: 3
Items: 
Size: 457462 Color: 1
Size: 280123 Color: 1
Size: 262401 Color: 0

Bin 3212: 15 of cap free
Amount of items: 3
Items: 
Size: 420759 Color: 0
Size: 297029 Color: 0
Size: 282198 Color: 1

Bin 3213: 15 of cap free
Amount of items: 3
Items: 
Size: 459131 Color: 0
Size: 272146 Color: 1
Size: 268709 Color: 0

Bin 3214: 15 of cap free
Amount of items: 3
Items: 
Size: 409354 Color: 1
Size: 314501 Color: 1
Size: 276131 Color: 0

Bin 3215: 15 of cap free
Amount of items: 3
Items: 
Size: 391221 Color: 1
Size: 342575 Color: 0
Size: 266190 Color: 1

Bin 3216: 15 of cap free
Amount of items: 3
Items: 
Size: 356665 Color: 1
Size: 345781 Color: 1
Size: 297540 Color: 0

Bin 3217: 16 of cap free
Amount of items: 3
Items: 
Size: 346788 Color: 0
Size: 343106 Color: 1
Size: 310091 Color: 1

Bin 3218: 16 of cap free
Amount of items: 3
Items: 
Size: 374258 Color: 1
Size: 349242 Color: 0
Size: 276485 Color: 0

Bin 3219: 18 of cap free
Amount of items: 3
Items: 
Size: 357353 Color: 1
Size: 332490 Color: 0
Size: 310140 Color: 1

Bin 3220: 18 of cap free
Amount of items: 3
Items: 
Size: 370124 Color: 1
Size: 355420 Color: 1
Size: 274439 Color: 0

Bin 3221: 19 of cap free
Amount of items: 3
Items: 
Size: 434509 Color: 1
Size: 311220 Color: 1
Size: 254253 Color: 0

Bin 3222: 19 of cap free
Amount of items: 3
Items: 
Size: 422122 Color: 1
Size: 310262 Color: 1
Size: 267598 Color: 0

Bin 3223: 19 of cap free
Amount of items: 3
Items: 
Size: 458493 Color: 1
Size: 271971 Color: 0
Size: 269518 Color: 1

Bin 3224: 19 of cap free
Amount of items: 3
Items: 
Size: 477338 Color: 0
Size: 264506 Color: 0
Size: 258138 Color: 1

Bin 3225: 19 of cap free
Amount of items: 3
Items: 
Size: 391888 Color: 1
Size: 342833 Color: 0
Size: 265261 Color: 1

Bin 3226: 19 of cap free
Amount of items: 3
Items: 
Size: 434411 Color: 1
Size: 302025 Color: 0
Size: 263546 Color: 0

Bin 3227: 19 of cap free
Amount of items: 3
Items: 
Size: 498512 Color: 1
Size: 251344 Color: 1
Size: 250126 Color: 0

Bin 3228: 19 of cap free
Amount of items: 3
Items: 
Size: 423044 Color: 1
Size: 308230 Color: 0
Size: 268708 Color: 0

Bin 3229: 20 of cap free
Amount of items: 3
Items: 
Size: 435831 Color: 1
Size: 295310 Color: 0
Size: 268840 Color: 1

Bin 3230: 20 of cap free
Amount of items: 3
Items: 
Size: 382059 Color: 0
Size: 357058 Color: 1
Size: 260864 Color: 0

Bin 3231: 20 of cap free
Amount of items: 3
Items: 
Size: 354037 Color: 0
Size: 334411 Color: 0
Size: 311533 Color: 1

Bin 3232: 21 of cap free
Amount of items: 3
Items: 
Size: 447188 Color: 0
Size: 290985 Color: 0
Size: 261807 Color: 1

Bin 3233: 21 of cap free
Amount of items: 3
Items: 
Size: 352414 Color: 0
Size: 337260 Color: 1
Size: 310306 Color: 1

Bin 3234: 21 of cap free
Amount of items: 3
Items: 
Size: 370703 Color: 0
Size: 352609 Color: 1
Size: 276668 Color: 0

Bin 3235: 21 of cap free
Amount of items: 3
Items: 
Size: 468556 Color: 0
Size: 266589 Color: 1
Size: 264835 Color: 1

Bin 3236: 21 of cap free
Amount of items: 3
Items: 
Size: 461261 Color: 1
Size: 269813 Color: 1
Size: 268906 Color: 0

Bin 3237: 22 of cap free
Amount of items: 3
Items: 
Size: 494016 Color: 1
Size: 254579 Color: 0
Size: 251384 Color: 1

Bin 3238: 22 of cap free
Amount of items: 3
Items: 
Size: 496794 Color: 0
Size: 252411 Color: 1
Size: 250774 Color: 1

Bin 3239: 22 of cap free
Amount of items: 3
Items: 
Size: 411689 Color: 1
Size: 337979 Color: 1
Size: 250311 Color: 0

Bin 3240: 22 of cap free
Amount of items: 3
Items: 
Size: 419032 Color: 0
Size: 308682 Color: 1
Size: 272265 Color: 1

Bin 3241: 23 of cap free
Amount of items: 3
Items: 
Size: 461285 Color: 1
Size: 283362 Color: 1
Size: 255331 Color: 0

Bin 3242: 23 of cap free
Amount of items: 3
Items: 
Size: 348249 Color: 0
Size: 343580 Color: 0
Size: 308149 Color: 1

Bin 3243: 23 of cap free
Amount of items: 3
Items: 
Size: 433328 Color: 1
Size: 315554 Color: 1
Size: 251096 Color: 0

Bin 3244: 24 of cap free
Amount of items: 3
Items: 
Size: 445139 Color: 0
Size: 297694 Color: 0
Size: 257144 Color: 1

Bin 3245: 24 of cap free
Amount of items: 3
Items: 
Size: 374147 Color: 0
Size: 319805 Color: 1
Size: 306025 Color: 0

Bin 3246: 24 of cap free
Amount of items: 3
Items: 
Size: 384542 Color: 0
Size: 325353 Color: 0
Size: 290082 Color: 1

Bin 3247: 25 of cap free
Amount of items: 3
Items: 
Size: 414768 Color: 1
Size: 327590 Color: 0
Size: 257618 Color: 0

Bin 3248: 27 of cap free
Amount of items: 3
Items: 
Size: 367042 Color: 0
Size: 363932 Color: 1
Size: 269000 Color: 1

Bin 3249: 27 of cap free
Amount of items: 3
Items: 
Size: 368285 Color: 0
Size: 360211 Color: 1
Size: 271478 Color: 1

Bin 3250: 27 of cap free
Amount of items: 3
Items: 
Size: 350733 Color: 0
Size: 344203 Color: 0
Size: 305038 Color: 1

Bin 3251: 29 of cap free
Amount of items: 3
Items: 
Size: 418713 Color: 0
Size: 327228 Color: 1
Size: 254031 Color: 0

Bin 3252: 29 of cap free
Amount of items: 3
Items: 
Size: 391971 Color: 1
Size: 357556 Color: 0
Size: 250445 Color: 1

Bin 3253: 30 of cap free
Amount of items: 3
Items: 
Size: 426880 Color: 1
Size: 315102 Color: 0
Size: 257989 Color: 0

Bin 3254: 31 of cap free
Amount of items: 3
Items: 
Size: 463766 Color: 1
Size: 273309 Color: 0
Size: 262895 Color: 0

Bin 3255: 32 of cap free
Amount of items: 3
Items: 
Size: 456658 Color: 1
Size: 287831 Color: 0
Size: 255480 Color: 0

Bin 3256: 32 of cap free
Amount of items: 3
Items: 
Size: 448527 Color: 1
Size: 276398 Color: 0
Size: 275044 Color: 0

Bin 3257: 32 of cap free
Amount of items: 3
Items: 
Size: 442447 Color: 0
Size: 300511 Color: 1
Size: 257011 Color: 1

Bin 3258: 33 of cap free
Amount of items: 3
Items: 
Size: 496065 Color: 0
Size: 252283 Color: 0
Size: 251620 Color: 1

Bin 3259: 33 of cap free
Amount of items: 3
Items: 
Size: 355107 Color: 1
Size: 353680 Color: 0
Size: 291181 Color: 1

Bin 3260: 35 of cap free
Amount of items: 3
Items: 
Size: 441172 Color: 0
Size: 297975 Color: 1
Size: 260819 Color: 1

Bin 3261: 35 of cap free
Amount of items: 3
Items: 
Size: 425376 Color: 1
Size: 292172 Color: 1
Size: 282418 Color: 0

Bin 3262: 35 of cap free
Amount of items: 3
Items: 
Size: 491939 Color: 1
Size: 254991 Color: 0
Size: 253036 Color: 1

Bin 3263: 35 of cap free
Amount of items: 3
Items: 
Size: 351609 Color: 1
Size: 342423 Color: 0
Size: 305934 Color: 0

Bin 3264: 36 of cap free
Amount of items: 3
Items: 
Size: 381778 Color: 0
Size: 359001 Color: 1
Size: 259186 Color: 1

Bin 3265: 37 of cap free
Amount of items: 3
Items: 
Size: 469268 Color: 0
Size: 266050 Color: 1
Size: 264646 Color: 1

Bin 3266: 37 of cap free
Amount of items: 3
Items: 
Size: 449664 Color: 1
Size: 293970 Color: 0
Size: 256330 Color: 0

Bin 3267: 37 of cap free
Amount of items: 3
Items: 
Size: 361588 Color: 1
Size: 344875 Color: 0
Size: 293501 Color: 1

Bin 3268: 38 of cap free
Amount of items: 3
Items: 
Size: 349490 Color: 1
Size: 343725 Color: 0
Size: 306748 Color: 0

Bin 3269: 39 of cap free
Amount of items: 3
Items: 
Size: 417396 Color: 1
Size: 314155 Color: 0
Size: 268411 Color: 0

Bin 3270: 39 of cap free
Amount of items: 3
Items: 
Size: 438059 Color: 0
Size: 296103 Color: 1
Size: 265800 Color: 1

Bin 3271: 40 of cap free
Amount of items: 3
Items: 
Size: 416898 Color: 0
Size: 301106 Color: 1
Size: 281957 Color: 0

Bin 3272: 41 of cap free
Amount of items: 3
Items: 
Size: 427410 Color: 0
Size: 298834 Color: 0
Size: 273716 Color: 1

Bin 3273: 41 of cap free
Amount of items: 3
Items: 
Size: 363146 Color: 0
Size: 354022 Color: 0
Size: 282792 Color: 1

Bin 3274: 44 of cap free
Amount of items: 3
Items: 
Size: 472653 Color: 1
Size: 267475 Color: 0
Size: 259829 Color: 0

Bin 3275: 45 of cap free
Amount of items: 3
Items: 
Size: 410960 Color: 0
Size: 314691 Color: 1
Size: 274305 Color: 0

Bin 3276: 46 of cap free
Amount of items: 3
Items: 
Size: 432679 Color: 1
Size: 302946 Color: 0
Size: 264330 Color: 0

Bin 3277: 47 of cap free
Amount of items: 3
Items: 
Size: 443767 Color: 1
Size: 278467 Color: 0
Size: 277720 Color: 0

Bin 3278: 48 of cap free
Amount of items: 3
Items: 
Size: 366254 Color: 0
Size: 360670 Color: 0
Size: 273029 Color: 1

Bin 3279: 52 of cap free
Amount of items: 3
Items: 
Size: 403538 Color: 0
Size: 311297 Color: 0
Size: 285114 Color: 1

Bin 3280: 53 of cap free
Amount of items: 3
Items: 
Size: 352539 Color: 0
Size: 350400 Color: 0
Size: 297009 Color: 1

Bin 3281: 57 of cap free
Amount of items: 3
Items: 
Size: 411319 Color: 0
Size: 306673 Color: 1
Size: 281952 Color: 0

Bin 3282: 63 of cap free
Amount of items: 3
Items: 
Size: 438985 Color: 0
Size: 290213 Color: 1
Size: 270740 Color: 1

Bin 3283: 64 of cap free
Amount of items: 3
Items: 
Size: 393721 Color: 1
Size: 322922 Color: 0
Size: 283294 Color: 1

Bin 3284: 65 of cap free
Amount of items: 3
Items: 
Size: 423437 Color: 1
Size: 317320 Color: 0
Size: 259179 Color: 1

Bin 3285: 69 of cap free
Amount of items: 3
Items: 
Size: 351717 Color: 1
Size: 327091 Color: 1
Size: 321124 Color: 0

Bin 3286: 72 of cap free
Amount of items: 3
Items: 
Size: 493810 Color: 0
Size: 253199 Color: 1
Size: 252920 Color: 1

Bin 3287: 72 of cap free
Amount of items: 3
Items: 
Size: 487609 Color: 0
Size: 256501 Color: 1
Size: 255819 Color: 1

Bin 3288: 73 of cap free
Amount of items: 3
Items: 
Size: 363146 Color: 0
Size: 333667 Color: 1
Size: 303115 Color: 0

Bin 3289: 74 of cap free
Amount of items: 3
Items: 
Size: 444632 Color: 1
Size: 284150 Color: 0
Size: 271145 Color: 0

Bin 3290: 74 of cap free
Amount of items: 3
Items: 
Size: 434124 Color: 0
Size: 298341 Color: 1
Size: 267462 Color: 1

Bin 3291: 76 of cap free
Amount of items: 3
Items: 
Size: 418675 Color: 0
Size: 330483 Color: 0
Size: 250767 Color: 1

Bin 3292: 78 of cap free
Amount of items: 3
Items: 
Size: 419520 Color: 0
Size: 303458 Color: 1
Size: 276945 Color: 1

Bin 3293: 78 of cap free
Amount of items: 3
Items: 
Size: 415097 Color: 0
Size: 295842 Color: 1
Size: 288984 Color: 1

Bin 3294: 83 of cap free
Amount of items: 3
Items: 
Size: 436568 Color: 1
Size: 308874 Color: 1
Size: 254476 Color: 0

Bin 3295: 88 of cap free
Amount of items: 3
Items: 
Size: 368359 Color: 1
Size: 342225 Color: 1
Size: 289329 Color: 0

Bin 3296: 92 of cap free
Amount of items: 3
Items: 
Size: 392052 Color: 0
Size: 350400 Color: 0
Size: 257457 Color: 1

Bin 3297: 96 of cap free
Amount of items: 3
Items: 
Size: 445802 Color: 1
Size: 290498 Color: 0
Size: 263605 Color: 0

Bin 3298: 101 of cap free
Amount of items: 3
Items: 
Size: 368546 Color: 1
Size: 363666 Color: 1
Size: 267688 Color: 0

Bin 3299: 103 of cap free
Amount of items: 2
Items: 
Size: 499999 Color: 1
Size: 499899 Color: 0

Bin 3300: 118 of cap free
Amount of items: 3
Items: 
Size: 420955 Color: 1
Size: 316745 Color: 0
Size: 262183 Color: 0

Bin 3301: 119 of cap free
Amount of items: 3
Items: 
Size: 429543 Color: 0
Size: 318118 Color: 1
Size: 252221 Color: 1

Bin 3302: 123 of cap free
Amount of items: 3
Items: 
Size: 345848 Color: 1
Size: 345757 Color: 0
Size: 308273 Color: 0

Bin 3303: 124 of cap free
Amount of items: 3
Items: 
Size: 477764 Color: 1
Size: 264058 Color: 0
Size: 258055 Color: 0

Bin 3304: 133 of cap free
Amount of items: 3
Items: 
Size: 418670 Color: 1
Size: 299521 Color: 1
Size: 281677 Color: 0

Bin 3305: 139 of cap free
Amount of items: 3
Items: 
Size: 363099 Color: 0
Size: 332206 Color: 1
Size: 304557 Color: 1

Bin 3306: 143 of cap free
Amount of items: 3
Items: 
Size: 344256 Color: 1
Size: 338036 Color: 0
Size: 317566 Color: 1

Bin 3307: 159 of cap free
Amount of items: 3
Items: 
Size: 363870 Color: 0
Size: 352126 Color: 0
Size: 283846 Color: 1

Bin 3308: 178 of cap free
Amount of items: 3
Items: 
Size: 380132 Color: 1
Size: 325320 Color: 1
Size: 294371 Color: 0

Bin 3309: 186 of cap free
Amount of items: 3
Items: 
Size: 383420 Color: 1
Size: 356122 Color: 1
Size: 260273 Color: 0

Bin 3310: 189 of cap free
Amount of items: 3
Items: 
Size: 416451 Color: 1
Size: 306470 Color: 0
Size: 276891 Color: 1

Bin 3311: 198 of cap free
Amount of items: 3
Items: 
Size: 369693 Color: 1
Size: 369037 Color: 1
Size: 261073 Color: 0

Bin 3312: 203 of cap free
Amount of items: 3
Items: 
Size: 438562 Color: 1
Size: 288915 Color: 0
Size: 272321 Color: 1

Bin 3313: 229 of cap free
Amount of items: 3
Items: 
Size: 433768 Color: 0
Size: 301685 Color: 1
Size: 264319 Color: 1

Bin 3314: 257 of cap free
Amount of items: 3
Items: 
Size: 376073 Color: 1
Size: 350279 Color: 1
Size: 273392 Color: 0

Bin 3315: 285 of cap free
Amount of items: 3
Items: 
Size: 363090 Color: 0
Size: 338391 Color: 1
Size: 298235 Color: 0

Bin 3316: 289 of cap free
Amount of items: 3
Items: 
Size: 375664 Color: 1
Size: 348080 Color: 0
Size: 275968 Color: 0

Bin 3317: 316 of cap free
Amount of items: 3
Items: 
Size: 436413 Color: 0
Size: 288799 Color: 1
Size: 274473 Color: 1

Bin 3318: 365 of cap free
Amount of items: 3
Items: 
Size: 366041 Color: 0
Size: 319653 Color: 1
Size: 313942 Color: 0

Bin 3319: 387 of cap free
Amount of items: 3
Items: 
Size: 444519 Color: 1
Size: 300424 Color: 0
Size: 254671 Color: 0

Bin 3320: 429 of cap free
Amount of items: 3
Items: 
Size: 342872 Color: 1
Size: 340509 Color: 0
Size: 316191 Color: 0

Bin 3321: 480 of cap free
Amount of items: 3
Items: 
Size: 455967 Color: 1
Size: 274069 Color: 0
Size: 269485 Color: 1

Bin 3322: 486 of cap free
Amount of items: 3
Items: 
Size: 365534 Color: 1
Size: 355737 Color: 0
Size: 278244 Color: 1

Bin 3323: 495 of cap free
Amount of items: 3
Items: 
Size: 435798 Color: 1
Size: 305514 Color: 0
Size: 258194 Color: 0

Bin 3324: 654 of cap free
Amount of items: 3
Items: 
Size: 354866 Color: 0
Size: 352340 Color: 0
Size: 292141 Color: 1

Bin 3325: 1052 of cap free
Amount of items: 3
Items: 
Size: 371010 Color: 0
Size: 339184 Color: 1
Size: 288755 Color: 1

Bin 3326: 1428 of cap free
Amount of items: 3
Items: 
Size: 406772 Color: 1
Size: 296177 Color: 0
Size: 295624 Color: 0

Bin 3327: 1582 of cap free
Amount of items: 3
Items: 
Size: 340262 Color: 0
Size: 333699 Color: 0
Size: 324458 Color: 1

Bin 3328: 2151 of cap free
Amount of items: 3
Items: 
Size: 388340 Color: 0
Size: 304769 Color: 0
Size: 304741 Color: 1

Bin 3329: 2164 of cap free
Amount of items: 2
Items: 
Size: 498966 Color: 0
Size: 498871 Color: 1

Bin 3330: 2199 of cap free
Amount of items: 3
Items: 
Size: 484081 Color: 0
Size: 256962 Color: 1
Size: 256759 Color: 0

Bin 3331: 68592 of cap free
Amount of items: 3
Items: 
Size: 347049 Color: 1
Size: 313404 Color: 1
Size: 270956 Color: 0

Bin 3332: 186406 of cap free
Amount of items: 3
Items: 
Size: 290146 Color: 1
Size: 267056 Color: 1
Size: 256393 Color: 0

Bin 3333: 232878 of cap free
Amount of items: 3
Items: 
Size: 256262 Color: 0
Size: 255456 Color: 1
Size: 255405 Color: 0

Bin 3334: 238610 of cap free
Amount of items: 3
Items: 
Size: 254639 Color: 1
Size: 253993 Color: 1
Size: 252759 Color: 0

Bin 3335: 251674 of cap free
Amount of items: 2
Items: 
Size: 498263 Color: 0
Size: 250064 Color: 1

Total size: 3334003334
Total free space: 1000001

