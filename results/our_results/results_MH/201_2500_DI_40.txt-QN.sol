Capicity Bin: 1916
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 967 Color: 141
Size: 653 Color: 127
Size: 112 Color: 55
Size: 64 Color: 36
Size: 56 Color: 30
Size: 44 Color: 19
Size: 20 Color: 3

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1526 Color: 173
Size: 212 Color: 79
Size: 134 Color: 61
Size: 44 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 198
Size: 158 Color: 66
Size: 52 Color: 28

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 185
Size: 237 Color: 87
Size: 46 Color: 22

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1062 Color: 143
Size: 714 Color: 131
Size: 140 Color: 63

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 196
Size: 190 Color: 72
Size: 40 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 183
Size: 251 Color: 89
Size: 48 Color: 25

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1282 Color: 157
Size: 530 Color: 117
Size: 104 Color: 52

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 195
Size: 197 Color: 75
Size: 38 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1331 Color: 160
Size: 489 Color: 115
Size: 96 Color: 50

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 962 Color: 139
Size: 798 Color: 136
Size: 156 Color: 65

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1204 Color: 150
Size: 644 Color: 125
Size: 68 Color: 40

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 1251 Color: 154
Size: 657 Color: 128
Size: 4 Color: 1
Size: 4 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1133 Color: 147
Size: 673 Color: 129
Size: 110 Color: 53

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 166
Size: 394 Color: 107
Size: 76 Color: 45

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 177
Size: 282 Color: 93
Size: 72 Color: 42

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 170
Size: 355 Color: 103
Size: 70 Color: 41

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 169
Size: 300 Color: 96
Size: 130 Color: 59

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1095 Color: 144
Size: 685 Color: 130
Size: 136 Color: 62

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 174
Size: 321 Color: 99
Size: 64 Color: 37

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1247 Color: 153
Size: 559 Color: 119
Size: 110 Color: 54

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1515 Color: 172
Size: 335 Color: 101
Size: 66 Color: 39

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 971 Color: 142
Size: 789 Color: 133
Size: 156 Color: 64

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 194
Size: 219 Color: 81
Size: 20 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 959 Color: 138
Size: 799 Color: 137
Size: 158 Color: 68

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 184
Size: 246 Color: 88
Size: 48 Color: 24

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 155
Size: 615 Color: 123
Size: 34 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 963 Color: 140
Size: 795 Color: 135
Size: 158 Color: 67

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 182
Size: 253 Color: 90
Size: 50 Color: 26

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 165
Size: 433 Color: 110
Size: 44 Color: 21

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1109 Color: 145
Size: 791 Color: 134
Size: 16 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1320 Color: 159
Size: 362 Color: 104
Size: 234 Color: 86

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 176
Size: 309 Color: 97
Size: 58 Color: 32

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 191
Size: 209 Color: 78
Size: 40 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 162
Size: 461 Color: 113
Size: 90 Color: 48

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1565 Color: 178
Size: 293 Color: 94
Size: 58 Color: 33

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1137 Color: 148
Size: 651 Color: 126
Size: 128 Color: 57

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1648 Color: 188
Size: 228 Color: 84
Size: 40 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 190
Size: 221 Color: 83
Size: 42 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 149
Size: 444 Color: 112
Size: 326 Color: 100

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 163
Size: 396 Color: 108
Size: 130 Color: 60

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 187
Size: 194 Color: 74
Size: 84 Color: 46

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1672 Color: 192
Size: 204 Color: 77
Size: 40 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 152
Size: 555 Color: 118
Size: 128 Color: 58

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 197
Size: 191 Color: 73
Size: 36 Color: 9

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1129 Color: 146
Size: 571 Color: 120
Size: 216 Color: 80

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 175
Size: 318 Color: 98
Size: 60 Color: 34

Bin 48: 0 of cap free
Amount of items: 4
Items: 
Size: 772 Color: 132
Size: 642 Color: 124
Size: 442 Color: 111
Size: 60 Color: 35

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 168
Size: 375 Color: 105
Size: 74 Color: 43

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 179
Size: 298 Color: 95
Size: 36 Color: 10

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 181
Size: 262 Color: 91
Size: 52 Color: 27

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 167
Size: 383 Color: 106
Size: 76 Color: 44

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 200
Size: 171 Color: 70
Size: 34 Color: 7

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 171
Size: 339 Color: 102
Size: 66 Color: 38

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1295 Color: 158
Size: 519 Color: 116
Size: 102 Color: 51

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 161
Size: 474 Color: 114
Size: 92 Color: 49

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 189
Size: 221 Color: 82
Size: 44 Color: 20

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1278 Color: 156
Size: 582 Color: 122
Size: 56 Color: 31

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 186
Size: 233 Color: 85
Size: 46 Color: 23

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 164
Size: 427 Color: 109
Size: 84 Color: 47

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 199
Size: 173 Color: 71
Size: 34 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 180
Size: 271 Color: 92
Size: 52 Color: 29

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 193
Size: 202 Color: 76
Size: 40 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 151
Size: 582 Color: 121
Size: 116 Color: 56

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 201
Size: 171 Color: 69
Size: 32 Color: 5

Total size: 124540
Total free space: 0

