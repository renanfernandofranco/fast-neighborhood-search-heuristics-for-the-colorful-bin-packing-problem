Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 976 Color: 16
Size: 568 Color: 18
Size: 512 Color: 12
Size: 272 Color: 17
Size: 88 Color: 19
Size: 40 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 3
Size: 378 Color: 14
Size: 72 Color: 18

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1696 Color: 0
Size: 384 Color: 6
Size: 280 Color: 0
Size: 48 Color: 11
Size: 48 Color: 1

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 1932 Color: 15
Size: 444 Color: 18
Size: 56 Color: 6
Size: 16 Color: 1
Size: 8 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 15
Size: 330 Color: 10
Size: 8 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 13
Size: 421 Color: 4
Size: 84 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 17
Size: 244 Color: 11
Size: 24 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 16
Size: 292 Color: 2
Size: 56 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 5
Size: 580 Color: 19
Size: 112 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 19
Size: 214 Color: 15
Size: 40 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 6
Size: 443 Color: 2
Size: 88 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 2
Size: 244 Color: 19
Size: 40 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 4
Size: 621 Color: 1
Size: 122 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 0
Size: 258 Color: 12
Size: 32 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 1
Size: 454 Color: 16
Size: 88 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 11
Size: 513 Color: 15
Size: 102 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 6
Size: 324 Color: 8
Size: 64 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 9
Size: 364 Color: 16
Size: 72 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 3
Size: 514 Color: 10
Size: 100 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 5
Size: 377 Color: 4
Size: 74 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 18
Size: 484 Color: 5
Size: 88 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 3
Size: 517 Color: 0
Size: 102 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 1
Size: 300 Color: 12
Size: 16 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 5
Size: 755 Color: 8
Size: 150 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 5
Size: 863 Color: 2
Size: 172 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 10
Size: 1023 Color: 6
Size: 204 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 10
Size: 262 Color: 1
Size: 48 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 2
Size: 689 Color: 6
Size: 94 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 6
Size: 840 Color: 19
Size: 80 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 11
Size: 1018 Color: 8
Size: 200 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1853 Color: 9
Size: 503 Color: 18
Size: 100 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 19
Size: 339 Color: 6
Size: 66 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 3
Size: 641 Color: 9
Size: 126 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 17
Size: 916 Color: 8
Size: 176 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 16
Size: 637 Color: 10
Size: 126 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 11
Size: 607 Color: 16
Size: 120 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 9
Size: 620 Color: 10
Size: 120 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 16
Size: 541 Color: 12
Size: 108 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 11
Size: 342 Color: 4
Size: 64 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 8
Size: 741 Color: 7
Size: 148 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 10
Size: 232 Color: 18
Size: 32 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 7
Size: 1022 Color: 12
Size: 204 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 15
Size: 714 Color: 12
Size: 140 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 17
Size: 588 Color: 11
Size: 16 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 7
Size: 212 Color: 1
Size: 40 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 2
Size: 877 Color: 7
Size: 174 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 3
Size: 622 Color: 18
Size: 120 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 15
Size: 422 Color: 11
Size: 60 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 16
Size: 467 Color: 6
Size: 54 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1931 Color: 8
Size: 439 Color: 17
Size: 86 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 19
Size: 282 Color: 5
Size: 88 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 17
Size: 226 Color: 15
Size: 44 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 19
Size: 586 Color: 11
Size: 64 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 12
Size: 828 Color: 15
Size: 160 Color: 11

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 14
Size: 756 Color: 19
Size: 144 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 17
Size: 862 Color: 11
Size: 168 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 9
Size: 380 Color: 10
Size: 40 Color: 12

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 3
Size: 850 Color: 15
Size: 20 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1430 Color: 0
Size: 858 Color: 6
Size: 168 Color: 6

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 17
Size: 1020 Color: 3
Size: 200 Color: 19

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1900 Color: 10
Size: 468 Color: 9
Size: 88 Color: 5

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 0
Size: 452 Color: 12
Size: 88 Color: 11

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 11
Size: 684 Color: 3
Size: 128 Color: 18

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2021 Color: 13
Size: 363 Color: 15
Size: 72 Color: 17

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 6
Size: 1021 Color: 12
Size: 204 Color: 19

Total size: 159640
Total free space: 0

