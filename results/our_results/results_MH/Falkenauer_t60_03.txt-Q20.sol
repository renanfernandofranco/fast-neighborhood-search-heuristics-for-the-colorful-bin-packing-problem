Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 16
Size: 273 Color: 9
Size: 250 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9
Size: 255 Color: 0
Size: 250 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 16
Size: 261 Color: 5
Size: 254 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 19
Size: 298 Color: 8
Size: 297 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 19
Size: 332 Color: 12
Size: 254 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 345 Color: 9
Size: 277 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 357 Color: 16
Size: 268 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 281 Color: 5
Size: 263 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 13
Size: 367 Color: 13
Size: 265 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 281 Color: 12
Size: 258 Color: 6
Size: 461 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 11
Size: 255 Color: 4
Size: 252 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 278 Color: 7
Size: 263 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 265 Color: 1
Size: 257 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 347 Color: 8
Size: 262 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 7
Size: 277 Color: 0
Size: 261 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 14
Size: 293 Color: 1
Size: 256 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 17
Size: 316 Color: 4
Size: 258 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 11
Size: 293 Color: 6
Size: 278 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 354 Color: 17
Size: 275 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 361 Color: 16
Size: 270 Color: 18

Total size: 20000
Total free space: 0

