Capicity Bin: 7520
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3990 Color: 3
Size: 3366 Color: 1
Size: 164 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4699 Color: 3
Size: 2349 Color: 4
Size: 472 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 4
Size: 2344 Color: 4
Size: 140 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5061 Color: 4
Size: 2051 Color: 1
Size: 408 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5242 Color: 0
Size: 2100 Color: 1
Size: 178 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5238 Color: 2
Size: 2010 Color: 4
Size: 272 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 3
Size: 1878 Color: 0
Size: 128 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5595 Color: 0
Size: 1429 Color: 1
Size: 496 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5692 Color: 0
Size: 1420 Color: 4
Size: 408 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 1
Size: 1556 Color: 0
Size: 144 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 2
Size: 1432 Color: 4
Size: 236 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 2
Size: 1291 Color: 0
Size: 256 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 4
Size: 1266 Color: 0
Size: 280 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 2
Size: 1044 Color: 0
Size: 496 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6039 Color: 0
Size: 1235 Color: 4
Size: 246 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6043 Color: 1
Size: 1117 Color: 0
Size: 360 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 0
Size: 1186 Color: 2
Size: 244 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6076 Color: 4
Size: 1204 Color: 0
Size: 240 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6098 Color: 3
Size: 1062 Color: 0
Size: 360 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 2
Size: 881 Color: 0
Size: 464 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6209 Color: 3
Size: 1183 Color: 1
Size: 128 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6261 Color: 1
Size: 1121 Color: 0
Size: 138 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 3
Size: 1132 Color: 4
Size: 120 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6319 Color: 3
Size: 905 Color: 0
Size: 296 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 3
Size: 978 Color: 3
Size: 200 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6353 Color: 3
Size: 751 Color: 2
Size: 416 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 0
Size: 660 Color: 3
Size: 496 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6441 Color: 0
Size: 669 Color: 4
Size: 410 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 3
Size: 882 Color: 4
Size: 208 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6445 Color: 2
Size: 897 Color: 3
Size: 178 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 0
Size: 723 Color: 2
Size: 320 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 0
Size: 642 Color: 1
Size: 352 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6533 Color: 0
Size: 695 Color: 2
Size: 292 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 3
Size: 540 Color: 0
Size: 496 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6534 Color: 3
Size: 812 Color: 0
Size: 174 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 4
Size: 540 Color: 0
Size: 432 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6558 Color: 4
Size: 740 Color: 1
Size: 222 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6576 Color: 4
Size: 568 Color: 4
Size: 376 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 0
Size: 699 Color: 2
Size: 200 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 2
Size: 644 Color: 3
Size: 286 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 0
Size: 716 Color: 3
Size: 174 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 3
Size: 624 Color: 0
Size: 260 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 3
Size: 718 Color: 0
Size: 140 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 3
Size: 690 Color: 2
Size: 136 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 2
Size: 432 Color: 3
Size: 388 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 2
Size: 572 Color: 2
Size: 222 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6747 Color: 2
Size: 677 Color: 1
Size: 96 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 3
Size: 600 Color: 0
Size: 164 Color: 4

Bin 49: 1 of cap free
Amount of items: 10
Items: 
Size: 1605 Color: 2
Size: 1204 Color: 3
Size: 1001 Color: 0
Size: 973 Color: 3
Size: 892 Color: 3
Size: 870 Color: 0
Size: 286 Color: 1
Size: 284 Color: 1
Size: 256 Color: 0
Size: 148 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 1
Size: 3131 Color: 0
Size: 304 Color: 4

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 5020 Color: 0
Size: 2355 Color: 3
Size: 144 Color: 1

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 2
Size: 1603 Color: 0
Size: 152 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 5803 Color: 1
Size: 1498 Color: 0
Size: 218 Color: 2

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6122 Color: 0
Size: 1229 Color: 3
Size: 168 Color: 3

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 2
Size: 1435 Color: 1

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6205 Color: 0
Size: 710 Color: 3
Size: 604 Color: 2

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 0
Size: 651 Color: 2
Size: 626 Color: 1

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 0
Size: 1093 Color: 4
Size: 176 Color: 3

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 6438 Color: 0
Size: 721 Color: 1
Size: 360 Color: 4

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 2
Size: 901 Color: 3
Size: 156 Color: 0

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 3
Size: 813 Color: 2
Size: 36 Color: 0

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 6683 Color: 4
Size: 780 Color: 0
Size: 56 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 4
Size: 468 Color: 3
Size: 332 Color: 0

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 6732 Color: 2
Size: 787 Color: 4

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 6739 Color: 2
Size: 608 Color: 3
Size: 172 Color: 0

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 4396 Color: 3
Size: 2942 Color: 0
Size: 180 Color: 2

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 5351 Color: 3
Size: 1807 Color: 4
Size: 360 Color: 0

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 4
Size: 1770 Color: 0
Size: 88 Color: 3

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 5799 Color: 0
Size: 1431 Color: 1
Size: 288 Color: 1

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 4
Size: 1066 Color: 3

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 4273 Color: 0
Size: 3012 Color: 3
Size: 232 Color: 3

Bin 72: 3 of cap free
Amount of items: 4
Items: 
Size: 4918 Color: 3
Size: 2351 Color: 1
Size: 128 Color: 0
Size: 120 Color: 4

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 0
Size: 1365 Color: 1
Size: 964 Color: 4

Bin 74: 3 of cap free
Amount of items: 2
Items: 
Size: 6687 Color: 1
Size: 830 Color: 4

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 4
Size: 3288 Color: 1
Size: 320 Color: 3

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 1
Size: 3134 Color: 2
Size: 232 Color: 2

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 5353 Color: 0
Size: 1290 Color: 3
Size: 873 Color: 4

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 0
Size: 1446 Color: 4
Size: 344 Color: 1

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 4
Size: 1166 Color: 0
Size: 588 Color: 2

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 2
Size: 1051 Color: 4

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 6545 Color: 3
Size: 971 Color: 2

Bin 82: 5 of cap free
Amount of items: 3
Items: 
Size: 4644 Color: 0
Size: 2709 Color: 4
Size: 162 Color: 3

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 6047 Color: 1
Size: 1468 Color: 3

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 4534 Color: 1
Size: 2800 Color: 0
Size: 180 Color: 1

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 5053 Color: 1
Size: 2076 Color: 0
Size: 384 Color: 3

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 3
Size: 1040 Color: 2

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 3
Size: 2604 Color: 0
Size: 196 Color: 1

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 4
Size: 1396 Color: 2

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 3
Size: 1276 Color: 2
Size: 64 Color: 0

Bin 90: 9 of cap free
Amount of items: 5
Items: 
Size: 3761 Color: 2
Size: 2053 Color: 3
Size: 1097 Color: 0
Size: 408 Color: 1
Size: 192 Color: 4

Bin 91: 10 of cap free
Amount of items: 5
Items: 
Size: 3762 Color: 3
Size: 2490 Color: 0
Size: 624 Color: 1
Size: 410 Color: 1
Size: 224 Color: 2

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 3765 Color: 1
Size: 2874 Color: 0
Size: 871 Color: 2

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 5398 Color: 2
Size: 1948 Color: 0
Size: 164 Color: 4

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 4
Size: 842 Color: 3

Bin 95: 11 of cap free
Amount of items: 3
Items: 
Size: 5506 Color: 1
Size: 1809 Color: 2
Size: 194 Color: 0

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 4
Size: 982 Color: 2
Size: 92 Color: 0

Bin 97: 11 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 1
Size: 822 Color: 3
Size: 32 Color: 2

Bin 98: 12 of cap free
Amount of items: 3
Items: 
Size: 4922 Color: 3
Size: 2170 Color: 0
Size: 416 Color: 3

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 6750 Color: 1
Size: 758 Color: 3

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 4
Size: 1231 Color: 3

Bin 101: 13 of cap free
Amount of items: 2
Items: 
Size: 6715 Color: 2
Size: 792 Color: 3

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 4
Size: 902 Color: 2

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 6181 Color: 1
Size: 1324 Color: 4

Bin 104: 16 of cap free
Amount of items: 3
Items: 
Size: 5942 Color: 3
Size: 1466 Color: 2
Size: 96 Color: 1

Bin 105: 17 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 0
Size: 3133 Color: 2
Size: 292 Color: 0

Bin 106: 18 of cap free
Amount of items: 2
Items: 
Size: 5754 Color: 3
Size: 1748 Color: 2

Bin 107: 21 of cap free
Amount of items: 2
Items: 
Size: 5975 Color: 4
Size: 1524 Color: 3

Bin 108: 22 of cap free
Amount of items: 3
Items: 
Size: 4271 Color: 0
Size: 2707 Color: 2
Size: 520 Color: 2

Bin 109: 22 of cap free
Amount of items: 2
Items: 
Size: 6024 Color: 2
Size: 1474 Color: 4

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 6213 Color: 3
Size: 1284 Color: 1

Bin 111: 23 of cap free
Amount of items: 2
Items: 
Size: 6577 Color: 1
Size: 920 Color: 3

Bin 112: 24 of cap free
Amount of items: 2
Items: 
Size: 5004 Color: 2
Size: 2492 Color: 4

Bin 113: 25 of cap free
Amount of items: 2
Items: 
Size: 6177 Color: 4
Size: 1318 Color: 1

Bin 114: 26 of cap free
Amount of items: 2
Items: 
Size: 6350 Color: 2
Size: 1144 Color: 4

Bin 115: 27 of cap free
Amount of items: 3
Items: 
Size: 4703 Color: 0
Size: 2486 Color: 2
Size: 304 Color: 1

Bin 116: 30 of cap free
Amount of items: 3
Items: 
Size: 3886 Color: 1
Size: 3132 Color: 2
Size: 472 Color: 4

Bin 117: 30 of cap free
Amount of items: 2
Items: 
Size: 5324 Color: 1
Size: 2166 Color: 2

Bin 118: 31 of cap free
Amount of items: 2
Items: 
Size: 5807 Color: 3
Size: 1682 Color: 1

Bin 119: 35 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 2
Size: 2057 Color: 3

Bin 120: 38 of cap free
Amount of items: 22
Items: 
Size: 891 Color: 2
Size: 868 Color: 0
Size: 742 Color: 0
Size: 671 Color: 4
Size: 470 Color: 4
Size: 470 Color: 0
Size: 328 Color: 4
Size: 288 Color: 4
Size: 272 Color: 1
Size: 246 Color: 2
Size: 218 Color: 1
Size: 216 Color: 1
Size: 212 Color: 0
Size: 208 Color: 0
Size: 192 Color: 1
Size: 192 Color: 0
Size: 180 Color: 1
Size: 180 Color: 1
Size: 176 Color: 3
Size: 160 Color: 2
Size: 158 Color: 2
Size: 144 Color: 0

Bin 121: 41 of cap free
Amount of items: 2
Items: 
Size: 6569 Color: 1
Size: 910 Color: 4

Bin 122: 42 of cap free
Amount of items: 3
Items: 
Size: 3764 Color: 2
Size: 3030 Color: 0
Size: 684 Color: 4

Bin 123: 42 of cap free
Amount of items: 2
Items: 
Size: 3766 Color: 4
Size: 3712 Color: 3

Bin 124: 42 of cap free
Amount of items: 2
Items: 
Size: 6357 Color: 4
Size: 1121 Color: 1

Bin 125: 59 of cap free
Amount of items: 2
Items: 
Size: 5057 Color: 2
Size: 2404 Color: 1

Bin 126: 74 of cap free
Amount of items: 2
Items: 
Size: 6653 Color: 0
Size: 793 Color: 2

Bin 127: 80 of cap free
Amount of items: 2
Items: 
Size: 5538 Color: 3
Size: 1902 Color: 4

Bin 128: 80 of cap free
Amount of items: 2
Items: 
Size: 5786 Color: 2
Size: 1654 Color: 1

Bin 129: 87 of cap free
Amount of items: 2
Items: 
Size: 5597 Color: 4
Size: 1836 Color: 2

Bin 130: 114 of cap free
Amount of items: 2
Items: 
Size: 4538 Color: 1
Size: 2868 Color: 3

Bin 131: 117 of cap free
Amount of items: 3
Items: 
Size: 4695 Color: 0
Size: 2084 Color: 2
Size: 624 Color: 1

Bin 132: 118 of cap free
Amount of items: 2
Items: 
Size: 4532 Color: 2
Size: 2870 Color: 1

Bin 133: 5914 of cap free
Amount of items: 12
Items: 
Size: 148 Color: 1
Size: 138 Color: 2
Size: 136 Color: 3
Size: 136 Color: 3
Size: 136 Color: 2
Size: 136 Color: 1
Size: 134 Color: 4
Size: 132 Color: 3
Size: 130 Color: 1
Size: 128 Color: 2
Size: 128 Color: 0
Size: 124 Color: 4

Total size: 992640
Total free space: 7520

