Capicity Bin: 16480
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8244 Color: 2
Size: 1658 Color: 1
Size: 1496 Color: 3
Size: 1484 Color: 11
Size: 1372 Color: 4
Size: 1176 Color: 3
Size: 1050 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10214 Color: 18
Size: 5924 Color: 5
Size: 342 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 18
Size: 5208 Color: 7
Size: 1040 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10262 Color: 9
Size: 5938 Color: 14
Size: 280 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11860 Color: 4
Size: 4452 Color: 16
Size: 168 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 6
Size: 3566 Color: 15
Size: 736 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12372 Color: 9
Size: 2246 Color: 0
Size: 1862 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12617 Color: 15
Size: 2567 Color: 13
Size: 1296 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 18
Size: 2726 Color: 14
Size: 1042 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 5
Size: 1973 Color: 7
Size: 1837 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 18
Size: 3004 Color: 8
Size: 680 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12981 Color: 11
Size: 2937 Color: 4
Size: 562 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 12
Size: 2803 Color: 8
Size: 648 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13049 Color: 10
Size: 2857 Color: 17
Size: 574 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13077 Color: 18
Size: 3139 Color: 3
Size: 264 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 1
Size: 2552 Color: 15
Size: 816 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13210 Color: 18
Size: 2670 Color: 11
Size: 600 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13128 Color: 16
Size: 2808 Color: 17
Size: 544 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13310 Color: 11
Size: 2642 Color: 0
Size: 528 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 13
Size: 2724 Color: 7
Size: 340 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13597 Color: 19
Size: 2673 Color: 12
Size: 210 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 15
Size: 2418 Color: 7
Size: 374 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13829 Color: 6
Size: 1933 Color: 14
Size: 718 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 7
Size: 2124 Color: 4
Size: 416 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 4
Size: 1524 Color: 5
Size: 992 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 6
Size: 1554 Color: 15
Size: 914 Color: 12

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 6
Size: 1662 Color: 13
Size: 716 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14113 Color: 18
Size: 1721 Color: 0
Size: 646 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 6
Size: 1916 Color: 13
Size: 396 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14174 Color: 13
Size: 1970 Color: 2
Size: 336 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14185 Color: 15
Size: 1881 Color: 18
Size: 414 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14225 Color: 19
Size: 1857 Color: 9
Size: 398 Color: 5

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14301 Color: 18
Size: 1859 Color: 1
Size: 320 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 10
Size: 1842 Color: 15
Size: 368 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 18
Size: 1576 Color: 1
Size: 592 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14336 Color: 4
Size: 1432 Color: 5
Size: 712 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 11
Size: 1672 Color: 8
Size: 428 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 16
Size: 1742 Color: 18
Size: 348 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 10
Size: 1196 Color: 14
Size: 880 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 8
Size: 1528 Color: 1
Size: 514 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 13
Size: 1542 Color: 8
Size: 404 Color: 16

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14558 Color: 19
Size: 1564 Color: 18
Size: 358 Color: 5

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 13
Size: 1368 Color: 16
Size: 512 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 12
Size: 1490 Color: 10
Size: 378 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14678 Color: 1
Size: 1450 Color: 8
Size: 352 Color: 16

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14754 Color: 5
Size: 1372 Color: 17
Size: 354 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14776 Color: 3
Size: 1384 Color: 1
Size: 320 Color: 18

Bin 48: 1 of cap free
Amount of items: 11
Items: 
Size: 8241 Color: 1
Size: 1046 Color: 5
Size: 1044 Color: 8
Size: 1040 Color: 6
Size: 1016 Color: 17
Size: 924 Color: 11
Size: 918 Color: 3
Size: 916 Color: 18
Size: 570 Color: 13
Size: 396 Color: 17
Size: 368 Color: 2

Bin 49: 1 of cap free
Amount of items: 5
Items: 
Size: 8260 Color: 2
Size: 4543 Color: 7
Size: 1998 Color: 0
Size: 1042 Color: 12
Size: 636 Color: 8

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 10177 Color: 11
Size: 5998 Color: 8
Size: 304 Color: 10

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 10978 Color: 2
Size: 5213 Color: 5
Size: 288 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 10985 Color: 19
Size: 5222 Color: 10
Size: 272 Color: 1

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 11001 Color: 5
Size: 5084 Color: 0
Size: 394 Color: 10

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 11621 Color: 19
Size: 4586 Color: 3
Size: 272 Color: 7

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 11675 Color: 17
Size: 4412 Color: 17
Size: 392 Color: 13

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 18
Size: 4005 Color: 4
Size: 344 Color: 1

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12884 Color: 10
Size: 3227 Color: 0
Size: 368 Color: 5

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13097 Color: 16
Size: 2808 Color: 9
Size: 574 Color: 17

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 4
Size: 2877 Color: 15
Size: 382 Color: 18

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 13582 Color: 1
Size: 2897 Color: 11

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 3
Size: 2601 Color: 17

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 11
Size: 1380 Color: 3
Size: 1196 Color: 12

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 14349 Color: 13
Size: 1768 Color: 18
Size: 362 Color: 9

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 14417 Color: 3
Size: 1502 Color: 3
Size: 560 Color: 7

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 0
Size: 5958 Color: 8
Size: 528 Color: 8

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 10380 Color: 5
Size: 5532 Color: 18
Size: 566 Color: 12

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 11002 Color: 14
Size: 4566 Color: 12
Size: 910 Color: 18

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 14
Size: 4440 Color: 5
Size: 452 Color: 16

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 18
Size: 4488 Color: 10
Size: 332 Color: 15

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 4
Size: 3626 Color: 18
Size: 432 Color: 15

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 12586 Color: 19
Size: 3892 Color: 16

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 14021 Color: 12
Size: 1913 Color: 18
Size: 544 Color: 14

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 14630 Color: 5
Size: 1232 Color: 17
Size: 616 Color: 18

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 14694 Color: 5
Size: 1692 Color: 18
Size: 92 Color: 0

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 10332 Color: 0
Size: 5233 Color: 3
Size: 912 Color: 14

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 7
Size: 5203 Color: 14
Size: 344 Color: 2

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 11140 Color: 14
Size: 4561 Color: 5
Size: 776 Color: 5

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 7
Size: 4581 Color: 10
Size: 448 Color: 19

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 10
Size: 4051 Color: 14
Size: 370 Color: 17

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 12161 Color: 1
Size: 3852 Color: 19
Size: 464 Color: 10

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 8
Size: 3688 Color: 19
Size: 592 Color: 18

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 13025 Color: 8
Size: 3236 Color: 17
Size: 216 Color: 2

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13401 Color: 9
Size: 3076 Color: 8

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 13767 Color: 16
Size: 2710 Color: 15

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 13993 Color: 4
Size: 2484 Color: 17

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 14277 Color: 1
Size: 2200 Color: 2

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 6
Size: 1893 Color: 12

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 16
Size: 1817 Color: 19

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 18
Size: 5892 Color: 3
Size: 880 Color: 3

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 12552 Color: 12
Size: 3924 Color: 0

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 12593 Color: 10
Size: 3571 Color: 10
Size: 312 Color: 11

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13230 Color: 16
Size: 3246 Color: 8

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13278 Color: 17
Size: 3198 Color: 12

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 13740 Color: 13
Size: 2328 Color: 18
Size: 408 Color: 9

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 13830 Color: 8
Size: 2300 Color: 18
Size: 346 Color: 5

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14222 Color: 9
Size: 2254 Color: 16

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 12923 Color: 17
Size: 3144 Color: 11
Size: 408 Color: 0

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 13268 Color: 16
Size: 3207 Color: 2

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 13634 Color: 3
Size: 2841 Color: 17

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 0
Size: 2431 Color: 1
Size: 68 Color: 18

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 14305 Color: 1
Size: 2170 Color: 5

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 9299 Color: 5
Size: 5987 Color: 19
Size: 1188 Color: 7

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 11772 Color: 9
Size: 4184 Color: 18
Size: 518 Color: 9

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 13361 Color: 12
Size: 2861 Color: 7
Size: 252 Color: 18

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 7
Size: 2922 Color: 18
Size: 120 Color: 5

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 8
Size: 2888 Color: 9

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14325 Color: 4
Size: 2149 Color: 11

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 2
Size: 2022 Color: 9

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 14742 Color: 10
Size: 1732 Color: 1

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 14824 Color: 12
Size: 1602 Color: 19
Size: 48 Color: 7

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 9297 Color: 11
Size: 6852 Color: 18
Size: 324 Color: 10

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 10237 Color: 18
Size: 5884 Color: 17
Size: 352 Color: 13

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 12159 Color: 12
Size: 4042 Color: 1
Size: 272 Color: 13

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 1
Size: 3012 Color: 18
Size: 456 Color: 15

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 14696 Color: 15
Size: 1777 Color: 10

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 13651 Color: 4
Size: 2821 Color: 8

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 16
Size: 2284 Color: 12

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 9
Size: 1984 Color: 4

Bin 119: 9 of cap free
Amount of items: 3
Items: 
Size: 9334 Color: 1
Size: 6865 Color: 8
Size: 272 Color: 2

Bin 120: 9 of cap free
Amount of items: 3
Items: 
Size: 13385 Color: 8
Size: 2942 Color: 9
Size: 144 Color: 16

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 13631 Color: 0
Size: 2840 Color: 14

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 9358 Color: 18
Size: 6552 Color: 6
Size: 560 Color: 2

Bin 123: 10 of cap free
Amount of items: 3
Items: 
Size: 10961 Color: 19
Size: 5221 Color: 1
Size: 288 Color: 18

Bin 124: 10 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 9
Size: 1928 Color: 17
Size: 1588 Color: 18

Bin 125: 10 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 13
Size: 3241 Color: 9
Size: 176 Color: 8

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14209 Color: 7
Size: 2261 Color: 2

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 14708 Color: 2
Size: 1762 Color: 9

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 12868 Color: 0
Size: 3601 Color: 3

Bin 129: 12 of cap free
Amount of items: 32
Items: 
Size: 724 Color: 0
Size: 640 Color: 19
Size: 640 Color: 18
Size: 632 Color: 9
Size: 624 Color: 11
Size: 608 Color: 15
Size: 586 Color: 12
Size: 584 Color: 14
Size: 584 Color: 14
Size: 578 Color: 12
Size: 570 Color: 19
Size: 560 Color: 5
Size: 560 Color: 0
Size: 540 Color: 15
Size: 536 Color: 6
Size: 532 Color: 15
Size: 496 Color: 9
Size: 496 Color: 1
Size: 496 Color: 0
Size: 480 Color: 19
Size: 480 Color: 11
Size: 476 Color: 3
Size: 474 Color: 0
Size: 472 Color: 14
Size: 448 Color: 10
Size: 440 Color: 6
Size: 436 Color: 10
Size: 416 Color: 9
Size: 386 Color: 7
Size: 366 Color: 13
Size: 304 Color: 13
Size: 304 Color: 13

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 5
Size: 6844 Color: 18
Size: 272 Color: 11

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 10524 Color: 8
Size: 5944 Color: 11

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 13080 Color: 0
Size: 3388 Color: 15

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 14486 Color: 0
Size: 1982 Color: 6

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 14394 Color: 2
Size: 2073 Color: 14

Bin 135: 14 of cap free
Amount of items: 9
Items: 
Size: 8242 Color: 12
Size: 1184 Color: 15
Size: 1176 Color: 4
Size: 1168 Color: 9
Size: 1120 Color: 14
Size: 1072 Color: 16
Size: 944 Color: 1
Size: 888 Color: 13
Size: 672 Color: 4

Bin 136: 14 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 4
Size: 2138 Color: 17
Size: 92 Color: 11

Bin 137: 15 of cap free
Amount of items: 3
Items: 
Size: 9104 Color: 8
Size: 5985 Color: 11
Size: 1376 Color: 12

Bin 138: 15 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 3
Size: 2965 Color: 14

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 14253 Color: 19
Size: 2211 Color: 7

Bin 140: 16 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 16
Size: 2008 Color: 14
Size: 96 Color: 6

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 17
Size: 1816 Color: 9

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 13
Size: 2375 Color: 9

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 10806 Color: 15
Size: 5656 Color: 8

Bin 144: 18 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 17
Size: 2428 Color: 14
Size: 432 Color: 18

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 3
Size: 2684 Color: 0

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 14580 Color: 0
Size: 1882 Color: 13

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 13032 Color: 5
Size: 3428 Color: 8

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 14086 Color: 3
Size: 2374 Color: 7

Bin 149: 21 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 17
Size: 3896 Color: 12

Bin 150: 21 of cap free
Amount of items: 2
Items: 
Size: 14646 Color: 9
Size: 1813 Color: 1

Bin 151: 22 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 2
Size: 5346 Color: 0

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 14370 Color: 8
Size: 2088 Color: 0

Bin 153: 24 of cap free
Amount of items: 3
Items: 
Size: 9286 Color: 17
Size: 6862 Color: 9
Size: 308 Color: 15

Bin 154: 24 of cap free
Amount of items: 2
Items: 
Size: 14054 Color: 7
Size: 2402 Color: 17

Bin 155: 27 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 7
Size: 2881 Color: 11

Bin 156: 27 of cap free
Amount of items: 2
Items: 
Size: 14393 Color: 0
Size: 2060 Color: 11

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 13772 Color: 12
Size: 2678 Color: 15

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 14828 Color: 6
Size: 1622 Color: 7

Bin 159: 32 of cap free
Amount of items: 3
Items: 
Size: 8632 Color: 17
Size: 6872 Color: 2
Size: 944 Color: 14

Bin 160: 32 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 11
Size: 3586 Color: 2
Size: 216 Color: 16

Bin 161: 34 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 13
Size: 2336 Color: 7

Bin 162: 35 of cap free
Amount of items: 2
Items: 
Size: 11029 Color: 19
Size: 5416 Color: 13

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 10
Size: 4626 Color: 8

Bin 164: 38 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 11
Size: 3178 Color: 6

Bin 165: 39 of cap free
Amount of items: 2
Items: 
Size: 11188 Color: 9
Size: 5253 Color: 4

Bin 166: 40 of cap free
Amount of items: 2
Items: 
Size: 14250 Color: 16
Size: 2190 Color: 3

Bin 167: 42 of cap free
Amount of items: 2
Items: 
Size: 12974 Color: 13
Size: 3464 Color: 5

Bin 168: 43 of cap free
Amount of items: 2
Items: 
Size: 12836 Color: 17
Size: 3601 Color: 1

Bin 169: 45 of cap free
Amount of items: 2
Items: 
Size: 13854 Color: 7
Size: 2581 Color: 4

Bin 170: 49 of cap free
Amount of items: 6
Items: 
Size: 8248 Color: 18
Size: 1792 Color: 2
Size: 1752 Color: 13
Size: 1741 Color: 10
Size: 1702 Color: 7
Size: 1196 Color: 2

Bin 171: 52 of cap free
Amount of items: 2
Items: 
Size: 11464 Color: 5
Size: 4964 Color: 7

Bin 172: 53 of cap free
Amount of items: 2
Items: 
Size: 11651 Color: 10
Size: 4776 Color: 19

Bin 173: 59 of cap free
Amount of items: 2
Items: 
Size: 14161 Color: 0
Size: 2260 Color: 9

Bin 174: 60 of cap free
Amount of items: 2
Items: 
Size: 14369 Color: 13
Size: 2051 Color: 10

Bin 175: 63 of cap free
Amount of items: 2
Items: 
Size: 12957 Color: 12
Size: 3460 Color: 2

Bin 176: 64 of cap free
Amount of items: 2
Items: 
Size: 13848 Color: 12
Size: 2568 Color: 15

Bin 177: 65 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 5
Size: 4087 Color: 11

Bin 178: 66 of cap free
Amount of items: 2
Items: 
Size: 12332 Color: 11
Size: 4082 Color: 5

Bin 179: 67 of cap free
Amount of items: 2
Items: 
Size: 11812 Color: 16
Size: 4601 Color: 0

Bin 180: 72 of cap free
Amount of items: 2
Items: 
Size: 10760 Color: 9
Size: 5648 Color: 17

Bin 181: 75 of cap free
Amount of items: 2
Items: 
Size: 13117 Color: 12
Size: 3288 Color: 16

Bin 182: 76 of cap free
Amount of items: 2
Items: 
Size: 8276 Color: 12
Size: 8128 Color: 7

Bin 183: 76 of cap free
Amount of items: 2
Items: 
Size: 12633 Color: 4
Size: 3771 Color: 11

Bin 184: 78 of cap free
Amount of items: 2
Items: 
Size: 12202 Color: 13
Size: 4200 Color: 2

Bin 185: 79 of cap free
Amount of items: 2
Items: 
Size: 10217 Color: 5
Size: 6184 Color: 14

Bin 186: 82 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 11
Size: 4022 Color: 18
Size: 1216 Color: 17

Bin 187: 84 of cap free
Amount of items: 2
Items: 
Size: 11009 Color: 10
Size: 5387 Color: 7

Bin 188: 95 of cap free
Amount of items: 2
Items: 
Size: 10225 Color: 14
Size: 6160 Color: 12

Bin 189: 116 of cap free
Amount of items: 2
Items: 
Size: 11634 Color: 10
Size: 4730 Color: 0

Bin 190: 119 of cap free
Amount of items: 2
Items: 
Size: 10201 Color: 15
Size: 6160 Color: 17

Bin 191: 131 of cap free
Amount of items: 6
Items: 
Size: 8250 Color: 6
Size: 1978 Color: 6
Size: 1876 Color: 10
Size: 1797 Color: 11
Size: 1796 Color: 11
Size: 652 Color: 1

Bin 192: 142 of cap free
Amount of items: 2
Items: 
Size: 13073 Color: 3
Size: 3265 Color: 19

Bin 193: 184 of cap free
Amount of items: 2
Items: 
Size: 9428 Color: 19
Size: 6868 Color: 6

Bin 194: 190 of cap free
Amount of items: 21
Items: 
Size: 912 Color: 5
Size: 908 Color: 8
Size: 880 Color: 1
Size: 872 Color: 15
Size: 832 Color: 6
Size: 832 Color: 0
Size: 812 Color: 5
Size: 808 Color: 6
Size: 804 Color: 1
Size: 800 Color: 11
Size: 800 Color: 1
Size: 784 Color: 1
Size: 768 Color: 17
Size: 768 Color: 11
Size: 752 Color: 11
Size: 742 Color: 3
Size: 720 Color: 2
Size: 712 Color: 10
Size: 688 Color: 2
Size: 688 Color: 2
Size: 408 Color: 13

Bin 195: 201 of cap free
Amount of items: 2
Items: 
Size: 9412 Color: 16
Size: 6867 Color: 18

Bin 196: 225 of cap free
Amount of items: 7
Items: 
Size: 8243 Color: 18
Size: 1372 Color: 15
Size: 1368 Color: 12
Size: 1368 Color: 4
Size: 1360 Color: 6
Size: 1360 Color: 4
Size: 1184 Color: 5

Bin 197: 232 of cap free
Amount of items: 2
Items: 
Size: 9064 Color: 18
Size: 7184 Color: 17

Bin 198: 234 of cap free
Amount of items: 2
Items: 
Size: 9380 Color: 3
Size: 6866 Color: 6

Bin 199: 12070 of cap free
Amount of items: 13
Items: 
Size: 404 Color: 8
Size: 384 Color: 16
Size: 384 Color: 10
Size: 376 Color: 16
Size: 376 Color: 15
Size: 362 Color: 2
Size: 352 Color: 3
Size: 304 Color: 17
Size: 300 Color: 13
Size: 296 Color: 7
Size: 296 Color: 5
Size: 288 Color: 17
Size: 288 Color: 4

Total size: 3263040
Total free space: 16480

