Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 253 Color: 0
Size: 336 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 291 Color: 1
Size: 284 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 296 Color: 1
Size: 286 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 287 Color: 1
Size: 279 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 357 Color: 1
Size: 264 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 314 Color: 1
Size: 283 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 1
Size: 250 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 326 Color: 1
Size: 296 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 264 Color: 1
Size: 258 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 319 Color: 1
Size: 253 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 310 Color: 1
Size: 281 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 251 Color: 0
Size: 251 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 315 Color: 1
Size: 284 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 328 Color: 1
Size: 293 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 346 Color: 1
Size: 252 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 304 Color: 1
Size: 282 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 281 Color: 1
Size: 264 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 276 Color: 1
Size: 254 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 1
Size: 253 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 252 Color: 1
Size: 252 Color: 0

Total size: 20000
Total free space: 0

