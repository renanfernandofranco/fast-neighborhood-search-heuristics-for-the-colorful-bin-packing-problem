Capicity Bin: 6528
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 3272 Color: 276
Size: 1564 Color: 219
Size: 1228 Color: 195
Size: 252 Color: 79
Size: 212 Color: 60

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4232 Color: 303
Size: 2172 Color: 248
Size: 124 Color: 20

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4478 Color: 314
Size: 1382 Color: 208
Size: 668 Color: 146

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4828 Color: 328
Size: 1420 Color: 212
Size: 280 Color: 85

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4867 Color: 331
Size: 1241 Color: 199
Size: 420 Color: 113

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4876 Color: 333
Size: 1380 Color: 207
Size: 272 Color: 82

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4968 Color: 336
Size: 1304 Color: 204
Size: 256 Color: 80

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5039 Color: 340
Size: 1121 Color: 189
Size: 368 Color: 106

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 341
Size: 1108 Color: 187
Size: 372 Color: 107

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5050 Color: 342
Size: 792 Color: 162
Size: 686 Color: 147

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 344
Size: 1232 Color: 196
Size: 224 Color: 68

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 349
Size: 884 Color: 169
Size: 480 Color: 121

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 350
Size: 1126 Color: 190
Size: 224 Color: 69

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5198 Color: 352
Size: 906 Color: 172
Size: 424 Color: 114

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 353
Size: 1194 Color: 193
Size: 130 Color: 25

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5240 Color: 355
Size: 748 Color: 158
Size: 540 Color: 131

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5310 Color: 360
Size: 1018 Color: 181
Size: 200 Color: 56

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5342 Color: 362
Size: 730 Color: 155
Size: 456 Color: 117

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 363
Size: 988 Color: 177
Size: 192 Color: 48

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5352 Color: 364
Size: 1012 Color: 180
Size: 164 Color: 40

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5368 Color: 365
Size: 984 Color: 176
Size: 176 Color: 43

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 367
Size: 734 Color: 156
Size: 352 Color: 101

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5464 Color: 370
Size: 536 Color: 126
Size: 528 Color: 124

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 371
Size: 644 Color: 144
Size: 416 Color: 112

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 372
Size: 536 Color: 127
Size: 468 Color: 118

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 374
Size: 644 Color: 145
Size: 336 Color: 98

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5578 Color: 376
Size: 766 Color: 160
Size: 184 Color: 47

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 378
Size: 634 Color: 143
Size: 288 Color: 88

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 382
Size: 826 Color: 166
Size: 48 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5656 Color: 383
Size: 728 Color: 153
Size: 144 Color: 30

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 384
Size: 528 Color: 125
Size: 340 Color: 99

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 385
Size: 728 Color: 154
Size: 112 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5720 Color: 388
Size: 568 Color: 136
Size: 240 Color: 73

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5722 Color: 389
Size: 542 Color: 135
Size: 264 Color: 81

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5738 Color: 391
Size: 590 Color: 140
Size: 200 Color: 55

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5770 Color: 394
Size: 400 Color: 109
Size: 358 Color: 103

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5832 Color: 399
Size: 496 Color: 122
Size: 200 Color: 54

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 400
Size: 582 Color: 138
Size: 112 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 401
Size: 472 Color: 119
Size: 220 Color: 64

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5848 Color: 402
Size: 476 Color: 120
Size: 204 Color: 57

Bin 41: 1 of cap free
Amount of items: 5
Items: 
Size: 3284 Color: 281
Size: 2711 Color: 263
Size: 180 Color: 46
Size: 176 Color: 44
Size: 176 Color: 42

Bin 42: 1 of cap free
Amount of items: 4
Items: 
Size: 3966 Color: 293
Size: 1589 Color: 223
Size: 844 Color: 168
Size: 128 Color: 24

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4135 Color: 300
Size: 2268 Color: 250
Size: 124 Color: 21

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 329
Size: 1192 Color: 192
Size: 504 Color: 123

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 5009 Color: 337
Size: 1518 Color: 217

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 5098 Color: 345
Size: 1429 Color: 213

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 5112 Color: 346
Size: 1415 Color: 211

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 348
Size: 1234 Color: 197
Size: 136 Color: 26

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 5183 Color: 351
Size: 1344 Color: 206

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 5205 Color: 354
Size: 1322 Color: 205

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 5303 Color: 359
Size: 1008 Color: 179
Size: 216 Color: 61

Bin 52: 2 of cap free
Amount of items: 5
Items: 
Size: 3300 Color: 283
Size: 2714 Color: 265
Size: 192 Color: 51
Size: 160 Color: 38
Size: 160 Color: 37

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 3658 Color: 287
Size: 2724 Color: 270
Size: 144 Color: 31

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 4742 Color: 326
Size: 1784 Color: 231

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 327
Size: 1526 Color: 218
Size: 224 Color: 67

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 5624 Color: 380
Size: 902 Color: 171

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 390
Size: 794 Color: 163

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 392
Size: 770 Color: 161

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 5788 Color: 395
Size: 738 Color: 157

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 5806 Color: 396
Size: 720 Color: 150

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 5035 Color: 339
Size: 1490 Color: 215

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 5284 Color: 358
Size: 1241 Color: 200

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 3812 Color: 292
Size: 2568 Color: 257
Size: 144 Color: 27

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 4404 Color: 311
Size: 2120 Color: 245

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 4870 Color: 332
Size: 1654 Color: 225

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 343
Size: 1464 Color: 214

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 5316 Color: 361
Size: 1196 Color: 194
Size: 12 Color: 0

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 5414 Color: 366
Size: 1110 Color: 188

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 5444 Color: 368
Size: 1080 Color: 185

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 5636 Color: 381
Size: 888 Color: 170

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 5690 Color: 386
Size: 834 Color: 167

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 5710 Color: 387
Size: 814 Color: 164

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 393
Size: 760 Color: 159

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 397
Size: 712 Color: 149

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 398
Size: 702 Color: 148

Bin 76: 5 of cap free
Amount of items: 5
Items: 
Size: 3290 Color: 282
Size: 2713 Color: 264
Size: 196 Color: 53
Size: 164 Color: 41
Size: 160 Color: 39

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 4373 Color: 310
Size: 2149 Color: 247

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 5554 Color: 375
Size: 968 Color: 175

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 5592 Color: 377
Size: 930 Color: 174

Bin 80: 7 of cap free
Amount of items: 9
Items: 
Size: 3265 Color: 272
Size: 542 Color: 134
Size: 542 Color: 133
Size: 540 Color: 132
Size: 540 Color: 130
Size: 368 Color: 105
Size: 244 Color: 75
Size: 240 Color: 74
Size: 240 Color: 72

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 3709 Color: 289
Size: 2668 Color: 259
Size: 144 Color: 28

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 4405 Color: 312
Size: 2004 Color: 240
Size: 112 Color: 13

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 4026 Color: 296
Size: 2366 Color: 254
Size: 128 Color: 23

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 5252 Color: 357
Size: 1268 Color: 203

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 5450 Color: 369
Size: 1070 Color: 184

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 5530 Color: 373
Size: 990 Color: 178

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 4124 Color: 299
Size: 2394 Color: 255

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 4942 Color: 335
Size: 1576 Color: 222

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 5610 Color: 379
Size: 908 Color: 173

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 4835 Color: 330
Size: 1682 Color: 226

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 5012 Color: 338
Size: 1504 Color: 216

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 3787 Color: 291
Size: 2728 Color: 271

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 4164 Color: 301
Size: 2351 Color: 253

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 347
Size: 1385 Color: 209

Bin 95: 14 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 305
Size: 2120 Color: 244
Size: 120 Color: 18

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 4306 Color: 306
Size: 2208 Color: 249

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 323
Size: 1854 Color: 235

Bin 98: 15 of cap free
Amount of items: 7
Items: 
Size: 3268 Color: 274
Size: 1021 Color: 182
Size: 724 Color: 152
Size: 720 Color: 151
Size: 336 Color: 97
Size: 224 Color: 65
Size: 220 Color: 63

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 5246 Color: 356
Size: 1267 Color: 202

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 4584 Color: 317
Size: 1928 Color: 238

Bin 101: 17 of cap free
Amount of items: 4
Items: 
Size: 4650 Color: 322
Size: 1797 Color: 232
Size: 32 Color: 2
Size: 32 Color: 1

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 4710 Color: 325
Size: 1801 Color: 233

Bin 103: 18 of cap free
Amount of items: 3
Items: 
Size: 4308 Color: 307
Size: 2086 Color: 242
Size: 116 Color: 17

Bin 104: 19 of cap free
Amount of items: 3
Items: 
Size: 4096 Color: 298
Size: 2285 Color: 251
Size: 128 Color: 22

Bin 105: 19 of cap free
Amount of items: 4
Items: 
Size: 4649 Color: 321
Size: 1772 Color: 230
Size: 52 Color: 5
Size: 36 Color: 3

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 3994 Color: 295
Size: 1411 Color: 210
Size: 1103 Color: 186

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 4200 Color: 302
Size: 2308 Color: 252

Bin 108: 20 of cap free
Amount of items: 3
Items: 
Size: 4424 Color: 313
Size: 1972 Color: 239
Size: 112 Color: 12

Bin 109: 21 of cap free
Amount of items: 2
Items: 
Size: 4369 Color: 309
Size: 2138 Color: 246

Bin 110: 22 of cap free
Amount of items: 2
Items: 
Size: 4678 Color: 324
Size: 1828 Color: 234

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 4902 Color: 334
Size: 1604 Color: 224

Bin 112: 23 of cap free
Amount of items: 3
Items: 
Size: 4271 Color: 304
Size: 2114 Color: 243
Size: 120 Color: 19

Bin 113: 25 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 308
Size: 2051 Color: 241
Size: 112 Color: 14

Bin 114: 35 of cap free
Amount of items: 5
Items: 
Size: 3269 Color: 275
Size: 1140 Color: 191
Size: 1044 Color: 183
Size: 820 Color: 165
Size: 220 Color: 62

Bin 115: 35 of cap free
Amount of items: 2
Items: 
Size: 4069 Color: 297
Size: 2424 Color: 256

Bin 116: 37 of cap free
Amount of items: 3
Items: 
Size: 4510 Color: 315
Size: 1881 Color: 236
Size: 100 Color: 11

Bin 117: 38 of cap free
Amount of items: 3
Items: 
Size: 3624 Color: 286
Size: 2722 Color: 268
Size: 144 Color: 32

Bin 118: 38 of cap free
Amount of items: 3
Items: 
Size: 3992 Color: 294
Size: 1258 Color: 201
Size: 1240 Color: 198

Bin 119: 39 of cap free
Amount of items: 4
Items: 
Size: 3464 Color: 285
Size: 2721 Color: 267
Size: 152 Color: 34
Size: 152 Color: 33

Bin 120: 40 of cap free
Amount of items: 2
Items: 
Size: 3764 Color: 290
Size: 2724 Color: 269

Bin 121: 45 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 320
Size: 1771 Color: 229
Size: 64 Color: 6

Bin 122: 50 of cap free
Amount of items: 3
Items: 
Size: 4512 Color: 316
Size: 1882 Color: 237
Size: 84 Color: 10

Bin 123: 54 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 288
Size: 2640 Color: 258
Size: 144 Color: 29

Bin 124: 54 of cap free
Amount of items: 4
Items: 
Size: 4604 Color: 318
Size: 1710 Color: 227
Size: 80 Color: 9
Size: 80 Color: 8

Bin 125: 57 of cap free
Amount of items: 3
Items: 
Size: 4623 Color: 319
Size: 1768 Color: 228
Size: 80 Color: 7

Bin 126: 122 of cap free
Amount of items: 18
Items: 
Size: 540 Color: 129
Size: 536 Color: 128
Size: 456 Color: 116
Size: 448 Color: 115
Size: 416 Color: 111
Size: 408 Color: 110
Size: 376 Color: 108
Size: 360 Color: 104
Size: 358 Color: 102
Size: 352 Color: 100
Size: 296 Color: 89
Size: 282 Color: 87
Size: 282 Color: 86
Size: 276 Color: 84
Size: 276 Color: 83
Size: 248 Color: 78
Size: 248 Color: 77
Size: 248 Color: 76

Bin 127: 122 of cap free
Amount of items: 3
Items: 
Size: 3273 Color: 277
Size: 1567 Color: 221
Size: 1566 Color: 220

Bin 128: 136 of cap free
Amount of items: 4
Items: 
Size: 3274 Color: 278
Size: 2702 Color: 260
Size: 208 Color: 59
Size: 208 Color: 58

Bin 129: 157 of cap free
Amount of items: 4
Items: 
Size: 3277 Color: 279
Size: 2706 Color: 261
Size: 196 Color: 52
Size: 192 Color: 50

Bin 130: 159 of cap free
Amount of items: 4
Items: 
Size: 3332 Color: 284
Size: 2717 Color: 266
Size: 160 Color: 36
Size: 160 Color: 35

Bin 131: 170 of cap free
Amount of items: 4
Items: 
Size: 3282 Color: 280
Size: 2708 Color: 262
Size: 192 Color: 49
Size: 176 Color: 45

Bin 132: 174 of cap free
Amount of items: 8
Items: 
Size: 3266 Color: 273
Size: 620 Color: 142
Size: 604 Color: 141
Size: 584 Color: 139
Size: 580 Color: 137
Size: 240 Color: 71
Size: 236 Color: 70
Size: 224 Color: 66

Bin 133: 4360 of cap free
Amount of items: 7
Items: 
Size: 320 Color: 96
Size: 316 Color: 95
Size: 312 Color: 94
Size: 312 Color: 93
Size: 304 Color: 92
Size: 304 Color: 91
Size: 300 Color: 90

Total size: 861696
Total free space: 6528

