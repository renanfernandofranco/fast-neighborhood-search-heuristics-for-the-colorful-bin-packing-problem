Capicity Bin: 2068
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1035 Color: 2
Size: 805 Color: 13
Size: 100 Color: 5
Size: 72 Color: 13
Size: 56 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 15
Size: 733 Color: 0
Size: 298 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1041 Color: 15
Size: 857 Color: 14
Size: 170 Color: 15

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1049 Color: 4
Size: 861 Color: 11
Size: 96 Color: 2
Size: 62 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 19
Size: 694 Color: 3
Size: 60 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 2
Size: 511 Color: 19
Size: 100 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 2
Size: 311 Color: 11
Size: 168 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 3
Size: 434 Color: 14
Size: 44 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 17
Size: 317 Color: 13
Size: 146 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 4
Size: 386 Color: 16
Size: 76 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 18
Size: 380 Color: 2
Size: 66 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 12
Size: 296 Color: 1
Size: 56 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 3
Size: 281 Color: 5
Size: 56 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 1
Size: 189 Color: 0
Size: 112 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 9
Size: 242 Color: 17
Size: 48 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 10
Size: 227 Color: 5
Size: 44 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 8
Size: 178 Color: 17
Size: 84 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 17
Size: 183 Color: 5
Size: 42 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 9
Size: 203 Color: 9
Size: 16 Color: 17

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 3
Size: 853 Color: 5
Size: 68 Color: 7

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1300 Color: 18
Size: 691 Color: 0
Size: 76 Color: 7

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1307 Color: 14
Size: 696 Color: 12
Size: 64 Color: 9

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 13
Size: 429 Color: 2
Size: 152 Color: 19

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 428 Color: 6
Size: 84 Color: 12

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1612 Color: 2
Size: 283 Color: 18
Size: 172 Color: 8

Bin 26: 1 of cap free
Amount of items: 4
Items: 
Size: 1697 Color: 6
Size: 232 Color: 0
Size: 106 Color: 2
Size: 32 Color: 16

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 13
Size: 232 Color: 4
Size: 60 Color: 11

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1789 Color: 6
Size: 278 Color: 18

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 11
Size: 259 Color: 0
Size: 14 Color: 7

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 9
Size: 230 Color: 2
Size: 24 Color: 7

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1816 Color: 6
Size: 251 Color: 11

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1834 Color: 14
Size: 233 Color: 15

Bin 33: 2 of cap free
Amount of items: 21
Items: 
Size: 310 Color: 8
Size: 182 Color: 1
Size: 170 Color: 1
Size: 168 Color: 16
Size: 124 Color: 0
Size: 120 Color: 15
Size: 110 Color: 17
Size: 88 Color: 13
Size: 78 Color: 19
Size: 76 Color: 10
Size: 76 Color: 5
Size: 76 Color: 3
Size: 70 Color: 12
Size: 64 Color: 14
Size: 60 Color: 7
Size: 60 Color: 6
Size: 56 Color: 4
Size: 52 Color: 16
Size: 50 Color: 10
Size: 40 Color: 9
Size: 36 Color: 9

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 6
Size: 851 Color: 17
Size: 170 Color: 12

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 18
Size: 480 Color: 17
Size: 36 Color: 11

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 12
Size: 320 Color: 16
Size: 8 Color: 18

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1821 Color: 16
Size: 245 Color: 5

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 1
Size: 351 Color: 3

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 1
Size: 207 Color: 7

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1523 Color: 15
Size: 541 Color: 10

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1662 Color: 6
Size: 402 Color: 18

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 1
Size: 222 Color: 10

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1851 Color: 5
Size: 213 Color: 13

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1428 Color: 15
Size: 635 Color: 14

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 1689 Color: 13
Size: 374 Color: 4

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 10
Size: 198 Color: 9
Size: 136 Color: 6

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 1850 Color: 15
Size: 212 Color: 8

Bin 48: 7 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 6
Size: 578 Color: 4
Size: 78 Color: 6

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1759 Color: 13
Size: 302 Color: 17

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1197 Color: 0
Size: 861 Color: 10

Bin 51: 10 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 4
Size: 393 Color: 2
Size: 136 Color: 9

Bin 52: 11 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 17
Size: 553 Color: 9
Size: 126 Color: 6

Bin 53: 12 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 4
Size: 768 Color: 10
Size: 50 Color: 17

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 8
Size: 401 Color: 6

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1446 Color: 18
Size: 609 Color: 11

Bin 56: 14 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 13
Size: 318 Color: 19
Size: 46 Color: 15

Bin 57: 17 of cap free
Amount of items: 2
Items: 
Size: 1189 Color: 17
Size: 862 Color: 19

Bin 58: 17 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 18
Size: 770 Color: 19
Size: 40 Color: 15

Bin 59: 17 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 5
Size: 522 Color: 1
Size: 190 Color: 1

Bin 60: 17 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 16
Size: 630 Color: 19

Bin 61: 17 of cap free
Amount of items: 2
Items: 
Size: 1706 Color: 1
Size: 345 Color: 17

Bin 62: 20 of cap free
Amount of items: 2
Items: 
Size: 1597 Color: 1
Size: 451 Color: 15

Bin 63: 23 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 12
Size: 521 Color: 2
Size: 486 Color: 5

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1698 Color: 11
Size: 342 Color: 6

Bin 65: 34 of cap free
Amount of items: 2
Items: 
Size: 1647 Color: 15
Size: 387 Color: 10

Bin 66: 1716 of cap free
Amount of items: 8
Items: 
Size: 68 Color: 0
Size: 48 Color: 17
Size: 48 Color: 8
Size: 40 Color: 4
Size: 40 Color: 3
Size: 36 Color: 18
Size: 36 Color: 14
Size: 36 Color: 11

Total size: 134420
Total free space: 2068

