Capicity Bin: 7904
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 289
Size: 2792 Color: 256
Size: 192 Color: 44

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5880 Color: 323
Size: 1688 Color: 217
Size: 336 Color: 92

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 333
Size: 1450 Color: 206
Size: 298 Color: 84

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 336
Size: 891 Color: 162
Size: 780 Color: 145

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 339
Size: 1452 Color: 207
Size: 136 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 340
Size: 804 Color: 147
Size: 778 Color: 144

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6332 Color: 341
Size: 1076 Color: 183
Size: 496 Color: 122

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 342
Size: 1229 Color: 196
Size: 334 Color: 91

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 346
Size: 1204 Color: 193
Size: 240 Color: 66

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 347
Size: 1202 Color: 192
Size: 240 Color: 67

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 348
Size: 1294 Color: 198
Size: 142 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 349
Size: 1074 Color: 182
Size: 340 Color: 93

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6616 Color: 356
Size: 880 Color: 161
Size: 408 Color: 102

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 357
Size: 806 Color: 149
Size: 480 Color: 116

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 359
Size: 1098 Color: 185
Size: 164 Color: 28

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6653 Color: 360
Size: 859 Color: 158
Size: 392 Color: 100

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 363
Size: 908 Color: 164
Size: 288 Color: 80

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6712 Color: 364
Size: 1000 Color: 174
Size: 192 Color: 47

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 365
Size: 1026 Color: 178
Size: 160 Color: 24

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 370
Size: 656 Color: 132
Size: 476 Color: 113

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 371
Size: 568 Color: 128
Size: 560 Color: 126

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6810 Color: 374
Size: 834 Color: 153
Size: 260 Color: 74

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 376
Size: 850 Color: 156
Size: 220 Color: 59

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6880 Color: 379
Size: 656 Color: 131
Size: 368 Color: 98

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6906 Color: 383
Size: 834 Color: 154
Size: 164 Color: 30

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 387
Size: 488 Color: 121
Size: 480 Color: 117

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6938 Color: 388
Size: 742 Color: 139
Size: 224 Color: 60

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 391
Size: 828 Color: 152
Size: 108 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 392
Size: 762 Color: 140
Size: 170 Color: 33

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 393
Size: 792 Color: 146
Size: 128 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 394
Size: 767 Color: 142
Size: 152 Color: 21

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 395
Size: 880 Color: 159
Size: 38 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 398
Size: 482 Color: 118
Size: 404 Color: 101

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7084 Color: 400
Size: 564 Color: 127
Size: 256 Color: 71

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 401
Size: 658 Color: 135
Size: 160 Color: 25

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 402
Size: 656 Color: 130
Size: 140 Color: 12

Bin 37: 1 of cap free
Amount of items: 7
Items: 
Size: 3958 Color: 276
Size: 1322 Color: 201
Size: 1043 Color: 180
Size: 948 Color: 168
Size: 216 Color: 58
Size: 208 Color: 54
Size: 208 Color: 53

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 4958 Color: 294
Size: 2761 Color: 255
Size: 184 Color: 42

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 303
Size: 2411 Color: 243
Size: 160 Color: 27

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5334 Color: 304
Size: 1632 Color: 214
Size: 937 Color: 167

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 305
Size: 2395 Color: 242
Size: 160 Color: 26

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 324
Size: 1802 Color: 221
Size: 212 Color: 56

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6105 Color: 332
Size: 1612 Color: 212
Size: 186 Color: 43

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6525 Color: 352
Size: 1362 Color: 203
Size: 16 Color: 1

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6723 Color: 367
Size: 1180 Color: 190

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6875 Color: 378
Size: 1028 Color: 179

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6913 Color: 385
Size: 990 Color: 172

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6939 Color: 389
Size: 964 Color: 170

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 397
Size: 909 Color: 165

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 301
Size: 2444 Color: 246
Size: 168 Color: 31

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 302
Size: 2430 Color: 245
Size: 164 Color: 29

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6248 Color: 337
Size: 1654 Color: 215

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6392 Color: 344
Size: 1510 Color: 210

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6720 Color: 366
Size: 1182 Color: 191

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 368
Size: 1176 Color: 189

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6898 Color: 382
Size: 1004 Color: 175

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 399
Size: 852 Color: 157

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 5407 Color: 309
Size: 2494 Color: 251

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 326
Size: 1681 Color: 216
Size: 256 Color: 70

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 6907 Color: 384
Size: 994 Color: 173

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 386
Size: 985 Color: 171

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6987 Color: 396
Size: 914 Color: 166

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 6088 Color: 330
Size: 1812 Color: 223

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6166 Color: 335
Size: 1734 Color: 220

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 350
Size: 1384 Color: 204
Size: 16 Color: 0

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6820 Color: 375
Size: 1080 Color: 184

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6948 Color: 390
Size: 952 Color: 169

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 6583 Color: 354
Size: 1316 Color: 200

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 369
Size: 1151 Color: 188

Bin 70: 6 of cap free
Amount of items: 3
Items: 
Size: 4980 Color: 296
Size: 2742 Color: 254
Size: 176 Color: 39

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 315
Size: 2148 Color: 237
Size: 144 Color: 14

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 6053 Color: 328
Size: 1845 Color: 225

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6094 Color: 331
Size: 1804 Color: 222

Bin 74: 7 of cap free
Amount of items: 11
Items: 
Size: 3953 Color: 272
Size: 600 Color: 129
Size: 552 Color: 124
Size: 544 Color: 123
Size: 488 Color: 119
Size: 480 Color: 115
Size: 332 Color: 90
Size: 244 Color: 68
Size: 240 Color: 65
Size: 232 Color: 64
Size: 232 Color: 63

Bin 75: 7 of cap free
Amount of items: 3
Items: 
Size: 5352 Color: 306
Size: 2385 Color: 241
Size: 160 Color: 23

Bin 76: 7 of cap free
Amount of items: 3
Items: 
Size: 5673 Color: 317
Size: 2136 Color: 236
Size: 88 Color: 7

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 319
Size: 2081 Color: 231
Size: 84 Color: 5

Bin 78: 7 of cap free
Amount of items: 2
Items: 
Size: 6504 Color: 351
Size: 1393 Color: 205

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 6781 Color: 372
Size: 1116 Color: 187

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 381
Size: 1011 Color: 177

Bin 81: 8 of cap free
Amount of items: 7
Items: 
Size: 3957 Color: 275
Size: 894 Color: 163
Size: 880 Color: 160
Size: 842 Color: 155
Size: 827 Color: 151
Size: 288 Color: 83
Size: 208 Color: 55

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 353
Size: 1324 Color: 202

Bin 83: 9 of cap free
Amount of items: 2
Items: 
Size: 6691 Color: 362
Size: 1204 Color: 194

Bin 84: 10 of cap free
Amount of items: 3
Items: 
Size: 5746 Color: 320
Size: 2084 Color: 232
Size: 64 Color: 4

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 5842 Color: 322
Size: 2052 Color: 230

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 338
Size: 1620 Color: 213

Bin 87: 11 of cap free
Amount of items: 2
Items: 
Size: 6590 Color: 355
Size: 1303 Color: 199

Bin 88: 12 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 285
Size: 3200 Color: 264
Size: 192 Color: 46

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 5922 Color: 325
Size: 1970 Color: 227

Bin 90: 12 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 358
Size: 1272 Color: 197

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 361
Size: 1216 Color: 195

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 6884 Color: 380
Size: 1008 Color: 176

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 6431 Color: 345
Size: 1460 Color: 208

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 6790 Color: 373
Size: 1101 Color: 186

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 377
Size: 1054 Color: 181

Bin 96: 17 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 321
Size: 2099 Color: 233
Size: 40 Color: 3

Bin 97: 18 of cap free
Amount of items: 7
Items: 
Size: 3956 Color: 274
Size: 808 Color: 150
Size: 805 Color: 148
Size: 776 Color: 143
Size: 765 Color: 141
Size: 560 Color: 125
Size: 216 Color: 57

Bin 98: 18 of cap free
Amount of items: 2
Items: 
Size: 6062 Color: 329
Size: 1824 Color: 224

Bin 99: 18 of cap free
Amount of items: 2
Items: 
Size: 6164 Color: 334
Size: 1722 Color: 219

Bin 100: 19 of cap free
Amount of items: 2
Items: 
Size: 4591 Color: 286
Size: 3294 Color: 270

Bin 101: 22 of cap free
Amount of items: 22
Items: 
Size: 478 Color: 114
Size: 476 Color: 112
Size: 468 Color: 111
Size: 432 Color: 110
Size: 432 Color: 109
Size: 424 Color: 108
Size: 424 Color: 107
Size: 418 Color: 106
Size: 416 Color: 105
Size: 416 Color: 104
Size: 408 Color: 103
Size: 384 Color: 99
Size: 288 Color: 82
Size: 288 Color: 81
Size: 288 Color: 79
Size: 278 Color: 78
Size: 272 Color: 77
Size: 268 Color: 76
Size: 264 Color: 75
Size: 260 Color: 73
Size: 256 Color: 72
Size: 244 Color: 69

Bin 102: 22 of cap free
Amount of items: 2
Items: 
Size: 6354 Color: 343
Size: 1528 Color: 211

Bin 103: 28 of cap free
Amount of items: 4
Items: 
Size: 5444 Color: 311
Size: 2132 Color: 235
Size: 152 Color: 19
Size: 148 Color: 18

Bin 104: 29 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 284
Size: 3181 Color: 263
Size: 196 Color: 48

Bin 105: 33 of cap free
Amount of items: 2
Items: 
Size: 5387 Color: 308
Size: 2484 Color: 250

Bin 106: 36 of cap free
Amount of items: 2
Items: 
Size: 5972 Color: 327
Size: 1896 Color: 226

Bin 107: 37 of cap free
Amount of items: 3
Items: 
Size: 5043 Color: 300
Size: 2656 Color: 253
Size: 168 Color: 32

Bin 108: 38 of cap free
Amount of items: 5
Items: 
Size: 3960 Color: 277
Size: 1709 Color: 218
Size: 1501 Color: 209
Size: 488 Color: 120
Size: 208 Color: 52

Bin 109: 40 of cap free
Amount of items: 2
Items: 
Size: 5380 Color: 307
Size: 2484 Color: 249

Bin 110: 42 of cap free
Amount of items: 3
Items: 
Size: 5031 Color: 299
Size: 2655 Color: 252
Size: 176 Color: 34

Bin 111: 53 of cap free
Amount of items: 2
Items: 
Size: 4769 Color: 288
Size: 3082 Color: 262

Bin 112: 56 of cap free
Amount of items: 4
Items: 
Size: 5412 Color: 310
Size: 2132 Color: 234
Size: 152 Color: 22
Size: 152 Color: 20

Bin 113: 67 of cap free
Amount of items: 4
Items: 
Size: 3964 Color: 278
Size: 3471 Color: 271
Size: 202 Color: 51
Size: 200 Color: 50

Bin 114: 73 of cap free
Amount of items: 4
Items: 
Size: 5011 Color: 298
Size: 2468 Color: 248
Size: 176 Color: 36
Size: 176 Color: 35

Bin 115: 81 of cap free
Amount of items: 3
Items: 
Size: 5691 Color: 318
Size: 2044 Color: 229
Size: 88 Color: 6

Bin 116: 86 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 316
Size: 2038 Color: 228
Size: 140 Color: 11

Bin 117: 91 of cap free
Amount of items: 3
Items: 
Size: 4618 Color: 287
Size: 3003 Color: 261
Size: 192 Color: 45

Bin 118: 91 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 314
Size: 2193 Color: 240
Size: 144 Color: 15

Bin 119: 94 of cap free
Amount of items: 4
Items: 
Size: 5000 Color: 297
Size: 2458 Color: 247
Size: 176 Color: 38
Size: 176 Color: 37

Bin 120: 100 of cap free
Amount of items: 2
Items: 
Size: 4948 Color: 293
Size: 2856 Color: 260

Bin 121: 106 of cap free
Amount of items: 3
Items: 
Size: 5472 Color: 313
Size: 2182 Color: 239
Size: 144 Color: 16

Bin 122: 110 of cap free
Amount of items: 2
Items: 
Size: 4942 Color: 292
Size: 2852 Color: 259

Bin 123: 120 of cap free
Amount of items: 8
Items: 
Size: 3954 Color: 273
Size: 714 Color: 138
Size: 684 Color: 137
Size: 668 Color: 136
Size: 656 Color: 134
Size: 656 Color: 133
Size: 228 Color: 62
Size: 224 Color: 61

Bin 124: 121 of cap free
Amount of items: 2
Items: 
Size: 4490 Color: 283
Size: 3293 Color: 269

Bin 125: 123 of cap free
Amount of items: 3
Items: 
Size: 4301 Color: 279
Size: 3284 Color: 265
Size: 196 Color: 49

Bin 126: 124 of cap free
Amount of items: 2
Items: 
Size: 4488 Color: 282
Size: 3292 Color: 268

Bin 127: 128 of cap free
Amount of items: 2
Items: 
Size: 4932 Color: 291
Size: 2844 Color: 258

Bin 128: 130 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 281
Size: 3290 Color: 267

Bin 129: 134 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 312
Size: 2164 Color: 238
Size: 144 Color: 17

Bin 130: 137 of cap free
Amount of items: 2
Items: 
Size: 4479 Color: 280
Size: 3288 Color: 266

Bin 131: 138 of cap free
Amount of items: 2
Items: 
Size: 4924 Color: 290
Size: 2842 Color: 257

Bin 132: 140 of cap free
Amount of items: 4
Items: 
Size: 4976 Color: 295
Size: 2424 Color: 244
Size: 184 Color: 41
Size: 180 Color: 40

Bin 133: 4872 of cap free
Amount of items: 9
Items: 
Size: 368 Color: 97
Size: 360 Color: 96
Size: 356 Color: 95
Size: 352 Color: 94
Size: 328 Color: 89
Size: 328 Color: 88
Size: 320 Color: 87
Size: 320 Color: 86
Size: 300 Color: 85

Total size: 1043328
Total free space: 7904

