Capicity Bin: 2052
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 828 Color: 0
Size: 692 Color: 1
Size: 428 Color: 4
Size: 52 Color: 4
Size: 52 Color: 4

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1420 Color: 4
Size: 502 Color: 1
Size: 68 Color: 2
Size: 46 Color: 2
Size: 16 Color: 3

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1638 Color: 2
Size: 282 Color: 2
Size: 72 Color: 1
Size: 56 Color: 1
Size: 4 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 4
Size: 178 Color: 3
Size: 148 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1027 Color: 2
Size: 751 Color: 1
Size: 274 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 3
Size: 314 Color: 2
Size: 60 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 3
Size: 845 Color: 3
Size: 96 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1138 Color: 2
Size: 762 Color: 1
Size: 152 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 0
Size: 478 Color: 1
Size: 68 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1275 Color: 1
Size: 649 Color: 0
Size: 128 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 0
Size: 231 Color: 1
Size: 76 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 2
Size: 170 Color: 4
Size: 40 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 4
Size: 214 Color: 1
Size: 40 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 3
Size: 239 Color: 3
Size: 46 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1367 Color: 4
Size: 571 Color: 2
Size: 114 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 3
Size: 342 Color: 4
Size: 24 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 3
Size: 385 Color: 4
Size: 76 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1772 Color: 1
Size: 240 Color: 4
Size: 40 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 1
Size: 187 Color: 4
Size: 36 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1359 Color: 2
Size: 579 Color: 1
Size: 114 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 2
Size: 741 Color: 1
Size: 148 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 1
Size: 391 Color: 4
Size: 78 Color: 2

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1734 Color: 3
Size: 228 Color: 1
Size: 48 Color: 0
Size: 42 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 2
Size: 467 Color: 2
Size: 92 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 2
Size: 570 Color: 3
Size: 112 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 4
Size: 316 Color: 1
Size: 32 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 0
Size: 346 Color: 3
Size: 228 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1353 Color: 1
Size: 583 Color: 4
Size: 116 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 3
Size: 174 Color: 1
Size: 32 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 3
Size: 313 Color: 1
Size: 62 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 1
Size: 389 Color: 1
Size: 46 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 4
Size: 287 Color: 1
Size: 56 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 218 Color: 2
Size: 40 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 3
Size: 402 Color: 2
Size: 80 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1477 Color: 0
Size: 481 Color: 2
Size: 94 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 1
Size: 855 Color: 2
Size: 44 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 2
Size: 219 Color: 4
Size: 50 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 3
Size: 293 Color: 1
Size: 58 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1030 Color: 2
Size: 854 Color: 0
Size: 168 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 2
Size: 897 Color: 2
Size: 124 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 3
Size: 331 Color: 1
Size: 64 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 0
Size: 655 Color: 3
Size: 130 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 2
Size: 593 Color: 4
Size: 84 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 1
Size: 263 Color: 2
Size: 52 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 2
Size: 357 Color: 0
Size: 70 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 0
Size: 686 Color: 1
Size: 136 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 0
Size: 431 Color: 1
Size: 86 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 3
Size: 262 Color: 3
Size: 48 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1306 Color: 3
Size: 622 Color: 2
Size: 124 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 271 Color: 0
Size: 52 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 2
Size: 251 Color: 3
Size: 48 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 2
Size: 514 Color: 1
Size: 100 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 1
Size: 225 Color: 4
Size: 52 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 2
Size: 531 Color: 0
Size: 106 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 4
Size: 362 Color: 0
Size: 72 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1288 Color: 4
Size: 472 Color: 2
Size: 292 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 4
Size: 525 Color: 2
Size: 104 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 2
Size: 382 Color: 2
Size: 72 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 229 Color: 4
Size: 24 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 2
Size: 422 Color: 3
Size: 84 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1485 Color: 3
Size: 473 Color: 1
Size: 94 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 1
Size: 663 Color: 0
Size: 132 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 2
Size: 266 Color: 4
Size: 68 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 1
Size: 257 Color: 2
Size: 4 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 1
Size: 425 Color: 1
Size: 84 Color: 2

Total size: 133380
Total free space: 0

