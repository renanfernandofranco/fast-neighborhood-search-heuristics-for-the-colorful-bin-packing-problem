Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 18
Size: 295 Color: 7
Size: 287 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 2
Size: 250 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 18
Size: 265 Color: 3
Size: 262 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 4
Size: 363 Color: 16
Size: 252 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 10
Size: 350 Color: 4
Size: 300 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 8
Size: 295 Color: 0
Size: 259 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 2
Size: 258 Color: 10
Size: 255 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 6
Size: 343 Color: 9
Size: 273 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 17
Size: 348 Color: 3
Size: 273 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 5
Size: 292 Color: 11
Size: 272 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 18
Size: 352 Color: 19
Size: 271 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 5
Size: 309 Color: 13
Size: 259 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 16
Size: 299 Color: 14
Size: 255 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 17
Size: 336 Color: 12
Size: 274 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 7
Size: 341 Color: 1
Size: 252 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 9
Size: 309 Color: 14
Size: 261 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 12
Size: 251 Color: 7
Size: 328 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 10
Size: 260 Color: 5
Size: 252 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 15
Size: 323 Color: 13
Size: 301 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 19
Size: 296 Color: 16
Size: 295 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 17
Size: 281 Color: 1
Size: 254 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 346 Color: 14
Size: 271 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 18
Size: 252 Color: 18
Size: 251 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 11
Size: 337 Color: 8
Size: 295 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 310 Color: 14
Size: 259 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 13
Size: 293 Color: 15
Size: 267 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 8
Size: 279 Color: 9
Size: 250 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9
Size: 265 Color: 19
Size: 257 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 5
Size: 325 Color: 9
Size: 301 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 18
Size: 363 Color: 9
Size: 268 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 322 Color: 7
Size: 259 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 17
Size: 299 Color: 7
Size: 265 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 365 Color: 17
Size: 267 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 16
Size: 280 Color: 3
Size: 274 Color: 16

Total size: 34034
Total free space: 0

