Capicity Bin: 2000
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1405 Color: 3
Size: 457 Color: 1
Size: 98 Color: 4
Size: 32 Color: 2
Size: 8 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1794 Color: 1
Size: 182 Color: 1
Size: 16 Color: 2
Size: 8 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 1
Size: 669 Color: 0
Size: 132 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 2
Size: 213 Color: 1
Size: 42 Color: 3

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1256 Color: 4
Size: 460 Color: 3
Size: 220 Color: 2
Size: 64 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 0
Size: 246 Color: 0
Size: 48 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 3
Size: 362 Color: 3
Size: 68 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 2
Size: 493 Color: 1
Size: 98 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 2
Size: 302 Color: 3
Size: 32 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 3
Size: 230 Color: 1
Size: 44 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 1
Size: 174 Color: 3
Size: 40 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 1
Size: 265 Color: 1
Size: 52 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 4
Size: 663 Color: 3
Size: 132 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 0
Size: 231 Color: 4
Size: 44 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 4
Size: 202 Color: 1
Size: 40 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1002 Color: 2
Size: 834 Color: 1
Size: 164 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1005 Color: 2
Size: 907 Color: 3
Size: 88 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 2
Size: 398 Color: 0
Size: 76 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1592 Color: 3
Size: 344 Color: 2
Size: 64 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1485 Color: 0
Size: 431 Color: 1
Size: 84 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 1
Size: 311 Color: 3
Size: 60 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1134 Color: 3
Size: 738 Color: 0
Size: 128 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 1
Size: 361 Color: 3
Size: 72 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 4
Size: 289 Color: 4
Size: 56 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1065 Color: 2
Size: 781 Color: 1
Size: 154 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1479 Color: 4
Size: 501 Color: 2
Size: 20 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1724 Color: 1
Size: 220 Color: 4
Size: 56 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 2
Size: 501 Color: 1
Size: 98 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 3
Size: 282 Color: 3
Size: 76 Color: 1

Bin 30: 0 of cap free
Amount of items: 4
Items: 
Size: 808 Color: 4
Size: 676 Color: 0
Size: 416 Color: 0
Size: 100 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 0
Size: 210 Color: 0
Size: 40 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 4
Size: 343 Color: 1
Size: 68 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 2
Size: 218 Color: 3
Size: 40 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 0
Size: 562 Color: 0
Size: 112 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1515 Color: 3
Size: 405 Color: 0
Size: 80 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 1
Size: 571 Color: 4
Size: 112 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 0
Size: 330 Color: 3
Size: 64 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 2
Size: 181 Color: 0
Size: 36 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1001 Color: 1
Size: 833 Color: 3
Size: 166 Color: 0

Bin 40: 0 of cap free
Amount of items: 4
Items: 
Size: 1384 Color: 0
Size: 308 Color: 0
Size: 236 Color: 3
Size: 72 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 2
Size: 442 Color: 0
Size: 88 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 0
Size: 325 Color: 2
Size: 64 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 1
Size: 498 Color: 2
Size: 96 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 3
Size: 642 Color: 1
Size: 128 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 2
Size: 186 Color: 1
Size: 36 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 1
Size: 295 Color: 3
Size: 58 Color: 0

Bin 47: 0 of cap free
Amount of items: 4
Items: 
Size: 1399 Color: 2
Size: 497 Color: 4
Size: 52 Color: 4
Size: 52 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 2
Size: 211 Color: 2
Size: 40 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 0
Size: 263 Color: 0
Size: 52 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 4
Size: 245 Color: 3
Size: 48 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 1
Size: 487 Color: 1
Size: 96 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1309 Color: 2
Size: 577 Color: 4
Size: 114 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 0
Size: 237 Color: 1
Size: 46 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 3
Size: 773 Color: 1
Size: 154 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 1
Size: 291 Color: 1
Size: 58 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 0
Size: 262 Color: 1
Size: 52 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 2
Size: 197 Color: 2
Size: 38 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1069 Color: 3
Size: 777 Color: 3
Size: 154 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 3
Size: 241 Color: 4
Size: 46 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1081 Color: 2
Size: 767 Color: 4
Size: 152 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 1
Size: 383 Color: 3
Size: 76 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 0
Size: 225 Color: 0
Size: 34 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1303 Color: 1
Size: 581 Color: 2
Size: 116 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 3
Size: 187 Color: 0
Size: 36 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 4
Size: 573 Color: 1
Size: 114 Color: 1

Total size: 130000
Total free space: 0

