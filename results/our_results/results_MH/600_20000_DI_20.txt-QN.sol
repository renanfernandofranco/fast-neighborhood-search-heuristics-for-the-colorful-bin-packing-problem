Capicity Bin: 19232
Lower Bound: 198

Bins used: 198
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 9504 Color: 404
Size: 2720 Color: 265
Size: 2368 Color: 252
Size: 1632 Color: 204
Size: 1152 Color: 165
Size: 1024 Color: 152
Size: 832 Color: 134

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 13904 Color: 469
Size: 4464 Color: 334
Size: 320 Color: 18
Size: 288 Color: 11
Size: 160 Color: 7
Size: 96 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11440 Color: 436
Size: 6512 Color: 367
Size: 1280 Color: 174

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 15824 Color: 533
Size: 2864 Color: 271
Size: 544 Color: 75

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 16276 Color: 546
Size: 2468 Color: 256
Size: 488 Color: 65

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 16744 Color: 564
Size: 2088 Color: 237
Size: 400 Color: 47

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 17234 Color: 596
Size: 1666 Color: 207
Size: 332 Color: 20

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 417
Size: 7640 Color: 389
Size: 1520 Color: 191

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15456 Color: 517
Size: 2912 Color: 274
Size: 864 Color: 139

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15192 Color: 504
Size: 3368 Color: 300
Size: 672 Color: 108

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 17076 Color: 587
Size: 1804 Color: 215
Size: 352 Color: 28

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14274 Color: 480
Size: 4134 Color: 325
Size: 824 Color: 132

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 16846 Color: 571
Size: 1990 Color: 230
Size: 396 Color: 44

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9624 Color: 410
Size: 8472 Color: 403
Size: 1136 Color: 164

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13378 Color: 459
Size: 4970 Color: 346
Size: 884 Color: 143

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16988 Color: 579
Size: 1876 Color: 221
Size: 368 Color: 34

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15300 Color: 507
Size: 3284 Color: 298
Size: 648 Color: 103

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15962 Color: 535
Size: 2726 Color: 267
Size: 544 Color: 78

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16388 Color: 550
Size: 2484 Color: 258
Size: 360 Color: 32

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16520 Color: 556
Size: 2264 Color: 246
Size: 448 Color: 54

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16840 Color: 570
Size: 2008 Color: 231
Size: 384 Color: 41

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 442
Size: 6124 Color: 363
Size: 1216 Color: 171

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14469 Color: 485
Size: 3971 Color: 320
Size: 792 Color: 128

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 17072 Color: 586
Size: 1808 Color: 217
Size: 352 Color: 27

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 9622 Color: 409
Size: 8362 Color: 402
Size: 1248 Color: 173

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15778 Color: 530
Size: 2882 Color: 272
Size: 572 Color: 82

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14010 Color: 472
Size: 4354 Color: 332
Size: 868 Color: 140

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14798 Color: 494
Size: 3906 Color: 317
Size: 528 Color: 73

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16884 Color: 573
Size: 1964 Color: 226
Size: 384 Color: 38

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 552
Size: 2352 Color: 251
Size: 448 Color: 56

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 439
Size: 7128 Color: 382
Size: 464 Color: 59

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 9618 Color: 406
Size: 8014 Color: 397
Size: 1600 Color: 199

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14832 Color: 495
Size: 3696 Color: 311
Size: 704 Color: 111

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 10857 Color: 428
Size: 6981 Color: 374
Size: 1394 Color: 181

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 15676 Color: 524
Size: 2964 Color: 281
Size: 592 Color: 89

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 15698 Color: 525
Size: 2946 Color: 278
Size: 588 Color: 88

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16436 Color: 553
Size: 2332 Color: 249
Size: 464 Color: 60

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 10807 Color: 424
Size: 7021 Color: 378
Size: 1404 Color: 185

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 484
Size: 4020 Color: 322
Size: 800 Color: 129

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13469 Color: 463
Size: 4803 Color: 342
Size: 960 Color: 151

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 475
Size: 4266 Color: 329
Size: 852 Color: 137

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16328 Color: 549
Size: 2744 Color: 268
Size: 160 Color: 5

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11152 Color: 434
Size: 6736 Color: 369
Size: 1344 Color: 177

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 447
Size: 5818 Color: 359
Size: 1160 Color: 166

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17014 Color: 581
Size: 1850 Color: 219
Size: 368 Color: 33

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16304 Color: 548
Size: 2448 Color: 254
Size: 480 Color: 63

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12368 Color: 448
Size: 5744 Color: 358
Size: 1120 Color: 163

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 16556 Color: 558
Size: 2236 Color: 244
Size: 440 Color: 53

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17214 Color: 593
Size: 1682 Color: 209
Size: 336 Color: 23

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 15336 Color: 510
Size: 3256 Color: 295
Size: 640 Color: 99

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 15442 Color: 516
Size: 3162 Color: 289
Size: 628 Color: 96

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 438
Size: 7022 Color: 379
Size: 656 Color: 105

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 16902 Color: 574
Size: 1942 Color: 225
Size: 388 Color: 42

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 16294 Color: 547
Size: 2450 Color: 255
Size: 488 Color: 64

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12868 Color: 456
Size: 5308 Color: 350
Size: 1056 Color: 155

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14857 Color: 497
Size: 3647 Color: 308
Size: 728 Color: 116

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17192 Color: 592
Size: 1704 Color: 211
Size: 336 Color: 21

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 481
Size: 4580 Color: 336
Size: 368 Color: 35

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 16270 Color: 545
Size: 2470 Color: 257
Size: 492 Color: 66

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 16780 Color: 567
Size: 2196 Color: 243
Size: 256 Color: 9

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12805 Color: 453
Size: 5357 Color: 353
Size: 1070 Color: 159

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17066 Color: 585
Size: 1806 Color: 216
Size: 360 Color: 31

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 437
Size: 6424 Color: 366
Size: 1280 Color: 175

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 12124 Color: 446
Size: 5924 Color: 360
Size: 1184 Color: 167

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 468
Size: 4536 Color: 335
Size: 896 Color: 144

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14475 Color: 487
Size: 3965 Color: 319
Size: 792 Color: 127

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 11012 Color: 432
Size: 7036 Color: 380
Size: 1184 Color: 168

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 16534 Color: 557
Size: 2250 Color: 245
Size: 448 Color: 55

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 600
Size: 1608 Color: 202
Size: 320 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 15732 Color: 528
Size: 2924 Color: 276
Size: 576 Color: 84

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 10809 Color: 425
Size: 7021 Color: 377
Size: 1402 Color: 183

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 16910 Color: 575
Size: 1938 Color: 224
Size: 384 Color: 39

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 14908 Color: 499
Size: 3604 Color: 305
Size: 720 Color: 114

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 10865 Color: 429
Size: 7599 Color: 388
Size: 768 Color: 124

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 16812 Color: 568
Size: 2020 Color: 233
Size: 400 Color: 48

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 16036 Color: 536
Size: 2724 Color: 266
Size: 472 Color: 62

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 11229 Color: 435
Size: 6671 Color: 368
Size: 1332 Color: 176

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 9628 Color: 411
Size: 8044 Color: 399
Size: 1560 Color: 193

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13934 Color: 470
Size: 4706 Color: 340
Size: 592 Color: 90

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 15665 Color: 523
Size: 2973 Color: 282
Size: 594 Color: 92

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 16144 Color: 542
Size: 2576 Color: 260
Size: 512 Color: 68

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 16502 Color: 555
Size: 2278 Color: 247
Size: 452 Color: 57

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 17032 Color: 583
Size: 1848 Color: 218
Size: 352 Color: 29

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 16868 Color: 572
Size: 1972 Color: 227
Size: 392 Color: 43

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 14918 Color: 500
Size: 3598 Color: 304
Size: 716 Color: 113

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 423
Size: 7120 Color: 381
Size: 1408 Color: 186

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 14884 Color: 498
Size: 3628 Color: 307
Size: 720 Color: 115

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13086 Color: 458
Size: 5122 Color: 347
Size: 1024 Color: 154

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 16100 Color: 540
Size: 2612 Color: 262
Size: 520 Color: 71

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 14998 Color: 501
Size: 3530 Color: 303
Size: 704 Color: 112

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13458 Color: 461
Size: 4814 Color: 344
Size: 960 Color: 148

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14472 Color: 486
Size: 3976 Color: 321
Size: 784 Color: 126

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 419
Size: 7512 Color: 386
Size: 1488 Color: 189

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 15736 Color: 529
Size: 2920 Color: 275
Size: 576 Color: 83

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 15784 Color: 531
Size: 2888 Color: 273
Size: 560 Color: 80

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 479
Size: 4156 Color: 326
Size: 824 Color: 133

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 566
Size: 2056 Color: 235
Size: 400 Color: 45

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 16734 Color: 563
Size: 2082 Color: 236
Size: 416 Color: 49

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 9621 Color: 408
Size: 8011 Color: 394
Size: 1600 Color: 198

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 488
Size: 3912 Color: 318
Size: 752 Color: 120

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 16944 Color: 577
Size: 1936 Color: 223
Size: 352 Color: 30

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 492
Size: 3768 Color: 313
Size: 736 Color: 118

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 11138 Color: 433
Size: 6746 Color: 370
Size: 1348 Color: 178

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 17256 Color: 597
Size: 1656 Color: 206
Size: 320 Color: 16

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 16482 Color: 554
Size: 2294 Color: 248
Size: 456 Color: 58

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 483
Size: 4044 Color: 323
Size: 808 Color: 130

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 15651 Color: 521
Size: 2985 Color: 284
Size: 596 Color: 93

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 17124 Color: 589
Size: 1764 Color: 214
Size: 344 Color: 25

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 16964 Color: 578
Size: 1972 Color: 228
Size: 296 Color: 12

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 457
Size: 5208 Color: 349
Size: 1024 Color: 153

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 16818 Color: 569
Size: 2014 Color: 232
Size: 400 Color: 46

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 455
Size: 5332 Color: 351
Size: 1064 Color: 157

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 15364 Color: 513
Size: 3228 Color: 292
Size: 640 Color: 100

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 15294 Color: 506
Size: 3282 Color: 297
Size: 656 Color: 106

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 16912 Color: 576
Size: 1936 Color: 222
Size: 384 Color: 37

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 17220 Color: 594
Size: 1692 Color: 210
Size: 320 Color: 17

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 15600 Color: 520
Size: 3056 Color: 285
Size: 576 Color: 85

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 15802 Color: 532
Size: 2862 Color: 270
Size: 568 Color: 81

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 17050 Color: 584
Size: 2046 Color: 234
Size: 136 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 464
Size: 5192 Color: 348
Size: 544 Color: 76

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14348 Color: 482
Size: 4076 Color: 324
Size: 808 Color: 131

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 471
Size: 4360 Color: 333
Size: 864 Color: 138

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 13463 Color: 462
Size: 4809 Color: 343
Size: 960 Color: 149

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 16752 Color: 565
Size: 2096 Color: 239
Size: 384 Color: 40

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 8352 Color: 401
Size: 7232 Color: 383
Size: 3648 Color: 309

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 16664 Color: 559
Size: 2408 Color: 253
Size: 160 Color: 6

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 11760 Color: 440
Size: 6928 Color: 373
Size: 544 Color: 79

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 15173 Color: 503
Size: 3383 Color: 301
Size: 676 Color: 109

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14742 Color: 493
Size: 3742 Color: 312
Size: 748 Color: 119

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 12813 Color: 454
Size: 5351 Color: 352
Size: 1068 Color: 158

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 10841 Color: 427
Size: 6993 Color: 375
Size: 1398 Color: 182

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 17160 Color: 590
Size: 1736 Color: 213
Size: 336 Color: 24

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 449
Size: 5544 Color: 356
Size: 1104 Color: 162

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 416
Size: 7800 Color: 390
Size: 1552 Color: 192

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 452
Size: 5364 Color: 354
Size: 1064 Color: 156

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 489
Size: 3888 Color: 316
Size: 768 Color: 123

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 10972 Color: 430
Size: 6884 Color: 372
Size: 1376 Color: 180

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 10996 Color: 431
Size: 6868 Color: 371
Size: 1368 Color: 179

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 490
Size: 3830 Color: 315
Size: 764 Color: 122

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 16098 Color: 539
Size: 2614 Color: 263
Size: 520 Color: 70

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 16684 Color: 560
Size: 2124 Color: 242
Size: 424 Color: 52

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 414
Size: 7992 Color: 392
Size: 1584 Color: 195

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 450
Size: 5552 Color: 357
Size: 1088 Color: 161

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 12027 Color: 444
Size: 6005 Color: 361
Size: 1200 Color: 169

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 16120 Color: 541
Size: 2600 Color: 261
Size: 512 Color: 69

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 13456 Color: 460
Size: 4816 Color: 345
Size: 960 Color: 150

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 10817 Color: 426
Size: 7013 Color: 376
Size: 1402 Color: 184

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 16702 Color: 562
Size: 2110 Color: 240
Size: 420 Color: 51

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 445
Size: 6344 Color: 365
Size: 768 Color: 125

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 10392 Color: 420
Size: 7368 Color: 385
Size: 1472 Color: 188

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 15354 Color: 511
Size: 3234 Color: 293
Size: 644 Color: 102

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 16068 Color: 538
Size: 3060 Color: 286
Size: 104 Color: 2

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 17258 Color: 598
Size: 1646 Color: 205
Size: 328 Color: 19

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 418
Size: 7568 Color: 387
Size: 1504 Color: 190

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 16232 Color: 544
Size: 2504 Color: 259
Size: 496 Color: 67

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 15700 Color: 526
Size: 2948 Color: 279
Size: 584 Color: 87

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 465
Size: 4700 Color: 339
Size: 936 Color: 147

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 467
Size: 4592 Color: 337
Size: 896 Color: 145

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 13628 Color: 466
Size: 4676 Color: 338
Size: 928 Color: 146

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 15437 Color: 515
Size: 3163 Color: 290
Size: 632 Color: 98

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 16424 Color: 551
Size: 2344 Color: 250
Size: 464 Color: 61

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 12015 Color: 443
Size: 6015 Color: 362
Size: 1202 Color: 170

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 15362 Color: 512
Size: 3606 Color: 306
Size: 264 Color: 10

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 14170 Color: 476
Size: 4718 Color: 341
Size: 344 Color: 26

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 9692 Color: 415
Size: 7956 Color: 391
Size: 1584 Color: 196

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 10462 Color: 421
Size: 8246 Color: 400
Size: 524 Color: 72

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 14011 Color: 473
Size: 4351 Color: 331
Size: 870 Color: 142

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 14228 Color: 478
Size: 4172 Color: 327
Size: 832 Color: 135

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 17232 Color: 595
Size: 1680 Color: 208
Size: 320 Color: 15

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 15332 Color: 509
Size: 3252 Color: 294
Size: 648 Color: 104

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 15167 Color: 502
Size: 3389 Color: 302
Size: 676 Color: 110

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 10542 Color: 422
Size: 7242 Color: 384
Size: 1448 Color: 187

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 15272 Color: 505
Size: 3304 Color: 299
Size: 656 Color: 107

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 15312 Color: 508
Size: 3280 Color: 296
Size: 640 Color: 101

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 9644 Color: 412
Size: 7996 Color: 393
Size: 1592 Color: 197

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 17000 Color: 580
Size: 1864 Color: 220
Size: 368 Color: 36

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 15912 Color: 534
Size: 2776 Color: 269
Size: 544 Color: 77

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 17188 Color: 591
Size: 1708 Color: 212
Size: 336 Color: 22

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 17292 Color: 599
Size: 1620 Color: 203
Size: 320 Color: 14

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 15431 Color: 514
Size: 3169 Color: 291
Size: 632 Color: 97

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 17102 Color: 588
Size: 2090 Color: 238
Size: 40 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 15458 Color: 518
Size: 3146 Color: 288
Size: 628 Color: 95

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 12698 Color: 451
Size: 5446 Color: 355
Size: 1088 Color: 160

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 491
Size: 3816 Color: 314
Size: 752 Color: 121

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 9648 Color: 413
Size: 8016 Color: 398
Size: 1568 Color: 194

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 15721 Color: 527
Size: 2927 Color: 277
Size: 584 Color: 86

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 477
Size: 4186 Color: 328
Size: 836 Color: 136

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 11882 Color: 441
Size: 6126 Color: 364
Size: 1224 Color: 172

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 17028 Color: 582
Size: 1988 Color: 229
Size: 216 Color: 8

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 14849 Color: 496
Size: 3653 Color: 310
Size: 730 Color: 117

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 15482 Color: 519
Size: 3126 Color: 287
Size: 624 Color: 94

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 9617 Color: 405
Size: 8013 Color: 396
Size: 1602 Color: 201

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 14019 Color: 474
Size: 4345 Color: 330
Size: 868 Color: 141

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 9620 Color: 407
Size: 8012 Color: 395
Size: 1600 Color: 200

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 16166 Color: 543
Size: 2954 Color: 280
Size: 112 Color: 3

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 16696 Color: 561
Size: 2120 Color: 241
Size: 416 Color: 50

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 15657 Color: 522
Size: 2981 Color: 283
Size: 594 Color: 91

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 16046 Color: 537
Size: 2658 Color: 264
Size: 528 Color: 74

Total size: 3807936
Total free space: 0

