Capicity Bin: 1000001
Lower Bound: 920

Bins used: 921
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 740132 Color: 0
Size: 133679 Color: 1
Size: 126190 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 686349 Color: 1
Size: 179051 Color: 0
Size: 134601 Color: 0

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 668102 Color: 0
Size: 331899 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 612699 Color: 0
Size: 198347 Color: 1
Size: 188955 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 754239 Color: 0
Size: 143500 Color: 0
Size: 102262 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 786758 Color: 1
Size: 108074 Color: 0
Size: 105169 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 500141 Color: 0
Size: 371204 Color: 1
Size: 128656 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 501771 Color: 0
Size: 323965 Color: 1
Size: 174265 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 465563 Color: 1
Size: 359983 Color: 0
Size: 174455 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 484546 Color: 0
Size: 387438 Color: 0
Size: 128017 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 519746 Color: 1
Size: 347311 Color: 1
Size: 132944 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 693528 Color: 1
Size: 177164 Color: 1
Size: 129309 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 549938 Color: 0
Size: 271889 Color: 1
Size: 178174 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 450111 Color: 1
Size: 414837 Color: 0
Size: 135053 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 483515 Color: 1
Size: 383272 Color: 0
Size: 133214 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 687051 Color: 1
Size: 192926 Color: 0
Size: 120024 Color: 0

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 533519 Color: 1
Size: 466482 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 651468 Color: 1
Size: 175722 Color: 0
Size: 172811 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 687298 Color: 1
Size: 160236 Color: 0
Size: 152467 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 697131 Color: 1
Size: 159197 Color: 0
Size: 143673 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 522722 Color: 1
Size: 328134 Color: 0
Size: 149144 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 776692 Color: 0
Size: 122585 Color: 0
Size: 100723 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 519581 Color: 1
Size: 377783 Color: 0
Size: 102636 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 770293 Color: 1
Size: 126927 Color: 0
Size: 102780 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 716954 Color: 0
Size: 168188 Color: 1
Size: 114858 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 519066 Color: 1
Size: 321823 Color: 1
Size: 159111 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 619529 Color: 0
Size: 194485 Color: 1
Size: 185986 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 746765 Color: 0
Size: 144084 Color: 1
Size: 109151 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 555529 Color: 1
Size: 269366 Color: 0
Size: 175105 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 450263 Color: 1
Size: 375474 Color: 0
Size: 174263 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 666763 Color: 0
Size: 190629 Color: 1
Size: 142608 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 729430 Color: 0
Size: 163490 Color: 1
Size: 107080 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 523915 Color: 1
Size: 281108 Color: 0
Size: 194977 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 627943 Color: 1
Size: 230626 Color: 1
Size: 141430 Color: 0

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 657952 Color: 1
Size: 191613 Color: 0
Size: 150434 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 525611 Color: 1
Size: 320504 Color: 0
Size: 153884 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 785878 Color: 1
Size: 108683 Color: 1
Size: 105438 Color: 0

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 356821 Color: 1
Size: 322321 Color: 1
Size: 320857 Color: 0

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 602153 Color: 1
Size: 397846 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 690665 Color: 1
Size: 166725 Color: 1
Size: 142609 Color: 0

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 375723 Color: 0
Size: 348341 Color: 1
Size: 275934 Color: 1

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 751283 Color: 0
Size: 129621 Color: 1
Size: 119094 Color: 1

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 693350 Color: 1
Size: 178065 Color: 1
Size: 128583 Color: 0

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 729388 Color: 0
Size: 168368 Color: 1
Size: 102242 Color: 0

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 652886 Color: 1
Size: 183514 Color: 0
Size: 163597 Color: 0

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 518336 Color: 1
Size: 281623 Color: 0
Size: 200038 Color: 0

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 649319 Color: 1
Size: 201683 Color: 1
Size: 148995 Color: 0

Bin 48: 4 of cap free
Amount of items: 3
Items: 
Size: 550119 Color: 0
Size: 327346 Color: 0
Size: 122532 Color: 1

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 752278 Color: 0
Size: 141989 Color: 1
Size: 105730 Color: 1

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 558180 Color: 1
Size: 331428 Color: 0
Size: 110389 Color: 1

Bin 51: 5 of cap free
Amount of items: 3
Items: 
Size: 354259 Color: 0
Size: 342778 Color: 1
Size: 302959 Color: 1

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 729202 Color: 0
Size: 170103 Color: 1
Size: 100691 Color: 1

Bin 53: 5 of cap free
Amount of items: 2
Items: 
Size: 552148 Color: 0
Size: 447848 Color: 1

Bin 54: 6 of cap free
Amount of items: 3
Items: 
Size: 522741 Color: 1
Size: 339184 Color: 0
Size: 138070 Color: 0

Bin 55: 6 of cap free
Amount of items: 3
Items: 
Size: 754370 Color: 0
Size: 126324 Color: 1
Size: 119301 Color: 1

Bin 56: 7 of cap free
Amount of items: 3
Items: 
Size: 402403 Color: 0
Size: 348316 Color: 1
Size: 249275 Color: 0

Bin 57: 7 of cap free
Amount of items: 3
Items: 
Size: 518725 Color: 1
Size: 346292 Color: 1
Size: 134977 Color: 0

Bin 58: 7 of cap free
Amount of items: 3
Items: 
Size: 521299 Color: 1
Size: 291315 Color: 0
Size: 187380 Color: 0

Bin 59: 7 of cap free
Amount of items: 2
Items: 
Size: 763312 Color: 0
Size: 236682 Color: 1

Bin 60: 8 of cap free
Amount of items: 3
Items: 
Size: 656618 Color: 1
Size: 178216 Color: 0
Size: 165159 Color: 0

Bin 61: 8 of cap free
Amount of items: 3
Items: 
Size: 689184 Color: 1
Size: 159794 Color: 1
Size: 151015 Color: 0

Bin 62: 9 of cap free
Amount of items: 2
Items: 
Size: 657405 Color: 0
Size: 342587 Color: 1

Bin 63: 9 of cap free
Amount of items: 3
Items: 
Size: 635495 Color: 0
Size: 193192 Color: 1
Size: 171305 Color: 1

Bin 64: 10 of cap free
Amount of items: 2
Items: 
Size: 748558 Color: 1
Size: 251433 Color: 0

Bin 65: 11 of cap free
Amount of items: 3
Items: 
Size: 786753 Color: 1
Size: 108291 Color: 1
Size: 104946 Color: 0

Bin 66: 11 of cap free
Amount of items: 3
Items: 
Size: 694452 Color: 1
Size: 161792 Color: 0
Size: 143746 Color: 0

Bin 67: 11 of cap free
Amount of items: 2
Items: 
Size: 639266 Color: 0
Size: 360724 Color: 1

Bin 68: 12 of cap free
Amount of items: 3
Items: 
Size: 736632 Color: 0
Size: 152863 Color: 0
Size: 110494 Color: 1

Bin 69: 12 of cap free
Amount of items: 3
Items: 
Size: 700911 Color: 0
Size: 183744 Color: 1
Size: 115334 Color: 1

Bin 70: 12 of cap free
Amount of items: 3
Items: 
Size: 556077 Color: 0
Size: 320761 Color: 1
Size: 123151 Color: 1

Bin 71: 12 of cap free
Amount of items: 2
Items: 
Size: 649298 Color: 1
Size: 350691 Color: 0

Bin 72: 12 of cap free
Amount of items: 2
Items: 
Size: 768630 Color: 1
Size: 231359 Color: 0

Bin 73: 13 of cap free
Amount of items: 3
Items: 
Size: 377808 Color: 0
Size: 348490 Color: 1
Size: 273690 Color: 1

Bin 74: 13 of cap free
Amount of items: 3
Items: 
Size: 731379 Color: 0
Size: 161873 Color: 1
Size: 106736 Color: 1

Bin 75: 14 of cap free
Amount of items: 3
Items: 
Size: 686234 Color: 1
Size: 170319 Color: 1
Size: 143434 Color: 0

Bin 76: 14 of cap free
Amount of items: 2
Items: 
Size: 619709 Color: 0
Size: 380278 Color: 1

Bin 77: 14 of cap free
Amount of items: 3
Items: 
Size: 652352 Color: 1
Size: 185114 Color: 1
Size: 162521 Color: 0

Bin 78: 14 of cap free
Amount of items: 2
Items: 
Size: 730685 Color: 1
Size: 269302 Color: 0

Bin 79: 15 of cap free
Amount of items: 3
Items: 
Size: 462900 Color: 1
Size: 364341 Color: 1
Size: 172745 Color: 0

Bin 80: 16 of cap free
Amount of items: 3
Items: 
Size: 649812 Color: 1
Size: 177002 Color: 1
Size: 173171 Color: 0

Bin 81: 16 of cap free
Amount of items: 3
Items: 
Size: 451947 Color: 1
Size: 384326 Color: 0
Size: 163712 Color: 1

Bin 82: 18 of cap free
Amount of items: 2
Items: 
Size: 765638 Color: 1
Size: 234345 Color: 0

Bin 83: 18 of cap free
Amount of items: 3
Items: 
Size: 486510 Color: 0
Size: 322460 Color: 0
Size: 191013 Color: 1

Bin 84: 18 of cap free
Amount of items: 3
Items: 
Size: 455135 Color: 1
Size: 402177 Color: 0
Size: 142671 Color: 1

Bin 85: 18 of cap free
Amount of items: 2
Items: 
Size: 655682 Color: 1
Size: 344301 Color: 0

Bin 86: 18 of cap free
Amount of items: 3
Items: 
Size: 736890 Color: 0
Size: 141572 Color: 0
Size: 121521 Color: 1

Bin 87: 19 of cap free
Amount of items: 2
Items: 
Size: 592637 Color: 1
Size: 407345 Color: 0

Bin 88: 19 of cap free
Amount of items: 3
Items: 
Size: 684312 Color: 0
Size: 163044 Color: 1
Size: 152626 Color: 1

Bin 89: 21 of cap free
Amount of items: 3
Items: 
Size: 699741 Color: 0
Size: 169030 Color: 0
Size: 131209 Color: 1

Bin 90: 21 of cap free
Amount of items: 2
Items: 
Size: 584987 Color: 0
Size: 414993 Color: 1

Bin 91: 21 of cap free
Amount of items: 3
Items: 
Size: 709528 Color: 1
Size: 164006 Color: 0
Size: 126446 Color: 0

Bin 92: 21 of cap free
Amount of items: 2
Items: 
Size: 527184 Color: 0
Size: 472796 Color: 1

Bin 93: 21 of cap free
Amount of items: 3
Items: 
Size: 754709 Color: 0
Size: 123235 Color: 1
Size: 122036 Color: 0

Bin 94: 21 of cap free
Amount of items: 2
Items: 
Size: 513542 Color: 0
Size: 486438 Color: 1

Bin 95: 22 of cap free
Amount of items: 3
Items: 
Size: 347769 Color: 1
Size: 326620 Color: 0
Size: 325590 Color: 1

Bin 96: 22 of cap free
Amount of items: 2
Items: 
Size: 675719 Color: 0
Size: 324260 Color: 1

Bin 97: 22 of cap free
Amount of items: 2
Items: 
Size: 762911 Color: 0
Size: 237068 Color: 1

Bin 98: 23 of cap free
Amount of items: 2
Items: 
Size: 678653 Color: 0
Size: 321325 Color: 1

Bin 99: 23 of cap free
Amount of items: 2
Items: 
Size: 785794 Color: 1
Size: 214184 Color: 0

Bin 100: 23 of cap free
Amount of items: 2
Items: 
Size: 562553 Color: 1
Size: 437425 Color: 0

Bin 101: 24 of cap free
Amount of items: 2
Items: 
Size: 798260 Color: 0
Size: 201717 Color: 1

Bin 102: 25 of cap free
Amount of items: 2
Items: 
Size: 764177 Color: 0
Size: 235799 Color: 1

Bin 103: 25 of cap free
Amount of items: 3
Items: 
Size: 359843 Color: 0
Size: 321313 Color: 0
Size: 318820 Color: 1

Bin 104: 25 of cap free
Amount of items: 2
Items: 
Size: 586015 Color: 0
Size: 413961 Color: 1

Bin 105: 25 of cap free
Amount of items: 2
Items: 
Size: 601956 Color: 0
Size: 398020 Color: 1

Bin 106: 25 of cap free
Amount of items: 3
Items: 
Size: 620052 Color: 0
Size: 196039 Color: 1
Size: 183885 Color: 1

Bin 107: 25 of cap free
Amount of items: 2
Items: 
Size: 626618 Color: 0
Size: 373358 Color: 1

Bin 108: 26 of cap free
Amount of items: 3
Items: 
Size: 553073 Color: 1
Size: 323077 Color: 0
Size: 123825 Color: 0

Bin 109: 26 of cap free
Amount of items: 3
Items: 
Size: 685724 Color: 1
Size: 162010 Color: 1
Size: 152241 Color: 0

Bin 110: 26 of cap free
Amount of items: 2
Items: 
Size: 642481 Color: 0
Size: 357494 Color: 1

Bin 111: 27 of cap free
Amount of items: 2
Items: 
Size: 513816 Color: 1
Size: 486158 Color: 0

Bin 112: 28 of cap free
Amount of items: 3
Items: 
Size: 556139 Color: 1
Size: 272208 Color: 1
Size: 171626 Color: 0

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 527656 Color: 0
Size: 472315 Color: 1

Bin 114: 30 of cap free
Amount of items: 3
Items: 
Size: 660788 Color: 1
Size: 174989 Color: 1
Size: 164194 Color: 0

Bin 115: 30 of cap free
Amount of items: 2
Items: 
Size: 750740 Color: 0
Size: 249231 Color: 1

Bin 116: 30 of cap free
Amount of items: 3
Items: 
Size: 554222 Color: 1
Size: 293440 Color: 0
Size: 152309 Color: 1

Bin 117: 30 of cap free
Amount of items: 2
Items: 
Size: 672106 Color: 1
Size: 327865 Color: 0

Bin 118: 31 of cap free
Amount of items: 3
Items: 
Size: 755174 Color: 0
Size: 137760 Color: 0
Size: 107036 Color: 1

Bin 119: 31 of cap free
Amount of items: 3
Items: 
Size: 753658 Color: 0
Size: 127494 Color: 0
Size: 118818 Color: 1

Bin 120: 31 of cap free
Amount of items: 2
Items: 
Size: 634679 Color: 0
Size: 365291 Color: 1

Bin 121: 31 of cap free
Amount of items: 2
Items: 
Size: 732381 Color: 1
Size: 267589 Color: 0

Bin 122: 32 of cap free
Amount of items: 2
Items: 
Size: 678394 Color: 0
Size: 321575 Color: 1

Bin 123: 33 of cap free
Amount of items: 3
Items: 
Size: 735588 Color: 0
Size: 137354 Color: 0
Size: 127026 Color: 1

Bin 124: 34 of cap free
Amount of items: 2
Items: 
Size: 557859 Color: 0
Size: 442108 Color: 1

Bin 125: 34 of cap free
Amount of items: 2
Items: 
Size: 709468 Color: 0
Size: 290499 Color: 1

Bin 126: 34 of cap free
Amount of items: 2
Items: 
Size: 764224 Color: 0
Size: 235743 Color: 1

Bin 127: 34 of cap free
Amount of items: 2
Items: 
Size: 562688 Color: 0
Size: 437279 Color: 1

Bin 128: 35 of cap free
Amount of items: 2
Items: 
Size: 708160 Color: 1
Size: 291806 Color: 0

Bin 129: 35 of cap free
Amount of items: 2
Items: 
Size: 530602 Color: 0
Size: 469364 Color: 1

Bin 130: 36 of cap free
Amount of items: 3
Items: 
Size: 736023 Color: 0
Size: 152904 Color: 0
Size: 111038 Color: 1

Bin 131: 36 of cap free
Amount of items: 2
Items: 
Size: 569402 Color: 0
Size: 430563 Color: 1

Bin 132: 37 of cap free
Amount of items: 3
Items: 
Size: 668517 Color: 0
Size: 174656 Color: 1
Size: 156791 Color: 0

Bin 133: 38 of cap free
Amount of items: 3
Items: 
Size: 736228 Color: 0
Size: 157586 Color: 1
Size: 106149 Color: 0

Bin 134: 38 of cap free
Amount of items: 3
Items: 
Size: 781005 Color: 1
Size: 110800 Color: 0
Size: 108158 Color: 0

Bin 135: 38 of cap free
Amount of items: 2
Items: 
Size: 771465 Color: 0
Size: 228498 Color: 1

Bin 136: 40 of cap free
Amount of items: 3
Items: 
Size: 732189 Color: 1
Size: 133927 Color: 1
Size: 133845 Color: 0

Bin 137: 40 of cap free
Amount of items: 2
Items: 
Size: 512463 Color: 1
Size: 487498 Color: 0

Bin 138: 42 of cap free
Amount of items: 2
Items: 
Size: 693424 Color: 1
Size: 306535 Color: 0

Bin 139: 42 of cap free
Amount of items: 3
Items: 
Size: 516147 Color: 1
Size: 311602 Color: 1
Size: 172210 Color: 0

Bin 140: 43 of cap free
Amount of items: 3
Items: 
Size: 706108 Color: 0
Size: 148257 Color: 1
Size: 145593 Color: 0

Bin 141: 44 of cap free
Amount of items: 2
Items: 
Size: 675041 Color: 0
Size: 324916 Color: 1

Bin 142: 44 of cap free
Amount of items: 2
Items: 
Size: 571892 Color: 0
Size: 428065 Color: 1

Bin 143: 44 of cap free
Amount of items: 3
Items: 
Size: 529746 Color: 1
Size: 291027 Color: 0
Size: 179184 Color: 1

Bin 144: 45 of cap free
Amount of items: 3
Items: 
Size: 518377 Color: 1
Size: 283088 Color: 1
Size: 198491 Color: 0

Bin 145: 45 of cap free
Amount of items: 2
Items: 
Size: 574778 Color: 0
Size: 425178 Color: 1

Bin 146: 46 of cap free
Amount of items: 2
Items: 
Size: 588768 Color: 1
Size: 411187 Color: 0

Bin 147: 47 of cap free
Amount of items: 2
Items: 
Size: 564332 Color: 1
Size: 435622 Color: 0

Bin 148: 48 of cap free
Amount of items: 3
Items: 
Size: 764542 Color: 1
Size: 132226 Color: 1
Size: 103185 Color: 0

Bin 149: 48 of cap free
Amount of items: 2
Items: 
Size: 536569 Color: 1
Size: 463384 Color: 0

Bin 150: 48 of cap free
Amount of items: 2
Items: 
Size: 674780 Color: 1
Size: 325173 Color: 0

Bin 151: 48 of cap free
Amount of items: 3
Items: 
Size: 455717 Color: 1
Size: 401429 Color: 0
Size: 142807 Color: 0

Bin 152: 49 of cap free
Amount of items: 2
Items: 
Size: 776161 Color: 1
Size: 223791 Color: 0

Bin 153: 49 of cap free
Amount of items: 2
Items: 
Size: 718750 Color: 0
Size: 281202 Color: 1

Bin 154: 50 of cap free
Amount of items: 2
Items: 
Size: 645643 Color: 1
Size: 354308 Color: 0

Bin 155: 51 of cap free
Amount of items: 2
Items: 
Size: 616487 Color: 0
Size: 383463 Color: 1

Bin 156: 52 of cap free
Amount of items: 3
Items: 
Size: 546169 Color: 1
Size: 272766 Color: 1
Size: 181014 Color: 0

Bin 157: 54 of cap free
Amount of items: 2
Items: 
Size: 598217 Color: 1
Size: 401730 Color: 0

Bin 158: 54 of cap free
Amount of items: 2
Items: 
Size: 622897 Color: 0
Size: 377050 Color: 1

Bin 159: 54 of cap free
Amount of items: 3
Items: 
Size: 453443 Color: 1
Size: 366991 Color: 0
Size: 179513 Color: 0

Bin 160: 55 of cap free
Amount of items: 2
Items: 
Size: 631793 Color: 0
Size: 368153 Color: 1

Bin 161: 56 of cap free
Amount of items: 3
Items: 
Size: 551850 Color: 0
Size: 345311 Color: 1
Size: 102784 Color: 0

Bin 162: 57 of cap free
Amount of items: 3
Items: 
Size: 648217 Color: 1
Size: 183470 Color: 1
Size: 168257 Color: 0

Bin 163: 57 of cap free
Amount of items: 2
Items: 
Size: 506594 Color: 0
Size: 493350 Color: 1

Bin 164: 58 of cap free
Amount of items: 2
Items: 
Size: 790310 Color: 0
Size: 209633 Color: 1

Bin 165: 58 of cap free
Amount of items: 2
Items: 
Size: 510509 Color: 1
Size: 489434 Color: 0

Bin 166: 58 of cap free
Amount of items: 2
Items: 
Size: 612945 Color: 0
Size: 386998 Color: 1

Bin 167: 60 of cap free
Amount of items: 2
Items: 
Size: 663075 Color: 1
Size: 336866 Color: 0

Bin 168: 60 of cap free
Amount of items: 2
Items: 
Size: 516827 Color: 0
Size: 483114 Color: 1

Bin 169: 61 of cap free
Amount of items: 3
Items: 
Size: 693136 Color: 1
Size: 153736 Color: 1
Size: 153068 Color: 0

Bin 170: 61 of cap free
Amount of items: 2
Items: 
Size: 789682 Color: 1
Size: 210258 Color: 0

Bin 171: 62 of cap free
Amount of items: 2
Items: 
Size: 748886 Color: 1
Size: 251053 Color: 0

Bin 172: 63 of cap free
Amount of items: 3
Items: 
Size: 453250 Color: 1
Size: 375418 Color: 0
Size: 171270 Color: 1

Bin 173: 65 of cap free
Amount of items: 3
Items: 
Size: 692987 Color: 1
Size: 157005 Color: 0
Size: 149944 Color: 1

Bin 174: 65 of cap free
Amount of items: 2
Items: 
Size: 550746 Color: 1
Size: 449190 Color: 0

Bin 175: 65 of cap free
Amount of items: 2
Items: 
Size: 697800 Color: 0
Size: 302136 Color: 1

Bin 176: 66 of cap free
Amount of items: 3
Items: 
Size: 785905 Color: 1
Size: 108524 Color: 0
Size: 105506 Color: 1

Bin 177: 67 of cap free
Amount of items: 3
Items: 
Size: 658441 Color: 1
Size: 177681 Color: 1
Size: 163812 Color: 0

Bin 178: 67 of cap free
Amount of items: 3
Items: 
Size: 747774 Color: 0
Size: 129505 Color: 0
Size: 122655 Color: 1

Bin 179: 67 of cap free
Amount of items: 2
Items: 
Size: 526981 Color: 1
Size: 472953 Color: 0

Bin 180: 68 of cap free
Amount of items: 2
Items: 
Size: 596859 Color: 0
Size: 403074 Color: 1

Bin 181: 70 of cap free
Amount of items: 3
Items: 
Size: 546006 Color: 1
Size: 275644 Color: 0
Size: 178281 Color: 0

Bin 182: 70 of cap free
Amount of items: 2
Items: 
Size: 505188 Color: 0
Size: 494743 Color: 1

Bin 183: 71 of cap free
Amount of items: 2
Items: 
Size: 585029 Color: 0
Size: 414901 Color: 1

Bin 184: 71 of cap free
Amount of items: 2
Items: 
Size: 632162 Color: 0
Size: 367768 Color: 1

Bin 185: 71 of cap free
Amount of items: 3
Items: 
Size: 750322 Color: 0
Size: 144444 Color: 1
Size: 105164 Color: 1

Bin 186: 73 of cap free
Amount of items: 3
Items: 
Size: 357408 Color: 1
Size: 322305 Color: 0
Size: 320215 Color: 1

Bin 187: 74 of cap free
Amount of items: 2
Items: 
Size: 622514 Color: 1
Size: 377413 Color: 0

Bin 188: 74 of cap free
Amount of items: 2
Items: 
Size: 635144 Color: 0
Size: 364783 Color: 1

Bin 189: 75 of cap free
Amount of items: 3
Items: 
Size: 645751 Color: 1
Size: 187347 Color: 0
Size: 166828 Color: 1

Bin 190: 75 of cap free
Amount of items: 2
Items: 
Size: 649093 Color: 1
Size: 350833 Color: 0

Bin 191: 75 of cap free
Amount of items: 2
Items: 
Size: 675085 Color: 1
Size: 324841 Color: 0

Bin 192: 77 of cap free
Amount of items: 3
Items: 
Size: 456320 Color: 1
Size: 374768 Color: 0
Size: 168836 Color: 1

Bin 193: 80 of cap free
Amount of items: 2
Items: 
Size: 746203 Color: 1
Size: 253718 Color: 0

Bin 194: 80 of cap free
Amount of items: 2
Items: 
Size: 713658 Color: 0
Size: 286263 Color: 1

Bin 195: 83 of cap free
Amount of items: 2
Items: 
Size: 737933 Color: 0
Size: 261985 Color: 1

Bin 196: 83 of cap free
Amount of items: 3
Items: 
Size: 672318 Color: 0
Size: 171266 Color: 1
Size: 156334 Color: 0

Bin 197: 84 of cap free
Amount of items: 2
Items: 
Size: 794605 Color: 1
Size: 205312 Color: 0

Bin 198: 85 of cap free
Amount of items: 3
Items: 
Size: 502557 Color: 1
Size: 320874 Color: 1
Size: 176485 Color: 0

Bin 199: 85 of cap free
Amount of items: 2
Items: 
Size: 773597 Color: 0
Size: 226319 Color: 1

Bin 200: 85 of cap free
Amount of items: 3
Items: 
Size: 772229 Color: 1
Size: 124690 Color: 0
Size: 102997 Color: 0

Bin 201: 85 of cap free
Amount of items: 2
Items: 
Size: 602895 Color: 1
Size: 397021 Color: 0

Bin 202: 86 of cap free
Amount of items: 2
Items: 
Size: 556482 Color: 0
Size: 443433 Color: 1

Bin 203: 86 of cap free
Amount of items: 5
Items: 
Size: 373205 Color: 0
Size: 171239 Color: 0
Size: 166442 Color: 1
Size: 156166 Color: 1
Size: 132863 Color: 1

Bin 204: 89 of cap free
Amount of items: 3
Items: 
Size: 521949 Color: 1
Size: 285165 Color: 0
Size: 192798 Color: 1

Bin 205: 89 of cap free
Amount of items: 2
Items: 
Size: 502089 Color: 1
Size: 497823 Color: 0

Bin 206: 89 of cap free
Amount of items: 2
Items: 
Size: 564156 Color: 1
Size: 435756 Color: 0

Bin 207: 90 of cap free
Amount of items: 2
Items: 
Size: 525835 Color: 0
Size: 474076 Color: 1

Bin 208: 92 of cap free
Amount of items: 3
Items: 
Size: 552180 Color: 0
Size: 272490 Color: 1
Size: 175239 Color: 1

Bin 209: 92 of cap free
Amount of items: 2
Items: 
Size: 512566 Color: 0
Size: 487343 Color: 1

Bin 210: 92 of cap free
Amount of items: 2
Items: 
Size: 665658 Color: 1
Size: 334251 Color: 0

Bin 211: 94 of cap free
Amount of items: 2
Items: 
Size: 587330 Color: 1
Size: 412577 Color: 0

Bin 212: 96 of cap free
Amount of items: 3
Items: 
Size: 649832 Color: 1
Size: 187053 Color: 0
Size: 163020 Color: 0

Bin 213: 97 of cap free
Amount of items: 2
Items: 
Size: 533017 Color: 1
Size: 466887 Color: 0

Bin 214: 97 of cap free
Amount of items: 2
Items: 
Size: 759294 Color: 0
Size: 240610 Color: 1

Bin 215: 97 of cap free
Amount of items: 2
Items: 
Size: 794085 Color: 1
Size: 205819 Color: 0

Bin 216: 100 of cap free
Amount of items: 3
Items: 
Size: 661238 Color: 0
Size: 194884 Color: 1
Size: 143779 Color: 1

Bin 217: 100 of cap free
Amount of items: 2
Items: 
Size: 780275 Color: 0
Size: 219626 Color: 1

Bin 218: 102 of cap free
Amount of items: 3
Items: 
Size: 520884 Color: 1
Size: 339075 Color: 1
Size: 139940 Color: 0

Bin 219: 103 of cap free
Amount of items: 3
Items: 
Size: 457353 Color: 1
Size: 411266 Color: 1
Size: 131279 Color: 0

Bin 220: 103 of cap free
Amount of items: 3
Items: 
Size: 750583 Color: 0
Size: 125952 Color: 1
Size: 123363 Color: 1

Bin 221: 103 of cap free
Amount of items: 2
Items: 
Size: 575955 Color: 0
Size: 423943 Color: 1

Bin 222: 105 of cap free
Amount of items: 2
Items: 
Size: 572789 Color: 0
Size: 427107 Color: 1

Bin 223: 105 of cap free
Amount of items: 2
Items: 
Size: 752318 Color: 0
Size: 247578 Color: 1

Bin 224: 105 of cap free
Amount of items: 2
Items: 
Size: 599924 Color: 0
Size: 399972 Color: 1

Bin 225: 106 of cap free
Amount of items: 3
Items: 
Size: 527834 Color: 1
Size: 322407 Color: 0
Size: 149654 Color: 1

Bin 226: 106 of cap free
Amount of items: 2
Items: 
Size: 610489 Color: 0
Size: 389406 Color: 1

Bin 227: 106 of cap free
Amount of items: 3
Items: 
Size: 551009 Color: 0
Size: 306777 Color: 1
Size: 142109 Color: 1

Bin 228: 107 of cap free
Amount of items: 2
Items: 
Size: 632275 Color: 0
Size: 367619 Color: 1

Bin 229: 107 of cap free
Amount of items: 2
Items: 
Size: 701547 Color: 0
Size: 298347 Color: 1

Bin 230: 108 of cap free
Amount of items: 2
Items: 
Size: 671108 Color: 1
Size: 328785 Color: 0

Bin 231: 109 of cap free
Amount of items: 3
Items: 
Size: 682175 Color: 0
Size: 180347 Color: 0
Size: 137370 Color: 1

Bin 232: 109 of cap free
Amount of items: 2
Items: 
Size: 617518 Color: 0
Size: 382374 Color: 1

Bin 233: 112 of cap free
Amount of items: 2
Items: 
Size: 621922 Color: 1
Size: 377967 Color: 0

Bin 234: 113 of cap free
Amount of items: 2
Items: 
Size: 760498 Color: 0
Size: 239390 Color: 1

Bin 235: 113 of cap free
Amount of items: 2
Items: 
Size: 664829 Color: 1
Size: 335059 Color: 0

Bin 236: 113 of cap free
Amount of items: 2
Items: 
Size: 766971 Color: 1
Size: 232917 Color: 0

Bin 237: 114 of cap free
Amount of items: 3
Items: 
Size: 656716 Color: 1
Size: 184038 Color: 0
Size: 159133 Color: 0

Bin 238: 114 of cap free
Amount of items: 2
Items: 
Size: 564457 Color: 1
Size: 435430 Color: 0

Bin 239: 116 of cap free
Amount of items: 2
Items: 
Size: 721819 Color: 0
Size: 278066 Color: 1

Bin 240: 116 of cap free
Amount of items: 3
Items: 
Size: 697527 Color: 0
Size: 185098 Color: 1
Size: 117260 Color: 1

Bin 241: 117 of cap free
Amount of items: 2
Items: 
Size: 763339 Color: 1
Size: 236545 Color: 0

Bin 242: 117 of cap free
Amount of items: 2
Items: 
Size: 764013 Color: 0
Size: 235871 Color: 1

Bin 243: 118 of cap free
Amount of items: 2
Items: 
Size: 531857 Color: 1
Size: 468026 Color: 0

Bin 244: 118 of cap free
Amount of items: 3
Items: 
Size: 660369 Color: 1
Size: 171442 Color: 1
Size: 168072 Color: 0

Bin 245: 119 of cap free
Amount of items: 2
Items: 
Size: 643226 Color: 0
Size: 356656 Color: 1

Bin 246: 119 of cap free
Amount of items: 3
Items: 
Size: 523417 Color: 1
Size: 311882 Color: 1
Size: 164583 Color: 0

Bin 247: 119 of cap free
Amount of items: 2
Items: 
Size: 634237 Color: 0
Size: 365645 Color: 1

Bin 248: 120 of cap free
Amount of items: 3
Items: 
Size: 768414 Color: 1
Size: 125179 Color: 1
Size: 106288 Color: 0

Bin 249: 120 of cap free
Amount of items: 2
Items: 
Size: 575434 Color: 0
Size: 424447 Color: 1

Bin 250: 121 of cap free
Amount of items: 2
Items: 
Size: 796817 Color: 0
Size: 203063 Color: 1

Bin 251: 122 of cap free
Amount of items: 2
Items: 
Size: 709515 Color: 0
Size: 290364 Color: 1

Bin 252: 122 of cap free
Amount of items: 2
Items: 
Size: 672958 Color: 0
Size: 326921 Color: 1

Bin 253: 122 of cap free
Amount of items: 2
Items: 
Size: 675646 Color: 1
Size: 324233 Color: 0

Bin 254: 123 of cap free
Amount of items: 2
Items: 
Size: 592348 Color: 0
Size: 407530 Color: 1

Bin 255: 125 of cap free
Amount of items: 2
Items: 
Size: 520616 Color: 0
Size: 479260 Color: 1

Bin 256: 125 of cap free
Amount of items: 2
Items: 
Size: 519129 Color: 1
Size: 480747 Color: 0

Bin 257: 125 of cap free
Amount of items: 2
Items: 
Size: 644151 Color: 0
Size: 355725 Color: 1

Bin 258: 125 of cap free
Amount of items: 2
Items: 
Size: 655721 Color: 1
Size: 344155 Color: 0

Bin 259: 126 of cap free
Amount of items: 2
Items: 
Size: 648490 Color: 1
Size: 351385 Color: 0

Bin 260: 127 of cap free
Amount of items: 2
Items: 
Size: 760505 Color: 1
Size: 239369 Color: 0

Bin 261: 130 of cap free
Amount of items: 2
Items: 
Size: 570454 Color: 1
Size: 429417 Color: 0

Bin 262: 131 of cap free
Amount of items: 2
Items: 
Size: 635161 Color: 1
Size: 364709 Color: 0

Bin 263: 131 of cap free
Amount of items: 3
Items: 
Size: 700233 Color: 0
Size: 163634 Color: 1
Size: 136003 Color: 1

Bin 264: 132 of cap free
Amount of items: 2
Items: 
Size: 644817 Color: 1
Size: 355052 Color: 0

Bin 265: 135 of cap free
Amount of items: 2
Items: 
Size: 601554 Color: 0
Size: 398312 Color: 1

Bin 266: 135 of cap free
Amount of items: 2
Items: 
Size: 617490 Color: 1
Size: 382376 Color: 0

Bin 267: 136 of cap free
Amount of items: 2
Items: 
Size: 553310 Color: 1
Size: 446555 Color: 0

Bin 268: 138 of cap free
Amount of items: 2
Items: 
Size: 592062 Color: 1
Size: 407801 Color: 0

Bin 269: 139 of cap free
Amount of items: 2
Items: 
Size: 650209 Color: 0
Size: 349653 Color: 1

Bin 270: 139 of cap free
Amount of items: 2
Items: 
Size: 516005 Color: 0
Size: 483857 Color: 1

Bin 271: 140 of cap free
Amount of items: 2
Items: 
Size: 669700 Color: 1
Size: 330161 Color: 0

Bin 272: 141 of cap free
Amount of items: 2
Items: 
Size: 691179 Color: 0
Size: 308681 Color: 1

Bin 273: 146 of cap free
Amount of items: 3
Items: 
Size: 603300 Color: 0
Size: 203811 Color: 1
Size: 192744 Color: 1

Bin 274: 146 of cap free
Amount of items: 2
Items: 
Size: 658448 Color: 0
Size: 341407 Color: 1

Bin 275: 147 of cap free
Amount of items: 2
Items: 
Size: 722492 Color: 1
Size: 277362 Color: 0

Bin 276: 148 of cap free
Amount of items: 2
Items: 
Size: 618826 Color: 1
Size: 381027 Color: 0

Bin 277: 148 of cap free
Amount of items: 2
Items: 
Size: 589867 Color: 0
Size: 409986 Color: 1

Bin 278: 150 of cap free
Amount of items: 2
Items: 
Size: 585788 Color: 0
Size: 414063 Color: 1

Bin 279: 151 of cap free
Amount of items: 2
Items: 
Size: 798955 Color: 0
Size: 200895 Color: 1

Bin 280: 152 of cap free
Amount of items: 2
Items: 
Size: 677751 Color: 0
Size: 322098 Color: 1

Bin 281: 152 of cap free
Amount of items: 2
Items: 
Size: 711776 Color: 0
Size: 288073 Color: 1

Bin 282: 154 of cap free
Amount of items: 2
Items: 
Size: 763043 Color: 0
Size: 236804 Color: 1

Bin 283: 154 of cap free
Amount of items: 2
Items: 
Size: 792579 Color: 1
Size: 207268 Color: 0

Bin 284: 155 of cap free
Amount of items: 2
Items: 
Size: 523549 Color: 1
Size: 476297 Color: 0

Bin 285: 155 of cap free
Amount of items: 2
Items: 
Size: 715016 Color: 0
Size: 284830 Color: 1

Bin 286: 155 of cap free
Amount of items: 2
Items: 
Size: 733192 Color: 0
Size: 266654 Color: 1

Bin 287: 157 of cap free
Amount of items: 3
Items: 
Size: 451877 Color: 1
Size: 356265 Color: 0
Size: 191702 Color: 1

Bin 288: 159 of cap free
Amount of items: 2
Items: 
Size: 692699 Color: 1
Size: 307143 Color: 0

Bin 289: 160 of cap free
Amount of items: 2
Items: 
Size: 667302 Color: 1
Size: 332539 Color: 0

Bin 290: 160 of cap free
Amount of items: 2
Items: 
Size: 638079 Color: 0
Size: 361762 Color: 1

Bin 291: 160 of cap free
Amount of items: 2
Items: 
Size: 594523 Color: 0
Size: 405318 Color: 1

Bin 292: 161 of cap free
Amount of items: 2
Items: 
Size: 636357 Color: 1
Size: 363483 Color: 0

Bin 293: 161 of cap free
Amount of items: 2
Items: 
Size: 521801 Color: 0
Size: 478039 Color: 1

Bin 294: 161 of cap free
Amount of items: 2
Items: 
Size: 797159 Color: 0
Size: 202681 Color: 1

Bin 295: 162 of cap free
Amount of items: 2
Items: 
Size: 566824 Color: 0
Size: 433015 Color: 1

Bin 296: 162 of cap free
Amount of items: 3
Items: 
Size: 768940 Color: 1
Size: 119037 Color: 1
Size: 111862 Color: 0

Bin 297: 165 of cap free
Amount of items: 2
Items: 
Size: 635574 Color: 0
Size: 364262 Color: 1

Bin 298: 165 of cap free
Amount of items: 2
Items: 
Size: 572493 Color: 1
Size: 427343 Color: 0

Bin 299: 167 of cap free
Amount of items: 3
Items: 
Size: 526539 Color: 1
Size: 291165 Color: 1
Size: 182130 Color: 0

Bin 300: 167 of cap free
Amount of items: 2
Items: 
Size: 572924 Color: 1
Size: 426910 Color: 0

Bin 301: 167 of cap free
Amount of items: 3
Items: 
Size: 391002 Color: 0
Size: 313685 Color: 1
Size: 295147 Color: 0

Bin 302: 168 of cap free
Amount of items: 2
Items: 
Size: 786879 Color: 1
Size: 212954 Color: 0

Bin 303: 169 of cap free
Amount of items: 2
Items: 
Size: 599961 Color: 1
Size: 399871 Color: 0

Bin 304: 169 of cap free
Amount of items: 3
Items: 
Size: 754592 Color: 0
Size: 131653 Color: 1
Size: 113587 Color: 1

Bin 305: 169 of cap free
Amount of items: 2
Items: 
Size: 753646 Color: 0
Size: 246186 Color: 1

Bin 306: 169 of cap free
Amount of items: 2
Items: 
Size: 794164 Color: 0
Size: 205668 Color: 1

Bin 307: 173 of cap free
Amount of items: 2
Items: 
Size: 551973 Color: 0
Size: 447855 Color: 1

Bin 308: 173 of cap free
Amount of items: 2
Items: 
Size: 683134 Color: 1
Size: 316694 Color: 0

Bin 309: 173 of cap free
Amount of items: 2
Items: 
Size: 705764 Color: 0
Size: 294064 Color: 1

Bin 310: 174 of cap free
Amount of items: 2
Items: 
Size: 773183 Color: 1
Size: 226644 Color: 0

Bin 311: 174 of cap free
Amount of items: 2
Items: 
Size: 657727 Color: 1
Size: 342100 Color: 0

Bin 312: 175 of cap free
Amount of items: 2
Items: 
Size: 691765 Color: 0
Size: 308061 Color: 1

Bin 313: 178 of cap free
Amount of items: 2
Items: 
Size: 545191 Color: 1
Size: 454632 Color: 0

Bin 314: 179 of cap free
Amount of items: 3
Items: 
Size: 709972 Color: 1
Size: 154415 Color: 0
Size: 135435 Color: 0

Bin 315: 179 of cap free
Amount of items: 2
Items: 
Size: 702490 Color: 1
Size: 297332 Color: 0

Bin 316: 179 of cap free
Amount of items: 2
Items: 
Size: 737336 Color: 0
Size: 262486 Color: 1

Bin 317: 179 of cap free
Amount of items: 2
Items: 
Size: 507291 Color: 1
Size: 492531 Color: 0

Bin 318: 179 of cap free
Amount of items: 2
Items: 
Size: 513742 Color: 0
Size: 486080 Color: 1

Bin 319: 179 of cap free
Amount of items: 2
Items: 
Size: 501443 Color: 1
Size: 498379 Color: 0

Bin 320: 179 of cap free
Amount of items: 2
Items: 
Size: 615958 Color: 1
Size: 383864 Color: 0

Bin 321: 180 of cap free
Amount of items: 2
Items: 
Size: 523671 Color: 0
Size: 476150 Color: 1

Bin 322: 181 of cap free
Amount of items: 2
Items: 
Size: 598676 Color: 1
Size: 401144 Color: 0

Bin 323: 182 of cap free
Amount of items: 2
Items: 
Size: 576849 Color: 0
Size: 422970 Color: 1

Bin 324: 185 of cap free
Amount of items: 2
Items: 
Size: 620705 Color: 1
Size: 379111 Color: 0

Bin 325: 186 of cap free
Amount of items: 2
Items: 
Size: 772957 Color: 0
Size: 226858 Color: 1

Bin 326: 189 of cap free
Amount of items: 2
Items: 
Size: 640787 Color: 1
Size: 359025 Color: 0

Bin 327: 193 of cap free
Amount of items: 2
Items: 
Size: 581954 Color: 1
Size: 417854 Color: 0

Bin 328: 193 of cap free
Amount of items: 2
Items: 
Size: 721560 Color: 0
Size: 278248 Color: 1

Bin 329: 194 of cap free
Amount of items: 2
Items: 
Size: 501789 Color: 0
Size: 498018 Color: 1

Bin 330: 194 of cap free
Amount of items: 2
Items: 
Size: 674768 Color: 0
Size: 325039 Color: 1

Bin 331: 197 of cap free
Amount of items: 2
Items: 
Size: 577676 Color: 1
Size: 422128 Color: 0

Bin 332: 197 of cap free
Amount of items: 2
Items: 
Size: 605266 Color: 0
Size: 394538 Color: 1

Bin 333: 198 of cap free
Amount of items: 2
Items: 
Size: 757757 Color: 0
Size: 242046 Color: 1

Bin 334: 200 of cap free
Amount of items: 2
Items: 
Size: 652868 Color: 0
Size: 346933 Color: 1

Bin 335: 201 of cap free
Amount of items: 2
Items: 
Size: 744330 Color: 0
Size: 255470 Color: 1

Bin 336: 202 of cap free
Amount of items: 2
Items: 
Size: 504981 Color: 0
Size: 494818 Color: 1

Bin 337: 202 of cap free
Amount of items: 2
Items: 
Size: 694302 Color: 0
Size: 305497 Color: 1

Bin 338: 203 of cap free
Amount of items: 3
Items: 
Size: 552273 Color: 0
Size: 276337 Color: 1
Size: 171188 Color: 1

Bin 339: 203 of cap free
Amount of items: 2
Items: 
Size: 575657 Color: 1
Size: 424141 Color: 0

Bin 340: 204 of cap free
Amount of items: 2
Items: 
Size: 794817 Color: 1
Size: 204980 Color: 0

Bin 341: 205 of cap free
Amount of items: 2
Items: 
Size: 592650 Color: 0
Size: 407146 Color: 1

Bin 342: 209 of cap free
Amount of items: 2
Items: 
Size: 710359 Color: 1
Size: 289433 Color: 0

Bin 343: 210 of cap free
Amount of items: 2
Items: 
Size: 715512 Color: 1
Size: 284279 Color: 0

Bin 344: 211 of cap free
Amount of items: 2
Items: 
Size: 791414 Color: 1
Size: 208376 Color: 0

Bin 345: 221 of cap free
Amount of items: 2
Items: 
Size: 527360 Color: 0
Size: 472420 Color: 1

Bin 346: 223 of cap free
Amount of items: 3
Items: 
Size: 734964 Color: 1
Size: 132512 Color: 0
Size: 132302 Color: 1

Bin 347: 224 of cap free
Amount of items: 2
Items: 
Size: 507432 Color: 0
Size: 492345 Color: 1

Bin 348: 224 of cap free
Amount of items: 2
Items: 
Size: 660404 Color: 0
Size: 339373 Color: 1

Bin 349: 226 of cap free
Amount of items: 2
Items: 
Size: 688126 Color: 1
Size: 311649 Color: 0

Bin 350: 227 of cap free
Amount of items: 2
Items: 
Size: 563904 Color: 0
Size: 435870 Color: 1

Bin 351: 227 of cap free
Amount of items: 2
Items: 
Size: 610101 Color: 0
Size: 389673 Color: 1

Bin 352: 228 of cap free
Amount of items: 3
Items: 
Size: 738767 Color: 0
Size: 142922 Color: 0
Size: 118084 Color: 1

Bin 353: 228 of cap free
Amount of items: 2
Items: 
Size: 583797 Color: 0
Size: 415976 Color: 1

Bin 354: 230 of cap free
Amount of items: 2
Items: 
Size: 529566 Color: 0
Size: 470205 Color: 1

Bin 355: 231 of cap free
Amount of items: 2
Items: 
Size: 585720 Color: 0
Size: 414050 Color: 1

Bin 356: 233 of cap free
Amount of items: 2
Items: 
Size: 739828 Color: 0
Size: 259940 Color: 1

Bin 357: 233 of cap free
Amount of items: 2
Items: 
Size: 639172 Color: 1
Size: 360596 Color: 0

Bin 358: 236 of cap free
Amount of items: 2
Items: 
Size: 583425 Color: 0
Size: 416340 Color: 1

Bin 359: 239 of cap free
Amount of items: 2
Items: 
Size: 560807 Color: 1
Size: 438955 Color: 0

Bin 360: 244 of cap free
Amount of items: 2
Items: 
Size: 593420 Color: 1
Size: 406337 Color: 0

Bin 361: 246 of cap free
Amount of items: 2
Items: 
Size: 750644 Color: 1
Size: 249111 Color: 0

Bin 362: 247 of cap free
Amount of items: 2
Items: 
Size: 717574 Color: 0
Size: 282180 Color: 1

Bin 363: 247 of cap free
Amount of items: 2
Items: 
Size: 759260 Color: 1
Size: 240494 Color: 0

Bin 364: 248 of cap free
Amount of items: 2
Items: 
Size: 695092 Color: 0
Size: 304661 Color: 1

Bin 365: 249 of cap free
Amount of items: 2
Items: 
Size: 558691 Color: 0
Size: 441061 Color: 1

Bin 366: 249 of cap free
Amount of items: 2
Items: 
Size: 652264 Color: 1
Size: 347488 Color: 0

Bin 367: 249 of cap free
Amount of items: 2
Items: 
Size: 764875 Color: 0
Size: 234877 Color: 1

Bin 368: 250 of cap free
Amount of items: 2
Items: 
Size: 632261 Color: 0
Size: 367490 Color: 1

Bin 369: 254 of cap free
Amount of items: 2
Items: 
Size: 681747 Color: 0
Size: 318000 Color: 1

Bin 370: 254 of cap free
Amount of items: 2
Items: 
Size: 697490 Color: 1
Size: 302257 Color: 0

Bin 371: 254 of cap free
Amount of items: 2
Items: 
Size: 793724 Color: 1
Size: 206023 Color: 0

Bin 372: 256 of cap free
Amount of items: 2
Items: 
Size: 672487 Color: 1
Size: 327258 Color: 0

Bin 373: 257 of cap free
Amount of items: 2
Items: 
Size: 612837 Color: 0
Size: 386907 Color: 1

Bin 374: 258 of cap free
Amount of items: 2
Items: 
Size: 551400 Color: 0
Size: 448343 Color: 1

Bin 375: 260 of cap free
Amount of items: 2
Items: 
Size: 626014 Color: 1
Size: 373727 Color: 0

Bin 376: 261 of cap free
Amount of items: 2
Items: 
Size: 670305 Color: 1
Size: 329435 Color: 0

Bin 377: 263 of cap free
Amount of items: 2
Items: 
Size: 702413 Color: 1
Size: 297325 Color: 0

Bin 378: 264 of cap free
Amount of items: 2
Items: 
Size: 734475 Color: 1
Size: 265262 Color: 0

Bin 379: 266 of cap free
Amount of items: 2
Items: 
Size: 718598 Color: 0
Size: 281137 Color: 1

Bin 380: 266 of cap free
Amount of items: 2
Items: 
Size: 796280 Color: 0
Size: 203455 Color: 1

Bin 381: 266 of cap free
Amount of items: 3
Items: 
Size: 658354 Color: 1
Size: 175472 Color: 0
Size: 165909 Color: 1

Bin 382: 269 of cap free
Amount of items: 2
Items: 
Size: 743944 Color: 1
Size: 255788 Color: 0

Bin 383: 272 of cap free
Amount of items: 3
Items: 
Size: 772367 Color: 0
Size: 119224 Color: 0
Size: 108138 Color: 1

Bin 384: 272 of cap free
Amount of items: 2
Items: 
Size: 505621 Color: 1
Size: 494108 Color: 0

Bin 385: 274 of cap free
Amount of items: 2
Items: 
Size: 703978 Color: 1
Size: 295749 Color: 0

Bin 386: 274 of cap free
Amount of items: 2
Items: 
Size: 539487 Color: 1
Size: 460240 Color: 0

Bin 387: 274 of cap free
Amount of items: 2
Items: 
Size: 530235 Color: 1
Size: 469492 Color: 0

Bin 388: 274 of cap free
Amount of items: 2
Items: 
Size: 730121 Color: 1
Size: 269606 Color: 0

Bin 389: 276 of cap free
Amount of items: 2
Items: 
Size: 518943 Color: 0
Size: 480782 Color: 1

Bin 390: 279 of cap free
Amount of items: 2
Items: 
Size: 557915 Color: 1
Size: 441807 Color: 0

Bin 391: 279 of cap free
Amount of items: 2
Items: 
Size: 724721 Color: 1
Size: 275001 Color: 0

Bin 392: 283 of cap free
Amount of items: 2
Items: 
Size: 525805 Color: 1
Size: 473913 Color: 0

Bin 393: 284 of cap free
Amount of items: 2
Items: 
Size: 799645 Color: 1
Size: 200072 Color: 0

Bin 394: 287 of cap free
Amount of items: 2
Items: 
Size: 502085 Color: 0
Size: 497629 Color: 1

Bin 395: 289 of cap free
Amount of items: 2
Items: 
Size: 685411 Color: 0
Size: 314301 Color: 1

Bin 396: 290 of cap free
Amount of items: 2
Items: 
Size: 525155 Color: 0
Size: 474556 Color: 1

Bin 397: 290 of cap free
Amount of items: 2
Items: 
Size: 668001 Color: 1
Size: 331710 Color: 0

Bin 398: 291 of cap free
Amount of items: 2
Items: 
Size: 795698 Color: 0
Size: 204012 Color: 1

Bin 399: 291 of cap free
Amount of items: 2
Items: 
Size: 798618 Color: 1
Size: 201092 Color: 0

Bin 400: 296 of cap free
Amount of items: 3
Items: 
Size: 603190 Color: 1
Size: 245524 Color: 0
Size: 150991 Color: 0

Bin 401: 298 of cap free
Amount of items: 2
Items: 
Size: 738059 Color: 0
Size: 261644 Color: 1

Bin 402: 299 of cap free
Amount of items: 2
Items: 
Size: 612032 Color: 1
Size: 387670 Color: 0

Bin 403: 302 of cap free
Amount of items: 3
Items: 
Size: 649313 Color: 1
Size: 184639 Color: 1
Size: 165747 Color: 0

Bin 404: 302 of cap free
Amount of items: 2
Items: 
Size: 657216 Color: 1
Size: 342483 Color: 0

Bin 405: 303 of cap free
Amount of items: 2
Items: 
Size: 630190 Color: 0
Size: 369508 Color: 1

Bin 406: 303 of cap free
Amount of items: 2
Items: 
Size: 517044 Color: 0
Size: 482654 Color: 1

Bin 407: 304 of cap free
Amount of items: 2
Items: 
Size: 740758 Color: 0
Size: 258939 Color: 1

Bin 408: 309 of cap free
Amount of items: 2
Items: 
Size: 631505 Color: 1
Size: 368187 Color: 0

Bin 409: 315 of cap free
Amount of items: 2
Items: 
Size: 653779 Color: 1
Size: 345907 Color: 0

Bin 410: 318 of cap free
Amount of items: 2
Items: 
Size: 786694 Color: 0
Size: 212989 Color: 1

Bin 411: 318 of cap free
Amount of items: 3
Items: 
Size: 716854 Color: 1
Size: 159612 Color: 1
Size: 123217 Color: 0

Bin 412: 319 of cap free
Amount of items: 2
Items: 
Size: 594809 Color: 1
Size: 404873 Color: 0

Bin 413: 321 of cap free
Amount of items: 2
Items: 
Size: 602411 Color: 1
Size: 397269 Color: 0

Bin 414: 326 of cap free
Amount of items: 2
Items: 
Size: 517944 Color: 1
Size: 481731 Color: 0

Bin 415: 331 of cap free
Amount of items: 2
Items: 
Size: 561536 Color: 1
Size: 438134 Color: 0

Bin 416: 337 of cap free
Amount of items: 2
Items: 
Size: 583955 Color: 1
Size: 415709 Color: 0

Bin 417: 338 of cap free
Amount of items: 2
Items: 
Size: 664823 Color: 0
Size: 334840 Color: 1

Bin 418: 338 of cap free
Amount of items: 2
Items: 
Size: 586779 Color: 1
Size: 412884 Color: 0

Bin 419: 340 of cap free
Amount of items: 2
Items: 
Size: 732580 Color: 1
Size: 267081 Color: 0

Bin 420: 341 of cap free
Amount of items: 2
Items: 
Size: 795638 Color: 1
Size: 204022 Color: 0

Bin 421: 341 of cap free
Amount of items: 2
Items: 
Size: 777695 Color: 1
Size: 221965 Color: 0

Bin 422: 342 of cap free
Amount of items: 2
Items: 
Size: 624988 Color: 0
Size: 374671 Color: 1

Bin 423: 345 of cap free
Amount of items: 2
Items: 
Size: 599451 Color: 1
Size: 400205 Color: 0

Bin 424: 346 of cap free
Amount of items: 2
Items: 
Size: 510297 Color: 1
Size: 489358 Color: 0

Bin 425: 348 of cap free
Amount of items: 2
Items: 
Size: 563003 Color: 1
Size: 436650 Color: 0

Bin 426: 352 of cap free
Amount of items: 2
Items: 
Size: 798149 Color: 0
Size: 201500 Color: 1

Bin 427: 354 of cap free
Amount of items: 2
Items: 
Size: 533235 Color: 0
Size: 466412 Color: 1

Bin 428: 354 of cap free
Amount of items: 2
Items: 
Size: 748747 Color: 1
Size: 250900 Color: 0

Bin 429: 356 of cap free
Amount of items: 2
Items: 
Size: 569720 Color: 0
Size: 429925 Color: 1

Bin 430: 364 of cap free
Amount of items: 2
Items: 
Size: 780097 Color: 0
Size: 219540 Color: 1

Bin 431: 366 of cap free
Amount of items: 2
Items: 
Size: 626898 Color: 1
Size: 372737 Color: 0

Bin 432: 368 of cap free
Amount of items: 2
Items: 
Size: 693649 Color: 0
Size: 305984 Color: 1

Bin 433: 371 of cap free
Amount of items: 2
Items: 
Size: 612651 Color: 1
Size: 386979 Color: 0

Bin 434: 371 of cap free
Amount of items: 2
Items: 
Size: 667034 Color: 0
Size: 332596 Color: 1

Bin 435: 374 of cap free
Amount of items: 2
Items: 
Size: 526886 Color: 1
Size: 472741 Color: 0

Bin 436: 380 of cap free
Amount of items: 2
Items: 
Size: 607638 Color: 0
Size: 391983 Color: 1

Bin 437: 381 of cap free
Amount of items: 2
Items: 
Size: 541680 Color: 1
Size: 457940 Color: 0

Bin 438: 381 of cap free
Amount of items: 2
Items: 
Size: 722010 Color: 0
Size: 277610 Color: 1

Bin 439: 383 of cap free
Amount of items: 2
Items: 
Size: 799468 Color: 0
Size: 200150 Color: 1

Bin 440: 386 of cap free
Amount of items: 2
Items: 
Size: 531090 Color: 0
Size: 468525 Color: 1

Bin 441: 386 of cap free
Amount of items: 2
Items: 
Size: 677980 Color: 1
Size: 321635 Color: 0

Bin 442: 389 of cap free
Amount of items: 2
Items: 
Size: 703443 Color: 1
Size: 296169 Color: 0

Bin 443: 392 of cap free
Amount of items: 2
Items: 
Size: 641533 Color: 1
Size: 358076 Color: 0

Bin 444: 398 of cap free
Amount of items: 2
Items: 
Size: 564000 Color: 1
Size: 435603 Color: 0

Bin 445: 400 of cap free
Amount of items: 2
Items: 
Size: 514543 Color: 1
Size: 485058 Color: 0

Bin 446: 400 of cap free
Amount of items: 2
Items: 
Size: 632854 Color: 1
Size: 366747 Color: 0

Bin 447: 405 of cap free
Amount of items: 2
Items: 
Size: 672094 Color: 0
Size: 327502 Color: 1

Bin 448: 411 of cap free
Amount of items: 2
Items: 
Size: 754927 Color: 0
Size: 244663 Color: 1

Bin 449: 416 of cap free
Amount of items: 2
Items: 
Size: 584360 Color: 0
Size: 415225 Color: 1

Bin 450: 417 of cap free
Amount of items: 2
Items: 
Size: 623806 Color: 0
Size: 375778 Color: 1

Bin 451: 421 of cap free
Amount of items: 2
Items: 
Size: 682845 Color: 0
Size: 316735 Color: 1

Bin 452: 421 of cap free
Amount of items: 2
Items: 
Size: 764731 Color: 0
Size: 234849 Color: 1

Bin 453: 424 of cap free
Amount of items: 2
Items: 
Size: 565367 Color: 0
Size: 434210 Color: 1

Bin 454: 431 of cap free
Amount of items: 2
Items: 
Size: 688092 Color: 1
Size: 311478 Color: 0

Bin 455: 432 of cap free
Amount of items: 2
Items: 
Size: 572306 Color: 1
Size: 427263 Color: 0

Bin 456: 433 of cap free
Amount of items: 2
Items: 
Size: 568306 Color: 0
Size: 431262 Color: 1

Bin 457: 434 of cap free
Amount of items: 2
Items: 
Size: 560345 Color: 0
Size: 439222 Color: 1

Bin 458: 440 of cap free
Amount of items: 2
Items: 
Size: 798472 Color: 1
Size: 201089 Color: 0

Bin 459: 441 of cap free
Amount of items: 2
Items: 
Size: 684065 Color: 1
Size: 315495 Color: 0

Bin 460: 441 of cap free
Amount of items: 2
Items: 
Size: 652858 Color: 0
Size: 346702 Color: 1

Bin 461: 443 of cap free
Amount of items: 2
Items: 
Size: 500435 Color: 1
Size: 499123 Color: 0

Bin 462: 444 of cap free
Amount of items: 3
Items: 
Size: 738745 Color: 0
Size: 139437 Color: 0
Size: 121375 Color: 1

Bin 463: 444 of cap free
Amount of items: 2
Items: 
Size: 570286 Color: 0
Size: 429271 Color: 1

Bin 464: 448 of cap free
Amount of items: 2
Items: 
Size: 519935 Color: 1
Size: 479618 Color: 0

Bin 465: 451 of cap free
Amount of items: 2
Items: 
Size: 557091 Color: 0
Size: 442459 Color: 1

Bin 466: 455 of cap free
Amount of items: 2
Items: 
Size: 611405 Color: 1
Size: 388141 Color: 0

Bin 467: 456 of cap free
Amount of items: 3
Items: 
Size: 777167 Color: 0
Size: 111897 Color: 1
Size: 110481 Color: 1

Bin 468: 456 of cap free
Amount of items: 2
Items: 
Size: 713307 Color: 0
Size: 286238 Color: 1

Bin 469: 459 of cap free
Amount of items: 2
Items: 
Size: 656701 Color: 1
Size: 342841 Color: 0

Bin 470: 463 of cap free
Amount of items: 3
Items: 
Size: 796085 Color: 1
Size: 102220 Color: 0
Size: 101233 Color: 1

Bin 471: 467 of cap free
Amount of items: 2
Items: 
Size: 681267 Color: 1
Size: 318267 Color: 0

Bin 472: 467 of cap free
Amount of items: 2
Items: 
Size: 782684 Color: 1
Size: 216850 Color: 0

Bin 473: 468 of cap free
Amount of items: 2
Items: 
Size: 542228 Color: 0
Size: 457305 Color: 1

Bin 474: 468 of cap free
Amount of items: 2
Items: 
Size: 676850 Color: 0
Size: 322683 Color: 1

Bin 475: 471 of cap free
Amount of items: 2
Items: 
Size: 517858 Color: 1
Size: 481672 Color: 0

Bin 476: 477 of cap free
Amount of items: 2
Items: 
Size: 726238 Color: 1
Size: 273286 Color: 0

Bin 477: 480 of cap free
Amount of items: 2
Items: 
Size: 646608 Color: 1
Size: 352913 Color: 0

Bin 478: 480 of cap free
Amount of items: 2
Items: 
Size: 514002 Color: 1
Size: 485519 Color: 0

Bin 479: 480 of cap free
Amount of items: 2
Items: 
Size: 571464 Color: 0
Size: 428057 Color: 1

Bin 480: 483 of cap free
Amount of items: 2
Items: 
Size: 762867 Color: 0
Size: 236651 Color: 1

Bin 481: 483 of cap free
Amount of items: 2
Items: 
Size: 703816 Color: 0
Size: 295702 Color: 1

Bin 482: 484 of cap free
Amount of items: 2
Items: 
Size: 706106 Color: 0
Size: 293411 Color: 1

Bin 483: 491 of cap free
Amount of items: 2
Items: 
Size: 747641 Color: 0
Size: 251869 Color: 1

Bin 484: 492 of cap free
Amount of items: 2
Items: 
Size: 728476 Color: 1
Size: 271033 Color: 0

Bin 485: 501 of cap free
Amount of items: 2
Items: 
Size: 616776 Color: 1
Size: 382724 Color: 0

Bin 486: 504 of cap free
Amount of items: 2
Items: 
Size: 507430 Color: 0
Size: 492067 Color: 1

Bin 487: 509 of cap free
Amount of items: 2
Items: 
Size: 686744 Color: 1
Size: 312748 Color: 0

Bin 488: 510 of cap free
Amount of items: 2
Items: 
Size: 788553 Color: 1
Size: 210938 Color: 0

Bin 489: 510 of cap free
Amount of items: 2
Items: 
Size: 578427 Color: 0
Size: 421064 Color: 1

Bin 490: 514 of cap free
Amount of items: 2
Items: 
Size: 789248 Color: 0
Size: 210239 Color: 1

Bin 491: 516 of cap free
Amount of items: 2
Items: 
Size: 725222 Color: 0
Size: 274263 Color: 1

Bin 492: 518 of cap free
Amount of items: 2
Items: 
Size: 532951 Color: 1
Size: 466532 Color: 0

Bin 493: 518 of cap free
Amount of items: 2
Items: 
Size: 572906 Color: 0
Size: 426577 Color: 1

Bin 494: 519 of cap free
Amount of items: 2
Items: 
Size: 574478 Color: 1
Size: 425004 Color: 0

Bin 495: 519 of cap free
Amount of items: 2
Items: 
Size: 597070 Color: 0
Size: 402412 Color: 1

Bin 496: 533 of cap free
Amount of items: 2
Items: 
Size: 666160 Color: 1
Size: 333308 Color: 0

Bin 497: 536 of cap free
Amount of items: 2
Items: 
Size: 694781 Color: 1
Size: 304684 Color: 0

Bin 498: 536 of cap free
Amount of items: 2
Items: 
Size: 555392 Color: 0
Size: 444073 Color: 1

Bin 499: 542 of cap free
Amount of items: 2
Items: 
Size: 784916 Color: 1
Size: 214543 Color: 0

Bin 500: 544 of cap free
Amount of items: 2
Items: 
Size: 676728 Color: 1
Size: 322729 Color: 0

Bin 501: 550 of cap free
Amount of items: 2
Items: 
Size: 747159 Color: 1
Size: 252292 Color: 0

Bin 502: 552 of cap free
Amount of items: 2
Items: 
Size: 654497 Color: 0
Size: 344952 Color: 1

Bin 503: 554 of cap free
Amount of items: 2
Items: 
Size: 559276 Color: 1
Size: 440171 Color: 0

Bin 504: 556 of cap free
Amount of items: 2
Items: 
Size: 506496 Color: 0
Size: 492949 Color: 1

Bin 505: 559 of cap free
Amount of items: 2
Items: 
Size: 651988 Color: 1
Size: 347454 Color: 0

Bin 506: 580 of cap free
Amount of items: 2
Items: 
Size: 629729 Color: 1
Size: 369692 Color: 0

Bin 507: 588 of cap free
Amount of items: 2
Items: 
Size: 601735 Color: 0
Size: 397678 Color: 1

Bin 508: 591 of cap free
Amount of items: 3
Items: 
Size: 359922 Color: 1
Size: 319749 Color: 0
Size: 319739 Color: 0

Bin 509: 591 of cap free
Amount of items: 2
Items: 
Size: 649474 Color: 0
Size: 349936 Color: 1

Bin 510: 592 of cap free
Amount of items: 2
Items: 
Size: 504699 Color: 1
Size: 494710 Color: 0

Bin 511: 592 of cap free
Amount of items: 2
Items: 
Size: 743303 Color: 1
Size: 256106 Color: 0

Bin 512: 599 of cap free
Amount of items: 2
Items: 
Size: 539206 Color: 1
Size: 460196 Color: 0

Bin 513: 603 of cap free
Amount of items: 2
Items: 
Size: 506890 Color: 1
Size: 492508 Color: 0

Bin 514: 606 of cap free
Amount of items: 2
Items: 
Size: 775297 Color: 1
Size: 224098 Color: 0

Bin 515: 606 of cap free
Amount of items: 2
Items: 
Size: 512700 Color: 1
Size: 486695 Color: 0

Bin 516: 608 of cap free
Amount of items: 2
Items: 
Size: 728976 Color: 1
Size: 270417 Color: 0

Bin 517: 609 of cap free
Amount of items: 2
Items: 
Size: 757818 Color: 1
Size: 241574 Color: 0

Bin 518: 611 of cap free
Amount of items: 2
Items: 
Size: 546912 Color: 1
Size: 452478 Color: 0

Bin 519: 613 of cap free
Amount of items: 2
Items: 
Size: 749175 Color: 0
Size: 250213 Color: 1

Bin 520: 614 of cap free
Amount of items: 2
Items: 
Size: 765801 Color: 0
Size: 233586 Color: 1

Bin 521: 627 of cap free
Amount of items: 2
Items: 
Size: 503169 Color: 0
Size: 496205 Color: 1

Bin 522: 628 of cap free
Amount of items: 3
Items: 
Size: 701086 Color: 0
Size: 151604 Color: 1
Size: 146683 Color: 0

Bin 523: 629 of cap free
Amount of items: 2
Items: 
Size: 565859 Color: 1
Size: 433513 Color: 0

Bin 524: 652 of cap free
Amount of items: 2
Items: 
Size: 753233 Color: 1
Size: 246116 Color: 0

Bin 525: 652 of cap free
Amount of items: 2
Items: 
Size: 575059 Color: 0
Size: 424290 Color: 1

Bin 526: 653 of cap free
Amount of items: 2
Items: 
Size: 641464 Color: 1
Size: 357884 Color: 0

Bin 527: 661 of cap free
Amount of items: 2
Items: 
Size: 653516 Color: 1
Size: 345824 Color: 0

Bin 528: 663 of cap free
Amount of items: 2
Items: 
Size: 548932 Color: 0
Size: 450406 Color: 1

Bin 529: 667 of cap free
Amount of items: 2
Items: 
Size: 765090 Color: 1
Size: 234244 Color: 0

Bin 530: 675 of cap free
Amount of items: 2
Items: 
Size: 647525 Color: 1
Size: 351801 Color: 0

Bin 531: 685 of cap free
Amount of items: 2
Items: 
Size: 707462 Color: 1
Size: 291854 Color: 0

Bin 532: 686 of cap free
Amount of items: 2
Items: 
Size: 617070 Color: 0
Size: 382245 Color: 1

Bin 533: 689 of cap free
Amount of items: 2
Items: 
Size: 683375 Color: 1
Size: 315937 Color: 0

Bin 534: 695 of cap free
Amount of items: 2
Items: 
Size: 672661 Color: 0
Size: 326645 Color: 1

Bin 535: 695 of cap free
Amount of items: 2
Items: 
Size: 676755 Color: 0
Size: 322551 Color: 1

Bin 536: 701 of cap free
Amount of items: 2
Items: 
Size: 511746 Color: 0
Size: 487554 Color: 1

Bin 537: 709 of cap free
Amount of items: 2
Items: 
Size: 696167 Color: 1
Size: 303125 Color: 0

Bin 538: 711 of cap free
Amount of items: 2
Items: 
Size: 571170 Color: 1
Size: 428120 Color: 0

Bin 539: 714 of cap free
Amount of items: 2
Items: 
Size: 613505 Color: 0
Size: 385782 Color: 1

Bin 540: 715 of cap free
Amount of items: 2
Items: 
Size: 561352 Color: 1
Size: 437934 Color: 0

Bin 541: 716 of cap free
Amount of items: 2
Items: 
Size: 654633 Color: 1
Size: 344652 Color: 0

Bin 542: 717 of cap free
Amount of items: 2
Items: 
Size: 779897 Color: 0
Size: 219387 Color: 1

Bin 543: 717 of cap free
Amount of items: 2
Items: 
Size: 625993 Color: 0
Size: 373291 Color: 1

Bin 544: 719 of cap free
Amount of items: 2
Items: 
Size: 717592 Color: 1
Size: 281690 Color: 0

Bin 545: 727 of cap free
Amount of items: 2
Items: 
Size: 622290 Color: 0
Size: 376984 Color: 1

Bin 546: 732 of cap free
Amount of items: 2
Items: 
Size: 515658 Color: 1
Size: 483611 Color: 0

Bin 547: 738 of cap free
Amount of items: 2
Items: 
Size: 549816 Color: 1
Size: 449447 Color: 0

Bin 548: 741 of cap free
Amount of items: 2
Items: 
Size: 755883 Color: 0
Size: 243377 Color: 1

Bin 549: 749 of cap free
Amount of items: 2
Items: 
Size: 654402 Color: 0
Size: 344850 Color: 1

Bin 550: 756 of cap free
Amount of items: 2
Items: 
Size: 561227 Color: 0
Size: 438018 Color: 1

Bin 551: 763 of cap free
Amount of items: 2
Items: 
Size: 784381 Color: 0
Size: 214857 Color: 1

Bin 552: 764 of cap free
Amount of items: 2
Items: 
Size: 517481 Color: 0
Size: 481756 Color: 1

Bin 553: 765 of cap free
Amount of items: 2
Items: 
Size: 693672 Color: 1
Size: 305564 Color: 0

Bin 554: 772 of cap free
Amount of items: 2
Items: 
Size: 714584 Color: 0
Size: 284645 Color: 1

Bin 555: 787 of cap free
Amount of items: 2
Items: 
Size: 714093 Color: 1
Size: 285121 Color: 0

Bin 556: 787 of cap free
Amount of items: 2
Items: 
Size: 663135 Color: 0
Size: 336079 Color: 1

Bin 557: 788 of cap free
Amount of items: 2
Items: 
Size: 764994 Color: 1
Size: 234219 Color: 0

Bin 558: 798 of cap free
Amount of items: 2
Items: 
Size: 599359 Color: 0
Size: 399844 Color: 1

Bin 559: 799 of cap free
Amount of items: 2
Items: 
Size: 603025 Color: 1
Size: 396177 Color: 0

Bin 560: 803 of cap free
Amount of items: 2
Items: 
Size: 655515 Color: 1
Size: 343683 Color: 0

Bin 561: 808 of cap free
Amount of items: 2
Items: 
Size: 760451 Color: 0
Size: 238742 Color: 1

Bin 562: 811 of cap free
Amount of items: 3
Items: 
Size: 615177 Color: 0
Size: 195884 Color: 0
Size: 188129 Color: 1

Bin 563: 813 of cap free
Amount of items: 2
Items: 
Size: 703079 Color: 1
Size: 296109 Color: 0

Bin 564: 815 of cap free
Amount of items: 2
Items: 
Size: 554412 Color: 0
Size: 444774 Color: 1

Bin 565: 818 of cap free
Amount of items: 2
Items: 
Size: 543074 Color: 1
Size: 456109 Color: 0

Bin 566: 820 of cap free
Amount of items: 2
Items: 
Size: 551316 Color: 1
Size: 447865 Color: 0

Bin 567: 822 of cap free
Amount of items: 2
Items: 
Size: 511717 Color: 0
Size: 487462 Color: 1

Bin 568: 822 of cap free
Amount of items: 2
Items: 
Size: 707580 Color: 0
Size: 291599 Color: 1

Bin 569: 828 of cap free
Amount of items: 2
Items: 
Size: 605745 Color: 0
Size: 393428 Color: 1

Bin 570: 836 of cap free
Amount of items: 2
Items: 
Size: 613958 Color: 1
Size: 385207 Color: 0

Bin 571: 845 of cap free
Amount of items: 2
Items: 
Size: 706269 Color: 1
Size: 292887 Color: 0

Bin 572: 846 of cap free
Amount of items: 2
Items: 
Size: 724095 Color: 0
Size: 275060 Color: 1

Bin 573: 849 of cap free
Amount of items: 2
Items: 
Size: 519532 Color: 0
Size: 479620 Color: 1

Bin 574: 883 of cap free
Amount of items: 2
Items: 
Size: 790029 Color: 0
Size: 209089 Color: 1

Bin 575: 888 of cap free
Amount of items: 2
Items: 
Size: 681041 Color: 1
Size: 318072 Color: 0

Bin 576: 888 of cap free
Amount of items: 2
Items: 
Size: 601335 Color: 1
Size: 397778 Color: 0

Bin 577: 902 of cap free
Amount of items: 2
Items: 
Size: 733389 Color: 0
Size: 265710 Color: 1

Bin 578: 916 of cap free
Amount of items: 2
Items: 
Size: 648430 Color: 0
Size: 350655 Color: 1

Bin 579: 917 of cap free
Amount of items: 2
Items: 
Size: 766357 Color: 1
Size: 232727 Color: 0

Bin 580: 920 of cap free
Amount of items: 2
Items: 
Size: 759039 Color: 0
Size: 240042 Color: 1

Bin 581: 930 of cap free
Amount of items: 2
Items: 
Size: 672369 Color: 1
Size: 326702 Color: 0

Bin 582: 933 of cap free
Amount of items: 2
Items: 
Size: 500012 Color: 1
Size: 499056 Color: 0

Bin 583: 941 of cap free
Amount of items: 2
Items: 
Size: 629578 Color: 1
Size: 369482 Color: 0

Bin 584: 943 of cap free
Amount of items: 2
Items: 
Size: 566839 Color: 1
Size: 432219 Color: 0

Bin 585: 949 of cap free
Amount of items: 2
Items: 
Size: 512671 Color: 0
Size: 486381 Color: 1

Bin 586: 962 of cap free
Amount of items: 2
Items: 
Size: 605689 Color: 0
Size: 393350 Color: 1

Bin 587: 967 of cap free
Amount of items: 2
Items: 
Size: 762400 Color: 0
Size: 236634 Color: 1

Bin 588: 969 of cap free
Amount of items: 2
Items: 
Size: 735957 Color: 1
Size: 263075 Color: 0

Bin 589: 972 of cap free
Amount of items: 2
Items: 
Size: 543922 Color: 0
Size: 455107 Color: 1

Bin 590: 976 of cap free
Amount of items: 2
Items: 
Size: 641458 Color: 0
Size: 357567 Color: 1

Bin 591: 980 of cap free
Amount of items: 2
Items: 
Size: 636317 Color: 0
Size: 362704 Color: 1

Bin 592: 981 of cap free
Amount of items: 3
Items: 
Size: 528909 Color: 1
Size: 303191 Color: 1
Size: 166920 Color: 0

Bin 593: 982 of cap free
Amount of items: 2
Items: 
Size: 791853 Color: 0
Size: 207166 Color: 1

Bin 594: 986 of cap free
Amount of items: 2
Items: 
Size: 748359 Color: 1
Size: 250656 Color: 0

Bin 595: 990 of cap free
Amount of items: 2
Items: 
Size: 509142 Color: 1
Size: 489869 Color: 0

Bin 596: 992 of cap free
Amount of items: 2
Items: 
Size: 618300 Color: 1
Size: 380709 Color: 0

Bin 597: 994 of cap free
Amount of items: 2
Items: 
Size: 757251 Color: 0
Size: 241756 Color: 1

Bin 598: 1004 of cap free
Amount of items: 2
Items: 
Size: 682326 Color: 0
Size: 316671 Color: 1

Bin 599: 1009 of cap free
Amount of items: 2
Items: 
Size: 716995 Color: 0
Size: 281997 Color: 1

Bin 600: 1014 of cap free
Amount of items: 2
Items: 
Size: 634037 Color: 0
Size: 364950 Color: 1

Bin 601: 1015 of cap free
Amount of items: 2
Items: 
Size: 667875 Color: 1
Size: 331111 Color: 0

Bin 602: 1016 of cap free
Amount of items: 2
Items: 
Size: 719075 Color: 0
Size: 279910 Color: 1

Bin 603: 1017 of cap free
Amount of items: 2
Items: 
Size: 574403 Color: 1
Size: 424581 Color: 0

Bin 604: 1020 of cap free
Amount of items: 2
Items: 
Size: 659038 Color: 0
Size: 339943 Color: 1

Bin 605: 1023 of cap free
Amount of items: 3
Items: 
Size: 647312 Color: 1
Size: 239157 Color: 0
Size: 112509 Color: 0

Bin 606: 1034 of cap free
Amount of items: 2
Items: 
Size: 609952 Color: 0
Size: 389015 Color: 1

Bin 607: 1037 of cap free
Amount of items: 2
Items: 
Size: 790370 Color: 1
Size: 208594 Color: 0

Bin 608: 1039 of cap free
Amount of items: 2
Items: 
Size: 537171 Color: 1
Size: 461791 Color: 0

Bin 609: 1040 of cap free
Amount of items: 2
Items: 
Size: 614824 Color: 1
Size: 384137 Color: 0

Bin 610: 1044 of cap free
Amount of items: 2
Items: 
Size: 770819 Color: 0
Size: 228138 Color: 1

Bin 611: 1056 of cap free
Amount of items: 2
Items: 
Size: 699953 Color: 0
Size: 298992 Color: 1

Bin 612: 1062 of cap free
Amount of items: 2
Items: 
Size: 700404 Color: 1
Size: 298535 Color: 0

Bin 613: 1067 of cap free
Amount of items: 2
Items: 
Size: 642792 Color: 1
Size: 356142 Color: 0

Bin 614: 1083 of cap free
Amount of items: 2
Items: 
Size: 748719 Color: 0
Size: 250199 Color: 1

Bin 615: 1086 of cap free
Amount of items: 2
Items: 
Size: 680169 Color: 0
Size: 318746 Color: 1

Bin 616: 1089 of cap free
Amount of items: 2
Items: 
Size: 562464 Color: 1
Size: 436448 Color: 0

Bin 617: 1100 of cap free
Amount of items: 2
Items: 
Size: 772752 Color: 1
Size: 226149 Color: 0

Bin 618: 1102 of cap free
Amount of items: 2
Items: 
Size: 558136 Color: 0
Size: 440763 Color: 1

Bin 619: 1102 of cap free
Amount of items: 2
Items: 
Size: 591432 Color: 0
Size: 407467 Color: 1

Bin 620: 1108 of cap free
Amount of items: 2
Items: 
Size: 698138 Color: 0
Size: 300755 Color: 1

Bin 621: 1111 of cap free
Amount of items: 2
Items: 
Size: 720899 Color: 1
Size: 277991 Color: 0

Bin 622: 1118 of cap free
Amount of items: 2
Items: 
Size: 711377 Color: 0
Size: 287506 Color: 1

Bin 623: 1120 of cap free
Amount of items: 2
Items: 
Size: 644883 Color: 0
Size: 353998 Color: 1

Bin 624: 1130 of cap free
Amount of items: 2
Items: 
Size: 795618 Color: 0
Size: 203253 Color: 1

Bin 625: 1155 of cap free
Amount of items: 2
Items: 
Size: 662228 Color: 1
Size: 336618 Color: 0

Bin 626: 1157 of cap free
Amount of items: 2
Items: 
Size: 675518 Color: 1
Size: 323326 Color: 0

Bin 627: 1161 of cap free
Amount of items: 2
Items: 
Size: 568217 Color: 1
Size: 430623 Color: 0

Bin 628: 1162 of cap free
Amount of items: 2
Items: 
Size: 716990 Color: 0
Size: 281849 Color: 1

Bin 629: 1162 of cap free
Amount of items: 2
Items: 
Size: 652483 Color: 0
Size: 346356 Color: 1

Bin 630: 1166 of cap free
Amount of items: 2
Items: 
Size: 542933 Color: 1
Size: 455902 Color: 0

Bin 631: 1183 of cap free
Amount of items: 2
Items: 
Size: 632335 Color: 1
Size: 366483 Color: 0

Bin 632: 1203 of cap free
Amount of items: 2
Items: 
Size: 659316 Color: 1
Size: 339482 Color: 0

Bin 633: 1210 of cap free
Amount of items: 2
Items: 
Size: 726055 Color: 1
Size: 272736 Color: 0

Bin 634: 1223 of cap free
Amount of items: 2
Items: 
Size: 768191 Color: 0
Size: 230587 Color: 1

Bin 635: 1228 of cap free
Amount of items: 2
Items: 
Size: 703627 Color: 0
Size: 295146 Color: 1

Bin 636: 1234 of cap free
Amount of items: 2
Items: 
Size: 757487 Color: 1
Size: 241280 Color: 0

Bin 637: 1247 of cap free
Amount of items: 2
Items: 
Size: 513747 Color: 1
Size: 485007 Color: 0

Bin 638: 1255 of cap free
Amount of items: 2
Items: 
Size: 538721 Color: 0
Size: 460025 Color: 1

Bin 639: 1273 of cap free
Amount of items: 2
Items: 
Size: 751931 Color: 1
Size: 246797 Color: 0

Bin 640: 1276 of cap free
Amount of items: 2
Items: 
Size: 737945 Color: 1
Size: 260780 Color: 0

Bin 641: 1276 of cap free
Amount of items: 2
Items: 
Size: 577998 Color: 0
Size: 420727 Color: 1

Bin 642: 1280 of cap free
Amount of items: 2
Items: 
Size: 618186 Color: 1
Size: 380535 Color: 0

Bin 643: 1285 of cap free
Amount of items: 2
Items: 
Size: 799215 Color: 1
Size: 199501 Color: 0

Bin 644: 1286 of cap free
Amount of items: 2
Items: 
Size: 706087 Color: 1
Size: 292628 Color: 0

Bin 645: 1295 of cap free
Amount of items: 2
Items: 
Size: 687529 Color: 0
Size: 311177 Color: 1

Bin 646: 1304 of cap free
Amount of items: 2
Items: 
Size: 784314 Color: 0
Size: 214383 Color: 1

Bin 647: 1313 of cap free
Amount of items: 2
Items: 
Size: 743914 Color: 1
Size: 254774 Color: 0

Bin 648: 1316 of cap free
Amount of items: 2
Items: 
Size: 782265 Color: 1
Size: 216420 Color: 0

Bin 649: 1321 of cap free
Amount of items: 2
Items: 
Size: 776044 Color: 1
Size: 222636 Color: 0

Bin 650: 1332 of cap free
Amount of items: 2
Items: 
Size: 749675 Color: 1
Size: 248994 Color: 0

Bin 651: 1345 of cap free
Amount of items: 2
Items: 
Size: 728183 Color: 0
Size: 270473 Color: 1

Bin 652: 1354 of cap free
Amount of items: 2
Items: 
Size: 575592 Color: 1
Size: 423055 Color: 0

Bin 653: 1363 of cap free
Amount of items: 2
Items: 
Size: 788425 Color: 0
Size: 210213 Color: 1

Bin 654: 1372 of cap free
Amount of items: 2
Items: 
Size: 623417 Color: 0
Size: 375212 Color: 1

Bin 655: 1375 of cap free
Amount of items: 2
Items: 
Size: 624609 Color: 1
Size: 374017 Color: 0

Bin 656: 1380 of cap free
Amount of items: 2
Items: 
Size: 531050 Color: 1
Size: 467571 Color: 0

Bin 657: 1386 of cap free
Amount of items: 2
Items: 
Size: 622792 Color: 1
Size: 375823 Color: 0

Bin 658: 1400 of cap free
Amount of items: 2
Items: 
Size: 765184 Color: 0
Size: 233417 Color: 1

Bin 659: 1403 of cap free
Amount of items: 2
Items: 
Size: 664739 Color: 1
Size: 333859 Color: 0

Bin 660: 1407 of cap free
Amount of items: 2
Items: 
Size: 775971 Color: 1
Size: 222623 Color: 0

Bin 661: 1417 of cap free
Amount of items: 2
Items: 
Size: 558439 Color: 1
Size: 440145 Color: 0

Bin 662: 1426 of cap free
Amount of items: 2
Items: 
Size: 708968 Color: 0
Size: 289607 Color: 1

Bin 663: 1427 of cap free
Amount of items: 2
Items: 
Size: 779819 Color: 0
Size: 218755 Color: 1

Bin 664: 1431 of cap free
Amount of items: 2
Items: 
Size: 680948 Color: 1
Size: 317622 Color: 0

Bin 665: 1435 of cap free
Amount of items: 2
Items: 
Size: 730197 Color: 0
Size: 268369 Color: 1

Bin 666: 1441 of cap free
Amount of items: 2
Items: 
Size: 648011 Color: 0
Size: 350549 Color: 1

Bin 667: 1444 of cap free
Amount of items: 2
Items: 
Size: 621966 Color: 0
Size: 376591 Color: 1

Bin 668: 1453 of cap free
Amount of items: 2
Items: 
Size: 655211 Color: 0
Size: 343337 Color: 1

Bin 669: 1464 of cap free
Amount of items: 2
Items: 
Size: 606874 Color: 0
Size: 391663 Color: 1

Bin 670: 1474 of cap free
Amount of items: 2
Items: 
Size: 789853 Color: 0
Size: 208674 Color: 1

Bin 671: 1475 of cap free
Amount of items: 2
Items: 
Size: 652411 Color: 0
Size: 346115 Color: 1

Bin 672: 1482 of cap free
Amount of items: 2
Items: 
Size: 689446 Color: 0
Size: 309073 Color: 1

Bin 673: 1485 of cap free
Amount of items: 2
Items: 
Size: 743791 Color: 0
Size: 254725 Color: 1

Bin 674: 1494 of cap free
Amount of items: 2
Items: 
Size: 689824 Color: 1
Size: 308683 Color: 0

Bin 675: 1502 of cap free
Amount of items: 2
Items: 
Size: 507083 Color: 0
Size: 491416 Color: 1

Bin 676: 1506 of cap free
Amount of items: 2
Items: 
Size: 574926 Color: 0
Size: 423569 Color: 1

Bin 677: 1514 of cap free
Amount of items: 2
Items: 
Size: 739859 Color: 1
Size: 258628 Color: 0

Bin 678: 1516 of cap free
Amount of items: 2
Items: 
Size: 600826 Color: 1
Size: 397659 Color: 0

Bin 679: 1529 of cap free
Amount of items: 2
Items: 
Size: 564652 Color: 0
Size: 433820 Color: 1

Bin 680: 1534 of cap free
Amount of items: 2
Items: 
Size: 515545 Color: 1
Size: 482922 Color: 0

Bin 681: 1544 of cap free
Amount of items: 2
Items: 
Size: 722321 Color: 1
Size: 276136 Color: 0

Bin 682: 1547 of cap free
Amount of items: 2
Items: 
Size: 577968 Color: 0
Size: 420486 Color: 1

Bin 683: 1559 of cap free
Amount of items: 2
Items: 
Size: 677927 Color: 1
Size: 320515 Color: 0

Bin 684: 1565 of cap free
Amount of items: 2
Items: 
Size: 643903 Color: 1
Size: 354533 Color: 0

Bin 685: 1568 of cap free
Amount of items: 2
Items: 
Size: 562754 Color: 0
Size: 435679 Color: 1

Bin 686: 1575 of cap free
Amount of items: 2
Items: 
Size: 625898 Color: 0
Size: 372528 Color: 1

Bin 687: 1589 of cap free
Amount of items: 2
Items: 
Size: 758449 Color: 0
Size: 239963 Color: 1

Bin 688: 1590 of cap free
Amount of items: 2
Items: 
Size: 631782 Color: 0
Size: 366629 Color: 1

Bin 689: 1631 of cap free
Amount of items: 2
Items: 
Size: 639198 Color: 0
Size: 359172 Color: 1

Bin 690: 1633 of cap free
Amount of items: 2
Items: 
Size: 609757 Color: 0
Size: 388611 Color: 1

Bin 691: 1637 of cap free
Amount of items: 2
Items: 
Size: 609451 Color: 1
Size: 388913 Color: 0

Bin 692: 1648 of cap free
Amount of items: 2
Items: 
Size: 707456 Color: 1
Size: 290897 Color: 0

Bin 693: 1672 of cap free
Amount of items: 2
Items: 
Size: 703460 Color: 0
Size: 294869 Color: 1

Bin 694: 1678 of cap free
Amount of items: 2
Items: 
Size: 770435 Color: 0
Size: 227888 Color: 1

Bin 695: 1680 of cap free
Amount of items: 2
Items: 
Size: 682957 Color: 1
Size: 315364 Color: 0

Bin 696: 1693 of cap free
Amount of items: 2
Items: 
Size: 634579 Color: 1
Size: 363729 Color: 0

Bin 697: 1694 of cap free
Amount of items: 2
Items: 
Size: 672017 Color: 0
Size: 326290 Color: 1

Bin 698: 1704 of cap free
Amount of items: 2
Items: 
Size: 748251 Color: 0
Size: 250046 Color: 1

Bin 699: 1710 of cap free
Amount of items: 2
Items: 
Size: 788084 Color: 0
Size: 210207 Color: 1

Bin 700: 1723 of cap free
Amount of items: 2
Items: 
Size: 667999 Color: 0
Size: 330279 Color: 1

Bin 701: 1727 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 0
Size: 385534 Color: 1

Bin 702: 1736 of cap free
Amount of items: 2
Items: 
Size: 711121 Color: 0
Size: 287144 Color: 1

Bin 703: 1750 of cap free
Amount of items: 2
Items: 
Size: 574879 Color: 0
Size: 423372 Color: 1

Bin 704: 1785 of cap free
Amount of items: 3
Items: 
Size: 720835 Color: 0
Size: 170133 Color: 1
Size: 107248 Color: 0

Bin 705: 1786 of cap free
Amount of items: 2
Items: 
Size: 567950 Color: 1
Size: 430265 Color: 0

Bin 706: 1792 of cap free
Amount of items: 2
Items: 
Size: 770429 Color: 0
Size: 227780 Color: 1

Bin 707: 1820 of cap free
Amount of items: 2
Items: 
Size: 614830 Color: 0
Size: 383351 Color: 1

Bin 708: 1828 of cap free
Amount of items: 2
Items: 
Size: 523170 Color: 1
Size: 475003 Color: 0

Bin 709: 1835 of cap free
Amount of items: 2
Items: 
Size: 765977 Color: 1
Size: 232189 Color: 0

Bin 710: 1837 of cap free
Amount of items: 2
Items: 
Size: 549131 Color: 1
Size: 449033 Color: 0

Bin 711: 1847 of cap free
Amount of items: 2
Items: 
Size: 586100 Color: 1
Size: 412054 Color: 0

Bin 712: 1847 of cap free
Amount of items: 2
Items: 
Size: 680642 Color: 1
Size: 317512 Color: 0

Bin 713: 1851 of cap free
Amount of items: 2
Items: 
Size: 528015 Color: 0
Size: 470135 Color: 1

Bin 714: 1852 of cap free
Amount of items: 2
Items: 
Size: 567863 Color: 0
Size: 430286 Color: 1

Bin 715: 1854 of cap free
Amount of items: 2
Items: 
Size: 637967 Color: 1
Size: 360180 Color: 0

Bin 716: 1880 of cap free
Amount of items: 2
Items: 
Size: 591366 Color: 0
Size: 406755 Color: 1

Bin 717: 1885 of cap free
Amount of items: 2
Items: 
Size: 723192 Color: 0
Size: 274924 Color: 1

Bin 718: 1891 of cap free
Amount of items: 2
Items: 
Size: 510290 Color: 1
Size: 487820 Color: 0

Bin 719: 1925 of cap free
Amount of items: 2
Items: 
Size: 730017 Color: 0
Size: 268059 Color: 1

Bin 720: 1942 of cap free
Amount of items: 2
Items: 
Size: 758436 Color: 0
Size: 239623 Color: 1

Bin 721: 1962 of cap free
Amount of items: 2
Items: 
Size: 757236 Color: 1
Size: 240803 Color: 0

Bin 722: 1996 of cap free
Amount of items: 2
Items: 
Size: 562360 Color: 0
Size: 435645 Color: 1

Bin 723: 2002 of cap free
Amount of items: 2
Items: 
Size: 662369 Color: 0
Size: 335630 Color: 1

Bin 724: 2004 of cap free
Amount of items: 2
Items: 
Size: 558289 Color: 1
Size: 439708 Color: 0

Bin 725: 2041 of cap free
Amount of items: 2
Items: 
Size: 692521 Color: 0
Size: 305439 Color: 1

Bin 726: 2062 of cap free
Amount of items: 2
Items: 
Size: 532946 Color: 1
Size: 464993 Color: 0

Bin 727: 2069 of cap free
Amount of items: 2
Items: 
Size: 579662 Color: 0
Size: 418270 Color: 1

Bin 728: 2093 of cap free
Amount of items: 2
Items: 
Size: 634572 Color: 1
Size: 363336 Color: 0

Bin 729: 2093 of cap free
Amount of items: 2
Items: 
Size: 559912 Color: 0
Size: 437996 Color: 1

Bin 730: 2094 of cap free
Amount of items: 2
Items: 
Size: 711076 Color: 0
Size: 286831 Color: 1

Bin 731: 2101 of cap free
Amount of items: 2
Items: 
Size: 725226 Color: 1
Size: 272674 Color: 0

Bin 732: 2110 of cap free
Amount of items: 2
Items: 
Size: 640209 Color: 1
Size: 357682 Color: 0

Bin 733: 2114 of cap free
Amount of items: 2
Items: 
Size: 530093 Color: 0
Size: 467794 Color: 1

Bin 734: 2166 of cap free
Amount of items: 2
Items: 
Size: 609223 Color: 1
Size: 388612 Color: 0

Bin 735: 2171 of cap free
Amount of items: 2
Items: 
Size: 658105 Color: 0
Size: 339725 Color: 1

Bin 736: 2182 of cap free
Amount of items: 2
Items: 
Size: 594444 Color: 0
Size: 403375 Color: 1

Bin 737: 2209 of cap free
Amount of items: 2
Items: 
Size: 580155 Color: 1
Size: 417637 Color: 0

Bin 738: 2209 of cap free
Amount of items: 2
Items: 
Size: 727883 Color: 1
Size: 269909 Color: 0

Bin 739: 2214 of cap free
Amount of items: 2
Items: 
Size: 538278 Color: 0
Size: 459509 Color: 1

Bin 740: 2228 of cap free
Amount of items: 2
Items: 
Size: 784313 Color: 0
Size: 213460 Color: 1

Bin 741: 2277 of cap free
Amount of items: 2
Items: 
Size: 675500 Color: 1
Size: 322224 Color: 0

Bin 742: 2279 of cap free
Amount of items: 2
Items: 
Size: 522842 Color: 0
Size: 474880 Color: 1

Bin 743: 2319 of cap free
Amount of items: 2
Items: 
Size: 562218 Color: 0
Size: 435464 Color: 1

Bin 744: 2337 of cap free
Amount of items: 2
Items: 
Size: 754554 Color: 0
Size: 243110 Color: 1

Bin 745: 2347 of cap free
Amount of items: 2
Items: 
Size: 604053 Color: 1
Size: 393601 Color: 0

Bin 746: 2378 of cap free
Amount of items: 2
Items: 
Size: 614647 Color: 0
Size: 382976 Color: 1

Bin 747: 2382 of cap free
Amount of items: 2
Items: 
Size: 716763 Color: 1
Size: 280856 Color: 0

Bin 748: 2397 of cap free
Amount of items: 2
Items: 
Size: 689518 Color: 1
Size: 308086 Color: 0

Bin 749: 2420 of cap free
Amount of items: 2
Items: 
Size: 546078 Color: 1
Size: 451503 Color: 0

Bin 750: 2427 of cap free
Amount of items: 2
Items: 
Size: 594275 Color: 0
Size: 403299 Color: 1

Bin 751: 2467 of cap free
Amount of items: 2
Items: 
Size: 633412 Color: 0
Size: 364122 Color: 1

Bin 752: 2473 of cap free
Amount of items: 2
Items: 
Size: 510208 Color: 1
Size: 487320 Color: 0

Bin 753: 2493 of cap free
Amount of items: 2
Items: 
Size: 638896 Color: 0
Size: 358612 Color: 1

Bin 754: 2517 of cap free
Amount of items: 2
Items: 
Size: 618038 Color: 1
Size: 379446 Color: 0

Bin 755: 2520 of cap free
Amount of items: 2
Items: 
Size: 746917 Color: 1
Size: 250564 Color: 0

Bin 756: 2537 of cap free
Amount of items: 2
Items: 
Size: 553494 Color: 0
Size: 443970 Color: 1

Bin 757: 2552 of cap free
Amount of items: 2
Items: 
Size: 637270 Color: 1
Size: 360179 Color: 0

Bin 758: 2571 of cap free
Amount of items: 2
Items: 
Size: 798133 Color: 1
Size: 199297 Color: 0

Bin 759: 2613 of cap free
Amount of items: 2
Items: 
Size: 734799 Color: 1
Size: 262589 Color: 0

Bin 760: 2629 of cap free
Amount of items: 2
Items: 
Size: 667526 Color: 0
Size: 329846 Color: 1

Bin 761: 2647 of cap free
Amount of items: 2
Items: 
Size: 701714 Color: 1
Size: 295640 Color: 0

Bin 762: 2710 of cap free
Amount of items: 2
Items: 
Size: 613482 Color: 1
Size: 383809 Color: 0

Bin 763: 2741 of cap free
Amount of items: 2
Items: 
Size: 678595 Color: 0
Size: 318665 Color: 1

Bin 764: 2753 of cap free
Amount of items: 2
Items: 
Size: 522693 Color: 0
Size: 474555 Color: 1

Bin 765: 2777 of cap free
Amount of items: 2
Items: 
Size: 732620 Color: 0
Size: 264604 Color: 1

Bin 766: 2783 of cap free
Amount of items: 2
Items: 
Size: 527238 Color: 0
Size: 469980 Color: 1

Bin 767: 2786 of cap free
Amount of items: 2
Items: 
Size: 598535 Color: 0
Size: 398680 Color: 1

Bin 768: 2796 of cap free
Amount of items: 2
Items: 
Size: 545836 Color: 1
Size: 451369 Color: 0

Bin 769: 2823 of cap free
Amount of items: 2
Items: 
Size: 543001 Color: 0
Size: 454177 Color: 1

Bin 770: 2846 of cap free
Amount of items: 2
Items: 
Size: 580094 Color: 1
Size: 417061 Color: 0

Bin 771: 2871 of cap free
Amount of items: 2
Items: 
Size: 537783 Color: 0
Size: 459347 Color: 1

Bin 772: 2878 of cap free
Amount of items: 2
Items: 
Size: 789251 Color: 1
Size: 207872 Color: 0

Bin 773: 2881 of cap free
Amount of items: 2
Items: 
Size: 764972 Color: 1
Size: 232148 Color: 0

Bin 774: 2908 of cap free
Amount of items: 2
Items: 
Size: 657711 Color: 1
Size: 339382 Color: 0

Bin 775: 2946 of cap free
Amount of items: 2
Items: 
Size: 797373 Color: 0
Size: 199682 Color: 1

Bin 776: 2959 of cap free
Amount of items: 2
Items: 
Size: 550337 Color: 0
Size: 446705 Color: 1

Bin 777: 2993 of cap free
Amount of items: 2
Items: 
Size: 584110 Color: 0
Size: 412898 Color: 1

Bin 778: 3015 of cap free
Amount of items: 2
Items: 
Size: 742356 Color: 0
Size: 254630 Color: 1

Bin 779: 3046 of cap free
Amount of items: 2
Items: 
Size: 711060 Color: 0
Size: 285895 Color: 1

Bin 780: 3055 of cap free
Amount of items: 2
Items: 
Size: 712726 Color: 1
Size: 284220 Color: 0

Bin 781: 3161 of cap free
Amount of items: 2
Items: 
Size: 692225 Color: 0
Size: 304615 Color: 1

Bin 782: 3187 of cap free
Amount of items: 2
Items: 
Size: 522414 Color: 0
Size: 474400 Color: 1

Bin 783: 3199 of cap free
Amount of items: 2
Items: 
Size: 746756 Color: 0
Size: 250046 Color: 1

Bin 784: 3211 of cap free
Amount of items: 2
Items: 
Size: 605653 Color: 0
Size: 391137 Color: 1

Bin 785: 3289 of cap free
Amount of items: 2
Items: 
Size: 574256 Color: 1
Size: 422456 Color: 0

Bin 786: 3289 of cap free
Amount of items: 2
Items: 
Size: 764909 Color: 1
Size: 231803 Color: 0

Bin 787: 3300 of cap free
Amount of items: 2
Items: 
Size: 532631 Color: 1
Size: 464070 Color: 0

Bin 788: 3357 of cap free
Amount of items: 2
Items: 
Size: 716316 Color: 1
Size: 280328 Color: 0

Bin 789: 3371 of cap free
Amount of items: 2
Items: 
Size: 501196 Color: 0
Size: 495434 Color: 1

Bin 790: 3408 of cap free
Amount of items: 2
Items: 
Size: 503272 Color: 1
Size: 493321 Color: 0

Bin 791: 3447 of cap free
Amount of items: 2
Items: 
Size: 584786 Color: 1
Size: 411768 Color: 0

Bin 792: 3455 of cap free
Amount of items: 2
Items: 
Size: 510710 Color: 0
Size: 485836 Color: 1

Bin 793: 3568 of cap free
Amount of items: 2
Items: 
Size: 763789 Color: 0
Size: 232644 Color: 1

Bin 794: 3590 of cap free
Amount of items: 2
Items: 
Size: 720395 Color: 1
Size: 276016 Color: 0

Bin 795: 3643 of cap free
Amount of items: 2
Items: 
Size: 702114 Color: 0
Size: 294244 Color: 1

Bin 796: 3647 of cap free
Amount of items: 2
Items: 
Size: 712252 Color: 1
Size: 284102 Color: 0

Bin 797: 3761 of cap free
Amount of items: 2
Items: 
Size: 710821 Color: 0
Size: 285419 Color: 1

Bin 798: 3837 of cap free
Amount of items: 2
Items: 
Size: 705372 Color: 1
Size: 290792 Color: 0

Bin 799: 3867 of cap free
Amount of items: 2
Items: 
Size: 510485 Color: 0
Size: 485649 Color: 1

Bin 800: 3884 of cap free
Amount of items: 2
Items: 
Size: 638712 Color: 0
Size: 357405 Color: 1

Bin 801: 3892 of cap free
Amount of items: 2
Items: 
Size: 741742 Color: 0
Size: 254367 Color: 1

Bin 802: 3912 of cap free
Amount of items: 2
Items: 
Size: 793306 Color: 1
Size: 202783 Color: 0

Bin 803: 3939 of cap free
Amount of items: 2
Items: 
Size: 687524 Color: 0
Size: 308538 Color: 1

Bin 804: 3996 of cap free
Amount of items: 2
Items: 
Size: 584430 Color: 1
Size: 411575 Color: 0

Bin 805: 4034 of cap free
Amount of items: 2
Items: 
Size: 783169 Color: 0
Size: 212798 Color: 1

Bin 806: 4040 of cap free
Amount of items: 2
Items: 
Size: 629575 Color: 1
Size: 366386 Color: 0

Bin 807: 4062 of cap free
Amount of items: 2
Items: 
Size: 602381 Color: 1
Size: 393558 Color: 0

Bin 808: 4073 of cap free
Amount of items: 2
Items: 
Size: 566699 Color: 1
Size: 429229 Color: 0

Bin 809: 4117 of cap free
Amount of items: 2
Items: 
Size: 591306 Color: 1
Size: 404578 Color: 0

Bin 810: 4162 of cap free
Amount of items: 2
Items: 
Size: 691577 Color: 0
Size: 304262 Color: 1

Bin 811: 4168 of cap free
Amount of items: 2
Items: 
Size: 502646 Color: 1
Size: 493187 Color: 0

Bin 812: 4194 of cap free
Amount of items: 2
Items: 
Size: 544606 Color: 1
Size: 451201 Color: 0

Bin 813: 4224 of cap free
Amount of items: 2
Items: 
Size: 510176 Color: 0
Size: 485601 Color: 1

Bin 814: 4225 of cap free
Amount of items: 2
Items: 
Size: 621666 Color: 0
Size: 374110 Color: 1

Bin 815: 4232 of cap free
Amount of items: 2
Items: 
Size: 719993 Color: 1
Size: 275776 Color: 0

Bin 816: 4297 of cap free
Amount of items: 2
Items: 
Size: 516680 Color: 0
Size: 479024 Color: 1

Bin 817: 4405 of cap free
Amount of items: 2
Items: 
Size: 741431 Color: 0
Size: 254165 Color: 1

Bin 818: 4407 of cap free
Amount of items: 2
Items: 
Size: 594029 Color: 0
Size: 401565 Color: 1

Bin 819: 4469 of cap free
Amount of items: 2
Items: 
Size: 727991 Color: 0
Size: 267541 Color: 1

Bin 820: 4488 of cap free
Amount of items: 2
Items: 
Size: 666203 Color: 0
Size: 329310 Color: 1

Bin 821: 4589 of cap free
Amount of items: 2
Items: 
Size: 756737 Color: 1
Size: 238675 Color: 0

Bin 822: 4594 of cap free
Amount of items: 2
Items: 
Size: 746660 Color: 0
Size: 248747 Color: 1

Bin 823: 4598 of cap free
Amount of items: 2
Items: 
Size: 782800 Color: 0
Size: 212603 Color: 1

Bin 824: 4616 of cap free
Amount of items: 2
Items: 
Size: 537139 Color: 0
Size: 458246 Color: 1

Bin 825: 4733 of cap free
Amount of items: 2
Items: 
Size: 782669 Color: 0
Size: 212599 Color: 1

Bin 826: 4790 of cap free
Amount of items: 2
Items: 
Size: 509722 Color: 0
Size: 485489 Color: 1

Bin 827: 4831 of cap free
Amount of items: 2
Items: 
Size: 795587 Color: 0
Size: 199583 Color: 1

Bin 828: 5094 of cap free
Amount of items: 2
Items: 
Size: 646701 Color: 0
Size: 348206 Color: 1

Bin 829: 5145 of cap free
Amount of items: 2
Items: 
Size: 536823 Color: 0
Size: 458033 Color: 1

Bin 830: 5202 of cap free
Amount of items: 2
Items: 
Size: 573664 Color: 1
Size: 421135 Color: 0

Bin 831: 5298 of cap free
Amount of items: 2
Items: 
Size: 583728 Color: 1
Size: 410975 Color: 0

Bin 832: 5442 of cap free
Amount of items: 2
Items: 
Size: 756052 Color: 1
Size: 238507 Color: 0

Bin 833: 5455 of cap free
Amount of items: 2
Items: 
Size: 749446 Color: 1
Size: 245100 Color: 0

Bin 834: 5536 of cap free
Amount of items: 2
Items: 
Size: 526898 Color: 0
Size: 467567 Color: 1

Bin 835: 5560 of cap free
Amount of items: 2
Items: 
Size: 646489 Color: 0
Size: 347952 Color: 1

Bin 836: 5786 of cap free
Amount of items: 2
Items: 
Size: 631735 Color: 0
Size: 362480 Color: 1

Bin 837: 5885 of cap free
Amount of items: 2
Items: 
Size: 557702 Color: 1
Size: 436414 Color: 0

Bin 838: 5938 of cap free
Amount of items: 2
Items: 
Size: 699385 Color: 1
Size: 294678 Color: 0

Bin 839: 5940 of cap free
Amount of items: 2
Items: 
Size: 508682 Color: 0
Size: 485379 Color: 1

Bin 840: 5983 of cap free
Amount of items: 2
Items: 
Size: 787867 Color: 0
Size: 206151 Color: 1

Bin 841: 6023 of cap free
Amount of items: 2
Items: 
Size: 612709 Color: 0
Size: 381269 Color: 1

Bin 842: 6253 of cap free
Amount of items: 2
Items: 
Size: 565706 Color: 1
Size: 428042 Color: 0

Bin 843: 6280 of cap free
Amount of items: 2
Items: 
Size: 502484 Color: 1
Size: 491237 Color: 0

Bin 844: 6288 of cap free
Amount of items: 2
Items: 
Size: 621539 Color: 0
Size: 372174 Color: 1

Bin 845: 6340 of cap free
Amount of items: 2
Items: 
Size: 605210 Color: 0
Size: 388451 Color: 1

Bin 846: 6344 of cap free
Amount of items: 2
Items: 
Size: 663928 Color: 1
Size: 329729 Color: 0

Bin 847: 6351 of cap free
Amount of items: 2
Items: 
Size: 602373 Color: 1
Size: 391277 Color: 0

Bin 848: 6471 of cap free
Amount of items: 2
Items: 
Size: 781286 Color: 0
Size: 212244 Color: 1

Bin 849: 6560 of cap free
Amount of items: 2
Items: 
Size: 724621 Color: 1
Size: 268820 Color: 0

Bin 850: 6566 of cap free
Amount of items: 2
Items: 
Size: 618020 Color: 1
Size: 375415 Color: 0

Bin 851: 6585 of cap free
Amount of items: 2
Items: 
Size: 771022 Color: 1
Size: 222394 Color: 0

Bin 852: 6637 of cap free
Amount of items: 2
Items: 
Size: 665245 Color: 0
Size: 328119 Color: 1

Bin 853: 6832 of cap free
Amount of items: 2
Items: 
Size: 559759 Color: 0
Size: 433410 Color: 1

Bin 854: 6883 of cap free
Amount of items: 2
Items: 
Size: 709366 Color: 1
Size: 283752 Color: 0

Bin 855: 7122 of cap free
Amount of items: 2
Items: 
Size: 754413 Color: 0
Size: 238466 Color: 1

Bin 856: 7211 of cap free
Amount of items: 2
Items: 
Size: 734392 Color: 1
Size: 258398 Color: 0

Bin 857: 7217 of cap free
Amount of items: 2
Items: 
Size: 557669 Color: 1
Size: 435115 Color: 0

Bin 858: 7289 of cap free
Amount of items: 2
Items: 
Size: 576789 Color: 0
Size: 415923 Color: 1

Bin 859: 7495 of cap free
Amount of items: 2
Items: 
Size: 593889 Color: 0
Size: 398617 Color: 1

Bin 860: 7666 of cap free
Amount of items: 2
Items: 
Size: 793262 Color: 1
Size: 199073 Color: 0

Bin 861: 7839 of cap free
Amount of items: 2
Items: 
Size: 620472 Color: 0
Size: 371690 Color: 1

Bin 862: 7973 of cap free
Amount of items: 2
Items: 
Size: 620349 Color: 0
Size: 371679 Color: 1

Bin 863: 7975 of cap free
Amount of items: 2
Items: 
Size: 530675 Color: 1
Size: 461351 Color: 0

Bin 864: 8118 of cap free
Amount of items: 2
Items: 
Size: 557105 Color: 1
Size: 434778 Color: 0

Bin 865: 8228 of cap free
Amount of items: 2
Items: 
Size: 603633 Color: 0
Size: 388140 Color: 1

Bin 866: 8415 of cap free
Amount of items: 2
Items: 
Size: 671887 Color: 1
Size: 319699 Color: 0

Bin 867: 8667 of cap free
Amount of items: 2
Items: 
Size: 600386 Color: 1
Size: 390948 Color: 0

Bin 868: 8757 of cap free
Amount of items: 2
Items: 
Size: 746382 Color: 1
Size: 244862 Color: 0

Bin 869: 8834 of cap free
Amount of items: 2
Items: 
Size: 752979 Color: 0
Size: 238188 Color: 1

Bin 870: 8889 of cap free
Amount of items: 2
Items: 
Size: 770935 Color: 1
Size: 220177 Color: 0

Bin 871: 8907 of cap free
Amount of items: 2
Items: 
Size: 620125 Color: 0
Size: 370969 Color: 1

Bin 872: 8950 of cap free
Amount of items: 2
Items: 
Size: 556713 Color: 1
Size: 434338 Color: 0

Bin 873: 8957 of cap free
Amount of items: 2
Items: 
Size: 633636 Color: 1
Size: 357408 Color: 0

Bin 874: 9447 of cap free
Amount of items: 2
Items: 
Size: 526169 Color: 0
Size: 464385 Color: 1

Bin 875: 9701 of cap free
Amount of items: 2
Items: 
Size: 793052 Color: 1
Size: 197248 Color: 0

Bin 876: 9750 of cap free
Amount of items: 2
Items: 
Size: 619648 Color: 0
Size: 370603 Color: 1

Bin 877: 9828 of cap free
Amount of items: 2
Items: 
Size: 526136 Color: 0
Size: 464037 Color: 1

Bin 878: 9948 of cap free
Amount of items: 2
Items: 
Size: 745616 Color: 1
Size: 244437 Color: 0

Bin 879: 10022 of cap free
Amount of items: 2
Items: 
Size: 580008 Color: 1
Size: 409971 Color: 0

Bin 880: 10040 of cap free
Amount of items: 2
Items: 
Size: 619508 Color: 0
Size: 370453 Color: 1

Bin 881: 10117 of cap free
Amount of items: 2
Items: 
Size: 591323 Color: 0
Size: 398561 Color: 1

Bin 882: 10566 of cap free
Amount of items: 2
Items: 
Size: 745418 Color: 1
Size: 244017 Color: 0

Bin 883: 11178 of cap free
Amount of items: 2
Items: 
Size: 559692 Color: 0
Size: 429131 Color: 1

Bin 884: 11244 of cap free
Amount of items: 2
Items: 
Size: 528873 Color: 1
Size: 459884 Color: 0

Bin 885: 11271 of cap free
Amount of items: 2
Items: 
Size: 579284 Color: 1
Size: 409446 Color: 0

Bin 886: 11434 of cap free
Amount of items: 2
Items: 
Size: 528796 Color: 1
Size: 459771 Color: 0

Bin 887: 11593 of cap free
Amount of items: 2
Items: 
Size: 579052 Color: 1
Size: 409356 Color: 0

Bin 888: 11619 of cap free
Amount of items: 2
Items: 
Size: 528723 Color: 1
Size: 459659 Color: 0

Bin 889: 11792 of cap free
Amount of items: 2
Items: 
Size: 685711 Color: 0
Size: 302498 Color: 1

Bin 890: 11979 of cap free
Amount of items: 2
Items: 
Size: 662147 Color: 0
Size: 325875 Color: 1

Bin 891: 12008 of cap free
Amount of items: 2
Items: 
Size: 768322 Color: 1
Size: 219671 Color: 0

Bin 892: 12124 of cap free
Amount of items: 2
Items: 
Size: 578905 Color: 1
Size: 408972 Color: 0

Bin 893: 12382 of cap free
Amount of items: 2
Items: 
Size: 661817 Color: 0
Size: 325802 Color: 1

Bin 894: 12780 of cap free
Amount of items: 2
Items: 
Size: 600318 Color: 1
Size: 386903 Color: 0

Bin 895: 13269 of cap free
Amount of items: 2
Items: 
Size: 644675 Color: 0
Size: 342057 Color: 1

Bin 896: 13470 of cap free
Amount of items: 2
Items: 
Size: 527599 Color: 1
Size: 458932 Color: 0

Bin 897: 13472 of cap free
Amount of items: 2
Items: 
Size: 501819 Color: 1
Size: 484710 Color: 0

Bin 898: 14515 of cap free
Amount of items: 2
Items: 
Size: 613206 Color: 1
Size: 372280 Color: 0

Bin 899: 15064 of cap free
Amount of items: 2
Items: 
Size: 574765 Color: 0
Size: 410172 Color: 1

Bin 900: 15264 of cap free
Amount of items: 2
Items: 
Size: 707535 Color: 0
Size: 277202 Color: 1

Bin 901: 15709 of cap free
Amount of items: 2
Items: 
Size: 556361 Color: 0
Size: 427931 Color: 1

Bin 902: 15951 of cap free
Amount of items: 2
Items: 
Size: 661612 Color: 0
Size: 322438 Color: 1

Bin 903: 16140 of cap free
Amount of items: 2
Items: 
Size: 779338 Color: 0
Size: 204523 Color: 1

Bin 904: 16156 of cap free
Amount of items: 2
Items: 
Size: 573915 Color: 0
Size: 409930 Color: 1

Bin 905: 16245 of cap free
Amount of items: 2
Items: 
Size: 526324 Color: 1
Size: 457432 Color: 0

Bin 906: 16981 of cap free
Amount of items: 2
Items: 
Size: 573521 Color: 0
Size: 409499 Color: 1

Bin 907: 17075 of cap free
Amount of items: 2
Items: 
Size: 787261 Color: 1
Size: 195665 Color: 0

Bin 908: 17713 of cap free
Amount of items: 2
Items: 
Size: 573577 Color: 1
Size: 408711 Color: 0

Bin 909: 18911 of cap free
Amount of items: 2
Items: 
Size: 525313 Color: 1
Size: 455777 Color: 0

Bin 910: 20784 of cap free
Amount of items: 2
Items: 
Size: 786474 Color: 1
Size: 192743 Color: 0

Bin 911: 21568 of cap free
Amount of items: 2
Items: 
Size: 779263 Color: 0
Size: 199170 Color: 1

Bin 912: 25391 of cap free
Amount of items: 2
Items: 
Size: 701943 Color: 0
Size: 272667 Color: 1

Bin 913: 25763 of cap free
Amount of items: 2
Items: 
Size: 611959 Color: 0
Size: 362279 Color: 1

Bin 914: 29986 of cap free
Amount of items: 2
Items: 
Size: 655418 Color: 1
Size: 314597 Color: 0

Bin 915: 31619 of cap free
Amount of items: 2
Items: 
Size: 504801 Color: 0
Size: 463581 Color: 1

Bin 916: 37756 of cap free
Amount of items: 2
Items: 
Size: 498812 Color: 0
Size: 463433 Color: 1

Bin 917: 39095 of cap free
Amount of items: 2
Items: 
Size: 556379 Color: 1
Size: 404527 Color: 0

Bin 918: 41068 of cap free
Amount of items: 2
Items: 
Size: 556090 Color: 1
Size: 402843 Color: 0

Bin 919: 43048 of cap free
Amount of items: 2
Items: 
Size: 777711 Color: 0
Size: 179242 Color: 1

Bin 920: 49612 of cap free
Amount of items: 2
Items: 
Size: 599132 Color: 1
Size: 351257 Color: 0

Bin 921: 51467 of cap free
Amount of items: 2
Items: 
Size: 774956 Color: 0
Size: 173578 Color: 1

Total size: 919136191
Total free space: 1864730

