Capicity Bin: 7552
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 15
Size: 3146 Color: 7
Size: 144 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4686 Color: 9
Size: 2742 Color: 8
Size: 124 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5411 Color: 5
Size: 1395 Color: 3
Size: 746 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 5
Size: 2004 Color: 6
Size: 128 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5492 Color: 17
Size: 1812 Color: 9
Size: 248 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 15
Size: 1642 Color: 5
Size: 220 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 17
Size: 1580 Color: 18
Size: 240 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5858 Color: 2
Size: 1314 Color: 8
Size: 380 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 9
Size: 1204 Color: 12
Size: 416 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 7
Size: 1292 Color: 15
Size: 256 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6060 Color: 5
Size: 974 Color: 16
Size: 518 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6152 Color: 9
Size: 1176 Color: 7
Size: 224 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 17
Size: 1067 Color: 9
Size: 324 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 5
Size: 1151 Color: 7
Size: 188 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 17
Size: 1174 Color: 8
Size: 148 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6321 Color: 16
Size: 1007 Color: 12
Size: 224 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 16
Size: 802 Color: 15
Size: 416 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 1
Size: 1012 Color: 2
Size: 200 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 18
Size: 870 Color: 5
Size: 328 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 8
Size: 1043 Color: 9
Size: 154 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 1
Size: 717 Color: 4
Size: 456 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 2
Size: 999 Color: 10
Size: 110 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 11
Size: 924 Color: 3
Size: 232 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 19
Size: 999 Color: 8
Size: 142 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6444 Color: 2
Size: 684 Color: 1
Size: 424 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6539 Color: 13
Size: 871 Color: 19
Size: 142 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6541 Color: 7
Size: 793 Color: 3
Size: 218 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 2
Size: 751 Color: 9
Size: 200 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 3
Size: 624 Color: 5
Size: 356 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 0
Size: 804 Color: 18
Size: 160 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6592 Color: 11
Size: 608 Color: 2
Size: 352 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 2
Size: 698 Color: 13
Size: 196 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 13
Size: 638 Color: 10
Size: 248 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 1
Size: 724 Color: 6
Size: 136 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6740 Color: 11
Size: 554 Color: 15
Size: 258 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6754 Color: 4
Size: 400 Color: 2
Size: 398 Color: 6

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 4
Size: 1785 Color: 12
Size: 794 Color: 12

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5148 Color: 1
Size: 2255 Color: 12
Size: 148 Color: 18

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 13
Size: 2078 Color: 12
Size: 174 Color: 17

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 5
Size: 1117 Color: 2
Size: 862 Color: 3

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5675 Color: 5
Size: 1076 Color: 19
Size: 800 Color: 15

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5799 Color: 7
Size: 1554 Color: 4
Size: 198 Color: 1

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6107 Color: 6
Size: 742 Color: 12
Size: 702 Color: 0

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6137 Color: 6
Size: 1414 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 19
Size: 1148 Color: 9
Size: 230 Color: 2

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6467 Color: 18
Size: 556 Color: 19
Size: 528 Color: 11

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6625 Color: 14
Size: 666 Color: 10
Size: 260 Color: 13

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6693 Color: 8
Size: 624 Color: 2
Size: 234 Color: 17

Bin 49: 2 of cap free
Amount of items: 6
Items: 
Size: 3786 Color: 2
Size: 1258 Color: 19
Size: 1048 Color: 9
Size: 1002 Color: 19
Size: 296 Color: 7
Size: 160 Color: 13

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 4214 Color: 4
Size: 3142 Color: 13
Size: 194 Color: 19

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 2
Size: 2159 Color: 15
Size: 228 Color: 5

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 5586 Color: 18
Size: 1964 Color: 8

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 5772 Color: 2
Size: 1778 Color: 8

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 14
Size: 1318 Color: 5
Size: 292 Color: 6

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 5995 Color: 1
Size: 1555 Color: 12

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6345 Color: 0
Size: 1205 Color: 15

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6507 Color: 8
Size: 1043 Color: 3

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 14
Size: 905 Color: 12

Bin 59: 3 of cap free
Amount of items: 7
Items: 
Size: 3777 Color: 13
Size: 858 Color: 6
Size: 654 Color: 6
Size: 652 Color: 19
Size: 628 Color: 19
Size: 504 Color: 4
Size: 476 Color: 15

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 4847 Color: 13
Size: 2702 Color: 5

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 4
Size: 2298 Color: 3

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6015 Color: 1
Size: 1160 Color: 2
Size: 374 Color: 13

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 13
Size: 1137 Color: 0
Size: 296 Color: 2

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 14
Size: 1281 Color: 19

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6419 Color: 7
Size: 1130 Color: 19

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 5
Size: 945 Color: 13

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 10
Size: 2390 Color: 2
Size: 122 Color: 6

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 12
Size: 560 Color: 2
Size: 488 Color: 0

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 4310 Color: 6
Size: 3237 Color: 0

Bin 70: 5 of cap free
Amount of items: 3
Items: 
Size: 4437 Color: 6
Size: 2660 Color: 10
Size: 450 Color: 6

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 5951 Color: 7
Size: 1432 Color: 3
Size: 164 Color: 2

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 3
Size: 1161 Color: 5

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6790 Color: 1
Size: 757 Color: 13

Bin 74: 6 of cap free
Amount of items: 17
Items: 
Size: 628 Color: 12
Size: 628 Color: 7
Size: 548 Color: 5
Size: 540 Color: 10
Size: 500 Color: 10
Size: 480 Color: 2
Size: 448 Color: 12
Size: 430 Color: 8
Size: 414 Color: 11
Size: 412 Color: 16
Size: 412 Color: 15
Size: 392 Color: 8
Size: 382 Color: 2
Size: 336 Color: 19
Size: 336 Color: 3
Size: 332 Color: 4
Size: 328 Color: 3

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6198 Color: 13
Size: 1348 Color: 17

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 18
Size: 1081 Color: 13

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 10
Size: 1020 Color: 3

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 18
Size: 979 Color: 17

Bin 79: 7 of cap free
Amount of items: 3
Items: 
Size: 4164 Color: 10
Size: 2781 Color: 13
Size: 600 Color: 3

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 5554 Color: 4
Size: 1991 Color: 15

Bin 81: 7 of cap free
Amount of items: 2
Items: 
Size: 6146 Color: 10
Size: 1399 Color: 0

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 6189 Color: 17
Size: 1356 Color: 9

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 6301 Color: 2
Size: 1244 Color: 10

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 11
Size: 773 Color: 13

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 4524 Color: 11
Size: 3020 Color: 13

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 4970 Color: 11
Size: 2502 Color: 14
Size: 72 Color: 19

Bin 87: 9 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 16
Size: 2079 Color: 2
Size: 268 Color: 6

Bin 88: 10 of cap free
Amount of items: 3
Items: 
Size: 4217 Color: 12
Size: 3145 Color: 11
Size: 180 Color: 10

Bin 89: 10 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 4
Size: 1524 Color: 16
Size: 44 Color: 15

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 18
Size: 1665 Color: 2
Size: 320 Color: 6

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 6056 Color: 14
Size: 1484 Color: 5

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 5660 Color: 0
Size: 1879 Color: 7

Bin 93: 13 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 7
Size: 1461 Color: 16
Size: 48 Color: 17

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 6718 Color: 18
Size: 821 Color: 12

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 5262 Color: 5
Size: 2276 Color: 1

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 6653 Color: 1
Size: 885 Color: 19

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 6695 Color: 7
Size: 843 Color: 18

Bin 98: 16 of cap free
Amount of items: 2
Items: 
Size: 6237 Color: 8
Size: 1299 Color: 16

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 6434 Color: 11
Size: 1102 Color: 8

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 6518 Color: 3
Size: 1018 Color: 14

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 5
Size: 934 Color: 16

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 5789 Color: 14
Size: 1746 Color: 10

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 16
Size: 884 Color: 13

Bin 104: 18 of cap free
Amount of items: 2
Items: 
Size: 6714 Color: 14
Size: 820 Color: 0

Bin 105: 20 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 7
Size: 2782 Color: 8
Size: 264 Color: 2

Bin 106: 21 of cap free
Amount of items: 3
Items: 
Size: 3780 Color: 14
Size: 2654 Color: 18
Size: 1097 Color: 4

Bin 107: 21 of cap free
Amount of items: 2
Items: 
Size: 5879 Color: 6
Size: 1652 Color: 18

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 6046 Color: 18
Size: 1482 Color: 0

Bin 109: 27 of cap free
Amount of items: 2
Items: 
Size: 5927 Color: 11
Size: 1598 Color: 0

Bin 110: 28 of cap free
Amount of items: 2
Items: 
Size: 6770 Color: 5
Size: 754 Color: 4

Bin 111: 29 of cap free
Amount of items: 2
Items: 
Size: 6342 Color: 12
Size: 1181 Color: 10

Bin 112: 30 of cap free
Amount of items: 2
Items: 
Size: 5422 Color: 10
Size: 2100 Color: 3

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 6594 Color: 11
Size: 928 Color: 9

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 5062 Color: 3
Size: 2458 Color: 10

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 6510 Color: 0
Size: 1010 Color: 14

Bin 116: 36 of cap free
Amount of items: 2
Items: 
Size: 4550 Color: 13
Size: 2966 Color: 14

Bin 117: 37 of cap free
Amount of items: 2
Items: 
Size: 3779 Color: 18
Size: 3736 Color: 10

Bin 118: 37 of cap free
Amount of items: 2
Items: 
Size: 6180 Color: 10
Size: 1335 Color: 7

Bin 119: 38 of cap free
Amount of items: 35
Items: 
Size: 312 Color: 5
Size: 312 Color: 3
Size: 308 Color: 5
Size: 292 Color: 14
Size: 280 Color: 12
Size: 278 Color: 11
Size: 266 Color: 3
Size: 264 Color: 13
Size: 256 Color: 1
Size: 252 Color: 19
Size: 232 Color: 6
Size: 226 Color: 14
Size: 226 Color: 1
Size: 224 Color: 2
Size: 222 Color: 7
Size: 208 Color: 18
Size: 208 Color: 9
Size: 208 Color: 7
Size: 208 Color: 5
Size: 200 Color: 11
Size: 200 Color: 7
Size: 192 Color: 11
Size: 184 Color: 14
Size: 184 Color: 12
Size: 172 Color: 17
Size: 172 Color: 6
Size: 168 Color: 10
Size: 168 Color: 9
Size: 168 Color: 0
Size: 160 Color: 0
Size: 158 Color: 18
Size: 156 Color: 16
Size: 152 Color: 0
Size: 150 Color: 8
Size: 148 Color: 13

Bin 120: 40 of cap free
Amount of items: 2
Items: 
Size: 4364 Color: 1
Size: 3148 Color: 6

Bin 121: 47 of cap free
Amount of items: 2
Items: 
Size: 4193 Color: 2
Size: 3312 Color: 10

Bin 122: 48 of cap free
Amount of items: 3
Items: 
Size: 4798 Color: 11
Size: 1910 Color: 0
Size: 796 Color: 15

Bin 123: 50 of cap free
Amount of items: 2
Items: 
Size: 5059 Color: 11
Size: 2443 Color: 7

Bin 124: 50 of cap free
Amount of items: 2
Items: 
Size: 5778 Color: 5
Size: 1724 Color: 19

Bin 125: 53 of cap free
Amount of items: 3
Items: 
Size: 3778 Color: 9
Size: 2156 Color: 9
Size: 1565 Color: 2

Bin 126: 53 of cap free
Amount of items: 3
Items: 
Size: 4828 Color: 15
Size: 2597 Color: 7
Size: 74 Color: 6

Bin 127: 55 of cap free
Amount of items: 2
Items: 
Size: 5850 Color: 19
Size: 1647 Color: 16

Bin 128: 56 of cap free
Amount of items: 2
Items: 
Size: 5577 Color: 18
Size: 1919 Color: 7

Bin 129: 65 of cap free
Amount of items: 2
Items: 
Size: 4963 Color: 4
Size: 2524 Color: 14

Bin 130: 87 of cap free
Amount of items: 2
Items: 
Size: 4318 Color: 2
Size: 3147 Color: 14

Bin 131: 97 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 11
Size: 2808 Color: 3
Size: 715 Color: 7

Bin 132: 103 of cap free
Amount of items: 2
Items: 
Size: 4621 Color: 3
Size: 2828 Color: 4

Bin 133: 5878 of cap free
Amount of items: 13
Items: 
Size: 156 Color: 10
Size: 150 Color: 0
Size: 136 Color: 14
Size: 136 Color: 7
Size: 136 Color: 2
Size: 132 Color: 15
Size: 128 Color: 18
Size: 128 Color: 13
Size: 128 Color: 6
Size: 124 Color: 8
Size: 112 Color: 18
Size: 104 Color: 16
Size: 104 Color: 16

Total size: 996864
Total free space: 7552

