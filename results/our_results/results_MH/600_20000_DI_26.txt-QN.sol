Capicity Bin: 16032
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8034 Color: 414
Size: 6666 Color: 392
Size: 1332 Color: 196

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11051 Color: 452
Size: 4151 Color: 353
Size: 830 Color: 161

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 485
Size: 1997 Color: 262
Size: 1553 Color: 221

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12524 Color: 486
Size: 3380 Color: 333
Size: 128 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 500
Size: 2092 Color: 273
Size: 1088 Color: 182

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 501
Size: 1773 Color: 244
Size: 1404 Color: 206

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13009 Color: 504
Size: 2255 Color: 284
Size: 768 Color: 154

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13213 Color: 512
Size: 2521 Color: 301
Size: 298 Color: 33

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 513
Size: 2528 Color: 302
Size: 284 Color: 26

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13311 Color: 518
Size: 2351 Color: 293
Size: 370 Color: 67

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 519
Size: 1784 Color: 246
Size: 928 Color: 173

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13386 Color: 524
Size: 2348 Color: 292
Size: 298 Color: 32

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 526
Size: 2316 Color: 290
Size: 280 Color: 24

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13521 Color: 529
Size: 2093 Color: 274
Size: 418 Color: 86

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13591 Color: 534
Size: 2229 Color: 282
Size: 212 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 535
Size: 1428 Color: 210
Size: 992 Color: 177

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 537
Size: 1694 Color: 236
Size: 712 Color: 144

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 538
Size: 2004 Color: 263
Size: 392 Color: 77

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13637 Color: 539
Size: 1731 Color: 239
Size: 664 Color: 138

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13689 Color: 541
Size: 1953 Color: 260
Size: 390 Color: 76

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 543
Size: 1884 Color: 253
Size: 428 Color: 87

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13873 Color: 553
Size: 1615 Color: 230
Size: 544 Color: 120

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 555
Size: 1416 Color: 208
Size: 724 Color: 146

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 556
Size: 1556 Color: 222
Size: 582 Color: 127

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 557
Size: 1896 Color: 255
Size: 240 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13905 Color: 558
Size: 1663 Color: 233
Size: 464 Color: 101

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13910 Color: 559
Size: 1770 Color: 242
Size: 352 Color: 58

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 563
Size: 1771 Color: 243
Size: 304 Color: 37

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14037 Color: 568
Size: 1501 Color: 216
Size: 494 Color: 107

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14053 Color: 570
Size: 1611 Color: 229
Size: 368 Color: 66

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 571
Size: 1626 Color: 231
Size: 344 Color: 54

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14095 Color: 573
Size: 1537 Color: 219
Size: 400 Color: 79

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14107 Color: 575
Size: 1601 Color: 225
Size: 324 Color: 46

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 576
Size: 964 Color: 176
Size: 960 Color: 175

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14150 Color: 579
Size: 1328 Color: 194
Size: 554 Color: 122

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 580
Size: 1560 Color: 223
Size: 304 Color: 35

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14169 Color: 581
Size: 1497 Color: 215
Size: 366 Color: 65

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 583
Size: 1542 Color: 220
Size: 304 Color: 36

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14288 Color: 589
Size: 1528 Color: 218
Size: 216 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 592
Size: 1068 Color: 181
Size: 640 Color: 136

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 594
Size: 1104 Color: 183
Size: 588 Color: 129

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 596
Size: 1156 Color: 187
Size: 520 Color: 114

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 598
Size: 1328 Color: 195
Size: 344 Color: 53

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14378 Color: 599
Size: 1334 Color: 202
Size: 320 Color: 43

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 600
Size: 1332 Color: 197
Size: 310 Color: 39

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 9783 Color: 430
Size: 6248 Color: 389

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 469
Size: 4099 Color: 350

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 12306 Color: 479
Size: 2891 Color: 315
Size: 834 Color: 164

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 12357 Color: 482
Size: 3268 Color: 328
Size: 406 Color: 82

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 12533 Color: 487
Size: 3082 Color: 322
Size: 416 Color: 84

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 13041 Color: 506
Size: 1608 Color: 228
Size: 1382 Color: 204

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 521
Size: 2675 Color: 307

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 523
Size: 2649 Color: 305

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 13623 Color: 536
Size: 2408 Color: 295

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 561
Size: 2087 Color: 272

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 14025 Color: 567
Size: 2006 Color: 264

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 14233 Color: 587
Size: 1798 Color: 248

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 597
Size: 1673 Color: 234

Bin 59: 2 of cap free
Amount of items: 4
Items: 
Size: 11075 Color: 454
Size: 4411 Color: 363
Size: 272 Color: 21
Size: 272 Color: 20

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 11442 Color: 459
Size: 4348 Color: 361
Size: 240 Color: 15

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 12800 Color: 497
Size: 1660 Color: 232
Size: 1570 Color: 224

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 12823 Color: 499
Size: 3207 Color: 325

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 13283 Color: 516
Size: 2747 Color: 310

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 540
Size: 2390 Color: 294

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 548
Size: 2264 Color: 286

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 560
Size: 2101 Color: 275

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 562
Size: 2082 Color: 271

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 14290 Color: 590
Size: 1740 Color: 240

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 593
Size: 1704 Color: 237

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 595
Size: 1686 Color: 235

Bin 71: 3 of cap free
Amount of items: 9
Items: 
Size: 8021 Color: 408
Size: 1334 Color: 201
Size: 1334 Color: 200
Size: 1332 Color: 199
Size: 1332 Color: 198
Size: 1328 Color: 193
Size: 676 Color: 142
Size: 336 Color: 51
Size: 336 Color: 50

Bin 72: 3 of cap free
Amount of items: 5
Items: 
Size: 8280 Color: 416
Size: 6673 Color: 394
Size: 444 Color: 93
Size: 320 Color: 41
Size: 312 Color: 40

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 11043 Color: 451
Size: 4122 Color: 351
Size: 864 Color: 167

Bin 74: 3 of cap free
Amount of items: 2
Items: 
Size: 12185 Color: 477
Size: 3844 Color: 345

Bin 75: 3 of cap free
Amount of items: 2
Items: 
Size: 13513 Color: 528
Size: 2516 Color: 300

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 13765 Color: 547
Size: 2264 Color: 285

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 586
Size: 1801 Color: 249

Bin 78: 4 of cap free
Amount of items: 9
Items: 
Size: 8020 Color: 407
Size: 1280 Color: 192
Size: 1248 Color: 191
Size: 1200 Color: 190
Size: 1188 Color: 189
Size: 1188 Color: 188
Size: 1152 Color: 185
Size: 408 Color: 83
Size: 344 Color: 52

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 11115 Color: 456
Size: 4641 Color: 366
Size: 272 Color: 18

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 13020 Color: 505
Size: 3008 Color: 319

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 13166 Color: 511
Size: 2862 Color: 314

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 569
Size: 1984 Color: 261

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 14111 Color: 577
Size: 1917 Color: 257

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 490
Size: 3411 Color: 336

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 12705 Color: 493
Size: 3322 Color: 331

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 13065 Color: 507
Size: 2962 Color: 318

Bin 87: 5 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 554
Size: 2149 Color: 276

Bin 88: 5 of cap free
Amount of items: 2
Items: 
Size: 13992 Color: 565
Size: 2035 Color: 268

Bin 89: 5 of cap free
Amount of items: 2
Items: 
Size: 14099 Color: 574
Size: 1928 Color: 258

Bin 90: 5 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 578
Size: 1907 Color: 256

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 14216 Color: 585
Size: 1811 Color: 250

Bin 92: 6 of cap free
Amount of items: 2
Items: 
Size: 10245 Color: 436
Size: 5781 Color: 382

Bin 93: 6 of cap free
Amount of items: 2
Items: 
Size: 14302 Color: 591
Size: 1724 Color: 238

Bin 94: 7 of cap free
Amount of items: 2
Items: 
Size: 12737 Color: 495
Size: 3288 Color: 330

Bin 95: 7 of cap free
Amount of items: 2
Items: 
Size: 13578 Color: 533
Size: 2447 Color: 296

Bin 96: 7 of cap free
Amount of items: 2
Items: 
Size: 13702 Color: 542
Size: 2323 Color: 291

Bin 97: 7 of cap free
Amount of items: 2
Items: 
Size: 13861 Color: 552
Size: 2164 Color: 278

Bin 98: 7 of cap free
Amount of items: 2
Items: 
Size: 14172 Color: 582
Size: 1853 Color: 252

Bin 99: 7 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 588
Size: 1788 Color: 247

Bin 100: 8 of cap free
Amount of items: 5
Items: 
Size: 8029 Color: 413
Size: 6671 Color: 393
Size: 682 Color: 143
Size: 322 Color: 44
Size: 320 Color: 42

Bin 101: 8 of cap free
Amount of items: 5
Items: 
Size: 9480 Color: 425
Size: 2206 Color: 280
Size: 2160 Color: 277
Size: 1890 Color: 254
Size: 288 Color: 30

Bin 102: 8 of cap free
Amount of items: 2
Items: 
Size: 10923 Color: 446
Size: 5101 Color: 373

Bin 103: 8 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 532
Size: 2462 Color: 297

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 13733 Color: 545
Size: 2291 Color: 289

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 572
Size: 1942 Color: 259

Bin 106: 9 of cap free
Amount of items: 2
Items: 
Size: 11019 Color: 449
Size: 5004 Color: 371

Bin 107: 9 of cap free
Amount of items: 3
Items: 
Size: 12338 Color: 481
Size: 3085 Color: 323
Size: 600 Color: 130

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 13455 Color: 527
Size: 2568 Color: 303

Bin 109: 10 of cap free
Amount of items: 3
Items: 
Size: 12046 Color: 473
Size: 3800 Color: 342
Size: 176 Color: 2

Bin 110: 10 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 484
Size: 3582 Color: 338

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 12810 Color: 498
Size: 3212 Color: 326

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 502
Size: 3112 Color: 324

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 13529 Color: 530
Size: 2493 Color: 299

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 544
Size: 2290 Color: 288

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 14002 Color: 566
Size: 2020 Color: 267

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 14189 Color: 584
Size: 1833 Color: 251

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 12396 Color: 483
Size: 3625 Color: 339

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 12636 Color: 491
Size: 3385 Color: 334

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 13097 Color: 509
Size: 2924 Color: 317

Bin 120: 13 of cap free
Amount of items: 2
Items: 
Size: 12248 Color: 478
Size: 3771 Color: 340

Bin 121: 13 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 550
Size: 2210 Color: 281

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 13964 Color: 564
Size: 2055 Color: 270

Bin 123: 14 of cap free
Amount of items: 3
Items: 
Size: 11683 Color: 465
Size: 4131 Color: 352
Size: 204 Color: 6

Bin 124: 14 of cap free
Amount of items: 2
Items: 
Size: 12680 Color: 492
Size: 3338 Color: 332

Bin 125: 14 of cap free
Amount of items: 2
Items: 
Size: 13245 Color: 514
Size: 2773 Color: 312

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 525
Size: 2602 Color: 304

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 468
Size: 4089 Color: 349

Bin 128: 15 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 531
Size: 2473 Color: 298

Bin 129: 15 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 551
Size: 2184 Color: 279

Bin 130: 16 of cap free
Amount of items: 3
Items: 
Size: 9824 Color: 431
Size: 5904 Color: 384
Size: 288 Color: 29

Bin 131: 16 of cap free
Amount of items: 2
Items: 
Size: 10344 Color: 438
Size: 5672 Color: 380

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 13260 Color: 515
Size: 2756 Color: 311

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 13780 Color: 549
Size: 2236 Color: 283

Bin 134: 17 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 470
Size: 3884 Color: 346
Size: 192 Color: 4

Bin 135: 17 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 503
Size: 3063 Color: 321

Bin 136: 18 of cap free
Amount of items: 2
Items: 
Size: 13286 Color: 517
Size: 2728 Color: 309

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 546
Size: 2269 Color: 287

Bin 138: 19 of cap free
Amount of items: 2
Items: 
Size: 10662 Color: 441
Size: 5351 Color: 376

Bin 139: 19 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 480
Size: 3036 Color: 320
Size: 646 Color: 137

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 13327 Color: 520
Size: 2686 Color: 308

Bin 141: 21 of cap free
Amount of items: 2
Items: 
Size: 13359 Color: 522
Size: 2652 Color: 306

Bin 142: 23 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 496
Size: 3233 Color: 327

Bin 143: 25 of cap free
Amount of items: 2
Items: 
Size: 10741 Color: 443
Size: 5266 Color: 375

Bin 144: 26 of cap free
Amount of items: 2
Items: 
Size: 12180 Color: 476
Size: 3826 Color: 344

Bin 145: 28 of cap free
Amount of items: 2
Items: 
Size: 10216 Color: 435
Size: 5788 Color: 383

Bin 146: 28 of cap free
Amount of items: 4
Items: 
Size: 11064 Color: 453
Size: 4388 Color: 362
Size: 280 Color: 23
Size: 272 Color: 22

Bin 147: 28 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 474
Size: 3804 Color: 343
Size: 96 Color: 0

Bin 148: 28 of cap free
Amount of items: 2
Items: 
Size: 12732 Color: 494
Size: 3272 Color: 329

Bin 149: 30 of cap free
Amount of items: 2
Items: 
Size: 12602 Color: 489
Size: 3400 Color: 335

Bin 150: 32 of cap free
Amount of items: 2
Items: 
Size: 13144 Color: 510
Size: 2856 Color: 313

Bin 151: 33 of cap free
Amount of items: 2
Items: 
Size: 10463 Color: 440
Size: 5536 Color: 379

Bin 152: 33 of cap free
Amount of items: 2
Items: 
Size: 10955 Color: 447
Size: 5044 Color: 372

Bin 153: 33 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 508
Size: 2917 Color: 316

Bin 154: 35 of cap free
Amount of items: 2
Items: 
Size: 12565 Color: 488
Size: 3432 Color: 337

Bin 155: 36 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 448
Size: 4744 Color: 367
Size: 284 Color: 25

Bin 156: 37 of cap free
Amount of items: 2
Items: 
Size: 11736 Color: 466
Size: 4259 Color: 360

Bin 157: 37 of cap free
Amount of items: 2
Items: 
Size: 11971 Color: 471
Size: 4024 Color: 348

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 9522 Color: 426
Size: 6472 Color: 391

Bin 159: 39 of cap free
Amount of items: 2
Items: 
Size: 10213 Color: 434
Size: 5780 Color: 381

Bin 160: 45 of cap free
Amount of items: 2
Items: 
Size: 10036 Color: 433
Size: 5951 Color: 386

Bin 161: 51 of cap free
Amount of items: 3
Items: 
Size: 12153 Color: 475
Size: 2046 Color: 269
Size: 1782 Color: 245

Bin 162: 53 of cap free
Amount of items: 3
Items: 
Size: 11539 Color: 464
Size: 4232 Color: 359
Size: 208 Color: 7

Bin 163: 61 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 472
Size: 3799 Color: 341
Size: 192 Color: 3

Bin 164: 62 of cap free
Amount of items: 2
Items: 
Size: 9714 Color: 429
Size: 6256 Color: 390

Bin 165: 68 of cap free
Amount of items: 3
Items: 
Size: 10820 Color: 445
Size: 4856 Color: 369
Size: 288 Color: 27

Bin 166: 70 of cap free
Amount of items: 2
Items: 
Size: 8042 Color: 415
Size: 7920 Color: 404

Bin 167: 75 of cap free
Amount of items: 12
Items: 
Size: 8017 Color: 405
Size: 892 Color: 171
Size: 880 Color: 170
Size: 880 Color: 169
Size: 872 Color: 168
Size: 850 Color: 166
Size: 846 Color: 165
Size: 832 Color: 163
Size: 830 Color: 162
Size: 354 Color: 60
Size: 352 Color: 59
Size: 352 Color: 57

Bin 168: 78 of cap free
Amount of items: 3
Items: 
Size: 8528 Color: 417
Size: 7120 Color: 402
Size: 306 Color: 38

Bin 169: 86 of cap free
Amount of items: 3
Items: 
Size: 11507 Color: 463
Size: 4231 Color: 358
Size: 208 Color: 8

Bin 170: 91 of cap free
Amount of items: 2
Items: 
Size: 10732 Color: 442
Size: 5209 Color: 374

Bin 171: 92 of cap free
Amount of items: 10
Items: 
Size: 8018 Color: 406
Size: 1152 Color: 186
Size: 1120 Color: 184
Size: 1052 Color: 180
Size: 1040 Color: 179
Size: 1000 Color: 178
Size: 944 Color: 174
Size: 912 Color: 172
Size: 352 Color: 56
Size: 350 Color: 55

Bin 172: 98 of cap free
Amount of items: 2
Items: 
Size: 9988 Color: 432
Size: 5946 Color: 385

Bin 173: 99 of cap free
Amount of items: 2
Items: 
Size: 11031 Color: 450
Size: 4902 Color: 370

Bin 174: 108 of cap free
Amount of items: 2
Items: 
Size: 9240 Color: 424
Size: 6684 Color: 401

Bin 175: 113 of cap free
Amount of items: 3
Items: 
Size: 11738 Color: 467
Size: 3989 Color: 347
Size: 192 Color: 5

Bin 176: 117 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 462
Size: 4219 Color: 357
Size: 216 Color: 10

Bin 177: 142 of cap free
Amount of items: 2
Items: 
Size: 9208 Color: 423
Size: 6682 Color: 400

Bin 178: 149 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 444
Size: 4823 Color: 368
Size: 288 Color: 28

Bin 179: 153 of cap free
Amount of items: 3
Items: 
Size: 11476 Color: 461
Size: 4179 Color: 356
Size: 224 Color: 12

Bin 180: 155 of cap free
Amount of items: 3
Items: 
Size: 11127 Color: 457
Size: 4478 Color: 365
Size: 272 Color: 17

Bin 181: 161 of cap free
Amount of items: 3
Items: 
Size: 8893 Color: 418
Size: 6674 Color: 395
Size: 304 Color: 34

Bin 182: 162 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 419
Size: 6676 Color: 396
Size: 296 Color: 31

Bin 183: 162 of cap free
Amount of items: 3
Items: 
Size: 11475 Color: 460
Size: 4159 Color: 355
Size: 236 Color: 13

Bin 184: 172 of cap free
Amount of items: 2
Items: 
Size: 10362 Color: 439
Size: 5498 Color: 378

Bin 185: 184 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 458
Size: 4152 Color: 354
Size: 268 Color: 16

Bin 186: 199 of cap free
Amount of items: 7
Items: 
Size: 8025 Color: 410
Size: 1604 Color: 226
Size: 1508 Color: 217
Size: 1458 Color: 214
Size: 1456 Color: 213
Size: 1454 Color: 212
Size: 328 Color: 47

Bin 187: 218 of cap free
Amount of items: 2
Items: 
Size: 8028 Color: 412
Size: 7786 Color: 403

Bin 188: 251 of cap free
Amount of items: 2
Items: 
Size: 9100 Color: 422
Size: 6681 Color: 399

Bin 189: 254 of cap free
Amount of items: 3
Items: 
Size: 11086 Color: 455
Size: 4420 Color: 364
Size: 272 Color: 19

Bin 190: 256 of cap free
Amount of items: 2
Items: 
Size: 9618 Color: 428
Size: 6158 Color: 388

Bin 191: 257 of cap free
Amount of items: 2
Items: 
Size: 9095 Color: 421
Size: 6680 Color: 398

Bin 192: 263 of cap free
Amount of items: 2
Items: 
Size: 9092 Color: 420
Size: 6677 Color: 397

Bin 193: 270 of cap free
Amount of items: 2
Items: 
Size: 10298 Color: 437
Size: 5464 Color: 377

Bin 194: 273 of cap free
Amount of items: 2
Items: 
Size: 9613 Color: 427
Size: 6146 Color: 387

Bin 195: 296 of cap free
Amount of items: 8
Items: 
Size: 8024 Color: 409
Size: 1442 Color: 211
Size: 1422 Color: 209
Size: 1412 Color: 207
Size: 1400 Color: 205
Size: 1370 Color: 203
Size: 334 Color: 49
Size: 332 Color: 48

Bin 196: 298 of cap free
Amount of items: 24
Items: 
Size: 826 Color: 160
Size: 824 Color: 159
Size: 818 Color: 158
Size: 816 Color: 157
Size: 816 Color: 156
Size: 782 Color: 155
Size: 764 Color: 153
Size: 760 Color: 152
Size: 758 Color: 151
Size: 754 Color: 150
Size: 752 Color: 149
Size: 752 Color: 148
Size: 752 Color: 147
Size: 718 Color: 145
Size: 672 Color: 141
Size: 672 Color: 140
Size: 672 Color: 139
Size: 640 Color: 135
Size: 380 Color: 69
Size: 376 Color: 68
Size: 360 Color: 64
Size: 358 Color: 63
Size: 356 Color: 62
Size: 356 Color: 61

Bin 197: 309 of cap free
Amount of items: 6
Items: 
Size: 8026 Color: 411
Size: 2009 Color: 266
Size: 2008 Color: 265
Size: 1753 Color: 241
Size: 1605 Color: 227
Size: 322 Color: 45

Bin 198: 348 of cap free
Amount of items: 32
Items: 
Size: 640 Color: 134
Size: 616 Color: 133
Size: 612 Color: 132
Size: 612 Color: 131
Size: 584 Color: 128
Size: 576 Color: 126
Size: 576 Color: 125
Size: 568 Color: 124
Size: 560 Color: 123
Size: 548 Color: 121
Size: 536 Color: 119
Size: 534 Color: 118
Size: 528 Color: 117
Size: 528 Color: 116
Size: 528 Color: 115
Size: 512 Color: 113
Size: 512 Color: 112
Size: 440 Color: 92
Size: 440 Color: 91
Size: 440 Color: 90
Size: 432 Color: 89
Size: 432 Color: 88
Size: 418 Color: 85
Size: 400 Color: 81
Size: 400 Color: 80
Size: 398 Color: 78
Size: 388 Color: 75
Size: 388 Color: 74
Size: 388 Color: 73
Size: 384 Color: 72
Size: 384 Color: 71
Size: 382 Color: 70

Bin 199: 8448 of cap free
Amount of items: 16
Items: 
Size: 504 Color: 111
Size: 502 Color: 110
Size: 498 Color: 109
Size: 496 Color: 108
Size: 488 Color: 106
Size: 488 Color: 105
Size: 480 Color: 104
Size: 476 Color: 103
Size: 468 Color: 102
Size: 464 Color: 100
Size: 458 Color: 99
Size: 456 Color: 98
Size: 456 Color: 97
Size: 452 Color: 96
Size: 450 Color: 95
Size: 448 Color: 94

Total size: 3174336
Total free space: 16032

