Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 1
Size: 364 Color: 1
Size: 40 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 0
Size: 874 Color: 1
Size: 280 Color: 1
Size: 56 Color: 3
Size: 16 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1580 Color: 1
Size: 384 Color: 1
Size: 204 Color: 0
Size: 200 Color: 1
Size: 88 Color: 3

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1740 Color: 0
Size: 604 Color: 3
Size: 64 Color: 3
Size: 48 Color: 4

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1696 Color: 1
Size: 568 Color: 1
Size: 144 Color: 4
Size: 48 Color: 4

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 2098 Color: 0
Size: 286 Color: 3
Size: 64 Color: 4
Size: 8 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 4
Size: 750 Color: 3
Size: 148 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 4
Size: 538 Color: 3
Size: 104 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 4
Size: 228 Color: 4
Size: 40 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1449 Color: 0
Size: 841 Color: 0
Size: 166 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 4
Size: 1018 Color: 1
Size: 200 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 1
Size: 410 Color: 0
Size: 80 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 4
Size: 638 Color: 0
Size: 124 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 3
Size: 402 Color: 0
Size: 76 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 0
Size: 466 Color: 3
Size: 92 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 3
Size: 862 Color: 1
Size: 168 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 2
Size: 454 Color: 0
Size: 88 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 0
Size: 840 Color: 4
Size: 80 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 0
Size: 634 Color: 1
Size: 64 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 1
Size: 330 Color: 0
Size: 64 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 2
Size: 210 Color: 1
Size: 40 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 172 Color: 4
Size: 112 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 674 Color: 0
Size: 72 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1844 Color: 2
Size: 516 Color: 3
Size: 96 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 4
Size: 334 Color: 2
Size: 8 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 1
Size: 386 Color: 0
Size: 76 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 0
Size: 980 Color: 0
Size: 112 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 2
Size: 605 Color: 0
Size: 120 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1020 Color: 4
Size: 924 Color: 2
Size: 512 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 0
Size: 444 Color: 2
Size: 80 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 4
Size: 738 Color: 2
Size: 144 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 4
Size: 340 Color: 0
Size: 88 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 2
Size: 923 Color: 1
Size: 184 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1492 Color: 4
Size: 804 Color: 0
Size: 160 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 3
Size: 374 Color: 2
Size: 72 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 4
Size: 282 Color: 4
Size: 56 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 2
Size: 212 Color: 2
Size: 40 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 3
Size: 334 Color: 0
Size: 64 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1452 Color: 1
Size: 732 Color: 4
Size: 272 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2132 Color: 2
Size: 276 Color: 2
Size: 48 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1351 Color: 1
Size: 921 Color: 1
Size: 184 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 3
Size: 976 Color: 2
Size: 244 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 3
Size: 242 Color: 4
Size: 48 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 3
Size: 611 Color: 1
Size: 120 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 3
Size: 300 Color: 0
Size: 48 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 0
Size: 646 Color: 4
Size: 128 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 4
Size: 1023 Color: 3
Size: 204 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 4
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 0
Size: 214 Color: 4
Size: 40 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 2
Size: 540 Color: 1
Size: 104 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 3
Size: 827 Color: 4
Size: 164 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 1
Size: 420 Color: 4
Size: 80 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 1
Size: 550 Color: 1
Size: 108 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 3
Size: 232 Color: 4
Size: 32 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 1
Size: 246 Color: 2
Size: 48 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 4
Size: 482 Color: 4
Size: 92 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1668 Color: 3
Size: 660 Color: 0
Size: 128 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 2
Size: 1022 Color: 2
Size: 24 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 4
Size: 771 Color: 3
Size: 154 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 2
Size: 665 Color: 2
Size: 132 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 1
Size: 745 Color: 0
Size: 148 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 4
Size: 671 Color: 0
Size: 134 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 4
Size: 841 Color: 4
Size: 68 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 0
Size: 553 Color: 1
Size: 110 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 751 Color: 1
Size: 150 Color: 4

Total size: 159640
Total free space: 0

