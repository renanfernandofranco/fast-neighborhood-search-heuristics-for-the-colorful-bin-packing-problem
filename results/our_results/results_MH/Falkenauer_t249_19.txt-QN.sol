Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 245
Size: 253 Color: 23
Size: 251 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 242
Size: 260 Color: 51
Size: 251 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 225
Size: 271 Color: 85
Size: 260 Color: 53

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 234
Size: 261 Color: 57
Size: 255 Color: 28

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 171
Size: 332 Color: 156
Size: 298 Color: 128

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 167
Size: 353 Color: 165
Size: 291 Color: 123

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 232
Size: 268 Color: 73
Size: 254 Color: 26

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 183
Size: 318 Color: 145
Size: 273 Color: 88

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 213
Size: 283 Color: 111
Size: 258 Color: 43

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 160
Size: 337 Color: 158
Size: 322 Color: 149

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 191
Size: 326 Color: 154
Size: 254 Color: 27

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 246
Size: 253 Color: 21
Size: 251 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 240
Size: 256 Color: 33
Size: 256 Color: 36

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 215
Size: 274 Color: 93
Size: 264 Color: 67

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 276 Color: 96
Size: 250 Color: 4
Size: 474 Color: 229

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 207
Size: 281 Color: 108
Size: 271 Color: 83

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 221
Size: 267 Color: 70
Size: 266 Color: 69

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 195
Size: 314 Color: 141
Size: 259 Color: 48

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 206
Size: 299 Color: 130
Size: 255 Color: 29

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 277 Color: 98
Size: 263 Color: 61
Size: 460 Color: 214

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 186
Size: 312 Color: 138
Size: 271 Color: 82

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 202
Size: 305 Color: 133
Size: 253 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 174
Size: 326 Color: 153
Size: 296 Color: 125

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 188
Size: 318 Color: 146
Size: 264 Color: 66

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 203
Size: 306 Color: 134
Size: 252 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 230
Size: 270 Color: 79
Size: 255 Color: 30

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 163
Size: 342 Color: 161
Size: 308 Color: 135

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 175
Size: 315 Color: 142
Size: 296 Color: 126

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 178
Size: 347 Color: 162
Size: 250 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 238
Size: 263 Color: 62
Size: 250 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 192
Size: 293 Color: 124
Size: 284 Color: 112

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 231
Size: 269 Color: 75
Size: 255 Color: 32

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 219
Size: 277 Color: 99
Size: 257 Color: 41

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 193
Size: 303 Color: 131
Size: 273 Color: 90

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 172
Size: 339 Color: 159
Size: 284 Color: 113

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 10
Size: 250 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 228
Size: 278 Color: 100
Size: 250 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 187
Size: 320 Color: 147
Size: 263 Color: 63

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 196
Size: 313 Color: 139
Size: 260 Color: 49

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 241
Size: 258 Color: 44
Size: 254 Color: 25

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 205
Size: 299 Color: 129
Size: 256 Color: 38

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 218
Size: 282 Color: 109
Size: 252 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 189
Size: 325 Color: 152
Size: 256 Color: 39

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 223
Size: 278 Color: 102
Size: 254 Color: 24

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 226
Size: 280 Color: 107
Size: 250 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 233
Size: 259 Color: 47
Size: 259 Color: 45

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 216
Size: 270 Color: 80
Size: 266 Color: 68

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 190
Size: 320 Color: 148
Size: 261 Color: 58

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 185
Size: 312 Color: 137
Size: 275 Color: 95

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 209
Size: 278 Color: 101
Size: 270 Color: 76

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 170
Size: 361 Color: 168
Size: 273 Color: 89

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 201
Size: 288 Color: 118
Size: 271 Color: 81

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 235
Size: 264 Color: 65
Size: 252 Color: 13

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 208
Size: 280 Color: 105
Size: 270 Color: 78

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 176
Size: 324 Color: 151
Size: 280 Color: 106

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 182
Size: 323 Color: 150
Size: 271 Color: 86

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 239
Size: 260 Color: 52
Size: 253 Color: 20

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 200
Size: 289 Color: 119
Size: 272 Color: 87

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 180
Size: 305 Color: 132
Size: 290 Color: 121

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 179
Size: 335 Color: 157
Size: 261 Color: 56

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 169
Size: 353 Color: 166
Size: 284 Color: 114

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 184
Size: 316 Color: 143
Size: 274 Color: 92

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 199
Size: 313 Color: 140
Size: 253 Color: 22

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 194
Size: 296 Color: 127
Size: 279 Color: 103

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 224
Size: 271 Color: 84
Size: 260 Color: 50

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 217
Size: 274 Color: 91
Size: 262 Color: 59

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 198
Size: 309 Color: 136
Size: 259 Color: 46

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 177
Size: 331 Color: 155
Size: 268 Color: 72

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 181
Size: 318 Color: 144
Size: 277 Color: 97

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 220
Size: 275 Color: 94
Size: 258 Color: 42

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 210
Size: 286 Color: 116
Size: 260 Color: 54

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 211
Size: 287 Color: 117
Size: 256 Color: 34

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 212
Size: 285 Color: 115
Size: 257 Color: 40

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 173
Size: 353 Color: 164
Size: 270 Color: 77

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 222
Size: 280 Color: 104
Size: 252 Color: 17

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 227
Size: 267 Color: 71
Size: 262 Color: 60

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 204
Size: 289 Color: 120
Size: 269 Color: 74

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 236
Size: 261 Color: 55
Size: 255 Color: 31

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 237
Size: 263 Color: 64
Size: 252 Color: 18

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 197
Size: 291 Color: 122
Size: 282 Color: 110

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 243
Size: 256 Color: 35
Size: 252 Color: 16

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 244
Size: 256 Color: 37
Size: 251 Color: 11

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 247
Size: 251 Color: 9
Size: 251 Color: 7

Total size: 83000
Total free space: 0

