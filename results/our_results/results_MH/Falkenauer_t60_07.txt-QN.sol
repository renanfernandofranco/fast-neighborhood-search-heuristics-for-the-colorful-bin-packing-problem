Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 49
Size: 324 Color: 31
Size: 266 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 54
Size: 296 Color: 24
Size: 250 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 56
Size: 264 Color: 12
Size: 260 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 48
Size: 314 Color: 28
Size: 279 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 42
Size: 370 Color: 41
Size: 260 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 55
Size: 279 Color: 18
Size: 256 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 53
Size: 303 Color: 26
Size: 265 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 51
Size: 307 Color: 27
Size: 281 Color: 21

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 43
Size: 330 Color: 35
Size: 297 Color: 25

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 59
Size: 261 Color: 9
Size: 252 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 40
Size: 353 Color: 36
Size: 281 Color: 20

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 44
Size: 329 Color: 34
Size: 293 Color: 23

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 58
Size: 264 Color: 11
Size: 256 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 57
Size: 267 Color: 15
Size: 255 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 47
Size: 326 Color: 32
Size: 268 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 45
Size: 365 Color: 39
Size: 255 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 46
Size: 322 Color: 30
Size: 286 Color: 22

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 50
Size: 327 Color: 33
Size: 263 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 38
Size: 362 Color: 37
Size: 273 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 52
Size: 318 Color: 29
Size: 260 Color: 7

Total size: 20000
Total free space: 0

