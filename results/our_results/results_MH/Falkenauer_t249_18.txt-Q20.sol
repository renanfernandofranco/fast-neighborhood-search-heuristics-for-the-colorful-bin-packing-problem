Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 360 Color: 17
Size: 256 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 11
Size: 313 Color: 3
Size: 257 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 259 Color: 3
Size: 384 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 15
Size: 340 Color: 6
Size: 289 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 6
Size: 286 Color: 18
Size: 282 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 18
Size: 317 Color: 0
Size: 272 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 5
Size: 310 Color: 17
Size: 306 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 16
Size: 297 Color: 2
Size: 255 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 9
Size: 362 Color: 6
Size: 266 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 18
Size: 280 Color: 13
Size: 251 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 306 Color: 3
Size: 268 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 16
Size: 301 Color: 8
Size: 262 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 6
Size: 264 Color: 11
Size: 250 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 13
Size: 365 Color: 10
Size: 267 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 8
Size: 355 Color: 1
Size: 285 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 14
Size: 336 Color: 15
Size: 263 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 251 Color: 1
Size: 250 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 18
Size: 260 Color: 10
Size: 250 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 276 Color: 13
Size: 258 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 14
Size: 330 Color: 12
Size: 263 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 9
Size: 293 Color: 1
Size: 272 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 9
Size: 283 Color: 1
Size: 278 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 13
Size: 334 Color: 3
Size: 329 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7
Size: 356 Color: 6
Size: 253 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 19
Size: 343 Color: 13
Size: 290 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 8
Size: 367 Color: 11
Size: 259 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 288 Color: 10
Size: 281 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 16
Size: 283 Color: 8
Size: 268 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 17
Size: 323 Color: 6
Size: 281 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 15
Size: 305 Color: 1
Size: 285 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 19
Size: 263 Color: 3
Size: 251 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 11
Size: 322 Color: 13
Size: 279 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 258 Color: 17
Size: 250 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 10
Size: 355 Color: 18
Size: 251 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 5
Size: 258 Color: 18
Size: 251 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 12
Size: 262 Color: 1
Size: 254 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 351 Color: 0
Size: 278 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 8
Size: 255 Color: 14
Size: 250 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 8
Size: 262 Color: 0
Size: 251 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 13
Size: 264 Color: 4
Size: 253 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8
Size: 292 Color: 7
Size: 268 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 293 Color: 18
Size: 271 Color: 5

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 3
Size: 319 Color: 5
Size: 257 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9
Size: 288 Color: 11
Size: 253 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 16
Size: 310 Color: 18
Size: 250 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 330 Color: 4
Size: 307 Color: 18

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 18
Size: 256 Color: 18
Size: 256 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 306 Color: 18
Size: 303 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 5
Size: 356 Color: 4
Size: 281 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 14
Size: 260 Color: 9
Size: 251 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 342 Color: 11
Size: 251 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 16
Size: 303 Color: 6
Size: 302 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 13
Size: 315 Color: 19
Size: 255 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 264 Color: 13
Size: 256 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 259 Color: 8
Size: 257 Color: 7

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 10
Size: 287 Color: 17
Size: 268 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 5
Size: 258 Color: 4
Size: 251 Color: 11

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 19
Size: 300 Color: 9
Size: 278 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9
Size: 277 Color: 8
Size: 265 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 9
Size: 342 Color: 3
Size: 296 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 17
Size: 329 Color: 13
Size: 306 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 352 Color: 11
Size: 279 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 18
Size: 344 Color: 17
Size: 303 Color: 11

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 18
Size: 297 Color: 15
Size: 277 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 5
Size: 267 Color: 12
Size: 257 Color: 10

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 9
Size: 357 Color: 17
Size: 259 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 338 Color: 12
Size: 260 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 293 Color: 14
Size: 265 Color: 9

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 273 Color: 5
Size: 258 Color: 15

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 11
Size: 305 Color: 11
Size: 287 Color: 18

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 358 Color: 16
Size: 270 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 6
Size: 332 Color: 0
Size: 294 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 9
Size: 352 Color: 12
Size: 262 Color: 12

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 320 Color: 13
Size: 292 Color: 5

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 17
Size: 321 Color: 1
Size: 309 Color: 6

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 352 Color: 6
Size: 267 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 7
Size: 258 Color: 9
Size: 252 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 16
Size: 299 Color: 10
Size: 287 Color: 17

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9
Size: 294 Color: 5
Size: 256 Color: 17

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 15
Size: 276 Color: 12
Size: 258 Color: 13

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 18
Size: 276 Color: 0
Size: 267 Color: 5

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 263 Color: 5
Size: 254 Color: 11

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 351 Color: 3
Size: 253 Color: 15

Total size: 83000
Total free space: 0

