Capicity Bin: 16400
Lower Bound: 198

Bins used: 198
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 1
Size: 1432 Color: 1
Size: 272 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 13104 Color: 1
Size: 2064 Color: 1
Size: 752 Color: 0
Size: 256 Color: 0
Size: 224 Color: 0

Bin 3: 0 of cap free
Amount of items: 8
Items: 
Size: 8096 Color: 1
Size: 2496 Color: 1
Size: 2256 Color: 1
Size: 1248 Color: 1
Size: 1072 Color: 0
Size: 912 Color: 0
Size: 208 Color: 0
Size: 112 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10075 Color: 1
Size: 5271 Color: 1
Size: 1054 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 1
Size: 1838 Color: 1
Size: 364 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13750 Color: 1
Size: 2210 Color: 1
Size: 440 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 1
Size: 3322 Color: 1
Size: 664 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 1
Size: 3688 Color: 1
Size: 736 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 1
Size: 1764 Color: 1
Size: 352 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14169 Color: 1
Size: 1861 Color: 1
Size: 370 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13819 Color: 1
Size: 2151 Color: 1
Size: 430 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9207 Color: 1
Size: 5995 Color: 1
Size: 1198 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 1
Size: 2600 Color: 1
Size: 512 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 10616 Color: 1
Size: 4824 Color: 1
Size: 960 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13731 Color: 1
Size: 2225 Color: 1
Size: 444 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12228 Color: 1
Size: 3892 Color: 1
Size: 280 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 1
Size: 2052 Color: 1
Size: 408 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 1
Size: 4755 Color: 1
Size: 950 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11513 Color: 1
Size: 4073 Color: 1
Size: 814 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 1
Size: 6828 Color: 1
Size: 1360 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 10150 Color: 1
Size: 5210 Color: 1
Size: 1040 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13882 Color: 1
Size: 2102 Color: 1
Size: 416 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14156 Color: 1
Size: 1876 Color: 1
Size: 368 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14698 Color: 1
Size: 1422 Color: 1
Size: 280 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 1
Size: 1912 Color: 1
Size: 368 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 9708 Color: 1
Size: 6292 Color: 1
Size: 400 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 1
Size: 1596 Color: 1
Size: 312 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 1
Size: 6808 Color: 1
Size: 1344 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 1
Size: 1528 Color: 1
Size: 288 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14550 Color: 1
Size: 1542 Color: 1
Size: 308 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 10103 Color: 1
Size: 6025 Color: 1
Size: 272 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14350 Color: 1
Size: 1710 Color: 1
Size: 340 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13899 Color: 1
Size: 2085 Color: 1
Size: 416 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 1
Size: 6818 Color: 1
Size: 1360 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13070 Color: 1
Size: 2778 Color: 1
Size: 552 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 9179 Color: 1
Size: 6019 Color: 1
Size: 1202 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 10260 Color: 1
Size: 5124 Color: 1
Size: 1016 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11608 Color: 1
Size: 4008 Color: 1
Size: 784 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14604 Color: 1
Size: 1500 Color: 1
Size: 296 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 1
Size: 2642 Color: 1
Size: 524 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 9176 Color: 1
Size: 6024 Color: 1
Size: 1200 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 1
Size: 2760 Color: 1
Size: 544 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12690 Color: 1
Size: 3394 Color: 1
Size: 316 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 9931 Color: 1
Size: 5391 Color: 1
Size: 1078 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 1
Size: 3032 Color: 1
Size: 592 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13858 Color: 1
Size: 2122 Color: 1
Size: 420 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 9239 Color: 1
Size: 5969 Color: 1
Size: 1192 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 1
Size: 1768 Color: 1
Size: 336 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14067 Color: 1
Size: 1945 Color: 1
Size: 388 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 1
Size: 2342 Color: 1
Size: 464 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 8206 Color: 1
Size: 6830 Color: 1
Size: 1364 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12441 Color: 1
Size: 3663 Color: 1
Size: 296 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 9964 Color: 1
Size: 5364 Color: 1
Size: 1072 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13884 Color: 1
Size: 2100 Color: 1
Size: 416 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 1
Size: 2952 Color: 1
Size: 496 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13605 Color: 1
Size: 2331 Color: 1
Size: 464 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 10043 Color: 1
Size: 5299 Color: 1
Size: 1058 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 1
Size: 1918 Color: 1
Size: 380 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 1
Size: 1742 Color: 1
Size: 348 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 1
Size: 1832 Color: 1
Size: 352 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 9222 Color: 1
Size: 5982 Color: 1
Size: 1196 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14294 Color: 1
Size: 1758 Color: 1
Size: 348 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 10807 Color: 1
Size: 5105 Color: 1
Size: 488 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 1
Size: 1899 Color: 1
Size: 378 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12802 Color: 1
Size: 3002 Color: 1
Size: 596 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13628 Color: 1
Size: 2316 Color: 1
Size: 456 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13883 Color: 1
Size: 2099 Color: 1
Size: 418 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 1
Size: 1558 Color: 1
Size: 308 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 1
Size: 2520 Color: 1
Size: 208 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8201 Color: 1
Size: 6833 Color: 1
Size: 1366 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 1
Size: 2743 Color: 1
Size: 548 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 11585 Color: 1
Size: 4013 Color: 1
Size: 802 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 1
Size: 5900 Color: 1
Size: 1176 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 1
Size: 1640 Color: 1
Size: 320 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 1
Size: 1884 Color: 1
Size: 368 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13955 Color: 1
Size: 2039 Color: 1
Size: 406 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 9254 Color: 1
Size: 5958 Color: 1
Size: 1188 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 1
Size: 3364 Color: 1
Size: 256 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13787 Color: 1
Size: 2179 Color: 1
Size: 434 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 14740 Color: 1
Size: 1388 Color: 1
Size: 272 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 1
Size: 1400 Color: 1
Size: 272 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 11689 Color: 1
Size: 3927 Color: 1
Size: 784 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13513 Color: 1
Size: 2407 Color: 1
Size: 480 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 10963 Color: 1
Size: 4531 Color: 1
Size: 906 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 1
Size: 3208 Color: 1
Size: 640 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13038 Color: 1
Size: 2918 Color: 1
Size: 444 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 11577 Color: 1
Size: 4145 Color: 1
Size: 678 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 12276 Color: 1
Size: 3444 Color: 1
Size: 680 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13709 Color: 1
Size: 2243 Color: 1
Size: 448 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 1
Size: 2216 Color: 1
Size: 432 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 1
Size: 2376 Color: 1
Size: 464 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 8204 Color: 1
Size: 6836 Color: 1
Size: 1360 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 10935 Color: 1
Size: 4555 Color: 1
Size: 910 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 10071 Color: 1
Size: 5275 Color: 1
Size: 1054 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 12791 Color: 1
Size: 3009 Color: 1
Size: 600 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 1
Size: 3432 Color: 1
Size: 672 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14334 Color: 1
Size: 1722 Color: 1
Size: 344 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 12891 Color: 1
Size: 2925 Color: 1
Size: 584 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 12860 Color: 1
Size: 2956 Color: 1
Size: 584 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 1
Size: 3212 Color: 1
Size: 640 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 12497 Color: 1
Size: 3737 Color: 1
Size: 166 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 10155 Color: 1
Size: 5205 Color: 1
Size: 1040 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 9067 Color: 1
Size: 6111 Color: 1
Size: 1222 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 11633 Color: 1
Size: 3973 Color: 1
Size: 794 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 1
Size: 5368 Color: 1
Size: 1072 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 11434 Color: 1
Size: 4142 Color: 1
Size: 824 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14033 Color: 1
Size: 2309 Color: 1
Size: 58 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 1
Size: 1724 Color: 1
Size: 336 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 11665 Color: 1
Size: 4427 Color: 1
Size: 308 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 1
Size: 1420 Color: 1
Size: 280 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 13430 Color: 1
Size: 2478 Color: 1
Size: 492 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 2012 Color: 1
Size: 400 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 13518 Color: 1
Size: 2402 Color: 1
Size: 480 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 10202 Color: 1
Size: 5166 Color: 1
Size: 1032 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 1
Size: 2644 Color: 1
Size: 528 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 9372 Color: 1
Size: 6452 Color: 1
Size: 576 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 1
Size: 1732 Color: 1
Size: 344 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 9211 Color: 1
Size: 5991 Color: 1
Size: 1198 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 1
Size: 1460 Color: 1
Size: 288 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 13815 Color: 1
Size: 2155 Color: 1
Size: 430 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 11601 Color: 1
Size: 4379 Color: 1
Size: 420 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 1
Size: 7736 Color: 1
Size: 448 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 1
Size: 2008 Color: 1
Size: 384 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 10139 Color: 1
Size: 5219 Color: 1
Size: 1042 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 1
Size: 1380 Color: 1
Size: 272 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14542 Color: 1
Size: 1550 Color: 1
Size: 308 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14606 Color: 1
Size: 1498 Color: 1
Size: 296 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14656 Color: 1
Size: 1456 Color: 1
Size: 288 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 12382 Color: 1
Size: 3350 Color: 1
Size: 668 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 13638 Color: 1
Size: 2302 Color: 1
Size: 460 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 11982 Color: 1
Size: 3902 Color: 1
Size: 516 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 10967 Color: 1
Size: 4529 Color: 1
Size: 904 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 10064 Color: 1
Size: 5296 Color: 1
Size: 1040 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14038 Color: 1
Size: 1970 Color: 1
Size: 392 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14582 Color: 1
Size: 1518 Color: 1
Size: 300 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 1
Size: 1574 Color: 1
Size: 312 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 11548 Color: 1
Size: 4108 Color: 1
Size: 744 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 14540 Color: 1
Size: 1556 Color: 1
Size: 304 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14702 Color: 1
Size: 1558 Color: 1
Size: 140 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 1
Size: 3764 Color: 1
Size: 752 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 14654 Color: 1
Size: 1458 Color: 1
Size: 288 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 13866 Color: 1
Size: 2114 Color: 1
Size: 420 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 10147 Color: 1
Size: 5211 Color: 1
Size: 1042 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 1
Size: 2364 Color: 1
Size: 472 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 1
Size: 1638 Color: 1
Size: 324 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 14462 Color: 1
Size: 1618 Color: 1
Size: 320 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 9334 Color: 1
Size: 5890 Color: 1
Size: 1176 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 7216 Color: 1
Size: 6064 Color: 1
Size: 3120 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 1
Size: 2276 Color: 1
Size: 448 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 13073 Color: 1
Size: 2865 Color: 1
Size: 462 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 1
Size: 5148 Color: 1
Size: 168 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 1
Size: 6196 Color: 1
Size: 336 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 8214 Color: 1
Size: 6822 Color: 1
Size: 1364 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 11870 Color: 1
Size: 3830 Color: 1
Size: 700 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 1
Size: 2054 Color: 1
Size: 408 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 10099 Color: 1
Size: 5251 Color: 1
Size: 1050 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 1
Size: 2508 Color: 1
Size: 496 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 1
Size: 1644 Color: 1
Size: 320 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 13157 Color: 1
Size: 2703 Color: 1
Size: 540 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 1
Size: 1510 Color: 1
Size: 300 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 1
Size: 2764 Color: 1
Size: 552 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 1512 Color: 1
Size: 288 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 14154 Color: 1
Size: 1874 Color: 1
Size: 372 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 14598 Color: 1
Size: 1502 Color: 1
Size: 300 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 11902 Color: 1
Size: 3750 Color: 1
Size: 748 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 1
Size: 2152 Color: 1
Size: 416 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 14432 Color: 1
Size: 1648 Color: 1
Size: 320 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 1
Size: 3032 Color: 1
Size: 304 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 1
Size: 4376 Color: 1
Size: 864 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 10874 Color: 1
Size: 4606 Color: 1
Size: 920 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 12949 Color: 1
Size: 2877 Color: 1
Size: 574 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 12469 Color: 1
Size: 3277 Color: 1
Size: 654 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 13959 Color: 1
Size: 2035 Color: 1
Size: 406 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 13347 Color: 1
Size: 2545 Color: 1
Size: 508 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 12014 Color: 1
Size: 3658 Color: 1
Size: 728 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 1
Size: 5084 Color: 1
Size: 1008 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 11398 Color: 1
Size: 4170 Color: 1
Size: 832 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 14207 Color: 1
Size: 1829 Color: 1
Size: 364 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 11371 Color: 1
Size: 4421 Color: 1
Size: 608 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 13467 Color: 1
Size: 2493 Color: 1
Size: 440 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 8205 Color: 1
Size: 6831 Color: 1
Size: 1364 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 13250 Color: 1
Size: 2982 Color: 1
Size: 168 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 1
Size: 3571 Color: 1
Size: 712 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 12085 Color: 1
Size: 3597 Color: 1
Size: 718 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 13843 Color: 1
Size: 2131 Color: 1
Size: 426 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 12601 Color: 1
Size: 3167 Color: 1
Size: 632 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 13675 Color: 1
Size: 2511 Color: 1
Size: 214 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 11990 Color: 1
Size: 3678 Color: 1
Size: 732 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 8202 Color: 1
Size: 7934 Color: 1
Size: 264 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 10830 Color: 1
Size: 4642 Color: 1
Size: 928 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 13266 Color: 1
Size: 2614 Color: 1
Size: 520 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 9235 Color: 1
Size: 5971 Color: 1
Size: 1194 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 11076 Color: 1
Size: 4444 Color: 1
Size: 880 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 12113 Color: 1
Size: 3573 Color: 1
Size: 714 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 12329 Color: 1
Size: 3393 Color: 1
Size: 678 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 1
Size: 2974 Color: 1
Size: 592 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 13287 Color: 1
Size: 2595 Color: 1
Size: 518 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 13579 Color: 1
Size: 2393 Color: 1
Size: 428 Color: 0

Total size: 3247200
Total free space: 0

