Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 352 Color: 3
Size: 268 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 262 Color: 1
Size: 276 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 347 Color: 3
Size: 281 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 270 Color: 0
Size: 334 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 335 Color: 1
Size: 285 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 259 Color: 0
Size: 252 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 269 Color: 0
Size: 262 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 296 Color: 2
Size: 282 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 264 Color: 1
Size: 252 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 327 Color: 4
Size: 301 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 4
Size: 358 Color: 2
Size: 281 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 286 Color: 4
Size: 281 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 328 Color: 1
Size: 283 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 290 Color: 2
Size: 278 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 3
Size: 360 Color: 4
Size: 252 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 4
Size: 276 Color: 1
Size: 261 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 4
Size: 340 Color: 2
Size: 305 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 265 Color: 1
Size: 252 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 254 Color: 4
Size: 250 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 302 Color: 0
Size: 282 Color: 3

Total size: 20000
Total free space: 0

