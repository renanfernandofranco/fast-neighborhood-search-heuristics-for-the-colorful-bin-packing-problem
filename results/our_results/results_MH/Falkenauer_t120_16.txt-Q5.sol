Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 329 Color: 4
Size: 257 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 257 Color: 0
Size: 251 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 364 Color: 2
Size: 255 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 285 Color: 0
Size: 254 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 298 Color: 2
Size: 259 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 337 Color: 2
Size: 259 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 361 Color: 2
Size: 262 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 273 Color: 3
Size: 260 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 310 Color: 1
Size: 293 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 355 Color: 2
Size: 262 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 349 Color: 2
Size: 275 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 293 Color: 0
Size: 253 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 368 Color: 4
Size: 262 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 2
Size: 265 Color: 3
Size: 251 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 298 Color: 0
Size: 274 Color: 4
Size: 428 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 330 Color: 2
Size: 267 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 4
Size: 281 Color: 2
Size: 272 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 260 Color: 0
Size: 250 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 323 Color: 2
Size: 265 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 265 Color: 2
Size: 250 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 337 Color: 1
Size: 270 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 294 Color: 0
Size: 284 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 294 Color: 0
Size: 264 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 2
Size: 324 Color: 3
Size: 317 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 329 Color: 3
Size: 267 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 3
Size: 299 Color: 4
Size: 282 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 264 Color: 0
Size: 261 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 321 Color: 1
Size: 252 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 285 Color: 0
Size: 284 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 287 Color: 3
Size: 279 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 352 Color: 0
Size: 279 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 369 Color: 2
Size: 254 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 303 Color: 2
Size: 260 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 285 Color: 3
Size: 273 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 290 Color: 1
Size: 264 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 358 Color: 1
Size: 255 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 319 Color: 0
Size: 316 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 367 Color: 1
Size: 252 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 3
Size: 272 Color: 3
Size: 256 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 3
Size: 290 Color: 0
Size: 278 Color: 1

Total size: 40000
Total free space: 0

