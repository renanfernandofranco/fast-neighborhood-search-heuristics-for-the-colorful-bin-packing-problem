Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 252 Color: 2
Size: 250 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 355 Color: 3
Size: 288 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 334 Color: 0
Size: 294 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 340 Color: 3
Size: 282 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 4
Size: 274 Color: 2
Size: 262 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 320 Color: 1
Size: 261 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 0
Size: 250 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 357 Color: 3
Size: 274 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 308 Color: 3
Size: 290 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 294 Color: 3
Size: 274 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 326 Color: 2
Size: 283 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 287 Color: 2
Size: 285 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 263 Color: 2
Size: 259 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 2
Size: 302 Color: 2
Size: 267 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 260 Color: 3
Size: 256 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 3
Size: 359 Color: 2
Size: 255 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 343 Color: 4
Size: 250 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 338 Color: 1
Size: 271 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 4
Size: 308 Color: 1
Size: 307 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 272 Color: 3
Size: 256 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 2
Size: 282 Color: 0
Size: 280 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 304 Color: 2
Size: 287 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 4
Size: 307 Color: 3
Size: 300 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 256 Color: 4
Size: 253 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 324 Color: 3
Size: 285 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 334 Color: 3
Size: 279 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 298 Color: 3
Size: 255 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 3
Size: 352 Color: 1
Size: 297 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 335 Color: 3
Size: 292 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 333 Color: 0
Size: 274 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 4
Size: 337 Color: 4
Size: 253 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 326 Color: 4
Size: 312 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 312 Color: 2
Size: 277 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 273 Color: 3
Size: 263 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 338 Color: 0
Size: 281 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 285 Color: 4
Size: 262 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 300 Color: 3
Size: 283 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 294 Color: 3
Size: 255 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 264 Color: 1
Size: 256 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 292 Color: 1
Size: 280 Color: 2
Size: 429 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 279 Color: 4
Size: 265 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 251 Color: 4
Size: 251 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 3
Size: 353 Color: 0
Size: 284 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 273 Color: 2
Size: 251 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 3
Size: 336 Color: 2
Size: 313 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 2
Size: 318 Color: 2
Size: 273 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 304 Color: 1
Size: 302 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 3
Size: 312 Color: 2
Size: 259 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 271 Color: 0
Size: 269 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 2
Size: 317 Color: 2
Size: 268 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 330 Color: 2
Size: 258 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 334 Color: 3
Size: 259 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 338 Color: 3
Size: 277 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 297 Color: 4
Size: 270 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 337 Color: 1
Size: 297 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 303 Color: 2
Size: 267 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 368 Color: 3
Size: 255 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 367 Color: 1
Size: 259 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 350 Color: 4
Size: 274 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 255 Color: 2
Size: 251 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 333 Color: 2
Size: 259 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 319 Color: 2
Size: 277 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 305 Color: 1
Size: 290 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 291 Color: 0
Size: 261 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 4
Size: 347 Color: 3
Size: 297 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 315 Color: 4
Size: 294 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 336 Color: 4
Size: 266 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 322 Color: 3
Size: 299 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 302 Color: 0
Size: 280 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 268 Color: 1
Size: 268 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 4
Size: 313 Color: 4
Size: 275 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 320 Color: 4
Size: 320 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 292 Color: 2
Size: 257 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 4
Size: 262 Color: 3
Size: 257 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 302 Color: 4
Size: 278 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 350 Color: 3
Size: 272 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 259 Color: 4
Size: 255 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 350 Color: 0
Size: 276 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 318 Color: 0
Size: 269 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 275 Color: 2
Size: 251 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 263 Color: 2
Size: 250 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 277 Color: 2
Size: 462 Color: 0
Size: 262 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 290 Color: 0
Size: 269 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 264 Color: 0
Size: 254 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 310 Color: 0
Size: 281 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 288 Color: 1
Size: 259 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 269 Color: 0
Size: 266 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 355 Color: 4
Size: 265 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 373 Color: 3
Size: 253 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 341 Color: 0
Size: 296 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 4
Size: 343 Color: 0
Size: 283 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 2
Size: 324 Color: 1
Size: 269 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 4
Size: 322 Color: 2
Size: 267 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 334 Color: 3
Size: 272 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 2
Size: 306 Color: 4
Size: 301 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 289 Color: 1
Size: 265 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 3
Size: 250 Color: 2
Size: 250 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 329 Color: 3
Size: 258 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 2
Size: 321 Color: 4
Size: 307 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 0
Size: 251 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 330 Color: 3
Size: 291 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 328 Color: 3
Size: 275 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 3
Size: 269 Color: 4
Size: 256 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 317 Color: 4
Size: 284 Color: 3

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 2
Size: 321 Color: 2
Size: 316 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 2
Size: 347 Color: 1
Size: 292 Color: 2

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 321 Color: 2
Size: 304 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 0
Size: 271 Color: 3
Size: 263 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 273 Color: 0
Size: 253 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 316 Color: 0
Size: 288 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 326 Color: 0
Size: 300 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 318 Color: 3
Size: 317 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 338 Color: 4
Size: 262 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 255 Color: 1
Size: 253 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 331 Color: 1
Size: 277 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 1
Size: 333 Color: 4
Size: 327 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 313 Color: 0
Size: 288 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 323 Color: 3
Size: 269 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 338 Color: 4
Size: 258 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 285 Color: 0
Size: 268 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 349 Color: 4
Size: 262 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 284 Color: 3
Size: 255 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 339 Color: 0
Size: 299 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 324 Color: 2
Size: 269 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 337 Color: 4
Size: 250 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 256 Color: 3
Size: 250 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 274 Color: 0
Size: 265 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 296 Color: 3
Size: 284 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 319 Color: 2
Size: 270 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 3
Size: 334 Color: 2
Size: 312 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 4
Size: 353 Color: 4
Size: 285 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 3
Size: 318 Color: 3
Size: 254 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 2
Size: 295 Color: 0
Size: 274 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 299 Color: 0
Size: 295 Color: 4

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 275 Color: 3
Size: 265 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 308 Color: 3
Size: 260 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 327 Color: 4
Size: 290 Color: 3

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 301 Color: 2
Size: 275 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 314 Color: 1
Size: 255 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 348 Color: 2
Size: 294 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 327 Color: 3
Size: 276 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 321 Color: 3
Size: 294 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 358 Color: 0
Size: 269 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 282 Color: 0
Size: 251 Color: 3

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 4
Size: 267 Color: 3
Size: 262 Color: 4

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 342 Color: 2
Size: 292 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 345 Color: 1
Size: 279 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 332 Color: 0
Size: 276 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 316 Color: 0
Size: 308 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 3
Size: 267 Color: 1
Size: 257 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 344 Color: 4
Size: 292 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 307 Color: 0
Size: 291 Color: 4

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 310 Color: 2
Size: 267 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 287 Color: 3
Size: 261 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 338 Color: 1
Size: 306 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 296 Color: 4
Size: 283 Color: 3

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 272 Color: 1
Size: 266 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 352 Color: 2
Size: 284 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 348 Color: 2
Size: 296 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 347 Color: 1
Size: 284 Color: 4

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 320 Color: 1
Size: 295 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 3
Size: 346 Color: 0
Size: 262 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 339 Color: 3
Size: 250 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 332 Color: 0
Size: 253 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 3
Size: 292 Color: 1
Size: 259 Color: 2

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 282 Color: 0
Size: 266 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 262 Color: 3
Size: 253 Color: 1

Total size: 167167
Total free space: 0

