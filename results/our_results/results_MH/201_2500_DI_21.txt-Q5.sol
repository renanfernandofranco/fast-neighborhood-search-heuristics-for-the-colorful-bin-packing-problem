Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1229 Color: 0
Size: 621 Color: 1
Size: 504 Color: 3
Size: 102 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 3
Size: 874 Color: 0
Size: 140 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 4
Size: 718 Color: 1
Size: 52 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 0
Size: 611 Color: 1
Size: 132 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 2
Size: 604 Color: 1
Size: 120 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 3
Size: 678 Color: 1
Size: 28 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 2
Size: 590 Color: 3
Size: 32 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 3
Size: 540 Color: 1
Size: 40 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 3
Size: 514 Color: 3
Size: 52 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 3
Size: 526 Color: 0
Size: 32 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 1
Size: 449 Color: 4
Size: 88 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 1
Size: 364 Color: 4
Size: 112 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 1
Size: 236 Color: 4
Size: 200 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2046 Color: 1
Size: 330 Color: 3
Size: 80 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 4
Size: 308 Color: 0
Size: 88 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 2
Size: 272 Color: 1
Size: 122 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 1
Size: 282 Color: 0
Size: 52 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 4
Size: 226 Color: 2
Size: 92 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 1
Size: 200 Color: 2
Size: 126 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2156 Color: 1
Size: 252 Color: 4
Size: 48 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 3
Size: 204 Color: 2
Size: 82 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 1
Size: 242 Color: 4
Size: 12 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 2
Size: 182 Color: 3
Size: 64 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 2
Size: 777 Color: 3
Size: 146 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 840 Color: 3
Size: 44 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 2
Size: 613 Color: 1
Size: 244 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 0
Size: 654 Color: 2
Size: 76 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 3
Size: 434 Color: 4
Size: 200 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 2
Size: 413 Color: 2
Size: 104 Color: 1

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 0
Size: 1018 Color: 1
Size: 40 Color: 3

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 1
Size: 884 Color: 2
Size: 80 Color: 4

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1869 Color: 4
Size: 585 Color: 0

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 0
Size: 522 Color: 3

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 4
Size: 384 Color: 0
Size: 72 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 266 Color: 4
Size: 8 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 0
Size: 274 Color: 4
Size: 8 Color: 4

Bin 37: 4 of cap free
Amount of items: 17
Items: 
Size: 382 Color: 4
Size: 262 Color: 1
Size: 244 Color: 4
Size: 234 Color: 4
Size: 132 Color: 1
Size: 128 Color: 1
Size: 122 Color: 1
Size: 120 Color: 3
Size: 116 Color: 1
Size: 116 Color: 0
Size: 100 Color: 4
Size: 100 Color: 2
Size: 96 Color: 4
Size: 96 Color: 0
Size: 72 Color: 2
Size: 68 Color: 2
Size: 64 Color: 0

Bin 38: 4 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 0
Size: 684 Color: 1
Size: 484 Color: 1
Size: 48 Color: 4

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1445 Color: 0
Size: 1007 Color: 4

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1646 Color: 3
Size: 806 Color: 2

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 4
Size: 491 Color: 3

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 0
Size: 272 Color: 3
Size: 16 Color: 4

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 1
Size: 570 Color: 3
Size: 160 Color: 2

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 3
Size: 513 Color: 4
Size: 96 Color: 1

Bin 45: 6 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 0
Size: 1020 Color: 0
Size: 152 Color: 4
Size: 48 Color: 4

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1812 Color: 1
Size: 637 Color: 2

Bin 47: 9 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 3
Size: 404 Color: 3
Size: 202 Color: 1

Bin 48: 10 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 3
Size: 1045 Color: 2
Size: 168 Color: 0

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 444 Color: 0
Size: 358 Color: 4

Bin 50: 11 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 1
Size: 576 Color: 3
Size: 176 Color: 3

Bin 51: 12 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 0
Size: 984 Color: 2
Size: 50 Color: 4

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1970 Color: 0
Size: 474 Color: 4

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 2146 Color: 0
Size: 298 Color: 2

Bin 54: 13 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 4
Size: 1022 Color: 0
Size: 172 Color: 1

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1774 Color: 0
Size: 669 Color: 3

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 2100 Color: 0
Size: 342 Color: 2

Bin 57: 20 of cap free
Amount of items: 2
Items: 
Size: 2030 Color: 2
Size: 406 Color: 4

Bin 58: 21 of cap free
Amount of items: 3
Items: 
Size: 1402 Color: 3
Size: 861 Color: 0
Size: 172 Color: 4

Bin 59: 21 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 2
Size: 954 Color: 1
Size: 56 Color: 1

Bin 60: 22 of cap free
Amount of items: 2
Items: 
Size: 1591 Color: 4
Size: 843 Color: 2

Bin 61: 22 of cap free
Amount of items: 2
Items: 
Size: 2102 Color: 2
Size: 332 Color: 0

Bin 62: 27 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 4
Size: 1021 Color: 1
Size: 170 Color: 3

Bin 63: 29 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 3
Size: 772 Color: 4

Bin 64: 29 of cap free
Amount of items: 2
Items: 
Size: 1688 Color: 3
Size: 739 Color: 0

Bin 65: 38 of cap free
Amount of items: 2
Items: 
Size: 1536 Color: 0
Size: 882 Color: 4

Bin 66: 2044 of cap free
Amount of items: 6
Items: 
Size: 88 Color: 4
Size: 84 Color: 3
Size: 68 Color: 3
Size: 64 Color: 2
Size: 60 Color: 0
Size: 48 Color: 2

Total size: 159640
Total free space: 2456

