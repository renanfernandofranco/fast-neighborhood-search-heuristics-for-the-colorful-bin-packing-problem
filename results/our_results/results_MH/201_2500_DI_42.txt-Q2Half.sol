Capicity Bin: 1864
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1292 Color: 1
Size: 428 Color: 1
Size: 60 Color: 0
Size: 48 Color: 1
Size: 36 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 1
Size: 382 Color: 1
Size: 76 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 1
Size: 330 Color: 1
Size: 44 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1168 Color: 1
Size: 388 Color: 1
Size: 288 Color: 0
Size: 20 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 1
Size: 202 Color: 1
Size: 68 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 194 Color: 1
Size: 12 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1498 Color: 1
Size: 306 Color: 1
Size: 60 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 1
Size: 349 Color: 1
Size: 68 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 1
Size: 226 Color: 1
Size: 12 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 1
Size: 221 Color: 1
Size: 44 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 982 Color: 1
Size: 738 Color: 1
Size: 144 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 1
Size: 194 Color: 1
Size: 36 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 1
Size: 731 Color: 1
Size: 144 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 398 Color: 1
Size: 76 Color: 0

Bin 15: 0 of cap free
Amount of items: 5
Items: 
Size: 1470 Color: 1
Size: 314 Color: 1
Size: 64 Color: 1
Size: 8 Color: 0
Size: 8 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 934 Color: 1
Size: 778 Color: 1
Size: 152 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 1
Size: 446 Color: 1
Size: 88 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 1
Size: 380 Color: 1
Size: 68 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1358 Color: 1
Size: 422 Color: 1
Size: 84 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 1
Size: 370 Color: 1
Size: 72 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 1
Size: 274 Color: 1
Size: 52 Color: 0

Bin 22: 0 of cap free
Amount of items: 5
Items: 
Size: 752 Color: 1
Size: 632 Color: 1
Size: 208 Color: 1
Size: 204 Color: 0
Size: 68 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 529 Color: 1
Size: 104 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 1
Size: 261 Color: 1
Size: 52 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 246 Color: 1
Size: 48 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 1
Size: 602 Color: 1
Size: 116 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 1
Size: 666 Color: 1
Size: 132 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 1
Size: 542 Color: 1
Size: 104 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 1
Size: 694 Color: 1
Size: 136 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 1
Size: 186 Color: 1
Size: 36 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 941 Color: 1
Size: 879 Color: 1
Size: 44 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 1
Size: 191 Color: 1
Size: 36 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 1
Size: 302 Color: 1
Size: 56 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1519 Color: 1
Size: 289 Color: 1
Size: 56 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1565 Color: 1
Size: 251 Color: 1
Size: 48 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 1
Size: 443 Color: 1
Size: 88 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 1
Size: 250 Color: 1
Size: 48 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1286 Color: 1
Size: 482 Color: 1
Size: 96 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 1
Size: 507 Color: 1
Size: 100 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 1
Size: 554 Color: 1
Size: 108 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1109 Color: 1
Size: 631 Color: 1
Size: 124 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 961 Color: 1
Size: 753 Color: 1
Size: 150 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 1
Size: 157 Color: 1
Size: 30 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 937 Color: 1
Size: 773 Color: 1
Size: 154 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 1
Size: 182 Color: 1
Size: 32 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 1
Size: 196 Color: 1
Size: 32 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1621 Color: 1
Size: 213 Color: 1
Size: 30 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 1
Size: 351 Color: 1
Size: 70 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 1
Size: 163 Color: 1
Size: 32 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1135 Color: 1
Size: 609 Color: 1
Size: 120 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1131 Color: 1
Size: 611 Color: 1
Size: 122 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 1
Size: 425 Color: 1
Size: 84 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 1
Size: 359 Color: 1
Size: 70 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1619 Color: 1
Size: 205 Color: 1
Size: 40 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 933 Color: 1
Size: 777 Color: 1
Size: 154 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1123 Color: 1
Size: 619 Color: 1
Size: 122 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 981 Color: 1
Size: 737 Color: 1
Size: 146 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 1
Size: 513 Color: 1
Size: 102 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1347 Color: 1
Size: 431 Color: 1
Size: 86 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 1
Size: 375 Color: 1
Size: 74 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 1
Size: 311 Color: 1
Size: 62 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 1
Size: 295 Color: 1
Size: 58 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 1
Size: 239 Color: 1
Size: 46 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 1
Size: 193 Color: 1
Size: 38 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 1
Size: 181 Color: 1
Size: 34 Color: 0

Total size: 121160
Total free space: 0

