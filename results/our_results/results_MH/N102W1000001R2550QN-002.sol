Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 404432 Color: 81
Size: 332821 Color: 56
Size: 262748 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 386174 Color: 76
Size: 348934 Color: 64
Size: 264893 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 404260 Color: 80
Size: 342294 Color: 62
Size: 253447 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 261563 Color: 10
Size: 260544 Color: 8
Size: 477894 Color: 100

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 347861 Color: 63
Size: 372791 Color: 74
Size: 279349 Color: 33

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 408920 Color: 83
Size: 333494 Color: 57
Size: 257587 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 404236 Color: 79
Size: 323498 Color: 51
Size: 272267 Color: 20

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 367850 Color: 73
Size: 334975 Color: 60
Size: 297176 Color: 42

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 406621 Color: 82
Size: 287949 Color: 36
Size: 305431 Color: 47

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 279239 Color: 32
Size: 333959 Color: 59
Size: 386803 Color: 77

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 499931 Color: 101
Size: 250045 Color: 1
Size: 250025 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 360531 Color: 68
Size: 330946 Color: 55
Size: 308524 Color: 48

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 366188 Color: 71
Size: 333953 Color: 58
Size: 299860 Color: 45

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 400802 Color: 78
Size: 320299 Color: 50
Size: 278900 Color: 31

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 359225 Color: 67
Size: 278894 Color: 30
Size: 361882 Color: 70

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 361181 Color: 69
Size: 355356 Color: 66
Size: 283464 Color: 35

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 367354 Color: 72
Size: 296721 Color: 41
Size: 335926 Color: 61

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 377791 Color: 75
Size: 350988 Color: 65
Size: 271222 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 409656 Color: 84
Size: 326108 Color: 54
Size: 264237 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 420366 Color: 85
Size: 311852 Color: 49
Size: 267783 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 423015 Color: 86
Size: 301155 Color: 46
Size: 275831 Color: 26

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 424095 Color: 87
Size: 299124 Color: 43
Size: 276782 Color: 28

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 424129 Color: 88
Size: 323977 Color: 52
Size: 251895 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 424170 Color: 89
Size: 299410 Color: 44
Size: 276421 Color: 27

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 425103 Color: 90
Size: 324430 Color: 53
Size: 250468 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 430993 Color: 91
Size: 295302 Color: 39
Size: 273706 Color: 23

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 436431 Color: 92
Size: 288129 Color: 37
Size: 275441 Color: 25

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 442337 Color: 93
Size: 296700 Color: 40
Size: 260964 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 449871 Color: 94
Size: 276805 Color: 29
Size: 273325 Color: 22

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 456424 Color: 95
Size: 292203 Color: 38
Size: 251374 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 456561 Color: 96
Size: 273073 Color: 21
Size: 270367 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 459496 Color: 97
Size: 283271 Color: 34
Size: 257234 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 463043 Color: 98
Size: 274306 Color: 24
Size: 262652 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 469015 Color: 99
Size: 267991 Color: 17
Size: 262995 Color: 13

Total size: 34000034
Total free space: 0

