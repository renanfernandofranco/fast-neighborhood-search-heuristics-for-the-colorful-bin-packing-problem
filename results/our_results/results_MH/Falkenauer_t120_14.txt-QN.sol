Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 115
Size: 257 Color: 18
Size: 254 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 89
Size: 356 Color: 77
Size: 256 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 113
Size: 262 Color: 29
Size: 252 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 105
Size: 306 Color: 56
Size: 251 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 106
Size: 302 Color: 54
Size: 254 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 112
Size: 260 Color: 24
Size: 256 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 111
Size: 276 Color: 40
Size: 254 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 116
Size: 255 Color: 14
Size: 251 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 103
Size: 294 Color: 51
Size: 273 Color: 37

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 104
Size: 289 Color: 48
Size: 269 Color: 34

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 117
Size: 254 Color: 13
Size: 250 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 91
Size: 350 Color: 75
Size: 254 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 100
Size: 309 Color: 59
Size: 273 Color: 38

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 101
Size: 313 Color: 61
Size: 266 Color: 32

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 87
Size: 336 Color: 70
Size: 280 Color: 42

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 99
Size: 311 Color: 60
Size: 271 Color: 35

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 86
Size: 358 Color: 79
Size: 258 Color: 20

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 118
Size: 253 Color: 8
Size: 251 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 119
Size: 253 Color: 7
Size: 251 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 83
Size: 329 Color: 66
Size: 298 Color: 52

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 94
Size: 313 Color: 62
Size: 282 Color: 43

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 97
Size: 302 Color: 53
Size: 284 Color: 45

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 80
Size: 332 Color: 69
Size: 305 Color: 55

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 331 Color: 68
Size: 257 Color: 19
Size: 412 Color: 96

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 84
Size: 369 Color: 82
Size: 258 Color: 21

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 290 Color: 50
Size: 265 Color: 31
Size: 445 Color: 107

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 73
Size: 329 Color: 67
Size: 328 Color: 65

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 98
Size: 309 Color: 57
Size: 275 Color: 39

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 85
Size: 357 Color: 78
Size: 261 Color: 27

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 95
Size: 336 Color: 71
Size: 259 Color: 22

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 81
Size: 319 Color: 64
Size: 316 Color: 63

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 109
Size: 280 Color: 41
Size: 267 Color: 33

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 93
Size: 340 Color: 72
Size: 256 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 110
Size: 271 Color: 36
Size: 259 Color: 23

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 108
Size: 289 Color: 47
Size: 261 Color: 28

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 102
Size: 287 Color: 46
Size: 283 Color: 44

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 90
Size: 350 Color: 74
Size: 260 Color: 25

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 88
Size: 353 Color: 76
Size: 261 Color: 26

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 92
Size: 309 Color: 58
Size: 289 Color: 49

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 114
Size: 264 Color: 30
Size: 250 Color: 0

Total size: 40000
Total free space: 0

