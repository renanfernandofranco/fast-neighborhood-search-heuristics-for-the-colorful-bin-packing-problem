Capicity Bin: 9984
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4944 Color: 16
Size: 1392 Color: 12
Size: 1248 Color: 18
Size: 784 Color: 1
Size: 624 Color: 17
Size: 544 Color: 14
Size: 448 Color: 8

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8000 Color: 16
Size: 1488 Color: 9
Size: 256 Color: 1
Size: 176 Color: 8
Size: 64 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 19
Size: 3036 Color: 11
Size: 600 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 4
Size: 3118 Color: 10
Size: 620 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8376 Color: 11
Size: 1352 Color: 3
Size: 256 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5796 Color: 15
Size: 3492 Color: 3
Size: 696 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6753 Color: 19
Size: 3063 Color: 8
Size: 168 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7979 Color: 9
Size: 1671 Color: 4
Size: 334 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 0
Size: 3528 Color: 10
Size: 688 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6278 Color: 5
Size: 3090 Color: 18
Size: 616 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 0
Size: 2718 Color: 7
Size: 540 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8515 Color: 17
Size: 1225 Color: 3
Size: 244 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8036 Color: 11
Size: 1628 Color: 3
Size: 320 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 6
Size: 2812 Color: 3
Size: 200 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8918 Color: 11
Size: 890 Color: 11
Size: 176 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8039 Color: 1
Size: 1621 Color: 6
Size: 324 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8642 Color: 1
Size: 1190 Color: 9
Size: 152 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5702 Color: 19
Size: 3570 Color: 18
Size: 712 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5670 Color: 1
Size: 3598 Color: 4
Size: 716 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7063 Color: 5
Size: 2435 Color: 13
Size: 486 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8476 Color: 12
Size: 1260 Color: 5
Size: 248 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8348 Color: 8
Size: 1364 Color: 0
Size: 272 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8728 Color: 8
Size: 1144 Color: 14
Size: 112 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8023 Color: 10
Size: 1845 Color: 13
Size: 116 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 16
Size: 2968 Color: 13
Size: 592 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 3
Size: 2284 Color: 12
Size: 456 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8086 Color: 17
Size: 1582 Color: 3
Size: 316 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7982 Color: 16
Size: 1670 Color: 5
Size: 332 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7422 Color: 16
Size: 2138 Color: 17
Size: 424 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8341 Color: 18
Size: 1371 Color: 16
Size: 272 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 15
Size: 888 Color: 16
Size: 128 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 17
Size: 2600 Color: 8
Size: 512 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7706 Color: 7
Size: 1902 Color: 4
Size: 376 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7434 Color: 2
Size: 2126 Color: 8
Size: 424 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 18
Size: 1484 Color: 19
Size: 288 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4994 Color: 8
Size: 4162 Color: 10
Size: 828 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5256 Color: 11
Size: 3944 Color: 5
Size: 784 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8078 Color: 11
Size: 1590 Color: 11
Size: 316 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 8708 Color: 0
Size: 1068 Color: 14
Size: 208 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8238 Color: 7
Size: 1458 Color: 13
Size: 288 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 17
Size: 1426 Color: 1
Size: 44 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 8664 Color: 4
Size: 1112 Color: 15
Size: 208 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5010 Color: 14
Size: 4146 Color: 2
Size: 828 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4384 Color: 14
Size: 3696 Color: 10
Size: 1904 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 4
Size: 2868 Color: 11
Size: 176 Color: 17

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7147 Color: 6
Size: 2365 Color: 0
Size: 472 Color: 13

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 8836 Color: 19
Size: 964 Color: 15
Size: 184 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 8094 Color: 3
Size: 1578 Color: 18
Size: 312 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7608 Color: 16
Size: 2072 Color: 19
Size: 304 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 8810 Color: 15
Size: 982 Color: 18
Size: 192 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 12
Size: 2460 Color: 2
Size: 488 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7741 Color: 7
Size: 1871 Color: 15
Size: 372 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 13
Size: 1496 Color: 15
Size: 288 Color: 7

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 10
Size: 3796 Color: 7
Size: 752 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 16
Size: 3416 Color: 19
Size: 672 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 8888 Color: 13
Size: 920 Color: 16
Size: 176 Color: 16

Bin 57: 0 of cap free
Amount of items: 4
Items: 
Size: 8426 Color: 13
Size: 1302 Color: 1
Size: 128 Color: 10
Size: 128 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 8418 Color: 1
Size: 1306 Color: 15
Size: 260 Color: 17

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 16
Size: 1314 Color: 4
Size: 260 Color: 9

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5803 Color: 9
Size: 3565 Color: 18
Size: 616 Color: 19

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7420 Color: 4
Size: 2332 Color: 5
Size: 232 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 12
Size: 3126 Color: 2
Size: 624 Color: 9

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6742 Color: 12
Size: 2702 Color: 6
Size: 540 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7401 Color: 18
Size: 2153 Color: 4
Size: 430 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7988 Color: 13
Size: 1668 Color: 13
Size: 328 Color: 7

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 8645 Color: 7
Size: 1117 Color: 11
Size: 222 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7690 Color: 3
Size: 1914 Color: 18
Size: 380 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 13
Size: 2690 Color: 17
Size: 536 Color: 9

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6262 Color: 6
Size: 3102 Color: 17
Size: 620 Color: 10

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8962 Color: 13
Size: 854 Color: 1
Size: 168 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 14
Size: 2785 Color: 13
Size: 556 Color: 14

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 8104 Color: 10
Size: 1576 Color: 13
Size: 304 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 7114 Color: 18
Size: 2394 Color: 17
Size: 476 Color: 19

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7914 Color: 9
Size: 1726 Color: 0
Size: 344 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 8976 Color: 6
Size: 848 Color: 3
Size: 160 Color: 10

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 8900 Color: 14
Size: 908 Color: 14
Size: 176 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7768 Color: 18
Size: 1848 Color: 8
Size: 368 Color: 13

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 4993 Color: 5
Size: 4161 Color: 17
Size: 830 Color: 7

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 17
Size: 1000 Color: 14
Size: 192 Color: 8

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7000 Color: 19
Size: 2488 Color: 9
Size: 496 Color: 7

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 7464 Color: 0
Size: 2104 Color: 19
Size: 416 Color: 9

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 8868 Color: 2
Size: 932 Color: 8
Size: 184 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5005 Color: 19
Size: 4151 Color: 7
Size: 828 Color: 9

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 16
Size: 4892 Color: 14
Size: 96 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5020 Color: 6
Size: 4716 Color: 9
Size: 248 Color: 12

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 4
Size: 2716 Color: 18
Size: 536 Color: 8

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7864 Color: 13
Size: 1768 Color: 13
Size: 352 Color: 8

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5686 Color: 5
Size: 3582 Color: 16
Size: 716 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 8227 Color: 9
Size: 1465 Color: 14
Size: 292 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 8284 Color: 10
Size: 1420 Color: 1
Size: 280 Color: 18

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 17
Size: 3611 Color: 16
Size: 720 Color: 13

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 7
Size: 2860 Color: 0
Size: 568 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 8552 Color: 1
Size: 1208 Color: 0
Size: 224 Color: 14

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 7812 Color: 9
Size: 1812 Color: 4
Size: 360 Color: 19

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 13
Size: 1224 Color: 2
Size: 240 Color: 14

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 8329 Color: 15
Size: 1381 Color: 18
Size: 274 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 3
Size: 3244 Color: 19
Size: 640 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 8540 Color: 17
Size: 1412 Color: 4
Size: 32 Color: 9

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 17
Size: 3061 Color: 16
Size: 266 Color: 16

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 7484 Color: 0
Size: 2084 Color: 5
Size: 416 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 7990 Color: 5
Size: 1978 Color: 8
Size: 16 Color: 5

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 8676 Color: 9
Size: 1092 Color: 1
Size: 216 Color: 19

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 8644 Color: 8
Size: 1124 Color: 2
Size: 216 Color: 17

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 8812 Color: 14
Size: 980 Color: 14
Size: 192 Color: 6

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 8604 Color: 10
Size: 1156 Color: 13
Size: 224 Color: 12

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5002 Color: 11
Size: 4210 Color: 6
Size: 772 Color: 9

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 7112 Color: 10
Size: 2408 Color: 8
Size: 464 Color: 18

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5001 Color: 5
Size: 4153 Color: 17
Size: 830 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 8499 Color: 10
Size: 1239 Color: 6
Size: 246 Color: 12

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7509 Color: 7
Size: 2063 Color: 10
Size: 412 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5000 Color: 14
Size: 4168 Color: 18
Size: 816 Color: 9

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 7678 Color: 0
Size: 1922 Color: 14
Size: 384 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 7775 Color: 2
Size: 1841 Color: 9
Size: 368 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 0
Size: 3176 Color: 12
Size: 624 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 13
Size: 4156 Color: 13
Size: 824 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6209 Color: 1
Size: 3147 Color: 9
Size: 628 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 7620 Color: 15
Size: 2028 Color: 12
Size: 336 Color: 7

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6785 Color: 5
Size: 2667 Color: 10
Size: 532 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 8285 Color: 9
Size: 1417 Color: 10
Size: 282 Color: 19

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 8742 Color: 18
Size: 1038 Color: 5
Size: 204 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6193 Color: 18
Size: 3161 Color: 2
Size: 630 Color: 16

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 17
Size: 4134 Color: 3
Size: 824 Color: 12

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 7933 Color: 4
Size: 1711 Color: 10
Size: 340 Color: 11

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 4997 Color: 13
Size: 4157 Color: 3
Size: 830 Color: 10

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 7130 Color: 8
Size: 2382 Color: 5
Size: 472 Color: 15

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 7369 Color: 9
Size: 2181 Color: 9
Size: 434 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 8289 Color: 16
Size: 1619 Color: 5
Size: 76 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 8641 Color: 3
Size: 1121 Color: 16
Size: 222 Color: 17

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 8658 Color: 2
Size: 1106 Color: 2
Size: 220 Color: 6

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 8657 Color: 5
Size: 1107 Color: 0
Size: 220 Color: 10

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7995 Color: 12
Size: 1659 Color: 5
Size: 330 Color: 13

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7417 Color: 11
Size: 2141 Color: 19
Size: 426 Color: 15

Total size: 1317888
Total free space: 0

