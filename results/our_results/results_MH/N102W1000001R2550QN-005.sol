Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 445680 Color: 93
Size: 303889 Color: 40
Size: 250432 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 416500 Color: 88
Size: 297589 Color: 38
Size: 285912 Color: 33

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 365721 Color: 70
Size: 340175 Color: 60
Size: 294105 Color: 35

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 368806 Color: 75
Size: 335260 Color: 56
Size: 295935 Color: 37

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 392591 Color: 83
Size: 332909 Color: 53
Size: 274501 Color: 26

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 337768 Color: 57
Size: 324117 Color: 49
Size: 338116 Color: 58

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 349231 Color: 64
Size: 331163 Color: 52
Size: 319607 Color: 47

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 343823 Color: 63
Size: 341791 Color: 61
Size: 314387 Color: 43

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 372785 Color: 78
Size: 366180 Color: 73
Size: 261036 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 362549 Color: 68
Size: 332999 Color: 54
Size: 304453 Color: 41

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 372328 Color: 77
Size: 359310 Color: 65
Size: 268363 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 370253 Color: 76
Size: 367697 Color: 74
Size: 262051 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 384413 Color: 81
Size: 361663 Color: 67
Size: 253925 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 401105 Color: 84
Size: 317235 Color: 44
Size: 281661 Color: 30

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 375015 Color: 79
Size: 317581 Color: 45
Size: 307405 Color: 42

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 378359 Color: 80
Size: 338238 Color: 59
Size: 283404 Color: 31

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 365974 Color: 71
Size: 364316 Color: 69
Size: 269711 Color: 20

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 366170 Color: 72
Size: 360264 Color: 66
Size: 273567 Color: 23

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 384924 Color: 82
Size: 335175 Color: 55
Size: 279902 Color: 29

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 401307 Color: 85
Size: 324349 Color: 50
Size: 274345 Color: 25

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 402399 Color: 86
Size: 341887 Color: 62
Size: 255715 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 412561 Color: 87
Size: 325348 Color: 51
Size: 262092 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 422183 Color: 89
Size: 319351 Color: 46
Size: 258467 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 423680 Color: 90
Size: 323092 Color: 48
Size: 253229 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 430926 Color: 91
Size: 295375 Color: 36
Size: 273700 Color: 24

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 433969 Color: 92
Size: 302977 Color: 39
Size: 263055 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 449820 Color: 94
Size: 285528 Color: 32
Size: 264653 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 450839 Color: 95
Size: 278445 Color: 27
Size: 270717 Color: 21

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 454967 Color: 96
Size: 290841 Color: 34
Size: 254193 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 460843 Color: 97
Size: 279245 Color: 28
Size: 259913 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 470873 Color: 98
Size: 272672 Color: 22
Size: 256456 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 485849 Color: 99
Size: 260513 Color: 13
Size: 253639 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 490315 Color: 100
Size: 255936 Color: 9
Size: 253750 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 496617 Color: 101
Size: 252478 Color: 2
Size: 250906 Color: 1

Total size: 34000034
Total free space: 0

