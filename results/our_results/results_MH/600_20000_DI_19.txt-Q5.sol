Capicity Bin: 15328
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 7670 Color: 4
Size: 1548 Color: 4
Size: 1540 Color: 4
Size: 1442 Color: 2
Size: 1428 Color: 0
Size: 900 Color: 3
Size: 800 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8732 Color: 4
Size: 6308 Color: 1
Size: 288 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9212 Color: 3
Size: 5664 Color: 4
Size: 452 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 0
Size: 5352 Color: 4
Size: 256 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 3
Size: 4724 Color: 0
Size: 264 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 4
Size: 2860 Color: 1
Size: 1964 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10498 Color: 1
Size: 4506 Color: 1
Size: 324 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10703 Color: 4
Size: 4145 Color: 0
Size: 480 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 3
Size: 4184 Color: 4
Size: 384 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11721 Color: 4
Size: 3007 Color: 2
Size: 600 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11820 Color: 0
Size: 2844 Color: 0
Size: 664 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11870 Color: 2
Size: 2882 Color: 4
Size: 576 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11916 Color: 3
Size: 1920 Color: 4
Size: 1492 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 2
Size: 2904 Color: 4
Size: 368 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12102 Color: 2
Size: 2546 Color: 4
Size: 680 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 3
Size: 2508 Color: 4
Size: 496 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12367 Color: 0
Size: 2469 Color: 0
Size: 492 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12386 Color: 2
Size: 2214 Color: 4
Size: 728 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12402 Color: 1
Size: 2442 Color: 0
Size: 484 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12418 Color: 4
Size: 2486 Color: 3
Size: 424 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12524 Color: 4
Size: 2422 Color: 0
Size: 382 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12479 Color: 2
Size: 2129 Color: 2
Size: 720 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 2
Size: 2321 Color: 4
Size: 462 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 1
Size: 2352 Color: 4
Size: 408 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 2
Size: 2394 Color: 4
Size: 340 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 0
Size: 2292 Color: 4
Size: 392 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 0
Size: 1406 Color: 4
Size: 1264 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12664 Color: 1
Size: 2232 Color: 3
Size: 432 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 4
Size: 2096 Color: 1
Size: 524 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12676 Color: 2
Size: 2008 Color: 3
Size: 644 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12770 Color: 4
Size: 1982 Color: 0
Size: 576 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12760 Color: 0
Size: 1676 Color: 4
Size: 892 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12949 Color: 4
Size: 1983 Color: 0
Size: 396 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12898 Color: 0
Size: 2026 Color: 4
Size: 404 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 4
Size: 1358 Color: 0
Size: 1016 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 4
Size: 1276 Color: 2
Size: 1080 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13002 Color: 4
Size: 1942 Color: 0
Size: 384 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13033 Color: 3
Size: 1903 Color: 3
Size: 392 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 0
Size: 1746 Color: 4
Size: 528 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 0
Size: 1330 Color: 4
Size: 880 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 4
Size: 1896 Color: 0
Size: 240 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13186 Color: 1
Size: 1486 Color: 2
Size: 656 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13212 Color: 4
Size: 1604 Color: 1
Size: 512 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13298 Color: 4
Size: 1390 Color: 0
Size: 640 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 0
Size: 1464 Color: 1
Size: 576 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13382 Color: 4
Size: 1496 Color: 1
Size: 450 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 2
Size: 1272 Color: 2
Size: 690 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 1
Size: 1610 Color: 4
Size: 320 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 1
Size: 1528 Color: 4
Size: 396 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 4
Size: 1248 Color: 3
Size: 606 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13484 Color: 3
Size: 1308 Color: 0
Size: 536 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13550 Color: 4
Size: 1482 Color: 1
Size: 296 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 3
Size: 1352 Color: 3
Size: 464 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 4
Size: 1480 Color: 3
Size: 288 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 4
Size: 1276 Color: 1
Size: 448 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13620 Color: 1
Size: 1072 Color: 4
Size: 636 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 4
Size: 1388 Color: 2
Size: 304 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 0
Size: 1012 Color: 3
Size: 648 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13670 Color: 3
Size: 1444 Color: 0
Size: 214 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 2
Size: 1364 Color: 4
Size: 264 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 3
Size: 1040 Color: 1
Size: 568 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13734 Color: 0
Size: 1314 Color: 2
Size: 280 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13754 Color: 2
Size: 1302 Color: 2
Size: 272 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13766 Color: 0
Size: 1032 Color: 4
Size: 530 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 7678 Color: 1
Size: 6385 Color: 4
Size: 1264 Color: 3

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 9251 Color: 2
Size: 5692 Color: 4
Size: 384 Color: 2

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 10269 Color: 2
Size: 5058 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 10273 Color: 1
Size: 4446 Color: 2
Size: 608 Color: 4

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 10666 Color: 3
Size: 4217 Color: 4
Size: 444 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 2
Size: 3539 Color: 1
Size: 800 Color: 4

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 1
Size: 3833 Color: 2
Size: 460 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 11243 Color: 0
Size: 3592 Color: 0
Size: 492 Color: 4

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 11457 Color: 2
Size: 3582 Color: 3
Size: 288 Color: 4

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 11858 Color: 3
Size: 3181 Color: 1
Size: 288 Color: 4

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12137 Color: 4
Size: 2282 Color: 0
Size: 908 Color: 3

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 12293 Color: 4
Size: 2466 Color: 1
Size: 568 Color: 1

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 12354 Color: 1
Size: 2717 Color: 4
Size: 256 Color: 0

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 4
Size: 2482 Color: 3
Size: 280 Color: 1

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 12791 Color: 1
Size: 2152 Color: 2
Size: 384 Color: 4

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 12873 Color: 4
Size: 2106 Color: 0
Size: 348 Color: 1

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 12937 Color: 1
Size: 1638 Color: 4
Size: 752 Color: 2

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13023 Color: 4
Size: 1980 Color: 1
Size: 324 Color: 0

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 13004 Color: 1
Size: 2323 Color: 2

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 13045 Color: 2
Size: 1842 Color: 1
Size: 440 Color: 4

Bin 85: 1 of cap free
Amount of items: 2
Items: 
Size: 13406 Color: 1
Size: 1921 Color: 0

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 8538 Color: 2
Size: 6378 Color: 0
Size: 410 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 9922 Color: 4
Size: 4856 Color: 0
Size: 548 Color: 3

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 10026 Color: 4
Size: 4028 Color: 0
Size: 1272 Color: 3

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 10312 Color: 2
Size: 4422 Color: 0
Size: 592 Color: 4

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 10500 Color: 0
Size: 4426 Color: 4
Size: 400 Color: 2

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 11390 Color: 2
Size: 3676 Color: 4
Size: 260 Color: 0

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 11476 Color: 1
Size: 3530 Color: 4
Size: 320 Color: 3

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 11689 Color: 2
Size: 2661 Color: 4
Size: 976 Color: 3

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 4
Size: 3062 Color: 2
Size: 416 Color: 3

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 11913 Color: 2
Size: 2925 Color: 4
Size: 488 Color: 1

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 4
Size: 1898 Color: 1
Size: 1276 Color: 1

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 4
Size: 2226 Color: 1
Size: 848 Color: 1

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 12230 Color: 2
Size: 2836 Color: 4
Size: 260 Color: 0

Bin 99: 2 of cap free
Amount of items: 2
Items: 
Size: 12900 Color: 2
Size: 2426 Color: 1

Bin 100: 3 of cap free
Amount of items: 9
Items: 
Size: 7665 Color: 3
Size: 1168 Color: 4
Size: 1088 Color: 4
Size: 1056 Color: 0
Size: 1008 Color: 4
Size: 1000 Color: 2
Size: 928 Color: 1
Size: 928 Color: 0
Size: 484 Color: 4

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 4
Size: 5665 Color: 3
Size: 336 Color: 1

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 10757 Color: 4
Size: 4024 Color: 2
Size: 544 Color: 3

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 11594 Color: 2
Size: 3459 Color: 0
Size: 272 Color: 4

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 12256 Color: 4
Size: 2303 Color: 0
Size: 766 Color: 1

Bin 105: 3 of cap free
Amount of items: 2
Items: 
Size: 12181 Color: 3
Size: 3144 Color: 1

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 8788 Color: 4
Size: 6328 Color: 0
Size: 208 Color: 1

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 10018 Color: 2
Size: 4482 Color: 4
Size: 824 Color: 1

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 10924 Color: 2
Size: 2212 Color: 0
Size: 2188 Color: 4

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 3
Size: 3320 Color: 4
Size: 444 Color: 2

Bin 110: 4 of cap free
Amount of items: 3
Items: 
Size: 11932 Color: 0
Size: 3096 Color: 4
Size: 296 Color: 3

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 13504 Color: 1
Size: 1820 Color: 0

Bin 112: 4 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 1
Size: 1784 Color: 3

Bin 113: 4 of cap free
Amount of items: 2
Items: 
Size: 13702 Color: 2
Size: 1622 Color: 0

Bin 114: 5 of cap free
Amount of items: 5
Items: 
Size: 7667 Color: 2
Size: 2845 Color: 0
Size: 2623 Color: 4
Size: 1412 Color: 2
Size: 776 Color: 2

Bin 115: 5 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 3
Size: 5065 Color: 0
Size: 304 Color: 4

Bin 116: 5 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 1
Size: 3837 Color: 1
Size: 392 Color: 4

Bin 117: 5 of cap free
Amount of items: 3
Items: 
Size: 11338 Color: 3
Size: 2713 Color: 0
Size: 1272 Color: 4

Bin 118: 5 of cap free
Amount of items: 3
Items: 
Size: 12663 Color: 0
Size: 2564 Color: 1
Size: 96 Color: 0

Bin 119: 6 of cap free
Amount of items: 20
Items: 
Size: 960 Color: 3
Size: 944 Color: 3
Size: 928 Color: 1
Size: 912 Color: 1
Size: 884 Color: 4
Size: 880 Color: 2
Size: 856 Color: 0
Size: 842 Color: 4
Size: 842 Color: 1
Size: 836 Color: 3
Size: 832 Color: 2
Size: 800 Color: 2
Size: 776 Color: 4
Size: 766 Color: 4
Size: 760 Color: 2
Size: 704 Color: 0
Size: 608 Color: 0
Size: 560 Color: 0
Size: 440 Color: 2
Size: 192 Color: 0

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 11511 Color: 1
Size: 3811 Color: 3

Bin 121: 6 of cap free
Amount of items: 2
Items: 
Size: 12426 Color: 1
Size: 2896 Color: 2

Bin 122: 6 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 3
Size: 2648 Color: 1

Bin 123: 6 of cap free
Amount of items: 2
Items: 
Size: 13476 Color: 3
Size: 1846 Color: 1

Bin 124: 7 of cap free
Amount of items: 2
Items: 
Size: 12775 Color: 3
Size: 2546 Color: 1

Bin 125: 8 of cap free
Amount of items: 3
Items: 
Size: 10148 Color: 1
Size: 4164 Color: 4
Size: 1008 Color: 2

Bin 126: 8 of cap free
Amount of items: 2
Items: 
Size: 11915 Color: 1
Size: 3405 Color: 0

Bin 127: 8 of cap free
Amount of items: 2
Items: 
Size: 12038 Color: 0
Size: 3282 Color: 3

Bin 128: 9 of cap free
Amount of items: 3
Items: 
Size: 8531 Color: 1
Size: 6364 Color: 0
Size: 424 Color: 4

Bin 129: 10 of cap free
Amount of items: 3
Items: 
Size: 9262 Color: 3
Size: 5432 Color: 4
Size: 624 Color: 1

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 3
Size: 2690 Color: 0

Bin 131: 11 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 1
Size: 2047 Color: 2

Bin 132: 11 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 3
Size: 1993 Color: 0

Bin 133: 12 of cap free
Amount of items: 4
Items: 
Size: 7668 Color: 2
Size: 6356 Color: 4
Size: 1128 Color: 3
Size: 164 Color: 1

Bin 134: 12 of cap free
Amount of items: 3
Items: 
Size: 9660 Color: 4
Size: 4936 Color: 0
Size: 720 Color: 0

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 12865 Color: 3
Size: 2451 Color: 1

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 13598 Color: 0
Size: 1718 Color: 3

Bin 137: 13 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 2
Size: 2251 Color: 3

Bin 138: 14 of cap free
Amount of items: 3
Items: 
Size: 9753 Color: 0
Size: 5163 Color: 3
Size: 398 Color: 0

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 12389 Color: 0
Size: 2924 Color: 2

Bin 140: 16 of cap free
Amount of items: 3
Items: 
Size: 8844 Color: 1
Size: 4440 Color: 4
Size: 2028 Color: 0

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 0
Size: 1640 Color: 3

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 3
Size: 2163 Color: 0

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 8542 Color: 2
Size: 6768 Color: 3

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 12733 Color: 3
Size: 2577 Color: 2

Bin 145: 19 of cap free
Amount of items: 2
Items: 
Size: 10662 Color: 1
Size: 4647 Color: 2

Bin 146: 20 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 1
Size: 3268 Color: 0
Size: 1008 Color: 4

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 1
Size: 2728 Color: 2

Bin 148: 21 of cap free
Amount of items: 2
Items: 
Size: 12274 Color: 1
Size: 3033 Color: 0

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 10301 Color: 3
Size: 5004 Color: 0

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 0
Size: 2742 Color: 1

Bin 151: 25 of cap free
Amount of items: 6
Items: 
Size: 7666 Color: 3
Size: 2221 Color: 3
Size: 1940 Color: 4
Size: 1764 Color: 4
Size: 1520 Color: 3
Size: 192 Color: 1

Bin 152: 25 of cap free
Amount of items: 2
Items: 
Size: 9851 Color: 0
Size: 5452 Color: 3

Bin 153: 26 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 4
Size: 6862 Color: 3
Size: 192 Color: 1

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 11412 Color: 1
Size: 3890 Color: 3

Bin 155: 28 of cap free
Amount of items: 3
Items: 
Size: 9258 Color: 2
Size: 5658 Color: 3
Size: 384 Color: 2

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 12073 Color: 0
Size: 3227 Color: 1

Bin 157: 30 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 0
Size: 5662 Color: 2
Size: 620 Color: 4

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 11658 Color: 3
Size: 3640 Color: 2

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 3
Size: 1534 Color: 2

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 11083 Color: 1
Size: 4213 Color: 3

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 12956 Color: 1
Size: 2340 Color: 3

Bin 162: 33 of cap free
Amount of items: 2
Items: 
Size: 9512 Color: 3
Size: 5783 Color: 2

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 10729 Color: 2
Size: 4565 Color: 3

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 7708 Color: 0
Size: 7584 Color: 2

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 9416 Color: 0
Size: 5876 Color: 1

Bin 166: 36 of cap free
Amount of items: 2
Items: 
Size: 10968 Color: 2
Size: 4324 Color: 1

Bin 167: 36 of cap free
Amount of items: 2
Items: 
Size: 13048 Color: 0
Size: 2244 Color: 3

Bin 168: 38 of cap free
Amount of items: 2
Items: 
Size: 12802 Color: 0
Size: 2488 Color: 2

Bin 169: 38 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 2
Size: 1704 Color: 3
Size: 96 Color: 0

Bin 170: 40 of cap free
Amount of items: 3
Items: 
Size: 7676 Color: 1
Size: 6956 Color: 2
Size: 656 Color: 0

Bin 171: 41 of cap free
Amount of items: 2
Items: 
Size: 10725 Color: 0
Size: 4562 Color: 3

Bin 172: 41 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 3
Size: 2053 Color: 1

Bin 173: 43 of cap free
Amount of items: 2
Items: 
Size: 9133 Color: 3
Size: 6152 Color: 0

Bin 174: 46 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 2
Size: 1546 Color: 3

Bin 175: 47 of cap free
Amount of items: 2
Items: 
Size: 12069 Color: 2
Size: 3212 Color: 3

Bin 176: 47 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 0
Size: 1913 Color: 2

Bin 177: 48 of cap free
Amount of items: 2
Items: 
Size: 10008 Color: 0
Size: 5272 Color: 2

Bin 178: 50 of cap free
Amount of items: 32
Items: 
Size: 712 Color: 1
Size: 706 Color: 2
Size: 704 Color: 4
Size: 704 Color: 3
Size: 704 Color: 1
Size: 584 Color: 3
Size: 568 Color: 3
Size: 568 Color: 1
Size: 542 Color: 4
Size: 542 Color: 4
Size: 508 Color: 3
Size: 504 Color: 0
Size: 496 Color: 0
Size: 492 Color: 2
Size: 484 Color: 0
Size: 480 Color: 0
Size: 456 Color: 0
Size: 440 Color: 0
Size: 432 Color: 1
Size: 432 Color: 1
Size: 420 Color: 3
Size: 380 Color: 1
Size: 376 Color: 1
Size: 368 Color: 4
Size: 360 Color: 1
Size: 352 Color: 3
Size: 352 Color: 2
Size: 336 Color: 0
Size: 328 Color: 4
Size: 320 Color: 3
Size: 320 Color: 1
Size: 308 Color: 2

Bin 179: 52 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 1
Size: 6392 Color: 4
Size: 1132 Color: 2

Bin 180: 52 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 2
Size: 2906 Color: 1

Bin 181: 61 of cap free
Amount of items: 3
Items: 
Size: 8920 Color: 2
Size: 3816 Color: 1
Size: 2531 Color: 4

Bin 182: 65 of cap free
Amount of items: 2
Items: 
Size: 12149 Color: 3
Size: 3114 Color: 1

Bin 183: 66 of cap free
Amount of items: 2
Items: 
Size: 9858 Color: 4
Size: 5404 Color: 0

Bin 184: 71 of cap free
Amount of items: 2
Items: 
Size: 12410 Color: 1
Size: 2847 Color: 3

Bin 185: 80 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 1
Size: 2312 Color: 0

Bin 186: 84 of cap free
Amount of items: 2
Items: 
Size: 11624 Color: 3
Size: 3620 Color: 2

Bin 187: 84 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 1
Size: 1602 Color: 3

Bin 188: 88 of cap free
Amount of items: 2
Items: 
Size: 8860 Color: 2
Size: 6380 Color: 3

Bin 189: 88 of cap free
Amount of items: 2
Items: 
Size: 13546 Color: 4
Size: 1694 Color: 1

Bin 190: 90 of cap free
Amount of items: 2
Items: 
Size: 11352 Color: 0
Size: 3886 Color: 3

Bin 191: 90 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 3
Size: 2894 Color: 1

Bin 192: 91 of cap free
Amount of items: 2
Items: 
Size: 11179 Color: 0
Size: 4058 Color: 2

Bin 193: 102 of cap free
Amount of items: 2
Items: 
Size: 11900 Color: 3
Size: 3326 Color: 2

Bin 194: 107 of cap free
Amount of items: 2
Items: 
Size: 11030 Color: 3
Size: 4191 Color: 2

Bin 195: 111 of cap free
Amount of items: 3
Items: 
Size: 8529 Color: 0
Size: 4680 Color: 2
Size: 2008 Color: 1

Bin 196: 117 of cap free
Amount of items: 2
Items: 
Size: 8824 Color: 3
Size: 6387 Color: 2

Bin 197: 140 of cap free
Amount of items: 3
Items: 
Size: 7692 Color: 4
Size: 5062 Color: 3
Size: 2434 Color: 1

Bin 198: 146 of cap free
Amount of items: 3
Items: 
Size: 7672 Color: 1
Size: 6382 Color: 4
Size: 1128 Color: 1

Bin 199: 12002 of cap free
Amount of items: 12
Items: 
Size: 304 Color: 2
Size: 304 Color: 0
Size: 296 Color: 4
Size: 296 Color: 4
Size: 280 Color: 1
Size: 280 Color: 0
Size: 280 Color: 0
Size: 272 Color: 1
Size: 268 Color: 2
Size: 268 Color: 2
Size: 254 Color: 3
Size: 224 Color: 3

Total size: 3034944
Total free space: 15328

