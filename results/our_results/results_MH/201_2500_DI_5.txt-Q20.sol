Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 976 Color: 0
Size: 568 Color: 2
Size: 384 Color: 6
Size: 272 Color: 18
Size: 120 Color: 14
Size: 88 Color: 3
Size: 48 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 12
Size: 386 Color: 7
Size: 76 Color: 6

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1716 Color: 5
Size: 524 Color: 6
Size: 128 Color: 13
Size: 88 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 9
Size: 300 Color: 5
Size: 8 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 1
Size: 330 Color: 2
Size: 20 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 11
Size: 342 Color: 17
Size: 64 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2111 Color: 11
Size: 289 Color: 13
Size: 56 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 17
Size: 684 Color: 11
Size: 76 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 2
Size: 1023 Color: 0
Size: 204 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 19
Size: 404 Color: 1
Size: 24 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 11
Size: 282 Color: 9
Size: 56 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 8
Size: 535 Color: 10
Size: 106 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 9
Size: 1020 Color: 17
Size: 200 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 15
Size: 458 Color: 12
Size: 88 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 17
Size: 228 Color: 10
Size: 40 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 840 Color: 1
Size: 80 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 7
Size: 331 Color: 12
Size: 64 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 0
Size: 470 Color: 15
Size: 8 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 8
Size: 539 Color: 17
Size: 106 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 1
Size: 512 Color: 3
Size: 92 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 19
Size: 398 Color: 4
Size: 56 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 19
Size: 730 Color: 19
Size: 144 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 14
Size: 916 Color: 8
Size: 176 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 13
Size: 887 Color: 1
Size: 148 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 11
Size: 1021 Color: 0
Size: 204 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 14
Size: 866 Color: 17
Size: 164 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 10
Size: 232 Color: 6
Size: 32 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1924 Color: 7
Size: 444 Color: 7
Size: 88 Color: 1

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 1468 Color: 6
Size: 828 Color: 8
Size: 144 Color: 19
Size: 16 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 5
Size: 443 Color: 19
Size: 88 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 12
Size: 330 Color: 13
Size: 64 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 12
Size: 654 Color: 7
Size: 128 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 3
Size: 226 Color: 7
Size: 28 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 5
Size: 226 Color: 14
Size: 44 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1913 Color: 6
Size: 453 Color: 2
Size: 90 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 14
Size: 861 Color: 6
Size: 172 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 14
Size: 242 Color: 11
Size: 48 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 7
Size: 1022 Color: 16
Size: 204 Color: 12

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 0
Size: 637 Color: 0
Size: 126 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 15
Size: 262 Color: 9
Size: 48 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1991 Color: 13
Size: 389 Color: 17
Size: 76 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 9
Size: 381 Color: 0
Size: 74 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1827 Color: 2
Size: 525 Color: 6
Size: 104 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 0
Size: 260 Color: 4
Size: 96 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 6
Size: 457 Color: 8
Size: 90 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 4
Size: 554 Color: 0
Size: 108 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 15
Size: 633 Color: 6
Size: 126 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 3
Size: 751 Color: 3
Size: 148 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 14
Size: 1018 Color: 12
Size: 200 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 8
Size: 729 Color: 0
Size: 144 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 631 Color: 7
Size: 124 Color: 10

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 0
Size: 756 Color: 11
Size: 56 Color: 10

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 6
Size: 580 Color: 4
Size: 112 Color: 7

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 5
Size: 476 Color: 6
Size: 88 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 9
Size: 618 Color: 11
Size: 120 Color: 11

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 12
Size: 620 Color: 13
Size: 280 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 5
Size: 449 Color: 14
Size: 2 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 3
Size: 348 Color: 9
Size: 64 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 11
Size: 460 Color: 16
Size: 88 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 0
Size: 542 Color: 5
Size: 104 Color: 5

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 19
Size: 436 Color: 6
Size: 80 Color: 6

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 5
Size: 774 Color: 4
Size: 152 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 14
Size: 402 Color: 16
Size: 160 Color: 2

Bin 64: 0 of cap free
Amount of items: 4
Items: 
Size: 1442 Color: 11
Size: 918 Color: 8
Size: 48 Color: 18
Size: 48 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 4
Size: 253 Color: 3
Size: 50 Color: 10

Total size: 159640
Total free space: 0

