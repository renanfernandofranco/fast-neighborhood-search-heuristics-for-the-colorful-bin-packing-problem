Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 11
Size: 340 Color: 10
Size: 251 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 297 Color: 11
Size: 256 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 11
Size: 333 Color: 14
Size: 255 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 19
Size: 366 Color: 4
Size: 265 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 267 Color: 19
Size: 258 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 4
Size: 313 Color: 3
Size: 264 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 12
Size: 262 Color: 17
Size: 339 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 319 Color: 19
Size: 305 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7
Size: 304 Color: 8
Size: 300 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 10
Size: 277 Color: 12
Size: 261 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 14
Size: 302 Color: 11
Size: 254 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 18
Size: 347 Color: 10
Size: 251 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 8
Size: 361 Color: 16
Size: 257 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 307 Color: 16
Size: 282 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 14
Size: 314 Color: 13
Size: 260 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9
Size: 280 Color: 13
Size: 252 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 13
Size: 275 Color: 2
Size: 260 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 11
Size: 308 Color: 12
Size: 289 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 19
Size: 334 Color: 18
Size: 270 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9
Size: 269 Color: 12
Size: 258 Color: 6

Total size: 20000
Total free space: 0

