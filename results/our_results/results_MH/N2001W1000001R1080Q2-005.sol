Capicity Bin: 1000001
Lower Bound: 894

Bins used: 895
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 476783 Color: 0
Size: 388317 Color: 1
Size: 134901 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 706650 Color: 1
Size: 178927 Color: 0
Size: 114424 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 620643 Color: 1
Size: 197222 Color: 0
Size: 182136 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 671852 Color: 1
Size: 165208 Color: 0
Size: 162941 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 779723 Color: 1
Size: 115268 Color: 1
Size: 105010 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 689985 Color: 0
Size: 159528 Color: 1
Size: 150488 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 470267 Color: 1
Size: 387842 Color: 0
Size: 141892 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 433173 Color: 1
Size: 387779 Color: 1
Size: 179049 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 685857 Color: 0
Size: 158193 Color: 1
Size: 155951 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 753871 Color: 0
Size: 142071 Color: 0
Size: 104059 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 752860 Color: 1
Size: 137652 Color: 0
Size: 109489 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 676553 Color: 0
Size: 181063 Color: 0
Size: 142385 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 682564 Color: 1
Size: 162447 Color: 0
Size: 154990 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 418127 Color: 0
Size: 402245 Color: 0
Size: 179629 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 504958 Color: 0
Size: 311344 Color: 1
Size: 183699 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 393116 Color: 0
Size: 310428 Color: 1
Size: 296457 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 681809 Color: 1
Size: 164955 Color: 0
Size: 153237 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 758428 Color: 0
Size: 124062 Color: 1
Size: 117511 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 689565 Color: 0
Size: 163465 Color: 1
Size: 146971 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 672836 Color: 1
Size: 189193 Color: 1
Size: 137972 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 466184 Color: 1
Size: 406732 Color: 0
Size: 127085 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 758261 Color: 0
Size: 138407 Color: 0
Size: 103333 Color: 1

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 353175 Color: 1
Size: 328087 Color: 0
Size: 159988 Color: 0
Size: 158751 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 725147 Color: 0
Size: 154638 Color: 1
Size: 120216 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 505409 Color: 0
Size: 332935 Color: 1
Size: 161657 Color: 1

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 597405 Color: 1
Size: 402596 Color: 0

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 635625 Color: 0
Size: 364376 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 651512 Color: 1
Size: 185991 Color: 0
Size: 162498 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 691853 Color: 0
Size: 167021 Color: 1
Size: 141127 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 756691 Color: 0
Size: 124985 Color: 1
Size: 118325 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 444230 Color: 1
Size: 390562 Color: 0
Size: 165208 Color: 0

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 337598 Color: 0
Size: 335700 Color: 1
Size: 326702 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 722832 Color: 0
Size: 159259 Color: 1
Size: 117909 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 714208 Color: 0
Size: 169970 Color: 1
Size: 115822 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 713499 Color: 0
Size: 160458 Color: 1
Size: 126043 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 637260 Color: 0
Size: 185610 Color: 1
Size: 177130 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 460087 Color: 1
Size: 383859 Color: 0
Size: 156054 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 706790 Color: 1
Size: 173031 Color: 0
Size: 120179 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 713976 Color: 0
Size: 157896 Color: 1
Size: 128128 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 448825 Color: 1
Size: 378745 Color: 0
Size: 172430 Color: 0

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 694845 Color: 0
Size: 305155 Color: 1

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 711149 Color: 1
Size: 288851 Color: 0

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 775097 Color: 1
Size: 224903 Color: 0

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 457510 Color: 1
Size: 383760 Color: 0
Size: 158729 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 737005 Color: 0
Size: 136106 Color: 1
Size: 126888 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 469376 Color: 0
Size: 349219 Color: 0
Size: 181404 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 450835 Color: 1
Size: 292515 Color: 0
Size: 256649 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 766575 Color: 0
Size: 118784 Color: 1
Size: 114640 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 487059 Color: 0
Size: 388190 Color: 1
Size: 124750 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 768374 Color: 0
Size: 116914 Color: 1
Size: 114711 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 495973 Color: 1
Size: 391693 Color: 1
Size: 112333 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 554294 Color: 0
Size: 343973 Color: 1
Size: 101732 Color: 1

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 469136 Color: 0
Size: 334038 Color: 0
Size: 196825 Color: 1

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 704101 Color: 1
Size: 174894 Color: 1
Size: 121004 Color: 0

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 575147 Color: 0
Size: 284749 Color: 0
Size: 140103 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 492091 Color: 1
Size: 374967 Color: 1
Size: 132941 Color: 0

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 677960 Color: 0
Size: 179338 Color: 0
Size: 142700 Color: 1

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 457480 Color: 1
Size: 393258 Color: 1
Size: 149260 Color: 0

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 394570 Color: 1
Size: 370354 Color: 0
Size: 235074 Color: 1

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 636120 Color: 0
Size: 184511 Color: 1
Size: 179367 Color: 0

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 679894 Color: 1
Size: 171633 Color: 0
Size: 148471 Color: 1

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 682155 Color: 1
Size: 165196 Color: 0
Size: 152647 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 537736 Color: 1
Size: 331301 Color: 0
Size: 130961 Color: 1

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 717059 Color: 1
Size: 154329 Color: 1
Size: 128610 Color: 0

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 437591 Color: 1
Size: 369163 Color: 0
Size: 193244 Color: 1

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 767164 Color: 0
Size: 117451 Color: 1
Size: 115383 Color: 1

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 780798 Color: 1
Size: 112737 Color: 1
Size: 106462 Color: 0

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 622104 Color: 0
Size: 196424 Color: 1
Size: 181469 Color: 1

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 754883 Color: 0
Size: 130667 Color: 1
Size: 114447 Color: 0

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 452800 Color: 1
Size: 411844 Color: 0
Size: 135353 Color: 0

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 534961 Color: 0
Size: 465036 Color: 1

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 546147 Color: 0
Size: 453850 Color: 1

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 772806 Color: 0
Size: 121203 Color: 1
Size: 105987 Color: 1

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 532299 Color: 0
Size: 331575 Color: 1
Size: 136122 Color: 1

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 760065 Color: 0
Size: 137523 Color: 0
Size: 102408 Color: 1

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 754160 Color: 1
Size: 139060 Color: 1
Size: 106776 Color: 0

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 766331 Color: 1
Size: 117243 Color: 1
Size: 116422 Color: 0

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 679699 Color: 0
Size: 167802 Color: 1
Size: 152494 Color: 1

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 728788 Color: 1
Size: 146499 Color: 0
Size: 124708 Color: 1

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 541225 Color: 0
Size: 458770 Color: 1

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 577334 Color: 0
Size: 422661 Color: 1

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 558564 Color: 1
Size: 283686 Color: 0
Size: 157744 Color: 1

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 634074 Color: 0
Size: 184887 Color: 1
Size: 181033 Color: 0

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 535108 Color: 0
Size: 298636 Color: 0
Size: 166250 Color: 1

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 566391 Color: 0
Size: 267726 Color: 1
Size: 165876 Color: 1

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 744100 Color: 0
Size: 135431 Color: 1
Size: 120462 Color: 1

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 543205 Color: 0
Size: 456788 Color: 1

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 723449 Color: 0
Size: 276544 Color: 1

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 668013 Color: 0
Size: 331979 Color: 1

Bin 90: 11 of cap free
Amount of items: 3
Items: 
Size: 536279 Color: 1
Size: 298376 Color: 0
Size: 165335 Color: 1

Bin 91: 11 of cap free
Amount of items: 3
Items: 
Size: 635574 Color: 0
Size: 185881 Color: 0
Size: 178535 Color: 1

Bin 92: 11 of cap free
Amount of items: 2
Items: 
Size: 724844 Color: 0
Size: 275146 Color: 1

Bin 93: 12 of cap free
Amount of items: 3
Items: 
Size: 387291 Color: 1
Size: 360839 Color: 0
Size: 251859 Color: 0

Bin 94: 13 of cap free
Amount of items: 3
Items: 
Size: 715608 Color: 1
Size: 153169 Color: 0
Size: 131211 Color: 1

Bin 95: 13 of cap free
Amount of items: 3
Items: 
Size: 662493 Color: 1
Size: 176358 Color: 1
Size: 161137 Color: 0

Bin 96: 14 of cap free
Amount of items: 3
Items: 
Size: 784763 Color: 1
Size: 107786 Color: 0
Size: 107438 Color: 1

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 516534 Color: 1
Size: 483453 Color: 0

Bin 98: 15 of cap free
Amount of items: 3
Items: 
Size: 738461 Color: 0
Size: 157663 Color: 0
Size: 103862 Color: 1

Bin 99: 15 of cap free
Amount of items: 3
Items: 
Size: 676207 Color: 1
Size: 189468 Color: 1
Size: 134311 Color: 0

Bin 100: 16 of cap free
Amount of items: 3
Items: 
Size: 784710 Color: 0
Size: 113607 Color: 0
Size: 101668 Color: 1

Bin 101: 16 of cap free
Amount of items: 3
Items: 
Size: 550916 Color: 0
Size: 303602 Color: 0
Size: 145467 Color: 1

Bin 102: 16 of cap free
Amount of items: 3
Items: 
Size: 755587 Color: 0
Size: 126849 Color: 0
Size: 117549 Color: 1

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 586227 Color: 0
Size: 413758 Color: 1

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 765483 Color: 0
Size: 234502 Color: 1

Bin 105: 17 of cap free
Amount of items: 3
Items: 
Size: 686489 Color: 0
Size: 178774 Color: 1
Size: 134721 Color: 1

Bin 106: 17 of cap free
Amount of items: 3
Items: 
Size: 646104 Color: 0
Size: 180998 Color: 0
Size: 172882 Color: 1

Bin 107: 17 of cap free
Amount of items: 2
Items: 
Size: 712115 Color: 0
Size: 287869 Color: 1

Bin 108: 18 of cap free
Amount of items: 3
Items: 
Size: 387943 Color: 0
Size: 377650 Color: 0
Size: 234390 Color: 1

Bin 109: 18 of cap free
Amount of items: 3
Items: 
Size: 766265 Color: 1
Size: 117603 Color: 1
Size: 116115 Color: 0

Bin 110: 19 of cap free
Amount of items: 3
Items: 
Size: 565998 Color: 0
Size: 269694 Color: 1
Size: 164290 Color: 1

Bin 111: 19 of cap free
Amount of items: 3
Items: 
Size: 394578 Color: 1
Size: 384600 Color: 1
Size: 220804 Color: 0

Bin 112: 19 of cap free
Amount of items: 3
Items: 
Size: 451486 Color: 1
Size: 391913 Color: 1
Size: 156583 Color: 0

Bin 113: 20 of cap free
Amount of items: 3
Items: 
Size: 351786 Color: 1
Size: 329297 Color: 1
Size: 318898 Color: 0

Bin 114: 20 of cap free
Amount of items: 2
Items: 
Size: 614367 Color: 1
Size: 385614 Color: 0

Bin 115: 20 of cap free
Amount of items: 2
Items: 
Size: 635813 Color: 0
Size: 364168 Color: 1

Bin 116: 21 of cap free
Amount of items: 3
Items: 
Size: 458137 Color: 1
Size: 407468 Color: 0
Size: 134375 Color: 0

Bin 117: 21 of cap free
Amount of items: 3
Items: 
Size: 350005 Color: 0
Size: 340206 Color: 1
Size: 309769 Color: 0

Bin 118: 21 of cap free
Amount of items: 2
Items: 
Size: 635986 Color: 0
Size: 363994 Color: 1

Bin 119: 21 of cap free
Amount of items: 3
Items: 
Size: 766501 Color: 0
Size: 122167 Color: 0
Size: 111312 Color: 1

Bin 120: 22 of cap free
Amount of items: 3
Items: 
Size: 709988 Color: 0
Size: 151121 Color: 1
Size: 138870 Color: 1

Bin 121: 22 of cap free
Amount of items: 2
Items: 
Size: 517698 Color: 0
Size: 482281 Color: 1

Bin 122: 23 of cap free
Amount of items: 3
Items: 
Size: 684241 Color: 1
Size: 163707 Color: 0
Size: 152030 Color: 1

Bin 123: 23 of cap free
Amount of items: 3
Items: 
Size: 715452 Color: 0
Size: 154902 Color: 0
Size: 129624 Color: 1

Bin 124: 23 of cap free
Amount of items: 2
Items: 
Size: 667416 Color: 1
Size: 332562 Color: 0

Bin 125: 24 of cap free
Amount of items: 2
Items: 
Size: 658046 Color: 0
Size: 341931 Color: 1

Bin 126: 25 of cap free
Amount of items: 2
Items: 
Size: 698948 Color: 0
Size: 301028 Color: 1

Bin 127: 25 of cap free
Amount of items: 2
Items: 
Size: 729353 Color: 0
Size: 270623 Color: 1

Bin 128: 26 of cap free
Amount of items: 3
Items: 
Size: 388686 Color: 1
Size: 354913 Color: 0
Size: 256376 Color: 1

Bin 129: 26 of cap free
Amount of items: 3
Items: 
Size: 335009 Color: 1
Size: 334157 Color: 0
Size: 330809 Color: 0

Bin 130: 26 of cap free
Amount of items: 3
Items: 
Size: 757862 Color: 0
Size: 125382 Color: 1
Size: 116731 Color: 0

Bin 131: 27 of cap free
Amount of items: 3
Items: 
Size: 550409 Color: 1
Size: 320739 Color: 0
Size: 128826 Color: 0

Bin 132: 27 of cap free
Amount of items: 2
Items: 
Size: 519298 Color: 0
Size: 480676 Color: 1

Bin 133: 28 of cap free
Amount of items: 3
Items: 
Size: 548223 Color: 1
Size: 226704 Color: 0
Size: 225046 Color: 1

Bin 134: 28 of cap free
Amount of items: 3
Items: 
Size: 681582 Color: 0
Size: 177920 Color: 0
Size: 140471 Color: 1

Bin 135: 28 of cap free
Amount of items: 3
Items: 
Size: 635820 Color: 1
Size: 190730 Color: 1
Size: 173423 Color: 0

Bin 136: 29 of cap free
Amount of items: 3
Items: 
Size: 752598 Color: 0
Size: 129656 Color: 0
Size: 117718 Color: 1

Bin 137: 30 of cap free
Amount of items: 3
Items: 
Size: 673656 Color: 0
Size: 184266 Color: 1
Size: 142049 Color: 0

Bin 138: 30 of cap free
Amount of items: 2
Items: 
Size: 678033 Color: 1
Size: 321938 Color: 0

Bin 139: 31 of cap free
Amount of items: 2
Items: 
Size: 727562 Color: 1
Size: 272408 Color: 0

Bin 140: 31 of cap free
Amount of items: 2
Items: 
Size: 791345 Color: 0
Size: 208625 Color: 1

Bin 141: 32 of cap free
Amount of items: 3
Items: 
Size: 551548 Color: 0
Size: 293931 Color: 1
Size: 154490 Color: 1

Bin 142: 32 of cap free
Amount of items: 3
Items: 
Size: 498314 Color: 1
Size: 386882 Color: 0
Size: 114773 Color: 0

Bin 143: 33 of cap free
Amount of items: 3
Items: 
Size: 657564 Color: 1
Size: 177404 Color: 0
Size: 165000 Color: 0

Bin 144: 34 of cap free
Amount of items: 3
Items: 
Size: 383903 Color: 1
Size: 317661 Color: 0
Size: 298403 Color: 1

Bin 145: 34 of cap free
Amount of items: 2
Items: 
Size: 559898 Color: 1
Size: 440069 Color: 0

Bin 146: 34 of cap free
Amount of items: 2
Items: 
Size: 565097 Color: 0
Size: 434870 Color: 1

Bin 147: 34 of cap free
Amount of items: 2
Items: 
Size: 592242 Color: 0
Size: 407725 Color: 1

Bin 148: 35 of cap free
Amount of items: 3
Items: 
Size: 348928 Color: 0
Size: 344526 Color: 0
Size: 306512 Color: 1

Bin 149: 36 of cap free
Amount of items: 2
Items: 
Size: 554326 Color: 0
Size: 445639 Color: 1

Bin 150: 36 of cap free
Amount of items: 2
Items: 
Size: 538984 Color: 0
Size: 460981 Color: 1

Bin 151: 37 of cap free
Amount of items: 3
Items: 
Size: 786368 Color: 1
Size: 109740 Color: 1
Size: 103856 Color: 0

Bin 152: 37 of cap free
Amount of items: 2
Items: 
Size: 535712 Color: 0
Size: 464252 Color: 1

Bin 153: 37 of cap free
Amount of items: 3
Items: 
Size: 756855 Color: 0
Size: 130552 Color: 0
Size: 112557 Color: 1

Bin 154: 38 of cap free
Amount of items: 3
Items: 
Size: 482549 Color: 0
Size: 382126 Color: 1
Size: 135288 Color: 0

Bin 155: 38 of cap free
Amount of items: 3
Items: 
Size: 681944 Color: 0
Size: 169585 Color: 0
Size: 148434 Color: 1

Bin 156: 38 of cap free
Amount of items: 2
Items: 
Size: 680363 Color: 1
Size: 319600 Color: 0

Bin 157: 38 of cap free
Amount of items: 2
Items: 
Size: 678730 Color: 1
Size: 321233 Color: 0

Bin 158: 39 of cap free
Amount of items: 3
Items: 
Size: 559361 Color: 1
Size: 267218 Color: 0
Size: 173383 Color: 1

Bin 159: 40 of cap free
Amount of items: 3
Items: 
Size: 387809 Color: 1
Size: 362195 Color: 0
Size: 249957 Color: 0

Bin 160: 41 of cap free
Amount of items: 2
Items: 
Size: 717342 Color: 0
Size: 282618 Color: 1

Bin 161: 42 of cap free
Amount of items: 3
Items: 
Size: 679149 Color: 0
Size: 175144 Color: 0
Size: 145666 Color: 1

Bin 162: 42 of cap free
Amount of items: 2
Items: 
Size: 752416 Color: 0
Size: 247543 Color: 1

Bin 163: 43 of cap free
Amount of items: 2
Items: 
Size: 579801 Color: 1
Size: 420157 Color: 0

Bin 164: 44 of cap free
Amount of items: 2
Items: 
Size: 577070 Color: 0
Size: 422887 Color: 1

Bin 165: 44 of cap free
Amount of items: 2
Items: 
Size: 588059 Color: 0
Size: 411898 Color: 1

Bin 166: 45 of cap free
Amount of items: 2
Items: 
Size: 541471 Color: 1
Size: 458485 Color: 0

Bin 167: 46 of cap free
Amount of items: 3
Items: 
Size: 671212 Color: 0
Size: 170059 Color: 0
Size: 158684 Color: 1

Bin 168: 46 of cap free
Amount of items: 2
Items: 
Size: 525358 Color: 1
Size: 474597 Color: 0

Bin 169: 47 of cap free
Amount of items: 3
Items: 
Size: 651082 Color: 0
Size: 175124 Color: 1
Size: 173748 Color: 0

Bin 170: 48 of cap free
Amount of items: 2
Items: 
Size: 703264 Color: 1
Size: 296689 Color: 0

Bin 171: 48 of cap free
Amount of items: 2
Items: 
Size: 651497 Color: 1
Size: 348456 Color: 0

Bin 172: 49 of cap free
Amount of items: 3
Items: 
Size: 714862 Color: 1
Size: 155193 Color: 0
Size: 129897 Color: 1

Bin 173: 49 of cap free
Amount of items: 3
Items: 
Size: 711854 Color: 1
Size: 156804 Color: 0
Size: 131294 Color: 1

Bin 174: 49 of cap free
Amount of items: 2
Items: 
Size: 570729 Color: 1
Size: 429223 Color: 0

Bin 175: 49 of cap free
Amount of items: 2
Items: 
Size: 599044 Color: 0
Size: 400908 Color: 1

Bin 176: 50 of cap free
Amount of items: 2
Items: 
Size: 547703 Color: 1
Size: 452248 Color: 0

Bin 177: 53 of cap free
Amount of items: 2
Items: 
Size: 685623 Color: 1
Size: 314325 Color: 0

Bin 178: 55 of cap free
Amount of items: 7
Items: 
Size: 191247 Color: 0
Size: 178744 Color: 1
Size: 146783 Color: 1
Size: 146566 Color: 1
Size: 117701 Color: 0
Size: 117588 Color: 0
Size: 101317 Color: 0

Bin 179: 55 of cap free
Amount of items: 3
Items: 
Size: 565590 Color: 0
Size: 272876 Color: 0
Size: 161480 Color: 1

Bin 180: 56 of cap free
Amount of items: 2
Items: 
Size: 709905 Color: 1
Size: 290040 Color: 0

Bin 181: 56 of cap free
Amount of items: 2
Items: 
Size: 658891 Color: 1
Size: 341054 Color: 0

Bin 182: 57 of cap free
Amount of items: 2
Items: 
Size: 740554 Color: 1
Size: 259390 Color: 0

Bin 183: 57 of cap free
Amount of items: 3
Items: 
Size: 748837 Color: 0
Size: 126563 Color: 1
Size: 124544 Color: 0

Bin 184: 57 of cap free
Amount of items: 2
Items: 
Size: 599718 Color: 1
Size: 400226 Color: 0

Bin 185: 59 of cap free
Amount of items: 2
Items: 
Size: 606160 Color: 1
Size: 393782 Color: 0

Bin 186: 59 of cap free
Amount of items: 2
Items: 
Size: 637864 Color: 1
Size: 362078 Color: 0

Bin 187: 59 of cap free
Amount of items: 3
Items: 
Size: 771339 Color: 0
Size: 115386 Color: 1
Size: 113217 Color: 0

Bin 188: 59 of cap free
Amount of items: 2
Items: 
Size: 790434 Color: 0
Size: 209508 Color: 1

Bin 189: 61 of cap free
Amount of items: 2
Items: 
Size: 523603 Color: 0
Size: 476337 Color: 1

Bin 190: 61 of cap free
Amount of items: 2
Items: 
Size: 778077 Color: 1
Size: 221863 Color: 0

Bin 191: 62 of cap free
Amount of items: 3
Items: 
Size: 548354 Color: 1
Size: 290220 Color: 1
Size: 161365 Color: 0

Bin 192: 62 of cap free
Amount of items: 2
Items: 
Size: 521443 Color: 1
Size: 478496 Color: 0

Bin 193: 62 of cap free
Amount of items: 2
Items: 
Size: 770420 Color: 1
Size: 229519 Color: 0

Bin 194: 63 of cap free
Amount of items: 3
Items: 
Size: 576428 Color: 0
Size: 248224 Color: 0
Size: 175286 Color: 1

Bin 195: 63 of cap free
Amount of items: 2
Items: 
Size: 751637 Color: 1
Size: 248301 Color: 0

Bin 196: 63 of cap free
Amount of items: 2
Items: 
Size: 546477 Color: 0
Size: 453461 Color: 1

Bin 197: 64 of cap free
Amount of items: 3
Items: 
Size: 467358 Color: 0
Size: 335614 Color: 1
Size: 196965 Color: 1

Bin 198: 64 of cap free
Amount of items: 2
Items: 
Size: 795779 Color: 0
Size: 204158 Color: 1

Bin 199: 65 of cap free
Amount of items: 3
Items: 
Size: 739085 Color: 1
Size: 131800 Color: 1
Size: 129051 Color: 0

Bin 200: 65 of cap free
Amount of items: 2
Items: 
Size: 528367 Color: 1
Size: 471569 Color: 0

Bin 201: 65 of cap free
Amount of items: 2
Items: 
Size: 565651 Color: 1
Size: 434285 Color: 0

Bin 202: 65 of cap free
Amount of items: 2
Items: 
Size: 612572 Color: 0
Size: 387364 Color: 1

Bin 203: 66 of cap free
Amount of items: 2
Items: 
Size: 679067 Color: 1
Size: 320868 Color: 0

Bin 204: 66 of cap free
Amount of items: 2
Items: 
Size: 507104 Color: 1
Size: 492831 Color: 0

Bin 205: 66 of cap free
Amount of items: 2
Items: 
Size: 777441 Color: 0
Size: 222494 Color: 1

Bin 206: 67 of cap free
Amount of items: 3
Items: 
Size: 695427 Color: 1
Size: 163631 Color: 1
Size: 140876 Color: 0

Bin 207: 67 of cap free
Amount of items: 2
Items: 
Size: 596406 Color: 0
Size: 403528 Color: 1

Bin 208: 67 of cap free
Amount of items: 2
Items: 
Size: 726487 Color: 1
Size: 273447 Color: 0

Bin 209: 69 of cap free
Amount of items: 2
Items: 
Size: 518010 Color: 0
Size: 481922 Color: 1

Bin 210: 69 of cap free
Amount of items: 3
Items: 
Size: 617985 Color: 0
Size: 196661 Color: 1
Size: 185286 Color: 1

Bin 211: 70 of cap free
Amount of items: 3
Items: 
Size: 756412 Color: 0
Size: 128314 Color: 1
Size: 115205 Color: 0

Bin 212: 70 of cap free
Amount of items: 2
Items: 
Size: 705312 Color: 0
Size: 294619 Color: 1

Bin 213: 71 of cap free
Amount of items: 2
Items: 
Size: 670350 Color: 1
Size: 329580 Color: 0

Bin 214: 74 of cap free
Amount of items: 2
Items: 
Size: 628801 Color: 1
Size: 371126 Color: 0

Bin 215: 74 of cap free
Amount of items: 2
Items: 
Size: 745374 Color: 0
Size: 254553 Color: 1

Bin 216: 75 of cap free
Amount of items: 3
Items: 
Size: 720300 Color: 0
Size: 168999 Color: 1
Size: 110627 Color: 0

Bin 217: 78 of cap free
Amount of items: 3
Items: 
Size: 673186 Color: 0
Size: 199444 Color: 1
Size: 127293 Color: 0

Bin 218: 78 of cap free
Amount of items: 3
Items: 
Size: 693045 Color: 0
Size: 159969 Color: 1
Size: 146909 Color: 1

Bin 219: 78 of cap free
Amount of items: 2
Items: 
Size: 581234 Color: 1
Size: 418689 Color: 0

Bin 220: 79 of cap free
Amount of items: 3
Items: 
Size: 715729 Color: 0
Size: 159646 Color: 1
Size: 124547 Color: 0

Bin 221: 80 of cap free
Amount of items: 2
Items: 
Size: 672288 Color: 1
Size: 327633 Color: 0

Bin 222: 80 of cap free
Amount of items: 2
Items: 
Size: 725207 Color: 1
Size: 274714 Color: 0

Bin 223: 82 of cap free
Amount of items: 2
Items: 
Size: 589941 Color: 0
Size: 409978 Color: 1

Bin 224: 83 of cap free
Amount of items: 2
Items: 
Size: 701216 Color: 1
Size: 298702 Color: 0

Bin 225: 84 of cap free
Amount of items: 2
Items: 
Size: 504800 Color: 1
Size: 495117 Color: 0

Bin 226: 86 of cap free
Amount of items: 2
Items: 
Size: 690489 Color: 0
Size: 309426 Color: 1

Bin 227: 86 of cap free
Amount of items: 3
Items: 
Size: 676825 Color: 1
Size: 185541 Color: 0
Size: 137549 Color: 1

Bin 228: 89 of cap free
Amount of items: 3
Items: 
Size: 556096 Color: 1
Size: 285963 Color: 0
Size: 157853 Color: 1

Bin 229: 90 of cap free
Amount of items: 2
Items: 
Size: 722711 Color: 1
Size: 277200 Color: 0

Bin 230: 91 of cap free
Amount of items: 3
Items: 
Size: 673010 Color: 1
Size: 169353 Color: 0
Size: 157547 Color: 1

Bin 231: 91 of cap free
Amount of items: 2
Items: 
Size: 597410 Color: 0
Size: 402500 Color: 1

Bin 232: 93 of cap free
Amount of items: 2
Items: 
Size: 778839 Color: 0
Size: 221069 Color: 1

Bin 233: 94 of cap free
Amount of items: 2
Items: 
Size: 518612 Color: 0
Size: 481295 Color: 1

Bin 234: 94 of cap free
Amount of items: 2
Items: 
Size: 601466 Color: 0
Size: 398441 Color: 1

Bin 235: 98 of cap free
Amount of items: 3
Items: 
Size: 676817 Color: 0
Size: 180564 Color: 1
Size: 142522 Color: 0

Bin 236: 100 of cap free
Amount of items: 2
Items: 
Size: 596965 Color: 0
Size: 402936 Color: 1

Bin 237: 102 of cap free
Amount of items: 2
Items: 
Size: 514209 Color: 0
Size: 485690 Color: 1

Bin 238: 103 of cap free
Amount of items: 3
Items: 
Size: 675450 Color: 0
Size: 170166 Color: 1
Size: 154282 Color: 1

Bin 239: 103 of cap free
Amount of items: 2
Items: 
Size: 782047 Color: 0
Size: 217851 Color: 1

Bin 240: 105 of cap free
Amount of items: 3
Items: 
Size: 738225 Color: 0
Size: 133183 Color: 1
Size: 128488 Color: 0

Bin 241: 107 of cap free
Amount of items: 2
Items: 
Size: 574593 Color: 1
Size: 425301 Color: 0

Bin 242: 107 of cap free
Amount of items: 2
Items: 
Size: 626910 Color: 1
Size: 372984 Color: 0

Bin 243: 108 of cap free
Amount of items: 2
Items: 
Size: 641608 Color: 1
Size: 358285 Color: 0

Bin 244: 112 of cap free
Amount of items: 3
Items: 
Size: 735559 Color: 1
Size: 133799 Color: 0
Size: 130531 Color: 1

Bin 245: 112 of cap free
Amount of items: 2
Items: 
Size: 763884 Color: 1
Size: 236005 Color: 0

Bin 246: 113 of cap free
Amount of items: 3
Items: 
Size: 623181 Color: 0
Size: 194814 Color: 1
Size: 181893 Color: 0

Bin 247: 115 of cap free
Amount of items: 2
Items: 
Size: 665605 Color: 0
Size: 334281 Color: 1

Bin 248: 116 of cap free
Amount of items: 2
Items: 
Size: 702739 Color: 0
Size: 297146 Color: 1

Bin 249: 119 of cap free
Amount of items: 2
Items: 
Size: 577453 Color: 1
Size: 422429 Color: 0

Bin 250: 119 of cap free
Amount of items: 2
Items: 
Size: 583608 Color: 0
Size: 416274 Color: 1

Bin 251: 120 of cap free
Amount of items: 3
Items: 
Size: 717016 Color: 1
Size: 143723 Color: 0
Size: 139142 Color: 0

Bin 252: 121 of cap free
Amount of items: 2
Items: 
Size: 755241 Color: 0
Size: 244639 Color: 1

Bin 253: 121 of cap free
Amount of items: 3
Items: 
Size: 635240 Color: 0
Size: 186454 Color: 1
Size: 178186 Color: 1

Bin 254: 122 of cap free
Amount of items: 2
Items: 
Size: 692199 Color: 0
Size: 307680 Color: 1

Bin 255: 125 of cap free
Amount of items: 2
Items: 
Size: 774795 Color: 1
Size: 225081 Color: 0

Bin 256: 126 of cap free
Amount of items: 3
Items: 
Size: 551640 Color: 0
Size: 311947 Color: 1
Size: 136288 Color: 0

Bin 257: 127 of cap free
Amount of items: 2
Items: 
Size: 730676 Color: 0
Size: 269198 Color: 1

Bin 258: 130 of cap free
Amount of items: 3
Items: 
Size: 693048 Color: 1
Size: 163113 Color: 1
Size: 143710 Color: 0

Bin 259: 130 of cap free
Amount of items: 3
Items: 
Size: 655663 Color: 1
Size: 172974 Color: 1
Size: 171234 Color: 0

Bin 260: 131 of cap free
Amount of items: 2
Items: 
Size: 661751 Color: 0
Size: 338119 Color: 1

Bin 261: 131 of cap free
Amount of items: 2
Items: 
Size: 688338 Color: 0
Size: 311532 Color: 1

Bin 262: 133 of cap free
Amount of items: 2
Items: 
Size: 573781 Color: 1
Size: 426087 Color: 0

Bin 263: 133 of cap free
Amount of items: 2
Items: 
Size: 648046 Color: 0
Size: 351822 Color: 1

Bin 264: 133 of cap free
Amount of items: 2
Items: 
Size: 707668 Color: 0
Size: 292200 Color: 1

Bin 265: 134 of cap free
Amount of items: 2
Items: 
Size: 683726 Color: 1
Size: 316141 Color: 0

Bin 266: 134 of cap free
Amount of items: 2
Items: 
Size: 725745 Color: 1
Size: 274122 Color: 0

Bin 267: 136 of cap free
Amount of items: 2
Items: 
Size: 662841 Color: 0
Size: 337024 Color: 1

Bin 268: 136 of cap free
Amount of items: 2
Items: 
Size: 690233 Color: 1
Size: 309632 Color: 0

Bin 269: 136 of cap free
Amount of items: 2
Items: 
Size: 723390 Color: 0
Size: 276475 Color: 1

Bin 270: 138 of cap free
Amount of items: 2
Items: 
Size: 666908 Color: 0
Size: 332955 Color: 1

Bin 271: 139 of cap free
Amount of items: 2
Items: 
Size: 566881 Color: 0
Size: 432981 Color: 1

Bin 272: 141 of cap free
Amount of items: 2
Items: 
Size: 776764 Color: 1
Size: 223096 Color: 0

Bin 273: 142 of cap free
Amount of items: 2
Items: 
Size: 730887 Color: 1
Size: 268972 Color: 0

Bin 274: 143 of cap free
Amount of items: 2
Items: 
Size: 653880 Color: 1
Size: 345978 Color: 0

Bin 275: 144 of cap free
Amount of items: 2
Items: 
Size: 502533 Color: 1
Size: 497324 Color: 0

Bin 276: 146 of cap free
Amount of items: 2
Items: 
Size: 707899 Color: 0
Size: 291956 Color: 1

Bin 277: 148 of cap free
Amount of items: 2
Items: 
Size: 758003 Color: 0
Size: 241850 Color: 1

Bin 278: 152 of cap free
Amount of items: 3
Items: 
Size: 734766 Color: 1
Size: 147331 Color: 0
Size: 117752 Color: 0

Bin 279: 152 of cap free
Amount of items: 2
Items: 
Size: 533357 Color: 1
Size: 466492 Color: 0

Bin 280: 152 of cap free
Amount of items: 2
Items: 
Size: 707978 Color: 1
Size: 291871 Color: 0

Bin 281: 154 of cap free
Amount of items: 2
Items: 
Size: 556024 Color: 0
Size: 443823 Color: 1

Bin 282: 154 of cap free
Amount of items: 2
Items: 
Size: 582824 Color: 0
Size: 417023 Color: 1

Bin 283: 157 of cap free
Amount of items: 2
Items: 
Size: 731749 Color: 1
Size: 268095 Color: 0

Bin 284: 158 of cap free
Amount of items: 2
Items: 
Size: 503207 Color: 0
Size: 496636 Color: 1

Bin 285: 158 of cap free
Amount of items: 2
Items: 
Size: 666419 Color: 1
Size: 333424 Color: 0

Bin 286: 160 of cap free
Amount of items: 2
Items: 
Size: 611687 Color: 1
Size: 388154 Color: 0

Bin 287: 160 of cap free
Amount of items: 2
Items: 
Size: 634236 Color: 1
Size: 365605 Color: 0

Bin 288: 163 of cap free
Amount of items: 2
Items: 
Size: 757353 Color: 1
Size: 242485 Color: 0

Bin 289: 163 of cap free
Amount of items: 2
Items: 
Size: 525164 Color: 0
Size: 474674 Color: 1

Bin 290: 164 of cap free
Amount of items: 2
Items: 
Size: 552447 Color: 0
Size: 447390 Color: 1

Bin 291: 166 of cap free
Amount of items: 2
Items: 
Size: 750218 Color: 1
Size: 249617 Color: 0

Bin 292: 167 of cap free
Amount of items: 2
Items: 
Size: 671725 Color: 0
Size: 328109 Color: 1

Bin 293: 167 of cap free
Amount of items: 2
Items: 
Size: 795016 Color: 0
Size: 204818 Color: 1

Bin 294: 169 of cap free
Amount of items: 2
Items: 
Size: 614349 Color: 0
Size: 385483 Color: 1

Bin 295: 170 of cap free
Amount of items: 2
Items: 
Size: 657012 Color: 1
Size: 342819 Color: 0

Bin 296: 170 of cap free
Amount of items: 3
Items: 
Size: 463844 Color: 0
Size: 345971 Color: 1
Size: 190016 Color: 1

Bin 297: 170 of cap free
Amount of items: 2
Items: 
Size: 510709 Color: 1
Size: 489122 Color: 0

Bin 298: 171 of cap free
Amount of items: 2
Items: 
Size: 603505 Color: 0
Size: 396325 Color: 1

Bin 299: 172 of cap free
Amount of items: 2
Items: 
Size: 785611 Color: 1
Size: 214218 Color: 0

Bin 300: 175 of cap free
Amount of items: 2
Items: 
Size: 560130 Color: 1
Size: 439696 Color: 0

Bin 301: 180 of cap free
Amount of items: 2
Items: 
Size: 744609 Color: 1
Size: 255212 Color: 0

Bin 302: 183 of cap free
Amount of items: 2
Items: 
Size: 576373 Color: 1
Size: 423445 Color: 0

Bin 303: 185 of cap free
Amount of items: 3
Items: 
Size: 349923 Color: 0
Size: 342238 Color: 1
Size: 307655 Color: 0

Bin 304: 186 of cap free
Amount of items: 3
Items: 
Size: 754561 Color: 0
Size: 127837 Color: 1
Size: 117417 Color: 0

Bin 305: 187 of cap free
Amount of items: 2
Items: 
Size: 766135 Color: 1
Size: 233679 Color: 0

Bin 306: 190 of cap free
Amount of items: 3
Items: 
Size: 710159 Color: 0
Size: 161498 Color: 1
Size: 128154 Color: 0

Bin 307: 191 of cap free
Amount of items: 3
Items: 
Size: 554707 Color: 1
Size: 274132 Color: 1
Size: 170971 Color: 0

Bin 308: 192 of cap free
Amount of items: 2
Items: 
Size: 619913 Color: 1
Size: 379896 Color: 0

Bin 309: 192 of cap free
Amount of items: 2
Items: 
Size: 725381 Color: 0
Size: 274428 Color: 1

Bin 310: 193 of cap free
Amount of items: 2
Items: 
Size: 562006 Color: 1
Size: 437802 Color: 0

Bin 311: 193 of cap free
Amount of items: 2
Items: 
Size: 568733 Color: 0
Size: 431075 Color: 1

Bin 312: 194 of cap free
Amount of items: 2
Items: 
Size: 764182 Color: 1
Size: 235625 Color: 0

Bin 313: 197 of cap free
Amount of items: 2
Items: 
Size: 653491 Color: 0
Size: 346313 Color: 1

Bin 314: 201 of cap free
Amount of items: 2
Items: 
Size: 537603 Color: 1
Size: 462197 Color: 0

Bin 315: 201 of cap free
Amount of items: 2
Items: 
Size: 549257 Color: 1
Size: 450543 Color: 0

Bin 316: 201 of cap free
Amount of items: 2
Items: 
Size: 740684 Color: 0
Size: 259116 Color: 1

Bin 317: 206 of cap free
Amount of items: 2
Items: 
Size: 618835 Color: 1
Size: 380960 Color: 0

Bin 318: 211 of cap free
Amount of items: 3
Items: 
Size: 704067 Color: 0
Size: 160911 Color: 1
Size: 134812 Color: 1

Bin 319: 212 of cap free
Amount of items: 2
Items: 
Size: 536115 Color: 0
Size: 463674 Color: 1

Bin 320: 212 of cap free
Amount of items: 2
Items: 
Size: 588776 Color: 0
Size: 411013 Color: 1

Bin 321: 213 of cap free
Amount of items: 3
Items: 
Size: 634737 Color: 0
Size: 187804 Color: 1
Size: 177247 Color: 1

Bin 322: 213 of cap free
Amount of items: 2
Items: 
Size: 510344 Color: 1
Size: 489444 Color: 0

Bin 323: 218 of cap free
Amount of items: 2
Items: 
Size: 710918 Color: 0
Size: 288865 Color: 1

Bin 324: 221 of cap free
Amount of items: 2
Items: 
Size: 517853 Color: 1
Size: 481927 Color: 0

Bin 325: 223 of cap free
Amount of items: 3
Items: 
Size: 457849 Color: 1
Size: 373597 Color: 0
Size: 168332 Color: 0

Bin 326: 225 of cap free
Amount of items: 2
Items: 
Size: 746195 Color: 1
Size: 253581 Color: 0

Bin 327: 225 of cap free
Amount of items: 2
Items: 
Size: 709213 Color: 0
Size: 290563 Color: 1

Bin 328: 227 of cap free
Amount of items: 2
Items: 
Size: 589677 Color: 1
Size: 410097 Color: 0

Bin 329: 230 of cap free
Amount of items: 2
Items: 
Size: 678827 Color: 0
Size: 320944 Color: 1

Bin 330: 233 of cap free
Amount of items: 3
Items: 
Size: 737839 Color: 1
Size: 133773 Color: 1
Size: 128156 Color: 0

Bin 331: 236 of cap free
Amount of items: 2
Items: 
Size: 603574 Color: 1
Size: 396191 Color: 0

Bin 332: 236 of cap free
Amount of items: 3
Items: 
Size: 781516 Color: 0
Size: 114467 Color: 0
Size: 103782 Color: 1

Bin 333: 237 of cap free
Amount of items: 2
Items: 
Size: 557871 Color: 1
Size: 441893 Color: 0

Bin 334: 239 of cap free
Amount of items: 2
Items: 
Size: 751093 Color: 1
Size: 248669 Color: 0

Bin 335: 239 of cap free
Amount of items: 2
Items: 
Size: 546101 Color: 0
Size: 453661 Color: 1

Bin 336: 239 of cap free
Amount of items: 2
Items: 
Size: 568394 Color: 0
Size: 431368 Color: 1

Bin 337: 239 of cap free
Amount of items: 2
Items: 
Size: 624107 Color: 0
Size: 375655 Color: 1

Bin 338: 242 of cap free
Amount of items: 2
Items: 
Size: 735391 Color: 0
Size: 264368 Color: 1

Bin 339: 244 of cap free
Amount of items: 2
Items: 
Size: 645084 Color: 0
Size: 354673 Color: 1

Bin 340: 247 of cap free
Amount of items: 5
Items: 
Size: 333199 Color: 1
Size: 177961 Color: 1
Size: 173497 Color: 0
Size: 160795 Color: 0
Size: 154302 Color: 0

Bin 341: 247 of cap free
Amount of items: 2
Items: 
Size: 591341 Color: 1
Size: 408413 Color: 0

Bin 342: 247 of cap free
Amount of items: 2
Items: 
Size: 594808 Color: 0
Size: 404946 Color: 1

Bin 343: 250 of cap free
Amount of items: 3
Items: 
Size: 650564 Color: 0
Size: 184424 Color: 1
Size: 164763 Color: 1

Bin 344: 253 of cap free
Amount of items: 2
Items: 
Size: 605453 Color: 0
Size: 394295 Color: 1

Bin 345: 255 of cap free
Amount of items: 2
Items: 
Size: 540666 Color: 0
Size: 459080 Color: 1

Bin 346: 256 of cap free
Amount of items: 3
Items: 
Size: 712376 Color: 0
Size: 160638 Color: 1
Size: 126731 Color: 1

Bin 347: 256 of cap free
Amount of items: 2
Items: 
Size: 746864 Color: 1
Size: 252881 Color: 0

Bin 348: 256 of cap free
Amount of items: 2
Items: 
Size: 591775 Color: 1
Size: 407970 Color: 0

Bin 349: 256 of cap free
Amount of items: 2
Items: 
Size: 706929 Color: 0
Size: 292816 Color: 1

Bin 350: 257 of cap free
Amount of items: 2
Items: 
Size: 791251 Color: 1
Size: 208493 Color: 0

Bin 351: 258 of cap free
Amount of items: 2
Items: 
Size: 672677 Color: 1
Size: 327066 Color: 0

Bin 352: 260 of cap free
Amount of items: 2
Items: 
Size: 638295 Color: 1
Size: 361446 Color: 0

Bin 353: 262 of cap free
Amount of items: 2
Items: 
Size: 758223 Color: 0
Size: 241516 Color: 1

Bin 354: 262 of cap free
Amount of items: 2
Items: 
Size: 596996 Color: 1
Size: 402743 Color: 0

Bin 355: 263 of cap free
Amount of items: 2
Items: 
Size: 503863 Color: 1
Size: 495875 Color: 0

Bin 356: 263 of cap free
Amount of items: 2
Items: 
Size: 680473 Color: 0
Size: 319265 Color: 1

Bin 357: 264 of cap free
Amount of items: 2
Items: 
Size: 761164 Color: 1
Size: 238573 Color: 0

Bin 358: 265 of cap free
Amount of items: 2
Items: 
Size: 585146 Color: 0
Size: 414590 Color: 1

Bin 359: 265 of cap free
Amount of items: 2
Items: 
Size: 628562 Color: 0
Size: 371174 Color: 1

Bin 360: 266 of cap free
Amount of items: 2
Items: 
Size: 767268 Color: 0
Size: 232467 Color: 1

Bin 361: 269 of cap free
Amount of items: 2
Items: 
Size: 588769 Color: 0
Size: 410963 Color: 1

Bin 362: 270 of cap free
Amount of items: 2
Items: 
Size: 660233 Color: 1
Size: 339498 Color: 0

Bin 363: 274 of cap free
Amount of items: 2
Items: 
Size: 643079 Color: 0
Size: 356648 Color: 1

Bin 364: 275 of cap free
Amount of items: 2
Items: 
Size: 601699 Color: 1
Size: 398027 Color: 0

Bin 365: 277 of cap free
Amount of items: 2
Items: 
Size: 763239 Color: 0
Size: 236485 Color: 1

Bin 366: 277 of cap free
Amount of items: 2
Items: 
Size: 668505 Color: 0
Size: 331219 Color: 1

Bin 367: 278 of cap free
Amount of items: 2
Items: 
Size: 549211 Color: 0
Size: 450512 Color: 1

Bin 368: 279 of cap free
Amount of items: 2
Items: 
Size: 768364 Color: 0
Size: 231358 Color: 1

Bin 369: 280 of cap free
Amount of items: 2
Items: 
Size: 506321 Color: 0
Size: 493400 Color: 1

Bin 370: 282 of cap free
Amount of items: 2
Items: 
Size: 640870 Color: 0
Size: 358849 Color: 1

Bin 371: 283 of cap free
Amount of items: 2
Items: 
Size: 687483 Color: 1
Size: 312235 Color: 0

Bin 372: 286 of cap free
Amount of items: 2
Items: 
Size: 721598 Color: 1
Size: 278117 Color: 0

Bin 373: 290 of cap free
Amount of items: 2
Items: 
Size: 637993 Color: 1
Size: 361718 Color: 0

Bin 374: 290 of cap free
Amount of items: 2
Items: 
Size: 616584 Color: 0
Size: 383127 Color: 1

Bin 375: 292 of cap free
Amount of items: 2
Items: 
Size: 781742 Color: 1
Size: 217967 Color: 0

Bin 376: 295 of cap free
Amount of items: 2
Items: 
Size: 743379 Color: 0
Size: 256327 Color: 1

Bin 377: 296 of cap free
Amount of items: 2
Items: 
Size: 577143 Color: 0
Size: 422562 Color: 1

Bin 378: 297 of cap free
Amount of items: 3
Items: 
Size: 747317 Color: 0
Size: 139649 Color: 1
Size: 112738 Color: 0

Bin 379: 299 of cap free
Amount of items: 2
Items: 
Size: 598554 Color: 1
Size: 401148 Color: 0

Bin 380: 300 of cap free
Amount of items: 2
Items: 
Size: 611288 Color: 1
Size: 388413 Color: 0

Bin 381: 301 of cap free
Amount of items: 2
Items: 
Size: 774420 Color: 0
Size: 225280 Color: 1

Bin 382: 302 of cap free
Amount of items: 2
Items: 
Size: 602231 Color: 1
Size: 397468 Color: 0

Bin 383: 304 of cap free
Amount of items: 3
Items: 
Size: 455939 Color: 1
Size: 380322 Color: 0
Size: 163436 Color: 0

Bin 384: 306 of cap free
Amount of items: 3
Items: 
Size: 575891 Color: 0
Size: 271949 Color: 1
Size: 151855 Color: 1

Bin 385: 306 of cap free
Amount of items: 2
Items: 
Size: 751288 Color: 0
Size: 248407 Color: 1

Bin 386: 307 of cap free
Amount of items: 2
Items: 
Size: 563276 Color: 0
Size: 436418 Color: 1

Bin 387: 307 of cap free
Amount of items: 2
Items: 
Size: 677061 Color: 0
Size: 322633 Color: 1

Bin 388: 313 of cap free
Amount of items: 2
Items: 
Size: 615134 Color: 0
Size: 384554 Color: 1

Bin 389: 313 of cap free
Amount of items: 2
Items: 
Size: 775547 Color: 1
Size: 224141 Color: 0

Bin 390: 314 of cap free
Amount of items: 2
Items: 
Size: 657195 Color: 0
Size: 342492 Color: 1

Bin 391: 315 of cap free
Amount of items: 2
Items: 
Size: 713961 Color: 1
Size: 285725 Color: 0

Bin 392: 316 of cap free
Amount of items: 2
Items: 
Size: 681082 Color: 0
Size: 318603 Color: 1

Bin 393: 323 of cap free
Amount of items: 2
Items: 
Size: 526025 Color: 1
Size: 473653 Color: 0

Bin 394: 324 of cap free
Amount of items: 2
Items: 
Size: 798420 Color: 1
Size: 201257 Color: 0

Bin 395: 325 of cap free
Amount of items: 3
Items: 
Size: 467808 Color: 0
Size: 360145 Color: 0
Size: 171723 Color: 1

Bin 396: 329 of cap free
Amount of items: 3
Items: 
Size: 572384 Color: 0
Size: 262424 Color: 1
Size: 164864 Color: 0

Bin 397: 330 of cap free
Amount of items: 2
Items: 
Size: 759215 Color: 0
Size: 240456 Color: 1

Bin 398: 330 of cap free
Amount of items: 2
Items: 
Size: 781742 Color: 1
Size: 217929 Color: 0

Bin 399: 332 of cap free
Amount of items: 2
Items: 
Size: 706823 Color: 1
Size: 292846 Color: 0

Bin 400: 332 of cap free
Amount of items: 2
Items: 
Size: 530133 Color: 1
Size: 469536 Color: 0

Bin 401: 332 of cap free
Amount of items: 2
Items: 
Size: 533290 Color: 1
Size: 466379 Color: 0

Bin 402: 333 of cap free
Amount of items: 2
Items: 
Size: 585110 Color: 0
Size: 414558 Color: 1

Bin 403: 335 of cap free
Amount of items: 2
Items: 
Size: 622652 Color: 0
Size: 377014 Color: 1

Bin 404: 341 of cap free
Amount of items: 2
Items: 
Size: 706021 Color: 0
Size: 293639 Color: 1

Bin 405: 343 of cap free
Amount of items: 2
Items: 
Size: 780436 Color: 1
Size: 219222 Color: 0

Bin 406: 343 of cap free
Amount of items: 2
Items: 
Size: 788222 Color: 0
Size: 211436 Color: 1

Bin 407: 353 of cap free
Amount of items: 2
Items: 
Size: 673905 Color: 0
Size: 325743 Color: 1

Bin 408: 354 of cap free
Amount of items: 2
Items: 
Size: 509937 Color: 0
Size: 489710 Color: 1

Bin 409: 354 of cap free
Amount of items: 2
Items: 
Size: 761577 Color: 0
Size: 238070 Color: 1

Bin 410: 356 of cap free
Amount of items: 2
Items: 
Size: 647853 Color: 1
Size: 351792 Color: 0

Bin 411: 357 of cap free
Amount of items: 2
Items: 
Size: 573244 Color: 0
Size: 426400 Color: 1

Bin 412: 362 of cap free
Amount of items: 2
Items: 
Size: 593636 Color: 0
Size: 406003 Color: 1

Bin 413: 364 of cap free
Amount of items: 2
Items: 
Size: 729887 Color: 1
Size: 269750 Color: 0

Bin 414: 365 of cap free
Amount of items: 2
Items: 
Size: 523807 Color: 0
Size: 475829 Color: 1

Bin 415: 365 of cap free
Amount of items: 2
Items: 
Size: 647477 Color: 0
Size: 352159 Color: 1

Bin 416: 366 of cap free
Amount of items: 2
Items: 
Size: 643465 Color: 0
Size: 356170 Color: 1

Bin 417: 370 of cap free
Amount of items: 2
Items: 
Size: 685952 Color: 0
Size: 313679 Color: 1

Bin 418: 377 of cap free
Amount of items: 2
Items: 
Size: 559402 Color: 1
Size: 440222 Color: 0

Bin 419: 378 of cap free
Amount of items: 2
Items: 
Size: 669311 Color: 0
Size: 330312 Color: 1

Bin 420: 379 of cap free
Amount of items: 2
Items: 
Size: 631424 Color: 0
Size: 368198 Color: 1

Bin 421: 386 of cap free
Amount of items: 2
Items: 
Size: 760448 Color: 1
Size: 239167 Color: 0

Bin 422: 389 of cap free
Amount of items: 2
Items: 
Size: 655870 Color: 1
Size: 343742 Color: 0

Bin 423: 391 of cap free
Amount of items: 2
Items: 
Size: 568503 Color: 1
Size: 431107 Color: 0

Bin 424: 393 of cap free
Amount of items: 2
Items: 
Size: 693285 Color: 0
Size: 306323 Color: 1

Bin 425: 395 of cap free
Amount of items: 2
Items: 
Size: 676717 Color: 1
Size: 322889 Color: 0

Bin 426: 397 of cap free
Amount of items: 2
Items: 
Size: 536224 Color: 1
Size: 463380 Color: 0

Bin 427: 399 of cap free
Amount of items: 2
Items: 
Size: 583405 Color: 0
Size: 416197 Color: 1

Bin 428: 399 of cap free
Amount of items: 2
Items: 
Size: 601591 Color: 1
Size: 398011 Color: 0

Bin 429: 402 of cap free
Amount of items: 2
Items: 
Size: 708344 Color: 1
Size: 291255 Color: 0

Bin 430: 404 of cap free
Amount of items: 2
Items: 
Size: 766771 Color: 1
Size: 232826 Color: 0

Bin 431: 404 of cap free
Amount of items: 2
Items: 
Size: 535395 Color: 0
Size: 464202 Color: 1

Bin 432: 404 of cap free
Amount of items: 2
Items: 
Size: 620372 Color: 0
Size: 379225 Color: 1

Bin 433: 408 of cap free
Amount of items: 2
Items: 
Size: 774723 Color: 0
Size: 224870 Color: 1

Bin 434: 409 of cap free
Amount of items: 2
Items: 
Size: 741682 Color: 1
Size: 257910 Color: 0

Bin 435: 409 of cap free
Amount of items: 3
Items: 
Size: 634099 Color: 0
Size: 182840 Color: 1
Size: 182653 Color: 1

Bin 436: 413 of cap free
Amount of items: 2
Items: 
Size: 721472 Color: 1
Size: 278116 Color: 0

Bin 437: 414 of cap free
Amount of items: 2
Items: 
Size: 618292 Color: 0
Size: 381295 Color: 1

Bin 438: 417 of cap free
Amount of items: 2
Items: 
Size: 753700 Color: 1
Size: 245884 Color: 0

Bin 439: 427 of cap free
Amount of items: 2
Items: 
Size: 506864 Color: 1
Size: 492710 Color: 0

Bin 440: 430 of cap free
Amount of items: 2
Items: 
Size: 585422 Color: 1
Size: 414149 Color: 0

Bin 441: 434 of cap free
Amount of items: 2
Items: 
Size: 667318 Color: 1
Size: 332249 Color: 0

Bin 442: 436 of cap free
Amount of items: 2
Items: 
Size: 604243 Color: 1
Size: 395322 Color: 0

Bin 443: 442 of cap free
Amount of items: 2
Items: 
Size: 782348 Color: 1
Size: 217211 Color: 0

Bin 444: 452 of cap free
Amount of items: 2
Items: 
Size: 641781 Color: 1
Size: 357768 Color: 0

Bin 445: 454 of cap free
Amount of items: 2
Items: 
Size: 520901 Color: 1
Size: 478646 Color: 0

Bin 446: 458 of cap free
Amount of items: 2
Items: 
Size: 692347 Color: 1
Size: 307196 Color: 0

Bin 447: 460 of cap free
Amount of items: 2
Items: 
Size: 525260 Color: 1
Size: 474281 Color: 0

Bin 448: 461 of cap free
Amount of items: 2
Items: 
Size: 537488 Color: 0
Size: 462052 Color: 1

Bin 449: 463 of cap free
Amount of items: 2
Items: 
Size: 569967 Color: 1
Size: 429571 Color: 0

Bin 450: 464 of cap free
Amount of items: 2
Items: 
Size: 773317 Color: 1
Size: 226220 Color: 0

Bin 451: 473 of cap free
Amount of items: 2
Items: 
Size: 639561 Color: 1
Size: 359967 Color: 0

Bin 452: 478 of cap free
Amount of items: 2
Items: 
Size: 759640 Color: 1
Size: 239883 Color: 0

Bin 453: 482 of cap free
Amount of items: 2
Items: 
Size: 691903 Color: 0
Size: 307616 Color: 1

Bin 454: 488 of cap free
Amount of items: 2
Items: 
Size: 694227 Color: 0
Size: 305286 Color: 1

Bin 455: 489 of cap free
Amount of items: 3
Items: 
Size: 573813 Color: 1
Size: 253822 Color: 0
Size: 171877 Color: 1

Bin 456: 489 of cap free
Amount of items: 2
Items: 
Size: 630014 Color: 0
Size: 369498 Color: 1

Bin 457: 495 of cap free
Amount of items: 2
Items: 
Size: 772993 Color: 0
Size: 226513 Color: 1

Bin 458: 495 of cap free
Amount of items: 2
Items: 
Size: 557779 Color: 1
Size: 441727 Color: 0

Bin 459: 497 of cap free
Amount of items: 2
Items: 
Size: 561765 Color: 1
Size: 437739 Color: 0

Bin 460: 498 of cap free
Amount of items: 2
Items: 
Size: 660954 Color: 1
Size: 338549 Color: 0

Bin 461: 498 of cap free
Amount of items: 2
Items: 
Size: 718813 Color: 1
Size: 280690 Color: 0

Bin 462: 499 of cap free
Amount of items: 3
Items: 
Size: 707775 Color: 1
Size: 152524 Color: 0
Size: 139203 Color: 1

Bin 463: 502 of cap free
Amount of items: 2
Items: 
Size: 508257 Color: 0
Size: 491242 Color: 1

Bin 464: 508 of cap free
Amount of items: 2
Items: 
Size: 553150 Color: 1
Size: 446343 Color: 0

Bin 465: 510 of cap free
Amount of items: 2
Items: 
Size: 658882 Color: 1
Size: 340609 Color: 0

Bin 466: 513 of cap free
Amount of items: 2
Items: 
Size: 705753 Color: 1
Size: 293735 Color: 0

Bin 467: 513 of cap free
Amount of items: 2
Items: 
Size: 518366 Color: 1
Size: 481122 Color: 0

Bin 468: 513 of cap free
Amount of items: 2
Items: 
Size: 750317 Color: 0
Size: 249171 Color: 1

Bin 469: 515 of cap free
Amount of items: 2
Items: 
Size: 501823 Color: 1
Size: 497663 Color: 0

Bin 470: 516 of cap free
Amount of items: 2
Items: 
Size: 545531 Color: 0
Size: 453954 Color: 1

Bin 471: 519 of cap free
Amount of items: 2
Items: 
Size: 603931 Color: 0
Size: 395551 Color: 1

Bin 472: 521 of cap free
Amount of items: 2
Items: 
Size: 644164 Color: 0
Size: 355316 Color: 1

Bin 473: 521 of cap free
Amount of items: 2
Items: 
Size: 586456 Color: 0
Size: 413024 Color: 1

Bin 474: 525 of cap free
Amount of items: 2
Items: 
Size: 521298 Color: 0
Size: 478178 Color: 1

Bin 475: 525 of cap free
Amount of items: 2
Items: 
Size: 695919 Color: 0
Size: 303557 Color: 1

Bin 476: 534 of cap free
Amount of items: 2
Items: 
Size: 755653 Color: 1
Size: 243814 Color: 0

Bin 477: 536 of cap free
Amount of items: 2
Items: 
Size: 505672 Color: 0
Size: 493793 Color: 1

Bin 478: 543 of cap free
Amount of items: 2
Items: 
Size: 533698 Color: 0
Size: 465760 Color: 1

Bin 479: 547 of cap free
Amount of items: 2
Items: 
Size: 671351 Color: 1
Size: 328103 Color: 0

Bin 480: 551 of cap free
Amount of items: 2
Items: 
Size: 621716 Color: 0
Size: 377734 Color: 1

Bin 481: 553 of cap free
Amount of items: 2
Items: 
Size: 728759 Color: 1
Size: 270689 Color: 0

Bin 482: 553 of cap free
Amount of items: 2
Items: 
Size: 785261 Color: 1
Size: 214187 Color: 0

Bin 483: 560 of cap free
Amount of items: 2
Items: 
Size: 713255 Color: 0
Size: 286186 Color: 1

Bin 484: 560 of cap free
Amount of items: 2
Items: 
Size: 672062 Color: 0
Size: 327379 Color: 1

Bin 485: 561 of cap free
Amount of items: 2
Items: 
Size: 683656 Color: 0
Size: 315784 Color: 1

Bin 486: 571 of cap free
Amount of items: 2
Items: 
Size: 537648 Color: 0
Size: 461782 Color: 1

Bin 487: 571 of cap free
Amount of items: 2
Items: 
Size: 705058 Color: 0
Size: 294372 Color: 1

Bin 488: 575 of cap free
Amount of items: 2
Items: 
Size: 776728 Color: 1
Size: 222698 Color: 0

Bin 489: 580 of cap free
Amount of items: 2
Items: 
Size: 662637 Color: 1
Size: 336784 Color: 0

Bin 490: 581 of cap free
Amount of items: 3
Items: 
Size: 754376 Color: 0
Size: 123437 Color: 1
Size: 121607 Color: 0

Bin 491: 584 of cap free
Amount of items: 2
Items: 
Size: 757957 Color: 0
Size: 241460 Color: 1

Bin 492: 584 of cap free
Amount of items: 2
Items: 
Size: 615539 Color: 0
Size: 383878 Color: 1

Bin 493: 587 of cap free
Amount of items: 3
Items: 
Size: 575367 Color: 0
Size: 297295 Color: 0
Size: 126752 Color: 1

Bin 494: 589 of cap free
Amount of items: 3
Items: 
Size: 686204 Color: 0
Size: 168487 Color: 1
Size: 144721 Color: 1

Bin 495: 591 of cap free
Amount of items: 2
Items: 
Size: 531698 Color: 1
Size: 467712 Color: 0

Bin 496: 591 of cap free
Amount of items: 2
Items: 
Size: 579957 Color: 1
Size: 419453 Color: 0

Bin 497: 592 of cap free
Amount of items: 2
Items: 
Size: 710021 Color: 0
Size: 289388 Color: 1

Bin 498: 593 of cap free
Amount of items: 2
Items: 
Size: 627892 Color: 1
Size: 371516 Color: 0

Bin 499: 600 of cap free
Amount of items: 2
Items: 
Size: 745558 Color: 1
Size: 253843 Color: 0

Bin 500: 606 of cap free
Amount of items: 3
Items: 
Size: 553967 Color: 0
Size: 338804 Color: 1
Size: 106624 Color: 0

Bin 501: 613 of cap free
Amount of items: 2
Items: 
Size: 679679 Color: 1
Size: 319709 Color: 0

Bin 502: 614 of cap free
Amount of items: 2
Items: 
Size: 603928 Color: 0
Size: 395459 Color: 1

Bin 503: 614 of cap free
Amount of items: 2
Items: 
Size: 520291 Color: 0
Size: 479096 Color: 1

Bin 504: 617 of cap free
Amount of items: 2
Items: 
Size: 615117 Color: 1
Size: 384267 Color: 0

Bin 505: 619 of cap free
Amount of items: 2
Items: 
Size: 799986 Color: 0
Size: 199396 Color: 1

Bin 506: 621 of cap free
Amount of items: 2
Items: 
Size: 626669 Color: 0
Size: 372711 Color: 1

Bin 507: 622 of cap free
Amount of items: 3
Items: 
Size: 571425 Color: 0
Size: 270804 Color: 1
Size: 157150 Color: 1

Bin 508: 623 of cap free
Amount of items: 2
Items: 
Size: 536946 Color: 1
Size: 462432 Color: 0

Bin 509: 625 of cap free
Amount of items: 2
Items: 
Size: 518890 Color: 0
Size: 480486 Color: 1

Bin 510: 630 of cap free
Amount of items: 2
Items: 
Size: 793458 Color: 0
Size: 205913 Color: 1

Bin 511: 633 of cap free
Amount of items: 2
Items: 
Size: 685294 Color: 1
Size: 314074 Color: 0

Bin 512: 635 of cap free
Amount of items: 2
Items: 
Size: 755633 Color: 1
Size: 243733 Color: 0

Bin 513: 636 of cap free
Amount of items: 2
Items: 
Size: 735452 Color: 1
Size: 263913 Color: 0

Bin 514: 636 of cap free
Amount of items: 2
Items: 
Size: 699928 Color: 0
Size: 299437 Color: 1

Bin 515: 637 of cap free
Amount of items: 2
Items: 
Size: 612915 Color: 0
Size: 386449 Color: 1

Bin 516: 640 of cap free
Amount of items: 2
Items: 
Size: 790113 Color: 0
Size: 209248 Color: 1

Bin 517: 644 of cap free
Amount of items: 2
Items: 
Size: 572506 Color: 1
Size: 426851 Color: 0

Bin 518: 654 of cap free
Amount of items: 2
Items: 
Size: 735437 Color: 1
Size: 263910 Color: 0

Bin 519: 654 of cap free
Amount of items: 2
Items: 
Size: 618067 Color: 0
Size: 381280 Color: 1

Bin 520: 657 of cap free
Amount of items: 2
Items: 
Size: 683230 Color: 1
Size: 316114 Color: 0

Bin 521: 660 of cap free
Amount of items: 2
Items: 
Size: 594031 Color: 0
Size: 405310 Color: 1

Bin 522: 664 of cap free
Amount of items: 2
Items: 
Size: 539569 Color: 0
Size: 459768 Color: 1

Bin 523: 674 of cap free
Amount of items: 2
Items: 
Size: 789063 Color: 0
Size: 210264 Color: 1

Bin 524: 675 of cap free
Amount of items: 2
Items: 
Size: 629164 Color: 1
Size: 370162 Color: 0

Bin 525: 677 of cap free
Amount of items: 2
Items: 
Size: 668071 Color: 1
Size: 331253 Color: 0

Bin 526: 682 of cap free
Amount of items: 2
Items: 
Size: 515094 Color: 0
Size: 484225 Color: 1

Bin 527: 684 of cap free
Amount of items: 2
Items: 
Size: 601372 Color: 1
Size: 397945 Color: 0

Bin 528: 686 of cap free
Amount of items: 3
Items: 
Size: 563601 Color: 1
Size: 272445 Color: 0
Size: 163269 Color: 1

Bin 529: 695 of cap free
Amount of items: 2
Items: 
Size: 600511 Color: 1
Size: 398795 Color: 0

Bin 530: 697 of cap free
Amount of items: 2
Items: 
Size: 648804 Color: 0
Size: 350500 Color: 1

Bin 531: 702 of cap free
Amount of items: 2
Items: 
Size: 687370 Color: 1
Size: 311929 Color: 0

Bin 532: 707 of cap free
Amount of items: 2
Items: 
Size: 798913 Color: 0
Size: 200381 Color: 1

Bin 533: 709 of cap free
Amount of items: 2
Items: 
Size: 654197 Color: 0
Size: 345095 Color: 1

Bin 534: 711 of cap free
Amount of items: 2
Items: 
Size: 588266 Color: 1
Size: 411024 Color: 0

Bin 535: 714 of cap free
Amount of items: 2
Items: 
Size: 590171 Color: 1
Size: 409116 Color: 0

Bin 536: 718 of cap free
Amount of items: 2
Items: 
Size: 660771 Color: 1
Size: 338512 Color: 0

Bin 537: 720 of cap free
Amount of items: 2
Items: 
Size: 747355 Color: 0
Size: 251926 Color: 1

Bin 538: 720 of cap free
Amount of items: 2
Items: 
Size: 566047 Color: 1
Size: 433234 Color: 0

Bin 539: 723 of cap free
Amount of items: 3
Items: 
Size: 413324 Color: 0
Size: 338703 Color: 1
Size: 247251 Color: 0

Bin 540: 726 of cap free
Amount of items: 3
Items: 
Size: 575330 Color: 0
Size: 262098 Color: 1
Size: 161847 Color: 1

Bin 541: 729 of cap free
Amount of items: 2
Items: 
Size: 750214 Color: 0
Size: 249058 Color: 1

Bin 542: 732 of cap free
Amount of items: 2
Items: 
Size: 648468 Color: 1
Size: 350801 Color: 0

Bin 543: 733 of cap free
Amount of items: 2
Items: 
Size: 791695 Color: 1
Size: 207573 Color: 0

Bin 544: 734 of cap free
Amount of items: 2
Items: 
Size: 528919 Color: 0
Size: 470348 Color: 1

Bin 545: 735 of cap free
Amount of items: 2
Items: 
Size: 725222 Color: 0
Size: 274044 Color: 1

Bin 546: 743 of cap free
Amount of items: 2
Items: 
Size: 642381 Color: 1
Size: 356877 Color: 0

Bin 547: 745 of cap free
Amount of items: 2
Items: 
Size: 759611 Color: 0
Size: 239645 Color: 1

Bin 548: 749 of cap free
Amount of items: 2
Items: 
Size: 695577 Color: 1
Size: 303675 Color: 0

Bin 549: 752 of cap free
Amount of items: 2
Items: 
Size: 729658 Color: 1
Size: 269591 Color: 0

Bin 550: 757 of cap free
Amount of items: 2
Items: 
Size: 608606 Color: 0
Size: 390638 Color: 1

Bin 551: 758 of cap free
Amount of items: 2
Items: 
Size: 717267 Color: 1
Size: 281976 Color: 0

Bin 552: 758 of cap free
Amount of items: 2
Items: 
Size: 793239 Color: 1
Size: 206004 Color: 0

Bin 553: 759 of cap free
Amount of items: 2
Items: 
Size: 574225 Color: 1
Size: 425017 Color: 0

Bin 554: 761 of cap free
Amount of items: 2
Items: 
Size: 655891 Color: 0
Size: 343349 Color: 1

Bin 555: 761 of cap free
Amount of items: 2
Items: 
Size: 607763 Color: 1
Size: 391477 Color: 0

Bin 556: 763 of cap free
Amount of items: 2
Items: 
Size: 739575 Color: 1
Size: 259663 Color: 0

Bin 557: 765 of cap free
Amount of items: 2
Items: 
Size: 513751 Color: 1
Size: 485485 Color: 0

Bin 558: 765 of cap free
Amount of items: 2
Items: 
Size: 569157 Color: 0
Size: 430079 Color: 1

Bin 559: 774 of cap free
Amount of items: 2
Items: 
Size: 770493 Color: 0
Size: 228734 Color: 1

Bin 560: 775 of cap free
Amount of items: 2
Items: 
Size: 511315 Color: 1
Size: 487911 Color: 0

Bin 561: 778 of cap free
Amount of items: 2
Items: 
Size: 523002 Color: 0
Size: 476221 Color: 1

Bin 562: 779 of cap free
Amount of items: 2
Items: 
Size: 620717 Color: 1
Size: 378505 Color: 0

Bin 563: 779 of cap free
Amount of items: 2
Items: 
Size: 505459 Color: 0
Size: 493763 Color: 1

Bin 564: 780 of cap free
Amount of items: 2
Items: 
Size: 550348 Color: 1
Size: 448873 Color: 0

Bin 565: 780 of cap free
Amount of items: 2
Items: 
Size: 782726 Color: 0
Size: 216495 Color: 1

Bin 566: 791 of cap free
Amount of items: 2
Items: 
Size: 613249 Color: 1
Size: 385961 Color: 0

Bin 567: 792 of cap free
Amount of items: 2
Items: 
Size: 583118 Color: 0
Size: 416091 Color: 1

Bin 568: 795 of cap free
Amount of items: 2
Items: 
Size: 549201 Color: 0
Size: 450005 Color: 1

Bin 569: 795 of cap free
Amount of items: 2
Items: 
Size: 757708 Color: 1
Size: 241498 Color: 0

Bin 570: 800 of cap free
Amount of items: 2
Items: 
Size: 593924 Color: 1
Size: 405277 Color: 0

Bin 571: 809 of cap free
Amount of items: 2
Items: 
Size: 658203 Color: 0
Size: 340989 Color: 1

Bin 572: 812 of cap free
Amount of items: 2
Items: 
Size: 718359 Color: 0
Size: 280830 Color: 1

Bin 573: 813 of cap free
Amount of items: 2
Items: 
Size: 690596 Color: 1
Size: 308592 Color: 0

Bin 574: 816 of cap free
Amount of items: 2
Items: 
Size: 682754 Color: 0
Size: 316431 Color: 1

Bin 575: 828 of cap free
Amount of items: 2
Items: 
Size: 751059 Color: 0
Size: 248114 Color: 1

Bin 576: 833 of cap free
Amount of items: 2
Items: 
Size: 542457 Color: 1
Size: 456711 Color: 0

Bin 577: 843 of cap free
Amount of items: 2
Items: 
Size: 763248 Color: 1
Size: 235910 Color: 0

Bin 578: 851 of cap free
Amount of items: 3
Items: 
Size: 437079 Color: 1
Size: 390440 Color: 0
Size: 171631 Color: 1

Bin 579: 868 of cap free
Amount of items: 2
Items: 
Size: 576718 Color: 1
Size: 422415 Color: 0

Bin 580: 870 of cap free
Amount of items: 2
Items: 
Size: 516380 Color: 1
Size: 482751 Color: 0

Bin 581: 876 of cap free
Amount of items: 2
Items: 
Size: 571382 Color: 0
Size: 427743 Color: 1

Bin 582: 878 of cap free
Amount of items: 2
Items: 
Size: 697572 Color: 1
Size: 301551 Color: 0

Bin 583: 902 of cap free
Amount of items: 2
Items: 
Size: 753247 Color: 0
Size: 245852 Color: 1

Bin 584: 911 of cap free
Amount of items: 2
Items: 
Size: 657745 Color: 1
Size: 341345 Color: 0

Bin 585: 912 of cap free
Amount of items: 2
Items: 
Size: 674109 Color: 1
Size: 324980 Color: 0

Bin 586: 912 of cap free
Amount of items: 2
Items: 
Size: 765186 Color: 0
Size: 233903 Color: 1

Bin 587: 914 of cap free
Amount of items: 2
Items: 
Size: 528974 Color: 1
Size: 470113 Color: 0

Bin 588: 915 of cap free
Amount of items: 2
Items: 
Size: 654417 Color: 1
Size: 344669 Color: 0

Bin 589: 918 of cap free
Amount of items: 3
Items: 
Size: 687127 Color: 0
Size: 174698 Color: 1
Size: 137258 Color: 1

Bin 590: 919 of cap free
Amount of items: 2
Items: 
Size: 663275 Color: 0
Size: 335807 Color: 1

Bin 591: 931 of cap free
Amount of items: 2
Items: 
Size: 617434 Color: 1
Size: 381636 Color: 0

Bin 592: 944 of cap free
Amount of items: 2
Items: 
Size: 635594 Color: 1
Size: 363463 Color: 0

Bin 593: 947 of cap free
Amount of items: 2
Items: 
Size: 677509 Color: 1
Size: 321545 Color: 0

Bin 594: 949 of cap free
Amount of items: 2
Items: 
Size: 541388 Color: 0
Size: 457664 Color: 1

Bin 595: 950 of cap free
Amount of items: 2
Items: 
Size: 664177 Color: 1
Size: 334874 Color: 0

Bin 596: 952 of cap free
Amount of items: 2
Items: 
Size: 794307 Color: 0
Size: 204742 Color: 1

Bin 597: 955 of cap free
Amount of items: 2
Items: 
Size: 741960 Color: 0
Size: 257086 Color: 1

Bin 598: 958 of cap free
Amount of items: 2
Items: 
Size: 609557 Color: 0
Size: 389486 Color: 1

Bin 599: 974 of cap free
Amount of items: 2
Items: 
Size: 526633 Color: 0
Size: 472394 Color: 1

Bin 600: 977 of cap free
Amount of items: 2
Items: 
Size: 542729 Color: 0
Size: 456295 Color: 1

Bin 601: 979 of cap free
Amount of items: 2
Items: 
Size: 675076 Color: 1
Size: 323946 Color: 0

Bin 602: 987 of cap free
Amount of items: 2
Items: 
Size: 544121 Color: 0
Size: 454893 Color: 1

Bin 603: 997 of cap free
Amount of items: 2
Items: 
Size: 774377 Color: 0
Size: 224627 Color: 1

Bin 604: 1000 of cap free
Amount of items: 2
Items: 
Size: 693400 Color: 1
Size: 305601 Color: 0

Bin 605: 1002 of cap free
Amount of items: 2
Items: 
Size: 778058 Color: 1
Size: 220941 Color: 0

Bin 606: 1003 of cap free
Amount of items: 2
Items: 
Size: 710370 Color: 1
Size: 288628 Color: 0

Bin 607: 1016 of cap free
Amount of items: 2
Items: 
Size: 555377 Color: 1
Size: 443608 Color: 0

Bin 608: 1025 of cap free
Amount of items: 2
Items: 
Size: 689762 Color: 0
Size: 309214 Color: 1

Bin 609: 1025 of cap free
Amount of items: 2
Items: 
Size: 562521 Color: 1
Size: 436455 Color: 0

Bin 610: 1026 of cap free
Amount of items: 2
Items: 
Size: 668844 Color: 0
Size: 330131 Color: 1

Bin 611: 1032 of cap free
Amount of items: 2
Items: 
Size: 690589 Color: 1
Size: 308380 Color: 0

Bin 612: 1032 of cap free
Amount of items: 2
Items: 
Size: 563624 Color: 0
Size: 435345 Color: 1

Bin 613: 1033 of cap free
Amount of items: 2
Items: 
Size: 641251 Color: 0
Size: 357717 Color: 1

Bin 614: 1042 of cap free
Amount of items: 2
Items: 
Size: 549186 Color: 1
Size: 449773 Color: 0

Bin 615: 1051 of cap free
Amount of items: 2
Items: 
Size: 730363 Color: 0
Size: 268587 Color: 1

Bin 616: 1051 of cap free
Amount of items: 2
Items: 
Size: 684202 Color: 1
Size: 314748 Color: 0

Bin 617: 1063 of cap free
Amount of items: 2
Items: 
Size: 789929 Color: 0
Size: 209009 Color: 1

Bin 618: 1069 of cap free
Amount of items: 2
Items: 
Size: 740244 Color: 0
Size: 258688 Color: 1

Bin 619: 1070 of cap free
Amount of items: 2
Items: 
Size: 533970 Color: 1
Size: 464961 Color: 0

Bin 620: 1076 of cap free
Amount of items: 2
Items: 
Size: 653119 Color: 1
Size: 345806 Color: 0

Bin 621: 1077 of cap free
Amount of items: 2
Items: 
Size: 627718 Color: 1
Size: 371206 Color: 0

Bin 622: 1084 of cap free
Amount of items: 2
Items: 
Size: 578798 Color: 0
Size: 420119 Color: 1

Bin 623: 1087 of cap free
Amount of items: 2
Items: 
Size: 733148 Color: 0
Size: 265766 Color: 1

Bin 624: 1116 of cap free
Amount of items: 2
Items: 
Size: 667756 Color: 1
Size: 331129 Color: 0

Bin 625: 1127 of cap free
Amount of items: 2
Items: 
Size: 581342 Color: 0
Size: 417532 Color: 1

Bin 626: 1134 of cap free
Amount of items: 2
Items: 
Size: 519989 Color: 0
Size: 478878 Color: 1

Bin 627: 1141 of cap free
Amount of items: 2
Items: 
Size: 629035 Color: 1
Size: 369825 Color: 0

Bin 628: 1146 of cap free
Amount of items: 2
Items: 
Size: 641220 Color: 1
Size: 357635 Color: 0

Bin 629: 1151 of cap free
Amount of items: 2
Items: 
Size: 793026 Color: 1
Size: 205824 Color: 0

Bin 630: 1159 of cap free
Amount of items: 2
Items: 
Size: 718716 Color: 1
Size: 280126 Color: 0

Bin 631: 1160 of cap free
Amount of items: 2
Items: 
Size: 609504 Color: 0
Size: 389337 Color: 1

Bin 632: 1176 of cap free
Amount of items: 2
Items: 
Size: 525259 Color: 1
Size: 473566 Color: 0

Bin 633: 1182 of cap free
Amount of items: 2
Items: 
Size: 504188 Color: 1
Size: 494631 Color: 0

Bin 634: 1195 of cap free
Amount of items: 2
Items: 
Size: 725157 Color: 1
Size: 273649 Color: 0

Bin 635: 1200 of cap free
Amount of items: 2
Items: 
Size: 598150 Color: 0
Size: 400651 Color: 1

Bin 636: 1211 of cap free
Amount of items: 2
Items: 
Size: 531850 Color: 0
Size: 466940 Color: 1

Bin 637: 1215 of cap free
Amount of items: 2
Items: 
Size: 626365 Color: 0
Size: 372421 Color: 1

Bin 638: 1244 of cap free
Amount of items: 2
Items: 
Size: 565619 Color: 1
Size: 433138 Color: 0

Bin 639: 1250 of cap free
Amount of items: 2
Items: 
Size: 716725 Color: 0
Size: 282026 Color: 1

Bin 640: 1250 of cap free
Amount of items: 2
Items: 
Size: 585057 Color: 0
Size: 413694 Color: 1

Bin 641: 1253 of cap free
Amount of items: 2
Items: 
Size: 547420 Color: 0
Size: 451328 Color: 1

Bin 642: 1261 of cap free
Amount of items: 2
Items: 
Size: 743842 Color: 1
Size: 254898 Color: 0

Bin 643: 1266 of cap free
Amount of items: 2
Items: 
Size: 630767 Color: 0
Size: 367968 Color: 1

Bin 644: 1281 of cap free
Amount of items: 2
Items: 
Size: 614842 Color: 1
Size: 383878 Color: 0

Bin 645: 1284 of cap free
Amount of items: 2
Items: 
Size: 544844 Color: 1
Size: 453873 Color: 0

Bin 646: 1285 of cap free
Amount of items: 2
Items: 
Size: 737639 Color: 1
Size: 261077 Color: 0

Bin 647: 1287 of cap free
Amount of items: 2
Items: 
Size: 710349 Color: 1
Size: 288365 Color: 0

Bin 648: 1294 of cap free
Amount of items: 2
Items: 
Size: 738439 Color: 0
Size: 260268 Color: 1

Bin 649: 1301 of cap free
Amount of items: 2
Items: 
Size: 740956 Color: 1
Size: 257744 Color: 0

Bin 650: 1307 of cap free
Amount of items: 2
Items: 
Size: 515046 Color: 0
Size: 483648 Color: 1

Bin 651: 1318 of cap free
Amount of items: 2
Items: 
Size: 605426 Color: 1
Size: 393257 Color: 0

Bin 652: 1324 of cap free
Amount of items: 2
Items: 
Size: 730744 Color: 1
Size: 267933 Color: 0

Bin 653: 1325 of cap free
Amount of items: 2
Items: 
Size: 773878 Color: 0
Size: 224798 Color: 1

Bin 654: 1341 of cap free
Amount of items: 2
Items: 
Size: 595942 Color: 1
Size: 402718 Color: 0

Bin 655: 1346 of cap free
Amount of items: 2
Items: 
Size: 513365 Color: 0
Size: 485290 Color: 1

Bin 656: 1350 of cap free
Amount of items: 2
Items: 
Size: 518341 Color: 1
Size: 480310 Color: 0

Bin 657: 1352 of cap free
Amount of items: 2
Items: 
Size: 660231 Color: 1
Size: 338418 Color: 0

Bin 658: 1369 of cap free
Amount of items: 2
Items: 
Size: 684311 Color: 0
Size: 314321 Color: 1

Bin 659: 1387 of cap free
Amount of items: 2
Items: 
Size: 588666 Color: 0
Size: 409948 Color: 1

Bin 660: 1394 of cap free
Amount of items: 2
Items: 
Size: 576303 Color: 1
Size: 422304 Color: 0

Bin 661: 1403 of cap free
Amount of items: 2
Items: 
Size: 533854 Color: 1
Size: 464744 Color: 0

Bin 662: 1434 of cap free
Amount of items: 2
Items: 
Size: 555132 Color: 1
Size: 443435 Color: 0

Bin 663: 1436 of cap free
Amount of items: 3
Items: 
Size: 339988 Color: 1
Size: 330953 Color: 0
Size: 327624 Color: 0

Bin 664: 1438 of cap free
Amount of items: 2
Items: 
Size: 610497 Color: 1
Size: 388066 Color: 0

Bin 665: 1450 of cap free
Amount of items: 2
Items: 
Size: 581223 Color: 0
Size: 417328 Color: 1

Bin 666: 1457 of cap free
Amount of items: 2
Items: 
Size: 520286 Color: 1
Size: 478258 Color: 0

Bin 667: 1463 of cap free
Amount of items: 2
Items: 
Size: 703023 Color: 1
Size: 295515 Color: 0

Bin 668: 1464 of cap free
Amount of items: 2
Items: 
Size: 589143 Color: 1
Size: 409394 Color: 0

Bin 669: 1477 of cap free
Amount of items: 2
Items: 
Size: 700066 Color: 1
Size: 298458 Color: 0

Bin 670: 1477 of cap free
Amount of items: 2
Items: 
Size: 540098 Color: 1
Size: 458426 Color: 0

Bin 671: 1478 of cap free
Amount of items: 2
Items: 
Size: 687071 Color: 1
Size: 311452 Color: 0

Bin 672: 1490 of cap free
Amount of items: 2
Items: 
Size: 638404 Color: 0
Size: 360107 Color: 1

Bin 673: 1492 of cap free
Amount of items: 2
Items: 
Size: 622898 Color: 1
Size: 375611 Color: 0

Bin 674: 1493 of cap free
Amount of items: 2
Items: 
Size: 596122 Color: 0
Size: 402386 Color: 1

Bin 675: 1505 of cap free
Amount of items: 2
Items: 
Size: 518259 Color: 1
Size: 480237 Color: 0

Bin 676: 1513 of cap free
Amount of items: 2
Items: 
Size: 776385 Color: 1
Size: 222103 Color: 0

Bin 677: 1525 of cap free
Amount of items: 2
Items: 
Size: 749964 Color: 1
Size: 248512 Color: 0

Bin 678: 1525 of cap free
Amount of items: 2
Items: 
Size: 686771 Color: 0
Size: 311705 Color: 1

Bin 679: 1526 of cap free
Amount of items: 2
Items: 
Size: 552681 Color: 1
Size: 445794 Color: 0

Bin 680: 1549 of cap free
Amount of items: 2
Items: 
Size: 697300 Color: 0
Size: 301152 Color: 1

Bin 681: 1564 of cap free
Amount of items: 2
Items: 
Size: 511153 Color: 1
Size: 487284 Color: 0

Bin 682: 1573 of cap free
Amount of items: 2
Items: 
Size: 720727 Color: 0
Size: 277701 Color: 1

Bin 683: 1585 of cap free
Amount of items: 2
Items: 
Size: 576250 Color: 1
Size: 422166 Color: 0

Bin 684: 1592 of cap free
Amount of items: 2
Items: 
Size: 767920 Color: 0
Size: 230489 Color: 1

Bin 685: 1594 of cap free
Amount of items: 2
Items: 
Size: 526039 Color: 0
Size: 472368 Color: 1

Bin 686: 1601 of cap free
Amount of items: 3
Items: 
Size: 784539 Color: 1
Size: 108163 Color: 0
Size: 105698 Color: 1

Bin 687: 1603 of cap free
Amount of items: 2
Items: 
Size: 626337 Color: 0
Size: 372061 Color: 1

Bin 688: 1624 of cap free
Amount of items: 2
Items: 
Size: 699620 Color: 0
Size: 298757 Color: 1

Bin 689: 1625 of cap free
Amount of items: 2
Items: 
Size: 662415 Color: 1
Size: 335961 Color: 0

Bin 690: 1629 of cap free
Amount of items: 2
Items: 
Size: 522777 Color: 0
Size: 475595 Color: 1

Bin 691: 1630 of cap free
Amount of items: 2
Items: 
Size: 674706 Color: 0
Size: 323665 Color: 1

Bin 692: 1638 of cap free
Amount of items: 3
Items: 
Size: 522289 Color: 1
Size: 343471 Color: 1
Size: 132603 Color: 0

Bin 693: 1670 of cap free
Amount of items: 2
Items: 
Size: 652043 Color: 0
Size: 346288 Color: 1

Bin 694: 1696 of cap free
Amount of items: 2
Items: 
Size: 513129 Color: 1
Size: 485176 Color: 0

Bin 695: 1697 of cap free
Amount of items: 2
Items: 
Size: 518278 Color: 0
Size: 480026 Color: 1

Bin 696: 1700 of cap free
Amount of items: 3
Items: 
Size: 571202 Color: 0
Size: 247780 Color: 0
Size: 179319 Color: 1

Bin 697: 1709 of cap free
Amount of items: 2
Items: 
Size: 612460 Color: 1
Size: 385832 Color: 0

Bin 698: 1723 of cap free
Amount of items: 2
Items: 
Size: 699971 Color: 1
Size: 298307 Color: 0

Bin 699: 1735 of cap free
Amount of items: 2
Items: 
Size: 587317 Color: 1
Size: 410949 Color: 0

Bin 700: 1739 of cap free
Amount of items: 2
Items: 
Size: 797326 Color: 1
Size: 200936 Color: 0

Bin 701: 1748 of cap free
Amount of items: 2
Items: 
Size: 653061 Color: 1
Size: 345192 Color: 0

Bin 702: 1764 of cap free
Amount of items: 2
Items: 
Size: 551792 Color: 0
Size: 446445 Color: 1

Bin 703: 1785 of cap free
Amount of items: 2
Items: 
Size: 759081 Color: 0
Size: 239135 Color: 1

Bin 704: 1792 of cap free
Amount of items: 2
Items: 
Size: 779383 Color: 0
Size: 218826 Color: 1

Bin 705: 1796 of cap free
Amount of items: 3
Items: 
Size: 540093 Color: 1
Size: 247430 Color: 0
Size: 210682 Color: 0

Bin 706: 1807 of cap free
Amount of items: 2
Items: 
Size: 644539 Color: 0
Size: 353655 Color: 1

Bin 707: 1828 of cap free
Amount of items: 2
Items: 
Size: 746569 Color: 0
Size: 251604 Color: 1

Bin 708: 1857 of cap free
Amount of items: 3
Items: 
Size: 449205 Color: 1
Size: 363535 Color: 1
Size: 185404 Color: 0

Bin 709: 1881 of cap free
Amount of items: 2
Items: 
Size: 518090 Color: 1
Size: 480030 Color: 0

Bin 710: 1882 of cap free
Amount of items: 2
Items: 
Size: 540041 Color: 1
Size: 458078 Color: 0

Bin 711: 1922 of cap free
Amount of items: 2
Items: 
Size: 720620 Color: 0
Size: 277459 Color: 1

Bin 712: 1933 of cap free
Amount of items: 3
Items: 
Size: 762917 Color: 0
Size: 135056 Color: 1
Size: 100095 Color: 0

Bin 713: 1938 of cap free
Amount of items: 2
Items: 
Size: 709946 Color: 0
Size: 288117 Color: 1

Bin 714: 1945 of cap free
Amount of items: 2
Items: 
Size: 782711 Color: 0
Size: 215345 Color: 1

Bin 715: 1951 of cap free
Amount of items: 3
Items: 
Size: 676505 Color: 0
Size: 164201 Color: 1
Size: 157344 Color: 0

Bin 716: 1957 of cap free
Amount of items: 2
Items: 
Size: 780149 Color: 1
Size: 217895 Color: 0

Bin 717: 1958 of cap free
Amount of items: 2
Items: 
Size: 540663 Color: 0
Size: 457380 Color: 1

Bin 718: 1958 of cap free
Amount of items: 2
Items: 
Size: 578250 Color: 0
Size: 419793 Color: 1

Bin 719: 1968 of cap free
Amount of items: 2
Items: 
Size: 619513 Color: 0
Size: 378520 Color: 1

Bin 720: 1972 of cap free
Amount of items: 2
Items: 
Size: 798645 Color: 0
Size: 199384 Color: 1

Bin 721: 1978 of cap free
Amount of items: 2
Items: 
Size: 622715 Color: 1
Size: 375308 Color: 0

Bin 722: 1982 of cap free
Amount of items: 2
Items: 
Size: 562512 Color: 1
Size: 435507 Color: 0

Bin 723: 1990 of cap free
Amount of items: 2
Items: 
Size: 617389 Color: 1
Size: 380622 Color: 0

Bin 724: 1991 of cap free
Amount of items: 2
Items: 
Size: 740449 Color: 1
Size: 257561 Color: 0

Bin 725: 2003 of cap free
Amount of items: 2
Items: 
Size: 658153 Color: 0
Size: 339845 Color: 1

Bin 726: 2048 of cap free
Amount of items: 2
Items: 
Size: 643496 Color: 1
Size: 354457 Color: 0

Bin 727: 2071 of cap free
Amount of items: 2
Items: 
Size: 501171 Color: 1
Size: 496759 Color: 0

Bin 728: 2086 of cap free
Amount of items: 2
Items: 
Size: 710044 Color: 1
Size: 287871 Color: 0

Bin 729: 2087 of cap free
Amount of items: 2
Items: 
Size: 597835 Color: 0
Size: 400079 Color: 1

Bin 730: 2089 of cap free
Amount of items: 2
Items: 
Size: 537819 Color: 1
Size: 460093 Color: 0

Bin 731: 2097 of cap free
Amount of items: 2
Items: 
Size: 668100 Color: 0
Size: 329804 Color: 1

Bin 732: 2103 of cap free
Amount of items: 2
Items: 
Size: 734459 Color: 0
Size: 263439 Color: 1

Bin 733: 2143 of cap free
Amount of items: 2
Items: 
Size: 770889 Color: 1
Size: 226969 Color: 0

Bin 734: 2149 of cap free
Amount of items: 2
Items: 
Size: 534719 Color: 0
Size: 463133 Color: 1

Bin 735: 2177 of cap free
Amount of items: 2
Items: 
Size: 730616 Color: 0
Size: 267208 Color: 1

Bin 736: 2177 of cap free
Amount of items: 2
Items: 
Size: 696515 Color: 1
Size: 301309 Color: 0

Bin 737: 2198 of cap free
Amount of items: 2
Items: 
Size: 669457 Color: 1
Size: 328346 Color: 0

Bin 738: 2207 of cap free
Amount of items: 2
Items: 
Size: 686633 Color: 1
Size: 311161 Color: 0

Bin 739: 2247 of cap free
Amount of items: 2
Items: 
Size: 689400 Color: 1
Size: 308354 Color: 0

Bin 740: 2257 of cap free
Amount of items: 2
Items: 
Size: 566794 Color: 0
Size: 430950 Color: 1

Bin 741: 2281 of cap free
Amount of items: 2
Items: 
Size: 692848 Color: 0
Size: 304872 Color: 1

Bin 742: 2305 of cap free
Amount of items: 2
Items: 
Size: 593387 Color: 0
Size: 404309 Color: 1

Bin 743: 2310 of cap free
Amount of items: 2
Items: 
Size: 514890 Color: 0
Size: 482801 Color: 1

Bin 744: 2322 of cap free
Amount of items: 2
Items: 
Size: 697081 Color: 0
Size: 300598 Color: 1

Bin 745: 2332 of cap free
Amount of items: 2
Items: 
Size: 667593 Color: 0
Size: 330076 Color: 1

Bin 746: 2337 of cap free
Amount of items: 2
Items: 
Size: 799830 Color: 0
Size: 197834 Color: 1

Bin 747: 2424 of cap free
Amount of items: 3
Items: 
Size: 752575 Color: 0
Size: 122757 Color: 0
Size: 122245 Color: 1

Bin 748: 2425 of cap free
Amount of items: 2
Items: 
Size: 718364 Color: 1
Size: 279212 Color: 0

Bin 749: 2442 of cap free
Amount of items: 2
Items: 
Size: 570429 Color: 0
Size: 427130 Color: 1

Bin 750: 2479 of cap free
Amount of items: 2
Items: 
Size: 755181 Color: 1
Size: 242341 Color: 0

Bin 751: 2545 of cap free
Amount of items: 2
Items: 
Size: 593183 Color: 0
Size: 404273 Color: 1

Bin 752: 2598 of cap free
Amount of items: 2
Items: 
Size: 652391 Color: 0
Size: 345012 Color: 1

Bin 753: 2602 of cap free
Amount of items: 2
Items: 
Size: 543748 Color: 1
Size: 453651 Color: 0

Bin 754: 2606 of cap free
Amount of items: 2
Items: 
Size: 724134 Color: 0
Size: 273261 Color: 1

Bin 755: 2610 of cap free
Amount of items: 2
Items: 
Size: 762947 Color: 1
Size: 234444 Color: 0

Bin 756: 2670 of cap free
Amount of items: 2
Items: 
Size: 639977 Color: 0
Size: 357354 Color: 1

Bin 757: 2677 of cap free
Amount of items: 2
Items: 
Size: 572685 Color: 1
Size: 424639 Color: 0

Bin 758: 2757 of cap free
Amount of items: 2
Items: 
Size: 738815 Color: 0
Size: 258429 Color: 1

Bin 759: 2780 of cap free
Amount of items: 2
Items: 
Size: 565604 Color: 1
Size: 431617 Color: 0

Bin 760: 2797 of cap free
Amount of items: 2
Items: 
Size: 533023 Color: 1
Size: 464181 Color: 0

Bin 761: 2847 of cap free
Amount of items: 2
Items: 
Size: 629410 Color: 0
Size: 367744 Color: 1

Bin 762: 2927 of cap free
Amount of items: 2
Items: 
Size: 646624 Color: 0
Size: 350450 Color: 1

Bin 763: 2958 of cap free
Amount of items: 2
Items: 
Size: 531628 Color: 0
Size: 465415 Color: 1

Bin 764: 2982 of cap free
Amount of items: 2
Items: 
Size: 673273 Color: 0
Size: 323746 Color: 1

Bin 765: 2998 of cap free
Amount of items: 2
Items: 
Size: 600686 Color: 0
Size: 396317 Color: 1

Bin 766: 3027 of cap free
Amount of items: 2
Items: 
Size: 579846 Color: 1
Size: 417128 Color: 0

Bin 767: 3060 of cap free
Amount of items: 2
Items: 
Size: 563151 Color: 0
Size: 433790 Color: 1

Bin 768: 3079 of cap free
Amount of items: 2
Items: 
Size: 682955 Color: 1
Size: 313967 Color: 0

Bin 769: 3150 of cap free
Amount of items: 3
Items: 
Size: 549933 Color: 1
Size: 273055 Color: 1
Size: 173863 Color: 0

Bin 770: 3175 of cap free
Amount of items: 2
Items: 
Size: 539335 Color: 0
Size: 457491 Color: 1

Bin 771: 3178 of cap free
Amount of items: 2
Items: 
Size: 524450 Color: 1
Size: 472373 Color: 0

Bin 772: 3209 of cap free
Amount of items: 2
Items: 
Size: 748994 Color: 0
Size: 247798 Color: 1

Bin 773: 3210 of cap free
Amount of items: 2
Items: 
Size: 555227 Color: 1
Size: 441564 Color: 0

Bin 774: 3367 of cap free
Amount of items: 2
Items: 
Size: 616472 Color: 1
Size: 380162 Color: 0

Bin 775: 3374 of cap free
Amount of items: 2
Items: 
Size: 643260 Color: 1
Size: 353367 Color: 0

Bin 776: 3425 of cap free
Amount of items: 2
Items: 
Size: 629099 Color: 0
Size: 367477 Color: 1

Bin 777: 3429 of cap free
Amount of items: 2
Items: 
Size: 772296 Color: 0
Size: 224276 Color: 1

Bin 778: 3445 of cap free
Amount of items: 2
Items: 
Size: 612690 Color: 1
Size: 383866 Color: 0

Bin 779: 3502 of cap free
Amount of items: 2
Items: 
Size: 558137 Color: 0
Size: 438362 Color: 1

Bin 780: 3537 of cap free
Amount of items: 2
Items: 
Size: 733147 Color: 1
Size: 263317 Color: 0

Bin 781: 3547 of cap free
Amount of items: 2
Items: 
Size: 625251 Color: 1
Size: 371203 Color: 0

Bin 782: 3594 of cap free
Amount of items: 2
Items: 
Size: 554094 Color: 0
Size: 442313 Color: 1

Bin 783: 3615 of cap free
Amount of items: 2
Items: 
Size: 572006 Color: 1
Size: 424380 Color: 0

Bin 784: 3620 of cap free
Amount of items: 2
Items: 
Size: 724756 Color: 0
Size: 271625 Color: 1

Bin 785: 3655 of cap free
Amount of items: 2
Items: 
Size: 761516 Color: 0
Size: 234830 Color: 1

Bin 786: 3691 of cap free
Amount of items: 2
Items: 
Size: 588036 Color: 1
Size: 408274 Color: 0

Bin 787: 3692 of cap free
Amount of items: 3
Items: 
Size: 725847 Color: 0
Size: 144529 Color: 0
Size: 125933 Color: 1

Bin 788: 3723 of cap free
Amount of items: 2
Items: 
Size: 647698 Color: 1
Size: 348580 Color: 0

Bin 789: 3749 of cap free
Amount of items: 2
Items: 
Size: 579644 Color: 1
Size: 416608 Color: 0

Bin 790: 3767 of cap free
Amount of items: 2
Items: 
Size: 526039 Color: 0
Size: 470195 Color: 1

Bin 791: 3805 of cap free
Amount of items: 2
Items: 
Size: 795388 Color: 1
Size: 200808 Color: 0

Bin 792: 3942 of cap free
Amount of items: 2
Items: 
Size: 775147 Color: 1
Size: 220912 Color: 0

Bin 793: 3957 of cap free
Amount of items: 2
Items: 
Size: 793279 Color: 0
Size: 202765 Color: 1

Bin 794: 4033 of cap free
Amount of items: 2
Items: 
Size: 517711 Color: 1
Size: 478257 Color: 0

Bin 795: 4068 of cap free
Amount of items: 2
Items: 
Size: 505165 Color: 0
Size: 490768 Color: 1

Bin 796: 4096 of cap free
Amount of items: 2
Items: 
Size: 686376 Color: 1
Size: 309529 Color: 0

Bin 797: 4109 of cap free
Amount of items: 2
Items: 
Size: 766016 Color: 1
Size: 229876 Color: 0

Bin 798: 4208 of cap free
Amount of items: 2
Items: 
Size: 694837 Color: 1
Size: 300956 Color: 0

Bin 799: 4278 of cap free
Amount of items: 2
Items: 
Size: 598314 Color: 1
Size: 397409 Color: 0

Bin 800: 4279 of cap free
Amount of items: 2
Items: 
Size: 770828 Color: 1
Size: 224894 Color: 0

Bin 801: 4291 of cap free
Amount of items: 2
Items: 
Size: 702212 Color: 0
Size: 293498 Color: 1

Bin 802: 4330 of cap free
Amount of items: 2
Items: 
Size: 783191 Color: 1
Size: 212480 Color: 0

Bin 803: 4430 of cap free
Amount of items: 2
Items: 
Size: 560976 Color: 1
Size: 434595 Color: 0

Bin 804: 4456 of cap free
Amount of items: 2
Items: 
Size: 718096 Color: 1
Size: 277449 Color: 0

Bin 805: 4465 of cap free
Amount of items: 2
Items: 
Size: 795044 Color: 1
Size: 200492 Color: 0

Bin 806: 4519 of cap free
Amount of items: 2
Items: 
Size: 525625 Color: 0
Size: 469857 Color: 1

Bin 807: 4558 of cap free
Amount of items: 2
Items: 
Size: 542329 Color: 1
Size: 453114 Color: 0

Bin 808: 4572 of cap free
Amount of items: 2
Items: 
Size: 500876 Color: 1
Size: 494553 Color: 0

Bin 809: 4605 of cap free
Amount of items: 2
Items: 
Size: 512713 Color: 0
Size: 482683 Color: 1

Bin 810: 4605 of cap free
Amount of items: 2
Items: 
Size: 782956 Color: 1
Size: 212440 Color: 0

Bin 811: 4618 of cap free
Amount of items: 2
Items: 
Size: 723910 Color: 0
Size: 271473 Color: 1

Bin 812: 4801 of cap free
Amount of items: 2
Items: 
Size: 552887 Color: 0
Size: 442313 Color: 1

Bin 813: 4850 of cap free
Amount of items: 2
Items: 
Size: 657775 Color: 0
Size: 337376 Color: 1

Bin 814: 4909 of cap free
Amount of items: 2
Items: 
Size: 702618 Color: 0
Size: 292474 Color: 1

Bin 815: 4920 of cap free
Amount of items: 2
Items: 
Size: 583017 Color: 0
Size: 412064 Color: 1

Bin 816: 4955 of cap free
Amount of items: 2
Items: 
Size: 792864 Color: 0
Size: 202182 Color: 1

Bin 817: 4980 of cap free
Amount of items: 2
Items: 
Size: 762851 Color: 1
Size: 232170 Color: 0

Bin 818: 5022 of cap free
Amount of items: 2
Items: 
Size: 509937 Color: 1
Size: 485042 Color: 0

Bin 819: 5114 of cap free
Amount of items: 2
Items: 
Size: 737378 Color: 1
Size: 257509 Color: 0

Bin 820: 5132 of cap free
Amount of items: 2
Items: 
Size: 598140 Color: 1
Size: 396729 Color: 0

Bin 821: 5303 of cap free
Amount of items: 2
Items: 
Size: 782324 Color: 1
Size: 212374 Color: 0

Bin 822: 5328 of cap free
Amount of items: 2
Items: 
Size: 650192 Color: 1
Size: 344481 Color: 0

Bin 823: 5361 of cap free
Amount of items: 2
Items: 
Size: 640240 Color: 1
Size: 354400 Color: 0

Bin 824: 5410 of cap free
Amount of items: 2
Items: 
Size: 761086 Color: 0
Size: 233505 Color: 1

Bin 825: 5468 of cap free
Amount of items: 2
Items: 
Size: 512704 Color: 0
Size: 481829 Color: 1

Bin 826: 5655 of cap free
Amount of items: 2
Items: 
Size: 637796 Color: 0
Size: 356550 Color: 1

Bin 827: 5656 of cap free
Amount of items: 2
Items: 
Size: 762499 Color: 1
Size: 231846 Color: 0

Bin 828: 5665 of cap free
Amount of items: 2
Items: 
Size: 542270 Color: 1
Size: 452066 Color: 0

Bin 829: 5845 of cap free
Amount of items: 2
Items: 
Size: 510032 Color: 1
Size: 484124 Color: 0

Bin 830: 5847 of cap free
Amount of items: 2
Items: 
Size: 531576 Color: 0
Size: 462578 Color: 1

Bin 831: 5938 of cap free
Amount of items: 2
Items: 
Size: 649585 Color: 0
Size: 344478 Color: 1

Bin 832: 6028 of cap free
Amount of items: 2
Items: 
Size: 512248 Color: 0
Size: 481725 Color: 1

Bin 833: 6074 of cap free
Amount of items: 2
Items: 
Size: 587495 Color: 1
Size: 406432 Color: 0

Bin 834: 6183 of cap free
Amount of items: 3
Items: 
Size: 574294 Color: 0
Size: 318502 Color: 0
Size: 101022 Color: 1

Bin 835: 6403 of cap free
Amount of items: 2
Items: 
Size: 771983 Color: 0
Size: 221615 Color: 1

Bin 836: 6442 of cap free
Amount of items: 2
Items: 
Size: 737104 Color: 1
Size: 256455 Color: 0

Bin 837: 6615 of cap free
Amount of items: 2
Items: 
Size: 509700 Color: 1
Size: 483686 Color: 0

Bin 838: 6672 of cap free
Amount of items: 2
Items: 
Size: 754167 Color: 1
Size: 239162 Color: 0

Bin 839: 6708 of cap free
Amount of items: 2
Items: 
Size: 632023 Color: 1
Size: 361270 Color: 0

Bin 840: 6995 of cap free
Amount of items: 2
Items: 
Size: 570962 Color: 1
Size: 422044 Color: 0

Bin 841: 7012 of cap free
Amount of items: 2
Items: 
Size: 632080 Color: 1
Size: 360909 Color: 0

Bin 842: 7193 of cap free
Amount of items: 2
Items: 
Size: 631501 Color: 1
Size: 361307 Color: 0

Bin 843: 7343 of cap free
Amount of items: 2
Items: 
Size: 625380 Color: 0
Size: 367278 Color: 1

Bin 844: 7495 of cap free
Amount of items: 2
Items: 
Size: 592653 Color: 0
Size: 399853 Color: 1

Bin 845: 7501 of cap free
Amount of items: 2
Items: 
Size: 522966 Color: 1
Size: 469534 Color: 0

Bin 846: 7510 of cap free
Amount of items: 2
Items: 
Size: 794930 Color: 1
Size: 197561 Color: 0

Bin 847: 7687 of cap free
Amount of items: 2
Items: 
Size: 706650 Color: 1
Size: 285664 Color: 0

Bin 848: 7742 of cap free
Amount of items: 2
Items: 
Size: 761724 Color: 1
Size: 230535 Color: 0

Bin 849: 7800 of cap free
Amount of items: 2
Items: 
Size: 498092 Color: 1
Size: 494109 Color: 0

Bin 850: 7993 of cap free
Amount of items: 2
Items: 
Size: 522701 Color: 0
Size: 469307 Color: 1

Bin 851: 8094 of cap free
Amount of items: 2
Items: 
Size: 694662 Color: 1
Size: 297245 Color: 0

Bin 852: 8162 of cap free
Amount of items: 2
Items: 
Size: 498169 Color: 1
Size: 493670 Color: 0

Bin 853: 8306 of cap free
Amount of items: 3
Items: 
Size: 650972 Color: 0
Size: 177688 Color: 1
Size: 163035 Color: 0

Bin 854: 8876 of cap free
Amount of items: 3
Items: 
Size: 415013 Color: 0
Size: 394961 Color: 1
Size: 181151 Color: 0

Bin 855: 8900 of cap free
Amount of items: 2
Items: 
Size: 622573 Color: 1
Size: 368528 Color: 0

Bin 856: 9043 of cap free
Amount of items: 2
Items: 
Size: 788832 Color: 0
Size: 202126 Color: 1

Bin 857: 9199 of cap free
Amount of items: 2
Items: 
Size: 522192 Color: 0
Size: 468610 Color: 1

Bin 858: 9213 of cap free
Amount of items: 2
Items: 
Size: 581156 Color: 0
Size: 409632 Color: 1

Bin 859: 9265 of cap free
Amount of items: 2
Items: 
Size: 788649 Color: 0
Size: 202087 Color: 1

Bin 860: 9569 of cap free
Amount of items: 2
Items: 
Size: 795687 Color: 1
Size: 194745 Color: 0

Bin 861: 9734 of cap free
Amount of items: 2
Items: 
Size: 783222 Color: 1
Size: 207045 Color: 0

Bin 862: 9987 of cap free
Amount of items: 2
Items: 
Size: 623899 Color: 0
Size: 366115 Color: 1

Bin 863: 10066 of cap free
Amount of items: 2
Items: 
Size: 497547 Color: 1
Size: 492388 Color: 0

Bin 864: 10085 of cap free
Amount of items: 2
Items: 
Size: 623033 Color: 0
Size: 366883 Color: 1

Bin 865: 10194 of cap free
Amount of items: 2
Items: 
Size: 551776 Color: 0
Size: 438031 Color: 1

Bin 866: 10320 of cap free
Amount of items: 2
Items: 
Size: 787923 Color: 0
Size: 201758 Color: 1

Bin 867: 10553 of cap free
Amount of items: 2
Items: 
Size: 511505 Color: 0
Size: 477943 Color: 1

Bin 868: 10601 of cap free
Amount of items: 2
Items: 
Size: 586784 Color: 1
Size: 402616 Color: 0

Bin 869: 11210 of cap free
Amount of items: 2
Items: 
Size: 622329 Color: 0
Size: 366462 Color: 1

Bin 870: 11307 of cap free
Amount of items: 2
Items: 
Size: 717051 Color: 1
Size: 271643 Color: 0

Bin 871: 11326 of cap free
Amount of items: 2
Items: 
Size: 580513 Color: 0
Size: 408162 Color: 1

Bin 872: 11495 of cap free
Amount of items: 2
Items: 
Size: 497348 Color: 1
Size: 491158 Color: 0

Bin 873: 11796 of cap free
Amount of items: 2
Items: 
Size: 795469 Color: 1
Size: 192736 Color: 0

Bin 874: 12433 of cap free
Amount of items: 2
Items: 
Size: 496807 Color: 1
Size: 490761 Color: 0

Bin 875: 13283 of cap free
Amount of items: 2
Items: 
Size: 631936 Color: 1
Size: 354782 Color: 0

Bin 876: 13355 of cap free
Amount of items: 2
Items: 
Size: 591946 Color: 0
Size: 394700 Color: 1

Bin 877: 13727 of cap free
Amount of items: 2
Items: 
Size: 634311 Color: 0
Size: 351963 Color: 1

Bin 878: 13738 of cap free
Amount of items: 2
Items: 
Size: 787414 Color: 0
Size: 198849 Color: 1

Bin 879: 13864 of cap free
Amount of items: 2
Items: 
Size: 531552 Color: 0
Size: 454585 Color: 1

Bin 880: 15879 of cap free
Amount of items: 3
Items: 
Size: 568639 Color: 1
Size: 243712 Color: 0
Size: 171771 Color: 1

Bin 881: 16560 of cap free
Amount of items: 2
Items: 
Size: 787227 Color: 0
Size: 196214 Color: 1

Bin 882: 19996 of cap free
Amount of items: 2
Items: 
Size: 716069 Color: 1
Size: 263936 Color: 0

Bin 883: 21872 of cap free
Amount of items: 2
Items: 
Size: 495692 Color: 1
Size: 482437 Color: 0

Bin 884: 23241 of cap free
Amount of items: 2
Items: 
Size: 636668 Color: 0
Size: 340092 Color: 1

Bin 885: 24694 of cap free
Amount of items: 2
Items: 
Size: 559146 Color: 1
Size: 416161 Color: 0

Bin 886: 25304 of cap free
Amount of items: 2
Items: 
Size: 656754 Color: 1
Size: 317943 Color: 0

Bin 887: 27488 of cap free
Amount of items: 2
Items: 
Size: 634536 Color: 0
Size: 337977 Color: 1

Bin 888: 28569 of cap free
Amount of items: 2
Items: 
Size: 786605 Color: 0
Size: 184827 Color: 1

Bin 889: 28754 of cap free
Amount of items: 2
Items: 
Size: 769518 Color: 0
Size: 201729 Color: 1

Bin 890: 33007 of cap free
Amount of items: 2
Items: 
Size: 551508 Color: 1
Size: 415486 Color: 0

Bin 891: 34009 of cap free
Amount of items: 2
Items: 
Size: 785179 Color: 0
Size: 180813 Color: 1

Bin 892: 34114 of cap free
Amount of items: 2
Items: 
Size: 551242 Color: 1
Size: 414645 Color: 0

Bin 893: 46436 of cap free
Amount of items: 2
Items: 
Size: 619502 Color: 0
Size: 334063 Color: 1

Bin 894: 49129 of cap free
Amount of items: 3
Items: 
Size: 705872 Color: 1
Size: 138770 Color: 1
Size: 106230 Color: 0

Bin 895: 58266 of cap free
Amount of items: 3
Items: 
Size: 393336 Color: 1
Size: 368899 Color: 0
Size: 179500 Color: 0

Total size: 893348187
Total free space: 1652708

