Capicity Bin: 8264
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4072 Color: 1
Size: 1176 Color: 1
Size: 1000 Color: 0
Size: 704 Color: 1
Size: 488 Color: 1
Size: 448 Color: 0
Size: 376 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 6236 Color: 1
Size: 1692 Color: 1
Size: 264 Color: 0
Size: 48 Color: 0
Size: 24 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 1
Size: 756 Color: 1
Size: 144 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 1
Size: 1044 Color: 1
Size: 200 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 1
Size: 1372 Color: 1
Size: 272 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6251 Color: 1
Size: 1679 Color: 1
Size: 334 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7365 Color: 1
Size: 751 Color: 1
Size: 148 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6451 Color: 1
Size: 1511 Color: 1
Size: 302 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 1
Size: 1085 Color: 1
Size: 216 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6785 Color: 1
Size: 1233 Color: 1
Size: 246 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 1
Size: 1098 Color: 1
Size: 196 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6774 Color: 1
Size: 1242 Color: 1
Size: 248 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5409 Color: 1
Size: 2381 Color: 1
Size: 474 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 1
Size: 3298 Color: 1
Size: 236 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5009 Color: 1
Size: 2713 Color: 1
Size: 542 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 1
Size: 826 Color: 1
Size: 164 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5425 Color: 1
Size: 2367 Color: 1
Size: 472 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7412 Color: 1
Size: 716 Color: 1
Size: 136 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6245 Color: 1
Size: 1683 Color: 1
Size: 336 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 1
Size: 2091 Color: 1
Size: 416 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7214 Color: 1
Size: 878 Color: 1
Size: 172 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5017 Color: 1
Size: 2707 Color: 1
Size: 540 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6611 Color: 1
Size: 1379 Color: 1
Size: 274 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 1
Size: 1450 Color: 1
Size: 288 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7418 Color: 1
Size: 790 Color: 1
Size: 56 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7154 Color: 1
Size: 926 Color: 1
Size: 184 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5417 Color: 1
Size: 2373 Color: 1
Size: 474 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 1
Size: 1151 Color: 1
Size: 230 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7404 Color: 1
Size: 724 Color: 1
Size: 136 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7117 Color: 1
Size: 957 Color: 1
Size: 190 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7137 Color: 1
Size: 941 Color: 1
Size: 186 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6580 Color: 1
Size: 1404 Color: 1
Size: 280 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 1
Size: 782 Color: 1
Size: 152 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7359 Color: 1
Size: 841 Color: 1
Size: 64 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 1
Size: 1248 Color: 1
Size: 336 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7325 Color: 1
Size: 783 Color: 1
Size: 156 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4938 Color: 1
Size: 2774 Color: 1
Size: 552 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 1
Size: 1305 Color: 1
Size: 198 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5001 Color: 1
Size: 2721 Color: 1
Size: 542 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 1
Size: 1114 Color: 1
Size: 172 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 1
Size: 2962 Color: 1
Size: 588 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 1
Size: 2412 Color: 1
Size: 176 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6502 Color: 1
Size: 1470 Color: 1
Size: 292 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 1
Size: 1018 Color: 1
Size: 200 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 1
Size: 1289 Color: 1
Size: 256 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7257 Color: 1
Size: 841 Color: 1
Size: 166 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4516 Color: 1
Size: 3124 Color: 1
Size: 624 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6852 Color: 1
Size: 1180 Color: 1
Size: 232 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7039 Color: 1
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6734 Color: 1
Size: 1278 Color: 1
Size: 252 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 1
Size: 1996 Color: 1
Size: 392 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7306 Color: 1
Size: 802 Color: 1
Size: 156 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7002 Color: 1
Size: 1054 Color: 1
Size: 208 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 1
Size: 1730 Color: 1
Size: 344 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 1
Size: 1542 Color: 1
Size: 308 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 1
Size: 1548 Color: 1
Size: 304 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 1
Size: 858 Color: 1
Size: 168 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7354 Color: 1
Size: 854 Color: 1
Size: 56 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7309 Color: 1
Size: 797 Color: 1
Size: 158 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 1
Size: 732 Color: 1
Size: 144 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7298 Color: 1
Size: 854 Color: 1
Size: 112 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4836 Color: 1
Size: 2860 Color: 1
Size: 568 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7093 Color: 1
Size: 977 Color: 1
Size: 194 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 2596 Color: 1
Size: 512 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 1
Size: 1524 Color: 1
Size: 304 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 1
Size: 2108 Color: 1
Size: 416 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 1
Size: 3338 Color: 1
Size: 664 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 1
Size: 2578 Color: 1
Size: 512 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 1
Size: 1518 Color: 1
Size: 300 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4158 Color: 1
Size: 3422 Color: 1
Size: 684 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 7394 Color: 1
Size: 726 Color: 1
Size: 144 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 1
Size: 982 Color: 1
Size: 192 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 1
Size: 1318 Color: 1
Size: 260 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 4673 Color: 1
Size: 2993 Color: 1
Size: 598 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 3584 Color: 1
Size: 3104 Color: 1
Size: 1576 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5574 Color: 1
Size: 2242 Color: 1
Size: 448 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7289 Color: 1
Size: 813 Color: 1
Size: 162 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7322 Color: 1
Size: 786 Color: 1
Size: 156 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6338 Color: 1
Size: 1606 Color: 1
Size: 320 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5362 Color: 1
Size: 2422 Color: 1
Size: 480 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 7164 Color: 1
Size: 924 Color: 1
Size: 176 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 1
Size: 1361 Color: 1
Size: 138 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 1
Size: 1397 Color: 1
Size: 248 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 1
Size: 1671 Color: 1
Size: 334 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5403 Color: 1
Size: 2385 Color: 1
Size: 476 Color: 0

Bin 86: 0 of cap free
Amount of items: 4
Items: 
Size: 6648 Color: 1
Size: 1320 Color: 1
Size: 152 Color: 0
Size: 144 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 1
Size: 902 Color: 1
Size: 180 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 1
Size: 700 Color: 1
Size: 136 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 1
Size: 1334 Color: 1
Size: 264 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 4681 Color: 1
Size: 3115 Color: 1
Size: 468 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 1
Size: 3430 Color: 1
Size: 684 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 1
Size: 1862 Color: 1
Size: 372 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6793 Color: 1
Size: 1227 Color: 1
Size: 244 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 1
Size: 740 Color: 1
Size: 144 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 7236 Color: 1
Size: 868 Color: 1
Size: 160 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 1
Size: 2014 Color: 1
Size: 400 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 1
Size: 1124 Color: 1
Size: 216 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 1
Size: 1892 Color: 1
Size: 376 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 1
Size: 2716 Color: 1
Size: 536 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 1
Size: 964 Color: 1
Size: 192 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4140 Color: 1
Size: 3444 Color: 1
Size: 680 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 1
Size: 844 Color: 1
Size: 160 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6212 Color: 1
Size: 1788 Color: 1
Size: 264 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 1
Size: 1138 Color: 1
Size: 224 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7082 Color: 1
Size: 986 Color: 1
Size: 196 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 1
Size: 1116 Color: 1
Size: 144 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 1
Size: 2284 Color: 1
Size: 448 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 1
Size: 908 Color: 1
Size: 176 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 7281 Color: 1
Size: 821 Color: 1
Size: 162 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 1
Size: 1798 Color: 1
Size: 356 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6459 Color: 1
Size: 1505 Color: 1
Size: 300 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 1
Size: 2492 Color: 1
Size: 496 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 1
Size: 937 Color: 1
Size: 186 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4142 Color: 1
Size: 3438 Color: 1
Size: 684 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6605 Color: 1
Size: 1383 Color: 1
Size: 276 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 7201 Color: 1
Size: 887 Color: 1
Size: 176 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6931 Color: 1
Size: 1111 Color: 1
Size: 222 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5566 Color: 1
Size: 2518 Color: 1
Size: 180 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6866 Color: 1
Size: 1166 Color: 1
Size: 232 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 1
Size: 1284 Color: 1
Size: 248 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4134 Color: 1
Size: 3542 Color: 1
Size: 588 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 1
Size: 3764 Color: 1
Size: 184 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 1
Size: 1665 Color: 1
Size: 332 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6907 Color: 1
Size: 1131 Color: 1
Size: 226 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 1
Size: 1871 Color: 1
Size: 374 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6027 Color: 1
Size: 1865 Color: 1
Size: 372 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 4135 Color: 1
Size: 3441 Color: 1
Size: 688 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 4133 Color: 1
Size: 3443 Color: 1
Size: 688 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 1
Size: 2114 Color: 1
Size: 420 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 1
Size: 2097 Color: 1
Size: 418 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7069 Color: 1
Size: 1119 Color: 1
Size: 76 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7157 Color: 1
Size: 923 Color: 1
Size: 184 Color: 0

Total size: 1090848
Total free space: 0

