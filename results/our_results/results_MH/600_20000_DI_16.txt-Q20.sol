Capicity Bin: 16336
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8172 Color: 10
Size: 1562 Color: 7
Size: 1556 Color: 18
Size: 1528 Color: 3
Size: 1482 Color: 16
Size: 1168 Color: 18
Size: 868 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8196 Color: 11
Size: 6788 Color: 18
Size: 1352 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9234 Color: 10
Size: 6806 Color: 1
Size: 296 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9202 Color: 17
Size: 6802 Color: 7
Size: 332 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 14
Size: 5832 Color: 19
Size: 784 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 6
Size: 4316 Color: 9
Size: 476 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11589 Color: 13
Size: 3891 Color: 5
Size: 856 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11868 Color: 12
Size: 4008 Color: 13
Size: 460 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 10
Size: 4300 Color: 15
Size: 192 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 3
Size: 3592 Color: 19
Size: 288 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12481 Color: 14
Size: 3213 Color: 13
Size: 642 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12513 Color: 4
Size: 3507 Color: 18
Size: 316 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 16
Size: 3252 Color: 8
Size: 276 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 13
Size: 3240 Color: 13
Size: 224 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 11
Size: 2372 Color: 7
Size: 992 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 0
Size: 2804 Color: 3
Size: 478 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13081 Color: 2
Size: 2399 Color: 18
Size: 856 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13085 Color: 9
Size: 2689 Color: 12
Size: 562 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 17
Size: 2648 Color: 4
Size: 432 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 10
Size: 1681 Color: 13
Size: 1426 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 0
Size: 2660 Color: 12
Size: 392 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 18
Size: 1580 Color: 9
Size: 1360 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 10
Size: 2564 Color: 6
Size: 330 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 4
Size: 1458 Color: 5
Size: 1386 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 3
Size: 2364 Color: 6
Size: 464 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13630 Color: 5
Size: 2046 Color: 4
Size: 660 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13639 Color: 17
Size: 1481 Color: 19
Size: 1216 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 7
Size: 2392 Color: 6
Size: 288 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 19
Size: 1902 Color: 5
Size: 654 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 1
Size: 2381 Color: 8
Size: 110 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13962 Color: 11
Size: 1834 Color: 19
Size: 540 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13977 Color: 18
Size: 1851 Color: 14
Size: 508 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 9
Size: 1372 Color: 11
Size: 912 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14058 Color: 16
Size: 1908 Color: 12
Size: 370 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 6
Size: 1749 Color: 2
Size: 472 Color: 16

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 14
Size: 1600 Color: 4
Size: 636 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 4
Size: 1678 Color: 8
Size: 548 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 2
Size: 1782 Color: 6
Size: 434 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 9
Size: 1352 Color: 17
Size: 782 Color: 8

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14227 Color: 5
Size: 1581 Color: 3
Size: 528 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 6
Size: 1672 Color: 11
Size: 384 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 7
Size: 1352 Color: 10
Size: 700 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 6
Size: 1448 Color: 10
Size: 568 Color: 7

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14355 Color: 9
Size: 1557 Color: 2
Size: 424 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14378 Color: 16
Size: 1610 Color: 6
Size: 348 Color: 14

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14410 Color: 14
Size: 1494 Color: 16
Size: 432 Color: 18

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14416 Color: 5
Size: 1072 Color: 1
Size: 848 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 8
Size: 1078 Color: 13
Size: 776 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14511 Color: 4
Size: 1521 Color: 5
Size: 304 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 17
Size: 1414 Color: 14
Size: 408 Color: 10

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 8
Size: 1380 Color: 6
Size: 376 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14642 Color: 4
Size: 1344 Color: 12
Size: 350 Color: 17

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 9867 Color: 8
Size: 5820 Color: 17
Size: 648 Color: 8

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 14
Size: 2288 Color: 10
Size: 1192 Color: 0

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 13033 Color: 16
Size: 3302 Color: 2

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 13258 Color: 8
Size: 2741 Color: 13
Size: 336 Color: 6

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 13459 Color: 18
Size: 2876 Color: 3

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13839 Color: 5
Size: 1344 Color: 2
Size: 1152 Color: 2

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13991 Color: 8
Size: 2040 Color: 6
Size: 304 Color: 4

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 14039 Color: 8
Size: 2296 Color: 9

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 14
Size: 2149 Color: 7

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 14239 Color: 10
Size: 2096 Color: 17

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 14469 Color: 6
Size: 1546 Color: 15
Size: 320 Color: 14

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 14520 Color: 18
Size: 1815 Color: 11

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14569 Color: 6
Size: 1606 Color: 14
Size: 160 Color: 13

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 17
Size: 1651 Color: 15

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 8479 Color: 14
Size: 7511 Color: 13
Size: 344 Color: 14

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 6
Size: 6764 Color: 13
Size: 554 Color: 18

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 9634 Color: 10
Size: 5764 Color: 6
Size: 936 Color: 6

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 9682 Color: 4
Size: 6392 Color: 10
Size: 260 Color: 12

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11637 Color: 2
Size: 4185 Color: 6
Size: 512 Color: 1

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 5
Size: 4374 Color: 11

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 10
Size: 3442 Color: 14

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12953 Color: 7
Size: 2781 Color: 1
Size: 600 Color: 14

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 19
Size: 2738 Color: 13
Size: 280 Color: 14

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 15
Size: 2648 Color: 6
Size: 352 Color: 19

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 2
Size: 2452 Color: 1

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 1
Size: 2310 Color: 5

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14159 Color: 19
Size: 2175 Color: 8

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 2
Size: 1928 Color: 14

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14476 Color: 16
Size: 1858 Color: 18

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 0
Size: 1308 Color: 18
Size: 352 Color: 6

Bin 83: 3 of cap free
Amount of items: 7
Items: 
Size: 8180 Color: 17
Size: 1898 Color: 11
Size: 1848 Color: 2
Size: 1682 Color: 16
Size: 1473 Color: 8
Size: 964 Color: 4
Size: 288 Color: 10

Bin 84: 3 of cap free
Amount of items: 5
Items: 
Size: 8174 Color: 2
Size: 4008 Color: 8
Size: 2673 Color: 0
Size: 992 Color: 16
Size: 486 Color: 18

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 9181 Color: 0
Size: 7152 Color: 13

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 9675 Color: 1
Size: 6374 Color: 18
Size: 284 Color: 5

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 10364 Color: 2
Size: 5369 Color: 10
Size: 600 Color: 11

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 10787 Color: 17
Size: 5546 Color: 4

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 12049 Color: 0
Size: 3964 Color: 18
Size: 320 Color: 11

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 12097 Color: 8
Size: 3724 Color: 6
Size: 512 Color: 18

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 14252 Color: 16
Size: 2081 Color: 12

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 13
Size: 7540 Color: 7
Size: 580 Color: 9

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 8264 Color: 10
Size: 6804 Color: 11
Size: 1264 Color: 17

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 8680 Color: 1
Size: 6772 Color: 9
Size: 880 Color: 16

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 10808 Color: 13
Size: 5036 Color: 14
Size: 488 Color: 8

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 11128 Color: 6
Size: 5204 Color: 19

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 15
Size: 2504 Color: 1

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 11
Size: 2132 Color: 3

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 11
Size: 1612 Color: 1
Size: 40 Color: 2

Bin 100: 5 of cap free
Amount of items: 9
Items: 
Size: 8171 Color: 16
Size: 1468 Color: 12
Size: 1460 Color: 18
Size: 1416 Color: 5
Size: 1360 Color: 10
Size: 1360 Color: 8
Size: 512 Color: 2
Size: 384 Color: 0
Size: 200 Color: 12

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 9895 Color: 7
Size: 6104 Color: 11
Size: 332 Color: 1

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 11315 Color: 16
Size: 5016 Color: 8

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 19
Size: 3917 Color: 15
Size: 324 Color: 15

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 12374 Color: 9
Size: 3957 Color: 19

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 7
Size: 3533 Color: 8
Size: 362 Color: 6

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 14083 Color: 9
Size: 2248 Color: 18

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 14391 Color: 4
Size: 1940 Color: 1

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 9666 Color: 15
Size: 6354 Color: 2
Size: 310 Color: 1

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 13850 Color: 15
Size: 2130 Color: 3
Size: 350 Color: 6

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 14546 Color: 12
Size: 1784 Color: 11

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14590 Color: 17
Size: 1740 Color: 0

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 11978 Color: 6
Size: 4351 Color: 14

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 13419 Color: 7
Size: 2074 Color: 14
Size: 836 Color: 19

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 8
Size: 2275 Color: 14
Size: 272 Color: 6

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 13897 Color: 19
Size: 2432 Color: 13

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 12422 Color: 0
Size: 3634 Color: 17
Size: 272 Color: 7

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 13759 Color: 3
Size: 2568 Color: 15

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 10
Size: 2431 Color: 12

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 14012 Color: 1
Size: 2315 Color: 17

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 14235 Color: 2
Size: 2092 Color: 4

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 11180 Color: 19
Size: 4682 Color: 1
Size: 464 Color: 7

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 12753 Color: 14
Size: 3573 Color: 13

Bin 123: 10 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 19
Size: 3228 Color: 6
Size: 240 Color: 5

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 9
Size: 3150 Color: 4

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 10
Size: 2000 Color: 8

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 17
Size: 1982 Color: 9

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 14692 Color: 12
Size: 1634 Color: 9

Bin 128: 11 of cap free
Amount of items: 3
Items: 
Size: 9471 Color: 7
Size: 6064 Color: 19
Size: 790 Color: 8

Bin 129: 11 of cap free
Amount of items: 3
Items: 
Size: 11117 Color: 6
Size: 4680 Color: 11
Size: 528 Color: 9

Bin 130: 11 of cap free
Amount of items: 3
Items: 
Size: 13001 Color: 17
Size: 2888 Color: 6
Size: 436 Color: 3

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 11084 Color: 15
Size: 5240 Color: 10

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 19
Size: 2987 Color: 8

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 15
Size: 2261 Color: 4

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 14444 Color: 19
Size: 1879 Color: 12

Bin 135: 14 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 17
Size: 6728 Color: 19
Size: 294 Color: 18

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 12606 Color: 3
Size: 3716 Color: 19

Bin 137: 14 of cap free
Amount of items: 3
Items: 
Size: 14319 Color: 14
Size: 1955 Color: 8
Size: 48 Color: 3

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 8
Size: 1696 Color: 13

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 10056 Color: 15
Size: 6265 Color: 16

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 14562 Color: 1
Size: 1759 Color: 18

Bin 141: 16 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 6
Size: 4344 Color: 0
Size: 812 Color: 10

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 13607 Color: 17
Size: 2713 Color: 4

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 13
Size: 1720 Color: 0

Bin 144: 17 of cap free
Amount of items: 2
Items: 
Size: 13566 Color: 16
Size: 2753 Color: 13

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 18
Size: 1915 Color: 1

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 5
Size: 3898 Color: 0

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 13049 Color: 15
Size: 3268 Color: 11

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 12040 Color: 12
Size: 4276 Color: 18

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 13750 Color: 7
Size: 2566 Color: 19

Bin 150: 22 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 17
Size: 5586 Color: 19

Bin 151: 22 of cap free
Amount of items: 2
Items: 
Size: 13824 Color: 19
Size: 2490 Color: 15

Bin 152: 24 of cap free
Amount of items: 2
Items: 
Size: 14561 Color: 15
Size: 1751 Color: 5

Bin 153: 26 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 13
Size: 2414 Color: 3
Size: 80 Color: 11

Bin 154: 27 of cap free
Amount of items: 2
Items: 
Size: 13559 Color: 7
Size: 2750 Color: 4

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 8228 Color: 12
Size: 8080 Color: 19

Bin 156: 29 of cap free
Amount of items: 2
Items: 
Size: 14439 Color: 17
Size: 1868 Color: 4

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 14138 Color: 12
Size: 2168 Color: 4

Bin 158: 32 of cap free
Amount of items: 3
Items: 
Size: 12433 Color: 11
Size: 2711 Color: 18
Size: 1160 Color: 10

Bin 159: 32 of cap free
Amount of items: 2
Items: 
Size: 14588 Color: 11
Size: 1716 Color: 16

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 13350 Color: 11
Size: 2952 Color: 9

Bin 161: 35 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 8
Size: 2901 Color: 17
Size: 132 Color: 2

Bin 162: 35 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 9
Size: 2821 Color: 18

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 13038 Color: 0
Size: 3262 Color: 2

Bin 164: 37 of cap free
Amount of items: 5
Items: 
Size: 8184 Color: 4
Size: 2258 Color: 15
Size: 2158 Color: 19
Size: 1905 Color: 18
Size: 1794 Color: 16

Bin 165: 41 of cap free
Amount of items: 2
Items: 
Size: 13277 Color: 0
Size: 3018 Color: 14

Bin 166: 42 of cap free
Amount of items: 2
Items: 
Size: 11669 Color: 0
Size: 4625 Color: 9

Bin 167: 42 of cap free
Amount of items: 2
Items: 
Size: 13479 Color: 5
Size: 2815 Color: 8

Bin 168: 44 of cap free
Amount of items: 8
Items: 
Size: 8170 Color: 14
Size: 1344 Color: 14
Size: 1328 Color: 17
Size: 1188 Color: 19
Size: 1116 Color: 1
Size: 1110 Color: 6
Size: 1108 Color: 14
Size: 928 Color: 4

Bin 169: 44 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 17
Size: 4980 Color: 10
Size: 284 Color: 16

Bin 170: 45 of cap free
Amount of items: 2
Items: 
Size: 10328 Color: 16
Size: 5963 Color: 6

Bin 171: 45 of cap free
Amount of items: 2
Items: 
Size: 13104 Color: 13
Size: 3187 Color: 14

Bin 172: 45 of cap free
Amount of items: 2
Items: 
Size: 13389 Color: 0
Size: 2902 Color: 15

Bin 173: 46 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 5
Size: 2329 Color: 1
Size: 2077 Color: 7

Bin 174: 47 of cap free
Amount of items: 2
Items: 
Size: 14322 Color: 16
Size: 1967 Color: 13

Bin 175: 49 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 7
Size: 2551 Color: 11

Bin 176: 51 of cap free
Amount of items: 10
Items: 
Size: 8169 Color: 5
Size: 1108 Color: 15
Size: 1040 Color: 8
Size: 1008 Color: 4
Size: 1000 Color: 9
Size: 992 Color: 11
Size: 924 Color: 7
Size: 896 Color: 13
Size: 652 Color: 7
Size: 496 Color: 2

Bin 177: 51 of cap free
Amount of items: 2
Items: 
Size: 11462 Color: 9
Size: 4823 Color: 6

Bin 178: 56 of cap free
Amount of items: 37
Items: 
Size: 596 Color: 8
Size: 576 Color: 11
Size: 576 Color: 3
Size: 576 Color: 1
Size: 560 Color: 19
Size: 546 Color: 16
Size: 544 Color: 9
Size: 542 Color: 13
Size: 504 Color: 17
Size: 504 Color: 13
Size: 496 Color: 7
Size: 480 Color: 3
Size: 462 Color: 1
Size: 454 Color: 4
Size: 448 Color: 19
Size: 428 Color: 11
Size: 428 Color: 3
Size: 424 Color: 10
Size: 416 Color: 14
Size: 416 Color: 11
Size: 414 Color: 15
Size: 412 Color: 18
Size: 400 Color: 0
Size: 392 Color: 5
Size: 390 Color: 16
Size: 382 Color: 19
Size: 376 Color: 6
Size: 374 Color: 10
Size: 368 Color: 16
Size: 368 Color: 16
Size: 368 Color: 9
Size: 368 Color: 6
Size: 364 Color: 5
Size: 360 Color: 18
Size: 336 Color: 13
Size: 320 Color: 2
Size: 312 Color: 2

Bin 179: 56 of cap free
Amount of items: 2
Items: 
Size: 10718 Color: 16
Size: 5562 Color: 18

Bin 180: 56 of cap free
Amount of items: 2
Items: 
Size: 13160 Color: 9
Size: 3120 Color: 11

Bin 181: 58 of cap free
Amount of items: 2
Items: 
Size: 11662 Color: 2
Size: 4616 Color: 0

Bin 182: 60 of cap free
Amount of items: 3
Items: 
Size: 10549 Color: 8
Size: 5391 Color: 14
Size: 336 Color: 1

Bin 183: 61 of cap free
Amount of items: 2
Items: 
Size: 13727 Color: 14
Size: 2548 Color: 7

Bin 184: 70 of cap free
Amount of items: 2
Items: 
Size: 11212 Color: 14
Size: 5054 Color: 5

Bin 185: 76 of cap free
Amount of items: 2
Items: 
Size: 12718 Color: 19
Size: 3542 Color: 5

Bin 186: 79 of cap free
Amount of items: 2
Items: 
Size: 13447 Color: 18
Size: 2810 Color: 17

Bin 187: 82 of cap free
Amount of items: 2
Items: 
Size: 10703 Color: 4
Size: 5551 Color: 16

Bin 188: 84 of cap free
Amount of items: 2
Items: 
Size: 12468 Color: 17
Size: 3784 Color: 7

Bin 189: 89 of cap free
Amount of items: 2
Items: 
Size: 9698 Color: 11
Size: 6549 Color: 3

Bin 190: 90 of cap free
Amount of items: 2
Items: 
Size: 10300 Color: 6
Size: 5946 Color: 15

Bin 191: 91 of cap free
Amount of items: 3
Items: 
Size: 12558 Color: 4
Size: 3253 Color: 14
Size: 434 Color: 6

Bin 192: 93 of cap free
Amount of items: 2
Items: 
Size: 11090 Color: 1
Size: 5153 Color: 6

Bin 193: 108 of cap free
Amount of items: 2
Items: 
Size: 9420 Color: 1
Size: 6808 Color: 14

Bin 194: 108 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 14
Size: 4428 Color: 5

Bin 195: 124 of cap free
Amount of items: 24
Items: 
Size: 872 Color: 17
Size: 864 Color: 9
Size: 784 Color: 1
Size: 776 Color: 14
Size: 752 Color: 8
Size: 752 Color: 6
Size: 744 Color: 4
Size: 736 Color: 10
Size: 728 Color: 0
Size: 724 Color: 5
Size: 714 Color: 8
Size: 706 Color: 15
Size: 704 Color: 19
Size: 704 Color: 14
Size: 650 Color: 1
Size: 648 Color: 12
Size: 640 Color: 8
Size: 640 Color: 0
Size: 628 Color: 5
Size: 624 Color: 3
Size: 550 Color: 13
Size: 480 Color: 2
Size: 416 Color: 9
Size: 376 Color: 2

Bin 196: 145 of cap free
Amount of items: 2
Items: 
Size: 12129 Color: 3
Size: 4062 Color: 10

Bin 197: 173 of cap free
Amount of items: 2
Items: 
Size: 9356 Color: 7
Size: 6807 Color: 1

Bin 198: 194 of cap free
Amount of items: 2
Items: 
Size: 10274 Color: 0
Size: 5868 Color: 19

Bin 199: 12702 of cap free
Amount of items: 12
Items: 
Size: 356 Color: 9
Size: 320 Color: 11
Size: 320 Color: 10
Size: 320 Color: 4
Size: 308 Color: 7
Size: 304 Color: 17
Size: 294 Color: 2
Size: 292 Color: 18
Size: 288 Color: 13
Size: 288 Color: 8
Size: 288 Color: 5
Size: 256 Color: 12

Total size: 3234528
Total free space: 16336

