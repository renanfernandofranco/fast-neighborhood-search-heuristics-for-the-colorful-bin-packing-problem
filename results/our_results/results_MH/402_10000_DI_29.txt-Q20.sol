Capicity Bin: 7512
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 6000 Color: 6
Size: 592 Color: 0
Size: 464 Color: 13
Size: 352 Color: 19
Size: 104 Color: 14

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 3720 Color: 4
Size: 1136 Color: 1
Size: 1024 Color: 3
Size: 936 Color: 16
Size: 424 Color: 7
Size: 120 Color: 5
Size: 96 Color: 15
Size: 56 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6011 Color: 17
Size: 1347 Color: 10
Size: 154 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 2
Size: 1140 Color: 19
Size: 224 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 7
Size: 684 Color: 11
Size: 128 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6052 Color: 17
Size: 1220 Color: 17
Size: 240 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5069 Color: 18
Size: 2037 Color: 14
Size: 406 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 1
Size: 1372 Color: 11
Size: 56 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5373 Color: 2
Size: 1783 Color: 10
Size: 356 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 14
Size: 724 Color: 4
Size: 120 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 13
Size: 1661 Color: 3
Size: 222 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 19
Size: 990 Color: 9
Size: 32 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 19
Size: 897 Color: 12
Size: 112 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 13
Size: 1668 Color: 5
Size: 328 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 13
Size: 815 Color: 3
Size: 162 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6571 Color: 2
Size: 785 Color: 5
Size: 156 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 11
Size: 826 Color: 7
Size: 28 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6515 Color: 4
Size: 831 Color: 11
Size: 166 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 13
Size: 852 Color: 5
Size: 168 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5402 Color: 10
Size: 1762 Color: 7
Size: 348 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 14
Size: 1620 Color: 8
Size: 280 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4281 Color: 7
Size: 2693 Color: 2
Size: 538 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6391 Color: 8
Size: 935 Color: 7
Size: 186 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6117 Color: 7
Size: 1163 Color: 1
Size: 232 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 14
Size: 2916 Color: 17
Size: 576 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4719 Color: 4
Size: 2329 Color: 17
Size: 464 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6247 Color: 17
Size: 1055 Color: 19
Size: 210 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 19
Size: 2022 Color: 3
Size: 400 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5634 Color: 13
Size: 1566 Color: 14
Size: 312 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6539 Color: 11
Size: 811 Color: 8
Size: 162 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 1
Size: 789 Color: 18
Size: 66 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6194 Color: 3
Size: 1102 Color: 0
Size: 216 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 13
Size: 1581 Color: 13
Size: 316 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 3762 Color: 4
Size: 3126 Color: 11
Size: 624 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 15
Size: 962 Color: 0
Size: 188 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 9
Size: 839 Color: 9
Size: 166 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 18
Size: 962 Color: 18
Size: 192 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5354 Color: 18
Size: 1802 Color: 0
Size: 356 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 17
Size: 1338 Color: 4
Size: 264 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5846 Color: 6
Size: 1610 Color: 16
Size: 56 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6378 Color: 19
Size: 946 Color: 13
Size: 188 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 1
Size: 1563 Color: 17
Size: 312 Color: 8

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 3
Size: 972 Color: 0
Size: 192 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4412 Color: 0
Size: 2588 Color: 4
Size: 512 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 8
Size: 1194 Color: 10
Size: 236 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 1
Size: 1070 Color: 11
Size: 212 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4314 Color: 12
Size: 2666 Color: 7
Size: 532 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 18
Size: 2674 Color: 17
Size: 532 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 17
Size: 1292 Color: 8
Size: 256 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4298 Color: 18
Size: 2682 Color: 18
Size: 532 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 14
Size: 1612 Color: 9
Size: 320 Color: 6

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 7
Size: 1028 Color: 18
Size: 200 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 10
Size: 1380 Color: 1
Size: 272 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 16
Size: 2492 Color: 16
Size: 224 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3296 Color: 9
Size: 2784 Color: 12
Size: 1432 Color: 16

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 13
Size: 2180 Color: 13
Size: 96 Color: 8

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4698 Color: 8
Size: 2346 Color: 7
Size: 468 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 3764 Color: 9
Size: 3124 Color: 8
Size: 624 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 13
Size: 2044 Color: 7
Size: 408 Color: 8

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 8
Size: 782 Color: 16
Size: 156 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 3758 Color: 0
Size: 3130 Color: 4
Size: 624 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 4
Size: 932 Color: 14
Size: 184 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6098 Color: 2
Size: 1182 Color: 1
Size: 232 Color: 19

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 3759 Color: 3
Size: 3129 Color: 4
Size: 624 Color: 19

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6204 Color: 14
Size: 1092 Color: 19
Size: 216 Color: 19

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6469 Color: 16
Size: 871 Color: 10
Size: 172 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5077 Color: 1
Size: 2031 Color: 9
Size: 404 Color: 14

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 5
Size: 1143 Color: 8
Size: 166 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5484 Color: 16
Size: 1692 Color: 16
Size: 336 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5621 Color: 19
Size: 1577 Color: 0
Size: 314 Color: 6

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6740 Color: 7
Size: 644 Color: 16
Size: 128 Color: 13

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 1
Size: 963 Color: 15
Size: 192 Color: 8

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6026 Color: 13
Size: 1242 Color: 2
Size: 244 Color: 10

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 3852 Color: 1
Size: 3052 Color: 7
Size: 608 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5894 Color: 13
Size: 1350 Color: 1
Size: 268 Color: 8

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6742 Color: 0
Size: 642 Color: 5
Size: 128 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 2
Size: 1420 Color: 19
Size: 280 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 13
Size: 780 Color: 13
Size: 144 Color: 16

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 3757 Color: 18
Size: 3131 Color: 1
Size: 624 Color: 15

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 18
Size: 737 Color: 8
Size: 16 Color: 16

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 2
Size: 722 Color: 16
Size: 140 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5082 Color: 11
Size: 2026 Color: 18
Size: 404 Color: 7

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 19
Size: 2386 Color: 17
Size: 476 Color: 15

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 11
Size: 859 Color: 4
Size: 170 Color: 11

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5034 Color: 14
Size: 2066 Color: 10
Size: 412 Color: 15

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 0
Size: 1860 Color: 15
Size: 368 Color: 10

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 6
Size: 1746 Color: 12
Size: 348 Color: 16

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 16
Size: 2524 Color: 9
Size: 504 Color: 12

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 3
Size: 980 Color: 19
Size: 192 Color: 6

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 15
Size: 701 Color: 9
Size: 108 Color: 10

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 3
Size: 670 Color: 0
Size: 132 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 16
Size: 884 Color: 0
Size: 176 Color: 8

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6219 Color: 18
Size: 1087 Color: 5
Size: 206 Color: 16

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 8
Size: 2380 Color: 9
Size: 472 Color: 6

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 11
Size: 2148 Color: 11
Size: 424 Color: 10

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4273 Color: 17
Size: 2701 Color: 14
Size: 538 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 0
Size: 1742 Color: 15
Size: 344 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5849 Color: 3
Size: 1387 Color: 0
Size: 276 Color: 14

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6287 Color: 15
Size: 1021 Color: 11
Size: 204 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6074 Color: 8
Size: 1202 Color: 8
Size: 236 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4706 Color: 8
Size: 2342 Color: 16
Size: 464 Color: 11

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 18
Size: 2520 Color: 8
Size: 296 Color: 6

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4246 Color: 7
Size: 2722 Color: 16
Size: 544 Color: 18

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6394 Color: 15
Size: 934 Color: 3
Size: 184 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 5
Size: 778 Color: 8
Size: 152 Color: 18

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6478 Color: 5
Size: 914 Color: 16
Size: 120 Color: 9

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6498 Color: 15
Size: 846 Color: 14
Size: 168 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 13
Size: 1468 Color: 11
Size: 288 Color: 6

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6675 Color: 17
Size: 699 Color: 6
Size: 138 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 4711 Color: 13
Size: 2335 Color: 8
Size: 466 Color: 15

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5810 Color: 0
Size: 1422 Color: 6
Size: 280 Color: 17

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 14
Size: 2722 Color: 3
Size: 540 Color: 11

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 0
Size: 2726 Color: 14
Size: 464 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6596 Color: 9
Size: 764 Color: 0
Size: 152 Color: 9

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 8
Size: 1964 Color: 9
Size: 392 Color: 17

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 6
Size: 1245 Color: 15
Size: 248 Color: 12

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6682 Color: 15
Size: 694 Color: 14
Size: 136 Color: 8

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 13
Size: 2988 Color: 14
Size: 368 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6399 Color: 9
Size: 929 Color: 1
Size: 184 Color: 7

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6238 Color: 1
Size: 1066 Color: 14
Size: 208 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5841 Color: 14
Size: 1393 Color: 11
Size: 278 Color: 16

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6073 Color: 10
Size: 1201 Color: 1
Size: 238 Color: 17

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 2
Size: 954 Color: 16
Size: 188 Color: 5

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5678 Color: 17
Size: 1530 Color: 4
Size: 304 Color: 12

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 1
Size: 2092 Color: 12
Size: 416 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5410 Color: 14
Size: 1754 Color: 2
Size: 348 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6563 Color: 13
Size: 791 Color: 13
Size: 158 Color: 8

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 7
Size: 745 Color: 1
Size: 148 Color: 8

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 6
Size: 737 Color: 6
Size: 146 Color: 11

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 18
Size: 1789 Color: 7
Size: 356 Color: 10

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 4289 Color: 8
Size: 2687 Color: 12
Size: 536 Color: 11

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6731 Color: 15
Size: 651 Color: 10
Size: 130 Color: 9

Total size: 991584
Total free space: 0

