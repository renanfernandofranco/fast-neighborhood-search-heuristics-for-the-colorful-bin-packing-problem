Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1236 Color: 142
Size: 572 Color: 112
Size: 272 Color: 80
Size: 272 Color: 79
Size: 64 Color: 26
Size: 32 Color: 7
Size: 8 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 159
Size: 684 Color: 120
Size: 40 Color: 10

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 2188 Color: 199
Size: 128 Color: 53
Size: 84 Color: 36
Size: 56 Color: 21

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 166
Size: 384 Color: 90
Size: 244 Color: 74

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 170
Size: 476 Color: 102
Size: 88 Color: 41

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1994 Color: 180
Size: 438 Color: 100
Size: 16 Color: 4
Size: 8 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 164
Size: 554 Color: 110
Size: 84 Color: 38

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 171
Size: 478 Color: 103
Size: 80 Color: 33

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 154
Size: 576 Color: 113
Size: 236 Color: 72

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 161
Size: 562 Color: 111
Size: 112 Color: 49

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 147
Size: 806 Color: 126
Size: 160 Color: 60

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 179
Size: 396 Color: 92
Size: 72 Color: 29

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 139
Size: 1042 Color: 136
Size: 184 Color: 65

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 146
Size: 1020 Color: 133
Size: 40 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 172
Size: 402 Color: 94
Size: 120 Color: 52

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2165 Color: 196
Size: 243 Color: 73
Size: 48 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1870 Color: 168
Size: 490 Color: 105
Size: 96 Color: 43

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 186
Size: 324 Color: 85
Size: 64 Color: 24

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 1688 Color: 157
Size: 504 Color: 106
Size: 176 Color: 63
Size: 88 Color: 40

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 145
Size: 890 Color: 129
Size: 176 Color: 62

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1382 Color: 144
Size: 974 Color: 131
Size: 100 Color: 45

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 148
Size: 805 Color: 125
Size: 160 Color: 61

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 141
Size: 1022 Color: 135
Size: 200 Color: 66

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 189
Size: 310 Color: 83
Size: 56 Color: 20

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 153
Size: 694 Color: 121
Size: 136 Color: 56

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 175
Size: 430 Color: 97
Size: 84 Color: 37

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 177
Size: 386 Color: 91
Size: 96 Color: 44

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 183
Size: 402 Color: 95
Size: 20 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 185
Size: 334 Color: 86
Size: 64 Color: 25

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 150
Size: 840 Color: 127
Size: 80 Color: 32

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 198
Size: 200 Color: 67
Size: 76 Color: 30

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 174
Size: 436 Color: 99
Size: 80 Color: 34

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 194
Size: 254 Color: 76
Size: 48 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 182
Size: 364 Color: 88
Size: 64 Color: 27

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 188
Size: 370 Color: 89
Size: 8 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 984 Color: 132
Size: 884 Color: 128
Size: 588 Color: 114

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 156
Size: 642 Color: 118
Size: 128 Color: 54

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 165
Size: 526 Color: 108
Size: 104 Color: 47

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 190
Size: 300 Color: 82
Size: 56 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 197
Size: 228 Color: 71
Size: 56 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 163
Size: 604 Color: 116
Size: 40 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 158
Size: 606 Color: 117
Size: 120 Color: 51

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 149
Size: 772 Color: 124
Size: 152 Color: 59

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 195
Size: 244 Color: 75
Size: 48 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 178
Size: 399 Color: 93
Size: 78 Color: 31

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2105 Color: 191
Size: 293 Color: 81
Size: 58 Color: 22

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1373 Color: 143
Size: 903 Color: 130
Size: 180 Color: 64

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 192
Size: 270 Color: 78
Size: 52 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 151
Size: 738 Color: 123
Size: 144 Color: 58

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2077 Color: 187
Size: 317 Color: 84
Size: 62 Color: 23

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 201
Size: 213 Color: 69
Size: 42 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 167
Size: 511 Color: 107
Size: 102 Color: 46

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 152
Size: 723 Color: 122
Size: 144 Color: 57

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 184
Size: 345 Color: 87
Size: 68 Color: 28

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2015 Color: 181
Size: 439 Color: 101
Size: 2 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1959 Color: 176
Size: 415 Color: 96
Size: 82 Color: 35

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 140
Size: 1021 Color: 134
Size: 202 Color: 68

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1883 Color: 169
Size: 479 Color: 104
Size: 94 Color: 42

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 138
Size: 1197 Color: 137
Size: 30 Color: 6

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 162
Size: 551 Color: 109
Size: 110 Color: 48

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 200
Size: 222 Color: 70
Size: 44 Color: 12

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 173
Size: 435 Color: 98
Size: 86 Color: 39

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 155
Size: 655 Color: 119
Size: 130 Color: 55

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1739 Color: 160
Size: 599 Color: 115
Size: 118 Color: 50

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2149 Color: 193
Size: 257 Color: 77
Size: 50 Color: 16

Total size: 159640
Total free space: 0

