Capicity Bin: 16160
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 1
Size: 6648 Color: 1
Size: 1344 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 1
Size: 3712 Color: 1
Size: 744 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9252 Color: 1
Size: 4856 Color: 1
Size: 2052 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11568 Color: 1
Size: 3856 Color: 1
Size: 736 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 1
Size: 3832 Color: 1
Size: 752 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 7724 Color: 1
Size: 5812 Color: 1
Size: 2304 Color: 0
Size: 320 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8148 Color: 1
Size: 6684 Color: 1
Size: 1328 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 10148 Color: 1
Size: 5528 Color: 1
Size: 384 Color: 0
Size: 100 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8081 Color: 1
Size: 6733 Color: 1
Size: 1346 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 9622 Color: 1
Size: 5450 Color: 1
Size: 1088 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9163 Color: 1
Size: 5829 Color: 1
Size: 1168 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10948 Color: 1
Size: 3066 Color: 0
Size: 2146 Color: 1

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 8200 Color: 1
Size: 6696 Color: 1
Size: 656 Color: 0
Size: 608 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 1
Size: 5844 Color: 1
Size: 1128 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8085 Color: 1
Size: 6731 Color: 1
Size: 1344 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8944 Color: 1
Size: 6736 Color: 1
Size: 480 Color: 0

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 8082 Color: 1
Size: 5780 Color: 1
Size: 1722 Color: 0
Size: 576 Color: 0

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 9148 Color: 1
Size: 4112 Color: 1
Size: 2516 Color: 0
Size: 384 Color: 0

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 13048 Color: 1
Size: 2408 Color: 1
Size: 352 Color: 0
Size: 352 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8086 Color: 1
Size: 6730 Color: 1
Size: 1344 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8088 Color: 1
Size: 6728 Color: 1
Size: 1344 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8092 Color: 1
Size: 6724 Color: 1
Size: 1344 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8104 Color: 1
Size: 6728 Color: 1
Size: 1328 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 9268 Color: 1
Size: 5748 Color: 1
Size: 1144 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 9560 Color: 1
Size: 5512 Color: 1
Size: 1088 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 9648 Color: 1
Size: 6032 Color: 1
Size: 480 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 1
Size: 5406 Color: 1
Size: 1080 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 10144 Color: 1
Size: 5440 Color: 1
Size: 576 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 1
Size: 4964 Color: 1
Size: 984 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 10224 Color: 1
Size: 5456 Color: 1
Size: 480 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 10260 Color: 1
Size: 4924 Color: 1
Size: 976 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 10360 Color: 1
Size: 4840 Color: 1
Size: 960 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 1
Size: 4296 Color: 1
Size: 848 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11036 Color: 1
Size: 3740 Color: 1
Size: 1384 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11125 Color: 1
Size: 4197 Color: 1
Size: 838 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11129 Color: 1
Size: 4193 Color: 1
Size: 838 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 4248 Color: 1
Size: 272 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11660 Color: 1
Size: 3756 Color: 1
Size: 744 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11676 Color: 1
Size: 3844 Color: 1
Size: 640 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11708 Color: 1
Size: 3804 Color: 1
Size: 648 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11763 Color: 1
Size: 3665 Color: 1
Size: 732 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11824 Color: 1
Size: 3352 Color: 1
Size: 984 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 1
Size: 3720 Color: 1
Size: 336 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 1
Size: 3768 Color: 1
Size: 240 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 1
Size: 2952 Color: 1
Size: 992 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12233 Color: 1
Size: 3355 Color: 1
Size: 572 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 1
Size: 3332 Color: 1
Size: 584 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12272 Color: 1
Size: 3360 Color: 1
Size: 528 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 1
Size: 3240 Color: 1
Size: 640 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 1
Size: 3212 Color: 1
Size: 640 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 1
Size: 3024 Color: 1
Size: 704 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 1
Size: 2936 Color: 1
Size: 742 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12557 Color: 1
Size: 2731 Color: 1
Size: 872 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 1
Size: 3248 Color: 1
Size: 352 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12604 Color: 1
Size: 2124 Color: 1
Size: 1432 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 2916 Color: 1
Size: 612 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12778 Color: 1
Size: 2554 Color: 1
Size: 828 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 1
Size: 2170 Color: 1
Size: 1160 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 1
Size: 2776 Color: 1
Size: 544 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12880 Color: 1
Size: 2826 Color: 1
Size: 454 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12900 Color: 1
Size: 2724 Color: 1
Size: 536 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 1
Size: 2778 Color: 1
Size: 418 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 1
Size: 1812 Color: 1
Size: 1364 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13018 Color: 1
Size: 2622 Color: 1
Size: 520 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 1
Size: 2512 Color: 1
Size: 544 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13165 Color: 1
Size: 2497 Color: 1
Size: 498 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13168 Color: 1
Size: 2576 Color: 1
Size: 416 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13169 Color: 1
Size: 2483 Color: 1
Size: 508 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 1
Size: 2468 Color: 1
Size: 488 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 1
Size: 2416 Color: 1
Size: 512 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 1
Size: 2394 Color: 1
Size: 498 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 1
Size: 2440 Color: 1
Size: 448 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 1
Size: 1720 Color: 0
Size: 1152 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13290 Color: 1
Size: 2106 Color: 1
Size: 764 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 1
Size: 2496 Color: 1
Size: 368 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 1
Size: 2252 Color: 1
Size: 556 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13408 Color: 1
Size: 1904 Color: 1
Size: 848 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 1
Size: 2448 Color: 1
Size: 296 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 1
Size: 2412 Color: 1
Size: 288 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13463 Color: 1
Size: 2249 Color: 1
Size: 448 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13468 Color: 1
Size: 1744 Color: 1
Size: 948 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13506 Color: 1
Size: 2214 Color: 1
Size: 440 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 1
Size: 2344 Color: 1
Size: 272 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13570 Color: 1
Size: 2162 Color: 1
Size: 428 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13651 Color: 1
Size: 1773 Color: 1
Size: 736 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13675 Color: 1
Size: 2071 Color: 1
Size: 414 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13679 Color: 1
Size: 2069 Color: 1
Size: 412 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 1
Size: 2408 Color: 1
Size: 52 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 1
Size: 1952 Color: 1
Size: 476 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13738 Color: 1
Size: 1964 Color: 1
Size: 458 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13768 Color: 1
Size: 2104 Color: 1
Size: 288 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13770 Color: 1
Size: 1788 Color: 1
Size: 602 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 1
Size: 1922 Color: 1
Size: 448 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 1
Size: 2008 Color: 1
Size: 344 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 13812 Color: 1
Size: 1484 Color: 1
Size: 864 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 1
Size: 1862 Color: 1
Size: 478 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 13827 Color: 1
Size: 1945 Color: 1
Size: 388 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 1
Size: 1956 Color: 1
Size: 324 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 13888 Color: 1
Size: 1776 Color: 1
Size: 496 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 13893 Color: 1
Size: 1891 Color: 1
Size: 376 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 13936 Color: 1
Size: 1840 Color: 1
Size: 384 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13951 Color: 1
Size: 1841 Color: 1
Size: 368 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 1
Size: 1550 Color: 1
Size: 656 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 13968 Color: 1
Size: 1912 Color: 1
Size: 280 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 1484 Color: 1
Size: 688 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14006 Color: 1
Size: 1798 Color: 1
Size: 356 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 1
Size: 1460 Color: 1
Size: 680 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14021 Color: 1
Size: 1889 Color: 1
Size: 250 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 1
Size: 1456 Color: 1
Size: 672 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14033 Color: 1
Size: 2095 Color: 1
Size: 32 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 1
Size: 1792 Color: 1
Size: 328 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 1604 Color: 1
Size: 496 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14081 Color: 1
Size: 1733 Color: 1
Size: 346 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14093 Color: 1
Size: 1723 Color: 1
Size: 344 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14096 Color: 1
Size: 1792 Color: 1
Size: 272 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 1
Size: 1630 Color: 1
Size: 432 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 1
Size: 1328 Color: 1
Size: 728 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 1
Size: 1700 Color: 1
Size: 336 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14160 Color: 1
Size: 1968 Color: 1
Size: 32 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1768 Color: 1
Size: 224 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14172 Color: 1
Size: 1624 Color: 1
Size: 364 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14173 Color: 1
Size: 1657 Color: 1
Size: 330 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 1
Size: 1654 Color: 1
Size: 328 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14206 Color: 1
Size: 1534 Color: 1
Size: 420 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 1
Size: 1184 Color: 1
Size: 760 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14233 Color: 1
Size: 1799 Color: 1
Size: 128 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 1
Size: 1672 Color: 1
Size: 252 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14237 Color: 1
Size: 1603 Color: 1
Size: 320 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 1
Size: 1090 Color: 0
Size: 800 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 1
Size: 1564 Color: 1
Size: 304 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 1
Size: 1312 Color: 1
Size: 546 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 1
Size: 1372 Color: 1
Size: 408 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14382 Color: 1
Size: 1482 Color: 1
Size: 296 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 1
Size: 1164 Color: 0
Size: 608 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14446 Color: 1
Size: 1430 Color: 1
Size: 284 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 1
Size: 1428 Color: 1
Size: 284 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 1
Size: 1680 Color: 1
Size: 28 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 1
Size: 1192 Color: 1
Size: 512 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14504 Color: 1
Size: 1056 Color: 1
Size: 600 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 1
Size: 1374 Color: 1
Size: 272 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 1
Size: 1104 Color: 1
Size: 540 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 1
Size: 1328 Color: 1
Size: 308 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 14526 Color: 1
Size: 896 Color: 1
Size: 738 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 14544 Color: 1
Size: 1360 Color: 1
Size: 256 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 11185 Color: 1
Size: 4316 Color: 1
Size: 658 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 11596 Color: 1
Size: 4147 Color: 1
Size: 416 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 12062 Color: 1
Size: 3137 Color: 1
Size: 960 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 12196 Color: 1
Size: 3691 Color: 1
Size: 272 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 12541 Color: 1
Size: 3314 Color: 1
Size: 304 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 1
Size: 2972 Color: 1
Size: 304 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 12911 Color: 1
Size: 3120 Color: 1
Size: 128 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13181 Color: 1
Size: 1842 Color: 1
Size: 1136 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 1
Size: 1756 Color: 1
Size: 1000 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 1
Size: 2275 Color: 1
Size: 396 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 1
Size: 2256 Color: 1
Size: 256 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 1
Size: 5461 Color: 1
Size: 354 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 11703 Color: 1
Size: 3384 Color: 1
Size: 1072 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 10250 Color: 1
Size: 4948 Color: 1
Size: 960 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 10435 Color: 1
Size: 5363 Color: 1
Size: 360 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 10768 Color: 1
Size: 4926 Color: 1
Size: 464 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 12186 Color: 1
Size: 2768 Color: 1
Size: 1204 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 1
Size: 3293 Color: 1
Size: 320 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 13634 Color: 1
Size: 2184 Color: 1
Size: 340 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 1
Size: 4976 Color: 1
Size: 954 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 1
Size: 3418 Color: 0
Size: 1660 Color: 1

Bin 166: 3 of cap free
Amount of items: 3
Items: 
Size: 10922 Color: 1
Size: 4771 Color: 1
Size: 464 Color: 0

Bin 167: 3 of cap free
Amount of items: 3
Items: 
Size: 12209 Color: 1
Size: 3288 Color: 1
Size: 660 Color: 0

Bin 168: 3 of cap free
Amount of items: 3
Items: 
Size: 13148 Color: 1
Size: 2299 Color: 1
Size: 710 Color: 0

Bin 169: 4 of cap free
Amount of items: 3
Items: 
Size: 11774 Color: 1
Size: 3830 Color: 1
Size: 552 Color: 0

Bin 170: 4 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 1
Size: 4348 Color: 1
Size: 820 Color: 0

Bin 171: 4 of cap free
Amount of items: 3
Items: 
Size: 11248 Color: 1
Size: 4276 Color: 1
Size: 632 Color: 0

Bin 172: 5 of cap free
Amount of items: 3
Items: 
Size: 9725 Color: 1
Size: 5982 Color: 1
Size: 448 Color: 0

Bin 173: 6 of cap free
Amount of items: 3
Items: 
Size: 10694 Color: 1
Size: 5012 Color: 1
Size: 448 Color: 0

Bin 174: 7 of cap free
Amount of items: 3
Items: 
Size: 11731 Color: 1
Size: 4102 Color: 1
Size: 320 Color: 0

Bin 175: 7 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 1
Size: 3017 Color: 1
Size: 468 Color: 0

Bin 176: 8 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 1722 Color: 1
Size: 576 Color: 0

Bin 177: 8 of cap free
Amount of items: 3
Items: 
Size: 10206 Color: 1
Size: 4962 Color: 1
Size: 984 Color: 0

Bin 178: 8 of cap free
Amount of items: 3
Items: 
Size: 11238 Color: 1
Size: 4496 Color: 1
Size: 418 Color: 0

Bin 179: 9 of cap free
Amount of items: 5
Items: 
Size: 8084 Color: 1
Size: 4366 Color: 1
Size: 2709 Color: 1
Size: 608 Color: 0
Size: 384 Color: 0

Bin 180: 9 of cap free
Amount of items: 3
Items: 
Size: 9167 Color: 1
Size: 6664 Color: 1
Size: 320 Color: 0

Bin 181: 9 of cap free
Amount of items: 3
Items: 
Size: 13338 Color: 1
Size: 2493 Color: 1
Size: 320 Color: 0

Bin 182: 10 of cap free
Amount of items: 3
Items: 
Size: 13098 Color: 1
Size: 2668 Color: 1
Size: 384 Color: 0

Bin 183: 11 of cap free
Amount of items: 7
Items: 
Size: 3632 Color: 1
Size: 3268 Color: 1
Size: 2600 Color: 0
Size: 2244 Color: 1
Size: 2091 Color: 1
Size: 1994 Color: 0
Size: 320 Color: 0

Bin 184: 12 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 1
Size: 6528 Color: 1
Size: 320 Color: 0

Bin 185: 15 of cap free
Amount of items: 3
Items: 
Size: 9609 Color: 1
Size: 5784 Color: 1
Size: 752 Color: 0

Bin 186: 15 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 1
Size: 3715 Color: 1
Size: 3444 Color: 0

Bin 187: 22 of cap free
Amount of items: 3
Items: 
Size: 11168 Color: 1
Size: 3658 Color: 1
Size: 1312 Color: 0

Bin 188: 23 of cap free
Amount of items: 3
Items: 
Size: 13431 Color: 1
Size: 2354 Color: 1
Size: 352 Color: 0

Bin 189: 24 of cap free
Amount of items: 3
Items: 
Size: 8922 Color: 1
Size: 6734 Color: 1
Size: 480 Color: 0

Bin 190: 25 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 1
Size: 6287 Color: 1
Size: 320 Color: 0

Bin 191: 46 of cap free
Amount of items: 3
Items: 
Size: 9224 Color: 1
Size: 6034 Color: 1
Size: 856 Color: 0

Bin 192: 116 of cap free
Amount of items: 3
Items: 
Size: 10467 Color: 1
Size: 4745 Color: 1
Size: 832 Color: 0

Bin 193: 649 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 1
Size: 3003 Color: 1
Size: 480 Color: 0

Bin 194: 1748 of cap free
Amount of items: 1
Items: 
Size: 14412 Color: 1

Bin 195: 1916 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 1
Size: 5724 Color: 1
Size: 384 Color: 0

Bin 196: 2006 of cap free
Amount of items: 1
Items: 
Size: 14154 Color: 1

Bin 197: 2282 of cap free
Amount of items: 3
Items: 
Size: 8112 Color: 1
Size: 5414 Color: 1
Size: 352 Color: 0

Bin 198: 2520 of cap free
Amount of items: 1
Items: 
Size: 13640 Color: 1

Bin 199: 4594 of cap free
Amount of items: 1
Items: 
Size: 11566 Color: 1

Total size: 3199680
Total free space: 16160

