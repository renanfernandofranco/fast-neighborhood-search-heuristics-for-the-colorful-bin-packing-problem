Capicity Bin: 6528
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 2432 Color: 1
Size: 2232 Color: 0
Size: 904 Color: 0
Size: 440 Color: 0
Size: 352 Color: 1
Size: 120 Color: 1
Size: 48 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 5232 Color: 1
Size: 800 Color: 0
Size: 408 Color: 1
Size: 88 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3282 Color: 0
Size: 2706 Color: 1
Size: 540 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 1
Size: 940 Color: 0
Size: 184 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 5692 Color: 0
Size: 700 Color: 1
Size: 112 Color: 0
Size: 24 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 1
Size: 1570 Color: 0
Size: 312 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5405 Color: 1
Size: 937 Color: 1
Size: 186 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 0
Size: 791 Color: 1
Size: 78 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4582 Color: 1
Size: 1622 Color: 0
Size: 324 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5854 Color: 0
Size: 562 Color: 0
Size: 112 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5148 Color: 1
Size: 1156 Color: 1
Size: 224 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 1
Size: 1121 Color: 1
Size: 224 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4929 Color: 0
Size: 1407 Color: 0
Size: 192 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 1
Size: 1580 Color: 0
Size: 312 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5777 Color: 0
Size: 627 Color: 1
Size: 124 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 0
Size: 643 Color: 1
Size: 128 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 3273 Color: 0
Size: 2713 Color: 0
Size: 542 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 3751 Color: 0
Size: 2315 Color: 0
Size: 462 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 0
Size: 1842 Color: 0
Size: 364 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5119 Color: 1
Size: 1175 Color: 1
Size: 234 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 3266 Color: 1
Size: 2722 Color: 1
Size: 540 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 3265 Color: 1
Size: 2721 Color: 1
Size: 542 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 0
Size: 1484 Color: 0
Size: 296 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5626 Color: 0
Size: 754 Color: 1
Size: 148 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 0
Size: 1171 Color: 1
Size: 234 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 3775 Color: 1
Size: 2295 Color: 0
Size: 458 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 1
Size: 660 Color: 1
Size: 128 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 3795 Color: 0
Size: 2279 Color: 1
Size: 454 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5139 Color: 1
Size: 1159 Color: 0
Size: 230 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5167 Color: 1
Size: 1135 Color: 0
Size: 226 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5366 Color: 0
Size: 970 Color: 0
Size: 192 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 0
Size: 564 Color: 0
Size: 112 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5322 Color: 1
Size: 1006 Color: 1
Size: 200 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 3460 Color: 1
Size: 2564 Color: 0
Size: 504 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5070 Color: 1
Size: 1218 Color: 0
Size: 240 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 3819 Color: 1
Size: 2259 Color: 1
Size: 450 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 0
Size: 756 Color: 1
Size: 144 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 0
Size: 2138 Color: 1
Size: 424 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5342 Color: 0
Size: 990 Color: 0
Size: 196 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 1
Size: 1195 Color: 1
Size: 238 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 1
Size: 1564 Color: 0
Size: 304 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4509 Color: 1
Size: 1683 Color: 0
Size: 336 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 3269 Color: 0
Size: 2717 Color: 0
Size: 542 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5815 Color: 1
Size: 649 Color: 1
Size: 64 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 1
Size: 1044 Color: 1
Size: 208 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5694 Color: 1
Size: 698 Color: 0
Size: 136 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4971 Color: 1
Size: 1299 Color: 0
Size: 258 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4060 Color: 1
Size: 2060 Color: 0
Size: 408 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 0
Size: 860 Color: 1
Size: 168 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 0
Size: 1437 Color: 1
Size: 88 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4412 Color: 0
Size: 1764 Color: 1
Size: 352 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5046 Color: 1
Size: 1238 Color: 0
Size: 244 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5143 Color: 0
Size: 1155 Color: 0
Size: 230 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 0
Size: 610 Color: 0
Size: 120 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3268 Color: 1
Size: 2724 Color: 0
Size: 536 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5413 Color: 1
Size: 931 Color: 0
Size: 184 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 0
Size: 1692 Color: 0
Size: 336 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 4226 Color: 1
Size: 2214 Color: 0
Size: 88 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5814 Color: 0
Size: 618 Color: 1
Size: 96 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 1
Size: 794 Color: 0
Size: 4 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 0
Size: 1139 Color: 1
Size: 226 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 3290 Color: 1
Size: 2702 Color: 0
Size: 536 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 1
Size: 818 Color: 0
Size: 160 Color: 0

Bin 64: 0 of cap free
Amount of items: 4
Items: 
Size: 3224 Color: 0
Size: 2856 Color: 1
Size: 312 Color: 0
Size: 136 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5454 Color: 0
Size: 898 Color: 1
Size: 176 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 1
Size: 1965 Color: 1
Size: 392 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 3709 Color: 1
Size: 2351 Color: 1
Size: 468 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 0
Size: 1151 Color: 1
Size: 230 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4737 Color: 1
Size: 1493 Color: 0
Size: 298 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4967 Color: 0
Size: 1517 Color: 1
Size: 44 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5489 Color: 1
Size: 867 Color: 0
Size: 172 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 3738 Color: 1
Size: 2326 Color: 0
Size: 464 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4995 Color: 0
Size: 1279 Color: 1
Size: 254 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 4999 Color: 0
Size: 1275 Color: 0
Size: 254 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 3823 Color: 1
Size: 2255 Color: 1
Size: 450 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 0
Size: 734 Color: 0
Size: 144 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5683 Color: 1
Size: 705 Color: 0
Size: 140 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 0
Size: 772 Color: 0
Size: 152 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 1
Size: 1388 Color: 0
Size: 272 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4622 Color: 1
Size: 1590 Color: 1
Size: 316 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5791 Color: 1
Size: 615 Color: 0
Size: 122 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5526 Color: 1
Size: 838 Color: 1
Size: 164 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 3676 Color: 1
Size: 2380 Color: 0
Size: 472 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5381 Color: 0
Size: 957 Color: 0
Size: 190 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5670 Color: 1
Size: 718 Color: 1
Size: 140 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 0
Size: 1284 Color: 1
Size: 256 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4741 Color: 0
Size: 1491 Color: 0
Size: 296 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 0
Size: 1025 Color: 0
Size: 204 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 1
Size: 644 Color: 1
Size: 120 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5401 Color: 0
Size: 941 Color: 1
Size: 186 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 3827 Color: 0
Size: 2251 Color: 1
Size: 450 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5409 Color: 1
Size: 933 Color: 0
Size: 186 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 1
Size: 1254 Color: 1
Size: 248 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 1
Size: 948 Color: 0
Size: 184 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 3860 Color: 1
Size: 2228 Color: 0
Size: 440 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4346 Color: 0
Size: 1822 Color: 1
Size: 360 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 3831 Color: 1
Size: 2249 Color: 0
Size: 448 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 1
Size: 2212 Color: 1
Size: 112 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5027 Color: 1
Size: 1325 Color: 0
Size: 176 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 1
Size: 1220 Color: 0
Size: 24 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 4548 Color: 0
Size: 1652 Color: 1
Size: 328 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 0
Size: 894 Color: 0
Size: 176 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 3277 Color: 1
Size: 2711 Color: 0
Size: 540 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 0
Size: 1954 Color: 1
Size: 324 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5099 Color: 1
Size: 1191 Color: 0
Size: 238 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 3771 Color: 0
Size: 2299 Color: 1
Size: 458 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 3894 Color: 1
Size: 2198 Color: 1
Size: 436 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 0
Size: 566 Color: 0
Size: 112 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 1
Size: 1082 Color: 0
Size: 216 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 0
Size: 1828 Color: 0
Size: 360 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 0
Size: 1131 Color: 0
Size: 226 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 3799 Color: 0
Size: 2275 Color: 0
Size: 454 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5206 Color: 0
Size: 1102 Color: 0
Size: 220 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5712 Color: 0
Size: 680 Color: 0
Size: 136 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 0
Size: 1148 Color: 0
Size: 224 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4878 Color: 0
Size: 1378 Color: 0
Size: 272 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 1
Size: 1398 Color: 0
Size: 276 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5599 Color: 1
Size: 775 Color: 0
Size: 154 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 1
Size: 761 Color: 1
Size: 152 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5531 Color: 0
Size: 831 Color: 1
Size: 166 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4671 Color: 1
Size: 1549 Color: 1
Size: 308 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5575 Color: 1
Size: 795 Color: 1
Size: 158 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 3747 Color: 0
Size: 2319 Color: 1
Size: 462 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5839 Color: 1
Size: 575 Color: 1
Size: 114 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 3722 Color: 0
Size: 2530 Color: 1
Size: 276 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 0
Size: 811 Color: 1
Size: 162 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 1
Size: 1749 Color: 0
Size: 324 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 3274 Color: 0
Size: 2714 Color: 1
Size: 540 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5787 Color: 0
Size: 619 Color: 0
Size: 122 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5579 Color: 1
Size: 873 Color: 1
Size: 76 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 4237 Color: 0
Size: 1911 Color: 1
Size: 380 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 1
Size: 571 Color: 1
Size: 114 Color: 0

Total size: 861696
Total free space: 0

