Capicity Bin: 15760
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 7896 Color: 411
Size: 6524 Color: 393
Size: 528 Color: 125
Size: 416 Color: 88
Size: 396 Color: 85

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 7908 Color: 412
Size: 6532 Color: 394
Size: 540 Color: 126
Size: 392 Color: 84
Size: 388 Color: 82

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 8996 Color: 423
Size: 5644 Color: 381
Size: 412 Color: 87
Size: 360 Color: 69
Size: 348 Color: 67

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9624 Color: 429
Size: 5816 Color: 387
Size: 320 Color: 52

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11890 Color: 471
Size: 3022 Color: 324
Size: 848 Color: 162

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 476
Size: 3084 Color: 327
Size: 616 Color: 138

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 477
Size: 3324 Color: 335
Size: 360 Color: 70

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 478
Size: 2122 Color: 280
Size: 1560 Color: 231

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 483
Size: 3260 Color: 333
Size: 316 Color: 50

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 484
Size: 2296 Color: 293
Size: 1248 Color: 192

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 490
Size: 2226 Color: 288
Size: 1028 Color: 179

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 495
Size: 2458 Color: 302
Size: 718 Color: 149

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12624 Color: 497
Size: 2648 Color: 310
Size: 488 Color: 115

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 502
Size: 1644 Color: 241
Size: 1304 Color: 196

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12814 Color: 503
Size: 1962 Color: 267
Size: 984 Color: 175

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 505
Size: 1576 Color: 233
Size: 1312 Color: 199

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 508
Size: 1912 Color: 264
Size: 924 Color: 169

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 509
Size: 2116 Color: 278
Size: 688 Color: 146

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 510
Size: 2401 Color: 299
Size: 354 Color: 68

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 512
Size: 2408 Color: 300
Size: 336 Color: 61

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13138 Color: 517
Size: 2186 Color: 286
Size: 436 Color: 98

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13170 Color: 520
Size: 1844 Color: 258
Size: 746 Color: 153

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13218 Color: 523
Size: 2118 Color: 279
Size: 424 Color: 91

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 524
Size: 2188 Color: 287
Size: 344 Color: 65

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 528
Size: 1964 Color: 268
Size: 432 Color: 97

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13371 Color: 529
Size: 2065 Color: 273
Size: 324 Color: 54

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 532
Size: 1448 Color: 218
Size: 912 Color: 167

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 533
Size: 2185 Color: 285
Size: 172 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 537
Size: 1136 Color: 185
Size: 1128 Color: 184

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13503 Color: 538
Size: 1983 Color: 271
Size: 274 Color: 23

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 542
Size: 1920 Color: 265
Size: 300 Color: 40

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13548 Color: 544
Size: 1756 Color: 253
Size: 456 Color: 104

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13707 Color: 553
Size: 1633 Color: 240
Size: 420 Color: 90

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13708 Color: 554
Size: 1624 Color: 236
Size: 428 Color: 95

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 555
Size: 1699 Color: 247
Size: 338 Color: 62

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 556
Size: 1312 Color: 200
Size: 712 Color: 148

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13803 Color: 560
Size: 1631 Color: 239
Size: 326 Color: 55

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13805 Color: 561
Size: 1563 Color: 232
Size: 392 Color: 83

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 562
Size: 1528 Color: 228
Size: 424 Color: 92

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 563
Size: 1625 Color: 237
Size: 324 Color: 53

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 565
Size: 1414 Color: 215
Size: 492 Color: 118

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 567
Size: 1412 Color: 214
Size: 480 Color: 114

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13879 Color: 568
Size: 1549 Color: 230
Size: 332 Color: 58

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 569
Size: 964 Color: 172
Size: 916 Color: 168

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 574
Size: 1546 Color: 229
Size: 276 Color: 26

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 575
Size: 1444 Color: 217
Size: 372 Color: 77

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13961 Color: 578
Size: 1501 Color: 224
Size: 298 Color: 39

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13982 Color: 580
Size: 1494 Color: 223
Size: 284 Color: 29

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14023 Color: 583
Size: 1449 Color: 219
Size: 288 Color: 30

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 584
Size: 1312 Color: 202
Size: 424 Color: 93

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 585
Size: 1296 Color: 195
Size: 436 Color: 99

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 589
Size: 1383 Color: 211
Size: 304 Color: 42

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14090 Color: 590
Size: 1120 Color: 183
Size: 550 Color: 127

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 591
Size: 1386 Color: 212
Size: 276 Color: 24

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 594
Size: 992 Color: 177
Size: 628 Color: 139

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14141 Color: 595
Size: 1351 Color: 208
Size: 268 Color: 19

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14146 Color: 596
Size: 1136 Color: 186
Size: 478 Color: 112

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 597
Size: 1140 Color: 187
Size: 472 Color: 110

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14165 Color: 598
Size: 1331 Color: 205
Size: 264 Color: 16

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14170 Color: 599
Size: 1322 Color: 203
Size: 268 Color: 20

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 10910 Color: 452
Size: 4585 Color: 363
Size: 264 Color: 15

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 465
Size: 4042 Color: 354
Size: 144 Color: 5

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 12151 Color: 481
Size: 3608 Color: 342

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12246 Color: 486
Size: 3009 Color: 322
Size: 504 Color: 120

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12909 Color: 507
Size: 1674 Color: 244
Size: 1176 Color: 190

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13013 Color: 511
Size: 2456 Color: 301
Size: 290 Color: 35

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 518
Size: 1586 Color: 235
Size: 1034 Color: 180

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13213 Color: 522
Size: 2266 Color: 291
Size: 280 Color: 27

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 531
Size: 2377 Color: 298

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 541
Size: 1356 Color: 209
Size: 864 Color: 165

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13542 Color: 543
Size: 1753 Color: 252
Size: 464 Color: 106

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 13953 Color: 577
Size: 1806 Color: 256

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 14111 Color: 593
Size: 1648 Color: 242

Bin 74: 2 of cap free
Amount of items: 5
Items: 
Size: 7940 Color: 414
Size: 6562 Color: 396
Size: 488 Color: 117
Size: 384 Color: 79
Size: 384 Color: 78

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 491
Size: 3222 Color: 332

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12790 Color: 500
Size: 2968 Color: 320

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13090 Color: 515
Size: 2668 Color: 312

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 516
Size: 2662 Color: 311

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 527
Size: 2470 Color: 304

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 549
Size: 2102 Color: 277

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 557
Size: 2004 Color: 272

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 573
Size: 1852 Color: 261

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 588
Size: 1690 Color: 246

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 600
Size: 1580 Color: 234

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 470
Size: 3377 Color: 336
Size: 600 Color: 135

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 12156 Color: 482
Size: 3601 Color: 341

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 576
Size: 1809 Color: 257

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 12548 Color: 492
Size: 3208 Color: 331

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 13042 Color: 513
Size: 2714 Color: 314

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13594 Color: 547
Size: 2162 Color: 283

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 581
Size: 1768 Color: 254

Bin 92: 5 of cap free
Amount of items: 3
Items: 
Size: 11276 Color: 458
Size: 3024 Color: 325
Size: 1455 Color: 220

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 493
Size: 3191 Color: 330

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 14101 Color: 592
Size: 1654 Color: 243

Bin 95: 6 of cap free
Amount of items: 5
Items: 
Size: 9016 Color: 424
Size: 5652 Color: 382
Size: 400 Color: 86
Size: 344 Color: 66
Size: 342 Color: 64

Bin 96: 6 of cap free
Amount of items: 4
Items: 
Size: 9400 Color: 426
Size: 5682 Color: 383
Size: 336 Color: 60
Size: 336 Color: 59

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 9818 Color: 432
Size: 5624 Color: 380
Size: 312 Color: 48

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 12140 Color: 480
Size: 3614 Color: 343

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 501
Size: 2956 Color: 319

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 12824 Color: 504
Size: 2930 Color: 318

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 13058 Color: 514
Size: 2696 Color: 313

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 558
Size: 1976 Color: 270

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13858 Color: 566
Size: 1896 Color: 263

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13903 Color: 572
Size: 1851 Color: 260

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 587
Size: 1688 Color: 245

Bin 106: 7 of cap free
Amount of items: 3
Items: 
Size: 11441 Color: 463
Size: 4152 Color: 356
Size: 160 Color: 7

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 13521 Color: 540
Size: 2232 Color: 290

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 559
Size: 1965 Color: 269

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 14015 Color: 582
Size: 1738 Color: 250

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 13412 Color: 535
Size: 2340 Color: 296

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 545
Size: 2176 Color: 284

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13885 Color: 570
Size: 1867 Color: 262

Bin 113: 10 of cap free
Amount of items: 3
Items: 
Size: 10285 Color: 443
Size: 5177 Color: 376
Size: 288 Color: 33

Bin 114: 10 of cap free
Amount of items: 3
Items: 
Size: 11641 Color: 467
Size: 3981 Color: 350
Size: 128 Color: 4

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 12220 Color: 485
Size: 3530 Color: 339

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 12570 Color: 494
Size: 3180 Color: 329

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 13627 Color: 548
Size: 2123 Color: 281

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 13678 Color: 552
Size: 2072 Color: 274

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 564
Size: 1934 Color: 266

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 14034 Color: 586
Size: 1716 Color: 249

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 12333 Color: 488
Size: 3416 Color: 337

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 13970 Color: 579
Size: 1779 Color: 255

Bin 123: 12 of cap free
Amount of items: 7
Items: 
Size: 7883 Color: 407
Size: 1522 Color: 227
Size: 1516 Color: 226
Size: 1507 Color: 225
Size: 1482 Color: 221
Size: 1394 Color: 213
Size: 444 Color: 100

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 9852 Color: 434
Size: 5896 Color: 390

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 436
Size: 5486 Color: 379
Size: 308 Color: 46

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 456
Size: 3020 Color: 323
Size: 1484 Color: 222

Bin 127: 13 of cap free
Amount of items: 2
Items: 
Size: 12453 Color: 489
Size: 3294 Color: 334

Bin 128: 13 of cap free
Amount of items: 2
Items: 
Size: 13242 Color: 525
Size: 2505 Color: 305

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 13591 Color: 546
Size: 2156 Color: 282

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 551
Size: 2085 Color: 276

Bin 131: 14 of cap free
Amount of items: 7
Items: 
Size: 7884 Color: 408
Size: 2556 Color: 306
Size: 2291 Color: 292
Size: 1631 Color: 238
Size: 528 Color: 124
Size: 432 Color: 96
Size: 424 Color: 94

Bin 132: 14 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 438
Size: 5338 Color: 377
Size: 304 Color: 43

Bin 133: 14 of cap free
Amount of items: 3
Items: 
Size: 10628 Color: 447
Size: 4842 Color: 369
Size: 276 Color: 25

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 13410 Color: 534
Size: 2336 Color: 295

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 571
Size: 1850 Color: 259

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 13381 Color: 530
Size: 2364 Color: 297

Bin 137: 16 of cap free
Amount of items: 5
Items: 
Size: 7924 Color: 413
Size: 6548 Color: 395
Size: 500 Color: 119
Size: 388 Color: 81
Size: 384 Color: 80

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 11420 Color: 460
Size: 4324 Color: 360

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 550
Size: 2084 Color: 275

Bin 140: 17 of cap free
Amount of items: 3
Items: 
Size: 8871 Color: 419
Size: 6504 Color: 392
Size: 368 Color: 71

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 13283 Color: 526
Size: 2460 Color: 303

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 539
Size: 2227 Color: 289

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 10693 Color: 449
Size: 4775 Color: 367
Size: 272 Color: 21

Bin 144: 20 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 455
Size: 4284 Color: 358
Size: 260 Color: 12

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 12755 Color: 499
Size: 2984 Color: 321

Bin 146: 21 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 536
Size: 2297 Color: 294

Bin 147: 22 of cap free
Amount of items: 2
Items: 
Size: 12881 Color: 506
Size: 2857 Color: 316

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 519
Size: 2598 Color: 308

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 13180 Color: 521
Size: 2558 Color: 307

Bin 150: 25 of cap free
Amount of items: 2
Items: 
Size: 12585 Color: 496
Size: 3150 Color: 328

Bin 151: 29 of cap free
Amount of items: 3
Items: 
Size: 11709 Color: 469
Size: 3896 Color: 349
Size: 126 Color: 2

Bin 152: 32 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 457
Size: 2757 Color: 315
Size: 1711 Color: 248

Bin 153: 32 of cap free
Amount of items: 2
Items: 
Size: 12284 Color: 487
Size: 3444 Color: 338

Bin 154: 32 of cap free
Amount of items: 2
Items: 
Size: 12700 Color: 498
Size: 3028 Color: 326

Bin 155: 35 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 418
Size: 6565 Color: 399
Size: 368 Color: 72

Bin 156: 35 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 428
Size: 5816 Color: 388
Size: 328 Color: 56

Bin 157: 35 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 473
Size: 3733 Color: 344
Size: 80 Color: 1

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 11982 Color: 475
Size: 3740 Color: 346

Bin 159: 40 of cap free
Amount of items: 3
Items: 
Size: 11281 Color: 459
Size: 4223 Color: 357
Size: 216 Color: 11

Bin 160: 40 of cap free
Amount of items: 3
Items: 
Size: 11933 Color: 474
Size: 3733 Color: 345
Size: 54 Color: 0

Bin 161: 41 of cap free
Amount of items: 2
Items: 
Size: 8980 Color: 422
Size: 6739 Color: 402

Bin 162: 43 of cap free
Amount of items: 3
Items: 
Size: 9664 Color: 431
Size: 5741 Color: 385
Size: 312 Color: 49

Bin 163: 44 of cap free
Amount of items: 3
Items: 
Size: 10870 Color: 451
Size: 4582 Color: 362
Size: 264 Color: 17

Bin 164: 47 of cap free
Amount of items: 2
Items: 
Size: 9826 Color: 433
Size: 5887 Color: 389

Bin 165: 47 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 446
Size: 4837 Color: 368
Size: 280 Color: 28

Bin 166: 48 of cap free
Amount of items: 2
Items: 
Size: 12134 Color: 479
Size: 3578 Color: 340

Bin 167: 54 of cap free
Amount of items: 3
Items: 
Size: 10472 Color: 444
Size: 4946 Color: 372
Size: 288 Color: 32

Bin 168: 54 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 466
Size: 4078 Color: 355

Bin 169: 55 of cap free
Amount of items: 3
Items: 
Size: 10266 Color: 442
Size: 5151 Color: 375
Size: 288 Color: 34

Bin 170: 58 of cap free
Amount of items: 2
Items: 
Size: 11898 Color: 472
Size: 3804 Color: 347

Bin 171: 60 of cap free
Amount of items: 2
Items: 
Size: 7892 Color: 410
Size: 7808 Color: 404

Bin 172: 70 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 450
Size: 4634 Color: 364
Size: 264 Color: 18

Bin 173: 73 of cap free
Amount of items: 3
Items: 
Size: 10661 Color: 448
Size: 4754 Color: 366
Size: 272 Color: 22

Bin 174: 75 of cap free
Amount of items: 3
Items: 
Size: 9549 Color: 427
Size: 5808 Color: 386
Size: 328 Color: 57

Bin 175: 84 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 468
Size: 3876 Color: 348
Size: 128 Color: 3

Bin 176: 94 of cap free
Amount of items: 23
Items: 
Size: 856 Color: 163
Size: 844 Color: 161
Size: 816 Color: 160
Size: 812 Color: 159
Size: 808 Color: 158
Size: 796 Color: 157
Size: 768 Color: 156
Size: 768 Color: 155
Size: 760 Color: 154
Size: 744 Color: 152
Size: 720 Color: 151
Size: 720 Color: 150
Size: 700 Color: 147
Size: 592 Color: 133
Size: 584 Color: 132
Size: 584 Color: 131
Size: 576 Color: 130
Size: 576 Color: 129
Size: 570 Color: 128
Size: 528 Color: 123
Size: 528 Color: 122
Size: 528 Color: 121
Size: 488 Color: 116

Bin 177: 94 of cap free
Amount of items: 4
Items: 
Size: 10146 Color: 439
Size: 4924 Color: 371
Size: 300 Color: 41
Size: 296 Color: 38

Bin 178: 96 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 454
Size: 4308 Color: 359
Size: 260 Color: 13

Bin 179: 98 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 430
Size: 5686 Color: 384
Size: 320 Color: 51

Bin 180: 101 of cap free
Amount of items: 3
Items: 
Size: 10259 Color: 441
Size: 5104 Color: 374
Size: 296 Color: 36

Bin 181: 107 of cap free
Amount of items: 3
Items: 
Size: 10983 Color: 453
Size: 4408 Color: 361
Size: 262 Color: 14

Bin 182: 118 of cap free
Amount of items: 3
Items: 
Size: 11470 Color: 464
Size: 4028 Color: 353
Size: 144 Color: 6

Bin 183: 120 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 435
Size: 5464 Color: 378
Size: 308 Color: 47

Bin 184: 130 of cap free
Amount of items: 4
Items: 
Size: 7960 Color: 415
Size: 6928 Color: 403
Size: 372 Color: 76
Size: 370 Color: 75

Bin 185: 131 of cap free
Amount of items: 3
Items: 
Size: 8697 Color: 417
Size: 6564 Color: 398
Size: 368 Color: 73

Bin 186: 138 of cap free
Amount of items: 18
Items: 
Size: 1200 Color: 191
Size: 1152 Color: 189
Size: 1148 Color: 188
Size: 1120 Color: 182
Size: 1088 Color: 181
Size: 1008 Color: 178
Size: 988 Color: 176
Size: 976 Color: 174
Size: 976 Color: 173
Size: 948 Color: 171
Size: 928 Color: 170
Size: 880 Color: 166
Size: 856 Color: 164
Size: 480 Color: 113
Size: 474 Color: 111
Size: 472 Color: 109
Size: 464 Color: 108
Size: 464 Color: 107

Bin 187: 138 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 462
Size: 4028 Color: 352
Size: 162 Color: 8

Bin 188: 141 of cap free
Amount of items: 8
Items: 
Size: 7882 Color: 406
Size: 1442 Color: 216
Size: 1375 Color: 210
Size: 1348 Color: 207
Size: 1346 Color: 206
Size: 1326 Color: 204
Size: 452 Color: 102
Size: 448 Color: 101

Bin 189: 157 of cap free
Amount of items: 3
Items: 
Size: 11426 Color: 461
Size: 3993 Color: 351
Size: 184 Color: 10

Bin 190: 161 of cap free
Amount of items: 5
Items: 
Size: 7886 Color: 409
Size: 2900 Color: 317
Size: 2647 Color: 309
Size: 1750 Color: 251
Size: 416 Color: 89

Bin 191: 164 of cap free
Amount of items: 3
Items: 
Size: 10580 Color: 445
Size: 4728 Color: 365
Size: 288 Color: 31

Bin 192: 166 of cap free
Amount of items: 3
Items: 
Size: 10202 Color: 440
Size: 5096 Color: 373
Size: 296 Color: 37

Bin 193: 172 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 425
Size: 6040 Color: 391
Size: 340 Color: 63

Bin 194: 178 of cap free
Amount of items: 4
Items: 
Size: 10058 Color: 437
Size: 4916 Color: 370
Size: 304 Color: 45
Size: 304 Color: 44

Bin 195: 250 of cap free
Amount of items: 2
Items: 
Size: 8942 Color: 421
Size: 6568 Color: 401

Bin 196: 256 of cap free
Amount of items: 2
Items: 
Size: 8938 Color: 420
Size: 6566 Color: 400

Bin 197: 308 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 416
Size: 6564 Color: 397
Size: 368 Color: 74

Bin 198: 455 of cap free
Amount of items: 8
Items: 
Size: 7881 Color: 405
Size: 1312 Color: 201
Size: 1304 Color: 198
Size: 1304 Color: 197
Size: 1296 Color: 194
Size: 1296 Color: 193
Size: 458 Color: 105
Size: 454 Color: 103

Bin 199: 10034 of cap free
Amount of items: 9
Items: 
Size: 674 Color: 145
Size: 672 Color: 144
Size: 656 Color: 143
Size: 640 Color: 142
Size: 640 Color: 141
Size: 636 Color: 140
Size: 604 Color: 137
Size: 604 Color: 136
Size: 600 Color: 134

Total size: 3120480
Total free space: 15760

