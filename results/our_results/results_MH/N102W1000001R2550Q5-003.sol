Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 413160 Color: 2
Size: 322261 Color: 2
Size: 264580 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 404793 Color: 1
Size: 327529 Color: 3
Size: 267679 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 416500 Color: 1
Size: 284988 Color: 2
Size: 298513 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 456795 Color: 3
Size: 259298 Color: 3
Size: 283908 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 469386 Color: 1
Size: 267740 Color: 3
Size: 262875 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 358298 Color: 0
Size: 357139 Color: 2
Size: 284564 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 291707 Color: 1
Size: 280245 Color: 3
Size: 428049 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 361855 Color: 4
Size: 380870 Color: 2
Size: 257276 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 477298 Color: 4
Size: 261856 Color: 3
Size: 260847 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 424844 Color: 4
Size: 306298 Color: 4
Size: 268859 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 400963 Color: 1
Size: 347830 Color: 1
Size: 251208 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 298679 Color: 4
Size: 307893 Color: 1
Size: 393429 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 456662 Color: 1
Size: 269246 Color: 3
Size: 274093 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 465251 Color: 0
Size: 274146 Color: 1
Size: 260604 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 424719 Color: 0
Size: 273899 Color: 4
Size: 301383 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 433059 Color: 4
Size: 275695 Color: 0
Size: 291247 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 404675 Color: 1
Size: 314117 Color: 2
Size: 281209 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 362449 Color: 0
Size: 360778 Color: 3
Size: 276774 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 359154 Color: 0
Size: 308328 Color: 1
Size: 332519 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 313069 Color: 1
Size: 303729 Color: 1
Size: 383203 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 344473 Color: 2
Size: 272934 Color: 0
Size: 382594 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 344530 Color: 4
Size: 334013 Color: 0
Size: 321458 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 452348 Color: 3
Size: 268740 Color: 0
Size: 278913 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 477379 Color: 4
Size: 270895 Color: 2
Size: 251727 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 395966 Color: 4
Size: 353504 Color: 0
Size: 250531 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 400681 Color: 4
Size: 315517 Color: 4
Size: 283803 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 342064 Color: 0
Size: 270588 Color: 3
Size: 387349 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 368080 Color: 0
Size: 333927 Color: 2
Size: 297994 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 357603 Color: 1
Size: 379223 Color: 3
Size: 263175 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 340315 Color: 1
Size: 360046 Color: 3
Size: 299640 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 406520 Color: 4
Size: 304324 Color: 4
Size: 289157 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 451224 Color: 3
Size: 297284 Color: 1
Size: 251493 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 481740 Color: 2
Size: 265223 Color: 3
Size: 253038 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 496053 Color: 3
Size: 252587 Color: 1
Size: 251361 Color: 4

Total size: 34000034
Total free space: 0

