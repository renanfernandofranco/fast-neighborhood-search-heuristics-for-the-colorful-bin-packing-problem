Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 343 Color: 3
Size: 278 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 4
Size: 257 Color: 2
Size: 250 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 355 Color: 1
Size: 260 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 353 Color: 4
Size: 281 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 311 Color: 2
Size: 254 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 319 Color: 0
Size: 315 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 264 Color: 2
Size: 352 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 326 Color: 4
Size: 296 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 4
Size: 336 Color: 0
Size: 297 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 264 Color: 1
Size: 259 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 301 Color: 0
Size: 282 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 312 Color: 4
Size: 261 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 358 Color: 3
Size: 280 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 289 Color: 4
Size: 276 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 274 Color: 3
Size: 256 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 308 Color: 2
Size: 275 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 350 Color: 1
Size: 288 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 275 Color: 2
Size: 269 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 342 Color: 1
Size: 260 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 317 Color: 3
Size: 281 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 4
Size: 349 Color: 4
Size: 296 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 349 Color: 1
Size: 292 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 260 Color: 3
Size: 250 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 300 Color: 2
Size: 276 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 253 Color: 4
Size: 253 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 3
Size: 261 Color: 1
Size: 250 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 2
Size: 268 Color: 0
Size: 252 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 327 Color: 0
Size: 261 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 366 Color: 1
Size: 268 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 372 Color: 2
Size: 254 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 314 Color: 4
Size: 288 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 4
Size: 352 Color: 2
Size: 262 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 352 Color: 0
Size: 271 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 277 Color: 2
Size: 252 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 352 Color: 0
Size: 268 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 339 Color: 1
Size: 254 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 352 Color: 2
Size: 263 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 313 Color: 1
Size: 261 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 252 Color: 4
Size: 250 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 273 Color: 2
Size: 259 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 332 Color: 2
Size: 296 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 302 Color: 0
Size: 256 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 303 Color: 0
Size: 297 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 354 Color: 2
Size: 260 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 340 Color: 4
Size: 295 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 357 Color: 3
Size: 262 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 309 Color: 0
Size: 255 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 4
Size: 330 Color: 3
Size: 323 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 258 Color: 4
Size: 250 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 4
Size: 316 Color: 3
Size: 309 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 357 Color: 3
Size: 258 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 368 Color: 4
Size: 256 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 328 Color: 0
Size: 273 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 268 Color: 4
Size: 250 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 285 Color: 1
Size: 251 Color: 4
Size: 464 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 324 Color: 3
Size: 274 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 291 Color: 3
Size: 269 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 296 Color: 4
Size: 268 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 4
Size: 268 Color: 3
Size: 260 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 324 Color: 3
Size: 261 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 277 Color: 0
Size: 263 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 361 Color: 0
Size: 274 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 275 Color: 2
Size: 260 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 341 Color: 1
Size: 257 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 295 Color: 2
Size: 290 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 260 Color: 1
Size: 256 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 283 Color: 2
Size: 263 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 309 Color: 1
Size: 256 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 4
Size: 333 Color: 2
Size: 309 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 287 Color: 2
Size: 286 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 325 Color: 2
Size: 285 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 359 Color: 2
Size: 263 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 326 Color: 1
Size: 258 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 257 Color: 1
Size: 251 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 2
Size: 290 Color: 0
Size: 279 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 305 Color: 2
Size: 301 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 4
Size: 291 Color: 4
Size: 266 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 2
Size: 331 Color: 2
Size: 306 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 335 Color: 4
Size: 288 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 282 Color: 4
Size: 277 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 282 Color: 2
Size: 258 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 275 Color: 2
Size: 263 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 258 Color: 2
Size: 255 Color: 3

Total size: 83000
Total free space: 0

