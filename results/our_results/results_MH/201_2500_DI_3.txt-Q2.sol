Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 19
Items: 
Size: 204 Color: 1
Size: 204 Color: 1
Size: 200 Color: 0
Size: 150 Color: 1
Size: 144 Color: 0
Size: 142 Color: 1
Size: 142 Color: 0
Size: 140 Color: 0
Size: 128 Color: 1
Size: 122 Color: 0
Size: 112 Color: 0
Size: 112 Color: 0
Size: 108 Color: 1
Size: 104 Color: 0
Size: 104 Color: 0
Size: 88 Color: 1
Size: 88 Color: 1
Size: 88 Color: 1
Size: 76 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 1142 Color: 1
Size: 76 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 678 Color: 1
Size: 132 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1836 Color: 0
Size: 548 Color: 0
Size: 72 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 0
Size: 474 Color: 1
Size: 92 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 2034 Color: 0
Size: 354 Color: 1
Size: 36 Color: 1
Size: 32 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 0
Size: 262 Color: 1
Size: 88 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 0
Size: 220 Color: 1
Size: 112 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 1
Size: 174 Color: 0
Size: 96 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 184 Color: 0
Size: 80 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 0
Size: 172 Color: 1
Size: 104 Color: 0

Bin 12: 1 of cap free
Amount of items: 4
Items: 
Size: 1334 Color: 1
Size: 1021 Color: 0
Size: 52 Color: 1
Size: 48 Color: 0

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 0
Size: 873 Color: 0
Size: 68 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 0
Size: 840 Color: 1
Size: 44 Color: 1

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 1611 Color: 1
Size: 844 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 0
Size: 387 Color: 0
Size: 314 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 0
Size: 574 Color: 0
Size: 60 Color: 1

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1887 Color: 0
Size: 568 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 384 Color: 0
Size: 174 Color: 1

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1924 Color: 0
Size: 531 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 1
Size: 436 Color: 1
Size: 84 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 467 Color: 0
Size: 48 Color: 0

Bin 23: 2 of cap free
Amount of items: 6
Items: 
Size: 1229 Color: 1
Size: 349 Color: 0
Size: 280 Color: 0
Size: 226 Color: 1
Size: 214 Color: 1
Size: 156 Color: 0

Bin 24: 2 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 0
Size: 524 Color: 0
Size: 292 Color: 0
Size: 232 Color: 1
Size: 176 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 1
Size: 1022 Color: 0
Size: 68 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 0
Size: 884 Color: 0
Size: 120 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 1
Size: 938 Color: 0
Size: 48 Color: 1

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1804 Color: 1
Size: 650 Color: 0

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 1
Size: 607 Color: 0
Size: 40 Color: 1

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 164 Color: 0
Size: 144 Color: 0

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 0
Size: 284 Color: 1
Size: 4 Color: 1

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 0
Size: 242 Color: 1
Size: 16 Color: 1

Bin 33: 2 of cap free
Amount of items: 4
Items: 
Size: 2202 Color: 1
Size: 204 Color: 0
Size: 40 Color: 0
Size: 8 Color: 1

Bin 34: 3 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 1
Size: 705 Color: 1
Size: 356 Color: 0
Size: 92 Color: 1
Size: 64 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 877 Color: 0
Size: 40 Color: 1

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 0
Size: 724 Color: 1

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 2028 Color: 0
Size: 425 Color: 1

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1922 Color: 0
Size: 530 Color: 1

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 0
Size: 478 Color: 1

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 1
Size: 863 Color: 0

Bin 41: 6 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 1
Size: 650 Color: 0
Size: 52 Color: 1

Bin 42: 7 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 0
Size: 976 Color: 1
Size: 64 Color: 1

Bin 43: 7 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 0
Size: 751 Color: 0
Size: 128 Color: 1

Bin 44: 7 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 0
Size: 715 Color: 1
Size: 56 Color: 0

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 2003 Color: 1
Size: 446 Color: 0

Bin 46: 9 of cap free
Amount of items: 4
Items: 
Size: 1231 Color: 0
Size: 541 Color: 0
Size: 439 Color: 1
Size: 236 Color: 1

Bin 47: 10 of cap free
Amount of items: 2
Items: 
Size: 1660 Color: 0
Size: 786 Color: 1

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 1947 Color: 1
Size: 497 Color: 0

Bin 49: 13 of cap free
Amount of items: 2
Items: 
Size: 1822 Color: 1
Size: 621 Color: 0

Bin 50: 13 of cap free
Amount of items: 2
Items: 
Size: 1931 Color: 0
Size: 512 Color: 1

Bin 51: 15 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 0
Size: 1020 Color: 1

Bin 52: 15 of cap free
Amount of items: 2
Items: 
Size: 1599 Color: 1
Size: 842 Color: 0

Bin 53: 18 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 0
Size: 742 Color: 1

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1770 Color: 1
Size: 668 Color: 0

Bin 55: 18 of cap free
Amount of items: 2
Items: 
Size: 2074 Color: 0
Size: 364 Color: 1

Bin 56: 18 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 1
Size: 322 Color: 0
Size: 8 Color: 1

Bin 57: 23 of cap free
Amount of items: 2
Items: 
Size: 2039 Color: 0
Size: 394 Color: 1

Bin 58: 24 of cap free
Amount of items: 2
Items: 
Size: 1713 Color: 1
Size: 719 Color: 0

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1986 Color: 0
Size: 444 Color: 1

Bin 60: 28 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 1
Size: 1023 Color: 0

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 1
Size: 833 Color: 0

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1993 Color: 1
Size: 435 Color: 0

Bin 63: 31 of cap free
Amount of items: 2
Items: 
Size: 2036 Color: 1
Size: 389 Color: 0

Bin 64: 33 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 596 Color: 0
Size: 272 Color: 1

Bin 65: 40 of cap free
Amount of items: 2
Items: 
Size: 1500 Color: 0
Size: 916 Color: 1

Bin 66: 1944 of cap free
Amount of items: 7
Items: 
Size: 104 Color: 0
Size: 86 Color: 1
Size: 86 Color: 1
Size: 76 Color: 1
Size: 64 Color: 0
Size: 48 Color: 0
Size: 48 Color: 0

Total size: 159640
Total free space: 2456

