Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 302 Color: 0
Size: 296 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 367 Color: 0
Size: 261 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 365 Color: 0
Size: 254 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 290 Color: 1
Size: 268 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 352 Color: 1
Size: 291 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 300 Color: 0
Size: 269 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 0
Size: 311 Color: 0
Size: 282 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 268 Color: 1
Size: 255 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 349 Color: 0
Size: 285 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 0
Size: 347 Color: 0
Size: 301 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 258 Color: 0
Size: 255 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 349 Color: 0
Size: 261 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 0
Size: 296 Color: 1
Size: 277 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 276 Color: 0
Size: 268 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 0
Size: 250 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 260 Color: 0
Size: 260 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 309 Color: 0
Size: 274 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 308 Color: 0
Size: 256 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 282 Color: 0
Size: 258 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 325 Color: 0
Size: 277 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 340 Color: 0
Size: 283 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 324 Color: 1
Size: 315 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 326 Color: 0
Size: 312 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 258 Color: 1
Size: 253 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 309 Color: 1
Size: 291 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 324 Color: 1
Size: 274 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 297 Color: 0
Size: 268 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 295 Color: 0
Size: 290 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 350 Color: 0
Size: 252 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 266 Color: 0
Size: 250 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 352 Color: 1
Size: 263 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 328 Color: 1
Size: 260 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 359 Color: 0
Size: 256 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 358 Color: 0
Size: 263 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 354 Color: 1
Size: 289 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 319 Color: 0
Size: 301 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 355 Color: 0
Size: 261 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 259 Color: 0
Size: 251 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 313 Color: 1
Size: 288 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 296 Color: 0
Size: 264 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 295 Color: 0
Size: 251 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 359 Color: 0
Size: 263 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 363 Color: 1
Size: 259 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 0
Size: 250 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 303 Color: 1
Size: 262 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 273 Color: 0
Size: 257 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 285 Color: 0
Size: 279 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 260 Color: 0
Size: 258 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 309 Color: 0
Size: 297 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 0
Size: 331 Color: 1
Size: 330 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 309 Color: 0
Size: 306 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 352 Color: 1
Size: 280 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 353 Color: 1
Size: 281 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 352 Color: 1
Size: 262 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 332 Color: 0
Size: 252 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 323 Color: 0
Size: 305 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 281 Color: 1
Size: 276 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 335 Color: 0
Size: 250 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 275 Color: 0
Size: 257 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 336 Color: 0
Size: 288 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 254 Color: 1
Size: 254 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 275 Color: 1
Size: 253 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 343 Color: 0
Size: 282 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 366 Color: 0
Size: 260 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 316 Color: 0
Size: 260 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 327 Color: 0
Size: 271 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 314 Color: 0
Size: 269 Color: 1
Size: 417 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 1
Size: 326 Color: 0
Size: 333 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 362 Color: 1
Size: 273 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 256 Color: 1
Size: 250 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 296 Color: 1
Size: 263 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 358 Color: 0
Size: 256 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 288 Color: 0
Size: 286 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 355 Color: 1
Size: 268 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 258 Color: 0
Size: 250 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 277 Color: 1
Size: 263 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 317 Color: 0
Size: 256 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 268 Color: 1
Size: 261 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 342 Color: 0
Size: 292 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 287 Color: 0
Size: 278 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 274 Color: 1
Size: 264 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 275 Color: 0
Size: 261 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 275 Color: 1
Size: 260 Color: 0

Total size: 83000
Total free space: 0

