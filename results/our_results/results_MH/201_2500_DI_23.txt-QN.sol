Capicity Bin: 2028
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1017 Color: 139
Size: 303 Color: 91
Size: 282 Color: 88
Size: 234 Color: 76
Size: 104 Color: 51
Size: 44 Color: 12
Size: 44 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 151
Size: 462 Color: 112
Size: 236 Color: 77

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 165
Size: 409 Color: 105
Size: 98 Color: 49

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 166
Size: 418 Color: 107
Size: 80 Color: 41

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 172
Size: 329 Color: 96
Size: 110 Color: 55

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 176
Size: 321 Color: 94
Size: 76 Color: 39

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 181
Size: 302 Color: 90
Size: 56 Color: 23

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 182
Size: 281 Color: 87
Size: 72 Color: 35

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 186
Size: 194 Color: 71
Size: 120 Color: 58

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1715 Color: 187
Size: 261 Color: 84
Size: 52 Color: 21

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 188
Size: 261 Color: 85
Size: 50 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1735 Color: 190
Size: 245 Color: 81
Size: 48 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 191
Size: 243 Color: 79
Size: 48 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 192
Size: 242 Color: 78
Size: 48 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 193
Size: 186 Color: 69
Size: 94 Color: 48

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 194
Size: 220 Color: 72
Size: 58 Color: 25

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 195
Size: 245 Color: 80
Size: 32 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 196
Size: 257 Color: 83
Size: 16 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 197
Size: 227 Color: 74
Size: 44 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 201
Size: 130 Color: 62
Size: 92 Color: 47

Bin 21: 1 of cap free
Amount of items: 5
Items: 
Size: 1050 Color: 141
Size: 461 Color: 111
Size: 420 Color: 108
Size: 52 Color: 22
Size: 44 Color: 9

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 146
Size: 752 Color: 131
Size: 36 Color: 7

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 157
Size: 475 Color: 114
Size: 152 Color: 65

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1454 Color: 160
Size: 573 Color: 122

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 171
Size: 250 Color: 82
Size: 190 Color: 70

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 179
Size: 369 Color: 102
Size: 4 Color: 0

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1665 Color: 180
Size: 362 Color: 100

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 150
Size: 714 Color: 129
Size: 36 Color: 5

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 167
Size: 491 Color: 116

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1554 Color: 169
Size: 472 Color: 113

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1691 Color: 184
Size: 335 Color: 98

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1764 Color: 198
Size: 262 Color: 86

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 199
Size: 228 Color: 75

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 200
Size: 224 Color: 73

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1243 Color: 147
Size: 782 Color: 133

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1258 Color: 149
Size: 731 Color: 130
Size: 36 Color: 6

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1341 Color: 152
Size: 684 Color: 128

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1614 Color: 174
Size: 411 Color: 106

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1627 Color: 175
Size: 398 Color: 104

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 177
Size: 382 Color: 103

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1679 Color: 183
Size: 346 Color: 99

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 1694 Color: 185
Size: 331 Color: 97

Bin 43: 3 of cap free
Amount of items: 2
Items: 
Size: 1730 Color: 189
Size: 295 Color: 89

Bin 44: 6 of cap free
Amount of items: 5
Items: 
Size: 1018 Color: 140
Size: 326 Color: 95
Size: 318 Color: 93
Size: 316 Color: 92
Size: 44 Color: 10

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1377 Color: 156
Size: 645 Color: 125

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1475 Color: 163
Size: 546 Color: 119

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1539 Color: 168
Size: 482 Color: 115

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 1361 Color: 154
Size: 659 Color: 127

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 161
Size: 561 Color: 121

Bin 50: 8 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 170
Size: 446 Color: 110

Bin 51: 9 of cap free
Amount of items: 2
Items: 
Size: 1174 Color: 145
Size: 845 Color: 136

Bin 52: 11 of cap free
Amount of items: 2
Items: 
Size: 1474 Color: 162
Size: 543 Color: 118

Bin 53: 11 of cap free
Amount of items: 2
Items: 
Size: 1594 Color: 173
Size: 423 Color: 109

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 178
Size: 367 Color: 101

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1374 Color: 155
Size: 642 Color: 124

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 159
Size: 557 Color: 120
Size: 20 Color: 2

Bin 57: 13 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 144
Size: 818 Color: 134
Size: 44 Color: 8

Bin 58: 16 of cap free
Amount of items: 2
Items: 
Size: 1255 Color: 148
Size: 757 Color: 132

Bin 59: 16 of cap free
Amount of items: 2
Items: 
Size: 1357 Color: 153
Size: 655 Color: 126

Bin 60: 16 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 158
Size: 582 Color: 123
Size: 20 Color: 3

Bin 61: 16 of cap free
Amount of items: 2
Items: 
Size: 1494 Color: 164
Size: 518 Color: 117

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1094 Color: 142
Size: 906 Color: 137

Bin 63: 29 of cap free
Amount of items: 10
Items: 
Size: 1015 Color: 138
Size: 168 Color: 68
Size: 168 Color: 67
Size: 160 Color: 66
Size: 144 Color: 64
Size: 140 Color: 63
Size: 56 Color: 24
Size: 52 Color: 20
Size: 48 Color: 18
Size: 48 Color: 17

Bin 64: 34 of cap free
Amount of items: 2
Items: 
Size: 1151 Color: 143
Size: 843 Color: 135

Bin 65: 62 of cap free
Amount of items: 22
Items: 
Size: 130 Color: 61
Size: 128 Color: 60
Size: 128 Color: 59
Size: 116 Color: 57
Size: 114 Color: 56
Size: 110 Color: 54
Size: 108 Color: 53
Size: 108 Color: 52
Size: 100 Color: 50
Size: 92 Color: 46
Size: 92 Color: 45
Size: 88 Color: 44
Size: 72 Color: 36
Size: 72 Color: 34
Size: 68 Color: 33
Size: 66 Color: 32
Size: 66 Color: 31
Size: 64 Color: 30
Size: 64 Color: 29
Size: 60 Color: 28
Size: 60 Color: 27
Size: 60 Color: 26

Bin 66: 1634 of cap free
Amount of items: 5
Items: 
Size: 84 Color: 43
Size: 82 Color: 42
Size: 80 Color: 40
Size: 76 Color: 38
Size: 72 Color: 37

Total size: 131820
Total free space: 2028

