Capicity Bin: 1000001
Lower Bound: 905

Bins used: 905
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 750214 Color: 12
Size: 139694 Color: 3
Size: 110093 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 773053 Color: 4
Size: 125150 Color: 15
Size: 101798 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 515331 Color: 5
Size: 263088 Color: 13
Size: 221582 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 635955 Color: 15
Size: 238348 Color: 5
Size: 125698 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 735767 Color: 13
Size: 157204 Color: 4
Size: 107030 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 536367 Color: 1
Size: 316124 Color: 11
Size: 147510 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 711755 Color: 5
Size: 166291 Color: 8
Size: 121955 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 636615 Color: 7
Size: 195288 Color: 0
Size: 168098 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 579077 Color: 8
Size: 240404 Color: 16
Size: 180520 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 588185 Color: 0
Size: 217358 Color: 18
Size: 194458 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 718991 Color: 17
Size: 175221 Color: 14
Size: 105789 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 429581 Color: 1
Size: 352379 Color: 13
Size: 218041 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 547608 Color: 16
Size: 283250 Color: 17
Size: 169143 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 686817 Color: 2
Size: 197318 Color: 3
Size: 115866 Color: 8

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 358703 Color: 8
Size: 316594 Color: 17
Size: 168549 Color: 11
Size: 156155 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 783485 Color: 12
Size: 109247 Color: 4
Size: 107269 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 656742 Color: 17
Size: 195840 Color: 4
Size: 147419 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 694554 Color: 1
Size: 159494 Color: 11
Size: 145953 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 711068 Color: 12
Size: 164911 Color: 11
Size: 124022 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 694813 Color: 12
Size: 165127 Color: 8
Size: 140061 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 530428 Color: 16
Size: 320440 Color: 1
Size: 149133 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 470734 Color: 10
Size: 403426 Color: 7
Size: 125841 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 552174 Color: 17
Size: 294894 Color: 8
Size: 152933 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 570474 Color: 13
Size: 303492 Color: 16
Size: 126035 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 730206 Color: 12
Size: 164640 Color: 1
Size: 105155 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 538156 Color: 4
Size: 332106 Color: 6
Size: 129739 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 694143 Color: 13
Size: 170941 Color: 11
Size: 134917 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 540386 Color: 9
Size: 340011 Color: 3
Size: 119604 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 540078 Color: 9
Size: 330945 Color: 14
Size: 128978 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 582790 Color: 0
Size: 294637 Color: 16
Size: 122574 Color: 12

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 501586 Color: 2
Size: 194838 Color: 17
Size: 191264 Color: 9
Size: 112313 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 784571 Color: 9
Size: 112639 Color: 8
Size: 102791 Color: 11

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 765729 Color: 7
Size: 234272 Color: 19

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 562036 Color: 7
Size: 437965 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 535602 Color: 5
Size: 364101 Color: 11
Size: 100298 Color: 3

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 557180 Color: 0
Size: 442821 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 720039 Color: 0
Size: 148676 Color: 13
Size: 131286 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 706289 Color: 5
Size: 149753 Color: 12
Size: 143958 Color: 2

Bin 39: 1 of cap free
Amount of items: 4
Items: 
Size: 547204 Color: 7
Size: 154081 Color: 11
Size: 149545 Color: 7
Size: 149170 Color: 17

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 550633 Color: 19
Size: 257802 Color: 9
Size: 191565 Color: 11

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 698308 Color: 12
Size: 156712 Color: 15
Size: 144980 Color: 7

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 718638 Color: 14
Size: 170669 Color: 18
Size: 110693 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 772379 Color: 13
Size: 116589 Color: 19
Size: 111032 Color: 19

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 549740 Color: 6
Size: 232876 Color: 11
Size: 217384 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 548642 Color: 16
Size: 276643 Color: 16
Size: 174715 Color: 2

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 546657 Color: 13
Size: 267464 Color: 5
Size: 185879 Color: 19

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 571337 Color: 5
Size: 298005 Color: 16
Size: 130658 Color: 12

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 553300 Color: 12
Size: 267433 Color: 4
Size: 179267 Color: 14

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 706409 Color: 16
Size: 166373 Color: 17
Size: 127218 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 560531 Color: 12
Size: 325780 Color: 16
Size: 113689 Color: 5

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 508437 Color: 15
Size: 347091 Color: 9
Size: 144471 Color: 18

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 768667 Color: 1
Size: 118139 Color: 5
Size: 113193 Color: 2

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 583611 Color: 4
Size: 239479 Color: 8
Size: 176909 Color: 18

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 535775 Color: 10
Size: 327805 Color: 1
Size: 136419 Color: 4

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 704536 Color: 18
Size: 165650 Color: 18
Size: 129813 Color: 12

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 401021 Color: 14
Size: 309808 Color: 15
Size: 289170 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 583385 Color: 13
Size: 219016 Color: 8
Size: 197598 Color: 5

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 636684 Color: 18
Size: 192556 Color: 8
Size: 170759 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 624974 Color: 15
Size: 191476 Color: 19
Size: 183549 Color: 9

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 542130 Color: 7
Size: 308364 Color: 18
Size: 149505 Color: 4

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 418679 Color: 16
Size: 386003 Color: 5
Size: 195317 Color: 17

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 612727 Color: 15
Size: 387272 Color: 5

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 578919 Color: 6
Size: 421080 Color: 2

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 682884 Color: 9
Size: 317115 Color: 13

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 725019 Color: 8
Size: 274980 Color: 11

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 744926 Color: 5
Size: 255073 Color: 12

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 750422 Color: 18
Size: 249577 Color: 1

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 585384 Color: 11
Size: 219348 Color: 16
Size: 195266 Color: 11

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 744069 Color: 13
Size: 146907 Color: 3
Size: 109022 Color: 15

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 712793 Color: 17
Size: 156717 Color: 19
Size: 130488 Color: 1

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 601549 Color: 13
Size: 242359 Color: 9
Size: 156090 Color: 5

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 786665 Color: 7
Size: 110200 Color: 7
Size: 103133 Color: 2

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 706067 Color: 11
Size: 163962 Color: 7
Size: 129969 Color: 17

Bin 74: 3 of cap free
Amount of items: 2
Items: 
Size: 706769 Color: 2
Size: 293229 Color: 16

Bin 75: 3 of cap free
Amount of items: 2
Items: 
Size: 708617 Color: 15
Size: 291381 Color: 2

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 651191 Color: 12
Size: 187634 Color: 17
Size: 161172 Color: 12

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 711218 Color: 6
Size: 148566 Color: 18
Size: 140213 Color: 16

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 708590 Color: 13
Size: 183107 Color: 0
Size: 108300 Color: 19

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 364060 Color: 10
Size: 319250 Color: 11
Size: 316687 Color: 13

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 512895 Color: 9
Size: 381602 Color: 10
Size: 105500 Color: 11

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 743393 Color: 13
Size: 256604 Color: 2

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 757819 Color: 3
Size: 242178 Color: 6

Bin 83: 5 of cap free
Amount of items: 3
Items: 
Size: 655133 Color: 3
Size: 174948 Color: 5
Size: 169915 Color: 7

Bin 84: 5 of cap free
Amount of items: 3
Items: 
Size: 546881 Color: 12
Size: 295244 Color: 18
Size: 157871 Color: 3

Bin 85: 5 of cap free
Amount of items: 3
Items: 
Size: 540088 Color: 18
Size: 338001 Color: 7
Size: 121907 Color: 11

Bin 86: 5 of cap free
Amount of items: 3
Items: 
Size: 353967 Color: 17
Size: 333268 Color: 6
Size: 312761 Color: 0

Bin 87: 5 of cap free
Amount of items: 2
Items: 
Size: 577981 Color: 16
Size: 422015 Color: 18

Bin 88: 5 of cap free
Amount of items: 3
Items: 
Size: 793393 Color: 9
Size: 103849 Color: 14
Size: 102754 Color: 9

Bin 89: 6 of cap free
Amount of items: 3
Items: 
Size: 638554 Color: 17
Size: 180871 Color: 14
Size: 180570 Color: 0

Bin 90: 6 of cap free
Amount of items: 3
Items: 
Size: 768523 Color: 8
Size: 118639 Color: 0
Size: 112833 Color: 11

Bin 91: 6 of cap free
Amount of items: 3
Items: 
Size: 657703 Color: 7
Size: 219360 Color: 4
Size: 122932 Color: 17

Bin 92: 6 of cap free
Amount of items: 2
Items: 
Size: 521231 Color: 9
Size: 478764 Color: 18

Bin 93: 6 of cap free
Amount of items: 2
Items: 
Size: 740404 Color: 10
Size: 259591 Color: 15

Bin 94: 7 of cap free
Amount of items: 3
Items: 
Size: 725979 Color: 9
Size: 138096 Color: 12
Size: 135919 Color: 16

Bin 95: 7 of cap free
Amount of items: 2
Items: 
Size: 761935 Color: 17
Size: 238059 Color: 15

Bin 96: 7 of cap free
Amount of items: 2
Items: 
Size: 719802 Color: 15
Size: 280192 Color: 3

Bin 97: 7 of cap free
Amount of items: 2
Items: 
Size: 549615 Color: 9
Size: 450379 Color: 13

Bin 98: 7 of cap free
Amount of items: 2
Items: 
Size: 564203 Color: 16
Size: 435791 Color: 11

Bin 99: 8 of cap free
Amount of items: 3
Items: 
Size: 748359 Color: 17
Size: 134583 Color: 12
Size: 117051 Color: 5

Bin 100: 8 of cap free
Amount of items: 3
Items: 
Size: 562542 Color: 0
Size: 218920 Color: 3
Size: 218531 Color: 19

Bin 101: 8 of cap free
Amount of items: 3
Items: 
Size: 751124 Color: 18
Size: 133959 Color: 10
Size: 114910 Color: 12

Bin 102: 8 of cap free
Amount of items: 3
Items: 
Size: 384801 Color: 19
Size: 358193 Color: 11
Size: 256999 Color: 2

Bin 103: 8 of cap free
Amount of items: 2
Items: 
Size: 612049 Color: 4
Size: 387944 Color: 7

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 617570 Color: 1
Size: 382423 Color: 12

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 731378 Color: 5
Size: 268615 Color: 6

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 797684 Color: 2
Size: 202309 Color: 19

Bin 107: 9 of cap free
Amount of items: 3
Items: 
Size: 508381 Color: 17
Size: 252328 Color: 12
Size: 239283 Color: 9

Bin 108: 9 of cap free
Amount of items: 3
Items: 
Size: 653603 Color: 4
Size: 175032 Color: 7
Size: 171357 Color: 8

Bin 109: 9 of cap free
Amount of items: 3
Items: 
Size: 694230 Color: 16
Size: 189086 Color: 5
Size: 116676 Color: 7

Bin 110: 9 of cap free
Amount of items: 3
Items: 
Size: 761824 Color: 4
Size: 135709 Color: 19
Size: 102459 Color: 18

Bin 111: 9 of cap free
Amount of items: 3
Items: 
Size: 553887 Color: 5
Size: 325031 Color: 15
Size: 121074 Color: 13

Bin 112: 10 of cap free
Amount of items: 3
Items: 
Size: 471305 Color: 0
Size: 346645 Color: 6
Size: 182041 Color: 9

Bin 113: 10 of cap free
Amount of items: 3
Items: 
Size: 649694 Color: 1
Size: 197906 Color: 15
Size: 152391 Color: 10

Bin 114: 10 of cap free
Amount of items: 3
Items: 
Size: 650549 Color: 16
Size: 176806 Color: 3
Size: 172636 Color: 19

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 429696 Color: 15
Size: 291844 Color: 17
Size: 278451 Color: 11

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 557559 Color: 6
Size: 442432 Color: 5

Bin 117: 11 of cap free
Amount of items: 3
Items: 
Size: 545618 Color: 15
Size: 258341 Color: 16
Size: 196031 Color: 7

Bin 118: 11 of cap free
Amount of items: 3
Items: 
Size: 654853 Color: 5
Size: 196994 Color: 15
Size: 148143 Color: 7

Bin 119: 12 of cap free
Amount of items: 3
Items: 
Size: 694603 Color: 0
Size: 154999 Color: 10
Size: 150387 Color: 2

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 516379 Color: 3
Size: 483610 Color: 12

Bin 121: 13 of cap free
Amount of items: 3
Items: 
Size: 695304 Color: 19
Size: 168444 Color: 12
Size: 136240 Color: 14

Bin 122: 13 of cap free
Amount of items: 3
Items: 
Size: 431459 Color: 5
Size: 316497 Color: 11
Size: 252032 Color: 3

Bin 123: 13 of cap free
Amount of items: 2
Items: 
Size: 556231 Color: 19
Size: 443757 Color: 6

Bin 124: 13 of cap free
Amount of items: 2
Items: 
Size: 618796 Color: 2
Size: 381192 Color: 8

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 725138 Color: 16
Size: 274850 Color: 5

Bin 126: 13 of cap free
Amount of items: 3
Items: 
Size: 367608 Color: 11
Size: 316920 Color: 11
Size: 315460 Color: 18

Bin 127: 13 of cap free
Amount of items: 2
Items: 
Size: 623361 Color: 14
Size: 376627 Color: 9

Bin 128: 13 of cap free
Amount of items: 2
Items: 
Size: 644845 Color: 16
Size: 355143 Color: 1

Bin 129: 14 of cap free
Amount of items: 3
Items: 
Size: 761445 Color: 16
Size: 131358 Color: 10
Size: 107184 Color: 15

Bin 130: 14 of cap free
Amount of items: 3
Items: 
Size: 572249 Color: 5
Size: 233213 Color: 8
Size: 194525 Color: 7

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 502793 Color: 16
Size: 497194 Color: 12

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 659533 Color: 6
Size: 340454 Color: 10

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 650857 Color: 11
Size: 349129 Color: 10

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 623697 Color: 19
Size: 376289 Color: 10

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 774055 Color: 3
Size: 113680 Color: 13
Size: 112250 Color: 17

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 761520 Color: 0
Size: 132613 Color: 16
Size: 105852 Color: 4

Bin 137: 17 of cap free
Amount of items: 3
Items: 
Size: 712802 Color: 8
Size: 150386 Color: 8
Size: 136796 Color: 7

Bin 138: 17 of cap free
Amount of items: 3
Items: 
Size: 695503 Color: 3
Size: 185977 Color: 11
Size: 118504 Color: 14

Bin 139: 17 of cap free
Amount of items: 2
Items: 
Size: 565817 Color: 17
Size: 434167 Color: 4

Bin 140: 18 of cap free
Amount of items: 3
Items: 
Size: 625441 Color: 3
Size: 187517 Color: 9
Size: 187025 Color: 19

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 613298 Color: 1
Size: 386685 Color: 16

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 528116 Color: 7
Size: 471867 Color: 1

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 587937 Color: 17
Size: 412046 Color: 9

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 738103 Color: 13
Size: 261880 Color: 0

Bin 145: 19 of cap free
Amount of items: 2
Items: 
Size: 579202 Color: 14
Size: 420780 Color: 12

Bin 146: 19 of cap free
Amount of items: 2
Items: 
Size: 535176 Color: 3
Size: 464806 Color: 11

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 652023 Color: 8
Size: 347959 Color: 11

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 752997 Color: 4
Size: 246984 Color: 0

Bin 149: 20 of cap free
Amount of items: 3
Items: 
Size: 691997 Color: 4
Size: 192548 Color: 4
Size: 115436 Color: 8

Bin 150: 20 of cap free
Amount of items: 3
Items: 
Size: 386385 Color: 13
Size: 306818 Color: 18
Size: 306778 Color: 0

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 607539 Color: 5
Size: 392442 Color: 2

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 537232 Color: 1
Size: 462749 Color: 7

Bin 153: 21 of cap free
Amount of items: 3
Items: 
Size: 765051 Color: 16
Size: 118235 Color: 10
Size: 116694 Color: 3

Bin 154: 21 of cap free
Amount of items: 2
Items: 
Size: 656546 Color: 9
Size: 343434 Color: 0

Bin 155: 22 of cap free
Amount of items: 2
Items: 
Size: 640827 Color: 19
Size: 359152 Color: 2

Bin 156: 23 of cap free
Amount of items: 3
Items: 
Size: 731330 Color: 18
Size: 138297 Color: 0
Size: 130351 Color: 9

Bin 157: 23 of cap free
Amount of items: 3
Items: 
Size: 555805 Color: 5
Size: 327781 Color: 7
Size: 116392 Color: 19

Bin 158: 24 of cap free
Amount of items: 3
Items: 
Size: 730507 Color: 15
Size: 154301 Color: 2
Size: 115169 Color: 6

Bin 159: 24 of cap free
Amount of items: 2
Items: 
Size: 650560 Color: 19
Size: 349417 Color: 4

Bin 160: 24 of cap free
Amount of items: 2
Items: 
Size: 517036 Color: 6
Size: 482941 Color: 13

Bin 161: 24 of cap free
Amount of items: 2
Items: 
Size: 578067 Color: 16
Size: 421910 Color: 2

Bin 162: 24 of cap free
Amount of items: 2
Items: 
Size: 580576 Color: 6
Size: 419401 Color: 19

Bin 163: 24 of cap free
Amount of items: 2
Items: 
Size: 589873 Color: 16
Size: 410104 Color: 3

Bin 164: 25 of cap free
Amount of items: 3
Items: 
Size: 516264 Color: 8
Size: 243202 Color: 14
Size: 240510 Color: 11

Bin 165: 25 of cap free
Amount of items: 3
Items: 
Size: 554874 Color: 11
Size: 320086 Color: 3
Size: 125016 Color: 9

Bin 166: 25 of cap free
Amount of items: 2
Items: 
Size: 738558 Color: 16
Size: 261418 Color: 15

Bin 167: 27 of cap free
Amount of items: 3
Items: 
Size: 638213 Color: 12
Size: 198379 Color: 2
Size: 163382 Color: 7

Bin 168: 27 of cap free
Amount of items: 3
Items: 
Size: 767173 Color: 6
Size: 118147 Color: 19
Size: 114654 Color: 15

Bin 169: 28 of cap free
Amount of items: 3
Items: 
Size: 653401 Color: 4
Size: 188776 Color: 9
Size: 157796 Color: 2

Bin 170: 28 of cap free
Amount of items: 3
Items: 
Size: 508157 Color: 12
Size: 274624 Color: 7
Size: 217192 Color: 17

Bin 171: 29 of cap free
Amount of items: 2
Items: 
Size: 757167 Color: 3
Size: 242805 Color: 0

Bin 172: 30 of cap free
Amount of items: 2
Items: 
Size: 629980 Color: 8
Size: 369991 Color: 15

Bin 173: 30 of cap free
Amount of items: 2
Items: 
Size: 720358 Color: 16
Size: 279613 Color: 7

Bin 174: 30 of cap free
Amount of items: 2
Items: 
Size: 717782 Color: 17
Size: 282189 Color: 5

Bin 175: 31 of cap free
Amount of items: 3
Items: 
Size: 502102 Color: 6
Size: 382069 Color: 16
Size: 115799 Color: 3

Bin 176: 32 of cap free
Amount of items: 2
Items: 
Size: 742466 Color: 15
Size: 257503 Color: 9

Bin 177: 32 of cap free
Amount of items: 3
Items: 
Size: 502589 Color: 0
Size: 334761 Color: 7
Size: 162619 Color: 5

Bin 178: 32 of cap free
Amount of items: 2
Items: 
Size: 628219 Color: 16
Size: 371750 Color: 14

Bin 179: 32 of cap free
Amount of items: 2
Items: 
Size: 752297 Color: 12
Size: 247672 Color: 3

Bin 180: 32 of cap free
Amount of items: 2
Items: 
Size: 795120 Color: 17
Size: 204849 Color: 13

Bin 181: 33 of cap free
Amount of items: 3
Items: 
Size: 766411 Color: 10
Size: 124739 Color: 18
Size: 108818 Color: 12

Bin 182: 33 of cap free
Amount of items: 3
Items: 
Size: 530347 Color: 5
Size: 271620 Color: 4
Size: 198001 Color: 13

Bin 183: 33 of cap free
Amount of items: 3
Items: 
Size: 680008 Color: 17
Size: 166228 Color: 13
Size: 153732 Color: 19

Bin 184: 33 of cap free
Amount of items: 2
Items: 
Size: 535587 Color: 16
Size: 464381 Color: 8

Bin 185: 33 of cap free
Amount of items: 2
Items: 
Size: 590017 Color: 9
Size: 409951 Color: 14

Bin 186: 34 of cap free
Amount of items: 2
Items: 
Size: 638931 Color: 3
Size: 361036 Color: 7

Bin 187: 34 of cap free
Amount of items: 2
Items: 
Size: 530916 Color: 5
Size: 469051 Color: 10

Bin 188: 35 of cap free
Amount of items: 2
Items: 
Size: 511192 Color: 4
Size: 488774 Color: 11

Bin 189: 35 of cap free
Amount of items: 2
Items: 
Size: 541808 Color: 7
Size: 458158 Color: 12

Bin 190: 35 of cap free
Amount of items: 3
Items: 
Size: 714025 Color: 0
Size: 148333 Color: 1
Size: 137608 Color: 15

Bin 191: 36 of cap free
Amount of items: 2
Items: 
Size: 760535 Color: 12
Size: 239430 Color: 9

Bin 192: 36 of cap free
Amount of items: 2
Items: 
Size: 503689 Color: 6
Size: 496276 Color: 8

Bin 193: 37 of cap free
Amount of items: 3
Items: 
Size: 688231 Color: 7
Size: 181971 Color: 6
Size: 129762 Color: 13

Bin 194: 37 of cap free
Amount of items: 2
Items: 
Size: 544742 Color: 13
Size: 455222 Color: 18

Bin 195: 37 of cap free
Amount of items: 2
Items: 
Size: 585744 Color: 3
Size: 414220 Color: 11

Bin 196: 38 of cap free
Amount of items: 2
Items: 
Size: 575486 Color: 19
Size: 424477 Color: 15

Bin 197: 39 of cap free
Amount of items: 2
Items: 
Size: 749976 Color: 7
Size: 249986 Color: 15

Bin 198: 39 of cap free
Amount of items: 2
Items: 
Size: 564197 Color: 8
Size: 435765 Color: 4

Bin 199: 40 of cap free
Amount of items: 2
Items: 
Size: 785775 Color: 2
Size: 214186 Color: 6

Bin 200: 41 of cap free
Amount of items: 3
Items: 
Size: 783355 Color: 2
Size: 113941 Color: 17
Size: 102664 Color: 7

Bin 201: 41 of cap free
Amount of items: 2
Items: 
Size: 624051 Color: 7
Size: 375909 Color: 9

Bin 202: 41 of cap free
Amount of items: 2
Items: 
Size: 560160 Color: 17
Size: 439800 Color: 8

Bin 203: 41 of cap free
Amount of items: 2
Items: 
Size: 569936 Color: 3
Size: 430024 Color: 4

Bin 204: 41 of cap free
Amount of items: 2
Items: 
Size: 619531 Color: 17
Size: 380429 Color: 4

Bin 205: 41 of cap free
Amount of items: 2
Items: 
Size: 728222 Color: 14
Size: 271738 Color: 8

Bin 206: 42 of cap free
Amount of items: 3
Items: 
Size: 545226 Color: 4
Size: 313912 Color: 12
Size: 140821 Color: 2

Bin 207: 42 of cap free
Amount of items: 2
Items: 
Size: 770171 Color: 15
Size: 229788 Color: 0

Bin 208: 43 of cap free
Amount of items: 2
Items: 
Size: 717631 Color: 18
Size: 282327 Color: 16

Bin 209: 43 of cap free
Amount of items: 2
Items: 
Size: 539498 Color: 14
Size: 460460 Color: 10

Bin 210: 43 of cap free
Amount of items: 2
Items: 
Size: 584971 Color: 7
Size: 414987 Color: 9

Bin 211: 43 of cap free
Amount of items: 2
Items: 
Size: 628503 Color: 17
Size: 371455 Color: 12

Bin 212: 45 of cap free
Amount of items: 3
Items: 
Size: 656018 Color: 10
Size: 195651 Color: 12
Size: 148287 Color: 9

Bin 213: 45 of cap free
Amount of items: 2
Items: 
Size: 506150 Color: 17
Size: 493806 Color: 15

Bin 214: 46 of cap free
Amount of items: 2
Items: 
Size: 681063 Color: 15
Size: 318892 Color: 10

Bin 215: 46 of cap free
Amount of items: 2
Items: 
Size: 507726 Color: 3
Size: 492229 Color: 9

Bin 216: 46 of cap free
Amount of items: 2
Items: 
Size: 520948 Color: 13
Size: 479007 Color: 10

Bin 217: 46 of cap free
Amount of items: 2
Items: 
Size: 629371 Color: 11
Size: 370584 Color: 12

Bin 218: 46 of cap free
Amount of items: 2
Items: 
Size: 724580 Color: 2
Size: 275375 Color: 18

Bin 219: 48 of cap free
Amount of items: 3
Items: 
Size: 529709 Color: 19
Size: 338524 Color: 11
Size: 131720 Color: 5

Bin 220: 48 of cap free
Amount of items: 2
Items: 
Size: 500346 Color: 13
Size: 499607 Color: 19

Bin 221: 48 of cap free
Amount of items: 2
Items: 
Size: 528442 Color: 9
Size: 471511 Color: 13

Bin 222: 49 of cap free
Amount of items: 3
Items: 
Size: 727161 Color: 13
Size: 166191 Color: 16
Size: 106600 Color: 6

Bin 223: 49 of cap free
Amount of items: 2
Items: 
Size: 776911 Color: 0
Size: 223041 Color: 12

Bin 224: 49 of cap free
Amount of items: 2
Items: 
Size: 500871 Color: 7
Size: 499081 Color: 13

Bin 225: 49 of cap free
Amount of items: 2
Items: 
Size: 795732 Color: 0
Size: 204220 Color: 4

Bin 226: 51 of cap free
Amount of items: 3
Items: 
Size: 749402 Color: 12
Size: 148435 Color: 7
Size: 102113 Color: 6

Bin 227: 52 of cap free
Amount of items: 2
Items: 
Size: 597180 Color: 16
Size: 402769 Color: 11

Bin 228: 52 of cap free
Amount of items: 2
Items: 
Size: 655056 Color: 19
Size: 344893 Color: 4

Bin 229: 53 of cap free
Amount of items: 3
Items: 
Size: 383868 Color: 1
Size: 373093 Color: 7
Size: 242987 Color: 6

Bin 230: 53 of cap free
Amount of items: 3
Items: 
Size: 383088 Color: 16
Size: 310204 Color: 14
Size: 306656 Color: 4

Bin 231: 53 of cap free
Amount of items: 2
Items: 
Size: 727582 Color: 5
Size: 272366 Color: 19

Bin 232: 54 of cap free
Amount of items: 2
Items: 
Size: 758602 Color: 7
Size: 241345 Color: 10

Bin 233: 54 of cap free
Amount of items: 2
Items: 
Size: 559332 Color: 3
Size: 440615 Color: 10

Bin 234: 54 of cap free
Amount of items: 2
Items: 
Size: 508040 Color: 17
Size: 491907 Color: 0

Bin 235: 55 of cap free
Amount of items: 2
Items: 
Size: 665946 Color: 10
Size: 334000 Color: 16

Bin 236: 56 of cap free
Amount of items: 2
Items: 
Size: 728748 Color: 16
Size: 271197 Color: 8

Bin 237: 56 of cap free
Amount of items: 3
Items: 
Size: 502814 Color: 7
Size: 364142 Color: 2
Size: 132989 Color: 16

Bin 238: 56 of cap free
Amount of items: 3
Items: 
Size: 364074 Color: 4
Size: 326639 Color: 5
Size: 309232 Color: 4

Bin 239: 56 of cap free
Amount of items: 3
Items: 
Size: 675810 Color: 4
Size: 162996 Color: 6
Size: 161139 Color: 6

Bin 240: 58 of cap free
Amount of items: 2
Items: 
Size: 716222 Color: 1
Size: 283721 Color: 12

Bin 241: 59 of cap free
Amount of items: 2
Items: 
Size: 709054 Color: 15
Size: 290888 Color: 17

Bin 242: 59 of cap free
Amount of items: 2
Items: 
Size: 549198 Color: 15
Size: 450744 Color: 12

Bin 243: 60 of cap free
Amount of items: 3
Items: 
Size: 708470 Color: 13
Size: 157636 Color: 4
Size: 133835 Color: 11

Bin 244: 61 of cap free
Amount of items: 2
Items: 
Size: 733337 Color: 10
Size: 266603 Color: 17

Bin 245: 61 of cap free
Amount of items: 2
Items: 
Size: 576135 Color: 12
Size: 423805 Color: 0

Bin 246: 63 of cap free
Amount of items: 2
Items: 
Size: 577848 Color: 0
Size: 422090 Color: 8

Bin 247: 64 of cap free
Amount of items: 2
Items: 
Size: 754820 Color: 5
Size: 245117 Color: 8

Bin 248: 65 of cap free
Amount of items: 3
Items: 
Size: 590743 Color: 0
Size: 265994 Color: 8
Size: 143199 Color: 14

Bin 249: 67 of cap free
Amount of items: 2
Items: 
Size: 612862 Color: 5
Size: 387072 Color: 10

Bin 250: 67 of cap free
Amount of items: 2
Items: 
Size: 521635 Color: 13
Size: 478299 Color: 15

Bin 251: 67 of cap free
Amount of items: 2
Items: 
Size: 523592 Color: 17
Size: 476342 Color: 4

Bin 252: 68 of cap free
Amount of items: 2
Items: 
Size: 686827 Color: 7
Size: 313106 Color: 6

Bin 253: 68 of cap free
Amount of items: 2
Items: 
Size: 575384 Color: 10
Size: 424549 Color: 7

Bin 254: 69 of cap free
Amount of items: 3
Items: 
Size: 417366 Color: 15
Size: 324691 Color: 5
Size: 257875 Color: 3

Bin 255: 69 of cap free
Amount of items: 3
Items: 
Size: 546906 Color: 16
Size: 256995 Color: 7
Size: 196031 Color: 6

Bin 256: 69 of cap free
Amount of items: 2
Items: 
Size: 662196 Color: 18
Size: 337736 Color: 3

Bin 257: 70 of cap free
Amount of items: 3
Items: 
Size: 707761 Color: 9
Size: 172924 Color: 7
Size: 119246 Color: 2

Bin 258: 70 of cap free
Amount of items: 2
Items: 
Size: 561340 Color: 4
Size: 438591 Color: 17

Bin 259: 70 of cap free
Amount of items: 2
Items: 
Size: 587157 Color: 15
Size: 412774 Color: 17

Bin 260: 71 of cap free
Amount of items: 3
Items: 
Size: 583676 Color: 9
Size: 306296 Color: 16
Size: 109958 Color: 0

Bin 261: 71 of cap free
Amount of items: 2
Items: 
Size: 648184 Color: 16
Size: 351746 Color: 2

Bin 262: 71 of cap free
Amount of items: 2
Items: 
Size: 744890 Color: 17
Size: 255040 Color: 14

Bin 263: 71 of cap free
Amount of items: 2
Items: 
Size: 553722 Color: 5
Size: 446208 Color: 15

Bin 264: 71 of cap free
Amount of items: 2
Items: 
Size: 512650 Color: 8
Size: 487280 Color: 6

Bin 265: 71 of cap free
Amount of items: 2
Items: 
Size: 669982 Color: 12
Size: 329948 Color: 11

Bin 266: 73 of cap free
Amount of items: 2
Items: 
Size: 597475 Color: 19
Size: 402453 Color: 17

Bin 267: 74 of cap free
Amount of items: 2
Items: 
Size: 548758 Color: 16
Size: 451169 Color: 6

Bin 268: 75 of cap free
Amount of items: 2
Items: 
Size: 534992 Color: 10
Size: 464934 Color: 1

Bin 269: 75 of cap free
Amount of items: 2
Items: 
Size: 724874 Color: 13
Size: 275052 Color: 5

Bin 270: 76 of cap free
Amount of items: 3
Items: 
Size: 649914 Color: 2
Size: 194853 Color: 8
Size: 155158 Color: 12

Bin 271: 76 of cap free
Amount of items: 2
Items: 
Size: 526163 Color: 5
Size: 473762 Color: 12

Bin 272: 77 of cap free
Amount of items: 2
Items: 
Size: 514635 Color: 7
Size: 485289 Color: 0

Bin 273: 77 of cap free
Amount of items: 2
Items: 
Size: 578262 Color: 7
Size: 421662 Color: 16

Bin 274: 77 of cap free
Amount of items: 2
Items: 
Size: 730128 Color: 0
Size: 269796 Color: 3

Bin 275: 78 of cap free
Amount of items: 3
Items: 
Size: 694679 Color: 4
Size: 169948 Color: 10
Size: 135296 Color: 12

Bin 276: 79 of cap free
Amount of items: 2
Items: 
Size: 765692 Color: 5
Size: 234230 Color: 17

Bin 277: 79 of cap free
Amount of items: 2
Items: 
Size: 695414 Color: 5
Size: 304508 Color: 6

Bin 278: 79 of cap free
Amount of items: 2
Items: 
Size: 622843 Color: 5
Size: 377079 Color: 15

Bin 279: 80 of cap free
Amount of items: 2
Items: 
Size: 618623 Color: 6
Size: 381298 Color: 8

Bin 280: 80 of cap free
Amount of items: 2
Items: 
Size: 626333 Color: 2
Size: 373588 Color: 19

Bin 281: 81 of cap free
Amount of items: 2
Items: 
Size: 734726 Color: 9
Size: 265194 Color: 18

Bin 282: 82 of cap free
Amount of items: 2
Items: 
Size: 650337 Color: 16
Size: 349582 Color: 15

Bin 283: 84 of cap free
Amount of items: 2
Items: 
Size: 669085 Color: 15
Size: 330832 Color: 12

Bin 284: 84 of cap free
Amount of items: 2
Items: 
Size: 556028 Color: 4
Size: 443889 Color: 7

Bin 285: 84 of cap free
Amount of items: 2
Items: 
Size: 632397 Color: 15
Size: 367520 Color: 14

Bin 286: 85 of cap free
Amount of items: 2
Items: 
Size: 769561 Color: 15
Size: 230355 Color: 14

Bin 287: 85 of cap free
Amount of items: 2
Items: 
Size: 639809 Color: 16
Size: 360107 Color: 3

Bin 288: 85 of cap free
Amount of items: 2
Items: 
Size: 703676 Color: 0
Size: 296240 Color: 3

Bin 289: 85 of cap free
Amount of items: 2
Items: 
Size: 752110 Color: 19
Size: 247806 Color: 6

Bin 290: 86 of cap free
Amount of items: 3
Items: 
Size: 736124 Color: 10
Size: 140180 Color: 17
Size: 123611 Color: 10

Bin 291: 86 of cap free
Amount of items: 2
Items: 
Size: 650209 Color: 10
Size: 349706 Color: 12

Bin 292: 87 of cap free
Amount of items: 2
Items: 
Size: 687819 Color: 13
Size: 312095 Color: 15

Bin 293: 88 of cap free
Amount of items: 2
Items: 
Size: 501841 Color: 3
Size: 498072 Color: 15

Bin 294: 88 of cap free
Amount of items: 2
Items: 
Size: 573014 Color: 14
Size: 426899 Color: 2

Bin 295: 90 of cap free
Amount of items: 2
Items: 
Size: 533959 Color: 5
Size: 465952 Color: 9

Bin 296: 90 of cap free
Amount of items: 2
Items: 
Size: 527966 Color: 19
Size: 471945 Color: 10

Bin 297: 91 of cap free
Amount of items: 2
Items: 
Size: 612949 Color: 10
Size: 386961 Color: 9

Bin 298: 91 of cap free
Amount of items: 2
Items: 
Size: 616658 Color: 2
Size: 383252 Color: 13

Bin 299: 91 of cap free
Amount of items: 2
Items: 
Size: 673051 Color: 7
Size: 326859 Color: 8

Bin 300: 92 of cap free
Amount of items: 2
Items: 
Size: 551681 Color: 16
Size: 448228 Color: 0

Bin 301: 93 of cap free
Amount of items: 3
Items: 
Size: 700878 Color: 13
Size: 154563 Color: 9
Size: 144467 Color: 17

Bin 302: 93 of cap free
Amount of items: 2
Items: 
Size: 715743 Color: 7
Size: 284165 Color: 15

Bin 303: 93 of cap free
Amount of items: 2
Items: 
Size: 552759 Color: 11
Size: 447149 Color: 19

Bin 304: 94 of cap free
Amount of items: 2
Items: 
Size: 510231 Color: 5
Size: 489676 Color: 11

Bin 305: 96 of cap free
Amount of items: 2
Items: 
Size: 537176 Color: 18
Size: 462729 Color: 9

Bin 306: 97 of cap free
Amount of items: 2
Items: 
Size: 739069 Color: 1
Size: 260835 Color: 17

Bin 307: 98 of cap free
Amount of items: 2
Items: 
Size: 637547 Color: 15
Size: 362356 Color: 1

Bin 308: 98 of cap free
Amount of items: 2
Items: 
Size: 593014 Color: 11
Size: 406889 Color: 5

Bin 309: 99 of cap free
Amount of items: 2
Items: 
Size: 780823 Color: 10
Size: 219079 Color: 16

Bin 310: 100 of cap free
Amount of items: 2
Items: 
Size: 653912 Color: 17
Size: 345989 Color: 13

Bin 311: 101 of cap free
Amount of items: 2
Items: 
Size: 799709 Color: 8
Size: 200191 Color: 0

Bin 312: 101 of cap free
Amount of items: 2
Items: 
Size: 588321 Color: 18
Size: 411579 Color: 14

Bin 313: 101 of cap free
Amount of items: 2
Items: 
Size: 570988 Color: 11
Size: 428912 Color: 18

Bin 314: 102 of cap free
Amount of items: 2
Items: 
Size: 649693 Color: 10
Size: 350206 Color: 8

Bin 315: 102 of cap free
Amount of items: 2
Items: 
Size: 684693 Color: 10
Size: 315206 Color: 6

Bin 316: 103 of cap free
Amount of items: 2
Items: 
Size: 700505 Color: 15
Size: 299393 Color: 11

Bin 317: 104 of cap free
Amount of items: 2
Items: 
Size: 508781 Color: 5
Size: 491116 Color: 1

Bin 318: 104 of cap free
Amount of items: 2
Items: 
Size: 520489 Color: 10
Size: 479408 Color: 6

Bin 319: 106 of cap free
Amount of items: 2
Items: 
Size: 523661 Color: 2
Size: 476234 Color: 13

Bin 320: 107 of cap free
Amount of items: 2
Items: 
Size: 674345 Color: 19
Size: 325549 Color: 3

Bin 321: 108 of cap free
Amount of items: 2
Items: 
Size: 610605 Color: 5
Size: 389288 Color: 10

Bin 322: 108 of cap free
Amount of items: 2
Items: 
Size: 636063 Color: 18
Size: 363830 Color: 5

Bin 323: 110 of cap free
Amount of items: 2
Items: 
Size: 606745 Color: 0
Size: 393146 Color: 16

Bin 324: 110 of cap free
Amount of items: 2
Items: 
Size: 707095 Color: 2
Size: 292796 Color: 16

Bin 325: 112 of cap free
Amount of items: 3
Items: 
Size: 546235 Color: 8
Size: 276345 Color: 1
Size: 177309 Color: 14

Bin 326: 112 of cap free
Amount of items: 2
Items: 
Size: 782137 Color: 8
Size: 217752 Color: 19

Bin 327: 112 of cap free
Amount of items: 2
Items: 
Size: 740693 Color: 19
Size: 259196 Color: 3

Bin 328: 113 of cap free
Amount of items: 2
Items: 
Size: 525742 Color: 1
Size: 474146 Color: 5

Bin 329: 114 of cap free
Amount of items: 2
Items: 
Size: 641997 Color: 9
Size: 357890 Color: 4

Bin 330: 114 of cap free
Amount of items: 2
Items: 
Size: 719607 Color: 5
Size: 280280 Color: 3

Bin 331: 114 of cap free
Amount of items: 2
Items: 
Size: 796939 Color: 4
Size: 202948 Color: 17

Bin 332: 115 of cap free
Amount of items: 2
Items: 
Size: 549436 Color: 10
Size: 450450 Color: 0

Bin 333: 115 of cap free
Amount of items: 2
Items: 
Size: 668940 Color: 16
Size: 330946 Color: 19

Bin 334: 116 of cap free
Amount of items: 3
Items: 
Size: 676032 Color: 12
Size: 179569 Color: 5
Size: 144284 Color: 11

Bin 335: 116 of cap free
Amount of items: 2
Items: 
Size: 728621 Color: 10
Size: 271264 Color: 14

Bin 336: 116 of cap free
Amount of items: 2
Items: 
Size: 767752 Color: 13
Size: 232133 Color: 17

Bin 337: 117 of cap free
Amount of items: 2
Items: 
Size: 682409 Color: 4
Size: 317475 Color: 11

Bin 338: 120 of cap free
Amount of items: 2
Items: 
Size: 709505 Color: 18
Size: 290376 Color: 14

Bin 339: 121 of cap free
Amount of items: 2
Items: 
Size: 636867 Color: 17
Size: 363013 Color: 7

Bin 340: 122 of cap free
Amount of items: 2
Items: 
Size: 586536 Color: 19
Size: 413343 Color: 3

Bin 341: 122 of cap free
Amount of items: 2
Items: 
Size: 608535 Color: 19
Size: 391344 Color: 6

Bin 342: 123 of cap free
Amount of items: 2
Items: 
Size: 660331 Color: 17
Size: 339547 Color: 8

Bin 343: 126 of cap free
Amount of items: 2
Items: 
Size: 625408 Color: 9
Size: 374467 Color: 13

Bin 344: 126 of cap free
Amount of items: 2
Items: 
Size: 500105 Color: 18
Size: 499770 Color: 16

Bin 345: 126 of cap free
Amount of items: 2
Items: 
Size: 652724 Color: 14
Size: 347151 Color: 18

Bin 346: 127 of cap free
Amount of items: 3
Items: 
Size: 694823 Color: 2
Size: 168635 Color: 6
Size: 136416 Color: 1

Bin 347: 127 of cap free
Amount of items: 2
Items: 
Size: 536561 Color: 16
Size: 463313 Color: 5

Bin 348: 128 of cap free
Amount of items: 2
Items: 
Size: 688590 Color: 0
Size: 311283 Color: 2

Bin 349: 128 of cap free
Amount of items: 2
Items: 
Size: 702862 Color: 18
Size: 297011 Color: 11

Bin 350: 129 of cap free
Amount of items: 2
Items: 
Size: 600794 Color: 4
Size: 399078 Color: 3

Bin 351: 130 of cap free
Amount of items: 2
Items: 
Size: 630381 Color: 10
Size: 369490 Color: 9

Bin 352: 130 of cap free
Amount of items: 2
Items: 
Size: 758253 Color: 12
Size: 241618 Color: 7

Bin 353: 131 of cap free
Amount of items: 2
Items: 
Size: 640585 Color: 7
Size: 359285 Color: 5

Bin 354: 131 of cap free
Amount of items: 2
Items: 
Size: 509690 Color: 9
Size: 490180 Color: 8

Bin 355: 132 of cap free
Amount of items: 2
Items: 
Size: 658533 Color: 0
Size: 341336 Color: 9

Bin 356: 133 of cap free
Amount of items: 2
Items: 
Size: 715095 Color: 1
Size: 284773 Color: 7

Bin 357: 133 of cap free
Amount of items: 2
Items: 
Size: 601755 Color: 4
Size: 398113 Color: 12

Bin 358: 134 of cap free
Amount of items: 2
Items: 
Size: 573524 Color: 17
Size: 426343 Color: 9

Bin 359: 135 of cap free
Amount of items: 3
Items: 
Size: 773092 Color: 2
Size: 115019 Color: 1
Size: 111755 Color: 7

Bin 360: 136 of cap free
Amount of items: 2
Items: 
Size: 516596 Color: 18
Size: 483269 Color: 8

Bin 361: 137 of cap free
Amount of items: 2
Items: 
Size: 609443 Color: 14
Size: 390421 Color: 7

Bin 362: 137 of cap free
Amount of items: 2
Items: 
Size: 603618 Color: 18
Size: 396246 Color: 3

Bin 363: 139 of cap free
Amount of items: 2
Items: 
Size: 695588 Color: 2
Size: 304274 Color: 10

Bin 364: 139 of cap free
Amount of items: 2
Items: 
Size: 706453 Color: 11
Size: 293409 Color: 1

Bin 365: 139 of cap free
Amount of items: 2
Items: 
Size: 529462 Color: 18
Size: 470400 Color: 13

Bin 366: 139 of cap free
Amount of items: 2
Items: 
Size: 558070 Color: 9
Size: 441792 Color: 18

Bin 367: 141 of cap free
Amount of items: 2
Items: 
Size: 616820 Color: 11
Size: 383040 Color: 15

Bin 368: 141 of cap free
Amount of items: 2
Items: 
Size: 703291 Color: 19
Size: 296569 Color: 18

Bin 369: 142 of cap free
Amount of items: 2
Items: 
Size: 715037 Color: 13
Size: 284822 Color: 0

Bin 370: 142 of cap free
Amount of items: 2
Items: 
Size: 525155 Color: 8
Size: 474704 Color: 13

Bin 371: 143 of cap free
Amount of items: 2
Items: 
Size: 719869 Color: 6
Size: 279989 Color: 14

Bin 372: 144 of cap free
Amount of items: 2
Items: 
Size: 643651 Color: 12
Size: 356206 Color: 5

Bin 373: 145 of cap free
Amount of items: 3
Items: 
Size: 653869 Color: 5
Size: 179088 Color: 0
Size: 166899 Color: 19

Bin 374: 145 of cap free
Amount of items: 2
Items: 
Size: 584039 Color: 11
Size: 415817 Color: 0

Bin 375: 145 of cap free
Amount of items: 2
Items: 
Size: 643180 Color: 5
Size: 356676 Color: 12

Bin 376: 147 of cap free
Amount of items: 2
Items: 
Size: 557178 Color: 3
Size: 442676 Color: 0

Bin 377: 147 of cap free
Amount of items: 2
Items: 
Size: 699744 Color: 0
Size: 300110 Color: 9

Bin 378: 149 of cap free
Amount of items: 3
Items: 
Size: 624036 Color: 19
Size: 198569 Color: 1
Size: 177247 Color: 14

Bin 379: 151 of cap free
Amount of items: 2
Items: 
Size: 607920 Color: 5
Size: 391930 Color: 4

Bin 380: 151 of cap free
Amount of items: 2
Items: 
Size: 616361 Color: 0
Size: 383489 Color: 17

Bin 381: 151 of cap free
Amount of items: 2
Items: 
Size: 542276 Color: 14
Size: 457574 Color: 5

Bin 382: 151 of cap free
Amount of items: 2
Items: 
Size: 599195 Color: 19
Size: 400655 Color: 12

Bin 383: 152 of cap free
Amount of items: 3
Items: 
Size: 619851 Color: 2
Size: 191209 Color: 16
Size: 188789 Color: 19

Bin 384: 152 of cap free
Amount of items: 2
Items: 
Size: 551184 Color: 18
Size: 448665 Color: 2

Bin 385: 153 of cap free
Amount of items: 2
Items: 
Size: 664933 Color: 18
Size: 334915 Color: 16

Bin 386: 154 of cap free
Amount of items: 2
Items: 
Size: 769280 Color: 0
Size: 230567 Color: 13

Bin 387: 155 of cap free
Amount of items: 2
Items: 
Size: 557368 Color: 12
Size: 442478 Color: 3

Bin 388: 155 of cap free
Amount of items: 2
Items: 
Size: 604008 Color: 0
Size: 395838 Color: 15

Bin 389: 156 of cap free
Amount of items: 2
Items: 
Size: 654287 Color: 19
Size: 345558 Color: 5

Bin 390: 157 of cap free
Amount of items: 3
Items: 
Size: 629500 Color: 11
Size: 185449 Color: 10
Size: 184895 Color: 11

Bin 391: 159 of cap free
Amount of items: 2
Items: 
Size: 651274 Color: 16
Size: 348568 Color: 9

Bin 392: 160 of cap free
Amount of items: 2
Items: 
Size: 544137 Color: 12
Size: 455704 Color: 13

Bin 393: 160 of cap free
Amount of items: 2
Items: 
Size: 643222 Color: 12
Size: 356619 Color: 0

Bin 394: 161 of cap free
Amount of items: 2
Items: 
Size: 601636 Color: 12
Size: 398204 Color: 7

Bin 395: 161 of cap free
Amount of items: 2
Items: 
Size: 620216 Color: 1
Size: 379624 Color: 16

Bin 396: 162 of cap free
Amount of items: 2
Items: 
Size: 642117 Color: 6
Size: 357722 Color: 17

Bin 397: 163 of cap free
Amount of items: 3
Items: 
Size: 711904 Color: 17
Size: 151194 Color: 7
Size: 136740 Color: 4

Bin 398: 164 of cap free
Amount of items: 2
Items: 
Size: 784212 Color: 12
Size: 215625 Color: 18

Bin 399: 167 of cap free
Amount of items: 2
Items: 
Size: 565615 Color: 9
Size: 434219 Color: 15

Bin 400: 168 of cap free
Amount of items: 2
Items: 
Size: 756512 Color: 18
Size: 243321 Color: 8

Bin 401: 168 of cap free
Amount of items: 2
Items: 
Size: 757893 Color: 10
Size: 241940 Color: 5

Bin 402: 169 of cap free
Amount of items: 2
Items: 
Size: 680175 Color: 17
Size: 319657 Color: 7

Bin 403: 169 of cap free
Amount of items: 2
Items: 
Size: 512294 Color: 17
Size: 487538 Color: 16

Bin 404: 169 of cap free
Amount of items: 2
Items: 
Size: 772852 Color: 3
Size: 226980 Color: 0

Bin 405: 170 of cap free
Amount of items: 2
Items: 
Size: 732805 Color: 15
Size: 267026 Color: 14

Bin 406: 171 of cap free
Amount of items: 2
Items: 
Size: 637722 Color: 2
Size: 362108 Color: 17

Bin 407: 171 of cap free
Amount of items: 2
Items: 
Size: 681733 Color: 4
Size: 318097 Color: 15

Bin 408: 173 of cap free
Amount of items: 2
Items: 
Size: 556178 Color: 14
Size: 443650 Color: 3

Bin 409: 173 of cap free
Amount of items: 2
Items: 
Size: 777833 Color: 19
Size: 221995 Color: 1

Bin 410: 175 of cap free
Amount of items: 2
Items: 
Size: 550904 Color: 12
Size: 448922 Color: 17

Bin 411: 176 of cap free
Amount of items: 3
Items: 
Size: 532541 Color: 5
Size: 312779 Color: 4
Size: 154505 Color: 6

Bin 412: 176 of cap free
Amount of items: 2
Items: 
Size: 710113 Color: 9
Size: 289712 Color: 3

Bin 413: 178 of cap free
Amount of items: 2
Items: 
Size: 630370 Color: 3
Size: 369453 Color: 14

Bin 414: 179 of cap free
Amount of items: 2
Items: 
Size: 780067 Color: 0
Size: 219755 Color: 15

Bin 415: 181 of cap free
Amount of items: 3
Items: 
Size: 646402 Color: 6
Size: 217002 Color: 10
Size: 136416 Color: 13

Bin 416: 182 of cap free
Amount of items: 2
Items: 
Size: 513401 Color: 15
Size: 486418 Color: 18

Bin 417: 182 of cap free
Amount of items: 2
Items: 
Size: 665752 Color: 10
Size: 334067 Color: 4

Bin 418: 183 of cap free
Amount of items: 2
Items: 
Size: 708654 Color: 2
Size: 291164 Color: 1

Bin 419: 183 of cap free
Amount of items: 2
Items: 
Size: 614825 Color: 7
Size: 384993 Color: 2

Bin 420: 183 of cap free
Amount of items: 2
Items: 
Size: 506025 Color: 18
Size: 493793 Color: 9

Bin 421: 184 of cap free
Amount of items: 2
Items: 
Size: 562245 Color: 4
Size: 437572 Color: 8

Bin 422: 184 of cap free
Amount of items: 2
Items: 
Size: 577840 Color: 3
Size: 421977 Color: 10

Bin 423: 184 of cap free
Amount of items: 2
Items: 
Size: 771291 Color: 14
Size: 228526 Color: 4

Bin 424: 186 of cap free
Amount of items: 2
Items: 
Size: 629777 Color: 18
Size: 370038 Color: 10

Bin 425: 188 of cap free
Amount of items: 3
Items: 
Size: 652264 Color: 11
Size: 182298 Color: 18
Size: 165251 Color: 5

Bin 426: 188 of cap free
Amount of items: 2
Items: 
Size: 646769 Color: 9
Size: 353044 Color: 15

Bin 427: 189 of cap free
Amount of items: 2
Items: 
Size: 572727 Color: 4
Size: 427085 Color: 2

Bin 428: 190 of cap free
Amount of items: 2
Items: 
Size: 566159 Color: 7
Size: 433652 Color: 4

Bin 429: 191 of cap free
Amount of items: 2
Items: 
Size: 683271 Color: 15
Size: 316539 Color: 4

Bin 430: 191 of cap free
Amount of items: 2
Items: 
Size: 546427 Color: 1
Size: 453383 Color: 14

Bin 431: 191 of cap free
Amount of items: 2
Items: 
Size: 520332 Color: 11
Size: 479478 Color: 6

Bin 432: 192 of cap free
Amount of items: 2
Items: 
Size: 638197 Color: 2
Size: 361612 Color: 7

Bin 433: 196 of cap free
Amount of items: 2
Items: 
Size: 751358 Color: 13
Size: 248447 Color: 7

Bin 434: 196 of cap free
Amount of items: 2
Items: 
Size: 638832 Color: 14
Size: 360973 Color: 9

Bin 435: 196 of cap free
Amount of items: 3
Items: 
Size: 625120 Color: 15
Size: 192196 Color: 2
Size: 182489 Color: 19

Bin 436: 198 of cap free
Amount of items: 2
Items: 
Size: 570913 Color: 0
Size: 428890 Color: 12

Bin 437: 198 of cap free
Amount of items: 2
Items: 
Size: 785193 Color: 7
Size: 214610 Color: 2

Bin 438: 199 of cap free
Amount of items: 2
Items: 
Size: 789709 Color: 0
Size: 210093 Color: 7

Bin 439: 199 of cap free
Amount of items: 3
Items: 
Size: 752671 Color: 18
Size: 123586 Color: 15
Size: 123545 Color: 14

Bin 440: 199 of cap free
Amount of items: 2
Items: 
Size: 608893 Color: 13
Size: 390909 Color: 8

Bin 441: 200 of cap free
Amount of items: 3
Items: 
Size: 650813 Color: 12
Size: 221412 Color: 14
Size: 127576 Color: 14

Bin 442: 201 of cap free
Amount of items: 2
Items: 
Size: 794752 Color: 10
Size: 205048 Color: 15

Bin 443: 202 of cap free
Amount of items: 2
Items: 
Size: 774422 Color: 10
Size: 225377 Color: 15

Bin 444: 202 of cap free
Amount of items: 2
Items: 
Size: 757284 Color: 5
Size: 242515 Color: 2

Bin 445: 203 of cap free
Amount of items: 2
Items: 
Size: 515358 Color: 11
Size: 484440 Color: 2

Bin 446: 206 of cap free
Amount of items: 2
Items: 
Size: 506913 Color: 2
Size: 492882 Color: 14

Bin 447: 206 of cap free
Amount of items: 2
Items: 
Size: 666013 Color: 2
Size: 333782 Color: 13

Bin 448: 207 of cap free
Amount of items: 2
Items: 
Size: 659211 Color: 19
Size: 340583 Color: 9

Bin 449: 208 of cap free
Amount of items: 2
Items: 
Size: 594897 Color: 1
Size: 404896 Color: 3

Bin 450: 209 of cap free
Amount of items: 2
Items: 
Size: 510165 Color: 9
Size: 489627 Color: 16

Bin 451: 210 of cap free
Amount of items: 2
Items: 
Size: 710093 Color: 12
Size: 289698 Color: 3

Bin 452: 210 of cap free
Amount of items: 2
Items: 
Size: 671925 Color: 10
Size: 327866 Color: 12

Bin 453: 210 of cap free
Amount of items: 2
Items: 
Size: 758995 Color: 14
Size: 240796 Color: 13

Bin 454: 212 of cap free
Amount of items: 2
Items: 
Size: 660882 Color: 11
Size: 338907 Color: 15

Bin 455: 213 of cap free
Amount of items: 2
Items: 
Size: 613887 Color: 7
Size: 385901 Color: 19

Bin 456: 213 of cap free
Amount of items: 2
Items: 
Size: 721759 Color: 7
Size: 278029 Color: 5

Bin 457: 213 of cap free
Amount of items: 2
Items: 
Size: 589636 Color: 5
Size: 410152 Color: 15

Bin 458: 217 of cap free
Amount of items: 2
Items: 
Size: 799626 Color: 7
Size: 200158 Color: 2

Bin 459: 217 of cap free
Amount of items: 2
Items: 
Size: 501361 Color: 2
Size: 498423 Color: 16

Bin 460: 218 of cap free
Amount of items: 2
Items: 
Size: 548211 Color: 5
Size: 451572 Color: 10

Bin 461: 219 of cap free
Amount of items: 2
Items: 
Size: 581274 Color: 12
Size: 418508 Color: 10

Bin 462: 219 of cap free
Amount of items: 2
Items: 
Size: 529395 Color: 18
Size: 470387 Color: 19

Bin 463: 220 of cap free
Amount of items: 2
Items: 
Size: 602482 Color: 12
Size: 397299 Color: 11

Bin 464: 221 of cap free
Amount of items: 2
Items: 
Size: 640676 Color: 17
Size: 359104 Color: 4

Bin 465: 221 of cap free
Amount of items: 2
Items: 
Size: 637683 Color: 3
Size: 362097 Color: 2

Bin 466: 221 of cap free
Amount of items: 3
Items: 
Size: 514032 Color: 5
Size: 327283 Color: 4
Size: 158465 Color: 15

Bin 467: 222 of cap free
Amount of items: 2
Items: 
Size: 778091 Color: 14
Size: 221688 Color: 16

Bin 468: 224 of cap free
Amount of items: 2
Items: 
Size: 673497 Color: 5
Size: 326280 Color: 2

Bin 469: 225 of cap free
Amount of items: 3
Items: 
Size: 587911 Color: 9
Size: 222812 Color: 18
Size: 189053 Color: 0

Bin 470: 228 of cap free
Amount of items: 2
Items: 
Size: 564353 Color: 7
Size: 435420 Color: 2

Bin 471: 228 of cap free
Amount of items: 2
Items: 
Size: 597016 Color: 12
Size: 402757 Color: 15

Bin 472: 230 of cap free
Amount of items: 3
Items: 
Size: 702074 Color: 13
Size: 158325 Color: 7
Size: 139372 Color: 10

Bin 473: 235 of cap free
Amount of items: 2
Items: 
Size: 767215 Color: 6
Size: 232551 Color: 7

Bin 474: 237 of cap free
Amount of items: 2
Items: 
Size: 632274 Color: 9
Size: 367490 Color: 8

Bin 475: 238 of cap free
Amount of items: 2
Items: 
Size: 542275 Color: 15
Size: 457488 Color: 19

Bin 476: 239 of cap free
Amount of items: 2
Items: 
Size: 621882 Color: 8
Size: 377880 Color: 14

Bin 477: 240 of cap free
Amount of items: 2
Items: 
Size: 592899 Color: 2
Size: 406862 Color: 9

Bin 478: 241 of cap free
Amount of items: 2
Items: 
Size: 619478 Color: 5
Size: 380282 Color: 10

Bin 479: 242 of cap free
Amount of items: 2
Items: 
Size: 713583 Color: 4
Size: 286176 Color: 11

Bin 480: 242 of cap free
Amount of items: 2
Items: 
Size: 753053 Color: 5
Size: 246706 Color: 12

Bin 481: 244 of cap free
Amount of items: 2
Items: 
Size: 748556 Color: 12
Size: 251201 Color: 4

Bin 482: 246 of cap free
Amount of items: 2
Items: 
Size: 723410 Color: 11
Size: 276345 Color: 14

Bin 483: 247 of cap free
Amount of items: 2
Items: 
Size: 749106 Color: 2
Size: 250648 Color: 18

Bin 484: 248 of cap free
Amount of items: 2
Items: 
Size: 569327 Color: 18
Size: 430426 Color: 15

Bin 485: 249 of cap free
Amount of items: 2
Items: 
Size: 563195 Color: 16
Size: 436557 Color: 13

Bin 486: 250 of cap free
Amount of items: 2
Items: 
Size: 760079 Color: 11
Size: 239672 Color: 0

Bin 487: 251 of cap free
Amount of items: 2
Items: 
Size: 781656 Color: 11
Size: 218094 Color: 13

Bin 488: 252 of cap free
Amount of items: 2
Items: 
Size: 615486 Color: 19
Size: 384263 Color: 7

Bin 489: 252 of cap free
Amount of items: 2
Items: 
Size: 726796 Color: 19
Size: 272953 Color: 14

Bin 490: 256 of cap free
Amount of items: 2
Items: 
Size: 764192 Color: 17
Size: 235553 Color: 2

Bin 491: 257 of cap free
Amount of items: 2
Items: 
Size: 550467 Color: 19
Size: 449277 Color: 2

Bin 492: 258 of cap free
Amount of items: 2
Items: 
Size: 604843 Color: 6
Size: 394900 Color: 9

Bin 493: 260 of cap free
Amount of items: 2
Items: 
Size: 742031 Color: 12
Size: 257710 Color: 1

Bin 494: 260 of cap free
Amount of items: 2
Items: 
Size: 722002 Color: 19
Size: 277739 Color: 17

Bin 495: 261 of cap free
Amount of items: 2
Items: 
Size: 541023 Color: 8
Size: 458717 Color: 11

Bin 496: 263 of cap free
Amount of items: 3
Items: 
Size: 647767 Color: 19
Size: 203884 Color: 15
Size: 148087 Color: 9

Bin 497: 265 of cap free
Amount of items: 2
Items: 
Size: 744493 Color: 19
Size: 255243 Color: 17

Bin 498: 266 of cap free
Amount of items: 2
Items: 
Size: 755247 Color: 13
Size: 244488 Color: 12

Bin 499: 266 of cap free
Amount of items: 2
Items: 
Size: 783476 Color: 19
Size: 216259 Color: 9

Bin 500: 266 of cap free
Amount of items: 2
Items: 
Size: 605821 Color: 1
Size: 393914 Color: 7

Bin 501: 267 of cap free
Amount of items: 2
Items: 
Size: 705400 Color: 7
Size: 294334 Color: 10

Bin 502: 269 of cap free
Amount of items: 2
Items: 
Size: 765933 Color: 12
Size: 233799 Color: 14

Bin 503: 270 of cap free
Amount of items: 2
Items: 
Size: 737920 Color: 4
Size: 261811 Color: 14

Bin 504: 270 of cap free
Amount of items: 2
Items: 
Size: 508921 Color: 11
Size: 490810 Color: 19

Bin 505: 270 of cap free
Amount of items: 2
Items: 
Size: 558491 Color: 10
Size: 441240 Color: 7

Bin 506: 271 of cap free
Amount of items: 2
Items: 
Size: 590584 Color: 18
Size: 409146 Color: 14

Bin 507: 271 of cap free
Amount of items: 2
Items: 
Size: 547308 Color: 4
Size: 452422 Color: 8

Bin 508: 273 of cap free
Amount of items: 2
Items: 
Size: 710455 Color: 17
Size: 289273 Color: 11

Bin 509: 277 of cap free
Amount of items: 3
Items: 
Size: 542883 Color: 14
Size: 314685 Color: 4
Size: 142156 Color: 1

Bin 510: 279 of cap free
Amount of items: 3
Items: 
Size: 752423 Color: 1
Size: 129117 Color: 4
Size: 118182 Color: 14

Bin 511: 280 of cap free
Amount of items: 2
Items: 
Size: 696371 Color: 17
Size: 303350 Color: 11

Bin 512: 280 of cap free
Amount of items: 2
Items: 
Size: 599551 Color: 9
Size: 400170 Color: 4

Bin 513: 282 of cap free
Amount of items: 2
Items: 
Size: 690681 Color: 12
Size: 309038 Color: 16

Bin 514: 285 of cap free
Amount of items: 2
Items: 
Size: 773754 Color: 5
Size: 225962 Color: 8

Bin 515: 286 of cap free
Amount of items: 2
Items: 
Size: 507689 Color: 6
Size: 492026 Color: 19

Bin 516: 289 of cap free
Amount of items: 2
Items: 
Size: 685400 Color: 15
Size: 314312 Color: 12

Bin 517: 291 of cap free
Amount of items: 2
Items: 
Size: 620477 Color: 15
Size: 379233 Color: 16

Bin 518: 291 of cap free
Amount of items: 2
Items: 
Size: 743268 Color: 11
Size: 256442 Color: 10

Bin 519: 293 of cap free
Amount of items: 2
Items: 
Size: 633961 Color: 14
Size: 365747 Color: 2

Bin 520: 293 of cap free
Amount of items: 2
Items: 
Size: 609587 Color: 14
Size: 390121 Color: 11

Bin 521: 295 of cap free
Amount of items: 2
Items: 
Size: 566500 Color: 15
Size: 433206 Color: 18

Bin 522: 296 of cap free
Amount of items: 3
Items: 
Size: 637921 Color: 9
Size: 182010 Color: 14
Size: 179774 Color: 8

Bin 523: 297 of cap free
Amount of items: 2
Items: 
Size: 782641 Color: 4
Size: 217063 Color: 15

Bin 524: 298 of cap free
Amount of items: 2
Items: 
Size: 594025 Color: 4
Size: 405678 Color: 14

Bin 525: 299 of cap free
Amount of items: 2
Items: 
Size: 707752 Color: 15
Size: 291950 Color: 6

Bin 526: 299 of cap free
Amount of items: 2
Items: 
Size: 631463 Color: 16
Size: 368239 Color: 18

Bin 527: 301 of cap free
Amount of items: 2
Items: 
Size: 794122 Color: 16
Size: 205578 Color: 13

Bin 528: 306 of cap free
Amount of items: 2
Items: 
Size: 505962 Color: 6
Size: 493733 Color: 14

Bin 529: 307 of cap free
Amount of items: 2
Items: 
Size: 655249 Color: 13
Size: 344445 Color: 19

Bin 530: 309 of cap free
Amount of items: 2
Items: 
Size: 745178 Color: 3
Size: 254514 Color: 15

Bin 531: 312 of cap free
Amount of items: 2
Items: 
Size: 733564 Color: 3
Size: 266125 Color: 9

Bin 532: 313 of cap free
Amount of items: 2
Items: 
Size: 561263 Color: 17
Size: 438425 Color: 12

Bin 533: 313 of cap free
Amount of items: 2
Items: 
Size: 634560 Color: 9
Size: 365128 Color: 5

Bin 534: 314 of cap free
Amount of items: 2
Items: 
Size: 762081 Color: 17
Size: 237606 Color: 19

Bin 535: 315 of cap free
Amount of items: 2
Items: 
Size: 628616 Color: 1
Size: 371070 Color: 9

Bin 536: 316 of cap free
Amount of items: 2
Items: 
Size: 606121 Color: 0
Size: 393564 Color: 10

Bin 537: 320 of cap free
Amount of items: 2
Items: 
Size: 557582 Color: 7
Size: 442099 Color: 5

Bin 538: 321 of cap free
Amount of items: 2
Items: 
Size: 583447 Color: 5
Size: 416233 Color: 11

Bin 539: 322 of cap free
Amount of items: 2
Items: 
Size: 520859 Color: 19
Size: 478820 Color: 5

Bin 540: 323 of cap free
Amount of items: 2
Items: 
Size: 638117 Color: 1
Size: 361561 Color: 2

Bin 541: 323 of cap free
Amount of items: 2
Items: 
Size: 501681 Color: 14
Size: 497997 Color: 0

Bin 542: 325 of cap free
Amount of items: 2
Items: 
Size: 727542 Color: 14
Size: 272134 Color: 2

Bin 543: 326 of cap free
Amount of items: 2
Items: 
Size: 703203 Color: 7
Size: 296472 Color: 18

Bin 544: 327 of cap free
Amount of items: 2
Items: 
Size: 758385 Color: 2
Size: 241289 Color: 11

Bin 545: 327 of cap free
Amount of items: 2
Items: 
Size: 554399 Color: 7
Size: 445275 Color: 12

Bin 546: 329 of cap free
Amount of items: 2
Items: 
Size: 685022 Color: 2
Size: 314650 Color: 17

Bin 547: 331 of cap free
Amount of items: 2
Items: 
Size: 565858 Color: 15
Size: 433812 Color: 9

Bin 548: 332 of cap free
Amount of items: 2
Items: 
Size: 681217 Color: 9
Size: 318452 Color: 16

Bin 549: 332 of cap free
Amount of items: 2
Items: 
Size: 531882 Color: 6
Size: 467787 Color: 15

Bin 550: 332 of cap free
Amount of items: 2
Items: 
Size: 771581 Color: 17
Size: 228088 Color: 10

Bin 551: 333 of cap free
Amount of items: 2
Items: 
Size: 730569 Color: 13
Size: 269099 Color: 7

Bin 552: 334 of cap free
Amount of items: 3
Items: 
Size: 388930 Color: 12
Size: 386523 Color: 3
Size: 224214 Color: 9

Bin 553: 335 of cap free
Amount of items: 2
Items: 
Size: 782068 Color: 2
Size: 217598 Color: 7

Bin 554: 340 of cap free
Amount of items: 2
Items: 
Size: 797418 Color: 19
Size: 202243 Color: 17

Bin 555: 342 of cap free
Amount of items: 3
Items: 
Size: 534804 Color: 12
Size: 278626 Color: 19
Size: 186229 Color: 7

Bin 556: 342 of cap free
Amount of items: 3
Items: 
Size: 509303 Color: 18
Size: 324623 Color: 8
Size: 165733 Color: 17

Bin 557: 345 of cap free
Amount of items: 2
Items: 
Size: 644768 Color: 18
Size: 354888 Color: 0

Bin 558: 346 of cap free
Amount of items: 2
Items: 
Size: 772391 Color: 1
Size: 227264 Color: 16

Bin 559: 347 of cap free
Amount of items: 2
Items: 
Size: 737275 Color: 17
Size: 262379 Color: 2

Bin 560: 348 of cap free
Amount of items: 2
Items: 
Size: 521735 Color: 3
Size: 477918 Color: 11

Bin 561: 350 of cap free
Amount of items: 2
Items: 
Size: 668406 Color: 6
Size: 331245 Color: 8

Bin 562: 352 of cap free
Amount of items: 2
Items: 
Size: 683165 Color: 1
Size: 316484 Color: 7

Bin 563: 352 of cap free
Amount of items: 2
Items: 
Size: 765135 Color: 7
Size: 234514 Color: 12

Bin 564: 354 of cap free
Amount of items: 2
Items: 
Size: 526277 Color: 8
Size: 473370 Color: 4

Bin 565: 359 of cap free
Amount of items: 3
Items: 
Size: 471335 Color: 4
Size: 295147 Color: 3
Size: 233160 Color: 12

Bin 566: 361 of cap free
Amount of items: 2
Items: 
Size: 656595 Color: 1
Size: 343045 Color: 6

Bin 567: 363 of cap free
Amount of items: 2
Items: 
Size: 694147 Color: 1
Size: 305491 Color: 4

Bin 568: 365 of cap free
Amount of items: 2
Items: 
Size: 668939 Color: 14
Size: 330697 Color: 16

Bin 569: 366 of cap free
Amount of items: 2
Items: 
Size: 754384 Color: 9
Size: 245251 Color: 7

Bin 570: 367 of cap free
Amount of items: 2
Items: 
Size: 519737 Color: 15
Size: 479897 Color: 5

Bin 571: 369 of cap free
Amount of items: 2
Items: 
Size: 741607 Color: 18
Size: 258025 Color: 15

Bin 572: 377 of cap free
Amount of items: 2
Items: 
Size: 545684 Color: 1
Size: 453940 Color: 4

Bin 573: 382 of cap free
Amount of items: 2
Items: 
Size: 607320 Color: 6
Size: 392299 Color: 5

Bin 574: 382 of cap free
Amount of items: 2
Items: 
Size: 783697 Color: 9
Size: 215922 Color: 3

Bin 575: 387 of cap free
Amount of items: 2
Items: 
Size: 513025 Color: 4
Size: 486589 Color: 3

Bin 576: 389 of cap free
Amount of items: 2
Items: 
Size: 614303 Color: 1
Size: 385309 Color: 15

Bin 577: 390 of cap free
Amount of items: 3
Items: 
Size: 676042 Color: 7
Size: 189430 Color: 11
Size: 134139 Color: 1

Bin 578: 396 of cap free
Amount of items: 2
Items: 
Size: 582700 Color: 19
Size: 416905 Color: 4

Bin 579: 397 of cap free
Amount of items: 2
Items: 
Size: 618923 Color: 9
Size: 380681 Color: 11

Bin 580: 400 of cap free
Amount of items: 2
Items: 
Size: 714513 Color: 18
Size: 285088 Color: 12

Bin 581: 403 of cap free
Amount of items: 2
Items: 
Size: 797376 Color: 8
Size: 202222 Color: 11

Bin 582: 407 of cap free
Amount of items: 2
Items: 
Size: 526719 Color: 18
Size: 472875 Color: 8

Bin 583: 414 of cap free
Amount of items: 2
Items: 
Size: 637906 Color: 15
Size: 361681 Color: 12

Bin 584: 414 of cap free
Amount of items: 2
Items: 
Size: 720468 Color: 1
Size: 279119 Color: 8

Bin 585: 416 of cap free
Amount of items: 2
Items: 
Size: 533744 Color: 16
Size: 465841 Color: 0

Bin 586: 416 of cap free
Amount of items: 2
Items: 
Size: 517814 Color: 8
Size: 481771 Color: 13

Bin 587: 417 of cap free
Amount of items: 2
Items: 
Size: 584625 Color: 0
Size: 414959 Color: 9

Bin 588: 421 of cap free
Amount of items: 2
Items: 
Size: 795500 Color: 14
Size: 204080 Color: 7

Bin 589: 423 of cap free
Amount of items: 3
Items: 
Size: 744161 Color: 17
Size: 129202 Color: 10
Size: 126215 Color: 11

Bin 590: 427 of cap free
Amount of items: 2
Items: 
Size: 500677 Color: 3
Size: 498897 Color: 17

Bin 591: 428 of cap free
Amount of items: 2
Items: 
Size: 531397 Color: 13
Size: 468176 Color: 15

Bin 592: 433 of cap free
Amount of items: 2
Items: 
Size: 711259 Color: 0
Size: 288309 Color: 8

Bin 593: 435 of cap free
Amount of items: 2
Items: 
Size: 606589 Color: 9
Size: 392977 Color: 0

Bin 594: 436 of cap free
Amount of items: 2
Items: 
Size: 686363 Color: 17
Size: 313202 Color: 7

Bin 595: 438 of cap free
Amount of items: 2
Items: 
Size: 789484 Color: 6
Size: 210079 Color: 15

Bin 596: 438 of cap free
Amount of items: 2
Items: 
Size: 574013 Color: 7
Size: 425550 Color: 11

Bin 597: 441 of cap free
Amount of items: 2
Items: 
Size: 601714 Color: 9
Size: 397846 Color: 6

Bin 598: 443 of cap free
Amount of items: 2
Items: 
Size: 553884 Color: 18
Size: 445674 Color: 13

Bin 599: 444 of cap free
Amount of items: 2
Items: 
Size: 627257 Color: 9
Size: 372300 Color: 10

Bin 600: 448 of cap free
Amount of items: 2
Items: 
Size: 639997 Color: 18
Size: 359556 Color: 16

Bin 601: 449 of cap free
Amount of items: 2
Items: 
Size: 774261 Color: 0
Size: 225291 Color: 13

Bin 602: 450 of cap free
Amount of items: 2
Items: 
Size: 509283 Color: 11
Size: 490268 Color: 17

Bin 603: 452 of cap free
Amount of items: 2
Items: 
Size: 589053 Color: 16
Size: 410496 Color: 3

Bin 604: 452 of cap free
Amount of items: 2
Items: 
Size: 790808 Color: 13
Size: 208741 Color: 9

Bin 605: 454 of cap free
Amount of items: 2
Items: 
Size: 644136 Color: 14
Size: 355411 Color: 16

Bin 606: 454 of cap free
Amount of items: 2
Items: 
Size: 583815 Color: 17
Size: 415732 Color: 9

Bin 607: 455 of cap free
Amount of items: 2
Items: 
Size: 595266 Color: 5
Size: 404280 Color: 19

Bin 608: 460 of cap free
Amount of items: 2
Items: 
Size: 749889 Color: 13
Size: 249652 Color: 10

Bin 609: 461 of cap free
Amount of items: 2
Items: 
Size: 685570 Color: 12
Size: 313970 Color: 1

Bin 610: 474 of cap free
Amount of items: 5
Items: 
Size: 384974 Color: 10
Size: 181537 Color: 13
Size: 163961 Color: 0
Size: 146103 Color: 5
Size: 122952 Color: 17

Bin 611: 474 of cap free
Amount of items: 2
Items: 
Size: 644121 Color: 5
Size: 355406 Color: 12

Bin 612: 478 of cap free
Amount of items: 2
Items: 
Size: 603879 Color: 16
Size: 395644 Color: 6

Bin 613: 481 of cap free
Amount of items: 2
Items: 
Size: 748391 Color: 7
Size: 251129 Color: 0

Bin 614: 487 of cap free
Amount of items: 2
Items: 
Size: 744387 Color: 17
Size: 255127 Color: 15

Bin 615: 488 of cap free
Amount of items: 2
Items: 
Size: 677560 Color: 7
Size: 321953 Color: 8

Bin 616: 490 of cap free
Amount of items: 2
Items: 
Size: 529301 Color: 14
Size: 470210 Color: 0

Bin 617: 498 of cap free
Amount of items: 2
Items: 
Size: 753038 Color: 18
Size: 246465 Color: 2

Bin 618: 501 of cap free
Amount of items: 2
Items: 
Size: 703911 Color: 13
Size: 295589 Color: 16

Bin 619: 502 of cap free
Amount of items: 2
Items: 
Size: 569831 Color: 9
Size: 429668 Color: 7

Bin 620: 507 of cap free
Amount of items: 2
Items: 
Size: 566840 Color: 2
Size: 432654 Color: 16

Bin 621: 509 of cap free
Amount of items: 2
Items: 
Size: 706938 Color: 16
Size: 292554 Color: 10

Bin 622: 509 of cap free
Amount of items: 2
Items: 
Size: 568670 Color: 1
Size: 430822 Color: 17

Bin 623: 515 of cap free
Amount of items: 2
Items: 
Size: 798253 Color: 7
Size: 201233 Color: 13

Bin 624: 515 of cap free
Amount of items: 3
Items: 
Size: 665493 Color: 15
Size: 167158 Color: 16
Size: 166835 Color: 6

Bin 625: 516 of cap free
Amount of items: 2
Items: 
Size: 519075 Color: 19
Size: 480410 Color: 11

Bin 626: 520 of cap free
Amount of items: 2
Items: 
Size: 512038 Color: 5
Size: 487443 Color: 18

Bin 627: 522 of cap free
Amount of items: 2
Items: 
Size: 636647 Color: 9
Size: 362832 Color: 1

Bin 628: 522 of cap free
Amount of items: 2
Items: 
Size: 617267 Color: 17
Size: 382212 Color: 2

Bin 629: 523 of cap free
Amount of items: 2
Items: 
Size: 592841 Color: 17
Size: 406637 Color: 16

Bin 630: 525 of cap free
Amount of items: 2
Items: 
Size: 591578 Color: 4
Size: 407898 Color: 17

Bin 631: 531 of cap free
Amount of items: 2
Items: 
Size: 538836 Color: 0
Size: 460634 Color: 14

Bin 632: 531 of cap free
Amount of items: 2
Items: 
Size: 644620 Color: 3
Size: 354850 Color: 13

Bin 633: 533 of cap free
Amount of items: 2
Items: 
Size: 777552 Color: 10
Size: 221916 Color: 6

Bin 634: 535 of cap free
Amount of items: 2
Items: 
Size: 734626 Color: 5
Size: 264840 Color: 12

Bin 635: 544 of cap free
Amount of items: 2
Items: 
Size: 665690 Color: 1
Size: 333767 Color: 10

Bin 636: 544 of cap free
Amount of items: 2
Items: 
Size: 556441 Color: 9
Size: 443016 Color: 8

Bin 637: 545 of cap free
Amount of items: 2
Items: 
Size: 588834 Color: 19
Size: 410622 Color: 16

Bin 638: 547 of cap free
Amount of items: 2
Items: 
Size: 591356 Color: 1
Size: 408098 Color: 3

Bin 639: 549 of cap free
Amount of items: 2
Items: 
Size: 516383 Color: 18
Size: 483069 Color: 0

Bin 640: 550 of cap free
Amount of items: 2
Items: 
Size: 788044 Color: 13
Size: 211407 Color: 19

Bin 641: 550 of cap free
Amount of items: 2
Items: 
Size: 653651 Color: 3
Size: 345800 Color: 12

Bin 642: 551 of cap free
Amount of items: 2
Items: 
Size: 774964 Color: 7
Size: 224486 Color: 19

Bin 643: 552 of cap free
Amount of items: 2
Items: 
Size: 673759 Color: 1
Size: 325690 Color: 13

Bin 644: 553 of cap free
Amount of items: 2
Items: 
Size: 709444 Color: 13
Size: 290004 Color: 15

Bin 645: 555 of cap free
Amount of items: 2
Items: 
Size: 740685 Color: 10
Size: 258761 Color: 17

Bin 646: 556 of cap free
Amount of items: 2
Items: 
Size: 768825 Color: 17
Size: 230620 Color: 0

Bin 647: 560 of cap free
Amount of items: 2
Items: 
Size: 598062 Color: 14
Size: 401379 Color: 7

Bin 648: 564 of cap free
Amount of items: 2
Items: 
Size: 557723 Color: 6
Size: 441714 Color: 19

Bin 649: 565 of cap free
Amount of items: 2
Items: 
Size: 700156 Color: 6
Size: 299280 Color: 18

Bin 650: 577 of cap free
Amount of items: 2
Items: 
Size: 762792 Color: 15
Size: 236632 Color: 10

Bin 651: 578 of cap free
Amount of items: 2
Items: 
Size: 561661 Color: 6
Size: 437762 Color: 14

Bin 652: 584 of cap free
Amount of items: 2
Items: 
Size: 514461 Color: 7
Size: 484956 Color: 3

Bin 653: 585 of cap free
Amount of items: 2
Items: 
Size: 633887 Color: 8
Size: 365529 Color: 2

Bin 654: 585 of cap free
Amount of items: 2
Items: 
Size: 715284 Color: 13
Size: 284132 Color: 5

Bin 655: 587 of cap free
Amount of items: 2
Items: 
Size: 778886 Color: 4
Size: 220528 Color: 15

Bin 656: 589 of cap free
Amount of items: 2
Items: 
Size: 794668 Color: 10
Size: 204744 Color: 7

Bin 657: 592 of cap free
Amount of items: 2
Items: 
Size: 666059 Color: 17
Size: 333350 Color: 2

Bin 658: 594 of cap free
Amount of items: 2
Items: 
Size: 701474 Color: 1
Size: 297933 Color: 5

Bin 659: 594 of cap free
Amount of items: 2
Items: 
Size: 660752 Color: 2
Size: 338655 Color: 10

Bin 660: 603 of cap free
Amount of items: 2
Items: 
Size: 664549 Color: 15
Size: 334849 Color: 13

Bin 661: 603 of cap free
Amount of items: 2
Items: 
Size: 632266 Color: 4
Size: 367132 Color: 0

Bin 662: 604 of cap free
Amount of items: 2
Items: 
Size: 606569 Color: 19
Size: 392828 Color: 4

Bin 663: 607 of cap free
Amount of items: 2
Items: 
Size: 505896 Color: 1
Size: 493498 Color: 8

Bin 664: 608 of cap free
Amount of items: 2
Items: 
Size: 635116 Color: 12
Size: 364277 Color: 2

Bin 665: 608 of cap free
Amount of items: 2
Items: 
Size: 678388 Color: 7
Size: 321005 Color: 6

Bin 666: 616 of cap free
Amount of items: 2
Items: 
Size: 585625 Color: 5
Size: 413760 Color: 4

Bin 667: 619 of cap free
Amount of items: 2
Items: 
Size: 652271 Color: 1
Size: 347111 Color: 14

Bin 668: 620 of cap free
Amount of items: 2
Items: 
Size: 543947 Color: 14
Size: 455434 Color: 16

Bin 669: 622 of cap free
Amount of items: 2
Items: 
Size: 784194 Color: 17
Size: 215185 Color: 5

Bin 670: 623 of cap free
Amount of items: 2
Items: 
Size: 718156 Color: 15
Size: 281222 Color: 7

Bin 671: 625 of cap free
Amount of items: 2
Items: 
Size: 724327 Color: 10
Size: 275049 Color: 7

Bin 672: 626 of cap free
Amount of items: 2
Items: 
Size: 533534 Color: 19
Size: 465841 Color: 7

Bin 673: 628 of cap free
Amount of items: 2
Items: 
Size: 797160 Color: 4
Size: 202213 Color: 16

Bin 674: 634 of cap free
Amount of items: 2
Items: 
Size: 678332 Color: 8
Size: 321035 Color: 18

Bin 675: 636 of cap free
Amount of items: 2
Items: 
Size: 720460 Color: 13
Size: 278905 Color: 9

Bin 676: 637 of cap free
Amount of items: 2
Items: 
Size: 504438 Color: 4
Size: 494926 Color: 19

Bin 677: 637 of cap free
Amount of items: 2
Items: 
Size: 589513 Color: 16
Size: 409851 Color: 6

Bin 678: 638 of cap free
Amount of items: 2
Items: 
Size: 746418 Color: 17
Size: 252945 Color: 0

Bin 679: 639 of cap free
Amount of items: 2
Items: 
Size: 636842 Color: 9
Size: 362520 Color: 18

Bin 680: 646 of cap free
Amount of items: 2
Items: 
Size: 780932 Color: 13
Size: 218423 Color: 8

Bin 681: 646 of cap free
Amount of items: 2
Items: 
Size: 708569 Color: 10
Size: 290786 Color: 16

Bin 682: 648 of cap free
Amount of items: 3
Items: 
Size: 637786 Color: 7
Size: 180907 Color: 17
Size: 180660 Color: 16

Bin 683: 648 of cap free
Amount of items: 2
Items: 
Size: 572484 Color: 15
Size: 426869 Color: 8

Bin 684: 649 of cap free
Amount of items: 2
Items: 
Size: 518391 Color: 1
Size: 480961 Color: 13

Bin 685: 657 of cap free
Amount of items: 2
Items: 
Size: 644071 Color: 19
Size: 355273 Color: 3

Bin 686: 679 of cap free
Amount of items: 2
Items: 
Size: 541193 Color: 0
Size: 458129 Color: 16

Bin 687: 684 of cap free
Amount of items: 2
Items: 
Size: 526490 Color: 1
Size: 472827 Color: 10

Bin 688: 687 of cap free
Amount of items: 2
Items: 
Size: 692045 Color: 18
Size: 307269 Color: 10

Bin 689: 690 of cap free
Amount of items: 2
Items: 
Size: 769991 Color: 8
Size: 229320 Color: 16

Bin 690: 695 of cap free
Amount of items: 3
Items: 
Size: 584530 Color: 18
Size: 241078 Color: 15
Size: 173698 Color: 11

Bin 691: 712 of cap free
Amount of items: 2
Items: 
Size: 524076 Color: 14
Size: 475213 Color: 18

Bin 692: 715 of cap free
Amount of items: 2
Items: 
Size: 625208 Color: 13
Size: 374078 Color: 11

Bin 693: 717 of cap free
Amount of items: 2
Items: 
Size: 658240 Color: 13
Size: 341044 Color: 18

Bin 694: 728 of cap free
Amount of items: 2
Items: 
Size: 542544 Color: 17
Size: 456729 Color: 9

Bin 695: 733 of cap free
Amount of items: 2
Items: 
Size: 594015 Color: 8
Size: 405253 Color: 5

Bin 696: 736 of cap free
Amount of items: 2
Items: 
Size: 624228 Color: 19
Size: 375037 Color: 10

Bin 697: 745 of cap free
Amount of items: 2
Items: 
Size: 785356 Color: 2
Size: 213900 Color: 4

Bin 698: 751 of cap free
Amount of items: 4
Items: 
Size: 550098 Color: 7
Size: 198601 Color: 2
Size: 133831 Color: 12
Size: 116720 Color: 9

Bin 699: 762 of cap free
Amount of items: 2
Items: 
Size: 521560 Color: 3
Size: 477679 Color: 11

Bin 700: 767 of cap free
Amount of items: 2
Items: 
Size: 768697 Color: 9
Size: 230537 Color: 4

Bin 701: 768 of cap free
Amount of items: 2
Items: 
Size: 664476 Color: 5
Size: 334757 Color: 13

Bin 702: 773 of cap free
Amount of items: 2
Items: 
Size: 764011 Color: 15
Size: 235217 Color: 5

Bin 703: 778 of cap free
Amount of items: 2
Items: 
Size: 570524 Color: 6
Size: 428699 Color: 16

Bin 704: 779 of cap free
Amount of items: 2
Items: 
Size: 751153 Color: 13
Size: 248069 Color: 11

Bin 705: 780 of cap free
Amount of items: 3
Items: 
Size: 716638 Color: 9
Size: 157335 Color: 10
Size: 125248 Color: 8

Bin 706: 788 of cap free
Amount of items: 2
Items: 
Size: 629525 Color: 16
Size: 369688 Color: 4

Bin 707: 798 of cap free
Amount of items: 2
Items: 
Size: 785402 Color: 0
Size: 213801 Color: 2

Bin 708: 802 of cap free
Amount of items: 2
Items: 
Size: 563812 Color: 18
Size: 435387 Color: 13

Bin 709: 804 of cap free
Amount of items: 2
Items: 
Size: 553387 Color: 2
Size: 445810 Color: 18

Bin 710: 811 of cap free
Amount of items: 3
Items: 
Size: 792288 Color: 14
Size: 104188 Color: 15
Size: 102714 Color: 15

Bin 711: 819 of cap free
Amount of items: 2
Items: 
Size: 586981 Color: 10
Size: 412201 Color: 19

Bin 712: 840 of cap free
Amount of items: 2
Items: 
Size: 680886 Color: 11
Size: 318275 Color: 14

Bin 713: 845 of cap free
Amount of items: 2
Items: 
Size: 524058 Color: 13
Size: 475098 Color: 16

Bin 714: 854 of cap free
Amount of items: 2
Items: 
Size: 760025 Color: 2
Size: 239122 Color: 7

Bin 715: 856 of cap free
Amount of items: 2
Items: 
Size: 721746 Color: 6
Size: 277399 Color: 9

Bin 716: 866 of cap free
Amount of items: 2
Items: 
Size: 780391 Color: 14
Size: 218744 Color: 17

Bin 717: 867 of cap free
Amount of items: 2
Items: 
Size: 795339 Color: 9
Size: 203795 Color: 7

Bin 718: 868 of cap free
Amount of items: 2
Items: 
Size: 527851 Color: 16
Size: 471282 Color: 2

Bin 719: 874 of cap free
Amount of items: 2
Items: 
Size: 502166 Color: 7
Size: 496961 Color: 12

Bin 720: 927 of cap free
Amount of items: 2
Items: 
Size: 776211 Color: 16
Size: 222863 Color: 10

Bin 721: 935 of cap free
Amount of items: 2
Items: 
Size: 690441 Color: 1
Size: 308625 Color: 17

Bin 722: 936 of cap free
Amount of items: 2
Items: 
Size: 729446 Color: 8
Size: 269619 Color: 7

Bin 723: 939 of cap free
Amount of items: 2
Items: 
Size: 504604 Color: 12
Size: 494458 Color: 11

Bin 724: 944 of cap free
Amount of items: 2
Items: 
Size: 646584 Color: 15
Size: 352473 Color: 18

Bin 725: 949 of cap free
Amount of items: 2
Items: 
Size: 652181 Color: 11
Size: 346871 Color: 12

Bin 726: 956 of cap free
Amount of items: 2
Items: 
Size: 548181 Color: 17
Size: 450864 Color: 0

Bin 727: 964 of cap free
Amount of items: 2
Items: 
Size: 557951 Color: 19
Size: 441086 Color: 9

Bin 728: 972 of cap free
Amount of items: 2
Items: 
Size: 603403 Color: 10
Size: 395626 Color: 1

Bin 729: 980 of cap free
Amount of items: 2
Items: 
Size: 746640 Color: 10
Size: 252381 Color: 1

Bin 730: 988 of cap free
Amount of items: 2
Items: 
Size: 529344 Color: 12
Size: 469669 Color: 16

Bin 731: 990 of cap free
Amount of items: 2
Items: 
Size: 773271 Color: 8
Size: 225740 Color: 0

Bin 732: 996 of cap free
Amount of items: 2
Items: 
Size: 690366 Color: 14
Size: 308639 Color: 4

Bin 733: 1010 of cap free
Amount of items: 2
Items: 
Size: 687972 Color: 15
Size: 311019 Color: 8

Bin 734: 1016 of cap free
Amount of items: 2
Items: 
Size: 592768 Color: 2
Size: 406217 Color: 10

Bin 735: 1021 of cap free
Amount of items: 2
Items: 
Size: 599160 Color: 3
Size: 399820 Color: 16

Bin 736: 1022 of cap free
Amount of items: 2
Items: 
Size: 724157 Color: 5
Size: 274822 Color: 14

Bin 737: 1031 of cap free
Amount of items: 2
Items: 
Size: 555091 Color: 13
Size: 443879 Color: 11

Bin 738: 1039 of cap free
Amount of items: 2
Items: 
Size: 596859 Color: 6
Size: 402103 Color: 18

Bin 739: 1043 of cap free
Amount of items: 2
Items: 
Size: 698258 Color: 15
Size: 300700 Color: 7

Bin 740: 1047 of cap free
Amount of items: 2
Items: 
Size: 540899 Color: 18
Size: 458055 Color: 8

Bin 741: 1065 of cap free
Amount of items: 2
Items: 
Size: 666735 Color: 2
Size: 332201 Color: 3

Bin 742: 1069 of cap free
Amount of items: 2
Items: 
Size: 639397 Color: 5
Size: 359535 Color: 19

Bin 743: 1075 of cap free
Amount of items: 2
Items: 
Size: 761328 Color: 13
Size: 237598 Color: 12

Bin 744: 1101 of cap free
Amount of items: 2
Items: 
Size: 746614 Color: 4
Size: 252286 Color: 15

Bin 745: 1110 of cap free
Amount of items: 2
Items: 
Size: 573436 Color: 4
Size: 425455 Color: 18

Bin 746: 1112 of cap free
Amount of items: 2
Items: 
Size: 563529 Color: 12
Size: 435360 Color: 15

Bin 747: 1120 of cap free
Amount of items: 2
Items: 
Size: 716858 Color: 9
Size: 282023 Color: 14

Bin 748: 1123 of cap free
Amount of items: 2
Items: 
Size: 700058 Color: 12
Size: 298820 Color: 13

Bin 749: 1127 of cap free
Amount of items: 2
Items: 
Size: 668387 Color: 0
Size: 330487 Color: 7

Bin 750: 1142 of cap free
Amount of items: 2
Items: 
Size: 647978 Color: 7
Size: 350881 Color: 15

Bin 751: 1157 of cap free
Amount of items: 2
Items: 
Size: 502269 Color: 19
Size: 496575 Color: 14

Bin 752: 1167 of cap free
Amount of items: 2
Items: 
Size: 611298 Color: 15
Size: 387536 Color: 7

Bin 753: 1168 of cap free
Amount of items: 2
Items: 
Size: 697025 Color: 3
Size: 301808 Color: 7

Bin 754: 1169 of cap free
Amount of items: 2
Items: 
Size: 780259 Color: 17
Size: 218573 Color: 18

Bin 755: 1170 of cap free
Amount of items: 2
Items: 
Size: 769730 Color: 19
Size: 229101 Color: 14

Bin 756: 1183 of cap free
Amount of items: 2
Items: 
Size: 550836 Color: 1
Size: 447982 Color: 19

Bin 757: 1184 of cap free
Amount of items: 2
Items: 
Size: 513923 Color: 16
Size: 484894 Color: 9

Bin 758: 1197 of cap free
Amount of items: 2
Items: 
Size: 526199 Color: 12
Size: 472605 Color: 10

Bin 759: 1209 of cap free
Amount of items: 2
Items: 
Size: 580474 Color: 6
Size: 418318 Color: 14

Bin 760: 1209 of cap free
Amount of items: 2
Items: 
Size: 511162 Color: 3
Size: 487630 Color: 7

Bin 761: 1214 of cap free
Amount of items: 2
Items: 
Size: 521310 Color: 5
Size: 477477 Color: 7

Bin 762: 1220 of cap free
Amount of items: 2
Items: 
Size: 591299 Color: 8
Size: 407482 Color: 9

Bin 763: 1225 of cap free
Amount of items: 2
Items: 
Size: 533044 Color: 15
Size: 465732 Color: 2

Bin 764: 1228 of cap free
Amount of items: 2
Items: 
Size: 695785 Color: 14
Size: 302988 Color: 8

Bin 765: 1239 of cap free
Amount of items: 2
Items: 
Size: 606537 Color: 18
Size: 392225 Color: 7

Bin 766: 1244 of cap free
Amount of items: 2
Items: 
Size: 706368 Color: 4
Size: 292389 Color: 16

Bin 767: 1250 of cap free
Amount of items: 2
Items: 
Size: 647914 Color: 18
Size: 350837 Color: 7

Bin 768: 1259 of cap free
Amount of items: 2
Items: 
Size: 588897 Color: 16
Size: 409845 Color: 4

Bin 769: 1265 of cap free
Amount of items: 2
Items: 
Size: 510696 Color: 0
Size: 488040 Color: 15

Bin 770: 1267 of cap free
Amount of items: 2
Items: 
Size: 660645 Color: 2
Size: 338089 Color: 1

Bin 771: 1276 of cap free
Amount of items: 2
Items: 
Size: 677029 Color: 15
Size: 321696 Color: 8

Bin 772: 1276 of cap free
Amount of items: 2
Items: 
Size: 650706 Color: 2
Size: 348019 Color: 17

Bin 773: 1293 of cap free
Amount of items: 2
Items: 
Size: 797967 Color: 13
Size: 200741 Color: 18

Bin 774: 1293 of cap free
Amount of items: 2
Items: 
Size: 606328 Color: 5
Size: 392380 Color: 7

Bin 775: 1302 of cap free
Amount of items: 2
Items: 
Size: 523829 Color: 12
Size: 474870 Color: 15

Bin 776: 1304 of cap free
Amount of items: 2
Items: 
Size: 627779 Color: 7
Size: 370918 Color: 6

Bin 777: 1324 of cap free
Amount of items: 2
Items: 
Size: 620408 Color: 17
Size: 378269 Color: 2

Bin 778: 1324 of cap free
Amount of items: 2
Items: 
Size: 593940 Color: 5
Size: 404737 Color: 10

Bin 779: 1342 of cap free
Amount of items: 2
Items: 
Size: 749513 Color: 15
Size: 249146 Color: 13

Bin 780: 1350 of cap free
Amount of items: 2
Items: 
Size: 505736 Color: 16
Size: 492915 Color: 5

Bin 781: 1351 of cap free
Amount of items: 2
Items: 
Size: 510661 Color: 18
Size: 487989 Color: 6

Bin 782: 1353 of cap free
Amount of items: 2
Items: 
Size: 510757 Color: 3
Size: 487891 Color: 4

Bin 783: 1359 of cap free
Amount of items: 2
Items: 
Size: 673396 Color: 14
Size: 325246 Color: 9

Bin 784: 1363 of cap free
Amount of items: 2
Items: 
Size: 787253 Color: 1
Size: 211385 Color: 16

Bin 785: 1372 of cap free
Amount of items: 2
Items: 
Size: 727071 Color: 0
Size: 271558 Color: 12

Bin 786: 1402 of cap free
Amount of items: 2
Items: 
Size: 543454 Color: 13
Size: 455145 Color: 17

Bin 787: 1424 of cap free
Amount of items: 2
Items: 
Size: 792281 Color: 8
Size: 206296 Color: 9

Bin 788: 1440 of cap free
Amount of items: 2
Items: 
Size: 731875 Color: 4
Size: 266686 Color: 17

Bin 789: 1456 of cap free
Amount of items: 2
Items: 
Size: 530970 Color: 18
Size: 467575 Color: 0

Bin 790: 1507 of cap free
Amount of items: 2
Items: 
Size: 746416 Color: 14
Size: 252078 Color: 8

Bin 791: 1581 of cap free
Amount of items: 2
Items: 
Size: 602976 Color: 18
Size: 395444 Color: 2

Bin 792: 1583 of cap free
Amount of items: 2
Items: 
Size: 535495 Color: 4
Size: 462923 Color: 1

Bin 793: 1594 of cap free
Amount of items: 2
Items: 
Size: 602972 Color: 6
Size: 395435 Color: 5

Bin 794: 1594 of cap free
Amount of items: 2
Items: 
Size: 547798 Color: 11
Size: 450609 Color: 19

Bin 795: 1608 of cap free
Amount of items: 2
Items: 
Size: 737139 Color: 12
Size: 261254 Color: 4

Bin 796: 1629 of cap free
Amount of items: 2
Items: 
Size: 499539 Color: 0
Size: 498833 Color: 1

Bin 797: 1640 of cap free
Amount of items: 2
Items: 
Size: 598989 Color: 7
Size: 399372 Color: 16

Bin 798: 1660 of cap free
Amount of items: 2
Items: 
Size: 754793 Color: 5
Size: 243548 Color: 11

Bin 799: 1708 of cap free
Amount of items: 2
Items: 
Size: 535362 Color: 9
Size: 462931 Color: 18

Bin 800: 1724 of cap free
Amount of items: 2
Items: 
Size: 612510 Color: 1
Size: 385767 Color: 15

Bin 801: 1734 of cap free
Amount of items: 3
Items: 
Size: 629655 Color: 3
Size: 220175 Color: 8
Size: 148437 Color: 5

Bin 802: 1734 of cap free
Amount of items: 2
Items: 
Size: 557337 Color: 9
Size: 440930 Color: 5

Bin 803: 1800 of cap free
Amount of items: 2
Items: 
Size: 542145 Color: 16
Size: 456056 Color: 8

Bin 804: 1869 of cap free
Amount of items: 2
Items: 
Size: 787271 Color: 6
Size: 210861 Color: 7

Bin 805: 1899 of cap free
Amount of items: 2
Items: 
Size: 737052 Color: 15
Size: 261050 Color: 14

Bin 806: 1929 of cap free
Amount of items: 2
Items: 
Size: 573256 Color: 15
Size: 424816 Color: 8

Bin 807: 1968 of cap free
Amount of items: 2
Items: 
Size: 660155 Color: 0
Size: 337878 Color: 15

Bin 808: 1987 of cap free
Amount of items: 2
Items: 
Size: 752231 Color: 16
Size: 245783 Color: 13

Bin 809: 2018 of cap free
Amount of items: 2
Items: 
Size: 595835 Color: 9
Size: 402148 Color: 6

Bin 810: 2020 of cap free
Amount of items: 2
Items: 
Size: 677659 Color: 7
Size: 320322 Color: 2

Bin 811: 2021 of cap free
Amount of items: 2
Items: 
Size: 663301 Color: 7
Size: 334679 Color: 8

Bin 812: 2031 of cap free
Amount of items: 2
Items: 
Size: 632976 Color: 19
Size: 364994 Color: 2

Bin 813: 2049 of cap free
Amount of items: 2
Items: 
Size: 767666 Color: 8
Size: 230286 Color: 4

Bin 814: 2050 of cap free
Amount of items: 2
Items: 
Size: 560469 Color: 14
Size: 437482 Color: 10

Bin 815: 2094 of cap free
Amount of items: 3
Items: 
Size: 694115 Color: 16
Size: 189864 Color: 1
Size: 113928 Color: 12

Bin 816: 2094 of cap free
Amount of items: 2
Items: 
Size: 588255 Color: 9
Size: 409652 Color: 12

Bin 817: 2128 of cap free
Amount of items: 2
Items: 
Size: 515148 Color: 4
Size: 482725 Color: 15

Bin 818: 2131 of cap free
Amount of items: 2
Items: 
Size: 691449 Color: 4
Size: 306421 Color: 6

Bin 819: 2178 of cap free
Amount of items: 2
Items: 
Size: 639053 Color: 19
Size: 358770 Color: 16

Bin 820: 2189 of cap free
Amount of items: 2
Items: 
Size: 629270 Color: 19
Size: 368542 Color: 3

Bin 821: 2202 of cap free
Amount of items: 2
Items: 
Size: 760959 Color: 3
Size: 236840 Color: 16

Bin 822: 2207 of cap free
Amount of items: 2
Items: 
Size: 677416 Color: 14
Size: 320378 Color: 15

Bin 823: 2213 of cap free
Amount of items: 2
Items: 
Size: 764003 Color: 5
Size: 233785 Color: 8

Bin 824: 2223 of cap free
Amount of items: 2
Items: 
Size: 504322 Color: 0
Size: 493456 Color: 4

Bin 825: 2232 of cap free
Amount of items: 2
Items: 
Size: 616307 Color: 16
Size: 381462 Color: 4

Bin 826: 2238 of cap free
Amount of items: 2
Items: 
Size: 670547 Color: 5
Size: 327216 Color: 8

Bin 827: 2246 of cap free
Amount of items: 2
Items: 
Size: 736950 Color: 14
Size: 260805 Color: 12

Bin 828: 2271 of cap free
Amount of items: 2
Items: 
Size: 787047 Color: 16
Size: 210683 Color: 3

Bin 829: 2275 of cap free
Amount of items: 2
Items: 
Size: 761083 Color: 16
Size: 236643 Color: 19

Bin 830: 2369 of cap free
Amount of items: 2
Items: 
Size: 565047 Color: 14
Size: 432585 Color: 19

Bin 831: 2444 of cap free
Amount of items: 2
Items: 
Size: 736877 Color: 17
Size: 260680 Color: 14

Bin 832: 2491 of cap free
Amount of items: 2
Items: 
Size: 639037 Color: 18
Size: 358473 Color: 17

Bin 833: 2520 of cap free
Amount of items: 3
Items: 
Size: 649979 Color: 2
Size: 196371 Color: 10
Size: 151131 Color: 2

Bin 834: 2533 of cap free
Amount of items: 2
Items: 
Size: 670312 Color: 0
Size: 327156 Color: 3

Bin 835: 2540 of cap free
Amount of items: 2
Items: 
Size: 591098 Color: 8
Size: 406363 Color: 2

Bin 836: 2547 of cap free
Amount of items: 2
Items: 
Size: 606253 Color: 8
Size: 391201 Color: 3

Bin 837: 2619 of cap free
Amount of items: 2
Items: 
Size: 650520 Color: 7
Size: 346862 Color: 5

Bin 838: 2624 of cap free
Amount of items: 2
Items: 
Size: 602777 Color: 18
Size: 394600 Color: 9

Bin 839: 2636 of cap free
Amount of items: 2
Items: 
Size: 523005 Color: 16
Size: 474360 Color: 8

Bin 840: 2672 of cap free
Amount of items: 2
Items: 
Size: 667846 Color: 2
Size: 329483 Color: 1

Bin 841: 2706 of cap free
Amount of items: 2
Items: 
Size: 598047 Color: 5
Size: 399248 Color: 0

Bin 842: 2753 of cap free
Amount of items: 2
Items: 
Size: 787725 Color: 19
Size: 209523 Color: 16

Bin 843: 2756 of cap free
Amount of items: 2
Items: 
Size: 629445 Color: 3
Size: 367800 Color: 16

Bin 844: 2792 of cap free
Amount of items: 2
Items: 
Size: 736511 Color: 18
Size: 260698 Color: 11

Bin 845: 2812 of cap free
Amount of items: 2
Items: 
Size: 679517 Color: 13
Size: 317672 Color: 11

Bin 846: 2852 of cap free
Amount of items: 2
Items: 
Size: 695286 Color: 9
Size: 301863 Color: 16

Bin 847: 2852 of cap free
Amount of items: 2
Items: 
Size: 751075 Color: 0
Size: 246074 Color: 14

Bin 848: 2889 of cap free
Amount of items: 2
Items: 
Size: 736695 Color: 11
Size: 260417 Color: 0

Bin 849: 2959 of cap free
Amount of items: 2
Items: 
Size: 518268 Color: 14
Size: 478774 Color: 2

Bin 850: 2975 of cap free
Amount of items: 2
Items: 
Size: 659675 Color: 7
Size: 337351 Color: 11

Bin 851: 3083 of cap free
Amount of items: 3
Items: 
Size: 498686 Color: 8
Size: 358856 Color: 11
Size: 139376 Color: 6

Bin 852: 3092 of cap free
Amount of items: 2
Items: 
Size: 796773 Color: 14
Size: 200136 Color: 16

Bin 853: 3093 of cap free
Amount of items: 2
Items: 
Size: 718127 Color: 14
Size: 278781 Color: 0

Bin 854: 3104 of cap free
Amount of items: 2
Items: 
Size: 642452 Color: 5
Size: 354445 Color: 18

Bin 855: 3146 of cap free
Amount of items: 2
Items: 
Size: 730552 Color: 9
Size: 266303 Color: 2

Bin 856: 3161 of cap free
Amount of items: 2
Items: 
Size: 556167 Color: 17
Size: 440673 Color: 5

Bin 857: 3189 of cap free
Amount of items: 2
Items: 
Size: 606142 Color: 12
Size: 390670 Color: 8

Bin 858: 3211 of cap free
Amount of items: 2
Items: 
Size: 619471 Color: 5
Size: 377319 Color: 19

Bin 859: 3220 of cap free
Amount of items: 2
Items: 
Size: 580020 Color: 11
Size: 416761 Color: 14

Bin 860: 3238 of cap free
Amount of items: 2
Items: 
Size: 590581 Color: 15
Size: 406182 Color: 10

Bin 861: 3284 of cap free
Amount of items: 2
Items: 
Size: 642339 Color: 10
Size: 354378 Color: 5

Bin 862: 3439 of cap free
Amount of items: 2
Items: 
Size: 729982 Color: 4
Size: 266580 Color: 11

Bin 863: 3538 of cap free
Amount of items: 2
Items: 
Size: 530954 Color: 19
Size: 465509 Color: 12

Bin 864: 3590 of cap free
Amount of items: 2
Items: 
Size: 503818 Color: 9
Size: 492593 Color: 3

Bin 865: 3688 of cap free
Amount of items: 2
Items: 
Size: 605753 Color: 13
Size: 390560 Color: 12

Bin 866: 3743 of cap free
Amount of items: 2
Items: 
Size: 605663 Color: 7
Size: 390595 Color: 4

Bin 867: 3807 of cap free
Amount of items: 2
Items: 
Size: 687404 Color: 1
Size: 308790 Color: 14

Bin 868: 3830 of cap free
Amount of items: 2
Items: 
Size: 641988 Color: 7
Size: 354183 Color: 4

Bin 869: 3848 of cap free
Amount of items: 2
Items: 
Size: 796371 Color: 4
Size: 199782 Color: 10

Bin 870: 3862 of cap free
Amount of items: 2
Items: 
Size: 786770 Color: 16
Size: 209369 Color: 17

Bin 871: 3909 of cap free
Amount of items: 2
Items: 
Size: 508697 Color: 16
Size: 487395 Color: 11

Bin 872: 3937 of cap free
Amount of items: 2
Items: 
Size: 735829 Color: 16
Size: 260235 Color: 19

Bin 873: 4019 of cap free
Amount of items: 2
Items: 
Size: 797546 Color: 11
Size: 198436 Color: 16

Bin 874: 4195 of cap free
Amount of items: 2
Items: 
Size: 530589 Color: 11
Size: 465217 Color: 14

Bin 875: 4279 of cap free
Amount of items: 2
Items: 
Size: 523311 Color: 8
Size: 472411 Color: 19

Bin 876: 4616 of cap free
Amount of items: 2
Items: 
Size: 735251 Color: 5
Size: 260134 Color: 16

Bin 877: 4634 of cap free
Amount of items: 3
Items: 
Size: 585325 Color: 4
Size: 259671 Color: 9
Size: 150371 Color: 19

Bin 878: 4647 of cap free
Amount of items: 2
Items: 
Size: 605545 Color: 19
Size: 389809 Color: 11

Bin 879: 4686 of cap free
Amount of items: 2
Items: 
Size: 751198 Color: 0
Size: 244117 Color: 4

Bin 880: 4822 of cap free
Amount of items: 2
Items: 
Size: 578542 Color: 8
Size: 416637 Color: 9

Bin 881: 4960 of cap free
Amount of items: 2
Items: 
Size: 572174 Color: 13
Size: 422867 Color: 10

Bin 882: 5050 of cap free
Amount of items: 2
Items: 
Size: 618580 Color: 1
Size: 376371 Color: 17

Bin 883: 5221 of cap free
Amount of items: 2
Items: 
Size: 517321 Color: 19
Size: 477459 Color: 14

Bin 884: 5454 of cap free
Amount of items: 2
Items: 
Size: 657279 Color: 5
Size: 337268 Color: 10

Bin 885: 5612 of cap free
Amount of items: 2
Items: 
Size: 728887 Color: 11
Size: 265502 Color: 4

Bin 886: 5616 of cap free
Amount of items: 2
Items: 
Size: 735013 Color: 12
Size: 259372 Color: 5

Bin 887: 6270 of cap free
Amount of items: 2
Items: 
Size: 741485 Color: 15
Size: 252246 Color: 4

Bin 888: 6651 of cap free
Amount of items: 2
Items: 
Size: 571854 Color: 15
Size: 421496 Color: 16

Bin 889: 6755 of cap free
Amount of items: 2
Items: 
Size: 515953 Color: 15
Size: 477293 Color: 9

Bin 890: 6885 of cap free
Amount of items: 2
Items: 
Size: 571553 Color: 3
Size: 421563 Color: 8

Bin 891: 7104 of cap free
Amount of items: 2
Items: 
Size: 655839 Color: 11
Size: 337058 Color: 1

Bin 892: 7150 of cap free
Amount of items: 2
Items: 
Size: 783461 Color: 9
Size: 209390 Color: 6

Bin 893: 7688 of cap free
Amount of items: 2
Items: 
Size: 602437 Color: 13
Size: 389876 Color: 11

Bin 894: 7769 of cap free
Amount of items: 2
Items: 
Size: 560307 Color: 4
Size: 431925 Color: 12

Bin 895: 7829 of cap free
Amount of items: 2
Items: 
Size: 559999 Color: 6
Size: 432173 Color: 17

Bin 896: 7897 of cap free
Amount of items: 2
Items: 
Size: 602466 Color: 14
Size: 389638 Color: 18

Bin 897: 8575 of cap free
Amount of items: 2
Items: 
Size: 657949 Color: 14
Size: 333477 Color: 17

Bin 898: 9884 of cap free
Amount of items: 2
Items: 
Size: 559831 Color: 8
Size: 430286 Color: 19

Bin 899: 10446 of cap free
Amount of items: 2
Items: 
Size: 590879 Color: 10
Size: 398676 Color: 4

Bin 900: 11129 of cap free
Amount of items: 2
Items: 
Size: 590365 Color: 17
Size: 398507 Color: 10

Bin 901: 11410 of cap free
Amount of items: 2
Items: 
Size: 783000 Color: 10
Size: 205591 Color: 15

Bin 902: 18257 of cap free
Amount of items: 2
Items: 
Size: 649432 Color: 0
Size: 332312 Color: 14

Bin 903: 35119 of cap free
Amount of items: 2
Items: 
Size: 717563 Color: 7
Size: 247319 Color: 5

Bin 904: 57352 of cap free
Amount of items: 2
Items: 
Size: 509861 Color: 8
Size: 432788 Color: 17

Bin 905: 106800 of cap free
Amount of items: 3
Items: 
Size: 504064 Color: 2
Size: 209846 Color: 14
Size: 179291 Color: 3

Total size: 904149250
Total free space: 851655

