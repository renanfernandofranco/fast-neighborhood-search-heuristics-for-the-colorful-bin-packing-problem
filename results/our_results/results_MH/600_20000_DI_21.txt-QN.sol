Capicity Bin: 15792
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 467
Size: 2301 Color: 281
Size: 1931 Color: 254

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 469
Size: 3528 Color: 338
Size: 560 Color: 119

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11706 Color: 470
Size: 3518 Color: 337
Size: 568 Color: 123

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12156 Color: 482
Size: 3240 Color: 328
Size: 396 Color: 70

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12188 Color: 484
Size: 3188 Color: 327
Size: 416 Color: 83

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12207 Color: 485
Size: 2137 Color: 273
Size: 1448 Color: 209

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 488
Size: 2648 Color: 303
Size: 848 Color: 159

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 497
Size: 3036 Color: 322
Size: 280 Color: 22

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12608 Color: 500
Size: 2986 Color: 319
Size: 198 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 502
Size: 1628 Color: 225
Size: 1548 Color: 215

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12709 Color: 505
Size: 2595 Color: 300
Size: 488 Color: 106

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 509
Size: 2376 Color: 290
Size: 592 Color: 126

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12846 Color: 510
Size: 2750 Color: 305
Size: 196 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13001 Color: 518
Size: 2313 Color: 282
Size: 478 Color: 103

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13009 Color: 519
Size: 2321 Color: 284
Size: 462 Color: 96

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13049 Color: 524
Size: 2373 Color: 289
Size: 370 Color: 61

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13052 Color: 525
Size: 2260 Color: 277
Size: 480 Color: 104

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13082 Color: 526
Size: 1574 Color: 219
Size: 1136 Color: 186

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 527
Size: 2322 Color: 285
Size: 386 Color: 67

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 530
Size: 2104 Color: 270
Size: 464 Color: 98

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13245 Color: 532
Size: 1747 Color: 234
Size: 800 Color: 155

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 533
Size: 1678 Color: 229
Size: 860 Color: 163

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 536
Size: 2076 Color: 266
Size: 408 Color: 78

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 539
Size: 1464 Color: 210
Size: 992 Color: 177

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 541
Size: 1900 Color: 250
Size: 524 Color: 111

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 542
Size: 2056 Color: 263
Size: 364 Color: 55

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13439 Color: 546
Size: 1601 Color: 222
Size: 752 Color: 152

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13510 Color: 551
Size: 1518 Color: 213
Size: 764 Color: 153

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 553
Size: 1859 Color: 246
Size: 394 Color: 69

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 554
Size: 1608 Color: 224
Size: 640 Color: 136

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13548 Color: 555
Size: 1312 Color: 198
Size: 932 Color: 172

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13563 Color: 556
Size: 1749 Color: 235
Size: 480 Color: 105

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13566 Color: 557
Size: 1562 Color: 216
Size: 664 Color: 139

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13582 Color: 558
Size: 1784 Color: 241
Size: 426 Color: 85

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 562
Size: 1754 Color: 238
Size: 382 Color: 65

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 570
Size: 1140 Color: 187
Size: 896 Color: 171

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 572
Size: 1152 Color: 189
Size: 858 Color: 162

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13806 Color: 574
Size: 1658 Color: 227
Size: 328 Color: 41

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13844 Color: 576
Size: 1524 Color: 214
Size: 424 Color: 84

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13864 Color: 577
Size: 1312 Color: 196
Size: 616 Color: 132

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 578
Size: 1628 Color: 226
Size: 296 Color: 27

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 581
Size: 1572 Color: 218
Size: 324 Color: 40

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 584
Size: 1404 Color: 207
Size: 470 Color: 101

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13974 Color: 587
Size: 1482 Color: 211
Size: 336 Color: 47

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 588
Size: 1362 Color: 205
Size: 416 Color: 82

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 589
Size: 1564 Color: 217
Size: 208 Color: 9

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 592
Size: 976 Color: 174
Size: 744 Color: 147

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14084 Color: 593
Size: 1374 Color: 206
Size: 334 Color: 43

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 595
Size: 1152 Color: 190
Size: 518 Color: 110

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 596
Size: 1104 Color: 184
Size: 548 Color: 115

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14146 Color: 597
Size: 1326 Color: 203
Size: 320 Color: 38

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14158 Color: 598
Size: 1314 Color: 202
Size: 320 Color: 39

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 600
Size: 1200 Color: 191
Size: 390 Color: 68

Bin 54: 1 of cap free
Amount of items: 5
Items: 
Size: 8072 Color: 414
Size: 6571 Color: 394
Size: 432 Color: 88
Size: 364 Color: 56
Size: 352 Color: 54

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 8870 Color: 418
Size: 6573 Color: 396
Size: 348 Color: 50

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 481
Size: 1971 Color: 257
Size: 1700 Color: 231

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12210 Color: 486
Size: 2597 Color: 301
Size: 984 Color: 175

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12495 Color: 499
Size: 2976 Color: 318
Size: 320 Color: 37

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12614 Color: 501
Size: 1749 Color: 236
Size: 1428 Color: 208

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 12677 Color: 504
Size: 3114 Color: 325

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12758 Color: 507
Size: 2749 Color: 304
Size: 284 Color: 23

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13475 Color: 548
Size: 2316 Color: 283

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13767 Color: 571
Size: 2024 Color: 261

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13785 Color: 573
Size: 2006 Color: 259

Bin 65: 2 of cap free
Amount of items: 5
Items: 
Size: 7912 Color: 413
Size: 6568 Color: 393
Size: 576 Color: 125
Size: 368 Color: 58
Size: 366 Color: 57

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 8866 Color: 417
Size: 6572 Color: 395
Size: 352 Color: 51

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 9762 Color: 429
Size: 5692 Color: 382
Size: 336 Color: 46

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 10449 Color: 438
Size: 5009 Color: 374
Size: 332 Color: 42

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 11302 Color: 462
Size: 4488 Color: 365

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 465
Size: 4152 Color: 351
Size: 176 Color: 6

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11715 Color: 471
Size: 3091 Color: 324
Size: 984 Color: 176

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 12425 Color: 493
Size: 2357 Color: 287
Size: 1008 Color: 181

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12492 Color: 498
Size: 3298 Color: 329

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 12726 Color: 506
Size: 3064 Color: 323

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13010 Color: 520
Size: 2780 Color: 309

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13160 Color: 528
Size: 2630 Color: 302

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13326 Color: 538
Size: 2464 Color: 295

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13503 Color: 550
Size: 2287 Color: 280

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 569
Size: 2058 Color: 264

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 590
Size: 1750 Color: 237

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14192 Color: 599
Size: 1598 Color: 221

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 10545 Color: 443
Size: 4932 Color: 371
Size: 312 Color: 34

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 494
Size: 3349 Color: 332

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 12913 Color: 512
Size: 2876 Color: 315

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 12945 Color: 513
Size: 2844 Color: 314

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 12965 Color: 516
Size: 2824 Color: 313

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 12993 Color: 517
Size: 2796 Color: 310

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 13033 Color: 523
Size: 2756 Color: 307

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13697 Color: 567
Size: 2092 Color: 269

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 14116 Color: 594
Size: 1673 Color: 228

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 461
Size: 4308 Color: 356
Size: 208 Color: 10

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 477
Size: 3788 Color: 347
Size: 88 Color: 0

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 492
Size: 3380 Color: 333

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 12868 Color: 511
Size: 2920 Color: 317

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 13272 Color: 534
Size: 2516 Color: 297

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 13427 Color: 545
Size: 2361 Color: 288

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 564
Size: 2128 Color: 272

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 13873 Color: 579
Size: 1915 Color: 253

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13908 Color: 583
Size: 1880 Color: 249

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 14052 Color: 591
Size: 1736 Color: 233

Bin 101: 5 of cap free
Amount of items: 11
Items: 
Size: 7897 Color: 405
Size: 1136 Color: 185
Size: 1072 Color: 183
Size: 1024 Color: 182
Size: 1006 Color: 180
Size: 1004 Color: 179
Size: 1000 Color: 178
Size: 448 Color: 90
Size: 400 Color: 75
Size: 400 Color: 74
Size: 400 Color: 73

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 12018 Color: 478
Size: 3769 Color: 346

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 13229 Color: 531
Size: 2558 Color: 298

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 13386 Color: 543
Size: 2401 Color: 292

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13587 Color: 559
Size: 2200 Color: 276

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 561
Size: 2150 Color: 274

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13695 Color: 566
Size: 2091 Color: 268

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 11252 Color: 459
Size: 4293 Color: 354
Size: 240 Color: 12

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 12460 Color: 496
Size: 3325 Color: 331

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 13214 Color: 529
Size: 2571 Color: 299

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 13964 Color: 586
Size: 1821 Color: 243

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12638 Color: 503
Size: 3146 Color: 326

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 12780 Color: 508
Size: 3004 Color: 321

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13020 Color: 522
Size: 2764 Color: 308

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 540
Size: 2444 Color: 293

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 13451 Color: 547
Size: 2333 Color: 286

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 9908 Color: 434
Size: 5874 Color: 389

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 12058 Color: 479
Size: 3724 Color: 343

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 568
Size: 2062 Color: 265

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 585
Size: 1842 Color: 245

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 10561 Color: 444
Size: 4908 Color: 370
Size: 312 Color: 33

Bin 122: 11 of cap free
Amount of items: 4
Items: 
Size: 10882 Color: 454
Size: 4361 Color: 360
Size: 272 Color: 18
Size: 266 Color: 17

Bin 123: 11 of cap free
Amount of items: 3
Items: 
Size: 11271 Color: 460
Size: 4296 Color: 355
Size: 214 Color: 11

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 12330 Color: 489
Size: 3451 Color: 336

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 13399 Color: 544
Size: 2382 Color: 291

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 13658 Color: 563
Size: 2123 Color: 271

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 565
Size: 2087 Color: 267

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 13829 Color: 575
Size: 1952 Color: 256

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 13902 Color: 582
Size: 1879 Color: 248

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 10604 Color: 446
Size: 5176 Color: 378

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 11340 Color: 464
Size: 4440 Color: 363

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 12220 Color: 487
Size: 3560 Color: 339

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 580
Size: 1902 Color: 251

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 491
Size: 3399 Color: 334

Bin 135: 13 of cap free
Amount of items: 2
Items: 
Size: 13495 Color: 549
Size: 2284 Color: 279

Bin 136: 14 of cap free
Amount of items: 9
Items: 
Size: 7898 Color: 406
Size: 1312 Color: 195
Size: 1312 Color: 194
Size: 1280 Color: 193
Size: 1248 Color: 192
Size: 1146 Color: 188
Size: 784 Color: 154
Size: 400 Color: 72
Size: 398 Color: 71

Bin 137: 14 of cap free
Amount of items: 3
Items: 
Size: 10824 Color: 453
Size: 4682 Color: 368
Size: 272 Color: 19

Bin 138: 14 of cap free
Amount of items: 3
Items: 
Size: 11197 Color: 458
Size: 4341 Color: 359
Size: 240 Color: 13

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 552
Size: 2262 Color: 278

Bin 140: 15 of cap free
Amount of items: 3
Items: 
Size: 10497 Color: 441
Size: 4964 Color: 373
Size: 316 Color: 35

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 11324 Color: 463
Size: 4453 Color: 364

Bin 142: 16 of cap free
Amount of items: 7
Items: 
Size: 7901 Color: 408
Size: 1724 Color: 232
Size: 1689 Color: 230
Size: 1604 Color: 223
Size: 1592 Color: 220
Size: 890 Color: 170
Size: 376 Color: 63

Bin 143: 16 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 435
Size: 5432 Color: 380
Size: 336 Color: 45

Bin 144: 16 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 537
Size: 2458 Color: 294

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 13607 Color: 560
Size: 2168 Color: 275

Bin 146: 18 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 466
Size: 2886 Color: 316
Size: 1360 Color: 204

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 535
Size: 2488 Color: 296

Bin 148: 23 of cap free
Amount of items: 2
Items: 
Size: 11747 Color: 472
Size: 4022 Color: 350

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 12159 Color: 483
Size: 3610 Color: 340

Bin 150: 23 of cap free
Amount of items: 2
Items: 
Size: 12961 Color: 515
Size: 2808 Color: 312

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 12444 Color: 495
Size: 3324 Color: 330

Bin 152: 25 of cap free
Amount of items: 2
Items: 
Size: 13017 Color: 521
Size: 2750 Color: 306

Bin 153: 26 of cap free
Amount of items: 2
Items: 
Size: 12085 Color: 480
Size: 3681 Color: 341

Bin 154: 27 of cap free
Amount of items: 3
Items: 
Size: 11838 Color: 476
Size: 3831 Color: 348
Size: 96 Color: 1

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 12348 Color: 490
Size: 3416 Color: 335

Bin 156: 29 of cap free
Amount of items: 3
Items: 
Size: 10481 Color: 440
Size: 4964 Color: 372
Size: 318 Color: 36

Bin 157: 31 of cap free
Amount of items: 3
Items: 
Size: 11653 Color: 468
Size: 3952 Color: 349
Size: 156 Color: 5

Bin 158: 33 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 514
Size: 2807 Color: 311

Bin 159: 34 of cap free
Amount of items: 3
Items: 
Size: 11000 Color: 456
Size: 4514 Color: 366
Size: 244 Color: 15

Bin 160: 36 of cap free
Amount of items: 3
Items: 
Size: 10660 Color: 452
Size: 4824 Color: 369
Size: 272 Color: 20

Bin 161: 44 of cap free
Amount of items: 2
Items: 
Size: 9876 Color: 433
Size: 5872 Color: 388

Bin 162: 50 of cap free
Amount of items: 2
Items: 
Size: 9160 Color: 423
Size: 6582 Color: 401

Bin 163: 64 of cap free
Amount of items: 2
Items: 
Size: 9288 Color: 424
Size: 6440 Color: 392

Bin 164: 64 of cap free
Amount of items: 2
Items: 
Size: 10472 Color: 439
Size: 5256 Color: 379

Bin 165: 68 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 425
Size: 6056 Color: 390
Size: 348 Color: 49

Bin 166: 70 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 427
Size: 5700 Color: 383
Size: 348 Color: 48

Bin 167: 75 of cap free
Amount of items: 2
Items: 
Size: 7909 Color: 412
Size: 7808 Color: 404

Bin 168: 86 of cap free
Amount of items: 2
Items: 
Size: 10178 Color: 436
Size: 5528 Color: 381

Bin 169: 95 of cap free
Amount of items: 6
Items: 
Size: 7908 Color: 411
Size: 2989 Color: 320
Size: 2044 Color: 262
Size: 2020 Color: 260
Size: 368 Color: 60
Size: 368 Color: 59

Bin 170: 108 of cap free
Amount of items: 3
Items: 
Size: 11820 Color: 475
Size: 3768 Color: 345
Size: 96 Color: 2

Bin 171: 115 of cap free
Amount of items: 3
Items: 
Size: 11056 Color: 457
Size: 4381 Color: 362
Size: 240 Color: 14

Bin 172: 120 of cap free
Amount of items: 2
Items: 
Size: 10585 Color: 445
Size: 5087 Color: 377

Bin 173: 126 of cap free
Amount of items: 3
Items: 
Size: 11812 Color: 474
Size: 3742 Color: 344
Size: 112 Color: 3

Bin 174: 128 of cap free
Amount of items: 2
Items: 
Size: 9592 Color: 426
Size: 6072 Color: 391

Bin 175: 134 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 437
Size: 5026 Color: 375
Size: 336 Color: 44

Bin 176: 137 of cap free
Amount of items: 3
Items: 
Size: 11803 Color: 473
Size: 3716 Color: 342
Size: 136 Color: 4

Bin 177: 163 of cap free
Amount of items: 5
Items: 
Size: 7905 Color: 410
Size: 1995 Color: 258
Size: 1951 Color: 255
Size: 1902 Color: 252
Size: 1876 Color: 247

Bin 178: 164 of cap free
Amount of items: 2
Items: 
Size: 8964 Color: 422
Size: 6664 Color: 402

Bin 179: 174 of cap free
Amount of items: 2
Items: 
Size: 9844 Color: 432
Size: 5774 Color: 387

Bin 180: 184 of cap free
Amount of items: 3
Items: 
Size: 8312 Color: 415
Size: 6944 Color: 403
Size: 352 Color: 53

Bin 181: 185 of cap free
Amount of items: 3
Items: 
Size: 10970 Color: 455
Size: 4373 Color: 361
Size: 264 Color: 16

Bin 182: 186 of cap free
Amount of items: 2
Items: 
Size: 9836 Color: 431
Size: 5770 Color: 386

Bin 183: 190 of cap free
Amount of items: 3
Items: 
Size: 10649 Color: 451
Size: 4681 Color: 367
Size: 272 Color: 21

Bin 184: 220 of cap free
Amount of items: 2
Items: 
Size: 10537 Color: 442
Size: 5035 Color: 376

Bin 185: 241 of cap free
Amount of items: 4
Items: 
Size: 10641 Color: 449
Size: 4322 Color: 357
Size: 300 Color: 28
Size: 288 Color: 26

Bin 186: 248 of cap free
Amount of items: 4
Items: 
Size: 10648 Color: 450
Size: 4324 Color: 358
Size: 288 Color: 25
Size: 284 Color: 24

Bin 187: 255 of cap free
Amount of items: 2
Items: 
Size: 8956 Color: 421
Size: 6581 Color: 400

Bin 188: 267 of cap free
Amount of items: 2
Items: 
Size: 8945 Color: 420
Size: 6580 Color: 399

Bin 189: 269 of cap free
Amount of items: 4
Items: 
Size: 10628 Color: 448
Size: 4287 Color: 353
Size: 304 Color: 30
Size: 304 Color: 29

Bin 190: 276 of cap free
Amount of items: 2
Items: 
Size: 9783 Color: 430
Size: 5733 Color: 385

Bin 191: 290 of cap free
Amount of items: 4
Items: 
Size: 10610 Color: 447
Size: 4284 Color: 352
Size: 304 Color: 32
Size: 304 Color: 31

Bin 192: 301 of cap free
Amount of items: 2
Items: 
Size: 8913 Color: 419
Size: 6578 Color: 398

Bin 193: 308 of cap free
Amount of items: 27
Items: 
Size: 736 Color: 146
Size: 720 Color: 145
Size: 704 Color: 144
Size: 704 Color: 143
Size: 688 Color: 142
Size: 678 Color: 141
Size: 672 Color: 140
Size: 656 Color: 138
Size: 656 Color: 137
Size: 630 Color: 135
Size: 628 Color: 134
Size: 620 Color: 133
Size: 608 Color: 131
Size: 600 Color: 130
Size: 600 Color: 129
Size: 596 Color: 128
Size: 470 Color: 100
Size: 466 Color: 99
Size: 462 Color: 97
Size: 460 Color: 95
Size: 458 Color: 94
Size: 456 Color: 93
Size: 456 Color: 92
Size: 456 Color: 91
Size: 448 Color: 89
Size: 428 Color: 87
Size: 428 Color: 86

Bin 194: 311 of cap free
Amount of items: 6
Items: 
Size: 7902 Color: 409
Size: 1839 Color: 244
Size: 1804 Color: 242
Size: 1782 Color: 240
Size: 1780 Color: 239
Size: 374 Color: 62

Bin 195: 334 of cap free
Amount of items: 2
Items: 
Size: 9751 Color: 428
Size: 5707 Color: 384

Bin 196: 343 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 416
Size: 6577 Color: 397
Size: 352 Color: 52

Bin 197: 364 of cap free
Amount of items: 21
Items: 
Size: 960 Color: 173
Size: 880 Color: 169
Size: 874 Color: 168
Size: 874 Color: 167
Size: 870 Color: 166
Size: 866 Color: 165
Size: 864 Color: 164
Size: 856 Color: 161
Size: 856 Color: 160
Size: 848 Color: 158
Size: 816 Color: 157
Size: 816 Color: 156
Size: 752 Color: 151
Size: 752 Color: 150
Size: 752 Color: 149
Size: 748 Color: 148
Size: 416 Color: 81
Size: 412 Color: 80
Size: 408 Color: 79
Size: 408 Color: 77
Size: 400 Color: 76

Bin 198: 392 of cap free
Amount of items: 8
Items: 
Size: 7900 Color: 407
Size: 1484 Color: 212
Size: 1314 Color: 201
Size: 1314 Color: 200
Size: 1312 Color: 199
Size: 1312 Color: 197
Size: 384 Color: 66
Size: 380 Color: 64

Bin 199: 7662 of cap free
Amount of items: 15
Items: 
Size: 596 Color: 127
Size: 576 Color: 124
Size: 568 Color: 122
Size: 568 Color: 121
Size: 560 Color: 120
Size: 552 Color: 118
Size: 552 Color: 117
Size: 552 Color: 116
Size: 544 Color: 114
Size: 544 Color: 113
Size: 528 Color: 112
Size: 512 Color: 109
Size: 508 Color: 108
Size: 496 Color: 107
Size: 474 Color: 102

Total size: 3126816
Total free space: 15792

