Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 354738 Color: 1
Size: 328639 Color: 1
Size: 316624 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 431761 Color: 1
Size: 303668 Color: 1
Size: 264572 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 455663 Color: 1
Size: 281704 Color: 1
Size: 262634 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 440229 Color: 0
Size: 301623 Color: 0
Size: 258149 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 394207 Color: 1
Size: 329073 Color: 0
Size: 276721 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 473305 Color: 1
Size: 272276 Color: 0
Size: 254420 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 491352 Color: 1
Size: 257266 Color: 0
Size: 251383 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 452089 Color: 1
Size: 277572 Color: 0
Size: 270340 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 483351 Color: 0
Size: 263292 Color: 1
Size: 253358 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 353761 Color: 0
Size: 326381 Color: 1
Size: 319859 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 483551 Color: 1
Size: 262248 Color: 0
Size: 254202 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 381676 Color: 0
Size: 360462 Color: 1
Size: 257863 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 484622 Color: 0
Size: 259103 Color: 1
Size: 256276 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 439343 Color: 0
Size: 289169 Color: 1
Size: 271489 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 494132 Color: 0
Size: 253978 Color: 1
Size: 251891 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 437925 Color: 1
Size: 294056 Color: 0
Size: 268020 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 383258 Color: 1
Size: 323004 Color: 0
Size: 293739 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 444296 Color: 1
Size: 280538 Color: 0
Size: 275167 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 442465 Color: 1
Size: 295062 Color: 0
Size: 262474 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 394609 Color: 0
Size: 334881 Color: 0
Size: 270511 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 426115 Color: 1
Size: 306153 Color: 0
Size: 267733 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 398328 Color: 1
Size: 336003 Color: 0
Size: 265670 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 481052 Color: 1
Size: 262227 Color: 0
Size: 256722 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 455837 Color: 1
Size: 282981 Color: 0
Size: 261183 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 475712 Color: 1
Size: 274176 Color: 0
Size: 250113 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 423698 Color: 0
Size: 315044 Color: 0
Size: 261259 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 480144 Color: 1
Size: 267348 Color: 0
Size: 252509 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 381337 Color: 0
Size: 345657 Color: 1
Size: 273007 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 349393 Color: 0
Size: 342884 Color: 1
Size: 307724 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 451025 Color: 0
Size: 293406 Color: 1
Size: 255570 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 358085 Color: 0
Size: 345094 Color: 0
Size: 296822 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 476930 Color: 0
Size: 263272 Color: 1
Size: 259799 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 364504 Color: 0
Size: 353400 Color: 1
Size: 282097 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 449264 Color: 0
Size: 282770 Color: 1
Size: 267967 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 490029 Color: 1
Size: 255184 Color: 0
Size: 254788 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 479496 Color: 1
Size: 269459 Color: 0
Size: 251046 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 390208 Color: 1
Size: 336319 Color: 0
Size: 273474 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 445810 Color: 0
Size: 284434 Color: 1
Size: 269757 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 360749 Color: 0
Size: 334179 Color: 1
Size: 305073 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 418645 Color: 1
Size: 294264 Color: 0
Size: 287092 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 362754 Color: 1
Size: 328816 Color: 0
Size: 308431 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 377679 Color: 0
Size: 338745 Color: 1
Size: 283577 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 417176 Color: 0
Size: 317328 Color: 0
Size: 265497 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 493071 Color: 0
Size: 254314 Color: 1
Size: 252616 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 461479 Color: 0
Size: 275491 Color: 0
Size: 263031 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 480026 Color: 0
Size: 269112 Color: 0
Size: 250863 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 369347 Color: 1
Size: 327784 Color: 1
Size: 302870 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 486572 Color: 0
Size: 263049 Color: 1
Size: 250380 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 442804 Color: 1
Size: 283912 Color: 0
Size: 273285 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 427454 Color: 1
Size: 317396 Color: 0
Size: 255151 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 468077 Color: 0
Size: 279810 Color: 1
Size: 252114 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 441952 Color: 0
Size: 305491 Color: 1
Size: 252558 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 382747 Color: 1
Size: 360302 Color: 0
Size: 256952 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 366287 Color: 1
Size: 360206 Color: 1
Size: 273508 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 351207 Color: 1
Size: 350457 Color: 0
Size: 298337 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 443644 Color: 0
Size: 299367 Color: 0
Size: 256990 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 403316 Color: 0
Size: 321479 Color: 0
Size: 275206 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 368475 Color: 0
Size: 355517 Color: 1
Size: 276009 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 353986 Color: 1
Size: 330297 Color: 1
Size: 315718 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 340681 Color: 0
Size: 338318 Color: 1
Size: 321002 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 343639 Color: 1
Size: 343510 Color: 0
Size: 312852 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 396076 Color: 0
Size: 308445 Color: 1
Size: 295480 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 347365 Color: 0
Size: 332471 Color: 0
Size: 320165 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 390967 Color: 1
Size: 344779 Color: 0
Size: 264255 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 371143 Color: 0
Size: 339044 Color: 1
Size: 289814 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 386450 Color: 1
Size: 321642 Color: 0
Size: 291909 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 414206 Color: 1
Size: 298207 Color: 0
Size: 287588 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 479718 Color: 0
Size: 265653 Color: 0
Size: 254630 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 404582 Color: 0
Size: 306463 Color: 0
Size: 288956 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 462782 Color: 0
Size: 273294 Color: 0
Size: 263925 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 395770 Color: 1
Size: 302609 Color: 0
Size: 301622 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 379904 Color: 0
Size: 353726 Color: 1
Size: 266371 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 383409 Color: 0
Size: 327763 Color: 1
Size: 288829 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 439832 Color: 1
Size: 300800 Color: 1
Size: 259369 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 400750 Color: 1
Size: 333709 Color: 1
Size: 265542 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 472989 Color: 0
Size: 272455 Color: 1
Size: 254557 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 368542 Color: 1
Size: 355011 Color: 1
Size: 276448 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 450611 Color: 0
Size: 279008 Color: 1
Size: 270382 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 350819 Color: 0
Size: 340331 Color: 1
Size: 308851 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 359818 Color: 1
Size: 340682 Color: 1
Size: 299501 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 432415 Color: 0
Size: 296605 Color: 0
Size: 270981 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 370870 Color: 1
Size: 318365 Color: 1
Size: 310766 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 377064 Color: 0
Size: 357100 Color: 1
Size: 265837 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 368678 Color: 1
Size: 341639 Color: 0
Size: 289684 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 422912 Color: 0
Size: 304033 Color: 1
Size: 273056 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 379144 Color: 0
Size: 335113 Color: 1
Size: 285744 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 358079 Color: 0
Size: 321159 Color: 1
Size: 320763 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 470247 Color: 1
Size: 276264 Color: 0
Size: 253490 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 394320 Color: 1
Size: 324863 Color: 1
Size: 280818 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 348534 Color: 1
Size: 340979 Color: 0
Size: 310488 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 414461 Color: 0
Size: 311897 Color: 1
Size: 273643 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 377575 Color: 1
Size: 334955 Color: 1
Size: 287471 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 463877 Color: 1
Size: 281404 Color: 0
Size: 254720 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 461034 Color: 0
Size: 252098 Color: 1
Size: 286869 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 439411 Color: 1
Size: 298343 Color: 0
Size: 262247 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 414435 Color: 1
Size: 302417 Color: 0
Size: 283149 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 396421 Color: 1
Size: 328594 Color: 0
Size: 274986 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 348023 Color: 0
Size: 333351 Color: 1
Size: 318627 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 435867 Color: 0
Size: 293701 Color: 1
Size: 270433 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 378620 Color: 1
Size: 358514 Color: 0
Size: 262867 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 478143 Color: 1
Size: 261293 Color: 0
Size: 260565 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 484575 Color: 0
Size: 263701 Color: 1
Size: 251725 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 366808 Color: 1
Size: 359048 Color: 0
Size: 274145 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 474510 Color: 0
Size: 269013 Color: 1
Size: 256478 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 461639 Color: 0
Size: 269427 Color: 0
Size: 268935 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 428945 Color: 1
Size: 309079 Color: 0
Size: 261977 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 442522 Color: 1
Size: 283793 Color: 0
Size: 273686 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 363583 Color: 1
Size: 337188 Color: 0
Size: 299230 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 439495 Color: 0
Size: 302802 Color: 1
Size: 257704 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 480250 Color: 0
Size: 262048 Color: 1
Size: 257703 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 452738 Color: 0
Size: 296129 Color: 1
Size: 251134 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 496256 Color: 0
Size: 252188 Color: 1
Size: 251557 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 386601 Color: 1
Size: 330494 Color: 1
Size: 282906 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 364225 Color: 1
Size: 344044 Color: 0
Size: 291732 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 483538 Color: 0
Size: 261227 Color: 0
Size: 255236 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 406814 Color: 1
Size: 316053 Color: 0
Size: 277134 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 340465 Color: 1
Size: 337253 Color: 0
Size: 322283 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 431138 Color: 1
Size: 297123 Color: 0
Size: 271740 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 411320 Color: 0
Size: 295879 Color: 1
Size: 292802 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 429147 Color: 1
Size: 319906 Color: 1
Size: 250948 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 495956 Color: 0
Size: 253729 Color: 1
Size: 250316 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 419085 Color: 1
Size: 309064 Color: 0
Size: 271852 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 414877 Color: 0
Size: 305459 Color: 1
Size: 279665 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 374123 Color: 0
Size: 356147 Color: 1
Size: 269731 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 429928 Color: 1
Size: 304583 Color: 0
Size: 265490 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 426824 Color: 0
Size: 288434 Color: 1
Size: 284743 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 403646 Color: 0
Size: 325347 Color: 1
Size: 271008 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 370184 Color: 1
Size: 366936 Color: 0
Size: 262881 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 394224 Color: 1
Size: 335573 Color: 1
Size: 270204 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 394305 Color: 0
Size: 305280 Color: 0
Size: 300416 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 447471 Color: 1
Size: 296439 Color: 1
Size: 256091 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 342217 Color: 1
Size: 340062 Color: 0
Size: 317722 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 387182 Color: 0
Size: 315573 Color: 1
Size: 297246 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 448208 Color: 1
Size: 280900 Color: 0
Size: 270893 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 435481 Color: 1
Size: 299340 Color: 0
Size: 265180 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 389550 Color: 0
Size: 331921 Color: 1
Size: 278530 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 375681 Color: 1
Size: 365794 Color: 0
Size: 258526 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 414340 Color: 1
Size: 332774 Color: 0
Size: 252887 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 466998 Color: 1
Size: 271501 Color: 0
Size: 261502 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 358591 Color: 0
Size: 327000 Color: 1
Size: 314410 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 439142 Color: 0
Size: 298311 Color: 0
Size: 262548 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 385352 Color: 0
Size: 329228 Color: 1
Size: 285421 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 457570 Color: 0
Size: 283343 Color: 0
Size: 259088 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 340768 Color: 0
Size: 329883 Color: 0
Size: 329350 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 448672 Color: 1
Size: 290609 Color: 0
Size: 260720 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 451351 Color: 0
Size: 290875 Color: 0
Size: 257775 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 391234 Color: 1
Size: 330310 Color: 0
Size: 278457 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 345214 Color: 0
Size: 337106 Color: 1
Size: 317681 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 450972 Color: 0
Size: 292791 Color: 1
Size: 256238 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 424434 Color: 1
Size: 310792 Color: 0
Size: 264775 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 435744 Color: 0
Size: 295672 Color: 1
Size: 268585 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 432676 Color: 1
Size: 291468 Color: 0
Size: 275857 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 424195 Color: 1
Size: 325231 Color: 0
Size: 250575 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 409236 Color: 0
Size: 320942 Color: 1
Size: 269823 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 473953 Color: 0
Size: 270807 Color: 1
Size: 255241 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 363573 Color: 0
Size: 358541 Color: 1
Size: 277887 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 399167 Color: 0
Size: 335616 Color: 1
Size: 265218 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 458798 Color: 0
Size: 279994 Color: 1
Size: 261209 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 393546 Color: 1
Size: 330598 Color: 0
Size: 275857 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 464495 Color: 0
Size: 274138 Color: 1
Size: 261368 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 398914 Color: 0
Size: 318923 Color: 1
Size: 282164 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 423325 Color: 1
Size: 301346 Color: 0
Size: 275330 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 348759 Color: 0
Size: 347884 Color: 1
Size: 303358 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 476758 Color: 1
Size: 264616 Color: 1
Size: 258627 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 417794 Color: 0
Size: 298576 Color: 1
Size: 283631 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 436882 Color: 1
Size: 295069 Color: 0
Size: 268050 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 383798 Color: 1
Size: 357605 Color: 1
Size: 258598 Color: 0

Total size: 167000167
Total free space: 0

