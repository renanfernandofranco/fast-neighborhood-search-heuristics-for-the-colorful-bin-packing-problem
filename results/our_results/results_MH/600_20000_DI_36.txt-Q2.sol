Capicity Bin: 16304
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8162 Color: 0
Size: 6786 Color: 1
Size: 1356 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8940 Color: 1
Size: 6796 Color: 1
Size: 568 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9130 Color: 1
Size: 6782 Color: 0
Size: 392 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9196 Color: 1
Size: 6780 Color: 0
Size: 328 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9979 Color: 0
Size: 5271 Color: 1
Size: 1054 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10424 Color: 1
Size: 5528 Color: 0
Size: 352 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 1
Size: 5192 Color: 0
Size: 434 Color: 1

Bin 8: 0 of cap free
Amount of items: 5
Items: 
Size: 11154 Color: 1
Size: 1524 Color: 1
Size: 1448 Color: 0
Size: 1354 Color: 0
Size: 824 Color: 1

Bin 9: 0 of cap free
Amount of items: 5
Items: 
Size: 11158 Color: 1
Size: 1604 Color: 1
Size: 1484 Color: 0
Size: 1412 Color: 0
Size: 646 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12727 Color: 0
Size: 2957 Color: 1
Size: 620 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 1
Size: 1862 Color: 0
Size: 1506 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13142 Color: 0
Size: 2382 Color: 0
Size: 780 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 1
Size: 1660 Color: 0
Size: 1422 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13312 Color: 0
Size: 2246 Color: 0
Size: 746 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 1
Size: 2264 Color: 1
Size: 764 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13356 Color: 0
Size: 2328 Color: 1
Size: 620 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 0
Size: 2138 Color: 1
Size: 744 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 0
Size: 1528 Color: 1
Size: 1344 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 1
Size: 1562 Color: 1
Size: 664 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14300 Color: 0
Size: 1184 Color: 1
Size: 820 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 0
Size: 1428 Color: 0
Size: 544 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14306 Color: 1
Size: 1050 Color: 0
Size: 948 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14434 Color: 1
Size: 1364 Color: 0
Size: 506 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1356 Color: 0
Size: 416 Color: 1

Bin 25: 1 of cap free
Amount of items: 11
Items: 
Size: 8157 Color: 1
Size: 1048 Color: 1
Size: 976 Color: 1
Size: 960 Color: 1
Size: 960 Color: 1
Size: 856 Color: 0
Size: 856 Color: 0
Size: 856 Color: 0
Size: 840 Color: 0
Size: 410 Color: 1
Size: 384 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 0
Size: 5927 Color: 1
Size: 1168 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 10011 Color: 1
Size: 5924 Color: 0
Size: 368 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 10769 Color: 1
Size: 5128 Color: 1
Size: 406 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 1
Size: 4136 Color: 1
Size: 328 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 3735 Color: 0
Size: 384 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 12359 Color: 0
Size: 2936 Color: 1
Size: 1008 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 12459 Color: 0
Size: 2460 Color: 1
Size: 1384 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 12582 Color: 0
Size: 3721 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 1
Size: 3118 Color: 0
Size: 428 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 13103 Color: 1
Size: 1848 Color: 1
Size: 1352 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 0
Size: 1992 Color: 1
Size: 1048 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 13411 Color: 0
Size: 2512 Color: 1
Size: 380 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 13657 Color: 1
Size: 2362 Color: 0
Size: 284 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 1
Size: 2131 Color: 1
Size: 430 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 1
Size: 2399 Color: 0
Size: 108 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 9193 Color: 1
Size: 6781 Color: 1
Size: 328 Color: 0

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 9688 Color: 0
Size: 5982 Color: 1
Size: 632 Color: 1

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 10598 Color: 0
Size: 5704 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 0
Size: 4248 Color: 1
Size: 276 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 0
Size: 3406 Color: 1
Size: 678 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 12396 Color: 1
Size: 3906 Color: 0

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 12982 Color: 1
Size: 3320 Color: 0

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 0
Size: 2314 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 1
Size: 1666 Color: 0
Size: 160 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 0
Size: 1606 Color: 1
Size: 128 Color: 0

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 9940 Color: 0
Size: 5953 Color: 1
Size: 408 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 1
Size: 4147 Color: 1
Size: 392 Color: 0

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 11785 Color: 1
Size: 4044 Color: 1
Size: 472 Color: 0

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 13721 Color: 1
Size: 2580 Color: 0

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 1
Size: 2444 Color: 0

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 13930 Color: 0
Size: 2371 Color: 1

Bin 57: 4 of cap free
Amount of items: 27
Items: 
Size: 752 Color: 0
Size: 736 Color: 0
Size: 692 Color: 0
Size: 682 Color: 0
Size: 680 Color: 1
Size: 672 Color: 1
Size: 672 Color: 1
Size: 656 Color: 1
Size: 648 Color: 0
Size: 640 Color: 0
Size: 632 Color: 1
Size: 632 Color: 0
Size: 624 Color: 0
Size: 624 Color: 0
Size: 616 Color: 0
Size: 596 Color: 1
Size: 590 Color: 0
Size: 590 Color: 0
Size: 586 Color: 1
Size: 580 Color: 0
Size: 576 Color: 1
Size: 556 Color: 1
Size: 552 Color: 1
Size: 552 Color: 1
Size: 524 Color: 1
Size: 352 Color: 1
Size: 288 Color: 0

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 1
Size: 7452 Color: 1
Size: 328 Color: 0

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 0
Size: 7088 Color: 1
Size: 228 Color: 0

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 9161 Color: 0
Size: 6771 Color: 0
Size: 368 Color: 1

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 11823 Color: 0
Size: 4109 Color: 1
Size: 368 Color: 0

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 12227 Color: 1
Size: 3411 Color: 1
Size: 662 Color: 0

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 13240 Color: 0
Size: 2324 Color: 1
Size: 736 Color: 1

Bin 64: 5 of cap free
Amount of items: 4
Items: 
Size: 8181 Color: 1
Size: 6396 Color: 1
Size: 1442 Color: 0
Size: 280 Color: 0

Bin 65: 5 of cap free
Amount of items: 3
Items: 
Size: 10168 Color: 1
Size: 5259 Color: 1
Size: 872 Color: 0

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 10781 Color: 0
Size: 5246 Color: 1
Size: 272 Color: 1

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 11618 Color: 0
Size: 4681 Color: 1

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 12211 Color: 0
Size: 3592 Color: 1
Size: 496 Color: 0

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 0
Size: 2291 Color: 1

Bin 70: 6 of cap free
Amount of items: 8
Items: 
Size: 8170 Color: 1
Size: 1512 Color: 1
Size: 1352 Color: 1
Size: 1352 Color: 1
Size: 1232 Color: 0
Size: 1224 Color: 0
Size: 1184 Color: 0
Size: 272 Color: 0

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 1
Size: 5928 Color: 1
Size: 440 Color: 0

Bin 72: 6 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 1
Size: 4754 Color: 0
Size: 272 Color: 1

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 13774 Color: 0
Size: 2524 Color: 1

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 14010 Color: 1
Size: 2288 Color: 0

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 14298 Color: 1
Size: 2000 Color: 0

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 14316 Color: 1
Size: 1982 Color: 0

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 10687 Color: 0
Size: 5314 Color: 1
Size: 296 Color: 0

Bin 78: 7 of cap free
Amount of items: 4
Items: 
Size: 11224 Color: 1
Size: 2196 Color: 0
Size: 2173 Color: 0
Size: 704 Color: 1

Bin 79: 7 of cap free
Amount of items: 3
Items: 
Size: 12243 Color: 1
Size: 3774 Color: 0
Size: 280 Color: 1

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 12792 Color: 0
Size: 3505 Color: 1

Bin 81: 8 of cap free
Amount of items: 11
Items: 
Size: 8154 Color: 0
Size: 936 Color: 1
Size: 922 Color: 1
Size: 920 Color: 1
Size: 896 Color: 1
Size: 872 Color: 1
Size: 832 Color: 0
Size: 832 Color: 0
Size: 832 Color: 0
Size: 828 Color: 0
Size: 272 Color: 1

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 1
Size: 6488 Color: 0
Size: 344 Color: 0

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 1
Size: 3633 Color: 0
Size: 332 Color: 0

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 12822 Color: 0
Size: 3474 Color: 1

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 14574 Color: 1
Size: 1722 Color: 0

Bin 86: 9 of cap free
Amount of items: 3
Items: 
Size: 11452 Color: 0
Size: 4603 Color: 1
Size: 240 Color: 0

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 13641 Color: 0
Size: 2654 Color: 1

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 1
Size: 2462 Color: 0

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 13884 Color: 0
Size: 2411 Color: 1

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 14074 Color: 1
Size: 2221 Color: 0

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 12138 Color: 1
Size: 4156 Color: 0

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 12492 Color: 0
Size: 3802 Color: 1

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 0
Size: 1914 Color: 1

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 14650 Color: 1
Size: 1644 Color: 0

Bin 95: 11 of cap free
Amount of items: 12
Items: 
Size: 8153 Color: 0
Size: 816 Color: 0
Size: 808 Color: 1
Size: 808 Color: 0
Size: 800 Color: 1
Size: 768 Color: 0
Size: 756 Color: 0
Size: 752 Color: 0
Size: 712 Color: 1
Size: 704 Color: 1
Size: 684 Color: 1
Size: 532 Color: 1

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 8188 Color: 0
Size: 7081 Color: 1
Size: 1024 Color: 0

Bin 97: 11 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 1
Size: 2521 Color: 0
Size: 96 Color: 0

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 0
Size: 2365 Color: 1

Bin 99: 11 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 0
Size: 2153 Color: 1
Size: 80 Color: 0

Bin 100: 12 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 0
Size: 4684 Color: 0
Size: 256 Color: 1

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 12072 Color: 1
Size: 4220 Color: 0

Bin 102: 12 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 0
Size: 2903 Color: 1
Size: 1192 Color: 0

Bin 103: 12 of cap free
Amount of items: 2
Items: 
Size: 14364 Color: 0
Size: 1928 Color: 1

Bin 104: 13 of cap free
Amount of items: 2
Items: 
Size: 13201 Color: 0
Size: 3090 Color: 1

Bin 105: 13 of cap free
Amount of items: 2
Items: 
Size: 13354 Color: 1
Size: 2937 Color: 0

Bin 106: 13 of cap free
Amount of items: 2
Items: 
Size: 13567 Color: 0
Size: 2724 Color: 1

Bin 107: 14 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 0
Size: 4294 Color: 1

Bin 108: 14 of cap free
Amount of items: 2
Items: 
Size: 13452 Color: 1
Size: 2838 Color: 0

Bin 109: 14 of cap free
Amount of items: 2
Items: 
Size: 13530 Color: 0
Size: 2760 Color: 1

Bin 110: 15 of cap free
Amount of items: 3
Items: 
Size: 11372 Color: 1
Size: 4613 Color: 0
Size: 304 Color: 1

Bin 111: 15 of cap free
Amount of items: 2
Items: 
Size: 12761 Color: 1
Size: 3528 Color: 0

Bin 112: 15 of cap free
Amount of items: 2
Items: 
Size: 13029 Color: 0
Size: 3260 Color: 1

Bin 113: 16 of cap free
Amount of items: 2
Items: 
Size: 13592 Color: 0
Size: 2696 Color: 1

Bin 114: 16 of cap free
Amount of items: 2
Items: 
Size: 14430 Color: 0
Size: 1858 Color: 1

Bin 115: 16 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 1
Size: 1676 Color: 0

Bin 116: 16 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 1
Size: 1640 Color: 0

Bin 117: 16 of cap free
Amount of items: 2
Items: 
Size: 14668 Color: 1
Size: 1620 Color: 0

Bin 118: 17 of cap free
Amount of items: 2
Items: 
Size: 13427 Color: 1
Size: 2860 Color: 0

Bin 119: 18 of cap free
Amount of items: 2
Items: 
Size: 12598 Color: 1
Size: 3688 Color: 0

Bin 120: 18 of cap free
Amount of items: 2
Items: 
Size: 12902 Color: 0
Size: 3384 Color: 1

Bin 121: 18 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 1
Size: 2770 Color: 0

Bin 122: 19 of cap free
Amount of items: 9
Items: 
Size: 8169 Color: 1
Size: 1190 Color: 1
Size: 1168 Color: 1
Size: 1168 Color: 0
Size: 1136 Color: 1
Size: 1066 Color: 0
Size: 1060 Color: 0
Size: 928 Color: 0
Size: 400 Color: 0

Bin 123: 19 of cap free
Amount of items: 2
Items: 
Size: 13080 Color: 0
Size: 3205 Color: 1

Bin 124: 20 of cap free
Amount of items: 2
Items: 
Size: 13697 Color: 0
Size: 2587 Color: 1

Bin 125: 20 of cap free
Amount of items: 2
Items: 
Size: 13749 Color: 0
Size: 2535 Color: 1

Bin 126: 21 of cap free
Amount of items: 2
Items: 
Size: 12972 Color: 0
Size: 3311 Color: 1

Bin 127: 22 of cap free
Amount of items: 4
Items: 
Size: 11164 Color: 1
Size: 2042 Color: 0
Size: 2020 Color: 0
Size: 1056 Color: 1

Bin 128: 22 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 1
Size: 1784 Color: 0

Bin 129: 23 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 1
Size: 2061 Color: 0

Bin 130: 25 of cap free
Amount of items: 2
Items: 
Size: 14238 Color: 0
Size: 2041 Color: 1

Bin 131: 26 of cap free
Amount of items: 2
Items: 
Size: 10428 Color: 1
Size: 5850 Color: 0

Bin 132: 26 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 0
Size: 2190 Color: 1

Bin 133: 26 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 1
Size: 2110 Color: 0

Bin 134: 28 of cap free
Amount of items: 2
Items: 
Size: 11992 Color: 1
Size: 4284 Color: 0

Bin 135: 28 of cap free
Amount of items: 2
Items: 
Size: 12076 Color: 0
Size: 4200 Color: 1

Bin 136: 28 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 1
Size: 4124 Color: 0

Bin 137: 28 of cap free
Amount of items: 2
Items: 
Size: 14314 Color: 1
Size: 1962 Color: 0

Bin 138: 28 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 1
Size: 1932 Color: 0

Bin 139: 29 of cap free
Amount of items: 2
Items: 
Size: 12876 Color: 1
Size: 3399 Color: 0

Bin 140: 30 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 1
Size: 2902 Color: 0

Bin 141: 31 of cap free
Amount of items: 2
Items: 
Size: 12506 Color: 0
Size: 3767 Color: 1

Bin 142: 32 of cap free
Amount of items: 4
Items: 
Size: 11244 Color: 0
Size: 2396 Color: 0
Size: 1876 Color: 1
Size: 756 Color: 1

Bin 143: 32 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 1
Size: 3124 Color: 0
Size: 192 Color: 0

Bin 144: 32 of cap free
Amount of items: 2
Items: 
Size: 14598 Color: 0
Size: 1674 Color: 1

Bin 145: 34 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 0
Size: 2092 Color: 1

Bin 146: 34 of cap free
Amount of items: 2
Items: 
Size: 14552 Color: 1
Size: 1718 Color: 0

Bin 147: 35 of cap free
Amount of items: 2
Items: 
Size: 12427 Color: 0
Size: 3842 Color: 1

Bin 148: 35 of cap free
Amount of items: 2
Items: 
Size: 12821 Color: 1
Size: 3448 Color: 0

Bin 149: 36 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 1
Size: 2200 Color: 0

Bin 150: 37 of cap free
Amount of items: 2
Items: 
Size: 13036 Color: 1
Size: 3231 Color: 0

Bin 151: 39 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 1
Size: 6793 Color: 1
Size: 1272 Color: 0

Bin 152: 39 of cap free
Amount of items: 2
Items: 
Size: 13467 Color: 1
Size: 2798 Color: 0

Bin 153: 40 of cap free
Amount of items: 2
Items: 
Size: 14490 Color: 1
Size: 1774 Color: 0

Bin 154: 42 of cap free
Amount of items: 2
Items: 
Size: 13854 Color: 1
Size: 2408 Color: 0

Bin 155: 44 of cap free
Amount of items: 2
Items: 
Size: 13279 Color: 0
Size: 2981 Color: 1

Bin 156: 46 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 0
Size: 6794 Color: 1
Size: 1296 Color: 0

Bin 157: 46 of cap free
Amount of items: 2
Items: 
Size: 11500 Color: 1
Size: 4758 Color: 0

Bin 158: 46 of cap free
Amount of items: 2
Items: 
Size: 14596 Color: 0
Size: 1662 Color: 1

Bin 159: 48 of cap free
Amount of items: 3
Items: 
Size: 9860 Color: 1
Size: 5308 Color: 0
Size: 1088 Color: 0

Bin 160: 48 of cap free
Amount of items: 2
Items: 
Size: 14516 Color: 0
Size: 1740 Color: 1

Bin 161: 50 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 1
Size: 1766 Color: 0

Bin 162: 52 of cap free
Amount of items: 2
Items: 
Size: 12248 Color: 1
Size: 4004 Color: 0

Bin 163: 52 of cap free
Amount of items: 2
Items: 
Size: 13072 Color: 0
Size: 3180 Color: 1

Bin 164: 52 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 0
Size: 2782 Color: 1

Bin 165: 52 of cap free
Amount of items: 2
Items: 
Size: 13583 Color: 1
Size: 2669 Color: 0

Bin 166: 56 of cap free
Amount of items: 34
Items: 
Size: 580 Color: 0
Size: 564 Color: 0
Size: 560 Color: 0
Size: 556 Color: 0
Size: 552 Color: 0
Size: 544 Color: 0
Size: 544 Color: 0
Size: 528 Color: 0
Size: 516 Color: 0
Size: 512 Color: 0
Size: 504 Color: 1
Size: 504 Color: 1
Size: 488 Color: 0
Size: 488 Color: 0
Size: 488 Color: 0
Size: 482 Color: 1
Size: 480 Color: 1
Size: 478 Color: 0
Size: 472 Color: 1
Size: 472 Color: 0
Size: 472 Color: 0
Size: 464 Color: 1
Size: 464 Color: 0
Size: 446 Color: 1
Size: 442 Color: 1
Size: 436 Color: 1
Size: 432 Color: 1
Size: 432 Color: 1
Size: 424 Color: 1
Size: 424 Color: 1
Size: 420 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 344 Color: 1

Bin 167: 56 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 0
Size: 3596 Color: 1
Size: 176 Color: 1

Bin 168: 56 of cap free
Amount of items: 2
Items: 
Size: 13610 Color: 0
Size: 2638 Color: 1

Bin 169: 56 of cap free
Amount of items: 2
Items: 
Size: 13678 Color: 1
Size: 2570 Color: 0

Bin 170: 58 of cap free
Amount of items: 2
Items: 
Size: 13450 Color: 1
Size: 2796 Color: 0

Bin 171: 58 of cap free
Amount of items: 2
Items: 
Size: 14378 Color: 1
Size: 1868 Color: 0

Bin 172: 60 of cap free
Amount of items: 2
Items: 
Size: 10692 Color: 0
Size: 5552 Color: 1

Bin 173: 61 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 1
Size: 2731 Color: 0

Bin 174: 64 of cap free
Amount of items: 2
Items: 
Size: 10088 Color: 1
Size: 6152 Color: 0

Bin 175: 64 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 0
Size: 2568 Color: 1

Bin 176: 71 of cap free
Amount of items: 2
Items: 
Size: 11329 Color: 1
Size: 4904 Color: 0

Bin 177: 73 of cap free
Amount of items: 2
Items: 
Size: 11375 Color: 0
Size: 4856 Color: 1

Bin 178: 85 of cap free
Amount of items: 2
Items: 
Size: 13950 Color: 1
Size: 2269 Color: 0

Bin 179: 88 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 0
Size: 2780 Color: 1

Bin 180: 96 of cap free
Amount of items: 2
Items: 
Size: 10508 Color: 1
Size: 5700 Color: 0

Bin 181: 98 of cap free
Amount of items: 3
Items: 
Size: 11746 Color: 0
Size: 3102 Color: 0
Size: 1358 Color: 1

Bin 182: 100 of cap free
Amount of items: 2
Items: 
Size: 8156 Color: 0
Size: 8048 Color: 1

Bin 183: 100 of cap free
Amount of items: 2
Items: 
Size: 12781 Color: 0
Size: 3423 Color: 1

Bin 184: 108 of cap free
Amount of items: 2
Items: 
Size: 13000 Color: 1
Size: 3196 Color: 0

Bin 185: 130 of cap free
Amount of items: 2
Items: 
Size: 12566 Color: 0
Size: 3608 Color: 1

Bin 186: 134 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 0
Size: 4290 Color: 1

Bin 187: 144 of cap free
Amount of items: 2
Items: 
Size: 11324 Color: 1
Size: 4836 Color: 0

Bin 188: 144 of cap free
Amount of items: 2
Items: 
Size: 12556 Color: 0
Size: 3604 Color: 1

Bin 189: 146 of cap free
Amount of items: 2
Items: 
Size: 9286 Color: 0
Size: 6872 Color: 1

Bin 190: 153 of cap free
Amount of items: 4
Items: 
Size: 8172 Color: 1
Size: 5245 Color: 1
Size: 1382 Color: 0
Size: 1352 Color: 0

Bin 191: 154 of cap free
Amount of items: 2
Items: 
Size: 10010 Color: 0
Size: 6140 Color: 1

Bin 192: 161 of cap free
Amount of items: 3
Items: 
Size: 11698 Color: 1
Size: 2953 Color: 1
Size: 1492 Color: 0

Bin 193: 172 of cap free
Amount of items: 2
Items: 
Size: 12966 Color: 1
Size: 3166 Color: 0

Bin 194: 178 of cap free
Amount of items: 2
Items: 
Size: 12950 Color: 0
Size: 3176 Color: 1

Bin 195: 190 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 1
Size: 3786 Color: 0

Bin 196: 229 of cap free
Amount of items: 2
Items: 
Size: 9995 Color: 1
Size: 6080 Color: 0

Bin 197: 244 of cap free
Amount of items: 2
Items: 
Size: 10488 Color: 1
Size: 5572 Color: 0

Bin 198: 268 of cap free
Amount of items: 2
Items: 
Size: 8636 Color: 1
Size: 7400 Color: 0

Bin 199: 10260 of cap free
Amount of items: 17
Items: 
Size: 464 Color: 0
Size: 460 Color: 0
Size: 452 Color: 0
Size: 448 Color: 0
Size: 448 Color: 0
Size: 368 Color: 0
Size: 332 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 308 Color: 0
Size: 300 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0

Total size: 3228192
Total free space: 16304

