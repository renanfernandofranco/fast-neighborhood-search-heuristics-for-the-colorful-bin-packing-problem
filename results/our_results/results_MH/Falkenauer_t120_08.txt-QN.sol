Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 93
Size: 326 Color: 70
Size: 268 Color: 26

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 117
Size: 251 Color: 6
Size: 266 Color: 22

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 86
Size: 351 Color: 75
Size: 271 Color: 33

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 106
Size: 301 Color: 56
Size: 250 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 92
Size: 317 Color: 66
Size: 281 Color: 40

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 85
Size: 354 Color: 76
Size: 271 Color: 34

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 102
Size: 301 Color: 57
Size: 269 Color: 31

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 118
Size: 263 Color: 19
Size: 254 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 116
Size: 268 Color: 27
Size: 251 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 82
Size: 316 Color: 65
Size: 315 Color: 64

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 113
Size: 264 Color: 20
Size: 261 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 100
Size: 304 Color: 58
Size: 269 Color: 30

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 115
Size: 264 Color: 21
Size: 259 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 84
Size: 360 Color: 79
Size: 268 Color: 28

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 89
Size: 315 Color: 63
Size: 290 Color: 52

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 90
Size: 346 Color: 73
Size: 257 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 94
Size: 325 Color: 69
Size: 267 Color: 24

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 96
Size: 322 Color: 68
Size: 267 Color: 23

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 78
Size: 357 Color: 77
Size: 285 Color: 44

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 97
Size: 312 Color: 62
Size: 270 Color: 32

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 99
Size: 290 Color: 53
Size: 286 Color: 46

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 110
Size: 288 Color: 48
Size: 251 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 105
Size: 284 Color: 42
Size: 269 Color: 29

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 111
Size: 280 Color: 39
Size: 258 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 103
Size: 288 Color: 47
Size: 276 Color: 36

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 109
Size: 285 Color: 45
Size: 255 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 95
Size: 309 Color: 61
Size: 280 Color: 38

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 98
Size: 309 Color: 60
Size: 273 Color: 35

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 108
Size: 289 Color: 50
Size: 251 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 119
Size: 255 Color: 9
Size: 251 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 104
Size: 289 Color: 51
Size: 268 Color: 25

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 101
Size: 321 Color: 67
Size: 250 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 83
Size: 346 Color: 74
Size: 284 Color: 43

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 88
Size: 334 Color: 71
Size: 284 Color: 41

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 87
Size: 364 Color: 80
Size: 257 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 81
Size: 336 Color: 72
Size: 296 Color: 54

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 114
Size: 263 Color: 18
Size: 261 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 112
Size: 277 Color: 37
Size: 252 Color: 7

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 107
Size: 289 Color: 49
Size: 257 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 91
Size: 305 Color: 59
Size: 297 Color: 55

Total size: 40000
Total free space: 0

