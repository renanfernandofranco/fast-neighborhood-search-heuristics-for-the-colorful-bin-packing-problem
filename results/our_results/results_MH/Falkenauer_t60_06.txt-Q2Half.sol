Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 276 Color: 1
Size: 275 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 284 Color: 1
Size: 265 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 279 Color: 1
Size: 250 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 310 Color: 1
Size: 287 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 321 Color: 1
Size: 274 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 322 Color: 1
Size: 304 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 284 Color: 1
Size: 252 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 365 Color: 1
Size: 261 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 300 Color: 1
Size: 250 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 262 Color: 1
Size: 253 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 355 Color: 1
Size: 276 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 292 Color: 1
Size: 281 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 337 Color: 1
Size: 269 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 322 Color: 1
Size: 278 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 344 Color: 1
Size: 276 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 275 Color: 0
Size: 368 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 250 Color: 0
Size: 252 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 317 Color: 1
Size: 259 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 339 Color: 1
Size: 273 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 328 Color: 1
Size: 297 Color: 0

Total size: 20000
Total free space: 0

