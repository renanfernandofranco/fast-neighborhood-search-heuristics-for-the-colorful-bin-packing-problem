Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 354 Color: 4
Size: 271 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 325 Color: 4
Size: 257 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 289 Color: 3
Size: 251 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 301 Color: 2
Size: 297 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 273 Color: 2
Size: 251 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 281 Color: 3
Size: 257 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 288 Color: 4
Size: 269 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 304 Color: 1
Size: 267 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 346 Color: 0
Size: 284 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 312 Color: 0
Size: 270 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 316 Color: 4
Size: 315 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 336 Color: 4
Size: 285 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 2
Size: 268 Color: 1
Size: 255 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 4
Size: 284 Color: 0
Size: 280 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 284 Color: 0
Size: 269 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 360 Color: 1
Size: 268 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 267 Color: 4
Size: 250 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 288 Color: 0
Size: 251 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 269 Color: 1
Size: 250 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 4
Size: 309 Color: 3
Size: 261 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 326 Color: 1
Size: 263 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 368 Color: 1
Size: 254 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 289 Color: 0
Size: 257 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 322 Color: 4
Size: 251 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 276 Color: 1
Size: 264 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 255 Color: 2
Size: 251 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 280 Color: 1
Size: 271 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 334 Color: 1
Size: 268 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 309 Color: 0
Size: 296 Color: 3
Size: 395 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 290 Color: 3
Size: 286 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 317 Color: 1
Size: 301 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 2
Size: 315 Color: 0
Size: 277 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 266 Color: 1
Size: 263 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 264 Color: 0
Size: 261 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 346 Color: 4
Size: 290 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 0
Size: 357 Color: 2
Size: 285 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 305 Color: 2
Size: 289 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 351 Color: 0
Size: 252 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 321 Color: 3
Size: 268 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 259 Color: 0
Size: 258 Color: 4

Total size: 40000
Total free space: 0

