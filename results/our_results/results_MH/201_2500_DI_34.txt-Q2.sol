Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1239 Color: 0
Size: 1029 Color: 0
Size: 108 Color: 1
Size: 96 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1246 Color: 0
Size: 1018 Color: 1
Size: 120 Color: 1
Size: 80 Color: 0
Size: 8 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 144 Color: 1
Size: 124 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1706 Color: 1
Size: 606 Color: 1
Size: 96 Color: 0
Size: 64 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 1
Size: 558 Color: 0
Size: 168 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 1
Size: 860 Color: 0
Size: 40 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1911 Color: 1
Size: 469 Color: 1
Size: 92 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 0
Size: 522 Color: 1
Size: 104 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 1030 Color: 1
Size: 204 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 1
Size: 543 Color: 1
Size: 108 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 0
Size: 993 Color: 1
Size: 70 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 1
Size: 494 Color: 0
Size: 96 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 0
Size: 1022 Color: 1
Size: 32 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 0
Size: 370 Color: 0
Size: 44 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 0
Size: 284 Color: 0
Size: 48 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 1
Size: 576 Color: 0
Size: 192 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1839 Color: 1
Size: 529 Color: 0
Size: 104 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 0
Size: 447 Color: 1
Size: 54 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 0
Size: 709 Color: 0
Size: 140 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 0
Size: 230 Color: 1
Size: 44 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 0
Size: 308 Color: 0
Size: 56 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1292 Color: 1
Size: 1140 Color: 0
Size: 40 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 0
Size: 242 Color: 1
Size: 44 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 1
Size: 500 Color: 1
Size: 96 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 1
Size: 234 Color: 0
Size: 28 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 0
Size: 204 Color: 1
Size: 200 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 0
Size: 1000 Color: 0
Size: 228 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 0
Size: 756 Color: 1
Size: 272 Color: 0

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 1806 Color: 0
Size: 642 Color: 1
Size: 16 Color: 1
Size: 8 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 1
Size: 372 Color: 0
Size: 120 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 0
Size: 282 Color: 1
Size: 44 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 0
Size: 294 Color: 0
Size: 56 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1986 Color: 1
Size: 478 Color: 1
Size: 8 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 0
Size: 601 Color: 1
Size: 120 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 0
Size: 1031 Color: 1
Size: 204 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 0
Size: 734 Color: 1
Size: 144 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1676 Color: 0
Size: 412 Color: 1
Size: 384 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 0
Size: 340 Color: 1
Size: 104 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 1
Size: 443 Color: 0
Size: 88 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 698 Color: 1
Size: 136 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 0
Size: 418 Color: 0
Size: 80 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 0
Size: 767 Color: 1
Size: 152 Color: 1

Bin 43: 0 of cap free
Amount of items: 4
Items: 
Size: 988 Color: 0
Size: 840 Color: 1
Size: 556 Color: 1
Size: 88 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 1
Size: 452 Color: 0
Size: 88 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 1
Size: 619 Color: 1
Size: 122 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 1
Size: 204 Color: 1
Size: 72 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 0
Size: 802 Color: 1
Size: 112 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 0
Size: 378 Color: 1
Size: 72 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 0
Size: 382 Color: 1
Size: 72 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 0
Size: 620 Color: 0
Size: 40 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 0
Size: 314 Color: 1
Size: 60 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 0
Size: 499 Color: 1
Size: 76 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1926 Color: 1
Size: 466 Color: 0
Size: 80 Color: 1

Bin 54: 0 of cap free
Amount of items: 4
Items: 
Size: 1254 Color: 1
Size: 882 Color: 1
Size: 272 Color: 0
Size: 64 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 1
Size: 409 Color: 0
Size: 80 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 1
Size: 700 Color: 1
Size: 220 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 0
Size: 634 Color: 0
Size: 124 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 0
Size: 504 Color: 1
Size: 236 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 1
Size: 462 Color: 1
Size: 92 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 1
Size: 846 Color: 0
Size: 168 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 667 Color: 0
Size: 132 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 1
Size: 242 Color: 1
Size: 36 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 0
Size: 172 Color: 1
Size: 88 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 0
Size: 877 Color: 0
Size: 174 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 1
Size: 817 Color: 0
Size: 162 Color: 1

Total size: 160680
Total free space: 0

