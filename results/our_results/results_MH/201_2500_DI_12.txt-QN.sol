Capicity Bin: 1864
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 171
Size: 358 Color: 109
Size: 4 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1550 Color: 178
Size: 118 Color: 60
Size: 84 Color: 51
Size: 68 Color: 41
Size: 44 Color: 21

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1207 Color: 150
Size: 549 Color: 124
Size: 108 Color: 58

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1597 Color: 185
Size: 223 Color: 86
Size: 44 Color: 22

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1449 Color: 166
Size: 347 Color: 107
Size: 68 Color: 42

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1647 Color: 194
Size: 197 Color: 76
Size: 16 Color: 4
Size: 4 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 1338 Color: 158
Size: 432 Color: 116
Size: 54 Color: 33
Size: 40 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 172
Size: 293 Color: 101
Size: 58 Color: 35

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 151
Size: 531 Color: 123
Size: 96 Color: 55

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 1418 Color: 163
Size: 282 Color: 98
Size: 92 Color: 54
Size: 72 Color: 46

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 189
Size: 208 Color: 79
Size: 50 Color: 27

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 191
Size: 210 Color: 81
Size: 40 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 147
Size: 621 Color: 127
Size: 80 Color: 48

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 186
Size: 218 Color: 83
Size: 48 Color: 24

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 168
Size: 262 Color: 94
Size: 132 Color: 62

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 752 Color: 133
Size: 622 Color: 128
Size: 442 Color: 117
Size: 48 Color: 23

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 199
Size: 134 Color: 63
Size: 68 Color: 44

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 159
Size: 425 Color: 115
Size: 84 Color: 52

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1475 Color: 169
Size: 325 Color: 104
Size: 64 Color: 40

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 181
Size: 247 Color: 91
Size: 48 Color: 25

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 173
Size: 283 Color: 99
Size: 56 Color: 34

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 174
Size: 302 Color: 102
Size: 32 Color: 8

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 1057 Color: 143
Size: 667 Color: 130
Size: 100 Color: 57
Size: 40 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1361 Color: 160
Size: 421 Color: 114
Size: 82 Color: 50

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 192
Size: 189 Color: 75
Size: 40 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 190
Size: 211 Color: 82
Size: 40 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1288 Color: 154
Size: 330 Color: 105
Size: 246 Color: 90

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 184
Size: 222 Color: 85
Size: 52 Color: 30

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 164
Size: 374 Color: 110
Size: 52 Color: 32

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 934 Color: 139
Size: 778 Color: 137
Size: 152 Color: 66

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 152
Size: 384 Color: 111
Size: 230 Color: 87

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1305 Color: 157
Size: 467 Color: 118
Size: 92 Color: 53

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 156
Size: 514 Color: 121
Size: 52 Color: 31

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1279 Color: 153
Size: 489 Color: 120
Size: 96 Color: 56

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1604 Color: 187
Size: 220 Color: 84
Size: 40 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 175
Size: 269 Color: 95
Size: 60 Color: 37

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 937 Color: 140
Size: 773 Color: 135
Size: 154 Color: 67

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1454 Color: 167
Size: 342 Color: 106
Size: 68 Color: 43

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 179
Size: 275 Color: 97
Size: 36 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1065 Color: 144
Size: 591 Color: 126
Size: 208 Color: 80

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1441 Color: 165
Size: 353 Color: 108
Size: 70 Color: 45

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 195
Size: 181 Color: 72
Size: 34 Color: 11

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 145
Size: 474 Color: 119
Size: 272 Color: 96

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 142
Size: 694 Color: 132
Size: 136 Color: 64

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 182
Size: 170 Color: 71
Size: 124 Color: 61

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 196
Size: 182 Color: 74
Size: 32 Color: 9

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1155 Color: 146
Size: 673 Color: 131
Size: 36 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1544 Color: 177
Size: 288 Color: 100
Size: 32 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 176
Size: 261 Color: 93
Size: 60 Color: 38

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1172 Color: 148
Size: 628 Color: 129
Size: 64 Color: 39

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 180
Size: 253 Color: 92
Size: 50 Color: 29

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 188
Size: 237 Color: 88
Size: 22 Color: 5

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1190 Color: 149
Size: 562 Color: 125
Size: 112 Color: 59

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 161
Size: 414 Color: 113
Size: 80 Color: 49

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 183
Size: 241 Color: 89
Size: 48 Color: 26

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 933 Color: 138
Size: 777 Color: 136
Size: 154 Color: 68

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 941 Color: 141
Size: 771 Color: 134
Size: 152 Color: 65

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 197
Size: 201 Color: 78
Size: 8 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 162
Size: 387 Color: 112
Size: 76 Color: 47

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1639 Color: 193
Size: 181 Color: 73
Size: 44 Color: 20

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1297 Color: 155
Size: 517 Color: 122
Size: 50 Color: 28

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1497 Color: 170
Size: 309 Color: 103
Size: 58 Color: 36

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 200
Size: 163 Color: 70
Size: 32 Color: 7

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 198
Size: 199 Color: 77
Size: 8 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 201
Size: 161 Color: 69
Size: 30 Color: 6

Total size: 121160
Total free space: 0

