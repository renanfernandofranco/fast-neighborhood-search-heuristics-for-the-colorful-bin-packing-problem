Capicity Bin: 16256
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9252 Color: 420
Size: 5812 Color: 382
Size: 448 Color: 81
Size: 380 Color: 60
Size: 364 Color: 57

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 9284 Color: 422
Size: 5844 Color: 384
Size: 408 Color: 69
Size: 360 Color: 54
Size: 360 Color: 53

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 9332 Color: 423
Size: 5844 Color: 385
Size: 368 Color: 58
Size: 360 Color: 52
Size: 352 Color: 51

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 424
Size: 6568 Color: 394
Size: 352 Color: 50

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9978 Color: 427
Size: 5930 Color: 388
Size: 348 Color: 47

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 429
Size: 5832 Color: 383
Size: 336 Color: 44

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 470
Size: 3528 Color: 335
Size: 704 Color: 136

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 471
Size: 3524 Color: 334
Size: 704 Color: 137

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12098 Color: 474
Size: 2630 Color: 296
Size: 1528 Color: 216

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12114 Color: 475
Size: 3454 Color: 330
Size: 688 Color: 131

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 483
Size: 3546 Color: 336
Size: 290 Color: 25

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12526 Color: 487
Size: 3110 Color: 316
Size: 620 Color: 121

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 488
Size: 3016 Color: 311
Size: 708 Color: 138

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 493
Size: 3302 Color: 323
Size: 308 Color: 32

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 494
Size: 1948 Color: 254
Size: 1660 Color: 228

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12653 Color: 495
Size: 3001 Color: 309
Size: 602 Color: 117

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 498
Size: 1699 Color: 232
Size: 1655 Color: 227

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12960 Color: 500
Size: 1944 Color: 252
Size: 1352 Color: 199

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13006 Color: 504
Size: 2078 Color: 263
Size: 1172 Color: 189

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 508
Size: 2648 Color: 297
Size: 512 Color: 101

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 509
Size: 2622 Color: 295
Size: 532 Color: 105

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 511
Size: 2058 Color: 261
Size: 1022 Color: 177

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 514
Size: 2512 Color: 291
Size: 480 Color: 89

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13267 Color: 515
Size: 2481 Color: 288
Size: 508 Color: 97

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 516
Size: 1828 Color: 243
Size: 1160 Color: 186

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13278 Color: 517
Size: 2702 Color: 301
Size: 276 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 518
Size: 1588 Color: 224
Size: 1389 Color: 206

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 519
Size: 2492 Color: 290
Size: 472 Color: 88

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 520
Size: 2476 Color: 287
Size: 456 Color: 83

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 522
Size: 2408 Color: 283
Size: 480 Color: 90

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 523
Size: 2356 Color: 279
Size: 496 Color: 96

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 526
Size: 1802 Color: 238
Size: 992 Color: 170

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 528
Size: 2296 Color: 277
Size: 448 Color: 82

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 532
Size: 2280 Color: 274
Size: 336 Color: 45

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 533
Size: 2062 Color: 262
Size: 544 Color: 109

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 534
Size: 2056 Color: 260
Size: 540 Color: 107

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 538
Size: 1804 Color: 239
Size: 736 Color: 141

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 540
Size: 1930 Color: 251
Size: 544 Color: 111

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13852 Color: 543
Size: 1972 Color: 255
Size: 432 Color: 77

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 547
Size: 1945 Color: 253
Size: 388 Color: 63

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 549
Size: 1544 Color: 217
Size: 784 Color: 145

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 550
Size: 1818 Color: 241
Size: 508 Color: 98

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 552
Size: 1608 Color: 226
Size: 656 Color: 126

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14009 Color: 553
Size: 1459 Color: 211
Size: 788 Color: 146

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 555
Size: 1352 Color: 201
Size: 864 Color: 157

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 557
Size: 1830 Color: 244
Size: 364 Color: 56

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14094 Color: 561
Size: 1906 Color: 249
Size: 256 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14151 Color: 563
Size: 1481 Color: 214
Size: 624 Color: 122

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 564
Size: 1232 Color: 193
Size: 872 Color: 158

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 566
Size: 1558 Color: 220
Size: 488 Color: 93

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 567
Size: 1352 Color: 200
Size: 692 Color: 134

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14226 Color: 568
Size: 1344 Color: 196
Size: 686 Color: 130

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14242 Color: 570
Size: 1022 Color: 176
Size: 992 Color: 171

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 572
Size: 1200 Color: 192
Size: 790 Color: 149

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 575
Size: 1516 Color: 215
Size: 412 Color: 72

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 576
Size: 1008 Color: 174
Size: 916 Color: 166

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14354 Color: 578
Size: 1582 Color: 223
Size: 320 Color: 38

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14387 Color: 580
Size: 1559 Color: 221
Size: 310 Color: 33

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 581
Size: 1476 Color: 213
Size: 392 Color: 65

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14403 Color: 583
Size: 1545 Color: 218
Size: 308 Color: 31

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 585
Size: 1184 Color: 190
Size: 632 Color: 125

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 588
Size: 1434 Color: 208
Size: 330 Color: 42

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 591
Size: 1546 Color: 219
Size: 192 Color: 8

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 592
Size: 912 Color: 165
Size: 812 Color: 150

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14538 Color: 593
Size: 1344 Color: 197
Size: 374 Color: 59

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 594
Size: 992 Color: 172
Size: 692 Color: 133

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 595
Size: 1248 Color: 194
Size: 424 Color: 75

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14603 Color: 597
Size: 1379 Color: 205
Size: 274 Color: 16

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 14606 Color: 598
Size: 1366 Color: 203
Size: 284 Color: 19

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 599
Size: 1044 Color: 181
Size: 600 Color: 116

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 600
Size: 1040 Color: 180
Size: 598 Color: 115

Bin 72: 1 of cap free
Amount of items: 7
Items: 
Size: 8132 Color: 407
Size: 1708 Color: 234
Size: 1706 Color: 233
Size: 1688 Color: 231
Size: 1685 Color: 230
Size: 924 Color: 167
Size: 412 Color: 71

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 462
Size: 3451 Color: 329
Size: 1072 Color: 182

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 473
Size: 3429 Color: 325
Size: 744 Color: 142

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12590 Color: 490
Size: 3081 Color: 314
Size: 584 Color: 113

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 12637 Color: 492
Size: 3010 Color: 310
Size: 608 Color: 118

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 13507 Color: 527
Size: 2748 Color: 305

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 13705 Color: 537
Size: 2550 Color: 293

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 11048 Color: 449
Size: 5206 Color: 378

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 469
Size: 4076 Color: 350
Size: 176 Color: 7

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 503
Size: 1822 Color: 242
Size: 1444 Color: 209

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 535
Size: 2568 Color: 294

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 541
Size: 2444 Color: 285
Size: 20 Color: 0

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 548
Size: 2330 Color: 278

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 582
Size: 1864 Color: 246

Bin 86: 3 of cap free
Amount of items: 7
Items: 
Size: 8133 Color: 408
Size: 2291 Color: 276
Size: 2124 Color: 265
Size: 1873 Color: 247
Size: 1020 Color: 175
Size: 412 Color: 70
Size: 400 Color: 68

Bin 87: 3 of cap free
Amount of items: 5
Items: 
Size: 8136 Color: 410
Size: 3519 Color: 333
Size: 3166 Color: 318
Size: 1040 Color: 179
Size: 392 Color: 66

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 496
Size: 3596 Color: 337

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 12987 Color: 502
Size: 2710 Color: 302
Size: 556 Color: 112

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 530
Size: 2725 Color: 304

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 531
Size: 2667 Color: 300

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 14089 Color: 560
Size: 2164 Color: 269

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 14507 Color: 590
Size: 1746 Color: 235

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 14591 Color: 596
Size: 1662 Color: 229

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 10244 Color: 435
Size: 6008 Color: 391

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 11370 Color: 455
Size: 4602 Color: 364
Size: 280 Color: 18

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 12596 Color: 491
Size: 3656 Color: 338

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 505
Size: 3204 Color: 321
Size: 34 Color: 1

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13325 Color: 521
Size: 2927 Color: 308

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 556
Size: 2208 Color: 272

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 558
Size: 2184 Color: 271

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 14078 Color: 559
Size: 2174 Color: 270

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 571
Size: 2004 Color: 258

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 14408 Color: 584
Size: 1844 Color: 245

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 546
Size: 2359 Color: 281

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 13966 Color: 551
Size: 2285 Color: 275

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 14235 Color: 569
Size: 2016 Color: 259

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 14444 Color: 586
Size: 1807 Color: 240

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 10786 Color: 442
Size: 5144 Color: 377
Size: 320 Color: 36

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 454
Size: 4626 Color: 365
Size: 288 Color: 20

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 512
Size: 3052 Color: 313

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 14162 Color: 565
Size: 2088 Color: 264

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 14339 Color: 577
Size: 1910 Color: 250

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 14494 Color: 589
Size: 1755 Color: 236

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 421
Size: 6616 Color: 395
Size: 360 Color: 55

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 10160 Color: 433
Size: 6088 Color: 392

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 539
Size: 2482 Color: 289

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 542
Size: 2448 Color: 286

Bin 119: 9 of cap free
Amount of items: 9
Items: 
Size: 8129 Color: 405
Size: 1372 Color: 204
Size: 1354 Color: 202
Size: 1352 Color: 198
Size: 1184 Color: 191
Size: 1172 Color: 188
Size: 832 Color: 153
Size: 428 Color: 76
Size: 424 Color: 74

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 14269 Color: 574
Size: 1978 Color: 257

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 14479 Color: 587
Size: 1768 Color: 237

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13110 Color: 510
Size: 3136 Color: 317

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 13703 Color: 536
Size: 2542 Color: 292

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 525
Size: 2808 Color: 307

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 13886 Color: 545
Size: 2358 Color: 280

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 14362 Color: 579
Size: 1882 Color: 248

Bin 127: 13 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 438
Size: 3466 Color: 331
Size: 2129 Color: 267

Bin 128: 13 of cap free
Amount of items: 2
Items: 
Size: 13863 Color: 544
Size: 2380 Color: 282

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 14100 Color: 562
Size: 2142 Color: 268

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 573
Size: 1974 Color: 256

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 12559 Color: 489
Size: 3682 Color: 339

Bin 132: 16 of cap free
Amount of items: 3
Items: 
Size: 11505 Color: 457
Size: 3439 Color: 328
Size: 1296 Color: 195

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 11788 Color: 465
Size: 4260 Color: 353
Size: 192 Color: 10

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 482
Size: 3752 Color: 340
Size: 96 Color: 2

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 499
Size: 3336 Color: 324

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 14014 Color: 554
Size: 2226 Color: 273

Bin 137: 17 of cap free
Amount of items: 2
Items: 
Size: 11756 Color: 464
Size: 4483 Color: 360

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 13515 Color: 529
Size: 2724 Color: 303

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 11004 Color: 448
Size: 5234 Color: 379

Bin 140: 18 of cap free
Amount of items: 4
Items: 
Size: 11116 Color: 451
Size: 4546 Color: 362
Size: 288 Color: 24
Size: 288 Color: 23

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 13062 Color: 507
Size: 3176 Color: 320

Bin 142: 19 of cap free
Amount of items: 3
Items: 
Size: 10810 Color: 444
Size: 5115 Color: 376
Size: 312 Color: 34

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 12964 Color: 501
Size: 3273 Color: 322

Bin 144: 20 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 447
Size: 4988 Color: 370
Size: 296 Color: 28

Bin 145: 20 of cap free
Amount of items: 3
Items: 
Size: 11402 Color: 456
Size: 4562 Color: 363
Size: 272 Color: 15

Bin 146: 21 of cap free
Amount of items: 3
Items: 
Size: 10127 Color: 432
Size: 5772 Color: 381
Size: 336 Color: 43

Bin 147: 21 of cap free
Amount of items: 2
Items: 
Size: 12035 Color: 472
Size: 4200 Color: 352

Bin 148: 22 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 461
Size: 4442 Color: 358
Size: 262 Color: 12

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 13061 Color: 506
Size: 3172 Color: 319

Bin 150: 26 of cap free
Amount of items: 2
Items: 
Size: 12458 Color: 486
Size: 3772 Color: 343

Bin 151: 28 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 452
Size: 4792 Color: 368
Size: 288 Color: 22

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 524
Size: 2798 Color: 306

Bin 153: 29 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 446
Size: 5044 Color: 372
Size: 304 Color: 29

Bin 154: 29 of cap free
Amount of items: 2
Items: 
Size: 12745 Color: 497
Size: 3482 Color: 332

Bin 155: 31 of cap free
Amount of items: 3
Items: 
Size: 10802 Color: 443
Size: 5111 Color: 375
Size: 312 Color: 35

Bin 156: 31 of cap free
Amount of items: 2
Items: 
Size: 12264 Color: 481
Size: 3961 Color: 348

Bin 157: 32 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 467
Size: 4344 Color: 356

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 485
Size: 3768 Color: 342

Bin 159: 33 of cap free
Amount of items: 2
Items: 
Size: 13206 Color: 513
Size: 3017 Color: 312

Bin 160: 35 of cap free
Amount of items: 3
Items: 
Size: 10123 Color: 431
Size: 3435 Color: 327
Size: 2663 Color: 299

Bin 161: 37 of cap free
Amount of items: 3
Items: 
Size: 10010 Color: 428
Size: 5869 Color: 387
Size: 340 Color: 46

Bin 162: 37 of cap free
Amount of items: 2
Items: 
Size: 12115 Color: 476
Size: 4104 Color: 351

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 11838 Color: 466
Size: 4380 Color: 357

Bin 164: 39 of cap free
Amount of items: 2
Items: 
Size: 11736 Color: 463
Size: 4481 Color: 359

Bin 165: 42 of cap free
Amount of items: 4
Items: 
Size: 11084 Color: 450
Size: 4542 Color: 361
Size: 296 Color: 27
Size: 292 Color: 26

Bin 166: 42 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 468
Size: 4074 Color: 349
Size: 192 Color: 9

Bin 167: 44 of cap free
Amount of items: 5
Items: 
Size: 8134 Color: 409
Size: 3108 Color: 315
Size: 2443 Color: 284
Size: 2127 Color: 266
Size: 400 Color: 67

Bin 168: 44 of cap free
Amount of items: 3
Items: 
Size: 10119 Color: 430
Size: 3431 Color: 326
Size: 2662 Color: 298

Bin 169: 46 of cap free
Amount of items: 2
Items: 
Size: 10276 Color: 436
Size: 5934 Color: 389

Bin 170: 48 of cap free
Amount of items: 2
Items: 
Size: 12452 Color: 484
Size: 3756 Color: 341

Bin 171: 52 of cap free
Amount of items: 2
Items: 
Size: 10204 Color: 434
Size: 6000 Color: 390

Bin 172: 52 of cap free
Amount of items: 3
Items: 
Size: 12139 Color: 479
Size: 3953 Color: 346
Size: 112 Color: 4

Bin 173: 52 of cap free
Amount of items: 3
Items: 
Size: 12143 Color: 480
Size: 3957 Color: 347
Size: 104 Color: 3

Bin 174: 59 of cap free
Amount of items: 3
Items: 
Size: 12131 Color: 477
Size: 3942 Color: 344
Size: 124 Color: 6

Bin 175: 62 of cap free
Amount of items: 3
Items: 
Size: 12135 Color: 478
Size: 3947 Color: 345
Size: 112 Color: 5

Bin 176: 63 of cap free
Amount of items: 3
Items: 
Size: 10877 Color: 445
Size: 5012 Color: 371
Size: 304 Color: 30

Bin 177: 67 of cap free
Amount of items: 2
Items: 
Size: 11509 Color: 458
Size: 4680 Color: 367

Bin 178: 89 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 441
Size: 5109 Color: 374
Size: 320 Color: 37

Bin 179: 90 of cap free
Amount of items: 19
Items: 
Size: 1168 Color: 187
Size: 1160 Color: 185
Size: 1152 Color: 184
Size: 1152 Color: 183
Size: 1024 Color: 178
Size: 1000 Color: 173
Size: 928 Color: 169
Size: 928 Color: 168
Size: 912 Color: 164
Size: 908 Color: 163
Size: 908 Color: 162
Size: 904 Color: 161
Size: 896 Color: 160
Size: 896 Color: 159
Size: 464 Color: 85
Size: 458 Color: 84
Size: 444 Color: 80
Size: 432 Color: 79
Size: 432 Color: 78

Bin 180: 92 of cap free
Amount of items: 2
Items: 
Size: 8148 Color: 411
Size: 8016 Color: 404

Bin 181: 94 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 453
Size: 4650 Color: 366
Size: 288 Color: 21

Bin 182: 124 of cap free
Amount of items: 3
Items: 
Size: 10706 Color: 440
Size: 5104 Color: 373
Size: 322 Color: 39

Bin 183: 134 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 414
Size: 6770 Color: 397
Size: 384 Color: 61

Bin 184: 147 of cap free
Amount of items: 3
Items: 
Size: 11521 Color: 460
Size: 4316 Color: 355
Size: 272 Color: 13

Bin 185: 187 of cap free
Amount of items: 3
Items: 
Size: 11513 Color: 459
Size: 4284 Color: 354
Size: 272 Color: 14

Bin 186: 222 of cap free
Amount of items: 7
Items: 
Size: 8130 Color: 406
Size: 1604 Color: 225
Size: 1564 Color: 222
Size: 1470 Color: 212
Size: 1450 Color: 210
Size: 1400 Color: 207
Size: 416 Color: 73

Bin 187: 223 of cap free
Amount of items: 3
Items: 
Size: 9816 Color: 426
Size: 5867 Color: 386
Size: 350 Color: 48

Bin 188: 224 of cap free
Amount of items: 3
Items: 
Size: 9448 Color: 425
Size: 6232 Color: 393
Size: 352 Color: 49

Bin 189: 236 of cap free
Amount of items: 2
Items: 
Size: 9244 Color: 419
Size: 6776 Color: 402

Bin 190: 265 of cap free
Amount of items: 2
Items: 
Size: 9217 Color: 418
Size: 6774 Color: 401

Bin 191: 266 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 439
Size: 4984 Color: 369
Size: 328 Color: 40

Bin 192: 268 of cap free
Amount of items: 2
Items: 
Size: 9215 Color: 417
Size: 6773 Color: 400

Bin 193: 280 of cap free
Amount of items: 3
Items: 
Size: 10280 Color: 437
Size: 5368 Color: 380
Size: 328 Color: 41

Bin 194: 316 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 413
Size: 6764 Color: 396
Size: 384 Color: 62

Bin 195: 342 of cap free
Amount of items: 2
Items: 
Size: 9142 Color: 416
Size: 6772 Color: 399

Bin 196: 347 of cap free
Amount of items: 2
Items: 
Size: 9138 Color: 415
Size: 6771 Color: 398

Bin 197: 352 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 412
Size: 7120 Color: 403
Size: 392 Color: 64

Bin 198: 498 of cap free
Amount of items: 24
Items: 
Size: 856 Color: 156
Size: 856 Color: 155
Size: 848 Color: 154
Size: 816 Color: 152
Size: 816 Color: 151
Size: 790 Color: 148
Size: 790 Color: 147
Size: 752 Color: 144
Size: 752 Color: 143
Size: 720 Color: 140
Size: 712 Color: 139
Size: 702 Color: 135
Size: 690 Color: 132
Size: 686 Color: 129
Size: 524 Color: 103
Size: 524 Color: 102
Size: 512 Color: 100
Size: 512 Color: 99
Size: 496 Color: 95
Size: 496 Color: 94
Size: 488 Color: 92
Size: 488 Color: 91
Size: 468 Color: 87
Size: 464 Color: 86

Bin 199: 9638 of cap free
Amount of items: 11
Items: 
Size: 686 Color: 128
Size: 684 Color: 127
Size: 632 Color: 124
Size: 632 Color: 123
Size: 616 Color: 120
Size: 616 Color: 119
Size: 592 Color: 114
Size: 544 Color: 110
Size: 544 Color: 108
Size: 540 Color: 106
Size: 532 Color: 104

Total size: 3218688
Total free space: 16256

