Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 284 Color: 1
Size: 268 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 1
Size: 251 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 319 Color: 1
Size: 306 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 291 Color: 1
Size: 281 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 287 Color: 1
Size: 266 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 265 Color: 1
Size: 251 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 275 Color: 1
Size: 260 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 273 Color: 1
Size: 257 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 351 Color: 1
Size: 275 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 308 Color: 1
Size: 297 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 362 Color: 1
Size: 252 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 278 Color: 1
Size: 263 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 0
Size: 251 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 271 Color: 1
Size: 268 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 289 Color: 1
Size: 250 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 307 Color: 1
Size: 266 Color: 0
Size: 428 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 332 Color: 1
Size: 269 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 289 Color: 1
Size: 260 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 336 Color: 1
Size: 305 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 323 Color: 1
Size: 265 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 278 Color: 0
Size: 273 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 316 Color: 1
Size: 251 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 338 Color: 1
Size: 287 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 340 Color: 1
Size: 266 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 1
Size: 251 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 267 Color: 1
Size: 252 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 308 Color: 1
Size: 300 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 315 Color: 1
Size: 304 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 1
Size: 365 Color: 1
Size: 299 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 335 Color: 1
Size: 267 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 312 Color: 1
Size: 277 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 311 Color: 1
Size: 306 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 294 Color: 1
Size: 252 Color: 0

Total size: 34034
Total free space: 0

