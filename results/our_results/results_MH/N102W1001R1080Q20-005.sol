Capicity Bin: 1001
Lower Bound: 46

Bins used: 46
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 17
Size: 259 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 7
Size: 398 Color: 7
Size: 143 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 15
Size: 141 Color: 8
Size: 136 Color: 16

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 11
Size: 445 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 14
Size: 155 Color: 17
Size: 103 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 1
Size: 111 Color: 0
Size: 283 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 530 Color: 1
Size: 358 Color: 10
Size: 113 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 503 Color: 14
Size: 397 Color: 4
Size: 101 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 10
Size: 164 Color: 1
Size: 302 Color: 8

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 5
Size: 320 Color: 2

Bin 11: 1 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 244 Color: 9

Bin 12: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 5
Size: 289 Color: 19

Bin 13: 1 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 2
Size: 205 Color: 1

Bin 14: 1 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 1
Size: 412 Color: 12

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 19
Size: 240 Color: 17
Size: 141 Color: 4

Bin 16: 2 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 4
Size: 336 Color: 1

Bin 17: 2 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 6
Size: 280 Color: 10

Bin 18: 2 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 14
Size: 229 Color: 0

Bin 19: 3 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 8
Size: 271 Color: 4

Bin 20: 4 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 9
Size: 339 Color: 17

Bin 21: 4 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 18
Size: 196 Color: 17

Bin 22: 4 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 7
Size: 350 Color: 13

Bin 23: 4 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 14
Size: 353 Color: 15

Bin 24: 5 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 14
Size: 460 Color: 7

Bin 25: 5 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 19
Size: 343 Color: 0

Bin 26: 5 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 18
Size: 264 Color: 4

Bin 27: 7 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 4
Size: 318 Color: 0

Bin 28: 7 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 10
Size: 268 Color: 12

Bin 29: 7 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 5
Size: 385 Color: 10

Bin 30: 7 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 10
Size: 450 Color: 3

Bin 31: 9 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 17
Size: 290 Color: 7

Bin 32: 11 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 12
Size: 432 Color: 0

Bin 33: 12 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 11
Size: 303 Color: 10

Bin 34: 12 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 18
Size: 450 Color: 10

Bin 35: 12 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 17
Size: 221 Color: 4

Bin 36: 13 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 1
Size: 489 Color: 19

Bin 37: 13 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 16
Size: 418 Color: 12

Bin 38: 16 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 4
Size: 210 Color: 10
Size: 100 Color: 14

Bin 39: 17 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 16
Size: 236 Color: 15

Bin 40: 17 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 17
Size: 172 Color: 7
Size: 147 Color: 13

Bin 41: 21 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 17
Size: 349 Color: 9

Bin 42: 24 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 14
Size: 421 Color: 19

Bin 43: 27 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 4
Size: 365 Color: 13

Bin 44: 38 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 2
Size: 219 Color: 14

Bin 45: 46 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 19
Size: 284 Color: 3

Bin 46: 80 of cap free
Amount of items: 2
Items: 
Size: 395 Color: 14
Size: 526 Color: 9

Total size: 45605
Total free space: 441

