Capicity Bin: 2068
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 696 Color: 1
Size: 480 Color: 1
Size: 428 Color: 1
Size: 232 Color: 0
Size: 232 Color: 0

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 1428 Color: 1
Size: 320 Color: 1
Size: 112 Color: 1
Size: 76 Color: 0
Size: 68 Color: 0
Size: 64 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 362 Color: 1
Size: 68 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 1486 Color: 1
Size: 486 Color: 1
Size: 48 Color: 1
Size: 40 Color: 0
Size: 8 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 1
Size: 221 Color: 1
Size: 44 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 1
Size: 267 Color: 1
Size: 52 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1853 Color: 1
Size: 181 Color: 1
Size: 34 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 1
Size: 281 Color: 1
Size: 54 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 218 Color: 1
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 1
Size: 507 Color: 1
Size: 100 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1035 Color: 1
Size: 861 Color: 1
Size: 172 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 1
Size: 346 Color: 1
Size: 68 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 1
Size: 630 Color: 1
Size: 124 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 1
Size: 383 Color: 1
Size: 76 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 1
Size: 421 Color: 1
Size: 84 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 1
Size: 342 Color: 1
Size: 64 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 178 Color: 1
Size: 32 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 1
Size: 291 Color: 1
Size: 58 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 1
Size: 266 Color: 1
Size: 52 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 1
Size: 274 Color: 1
Size: 52 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1053 Color: 1
Size: 847 Color: 1
Size: 168 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 1
Size: 862 Color: 1
Size: 168 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 1
Size: 261 Color: 1
Size: 42 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 1
Size: 537 Color: 1
Size: 106 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 1
Size: 217 Color: 1
Size: 42 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 1
Size: 187 Color: 1
Size: 36 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 1
Size: 386 Color: 1
Size: 76 Color: 0

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 1300 Color: 1
Size: 768 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1641 Color: 1
Size: 357 Color: 1
Size: 70 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1817 Color: 1
Size: 211 Color: 1
Size: 40 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 1
Size: 214 Color: 1
Size: 40 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 1
Size: 434 Color: 1
Size: 84 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 1
Size: 247 Color: 1
Size: 48 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 1
Size: 282 Color: 1
Size: 56 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 1
Size: 261 Color: 1
Size: 50 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 1
Size: 522 Color: 1
Size: 100 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 1378 Color: 1
Size: 578 Color: 1
Size: 96 Color: 0
Size: 16 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 1
Size: 351 Color: 1
Size: 68 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 354 Color: 1
Size: 68 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 1
Size: 329 Color: 1
Size: 64 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1612 Color: 1
Size: 400 Color: 1
Size: 56 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 402 Color: 1
Size: 76 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 1
Size: 651 Color: 1
Size: 128 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 282 Color: 1
Size: 52 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 694 Color: 1
Size: 136 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 1
Size: 439 Color: 1
Size: 24 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 1
Size: 201 Color: 1
Size: 38 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 1
Size: 244 Color: 1
Size: 48 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 1
Size: 737 Color: 1
Size: 146 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 1
Size: 513 Color: 1
Size: 102 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 1
Size: 521 Color: 1
Size: 102 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 1
Size: 273 Color: 1
Size: 54 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 1
Size: 438 Color: 1
Size: 8 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 1
Size: 417 Color: 1
Size: 82 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 1
Size: 841 Color: 1
Size: 50 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 693 Color: 1
Size: 138 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 331 Color: 1
Size: 64 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 1
Size: 853 Color: 1
Size: 170 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 1
Size: 889 Color: 1
Size: 142 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 1
Size: 193 Color: 1
Size: 38 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 1
Size: 770 Color: 1
Size: 152 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 1
Size: 609 Color: 1
Size: 120 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1373 Color: 1
Size: 581 Color: 1
Size: 114 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 1
Size: 461 Color: 1
Size: 90 Color: 0

Total size: 134420
Total free space: 0

