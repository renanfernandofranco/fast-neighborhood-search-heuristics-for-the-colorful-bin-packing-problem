Capicity Bin: 7888
Lower Bound: 132

Bins used: 132
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3912 Color: 0
Size: 1064 Color: 0
Size: 960 Color: 4
Size: 640 Color: 4
Size: 480 Color: 4
Size: 448 Color: 4
Size: 384 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 3
Size: 776 Color: 0
Size: 144 Color: 4

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 5628 Color: 3
Size: 1884 Color: 2
Size: 168 Color: 3
Size: 128 Color: 1
Size: 80 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6390 Color: 3
Size: 1250 Color: 4
Size: 248 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 2
Size: 1366 Color: 4
Size: 272 Color: 4

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 6836 Color: 1
Size: 884 Color: 4
Size: 120 Color: 3
Size: 48 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6210 Color: 0
Size: 1402 Color: 2
Size: 276 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 1
Size: 1351 Color: 3
Size: 270 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 0
Size: 1124 Color: 2
Size: 216 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 1
Size: 2122 Color: 0
Size: 420 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 3
Size: 1819 Color: 3
Size: 342 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 4
Size: 1245 Color: 2
Size: 76 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 0
Size: 1423 Color: 1
Size: 284 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 3
Size: 942 Color: 3
Size: 188 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 1
Size: 1503 Color: 4
Size: 300 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 0
Size: 1165 Color: 0
Size: 148 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 3
Size: 1234 Color: 4
Size: 244 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6170 Color: 0
Size: 1434 Color: 1
Size: 284 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 3
Size: 1018 Color: 2
Size: 200 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 2
Size: 724 Color: 0
Size: 144 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4548 Color: 0
Size: 2788 Color: 0
Size: 552 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 0
Size: 916 Color: 1
Size: 176 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 4
Size: 724 Color: 4
Size: 136 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 3
Size: 1188 Color: 3
Size: 232 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 0
Size: 1466 Color: 0
Size: 292 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 4
Size: 1646 Color: 4
Size: 328 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 4
Size: 1139 Color: 4
Size: 226 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 3
Size: 1972 Color: 0
Size: 392 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 4
Size: 810 Color: 2
Size: 160 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 1
Size: 1862 Color: 4
Size: 372 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 3
Size: 1209 Color: 3
Size: 240 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 3
Size: 1484 Color: 2
Size: 288 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 0
Size: 2124 Color: 0
Size: 416 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5865 Color: 2
Size: 1687 Color: 4
Size: 336 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 4
Size: 702 Color: 0
Size: 136 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4060 Color: 2
Size: 3652 Color: 1
Size: 176 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 2
Size: 1924 Color: 0
Size: 384 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4611 Color: 0
Size: 3037 Color: 4
Size: 240 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6570 Color: 4
Size: 1102 Color: 4
Size: 216 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 0
Size: 2076 Color: 1
Size: 408 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5963 Color: 2
Size: 1605 Color: 4
Size: 320 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 4
Size: 956 Color: 2
Size: 184 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 0
Size: 706 Color: 3
Size: 140 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 0
Size: 1028 Color: 1
Size: 200 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 4
Size: 1506 Color: 0
Size: 300 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 4510 Color: 3
Size: 3238 Color: 4
Size: 140 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 3974 Color: 4
Size: 3290 Color: 0
Size: 624 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 4
Size: 2838 Color: 3
Size: 564 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4695 Color: 0
Size: 2969 Color: 3
Size: 224 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6299 Color: 1
Size: 1325 Color: 3
Size: 264 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 4
Size: 989 Color: 1
Size: 196 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6407 Color: 4
Size: 1235 Color: 1
Size: 246 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 2
Size: 1114 Color: 4
Size: 220 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 4
Size: 2386 Color: 2
Size: 476 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6164 Color: 0
Size: 1444 Color: 2
Size: 280 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 2
Size: 1154 Color: 0
Size: 228 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4494 Color: 4
Size: 2830 Color: 2
Size: 564 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 3464 Color: 0
Size: 2928 Color: 0
Size: 1496 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 0
Size: 1134 Color: 1
Size: 80 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 0
Size: 2524 Color: 4
Size: 496 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 2
Size: 2562 Color: 0
Size: 512 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 4
Size: 1660 Color: 1
Size: 328 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6014 Color: 2
Size: 1562 Color: 2
Size: 312 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 3
Size: 852 Color: 2
Size: 168 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 3
Size: 1068 Color: 1
Size: 208 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 2
Size: 2804 Color: 2
Size: 552 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 0
Size: 862 Color: 4
Size: 172 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 1
Size: 870 Color: 4
Size: 172 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 2
Size: 938 Color: 4
Size: 184 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 1
Size: 722 Color: 3
Size: 144 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 1
Size: 2452 Color: 2
Size: 488 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4438 Color: 2
Size: 2878 Color: 4
Size: 572 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5775 Color: 3
Size: 1761 Color: 4
Size: 352 Color: 3

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 3
Size: 1859 Color: 0
Size: 370 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 3958 Color: 4
Size: 3278 Color: 0
Size: 652 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 0
Size: 1204 Color: 0
Size: 232 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6922 Color: 1
Size: 806 Color: 0
Size: 160 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 4
Size: 988 Color: 0
Size: 192 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 2
Size: 3124 Color: 1
Size: 616 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6373 Color: 1
Size: 1263 Color: 3
Size: 252 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5948 Color: 0
Size: 1620 Color: 4
Size: 320 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 0
Size: 2242 Color: 0
Size: 444 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 2
Size: 666 Color: 4
Size: 132 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6735 Color: 3
Size: 1049 Color: 1
Size: 104 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6838 Color: 2
Size: 878 Color: 1
Size: 172 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 2
Size: 1054 Color: 3
Size: 208 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 2
Size: 700 Color: 4
Size: 136 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 3948 Color: 1
Size: 3284 Color: 1
Size: 656 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4401 Color: 0
Size: 2907 Color: 2
Size: 580 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 2
Size: 764 Color: 3
Size: 144 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 0
Size: 3270 Color: 2
Size: 652 Color: 2

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6862 Color: 4
Size: 858 Color: 0
Size: 168 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 0
Size: 1332 Color: 2
Size: 264 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 3
Size: 1374 Color: 0
Size: 152 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 0
Size: 1537 Color: 4
Size: 306 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4815 Color: 2
Size: 2561 Color: 1
Size: 512 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5774 Color: 3
Size: 1762 Color: 0
Size: 352 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 1
Size: 1106 Color: 1
Size: 220 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 1
Size: 754 Color: 1
Size: 148 Color: 2

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4502 Color: 0
Size: 2822 Color: 2
Size: 564 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6304 Color: 3
Size: 1208 Color: 1
Size: 376 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 4
Size: 682 Color: 1
Size: 132 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 2
Size: 1222 Color: 2
Size: 240 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 4
Size: 3286 Color: 3
Size: 656 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 1
Size: 770 Color: 2
Size: 152 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 0
Size: 662 Color: 0
Size: 128 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 2
Size: 1348 Color: 2
Size: 200 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5459 Color: 1
Size: 2073 Color: 4
Size: 356 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5923 Color: 2
Size: 1639 Color: 3
Size: 326 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 3
Size: 796 Color: 1
Size: 152 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 0
Size: 2124 Color: 2
Size: 424 Color: 3

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 4143 Color: 2
Size: 3121 Color: 4
Size: 624 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 1
Size: 2388 Color: 4
Size: 472 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 2
Size: 974 Color: 0
Size: 192 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 3950 Color: 3
Size: 3406 Color: 3
Size: 532 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 3
Size: 1111 Color: 0
Size: 222 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 4
Size: 1700 Color: 1
Size: 336 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 3945 Color: 0
Size: 3645 Color: 4
Size: 298 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 2
Size: 2028 Color: 4
Size: 384 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 1
Size: 1175 Color: 0
Size: 58 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4755 Color: 3
Size: 2611 Color: 3
Size: 522 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5466 Color: 4
Size: 2022 Color: 1
Size: 400 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 4
Size: 1359 Color: 1
Size: 270 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 1
Size: 1023 Color: 4
Size: 204 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5337 Color: 0
Size: 2127 Color: 4
Size: 424 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5419 Color: 0
Size: 2059 Color: 2
Size: 410 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5073 Color: 2
Size: 2347 Color: 0
Size: 468 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6187 Color: 1
Size: 1465 Color: 1
Size: 236 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6607 Color: 1
Size: 1069 Color: 1
Size: 212 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 4
Size: 2265 Color: 4
Size: 452 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 3949 Color: 0
Size: 3283 Color: 4
Size: 656 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5611 Color: 3
Size: 2099 Color: 2
Size: 178 Color: 4

Total size: 1041216
Total free space: 0

