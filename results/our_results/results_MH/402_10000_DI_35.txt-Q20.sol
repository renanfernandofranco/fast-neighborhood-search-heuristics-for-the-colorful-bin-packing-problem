Capicity Bin: 8136
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4024 Color: 13
Size: 1120 Color: 19
Size: 1008 Color: 4
Size: 656 Color: 12
Size: 520 Color: 0
Size: 448 Color: 16
Size: 360 Color: 18

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 6071 Color: 2
Size: 1721 Color: 14
Size: 184 Color: 7
Size: 120 Color: 13
Size: 40 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 10
Size: 1052 Color: 8
Size: 208 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 12
Size: 748 Color: 18
Size: 144 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7316 Color: 7
Size: 684 Color: 4
Size: 136 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 8
Size: 790 Color: 6
Size: 156 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6654 Color: 3
Size: 1238 Color: 19
Size: 244 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 8
Size: 2910 Color: 17
Size: 580 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4071 Color: 6
Size: 3389 Color: 8
Size: 676 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 9
Size: 764 Color: 19
Size: 152 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 3576 Color: 16
Size: 3040 Color: 5
Size: 1520 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6113 Color: 4
Size: 1687 Color: 19
Size: 336 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6073 Color: 8
Size: 1721 Color: 13
Size: 342 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 4
Size: 1498 Color: 15
Size: 296 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 6
Size: 1010 Color: 19
Size: 200 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4069 Color: 4
Size: 3391 Color: 18
Size: 676 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 0
Size: 1390 Color: 17
Size: 276 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 10
Size: 1244 Color: 5
Size: 240 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 5
Size: 2983 Color: 12
Size: 596 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 9
Size: 2900 Color: 8
Size: 576 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 7
Size: 1042 Color: 9
Size: 208 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 2
Size: 698 Color: 4
Size: 136 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6251 Color: 16
Size: 1571 Color: 10
Size: 314 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4070 Color: 11
Size: 3390 Color: 10
Size: 676 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 1
Size: 804 Color: 5
Size: 152 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 16
Size: 3322 Color: 13
Size: 664 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6806 Color: 6
Size: 1110 Color: 3
Size: 220 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 0
Size: 2230 Color: 4
Size: 444 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 7
Size: 2059 Color: 5
Size: 410 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 19
Size: 828 Color: 12
Size: 160 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 0
Size: 1276 Color: 11
Size: 248 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6510 Color: 13
Size: 1358 Color: 13
Size: 268 Color: 5

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 14
Size: 1964 Color: 3
Size: 392 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7264 Color: 15
Size: 728 Color: 9
Size: 144 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 3
Size: 1875 Color: 2
Size: 374 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 6
Size: 1356 Color: 17
Size: 264 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 18
Size: 1932 Color: 7
Size: 384 Color: 5

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 7020 Color: 1
Size: 932 Color: 3
Size: 104 Color: 7
Size: 80 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 15
Size: 1660 Color: 12
Size: 328 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 9
Size: 1942 Color: 19
Size: 388 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7122 Color: 18
Size: 846 Color: 9
Size: 168 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 7
Size: 2684 Color: 10
Size: 536 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5683 Color: 2
Size: 2045 Color: 12
Size: 408 Color: 12

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 17
Size: 1495 Color: 19
Size: 230 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 12
Size: 2412 Color: 6
Size: 480 Color: 14

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 1
Size: 2292 Color: 17
Size: 456 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 5
Size: 922 Color: 13
Size: 184 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 17
Size: 1299 Color: 12
Size: 258 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5830 Color: 16
Size: 1922 Color: 0
Size: 384 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 7
Size: 2260 Color: 10
Size: 448 Color: 15

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 1
Size: 1078 Color: 16
Size: 212 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7232 Color: 8
Size: 760 Color: 1
Size: 144 Color: 13

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 13
Size: 1591 Color: 17
Size: 318 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6326 Color: 18
Size: 1510 Color: 14
Size: 300 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6097 Color: 0
Size: 1809 Color: 3
Size: 230 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 5
Size: 3342 Color: 16
Size: 668 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 5078 Color: 18
Size: 2550 Color: 0
Size: 508 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 5
Size: 1419 Color: 16
Size: 282 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 19
Size: 2060 Color: 3
Size: 408 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 3
Size: 860 Color: 12
Size: 136 Color: 5

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6286 Color: 5
Size: 1638 Color: 4
Size: 212 Color: 5

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6513 Color: 13
Size: 1353 Color: 18
Size: 270 Color: 17

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 15
Size: 774 Color: 1
Size: 116 Color: 8

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5446 Color: 18
Size: 2278 Color: 16
Size: 412 Color: 7

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 16
Size: 2278 Color: 13
Size: 452 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 7
Size: 1272 Color: 14
Size: 344 Color: 17

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4935 Color: 10
Size: 2669 Color: 11
Size: 532 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 9
Size: 2436 Color: 5
Size: 480 Color: 7

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 7011 Color: 11
Size: 1047 Color: 16
Size: 78 Color: 18

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 11
Size: 1084 Color: 7
Size: 216 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 9
Size: 1059 Color: 13
Size: 210 Color: 17

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4949 Color: 8
Size: 2657 Color: 5
Size: 530 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6086 Color: 1
Size: 1710 Color: 0
Size: 340 Color: 10

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5750 Color: 9
Size: 1990 Color: 0
Size: 396 Color: 10

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 17
Size: 1302 Color: 5
Size: 260 Color: 12

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4965 Color: 2
Size: 2643 Color: 2
Size: 528 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 17
Size: 3204 Color: 4
Size: 640 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 15
Size: 1468 Color: 13
Size: 288 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 13
Size: 1742 Color: 8
Size: 348 Color: 16

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 3
Size: 1148 Color: 12
Size: 224 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6633 Color: 18
Size: 1253 Color: 15
Size: 250 Color: 13

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7082 Color: 14
Size: 882 Color: 2
Size: 172 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 12
Size: 2218 Color: 12
Size: 440 Color: 10

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 1
Size: 3052 Color: 5
Size: 608 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 9
Size: 710 Color: 7
Size: 140 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6118 Color: 2
Size: 1682 Color: 0
Size: 336 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4686 Color: 19
Size: 2890 Color: 14
Size: 560 Color: 8

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6471 Color: 17
Size: 1499 Color: 19
Size: 166 Color: 10

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 11
Size: 1692 Color: 1
Size: 336 Color: 10

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 1
Size: 2347 Color: 15
Size: 422 Color: 15

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 9
Size: 732 Color: 8
Size: 144 Color: 10

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 0
Size: 1412 Color: 9
Size: 264 Color: 13

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 10
Size: 914 Color: 17
Size: 180 Color: 17

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 6
Size: 874 Color: 8
Size: 172 Color: 18

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 5118 Color: 18
Size: 2562 Color: 1
Size: 456 Color: 5

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 7
Size: 1142 Color: 5
Size: 228 Color: 10

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6887 Color: 6
Size: 1041 Color: 19
Size: 208 Color: 9

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 11
Size: 1203 Color: 8
Size: 214 Color: 6

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5289 Color: 7
Size: 2503 Color: 4
Size: 344 Color: 2

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 11
Size: 1564 Color: 2
Size: 304 Color: 7

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 19
Size: 1124 Color: 8
Size: 216 Color: 12

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 17
Size: 2997 Color: 1
Size: 598 Color: 18

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 10
Size: 2568 Color: 1
Size: 496 Color: 8

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 18
Size: 3382 Color: 14
Size: 676 Color: 13

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5903 Color: 11
Size: 1861 Color: 9
Size: 372 Color: 14

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 3
Size: 722 Color: 9
Size: 144 Color: 12

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 6
Size: 1357 Color: 3
Size: 20 Color: 9

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 10
Size: 3388 Color: 7
Size: 672 Color: 10

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6969 Color: 13
Size: 1149 Color: 1
Size: 18 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7198 Color: 3
Size: 782 Color: 18
Size: 156 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 13
Size: 2022 Color: 10
Size: 292 Color: 11

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 19
Size: 1274 Color: 11
Size: 192 Color: 14

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 7
Size: 2596 Color: 19
Size: 512 Color: 12

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 14
Size: 1811 Color: 3
Size: 42 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6695 Color: 2
Size: 1247 Color: 17
Size: 194 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 16
Size: 1278 Color: 6
Size: 252 Color: 5

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 0
Size: 1202 Color: 18
Size: 240 Color: 6

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6542 Color: 5
Size: 1478 Color: 14
Size: 116 Color: 7

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 5
Size: 924 Color: 8
Size: 184 Color: 17

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5058 Color: 3
Size: 2566 Color: 18
Size: 512 Color: 15

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5790 Color: 6
Size: 1958 Color: 15
Size: 388 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 7162 Color: 6
Size: 814 Color: 19
Size: 160 Color: 6

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 7
Size: 982 Color: 19
Size: 192 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 1
Size: 1020 Color: 2
Size: 200 Color: 9

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 9
Size: 3518 Color: 7
Size: 532 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 13
Size: 1559 Color: 9
Size: 310 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5627 Color: 3
Size: 2091 Color: 7
Size: 418 Color: 18

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 9
Size: 1115 Color: 13
Size: 222 Color: 2

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 19
Size: 2079 Color: 8
Size: 414 Color: 18

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5355 Color: 10
Size: 2319 Color: 9
Size: 462 Color: 12

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5371 Color: 5
Size: 2305 Color: 6
Size: 460 Color: 5

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6945 Color: 7
Size: 993 Color: 19
Size: 198 Color: 15

Total size: 1073952
Total free space: 0

