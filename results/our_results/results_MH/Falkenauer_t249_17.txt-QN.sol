Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 226
Size: 287 Color: 90
Size: 259 Color: 32

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 233
Size: 275 Color: 72
Size: 257 Color: 24

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 232
Size: 281 Color: 82
Size: 254 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 180
Size: 366 Color: 175
Size: 260 Color: 37

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 192
Size: 357 Color: 160
Size: 258 Color: 29

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 247
Size: 255 Color: 17
Size: 251 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 223
Size: 285 Color: 88
Size: 274 Color: 66

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 220
Size: 301 Color: 109
Size: 263 Color: 50

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 149
Size: 327 Color: 134
Size: 324 Color: 129

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 174
Size: 352 Color: 154
Size: 282 Color: 85

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 235
Size: 268 Color: 60
Size: 261 Color: 40

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 238
Size: 260 Color: 38
Size: 260 Color: 34

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 191
Size: 333 Color: 139
Size: 282 Color: 83

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 194
Size: 358 Color: 163
Size: 256 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 190
Size: 358 Color: 162
Size: 258 Color: 28

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 200
Size: 312 Color: 121
Size: 289 Color: 94

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 211
Size: 302 Color: 111
Size: 281 Color: 81

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 208
Size: 311 Color: 120
Size: 274 Color: 68

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 215
Size: 291 Color: 98
Size: 282 Color: 84

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 231
Size: 275 Color: 69
Size: 261 Color: 41

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 228
Size: 279 Color: 79
Size: 261 Color: 44

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 167
Size: 352 Color: 151
Size: 286 Color: 89

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 241
Size: 260 Color: 35
Size: 253 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 218
Size: 296 Color: 104
Size: 269 Color: 61

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 248
Size: 252 Color: 8
Size: 250 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 169
Size: 359 Color: 165
Size: 278 Color: 78

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 206
Size: 330 Color: 136
Size: 258 Color: 27

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 201
Size: 336 Color: 141
Size: 264 Color: 53

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 182
Size: 365 Color: 171
Size: 259 Color: 31

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 155
Size: 339 Color: 142
Size: 309 Color: 118

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 188
Size: 357 Color: 161
Size: 263 Color: 51

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 207
Size: 314 Color: 123
Size: 271 Color: 63

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 198
Size: 352 Color: 153
Size: 250 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 193
Size: 354 Color: 157
Size: 261 Color: 43

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 236
Size: 268 Color: 56
Size: 260 Color: 33

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 196
Size: 350 Color: 150
Size: 260 Color: 39

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 189
Size: 368 Color: 177
Size: 251 Color: 7

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 195
Size: 353 Color: 156
Size: 261 Color: 42

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 239
Size: 268 Color: 59
Size: 250 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 170
Size: 355 Color: 158
Size: 280 Color: 80

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 202
Size: 303 Color: 112
Size: 295 Color: 100

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 240
Size: 266 Color: 54
Size: 250 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 234
Size: 268 Color: 55
Size: 262 Color: 46

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 217
Size: 290 Color: 96
Size: 275 Color: 71

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 210
Size: 319 Color: 127
Size: 264 Color: 52

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 237
Size: 263 Color: 47
Size: 260 Color: 36

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 205
Size: 297 Color: 106
Size: 296 Color: 105

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 216
Size: 295 Color: 101
Size: 274 Color: 67

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 245
Size: 256 Color: 20
Size: 252 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 164
Size: 324 Color: 130
Size: 317 Color: 126

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 204
Size: 341 Color: 144
Size: 257 Color: 23

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 221
Size: 291 Color: 97
Size: 273 Color: 65

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 242
Size: 256 Color: 21
Size: 255 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 179
Size: 332 Color: 138
Size: 296 Color: 103

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 212
Size: 308 Color: 115
Size: 268 Color: 58

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 225
Size: 288 Color: 93
Size: 269 Color: 62

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 224
Size: 300 Color: 108
Size: 258 Color: 26

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 152
Size: 325 Color: 131
Size: 323 Color: 128

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 176
Size: 328 Color: 135
Size: 305 Color: 113

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 246
Size: 254 Color: 14
Size: 253 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 230
Size: 288 Color: 92
Size: 250 Color: 5

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 199
Size: 340 Color: 143
Size: 262 Color: 45

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 222
Size: 283 Color: 86
Size: 277 Color: 76

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 244
Size: 256 Color: 19
Size: 252 Color: 10

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 227
Size: 276 Color: 73
Size: 268 Color: 57

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 168
Size: 361 Color: 166
Size: 277 Color: 77

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 185
Size: 313 Color: 122
Size: 309 Color: 117

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 183
Size: 347 Color: 147
Size: 276 Color: 74

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 209
Size: 326 Color: 133
Size: 258 Color: 30

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 229
Size: 277 Color: 75
Size: 263 Color: 49

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 219
Size: 290 Color: 95
Size: 275 Color: 70

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 197
Size: 309 Color: 119
Size: 297 Color: 107

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 146
Size: 331 Color: 137
Size: 326 Color: 132

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 203
Size: 335 Color: 140
Size: 263 Color: 48

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 214
Size: 288 Color: 91
Size: 285 Color: 87

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 172
Size: 342 Color: 145
Size: 292 Color: 99

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 243
Size: 256 Color: 22
Size: 254 Color: 15

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 213
Size: 301 Color: 110
Size: 273 Color: 64

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 187
Size: 315 Color: 124
Size: 306 Color: 114

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 186
Size: 372 Color: 178
Size: 250 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 184
Size: 366 Color: 173
Size: 257 Color: 25

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 159
Size: 349 Color: 148
Size: 296 Color: 102

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 181
Size: 316 Color: 125
Size: 309 Color: 116

Total size: 83000
Total free space: 0

