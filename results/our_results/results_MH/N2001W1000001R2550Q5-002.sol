Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 386894 Color: 0
Size: 356589 Color: 3
Size: 256518 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 349582 Color: 4
Size: 329838 Color: 1
Size: 320581 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 412789 Color: 4
Size: 320294 Color: 1
Size: 266918 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 430164 Color: 3
Size: 314900 Color: 0
Size: 254937 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 451781 Color: 2
Size: 292823 Color: 1
Size: 255397 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 485034 Color: 4
Size: 261190 Color: 4
Size: 253777 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 362864 Color: 3
Size: 358331 Color: 0
Size: 278806 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 365930 Color: 4
Size: 363386 Color: 2
Size: 270685 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 373227 Color: 0
Size: 350715 Color: 0
Size: 276059 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 356213 Color: 2
Size: 334180 Color: 1
Size: 309608 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 405641 Color: 2
Size: 318763 Color: 0
Size: 275597 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 481547 Color: 3
Size: 267688 Color: 2
Size: 250766 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 482829 Color: 4
Size: 259957 Color: 3
Size: 257215 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 378830 Color: 3
Size: 360937 Color: 1
Size: 260234 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 351659 Color: 2
Size: 349716 Color: 2
Size: 298626 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 389448 Color: 0
Size: 349681 Color: 3
Size: 260872 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 396567 Color: 4
Size: 351118 Color: 0
Size: 252316 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 422429 Color: 4
Size: 316125 Color: 4
Size: 261447 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 478203 Color: 3
Size: 260945 Color: 3
Size: 260853 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 364769 Color: 4
Size: 357946 Color: 1
Size: 277286 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 374419 Color: 0
Size: 337715 Color: 1
Size: 287867 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 348087 Color: 2
Size: 341312 Color: 2
Size: 310602 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 355631 Color: 0
Size: 332805 Color: 1
Size: 311565 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 349558 Color: 3
Size: 330908 Color: 0
Size: 319535 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 346077 Color: 4
Size: 338231 Color: 3
Size: 315693 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 408040 Color: 4
Size: 340128 Color: 2
Size: 251833 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 377169 Color: 2
Size: 348057 Color: 4
Size: 274775 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 376394 Color: 2
Size: 336143 Color: 2
Size: 287464 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 396875 Color: 1
Size: 352503 Color: 0
Size: 250623 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 358845 Color: 4
Size: 354683 Color: 4
Size: 286473 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 361733 Color: 4
Size: 343666 Color: 0
Size: 294602 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 397141 Color: 2
Size: 343063 Color: 1
Size: 259797 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 369547 Color: 4
Size: 326521 Color: 0
Size: 303933 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 369790 Color: 2
Size: 367570 Color: 0
Size: 262641 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 364446 Color: 3
Size: 358807 Color: 1
Size: 276748 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 392564 Color: 1
Size: 339115 Color: 1
Size: 268322 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 366760 Color: 3
Size: 359624 Color: 1
Size: 273617 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 358533 Color: 0
Size: 352595 Color: 4
Size: 288873 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 365274 Color: 1
Size: 352030 Color: 3
Size: 282697 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 382175 Color: 2
Size: 333398 Color: 2
Size: 284428 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 372738 Color: 3
Size: 346740 Color: 1
Size: 280523 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 350632 Color: 2
Size: 350170 Color: 1
Size: 299199 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 353015 Color: 2
Size: 328556 Color: 2
Size: 318430 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 371608 Color: 2
Size: 337374 Color: 1
Size: 291019 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 337776 Color: 0
Size: 336824 Color: 0
Size: 325401 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 363570 Color: 4
Size: 345601 Color: 2
Size: 290830 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 366573 Color: 1
Size: 360660 Color: 4
Size: 272768 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 369185 Color: 2
Size: 356468 Color: 4
Size: 274348 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 369984 Color: 1
Size: 364972 Color: 3
Size: 265045 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 371276 Color: 2
Size: 315939 Color: 4
Size: 312786 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 371328 Color: 1
Size: 349437 Color: 4
Size: 279236 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 371877 Color: 4
Size: 368742 Color: 1
Size: 259382 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 372758 Color: 3
Size: 338757 Color: 3
Size: 288486 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 373725 Color: 1
Size: 337417 Color: 3
Size: 288859 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 374784 Color: 0
Size: 324908 Color: 1
Size: 300309 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 375683 Color: 4
Size: 327995 Color: 3
Size: 296323 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 376797 Color: 0
Size: 321495 Color: 2
Size: 301709 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 376527 Color: 1
Size: 356442 Color: 3
Size: 267032 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 376678 Color: 2
Size: 325198 Color: 1
Size: 298125 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 377094 Color: 0
Size: 327370 Color: 2
Size: 295537 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 377245 Color: 0
Size: 317935 Color: 4
Size: 304821 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 377610 Color: 0
Size: 350618 Color: 4
Size: 271773 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 377746 Color: 0
Size: 344536 Color: 4
Size: 277719 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 378881 Color: 0
Size: 315516 Color: 3
Size: 305604 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 380362 Color: 3
Size: 349713 Color: 3
Size: 269926 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 380465 Color: 1
Size: 310063 Color: 0
Size: 309473 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 382124 Color: 4
Size: 355970 Color: 3
Size: 261907 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 383473 Color: 1
Size: 322712 Color: 3
Size: 293816 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 385779 Color: 0
Size: 328705 Color: 2
Size: 285517 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 384594 Color: 2
Size: 327337 Color: 3
Size: 288070 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 384887 Color: 4
Size: 345681 Color: 0
Size: 269433 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 386429 Color: 0
Size: 309476 Color: 2
Size: 304096 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 386337 Color: 2
Size: 313759 Color: 4
Size: 299905 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 386376 Color: 1
Size: 309438 Color: 4
Size: 304187 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 386521 Color: 1
Size: 308197 Color: 0
Size: 305283 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 387361 Color: 0
Size: 331261 Color: 1
Size: 281379 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 387054 Color: 1
Size: 338851 Color: 4
Size: 274096 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 388087 Color: 0
Size: 312990 Color: 2
Size: 298924 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 387453 Color: 4
Size: 358023 Color: 1
Size: 254525 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 388488 Color: 3
Size: 358066 Color: 4
Size: 253447 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 388514 Color: 1
Size: 342600 Color: 0
Size: 268887 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 388948 Color: 1
Size: 313911 Color: 0
Size: 297142 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 389094 Color: 2
Size: 346154 Color: 3
Size: 264753 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 389613 Color: 3
Size: 315591 Color: 3
Size: 294797 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 390016 Color: 0
Size: 306414 Color: 3
Size: 303571 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 390821 Color: 0
Size: 326226 Color: 4
Size: 282954 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 392238 Color: 1
Size: 326846 Color: 4
Size: 280917 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 392293 Color: 4
Size: 325792 Color: 1
Size: 281916 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 392610 Color: 4
Size: 320216 Color: 1
Size: 287175 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 394279 Color: 4
Size: 345018 Color: 2
Size: 260704 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 394314 Color: 3
Size: 324663 Color: 0
Size: 281024 Color: 2

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 394112 Color: 0
Size: 344923 Color: 4
Size: 260966 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 394908 Color: 3
Size: 324195 Color: 3
Size: 280898 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 395276 Color: 1
Size: 323064 Color: 1
Size: 281661 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 395619 Color: 3
Size: 321214 Color: 2
Size: 283168 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 396569 Color: 2
Size: 346618 Color: 0
Size: 256814 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 396831 Color: 4
Size: 349737 Color: 3
Size: 253433 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 397630 Color: 0
Size: 317613 Color: 2
Size: 284758 Color: 2

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 397980 Color: 0
Size: 318979 Color: 4
Size: 283042 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 398346 Color: 0
Size: 335516 Color: 4
Size: 266139 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 398432 Color: 0
Size: 345982 Color: 3
Size: 255587 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 397708 Color: 4
Size: 307428 Color: 1
Size: 294865 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 398597 Color: 3
Size: 342710 Color: 2
Size: 258694 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 398935 Color: 0
Size: 322943 Color: 3
Size: 278123 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 398829 Color: 1
Size: 343666 Color: 1
Size: 257506 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 399184 Color: 3
Size: 321280 Color: 3
Size: 279537 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 400325 Color: 4
Size: 344942 Color: 2
Size: 254734 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 400685 Color: 0
Size: 345855 Color: 1
Size: 253461 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 401771 Color: 4
Size: 315052 Color: 0
Size: 283178 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 401798 Color: 3
Size: 340195 Color: 1
Size: 258008 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 401969 Color: 2
Size: 301007 Color: 4
Size: 297025 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 403144 Color: 4
Size: 331210 Color: 2
Size: 265647 Color: 2

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 403258 Color: 2
Size: 302081 Color: 2
Size: 294662 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 403417 Color: 4
Size: 298731 Color: 0
Size: 297853 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 403570 Color: 4
Size: 302460 Color: 3
Size: 293971 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 403574 Color: 4
Size: 336433 Color: 1
Size: 259994 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 403743 Color: 4
Size: 312505 Color: 0
Size: 283753 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 404396 Color: 0
Size: 299408 Color: 2
Size: 296197 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 404010 Color: 1
Size: 307242 Color: 2
Size: 288749 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 404529 Color: 3
Size: 307978 Color: 4
Size: 287494 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 404878 Color: 0
Size: 300007 Color: 3
Size: 295116 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 404608 Color: 3
Size: 324424 Color: 2
Size: 270969 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 405740 Color: 3
Size: 307559 Color: 2
Size: 286702 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 406550 Color: 0
Size: 312424 Color: 2
Size: 281027 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 407053 Color: 4
Size: 340551 Color: 1
Size: 252397 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 407263 Color: 2
Size: 319592 Color: 1
Size: 273146 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 407467 Color: 4
Size: 311815 Color: 3
Size: 280719 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 407658 Color: 2
Size: 317646 Color: 2
Size: 274697 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 408101 Color: 0
Size: 303901 Color: 2
Size: 287999 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 408331 Color: 1
Size: 314185 Color: 4
Size: 277485 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 408676 Color: 3
Size: 307108 Color: 0
Size: 284217 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 408915 Color: 3
Size: 306471 Color: 3
Size: 284615 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 409254 Color: 4
Size: 327324 Color: 1
Size: 263423 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 409697 Color: 1
Size: 307162 Color: 0
Size: 283142 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 409773 Color: 4
Size: 302532 Color: 2
Size: 287696 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 409832 Color: 2
Size: 311722 Color: 3
Size: 278447 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 410461 Color: 2
Size: 295375 Color: 3
Size: 294165 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 410728 Color: 3
Size: 329250 Color: 4
Size: 260023 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 411566 Color: 1
Size: 297838 Color: 4
Size: 290597 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 412634 Color: 0
Size: 317708 Color: 3
Size: 269659 Color: 4

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 412160 Color: 4
Size: 325472 Color: 1
Size: 262369 Color: 3

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 412755 Color: 0
Size: 307935 Color: 3
Size: 279311 Color: 3

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 413155 Color: 4
Size: 310796 Color: 3
Size: 276050 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 413728 Color: 4
Size: 297498 Color: 3
Size: 288775 Color: 2

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 414154 Color: 1
Size: 298165 Color: 3
Size: 287682 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 414218 Color: 1
Size: 326492 Color: 3
Size: 259291 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 415269 Color: 0
Size: 320655 Color: 4
Size: 264077 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 415821 Color: 0
Size: 317394 Color: 2
Size: 266786 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 415483 Color: 4
Size: 326015 Color: 4
Size: 258503 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 415540 Color: 1
Size: 333896 Color: 4
Size: 250565 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 416103 Color: 1
Size: 330187 Color: 4
Size: 253711 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 416265 Color: 4
Size: 308610 Color: 0
Size: 275126 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 416330 Color: 3
Size: 315725 Color: 1
Size: 267946 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 416567 Color: 1
Size: 295160 Color: 0
Size: 288274 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 416998 Color: 2
Size: 332065 Color: 1
Size: 250938 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 417578 Color: 0
Size: 294303 Color: 3
Size: 288120 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 418445 Color: 3
Size: 310477 Color: 4
Size: 271079 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 418572 Color: 2
Size: 291771 Color: 0
Size: 289658 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 418590 Color: 4
Size: 316401 Color: 2
Size: 265010 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 418598 Color: 2
Size: 307114 Color: 1
Size: 274289 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 419112 Color: 1
Size: 326507 Color: 0
Size: 254382 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 420048 Color: 1
Size: 313926 Color: 0
Size: 266027 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 420256 Color: 1
Size: 298312 Color: 1
Size: 281433 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 421281 Color: 3
Size: 315946 Color: 1
Size: 262774 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 421555 Color: 3
Size: 294050 Color: 0
Size: 284396 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 421566 Color: 4
Size: 311071 Color: 2
Size: 267364 Color: 2

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 422923 Color: 1
Size: 307017 Color: 1
Size: 270061 Color: 4

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 422476 Color: 4
Size: 312122 Color: 4
Size: 265403 Color: 3

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 423144 Color: 4
Size: 303494 Color: 0
Size: 273363 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 423586 Color: 3
Size: 315937 Color: 2
Size: 260478 Color: 2

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 423953 Color: 4
Size: 303306 Color: 3
Size: 272742 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 424433 Color: 1
Size: 316167 Color: 2
Size: 259401 Color: 3

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 424829 Color: 1
Size: 287899 Color: 3
Size: 287273 Color: 4

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 424301 Color: 4
Size: 290328 Color: 0
Size: 285372 Color: 2

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 424915 Color: 3
Size: 318863 Color: 3
Size: 256223 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 425083 Color: 3
Size: 315475 Color: 3
Size: 259443 Color: 4

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 425179 Color: 2
Size: 295401 Color: 1
Size: 279421 Color: 3

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 425383 Color: 1
Size: 309515 Color: 3
Size: 265103 Color: 3

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 425304 Color: 2
Size: 306482 Color: 0
Size: 268215 Color: 3

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 425577 Color: 4
Size: 317019 Color: 0
Size: 257405 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 425671 Color: 1
Size: 314409 Color: 2
Size: 259921 Color: 2

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 425630 Color: 2
Size: 289363 Color: 3
Size: 285008 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 425957 Color: 0
Size: 321971 Color: 2
Size: 252073 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 425960 Color: 4
Size: 314478 Color: 2
Size: 259563 Color: 4

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 426253 Color: 0
Size: 321603 Color: 0
Size: 252145 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 426457 Color: 4
Size: 294503 Color: 1
Size: 279041 Color: 2

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 427088 Color: 4
Size: 303253 Color: 3
Size: 269660 Color: 4

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 427292 Color: 1
Size: 292884 Color: 3
Size: 279825 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 428194 Color: 0
Size: 295306 Color: 1
Size: 276501 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 428207 Color: 0
Size: 287194 Color: 0
Size: 284600 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 428276 Color: 3
Size: 314686 Color: 0
Size: 257039 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 428282 Color: 2
Size: 287958 Color: 3
Size: 283761 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 428617 Color: 1
Size: 297267 Color: 0
Size: 274117 Color: 3

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 428675 Color: 4
Size: 288430 Color: 0
Size: 282896 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 429143 Color: 2
Size: 307052 Color: 4
Size: 263806 Color: 2

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 429407 Color: 4
Size: 291582 Color: 3
Size: 279012 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 429612 Color: 4
Size: 291963 Color: 2
Size: 278426 Color: 4

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 430543 Color: 4
Size: 311146 Color: 4
Size: 258312 Color: 2

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 430948 Color: 2
Size: 294772 Color: 1
Size: 274281 Color: 3

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 431555 Color: 4
Size: 308051 Color: 3
Size: 260395 Color: 3

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 431671 Color: 4
Size: 299580 Color: 0
Size: 268750 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 431774 Color: 2
Size: 289031 Color: 2
Size: 279196 Color: 4

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 431849 Color: 4
Size: 289416 Color: 2
Size: 278736 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 432418 Color: 1
Size: 316106 Color: 4
Size: 251477 Color: 2

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 431965 Color: 2
Size: 288978 Color: 0
Size: 279058 Color: 3

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 432421 Color: 1
Size: 285208 Color: 0
Size: 282372 Color: 4

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 432220 Color: 4
Size: 301356 Color: 3
Size: 266425 Color: 4

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 432941 Color: 1
Size: 305836 Color: 2
Size: 261224 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 432762 Color: 0
Size: 301871 Color: 0
Size: 265368 Color: 3

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 433344 Color: 4
Size: 284021 Color: 0
Size: 282636 Color: 2

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 434059 Color: 1
Size: 308851 Color: 4
Size: 257091 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 434146 Color: 3
Size: 309102 Color: 2
Size: 256753 Color: 3

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 434643 Color: 2
Size: 295434 Color: 1
Size: 269924 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 435428 Color: 4
Size: 283361 Color: 1
Size: 281212 Color: 2

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 435914 Color: 3
Size: 289769 Color: 2
Size: 274318 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 435982 Color: 0
Size: 302896 Color: 2
Size: 261123 Color: 4

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 436390 Color: 0
Size: 291160 Color: 1
Size: 272451 Color: 3

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 437012 Color: 1
Size: 302339 Color: 4
Size: 260650 Color: 4

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 437037 Color: 3
Size: 297762 Color: 4
Size: 265202 Color: 3

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 437287 Color: 3
Size: 304208 Color: 0
Size: 258506 Color: 4

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 437820 Color: 0
Size: 296445 Color: 1
Size: 265736 Color: 3

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 438852 Color: 1
Size: 297177 Color: 4
Size: 263972 Color: 3

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 438668 Color: 4
Size: 288255 Color: 2
Size: 273078 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 438872 Color: 1
Size: 308180 Color: 4
Size: 252949 Color: 4

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 438865 Color: 3
Size: 291164 Color: 0
Size: 269972 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 439475 Color: 3
Size: 301691 Color: 2
Size: 258835 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 439625 Color: 0
Size: 299205 Color: 1
Size: 261171 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 439973 Color: 3
Size: 301672 Color: 4
Size: 258356 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 440174 Color: 2
Size: 304230 Color: 0
Size: 255597 Color: 3

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 440292 Color: 0
Size: 295764 Color: 3
Size: 263945 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 440479 Color: 1
Size: 295122 Color: 0
Size: 264400 Color: 4

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 440325 Color: 4
Size: 303034 Color: 2
Size: 256642 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 440693 Color: 2
Size: 282019 Color: 2
Size: 277289 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 441418 Color: 4
Size: 294695 Color: 1
Size: 263888 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 442285 Color: 1
Size: 300814 Color: 3
Size: 256902 Color: 2

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 441785 Color: 0
Size: 297669 Color: 4
Size: 260547 Color: 3

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 442430 Color: 3
Size: 285577 Color: 1
Size: 271994 Color: 4

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 444375 Color: 1
Size: 280241 Color: 4
Size: 275385 Color: 3

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 444550 Color: 1
Size: 287449 Color: 2
Size: 268002 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 444069 Color: 3
Size: 303738 Color: 0
Size: 252194 Color: 4

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 444696 Color: 1
Size: 296994 Color: 2
Size: 258311 Color: 4

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 444794 Color: 0
Size: 303462 Color: 4
Size: 251745 Color: 3

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 445016 Color: 3
Size: 297767 Color: 0
Size: 257218 Color: 4

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 446291 Color: 4
Size: 292677 Color: 1
Size: 261033 Color: 3

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 446427 Color: 4
Size: 277318 Color: 1
Size: 276256 Color: 2

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 446774 Color: 4
Size: 295016 Color: 2
Size: 258211 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 446852 Color: 0
Size: 283128 Color: 0
Size: 270021 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 446988 Color: 2
Size: 285049 Color: 1
Size: 267964 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 447457 Color: 3
Size: 296588 Color: 2
Size: 255956 Color: 2

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 447525 Color: 3
Size: 295580 Color: 1
Size: 256896 Color: 4

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 448027 Color: 3
Size: 276100 Color: 4
Size: 275874 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 448031 Color: 2
Size: 285471 Color: 1
Size: 266499 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 448513 Color: 2
Size: 283738 Color: 0
Size: 267750 Color: 4

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 449081 Color: 1
Size: 290110 Color: 3
Size: 260810 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 449035 Color: 4
Size: 299256 Color: 3
Size: 251710 Color: 3

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 449557 Color: 0
Size: 286834 Color: 4
Size: 263610 Color: 4

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 450560 Color: 1
Size: 291265 Color: 2
Size: 258176 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 450553 Color: 4
Size: 285765 Color: 4
Size: 263683 Color: 3

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 450738 Color: 0
Size: 277256 Color: 1
Size: 272007 Color: 4

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 450898 Color: 3
Size: 288623 Color: 2
Size: 260480 Color: 2

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 451765 Color: 1
Size: 281020 Color: 0
Size: 267216 Color: 3

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 451767 Color: 3
Size: 295454 Color: 1
Size: 252780 Color: 2

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 452066 Color: 0
Size: 293285 Color: 1
Size: 254650 Color: 4

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 452084 Color: 4
Size: 286198 Color: 2
Size: 261719 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 452444 Color: 0
Size: 288833 Color: 2
Size: 258724 Color: 3

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 452674 Color: 1
Size: 287811 Color: 4
Size: 259516 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 452959 Color: 1
Size: 281968 Color: 0
Size: 265074 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 452966 Color: 0
Size: 293007 Color: 1
Size: 254028 Color: 3

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 453557 Color: 2
Size: 284536 Color: 0
Size: 261908 Color: 4

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 453721 Color: 4
Size: 278176 Color: 1
Size: 268104 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 453741 Color: 3
Size: 274557 Color: 3
Size: 271703 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 454308 Color: 0
Size: 294621 Color: 3
Size: 251072 Color: 4

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 455245 Color: 2
Size: 274143 Color: 2
Size: 270613 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 456041 Color: 3
Size: 293935 Color: 4
Size: 250025 Color: 2

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 456778 Color: 0
Size: 279626 Color: 0
Size: 263597 Color: 3

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 457613 Color: 1
Size: 286529 Color: 2
Size: 255859 Color: 2

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 457212 Color: 2
Size: 292386 Color: 3
Size: 250403 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 457559 Color: 0
Size: 291040 Color: 3
Size: 251402 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 457700 Color: 4
Size: 276812 Color: 0
Size: 265489 Color: 4

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 458878 Color: 4
Size: 288410 Color: 3
Size: 252713 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 459513 Color: 4
Size: 287007 Color: 1
Size: 253481 Color: 2

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 460138 Color: 3
Size: 283693 Color: 3
Size: 256170 Color: 4

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 460995 Color: 1
Size: 279841 Color: 4
Size: 259165 Color: 4

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 461511 Color: 1
Size: 286810 Color: 2
Size: 251680 Color: 3

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 461034 Color: 3
Size: 277817 Color: 0
Size: 261150 Color: 2

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 461944 Color: 1
Size: 271238 Color: 3
Size: 266819 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 462212 Color: 1
Size: 270824 Color: 0
Size: 266965 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 461961 Color: 3
Size: 279108 Color: 2
Size: 258932 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 462527 Color: 1
Size: 270525 Color: 3
Size: 266949 Color: 4

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 462999 Color: 3
Size: 268571 Color: 4
Size: 268431 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 463149 Color: 4
Size: 281965 Color: 1
Size: 254887 Color: 3

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 463379 Color: 2
Size: 272027 Color: 0
Size: 264595 Color: 4

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 463421 Color: 0
Size: 276498 Color: 3
Size: 260082 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 464400 Color: 0
Size: 278070 Color: 1
Size: 257531 Color: 3

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 466354 Color: 3
Size: 267166 Color: 3
Size: 266481 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 466899 Color: 1
Size: 281192 Color: 4
Size: 251910 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 467622 Color: 1
Size: 279263 Color: 2
Size: 253116 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 467370 Color: 4
Size: 277033 Color: 3
Size: 255598 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 468148 Color: 4
Size: 271815 Color: 4
Size: 260038 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 468706 Color: 3
Size: 266744 Color: 2
Size: 264551 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 469950 Color: 2
Size: 273606 Color: 1
Size: 256445 Color: 2

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 471370 Color: 3
Size: 270169 Color: 0
Size: 258462 Color: 2

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 471504 Color: 3
Size: 265877 Color: 1
Size: 262620 Color: 4

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 471616 Color: 3
Size: 265392 Color: 2
Size: 262993 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 471666 Color: 3
Size: 276288 Color: 1
Size: 252047 Color: 3

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 471971 Color: 4
Size: 266923 Color: 2
Size: 261107 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 472687 Color: 4
Size: 269478 Color: 0
Size: 257836 Color: 4

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 472806 Color: 0
Size: 272387 Color: 0
Size: 254808 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 473330 Color: 1
Size: 264709 Color: 4
Size: 261962 Color: 2

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 475679 Color: 0
Size: 262550 Color: 0
Size: 261772 Color: 2

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 476350 Color: 0
Size: 268263 Color: 3
Size: 255388 Color: 4

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 476537 Color: 3
Size: 265002 Color: 4
Size: 258462 Color: 2

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 477386 Color: 0
Size: 266350 Color: 4
Size: 256265 Color: 3

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 476674 Color: 2
Size: 265179 Color: 4
Size: 258148 Color: 3

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 478256 Color: 0
Size: 270649 Color: 1
Size: 251096 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 478415 Color: 3
Size: 265350 Color: 0
Size: 256236 Color: 4

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 478855 Color: 1
Size: 266738 Color: 2
Size: 254408 Color: 3

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 479094 Color: 3
Size: 269680 Color: 2
Size: 251227 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 479534 Color: 1
Size: 269850 Color: 2
Size: 250617 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 479761 Color: 3
Size: 269630 Color: 2
Size: 250610 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 479853 Color: 3
Size: 269307 Color: 4
Size: 250841 Color: 2

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 480386 Color: 3
Size: 268308 Color: 4
Size: 251307 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 480721 Color: 1
Size: 262984 Color: 0
Size: 256296 Color: 4

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 480908 Color: 1
Size: 261299 Color: 3
Size: 257794 Color: 3

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 480980 Color: 4
Size: 268400 Color: 2
Size: 250621 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 481540 Color: 1
Size: 268122 Color: 2
Size: 250339 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 481666 Color: 2
Size: 263638 Color: 4
Size: 254697 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 481800 Color: 1
Size: 268005 Color: 3
Size: 250196 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 481777 Color: 0
Size: 261226 Color: 3
Size: 256998 Color: 4

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 481879 Color: 1
Size: 265686 Color: 2
Size: 252436 Color: 3

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 481884 Color: 3
Size: 267216 Color: 0
Size: 250901 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 481848 Color: 0
Size: 264618 Color: 4
Size: 253535 Color: 3

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 481960 Color: 4
Size: 259976 Color: 3
Size: 258065 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 482860 Color: 0
Size: 260883 Color: 4
Size: 256258 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 482867 Color: 0
Size: 266102 Color: 4
Size: 251032 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 482834 Color: 3
Size: 263055 Color: 4
Size: 254112 Color: 4

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 483209 Color: 3
Size: 263708 Color: 0
Size: 253084 Color: 4

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 483554 Color: 2
Size: 261787 Color: 0
Size: 254660 Color: 3

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 484160 Color: 3
Size: 262719 Color: 2
Size: 253122 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 486227 Color: 2
Size: 262015 Color: 4
Size: 251759 Color: 4

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 486564 Color: 1
Size: 262029 Color: 2
Size: 251408 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 486877 Color: 1
Size: 257171 Color: 4
Size: 255953 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 487582 Color: 2
Size: 258611 Color: 4
Size: 253808 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 487738 Color: 4
Size: 257283 Color: 4
Size: 254980 Color: 2

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 488040 Color: 0
Size: 259036 Color: 3
Size: 252925 Color: 3

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 488773 Color: 3
Size: 260509 Color: 2
Size: 250719 Color: 4

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 489102 Color: 1
Size: 257719 Color: 0
Size: 253180 Color: 2

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 489810 Color: 0
Size: 255247 Color: 1
Size: 254944 Color: 2

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 489720 Color: 1
Size: 256819 Color: 2
Size: 253462 Color: 2

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 491077 Color: 0
Size: 258889 Color: 3
Size: 250035 Color: 3

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 490787 Color: 2
Size: 255236 Color: 3
Size: 253978 Color: 2

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 491971 Color: 3
Size: 256165 Color: 1
Size: 251865 Color: 3

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 492505 Color: 4
Size: 256714 Color: 0
Size: 250782 Color: 4

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 492788 Color: 0
Size: 256371 Color: 2
Size: 250842 Color: 3

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 492680 Color: 1
Size: 254174 Color: 3
Size: 253147 Color: 2

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 493172 Color: 3
Size: 254092 Color: 1
Size: 252737 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 493227 Color: 1
Size: 254669 Color: 4
Size: 252105 Color: 4

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 493515 Color: 3
Size: 256035 Color: 4
Size: 250451 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 494507 Color: 2
Size: 253253 Color: 4
Size: 252241 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 495216 Color: 2
Size: 254597 Color: 4
Size: 250188 Color: 3

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 495455 Color: 0
Size: 254117 Color: 2
Size: 250429 Color: 2

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 495717 Color: 1
Size: 253075 Color: 1
Size: 251209 Color: 3

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 495874 Color: 1
Size: 253207 Color: 4
Size: 250920 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 496438 Color: 4
Size: 252166 Color: 3
Size: 251397 Color: 3

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 498213 Color: 0
Size: 251422 Color: 1
Size: 250366 Color: 3

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 498010 Color: 3
Size: 251155 Color: 1
Size: 250836 Color: 4

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 498739 Color: 0
Size: 251171 Color: 3
Size: 250091 Color: 4

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 360545 Color: 1
Size: 334461 Color: 2
Size: 304994 Color: 2

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 361903 Color: 2
Size: 355626 Color: 0
Size: 282471 Color: 4

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 365634 Color: 0
Size: 333828 Color: 1
Size: 300538 Color: 1

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 366313 Color: 4
Size: 351279 Color: 0
Size: 282408 Color: 3

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 366694 Color: 3
Size: 338568 Color: 3
Size: 294738 Color: 2

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 368209 Color: 2
Size: 364007 Color: 1
Size: 267784 Color: 1

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 368789 Color: 1
Size: 327897 Color: 4
Size: 303314 Color: 0

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 371323 Color: 1
Size: 323753 Color: 0
Size: 304924 Color: 4

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 375064 Color: 2
Size: 336865 Color: 0
Size: 288071 Color: 2

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 375374 Color: 2
Size: 347081 Color: 3
Size: 277545 Color: 4

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 377126 Color: 3
Size: 345444 Color: 4
Size: 277430 Color: 4

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 377494 Color: 4
Size: 322730 Color: 1
Size: 299776 Color: 0

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 378113 Color: 4
Size: 351730 Color: 2
Size: 270157 Color: 2

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 378421 Color: 1
Size: 326290 Color: 4
Size: 295289 Color: 4

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 378602 Color: 2
Size: 358551 Color: 0
Size: 262847 Color: 3

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 379201 Color: 4
Size: 363204 Color: 3
Size: 257595 Color: 0

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 379769 Color: 1
Size: 335212 Color: 3
Size: 285019 Color: 1

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 380267 Color: 0
Size: 339192 Color: 2
Size: 280541 Color: 2

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 380348 Color: 1
Size: 357628 Color: 4
Size: 262024 Color: 1

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 380636 Color: 0
Size: 342135 Color: 1
Size: 277229 Color: 2

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 380833 Color: 3
Size: 314295 Color: 2
Size: 304872 Color: 4

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 380900 Color: 3
Size: 340702 Color: 0
Size: 278398 Color: 2

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 381818 Color: 4
Size: 335379 Color: 4
Size: 282803 Color: 0

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 382446 Color: 1
Size: 313042 Color: 2
Size: 304512 Color: 3

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 382778 Color: 1
Size: 355317 Color: 2
Size: 261905 Color: 0

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 384173 Color: 0
Size: 311733 Color: 4
Size: 304094 Color: 4

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 384134 Color: 3
Size: 345920 Color: 0
Size: 269946 Color: 4

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 384756 Color: 0
Size: 308834 Color: 4
Size: 306410 Color: 4

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 384383 Color: 1
Size: 329572 Color: 3
Size: 286045 Color: 2

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 385247 Color: 3
Size: 327240 Color: 2
Size: 287513 Color: 3

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 387153 Color: 0
Size: 310405 Color: 4
Size: 302442 Color: 3

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 386590 Color: 2
Size: 316727 Color: 1
Size: 296683 Color: 2

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 387161 Color: 3
Size: 336983 Color: 3
Size: 275856 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 388155 Color: 0
Size: 309802 Color: 1
Size: 302043 Color: 2

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 391004 Color: 4
Size: 314597 Color: 3
Size: 294399 Color: 2

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 391134 Color: 4
Size: 324408 Color: 0
Size: 284458 Color: 3

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 393457 Color: 4
Size: 325644 Color: 1
Size: 280899 Color: 3

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 393832 Color: 3
Size: 314652 Color: 0
Size: 291516 Color: 3

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 394563 Color: 1
Size: 347460 Color: 2
Size: 257977 Color: 4

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 395057 Color: 1
Size: 309948 Color: 0
Size: 294995 Color: 1

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 395730 Color: 0
Size: 353461 Color: 4
Size: 250809 Color: 1

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 395979 Color: 0
Size: 313090 Color: 3
Size: 290931 Color: 4

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 396317 Color: 1
Size: 345260 Color: 3
Size: 258423 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 396686 Color: 4
Size: 351428 Color: 0
Size: 251886 Color: 4

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 396965 Color: 2
Size: 308885 Color: 3
Size: 294150 Color: 4

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 398002 Color: 3
Size: 301887 Color: 0
Size: 300111 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 398670 Color: 0
Size: 309253 Color: 1
Size: 292077 Color: 1

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 398998 Color: 2
Size: 328750 Color: 3
Size: 272252 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 401680 Color: 0
Size: 329656 Color: 2
Size: 268664 Color: 3

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 402535 Color: 3
Size: 322860 Color: 4
Size: 274605 Color: 0

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 403106 Color: 1
Size: 302859 Color: 1
Size: 294035 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 403416 Color: 1
Size: 339122 Color: 1
Size: 257462 Color: 3

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 405035 Color: 0
Size: 344427 Color: 4
Size: 250538 Color: 2

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 2
Size: 330318 Color: 4
Size: 264539 Color: 4

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 405847 Color: 0
Size: 326564 Color: 1
Size: 267589 Color: 3

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 405672 Color: 1
Size: 311325 Color: 0
Size: 283003 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 405975 Color: 1
Size: 329341 Color: 0
Size: 264684 Color: 4

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 406249 Color: 1
Size: 313384 Color: 3
Size: 280367 Color: 1

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 409134 Color: 0
Size: 332702 Color: 1
Size: 258164 Color: 4

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 409624 Color: 4
Size: 307980 Color: 3
Size: 282396 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 409983 Color: 1
Size: 326951 Color: 0
Size: 263066 Color: 4

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 410477 Color: 4
Size: 332212 Color: 3
Size: 257311 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 410516 Color: 4
Size: 333191 Color: 1
Size: 256293 Color: 4

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 411885 Color: 0
Size: 337671 Color: 4
Size: 250444 Color: 4

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 411979 Color: 0
Size: 303846 Color: 4
Size: 284175 Color: 2

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 411739 Color: 1
Size: 337890 Color: 1
Size: 250371 Color: 3

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 413194 Color: 0
Size: 333231 Color: 2
Size: 253575 Color: 4

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 413562 Color: 1
Size: 302474 Color: 3
Size: 283964 Color: 0

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 414111 Color: 0
Size: 303251 Color: 3
Size: 282638 Color: 2

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 414531 Color: 1
Size: 306467 Color: 4
Size: 279002 Color: 3

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 416033 Color: 0
Size: 296173 Color: 4
Size: 287794 Color: 3

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 415660 Color: 4
Size: 320548 Color: 1
Size: 263792 Color: 1

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 418479 Color: 2
Size: 324306 Color: 0
Size: 257215 Color: 3

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 420827 Color: 2
Size: 312915 Color: 0
Size: 266258 Color: 1

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 420859 Color: 4
Size: 307469 Color: 3
Size: 271672 Color: 3

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 421050 Color: 4
Size: 290452 Color: 3
Size: 288498 Color: 1

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 422345 Color: 1
Size: 309130 Color: 2
Size: 268525 Color: 0

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 423039 Color: 4
Size: 326958 Color: 1
Size: 250003 Color: 3

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 423168 Color: 2
Size: 301216 Color: 1
Size: 275616 Color: 2

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 427445 Color: 3
Size: 311256 Color: 0
Size: 261299 Color: 3

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 427640 Color: 3
Size: 321759 Color: 2
Size: 250601 Color: 3

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 431726 Color: 1
Size: 284665 Color: 0
Size: 283609 Color: 2

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 432290 Color: 4
Size: 303584 Color: 0
Size: 264126 Color: 1

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 432879 Color: 2
Size: 291271 Color: 1
Size: 275850 Color: 0

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 434180 Color: 3
Size: 310707 Color: 1
Size: 255113 Color: 4

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 435326 Color: 2
Size: 291792 Color: 2
Size: 272882 Color: 0

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 435812 Color: 3
Size: 308379 Color: 2
Size: 255809 Color: 2

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 435878 Color: 3
Size: 282279 Color: 3
Size: 281843 Color: 1

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 437970 Color: 1
Size: 297676 Color: 0
Size: 264354 Color: 0

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 438205 Color: 0
Size: 286085 Color: 2
Size: 275710 Color: 0

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 442512 Color: 2
Size: 305100 Color: 3
Size: 252388 Color: 0

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 444517 Color: 4
Size: 303240 Color: 1
Size: 252243 Color: 3

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 444840 Color: 0
Size: 288832 Color: 1
Size: 266328 Color: 0

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 447583 Color: 3
Size: 291073 Color: 2
Size: 261344 Color: 4

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 449957 Color: 4
Size: 285255 Color: 0
Size: 264788 Color: 1

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 454818 Color: 0
Size: 281876 Color: 1
Size: 263306 Color: 4

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 457169 Color: 2
Size: 277002 Color: 1
Size: 265829 Color: 3

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 467378 Color: 2
Size: 268243 Color: 3
Size: 264379 Color: 1

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 470324 Color: 0
Size: 275030 Color: 1
Size: 254646 Color: 0

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 479404 Color: 1
Size: 264487 Color: 0
Size: 256109 Color: 2

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 481449 Color: 0
Size: 266875 Color: 2
Size: 251676 Color: 2

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 482650 Color: 2
Size: 260967 Color: 4
Size: 256383 Color: 1

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 486912 Color: 1
Size: 261933 Color: 2
Size: 251155 Color: 0

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 495543 Color: 1
Size: 253218 Color: 4
Size: 251239 Color: 0

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 496920 Color: 2
Size: 252699 Color: 1
Size: 250381 Color: 0

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 422715 Color: 1
Size: 312521 Color: 2
Size: 264764 Color: 1

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 390203 Color: 4
Size: 351239 Color: 1
Size: 258558 Color: 0

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 367692 Color: 2
Size: 320501 Color: 3
Size: 311807 Color: 2

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 380966 Color: 0
Size: 365317 Color: 1
Size: 253717 Color: 2

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 347786 Color: 1
Size: 333278 Color: 0
Size: 318936 Color: 0

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 334605 Color: 1
Size: 334434 Color: 3
Size: 330961 Color: 1

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 401622 Color: 4
Size: 345646 Color: 4
Size: 252732 Color: 2

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 361979 Color: 4
Size: 325019 Color: 0
Size: 313002 Color: 2

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 400369 Color: 1
Size: 333819 Color: 3
Size: 265811 Color: 1

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 428947 Color: 1
Size: 316116 Color: 4
Size: 254936 Color: 1

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 459095 Color: 0
Size: 279715 Color: 3
Size: 261189 Color: 0

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 363665 Color: 1
Size: 318807 Color: 0
Size: 317527 Color: 1

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 366921 Color: 0
Size: 336534 Color: 1
Size: 296544 Color: 4

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 372391 Color: 4
Size: 333076 Color: 2
Size: 294532 Color: 2

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 373110 Color: 1
Size: 317564 Color: 2
Size: 309325 Color: 2

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 375704 Color: 1
Size: 312330 Color: 2
Size: 311965 Color: 2

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 376603 Color: 2
Size: 331054 Color: 3
Size: 292342 Color: 0

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 377330 Color: 3
Size: 370066 Color: 3
Size: 252603 Color: 2

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 386545 Color: 3
Size: 348568 Color: 4
Size: 264886 Color: 2

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 392330 Color: 4
Size: 336577 Color: 0
Size: 271092 Color: 1

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 394191 Color: 4
Size: 329557 Color: 0
Size: 276251 Color: 3

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 395182 Color: 3
Size: 334620 Color: 2
Size: 270197 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 395803 Color: 1
Size: 325973 Color: 1
Size: 278223 Color: 2

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 396697 Color: 3
Size: 310731 Color: 2
Size: 292571 Color: 4

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 400588 Color: 3
Size: 308434 Color: 0
Size: 290977 Color: 3

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 403271 Color: 4
Size: 309642 Color: 2
Size: 287086 Color: 4

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 404846 Color: 1
Size: 313386 Color: 2
Size: 281767 Color: 0

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 405883 Color: 0
Size: 298135 Color: 1
Size: 295981 Color: 1

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 418065 Color: 4
Size: 321827 Color: 4
Size: 260107 Color: 0

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 418957 Color: 2
Size: 314317 Color: 2
Size: 266725 Color: 3

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 420795 Color: 4
Size: 300761 Color: 3
Size: 278443 Color: 0

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 423932 Color: 0
Size: 307064 Color: 4
Size: 269003 Color: 1

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 429691 Color: 3
Size: 288573 Color: 1
Size: 281735 Color: 2

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 434006 Color: 3
Size: 313354 Color: 3
Size: 252639 Color: 1

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 438804 Color: 3
Size: 281295 Color: 2
Size: 279900 Color: 2

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 441168 Color: 1
Size: 299976 Color: 0
Size: 258855 Color: 2

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 452757 Color: 3
Size: 293024 Color: 4
Size: 254218 Color: 0

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 465276 Color: 0
Size: 278364 Color: 3
Size: 256359 Color: 0

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 465502 Color: 4
Size: 278899 Color: 4
Size: 255598 Color: 1

Bin 512: 2 of cap free
Amount of items: 3
Items: 
Size: 475871 Color: 0
Size: 268388 Color: 0
Size: 255740 Color: 3

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 491591 Color: 3
Size: 257960 Color: 2
Size: 250448 Color: 0

Bin 514: 2 of cap free
Amount of items: 3
Items: 
Size: 342305 Color: 3
Size: 342292 Color: 2
Size: 315402 Color: 0

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 427189 Color: 3
Size: 312608 Color: 2
Size: 260201 Color: 4

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 354366 Color: 3
Size: 352532 Color: 1
Size: 293100 Color: 1

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 369823 Color: 0
Size: 317572 Color: 2
Size: 312603 Color: 4

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 369513 Color: 1
Size: 315600 Color: 1
Size: 314885 Color: 3

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 372026 Color: 0
Size: 323275 Color: 0
Size: 304697 Color: 4

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 372812 Color: 4
Size: 371616 Color: 1
Size: 255570 Color: 0

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 376027 Color: 0
Size: 368226 Color: 2
Size: 255745 Color: 3

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 376104 Color: 2
Size: 371909 Color: 3
Size: 251985 Color: 1

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 376441 Color: 2
Size: 331652 Color: 1
Size: 291905 Color: 0

Bin 524: 3 of cap free
Amount of items: 3
Items: 
Size: 377128 Color: 2
Size: 312317 Color: 0
Size: 310553 Color: 1

Bin 525: 3 of cap free
Amount of items: 3
Items: 
Size: 378759 Color: 3
Size: 320032 Color: 4
Size: 301207 Color: 2

Bin 526: 3 of cap free
Amount of items: 3
Items: 
Size: 386354 Color: 1
Size: 339173 Color: 1
Size: 274471 Color: 0

Bin 527: 3 of cap free
Amount of items: 3
Items: 
Size: 388752 Color: 4
Size: 326106 Color: 1
Size: 285140 Color: 3

Bin 528: 3 of cap free
Amount of items: 3
Items: 
Size: 402894 Color: 0
Size: 299462 Color: 4
Size: 297642 Color: 1

Bin 529: 3 of cap free
Amount of items: 3
Items: 
Size: 403241 Color: 1
Size: 312983 Color: 0
Size: 283774 Color: 1

Bin 530: 3 of cap free
Amount of items: 3
Items: 
Size: 403350 Color: 1
Size: 301651 Color: 1
Size: 294997 Color: 0

Bin 531: 3 of cap free
Amount of items: 3
Items: 
Size: 405259 Color: 3
Size: 317186 Color: 1
Size: 277553 Color: 3

Bin 532: 3 of cap free
Amount of items: 3
Items: 
Size: 407406 Color: 0
Size: 307582 Color: 2
Size: 285010 Color: 1

Bin 533: 3 of cap free
Amount of items: 3
Items: 
Size: 423123 Color: 3
Size: 292857 Color: 4
Size: 284018 Color: 1

Bin 534: 3 of cap free
Amount of items: 3
Items: 
Size: 428002 Color: 1
Size: 294921 Color: 3
Size: 277075 Color: 3

Bin 535: 3 of cap free
Amount of items: 3
Items: 
Size: 456403 Color: 3
Size: 273151 Color: 1
Size: 270444 Color: 2

Bin 536: 3 of cap free
Amount of items: 3
Items: 
Size: 476271 Color: 0
Size: 266447 Color: 3
Size: 257280 Color: 2

Bin 537: 3 of cap free
Amount of items: 3
Items: 
Size: 346165 Color: 3
Size: 344338 Color: 4
Size: 309495 Color: 3

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 423485 Color: 1
Size: 315643 Color: 0
Size: 260869 Color: 1

Bin 539: 4 of cap free
Amount of items: 3
Items: 
Size: 365297 Color: 1
Size: 327395 Color: 3
Size: 307305 Color: 0

Bin 540: 4 of cap free
Amount of items: 3
Items: 
Size: 376509 Color: 1
Size: 343234 Color: 3
Size: 280254 Color: 0

Bin 541: 4 of cap free
Amount of items: 3
Items: 
Size: 383821 Color: 4
Size: 335226 Color: 0
Size: 280950 Color: 2

Bin 542: 4 of cap free
Amount of items: 3
Items: 
Size: 386257 Color: 2
Size: 319115 Color: 2
Size: 294625 Color: 0

Bin 543: 4 of cap free
Amount of items: 3
Items: 
Size: 391129 Color: 4
Size: 306912 Color: 0
Size: 301956 Color: 3

Bin 544: 4 of cap free
Amount of items: 3
Items: 
Size: 392283 Color: 0
Size: 354140 Color: 1
Size: 253574 Color: 1

Bin 545: 4 of cap free
Amount of items: 3
Items: 
Size: 395125 Color: 0
Size: 323200 Color: 1
Size: 281672 Color: 2

Bin 546: 4 of cap free
Amount of items: 3
Items: 
Size: 400178 Color: 4
Size: 328128 Color: 1
Size: 271691 Color: 0

Bin 547: 4 of cap free
Amount of items: 3
Items: 
Size: 448705 Color: 4
Size: 278391 Color: 0
Size: 272901 Color: 1

Bin 548: 4 of cap free
Amount of items: 3
Items: 
Size: 449385 Color: 4
Size: 290651 Color: 4
Size: 259961 Color: 1

Bin 549: 4 of cap free
Amount of items: 3
Items: 
Size: 496824 Color: 2
Size: 251990 Color: 1
Size: 251183 Color: 0

Bin 550: 4 of cap free
Amount of items: 3
Items: 
Size: 360180 Color: 4
Size: 329641 Color: 3
Size: 310176 Color: 3

Bin 551: 4 of cap free
Amount of items: 3
Items: 
Size: 364126 Color: 0
Size: 361201 Color: 2
Size: 274670 Color: 4

Bin 552: 4 of cap free
Amount of items: 3
Items: 
Size: 360663 Color: 4
Size: 322881 Color: 1
Size: 316453 Color: 1

Bin 553: 4 of cap free
Amount of items: 3
Items: 
Size: 348782 Color: 3
Size: 326039 Color: 2
Size: 325176 Color: 1

Bin 554: 5 of cap free
Amount of items: 3
Items: 
Size: 421035 Color: 2
Size: 325992 Color: 0
Size: 252969 Color: 0

Bin 555: 5 of cap free
Amount of items: 3
Items: 
Size: 392004 Color: 3
Size: 332522 Color: 4
Size: 275470 Color: 1

Bin 556: 5 of cap free
Amount of items: 3
Items: 
Size: 396123 Color: 3
Size: 330843 Color: 2
Size: 273030 Color: 0

Bin 557: 5 of cap free
Amount of items: 3
Items: 
Size: 404579 Color: 4
Size: 315553 Color: 0
Size: 279864 Color: 2

Bin 558: 5 of cap free
Amount of items: 3
Items: 
Size: 408785 Color: 1
Size: 310384 Color: 4
Size: 280827 Color: 0

Bin 559: 5 of cap free
Amount of items: 3
Items: 
Size: 356804 Color: 0
Size: 346155 Color: 4
Size: 297037 Color: 0

Bin 560: 5 of cap free
Amount of items: 3
Items: 
Size: 349869 Color: 1
Size: 340414 Color: 2
Size: 309713 Color: 2

Bin 561: 6 of cap free
Amount of items: 3
Items: 
Size: 410550 Color: 2
Size: 309180 Color: 3
Size: 280265 Color: 4

Bin 562: 6 of cap free
Amount of items: 3
Items: 
Size: 355056 Color: 3
Size: 337462 Color: 4
Size: 307477 Color: 3

Bin 563: 6 of cap free
Amount of items: 3
Items: 
Size: 368195 Color: 4
Size: 326539 Color: 0
Size: 305261 Color: 4

Bin 564: 6 of cap free
Amount of items: 3
Items: 
Size: 371019 Color: 2
Size: 333083 Color: 0
Size: 295893 Color: 1

Bin 565: 6 of cap free
Amount of items: 3
Items: 
Size: 374356 Color: 1
Size: 324659 Color: 4
Size: 300980 Color: 3

Bin 566: 6 of cap free
Amount of items: 3
Items: 
Size: 395481 Color: 2
Size: 303907 Color: 0
Size: 300607 Color: 3

Bin 567: 6 of cap free
Amount of items: 3
Items: 
Size: 396829 Color: 1
Size: 317276 Color: 2
Size: 285890 Color: 0

Bin 568: 6 of cap free
Amount of items: 3
Items: 
Size: 404384 Color: 4
Size: 323961 Color: 0
Size: 271650 Color: 2

Bin 569: 6 of cap free
Amount of items: 3
Items: 
Size: 429358 Color: 2
Size: 289190 Color: 0
Size: 281447 Color: 1

Bin 570: 7 of cap free
Amount of items: 3
Items: 
Size: 354101 Color: 4
Size: 332469 Color: 4
Size: 313424 Color: 2

Bin 571: 7 of cap free
Amount of items: 3
Items: 
Size: 363840 Color: 0
Size: 327954 Color: 3
Size: 308200 Color: 4

Bin 572: 7 of cap free
Amount of items: 3
Items: 
Size: 347247 Color: 1
Size: 329008 Color: 4
Size: 323739 Color: 4

Bin 573: 7 of cap free
Amount of items: 3
Items: 
Size: 370221 Color: 4
Size: 360497 Color: 0
Size: 269276 Color: 4

Bin 574: 8 of cap free
Amount of items: 3
Items: 
Size: 371006 Color: 4
Size: 336165 Color: 2
Size: 292822 Color: 0

Bin 575: 8 of cap free
Amount of items: 3
Items: 
Size: 366756 Color: 2
Size: 336601 Color: 1
Size: 296636 Color: 0

Bin 576: 8 of cap free
Amount of items: 3
Items: 
Size: 373130 Color: 3
Size: 323805 Color: 0
Size: 303058 Color: 3

Bin 577: 8 of cap free
Amount of items: 3
Items: 
Size: 375997 Color: 3
Size: 338794 Color: 0
Size: 285202 Color: 4

Bin 578: 8 of cap free
Amount of items: 3
Items: 
Size: 382545 Color: 1
Size: 340206 Color: 4
Size: 277242 Color: 0

Bin 579: 8 of cap free
Amount of items: 3
Items: 
Size: 384053 Color: 1
Size: 318736 Color: 4
Size: 297204 Color: 3

Bin 580: 8 of cap free
Amount of items: 3
Items: 
Size: 352990 Color: 0
Size: 324233 Color: 2
Size: 322770 Color: 0

Bin 581: 8 of cap free
Amount of items: 3
Items: 
Size: 350809 Color: 0
Size: 350732 Color: 2
Size: 298452 Color: 0

Bin 582: 9 of cap free
Amount of items: 3
Items: 
Size: 351555 Color: 1
Size: 325776 Color: 0
Size: 322661 Color: 4

Bin 583: 9 of cap free
Amount of items: 3
Items: 
Size: 399027 Color: 4
Size: 306331 Color: 3
Size: 294634 Color: 3

Bin 584: 9 of cap free
Amount of items: 3
Items: 
Size: 362482 Color: 2
Size: 331735 Color: 1
Size: 305775 Color: 3

Bin 585: 9 of cap free
Amount of items: 3
Items: 
Size: 366921 Color: 0
Size: 339921 Color: 2
Size: 293150 Color: 2

Bin 586: 9 of cap free
Amount of items: 3
Items: 
Size: 369645 Color: 2
Size: 317516 Color: 2
Size: 312831 Color: 3

Bin 587: 9 of cap free
Amount of items: 3
Items: 
Size: 380021 Color: 4
Size: 330501 Color: 0
Size: 289470 Color: 1

Bin 588: 9 of cap free
Amount of items: 3
Items: 
Size: 388645 Color: 0
Size: 336631 Color: 1
Size: 274716 Color: 1

Bin 589: 10 of cap free
Amount of items: 3
Items: 
Size: 405160 Color: 4
Size: 333449 Color: 4
Size: 261382 Color: 3

Bin 590: 10 of cap free
Amount of items: 3
Items: 
Size: 391278 Color: 1
Size: 354188 Color: 1
Size: 254525 Color: 3

Bin 591: 10 of cap free
Amount of items: 3
Items: 
Size: 374395 Color: 3
Size: 323211 Color: 2
Size: 302385 Color: 0

Bin 592: 10 of cap free
Amount of items: 3
Items: 
Size: 354746 Color: 2
Size: 328273 Color: 4
Size: 316972 Color: 2

Bin 593: 10 of cap free
Amount of items: 3
Items: 
Size: 344042 Color: 0
Size: 343827 Color: 2
Size: 312122 Color: 2

Bin 594: 11 of cap free
Amount of items: 3
Items: 
Size: 399721 Color: 1
Size: 320492 Color: 1
Size: 279777 Color: 0

Bin 595: 11 of cap free
Amount of items: 3
Items: 
Size: 363885 Color: 2
Size: 350410 Color: 4
Size: 285695 Color: 3

Bin 596: 11 of cap free
Amount of items: 3
Items: 
Size: 372221 Color: 3
Size: 320209 Color: 4
Size: 307560 Color: 0

Bin 597: 11 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 1
Size: 340869 Color: 4
Size: 286311 Color: 3

Bin 598: 11 of cap free
Amount of items: 3
Items: 
Size: 376504 Color: 4
Size: 342804 Color: 4
Size: 280682 Color: 3

Bin 599: 11 of cap free
Amount of items: 3
Items: 
Size: 355729 Color: 4
Size: 345732 Color: 3
Size: 298529 Color: 3

Bin 600: 12 of cap free
Amount of items: 3
Items: 
Size: 369354 Color: 2
Size: 358579 Color: 3
Size: 272056 Color: 0

Bin 601: 13 of cap free
Amount of items: 3
Items: 
Size: 457965 Color: 4
Size: 273594 Color: 3
Size: 268429 Color: 3

Bin 602: 13 of cap free
Amount of items: 3
Items: 
Size: 386453 Color: 0
Size: 346480 Color: 4
Size: 267055 Color: 3

Bin 603: 14 of cap free
Amount of items: 3
Items: 
Size: 369167 Color: 2
Size: 332183 Color: 0
Size: 298637 Color: 4

Bin 604: 14 of cap free
Amount of items: 3
Items: 
Size: 351106 Color: 2
Size: 347815 Color: 4
Size: 301066 Color: 2

Bin 605: 15 of cap free
Amount of items: 3
Items: 
Size: 352966 Color: 3
Size: 345055 Color: 4
Size: 301965 Color: 3

Bin 606: 17 of cap free
Amount of items: 3
Items: 
Size: 378155 Color: 4
Size: 351121 Color: 3
Size: 270708 Color: 1

Bin 607: 17 of cap free
Amount of items: 3
Items: 
Size: 359756 Color: 3
Size: 322445 Color: 4
Size: 317783 Color: 1

Bin 608: 17 of cap free
Amount of items: 3
Items: 
Size: 365793 Color: 2
Size: 365464 Color: 1
Size: 268727 Color: 0

Bin 609: 21 of cap free
Amount of items: 3
Items: 
Size: 347991 Color: 1
Size: 347193 Color: 1
Size: 304796 Color: 2

Bin 610: 21 of cap free
Amount of items: 3
Items: 
Size: 356848 Color: 0
Size: 346184 Color: 4
Size: 296948 Color: 2

Bin 611: 22 of cap free
Amount of items: 3
Items: 
Size: 376876 Color: 1
Size: 345833 Color: 0
Size: 277270 Color: 3

Bin 612: 23 of cap free
Amount of items: 3
Items: 
Size: 369483 Color: 3
Size: 327019 Color: 1
Size: 303476 Color: 3

Bin 613: 23 of cap free
Amount of items: 3
Items: 
Size: 354687 Color: 4
Size: 345103 Color: 3
Size: 300188 Color: 3

Bin 614: 25 of cap free
Amount of items: 3
Items: 
Size: 348123 Color: 0
Size: 328426 Color: 0
Size: 323427 Color: 4

Bin 615: 25 of cap free
Amount of items: 3
Items: 
Size: 362311 Color: 1
Size: 335630 Color: 2
Size: 302035 Color: 0

Bin 616: 25 of cap free
Amount of items: 3
Items: 
Size: 362233 Color: 0
Size: 331916 Color: 1
Size: 305827 Color: 1

Bin 617: 25 of cap free
Amount of items: 3
Items: 
Size: 362603 Color: 4
Size: 331013 Color: 3
Size: 306360 Color: 0

Bin 618: 25 of cap free
Amount of items: 3
Items: 
Size: 343280 Color: 2
Size: 342021 Color: 4
Size: 314675 Color: 4

Bin 619: 27 of cap free
Amount of items: 3
Items: 
Size: 361333 Color: 2
Size: 344395 Color: 3
Size: 294246 Color: 2

Bin 620: 27 of cap free
Amount of items: 3
Items: 
Size: 362243 Color: 1
Size: 361792 Color: 1
Size: 275939 Color: 4

Bin 621: 27 of cap free
Amount of items: 3
Items: 
Size: 361471 Color: 0
Size: 343917 Color: 3
Size: 294586 Color: 2

Bin 622: 27 of cap free
Amount of items: 3
Items: 
Size: 351314 Color: 4
Size: 351306 Color: 4
Size: 297354 Color: 0

Bin 623: 28 of cap free
Amount of items: 3
Items: 
Size: 354843 Color: 1
Size: 333799 Color: 3
Size: 311331 Color: 0

Bin 624: 28 of cap free
Amount of items: 3
Items: 
Size: 352058 Color: 2
Size: 347047 Color: 3
Size: 300868 Color: 0

Bin 625: 30 of cap free
Amount of items: 3
Items: 
Size: 387372 Color: 0
Size: 353987 Color: 4
Size: 258612 Color: 1

Bin 626: 33 of cap free
Amount of items: 3
Items: 
Size: 353612 Color: 1
Size: 337013 Color: 1
Size: 309343 Color: 4

Bin 627: 40 of cap free
Amount of items: 3
Items: 
Size: 359373 Color: 2
Size: 321704 Color: 4
Size: 318884 Color: 3

Bin 628: 41 of cap free
Amount of items: 3
Items: 
Size: 342021 Color: 2
Size: 333287 Color: 0
Size: 324652 Color: 1

Bin 629: 44 of cap free
Amount of items: 3
Items: 
Size: 352047 Color: 0
Size: 325164 Color: 2
Size: 322746 Color: 0

Bin 630: 45 of cap free
Amount of items: 3
Items: 
Size: 486784 Color: 0
Size: 261708 Color: 0
Size: 251464 Color: 1

Bin 631: 48 of cap free
Amount of items: 3
Items: 
Size: 357903 Color: 0
Size: 345654 Color: 3
Size: 296396 Color: 4

Bin 632: 48 of cap free
Amount of items: 3
Items: 
Size: 366588 Color: 4
Size: 364338 Color: 2
Size: 269027 Color: 0

Bin 633: 55 of cap free
Amount of items: 3
Items: 
Size: 373991 Color: 4
Size: 336803 Color: 2
Size: 289152 Color: 3

Bin 634: 59 of cap free
Amount of items: 3
Items: 
Size: 348993 Color: 1
Size: 328856 Color: 1
Size: 322093 Color: 2

Bin 635: 61 of cap free
Amount of items: 3
Items: 
Size: 357698 Color: 0
Size: 351947 Color: 0
Size: 290295 Color: 2

Bin 636: 64 of cap free
Amount of items: 3
Items: 
Size: 357046 Color: 2
Size: 325119 Color: 0
Size: 317772 Color: 1

Bin 637: 66 of cap free
Amount of items: 3
Items: 
Size: 354476 Color: 1
Size: 323910 Color: 0
Size: 321549 Color: 3

Bin 638: 75 of cap free
Amount of items: 3
Items: 
Size: 369482 Color: 2
Size: 348564 Color: 3
Size: 281880 Color: 4

Bin 639: 77 of cap free
Amount of items: 3
Items: 
Size: 374857 Color: 1
Size: 312933 Color: 3
Size: 312134 Color: 1

Bin 640: 86 of cap free
Amount of items: 3
Items: 
Size: 355377 Color: 0
Size: 352493 Color: 2
Size: 292045 Color: 1

Bin 641: 95 of cap free
Amount of items: 3
Items: 
Size: 347874 Color: 0
Size: 346589 Color: 2
Size: 305443 Color: 1

Bin 642: 103 of cap free
Amount of items: 3
Items: 
Size: 361397 Color: 1
Size: 340763 Color: 0
Size: 297738 Color: 1

Bin 643: 104 of cap free
Amount of items: 3
Items: 
Size: 369832 Color: 4
Size: 318549 Color: 0
Size: 311516 Color: 0

Bin 644: 112 of cap free
Amount of items: 3
Items: 
Size: 491166 Color: 4
Size: 254875 Color: 3
Size: 253848 Color: 0

Bin 645: 115 of cap free
Amount of items: 3
Items: 
Size: 370492 Color: 4
Size: 354302 Color: 2
Size: 275092 Color: 3

Bin 646: 120 of cap free
Amount of items: 3
Items: 
Size: 335549 Color: 0
Size: 334214 Color: 3
Size: 330118 Color: 0

Bin 647: 128 of cap free
Amount of items: 3
Items: 
Size: 356783 Color: 1
Size: 342974 Color: 3
Size: 300116 Color: 0

Bin 648: 133 of cap free
Amount of items: 3
Items: 
Size: 392838 Color: 3
Size: 341758 Color: 3
Size: 265272 Color: 4

Bin 649: 143 of cap free
Amount of items: 3
Items: 
Size: 383244 Color: 3
Size: 348776 Color: 3
Size: 267838 Color: 4

Bin 650: 188 of cap free
Amount of items: 3
Items: 
Size: 462305 Color: 4
Size: 283695 Color: 3
Size: 253813 Color: 1

Bin 651: 191 of cap free
Amount of items: 3
Items: 
Size: 382947 Color: 3
Size: 315593 Color: 4
Size: 301270 Color: 1

Bin 652: 194 of cap free
Amount of items: 3
Items: 
Size: 350742 Color: 1
Size: 330583 Color: 3
Size: 318482 Color: 3

Bin 653: 203 of cap free
Amount of items: 3
Items: 
Size: 367908 Color: 0
Size: 328358 Color: 4
Size: 303532 Color: 4

Bin 654: 246 of cap free
Amount of items: 3
Items: 
Size: 339548 Color: 2
Size: 336111 Color: 4
Size: 324096 Color: 3

Bin 655: 327 of cap free
Amount of items: 3
Items: 
Size: 372581 Color: 1
Size: 363805 Color: 0
Size: 263288 Color: 3

Bin 656: 408 of cap free
Amount of items: 3
Items: 
Size: 392237 Color: 1
Size: 351875 Color: 3
Size: 255481 Color: 3

Bin 657: 411 of cap free
Amount of items: 3
Items: 
Size: 344227 Color: 4
Size: 328291 Color: 4
Size: 327072 Color: 3

Bin 658: 442 of cap free
Amount of items: 3
Items: 
Size: 356596 Color: 0
Size: 350190 Color: 1
Size: 292773 Color: 4

Bin 659: 813 of cap free
Amount of items: 3
Items: 
Size: 402949 Color: 0
Size: 320556 Color: 1
Size: 275683 Color: 0

Bin 660: 1620 of cap free
Amount of items: 3
Items: 
Size: 360590 Color: 3
Size: 358041 Color: 2
Size: 279750 Color: 2

Bin 661: 2854 of cap free
Amount of items: 2
Items: 
Size: 499156 Color: 1
Size: 497991 Color: 2

Bin 662: 3093 of cap free
Amount of items: 3
Items: 
Size: 352671 Color: 2
Size: 342206 Color: 3
Size: 302031 Color: 0

Bin 663: 4777 of cap free
Amount of items: 2
Items: 
Size: 497982 Color: 4
Size: 497242 Color: 3

Bin 664: 75265 of cap free
Amount of items: 3
Items: 
Size: 319108 Color: 3
Size: 317553 Color: 0
Size: 288075 Color: 0

Bin 665: 202052 of cap free
Amount of items: 3
Items: 
Size: 270333 Color: 0
Size: 264414 Color: 3
Size: 263202 Color: 3

Bin 666: 218541 of cap free
Amount of items: 3
Items: 
Size: 262189 Color: 4
Size: 261219 Color: 3
Size: 258052 Color: 1

Bin 667: 233293 of cap free
Amount of items: 3
Items: 
Size: 257599 Color: 2
Size: 256063 Color: 1
Size: 253046 Color: 4

Bin 668: 251953 of cap free
Amount of items: 2
Items: 
Size: 497972 Color: 4
Size: 250076 Color: 3

Total size: 667000667
Total free space: 1000001

