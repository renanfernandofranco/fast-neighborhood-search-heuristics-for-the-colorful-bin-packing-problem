Capicity Bin: 8000
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6900 Color: 381
Size: 924 Color: 157
Size: 176 Color: 30

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 3952 Color: 272
Size: 1088 Color: 172
Size: 992 Color: 164
Size: 632 Color: 130
Size: 512 Color: 120
Size: 448 Color: 111
Size: 376 Color: 96

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 365
Size: 1106 Color: 175
Size: 220 Color: 49

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 278
Size: 3212 Color: 261
Size: 640 Color: 132

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 305
Size: 2104 Color: 236
Size: 416 Color: 104

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 5620 Color: 310
Size: 2012 Color: 231
Size: 168 Color: 26
Size: 112 Color: 7
Size: 88 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 378
Size: 969 Color: 160
Size: 192 Color: 37

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5915 Color: 322
Size: 1739 Color: 220
Size: 346 Color: 90

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5954 Color: 323
Size: 1706 Color: 219
Size: 340 Color: 87

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 332
Size: 1490 Color: 209
Size: 296 Color: 78

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 353
Size: 1195 Color: 184
Size: 238 Color: 59

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 314
Size: 1957 Color: 227
Size: 390 Color: 97

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 393
Size: 818 Color: 146
Size: 160 Color: 20

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 395
Size: 802 Color: 144
Size: 156 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 358
Size: 1162 Color: 182
Size: 228 Color: 53

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6814 Color: 375
Size: 990 Color: 162
Size: 196 Color: 39

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4990 Color: 295
Size: 2510 Color: 247
Size: 500 Color: 117

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7006 Color: 390
Size: 830 Color: 149
Size: 164 Color: 23

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4004 Color: 275
Size: 3332 Color: 268
Size: 664 Color: 133

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6193 Color: 331
Size: 1603 Color: 214
Size: 204 Color: 43

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 287
Size: 3228 Color: 262
Size: 112 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6679 Color: 366
Size: 1101 Color: 173
Size: 220 Color: 48

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 344
Size: 1310 Color: 197
Size: 260 Color: 66

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6066 Color: 327
Size: 1614 Color: 215
Size: 320 Color: 83

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6153 Color: 328
Size: 1541 Color: 213
Size: 306 Color: 82

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 379
Size: 954 Color: 159
Size: 188 Color: 35

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 318
Size: 1804 Color: 224
Size: 352 Color: 92

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5482 Color: 306
Size: 2102 Color: 235
Size: 416 Color: 105

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 388
Size: 854 Color: 151
Size: 168 Color: 25

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 345
Size: 1305 Color: 196
Size: 260 Color: 65

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 304
Size: 2140 Color: 237
Size: 424 Color: 106

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4430 Color: 282
Size: 3274 Color: 264
Size: 296 Color: 79

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4426 Color: 281
Size: 2982 Color: 259
Size: 592 Color: 129

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 402
Size: 674 Color: 137
Size: 132 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 391
Size: 828 Color: 148
Size: 160 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4884 Color: 291
Size: 2604 Color: 251
Size: 512 Color: 121

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5657 Color: 315
Size: 2193 Color: 240
Size: 150 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6682 Color: 367
Size: 1102 Color: 174
Size: 216 Color: 47

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 400
Size: 716 Color: 139
Size: 136 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 316
Size: 1863 Color: 226
Size: 372 Color: 95

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 294
Size: 2540 Color: 248
Size: 504 Color: 118

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5908 Color: 321
Size: 1748 Color: 221
Size: 344 Color: 89

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 354
Size: 1208 Color: 187
Size: 224 Color: 50

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 383
Size: 991 Color: 163
Size: 86 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6042 Color: 326
Size: 1634 Color: 216
Size: 324 Color: 84

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 307
Size: 2084 Color: 234
Size: 408 Color: 103

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 361
Size: 1149 Color: 178
Size: 228 Color: 54

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 290
Size: 2686 Color: 252
Size: 536 Color: 124

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 337
Size: 1436 Color: 204
Size: 280 Color: 71

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 330
Size: 1524 Color: 211
Size: 304 Color: 80

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4268 Color: 280
Size: 3260 Color: 263
Size: 472 Color: 113

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 339
Size: 1356 Color: 201
Size: 264 Color: 67

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 349
Size: 1231 Color: 189
Size: 246 Color: 61

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 360
Size: 1156 Color: 181
Size: 224 Color: 51

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 340
Size: 1347 Color: 200
Size: 268 Color: 70

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 341
Size: 1240 Color: 190
Size: 368 Color: 94

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 351
Size: 1362 Color: 202
Size: 88 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5305 Color: 300
Size: 2247 Color: 242
Size: 448 Color: 110

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 355
Size: 1196 Color: 185
Size: 232 Color: 57

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 348
Size: 1252 Color: 192
Size: 248 Color: 62

Bin 61: 0 of cap free
Amount of items: 4
Items: 
Size: 6948 Color: 386
Size: 884 Color: 154
Size: 120 Color: 8
Size: 48 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 374
Size: 1154 Color: 179
Size: 44 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 289
Size: 2716 Color: 253
Size: 536 Color: 123

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 4902 Color: 292
Size: 2582 Color: 250
Size: 516 Color: 122

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 346
Size: 1298 Color: 195
Size: 256 Color: 64

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5358 Color: 301
Size: 2202 Color: 241
Size: 440 Color: 109

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 384
Size: 894 Color: 155
Size: 176 Color: 31

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5558 Color: 308
Size: 2038 Color: 233
Size: 404 Color: 102

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5625 Color: 311
Size: 1981 Color: 230
Size: 394 Color: 99

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 284
Size: 2900 Color: 257
Size: 576 Color: 128

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6634 Color: 363
Size: 1178 Color: 183
Size: 188 Color: 34

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5886 Color: 319
Size: 1762 Color: 222
Size: 352 Color: 91

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 313
Size: 1962 Color: 228
Size: 392 Color: 98

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 298
Size: 2412 Color: 244
Size: 480 Color: 114

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7100 Color: 397
Size: 756 Color: 142
Size: 144 Color: 14

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 7158 Color: 401
Size: 702 Color: 138
Size: 140 Color: 12

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 338
Size: 1414 Color: 203
Size: 280 Color: 72

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 334
Size: 1474 Color: 207
Size: 292 Color: 76

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6941 Color: 385
Size: 883 Color: 153
Size: 176 Color: 29

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 371
Size: 1031 Color: 168
Size: 204 Color: 42

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6258 Color: 335
Size: 1454 Color: 206
Size: 288 Color: 74

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 312
Size: 1977 Color: 229
Size: 394 Color: 100

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 4002 Color: 274
Size: 3334 Color: 269
Size: 664 Color: 136

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 3512 Color: 270
Size: 2976 Color: 258
Size: 1512 Color: 210

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 372
Size: 1028 Color: 167
Size: 200 Color: 40

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 392
Size: 820 Color: 147
Size: 160 Color: 18

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4005 Color: 276
Size: 3331 Color: 267
Size: 664 Color: 134

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5014 Color: 296
Size: 2490 Color: 246
Size: 496 Color: 116

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 357
Size: 1294 Color: 194
Size: 112 Color: 6

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 329
Size: 1540 Color: 212
Size: 304 Color: 81

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6821 Color: 376
Size: 983 Color: 161
Size: 196 Color: 38

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6915 Color: 382
Size: 905 Color: 156
Size: 180 Color: 32

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5581 Color: 309
Size: 2017 Color: 232
Size: 402 Color: 101

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 303
Size: 2164 Color: 238
Size: 432 Color: 108

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6831 Color: 377
Size: 1007 Color: 165
Size: 162 Color: 22

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6971 Color: 387
Size: 859 Color: 152
Size: 170 Color: 27

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 399
Size: 724 Color: 140
Size: 136 Color: 11

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 352
Size: 1202 Color: 186
Size: 236 Color: 58

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 347
Size: 1284 Color: 193
Size: 248 Color: 63

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5399 Color: 302
Size: 2169 Color: 239
Size: 432 Color: 107

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5270 Color: 299
Size: 2278 Color: 243
Size: 452 Color: 112

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 4207 Color: 279
Size: 3161 Color: 260
Size: 632 Color: 131

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4001 Color: 273
Size: 3709 Color: 271
Size: 290 Color: 75

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 285
Size: 2883 Color: 256
Size: 576 Color: 127

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 394
Size: 810 Color: 145
Size: 160 Color: 21

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 373
Size: 1020 Color: 166
Size: 200 Color: 41

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6737 Color: 369
Size: 1053 Color: 170
Size: 210 Color: 45

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6269 Color: 336
Size: 1443 Color: 205
Size: 288 Color: 73

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 320
Size: 1764 Color: 223
Size: 344 Color: 88

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 362
Size: 1146 Color: 177
Size: 228 Color: 55

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 364
Size: 1124 Color: 176
Size: 224 Color: 52

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 7118 Color: 398
Size: 738 Color: 141
Size: 144 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6003 Color: 325
Size: 1665 Color: 217
Size: 332 Color: 85

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5963 Color: 324
Size: 1699 Color: 218
Size: 338 Color: 86

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4006 Color: 277
Size: 3330 Color: 266
Size: 664 Color: 135

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6727 Color: 368
Size: 1061 Color: 171
Size: 212 Color: 46

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4945 Color: 293
Size: 2547 Color: 249
Size: 508 Color: 119

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6225 Color: 333
Size: 1481 Color: 208
Size: 294 Color: 77

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6395 Color: 342
Size: 1339 Color: 199
Size: 266 Color: 69

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 380
Size: 931 Color: 158
Size: 186 Color: 33

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 317
Size: 1806 Color: 225
Size: 360 Color: 93

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 396
Size: 774 Color: 143
Size: 152 Color: 16

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 4711 Color: 288
Size: 2741 Color: 254
Size: 548 Color: 125

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6547 Color: 350
Size: 1211 Color: 188
Size: 242 Color: 60

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 370
Size: 1046 Color: 169
Size: 208 Color: 44

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 4574 Color: 286
Size: 2858 Color: 255
Size: 568 Color: 126

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6995 Color: 389
Size: 839 Color: 150
Size: 166 Color: 24

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6615 Color: 359
Size: 1155 Color: 180
Size: 230 Color: 56

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 356
Size: 1251 Color: 191
Size: 170 Color: 28

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4497 Color: 283
Size: 3311 Color: 265
Size: 192 Color: 36

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5087 Color: 297
Size: 2429 Color: 245
Size: 484 Color: 115

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6403 Color: 343
Size: 1331 Color: 198
Size: 266 Color: 68

Total size: 1056000
Total free space: 0

