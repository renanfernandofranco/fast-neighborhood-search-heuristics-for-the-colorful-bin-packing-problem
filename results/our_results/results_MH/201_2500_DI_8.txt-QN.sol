Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 158
Size: 874 Color: 127
Size: 8 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1492 Color: 151
Size: 384 Color: 89
Size: 272 Color: 77
Size: 160 Color: 57
Size: 148 Color: 54

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1558 Color: 156
Size: 674 Color: 114
Size: 88 Color: 33
Size: 88 Color: 32
Size: 48 Color: 14

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1536 Color: 153
Size: 840 Color: 123
Size: 40 Color: 7
Size: 40 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 167
Size: 611 Color: 107
Size: 120 Color: 44

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 145
Size: 980 Color: 132
Size: 112 Color: 42

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 141
Size: 976 Color: 131
Size: 244 Color: 75

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2132 Color: 193
Size: 276 Color: 78
Size: 48 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 140
Size: 1021 Color: 135
Size: 204 Color: 68

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 198
Size: 232 Color: 73
Size: 32 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1452 Color: 149
Size: 804 Color: 121
Size: 200 Color: 65

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 186
Size: 340 Color: 86
Size: 64 Color: 22

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 196
Size: 212 Color: 70
Size: 72 Color: 24

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 174
Size: 538 Color: 100
Size: 104 Color: 38

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 138
Size: 1023 Color: 137
Size: 204 Color: 67

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 197
Size: 228 Color: 72
Size: 40 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 189
Size: 334 Color: 85
Size: 24 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 188
Size: 330 Color: 83
Size: 64 Color: 21

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 190
Size: 300 Color: 82
Size: 48 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 176
Size: 482 Color: 97
Size: 92 Color: 36

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 157
Size: 745 Color: 117
Size: 148 Color: 53

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 163
Size: 646 Color: 110
Size: 128 Color: 48

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 179
Size: 444 Color: 94
Size: 80 Color: 31

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 182
Size: 402 Color: 91
Size: 76 Color: 26

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1351 Color: 144
Size: 921 Color: 128
Size: 184 Color: 63

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 166
Size: 738 Color: 116
Size: 8 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1449 Color: 148
Size: 841 Color: 125
Size: 166 Color: 59

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 195
Size: 242 Color: 74
Size: 48 Color: 15

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 1696 Color: 165
Size: 568 Color: 104
Size: 144 Color: 51
Size: 48 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1020 Color: 134
Size: 924 Color: 130
Size: 512 Color: 98

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 191
Size: 286 Color: 81
Size: 56 Color: 17

Bin 32: 0 of cap free
Amount of items: 4
Items: 
Size: 1349 Color: 143
Size: 923 Color: 129
Size: 168 Color: 60
Size: 16 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 185
Size: 364 Color: 87
Size: 64 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 178
Size: 454 Color: 95
Size: 88 Color: 34

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 139
Size: 1022 Color: 136
Size: 204 Color: 66

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 184
Size: 374 Color: 88
Size: 72 Color: 25

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 146
Size: 862 Color: 126
Size: 184 Color: 62

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1844 Color: 175
Size: 516 Color: 99
Size: 96 Color: 37

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 183
Size: 386 Color: 90
Size: 76 Color: 27

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 150
Size: 827 Color: 122
Size: 164 Color: 58

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 181
Size: 410 Color: 92
Size: 80 Color: 29

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 173
Size: 540 Color: 101
Size: 104 Color: 39

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 172
Size: 550 Color: 102
Size: 108 Color: 40

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 170
Size: 634 Color: 108
Size: 64 Color: 20

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 199
Size: 214 Color: 71
Size: 40 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 147
Size: 750 Color: 118
Size: 280 Color: 79

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1668 Color: 162
Size: 660 Color: 111
Size: 128 Color: 47

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 164
Size: 638 Color: 109
Size: 124 Color: 46

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 187
Size: 334 Color: 84
Size: 64 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 142
Size: 1018 Color: 133
Size: 200 Color: 64

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 200
Size: 172 Color: 61
Size: 80 Color: 30

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 169
Size: 604 Color: 105
Size: 112 Color: 43

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 194
Size: 246 Color: 76
Size: 48 Color: 12

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1580 Color: 159
Size: 732 Color: 115
Size: 144 Color: 52

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 177
Size: 466 Color: 96
Size: 92 Color: 35

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 152
Size: 771 Color: 120
Size: 154 Color: 56

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 171
Size: 553 Color: 103
Size: 110 Color: 41

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 180
Size: 420 Color: 93
Size: 80 Color: 28

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 160
Size: 671 Color: 113
Size: 134 Color: 50

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 155
Size: 751 Color: 119
Size: 150 Color: 55

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 154
Size: 841 Color: 124
Size: 68 Color: 23

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 192
Size: 282 Color: 80
Size: 56 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 161
Size: 665 Color: 112
Size: 132 Color: 49

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 168
Size: 605 Color: 106
Size: 120 Color: 45

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 201
Size: 210 Color: 69
Size: 40 Color: 9

Total size: 159640
Total free space: 0

