Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 52
Size: 277 Color: 21
Size: 273 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 57
Size: 262 Color: 12
Size: 254 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 48
Size: 303 Color: 28
Size: 282 Color: 25

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 43
Size: 367 Color: 42
Size: 266 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 272 Color: 18
Size: 474 Color: 56
Size: 254 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 44
Size: 361 Color: 39
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 59
Size: 257 Color: 6
Size: 251 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 38
Size: 340 Color: 36
Size: 300 Color: 27

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 45
Size: 327 Color: 32
Size: 274 Color: 20

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 37
Size: 336 Color: 34
Size: 311 Color: 30

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 54
Size: 278 Color: 22
Size: 258 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 51
Size: 282 Color: 26
Size: 270 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 47
Size: 309 Color: 29
Size: 279 Color: 23

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 53
Size: 281 Color: 24
Size: 259 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 55
Size: 270 Color: 16
Size: 260 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 58
Size: 257 Color: 7
Size: 252 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 41
Size: 365 Color: 40
Size: 269 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 50
Size: 311 Color: 31
Size: 260 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 46
Size: 336 Color: 35
Size: 264 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 49
Size: 334 Color: 33
Size: 251 Color: 2

Total size: 20000
Total free space: 0

