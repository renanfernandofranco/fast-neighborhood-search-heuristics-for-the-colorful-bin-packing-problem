Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 361957 Color: 19
Size: 324936 Color: 1
Size: 313108 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 363238 Color: 8
Size: 332119 Color: 8
Size: 304644 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 363974 Color: 11
Size: 321520 Color: 18
Size: 314507 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 366689 Color: 12
Size: 346824 Color: 11
Size: 286488 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 366873 Color: 7
Size: 365613 Color: 16
Size: 267515 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 365259 Color: 17
Size: 328597 Color: 3
Size: 306145 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 367646 Color: 9
Size: 337951 Color: 0
Size: 294404 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 367694 Color: 10
Size: 327272 Color: 19
Size: 305035 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 368663 Color: 16
Size: 321890 Color: 8
Size: 309448 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 369069 Color: 1
Size: 326459 Color: 19
Size: 304473 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 369284 Color: 5
Size: 355089 Color: 14
Size: 275628 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 369419 Color: 14
Size: 369213 Color: 9
Size: 261369 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 370099 Color: 12
Size: 334449 Color: 13
Size: 295453 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 370489 Color: 15
Size: 360660 Color: 1
Size: 268852 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 371219 Color: 15
Size: 366519 Color: 0
Size: 262263 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 371794 Color: 14
Size: 344361 Color: 11
Size: 283846 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 372280 Color: 16
Size: 343681 Color: 19
Size: 284040 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 372644 Color: 16
Size: 334741 Color: 19
Size: 292616 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 373333 Color: 10
Size: 326218 Color: 7
Size: 300450 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 374073 Color: 12
Size: 362485 Color: 9
Size: 263443 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 374110 Color: 17
Size: 353554 Color: 6
Size: 272337 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 374766 Color: 13
Size: 342108 Color: 2
Size: 283127 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 374875 Color: 7
Size: 320413 Color: 11
Size: 304713 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 374914 Color: 2
Size: 356755 Color: 15
Size: 268332 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 375170 Color: 7
Size: 371304 Color: 6
Size: 253527 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 375937 Color: 16
Size: 345222 Color: 10
Size: 278842 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 375958 Color: 1
Size: 366315 Color: 1
Size: 257728 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 376017 Color: 18
Size: 347623 Color: 6
Size: 276361 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 376272 Color: 11
Size: 325070 Color: 14
Size: 298659 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 376321 Color: 15
Size: 334402 Color: 4
Size: 289278 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 377333 Color: 6
Size: 324131 Color: 11
Size: 298537 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 378219 Color: 16
Size: 330283 Color: 18
Size: 291499 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 378233 Color: 13
Size: 370213 Color: 2
Size: 251555 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 378895 Color: 13
Size: 341827 Color: 6
Size: 279279 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 378972 Color: 3
Size: 357672 Color: 14
Size: 263357 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 379833 Color: 14
Size: 359058 Color: 11
Size: 261110 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 379870 Color: 9
Size: 337109 Color: 13
Size: 283022 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 379910 Color: 2
Size: 368987 Color: 10
Size: 251104 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 379981 Color: 6
Size: 323702 Color: 4
Size: 296318 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 380054 Color: 4
Size: 349792 Color: 16
Size: 270155 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 380426 Color: 2
Size: 361854 Color: 7
Size: 257721 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 381424 Color: 17
Size: 319825 Color: 5
Size: 298752 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 381920 Color: 4
Size: 313461 Color: 14
Size: 304620 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 382663 Color: 19
Size: 353245 Color: 7
Size: 264093 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 382688 Color: 14
Size: 310060 Color: 16
Size: 307253 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 382833 Color: 10
Size: 356245 Color: 5
Size: 260923 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 383172 Color: 19
Size: 312192 Color: 7
Size: 304637 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 383418 Color: 9
Size: 342791 Color: 15
Size: 273792 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 383836 Color: 12
Size: 346842 Color: 15
Size: 269323 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 384142 Color: 14
Size: 325912 Color: 4
Size: 289947 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 384230 Color: 12
Size: 365287 Color: 4
Size: 250484 Color: 9

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 384296 Color: 1
Size: 356599 Color: 17
Size: 259106 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 385130 Color: 9
Size: 319699 Color: 10
Size: 295172 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 385388 Color: 10
Size: 338856 Color: 16
Size: 275757 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 385583 Color: 3
Size: 340745 Color: 3
Size: 273673 Color: 16

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 385807 Color: 8
Size: 345493 Color: 1
Size: 268701 Color: 18

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 386093 Color: 19
Size: 335733 Color: 9
Size: 278175 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 386333 Color: 8
Size: 344485 Color: 9
Size: 269183 Color: 7

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 386630 Color: 1
Size: 321604 Color: 12
Size: 291767 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 386730 Color: 14
Size: 351830 Color: 13
Size: 261441 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 387017 Color: 14
Size: 332344 Color: 6
Size: 280640 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 386861 Color: 15
Size: 360449 Color: 1
Size: 252691 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 387214 Color: 10
Size: 358596 Color: 1
Size: 254191 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 387561 Color: 0
Size: 356759 Color: 2
Size: 255681 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 387720 Color: 19
Size: 349239 Color: 6
Size: 263042 Color: 17

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 388303 Color: 3
Size: 359506 Color: 19
Size: 252192 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 388434 Color: 13
Size: 320823 Color: 6
Size: 290744 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 389215 Color: 19
Size: 345943 Color: 2
Size: 264843 Color: 9

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 389249 Color: 8
Size: 322748 Color: 8
Size: 288004 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 389358 Color: 0
Size: 360464 Color: 11
Size: 250179 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 389612 Color: 15
Size: 326797 Color: 4
Size: 283592 Color: 9

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 389917 Color: 8
Size: 320463 Color: 15
Size: 289621 Color: 13

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 389969 Color: 11
Size: 316079 Color: 12
Size: 293953 Color: 11

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 390109 Color: 19
Size: 323220 Color: 18
Size: 286672 Color: 17

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 391755 Color: 5
Size: 307532 Color: 7
Size: 300714 Color: 15

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 391817 Color: 16
Size: 338480 Color: 7
Size: 269704 Color: 10

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 392188 Color: 14
Size: 334846 Color: 9
Size: 272967 Color: 14

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 392422 Color: 2
Size: 307372 Color: 17
Size: 300207 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 393219 Color: 5
Size: 336467 Color: 12
Size: 270315 Color: 9

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 392905 Color: 3
Size: 354796 Color: 2
Size: 252300 Color: 15

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 393154 Color: 4
Size: 311227 Color: 12
Size: 295620 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 393231 Color: 10
Size: 322140 Color: 6
Size: 284630 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 393534 Color: 4
Size: 345775 Color: 9
Size: 260692 Color: 10

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 393776 Color: 7
Size: 307174 Color: 7
Size: 299051 Color: 8

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 394474 Color: 9
Size: 308656 Color: 12
Size: 296871 Color: 8

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 394835 Color: 17
Size: 335417 Color: 17
Size: 269749 Color: 7

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 394883 Color: 13
Size: 310449 Color: 10
Size: 294669 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 395236 Color: 8
Size: 309576 Color: 7
Size: 295189 Color: 13

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 395349 Color: 4
Size: 305955 Color: 14
Size: 298697 Color: 18

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 395598 Color: 0
Size: 350131 Color: 4
Size: 254272 Color: 7

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 395963 Color: 13
Size: 325923 Color: 11
Size: 278115 Color: 16

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 395999 Color: 0
Size: 353808 Color: 3
Size: 250194 Color: 13

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 396417 Color: 17
Size: 328112 Color: 8
Size: 275472 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 396653 Color: 8
Size: 332537 Color: 6
Size: 270811 Color: 9

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 396798 Color: 0
Size: 303179 Color: 3
Size: 300024 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 396852 Color: 13
Size: 344559 Color: 3
Size: 258590 Color: 12

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 397040 Color: 1
Size: 342071 Color: 17
Size: 260890 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 397445 Color: 2
Size: 346486 Color: 9
Size: 256070 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 397721 Color: 7
Size: 347646 Color: 7
Size: 254634 Color: 11

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 398984 Color: 16
Size: 344325 Color: 18
Size: 256692 Color: 8

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 399255 Color: 0
Size: 331960 Color: 13
Size: 268786 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 399289 Color: 17
Size: 348823 Color: 15
Size: 251889 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 399389 Color: 8
Size: 330899 Color: 18
Size: 269713 Color: 9

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 399546 Color: 17
Size: 350411 Color: 12
Size: 250044 Color: 16

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 399576 Color: 18
Size: 305428 Color: 4
Size: 294997 Color: 5

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 400317 Color: 4
Size: 309985 Color: 7
Size: 289699 Color: 11

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 400794 Color: 13
Size: 303280 Color: 11
Size: 295927 Color: 9

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 401044 Color: 18
Size: 334108 Color: 17
Size: 264849 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 401103 Color: 14
Size: 322844 Color: 18
Size: 276054 Color: 18

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 401531 Color: 19
Size: 318023 Color: 0
Size: 280447 Color: 7

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 402324 Color: 14
Size: 304438 Color: 7
Size: 293239 Color: 12

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 403237 Color: 15
Size: 346299 Color: 9
Size: 250465 Color: 15

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 403513 Color: 13
Size: 306682 Color: 4
Size: 289806 Color: 17

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 403244 Color: 8
Size: 300194 Color: 1
Size: 296563 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 403516 Color: 1
Size: 345977 Color: 19
Size: 250508 Color: 17

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 403897 Color: 9
Size: 321199 Color: 0
Size: 274905 Color: 6

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 404086 Color: 15
Size: 308715 Color: 0
Size: 287200 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 404185 Color: 14
Size: 310084 Color: 3
Size: 285732 Color: 19

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 404407 Color: 10
Size: 308116 Color: 14
Size: 287478 Color: 18

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 404522 Color: 16
Size: 318966 Color: 14
Size: 276513 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 404653 Color: 13
Size: 322939 Color: 2
Size: 272409 Color: 8

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 404702 Color: 3
Size: 322316 Color: 18
Size: 272983 Color: 9

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 404913 Color: 5
Size: 317622 Color: 11
Size: 277466 Color: 15

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 404928 Color: 6
Size: 331844 Color: 4
Size: 263229 Color: 10

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 404974 Color: 10
Size: 308906 Color: 3
Size: 286121 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 405488 Color: 13
Size: 339660 Color: 17
Size: 254853 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 405066 Color: 3
Size: 343593 Color: 16
Size: 251342 Color: 7

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 405496 Color: 0
Size: 338359 Color: 3
Size: 256146 Color: 15

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 405520 Color: 4
Size: 298895 Color: 7
Size: 295586 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 405547 Color: 15
Size: 315578 Color: 7
Size: 278876 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 405961 Color: 7
Size: 334484 Color: 6
Size: 259556 Color: 10

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 406055 Color: 10
Size: 317820 Color: 17
Size: 276126 Color: 7

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 406375 Color: 16
Size: 342461 Color: 5
Size: 251165 Color: 9

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 406810 Color: 17
Size: 302274 Color: 12
Size: 290917 Color: 8

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 407471 Color: 13
Size: 299394 Color: 7
Size: 293136 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 407439 Color: 3
Size: 323549 Color: 8
Size: 269013 Color: 3

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 407563 Color: 15
Size: 322214 Color: 8
Size: 270224 Color: 9

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 407813 Color: 8
Size: 306955 Color: 17
Size: 285233 Color: 16

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 408380 Color: 16
Size: 309056 Color: 11
Size: 282565 Color: 10

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 408460 Color: 17
Size: 307145 Color: 16
Size: 284396 Color: 19

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 408616 Color: 12
Size: 310576 Color: 16
Size: 280809 Color: 5

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 408927 Color: 2
Size: 332307 Color: 0
Size: 258767 Color: 8

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 408970 Color: 17
Size: 324235 Color: 0
Size: 266796 Color: 18

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 409649 Color: 13
Size: 321547 Color: 4
Size: 268805 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 409837 Color: 15
Size: 317372 Color: 5
Size: 272792 Color: 18

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 409945 Color: 10
Size: 298892 Color: 15
Size: 291164 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 409953 Color: 15
Size: 334414 Color: 2
Size: 255634 Color: 10

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 411163 Color: 13
Size: 307052 Color: 2
Size: 281786 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 410998 Color: 10
Size: 318948 Color: 11
Size: 270055 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 411813 Color: 16
Size: 298829 Color: 17
Size: 289359 Color: 16

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 411949 Color: 18
Size: 299880 Color: 19
Size: 288172 Color: 17

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 411979 Color: 12
Size: 314618 Color: 7
Size: 273404 Color: 15

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 412877 Color: 13
Size: 293859 Color: 8
Size: 293265 Color: 5

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 412456 Color: 7
Size: 317912 Color: 0
Size: 269633 Color: 9

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 412518 Color: 15
Size: 334519 Color: 18
Size: 252964 Color: 17

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 413117 Color: 16
Size: 327048 Color: 1
Size: 259836 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 413493 Color: 10
Size: 325610 Color: 15
Size: 260898 Color: 10

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 413802 Color: 13
Size: 324472 Color: 11
Size: 261727 Color: 14

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 413643 Color: 17
Size: 311880 Color: 18
Size: 274478 Color: 11

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 413976 Color: 18
Size: 299201 Color: 3
Size: 286824 Color: 6

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 414351 Color: 18
Size: 321391 Color: 8
Size: 264259 Color: 9

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 415182 Color: 10
Size: 310788 Color: 8
Size: 274031 Color: 11

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 415683 Color: 3
Size: 306872 Color: 18
Size: 277446 Color: 13

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 415778 Color: 14
Size: 299424 Color: 9
Size: 284799 Color: 12

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 415890 Color: 0
Size: 319710 Color: 16
Size: 264401 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 415936 Color: 18
Size: 328906 Color: 18
Size: 255159 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 416770 Color: 3
Size: 319643 Color: 10
Size: 263588 Color: 11

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 417147 Color: 15
Size: 323175 Color: 11
Size: 259679 Color: 5

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 417433 Color: 14
Size: 302440 Color: 11
Size: 280128 Color: 18

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 418245 Color: 5
Size: 299798 Color: 18
Size: 281958 Color: 2

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 419105 Color: 3
Size: 305007 Color: 5
Size: 275889 Color: 2

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 419232 Color: 8
Size: 313867 Color: 1
Size: 266902 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 419435 Color: 8
Size: 312375 Color: 8
Size: 268191 Color: 19

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 419569 Color: 15
Size: 308311 Color: 14
Size: 272121 Color: 17

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 419588 Color: 10
Size: 315515 Color: 16
Size: 264898 Color: 4

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 419656 Color: 16
Size: 300854 Color: 11
Size: 279491 Color: 9

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 420821 Color: 13
Size: 319074 Color: 4
Size: 260106 Color: 5

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 419685 Color: 0
Size: 320298 Color: 6
Size: 260018 Color: 7

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 421269 Color: 6
Size: 291109 Color: 14
Size: 287623 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 421451 Color: 5
Size: 316913 Color: 0
Size: 261637 Color: 14

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 421873 Color: 1
Size: 325717 Color: 2
Size: 252411 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 421885 Color: 3
Size: 294209 Color: 18
Size: 283907 Color: 10

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 421974 Color: 14
Size: 290168 Color: 3
Size: 287859 Color: 19

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 422019 Color: 7
Size: 301066 Color: 6
Size: 276916 Color: 15

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 422085 Color: 1
Size: 300916 Color: 0
Size: 277000 Color: 12

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 422475 Color: 11
Size: 299364 Color: 14
Size: 278162 Color: 16

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 422563 Color: 4
Size: 319833 Color: 14
Size: 257605 Color: 19

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 422626 Color: 16
Size: 308393 Color: 12
Size: 268982 Color: 10

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 422785 Color: 7
Size: 314191 Color: 0
Size: 263025 Color: 2

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 423026 Color: 7
Size: 325654 Color: 17
Size: 251321 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 423353 Color: 4
Size: 291789 Color: 5
Size: 284859 Color: 11

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 423733 Color: 12
Size: 325959 Color: 14
Size: 250309 Color: 14

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 424081 Color: 4
Size: 295477 Color: 17
Size: 280443 Color: 15

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 424350 Color: 15
Size: 314078 Color: 17
Size: 261573 Color: 11

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 425198 Color: 8
Size: 316702 Color: 1
Size: 258101 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 425591 Color: 4
Size: 304211 Color: 15
Size: 270199 Color: 6

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 425592 Color: 2
Size: 300621 Color: 7
Size: 273788 Color: 18

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 425611 Color: 7
Size: 312583 Color: 9
Size: 261807 Color: 5

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 425679 Color: 19
Size: 305102 Color: 15
Size: 269220 Color: 9

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 426125 Color: 17
Size: 302158 Color: 12
Size: 271718 Color: 3

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 426346 Color: 1
Size: 290708 Color: 3
Size: 282947 Color: 9

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 426761 Color: 2
Size: 310977 Color: 7
Size: 262263 Color: 17

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 427200 Color: 19
Size: 318287 Color: 18
Size: 254514 Color: 9

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 427237 Color: 2
Size: 312062 Color: 17
Size: 260702 Color: 10

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 427411 Color: 16
Size: 296689 Color: 2
Size: 275901 Color: 11

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 427448 Color: 12
Size: 318304 Color: 8
Size: 254249 Color: 16

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 428040 Color: 9
Size: 318880 Color: 18
Size: 253081 Color: 18

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 428155 Color: 7
Size: 319099 Color: 4
Size: 252747 Color: 13

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 428172 Color: 15
Size: 311507 Color: 16
Size: 260322 Color: 15

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 428923 Color: 10
Size: 291807 Color: 19
Size: 279271 Color: 18

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 428949 Color: 10
Size: 293922 Color: 18
Size: 277130 Color: 12

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 429306 Color: 18
Size: 285669 Color: 17
Size: 285026 Color: 5

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 429375 Color: 6
Size: 292618 Color: 14
Size: 278008 Color: 9

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 429537 Color: 13
Size: 291133 Color: 17
Size: 279331 Color: 5

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 429405 Color: 8
Size: 295661 Color: 5
Size: 274935 Color: 17

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 429781 Color: 10
Size: 288066 Color: 12
Size: 282154 Color: 2

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 429935 Color: 3
Size: 288196 Color: 9
Size: 281870 Color: 6

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 430168 Color: 15
Size: 317405 Color: 8
Size: 252428 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 430598 Color: 8
Size: 314456 Color: 18
Size: 254947 Color: 5

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 431264 Color: 2
Size: 294272 Color: 7
Size: 274465 Color: 4

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 431302 Color: 15
Size: 287267 Color: 17
Size: 281432 Color: 7

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 431421 Color: 12
Size: 303801 Color: 14
Size: 264779 Color: 8

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 431420 Color: 13
Size: 318154 Color: 14
Size: 250427 Color: 18

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 431683 Color: 15
Size: 306953 Color: 18
Size: 261365 Color: 18

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 431695 Color: 0
Size: 287550 Color: 19
Size: 280756 Color: 12

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 432113 Color: 5
Size: 301412 Color: 4
Size: 266476 Color: 17

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 432203 Color: 17
Size: 302281 Color: 12
Size: 265517 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 432639 Color: 12
Size: 283812 Color: 12
Size: 283550 Color: 15

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 432823 Color: 15
Size: 301106 Color: 1
Size: 266072 Color: 13

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 432844 Color: 17
Size: 316812 Color: 5
Size: 250345 Color: 7

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 433116 Color: 15
Size: 289160 Color: 14
Size: 277725 Color: 14

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 433370 Color: 10
Size: 294319 Color: 3
Size: 272312 Color: 8

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 433803 Color: 15
Size: 304090 Color: 1
Size: 262108 Color: 14

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 434088 Color: 11
Size: 301200 Color: 16
Size: 264713 Color: 14

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 435142 Color: 8
Size: 299025 Color: 8
Size: 265834 Color: 16

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 435708 Color: 5
Size: 284206 Color: 11
Size: 280087 Color: 4

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 435952 Color: 1
Size: 312581 Color: 15
Size: 251468 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 436003 Color: 2
Size: 293268 Color: 0
Size: 270730 Color: 16

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 436438 Color: 11
Size: 295234 Color: 10
Size: 268329 Color: 8

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 437163 Color: 5
Size: 307304 Color: 6
Size: 255534 Color: 6

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 437397 Color: 6
Size: 297663 Color: 3
Size: 264941 Color: 6

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 438519 Color: 7
Size: 290305 Color: 2
Size: 271177 Color: 3

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 438893 Color: 4
Size: 292827 Color: 14
Size: 268281 Color: 9

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 439001 Color: 17
Size: 292790 Color: 13
Size: 268210 Color: 12

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 439058 Color: 7
Size: 300303 Color: 17
Size: 260640 Color: 17

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 439259 Color: 11
Size: 310431 Color: 8
Size: 250311 Color: 7

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 439421 Color: 3
Size: 308470 Color: 19
Size: 252110 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 439722 Color: 15
Size: 308065 Color: 9
Size: 252214 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 439596 Color: 0
Size: 283735 Color: 11
Size: 276670 Color: 17

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 440055 Color: 0
Size: 301864 Color: 4
Size: 258082 Color: 13

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 440154 Color: 7
Size: 286955 Color: 0
Size: 272892 Color: 19

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 440192 Color: 8
Size: 293697 Color: 1
Size: 266112 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 440344 Color: 3
Size: 292111 Color: 19
Size: 267546 Color: 7

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 441216 Color: 13
Size: 285264 Color: 12
Size: 273521 Color: 12

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 440805 Color: 1
Size: 293162 Color: 12
Size: 266034 Color: 9

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 440897 Color: 15
Size: 304956 Color: 14
Size: 254148 Color: 8

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 441161 Color: 10
Size: 280367 Color: 5
Size: 278473 Color: 8

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 441987 Color: 3
Size: 285039 Color: 6
Size: 272975 Color: 18

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 442137 Color: 10
Size: 282725 Color: 15
Size: 275139 Color: 18

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 442718 Color: 19
Size: 286401 Color: 8
Size: 270882 Color: 16

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 442770 Color: 5
Size: 289072 Color: 5
Size: 268159 Color: 17

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 443272 Color: 10
Size: 302294 Color: 18
Size: 254435 Color: 18

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 444081 Color: 16
Size: 280742 Color: 10
Size: 275178 Color: 7

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 444413 Color: 12
Size: 302070 Color: 7
Size: 253518 Color: 16

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 444919 Color: 15
Size: 301715 Color: 4
Size: 253367 Color: 5

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 444948 Color: 18
Size: 283211 Color: 0
Size: 271842 Color: 14

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 445210 Color: 11
Size: 297226 Color: 19
Size: 257565 Color: 2

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 445284 Color: 5
Size: 295853 Color: 12
Size: 258864 Color: 12

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 445400 Color: 7
Size: 299276 Color: 1
Size: 255325 Color: 18

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 446937 Color: 13
Size: 281052 Color: 4
Size: 272012 Color: 16

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 446572 Color: 3
Size: 295463 Color: 8
Size: 257966 Color: 3

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 446590 Color: 4
Size: 279789 Color: 3
Size: 273622 Color: 9

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 446957 Color: 11
Size: 291182 Color: 7
Size: 261862 Color: 10

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 448000 Color: 3
Size: 301359 Color: 12
Size: 250642 Color: 8

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 448128 Color: 3
Size: 292204 Color: 12
Size: 259669 Color: 10

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 448185 Color: 14
Size: 288259 Color: 18
Size: 263557 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 448258 Color: 12
Size: 301625 Color: 9
Size: 250118 Color: 4

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 448296 Color: 3
Size: 290081 Color: 9
Size: 261624 Color: 10

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 448422 Color: 16
Size: 299337 Color: 8
Size: 252242 Color: 13

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 448637 Color: 7
Size: 296593 Color: 9
Size: 254771 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 448707 Color: 0
Size: 296192 Color: 10
Size: 255102 Color: 14

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 448868 Color: 2
Size: 279281 Color: 0
Size: 271852 Color: 8

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 449196 Color: 1
Size: 279382 Color: 12
Size: 271423 Color: 17

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 449797 Color: 13
Size: 291028 Color: 9
Size: 259176 Color: 6

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 450299 Color: 15
Size: 298899 Color: 16
Size: 250803 Color: 11

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 450531 Color: 2
Size: 294425 Color: 14
Size: 255045 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 450684 Color: 11
Size: 283778 Color: 8
Size: 265539 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 451090 Color: 0
Size: 298248 Color: 17
Size: 250663 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 451384 Color: 0
Size: 287897 Color: 3
Size: 260720 Color: 15

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 451492 Color: 3
Size: 278273 Color: 13
Size: 270236 Color: 5

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 451837 Color: 8
Size: 297953 Color: 15
Size: 250211 Color: 6

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 452010 Color: 7
Size: 281777 Color: 1
Size: 266214 Color: 6

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 452071 Color: 8
Size: 277044 Color: 3
Size: 270886 Color: 6

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 452399 Color: 14
Size: 296327 Color: 12
Size: 251275 Color: 4

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 452454 Color: 17
Size: 285153 Color: 14
Size: 262394 Color: 8

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 453015 Color: 18
Size: 289993 Color: 16
Size: 256993 Color: 9

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 453150 Color: 9
Size: 292462 Color: 16
Size: 254389 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 453356 Color: 5
Size: 295951 Color: 19
Size: 250694 Color: 2

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 453958 Color: 17
Size: 281341 Color: 11
Size: 264702 Color: 7

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 455241 Color: 1
Size: 280216 Color: 19
Size: 264544 Color: 16

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 455629 Color: 6
Size: 293245 Color: 6
Size: 251127 Color: 10

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 455633 Color: 2
Size: 285744 Color: 19
Size: 258624 Color: 4

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 455794 Color: 6
Size: 283217 Color: 11
Size: 260990 Color: 8

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 456058 Color: 13
Size: 277132 Color: 15
Size: 266811 Color: 9

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 456094 Color: 4
Size: 273412 Color: 12
Size: 270495 Color: 18

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 456458 Color: 4
Size: 276056 Color: 2
Size: 267487 Color: 3

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 456625 Color: 1
Size: 284292 Color: 7
Size: 259084 Color: 6

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 456907 Color: 0
Size: 284675 Color: 17
Size: 258419 Color: 6

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 457288 Color: 18
Size: 279019 Color: 10
Size: 263694 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 457690 Color: 2
Size: 279981 Color: 14
Size: 262330 Color: 18

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 458253 Color: 3
Size: 282378 Color: 4
Size: 259370 Color: 18

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 458318 Color: 16
Size: 276546 Color: 9
Size: 265137 Color: 11

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 458439 Color: 14
Size: 271683 Color: 17
Size: 269879 Color: 19

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 458638 Color: 9
Size: 283341 Color: 5
Size: 258022 Color: 7

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 458723 Color: 18
Size: 277877 Color: 8
Size: 263401 Color: 8

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 458842 Color: 2
Size: 277428 Color: 5
Size: 263731 Color: 18

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 459105 Color: 4
Size: 278744 Color: 0
Size: 262152 Color: 9

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 459282 Color: 16
Size: 289938 Color: 13
Size: 250781 Color: 9

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 459521 Color: 11
Size: 287337 Color: 1
Size: 253143 Color: 8

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 459621 Color: 19
Size: 286141 Color: 4
Size: 254239 Color: 18

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 460498 Color: 3
Size: 277492 Color: 5
Size: 262011 Color: 17

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 460908 Color: 17
Size: 273083 Color: 8
Size: 266010 Color: 10

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 460911 Color: 18
Size: 285752 Color: 11
Size: 253338 Color: 6

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 461793 Color: 19
Size: 275751 Color: 14
Size: 262457 Color: 19

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 461808 Color: 10
Size: 285211 Color: 7
Size: 252982 Color: 4

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 461858 Color: 1
Size: 271227 Color: 7
Size: 266916 Color: 6

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 462005 Color: 18
Size: 287473 Color: 18
Size: 250523 Color: 19

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 463209 Color: 9
Size: 271704 Color: 12
Size: 265088 Color: 16

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 463623 Color: 2
Size: 282355 Color: 6
Size: 254023 Color: 5

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 463636 Color: 14
Size: 272813 Color: 12
Size: 263552 Color: 18

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 463730 Color: 18
Size: 274060 Color: 14
Size: 262211 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 463839 Color: 15
Size: 284252 Color: 17
Size: 251910 Color: 3

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 463854 Color: 0
Size: 269706 Color: 12
Size: 266441 Color: 10

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 464020 Color: 7
Size: 278414 Color: 18
Size: 257567 Color: 14

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 464192 Color: 2
Size: 278181 Color: 16
Size: 257628 Color: 19

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 464213 Color: 9
Size: 285508 Color: 2
Size: 250280 Color: 8

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 464538 Color: 10
Size: 282293 Color: 4
Size: 253170 Color: 13

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 465496 Color: 4
Size: 283119 Color: 18
Size: 251386 Color: 3

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 465552 Color: 3
Size: 270363 Color: 3
Size: 264086 Color: 7

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 466068 Color: 4
Size: 270748 Color: 6
Size: 263185 Color: 14

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 466163 Color: 2
Size: 280701 Color: 14
Size: 253137 Color: 8

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 466321 Color: 16
Size: 279229 Color: 9
Size: 254451 Color: 5

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 467011 Color: 15
Size: 269716 Color: 8
Size: 263274 Color: 12

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 467422 Color: 13
Size: 272660 Color: 5
Size: 259919 Color: 12

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 467774 Color: 15
Size: 276065 Color: 10
Size: 256162 Color: 16

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 467848 Color: 11
Size: 272136 Color: 2
Size: 260017 Color: 11

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 467941 Color: 13
Size: 279764 Color: 3
Size: 252296 Color: 8

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 468885 Color: 2
Size: 268054 Color: 7
Size: 263062 Color: 3

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 468949 Color: 7
Size: 275561 Color: 0
Size: 255491 Color: 11

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 468976 Color: 16
Size: 280258 Color: 15
Size: 250767 Color: 11

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 469069 Color: 17
Size: 275504 Color: 8
Size: 255428 Color: 13

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 469068 Color: 18
Size: 274469 Color: 4
Size: 256464 Color: 8

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 469071 Color: 16
Size: 268257 Color: 16
Size: 262673 Color: 19

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 469426 Color: 7
Size: 273148 Color: 5
Size: 257427 Color: 15

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 470380 Color: 6
Size: 267454 Color: 19
Size: 262167 Color: 4

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 470423 Color: 7
Size: 278513 Color: 19
Size: 251065 Color: 15

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 471547 Color: 6
Size: 271188 Color: 0
Size: 257266 Color: 17

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 471736 Color: 17
Size: 277898 Color: 2
Size: 250367 Color: 9

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 471965 Color: 12
Size: 269118 Color: 4
Size: 258918 Color: 3

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 472192 Color: 3
Size: 269061 Color: 19
Size: 258748 Color: 11

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 473426 Color: 6
Size: 266598 Color: 14
Size: 259977 Color: 3

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 473509 Color: 19
Size: 272074 Color: 19
Size: 254418 Color: 10

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 473571 Color: 2
Size: 275095 Color: 0
Size: 251335 Color: 16

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 473743 Color: 0
Size: 271234 Color: 4
Size: 255024 Color: 5

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 473878 Color: 1
Size: 274783 Color: 6
Size: 251340 Color: 15

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 474043 Color: 0
Size: 274404 Color: 1
Size: 251554 Color: 18

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 474416 Color: 12
Size: 269483 Color: 2
Size: 256102 Color: 8

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 474543 Color: 17
Size: 272408 Color: 4
Size: 253050 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 475874 Color: 14
Size: 269632 Color: 4
Size: 254495 Color: 5

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 476326 Color: 17
Size: 266845 Color: 7
Size: 256830 Color: 19

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 476353 Color: 1
Size: 273170 Color: 17
Size: 250478 Color: 16

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 478458 Color: 18
Size: 263041 Color: 8
Size: 258502 Color: 9

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 479872 Color: 2
Size: 267805 Color: 0
Size: 252324 Color: 8

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 480757 Color: 12
Size: 268178 Color: 17
Size: 251066 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 480819 Color: 8
Size: 259939 Color: 14
Size: 259243 Color: 11

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 480943 Color: 0
Size: 260211 Color: 19
Size: 258847 Color: 9

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 481911 Color: 9
Size: 266878 Color: 10
Size: 251212 Color: 7

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 482062 Color: 8
Size: 267881 Color: 7
Size: 250058 Color: 15

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 482281 Color: 11
Size: 264732 Color: 1
Size: 252988 Color: 4

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 482294 Color: 1
Size: 262449 Color: 6
Size: 255258 Color: 16

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 482552 Color: 5
Size: 264768 Color: 14
Size: 252681 Color: 17

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 482676 Color: 3
Size: 260383 Color: 14
Size: 256942 Color: 11

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 482712 Color: 8
Size: 262629 Color: 11
Size: 254660 Color: 9

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 482944 Color: 0
Size: 260122 Color: 9
Size: 256935 Color: 8

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 483283 Color: 6
Size: 263299 Color: 12
Size: 253419 Color: 11

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 483876 Color: 14
Size: 264306 Color: 4
Size: 251819 Color: 16

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 483895 Color: 3
Size: 258491 Color: 13
Size: 257615 Color: 14

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 484805 Color: 18
Size: 261409 Color: 0
Size: 253787 Color: 15

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 485495 Color: 8
Size: 258480 Color: 11
Size: 256026 Color: 7

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 486215 Color: 3
Size: 263127 Color: 19
Size: 250659 Color: 11

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 486968 Color: 6
Size: 260377 Color: 7
Size: 252656 Color: 4

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 487893 Color: 17
Size: 256210 Color: 15
Size: 255898 Color: 9

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 488069 Color: 19
Size: 261006 Color: 5
Size: 250926 Color: 3

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 488241 Color: 10
Size: 256743 Color: 0
Size: 255017 Color: 3

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 489103 Color: 6
Size: 256026 Color: 13
Size: 254872 Color: 7

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 489910 Color: 15
Size: 256800 Color: 7
Size: 253291 Color: 9

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 490433 Color: 16
Size: 255322 Color: 7
Size: 254246 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 491168 Color: 16
Size: 258443 Color: 0
Size: 250390 Color: 9

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 491602 Color: 14
Size: 254604 Color: 2
Size: 253795 Color: 19

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 491950 Color: 0
Size: 257268 Color: 5
Size: 250783 Color: 9

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 492160 Color: 5
Size: 256641 Color: 0
Size: 251200 Color: 5

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 493075 Color: 17
Size: 255549 Color: 3
Size: 251377 Color: 13

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 494545 Color: 18
Size: 253902 Color: 16
Size: 251554 Color: 8

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 494980 Color: 13
Size: 252783 Color: 15
Size: 252238 Color: 5

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 495372 Color: 18
Size: 254378 Color: 7
Size: 250251 Color: 18

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 495526 Color: 2
Size: 254354 Color: 15
Size: 250121 Color: 16

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 495531 Color: 9
Size: 252910 Color: 3
Size: 251560 Color: 18

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 495721 Color: 17
Size: 252651 Color: 3
Size: 251629 Color: 2

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 495541 Color: 15
Size: 252731 Color: 10
Size: 251729 Color: 11

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 495898 Color: 5
Size: 253788 Color: 15
Size: 250315 Color: 10

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 496092 Color: 16
Size: 252517 Color: 5
Size: 251392 Color: 12

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 496228 Color: 7
Size: 253030 Color: 3
Size: 250743 Color: 19

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 496507 Color: 18
Size: 252654 Color: 4
Size: 250840 Color: 2

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 496841 Color: 14
Size: 251891 Color: 5
Size: 251269 Color: 18

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 352007 Color: 9
Size: 331407 Color: 1
Size: 316586 Color: 13

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 359949 Color: 16
Size: 330048 Color: 13
Size: 310003 Color: 16

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 360070 Color: 6
Size: 350902 Color: 13
Size: 289028 Color: 11

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 360698 Color: 16
Size: 326922 Color: 18
Size: 312380 Color: 1

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 362918 Color: 15
Size: 359418 Color: 0
Size: 277664 Color: 17

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 363796 Color: 1
Size: 349340 Color: 8
Size: 286864 Color: 18

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 365695 Color: 0
Size: 325032 Color: 8
Size: 309273 Color: 2

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 370237 Color: 3
Size: 326434 Color: 6
Size: 303329 Color: 9

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 370882 Color: 3
Size: 343257 Color: 5
Size: 285861 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 371214 Color: 4
Size: 326257 Color: 12
Size: 302529 Color: 2

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 371465 Color: 0
Size: 327943 Color: 16
Size: 300592 Color: 17

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 372195 Color: 15
Size: 324899 Color: 16
Size: 302906 Color: 19

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 374322 Color: 5
Size: 371554 Color: 13
Size: 254124 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 374388 Color: 4
Size: 365074 Color: 3
Size: 260538 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 375594 Color: 2
Size: 365026 Color: 17
Size: 259380 Color: 18

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 381005 Color: 0
Size: 335524 Color: 3
Size: 283471 Color: 11

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 381456 Color: 17
Size: 359421 Color: 11
Size: 259123 Color: 19

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 381886 Color: 18
Size: 354444 Color: 3
Size: 263670 Color: 13

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 382037 Color: 4
Size: 364434 Color: 10
Size: 253529 Color: 18

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 382280 Color: 3
Size: 352821 Color: 8
Size: 264899 Color: 16

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 385501 Color: 12
Size: 337029 Color: 6
Size: 277470 Color: 3

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 386317 Color: 5
Size: 310530 Color: 14
Size: 303153 Color: 6

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 387280 Color: 14
Size: 346696 Color: 5
Size: 266024 Color: 2

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 387530 Color: 12
Size: 311875 Color: 16
Size: 300595 Color: 6

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 393906 Color: 11
Size: 354711 Color: 16
Size: 251383 Color: 5

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 393457 Color: 15
Size: 338484 Color: 17
Size: 268059 Color: 19

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 395292 Color: 14
Size: 340983 Color: 16
Size: 263725 Color: 2

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 395932 Color: 5
Size: 303924 Color: 16
Size: 300144 Color: 10

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 402190 Color: 15
Size: 340143 Color: 14
Size: 257667 Color: 3

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 402592 Color: 8
Size: 299353 Color: 7
Size: 298055 Color: 2

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 403092 Color: 16
Size: 332465 Color: 11
Size: 264443 Color: 0

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 403826 Color: 11
Size: 323624 Color: 0
Size: 272550 Color: 19

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 404710 Color: 4
Size: 311756 Color: 5
Size: 283534 Color: 1

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 406783 Color: 1
Size: 310443 Color: 18
Size: 282774 Color: 4

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 406818 Color: 17
Size: 319420 Color: 16
Size: 273762 Color: 5

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 407536 Color: 7
Size: 309319 Color: 4
Size: 283145 Color: 0

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 409413 Color: 12
Size: 329947 Color: 5
Size: 260640 Color: 17

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 412134 Color: 17
Size: 306853 Color: 3
Size: 281013 Color: 5

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 421989 Color: 9
Size: 308243 Color: 13
Size: 269768 Color: 19

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 422877 Color: 9
Size: 320716 Color: 0
Size: 256407 Color: 10

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 423670 Color: 0
Size: 291322 Color: 19
Size: 285008 Color: 4

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 424777 Color: 13
Size: 296818 Color: 10
Size: 278405 Color: 11

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 425431 Color: 18
Size: 320544 Color: 13
Size: 254025 Color: 16

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 425636 Color: 12
Size: 296036 Color: 12
Size: 278328 Color: 3

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 427952 Color: 17
Size: 319764 Color: 11
Size: 252284 Color: 5

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 429885 Color: 0
Size: 288681 Color: 14
Size: 281434 Color: 12

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 438307 Color: 10
Size: 289930 Color: 17
Size: 271763 Color: 18

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 443948 Color: 17
Size: 286838 Color: 13
Size: 269214 Color: 14

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 444142 Color: 18
Size: 297065 Color: 6
Size: 258793 Color: 7

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 447691 Color: 16
Size: 296198 Color: 13
Size: 256111 Color: 6

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 457899 Color: 18
Size: 281484 Color: 3
Size: 260617 Color: 8

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 458623 Color: 0
Size: 271691 Color: 16
Size: 269686 Color: 15

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 459109 Color: 17
Size: 281289 Color: 15
Size: 259602 Color: 7

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 459655 Color: 9
Size: 280828 Color: 18
Size: 259517 Color: 11

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 467223 Color: 14
Size: 267806 Color: 2
Size: 264971 Color: 18

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 467810 Color: 10
Size: 279157 Color: 1
Size: 253033 Color: 13

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 468366 Color: 1
Size: 276880 Color: 8
Size: 254754 Color: 18

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 469781 Color: 2
Size: 279865 Color: 6
Size: 250354 Color: 9

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 367874 Color: 19
Size: 331677 Color: 10
Size: 300448 Color: 15

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 368845 Color: 18
Size: 351716 Color: 13
Size: 279438 Color: 9

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 369330 Color: 13
Size: 324093 Color: 3
Size: 306576 Color: 0

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 372301 Color: 6
Size: 333793 Color: 1
Size: 293905 Color: 16

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 377834 Color: 1
Size: 324611 Color: 16
Size: 297554 Color: 14

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 379209 Color: 16
Size: 316619 Color: 2
Size: 304171 Color: 12

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 380322 Color: 10
Size: 321649 Color: 0
Size: 298028 Color: 7

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 385599 Color: 17
Size: 326778 Color: 13
Size: 287622 Color: 19

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 392983 Color: 15
Size: 317577 Color: 3
Size: 289439 Color: 6

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 393495 Color: 1
Size: 322376 Color: 2
Size: 284128 Color: 1

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 401843 Color: 16
Size: 308958 Color: 3
Size: 289198 Color: 19

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 403262 Color: 0
Size: 333958 Color: 9
Size: 262779 Color: 11

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 446519 Color: 13
Size: 290222 Color: 15
Size: 263258 Color: 1

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 452810 Color: 0
Size: 276553 Color: 8
Size: 270636 Color: 15

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 454568 Color: 2
Size: 283557 Color: 17
Size: 261874 Color: 7

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 462143 Color: 0
Size: 286646 Color: 17
Size: 251210 Color: 13

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 480965 Color: 17
Size: 268535 Color: 12
Size: 250499 Color: 18

Bin 490: 3 of cap free
Amount of items: 3
Items: 
Size: 359369 Color: 2
Size: 339529 Color: 7
Size: 301100 Color: 12

Bin 491: 3 of cap free
Amount of items: 3
Items: 
Size: 365224 Color: 7
Size: 340702 Color: 19
Size: 294072 Color: 2

Bin 492: 3 of cap free
Amount of items: 3
Items: 
Size: 366482 Color: 2
Size: 317345 Color: 17
Size: 316171 Color: 11

Bin 493: 3 of cap free
Amount of items: 3
Items: 
Size: 370009 Color: 16
Size: 359752 Color: 14
Size: 270237 Color: 6

Bin 494: 3 of cap free
Amount of items: 3
Items: 
Size: 371025 Color: 15
Size: 350916 Color: 3
Size: 278057 Color: 14

Bin 495: 3 of cap free
Amount of items: 3
Items: 
Size: 371086 Color: 2
Size: 333336 Color: 7
Size: 295576 Color: 10

Bin 496: 3 of cap free
Amount of items: 3
Items: 
Size: 371742 Color: 1
Size: 320522 Color: 6
Size: 307734 Color: 18

Bin 497: 3 of cap free
Amount of items: 3
Items: 
Size: 372326 Color: 7
Size: 347681 Color: 16
Size: 279991 Color: 2

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 375905 Color: 5
Size: 317836 Color: 19
Size: 306257 Color: 0

Bin 499: 3 of cap free
Amount of items: 3
Items: 
Size: 377750 Color: 5
Size: 353574 Color: 15
Size: 268674 Color: 18

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 377408 Color: 2
Size: 328076 Color: 18
Size: 294514 Color: 7

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 379988 Color: 5
Size: 329252 Color: 4
Size: 290758 Color: 16

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 381231 Color: 14
Size: 334616 Color: 5
Size: 284151 Color: 9

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 382184 Color: 18
Size: 357704 Color: 15
Size: 260110 Color: 17

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 383666 Color: 0
Size: 360496 Color: 15
Size: 255836 Color: 15

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 383691 Color: 11
Size: 314213 Color: 8
Size: 302094 Color: 10

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 391414 Color: 2
Size: 339373 Color: 7
Size: 269211 Color: 18

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 400703 Color: 3
Size: 336393 Color: 3
Size: 262902 Color: 7

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 402084 Color: 14
Size: 317866 Color: 19
Size: 280048 Color: 7

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 417301 Color: 10
Size: 311858 Color: 13
Size: 270839 Color: 10

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 436270 Color: 9
Size: 302631 Color: 14
Size: 261097 Color: 15

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 438635 Color: 12
Size: 309781 Color: 15
Size: 251582 Color: 0

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 441229 Color: 4
Size: 282248 Color: 13
Size: 276521 Color: 4

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 445084 Color: 18
Size: 284749 Color: 9
Size: 270165 Color: 17

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 473502 Color: 13
Size: 269693 Color: 18
Size: 256803 Color: 16

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 488043 Color: 1
Size: 260895 Color: 17
Size: 251060 Color: 4

Bin 516: 4 of cap free
Amount of items: 3
Items: 
Size: 362194 Color: 17
Size: 319374 Color: 8
Size: 318429 Color: 1

Bin 517: 4 of cap free
Amount of items: 3
Items: 
Size: 366315 Color: 0
Size: 340462 Color: 10
Size: 293220 Color: 13

Bin 518: 4 of cap free
Amount of items: 3
Items: 
Size: 365322 Color: 18
Size: 356487 Color: 11
Size: 278188 Color: 8

Bin 519: 4 of cap free
Amount of items: 3
Items: 
Size: 366694 Color: 17
Size: 318354 Color: 10
Size: 314949 Color: 13

Bin 520: 4 of cap free
Amount of items: 3
Items: 
Size: 373566 Color: 17
Size: 337862 Color: 19
Size: 288569 Color: 18

Bin 521: 4 of cap free
Amount of items: 3
Items: 
Size: 373694 Color: 1
Size: 353921 Color: 16
Size: 272382 Color: 10

Bin 522: 4 of cap free
Amount of items: 3
Items: 
Size: 374372 Color: 3
Size: 329036 Color: 5
Size: 296589 Color: 10

Bin 523: 4 of cap free
Amount of items: 3
Items: 
Size: 381552 Color: 1
Size: 350029 Color: 5
Size: 268416 Color: 19

Bin 524: 4 of cap free
Amount of items: 3
Items: 
Size: 381618 Color: 6
Size: 349250 Color: 2
Size: 269129 Color: 0

Bin 525: 4 of cap free
Amount of items: 3
Items: 
Size: 399316 Color: 13
Size: 317824 Color: 19
Size: 282857 Color: 2

Bin 526: 4 of cap free
Amount of items: 3
Items: 
Size: 402117 Color: 1
Size: 308797 Color: 13
Size: 289083 Color: 5

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 409658 Color: 16
Size: 301564 Color: 10
Size: 288775 Color: 10

Bin 528: 5 of cap free
Amount of items: 3
Items: 
Size: 361532 Color: 0
Size: 353975 Color: 11
Size: 284489 Color: 19

Bin 529: 5 of cap free
Amount of items: 3
Items: 
Size: 363289 Color: 5
Size: 349679 Color: 2
Size: 287028 Color: 11

Bin 530: 5 of cap free
Amount of items: 3
Items: 
Size: 366766 Color: 2
Size: 322468 Color: 15
Size: 310762 Color: 3

Bin 531: 5 of cap free
Amount of items: 3
Items: 
Size: 370155 Color: 19
Size: 353758 Color: 9
Size: 276083 Color: 15

Bin 532: 5 of cap free
Amount of items: 3
Items: 
Size: 375382 Color: 7
Size: 363256 Color: 4
Size: 261358 Color: 15

Bin 533: 5 of cap free
Amount of items: 3
Items: 
Size: 381086 Color: 10
Size: 363680 Color: 3
Size: 255230 Color: 4

Bin 534: 5 of cap free
Amount of items: 3
Items: 
Size: 381224 Color: 6
Size: 351911 Color: 1
Size: 266861 Color: 2

Bin 535: 5 of cap free
Amount of items: 3
Items: 
Size: 392608 Color: 14
Size: 336119 Color: 3
Size: 271269 Color: 16

Bin 536: 5 of cap free
Amount of items: 3
Items: 
Size: 430556 Color: 18
Size: 304191 Color: 7
Size: 265249 Color: 13

Bin 537: 6 of cap free
Amount of items: 3
Items: 
Size: 366440 Color: 14
Size: 320947 Color: 11
Size: 312608 Color: 18

Bin 538: 6 of cap free
Amount of items: 3
Items: 
Size: 369317 Color: 0
Size: 316303 Color: 5
Size: 314375 Color: 6

Bin 539: 6 of cap free
Amount of items: 3
Items: 
Size: 370117 Color: 13
Size: 341431 Color: 15
Size: 288447 Color: 11

Bin 540: 6 of cap free
Amount of items: 3
Items: 
Size: 374329 Color: 1
Size: 365073 Color: 15
Size: 260593 Color: 9

Bin 541: 6 of cap free
Amount of items: 3
Items: 
Size: 394327 Color: 3
Size: 332985 Color: 0
Size: 272683 Color: 6

Bin 542: 6 of cap free
Amount of items: 3
Items: 
Size: 425848 Color: 19
Size: 301987 Color: 13
Size: 272160 Color: 14

Bin 543: 6 of cap free
Amount of items: 3
Items: 
Size: 441068 Color: 13
Size: 298851 Color: 17
Size: 260076 Color: 16

Bin 544: 6 of cap free
Amount of items: 3
Items: 
Size: 447416 Color: 17
Size: 292380 Color: 5
Size: 260199 Color: 15

Bin 545: 6 of cap free
Amount of items: 3
Items: 
Size: 457456 Color: 14
Size: 291315 Color: 15
Size: 251224 Color: 4

Bin 546: 7 of cap free
Amount of items: 3
Items: 
Size: 363231 Color: 7
Size: 321544 Color: 3
Size: 315219 Color: 19

Bin 547: 7 of cap free
Amount of items: 3
Items: 
Size: 375169 Color: 13
Size: 324164 Color: 5
Size: 300661 Color: 16

Bin 548: 7 of cap free
Amount of items: 3
Items: 
Size: 383315 Color: 9
Size: 328183 Color: 2
Size: 288496 Color: 0

Bin 549: 7 of cap free
Amount of items: 3
Items: 
Size: 384506 Color: 2
Size: 352664 Color: 5
Size: 262824 Color: 14

Bin 550: 7 of cap free
Amount of items: 3
Items: 
Size: 391227 Color: 14
Size: 347467 Color: 6
Size: 261300 Color: 8

Bin 551: 7 of cap free
Amount of items: 3
Items: 
Size: 427404 Color: 12
Size: 297862 Color: 6
Size: 274728 Color: 13

Bin 552: 8 of cap free
Amount of items: 3
Items: 
Size: 361200 Color: 13
Size: 358125 Color: 3
Size: 280668 Color: 0

Bin 553: 8 of cap free
Amount of items: 3
Items: 
Size: 422636 Color: 4
Size: 313595 Color: 13
Size: 263762 Color: 12

Bin 554: 8 of cap free
Amount of items: 3
Items: 
Size: 455228 Color: 14
Size: 278770 Color: 15
Size: 265995 Color: 3

Bin 555: 8 of cap free
Amount of items: 3
Items: 
Size: 493554 Color: 2
Size: 253433 Color: 17
Size: 253006 Color: 16

Bin 556: 9 of cap free
Amount of items: 3
Items: 
Size: 388516 Color: 14
Size: 323039 Color: 9
Size: 288437 Color: 5

Bin 557: 9 of cap free
Amount of items: 3
Items: 
Size: 445648 Color: 19
Size: 289658 Color: 15
Size: 264686 Color: 17

Bin 558: 9 of cap free
Amount of items: 3
Items: 
Size: 470500 Color: 13
Size: 276255 Color: 16
Size: 253237 Color: 18

Bin 559: 9 of cap free
Amount of items: 3
Items: 
Size: 481213 Color: 10
Size: 261511 Color: 6
Size: 257268 Color: 17

Bin 560: 10 of cap free
Amount of items: 3
Items: 
Size: 357604 Color: 2
Size: 321534 Color: 8
Size: 320853 Color: 6

Bin 561: 10 of cap free
Amount of items: 3
Items: 
Size: 365434 Color: 12
Size: 322352 Color: 2
Size: 312205 Color: 18

Bin 562: 10 of cap free
Amount of items: 3
Items: 
Size: 370266 Color: 9
Size: 320266 Color: 13
Size: 309459 Color: 18

Bin 563: 10 of cap free
Amount of items: 3
Items: 
Size: 405953 Color: 17
Size: 344005 Color: 13
Size: 250033 Color: 10

Bin 564: 11 of cap free
Amount of items: 3
Items: 
Size: 346456 Color: 17
Size: 343087 Color: 12
Size: 310447 Color: 15

Bin 565: 11 of cap free
Amount of items: 3
Items: 
Size: 364127 Color: 2
Size: 349153 Color: 3
Size: 286710 Color: 12

Bin 566: 11 of cap free
Amount of items: 3
Items: 
Size: 368624 Color: 4
Size: 355330 Color: 7
Size: 276036 Color: 10

Bin 567: 11 of cap free
Amount of items: 3
Items: 
Size: 371020 Color: 12
Size: 339981 Color: 11
Size: 288989 Color: 17

Bin 568: 11 of cap free
Amount of items: 3
Items: 
Size: 384072 Color: 9
Size: 317681 Color: 15
Size: 298237 Color: 12

Bin 569: 11 of cap free
Amount of items: 3
Items: 
Size: 390217 Color: 8
Size: 359162 Color: 7
Size: 250611 Color: 13

Bin 570: 12 of cap free
Amount of items: 3
Items: 
Size: 364552 Color: 5
Size: 325734 Color: 6
Size: 309703 Color: 18

Bin 571: 12 of cap free
Amount of items: 3
Items: 
Size: 366027 Color: 2
Size: 332025 Color: 9
Size: 301937 Color: 10

Bin 572: 12 of cap free
Amount of items: 3
Items: 
Size: 367341 Color: 18
Size: 357113 Color: 8
Size: 275535 Color: 4

Bin 573: 12 of cap free
Amount of items: 3
Items: 
Size: 379726 Color: 7
Size: 354877 Color: 18
Size: 265386 Color: 16

Bin 574: 12 of cap free
Amount of items: 3
Items: 
Size: 383050 Color: 1
Size: 338059 Color: 13
Size: 278880 Color: 16

Bin 575: 12 of cap free
Amount of items: 3
Items: 
Size: 389828 Color: 4
Size: 321805 Color: 10
Size: 288356 Color: 5

Bin 576: 13 of cap free
Amount of items: 3
Items: 
Size: 369201 Color: 18
Size: 338213 Color: 19
Size: 292574 Color: 16

Bin 577: 14 of cap free
Amount of items: 3
Items: 
Size: 376627 Color: 4
Size: 320877 Color: 13
Size: 302483 Color: 19

Bin 578: 14 of cap free
Amount of items: 3
Items: 
Size: 407993 Color: 2
Size: 297054 Color: 13
Size: 294940 Color: 1

Bin 579: 14 of cap free
Amount of items: 3
Items: 
Size: 420999 Color: 19
Size: 298889 Color: 13
Size: 280099 Color: 16

Bin 580: 15 of cap free
Amount of items: 3
Items: 
Size: 358863 Color: 14
Size: 357536 Color: 18
Size: 283587 Color: 4

Bin 581: 15 of cap free
Amount of items: 3
Items: 
Size: 358938 Color: 19
Size: 354932 Color: 15
Size: 286116 Color: 19

Bin 582: 15 of cap free
Amount of items: 3
Items: 
Size: 360769 Color: 9
Size: 338419 Color: 6
Size: 300798 Color: 2

Bin 583: 15 of cap free
Amount of items: 3
Items: 
Size: 363795 Color: 10
Size: 333699 Color: 18
Size: 302492 Color: 5

Bin 584: 15 of cap free
Amount of items: 3
Items: 
Size: 381407 Color: 2
Size: 342848 Color: 14
Size: 275731 Color: 13

Bin 585: 15 of cap free
Amount of items: 3
Items: 
Size: 382988 Color: 2
Size: 314647 Color: 13
Size: 302351 Color: 4

Bin 586: 15 of cap free
Amount of items: 3
Items: 
Size: 389811 Color: 7
Size: 352955 Color: 8
Size: 257220 Color: 6

Bin 587: 15 of cap free
Amount of items: 3
Items: 
Size: 403834 Color: 9
Size: 329996 Color: 13
Size: 266156 Color: 6

Bin 588: 15 of cap free
Amount of items: 3
Items: 
Size: 434853 Color: 3
Size: 312339 Color: 13
Size: 252794 Color: 0

Bin 589: 16 of cap free
Amount of items: 3
Items: 
Size: 380061 Color: 10
Size: 310092 Color: 6
Size: 309832 Color: 13

Bin 590: 17 of cap free
Amount of items: 3
Items: 
Size: 384705 Color: 17
Size: 324690 Color: 6
Size: 290589 Color: 2

Bin 591: 18 of cap free
Amount of items: 3
Items: 
Size: 355358 Color: 17
Size: 330536 Color: 3
Size: 314089 Color: 15

Bin 592: 18 of cap free
Amount of items: 3
Items: 
Size: 372207 Color: 7
Size: 341398 Color: 9
Size: 286378 Color: 5

Bin 593: 19 of cap free
Amount of items: 3
Items: 
Size: 365743 Color: 1
Size: 356470 Color: 8
Size: 277769 Color: 1

Bin 594: 19 of cap free
Amount of items: 3
Items: 
Size: 378590 Color: 11
Size: 315421 Color: 10
Size: 305971 Color: 5

Bin 595: 19 of cap free
Amount of items: 3
Items: 
Size: 384621 Color: 13
Size: 334241 Color: 15
Size: 281120 Color: 4

Bin 596: 19 of cap free
Amount of items: 3
Items: 
Size: 386637 Color: 6
Size: 323638 Color: 5
Size: 289707 Color: 4

Bin 597: 20 of cap free
Amount of items: 3
Items: 
Size: 364298 Color: 17
Size: 335916 Color: 4
Size: 299767 Color: 7

Bin 598: 23 of cap free
Amount of items: 3
Items: 
Size: 365183 Color: 9
Size: 320893 Color: 9
Size: 313902 Color: 15

Bin 599: 24 of cap free
Amount of items: 3
Items: 
Size: 394935 Color: 2
Size: 341765 Color: 5
Size: 263277 Color: 7

Bin 600: 25 of cap free
Amount of items: 3
Items: 
Size: 364215 Color: 5
Size: 343857 Color: 15
Size: 291904 Color: 13

Bin 601: 26 of cap free
Amount of items: 3
Items: 
Size: 353396 Color: 18
Size: 331850 Color: 0
Size: 314729 Color: 3

Bin 602: 26 of cap free
Amount of items: 3
Items: 
Size: 356011 Color: 11
Size: 333845 Color: 13
Size: 310119 Color: 19

Bin 603: 28 of cap free
Amount of items: 3
Items: 
Size: 366532 Color: 5
Size: 337584 Color: 13
Size: 295857 Color: 2

Bin 604: 29 of cap free
Amount of items: 3
Items: 
Size: 367412 Color: 5
Size: 327901 Color: 2
Size: 304659 Color: 0

Bin 605: 30 of cap free
Amount of items: 3
Items: 
Size: 396243 Color: 9
Size: 349166 Color: 2
Size: 254562 Color: 4

Bin 606: 31 of cap free
Amount of items: 3
Items: 
Size: 385771 Color: 7
Size: 326737 Color: 5
Size: 287462 Color: 15

Bin 607: 33 of cap free
Amount of items: 3
Items: 
Size: 363775 Color: 10
Size: 332145 Color: 16
Size: 304048 Color: 6

Bin 608: 35 of cap free
Amount of items: 3
Items: 
Size: 358183 Color: 10
Size: 340748 Color: 14
Size: 301035 Color: 12

Bin 609: 36 of cap free
Amount of items: 3
Items: 
Size: 374925 Color: 17
Size: 353982 Color: 10
Size: 271058 Color: 9

Bin 610: 37 of cap free
Amount of items: 3
Items: 
Size: 369702 Color: 5
Size: 321559 Color: 5
Size: 308703 Color: 17

Bin 611: 42 of cap free
Amount of items: 3
Items: 
Size: 365977 Color: 1
Size: 327424 Color: 2
Size: 306558 Color: 15

Bin 612: 45 of cap free
Amount of items: 3
Items: 
Size: 359492 Color: 16
Size: 350992 Color: 5
Size: 289472 Color: 1

Bin 613: 45 of cap free
Amount of items: 3
Items: 
Size: 361068 Color: 6
Size: 343478 Color: 2
Size: 295410 Color: 4

Bin 614: 46 of cap free
Amount of items: 3
Items: 
Size: 365650 Color: 8
Size: 340634 Color: 17
Size: 293671 Color: 6

Bin 615: 47 of cap free
Amount of items: 3
Items: 
Size: 356544 Color: 16
Size: 331350 Color: 4
Size: 312060 Color: 10

Bin 616: 52 of cap free
Amount of items: 3
Items: 
Size: 360639 Color: 4
Size: 357606 Color: 4
Size: 281704 Color: 5

Bin 617: 54 of cap free
Amount of items: 3
Items: 
Size: 371987 Color: 5
Size: 343175 Color: 13
Size: 284785 Color: 7

Bin 618: 60 of cap free
Amount of items: 3
Items: 
Size: 360601 Color: 9
Size: 334439 Color: 4
Size: 304901 Color: 9

Bin 619: 60 of cap free
Amount of items: 3
Items: 
Size: 360861 Color: 9
Size: 326475 Color: 15
Size: 312605 Color: 10

Bin 620: 64 of cap free
Amount of items: 3
Items: 
Size: 362187 Color: 17
Size: 350734 Color: 15
Size: 287016 Color: 6

Bin 621: 66 of cap free
Amount of items: 3
Items: 
Size: 363060 Color: 19
Size: 334624 Color: 9
Size: 302251 Color: 19

Bin 622: 67 of cap free
Amount of items: 3
Items: 
Size: 356543 Color: 18
Size: 334035 Color: 12
Size: 309356 Color: 16

Bin 623: 82 of cap free
Amount of items: 3
Items: 
Size: 370142 Color: 5
Size: 368942 Color: 18
Size: 260835 Color: 4

Bin 624: 84 of cap free
Amount of items: 3
Items: 
Size: 348731 Color: 18
Size: 333469 Color: 10
Size: 317717 Color: 15

Bin 625: 97 of cap free
Amount of items: 3
Items: 
Size: 356379 Color: 5
Size: 352456 Color: 5
Size: 291069 Color: 9

Bin 626: 106 of cap free
Amount of items: 3
Items: 
Size: 354135 Color: 2
Size: 339333 Color: 11
Size: 306427 Color: 3

Bin 627: 120 of cap free
Amount of items: 3
Items: 
Size: 352733 Color: 11
Size: 336675 Color: 7
Size: 310473 Color: 0

Bin 628: 124 of cap free
Amount of items: 3
Items: 
Size: 359972 Color: 12
Size: 344870 Color: 17
Size: 295035 Color: 7

Bin 629: 124 of cap free
Amount of items: 3
Items: 
Size: 363623 Color: 19
Size: 328620 Color: 13
Size: 307634 Color: 6

Bin 630: 137 of cap free
Amount of items: 3
Items: 
Size: 352801 Color: 13
Size: 345015 Color: 6
Size: 302048 Color: 14

Bin 631: 140 of cap free
Amount of items: 3
Items: 
Size: 366179 Color: 11
Size: 322683 Color: 6
Size: 310999 Color: 14

Bin 632: 145 of cap free
Amount of items: 3
Items: 
Size: 363014 Color: 3
Size: 327278 Color: 9
Size: 309564 Color: 12

Bin 633: 156 of cap free
Amount of items: 3
Items: 
Size: 357748 Color: 12
Size: 346143 Color: 8
Size: 295954 Color: 17

Bin 634: 171 of cap free
Amount of items: 3
Items: 
Size: 354953 Color: 15
Size: 339524 Color: 17
Size: 305353 Color: 2

Bin 635: 190 of cap free
Amount of items: 3
Items: 
Size: 359472 Color: 18
Size: 349226 Color: 15
Size: 291113 Color: 10

Bin 636: 192 of cap free
Amount of items: 3
Items: 
Size: 352716 Color: 1
Size: 330435 Color: 4
Size: 316658 Color: 11

Bin 637: 228 of cap free
Amount of items: 3
Items: 
Size: 351494 Color: 13
Size: 349624 Color: 14
Size: 298655 Color: 9

Bin 638: 239 of cap free
Amount of items: 3
Items: 
Size: 356118 Color: 11
Size: 336517 Color: 12
Size: 307127 Color: 8

Bin 639: 308 of cap free
Amount of items: 3
Items: 
Size: 358398 Color: 0
Size: 336974 Color: 8
Size: 304321 Color: 5

Bin 640: 390 of cap free
Amount of items: 3
Items: 
Size: 356277 Color: 7
Size: 337283 Color: 8
Size: 306051 Color: 15

Bin 641: 417 of cap free
Amount of items: 3
Items: 
Size: 358140 Color: 4
Size: 341428 Color: 13
Size: 300016 Color: 7

Bin 642: 481 of cap free
Amount of items: 3
Items: 
Size: 356257 Color: 1
Size: 328141 Color: 6
Size: 315122 Color: 9

Bin 643: 484 of cap free
Amount of items: 3
Items: 
Size: 350357 Color: 17
Size: 346437 Color: 18
Size: 302723 Color: 10

Bin 644: 682 of cap free
Amount of items: 3
Items: 
Size: 358324 Color: 19
Size: 330415 Color: 10
Size: 310580 Color: 1

Bin 645: 684 of cap free
Amount of items: 3
Items: 
Size: 354869 Color: 1
Size: 327864 Color: 12
Size: 316584 Color: 17

Bin 646: 933 of cap free
Amount of items: 3
Items: 
Size: 354529 Color: 12
Size: 325792 Color: 15
Size: 318747 Color: 16

Bin 647: 943 of cap free
Amount of items: 3
Items: 
Size: 356389 Color: 5
Size: 340627 Color: 1
Size: 302042 Color: 11

Bin 648: 1033 of cap free
Amount of items: 2
Items: 
Size: 499944 Color: 5
Size: 499024 Color: 0

Bin 649: 1177 of cap free
Amount of items: 3
Items: 
Size: 357038 Color: 19
Size: 344612 Color: 5
Size: 297174 Color: 7

Bin 650: 1551 of cap free
Amount of items: 3
Items: 
Size: 344729 Color: 17
Size: 333354 Color: 18
Size: 320367 Color: 15

Bin 651: 2276 of cap free
Amount of items: 3
Items: 
Size: 353463 Color: 10
Size: 337850 Color: 5
Size: 306412 Color: 13

Bin 652: 2844 of cap free
Amount of items: 2
Items: 
Size: 498851 Color: 4
Size: 498306 Color: 8

Bin 653: 2908 of cap free
Amount of items: 3
Items: 
Size: 346061 Color: 6
Size: 333885 Color: 8
Size: 317147 Color: 13

Bin 654: 4211 of cap free
Amount of items: 2
Items: 
Size: 498276 Color: 4
Size: 497514 Color: 8

Bin 655: 5007 of cap free
Amount of items: 3
Items: 
Size: 350929 Color: 12
Size: 331504 Color: 7
Size: 312561 Color: 0

Bin 656: 9555 of cap free
Amount of items: 3
Items: 
Size: 347640 Color: 14
Size: 321991 Color: 17
Size: 320815 Color: 3

Bin 657: 16380 of cap free
Amount of items: 3
Items: 
Size: 354416 Color: 10
Size: 353911 Color: 14
Size: 275294 Color: 0

Bin 658: 23015 of cap free
Amount of items: 3
Items: 
Size: 353361 Color: 4
Size: 348599 Color: 6
Size: 275026 Color: 16

Bin 659: 23734 of cap free
Amount of items: 3
Items: 
Size: 351763 Color: 16
Size: 351159 Color: 8
Size: 273345 Color: 14

Bin 660: 28896 of cap free
Amount of items: 3
Items: 
Size: 350734 Color: 8
Size: 346139 Color: 4
Size: 274232 Color: 5

Bin 661: 43780 of cap free
Amount of items: 3
Items: 
Size: 349469 Color: 1
Size: 343701 Color: 6
Size: 263051 Color: 9

Bin 662: 44109 of cap free
Amount of items: 3
Items: 
Size: 348258 Color: 18
Size: 340589 Color: 10
Size: 267045 Color: 14

Bin 663: 44158 of cap free
Amount of items: 3
Items: 
Size: 345882 Color: 19
Size: 342066 Color: 17
Size: 267895 Color: 6

Bin 664: 47786 of cap free
Amount of items: 3
Items: 
Size: 346134 Color: 13
Size: 346121 Color: 16
Size: 259960 Color: 9

Bin 665: 52784 of cap free
Amount of items: 3
Items: 
Size: 344983 Color: 11
Size: 344256 Color: 12
Size: 257978 Color: 4

Bin 666: 146183 of cap free
Amount of items: 3
Items: 
Size: 347250 Color: 2
Size: 256555 Color: 19
Size: 250013 Color: 5

Bin 667: 242387 of cap free
Amount of items: 3
Items: 
Size: 255001 Color: 14
Size: 251753 Color: 10
Size: 250860 Color: 7

Bin 668: 246579 of cap free
Amount of items: 3
Items: 
Size: 252178 Color: 15
Size: 250878 Color: 17
Size: 250366 Color: 18

Total size: 667000667
Total free space: 1000001

