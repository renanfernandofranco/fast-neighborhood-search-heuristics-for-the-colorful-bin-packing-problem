Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 442
Size: 6968 Color: 375
Size: 392 Color: 45

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 459
Size: 3280 Color: 291
Size: 2984 Color: 274

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 460
Size: 5200 Color: 345
Size: 1024 Color: 152

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13956 Color: 469
Size: 5428 Color: 353
Size: 264 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 14262 Color: 475
Size: 4490 Color: 328
Size: 896 Color: 141

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 14495 Color: 481
Size: 4471 Color: 327
Size: 682 Color: 105

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14688 Color: 486
Size: 4496 Color: 329
Size: 464 Color: 59

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 488
Size: 4192 Color: 321
Size: 592 Color: 87

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14872 Color: 489
Size: 3992 Color: 314
Size: 784 Color: 125

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14936 Color: 490
Size: 3928 Color: 313
Size: 784 Color: 126

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15000 Color: 492
Size: 4504 Color: 330
Size: 144 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15023 Color: 493
Size: 3855 Color: 310
Size: 770 Color: 123

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15216 Color: 498
Size: 3464 Color: 298
Size: 968 Color: 144

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15382 Color: 502
Size: 3260 Color: 290
Size: 1006 Color: 149

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15517 Color: 506
Size: 3443 Color: 297
Size: 688 Color: 107

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 507
Size: 3072 Color: 280
Size: 1056 Color: 158

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15527 Color: 508
Size: 3435 Color: 296
Size: 686 Color: 106

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15559 Color: 512
Size: 3409 Color: 292
Size: 680 Color: 104

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15560 Color: 513
Size: 2936 Color: 270
Size: 1152 Color: 164

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15728 Color: 514
Size: 2912 Color: 267
Size: 1008 Color: 150

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15907 Color: 517
Size: 3119 Color: 286
Size: 622 Color: 95

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15915 Color: 518
Size: 3085 Color: 282
Size: 648 Color: 100

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15990 Color: 523
Size: 2618 Color: 257
Size: 1040 Color: 156

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16008 Color: 524
Size: 2008 Color: 225
Size: 1632 Color: 198

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16072 Color: 526
Size: 3048 Color: 278
Size: 528 Color: 75

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16252 Color: 534
Size: 3036 Color: 277
Size: 360 Color: 32

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16258 Color: 535
Size: 2814 Color: 264
Size: 576 Color: 84

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16274 Color: 536
Size: 2922 Color: 269
Size: 452 Color: 57

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 537
Size: 2224 Color: 236
Size: 992 Color: 146

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16433 Color: 538
Size: 2681 Color: 261
Size: 534 Color: 77

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16436 Color: 539
Size: 1676 Color: 203
Size: 1536 Color: 191

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16441 Color: 540
Size: 2919 Color: 268
Size: 288 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16461 Color: 542
Size: 2571 Color: 254
Size: 616 Color: 92

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16478 Color: 543
Size: 2416 Color: 249
Size: 754 Color: 118

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16488 Color: 544
Size: 2092 Color: 231
Size: 1068 Color: 160

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16636 Color: 550
Size: 2516 Color: 252
Size: 496 Color: 68

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 553
Size: 1768 Color: 209
Size: 1104 Color: 162

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16777 Color: 554
Size: 2393 Color: 246
Size: 478 Color: 63

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16784 Color: 555
Size: 2000 Color: 224
Size: 864 Color: 135

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16785 Color: 556
Size: 2387 Color: 244
Size: 476 Color: 62

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16788 Color: 557
Size: 2684 Color: 262
Size: 176 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16840 Color: 560
Size: 1808 Color: 211
Size: 1000 Color: 148

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16884 Color: 561
Size: 2400 Color: 247
Size: 364 Color: 33

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16904 Color: 562
Size: 2296 Color: 241
Size: 448 Color: 55

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 16914 Color: 563
Size: 1982 Color: 223
Size: 752 Color: 117

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16928 Color: 564
Size: 1418 Color: 186
Size: 1302 Color: 175

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17008 Color: 566
Size: 1744 Color: 207
Size: 896 Color: 138

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 569
Size: 1648 Color: 202
Size: 864 Color: 136

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17140 Color: 570
Size: 1916 Color: 220
Size: 592 Color: 88

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17184 Color: 572
Size: 1824 Color: 213
Size: 640 Color: 98

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17196 Color: 573
Size: 1600 Color: 192
Size: 852 Color: 131

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17274 Color: 577
Size: 1414 Color: 184
Size: 960 Color: 143

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17372 Color: 580
Size: 1900 Color: 219
Size: 376 Color: 36

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17388 Color: 581
Size: 1780 Color: 210
Size: 480 Color: 64

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17478 Color: 586
Size: 1414 Color: 185
Size: 756 Color: 119

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17488 Color: 587
Size: 1392 Color: 180
Size: 768 Color: 120

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17504 Color: 588
Size: 1736 Color: 206
Size: 408 Color: 46

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17516 Color: 589
Size: 1856 Color: 215
Size: 276 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17536 Color: 590
Size: 1404 Color: 181
Size: 708 Color: 114

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17590 Color: 593
Size: 1418 Color: 187
Size: 640 Color: 99

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 17606 Color: 595
Size: 1424 Color: 190
Size: 618 Color: 94

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 596
Size: 1344 Color: 178
Size: 688 Color: 108

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 17652 Color: 598
Size: 1420 Color: 188
Size: 576 Color: 82

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 17672 Color: 599
Size: 1296 Color: 173
Size: 680 Color: 103

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 17680 Color: 600
Size: 1408 Color: 182
Size: 560 Color: 79

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 480
Size: 4275 Color: 323
Size: 892 Color: 137

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 15047 Color: 494
Size: 4552 Color: 334
Size: 48 Color: 2

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 15396 Color: 503
Size: 3091 Color: 283
Size: 1160 Color: 165

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 15478 Color: 504
Size: 2969 Color: 272
Size: 1200 Color: 167

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 15551 Color: 510
Size: 4096 Color: 318

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 15931 Color: 519
Size: 1884 Color: 218
Size: 1832 Color: 214

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 16231 Color: 532
Size: 3416 Color: 295

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 16453 Color: 541
Size: 2576 Color: 255
Size: 618 Color: 93

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 16745 Color: 552
Size: 1718 Color: 205
Size: 1184 Color: 166

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 16798 Color: 558
Size: 2849 Color: 266

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 17226 Color: 574
Size: 2421 Color: 250

Bin 77: 2 of cap free
Amount of items: 7
Items: 
Size: 9828 Color: 407
Size: 2648 Color: 259
Size: 2528 Color: 253
Size: 2278 Color: 239
Size: 968 Color: 145
Size: 704 Color: 110
Size: 692 Color: 109

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 458
Size: 4186 Color: 320
Size: 2088 Color: 230

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 15102 Color: 495
Size: 4544 Color: 333

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 15246 Color: 499
Size: 4016 Color: 316
Size: 384 Color: 43

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 15558 Color: 511
Size: 2776 Color: 263
Size: 1312 Color: 176

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 16136 Color: 529
Size: 3478 Color: 299
Size: 32 Color: 1

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 16236 Color: 533
Size: 3410 Color: 293

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 16504 Color: 545
Size: 3142 Color: 288

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 17144 Color: 571
Size: 2502 Color: 251

Bin 86: 3 of cap free
Amount of items: 4
Items: 
Size: 12384 Color: 444
Size: 6493 Color: 368
Size: 384 Color: 42
Size: 384 Color: 41

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 15364 Color: 501
Size: 4281 Color: 324

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 16565 Color: 548
Size: 3080 Color: 281

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 13641 Color: 465
Size: 5683 Color: 356
Size: 320 Color: 21

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 16012 Color: 525
Size: 3632 Color: 303

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 16096 Color: 528
Size: 3548 Color: 301

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 16533 Color: 547
Size: 3111 Color: 285

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 17012 Color: 567
Size: 2632 Color: 258

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 17256 Color: 575
Size: 2388 Color: 245

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 17404 Color: 582
Size: 2240 Color: 238

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 17418 Color: 583
Size: 2226 Color: 237

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 17576 Color: 592
Size: 2068 Color: 228

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 14259 Color: 474
Size: 2976 Color: 273
Size: 2408 Color: 248

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 15119 Color: 496
Size: 4524 Color: 332

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 15131 Color: 497
Size: 4512 Color: 331

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 15878 Color: 516
Size: 3765 Color: 307

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 15947 Color: 521
Size: 3696 Color: 306

Bin 103: 6 of cap free
Amount of items: 5
Items: 
Size: 11169 Color: 425
Size: 6487 Color: 367
Size: 858 Color: 134
Size: 568 Color: 80
Size: 560 Color: 78

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 12010 Color: 441
Size: 7216 Color: 387
Size: 416 Color: 47

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 16592 Color: 549
Size: 3050 Color: 279

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 16650 Color: 551
Size: 2992 Color: 275

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 17264 Color: 576
Size: 2378 Color: 243

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 17622 Color: 597
Size: 2020 Color: 226

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 14416 Color: 479
Size: 5033 Color: 342
Size: 192 Color: 10

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 15939 Color: 520
Size: 3670 Color: 304
Size: 32 Color: 0

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 15960 Color: 522
Size: 3680 Color: 305

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 17464 Color: 585
Size: 2176 Color: 234

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 17544 Color: 591
Size: 2096 Color: 232

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 17596 Color: 594
Size: 2044 Color: 227

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 16224 Color: 531
Size: 3415 Color: 294

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 568
Size: 2597 Color: 256

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 16080 Color: 527
Size: 3558 Color: 302

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 17356 Color: 579
Size: 2282 Color: 240

Bin 119: 11 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 456
Size: 5989 Color: 359
Size: 352 Color: 28

Bin 120: 12 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 429
Size: 7820 Color: 391
Size: 512 Color: 70

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 12996 Color: 451
Size: 6640 Color: 370

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 472
Size: 5408 Color: 351

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 15496 Color: 505
Size: 4140 Color: 319

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 16800 Color: 559
Size: 2836 Color: 265

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 17462 Color: 584
Size: 2174 Color: 233

Bin 126: 13 of cap free
Amount of items: 3
Items: 
Size: 13303 Color: 457
Size: 5992 Color: 360
Size: 340 Color: 27

Bin 127: 13 of cap free
Amount of items: 2
Items: 
Size: 16978 Color: 565
Size: 2657 Color: 260

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 14738 Color: 487
Size: 4896 Color: 340

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 16146 Color: 530
Size: 3488 Color: 300

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 17326 Color: 578
Size: 2308 Color: 242

Bin 131: 16 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 450
Size: 6688 Color: 372

Bin 132: 16 of cap free
Amount of items: 3
Items: 
Size: 13168 Color: 453
Size: 6112 Color: 363
Size: 352 Color: 31

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 471
Size: 5176 Color: 344
Size: 256 Color: 13

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 485
Size: 4884 Color: 339
Size: 64 Color: 3

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 500
Size: 4368 Color: 326

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 16510 Color: 546
Size: 3122 Color: 287

Bin 137: 17 of cap free
Amount of items: 5
Items: 
Size: 9840 Color: 411
Size: 4269 Color: 322
Size: 4004 Color: 315
Size: 896 Color: 140
Size: 622 Color: 96

Bin 138: 17 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 473
Size: 4295 Color: 325
Size: 1088 Color: 161

Bin 139: 17 of cap free
Amount of items: 2
Items: 
Size: 14285 Color: 476
Size: 5346 Color: 350

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 15536 Color: 509
Size: 4094 Color: 317

Bin 141: 22 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 455
Size: 6040 Color: 361
Size: 352 Color: 29

Bin 142: 26 of cap free
Amount of items: 4
Items: 
Size: 14626 Color: 484
Size: 4852 Color: 338
Size: 80 Color: 5
Size: 64 Color: 4

Bin 143: 27 of cap free
Amount of items: 4
Items: 
Size: 11596 Color: 432
Size: 7081 Color: 380
Size: 472 Color: 61
Size: 472 Color: 60

Bin 144: 28 of cap free
Amount of items: 2
Items: 
Size: 15740 Color: 515
Size: 3880 Color: 311

Bin 145: 31 of cap free
Amount of items: 3
Items: 
Size: 12829 Color: 449
Size: 6416 Color: 366
Size: 372 Color: 35

Bin 146: 32 of cap free
Amount of items: 4
Items: 
Size: 11628 Color: 433
Size: 7084 Color: 381
Size: 456 Color: 58
Size: 448 Color: 56

Bin 147: 35 of cap free
Amount of items: 9
Items: 
Size: 9825 Color: 404
Size: 1636 Color: 200
Size: 1636 Color: 199
Size: 1632 Color: 197
Size: 1632 Color: 196
Size: 932 Color: 142
Size: 784 Color: 124
Size: 768 Color: 122
Size: 768 Color: 121

Bin 148: 36 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 443
Size: 6896 Color: 374
Size: 384 Color: 44

Bin 149: 38 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 424
Size: 6366 Color: 364
Size: 2080 Color: 229

Bin 150: 42 of cap free
Amount of items: 3
Items: 
Size: 13448 Color: 461
Size: 5818 Color: 357
Size: 340 Color: 26

Bin 151: 42 of cap free
Amount of items: 3
Items: 
Size: 14054 Color: 470
Size: 5296 Color: 349
Size: 256 Color: 14

Bin 152: 46 of cap free
Amount of items: 2
Items: 
Size: 14940 Color: 491
Size: 4662 Color: 335

Bin 153: 48 of cap free
Amount of items: 5
Items: 
Size: 9836 Color: 410
Size: 3924 Color: 312
Size: 3790 Color: 309
Size: 1422 Color: 189
Size: 628 Color: 97

Bin 154: 48 of cap free
Amount of items: 3
Items: 
Size: 10848 Color: 415
Size: 8176 Color: 395
Size: 576 Color: 81

Bin 155: 49 of cap free
Amount of items: 3
Items: 
Size: 11696 Color: 435
Size: 7459 Color: 389
Size: 444 Color: 53

Bin 156: 67 of cap free
Amount of items: 5
Items: 
Size: 9832 Color: 408
Size: 3099 Color: 284
Size: 3018 Color: 276
Size: 2960 Color: 271
Size: 672 Color: 102

Bin 157: 72 of cap free
Amount of items: 3
Items: 
Size: 12472 Color: 446
Size: 6720 Color: 373
Size: 384 Color: 39

Bin 158: 74 of cap free
Amount of items: 3
Items: 
Size: 14304 Color: 478
Size: 5062 Color: 343
Size: 208 Color: 11

Bin 159: 87 of cap free
Amount of items: 3
Items: 
Size: 12805 Color: 448
Size: 6380 Color: 365
Size: 376 Color: 37

Bin 160: 88 of cap free
Amount of items: 3
Items: 
Size: 11240 Color: 428
Size: 7808 Color: 390
Size: 512 Color: 71

Bin 161: 89 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 447
Size: 6513 Color: 369
Size: 376 Color: 38

Bin 162: 92 of cap free
Amount of items: 3
Items: 
Size: 13088 Color: 452
Size: 6100 Color: 362
Size: 368 Color: 34

Bin 163: 96 of cap free
Amount of items: 3
Items: 
Size: 13216 Color: 454
Size: 5984 Color: 358
Size: 352 Color: 30

Bin 164: 103 of cap free
Amount of items: 3
Items: 
Size: 13609 Color: 464
Size: 5616 Color: 355
Size: 320 Color: 22

Bin 165: 114 of cap free
Amount of items: 3
Items: 
Size: 11996 Color: 440
Size: 7122 Color: 386
Size: 416 Color: 48

Bin 166: 115 of cap free
Amount of items: 3
Items: 
Size: 14288 Color: 477
Size: 5007 Color: 341
Size: 238 Color: 12

Bin 167: 116 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 468
Size: 5424 Color: 352
Size: 280 Color: 17

Bin 168: 128 of cap free
Amount of items: 3
Items: 
Size: 11680 Color: 434
Size: 7392 Color: 388
Size: 448 Color: 54

Bin 169: 140 of cap free
Amount of items: 3
Items: 
Size: 14519 Color: 482
Size: 4797 Color: 336
Size: 192 Color: 9

Bin 170: 158 of cap free
Amount of items: 3
Items: 
Size: 11952 Color: 439
Size: 7122 Color: 385
Size: 416 Color: 49

Bin 171: 172 of cap free
Amount of items: 3
Items: 
Size: 12408 Color: 445
Size: 6684 Color: 371
Size: 384 Color: 40

Bin 172: 173 of cap free
Amount of items: 4
Items: 
Size: 11440 Color: 431
Size: 7073 Color: 379
Size: 482 Color: 66
Size: 480 Color: 65

Bin 173: 177 of cap free
Amount of items: 3
Items: 
Size: 14527 Color: 483
Size: 4816 Color: 337
Size: 128 Color: 6

Bin 174: 180 of cap free
Amount of items: 4
Items: 
Size: 11408 Color: 430
Size: 7052 Color: 378
Size: 512 Color: 69
Size: 496 Color: 67

Bin 175: 184 of cap free
Amount of items: 4
Items: 
Size: 13568 Color: 462
Size: 5224 Color: 346
Size: 336 Color: 25
Size: 336 Color: 24

Bin 176: 202 of cap free
Amount of items: 3
Items: 
Size: 13578 Color: 463
Size: 5548 Color: 354
Size: 320 Color: 23

Bin 177: 209 of cap free
Amount of items: 5
Items: 
Size: 9834 Color: 409
Size: 3775 Color: 308
Size: 3220 Color: 289
Size: 1946 Color: 222
Size: 664 Color: 101

Bin 178: 246 of cap free
Amount of items: 3
Items: 
Size: 11865 Color: 438
Size: 7121 Color: 384
Size: 416 Color: 50

Bin 179: 247 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 467
Size: 5289 Color: 348
Size: 316 Color: 19

Bin 180: 258 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 437
Size: 7101 Color: 383
Size: 432 Color: 51

Bin 181: 263 of cap free
Amount of items: 2
Items: 
Size: 11161 Color: 423
Size: 8224 Color: 403

Bin 182: 290 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 436
Size: 7093 Color: 382
Size: 432 Color: 52

Bin 183: 300 of cap free
Amount of items: 3
Items: 
Size: 13792 Color: 466
Size: 5236 Color: 347
Size: 320 Color: 20

Bin 184: 304 of cap free
Amount of items: 2
Items: 
Size: 11156 Color: 422
Size: 8188 Color: 402

Bin 185: 308 of cap free
Amount of items: 2
Items: 
Size: 11153 Color: 421
Size: 8187 Color: 401

Bin 186: 325 of cap free
Amount of items: 2
Items: 
Size: 11137 Color: 420
Size: 8186 Color: 400

Bin 187: 334 of cap free
Amount of items: 2
Items: 
Size: 11129 Color: 419
Size: 8185 Color: 399

Bin 188: 358 of cap free
Amount of items: 2
Items: 
Size: 11106 Color: 418
Size: 8184 Color: 398

Bin 189: 358 of cap free
Amount of items: 4
Items: 
Size: 11234 Color: 427
Size: 7026 Color: 377
Size: 518 Color: 73
Size: 512 Color: 72

Bin 190: 361 of cap free
Amount of items: 2
Items: 
Size: 11105 Color: 417
Size: 8182 Color: 397

Bin 191: 364 of cap free
Amount of items: 4
Items: 
Size: 11218 Color: 426
Size: 7016 Color: 376
Size: 530 Color: 76
Size: 520 Color: 74

Bin 192: 366 of cap free
Amount of items: 2
Items: 
Size: 11102 Color: 416
Size: 8180 Color: 396

Bin 193: 420 of cap free
Amount of items: 4
Items: 
Size: 9904 Color: 414
Size: 8168 Color: 394
Size: 580 Color: 85
Size: 576 Color: 83

Bin 194: 424 of cap free
Amount of items: 4
Items: 
Size: 9888 Color: 413
Size: 8144 Color: 393
Size: 600 Color: 89
Size: 592 Color: 86

Bin 195: 534 of cap free
Amount of items: 4
Items: 
Size: 9848 Color: 412
Size: 8050 Color: 392
Size: 608 Color: 91
Size: 608 Color: 90

Bin 196: 539 of cap free
Amount of items: 7
Items: 
Size: 9827 Color: 406
Size: 2204 Color: 235
Size: 1938 Color: 221
Size: 1870 Color: 217
Size: 1862 Color: 216
Size: 704 Color: 112
Size: 704 Color: 111

Bin 197: 734 of cap free
Amount of items: 8
Items: 
Size: 9826 Color: 405
Size: 1810 Color: 212
Size: 1768 Color: 208
Size: 1702 Color: 204
Size: 1636 Color: 201
Size: 736 Color: 116
Size: 732 Color: 115
Size: 704 Color: 113

Bin 198: 862 of cap free
Amount of items: 16
Items: 
Size: 1632 Color: 195
Size: 1632 Color: 194
Size: 1632 Color: 193
Size: 1408 Color: 183
Size: 1376 Color: 179
Size: 1336 Color: 177
Size: 1298 Color: 174
Size: 1280 Color: 172
Size: 1280 Color: 171
Size: 896 Color: 139
Size: 854 Color: 133
Size: 854 Color: 132
Size: 836 Color: 130
Size: 832 Color: 129
Size: 824 Color: 128
Size: 816 Color: 127

Bin 199: 7532 of cap free
Amount of items: 11
Items: 
Size: 1272 Color: 170
Size: 1272 Color: 169
Size: 1216 Color: 168
Size: 1136 Color: 163
Size: 1056 Color: 159
Size: 1056 Color: 157
Size: 1040 Color: 155
Size: 1024 Color: 154
Size: 1024 Color: 153
Size: 1020 Color: 151
Size: 1000 Color: 147

Total size: 3890304
Total free space: 19648

