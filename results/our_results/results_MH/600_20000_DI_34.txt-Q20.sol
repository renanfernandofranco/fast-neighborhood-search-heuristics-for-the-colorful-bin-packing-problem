Capicity Bin: 16544
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 8274 Color: 5
Size: 1192 Color: 19
Size: 1192 Color: 17
Size: 1188 Color: 12
Size: 1152 Color: 14
Size: 1056 Color: 18
Size: 1030 Color: 11
Size: 1022 Color: 5
Size: 438 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10456 Color: 4
Size: 5524 Color: 12
Size: 564 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11728 Color: 0
Size: 4528 Color: 17
Size: 288 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 6
Size: 4344 Color: 17
Size: 224 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12118 Color: 11
Size: 3702 Color: 8
Size: 724 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12316 Color: 14
Size: 3704 Color: 8
Size: 524 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12445 Color: 14
Size: 3417 Color: 9
Size: 682 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 11
Size: 3704 Color: 8
Size: 336 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 3
Size: 2675 Color: 1
Size: 1112 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12889 Color: 0
Size: 2759 Color: 7
Size: 896 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12912 Color: 13
Size: 3272 Color: 5
Size: 360 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 12
Size: 3266 Color: 1
Size: 362 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 16
Size: 2904 Color: 19
Size: 576 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 12
Size: 2642 Color: 7
Size: 664 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13252 Color: 17
Size: 2684 Color: 3
Size: 608 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13342 Color: 11
Size: 2758 Color: 12
Size: 444 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 1
Size: 2596 Color: 7
Size: 576 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 18
Size: 1952 Color: 6
Size: 1080 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13520 Color: 1
Size: 2748 Color: 11
Size: 276 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13552 Color: 6
Size: 2544 Color: 4
Size: 448 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13784 Color: 4
Size: 2296 Color: 11
Size: 464 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13826 Color: 14
Size: 2518 Color: 17
Size: 200 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 7
Size: 2084 Color: 6
Size: 624 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13915 Color: 11
Size: 1669 Color: 16
Size: 960 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 19
Size: 2284 Color: 11
Size: 248 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14109 Color: 2
Size: 2031 Color: 12
Size: 404 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14150 Color: 11
Size: 1856 Color: 6
Size: 538 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14163 Color: 8
Size: 2011 Color: 2
Size: 370 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14192 Color: 1
Size: 1456 Color: 3
Size: 896 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 10
Size: 1596 Color: 11
Size: 752 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14276 Color: 0
Size: 1940 Color: 10
Size: 328 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14308 Color: 9
Size: 1912 Color: 15
Size: 324 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14363 Color: 4
Size: 1581 Color: 3
Size: 600 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 3
Size: 1519 Color: 0
Size: 646 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 11
Size: 1788 Color: 6
Size: 352 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 10
Size: 1512 Color: 2
Size: 626 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 14
Size: 1808 Color: 7
Size: 312 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14543 Color: 11
Size: 1417 Color: 2
Size: 584 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 3
Size: 1616 Color: 4
Size: 376 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14608 Color: 11
Size: 1168 Color: 10
Size: 768 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 6
Size: 1476 Color: 7
Size: 384 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14744 Color: 8
Size: 1500 Color: 19
Size: 300 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 19
Size: 1188 Color: 16
Size: 608 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14760 Color: 17
Size: 1080 Color: 4
Size: 704 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14804 Color: 0
Size: 1452 Color: 11
Size: 288 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14800 Color: 11
Size: 1216 Color: 9
Size: 528 Color: 18

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14811 Color: 13
Size: 1445 Color: 14
Size: 288 Color: 4

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 10862 Color: 4
Size: 4457 Color: 14
Size: 1224 Color: 14

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 11975 Color: 13
Size: 3324 Color: 5
Size: 1244 Color: 4

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 8
Size: 3927 Color: 19

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 13335 Color: 2
Size: 3208 Color: 10

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 13679 Color: 14
Size: 2768 Color: 10
Size: 96 Color: 19

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 13701 Color: 13
Size: 1964 Color: 0
Size: 878 Color: 12

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 13748 Color: 18
Size: 2795 Color: 8

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 13911 Color: 13
Size: 2632 Color: 15

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 13943 Color: 6
Size: 1360 Color: 3
Size: 1240 Color: 15

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 14352 Color: 17
Size: 2191 Color: 7

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 14495 Color: 4
Size: 1184 Color: 11
Size: 864 Color: 0

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 9078 Color: 1
Size: 6824 Color: 8
Size: 640 Color: 7

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 9086 Color: 15
Size: 6888 Color: 8
Size: 568 Color: 6

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 7
Size: 6218 Color: 16
Size: 456 Color: 19

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 12556 Color: 10
Size: 3690 Color: 1
Size: 296 Color: 19

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 13142 Color: 3
Size: 3044 Color: 12
Size: 356 Color: 4

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 0
Size: 2512 Color: 4
Size: 656 Color: 15

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 5
Size: 1496 Color: 10
Size: 736 Color: 11

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 14464 Color: 7
Size: 2078 Color: 12

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 14557 Color: 19
Size: 1985 Color: 14

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 14723 Color: 13
Size: 1819 Color: 19

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 14782 Color: 3
Size: 1760 Color: 0

Bin 70: 3 of cap free
Amount of items: 7
Items: 
Size: 8278 Color: 10
Size: 1805 Color: 5
Size: 1798 Color: 14
Size: 1556 Color: 19
Size: 1344 Color: 15
Size: 1344 Color: 3
Size: 416 Color: 6

Bin 71: 3 of cap free
Amount of items: 7
Items: 
Size: 8280 Color: 19
Size: 1862 Color: 10
Size: 1855 Color: 11
Size: 1848 Color: 17
Size: 1376 Color: 15
Size: 672 Color: 17
Size: 648 Color: 0

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 3
Size: 6893 Color: 16
Size: 292 Color: 3

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 10357 Color: 1
Size: 5864 Color: 6
Size: 320 Color: 15

Bin 74: 3 of cap free
Amount of items: 2
Items: 
Size: 10417 Color: 2
Size: 6124 Color: 4

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 11120 Color: 7
Size: 5133 Color: 1
Size: 288 Color: 19

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 12080 Color: 15
Size: 4461 Color: 2

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 4
Size: 3884 Color: 8

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 2
Size: 6808 Color: 10
Size: 312 Color: 5

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 8
Size: 3270 Color: 10
Size: 1378 Color: 13

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 18
Size: 3368 Color: 7
Size: 416 Color: 4

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 18
Size: 3028 Color: 11
Size: 608 Color: 3

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 13108 Color: 6
Size: 3176 Color: 14
Size: 256 Color: 12

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 15
Size: 2878 Color: 8

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 14104 Color: 4
Size: 2436 Color: 5

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 16
Size: 2086 Color: 0
Size: 64 Color: 14

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 14722 Color: 19
Size: 1818 Color: 18

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 14864 Color: 2
Size: 1676 Color: 10

Bin 88: 5 of cap free
Amount of items: 3
Items: 
Size: 9968 Color: 16
Size: 5947 Color: 4
Size: 624 Color: 7

Bin 89: 5 of cap free
Amount of items: 3
Items: 
Size: 11240 Color: 12
Size: 4467 Color: 11
Size: 832 Color: 4

Bin 90: 5 of cap free
Amount of items: 2
Items: 
Size: 12106 Color: 18
Size: 4433 Color: 3

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 12752 Color: 8
Size: 3157 Color: 14
Size: 630 Color: 11

Bin 92: 6 of cap free
Amount of items: 3
Items: 
Size: 10552 Color: 19
Size: 5506 Color: 10
Size: 480 Color: 14

Bin 93: 6 of cap free
Amount of items: 2
Items: 
Size: 11538 Color: 3
Size: 5000 Color: 11

Bin 94: 6 of cap free
Amount of items: 3
Items: 
Size: 13294 Color: 13
Size: 2592 Color: 5
Size: 652 Color: 4

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 14240 Color: 6
Size: 2238 Color: 0
Size: 60 Color: 0

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 14698 Color: 2
Size: 1840 Color: 3

Bin 97: 7 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 13
Size: 4401 Color: 7
Size: 800 Color: 15

Bin 98: 7 of cap free
Amount of items: 3
Items: 
Size: 11424 Color: 9
Size: 4505 Color: 1
Size: 608 Color: 8

Bin 99: 7 of cap free
Amount of items: 2
Items: 
Size: 12590 Color: 7
Size: 3947 Color: 16

Bin 100: 7 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 2
Size: 2371 Color: 18

Bin 101: 8 of cap free
Amount of items: 3
Items: 
Size: 11550 Color: 7
Size: 4730 Color: 11
Size: 256 Color: 10

Bin 102: 8 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 6
Size: 3644 Color: 14

Bin 103: 8 of cap free
Amount of items: 2
Items: 
Size: 13602 Color: 19
Size: 2934 Color: 18

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 10
Size: 2492 Color: 13

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 14566 Color: 3
Size: 1970 Color: 13

Bin 106: 9 of cap free
Amount of items: 3
Items: 
Size: 12642 Color: 5
Size: 3241 Color: 14
Size: 652 Color: 1

Bin 107: 9 of cap free
Amount of items: 2
Items: 
Size: 13191 Color: 12
Size: 3344 Color: 3

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 14878 Color: 9
Size: 1657 Color: 6

Bin 109: 10 of cap free
Amount of items: 2
Items: 
Size: 10858 Color: 8
Size: 5676 Color: 14

Bin 110: 10 of cap free
Amount of items: 3
Items: 
Size: 12172 Color: 19
Size: 2838 Color: 19
Size: 1524 Color: 5

Bin 111: 10 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 1
Size: 3718 Color: 18
Size: 256 Color: 11

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 13046 Color: 8
Size: 3488 Color: 16

Bin 113: 11 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 2
Size: 3789 Color: 3

Bin 114: 11 of cap free
Amount of items: 2
Items: 
Size: 13235 Color: 16
Size: 3298 Color: 0

Bin 115: 11 of cap free
Amount of items: 2
Items: 
Size: 14131 Color: 6
Size: 2402 Color: 4

Bin 116: 12 of cap free
Amount of items: 3
Items: 
Size: 9200 Color: 8
Size: 6884 Color: 5
Size: 448 Color: 14

Bin 117: 12 of cap free
Amount of items: 3
Items: 
Size: 10608 Color: 16
Size: 4500 Color: 17
Size: 1424 Color: 1

Bin 118: 12 of cap free
Amount of items: 3
Items: 
Size: 11197 Color: 13
Size: 5107 Color: 10
Size: 228 Color: 12

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 11556 Color: 0
Size: 4976 Color: 11

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 4
Size: 2312 Color: 12

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 14534 Color: 2
Size: 1998 Color: 8

Bin 122: 12 of cap free
Amount of items: 3
Items: 
Size: 14636 Color: 8
Size: 1864 Color: 12
Size: 32 Color: 13

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 14716 Color: 8
Size: 1816 Color: 6

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 14780 Color: 18
Size: 1678 Color: 2
Size: 74 Color: 16

Bin 125: 14 of cap free
Amount of items: 11
Items: 
Size: 8276 Color: 17
Size: 1128 Color: 6
Size: 1072 Color: 4
Size: 1032 Color: 1
Size: 1030 Color: 6
Size: 1026 Color: 10
Size: 1020 Color: 2
Size: 682 Color: 0
Size: 544 Color: 3
Size: 432 Color: 6
Size: 288 Color: 9

Bin 126: 14 of cap free
Amount of items: 3
Items: 
Size: 10054 Color: 2
Size: 5964 Color: 16
Size: 512 Color: 3

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 10401 Color: 11
Size: 6128 Color: 8

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 14
Size: 3816 Color: 4

Bin 129: 17 of cap free
Amount of items: 3
Items: 
Size: 10353 Color: 9
Size: 5414 Color: 0
Size: 760 Color: 11

Bin 130: 17 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 12
Size: 2621 Color: 3

Bin 131: 17 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 18
Size: 2389 Color: 15
Size: 96 Color: 6

Bin 132: 18 of cap free
Amount of items: 3
Items: 
Size: 9196 Color: 1
Size: 6892 Color: 4
Size: 438 Color: 11

Bin 133: 19 of cap free
Amount of items: 3
Items: 
Size: 11225 Color: 5
Size: 4812 Color: 13
Size: 488 Color: 7

Bin 134: 19 of cap free
Amount of items: 2
Items: 
Size: 11241 Color: 10
Size: 5284 Color: 13

Bin 135: 19 of cap free
Amount of items: 3
Items: 
Size: 11353 Color: 17
Size: 4164 Color: 6
Size: 1008 Color: 4

Bin 136: 19 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 11
Size: 4421 Color: 0

Bin 137: 19 of cap free
Amount of items: 2
Items: 
Size: 12453 Color: 3
Size: 4072 Color: 8

Bin 138: 20 of cap free
Amount of items: 3
Items: 
Size: 10368 Color: 4
Size: 5564 Color: 5
Size: 592 Color: 1

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 10740 Color: 15
Size: 5784 Color: 14

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 14264 Color: 0
Size: 2260 Color: 19

Bin 141: 21 of cap free
Amount of items: 2
Items: 
Size: 14328 Color: 1
Size: 2195 Color: 19

Bin 142: 22 of cap free
Amount of items: 5
Items: 
Size: 8296 Color: 5
Size: 3254 Color: 12
Size: 2226 Color: 10
Size: 1856 Color: 0
Size: 890 Color: 16

Bin 143: 22 of cap free
Amount of items: 4
Items: 
Size: 8304 Color: 7
Size: 4162 Color: 13
Size: 3272 Color: 7
Size: 784 Color: 16

Bin 144: 22 of cap free
Amount of items: 2
Items: 
Size: 14540 Color: 4
Size: 1982 Color: 19

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 13904 Color: 1
Size: 2616 Color: 15

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 14480 Color: 10
Size: 2040 Color: 16

Bin 147: 26 of cap free
Amount of items: 2
Items: 
Size: 13874 Color: 3
Size: 2644 Color: 14

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 14064 Color: 5
Size: 2454 Color: 14

Bin 149: 27 of cap free
Amount of items: 11
Items: 
Size: 8273 Color: 18
Size: 992 Color: 16
Size: 960 Color: 4
Size: 944 Color: 17
Size: 944 Color: 12
Size: 882 Color: 13
Size: 840 Color: 7
Size: 832 Color: 9
Size: 782 Color: 15
Size: 540 Color: 11
Size: 528 Color: 0

Bin 150: 27 of cap free
Amount of items: 2
Items: 
Size: 14649 Color: 12
Size: 1868 Color: 19

Bin 151: 27 of cap free
Amount of items: 2
Items: 
Size: 14845 Color: 2
Size: 1672 Color: 3

Bin 152: 28 of cap free
Amount of items: 3
Items: 
Size: 9512 Color: 18
Size: 6604 Color: 7
Size: 400 Color: 3

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 13332 Color: 14
Size: 3184 Color: 16

Bin 154: 28 of cap free
Amount of items: 3
Items: 
Size: 14392 Color: 4
Size: 2096 Color: 5
Size: 28 Color: 5

Bin 155: 30 of cap free
Amount of items: 2
Items: 
Size: 13232 Color: 6
Size: 3282 Color: 8

Bin 156: 30 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 2
Size: 2710 Color: 12

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 14182 Color: 2
Size: 2332 Color: 14

Bin 158: 31 of cap free
Amount of items: 3
Items: 
Size: 11257 Color: 5
Size: 5080 Color: 1
Size: 176 Color: 6

Bin 159: 33 of cap free
Amount of items: 3
Items: 
Size: 10050 Color: 10
Size: 5949 Color: 7
Size: 512 Color: 15

Bin 160: 40 of cap free
Amount of items: 4
Items: 
Size: 8328 Color: 3
Size: 5410 Color: 18
Size: 1390 Color: 12
Size: 1376 Color: 15

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 9608 Color: 4
Size: 6896 Color: 16

Bin 162: 40 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 8
Size: 3728 Color: 0

Bin 163: 42 of cap free
Amount of items: 3
Items: 
Size: 10870 Color: 15
Size: 4742 Color: 10
Size: 890 Color: 7

Bin 164: 44 of cap free
Amount of items: 3
Items: 
Size: 12626 Color: 3
Size: 2224 Color: 11
Size: 1650 Color: 13

Bin 165: 49 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 17
Size: 2695 Color: 10

Bin 166: 52 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 19
Size: 3056 Color: 11

Bin 167: 55 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 11
Size: 6891 Color: 4
Size: 886 Color: 0
Size: 320 Color: 5

Bin 168: 55 of cap free
Amount of items: 2
Items: 
Size: 9409 Color: 1
Size: 7080 Color: 8

Bin 169: 56 of cap free
Amount of items: 2
Items: 
Size: 14319 Color: 14
Size: 2169 Color: 1

Bin 170: 63 of cap free
Amount of items: 2
Items: 
Size: 11833 Color: 12
Size: 4648 Color: 9

Bin 171: 67 of cap free
Amount of items: 3
Items: 
Size: 11971 Color: 2
Size: 4174 Color: 8
Size: 332 Color: 7

Bin 172: 69 of cap free
Amount of items: 3
Items: 
Size: 9407 Color: 6
Size: 6588 Color: 1
Size: 480 Color: 15

Bin 173: 69 of cap free
Amount of items: 2
Items: 
Size: 13311 Color: 14
Size: 3164 Color: 11

Bin 174: 79 of cap free
Amount of items: 2
Items: 
Size: 12449 Color: 18
Size: 4016 Color: 19

Bin 175: 84 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 10
Size: 3524 Color: 3

Bin 176: 92 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 15
Size: 4244 Color: 0
Size: 536 Color: 4

Bin 177: 96 of cap free
Amount of items: 2
Items: 
Size: 13400 Color: 0
Size: 3048 Color: 18

Bin 178: 98 of cap free
Amount of items: 2
Items: 
Size: 11285 Color: 0
Size: 5161 Color: 15

Bin 179: 98 of cap free
Amount of items: 2
Items: 
Size: 13399 Color: 13
Size: 3047 Color: 19

Bin 180: 105 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 9
Size: 3413 Color: 12

Bin 181: 118 of cap free
Amount of items: 2
Items: 
Size: 10204 Color: 7
Size: 6222 Color: 8

Bin 182: 120 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 19
Size: 2868 Color: 8

Bin 183: 122 of cap free
Amount of items: 2
Items: 
Size: 11265 Color: 16
Size: 5157 Color: 8

Bin 184: 125 of cap free
Amount of items: 8
Items: 
Size: 8277 Color: 1
Size: 1522 Color: 9
Size: 1376 Color: 13
Size: 1376 Color: 6
Size: 1360 Color: 15
Size: 1360 Color: 10
Size: 752 Color: 0
Size: 396 Color: 3

Bin 185: 125 of cap free
Amount of items: 2
Items: 
Size: 12610 Color: 6
Size: 3809 Color: 4

Bin 186: 137 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 8
Size: 4947 Color: 3

Bin 187: 157 of cap free
Amount of items: 3
Items: 
Size: 10351 Color: 7
Size: 5488 Color: 9
Size: 548 Color: 10

Bin 188: 158 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 16
Size: 3808 Color: 18

Bin 189: 162 of cap free
Amount of items: 2
Items: 
Size: 9188 Color: 2
Size: 7194 Color: 7

Bin 190: 163 of cap free
Amount of items: 2
Items: 
Size: 10385 Color: 4
Size: 5996 Color: 8

Bin 191: 172 of cap free
Amount of items: 2
Items: 
Size: 10772 Color: 14
Size: 5600 Color: 17

Bin 192: 180 of cap free
Amount of items: 2
Items: 
Size: 9740 Color: 1
Size: 6624 Color: 7

Bin 193: 184 of cap free
Amount of items: 33
Items: 
Size: 824 Color: 2
Size: 800 Color: 7
Size: 736 Color: 13
Size: 736 Color: 1
Size: 728 Color: 15
Size: 656 Color: 9
Size: 624 Color: 5
Size: 558 Color: 18
Size: 550 Color: 5
Size: 544 Color: 18
Size: 534 Color: 14
Size: 528 Color: 10
Size: 512 Color: 18
Size: 496 Color: 16
Size: 496 Color: 9
Size: 480 Color: 17
Size: 476 Color: 9
Size: 472 Color: 5
Size: 448 Color: 10
Size: 416 Color: 19
Size: 416 Color: 2
Size: 402 Color: 2
Size: 396 Color: 15
Size: 392 Color: 0
Size: 384 Color: 3
Size: 384 Color: 1
Size: 368 Color: 0
Size: 352 Color: 12
Size: 352 Color: 11
Size: 336 Color: 11
Size: 330 Color: 3
Size: 320 Color: 3
Size: 314 Color: 1

Bin 194: 190 of cap free
Amount of items: 2
Items: 
Size: 11193 Color: 11
Size: 5161 Color: 16

Bin 195: 219 of cap free
Amount of items: 6
Items: 
Size: 8284 Color: 18
Size: 1975 Color: 4
Size: 1968 Color: 9
Size: 1892 Color: 4
Size: 1470 Color: 6
Size: 736 Color: 11

Bin 196: 240 of cap free
Amount of items: 3
Items: 
Size: 8360 Color: 12
Size: 6856 Color: 4
Size: 1088 Color: 10

Bin 197: 262 of cap free
Amount of items: 2
Items: 
Size: 9388 Color: 10
Size: 6894 Color: 6

Bin 198: 275 of cap free
Amount of items: 2
Items: 
Size: 11148 Color: 9
Size: 5121 Color: 4

Bin 199: 10830 of cap free
Amount of items: 17
Items: 
Size: 400 Color: 6
Size: 396 Color: 8
Size: 384 Color: 19
Size: 384 Color: 5
Size: 372 Color: 15
Size: 368 Color: 15
Size: 368 Color: 10
Size: 332 Color: 12
Size: 328 Color: 11
Size: 320 Color: 0
Size: 304 Color: 12
Size: 304 Color: 6
Size: 302 Color: 17
Size: 302 Color: 11
Size: 288 Color: 1
Size: 282 Color: 18
Size: 280 Color: 3

Total size: 3275712
Total free space: 16544

