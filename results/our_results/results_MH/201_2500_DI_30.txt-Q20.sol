Capicity Bin: 2032
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1258 Color: 9
Size: 646 Color: 9
Size: 128 Color: 7

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1646 Color: 0
Size: 186 Color: 17
Size: 148 Color: 1
Size: 52 Color: 14

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1790 Color: 11
Size: 136 Color: 0
Size: 56 Color: 7
Size: 42 Color: 14
Size: 8 Color: 7

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1722 Color: 2
Size: 202 Color: 16
Size: 72 Color: 14
Size: 36 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 13
Size: 168 Color: 11
Size: 38 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1371 Color: 5
Size: 551 Color: 16
Size: 110 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 1
Size: 549 Color: 12
Size: 108 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1186 Color: 1
Size: 468 Color: 9
Size: 306 Color: 10
Size: 72 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 18
Size: 382 Color: 7
Size: 72 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1142 Color: 16
Size: 858 Color: 10
Size: 32 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 18
Size: 210 Color: 6
Size: 40 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 5
Size: 233 Color: 4
Size: 20 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 16
Size: 277 Color: 17
Size: 54 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1157 Color: 14
Size: 731 Color: 15
Size: 144 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 12
Size: 205 Color: 9
Size: 40 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 14
Size: 282 Color: 15
Size: 52 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 19
Size: 373 Color: 15
Size: 74 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 13
Size: 514 Color: 14
Size: 100 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1277 Color: 8
Size: 631 Color: 4
Size: 124 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1159 Color: 15
Size: 729 Color: 0
Size: 144 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 6
Size: 202 Color: 7
Size: 36 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 11
Size: 427 Color: 3
Size: 84 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 5
Size: 182 Color: 19
Size: 40 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 6
Size: 231 Color: 14
Size: 48 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 7
Size: 281 Color: 13
Size: 46 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 5
Size: 218 Color: 10
Size: 40 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1408 Color: 19
Size: 420 Color: 3
Size: 204 Color: 7

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 5
Size: 342 Color: 16
Size: 64 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 2
Size: 570 Color: 2
Size: 112 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 8
Size: 886 Color: 11
Size: 128 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1102 Color: 1
Size: 710 Color: 6
Size: 220 Color: 0

Bin 32: 0 of cap free
Amount of items: 4
Items: 
Size: 1017 Color: 15
Size: 847 Color: 8
Size: 92 Color: 9
Size: 76 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1474 Color: 0
Size: 312 Color: 13
Size: 246 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 13
Size: 274 Color: 13
Size: 52 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 8
Size: 219 Color: 18
Size: 42 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 9
Size: 425 Color: 4
Size: 84 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 2
Size: 262 Color: 0
Size: 32 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 16
Size: 212 Color: 16
Size: 40 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 18
Size: 322 Color: 4
Size: 44 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 0
Size: 211 Color: 2
Size: 64 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1042 Color: 10
Size: 826 Color: 13
Size: 164 Color: 7

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 1
Size: 194 Color: 7
Size: 36 Color: 2

Bin 43: 0 of cap free
Amount of items: 4
Items: 
Size: 1272 Color: 3
Size: 692 Color: 3
Size: 60 Color: 12
Size: 8 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 12
Size: 374 Color: 1
Size: 72 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 8
Size: 845 Color: 8
Size: 168 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 8
Size: 289 Color: 4
Size: 48 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 19
Size: 203 Color: 9
Size: 22 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 5
Size: 410 Color: 3
Size: 80 Color: 14

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1342 Color: 18
Size: 578 Color: 11
Size: 112 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 12
Size: 367 Color: 9
Size: 72 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 0
Size: 309 Color: 11
Size: 60 Color: 5

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 7
Size: 303 Color: 17
Size: 60 Color: 15

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 3
Size: 251 Color: 13
Size: 48 Color: 13

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 18
Size: 257 Color: 17
Size: 50 Color: 10

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 12
Size: 335 Color: 4
Size: 66 Color: 10

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 2
Size: 174 Color: 16
Size: 40 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 17
Size: 426 Color: 7
Size: 84 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 824 Color: 17
Size: 742 Color: 17
Size: 466 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1273 Color: 6
Size: 633 Color: 15
Size: 126 Color: 16

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 7
Size: 191 Color: 7
Size: 38 Color: 10

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 17
Size: 266 Color: 1
Size: 52 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 8
Size: 381 Color: 7
Size: 74 Color: 14

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1455 Color: 1
Size: 481 Color: 9
Size: 96 Color: 17

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 15
Size: 483 Color: 14
Size: 96 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 5
Size: 377 Color: 11
Size: 74 Color: 16

Total size: 132080
Total free space: 0

