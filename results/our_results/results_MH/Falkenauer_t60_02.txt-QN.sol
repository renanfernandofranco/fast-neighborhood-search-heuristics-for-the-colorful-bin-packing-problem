Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 51
Size: 281 Color: 26
Size: 260 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 53
Size: 263 Color: 15
Size: 261 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 56
Size: 267 Color: 19
Size: 251 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 48
Size: 305 Color: 30
Size: 266 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 55
Size: 267 Color: 17
Size: 251 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 57
Size: 256 Color: 8
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 59
Size: 250 Color: 1
Size: 252 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 40
Size: 350 Color: 37
Size: 288 Color: 29

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 52
Size: 277 Color: 23
Size: 259 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 45
Size: 326 Color: 32
Size: 276 Color: 22

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 46
Size: 339 Color: 34
Size: 261 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 42
Size: 352 Color: 38
Size: 279 Color: 24

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 50
Size: 284 Color: 27
Size: 280 Color: 25

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 43
Size: 354 Color: 39
Size: 268 Color: 20

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 58
Size: 252 Color: 5
Size: 250 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 41
Size: 345 Color: 35
Size: 288 Color: 28

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 267 Color: 18
Size: 254 Color: 7
Size: 479 Color: 54

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 47
Size: 328 Color: 33
Size: 271 Color: 21

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 44
Size: 350 Color: 36
Size: 260 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 49
Size: 308 Color: 31
Size: 262 Color: 14

Total size: 20000
Total free space: 0

