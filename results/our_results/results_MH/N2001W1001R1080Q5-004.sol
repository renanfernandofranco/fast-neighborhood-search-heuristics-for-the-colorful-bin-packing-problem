Capicity Bin: 1001
Lower Bound: 905

Bins used: 905
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 3
Size: 378 Color: 0
Size: 188 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 4
Size: 337 Color: 1
Size: 155 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 3
Size: 261 Color: 1
Size: 155 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 1
Size: 189 Color: 0
Size: 178 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 3
Size: 162 Color: 3
Size: 147 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 4
Size: 186 Color: 3
Size: 108 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 155 Color: 4
Size: 154 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 4
Size: 166 Color: 0
Size: 125 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 0
Size: 136 Color: 1
Size: 110 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 1
Size: 138 Color: 2
Size: 108 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 1
Size: 167 Color: 2
Size: 159 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 1
Size: 185 Color: 4
Size: 167 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 0
Size: 133 Color: 3
Size: 110 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 2
Size: 126 Color: 4
Size: 126 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 4
Size: 138 Color: 0
Size: 115 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 342 Color: 3
Size: 225 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 0
Size: 303 Color: 1
Size: 182 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 392 Color: 4
Size: 183 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 4
Size: 160 Color: 4
Size: 149 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 3
Size: 129 Color: 1
Size: 108 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 0
Size: 141 Color: 3
Size: 137 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 0
Size: 221 Color: 1
Size: 167 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 1
Size: 125 Color: 2
Size: 115 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 0
Size: 165 Color: 2
Size: 164 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 615 Color: 1
Size: 194 Color: 4
Size: 192 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 162 Color: 0
Size: 147 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 0
Size: 138 Color: 3
Size: 101 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 343 Color: 2
Size: 295 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 2
Size: 107 Color: 4
Size: 106 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 3
Size: 141 Color: 2
Size: 110 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 1
Size: 162 Color: 4
Size: 147 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 1
Size: 191 Color: 3
Size: 191 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 399 Color: 4
Size: 197 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 2
Size: 196 Color: 0
Size: 195 Color: 2

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 512 Color: 1
Size: 342 Color: 2
Size: 147 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 2
Size: 342 Color: 3
Size: 143 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 4
Size: 117 Color: 1
Size: 116 Color: 1

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 4
Size: 233 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 4
Size: 109 Color: 0
Size: 104 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 2
Size: 133 Color: 3
Size: 104 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 4
Size: 166 Color: 1
Size: 160 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 0
Size: 158 Color: 3
Size: 151 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 0
Size: 261 Color: 4
Size: 137 Color: 2

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 2
Size: 493 Color: 3

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 3
Size: 171 Color: 3
Size: 169 Color: 1

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 0
Size: 340 Color: 2

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 3
Size: 375 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 528 Color: 2
Size: 302 Color: 2
Size: 171 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 4
Size: 183 Color: 0
Size: 179 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 2
Size: 197 Color: 0
Size: 165 Color: 0

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 416 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 0
Size: 191 Color: 2
Size: 191 Color: 1

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 4

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 265 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 4
Size: 167 Color: 1
Size: 159 Color: 3

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 4
Size: 314 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 4
Size: 314 Color: 1
Size: 178 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 3
Size: 114 Color: 3
Size: 100 Color: 2

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 378 Color: 1

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 3
Size: 378 Color: 1

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 1
Size: 278 Color: 2

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 4
Size: 367 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 366 Color: 1
Size: 141 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 529 Color: 0
Size: 291 Color: 0
Size: 181 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 4
Size: 229 Color: 2
Size: 181 Color: 3

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 276 Color: 2

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 278 Color: 2

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 3
Size: 342 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 2
Size: 155 Color: 1
Size: 154 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 512 Color: 2
Size: 342 Color: 0
Size: 147 Color: 0

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 2
Size: 336 Color: 1

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 4
Size: 309 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 4
Size: 176 Color: 4
Size: 160 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 343 Color: 3
Size: 178 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 2
Size: 392 Color: 4
Size: 132 Color: 3

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 477 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 2
Size: 150 Color: 0
Size: 120 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 690 Color: 0
Size: 198 Color: 1
Size: 113 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 4
Size: 349 Color: 4
Size: 120 Color: 3

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 429 Color: 4

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 430 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 4
Size: 197 Color: 3
Size: 190 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 3
Size: 234 Color: 0
Size: 197 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 432 Color: 0
Size: 120 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 427 Color: 4
Size: 142 Color: 2

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 4
Size: 257 Color: 3

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 2
Size: 257 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 4
Size: 179 Color: 0
Size: 141 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 3
Size: 348 Color: 4
Size: 144 Color: 2

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 1
Size: 309 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 2
Size: 196 Color: 1
Size: 113 Color: 3

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 0

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 1
Size: 350 Color: 2

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 2
Size: 120 Color: 4
Size: 117 Color: 4

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 350 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 4
Size: 330 Color: 0
Size: 139 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 1
Size: 330 Color: 1
Size: 139 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 2
Size: 140 Color: 4
Size: 112 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 4
Size: 122 Color: 3
Size: 122 Color: 2

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 289 Color: 3

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 0
Size: 290 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 2
Size: 146 Color: 1
Size: 144 Color: 1

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 2
Size: 135 Color: 1
Size: 104 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 586 Color: 0
Size: 301 Color: 1
Size: 114 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 744 Color: 2
Size: 132 Color: 1
Size: 125 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 4
Size: 204 Color: 3
Size: 184 Color: 4

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 4
Size: 224 Color: 0

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 4
Size: 224 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 363 Color: 3
Size: 143 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 2
Size: 122 Color: 0
Size: 121 Color: 3

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 0
Size: 239 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 529 Color: 4
Size: 291 Color: 1
Size: 181 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 3
Size: 230 Color: 3
Size: 180 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 0
Size: 303 Color: 3
Size: 182 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 412 Color: 2
Size: 157 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 4
Size: 177 Color: 0
Size: 170 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 399 Color: 2
Size: 157 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 0
Size: 180 Color: 3
Size: 129 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 4
Size: 146 Color: 2
Size: 144 Color: 2

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 3
Size: 253 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 2
Size: 129 Color: 1
Size: 124 Color: 4

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 1
Size: 492 Color: 2

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 332 Color: 2

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 332 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 332 Color: 0
Size: 175 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 1
Size: 332 Color: 2
Size: 160 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 0
Size: 105 Color: 4
Size: 104 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 426 Color: 4
Size: 143 Color: 2

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 349 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 3
Size: 125 Color: 1
Size: 117 Color: 2

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 323 Color: 3
Size: 183 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 528 Color: 2
Size: 291 Color: 1
Size: 182 Color: 3

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 2
Size: 227 Color: 3
Size: 126 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 399 Color: 2
Size: 142 Color: 3

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 3

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 3

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 3
Size: 155 Color: 1
Size: 139 Color: 1

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 2
Size: 246 Color: 3

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 4
Size: 139 Color: 1
Size: 100 Color: 4

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 3
Size: 365 Color: 2

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 4
Size: 365 Color: 4
Size: 180 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 742 Color: 0
Size: 131 Color: 2
Size: 128 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 4
Size: 127 Color: 3
Size: 121 Color: 0

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 2

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 2
Size: 460 Color: 1

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 0

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 4

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 4
Size: 329 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 3
Size: 161 Color: 1
Size: 151 Color: 2

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 0
Size: 312 Color: 4

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 305 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 0
Size: 164 Color: 3
Size: 148 Color: 3

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 234 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 3
Size: 234 Color: 2
Size: 197 Color: 0

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 0
Size: 347 Color: 4

Bin 164: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 347 Color: 3

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 4
Size: 443 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 443 Color: 0
Size: 110 Color: 2

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 0
Size: 124 Color: 4
Size: 115 Color: 3

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 576 Color: 4
Size: 291 Color: 0
Size: 134 Color: 3

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 4
Size: 233 Color: 4
Size: 186 Color: 3

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 432 Color: 4
Size: 120 Color: 3

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 431 Color: 4

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 0
Size: 233 Color: 4
Size: 198 Color: 2

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 3
Size: 193 Color: 0
Size: 190 Color: 3

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 1
Size: 216 Color: 2
Size: 167 Color: 3

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 4
Size: 425 Color: 1

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 425 Color: 3

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 576 Color: 2
Size: 287 Color: 0
Size: 138 Color: 3

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 1
Size: 126 Color: 3
Size: 122 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 4
Size: 152 Color: 3
Size: 118 Color: 2

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 3
Size: 145 Color: 2
Size: 125 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 2
Size: 170 Color: 2
Size: 152 Color: 4

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 3
Size: 152 Color: 1
Size: 134 Color: 4

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 4

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 4
Size: 198 Color: 4
Size: 184 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 0
Size: 193 Color: 1
Size: 189 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 0
Size: 126 Color: 3
Size: 123 Color: 3

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 3
Size: 412 Color: 1

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 3
Size: 444 Color: 2

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 245 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 295 Color: 1
Size: 262 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 412 Color: 3
Size: 145 Color: 2

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 730 Color: 2
Size: 139 Color: 0
Size: 132 Color: 2

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 3
Size: 132 Color: 0
Size: 119 Color: 4

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 243 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 4
Size: 135 Color: 1
Size: 108 Color: 2

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 3
Size: 243 Color: 2

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 4
Size: 195 Color: 1
Size: 192 Color: 1

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 382 Color: 2

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 3

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 2
Size: 195 Color: 3
Size: 180 Color: 3

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 248 Color: 1

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 277 Color: 2

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 4
Size: 186 Color: 2
Size: 166 Color: 1

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 3
Size: 194 Color: 1
Size: 158 Color: 2

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 0

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 4

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 226 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 331 Color: 1
Size: 225 Color: 3

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 627 Color: 2
Size: 227 Color: 3
Size: 147 Color: 1

Bin 211: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 480 Color: 2

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 0
Size: 363 Color: 3

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 4
Size: 352 Color: 1

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 1
Size: 376 Color: 4

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 376 Color: 4
Size: 188 Color: 1

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 3

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 400 Color: 1

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 400 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 376 Color: 2
Size: 225 Color: 3

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 4
Size: 376 Color: 2
Size: 185 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 4
Size: 217 Color: 1
Size: 171 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 2
Size: 125 Color: 0
Size: 117 Color: 2

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 744 Color: 3
Size: 152 Color: 1
Size: 105 Color: 3

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 2
Size: 343 Color: 4

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 303 Color: 2

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 3
Size: 303 Color: 1

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 407 Color: 0

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 409 Color: 3

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 615 Color: 4
Size: 233 Color: 4
Size: 153 Color: 3

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 799 Color: 3
Size: 102 Color: 3
Size: 100 Color: 0

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 2

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 2

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 0
Size: 234 Color: 0
Size: 163 Color: 2

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 364 Color: 3

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 0
Size: 364 Color: 3

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 3
Size: 140 Color: 0
Size: 112 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 2
Size: 313 Color: 4
Size: 179 Color: 4

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 343 Color: 2
Size: 178 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 399 Color: 4
Size: 157 Color: 0

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 283 Color: 1

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 431 Color: 1

Bin 243: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 470 Color: 3

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 470 Color: 2

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 282 Color: 1

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 4
Size: 282 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 4
Size: 282 Color: 4
Size: 188 Color: 0

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 4
Size: 261 Color: 3

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 2
Size: 492 Color: 1

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 2

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 0

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 230 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 4
Size: 195 Color: 3
Size: 180 Color: 3

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 742 Color: 0
Size: 131 Color: 3
Size: 128 Color: 3

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 3
Size: 302 Color: 0

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 4
Size: 306 Color: 1

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 3
Size: 213 Color: 2

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 2
Size: 106 Color: 2
Size: 103 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 2
Size: 107 Color: 3
Size: 106 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 0
Size: 173 Color: 4
Size: 163 Color: 1

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 3
Size: 207 Color: 0

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 207 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 4
Size: 195 Color: 4
Size: 194 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 4
Size: 233 Color: 4
Size: 154 Color: 1

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 387 Color: 1

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 493 Color: 2

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 528 Color: 4
Size: 302 Color: 1
Size: 171 Color: 4

Bin 268: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 302 Color: 3

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 528 Color: 4
Size: 296 Color: 3
Size: 177 Color: 3

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 1

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 439 Color: 4

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 4
Size: 439 Color: 1

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 420 Color: 1
Size: 142 Color: 3

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 4

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 4
Size: 484 Color: 3

Bin 277: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 2
Size: 235 Color: 1

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 1
Size: 273 Color: 4

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 3
Size: 449 Color: 0

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 449 Color: 0

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 2
Size: 249 Color: 3

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 3
Size: 227 Color: 1

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 459 Color: 3

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 0
Size: 316 Color: 1

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 342 Color: 3

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 315 Color: 2
Size: 227 Color: 0

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 397 Color: 0

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 3
Size: 229 Color: 4

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 168 Color: 4
Size: 107 Color: 0

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 660 Color: 0
Size: 173 Color: 3
Size: 168 Color: 4

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 2
Size: 229 Color: 1
Size: 168 Color: 1

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 3
Size: 345 Color: 1

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 237 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 744 Color: 3
Size: 138 Color: 0
Size: 119 Color: 1

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 3
Size: 212 Color: 1

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 211 Color: 2

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 2
Size: 212 Color: 1

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 2

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 4
Size: 108 Color: 2
Size: 105 Color: 2

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 3
Size: 163 Color: 4
Size: 156 Color: 0

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 3
Size: 489 Color: 1

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 3

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 340 Color: 3

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 3

Bin 306: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 0
Size: 287 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 1
Size: 146 Color: 2
Size: 144 Color: 4

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 0
Size: 421 Color: 2

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 2

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 0
Size: 358 Color: 4

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 4
Size: 358 Color: 2
Size: 166 Color: 2

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 372 Color: 3

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 374 Color: 2

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 374 Color: 3

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 2

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 358 Color: 0
Size: 166 Color: 1

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 328 Color: 2

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 2
Size: 328 Color: 3

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 1
Size: 179 Color: 2
Size: 158 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 399 Color: 2
Size: 142 Color: 4

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 0
Size: 390 Color: 1

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 0
Size: 390 Color: 1

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 0
Size: 238 Color: 4

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 0
Size: 180 Color: 3
Size: 175 Color: 3

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 269 Color: 1

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 3

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 576 Color: 1
Size: 291 Color: 0
Size: 134 Color: 4

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 4
Size: 122 Color: 0
Size: 117 Color: 4

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 3
Size: 262 Color: 4

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 3
Size: 262 Color: 4

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 2

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 494 Color: 4

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 2
Size: 392 Color: 4
Size: 135 Color: 3

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 1

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 3
Size: 231 Color: 0

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 428 Color: 2

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 1
Size: 360 Color: 0

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 1

Bin 340: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 0
Size: 360 Color: 3

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 4
Size: 205 Color: 0

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 205 Color: 0

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 3
Size: 425 Color: 1

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 3
Size: 453 Color: 2

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 2
Size: 372 Color: 3

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 311 Color: 4

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 1
Size: 350 Color: 4

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 2

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 3
Size: 487 Color: 0

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 498 Color: 0

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 2
Size: 461 Color: 3

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 0
Size: 316 Color: 2

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 446 Color: 2

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 0
Size: 432 Color: 3

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 2

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 2

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 4
Size: 472 Color: 3

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 3

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 0
Size: 268 Color: 4

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 295 Color: 0

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 339 Color: 0

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 3
Size: 202 Color: 1

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 218 Color: 4

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 2
Size: 453 Color: 0

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 4

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 3

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 1

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 0
Size: 448 Color: 1

Bin 369: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 223 Color: 0

Bin 370: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 4
Size: 223 Color: 2

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 1

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 446 Color: 4

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 4
Size: 355 Color: 0

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 371 Color: 3

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 2

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 1
Size: 281 Color: 4

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 0
Size: 479 Color: 1

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 1

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 4
Size: 414 Color: 0

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 463 Color: 4

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 4

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 0

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 4

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 2
Size: 228 Color: 0

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 2

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 297 Color: 1

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 437 Color: 0

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 0

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 2
Size: 260 Color: 1

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 1

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 4
Size: 421 Color: 1

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 455 Color: 0

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 437 Color: 4

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 0
Size: 423 Color: 2

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 263 Color: 4
Size: 260 Color: 2

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 404 Color: 2

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 463 Color: 4

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 0
Size: 207 Color: 1

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 0

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 2
Size: 276 Color: 4

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 3
Size: 495 Color: 1

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 347 Color: 1

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 381 Color: 3

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 0
Size: 294 Color: 4

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 3
Size: 461 Color: 1

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 4
Size: 435 Color: 0

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 349 Color: 3

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 0

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 333 Color: 0

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 4
Size: 381 Color: 2

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 247 Color: 3

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 4
Size: 247 Color: 3

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 0

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 1
Size: 300 Color: 2

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 0

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 1
Size: 443 Color: 3

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 0

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 440 Color: 2

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 2
Size: 401 Color: 0

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 455 Color: 0

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 4
Size: 281 Color: 1

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 2
Size: 498 Color: 1

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 4
Size: 348 Color: 3

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 4

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 3

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 2
Size: 211 Color: 4

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 4
Size: 202 Color: 3

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 2
Size: 277 Color: 0

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 4
Size: 231 Color: 3

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 344 Color: 1

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 4
Size: 311 Color: 0

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 0

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 1

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 1
Size: 364 Color: 3

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 232 Color: 2

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 3
Size: 407 Color: 2

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 1

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 4
Size: 420 Color: 3

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 459 Color: 2

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 3
Size: 459 Color: 2

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 4
Size: 427 Color: 0

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 4
Size: 437 Color: 2

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 3

Bin 444: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 3
Size: 354 Color: 0

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 424 Color: 2

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 3
Size: 334 Color: 4

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 0
Size: 334 Color: 2

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 4
Size: 292 Color: 3

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 455 Color: 3

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 371 Color: 4

Bin 451: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 1
Size: 452 Color: 2

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 1
Size: 410 Color: 3

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 379 Color: 2

Bin 454: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 2
Size: 342 Color: 1

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 1
Size: 365 Color: 0

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 3
Size: 365 Color: 4

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 2

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 4

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 4
Size: 286 Color: 3

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 3
Size: 214 Color: 2

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 0
Size: 433 Color: 4

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 3
Size: 256 Color: 0

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 1
Size: 279 Color: 3

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 4
Size: 472 Color: 1

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 3
Size: 433 Color: 4

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 415 Color: 0

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 4
Size: 420 Color: 2

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 208 Color: 3

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 3
Size: 456 Color: 4

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 244 Color: 4

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 1
Size: 244 Color: 0

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 234 Color: 3

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 256 Color: 3

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 4
Size: 373 Color: 0

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 1
Size: 232 Color: 4

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 2

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 451 Color: 2

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 483 Color: 4

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 3
Size: 238 Color: 1

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 384 Color: 3

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 1
Size: 384 Color: 2

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 1

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 418 Color: 4

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 285 Color: 4

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 1
Size: 309 Color: 2

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 0

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 0

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 4
Size: 439 Color: 0

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 296 Color: 1

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 2

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 0
Size: 394 Color: 2

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 398 Color: 0

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 3

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 0
Size: 457 Color: 2

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 3
Size: 281 Color: 4

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 4

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 3

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 3
Size: 250 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 2
Size: 135 Color: 3
Size: 119 Color: 0

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 4
Size: 236 Color: 2

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 0
Size: 457 Color: 3

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 2

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 4

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 0
Size: 398 Color: 2

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 1
Size: 228 Color: 0

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 2

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 451 Color: 4

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 1
Size: 346 Color: 0

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 3
Size: 448 Color: 4

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 3

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 2
Size: 358 Color: 4

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 279 Color: 1

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 4

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 2
Size: 223 Color: 0

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 2

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 1

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 4
Size: 475 Color: 1

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 393 Color: 3

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 3

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 2
Size: 300 Color: 1

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 1
Size: 481 Color: 4

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 406 Color: 3

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 4
Size: 346 Color: 3

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 2
Size: 369 Color: 4

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 0

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 339 Color: 0

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 469 Color: 4

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 321 Color: 3

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 311 Color: 2

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 4

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 2
Size: 254 Color: 1

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 491 Color: 0

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 491 Color: 4

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 647 Color: 0
Size: 180 Color: 2
Size: 174 Color: 3

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 0
Size: 250 Color: 2

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 4
Size: 380 Color: 0

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 311 Color: 1

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 0
Size: 292 Color: 4

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 4
Size: 250 Color: 2

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 2
Size: 461 Color: 0

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 3
Size: 264 Color: 0

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 3

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 322 Color: 2

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 488 Color: 3

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 3
Size: 322 Color: 2

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 329 Color: 4

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 2

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 0
Size: 414 Color: 3

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 2
Size: 365 Color: 3

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 483 Color: 4

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 441 Color: 0

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 0

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 479 Color: 4

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 0
Size: 490 Color: 1

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 2
Size: 490 Color: 3

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 0
Size: 380 Color: 2

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 431 Color: 4

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 398 Color: 0

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 3
Size: 313 Color: 1

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 475 Color: 2

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 0
Size: 357 Color: 2

Bin 562: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 2
Size: 329 Color: 1

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 1

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 2

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 1

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 4

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 3

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 3

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 464 Color: 3

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 1

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 481 Color: 2

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 393 Color: 0

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 3
Size: 231 Color: 0

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 1
Size: 456 Color: 3

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 1

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 3
Size: 300 Color: 4

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 4
Size: 349 Color: 3

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 3
Size: 310 Color: 4

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 285 Color: 0

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 2
Size: 420 Color: 3

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 337 Color: 2

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 3
Size: 341 Color: 0

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 356 Color: 2

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 341 Color: 1

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 4
Size: 404 Color: 1

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 446 Color: 1

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 2
Size: 322 Color: 3

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 2
Size: 264 Color: 0

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 280 Color: 3

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 2

Bin 591: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 4
Size: 285 Color: 2

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 475 Color: 3

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 2

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 3

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 279 Color: 4

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 215 Color: 2

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 2
Size: 396 Color: 3

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 4

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 2

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 0
Size: 232 Color: 4

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 0

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 4

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 4

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 475 Color: 4

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 4
Size: 466 Color: 1

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 0

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 409 Color: 2

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 3
Size: 409 Color: 2

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 366 Color: 0

Bin 610: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 2
Size: 361 Color: 0

Bin 611: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 3
Size: 317 Color: 1

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 241 Color: 1

Bin 613: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 4

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 482 Color: 4

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 1
Size: 413 Color: 3

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 2

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 2

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 0
Size: 307 Color: 2

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 3

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 4
Size: 448 Color: 0

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 2

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 395 Color: 2

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 369 Color: 2

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 0
Size: 377 Color: 4

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 210 Color: 1

Bin 626: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 4

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 462 Color: 1

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 2

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 3

Bin 630: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 4
Size: 321 Color: 0

Bin 631: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 3
Size: 405 Color: 1

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 423 Color: 0

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 0
Size: 413 Color: 1

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 2
Size: 499 Color: 0

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 474 Color: 4

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 0

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 499 Color: 2

Bin 638: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 496 Color: 4

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 1
Size: 490 Color: 4

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 489 Color: 2

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 0
Size: 488 Color: 2

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 3

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 487 Color: 3

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 4
Size: 486 Color: 3

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 483 Color: 0

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 2
Size: 483 Color: 4

Bin 647: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 4
Size: 479 Color: 0

Bin 648: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 477 Color: 2

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 4
Size: 471 Color: 0

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 474 Color: 0

Bin 651: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 470 Color: 4

Bin 652: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 469 Color: 0

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 0

Bin 654: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 3
Size: 469 Color: 4

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 4
Size: 467 Color: 0

Bin 656: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 2
Size: 465 Color: 3

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 0
Size: 463 Color: 2

Bin 658: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 3
Size: 464 Color: 0

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 462 Color: 4

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 455 Color: 2

Bin 661: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 4

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 4
Size: 442 Color: 2

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 438 Color: 2

Bin 664: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 0

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 4

Bin 666: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 3

Bin 667: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 0

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 2

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 0
Size: 422 Color: 4

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 417 Color: 0

Bin 671: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 417 Color: 4

Bin 672: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 417 Color: 2

Bin 673: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 4
Size: 411 Color: 3

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 1

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 3

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 0

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 403 Color: 4

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 2
Size: 403 Color: 1

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 1
Size: 400 Color: 4

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 3
Size: 396 Color: 2

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 4

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 1

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 4
Size: 393 Color: 2

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 4
Size: 389 Color: 0

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 381 Color: 4

Bin 686: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 4

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 4

Bin 688: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 362 Color: 4

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 4
Size: 351 Color: 3

Bin 690: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 0
Size: 338 Color: 3

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 4
Size: 334 Color: 0

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 0

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 4

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 3
Size: 324 Color: 2

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 2
Size: 320 Color: 3

Bin 696: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 4

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 0

Bin 698: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 318 Color: 2

Bin 699: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 317 Color: 3

Bin 700: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 4

Bin 701: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 1

Bin 702: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 2
Size: 308 Color: 4

Bin 703: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 308 Color: 1

Bin 704: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 0
Size: 307 Color: 2

Bin 705: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 4

Bin 706: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 288 Color: 0

Bin 707: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 2

Bin 708: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 283 Color: 2

Bin 709: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 2
Size: 277 Color: 0

Bin 710: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 275 Color: 4

Bin 711: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 272 Color: 3

Bin 712: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 4

Bin 713: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 2
Size: 263 Color: 3

Bin 714: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 262 Color: 3

Bin 715: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 258 Color: 1

Bin 716: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 0

Bin 717: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 2

Bin 718: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 4
Size: 254 Color: 0

Bin 719: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 2
Size: 249 Color: 0

Bin 720: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 4
Size: 241 Color: 0

Bin 721: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 4
Size: 241 Color: 0

Bin 722: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 240 Color: 1

Bin 723: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 2
Size: 238 Color: 3

Bin 724: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 0
Size: 236 Color: 1

Bin 725: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 1
Size: 232 Color: 3

Bin 726: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 231 Color: 2

Bin 727: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 2
Size: 231 Color: 1

Bin 728: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 2

Bin 729: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 228 Color: 2

Bin 730: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 222 Color: 1

Bin 731: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 220 Color: 0

Bin 732: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 1
Size: 210 Color: 4

Bin 733: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 1
Size: 210 Color: 0

Bin 734: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 3
Size: 205 Color: 1

Bin 735: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 201 Color: 4

Bin 736: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 2
Size: 201 Color: 1

Bin 737: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 200 Color: 2

Bin 738: 1 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 1
Size: 192 Color: 0
Size: 132 Color: 2

Bin 739: 1 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 3
Size: 245 Color: 2

Bin 740: 1 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 3
Size: 245 Color: 4

Bin 741: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 264 Color: 3

Bin 742: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 225 Color: 3

Bin 743: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 276 Color: 1

Bin 744: 1 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 2
Size: 385 Color: 3

Bin 745: 1 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 2
Size: 235 Color: 4

Bin 746: 1 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 1
Size: 349 Color: 4

Bin 747: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 3
Size: 289 Color: 4

Bin 748: 1 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 1
Size: 415 Color: 0

Bin 749: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 414 Color: 1

Bin 750: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 2
Size: 293 Color: 1

Bin 751: 1 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 0
Size: 344 Color: 3

Bin 752: 1 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 468 Color: 1

Bin 753: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 293 Color: 1

Bin 754: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 293 Color: 0

Bin 755: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 2
Size: 293 Color: 3

Bin 756: 1 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 245 Color: 3

Bin 757: 1 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 361 Color: 2

Bin 758: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 1
Size: 305 Color: 3

Bin 759: 1 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 4
Size: 418 Color: 2

Bin 760: 1 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 287 Color: 3

Bin 761: 1 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 285 Color: 4

Bin 762: 1 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 1
Size: 245 Color: 4

Bin 763: 1 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 4
Size: 369 Color: 0

Bin 764: 1 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 370 Color: 1

Bin 765: 1 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 370 Color: 2

Bin 766: 1 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 0
Size: 440 Color: 3

Bin 767: 1 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 4
Size: 376 Color: 2

Bin 768: 1 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 408 Color: 1

Bin 769: 1 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 3
Size: 282 Color: 0

Bin 770: 1 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 2
Size: 206 Color: 3

Bin 771: 1 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 2
Size: 269 Color: 1

Bin 772: 1 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 0
Size: 273 Color: 4

Bin 773: 1 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 2
Size: 272 Color: 4

Bin 774: 1 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 258 Color: 1

Bin 775: 1 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 2
Size: 370 Color: 1

Bin 776: 1 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 316 Color: 4

Bin 777: 1 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 0
Size: 212 Color: 2

Bin 778: 1 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 211 Color: 1

Bin 779: 1 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 350 Color: 1

Bin 780: 1 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 3
Size: 339 Color: 2

Bin 781: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 1
Size: 404 Color: 2

Bin 782: 1 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 480 Color: 4

Bin 783: 1 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 2
Size: 266 Color: 4

Bin 784: 1 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 445 Color: 3

Bin 785: 1 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 0
Size: 452 Color: 1

Bin 786: 1 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 310 Color: 2

Bin 787: 1 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 372 Color: 3

Bin 788: 1 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 2
Size: 372 Color: 4

Bin 789: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 488 Color: 1

Bin 790: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 2
Size: 199 Color: 1

Bin 791: 1 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 3
Size: 486 Color: 1

Bin 792: 1 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 4
Size: 440 Color: 0

Bin 793: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 2
Size: 276 Color: 4

Bin 794: 1 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 3
Size: 356 Color: 2

Bin 795: 1 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 422 Color: 0

Bin 796: 1 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 0
Size: 279 Color: 1

Bin 797: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 1
Size: 404 Color: 2

Bin 798: 1 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 282 Color: 1

Bin 799: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 483 Color: 3

Bin 800: 1 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 461 Color: 2

Bin 801: 1 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 298 Color: 2

Bin 802: 1 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 378 Color: 1

Bin 803: 1 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 310 Color: 1

Bin 804: 1 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 4
Size: 456 Color: 1

Bin 805: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 264 Color: 3

Bin 806: 1 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 1
Size: 437 Color: 4

Bin 807: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 427 Color: 3

Bin 808: 1 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 401 Color: 3

Bin 809: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 268 Color: 3

Bin 810: 1 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 418 Color: 4

Bin 811: 1 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 356 Color: 4

Bin 812: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 268 Color: 4

Bin 813: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 264 Color: 4

Bin 814: 1 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 221 Color: 4

Bin 815: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 0
Size: 293 Color: 1

Bin 816: 1 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 478 Color: 4

Bin 817: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 199 Color: 2

Bin 818: 1 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 4
Size: 206 Color: 1

Bin 819: 1 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 0
Size: 497 Color: 2

Bin 820: 1 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 4
Size: 231 Color: 3

Bin 821: 1 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 401 Color: 0

Bin 822: 1 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 2
Size: 497 Color: 3

Bin 823: 1 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 2
Size: 481 Color: 3

Bin 824: 1 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 0
Size: 462 Color: 1

Bin 825: 1 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 4
Size: 440 Color: 1

Bin 826: 1 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 407 Color: 4

Bin 827: 1 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 0
Size: 395 Color: 4

Bin 828: 1 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 349 Color: 0

Bin 829: 1 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 279 Color: 1

Bin 830: 1 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 4
Size: 265 Color: 0

Bin 831: 1 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 2
Size: 218 Color: 1

Bin 832: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 199 Color: 2

Bin 833: 2 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 0
Size: 188 Color: 1
Size: 162 Color: 2

Bin 834: 2 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 4
Size: 164 Color: 4
Size: 112 Color: 2

Bin 835: 2 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 235 Color: 0

Bin 836: 2 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 4
Size: 307 Color: 0

Bin 837: 2 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 1
Size: 276 Color: 4

Bin 838: 2 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 304 Color: 3

Bin 839: 2 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 304 Color: 4

Bin 840: 2 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 1
Size: 427 Color: 2

Bin 841: 2 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 2
Size: 323 Color: 0

Bin 842: 2 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 467 Color: 3

Bin 843: 2 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 467 Color: 4

Bin 844: 2 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 350 Color: 2

Bin 845: 2 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 323 Color: 1

Bin 846: 2 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 206 Color: 4

Bin 847: 2 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 445 Color: 1

Bin 848: 2 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 4
Size: 445 Color: 0

Bin 849: 2 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 369 Color: 2

Bin 850: 2 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 4
Size: 440 Color: 3

Bin 851: 2 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 2
Size: 440 Color: 3

Bin 852: 2 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 2
Size: 406 Color: 0

Bin 853: 2 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 282 Color: 0

Bin 854: 2 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 272 Color: 4

Bin 855: 2 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 3
Size: 372 Color: 0

Bin 856: 2 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 4
Size: 452 Color: 0

Bin 857: 2 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 316 Color: 2

Bin 858: 2 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 2
Size: 217 Color: 1

Bin 859: 2 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 2
Size: 282 Color: 4

Bin 860: 2 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 445 Color: 1

Bin 861: 2 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 3
Size: 355 Color: 0

Bin 862: 2 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 3
Size: 474 Color: 1

Bin 863: 2 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 467 Color: 3

Bin 864: 2 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 4
Size: 333 Color: 3

Bin 865: 2 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 1
Size: 394 Color: 0

Bin 866: 2 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 220 Color: 2

Bin 867: 2 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 199 Color: 0

Bin 868: 3 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 3
Size: 426 Color: 1
Size: 140 Color: 4

Bin 869: 3 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 349 Color: 0
Size: 189 Color: 3

Bin 870: 3 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 1
Size: 194 Color: 4
Size: 178 Color: 1

Bin 871: 3 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 0
Size: 137 Color: 1
Size: 100 Color: 3

Bin 872: 3 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 359 Color: 4

Bin 873: 3 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 2
Size: 427 Color: 1

Bin 874: 3 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 428 Color: 2

Bin 875: 3 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 4
Size: 221 Color: 1

Bin 876: 3 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 349 Color: 2

Bin 877: 3 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 466 Color: 4

Bin 878: 3 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 344 Color: 1

Bin 879: 3 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 206 Color: 3

Bin 880: 3 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 406 Color: 0

Bin 881: 3 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 4
Size: 407 Color: 3

Bin 882: 3 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 364 Color: 3

Bin 883: 3 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 344 Color: 1

Bin 884: 3 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 4
Size: 421 Color: 1

Bin 885: 3 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 4
Size: 451 Color: 0

Bin 886: 3 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 421 Color: 0

Bin 887: 3 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 0
Size: 420 Color: 3

Bin 888: 4 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 3
Size: 363 Color: 0
Size: 141 Color: 4

Bin 889: 4 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 427 Color: 2

Bin 890: 4 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 4
Size: 406 Color: 2

Bin 891: 4 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 4
Size: 406 Color: 3

Bin 892: 4 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 406 Color: 1

Bin 893: 4 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 496 Color: 2

Bin 894: 4 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 480 Color: 0

Bin 895: 4 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 451 Color: 2

Bin 896: 4 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 496 Color: 2

Bin 897: 4 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 420 Color: 1

Bin 898: 6 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 197 Color: 4

Bin 899: 11 of cap free
Amount of items: 3
Items: 
Size: 615 Color: 1
Size: 264 Color: 2
Size: 111 Color: 0

Bin 900: 17 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 197 Color: 3

Bin 901: 20 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 399 Color: 2
Size: 171 Color: 4

Bin 902: 20 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 3
Size: 449 Color: 4

Bin 903: 30 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 0
Size: 261 Color: 2
Size: 140 Color: 3

Bin 904: 42 of cap free
Amount of items: 4
Items: 
Size: 363 Color: 1
Size: 322 Color: 2
Size: 149 Color: 4
Size: 125 Color: 3

Bin 905: 533 of cap free
Amount of items: 3
Items: 
Size: 198 Color: 2
Size: 150 Color: 0
Size: 120 Color: 4

Total size: 904961
Total free space: 944

