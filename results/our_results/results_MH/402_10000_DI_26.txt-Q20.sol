Capicity Bin: 6240
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 3125 Color: 0
Size: 2112 Color: 4
Size: 843 Color: 2
Size: 160 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 13
Size: 1976 Color: 18
Size: 188 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4258 Color: 12
Size: 1094 Color: 15
Size: 888 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4293 Color: 17
Size: 1529 Color: 14
Size: 418 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4323 Color: 17
Size: 1725 Color: 2
Size: 192 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4390 Color: 10
Size: 1654 Color: 17
Size: 196 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4452 Color: 6
Size: 1740 Color: 2
Size: 48 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4456 Color: 11
Size: 1736 Color: 15
Size: 48 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 12
Size: 1438 Color: 3
Size: 284 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 12
Size: 1518 Color: 5
Size: 124 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4700 Color: 7
Size: 1060 Color: 17
Size: 480 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 18
Size: 1162 Color: 5
Size: 364 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4792 Color: 5
Size: 1336 Color: 9
Size: 112 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 7
Size: 1148 Color: 5
Size: 224 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4843 Color: 3
Size: 1201 Color: 11
Size: 196 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4846 Color: 9
Size: 1112 Color: 3
Size: 282 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 7
Size: 1084 Color: 11
Size: 236 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 6
Size: 652 Color: 19
Size: 648 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5016 Color: 18
Size: 848 Color: 9
Size: 376 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 9
Size: 942 Color: 15
Size: 272 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5053 Color: 16
Size: 911 Color: 17
Size: 276 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 5
Size: 1106 Color: 11
Size: 72 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 7
Size: 952 Color: 2
Size: 162 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5094 Color: 10
Size: 930 Color: 3
Size: 216 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5103 Color: 2
Size: 949 Color: 9
Size: 188 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5110 Color: 10
Size: 764 Color: 17
Size: 366 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 7
Size: 699 Color: 13
Size: 384 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 0
Size: 582 Color: 5
Size: 518 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 7
Size: 958 Color: 16
Size: 118 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5222 Color: 2
Size: 532 Color: 1
Size: 486 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5229 Color: 3
Size: 797 Color: 17
Size: 214 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5232 Color: 12
Size: 784 Color: 3
Size: 224 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5267 Color: 10
Size: 785 Color: 2
Size: 188 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5326 Color: 0
Size: 482 Color: 16
Size: 432 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5321 Color: 17
Size: 767 Color: 17
Size: 152 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 2
Size: 840 Color: 7
Size: 68 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 16
Size: 729 Color: 16
Size: 144 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5384 Color: 13
Size: 504 Color: 11
Size: 352 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5390 Color: 13
Size: 710 Color: 10
Size: 140 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 19
Size: 666 Color: 5
Size: 152 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5486 Color: 6
Size: 602 Color: 17
Size: 152 Color: 7

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 2
Size: 416 Color: 16
Size: 252 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 13
Size: 516 Color: 18
Size: 138 Color: 15

Bin 44: 1 of cap free
Amount of items: 5
Items: 
Size: 3132 Color: 1
Size: 1391 Color: 0
Size: 1254 Color: 2
Size: 254 Color: 14
Size: 208 Color: 17

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 3341 Color: 4
Size: 2648 Color: 1
Size: 250 Color: 8

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 5
Size: 2374 Color: 17
Size: 140 Color: 6

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 3980 Color: 13
Size: 2097 Color: 11
Size: 162 Color: 6

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 4051 Color: 11
Size: 2188 Color: 6

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 4094 Color: 17
Size: 1611 Color: 10
Size: 534 Color: 19

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 4156 Color: 10
Size: 2083 Color: 12

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 1
Size: 1839 Color: 10
Size: 180 Color: 12

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 17
Size: 1370 Color: 19
Size: 328 Color: 1

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 2
Size: 1353 Color: 13
Size: 238 Color: 12

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 4715 Color: 6
Size: 1274 Color: 13
Size: 250 Color: 15

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 4731 Color: 3
Size: 1324 Color: 7
Size: 184 Color: 1

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 4747 Color: 18
Size: 1492 Color: 3

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 5112 Color: 9
Size: 1127 Color: 0

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 4
Size: 584 Color: 13
Size: 472 Color: 17

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 5190 Color: 4
Size: 1049 Color: 17

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 5336 Color: 9
Size: 903 Color: 15

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 5358 Color: 13
Size: 881 Color: 5

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 5435 Color: 11
Size: 564 Color: 17
Size: 240 Color: 2

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 5548 Color: 10
Size: 691 Color: 13

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 3662 Color: 3
Size: 2408 Color: 10
Size: 168 Color: 18

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 3741 Color: 7
Size: 2417 Color: 1
Size: 80 Color: 19

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 9
Size: 2150 Color: 4
Size: 192 Color: 9

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 4012 Color: 6
Size: 1762 Color: 16
Size: 464 Color: 14

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 4779 Color: 3
Size: 1271 Color: 0
Size: 188 Color: 0

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 4795 Color: 1
Size: 1219 Color: 17
Size: 224 Color: 16

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 4947 Color: 0
Size: 991 Color: 7
Size: 300 Color: 6

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 4983 Color: 15
Size: 1255 Color: 18

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 5294 Color: 5
Size: 712 Color: 18
Size: 232 Color: 7

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 5416 Color: 2
Size: 822 Color: 9

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 5518 Color: 2
Size: 688 Color: 7
Size: 32 Color: 16

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 5550 Color: 4
Size: 688 Color: 15

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 11
Size: 516 Color: 17
Size: 120 Color: 16

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 5608 Color: 12
Size: 630 Color: 16

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 3321 Color: 5
Size: 2580 Color: 9
Size: 336 Color: 6

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 4573 Color: 7
Size: 1532 Color: 16
Size: 132 Color: 8

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 5403 Color: 16
Size: 810 Color: 17
Size: 24 Color: 7

Bin 81: 4 of cap free
Amount of items: 29
Items: 
Size: 416 Color: 18
Size: 416 Color: 4
Size: 376 Color: 19
Size: 324 Color: 2
Size: 304 Color: 10
Size: 304 Color: 1
Size: 256 Color: 19
Size: 256 Color: 11
Size: 242 Color: 12
Size: 224 Color: 13
Size: 220 Color: 1
Size: 216 Color: 19
Size: 208 Color: 6
Size: 208 Color: 2
Size: 200 Color: 13
Size: 196 Color: 0
Size: 176 Color: 16
Size: 176 Color: 4
Size: 176 Color: 3
Size: 176 Color: 2
Size: 164 Color: 9
Size: 160 Color: 11
Size: 144 Color: 14
Size: 144 Color: 10
Size: 114 Color: 8
Size: 112 Color: 12
Size: 112 Color: 10
Size: 112 Color: 3
Size: 104 Color: 6

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 3130 Color: 7
Size: 2594 Color: 5
Size: 512 Color: 13

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 13
Size: 1996 Color: 17
Size: 72 Color: 0

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 5254 Color: 11
Size: 982 Color: 2

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 5312 Color: 6
Size: 924 Color: 13

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 5460 Color: 11
Size: 776 Color: 13

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 9
Size: 600 Color: 19
Size: 32 Color: 15

Bin 88: 5 of cap free
Amount of items: 3
Items: 
Size: 3337 Color: 7
Size: 2602 Color: 19
Size: 296 Color: 3

Bin 89: 5 of cap free
Amount of items: 3
Items: 
Size: 3734 Color: 4
Size: 2421 Color: 14
Size: 80 Color: 0

Bin 90: 5 of cap free
Amount of items: 2
Items: 
Size: 4908 Color: 5
Size: 1327 Color: 18

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 4951 Color: 14
Size: 1284 Color: 2

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 5299 Color: 14
Size: 936 Color: 9

Bin 93: 6 of cap free
Amount of items: 9
Items: 
Size: 3122 Color: 1
Size: 578 Color: 14
Size: 440 Color: 18
Size: 428 Color: 9
Size: 360 Color: 8
Size: 344 Color: 12
Size: 336 Color: 10
Size: 322 Color: 3
Size: 304 Color: 14

Bin 94: 6 of cap free
Amount of items: 3
Items: 
Size: 3148 Color: 8
Size: 2604 Color: 3
Size: 482 Color: 0

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 3920 Color: 5
Size: 2090 Color: 17
Size: 224 Color: 7

Bin 96: 6 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 18
Size: 1662 Color: 11
Size: 180 Color: 4

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 5300 Color: 9
Size: 934 Color: 18

Bin 98: 7 of cap free
Amount of items: 3
Items: 
Size: 3720 Color: 5
Size: 2433 Color: 8
Size: 80 Color: 11

Bin 99: 7 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 0
Size: 982 Color: 15

Bin 100: 8 of cap free
Amount of items: 5
Items: 
Size: 3121 Color: 7
Size: 1079 Color: 11
Size: 1014 Color: 7
Size: 762 Color: 19
Size: 256 Color: 1

Bin 101: 8 of cap free
Amount of items: 3
Items: 
Size: 4040 Color: 4
Size: 1960 Color: 7
Size: 232 Color: 12

Bin 102: 8 of cap free
Amount of items: 2
Items: 
Size: 4407 Color: 11
Size: 1825 Color: 10

Bin 103: 8 of cap free
Amount of items: 3
Items: 
Size: 5240 Color: 8
Size: 960 Color: 12
Size: 32 Color: 10

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 5442 Color: 16
Size: 790 Color: 10

Bin 105: 9 of cap free
Amount of items: 5
Items: 
Size: 3124 Color: 8
Size: 1208 Color: 17
Size: 1191 Color: 7
Size: 580 Color: 16
Size: 128 Color: 18

Bin 106: 9 of cap free
Amount of items: 2
Items: 
Size: 4735 Color: 6
Size: 1496 Color: 17

Bin 107: 9 of cap free
Amount of items: 2
Items: 
Size: 4972 Color: 11
Size: 1259 Color: 17

Bin 108: 10 of cap free
Amount of items: 2
Items: 
Size: 5419 Color: 8
Size: 811 Color: 14

Bin 109: 12 of cap free
Amount of items: 2
Items: 
Size: 4811 Color: 13
Size: 1417 Color: 19

Bin 110: 14 of cap free
Amount of items: 3
Items: 
Size: 3128 Color: 15
Size: 2586 Color: 19
Size: 512 Color: 5

Bin 111: 14 of cap free
Amount of items: 2
Items: 
Size: 4422 Color: 14
Size: 1804 Color: 7

Bin 112: 16 of cap free
Amount of items: 2
Items: 
Size: 5192 Color: 5
Size: 1032 Color: 19

Bin 113: 16 of cap free
Amount of items: 2
Items: 
Size: 5436 Color: 11
Size: 788 Color: 15

Bin 114: 17 of cap free
Amount of items: 3
Items: 
Size: 4035 Color: 14
Size: 2104 Color: 15
Size: 84 Color: 0

Bin 115: 20 of cap free
Amount of items: 2
Items: 
Size: 5544 Color: 4
Size: 676 Color: 11

Bin 116: 21 of cap free
Amount of items: 3
Items: 
Size: 3138 Color: 4
Size: 2601 Color: 11
Size: 480 Color: 12

Bin 117: 22 of cap free
Amount of items: 3
Items: 
Size: 3352 Color: 1
Size: 1966 Color: 5
Size: 900 Color: 7

Bin 118: 22 of cap free
Amount of items: 3
Items: 
Size: 4307 Color: 4
Size: 1623 Color: 15
Size: 288 Color: 7

Bin 119: 22 of cap free
Amount of items: 2
Items: 
Size: 5480 Color: 5
Size: 738 Color: 10

Bin 120: 24 of cap free
Amount of items: 2
Items: 
Size: 4532 Color: 9
Size: 1684 Color: 15

Bin 121: 26 of cap free
Amount of items: 2
Items: 
Size: 4320 Color: 6
Size: 1894 Color: 9

Bin 122: 26 of cap free
Amount of items: 2
Items: 
Size: 4930 Color: 15
Size: 1284 Color: 4

Bin 123: 29 of cap free
Amount of items: 2
Items: 
Size: 3353 Color: 18
Size: 2858 Color: 0

Bin 124: 35 of cap free
Amount of items: 2
Items: 
Size: 3394 Color: 14
Size: 2811 Color: 10

Bin 125: 36 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 19
Size: 1544 Color: 3

Bin 126: 38 of cap free
Amount of items: 2
Items: 
Size: 4762 Color: 3
Size: 1440 Color: 9

Bin 127: 46 of cap free
Amount of items: 2
Items: 
Size: 4914 Color: 6
Size: 1280 Color: 18

Bin 128: 48 of cap free
Amount of items: 2
Items: 
Size: 3588 Color: 0
Size: 2604 Color: 4

Bin 129: 50 of cap free
Amount of items: 3
Items: 
Size: 3146 Color: 19
Size: 2528 Color: 2
Size: 516 Color: 10

Bin 130: 54 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 14
Size: 1884 Color: 17
Size: 176 Color: 4

Bin 131: 57 of cap free
Amount of items: 3
Items: 
Size: 3620 Color: 10
Size: 2407 Color: 14
Size: 156 Color: 19

Bin 132: 58 of cap free
Amount of items: 2
Items: 
Size: 3970 Color: 9
Size: 2212 Color: 11

Bin 133: 5286 of cap free
Amount of items: 9
Items: 
Size: 156 Color: 4
Size: 144 Color: 14
Size: 128 Color: 7
Size: 112 Color: 3
Size: 104 Color: 15
Size: 104 Color: 12
Size: 78 Color: 11
Size: 64 Color: 8
Size: 64 Color: 6

Total size: 823680
Total free space: 6240

