Capicity Bin: 9504
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 333
Size: 1846 Color: 209
Size: 368 Color: 79

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 4688 Color: 272
Size: 1296 Color: 173
Size: 1200 Color: 164
Size: 704 Color: 126
Size: 640 Color: 116
Size: 448 Color: 94
Size: 384 Color: 84
Size: 144 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 382
Size: 1080 Color: 156
Size: 208 Color: 30

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 329
Size: 2072 Color: 217
Size: 240 Color: 44

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8534 Color: 401
Size: 810 Color: 138
Size: 160 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8344 Color: 389
Size: 968 Color: 148
Size: 192 Color: 27

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6751 Color: 314
Size: 2455 Color: 228
Size: 298 Color: 63

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7548 Color: 341
Size: 1636 Color: 199
Size: 320 Color: 70

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6373 Color: 304
Size: 2905 Color: 242
Size: 226 Color: 35

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6870 Color: 319
Size: 2198 Color: 222
Size: 436 Color: 92

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8190 Color: 381
Size: 1098 Color: 157
Size: 216 Color: 32

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 5943 Color: 299
Size: 2969 Color: 243
Size: 528 Color: 106
Size: 64 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 395
Size: 886 Color: 142
Size: 176 Color: 23

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4764 Color: 280
Size: 3956 Color: 265
Size: 784 Color: 127

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7541 Color: 340
Size: 1725 Color: 202
Size: 238 Color: 42

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7584 Color: 345
Size: 1328 Color: 178
Size: 592 Color: 110

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 385
Size: 1048 Color: 154
Size: 208 Color: 29

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 326
Size: 2002 Color: 215
Size: 400 Color: 86

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 324
Size: 2136 Color: 218
Size: 416 Color: 88

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7067 Color: 325
Size: 2031 Color: 216
Size: 406 Color: 87

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 306
Size: 2600 Color: 233
Size: 512 Color: 102

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8010 Color: 370
Size: 1246 Color: 170
Size: 248 Color: 49

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4770 Color: 282
Size: 3946 Color: 261
Size: 788 Color: 133

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4754 Color: 274
Size: 3962 Color: 268
Size: 788 Color: 131

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 307
Size: 2740 Color: 240
Size: 344 Color: 74

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7853 Color: 358
Size: 1557 Color: 192
Size: 94 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7672 Color: 348
Size: 1528 Color: 191
Size: 304 Color: 65

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7572 Color: 343
Size: 1612 Color: 197
Size: 320 Color: 68

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7934 Color: 366
Size: 1310 Color: 175
Size: 260 Color: 54

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5413 Color: 291
Size: 3411 Color: 251
Size: 680 Color: 118

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6888 Color: 320
Size: 2456 Color: 229
Size: 160 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7798 Color: 355
Size: 1422 Color: 184
Size: 284 Color: 60

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6518 Color: 310
Size: 2606 Color: 236
Size: 380 Color: 83

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4765 Color: 281
Size: 3951 Color: 262
Size: 788 Color: 132

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5378 Color: 288
Size: 3442 Color: 254
Size: 684 Color: 120

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 8428 Color: 394
Size: 900 Color: 143
Size: 176 Color: 22

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 300
Size: 2872 Color: 241
Size: 560 Color: 108

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8078 Color: 375
Size: 1190 Color: 163
Size: 236 Color: 41

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7573 Color: 344
Size: 1611 Color: 196
Size: 320 Color: 69

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 376
Size: 1188 Color: 162
Size: 232 Color: 40

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 8254 Color: 386
Size: 1042 Color: 153
Size: 208 Color: 28

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 275
Size: 3964 Color: 269
Size: 784 Color: 128

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6664 Color: 313
Size: 2376 Color: 226
Size: 464 Color: 97

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 8244 Color: 384
Size: 1228 Color: 168
Size: 32 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 7780 Color: 354
Size: 1444 Color: 185
Size: 280 Color: 59

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 357
Size: 1388 Color: 182
Size: 272 Color: 58

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 323
Size: 2142 Color: 219
Size: 428 Color: 89

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 388
Size: 972 Color: 149
Size: 192 Color: 26

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7756 Color: 352
Size: 1460 Color: 187
Size: 288 Color: 61

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 8368 Color: 391
Size: 976 Color: 150
Size: 160 Color: 15

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 327
Size: 1981 Color: 214
Size: 396 Color: 85

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 4753 Color: 273
Size: 3961 Color: 267
Size: 790 Color: 134

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 295
Size: 3002 Color: 246
Size: 596 Color: 112

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6537 Color: 311
Size: 2475 Color: 230
Size: 492 Color: 99

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 284
Size: 3576 Color: 259
Size: 704 Color: 125

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 8516 Color: 400
Size: 828 Color: 139
Size: 160 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 334
Size: 1844 Color: 208
Size: 368 Color: 78

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7558 Color: 342
Size: 1622 Color: 198
Size: 324 Color: 71

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 8072 Color: 374
Size: 1208 Color: 166
Size: 224 Color: 34

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5372 Color: 287
Size: 3444 Color: 255
Size: 688 Color: 121

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 292
Size: 3192 Color: 249
Size: 624 Color: 115

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 298
Size: 2972 Color: 244
Size: 592 Color: 109

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7863 Color: 359
Size: 1369 Color: 181
Size: 272 Color: 57

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 309
Size: 2508 Color: 231
Size: 496 Color: 100

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 8029 Color: 371
Size: 1231 Color: 169
Size: 244 Color: 46

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 8004 Color: 369
Size: 1252 Color: 171
Size: 248 Color: 48

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 8488 Color: 398
Size: 856 Color: 141
Size: 160 Color: 12

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 7864 Color: 360
Size: 1400 Color: 183
Size: 240 Color: 43

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 7949 Color: 367
Size: 1297 Color: 174
Size: 258 Color: 53

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4192 Color: 271
Size: 3504 Color: 258
Size: 1808 Color: 205

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 7920 Color: 363
Size: 1456 Color: 186
Size: 128 Color: 8

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 8134 Color: 380
Size: 1142 Color: 158
Size: 228 Color: 37

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6794 Color: 316
Size: 2262 Color: 224
Size: 448 Color: 95

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7256 Color: 331
Size: 1880 Color: 211
Size: 368 Color: 80

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7228 Color: 330
Size: 1900 Color: 212
Size: 376 Color: 82

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 8100 Color: 377
Size: 1172 Color: 161
Size: 232 Color: 39

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 8042 Color: 372
Size: 1222 Color: 167
Size: 240 Color: 45

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7878 Color: 361
Size: 1358 Color: 180
Size: 268 Color: 56

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 7333 Color: 336
Size: 1811 Color: 206
Size: 360 Color: 76

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 8068 Color: 373
Size: 1204 Color: 165
Size: 232 Color: 38

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 283
Size: 3942 Color: 260
Size: 784 Color: 129

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 399
Size: 922 Color: 146
Size: 68 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7928 Color: 364
Size: 1320 Color: 176
Size: 256 Color: 52

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 321
Size: 2164 Color: 221
Size: 432 Color: 90

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 8110 Color: 378
Size: 1166 Color: 160
Size: 228 Color: 36

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7678 Color: 349
Size: 1522 Color: 190
Size: 304 Color: 66

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7258 Color: 332
Size: 1874 Color: 210
Size: 372 Color: 81

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 8346 Color: 390
Size: 986 Color: 151
Size: 172 Color: 21

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 7414 Color: 337
Size: 1742 Color: 204
Size: 348 Color: 75

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7800 Color: 356
Size: 1608 Color: 195
Size: 96 Color: 5

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5401 Color: 289
Size: 3421 Color: 253
Size: 682 Color: 119

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 312
Size: 2418 Color: 227
Size: 480 Color: 98

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 7590 Color: 346
Size: 1598 Color: 194
Size: 316 Color: 67

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 286
Size: 3452 Color: 256
Size: 688 Color: 122

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 8132 Color: 379
Size: 1148 Color: 159
Size: 224 Color: 33

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 7707 Color: 350
Size: 1499 Color: 189
Size: 298 Color: 64

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 308
Size: 2548 Color: 232
Size: 504 Color: 101

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 294
Size: 3028 Color: 248
Size: 600 Color: 113

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 353
Size: 1574 Color: 193
Size: 164 Color: 18

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 8470 Color: 396
Size: 922 Color: 145
Size: 112 Color: 6

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 7608 Color: 347
Size: 1736 Color: 203
Size: 160 Color: 13

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 317
Size: 2604 Color: 235
Size: 80 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 318
Size: 2220 Color: 223
Size: 440 Color: 93

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 297
Size: 3410 Color: 250
Size: 156 Color: 10

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 285
Size: 3466 Color: 257
Size: 692 Color: 123

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7719 Color: 351
Size: 1489 Color: 188
Size: 296 Color: 62

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 7321 Color: 335
Size: 1821 Color: 207
Size: 362 Color: 77

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7448 Color: 338
Size: 1720 Color: 201
Size: 336 Color: 73

Bin 109: 0 of cap free
Amount of items: 4
Items: 
Size: 7172 Color: 328
Size: 1948 Color: 213
Size: 256 Color: 51
Size: 128 Color: 7

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 387
Size: 1028 Color: 152
Size: 168 Color: 20

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 322
Size: 2163 Color: 220
Size: 432 Color: 91

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 7531 Color: 339
Size: 1645 Color: 200
Size: 328 Color: 72

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 303
Size: 2628 Color: 237
Size: 520 Color: 104

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5931 Color: 296
Size: 2979 Color: 245
Size: 594 Color: 111

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 277
Size: 4040 Color: 270
Size: 704 Color: 124

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 279
Size: 3954 Color: 264
Size: 788 Color: 130

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6300 Color: 302
Size: 2676 Color: 238
Size: 528 Color: 105

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 397
Size: 852 Color: 140
Size: 168 Color: 19

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 4761 Color: 278
Size: 3953 Color: 263
Size: 790 Color: 136

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 301
Size: 2694 Color: 239
Size: 536 Color: 107

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 402
Size: 802 Color: 137
Size: 160 Color: 17

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4757 Color: 276
Size: 3957 Color: 266
Size: 790 Color: 135

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 293
Size: 3026 Color: 247
Size: 604 Color: 114

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 383
Size: 1070 Color: 155
Size: 212 Color: 31

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 7901 Color: 362
Size: 1337 Color: 179
Size: 266 Color: 55

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 305
Size: 2601 Color: 234
Size: 518 Color: 103

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 392
Size: 942 Color: 147
Size: 184 Color: 25

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 7997 Color: 368
Size: 1257 Color: 172
Size: 250 Color: 50

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5410 Color: 290
Size: 3414 Color: 252
Size: 680 Color: 117

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 315
Size: 2285 Color: 225
Size: 456 Color: 96

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7933 Color: 365
Size: 1325 Color: 177
Size: 246 Color: 47

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 393
Size: 914 Color: 144
Size: 180 Color: 24

Total size: 1254528
Total free space: 0

