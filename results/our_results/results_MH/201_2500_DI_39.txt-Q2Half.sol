Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 1696 Color: 1
Size: 384 Color: 1
Size: 96 Color: 1
Size: 88 Color: 1
Size: 72 Color: 1
Size: 64 Color: 0
Size: 48 Color: 0
Size: 8 Color: 0
Size: 8 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 840 Color: 1
Size: 576 Color: 1
Size: 504 Color: 1
Size: 272 Color: 0
Size: 272 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 1
Size: 881 Color: 1
Size: 176 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 1
Size: 468 Color: 1
Size: 88 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 390 Color: 1
Size: 76 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 763 Color: 1
Size: 152 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 1
Size: 212 Color: 1
Size: 40 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 1
Size: 388 Color: 1
Size: 72 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1930 Color: 1
Size: 446 Color: 1
Size: 88 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1927 Color: 1
Size: 449 Color: 1
Size: 88 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 1
Size: 622 Color: 1
Size: 120 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 1
Size: 401 Color: 1
Size: 80 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 1022 Color: 1
Size: 200 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 1
Size: 838 Color: 1
Size: 28 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 758 Color: 1
Size: 148 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 682 Color: 1
Size: 136 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 882 Color: 1
Size: 172 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 290 Color: 1
Size: 56 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 1
Size: 581 Color: 1
Size: 114 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 1
Size: 324 Color: 1
Size: 56 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 1
Size: 562 Color: 1
Size: 112 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 1
Size: 892 Color: 1
Size: 176 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 1
Size: 546 Color: 1
Size: 68 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 1
Size: 761 Color: 1
Size: 152 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 1
Size: 883 Color: 1
Size: 176 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 874 Color: 1
Size: 172 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 1
Size: 305 Color: 1
Size: 60 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 1
Size: 882 Color: 1
Size: 176 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 1
Size: 220 Color: 1
Size: 40 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1985 Color: 1
Size: 471 Color: 1
Size: 8 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 1
Size: 428 Color: 1
Size: 80 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 1
Size: 356 Color: 1
Size: 64 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 1
Size: 1026 Color: 1
Size: 204 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 1
Size: 462 Color: 1
Size: 44 Color: 0

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 1
Size: 920 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 1
Size: 628 Color: 1
Size: 96 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 1
Size: 351 Color: 1
Size: 70 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 474 Color: 1
Size: 92 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1929 Color: 1
Size: 447 Color: 1
Size: 88 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1861 Color: 1
Size: 503 Color: 1
Size: 100 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 573 Color: 1
Size: 114 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 684 Color: 1
Size: 136 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1796 Color: 1
Size: 564 Color: 1
Size: 104 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2150 Color: 1
Size: 262 Color: 1
Size: 52 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 1
Size: 268 Color: 1
Size: 48 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1028 Color: 1
Size: 200 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2025 Color: 1
Size: 367 Color: 1
Size: 72 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 1
Size: 516 Color: 1
Size: 88 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 1
Size: 246 Color: 1
Size: 48 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 1
Size: 345 Color: 1
Size: 68 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 1
Size: 292 Color: 1
Size: 56 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 1
Size: 1025 Color: 1
Size: 204 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 1
Size: 322 Color: 1
Size: 64 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 1
Size: 846 Color: 1
Size: 168 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 1
Size: 780 Color: 1
Size: 152 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 1
Size: 222 Color: 1
Size: 44 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 1
Size: 571 Color: 1
Size: 112 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 1
Size: 443 Color: 1
Size: 24 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 1
Size: 354 Color: 1
Size: 68 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 1
Size: 1027 Color: 1
Size: 204 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1859 Color: 1
Size: 505 Color: 1
Size: 100 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 1
Size: 661 Color: 1
Size: 132 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 1
Size: 663 Color: 1
Size: 132 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2093 Color: 1
Size: 311 Color: 1
Size: 60 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2133 Color: 1
Size: 277 Color: 1
Size: 54 Color: 0

Total size: 160160
Total free space: 0

