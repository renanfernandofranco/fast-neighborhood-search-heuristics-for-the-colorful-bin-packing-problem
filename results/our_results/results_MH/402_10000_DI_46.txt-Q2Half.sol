Capicity Bin: 7360
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 5528 Color: 1
Size: 1480 Color: 1
Size: 192 Color: 0
Size: 128 Color: 0
Size: 32 Color: 0

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 2496 Color: 1
Size: 1696 Color: 1
Size: 1536 Color: 1
Size: 816 Color: 0
Size: 304 Color: 0
Size: 272 Color: 0
Size: 240 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 1
Size: 1588 Color: 1
Size: 312 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 1
Size: 675 Color: 1
Size: 134 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6622 Color: 1
Size: 618 Color: 1
Size: 120 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 1
Size: 1218 Color: 1
Size: 240 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6068 Color: 1
Size: 1276 Color: 1
Size: 16 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5987 Color: 1
Size: 1145 Color: 1
Size: 228 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5928 Color: 1
Size: 1208 Color: 1
Size: 224 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 1
Size: 702 Color: 1
Size: 136 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 1
Size: 1012 Color: 1
Size: 200 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5437 Color: 1
Size: 1603 Color: 1
Size: 320 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 1
Size: 1404 Color: 1
Size: 272 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 1
Size: 2872 Color: 1
Size: 560 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4596 Color: 1
Size: 2308 Color: 1
Size: 456 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 1
Size: 2262 Color: 1
Size: 452 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 1
Size: 1668 Color: 1
Size: 328 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6387 Color: 1
Size: 811 Color: 1
Size: 162 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 1
Size: 2118 Color: 1
Size: 420 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5464 Color: 1
Size: 1592 Color: 1
Size: 304 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4964 Color: 1
Size: 2132 Color: 1
Size: 264 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4610 Color: 1
Size: 2294 Color: 1
Size: 456 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4966 Color: 1
Size: 1998 Color: 1
Size: 396 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 1
Size: 2020 Color: 1
Size: 400 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 1
Size: 770 Color: 1
Size: 140 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 1
Size: 945 Color: 1
Size: 188 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5584 Color: 1
Size: 1488 Color: 1
Size: 288 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 1
Size: 932 Color: 1
Size: 184 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 1
Size: 951 Color: 1
Size: 188 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6168 Color: 1
Size: 1000 Color: 1
Size: 192 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6226 Color: 1
Size: 946 Color: 1
Size: 188 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 1
Size: 1586 Color: 1
Size: 296 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 1
Size: 714 Color: 1
Size: 140 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 1
Size: 2409 Color: 1
Size: 480 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 1
Size: 737 Color: 1
Size: 146 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 1
Size: 1784 Color: 1
Size: 352 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5390 Color: 1
Size: 1670 Color: 1
Size: 300 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5997 Color: 1
Size: 1137 Color: 1
Size: 226 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6070 Color: 1
Size: 1078 Color: 1
Size: 212 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 1
Size: 850 Color: 1
Size: 168 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 1
Size: 1972 Color: 1
Size: 392 Color: 0

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 4608 Color: 1
Size: 2752 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4628 Color: 1
Size: 2284 Color: 1
Size: 448 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6403 Color: 1
Size: 941 Color: 1
Size: 16 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6388 Color: 1
Size: 812 Color: 1
Size: 160 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5421 Color: 1
Size: 1617 Color: 1
Size: 322 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 1
Size: 1256 Color: 1
Size: 208 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 1
Size: 1386 Color: 1
Size: 276 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 1
Size: 934 Color: 1
Size: 184 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5635 Color: 1
Size: 1439 Color: 1
Size: 286 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5651 Color: 1
Size: 1425 Color: 1
Size: 284 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 1
Size: 646 Color: 1
Size: 128 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6508 Color: 1
Size: 716 Color: 1
Size: 136 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 1
Size: 1726 Color: 1
Size: 344 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3945 Color: 1
Size: 3291 Color: 1
Size: 124 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 1
Size: 1050 Color: 1
Size: 208 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 1
Size: 985 Color: 1
Size: 74 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 3941 Color: 1
Size: 2869 Color: 1
Size: 550 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 3684 Color: 1
Size: 3068 Color: 1
Size: 608 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 1
Size: 728 Color: 1
Size: 128 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 1
Size: 2712 Color: 1
Size: 528 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4857 Color: 1
Size: 2087 Color: 1
Size: 416 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 1
Size: 2511 Color: 1
Size: 394 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 1
Size: 1127 Color: 1
Size: 224 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5592 Color: 1
Size: 1528 Color: 1
Size: 240 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 1
Size: 958 Color: 1
Size: 48 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 1
Size: 630 Color: 1
Size: 124 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6434 Color: 1
Size: 826 Color: 1
Size: 100 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 1
Size: 2644 Color: 1
Size: 528 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 3688 Color: 1
Size: 3064 Color: 1
Size: 608 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 1
Size: 1268 Color: 1
Size: 248 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5086 Color: 1
Size: 1898 Color: 1
Size: 376 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 1
Size: 1206 Color: 1
Size: 240 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 1
Size: 1884 Color: 1
Size: 368 Color: 0

Bin 75: 0 of cap free
Amount of items: 5
Items: 
Size: 5088 Color: 1
Size: 1136 Color: 1
Size: 816 Color: 0
Size: 288 Color: 1
Size: 32 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 1
Size: 1031 Color: 1
Size: 204 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5336 Color: 1
Size: 1688 Color: 1
Size: 336 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 660 Color: 1
Size: 128 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 3681 Color: 1
Size: 3343 Color: 1
Size: 336 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6423 Color: 1
Size: 781 Color: 1
Size: 156 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5492 Color: 1
Size: 1564 Color: 1
Size: 304 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6418 Color: 1
Size: 786 Color: 1
Size: 156 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 1
Size: 1156 Color: 1
Size: 224 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 1
Size: 2568 Color: 1
Size: 512 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 1
Size: 1244 Color: 1
Size: 240 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5784 Color: 1
Size: 1320 Color: 1
Size: 256 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5724 Color: 1
Size: 1364 Color: 1
Size: 272 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 3682 Color: 1
Size: 3066 Color: 1
Size: 612 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 3961 Color: 1
Size: 2833 Color: 1
Size: 566 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 1
Size: 984 Color: 1
Size: 192 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5864 Color: 1
Size: 1224 Color: 1
Size: 272 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 1
Size: 2344 Color: 1
Size: 464 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6280 Color: 1
Size: 904 Color: 1
Size: 176 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 3997 Color: 1
Size: 2803 Color: 1
Size: 560 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6493 Color: 1
Size: 723 Color: 1
Size: 144 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5813 Color: 1
Size: 1291 Color: 1
Size: 256 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6131 Color: 1
Size: 1025 Color: 1
Size: 204 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 1
Size: 684 Color: 1
Size: 136 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6440 Color: 1
Size: 776 Color: 1
Size: 144 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6466 Color: 1
Size: 746 Color: 1
Size: 148 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 1
Size: 1324 Color: 1
Size: 32 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 1
Size: 1108 Color: 1
Size: 120 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6086 Color: 1
Size: 1062 Color: 1
Size: 212 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4449 Color: 1
Size: 2427 Color: 1
Size: 484 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6408 Color: 1
Size: 808 Color: 1
Size: 144 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 1
Size: 1374 Color: 1
Size: 272 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 1
Size: 1928 Color: 1
Size: 384 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 1
Size: 2168 Color: 1
Size: 416 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6324 Color: 1
Size: 868 Color: 1
Size: 168 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 1
Size: 3062 Color: 1
Size: 608 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6472 Color: 1
Size: 744 Color: 1
Size: 144 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6536 Color: 1
Size: 728 Color: 1
Size: 96 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 1
Size: 1096 Color: 1
Size: 208 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4841 Color: 1
Size: 2101 Color: 1
Size: 418 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 3925 Color: 1
Size: 2933 Color: 1
Size: 502 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4662 Color: 1
Size: 2494 Color: 1
Size: 204 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 1
Size: 1474 Color: 1
Size: 236 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 4578 Color: 1
Size: 2694 Color: 1
Size: 88 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 1
Size: 2642 Color: 1
Size: 528 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5827 Color: 1
Size: 1279 Color: 1
Size: 254 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6307 Color: 1
Size: 879 Color: 1
Size: 174 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 3692 Color: 1
Size: 3060 Color: 1
Size: 608 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5175 Color: 1
Size: 1821 Color: 1
Size: 364 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5165 Color: 1
Size: 1831 Color: 1
Size: 364 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6323 Color: 1
Size: 865 Color: 1
Size: 172 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6471 Color: 1
Size: 741 Color: 1
Size: 148 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 4825 Color: 1
Size: 2113 Color: 1
Size: 422 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 3909 Color: 1
Size: 2877 Color: 1
Size: 574 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 1
Size: 2652 Color: 1
Size: 528 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 3683 Color: 1
Size: 3261 Color: 1
Size: 416 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 1
Size: 2678 Color: 1
Size: 488 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 1
Size: 1265 Color: 1
Size: 252 Color: 0

Total size: 971520
Total free space: 0

