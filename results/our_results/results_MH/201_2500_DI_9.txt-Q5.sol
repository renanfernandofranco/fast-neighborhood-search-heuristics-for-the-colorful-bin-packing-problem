Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 0
Size: 662 Color: 3
Size: 452 Color: 3
Size: 68 Color: 0
Size: 48 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 2
Size: 882 Color: 1
Size: 176 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 0
Size: 882 Color: 4
Size: 172 Color: 1

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 0
Size: 920 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 3
Size: 785 Color: 2
Size: 120 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 1
Size: 604 Color: 0
Size: 40 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 3
Size: 504 Color: 2
Size: 76 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1862 Color: 4
Size: 558 Color: 1
Size: 44 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 3
Size: 484 Color: 2
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 414 Color: 3
Size: 80 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 1
Size: 365 Color: 1
Size: 72 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 0
Size: 378 Color: 2
Size: 72 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 2
Size: 315 Color: 4
Size: 104 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 308 Color: 2
Size: 88 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 2
Size: 314 Color: 2
Size: 60 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 3
Size: 200 Color: 4
Size: 94 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 4
Size: 236 Color: 3
Size: 48 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 4
Size: 238 Color: 0
Size: 44 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 1
Size: 252 Color: 1
Size: 18 Color: 3

Bin 20: 1 of cap free
Amount of items: 15
Items: 
Size: 299 Color: 3
Size: 272 Color: 3
Size: 272 Color: 3
Size: 244 Color: 2
Size: 244 Color: 1
Size: 148 Color: 0
Size: 136 Color: 2
Size: 136 Color: 1
Size: 128 Color: 1
Size: 124 Color: 1
Size: 120 Color: 4
Size: 100 Color: 4
Size: 96 Color: 4
Size: 96 Color: 1
Size: 48 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 869 Color: 3
Size: 176 Color: 2

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 2
Size: 941 Color: 1
Size: 72 Color: 2

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 4
Size: 684 Color: 1
Size: 136 Color: 3

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 4
Size: 758 Color: 3
Size: 48 Color: 4

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 3
Size: 617 Color: 1
Size: 48 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 4
Size: 502 Color: 2
Size: 64 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 3
Size: 384 Color: 4
Size: 110 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2107 Color: 3
Size: 204 Color: 4
Size: 152 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 2115 Color: 3
Size: 290 Color: 1
Size: 58 Color: 4

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 4
Size: 204 Color: 3
Size: 108 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 2167 Color: 4
Size: 152 Color: 3
Size: 144 Color: 4

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 2
Size: 291 Color: 4

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 0
Size: 249 Color: 2
Size: 26 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 4
Size: 840 Color: 3
Size: 226 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 0
Size: 874 Color: 3
Size: 56 Color: 4

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 2
Size: 576 Color: 3
Size: 176 Color: 0

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 2
Size: 561 Color: 4
Size: 108 Color: 1

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 3
Size: 413 Color: 1
Size: 200 Color: 0

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 3
Size: 372 Color: 0
Size: 102 Color: 1

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 3
Size: 685 Color: 4
Size: 58 Color: 2

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 2020 Color: 1
Size: 441 Color: 2

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1487 Color: 3
Size: 887 Color: 2
Size: 86 Color: 0

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 3
Size: 342 Color: 0

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 3
Size: 846 Color: 4
Size: 72 Color: 2

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 3
Size: 473 Color: 4
Size: 246 Color: 1

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1725 Color: 2
Size: 734 Color: 4

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 2001 Color: 3
Size: 458 Color: 4

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1918 Color: 0
Size: 540 Color: 1

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 2
Size: 404 Color: 4

Bin 50: 6 of cap free
Amount of items: 2
Items: 
Size: 2123 Color: 0
Size: 335 Color: 2

Bin 51: 7 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 3
Size: 1093 Color: 1
Size: 122 Color: 2

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 0
Size: 771 Color: 4
Size: 88 Color: 3

Bin 53: 10 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 2
Size: 780 Color: 1

Bin 54: 11 of cap free
Amount of items: 5
Items: 
Size: 2164 Color: 1
Size: 261 Color: 0
Size: 12 Color: 3
Size: 8 Color: 1
Size: 8 Color: 0

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 2100 Color: 3
Size: 351 Color: 2

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 0
Size: 892 Color: 4

Bin 57: 14 of cap free
Amount of items: 2
Items: 
Size: 1937 Color: 0
Size: 513 Color: 2

Bin 58: 14 of cap free
Amount of items: 2
Items: 
Size: 2063 Color: 0
Size: 387 Color: 4

Bin 59: 18 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 4
Size: 1022 Color: 4
Size: 188 Color: 2

Bin 60: 20 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 2
Size: 1027 Color: 4
Size: 82 Color: 3

Bin 61: 29 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 0
Size: 630 Color: 1
Size: 332 Color: 4
Size: 168 Color: 0
Size: 72 Color: 3

Bin 62: 31 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 0
Size: 1026 Color: 0
Size: 172 Color: 1

Bin 63: 31 of cap free
Amount of items: 2
Items: 
Size: 1644 Color: 1
Size: 789 Color: 2

Bin 64: 35 of cap free
Amount of items: 2
Items: 
Size: 1401 Color: 3
Size: 1028 Color: 2

Bin 65: 46 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 1
Size: 722 Color: 2

Bin 66: 2086 of cap free
Amount of items: 6
Items: 
Size: 72 Color: 3
Size: 68 Color: 4
Size: 66 Color: 4
Size: 64 Color: 2
Size: 56 Color: 3
Size: 52 Color: 2

Total size: 160160
Total free space: 2464

