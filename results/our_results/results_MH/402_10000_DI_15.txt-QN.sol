Capicity Bin: 9984
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4944 Color: 272
Size: 1392 Color: 173
Size: 1248 Color: 164
Size: 784 Color: 127
Size: 624 Color: 111
Size: 544 Color: 102
Size: 448 Color: 87

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 8000 Color: 351
Size: 1488 Color: 181
Size: 176 Color: 17
Size: 128 Color: 10
Size: 128 Color: 9
Size: 64 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 295
Size: 3244 Color: 247
Size: 640 Color: 116

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 303
Size: 3036 Color: 237
Size: 600 Color: 106

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8284 Color: 363
Size: 1420 Color: 176
Size: 280 Color: 51

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8078 Color: 355
Size: 1590 Color: 186
Size: 316 Color: 60

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7420 Color: 328
Size: 2332 Color: 218
Size: 232 Color: 37

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7401 Color: 326
Size: 2153 Color: 215
Size: 430 Color: 85

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 299
Size: 3126 Color: 243
Size: 624 Color: 113

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7608 Color: 334
Size: 2072 Color: 209
Size: 304 Color: 58

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 316
Size: 2812 Color: 233
Size: 200 Color: 25

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5686 Color: 289
Size: 3582 Color: 253
Size: 716 Color: 121

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7979 Color: 346
Size: 1671 Color: 193
Size: 334 Color: 67

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7484 Color: 332
Size: 2084 Color: 210
Size: 416 Color: 81

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7620 Color: 335
Size: 2028 Color: 207
Size: 336 Color: 68

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 360
Size: 1484 Color: 180
Size: 288 Color: 54

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7741 Color: 339
Size: 1871 Color: 201
Size: 372 Color: 75

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8888 Color: 397
Size: 920 Color: 142
Size: 176 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8728 Color: 390
Size: 1144 Color: 157
Size: 112 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8742 Color: 391
Size: 1038 Color: 148
Size: 204 Color: 26

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8023 Color: 352
Size: 1845 Color: 199
Size: 116 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 375
Size: 1426 Color: 177
Size: 44 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8515 Color: 376
Size: 1225 Color: 162
Size: 244 Color: 39

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5002 Color: 279
Size: 4210 Color: 268
Size: 772 Color: 125

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 291
Size: 3528 Color: 250
Size: 688 Color: 118

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8976 Color: 402
Size: 848 Color: 137
Size: 160 Color: 12

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8962 Color: 400
Size: 854 Color: 138
Size: 168 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8227 Color: 361
Size: 1465 Color: 179
Size: 292 Color: 56

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7982 Color: 347
Size: 1670 Color: 192
Size: 332 Color: 66

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7864 Color: 343
Size: 1768 Color: 196
Size: 352 Color: 71

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8708 Color: 389
Size: 1068 Color: 149
Size: 208 Color: 27

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6753 Color: 311
Size: 3063 Color: 239
Size: 168 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6742 Color: 310
Size: 2702 Color: 229
Size: 540 Color: 100

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 359
Size: 1496 Color: 182
Size: 288 Color: 55

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 275
Size: 4892 Color: 271
Size: 96 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7509 Color: 333
Size: 2063 Color: 208
Size: 412 Color: 79

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7690 Color: 337
Size: 1914 Color: 204
Size: 380 Color: 77

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8918 Color: 399
Size: 890 Color: 140
Size: 176 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7933 Color: 345
Size: 1711 Color: 194
Size: 340 Color: 69

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8664 Color: 387
Size: 1112 Color: 153
Size: 208 Color: 28

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7130 Color: 322
Size: 2382 Color: 220
Size: 472 Color: 91

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5001 Color: 278
Size: 4153 Color: 262
Size: 830 Color: 136

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7678 Color: 336
Size: 1922 Color: 205
Size: 384 Color: 78

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 8289 Color: 365
Size: 1619 Color: 187
Size: 76 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 8641 Color: 381
Size: 1121 Color: 155
Size: 222 Color: 34

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 8810 Color: 393
Size: 982 Color: 146
Size: 192 Color: 22

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 8900 Color: 398
Size: 908 Color: 141
Size: 176 Color: 19

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5010 Color: 282
Size: 4146 Color: 260
Size: 828 Color: 132

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5005 Color: 281
Size: 4151 Color: 261
Size: 828 Color: 131

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4997 Color: 276
Size: 4157 Color: 264
Size: 830 Color: 134

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 314
Size: 2600 Color: 226
Size: 512 Color: 96

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 8676 Color: 388
Size: 1092 Color: 150
Size: 216 Color: 29

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 8348 Color: 368
Size: 1364 Color: 170
Size: 272 Color: 49

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 7990 Color: 349
Size: 1978 Color: 206
Size: 16 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 8104 Color: 358
Size: 1576 Color: 183
Size: 304 Color: 57

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 8036 Color: 353
Size: 1628 Color: 189
Size: 320 Color: 62

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7434 Color: 330
Size: 2126 Color: 212
Size: 424 Color: 82

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 8086 Color: 356
Size: 1582 Color: 185
Size: 316 Color: 61

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 308
Size: 2718 Color: 231
Size: 540 Color: 101

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 312
Size: 2690 Color: 228
Size: 536 Color: 99

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7422 Color: 329
Size: 2138 Color: 213
Size: 424 Color: 83

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6785 Color: 313
Size: 2667 Color: 227
Size: 532 Color: 97

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 286
Size: 3796 Color: 257
Size: 752 Color: 124

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7914 Color: 344
Size: 1726 Color: 195
Size: 344 Color: 70

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5000 Color: 277
Size: 4168 Color: 267
Size: 816 Color: 128

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 8476 Color: 373
Size: 1260 Color: 165
Size: 248 Color: 41

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7768 Color: 340
Size: 1848 Color: 200
Size: 368 Color: 74

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 309
Size: 2716 Color: 230
Size: 536 Color: 98

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5670 Color: 288
Size: 3598 Color: 254
Size: 716 Color: 122

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8039 Color: 354
Size: 1621 Color: 188
Size: 324 Color: 63

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 304
Size: 2968 Color: 236
Size: 592 Color: 105

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7000 Color: 317
Size: 2488 Color: 225
Size: 496 Color: 95

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 287
Size: 3611 Color: 255
Size: 720 Color: 123

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 8836 Color: 395
Size: 964 Color: 144
Size: 184 Color: 21

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5256 Color: 285
Size: 3944 Color: 258
Size: 784 Color: 126

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 7112 Color: 320
Size: 2408 Color: 222
Size: 464 Color: 89

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 315
Size: 2868 Color: 235
Size: 176 Color: 18

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6262 Color: 301
Size: 3102 Color: 241
Size: 620 Color: 109

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 305
Size: 2860 Color: 234
Size: 568 Color: 104

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 8812 Color: 394
Size: 980 Color: 145
Size: 192 Color: 24

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 300
Size: 3118 Color: 242
Size: 620 Color: 110

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7464 Color: 331
Size: 2104 Color: 211
Size: 416 Color: 80

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 4384 Color: 269
Size: 3696 Color: 256
Size: 1904 Color: 203

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 8645 Color: 384
Size: 1117 Color: 154
Size: 222 Color: 33

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7706 Color: 338
Size: 1902 Color: 202
Size: 376 Color: 76

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 8499 Color: 374
Size: 1239 Color: 163
Size: 246 Color: 40

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 377
Size: 1224 Color: 161
Size: 240 Color: 38

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 8868 Color: 396
Size: 932 Color: 143
Size: 184 Color: 20

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 7417 Color: 327
Size: 2141 Color: 214
Size: 426 Color: 84

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 318
Size: 2460 Color: 224
Size: 488 Color: 94

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5020 Color: 283
Size: 4716 Color: 270
Size: 248 Color: 42

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 8540 Color: 378
Size: 1412 Color: 174
Size: 32 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 8418 Color: 371
Size: 1306 Color: 167
Size: 260 Color: 45

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 370
Size: 1314 Color: 168
Size: 260 Color: 46

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 8426 Color: 372
Size: 1302 Color: 166
Size: 256 Color: 44

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 7063 Color: 319
Size: 2435 Color: 223
Size: 486 Color: 93

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 8094 Color: 357
Size: 1578 Color: 184
Size: 312 Color: 59

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 306
Size: 2785 Color: 232
Size: 556 Color: 103

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 7114 Color: 321
Size: 2394 Color: 221
Size: 476 Color: 92

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 8642 Color: 382
Size: 1190 Color: 159
Size: 152 Color: 11

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 7988 Color: 348
Size: 1668 Color: 191
Size: 328 Color: 64

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 8604 Color: 380
Size: 1156 Color: 158
Size: 224 Color: 36

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 7995 Color: 350
Size: 1659 Color: 190
Size: 330 Color: 65

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 284
Size: 4134 Color: 259
Size: 824 Color: 129

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 294
Size: 3416 Color: 248
Size: 672 Color: 117

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 8238 Color: 362
Size: 1458 Color: 178
Size: 288 Color: 53

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 8329 Color: 366
Size: 1381 Color: 172
Size: 274 Color: 50

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7812 Color: 342
Size: 1812 Color: 197
Size: 360 Color: 72

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 401
Size: 888 Color: 139
Size: 128 Color: 8

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 8285 Color: 364
Size: 1417 Color: 175
Size: 282 Color: 52

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 280
Size: 4156 Color: 263
Size: 824 Color: 130

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 7775 Color: 341
Size: 1841 Color: 198
Size: 368 Color: 73

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 8376 Color: 369
Size: 1352 Color: 169
Size: 256 Color: 43

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 296
Size: 3176 Color: 246
Size: 624 Color: 112

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4993 Color: 273
Size: 4161 Color: 265
Size: 830 Color: 135

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 8644 Color: 383
Size: 1124 Color: 156
Size: 216 Color: 30

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4994 Color: 274
Size: 4162 Color: 266
Size: 828 Color: 133

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 324
Size: 2284 Color: 217
Size: 456 Color: 88

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 392
Size: 1000 Color: 147
Size: 192 Color: 23

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5796 Color: 292
Size: 3492 Color: 249
Size: 696 Color: 119

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 307
Size: 3061 Color: 238
Size: 266 Color: 47

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5702 Color: 290
Size: 3570 Color: 252
Size: 712 Color: 120

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6278 Color: 302
Size: 3090 Color: 240
Size: 616 Color: 108

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 8552 Color: 379
Size: 1208 Color: 160
Size: 224 Color: 35

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 8341 Color: 367
Size: 1371 Color: 171
Size: 272 Color: 48

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5803 Color: 293
Size: 3565 Color: 251
Size: 616 Color: 107

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6209 Color: 298
Size: 3147 Color: 244
Size: 628 Color: 114

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 7147 Color: 323
Size: 2365 Color: 219
Size: 472 Color: 90

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 8658 Color: 386
Size: 1106 Color: 151
Size: 220 Color: 32

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 7369 Color: 325
Size: 2181 Color: 216
Size: 434 Color: 86

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6193 Color: 297
Size: 3161 Color: 245
Size: 630 Color: 115

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 8657 Color: 385
Size: 1107 Color: 152
Size: 220 Color: 31

Total size: 1317888
Total free space: 0

