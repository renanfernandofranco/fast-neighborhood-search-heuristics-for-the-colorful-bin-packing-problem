Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 274 Color: 6
Size: 254 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 2
Size: 275 Color: 5
Size: 252 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 266 Color: 8
Size: 260 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 330 Color: 18
Size: 308 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 290 Color: 15
Size: 285 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 12
Size: 312 Color: 15
Size: 269 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 6
Size: 251 Color: 11
Size: 250 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8
Size: 337 Color: 1
Size: 251 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 265 Color: 0
Size: 252 Color: 11
Size: 484 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 12
Size: 339 Color: 18
Size: 296 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 10
Size: 331 Color: 9
Size: 329 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8
Size: 310 Color: 0
Size: 268 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 5
Size: 352 Color: 2
Size: 276 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 13
Size: 326 Color: 1
Size: 312 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 278 Color: 4
Size: 274 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 319 Color: 10
Size: 256 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 313 Color: 7
Size: 312 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 19
Size: 359 Color: 3
Size: 253 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 270 Color: 12
Size: 263 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 5
Size: 312 Color: 9
Size: 289 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 8
Size: 338 Color: 12
Size: 273 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 263 Color: 17
Size: 251 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 17
Size: 333 Color: 19
Size: 250 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 16
Size: 333 Color: 11
Size: 264 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 13
Size: 302 Color: 2
Size: 291 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 17
Size: 291 Color: 0
Size: 275 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 6
Size: 285 Color: 7
Size: 283 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 16
Size: 370 Color: 2
Size: 251 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 328 Color: 16
Size: 295 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 7
Size: 339 Color: 17
Size: 259 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 16
Size: 333 Color: 4
Size: 331 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8
Size: 294 Color: 7
Size: 283 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 17
Size: 330 Color: 18
Size: 329 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 15
Size: 285 Color: 11
Size: 250 Color: 1

Total size: 34034
Total free space: 0

