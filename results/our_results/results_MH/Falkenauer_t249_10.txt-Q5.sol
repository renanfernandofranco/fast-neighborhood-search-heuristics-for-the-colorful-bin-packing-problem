Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 255 Color: 4
Size: 253 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 259 Color: 4
Size: 252 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 296 Color: 4
Size: 280 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 258 Color: 3
Size: 254 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 4
Size: 313 Color: 3
Size: 251 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 265 Color: 4
Size: 258 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 257 Color: 4
Size: 250 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 279 Color: 2
Size: 260 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 294 Color: 4
Size: 284 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 266 Color: 4
Size: 263 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 2
Size: 319 Color: 1
Size: 292 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 294 Color: 1
Size: 258 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 350 Color: 0
Size: 267 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 352 Color: 1
Size: 265 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 288 Color: 1
Size: 260 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 3
Size: 281 Color: 0
Size: 269 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 360 Color: 2
Size: 275 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 304 Color: 3
Size: 291 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 4
Size: 357 Color: 3
Size: 284 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 274 Color: 2
Size: 269 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 269 Color: 4
Size: 250 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 352 Color: 3
Size: 250 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 305 Color: 4
Size: 279 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 265 Color: 3
Size: 260 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 313 Color: 1
Size: 310 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 272 Color: 3
Size: 269 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 266 Color: 0
Size: 261 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 0
Size: 307 Color: 2
Size: 300 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 301 Color: 3
Size: 290 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 324 Color: 4
Size: 287 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 265 Color: 0
Size: 255 Color: 2
Size: 480 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 312 Color: 0
Size: 251 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 3
Size: 330 Color: 1
Size: 277 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 303 Color: 4
Size: 261 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 302 Color: 0
Size: 280 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 366 Color: 2
Size: 260 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 293 Color: 0
Size: 292 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 321 Color: 1
Size: 277 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 302 Color: 4
Size: 264 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 345 Color: 0
Size: 257 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 289 Color: 2
Size: 252 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 251 Color: 4
Size: 250 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 274 Color: 0
Size: 261 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 323 Color: 1
Size: 281 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 3
Size: 279 Color: 1
Size: 250 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 363 Color: 0
Size: 264 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 4
Size: 362 Color: 3
Size: 276 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 290 Color: 1
Size: 261 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 0
Size: 347 Color: 0
Size: 299 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 348 Color: 0
Size: 278 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 308 Color: 4
Size: 251 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 352 Color: 2
Size: 263 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 326 Color: 3
Size: 274 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 4
Size: 294 Color: 3
Size: 266 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 339 Color: 3
Size: 284 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 288 Color: 2
Size: 251 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 264 Color: 3
Size: 250 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 287 Color: 1
Size: 268 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 4
Size: 257 Color: 0
Size: 256 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 263 Color: 4
Size: 260 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 4
Size: 262 Color: 3
Size: 261 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 315 Color: 2
Size: 295 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 305 Color: 2
Size: 296 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 255 Color: 2
Size: 253 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 267 Color: 3
Size: 252 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 275 Color: 3
Size: 250 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 326 Color: 1
Size: 303 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 3
Size: 324 Color: 2
Size: 318 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 263 Color: 4
Size: 254 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 366 Color: 4
Size: 260 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 305 Color: 0
Size: 287 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 309 Color: 2
Size: 296 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 349 Color: 3
Size: 254 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 329 Color: 4
Size: 261 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 300 Color: 4
Size: 257 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 271 Color: 2
Size: 271 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 275 Color: 1
Size: 270 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 272 Color: 4
Size: 254 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 279 Color: 2
Size: 260 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 267 Color: 2
Size: 261 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 265 Color: 1
Size: 250 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 264 Color: 0
Size: 251 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 4
Size: 255 Color: 0
Size: 251 Color: 1

Total size: 83000
Total free space: 0

