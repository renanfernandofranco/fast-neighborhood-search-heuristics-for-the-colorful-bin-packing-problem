Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 11
Size: 260 Color: 1
Size: 250 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 315 Color: 7
Size: 258 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 13
Size: 363 Color: 8
Size: 262 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 1
Size: 330 Color: 0
Size: 328 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 16
Size: 265 Color: 5
Size: 251 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 306 Color: 12
Size: 268 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 357 Color: 18
Size: 255 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 11
Size: 308 Color: 9
Size: 256 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 6
Size: 372 Color: 5
Size: 254 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 273 Color: 7
Size: 257 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 7
Size: 310 Color: 4
Size: 254 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 17
Size: 270 Color: 19
Size: 268 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 18
Size: 290 Color: 19
Size: 287 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 17
Size: 349 Color: 14
Size: 276 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7
Size: 352 Color: 4
Size: 276 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 11
Size: 323 Color: 4
Size: 274 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 7
Size: 258 Color: 8
Size: 252 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 364 Color: 16
Size: 250 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 6
Size: 308 Color: 12
Size: 250 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 5
Size: 266 Color: 8
Size: 262 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 7
Size: 294 Color: 8
Size: 273 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 18
Size: 312 Color: 0
Size: 269 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 5
Size: 318 Color: 18
Size: 305 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 14
Size: 285 Color: 10
Size: 270 Color: 5

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 10
Size: 329 Color: 1
Size: 275 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 302 Color: 14
Size: 276 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 19
Size: 336 Color: 14
Size: 254 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 7
Size: 252 Color: 17
Size: 252 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7
Size: 352 Color: 18
Size: 275 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 16
Size: 262 Color: 9
Size: 253 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9
Size: 272 Color: 9
Size: 270 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 17
Size: 254 Color: 6
Size: 253 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 334 Color: 13
Size: 258 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 5
Size: 302 Color: 17
Size: 252 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 11
Size: 335 Color: 4
Size: 263 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 8
Size: 278 Color: 18
Size: 276 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 12
Size: 306 Color: 0
Size: 280 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 12
Size: 339 Color: 16
Size: 274 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 253 Color: 2
Size: 250 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 2
Size: 347 Color: 17
Size: 292 Color: 18

Total size: 40000
Total free space: 0

