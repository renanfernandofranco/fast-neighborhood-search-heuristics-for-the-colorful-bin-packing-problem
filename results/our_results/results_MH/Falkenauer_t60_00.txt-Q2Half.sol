Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 347 Color: 1
Size: 258 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 292 Color: 1
Size: 269 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 298 Color: 1
Size: 252 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 255 Color: 0
Size: 272 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 357 Color: 1
Size: 271 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 251 Color: 0
Size: 254 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 315 Color: 1
Size: 275 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 307 Color: 1
Size: 263 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 320 Color: 1
Size: 261 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 298 Color: 1
Size: 288 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 350 Color: 1
Size: 299 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 272 Color: 1
Size: 262 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 350 Color: 1
Size: 287 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 283 Color: 1
Size: 273 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 355 Color: 1
Size: 275 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 269 Color: 1
Size: 259 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 274 Color: 1
Size: 252 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 361 Color: 1
Size: 273 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 303 Color: 1
Size: 252 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 366 Color: 1
Size: 268 Color: 0

Total size: 20000
Total free space: 0

