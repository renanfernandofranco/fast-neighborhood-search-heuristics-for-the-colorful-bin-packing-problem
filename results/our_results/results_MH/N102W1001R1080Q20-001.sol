Capicity Bin: 1001
Lower Bound: 44

Bins used: 44
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 5
Size: 250 Color: 3
Size: 137 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 3
Size: 273 Color: 11
Size: 109 Color: 0

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 18
Size: 342 Color: 17

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 5

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 7
Size: 230 Color: 14

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 464 Color: 5

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 17
Size: 318 Color: 6

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 1
Size: 308 Color: 8

Bin 9: 2 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 244 Color: 6

Bin 10: 3 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 15
Size: 390 Color: 12
Size: 164 Color: 13

Bin 11: 3 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 19
Size: 402 Color: 17

Bin 12: 3 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 7
Size: 197 Color: 14

Bin 13: 4 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 14
Size: 348 Color: 0

Bin 14: 4 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 5
Size: 279 Color: 14

Bin 15: 5 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 3
Size: 299 Color: 0
Size: 135 Color: 7

Bin 16: 6 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 12
Size: 485 Color: 19

Bin 17: 6 of cap free
Amount of items: 3
Items: 
Size: 544 Color: 7
Size: 298 Color: 16
Size: 153 Color: 19

Bin 18: 6 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 6
Size: 379 Color: 9

Bin 19: 6 of cap free
Amount of items: 3
Items: 
Size: 625 Color: 9
Size: 259 Color: 19
Size: 111 Color: 15

Bin 20: 6 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 12
Size: 299 Color: 14

Bin 21: 7 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 1
Size: 211 Color: 0

Bin 22: 8 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 18
Size: 451 Color: 5

Bin 23: 8 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 5
Size: 421 Color: 15

Bin 24: 9 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 17
Size: 312 Color: 16
Size: 294 Color: 13

Bin 25: 9 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 265 Color: 5
Size: 264 Color: 0

Bin 26: 10 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 8
Size: 211 Color: 1

Bin 27: 11 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 7
Size: 190 Color: 11

Bin 28: 12 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 4
Size: 314 Color: 15
Size: 119 Color: 8

Bin 29: 13 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 6
Size: 484 Color: 15

Bin 30: 14 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 293 Color: 4
Size: 257 Color: 1

Bin 31: 14 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 2
Size: 270 Color: 0
Size: 184 Color: 10

Bin 32: 14 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 7
Size: 187 Color: 13

Bin 33: 16 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 2
Size: 226 Color: 10

Bin 34: 18 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 16
Size: 184 Color: 12

Bin 35: 24 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 9
Size: 334 Color: 15

Bin 36: 26 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 6
Size: 343 Color: 16
Size: 183 Color: 9

Bin 37: 26 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 326 Color: 5

Bin 38: 33 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 11
Size: 345 Color: 6

Bin 39: 34 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 16
Size: 344 Color: 19
Size: 104 Color: 8

Bin 40: 36 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 6
Size: 206 Color: 15

Bin 41: 44 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 9
Size: 289 Color: 17
Size: 257 Color: 10

Bin 42: 44 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 8
Size: 478 Color: 14

Bin 43: 54 of cap free
Amount of items: 2
Items: 
Size: 476 Color: 1
Size: 471 Color: 8

Bin 44: 440 of cap free
Amount of items: 2
Items: 
Size: 412 Color: 11
Size: 149 Color: 19

Total size: 43063
Total free space: 981

