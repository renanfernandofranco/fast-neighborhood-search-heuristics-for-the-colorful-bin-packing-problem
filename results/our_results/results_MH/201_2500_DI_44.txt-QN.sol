Capicity Bin: 2036
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 684 Color: 125
Size: 424 Color: 108
Size: 316 Color: 95
Size: 228 Color: 82
Size: 228 Color: 81
Size: 64 Color: 32
Size: 48 Color: 22
Size: 40 Color: 16
Size: 4 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 142
Size: 721 Color: 131
Size: 144 Color: 64

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 140
Size: 846 Color: 135
Size: 168 Color: 66

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 149
Size: 678 Color: 124
Size: 132 Color: 58

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1302 Color: 151
Size: 650 Color: 123
Size: 64 Color: 34
Size: 20 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 173
Size: 366 Color: 100
Size: 72 Color: 37

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 200
Size: 178 Color: 70
Size: 32 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 179
Size: 334 Color: 98
Size: 28 Color: 3

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 1404 Color: 156
Size: 472 Color: 110
Size: 84 Color: 45
Size: 76 Color: 40

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 167
Size: 402 Color: 105
Size: 76 Color: 38

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 152
Size: 591 Color: 121
Size: 118 Color: 56

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 163
Size: 481 Color: 111
Size: 96 Color: 48

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 198
Size: 194 Color: 73
Size: 36 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 186
Size: 243 Color: 85
Size: 48 Color: 21

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 162
Size: 482 Color: 112
Size: 96 Color: 47

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 187
Size: 242 Color: 84
Size: 48 Color: 23

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 172
Size: 382 Color: 101
Size: 76 Color: 39

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 148
Size: 691 Color: 126
Size: 136 Color: 59

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 184
Size: 278 Color: 88
Size: 52 Color: 27

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1173 Color: 143
Size: 721 Color: 132
Size: 142 Color: 61

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1498 Color: 164
Size: 450 Color: 109
Size: 88 Color: 46

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 158
Size: 507 Color: 117
Size: 100 Color: 52

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 145
Size: 713 Color: 129
Size: 142 Color: 63

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 154
Size: 581 Color: 119
Size: 116 Color: 55

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 171
Size: 417 Color: 107
Size: 48 Color: 24

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 176
Size: 323 Color: 96
Size: 64 Color: 33

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 194
Size: 214 Color: 77
Size: 40 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 144
Size: 717 Color: 130
Size: 142 Color: 62

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 139
Size: 847 Color: 136
Size: 168 Color: 68

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1134 Color: 141
Size: 754 Color: 133
Size: 148 Color: 65

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 157
Size: 522 Color: 118
Size: 100 Color: 53

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 175
Size: 334 Color: 97
Size: 64 Color: 35

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 138
Size: 849 Color: 137
Size: 168 Color: 67

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 146
Size: 713 Color: 128
Size: 130 Color: 57

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1752 Color: 188
Size: 244 Color: 86
Size: 40 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 192
Size: 218 Color: 79
Size: 40 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 155
Size: 606 Color: 122
Size: 40 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 199
Size: 191 Color: 71
Size: 36 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 165
Size: 493 Color: 114
Size: 4 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 183
Size: 289 Color: 89
Size: 48 Color: 25

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 147
Size: 701 Color: 127
Size: 138 Color: 60

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 153
Size: 590 Color: 120
Size: 116 Color: 54

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 190
Size: 220 Color: 80
Size: 40 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 182
Size: 294 Color: 90
Size: 56 Color: 28

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 1280 Color: 150
Size: 756 Color: 134

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 181
Size: 295 Color: 91
Size: 58 Color: 29

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 185
Size: 257 Color: 87
Size: 50 Color: 26

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 159
Size: 499 Color: 116
Size: 98 Color: 49

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 166
Size: 407 Color: 106
Size: 80 Color: 44

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 161
Size: 491 Color: 113
Size: 98 Color: 50

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 189
Size: 229 Color: 83
Size: 44 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 174
Size: 361 Color: 99
Size: 72 Color: 36

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 191
Size: 217 Color: 78
Size: 42 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 195
Size: 201 Color: 75
Size: 38 Color: 9

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 197
Size: 193 Color: 72
Size: 38 Color: 8

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 169
Size: 395 Color: 103
Size: 78 Color: 41

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 180
Size: 311 Color: 92
Size: 46 Color: 20

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 160
Size: 495 Color: 115
Size: 98 Color: 51

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 178
Size: 311 Color: 93
Size: 62 Color: 31

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 168
Size: 399 Color: 104
Size: 78 Color: 43

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 170
Size: 391 Color: 102
Size: 78 Color: 42

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 196
Size: 197 Color: 74
Size: 38 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 177
Size: 315 Color: 94
Size: 62 Color: 30

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 193
Size: 213 Color: 76
Size: 42 Color: 17

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 201
Size: 173 Color: 69
Size: 34 Color: 5

Total size: 132340
Total free space: 0

