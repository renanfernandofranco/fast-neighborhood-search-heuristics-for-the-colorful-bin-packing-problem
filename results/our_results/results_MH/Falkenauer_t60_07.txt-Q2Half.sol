Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 362 Color: 1
Size: 273 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 314 Color: 1
Size: 279 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 260 Color: 0
Size: 264 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 297 Color: 1
Size: 293 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 250 Color: 0
Size: 263 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 324 Color: 1
Size: 266 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 327 Color: 1
Size: 281 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 303 Color: 1
Size: 265 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 252 Color: 0
Size: 268 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 322 Color: 1
Size: 256 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 267 Color: 1
Size: 255 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 365 Color: 1
Size: 255 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 329 Color: 1
Size: 318 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 366 Color: 1
Size: 261 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 326 Color: 1
Size: 296 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 370 Color: 1
Size: 260 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 286 Color: 1
Size: 260 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 307 Color: 1
Size: 281 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 330 Color: 1
Size: 264 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 279 Color: 1
Size: 256 Color: 0

Total size: 20000
Total free space: 0

