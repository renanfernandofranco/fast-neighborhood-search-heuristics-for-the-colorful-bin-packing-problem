Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 1
Size: 250 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 102
Size: 296 Color: 57
Size: 257 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 82
Size: 314 Color: 67
Size: 308 Color: 65

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 109
Size: 279 Color: 42
Size: 251 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 117
Size: 254 Color: 10
Size: 251 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 115
Size: 258 Color: 18
Size: 253 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 97
Size: 289 Color: 54
Size: 279 Color: 43

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 112
Size: 269 Color: 29
Size: 257 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 106
Size: 288 Color: 52
Size: 259 Color: 20

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 94
Size: 322 Color: 71
Size: 256 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 103
Size: 296 Color: 58
Size: 257 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 301 Color: 61
Size: 257 Color: 16
Size: 442 Color: 100

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 101
Size: 283 Color: 48
Size: 271 Color: 34

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 110
Size: 275 Color: 40
Size: 254 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 93
Size: 321 Color: 70
Size: 263 Color: 24

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 84
Size: 337 Color: 76
Size: 279 Color: 45

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 113
Size: 270 Color: 33
Size: 254 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 81
Size: 319 Color: 68
Size: 304 Color: 62

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 83
Size: 359 Color: 79
Size: 262 Color: 23

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 104
Size: 285 Color: 50
Size: 266 Color: 25

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 280 Color: 46
Size: 266 Color: 26
Size: 454 Color: 107

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 92
Size: 314 Color: 66
Size: 272 Color: 35

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 89
Size: 323 Color: 72
Size: 279 Color: 44

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 111
Size: 267 Color: 27
Size: 260 Color: 22

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 99
Size: 306 Color: 63
Size: 259 Color: 21

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 95
Size: 292 Color: 55
Size: 286 Color: 51

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 116
Size: 255 Color: 11
Size: 253 Color: 7

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 108
Size: 274 Color: 37
Size: 270 Color: 32

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 98
Size: 294 Color: 56
Size: 273 Color: 36

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 105
Size: 276 Color: 41
Size: 274 Color: 38

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 118
Size: 252 Color: 5
Size: 252 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 86
Size: 330 Color: 74
Size: 282 Color: 47

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 85
Size: 357 Color: 78
Size: 258 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 114
Size: 267 Color: 28
Size: 256 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 87
Size: 319 Color: 69
Size: 285 Color: 49

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 80
Size: 352 Color: 77
Size: 288 Color: 53

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 96
Size: 300 Color: 60
Size: 269 Color: 30

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 88
Size: 307 Color: 64
Size: 296 Color: 59

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 90
Size: 332 Color: 75
Size: 269 Color: 31

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 91
Size: 324 Color: 73
Size: 275 Color: 39

Total size: 40000
Total free space: 0

