Capicity Bin: 8080
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4044 Color: 1
Size: 3364 Color: 1
Size: 672 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4046 Color: 1
Size: 3362 Color: 1
Size: 672 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 1
Size: 3364 Color: 1
Size: 664 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 1
Size: 3332 Color: 1
Size: 664 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4312 Color: 1
Size: 3144 Color: 1
Size: 624 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4744 Color: 1
Size: 2792 Color: 1
Size: 544 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5113 Color: 1
Size: 2473 Color: 1
Size: 494 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 1
Size: 2442 Color: 1
Size: 496 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 2444 Color: 1
Size: 480 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 1
Size: 2792 Color: 1
Size: 128 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5170 Color: 1
Size: 2426 Color: 1
Size: 484 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5186 Color: 1
Size: 2414 Color: 1
Size: 480 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 1
Size: 2408 Color: 1
Size: 480 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5217 Color: 1
Size: 2387 Color: 1
Size: 476 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5448 Color: 1
Size: 2200 Color: 1
Size: 432 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 1
Size: 2132 Color: 1
Size: 424 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5577 Color: 1
Size: 2087 Color: 1
Size: 416 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 1
Size: 1928 Color: 1
Size: 558 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 1
Size: 2062 Color: 1
Size: 408 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5624 Color: 1
Size: 1800 Color: 1
Size: 656 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5735 Color: 1
Size: 1941 Color: 1
Size: 404 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5751 Color: 1
Size: 1769 Color: 1
Size: 560 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 1
Size: 2056 Color: 1
Size: 256 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 1
Size: 1876 Color: 1
Size: 368 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5934 Color: 1
Size: 1660 Color: 1
Size: 486 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 1
Size: 1856 Color: 1
Size: 274 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5959 Color: 1
Size: 1733 Color: 1
Size: 388 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6001 Color: 1
Size: 1727 Color: 1
Size: 352 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 1
Size: 1852 Color: 1
Size: 172 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6092 Color: 1
Size: 1812 Color: 1
Size: 176 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 1
Size: 1688 Color: 1
Size: 268 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 1
Size: 1604 Color: 1
Size: 320 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6168 Color: 1
Size: 1608 Color: 1
Size: 304 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 1
Size: 1591 Color: 1
Size: 316 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6218 Color: 1
Size: 1554 Color: 1
Size: 308 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 1
Size: 1542 Color: 1
Size: 304 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6239 Color: 1
Size: 1535 Color: 1
Size: 306 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6255 Color: 1
Size: 1521 Color: 1
Size: 304 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 1
Size: 1484 Color: 1
Size: 288 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6365 Color: 1
Size: 1431 Color: 1
Size: 284 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6376 Color: 1
Size: 1432 Color: 1
Size: 272 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 1
Size: 1419 Color: 1
Size: 282 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 1
Size: 1272 Color: 1
Size: 412 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 1
Size: 1400 Color: 1
Size: 256 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 1
Size: 1307 Color: 1
Size: 346 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 1
Size: 1436 Color: 1
Size: 216 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 1
Size: 1386 Color: 1
Size: 240 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6486 Color: 1
Size: 1330 Color: 1
Size: 264 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 1
Size: 1436 Color: 1
Size: 152 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 1
Size: 1404 Color: 1
Size: 152 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 1
Size: 1201 Color: 1
Size: 344 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 1
Size: 1249 Color: 1
Size: 280 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 1
Size: 1248 Color: 1
Size: 264 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 1
Size: 1182 Color: 1
Size: 232 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6681 Color: 1
Size: 1201 Color: 1
Size: 198 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6696 Color: 1
Size: 1160 Color: 1
Size: 224 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 1
Size: 1142 Color: 1
Size: 228 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 1
Size: 1130 Color: 1
Size: 224 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6747 Color: 1
Size: 949 Color: 1
Size: 384 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 1
Size: 1092 Color: 1
Size: 216 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 1
Size: 1084 Color: 1
Size: 208 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 1
Size: 1065 Color: 1
Size: 212 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 1
Size: 1061 Color: 1
Size: 210 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 1
Size: 1192 Color: 1
Size: 64 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6825 Color: 1
Size: 1047 Color: 1
Size: 208 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 1
Size: 952 Color: 1
Size: 288 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 1
Size: 1006 Color: 1
Size: 196 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 1
Size: 1043 Color: 1
Size: 128 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6910 Color: 1
Size: 978 Color: 1
Size: 192 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 1112 Color: 1
Size: 16 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 1
Size: 881 Color: 1
Size: 238 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 1
Size: 900 Color: 1
Size: 176 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 1
Size: 980 Color: 1
Size: 88 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7016 Color: 1
Size: 888 Color: 1
Size: 176 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 1
Size: 878 Color: 1
Size: 184 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 7023 Color: 1
Size: 881 Color: 1
Size: 176 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 1
Size: 862 Color: 1
Size: 188 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 1
Size: 867 Color: 1
Size: 172 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 1
Size: 848 Color: 1
Size: 186 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 1
Size: 850 Color: 1
Size: 168 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 1
Size: 839 Color: 1
Size: 166 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 7080 Color: 1
Size: 770 Color: 1
Size: 230 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7088 Color: 1
Size: 820 Color: 1
Size: 172 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 1
Size: 840 Color: 1
Size: 132 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7158 Color: 1
Size: 748 Color: 1
Size: 174 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 1
Size: 726 Color: 1
Size: 144 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7224 Color: 1
Size: 680 Color: 1
Size: 176 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 1
Size: 702 Color: 1
Size: 140 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 1
Size: 690 Color: 1
Size: 136 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7272 Color: 1
Size: 728 Color: 1
Size: 80 Color: 0

Bin 91: 1 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 1
Size: 3367 Color: 1
Size: 656 Color: 0

Bin 92: 1 of cap free
Amount of items: 3
Items: 
Size: 4267 Color: 1
Size: 3324 Color: 1
Size: 488 Color: 0

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 4644 Color: 1
Size: 2801 Color: 1
Size: 634 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 5483 Color: 1
Size: 2468 Color: 1
Size: 128 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 1
Size: 1790 Color: 1
Size: 280 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 1
Size: 1396 Color: 1
Size: 272 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 1
Size: 1358 Color: 1
Size: 278 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 6583 Color: 1
Size: 1300 Color: 1
Size: 196 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 1
Size: 1379 Color: 1
Size: 32 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 1
Size: 1153 Color: 1
Size: 248 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 1
Size: 1342 Color: 1
Size: 32 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 6894 Color: 1
Size: 931 Color: 1
Size: 254 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 6943 Color: 1
Size: 568 Color: 0
Size: 568 Color: 0

Bin 104: 1 of cap free
Amount of items: 3
Items: 
Size: 6965 Color: 1
Size: 886 Color: 1
Size: 228 Color: 0

Bin 105: 1 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 1
Size: 990 Color: 1
Size: 64 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 4888 Color: 1
Size: 2854 Color: 1
Size: 336 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 5161 Color: 1
Size: 2433 Color: 1
Size: 484 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 1
Size: 2074 Color: 1
Size: 144 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 1
Size: 1324 Color: 1
Size: 390 Color: 0

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 1
Size: 1365 Color: 1
Size: 16 Color: 0

Bin 111: 3 of cap free
Amount of items: 3
Items: 
Size: 4721 Color: 1
Size: 2868 Color: 1
Size: 488 Color: 0

Bin 112: 3 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 1
Size: 2165 Color: 1
Size: 356 Color: 0

Bin 113: 4 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 1
Size: 2736 Color: 1
Size: 268 Color: 0

Bin 114: 4 of cap free
Amount of items: 3
Items: 
Size: 5124 Color: 1
Size: 2450 Color: 1
Size: 502 Color: 0

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 5600 Color: 1
Size: 2108 Color: 1
Size: 368 Color: 0

Bin 116: 5 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 1
Size: 2521 Color: 1
Size: 400 Color: 0

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 4043 Color: 1
Size: 3366 Color: 1
Size: 664 Color: 0

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 4658 Color: 1
Size: 2843 Color: 1
Size: 572 Color: 0

Bin 119: 7 of cap free
Amount of items: 3
Items: 
Size: 5057 Color: 1
Size: 2664 Color: 1
Size: 352 Color: 0

Bin 120: 8 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 1
Size: 3368 Color: 1
Size: 604 Color: 0

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 5100 Color: 1
Size: 2844 Color: 1
Size: 128 Color: 0

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 4676 Color: 1
Size: 2866 Color: 1
Size: 528 Color: 0

Bin 123: 11 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 1
Size: 3348 Color: 1
Size: 266 Color: 0

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 7188 Color: 1
Size: 880 Color: 0

Bin 125: 14 of cap free
Amount of items: 3
Items: 
Size: 4642 Color: 1
Size: 3264 Color: 1
Size: 160 Color: 0

Bin 126: 18 of cap free
Amount of items: 3
Items: 
Size: 4833 Color: 1
Size: 3021 Color: 1
Size: 208 Color: 0

Bin 127: 40 of cap free
Amount of items: 3
Items: 
Size: 5928 Color: 1
Size: 1680 Color: 0
Size: 432 Color: 0

Bin 128: 42 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 1
Size: 896 Color: 0
Size: 672 Color: 0

Bin 129: 43 of cap free
Amount of items: 4
Items: 
Size: 4042 Color: 1
Size: 3179 Color: 1
Size: 672 Color: 0
Size: 144 Color: 0

Bin 130: 78 of cap free
Amount of items: 5
Items: 
Size: 4041 Color: 1
Size: 2484 Color: 1
Size: 933 Color: 1
Size: 416 Color: 0
Size: 128 Color: 0

Bin 131: 97 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 1
Size: 3771 Color: 1
Size: 144 Color: 0

Bin 132: 2276 of cap free
Amount of items: 5
Items: 
Size: 1955 Color: 1
Size: 1778 Color: 1
Size: 1391 Color: 1
Size: 352 Color: 0
Size: 328 Color: 0

Bin 133: 5354 of cap free
Amount of items: 3
Items: 
Size: 1275 Color: 1
Size: 1147 Color: 1
Size: 304 Color: 0

Total size: 1066560
Total free space: 8080

