Capicity Bin: 16480
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 9104 Color: 1
Size: 5222 Color: 1
Size: 1882 Color: 0
Size: 272 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 1
Size: 1602 Color: 1
Size: 518 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 12974 Color: 1
Size: 2402 Color: 1
Size: 608 Color: 0
Size: 496 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 1
Size: 2300 Color: 0
Size: 912 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 1978 Color: 1
Size: 648 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 11621 Color: 1
Size: 4051 Color: 1
Size: 712 Color: 0
Size: 96 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12563 Color: 1
Size: 3265 Color: 1
Size: 652 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12957 Color: 1
Size: 2937 Color: 1
Size: 586 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13117 Color: 1
Size: 2803 Color: 1
Size: 560 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8241 Color: 1
Size: 6867 Color: 1
Size: 1372 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 10524 Color: 1
Size: 2726 Color: 1
Size: 2670 Color: 0
Size: 560 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 1
Size: 1672 Color: 1
Size: 370 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 1
Size: 5944 Color: 1
Size: 832 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9286 Color: 1
Size: 5998 Color: 1
Size: 1196 Color: 0

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 8244 Color: 1
Size: 5648 Color: 1
Size: 1372 Color: 0
Size: 1216 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8276 Color: 1
Size: 6844 Color: 1
Size: 1360 Color: 0

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 10262 Color: 1
Size: 4082 Color: 0
Size: 1752 Color: 1
Size: 384 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8243 Color: 1
Size: 6865 Color: 1
Size: 1372 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8250 Color: 1
Size: 6862 Color: 1
Size: 1368 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8260 Color: 1
Size: 6852 Color: 1
Size: 1368 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9299 Color: 1
Size: 5985 Color: 1
Size: 1196 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 1
Size: 6184 Color: 1
Size: 944 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9358 Color: 1
Size: 5938 Color: 1
Size: 1184 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 9380 Color: 1
Size: 5924 Color: 1
Size: 1176 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 1
Size: 5208 Color: 1
Size: 1040 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10332 Color: 1
Size: 4964 Color: 1
Size: 1184 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11002 Color: 1
Size: 4566 Color: 1
Size: 912 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11029 Color: 1
Size: 4543 Color: 1
Size: 908 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 1
Size: 4200 Color: 1
Size: 1168 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 1
Size: 4440 Color: 1
Size: 880 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 1
Size: 4488 Color: 1
Size: 804 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 1
Size: 4184 Color: 1
Size: 832 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 1
Size: 4022 Color: 1
Size: 800 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11816 Color: 1
Size: 3288 Color: 1
Size: 1376 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11860 Color: 1
Size: 3852 Color: 1
Size: 768 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 1
Size: 3688 Color: 1
Size: 736 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 1
Size: 3626 Color: 1
Size: 724 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12202 Color: 1
Size: 3566 Color: 1
Size: 712 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12633 Color: 1
Size: 3207 Color: 1
Size: 640 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 1
Size: 3198 Color: 1
Size: 636 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 1
Size: 3178 Color: 1
Size: 632 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 1
Size: 3464 Color: 1
Size: 304 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 1
Size: 3236 Color: 1
Size: 448 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 1
Size: 3004 Color: 1
Size: 640 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12868 Color: 1
Size: 3012 Color: 1
Size: 600 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12884 Color: 1
Size: 2724 Color: 1
Size: 872 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12923 Color: 1
Size: 2965 Color: 1
Size: 592 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 1
Size: 2942 Color: 1
Size: 584 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12981 Color: 1
Size: 2581 Color: 1
Size: 918 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 1
Size: 2877 Color: 1
Size: 574 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 1
Size: 3144 Color: 1
Size: 304 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13049 Color: 1
Size: 2861 Color: 1
Size: 570 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 1
Size: 2857 Color: 1
Size: 570 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13073 Color: 1
Size: 2841 Color: 1
Size: 566 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13077 Color: 1
Size: 3139 Color: 1
Size: 264 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13080 Color: 1
Size: 2840 Color: 1
Size: 560 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13097 Color: 1
Size: 2821 Color: 1
Size: 562 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 1
Size: 2552 Color: 1
Size: 816 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13128 Color: 1
Size: 2808 Color: 1
Size: 544 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13230 Color: 1
Size: 2710 Color: 1
Size: 540 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 1
Size: 2808 Color: 1
Size: 408 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13310 Color: 1
Size: 2642 Color: 1
Size: 528 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13361 Color: 1
Size: 2431 Color: 1
Size: 688 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13401 Color: 1
Size: 2567 Color: 1
Size: 512 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 1
Size: 2568 Color: 1
Size: 480 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 1
Size: 2888 Color: 1
Size: 92 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 1
Size: 2428 Color: 1
Size: 480 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13586 Color: 1
Size: 2678 Color: 1
Size: 216 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13597 Color: 1
Size: 1837 Color: 1
Size: 1046 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 1
Size: 2190 Color: 1
Size: 688 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13631 Color: 1
Size: 2375 Color: 1
Size: 474 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13634 Color: 1
Size: 2374 Color: 1
Size: 472 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13651 Color: 1
Size: 2149 Color: 1
Size: 680 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 1
Size: 2484 Color: 1
Size: 308 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13740 Color: 1
Size: 2284 Color: 1
Size: 456 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13767 Color: 1
Size: 2261 Color: 1
Size: 452 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 1
Size: 2260 Color: 1
Size: 448 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13829 Color: 1
Size: 1933 Color: 1
Size: 718 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13830 Color: 1
Size: 2246 Color: 1
Size: 404 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 1
Size: 2336 Color: 1
Size: 296 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 1
Size: 2254 Color: 1
Size: 348 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 1
Size: 1857 Color: 1
Size: 720 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 1
Size: 2200 Color: 1
Size: 340 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 1
Size: 2088 Color: 1
Size: 416 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13993 Color: 1
Size: 1913 Color: 1
Size: 574 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 1
Size: 1876 Color: 1
Size: 592 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 14054 Color: 1
Size: 1998 Color: 1
Size: 428 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 1
Size: 2060 Color: 1
Size: 332 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 1
Size: 1662 Color: 1
Size: 716 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 14161 Color: 1
Size: 1741 Color: 1
Size: 578 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 2008 Color: 1
Size: 304 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14174 Color: 1
Size: 1792 Color: 1
Size: 514 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 1
Size: 1916 Color: 1
Size: 376 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14209 Color: 1
Size: 1893 Color: 1
Size: 378 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14222 Color: 1
Size: 1862 Color: 1
Size: 396 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14225 Color: 1
Size: 1881 Color: 1
Size: 374 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 1
Size: 2124 Color: 1
Size: 120 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14250 Color: 1
Size: 2138 Color: 1
Size: 92 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14253 Color: 1
Size: 2051 Color: 1
Size: 176 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 1
Size: 1796 Color: 1
Size: 414 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14301 Color: 1
Size: 1817 Color: 1
Size: 362 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14305 Color: 1
Size: 1813 Color: 1
Size: 362 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 1
Size: 1816 Color: 1
Size: 352 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14336 Color: 1
Size: 1928 Color: 1
Size: 216 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14369 Color: 1
Size: 1859 Color: 1
Size: 252 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14370 Color: 1
Size: 1742 Color: 1
Size: 368 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 1
Size: 1702 Color: 1
Size: 398 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 2022 Color: 1
Size: 68 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14394 Color: 1
Size: 1692 Color: 1
Size: 394 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 1
Size: 1732 Color: 1
Size: 344 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14417 Color: 1
Size: 1721 Color: 1
Size: 342 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14486 Color: 1
Size: 1050 Color: 0
Size: 944 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 1588 Color: 1
Size: 404 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 1
Size: 1622 Color: 1
Size: 324 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14558 Color: 1
Size: 1576 Color: 1
Size: 346 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 1
Size: 1564 Color: 1
Size: 336 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 1072 Color: 1
Size: 808 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 1524 Color: 1
Size: 344 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14630 Color: 1
Size: 1496 Color: 1
Size: 354 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14646 Color: 1
Size: 1554 Color: 1
Size: 280 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14648 Color: 1
Size: 1450 Color: 1
Size: 382 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14660 Color: 1
Size: 1384 Color: 1
Size: 436 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14678 Color: 1
Size: 1502 Color: 1
Size: 300 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14694 Color: 1
Size: 1490 Color: 1
Size: 296 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 1
Size: 1432 Color: 1
Size: 352 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14708 Color: 1
Size: 1296 Color: 1
Size: 476 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14742 Color: 1
Size: 1528 Color: 1
Size: 210 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14754 Color: 1
Size: 1360 Color: 1
Size: 366 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14776 Color: 1
Size: 1176 Color: 1
Size: 528 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14824 Color: 1
Size: 1120 Color: 1
Size: 536 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14828 Color: 1
Size: 1484 Color: 1
Size: 168 Color: 0

Bin 132: 1 of cap free
Amount of items: 3
Items: 
Size: 10217 Color: 1
Size: 5346 Color: 1
Size: 916 Color: 0

Bin 133: 1 of cap free
Amount of items: 3
Items: 
Size: 11651 Color: 1
Size: 4452 Color: 1
Size: 376 Color: 0

Bin 134: 1 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 1
Size: 3896 Color: 1
Size: 386 Color: 0

Bin 135: 1 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 1
Size: 3241 Color: 1
Size: 910 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 12617 Color: 1
Size: 3246 Color: 1
Size: 616 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 13025 Color: 1
Size: 2922 Color: 1
Size: 532 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 13278 Color: 1
Size: 2881 Color: 1
Size: 320 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 1
Size: 2211 Color: 1
Size: 304 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 14021 Color: 1
Size: 2170 Color: 1
Size: 288 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 14086 Color: 1
Size: 2073 Color: 1
Size: 320 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 14113 Color: 1
Size: 1970 Color: 1
Size: 396 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 14185 Color: 1
Size: 1982 Color: 1
Size: 312 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 14277 Color: 1
Size: 1762 Color: 1
Size: 440 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 14349 Color: 1
Size: 1842 Color: 1
Size: 288 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 14393 Color: 1
Size: 1542 Color: 1
Size: 544 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 1
Size: 1777 Color: 1
Size: 924 Color: 0

Bin 148: 1 of cap free
Amount of items: 5
Items: 
Size: 6872 Color: 1
Size: 3771 Color: 1
Size: 3076 Color: 1
Size: 2328 Color: 0
Size: 432 Color: 0

Bin 149: 2 of cap free
Amount of items: 3
Items: 
Size: 10985 Color: 1
Size: 5221 Color: 1
Size: 272 Color: 0

Bin 150: 2 of cap free
Amount of items: 3
Items: 
Size: 10978 Color: 1
Size: 5084 Color: 1
Size: 416 Color: 0

Bin 151: 2 of cap free
Amount of items: 3
Items: 
Size: 11009 Color: 1
Size: 4581 Color: 1
Size: 888 Color: 0

Bin 152: 2 of cap free
Amount of items: 3
Items: 
Size: 11812 Color: 1
Size: 3924 Color: 1
Size: 742 Color: 0

Bin 153: 2 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 1
Size: 3892 Color: 1
Size: 408 Color: 0

Bin 154: 2 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 1
Size: 3586 Color: 1
Size: 560 Color: 0

Bin 155: 2 of cap free
Amount of items: 3
Items: 
Size: 12586 Color: 1
Size: 3428 Color: 1
Size: 464 Color: 0

Bin 156: 2 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 1
Size: 2673 Color: 1
Size: 800 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 10201 Color: 1
Size: 5233 Color: 1
Size: 1044 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 8242 Color: 1
Size: 6868 Color: 1
Size: 1368 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 13210 Color: 1
Size: 2684 Color: 1
Size: 584 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 1
Size: 1658 Color: 1
Size: 368 Color: 0

Bin 161: 3 of cap free
Amount of items: 3
Items: 
Size: 14325 Color: 1
Size: 1768 Color: 1
Size: 384 Color: 0

Bin 162: 3 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 1
Size: 5253 Color: 1
Size: 1232 Color: 0

Bin 163: 3 of cap free
Amount of items: 3
Items: 
Size: 10225 Color: 1
Size: 5884 Color: 1
Size: 368 Color: 0

Bin 164: 3 of cap free
Amount of items: 3
Items: 
Size: 11140 Color: 1
Size: 4561 Color: 1
Size: 776 Color: 0

Bin 165: 3 of cap free
Amount of items: 3
Items: 
Size: 12593 Color: 1
Size: 3388 Color: 1
Size: 496 Color: 0

Bin 166: 4 of cap free
Amount of items: 3
Items: 
Size: 11001 Color: 1
Size: 5203 Color: 1
Size: 272 Color: 0

Bin 167: 5 of cap free
Amount of items: 3
Items: 
Size: 13385 Color: 1
Size: 2418 Color: 1
Size: 672 Color: 0

Bin 168: 5 of cap free
Amount of items: 3
Items: 
Size: 10961 Color: 1
Size: 4730 Color: 1
Size: 784 Color: 0

Bin 169: 5 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 1
Size: 4601 Color: 1
Size: 288 Color: 0

Bin 170: 5 of cap free
Amount of items: 3
Items: 
Size: 12161 Color: 1
Size: 4042 Color: 1
Size: 272 Color: 0

Bin 171: 5 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 1
Size: 3571 Color: 1
Size: 352 Color: 0

Bin 172: 5 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 1
Size: 1973 Color: 1
Size: 392 Color: 0

Bin 173: 6 of cap free
Amount of items: 3
Items: 
Size: 10806 Color: 1
Size: 4626 Color: 1
Size: 1042 Color: 0

Bin 174: 6 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 1
Size: 4776 Color: 1
Size: 768 Color: 0

Bin 175: 6 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 1
Size: 7184 Color: 1
Size: 1042 Color: 0

Bin 176: 7 of cap free
Amount of items: 3
Items: 
Size: 9297 Color: 1
Size: 6552 Color: 1
Size: 624 Color: 0

Bin 177: 7 of cap free
Amount of items: 3
Items: 
Size: 10177 Color: 1
Size: 5416 Color: 1
Size: 880 Color: 0

Bin 178: 7 of cap free
Amount of items: 3
Items: 
Size: 10380 Color: 1
Size: 5213 Color: 1
Size: 880 Color: 0

Bin 179: 7 of cap free
Amount of items: 3
Items: 
Size: 11634 Color: 1
Size: 4087 Color: 1
Size: 752 Color: 0

Bin 180: 8 of cap free
Amount of items: 3
Items: 
Size: 11675 Color: 1
Size: 3601 Color: 1
Size: 1196 Color: 0

Bin 181: 8 of cap free
Amount of items: 3
Items: 
Size: 11772 Color: 1
Size: 4412 Color: 1
Size: 288 Color: 0

Bin 182: 11 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 1
Size: 4005 Color: 1
Size: 1016 Color: 0

Bin 183: 13 of cap free
Amount of items: 3
Items: 
Size: 10237 Color: 1
Size: 5958 Color: 1
Size: 272 Color: 0

Bin 184: 16 of cap free
Amount of items: 3
Items: 
Size: 10214 Color: 1
Size: 5892 Color: 1
Size: 358 Color: 0

Bin 185: 16 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 1
Size: 5656 Color: 1
Size: 1380 Color: 0

Bin 186: 23 of cap free
Amount of items: 3
Items: 
Size: 12372 Color: 1
Size: 2897 Color: 1
Size: 1188 Color: 0

Bin 187: 25 of cap free
Amount of items: 5
Items: 
Size: 6160 Color: 1
Size: 5532 Color: 1
Size: 3227 Color: 1
Size: 1040 Color: 0
Size: 496 Color: 0

Bin 188: 31 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 1
Size: 2601 Color: 1
Size: 432 Color: 0

Bin 189: 68 of cap free
Amount of items: 3
Items: 
Size: 8632 Color: 1
Size: 6866 Color: 1
Size: 914 Color: 0

Bin 190: 74 of cap free
Amount of items: 3
Items: 
Size: 12159 Color: 1
Size: 3601 Color: 1
Size: 646 Color: 0

Bin 191: 96 of cap free
Amount of items: 3
Items: 
Size: 9412 Color: 1
Size: 6160 Color: 1
Size: 812 Color: 0

Bin 192: 119 of cap free
Amount of items: 5
Items: 
Size: 8128 Color: 1
Size: 3460 Color: 1
Size: 1984 Color: 0
Size: 1797 Color: 1
Size: 992 Color: 0

Bin 193: 189 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 1
Size: 5387 Color: 1
Size: 144 Color: 0

Bin 194: 751 of cap free
Amount of items: 3
Items: 
Size: 9334 Color: 1
Size: 5987 Color: 1
Size: 408 Color: 0

Bin 195: 1896 of cap free
Amount of items: 1
Items: 
Size: 14584 Color: 1

Bin 196: 2782 of cap free
Amount of items: 3
Items: 
Size: 9064 Color: 1
Size: 4586 Color: 1
Size: 48 Color: 0

Bin 197: 2898 of cap free
Amount of items: 1
Items: 
Size: 13582 Color: 1

Bin 198: 3260 of cap free
Amount of items: 1
Items: 
Size: 13220 Color: 1

Bin 199: 4060 of cap free
Amount of items: 1
Items: 
Size: 12420 Color: 1

Total size: 3263040
Total free space: 16480

