Capicity Bin: 15792
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7908 Color: 0
Size: 6572 Color: 2
Size: 1312 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 3
Size: 6664 Color: 4
Size: 608 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 4
Size: 6072 Color: 0
Size: 400 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9836 Color: 4
Size: 5432 Color: 0
Size: 524 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10561 Color: 0
Size: 4361 Color: 1
Size: 870 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10604 Color: 1
Size: 4908 Color: 0
Size: 280 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 3
Size: 4284 Color: 2
Size: 860 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10882 Color: 4
Size: 4514 Color: 0
Size: 396 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11340 Color: 4
Size: 3716 Color: 0
Size: 736 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11706 Color: 0
Size: 3768 Color: 4
Size: 318 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 3
Size: 3528 Color: 0
Size: 560 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11812 Color: 0
Size: 2844 Color: 1
Size: 1136 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11747 Color: 2
Size: 3831 Color: 3
Size: 214 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12018 Color: 3
Size: 2750 Color: 0
Size: 1024 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12156 Color: 3
Size: 3324 Color: 1
Size: 312 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12207 Color: 2
Size: 2989 Color: 0
Size: 596 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12220 Color: 2
Size: 3004 Color: 0
Size: 568 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 2
Size: 3298 Color: 0
Size: 198 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 1
Size: 3064 Color: 0
Size: 380 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12408 Color: 4
Size: 3188 Color: 0
Size: 196 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12444 Color: 0
Size: 2796 Color: 4
Size: 552 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12460 Color: 0
Size: 2876 Color: 2
Size: 456 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12638 Color: 0
Size: 2150 Color: 4
Size: 1004 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12614 Color: 4
Size: 2780 Color: 0
Size: 398 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12709 Color: 0
Size: 1931 Color: 2
Size: 1152 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 0
Size: 2260 Color: 4
Size: 752 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12846 Color: 1
Size: 2056 Color: 4
Size: 890 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12961 Color: 0
Size: 1839 Color: 1
Size: 992 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12965 Color: 0
Size: 1971 Color: 2
Size: 856 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12993 Color: 4
Size: 1951 Color: 2
Size: 848 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13033 Color: 0
Size: 2087 Color: 4
Size: 672 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13052 Color: 0
Size: 2376 Color: 3
Size: 364 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13082 Color: 0
Size: 2284 Color: 1
Size: 426 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 2
Size: 2322 Color: 0
Size: 386 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 4
Size: 2316 Color: 0
Size: 316 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 4
Size: 1952 Color: 0
Size: 616 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 2
Size: 1995 Color: 0
Size: 568 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 1
Size: 2092 Color: 0
Size: 416 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 0
Size: 2104 Color: 1
Size: 370 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 0
Size: 1448 Color: 2
Size: 976 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 3
Size: 1598 Color: 0
Size: 858 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 4
Size: 2044 Color: 4
Size: 376 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13495 Color: 0
Size: 1749 Color: 2
Size: 548 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13439 Color: 3
Size: 1601 Color: 0
Size: 752 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13510 Color: 0
Size: 1754 Color: 1
Size: 528 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 0
Size: 1628 Color: 1
Size: 620 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13566 Color: 0
Size: 1360 Color: 3
Size: 866 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13582 Color: 0
Size: 1314 Color: 4
Size: 896 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13587 Color: 0
Size: 1747 Color: 2
Size: 458 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13607 Color: 0
Size: 1689 Color: 1
Size: 496 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13548 Color: 1
Size: 1782 Color: 1
Size: 462 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 0
Size: 1678 Color: 4
Size: 478 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 3
Size: 1152 Color: 0
Size: 984 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 1804 Color: 0
Size: 328 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13695 Color: 0
Size: 1673 Color: 2
Size: 424 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 0
Size: 1658 Color: 2
Size: 352 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 1
Size: 1548 Color: 3
Size: 488 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13806 Color: 4
Size: 1524 Color: 4
Size: 462 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13844 Color: 1
Size: 1404 Color: 3
Size: 544 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 0
Size: 1482 Color: 1
Size: 432 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 3
Size: 1592 Color: 0
Size: 304 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 1
Size: 1572 Color: 0
Size: 312 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 4
Size: 1314 Color: 1
Size: 560 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 4
Size: 1428 Color: 0
Size: 400 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 3
Size: 1362 Color: 0
Size: 416 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 1
Size: 1008 Color: 0
Size: 764 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14116 Color: 0
Size: 1564 Color: 1
Size: 112 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 1
Size: 1312 Color: 0
Size: 408 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 4
Size: 1280 Color: 0
Size: 390 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 14146 Color: 0
Size: 1312 Color: 4
Size: 334 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14192 Color: 0
Size: 1140 Color: 1
Size: 460 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 1
Size: 960 Color: 0
Size: 630 Color: 3

Bin 73: 1 of cap free
Amount of items: 7
Items: 
Size: 7898 Color: 0
Size: 2123 Color: 2
Size: 1902 Color: 2
Size: 1574 Color: 3
Size: 1518 Color: 4
Size: 408 Color: 0
Size: 368 Color: 4

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 7909 Color: 4
Size: 6568 Color: 2
Size: 1314 Color: 0

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 8866 Color: 1
Size: 6577 Color: 0
Size: 348 Color: 2

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 8956 Color: 2
Size: 6571 Color: 3
Size: 264 Color: 3

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 8913 Color: 0
Size: 6578 Color: 0
Size: 300 Color: 2

Bin 78: 1 of cap free
Amount of items: 4
Items: 
Size: 9762 Color: 0
Size: 2597 Color: 1
Size: 2558 Color: 3
Size: 874 Color: 4

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 2
Size: 5707 Color: 0
Size: 208 Color: 2

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 10585 Color: 0
Size: 4824 Color: 4
Size: 382 Color: 1

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 10481 Color: 3
Size: 5026 Color: 3
Size: 284 Color: 4

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 11056 Color: 4
Size: 4287 Color: 0
Size: 448 Color: 2

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 11803 Color: 0
Size: 3518 Color: 2
Size: 470 Color: 2

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 2
Size: 2807 Color: 4
Size: 508 Color: 0

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 12495 Color: 3
Size: 2976 Color: 0
Size: 320 Color: 2

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 12726 Color: 0
Size: 2595 Color: 2
Size: 470 Color: 4

Bin 87: 1 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 0
Size: 1821 Color: 1
Size: 1146 Color: 1

Bin 88: 1 of cap free
Amount of items: 3
Items: 
Size: 12913 Color: 1
Size: 2062 Color: 4
Size: 816 Color: 0

Bin 89: 1 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 3
Size: 2333 Color: 0
Size: 448 Color: 2

Bin 90: 1 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 4
Size: 2361 Color: 4
Size: 176 Color: 0

Bin 91: 1 of cap free
Amount of items: 3
Items: 
Size: 13697 Color: 1
Size: 2006 Color: 0
Size: 88 Color: 2

Bin 92: 1 of cap free
Amount of items: 2
Items: 
Size: 13767 Color: 2
Size: 2024 Color: 1

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 13873 Color: 0
Size: 1326 Color: 2
Size: 592 Color: 4

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 1
Size: 5700 Color: 0
Size: 416 Color: 1

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 10497 Color: 3
Size: 5009 Color: 0
Size: 284 Color: 2

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 10970 Color: 1
Size: 4488 Color: 0
Size: 332 Color: 2

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 13049 Color: 0
Size: 2313 Color: 3
Size: 428 Color: 2

Bin 98: 2 of cap free
Amount of items: 2
Items: 
Size: 13326 Color: 1
Size: 2464 Color: 4

Bin 99: 2 of cap free
Amount of items: 2
Items: 
Size: 13503 Color: 4
Size: 2287 Color: 1

Bin 100: 2 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 3
Size: 2058 Color: 2

Bin 101: 2 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 1
Size: 1750 Color: 2

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 10537 Color: 1
Size: 4964 Color: 0
Size: 288 Color: 2

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 10660 Color: 1
Size: 3380 Color: 1
Size: 1749 Color: 0

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 12159 Color: 0
Size: 2382 Color: 3
Size: 1248 Color: 4

Bin 105: 3 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 3
Size: 3349 Color: 2

Bin 106: 3 of cap free
Amount of items: 3
Items: 
Size: 13386 Color: 2
Size: 2137 Color: 0
Size: 266 Color: 2

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 9288 Color: 0
Size: 5872 Color: 0
Size: 628 Color: 2

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 10472 Color: 3
Size: 4932 Color: 3
Size: 384 Color: 4

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 10824 Color: 2
Size: 4964 Color: 1

Bin 110: 4 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 3
Size: 3952 Color: 0
Size: 374 Color: 3

Bin 111: 4 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 3
Size: 3742 Color: 4
Size: 518 Color: 1

Bin 112: 4 of cap free
Amount of items: 2
Items: 
Size: 12868 Color: 4
Size: 2920 Color: 2

Bin 113: 4 of cap free
Amount of items: 2
Items: 
Size: 13272 Color: 2
Size: 2516 Color: 3

Bin 114: 4 of cap free
Amount of items: 2
Items: 
Size: 14052 Color: 4
Size: 1736 Color: 1

Bin 115: 5 of cap free
Amount of items: 3
Items: 
Size: 7902 Color: 3
Size: 6573 Color: 2
Size: 1312 Color: 3

Bin 116: 5 of cap free
Amount of items: 3
Items: 
Size: 11838 Color: 3
Size: 2749 Color: 0
Size: 1200 Color: 2

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 8870 Color: 4
Size: 6580 Color: 0
Size: 336 Color: 3

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 9592 Color: 4
Size: 5874 Color: 0
Size: 320 Color: 2

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 10610 Color: 4
Size: 5176 Color: 3

Bin 120: 6 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 2
Size: 3114 Color: 0
Size: 552 Color: 3

Bin 121: 6 of cap free
Amount of items: 2
Items: 
Size: 13658 Color: 4
Size: 2128 Color: 1

Bin 122: 6 of cap free
Amount of items: 2
Items: 
Size: 14158 Color: 2
Size: 1628 Color: 3

Bin 123: 7 of cap free
Amount of items: 3
Items: 
Size: 8945 Color: 2
Size: 6056 Color: 1
Size: 784 Color: 3

Bin 124: 7 of cap free
Amount of items: 3
Items: 
Size: 8964 Color: 1
Size: 6581 Color: 3
Size: 240 Color: 2

Bin 125: 7 of cap free
Amount of items: 3
Items: 
Size: 10545 Color: 0
Size: 4308 Color: 3
Size: 932 Color: 4

Bin 126: 7 of cap free
Amount of items: 3
Items: 
Size: 10628 Color: 4
Size: 4341 Color: 0
Size: 816 Color: 4

Bin 127: 7 of cap free
Amount of items: 3
Items: 
Size: 11252 Color: 1
Size: 4293 Color: 3
Size: 240 Color: 0

Bin 128: 7 of cap free
Amount of items: 3
Items: 
Size: 13001 Color: 2
Size: 2648 Color: 4
Size: 136 Color: 4

Bin 129: 7 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 4
Size: 2091 Color: 1

Bin 130: 8 of cap free
Amount of items: 2
Items: 
Size: 13020 Color: 2
Size: 2764 Color: 4

Bin 131: 8 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 3
Size: 2444 Color: 2

Bin 132: 8 of cap free
Amount of items: 2
Items: 
Size: 13427 Color: 4
Size: 2357 Color: 2

Bin 133: 8 of cap free
Amount of items: 2
Items: 
Size: 14084 Color: 4
Size: 1700 Color: 3

Bin 134: 9 of cap free
Amount of items: 2
Items: 
Size: 13868 Color: 3
Size: 1915 Color: 2

Bin 135: 10 of cap free
Amount of items: 3
Items: 
Size: 11820 Color: 1
Size: 3610 Color: 0
Size: 352 Color: 4

Bin 136: 10 of cap free
Amount of items: 2
Items: 
Size: 12058 Color: 2
Size: 3724 Color: 4

Bin 137: 10 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 4
Size: 1842 Color: 3

Bin 138: 11 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 3
Size: 4381 Color: 3
Size: 1104 Color: 1

Bin 139: 11 of cap free
Amount of items: 2
Items: 
Size: 12330 Color: 2
Size: 3451 Color: 4

Bin 140: 11 of cap free
Amount of items: 2
Items: 
Size: 13902 Color: 4
Size: 1879 Color: 2

Bin 141: 12 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 0
Size: 5528 Color: 3
Size: 244 Color: 1

Bin 142: 13 of cap free
Amount of items: 3
Items: 
Size: 10449 Color: 0
Size: 4324 Color: 4
Size: 1006 Color: 3

Bin 143: 13 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 2
Size: 3399 Color: 4

Bin 144: 14 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 3
Size: 2262 Color: 1

Bin 145: 15 of cap free
Amount of items: 2
Items: 
Size: 11324 Color: 1
Size: 4453 Color: 2

Bin 146: 16 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 3
Size: 2824 Color: 2

Bin 147: 16 of cap free
Amount of items: 2
Items: 
Size: 13475 Color: 3
Size: 2301 Color: 4

Bin 148: 17 of cap free
Amount of items: 3
Items: 
Size: 11271 Color: 4
Size: 4296 Color: 0
Size: 208 Color: 2

Bin 149: 19 of cap free
Amount of items: 3
Items: 
Size: 11302 Color: 4
Size: 2571 Color: 3
Size: 1900 Color: 0

Bin 150: 19 of cap free
Amount of items: 2
Items: 
Size: 13017 Color: 1
Size: 2756 Color: 3

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 13399 Color: 3
Size: 2373 Color: 2

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 13451 Color: 3
Size: 2321 Color: 2

Bin 153: 21 of cap free
Amount of items: 3
Items: 
Size: 13214 Color: 3
Size: 2401 Color: 2
Size: 156 Color: 1

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 12210 Color: 1
Size: 3560 Color: 3

Bin 155: 23 of cap free
Amount of items: 3
Items: 
Size: 8312 Color: 2
Size: 5733 Color: 0
Size: 1724 Color: 1

Bin 156: 24 of cap free
Amount of items: 3
Items: 
Size: 8072 Color: 2
Size: 6944 Color: 4
Size: 752 Color: 1

Bin 157: 24 of cap free
Amount of items: 2
Items: 
Size: 12677 Color: 2
Size: 3091 Color: 1

Bin 158: 26 of cap free
Amount of items: 2
Items: 
Size: 12085 Color: 4
Size: 3681 Color: 3

Bin 159: 26 of cap free
Amount of items: 2
Items: 
Size: 13308 Color: 2
Size: 2458 Color: 3

Bin 160: 29 of cap free
Amount of items: 2
Items: 
Size: 13563 Color: 2
Size: 2200 Color: 4

Bin 161: 33 of cap free
Amount of items: 2
Items: 
Size: 13009 Color: 3
Size: 2750 Color: 1

Bin 162: 34 of cap free
Amount of items: 2
Items: 
Size: 13974 Color: 4
Size: 1784 Color: 1

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 12608 Color: 3
Size: 3146 Color: 4

Bin 164: 39 of cap free
Amount of items: 2
Items: 
Size: 12945 Color: 1
Size: 2808 Color: 3

Bin 165: 42 of cap free
Amount of items: 2
Items: 
Size: 12425 Color: 1
Size: 3325 Color: 3

Bin 166: 44 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 4
Size: 3036 Color: 2
Size: 96 Color: 3

Bin 167: 44 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 1
Size: 1608 Color: 4

Bin 168: 45 of cap free
Amount of items: 3
Items: 
Size: 9783 Color: 2
Size: 5692 Color: 3
Size: 272 Color: 0

Bin 169: 47 of cap free
Amount of items: 3
Items: 
Size: 11653 Color: 4
Size: 3788 Color: 3
Size: 304 Color: 0

Bin 170: 48 of cap free
Amount of items: 2
Items: 
Size: 12758 Color: 3
Size: 2986 Color: 4

Bin 171: 48 of cap free
Amount of items: 2
Items: 
Size: 13864 Color: 2
Size: 1880 Color: 4

Bin 172: 50 of cap free
Amount of items: 2
Items: 
Size: 9160 Color: 3
Size: 6582 Color: 0

Bin 173: 52 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 4
Size: 2020 Color: 1

Bin 174: 52 of cap free
Amount of items: 3
Items: 
Size: 13785 Color: 2
Size: 1859 Color: 1
Size: 96 Color: 0

Bin 175: 55 of cap free
Amount of items: 2
Items: 
Size: 11715 Color: 1
Size: 4022 Color: 2

Bin 176: 56 of cap free
Amount of items: 2
Items: 
Size: 10649 Color: 1
Size: 5087 Color: 2

Bin 177: 59 of cap free
Amount of items: 2
Items: 
Size: 13245 Color: 3
Size: 2488 Color: 4

Bin 178: 60 of cap free
Amount of items: 2
Items: 
Size: 12492 Color: 3
Size: 3240 Color: 1

Bin 179: 66 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 4
Size: 6440 Color: 0
Size: 1374 Color: 3

Bin 180: 80 of cap free
Amount of items: 2
Items: 
Size: 11560 Color: 1
Size: 4152 Color: 3

Bin 181: 81 of cap free
Amount of items: 4
Items: 
Size: 7905 Color: 3
Size: 5256 Color: 0
Size: 2076 Color: 0
Size: 474 Color: 3

Bin 182: 83 of cap free
Amount of items: 2
Items: 
Size: 7901 Color: 3
Size: 7808 Color: 2

Bin 183: 85 of cap free
Amount of items: 2
Items: 
Size: 13539 Color: 4
Size: 2168 Color: 1

Bin 184: 87 of cap free
Amount of items: 2
Items: 
Size: 13829 Color: 2
Size: 1876 Color: 1

Bin 185: 98 of cap free
Amount of items: 3
Items: 
Size: 10178 Color: 0
Size: 2886 Color: 3
Size: 2630 Color: 3

Bin 186: 110 of cap free
Amount of items: 2
Items: 
Size: 9908 Color: 4
Size: 5774 Color: 3

Bin 187: 110 of cap free
Amount of items: 2
Items: 
Size: 11000 Color: 1
Size: 4682 Color: 3

Bin 188: 111 of cap free
Amount of items: 2
Items: 
Size: 11912 Color: 3
Size: 3769 Color: 1

Bin 189: 115 of cap free
Amount of items: 3
Items: 
Size: 9751 Color: 3
Size: 4322 Color: 1
Size: 1604 Color: 4

Bin 190: 116 of cap free
Amount of items: 2
Items: 
Size: 10641 Color: 2
Size: 5035 Color: 4

Bin 191: 119 of cap free
Amount of items: 4
Items: 
Size: 7900 Color: 4
Size: 4681 Color: 0
Size: 1780 Color: 1
Size: 1312 Color: 3

Bin 192: 132 of cap free
Amount of items: 33
Items: 
Size: 678 Color: 0
Size: 664 Color: 0
Size: 656 Color: 0
Size: 656 Color: 0
Size: 600 Color: 4
Size: 596 Color: 1
Size: 576 Color: 4
Size: 576 Color: 3
Size: 568 Color: 2
Size: 552 Color: 0
Size: 544 Color: 2
Size: 512 Color: 2
Size: 480 Color: 0
Size: 480 Color: 0
Size: 466 Color: 4
Size: 464 Color: 1
Size: 456 Color: 0
Size: 456 Color: 0
Size: 428 Color: 2
Size: 412 Color: 2
Size: 408 Color: 0
Size: 400 Color: 4
Size: 400 Color: 4
Size: 400 Color: 1
Size: 394 Color: 4
Size: 368 Color: 3
Size: 368 Color: 3
Size: 366 Color: 3
Size: 364 Color: 3
Size: 352 Color: 4
Size: 348 Color: 1
Size: 348 Color: 1
Size: 324 Color: 1

Bin 193: 147 of cap free
Amount of items: 2
Items: 
Size: 11272 Color: 3
Size: 4373 Color: 2

Bin 194: 155 of cap free
Amount of items: 2
Items: 
Size: 11197 Color: 2
Size: 4440 Color: 3

Bin 195: 171 of cap free
Amount of items: 6
Items: 
Size: 7897 Color: 1
Size: 1902 Color: 2
Size: 1562 Color: 2
Size: 1484 Color: 1
Size: 1464 Color: 1
Size: 1312 Color: 2

Bin 196: 178 of cap free
Amount of items: 19
Items: 
Size: 1136 Color: 3
Size: 1072 Color: 1
Size: 1000 Color: 1
Size: 984 Color: 3
Size: 880 Color: 4
Size: 874 Color: 4
Size: 864 Color: 3
Size: 856 Color: 1
Size: 848 Color: 4
Size: 800 Color: 3
Size: 752 Color: 2
Size: 748 Color: 2
Size: 744 Color: 0
Size: 720 Color: 4
Size: 704 Color: 2
Size: 704 Color: 0
Size: 688 Color: 3
Size: 640 Color: 2
Size: 600 Color: 4

Bin 197: 178 of cap free
Amount of items: 2
Items: 
Size: 9844 Color: 2
Size: 5770 Color: 1

Bin 198: 188 of cap free
Amount of items: 2
Items: 
Size: 12188 Color: 2
Size: 3416 Color: 4

Bin 199: 11864 of cap free
Amount of items: 13
Items: 
Size: 352 Color: 4
Size: 336 Color: 2
Size: 336 Color: 2
Size: 336 Color: 2
Size: 320 Color: 3
Size: 304 Color: 3
Size: 304 Color: 1
Size: 296 Color: 2
Size: 288 Color: 3
Size: 272 Color: 4
Size: 272 Color: 1
Size: 272 Color: 1
Size: 240 Color: 4

Total size: 3126816
Total free space: 15792

