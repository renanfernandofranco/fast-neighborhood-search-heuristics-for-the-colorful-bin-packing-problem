Capicity Bin: 1000001
Lower Bound: 48

Bins used: 52
Amount of Colors: 2

Bin 1: 19 of cap free
Amount of items: 3
Items: 
Size: 633046 Color: 1
Size: 225457 Color: 0
Size: 141479 Color: 1

Bin 2: 161 of cap free
Amount of items: 3
Items: 
Size: 609366 Color: 1
Size: 259821 Color: 1
Size: 130653 Color: 0

Bin 3: 578 of cap free
Amount of items: 4
Items: 
Size: 504071 Color: 0
Size: 191588 Color: 1
Size: 191525 Color: 0
Size: 112239 Color: 1

Bin 4: 615 of cap free
Amount of items: 2
Items: 
Size: 527056 Color: 0
Size: 472330 Color: 1

Bin 5: 680 of cap free
Amount of items: 3
Items: 
Size: 479591 Color: 0
Size: 312521 Color: 1
Size: 207209 Color: 1

Bin 6: 977 of cap free
Amount of items: 2
Items: 
Size: 597926 Color: 0
Size: 401098 Color: 1

Bin 7: 1307 of cap free
Amount of items: 3
Items: 
Size: 502562 Color: 0
Size: 265801 Color: 1
Size: 230331 Color: 0

Bin 8: 1345 of cap free
Amount of items: 3
Items: 
Size: 662802 Color: 0
Size: 179174 Color: 1
Size: 156680 Color: 0

Bin 9: 1363 of cap free
Amount of items: 2
Items: 
Size: 622480 Color: 1
Size: 376158 Color: 0

Bin 10: 1496 of cap free
Amount of items: 3
Items: 
Size: 641125 Color: 1
Size: 194630 Color: 0
Size: 162750 Color: 0

Bin 11: 1633 of cap free
Amount of items: 3
Items: 
Size: 549325 Color: 0
Size: 306116 Color: 1
Size: 142927 Color: 0

Bin 12: 1762 of cap free
Amount of items: 2
Items: 
Size: 552439 Color: 0
Size: 445800 Color: 1

Bin 13: 2648 of cap free
Amount of items: 2
Items: 
Size: 796913 Color: 0
Size: 200440 Color: 1

Bin 14: 2822 of cap free
Amount of items: 2
Items: 
Size: 649739 Color: 1
Size: 347440 Color: 0

Bin 15: 3152 of cap free
Amount of items: 3
Items: 
Size: 481574 Color: 0
Size: 394286 Color: 0
Size: 120989 Color: 1

Bin 16: 4399 of cap free
Amount of items: 2
Items: 
Size: 646036 Color: 0
Size: 349566 Color: 1

Bin 17: 4611 of cap free
Amount of items: 2
Items: 
Size: 557130 Color: 0
Size: 438260 Color: 1

Bin 18: 4726 of cap free
Amount of items: 3
Items: 
Size: 393610 Color: 0
Size: 392726 Color: 0
Size: 208939 Color: 1

Bin 19: 5593 of cap free
Amount of items: 2
Items: 
Size: 541444 Color: 0
Size: 452964 Color: 1

Bin 20: 6232 of cap free
Amount of items: 2
Items: 
Size: 741864 Color: 1
Size: 251905 Color: 0

Bin 21: 6250 of cap free
Amount of items: 2
Items: 
Size: 525559 Color: 1
Size: 468192 Color: 0

Bin 22: 7566 of cap free
Amount of items: 2
Items: 
Size: 703546 Color: 0
Size: 288889 Color: 1

Bin 23: 7997 of cap free
Amount of items: 2
Items: 
Size: 518895 Color: 0
Size: 473109 Color: 1

Bin 24: 8734 of cap free
Amount of items: 2
Items: 
Size: 549524 Color: 1
Size: 441743 Color: 0

Bin 25: 9887 of cap free
Amount of items: 3
Items: 
Size: 634074 Color: 1
Size: 190285 Color: 0
Size: 165755 Color: 0

Bin 26: 10142 of cap free
Amount of items: 2
Items: 
Size: 645176 Color: 1
Size: 344683 Color: 0

Bin 27: 10452 of cap free
Amount of items: 2
Items: 
Size: 507523 Color: 0
Size: 482026 Color: 1

Bin 28: 12658 of cap free
Amount of items: 2
Items: 
Size: 668163 Color: 0
Size: 319180 Color: 1

Bin 29: 13365 of cap free
Amount of items: 2
Items: 
Size: 616965 Color: 0
Size: 369671 Color: 1

Bin 30: 16704 of cap free
Amount of items: 3
Items: 
Size: 434064 Color: 0
Size: 405672 Color: 0
Size: 143561 Color: 1

Bin 31: 16876 of cap free
Amount of items: 2
Items: 
Size: 532044 Color: 0
Size: 451081 Color: 1

Bin 32: 20235 of cap free
Amount of items: 2
Items: 
Size: 797990 Color: 1
Size: 181776 Color: 0

Bin 33: 21553 of cap free
Amount of items: 2
Items: 
Size: 658264 Color: 1
Size: 320184 Color: 0

Bin 34: 25539 of cap free
Amount of items: 2
Items: 
Size: 582352 Color: 0
Size: 392110 Color: 1

Bin 35: 44893 of cap free
Amount of items: 2
Items: 
Size: 527032 Color: 0
Size: 428076 Color: 1

Bin 36: 74842 of cap free
Amount of items: 2
Items: 
Size: 580287 Color: 0
Size: 344872 Color: 1

Bin 37: 75447 of cap free
Amount of items: 2
Items: 
Size: 787538 Color: 1
Size: 137016 Color: 0

Bin 38: 212910 of cap free
Amount of items: 1
Items: 
Size: 787091 Color: 1

Bin 39: 214583 of cap free
Amount of items: 1
Items: 
Size: 785418 Color: 1

Bin 40: 219926 of cap free
Amount of items: 1
Items: 
Size: 780075 Color: 1

Bin 41: 228411 of cap free
Amount of items: 1
Items: 
Size: 771590 Color: 0

Bin 42: 238949 of cap free
Amount of items: 1
Items: 
Size: 761052 Color: 0

Bin 43: 240945 of cap free
Amount of items: 1
Items: 
Size: 759056 Color: 1

Bin 44: 244907 of cap free
Amount of items: 1
Items: 
Size: 755094 Color: 1

Bin 45: 256749 of cap free
Amount of items: 1
Items: 
Size: 743252 Color: 0

Bin 46: 259301 of cap free
Amount of items: 1
Items: 
Size: 740700 Color: 0

Bin 47: 263974 of cap free
Amount of items: 1
Items: 
Size: 736027 Color: 1

Bin 48: 273267 of cap free
Amount of items: 1
Items: 
Size: 726734 Color: 1

Bin 49: 304717 of cap free
Amount of items: 1
Items: 
Size: 695284 Color: 0

Bin 50: 318038 of cap free
Amount of items: 1
Items: 
Size: 681963 Color: 1

Bin 51: 424189 of cap free
Amount of items: 1
Items: 
Size: 575812 Color: 0

Bin 52: 425575 of cap free
Amount of items: 1
Items: 
Size: 574426 Color: 0

Total size: 47444352
Total free space: 4555700

