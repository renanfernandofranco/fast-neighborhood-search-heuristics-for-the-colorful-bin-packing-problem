Capicity Bin: 7744
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 3840 Color: 1
Size: 1168 Color: 1
Size: 968 Color: 1
Size: 608 Color: 1
Size: 488 Color: 0
Size: 416 Color: 0
Size: 144 Color: 0
Size: 112 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 1
Size: 2300 Color: 1
Size: 456 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 1
Size: 1162 Color: 1
Size: 228 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 1
Size: 2980 Color: 1
Size: 592 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 1
Size: 876 Color: 1
Size: 168 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 1
Size: 1669 Color: 1
Size: 332 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 1
Size: 658 Color: 1
Size: 128 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 1
Size: 662 Color: 1
Size: 132 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4628 Color: 1
Size: 2604 Color: 1
Size: 512 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 1
Size: 772 Color: 1
Size: 152 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 1
Size: 788 Color: 1
Size: 152 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 1
Size: 1012 Color: 1
Size: 200 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 1
Size: 1245 Color: 1
Size: 198 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4832 Color: 1
Size: 2440 Color: 1
Size: 472 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6285 Color: 1
Size: 1217 Color: 1
Size: 242 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 1
Size: 1202 Color: 1
Size: 236 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 1
Size: 716 Color: 1
Size: 136 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5493 Color: 1
Size: 1877 Color: 1
Size: 374 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6333 Color: 1
Size: 1177 Color: 1
Size: 234 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6753 Color: 1
Size: 827 Color: 1
Size: 164 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 1
Size: 1204 Color: 1
Size: 232 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4642 Color: 1
Size: 2586 Color: 1
Size: 516 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6745 Color: 1
Size: 967 Color: 1
Size: 32 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 1
Size: 1661 Color: 1
Size: 330 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 3875 Color: 1
Size: 3225 Color: 1
Size: 644 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 1
Size: 1060 Color: 1
Size: 208 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 1
Size: 3084 Color: 1
Size: 416 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 1
Size: 1014 Color: 1
Size: 112 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6106 Color: 1
Size: 1366 Color: 1
Size: 272 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 1
Size: 820 Color: 1
Size: 160 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6734 Color: 1
Size: 842 Color: 1
Size: 168 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5434 Color: 1
Size: 1926 Color: 1
Size: 384 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4994 Color: 1
Size: 2294 Color: 1
Size: 456 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 1
Size: 1146 Color: 1
Size: 228 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 1
Size: 1758 Color: 1
Size: 348 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6922 Color: 1
Size: 686 Color: 1
Size: 136 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 1
Size: 1364 Color: 1
Size: 272 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6309 Color: 1
Size: 1197 Color: 1
Size: 238 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 1
Size: 908 Color: 1
Size: 176 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 2364 Color: 1
Size: 464 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 1
Size: 994 Color: 1
Size: 196 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 1
Size: 762 Color: 1
Size: 152 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 1
Size: 2724 Color: 1
Size: 536 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 1
Size: 1538 Color: 1
Size: 304 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 1
Size: 1114 Color: 1
Size: 220 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 1
Size: 1009 Color: 1
Size: 200 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6159 Color: 1
Size: 1321 Color: 1
Size: 264 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4393 Color: 1
Size: 2793 Color: 1
Size: 558 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 3873 Color: 1
Size: 3227 Color: 1
Size: 644 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6305 Color: 1
Size: 1201 Color: 1
Size: 238 Color: 0

Bin 51: 0 of cap free
Amount of items: 4
Items: 
Size: 6852 Color: 1
Size: 748 Color: 1
Size: 88 Color: 0
Size: 56 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 1
Size: 900 Color: 1
Size: 56 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 1
Size: 933 Color: 1
Size: 2 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 1
Size: 1194 Color: 1
Size: 236 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3876 Color: 1
Size: 3396 Color: 1
Size: 472 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6611 Color: 1
Size: 1001 Color: 1
Size: 132 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 1
Size: 776 Color: 1
Size: 144 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5489 Color: 1
Size: 1881 Color: 1
Size: 374 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 1
Size: 1572 Color: 1
Size: 312 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6538 Color: 1
Size: 1058 Color: 1
Size: 148 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 1
Size: 1476 Color: 1
Size: 288 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6442 Color: 1
Size: 1086 Color: 1
Size: 216 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5866 Color: 1
Size: 1566 Color: 1
Size: 312 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 1
Size: 1306 Color: 1
Size: 164 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6260 Color: 1
Size: 1244 Color: 1
Size: 240 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6701 Color: 1
Size: 1009 Color: 1
Size: 34 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 1
Size: 934 Color: 1
Size: 184 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6143 Color: 1
Size: 1335 Color: 1
Size: 266 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 1
Size: 2708 Color: 1
Size: 536 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5198 Color: 1
Size: 2122 Color: 1
Size: 424 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 1
Size: 756 Color: 1
Size: 112 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 3874 Color: 1
Size: 3226 Color: 1
Size: 644 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6717 Color: 1
Size: 857 Color: 1
Size: 170 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 1
Size: 1228 Color: 1
Size: 240 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 1
Size: 1154 Color: 1
Size: 28 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 1
Size: 1948 Color: 1
Size: 384 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 1
Size: 2108 Color: 1
Size: 416 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 1
Size: 1724 Color: 1
Size: 344 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 1
Size: 1276 Color: 1
Size: 248 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 1
Size: 1122 Color: 1
Size: 220 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 3392 Color: 1
Size: 2872 Color: 1
Size: 1480 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 1
Size: 2431 Color: 1
Size: 486 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 1
Size: 1844 Color: 1
Size: 368 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 3884 Color: 1
Size: 3220 Color: 1
Size: 640 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6417 Color: 1
Size: 1107 Color: 1
Size: 220 Color: 0

Bin 86: 0 of cap free
Amount of items: 4
Items: 
Size: 6192 Color: 1
Size: 1056 Color: 1
Size: 368 Color: 0
Size: 128 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6058 Color: 1
Size: 1558 Color: 1
Size: 128 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 1
Size: 860 Color: 1
Size: 168 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5191 Color: 1
Size: 2129 Color: 1
Size: 424 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 1
Size: 1604 Color: 1
Size: 312 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5662 Color: 1
Size: 1738 Color: 1
Size: 344 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 1
Size: 890 Color: 1
Size: 176 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 1
Size: 2052 Color: 1
Size: 408 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 1
Size: 948 Color: 1
Size: 184 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4414 Color: 1
Size: 2778 Color: 1
Size: 552 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 1
Size: 668 Color: 1
Size: 128 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6238 Color: 1
Size: 1258 Color: 1
Size: 248 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6903 Color: 1
Size: 701 Color: 1
Size: 140 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 1
Size: 1031 Color: 1
Size: 206 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4398 Color: 1
Size: 3142 Color: 1
Size: 204 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 3882 Color: 1
Size: 3222 Color: 1
Size: 640 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5948 Color: 1
Size: 1500 Color: 1
Size: 296 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6933 Color: 1
Size: 677 Color: 1
Size: 134 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 1
Size: 801 Color: 1
Size: 160 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 1
Size: 806 Color: 1
Size: 160 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6258 Color: 1
Size: 1242 Color: 1
Size: 244 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 1
Size: 3068 Color: 1
Size: 88 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6835 Color: 1
Size: 759 Color: 1
Size: 150 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 1
Size: 1108 Color: 1
Size: 216 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5977 Color: 1
Size: 1675 Color: 1
Size: 92 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 1
Size: 2002 Color: 1
Size: 396 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 1
Size: 1657 Color: 1
Size: 330 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 1
Size: 1157 Color: 1
Size: 230 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 1
Size: 989 Color: 1
Size: 138 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 1
Size: 742 Color: 1
Size: 144 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 1
Size: 1386 Color: 1
Size: 276 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6806 Color: 1
Size: 782 Color: 1
Size: 156 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 1
Size: 2429 Color: 1
Size: 484 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5989 Color: 1
Size: 1463 Color: 1
Size: 292 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6313 Color: 1
Size: 1285 Color: 1
Size: 146 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6673 Color: 1
Size: 893 Color: 1
Size: 178 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 1
Size: 917 Color: 1
Size: 182 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6377 Color: 1
Size: 1141 Color: 1
Size: 226 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5189 Color: 1
Size: 2131 Color: 1
Size: 424 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 1
Size: 1019 Color: 1
Size: 202 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 1
Size: 1477 Color: 1
Size: 294 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 4397 Color: 1
Size: 2791 Color: 1
Size: 556 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 4834 Color: 1
Size: 2426 Color: 1
Size: 484 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 1
Size: 1867 Color: 1
Size: 372 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5769 Color: 1
Size: 1647 Color: 1
Size: 328 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 1
Size: 913 Color: 1
Size: 182 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 1
Size: 653 Color: 1
Size: 130 Color: 0

Total size: 1022208
Total free space: 0

