Capicity Bin: 14688
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7352 Color: 1
Size: 6120 Color: 1
Size: 1216 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7356 Color: 1
Size: 6116 Color: 1
Size: 1216 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7362 Color: 1
Size: 6750 Color: 1
Size: 576 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7560 Color: 1
Size: 5944 Color: 1
Size: 1184 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7729 Color: 1
Size: 5829 Color: 1
Size: 1130 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7896 Color: 1
Size: 5672 Color: 1
Size: 1120 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8285 Color: 1
Size: 5475 Color: 1
Size: 928 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8372 Color: 1
Size: 5420 Color: 1
Size: 896 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8388 Color: 1
Size: 5252 Color: 1
Size: 1048 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 1
Size: 5166 Color: 1
Size: 1032 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8712 Color: 1
Size: 4984 Color: 1
Size: 992 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9032 Color: 1
Size: 4808 Color: 1
Size: 848 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 1
Size: 4520 Color: 1
Size: 896 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 1
Size: 4500 Color: 1
Size: 896 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 1
Size: 3784 Color: 1
Size: 912 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10044 Color: 1
Size: 3876 Color: 1
Size: 768 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10467 Color: 1
Size: 3519 Color: 1
Size: 702 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 1
Size: 3412 Color: 1
Size: 680 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10611 Color: 1
Size: 3455 Color: 1
Size: 622 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10639 Color: 1
Size: 3375 Color: 1
Size: 674 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 10643 Color: 1
Size: 3371 Color: 1
Size: 674 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10660 Color: 1
Size: 3364 Color: 1
Size: 664 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 10671 Color: 1
Size: 3631 Color: 1
Size: 386 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 1
Size: 3336 Color: 1
Size: 656 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10782 Color: 1
Size: 3324 Color: 1
Size: 582 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10920 Color: 1
Size: 2936 Color: 1
Size: 832 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10991 Color: 1
Size: 3081 Color: 1
Size: 616 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11050 Color: 1
Size: 2680 Color: 1
Size: 958 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11118 Color: 1
Size: 2978 Color: 1
Size: 592 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11132 Color: 1
Size: 2964 Color: 1
Size: 592 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11176 Color: 1
Size: 3144 Color: 1
Size: 368 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11225 Color: 1
Size: 2887 Color: 1
Size: 576 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11258 Color: 1
Size: 2862 Color: 1
Size: 568 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11419 Color: 1
Size: 2725 Color: 1
Size: 544 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 1
Size: 2600 Color: 1
Size: 528 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11572 Color: 1
Size: 2596 Color: 1
Size: 520 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11575 Color: 1
Size: 2595 Color: 1
Size: 518 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 1
Size: 2342 Color: 1
Size: 766 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11582 Color: 1
Size: 2616 Color: 1
Size: 490 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11615 Color: 1
Size: 2561 Color: 1
Size: 512 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 2408 Color: 1
Size: 640 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11646 Color: 1
Size: 2766 Color: 1
Size: 276 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11744 Color: 1
Size: 2168 Color: 1
Size: 776 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11753 Color: 1
Size: 2447 Color: 1
Size: 488 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 11774 Color: 1
Size: 2498 Color: 1
Size: 416 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 1
Size: 2120 Color: 1
Size: 690 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 1
Size: 2332 Color: 1
Size: 464 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11943 Color: 1
Size: 2221 Color: 1
Size: 524 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 11998 Color: 1
Size: 2140 Color: 1
Size: 550 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12073 Color: 1
Size: 2147 Color: 1
Size: 468 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 1
Size: 2000 Color: 1
Size: 584 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12113 Color: 1
Size: 2415 Color: 1
Size: 160 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12124 Color: 1
Size: 2156 Color: 1
Size: 408 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 1
Size: 2242 Color: 1
Size: 316 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 1
Size: 1784 Color: 1
Size: 752 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12153 Color: 1
Size: 2081 Color: 1
Size: 454 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12156 Color: 1
Size: 2116 Color: 1
Size: 416 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12193 Color: 1
Size: 2191 Color: 1
Size: 304 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 1
Size: 2082 Color: 1
Size: 412 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12201 Color: 1
Size: 2073 Color: 1
Size: 414 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12209 Color: 1
Size: 2067 Color: 1
Size: 412 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 1
Size: 1998 Color: 1
Size: 396 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 12329 Color: 1
Size: 1967 Color: 1
Size: 392 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 12363 Color: 1
Size: 1793 Color: 1
Size: 532 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 1
Size: 1864 Color: 1
Size: 448 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 12379 Color: 1
Size: 1925 Color: 1
Size: 384 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 1
Size: 1852 Color: 1
Size: 456 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 1
Size: 1960 Color: 1
Size: 336 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 12404 Color: 1
Size: 1908 Color: 1
Size: 376 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 12409 Color: 1
Size: 1901 Color: 1
Size: 378 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 1
Size: 1924 Color: 1
Size: 344 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 12430 Color: 1
Size: 1882 Color: 1
Size: 376 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 1928 Color: 1
Size: 304 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 12468 Color: 1
Size: 1892 Color: 1
Size: 328 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 12494 Color: 1
Size: 1830 Color: 1
Size: 364 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 12529 Color: 1
Size: 1801 Color: 1
Size: 358 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 12537 Color: 1
Size: 1835 Color: 1
Size: 316 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 1
Size: 1736 Color: 1
Size: 400 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 1
Size: 1792 Color: 1
Size: 280 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 1
Size: 1764 Color: 1
Size: 280 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 12652 Color: 1
Size: 1788 Color: 1
Size: 248 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 12661 Color: 1
Size: 1611 Color: 1
Size: 416 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 12666 Color: 1
Size: 1686 Color: 1
Size: 336 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 12685 Color: 1
Size: 1671 Color: 1
Size: 332 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 1
Size: 1542 Color: 1
Size: 438 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 12755 Color: 1
Size: 1591 Color: 1
Size: 342 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 1
Size: 1590 Color: 1
Size: 324 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 1
Size: 1704 Color: 1
Size: 208 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 12782 Color: 1
Size: 1622 Color: 1
Size: 284 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 12835 Color: 1
Size: 1545 Color: 1
Size: 308 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 12838 Color: 1
Size: 1730 Color: 1
Size: 120 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 1
Size: 1530 Color: 1
Size: 318 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 1
Size: 1540 Color: 1
Size: 304 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 1
Size: 932 Color: 0
Size: 904 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 12854 Color: 1
Size: 1420 Color: 1
Size: 414 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 1
Size: 1544 Color: 1
Size: 272 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 12923 Color: 1
Size: 1471 Color: 1
Size: 294 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 1
Size: 1532 Color: 1
Size: 200 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 1
Size: 1404 Color: 1
Size: 320 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 1
Size: 1448 Color: 1
Size: 272 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 12973 Color: 1
Size: 1431 Color: 1
Size: 284 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 1
Size: 1456 Color: 1
Size: 256 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 1
Size: 1428 Color: 1
Size: 280 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 13003 Color: 1
Size: 1405 Color: 1
Size: 280 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 1
Size: 1402 Color: 1
Size: 276 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 1
Size: 1444 Color: 1
Size: 232 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 13021 Color: 1
Size: 1443 Color: 1
Size: 224 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 1
Size: 1362 Color: 1
Size: 300 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 1
Size: 1322 Color: 1
Size: 308 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 13061 Color: 1
Size: 1357 Color: 1
Size: 270 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 13068 Color: 1
Size: 1332 Color: 1
Size: 288 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 13069 Color: 1
Size: 1351 Color: 1
Size: 268 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 1
Size: 1346 Color: 1
Size: 268 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 13087 Color: 1
Size: 1335 Color: 1
Size: 266 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 1
Size: 1336 Color: 1
Size: 260 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 13095 Color: 1
Size: 1329 Color: 1
Size: 264 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 1
Size: 1292 Color: 1
Size: 300 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 13106 Color: 1
Size: 1260 Color: 1
Size: 322 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 1
Size: 1431 Color: 1
Size: 148 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 13114 Color: 1
Size: 1314 Color: 1
Size: 260 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 13130 Color: 1
Size: 1302 Color: 1
Size: 256 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 1
Size: 1291 Color: 1
Size: 258 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 1
Size: 812 Color: 0
Size: 736 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 1442 Color: 1
Size: 70 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 1
Size: 1244 Color: 1
Size: 264 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 13194 Color: 1
Size: 1246 Color: 1
Size: 248 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 1
Size: 1242 Color: 1
Size: 248 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 13212 Color: 1
Size: 1272 Color: 1
Size: 204 Color: 0

Bin 129: 1 of cap free
Amount of items: 3
Items: 
Size: 7354 Color: 1
Size: 6117 Color: 1
Size: 1216 Color: 0

Bin 130: 1 of cap free
Amount of items: 3
Items: 
Size: 8739 Color: 1
Size: 4728 Color: 1
Size: 1220 Color: 0

Bin 131: 1 of cap free
Amount of items: 3
Items: 
Size: 9545 Color: 1
Size: 4566 Color: 1
Size: 576 Color: 0

Bin 132: 1 of cap free
Amount of items: 3
Items: 
Size: 9635 Color: 1
Size: 4684 Color: 1
Size: 368 Color: 0

Bin 133: 1 of cap free
Amount of items: 3
Items: 
Size: 9896 Color: 1
Size: 3967 Color: 1
Size: 824 Color: 0

Bin 134: 1 of cap free
Amount of items: 3
Items: 
Size: 9996 Color: 1
Size: 3471 Color: 1
Size: 1220 Color: 0

Bin 135: 1 of cap free
Amount of items: 3
Items: 
Size: 10012 Color: 1
Size: 3833 Color: 1
Size: 842 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 10333 Color: 1
Size: 4130 Color: 1
Size: 224 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 10543 Color: 1
Size: 3720 Color: 1
Size: 424 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 10582 Color: 1
Size: 3349 Color: 1
Size: 756 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 10824 Color: 1
Size: 3399 Color: 1
Size: 464 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 10951 Color: 1
Size: 3224 Color: 1
Size: 512 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 1
Size: 3115 Color: 1
Size: 384 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 11379 Color: 1
Size: 2932 Color: 1
Size: 376 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 1
Size: 2759 Color: 1
Size: 448 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 11542 Color: 1
Size: 2921 Color: 1
Size: 224 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 11670 Color: 1
Size: 2289 Color: 1
Size: 728 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 11800 Color: 1
Size: 2453 Color: 1
Size: 434 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 11901 Color: 1
Size: 2518 Color: 1
Size: 268 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 12025 Color: 1
Size: 2278 Color: 1
Size: 384 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 1
Size: 2323 Color: 1
Size: 256 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 1
Size: 2181 Color: 1
Size: 288 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 12289 Color: 1
Size: 2062 Color: 1
Size: 336 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 12474 Color: 1
Size: 1713 Color: 1
Size: 500 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 12553 Color: 1
Size: 1846 Color: 1
Size: 288 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 12609 Color: 1
Size: 1598 Color: 1
Size: 480 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 12633 Color: 1
Size: 1386 Color: 1
Size: 668 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 12724 Color: 1
Size: 1691 Color: 1
Size: 272 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 12742 Color: 1
Size: 1503 Color: 1
Size: 442 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 12779 Color: 1
Size: 1652 Color: 1
Size: 256 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 12885 Color: 1
Size: 1444 Color: 1
Size: 358 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 12951 Color: 1
Size: 1528 Color: 1
Size: 208 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 1
Size: 1449 Color: 1
Size: 276 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 9076 Color: 1
Size: 5258 Color: 1
Size: 352 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 1
Size: 4514 Color: 1
Size: 928 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 9906 Color: 1
Size: 4540 Color: 1
Size: 240 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 10138 Color: 1
Size: 3900 Color: 1
Size: 648 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 10152 Color: 1
Size: 3646 Color: 1
Size: 888 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 1
Size: 3948 Color: 1
Size: 424 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 10708 Color: 1
Size: 3422 Color: 1
Size: 556 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 1
Size: 3258 Color: 1
Size: 256 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 11494 Color: 1
Size: 2924 Color: 1
Size: 268 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 12053 Color: 1
Size: 1939 Color: 1
Size: 694 Color: 0

Bin 172: 2 of cap free
Amount of items: 3
Items: 
Size: 12614 Color: 1
Size: 1644 Color: 1
Size: 428 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 8420 Color: 1
Size: 5561 Color: 1
Size: 704 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 10165 Color: 1
Size: 4008 Color: 1
Size: 512 Color: 0

Bin 175: 3 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 1
Size: 4149 Color: 1
Size: 304 Color: 0

Bin 176: 3 of cap free
Amount of items: 3
Items: 
Size: 10523 Color: 1
Size: 4066 Color: 1
Size: 96 Color: 0

Bin 177: 3 of cap free
Amount of items: 3
Items: 
Size: 11745 Color: 1
Size: 2604 Color: 1
Size: 336 Color: 0

Bin 178: 3 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 1
Size: 2009 Color: 1
Size: 776 Color: 0

Bin 179: 4 of cap free
Amount of items: 3
Items: 
Size: 8382 Color: 1
Size: 5886 Color: 1
Size: 416 Color: 0

Bin 180: 4 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 1
Size: 5024 Color: 1
Size: 724 Color: 0

Bin 181: 4 of cap free
Amount of items: 3
Items: 
Size: 9091 Color: 1
Size: 4665 Color: 1
Size: 928 Color: 0

Bin 182: 5 of cap free
Amount of items: 3
Items: 
Size: 10089 Color: 1
Size: 3916 Color: 1
Size: 678 Color: 0

Bin 183: 5 of cap free
Amount of items: 3
Items: 
Size: 11185 Color: 1
Size: 2388 Color: 1
Size: 1110 Color: 0

Bin 184: 6 of cap free
Amount of items: 3
Items: 
Size: 9810 Color: 1
Size: 3928 Color: 1
Size: 944 Color: 0

Bin 185: 7 of cap free
Amount of items: 3
Items: 
Size: 9118 Color: 1
Size: 4795 Color: 1
Size: 768 Color: 0

Bin 186: 7 of cap free
Amount of items: 3
Items: 
Size: 9711 Color: 1
Size: 3794 Color: 1
Size: 1176 Color: 0

Bin 187: 8 of cap free
Amount of items: 3
Items: 
Size: 9210 Color: 1
Size: 4642 Color: 1
Size: 828 Color: 0

Bin 188: 9 of cap free
Amount of items: 3
Items: 
Size: 8017 Color: 1
Size: 5440 Color: 1
Size: 1222 Color: 0

Bin 189: 9 of cap free
Amount of items: 3
Items: 
Size: 9956 Color: 1
Size: 4211 Color: 1
Size: 512 Color: 0

Bin 190: 10 of cap free
Amount of items: 3
Items: 
Size: 7626 Color: 1
Size: 6124 Color: 1
Size: 928 Color: 0

Bin 191: 10 of cap free
Amount of items: 3
Items: 
Size: 8935 Color: 1
Size: 4959 Color: 1
Size: 784 Color: 0

Bin 192: 11 of cap free
Amount of items: 3
Items: 
Size: 9734 Color: 1
Size: 4287 Color: 1
Size: 656 Color: 0

Bin 193: 35 of cap free
Amount of items: 3
Items: 
Size: 7349 Color: 1
Size: 6448 Color: 1
Size: 856 Color: 0

Bin 194: 83 of cap free
Amount of items: 3
Items: 
Size: 7348 Color: 1
Size: 6889 Color: 1
Size: 368 Color: 0

Bin 195: 437 of cap free
Amount of items: 3
Items: 
Size: 7345 Color: 1
Size: 6122 Color: 1
Size: 784 Color: 0

Bin 196: 610 of cap free
Amount of items: 3
Items: 
Size: 7280 Color: 1
Size: 6114 Color: 1
Size: 684 Color: 0

Bin 197: 1226 of cap free
Amount of items: 5
Items: 
Size: 7346 Color: 1
Size: 3118 Color: 1
Size: 1048 Color: 0
Size: 990 Color: 0
Size: 960 Color: 0

Bin 198: 2496 of cap free
Amount of items: 5
Items: 
Size: 5428 Color: 1
Size: 2902 Color: 1
Size: 2662 Color: 1
Size: 624 Color: 0
Size: 576 Color: 0

Bin 199: 9629 of cap free
Amount of items: 3
Items: 
Size: 2622 Color: 1
Size: 2197 Color: 1
Size: 240 Color: 0

Total size: 2908224
Total free space: 14688

