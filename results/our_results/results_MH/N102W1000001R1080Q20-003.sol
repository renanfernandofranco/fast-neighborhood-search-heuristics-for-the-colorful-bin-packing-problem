Capicity Bin: 1000001
Lower Bound: 49

Bins used: 54
Amount of Colors: 20

Bin 1: 3 of cap free
Amount of items: 3
Items: 
Size: 569036 Color: 6
Size: 324798 Color: 9
Size: 106164 Color: 2

Bin 2: 20 of cap free
Amount of items: 4
Items: 
Size: 548422 Color: 8
Size: 172804 Color: 17
Size: 146935 Color: 13
Size: 131820 Color: 7

Bin 3: 26 of cap free
Amount of items: 3
Items: 
Size: 548637 Color: 9
Size: 323672 Color: 17
Size: 127666 Color: 19

Bin 4: 38 of cap free
Amount of items: 3
Items: 
Size: 581340 Color: 17
Size: 250200 Color: 2
Size: 168423 Color: 14

Bin 5: 38 of cap free
Amount of items: 3
Items: 
Size: 628429 Color: 18
Size: 214671 Color: 14
Size: 156863 Color: 17

Bin 6: 52 of cap free
Amount of items: 3
Items: 
Size: 557941 Color: 13
Size: 231749 Color: 18
Size: 210259 Color: 14

Bin 7: 62 of cap free
Amount of items: 3
Items: 
Size: 566928 Color: 8
Size: 325995 Color: 13
Size: 107016 Color: 16

Bin 8: 271 of cap free
Amount of items: 3
Items: 
Size: 591709 Color: 1
Size: 252177 Color: 11
Size: 155844 Color: 18

Bin 9: 572 of cap free
Amount of items: 2
Items: 
Size: 630031 Color: 16
Size: 369398 Color: 12

Bin 10: 575 of cap free
Amount of items: 2
Items: 
Size: 787631 Color: 19
Size: 211795 Color: 0

Bin 11: 676 of cap free
Amount of items: 4
Items: 
Size: 609169 Color: 10
Size: 151255 Color: 12
Size: 122101 Color: 1
Size: 116800 Color: 17

Bin 12: 908 of cap free
Amount of items: 2
Items: 
Size: 716400 Color: 4
Size: 282693 Color: 14

Bin 13: 1135 of cap free
Amount of items: 2
Items: 
Size: 518821 Color: 1
Size: 480045 Color: 18

Bin 14: 1550 of cap free
Amount of items: 2
Items: 
Size: 521580 Color: 2
Size: 476871 Color: 9

Bin 15: 1816 of cap free
Amount of items: 3
Items: 
Size: 583911 Color: 3
Size: 226495 Color: 3
Size: 187779 Color: 18

Bin 16: 2061 of cap free
Amount of items: 2
Items: 
Size: 575838 Color: 15
Size: 422102 Color: 7

Bin 17: 2143 of cap free
Amount of items: 2
Items: 
Size: 652199 Color: 9
Size: 345659 Color: 15

Bin 18: 2582 of cap free
Amount of items: 2
Items: 
Size: 611269 Color: 11
Size: 386150 Color: 13

Bin 19: 2613 of cap free
Amount of items: 4
Items: 
Size: 601134 Color: 15
Size: 152608 Color: 18
Size: 134318 Color: 2
Size: 109328 Color: 7

Bin 20: 3587 of cap free
Amount of items: 2
Items: 
Size: 536100 Color: 19
Size: 460314 Color: 10

Bin 21: 3771 of cap free
Amount of items: 2
Items: 
Size: 659896 Color: 6
Size: 336334 Color: 7

Bin 22: 4281 of cap free
Amount of items: 2
Items: 
Size: 574270 Color: 17
Size: 421450 Color: 0

Bin 23: 5330 of cap free
Amount of items: 2
Items: 
Size: 543074 Color: 7
Size: 451597 Color: 6

Bin 24: 5493 of cap free
Amount of items: 2
Items: 
Size: 642569 Color: 13
Size: 351939 Color: 6

Bin 25: 8340 of cap free
Amount of items: 2
Items: 
Size: 514905 Color: 11
Size: 476756 Color: 5

Bin 26: 9030 of cap free
Amount of items: 2
Items: 
Size: 664337 Color: 0
Size: 326634 Color: 9

Bin 27: 10777 of cap free
Amount of items: 2
Items: 
Size: 639731 Color: 17
Size: 349493 Color: 8

Bin 28: 10848 of cap free
Amount of items: 2
Items: 
Size: 552752 Color: 8
Size: 436401 Color: 3

Bin 29: 13573 of cap free
Amount of items: 2
Items: 
Size: 581863 Color: 1
Size: 404565 Color: 6

Bin 30: 13955 of cap free
Amount of items: 2
Items: 
Size: 637278 Color: 4
Size: 348768 Color: 5

Bin 31: 15124 of cap free
Amount of items: 2
Items: 
Size: 526774 Color: 3
Size: 458103 Color: 12

Bin 32: 20039 of cap free
Amount of items: 2
Items: 
Size: 659720 Color: 19
Size: 320242 Color: 15

Bin 33: 20836 of cap free
Amount of items: 2
Items: 
Size: 580258 Color: 5
Size: 398907 Color: 18

Bin 34: 25907 of cap free
Amount of items: 2
Items: 
Size: 631412 Color: 19
Size: 342682 Color: 1

Bin 35: 205060 of cap free
Amount of items: 1
Items: 
Size: 794941 Color: 16

Bin 36: 221627 of cap free
Amount of items: 1
Items: 
Size: 778374 Color: 14

Bin 37: 223062 of cap free
Amount of items: 1
Items: 
Size: 776939 Color: 13

Bin 38: 238059 of cap free
Amount of items: 1
Items: 
Size: 761942 Color: 18

Bin 39: 242029 of cap free
Amount of items: 1
Items: 
Size: 757972 Color: 14

Bin 40: 247585 of cap free
Amount of items: 1
Items: 
Size: 752416 Color: 19

Bin 41: 247822 of cap free
Amount of items: 1
Items: 
Size: 752179 Color: 10

Bin 42: 248082 of cap free
Amount of items: 1
Items: 
Size: 751919 Color: 14

Bin 43: 266260 of cap free
Amount of items: 1
Items: 
Size: 733741 Color: 10

Bin 44: 271276 of cap free
Amount of items: 1
Items: 
Size: 728725 Color: 4

Bin 45: 273161 of cap free
Amount of items: 1
Items: 
Size: 726840 Color: 10

Bin 46: 289211 of cap free
Amount of items: 1
Items: 
Size: 710790 Color: 12

Bin 47: 289639 of cap free
Amount of items: 1
Items: 
Size: 710362 Color: 12

Bin 48: 299474 of cap free
Amount of items: 1
Items: 
Size: 700527 Color: 11

Bin 49: 306620 of cap free
Amount of items: 1
Items: 
Size: 693381 Color: 8

Bin 50: 307404 of cap free
Amount of items: 1
Items: 
Size: 692597 Color: 18

Bin 51: 314877 of cap free
Amount of items: 1
Items: 
Size: 685124 Color: 9

Bin 52: 317670 of cap free
Amount of items: 1
Items: 
Size: 682331 Color: 10

Bin 53: 317962 of cap free
Amount of items: 1
Items: 
Size: 682039 Color: 9

Bin 54: 340909 of cap free
Amount of items: 1
Items: 
Size: 659092 Color: 16

Total size: 48344233
Total free space: 5655821

