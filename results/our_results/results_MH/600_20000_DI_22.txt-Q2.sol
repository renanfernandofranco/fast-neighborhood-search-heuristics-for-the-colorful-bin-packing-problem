Capicity Bin: 16416
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8220 Color: 0
Size: 1581 Color: 1
Size: 1507 Color: 1
Size: 1488 Color: 0
Size: 1420 Color: 0
Size: 1336 Color: 0
Size: 864 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8248 Color: 0
Size: 2992 Color: 0
Size: 2336 Color: 1
Size: 1528 Color: 0
Size: 1312 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 0
Size: 6444 Color: 0
Size: 728 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9314 Color: 0
Size: 6602 Color: 1
Size: 500 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10100 Color: 0
Size: 5852 Color: 0
Size: 464 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11504 Color: 1
Size: 4560 Color: 1
Size: 352 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11802 Color: 1
Size: 3286 Color: 0
Size: 1328 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 0
Size: 4180 Color: 1
Size: 324 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12190 Color: 1
Size: 3822 Color: 1
Size: 404 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12196 Color: 1
Size: 2164 Color: 0
Size: 2056 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 1
Size: 3344 Color: 0
Size: 824 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12228 Color: 0
Size: 3768 Color: 1
Size: 420 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12260 Color: 1
Size: 3846 Color: 1
Size: 310 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 0
Size: 3424 Color: 0
Size: 400 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 1
Size: 2542 Color: 0
Size: 476 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 0
Size: 1564 Color: 0
Size: 1364 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 2324 Color: 1
Size: 628 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13864 Color: 1
Size: 1452 Color: 1
Size: 1100 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 1
Size: 2077 Color: 1
Size: 418 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13936 Color: 1
Size: 2184 Color: 0
Size: 296 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 1
Size: 1772 Color: 0
Size: 480 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 1
Size: 1152 Color: 1
Size: 1040 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 1
Size: 1456 Color: 1
Size: 696 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14261 Color: 0
Size: 1797 Color: 1
Size: 358 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14314 Color: 1
Size: 1796 Color: 0
Size: 306 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14352 Color: 1
Size: 1108 Color: 0
Size: 956 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 1
Size: 1344 Color: 0
Size: 596 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14521 Color: 1
Size: 1537 Color: 1
Size: 358 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14597 Color: 0
Size: 1541 Color: 0
Size: 278 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 1436 Color: 0
Size: 368 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14704 Color: 0
Size: 944 Color: 1
Size: 768 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 1
Size: 1024 Color: 1
Size: 676 Color: 0

Bin 33: 1 of cap free
Amount of items: 15
Items: 
Size: 8209 Color: 1
Size: 684 Color: 0
Size: 672 Color: 0
Size: 608 Color: 1
Size: 608 Color: 0
Size: 608 Color: 0
Size: 608 Color: 0
Size: 604 Color: 0
Size: 604 Color: 0
Size: 600 Color: 1
Size: 596 Color: 1
Size: 592 Color: 1
Size: 592 Color: 1
Size: 416 Color: 1
Size: 414 Color: 0

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 11209 Color: 1
Size: 5206 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 0
Size: 3911 Color: 0
Size: 192 Color: 1

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 12303 Color: 1
Size: 4112 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 12978 Color: 0
Size: 2073 Color: 1
Size: 1364 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 1
Size: 1771 Color: 0
Size: 752 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 0
Size: 2093 Color: 1
Size: 128 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 14257 Color: 0
Size: 1358 Color: 1
Size: 800 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 14265 Color: 1
Size: 1462 Color: 1
Size: 688 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 14545 Color: 1
Size: 1582 Color: 0
Size: 288 Color: 1

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 0
Size: 5506 Color: 1
Size: 680 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 11894 Color: 0
Size: 4044 Color: 1
Size: 476 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 1
Size: 3386 Color: 0
Size: 896 Color: 1

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 0
Size: 3572 Color: 1

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 13252 Color: 0
Size: 3162 Color: 1

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 13366 Color: 1
Size: 3048 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 13925 Color: 1
Size: 1561 Color: 0
Size: 928 Color: 0

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 1
Size: 2390 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 14193 Color: 1
Size: 1773 Color: 0
Size: 448 Color: 1

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 14573 Color: 0
Size: 1813 Color: 1
Size: 28 Color: 0

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 0
Size: 5765 Color: 1
Size: 352 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 11879 Color: 1
Size: 4158 Color: 1
Size: 376 Color: 0

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 12291 Color: 1
Size: 3770 Color: 1
Size: 352 Color: 0

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 1
Size: 3213 Color: 0

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 14640 Color: 0
Size: 1621 Color: 1
Size: 152 Color: 0

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 1
Size: 6624 Color: 0
Size: 480 Color: 0

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 10164 Color: 1
Size: 5848 Color: 0
Size: 400 Color: 0

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 13550 Color: 0
Size: 1482 Color: 1
Size: 1380 Color: 0

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 1
Size: 2584 Color: 0

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 1
Size: 1908 Color: 0
Size: 64 Color: 0

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 1
Size: 1840 Color: 0
Size: 52 Color: 0

Bin 64: 4 of cap free
Amount of items: 4
Items: 
Size: 14676 Color: 1
Size: 1620 Color: 0
Size: 84 Color: 1
Size: 32 Color: 0

Bin 65: 5 of cap free
Amount of items: 3
Items: 
Size: 8393 Color: 1
Size: 6826 Color: 1
Size: 1192 Color: 0

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 11170 Color: 1
Size: 4969 Color: 0
Size: 272 Color: 1

Bin 67: 5 of cap free
Amount of items: 3
Items: 
Size: 11824 Color: 1
Size: 4331 Color: 1
Size: 256 Color: 0

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 13750 Color: 0
Size: 2661 Color: 1

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 9104 Color: 0
Size: 7306 Color: 1

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 9810 Color: 1
Size: 6600 Color: 0

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 10936 Color: 0
Size: 4738 Color: 1
Size: 736 Color: 0

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 12036 Color: 0
Size: 4374 Color: 1

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 12676 Color: 0
Size: 2982 Color: 1
Size: 752 Color: 1

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 13890 Color: 0
Size: 2520 Color: 1

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 0
Size: 1744 Color: 1

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 14609 Color: 1
Size: 1800 Color: 0

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 11360 Color: 0
Size: 5048 Color: 1

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 11900 Color: 1
Size: 4508 Color: 0

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 12080 Color: 1
Size: 4328 Color: 0

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 12464 Color: 1
Size: 3944 Color: 0

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 0
Size: 3124 Color: 1

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 13632 Color: 1
Size: 2776 Color: 0

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 1
Size: 1824 Color: 0

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 1
Size: 1644 Color: 0
Size: 64 Color: 1

Bin 85: 9 of cap free
Amount of items: 3
Items: 
Size: 10304 Color: 0
Size: 5815 Color: 1
Size: 288 Color: 1

Bin 86: 9 of cap free
Amount of items: 3
Items: 
Size: 13905 Color: 0
Size: 2342 Color: 1
Size: 160 Color: 0

Bin 87: 10 of cap free
Amount of items: 5
Items: 
Size: 8226 Color: 1
Size: 2990 Color: 0
Size: 2081 Color: 1
Size: 1592 Color: 0
Size: 1517 Color: 1

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 0
Size: 1884 Color: 1

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 14548 Color: 1
Size: 1858 Color: 0

Bin 90: 10 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 0
Size: 1604 Color: 1
Size: 164 Color: 1

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 13957 Color: 1
Size: 2448 Color: 0

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 10916 Color: 1
Size: 5488 Color: 0

Bin 93: 12 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 0
Size: 2988 Color: 1

Bin 94: 14 of cap free
Amount of items: 3
Items: 
Size: 10812 Color: 1
Size: 5268 Color: 1
Size: 322 Color: 0

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 1
Size: 4130 Color: 0

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 12626 Color: 1
Size: 3776 Color: 0

Bin 97: 14 of cap free
Amount of items: 3
Items: 
Size: 14569 Color: 0
Size: 1801 Color: 1
Size: 32 Color: 0

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 0
Size: 1754 Color: 1

Bin 99: 14 of cap free
Amount of items: 3
Items: 
Size: 14764 Color: 0
Size: 1634 Color: 1
Size: 4 Color: 0

Bin 100: 15 of cap free
Amount of items: 3
Items: 
Size: 12100 Color: 1
Size: 3429 Color: 0
Size: 872 Color: 0

Bin 101: 15 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 1
Size: 2472 Color: 0

Bin 102: 15 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 1
Size: 2133 Color: 0

Bin 103: 16 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 0
Size: 6840 Color: 1
Size: 208 Color: 1

Bin 104: 16 of cap free
Amount of items: 3
Items: 
Size: 14176 Color: 0
Size: 2096 Color: 1
Size: 128 Color: 0

Bin 105: 17 of cap free
Amount of items: 2
Items: 
Size: 14012 Color: 0
Size: 2387 Color: 1

Bin 106: 17 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 1
Size: 2106 Color: 0

Bin 107: 18 of cap free
Amount of items: 8
Items: 
Size: 8216 Color: 0
Size: 1360 Color: 0
Size: 1360 Color: 0
Size: 1172 Color: 1
Size: 1168 Color: 1
Size: 1162 Color: 1
Size: 1048 Color: 0
Size: 912 Color: 1

Bin 108: 18 of cap free
Amount of items: 2
Items: 
Size: 11012 Color: 1
Size: 5386 Color: 0

Bin 109: 18 of cap free
Amount of items: 2
Items: 
Size: 11830 Color: 1
Size: 4568 Color: 0

Bin 110: 18 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 1
Size: 3096 Color: 0

Bin 111: 18 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 0
Size: 2598 Color: 1

Bin 112: 18 of cap free
Amount of items: 2
Items: 
Size: 14394 Color: 0
Size: 2004 Color: 1

Bin 113: 18 of cap free
Amount of items: 2
Items: 
Size: 14500 Color: 0
Size: 1898 Color: 1

Bin 114: 19 of cap free
Amount of items: 2
Items: 
Size: 9236 Color: 0
Size: 7161 Color: 1

Bin 115: 19 of cap free
Amount of items: 2
Items: 
Size: 9840 Color: 0
Size: 6557 Color: 1

Bin 116: 20 of cap free
Amount of items: 2
Items: 
Size: 10960 Color: 1
Size: 5436 Color: 0

Bin 117: 21 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 1
Size: 3604 Color: 0

Bin 118: 21 of cap free
Amount of items: 2
Items: 
Size: 13223 Color: 1
Size: 3172 Color: 0

Bin 119: 22 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 1
Size: 2722 Color: 0

Bin 120: 23 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 0
Size: 3031 Color: 1
Size: 320 Color: 1

Bin 121: 23 of cap free
Amount of items: 2
Items: 
Size: 14473 Color: 1
Size: 1920 Color: 0

Bin 122: 23 of cap free
Amount of items: 2
Items: 
Size: 14737 Color: 0
Size: 1656 Color: 1

Bin 123: 24 of cap free
Amount of items: 3
Items: 
Size: 11891 Color: 1
Size: 4341 Color: 0
Size: 160 Color: 1

Bin 124: 24 of cap free
Amount of items: 2
Items: 
Size: 12740 Color: 0
Size: 3652 Color: 1

Bin 125: 25 of cap free
Amount of items: 2
Items: 
Size: 14537 Color: 0
Size: 1854 Color: 1

Bin 126: 26 of cap free
Amount of items: 12
Items: 
Size: 8212 Color: 1
Size: 866 Color: 0
Size: 864 Color: 0
Size: 832 Color: 0
Size: 828 Color: 1
Size: 800 Color: 1
Size: 784 Color: 1
Size: 764 Color: 0
Size: 736 Color: 0
Size: 712 Color: 1
Size: 672 Color: 1
Size: 320 Color: 0

Bin 127: 26 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 1
Size: 5536 Color: 1
Size: 900 Color: 0

Bin 128: 26 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 1
Size: 2612 Color: 0

Bin 129: 27 of cap free
Amount of items: 3
Items: 
Size: 8265 Color: 1
Size: 7688 Color: 0
Size: 436 Color: 1

Bin 130: 28 of cap free
Amount of items: 2
Items: 
Size: 11224 Color: 0
Size: 5164 Color: 1

Bin 131: 28 of cap free
Amount of items: 2
Items: 
Size: 14132 Color: 0
Size: 2256 Color: 1

Bin 132: 29 of cap free
Amount of items: 6
Items: 
Size: 8240 Color: 0
Size: 2053 Color: 1
Size: 2051 Color: 1
Size: 1824 Color: 0
Size: 1793 Color: 0
Size: 426 Color: 1

Bin 133: 31 of cap free
Amount of items: 2
Items: 
Size: 10405 Color: 1
Size: 5980 Color: 0

Bin 134: 31 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 0
Size: 2528 Color: 1

Bin 135: 32 of cap free
Amount of items: 2
Items: 
Size: 10670 Color: 0
Size: 5714 Color: 1

Bin 136: 35 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 0
Size: 3334 Color: 0
Size: 1585 Color: 1

Bin 137: 36 of cap free
Amount of items: 2
Items: 
Size: 11404 Color: 1
Size: 4976 Color: 0

Bin 138: 37 of cap free
Amount of items: 2
Items: 
Size: 10455 Color: 1
Size: 5924 Color: 0

Bin 139: 40 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 1
Size: 3764 Color: 0

Bin 140: 40 of cap free
Amount of items: 2
Items: 
Size: 13332 Color: 1
Size: 3044 Color: 0

Bin 141: 41 of cap free
Amount of items: 3
Items: 
Size: 14458 Color: 0
Size: 1853 Color: 1
Size: 64 Color: 0

Bin 142: 44 of cap free
Amount of items: 2
Items: 
Size: 12164 Color: 1
Size: 4208 Color: 0

Bin 143: 46 of cap free
Amount of items: 2
Items: 
Size: 12848 Color: 0
Size: 3522 Color: 1

Bin 144: 49 of cap free
Amount of items: 2
Items: 
Size: 13553 Color: 0
Size: 2814 Color: 1

Bin 145: 52 of cap free
Amount of items: 2
Items: 
Size: 11688 Color: 0
Size: 4676 Color: 1

Bin 146: 54 of cap free
Amount of items: 2
Items: 
Size: 11572 Color: 1
Size: 4790 Color: 0

Bin 147: 58 of cap free
Amount of items: 2
Items: 
Size: 12786 Color: 1
Size: 3572 Color: 0

Bin 148: 60 of cap free
Amount of items: 2
Items: 
Size: 13628 Color: 0
Size: 2728 Color: 1

Bin 149: 62 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 0
Size: 6836 Color: 1
Size: 1076 Color: 0

Bin 150: 64 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 1
Size: 2040 Color: 0

Bin 151: 66 of cap free
Amount of items: 2
Items: 
Size: 9663 Color: 0
Size: 6687 Color: 1

Bin 152: 67 of cap free
Amount of items: 2
Items: 
Size: 14241 Color: 0
Size: 2108 Color: 1

Bin 153: 69 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 1
Size: 2361 Color: 0

Bin 154: 72 of cap free
Amount of items: 2
Items: 
Size: 10448 Color: 0
Size: 5896 Color: 1

Bin 155: 72 of cap free
Amount of items: 2
Items: 
Size: 14142 Color: 1
Size: 2202 Color: 0

Bin 156: 80 of cap free
Amount of items: 2
Items: 
Size: 10680 Color: 1
Size: 5656 Color: 0

Bin 157: 83 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 0
Size: 6837 Color: 1
Size: 992 Color: 1

Bin 158: 88 of cap free
Amount of items: 2
Items: 
Size: 12836 Color: 0
Size: 3492 Color: 1

Bin 159: 88 of cap free
Amount of items: 2
Items: 
Size: 13744 Color: 0
Size: 2584 Color: 1

Bin 160: 91 of cap free
Amount of items: 2
Items: 
Size: 9416 Color: 0
Size: 6909 Color: 1

Bin 161: 98 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 1
Size: 2026 Color: 0

Bin 162: 99 of cap free
Amount of items: 2
Items: 
Size: 9483 Color: 1
Size: 6834 Color: 0

Bin 163: 99 of cap free
Amount of items: 2
Items: 
Size: 12781 Color: 1
Size: 3536 Color: 0

Bin 164: 103 of cap free
Amount of items: 2
Items: 
Size: 13953 Color: 1
Size: 2360 Color: 0

Bin 165: 104 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 1
Size: 2704 Color: 0

Bin 166: 108 of cap free
Amount of items: 3
Items: 
Size: 10734 Color: 0
Size: 3002 Color: 1
Size: 2572 Color: 0

Bin 167: 108 of cap free
Amount of items: 2
Items: 
Size: 13960 Color: 0
Size: 2348 Color: 1

Bin 168: 118 of cap free
Amount of items: 2
Items: 
Size: 13606 Color: 1
Size: 2692 Color: 0

Bin 169: 119 of cap free
Amount of items: 2
Items: 
Size: 14289 Color: 1
Size: 2008 Color: 0

Bin 170: 121 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 1
Size: 2691 Color: 0

Bin 171: 126 of cap free
Amount of items: 34
Items: 
Size: 604 Color: 0
Size: 600 Color: 0
Size: 576 Color: 0
Size: 560 Color: 1
Size: 544 Color: 1
Size: 544 Color: 1
Size: 544 Color: 0
Size: 544 Color: 0
Size: 536 Color: 1
Size: 536 Color: 1
Size: 532 Color: 0
Size: 520 Color: 1
Size: 516 Color: 0
Size: 512 Color: 1
Size: 512 Color: 1
Size: 508 Color: 0
Size: 480 Color: 1
Size: 472 Color: 0
Size: 468 Color: 0
Size: 464 Color: 1
Size: 464 Color: 0
Size: 432 Color: 0
Size: 424 Color: 1
Size: 416 Color: 1
Size: 416 Color: 1
Size: 414 Color: 0
Size: 414 Color: 0
Size: 410 Color: 0
Size: 408 Color: 0
Size: 400 Color: 1
Size: 384 Color: 1
Size: 384 Color: 0
Size: 376 Color: 1
Size: 376 Color: 1

Bin 172: 126 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 0
Size: 3472 Color: 1

Bin 173: 129 of cap free
Amount of items: 2
Items: 
Size: 9455 Color: 1
Size: 6832 Color: 0

Bin 174: 132 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 1
Size: 3524 Color: 0

Bin 175: 133 of cap free
Amount of items: 2
Items: 
Size: 12354 Color: 1
Size: 3929 Color: 0

Bin 176: 142 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 0
Size: 3856 Color: 1

Bin 177: 150 of cap free
Amount of items: 2
Items: 
Size: 10170 Color: 1
Size: 6096 Color: 0

Bin 178: 166 of cap free
Amount of items: 2
Items: 
Size: 13150 Color: 0
Size: 3100 Color: 1

Bin 179: 173 of cap free
Amount of items: 2
Items: 
Size: 12804 Color: 0
Size: 3439 Color: 1

Bin 180: 176 of cap free
Amount of items: 2
Items: 
Size: 9594 Color: 0
Size: 6646 Color: 1

Bin 181: 180 of cap free
Amount of items: 2
Items: 
Size: 13144 Color: 0
Size: 3092 Color: 1

Bin 182: 184 of cap free
Amount of items: 2
Items: 
Size: 9439 Color: 1
Size: 6793 Color: 0

Bin 183: 184 of cap free
Amount of items: 2
Items: 
Size: 11221 Color: 0
Size: 5011 Color: 1

Bin 184: 190 of cap free
Amount of items: 2
Items: 
Size: 10360 Color: 0
Size: 5866 Color: 1

Bin 185: 191 of cap free
Amount of items: 7
Items: 
Size: 8218 Color: 1
Size: 1480 Color: 1
Size: 1401 Color: 0
Size: 1366 Color: 0
Size: 1360 Color: 0
Size: 1216 Color: 1
Size: 1184 Color: 1

Bin 186: 191 of cap free
Amount of items: 2
Items: 
Size: 13199 Color: 1
Size: 3026 Color: 0

Bin 187: 194 of cap free
Amount of items: 2
Items: 
Size: 9378 Color: 0
Size: 6844 Color: 1

Bin 188: 194 of cap free
Amount of items: 2
Items: 
Size: 11430 Color: 0
Size: 4792 Color: 1

Bin 189: 206 of cap free
Amount of items: 2
Items: 
Size: 13189 Color: 1
Size: 3021 Color: 0

Bin 190: 212 of cap free
Amount of items: 2
Items: 
Size: 12772 Color: 0
Size: 3432 Color: 1

Bin 191: 214 of cap free
Amount of items: 2
Items: 
Size: 12722 Color: 1
Size: 3480 Color: 0

Bin 192: 216 of cap free
Amount of items: 2
Items: 
Size: 13188 Color: 1
Size: 3012 Color: 0

Bin 193: 218 of cap free
Amount of items: 13
Items: 
Size: 8210 Color: 1
Size: 736 Color: 0
Size: 712 Color: 0
Size: 704 Color: 0
Size: 696 Color: 0
Size: 688 Color: 0
Size: 686 Color: 0
Size: 664 Color: 1
Size: 632 Color: 1
Size: 626 Color: 1
Size: 616 Color: 1
Size: 616 Color: 1
Size: 612 Color: 1

Bin 194: 236 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 1
Size: 3468 Color: 0

Bin 195: 238 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 0
Size: 3082 Color: 1

Bin 196: 240 of cap free
Amount of items: 2
Items: 
Size: 12708 Color: 1
Size: 3468 Color: 0

Bin 197: 241 of cap free
Amount of items: 9
Items: 
Size: 8213 Color: 0
Size: 1088 Color: 1
Size: 1046 Color: 0
Size: 1008 Color: 0
Size: 1000 Color: 0
Size: 992 Color: 0
Size: 988 Color: 1
Size: 944 Color: 1
Size: 896 Color: 1

Bin 198: 251 of cap free
Amount of items: 2
Items: 
Size: 13583 Color: 1
Size: 2582 Color: 0

Bin 199: 7444 of cap free
Amount of items: 28
Items: 
Size: 384 Color: 0
Size: 370 Color: 0
Size: 368 Color: 1
Size: 362 Color: 1
Size: 358 Color: 0
Size: 354 Color: 1
Size: 352 Color: 1
Size: 352 Color: 0
Size: 348 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 314 Color: 0
Size: 312 Color: 1
Size: 312 Color: 1
Size: 306 Color: 0
Size: 304 Color: 1
Size: 304 Color: 1
Size: 304 Color: 1
Size: 302 Color: 1
Size: 300 Color: 0
Size: 294 Color: 0
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 280 Color: 0
Size: 280 Color: 0

Total size: 3250368
Total free space: 16416

