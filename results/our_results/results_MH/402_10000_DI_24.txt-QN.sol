Capicity Bin: 8192
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4098 Color: 274
Size: 1092 Color: 174
Size: 1078 Color: 172
Size: 1076 Color: 171
Size: 384 Color: 98
Size: 232 Color: 56
Size: 232 Color: 55

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4102 Color: 277
Size: 3410 Color: 266
Size: 236 Color: 57
Size: 224 Color: 52
Size: 220 Color: 51

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4708 Color: 287
Size: 3308 Color: 264
Size: 176 Color: 34

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 5172 Color: 294
Size: 2524 Color: 248
Size: 166 Color: 29
Size: 166 Color: 28
Size: 164 Color: 27

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 308
Size: 1313 Color: 192
Size: 1126 Color: 177

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 324
Size: 1014 Color: 163
Size: 964 Color: 159

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 326
Size: 1804 Color: 222
Size: 144 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6297 Color: 329
Size: 1567 Color: 208
Size: 328 Color: 85

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6438 Color: 334
Size: 1180 Color: 180
Size: 574 Color: 122

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 335
Size: 1492 Color: 206
Size: 248 Color: 61

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6453 Color: 336
Size: 1451 Color: 203
Size: 288 Color: 75

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 342
Size: 1018 Color: 164
Size: 584 Color: 125

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6637 Color: 348
Size: 1187 Color: 181
Size: 368 Color: 92

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6641 Color: 349
Size: 1229 Color: 185
Size: 322 Color: 83

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 350
Size: 1332 Color: 197
Size: 208 Color: 45

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 351
Size: 1251 Color: 187
Size: 284 Color: 73

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 352
Size: 1321 Color: 194
Size: 210 Color: 46

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 355
Size: 1090 Color: 173
Size: 352 Color: 88

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 356
Size: 1324 Color: 195
Size: 112 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 358
Size: 892 Color: 154
Size: 520 Color: 120

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6881 Color: 364
Size: 953 Color: 158
Size: 358 Color: 90

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6905 Color: 367
Size: 1093 Color: 175
Size: 194 Color: 40

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 371
Size: 716 Color: 138
Size: 504 Color: 115

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6974 Color: 372
Size: 894 Color: 155
Size: 324 Color: 84

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 376
Size: 972 Color: 160
Size: 192 Color: 39

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7037 Color: 377
Size: 1019 Color: 165
Size: 136 Color: 12

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 378
Size: 742 Color: 142
Size: 406 Color: 102

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 379
Size: 682 Color: 136
Size: 464 Color: 112

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 383
Size: 836 Color: 150
Size: 270 Color: 72

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 388
Size: 758 Color: 143
Size: 254 Color: 62

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 389
Size: 764 Color: 145
Size: 232 Color: 54

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 391
Size: 790 Color: 147
Size: 156 Color: 22

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 393
Size: 764 Color: 146
Size: 152 Color: 20

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7280 Color: 394
Size: 520 Color: 119
Size: 392 Color: 99

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7284 Color: 395
Size: 760 Color: 144
Size: 148 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 396
Size: 706 Color: 137
Size: 200 Color: 43

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 397
Size: 730 Color: 141
Size: 160 Color: 25

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 399
Size: 576 Color: 123
Size: 292 Color: 76

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7340 Color: 401
Size: 590 Color: 128
Size: 262 Color: 66

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7346 Color: 402
Size: 656 Color: 131
Size: 190 Color: 38

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5069 Color: 290
Size: 2954 Color: 260
Size: 168 Color: 31

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5882 Color: 311
Size: 1797 Color: 221
Size: 512 Color: 118

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 325
Size: 1763 Color: 220
Size: 186 Color: 37

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 338
Size: 1626 Color: 216
Size: 78 Color: 3

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6569 Color: 341
Size: 1622 Color: 215

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6693 Color: 353
Size: 1498 Color: 207

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 375
Size: 922 Color: 156
Size: 244 Color: 59

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 7069 Color: 381
Size: 718 Color: 139
Size: 404 Color: 100

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 307
Size: 1613 Color: 214
Size: 828 Color: 149

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6609 Color: 345
Size: 1581 Color: 213

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6613 Color: 346
Size: 1577 Color: 212

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 347
Size: 1573 Color: 211

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6769 Color: 357
Size: 1421 Color: 202

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6873 Color: 363
Size: 1317 Color: 193

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 7049 Color: 380
Size: 1141 Color: 179

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 7170 Color: 387
Size: 1020 Color: 166

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 7318 Color: 398
Size: 872 Color: 152

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 323
Size: 2041 Color: 235

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 330
Size: 1452 Color: 204
Size: 436 Color: 107

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 6356 Color: 333
Size: 1833 Color: 223

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 368
Size: 1281 Color: 189

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6985 Color: 374
Size: 1204 Color: 183

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 7252 Color: 392
Size: 937 Color: 157

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 289
Size: 2953 Color: 259
Size: 168 Color: 32

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 354
Size: 1462 Color: 205

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 360
Size: 1365 Color: 200

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6862 Color: 362
Size: 1326 Color: 196

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 7334 Color: 400
Size: 854 Color: 151

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 390
Size: 973 Color: 161

Bin 70: 6 of cap free
Amount of items: 3
Items: 
Size: 5110 Color: 291
Size: 2908 Color: 255
Size: 168 Color: 30

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 5154 Color: 293
Size: 3032 Color: 262

Bin 72: 6 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 295
Size: 2780 Color: 254
Size: 162 Color: 26

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 313
Size: 2162 Color: 237
Size: 120 Color: 9

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 7160 Color: 386
Size: 1024 Color: 167

Bin 75: 9 of cap free
Amount of items: 3
Items: 
Size: 5745 Color: 306
Size: 1850 Color: 224
Size: 588 Color: 127

Bin 76: 9 of cap free
Amount of items: 2
Items: 
Size: 6257 Color: 328
Size: 1926 Color: 229

Bin 77: 9 of cap free
Amount of items: 2
Items: 
Size: 6313 Color: 332
Size: 1870 Color: 226

Bin 78: 9 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 366
Size: 1297 Color: 191

Bin 79: 9 of cap free
Amount of items: 2
Items: 
Size: 7122 Color: 384
Size: 1061 Color: 170

Bin 80: 10 of cap free
Amount of items: 3
Items: 
Size: 5578 Color: 303
Size: 2460 Color: 247
Size: 144 Color: 16

Bin 81: 10 of cap free
Amount of items: 4
Items: 
Size: 6037 Color: 321
Size: 2037 Color: 234
Size: 64 Color: 2
Size: 44 Color: 1

Bin 82: 10 of cap free
Amount of items: 3
Items: 
Size: 6077 Color: 322
Size: 2073 Color: 236
Size: 32 Color: 0

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 6532 Color: 339
Size: 1650 Color: 218

Bin 84: 11 of cap free
Amount of items: 2
Items: 
Size: 5900 Color: 312
Size: 2281 Color: 243

Bin 85: 11 of cap free
Amount of items: 2
Items: 
Size: 6949 Color: 370
Size: 1232 Color: 186

Bin 86: 11 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 385
Size: 1041 Color: 169

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 6842 Color: 361
Size: 1338 Color: 198

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 373
Size: 1202 Color: 182

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 7070 Color: 382
Size: 1110 Color: 176

Bin 90: 13 of cap free
Amount of items: 2
Items: 
Size: 6884 Color: 365
Size: 1295 Color: 190

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 292
Size: 3048 Color: 263

Bin 92: 15 of cap free
Amount of items: 3
Items: 
Size: 5455 Color: 298
Size: 2570 Color: 251
Size: 152 Color: 21

Bin 93: 15 of cap free
Amount of items: 4
Items: 
Size: 5570 Color: 302
Size: 2315 Color: 244
Size: 148 Color: 18
Size: 144 Color: 17

Bin 94: 16 of cap free
Amount of items: 2
Items: 
Size: 6921 Color: 369
Size: 1255 Color: 188

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 344
Size: 1572 Color: 210

Bin 96: 20 of cap free
Amount of items: 2
Items: 
Size: 6544 Color: 340
Size: 1628 Color: 217

Bin 97: 20 of cap free
Amount of items: 2
Items: 
Size: 6819 Color: 359
Size: 1353 Color: 199

Bin 98: 21 of cap free
Amount of items: 3
Items: 
Size: 4544 Color: 279
Size: 3411 Color: 267
Size: 216 Color: 49

Bin 99: 22 of cap free
Amount of items: 2
Items: 
Size: 5918 Color: 314
Size: 2252 Color: 242

Bin 100: 22 of cap free
Amount of items: 2
Items: 
Size: 5958 Color: 316
Size: 2212 Color: 241

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 6462 Color: 337
Size: 1708 Color: 219

Bin 102: 23 of cap free
Amount of items: 3
Items: 
Size: 5705 Color: 305
Size: 2324 Color: 246
Size: 140 Color: 13

Bin 103: 25 of cap free
Amount of items: 2
Items: 
Size: 6305 Color: 331
Size: 1862 Color: 225

Bin 104: 26 of cap free
Amount of items: 3
Items: 
Size: 4609 Color: 281
Size: 3341 Color: 265
Size: 216 Color: 48

Bin 105: 26 of cap free
Amount of items: 2
Items: 
Size: 6250 Color: 327
Size: 1916 Color: 228

Bin 106: 28 of cap free
Amount of items: 7
Items: 
Size: 4097 Color: 273
Size: 1037 Color: 168
Size: 982 Color: 162
Size: 884 Color: 153
Size: 680 Color: 135
Size: 244 Color: 60
Size: 240 Color: 58

Bin 107: 28 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 299
Size: 1910 Color: 227
Size: 724 Color: 140

Bin 108: 28 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 343
Size: 1568 Color: 209

Bin 109: 33 of cap free
Amount of items: 2
Items: 
Size: 5554 Color: 301
Size: 2605 Color: 253

Bin 110: 33 of cap free
Amount of items: 3
Items: 
Size: 6036 Color: 320
Size: 2033 Color: 233
Size: 90 Color: 4

Bin 111: 36 of cap free
Amount of items: 3
Items: 
Size: 6033 Color: 319
Size: 2027 Color: 232
Size: 96 Color: 5

Bin 112: 40 of cap free
Amount of items: 16
Items: 
Size: 818 Color: 148
Size: 680 Color: 134
Size: 680 Color: 133
Size: 680 Color: 132
Size: 640 Color: 130
Size: 600 Color: 129
Size: 588 Color: 126
Size: 584 Color: 124
Size: 552 Color: 121
Size: 512 Color: 117
Size: 508 Color: 116
Size: 264 Color: 68
Size: 264 Color: 67
Size: 262 Color: 65
Size: 262 Color: 64
Size: 258 Color: 63

Bin 113: 40 of cap free
Amount of items: 2
Items: 
Size: 5950 Color: 315
Size: 2202 Color: 240

Bin 114: 43 of cap free
Amount of items: 2
Items: 
Size: 4101 Color: 276
Size: 4048 Color: 272

Bin 115: 49 of cap free
Amount of items: 2
Items: 
Size: 5540 Color: 300
Size: 2603 Color: 252

Bin 116: 54 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 304
Size: 2322 Color: 245
Size: 140 Color: 14

Bin 117: 58 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 310
Size: 2186 Color: 239
Size: 136 Color: 10

Bin 118: 63 of cap free
Amount of items: 3
Items: 
Size: 5415 Color: 297
Size: 2554 Color: 250
Size: 160 Color: 23

Bin 119: 94 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 296
Size: 2534 Color: 249
Size: 160 Color: 24

Bin 120: 99 of cap free
Amount of items: 3
Items: 
Size: 5993 Color: 318
Size: 1988 Color: 231
Size: 112 Color: 6

Bin 121: 113 of cap free
Amount of items: 3
Items: 
Size: 5761 Color: 309
Size: 2182 Color: 238
Size: 136 Color: 11

Bin 122: 115 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 317
Size: 1983 Color: 230
Size: 120 Color: 8

Bin 123: 125 of cap free
Amount of items: 5
Items: 
Size: 4100 Color: 275
Size: 1389 Color: 201
Size: 1222 Color: 184
Size: 1128 Color: 178
Size: 228 Color: 53

Bin 124: 128 of cap free
Amount of items: 2
Items: 
Size: 4650 Color: 283
Size: 3414 Color: 270

Bin 125: 130 of cap free
Amount of items: 2
Items: 
Size: 4649 Color: 282
Size: 3413 Color: 269

Bin 126: 147 of cap free
Amount of items: 3
Items: 
Size: 4860 Color: 288
Size: 3009 Color: 261
Size: 176 Color: 33

Bin 127: 154 of cap free
Amount of items: 3
Items: 
Size: 4228 Color: 278
Size: 3592 Color: 271
Size: 218 Color: 50

Bin 128: 173 of cap free
Amount of items: 2
Items: 
Size: 4607 Color: 280
Size: 3412 Color: 268

Bin 129: 184 of cap free
Amount of items: 4
Items: 
Size: 4674 Color: 285
Size: 2934 Color: 257
Size: 200 Color: 42
Size: 200 Color: 41

Bin 130: 185 of cap free
Amount of items: 4
Items: 
Size: 4653 Color: 284
Size: 2932 Color: 256
Size: 216 Color: 47
Size: 206 Color: 44

Bin 131: 197 of cap free
Amount of items: 4
Items: 
Size: 4676 Color: 286
Size: 2951 Color: 258
Size: 184 Color: 36
Size: 184 Color: 35

Bin 132: 258 of cap free
Amount of items: 21
Items: 
Size: 496 Color: 114
Size: 488 Color: 113
Size: 464 Color: 111
Size: 462 Color: 110
Size: 456 Color: 109
Size: 440 Color: 108
Size: 436 Color: 106
Size: 432 Color: 105
Size: 414 Color: 104
Size: 406 Color: 103
Size: 406 Color: 101
Size: 380 Color: 97
Size: 320 Color: 81
Size: 314 Color: 80
Size: 314 Color: 79
Size: 314 Color: 78
Size: 312 Color: 77
Size: 288 Color: 74
Size: 264 Color: 71
Size: 264 Color: 70
Size: 264 Color: 69

Bin 133: 4982 of cap free
Amount of items: 9
Items: 
Size: 376 Color: 96
Size: 376 Color: 95
Size: 372 Color: 94
Size: 372 Color: 93
Size: 366 Color: 91
Size: 352 Color: 89
Size: 340 Color: 87
Size: 336 Color: 86
Size: 320 Color: 82

Total size: 1081344
Total free space: 8192

