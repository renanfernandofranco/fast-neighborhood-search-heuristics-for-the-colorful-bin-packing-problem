Capicity Bin: 15872
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 13769 Color: 1
Size: 1499 Color: 1
Size: 604 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9050 Color: 1
Size: 5686 Color: 1
Size: 1136 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 1
Size: 4276 Color: 1
Size: 848 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10514 Color: 1
Size: 4466 Color: 1
Size: 892 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9924 Color: 1
Size: 4964 Color: 1
Size: 984 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10700 Color: 1
Size: 4316 Color: 1
Size: 856 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7938 Color: 1
Size: 6614 Color: 1
Size: 1320 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 11623 Color: 1
Size: 2519 Color: 1
Size: 1136 Color: 0
Size: 594 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7940 Color: 1
Size: 6612 Color: 1
Size: 1320 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7941 Color: 1
Size: 6611 Color: 1
Size: 1320 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7942 Color: 1
Size: 6610 Color: 1
Size: 1320 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9020 Color: 1
Size: 5716 Color: 1
Size: 1136 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 1
Size: 5512 Color: 1
Size: 1088 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9544 Color: 1
Size: 5704 Color: 1
Size: 624 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9605 Color: 1
Size: 5223 Color: 1
Size: 1044 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 1
Size: 5125 Color: 1
Size: 1024 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9918 Color: 1
Size: 4962 Color: 1
Size: 992 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9938 Color: 1
Size: 4946 Color: 1
Size: 988 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 9956 Color: 1
Size: 4932 Color: 1
Size: 984 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 1
Size: 5160 Color: 1
Size: 752 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9972 Color: 1
Size: 4924 Color: 1
Size: 976 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10040 Color: 1
Size: 4872 Color: 1
Size: 960 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 1
Size: 4808 Color: 1
Size: 960 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10248 Color: 1
Size: 4696 Color: 1
Size: 928 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10335 Color: 1
Size: 4615 Color: 1
Size: 922 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10845 Color: 1
Size: 4191 Color: 1
Size: 836 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10899 Color: 1
Size: 4145 Color: 1
Size: 828 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 1
Size: 4168 Color: 1
Size: 688 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11269 Color: 1
Size: 3837 Color: 1
Size: 766 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11356 Color: 1
Size: 3764 Color: 1
Size: 752 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 1
Size: 3804 Color: 1
Size: 700 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11394 Color: 1
Size: 4022 Color: 1
Size: 456 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11496 Color: 1
Size: 3656 Color: 1
Size: 720 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11558 Color: 1
Size: 3850 Color: 1
Size: 464 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 1
Size: 3768 Color: 1
Size: 454 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11768 Color: 1
Size: 3432 Color: 1
Size: 672 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 1
Size: 3180 Color: 1
Size: 864 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11919 Color: 1
Size: 2813 Color: 1
Size: 1140 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11938 Color: 1
Size: 3282 Color: 1
Size: 652 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11983 Color: 1
Size: 3241 Color: 1
Size: 648 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12092 Color: 1
Size: 3244 Color: 1
Size: 536 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 1
Size: 3320 Color: 1
Size: 320 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12299 Color: 1
Size: 2761 Color: 1
Size: 812 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12315 Color: 1
Size: 2965 Color: 1
Size: 592 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 3048 Color: 1
Size: 480 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 1
Size: 2920 Color: 1
Size: 576 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12495 Color: 1
Size: 2815 Color: 1
Size: 562 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 1
Size: 3112 Color: 1
Size: 144 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 1
Size: 2804 Color: 1
Size: 424 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12645 Color: 1
Size: 2691 Color: 1
Size: 536 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 1
Size: 2434 Color: 1
Size: 768 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12699 Color: 1
Size: 2761 Color: 1
Size: 412 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 1
Size: 2952 Color: 1
Size: 200 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12724 Color: 1
Size: 2628 Color: 1
Size: 520 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12779 Color: 1
Size: 2579 Color: 1
Size: 514 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 1
Size: 2570 Color: 1
Size: 512 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12819 Color: 1
Size: 2551 Color: 1
Size: 502 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12851 Color: 1
Size: 2381 Color: 1
Size: 640 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12863 Color: 1
Size: 2545 Color: 1
Size: 464 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12888 Color: 1
Size: 2408 Color: 1
Size: 576 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 2648 Color: 1
Size: 320 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 1
Size: 2348 Color: 1
Size: 608 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 1
Size: 2006 Color: 1
Size: 912 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 1
Size: 2632 Color: 1
Size: 256 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 1
Size: 2404 Color: 1
Size: 472 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 1
Size: 2532 Color: 1
Size: 280 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 1
Size: 2488 Color: 1
Size: 320 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 1
Size: 2238 Color: 1
Size: 560 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13159 Color: 1
Size: 2261 Color: 1
Size: 452 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 1
Size: 2198 Color: 1
Size: 484 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 1322 Color: 0
Size: 1312 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13249 Color: 1
Size: 2275 Color: 1
Size: 348 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 1
Size: 2184 Color: 1
Size: 432 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 1
Size: 2175 Color: 1
Size: 434 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 1
Size: 2164 Color: 1
Size: 432 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 1
Size: 2081 Color: 1
Size: 512 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 1
Size: 2164 Color: 1
Size: 424 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13295 Color: 1
Size: 2149 Color: 1
Size: 428 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 1
Size: 1882 Color: 1
Size: 672 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 1
Size: 2124 Color: 1
Size: 396 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13375 Color: 1
Size: 1945 Color: 1
Size: 552 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13426 Color: 1
Size: 2042 Color: 1
Size: 404 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13465 Color: 1
Size: 2007 Color: 1
Size: 400 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13466 Color: 1
Size: 2102 Color: 1
Size: 304 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 1
Size: 1980 Color: 1
Size: 392 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 1
Size: 2276 Color: 1
Size: 80 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 1
Size: 2104 Color: 1
Size: 240 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13565 Color: 1
Size: 1747 Color: 1
Size: 560 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 1
Size: 1960 Color: 1
Size: 336 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13580 Color: 1
Size: 1916 Color: 1
Size: 376 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 1
Size: 2116 Color: 1
Size: 144 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 1
Size: 1867 Color: 1
Size: 372 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 13659 Color: 1
Size: 1845 Color: 1
Size: 368 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 13698 Color: 1
Size: 1814 Color: 1
Size: 360 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 13704 Color: 1
Size: 1768 Color: 1
Size: 400 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13745 Color: 1
Size: 1853 Color: 1
Size: 274 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 1
Size: 1928 Color: 1
Size: 192 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 13758 Color: 1
Size: 1614 Color: 1
Size: 500 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 1
Size: 1761 Color: 1
Size: 352 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 1
Size: 1708 Color: 1
Size: 400 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 1
Size: 1753 Color: 1
Size: 342 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13806 Color: 1
Size: 1722 Color: 1
Size: 344 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 13809 Color: 1
Size: 1619 Color: 1
Size: 444 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 13825 Color: 1
Size: 1707 Color: 1
Size: 340 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 1
Size: 1656 Color: 1
Size: 388 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 13850 Color: 1
Size: 1686 Color: 1
Size: 336 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 1
Size: 1464 Color: 1
Size: 512 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 1
Size: 1647 Color: 1
Size: 328 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 13902 Color: 1
Size: 1642 Color: 1
Size: 328 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 1
Size: 1644 Color: 1
Size: 312 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 1
Size: 1625 Color: 1
Size: 324 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 1
Size: 1562 Color: 1
Size: 372 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 13984 Color: 1
Size: 1600 Color: 1
Size: 288 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 13993 Color: 1
Size: 1567 Color: 1
Size: 312 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 1
Size: 1565 Color: 1
Size: 312 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 13996 Color: 1
Size: 1564 Color: 1
Size: 312 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 13998 Color: 1
Size: 1216 Color: 0
Size: 658 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 1
Size: 1816 Color: 1
Size: 48 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14044 Color: 1
Size: 1636 Color: 1
Size: 192 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 1
Size: 1646 Color: 1
Size: 176 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14069 Color: 1
Size: 1503 Color: 1
Size: 300 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 1
Size: 1444 Color: 1
Size: 350 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14079 Color: 1
Size: 1495 Color: 1
Size: 298 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14090 Color: 1
Size: 1486 Color: 1
Size: 296 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 1
Size: 1484 Color: 1
Size: 288 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 1
Size: 1400 Color: 1
Size: 352 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 1
Size: 1459 Color: 1
Size: 290 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14126 Color: 1
Size: 1458 Color: 1
Size: 288 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 1
Size: 1455 Color: 1
Size: 290 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 1
Size: 1404 Color: 1
Size: 328 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14154 Color: 1
Size: 1434 Color: 1
Size: 284 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14155 Color: 1
Size: 1431 Color: 1
Size: 286 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 1
Size: 1429 Color: 1
Size: 284 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 1
Size: 1394 Color: 1
Size: 298 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 1
Size: 1412 Color: 1
Size: 264 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 1
Size: 1560 Color: 1
Size: 112 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 1
Size: 1348 Color: 1
Size: 322 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 14218 Color: 1
Size: 1382 Color: 1
Size: 272 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14220 Color: 1
Size: 1492 Color: 1
Size: 160 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 1
Size: 1336 Color: 1
Size: 276 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 14276 Color: 1
Size: 1332 Color: 1
Size: 264 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 1
Size: 1248 Color: 1
Size: 344 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 14282 Color: 1
Size: 1326 Color: 1
Size: 264 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 10399 Color: 1
Size: 5008 Color: 1
Size: 464 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 11316 Color: 1
Size: 3799 Color: 1
Size: 756 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 11868 Color: 1
Size: 3295 Color: 1
Size: 708 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 1
Size: 3443 Color: 1
Size: 368 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 12508 Color: 1
Size: 2979 Color: 1
Size: 384 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 12811 Color: 1
Size: 2468 Color: 1
Size: 592 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 1
Size: 2619 Color: 1
Size: 416 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13015 Color: 1
Size: 2692 Color: 1
Size: 164 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13061 Color: 1
Size: 2334 Color: 1
Size: 476 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 13143 Color: 1
Size: 2130 Color: 1
Size: 598 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 13332 Color: 1
Size: 2323 Color: 1
Size: 216 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 1
Size: 1721 Color: 1
Size: 532 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 1
Size: 1923 Color: 1
Size: 304 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 13931 Color: 1
Size: 1764 Color: 1
Size: 176 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 14075 Color: 1
Size: 1524 Color: 1
Size: 272 Color: 0

Bin 159: 1 of cap free
Amount of items: 4
Items: 
Size: 9688 Color: 1
Size: 3783 Color: 1
Size: 1936 Color: 0
Size: 464 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 9659 Color: 1
Size: 5708 Color: 1
Size: 504 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 1
Size: 1498 Color: 1
Size: 472 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 1
Size: 6904 Color: 1
Size: 510 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 1
Size: 3522 Color: 1
Size: 368 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 1
Size: 3372 Color: 1
Size: 320 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 12330 Color: 1
Size: 3156 Color: 1
Size: 384 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 1
Size: 2806 Color: 1
Size: 588 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 12559 Color: 1
Size: 3023 Color: 1
Size: 288 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 1
Size: 2670 Color: 1
Size: 488 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 1
Size: 2187 Color: 1
Size: 144 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 13658 Color: 1
Size: 1860 Color: 1
Size: 352 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 9028 Color: 1
Size: 5754 Color: 1
Size: 1088 Color: 0

Bin 172: 3 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 1
Size: 5179 Color: 1
Size: 736 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 1
Size: 4561 Color: 1
Size: 436 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 11333 Color: 1
Size: 4056 Color: 1
Size: 480 Color: 0

Bin 175: 3 of cap free
Amount of items: 3
Items: 
Size: 12283 Color: 1
Size: 2954 Color: 1
Size: 632 Color: 0

Bin 176: 3 of cap free
Amount of items: 3
Items: 
Size: 13085 Color: 1
Size: 2488 Color: 1
Size: 296 Color: 0

Bin 177: 3 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 1
Size: 6613 Color: 1
Size: 1040 Color: 0

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 1
Size: 3532 Color: 1
Size: 436 Color: 0

Bin 179: 4 of cap free
Amount of items: 5
Items: 
Size: 7208 Color: 1
Size: 4936 Color: 1
Size: 3180 Color: 1
Size: 272 Color: 0
Size: 272 Color: 0

Bin 180: 5 of cap free
Amount of items: 3
Items: 
Size: 9032 Color: 1
Size: 5801 Color: 1
Size: 1034 Color: 0

Bin 181: 5 of cap free
Amount of items: 3
Items: 
Size: 11741 Color: 1
Size: 3294 Color: 1
Size: 832 Color: 0

Bin 182: 6 of cap free
Amount of items: 3
Items: 
Size: 9872 Color: 1
Size: 5706 Color: 1
Size: 288 Color: 0

Bin 183: 6 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 1
Size: 3541 Color: 1
Size: 80 Color: 0

Bin 184: 8 of cap free
Amount of items: 3
Items: 
Size: 8024 Color: 1
Size: 6680 Color: 1
Size: 1160 Color: 0

Bin 185: 9 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 1
Size: 2991 Color: 1
Size: 976 Color: 0

Bin 186: 9 of cap free
Amount of items: 3
Items: 
Size: 8911 Color: 1
Size: 6152 Color: 1
Size: 800 Color: 0

Bin 187: 12 of cap free
Amount of items: 3
Items: 
Size: 9916 Color: 1
Size: 5288 Color: 1
Size: 656 Color: 0

Bin 188: 13 of cap free
Amount of items: 3
Items: 
Size: 8847 Color: 1
Size: 5872 Color: 1
Size: 1140 Color: 0

Bin 189: 15 of cap free
Amount of items: 3
Items: 
Size: 9010 Color: 1
Size: 5855 Color: 1
Size: 992 Color: 0

Bin 190: 18 of cap free
Amount of items: 3
Items: 
Size: 7956 Color: 1
Size: 5722 Color: 1
Size: 2176 Color: 0

Bin 191: 38 of cap free
Amount of items: 3
Items: 
Size: 9026 Color: 1
Size: 6392 Color: 1
Size: 416 Color: 0

Bin 192: 56 of cap free
Amount of items: 3
Items: 
Size: 10994 Color: 1
Size: 3652 Color: 1
Size: 1170 Color: 0

Bin 193: 60 of cap free
Amount of items: 3
Items: 
Size: 7944 Color: 1
Size: 6604 Color: 1
Size: 1264 Color: 0

Bin 194: 104 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 1
Size: 6976 Color: 1
Size: 288 Color: 0

Bin 195: 209 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 1
Size: 2509 Color: 1
Size: 648 Color: 0

Bin 196: 1145 of cap free
Amount of items: 3
Items: 
Size: 11677 Color: 1
Size: 1914 Color: 1
Size: 1136 Color: 0

Bin 197: 1519 of cap free
Amount of items: 3
Items: 
Size: 7937 Color: 1
Size: 5392 Color: 1
Size: 1024 Color: 0

Bin 198: 2140 of cap free
Amount of items: 3
Items: 
Size: 7888 Color: 1
Size: 5348 Color: 1
Size: 496 Color: 0

Bin 199: 10430 of cap free
Amount of items: 3
Items: 
Size: 4066 Color: 1
Size: 960 Color: 1
Size: 416 Color: 0

Total size: 3142656
Total free space: 15872

