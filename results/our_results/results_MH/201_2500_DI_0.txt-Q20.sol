Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 3
Size: 1023 Color: 3
Size: 204 Color: 13

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1682 Color: 8
Size: 646 Color: 12
Size: 88 Color: 17
Size: 40 Color: 16

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 2114 Color: 3
Size: 238 Color: 11
Size: 56 Color: 12
Size: 48 Color: 7

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 1701 Color: 14
Size: 631 Color: 0
Size: 64 Color: 13
Size: 44 Color: 5
Size: 16 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 17
Size: 332 Color: 17
Size: 64 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 14
Size: 611 Color: 6
Size: 122 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 2
Size: 1020 Color: 12
Size: 200 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 18
Size: 228 Color: 12
Size: 40 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 976 Color: 8
Size: 812 Color: 14
Size: 668 Color: 5

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 1
Size: 750 Color: 3
Size: 436 Color: 16
Size: 32 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 1021 Color: 15
Size: 204 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1907 Color: 3
Size: 485 Color: 2
Size: 64 Color: 9

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 1276 Color: 15
Size: 988 Color: 12
Size: 104 Color: 18
Size: 88 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 14
Size: 248 Color: 4
Size: 16 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 14
Size: 280 Color: 11
Size: 60 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1891 Color: 8
Size: 471 Color: 6
Size: 94 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 13
Size: 508 Color: 11
Size: 128 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 6
Size: 254 Color: 14
Size: 144 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1833 Color: 7
Size: 615 Color: 9
Size: 8 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 16
Size: 738 Color: 8
Size: 160 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 9
Size: 564 Color: 9
Size: 356 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 1
Size: 568 Color: 1
Size: 192 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 14
Size: 1018 Color: 5
Size: 28 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1964 Color: 11
Size: 412 Color: 15
Size: 80 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2132 Color: 9
Size: 276 Color: 3
Size: 48 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 17
Size: 512 Color: 15
Size: 4 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 0
Size: 874 Color: 15
Size: 8 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 4
Size: 462 Color: 3
Size: 88 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 6
Size: 862 Color: 18
Size: 168 Color: 2

Bin 30: 0 of cap free
Amount of items: 4
Items: 
Size: 1719 Color: 8
Size: 541 Color: 5
Size: 124 Color: 7
Size: 72 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 5
Size: 330 Color: 11
Size: 64 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 17
Size: 286 Color: 18
Size: 96 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 12
Size: 840 Color: 7
Size: 44 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 0
Size: 438 Color: 16
Size: 284 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 17
Size: 262 Color: 0
Size: 36 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 17
Size: 730 Color: 17
Size: 16 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 4
Size: 483 Color: 17
Size: 26 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 17
Size: 272 Color: 0
Size: 148 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1830 Color: 15
Size: 522 Color: 1
Size: 104 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 11
Size: 262 Color: 15
Size: 48 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 5
Size: 731 Color: 2
Size: 144 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2174 Color: 12
Size: 200 Color: 14
Size: 82 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 4
Size: 458 Color: 19
Size: 88 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 5
Size: 384 Color: 18
Size: 220 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1484 Color: 0
Size: 602 Color: 17
Size: 370 Color: 11

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 8
Size: 370 Color: 9
Size: 112 Color: 10

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 17
Size: 609 Color: 6
Size: 120 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 14
Size: 980 Color: 17
Size: 80 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 11
Size: 128 Color: 1
Size: 122 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 9
Size: 1022 Color: 7
Size: 204 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 14
Size: 526 Color: 16
Size: 104 Color: 5

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 8
Size: 749 Color: 0
Size: 148 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 14
Size: 406 Color: 14
Size: 80 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 1
Size: 532 Color: 1
Size: 144 Color: 15

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 8
Size: 322 Color: 0
Size: 120 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 15
Size: 692 Color: 19
Size: 32 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 6
Size: 172 Color: 6
Size: 80 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1660 Color: 19
Size: 740 Color: 4
Size: 56 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 6
Size: 638 Color: 8
Size: 124 Color: 5

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 4
Size: 741 Color: 13
Size: 146 Color: 16

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 11
Size: 873 Color: 19
Size: 174 Color: 5

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 12
Size: 863 Color: 11
Size: 172 Color: 18

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 10
Size: 210 Color: 16
Size: 48 Color: 10

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 7
Size: 507 Color: 6
Size: 100 Color: 12

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1823 Color: 18
Size: 585 Color: 1
Size: 48 Color: 3

Total size: 159640
Total free space: 0

