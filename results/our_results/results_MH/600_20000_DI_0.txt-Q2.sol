Capicity Bin: 16256
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 9142 Color: 0
Size: 6232 Color: 1
Size: 458 Color: 0
Size: 424 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 13790 Color: 1
Size: 2078 Color: 0
Size: 292 Color: 1
Size: 96 Color: 0

Bin 3: 0 of cap free
Amount of items: 32
Items: 
Size: 1804 Color: 1
Size: 1160 Color: 1
Size: 592 Color: 0
Size: 584 Color: 0
Size: 544 Color: 0
Size: 532 Color: 0
Size: 532 Color: 0
Size: 512 Color: 1
Size: 512 Color: 0
Size: 496 Color: 0
Size: 496 Color: 0
Size: 496 Color: 0
Size: 480 Color: 1
Size: 480 Color: 0
Size: 472 Color: 0
Size: 464 Color: 1
Size: 464 Color: 0
Size: 456 Color: 0
Size: 448 Color: 1
Size: 448 Color: 1
Size: 444 Color: 0
Size: 432 Color: 1
Size: 408 Color: 1
Size: 400 Color: 1
Size: 400 Color: 1
Size: 392 Color: 1
Size: 392 Color: 0
Size: 384 Color: 1
Size: 360 Color: 1
Size: 288 Color: 1
Size: 192 Color: 1
Size: 192 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9816 Color: 0
Size: 6008 Color: 1
Size: 432 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 11336 Color: 0
Size: 4284 Color: 1
Size: 352 Color: 0
Size: 284 Color: 1

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 10678 Color: 1
Size: 3273 Color: 0
Size: 1945 Color: 0
Size: 360 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14162 Color: 0
Size: 1072 Color: 1
Size: 1022 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9138 Color: 0
Size: 6770 Color: 0
Size: 348 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 1
Size: 1476 Color: 0
Size: 262 Color: 1

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 1
Size: 4602 Color: 0
Size: 1873 Color: 1
Size: 1389 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1184 Color: 1
Size: 540 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 0
Size: 3528 Color: 0
Size: 704 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 1
Size: 1352 Color: 1
Size: 320 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 0
Size: 3336 Color: 1
Size: 272 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 1
Size: 1706 Color: 0
Size: 488 Color: 1

Bin 16: 0 of cap free
Amount of items: 5
Items: 
Size: 8148 Color: 0
Size: 3017 Color: 1
Size: 1944 Color: 0
Size: 1688 Color: 0
Size: 1459 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 1
Size: 2208 Color: 0
Size: 34 Color: 0

Bin 18: 0 of cap free
Amount of items: 5
Items: 
Size: 8133 Color: 0
Size: 2630 Color: 1
Size: 2481 Color: 0
Size: 1660 Color: 1
Size: 1352 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11505 Color: 1
Size: 3052 Color: 1
Size: 1699 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 0
Size: 1400 Color: 1
Size: 704 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 0
Size: 2062 Color: 1
Size: 544 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 0
Size: 1296 Color: 0
Size: 468 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 0
Size: 1344 Color: 0
Size: 524 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12114 Color: 1
Size: 3454 Color: 0
Size: 688 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 1
Size: 3302 Color: 0
Size: 308 Color: 0

Bin 26: 0 of cap free
Amount of items: 10
Items: 
Size: 8136 Color: 1
Size: 1152 Color: 0
Size: 1008 Color: 1
Size: 992 Color: 1
Size: 992 Color: 0
Size: 992 Color: 0
Size: 912 Color: 0
Size: 908 Color: 0
Size: 656 Color: 1
Size: 508 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 2088 Color: 1
Size: 508 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 1
Size: 3752 Color: 0
Size: 112 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 2492 Color: 0
Size: 124 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 9252 Color: 0
Size: 5772 Color: 1
Size: 1232 Color: 0

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 9272 Color: 1
Size: 6772 Color: 0
Size: 192 Color: 1
Size: 20 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 9332 Color: 0
Size: 6616 Color: 1
Size: 308 Color: 1

Bin 33: 0 of cap free
Amount of items: 9
Items: 
Size: 8132 Color: 1
Size: 1344 Color: 1
Size: 1184 Color: 1
Size: 1172 Color: 1
Size: 1044 Color: 0
Size: 1040 Color: 0
Size: 1040 Color: 0
Size: 1020 Color: 0
Size: 280 Color: 0

Bin 34: 0 of cap free
Amount of items: 11
Items: 
Size: 8130 Color: 0
Size: 928 Color: 1
Size: 924 Color: 1
Size: 916 Color: 1
Size: 904 Color: 1
Size: 896 Color: 1
Size: 872 Color: 0
Size: 856 Color: 0
Size: 790 Color: 0
Size: 752 Color: 0
Size: 288 Color: 1

Bin 35: 0 of cap free
Amount of items: 21
Items: 
Size: 9448 Color: 0
Size: 428 Color: 0
Size: 416 Color: 0
Size: 412 Color: 0
Size: 380 Color: 0
Size: 374 Color: 0
Size: 364 Color: 1
Size: 364 Color: 0
Size: 360 Color: 1
Size: 340 Color: 0
Size: 336 Color: 1
Size: 330 Color: 1
Size: 328 Color: 0
Size: 320 Color: 1
Size: 320 Color: 0
Size: 312 Color: 1
Size: 304 Color: 1
Size: 304 Color: 1
Size: 288 Color: 1
Size: 272 Color: 1
Size: 256 Color: 1

Bin 36: 0 of cap free
Amount of items: 25
Items: 
Size: 896 Color: 1
Size: 816 Color: 1
Size: 812 Color: 1
Size: 790 Color: 1
Size: 784 Color: 1
Size: 752 Color: 1
Size: 744 Color: 0
Size: 720 Color: 0
Size: 708 Color: 0
Size: 690 Color: 0
Size: 686 Color: 1
Size: 686 Color: 0
Size: 686 Color: 0
Size: 684 Color: 0
Size: 632 Color: 1
Size: 616 Color: 0
Size: 608 Color: 0
Size: 600 Color: 0
Size: 598 Color: 0
Size: 556 Color: 1
Size: 544 Color: 1
Size: 544 Color: 0
Size: 540 Color: 1
Size: 388 Color: 0
Size: 176 Color: 1

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 8792 Color: 0
Size: 4546 Color: 1
Size: 2280 Color: 1
Size: 350 Color: 0
Size: 288 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 0
Size: 4988 Color: 1
Size: 620 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 1
Size: 4260 Color: 1
Size: 848 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11402 Color: 1
Size: 4542 Color: 0
Size: 312 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12098 Color: 1
Size: 3466 Color: 0
Size: 692 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 3010 Color: 1
Size: 790 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12452 Color: 0
Size: 3482 Color: 0
Size: 322 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12458 Color: 1
Size: 3166 Color: 1
Size: 632 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 1
Size: 3172 Color: 1
Size: 488 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 3016 Color: 1
Size: 336 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 1
Size: 1818 Color: 0
Size: 1450 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13110 Color: 1
Size: 2622 Color: 0
Size: 524 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 2648 Color: 0
Size: 432 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 1
Size: 2356 Color: 0
Size: 702 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13206 Color: 0
Size: 1802 Color: 0
Size: 1248 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13278 Color: 1
Size: 1544 Color: 1
Size: 1434 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 0
Size: 2056 Color: 0
Size: 908 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 1
Size: 2285 Color: 1
Size: 692 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13325 Color: 0
Size: 2443 Color: 1
Size: 488 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 0
Size: 2476 Color: 1
Size: 412 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 1
Size: 2748 Color: 0
Size: 104 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 1
Size: 1516 Color: 0
Size: 1024 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 1
Size: 1172 Color: 1
Size: 1160 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 0
Size: 1470 Color: 1
Size: 856 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 0
Size: 1352 Color: 1
Size: 912 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 1
Size: 1352 Color: 0
Size: 864 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14151 Color: 1
Size: 1481 Color: 1
Size: 624 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 1
Size: 1444 Color: 1
Size: 602 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 1
Size: 1564 Color: 1
Size: 424 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14538 Color: 1
Size: 1366 Color: 1
Size: 352 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 0
Size: 1022 Color: 1
Size: 616 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14603 Color: 1
Size: 1379 Color: 1
Size: 274 Color: 0

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 10123 Color: 1
Size: 5844 Color: 1
Size: 288 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 0
Size: 3519 Color: 0
Size: 788 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12135 Color: 0
Size: 3768 Color: 1
Size: 352 Color: 1

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 0
Size: 3451 Color: 0
Size: 272 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 1
Size: 2663 Color: 0
Size: 328 Color: 1

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13515 Color: 0
Size: 1588 Color: 1
Size: 1152 Color: 1

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12115 Color: 1
Size: 3756 Color: 1
Size: 384 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 0
Size: 4562 Color: 1
Size: 2725 Color: 1

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 10127 Color: 1
Size: 5832 Color: 0
Size: 296 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 1
Size: 3957 Color: 0
Size: 296 Color: 0

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 7120 Color: 0
Size: 6776 Color: 1
Size: 2359 Color: 1

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 12035 Color: 1
Size: 2512 Color: 0
Size: 1708 Color: 1

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 10786 Color: 1
Size: 5109 Color: 0
Size: 360 Color: 1

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13062 Color: 1
Size: 3081 Color: 0
Size: 112 Color: 1

Bin 83: 2 of cap free
Amount of items: 6
Items: 
Size: 8129 Color: 1
Size: 1768 Color: 0
Size: 1662 Color: 0
Size: 1608 Color: 1
Size: 1559 Color: 1
Size: 1528 Color: 0

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 9978 Color: 1
Size: 6000 Color: 1
Size: 276 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 10010 Color: 1
Size: 5934 Color: 1
Size: 310 Color: 0

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 10706 Color: 1
Size: 4380 Color: 1
Size: 1168 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 1
Size: 5104 Color: 1
Size: 412 Color: 0

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 1
Size: 5012 Color: 1
Size: 290 Color: 0

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 1
Size: 2408 Color: 1
Size: 832 Color: 0

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 1
Size: 2568 Color: 0

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 1
Size: 1864 Color: 0

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 10204 Color: 0
Size: 5234 Color: 1
Size: 816 Color: 1

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 0
Size: 3435 Color: 0
Size: 736 Color: 1

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 1
Size: 2667 Color: 0

Bin 95: 3 of cap free
Amount of items: 2
Items: 
Size: 14507 Color: 1
Size: 1746 Color: 0

Bin 96: 3 of cap free
Amount of items: 2
Items: 
Size: 14089 Color: 0
Size: 2164 Color: 1

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 0
Size: 2184 Color: 0
Size: 632 Color: 1

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 1
Size: 3656 Color: 0
Size: 1372 Color: 0

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 14408 Color: 0
Size: 1844 Color: 1

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 0
Size: 2724 Color: 1

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 0
Size: 2927 Color: 1

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 14444 Color: 0
Size: 1807 Color: 1

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 13705 Color: 1
Size: 1546 Color: 1
Size: 1000 Color: 0

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 13966 Color: 1
Size: 1948 Color: 0
Size: 336 Color: 1

Bin 105: 7 of cap free
Amount of items: 3
Items: 
Size: 13006 Color: 1
Size: 1685 Color: 0
Size: 1558 Color: 1

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 12653 Color: 0
Size: 3596 Color: 1

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 14339 Color: 0
Size: 1910 Color: 1

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 14494 Color: 0
Size: 1755 Color: 1

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 12987 Color: 0
Size: 2550 Color: 1
Size: 712 Color: 0

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 10160 Color: 0
Size: 6088 Color: 1

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 0
Size: 2482 Color: 1

Bin 112: 9 of cap free
Amount of items: 3
Items: 
Size: 11788 Color: 1
Size: 3947 Color: 0
Size: 512 Color: 1

Bin 113: 9 of cap free
Amount of items: 2
Items: 
Size: 12143 Color: 1
Size: 4104 Color: 0

Bin 114: 9 of cap free
Amount of items: 2
Items: 
Size: 10879 Color: 0
Size: 5368 Color: 1

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 14269 Color: 0
Size: 1978 Color: 1

Bin 116: 9 of cap free
Amount of items: 3
Items: 
Size: 10877 Color: 1
Size: 4442 Color: 1
Size: 928 Color: 0

Bin 117: 10 of cap free
Amount of items: 3
Items: 
Size: 10810 Color: 0
Size: 5044 Color: 1
Size: 392 Color: 0

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 14242 Color: 1
Size: 2004 Color: 0

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 14591 Color: 1
Size: 1655 Color: 0

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 13703 Color: 1
Size: 2542 Color: 0

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 1
Size: 2444 Color: 0

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 14362 Color: 0
Size: 1882 Color: 1

Bin 123: 12 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 0
Size: 1582 Color: 0
Size: 1200 Color: 1

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 13886 Color: 1
Size: 2358 Color: 0

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 13863 Color: 0
Size: 2380 Color: 1

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 14226 Color: 1
Size: 2016 Color: 0

Bin 127: 14 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 0
Size: 4344 Color: 0
Size: 368 Color: 1

Bin 128: 15 of cap free
Amount of items: 2
Items: 
Size: 12559 Color: 1
Size: 3682 Color: 0

Bin 129: 17 of cap free
Amount of items: 2
Items: 
Size: 11756 Color: 1
Size: 4483 Color: 0

Bin 130: 18 of cap free
Amount of items: 2
Items: 
Size: 14266 Color: 0
Size: 1972 Color: 1

Bin 131: 21 of cap free
Amount of items: 2
Items: 
Size: 14009 Color: 0
Size: 2226 Color: 1

Bin 132: 22 of cap free
Amount of items: 2
Items: 
Size: 14328 Color: 0
Size: 1906 Color: 1

Bin 133: 23 of cap free
Amount of items: 2
Items: 
Size: 14403 Color: 0
Size: 1830 Color: 1

Bin 134: 25 of cap free
Amount of items: 2
Items: 
Size: 11116 Color: 1
Size: 5115 Color: 0

Bin 135: 26 of cap free
Amount of items: 2
Items: 
Size: 13782 Color: 0
Size: 2448 Color: 1

Bin 136: 28 of cap free
Amount of items: 2
Items: 
Size: 12028 Color: 0
Size: 4200 Color: 1

Bin 137: 29 of cap free
Amount of items: 2
Items: 
Size: 14100 Color: 1
Size: 2127 Color: 0

Bin 138: 32 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 1
Size: 2296 Color: 0

Bin 139: 34 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 0
Size: 2330 Color: 1

Bin 140: 36 of cap free
Amount of items: 2
Items: 
Size: 14078 Color: 0
Size: 2142 Color: 1

Bin 141: 39 of cap free
Amount of items: 2
Items: 
Size: 11736 Color: 1
Size: 4481 Color: 0

Bin 142: 39 of cap free
Amount of items: 2
Items: 
Size: 13507 Color: 0
Size: 2710 Color: 1

Bin 143: 39 of cap free
Amount of items: 2
Items: 
Size: 12264 Color: 0
Size: 3953 Color: 1

Bin 144: 41 of cap free
Amount of items: 2
Items: 
Size: 12139 Color: 1
Size: 4076 Color: 0

Bin 145: 41 of cap free
Amount of items: 2
Items: 
Size: 14387 Color: 1
Size: 1828 Color: 0

Bin 146: 42 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 1
Size: 2702 Color: 0

Bin 147: 44 of cap free
Amount of items: 2
Items: 
Size: 13102 Color: 0
Size: 3110 Color: 1

Bin 148: 46 of cap free
Amount of items: 2
Items: 
Size: 14606 Color: 0
Size: 1604 Color: 1

Bin 149: 46 of cap free
Amount of items: 2
Items: 
Size: 10280 Color: 1
Size: 5930 Color: 0

Bin 150: 46 of cap free
Amount of items: 2
Items: 
Size: 11004 Color: 0
Size: 5206 Color: 1

Bin 151: 51 of cap free
Amount of items: 2
Items: 
Size: 12131 Color: 1
Size: 4074 Color: 0

Bin 152: 52 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 0
Size: 3108 Color: 1

Bin 153: 53 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 1
Size: 3546 Color: 0

Bin 154: 59 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 0
Size: 2129 Color: 1

Bin 155: 59 of cap free
Amount of items: 2
Items: 
Size: 13061 Color: 1
Size: 3136 Color: 0

Bin 156: 61 of cap free
Amount of items: 2
Items: 
Size: 11084 Color: 1
Size: 5111 Color: 0

Bin 157: 63 of cap free
Amount of items: 2
Items: 
Size: 11513 Color: 0
Size: 4680 Color: 1

Bin 158: 64 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 0
Size: 3772 Color: 1

Bin 159: 70 of cap free
Amount of items: 2
Items: 
Size: 14212 Color: 1
Size: 1974 Color: 0

Bin 160: 72 of cap free
Amount of items: 2
Items: 
Size: 12745 Color: 0
Size: 3439 Color: 1

Bin 161: 78 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 0
Size: 1930 Color: 1

Bin 162: 80 of cap free
Amount of items: 2
Items: 
Size: 14354 Color: 1
Size: 1822 Color: 0

Bin 163: 88 of cap free
Amount of items: 2
Items: 
Size: 12964 Color: 0
Size: 3204 Color: 1

Bin 164: 88 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 0
Size: 2124 Color: 1

Bin 165: 94 of cap free
Amount of items: 2
Items: 
Size: 11370 Color: 0
Size: 4792 Color: 1

Bin 166: 95 of cap free
Amount of items: 2
Items: 
Size: 12637 Color: 1
Size: 3524 Color: 0

Bin 167: 97 of cap free
Amount of items: 2
Items: 
Size: 11509 Color: 0
Size: 4650 Color: 1

Bin 168: 104 of cap free
Amount of items: 2
Items: 
Size: 14094 Color: 1
Size: 2058 Color: 0

Bin 169: 106 of cap free
Amount of items: 2
Items: 
Size: 8134 Color: 1
Size: 8016 Color: 0

Bin 170: 109 of cap free
Amount of items: 2
Items: 
Size: 11521 Color: 1
Size: 4626 Color: 0

Bin 171: 113 of cap free
Amount of items: 2
Items: 
Size: 10276 Color: 1
Size: 5867 Color: 0

Bin 172: 113 of cap free
Amount of items: 2
Items: 
Size: 13852 Color: 0
Size: 2291 Color: 1

Bin 173: 120 of cap free
Amount of items: 2
Items: 
Size: 12960 Color: 0
Size: 3176 Color: 1

Bin 174: 139 of cap free
Amount of items: 2
Items: 
Size: 14572 Color: 0
Size: 1545 Color: 1

Bin 175: 146 of cap free
Amount of items: 2
Items: 
Size: 9336 Color: 1
Size: 6774 Color: 0

Bin 176: 159 of cap free
Amount of items: 2
Items: 
Size: 13923 Color: 1
Size: 2174 Color: 0

Bin 177: 164 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 0
Size: 2662 Color: 1

Bin 178: 168 of cap free
Amount of items: 2
Items: 
Size: 10244 Color: 1
Size: 5844 Color: 0

Bin 179: 180 of cap free
Amount of items: 2
Items: 
Size: 13268 Color: 1
Size: 2808 Color: 0

Bin 180: 191 of cap free
Amount of items: 2
Items: 
Size: 13267 Color: 1
Size: 2798 Color: 0

Bin 181: 208 of cap free
Amount of items: 2
Items: 
Size: 11732 Color: 0
Size: 4316 Color: 1

Bin 182: 208 of cap free
Amount of items: 2
Items: 
Size: 9284 Color: 1
Size: 6764 Color: 0

Bin 183: 224 of cap free
Amount of items: 2
Items: 
Size: 11048 Color: 1
Size: 4984 Color: 0

Bin 184: 235 of cap free
Amount of items: 2
Items: 
Size: 12590 Color: 0
Size: 3431 Color: 1

Bin 185: 239 of cap free
Amount of items: 2
Items: 
Size: 9244 Color: 0
Size: 6773 Color: 1

Bin 186: 268 of cap free
Amount of items: 2
Items: 
Size: 9217 Color: 0
Size: 6771 Color: 1

Bin 187: 290 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 1
Size: 1354 Color: 0

Bin 188: 299 of cap free
Amount of items: 2
Items: 
Size: 10088 Color: 0
Size: 5869 Color: 1

Bin 189: 301 of cap free
Amount of items: 2
Items: 
Size: 12526 Color: 0
Size: 3429 Color: 1

Bin 190: 310 of cap free
Amount of items: 2
Items: 
Size: 10802 Color: 0
Size: 5144 Color: 1

Bin 191: 325 of cap free
Amount of items: 2
Items: 
Size: 10119 Color: 1
Size: 5812 Color: 0

Bin 192: 353 of cap free
Amount of items: 2
Items: 
Size: 12902 Color: 0
Size: 3001 Color: 1

Bin 193: 415 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 1
Size: 3961 Color: 0

Bin 194: 473 of cap free
Amount of items: 2
Items: 
Size: 9215 Color: 0
Size: 6568 Color: 1

Bin 195: 476 of cap free
Amount of items: 2
Items: 
Size: 11838 Color: 1
Size: 3942 Color: 0

Bin 196: 1777 of cap free
Amount of items: 1
Items: 
Size: 14479 Color: 0

Bin 197: 1816 of cap free
Amount of items: 1
Items: 
Size: 14440 Color: 1

Bin 198: 1924 of cap free
Amount of items: 1
Items: 
Size: 14332 Color: 1

Bin 199: 2021 of cap free
Amount of items: 1
Items: 
Size: 14235 Color: 0

Total size: 3218688
Total free space: 16256

