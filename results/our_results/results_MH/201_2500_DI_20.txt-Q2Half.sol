Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 984 Color: 1
Size: 840 Color: 1
Size: 272 Color: 1
Size: 272 Color: 1
Size: 64 Color: 0
Size: 16 Color: 0
Size: 8 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1688 Color: 1
Size: 576 Color: 1
Size: 96 Color: 0
Size: 88 Color: 0
Size: 8 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1536 Color: 1
Size: 504 Color: 0
Size: 384 Color: 1
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 1
Size: 374 Color: 1
Size: 8 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 1
Size: 340 Color: 1
Size: 64 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1874 Color: 1
Size: 486 Color: 1
Size: 96 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 1
Size: 484 Color: 1
Size: 88 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 1
Size: 338 Color: 1
Size: 64 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1967 Color: 1
Size: 409 Color: 1
Size: 80 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1948 Color: 1
Size: 428 Color: 1
Size: 80 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1923 Color: 1
Size: 445 Color: 1
Size: 88 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 1
Size: 874 Color: 1
Size: 172 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 1
Size: 1028 Color: 1
Size: 32 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 1
Size: 747 Color: 1
Size: 148 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 750 Color: 1
Size: 148 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1391 Color: 1
Size: 969 Color: 1
Size: 96 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 1
Size: 844 Color: 1
Size: 168 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 1
Size: 919 Color: 1
Size: 16 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 1
Size: 254 Color: 1
Size: 32 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 1
Size: 530 Color: 1
Size: 32 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 1
Size: 538 Color: 1
Size: 104 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1804 Color: 1
Size: 548 Color: 1
Size: 104 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 1
Size: 628 Color: 1
Size: 120 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 1
Size: 724 Color: 1
Size: 144 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 1
Size: 250 Color: 1
Size: 48 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 1
Size: 284 Color: 1
Size: 48 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 1
Size: 618 Color: 1
Size: 40 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 1
Size: 206 Color: 1
Size: 40 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 1
Size: 220 Color: 1
Size: 40 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 1
Size: 306 Color: 1
Size: 60 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 1
Size: 414 Color: 1
Size: 60 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 1
Size: 614 Color: 1
Size: 120 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2092 Color: 1
Size: 308 Color: 1
Size: 56 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 236 Color: 1
Size: 40 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1018 Color: 1
Size: 200 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 1
Size: 378 Color: 1
Size: 72 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1020 Color: 1
Size: 200 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 1
Size: 647 Color: 1
Size: 128 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 1
Size: 891 Color: 1
Size: 176 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 1
Size: 661 Color: 1
Size: 130 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 1
Size: 380 Color: 1
Size: 72 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 1022 Color: 1
Size: 204 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 1
Size: 294 Color: 1
Size: 56 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 1
Size: 846 Color: 1
Size: 168 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 1
Size: 845 Color: 1
Size: 168 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 814 Color: 1
Size: 52 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 731 Color: 1
Size: 52 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 1
Size: 381 Color: 1
Size: 74 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1907 Color: 1
Size: 459 Color: 1
Size: 90 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 665 Color: 1
Size: 132 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 1
Size: 690 Color: 1
Size: 136 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 1
Size: 577 Color: 1
Size: 114 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1901 Color: 1
Size: 463 Color: 1
Size: 92 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 1
Size: 571 Color: 1
Size: 112 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1545 Color: 1
Size: 761 Color: 1
Size: 150 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 1
Size: 753 Color: 1
Size: 150 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 1
Size: 430 Color: 1
Size: 84 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 406 Color: 1
Size: 52 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 1
Size: 438 Color: 1
Size: 84 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1851 Color: 1
Size: 505 Color: 1
Size: 100 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 1
Size: 511 Color: 1
Size: 102 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1835 Color: 1
Size: 519 Color: 1
Size: 102 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1023 Color: 1
Size: 204 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1915 Color: 1
Size: 451 Color: 1
Size: 90 Color: 0

Total size: 159640
Total free space: 0

