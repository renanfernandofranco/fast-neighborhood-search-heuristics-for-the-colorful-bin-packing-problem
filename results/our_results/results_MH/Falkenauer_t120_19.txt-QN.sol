Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 94
Size: 319 Color: 64
Size: 276 Color: 42

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 114
Size: 259 Color: 21
Size: 255 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 118
Size: 253 Color: 7
Size: 250 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 103
Size: 282 Color: 47
Size: 276 Color: 43

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 89
Size: 333 Color: 72
Size: 281 Color: 46

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 99
Size: 309 Color: 61
Size: 273 Color: 36

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 84
Size: 369 Color: 82
Size: 257 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 105
Size: 276 Color: 41
Size: 268 Color: 28

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 90
Size: 352 Color: 78
Size: 258 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 111
Size: 271 Color: 32
Size: 253 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 95
Size: 326 Color: 67
Size: 267 Color: 27

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 91
Size: 309 Color: 62
Size: 301 Color: 56

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 112
Size: 270 Color: 31
Size: 251 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 88
Size: 350 Color: 77
Size: 269 Color: 29

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 93
Size: 309 Color: 58
Size: 292 Color: 51

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 79
Size: 333 Color: 71
Size: 309 Color: 59

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 113
Size: 265 Color: 25
Size: 251 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 86
Size: 341 Color: 75
Size: 279 Color: 45

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 81
Size: 364 Color: 80
Size: 269 Color: 30

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 87
Size: 320 Color: 65
Size: 300 Color: 55

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 97
Size: 296 Color: 53
Size: 288 Color: 48

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 109
Size: 271 Color: 33
Size: 260 Color: 22

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 117
Size: 254 Color: 11
Size: 250 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 102
Size: 305 Color: 57
Size: 261 Color: 24

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 115
Size: 256 Color: 16
Size: 253 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 100
Size: 321 Color: 66
Size: 253 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 108
Size: 272 Color: 35
Size: 260 Color: 23

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 116
Size: 254 Color: 13
Size: 254 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 107
Size: 277 Color: 44
Size: 256 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 104
Size: 291 Color: 50
Size: 259 Color: 20

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 76
Size: 340 Color: 74
Size: 315 Color: 63

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 5
Size: 250 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 101
Size: 296 Color: 52
Size: 274 Color: 38

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 106
Size: 275 Color: 40
Size: 265 Color: 26

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 110
Size: 271 Color: 34
Size: 257 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 92
Size: 330 Color: 68
Size: 275 Color: 39

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 83
Size: 331 Color: 70
Size: 298 Color: 54

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 98
Size: 309 Color: 60
Size: 273 Color: 37

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 96
Size: 337 Color: 73
Size: 253 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 330 Color: 69
Size: 291 Color: 49
Size: 379 Color: 85

Total size: 40000
Total free space: 0

