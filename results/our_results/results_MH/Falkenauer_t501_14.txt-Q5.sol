Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 283 Color: 2
Size: 254 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 320 Color: 1
Size: 299 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 368 Color: 4
Size: 258 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 341 Color: 3
Size: 276 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 2
Size: 287 Color: 4
Size: 251 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 2
Size: 261 Color: 1
Size: 253 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 359 Color: 2
Size: 259 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 333 Color: 2
Size: 277 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 257 Color: 2
Size: 257 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 261 Color: 0
Size: 253 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 349 Color: 1
Size: 286 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 264 Color: 4
Size: 251 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 288 Color: 1
Size: 459 Color: 4
Size: 253 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 260 Color: 3
Size: 251 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 2
Size: 312 Color: 2
Size: 295 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 332 Color: 4
Size: 299 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 317 Color: 0
Size: 305 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 4
Size: 264 Color: 2
Size: 255 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 342 Color: 0
Size: 256 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 320 Color: 0
Size: 316 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 297 Color: 1
Size: 250 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 254 Color: 4
Size: 250 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 325 Color: 2
Size: 254 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 356 Color: 3
Size: 278 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 364 Color: 3
Size: 260 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 2
Size: 353 Color: 0
Size: 287 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 3
Size: 253 Color: 1
Size: 250 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 2
Size: 343 Color: 0
Size: 252 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 357 Color: 4
Size: 282 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 4
Size: 265 Color: 2
Size: 260 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 3
Size: 354 Color: 4
Size: 278 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 359 Color: 2
Size: 253 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 303 Color: 3
Size: 253 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 3
Size: 330 Color: 4
Size: 317 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 268 Color: 4
Size: 250 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 359 Color: 2
Size: 259 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 343 Color: 1
Size: 276 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 309 Color: 4
Size: 258 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 275 Color: 0
Size: 269 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 365 Color: 0
Size: 258 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 356 Color: 4
Size: 280 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 297 Color: 3
Size: 250 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 4
Size: 274 Color: 1
Size: 256 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 260 Color: 4
Size: 257 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 265 Color: 3
Size: 250 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 262 Color: 1
Size: 250 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 353 Color: 2
Size: 251 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 313 Color: 1
Size: 250 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 362 Color: 1
Size: 269 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 263 Color: 1
Size: 262 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 2
Size: 314 Color: 3
Size: 254 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 285 Color: 0
Size: 280 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 260 Color: 0
Size: 256 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 284 Color: 0
Size: 255 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 326 Color: 3
Size: 263 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 358 Color: 0
Size: 257 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 307 Color: 3
Size: 279 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 350 Color: 2
Size: 262 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 341 Color: 2
Size: 294 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 298 Color: 4
Size: 258 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 340 Color: 2
Size: 293 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 357 Color: 0
Size: 280 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 314 Color: 1
Size: 312 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 252 Color: 3
Size: 250 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 2
Size: 250 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 268 Color: 1
Size: 257 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 347 Color: 0
Size: 288 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 2
Size: 304 Color: 4
Size: 264 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 289 Color: 1
Size: 272 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 328 Color: 2
Size: 283 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 344 Color: 4
Size: 280 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 255 Color: 0
Size: 251 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 272 Color: 0
Size: 261 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 3
Size: 316 Color: 1
Size: 255 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 0
Size: 276 Color: 1
Size: 257 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 2
Size: 259 Color: 0
Size: 251 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 369 Color: 2
Size: 256 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 281 Color: 2
Size: 263 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 314 Color: 2
Size: 272 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 367 Color: 0
Size: 262 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 343 Color: 4
Size: 285 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 325 Color: 4
Size: 264 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 300 Color: 1
Size: 295 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 360 Color: 2
Size: 273 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 317 Color: 4
Size: 293 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 324 Color: 4
Size: 268 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 320 Color: 2
Size: 252 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 306 Color: 2
Size: 290 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 280 Color: 0
Size: 271 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 277 Color: 4
Size: 268 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 315 Color: 0
Size: 270 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 337 Color: 3
Size: 250 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 253 Color: 1
Size: 252 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 4
Size: 343 Color: 2
Size: 306 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 301 Color: 0
Size: 292 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 4
Size: 354 Color: 1
Size: 290 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 2
Size: 264 Color: 4
Size: 262 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 277 Color: 3
Size: 266 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 320 Color: 2
Size: 264 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 287 Color: 0
Size: 277 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 284 Color: 4
Size: 258 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 310 Color: 4
Size: 267 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 313 Color: 3
Size: 296 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 314 Color: 3
Size: 281 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 348 Color: 0
Size: 264 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 322 Color: 1
Size: 279 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 292 Color: 1
Size: 264 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 310 Color: 0
Size: 252 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 317 Color: 3
Size: 275 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 294 Color: 2
Size: 260 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 0
Size: 257 Color: 4
Size: 256 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 346 Color: 2
Size: 294 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 332 Color: 3
Size: 250 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 298 Color: 2
Size: 281 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 266 Color: 1
Size: 264 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 261 Color: 4
Size: 257 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 339 Color: 4
Size: 308 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 305 Color: 2
Size: 283 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 3
Size: 250 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 355 Color: 3
Size: 272 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 303 Color: 3
Size: 253 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 295 Color: 4
Size: 279 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 0
Size: 319 Color: 4
Size: 251 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 291 Color: 2
Size: 251 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 260 Color: 1
Size: 250 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 362 Color: 2
Size: 273 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 312 Color: 2
Size: 257 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 301 Color: 0
Size: 299 Color: 2

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 4
Size: 345 Color: 0
Size: 254 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 256 Color: 4
Size: 253 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 347 Color: 4
Size: 274 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 263 Color: 3
Size: 257 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 273 Color: 1
Size: 261 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 391 Color: 3
Size: 256 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 326 Color: 2
Size: 314 Color: 3

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 343 Color: 4
Size: 296 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 2
Size: 347 Color: 0
Size: 303 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 354 Color: 0
Size: 277 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 329 Color: 0
Size: 299 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 341 Color: 2
Size: 285 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 315 Color: 4
Size: 258 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 343 Color: 1
Size: 260 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 309 Color: 1
Size: 292 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 333 Color: 2
Size: 268 Color: 3

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 331 Color: 2
Size: 259 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 307 Color: 2
Size: 282 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 322 Color: 2
Size: 268 Color: 2

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 2
Size: 313 Color: 4
Size: 277 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 307 Color: 3
Size: 274 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 304 Color: 3
Size: 272 Color: 3

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 321 Color: 1
Size: 251 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 302 Color: 0
Size: 271 Color: 2

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 296 Color: 1
Size: 269 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 290 Color: 0
Size: 272 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 285 Color: 4
Size: 274 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 291 Color: 4
Size: 268 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 286 Color: 4
Size: 270 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 296 Color: 2
Size: 252 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 284 Color: 4
Size: 262 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 289 Color: 2
Size: 255 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 279 Color: 3
Size: 260 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 254 Color: 4
Size: 251 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 266 Color: 1
Size: 250 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 260 Color: 1
Size: 255 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 261 Color: 0
Size: 253 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 260 Color: 2
Size: 254 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 255 Color: 0
Size: 255 Color: 0

Total size: 167000
Total free space: 0

