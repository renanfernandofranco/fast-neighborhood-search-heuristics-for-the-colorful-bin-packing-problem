Capicity Bin: 1000001
Lower Bound: 227

Bins used: 228
Amount of Colors: 5

Bin 1: 2 of cap free
Amount of items: 2
Items: 
Size: 797120 Color: 4
Size: 202879 Color: 3

Bin 2: 3 of cap free
Amount of items: 3
Items: 
Size: 662002 Color: 3
Size: 171530 Color: 1
Size: 166466 Color: 3

Bin 3: 3 of cap free
Amount of items: 3
Items: 
Size: 647009 Color: 2
Size: 199699 Color: 1
Size: 153290 Color: 2

Bin 4: 3 of cap free
Amount of items: 3
Items: 
Size: 531099 Color: 0
Size: 357194 Color: 3
Size: 111705 Color: 1

Bin 5: 4 of cap free
Amount of items: 3
Items: 
Size: 761119 Color: 3
Size: 124903 Color: 0
Size: 113975 Color: 3

Bin 6: 4 of cap free
Amount of items: 3
Items: 
Size: 728524 Color: 1
Size: 156738 Color: 4
Size: 114735 Color: 2

Bin 7: 5 of cap free
Amount of items: 3
Items: 
Size: 664907 Color: 0
Size: 181119 Color: 4
Size: 153970 Color: 3

Bin 8: 7 of cap free
Amount of items: 4
Items: 
Size: 556063 Color: 4
Size: 197719 Color: 3
Size: 139802 Color: 4
Size: 106410 Color: 1

Bin 9: 9 of cap free
Amount of items: 3
Items: 
Size: 669103 Color: 1
Size: 178914 Color: 3
Size: 151975 Color: 2

Bin 10: 9 of cap free
Amount of items: 2
Items: 
Size: 742368 Color: 1
Size: 257624 Color: 3

Bin 11: 10 of cap free
Amount of items: 3
Items: 
Size: 774636 Color: 1
Size: 121000 Color: 2
Size: 104355 Color: 0

Bin 12: 16 of cap free
Amount of items: 3
Items: 
Size: 638359 Color: 2
Size: 258051 Color: 3
Size: 103575 Color: 1

Bin 13: 17 of cap free
Amount of items: 3
Items: 
Size: 567050 Color: 1
Size: 242379 Color: 2
Size: 190555 Color: 2

Bin 14: 19 of cap free
Amount of items: 3
Items: 
Size: 555189 Color: 2
Size: 318135 Color: 0
Size: 126658 Color: 2

Bin 15: 20 of cap free
Amount of items: 3
Items: 
Size: 577838 Color: 2
Size: 284143 Color: 4
Size: 138000 Color: 2

Bin 16: 22 of cap free
Amount of items: 3
Items: 
Size: 574450 Color: 0
Size: 248724 Color: 3
Size: 176805 Color: 1

Bin 17: 23 of cap free
Amount of items: 2
Items: 
Size: 632941 Color: 4
Size: 367037 Color: 2

Bin 18: 24 of cap free
Amount of items: 3
Items: 
Size: 671412 Color: 0
Size: 202097 Color: 2
Size: 126468 Color: 3

Bin 19: 25 of cap free
Amount of items: 3
Items: 
Size: 736183 Color: 1
Size: 144558 Color: 3
Size: 119235 Color: 1

Bin 20: 26 of cap free
Amount of items: 3
Items: 
Size: 736061 Color: 1
Size: 151224 Color: 2
Size: 112690 Color: 0

Bin 21: 28 of cap free
Amount of items: 3
Items: 
Size: 457437 Color: 1
Size: 404490 Color: 1
Size: 138046 Color: 2

Bin 22: 30 of cap free
Amount of items: 2
Items: 
Size: 713246 Color: 1
Size: 286725 Color: 3

Bin 23: 33 of cap free
Amount of items: 3
Items: 
Size: 530959 Color: 3
Size: 272462 Color: 2
Size: 196547 Color: 0

Bin 24: 35 of cap free
Amount of items: 3
Items: 
Size: 673095 Color: 0
Size: 215382 Color: 2
Size: 111489 Color: 3

Bin 25: 41 of cap free
Amount of items: 2
Items: 
Size: 548131 Color: 1
Size: 451829 Color: 3

Bin 26: 41 of cap free
Amount of items: 3
Items: 
Size: 598262 Color: 4
Size: 285937 Color: 3
Size: 115761 Color: 3

Bin 27: 48 of cap free
Amount of items: 3
Items: 
Size: 573872 Color: 2
Size: 243535 Color: 0
Size: 182546 Color: 4

Bin 28: 54 of cap free
Amount of items: 2
Items: 
Size: 588452 Color: 3
Size: 411495 Color: 2

Bin 29: 55 of cap free
Amount of items: 2
Items: 
Size: 609957 Color: 4
Size: 389989 Color: 0

Bin 30: 55 of cap free
Amount of items: 2
Items: 
Size: 758303 Color: 0
Size: 241643 Color: 1

Bin 31: 56 of cap free
Amount of items: 3
Items: 
Size: 568522 Color: 1
Size: 253864 Color: 3
Size: 177559 Color: 3

Bin 32: 58 of cap free
Amount of items: 3
Items: 
Size: 530782 Color: 2
Size: 308249 Color: 1
Size: 160912 Color: 2

Bin 33: 59 of cap free
Amount of items: 3
Items: 
Size: 657745 Color: 1
Size: 176701 Color: 2
Size: 165496 Color: 2

Bin 34: 61 of cap free
Amount of items: 2
Items: 
Size: 699750 Color: 1
Size: 300190 Color: 0

Bin 35: 62 of cap free
Amount of items: 2
Items: 
Size: 549850 Color: 4
Size: 450089 Color: 2

Bin 36: 63 of cap free
Amount of items: 3
Items: 
Size: 613315 Color: 1
Size: 194685 Color: 3
Size: 191938 Color: 4

Bin 37: 63 of cap free
Amount of items: 3
Items: 
Size: 612391 Color: 0
Size: 200555 Color: 3
Size: 186992 Color: 1

Bin 38: 65 of cap free
Amount of items: 2
Items: 
Size: 792981 Color: 1
Size: 206955 Color: 4

Bin 39: 75 of cap free
Amount of items: 2
Items: 
Size: 527916 Color: 1
Size: 472010 Color: 3

Bin 40: 93 of cap free
Amount of items: 2
Items: 
Size: 637997 Color: 2
Size: 361911 Color: 4

Bin 41: 95 of cap free
Amount of items: 3
Items: 
Size: 659502 Color: 1
Size: 203435 Color: 2
Size: 136969 Color: 2

Bin 42: 96 of cap free
Amount of items: 3
Items: 
Size: 583916 Color: 2
Size: 295110 Color: 3
Size: 120879 Color: 0

Bin 43: 122 of cap free
Amount of items: 2
Items: 
Size: 571714 Color: 3
Size: 428165 Color: 1

Bin 44: 133 of cap free
Amount of items: 2
Items: 
Size: 799932 Color: 0
Size: 199936 Color: 2

Bin 45: 146 of cap free
Amount of items: 2
Items: 
Size: 518464 Color: 2
Size: 481391 Color: 4

Bin 46: 147 of cap free
Amount of items: 2
Items: 
Size: 677965 Color: 0
Size: 321889 Color: 3

Bin 47: 151 of cap free
Amount of items: 3
Items: 
Size: 702453 Color: 4
Size: 166827 Color: 0
Size: 130570 Color: 4

Bin 48: 153 of cap free
Amount of items: 2
Items: 
Size: 509202 Color: 3
Size: 490646 Color: 2

Bin 49: 167 of cap free
Amount of items: 3
Items: 
Size: 726010 Color: 1
Size: 160171 Color: 2
Size: 113653 Color: 4

Bin 50: 171 of cap free
Amount of items: 2
Items: 
Size: 501885 Color: 2
Size: 497945 Color: 0

Bin 51: 181 of cap free
Amount of items: 2
Items: 
Size: 700743 Color: 2
Size: 299077 Color: 4

Bin 52: 191 of cap free
Amount of items: 2
Items: 
Size: 725445 Color: 4
Size: 274365 Color: 1

Bin 53: 191 of cap free
Amount of items: 3
Items: 
Size: 663910 Color: 2
Size: 213140 Color: 2
Size: 122760 Color: 0

Bin 54: 193 of cap free
Amount of items: 2
Items: 
Size: 726549 Color: 2
Size: 273259 Color: 0

Bin 55: 196 of cap free
Amount of items: 2
Items: 
Size: 594985 Color: 0
Size: 404820 Color: 1

Bin 56: 204 of cap free
Amount of items: 3
Items: 
Size: 563559 Color: 4
Size: 243100 Color: 3
Size: 193138 Color: 1

Bin 57: 204 of cap free
Amount of items: 2
Items: 
Size: 653833 Color: 1
Size: 345964 Color: 3

Bin 58: 233 of cap free
Amount of items: 2
Items: 
Size: 704521 Color: 3
Size: 295247 Color: 2

Bin 59: 234 of cap free
Amount of items: 2
Items: 
Size: 562036 Color: 2
Size: 437731 Color: 0

Bin 60: 237 of cap free
Amount of items: 2
Items: 
Size: 543834 Color: 4
Size: 455930 Color: 0

Bin 61: 250 of cap free
Amount of items: 2
Items: 
Size: 609091 Color: 4
Size: 390660 Color: 1

Bin 62: 260 of cap free
Amount of items: 2
Items: 
Size: 541246 Color: 2
Size: 458495 Color: 3

Bin 63: 273 of cap free
Amount of items: 2
Items: 
Size: 678609 Color: 0
Size: 321119 Color: 1

Bin 64: 283 of cap free
Amount of items: 2
Items: 
Size: 686333 Color: 4
Size: 313385 Color: 2

Bin 65: 295 of cap free
Amount of items: 3
Items: 
Size: 663822 Color: 2
Size: 180349 Color: 4
Size: 155535 Color: 3

Bin 66: 300 of cap free
Amount of items: 2
Items: 
Size: 509659 Color: 3
Size: 490042 Color: 4

Bin 67: 301 of cap free
Amount of items: 2
Items: 
Size: 545329 Color: 3
Size: 454371 Color: 4

Bin 68: 306 of cap free
Amount of items: 2
Items: 
Size: 792086 Color: 1
Size: 207609 Color: 0

Bin 69: 308 of cap free
Amount of items: 3
Items: 
Size: 637647 Color: 3
Size: 182867 Color: 3
Size: 179179 Color: 1

Bin 70: 351 of cap free
Amount of items: 2
Items: 
Size: 677926 Color: 2
Size: 321724 Color: 3

Bin 71: 352 of cap free
Amount of items: 2
Items: 
Size: 680541 Color: 4
Size: 319108 Color: 3

Bin 72: 357 of cap free
Amount of items: 2
Items: 
Size: 652696 Color: 3
Size: 346948 Color: 2

Bin 73: 358 of cap free
Amount of items: 3
Items: 
Size: 639435 Color: 3
Size: 189217 Color: 4
Size: 170991 Color: 2

Bin 74: 361 of cap free
Amount of items: 2
Items: 
Size: 767991 Color: 2
Size: 231649 Color: 3

Bin 75: 363 of cap free
Amount of items: 2
Items: 
Size: 549030 Color: 3
Size: 450608 Color: 0

Bin 76: 363 of cap free
Amount of items: 2
Items: 
Size: 605982 Color: 4
Size: 393656 Color: 0

Bin 77: 371 of cap free
Amount of items: 2
Items: 
Size: 540313 Color: 0
Size: 459317 Color: 1

Bin 78: 426 of cap free
Amount of items: 2
Items: 
Size: 759502 Color: 1
Size: 240073 Color: 3

Bin 79: 434 of cap free
Amount of items: 2
Items: 
Size: 519338 Color: 0
Size: 480229 Color: 4

Bin 80: 436 of cap free
Amount of items: 2
Items: 
Size: 698971 Color: 2
Size: 300594 Color: 1

Bin 81: 472 of cap free
Amount of items: 2
Items: 
Size: 514740 Color: 0
Size: 484789 Color: 4

Bin 82: 473 of cap free
Amount of items: 2
Items: 
Size: 637098 Color: 4
Size: 362430 Color: 1

Bin 83: 483 of cap free
Amount of items: 2
Items: 
Size: 682049 Color: 3
Size: 317469 Color: 1

Bin 84: 491 of cap free
Amount of items: 2
Items: 
Size: 573849 Color: 3
Size: 425661 Color: 2

Bin 85: 496 of cap free
Amount of items: 2
Items: 
Size: 779215 Color: 2
Size: 220290 Color: 3

Bin 86: 505 of cap free
Amount of items: 2
Items: 
Size: 672936 Color: 4
Size: 326560 Color: 1

Bin 87: 506 of cap free
Amount of items: 2
Items: 
Size: 627845 Color: 3
Size: 371650 Color: 0

Bin 88: 508 of cap free
Amount of items: 2
Items: 
Size: 783543 Color: 1
Size: 215950 Color: 3

Bin 89: 522 of cap free
Amount of items: 2
Items: 
Size: 671498 Color: 0
Size: 327981 Color: 1

Bin 90: 525 of cap free
Amount of items: 2
Items: 
Size: 524850 Color: 1
Size: 474626 Color: 3

Bin 91: 528 of cap free
Amount of items: 3
Items: 
Size: 761061 Color: 4
Size: 123486 Color: 4
Size: 114926 Color: 3

Bin 92: 548 of cap free
Amount of items: 3
Items: 
Size: 575477 Color: 4
Size: 296779 Color: 4
Size: 127197 Color: 1

Bin 93: 568 of cap free
Amount of items: 2
Items: 
Size: 597626 Color: 4
Size: 401807 Color: 3

Bin 94: 579 of cap free
Amount of items: 2
Items: 
Size: 547812 Color: 3
Size: 451610 Color: 2

Bin 95: 590 of cap free
Amount of items: 2
Items: 
Size: 794113 Color: 4
Size: 205298 Color: 0

Bin 96: 597 of cap free
Amount of items: 2
Items: 
Size: 636722 Color: 1
Size: 362682 Color: 3

Bin 97: 622 of cap free
Amount of items: 2
Items: 
Size: 603798 Color: 2
Size: 395581 Color: 4

Bin 98: 624 of cap free
Amount of items: 2
Items: 
Size: 656416 Color: 4
Size: 342961 Color: 3

Bin 99: 646 of cap free
Amount of items: 2
Items: 
Size: 653448 Color: 4
Size: 345907 Color: 0

Bin 100: 651 of cap free
Amount of items: 3
Items: 
Size: 601125 Color: 0
Size: 248662 Color: 0
Size: 149563 Color: 2

Bin 101: 659 of cap free
Amount of items: 2
Items: 
Size: 604994 Color: 1
Size: 394348 Color: 3

Bin 102: 693 of cap free
Amount of items: 2
Items: 
Size: 669686 Color: 3
Size: 329622 Color: 2

Bin 103: 715 of cap free
Amount of items: 2
Items: 
Size: 730750 Color: 2
Size: 268536 Color: 3

Bin 104: 715 of cap free
Amount of items: 2
Items: 
Size: 782089 Color: 1
Size: 217197 Color: 2

Bin 105: 753 of cap free
Amount of items: 2
Items: 
Size: 549020 Color: 2
Size: 450228 Color: 1

Bin 106: 754 of cap free
Amount of items: 2
Items: 
Size: 701372 Color: 1
Size: 297875 Color: 4

Bin 107: 761 of cap free
Amount of items: 3
Items: 
Size: 671465 Color: 1
Size: 167701 Color: 2
Size: 160074 Color: 1

Bin 108: 819 of cap free
Amount of items: 2
Items: 
Size: 602763 Color: 3
Size: 396419 Color: 0

Bin 109: 843 of cap free
Amount of items: 2
Items: 
Size: 502940 Color: 4
Size: 496218 Color: 2

Bin 110: 850 of cap free
Amount of items: 2
Items: 
Size: 606173 Color: 0
Size: 392978 Color: 3

Bin 111: 850 of cap free
Amount of items: 2
Items: 
Size: 722282 Color: 2
Size: 276869 Color: 4

Bin 112: 870 of cap free
Amount of items: 2
Items: 
Size: 557859 Color: 3
Size: 441272 Color: 0

Bin 113: 922 of cap free
Amount of items: 2
Items: 
Size: 728242 Color: 4
Size: 270837 Color: 3

Bin 114: 928 of cap free
Amount of items: 2
Items: 
Size: 506152 Color: 1
Size: 492921 Color: 3

Bin 115: 968 of cap free
Amount of items: 2
Items: 
Size: 793053 Color: 3
Size: 205980 Color: 0

Bin 116: 980 of cap free
Amount of items: 2
Items: 
Size: 623830 Color: 2
Size: 375191 Color: 4

Bin 117: 992 of cap free
Amount of items: 2
Items: 
Size: 795448 Color: 2
Size: 203561 Color: 4

Bin 118: 1002 of cap free
Amount of items: 2
Items: 
Size: 792096 Color: 0
Size: 206903 Color: 3

Bin 119: 1004 of cap free
Amount of items: 2
Items: 
Size: 510741 Color: 4
Size: 488256 Color: 3

Bin 120: 1066 of cap free
Amount of items: 2
Items: 
Size: 657609 Color: 0
Size: 341326 Color: 1

Bin 121: 1102 of cap free
Amount of items: 2
Items: 
Size: 741161 Color: 0
Size: 257738 Color: 1

Bin 122: 1109 of cap free
Amount of items: 3
Items: 
Size: 686173 Color: 2
Size: 166309 Color: 2
Size: 146410 Color: 4

Bin 123: 1119 of cap free
Amount of items: 2
Items: 
Size: 550042 Color: 3
Size: 448840 Color: 4

Bin 124: 1124 of cap free
Amount of items: 2
Items: 
Size: 742636 Color: 1
Size: 256241 Color: 4

Bin 125: 1125 of cap free
Amount of items: 2
Items: 
Size: 644475 Color: 1
Size: 354401 Color: 2

Bin 126: 1137 of cap free
Amount of items: 2
Items: 
Size: 787553 Color: 4
Size: 211311 Color: 2

Bin 127: 1165 of cap free
Amount of items: 2
Items: 
Size: 790001 Color: 4
Size: 208835 Color: 1

Bin 128: 1192 of cap free
Amount of items: 2
Items: 
Size: 618769 Color: 2
Size: 380040 Color: 0

Bin 129: 1201 of cap free
Amount of items: 2
Items: 
Size: 508701 Color: 0
Size: 490099 Color: 3

Bin 130: 1216 of cap free
Amount of items: 2
Items: 
Size: 627345 Color: 0
Size: 371440 Color: 2

Bin 131: 1232 of cap free
Amount of items: 2
Items: 
Size: 581318 Color: 4
Size: 417451 Color: 2

Bin 132: 1385 of cap free
Amount of items: 2
Items: 
Size: 720050 Color: 4
Size: 278566 Color: 0

Bin 133: 1413 of cap free
Amount of items: 2
Items: 
Size: 588794 Color: 4
Size: 409794 Color: 3

Bin 134: 1418 of cap free
Amount of items: 2
Items: 
Size: 658823 Color: 2
Size: 339760 Color: 0

Bin 135: 1432 of cap free
Amount of items: 2
Items: 
Size: 544623 Color: 1
Size: 453946 Color: 2

Bin 136: 1498 of cap free
Amount of items: 2
Items: 
Size: 591283 Color: 2
Size: 407220 Color: 3

Bin 137: 1515 of cap free
Amount of items: 3
Items: 
Size: 757457 Color: 2
Size: 121689 Color: 4
Size: 119340 Color: 3

Bin 138: 1529 of cap free
Amount of items: 2
Items: 
Size: 708211 Color: 3
Size: 290261 Color: 2

Bin 139: 1562 of cap free
Amount of items: 2
Items: 
Size: 752441 Color: 4
Size: 245998 Color: 0

Bin 140: 1578 of cap free
Amount of items: 2
Items: 
Size: 634658 Color: 4
Size: 363765 Color: 1

Bin 141: 1676 of cap free
Amount of items: 2
Items: 
Size: 706335 Color: 2
Size: 291990 Color: 4

Bin 142: 1678 of cap free
Amount of items: 2
Items: 
Size: 682366 Color: 1
Size: 315957 Color: 4

Bin 143: 1684 of cap free
Amount of items: 3
Items: 
Size: 555569 Color: 2
Size: 276398 Color: 2
Size: 166350 Color: 3

Bin 144: 1728 of cap free
Amount of items: 2
Items: 
Size: 742503 Color: 1
Size: 255770 Color: 4

Bin 145: 1754 of cap free
Amount of items: 2
Items: 
Size: 783291 Color: 3
Size: 214956 Color: 0

Bin 146: 1827 of cap free
Amount of items: 2
Items: 
Size: 500856 Color: 4
Size: 497318 Color: 1

Bin 147: 1856 of cap free
Amount of items: 2
Items: 
Size: 674747 Color: 3
Size: 323398 Color: 0

Bin 148: 1882 of cap free
Amount of items: 2
Items: 
Size: 778893 Color: 1
Size: 219226 Color: 0

Bin 149: 1898 of cap free
Amount of items: 2
Items: 
Size: 588389 Color: 2
Size: 409714 Color: 3

Bin 150: 1902 of cap free
Amount of items: 2
Items: 
Size: 685843 Color: 2
Size: 312256 Color: 0

Bin 151: 1975 of cap free
Amount of items: 2
Items: 
Size: 690855 Color: 1
Size: 307171 Color: 4

Bin 152: 1996 of cap free
Amount of items: 2
Items: 
Size: 651203 Color: 4
Size: 346802 Color: 0

Bin 153: 2010 of cap free
Amount of items: 2
Items: 
Size: 716233 Color: 4
Size: 281758 Color: 1

Bin 154: 2179 of cap free
Amount of items: 2
Items: 
Size: 731785 Color: 1
Size: 266037 Color: 2

Bin 155: 2305 of cap free
Amount of items: 2
Items: 
Size: 505165 Color: 4
Size: 492531 Color: 0

Bin 156: 2326 of cap free
Amount of items: 3
Items: 
Size: 728239 Color: 4
Size: 154730 Color: 0
Size: 114706 Color: 2

Bin 157: 2452 of cap free
Amount of items: 2
Items: 
Size: 771913 Color: 1
Size: 225636 Color: 3

Bin 158: 2454 of cap free
Amount of items: 2
Items: 
Size: 694171 Color: 3
Size: 303376 Color: 0

Bin 159: 2562 of cap free
Amount of items: 2
Items: 
Size: 605940 Color: 0
Size: 391499 Color: 1

Bin 160: 2603 of cap free
Amount of items: 2
Items: 
Size: 674424 Color: 4
Size: 322974 Color: 3

Bin 161: 2675 of cap free
Amount of items: 2
Items: 
Size: 684806 Color: 4
Size: 312520 Color: 2

Bin 162: 2758 of cap free
Amount of items: 2
Items: 
Size: 617391 Color: 4
Size: 379852 Color: 2

Bin 163: 2824 of cap free
Amount of items: 2
Items: 
Size: 631693 Color: 3
Size: 365484 Color: 1

Bin 164: 2827 of cap free
Amount of items: 2
Items: 
Size: 653450 Color: 2
Size: 343724 Color: 4

Bin 165: 2834 of cap free
Amount of items: 2
Items: 
Size: 717792 Color: 1
Size: 279375 Color: 4

Bin 166: 2857 of cap free
Amount of items: 2
Items: 
Size: 523463 Color: 0
Size: 473681 Color: 1

Bin 167: 3149 of cap free
Amount of items: 2
Items: 
Size: 653436 Color: 3
Size: 343416 Color: 1

Bin 168: 3258 of cap free
Amount of items: 2
Items: 
Size: 568702 Color: 4
Size: 428041 Color: 2

Bin 169: 3357 of cap free
Amount of items: 2
Items: 
Size: 625867 Color: 1
Size: 370777 Color: 2

Bin 170: 3360 of cap free
Amount of items: 2
Items: 
Size: 666146 Color: 0
Size: 330495 Color: 1

Bin 171: 3367 of cap free
Amount of items: 2
Items: 
Size: 588320 Color: 0
Size: 408314 Color: 2

Bin 172: 3597 of cap free
Amount of items: 2
Items: 
Size: 693163 Color: 1
Size: 303241 Color: 3

Bin 173: 3609 of cap free
Amount of items: 2
Items: 
Size: 712268 Color: 2
Size: 284124 Color: 4

Bin 174: 3613 of cap free
Amount of items: 2
Items: 
Size: 625617 Color: 2
Size: 370771 Color: 0

Bin 175: 3657 of cap free
Amount of items: 2
Items: 
Size: 604868 Color: 4
Size: 391476 Color: 1

Bin 176: 3662 of cap free
Amount of items: 2
Items: 
Size: 607315 Color: 1
Size: 389024 Color: 3

Bin 177: 3834 of cap free
Amount of items: 2
Items: 
Size: 674087 Color: 4
Size: 322080 Color: 0

Bin 178: 3868 of cap free
Amount of items: 2
Items: 
Size: 706164 Color: 2
Size: 289969 Color: 3

Bin 179: 3873 of cap free
Amount of items: 2
Items: 
Size: 706870 Color: 3
Size: 289258 Color: 2

Bin 180: 3986 of cap free
Amount of items: 2
Items: 
Size: 724008 Color: 2
Size: 272007 Color: 3

Bin 181: 4017 of cap free
Amount of items: 2
Items: 
Size: 740799 Color: 1
Size: 255185 Color: 0

Bin 182: 4029 of cap free
Amount of items: 2
Items: 
Size: 508257 Color: 1
Size: 487715 Color: 4

Bin 183: 4324 of cap free
Amount of items: 2
Items: 
Size: 523325 Color: 3
Size: 472352 Color: 1

Bin 184: 4360 of cap free
Amount of items: 2
Items: 
Size: 512544 Color: 1
Size: 483097 Color: 0

Bin 185: 4675 of cap free
Amount of items: 2
Items: 
Size: 524032 Color: 1
Size: 471294 Color: 3

Bin 186: 4702 of cap free
Amount of items: 2
Items: 
Size: 648694 Color: 1
Size: 346605 Color: 4

Bin 187: 5122 of cap free
Amount of items: 2
Items: 
Size: 771353 Color: 1
Size: 223526 Color: 4

Bin 188: 5583 of cap free
Amount of items: 2
Items: 
Size: 764330 Color: 1
Size: 230088 Color: 2

Bin 189: 6049 of cap free
Amount of items: 2
Items: 
Size: 765728 Color: 2
Size: 228224 Color: 1

Bin 190: 6128 of cap free
Amount of items: 2
Items: 
Size: 625387 Color: 0
Size: 368486 Color: 3

Bin 191: 6578 of cap free
Amount of items: 2
Items: 
Size: 522325 Color: 1
Size: 471098 Color: 2

Bin 192: 6830 of cap free
Amount of items: 2
Items: 
Size: 748580 Color: 3
Size: 244591 Color: 1

Bin 193: 6905 of cap free
Amount of items: 2
Items: 
Size: 602156 Color: 0
Size: 390940 Color: 1

Bin 194: 7055 of cap free
Amount of items: 2
Items: 
Size: 580105 Color: 1
Size: 412841 Color: 4

Bin 195: 7104 of cap free
Amount of items: 3
Items: 
Size: 738193 Color: 1
Size: 128915 Color: 2
Size: 125789 Color: 4

Bin 196: 7183 of cap free
Amount of items: 2
Items: 
Size: 546640 Color: 0
Size: 446178 Color: 1

Bin 197: 7183 of cap free
Amount of items: 2
Items: 
Size: 763471 Color: 1
Size: 229347 Color: 4

Bin 198: 7302 of cap free
Amount of items: 2
Items: 
Size: 778206 Color: 1
Size: 214493 Color: 3

Bin 199: 7308 of cap free
Amount of items: 2
Items: 
Size: 705760 Color: 0
Size: 286933 Color: 2

Bin 200: 7728 of cap free
Amount of items: 2
Items: 
Size: 672735 Color: 3
Size: 319538 Color: 0

Bin 201: 8284 of cap free
Amount of items: 2
Items: 
Size: 648983 Color: 4
Size: 342734 Color: 1

Bin 202: 8653 of cap free
Amount of items: 2
Items: 
Size: 587418 Color: 1
Size: 403930 Color: 4

Bin 203: 9109 of cap free
Amount of items: 2
Items: 
Size: 737717 Color: 0
Size: 253175 Color: 4

Bin 204: 9417 of cap free
Amount of items: 2
Items: 
Size: 533880 Color: 3
Size: 456704 Color: 2

Bin 205: 9484 of cap free
Amount of items: 2
Items: 
Size: 520444 Color: 1
Size: 470073 Color: 4

Bin 206: 9861 of cap free
Amount of items: 2
Items: 
Size: 736297 Color: 3
Size: 253843 Color: 0

Bin 207: 10371 of cap free
Amount of items: 2
Items: 
Size: 544134 Color: 3
Size: 445496 Color: 0

Bin 208: 12305 of cap free
Amount of items: 2
Items: 
Size: 531520 Color: 4
Size: 456176 Color: 1

Bin 209: 12595 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 0
Size: 374666 Color: 3

Bin 210: 15797 of cap free
Amount of items: 2
Items: 
Size: 495986 Color: 3
Size: 488218 Color: 1

Bin 211: 16640 of cap free
Amount of items: 2
Items: 
Size: 579582 Color: 1
Size: 403779 Color: 0

Bin 212: 17226 of cap free
Amount of items: 2
Items: 
Size: 663622 Color: 0
Size: 319153 Color: 4

Bin 213: 17589 of cap free
Amount of items: 2
Items: 
Size: 579308 Color: 4
Size: 403104 Color: 1

Bin 214: 20245 of cap free
Amount of items: 2
Items: 
Size: 577917 Color: 0
Size: 401839 Color: 4

Bin 215: 21355 of cap free
Amount of items: 2
Items: 
Size: 578654 Color: 4
Size: 399992 Color: 3

Bin 216: 21417 of cap free
Amount of items: 2
Items: 
Size: 644398 Color: 4
Size: 334186 Color: 2

Bin 217: 23659 of cap free
Amount of items: 2
Items: 
Size: 762231 Color: 3
Size: 214111 Color: 4

Bin 218: 30330 of cap free
Amount of items: 2
Items: 
Size: 658785 Color: 2
Size: 310886 Color: 0

Bin 219: 30692 of cap free
Amount of items: 2
Items: 
Size: 487984 Color: 1
Size: 481325 Color: 0

Bin 220: 35571 of cap free
Amount of items: 2
Items: 
Size: 601102 Color: 4
Size: 363328 Color: 3

Bin 221: 36487 of cap free
Amount of items: 2
Items: 
Size: 564018 Color: 2
Size: 399496 Color: 3

Bin 222: 40902 of cap free
Amount of items: 2
Items: 
Size: 776740 Color: 4
Size: 182359 Color: 2

Bin 223: 41103 of cap free
Amount of items: 2
Items: 
Size: 480464 Color: 0
Size: 478434 Color: 1

Bin 224: 60858 of cap free
Amount of items: 2
Items: 
Size: 641155 Color: 0
Size: 297988 Color: 1

Bin 225: 60877 of cap free
Amount of items: 2
Items: 
Size: 469669 Color: 3
Size: 469455 Color: 2

Bin 226: 61989 of cap free
Amount of items: 2
Items: 
Size: 759027 Color: 1
Size: 178985 Color: 0

Bin 227: 114315 of cap free
Amount of items: 2
Items: 
Size: 735008 Color: 3
Size: 150678 Color: 1

Bin 228: 276313 of cap free
Amount of items: 1
Items: 
Size: 723688 Color: 0

Total size: 226673258
Total free space: 1326970

