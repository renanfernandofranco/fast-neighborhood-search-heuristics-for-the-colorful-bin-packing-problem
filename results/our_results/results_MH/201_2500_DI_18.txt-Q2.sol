Capicity Bin: 1888
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 1
Size: 522 Color: 1
Size: 104 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 1
Size: 497 Color: 0
Size: 98 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 0
Size: 530 Color: 1
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 1
Size: 522 Color: 0
Size: 32 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1367 Color: 1
Size: 313 Color: 0
Size: 208 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 0
Size: 462 Color: 0
Size: 32 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1500 Color: 1
Size: 206 Color: 0
Size: 182 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 0
Size: 327 Color: 1
Size: 44 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 0
Size: 265 Color: 0
Size: 86 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 1
Size: 261 Color: 0
Size: 42 Color: 0

Bin 11: 1 of cap free
Amount of items: 4
Items: 
Size: 1073 Color: 0
Size: 501 Color: 0
Size: 269 Color: 1
Size: 44 Color: 1

Bin 12: 1 of cap free
Amount of items: 3
Items: 
Size: 1184 Color: 1
Size: 667 Color: 0
Size: 36 Color: 1

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 1187 Color: 0
Size: 640 Color: 0
Size: 60 Color: 1

Bin 14: 1 of cap free
Amount of items: 2
Items: 
Size: 1473 Color: 1
Size: 414 Color: 0

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 1495 Color: 1
Size: 392 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 1
Size: 178 Color: 0
Size: 132 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 1
Size: 254 Color: 1
Size: 20 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 1
Size: 124 Color: 0
Size: 112 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 0
Size: 198 Color: 1
Size: 42 Color: 0

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1670 Color: 0
Size: 217 Color: 1

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 1
Size: 209 Color: 0

Bin 22: 2 of cap free
Amount of items: 5
Items: 
Size: 946 Color: 1
Size: 491 Color: 1
Size: 191 Color: 0
Size: 144 Color: 0
Size: 114 Color: 1

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 1209 Color: 1
Size: 677 Color: 0

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 1451 Color: 1
Size: 435 Color: 0

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1562 Color: 0
Size: 324 Color: 1

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1593 Color: 1
Size: 293 Color: 0

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 1
Size: 213 Color: 0
Size: 44 Color: 0

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1633 Color: 1
Size: 253 Color: 0

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1081 Color: 0
Size: 760 Color: 1
Size: 44 Color: 0

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1304 Color: 0
Size: 581 Color: 1

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 0
Size: 231 Color: 1

Bin 32: 3 of cap free
Amount of items: 4
Items: 
Size: 1661 Color: 0
Size: 208 Color: 1
Size: 8 Color: 1
Size: 8 Color: 0

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 0
Size: 199 Color: 1

Bin 34: 4 of cap free
Amount of items: 3
Items: 
Size: 1090 Color: 1
Size: 638 Color: 0
Size: 156 Color: 1

Bin 35: 4 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 1
Size: 329 Color: 1
Size: 156 Color: 0

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1534 Color: 0
Size: 350 Color: 1

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1642 Color: 1
Size: 242 Color: 0

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1662 Color: 0
Size: 222 Color: 1

Bin 39: 5 of cap free
Amount of items: 4
Items: 
Size: 947 Color: 1
Size: 493 Color: 1
Size: 347 Color: 0
Size: 96 Color: 0

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1006 Color: 0
Size: 785 Color: 0
Size: 92 Color: 1

Bin 41: 7 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 0
Size: 387 Color: 1
Size: 80 Color: 1

Bin 42: 9 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 1
Size: 409 Color: 0

Bin 43: 9 of cap free
Amount of items: 2
Items: 
Size: 1581 Color: 0
Size: 298 Color: 1

Bin 44: 9 of cap free
Amount of items: 2
Items: 
Size: 1622 Color: 0
Size: 257 Color: 1

Bin 45: 10 of cap free
Amount of items: 2
Items: 
Size: 1197 Color: 1
Size: 681 Color: 0

Bin 46: 10 of cap free
Amount of items: 2
Items: 
Size: 1513 Color: 1
Size: 365 Color: 0

Bin 47: 12 of cap free
Amount of items: 2
Items: 
Size: 1089 Color: 1
Size: 787 Color: 0

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 0
Size: 274 Color: 1

Bin 49: 14 of cap free
Amount of items: 2
Items: 
Size: 1201 Color: 0
Size: 673 Color: 1

Bin 50: 14 of cap free
Amount of items: 2
Items: 
Size: 1297 Color: 0
Size: 577 Color: 1

Bin 51: 17 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 1
Size: 322 Color: 0

Bin 52: 19 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 1
Size: 283 Color: 0

Bin 53: 20 of cap free
Amount of items: 2
Items: 
Size: 1301 Color: 1
Size: 567 Color: 0

Bin 54: 22 of cap free
Amount of items: 24
Items: 
Size: 134 Color: 0
Size: 114 Color: 0
Size: 114 Color: 0
Size: 104 Color: 1
Size: 100 Color: 1
Size: 98 Color: 1
Size: 92 Color: 1
Size: 80 Color: 0
Size: 76 Color: 0
Size: 76 Color: 0
Size: 72 Color: 1
Size: 72 Color: 1
Size: 68 Color: 1
Size: 68 Color: 0
Size: 68 Color: 0
Size: 68 Color: 0
Size: 64 Color: 0
Size: 64 Color: 0
Size: 62 Color: 1
Size: 58 Color: 1
Size: 56 Color: 1
Size: 56 Color: 0
Size: 52 Color: 1
Size: 50 Color: 1

Bin 55: 24 of cap free
Amount of items: 2
Items: 
Size: 1126 Color: 1
Size: 738 Color: 0

Bin 56: 24 of cap free
Amount of items: 2
Items: 
Size: 1506 Color: 1
Size: 358 Color: 0

Bin 57: 25 of cap free
Amount of items: 2
Items: 
Size: 1077 Color: 1
Size: 786 Color: 0

Bin 58: 25 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 0
Size: 292 Color: 1

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1193 Color: 0
Size: 669 Color: 1

Bin 60: 27 of cap free
Amount of items: 7
Items: 
Size: 945 Color: 0
Size: 190 Color: 0
Size: 170 Color: 0
Size: 156 Color: 1
Size: 134 Color: 1
Size: 134 Color: 0
Size: 132 Color: 1

Bin 61: 27 of cap free
Amount of items: 2
Items: 
Size: 1425 Color: 0
Size: 436 Color: 1

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1254 Color: 1
Size: 606 Color: 0

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 1287 Color: 0
Size: 573 Color: 1

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1462 Color: 0
Size: 398 Color: 1

Bin 65: 32 of cap free
Amount of items: 2
Items: 
Size: 1190 Color: 0
Size: 666 Color: 1

Bin 66: 1340 of cap free
Amount of items: 14
Items: 
Size: 52 Color: 0
Size: 50 Color: 1
Size: 50 Color: 0
Size: 48 Color: 0
Size: 48 Color: 0
Size: 40 Color: 1
Size: 40 Color: 1
Size: 38 Color: 1
Size: 36 Color: 1
Size: 36 Color: 1
Size: 36 Color: 1
Size: 32 Color: 0
Size: 26 Color: 0
Size: 16 Color: 0

Total size: 122720
Total free space: 1888

