Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 11
Size: 292 Color: 19
Size: 276 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9
Size: 259 Color: 1
Size: 250 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8
Size: 311 Color: 6
Size: 265 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 6
Size: 303 Color: 3
Size: 289 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 16
Size: 295 Color: 10
Size: 257 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 11
Size: 266 Color: 8
Size: 251 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 9
Size: 328 Color: 15
Size: 280 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 14
Size: 294 Color: 19
Size: 277 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 363 Color: 10
Size: 254 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 19
Size: 303 Color: 9
Size: 298 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 10
Size: 331 Color: 7
Size: 271 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 328 Color: 3
Size: 279 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 15
Size: 317 Color: 9
Size: 262 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 11
Size: 325 Color: 18
Size: 324 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 11
Size: 353 Color: 10
Size: 275 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 13
Size: 338 Color: 14
Size: 260 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 6
Size: 330 Color: 10
Size: 256 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 18
Size: 255 Color: 2
Size: 252 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 7
Size: 259 Color: 11
Size: 250 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 10
Size: 363 Color: 13
Size: 262 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 6
Size: 251 Color: 17
Size: 251 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 14
Size: 322 Color: 7
Size: 289 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 10
Size: 336 Color: 6
Size: 268 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 12
Size: 366 Color: 18
Size: 253 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 7
Size: 329 Color: 6
Size: 268 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 311 Color: 9
Size: 268 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 8
Size: 254 Color: 1
Size: 250 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 14
Size: 296 Color: 4
Size: 260 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 11
Size: 283 Color: 5
Size: 252 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 308 Color: 16
Size: 259 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 265 Color: 1
Size: 250 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 10
Size: 316 Color: 11
Size: 316 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 5
Size: 367 Color: 2
Size: 253 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 16
Size: 308 Color: 11
Size: 303 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 13
Size: 335 Color: 2
Size: 259 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 9
Size: 313 Color: 14
Size: 260 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 18
Size: 353 Color: 14
Size: 282 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 14
Size: 340 Color: 9
Size: 295 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 347 Color: 4
Size: 296 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 363 Color: 14
Size: 265 Color: 7

Total size: 40000
Total free space: 0

