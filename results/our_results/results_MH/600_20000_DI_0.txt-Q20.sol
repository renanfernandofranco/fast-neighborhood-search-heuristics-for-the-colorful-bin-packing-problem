Capicity Bin: 16256
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9142 Color: 19
Size: 6764 Color: 14
Size: 350 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 15
Size: 6568 Color: 2
Size: 444 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 12
Size: 5832 Color: 15
Size: 336 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10204 Color: 7
Size: 5772 Color: 7
Size: 280 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 12
Size: 4988 Color: 4
Size: 620 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10877 Color: 8
Size: 3435 Color: 1
Size: 1944 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 5
Size: 4984 Color: 15
Size: 320 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 16
Size: 4542 Color: 18
Size: 598 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12035 Color: 0
Size: 3431 Color: 15
Size: 790 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 7
Size: 3752 Color: 16
Size: 480 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12098 Color: 6
Size: 3166 Color: 14
Size: 992 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12115 Color: 18
Size: 3451 Color: 3
Size: 690 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12114 Color: 0
Size: 3454 Color: 1
Size: 688 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 6
Size: 3528 Color: 14
Size: 336 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 16
Size: 3524 Color: 7
Size: 312 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12526 Color: 14
Size: 3010 Color: 15
Size: 720 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 7
Size: 3016 Color: 4
Size: 708 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12637 Color: 0
Size: 3017 Color: 9
Size: 602 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 15
Size: 3302 Color: 1
Size: 308 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13006 Color: 5
Size: 2710 Color: 9
Size: 540 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 8
Size: 1564 Color: 17
Size: 1516 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 9
Size: 2058 Color: 15
Size: 1000 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13206 Color: 7
Size: 1802 Color: 0
Size: 1248 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 8
Size: 2662 Color: 2
Size: 330 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13267 Color: 12
Size: 2481 Color: 4
Size: 508 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 4
Size: 2476 Color: 17
Size: 512 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13278 Color: 8
Size: 1906 Color: 12
Size: 1072 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 3
Size: 1972 Color: 2
Size: 992 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 16
Size: 2622 Color: 15
Size: 310 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 3
Size: 2648 Color: 4
Size: 96 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13515 Color: 6
Size: 2285 Color: 4
Size: 456 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 7
Size: 2444 Color: 4
Size: 284 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 3
Size: 2062 Color: 9
Size: 544 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 11
Size: 2164 Color: 13
Size: 432 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 11
Size: 1450 Color: 10
Size: 1024 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 19
Size: 1558 Color: 17
Size: 908 Color: 8

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 18
Size: 2056 Color: 10
Size: 308 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 12
Size: 1945 Color: 14
Size: 388 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 4
Size: 1476 Color: 11
Size: 856 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14009 Color: 19
Size: 1873 Color: 16
Size: 374 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 18
Size: 1882 Color: 5
Size: 360 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 4
Size: 1372 Color: 18
Size: 816 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 9
Size: 1818 Color: 0
Size: 360 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14089 Color: 0
Size: 1481 Color: 8
Size: 686 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14094 Color: 18
Size: 1822 Color: 4
Size: 340 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14162 Color: 19
Size: 1470 Color: 6
Size: 624 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 10
Size: 1344 Color: 11
Size: 702 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 1
Size: 1022 Color: 14
Size: 1022 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 8
Size: 1296 Color: 11
Size: 712 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 9
Size: 1582 Color: 17
Size: 408 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 18
Size: 1444 Color: 16
Size: 544 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14339 Color: 4
Size: 1655 Color: 14
Size: 262 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 13
Size: 1020 Color: 5
Size: 904 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14354 Color: 13
Size: 1434 Color: 16
Size: 468 Color: 10

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14362 Color: 16
Size: 1354 Color: 4
Size: 540 Color: 13

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14408 Color: 17
Size: 1528 Color: 14
Size: 320 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 16
Size: 1400 Color: 5
Size: 416 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14507 Color: 18
Size: 1459 Color: 9
Size: 290 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 13
Size: 1546 Color: 10
Size: 192 Color: 8

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 14
Size: 1200 Color: 17
Size: 524 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14591 Color: 5
Size: 1389 Color: 9
Size: 276 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14603 Color: 11
Size: 1379 Color: 0
Size: 274 Color: 2

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10010 Color: 3
Size: 4546 Color: 15
Size: 1699 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10119 Color: 19
Size: 5844 Color: 0
Size: 292 Color: 12

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 6
Size: 5104 Color: 16
Size: 272 Color: 17

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 10
Size: 2129 Color: 10
Size: 2124 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12264 Color: 13
Size: 3519 Color: 10
Size: 472 Color: 16

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12559 Color: 1
Size: 2330 Color: 14
Size: 1366 Color: 11

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 7
Size: 3081 Color: 2
Size: 272 Color: 10

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 13507 Color: 5
Size: 2748 Color: 7

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 13705 Color: 5
Size: 2550 Color: 15

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 9332 Color: 13
Size: 5930 Color: 5
Size: 992 Color: 11

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 11048 Color: 14
Size: 5206 Color: 13

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 13
Size: 4602 Color: 0
Size: 428 Color: 6

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 4
Size: 3336 Color: 18
Size: 322 Color: 4

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 12960 Color: 9
Size: 2702 Color: 10
Size: 592 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 7
Size: 2663 Color: 14
Size: 312 Color: 0

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13703 Color: 1
Size: 1807 Color: 4
Size: 744 Color: 2

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 10
Size: 1864 Color: 15

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 14606 Color: 1
Size: 1152 Color: 9
Size: 496 Color: 0

Bin 81: 3 of cap free
Amount of items: 5
Items: 
Size: 8136 Color: 3
Size: 3466 Color: 3
Size: 3439 Color: 7
Size: 832 Color: 7
Size: 380 Color: 10

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 8
Size: 5111 Color: 8
Size: 464 Color: 13

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 11370 Color: 11
Size: 4483 Color: 4
Size: 400 Color: 15

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 19
Size: 3596 Color: 9

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 10
Size: 2667 Color: 8

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 2
Size: 1545 Color: 4
Size: 908 Color: 12

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 10706 Color: 0
Size: 3942 Color: 6
Size: 1604 Color: 12

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 11505 Color: 17
Size: 3957 Color: 4
Size: 790 Color: 3

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 17
Size: 3052 Color: 15
Size: 296 Color: 19

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13325 Color: 6
Size: 2927 Color: 17

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 19
Size: 2492 Color: 15
Size: 392 Color: 7

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 1
Size: 2448 Color: 0
Size: 400 Color: 9

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 6
Size: 2208 Color: 15

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 11513 Color: 2
Size: 4626 Color: 4
Size: 112 Color: 12

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 14235 Color: 18
Size: 2016 Color: 15

Bin 96: 6 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 13
Size: 4562 Color: 15
Size: 352 Color: 2

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 11509 Color: 4
Size: 3953 Color: 19
Size: 788 Color: 5

Bin 98: 7 of cap free
Amount of items: 5
Items: 
Size: 9215 Color: 17
Size: 3947 Color: 14
Size: 2127 Color: 10
Size: 656 Color: 19
Size: 304 Color: 14

Bin 99: 7 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 4
Size: 5109 Color: 2
Size: 864 Color: 5

Bin 100: 7 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 7
Size: 3001 Color: 17
Size: 600 Color: 0

Bin 101: 8 of cap free
Amount of items: 7
Items: 
Size: 8132 Color: 2
Size: 1688 Color: 16
Size: 1685 Color: 12
Size: 1559 Color: 0
Size: 1344 Color: 8
Size: 924 Color: 11
Size: 916 Color: 18

Bin 102: 8 of cap free
Amount of items: 3
Items: 
Size: 9816 Color: 14
Size: 6000 Color: 0
Size: 432 Color: 3

Bin 103: 8 of cap free
Amount of items: 2
Items: 
Size: 10160 Color: 13
Size: 6088 Color: 12

Bin 104: 8 of cap free
Amount of items: 3
Items: 
Size: 10244 Color: 15
Size: 5812 Color: 9
Size: 192 Color: 15

Bin 105: 8 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 3
Size: 4260 Color: 14
Size: 256 Color: 9

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 11756 Color: 9
Size: 4316 Color: 7
Size: 176 Color: 5

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 19
Size: 2482 Color: 8

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 14444 Color: 8
Size: 1804 Color: 2

Bin 109: 9 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 0
Size: 5867 Color: 6
Size: 1044 Color: 10

Bin 110: 9 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 0
Size: 3429 Color: 5
Size: 736 Color: 10

Bin 111: 9 of cap free
Amount of items: 2
Items: 
Size: 12143 Color: 16
Size: 4104 Color: 15

Bin 112: 9 of cap free
Amount of items: 3
Items: 
Size: 13061 Color: 9
Size: 2630 Color: 3
Size: 556 Color: 0

Bin 113: 9 of cap free
Amount of items: 2
Items: 
Size: 14269 Color: 9
Size: 1978 Color: 17

Bin 114: 9 of cap free
Amount of items: 2
Items: 
Size: 14403 Color: 18
Size: 1844 Color: 6

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 14479 Color: 16
Size: 1768 Color: 3

Bin 116: 10 of cap free
Amount of items: 33
Items: 
Size: 692 Color: 3
Size: 686 Color: 4
Size: 686 Color: 4
Size: 632 Color: 16
Size: 632 Color: 2
Size: 616 Color: 15
Size: 616 Color: 7
Size: 584 Color: 13
Size: 544 Color: 2
Size: 544 Color: 1
Size: 532 Color: 10
Size: 524 Color: 19
Size: 512 Color: 10
Size: 512 Color: 0
Size: 496 Color: 1
Size: 488 Color: 6
Size: 488 Color: 4
Size: 480 Color: 11
Size: 464 Color: 10
Size: 458 Color: 9
Size: 448 Color: 13
Size: 448 Color: 7
Size: 424 Color: 11
Size: 424 Color: 9
Size: 412 Color: 14
Size: 412 Color: 6
Size: 392 Color: 1
Size: 384 Color: 8
Size: 364 Color: 19
Size: 360 Color: 8
Size: 352 Color: 15
Size: 352 Color: 15
Size: 288 Color: 19

Bin 117: 10 of cap free
Amount of items: 3
Items: 
Size: 9138 Color: 15
Size: 6772 Color: 4
Size: 336 Color: 13

Bin 118: 10 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 19
Size: 5012 Color: 6
Size: 496 Color: 2

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 12590 Color: 15
Size: 3656 Color: 2

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 4
Size: 2724 Color: 7
Size: 508 Color: 15

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13110 Color: 12
Size: 3136 Color: 7

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13966 Color: 9
Size: 2280 Color: 7

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 17
Size: 2184 Color: 13

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 14242 Color: 7
Size: 2004 Color: 6

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 14538 Color: 9
Size: 1708 Color: 3

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 11
Size: 1662 Color: 12

Bin 127: 10 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 13
Size: 1608 Color: 19
Size: 20 Color: 15

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 13886 Color: 17
Size: 2359 Color: 0

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 5
Size: 2808 Color: 14

Bin 130: 13 of cap free
Amount of items: 3
Items: 
Size: 10127 Color: 0
Size: 5844 Color: 0
Size: 272 Color: 4

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 13863 Color: 1
Size: 2380 Color: 14

Bin 132: 14 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 2
Size: 6770 Color: 6
Size: 752 Color: 10
Size: 328 Color: 0

Bin 133: 14 of cap free
Amount of items: 3
Items: 
Size: 10786 Color: 6
Size: 5044 Color: 8
Size: 412 Color: 0

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 14100 Color: 19
Size: 2142 Color: 10

Bin 135: 15 of cap free
Amount of items: 9
Items: 
Size: 8129 Color: 10
Size: 1184 Color: 5
Size: 1184 Color: 3
Size: 1172 Color: 5
Size: 1172 Color: 2
Size: 1168 Color: 13
Size: 896 Color: 8
Size: 704 Color: 9
Size: 632 Color: 17

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 14152 Color: 15
Size: 2088 Color: 6

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 14494 Color: 9
Size: 1746 Color: 8

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 13062 Color: 14
Size: 3176 Color: 8

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 14328 Color: 9
Size: 1910 Color: 0

Bin 140: 19 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 5
Size: 4481 Color: 0
Size: 608 Color: 8

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 12964 Color: 7
Size: 3273 Color: 18

Bin 142: 21 of cap free
Amount of items: 3
Items: 
Size: 10123 Color: 12
Size: 6008 Color: 13
Size: 104 Color: 11

Bin 143: 22 of cap free
Amount of items: 3
Items: 
Size: 11736 Color: 14
Size: 3682 Color: 16
Size: 816 Color: 15

Bin 144: 22 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 12
Size: 1588 Color: 14
Size: 34 Color: 8

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 0
Size: 4284 Color: 8

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 14572 Color: 3
Size: 1660 Color: 5

Bin 147: 25 of cap free
Amount of items: 3
Items: 
Size: 11004 Color: 5
Size: 5115 Color: 9
Size: 112 Color: 18

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 11788 Color: 14
Size: 4442 Color: 17

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 12458 Color: 5
Size: 3772 Color: 7

Bin 150: 27 of cap free
Amount of items: 2
Items: 
Size: 14151 Color: 2
Size: 2078 Color: 17

Bin 151: 28 of cap free
Amount of items: 3
Items: 
Size: 10802 Color: 1
Size: 5234 Color: 5
Size: 192 Color: 3

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 11084 Color: 11
Size: 5144 Color: 19

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 12028 Color: 1
Size: 4200 Color: 16

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 6
Size: 2798 Color: 4

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 0
Size: 2542 Color: 16

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 13716 Color: 16
Size: 2512 Color: 5

Bin 157: 29 of cap free
Amount of items: 2
Items: 
Size: 12745 Color: 13
Size: 3482 Color: 18

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 13930 Color: 10
Size: 2296 Color: 9

Bin 159: 32 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 1
Size: 4344 Color: 9

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 10
Size: 3768 Color: 1

Bin 161: 37 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 10
Size: 2291 Color: 11

Bin 162: 38 of cap free
Amount of items: 2
Items: 
Size: 11838 Color: 8
Size: 4380 Color: 3

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 13992 Color: 13
Size: 2226 Color: 1

Bin 164: 38 of cap free
Amount of items: 2
Items: 
Size: 14388 Color: 13
Size: 1830 Color: 6

Bin 165: 40 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 7
Size: 7120 Color: 5
Size: 304 Color: 9

Bin 166: 40 of cap free
Amount of items: 3
Items: 
Size: 12131 Color: 17
Size: 3961 Color: 13
Size: 124 Color: 18

Bin 167: 41 of cap free
Amount of items: 2
Items: 
Size: 12139 Color: 3
Size: 4076 Color: 4

Bin 168: 41 of cap free
Amount of items: 2
Items: 
Size: 14387 Color: 1
Size: 1828 Color: 3

Bin 169: 42 of cap free
Amount of items: 19
Items: 
Size: 1160 Color: 14
Size: 1160 Color: 12
Size: 1152 Color: 9
Size: 1008 Color: 19
Size: 928 Color: 13
Size: 928 Color: 7
Size: 912 Color: 16
Size: 912 Color: 9
Size: 896 Color: 0
Size: 856 Color: 4
Size: 848 Color: 1
Size: 812 Color: 15
Size: 790 Color: 6
Size: 752 Color: 11
Size: 704 Color: 7
Size: 692 Color: 14
Size: 684 Color: 10
Size: 532 Color: 11
Size: 488 Color: 8

Bin 170: 42 of cap free
Amount of items: 2
Items: 
Size: 10280 Color: 9
Size: 5934 Color: 4

Bin 171: 42 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 8
Size: 2174 Color: 7

Bin 172: 43 of cap free
Amount of items: 5
Items: 
Size: 8134 Color: 9
Size: 2443 Color: 12
Size: 2408 Color: 18
Size: 2356 Color: 7
Size: 872 Color: 13

Bin 173: 44 of cap free
Amount of items: 2
Items: 
Size: 13102 Color: 9
Size: 3110 Color: 3

Bin 174: 46 of cap free
Amount of items: 2
Items: 
Size: 9978 Color: 4
Size: 6232 Color: 16

Bin 175: 46 of cap free
Amount of items: 2
Items: 
Size: 11530 Color: 18
Size: 4680 Color: 15

Bin 176: 46 of cap free
Amount of items: 2
Items: 
Size: 13852 Color: 12
Size: 2358 Color: 15

Bin 177: 47 of cap free
Amount of items: 2
Items: 
Size: 12135 Color: 10
Size: 4074 Color: 1

Bin 178: 48 of cap free
Amount of items: 2
Items: 
Size: 12452 Color: 8
Size: 3756 Color: 11

Bin 179: 48 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 19
Size: 2568 Color: 15

Bin 180: 52 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 18
Size: 3108 Color: 14

Bin 181: 56 of cap free
Amount of items: 2
Items: 
Size: 14226 Color: 1
Size: 1974 Color: 5

Bin 182: 57 of cap free
Amount of items: 2
Items: 
Size: 12653 Color: 1
Size: 3546 Color: 6

Bin 183: 58 of cap free
Amount of items: 2
Items: 
Size: 14492 Color: 11
Size: 1706 Color: 18

Bin 184: 62 of cap free
Amount of items: 2
Items: 
Size: 11402 Color: 19
Size: 4792 Color: 5

Bin 185: 64 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 9
Size: 3204 Color: 8

Bin 186: 69 of cap free
Amount of items: 2
Items: 
Size: 13462 Color: 9
Size: 2725 Color: 4

Bin 187: 78 of cap free
Amount of items: 2
Items: 
Size: 10810 Color: 8
Size: 5368 Color: 11

Bin 188: 85 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 16
Size: 6771 Color: 1
Size: 432 Color: 2

Bin 189: 85 of cap free
Amount of items: 2
Items: 
Size: 11521 Color: 16
Size: 4650 Color: 1

Bin 190: 92 of cap free
Amount of items: 2
Items: 
Size: 8148 Color: 2
Size: 8016 Color: 9

Bin 191: 95 of cap free
Amount of items: 3
Items: 
Size: 9217 Color: 8
Size: 6616 Color: 12
Size: 328 Color: 7

Bin 192: 97 of cap free
Amount of items: 2
Items: 
Size: 12987 Color: 10
Size: 3172 Color: 14

Bin 193: 98 of cap free
Amount of items: 6
Items: 
Size: 8133 Color: 10
Size: 1948 Color: 1
Size: 1930 Color: 15
Size: 1755 Color: 4
Size: 1352 Color: 19
Size: 1040 Color: 1

Bin 194: 155 of cap free
Amount of items: 3
Items: 
Size: 9448 Color: 5
Size: 5869 Color: 17
Size: 784 Color: 2

Bin 195: 196 of cap free
Amount of items: 2
Items: 
Size: 9284 Color: 3
Size: 6776 Color: 5

Bin 196: 210 of cap free
Amount of items: 2
Items: 
Size: 9272 Color: 5
Size: 6774 Color: 12

Bin 197: 231 of cap free
Amount of items: 2
Items: 
Size: 9252 Color: 1
Size: 6773 Color: 6

Bin 198: 254 of cap free
Amount of items: 7
Items: 
Size: 8130 Color: 0
Size: 1544 Color: 17
Size: 1352 Color: 13
Size: 1352 Color: 5
Size: 1352 Color: 2
Size: 1232 Color: 18
Size: 1040 Color: 19

Bin 199: 12272 of cap free
Amount of items: 12
Items: 
Size: 392 Color: 0
Size: 384 Color: 17
Size: 368 Color: 13
Size: 364 Color: 11
Size: 360 Color: 16
Size: 348 Color: 5
Size: 320 Color: 15
Size: 296 Color: 8
Size: 288 Color: 19
Size: 288 Color: 14
Size: 288 Color: 12
Size: 288 Color: 3

Total size: 3218688
Total free space: 16256

