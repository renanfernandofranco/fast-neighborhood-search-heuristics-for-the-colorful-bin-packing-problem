Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 474477 Color: 1
Size: 264487 Color: 1
Size: 261037 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 403104 Color: 1
Size: 322007 Color: 1
Size: 274890 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 442881 Color: 1
Size: 294969 Color: 1
Size: 262151 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 485371 Color: 1
Size: 263148 Color: 1
Size: 251482 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 358316 Color: 1
Size: 355003 Color: 1
Size: 286682 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 359871 Color: 1
Size: 331268 Color: 1
Size: 308862 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 397591 Color: 1
Size: 343396 Color: 1
Size: 259014 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 475414 Color: 1
Size: 264257 Color: 1
Size: 260330 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 410022 Color: 1
Size: 312139 Color: 1
Size: 277840 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 472683 Color: 1
Size: 268511 Color: 1
Size: 258807 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 408190 Color: 1
Size: 341241 Color: 1
Size: 250570 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 378111 Color: 1
Size: 343883 Color: 1
Size: 278007 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 408723 Color: 1
Size: 297984 Color: 1
Size: 293294 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 485865 Color: 1
Size: 258143 Color: 1
Size: 255993 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 433561 Color: 1
Size: 293370 Color: 1
Size: 273070 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 469234 Color: 1
Size: 277719 Color: 1
Size: 253048 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 482109 Color: 1
Size: 263858 Color: 1
Size: 254034 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 486530 Color: 1
Size: 262476 Color: 1
Size: 250995 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 396214 Color: 1
Size: 327567 Color: 1
Size: 276220 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 477344 Color: 1
Size: 266771 Color: 1
Size: 255886 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 413458 Color: 1
Size: 302992 Color: 1
Size: 283551 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 455192 Color: 1
Size: 294062 Color: 1
Size: 250747 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 493659 Color: 1
Size: 254257 Color: 1
Size: 252085 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 483687 Color: 1
Size: 258415 Color: 1
Size: 257899 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 487435 Color: 1
Size: 258450 Color: 1
Size: 254116 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 483328 Color: 1
Size: 264379 Color: 1
Size: 252294 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 373851 Color: 1
Size: 320844 Color: 1
Size: 305306 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 439670 Color: 1
Size: 280543 Color: 1
Size: 279788 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 429366 Color: 1
Size: 308014 Color: 1
Size: 262621 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 360189 Color: 1
Size: 331185 Color: 1
Size: 308627 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 497329 Color: 1
Size: 252521 Color: 1
Size: 250151 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 410887 Color: 1
Size: 305489 Color: 1
Size: 283625 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 481237 Color: 1
Size: 265399 Color: 1
Size: 253365 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 441300 Color: 1
Size: 305416 Color: 1
Size: 253285 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 489381 Color: 1
Size: 258078 Color: 1
Size: 252542 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 382313 Color: 1
Size: 343975 Color: 1
Size: 273713 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 385163 Color: 1
Size: 331130 Color: 1
Size: 283708 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 455154 Color: 1
Size: 272693 Color: 1
Size: 272154 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 355666 Color: 1
Size: 334046 Color: 1
Size: 310289 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 364547 Color: 1
Size: 352694 Color: 1
Size: 282760 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 361114 Color: 1
Size: 325231 Color: 1
Size: 313656 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 481369 Color: 1
Size: 267331 Color: 1
Size: 251301 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 445292 Color: 1
Size: 292243 Color: 1
Size: 262466 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 494099 Color: 1
Size: 253038 Color: 1
Size: 252864 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 466500 Color: 1
Size: 280753 Color: 1
Size: 252748 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 405317 Color: 1
Size: 326115 Color: 1
Size: 268569 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 480652 Color: 1
Size: 260488 Color: 1
Size: 258861 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 494595 Color: 1
Size: 254018 Color: 1
Size: 251388 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 480133 Color: 1
Size: 264335 Color: 1
Size: 255533 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 437231 Color: 1
Size: 284557 Color: 1
Size: 278213 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 382839 Color: 1
Size: 327068 Color: 1
Size: 290094 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 491971 Color: 1
Size: 256510 Color: 0
Size: 251520 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 432558 Color: 1
Size: 315282 Color: 1
Size: 252161 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 474125 Color: 1
Size: 269337 Color: 1
Size: 256539 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 468709 Color: 1
Size: 267972 Color: 1
Size: 263320 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 372774 Color: 1
Size: 355434 Color: 1
Size: 271793 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 473569 Color: 1
Size: 271368 Color: 1
Size: 255064 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 458551 Color: 1
Size: 290781 Color: 1
Size: 250669 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 445956 Color: 1
Size: 294063 Color: 1
Size: 259982 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 413487 Color: 1
Size: 321673 Color: 1
Size: 264841 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 457015 Color: 1
Size: 277082 Color: 1
Size: 265904 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 458886 Color: 1
Size: 275814 Color: 1
Size: 265301 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 472290 Color: 1
Size: 271435 Color: 1
Size: 256276 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 375182 Color: 1
Size: 326455 Color: 1
Size: 298364 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 378508 Color: 1
Size: 351764 Color: 1
Size: 269729 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 351243 Color: 1
Size: 350151 Color: 1
Size: 298607 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 492949 Color: 1
Size: 255158 Color: 1
Size: 251894 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 466504 Color: 1
Size: 278711 Color: 1
Size: 254786 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 385581 Color: 1
Size: 332828 Color: 1
Size: 281592 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 409551 Color: 1
Size: 336860 Color: 1
Size: 253590 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 460717 Color: 1
Size: 281137 Color: 1
Size: 258147 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 438418 Color: 1
Size: 292486 Color: 1
Size: 269097 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 411329 Color: 1
Size: 306813 Color: 1
Size: 281859 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 431863 Color: 1
Size: 287430 Color: 1
Size: 280708 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 451478 Color: 1
Size: 290552 Color: 1
Size: 257971 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 482471 Color: 1
Size: 261173 Color: 1
Size: 256357 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 408470 Color: 1
Size: 326827 Color: 1
Size: 264704 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 352103 Color: 1
Size: 333070 Color: 1
Size: 314828 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 452856 Color: 1
Size: 295715 Color: 1
Size: 251430 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 357687 Color: 1
Size: 339666 Color: 1
Size: 302648 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 389254 Color: 1
Size: 310771 Color: 1
Size: 299976 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 480836 Color: 1
Size: 264942 Color: 1
Size: 254223 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 428758 Color: 1
Size: 314898 Color: 1
Size: 256345 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 376858 Color: 1
Size: 355276 Color: 1
Size: 267867 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 361811 Color: 1
Size: 356044 Color: 1
Size: 282146 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 492306 Color: 1
Size: 257101 Color: 1
Size: 250594 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 456687 Color: 1
Size: 277022 Color: 1
Size: 266292 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 354470 Color: 1
Size: 326823 Color: 1
Size: 318708 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 444413 Color: 1
Size: 280440 Color: 1
Size: 275148 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 373214 Color: 1
Size: 332370 Color: 1
Size: 294417 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 449378 Color: 1
Size: 282868 Color: 1
Size: 267755 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 499675 Color: 1
Size: 250169 Color: 1
Size: 250157 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 450726 Color: 1
Size: 284756 Color: 1
Size: 264519 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 363591 Color: 1
Size: 359177 Color: 1
Size: 277233 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 379737 Color: 1
Size: 329417 Color: 1
Size: 290847 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 478323 Color: 1
Size: 267006 Color: 1
Size: 254672 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 499273 Color: 1
Size: 250595 Color: 1
Size: 250133 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 388238 Color: 1
Size: 343829 Color: 1
Size: 267934 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 371056 Color: 1
Size: 356664 Color: 1
Size: 272281 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 458657 Color: 1
Size: 287396 Color: 1
Size: 253948 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 354554 Color: 1
Size: 354000 Color: 1
Size: 291447 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 452330 Color: 1
Size: 294119 Color: 1
Size: 253552 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 419386 Color: 1
Size: 319284 Color: 1
Size: 261331 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 399609 Color: 1
Size: 300536 Color: 1
Size: 299856 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 383665 Color: 1
Size: 335786 Color: 1
Size: 280550 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 454627 Color: 1
Size: 274225 Color: 1
Size: 271149 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 448929 Color: 1
Size: 284102 Color: 1
Size: 266970 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 488800 Color: 1
Size: 256900 Color: 1
Size: 254301 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 483272 Color: 1
Size: 261242 Color: 1
Size: 255487 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 464169 Color: 1
Size: 275757 Color: 1
Size: 260075 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 411627 Color: 1
Size: 315696 Color: 1
Size: 272678 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 371854 Color: 1
Size: 334964 Color: 1
Size: 293183 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 396104 Color: 1
Size: 323686 Color: 1
Size: 280211 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 439597 Color: 1
Size: 308389 Color: 1
Size: 252015 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 457873 Color: 1
Size: 282141 Color: 1
Size: 259987 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 452382 Color: 1
Size: 274034 Color: 1
Size: 273585 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 489584 Color: 1
Size: 256847 Color: 1
Size: 253570 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 453046 Color: 1
Size: 273568 Color: 1
Size: 273387 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 477204 Color: 1
Size: 267107 Color: 1
Size: 255690 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 472520 Color: 1
Size: 270215 Color: 1
Size: 257266 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 356484 Color: 1
Size: 333392 Color: 1
Size: 310125 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 498772 Color: 1
Size: 250718 Color: 1
Size: 250511 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 400999 Color: 1
Size: 316639 Color: 1
Size: 282363 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 449493 Color: 1
Size: 280555 Color: 1
Size: 269953 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 464513 Color: 1
Size: 276685 Color: 1
Size: 258803 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 372585 Color: 1
Size: 332670 Color: 1
Size: 294746 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 440272 Color: 1
Size: 304135 Color: 1
Size: 255594 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 393230 Color: 1
Size: 336206 Color: 1
Size: 270565 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 386387 Color: 1
Size: 322727 Color: 1
Size: 290887 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 411499 Color: 1
Size: 318128 Color: 1
Size: 270374 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 458868 Color: 1
Size: 275178 Color: 1
Size: 265955 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 376352 Color: 1
Size: 344828 Color: 1
Size: 278821 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 388283 Color: 1
Size: 311760 Color: 1
Size: 299958 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 396842 Color: 1
Size: 327170 Color: 1
Size: 275989 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 397059 Color: 1
Size: 346180 Color: 1
Size: 256762 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 420274 Color: 1
Size: 314224 Color: 1
Size: 265503 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 385329 Color: 1
Size: 311160 Color: 1
Size: 303512 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 435636 Color: 1
Size: 295597 Color: 1
Size: 268768 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 459055 Color: 1
Size: 285495 Color: 1
Size: 255451 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 420391 Color: 1
Size: 325668 Color: 1
Size: 253942 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 416053 Color: 1
Size: 308114 Color: 1
Size: 275834 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 487941 Color: 1
Size: 256764 Color: 1
Size: 255296 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 432593 Color: 1
Size: 310033 Color: 1
Size: 257375 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 468490 Color: 1
Size: 267309 Color: 1
Size: 264202 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 493583 Color: 1
Size: 253268 Color: 1
Size: 253150 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 447337 Color: 1
Size: 289011 Color: 1
Size: 263653 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 405529 Color: 1
Size: 331805 Color: 1
Size: 262667 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 486771 Color: 1
Size: 262361 Color: 1
Size: 250869 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 439218 Color: 1
Size: 304130 Color: 1
Size: 256653 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 483960 Color: 1
Size: 262419 Color: 1
Size: 253622 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 471227 Color: 1
Size: 277302 Color: 1
Size: 251472 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 389834 Color: 1
Size: 335633 Color: 1
Size: 274534 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 483232 Color: 1
Size: 265932 Color: 1
Size: 250837 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 393974 Color: 1
Size: 312366 Color: 1
Size: 293661 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 477203 Color: 1
Size: 264737 Color: 1
Size: 258061 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 479533 Color: 1
Size: 265195 Color: 1
Size: 255273 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 374650 Color: 1
Size: 348937 Color: 1
Size: 276414 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 387386 Color: 1
Size: 352567 Color: 1
Size: 260048 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 376150 Color: 1
Size: 365414 Color: 1
Size: 258437 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 388679 Color: 1
Size: 333429 Color: 1
Size: 277893 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 373707 Color: 1
Size: 344278 Color: 1
Size: 282016 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 397292 Color: 1
Size: 312849 Color: 1
Size: 289860 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 378048 Color: 1
Size: 359872 Color: 1
Size: 262081 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 453824 Color: 1
Size: 287909 Color: 1
Size: 258268 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 381925 Color: 1
Size: 340311 Color: 1
Size: 277765 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 377489 Color: 1
Size: 314486 Color: 0
Size: 308026 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 374406 Color: 1
Size: 329452 Color: 1
Size: 296143 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 463518 Color: 1
Size: 271208 Color: 1
Size: 265275 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 494424 Color: 1
Size: 253206 Color: 1
Size: 252371 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 443512 Color: 1
Size: 298976 Color: 1
Size: 257513 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 375080 Color: 1
Size: 353872 Color: 1
Size: 271049 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 449134 Color: 1
Size: 297820 Color: 1
Size: 253047 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 350222 Color: 1
Size: 333453 Color: 1
Size: 316326 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 493351 Color: 1
Size: 255098 Color: 1
Size: 251552 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 440751 Color: 1
Size: 303091 Color: 1
Size: 256159 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 378353 Color: 1
Size: 325937 Color: 1
Size: 295711 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 427713 Color: 1
Size: 287788 Color: 0
Size: 284500 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 410419 Color: 1
Size: 298987 Color: 1
Size: 290595 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 487344 Color: 1
Size: 256332 Color: 1
Size: 256325 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 354662 Color: 1
Size: 338377 Color: 1
Size: 306962 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 406981 Color: 1
Size: 324996 Color: 1
Size: 268024 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 421988 Color: 1
Size: 293183 Color: 1
Size: 284830 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 359904 Color: 1
Size: 343796 Color: 1
Size: 296301 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 424322 Color: 1
Size: 323868 Color: 1
Size: 251811 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 444399 Color: 1
Size: 280455 Color: 0
Size: 275147 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 478132 Color: 1
Size: 270731 Color: 1
Size: 251138 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 342783 Color: 1
Size: 331168 Color: 1
Size: 326050 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 455722 Color: 1
Size: 279444 Color: 0
Size: 264835 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 446537 Color: 1
Size: 286862 Color: 1
Size: 266602 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 462194 Color: 1
Size: 283446 Color: 1
Size: 254361 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 377925 Color: 1
Size: 348868 Color: 1
Size: 273208 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 423984 Color: 1
Size: 304940 Color: 1
Size: 271077 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 407576 Color: 1
Size: 333170 Color: 1
Size: 259255 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 489332 Color: 1
Size: 257071 Color: 1
Size: 253598 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 408520 Color: 1
Size: 316686 Color: 1
Size: 274795 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 364039 Color: 1
Size: 344669 Color: 1
Size: 291293 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 472313 Color: 1
Size: 270260 Color: 1
Size: 257428 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 401034 Color: 1
Size: 323503 Color: 1
Size: 275464 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 461388 Color: 1
Size: 277073 Color: 1
Size: 261540 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 446947 Color: 1
Size: 284267 Color: 1
Size: 268787 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 384038 Color: 1
Size: 359995 Color: 1
Size: 255968 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 408199 Color: 1
Size: 316029 Color: 1
Size: 275773 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 413334 Color: 1
Size: 323431 Color: 1
Size: 263236 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 467219 Color: 1
Size: 276756 Color: 1
Size: 256026 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 386435 Color: 1
Size: 328083 Color: 0
Size: 285483 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 419136 Color: 1
Size: 318916 Color: 1
Size: 261949 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 413313 Color: 1
Size: 315909 Color: 1
Size: 270779 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 465998 Color: 1
Size: 273954 Color: 1
Size: 260049 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 471947 Color: 1
Size: 264530 Color: 1
Size: 263524 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 452763 Color: 1
Size: 290000 Color: 1
Size: 257238 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 429172 Color: 1
Size: 293271 Color: 0
Size: 277558 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 419425 Color: 1
Size: 307490 Color: 1
Size: 273086 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 437147 Color: 1
Size: 288004 Color: 0
Size: 274850 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 407534 Color: 1
Size: 310135 Color: 1
Size: 282332 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 436091 Color: 1
Size: 290056 Color: 1
Size: 273854 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 411310 Color: 1
Size: 313205 Color: 1
Size: 275486 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 458632 Color: 1
Size: 282385 Color: 1
Size: 258984 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 418818 Color: 1
Size: 311999 Color: 1
Size: 269184 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 379576 Color: 1
Size: 341660 Color: 1
Size: 278765 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 360102 Color: 1
Size: 336954 Color: 1
Size: 302945 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 426993 Color: 1
Size: 305261 Color: 1
Size: 267747 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 446056 Color: 1
Size: 295373 Color: 1
Size: 258572 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 408198 Color: 1
Size: 315479 Color: 1
Size: 276324 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 442578 Color: 1
Size: 279066 Color: 1
Size: 278357 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 371406 Color: 1
Size: 344243 Color: 1
Size: 284352 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 387804 Color: 1
Size: 315561 Color: 1
Size: 296636 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 420439 Color: 1
Size: 299436 Color: 1
Size: 280126 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 395322 Color: 1
Size: 343641 Color: 1
Size: 261038 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 375437 Color: 1
Size: 325867 Color: 0
Size: 298697 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 498529 Color: 1
Size: 250871 Color: 1
Size: 250601 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 411325 Color: 1
Size: 335744 Color: 1
Size: 252932 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 403216 Color: 1
Size: 311975 Color: 1
Size: 284810 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 410181 Color: 1
Size: 318034 Color: 1
Size: 271786 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 400728 Color: 1
Size: 331067 Color: 1
Size: 268206 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 367084 Color: 1
Size: 357846 Color: 1
Size: 275071 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 370670 Color: 1
Size: 368542 Color: 1
Size: 260789 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 438406 Color: 1
Size: 283472 Color: 1
Size: 278123 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 352023 Color: 1
Size: 340928 Color: 1
Size: 307050 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 454769 Color: 1
Size: 290994 Color: 1
Size: 254238 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 428939 Color: 1
Size: 295536 Color: 1
Size: 275526 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 441362 Color: 1
Size: 307807 Color: 1
Size: 250832 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 364517 Color: 1
Size: 337381 Color: 1
Size: 298103 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 426585 Color: 1
Size: 301917 Color: 1
Size: 271499 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 369646 Color: 1
Size: 343882 Color: 1
Size: 286473 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 474003 Color: 1
Size: 269707 Color: 1
Size: 256291 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 388485 Color: 1
Size: 345941 Color: 1
Size: 265575 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 363600 Color: 1
Size: 333914 Color: 1
Size: 302487 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 359734 Color: 1
Size: 358595 Color: 1
Size: 281672 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 490818 Color: 1
Size: 255322 Color: 1
Size: 253861 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 431080 Color: 1
Size: 286823 Color: 0
Size: 282098 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 469624 Color: 1
Size: 268825 Color: 1
Size: 261552 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 377354 Color: 1
Size: 366728 Color: 1
Size: 255919 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 445023 Color: 1
Size: 296343 Color: 1
Size: 258635 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 407670 Color: 1
Size: 321761 Color: 1
Size: 270570 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 380280 Color: 1
Size: 345286 Color: 1
Size: 274435 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 492441 Color: 1
Size: 256701 Color: 1
Size: 250859 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 375235 Color: 1
Size: 336659 Color: 1
Size: 288107 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 398366 Color: 1
Size: 325170 Color: 1
Size: 276465 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 452199 Color: 1
Size: 279260 Color: 1
Size: 268542 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 411517 Color: 1
Size: 326265 Color: 1
Size: 262219 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 402710 Color: 1
Size: 338969 Color: 1
Size: 258322 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 403712 Color: 1
Size: 336927 Color: 1
Size: 259362 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 458935 Color: 1
Size: 279835 Color: 1
Size: 261231 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 477748 Color: 1
Size: 270045 Color: 1
Size: 252208 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 381985 Color: 1
Size: 340342 Color: 1
Size: 277674 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 365682 Color: 1
Size: 356033 Color: 1
Size: 278286 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 475852 Color: 1
Size: 268223 Color: 1
Size: 255926 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 383402 Color: 1
Size: 312334 Color: 1
Size: 304265 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 349466 Color: 1
Size: 348279 Color: 1
Size: 302256 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 485991 Color: 1
Size: 259547 Color: 1
Size: 254463 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 421947 Color: 1
Size: 298503 Color: 1
Size: 279551 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 361668 Color: 1
Size: 338334 Color: 1
Size: 299999 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 344698 Color: 1
Size: 329291 Color: 0
Size: 326012 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 443685 Color: 1
Size: 285684 Color: 1
Size: 270632 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 496929 Color: 1
Size: 252095 Color: 1
Size: 250977 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 403845 Color: 1
Size: 303616 Color: 1
Size: 292540 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 408365 Color: 1
Size: 339818 Color: 1
Size: 251818 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 364777 Color: 1
Size: 345159 Color: 1
Size: 290065 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 372307 Color: 1
Size: 358538 Color: 1
Size: 269156 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 383750 Color: 1
Size: 353548 Color: 1
Size: 262703 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 426076 Color: 1
Size: 305243 Color: 1
Size: 268682 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 446788 Color: 1
Size: 278355 Color: 1
Size: 274858 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 470522 Color: 1
Size: 275867 Color: 1
Size: 253612 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 455130 Color: 1
Size: 281905 Color: 1
Size: 262966 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 453750 Color: 1
Size: 285479 Color: 1
Size: 260772 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 395683 Color: 1
Size: 311561 Color: 1
Size: 292757 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 407302 Color: 1
Size: 337358 Color: 1
Size: 255341 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 435815 Color: 1
Size: 309107 Color: 1
Size: 255079 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 347486 Color: 1
Size: 342951 Color: 1
Size: 309564 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 452309 Color: 1
Size: 290857 Color: 1
Size: 256835 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 362567 Color: 1
Size: 324510 Color: 1
Size: 312924 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 472868 Color: 1
Size: 275671 Color: 1
Size: 251462 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 487135 Color: 1
Size: 259578 Color: 1
Size: 253288 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 488523 Color: 1
Size: 260955 Color: 1
Size: 250523 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 498721 Color: 1
Size: 250665 Color: 1
Size: 250615 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 487439 Color: 1
Size: 261449 Color: 1
Size: 251113 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 477215 Color: 1
Size: 265145 Color: 1
Size: 257641 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 498798 Color: 1
Size: 251079 Color: 1
Size: 250124 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 379026 Color: 1
Size: 315045 Color: 1
Size: 305930 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 456886 Color: 1
Size: 282005 Color: 1
Size: 261110 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 450153 Color: 1
Size: 282703 Color: 1
Size: 267145 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 443352 Color: 1
Size: 284730 Color: 1
Size: 271919 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 403489 Color: 1
Size: 321472 Color: 1
Size: 275040 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 460076 Color: 1
Size: 279255 Color: 1
Size: 260670 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 378950 Color: 1
Size: 356179 Color: 1
Size: 264872 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 386007 Color: 1
Size: 309168 Color: 1
Size: 304826 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 394752 Color: 1
Size: 304870 Color: 1
Size: 300379 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 356552 Color: 1
Size: 329353 Color: 0
Size: 314096 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 488382 Color: 1
Size: 260557 Color: 1
Size: 251062 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 431662 Color: 1
Size: 299413 Color: 1
Size: 268926 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 404970 Color: 1
Size: 329612 Color: 1
Size: 265419 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 357279 Color: 1
Size: 331062 Color: 1
Size: 311660 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 410630 Color: 1
Size: 318222 Color: 1
Size: 271149 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 386697 Color: 1
Size: 323314 Color: 1
Size: 289990 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 425323 Color: 1
Size: 315374 Color: 1
Size: 259304 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 493272 Color: 1
Size: 253475 Color: 1
Size: 253254 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 450717 Color: 1
Size: 295205 Color: 1
Size: 254079 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 458602 Color: 1
Size: 279168 Color: 1
Size: 262231 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 353456 Color: 1
Size: 330496 Color: 1
Size: 316049 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 463195 Color: 1
Size: 273772 Color: 1
Size: 263034 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 383721 Color: 1
Size: 361518 Color: 1
Size: 254762 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 382481 Color: 1
Size: 365457 Color: 1
Size: 252063 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 461030 Color: 1
Size: 269831 Color: 0
Size: 269140 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 365172 Color: 1
Size: 363075 Color: 1
Size: 271754 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 382394 Color: 1
Size: 335721 Color: 1
Size: 281886 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 385803 Color: 1
Size: 322650 Color: 1
Size: 291548 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 458731 Color: 1
Size: 284410 Color: 1
Size: 256860 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 393904 Color: 1
Size: 334508 Color: 1
Size: 271589 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 374289 Color: 1
Size: 332278 Color: 1
Size: 293434 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 404980 Color: 1
Size: 299354 Color: 1
Size: 295667 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 456283 Color: 1
Size: 284844 Color: 1
Size: 258874 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 360469 Color: 1
Size: 360065 Color: 1
Size: 279467 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 349748 Color: 1
Size: 327725 Color: 1
Size: 322528 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 491238 Color: 1
Size: 257780 Color: 1
Size: 250983 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 461791 Color: 1
Size: 271896 Color: 1
Size: 266314 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 353988 Color: 1
Size: 344011 Color: 1
Size: 302002 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 434349 Color: 1
Size: 302712 Color: 1
Size: 262940 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 384577 Color: 1
Size: 350848 Color: 1
Size: 264576 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 434712 Color: 1
Size: 305989 Color: 1
Size: 259300 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 390289 Color: 1
Size: 335293 Color: 1
Size: 274419 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 458355 Color: 1
Size: 290245 Color: 1
Size: 251401 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 395953 Color: 1
Size: 304516 Color: 1
Size: 299532 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 457465 Color: 1
Size: 281697 Color: 1
Size: 260839 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 384945 Color: 1
Size: 315404 Color: 1
Size: 299652 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 377233 Color: 1
Size: 332499 Color: 1
Size: 290269 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 369003 Color: 1
Size: 324978 Color: 0
Size: 306020 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 359901 Color: 1
Size: 329845 Color: 1
Size: 310255 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 451683 Color: 1
Size: 279540 Color: 1
Size: 268778 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 468561 Color: 1
Size: 273361 Color: 1
Size: 258079 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 401926 Color: 1
Size: 305630 Color: 1
Size: 292445 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 384088 Color: 1
Size: 338709 Color: 1
Size: 277204 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 386742 Color: 1
Size: 331282 Color: 1
Size: 281977 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 376467 Color: 1
Size: 352544 Color: 1
Size: 270990 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 368160 Color: 1
Size: 324554 Color: 1
Size: 307287 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 392047 Color: 1
Size: 307658 Color: 1
Size: 300296 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 456298 Color: 1
Size: 274039 Color: 1
Size: 269664 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 418058 Color: 1
Size: 308092 Color: 1
Size: 273851 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 424834 Color: 1
Size: 307250 Color: 1
Size: 267917 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 402693 Color: 1
Size: 339015 Color: 1
Size: 258293 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 489930 Color: 1
Size: 259873 Color: 1
Size: 250198 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 480820 Color: 1
Size: 267135 Color: 1
Size: 252046 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 463259 Color: 1
Size: 276605 Color: 1
Size: 260137 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 424415 Color: 1
Size: 308033 Color: 0
Size: 267553 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 396455 Color: 1
Size: 323358 Color: 1
Size: 280188 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 374132 Color: 1
Size: 366811 Color: 1
Size: 259058 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 345895 Color: 1
Size: 328020 Color: 1
Size: 326086 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 440862 Color: 1
Size: 289455 Color: 1
Size: 269684 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 368244 Color: 1
Size: 333596 Color: 1
Size: 298161 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 492864 Color: 1
Size: 256626 Color: 1
Size: 250511 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 446865 Color: 1
Size: 296635 Color: 1
Size: 256501 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 460803 Color: 1
Size: 284564 Color: 1
Size: 254634 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 468524 Color: 1
Size: 276551 Color: 1
Size: 254926 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 384790 Color: 1
Size: 364309 Color: 1
Size: 250902 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 395248 Color: 1
Size: 310605 Color: 1
Size: 294148 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 360963 Color: 1
Size: 349333 Color: 1
Size: 289705 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 477933 Color: 1
Size: 261736 Color: 1
Size: 260332 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 378866 Color: 1
Size: 358333 Color: 1
Size: 262802 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 354198 Color: 1
Size: 348379 Color: 1
Size: 297424 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 405743 Color: 1
Size: 336860 Color: 1
Size: 257398 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 410738 Color: 1
Size: 333377 Color: 1
Size: 255886 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 469012 Color: 1
Size: 267840 Color: 1
Size: 263149 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 361496 Color: 1
Size: 338530 Color: 1
Size: 299975 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 373717 Color: 1
Size: 365258 Color: 1
Size: 261026 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 398769 Color: 1
Size: 319794 Color: 1
Size: 281438 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 354276 Color: 1
Size: 325861 Color: 1
Size: 319864 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 414069 Color: 1
Size: 333150 Color: 1
Size: 252782 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 481319 Color: 1
Size: 261297 Color: 1
Size: 257385 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 471209 Color: 1
Size: 273418 Color: 1
Size: 255374 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 372605 Color: 1
Size: 314119 Color: 1
Size: 313277 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 479531 Color: 1
Size: 264867 Color: 1
Size: 255603 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 432270 Color: 1
Size: 306383 Color: 1
Size: 261348 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 361955 Color: 1
Size: 337726 Color: 1
Size: 300320 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 376602 Color: 1
Size: 323285 Color: 0
Size: 300114 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 372134 Color: 1
Size: 318071 Color: 1
Size: 309796 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 385906 Color: 1
Size: 351485 Color: 1
Size: 262610 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 428034 Color: 1
Size: 312528 Color: 1
Size: 259439 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 413096 Color: 1
Size: 327699 Color: 1
Size: 259206 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 398660 Color: 1
Size: 310885 Color: 1
Size: 290456 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 497832 Color: 1
Size: 251334 Color: 1
Size: 250835 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 402225 Color: 1
Size: 323318 Color: 1
Size: 274458 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 439431 Color: 1
Size: 287849 Color: 0
Size: 272720 Color: 1

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 429790 Color: 1
Size: 298869 Color: 1
Size: 271341 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 437348 Color: 1
Size: 283384 Color: 0
Size: 279268 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 363495 Color: 1
Size: 326248 Color: 1
Size: 310257 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 397017 Color: 1
Size: 352738 Color: 1
Size: 250245 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 442351 Color: 1
Size: 283113 Color: 1
Size: 274536 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 355348 Color: 1
Size: 347970 Color: 1
Size: 296682 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 354875 Color: 1
Size: 348031 Color: 1
Size: 297094 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 412969 Color: 1
Size: 309103 Color: 1
Size: 277928 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 437320 Color: 1
Size: 298810 Color: 1
Size: 263870 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 452747 Color: 1
Size: 284635 Color: 1
Size: 262618 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 362659 Color: 1
Size: 354644 Color: 1
Size: 282697 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 404860 Color: 1
Size: 324240 Color: 1
Size: 270900 Color: 0

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 423615 Color: 1
Size: 315862 Color: 1
Size: 260523 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 377070 Color: 1
Size: 354247 Color: 1
Size: 268683 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 382882 Color: 1
Size: 333688 Color: 1
Size: 283430 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 445434 Color: 1
Size: 294433 Color: 1
Size: 260133 Color: 0

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 352608 Color: 1
Size: 349855 Color: 1
Size: 297537 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 357636 Color: 1
Size: 333825 Color: 1
Size: 308539 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 374314 Color: 1
Size: 341485 Color: 1
Size: 284201 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 351641 Color: 1
Size: 330184 Color: 1
Size: 318175 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 367827 Color: 1
Size: 351822 Color: 1
Size: 280351 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 401901 Color: 1
Size: 331709 Color: 1
Size: 266390 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 388671 Color: 1
Size: 321009 Color: 1
Size: 290320 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 374340 Color: 1
Size: 343122 Color: 1
Size: 282538 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 408970 Color: 1
Size: 299784 Color: 1
Size: 291246 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 408776 Color: 1
Size: 315315 Color: 1
Size: 275909 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 364475 Color: 1
Size: 340212 Color: 1
Size: 295313 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 366130 Color: 1
Size: 366033 Color: 1
Size: 267837 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 395234 Color: 1
Size: 321874 Color: 1
Size: 282892 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 394026 Color: 1
Size: 311644 Color: 1
Size: 294330 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 428449 Color: 1
Size: 311470 Color: 1
Size: 260081 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 362250 Color: 1
Size: 337877 Color: 1
Size: 299873 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 364319 Color: 1
Size: 343556 Color: 1
Size: 292125 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 479869 Color: 1
Size: 265862 Color: 0
Size: 254269 Color: 1

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 464196 Color: 1
Size: 281331 Color: 1
Size: 254473 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 400304 Color: 1
Size: 343082 Color: 1
Size: 256614 Color: 0

Bin 438: 2 of cap free
Amount of items: 3
Items: 
Size: 373098 Color: 1
Size: 362031 Color: 1
Size: 264870 Color: 0

Bin 439: 2 of cap free
Amount of items: 3
Items: 
Size: 445043 Color: 1
Size: 279546 Color: 1
Size: 275410 Color: 0

Bin 440: 2 of cap free
Amount of items: 3
Items: 
Size: 406739 Color: 1
Size: 308378 Color: 1
Size: 284882 Color: 0

Bin 441: 2 of cap free
Amount of items: 3
Items: 
Size: 377015 Color: 1
Size: 318088 Color: 0
Size: 304896 Color: 1

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 426172 Color: 1
Size: 310453 Color: 1
Size: 263374 Color: 0

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 411377 Color: 1
Size: 335094 Color: 1
Size: 253528 Color: 0

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 449749 Color: 1
Size: 289970 Color: 1
Size: 260280 Color: 0

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 412825 Color: 1
Size: 328093 Color: 1
Size: 259081 Color: 0

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 427514 Color: 1
Size: 287757 Color: 1
Size: 284728 Color: 0

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 365515 Color: 1
Size: 342615 Color: 1
Size: 291869 Color: 0

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 391921 Color: 1
Size: 357367 Color: 1
Size: 250711 Color: 0

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 413480 Color: 1
Size: 294977 Color: 1
Size: 291542 Color: 0

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 399912 Color: 1
Size: 319350 Color: 1
Size: 280737 Color: 0

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 473949 Color: 1
Size: 272072 Color: 1
Size: 253978 Color: 0

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 393114 Color: 1
Size: 336591 Color: 1
Size: 270294 Color: 0

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 390429 Color: 1
Size: 358054 Color: 1
Size: 251516 Color: 0

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 354684 Color: 1
Size: 349800 Color: 1
Size: 295515 Color: 0

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 360449 Color: 1
Size: 340046 Color: 1
Size: 299504 Color: 0

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 375696 Color: 1
Size: 354748 Color: 1
Size: 269555 Color: 0

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 396635 Color: 1
Size: 318061 Color: 1
Size: 285303 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 449321 Color: 1
Size: 299071 Color: 1
Size: 251607 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 370163 Color: 1
Size: 343396 Color: 1
Size: 286440 Color: 0

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 418457 Color: 1
Size: 326992 Color: 1
Size: 254550 Color: 0

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 422040 Color: 1
Size: 321482 Color: 1
Size: 256477 Color: 0

Bin 462: 3 of cap free
Amount of items: 3
Items: 
Size: 422702 Color: 1
Size: 307145 Color: 1
Size: 270151 Color: 0

Bin 463: 3 of cap free
Amount of items: 3
Items: 
Size: 383451 Color: 1
Size: 316893 Color: 1
Size: 299654 Color: 0

Bin 464: 3 of cap free
Amount of items: 3
Items: 
Size: 417964 Color: 1
Size: 312899 Color: 1
Size: 269135 Color: 0

Bin 465: 3 of cap free
Amount of items: 3
Items: 
Size: 380500 Color: 1
Size: 365777 Color: 1
Size: 253721 Color: 0

Bin 466: 3 of cap free
Amount of items: 3
Items: 
Size: 365310 Color: 1
Size: 355943 Color: 1
Size: 278745 Color: 0

Bin 467: 3 of cap free
Amount of items: 3
Items: 
Size: 386276 Color: 1
Size: 309742 Color: 1
Size: 303980 Color: 0

Bin 468: 3 of cap free
Amount of items: 3
Items: 
Size: 389281 Color: 1
Size: 338313 Color: 1
Size: 272404 Color: 0

Bin 469: 3 of cap free
Amount of items: 3
Items: 
Size: 360466 Color: 1
Size: 355911 Color: 1
Size: 283621 Color: 0

Bin 470: 3 of cap free
Amount of items: 3
Items: 
Size: 387579 Color: 1
Size: 335336 Color: 1
Size: 277083 Color: 0

Bin 471: 3 of cap free
Amount of items: 3
Items: 
Size: 403007 Color: 1
Size: 339246 Color: 1
Size: 257745 Color: 0

Bin 472: 3 of cap free
Amount of items: 3
Items: 
Size: 440999 Color: 1
Size: 298137 Color: 1
Size: 260862 Color: 0

Bin 473: 3 of cap free
Amount of items: 3
Items: 
Size: 369772 Color: 1
Size: 338748 Color: 1
Size: 291478 Color: 0

Bin 474: 3 of cap free
Amount of items: 3
Items: 
Size: 373385 Color: 1
Size: 324763 Color: 0
Size: 301850 Color: 1

Bin 475: 3 of cap free
Amount of items: 3
Items: 
Size: 377677 Color: 1
Size: 340218 Color: 1
Size: 282103 Color: 0

Bin 476: 4 of cap free
Amount of items: 3
Items: 
Size: 385956 Color: 1
Size: 310228 Color: 1
Size: 303813 Color: 0

Bin 477: 4 of cap free
Amount of items: 3
Items: 
Size: 419164 Color: 1
Size: 325260 Color: 1
Size: 255573 Color: 0

Bin 478: 4 of cap free
Amount of items: 3
Items: 
Size: 389464 Color: 1
Size: 350069 Color: 1
Size: 260464 Color: 0

Bin 479: 4 of cap free
Amount of items: 3
Items: 
Size: 399027 Color: 1
Size: 311589 Color: 1
Size: 289381 Color: 0

Bin 480: 4 of cap free
Amount of items: 3
Items: 
Size: 384971 Color: 1
Size: 323283 Color: 1
Size: 291743 Color: 0

Bin 481: 4 of cap free
Amount of items: 3
Items: 
Size: 424308 Color: 1
Size: 291936 Color: 1
Size: 283753 Color: 0

Bin 482: 4 of cap free
Amount of items: 3
Items: 
Size: 416152 Color: 1
Size: 299223 Color: 1
Size: 284622 Color: 0

Bin 483: 4 of cap free
Amount of items: 3
Items: 
Size: 409285 Color: 1
Size: 340644 Color: 1
Size: 250068 Color: 0

Bin 484: 4 of cap free
Amount of items: 3
Items: 
Size: 373227 Color: 1
Size: 337721 Color: 1
Size: 289049 Color: 0

Bin 485: 4 of cap free
Amount of items: 3
Items: 
Size: 412737 Color: 1
Size: 303129 Color: 1
Size: 284131 Color: 0

Bin 486: 4 of cap free
Amount of items: 3
Items: 
Size: 362358 Color: 1
Size: 336455 Color: 1
Size: 301184 Color: 0

Bin 487: 4 of cap free
Amount of items: 3
Items: 
Size: 406901 Color: 1
Size: 333456 Color: 1
Size: 259640 Color: 0

Bin 488: 5 of cap free
Amount of items: 3
Items: 
Size: 402889 Color: 1
Size: 328967 Color: 1
Size: 268140 Color: 0

Bin 489: 5 of cap free
Amount of items: 3
Items: 
Size: 419599 Color: 1
Size: 308872 Color: 1
Size: 271525 Color: 0

Bin 490: 5 of cap free
Amount of items: 3
Items: 
Size: 428393 Color: 1
Size: 304219 Color: 1
Size: 267384 Color: 0

Bin 491: 5 of cap free
Amount of items: 3
Items: 
Size: 348471 Color: 1
Size: 333603 Color: 1
Size: 317922 Color: 0

Bin 492: 5 of cap free
Amount of items: 3
Items: 
Size: 406777 Color: 1
Size: 338695 Color: 1
Size: 254524 Color: 0

Bin 493: 5 of cap free
Amount of items: 3
Items: 
Size: 472780 Color: 1
Size: 276715 Color: 1
Size: 250501 Color: 0

Bin 494: 5 of cap free
Amount of items: 3
Items: 
Size: 451633 Color: 1
Size: 284742 Color: 1
Size: 263621 Color: 0

Bin 495: 5 of cap free
Amount of items: 3
Items: 
Size: 372665 Color: 1
Size: 370185 Color: 1
Size: 257146 Color: 0

Bin 496: 5 of cap free
Amount of items: 3
Items: 
Size: 450586 Color: 1
Size: 292307 Color: 1
Size: 257103 Color: 0

Bin 497: 6 of cap free
Amount of items: 3
Items: 
Size: 407890 Color: 1
Size: 329332 Color: 1
Size: 262773 Color: 0

Bin 498: 6 of cap free
Amount of items: 3
Items: 
Size: 446800 Color: 1
Size: 297473 Color: 1
Size: 255722 Color: 0

Bin 499: 6 of cap free
Amount of items: 3
Items: 
Size: 362783 Color: 1
Size: 349088 Color: 1
Size: 288124 Color: 0

Bin 500: 6 of cap free
Amount of items: 3
Items: 
Size: 452418 Color: 1
Size: 289766 Color: 1
Size: 257811 Color: 0

Bin 501: 6 of cap free
Amount of items: 3
Items: 
Size: 369473 Color: 1
Size: 364792 Color: 1
Size: 265730 Color: 0

Bin 502: 6 of cap free
Amount of items: 3
Items: 
Size: 396127 Color: 1
Size: 349560 Color: 1
Size: 254308 Color: 0

Bin 503: 6 of cap free
Amount of items: 3
Items: 
Size: 434620 Color: 1
Size: 303040 Color: 1
Size: 262335 Color: 0

Bin 504: 6 of cap free
Amount of items: 3
Items: 
Size: 398954 Color: 1
Size: 349833 Color: 1
Size: 251208 Color: 0

Bin 505: 6 of cap free
Amount of items: 3
Items: 
Size: 437126 Color: 1
Size: 303556 Color: 1
Size: 259313 Color: 0

Bin 506: 6 of cap free
Amount of items: 3
Items: 
Size: 446827 Color: 1
Size: 295698 Color: 1
Size: 257470 Color: 0

Bin 507: 6 of cap free
Amount of items: 3
Items: 
Size: 387113 Color: 1
Size: 357265 Color: 1
Size: 255617 Color: 0

Bin 508: 6 of cap free
Amount of items: 3
Items: 
Size: 415598 Color: 1
Size: 331889 Color: 1
Size: 252508 Color: 0

Bin 509: 6 of cap free
Amount of items: 3
Items: 
Size: 386828 Color: 1
Size: 322552 Color: 1
Size: 290615 Color: 0

Bin 510: 6 of cap free
Amount of items: 3
Items: 
Size: 368666 Color: 1
Size: 319301 Color: 1
Size: 312028 Color: 0

Bin 511: 6 of cap free
Amount of items: 3
Items: 
Size: 451311 Color: 1
Size: 287614 Color: 1
Size: 261070 Color: 0

Bin 512: 7 of cap free
Amount of items: 3
Items: 
Size: 428806 Color: 1
Size: 286206 Color: 1
Size: 284982 Color: 0

Bin 513: 7 of cap free
Amount of items: 3
Items: 
Size: 440466 Color: 1
Size: 308482 Color: 1
Size: 251046 Color: 0

Bin 514: 7 of cap free
Amount of items: 3
Items: 
Size: 391437 Color: 1
Size: 346900 Color: 1
Size: 261657 Color: 0

Bin 515: 7 of cap free
Amount of items: 3
Items: 
Size: 430042 Color: 1
Size: 302299 Color: 1
Size: 267653 Color: 0

Bin 516: 7 of cap free
Amount of items: 3
Items: 
Size: 406716 Color: 1
Size: 302850 Color: 0
Size: 290428 Color: 1

Bin 517: 7 of cap free
Amount of items: 3
Items: 
Size: 368677 Color: 1
Size: 352729 Color: 1
Size: 278588 Color: 0

Bin 518: 7 of cap free
Amount of items: 3
Items: 
Size: 431880 Color: 1
Size: 309232 Color: 1
Size: 258882 Color: 0

Bin 519: 8 of cap free
Amount of items: 3
Items: 
Size: 363527 Color: 1
Size: 331722 Color: 1
Size: 304744 Color: 0

Bin 520: 8 of cap free
Amount of items: 3
Items: 
Size: 392906 Color: 1
Size: 314100 Color: 1
Size: 292987 Color: 0

Bin 521: 8 of cap free
Amount of items: 3
Items: 
Size: 371874 Color: 1
Size: 348068 Color: 1
Size: 280051 Color: 0

Bin 522: 8 of cap free
Amount of items: 3
Items: 
Size: 425660 Color: 1
Size: 310492 Color: 1
Size: 263841 Color: 0

Bin 523: 8 of cap free
Amount of items: 3
Items: 
Size: 357862 Color: 1
Size: 339094 Color: 1
Size: 303037 Color: 0

Bin 524: 8 of cap free
Amount of items: 3
Items: 
Size: 387950 Color: 1
Size: 324510 Color: 1
Size: 287533 Color: 0

Bin 525: 8 of cap free
Amount of items: 3
Items: 
Size: 467111 Color: 1
Size: 279291 Color: 1
Size: 253591 Color: 0

Bin 526: 9 of cap free
Amount of items: 3
Items: 
Size: 432167 Color: 1
Size: 307487 Color: 1
Size: 260338 Color: 0

Bin 527: 9 of cap free
Amount of items: 3
Items: 
Size: 389996 Color: 1
Size: 342278 Color: 1
Size: 267718 Color: 0

Bin 528: 9 of cap free
Amount of items: 3
Items: 
Size: 391498 Color: 1
Size: 337845 Color: 1
Size: 270649 Color: 0

Bin 529: 9 of cap free
Amount of items: 3
Items: 
Size: 392434 Color: 1
Size: 304676 Color: 0
Size: 302882 Color: 1

Bin 530: 9 of cap free
Amount of items: 3
Items: 
Size: 408985 Color: 1
Size: 324661 Color: 1
Size: 266346 Color: 0

Bin 531: 9 of cap free
Amount of items: 3
Items: 
Size: 360615 Color: 1
Size: 332945 Color: 1
Size: 306432 Color: 0

Bin 532: 9 of cap free
Amount of items: 3
Items: 
Size: 369232 Color: 1
Size: 352628 Color: 1
Size: 278132 Color: 0

Bin 533: 9 of cap free
Amount of items: 3
Items: 
Size: 347249 Color: 1
Size: 336734 Color: 1
Size: 316009 Color: 0

Bin 534: 9 of cap free
Amount of items: 3
Items: 
Size: 403631 Color: 1
Size: 324722 Color: 1
Size: 271639 Color: 0

Bin 535: 10 of cap free
Amount of items: 3
Items: 
Size: 348543 Color: 1
Size: 337866 Color: 1
Size: 313582 Color: 0

Bin 536: 10 of cap free
Amount of items: 3
Items: 
Size: 399631 Color: 1
Size: 310981 Color: 1
Size: 289379 Color: 0

Bin 537: 10 of cap free
Amount of items: 3
Items: 
Size: 371680 Color: 1
Size: 352035 Color: 1
Size: 276276 Color: 0

Bin 538: 10 of cap free
Amount of items: 3
Items: 
Size: 416691 Color: 1
Size: 329045 Color: 1
Size: 254255 Color: 0

Bin 539: 11 of cap free
Amount of items: 3
Items: 
Size: 407164 Color: 1
Size: 315205 Color: 1
Size: 277621 Color: 0

Bin 540: 11 of cap free
Amount of items: 3
Items: 
Size: 346777 Color: 1
Size: 343427 Color: 1
Size: 309786 Color: 0

Bin 541: 11 of cap free
Amount of items: 3
Items: 
Size: 368340 Color: 1
Size: 347396 Color: 1
Size: 284254 Color: 0

Bin 542: 11 of cap free
Amount of items: 3
Items: 
Size: 394828 Color: 1
Size: 323108 Color: 1
Size: 282054 Color: 0

Bin 543: 11 of cap free
Amount of items: 3
Items: 
Size: 390613 Color: 1
Size: 341810 Color: 1
Size: 267567 Color: 0

Bin 544: 11 of cap free
Amount of items: 3
Items: 
Size: 440915 Color: 1
Size: 308450 Color: 1
Size: 250625 Color: 0

Bin 545: 11 of cap free
Amount of items: 3
Items: 
Size: 370352 Color: 1
Size: 359776 Color: 1
Size: 269862 Color: 0

Bin 546: 12 of cap free
Amount of items: 3
Items: 
Size: 386082 Color: 1
Size: 361464 Color: 1
Size: 252443 Color: 0

Bin 547: 12 of cap free
Amount of items: 3
Items: 
Size: 436062 Color: 1
Size: 309007 Color: 1
Size: 254920 Color: 0

Bin 548: 12 of cap free
Amount of items: 3
Items: 
Size: 391162 Color: 1
Size: 327761 Color: 1
Size: 281066 Color: 0

Bin 549: 13 of cap free
Amount of items: 3
Items: 
Size: 394225 Color: 1
Size: 337052 Color: 1
Size: 268711 Color: 0

Bin 550: 13 of cap free
Amount of items: 3
Items: 
Size: 388338 Color: 1
Size: 344449 Color: 1
Size: 267201 Color: 0

Bin 551: 13 of cap free
Amount of items: 3
Items: 
Size: 416203 Color: 1
Size: 310681 Color: 1
Size: 273104 Color: 0

Bin 552: 13 of cap free
Amount of items: 3
Items: 
Size: 397033 Color: 1
Size: 304787 Color: 1
Size: 298168 Color: 0

Bin 553: 13 of cap free
Amount of items: 3
Items: 
Size: 368720 Color: 1
Size: 318727 Color: 1
Size: 312541 Color: 0

Bin 554: 13 of cap free
Amount of items: 3
Items: 
Size: 391687 Color: 1
Size: 351443 Color: 1
Size: 256858 Color: 0

Bin 555: 13 of cap free
Amount of items: 3
Items: 
Size: 360737 Color: 1
Size: 357859 Color: 1
Size: 281392 Color: 0

Bin 556: 14 of cap free
Amount of items: 3
Items: 
Size: 464516 Color: 1
Size: 272000 Color: 1
Size: 263471 Color: 0

Bin 557: 14 of cap free
Amount of items: 3
Items: 
Size: 448750 Color: 1
Size: 300697 Color: 1
Size: 250540 Color: 0

Bin 558: 14 of cap free
Amount of items: 3
Items: 
Size: 426805 Color: 1
Size: 319421 Color: 1
Size: 253761 Color: 0

Bin 559: 14 of cap free
Amount of items: 3
Items: 
Size: 462542 Color: 1
Size: 273473 Color: 1
Size: 263972 Color: 0

Bin 560: 15 of cap free
Amount of items: 3
Items: 
Size: 401564 Color: 1
Size: 332632 Color: 1
Size: 265790 Color: 0

Bin 561: 15 of cap free
Amount of items: 3
Items: 
Size: 456060 Color: 1
Size: 292077 Color: 1
Size: 251849 Color: 0

Bin 562: 15 of cap free
Amount of items: 3
Items: 
Size: 377910 Color: 1
Size: 318748 Color: 1
Size: 303328 Color: 0

Bin 563: 15 of cap free
Amount of items: 3
Items: 
Size: 401585 Color: 1
Size: 346231 Color: 1
Size: 252170 Color: 0

Bin 564: 15 of cap free
Amount of items: 3
Items: 
Size: 382909 Color: 1
Size: 363136 Color: 1
Size: 253941 Color: 0

Bin 565: 15 of cap free
Amount of items: 3
Items: 
Size: 427924 Color: 1
Size: 312227 Color: 1
Size: 259835 Color: 0

Bin 566: 16 of cap free
Amount of items: 3
Items: 
Size: 447837 Color: 1
Size: 297274 Color: 1
Size: 254874 Color: 0

Bin 567: 16 of cap free
Amount of items: 3
Items: 
Size: 373021 Color: 1
Size: 331753 Color: 1
Size: 295211 Color: 0

Bin 568: 16 of cap free
Amount of items: 3
Items: 
Size: 441867 Color: 1
Size: 296961 Color: 1
Size: 261157 Color: 0

Bin 569: 16 of cap free
Amount of items: 3
Items: 
Size: 396451 Color: 1
Size: 328193 Color: 1
Size: 275341 Color: 0

Bin 570: 17 of cap free
Amount of items: 3
Items: 
Size: 411118 Color: 1
Size: 299082 Color: 0
Size: 289784 Color: 1

Bin 571: 17 of cap free
Amount of items: 3
Items: 
Size: 380458 Color: 1
Size: 327801 Color: 1
Size: 291725 Color: 0

Bin 572: 17 of cap free
Amount of items: 3
Items: 
Size: 370532 Color: 1
Size: 318321 Color: 1
Size: 311131 Color: 0

Bin 573: 18 of cap free
Amount of items: 3
Items: 
Size: 371045 Color: 1
Size: 346404 Color: 1
Size: 282534 Color: 0

Bin 574: 18 of cap free
Amount of items: 3
Items: 
Size: 415659 Color: 1
Size: 321653 Color: 1
Size: 262671 Color: 0

Bin 575: 19 of cap free
Amount of items: 3
Items: 
Size: 373113 Color: 1
Size: 320569 Color: 0
Size: 306300 Color: 1

Bin 576: 19 of cap free
Amount of items: 3
Items: 
Size: 380521 Color: 1
Size: 320810 Color: 1
Size: 298651 Color: 0

Bin 577: 19 of cap free
Amount of items: 3
Items: 
Size: 452683 Color: 1
Size: 280298 Color: 1
Size: 267001 Color: 0

Bin 578: 19 of cap free
Amount of items: 3
Items: 
Size: 450430 Color: 1
Size: 290936 Color: 1
Size: 258616 Color: 0

Bin 579: 20 of cap free
Amount of items: 3
Items: 
Size: 359642 Color: 1
Size: 331968 Color: 1
Size: 308371 Color: 0

Bin 580: 21 of cap free
Amount of items: 3
Items: 
Size: 397310 Color: 1
Size: 328843 Color: 1
Size: 273827 Color: 0

Bin 581: 21 of cap free
Amount of items: 3
Items: 
Size: 359470 Color: 1
Size: 354736 Color: 1
Size: 285774 Color: 0

Bin 582: 22 of cap free
Amount of items: 3
Items: 
Size: 412271 Color: 1
Size: 311473 Color: 1
Size: 276235 Color: 0

Bin 583: 22 of cap free
Amount of items: 3
Items: 
Size: 403339 Color: 1
Size: 308227 Color: 0
Size: 288413 Color: 1

Bin 584: 22 of cap free
Amount of items: 3
Items: 
Size: 405335 Color: 1
Size: 320384 Color: 1
Size: 274260 Color: 0

Bin 585: 23 of cap free
Amount of items: 3
Items: 
Size: 391340 Color: 1
Size: 330182 Color: 1
Size: 278456 Color: 0

Bin 586: 23 of cap free
Amount of items: 3
Items: 
Size: 383931 Color: 1
Size: 336915 Color: 1
Size: 279132 Color: 0

Bin 587: 26 of cap free
Amount of items: 3
Items: 
Size: 445103 Color: 1
Size: 302128 Color: 1
Size: 252744 Color: 0

Bin 588: 27 of cap free
Amount of items: 3
Items: 
Size: 410925 Color: 1
Size: 329320 Color: 1
Size: 259729 Color: 0

Bin 589: 27 of cap free
Amount of items: 3
Items: 
Size: 367406 Color: 1
Size: 342391 Color: 1
Size: 290177 Color: 0

Bin 590: 27 of cap free
Amount of items: 3
Items: 
Size: 401678 Color: 1
Size: 322965 Color: 1
Size: 275331 Color: 0

Bin 591: 28 of cap free
Amount of items: 3
Items: 
Size: 420653 Color: 1
Size: 302899 Color: 1
Size: 276421 Color: 0

Bin 592: 29 of cap free
Amount of items: 3
Items: 
Size: 366746 Color: 1
Size: 336805 Color: 1
Size: 296421 Color: 0

Bin 593: 30 of cap free
Amount of items: 3
Items: 
Size: 386033 Color: 1
Size: 317088 Color: 0
Size: 296850 Color: 1

Bin 594: 30 of cap free
Amount of items: 3
Items: 
Size: 430817 Color: 1
Size: 313107 Color: 1
Size: 256047 Color: 0

Bin 595: 30 of cap free
Amount of items: 3
Items: 
Size: 374883 Color: 1
Size: 370422 Color: 1
Size: 254666 Color: 0

Bin 596: 31 of cap free
Amount of items: 3
Items: 
Size: 443961 Color: 1
Size: 283292 Color: 0
Size: 272717 Color: 1

Bin 597: 32 of cap free
Amount of items: 3
Items: 
Size: 351618 Color: 1
Size: 333987 Color: 1
Size: 314364 Color: 0

Bin 598: 32 of cap free
Amount of items: 3
Items: 
Size: 389977 Color: 1
Size: 313618 Color: 1
Size: 296374 Color: 0

Bin 599: 34 of cap free
Amount of items: 3
Items: 
Size: 403142 Color: 1
Size: 343244 Color: 1
Size: 253581 Color: 0

Bin 600: 35 of cap free
Amount of items: 3
Items: 
Size: 404094 Color: 1
Size: 308673 Color: 1
Size: 287199 Color: 0

Bin 601: 35 of cap free
Amount of items: 3
Items: 
Size: 425236 Color: 1
Size: 310305 Color: 1
Size: 264425 Color: 0

Bin 602: 35 of cap free
Amount of items: 3
Items: 
Size: 394779 Color: 1
Size: 316469 Color: 1
Size: 288718 Color: 0

Bin 603: 38 of cap free
Amount of items: 3
Items: 
Size: 386988 Color: 1
Size: 327860 Color: 1
Size: 285115 Color: 0

Bin 604: 38 of cap free
Amount of items: 3
Items: 
Size: 364463 Color: 1
Size: 336518 Color: 1
Size: 298982 Color: 0

Bin 605: 39 of cap free
Amount of items: 3
Items: 
Size: 428631 Color: 1
Size: 311069 Color: 1
Size: 260262 Color: 0

Bin 606: 41 of cap free
Amount of items: 3
Items: 
Size: 369427 Color: 1
Size: 331295 Color: 1
Size: 299238 Color: 0

Bin 607: 43 of cap free
Amount of items: 3
Items: 
Size: 416546 Color: 1
Size: 323534 Color: 1
Size: 259878 Color: 0

Bin 608: 44 of cap free
Amount of items: 3
Items: 
Size: 358593 Color: 1
Size: 349515 Color: 1
Size: 291849 Color: 0

Bin 609: 44 of cap free
Amount of items: 3
Items: 
Size: 374320 Color: 1
Size: 346047 Color: 1
Size: 279590 Color: 0

Bin 610: 44 of cap free
Amount of items: 3
Items: 
Size: 376071 Color: 1
Size: 349264 Color: 1
Size: 274622 Color: 0

Bin 611: 46 of cap free
Amount of items: 3
Items: 
Size: 365400 Color: 1
Size: 356196 Color: 1
Size: 278359 Color: 0

Bin 612: 46 of cap free
Amount of items: 3
Items: 
Size: 453534 Color: 1
Size: 289615 Color: 1
Size: 256806 Color: 0

Bin 613: 47 of cap free
Amount of items: 3
Items: 
Size: 399747 Color: 1
Size: 307589 Color: 1
Size: 292618 Color: 0

Bin 614: 48 of cap free
Amount of items: 3
Items: 
Size: 371384 Color: 1
Size: 348136 Color: 1
Size: 280433 Color: 0

Bin 615: 49 of cap free
Amount of items: 3
Items: 
Size: 448530 Color: 1
Size: 298213 Color: 1
Size: 253209 Color: 0

Bin 616: 51 of cap free
Amount of items: 3
Items: 
Size: 465697 Color: 1
Size: 268869 Color: 1
Size: 265384 Color: 0

Bin 617: 52 of cap free
Amount of items: 3
Items: 
Size: 390304 Color: 1
Size: 341055 Color: 1
Size: 268590 Color: 0

Bin 618: 53 of cap free
Amount of items: 3
Items: 
Size: 452081 Color: 1
Size: 293336 Color: 1
Size: 254531 Color: 0

Bin 619: 53 of cap free
Amount of items: 3
Items: 
Size: 423144 Color: 1
Size: 309955 Color: 1
Size: 266849 Color: 0

Bin 620: 56 of cap free
Amount of items: 3
Items: 
Size: 396800 Color: 1
Size: 325041 Color: 1
Size: 278104 Color: 0

Bin 621: 59 of cap free
Amount of items: 3
Items: 
Size: 360851 Color: 1
Size: 339355 Color: 1
Size: 299736 Color: 0

Bin 622: 65 of cap free
Amount of items: 3
Items: 
Size: 378612 Color: 1
Size: 355445 Color: 1
Size: 265879 Color: 0

Bin 623: 67 of cap free
Amount of items: 3
Items: 
Size: 463777 Color: 1
Size: 270418 Color: 1
Size: 265739 Color: 0

Bin 624: 69 of cap free
Amount of items: 3
Items: 
Size: 387255 Color: 1
Size: 314898 Color: 1
Size: 297779 Color: 0

Bin 625: 70 of cap free
Amount of items: 3
Items: 
Size: 387542 Color: 1
Size: 337882 Color: 1
Size: 274507 Color: 0

Bin 626: 70 of cap free
Amount of items: 3
Items: 
Size: 368487 Color: 1
Size: 316631 Color: 1
Size: 314813 Color: 0

Bin 627: 71 of cap free
Amount of items: 3
Items: 
Size: 382933 Color: 1
Size: 311716 Color: 1
Size: 305281 Color: 0

Bin 628: 72 of cap free
Amount of items: 3
Items: 
Size: 384940 Color: 1
Size: 337733 Color: 1
Size: 277256 Color: 0

Bin 629: 74 of cap free
Amount of items: 3
Items: 
Size: 406121 Color: 1
Size: 336425 Color: 1
Size: 257381 Color: 0

Bin 630: 76 of cap free
Amount of items: 3
Items: 
Size: 468431 Color: 1
Size: 276011 Color: 1
Size: 255483 Color: 0

Bin 631: 77 of cap free
Amount of items: 3
Items: 
Size: 384495 Color: 1
Size: 355653 Color: 1
Size: 259776 Color: 0

Bin 632: 81 of cap free
Amount of items: 3
Items: 
Size: 461583 Color: 1
Size: 283040 Color: 1
Size: 255297 Color: 0

Bin 633: 82 of cap free
Amount of items: 3
Items: 
Size: 385259 Color: 1
Size: 357857 Color: 1
Size: 256803 Color: 0

Bin 634: 82 of cap free
Amount of items: 3
Items: 
Size: 435914 Color: 1
Size: 304588 Color: 1
Size: 259417 Color: 0

Bin 635: 87 of cap free
Amount of items: 3
Items: 
Size: 362000 Color: 1
Size: 327635 Color: 0
Size: 310279 Color: 1

Bin 636: 93 of cap free
Amount of items: 3
Items: 
Size: 449194 Color: 1
Size: 296485 Color: 1
Size: 254229 Color: 0

Bin 637: 100 of cap free
Amount of items: 3
Items: 
Size: 387713 Color: 1
Size: 308267 Color: 1
Size: 303921 Color: 0

Bin 638: 102 of cap free
Amount of items: 3
Items: 
Size: 369399 Color: 1
Size: 350287 Color: 1
Size: 280213 Color: 0

Bin 639: 104 of cap free
Amount of items: 3
Items: 
Size: 398108 Color: 1
Size: 350989 Color: 1
Size: 250800 Color: 0

Bin 640: 105 of cap free
Amount of items: 3
Items: 
Size: 408702 Color: 1
Size: 309912 Color: 1
Size: 281282 Color: 0

Bin 641: 114 of cap free
Amount of items: 3
Items: 
Size: 451049 Color: 1
Size: 297641 Color: 1
Size: 251197 Color: 0

Bin 642: 118 of cap free
Amount of items: 3
Items: 
Size: 443619 Color: 1
Size: 292666 Color: 1
Size: 263598 Color: 0

Bin 643: 141 of cap free
Amount of items: 3
Items: 
Size: 433380 Color: 1
Size: 308725 Color: 1
Size: 257755 Color: 0

Bin 644: 144 of cap free
Amount of items: 3
Items: 
Size: 395338 Color: 1
Size: 306218 Color: 1
Size: 298301 Color: 0

Bin 645: 146 of cap free
Amount of items: 3
Items: 
Size: 444400 Color: 1
Size: 290482 Color: 1
Size: 264973 Color: 0

Bin 646: 155 of cap free
Amount of items: 3
Items: 
Size: 455703 Color: 1
Size: 292301 Color: 1
Size: 251842 Color: 0

Bin 647: 163 of cap free
Amount of items: 3
Items: 
Size: 392859 Color: 1
Size: 324716 Color: 1
Size: 282263 Color: 0

Bin 648: 164 of cap free
Amount of items: 3
Items: 
Size: 399429 Color: 1
Size: 348930 Color: 1
Size: 251478 Color: 0

Bin 649: 172 of cap free
Amount of items: 3
Items: 
Size: 392178 Color: 1
Size: 316606 Color: 1
Size: 291045 Color: 0

Bin 650: 176 of cap free
Amount of items: 3
Items: 
Size: 376588 Color: 1
Size: 323754 Color: 1
Size: 299483 Color: 0

Bin 651: 190 of cap free
Amount of items: 3
Items: 
Size: 442056 Color: 1
Size: 294730 Color: 1
Size: 263025 Color: 0

Bin 652: 212 of cap free
Amount of items: 3
Items: 
Size: 406716 Color: 1
Size: 335822 Color: 1
Size: 257251 Color: 0

Bin 653: 220 of cap free
Amount of items: 3
Items: 
Size: 424771 Color: 1
Size: 320782 Color: 1
Size: 254228 Color: 0

Bin 654: 344 of cap free
Amount of items: 3
Items: 
Size: 371627 Color: 1
Size: 365151 Color: 1
Size: 262879 Color: 0

Bin 655: 377 of cap free
Amount of items: 3
Items: 
Size: 419249 Color: 1
Size: 325582 Color: 1
Size: 254793 Color: 0

Bin 656: 450 of cap free
Amount of items: 3
Items: 
Size: 469016 Color: 1
Size: 278481 Color: 1
Size: 252054 Color: 0

Bin 657: 511 of cap free
Amount of items: 3
Items: 
Size: 362900 Color: 1
Size: 333516 Color: 1
Size: 303074 Color: 0

Bin 658: 535 of cap free
Amount of items: 3
Items: 
Size: 469827 Color: 1
Size: 276434 Color: 1
Size: 253205 Color: 0

Bin 659: 667 of cap free
Amount of items: 3
Items: 
Size: 417238 Color: 1
Size: 330715 Color: 1
Size: 251381 Color: 0

Bin 660: 670 of cap free
Amount of items: 3
Items: 
Size: 438521 Color: 1
Size: 300017 Color: 1
Size: 260793 Color: 0

Bin 661: 1074 of cap free
Amount of items: 3
Items: 
Size: 382049 Color: 1
Size: 319886 Color: 0
Size: 296992 Color: 1

Bin 662: 1791 of cap free
Amount of items: 3
Items: 
Size: 412901 Color: 1
Size: 308585 Color: 1
Size: 276724 Color: 0

Bin 663: 1914 of cap free
Amount of items: 3
Items: 
Size: 358352 Color: 1
Size: 324494 Color: 1
Size: 315241 Color: 0

Bin 664: 7408 of cap free
Amount of items: 3
Items: 
Size: 377630 Color: 1
Size: 343748 Color: 1
Size: 271215 Color: 0

Bin 665: 74051 of cap free
Amount of items: 3
Items: 
Size: 338438 Color: 1
Size: 320475 Color: 1
Size: 267037 Color: 0

Bin 666: 136666 of cap free
Amount of items: 3
Items: 
Size: 304473 Color: 1
Size: 300783 Color: 1
Size: 258079 Color: 0

Bin 667: 248090 of cap free
Amount of items: 2
Items: 
Size: 497273 Color: 1
Size: 254638 Color: 0

Bin 668: 519125 of cap free
Amount of items: 1
Items: 
Size: 480876 Color: 1

Total size: 667000667
Total free space: 1000001

