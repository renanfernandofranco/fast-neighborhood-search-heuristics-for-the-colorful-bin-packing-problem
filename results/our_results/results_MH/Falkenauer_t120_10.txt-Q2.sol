Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 301 Color: 1
Size: 292 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 270 Color: 1
Size: 251 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 330 Color: 1
Size: 292 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 0
Size: 250 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 258 Color: 0
Size: 254 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 284 Color: 1
Size: 257 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 0
Size: 251 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 355 Color: 0
Size: 268 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 315 Color: 0
Size: 258 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 361 Color: 0
Size: 277 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 316 Color: 1
Size: 294 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 0
Size: 264 Color: 0
Size: 262 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 308 Color: 1
Size: 266 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 300 Color: 1
Size: 258 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 297 Color: 0
Size: 282 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 339 Color: 1
Size: 276 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 352 Color: 1
Size: 265 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 1
Size: 342 Color: 0
Size: 312 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 320 Color: 1
Size: 273 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 329 Color: 1
Size: 303 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 255 Color: 1
Size: 254 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 339 Color: 1
Size: 306 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 355 Color: 0
Size: 269 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 297 Color: 0
Size: 251 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 345 Color: 0
Size: 265 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 305 Color: 1
Size: 257 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 0
Size: 250 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 281 Color: 0
Size: 298 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 319 Color: 0
Size: 266 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 306 Color: 0
Size: 269 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 287 Color: 1
Size: 272 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 352 Color: 1
Size: 263 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 297 Color: 1
Size: 282 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 285 Color: 1
Size: 279 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 305 Color: 1
Size: 287 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 0
Size: 346 Color: 0
Size: 298 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 272 Color: 1
Size: 257 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 269 Color: 0
Size: 253 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 287 Color: 0
Size: 251 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 324 Color: 0
Size: 274 Color: 1

Total size: 40000
Total free space: 0

