Capicity Bin: 7880
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 312
Size: 1940 Color: 229
Size: 384 Color: 98

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 323
Size: 1692 Color: 218
Size: 336 Color: 87

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5884 Color: 324
Size: 1388 Color: 206
Size: 608 Color: 126

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 326
Size: 1724 Color: 220
Size: 160 Color: 37

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6006 Color: 328
Size: 1724 Color: 221
Size: 150 Color: 28

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 335
Size: 909 Color: 169
Size: 751 Color: 152

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6222 Color: 336
Size: 1382 Color: 205
Size: 276 Color: 74

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 337
Size: 981 Color: 176
Size: 671 Color: 139

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 338
Size: 1222 Color: 193
Size: 412 Color: 102

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6407 Color: 345
Size: 1357 Color: 202
Size: 116 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6418 Color: 347
Size: 1242 Color: 197
Size: 220 Color: 59

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6453 Color: 348
Size: 863 Color: 165
Size: 564 Color: 122

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6527 Color: 350
Size: 1077 Color: 182
Size: 276 Color: 73

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 351
Size: 685 Color: 142
Size: 660 Color: 137

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 352
Size: 1226 Color: 195
Size: 106 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 356
Size: 931 Color: 170
Size: 348 Color: 90

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 359
Size: 1140 Color: 189
Size: 74 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6667 Color: 360
Size: 797 Color: 157
Size: 416 Color: 104

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 361
Size: 774 Color: 154
Size: 428 Color: 105

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 363
Size: 996 Color: 177
Size: 192 Color: 49

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 372
Size: 690 Color: 144
Size: 368 Color: 96

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6842 Color: 373
Size: 836 Color: 161
Size: 202 Color: 54

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6845 Color: 374
Size: 703 Color: 145
Size: 332 Color: 85

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6900 Color: 378
Size: 552 Color: 119
Size: 428 Color: 106

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6922 Color: 380
Size: 786 Color: 155
Size: 172 Color: 42

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 384
Size: 566 Color: 124
Size: 360 Color: 95

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 385
Size: 764 Color: 153
Size: 144 Color: 25

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6979 Color: 386
Size: 717 Color: 147
Size: 184 Color: 46

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 388
Size: 718 Color: 148
Size: 172 Color: 41

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 393
Size: 708 Color: 146
Size: 136 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7054 Color: 395
Size: 684 Color: 141
Size: 142 Color: 23

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 396
Size: 662 Color: 138
Size: 160 Color: 36

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7068 Color: 398
Size: 568 Color: 125
Size: 244 Color: 66

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 401
Size: 490 Color: 114
Size: 304 Color: 78

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 315
Size: 1895 Color: 228
Size: 308 Color: 79

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6005 Color: 327
Size: 1578 Color: 215
Size: 296 Color: 76

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6119 Color: 333
Size: 1284 Color: 199
Size: 476 Color: 109

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6307 Color: 339
Size: 1572 Color: 214

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6371 Color: 342
Size: 1380 Color: 204
Size: 128 Color: 12

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6410 Color: 346
Size: 1469 Color: 208

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 353
Size: 677 Color: 140
Size: 652 Color: 132

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6655 Color: 358
Size: 1224 Color: 194

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6812 Color: 371
Size: 1067 Color: 181

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6925 Color: 381
Size: 954 Color: 173

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 7059 Color: 397
Size: 820 Color: 160

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 7069 Color: 399
Size: 810 Color: 159

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 7077 Color: 400
Size: 802 Color: 158

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 402
Size: 787 Color: 156

Bin 49: 2 of cap free
Amount of items: 5
Items: 
Size: 4076 Color: 281
Size: 3274 Color: 265
Size: 192 Color: 50
Size: 168 Color: 39
Size: 168 Color: 38

Bin 50: 2 of cap free
Amount of items: 5
Items: 
Size: 4994 Color: 300
Size: 2412 Color: 241
Size: 200 Color: 53
Size: 136 Color: 19
Size: 136 Color: 18

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 306
Size: 2402 Color: 240
Size: 112 Color: 8

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 5430 Color: 311
Size: 2382 Color: 239
Size: 66 Color: 5

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 349
Size: 1362 Color: 203

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6687 Color: 362
Size: 1191 Color: 192

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6762 Color: 367
Size: 1116 Color: 187

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 383
Size: 940 Color: 172

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 7012 Color: 390
Size: 866 Color: 166

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 7037 Color: 394
Size: 841 Color: 162

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 5734 Color: 317
Size: 2143 Color: 235

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 6095 Color: 332
Size: 1782 Color: 224

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6566 Color: 354
Size: 1311 Color: 201

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 366
Size: 1121 Color: 188

Bin 63: 4 of cap free
Amount of items: 5
Items: 
Size: 3970 Color: 280
Size: 3266 Color: 264
Size: 296 Color: 77
Size: 176 Color: 43
Size: 168 Color: 40

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 7022 Color: 392
Size: 854 Color: 164

Bin 65: 5 of cap free
Amount of items: 5
Items: 
Size: 3946 Color: 276
Size: 1711 Color: 219
Size: 1554 Color: 212
Size: 480 Color: 111
Size: 184 Color: 48

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 4929 Color: 295
Size: 2806 Color: 253
Size: 140 Color: 22

Bin 67: 5 of cap free
Amount of items: 3
Items: 
Size: 4937 Color: 296
Size: 2798 Color: 252
Size: 140 Color: 21

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 325
Size: 1668 Color: 217
Size: 320 Color: 83

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 6312 Color: 340
Size: 1563 Color: 213

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 6765 Color: 368
Size: 1110 Color: 186

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 6791 Color: 370
Size: 1084 Color: 184

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 6983 Color: 387
Size: 892 Color: 168

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 284
Size: 3282 Color: 267
Size: 156 Color: 33

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 321
Size: 2062 Color: 232

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 331
Size: 1790 Color: 225

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 6213 Color: 334
Size: 1661 Color: 216

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 6394 Color: 344
Size: 1480 Color: 209

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 376
Size: 1014 Color: 180

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6910 Color: 379
Size: 964 Color: 175

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6992 Color: 389
Size: 882 Color: 167

Bin 81: 7 of cap free
Amount of items: 5
Items: 
Size: 3948 Color: 277
Size: 1771 Color: 223
Size: 1770 Color: 222
Size: 200 Color: 52
Size: 184 Color: 47

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 4945 Color: 297
Size: 2928 Color: 259

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 5309 Color: 305
Size: 2444 Color: 243
Size: 120 Color: 10

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 5420 Color: 310
Size: 2453 Color: 245

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 5714 Color: 316
Size: 2159 Color: 238

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 6871 Color: 377
Size: 1002 Color: 178

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 7021 Color: 391
Size: 852 Color: 163

Bin 88: 8 of cap free
Amount of items: 5
Items: 
Size: 3962 Color: 279
Size: 3262 Color: 263
Size: 292 Color: 75
Size: 180 Color: 45
Size: 176 Color: 44

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 5390 Color: 308
Size: 2482 Color: 247

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 322
Size: 2052 Color: 231

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 6372 Color: 343
Size: 1500 Color: 210

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 357
Size: 1260 Color: 198

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 365
Size: 1148 Color: 190

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 6774 Color: 369
Size: 1098 Color: 185

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 6937 Color: 382
Size: 934 Color: 171

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 6703 Color: 364
Size: 1167 Color: 191

Bin 97: 11 of cap free
Amount of items: 7
Items: 
Size: 3942 Color: 274
Size: 960 Color: 174
Size: 749 Color: 151
Size: 744 Color: 150
Size: 742 Color: 149
Size: 520 Color: 118
Size: 212 Color: 55

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 6858 Color: 375
Size: 1011 Color: 179

Bin 99: 12 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 320
Size: 2078 Color: 233
Size: 32 Color: 0

Bin 100: 13 of cap free
Amount of items: 4
Items: 
Size: 4740 Color: 291
Size: 2827 Color: 254
Size: 152 Color: 29
Size: 148 Color: 27

Bin 101: 13 of cap free
Amount of items: 3
Items: 
Size: 5303 Color: 304
Size: 2436 Color: 242
Size: 128 Color: 11

Bin 102: 13 of cap free
Amount of items: 2
Items: 
Size: 5406 Color: 309
Size: 2461 Color: 246

Bin 103: 13 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 355
Size: 1287 Color: 200

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 6340 Color: 341
Size: 1526 Color: 211

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 4988 Color: 299
Size: 2876 Color: 257

Bin 106: 18 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 298
Size: 2770 Color: 251
Size: 136 Color: 20

Bin 107: 18 of cap free
Amount of items: 2
Items: 
Size: 6018 Color: 329
Size: 1844 Color: 227

Bin 108: 19 of cap free
Amount of items: 4
Items: 
Size: 5755 Color: 319
Size: 2042 Color: 230
Size: 32 Color: 2
Size: 32 Color: 1

Bin 109: 22 of cap free
Amount of items: 3
Items: 
Size: 4526 Color: 289
Size: 3180 Color: 262
Size: 152 Color: 31

Bin 110: 24 of cap free
Amount of items: 2
Items: 
Size: 6050 Color: 330
Size: 1806 Color: 226

Bin 111: 30 of cap free
Amount of items: 2
Items: 
Size: 3954 Color: 278
Size: 3896 Color: 272

Bin 112: 30 of cap free
Amount of items: 2
Items: 
Size: 5750 Color: 318
Size: 2100 Color: 234

Bin 113: 32 of cap free
Amount of items: 2
Items: 
Size: 4923 Color: 294
Size: 2925 Color: 258

Bin 114: 38 of cap free
Amount of items: 3
Items: 
Size: 4514 Color: 288
Size: 3172 Color: 261
Size: 156 Color: 32

Bin 115: 41 of cap free
Amount of items: 5
Items: 
Size: 3943 Color: 275
Size: 1391 Color: 207
Size: 1229 Color: 196
Size: 1080 Color: 183
Size: 196 Color: 51

Bin 116: 47 of cap free
Amount of items: 3
Items: 
Size: 4848 Color: 293
Size: 2841 Color: 256
Size: 144 Color: 24

Bin 117: 49 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 314
Size: 2158 Color: 237
Size: 58 Color: 3

Bin 118: 52 of cap free
Amount of items: 3
Items: 
Size: 5132 Color: 303
Size: 2564 Color: 249
Size: 132 Color: 13

Bin 119: 59 of cap free
Amount of items: 2
Items: 
Size: 5374 Color: 307
Size: 2447 Color: 244

Bin 120: 60 of cap free
Amount of items: 3
Items: 
Size: 5607 Color: 313
Size: 2149 Color: 236
Size: 64 Color: 4

Bin 121: 62 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 282
Size: 3472 Color: 271
Size: 158 Color: 35

Bin 122: 63 of cap free
Amount of items: 3
Items: 
Size: 4380 Color: 283
Size: 3281 Color: 266
Size: 156 Color: 34

Bin 123: 72 of cap free
Amount of items: 4
Items: 
Size: 5002 Color: 301
Size: 2536 Color: 248
Size: 136 Color: 17
Size: 134 Color: 15

Bin 124: 86 of cap free
Amount of items: 3
Items: 
Size: 4558 Color: 290
Size: 3084 Color: 260
Size: 152 Color: 30

Bin 125: 87 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 292
Size: 2833 Color: 255
Size: 148 Color: 26

Bin 126: 92 of cap free
Amount of items: 16
Items: 
Size: 652 Color: 131
Size: 648 Color: 130
Size: 648 Color: 129
Size: 632 Color: 128
Size: 632 Color: 127
Size: 566 Color: 123
Size: 560 Color: 121
Size: 556 Color: 120
Size: 504 Color: 117
Size: 504 Color: 116
Size: 496 Color: 115
Size: 490 Color: 113
Size: 236 Color: 62
Size: 224 Color: 61
Size: 224 Color: 60
Size: 216 Color: 58

Bin 127: 102 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 302
Size: 2620 Color: 250
Size: 132 Color: 14

Bin 128: 107 of cap free
Amount of items: 2
Items: 
Size: 4489 Color: 287
Size: 3284 Color: 270

Bin 129: 116 of cap free
Amount of items: 2
Items: 
Size: 4481 Color: 286
Size: 3283 Color: 269

Bin 130: 125 of cap free
Amount of items: 2
Items: 
Size: 4473 Color: 285
Size: 3282 Color: 268

Bin 131: 201 of cap free
Amount of items: 8
Items: 
Size: 3941 Color: 273
Size: 686 Color: 143
Size: 656 Color: 136
Size: 656 Color: 135
Size: 656 Color: 134
Size: 652 Color: 133
Size: 216 Color: 57
Size: 216 Color: 56

Bin 132: 298 of cap free
Amount of items: 22
Items: 
Size: 488 Color: 112
Size: 480 Color: 110
Size: 472 Color: 108
Size: 440 Color: 107
Size: 412 Color: 103
Size: 408 Color: 101
Size: 408 Color: 100
Size: 404 Color: 99
Size: 378 Color: 97
Size: 360 Color: 94
Size: 356 Color: 93
Size: 354 Color: 92
Size: 312 Color: 80
Size: 272 Color: 72
Size: 272 Color: 71
Size: 272 Color: 70
Size: 262 Color: 69
Size: 256 Color: 68
Size: 248 Color: 67
Size: 244 Color: 65
Size: 244 Color: 64
Size: 240 Color: 63

Bin 133: 5548 of cap free
Amount of items: 7
Items: 
Size: 352 Color: 91
Size: 348 Color: 89
Size: 344 Color: 88
Size: 336 Color: 86
Size: 328 Color: 84
Size: 312 Color: 82
Size: 312 Color: 81

Total size: 1040160
Total free space: 7880

