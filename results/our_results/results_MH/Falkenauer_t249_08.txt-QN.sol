Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 247
Size: 252 Color: 11
Size: 250 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 203
Size: 325 Color: 137
Size: 270 Color: 74

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 214
Size: 288 Color: 106
Size: 270 Color: 71

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 207
Size: 334 Color: 140
Size: 255 Color: 25

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 152
Size: 346 Color: 151
Size: 308 Color: 119

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 183
Size: 363 Color: 166
Size: 254 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 205
Size: 324 Color: 135
Size: 269 Color: 67

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 211
Size: 300 Color: 113
Size: 264 Color: 53

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 200
Size: 319 Color: 130
Size: 278 Color: 94

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 239
Size: 264 Color: 52
Size: 253 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 195
Size: 348 Color: 154
Size: 251 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 216
Size: 278 Color: 93
Size: 275 Color: 85

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 196
Size: 325 Color: 136
Size: 274 Color: 79

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 238
Size: 260 Color: 37
Size: 258 Color: 31

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 175
Size: 347 Color: 153
Size: 276 Color: 88

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 204
Size: 340 Color: 148
Size: 254 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 188
Size: 320 Color: 133
Size: 291 Color: 108

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 173
Size: 360 Color: 163
Size: 269 Color: 68

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 217
Size: 277 Color: 90
Size: 275 Color: 83

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 240
Size: 267 Color: 62
Size: 250 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 224
Size: 275 Color: 84
Size: 266 Color: 58

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 220
Size: 289 Color: 107
Size: 261 Color: 42

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 218
Size: 276 Color: 87
Size: 275 Color: 81

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 225
Size: 275 Color: 80
Size: 266 Color: 59

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 241
Size: 258 Color: 33
Size: 255 Color: 22

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 228
Size: 271 Color: 76
Size: 263 Color: 51

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 181
Size: 320 Color: 132
Size: 298 Color: 112

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 185
Size: 365 Color: 169
Size: 250 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 206
Size: 317 Color: 127
Size: 275 Color: 86

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 212
Size: 286 Color: 104
Size: 276 Color: 89

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 178
Size: 314 Color: 125
Size: 308 Color: 120

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 199
Size: 317 Color: 126
Size: 281 Color: 97

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 164
Size: 326 Color: 138
Size: 313 Color: 123

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 156
Size: 349 Color: 155
Size: 301 Color: 115

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 219
Size: 295 Color: 111
Size: 256 Color: 26

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 243
Size: 260 Color: 38
Size: 252 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 231
Size: 268 Color: 65
Size: 257 Color: 29

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 236
Size: 260 Color: 39
Size: 259 Color: 36

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 248
Size: 252 Color: 12
Size: 250 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 182
Size: 355 Color: 159
Size: 263 Color: 49

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 201
Size: 317 Color: 129
Size: 279 Color: 96

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 184
Size: 363 Color: 167
Size: 254 Color: 21

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 197
Size: 338 Color: 146
Size: 260 Color: 40

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 192
Size: 334 Color: 143
Size: 270 Color: 73

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 246
Size: 257 Color: 30
Size: 250 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 194
Size: 339 Color: 147
Size: 263 Color: 47

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 189
Size: 357 Color: 160
Size: 252 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 193
Size: 319 Color: 131
Size: 285 Color: 101

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 237
Size: 268 Color: 64
Size: 250 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 168
Size: 362 Color: 165
Size: 275 Color: 82

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 244
Size: 258 Color: 32
Size: 252 Color: 14

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 190
Size: 335 Color: 144
Size: 270 Color: 72

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 234
Size: 264 Color: 54
Size: 257 Color: 28

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 180
Size: 320 Color: 134
Size: 300 Color: 114

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 223
Size: 283 Color: 99
Size: 262 Color: 44

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 230
Size: 270 Color: 70
Size: 261 Color: 41

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 226
Size: 286 Color: 103
Size: 250 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 186
Size: 358 Color: 161
Size: 256 Color: 27

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 191
Size: 336 Color: 145
Size: 269 Color: 69

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 232
Size: 273 Color: 78
Size: 251 Color: 8

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 227
Size: 269 Color: 66
Size: 266 Color: 57

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 176
Size: 359 Color: 162
Size: 263 Color: 48

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 177
Size: 313 Color: 124
Size: 309 Color: 121

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 187
Size: 317 Color: 128
Size: 295 Color: 110

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 233
Size: 267 Color: 60
Size: 254 Color: 20

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 170
Size: 351 Color: 158
Size: 283 Color: 98

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 174
Size: 351 Color: 157
Size: 278 Color: 91

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 150
Size: 343 Color: 149
Size: 312 Color: 122

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 172
Size: 367 Color: 171
Size: 264 Color: 55

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 242
Size: 262 Color: 46
Size: 250 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 179
Size: 334 Color: 141
Size: 287 Color: 105

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 215
Size: 284 Color: 100
Size: 271 Color: 77

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 213
Size: 307 Color: 118
Size: 251 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 198
Size: 331 Color: 139
Size: 267 Color: 61

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 208
Size: 293 Color: 109
Size: 278 Color: 92

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 222
Size: 279 Color: 95
Size: 267 Color: 63

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 229
Size: 271 Color: 75
Size: 261 Color: 43

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 221
Size: 286 Color: 102
Size: 263 Color: 50

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 245
Size: 255 Color: 24
Size: 252 Color: 16

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 210
Size: 305 Color: 116
Size: 259 Color: 35

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 209
Size: 306 Color: 117
Size: 259 Color: 34

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 202
Size: 334 Color: 142
Size: 262 Color: 45

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 235
Size: 265 Color: 56
Size: 255 Color: 23

Total size: 83000
Total free space: 0

