Capicity Bin: 9808
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4800 Color: 272
Size: 1440 Color: 189
Size: 1184 Color: 161
Size: 864 Color: 139
Size: 592 Color: 111
Size: 544 Color: 106
Size: 384 Color: 85

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 374
Size: 1192 Color: 164
Size: 160 Color: 12
Size: 64 Color: 1

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 7904 Color: 345
Size: 1504 Color: 192
Size: 368 Color: 82
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6367 Color: 304
Size: 2869 Color: 237
Size: 572 Color: 108

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 281
Size: 4068 Color: 261
Size: 808 Color: 129

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6158 Color: 301
Size: 3042 Color: 241
Size: 608 Color: 113

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 323
Size: 2105 Color: 218
Size: 420 Color: 90

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 400
Size: 856 Color: 138
Size: 160 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7798 Color: 340
Size: 1678 Color: 201
Size: 332 Color: 75

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7496 Color: 330
Size: 1928 Color: 212
Size: 384 Color: 84

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8742 Color: 396
Size: 890 Color: 142
Size: 176 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 382
Size: 1108 Color: 158
Size: 216 Color: 34

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8547 Color: 386
Size: 1051 Color: 151
Size: 210 Color: 32

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7830 Color: 342
Size: 1650 Color: 199
Size: 328 Color: 72

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7124 Color: 319
Size: 2596 Color: 230
Size: 88 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8533 Color: 384
Size: 1063 Color: 152
Size: 212 Color: 33

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 321
Size: 2212 Color: 221
Size: 440 Color: 92

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8626 Color: 391
Size: 986 Color: 148
Size: 196 Color: 28

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7996 Color: 349
Size: 1516 Color: 193
Size: 296 Color: 65

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8182 Color: 359
Size: 1358 Color: 180
Size: 268 Color: 55

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 277
Size: 4084 Color: 263
Size: 816 Color: 133

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 294
Size: 3444 Color: 247
Size: 680 Color: 118

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7887 Color: 344
Size: 1601 Color: 197
Size: 320 Color: 70

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8650 Color: 393
Size: 966 Color: 145
Size: 192 Color: 27

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6637 Color: 309
Size: 2643 Color: 231
Size: 528 Color: 102

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 398
Size: 888 Color: 141
Size: 160 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8794 Color: 401
Size: 846 Color: 137
Size: 168 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5272 Color: 286
Size: 3784 Color: 257
Size: 752 Color: 126

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 290
Size: 3522 Color: 251
Size: 700 Color: 121

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 377
Size: 1326 Color: 177
Size: 72 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 287
Size: 3546 Color: 255
Size: 708 Color: 123

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7318 Color: 325
Size: 2078 Color: 217
Size: 412 Color: 89

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8124 Color: 354
Size: 1404 Color: 185
Size: 280 Color: 60

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8122 Color: 353
Size: 1406 Color: 186
Size: 280 Color: 61

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4240 Color: 267
Size: 3712 Color: 256
Size: 1856 Color: 208

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 302
Size: 2932 Color: 240
Size: 584 Color: 110

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4964 Color: 284
Size: 4044 Color: 259
Size: 800 Color: 128

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8574 Color: 389
Size: 1074 Color: 154
Size: 160 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 8275 Color: 366
Size: 1279 Color: 172
Size: 254 Color: 48

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8150 Color: 356
Size: 1382 Color: 183
Size: 276 Color: 59

Bin 41: 0 of cap free
Amount of items: 4
Items: 
Size: 7594 Color: 332
Size: 1846 Color: 207
Size: 224 Color: 37
Size: 144 Color: 7

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7151 Color: 320
Size: 2215 Color: 222
Size: 442 Color: 93

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7836 Color: 343
Size: 1644 Color: 198
Size: 328 Color: 74

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 8692 Color: 395
Size: 932 Color: 144
Size: 184 Color: 21

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 8748 Color: 397
Size: 908 Color: 143
Size: 152 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5559 Color: 289
Size: 3541 Color: 253
Size: 708 Color: 124

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 311
Size: 2516 Color: 229
Size: 496 Color: 100

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 8255 Color: 365
Size: 1295 Color: 174
Size: 258 Color: 50

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 8195 Color: 361
Size: 1427 Color: 188
Size: 186 Color: 25

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 8005 Color: 350
Size: 1503 Color: 191
Size: 300 Color: 67

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 8227 Color: 363
Size: 1319 Color: 176
Size: 262 Color: 52

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 285
Size: 3818 Color: 258
Size: 760 Color: 127

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 307
Size: 2678 Color: 233
Size: 532 Color: 104

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5557 Color: 288
Size: 3543 Color: 254
Size: 708 Color: 125

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 8536 Color: 385
Size: 1064 Color: 153
Size: 208 Color: 31

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 306
Size: 2686 Color: 234
Size: 536 Color: 105

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 8214 Color: 362
Size: 1330 Color: 178
Size: 264 Color: 53

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 8436 Color: 379
Size: 1188 Color: 163
Size: 184 Color: 23

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7741 Color: 336
Size: 1909 Color: 210
Size: 158 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 310
Size: 2908 Color: 239
Size: 152 Color: 9

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5618 Color: 292
Size: 3494 Color: 248
Size: 696 Color: 120

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 279
Size: 4084 Color: 262
Size: 808 Color: 130

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4905 Color: 274
Size: 4087 Color: 266
Size: 816 Color: 132

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 317
Size: 2322 Color: 223
Size: 464 Color: 94

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 324
Size: 2190 Color: 220
Size: 328 Color: 73

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 346
Size: 1544 Color: 196
Size: 304 Color: 68

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7806 Color: 341
Size: 1670 Color: 200
Size: 332 Color: 76

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 322
Size: 2184 Color: 219
Size: 432 Color: 91

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 337
Size: 1720 Color: 204
Size: 336 Color: 78

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8022 Color: 351
Size: 1490 Color: 190
Size: 296 Color: 64

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 8375 Color: 372
Size: 1195 Color: 166
Size: 238 Color: 42

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 326
Size: 2050 Color: 216
Size: 408 Color: 88

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 295
Size: 3412 Color: 246
Size: 680 Color: 119

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5636 Color: 293
Size: 3540 Color: 252
Size: 632 Color: 115

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 380
Size: 1218 Color: 167
Size: 148 Color: 8

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4936 Color: 282
Size: 4584 Color: 270
Size: 288 Color: 63

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 8190 Color: 360
Size: 1350 Color: 179
Size: 268 Color: 54

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 8399 Color: 375
Size: 1175 Color: 160
Size: 234 Color: 40

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 316
Size: 2708 Color: 235
Size: 88 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 8310 Color: 369
Size: 1250 Color: 170
Size: 248 Color: 46

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 7477 Color: 329
Size: 1943 Color: 213
Size: 388 Color: 86

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 8572 Color: 388
Size: 1036 Color: 149
Size: 200 Color: 29

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7785 Color: 339
Size: 1687 Color: 202
Size: 336 Color: 77

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 280
Size: 4408 Color: 268
Size: 480 Color: 98

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7990 Color: 348
Size: 1518 Color: 194
Size: 300 Color: 66

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4907 Color: 276
Size: 4085 Color: 264
Size: 816 Color: 134

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 8236 Color: 364
Size: 1316 Color: 175
Size: 256 Color: 49

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 8644 Color: 392
Size: 980 Color: 147
Size: 184 Color: 22

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 313
Size: 2488 Color: 226
Size: 480 Color: 97

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 373
Size: 1194 Color: 165
Size: 236 Color: 41

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 8820 Color: 402
Size: 828 Color: 136
Size: 160 Color: 15

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 7461 Color: 328
Size: 2021 Color: 215
Size: 326 Color: 71

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 8762 Color: 399
Size: 874 Color: 140
Size: 172 Color: 18

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 8407 Color: 376
Size: 1169 Color: 159
Size: 232 Color: 39

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 8324 Color: 371
Size: 1244 Color: 169
Size: 240 Color: 44

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 7562 Color: 331
Size: 1874 Color: 209
Size: 372 Color: 83

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 308
Size: 2650 Color: 232
Size: 528 Color: 103

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 283
Size: 4052 Color: 260
Size: 808 Color: 131

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 8483 Color: 381
Size: 1105 Color: 157
Size: 220 Color: 36

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 367
Size: 1288 Color: 173
Size: 240 Color: 43

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 327
Size: 1988 Color: 214
Size: 392 Color: 87

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 8664 Color: 394
Size: 968 Color: 146
Size: 176 Color: 20

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 7623 Color: 333
Size: 1925 Color: 211
Size: 260 Color: 51

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6408 Color: 305
Size: 2840 Color: 236
Size: 560 Color: 107

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7652 Color: 334
Size: 1804 Color: 206
Size: 352 Color: 81

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 291
Size: 3514 Color: 249
Size: 700 Color: 122

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 8287 Color: 368
Size: 1269 Color: 171
Size: 252 Color: 47

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6103 Color: 299
Size: 3519 Color: 250
Size: 186 Color: 24

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 338
Size: 1702 Color: 203
Size: 340 Color: 79

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 315
Size: 2350 Color: 224
Size: 468 Color: 95

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 8563 Color: 387
Size: 1039 Color: 150
Size: 206 Color: 30

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 390
Size: 1102 Color: 155
Size: 128 Color: 6

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 4906 Color: 275
Size: 4086 Color: 265
Size: 816 Color: 135

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4904 Color: 273
Size: 4792 Color: 271
Size: 112 Color: 5

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 355
Size: 1400 Color: 184
Size: 272 Color: 56

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 383
Size: 1102 Color: 156
Size: 216 Color: 35

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 300
Size: 3054 Color: 242
Size: 608 Color: 112

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 297
Size: 3272 Color: 244
Size: 640 Color: 116

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 370
Size: 1242 Color: 168
Size: 244 Color: 45

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 303
Size: 2892 Color: 238
Size: 576 Color: 109

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 7087 Color: 318
Size: 2491 Color: 227
Size: 230 Color: 38

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 312
Size: 2495 Color: 228
Size: 498 Color: 101

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 7701 Color: 335
Size: 1757 Color: 205
Size: 350 Color: 80

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 8155 Color: 357
Size: 1379 Color: 182
Size: 274 Color: 58

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 8107 Color: 352
Size: 1419 Color: 187
Size: 282 Color: 62

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6101 Color: 298
Size: 3091 Color: 243
Size: 616 Color: 114

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 296
Size: 3388 Color: 245
Size: 672 Color: 117

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 378
Size: 1186 Color: 162
Size: 188 Color: 26

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 8163 Color: 358
Size: 1371 Color: 181
Size: 274 Color: 57

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 278
Size: 4410 Color: 269
Size: 488 Color: 99

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 314
Size: 2371 Color: 225
Size: 474 Color: 96

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7965 Color: 347
Size: 1537 Color: 195
Size: 306 Color: 69

Total size: 1294656
Total free space: 0

