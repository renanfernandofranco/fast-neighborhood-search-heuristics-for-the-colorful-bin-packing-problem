Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 320 Color: 1
Size: 267 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 341 Color: 1
Size: 268 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 266 Color: 1
Size: 251 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 329 Color: 1
Size: 272 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 335 Color: 1
Size: 304 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 293 Color: 0
Size: 291 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 326 Color: 0
Size: 305 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 347 Color: 1
Size: 265 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 264 Color: 1
Size: 251 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 324 Color: 1
Size: 281 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 332 Color: 1
Size: 303 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 324 Color: 1
Size: 275 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 322 Color: 1
Size: 252 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 266 Color: 0
Size: 361 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 326 Color: 1
Size: 285 Color: 0
Size: 390 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 320 Color: 1
Size: 287 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 366 Color: 1
Size: 254 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 316 Color: 1
Size: 285 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 1
Size: 297 Color: 0
Size: 359 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 276 Color: 1
Size: 269 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 302 Color: 1
Size: 297 Color: 0
Size: 402 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 323 Color: 1
Size: 265 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 345 Color: 1
Size: 262 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 334 Color: 1
Size: 291 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 359 Color: 1
Size: 253 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 344 Color: 1
Size: 258 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 331 Color: 1
Size: 287 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 285 Color: 1
Size: 265 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 298 Color: 1
Size: 283 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 313 Color: 1
Size: 250 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1
Size: 335 Color: 1
Size: 319 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 334 Color: 1
Size: 288 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 1
Size: 252 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 312 Color: 1
Size: 256 Color: 0

Total size: 34034
Total free space: 0

