Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1798 Color: 0
Size: 466 Color: 1
Size: 88 Color: 0
Size: 56 Color: 0
Size: 48 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 0
Size: 286 Color: 0
Size: 72 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1956 Color: 0
Size: 280 Color: 0
Size: 92 Color: 0
Size: 80 Color: 1
Size: 48 Color: 1

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 2206 Color: 0
Size: 210 Color: 0
Size: 24 Color: 1
Size: 16 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 738 Color: 1
Size: 144 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 0
Size: 827 Color: 1
Size: 164 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 1
Size: 980 Color: 1
Size: 112 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 0
Size: 1022 Color: 0
Size: 204 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 0
Size: 923 Color: 0
Size: 184 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 232 Color: 1
Size: 32 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 1
Size: 334 Color: 0
Size: 64 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 0
Size: 374 Color: 1
Size: 72 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 1
Size: 444 Color: 0
Size: 272 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1844 Color: 0
Size: 516 Color: 0
Size: 96 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 160 Color: 0
Size: 124 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 1
Size: 410 Color: 1
Size: 80 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 0
Size: 330 Color: 1
Size: 64 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 1
Size: 246 Color: 1
Size: 48 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 0
Size: 771 Color: 1
Size: 154 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1020 Color: 0
Size: 924 Color: 0
Size: 512 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 0
Size: 611 Color: 0
Size: 120 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 1
Size: 300 Color: 0
Size: 48 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 282 Color: 1
Size: 56 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1580 Color: 0
Size: 732 Color: 1
Size: 144 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 550 Color: 0
Size: 8 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1018 Color: 1
Size: 200 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 0
Size: 604 Color: 0
Size: 40 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 0
Size: 605 Color: 0
Size: 120 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 0
Size: 538 Color: 0
Size: 104 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 634 Color: 0
Size: 64 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 0
Size: 674 Color: 0
Size: 88 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 750 Color: 0
Size: 148 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 638 Color: 0
Size: 108 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2132 Color: 0
Size: 276 Color: 0
Size: 48 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 1
Size: 334 Color: 0
Size: 8 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1023 Color: 1
Size: 204 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 0
Size: 862 Color: 1
Size: 168 Color: 1

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 1492 Color: 1
Size: 540 Color: 0
Size: 384 Color: 0
Size: 40 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 0
Size: 745 Color: 1
Size: 148 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 0
Size: 340 Color: 0
Size: 64 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 0
Size: 976 Color: 1
Size: 244 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 1
Size: 214 Color: 0
Size: 40 Color: 1

Bin 43: 0 of cap free
Amount of items: 4
Items: 
Size: 1696 Color: 1
Size: 568 Color: 1
Size: 112 Color: 0
Size: 80 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1668 Color: 0
Size: 660 Color: 1
Size: 128 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 364 Color: 0
Size: 64 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 1
Size: 454 Color: 1
Size: 88 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 0
Size: 228 Color: 1
Size: 40 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 0
Size: 402 Color: 0
Size: 76 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 0
Size: 482 Color: 1
Size: 92 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 0
Size: 420 Color: 0
Size: 104 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 0
Size: 874 Color: 0
Size: 172 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 0
Size: 386 Color: 0
Size: 76 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1449 Color: 0
Size: 841 Color: 1
Size: 166 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 0
Size: 840 Color: 0
Size: 80 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1452 Color: 0
Size: 804 Color: 1
Size: 200 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 0
Size: 841 Color: 0
Size: 68 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 1021 Color: 0
Size: 204 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 646 Color: 0
Size: 128 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 212 Color: 1
Size: 40 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 1
Size: 242 Color: 0
Size: 48 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 0
Size: 553 Color: 1
Size: 110 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 1
Size: 671 Color: 1
Size: 134 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 665 Color: 1
Size: 132 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1351 Color: 0
Size: 921 Color: 1
Size: 184 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 1
Size: 751 Color: 1
Size: 150 Color: 0

Total size: 159640
Total free space: 0

