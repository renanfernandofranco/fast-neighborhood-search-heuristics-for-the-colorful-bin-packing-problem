Capicity Bin: 2464
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 10
Size: 1080 Color: 13
Size: 46 Color: 18

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 2106 Color: 13
Size: 168 Color: 10
Size: 116 Color: 14
Size: 42 Color: 4
Size: 32 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1972 Color: 6
Size: 304 Color: 9
Size: 104 Color: 14
Size: 68 Color: 6
Size: 16 Color: 3

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1994 Color: 6
Size: 394 Color: 19
Size: 44 Color: 16
Size: 32 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 3
Size: 340 Color: 12
Size: 306 Color: 9

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 942 Color: 1
Size: 840 Color: 14
Size: 542 Color: 6
Size: 140 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 7
Size: 481 Color: 9
Size: 96 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 9
Size: 621 Color: 3
Size: 122 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 4
Size: 870 Color: 17
Size: 172 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 3
Size: 622 Color: 18
Size: 124 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2024 Color: 19
Size: 368 Color: 14
Size: 72 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 5
Size: 362 Color: 5
Size: 68 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 10
Size: 351 Color: 2
Size: 68 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 13
Size: 478 Color: 19
Size: 92 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2215 Color: 15
Size: 209 Color: 3
Size: 40 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 7
Size: 418 Color: 19
Size: 80 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 14
Size: 578 Color: 10
Size: 112 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 11
Size: 214 Color: 12
Size: 40 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 11
Size: 302 Color: 5
Size: 64 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 5
Size: 1102 Color: 12
Size: 128 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 0
Size: 250 Color: 16
Size: 48 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2135 Color: 18
Size: 275 Color: 14
Size: 54 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 16
Size: 338 Color: 10
Size: 76 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 15
Size: 810 Color: 0
Size: 160 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 6
Size: 226 Color: 0
Size: 44 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 8
Size: 591 Color: 8
Size: 116 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 5
Size: 346 Color: 6
Size: 56 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1905 Color: 17
Size: 467 Color: 9
Size: 92 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 6
Size: 261 Color: 3
Size: 50 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 17
Size: 301 Color: 1
Size: 60 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 19
Size: 722 Color: 17
Size: 140 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 5
Size: 762 Color: 7
Size: 148 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 9
Size: 374 Color: 3
Size: 72 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 16
Size: 715 Color: 18
Size: 142 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 9
Size: 669 Color: 13
Size: 132 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 9
Size: 269 Color: 9
Size: 52 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 5
Size: 1062 Color: 19
Size: 48 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2150 Color: 1
Size: 262 Color: 14
Size: 52 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 11
Size: 523 Color: 14
Size: 104 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 9
Size: 771 Color: 17
Size: 152 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 6
Size: 274 Color: 19
Size: 52 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 10
Size: 421 Color: 12
Size: 82 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2067 Color: 13
Size: 331 Color: 11
Size: 66 Color: 15

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 13
Size: 662 Color: 16
Size: 128 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2087 Color: 9
Size: 315 Color: 2
Size: 62 Color: 9

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2121 Color: 7
Size: 287 Color: 5
Size: 56 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 1
Size: 1014 Color: 2
Size: 200 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 7
Size: 235 Color: 13
Size: 28 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1866 Color: 0
Size: 502 Color: 14
Size: 96 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 3
Size: 286 Color: 7
Size: 56 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 2
Size: 871 Color: 10
Size: 50 Color: 12

Bin 52: 0 of cap free
Amount of items: 4
Items: 
Size: 1220 Color: 11
Size: 912 Color: 12
Size: 184 Color: 15
Size: 148 Color: 17

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2169 Color: 13
Size: 273 Color: 8
Size: 22 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 14
Size: 383 Color: 4
Size: 76 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2055 Color: 18
Size: 387 Color: 4
Size: 22 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 13
Size: 442 Color: 18
Size: 88 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2183 Color: 18
Size: 221 Color: 12
Size: 60 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 16
Size: 425 Color: 12
Size: 84 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 0
Size: 413 Color: 16
Size: 82 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2193 Color: 14
Size: 267 Color: 0
Size: 4 Color: 16

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 8
Size: 885 Color: 8
Size: 176 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 6
Size: 1027 Color: 3
Size: 204 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 13
Size: 887 Color: 12
Size: 176 Color: 11

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 10
Size: 1025 Color: 18
Size: 204 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 10
Size: 541 Color: 9
Size: 108 Color: 17

Total size: 160160
Total free space: 0

