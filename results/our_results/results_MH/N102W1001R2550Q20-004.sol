Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 6
Size: 333 Color: 5
Size: 300 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 9
Size: 250 Color: 13
Size: 250 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 18
Size: 269 Color: 2
Size: 260 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 320 Color: 18
Size: 293 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 10
Size: 256 Color: 19
Size: 256 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 16
Size: 269 Color: 15
Size: 268 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 19
Size: 251 Color: 5
Size: 251 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 361 Color: 3
Size: 277 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 309 Color: 5
Size: 252 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 262 Color: 9
Size: 254 Color: 17
Size: 485 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 9
Size: 299 Color: 3
Size: 296 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 289 Color: 17
Size: 261 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 3
Size: 371 Color: 3
Size: 259 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6
Size: 331 Color: 12
Size: 313 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 11
Size: 250 Color: 11
Size: 250 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 6
Size: 333 Color: 14
Size: 286 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 19
Size: 329 Color: 9
Size: 264 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 335 Color: 7
Size: 292 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 19
Size: 338 Color: 3
Size: 302 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 14
Size: 337 Color: 4
Size: 315 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 16
Size: 345 Color: 5
Size: 250 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 13
Size: 300 Color: 7
Size: 284 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 16
Size: 366 Color: 16
Size: 269 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 13
Size: 276 Color: 5
Size: 252 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 18
Size: 295 Color: 13
Size: 260 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 18
Size: 307 Color: 3
Size: 291 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 7
Size: 291 Color: 12
Size: 266 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 374 Color: 6
Size: 250 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 293 Color: 4
Size: 298 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 317 Color: 13
Size: 253 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 355 Color: 5
Size: 253 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 0
Size: 334 Color: 11
Size: 330 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 13
Size: 315 Color: 16
Size: 265 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 331 Color: 12
Size: 253 Color: 5

Total size: 34034
Total free space: 0

