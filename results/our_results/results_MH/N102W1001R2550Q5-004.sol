Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 252 Color: 0
Size: 251 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 315 Color: 2
Size: 292 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 284 Color: 0
Size: 256 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 265 Color: 1
Size: 261 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 366 Color: 1
Size: 266 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 367 Color: 0
Size: 252 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 303 Color: 2
Size: 262 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 2
Size: 250 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 260 Color: 1
Size: 252 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 265 Color: 0
Size: 260 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 290 Color: 4
Size: 277 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 327 Color: 2
Size: 288 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 298 Color: 0
Size: 285 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 3
Size: 301 Color: 4
Size: 293 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 343 Color: 3
Size: 266 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 275 Color: 4
Size: 267 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 4
Size: 315 Color: 2
Size: 261 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 270 Color: 1
Size: 265 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 334 Color: 3
Size: 256 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 317 Color: 4
Size: 259 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 361 Color: 4
Size: 270 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 278 Color: 1
Size: 263 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 347 Color: 2
Size: 254 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 323 Color: 4
Size: 300 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 300 Color: 0
Size: 295 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 271 Color: 4
Size: 252 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 2
Size: 316 Color: 1
Size: 291 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 281 Color: 4
Size: 276 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 313 Color: 0
Size: 279 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 279 Color: 2
Size: 276 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 285 Color: 3
Size: 252 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 326 Color: 4
Size: 255 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 267 Color: 0
Size: 254 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 306 Color: 1
Size: 254 Color: 1

Total size: 34034
Total free space: 0

