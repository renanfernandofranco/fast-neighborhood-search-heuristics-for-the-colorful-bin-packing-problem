Capicity Bin: 8016
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7056 Color: 387
Size: 800 Color: 150
Size: 160 Color: 27

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 3968 Color: 272
Size: 1096 Color: 177
Size: 992 Color: 169
Size: 640 Color: 128
Size: 496 Color: 115
Size: 448 Color: 102
Size: 376 Color: 93

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 6652 Color: 360
Size: 1140 Color: 180
Size: 128 Color: 9
Size: 96 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 362
Size: 1124 Color: 178
Size: 216 Color: 51

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 310
Size: 2118 Color: 232
Size: 420 Color: 99

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4108 Color: 280
Size: 3260 Color: 263
Size: 648 Color: 129

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 297
Size: 2451 Color: 243
Size: 490 Color: 113

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 326
Size: 1713 Color: 216
Size: 342 Color: 84

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 324
Size: 1758 Color: 217
Size: 348 Color: 85

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4569 Color: 286
Size: 2873 Color: 256
Size: 574 Color: 122

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 363
Size: 1093 Color: 176
Size: 218 Color: 53

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 320
Size: 1773 Color: 221
Size: 354 Color: 88

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6310 Color: 340
Size: 1498 Color: 203
Size: 208 Color: 48

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7206 Color: 402
Size: 678 Color: 137
Size: 132 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5092 Color: 298
Size: 2468 Color: 246
Size: 456 Color: 103

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6447 Color: 350
Size: 1309 Color: 191
Size: 260 Color: 63

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 389
Size: 798 Color: 149
Size: 156 Color: 23

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5794 Color: 316
Size: 1854 Color: 226
Size: 368 Color: 92

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 289
Size: 2804 Color: 251
Size: 560 Color: 119

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 328
Size: 1702 Color: 214
Size: 340 Color: 82

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6587 Color: 355
Size: 1191 Color: 184
Size: 238 Color: 59

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 354
Size: 1220 Color: 187
Size: 240 Color: 60

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6254 Color: 338
Size: 1658 Color: 212
Size: 104 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 401
Size: 690 Color: 138
Size: 136 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7089 Color: 392
Size: 773 Color: 146
Size: 154 Color: 22

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6415 Color: 348
Size: 1335 Color: 192
Size: 266 Color: 65

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6012 Color: 329
Size: 1676 Color: 213
Size: 328 Color: 81

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 379
Size: 882 Color: 159
Size: 172 Color: 34

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 6408 Color: 346
Size: 1216 Color: 186
Size: 224 Color: 55
Size: 168 Color: 32

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 332
Size: 1566 Color: 208
Size: 312 Color: 77

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4978 Color: 292
Size: 2858 Color: 254
Size: 180 Color: 36

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4009 Color: 273
Size: 3341 Color: 269
Size: 666 Color: 134

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 366
Size: 1042 Color: 172
Size: 208 Color: 47

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 302
Size: 2404 Color: 240
Size: 472 Color: 109

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 349
Size: 1522 Color: 205
Size: 48 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 373
Size: 943 Color: 166
Size: 188 Color: 41

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 372
Size: 970 Color: 167
Size: 192 Color: 42

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5029 Color: 294
Size: 2491 Color: 247
Size: 496 Color: 116

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4014 Color: 277
Size: 3338 Color: 267
Size: 664 Color: 131

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5398 Color: 308
Size: 2182 Color: 234
Size: 436 Color: 101

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5071 Color: 296
Size: 2455 Color: 244
Size: 490 Color: 114

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 304
Size: 2356 Color: 238
Size: 464 Color: 104

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6235 Color: 337
Size: 1485 Color: 201
Size: 296 Color: 74

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 352
Size: 1356 Color: 197
Size: 136 Color: 11

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 390
Size: 788 Color: 148
Size: 152 Color: 20

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 4565 Color: 285
Size: 2877 Color: 257
Size: 574 Color: 123

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 375
Size: 924 Color: 164
Size: 184 Color: 39

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 309
Size: 2172 Color: 233
Size: 432 Color: 100

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 322
Size: 1853 Color: 225
Size: 266 Color: 66

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5937 Color: 325
Size: 1765 Color: 219
Size: 314 Color: 79

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 359
Size: 1294 Color: 190
Size: 96 Color: 5

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 317
Size: 1804 Color: 224
Size: 360 Color: 91

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7010 Color: 383
Size: 914 Color: 161
Size: 92 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4612 Color: 288
Size: 2844 Color: 253
Size: 560 Color: 120

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 331
Size: 1596 Color: 210
Size: 312 Color: 78

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 361
Size: 1134 Color: 179
Size: 224 Color: 54

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6709 Color: 364
Size: 1091 Color: 175
Size: 216 Color: 50

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 400
Size: 692 Color: 139
Size: 136 Color: 13

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5614 Color: 314
Size: 2002 Color: 228
Size: 400 Color: 96

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4906 Color: 290
Size: 2594 Color: 249
Size: 516 Color: 118

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 342
Size: 1398 Color: 198
Size: 276 Color: 70

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 282
Size: 2934 Color: 260
Size: 584 Color: 126

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6405 Color: 345
Size: 1343 Color: 194
Size: 268 Color: 68

Bin 64: 0 of cap free
Amount of items: 4
Items: 
Size: 5023 Color: 293
Size: 2825 Color: 252
Size: 112 Color: 8
Size: 56 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 398
Size: 724 Color: 141
Size: 136 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 347
Size: 1339 Color: 193
Size: 266 Color: 67

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 367
Size: 1212 Color: 185
Size: 32 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 374
Size: 934 Color: 165
Size: 184 Color: 40

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 315
Size: 1932 Color: 227
Size: 384 Color: 94

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4577 Color: 287
Size: 2867 Color: 255
Size: 572 Color: 121

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 343
Size: 1356 Color: 196
Size: 264 Color: 64

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 344
Size: 1350 Color: 195
Size: 268 Color: 69

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5211 Color: 305
Size: 2339 Color: 237
Size: 466 Color: 106

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6591 Color: 356
Size: 1189 Color: 183
Size: 236 Color: 58

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 371
Size: 981 Color: 168
Size: 196 Color: 43

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 284
Size: 2884 Color: 258
Size: 576 Color: 124

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 365
Size: 1046 Color: 173
Size: 208 Color: 46

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 295
Size: 2460 Color: 245
Size: 488 Color: 112

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 321
Size: 1772 Color: 220
Size: 352 Color: 87

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4472 Color: 281
Size: 2968 Color: 261
Size: 576 Color: 125

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5567 Color: 312
Size: 2041 Color: 230
Size: 408 Color: 98

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4021 Color: 279
Size: 3331 Color: 264
Size: 664 Color: 133

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6805 Color: 369
Size: 1011 Color: 171
Size: 200 Color: 45

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4013 Color: 276
Size: 3337 Color: 266
Size: 666 Color: 135

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 3512 Color: 271
Size: 2976 Color: 262
Size: 1528 Color: 207

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7118 Color: 396
Size: 750 Color: 143
Size: 148 Color: 17

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 334
Size: 1500 Color: 204
Size: 296 Color: 73

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 333
Size: 1527 Color: 206
Size: 304 Color: 76

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6478 Color: 351
Size: 1282 Color: 189
Size: 256 Color: 62

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 323
Size: 1761 Color: 218
Size: 350 Color: 86

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 7093 Color: 393
Size: 865 Color: 157
Size: 58 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4012 Color: 275
Size: 3340 Color: 268
Size: 664 Color: 132

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 377
Size: 915 Color: 162
Size: 182 Color: 38

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 384
Size: 834 Color: 153
Size: 164 Color: 29

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4502 Color: 283
Size: 2930 Color: 259
Size: 584 Color: 127

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 330
Size: 1658 Color: 211
Size: 328 Color: 80

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 358
Size: 1164 Color: 181
Size: 232 Color: 56

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5965 Color: 327
Size: 1711 Color: 215
Size: 340 Color: 83

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5223 Color: 307
Size: 2329 Color: 235
Size: 464 Color: 105

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 306
Size: 2335 Color: 236
Size: 466 Color: 107

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 7047 Color: 386
Size: 809 Color: 152
Size: 160 Color: 28

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5119 Color: 300
Size: 2415 Color: 241
Size: 482 Color: 110

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 380
Size: 876 Color: 158
Size: 168 Color: 30

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 370
Size: 1004 Color: 170
Size: 200 Color: 44

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 311
Size: 2084 Color: 231
Size: 408 Color: 97

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 368
Size: 1057 Color: 174
Size: 156 Color: 24

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 341
Size: 1420 Color: 199
Size: 280 Color: 71

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 385
Size: 836 Color: 154
Size: 160 Color: 26

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 7057 Color: 388
Size: 801 Color: 151
Size: 158 Color: 25

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 339
Size: 1454 Color: 200
Size: 288 Color: 72

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 353
Size: 1226 Color: 188
Size: 244 Color: 61

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5855 Color: 318
Size: 1801 Color: 223
Size: 360 Color: 90

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 394
Size: 766 Color: 145
Size: 152 Color: 19

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 399
Size: 710 Color: 140
Size: 140 Color: 15

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5170 Color: 303
Size: 2374 Color: 239
Size: 472 Color: 108

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4017 Color: 278
Size: 3333 Color: 265
Size: 666 Color: 136

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 357
Size: 1186 Color: 182
Size: 236 Color: 57

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 378
Size: 901 Color: 160
Size: 180 Color: 35

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 5885 Color: 319
Size: 1777 Color: 222
Size: 354 Color: 89

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4010 Color: 274
Size: 3342 Color: 270
Size: 664 Color: 130

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 7081 Color: 391
Size: 781 Color: 147
Size: 154 Color: 21

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6995 Color: 381
Size: 851 Color: 156
Size: 170 Color: 33

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5115 Color: 299
Size: 2419 Color: 242
Size: 482 Color: 111

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 335
Size: 1491 Color: 202
Size: 298 Color: 75

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5605 Color: 313
Size: 2011 Color: 229
Size: 400 Color: 95

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 4962 Color: 291
Size: 2546 Color: 248
Size: 508 Color: 117

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 7113 Color: 395
Size: 753 Color: 144
Size: 150 Color: 18

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6231 Color: 336
Size: 1567 Color: 209
Size: 218 Color: 52

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 301
Size: 2685 Color: 250
Size: 208 Color: 49

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6999 Color: 382
Size: 849 Color: 155
Size: 168 Color: 31

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6915 Color: 376
Size: 919 Color: 163
Size: 182 Color: 37

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 397
Size: 731 Color: 142
Size: 144 Color: 16

Total size: 1058112
Total free space: 0

