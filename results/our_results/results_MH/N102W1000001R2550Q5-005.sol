Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 448222 Color: 2
Size: 299865 Color: 3
Size: 251914 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 344928 Color: 2
Size: 305357 Color: 3
Size: 349716 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 396866 Color: 0
Size: 327927 Color: 1
Size: 275208 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 413348 Color: 3
Size: 327603 Color: 3
Size: 259050 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 386227 Color: 2
Size: 355600 Color: 0
Size: 258174 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 431556 Color: 4
Size: 277708 Color: 4
Size: 290737 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 435064 Color: 2
Size: 292380 Color: 2
Size: 272557 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 456172 Color: 2
Size: 287403 Color: 0
Size: 256426 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 318410 Color: 2
Size: 380022 Color: 2
Size: 301569 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 355183 Color: 3
Size: 338446 Color: 1
Size: 306372 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 368898 Color: 0
Size: 336220 Color: 2
Size: 294883 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 375454 Color: 1
Size: 353385 Color: 1
Size: 271162 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 391492 Color: 2
Size: 334523 Color: 0
Size: 273986 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 386276 Color: 4
Size: 341406 Color: 1
Size: 272319 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 281671 Color: 1
Size: 342862 Color: 0
Size: 375468 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 305253 Color: 4
Size: 312292 Color: 1
Size: 382456 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 333530 Color: 3
Size: 280567 Color: 1
Size: 385904 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 372691 Color: 2
Size: 353242 Color: 1
Size: 274068 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 370138 Color: 1
Size: 304570 Color: 4
Size: 325293 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368454 Color: 4
Size: 257857 Color: 4
Size: 373690 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 379461 Color: 2
Size: 358917 Color: 4
Size: 261623 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 403348 Color: 4
Size: 328481 Color: 3
Size: 268172 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 403801 Color: 0
Size: 308554 Color: 4
Size: 287646 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 414609 Color: 2
Size: 292867 Color: 1
Size: 292525 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 416862 Color: 3
Size: 320576 Color: 1
Size: 262563 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 425593 Color: 3
Size: 296962 Color: 0
Size: 277446 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 431017 Color: 2
Size: 293801 Color: 2
Size: 275183 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 463593 Color: 1
Size: 279649 Color: 2
Size: 256759 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 457875 Color: 3
Size: 276168 Color: 0
Size: 265958 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 481387 Color: 1
Size: 268354 Color: 4
Size: 250260 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 474589 Color: 0
Size: 263284 Color: 2
Size: 262128 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 497075 Color: 1
Size: 252328 Color: 3
Size: 250598 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 497222 Color: 1
Size: 252518 Color: 2
Size: 250261 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 499744 Color: 4
Size: 250157 Color: 1
Size: 250100 Color: 2

Total size: 34000034
Total free space: 0

