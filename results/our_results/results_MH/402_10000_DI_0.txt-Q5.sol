Capicity Bin: 7552
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 2
Size: 3142 Color: 3
Size: 148 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 1
Size: 2078 Color: 3
Size: 412 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 0
Size: 2156 Color: 1
Size: 200 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 0
Size: 2079 Color: 4
Size: 174 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5411 Color: 3
Size: 1919 Color: 3
Size: 222 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5778 Color: 3
Size: 1554 Color: 2
Size: 220 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 4
Size: 1432 Color: 0
Size: 188 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 4
Size: 1314 Color: 0
Size: 264 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 4
Size: 1174 Color: 0
Size: 374 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 1
Size: 1130 Color: 0
Size: 392 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6152 Color: 0
Size: 1148 Color: 1
Size: 252 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 2
Size: 999 Color: 0
Size: 380 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6180 Color: 3
Size: 1244 Color: 1
Size: 128 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6198 Color: 4
Size: 1002 Color: 0
Size: 352 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 0
Size: 1181 Color: 3
Size: 158 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6237 Color: 4
Size: 1067 Color: 0
Size: 248 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 0
Size: 742 Color: 2
Size: 476 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 1
Size: 1043 Color: 2
Size: 208 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 0
Size: 794 Color: 4
Size: 416 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 2
Size: 724 Color: 0
Size: 488 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 0
Size: 1018 Color: 2
Size: 180 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 3
Size: 979 Color: 0
Size: 218 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 0
Size: 870 Color: 2
Size: 296 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 0
Size: 999 Color: 3
Size: 110 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6444 Color: 0
Size: 560 Color: 3
Size: 548 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 0
Size: 1043 Color: 2
Size: 44 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6467 Color: 0
Size: 773 Color: 1
Size: 312 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 2
Size: 885 Color: 0
Size: 160 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6510 Color: 1
Size: 802 Color: 1
Size: 240 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 4
Size: 796 Color: 1
Size: 230 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 4
Size: 751 Color: 3
Size: 234 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 1
Size: 684 Color: 0
Size: 280 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6592 Color: 3
Size: 652 Color: 1
Size: 308 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 2
Size: 757 Color: 0
Size: 194 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 2
Size: 800 Color: 4
Size: 150 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 0
Size: 654 Color: 2
Size: 232 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 2
Size: 754 Color: 0
Size: 148 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6693 Color: 0
Size: 717 Color: 2
Size: 142 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 1
Size: 698 Color: 0
Size: 196 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 0
Size: 638 Color: 2
Size: 200 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6754 Color: 0
Size: 540 Color: 1
Size: 258 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 0
Size: 556 Color: 3
Size: 226 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 0
Size: 554 Color: 1
Size: 226 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6790 Color: 1
Size: 608 Color: 4
Size: 154 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 4214 Color: 2
Size: 3145 Color: 4
Size: 192 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 4970 Color: 2
Size: 1879 Color: 0
Size: 702 Color: 3

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5262 Color: 4
Size: 1665 Color: 0
Size: 624 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 4
Size: 1281 Color: 0
Size: 715 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5799 Color: 1
Size: 1524 Color: 0
Size: 228 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 5858 Color: 0
Size: 1565 Color: 3
Size: 128 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 5927 Color: 0
Size: 1292 Color: 4
Size: 332 Color: 3

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 3
Size: 1081 Color: 1
Size: 414 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6137 Color: 0
Size: 1102 Color: 2
Size: 312 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 0
Size: 905 Color: 2
Size: 500 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 2
Size: 1299 Color: 1
Size: 136 Color: 0

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 0
Size: 862 Color: 3
Size: 278 Color: 4

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6419 Color: 2
Size: 628 Color: 0
Size: 504 Color: 4

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 6434 Color: 3
Size: 1117 Color: 1

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 6539 Color: 4
Size: 1012 Color: 2

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 6541 Color: 1
Size: 746 Color: 2
Size: 264 Color: 0

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 6625 Color: 4
Size: 528 Color: 0
Size: 398 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 6695 Color: 1
Size: 600 Color: 0
Size: 256 Color: 3

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 0
Size: 2255 Color: 3
Size: 132 Color: 4

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 0
Size: 1598 Color: 3
Size: 292 Color: 1

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 5772 Color: 0
Size: 1642 Color: 4
Size: 136 Color: 3

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 0
Size: 1160 Color: 4
Size: 450 Color: 2

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 5951 Color: 0
Size: 1399 Color: 4
Size: 200 Color: 4

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 5995 Color: 3
Size: 1555 Color: 1

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 6015 Color: 2
Size: 1461 Color: 1
Size: 74 Color: 4

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 6345 Color: 2
Size: 1205 Color: 1

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 3
Size: 858 Color: 1

Bin 72: 3 of cap free
Amount of items: 11
Items: 
Size: 1137 Color: 1
Size: 1010 Color: 0
Size: 1007 Color: 0
Size: 843 Color: 4
Size: 820 Color: 4
Size: 628 Color: 2
Size: 624 Color: 1
Size: 480 Color: 0
Size: 456 Color: 3
Size: 296 Color: 2
Size: 248 Color: 4

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 4217 Color: 1
Size: 3148 Color: 2
Size: 184 Color: 4

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 4828 Color: 0
Size: 2597 Color: 1
Size: 124 Color: 4

Bin 75: 3 of cap free
Amount of items: 2
Items: 
Size: 4847 Color: 1
Size: 2702 Color: 4

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 1
Size: 2298 Color: 4

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 3
Size: 1991 Color: 0
Size: 136 Color: 1

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 1
Size: 945 Color: 4

Bin 79: 4 of cap free
Amount of items: 5
Items: 
Size: 3777 Color: 0
Size: 1395 Color: 4
Size: 1348 Color: 4
Size: 628 Color: 4
Size: 400 Color: 2

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 4193 Color: 2
Size: 3147 Color: 3
Size: 208 Color: 1

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 3
Size: 1580 Color: 0
Size: 382 Color: 1

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 1746 Color: 0
Size: 112 Color: 1

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 6230 Color: 4
Size: 1318 Color: 2

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 1
Size: 1048 Color: 3

Bin 85: 5 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 0
Size: 2159 Color: 4
Size: 416 Color: 3

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 4
Size: 1151 Color: 1

Bin 87: 6 of cap free
Amount of items: 3
Items: 
Size: 4437 Color: 0
Size: 2781 Color: 3
Size: 328 Color: 2

Bin 88: 6 of cap free
Amount of items: 3
Items: 
Size: 4686 Color: 3
Size: 2524 Color: 0
Size: 336 Color: 1

Bin 89: 6 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 1
Size: 974 Color: 3

Bin 90: 7 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 4
Size: 1647 Color: 3
Size: 48 Color: 4

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 1
Size: 2276 Color: 0
Size: 232 Color: 1

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 6740 Color: 4
Size: 804 Color: 2

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 6060 Color: 2
Size: 1482 Color: 1

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 4
Size: 1176 Color: 0
Size: 793 Color: 1

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 5577 Color: 2
Size: 1964 Color: 1

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 4798 Color: 1
Size: 2742 Color: 3

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 0
Size: 1778 Color: 4
Size: 208 Color: 3

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 6379 Color: 4
Size: 1161 Color: 3

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 6718 Color: 4
Size: 821 Color: 3

Bin 100: 14 of cap free
Amount of items: 3
Items: 
Size: 4310 Color: 1
Size: 3020 Color: 0
Size: 208 Color: 2

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 5148 Color: 4
Size: 2390 Color: 2

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 6518 Color: 3
Size: 1020 Color: 4

Bin 103: 15 of cap free
Amount of items: 3
Items: 
Size: 3786 Color: 4
Size: 2654 Color: 3
Size: 1097 Color: 0

Bin 104: 15 of cap free
Amount of items: 3
Items: 
Size: 4963 Color: 4
Size: 2502 Color: 2
Size: 72 Color: 0

Bin 105: 16 of cap free
Amount of items: 3
Items: 
Size: 4550 Color: 3
Size: 1910 Color: 1
Size: 1076 Color: 0

Bin 106: 18 of cap free
Amount of items: 3
Items: 
Size: 3779 Color: 0
Size: 3237 Color: 2
Size: 518 Color: 3

Bin 107: 18 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 2
Size: 2782 Color: 4
Size: 266 Color: 0

Bin 108: 21 of cap free
Amount of items: 2
Items: 
Size: 5879 Color: 1
Size: 1652 Color: 3

Bin 109: 22 of cap free
Amount of items: 2
Items: 
Size: 6046 Color: 0
Size: 1484 Color: 2

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 2
Size: 884 Color: 1

Bin 111: 24 of cap free
Amount of items: 2
Items: 
Size: 6594 Color: 3
Size: 934 Color: 1

Bin 112: 26 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 3
Size: 1258 Color: 4

Bin 113: 27 of cap free
Amount of items: 2
Items: 
Size: 6321 Color: 1
Size: 1204 Color: 2

Bin 114: 28 of cap free
Amount of items: 2
Items: 
Size: 6189 Color: 4
Size: 1335 Color: 2

Bin 115: 28 of cap free
Amount of items: 2
Items: 
Size: 6653 Color: 1
Size: 871 Color: 4

Bin 116: 31 of cap free
Amount of items: 2
Items: 
Size: 6107 Color: 3
Size: 1414 Color: 2

Bin 117: 32 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 2
Size: 2660 Color: 1
Size: 928 Color: 0

Bin 118: 32 of cap free
Amount of items: 2
Items: 
Size: 5420 Color: 2
Size: 2100 Color: 1

Bin 119: 35 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 3
Size: 1785 Color: 4

Bin 120: 35 of cap free
Amount of items: 2
Items: 
Size: 6161 Color: 3
Size: 1356 Color: 4

Bin 121: 36 of cap free
Amount of items: 2
Items: 
Size: 3780 Color: 3
Size: 3736 Color: 2

Bin 122: 39 of cap free
Amount of items: 2
Items: 
Size: 5789 Color: 3
Size: 1724 Color: 1

Bin 123: 42 of cap free
Amount of items: 3
Items: 
Size: 3778 Color: 2
Size: 2808 Color: 1
Size: 924 Color: 0

Bin 124: 48 of cap free
Amount of items: 30
Items: 
Size: 448 Color: 4
Size: 430 Color: 0
Size: 424 Color: 4
Size: 412 Color: 2
Size: 356 Color: 4
Size: 336 Color: 3
Size: 328 Color: 1
Size: 324 Color: 3
Size: 320 Color: 0
Size: 292 Color: 2
Size: 268 Color: 2
Size: 260 Color: 0
Size: 256 Color: 1
Size: 224 Color: 4
Size: 224 Color: 3
Size: 224 Color: 2
Size: 200 Color: 0
Size: 198 Color: 0
Size: 184 Color: 2
Size: 172 Color: 1
Size: 172 Color: 1
Size: 168 Color: 4
Size: 168 Color: 2
Size: 168 Color: 2
Size: 164 Color: 4
Size: 160 Color: 3
Size: 160 Color: 0
Size: 156 Color: 3
Size: 156 Color: 0
Size: 152 Color: 2

Bin 125: 50 of cap free
Amount of items: 2
Items: 
Size: 5059 Color: 2
Size: 2443 Color: 4

Bin 126: 56 of cap free
Amount of items: 2
Items: 
Size: 5492 Color: 3
Size: 2004 Color: 2

Bin 127: 62 of cap free
Amount of items: 2
Items: 
Size: 4524 Color: 0
Size: 2966 Color: 3

Bin 128: 64 of cap free
Amount of items: 3
Items: 
Size: 4364 Color: 0
Size: 2458 Color: 2
Size: 666 Color: 0

Bin 129: 65 of cap free
Amount of items: 2
Items: 
Size: 5675 Color: 4
Size: 1812 Color: 3

Bin 130: 76 of cap free
Amount of items: 2
Items: 
Size: 4164 Color: 3
Size: 3312 Color: 0

Bin 131: 88 of cap free
Amount of items: 2
Items: 
Size: 4318 Color: 3
Size: 3146 Color: 4

Bin 132: 103 of cap free
Amount of items: 2
Items: 
Size: 4621 Color: 4
Size: 2828 Color: 2

Bin 133: 6122 of cap free
Amount of items: 11
Items: 
Size: 150 Color: 1
Size: 148 Color: 1
Size: 144 Color: 4
Size: 142 Color: 4
Size: 136 Color: 1
Size: 128 Color: 2
Size: 128 Color: 2
Size: 124 Color: 4
Size: 122 Color: 0
Size: 104 Color: 0
Size: 104 Color: 0

Total size: 996864
Total free space: 7552

