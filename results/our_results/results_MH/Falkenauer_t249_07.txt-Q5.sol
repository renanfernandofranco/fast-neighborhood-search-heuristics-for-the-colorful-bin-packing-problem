Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 358 Color: 1
Size: 259 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 3
Size: 340 Color: 3
Size: 264 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 3
Size: 344 Color: 1
Size: 304 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 354 Color: 4
Size: 254 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 370 Color: 2
Size: 253 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 363 Color: 3
Size: 254 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 321 Color: 1
Size: 305 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 291 Color: 1
Size: 291 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 361 Color: 4
Size: 259 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 350 Color: 0
Size: 254 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 319 Color: 3
Size: 297 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 335 Color: 1
Size: 253 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 253 Color: 2
Size: 252 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 270 Color: 1
Size: 260 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 270 Color: 0
Size: 268 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 259 Color: 2
Size: 255 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 252 Color: 2
Size: 250 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 303 Color: 4
Size: 277 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 309 Color: 4
Size: 274 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 313 Color: 2
Size: 275 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 313 Color: 2
Size: 283 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 284 Color: 1
Size: 256 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 266 Color: 0
Size: 258 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 347 Color: 1
Size: 285 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 324 Color: 0
Size: 279 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 322 Color: 3
Size: 253 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 307 Color: 4
Size: 304 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 258 Color: 1
Size: 254 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 0
Size: 314 Color: 3
Size: 256 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 289 Color: 1
Size: 262 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 4
Size: 304 Color: 0
Size: 266 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 355 Color: 4
Size: 254 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 350 Color: 3
Size: 258 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 318 Color: 0
Size: 287 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 287 Color: 1
Size: 283 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 261 Color: 3
Size: 250 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 2
Size: 273 Color: 0
Size: 259 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 355 Color: 1
Size: 266 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 301 Color: 1
Size: 266 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 262 Color: 4
Size: 250 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 292 Color: 3
Size: 261 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 252 Color: 0
Size: 250 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 284 Color: 1
Size: 255 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 335 Color: 2
Size: 258 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 271 Color: 4
Size: 261 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 284 Color: 1
Size: 254 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 295 Color: 2
Size: 255 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 252 Color: 1
Size: 251 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 324 Color: 3
Size: 296 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 283 Color: 4
Size: 266 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 303 Color: 1
Size: 278 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 4
Size: 319 Color: 0
Size: 266 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 305 Color: 1
Size: 268 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 271 Color: 3
Size: 264 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 282 Color: 0
Size: 262 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 296 Color: 1
Size: 278 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 278 Color: 0
Size: 250 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 280 Color: 1
Size: 271 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 327 Color: 1
Size: 254 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 332 Color: 3
Size: 267 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 271 Color: 2
Size: 261 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 277 Color: 4
Size: 253 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 336 Color: 2
Size: 258 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 276 Color: 4
Size: 256 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 2
Size: 341 Color: 2
Size: 300 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 347 Color: 4
Size: 276 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 254 Color: 1
Size: 252 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 268 Color: 0
Size: 252 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 299 Color: 0
Size: 296 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 331 Color: 4
Size: 258 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 251 Color: 2
Size: 250 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 2
Size: 277 Color: 1
Size: 255 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 303 Color: 2
Size: 261 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 284 Color: 3
Size: 256 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 330 Color: 0
Size: 289 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 337 Color: 3
Size: 292 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 292 Color: 2
Size: 264 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 304 Color: 4
Size: 279 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 269 Color: 0
Size: 260 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 281 Color: 3
Size: 276 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 365 Color: 2
Size: 259 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 365 Color: 1
Size: 260 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 298 Color: 2
Size: 262 Color: 0

Total size: 83000
Total free space: 0

