Capicity Bin: 6624
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 325
Size: 1428 Color: 225
Size: 70 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5162 Color: 328
Size: 1124 Color: 207
Size: 338 Color: 108

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 333
Size: 1204 Color: 212
Size: 144 Color: 35

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5282 Color: 334
Size: 902 Color: 186
Size: 440 Color: 121

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5298 Color: 335
Size: 1122 Color: 206
Size: 204 Color: 69

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5431 Color: 344
Size: 995 Color: 198
Size: 198 Color: 67

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 346
Size: 988 Color: 197
Size: 178 Color: 57

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5470 Color: 348
Size: 782 Color: 169
Size: 372 Color: 110

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5552 Color: 354
Size: 818 Color: 175
Size: 254 Color: 88

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5553 Color: 355
Size: 687 Color: 157
Size: 384 Color: 113

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 360
Size: 742 Color: 166
Size: 280 Color: 95

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5605 Color: 361
Size: 851 Color: 178
Size: 168 Color: 50

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 362
Size: 686 Color: 156
Size: 332 Color: 105

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5665 Color: 365
Size: 739 Color: 165
Size: 220 Color: 76

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 366
Size: 666 Color: 152
Size: 292 Color: 98

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5669 Color: 367
Size: 721 Color: 161
Size: 234 Color: 81

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 368
Size: 548 Color: 133
Size: 392 Color: 114

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 376
Size: 674 Color: 154
Size: 194 Color: 65

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5778 Color: 380
Size: 722 Color: 162
Size: 124 Color: 22

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 381
Size: 544 Color: 131
Size: 300 Color: 100

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5826 Color: 386
Size: 662 Color: 151
Size: 136 Color: 30

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5851 Color: 389
Size: 645 Color: 150
Size: 128 Color: 24

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 390
Size: 588 Color: 142
Size: 184 Color: 60

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 392
Size: 643 Color: 148
Size: 94 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 393
Size: 613 Color: 146
Size: 122 Color: 21

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5919 Color: 397
Size: 589 Color: 143
Size: 116 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5924 Color: 398
Size: 492 Color: 128
Size: 208 Color: 71

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5927 Color: 399
Size: 581 Color: 140
Size: 116 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5930 Color: 400
Size: 550 Color: 137
Size: 144 Color: 38

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 401
Size: 476 Color: 124
Size: 216 Color: 74

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5935 Color: 402
Size: 575 Color: 138
Size: 114 Color: 16

Bin 32: 1 of cap free
Amount of items: 5
Items: 
Size: 3322 Color: 278
Size: 2753 Color: 264
Size: 220 Color: 75
Size: 168 Color: 49
Size: 160 Color: 48

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 3325 Color: 279
Size: 3138 Color: 271
Size: 160 Color: 47

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 285
Size: 2754 Color: 265
Size: 144 Color: 36

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 4907 Color: 314
Size: 1430 Color: 226
Size: 286 Color: 97

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 321
Size: 1196 Color: 211
Size: 360 Color: 109

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 323
Size: 1378 Color: 222
Size: 150 Color: 40

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 5331 Color: 337
Size: 1292 Color: 219

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 5444 Color: 345
Size: 1179 Color: 210

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5515 Color: 350
Size: 962 Color: 193
Size: 146 Color: 39

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 5577 Color: 357
Size: 1046 Color: 202

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 5646 Color: 364
Size: 977 Color: 196

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 5709 Color: 371
Size: 914 Color: 188

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 5742 Color: 375
Size: 881 Color: 184

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 5765 Color: 379
Size: 858 Color: 181

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 388
Size: 548 Color: 134
Size: 232 Color: 80

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 5906 Color: 395
Size: 717 Color: 160

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 4123 Color: 294
Size: 2383 Color: 254
Size: 116 Color: 19

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 296
Size: 2244 Color: 251
Size: 112 Color: 14

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 5239 Color: 331
Size: 1383 Color: 223

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 351
Size: 1106 Color: 205

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 5702 Color: 370
Size: 920 Color: 189

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 5834 Color: 387
Size: 788 Color: 170

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 5898 Color: 394
Size: 724 Color: 163

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 5914 Color: 396
Size: 708 Color: 159

Bin 56: 3 of cap free
Amount of items: 5
Items: 
Size: 3317 Color: 276
Size: 2732 Color: 262
Size: 228 Color: 79
Size: 174 Color: 54
Size: 170 Color: 53

Bin 57: 3 of cap free
Amount of items: 5
Items: 
Size: 3348 Color: 281
Size: 2761 Color: 267
Size: 200 Color: 68
Size: 156 Color: 44
Size: 156 Color: 43

Bin 58: 3 of cap free
Amount of items: 5
Items: 
Size: 3753 Color: 286
Size: 2302 Color: 253
Size: 284 Color: 96
Size: 142 Color: 34
Size: 140 Color: 33

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 327
Size: 755 Color: 167
Size: 706 Color: 158

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 5356 Color: 339
Size: 1265 Color: 217

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 5371 Color: 341
Size: 1250 Color: 216

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 5397 Color: 343
Size: 1224 Color: 214

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 5801 Color: 382
Size: 820 Color: 176

Bin 64: 4 of cap free
Amount of items: 5
Items: 
Size: 3321 Color: 277
Size: 2751 Color: 263
Size: 212 Color: 72
Size: 168 Color: 52
Size: 168 Color: 51

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 4974 Color: 318
Size: 1606 Color: 233
Size: 40 Color: 1

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 5211 Color: 330
Size: 1409 Color: 224

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 5546 Color: 353
Size: 1074 Color: 203

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 5597 Color: 359
Size: 1023 Color: 199

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 5818 Color: 385
Size: 802 Color: 173

Bin 70: 5 of cap free
Amount of items: 5
Items: 
Size: 3330 Color: 280
Size: 2757 Color: 266
Size: 214 Color: 73
Size: 160 Color: 46
Size: 158 Color: 45

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 4935 Color: 317
Size: 1644 Color: 234
Size: 40 Color: 2

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 5188 Color: 329
Size: 1431 Color: 227

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 5715 Color: 372
Size: 904 Color: 187

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 5762 Color: 378
Size: 857 Color: 180

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 4338 Color: 298
Size: 2280 Color: 252

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 5396 Color: 342
Size: 1222 Color: 213

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 5459 Color: 347
Size: 1159 Color: 209

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 363
Size: 974 Color: 195

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 5817 Color: 384
Size: 801 Color: 172

Bin 80: 7 of cap free
Amount of items: 7
Items: 
Size: 3314 Color: 274
Size: 738 Color: 164
Size: 680 Color: 155
Size: 673 Color: 153
Size: 644 Color: 149
Size: 392 Color: 116
Size: 176 Color: 55

Bin 81: 7 of cap free
Amount of items: 2
Items: 
Size: 4811 Color: 311
Size: 1806 Color: 239

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 5107 Color: 324
Size: 1510 Color: 229

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 5858 Color: 391
Size: 759 Color: 168

Bin 84: 8 of cap free
Amount of items: 23
Items: 
Size: 420 Color: 118
Size: 416 Color: 117
Size: 392 Color: 115
Size: 380 Color: 112
Size: 376 Color: 111
Size: 336 Color: 106
Size: 328 Color: 104
Size: 320 Color: 103
Size: 312 Color: 102
Size: 306 Color: 101
Size: 296 Color: 99
Size: 280 Color: 94
Size: 272 Color: 93
Size: 272 Color: 92
Size: 240 Color: 83
Size: 240 Color: 82
Size: 226 Color: 78
Size: 224 Color: 77
Size: 208 Color: 70
Size: 196 Color: 66
Size: 192 Color: 64
Size: 192 Color: 63
Size: 192 Color: 62

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 4748 Color: 309
Size: 1868 Color: 241

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 5138 Color: 326
Size: 1478 Color: 228

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 5471 Color: 349
Size: 1145 Color: 208

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 5739 Color: 374
Size: 877 Color: 183

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 5816 Color: 383
Size: 800 Color: 171

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 5690 Color: 369
Size: 925 Color: 191

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 5045 Color: 320
Size: 1569 Color: 232

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 5569 Color: 356
Size: 1045 Color: 201

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 5738 Color: 373
Size: 876 Color: 182

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 307
Size: 1883 Color: 242
Size: 78 Color: 6

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 5338 Color: 338
Size: 1275 Color: 218

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 5759 Color: 377
Size: 854 Color: 179

Bin 97: 12 of cap free
Amount of items: 4
Items: 
Size: 3866 Color: 289
Size: 2482 Color: 258
Size: 132 Color: 28
Size: 132 Color: 27

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 5370 Color: 340
Size: 1242 Color: 215

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 5076 Color: 322
Size: 1535 Color: 230

Bin 100: 14 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 312
Size: 1724 Color: 238
Size: 72 Color: 5

Bin 101: 15 of cap free
Amount of items: 5
Items: 
Size: 4540 Color: 302
Size: 826 Color: 177
Size: 811 Color: 174
Size: 336 Color: 107
Size: 96 Color: 10

Bin 102: 15 of cap free
Amount of items: 2
Items: 
Size: 4916 Color: 316
Size: 1693 Color: 237

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 5530 Color: 352
Size: 1079 Color: 204

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 5580 Color: 358
Size: 1028 Color: 200

Bin 105: 17 of cap free
Amount of items: 3
Items: 
Size: 4062 Color: 292
Size: 2417 Color: 256
Size: 128 Color: 23

Bin 106: 17 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 332
Size: 1356 Color: 221

Bin 107: 18 of cap free
Amount of items: 3
Items: 
Size: 3844 Color: 288
Size: 2628 Color: 261
Size: 134 Color: 29

Bin 108: 18 of cap free
Amount of items: 3
Items: 
Size: 4391 Color: 300
Size: 2111 Color: 247
Size: 104 Color: 11

Bin 109: 20 of cap free
Amount of items: 2
Items: 
Size: 4698 Color: 308
Size: 1906 Color: 243

Bin 110: 21 of cap free
Amount of items: 2
Items: 
Size: 5304 Color: 336
Size: 1299 Color: 220

Bin 111: 22 of cap free
Amount of items: 3
Items: 
Size: 4093 Color: 293
Size: 2393 Color: 255
Size: 116 Color: 20

Bin 112: 22 of cap free
Amount of items: 2
Items: 
Size: 4598 Color: 305
Size: 2004 Color: 245

Bin 113: 24 of cap free
Amount of items: 3
Items: 
Size: 3964 Color: 291
Size: 2508 Color: 260
Size: 128 Color: 25

Bin 114: 24 of cap free
Amount of items: 3
Items: 
Size: 4783 Color: 310
Size: 924 Color: 190
Size: 893 Color: 185

Bin 115: 24 of cap free
Amount of items: 2
Items: 
Size: 4910 Color: 315
Size: 1690 Color: 236

Bin 116: 26 of cap free
Amount of items: 3
Items: 
Size: 3650 Color: 284
Size: 2804 Color: 269
Size: 144 Color: 37

Bin 117: 27 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 313
Size: 1671 Color: 235
Size: 72 Color: 4

Bin 118: 28 of cap free
Amount of items: 2
Items: 
Size: 4458 Color: 301
Size: 2138 Color: 248

Bin 119: 30 of cap free
Amount of items: 3
Items: 
Size: 4300 Color: 297
Size: 2182 Color: 249
Size: 112 Color: 13

Bin 120: 30 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 319
Size: 1564 Color: 231
Size: 26 Color: 0

Bin 121: 32 of cap free
Amount of items: 4
Items: 
Size: 4564 Color: 303
Size: 971 Color: 194
Size: 961 Color: 192
Size: 96 Color: 9

Bin 122: 44 of cap free
Amount of items: 2
Items: 
Size: 3316 Color: 275
Size: 3264 Color: 272

Bin 123: 52 of cap free
Amount of items: 3
Items: 
Size: 3940 Color: 290
Size: 2504 Color: 259
Size: 128 Color: 26

Bin 124: 54 of cap free
Amount of items: 3
Items: 
Size: 4621 Color: 306
Size: 1861 Color: 240
Size: 88 Color: 7

Bin 125: 64 of cap free
Amount of items: 3
Items: 
Size: 4228 Color: 295
Size: 2220 Color: 250
Size: 112 Color: 15

Bin 126: 70 of cap free
Amount of items: 3
Items: 
Size: 4365 Color: 299
Size: 2085 Color: 246
Size: 104 Color: 12

Bin 127: 78 of cap free
Amount of items: 3
Items: 
Size: 3632 Color: 283
Size: 2762 Color: 268
Size: 152 Color: 41

Bin 128: 91 of cap free
Amount of items: 2
Items: 
Size: 4593 Color: 304
Size: 1940 Color: 244

Bin 129: 103 of cap free
Amount of items: 4
Items: 
Size: 3765 Color: 287
Size: 2480 Color: 257
Size: 140 Color: 32
Size: 136 Color: 31

Bin 130: 108 of cap free
Amount of items: 3
Items: 
Size: 3476 Color: 282
Size: 2888 Color: 270
Size: 152 Color: 42

Bin 131: 135 of cap free
Amount of items: 7
Items: 
Size: 3313 Color: 273
Size: 642 Color: 147
Size: 602 Color: 145
Size: 594 Color: 144
Size: 582 Color: 141
Size: 580 Color: 139
Size: 176 Color: 56

Bin 132: 172 of cap free
Amount of items: 15
Items: 
Size: 550 Color: 136
Size: 550 Color: 135
Size: 548 Color: 132
Size: 520 Color: 130
Size: 504 Color: 129
Size: 488 Color: 127
Size: 482 Color: 126
Size: 478 Color: 125
Size: 456 Color: 123
Size: 456 Color: 122
Size: 440 Color: 120
Size: 424 Color: 119
Size: 192 Color: 61
Size: 184 Color: 59
Size: 180 Color: 58

Bin 133: 4858 of cap free
Amount of items: 7
Items: 
Size: 264 Color: 91
Size: 258 Color: 90
Size: 256 Color: 89
Size: 252 Color: 87
Size: 248 Color: 86
Size: 244 Color: 85
Size: 244 Color: 84

Total size: 874368
Total free space: 6624

