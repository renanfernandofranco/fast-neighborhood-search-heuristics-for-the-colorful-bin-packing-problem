Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 427666 Color: 1
Size: 303427 Color: 1
Size: 268908 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 420763 Color: 1
Size: 298369 Color: 1
Size: 280869 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 496892 Color: 1
Size: 252145 Color: 1
Size: 250964 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 373187 Color: 1
Size: 368590 Color: 1
Size: 258224 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 481374 Color: 1
Size: 262435 Color: 1
Size: 256192 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 420081 Color: 1
Size: 317351 Color: 1
Size: 262569 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 486294 Color: 1
Size: 259688 Color: 1
Size: 254019 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 496337 Color: 1
Size: 252070 Color: 1
Size: 251594 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 409233 Color: 1
Size: 317406 Color: 1
Size: 273362 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 433288 Color: 1
Size: 306911 Color: 1
Size: 259802 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 462436 Color: 1
Size: 276785 Color: 1
Size: 260780 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 489552 Color: 1
Size: 255508 Color: 1
Size: 254941 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 419497 Color: 1
Size: 304859 Color: 0
Size: 275645 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 462496 Color: 1
Size: 285763 Color: 1
Size: 251742 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 441611 Color: 1
Size: 280472 Color: 1
Size: 277918 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 418349 Color: 1
Size: 319510 Color: 1
Size: 262142 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 468194 Color: 1
Size: 273772 Color: 1
Size: 258035 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 368035 Color: 1
Size: 360943 Color: 1
Size: 271023 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 448705 Color: 1
Size: 278782 Color: 0
Size: 272514 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 358426 Color: 1
Size: 330952 Color: 1
Size: 310623 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 403710 Color: 1
Size: 318976 Color: 1
Size: 277315 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 493737 Color: 1
Size: 253200 Color: 1
Size: 253064 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 485617 Color: 1
Size: 258865 Color: 1
Size: 255519 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 387894 Color: 1
Size: 341929 Color: 1
Size: 270178 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 482601 Color: 1
Size: 263042 Color: 1
Size: 254358 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 475840 Color: 1
Size: 265889 Color: 1
Size: 258272 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 417432 Color: 1
Size: 316689 Color: 0
Size: 265880 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 412464 Color: 1
Size: 317298 Color: 1
Size: 270239 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 383503 Color: 1
Size: 326019 Color: 1
Size: 290479 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 366626 Color: 1
Size: 329431 Color: 1
Size: 303944 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 443184 Color: 1
Size: 300570 Color: 1
Size: 256247 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 472074 Color: 1
Size: 277205 Color: 1
Size: 250722 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 404276 Color: 1
Size: 297872 Color: 1
Size: 297853 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 418120 Color: 1
Size: 305120 Color: 1
Size: 276761 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 407014 Color: 1
Size: 332381 Color: 1
Size: 260606 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 460146 Color: 1
Size: 282534 Color: 1
Size: 257321 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 404198 Color: 1
Size: 299678 Color: 1
Size: 296125 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 363018 Color: 1
Size: 348606 Color: 1
Size: 288377 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 487569 Color: 1
Size: 259102 Color: 1
Size: 253330 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 481908 Color: 1
Size: 266009 Color: 1
Size: 252084 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 468647 Color: 1
Size: 274466 Color: 1
Size: 256888 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 391326 Color: 1
Size: 305282 Color: 1
Size: 303393 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 483805 Color: 1
Size: 265295 Color: 1
Size: 250901 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 427075 Color: 1
Size: 317839 Color: 1
Size: 255087 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 378927 Color: 1
Size: 312733 Color: 1
Size: 308341 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 395777 Color: 1
Size: 307074 Color: 1
Size: 297150 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 495533 Color: 1
Size: 252916 Color: 1
Size: 251552 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 371598 Color: 1
Size: 348246 Color: 1
Size: 280157 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 496567 Color: 1
Size: 252699 Color: 1
Size: 250735 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 382794 Color: 1
Size: 310726 Color: 1
Size: 306481 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 398472 Color: 1
Size: 340089 Color: 1
Size: 261440 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 476986 Color: 1
Size: 264121 Color: 1
Size: 258894 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 460140 Color: 1
Size: 281908 Color: 1
Size: 257953 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 475081 Color: 1
Size: 270206 Color: 1
Size: 254714 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 411549 Color: 1
Size: 297873 Color: 1
Size: 290579 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 457151 Color: 1
Size: 279768 Color: 1
Size: 263082 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 495540 Color: 1
Size: 254044 Color: 1
Size: 250417 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 424532 Color: 1
Size: 311873 Color: 1
Size: 263596 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 434382 Color: 1
Size: 294385 Color: 1
Size: 271234 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 420616 Color: 1
Size: 316750 Color: 1
Size: 262635 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 418782 Color: 1
Size: 313180 Color: 1
Size: 268039 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 414347 Color: 1
Size: 313038 Color: 1
Size: 272616 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 496136 Color: 1
Size: 253475 Color: 1
Size: 250390 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 493577 Color: 1
Size: 253831 Color: 1
Size: 252593 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 380443 Color: 1
Size: 327668 Color: 1
Size: 291890 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 443375 Color: 1
Size: 283735 Color: 1
Size: 272891 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 432244 Color: 1
Size: 289802 Color: 1
Size: 277955 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 371510 Color: 1
Size: 354508 Color: 1
Size: 273983 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 368307 Color: 1
Size: 365035 Color: 1
Size: 266659 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 421536 Color: 1
Size: 291007 Color: 1
Size: 287458 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 467461 Color: 1
Size: 274912 Color: 1
Size: 257628 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 389055 Color: 1
Size: 306779 Color: 1
Size: 304167 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 337754 Color: 1
Size: 337149 Color: 1
Size: 325098 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 384965 Color: 1
Size: 330120 Color: 1
Size: 284916 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 360123 Color: 1
Size: 352766 Color: 1
Size: 287112 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 385219 Color: 1
Size: 340104 Color: 1
Size: 274678 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 495492 Color: 1
Size: 253660 Color: 1
Size: 250849 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 375006 Color: 1
Size: 316151 Color: 0
Size: 308844 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 404846 Color: 1
Size: 336671 Color: 1
Size: 258484 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 407817 Color: 1
Size: 326569 Color: 1
Size: 265615 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 424291 Color: 1
Size: 313292 Color: 1
Size: 262418 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 421251 Color: 1
Size: 302245 Color: 1
Size: 276505 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 438988 Color: 1
Size: 289170 Color: 1
Size: 271843 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 435913 Color: 1
Size: 311785 Color: 1
Size: 252303 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 400825 Color: 1
Size: 323074 Color: 1
Size: 276102 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 368154 Color: 1
Size: 350184 Color: 1
Size: 281663 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 495642 Color: 1
Size: 253496 Color: 1
Size: 250863 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 378143 Color: 1
Size: 365201 Color: 1
Size: 256657 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 367233 Color: 1
Size: 343901 Color: 1
Size: 288867 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 369626 Color: 1
Size: 359546 Color: 1
Size: 270829 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 411501 Color: 1
Size: 325428 Color: 1
Size: 263072 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 444972 Color: 1
Size: 287511 Color: 1
Size: 267518 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 461686 Color: 1
Size: 277907 Color: 1
Size: 260408 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 367170 Color: 1
Size: 332232 Color: 1
Size: 300599 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 493427 Color: 1
Size: 255414 Color: 1
Size: 251160 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 392230 Color: 1
Size: 303886 Color: 1
Size: 303885 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 460410 Color: 1
Size: 279976 Color: 1
Size: 259615 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 473795 Color: 1
Size: 263348 Color: 1
Size: 262858 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 480780 Color: 1
Size: 260897 Color: 1
Size: 258324 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 387330 Color: 1
Size: 360174 Color: 1
Size: 252497 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 462962 Color: 1
Size: 272771 Color: 1
Size: 264268 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 441196 Color: 1
Size: 279666 Color: 0
Size: 279139 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 431710 Color: 1
Size: 286818 Color: 1
Size: 281473 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 455292 Color: 1
Size: 275452 Color: 0
Size: 269257 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 485813 Color: 1
Size: 258711 Color: 1
Size: 255477 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 469100 Color: 1
Size: 280137 Color: 1
Size: 250764 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 454566 Color: 1
Size: 286389 Color: 1
Size: 259046 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 474156 Color: 1
Size: 269013 Color: 1
Size: 256832 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 480932 Color: 1
Size: 263392 Color: 1
Size: 255677 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 407044 Color: 1
Size: 314683 Color: 1
Size: 278274 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 495434 Color: 1
Size: 253009 Color: 1
Size: 251558 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 488434 Color: 1
Size: 259275 Color: 1
Size: 252292 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 372452 Color: 1
Size: 368158 Color: 1
Size: 259391 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 499114 Color: 1
Size: 250805 Color: 1
Size: 250082 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 431684 Color: 1
Size: 294077 Color: 1
Size: 274240 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 486524 Color: 1
Size: 259847 Color: 1
Size: 253630 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 485743 Color: 1
Size: 258311 Color: 1
Size: 255947 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 478794 Color: 1
Size: 266596 Color: 1
Size: 254611 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 357263 Color: 1
Size: 323687 Color: 1
Size: 319051 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 477306 Color: 1
Size: 267449 Color: 1
Size: 255246 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 369359 Color: 1
Size: 359243 Color: 1
Size: 271399 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 433873 Color: 1
Size: 298220 Color: 1
Size: 267908 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 471289 Color: 1
Size: 269162 Color: 1
Size: 259550 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 453656 Color: 1
Size: 286741 Color: 1
Size: 259604 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 364995 Color: 1
Size: 320695 Color: 1
Size: 314311 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 492252 Color: 1
Size: 254439 Color: 1
Size: 253310 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 366195 Color: 1
Size: 329609 Color: 1
Size: 304197 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 464951 Color: 1
Size: 268482 Color: 1
Size: 266568 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 371627 Color: 1
Size: 341609 Color: 1
Size: 286765 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 390031 Color: 1
Size: 331391 Color: 1
Size: 278579 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 383517 Color: 1
Size: 310809 Color: 1
Size: 305675 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 405817 Color: 1
Size: 324270 Color: 1
Size: 269914 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 350272 Color: 1
Size: 326345 Color: 0
Size: 323384 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 484924 Color: 1
Size: 264161 Color: 1
Size: 250916 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 446026 Color: 1
Size: 278879 Color: 1
Size: 275096 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 478445 Color: 1
Size: 264473 Color: 1
Size: 257083 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 441689 Color: 1
Size: 286081 Color: 1
Size: 272231 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 480629 Color: 1
Size: 265484 Color: 1
Size: 253888 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 417991 Color: 1
Size: 322935 Color: 1
Size: 259075 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 371951 Color: 1
Size: 346445 Color: 1
Size: 281605 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 397139 Color: 1
Size: 333921 Color: 1
Size: 268941 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 371818 Color: 1
Size: 322904 Color: 0
Size: 305279 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 454718 Color: 1
Size: 278849 Color: 1
Size: 266434 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 371837 Color: 1
Size: 362098 Color: 1
Size: 266066 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 464690 Color: 1
Size: 276002 Color: 1
Size: 259309 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 354982 Color: 1
Size: 332044 Color: 0
Size: 312975 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 472701 Color: 1
Size: 264766 Color: 1
Size: 262534 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 374112 Color: 1
Size: 343417 Color: 1
Size: 282472 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 410493 Color: 1
Size: 321245 Color: 1
Size: 268263 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 481584 Color: 1
Size: 260087 Color: 1
Size: 258330 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 367659 Color: 1
Size: 328434 Color: 1
Size: 303908 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 436502 Color: 1
Size: 298484 Color: 1
Size: 265015 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 487729 Color: 1
Size: 258972 Color: 1
Size: 253300 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 493129 Color: 1
Size: 254675 Color: 1
Size: 252197 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 432315 Color: 1
Size: 301389 Color: 1
Size: 266297 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 350989 Color: 1
Size: 333747 Color: 1
Size: 315265 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 488145 Color: 1
Size: 257737 Color: 1
Size: 254119 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 465642 Color: 1
Size: 280572 Color: 1
Size: 253787 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 372233 Color: 1
Size: 320320 Color: 1
Size: 307448 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 420031 Color: 1
Size: 305159 Color: 1
Size: 274811 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 464243 Color: 1
Size: 282438 Color: 1
Size: 253320 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 428804 Color: 1
Size: 301790 Color: 1
Size: 269407 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 464207 Color: 1
Size: 282940 Color: 1
Size: 252854 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 454027 Color: 1
Size: 295217 Color: 1
Size: 250757 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 394655 Color: 1
Size: 326529 Color: 0
Size: 278817 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 472031 Color: 1
Size: 275285 Color: 1
Size: 252685 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 432007 Color: 1
Size: 312649 Color: 1
Size: 255345 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 441197 Color: 1
Size: 300662 Color: 1
Size: 258142 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 467090 Color: 1
Size: 282873 Color: 1
Size: 250038 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 475519 Color: 1
Size: 270058 Color: 1
Size: 254424 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 387031 Color: 1
Size: 339590 Color: 1
Size: 273380 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 476625 Color: 1
Size: 270868 Color: 1
Size: 252508 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 358020 Color: 1
Size: 326931 Color: 1
Size: 315050 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 470677 Color: 1
Size: 278133 Color: 1
Size: 251191 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 370292 Color: 1
Size: 323988 Color: 1
Size: 305721 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 469920 Color: 1
Size: 276780 Color: 1
Size: 253301 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 479621 Color: 1
Size: 267867 Color: 1
Size: 252513 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 433955 Color: 1
Size: 294001 Color: 1
Size: 272045 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 413071 Color: 1
Size: 311543 Color: 1
Size: 275387 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 433271 Color: 1
Size: 310736 Color: 1
Size: 255994 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 411909 Color: 1
Size: 305974 Color: 1
Size: 282118 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 388441 Color: 1
Size: 342966 Color: 1
Size: 268594 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 380674 Color: 1
Size: 342344 Color: 1
Size: 276983 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 441459 Color: 1
Size: 285707 Color: 1
Size: 272835 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 400937 Color: 1
Size: 317392 Color: 1
Size: 281672 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 404643 Color: 1
Size: 328383 Color: 1
Size: 266975 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 416551 Color: 1
Size: 297456 Color: 1
Size: 285994 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 425016 Color: 1
Size: 309893 Color: 1
Size: 265092 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 484125 Color: 1
Size: 265626 Color: 1
Size: 250250 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 386896 Color: 1
Size: 335627 Color: 1
Size: 277478 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 383847 Color: 1
Size: 321304 Color: 1
Size: 294850 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 456462 Color: 1
Size: 291560 Color: 1
Size: 251979 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 438722 Color: 1
Size: 309699 Color: 1
Size: 251580 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 351399 Color: 1
Size: 340040 Color: 1
Size: 308562 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 385400 Color: 1
Size: 309031 Color: 0
Size: 305570 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 430409 Color: 1
Size: 295201 Color: 1
Size: 274391 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 487881 Color: 1
Size: 257418 Color: 1
Size: 254702 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 402022 Color: 1
Size: 327067 Color: 1
Size: 270912 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 397699 Color: 1
Size: 348250 Color: 1
Size: 254052 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 409002 Color: 1
Size: 313232 Color: 1
Size: 277767 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 477139 Color: 1
Size: 272551 Color: 1
Size: 250311 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 438533 Color: 1
Size: 293826 Color: 1
Size: 267642 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 483576 Color: 1
Size: 265210 Color: 1
Size: 251215 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 408682 Color: 1
Size: 333319 Color: 1
Size: 258000 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 372421 Color: 1
Size: 336981 Color: 1
Size: 290599 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 385170 Color: 1
Size: 309367 Color: 0
Size: 305464 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 362035 Color: 1
Size: 334017 Color: 1
Size: 303949 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 376759 Color: 1
Size: 370100 Color: 1
Size: 253142 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 493315 Color: 1
Size: 254313 Color: 1
Size: 252373 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 440069 Color: 1
Size: 289576 Color: 1
Size: 270356 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 370294 Color: 1
Size: 320372 Color: 0
Size: 309335 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 406025 Color: 1
Size: 301147 Color: 1
Size: 292829 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 417134 Color: 1
Size: 317095 Color: 1
Size: 265772 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 372470 Color: 1
Size: 338611 Color: 1
Size: 288920 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 386527 Color: 1
Size: 362782 Color: 1
Size: 250692 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 492057 Color: 1
Size: 257408 Color: 1
Size: 250536 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 386414 Color: 1
Size: 309952 Color: 1
Size: 303635 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 481035 Color: 1
Size: 263270 Color: 1
Size: 255696 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 499330 Color: 1
Size: 250643 Color: 1
Size: 250028 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 443092 Color: 1
Size: 306823 Color: 1
Size: 250086 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 446006 Color: 1
Size: 280123 Color: 1
Size: 273872 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 412684 Color: 1
Size: 326371 Color: 1
Size: 260946 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 364701 Color: 1
Size: 330690 Color: 1
Size: 304610 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 398843 Color: 1
Size: 347996 Color: 1
Size: 253162 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 495170 Color: 1
Size: 254597 Color: 1
Size: 250234 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 405605 Color: 1
Size: 317012 Color: 1
Size: 277384 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 478157 Color: 1
Size: 262075 Color: 1
Size: 259769 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 375766 Color: 1
Size: 348674 Color: 1
Size: 275561 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 357946 Color: 1
Size: 356189 Color: 1
Size: 285866 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 443099 Color: 1
Size: 278660 Color: 1
Size: 278242 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 394670 Color: 1
Size: 310090 Color: 1
Size: 295241 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 435314 Color: 1
Size: 283549 Color: 0
Size: 281138 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 356727 Color: 1
Size: 350656 Color: 1
Size: 292618 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 431209 Color: 1
Size: 290371 Color: 1
Size: 278421 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 374759 Color: 1
Size: 350609 Color: 1
Size: 274633 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 450622 Color: 1
Size: 284856 Color: 1
Size: 264523 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 429877 Color: 1
Size: 319037 Color: 1
Size: 251087 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 400031 Color: 1
Size: 312962 Color: 1
Size: 287008 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 481411 Color: 1
Size: 260759 Color: 1
Size: 257831 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 378126 Color: 1
Size: 333265 Color: 1
Size: 288610 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 383752 Color: 1
Size: 323032 Color: 1
Size: 293217 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 476683 Color: 1
Size: 273249 Color: 1
Size: 250069 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 402001 Color: 1
Size: 309073 Color: 1
Size: 288927 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 367394 Color: 1
Size: 356451 Color: 1
Size: 276156 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 497888 Color: 1
Size: 251442 Color: 1
Size: 250671 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 469672 Color: 1
Size: 267912 Color: 1
Size: 262417 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 378136 Color: 1
Size: 331558 Color: 0
Size: 290307 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 413581 Color: 1
Size: 328686 Color: 1
Size: 257734 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 343894 Color: 1
Size: 334001 Color: 1
Size: 322106 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 493536 Color: 1
Size: 254750 Color: 1
Size: 251715 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 428049 Color: 1
Size: 297073 Color: 1
Size: 274879 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 449108 Color: 1
Size: 284568 Color: 1
Size: 266325 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 462075 Color: 1
Size: 284585 Color: 1
Size: 253341 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 496518 Color: 1
Size: 253177 Color: 1
Size: 250306 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 468464 Color: 1
Size: 280927 Color: 1
Size: 250610 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 373469 Color: 1
Size: 368999 Color: 1
Size: 257533 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 492531 Color: 1
Size: 255371 Color: 1
Size: 252099 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 413480 Color: 1
Size: 333890 Color: 1
Size: 252631 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 421870 Color: 1
Size: 327641 Color: 1
Size: 250490 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 495564 Color: 1
Size: 253655 Color: 1
Size: 250782 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 434929 Color: 1
Size: 313796 Color: 1
Size: 251276 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 391348 Color: 1
Size: 315478 Color: 1
Size: 293175 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 475122 Color: 1
Size: 264084 Color: 1
Size: 260795 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 452985 Color: 1
Size: 294496 Color: 1
Size: 252520 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 401724 Color: 1
Size: 340760 Color: 1
Size: 257517 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 476813 Color: 1
Size: 269341 Color: 1
Size: 253847 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 422877 Color: 1
Size: 321756 Color: 1
Size: 255368 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 450912 Color: 1
Size: 283932 Color: 1
Size: 265157 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 378012 Color: 1
Size: 328516 Color: 1
Size: 293473 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 391071 Color: 1
Size: 332423 Color: 1
Size: 276507 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 462391 Color: 1
Size: 280595 Color: 1
Size: 257015 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 357860 Color: 1
Size: 330190 Color: 1
Size: 311951 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 418444 Color: 1
Size: 312258 Color: 1
Size: 269299 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 475600 Color: 1
Size: 264819 Color: 1
Size: 259582 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 392376 Color: 1
Size: 313404 Color: 1
Size: 294221 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 439902 Color: 1
Size: 296277 Color: 1
Size: 263822 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 422755 Color: 1
Size: 304197 Color: 1
Size: 273049 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 418559 Color: 1
Size: 308181 Color: 1
Size: 273261 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 471779 Color: 1
Size: 273101 Color: 1
Size: 255121 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 496857 Color: 1
Size: 252502 Color: 1
Size: 250642 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 406827 Color: 1
Size: 297890 Color: 1
Size: 295284 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 472737 Color: 1
Size: 264636 Color: 1
Size: 262628 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 391450 Color: 1
Size: 319772 Color: 1
Size: 288779 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 476531 Color: 1
Size: 271831 Color: 1
Size: 251639 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 394238 Color: 1
Size: 305612 Color: 0
Size: 300151 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 430727 Color: 1
Size: 303146 Color: 1
Size: 266128 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 398324 Color: 1
Size: 321634 Color: 1
Size: 280043 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 411727 Color: 1
Size: 301402 Color: 1
Size: 286872 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 362877 Color: 1
Size: 319841 Color: 0
Size: 317283 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 429590 Color: 1
Size: 301171 Color: 1
Size: 269240 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 382272 Color: 1
Size: 310119 Color: 1
Size: 307610 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 370131 Color: 1
Size: 339179 Color: 1
Size: 290691 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 470819 Color: 1
Size: 271717 Color: 1
Size: 257465 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 409539 Color: 1
Size: 313792 Color: 1
Size: 276670 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 347705 Color: 1
Size: 340019 Color: 1
Size: 312277 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 378503 Color: 1
Size: 359544 Color: 1
Size: 261954 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 410993 Color: 1
Size: 324989 Color: 1
Size: 264019 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 421488 Color: 1
Size: 289341 Color: 1
Size: 289172 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 365378 Color: 1
Size: 344668 Color: 1
Size: 289955 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 374655 Color: 1
Size: 319718 Color: 1
Size: 305628 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 471543 Color: 1
Size: 271997 Color: 1
Size: 256461 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 431402 Color: 1
Size: 299626 Color: 1
Size: 268973 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 384734 Color: 1
Size: 330774 Color: 1
Size: 284493 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 376620 Color: 1
Size: 338436 Color: 1
Size: 284945 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 428573 Color: 1
Size: 299749 Color: 1
Size: 271679 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 447649 Color: 1
Size: 295746 Color: 1
Size: 256606 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 490401 Color: 1
Size: 258844 Color: 1
Size: 250756 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 466216 Color: 1
Size: 278858 Color: 1
Size: 254927 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 456950 Color: 1
Size: 271789 Color: 0
Size: 271262 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 372370 Color: 1
Size: 343572 Color: 1
Size: 284059 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 436108 Color: 1
Size: 299357 Color: 1
Size: 264536 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 373752 Color: 1
Size: 327713 Color: 1
Size: 298536 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 433224 Color: 1
Size: 305304 Color: 1
Size: 261473 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 411899 Color: 1
Size: 318549 Color: 1
Size: 269553 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 473117 Color: 1
Size: 271728 Color: 1
Size: 255156 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 351099 Color: 1
Size: 333587 Color: 1
Size: 315315 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 491968 Color: 1
Size: 254128 Color: 1
Size: 253905 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 488059 Color: 1
Size: 258128 Color: 1
Size: 253814 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 353940 Color: 1
Size: 344449 Color: 1
Size: 301612 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 450650 Color: 1
Size: 299024 Color: 1
Size: 250327 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 368814 Color: 1
Size: 365668 Color: 1
Size: 265519 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 476985 Color: 1
Size: 271301 Color: 1
Size: 251715 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 480630 Color: 1
Size: 266419 Color: 1
Size: 252952 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 434105 Color: 1
Size: 289380 Color: 1
Size: 276516 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 364554 Color: 1
Size: 347583 Color: 1
Size: 287864 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 368481 Color: 1
Size: 366568 Color: 1
Size: 264952 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 431083 Color: 1
Size: 295033 Color: 0
Size: 273885 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 425631 Color: 1
Size: 301371 Color: 1
Size: 272999 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 374549 Color: 1
Size: 369303 Color: 1
Size: 256149 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 429487 Color: 1
Size: 285329 Color: 1
Size: 285185 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 398912 Color: 1
Size: 345924 Color: 1
Size: 255165 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 395334 Color: 1
Size: 341631 Color: 1
Size: 263036 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 364300 Color: 1
Size: 361639 Color: 1
Size: 274062 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 454114 Color: 1
Size: 273622 Color: 1
Size: 272265 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 460277 Color: 1
Size: 279134 Color: 1
Size: 260590 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 473684 Color: 1
Size: 265545 Color: 1
Size: 260772 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 457944 Color: 1
Size: 286452 Color: 1
Size: 255605 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 388850 Color: 1
Size: 358615 Color: 1
Size: 252536 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 450664 Color: 1
Size: 287790 Color: 1
Size: 261547 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 433154 Color: 1
Size: 300999 Color: 1
Size: 265848 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 380614 Color: 1
Size: 333264 Color: 1
Size: 286123 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 356371 Color: 1
Size: 355208 Color: 1
Size: 288422 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 429619 Color: 1
Size: 298481 Color: 1
Size: 271901 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 414088 Color: 1
Size: 295122 Color: 1
Size: 290791 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 487305 Color: 1
Size: 258717 Color: 1
Size: 253979 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 460899 Color: 1
Size: 283316 Color: 1
Size: 255786 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 418539 Color: 1
Size: 295546 Color: 1
Size: 285916 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 440893 Color: 1
Size: 293516 Color: 1
Size: 265592 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 469282 Color: 1
Size: 267136 Color: 1
Size: 263583 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 359660 Color: 1
Size: 351351 Color: 1
Size: 288990 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 408982 Color: 1
Size: 299920 Color: 1
Size: 291099 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 393152 Color: 1
Size: 319612 Color: 1
Size: 287237 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 371534 Color: 1
Size: 343833 Color: 1
Size: 284634 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 348820 Color: 1
Size: 347116 Color: 1
Size: 304065 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 420216 Color: 1
Size: 310930 Color: 1
Size: 268855 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 356186 Color: 1
Size: 340078 Color: 1
Size: 303737 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 384291 Color: 1
Size: 315062 Color: 1
Size: 300648 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 410240 Color: 1
Size: 301599 Color: 1
Size: 288162 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 394684 Color: 1
Size: 353105 Color: 1
Size: 252212 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 485661 Color: 1
Size: 257944 Color: 1
Size: 256396 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 464655 Color: 1
Size: 277847 Color: 1
Size: 257499 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 459164 Color: 1
Size: 279115 Color: 1
Size: 261722 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 495473 Color: 1
Size: 252897 Color: 1
Size: 251631 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 398827 Color: 1
Size: 333141 Color: 1
Size: 268033 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 380217 Color: 1
Size: 313945 Color: 1
Size: 305839 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 377568 Color: 1
Size: 334619 Color: 1
Size: 287814 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 402172 Color: 1
Size: 305682 Color: 1
Size: 292147 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 411824 Color: 1
Size: 304236 Color: 1
Size: 283941 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 476054 Color: 1
Size: 264981 Color: 1
Size: 258966 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 406033 Color: 1
Size: 340704 Color: 1
Size: 253264 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 404076 Color: 1
Size: 314701 Color: 1
Size: 281224 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 360881 Color: 1
Size: 355309 Color: 1
Size: 283811 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 452329 Color: 1
Size: 297159 Color: 1
Size: 250513 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 481444 Color: 1
Size: 263269 Color: 1
Size: 255288 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 349575 Color: 1
Size: 346453 Color: 1
Size: 303973 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 384089 Color: 1
Size: 360697 Color: 1
Size: 255215 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 403045 Color: 1
Size: 342281 Color: 1
Size: 254675 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 374756 Color: 1
Size: 363292 Color: 1
Size: 261953 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 368535 Color: 1
Size: 358960 Color: 1
Size: 272506 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 362641 Color: 1
Size: 353942 Color: 1
Size: 283418 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 438053 Color: 1
Size: 287420 Color: 1
Size: 274528 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 434730 Color: 1
Size: 296789 Color: 1
Size: 268482 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 391295 Color: 1
Size: 322140 Color: 1
Size: 286566 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 413624 Color: 1
Size: 305800 Color: 0
Size: 280577 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 427014 Color: 1
Size: 289079 Color: 1
Size: 283908 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 448436 Color: 1
Size: 291009 Color: 1
Size: 260556 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 393973 Color: 1
Size: 317321 Color: 1
Size: 288707 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 490459 Color: 1
Size: 255747 Color: 1
Size: 253795 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 383529 Color: 1
Size: 352852 Color: 1
Size: 263620 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 472622 Color: 1
Size: 270938 Color: 1
Size: 256441 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 450442 Color: 1
Size: 281294 Color: 1
Size: 268265 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 404502 Color: 1
Size: 309678 Color: 1
Size: 285821 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 353742 Color: 1
Size: 342965 Color: 1
Size: 303294 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 392932 Color: 1
Size: 342465 Color: 1
Size: 264604 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 422321 Color: 1
Size: 296709 Color: 1
Size: 280971 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 351912 Color: 1
Size: 324990 Color: 1
Size: 323099 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 428263 Color: 1
Size: 321008 Color: 1
Size: 250730 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 462154 Color: 1
Size: 283035 Color: 1
Size: 254812 Color: 0

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 441681 Color: 1
Size: 290117 Color: 1
Size: 268202 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 408943 Color: 1
Size: 308481 Color: 1
Size: 282576 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 435002 Color: 1
Size: 300045 Color: 1
Size: 264953 Color: 0

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 374280 Color: 1
Size: 315090 Color: 1
Size: 310630 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 347054 Color: 1
Size: 341590 Color: 1
Size: 311356 Color: 0

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 383440 Color: 1
Size: 327432 Color: 1
Size: 289128 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 429011 Color: 1
Size: 319013 Color: 1
Size: 251976 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 428983 Color: 1
Size: 303551 Color: 1
Size: 267466 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 359989 Color: 1
Size: 336797 Color: 1
Size: 303214 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 451024 Color: 1
Size: 293164 Color: 1
Size: 255812 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 406164 Color: 1
Size: 339131 Color: 1
Size: 254705 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 371562 Color: 1
Size: 338070 Color: 1
Size: 290368 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 408084 Color: 1
Size: 315888 Color: 1
Size: 276028 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 473234 Color: 1
Size: 269937 Color: 1
Size: 256829 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 414459 Color: 1
Size: 328134 Color: 1
Size: 257407 Color: 0

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 383271 Color: 1
Size: 349310 Color: 1
Size: 267419 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 388334 Color: 1
Size: 340190 Color: 1
Size: 271476 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 439272 Color: 1
Size: 308578 Color: 1
Size: 252150 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 380069 Color: 1
Size: 361054 Color: 1
Size: 258877 Color: 0

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 365669 Color: 1
Size: 327098 Color: 1
Size: 307233 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 373303 Color: 1
Size: 346717 Color: 1
Size: 279980 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 373786 Color: 1
Size: 322757 Color: 1
Size: 303457 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 367860 Color: 1
Size: 320059 Color: 1
Size: 312081 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 396345 Color: 1
Size: 343721 Color: 1
Size: 259934 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 400066 Color: 1
Size: 341418 Color: 1
Size: 258516 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 380323 Color: 1
Size: 347340 Color: 1
Size: 272337 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 405524 Color: 1
Size: 336380 Color: 1
Size: 258096 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 446957 Color: 1
Size: 281393 Color: 1
Size: 271650 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 434291 Color: 1
Size: 286601 Color: 0
Size: 279108 Color: 1

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 355236 Color: 1
Size: 353405 Color: 1
Size: 291359 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 451858 Color: 1
Size: 293263 Color: 1
Size: 254879 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 370376 Color: 1
Size: 318128 Color: 1
Size: 311496 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 434640 Color: 1
Size: 297850 Color: 1
Size: 267510 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 369056 Color: 1
Size: 368111 Color: 1
Size: 262833 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 418972 Color: 1
Size: 326244 Color: 1
Size: 254784 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 405944 Color: 1
Size: 329499 Color: 1
Size: 264557 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 401530 Color: 1
Size: 323176 Color: 1
Size: 275294 Color: 0

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 396269 Color: 1
Size: 317785 Color: 1
Size: 285946 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 456526 Color: 1
Size: 274324 Color: 1
Size: 269150 Color: 0

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 385143 Color: 1
Size: 337376 Color: 1
Size: 277481 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 418536 Color: 1
Size: 325433 Color: 1
Size: 256031 Color: 0

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 387412 Color: 1
Size: 308030 Color: 0
Size: 304558 Color: 1

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 441528 Color: 1
Size: 290650 Color: 1
Size: 267822 Color: 0

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 375377 Color: 1
Size: 336148 Color: 1
Size: 288475 Color: 0

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 378410 Color: 1
Size: 343567 Color: 1
Size: 278023 Color: 0

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 378114 Color: 1
Size: 362369 Color: 1
Size: 259517 Color: 0

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 369378 Color: 1
Size: 333452 Color: 1
Size: 297170 Color: 0

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 432818 Color: 1
Size: 310336 Color: 1
Size: 256845 Color: 0

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 401502 Color: 1
Size: 320816 Color: 1
Size: 277681 Color: 0

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 390762 Color: 1
Size: 326046 Color: 1
Size: 283191 Color: 0

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 372831 Color: 1
Size: 358748 Color: 1
Size: 268420 Color: 0

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 377232 Color: 1
Size: 321321 Color: 0
Size: 301446 Color: 1

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 377015 Color: 1
Size: 347115 Color: 1
Size: 275869 Color: 0

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 446064 Color: 1
Size: 299440 Color: 1
Size: 254495 Color: 0

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 378158 Color: 1
Size: 337496 Color: 1
Size: 284345 Color: 0

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 376326 Color: 1
Size: 333607 Color: 1
Size: 290066 Color: 0

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 367922 Color: 1
Size: 327969 Color: 1
Size: 304108 Color: 0

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 441396 Color: 1
Size: 285573 Color: 1
Size: 273030 Color: 0

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 410961 Color: 1
Size: 325661 Color: 1
Size: 263377 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 387719 Color: 1
Size: 329082 Color: 1
Size: 283198 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 398656 Color: 1
Size: 325243 Color: 1
Size: 276100 Color: 0

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 384135 Color: 1
Size: 356646 Color: 1
Size: 259218 Color: 0

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 385099 Color: 1
Size: 361797 Color: 1
Size: 253103 Color: 0

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 414302 Color: 1
Size: 297083 Color: 1
Size: 288614 Color: 0

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 356180 Color: 1
Size: 353252 Color: 1
Size: 290567 Color: 0

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 387362 Color: 1
Size: 331062 Color: 1
Size: 281575 Color: 0

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 432473 Color: 1
Size: 317404 Color: 1
Size: 250122 Color: 0

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 426945 Color: 1
Size: 318900 Color: 1
Size: 254154 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 377536 Color: 1
Size: 358795 Color: 1
Size: 263668 Color: 0

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 431662 Color: 1
Size: 298570 Color: 1
Size: 269767 Color: 0

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 394310 Color: 1
Size: 330198 Color: 1
Size: 275491 Color: 0

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 427630 Color: 1
Size: 316527 Color: 1
Size: 255842 Color: 0

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 425038 Color: 1
Size: 296442 Color: 0
Size: 278519 Color: 1

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 391690 Color: 1
Size: 317720 Color: 1
Size: 290589 Color: 0

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 380953 Color: 1
Size: 345503 Color: 1
Size: 273543 Color: 0

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 450472 Color: 1
Size: 298287 Color: 1
Size: 251240 Color: 0

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 389250 Color: 1
Size: 332950 Color: 1
Size: 277799 Color: 0

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 362673 Color: 1
Size: 326676 Color: 0
Size: 310650 Color: 1

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 396705 Color: 1
Size: 308097 Color: 0
Size: 295197 Color: 1

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 372746 Color: 1
Size: 334068 Color: 1
Size: 293185 Color: 0

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 444593 Color: 1
Size: 279248 Color: 0
Size: 276158 Color: 1

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 363445 Color: 1
Size: 329642 Color: 1
Size: 306912 Color: 0

Bin 481: 3 of cap free
Amount of items: 3
Items: 
Size: 453333 Color: 1
Size: 286017 Color: 1
Size: 260648 Color: 0

Bin 482: 3 of cap free
Amount of items: 3
Items: 
Size: 397389 Color: 1
Size: 305429 Color: 1
Size: 297180 Color: 0

Bin 483: 3 of cap free
Amount of items: 3
Items: 
Size: 470102 Color: 1
Size: 276744 Color: 0
Size: 253152 Color: 1

Bin 484: 3 of cap free
Amount of items: 3
Items: 
Size: 357592 Color: 1
Size: 356640 Color: 1
Size: 285766 Color: 0

Bin 485: 3 of cap free
Amount of items: 3
Items: 
Size: 451455 Color: 1
Size: 285374 Color: 1
Size: 263169 Color: 0

Bin 486: 3 of cap free
Amount of items: 3
Items: 
Size: 377390 Color: 1
Size: 352728 Color: 1
Size: 269880 Color: 0

Bin 487: 3 of cap free
Amount of items: 3
Items: 
Size: 385328 Color: 1
Size: 330393 Color: 1
Size: 284277 Color: 0

Bin 488: 3 of cap free
Amount of items: 3
Items: 
Size: 426714 Color: 1
Size: 297886 Color: 0
Size: 275398 Color: 1

Bin 489: 3 of cap free
Amount of items: 3
Items: 
Size: 454351 Color: 1
Size: 291710 Color: 1
Size: 253937 Color: 0

Bin 490: 3 of cap free
Amount of items: 3
Items: 
Size: 452471 Color: 1
Size: 292884 Color: 1
Size: 254643 Color: 0

Bin 491: 3 of cap free
Amount of items: 3
Items: 
Size: 413689 Color: 1
Size: 326014 Color: 1
Size: 260295 Color: 0

Bin 492: 3 of cap free
Amount of items: 3
Items: 
Size: 433661 Color: 1
Size: 288576 Color: 1
Size: 277761 Color: 0

Bin 493: 3 of cap free
Amount of items: 3
Items: 
Size: 405984 Color: 1
Size: 325808 Color: 1
Size: 268206 Color: 0

Bin 494: 3 of cap free
Amount of items: 3
Items: 
Size: 395486 Color: 1
Size: 321732 Color: 1
Size: 282780 Color: 0

Bin 495: 3 of cap free
Amount of items: 3
Items: 
Size: 375477 Color: 1
Size: 373755 Color: 1
Size: 250766 Color: 0

Bin 496: 3 of cap free
Amount of items: 3
Items: 
Size: 420125 Color: 1
Size: 318208 Color: 1
Size: 261665 Color: 0

Bin 497: 3 of cap free
Amount of items: 3
Items: 
Size: 412547 Color: 1
Size: 319513 Color: 1
Size: 267938 Color: 0

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 440987 Color: 1
Size: 299785 Color: 1
Size: 259226 Color: 0

Bin 499: 4 of cap free
Amount of items: 3
Items: 
Size: 458589 Color: 1
Size: 280814 Color: 1
Size: 260594 Color: 0

Bin 500: 4 of cap free
Amount of items: 3
Items: 
Size: 425291 Color: 1
Size: 298387 Color: 1
Size: 276319 Color: 0

Bin 501: 4 of cap free
Amount of items: 3
Items: 
Size: 397049 Color: 1
Size: 306554 Color: 1
Size: 296394 Color: 0

Bin 502: 4 of cap free
Amount of items: 3
Items: 
Size: 468132 Color: 1
Size: 276175 Color: 1
Size: 255690 Color: 0

Bin 503: 4 of cap free
Amount of items: 3
Items: 
Size: 441056 Color: 1
Size: 287619 Color: 1
Size: 271322 Color: 0

Bin 504: 4 of cap free
Amount of items: 3
Items: 
Size: 379176 Color: 1
Size: 322862 Color: 1
Size: 297959 Color: 0

Bin 505: 4 of cap free
Amount of items: 3
Items: 
Size: 411715 Color: 1
Size: 318651 Color: 1
Size: 269631 Color: 0

Bin 506: 4 of cap free
Amount of items: 3
Items: 
Size: 404175 Color: 1
Size: 334149 Color: 1
Size: 261673 Color: 0

Bin 507: 5 of cap free
Amount of items: 3
Items: 
Size: 350353 Color: 1
Size: 337818 Color: 1
Size: 311825 Color: 0

Bin 508: 5 of cap free
Amount of items: 3
Items: 
Size: 371565 Color: 1
Size: 331942 Color: 1
Size: 296489 Color: 0

Bin 509: 5 of cap free
Amount of items: 3
Items: 
Size: 360953 Color: 1
Size: 342043 Color: 1
Size: 297000 Color: 0

Bin 510: 5 of cap free
Amount of items: 3
Items: 
Size: 410171 Color: 1
Size: 320548 Color: 1
Size: 269277 Color: 0

Bin 511: 5 of cap free
Amount of items: 3
Items: 
Size: 425281 Color: 1
Size: 318242 Color: 1
Size: 256473 Color: 0

Bin 512: 5 of cap free
Amount of items: 3
Items: 
Size: 378653 Color: 1
Size: 352554 Color: 1
Size: 268789 Color: 0

Bin 513: 5 of cap free
Amount of items: 3
Items: 
Size: 417853 Color: 1
Size: 307794 Color: 1
Size: 274349 Color: 0

Bin 514: 5 of cap free
Amount of items: 3
Items: 
Size: 419066 Color: 1
Size: 299738 Color: 0
Size: 281192 Color: 1

Bin 515: 5 of cap free
Amount of items: 3
Items: 
Size: 369823 Color: 1
Size: 357579 Color: 1
Size: 272594 Color: 0

Bin 516: 5 of cap free
Amount of items: 3
Items: 
Size: 409584 Color: 1
Size: 323281 Color: 1
Size: 267131 Color: 0

Bin 517: 5 of cap free
Amount of items: 3
Items: 
Size: 398717 Color: 1
Size: 324855 Color: 1
Size: 276424 Color: 0

Bin 518: 5 of cap free
Amount of items: 3
Items: 
Size: 384253 Color: 1
Size: 329904 Color: 1
Size: 285839 Color: 0

Bin 519: 5 of cap free
Amount of items: 3
Items: 
Size: 394892 Color: 1
Size: 354595 Color: 1
Size: 250509 Color: 0

Bin 520: 5 of cap free
Amount of items: 3
Items: 
Size: 408156 Color: 1
Size: 309699 Color: 1
Size: 282141 Color: 0

Bin 521: 5 of cap free
Amount of items: 3
Items: 
Size: 377631 Color: 1
Size: 367240 Color: 1
Size: 255125 Color: 0

Bin 522: 6 of cap free
Amount of items: 3
Items: 
Size: 380066 Color: 1
Size: 334420 Color: 1
Size: 285509 Color: 0

Bin 523: 6 of cap free
Amount of items: 3
Items: 
Size: 348621 Color: 1
Size: 345052 Color: 1
Size: 306322 Color: 0

Bin 524: 6 of cap free
Amount of items: 3
Items: 
Size: 419462 Color: 1
Size: 310671 Color: 0
Size: 269862 Color: 1

Bin 525: 6 of cap free
Amount of items: 3
Items: 
Size: 448204 Color: 1
Size: 300252 Color: 1
Size: 251539 Color: 0

Bin 526: 6 of cap free
Amount of items: 3
Items: 
Size: 401376 Color: 1
Size: 330174 Color: 1
Size: 268445 Color: 0

Bin 527: 6 of cap free
Amount of items: 3
Items: 
Size: 367116 Color: 1
Size: 348739 Color: 1
Size: 284140 Color: 0

Bin 528: 6 of cap free
Amount of items: 3
Items: 
Size: 400779 Color: 1
Size: 339861 Color: 1
Size: 259355 Color: 0

Bin 529: 6 of cap free
Amount of items: 3
Items: 
Size: 434012 Color: 1
Size: 311810 Color: 1
Size: 254173 Color: 0

Bin 530: 6 of cap free
Amount of items: 3
Items: 
Size: 383491 Color: 1
Size: 318004 Color: 1
Size: 298500 Color: 0

Bin 531: 7 of cap free
Amount of items: 3
Items: 
Size: 382378 Color: 1
Size: 364517 Color: 1
Size: 253099 Color: 0

Bin 532: 7 of cap free
Amount of items: 3
Items: 
Size: 400741 Color: 1
Size: 313221 Color: 1
Size: 286032 Color: 0

Bin 533: 7 of cap free
Amount of items: 3
Items: 
Size: 430267 Color: 1
Size: 306275 Color: 1
Size: 263452 Color: 0

Bin 534: 7 of cap free
Amount of items: 3
Items: 
Size: 359599 Color: 1
Size: 325705 Color: 1
Size: 314690 Color: 0

Bin 535: 7 of cap free
Amount of items: 3
Items: 
Size: 391535 Color: 1
Size: 324529 Color: 1
Size: 283930 Color: 0

Bin 536: 7 of cap free
Amount of items: 3
Items: 
Size: 446472 Color: 1
Size: 299064 Color: 1
Size: 254458 Color: 0

Bin 537: 7 of cap free
Amount of items: 3
Items: 
Size: 401510 Color: 1
Size: 342184 Color: 1
Size: 256300 Color: 0

Bin 538: 8 of cap free
Amount of items: 3
Items: 
Size: 391436 Color: 1
Size: 346851 Color: 1
Size: 261706 Color: 0

Bin 539: 8 of cap free
Amount of items: 3
Items: 
Size: 457823 Color: 1
Size: 291811 Color: 1
Size: 250359 Color: 0

Bin 540: 8 of cap free
Amount of items: 3
Items: 
Size: 348889 Color: 1
Size: 335687 Color: 1
Size: 315417 Color: 0

Bin 541: 8 of cap free
Amount of items: 3
Items: 
Size: 437034 Color: 1
Size: 290626 Color: 1
Size: 272333 Color: 0

Bin 542: 8 of cap free
Amount of items: 3
Items: 
Size: 389363 Color: 1
Size: 338498 Color: 1
Size: 272132 Color: 0

Bin 543: 8 of cap free
Amount of items: 3
Items: 
Size: 397705 Color: 1
Size: 306643 Color: 0
Size: 295645 Color: 1

Bin 544: 9 of cap free
Amount of items: 3
Items: 
Size: 369100 Color: 1
Size: 328404 Color: 1
Size: 302488 Color: 0

Bin 545: 10 of cap free
Amount of items: 3
Items: 
Size: 374384 Color: 1
Size: 355100 Color: 1
Size: 270507 Color: 0

Bin 546: 10 of cap free
Amount of items: 3
Items: 
Size: 374435 Color: 1
Size: 317580 Color: 0
Size: 307976 Color: 1

Bin 547: 10 of cap free
Amount of items: 3
Items: 
Size: 392977 Color: 1
Size: 331061 Color: 1
Size: 275953 Color: 0

Bin 548: 10 of cap free
Amount of items: 3
Items: 
Size: 383399 Color: 1
Size: 318870 Color: 0
Size: 297722 Color: 1

Bin 549: 10 of cap free
Amount of items: 3
Items: 
Size: 352878 Color: 1
Size: 349614 Color: 1
Size: 297499 Color: 0

Bin 550: 12 of cap free
Amount of items: 3
Items: 
Size: 398233 Color: 1
Size: 305281 Color: 1
Size: 296475 Color: 0

Bin 551: 12 of cap free
Amount of items: 3
Items: 
Size: 380886 Color: 1
Size: 336020 Color: 1
Size: 283083 Color: 0

Bin 552: 12 of cap free
Amount of items: 3
Items: 
Size: 445979 Color: 1
Size: 285483 Color: 1
Size: 268527 Color: 0

Bin 553: 12 of cap free
Amount of items: 3
Items: 
Size: 354193 Color: 1
Size: 339705 Color: 1
Size: 306091 Color: 0

Bin 554: 12 of cap free
Amount of items: 3
Items: 
Size: 397985 Color: 1
Size: 336553 Color: 1
Size: 265451 Color: 0

Bin 555: 13 of cap free
Amount of items: 3
Items: 
Size: 444856 Color: 1
Size: 302621 Color: 1
Size: 252511 Color: 0

Bin 556: 13 of cap free
Amount of items: 3
Items: 
Size: 356784 Color: 1
Size: 326054 Color: 1
Size: 317150 Color: 0

Bin 557: 13 of cap free
Amount of items: 3
Items: 
Size: 403562 Color: 1
Size: 337929 Color: 1
Size: 258497 Color: 0

Bin 558: 13 of cap free
Amount of items: 3
Items: 
Size: 370454 Color: 1
Size: 366964 Color: 1
Size: 262570 Color: 0

Bin 559: 14 of cap free
Amount of items: 3
Items: 
Size: 447611 Color: 1
Size: 299038 Color: 1
Size: 253338 Color: 0

Bin 560: 14 of cap free
Amount of items: 3
Items: 
Size: 431252 Color: 1
Size: 295496 Color: 1
Size: 273239 Color: 0

Bin 561: 14 of cap free
Amount of items: 3
Items: 
Size: 422087 Color: 1
Size: 311489 Color: 1
Size: 266411 Color: 0

Bin 562: 14 of cap free
Amount of items: 3
Items: 
Size: 413624 Color: 1
Size: 333543 Color: 1
Size: 252820 Color: 0

Bin 563: 14 of cap free
Amount of items: 3
Items: 
Size: 353044 Color: 1
Size: 349612 Color: 1
Size: 297331 Color: 0

Bin 564: 14 of cap free
Amount of items: 3
Items: 
Size: 374247 Color: 1
Size: 360370 Color: 1
Size: 265370 Color: 0

Bin 565: 15 of cap free
Amount of items: 3
Items: 
Size: 392324 Color: 1
Size: 352432 Color: 1
Size: 255230 Color: 0

Bin 566: 15 of cap free
Amount of items: 3
Items: 
Size: 404040 Color: 1
Size: 327475 Color: 1
Size: 268471 Color: 0

Bin 567: 16 of cap free
Amount of items: 3
Items: 
Size: 393540 Color: 1
Size: 338697 Color: 1
Size: 267748 Color: 0

Bin 568: 16 of cap free
Amount of items: 3
Items: 
Size: 360400 Color: 1
Size: 319809 Color: 1
Size: 319776 Color: 0

Bin 569: 16 of cap free
Amount of items: 3
Items: 
Size: 384974 Color: 1
Size: 363056 Color: 1
Size: 251955 Color: 0

Bin 570: 16 of cap free
Amount of items: 3
Items: 
Size: 357670 Color: 1
Size: 342381 Color: 1
Size: 299934 Color: 0

Bin 571: 16 of cap free
Amount of items: 3
Items: 
Size: 410977 Color: 1
Size: 310365 Color: 1
Size: 278643 Color: 0

Bin 572: 16 of cap free
Amount of items: 3
Items: 
Size: 414553 Color: 1
Size: 314037 Color: 1
Size: 271395 Color: 0

Bin 573: 18 of cap free
Amount of items: 3
Items: 
Size: 432058 Color: 1
Size: 315337 Color: 1
Size: 252588 Color: 0

Bin 574: 18 of cap free
Amount of items: 3
Items: 
Size: 420423 Color: 1
Size: 308259 Color: 1
Size: 271301 Color: 0

Bin 575: 18 of cap free
Amount of items: 3
Items: 
Size: 398979 Color: 1
Size: 350412 Color: 1
Size: 250592 Color: 0

Bin 576: 18 of cap free
Amount of items: 3
Items: 
Size: 373334 Color: 1
Size: 330434 Color: 1
Size: 296215 Color: 0

Bin 577: 19 of cap free
Amount of items: 3
Items: 
Size: 371145 Color: 1
Size: 329685 Color: 1
Size: 299152 Color: 0

Bin 578: 19 of cap free
Amount of items: 3
Items: 
Size: 403735 Color: 1
Size: 307317 Color: 1
Size: 288930 Color: 0

Bin 579: 20 of cap free
Amount of items: 3
Items: 
Size: 388975 Color: 1
Size: 346474 Color: 1
Size: 264532 Color: 0

Bin 580: 20 of cap free
Amount of items: 3
Items: 
Size: 369959 Color: 1
Size: 351967 Color: 1
Size: 278055 Color: 0

Bin 581: 20 of cap free
Amount of items: 3
Items: 
Size: 386273 Color: 1
Size: 320568 Color: 1
Size: 293140 Color: 0

Bin 582: 21 of cap free
Amount of items: 3
Items: 
Size: 428378 Color: 1
Size: 291519 Color: 1
Size: 280083 Color: 0

Bin 583: 21 of cap free
Amount of items: 3
Items: 
Size: 380597 Color: 1
Size: 315225 Color: 0
Size: 304158 Color: 1

Bin 584: 21 of cap free
Amount of items: 3
Items: 
Size: 365080 Color: 1
Size: 318020 Color: 0
Size: 316880 Color: 1

Bin 585: 22 of cap free
Amount of items: 3
Items: 
Size: 376847 Color: 1
Size: 333445 Color: 1
Size: 289687 Color: 0

Bin 586: 22 of cap free
Amount of items: 3
Items: 
Size: 440637 Color: 1
Size: 306062 Color: 1
Size: 253280 Color: 0

Bin 587: 23 of cap free
Amount of items: 3
Items: 
Size: 372793 Color: 1
Size: 359037 Color: 1
Size: 268148 Color: 0

Bin 588: 25 of cap free
Amount of items: 3
Items: 
Size: 449349 Color: 1
Size: 291932 Color: 1
Size: 258695 Color: 0

Bin 589: 25 of cap free
Amount of items: 3
Items: 
Size: 435748 Color: 1
Size: 296570 Color: 1
Size: 267658 Color: 0

Bin 590: 25 of cap free
Amount of items: 3
Items: 
Size: 471435 Color: 1
Size: 278526 Color: 1
Size: 250015 Color: 0

Bin 591: 25 of cap free
Amount of items: 3
Items: 
Size: 385885 Color: 1
Size: 313615 Color: 1
Size: 300476 Color: 0

Bin 592: 25 of cap free
Amount of items: 3
Items: 
Size: 382557 Color: 1
Size: 362525 Color: 1
Size: 254894 Color: 0

Bin 593: 26 of cap free
Amount of items: 3
Items: 
Size: 452747 Color: 1
Size: 286046 Color: 1
Size: 261182 Color: 0

Bin 594: 26 of cap free
Amount of items: 3
Items: 
Size: 394883 Color: 1
Size: 351880 Color: 1
Size: 253212 Color: 0

Bin 595: 28 of cap free
Amount of items: 3
Items: 
Size: 357156 Color: 1
Size: 326310 Color: 1
Size: 316507 Color: 0

Bin 596: 29 of cap free
Amount of items: 3
Items: 
Size: 359721 Color: 1
Size: 344721 Color: 1
Size: 295530 Color: 0

Bin 597: 29 of cap free
Amount of items: 3
Items: 
Size: 383439 Color: 1
Size: 332416 Color: 1
Size: 284117 Color: 0

Bin 598: 30 of cap free
Amount of items: 3
Items: 
Size: 382320 Color: 1
Size: 335267 Color: 1
Size: 282384 Color: 0

Bin 599: 31 of cap free
Amount of items: 3
Items: 
Size: 392785 Color: 1
Size: 332551 Color: 1
Size: 274634 Color: 0

Bin 600: 31 of cap free
Amount of items: 3
Items: 
Size: 437191 Color: 1
Size: 283036 Color: 0
Size: 279743 Color: 1

Bin 601: 32 of cap free
Amount of items: 3
Items: 
Size: 389406 Color: 1
Size: 355340 Color: 1
Size: 255223 Color: 0

Bin 602: 32 of cap free
Amount of items: 3
Items: 
Size: 419415 Color: 1
Size: 292132 Color: 0
Size: 288422 Color: 1

Bin 603: 35 of cap free
Amount of items: 3
Items: 
Size: 382793 Color: 1
Size: 334328 Color: 1
Size: 282845 Color: 0

Bin 604: 37 of cap free
Amount of items: 3
Items: 
Size: 392126 Color: 1
Size: 354039 Color: 1
Size: 253799 Color: 0

Bin 605: 37 of cap free
Amount of items: 3
Items: 
Size: 462979 Color: 1
Size: 285763 Color: 1
Size: 251222 Color: 0

Bin 606: 38 of cap free
Amount of items: 3
Items: 
Size: 346395 Color: 1
Size: 340604 Color: 1
Size: 312964 Color: 0

Bin 607: 38 of cap free
Amount of items: 3
Items: 
Size: 426248 Color: 1
Size: 307460 Color: 1
Size: 266255 Color: 0

Bin 608: 39 of cap free
Amount of items: 3
Items: 
Size: 407680 Color: 1
Size: 336859 Color: 1
Size: 255423 Color: 0

Bin 609: 39 of cap free
Amount of items: 3
Items: 
Size: 369607 Color: 1
Size: 365730 Color: 1
Size: 264625 Color: 0

Bin 610: 41 of cap free
Amount of items: 3
Items: 
Size: 447918 Color: 1
Size: 286943 Color: 1
Size: 265099 Color: 0

Bin 611: 41 of cap free
Amount of items: 3
Items: 
Size: 431670 Color: 1
Size: 304383 Color: 1
Size: 263907 Color: 0

Bin 612: 41 of cap free
Amount of items: 3
Items: 
Size: 387154 Color: 1
Size: 350933 Color: 1
Size: 261873 Color: 0

Bin 613: 42 of cap free
Amount of items: 3
Items: 
Size: 396758 Color: 1
Size: 338927 Color: 1
Size: 264274 Color: 0

Bin 614: 42 of cap free
Amount of items: 3
Items: 
Size: 458939 Color: 1
Size: 287625 Color: 1
Size: 253395 Color: 0

Bin 615: 43 of cap free
Amount of items: 3
Items: 
Size: 415760 Color: 1
Size: 318353 Color: 1
Size: 265845 Color: 0

Bin 616: 45 of cap free
Amount of items: 3
Items: 
Size: 452442 Color: 1
Size: 292592 Color: 1
Size: 254922 Color: 0

Bin 617: 48 of cap free
Amount of items: 3
Items: 
Size: 387246 Color: 1
Size: 336772 Color: 1
Size: 275935 Color: 0

Bin 618: 48 of cap free
Amount of items: 3
Items: 
Size: 448635 Color: 1
Size: 290066 Color: 1
Size: 261252 Color: 0

Bin 619: 50 of cap free
Amount of items: 3
Items: 
Size: 354455 Color: 1
Size: 329894 Color: 0
Size: 315602 Color: 1

Bin 620: 51 of cap free
Amount of items: 3
Items: 
Size: 436114 Color: 1
Size: 305705 Color: 1
Size: 258131 Color: 0

Bin 621: 55 of cap free
Amount of items: 3
Items: 
Size: 361828 Color: 1
Size: 332280 Color: 1
Size: 305838 Color: 0

Bin 622: 55 of cap free
Amount of items: 3
Items: 
Size: 430767 Color: 1
Size: 315994 Color: 1
Size: 253185 Color: 0

Bin 623: 56 of cap free
Amount of items: 3
Items: 
Size: 384738 Color: 1
Size: 340252 Color: 1
Size: 274955 Color: 0

Bin 624: 68 of cap free
Amount of items: 3
Items: 
Size: 346444 Color: 1
Size: 328615 Color: 0
Size: 324874 Color: 1

Bin 625: 68 of cap free
Amount of items: 3
Items: 
Size: 391323 Color: 1
Size: 323498 Color: 1
Size: 285112 Color: 0

Bin 626: 68 of cap free
Amount of items: 3
Items: 
Size: 399467 Color: 1
Size: 349274 Color: 1
Size: 251192 Color: 0

Bin 627: 72 of cap free
Amount of items: 3
Items: 
Size: 377947 Color: 1
Size: 313793 Color: 1
Size: 308189 Color: 0

Bin 628: 73 of cap free
Amount of items: 3
Items: 
Size: 415058 Color: 1
Size: 323628 Color: 1
Size: 261242 Color: 0

Bin 629: 74 of cap free
Amount of items: 3
Items: 
Size: 386688 Color: 1
Size: 333267 Color: 1
Size: 279972 Color: 0

Bin 630: 74 of cap free
Amount of items: 3
Items: 
Size: 461180 Color: 1
Size: 281061 Color: 1
Size: 257686 Color: 0

Bin 631: 74 of cap free
Amount of items: 3
Items: 
Size: 410419 Color: 1
Size: 332690 Color: 1
Size: 256818 Color: 0

Bin 632: 74 of cap free
Amount of items: 3
Items: 
Size: 455528 Color: 1
Size: 291376 Color: 1
Size: 253023 Color: 0

Bin 633: 75 of cap free
Amount of items: 3
Items: 
Size: 439102 Color: 1
Size: 288838 Color: 1
Size: 271986 Color: 0

Bin 634: 87 of cap free
Amount of items: 3
Items: 
Size: 385795 Color: 1
Size: 320865 Color: 1
Size: 293254 Color: 0

Bin 635: 87 of cap free
Amount of items: 3
Items: 
Size: 347176 Color: 1
Size: 342201 Color: 1
Size: 310537 Color: 0

Bin 636: 91 of cap free
Amount of items: 3
Items: 
Size: 459253 Color: 1
Size: 279322 Color: 1
Size: 261335 Color: 0

Bin 637: 92 of cap free
Amount of items: 3
Items: 
Size: 445441 Color: 1
Size: 300301 Color: 1
Size: 254167 Color: 0

Bin 638: 96 of cap free
Amount of items: 3
Items: 
Size: 391811 Color: 1
Size: 323111 Color: 1
Size: 284983 Color: 0

Bin 639: 103 of cap free
Amount of items: 3
Items: 
Size: 395443 Color: 1
Size: 303850 Color: 0
Size: 300605 Color: 1

Bin 640: 107 of cap free
Amount of items: 3
Items: 
Size: 411326 Color: 1
Size: 312847 Color: 1
Size: 275721 Color: 0

Bin 641: 110 of cap free
Amount of items: 3
Items: 
Size: 416021 Color: 1
Size: 320198 Color: 1
Size: 263672 Color: 0

Bin 642: 112 of cap free
Amount of items: 3
Items: 
Size: 472904 Color: 1
Size: 274938 Color: 1
Size: 252047 Color: 0

Bin 643: 112 of cap free
Amount of items: 3
Items: 
Size: 403150 Color: 1
Size: 339472 Color: 1
Size: 257267 Color: 0

Bin 644: 115 of cap free
Amount of items: 3
Items: 
Size: 434134 Color: 1
Size: 309193 Color: 1
Size: 256559 Color: 0

Bin 645: 125 of cap free
Amount of items: 3
Items: 
Size: 379915 Color: 1
Size: 368167 Color: 1
Size: 251794 Color: 0

Bin 646: 143 of cap free
Amount of items: 3
Items: 
Size: 443205 Color: 1
Size: 286997 Color: 1
Size: 269656 Color: 0

Bin 647: 171 of cap free
Amount of items: 3
Items: 
Size: 450254 Color: 1
Size: 295643 Color: 1
Size: 253933 Color: 0

Bin 648: 188 of cap free
Amount of items: 3
Items: 
Size: 414282 Color: 1
Size: 297447 Color: 1
Size: 288084 Color: 0

Bin 649: 202 of cap free
Amount of items: 3
Items: 
Size: 406084 Color: 1
Size: 329865 Color: 1
Size: 263850 Color: 0

Bin 650: 230 of cap free
Amount of items: 3
Items: 
Size: 397169 Color: 1
Size: 302284 Color: 1
Size: 300318 Color: 0

Bin 651: 278 of cap free
Amount of items: 3
Items: 
Size: 371196 Color: 1
Size: 337140 Color: 1
Size: 291387 Color: 0

Bin 652: 329 of cap free
Amount of items: 3
Items: 
Size: 389619 Color: 1
Size: 335890 Color: 1
Size: 274163 Color: 0

Bin 653: 427 of cap free
Amount of items: 3
Items: 
Size: 442430 Color: 1
Size: 283744 Color: 1
Size: 273400 Color: 0

Bin 654: 457 of cap free
Amount of items: 3
Items: 
Size: 461511 Color: 1
Size: 286289 Color: 1
Size: 251744 Color: 0

Bin 655: 473 of cap free
Amount of items: 3
Items: 
Size: 417470 Color: 1
Size: 328875 Color: 1
Size: 253183 Color: 0

Bin 656: 489 of cap free
Amount of items: 3
Items: 
Size: 474849 Color: 1
Size: 271235 Color: 0
Size: 253428 Color: 1

Bin 657: 752 of cap free
Amount of items: 3
Items: 
Size: 352531 Color: 1
Size: 349834 Color: 1
Size: 296884 Color: 0

Bin 658: 805 of cap free
Amount of items: 3
Items: 
Size: 342889 Color: 1
Size: 335839 Color: 1
Size: 320468 Color: 0

Bin 659: 890 of cap free
Amount of items: 3
Items: 
Size: 356468 Color: 1
Size: 344313 Color: 1
Size: 298330 Color: 0

Bin 660: 1293 of cap free
Amount of items: 3
Items: 
Size: 423341 Color: 1
Size: 319897 Color: 1
Size: 255470 Color: 0

Bin 661: 2433 of cap free
Amount of items: 3
Items: 
Size: 377400 Color: 1
Size: 324490 Color: 1
Size: 295678 Color: 0

Bin 662: 2883 of cap free
Amount of items: 3
Items: 
Size: 374015 Color: 1
Size: 357395 Color: 1
Size: 265708 Color: 0

Bin 663: 2954 of cap free
Amount of items: 3
Items: 
Size: 468814 Color: 1
Size: 273263 Color: 1
Size: 254970 Color: 0

Bin 664: 6419 of cap free
Amount of items: 3
Items: 
Size: 386042 Color: 1
Size: 312349 Color: 1
Size: 295191 Color: 0

Bin 665: 62242 of cap free
Amount of items: 3
Items: 
Size: 353584 Color: 1
Size: 299837 Color: 1
Size: 284338 Color: 0

Bin 666: 153415 of cap free
Amount of items: 3
Items: 
Size: 295369 Color: 1
Size: 290842 Color: 1
Size: 260375 Color: 0

Bin 667: 252433 of cap free
Amount of items: 2
Items: 
Size: 496558 Color: 1
Size: 251010 Color: 0

Bin 668: 505688 of cap free
Amount of items: 1
Items: 
Size: 494313 Color: 1

Total size: 667000667
Total free space: 1000001

