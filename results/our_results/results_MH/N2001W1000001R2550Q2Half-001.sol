Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 436589 Color: 1
Size: 291472 Color: 1
Size: 271940 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 410909 Color: 1
Size: 303326 Color: 1
Size: 285766 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 432825 Color: 1
Size: 299846 Color: 1
Size: 267330 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 492096 Color: 1
Size: 254046 Color: 1
Size: 253859 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 493333 Color: 1
Size: 255859 Color: 1
Size: 250809 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 421624 Color: 1
Size: 314936 Color: 1
Size: 263441 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 364174 Color: 1
Size: 347605 Color: 1
Size: 288222 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 494053 Color: 1
Size: 253899 Color: 1
Size: 252049 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 464820 Color: 1
Size: 279607 Color: 1
Size: 255574 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 448253 Color: 1
Size: 292914 Color: 1
Size: 258834 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 498576 Color: 1
Size: 250909 Color: 1
Size: 250516 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 419297 Color: 1
Size: 305857 Color: 1
Size: 274847 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 487804 Color: 1
Size: 257214 Color: 1
Size: 254983 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 383897 Color: 1
Size: 308216 Color: 0
Size: 307888 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 489458 Color: 1
Size: 259500 Color: 1
Size: 251043 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 395828 Color: 1
Size: 317213 Color: 1
Size: 286960 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 400702 Color: 1
Size: 348076 Color: 1
Size: 251223 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375634 Color: 1
Size: 353325 Color: 1
Size: 271042 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 413183 Color: 1
Size: 311365 Color: 1
Size: 275453 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 405682 Color: 1
Size: 325439 Color: 1
Size: 268880 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 372653 Color: 1
Size: 354675 Color: 1
Size: 272673 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 447854 Color: 1
Size: 278049 Color: 1
Size: 274098 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 453675 Color: 1
Size: 281614 Color: 1
Size: 264712 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 414593 Color: 1
Size: 308011 Color: 1
Size: 277397 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 447447 Color: 1
Size: 293399 Color: 1
Size: 259155 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 430061 Color: 1
Size: 302134 Color: 1
Size: 267806 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 412418 Color: 1
Size: 329626 Color: 1
Size: 257957 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 481899 Color: 1
Size: 267073 Color: 1
Size: 251029 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 453092 Color: 1
Size: 286120 Color: 1
Size: 260789 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 461254 Color: 1
Size: 276869 Color: 1
Size: 261878 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 480478 Color: 1
Size: 263265 Color: 1
Size: 256258 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 493731 Color: 1
Size: 253411 Color: 1
Size: 252859 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 440211 Color: 1
Size: 300177 Color: 1
Size: 259613 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 391153 Color: 1
Size: 320300 Color: 1
Size: 288548 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 448540 Color: 1
Size: 275952 Color: 1
Size: 275509 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 428945 Color: 1
Size: 302059 Color: 1
Size: 268997 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 352996 Color: 1
Size: 339893 Color: 1
Size: 307112 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 405645 Color: 1
Size: 308681 Color: 1
Size: 285675 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 476210 Color: 1
Size: 262171 Color: 1
Size: 261620 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 402929 Color: 1
Size: 307626 Color: 1
Size: 289446 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 453816 Color: 1
Size: 284752 Color: 1
Size: 261433 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 348584 Color: 1
Size: 333755 Color: 1
Size: 317662 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 362629 Color: 1
Size: 358598 Color: 1
Size: 278774 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 389464 Color: 1
Size: 332936 Color: 1
Size: 277601 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 455142 Color: 1
Size: 282316 Color: 1
Size: 262543 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 415924 Color: 1
Size: 308648 Color: 1
Size: 275429 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 438511 Color: 1
Size: 289943 Color: 1
Size: 271547 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 383121 Color: 1
Size: 357975 Color: 1
Size: 258905 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 456726 Color: 1
Size: 292079 Color: 1
Size: 251196 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 482155 Color: 1
Size: 265490 Color: 1
Size: 252356 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 495371 Color: 1
Size: 254566 Color: 1
Size: 250064 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 380768 Color: 1
Size: 333055 Color: 1
Size: 286178 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 398294 Color: 1
Size: 332918 Color: 1
Size: 268789 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 494097 Color: 1
Size: 253337 Color: 1
Size: 252567 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 469577 Color: 1
Size: 277468 Color: 1
Size: 252956 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 434852 Color: 1
Size: 303966 Color: 1
Size: 261183 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 416887 Color: 1
Size: 315224 Color: 1
Size: 267890 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 356690 Color: 1
Size: 330489 Color: 1
Size: 312822 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 410711 Color: 1
Size: 334098 Color: 1
Size: 255192 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 495042 Color: 1
Size: 253650 Color: 1
Size: 251309 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 470971 Color: 1
Size: 275868 Color: 1
Size: 253162 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 487823 Color: 1
Size: 258911 Color: 1
Size: 253267 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 479724 Color: 1
Size: 270276 Color: 1
Size: 250001 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 438112 Color: 1
Size: 289149 Color: 1
Size: 272740 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 475491 Color: 1
Size: 272590 Color: 1
Size: 251920 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 488551 Color: 1
Size: 256151 Color: 1
Size: 255299 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 451002 Color: 1
Size: 280696 Color: 1
Size: 268303 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 477811 Color: 1
Size: 269844 Color: 1
Size: 252346 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 463775 Color: 1
Size: 285665 Color: 1
Size: 250561 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 462633 Color: 1
Size: 270431 Color: 1
Size: 266937 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 465220 Color: 1
Size: 276602 Color: 1
Size: 258179 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 471648 Color: 1
Size: 272730 Color: 1
Size: 255623 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 474213 Color: 1
Size: 272593 Color: 1
Size: 253195 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 461479 Color: 1
Size: 279759 Color: 1
Size: 258763 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 431399 Color: 1
Size: 291331 Color: 1
Size: 277271 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 380986 Color: 1
Size: 358455 Color: 1
Size: 260560 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 442469 Color: 1
Size: 300702 Color: 1
Size: 256830 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 487280 Color: 1
Size: 258940 Color: 1
Size: 253781 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 499282 Color: 1
Size: 250424 Color: 1
Size: 250295 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 456035 Color: 1
Size: 278165 Color: 1
Size: 265801 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 335181 Color: 1
Size: 334411 Color: 1
Size: 330409 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 413759 Color: 1
Size: 318925 Color: 1
Size: 267317 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 393691 Color: 1
Size: 316017 Color: 1
Size: 290293 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 464934 Color: 1
Size: 271382 Color: 1
Size: 263685 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 404597 Color: 1
Size: 321892 Color: 1
Size: 273512 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 382180 Color: 1
Size: 324585 Color: 1
Size: 293236 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 423253 Color: 1
Size: 297177 Color: 1
Size: 279571 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 406610 Color: 1
Size: 303818 Color: 1
Size: 289573 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 486828 Color: 1
Size: 259846 Color: 1
Size: 253327 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 468554 Color: 1
Size: 280514 Color: 1
Size: 250933 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 433624 Color: 1
Size: 310081 Color: 1
Size: 256296 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 430691 Color: 1
Size: 311775 Color: 1
Size: 257535 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 357207 Color: 1
Size: 331817 Color: 1
Size: 310977 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 453117 Color: 1
Size: 289936 Color: 1
Size: 256948 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 497319 Color: 1
Size: 252217 Color: 1
Size: 250465 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 430369 Color: 1
Size: 318854 Color: 1
Size: 250778 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 424082 Color: 1
Size: 312770 Color: 1
Size: 263149 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 436344 Color: 1
Size: 306604 Color: 1
Size: 257053 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 371198 Color: 1
Size: 349849 Color: 1
Size: 278954 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 363065 Color: 1
Size: 335106 Color: 1
Size: 301830 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 370187 Color: 1
Size: 347731 Color: 1
Size: 282083 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 477172 Color: 1
Size: 264480 Color: 1
Size: 258349 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 369081 Color: 1
Size: 344254 Color: 1
Size: 286666 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 337492 Color: 1
Size: 333897 Color: 1
Size: 328612 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 374142 Color: 1
Size: 340856 Color: 1
Size: 285003 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 407083 Color: 1
Size: 329550 Color: 1
Size: 263368 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 495170 Color: 1
Size: 253864 Color: 1
Size: 250967 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 380516 Color: 1
Size: 317822 Color: 0
Size: 301663 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 438432 Color: 1
Size: 295548 Color: 1
Size: 266021 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 375518 Color: 1
Size: 343418 Color: 1
Size: 281065 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 466637 Color: 1
Size: 269230 Color: 1
Size: 264134 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 444509 Color: 1
Size: 298752 Color: 1
Size: 256740 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 354013 Color: 1
Size: 340205 Color: 1
Size: 305783 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 425034 Color: 1
Size: 324869 Color: 1
Size: 250098 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 495407 Color: 1
Size: 253564 Color: 1
Size: 251030 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 458026 Color: 1
Size: 271546 Color: 1
Size: 270429 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 467344 Color: 1
Size: 280453 Color: 1
Size: 252204 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 393325 Color: 1
Size: 328635 Color: 1
Size: 278041 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 418108 Color: 1
Size: 292702 Color: 1
Size: 289191 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 443921 Color: 1
Size: 285927 Color: 1
Size: 270153 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 375236 Color: 1
Size: 331423 Color: 1
Size: 293342 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 472593 Color: 1
Size: 270952 Color: 1
Size: 256456 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 474769 Color: 1
Size: 263826 Color: 1
Size: 261406 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 437182 Color: 1
Size: 290459 Color: 1
Size: 272360 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 435584 Color: 1
Size: 285770 Color: 0
Size: 278647 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 443654 Color: 1
Size: 279078 Color: 1
Size: 277269 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 349891 Color: 1
Size: 337774 Color: 1
Size: 312336 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 469433 Color: 1
Size: 276690 Color: 1
Size: 253878 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 468632 Color: 1
Size: 278226 Color: 1
Size: 253143 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 478364 Color: 1
Size: 264740 Color: 1
Size: 256897 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 374440 Color: 1
Size: 345329 Color: 1
Size: 280232 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 376433 Color: 1
Size: 339199 Color: 1
Size: 284369 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 395071 Color: 1
Size: 312350 Color: 1
Size: 292580 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 473374 Color: 1
Size: 267902 Color: 1
Size: 258725 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 465692 Color: 1
Size: 279504 Color: 1
Size: 254805 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 457511 Color: 1
Size: 280535 Color: 1
Size: 261955 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 401231 Color: 1
Size: 324907 Color: 1
Size: 273863 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 396798 Color: 1
Size: 316763 Color: 1
Size: 286440 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 391476 Color: 1
Size: 315379 Color: 1
Size: 293146 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 363803 Color: 1
Size: 356117 Color: 1
Size: 280081 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 387263 Color: 1
Size: 348105 Color: 1
Size: 264633 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 425726 Color: 1
Size: 310598 Color: 1
Size: 263677 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 424983 Color: 1
Size: 291001 Color: 1
Size: 284017 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 490007 Color: 1
Size: 255351 Color: 1
Size: 254643 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 389230 Color: 1
Size: 326673 Color: 1
Size: 284098 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 435003 Color: 1
Size: 295712 Color: 1
Size: 269286 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 348618 Color: 1
Size: 342076 Color: 1
Size: 309307 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 393642 Color: 1
Size: 313845 Color: 1
Size: 292514 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 469267 Color: 1
Size: 268629 Color: 1
Size: 262105 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 491868 Color: 1
Size: 256918 Color: 1
Size: 251215 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 385847 Color: 1
Size: 312509 Color: 1
Size: 301645 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 411583 Color: 1
Size: 320219 Color: 1
Size: 268199 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 403822 Color: 1
Size: 334545 Color: 1
Size: 261634 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 364270 Color: 1
Size: 325983 Color: 1
Size: 309748 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 404531 Color: 1
Size: 324317 Color: 1
Size: 271153 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 375816 Color: 1
Size: 363767 Color: 1
Size: 260418 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 384576 Color: 1
Size: 354924 Color: 1
Size: 260501 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 438773 Color: 1
Size: 295726 Color: 0
Size: 265502 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 495226 Color: 1
Size: 253244 Color: 1
Size: 251531 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 492759 Color: 1
Size: 255334 Color: 1
Size: 251908 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 484524 Color: 1
Size: 258322 Color: 1
Size: 257155 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 418217 Color: 1
Size: 308249 Color: 1
Size: 273535 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 406745 Color: 1
Size: 327414 Color: 1
Size: 265842 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 427905 Color: 1
Size: 306679 Color: 1
Size: 265417 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 445201 Color: 1
Size: 294488 Color: 1
Size: 260312 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 463596 Color: 1
Size: 278816 Color: 1
Size: 257589 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 491200 Color: 1
Size: 256199 Color: 1
Size: 252602 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 382679 Color: 1
Size: 351433 Color: 1
Size: 265889 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 375358 Color: 1
Size: 360717 Color: 1
Size: 263926 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 494001 Color: 1
Size: 254241 Color: 1
Size: 251759 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 490063 Color: 1
Size: 259852 Color: 1
Size: 250086 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 416531 Color: 1
Size: 318561 Color: 1
Size: 264909 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 363165 Color: 1
Size: 342798 Color: 1
Size: 294038 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 396775 Color: 1
Size: 348774 Color: 1
Size: 254452 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 381951 Color: 1
Size: 325109 Color: 1
Size: 292941 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 442241 Color: 1
Size: 290662 Color: 1
Size: 267098 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 412547 Color: 1
Size: 300996 Color: 1
Size: 286458 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 468626 Color: 1
Size: 276871 Color: 1
Size: 254504 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 391369 Color: 1
Size: 332510 Color: 1
Size: 276122 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 403461 Color: 1
Size: 319034 Color: 1
Size: 277506 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 447782 Color: 1
Size: 286900 Color: 1
Size: 265319 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 405860 Color: 1
Size: 335109 Color: 1
Size: 259032 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 414319 Color: 1
Size: 314928 Color: 1
Size: 270754 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 452238 Color: 1
Size: 289520 Color: 1
Size: 258243 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 400300 Color: 1
Size: 345184 Color: 1
Size: 254517 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 443736 Color: 1
Size: 296641 Color: 1
Size: 259624 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 457317 Color: 1
Size: 275859 Color: 0
Size: 266825 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 402805 Color: 1
Size: 304876 Color: 1
Size: 292320 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 457678 Color: 1
Size: 289583 Color: 1
Size: 252740 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 483747 Color: 1
Size: 259566 Color: 1
Size: 256688 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 437159 Color: 1
Size: 303579 Color: 1
Size: 259263 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 352130 Color: 1
Size: 348929 Color: 1
Size: 298942 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 394613 Color: 1
Size: 354283 Color: 1
Size: 251105 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 461821 Color: 1
Size: 279396 Color: 1
Size: 258784 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 393580 Color: 1
Size: 304871 Color: 1
Size: 301550 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 368114 Color: 1
Size: 345122 Color: 1
Size: 286765 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 358820 Color: 1
Size: 351919 Color: 1
Size: 289262 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 431396 Color: 1
Size: 303873 Color: 1
Size: 264732 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 395854 Color: 1
Size: 335086 Color: 1
Size: 269061 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 468334 Color: 1
Size: 271723 Color: 1
Size: 259944 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 482006 Color: 1
Size: 260927 Color: 1
Size: 257068 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 416850 Color: 1
Size: 324978 Color: 1
Size: 258173 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 422027 Color: 1
Size: 299072 Color: 1
Size: 278902 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 364821 Color: 1
Size: 348419 Color: 1
Size: 286761 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 469797 Color: 1
Size: 270588 Color: 1
Size: 259616 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 485363 Color: 1
Size: 262137 Color: 1
Size: 252501 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 441315 Color: 1
Size: 279627 Color: 1
Size: 279059 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 360634 Color: 1
Size: 342159 Color: 1
Size: 297208 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 346964 Color: 1
Size: 334462 Color: 1
Size: 318575 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 403190 Color: 1
Size: 308404 Color: 1
Size: 288407 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 480509 Color: 1
Size: 267086 Color: 1
Size: 252406 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 458852 Color: 1
Size: 284331 Color: 1
Size: 256818 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 433586 Color: 1
Size: 310041 Color: 1
Size: 256374 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 425223 Color: 1
Size: 317072 Color: 1
Size: 257706 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 486537 Color: 1
Size: 259634 Color: 1
Size: 253830 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 373657 Color: 1
Size: 318721 Color: 0
Size: 307623 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 408414 Color: 1
Size: 311893 Color: 1
Size: 279694 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 477737 Color: 1
Size: 264658 Color: 1
Size: 257606 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 416027 Color: 1
Size: 307086 Color: 1
Size: 276888 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 346264 Color: 1
Size: 338738 Color: 1
Size: 314999 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 427868 Color: 1
Size: 305039 Color: 1
Size: 267094 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 417150 Color: 1
Size: 309048 Color: 1
Size: 273803 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 438454 Color: 1
Size: 299097 Color: 1
Size: 262450 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 362219 Color: 1
Size: 357766 Color: 1
Size: 280016 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 358447 Color: 1
Size: 341537 Color: 1
Size: 300017 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 459520 Color: 1
Size: 277705 Color: 1
Size: 262776 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 390106 Color: 1
Size: 330431 Color: 1
Size: 279464 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 456873 Color: 1
Size: 288870 Color: 1
Size: 254258 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 363971 Color: 1
Size: 348134 Color: 1
Size: 287896 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 410268 Color: 1
Size: 329051 Color: 1
Size: 260682 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 422578 Color: 1
Size: 315408 Color: 1
Size: 262015 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 470677 Color: 1
Size: 269452 Color: 0
Size: 259872 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 363497 Color: 1
Size: 344157 Color: 1
Size: 292347 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 423240 Color: 1
Size: 300159 Color: 1
Size: 276602 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 437682 Color: 1
Size: 299291 Color: 0
Size: 263028 Color: 1

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 464873 Color: 1
Size: 277193 Color: 1
Size: 257935 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 483030 Color: 1
Size: 263702 Color: 1
Size: 253269 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 369351 Color: 1
Size: 366149 Color: 1
Size: 264501 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 418877 Color: 1
Size: 310087 Color: 1
Size: 271037 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 451012 Color: 1
Size: 287132 Color: 1
Size: 261857 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 467671 Color: 1
Size: 274712 Color: 1
Size: 257618 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 486079 Color: 1
Size: 257019 Color: 1
Size: 256903 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 378266 Color: 1
Size: 366480 Color: 1
Size: 255255 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 473104 Color: 1
Size: 265606 Color: 1
Size: 261291 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 478602 Color: 1
Size: 270252 Color: 1
Size: 251147 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 450367 Color: 1
Size: 298805 Color: 1
Size: 250829 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 430389 Color: 1
Size: 311186 Color: 1
Size: 258426 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 464695 Color: 1
Size: 267999 Color: 1
Size: 267307 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 378576 Color: 1
Size: 360144 Color: 1
Size: 261281 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 447840 Color: 1
Size: 285931 Color: 1
Size: 266230 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 498215 Color: 1
Size: 251306 Color: 1
Size: 250480 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 471407 Color: 1
Size: 267524 Color: 1
Size: 261070 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 400868 Color: 1
Size: 320056 Color: 1
Size: 279077 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 443460 Color: 1
Size: 291402 Color: 1
Size: 265139 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 355803 Color: 1
Size: 325937 Color: 0
Size: 318261 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 383897 Color: 1
Size: 327556 Color: 1
Size: 288548 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 361716 Color: 1
Size: 344013 Color: 1
Size: 294272 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 471378 Color: 1
Size: 276841 Color: 1
Size: 251782 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 389459 Color: 1
Size: 315583 Color: 1
Size: 294959 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 365465 Color: 1
Size: 362537 Color: 1
Size: 271999 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 364318 Color: 1
Size: 346271 Color: 1
Size: 289412 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 410025 Color: 1
Size: 323708 Color: 1
Size: 266268 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 464643 Color: 1
Size: 273761 Color: 1
Size: 261597 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 497436 Color: 1
Size: 251712 Color: 1
Size: 250853 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 436256 Color: 1
Size: 292509 Color: 1
Size: 271236 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 387330 Color: 1
Size: 332945 Color: 1
Size: 279726 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 349830 Color: 1
Size: 338994 Color: 1
Size: 311177 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 480820 Color: 1
Size: 262268 Color: 1
Size: 256913 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 418028 Color: 1
Size: 317237 Color: 1
Size: 264736 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 434960 Color: 1
Size: 282905 Color: 1
Size: 282136 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 497670 Color: 1
Size: 252111 Color: 1
Size: 250220 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 377802 Color: 1
Size: 345770 Color: 1
Size: 276429 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 451707 Color: 1
Size: 293469 Color: 1
Size: 254825 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 433683 Color: 1
Size: 305202 Color: 1
Size: 261116 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 379331 Color: 1
Size: 333522 Color: 1
Size: 287148 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 456841 Color: 1
Size: 290105 Color: 1
Size: 253055 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 473598 Color: 1
Size: 270893 Color: 0
Size: 255510 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 367594 Color: 1
Size: 351144 Color: 1
Size: 281263 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 391279 Color: 1
Size: 340852 Color: 1
Size: 267870 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 382782 Color: 1
Size: 329745 Color: 1
Size: 287474 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 484173 Color: 1
Size: 259771 Color: 1
Size: 256057 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 407662 Color: 1
Size: 326594 Color: 1
Size: 265745 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 486125 Color: 1
Size: 261624 Color: 1
Size: 252252 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 438769 Color: 1
Size: 294742 Color: 1
Size: 266490 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 470580 Color: 1
Size: 277479 Color: 1
Size: 251942 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 419402 Color: 1
Size: 329334 Color: 1
Size: 251265 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 465254 Color: 1
Size: 283652 Color: 1
Size: 251095 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 405773 Color: 1
Size: 314416 Color: 1
Size: 279812 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 463329 Color: 1
Size: 280719 Color: 1
Size: 255953 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 373280 Color: 1
Size: 336895 Color: 1
Size: 289826 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 430447 Color: 1
Size: 313015 Color: 1
Size: 256539 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 454736 Color: 1
Size: 286039 Color: 1
Size: 259226 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 479398 Color: 1
Size: 266168 Color: 1
Size: 254435 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 460747 Color: 1
Size: 282985 Color: 1
Size: 256269 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 383046 Color: 1
Size: 345719 Color: 1
Size: 271236 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 474607 Color: 1
Size: 268336 Color: 1
Size: 257058 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 480513 Color: 1
Size: 268405 Color: 1
Size: 251083 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 418258 Color: 1
Size: 311670 Color: 1
Size: 270073 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 375411 Color: 1
Size: 345228 Color: 1
Size: 279362 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 461529 Color: 1
Size: 272332 Color: 1
Size: 266140 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 458317 Color: 1
Size: 283240 Color: 1
Size: 258444 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 380622 Color: 1
Size: 326875 Color: 1
Size: 292504 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 471469 Color: 1
Size: 276272 Color: 1
Size: 252260 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 395397 Color: 1
Size: 345720 Color: 1
Size: 258884 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 404295 Color: 1
Size: 337519 Color: 1
Size: 258187 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 417075 Color: 1
Size: 313850 Color: 1
Size: 269076 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 376896 Color: 1
Size: 368822 Color: 1
Size: 254283 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 480823 Color: 1
Size: 262267 Color: 1
Size: 256911 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 433870 Color: 1
Size: 297617 Color: 1
Size: 268514 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 448888 Color: 1
Size: 283143 Color: 1
Size: 267970 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 391786 Color: 1
Size: 342085 Color: 1
Size: 266130 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 462540 Color: 1
Size: 274201 Color: 1
Size: 263260 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 460648 Color: 1
Size: 285366 Color: 1
Size: 253987 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 494248 Color: 1
Size: 253139 Color: 1
Size: 252614 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 374736 Color: 1
Size: 349008 Color: 1
Size: 276257 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 421619 Color: 1
Size: 310081 Color: 1
Size: 268301 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 392208 Color: 1
Size: 356177 Color: 1
Size: 251616 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 435034 Color: 1
Size: 308492 Color: 1
Size: 256475 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 393421 Color: 1
Size: 328481 Color: 1
Size: 278099 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 403217 Color: 1
Size: 327775 Color: 1
Size: 269009 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 351098 Color: 1
Size: 348395 Color: 1
Size: 300508 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 438276 Color: 1
Size: 281652 Color: 1
Size: 280073 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 485656 Color: 1
Size: 262682 Color: 1
Size: 251663 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 461127 Color: 1
Size: 281056 Color: 1
Size: 257818 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 453509 Color: 1
Size: 285022 Color: 1
Size: 261470 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 364817 Color: 1
Size: 334409 Color: 1
Size: 300775 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 397650 Color: 1
Size: 338469 Color: 1
Size: 263882 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 400124 Color: 1
Size: 334012 Color: 1
Size: 265865 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 404850 Color: 1
Size: 307090 Color: 1
Size: 288061 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 370653 Color: 1
Size: 342753 Color: 1
Size: 286595 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 498713 Color: 1
Size: 250842 Color: 1
Size: 250446 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 454458 Color: 1
Size: 283483 Color: 1
Size: 262060 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 467864 Color: 1
Size: 279076 Color: 1
Size: 253061 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 464058 Color: 1
Size: 285117 Color: 1
Size: 250826 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 375477 Color: 1
Size: 358968 Color: 1
Size: 265556 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 414689 Color: 1
Size: 320844 Color: 1
Size: 264468 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 375897 Color: 1
Size: 331345 Color: 1
Size: 292759 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 390803 Color: 1
Size: 355195 Color: 1
Size: 254003 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 374344 Color: 1
Size: 321028 Color: 1
Size: 304629 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 378052 Color: 1
Size: 328901 Color: 1
Size: 293048 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 411557 Color: 1
Size: 294757 Color: 1
Size: 293687 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 357673 Color: 1
Size: 326099 Color: 1
Size: 316229 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 451272 Color: 1
Size: 280302 Color: 1
Size: 268427 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 412004 Color: 1
Size: 311248 Color: 1
Size: 276749 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 435757 Color: 1
Size: 300390 Color: 1
Size: 263854 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 424136 Color: 1
Size: 303510 Color: 1
Size: 272355 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 353876 Color: 1
Size: 334855 Color: 1
Size: 311270 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 437305 Color: 1
Size: 305332 Color: 1
Size: 257364 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 371650 Color: 1
Size: 333114 Color: 1
Size: 295237 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 356090 Color: 1
Size: 334974 Color: 1
Size: 308937 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 378117 Color: 1
Size: 369715 Color: 1
Size: 252169 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 415954 Color: 1
Size: 308212 Color: 1
Size: 275835 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 472715 Color: 1
Size: 266761 Color: 1
Size: 260525 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 420182 Color: 1
Size: 329340 Color: 1
Size: 250479 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 419207 Color: 1
Size: 316939 Color: 1
Size: 263855 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 364473 Color: 1
Size: 358982 Color: 1
Size: 276546 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 361702 Color: 1
Size: 351200 Color: 1
Size: 287099 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 415916 Color: 1
Size: 315534 Color: 1
Size: 268551 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 374782 Color: 1
Size: 350660 Color: 1
Size: 274559 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 368984 Color: 1
Size: 357653 Color: 1
Size: 273364 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 416793 Color: 1
Size: 303309 Color: 1
Size: 279899 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 368246 Color: 1
Size: 352159 Color: 1
Size: 279596 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 366907 Color: 1
Size: 318171 Color: 1
Size: 314923 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 451596 Color: 1
Size: 294858 Color: 1
Size: 253547 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 352541 Color: 1
Size: 348927 Color: 1
Size: 298533 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 456245 Color: 1
Size: 277536 Color: 1
Size: 266220 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 389724 Color: 1
Size: 359082 Color: 1
Size: 251195 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 483504 Color: 1
Size: 258670 Color: 1
Size: 257827 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 389852 Color: 1
Size: 310747 Color: 0
Size: 299402 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 417577 Color: 1
Size: 302088 Color: 1
Size: 280336 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 383923 Color: 1
Size: 320423 Color: 1
Size: 295655 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 364460 Color: 1
Size: 350323 Color: 1
Size: 285218 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 360109 Color: 1
Size: 350013 Color: 1
Size: 289879 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 450217 Color: 1
Size: 283890 Color: 1
Size: 265894 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 402875 Color: 1
Size: 315025 Color: 1
Size: 282101 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 466101 Color: 1
Size: 269063 Color: 1
Size: 264837 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 370680 Color: 1
Size: 339458 Color: 1
Size: 289863 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 352332 Color: 1
Size: 348237 Color: 1
Size: 299432 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 391840 Color: 1
Size: 341047 Color: 1
Size: 267114 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 409977 Color: 1
Size: 296066 Color: 1
Size: 293958 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 442216 Color: 1
Size: 280265 Color: 1
Size: 277520 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 430555 Color: 1
Size: 316365 Color: 1
Size: 253081 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 490975 Color: 1
Size: 256679 Color: 1
Size: 252347 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 382235 Color: 1
Size: 335213 Color: 1
Size: 282553 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 402570 Color: 1
Size: 331836 Color: 1
Size: 265595 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 440203 Color: 1
Size: 299262 Color: 1
Size: 260536 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 426523 Color: 1
Size: 298164 Color: 1
Size: 275314 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 363256 Color: 1
Size: 353705 Color: 1
Size: 283040 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 483070 Color: 1
Size: 259247 Color: 1
Size: 257684 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 380188 Color: 1
Size: 320453 Color: 1
Size: 299360 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 371736 Color: 1
Size: 322853 Color: 1
Size: 305412 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 493085 Color: 1
Size: 255627 Color: 1
Size: 251289 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 391999 Color: 1
Size: 344574 Color: 1
Size: 263428 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 366252 Color: 1
Size: 318238 Color: 1
Size: 315511 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 455085 Color: 1
Size: 285912 Color: 1
Size: 259004 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 428057 Color: 1
Size: 295385 Color: 1
Size: 276559 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 378455 Color: 1
Size: 371040 Color: 1
Size: 250506 Color: 0

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 418745 Color: 1
Size: 299491 Color: 1
Size: 281764 Color: 0

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 408866 Color: 1
Size: 304761 Color: 1
Size: 286373 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 352247 Color: 1
Size: 337630 Color: 1
Size: 310123 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 400726 Color: 1
Size: 343955 Color: 1
Size: 255319 Color: 0

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 426291 Color: 1
Size: 318787 Color: 1
Size: 254922 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 390617 Color: 1
Size: 343575 Color: 1
Size: 265808 Color: 0

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 354993 Color: 1
Size: 347832 Color: 1
Size: 297175 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 424821 Color: 1
Size: 293606 Color: 0
Size: 281573 Color: 1

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 359083 Color: 1
Size: 336665 Color: 1
Size: 304252 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 415356 Color: 1
Size: 333910 Color: 1
Size: 250734 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 412329 Color: 1
Size: 312272 Color: 1
Size: 275399 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 394411 Color: 1
Size: 338874 Color: 1
Size: 266715 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 413765 Color: 1
Size: 327496 Color: 1
Size: 258739 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 394634 Color: 1
Size: 331878 Color: 1
Size: 273488 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 456611 Color: 1
Size: 279358 Color: 0
Size: 264031 Color: 1

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 377051 Color: 1
Size: 367700 Color: 1
Size: 255249 Color: 0

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 449418 Color: 1
Size: 294861 Color: 1
Size: 255721 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 467838 Color: 1
Size: 270672 Color: 1
Size: 261490 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 408247 Color: 1
Size: 327296 Color: 1
Size: 264457 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 378456 Color: 1
Size: 350072 Color: 1
Size: 271472 Color: 0

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 413562 Color: 1
Size: 299970 Color: 1
Size: 286468 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 431824 Color: 1
Size: 292677 Color: 0
Size: 275499 Color: 1

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 388286 Color: 1
Size: 311587 Color: 1
Size: 300127 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 416958 Color: 1
Size: 318156 Color: 1
Size: 264886 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 383735 Color: 1
Size: 344456 Color: 1
Size: 271809 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 381884 Color: 1
Size: 335806 Color: 1
Size: 282310 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 418041 Color: 1
Size: 296840 Color: 1
Size: 285119 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 435534 Color: 1
Size: 312242 Color: 1
Size: 252224 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 430086 Color: 1
Size: 303749 Color: 1
Size: 266165 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 399061 Color: 1
Size: 301054 Color: 0
Size: 299885 Color: 1

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 403603 Color: 1
Size: 299155 Color: 0
Size: 297242 Color: 1

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 360053 Color: 1
Size: 352388 Color: 1
Size: 287559 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 469016 Color: 1
Size: 268921 Color: 1
Size: 262063 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 407234 Color: 1
Size: 320123 Color: 1
Size: 272643 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 428737 Color: 1
Size: 306192 Color: 1
Size: 265071 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 443469 Color: 1
Size: 302977 Color: 1
Size: 253554 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 386203 Color: 1
Size: 315734 Color: 1
Size: 298063 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 360915 Color: 1
Size: 341075 Color: 1
Size: 298010 Color: 0

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 414543 Color: 1
Size: 306078 Color: 0
Size: 279379 Color: 1

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 394664 Color: 1
Size: 343187 Color: 1
Size: 262149 Color: 0

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 441684 Color: 1
Size: 304932 Color: 1
Size: 253384 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 398828 Color: 1
Size: 302637 Color: 1
Size: 298535 Color: 0

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 433168 Color: 1
Size: 299266 Color: 1
Size: 267566 Color: 0

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 410413 Color: 1
Size: 326691 Color: 1
Size: 262896 Color: 0

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 407120 Color: 1
Size: 303477 Color: 1
Size: 289403 Color: 0

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 341225 Color: 1
Size: 338125 Color: 1
Size: 320650 Color: 0

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 352998 Color: 1
Size: 343762 Color: 1
Size: 303239 Color: 0

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 407734 Color: 1
Size: 319152 Color: 1
Size: 273113 Color: 0

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 431560 Color: 1
Size: 311375 Color: 1
Size: 257064 Color: 0

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 456762 Color: 1
Size: 283558 Color: 1
Size: 259679 Color: 0

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 491833 Color: 1
Size: 256811 Color: 0
Size: 251355 Color: 1

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 369869 Color: 1
Size: 316687 Color: 1
Size: 313443 Color: 0

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 386776 Color: 1
Size: 315351 Color: 1
Size: 297872 Color: 0

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 363011 Color: 1
Size: 336581 Color: 1
Size: 300407 Color: 0

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 453930 Color: 1
Size: 286547 Color: 1
Size: 259522 Color: 0

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 387164 Color: 1
Size: 327556 Color: 1
Size: 285279 Color: 0

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 472067 Color: 1
Size: 277026 Color: 1
Size: 250906 Color: 0

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 422716 Color: 1
Size: 315363 Color: 1
Size: 261920 Color: 0

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 395146 Color: 1
Size: 311623 Color: 1
Size: 293230 Color: 0

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 383986 Color: 1
Size: 334330 Color: 1
Size: 281683 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 393408 Color: 1
Size: 319427 Color: 1
Size: 287164 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 422417 Color: 1
Size: 318823 Color: 1
Size: 258759 Color: 0

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 355517 Color: 1
Size: 348928 Color: 1
Size: 295554 Color: 0

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 358873 Color: 1
Size: 340346 Color: 1
Size: 300780 Color: 0

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 416253 Color: 1
Size: 292900 Color: 1
Size: 290846 Color: 0

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 420097 Color: 1
Size: 327694 Color: 1
Size: 252208 Color: 0

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 358276 Color: 1
Size: 344427 Color: 1
Size: 297296 Color: 0

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 396534 Color: 1
Size: 319923 Color: 1
Size: 283542 Color: 0

Bin 466: 3 of cap free
Amount of items: 3
Items: 
Size: 424852 Color: 1
Size: 289887 Color: 0
Size: 285259 Color: 1

Bin 467: 3 of cap free
Amount of items: 3
Items: 
Size: 371945 Color: 1
Size: 350521 Color: 1
Size: 277532 Color: 0

Bin 468: 3 of cap free
Amount of items: 3
Items: 
Size: 377387 Color: 1
Size: 371332 Color: 1
Size: 251279 Color: 0

Bin 469: 3 of cap free
Amount of items: 3
Items: 
Size: 474750 Color: 1
Size: 271656 Color: 1
Size: 253592 Color: 0

Bin 470: 3 of cap free
Amount of items: 3
Items: 
Size: 379838 Color: 1
Size: 327825 Color: 1
Size: 292335 Color: 0

Bin 471: 3 of cap free
Amount of items: 3
Items: 
Size: 410086 Color: 1
Size: 328265 Color: 1
Size: 261647 Color: 0

Bin 472: 3 of cap free
Amount of items: 3
Items: 
Size: 426698 Color: 1
Size: 317842 Color: 1
Size: 255458 Color: 0

Bin 473: 3 of cap free
Amount of items: 3
Items: 
Size: 375762 Color: 1
Size: 353793 Color: 1
Size: 270443 Color: 0

Bin 474: 3 of cap free
Amount of items: 3
Items: 
Size: 423007 Color: 1
Size: 313519 Color: 1
Size: 263472 Color: 0

Bin 475: 3 of cap free
Amount of items: 3
Items: 
Size: 395100 Color: 1
Size: 351775 Color: 1
Size: 253123 Color: 0

Bin 476: 3 of cap free
Amount of items: 3
Items: 
Size: 367614 Color: 1
Size: 353993 Color: 1
Size: 278391 Color: 0

Bin 477: 3 of cap free
Amount of items: 3
Items: 
Size: 381658 Color: 1
Size: 361282 Color: 1
Size: 257058 Color: 0

Bin 478: 3 of cap free
Amount of items: 3
Items: 
Size: 409264 Color: 1
Size: 328023 Color: 1
Size: 262711 Color: 0

Bin 479: 3 of cap free
Amount of items: 3
Items: 
Size: 358779 Color: 1
Size: 329640 Color: 1
Size: 311579 Color: 0

Bin 480: 3 of cap free
Amount of items: 3
Items: 
Size: 448315 Color: 1
Size: 299398 Color: 1
Size: 252285 Color: 0

Bin 481: 3 of cap free
Amount of items: 3
Items: 
Size: 360317 Color: 1
Size: 358653 Color: 1
Size: 281028 Color: 0

Bin 482: 3 of cap free
Amount of items: 3
Items: 
Size: 365842 Color: 1
Size: 341662 Color: 1
Size: 292494 Color: 0

Bin 483: 3 of cap free
Amount of items: 3
Items: 
Size: 341418 Color: 1
Size: 340656 Color: 1
Size: 317924 Color: 0

Bin 484: 3 of cap free
Amount of items: 3
Items: 
Size: 352261 Color: 1
Size: 352109 Color: 1
Size: 295628 Color: 0

Bin 485: 4 of cap free
Amount of items: 3
Items: 
Size: 402854 Color: 1
Size: 344374 Color: 1
Size: 252769 Color: 0

Bin 486: 4 of cap free
Amount of items: 3
Items: 
Size: 431810 Color: 1
Size: 306585 Color: 1
Size: 261602 Color: 0

Bin 487: 4 of cap free
Amount of items: 3
Items: 
Size: 410219 Color: 1
Size: 329746 Color: 1
Size: 260032 Color: 0

Bin 488: 4 of cap free
Amount of items: 3
Items: 
Size: 357654 Color: 1
Size: 356753 Color: 1
Size: 285590 Color: 0

Bin 489: 4 of cap free
Amount of items: 3
Items: 
Size: 394429 Color: 1
Size: 325871 Color: 1
Size: 279697 Color: 0

Bin 490: 4 of cap free
Amount of items: 3
Items: 
Size: 395814 Color: 1
Size: 309720 Color: 0
Size: 294463 Color: 1

Bin 491: 4 of cap free
Amount of items: 3
Items: 
Size: 413261 Color: 1
Size: 312409 Color: 0
Size: 274327 Color: 1

Bin 492: 4 of cap free
Amount of items: 3
Items: 
Size: 413746 Color: 1
Size: 305086 Color: 1
Size: 281165 Color: 0

Bin 493: 4 of cap free
Amount of items: 3
Items: 
Size: 416024 Color: 1
Size: 313742 Color: 1
Size: 270231 Color: 0

Bin 494: 4 of cap free
Amount of items: 3
Items: 
Size: 450897 Color: 1
Size: 280870 Color: 1
Size: 268230 Color: 0

Bin 495: 4 of cap free
Amount of items: 3
Items: 
Size: 365797 Color: 1
Size: 348771 Color: 1
Size: 285429 Color: 0

Bin 496: 4 of cap free
Amount of items: 3
Items: 
Size: 482657 Color: 1
Size: 267254 Color: 1
Size: 250086 Color: 0

Bin 497: 5 of cap free
Amount of items: 3
Items: 
Size: 440962 Color: 1
Size: 307885 Color: 1
Size: 251149 Color: 0

Bin 498: 5 of cap free
Amount of items: 3
Items: 
Size: 446428 Color: 1
Size: 300997 Color: 1
Size: 252571 Color: 0

Bin 499: 5 of cap free
Amount of items: 3
Items: 
Size: 412180 Color: 1
Size: 324643 Color: 1
Size: 263173 Color: 0

Bin 500: 5 of cap free
Amount of items: 3
Items: 
Size: 401890 Color: 1
Size: 299404 Color: 1
Size: 298702 Color: 0

Bin 501: 5 of cap free
Amount of items: 3
Items: 
Size: 406524 Color: 1
Size: 329670 Color: 1
Size: 263802 Color: 0

Bin 502: 5 of cap free
Amount of items: 3
Items: 
Size: 429861 Color: 1
Size: 311483 Color: 1
Size: 258652 Color: 0

Bin 503: 5 of cap free
Amount of items: 3
Items: 
Size: 444553 Color: 1
Size: 288353 Color: 1
Size: 267090 Color: 0

Bin 504: 5 of cap free
Amount of items: 3
Items: 
Size: 403744 Color: 1
Size: 337309 Color: 1
Size: 258943 Color: 0

Bin 505: 5 of cap free
Amount of items: 3
Items: 
Size: 469087 Color: 1
Size: 276008 Color: 1
Size: 254901 Color: 0

Bin 506: 5 of cap free
Amount of items: 3
Items: 
Size: 452495 Color: 1
Size: 295593 Color: 1
Size: 251908 Color: 0

Bin 507: 5 of cap free
Amount of items: 3
Items: 
Size: 394190 Color: 1
Size: 318997 Color: 1
Size: 286809 Color: 0

Bin 508: 5 of cap free
Amount of items: 3
Items: 
Size: 426578 Color: 1
Size: 297600 Color: 1
Size: 275818 Color: 0

Bin 509: 5 of cap free
Amount of items: 3
Items: 
Size: 351279 Color: 1
Size: 325043 Color: 1
Size: 323674 Color: 0

Bin 510: 6 of cap free
Amount of items: 3
Items: 
Size: 380011 Color: 1
Size: 347546 Color: 1
Size: 272438 Color: 0

Bin 511: 6 of cap free
Amount of items: 3
Items: 
Size: 409174 Color: 1
Size: 320153 Color: 1
Size: 270668 Color: 0

Bin 512: 6 of cap free
Amount of items: 3
Items: 
Size: 439531 Color: 1
Size: 307855 Color: 1
Size: 252609 Color: 0

Bin 513: 6 of cap free
Amount of items: 3
Items: 
Size: 381943 Color: 1
Size: 341203 Color: 1
Size: 276849 Color: 0

Bin 514: 6 of cap free
Amount of items: 3
Items: 
Size: 414824 Color: 1
Size: 334923 Color: 1
Size: 250248 Color: 0

Bin 515: 6 of cap free
Amount of items: 3
Items: 
Size: 422291 Color: 1
Size: 308303 Color: 1
Size: 269401 Color: 0

Bin 516: 6 of cap free
Amount of items: 3
Items: 
Size: 381050 Color: 1
Size: 345715 Color: 1
Size: 273230 Color: 0

Bin 517: 6 of cap free
Amount of items: 3
Items: 
Size: 373519 Color: 1
Size: 366184 Color: 1
Size: 260292 Color: 0

Bin 518: 7 of cap free
Amount of items: 3
Items: 
Size: 372268 Color: 1
Size: 344381 Color: 1
Size: 283345 Color: 0

Bin 519: 7 of cap free
Amount of items: 3
Items: 
Size: 424999 Color: 1
Size: 297520 Color: 1
Size: 277475 Color: 0

Bin 520: 7 of cap free
Amount of items: 3
Items: 
Size: 406594 Color: 1
Size: 321719 Color: 1
Size: 271681 Color: 0

Bin 521: 7 of cap free
Amount of items: 3
Items: 
Size: 388899 Color: 1
Size: 310160 Color: 1
Size: 300935 Color: 0

Bin 522: 7 of cap free
Amount of items: 3
Items: 
Size: 469676 Color: 1
Size: 278626 Color: 1
Size: 251692 Color: 0

Bin 523: 7 of cap free
Amount of items: 3
Items: 
Size: 428055 Color: 1
Size: 311613 Color: 1
Size: 260326 Color: 0

Bin 524: 8 of cap free
Amount of items: 3
Items: 
Size: 437191 Color: 1
Size: 298341 Color: 1
Size: 264461 Color: 0

Bin 525: 8 of cap free
Amount of items: 3
Items: 
Size: 442777 Color: 1
Size: 280706 Color: 1
Size: 276510 Color: 0

Bin 526: 8 of cap free
Amount of items: 3
Items: 
Size: 399111 Color: 1
Size: 336295 Color: 1
Size: 264587 Color: 0

Bin 527: 8 of cap free
Amount of items: 3
Items: 
Size: 376714 Color: 1
Size: 319322 Color: 1
Size: 303957 Color: 0

Bin 528: 8 of cap free
Amount of items: 3
Items: 
Size: 387333 Color: 1
Size: 341181 Color: 1
Size: 271479 Color: 0

Bin 529: 8 of cap free
Amount of items: 3
Items: 
Size: 411954 Color: 1
Size: 324894 Color: 1
Size: 263145 Color: 0

Bin 530: 8 of cap free
Amount of items: 3
Items: 
Size: 414212 Color: 1
Size: 312660 Color: 1
Size: 273121 Color: 0

Bin 531: 8 of cap free
Amount of items: 3
Items: 
Size: 418202 Color: 1
Size: 297716 Color: 1
Size: 284075 Color: 0

Bin 532: 8 of cap free
Amount of items: 3
Items: 
Size: 460794 Color: 1
Size: 270661 Color: 1
Size: 268538 Color: 0

Bin 533: 9 of cap free
Amount of items: 3
Items: 
Size: 365058 Color: 1
Size: 332821 Color: 1
Size: 302113 Color: 0

Bin 534: 9 of cap free
Amount of items: 3
Items: 
Size: 436103 Color: 1
Size: 303945 Color: 1
Size: 259944 Color: 0

Bin 535: 9 of cap free
Amount of items: 3
Items: 
Size: 374901 Color: 1
Size: 329551 Color: 1
Size: 295540 Color: 0

Bin 536: 9 of cap free
Amount of items: 3
Items: 
Size: 380602 Color: 1
Size: 355823 Color: 1
Size: 263567 Color: 0

Bin 537: 9 of cap free
Amount of items: 3
Items: 
Size: 350044 Color: 1
Size: 331387 Color: 0
Size: 318561 Color: 1

Bin 538: 9 of cap free
Amount of items: 3
Items: 
Size: 347473 Color: 1
Size: 336875 Color: 1
Size: 315644 Color: 0

Bin 539: 9 of cap free
Amount of items: 3
Items: 
Size: 370366 Color: 1
Size: 353056 Color: 1
Size: 276570 Color: 0

Bin 540: 10 of cap free
Amount of items: 3
Items: 
Size: 452082 Color: 1
Size: 295553 Color: 1
Size: 252356 Color: 0

Bin 541: 10 of cap free
Amount of items: 3
Items: 
Size: 445435 Color: 1
Size: 302025 Color: 1
Size: 252531 Color: 0

Bin 542: 10 of cap free
Amount of items: 3
Items: 
Size: 422694 Color: 1
Size: 308738 Color: 1
Size: 268559 Color: 0

Bin 543: 10 of cap free
Amount of items: 3
Items: 
Size: 353003 Color: 1
Size: 338668 Color: 1
Size: 308320 Color: 0

Bin 544: 10 of cap free
Amount of items: 3
Items: 
Size: 469888 Color: 1
Size: 272184 Color: 1
Size: 257919 Color: 0

Bin 545: 10 of cap free
Amount of items: 3
Items: 
Size: 402195 Color: 1
Size: 316146 Color: 1
Size: 281650 Color: 0

Bin 546: 11 of cap free
Amount of items: 3
Items: 
Size: 416768 Color: 1
Size: 303527 Color: 1
Size: 279695 Color: 0

Bin 547: 11 of cap free
Amount of items: 3
Items: 
Size: 417733 Color: 1
Size: 300155 Color: 1
Size: 282102 Color: 0

Bin 548: 11 of cap free
Amount of items: 3
Items: 
Size: 469786 Color: 1
Size: 279381 Color: 1
Size: 250823 Color: 0

Bin 549: 11 of cap free
Amount of items: 3
Items: 
Size: 375627 Color: 1
Size: 330223 Color: 1
Size: 294140 Color: 0

Bin 550: 11 of cap free
Amount of items: 3
Items: 
Size: 422732 Color: 1
Size: 298486 Color: 1
Size: 278772 Color: 0

Bin 551: 11 of cap free
Amount of items: 3
Items: 
Size: 389168 Color: 1
Size: 343592 Color: 1
Size: 267230 Color: 0

Bin 552: 12 of cap free
Amount of items: 3
Items: 
Size: 388182 Color: 1
Size: 322190 Color: 1
Size: 289617 Color: 0

Bin 553: 12 of cap free
Amount of items: 3
Items: 
Size: 478145 Color: 1
Size: 270975 Color: 0
Size: 250869 Color: 1

Bin 554: 12 of cap free
Amount of items: 3
Items: 
Size: 411578 Color: 1
Size: 319486 Color: 1
Size: 268925 Color: 0

Bin 555: 12 of cap free
Amount of items: 3
Items: 
Size: 372093 Color: 1
Size: 339227 Color: 1
Size: 288669 Color: 0

Bin 556: 12 of cap free
Amount of items: 3
Items: 
Size: 392899 Color: 1
Size: 304265 Color: 1
Size: 302825 Color: 0

Bin 557: 12 of cap free
Amount of items: 3
Items: 
Size: 362739 Color: 1
Size: 357617 Color: 1
Size: 279633 Color: 0

Bin 558: 12 of cap free
Amount of items: 3
Items: 
Size: 386051 Color: 1
Size: 310368 Color: 1
Size: 303570 Color: 0

Bin 559: 12 of cap free
Amount of items: 3
Items: 
Size: 413840 Color: 1
Size: 319186 Color: 1
Size: 266963 Color: 0

Bin 560: 12 of cap free
Amount of items: 3
Items: 
Size: 374620 Color: 1
Size: 347804 Color: 1
Size: 277565 Color: 0

Bin 561: 13 of cap free
Amount of items: 3
Items: 
Size: 444000 Color: 1
Size: 299727 Color: 1
Size: 256261 Color: 0

Bin 562: 13 of cap free
Amount of items: 3
Items: 
Size: 484437 Color: 1
Size: 261779 Color: 1
Size: 253772 Color: 0

Bin 563: 13 of cap free
Amount of items: 3
Items: 
Size: 381551 Color: 1
Size: 367780 Color: 1
Size: 250657 Color: 0

Bin 564: 13 of cap free
Amount of items: 3
Items: 
Size: 399128 Color: 1
Size: 331238 Color: 1
Size: 269622 Color: 0

Bin 565: 14 of cap free
Amount of items: 3
Items: 
Size: 386545 Color: 1
Size: 312498 Color: 1
Size: 300944 Color: 0

Bin 566: 14 of cap free
Amount of items: 3
Items: 
Size: 387794 Color: 1
Size: 336736 Color: 1
Size: 275457 Color: 0

Bin 567: 15 of cap free
Amount of items: 3
Items: 
Size: 392005 Color: 1
Size: 354913 Color: 1
Size: 253068 Color: 0

Bin 568: 16 of cap free
Amount of items: 3
Items: 
Size: 493985 Color: 1
Size: 254899 Color: 1
Size: 251101 Color: 0

Bin 569: 16 of cap free
Amount of items: 3
Items: 
Size: 408013 Color: 1
Size: 325675 Color: 1
Size: 266297 Color: 0

Bin 570: 16 of cap free
Amount of items: 3
Items: 
Size: 395710 Color: 1
Size: 307975 Color: 0
Size: 296300 Color: 1

Bin 571: 16 of cap free
Amount of items: 3
Items: 
Size: 428701 Color: 1
Size: 318325 Color: 1
Size: 252959 Color: 0

Bin 572: 17 of cap free
Amount of items: 3
Items: 
Size: 415268 Color: 1
Size: 322015 Color: 1
Size: 262701 Color: 0

Bin 573: 17 of cap free
Amount of items: 3
Items: 
Size: 434002 Color: 1
Size: 314952 Color: 1
Size: 251030 Color: 0

Bin 574: 17 of cap free
Amount of items: 3
Items: 
Size: 397817 Color: 1
Size: 330930 Color: 1
Size: 271237 Color: 0

Bin 575: 17 of cap free
Amount of items: 3
Items: 
Size: 385855 Color: 1
Size: 332427 Color: 1
Size: 281702 Color: 0

Bin 576: 17 of cap free
Amount of items: 3
Items: 
Size: 402568 Color: 1
Size: 331678 Color: 1
Size: 265738 Color: 0

Bin 577: 17 of cap free
Amount of items: 3
Items: 
Size: 391321 Color: 1
Size: 330105 Color: 1
Size: 278558 Color: 0

Bin 578: 18 of cap free
Amount of items: 3
Items: 
Size: 360262 Color: 1
Size: 325338 Color: 1
Size: 314383 Color: 0

Bin 579: 18 of cap free
Amount of items: 3
Items: 
Size: 443185 Color: 1
Size: 305389 Color: 1
Size: 251409 Color: 0

Bin 580: 18 of cap free
Amount of items: 3
Items: 
Size: 374492 Color: 1
Size: 314999 Color: 1
Size: 310492 Color: 0

Bin 581: 19 of cap free
Amount of items: 3
Items: 
Size: 355034 Color: 1
Size: 346041 Color: 1
Size: 298907 Color: 0

Bin 582: 19 of cap free
Amount of items: 3
Items: 
Size: 437165 Color: 1
Size: 312574 Color: 1
Size: 250243 Color: 0

Bin 583: 21 of cap free
Amount of items: 3
Items: 
Size: 371180 Color: 1
Size: 361505 Color: 1
Size: 267295 Color: 0

Bin 584: 21 of cap free
Amount of items: 3
Items: 
Size: 355321 Color: 1
Size: 344358 Color: 1
Size: 300301 Color: 0

Bin 585: 23 of cap free
Amount of items: 3
Items: 
Size: 357082 Color: 1
Size: 350777 Color: 1
Size: 292119 Color: 0

Bin 586: 25 of cap free
Amount of items: 3
Items: 
Size: 458562 Color: 1
Size: 276724 Color: 1
Size: 264690 Color: 0

Bin 587: 26 of cap free
Amount of items: 3
Items: 
Size: 413521 Color: 1
Size: 313278 Color: 1
Size: 273176 Color: 0

Bin 588: 27 of cap free
Amount of items: 3
Items: 
Size: 420854 Color: 1
Size: 319058 Color: 1
Size: 260062 Color: 0

Bin 589: 27 of cap free
Amount of items: 3
Items: 
Size: 372459 Color: 1
Size: 328027 Color: 1
Size: 299488 Color: 0

Bin 590: 29 of cap free
Amount of items: 3
Items: 
Size: 402030 Color: 1
Size: 315022 Color: 1
Size: 282920 Color: 0

Bin 591: 30 of cap free
Amount of items: 3
Items: 
Size: 422890 Color: 1
Size: 319391 Color: 1
Size: 257690 Color: 0

Bin 592: 30 of cap free
Amount of items: 3
Items: 
Size: 362479 Color: 1
Size: 333804 Color: 1
Size: 303688 Color: 0

Bin 593: 31 of cap free
Amount of items: 3
Items: 
Size: 383976 Color: 1
Size: 363091 Color: 1
Size: 252903 Color: 0

Bin 594: 31 of cap free
Amount of items: 3
Items: 
Size: 371999 Color: 1
Size: 317914 Color: 0
Size: 310057 Color: 1

Bin 595: 32 of cap free
Amount of items: 3
Items: 
Size: 372124 Color: 1
Size: 353271 Color: 1
Size: 274574 Color: 0

Bin 596: 32 of cap free
Amount of items: 3
Items: 
Size: 353509 Color: 1
Size: 339430 Color: 1
Size: 307030 Color: 0

Bin 597: 33 of cap free
Amount of items: 3
Items: 
Size: 374185 Color: 1
Size: 334544 Color: 1
Size: 291239 Color: 0

Bin 598: 34 of cap free
Amount of items: 3
Items: 
Size: 389054 Color: 1
Size: 360414 Color: 1
Size: 250499 Color: 0

Bin 599: 34 of cap free
Amount of items: 3
Items: 
Size: 415470 Color: 1
Size: 293823 Color: 0
Size: 290674 Color: 1

Bin 600: 35 of cap free
Amount of items: 3
Items: 
Size: 371813 Color: 1
Size: 355672 Color: 1
Size: 272481 Color: 0

Bin 601: 35 of cap free
Amount of items: 3
Items: 
Size: 430643 Color: 1
Size: 287713 Color: 1
Size: 281610 Color: 0

Bin 602: 39 of cap free
Amount of items: 3
Items: 
Size: 365301 Color: 1
Size: 350849 Color: 1
Size: 283812 Color: 0

Bin 603: 39 of cap free
Amount of items: 3
Items: 
Size: 370035 Color: 1
Size: 340674 Color: 1
Size: 289253 Color: 0

Bin 604: 39 of cap free
Amount of items: 3
Items: 
Size: 340394 Color: 1
Size: 334011 Color: 1
Size: 325557 Color: 0

Bin 605: 40 of cap free
Amount of items: 3
Items: 
Size: 475594 Color: 1
Size: 268528 Color: 1
Size: 255839 Color: 0

Bin 606: 40 of cap free
Amount of items: 3
Items: 
Size: 397976 Color: 1
Size: 324232 Color: 1
Size: 277753 Color: 0

Bin 607: 41 of cap free
Amount of items: 3
Items: 
Size: 394620 Color: 1
Size: 346838 Color: 1
Size: 258502 Color: 0

Bin 608: 41 of cap free
Amount of items: 3
Items: 
Size: 393631 Color: 1
Size: 340123 Color: 1
Size: 266206 Color: 0

Bin 609: 42 of cap free
Amount of items: 3
Items: 
Size: 459901 Color: 1
Size: 285965 Color: 1
Size: 254093 Color: 0

Bin 610: 42 of cap free
Amount of items: 3
Items: 
Size: 388539 Color: 1
Size: 337457 Color: 1
Size: 273963 Color: 0

Bin 611: 45 of cap free
Amount of items: 3
Items: 
Size: 399761 Color: 1
Size: 318376 Color: 1
Size: 281819 Color: 0

Bin 612: 45 of cap free
Amount of items: 3
Items: 
Size: 456243 Color: 1
Size: 289854 Color: 1
Size: 253859 Color: 0

Bin 613: 46 of cap free
Amount of items: 3
Items: 
Size: 403725 Color: 1
Size: 330347 Color: 1
Size: 265883 Color: 0

Bin 614: 46 of cap free
Amount of items: 3
Items: 
Size: 392756 Color: 1
Size: 308624 Color: 1
Size: 298575 Color: 0

Bin 615: 48 of cap free
Amount of items: 3
Items: 
Size: 413732 Color: 1
Size: 319044 Color: 1
Size: 267177 Color: 0

Bin 616: 52 of cap free
Amount of items: 3
Items: 
Size: 398770 Color: 1
Size: 338488 Color: 1
Size: 262691 Color: 0

Bin 617: 52 of cap free
Amount of items: 3
Items: 
Size: 384365 Color: 1
Size: 316827 Color: 1
Size: 298757 Color: 0

Bin 618: 52 of cap free
Amount of items: 3
Items: 
Size: 411481 Color: 1
Size: 303757 Color: 1
Size: 284711 Color: 0

Bin 619: 52 of cap free
Amount of items: 3
Items: 
Size: 382224 Color: 1
Size: 335192 Color: 1
Size: 282533 Color: 0

Bin 620: 52 of cap free
Amount of items: 3
Items: 
Size: 418377 Color: 1
Size: 308927 Color: 1
Size: 272645 Color: 0

Bin 621: 53 of cap free
Amount of items: 3
Items: 
Size: 374068 Color: 1
Size: 331558 Color: 1
Size: 294322 Color: 0

Bin 622: 54 of cap free
Amount of items: 3
Items: 
Size: 356176 Color: 1
Size: 324112 Color: 1
Size: 319659 Color: 0

Bin 623: 59 of cap free
Amount of items: 3
Items: 
Size: 382058 Color: 1
Size: 315412 Color: 0
Size: 302472 Color: 1

Bin 624: 59 of cap free
Amount of items: 3
Items: 
Size: 372557 Color: 1
Size: 323357 Color: 0
Size: 304028 Color: 1

Bin 625: 60 of cap free
Amount of items: 3
Items: 
Size: 400749 Color: 1
Size: 332293 Color: 1
Size: 266899 Color: 0

Bin 626: 60 of cap free
Amount of items: 3
Items: 
Size: 451963 Color: 1
Size: 289063 Color: 1
Size: 258915 Color: 0

Bin 627: 61 of cap free
Amount of items: 3
Items: 
Size: 350028 Color: 1
Size: 347585 Color: 1
Size: 302327 Color: 0

Bin 628: 61 of cap free
Amount of items: 3
Items: 
Size: 379304 Color: 1
Size: 352936 Color: 1
Size: 267700 Color: 0

Bin 629: 63 of cap free
Amount of items: 3
Items: 
Size: 418644 Color: 1
Size: 323626 Color: 1
Size: 257668 Color: 0

Bin 630: 64 of cap free
Amount of items: 3
Items: 
Size: 412442 Color: 1
Size: 334976 Color: 1
Size: 252519 Color: 0

Bin 631: 73 of cap free
Amount of items: 3
Items: 
Size: 457674 Color: 1
Size: 281658 Color: 1
Size: 260596 Color: 0

Bin 632: 76 of cap free
Amount of items: 3
Items: 
Size: 424543 Color: 1
Size: 322778 Color: 1
Size: 252604 Color: 0

Bin 633: 88 of cap free
Amount of items: 3
Items: 
Size: 408421 Color: 1
Size: 304901 Color: 1
Size: 286591 Color: 0

Bin 634: 91 of cap free
Amount of items: 3
Items: 
Size: 378522 Color: 1
Size: 346827 Color: 1
Size: 274561 Color: 0

Bin 635: 92 of cap free
Amount of items: 3
Items: 
Size: 398544 Color: 1
Size: 337734 Color: 1
Size: 263631 Color: 0

Bin 636: 98 of cap free
Amount of items: 3
Items: 
Size: 428467 Color: 1
Size: 309248 Color: 1
Size: 262188 Color: 0

Bin 637: 100 of cap free
Amount of items: 3
Items: 
Size: 379504 Color: 1
Size: 320959 Color: 0
Size: 299438 Color: 1

Bin 638: 107 of cap free
Amount of items: 3
Items: 
Size: 395356 Color: 1
Size: 316647 Color: 1
Size: 287891 Color: 0

Bin 639: 109 of cap free
Amount of items: 3
Items: 
Size: 390452 Color: 1
Size: 324862 Color: 1
Size: 284578 Color: 0

Bin 640: 124 of cap free
Amount of items: 3
Items: 
Size: 416010 Color: 1
Size: 330037 Color: 1
Size: 253830 Color: 0

Bin 641: 132 of cap free
Amount of items: 3
Items: 
Size: 388356 Color: 1
Size: 331979 Color: 1
Size: 279534 Color: 0

Bin 642: 137 of cap free
Amount of items: 3
Items: 
Size: 429136 Color: 1
Size: 292612 Color: 1
Size: 278116 Color: 0

Bin 643: 139 of cap free
Amount of items: 3
Items: 
Size: 380448 Color: 1
Size: 313673 Color: 0
Size: 305741 Color: 1

Bin 644: 169 of cap free
Amount of items: 3
Items: 
Size: 393960 Color: 1
Size: 322165 Color: 0
Size: 283707 Color: 1

Bin 645: 197 of cap free
Amount of items: 3
Items: 
Size: 410352 Color: 1
Size: 296150 Color: 0
Size: 293302 Color: 1

Bin 646: 218 of cap free
Amount of items: 3
Items: 
Size: 424105 Color: 1
Size: 319361 Color: 1
Size: 256317 Color: 0

Bin 647: 236 of cap free
Amount of items: 3
Items: 
Size: 410113 Color: 1
Size: 307396 Color: 1
Size: 282256 Color: 0

Bin 648: 236 of cap free
Amount of items: 3
Items: 
Size: 407330 Color: 1
Size: 322240 Color: 1
Size: 270195 Color: 0

Bin 649: 237 of cap free
Amount of items: 3
Items: 
Size: 421577 Color: 1
Size: 303229 Color: 1
Size: 274958 Color: 0

Bin 650: 239 of cap free
Amount of items: 3
Items: 
Size: 446238 Color: 1
Size: 291837 Color: 1
Size: 261687 Color: 0

Bin 651: 279 of cap free
Amount of items: 3
Items: 
Size: 422159 Color: 1
Size: 305932 Color: 1
Size: 271631 Color: 0

Bin 652: 303 of cap free
Amount of items: 3
Items: 
Size: 425221 Color: 1
Size: 307966 Color: 1
Size: 266511 Color: 0

Bin 653: 338 of cap free
Amount of items: 3
Items: 
Size: 388001 Color: 1
Size: 341541 Color: 1
Size: 270121 Color: 0

Bin 654: 348 of cap free
Amount of items: 3
Items: 
Size: 372888 Color: 1
Size: 332647 Color: 1
Size: 294118 Color: 0

Bin 655: 414 of cap free
Amount of items: 3
Items: 
Size: 369892 Color: 1
Size: 357178 Color: 1
Size: 272517 Color: 0

Bin 656: 472 of cap free
Amount of items: 3
Items: 
Size: 479305 Color: 1
Size: 267875 Color: 1
Size: 252349 Color: 0

Bin 657: 627 of cap free
Amount of items: 3
Items: 
Size: 426338 Color: 1
Size: 291586 Color: 0
Size: 281450 Color: 1

Bin 658: 738 of cap free
Amount of items: 3
Items: 
Size: 388727 Color: 1
Size: 323115 Color: 1
Size: 287421 Color: 0

Bin 659: 1047 of cap free
Amount of items: 3
Items: 
Size: 433983 Color: 1
Size: 312953 Color: 1
Size: 252018 Color: 0

Bin 660: 1065 of cap free
Amount of items: 3
Items: 
Size: 377659 Color: 1
Size: 335160 Color: 1
Size: 286117 Color: 0

Bin 661: 1623 of cap free
Amount of items: 3
Items: 
Size: 435580 Color: 1
Size: 305325 Color: 1
Size: 257473 Color: 0

Bin 662: 1674 of cap free
Amount of items: 3
Items: 
Size: 407690 Color: 1
Size: 312639 Color: 1
Size: 277998 Color: 0

Bin 663: 7018 of cap free
Amount of items: 3
Items: 
Size: 426058 Color: 1
Size: 312284 Color: 1
Size: 254641 Color: 0

Bin 664: 7203 of cap free
Amount of items: 3
Items: 
Size: 356412 Color: 1
Size: 339483 Color: 1
Size: 296903 Color: 0

Bin 665: 50370 of cap free
Amount of items: 3
Items: 
Size: 353952 Color: 1
Size: 321128 Color: 1
Size: 274551 Color: 0

Bin 666: 168194 of cap free
Amount of items: 3
Items: 
Size: 294300 Color: 1
Size: 271497 Color: 1
Size: 266010 Color: 0

Bin 667: 249855 of cap free
Amount of items: 2
Items: 
Size: 499046 Color: 1
Size: 251100 Color: 0

Bin 668: 502460 of cap free
Amount of items: 1
Items: 
Size: 497541 Color: 1

Total size: 667000667
Total free space: 1000001

