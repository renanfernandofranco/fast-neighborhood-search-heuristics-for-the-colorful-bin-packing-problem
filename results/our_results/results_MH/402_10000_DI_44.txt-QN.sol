Capicity Bin: 8400
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5962 Color: 309
Size: 2290 Color: 238
Size: 148 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 318
Size: 1201 Color: 188
Size: 957 Color: 166

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 328
Size: 1226 Color: 192
Size: 698 Color: 136

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 334
Size: 1537 Color: 210
Size: 218 Color: 54

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 337
Size: 1532 Color: 208
Size: 152 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 338
Size: 1402 Color: 202
Size: 280 Color: 72

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6730 Color: 339
Size: 974 Color: 170
Size: 696 Color: 132

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 340
Size: 1200 Color: 187
Size: 456 Color: 107

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6800 Color: 343
Size: 834 Color: 151
Size: 766 Color: 142

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 348
Size: 1486 Color: 207
Size: 16 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6945 Color: 351
Size: 1213 Color: 190
Size: 242 Color: 62

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 353
Size: 1068 Color: 178
Size: 376 Color: 96

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 354
Size: 1187 Color: 185
Size: 252 Color: 64

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7111 Color: 361
Size: 1107 Color: 180
Size: 182 Color: 36

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7124 Color: 362
Size: 836 Color: 152
Size: 440 Color: 106

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7218 Color: 368
Size: 794 Color: 146
Size: 388 Color: 98

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 370
Size: 604 Color: 127
Size: 552 Color: 122

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 371
Size: 849 Color: 154
Size: 304 Color: 80

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7249 Color: 372
Size: 863 Color: 156
Size: 288 Color: 75

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 373
Size: 1124 Color: 182
Size: 24 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7253 Color: 374
Size: 823 Color: 150
Size: 324 Color: 84

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 377
Size: 876 Color: 158
Size: 224 Color: 56

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7349 Color: 381
Size: 837 Color: 153
Size: 214 Color: 53

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7354 Color: 382
Size: 874 Color: 157
Size: 172 Color: 31

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7370 Color: 385
Size: 862 Color: 155
Size: 168 Color: 27

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7402 Color: 388
Size: 754 Color: 139
Size: 244 Color: 63

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7404 Color: 389
Size: 696 Color: 134
Size: 300 Color: 78

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7444 Color: 391
Size: 812 Color: 149
Size: 144 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7447 Color: 392
Size: 795 Color: 147
Size: 158 Color: 22

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7463 Color: 394
Size: 781 Color: 144
Size: 156 Color: 21

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7479 Color: 396
Size: 769 Color: 143
Size: 152 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7492 Color: 398
Size: 688 Color: 131
Size: 220 Color: 55

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7495 Color: 399
Size: 755 Color: 140
Size: 150 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7530 Color: 401
Size: 604 Color: 128
Size: 266 Color: 68

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7556 Color: 402
Size: 648 Color: 129
Size: 196 Color: 45

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 316
Size: 1762 Color: 219
Size: 416 Color: 103

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6281 Color: 321
Size: 2022 Color: 228
Size: 96 Color: 7

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 326
Size: 1804 Color: 222
Size: 152 Color: 16

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 342
Size: 1635 Color: 216

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 344
Size: 1394 Color: 201
Size: 202 Color: 49

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 7073 Color: 358
Size: 926 Color: 163
Size: 400 Color: 99

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 7185 Color: 365
Size: 1214 Color: 191

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 7303 Color: 378
Size: 584 Color: 124
Size: 512 Color: 117

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 7338 Color: 380
Size: 1061 Color: 177

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 7397 Color: 387
Size: 1002 Color: 172

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 7413 Color: 390
Size: 986 Color: 171

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 7498 Color: 400
Size: 901 Color: 160

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 347
Size: 1364 Color: 199
Size: 152 Color: 19

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 7290 Color: 376
Size: 1108 Color: 181

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 7356 Color: 383
Size: 1042 Color: 176

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 5953 Color: 307
Size: 2284 Color: 237
Size: 160 Color: 23

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 324
Size: 2043 Color: 232
Size: 32 Color: 4

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 325
Size: 1802 Color: 221
Size: 156 Color: 20

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 352
Size: 1451 Color: 204

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 6977 Color: 355
Size: 1404 Color: 203
Size: 16 Color: 1

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 7365 Color: 384
Size: 1032 Color: 175

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 7482 Color: 397
Size: 915 Color: 161

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 306
Size: 2332 Color: 242
Size: 164 Color: 24

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 317
Size: 2034 Color: 229
Size: 132 Color: 9

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 6530 Color: 329
Size: 1604 Color: 213
Size: 262 Color: 67

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 6629 Color: 333
Size: 1767 Color: 220

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 7095 Color: 360
Size: 1301 Color: 195

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 7383 Color: 386
Size: 1013 Color: 174

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 7466 Color: 395
Size: 930 Color: 164

Bin 65: 5 of cap free
Amount of items: 7
Items: 
Size: 4201 Color: 273
Size: 961 Color: 168
Size: 961 Color: 167
Size: 924 Color: 162
Size: 764 Color: 141
Size: 294 Color: 77
Size: 290 Color: 76

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 323
Size: 2041 Color: 231
Size: 48 Color: 5

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6455 Color: 327
Size: 1940 Color: 226

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 6649 Color: 335
Size: 1746 Color: 218

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 6661 Color: 336
Size: 1734 Color: 217

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 7269 Color: 375
Size: 1126 Color: 183

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 6076 Color: 312
Size: 2318 Color: 241

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 356
Size: 1344 Color: 198

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 7063 Color: 357
Size: 1331 Color: 197

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 7150 Color: 364
Size: 1244 Color: 193

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 7319 Color: 379
Size: 1075 Color: 179

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 5957 Color: 308
Size: 2436 Color: 243

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 341
Size: 1631 Color: 215
Size: 16 Color: 0

Bin 78: 7 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 349
Size: 1477 Color: 206

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 7127 Color: 363
Size: 1266 Color: 194

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 7189 Color: 366
Size: 1204 Color: 189

Bin 81: 7 of cap free
Amount of items: 2
Items: 
Size: 7234 Color: 369
Size: 1159 Color: 184

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 7450 Color: 393
Size: 943 Color: 165

Bin 83: 9 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 350
Size: 1461 Color: 205

Bin 84: 9 of cap free
Amount of items: 2
Items: 
Size: 7076 Color: 359
Size: 1315 Color: 196

Bin 85: 9 of cap free
Amount of items: 2
Items: 
Size: 7202 Color: 367
Size: 1189 Color: 186

Bin 86: 10 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 281
Size: 3916 Color: 271
Size: 212 Color: 52

Bin 87: 10 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 295
Size: 2932 Color: 253
Size: 184 Color: 38

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 6546 Color: 330
Size: 1844 Color: 225

Bin 89: 11 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 314
Size: 1621 Color: 214
Size: 572 Color: 123

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 302
Size: 2576 Color: 245
Size: 174 Color: 32

Bin 91: 12 of cap free
Amount of items: 4
Items: 
Size: 5751 Color: 305
Size: 2307 Color: 240
Size: 166 Color: 26
Size: 164 Color: 25

Bin 92: 14 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 299
Size: 2606 Color: 246
Size: 176 Color: 33

Bin 93: 15 of cap free
Amount of items: 2
Items: 
Size: 6564 Color: 332
Size: 1821 Color: 224

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 345
Size: 1562 Color: 212

Bin 95: 15 of cap free
Amount of items: 2
Items: 
Size: 6839 Color: 346
Size: 1546 Color: 211

Bin 96: 19 of cap free
Amount of items: 3
Items: 
Size: 6217 Color: 315
Size: 1382 Color: 200
Size: 782 Color: 145

Bin 97: 23 of cap free
Amount of items: 3
Items: 
Size: 4884 Color: 289
Size: 3301 Color: 261
Size: 192 Color: 42

Bin 98: 25 of cap free
Amount of items: 3
Items: 
Size: 6290 Color: 322
Size: 2037 Color: 230
Size: 48 Color: 6

Bin 99: 27 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 283
Size: 3497 Color: 266
Size: 208 Color: 50

Bin 100: 28 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 331
Size: 1810 Color: 223

Bin 101: 30 of cap free
Amount of items: 3
Items: 
Size: 4468 Color: 282
Size: 3694 Color: 270
Size: 208 Color: 51

Bin 102: 30 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 319
Size: 2126 Color: 234

Bin 103: 31 of cap free
Amount of items: 3
Items: 
Size: 4959 Color: 290
Size: 3220 Color: 259
Size: 190 Color: 41

Bin 104: 35 of cap free
Amount of items: 3
Items: 
Size: 5312 Color: 296
Size: 2869 Color: 252
Size: 184 Color: 37

Bin 105: 38 of cap free
Amount of items: 4
Items: 
Size: 5724 Color: 304
Size: 2302 Color: 239
Size: 168 Color: 29
Size: 168 Color: 28

Bin 106: 40 of cap free
Amount of items: 3
Items: 
Size: 5978 Color: 311
Size: 2236 Color: 236
Size: 146 Color: 12

Bin 107: 41 of cap free
Amount of items: 3
Items: 
Size: 5391 Color: 297
Size: 2788 Color: 250
Size: 180 Color: 35

Bin 108: 42 of cap free
Amount of items: 2
Items: 
Size: 5230 Color: 293
Size: 3128 Color: 258

Bin 109: 51 of cap free
Amount of items: 3
Items: 
Size: 6258 Color: 320
Size: 1979 Color: 227
Size: 112 Color: 8

Bin 110: 58 of cap free
Amount of items: 4
Items: 
Size: 4220 Color: 280
Size: 3648 Color: 269
Size: 238 Color: 58
Size: 236 Color: 57

Bin 111: 60 of cap free
Amount of items: 2
Items: 
Size: 4204 Color: 275
Size: 4136 Color: 272

Bin 112: 65 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 303
Size: 2509 Color: 244
Size: 172 Color: 30

Bin 113: 72 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 313
Size: 2084 Color: 233
Size: 144 Color: 11

Bin 114: 74 of cap free
Amount of items: 3
Items: 
Size: 5969 Color: 310
Size: 2209 Color: 235
Size: 148 Color: 13

Bin 115: 99 of cap free
Amount of items: 2
Items: 
Size: 5633 Color: 301
Size: 2668 Color: 249

Bin 116: 107 of cap free
Amount of items: 3
Items: 
Size: 5249 Color: 294
Size: 2856 Color: 251
Size: 188 Color: 39

Bin 117: 113 of cap free
Amount of items: 3
Items: 
Size: 5484 Color: 298
Size: 2627 Color: 247
Size: 176 Color: 34

Bin 118: 123 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 291
Size: 3027 Color: 256
Size: 190 Color: 40

Bin 119: 125 of cap free
Amount of items: 3
Items: 
Size: 4799 Color: 288
Size: 3284 Color: 260
Size: 192 Color: 43

Bin 120: 129 of cap free
Amount of items: 6
Items: 
Size: 4202 Color: 274
Size: 1534 Color: 209
Size: 1011 Color: 173
Size: 964 Color: 169
Size: 280 Color: 74
Size: 280 Color: 73

Bin 121: 129 of cap free
Amount of items: 2
Items: 
Size: 4769 Color: 285
Size: 3502 Color: 268

Bin 122: 136 of cap free
Amount of items: 2
Items: 
Size: 5622 Color: 300
Size: 2642 Color: 248

Bin 123: 137 of cap free
Amount of items: 2
Items: 
Size: 4762 Color: 284
Size: 3501 Color: 267

Bin 124: 162 of cap free
Amount of items: 2
Items: 
Size: 5204 Color: 292
Size: 3034 Color: 257

Bin 125: 178 of cap free
Amount of items: 4
Items: 
Size: 4206 Color: 277
Size: 3484 Color: 263
Size: 272 Color: 69
Size: 260 Color: 66

Bin 126: 197 of cap free
Amount of items: 4
Items: 
Size: 4205 Color: 276
Size: 3450 Color: 262
Size: 276 Color: 71
Size: 272 Color: 70

Bin 127: 199 of cap free
Amount of items: 4
Items: 
Size: 4783 Color: 287
Size: 3022 Color: 255
Size: 200 Color: 46
Size: 196 Color: 44

Bin 128: 204 of cap free
Amount of items: 19
Items: 
Size: 528 Color: 120
Size: 524 Color: 119
Size: 520 Color: 118
Size: 512 Color: 116
Size: 500 Color: 115
Size: 500 Color: 114
Size: 496 Color: 113
Size: 480 Color: 112
Size: 464 Color: 111
Size: 460 Color: 110
Size: 460 Color: 109
Size: 356 Color: 92
Size: 352 Color: 91
Size: 352 Color: 90
Size: 348 Color: 89
Size: 348 Color: 88
Size: 344 Color: 87
Size: 326 Color: 86
Size: 326 Color: 85

Bin 129: 204 of cap free
Amount of items: 4
Items: 
Size: 4209 Color: 278
Size: 3491 Color: 264
Size: 256 Color: 65
Size: 240 Color: 61

Bin 130: 207 of cap free
Amount of items: 4
Items: 
Size: 4778 Color: 286
Size: 3015 Color: 254
Size: 200 Color: 48
Size: 200 Color: 47

Bin 131: 214 of cap free
Amount of items: 4
Items: 
Size: 4213 Color: 279
Size: 3493 Color: 265
Size: 240 Color: 60
Size: 240 Color: 59

Bin 132: 243 of cap free
Amount of items: 14
Items: 
Size: 877 Color: 159
Size: 804 Color: 148
Size: 726 Color: 138
Size: 698 Color: 137
Size: 698 Color: 135
Size: 696 Color: 133
Size: 688 Color: 130
Size: 602 Color: 126
Size: 600 Color: 125
Size: 528 Color: 121
Size: 320 Color: 83
Size: 308 Color: 82
Size: 308 Color: 81
Size: 304 Color: 79

Bin 133: 4378 of cap free
Amount of items: 10
Items: 
Size: 460 Color: 108
Size: 440 Color: 105
Size: 440 Color: 104
Size: 406 Color: 102
Size: 406 Color: 101
Size: 404 Color: 100
Size: 384 Color: 97
Size: 362 Color: 95
Size: 360 Color: 94
Size: 360 Color: 93

Total size: 1108800
Total free space: 8400

