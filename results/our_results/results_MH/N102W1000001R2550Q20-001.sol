Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 389663 Color: 14
Size: 357886 Color: 9
Size: 252452 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 401334 Color: 7
Size: 333374 Color: 9
Size: 265293 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 375799 Color: 17
Size: 260372 Color: 3
Size: 363830 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 421084 Color: 13
Size: 271214 Color: 3
Size: 307703 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 340691 Color: 11
Size: 310520 Color: 0
Size: 348790 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 383420 Color: 4
Size: 335445 Color: 6
Size: 281136 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 426740 Color: 14
Size: 275417 Color: 10
Size: 297844 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 488349 Color: 14
Size: 257934 Color: 12
Size: 253718 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 381316 Color: 12
Size: 355953 Color: 12
Size: 262732 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 392051 Color: 2
Size: 352827 Color: 19
Size: 255123 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 386546 Color: 16
Size: 341135 Color: 16
Size: 272320 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 378121 Color: 19
Size: 334225 Color: 16
Size: 287655 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 374312 Color: 15
Size: 319865 Color: 12
Size: 305824 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 399381 Color: 12
Size: 332660 Color: 8
Size: 267960 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 367745 Color: 12
Size: 335177 Color: 17
Size: 297079 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 376613 Color: 0
Size: 346774 Color: 2
Size: 276614 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 373767 Color: 8
Size: 315686 Color: 5
Size: 310548 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 487617 Color: 19
Size: 259717 Color: 5
Size: 252667 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 380859 Color: 4
Size: 301733 Color: 7
Size: 317409 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 398336 Color: 9
Size: 276630 Color: 14
Size: 325035 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 486094 Color: 3
Size: 261541 Color: 7
Size: 252366 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 397240 Color: 12
Size: 318560 Color: 18
Size: 284201 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 403757 Color: 17
Size: 338024 Color: 2
Size: 258220 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 414280 Color: 17
Size: 313747 Color: 12
Size: 271974 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 420746 Color: 16
Size: 320783 Color: 7
Size: 258472 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 439789 Color: 9
Size: 292017 Color: 8
Size: 268195 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 441027 Color: 7
Size: 280218 Color: 2
Size: 278756 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 448120 Color: 1
Size: 285520 Color: 5
Size: 266361 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 477439 Color: 12
Size: 263622 Color: 3
Size: 258940 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 473612 Color: 16
Size: 271722 Color: 2
Size: 254667 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 474178 Color: 13
Size: 264046 Color: 15
Size: 261777 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 480597 Color: 12
Size: 266831 Color: 10
Size: 252573 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 483145 Color: 13
Size: 266038 Color: 0
Size: 250818 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 484746 Color: 16
Size: 261219 Color: 6
Size: 254036 Color: 16

Total size: 34000034
Total free space: 0

