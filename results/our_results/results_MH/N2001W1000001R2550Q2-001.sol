Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 399819 Color: 1
Size: 320308 Color: 0
Size: 279874 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 476986 Color: 0
Size: 267674 Color: 1
Size: 255341 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 384791 Color: 0
Size: 322725 Color: 1
Size: 292485 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 472972 Color: 0
Size: 267787 Color: 1
Size: 259242 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 412682 Color: 0
Size: 320621 Color: 0
Size: 266698 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 479458 Color: 0
Size: 260506 Color: 1
Size: 260037 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 359089 Color: 1
Size: 358260 Color: 0
Size: 282652 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 415658 Color: 1
Size: 325535 Color: 0
Size: 258808 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380530 Color: 1
Size: 330205 Color: 1
Size: 289266 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 429488 Color: 1
Size: 309771 Color: 0
Size: 260742 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 398288 Color: 1
Size: 346350 Color: 1
Size: 255363 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 389441 Color: 1
Size: 333279 Color: 0
Size: 277281 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 410719 Color: 1
Size: 313605 Color: 1
Size: 275677 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 428258 Color: 0
Size: 316936 Color: 1
Size: 254807 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 408588 Color: 0
Size: 336780 Color: 1
Size: 254633 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 490402 Color: 1
Size: 255169 Color: 1
Size: 254430 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 363687 Color: 1
Size: 349770 Color: 1
Size: 286544 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 405882 Color: 0
Size: 326396 Color: 1
Size: 267723 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 422171 Color: 1
Size: 325557 Color: 0
Size: 252273 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 392136 Color: 1
Size: 350772 Color: 0
Size: 257093 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 373814 Color: 0
Size: 366669 Color: 1
Size: 259518 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 380643 Color: 0
Size: 364533 Color: 0
Size: 254825 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 408247 Color: 0
Size: 296343 Color: 0
Size: 295411 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 353540 Color: 1
Size: 329412 Color: 0
Size: 317049 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 368039 Color: 1
Size: 366015 Color: 0
Size: 265947 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 487225 Color: 0
Size: 259689 Color: 1
Size: 253087 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 482899 Color: 1
Size: 261994 Color: 1
Size: 255108 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 402718 Color: 0
Size: 343592 Color: 0
Size: 253691 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 395993 Color: 0
Size: 328256 Color: 0
Size: 275752 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 387231 Color: 1
Size: 330300 Color: 0
Size: 282470 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 379776 Color: 1
Size: 330118 Color: 0
Size: 290107 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 438461 Color: 1
Size: 292205 Color: 1
Size: 269335 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 456327 Color: 1
Size: 290222 Color: 0
Size: 253452 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 467381 Color: 1
Size: 277847 Color: 0
Size: 254773 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 490349 Color: 0
Size: 259599 Color: 0
Size: 250053 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 452386 Color: 0
Size: 275575 Color: 1
Size: 272040 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 490285 Color: 0
Size: 255963 Color: 1
Size: 253753 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 376911 Color: 1
Size: 325100 Color: 0
Size: 297990 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 463982 Color: 0
Size: 281407 Color: 1
Size: 254612 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 459150 Color: 1
Size: 275062 Color: 0
Size: 265789 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 388533 Color: 1
Size: 307694 Color: 0
Size: 303774 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 489978 Color: 1
Size: 259227 Color: 0
Size: 250796 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 408852 Color: 0
Size: 323664 Color: 1
Size: 267485 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 383082 Color: 1
Size: 328476 Color: 0
Size: 288443 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 386039 Color: 1
Size: 324846 Color: 1
Size: 289116 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 450963 Color: 1
Size: 294535 Color: 0
Size: 254503 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 448481 Color: 1
Size: 281447 Color: 0
Size: 270073 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 351200 Color: 0
Size: 338786 Color: 1
Size: 310015 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 457070 Color: 1
Size: 276854 Color: 0
Size: 266077 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 414405 Color: 0
Size: 332567 Color: 1
Size: 253029 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 469435 Color: 0
Size: 274806 Color: 0
Size: 255760 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 391652 Color: 0
Size: 350941 Color: 1
Size: 257408 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 447662 Color: 0
Size: 281482 Color: 1
Size: 270857 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 484461 Color: 1
Size: 261558 Color: 1
Size: 253982 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 353088 Color: 1
Size: 352474 Color: 0
Size: 294439 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 367170 Color: 0
Size: 357071 Color: 1
Size: 275760 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 428744 Color: 0
Size: 293663 Color: 1
Size: 277594 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 372951 Color: 0
Size: 332488 Color: 1
Size: 294562 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 474758 Color: 1
Size: 267830 Color: 0
Size: 257413 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 363836 Color: 0
Size: 347722 Color: 1
Size: 288443 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 358483 Color: 1
Size: 339746 Color: 1
Size: 301772 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 446955 Color: 1
Size: 289089 Color: 0
Size: 263957 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 352849 Color: 0
Size: 348793 Color: 1
Size: 298359 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 397202 Color: 1
Size: 344574 Color: 0
Size: 258225 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 356767 Color: 1
Size: 331812 Color: 1
Size: 311422 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 341271 Color: 0
Size: 340316 Color: 1
Size: 318414 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 353055 Color: 0
Size: 350730 Color: 1
Size: 296216 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 496478 Color: 1
Size: 252710 Color: 1
Size: 250813 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 363634 Color: 1
Size: 332919 Color: 1
Size: 303448 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 358164 Color: 1
Size: 336735 Color: 1
Size: 305102 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 395430 Color: 0
Size: 338312 Color: 1
Size: 266259 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 363078 Color: 1
Size: 318877 Color: 0
Size: 318046 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 364557 Color: 1
Size: 327196 Color: 0
Size: 308248 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 367513 Color: 1
Size: 343700 Color: 1
Size: 288788 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 368597 Color: 1
Size: 326175 Color: 0
Size: 305229 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 370378 Color: 0
Size: 330373 Color: 1
Size: 299250 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 374663 Color: 1
Size: 338800 Color: 0
Size: 286538 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 375265 Color: 0
Size: 331248 Color: 0
Size: 293488 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 376417 Color: 0
Size: 331808 Color: 1
Size: 291776 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 376728 Color: 0
Size: 324995 Color: 0
Size: 298278 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 376856 Color: 0
Size: 372296 Color: 1
Size: 250849 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 377181 Color: 1
Size: 351669 Color: 0
Size: 271151 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 377869 Color: 1
Size: 345542 Color: 0
Size: 276590 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 378242 Color: 0
Size: 314677 Color: 0
Size: 307082 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 378980 Color: 1
Size: 324006 Color: 0
Size: 297015 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 379377 Color: 1
Size: 327610 Color: 1
Size: 293014 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 379538 Color: 0
Size: 366677 Color: 0
Size: 253786 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 379958 Color: 1
Size: 310507 Color: 1
Size: 309536 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 380154 Color: 1
Size: 320458 Color: 0
Size: 299389 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 380456 Color: 0
Size: 337711 Color: 0
Size: 281834 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 380705 Color: 1
Size: 338173 Color: 0
Size: 281123 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 380932 Color: 0
Size: 324006 Color: 1
Size: 295063 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 380998 Color: 0
Size: 323624 Color: 0
Size: 295379 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 381944 Color: 0
Size: 311578 Color: 1
Size: 306479 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 382011 Color: 1
Size: 325605 Color: 1
Size: 292385 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 383610 Color: 1
Size: 349803 Color: 0
Size: 266588 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 384677 Color: 0
Size: 329936 Color: 1
Size: 285388 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 384886 Color: 1
Size: 338048 Color: 0
Size: 277067 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 385535 Color: 0
Size: 350338 Color: 0
Size: 264128 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 385545 Color: 0
Size: 336753 Color: 0
Size: 277703 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 385719 Color: 1
Size: 311268 Color: 1
Size: 303014 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 386034 Color: 0
Size: 315913 Color: 1
Size: 298054 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 386243 Color: 0
Size: 346001 Color: 1
Size: 267757 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 386121 Color: 1
Size: 361034 Color: 0
Size: 252846 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 386302 Color: 1
Size: 361335 Color: 1
Size: 252364 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 386366 Color: 0
Size: 362879 Color: 1
Size: 250756 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 387421 Color: 1
Size: 348884 Color: 1
Size: 263696 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 387388 Color: 0
Size: 359900 Color: 1
Size: 252713 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 387527 Color: 0
Size: 321903 Color: 0
Size: 290571 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 387748 Color: 1
Size: 333742 Color: 0
Size: 278511 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 388775 Color: 1
Size: 306314 Color: 0
Size: 304912 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 390331 Color: 0
Size: 325279 Color: 1
Size: 284391 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 390678 Color: 0
Size: 309326 Color: 1
Size: 299997 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 391612 Color: 0
Size: 329373 Color: 1
Size: 279016 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 391846 Color: 0
Size: 313445 Color: 0
Size: 294710 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 391587 Color: 1
Size: 318341 Color: 0
Size: 290073 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 391922 Color: 0
Size: 346975 Color: 0
Size: 261104 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 392387 Color: 1
Size: 347241 Color: 1
Size: 260373 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 393092 Color: 0
Size: 344297 Color: 0
Size: 262612 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 393144 Color: 0
Size: 338098 Color: 1
Size: 268759 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 393210 Color: 1
Size: 355978 Color: 1
Size: 250813 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 394371 Color: 0
Size: 319633 Color: 0
Size: 285997 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 394530 Color: 1
Size: 320123 Color: 0
Size: 285348 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 395056 Color: 1
Size: 309306 Color: 1
Size: 295639 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 395341 Color: 0
Size: 334416 Color: 0
Size: 270244 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 395436 Color: 0
Size: 338997 Color: 1
Size: 265568 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 396119 Color: 0
Size: 351260 Color: 1
Size: 252622 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 396317 Color: 1
Size: 328271 Color: 0
Size: 275413 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 396458 Color: 1
Size: 352228 Color: 1
Size: 251315 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 397433 Color: 0
Size: 332559 Color: 1
Size: 270009 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 397514 Color: 0
Size: 347370 Color: 0
Size: 255117 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 397514 Color: 0
Size: 301628 Color: 1
Size: 300859 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 397954 Color: 0
Size: 320528 Color: 1
Size: 281519 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 398012 Color: 0
Size: 316159 Color: 1
Size: 285830 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 398285 Color: 1
Size: 319279 Color: 1
Size: 282437 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 398523 Color: 0
Size: 337896 Color: 0
Size: 263582 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 398775 Color: 0
Size: 324905 Color: 1
Size: 276321 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 398537 Color: 1
Size: 307736 Color: 0
Size: 293728 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 398848 Color: 0
Size: 344261 Color: 0
Size: 256892 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 399223 Color: 1
Size: 337796 Color: 0
Size: 262982 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 399354 Color: 1
Size: 313719 Color: 0
Size: 286928 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 400451 Color: 1
Size: 322329 Color: 0
Size: 277221 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 400398 Color: 0
Size: 345278 Color: 0
Size: 254325 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 401722 Color: 0
Size: 337167 Color: 0
Size: 261112 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 401880 Color: 0
Size: 330564 Color: 1
Size: 267557 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 402549 Color: 0
Size: 343917 Color: 1
Size: 253535 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 402594 Color: 0
Size: 344098 Color: 1
Size: 253309 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 402635 Color: 0
Size: 302404 Color: 1
Size: 294962 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 402652 Color: 1
Size: 309004 Color: 0
Size: 288345 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 403089 Color: 0
Size: 299666 Color: 1
Size: 297246 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 403927 Color: 0
Size: 342650 Color: 0
Size: 253424 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 404529 Color: 1
Size: 315528 Color: 0
Size: 279944 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 404527 Color: 0
Size: 323538 Color: 0
Size: 271936 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 405795 Color: 0
Size: 314886 Color: 1
Size: 279320 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 406152 Color: 1
Size: 321327 Color: 0
Size: 272522 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 406301 Color: 0
Size: 299574 Color: 1
Size: 294126 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 406185 Color: 1
Size: 303690 Color: 0
Size: 290126 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 406323 Color: 0
Size: 311940 Color: 1
Size: 281738 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 406477 Color: 1
Size: 341000 Color: 0
Size: 252524 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 407687 Color: 0
Size: 297543 Color: 0
Size: 294771 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 407901 Color: 0
Size: 332039 Color: 1
Size: 260061 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 407720 Color: 1
Size: 304043 Color: 1
Size: 288238 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 407816 Color: 1
Size: 337758 Color: 0
Size: 254427 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 409029 Color: 0
Size: 335825 Color: 1
Size: 255147 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 408998 Color: 1
Size: 331648 Color: 1
Size: 259355 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 409613 Color: 0
Size: 328606 Color: 0
Size: 261782 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 410543 Color: 0
Size: 334185 Color: 1
Size: 255273 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 411361 Color: 1
Size: 335604 Color: 0
Size: 253036 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 410974 Color: 0
Size: 321176 Color: 0
Size: 267851 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 411886 Color: 1
Size: 332605 Color: 0
Size: 255510 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 411993 Color: 1
Size: 337449 Color: 0
Size: 250559 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 412016 Color: 1
Size: 332187 Color: 0
Size: 255798 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 412585 Color: 0
Size: 294763 Color: 0
Size: 292653 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 412304 Color: 1
Size: 336586 Color: 0
Size: 251111 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 412655 Color: 1
Size: 322734 Color: 0
Size: 264612 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 412718 Color: 0
Size: 296011 Color: 1
Size: 291272 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 412752 Color: 1
Size: 307268 Color: 1
Size: 279981 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 413741 Color: 0
Size: 319692 Color: 1
Size: 266568 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 416615 Color: 1
Size: 294174 Color: 1
Size: 289212 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 417346 Color: 0
Size: 298156 Color: 0
Size: 284499 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 418098 Color: 0
Size: 318843 Color: 0
Size: 263060 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 418068 Color: 1
Size: 307602 Color: 0
Size: 274331 Color: 1

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 418424 Color: 1
Size: 316269 Color: 0
Size: 265308 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 418437 Color: 0
Size: 309128 Color: 0
Size: 272436 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 418452 Color: 1
Size: 329711 Color: 1
Size: 251838 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 419363 Color: 1
Size: 297833 Color: 0
Size: 282805 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 419218 Color: 0
Size: 328054 Color: 0
Size: 252729 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 419854 Color: 1
Size: 302490 Color: 0
Size: 277657 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 420869 Color: 0
Size: 310988 Color: 1
Size: 268144 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 420942 Color: 1
Size: 298622 Color: 0
Size: 280437 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 420906 Color: 0
Size: 294435 Color: 1
Size: 284660 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 421096 Color: 0
Size: 323926 Color: 1
Size: 254979 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 421855 Color: 0
Size: 311646 Color: 1
Size: 266500 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 422079 Color: 1
Size: 306361 Color: 0
Size: 271561 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 422345 Color: 0
Size: 299106 Color: 0
Size: 278550 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 422870 Color: 1
Size: 325816 Color: 1
Size: 251315 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 423224 Color: 0
Size: 303393 Color: 0
Size: 273384 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 423421 Color: 1
Size: 294001 Color: 1
Size: 282579 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 423924 Color: 1
Size: 312758 Color: 1
Size: 263319 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 424001 Color: 1
Size: 322727 Color: 0
Size: 253273 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 424124 Color: 1
Size: 315155 Color: 1
Size: 260722 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 424521 Color: 1
Size: 306462 Color: 0
Size: 269018 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 426280 Color: 1
Size: 315487 Color: 1
Size: 258234 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 426536 Color: 1
Size: 298900 Color: 1
Size: 274565 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 426582 Color: 1
Size: 317283 Color: 0
Size: 256136 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 426784 Color: 1
Size: 310200 Color: 0
Size: 263017 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 426994 Color: 1
Size: 305527 Color: 0
Size: 267480 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 427500 Color: 0
Size: 313760 Color: 0
Size: 258741 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 427357 Color: 1
Size: 315683 Color: 1
Size: 256961 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 427924 Color: 0
Size: 288899 Color: 0
Size: 283178 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 428519 Color: 1
Size: 289380 Color: 1
Size: 282102 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 428798 Color: 0
Size: 306533 Color: 1
Size: 264670 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 430139 Color: 0
Size: 316604 Color: 1
Size: 253258 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 430818 Color: 0
Size: 305286 Color: 1
Size: 263897 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 430923 Color: 0
Size: 311853 Color: 1
Size: 257225 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 431861 Color: 1
Size: 315523 Color: 0
Size: 252617 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 432352 Color: 1
Size: 313176 Color: 0
Size: 254473 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 432427 Color: 1
Size: 308045 Color: 0
Size: 259529 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 432960 Color: 1
Size: 291569 Color: 1
Size: 275472 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 433075 Color: 1
Size: 303367 Color: 0
Size: 263559 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 433200 Color: 1
Size: 298225 Color: 0
Size: 268576 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 434207 Color: 1
Size: 286877 Color: 0
Size: 278917 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 435148 Color: 0
Size: 298650 Color: 1
Size: 266203 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 435255 Color: 0
Size: 296160 Color: 0
Size: 268586 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 435529 Color: 1
Size: 303882 Color: 0
Size: 260590 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 436909 Color: 1
Size: 310986 Color: 0
Size: 252106 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 437932 Color: 1
Size: 288388 Color: 0
Size: 273681 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 438361 Color: 1
Size: 305941 Color: 1
Size: 255699 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 438517 Color: 0
Size: 302631 Color: 0
Size: 258853 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 439434 Color: 1
Size: 304682 Color: 1
Size: 255885 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 440075 Color: 0
Size: 289861 Color: 0
Size: 270065 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 440290 Color: 0
Size: 294293 Color: 1
Size: 265418 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 440681 Color: 0
Size: 297803 Color: 1
Size: 261517 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 440985 Color: 1
Size: 301129 Color: 1
Size: 257887 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 441183 Color: 0
Size: 294839 Color: 0
Size: 263979 Color: 1

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 442149 Color: 1
Size: 307484 Color: 0
Size: 250368 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 442615 Color: 1
Size: 299696 Color: 1
Size: 257690 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 442353 Color: 0
Size: 281139 Color: 1
Size: 276509 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 442944 Color: 0
Size: 300494 Color: 0
Size: 256563 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 444415 Color: 0
Size: 282167 Color: 1
Size: 273419 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 444473 Color: 0
Size: 287858 Color: 1
Size: 267670 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 444866 Color: 0
Size: 292372 Color: 0
Size: 262763 Color: 1

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 445685 Color: 1
Size: 281343 Color: 0
Size: 272973 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 446417 Color: 0
Size: 303047 Color: 1
Size: 250537 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 446749 Color: 1
Size: 290291 Color: 0
Size: 262961 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 447098 Color: 0
Size: 295213 Color: 1
Size: 257690 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 447425 Color: 0
Size: 290352 Color: 1
Size: 262224 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 447572 Color: 0
Size: 287659 Color: 1
Size: 264770 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 447625 Color: 0
Size: 301710 Color: 0
Size: 250666 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 447465 Color: 1
Size: 276936 Color: 0
Size: 275600 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 448090 Color: 1
Size: 282036 Color: 1
Size: 269875 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 448759 Color: 0
Size: 286835 Color: 0
Size: 264407 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 448985 Color: 0
Size: 294969 Color: 0
Size: 256047 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 448866 Color: 1
Size: 299307 Color: 1
Size: 251828 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 449439 Color: 0
Size: 282956 Color: 0
Size: 267606 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 449606 Color: 1
Size: 284303 Color: 1
Size: 266092 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 449469 Color: 0
Size: 280779 Color: 1
Size: 269753 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 449904 Color: 1
Size: 283791 Color: 1
Size: 266306 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 450028 Color: 0
Size: 290902 Color: 0
Size: 259071 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 450277 Color: 1
Size: 299657 Color: 0
Size: 250067 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 450324 Color: 0
Size: 296673 Color: 0
Size: 253004 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 450583 Color: 1
Size: 280170 Color: 0
Size: 269248 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 450597 Color: 1
Size: 285873 Color: 0
Size: 263531 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 451019 Color: 1
Size: 280802 Color: 0
Size: 268180 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 451512 Color: 1
Size: 297778 Color: 1
Size: 250711 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 451558 Color: 1
Size: 288949 Color: 1
Size: 259494 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 453428 Color: 1
Size: 291016 Color: 0
Size: 255557 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 453286 Color: 0
Size: 274176 Color: 0
Size: 272539 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 454101 Color: 1
Size: 286112 Color: 0
Size: 259788 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 453962 Color: 0
Size: 285001 Color: 1
Size: 261038 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 454796 Color: 0
Size: 293215 Color: 1
Size: 251990 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 454918 Color: 1
Size: 278161 Color: 0
Size: 266922 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 454935 Color: 1
Size: 274492 Color: 1
Size: 270574 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 455370 Color: 0
Size: 291813 Color: 1
Size: 252818 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 455956 Color: 1
Size: 287723 Color: 0
Size: 256322 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 455663 Color: 0
Size: 278794 Color: 0
Size: 265544 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 456211 Color: 0
Size: 283786 Color: 0
Size: 260004 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 457400 Color: 1
Size: 286357 Color: 0
Size: 256244 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 457380 Color: 0
Size: 292512 Color: 1
Size: 250109 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 457836 Color: 1
Size: 282503 Color: 0
Size: 259662 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 459479 Color: 1
Size: 284927 Color: 0
Size: 255595 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 460083 Color: 1
Size: 289672 Color: 1
Size: 250246 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 460245 Color: 0
Size: 273023 Color: 1
Size: 266733 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 461678 Color: 0
Size: 271610 Color: 0
Size: 266713 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 462014 Color: 1
Size: 281788 Color: 0
Size: 256199 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 462089 Color: 1
Size: 271984 Color: 1
Size: 265928 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 462446 Color: 0
Size: 279458 Color: 1
Size: 258097 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 462679 Color: 0
Size: 274821 Color: 1
Size: 262501 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 462762 Color: 1
Size: 282133 Color: 0
Size: 255106 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 462920 Color: 1
Size: 271503 Color: 0
Size: 265578 Color: 1

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 463080 Color: 1
Size: 277254 Color: 0
Size: 259667 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 462771 Color: 0
Size: 281914 Color: 1
Size: 255316 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 463622 Color: 1
Size: 277507 Color: 1
Size: 258872 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 463948 Color: 1
Size: 281078 Color: 0
Size: 254975 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 464265 Color: 0
Size: 280406 Color: 0
Size: 255330 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 464167 Color: 1
Size: 278385 Color: 0
Size: 257449 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 465970 Color: 1
Size: 280726 Color: 0
Size: 253305 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 467003 Color: 0
Size: 271064 Color: 1
Size: 261934 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 467987 Color: 0
Size: 267088 Color: 0
Size: 264926 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 468043 Color: 0
Size: 278155 Color: 1
Size: 253803 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 468641 Color: 0
Size: 273249 Color: 0
Size: 258111 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 468652 Color: 1
Size: 277890 Color: 0
Size: 253459 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 469076 Color: 0
Size: 272484 Color: 0
Size: 258441 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 469323 Color: 0
Size: 266824 Color: 0
Size: 263854 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 469320 Color: 1
Size: 274124 Color: 1
Size: 256557 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 469821 Color: 0
Size: 266001 Color: 0
Size: 264179 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 470170 Color: 1
Size: 273645 Color: 0
Size: 256186 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 471180 Color: 1
Size: 273066 Color: 0
Size: 255755 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 471595 Color: 0
Size: 278330 Color: 0
Size: 250076 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 471466 Color: 1
Size: 272376 Color: 1
Size: 256159 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 472062 Color: 1
Size: 270358 Color: 1
Size: 257581 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 472545 Color: 1
Size: 267208 Color: 1
Size: 260248 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 473719 Color: 1
Size: 272365 Color: 1
Size: 253917 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 473999 Color: 0
Size: 272657 Color: 1
Size: 253345 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 474104 Color: 1
Size: 272525 Color: 0
Size: 253372 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 474185 Color: 1
Size: 273948 Color: 0
Size: 251868 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 474231 Color: 0
Size: 273843 Color: 1
Size: 251927 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 474571 Color: 1
Size: 268340 Color: 1
Size: 257090 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 474382 Color: 0
Size: 269429 Color: 1
Size: 256190 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 475073 Color: 1
Size: 271662 Color: 0
Size: 253266 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 475524 Color: 1
Size: 271085 Color: 1
Size: 253392 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 475829 Color: 1
Size: 273875 Color: 0
Size: 250297 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 476223 Color: 1
Size: 269699 Color: 0
Size: 254079 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 478062 Color: 1
Size: 265850 Color: 0
Size: 256089 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 478063 Color: 1
Size: 262874 Color: 0
Size: 259064 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 478807 Color: 0
Size: 261182 Color: 1
Size: 260012 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 478845 Color: 1
Size: 262254 Color: 0
Size: 258902 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 479359 Color: 1
Size: 266901 Color: 0
Size: 253741 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 480068 Color: 1
Size: 268481 Color: 0
Size: 251452 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 480293 Color: 1
Size: 260895 Color: 0
Size: 258813 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 480350 Color: 0
Size: 264332 Color: 0
Size: 255319 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 480444 Color: 1
Size: 269527 Color: 1
Size: 250030 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 480878 Color: 1
Size: 266478 Color: 0
Size: 252645 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 481252 Color: 1
Size: 264892 Color: 1
Size: 253857 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 481577 Color: 0
Size: 264195 Color: 0
Size: 254229 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 482955 Color: 1
Size: 259246 Color: 0
Size: 257800 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 483536 Color: 0
Size: 265734 Color: 1
Size: 250731 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 483829 Color: 1
Size: 265357 Color: 1
Size: 250815 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 484500 Color: 0
Size: 260103 Color: 1
Size: 255398 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 486655 Color: 1
Size: 257526 Color: 0
Size: 255820 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 486771 Color: 1
Size: 262658 Color: 0
Size: 250572 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 486880 Color: 0
Size: 260114 Color: 0
Size: 253007 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 487346 Color: 1
Size: 260052 Color: 1
Size: 252603 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 487724 Color: 0
Size: 259883 Color: 0
Size: 252394 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 488081 Color: 1
Size: 261829 Color: 1
Size: 250091 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 488198 Color: 1
Size: 256114 Color: 0
Size: 255689 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 488854 Color: 1
Size: 255690 Color: 0
Size: 255457 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 489193 Color: 0
Size: 256997 Color: 1
Size: 253811 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 489577 Color: 1
Size: 258077 Color: 0
Size: 252347 Color: 1

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 489740 Color: 0
Size: 258618 Color: 0
Size: 251643 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 490293 Color: 1
Size: 259397 Color: 0
Size: 250311 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 490674 Color: 1
Size: 258004 Color: 0
Size: 251323 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 491836 Color: 1
Size: 257130 Color: 0
Size: 251035 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 492567 Color: 0
Size: 257364 Color: 1
Size: 250070 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 492549 Color: 1
Size: 254569 Color: 0
Size: 252883 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 492731 Color: 0
Size: 255818 Color: 1
Size: 251452 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 492725 Color: 1
Size: 254460 Color: 0
Size: 252816 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 492878 Color: 1
Size: 255289 Color: 1
Size: 251834 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 492904 Color: 0
Size: 255440 Color: 1
Size: 251657 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 493480 Color: 1
Size: 253910 Color: 0
Size: 252611 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 493585 Color: 0
Size: 255106 Color: 1
Size: 251310 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 494492 Color: 1
Size: 254547 Color: 1
Size: 250962 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 494690 Color: 0
Size: 253054 Color: 0
Size: 252257 Color: 1

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 372381 Color: 0
Size: 368941 Color: 0
Size: 258678 Color: 1

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 414931 Color: 1
Size: 306359 Color: 0
Size: 278710 Color: 0

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 385729 Color: 1
Size: 346340 Color: 0
Size: 267931 Color: 0

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 368308 Color: 1
Size: 329229 Color: 1
Size: 302463 Color: 0

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 396831 Color: 0
Size: 329718 Color: 0
Size: 273451 Color: 1

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 391106 Color: 0
Size: 344321 Color: 0
Size: 264573 Color: 1

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 454954 Color: 0
Size: 276430 Color: 0
Size: 268616 Color: 1

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 380079 Color: 1
Size: 330417 Color: 1
Size: 289504 Color: 0

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 489371 Color: 1
Size: 258686 Color: 1
Size: 251943 Color: 0

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 408839 Color: 0
Size: 324982 Color: 1
Size: 266179 Color: 0

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 484932 Color: 0
Size: 262900 Color: 0
Size: 252168 Color: 1

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 418028 Color: 0
Size: 322742 Color: 1
Size: 259230 Color: 1

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 462315 Color: 0
Size: 285157 Color: 1
Size: 252528 Color: 0

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 385270 Color: 0
Size: 329288 Color: 1
Size: 285442 Color: 0

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 413548 Color: 0
Size: 307387 Color: 1
Size: 279065 Color: 0

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 373474 Color: 0
Size: 369292 Color: 1
Size: 257234 Color: 0

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 440994 Color: 1
Size: 307291 Color: 1
Size: 251715 Color: 0

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 371571 Color: 1
Size: 360202 Color: 0
Size: 268227 Color: 0

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 384872 Color: 1
Size: 320281 Color: 1
Size: 294847 Color: 0

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 367925 Color: 1
Size: 343018 Color: 1
Size: 289057 Color: 0

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 369181 Color: 0
Size: 322538 Color: 0
Size: 308281 Color: 1

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 369055 Color: 1
Size: 328285 Color: 1
Size: 302660 Color: 0

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 370020 Color: 1
Size: 343676 Color: 0
Size: 286304 Color: 1

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 370934 Color: 1
Size: 324404 Color: 0
Size: 304662 Color: 0

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 373450 Color: 1
Size: 357457 Color: 0
Size: 269093 Color: 1

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 374395 Color: 0
Size: 366397 Color: 0
Size: 259208 Color: 1

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 374708 Color: 1
Size: 371153 Color: 0
Size: 254139 Color: 1

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 375051 Color: 1
Size: 339964 Color: 1
Size: 284985 Color: 0

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 375623 Color: 1
Size: 312446 Color: 0
Size: 311931 Color: 0

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 376026 Color: 0
Size: 345669 Color: 1
Size: 278305 Color: 0

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 376537 Color: 0
Size: 324852 Color: 0
Size: 298611 Color: 1

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 376394 Color: 1
Size: 354524 Color: 0
Size: 269082 Color: 1

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 377070 Color: 0
Size: 353739 Color: 0
Size: 269191 Color: 1

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 377686 Color: 0
Size: 357634 Color: 0
Size: 264680 Color: 1

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 377972 Color: 0
Size: 315090 Color: 0
Size: 306938 Color: 1

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 378146 Color: 1
Size: 363335 Color: 1
Size: 258519 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 381026 Color: 1
Size: 321171 Color: 0
Size: 297803 Color: 1

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 383730 Color: 0
Size: 360834 Color: 0
Size: 255436 Color: 1

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 386078 Color: 0
Size: 343026 Color: 0
Size: 270896 Color: 1

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 387089 Color: 0
Size: 354590 Color: 0
Size: 258321 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 387499 Color: 1
Size: 358010 Color: 1
Size: 254491 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 388497 Color: 1
Size: 344026 Color: 0
Size: 267477 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 388365 Color: 0
Size: 333590 Color: 1
Size: 278045 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 392851 Color: 0
Size: 352982 Color: 1
Size: 254167 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 392875 Color: 1
Size: 321623 Color: 1
Size: 285502 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 394399 Color: 0
Size: 314042 Color: 0
Size: 291559 Color: 1

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 395426 Color: 0
Size: 354104 Color: 1
Size: 250470 Color: 1

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 396158 Color: 1
Size: 339934 Color: 0
Size: 263908 Color: 1

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 396152 Color: 0
Size: 338221 Color: 1
Size: 265627 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 397920 Color: 0
Size: 340674 Color: 1
Size: 261406 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 398042 Color: 1
Size: 333215 Color: 1
Size: 268743 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 399533 Color: 0
Size: 307801 Color: 1
Size: 292666 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 401687 Color: 1
Size: 317911 Color: 0
Size: 280402 Color: 1

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 402037 Color: 0
Size: 303101 Color: 0
Size: 294862 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 403500 Color: 1
Size: 322644 Color: 1
Size: 273856 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 406062 Color: 0
Size: 307003 Color: 1
Size: 286935 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 408195 Color: 0
Size: 334569 Color: 1
Size: 257236 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 409241 Color: 0
Size: 323328 Color: 1
Size: 267431 Color: 1

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 413472 Color: 1
Size: 293581 Color: 1
Size: 292947 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 414537 Color: 1
Size: 335010 Color: 0
Size: 250453 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 416455 Color: 0
Size: 324240 Color: 0
Size: 259305 Color: 1

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 420971 Color: 1
Size: 291465 Color: 1
Size: 287564 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 421522 Color: 0
Size: 296079 Color: 1
Size: 282399 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 422795 Color: 1
Size: 322522 Color: 0
Size: 254683 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 423257 Color: 0
Size: 313573 Color: 0
Size: 263170 Color: 1

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 426281 Color: 0
Size: 301602 Color: 0
Size: 272117 Color: 1

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 427297 Color: 0
Size: 299292 Color: 1
Size: 273411 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 431211 Color: 0
Size: 285277 Color: 1
Size: 283512 Color: 1

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 438346 Color: 0
Size: 291348 Color: 1
Size: 270306 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 441582 Color: 1
Size: 307088 Color: 0
Size: 251330 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 445769 Color: 0
Size: 290683 Color: 0
Size: 263548 Color: 1

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 452053 Color: 0
Size: 297096 Color: 1
Size: 250851 Color: 0

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 453587 Color: 1
Size: 286576 Color: 1
Size: 259837 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 454161 Color: 1
Size: 288205 Color: 1
Size: 257634 Color: 0

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 459187 Color: 1
Size: 270990 Color: 1
Size: 269823 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 459199 Color: 0
Size: 277304 Color: 1
Size: 263497 Color: 0

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 464135 Color: 1
Size: 269539 Color: 1
Size: 266326 Color: 0

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 466623 Color: 0
Size: 281455 Color: 1
Size: 251922 Color: 0

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 467326 Color: 0
Size: 273462 Color: 0
Size: 259212 Color: 1

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 469278 Color: 0
Size: 280651 Color: 1
Size: 250071 Color: 1

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 469731 Color: 0
Size: 280046 Color: 1
Size: 250223 Color: 1

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 469473 Color: 1
Size: 273113 Color: 0
Size: 257414 Color: 1

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 474891 Color: 0
Size: 266775 Color: 1
Size: 258334 Color: 0

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 478595 Color: 0
Size: 266028 Color: 1
Size: 255377 Color: 1

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 485793 Color: 1
Size: 262210 Color: 1
Size: 251997 Color: 0

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 487439 Color: 0
Size: 261513 Color: 1
Size: 251048 Color: 1

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 490171 Color: 0
Size: 258751 Color: 0
Size: 251078 Color: 1

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 367241 Color: 0
Size: 320306 Color: 1
Size: 312453 Color: 1

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 497016 Color: 1
Size: 252264 Color: 0
Size: 250720 Color: 1

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 498642 Color: 0
Size: 250894 Color: 0
Size: 250464 Color: 1

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 377265 Color: 1
Size: 348543 Color: 0
Size: 274191 Color: 1

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 399461 Color: 1
Size: 303834 Color: 0
Size: 296704 Color: 1

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 362082 Color: 0
Size: 333974 Color: 1
Size: 303943 Color: 0

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 361559 Color: 1
Size: 342618 Color: 1
Size: 295822 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 362242 Color: 0
Size: 349383 Color: 0
Size: 288374 Color: 1

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 363574 Color: 0
Size: 343111 Color: 0
Size: 293314 Color: 1

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 366192 Color: 1
Size: 339125 Color: 0
Size: 294682 Color: 1

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 365539 Color: 0
Size: 351215 Color: 0
Size: 283245 Color: 1

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 367866 Color: 1
Size: 334066 Color: 1
Size: 298067 Color: 0

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 369131 Color: 1
Size: 349519 Color: 0
Size: 281349 Color: 1

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 370947 Color: 1
Size: 351770 Color: 0
Size: 277282 Color: 1

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 371668 Color: 1
Size: 316116 Color: 0
Size: 312215 Color: 1

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 371493 Color: 0
Size: 361805 Color: 0
Size: 266701 Color: 1

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 372512 Color: 0
Size: 357314 Color: 0
Size: 270173 Color: 1

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 374299 Color: 0
Size: 325776 Color: 1
Size: 299924 Color: 0

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 374522 Color: 1
Size: 331176 Color: 1
Size: 294301 Color: 0

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 375699 Color: 0
Size: 333106 Color: 0
Size: 291194 Color: 1

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 376274 Color: 0
Size: 337772 Color: 1
Size: 285953 Color: 1

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 376428 Color: 1
Size: 328835 Color: 1
Size: 294736 Color: 0

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 378087 Color: 0
Size: 364791 Color: 1
Size: 257121 Color: 0

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 382032 Color: 0
Size: 362182 Color: 0
Size: 255785 Color: 1

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 382205 Color: 0
Size: 326082 Color: 1
Size: 291712 Color: 0

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 382581 Color: 1
Size: 330091 Color: 1
Size: 287327 Color: 0

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 389545 Color: 0
Size: 323039 Color: 1
Size: 287415 Color: 0

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 390481 Color: 1
Size: 322349 Color: 1
Size: 287169 Color: 0

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 396845 Color: 0
Size: 312546 Color: 1
Size: 290608 Color: 0

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 404763 Color: 1
Size: 340418 Color: 1
Size: 254818 Color: 0

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 405457 Color: 0
Size: 340690 Color: 1
Size: 253852 Color: 1

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 409168 Color: 0
Size: 328713 Color: 1
Size: 262118 Color: 0

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 410445 Color: 0
Size: 308846 Color: 0
Size: 280708 Color: 1

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 410815 Color: 1
Size: 309645 Color: 1
Size: 279539 Color: 0

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 411450 Color: 0
Size: 327174 Color: 0
Size: 261375 Color: 1

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 413422 Color: 1
Size: 335253 Color: 0
Size: 251324 Color: 1

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 427985 Color: 0
Size: 304846 Color: 1
Size: 267168 Color: 1

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 431052 Color: 0
Size: 318752 Color: 1
Size: 250195 Color: 0

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 436473 Color: 0
Size: 301437 Color: 0
Size: 262089 Color: 1

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 437084 Color: 1
Size: 309843 Color: 0
Size: 253072 Color: 0

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 439513 Color: 1
Size: 300794 Color: 0
Size: 259692 Color: 1

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 442960 Color: 0
Size: 293846 Color: 1
Size: 263193 Color: 1

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 452113 Color: 0
Size: 281928 Color: 1
Size: 265958 Color: 0

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 453849 Color: 1
Size: 278302 Color: 0
Size: 267848 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 464048 Color: 1
Size: 277336 Color: 0
Size: 258615 Color: 1

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 472273 Color: 0
Size: 270012 Color: 0
Size: 257714 Color: 1

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 475319 Color: 1
Size: 270392 Color: 0
Size: 254288 Color: 1

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 477050 Color: 1
Size: 272929 Color: 0
Size: 250020 Color: 1

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 477385 Color: 1
Size: 271427 Color: 0
Size: 251187 Color: 0

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 486086 Color: 1
Size: 262263 Color: 0
Size: 251650 Color: 0

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 492182 Color: 0
Size: 257699 Color: 1
Size: 250118 Color: 1

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 374705 Color: 0
Size: 331242 Color: 1
Size: 294052 Color: 0

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 360294 Color: 0
Size: 331064 Color: 0
Size: 308641 Color: 1

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 356280 Color: 0
Size: 346574 Color: 1
Size: 297145 Color: 1

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 347542 Color: 0
Size: 347173 Color: 0
Size: 305284 Color: 1

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 338774 Color: 1
Size: 337422 Color: 0
Size: 323803 Color: 1

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 492884 Color: 0
Size: 253980 Color: 0
Size: 253135 Color: 1

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 368333 Color: 0
Size: 347889 Color: 1
Size: 283776 Color: 1

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 358532 Color: 0
Size: 329151 Color: 0
Size: 312315 Color: 1

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 371428 Color: 0
Size: 366949 Color: 0
Size: 261621 Color: 1

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 374274 Color: 1
Size: 354036 Color: 1
Size: 271688 Color: 0

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 374798 Color: 1
Size: 371538 Color: 1
Size: 253662 Color: 0

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 378512 Color: 1
Size: 339057 Color: 1
Size: 282429 Color: 0

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 379381 Color: 1
Size: 317743 Color: 0
Size: 302874 Color: 0

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 379843 Color: 0
Size: 349937 Color: 1
Size: 270218 Color: 0

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 379968 Color: 0
Size: 321779 Color: 0
Size: 298251 Color: 1

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 380232 Color: 1
Size: 325117 Color: 0
Size: 294649 Color: 0

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 380995 Color: 0
Size: 332416 Color: 1
Size: 286587 Color: 0

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 382347 Color: 0
Size: 346943 Color: 0
Size: 270708 Color: 1

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 394942 Color: 0
Size: 350190 Color: 0
Size: 254866 Color: 1

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 395113 Color: 1
Size: 325449 Color: 0
Size: 279436 Color: 1

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 395887 Color: 0
Size: 316575 Color: 1
Size: 287536 Color: 1

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 403833 Color: 0
Size: 298679 Color: 1
Size: 297486 Color: 0

Bin 524: 3 of cap free
Amount of items: 3
Items: 
Size: 404025 Color: 0
Size: 324599 Color: 1
Size: 271374 Color: 1

Bin 525: 3 of cap free
Amount of items: 3
Items: 
Size: 418565 Color: 0
Size: 325491 Color: 0
Size: 255942 Color: 1

Bin 526: 3 of cap free
Amount of items: 3
Items: 
Size: 429351 Color: 1
Size: 301167 Color: 0
Size: 269480 Color: 0

Bin 527: 3 of cap free
Amount of items: 3
Items: 
Size: 483726 Color: 1
Size: 263367 Color: 0
Size: 252905 Color: 0

Bin 528: 3 of cap free
Amount of items: 3
Items: 
Size: 350252 Color: 0
Size: 345311 Color: 1
Size: 304435 Color: 0

Bin 529: 3 of cap free
Amount of items: 3
Items: 
Size: 350710 Color: 0
Size: 329021 Color: 1
Size: 320267 Color: 1

Bin 530: 4 of cap free
Amount of items: 3
Items: 
Size: 375676 Color: 1
Size: 356013 Color: 0
Size: 268308 Color: 1

Bin 531: 4 of cap free
Amount of items: 3
Items: 
Size: 358969 Color: 0
Size: 347469 Color: 0
Size: 293559 Color: 1

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 437696 Color: 0
Size: 307204 Color: 1
Size: 255097 Color: 1

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 372314 Color: 1
Size: 332487 Color: 1
Size: 295196 Color: 0

Bin 534: 4 of cap free
Amount of items: 3
Items: 
Size: 383141 Color: 1
Size: 350768 Color: 1
Size: 266088 Color: 0

Bin 535: 4 of cap free
Amount of items: 3
Items: 
Size: 367665 Color: 0
Size: 319394 Color: 0
Size: 312938 Color: 1

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 372900 Color: 0
Size: 350488 Color: 1
Size: 276609 Color: 1

Bin 537: 4 of cap free
Amount of items: 3
Items: 
Size: 373826 Color: 0
Size: 347834 Color: 1
Size: 278337 Color: 0

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 374010 Color: 1
Size: 336679 Color: 0
Size: 289308 Color: 1

Bin 539: 4 of cap free
Amount of items: 3
Items: 
Size: 383115 Color: 0
Size: 333783 Color: 0
Size: 283099 Color: 1

Bin 540: 4 of cap free
Amount of items: 3
Items: 
Size: 393888 Color: 1
Size: 350884 Color: 1
Size: 255225 Color: 0

Bin 541: 4 of cap free
Amount of items: 3
Items: 
Size: 412478 Color: 0
Size: 311413 Color: 1
Size: 276106 Color: 1

Bin 542: 4 of cap free
Amount of items: 3
Items: 
Size: 445680 Color: 1
Size: 285603 Color: 0
Size: 268714 Color: 1

Bin 543: 4 of cap free
Amount of items: 3
Items: 
Size: 352385 Color: 1
Size: 327300 Color: 0
Size: 320312 Color: 0

Bin 544: 4 of cap free
Amount of items: 3
Items: 
Size: 389775 Color: 1
Size: 358284 Color: 0
Size: 251938 Color: 1

Bin 545: 5 of cap free
Amount of items: 3
Items: 
Size: 461086 Color: 1
Size: 273158 Color: 0
Size: 265752 Color: 0

Bin 546: 5 of cap free
Amount of items: 3
Items: 
Size: 362134 Color: 0
Size: 322459 Color: 0
Size: 315403 Color: 1

Bin 547: 5 of cap free
Amount of items: 3
Items: 
Size: 366692 Color: 1
Size: 362852 Color: 1
Size: 270452 Color: 0

Bin 548: 5 of cap free
Amount of items: 3
Items: 
Size: 367646 Color: 0
Size: 341014 Color: 0
Size: 291336 Color: 1

Bin 549: 5 of cap free
Amount of items: 3
Items: 
Size: 370273 Color: 0
Size: 334700 Color: 1
Size: 295023 Color: 0

Bin 550: 5 of cap free
Amount of items: 3
Items: 
Size: 369928 Color: 1
Size: 366929 Color: 0
Size: 263139 Color: 1

Bin 551: 5 of cap free
Amount of items: 3
Items: 
Size: 372951 Color: 0
Size: 368152 Color: 1
Size: 258893 Color: 0

Bin 552: 5 of cap free
Amount of items: 3
Items: 
Size: 372856 Color: 1
Size: 364358 Color: 1
Size: 262782 Color: 0

Bin 553: 5 of cap free
Amount of items: 3
Items: 
Size: 374643 Color: 0
Size: 338657 Color: 0
Size: 286696 Color: 1

Bin 554: 5 of cap free
Amount of items: 3
Items: 
Size: 376751 Color: 0
Size: 324643 Color: 1
Size: 298602 Color: 0

Bin 555: 5 of cap free
Amount of items: 3
Items: 
Size: 377076 Color: 1
Size: 344649 Color: 1
Size: 278271 Color: 0

Bin 556: 5 of cap free
Amount of items: 3
Items: 
Size: 378465 Color: 1
Size: 312987 Color: 0
Size: 308544 Color: 1

Bin 557: 5 of cap free
Amount of items: 3
Items: 
Size: 394388 Color: 0
Size: 341561 Color: 0
Size: 264047 Color: 1

Bin 558: 5 of cap free
Amount of items: 3
Items: 
Size: 395491 Color: 1
Size: 337294 Color: 1
Size: 267211 Color: 0

Bin 559: 5 of cap free
Amount of items: 3
Items: 
Size: 442854 Color: 0
Size: 304957 Color: 1
Size: 252185 Color: 1

Bin 560: 5 of cap free
Amount of items: 3
Items: 
Size: 349226 Color: 1
Size: 346477 Color: 0
Size: 304293 Color: 0

Bin 561: 5 of cap free
Amount of items: 3
Items: 
Size: 359925 Color: 0
Size: 333573 Color: 0
Size: 306498 Color: 1

Bin 562: 5 of cap free
Amount of items: 3
Items: 
Size: 334487 Color: 0
Size: 334043 Color: 1
Size: 331466 Color: 1

Bin 563: 5 of cap free
Amount of items: 3
Items: 
Size: 346621 Color: 1
Size: 334533 Color: 0
Size: 318842 Color: 0

Bin 564: 5 of cap free
Amount of items: 3
Items: 
Size: 380603 Color: 1
Size: 336427 Color: 0
Size: 282966 Color: 0

Bin 565: 5 of cap free
Amount of items: 3
Items: 
Size: 429708 Color: 0
Size: 287756 Color: 1
Size: 282532 Color: 1

Bin 566: 6 of cap free
Amount of items: 3
Items: 
Size: 383224 Color: 1
Size: 350931 Color: 0
Size: 265840 Color: 1

Bin 567: 6 of cap free
Amount of items: 3
Items: 
Size: 397168 Color: 1
Size: 343548 Color: 1
Size: 259279 Color: 0

Bin 568: 6 of cap free
Amount of items: 3
Items: 
Size: 360377 Color: 1
Size: 345406 Color: 0
Size: 294212 Color: 1

Bin 569: 6 of cap free
Amount of items: 3
Items: 
Size: 362455 Color: 1
Size: 331018 Color: 1
Size: 306522 Color: 0

Bin 570: 6 of cap free
Amount of items: 3
Items: 
Size: 364022 Color: 1
Size: 350924 Color: 0
Size: 285049 Color: 1

Bin 571: 6 of cap free
Amount of items: 3
Items: 
Size: 365359 Color: 0
Size: 326173 Color: 0
Size: 308463 Color: 1

Bin 572: 6 of cap free
Amount of items: 3
Items: 
Size: 371891 Color: 1
Size: 358510 Color: 1
Size: 269594 Color: 0

Bin 573: 6 of cap free
Amount of items: 3
Items: 
Size: 385142 Color: 1
Size: 332258 Color: 1
Size: 282595 Color: 0

Bin 574: 6 of cap free
Amount of items: 3
Items: 
Size: 387614 Color: 1
Size: 316979 Color: 1
Size: 295402 Color: 0

Bin 575: 6 of cap free
Amount of items: 3
Items: 
Size: 386897 Color: 1
Size: 346221 Color: 1
Size: 266877 Color: 0

Bin 576: 6 of cap free
Amount of items: 3
Items: 
Size: 355360 Color: 1
Size: 327116 Color: 0
Size: 317519 Color: 1

Bin 577: 7 of cap free
Amount of items: 3
Items: 
Size: 375489 Color: 0
Size: 329296 Color: 0
Size: 295209 Color: 1

Bin 578: 7 of cap free
Amount of items: 3
Items: 
Size: 367006 Color: 1
Size: 345299 Color: 1
Size: 287689 Color: 0

Bin 579: 7 of cap free
Amount of items: 3
Items: 
Size: 356003 Color: 1
Size: 325772 Color: 1
Size: 318219 Color: 0

Bin 580: 7 of cap free
Amount of items: 3
Items: 
Size: 391338 Color: 0
Size: 337040 Color: 1
Size: 271616 Color: 1

Bin 581: 7 of cap free
Amount of items: 3
Items: 
Size: 371870 Color: 1
Size: 339999 Color: 0
Size: 288125 Color: 1

Bin 582: 9 of cap free
Amount of items: 3
Items: 
Size: 375000 Color: 1
Size: 315069 Color: 1
Size: 309923 Color: 0

Bin 583: 9 of cap free
Amount of items: 3
Items: 
Size: 376702 Color: 0
Size: 371611 Color: 0
Size: 251679 Color: 1

Bin 584: 9 of cap free
Amount of items: 3
Items: 
Size: 339636 Color: 1
Size: 332594 Color: 0
Size: 327762 Color: 0

Bin 585: 10 of cap free
Amount of items: 3
Items: 
Size: 371678 Color: 0
Size: 368605 Color: 1
Size: 259708 Color: 0

Bin 586: 10 of cap free
Amount of items: 3
Items: 
Size: 376302 Color: 1
Size: 338088 Color: 0
Size: 285601 Color: 1

Bin 587: 10 of cap free
Amount of items: 3
Items: 
Size: 393494 Color: 1
Size: 348070 Color: 1
Size: 258427 Color: 0

Bin 588: 11 of cap free
Amount of items: 3
Items: 
Size: 371115 Color: 0
Size: 358476 Color: 1
Size: 270399 Color: 0

Bin 589: 12 of cap free
Amount of items: 3
Items: 
Size: 490325 Color: 0
Size: 257181 Color: 1
Size: 252483 Color: 1

Bin 590: 12 of cap free
Amount of items: 3
Items: 
Size: 397140 Color: 1
Size: 314853 Color: 0
Size: 287996 Color: 0

Bin 591: 12 of cap free
Amount of items: 3
Items: 
Size: 358775 Color: 1
Size: 348521 Color: 0
Size: 292693 Color: 1

Bin 592: 13 of cap free
Amount of items: 3
Items: 
Size: 459778 Color: 0
Size: 277043 Color: 0
Size: 263167 Color: 1

Bin 593: 15 of cap free
Amount of items: 3
Items: 
Size: 360543 Color: 1
Size: 354003 Color: 0
Size: 285440 Color: 1

Bin 594: 16 of cap free
Amount of items: 3
Items: 
Size: 363311 Color: 1
Size: 353041 Color: 0
Size: 283633 Color: 1

Bin 595: 16 of cap free
Amount of items: 3
Items: 
Size: 359385 Color: 1
Size: 356249 Color: 0
Size: 284351 Color: 0

Bin 596: 17 of cap free
Amount of items: 3
Items: 
Size: 365348 Color: 1
Size: 351512 Color: 0
Size: 283124 Color: 0

Bin 597: 18 of cap free
Amount of items: 3
Items: 
Size: 374066 Color: 0
Size: 335867 Color: 1
Size: 290050 Color: 0

Bin 598: 18 of cap free
Amount of items: 3
Items: 
Size: 387390 Color: 1
Size: 354810 Color: 0
Size: 257783 Color: 0

Bin 599: 19 of cap free
Amount of items: 3
Items: 
Size: 396793 Color: 1
Size: 350762 Color: 1
Size: 252427 Color: 0

Bin 600: 20 of cap free
Amount of items: 3
Items: 
Size: 358986 Color: 0
Size: 354536 Color: 1
Size: 286459 Color: 0

Bin 601: 21 of cap free
Amount of items: 3
Items: 
Size: 366332 Color: 1
Size: 347251 Color: 1
Size: 286397 Color: 0

Bin 602: 21 of cap free
Amount of items: 3
Items: 
Size: 346409 Color: 1
Size: 330403 Color: 0
Size: 323168 Color: 0

Bin 603: 22 of cap free
Amount of items: 3
Items: 
Size: 355913 Color: 1
Size: 348336 Color: 0
Size: 295730 Color: 1

Bin 604: 22 of cap free
Amount of items: 3
Items: 
Size: 378520 Color: 0
Size: 339882 Color: 1
Size: 281577 Color: 0

Bin 605: 23 of cap free
Amount of items: 3
Items: 
Size: 488274 Color: 1
Size: 258622 Color: 0
Size: 253082 Color: 0

Bin 606: 24 of cap free
Amount of items: 3
Items: 
Size: 368714 Color: 0
Size: 357758 Color: 1
Size: 273505 Color: 0

Bin 607: 25 of cap free
Amount of items: 3
Items: 
Size: 359311 Color: 0
Size: 342177 Color: 1
Size: 298488 Color: 1

Bin 608: 27 of cap free
Amount of items: 3
Items: 
Size: 349132 Color: 0
Size: 334945 Color: 0
Size: 315897 Color: 1

Bin 609: 29 of cap free
Amount of items: 3
Items: 
Size: 359199 Color: 1
Size: 321875 Color: 0
Size: 318898 Color: 1

Bin 610: 30 of cap free
Amount of items: 3
Items: 
Size: 362530 Color: 0
Size: 322216 Color: 0
Size: 315225 Color: 1

Bin 611: 31 of cap free
Amount of items: 3
Items: 
Size: 370536 Color: 0
Size: 342861 Color: 0
Size: 286573 Color: 1

Bin 612: 33 of cap free
Amount of items: 3
Items: 
Size: 355446 Color: 0
Size: 345588 Color: 0
Size: 298934 Color: 1

Bin 613: 36 of cap free
Amount of items: 3
Items: 
Size: 364560 Color: 0
Size: 353690 Color: 0
Size: 281715 Color: 1

Bin 614: 37 of cap free
Amount of items: 3
Items: 
Size: 496933 Color: 0
Size: 251767 Color: 1
Size: 251264 Color: 0

Bin 615: 40 of cap free
Amount of items: 3
Items: 
Size: 472474 Color: 1
Size: 274495 Color: 0
Size: 252992 Color: 1

Bin 616: 42 of cap free
Amount of items: 3
Items: 
Size: 373406 Color: 0
Size: 313663 Color: 1
Size: 312890 Color: 0

Bin 617: 47 of cap free
Amount of items: 3
Items: 
Size: 386323 Color: 0
Size: 340334 Color: 1
Size: 273297 Color: 0

Bin 618: 51 of cap free
Amount of items: 3
Items: 
Size: 358685 Color: 1
Size: 337531 Color: 1
Size: 303734 Color: 0

Bin 619: 51 of cap free
Amount of items: 3
Items: 
Size: 367261 Color: 1
Size: 333140 Color: 1
Size: 299549 Color: 0

Bin 620: 52 of cap free
Amount of items: 3
Items: 
Size: 491111 Color: 1
Size: 257444 Color: 0
Size: 251394 Color: 0

Bin 621: 54 of cap free
Amount of items: 3
Items: 
Size: 365847 Color: 1
Size: 351919 Color: 0
Size: 282181 Color: 1

Bin 622: 57 of cap free
Amount of items: 3
Items: 
Size: 360941 Color: 0
Size: 341851 Color: 0
Size: 297152 Color: 1

Bin 623: 58 of cap free
Amount of items: 3
Items: 
Size: 369318 Color: 0
Size: 317594 Color: 0
Size: 313031 Color: 1

Bin 624: 60 of cap free
Amount of items: 3
Items: 
Size: 360030 Color: 1
Size: 354777 Color: 1
Size: 285134 Color: 0

Bin 625: 61 of cap free
Amount of items: 3
Items: 
Size: 496764 Color: 1
Size: 253170 Color: 0
Size: 250006 Color: 0

Bin 626: 68 of cap free
Amount of items: 3
Items: 
Size: 348742 Color: 1
Size: 326359 Color: 0
Size: 324832 Color: 1

Bin 627: 73 of cap free
Amount of items: 3
Items: 
Size: 400833 Color: 0
Size: 332905 Color: 1
Size: 266190 Color: 1

Bin 628: 75 of cap free
Amount of items: 3
Items: 
Size: 354635 Color: 0
Size: 322977 Color: 0
Size: 322314 Color: 1

Bin 629: 76 of cap free
Amount of items: 3
Items: 
Size: 355326 Color: 0
Size: 336881 Color: 1
Size: 307718 Color: 1

Bin 630: 77 of cap free
Amount of items: 3
Items: 
Size: 360023 Color: 0
Size: 333334 Color: 1
Size: 306567 Color: 0

Bin 631: 80 of cap free
Amount of items: 3
Items: 
Size: 346451 Color: 0
Size: 332166 Color: 1
Size: 321304 Color: 1

Bin 632: 86 of cap free
Amount of items: 3
Items: 
Size: 368358 Color: 0
Size: 358462 Color: 1
Size: 273095 Color: 0

Bin 633: 88 of cap free
Amount of items: 3
Items: 
Size: 358270 Color: 1
Size: 325427 Color: 1
Size: 316216 Color: 0

Bin 634: 113 of cap free
Amount of items: 3
Items: 
Size: 335508 Color: 0
Size: 333454 Color: 1
Size: 330926 Color: 0

Bin 635: 119 of cap free
Amount of items: 3
Items: 
Size: 349540 Color: 0
Size: 336557 Color: 0
Size: 313785 Color: 1

Bin 636: 119 of cap free
Amount of items: 3
Items: 
Size: 360266 Color: 1
Size: 345571 Color: 0
Size: 294045 Color: 0

Bin 637: 127 of cap free
Amount of items: 3
Items: 
Size: 401594 Color: 0
Size: 315528 Color: 1
Size: 282752 Color: 0

Bin 638: 147 of cap free
Amount of items: 3
Items: 
Size: 464841 Color: 1
Size: 267686 Color: 0
Size: 267327 Color: 0

Bin 639: 147 of cap free
Amount of items: 3
Items: 
Size: 366854 Color: 0
Size: 324256 Color: 1
Size: 308744 Color: 0

Bin 640: 151 of cap free
Amount of items: 3
Items: 
Size: 358259 Color: 1
Size: 350237 Color: 0
Size: 291354 Color: 1

Bin 641: 165 of cap free
Amount of items: 3
Items: 
Size: 351929 Color: 1
Size: 340937 Color: 0
Size: 306970 Color: 1

Bin 642: 197 of cap free
Amount of items: 3
Items: 
Size: 389037 Color: 1
Size: 319433 Color: 0
Size: 291334 Color: 1

Bin 643: 207 of cap free
Amount of items: 3
Items: 
Size: 371176 Color: 1
Size: 345555 Color: 1
Size: 283063 Color: 0

Bin 644: 212 of cap free
Amount of items: 3
Items: 
Size: 335777 Color: 1
Size: 335091 Color: 0
Size: 328921 Color: 0

Bin 645: 214 of cap free
Amount of items: 3
Items: 
Size: 368141 Color: 0
Size: 316388 Color: 0
Size: 315258 Color: 1

Bin 646: 219 of cap free
Amount of items: 3
Items: 
Size: 498102 Color: 0
Size: 251394 Color: 0
Size: 250286 Color: 1

Bin 647: 221 of cap free
Amount of items: 3
Items: 
Size: 372330 Color: 1
Size: 337765 Color: 1
Size: 289685 Color: 0

Bin 648: 249 of cap free
Amount of items: 3
Items: 
Size: 351190 Color: 0
Size: 350935 Color: 1
Size: 297627 Color: 1

Bin 649: 262 of cap free
Amount of items: 3
Items: 
Size: 363578 Color: 0
Size: 339918 Color: 1
Size: 296243 Color: 0

Bin 650: 275 of cap free
Amount of items: 3
Items: 
Size: 402242 Color: 1
Size: 326901 Color: 0
Size: 270583 Color: 0

Bin 651: 324 of cap free
Amount of items: 3
Items: 
Size: 390594 Color: 0
Size: 338927 Color: 1
Size: 270156 Color: 0

Bin 652: 345 of cap free
Amount of items: 3
Items: 
Size: 374019 Color: 1
Size: 315039 Color: 0
Size: 310598 Color: 1

Bin 653: 347 of cap free
Amount of items: 3
Items: 
Size: 352135 Color: 0
Size: 334317 Color: 1
Size: 313202 Color: 1

Bin 654: 369 of cap free
Amount of items: 2
Items: 
Size: 499816 Color: 1
Size: 499816 Color: 0

Bin 655: 410 of cap free
Amount of items: 3
Items: 
Size: 439438 Color: 1
Size: 287532 Color: 0
Size: 272621 Color: 0

Bin 656: 462 of cap free
Amount of items: 3
Items: 
Size: 498072 Color: 1
Size: 250919 Color: 1
Size: 250548 Color: 0

Bin 657: 518 of cap free
Amount of items: 3
Items: 
Size: 379197 Color: 1
Size: 338232 Color: 0
Size: 282054 Color: 1

Bin 658: 669 of cap free
Amount of items: 3
Items: 
Size: 360451 Color: 0
Size: 347620 Color: 1
Size: 291261 Color: 1

Bin 659: 1313 of cap free
Amount of items: 3
Items: 
Size: 360189 Color: 0
Size: 345870 Color: 1
Size: 292629 Color: 0

Bin 660: 1701 of cap free
Amount of items: 3
Items: 
Size: 456707 Color: 0
Size: 278207 Color: 0
Size: 263386 Color: 1

Bin 661: 1706 of cap free
Amount of items: 2
Items: 
Size: 499702 Color: 1
Size: 498593 Color: 0

Bin 662: 2476 of cap free
Amount of items: 3
Items: 
Size: 429355 Color: 1
Size: 295637 Color: 0
Size: 272533 Color: 0

Bin 663: 3941 of cap free
Amount of items: 3
Items: 
Size: 374958 Color: 0
Size: 336363 Color: 1
Size: 284739 Color: 0

Bin 664: 69189 of cap free
Amount of items: 3
Items: 
Size: 344923 Color: 1
Size: 304288 Color: 0
Size: 281601 Color: 1

Bin 665: 210219 of cap free
Amount of items: 3
Items: 
Size: 265589 Color: 0
Size: 262482 Color: 1
Size: 261711 Color: 0

Bin 666: 218401 of cap free
Amount of items: 3
Items: 
Size: 261276 Color: 1
Size: 260640 Color: 0
Size: 259684 Color: 0

Bin 667: 229202 of cap free
Amount of items: 3
Items: 
Size: 259128 Color: 1
Size: 258604 Color: 0
Size: 253067 Color: 1

Bin 668: 252902 of cap free
Amount of items: 2
Items: 
Size: 494769 Color: 0
Size: 252330 Color: 1

Total size: 667000667
Total free space: 1000001

