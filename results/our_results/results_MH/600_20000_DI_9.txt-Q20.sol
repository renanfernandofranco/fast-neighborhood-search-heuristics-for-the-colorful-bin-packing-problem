Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9904 Color: 7
Size: 6968 Color: 15
Size: 2776 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12408 Color: 19
Size: 6640 Color: 17
Size: 600 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 14262 Color: 0
Size: 4490 Color: 0
Size: 896 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 14495 Color: 18
Size: 3435 Color: 17
Size: 1718 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 14519 Color: 4
Size: 4275 Color: 2
Size: 854 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 14872 Color: 5
Size: 4496 Color: 0
Size: 280 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 15119 Color: 9
Size: 3775 Color: 1
Size: 754 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 15264 Color: 15
Size: 3928 Color: 6
Size: 456 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15364 Color: 6
Size: 3924 Color: 14
Size: 360 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15517 Color: 18
Size: 3091 Color: 15
Size: 1040 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 11
Size: 3072 Color: 15
Size: 1056 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15559 Color: 18
Size: 3409 Color: 12
Size: 680 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15728 Color: 18
Size: 2618 Color: 15
Size: 1302 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15915 Color: 13
Size: 3085 Color: 11
Size: 648 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15990 Color: 13
Size: 3018 Color: 4
Size: 640 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16072 Color: 14
Size: 2984 Color: 10
Size: 592 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 16224 Color: 9
Size: 2960 Color: 18
Size: 464 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16236 Color: 3
Size: 1780 Color: 4
Size: 1632 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16274 Color: 0
Size: 2922 Color: 14
Size: 452 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 14
Size: 2528 Color: 2
Size: 688 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16488 Color: 16
Size: 2648 Color: 9
Size: 512 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16504 Color: 15
Size: 2516 Color: 7
Size: 628 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16533 Color: 4
Size: 2597 Color: 9
Size: 518 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16592 Color: 13
Size: 2204 Color: 18
Size: 852 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16650 Color: 3
Size: 2174 Color: 1
Size: 824 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16785 Color: 5
Size: 2387 Color: 15
Size: 476 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 10
Size: 2296 Color: 15
Size: 576 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16784 Color: 18
Size: 2176 Color: 1
Size: 688 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16840 Color: 5
Size: 1768 Color: 11
Size: 1040 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16800 Color: 16
Size: 2080 Color: 4
Size: 768 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16884 Color: 8
Size: 1676 Color: 17
Size: 1088 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16904 Color: 16
Size: 1736 Color: 5
Size: 1008 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 17012 Color: 18
Size: 1744 Color: 13
Size: 892 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 17184 Color: 18
Size: 1408 Color: 14
Size: 1056 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 17196 Color: 18
Size: 2068 Color: 6
Size: 384 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 17226 Color: 19
Size: 2278 Color: 19
Size: 144 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 17356 Color: 5
Size: 1810 Color: 19
Size: 482 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17274 Color: 17
Size: 1702 Color: 4
Size: 672 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17326 Color: 1
Size: 1298 Color: 7
Size: 1024 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17462 Color: 1
Size: 1418 Color: 11
Size: 768 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17464 Color: 15
Size: 1414 Color: 18
Size: 770 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17488 Color: 5
Size: 1768 Color: 3
Size: 392 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17536 Color: 9
Size: 1824 Color: 12
Size: 288 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17544 Color: 0
Size: 1272 Color: 16
Size: 832 Color: 8

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17576 Color: 5
Size: 1808 Color: 17
Size: 264 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 15
Size: 1536 Color: 19
Size: 496 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17622 Color: 10
Size: 1404 Color: 6
Size: 622 Color: 13

Bin 48: 1 of cap free
Amount of items: 7
Items: 
Size: 9832 Color: 4
Size: 2571 Color: 2
Size: 2000 Color: 16
Size: 1884 Color: 6
Size: 1856 Color: 12
Size: 896 Color: 9
Size: 608 Color: 4

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 11129 Color: 7
Size: 8182 Color: 1
Size: 336 Color: 4

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 11996 Color: 3
Size: 7459 Color: 6
Size: 192 Color: 11

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 11
Size: 3415 Color: 19
Size: 2936 Color: 8

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 14527 Color: 2
Size: 4504 Color: 16
Size: 616 Color: 5

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 15527 Color: 2
Size: 3416 Color: 1
Size: 704 Color: 7

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 15551 Color: 18
Size: 4096 Color: 14

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 16231 Color: 7
Size: 3048 Color: 12
Size: 368 Color: 5

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 16777 Color: 18
Size: 1870 Color: 12
Size: 1000 Color: 3

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 16798 Color: 15
Size: 2849 Color: 16

Bin 58: 2 of cap free
Amount of items: 32
Items: 
Size: 896 Color: 1
Size: 864 Color: 18
Size: 864 Color: 3
Size: 858 Color: 8
Size: 816 Color: 17
Size: 784 Color: 19
Size: 784 Color: 8
Size: 784 Color: 5
Size: 768 Color: 6
Size: 756 Color: 18
Size: 732 Color: 11
Size: 680 Color: 1
Size: 640 Color: 19
Size: 580 Color: 12
Size: 576 Color: 17
Size: 576 Color: 12
Size: 568 Color: 3
Size: 560 Color: 16
Size: 560 Color: 14
Size: 534 Color: 14
Size: 530 Color: 14
Size: 528 Color: 9
Size: 512 Color: 12
Size: 512 Color: 10
Size: 472 Color: 7
Size: 472 Color: 6
Size: 448 Color: 7
Size: 448 Color: 2
Size: 416 Color: 4
Size: 376 Color: 4
Size: 376 Color: 4
Size: 376 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 11156 Color: 7
Size: 7808 Color: 12
Size: 682 Color: 14

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 11218 Color: 15
Size: 7052 Color: 11
Size: 1376 Color: 1

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 11596 Color: 19
Size: 8050 Color: 8

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 11680 Color: 15
Size: 6366 Color: 10
Size: 1600 Color: 10

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 11865 Color: 14
Size: 7073 Color: 18
Size: 708 Color: 19

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 11
Size: 7122 Color: 4
Size: 192 Color: 16

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 15
Size: 5818 Color: 1
Size: 444 Color: 0

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 14054 Color: 14
Size: 5176 Color: 6
Size: 416 Color: 16

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 11
Size: 3790 Color: 0
Size: 992 Color: 0

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 15102 Color: 6
Size: 2912 Color: 10
Size: 1632 Color: 5

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 15478 Color: 17
Size: 3992 Color: 3
Size: 176 Color: 4

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 16478 Color: 16
Size: 1832 Color: 8
Size: 1336 Color: 7

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 17042 Color: 18
Size: 1982 Color: 5
Size: 622 Color: 7

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 17144 Color: 18
Size: 2502 Color: 15

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 17372 Color: 9
Size: 1938 Color: 5
Size: 336 Color: 2

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 17478 Color: 16
Size: 2088 Color: 15
Size: 80 Color: 1

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 18
Size: 8168 Color: 1
Size: 316 Color: 10

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 6
Size: 8188 Color: 5
Size: 352 Color: 7

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 14
Size: 7084 Color: 2
Size: 704 Color: 1

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 11952 Color: 5
Size: 7101 Color: 0
Size: 592 Color: 10

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 13641 Color: 12
Size: 5428 Color: 10
Size: 576 Color: 19

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 14285 Color: 18
Size: 4524 Color: 2
Size: 836 Color: 9

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 14936 Color: 1
Size: 3855 Color: 12
Size: 854 Color: 2

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 16565 Color: 5
Size: 3080 Color: 18

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 12010 Color: 5
Size: 7016 Color: 2
Size: 618 Color: 0

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 15382 Color: 13
Size: 2400 Color: 15
Size: 1862 Color: 1

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 16012 Color: 17
Size: 2576 Color: 6
Size: 1056 Color: 11

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 16096 Color: 11
Size: 3548 Color: 0

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 17256 Color: 12
Size: 2388 Color: 17

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 17404 Color: 6
Size: 2240 Color: 17

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 17418 Color: 15
Size: 2226 Color: 3

Bin 90: 5 of cap free
Amount of items: 3
Items: 
Size: 13956 Color: 2
Size: 4471 Color: 15
Size: 1216 Color: 19

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 15131 Color: 7
Size: 4512 Color: 18

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 15878 Color: 1
Size: 3765 Color: 6

Bin 93: 5 of cap free
Amount of items: 3
Items: 
Size: 15931 Color: 15
Size: 2976 Color: 12
Size: 736 Color: 8

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 15947 Color: 11
Size: 3696 Color: 14

Bin 95: 6 of cap free
Amount of items: 9
Items: 
Size: 9828 Color: 5
Size: 1632 Color: 11
Size: 1632 Color: 3
Size: 1414 Color: 6
Size: 1408 Color: 1
Size: 1344 Color: 7
Size: 1312 Color: 6
Size: 592 Color: 0
Size: 480 Color: 16

Bin 96: 6 of cap free
Amount of items: 3
Items: 
Size: 11169 Color: 10
Size: 7081 Color: 14
Size: 1392 Color: 15

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 15560 Color: 15
Size: 3464 Color: 0
Size: 618 Color: 12

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 17264 Color: 2
Size: 2378 Color: 4

Bin 99: 7 of cap free
Amount of items: 3
Items: 
Size: 13609 Color: 3
Size: 5424 Color: 11
Size: 608 Color: 6

Bin 100: 7 of cap free
Amount of items: 3
Items: 
Size: 14288 Color: 5
Size: 5033 Color: 1
Size: 320 Color: 8

Bin 101: 7 of cap free
Amount of items: 3
Items: 
Size: 15047 Color: 7
Size: 4186 Color: 12
Size: 408 Color: 17

Bin 102: 7 of cap free
Amount of items: 3
Items: 
Size: 15939 Color: 11
Size: 2282 Color: 16
Size: 1420 Color: 1

Bin 103: 8 of cap free
Amount of items: 10
Items: 
Size: 9826 Color: 19
Size: 1296 Color: 11
Size: 1280 Color: 2
Size: 1200 Color: 13
Size: 1184 Color: 16
Size: 1152 Color: 5
Size: 1024 Color: 18
Size: 1024 Color: 9
Size: 968 Color: 18
Size: 686 Color: 16

Bin 104: 8 of cap free
Amount of items: 5
Items: 
Size: 9836 Color: 14
Size: 3220 Color: 7
Size: 2919 Color: 9
Size: 2393 Color: 5
Size: 1272 Color: 11

Bin 105: 8 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 17
Size: 5992 Color: 6
Size: 276 Color: 19

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 15960 Color: 5
Size: 3680 Color: 13

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 16008 Color: 10
Size: 3632 Color: 7

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 16252 Color: 14
Size: 3260 Color: 3
Size: 128 Color: 13

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 16433 Color: 19
Size: 2969 Color: 1
Size: 238 Color: 12

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 17008 Color: 6
Size: 2632 Color: 10

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 17596 Color: 7
Size: 2044 Color: 19

Bin 112: 9 of cap free
Amount of items: 3
Items: 
Size: 11102 Color: 14
Size: 8185 Color: 0
Size: 352 Color: 1

Bin 113: 9 of cap free
Amount of items: 3
Items: 
Size: 13303 Color: 7
Size: 5984 Color: 19
Size: 352 Color: 12

Bin 114: 10 of cap free
Amount of items: 4
Items: 
Size: 9888 Color: 12
Size: 7122 Color: 13
Size: 2308 Color: 6
Size: 320 Color: 1

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 11106 Color: 12
Size: 8180 Color: 9
Size: 352 Color: 16

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 16080 Color: 5
Size: 3558 Color: 15

Bin 117: 12 of cap free
Amount of items: 3
Items: 
Size: 13088 Color: 13
Size: 5548 Color: 11
Size: 1000 Color: 2

Bin 118: 12 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 4
Size: 5408 Color: 9

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 15496 Color: 8
Size: 4140 Color: 16

Bin 120: 13 of cap free
Amount of items: 2
Items: 
Size: 16978 Color: 2
Size: 2657 Color: 14

Bin 121: 14 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 9
Size: 7026 Color: 1
Size: 320 Color: 0

Bin 122: 14 of cap free
Amount of items: 2
Items: 
Size: 14738 Color: 14
Size: 4896 Color: 0

Bin 123: 14 of cap free
Amount of items: 2
Items: 
Size: 16146 Color: 17
Size: 3488 Color: 5

Bin 124: 15 of cap free
Amount of items: 5
Items: 
Size: 9840 Color: 2
Size: 4269 Color: 15
Size: 3410 Color: 10
Size: 1422 Color: 4
Size: 692 Color: 12

Bin 125: 15 of cap free
Amount of items: 3
Items: 
Size: 11137 Color: 4
Size: 8176 Color: 6
Size: 320 Color: 11

Bin 126: 15 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 9
Size: 5007 Color: 5

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 19
Size: 6688 Color: 10

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 13216 Color: 17
Size: 6416 Color: 4

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 16510 Color: 19
Size: 3122 Color: 11

Bin 130: 16 of cap free
Amount of items: 4
Items: 
Size: 17652 Color: 4
Size: 1900 Color: 2
Size: 48 Color: 5
Size: 32 Color: 19

Bin 131: 18 of cap free
Amount of items: 4
Items: 
Size: 9848 Color: 15
Size: 4797 Color: 19
Size: 4281 Color: 6
Size: 704 Color: 18

Bin 132: 18 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 8
Size: 7093 Color: 6
Size: 704 Color: 19

Bin 133: 18 of cap free
Amount of items: 2
Items: 
Size: 15536 Color: 13
Size: 4094 Color: 15

Bin 134: 19 of cap free
Amount of items: 12
Items: 
Size: 9825 Color: 19
Size: 1160 Color: 0
Size: 1136 Color: 13
Size: 1068 Color: 12
Size: 1006 Color: 1
Size: 968 Color: 10
Size: 960 Color: 0
Size: 932 Color: 3
Size: 896 Color: 8
Size: 752 Color: 9
Size: 478 Color: 0
Size: 448 Color: 4

Bin 135: 20 of cap free
Amount of items: 3
Items: 
Size: 13792 Color: 12
Size: 4816 Color: 11
Size: 1020 Color: 1

Bin 136: 20 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 15
Size: 4004 Color: 1
Size: 1424 Color: 13

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 16636 Color: 5
Size: 2992 Color: 0

Bin 138: 22 of cap free
Amount of items: 2
Items: 
Size: 17606 Color: 17
Size: 2020 Color: 4

Bin 139: 22 of cap free
Amount of items: 2
Items: 
Size: 17680 Color: 9
Size: 1946 Color: 4

Bin 140: 23 of cap free
Amount of items: 3
Items: 
Size: 17140 Color: 7
Size: 2421 Color: 4
Size: 64 Color: 2

Bin 141: 24 of cap free
Amount of items: 2
Items: 
Size: 16788 Color: 12
Size: 2836 Color: 6

Bin 142: 25 of cap free
Amount of items: 3
Items: 
Size: 12472 Color: 18
Size: 6487 Color: 8
Size: 664 Color: 2

Bin 143: 28 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 17
Size: 5616 Color: 16
Size: 208 Color: 3

Bin 144: 28 of cap free
Amount of items: 2
Items: 
Size: 15740 Color: 7
Size: 3880 Color: 10

Bin 145: 28 of cap free
Amount of items: 3
Items: 
Size: 17672 Color: 7
Size: 1916 Color: 18
Size: 32 Color: 19

Bin 146: 30 of cap free
Amount of items: 3
Items: 
Size: 13168 Color: 16
Size: 5346 Color: 1
Size: 1104 Color: 12

Bin 147: 30 of cap free
Amount of items: 2
Items: 
Size: 13578 Color: 2
Size: 6040 Color: 16

Bin 148: 32 of cap free
Amount of items: 3
Items: 
Size: 11440 Color: 12
Size: 6896 Color: 15
Size: 1280 Color: 1

Bin 149: 32 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 15
Size: 5200 Color: 4

Bin 150: 32 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 10
Size: 2416 Color: 8
Size: 64 Color: 4

Bin 151: 34 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 19
Size: 6380 Color: 12

Bin 152: 34 of cap free
Amount of items: 2
Items: 
Size: 16136 Color: 12
Size: 3478 Color: 7

Bin 153: 36 of cap free
Amount of items: 2
Items: 
Size: 16928 Color: 7
Size: 2684 Color: 1

Bin 154: 36 of cap free
Amount of items: 2
Items: 
Size: 17388 Color: 19
Size: 2224 Color: 8

Bin 155: 36 of cap free
Amount of items: 2
Items: 
Size: 17516 Color: 0
Size: 2096 Color: 8

Bin 156: 40 of cap free
Amount of items: 3
Items: 
Size: 11696 Color: 3
Size: 7392 Color: 13
Size: 520 Color: 7

Bin 157: 42 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 15
Size: 8186 Color: 7
Size: 256 Color: 6

Bin 158: 45 of cap free
Amount of items: 2
Items: 
Size: 16461 Color: 5
Size: 3142 Color: 14

Bin 159: 46 of cap free
Amount of items: 2
Items: 
Size: 14940 Color: 8
Size: 4662 Color: 4

Bin 160: 48 of cap free
Amount of items: 2
Items: 
Size: 12384 Color: 14
Size: 7216 Color: 16

Bin 161: 48 of cap free
Amount of items: 2
Items: 
Size: 14304 Color: 4
Size: 5296 Color: 10

Bin 162: 50 of cap free
Amount of items: 2
Items: 
Size: 17590 Color: 10
Size: 2008 Color: 19

Bin 163: 52 of cap free
Amount of items: 2
Items: 
Size: 17504 Color: 3
Size: 2092 Color: 6

Bin 164: 53 of cap free
Amount of items: 2
Items: 
Size: 16914 Color: 10
Size: 2681 Color: 14

Bin 165: 55 of cap free
Amount of items: 3
Items: 
Size: 11153 Color: 5
Size: 8184 Color: 2
Size: 256 Color: 10

Bin 166: 60 of cap free
Amount of items: 2
Items: 
Size: 15396 Color: 4
Size: 4192 Color: 16

Bin 167: 64 of cap free
Amount of items: 2
Items: 
Size: 15216 Color: 7
Size: 4368 Color: 3

Bin 168: 71 of cap free
Amount of items: 2
Items: 
Size: 15907 Color: 6
Size: 3670 Color: 9

Bin 169: 73 of cap free
Amount of items: 2
Items: 
Size: 15023 Color: 18
Size: 4552 Color: 14

Bin 170: 74 of cap free
Amount of items: 2
Items: 
Size: 15558 Color: 11
Size: 4016 Color: 13

Bin 171: 76 of cap free
Amount of items: 2
Items: 
Size: 14688 Color: 7
Size: 4884 Color: 12

Bin 172: 76 of cap free
Amount of items: 2
Items: 
Size: 16453 Color: 0
Size: 3119 Color: 18

Bin 173: 84 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 2
Size: 5224 Color: 15
Size: 3036 Color: 4

Bin 174: 88 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 17
Size: 6112 Color: 16

Bin 175: 89 of cap free
Amount of items: 2
Items: 
Size: 16745 Color: 15
Size: 2814 Color: 7

Bin 176: 91 of cap free
Amount of items: 3
Items: 
Size: 11408 Color: 12
Size: 6513 Color: 17
Size: 1636 Color: 2

Bin 177: 91 of cap free
Amount of items: 2
Items: 
Size: 13568 Color: 6
Size: 5989 Color: 10

Bin 178: 96 of cap free
Amount of items: 2
Items: 
Size: 16441 Color: 11
Size: 3111 Color: 5

Bin 179: 99 of cap free
Amount of items: 2
Items: 
Size: 12829 Color: 16
Size: 6720 Color: 12

Bin 180: 100 of cap free
Amount of items: 2
Items: 
Size: 14259 Color: 7
Size: 5289 Color: 13

Bin 181: 101 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 17
Size: 6493 Color: 13
Size: 384 Color: 1

Bin 182: 104 of cap free
Amount of items: 2
Items: 
Size: 15000 Color: 6
Size: 4544 Color: 13

Bin 183: 106 of cap free
Amount of items: 2
Items: 
Size: 14480 Color: 5
Size: 5062 Color: 15

Bin 184: 107 of cap free
Amount of items: 2
Items: 
Size: 15246 Color: 1
Size: 4295 Color: 16

Bin 185: 110 of cap free
Amount of items: 2
Items: 
Size: 16258 Color: 8
Size: 3280 Color: 1

Bin 186: 112 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 0
Size: 4852 Color: 12

Bin 187: 113 of cap free
Amount of items: 2
Items: 
Size: 16436 Color: 10
Size: 3099 Color: 8

Bin 188: 124 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 4
Size: 6100 Color: 16

Bin 189: 137 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 6
Size: 5683 Color: 0

Bin 190: 159 of cap free
Amount of items: 2
Items: 
Size: 12805 Color: 13
Size: 6684 Color: 5

Bin 191: 159 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 8
Size: 3443 Color: 5
Size: 3050 Color: 8

Bin 192: 164 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 12
Size: 5236 Color: 8

Bin 193: 176 of cap free
Amount of items: 3
Items: 
Size: 10848 Color: 7
Size: 8144 Color: 5
Size: 480 Color: 2

Bin 194: 184 of cap free
Amount of items: 2
Items: 
Size: 11240 Color: 10
Size: 8224 Color: 9

Bin 195: 200 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 5
Size: 7820 Color: 19

Bin 196: 212 of cap free
Amount of items: 7
Items: 
Size: 9834 Color: 2
Size: 1648 Color: 13
Size: 1636 Color: 19
Size: 1636 Color: 16
Size: 1632 Color: 17
Size: 1632 Color: 5
Size: 1418 Color: 8

Bin 197: 227 of cap free
Amount of items: 2
Items: 
Size: 11234 Color: 3
Size: 8187 Color: 5

Bin 198: 292 of cap free
Amount of items: 3
Items: 
Size: 9827 Color: 9
Size: 7121 Color: 18
Size: 2408 Color: 14

Bin 199: 13992 of cap free
Amount of items: 14
Items: 
Size: 512 Color: 3
Size: 496 Color: 1
Size: 432 Color: 18
Size: 432 Color: 9
Size: 416 Color: 9
Size: 416 Color: 5
Size: 384 Color: 13
Size: 384 Color: 10
Size: 384 Color: 6
Size: 384 Color: 0
Size: 372 Color: 7
Size: 364 Color: 10
Size: 340 Color: 13
Size: 340 Color: 2

Total size: 3890304
Total free space: 19648

