Capicity Bin: 16400
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9176 Color: 4
Size: 6808 Color: 2
Size: 416 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9372 Color: 4
Size: 6452 Color: 0
Size: 576 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 3
Size: 6024 Color: 1
Size: 508 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9931 Color: 4
Size: 5271 Color: 1
Size: 1198 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10155 Color: 1
Size: 5205 Color: 0
Size: 1040 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10150 Color: 3
Size: 5210 Color: 1
Size: 1040 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12014 Color: 4
Size: 4008 Color: 1
Size: 378 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 1
Size: 3688 Color: 3
Size: 416 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12329 Color: 4
Size: 3393 Color: 2
Size: 678 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 3
Size: 3364 Color: 1
Size: 256 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12860 Color: 3
Size: 2956 Color: 1
Size: 584 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13038 Color: 3
Size: 2974 Color: 4
Size: 388 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 2
Size: 2918 Color: 1
Size: 418 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 0
Size: 2952 Color: 1
Size: 364 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13266 Color: 2
Size: 2642 Color: 3
Size: 492 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 1
Size: 2210 Color: 3
Size: 794 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13518 Color: 2
Size: 1970 Color: 1
Size: 912 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 4
Size: 1764 Color: 1
Size: 1072 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13579 Color: 4
Size: 2225 Color: 1
Size: 596 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13605 Color: 1
Size: 2331 Color: 2
Size: 464 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13750 Color: 1
Size: 2342 Color: 4
Size: 308 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13787 Color: 2
Size: 2179 Color: 1
Size: 434 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13819 Color: 3
Size: 2309 Color: 1
Size: 272 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 1
Size: 1518 Color: 2
Size: 1050 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13858 Color: 0
Size: 2122 Color: 4
Size: 420 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13882 Color: 2
Size: 1502 Color: 1
Size: 1016 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13884 Color: 3
Size: 2302 Color: 1
Size: 214 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 2
Size: 1710 Color: 1
Size: 752 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 2
Size: 1500 Color: 0
Size: 960 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 1
Size: 2012 Color: 2
Size: 380 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 2
Size: 2100 Color: 1
Size: 312 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14038 Color: 3
Size: 1722 Color: 1
Size: 640 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14067 Color: 3
Size: 1861 Color: 1
Size: 472 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 0
Size: 1360 Color: 1
Size: 920 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 3
Size: 1912 Color: 1
Size: 340 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14156 Color: 0
Size: 1876 Color: 4
Size: 368 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 2
Size: 1838 Color: 1
Size: 364 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 1
Size: 1360 Color: 3
Size: 824 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 1
Size: 1072 Color: 3
Size: 1032 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14294 Color: 4
Size: 1388 Color: 1
Size: 718 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 2
Size: 1558 Color: 1
Size: 518 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14334 Color: 0
Size: 1202 Color: 4
Size: 864 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14350 Color: 4
Size: 1742 Color: 1
Size: 308 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 0
Size: 1054 Color: 1
Size: 910 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 4
Size: 1498 Color: 4
Size: 462 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 4
Size: 1364 Color: 2
Size: 544 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 3
Size: 1458 Color: 1
Size: 408 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 1
Size: 1528 Color: 4
Size: 288 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14540 Color: 2
Size: 1460 Color: 4
Size: 400 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14542 Color: 3
Size: 1542 Color: 1
Size: 316 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 1
Size: 1510 Color: 2
Size: 300 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14598 Color: 1
Size: 1054 Color: 2
Size: 748 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14604 Color: 0
Size: 1366 Color: 1
Size: 430 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 1456 Color: 2
Size: 344 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 3
Size: 1380 Color: 1
Size: 368 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 4
Size: 1432 Color: 1
Size: 272 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14740 Color: 1
Size: 1200 Color: 0
Size: 460 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14702 Color: 0
Size: 1058 Color: 2
Size: 640 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 2
Size: 1364 Color: 1
Size: 288 Color: 4

Bin 60: 1 of cap free
Amount of items: 5
Items: 
Size: 8205 Color: 2
Size: 3658 Color: 3
Size: 3002 Color: 3
Size: 950 Color: 3
Size: 584 Color: 1

Bin 61: 1 of cap free
Amount of items: 4
Items: 
Size: 8214 Color: 0
Size: 5275 Color: 3
Size: 2614 Color: 1
Size: 296 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10071 Color: 1
Size: 5958 Color: 3
Size: 370 Color: 2

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 9964 Color: 2
Size: 5991 Color: 2
Size: 444 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10616 Color: 4
Size: 5391 Color: 0
Size: 392 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10807 Color: 0
Size: 5166 Color: 0
Size: 426 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 10963 Color: 1
Size: 5084 Color: 1
Size: 352 Color: 0

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11990 Color: 3
Size: 3009 Color: 4
Size: 1400 Color: 1

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12469 Color: 3
Size: 3764 Color: 1
Size: 166 Color: 2

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 12497 Color: 3
Size: 3902 Color: 4

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12891 Color: 3
Size: 3208 Color: 1
Size: 300 Color: 0

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 3
Size: 3167 Color: 1
Size: 280 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13070 Color: 2
Size: 2865 Color: 4
Size: 464 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 4
Size: 1732 Color: 1
Size: 1558 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 0
Size: 2131 Color: 1
Size: 1040 Color: 4

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 1
Size: 2039 Color: 2
Size: 1072 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13467 Color: 1
Size: 2644 Color: 3
Size: 288 Color: 4

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13675 Color: 0
Size: 2052 Color: 0
Size: 672 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13731 Color: 0
Size: 2316 Color: 0
Size: 352 Color: 2

Bin 79: 2 of cap free
Amount of items: 19
Items: 
Size: 1248 Color: 4
Size: 1196 Color: 2
Size: 1194 Color: 3
Size: 1192 Color: 3
Size: 1176 Color: 3
Size: 1078 Color: 4
Size: 1042 Color: 2
Size: 906 Color: 3
Size: 880 Color: 2
Size: 814 Color: 3
Size: 802 Color: 3
Size: 732 Color: 0
Size: 714 Color: 0
Size: 712 Color: 1
Size: 680 Color: 0
Size: 664 Color: 0
Size: 632 Color: 1
Size: 416 Color: 1
Size: 320 Color: 3

Bin 80: 2 of cap free
Amount of items: 8
Items: 
Size: 8202 Color: 4
Size: 1596 Color: 1
Size: 1556 Color: 1
Size: 1420 Color: 4
Size: 1344 Color: 2
Size: 1176 Color: 0
Size: 784 Color: 0
Size: 320 Color: 2

Bin 81: 2 of cap free
Amount of items: 4
Items: 
Size: 8204 Color: 0
Size: 5211 Color: 4
Size: 2151 Color: 1
Size: 832 Color: 4

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 3
Size: 7736 Color: 3
Size: 440 Color: 2

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 10099 Color: 1
Size: 5995 Color: 2
Size: 304 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 10075 Color: 2
Size: 6019 Color: 3
Size: 304 Color: 1

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 10202 Color: 1
Size: 5900 Color: 4
Size: 296 Color: 3

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 10830 Color: 0
Size: 5296 Color: 2
Size: 272 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 10874 Color: 1
Size: 5124 Color: 4
Size: 400 Color: 0

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 11513 Color: 0
Size: 4421 Color: 2
Size: 464 Color: 4

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 0
Size: 4170 Color: 1
Size: 344 Color: 2

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 12113 Color: 3
Size: 4013 Color: 0
Size: 272 Color: 1

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 3
Size: 3678 Color: 2
Size: 168 Color: 1

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 1
Size: 2982 Color: 4
Size: 320 Color: 0

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 13347 Color: 4
Size: 2743 Color: 0
Size: 308 Color: 1

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 13638 Color: 2
Size: 1832 Color: 1
Size: 928 Color: 3

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 13709 Color: 3
Size: 2393 Color: 0
Size: 296 Color: 1

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 14284 Color: 4
Size: 2114 Color: 2

Bin 97: 2 of cap free
Amount of items: 2
Items: 
Size: 14514 Color: 3
Size: 1884 Color: 0

Bin 98: 3 of cap free
Amount of items: 7
Items: 
Size: 8206 Color: 1
Size: 2035 Color: 1
Size: 2008 Color: 4
Size: 1422 Color: 2
Size: 1222 Color: 0
Size: 1008 Color: 3
Size: 496 Color: 0

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 9067 Color: 4
Size: 6818 Color: 4
Size: 512 Color: 1

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 10139 Color: 4
Size: 5890 Color: 3
Size: 368 Color: 1

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 11577 Color: 0
Size: 4376 Color: 3
Size: 444 Color: 3

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 11665 Color: 3
Size: 4444 Color: 1
Size: 288 Color: 2

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 12802 Color: 0
Size: 2407 Color: 1
Size: 1188 Color: 4

Bin 104: 3 of cap free
Amount of items: 2
Items: 
Size: 14033 Color: 3
Size: 2364 Color: 4

Bin 105: 3 of cap free
Amount of items: 2
Items: 
Size: 14154 Color: 4
Size: 2243 Color: 3

Bin 106: 4 of cap free
Amount of items: 5
Items: 
Size: 8201 Color: 0
Size: 2600 Color: 3
Size: 2595 Color: 3
Size: 1640 Color: 1
Size: 1360 Color: 0

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 9254 Color: 0
Size: 6822 Color: 2
Size: 320 Color: 1

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 11601 Color: 1
Size: 4531 Color: 0
Size: 264 Color: 2

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 9179 Color: 3
Size: 7216 Color: 4

Bin 110: 5 of cap free
Amount of items: 3
Items: 
Size: 11434 Color: 1
Size: 4529 Color: 3
Size: 432 Color: 0

Bin 111: 5 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 0
Size: 3597 Color: 1
Size: 384 Color: 2

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 12690 Color: 2
Size: 2155 Color: 2
Size: 1550 Color: 1

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 13073 Color: 3
Size: 3322 Color: 4

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 13899 Color: 2
Size: 2496 Color: 4

Bin 115: 5 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 3
Size: 2085 Color: 0

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 9222 Color: 1
Size: 6836 Color: 4
Size: 336 Color: 4

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 2
Size: 5251 Color: 1
Size: 448 Color: 0

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 12276 Color: 2
Size: 3830 Color: 3
Size: 288 Color: 1

Bin 119: 6 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 3
Size: 3350 Color: 1
Size: 496 Color: 3

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 0
Size: 2054 Color: 3

Bin 121: 6 of cap free
Amount of items: 3
Items: 
Size: 14698 Color: 0
Size: 1638 Color: 4
Size: 58 Color: 4

Bin 122: 7 of cap free
Amount of items: 2
Items: 
Size: 12949 Color: 3
Size: 3444 Color: 4

Bin 123: 8 of cap free
Amount of items: 2
Items: 
Size: 13628 Color: 4
Size: 2764 Color: 0

Bin 124: 9 of cap free
Amount of items: 3
Items: 
Size: 9211 Color: 0
Size: 5982 Color: 2
Size: 1198 Color: 1

Bin 125: 9 of cap free
Amount of items: 3
Items: 
Size: 11633 Color: 1
Size: 3394 Color: 4
Size: 1364 Color: 2

Bin 126: 9 of cap free
Amount of items: 3
Items: 
Size: 11870 Color: 3
Size: 3737 Color: 2
Size: 784 Color: 1

Bin 127: 9 of cap free
Amount of items: 2
Items: 
Size: 13883 Color: 2
Size: 2508 Color: 4

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 4
Size: 4606 Color: 2
Size: 700 Color: 2

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 13513 Color: 4
Size: 2877 Color: 3

Bin 130: 11 of cap free
Amount of items: 3
Items: 
Size: 11076 Color: 2
Size: 5105 Color: 2
Size: 208 Color: 1

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 13843 Color: 0
Size: 2545 Color: 4

Bin 132: 13 of cap free
Amount of items: 3
Items: 
Size: 11608 Color: 2
Size: 4555 Color: 3
Size: 224 Color: 1

Bin 133: 14 of cap free
Amount of items: 3
Items: 
Size: 10043 Color: 2
Size: 5971 Color: 0
Size: 372 Color: 1

Bin 134: 14 of cap free
Amount of items: 3
Items: 
Size: 11371 Color: 1
Size: 3973 Color: 0
Size: 1042 Color: 4

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 13866 Color: 2
Size: 2520 Color: 3

Bin 136: 14 of cap free
Amount of items: 3
Items: 
Size: 14656 Color: 0
Size: 1618 Color: 2
Size: 112 Color: 3

Bin 137: 15 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 2
Size: 5969 Color: 0
Size: 456 Color: 3

Bin 138: 15 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 3
Size: 2493 Color: 0
Size: 140 Color: 1

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 14169 Color: 0
Size: 2216 Color: 2

Bin 140: 16 of cap free
Amount of items: 3
Items: 
Size: 10103 Color: 3
Size: 6025 Color: 0
Size: 256 Color: 4

Bin 141: 17 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 4
Size: 3663 Color: 0
Size: 744 Color: 1

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 14438 Color: 3
Size: 1945 Color: 0

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 13104 Color: 0
Size: 3277 Color: 3

Bin 144: 20 of cap free
Amount of items: 3
Items: 
Size: 9239 Color: 4
Size: 6833 Color: 2
Size: 308 Color: 1

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 14462 Color: 4
Size: 1918 Color: 2

Bin 146: 21 of cap free
Amount of items: 2
Items: 
Size: 11160 Color: 2
Size: 5219 Color: 0

Bin 147: 21 of cap free
Amount of items: 2
Items: 
Size: 14123 Color: 0
Size: 2256 Color: 4

Bin 148: 21 of cap free
Amount of items: 2
Items: 
Size: 14550 Color: 0
Size: 1829 Color: 4

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 14102 Color: 2
Size: 2276 Color: 0

Bin 150: 22 of cap free
Amount of items: 2
Items: 
Size: 14654 Color: 1
Size: 1724 Color: 0

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 14728 Color: 2
Size: 1648 Color: 4

Bin 152: 25 of cap free
Amount of items: 3
Items: 
Size: 11398 Color: 1
Size: 4073 Color: 0
Size: 904 Color: 4

Bin 153: 25 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 4
Size: 2703 Color: 3

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 14606 Color: 4
Size: 1768 Color: 0

Bin 155: 27 of cap free
Amount of items: 2
Items: 
Size: 12228 Color: 4
Size: 4145 Color: 2

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 10308 Color: 0
Size: 6064 Color: 4

Bin 157: 28 of cap free
Amount of items: 2
Items: 
Size: 11548 Color: 2
Size: 4824 Color: 3

Bin 158: 29 of cap free
Amount of items: 2
Items: 
Size: 10260 Color: 1
Size: 6111 Color: 0

Bin 159: 31 of cap free
Amount of items: 2
Items: 
Size: 13157 Color: 0
Size: 3212 Color: 3

Bin 160: 32 of cap free
Amount of items: 3
Items: 
Size: 9708 Color: 1
Size: 5148 Color: 3
Size: 1512 Color: 4

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 12441 Color: 2
Size: 3927 Color: 3

Bin 162: 34 of cap free
Amount of items: 3
Items: 
Size: 9235 Color: 0
Size: 6831 Color: 3
Size: 300 Color: 1

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 4
Size: 3573 Color: 0

Bin 164: 38 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 3
Size: 2478 Color: 0
Size: 208 Color: 3

Bin 165: 39 of cap free
Amount of items: 2
Items: 
Size: 11982 Color: 3
Size: 4379 Color: 0

Bin 166: 39 of cap free
Amount of items: 3
Items: 
Size: 12085 Color: 3
Size: 4108 Color: 0
Size: 168 Color: 2

Bin 167: 39 of cap free
Amount of items: 2
Items: 
Size: 13959 Color: 4
Size: 2402 Color: 0

Bin 168: 41 of cap free
Amount of items: 2
Items: 
Size: 14207 Color: 2
Size: 2152 Color: 4

Bin 169: 44 of cap free
Amount of items: 2
Items: 
Size: 10064 Color: 4
Size: 6292 Color: 3

Bin 170: 45 of cap free
Amount of items: 5
Items: 
Size: 8216 Color: 1
Size: 2102 Color: 4
Size: 2099 Color: 3
Size: 2064 Color: 1
Size: 1874 Color: 4

Bin 171: 45 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 2
Size: 2925 Color: 3

Bin 172: 46 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 0
Size: 3120 Color: 2

Bin 173: 46 of cap free
Amount of items: 2
Items: 
Size: 13594 Color: 3
Size: 2760 Color: 0

Bin 174: 49 of cap free
Amount of items: 2
Items: 
Size: 12601 Color: 2
Size: 3750 Color: 4

Bin 175: 53 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 2
Size: 3571 Color: 3

Bin 176: 56 of cap free
Amount of items: 2
Items: 
Size: 8248 Color: 1
Size: 8096 Color: 0

Bin 177: 56 of cap free
Amount of items: 2
Items: 
Size: 14700 Color: 4
Size: 1644 Color: 2

Bin 178: 57 of cap free
Amount of items: 2
Items: 
Size: 10147 Color: 0
Size: 6196 Color: 4

Bin 179: 60 of cap free
Amount of items: 2
Items: 
Size: 11585 Color: 4
Size: 4755 Color: 1

Bin 180: 60 of cap free
Amount of items: 2
Items: 
Size: 14582 Color: 2
Size: 1758 Color: 4

Bin 181: 62 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 4
Size: 2778 Color: 3

Bin 182: 65 of cap free
Amount of items: 2
Items: 
Size: 10967 Color: 4
Size: 5368 Color: 2

Bin 183: 69 of cap free
Amount of items: 2
Items: 
Size: 11689 Color: 3
Size: 4642 Color: 2

Bin 184: 69 of cap free
Amount of items: 2
Items: 
Size: 13955 Color: 0
Size: 2376 Color: 3

Bin 185: 69 of cap free
Amount of items: 2
Items: 
Size: 14432 Color: 4
Size: 1899 Color: 0

Bin 186: 71 of cap free
Amount of items: 2
Items: 
Size: 11902 Color: 0
Size: 4427 Color: 2

Bin 187: 74 of cap free
Amount of items: 2
Items: 
Size: 13815 Color: 3
Size: 2511 Color: 2

Bin 188: 81 of cap free
Amount of items: 2
Items: 
Size: 13287 Color: 4
Size: 3032 Color: 3

Bin 189: 83 of cap free
Amount of items: 3
Items: 
Size: 9207 Color: 0
Size: 6830 Color: 4
Size: 280 Color: 1

Bin 190: 101 of cap free
Amount of items: 2
Items: 
Size: 10935 Color: 0
Size: 5364 Color: 4

Bin 191: 118 of cap free
Amount of items: 2
Items: 
Size: 13250 Color: 2
Size: 3032 Color: 3

Bin 192: 126 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 0
Size: 3892 Color: 3

Bin 193: 132 of cap free
Amount of items: 30
Items: 
Size: 752 Color: 3
Size: 736 Color: 4
Size: 728 Color: 2
Size: 678 Color: 4
Size: 668 Color: 2
Size: 654 Color: 0
Size: 608 Color: 4
Size: 600 Color: 1
Size: 592 Color: 2
Size: 592 Color: 0
Size: 574 Color: 1
Size: 552 Color: 4
Size: 552 Color: 3
Size: 548 Color: 3
Size: 540 Color: 1
Size: 528 Color: 1
Size: 524 Color: 0
Size: 520 Color: 1
Size: 516 Color: 1
Size: 488 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 448 Color: 4
Size: 448 Color: 0
Size: 440 Color: 0
Size: 430 Color: 2
Size: 428 Color: 4
Size: 420 Color: 1
Size: 408 Color: 4
Size: 336 Color: 2

Bin 194: 134 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 3
Size: 3432 Color: 2

Bin 195: 141 of cap free
Amount of items: 2
Items: 
Size: 12117 Color: 4
Size: 4142 Color: 2

Bin 196: 193 of cap free
Amount of items: 3
Items: 
Size: 9334 Color: 2
Size: 5299 Color: 4
Size: 1574 Color: 0

Bin 197: 248 of cap free
Amount of items: 2
Items: 
Size: 9324 Color: 4
Size: 6828 Color: 0

Bin 198: 254 of cap free
Amount of items: 2
Items: 
Size: 8212 Color: 3
Size: 7934 Color: 0

Bin 199: 12532 of cap free
Amount of items: 11
Items: 
Size: 420 Color: 3
Size: 416 Color: 1
Size: 406 Color: 3
Size: 406 Color: 0
Size: 348 Color: 4
Size: 348 Color: 3
Size: 336 Color: 4
Size: 324 Color: 4
Size: 312 Color: 2
Size: 280 Color: 2
Size: 272 Color: 1

Total size: 3247200
Total free space: 16400

