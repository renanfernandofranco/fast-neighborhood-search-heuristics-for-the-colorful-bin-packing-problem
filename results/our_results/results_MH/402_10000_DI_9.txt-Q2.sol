Capicity Bin: 9808
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 0
Size: 888 Color: 0
Size: 160 Color: 1

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 4800 Color: 0
Size: 1440 Color: 1
Size: 1184 Color: 0
Size: 864 Color: 0
Size: 592 Color: 1
Size: 544 Color: 1
Size: 384 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 8236 Color: 1
Size: 1316 Color: 0
Size: 160 Color: 1
Size: 64 Color: 1
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 0
Size: 2908 Color: 1
Size: 152 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8150 Color: 0
Size: 1382 Color: 1
Size: 276 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 0
Size: 3546 Color: 0
Size: 708 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 1
Size: 1288 Color: 1
Size: 240 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8762 Color: 1
Size: 874 Color: 1
Size: 172 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 0
Size: 2050 Color: 1
Size: 408 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8005 Color: 0
Size: 1503 Color: 0
Size: 300 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 0
Size: 3818 Color: 1
Size: 760 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7836 Color: 1
Size: 1644 Color: 1
Size: 328 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 0
Size: 4084 Color: 1
Size: 816 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 0
Size: 1102 Color: 1
Size: 216 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 0
Size: 2678 Color: 0
Size: 532 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8436 Color: 0
Size: 1188 Color: 1
Size: 184 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8664 Color: 0
Size: 968 Color: 1
Size: 176 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 0
Size: 2184 Color: 0
Size: 432 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8407 Color: 0
Size: 1169 Color: 0
Size: 232 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6367 Color: 1
Size: 2869 Color: 1
Size: 572 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6637 Color: 1
Size: 2643 Color: 1
Size: 528 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 1
Size: 2212 Color: 0
Size: 440 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4964 Color: 1
Size: 4044 Color: 0
Size: 800 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 0
Size: 1702 Color: 0
Size: 340 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8626 Color: 1
Size: 986 Color: 0
Size: 196 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 1
Size: 1108 Color: 0
Size: 216 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7887 Color: 0
Size: 1601 Color: 0
Size: 320 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 1
Size: 3054 Color: 0
Size: 608 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 4408 Color: 0
Size: 480 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8310 Color: 0
Size: 1250 Color: 0
Size: 248 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 0
Size: 2322 Color: 0
Size: 464 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8214 Color: 1
Size: 1330 Color: 1
Size: 264 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8287 Color: 1
Size: 1269 Color: 0
Size: 252 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7151 Color: 1
Size: 2215 Color: 0
Size: 442 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5557 Color: 0
Size: 3543 Color: 0
Size: 708 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4906 Color: 0
Size: 4086 Color: 1
Size: 816 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 0
Size: 1326 Color: 0
Size: 72 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 1
Size: 2488 Color: 1
Size: 480 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4936 Color: 0
Size: 4584 Color: 1
Size: 288 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8190 Color: 1
Size: 1350 Color: 0
Size: 268 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 0
Size: 3514 Color: 1
Size: 700 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4905 Color: 0
Size: 4087 Color: 1
Size: 816 Color: 0

Bin 43: 0 of cap free
Amount of items: 4
Items: 
Size: 7904 Color: 0
Size: 1504 Color: 0
Size: 256 Color: 1
Size: 144 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 1
Size: 3444 Color: 1
Size: 680 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 0
Size: 1544 Color: 1
Size: 304 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 0
Size: 2190 Color: 1
Size: 328 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 8820 Color: 1
Size: 828 Color: 0
Size: 160 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 1
Size: 2105 Color: 1
Size: 420 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7830 Color: 1
Size: 1650 Color: 1
Size: 328 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7965 Color: 0
Size: 1537 Color: 0
Size: 306 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7623 Color: 0
Size: 1925 Color: 1
Size: 260 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 8650 Color: 1
Size: 966 Color: 1
Size: 192 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 0
Size: 1218 Color: 1
Size: 148 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 7990 Color: 1
Size: 1518 Color: 1
Size: 300 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 1
Size: 1988 Color: 0
Size: 392 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 8124 Color: 1
Size: 1404 Color: 1
Size: 280 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6408 Color: 0
Size: 2840 Color: 1
Size: 560 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 0
Size: 2650 Color: 1
Size: 528 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 0
Size: 2892 Color: 1
Size: 576 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4904 Color: 0
Size: 4792 Color: 1
Size: 112 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6103 Color: 1
Size: 3519 Color: 0
Size: 186 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 8536 Color: 1
Size: 1064 Color: 0
Size: 208 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 0
Size: 4068 Color: 0
Size: 808 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 0
Size: 1194 Color: 0
Size: 236 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 0
Size: 4052 Color: 0
Size: 808 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 7594 Color: 1
Size: 1846 Color: 0
Size: 368 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7806 Color: 1
Size: 1670 Color: 1
Size: 332 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 4084 Color: 0
Size: 808 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 1
Size: 2516 Color: 1
Size: 496 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8122 Color: 0
Size: 1406 Color: 0
Size: 280 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 0
Size: 1102 Color: 1
Size: 128 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7461 Color: 0
Size: 2021 Color: 1
Size: 326 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 1
Size: 3272 Color: 0
Size: 640 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 1
Size: 2932 Color: 1
Size: 584 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 7562 Color: 0
Size: 1874 Color: 1
Size: 372 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 1
Size: 3388 Color: 1
Size: 672 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 8107 Color: 1
Size: 1419 Color: 0
Size: 282 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 0
Size: 856 Color: 0
Size: 160 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 8748 Color: 1
Size: 908 Color: 0
Size: 152 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7477 Color: 1
Size: 1943 Color: 1
Size: 388 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 7124 Color: 0
Size: 2596 Color: 1
Size: 88 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4907 Color: 1
Size: 4085 Color: 1
Size: 816 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 8794 Color: 0
Size: 846 Color: 0
Size: 168 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5636 Color: 0
Size: 3540 Color: 1
Size: 632 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7785 Color: 0
Size: 1687 Color: 0
Size: 336 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 7318 Color: 1
Size: 2078 Color: 1
Size: 412 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 8692 Color: 1
Size: 932 Color: 0
Size: 184 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 8022 Color: 0
Size: 1490 Color: 1
Size: 296 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 7996 Color: 1
Size: 1516 Color: 0
Size: 296 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 1
Size: 1242 Color: 0
Size: 244 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 8572 Color: 0
Size: 1036 Color: 0
Size: 200 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 8182 Color: 1
Size: 1358 Color: 0
Size: 268 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5618 Color: 1
Size: 3494 Color: 1
Size: 696 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 0
Size: 2708 Color: 0
Size: 88 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 1
Size: 1192 Color: 1
Size: 224 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 8574 Color: 0
Size: 1074 Color: 1
Size: 160 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 7496 Color: 1
Size: 1928 Color: 0
Size: 384 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 8644 Color: 0
Size: 980 Color: 0
Size: 184 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5272 Color: 0
Size: 3784 Color: 0
Size: 752 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5559 Color: 0
Size: 3541 Color: 1
Size: 708 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 1
Size: 2686 Color: 1
Size: 536 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 1
Size: 1400 Color: 1
Size: 272 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 7798 Color: 1
Size: 1678 Color: 0
Size: 332 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 0
Size: 3522 Color: 0
Size: 700 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 8324 Color: 1
Size: 1244 Color: 1
Size: 240 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 0
Size: 1720 Color: 1
Size: 336 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6101 Color: 0
Size: 3091 Color: 1
Size: 616 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 0
Size: 3412 Color: 1
Size: 680 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 0
Size: 4410 Color: 1
Size: 488 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7652 Color: 0
Size: 1804 Color: 0
Size: 352 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 4240 Color: 0
Size: 3712 Color: 0
Size: 1856 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 8742 Color: 1
Size: 890 Color: 1
Size: 176 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 7087 Color: 0
Size: 2491 Color: 1
Size: 230 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6158 Color: 0
Size: 3042 Color: 0
Size: 608 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 8155 Color: 1
Size: 1379 Color: 1
Size: 274 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 0
Size: 2371 Color: 1
Size: 474 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 8163 Color: 1
Size: 1371 Color: 0
Size: 274 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 8227 Color: 1
Size: 1319 Color: 0
Size: 262 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 8255 Color: 1
Size: 1295 Color: 0
Size: 258 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 8533 Color: 0
Size: 1063 Color: 0
Size: 212 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 8563 Color: 1
Size: 1039 Color: 1
Size: 206 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 8275 Color: 0
Size: 1279 Color: 0
Size: 254 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 7701 Color: 1
Size: 1757 Color: 0
Size: 350 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 1
Size: 1186 Color: 0
Size: 188 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 0
Size: 2350 Color: 0
Size: 468 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 8195 Color: 1
Size: 1427 Color: 0
Size: 186 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 7741 Color: 1
Size: 1909 Color: 1
Size: 158 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 1
Size: 2495 Color: 1
Size: 498 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 8399 Color: 0
Size: 1175 Color: 1
Size: 234 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 8547 Color: 1
Size: 1051 Color: 1
Size: 210 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 8483 Color: 0
Size: 1105 Color: 0
Size: 220 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 8375 Color: 0
Size: 1195 Color: 0
Size: 238 Color: 1

Total size: 1294656
Total free space: 0

