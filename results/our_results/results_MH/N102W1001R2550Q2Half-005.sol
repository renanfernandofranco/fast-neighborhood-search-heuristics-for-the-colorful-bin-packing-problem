Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 304 Color: 1
Size: 270 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 336 Color: 1
Size: 268 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 299 Color: 1
Size: 272 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 1
Size: 338 Color: 1
Size: 318 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 261 Color: 0
Size: 264 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 265 Color: 1
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 328 Color: 1
Size: 279 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 314 Color: 1
Size: 281 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 329 Color: 0
Size: 310 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 256 Color: 0
Size: 258 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 316 Color: 1
Size: 311 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 324 Color: 1
Size: 271 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 331 Color: 1
Size: 273 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 301 Color: 1
Size: 270 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 270 Color: 1
Size: 261 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 334 Color: 1
Size: 315 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 263 Color: 1
Size: 259 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 331 Color: 1
Size: 271 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 305 Color: 1
Size: 266 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 316 Color: 1
Size: 311 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 320 Color: 1
Size: 316 Color: 0
Size: 365 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 343 Color: 1
Size: 262 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 297 Color: 1
Size: 269 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 283 Color: 1
Size: 260 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 350 Color: 1
Size: 257 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 314 Color: 1
Size: 278 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 250 Color: 0
Size: 279 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 312 Color: 1
Size: 254 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 1
Size: 253 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 331 Color: 1
Size: 285 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 278 Color: 1
Size: 256 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1
Size: 328 Color: 1
Size: 326 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 337 Color: 1
Size: 279 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 273 Color: 1
Size: 258 Color: 0

Total size: 34034
Total free space: 0

