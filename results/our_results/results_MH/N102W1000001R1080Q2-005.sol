Capicity Bin: 1000001
Lower Bound: 48

Bins used: 51
Amount of Colors: 2

Bin 1: 31 of cap free
Amount of items: 3
Items: 
Size: 635850 Color: 0
Size: 212023 Color: 1
Size: 152097 Color: 1

Bin 2: 127 of cap free
Amount of items: 2
Items: 
Size: 783518 Color: 1
Size: 216356 Color: 0

Bin 3: 306 of cap free
Amount of items: 3
Items: 
Size: 551225 Color: 0
Size: 243520 Color: 1
Size: 204950 Color: 0

Bin 4: 439 of cap free
Amount of items: 2
Items: 
Size: 601417 Color: 0
Size: 398145 Color: 1

Bin 5: 453 of cap free
Amount of items: 3
Items: 
Size: 667420 Color: 1
Size: 219382 Color: 1
Size: 112746 Color: 0

Bin 6: 486 of cap free
Amount of items: 2
Items: 
Size: 654904 Color: 1
Size: 344611 Color: 0

Bin 7: 611 of cap free
Amount of items: 3
Items: 
Size: 607354 Color: 0
Size: 197818 Color: 0
Size: 194218 Color: 1

Bin 8: 705 of cap free
Amount of items: 3
Items: 
Size: 621392 Color: 0
Size: 252893 Color: 1
Size: 125011 Color: 1

Bin 9: 1058 of cap free
Amount of items: 3
Items: 
Size: 453490 Color: 0
Size: 274928 Color: 1
Size: 270525 Color: 0

Bin 10: 1140 of cap free
Amount of items: 3
Items: 
Size: 457511 Color: 0
Size: 353863 Color: 0
Size: 187487 Color: 1

Bin 11: 1174 of cap free
Amount of items: 3
Items: 
Size: 578741 Color: 0
Size: 249518 Color: 0
Size: 170568 Color: 1

Bin 12: 1224 of cap free
Amount of items: 3
Items: 
Size: 610169 Color: 0
Size: 195956 Color: 1
Size: 192652 Color: 0

Bin 13: 1467 of cap free
Amount of items: 2
Items: 
Size: 691092 Color: 1
Size: 307442 Color: 0

Bin 14: 1573 of cap free
Amount of items: 2
Items: 
Size: 527115 Color: 0
Size: 471313 Color: 1

Bin 15: 1662 of cap free
Amount of items: 3
Items: 
Size: 570200 Color: 1
Size: 217115 Color: 1
Size: 211024 Color: 0

Bin 16: 2091 of cap free
Amount of items: 2
Items: 
Size: 536098 Color: 1
Size: 461812 Color: 0

Bin 17: 2128 of cap free
Amount of items: 2
Items: 
Size: 664416 Color: 1
Size: 333457 Color: 0

Bin 18: 2911 of cap free
Amount of items: 2
Items: 
Size: 608848 Color: 0
Size: 388242 Color: 1

Bin 19: 3266 of cap free
Amount of items: 2
Items: 
Size: 669673 Color: 0
Size: 327062 Color: 1

Bin 20: 3340 of cap free
Amount of items: 2
Items: 
Size: 516871 Color: 1
Size: 479790 Color: 0

Bin 21: 3455 of cap free
Amount of items: 2
Items: 
Size: 708223 Color: 1
Size: 288323 Color: 0

Bin 22: 3684 of cap free
Amount of items: 2
Items: 
Size: 556957 Color: 1
Size: 439360 Color: 0

Bin 23: 4091 of cap free
Amount of items: 3
Items: 
Size: 574309 Color: 0
Size: 316963 Color: 1
Size: 104638 Color: 0

Bin 24: 5065 of cap free
Amount of items: 2
Items: 
Size: 544264 Color: 1
Size: 450672 Color: 0

Bin 25: 5196 of cap free
Amount of items: 2
Items: 
Size: 624110 Color: 1
Size: 370695 Color: 0

Bin 26: 5652 of cap free
Amount of items: 2
Items: 
Size: 526416 Color: 0
Size: 467933 Color: 1

Bin 27: 6717 of cap free
Amount of items: 2
Items: 
Size: 588277 Color: 1
Size: 405007 Color: 0

Bin 28: 8336 of cap free
Amount of items: 2
Items: 
Size: 655905 Color: 0
Size: 335760 Color: 1

Bin 29: 11093 of cap free
Amount of items: 2
Items: 
Size: 576665 Color: 1
Size: 412243 Color: 0

Bin 30: 13736 of cap free
Amount of items: 2
Items: 
Size: 718373 Color: 1
Size: 267892 Color: 0

Bin 31: 14169 of cap free
Amount of items: 3
Items: 
Size: 572035 Color: 0
Size: 311360 Color: 1
Size: 102437 Color: 0

Bin 32: 14698 of cap free
Amount of items: 2
Items: 
Size: 638660 Color: 0
Size: 346643 Color: 1

Bin 33: 15566 of cap free
Amount of items: 2
Items: 
Size: 652967 Color: 1
Size: 331468 Color: 0

Bin 34: 20900 of cap free
Amount of items: 2
Items: 
Size: 734494 Color: 0
Size: 244607 Color: 1

Bin 35: 32379 of cap free
Amount of items: 2
Items: 
Size: 493301 Color: 1
Size: 474321 Color: 0

Bin 36: 40449 of cap free
Amount of items: 2
Items: 
Size: 491596 Color: 1
Size: 467956 Color: 0

Bin 37: 41195 of cap free
Amount of items: 2
Items: 
Size: 575943 Color: 1
Size: 382863 Color: 0

Bin 38: 45372 of cap free
Amount of items: 2
Items: 
Size: 575523 Color: 1
Size: 379106 Color: 0

Bin 39: 47182 of cap free
Amount of items: 2
Items: 
Size: 574746 Color: 1
Size: 378073 Color: 0

Bin 40: 202224 of cap free
Amount of items: 1
Items: 
Size: 797777 Color: 0

Bin 41: 204239 of cap free
Amount of items: 1
Items: 
Size: 795762 Color: 0

Bin 42: 204380 of cap free
Amount of items: 1
Items: 
Size: 795621 Color: 1

Bin 43: 207593 of cap free
Amount of items: 1
Items: 
Size: 792408 Color: 0

Bin 44: 213376 of cap free
Amount of items: 1
Items: 
Size: 786625 Color: 1

Bin 45: 216029 of cap free
Amount of items: 1
Items: 
Size: 783972 Color: 0

Bin 46: 227056 of cap free
Amount of items: 1
Items: 
Size: 772945 Color: 1

Bin 47: 242738 of cap free
Amount of items: 1
Items: 
Size: 757263 Color: 1

Bin 48: 281297 of cap free
Amount of items: 1
Items: 
Size: 718704 Color: 0

Bin 49: 294473 of cap free
Amount of items: 1
Items: 
Size: 705528 Color: 1

Bin 50: 298278 of cap free
Amount of items: 1
Items: 
Size: 701723 Color: 0

Bin 51: 310169 of cap free
Amount of items: 1
Items: 
Size: 689832 Color: 0

Total size: 47733072
Total free space: 3266979

