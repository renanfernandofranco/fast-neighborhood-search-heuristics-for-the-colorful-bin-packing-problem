Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9318 Color: 428
Size: 6610 Color: 390
Size: 360 Color: 52

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 459
Size: 4600 Color: 357
Size: 272 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12180 Color: 477
Size: 4028 Color: 345
Size: 80 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 483
Size: 3528 Color: 331
Size: 272 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12522 Color: 484
Size: 3142 Color: 320
Size: 624 Color: 125

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 488
Size: 3332 Color: 324
Size: 360 Color: 53

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 495
Size: 3072 Color: 316
Size: 372 Color: 61

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12991 Color: 497
Size: 1686 Color: 230
Size: 1611 Color: 226

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13008 Color: 498
Size: 2482 Color: 294
Size: 798 Color: 149

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 500
Size: 2232 Color: 279
Size: 1008 Color: 172

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13066 Color: 501
Size: 2086 Color: 269
Size: 1136 Color: 181

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13151 Color: 503
Size: 2609 Color: 301
Size: 528 Color: 110

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 506
Size: 1706 Color: 233
Size: 1344 Color: 197

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13252 Color: 507
Size: 1964 Color: 257
Size: 1072 Color: 177

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 508
Size: 1622 Color: 227
Size: 1364 Color: 203

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 509
Size: 1628 Color: 228
Size: 1356 Color: 202

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 515
Size: 2712 Color: 305
Size: 160 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 516
Size: 1848 Color: 245
Size: 1022 Color: 176

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13482 Color: 518
Size: 1714 Color: 234
Size: 1092 Color: 178

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13505 Color: 520
Size: 2361 Color: 287
Size: 422 Color: 80

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 526
Size: 2084 Color: 268
Size: 580 Color: 117

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13630 Color: 527
Size: 1410 Color: 209
Size: 1248 Color: 192

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13712 Color: 530
Size: 2040 Color: 263
Size: 536 Color: 111

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13713 Color: 531
Size: 2025 Color: 261
Size: 550 Color: 114

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13761 Color: 533
Size: 1859 Color: 247
Size: 668 Color: 130

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 534
Size: 2204 Color: 275
Size: 312 Color: 34

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 540
Size: 1818 Color: 242
Size: 622 Color: 123

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13929 Color: 543
Size: 1967 Color: 258
Size: 392 Color: 70

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13970 Color: 546
Size: 1582 Color: 224
Size: 736 Color: 142

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14011 Color: 549
Size: 1911 Color: 253
Size: 366 Color: 57

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 550
Size: 1860 Color: 248
Size: 402 Color: 72

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 551
Size: 1692 Color: 232
Size: 568 Color: 115

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14031 Color: 552
Size: 1681 Color: 229
Size: 576 Color: 116

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14091 Color: 557
Size: 1765 Color: 236
Size: 432 Color: 82

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 558
Size: 1394 Color: 207
Size: 792 Color: 147

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14107 Color: 559
Size: 1819 Color: 243
Size: 362 Color: 55

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 564
Size: 1136 Color: 182
Size: 988 Color: 167

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 565
Size: 1500 Color: 213
Size: 620 Color: 122

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 571
Size: 1528 Color: 218
Size: 496 Color: 102

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 572
Size: 1502 Color: 214
Size: 520 Color: 107

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 574
Size: 1580 Color: 223
Size: 368 Color: 59

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14411 Color: 581
Size: 1565 Color: 221
Size: 312 Color: 33

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 583
Size: 1532 Color: 219
Size: 304 Color: 29

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 584
Size: 1352 Color: 198
Size: 480 Color: 99

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14457 Color: 585
Size: 1527 Color: 217
Size: 304 Color: 30

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14490 Color: 588
Size: 1352 Color: 199
Size: 446 Color: 87

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14502 Color: 590
Size: 1162 Color: 185
Size: 624 Color: 124

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 591
Size: 1476 Color: 212
Size: 288 Color: 21

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14535 Color: 592
Size: 1461 Color: 211
Size: 292 Color: 24

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14559 Color: 593
Size: 1441 Color: 210
Size: 288 Color: 23

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 595
Size: 1320 Color: 194
Size: 352 Color: 49

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 596
Size: 1002 Color: 171
Size: 668 Color: 131

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14642 Color: 597
Size: 1366 Color: 204
Size: 280 Color: 19

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 598
Size: 1232 Color: 191
Size: 412 Color: 76

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14650 Color: 599
Size: 1174 Color: 188
Size: 464 Color: 92

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 600
Size: 1328 Color: 195
Size: 308 Color: 31

Bin 57: 1 of cap free
Amount of items: 5
Items: 
Size: 8172 Color: 412
Size: 3625 Color: 335
Size: 3602 Color: 334
Size: 504 Color: 104
Size: 384 Color: 67

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12072 Color: 473
Size: 3631 Color: 336
Size: 584 Color: 119

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 12292 Color: 480
Size: 3995 Color: 344

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12789 Color: 492
Size: 2686 Color: 304
Size: 812 Color: 153

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12983 Color: 496
Size: 3176 Color: 321
Size: 128 Color: 3

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 505
Size: 3111 Color: 318

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13349 Color: 511
Size: 2938 Color: 314

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13376 Color: 513
Size: 2911 Color: 312

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13457 Color: 517
Size: 2542 Color: 299
Size: 288 Color: 22

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13489 Color: 519
Size: 2394 Color: 288
Size: 404 Color: 73

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13617 Color: 525
Size: 2062 Color: 266
Size: 608 Color: 121

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 555
Size: 2227 Color: 278

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 14260 Color: 570
Size: 2027 Color: 262

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 14396 Color: 579
Size: 1891 Color: 251

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 587
Size: 1799 Color: 240

Bin 72: 2 of cap free
Amount of items: 9
Items: 
Size: 8146 Color: 406
Size: 1356 Color: 200
Size: 1344 Color: 196
Size: 1312 Color: 193
Size: 1186 Color: 189
Size: 1170 Color: 187
Size: 900 Color: 163
Size: 440 Color: 84
Size: 432 Color: 83

Bin 73: 2 of cap free
Amount of items: 5
Items: 
Size: 9331 Color: 429
Size: 5795 Color: 376
Size: 444 Color: 86
Size: 358 Color: 51
Size: 358 Color: 50

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 10188 Color: 440
Size: 5782 Color: 374
Size: 316 Color: 35

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 11931 Color: 469
Size: 3345 Color: 325
Size: 1010 Color: 173

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 481
Size: 2894 Color: 311
Size: 1000 Color: 170

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13537 Color: 522
Size: 2749 Color: 308

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13838 Color: 538
Size: 2448 Color: 292

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13841 Color: 539
Size: 2445 Color: 291

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 545
Size: 2342 Color: 286

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14139 Color: 563
Size: 2147 Color: 272

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14387 Color: 577
Size: 1899 Color: 252

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 14598 Color: 594
Size: 1688 Color: 231

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 12458 Color: 482
Size: 3351 Color: 326
Size: 476 Color: 98

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 12549 Color: 486
Size: 3736 Color: 340

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 13609 Color: 524
Size: 2676 Color: 303

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13753 Color: 532
Size: 2532 Color: 298

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 13873 Color: 542
Size: 2412 Color: 290

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 14038 Color: 553
Size: 2247 Color: 281

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 11096 Color: 457
Size: 5188 Color: 368

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 474
Size: 3502 Color: 329
Size: 700 Color: 135

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13529 Color: 521
Size: 2755 Color: 309

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 560
Size: 2174 Color: 274

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14171 Color: 566
Size: 2113 Color: 270

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14271 Color: 573
Size: 2013 Color: 260

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 580
Size: 1878 Color: 249

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14462 Color: 586
Size: 1822 Color: 244

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 528
Size: 2615 Color: 302
Size: 24 Color: 0

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 14492 Color: 589
Size: 1791 Color: 239

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 9276 Color: 426
Size: 6642 Color: 391
Size: 364 Color: 56

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 10178 Color: 439
Size: 5788 Color: 375
Size: 316 Color: 36

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 466
Size: 4466 Color: 354

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 12018 Color: 472
Size: 4072 Color: 347
Size: 192 Color: 6

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13682 Color: 529
Size: 2600 Color: 300

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 14433 Color: 582
Size: 1849 Color: 246

Bin 106: 7 of cap free
Amount of items: 3
Items: 
Size: 9315 Color: 427
Size: 6606 Color: 389
Size: 360 Color: 54

Bin 107: 7 of cap free
Amount of items: 5
Items: 
Size: 9354 Color: 431
Size: 5851 Color: 379
Size: 384 Color: 69
Size: 348 Color: 46
Size: 344 Color: 45

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 455
Size: 5021 Color: 362
Size: 272 Color: 14

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 548
Size: 2284 Color: 282

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 14239 Color: 569
Size: 2042 Color: 265

Bin 111: 8 of cap free
Amount of items: 5
Items: 
Size: 9348 Color: 430
Size: 5844 Color: 378
Size: 384 Color: 68
Size: 352 Color: 48
Size: 352 Color: 47

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 556
Size: 2208 Color: 276

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 14346 Color: 575
Size: 1934 Color: 255

Bin 114: 9 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 434
Size: 6287 Color: 388
Size: 336 Color: 41

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 11832 Color: 467
Size: 4447 Color: 352

Bin 116: 10 of cap free
Amount of items: 21
Items: 
Size: 1016 Color: 174
Size: 998 Color: 169
Size: 992 Color: 168
Size: 944 Color: 166
Size: 944 Color: 165
Size: 912 Color: 164
Size: 896 Color: 162
Size: 892 Color: 161
Size: 888 Color: 160
Size: 888 Color: 159
Size: 880 Color: 158
Size: 878 Color: 157
Size: 864 Color: 156
Size: 848 Color: 155
Size: 640 Color: 127
Size: 472 Color: 96
Size: 470 Color: 95
Size: 466 Color: 94
Size: 464 Color: 93
Size: 464 Color: 91
Size: 462 Color: 90

Bin 117: 10 of cap free
Amount of items: 3
Items: 
Size: 10953 Color: 454
Size: 5053 Color: 364
Size: 272 Color: 15

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 494
Size: 3460 Color: 328

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13022 Color: 499
Size: 3256 Color: 323

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13084 Color: 502
Size: 3194 Color: 322

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13310 Color: 510
Size: 2968 Color: 315

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 12557 Color: 487
Size: 3720 Color: 339

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 14059 Color: 554
Size: 2218 Color: 277

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 576
Size: 1922 Color: 254

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 9123 Color: 420
Size: 6785 Color: 395
Size: 368 Color: 58

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 10145 Color: 438
Size: 5811 Color: 377
Size: 320 Color: 37

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 10297 Color: 445
Size: 5979 Color: 382

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 11562 Color: 465
Size: 4522 Color: 356
Size: 192 Color: 7

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13159 Color: 504
Size: 3117 Color: 319

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 536
Size: 2488 Color: 296

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 14116 Color: 561
Size: 2160 Color: 273

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 578
Size: 1886 Color: 250

Bin 133: 13 of cap free
Amount of items: 3
Items: 
Size: 11453 Color: 460
Size: 3084 Color: 317
Size: 1738 Color: 235

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 13982 Color: 547
Size: 2293 Color: 283

Bin 135: 13 of cap free
Amount of items: 2
Items: 
Size: 14234 Color: 568
Size: 2041 Color: 264

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 14206 Color: 567
Size: 2068 Color: 267

Bin 137: 15 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 475
Size: 4031 Color: 346
Size: 152 Color: 4

Bin 138: 15 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 544
Size: 2333 Color: 285

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 11852 Color: 468
Size: 4420 Color: 351

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 12766 Color: 491
Size: 3506 Color: 330

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 13355 Color: 512
Size: 2917 Color: 313

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 13396 Color: 514
Size: 2876 Color: 310

Bin 143: 17 of cap free
Amount of items: 3
Items: 
Size: 11527 Color: 464
Size: 4520 Color: 355
Size: 224 Color: 8

Bin 144: 17 of cap free
Amount of items: 2
Items: 
Size: 13786 Color: 535
Size: 2485 Color: 295

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 14131 Color: 562
Size: 2140 Color: 271

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 523
Size: 2722 Color: 306

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 12275 Color: 479
Size: 3994 Color: 343

Bin 148: 20 of cap free
Amount of items: 3
Items: 
Size: 10236 Color: 442
Size: 5720 Color: 373
Size: 312 Color: 32

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 489
Size: 3636 Color: 337

Bin 150: 21 of cap free
Amount of items: 2
Items: 
Size: 11939 Color: 470
Size: 4328 Color: 349

Bin 151: 22 of cap free
Amount of items: 3
Items: 
Size: 10948 Color: 453
Size: 5044 Color: 363
Size: 274 Color: 16

Bin 152: 23 of cap free
Amount of items: 3
Items: 
Size: 9115 Color: 419
Size: 6780 Color: 394
Size: 370 Color: 60

Bin 153: 23 of cap free
Amount of items: 2
Items: 
Size: 11144 Color: 458
Size: 5121 Color: 367

Bin 154: 23 of cap free
Amount of items: 2
Items: 
Size: 13814 Color: 537
Size: 2451 Color: 293

Bin 155: 23 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 541
Size: 2408 Color: 289

Bin 156: 24 of cap free
Amount of items: 5
Items: 
Size: 8156 Color: 411
Size: 3562 Color: 333
Size: 2746 Color: 307
Size: 1400 Color: 208
Size: 400 Color: 71

Bin 157: 24 of cap free
Amount of items: 2
Items: 
Size: 10225 Color: 441
Size: 6039 Color: 383

Bin 158: 24 of cap free
Amount of items: 3
Items: 
Size: 10270 Color: 444
Size: 5690 Color: 372
Size: 304 Color: 28

Bin 159: 26 of cap free
Amount of items: 2
Items: 
Size: 11966 Color: 471
Size: 4296 Color: 348

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 12728 Color: 490
Size: 3528 Color: 332

Bin 161: 35 of cap free
Amount of items: 3
Items: 
Size: 9113 Color: 418
Size: 6764 Color: 393
Size: 376 Color: 62

Bin 162: 36 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 461
Size: 4792 Color: 358

Bin 163: 38 of cap free
Amount of items: 3
Items: 
Size: 11031 Color: 456
Size: 4947 Color: 359
Size: 272 Color: 13

Bin 164: 40 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 437
Size: 6080 Color: 384
Size: 320 Color: 38

Bin 165: 40 of cap free
Amount of items: 2
Items: 
Size: 12548 Color: 485
Size: 3700 Color: 338

Bin 166: 42 of cap free
Amount of items: 3
Items: 
Size: 10866 Color: 449
Size: 5084 Color: 365
Size: 296 Color: 25

Bin 167: 42 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 450
Size: 5094 Color: 366
Size: 280 Color: 20

Bin 168: 43 of cap free
Amount of items: 2
Items: 
Size: 12797 Color: 493
Size: 3448 Color: 327

Bin 169: 47 of cap free
Amount of items: 2
Items: 
Size: 10776 Color: 448
Size: 5465 Color: 370

Bin 170: 50 of cap free
Amount of items: 2
Items: 
Size: 12269 Color: 478
Size: 3969 Color: 342

Bin 171: 51 of cap free
Amount of items: 3
Items: 
Size: 9731 Color: 436
Size: 6184 Color: 386
Size: 322 Color: 39

Bin 172: 52 of cap free
Amount of items: 2
Items: 
Size: 8188 Color: 413
Size: 8048 Color: 404

Bin 173: 52 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 435
Size: 6177 Color: 385
Size: 336 Color: 40

Bin 174: 56 of cap free
Amount of items: 3
Items: 
Size: 10938 Color: 452
Size: 5018 Color: 361
Size: 276 Color: 17

Bin 175: 60 of cap free
Amount of items: 7
Items: 
Size: 8148 Color: 408
Size: 1768 Color: 237
Size: 1585 Color: 225
Size: 1570 Color: 222
Size: 1547 Color: 220
Size: 1194 Color: 190
Size: 416 Color: 78

Bin 176: 61 of cap free
Amount of items: 10
Items: 
Size: 8145 Color: 405
Size: 1168 Color: 186
Size: 1152 Color: 184
Size: 1152 Color: 183
Size: 1136 Color: 180
Size: 1104 Color: 179
Size: 1016 Color: 175
Size: 458 Color: 89
Size: 456 Color: 88
Size: 440 Color: 85

Bin 177: 72 of cap free
Amount of items: 3
Items: 
Size: 10552 Color: 447
Size: 5368 Color: 369
Size: 296 Color: 26

Bin 178: 82 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 476
Size: 3942 Color: 341
Size: 96 Color: 2

Bin 179: 85 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 451
Size: 4993 Color: 360
Size: 280 Color: 18

Bin 180: 88 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 463
Size: 4462 Color: 353
Size: 240 Color: 9

Bin 181: 92 of cap free
Amount of items: 2
Items: 
Size: 10265 Color: 443
Size: 5931 Color: 381

Bin 182: 103 of cap free
Amount of items: 3
Items: 
Size: 10353 Color: 446
Size: 5528 Color: 371
Size: 304 Color: 27

Bin 183: 104 of cap free
Amount of items: 2
Items: 
Size: 9267 Color: 425
Size: 6917 Color: 400

Bin 184: 128 of cap free
Amount of items: 2
Items: 
Size: 8776 Color: 416
Size: 7384 Color: 403

Bin 185: 140 of cap free
Amount of items: 4
Items: 
Size: 8248 Color: 414
Size: 7136 Color: 401
Size: 384 Color: 66
Size: 380 Color: 65

Bin 186: 140 of cap free
Amount of items: 3
Items: 
Size: 11495 Color: 462
Size: 4413 Color: 350
Size: 240 Color: 10

Bin 187: 161 of cap free
Amount of items: 8
Items: 
Size: 8147 Color: 407
Size: 1522 Color: 216
Size: 1512 Color: 215
Size: 1374 Color: 206
Size: 1372 Color: 205
Size: 1356 Color: 201
Size: 428 Color: 81
Size: 416 Color: 79

Bin 188: 176 of cap free
Amount of items: 6
Items: 
Size: 8152 Color: 409
Size: 2000 Color: 259
Size: 1960 Color: 256
Size: 1812 Color: 241
Size: 1772 Color: 238
Size: 416 Color: 77

Bin 189: 225 of cap free
Amount of items: 2
Items: 
Size: 9235 Color: 424
Size: 6828 Color: 399

Bin 190: 226 of cap free
Amount of items: 3
Items: 
Size: 8362 Color: 415
Size: 7322 Color: 402
Size: 378 Color: 64

Bin 191: 226 of cap free
Amount of items: 3
Items: 
Size: 9462 Color: 433
Size: 6264 Color: 387
Size: 336 Color: 42

Bin 192: 238 of cap free
Amount of items: 6
Items: 
Size: 8154 Color: 410
Size: 2528 Color: 297
Size: 2321 Color: 284
Size: 2233 Color: 280
Size: 408 Color: 75
Size: 406 Color: 74

Bin 193: 284 of cap free
Amount of items: 3
Items: 
Size: 8872 Color: 417
Size: 6756 Color: 392
Size: 376 Color: 63

Bin 194: 285 of cap free
Amount of items: 2
Items: 
Size: 9211 Color: 423
Size: 6792 Color: 398

Bin 195: 301 of cap free
Amount of items: 4
Items: 
Size: 9432 Color: 432
Size: 5879 Color: 380
Size: 340 Color: 44
Size: 336 Color: 43

Bin 196: 327 of cap free
Amount of items: 2
Items: 
Size: 9174 Color: 422
Size: 6787 Color: 397

Bin 197: 331 of cap free
Amount of items: 2
Items: 
Size: 9171 Color: 421
Size: 6786 Color: 396

Bin 198: 532 of cap free
Amount of items: 24
Items: 
Size: 844 Color: 154
Size: 804 Color: 152
Size: 800 Color: 151
Size: 800 Color: 150
Size: 796 Color: 148
Size: 784 Color: 146
Size: 780 Color: 145
Size: 770 Color: 144
Size: 768 Color: 143
Size: 736 Color: 141
Size: 736 Color: 140
Size: 726 Color: 139
Size: 724 Color: 138
Size: 582 Color: 118
Size: 548 Color: 113
Size: 544 Color: 112
Size: 528 Color: 109
Size: 522 Color: 108
Size: 512 Color: 106
Size: 508 Color: 105
Size: 496 Color: 103
Size: 488 Color: 101
Size: 488 Color: 100
Size: 472 Color: 97

Bin 199: 10256 of cap free
Amount of items: 9
Items: 
Size: 720 Color: 137
Size: 708 Color: 136
Size: 696 Color: 134
Size: 688 Color: 133
Size: 672 Color: 132
Size: 664 Color: 129
Size: 656 Color: 128
Size: 636 Color: 126
Size: 592 Color: 120

Total size: 3225024
Total free space: 16288

