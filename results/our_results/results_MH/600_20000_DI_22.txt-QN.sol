Capicity Bin: 16416
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11802 Color: 463
Size: 3846 Color: 340
Size: 768 Color: 152

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11879 Color: 466
Size: 3429 Color: 320
Size: 1108 Color: 183

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12100 Color: 473
Size: 3492 Color: 327
Size: 824 Color: 156

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 474
Size: 3764 Color: 335
Size: 520 Color: 102

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12164 Color: 475
Size: 3424 Color: 319
Size: 828 Color: 157

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12190 Color: 476
Size: 3856 Color: 341
Size: 370 Color: 61

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12291 Color: 482
Size: 3439 Color: 322
Size: 686 Color: 137

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12418 Color: 486
Size: 3522 Color: 328
Size: 476 Color: 93

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12676 Color: 491
Size: 3044 Color: 305
Size: 696 Color: 140

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 492
Size: 3386 Color: 318
Size: 322 Color: 46

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 493
Size: 2324 Color: 269
Size: 1380 Color: 202

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12722 Color: 494
Size: 2982 Color: 296
Size: 712 Color: 143

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12740 Color: 495
Size: 3468 Color: 323
Size: 208 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 497
Size: 3480 Color: 326
Size: 164 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12781 Color: 498
Size: 3031 Color: 304
Size: 604 Color: 118

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12786 Color: 499
Size: 3334 Color: 316
Size: 296 Color: 29

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12791 Color: 500
Size: 3021 Color: 302
Size: 604 Color: 119

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12818 Color: 502
Size: 2990 Color: 298
Size: 608 Color: 124

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12848 Color: 505
Size: 3536 Color: 330
Size: 32 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13144 Color: 509
Size: 2360 Color: 273
Size: 912 Color: 166

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 510
Size: 2722 Color: 292
Size: 544 Color: 109

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 511
Size: 2692 Color: 290
Size: 536 Color: 105

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13189 Color: 512
Size: 2691 Color: 289
Size: 536 Color: 104

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 523
Size: 2348 Color: 272
Size: 604 Color: 120

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 524
Size: 1920 Color: 249
Size: 1008 Color: 175

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 528
Size: 1824 Color: 241
Size: 988 Color: 171

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13606 Color: 529
Size: 2026 Color: 252
Size: 784 Color: 153

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 530
Size: 2528 Color: 280
Size: 280 Color: 20

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13628 Color: 531
Size: 2704 Color: 291
Size: 84 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 534
Size: 2040 Color: 253
Size: 632 Color: 130

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 536
Size: 2202 Color: 267
Size: 436 Color: 85

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13890 Color: 541
Size: 1364 Color: 200
Size: 1162 Color: 185

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 544
Size: 2081 Color: 259
Size: 414 Color: 76

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13925 Color: 545
Size: 2073 Color: 257
Size: 418 Color: 80

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13929 Color: 546
Size: 1621 Color: 225
Size: 866 Color: 161

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13953 Color: 548
Size: 2053 Color: 255
Size: 410 Color: 73

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 549
Size: 2051 Color: 254
Size: 408 Color: 72

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 551
Size: 2106 Color: 262
Size: 324 Color: 47

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 554
Size: 1604 Color: 223
Size: 680 Color: 135

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 556
Size: 1420 Color: 204
Size: 832 Color: 158

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14176 Color: 557
Size: 1192 Color: 189
Size: 1048 Color: 179

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14193 Color: 558
Size: 1797 Color: 236
Size: 426 Color: 83

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 559
Size: 1754 Color: 230
Size: 468 Color: 90

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14261 Color: 563
Size: 1853 Color: 243
Size: 302 Color: 31

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 564
Size: 1456 Color: 207
Size: 696 Color: 141

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14265 Color: 565
Size: 1793 Color: 234
Size: 358 Color: 57

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 568
Size: 1644 Color: 227
Size: 480 Color: 96

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 574
Size: 1656 Color: 228
Size: 320 Color: 43

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14458 Color: 575
Size: 1358 Color: 195
Size: 600 Color: 117

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14473 Color: 576
Size: 1581 Color: 219
Size: 362 Color: 58

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 577
Size: 1344 Color: 194
Size: 596 Color: 115

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 579
Size: 1582 Color: 220
Size: 314 Color: 40

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14521 Color: 580
Size: 1537 Color: 215
Size: 358 Color: 56

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14537 Color: 582
Size: 1585 Color: 221
Size: 294 Color: 28

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14545 Color: 583
Size: 1517 Color: 213
Size: 354 Color: 54

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14569 Color: 585
Size: 1541 Color: 216
Size: 306 Color: 36

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14648 Color: 593
Size: 1480 Color: 209
Size: 288 Color: 22

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 595
Size: 1436 Color: 205
Size: 304 Color: 33

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 596
Size: 1088 Color: 181
Size: 628 Color: 129

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14704 Color: 597
Size: 1312 Color: 191
Size: 400 Color: 70

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 598
Size: 1168 Color: 186
Size: 532 Color: 103

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14737 Color: 599
Size: 1401 Color: 203
Size: 278 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14764 Color: 600
Size: 1152 Color: 184
Size: 500 Color: 97

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 11209 Color: 453
Size: 5206 Color: 367

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 11404 Color: 457
Size: 5011 Color: 364

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11891 Color: 467
Size: 3524 Color: 329
Size: 1000 Color: 174

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 12303 Color: 483
Size: 4112 Color: 346

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 484
Size: 3911 Color: 342
Size: 192 Color: 15

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13553 Color: 526
Size: 2582 Color: 283
Size: 280 Color: 21

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13905 Color: 543
Size: 1898 Color: 247
Size: 612 Color: 125

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 14257 Color: 562
Size: 1482 Color: 210
Size: 676 Color: 134

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 14289 Color: 567
Size: 1854 Color: 244
Size: 272 Color: 18

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12592 Color: 488
Size: 3822 Color: 339

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 504
Size: 3572 Color: 331

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 507
Size: 3344 Color: 317
Size: 28 Color: 1

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 508
Size: 3286 Color: 315
Size: 32 Color: 3

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13252 Color: 516
Size: 3162 Color: 312

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13332 Color: 519
Size: 3082 Color: 307

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13366 Color: 520
Size: 3048 Color: 306

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 13550 Color: 525
Size: 1528 Color: 214
Size: 1336 Color: 193

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 13857 Color: 539
Size: 2077 Color: 258
Size: 480 Color: 95

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 553
Size: 2390 Color: 276

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 14666 Color: 594
Size: 1744 Color: 229
Size: 4 Color: 0

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 10360 Color: 440
Size: 5765 Color: 376
Size: 288 Color: 23

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 11170 Color: 452
Size: 3472 Color: 325
Size: 1771 Color: 231

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 13199 Color: 513
Size: 2598 Color: 286
Size: 616 Color: 127

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 514
Size: 3213 Color: 314

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 14573 Color: 586
Size: 1840 Color: 242

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 590
Size: 1801 Color: 238

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 14640 Color: 592
Size: 1773 Color: 233

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 9810 Color: 431
Size: 6602 Color: 388

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 437
Size: 5896 Color: 381
Size: 288 Color: 25

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 496
Size: 3652 Color: 334

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 537
Size: 2612 Color: 287

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 538
Size: 2584 Color: 284

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 542
Size: 2520 Color: 279

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 9483 Color: 428
Size: 6624 Color: 389
Size: 304 Color: 32

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 12080 Color: 472
Size: 4331 Color: 352

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 13750 Color: 535
Size: 2661 Color: 288

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 12036 Color: 471
Size: 4374 Color: 354

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 12978 Color: 506
Size: 3432 Color: 321

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 521
Size: 3012 Color: 301

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 14314 Color: 571
Size: 2096 Color: 261

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 14597 Color: 588
Size: 1813 Color: 239

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 14638 Color: 591
Size: 1772 Color: 232

Bin 106: 7 of cap free
Amount of items: 5
Items: 
Size: 8265 Color: 414
Size: 3768 Color: 336
Size: 3468 Color: 324
Size: 560 Color: 110
Size: 348 Color: 48

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 14609 Color: 589
Size: 1800 Color: 237

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 11360 Color: 456
Size: 5048 Color: 365

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 12228 Color: 478
Size: 4180 Color: 349

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 12464 Color: 487
Size: 3944 Color: 344

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 12804 Color: 501
Size: 3604 Color: 333

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12836 Color: 503
Size: 3572 Color: 332

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 517
Size: 3124 Color: 311

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 522
Size: 2992 Color: 299

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 13632 Color: 532
Size: 2776 Color: 294

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 547
Size: 2472 Color: 278

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13960 Color: 550
Size: 2448 Color: 277

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 14224 Color: 560
Size: 2184 Color: 266

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 14352 Color: 572
Size: 2056 Color: 256

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 14500 Color: 578
Size: 1908 Color: 248

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 587
Size: 1824 Color: 240

Bin 122: 9 of cap free
Amount of items: 3
Items: 
Size: 10304 Color: 439
Size: 5815 Color: 377
Size: 288 Color: 24

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 11430 Color: 458
Size: 4976 Color: 363

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 12248 Color: 479
Size: 4158 Color: 348

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13864 Color: 540
Size: 2542 Color: 281

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 581
Size: 1884 Color: 246

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 14548 Color: 584
Size: 1858 Color: 245

Bin 128: 11 of cap free
Amount of items: 3
Items: 
Size: 9455 Color: 427
Size: 6646 Color: 390
Size: 304 Color: 34

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 14241 Color: 561
Size: 2164 Color: 265

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 570
Size: 2093 Color: 260

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 10916 Color: 448
Size: 5488 Color: 371

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 459
Size: 4790 Color: 360
Size: 152 Color: 11

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 12196 Color: 477
Size: 4208 Color: 350

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 11894 Color: 468
Size: 4508 Color: 355

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 481
Size: 4130 Color: 347

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 12626 Color: 490
Size: 3776 Color: 338

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 518
Size: 3100 Color: 310

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 14394 Color: 573
Size: 2008 Color: 251

Bin 139: 15 of cap free
Amount of items: 3
Items: 
Size: 8393 Color: 415
Size: 7688 Color: 403
Size: 320 Color: 45

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 566
Size: 2133 Color: 264

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 569
Size: 2108 Color: 263

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 533
Size: 2728 Color: 293

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 14012 Color: 552
Size: 2387 Color: 275

Bin 144: 18 of cap free
Amount of items: 11
Items: 
Size: 8212 Color: 406
Size: 1076 Color: 180
Size: 1046 Color: 178
Size: 1040 Color: 177
Size: 1024 Color: 176
Size: 992 Color: 173
Size: 992 Color: 172
Size: 872 Color: 162
Size: 384 Color: 66
Size: 384 Color: 65
Size: 376 Color: 64

Bin 145: 18 of cap free
Amount of items: 9
Items: 
Size: 8216 Color: 408
Size: 1452 Color: 206
Size: 1366 Color: 201
Size: 1364 Color: 199
Size: 1360 Color: 198
Size: 1360 Color: 196
Size: 544 Color: 108
Size: 368 Color: 60
Size: 368 Color: 59

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 9954 Color: 433
Size: 6444 Color: 385

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 11012 Color: 451
Size: 5386 Color: 369

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 11830 Color: 465
Size: 4568 Color: 357

Bin 149: 18 of cap free
Amount of items: 2
Items: 
Size: 12354 Color: 485
Size: 4044 Color: 345

Bin 150: 18 of cap free
Amount of items: 2
Items: 
Size: 14142 Color: 555
Size: 2256 Color: 268

Bin 151: 19 of cap free
Amount of items: 2
Items: 
Size: 9840 Color: 432
Size: 6557 Color: 386

Bin 152: 19 of cap free
Amount of items: 2
Items: 
Size: 13583 Color: 527
Size: 2814 Color: 295

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 13223 Color: 515
Size: 3172 Color: 313

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 10680 Color: 445
Size: 5714 Color: 375

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 10296 Color: 438
Size: 6096 Color: 384

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 10734 Color: 446
Size: 5656 Color: 374

Bin 157: 28 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 420
Size: 6834 Color: 395
Size: 310 Color: 37

Bin 158: 28 of cap free
Amount of items: 3
Items: 
Size: 10960 Color: 450
Size: 5268 Color: 368
Size: 160 Color: 13

Bin 159: 29 of cap free
Amount of items: 2
Items: 
Size: 9594 Color: 429
Size: 6793 Color: 392

Bin 160: 31 of cap free
Amount of items: 2
Items: 
Size: 10405 Color: 441
Size: 5980 Color: 383

Bin 161: 31 of cap free
Amount of items: 2
Items: 
Size: 11221 Color: 454
Size: 5164 Color: 366

Bin 162: 32 of cap free
Amount of items: 2
Items: 
Size: 11824 Color: 464
Size: 4560 Color: 356

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 489
Size: 3770 Color: 337

Bin 164: 36 of cap free
Amount of items: 3
Items: 
Size: 9236 Color: 419
Size: 6832 Color: 394
Size: 312 Color: 38

Bin 165: 38 of cap free
Amount of items: 5
Items: 
Size: 8240 Color: 412
Size: 3002 Color: 300
Size: 2988 Color: 297
Size: 1796 Color: 235
Size: 352 Color: 50

Bin 166: 44 of cap free
Amount of items: 2
Items: 
Size: 10448 Color: 442
Size: 5924 Color: 382

Bin 167: 44 of cap free
Amount of items: 2
Items: 
Size: 10936 Color: 449
Size: 5436 Color: 370

Bin 168: 46 of cap free
Amount of items: 3
Items: 
Size: 11504 Color: 460
Size: 4738 Color: 359
Size: 128 Color: 10

Bin 169: 47 of cap free
Amount of items: 5
Items: 
Size: 12260 Color: 480
Size: 3929 Color: 343
Size: 64 Color: 7
Size: 64 Color: 5
Size: 52 Color: 4

Bin 170: 52 of cap free
Amount of items: 2
Items: 
Size: 11572 Color: 461
Size: 4792 Color: 361

Bin 171: 52 of cap free
Amount of items: 2
Items: 
Size: 11688 Color: 462
Size: 4676 Color: 358

Bin 172: 60 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 469
Size: 4328 Color: 351
Size: 128 Color: 9

Bin 173: 63 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 455
Size: 4969 Color: 362
Size: 160 Color: 12

Bin 174: 64 of cap free
Amount of items: 5
Items: 
Size: 8248 Color: 413
Size: 3092 Color: 308
Size: 3026 Color: 303
Size: 1634 Color: 226
Size: 352 Color: 49

Bin 175: 66 of cap free
Amount of items: 2
Items: 
Size: 9663 Color: 430
Size: 6687 Color: 391

Bin 176: 66 of cap free
Amount of items: 3
Items: 
Size: 10670 Color: 444
Size: 3096 Color: 309
Size: 2584 Color: 285

Bin 177: 68 of cap free
Amount of items: 2
Items: 
Size: 10812 Color: 447
Size: 5536 Color: 373

Bin 178: 71 of cap free
Amount of items: 3
Items: 
Size: 9439 Color: 426
Size: 6600 Color: 387
Size: 306 Color: 35

Bin 179: 91 of cap free
Amount of items: 9
Items: 
Size: 8213 Color: 407
Size: 1360 Color: 197
Size: 1328 Color: 192
Size: 1216 Color: 190
Size: 1184 Color: 188
Size: 1172 Color: 187
Size: 1100 Color: 182
Size: 376 Color: 63
Size: 376 Color: 62

Bin 180: 91 of cap free
Amount of items: 2
Items: 
Size: 9416 Color: 425
Size: 6909 Color: 400

Bin 181: 92 of cap free
Amount of items: 3
Items: 
Size: 10170 Color: 436
Size: 5866 Color: 380
Size: 288 Color: 26

Bin 182: 99 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 470
Size: 4341 Color: 353
Size: 64 Color: 6

Bin 183: 112 of cap free
Amount of items: 3
Items: 
Size: 10164 Color: 435
Size: 5852 Color: 379
Size: 288 Color: 27

Bin 184: 115 of cap free
Amount of items: 14
Items: 
Size: 8209 Color: 404
Size: 864 Color: 160
Size: 864 Color: 159
Size: 800 Color: 155
Size: 800 Color: 154
Size: 764 Color: 151
Size: 752 Color: 150
Size: 752 Color: 149
Size: 420 Color: 81
Size: 416 Color: 79
Size: 416 Color: 78
Size: 416 Color: 77
Size: 414 Color: 75
Size: 414 Color: 74

Bin 185: 154 of cap free
Amount of items: 12
Items: 
Size: 8210 Color: 405
Size: 956 Color: 170
Size: 944 Color: 169
Size: 944 Color: 168
Size: 928 Color: 167
Size: 900 Color: 165
Size: 896 Color: 164
Size: 896 Color: 163
Size: 404 Color: 71
Size: 400 Color: 69
Size: 400 Color: 68
Size: 384 Color: 67

Bin 186: 168 of cap free
Amount of items: 3
Items: 
Size: 10100 Color: 434
Size: 5848 Color: 378
Size: 300 Color: 30

Bin 187: 173 of cap free
Amount of items: 4
Items: 
Size: 8442 Color: 416
Size: 7161 Color: 401
Size: 320 Color: 44
Size: 320 Color: 42

Bin 188: 174 of cap free
Amount of items: 3
Items: 
Size: 9104 Color: 418
Size: 6826 Color: 393
Size: 312 Color: 39

Bin 189: 194 of cap free
Amount of items: 2
Items: 
Size: 9378 Color: 424
Size: 6844 Color: 399

Bin 190: 199 of cap free
Amount of items: 3
Items: 
Size: 10455 Color: 443
Size: 5506 Color: 372
Size: 256 Color: 17

Bin 191: 211 of cap free
Amount of items: 6
Items: 
Size: 8226 Color: 411
Size: 2572 Color: 282
Size: 2361 Color: 274
Size: 2342 Color: 271
Size: 352 Color: 52
Size: 352 Color: 51

Bin 192: 224 of cap free
Amount of items: 2
Items: 
Size: 9352 Color: 423
Size: 6840 Color: 398

Bin 193: 258 of cap free
Amount of items: 7
Items: 
Size: 8218 Color: 409
Size: 1564 Color: 218
Size: 1561 Color: 217
Size: 1507 Color: 212
Size: 1488 Color: 211
Size: 1462 Color: 208
Size: 358 Color: 55

Bin 194: 265 of cap free
Amount of items: 2
Items: 
Size: 9314 Color: 422
Size: 6837 Color: 397

Bin 195: 272 of cap free
Amount of items: 2
Items: 
Size: 9308 Color: 421
Size: 6836 Color: 396

Bin 196: 286 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 417
Size: 7306 Color: 402
Size: 320 Color: 41

Bin 197: 292 of cap free
Amount of items: 6
Items: 
Size: 8220 Color: 410
Size: 2336 Color: 270
Size: 2004 Color: 250
Size: 1620 Color: 224
Size: 1592 Color: 222
Size: 352 Color: 53

Bin 198: 490 of cap free
Amount of items: 27
Items: 
Size: 736 Color: 148
Size: 736 Color: 147
Size: 736 Color: 146
Size: 728 Color: 145
Size: 712 Color: 144
Size: 704 Color: 142
Size: 688 Color: 139
Size: 688 Color: 138
Size: 684 Color: 136
Size: 672 Color: 133
Size: 672 Color: 132
Size: 664 Color: 131
Size: 626 Color: 128
Size: 616 Color: 126
Size: 608 Color: 123
Size: 512 Color: 100
Size: 512 Color: 99
Size: 508 Color: 98
Size: 480 Color: 94
Size: 476 Color: 92
Size: 472 Color: 91
Size: 464 Color: 89
Size: 464 Color: 88
Size: 464 Color: 87
Size: 448 Color: 86
Size: 432 Color: 84
Size: 424 Color: 82

Bin 199: 10640 of cap free
Amount of items: 10
Items: 
Size: 608 Color: 122
Size: 608 Color: 121
Size: 600 Color: 116
Size: 596 Color: 114
Size: 592 Color: 113
Size: 592 Color: 112
Size: 576 Color: 111
Size: 544 Color: 107
Size: 544 Color: 106
Size: 516 Color: 101

Total size: 3250368
Total free space: 16416

