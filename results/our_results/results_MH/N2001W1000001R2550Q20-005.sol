Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 366858 Color: 3
Size: 341158 Color: 1
Size: 291985 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 381405 Color: 17
Size: 317507 Color: 2
Size: 301089 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 433786 Color: 14
Size: 304809 Color: 19
Size: 261406 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 386593 Color: 14
Size: 322332 Color: 12
Size: 291076 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 496142 Color: 6
Size: 253856 Color: 7
Size: 250003 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 367724 Color: 19
Size: 341060 Color: 11
Size: 291217 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 338128 Color: 13
Size: 331684 Color: 4
Size: 330189 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 358465 Color: 17
Size: 333416 Color: 16
Size: 308120 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 361266 Color: 2
Size: 355298 Color: 2
Size: 283437 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 368860 Color: 11
Size: 324020 Color: 13
Size: 307121 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 373363 Color: 12
Size: 325955 Color: 3
Size: 300683 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 373646 Color: 3
Size: 367518 Color: 8
Size: 258837 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 375139 Color: 3
Size: 332668 Color: 1
Size: 292194 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 374797 Color: 13
Size: 343967 Color: 6
Size: 281237 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 374992 Color: 17
Size: 313734 Color: 16
Size: 311275 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 374996 Color: 2
Size: 357918 Color: 6
Size: 267087 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 375734 Color: 5
Size: 314069 Color: 4
Size: 310198 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 376500 Color: 1
Size: 361530 Color: 11
Size: 261971 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 376509 Color: 5
Size: 324755 Color: 12
Size: 298737 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 376538 Color: 14
Size: 322868 Color: 18
Size: 300595 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 376409 Color: 4
Size: 358655 Color: 15
Size: 264937 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 376917 Color: 7
Size: 322127 Color: 18
Size: 300957 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 376955 Color: 12
Size: 320067 Color: 7
Size: 302979 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 378140 Color: 16
Size: 317300 Color: 6
Size: 304561 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 378148 Color: 1
Size: 349613 Color: 13
Size: 272240 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 378301 Color: 19
Size: 345607 Color: 19
Size: 276093 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 379427 Color: 0
Size: 346056 Color: 13
Size: 274518 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 379289 Color: 13
Size: 312859 Color: 10
Size: 307853 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 379961 Color: 17
Size: 358832 Color: 17
Size: 261208 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 380002 Color: 11
Size: 334254 Color: 9
Size: 285745 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 380129 Color: 18
Size: 366821 Color: 9
Size: 253051 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 380422 Color: 19
Size: 332265 Color: 1
Size: 287314 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 380429 Color: 12
Size: 316105 Color: 2
Size: 303467 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 380982 Color: 7
Size: 309716 Color: 19
Size: 309303 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 381043 Color: 18
Size: 354806 Color: 6
Size: 264152 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 381174 Color: 19
Size: 327897 Color: 13
Size: 290930 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381244 Color: 19
Size: 335641 Color: 16
Size: 283116 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 381393 Color: 7
Size: 325319 Color: 18
Size: 293289 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 381584 Color: 16
Size: 357642 Color: 8
Size: 260775 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 382096 Color: 5
Size: 362054 Color: 4
Size: 255851 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 384250 Color: 14
Size: 334022 Color: 5
Size: 281729 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 384560 Color: 5
Size: 312666 Color: 15
Size: 302775 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 384575 Color: 14
Size: 335371 Color: 15
Size: 280055 Color: 11

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 385011 Color: 4
Size: 358507 Color: 8
Size: 256483 Color: 16

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 384778 Color: 5
Size: 338251 Color: 19
Size: 276972 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 384950 Color: 2
Size: 355802 Color: 15
Size: 259249 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 385073 Color: 16
Size: 333198 Color: 1
Size: 281730 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 385547 Color: 5
Size: 313934 Color: 0
Size: 300520 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 386910 Color: 15
Size: 351934 Color: 11
Size: 261157 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 387035 Color: 11
Size: 310779 Color: 19
Size: 302187 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 386762 Color: 16
Size: 314512 Color: 2
Size: 298727 Color: 15

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 387087 Color: 1
Size: 324189 Color: 4
Size: 288725 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 387358 Color: 1
Size: 315041 Color: 6
Size: 297602 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 387565 Color: 6
Size: 348940 Color: 19
Size: 263496 Color: 12

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 387866 Color: 0
Size: 307214 Color: 7
Size: 304921 Color: 13

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 388130 Color: 19
Size: 320743 Color: 13
Size: 291128 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 388229 Color: 17
Size: 314068 Color: 5
Size: 297704 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 388675 Color: 6
Size: 339204 Color: 12
Size: 272122 Color: 11

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 388782 Color: 15
Size: 341090 Color: 11
Size: 270129 Color: 19

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 389094 Color: 9
Size: 344365 Color: 0
Size: 266542 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 388644 Color: 19
Size: 353804 Color: 19
Size: 257553 Color: 11

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 389588 Color: 2
Size: 313707 Color: 16
Size: 296706 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 390420 Color: 5
Size: 348032 Color: 6
Size: 261549 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 390707 Color: 3
Size: 305264 Color: 14
Size: 304030 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 390762 Color: 3
Size: 338218 Color: 3
Size: 271021 Color: 15

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 391674 Color: 6
Size: 357541 Color: 9
Size: 250786 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 391157 Color: 12
Size: 329394 Color: 14
Size: 279450 Color: 17

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 392557 Color: 6
Size: 319847 Color: 16
Size: 287597 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 392269 Color: 8
Size: 352285 Color: 16
Size: 255447 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 393133 Color: 4
Size: 305381 Color: 19
Size: 301487 Color: 13

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 393575 Color: 3
Size: 326363 Color: 13
Size: 280063 Color: 6

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 393867 Color: 8
Size: 313213 Color: 8
Size: 292921 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 396240 Color: 6
Size: 352100 Color: 19
Size: 251661 Color: 11

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 396305 Color: 14
Size: 322973 Color: 16
Size: 280723 Color: 14

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 397278 Color: 10
Size: 351676 Color: 3
Size: 251047 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 397798 Color: 3
Size: 343514 Color: 19
Size: 258689 Color: 14

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 397817 Color: 12
Size: 349861 Color: 5
Size: 252323 Color: 18

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 398335 Color: 12
Size: 351393 Color: 10
Size: 250273 Color: 19

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 398335 Color: 8
Size: 329699 Color: 6
Size: 271967 Color: 14

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 398538 Color: 3
Size: 351250 Color: 8
Size: 250213 Color: 11

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 398633 Color: 19
Size: 325090 Color: 17
Size: 276278 Color: 9

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 399334 Color: 16
Size: 330390 Color: 5
Size: 270277 Color: 18

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 399538 Color: 9
Size: 319689 Color: 9
Size: 280774 Color: 12

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 399668 Color: 12
Size: 346497 Color: 7
Size: 253836 Color: 10

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 399690 Color: 15
Size: 314448 Color: 7
Size: 285863 Color: 13

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 400199 Color: 11
Size: 329501 Color: 17
Size: 270301 Color: 11

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 400838 Color: 7
Size: 333976 Color: 2
Size: 265187 Color: 8

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 401059 Color: 1
Size: 348429 Color: 12
Size: 250513 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 401557 Color: 9
Size: 345099 Color: 11
Size: 253345 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 401629 Color: 2
Size: 310812 Color: 10
Size: 287560 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 401683 Color: 7
Size: 345002 Color: 3
Size: 253316 Color: 7

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 401686 Color: 19
Size: 325510 Color: 17
Size: 272805 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 402571 Color: 8
Size: 306835 Color: 8
Size: 290595 Color: 13

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 402650 Color: 1
Size: 341662 Color: 18
Size: 255689 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 403134 Color: 0
Size: 329925 Color: 10
Size: 266942 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 403644 Color: 17
Size: 319695 Color: 9
Size: 276662 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 404069 Color: 18
Size: 302541 Color: 3
Size: 293391 Color: 17

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 404161 Color: 9
Size: 340434 Color: 12
Size: 255406 Color: 2

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 404700 Color: 6
Size: 324167 Color: 3
Size: 271134 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 404688 Color: 11
Size: 337553 Color: 7
Size: 257760 Color: 17

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 405049 Color: 11
Size: 332918 Color: 19
Size: 262034 Color: 13

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 405058 Color: 9
Size: 324194 Color: 2
Size: 270749 Color: 10

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 405559 Color: 8
Size: 311392 Color: 8
Size: 283050 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 405733 Color: 16
Size: 327962 Color: 10
Size: 266306 Color: 9

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 405736 Color: 12
Size: 316606 Color: 19
Size: 277659 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 405957 Color: 2
Size: 309918 Color: 19
Size: 284126 Color: 5

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 406144 Color: 9
Size: 342821 Color: 7
Size: 251036 Color: 7

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 406214 Color: 2
Size: 318173 Color: 16
Size: 275614 Color: 15

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 406851 Color: 6
Size: 340656 Color: 4
Size: 252494 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 406662 Color: 3
Size: 302907 Color: 13
Size: 290432 Color: 5

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 406922 Color: 3
Size: 325718 Color: 5
Size: 267361 Color: 13

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 406939 Color: 19
Size: 322468 Color: 16
Size: 270594 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 407226 Color: 17
Size: 300913 Color: 9
Size: 291862 Color: 5

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 407308 Color: 9
Size: 321750 Color: 6
Size: 270943 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 407319 Color: 17
Size: 341400 Color: 12
Size: 251282 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 407458 Color: 12
Size: 308161 Color: 7
Size: 284382 Color: 14

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 407513 Color: 15
Size: 338482 Color: 12
Size: 254006 Color: 19

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 407528 Color: 19
Size: 298914 Color: 9
Size: 293559 Color: 15

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 408873 Color: 0
Size: 331128 Color: 16
Size: 260000 Color: 14

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 408877 Color: 18
Size: 306462 Color: 2
Size: 284662 Color: 14

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 408896 Color: 9
Size: 322974 Color: 1
Size: 268131 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 409170 Color: 15
Size: 299348 Color: 15
Size: 291483 Color: 17

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 409410 Color: 15
Size: 315528 Color: 18
Size: 275063 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410328 Color: 10
Size: 315778 Color: 11
Size: 273895 Color: 10

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 410464 Color: 16
Size: 318952 Color: 19
Size: 270585 Color: 13

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 410920 Color: 8
Size: 294616 Color: 1
Size: 294465 Color: 13

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 411045 Color: 0
Size: 296255 Color: 8
Size: 292701 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 411305 Color: 2
Size: 305857 Color: 3
Size: 282839 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 412103 Color: 13
Size: 320192 Color: 18
Size: 267706 Color: 10

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 412242 Color: 6
Size: 318283 Color: 1
Size: 269476 Color: 18

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 412422 Color: 13
Size: 318102 Color: 8
Size: 269477 Color: 5

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 412541 Color: 0
Size: 330319 Color: 14
Size: 257141 Color: 19

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 413056 Color: 6
Size: 311338 Color: 15
Size: 275607 Color: 7

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 413835 Color: 6
Size: 302388 Color: 11
Size: 283778 Color: 13

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 412590 Color: 10
Size: 317600 Color: 18
Size: 269811 Color: 18

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 413037 Color: 14
Size: 303350 Color: 12
Size: 283614 Color: 16

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 413199 Color: 0
Size: 302283 Color: 3
Size: 284519 Color: 14

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 413899 Color: 6
Size: 317267 Color: 14
Size: 268835 Color: 7

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 413515 Color: 2
Size: 335526 Color: 3
Size: 250960 Color: 14

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 413657 Color: 2
Size: 324535 Color: 3
Size: 261809 Color: 7

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 413793 Color: 12
Size: 308898 Color: 12
Size: 277310 Color: 18

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 414001 Color: 5
Size: 325050 Color: 8
Size: 260950 Color: 11

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 414453 Color: 17
Size: 310932 Color: 17
Size: 274616 Color: 10

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 414586 Color: 0
Size: 318488 Color: 5
Size: 266927 Color: 6

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 414616 Color: 7
Size: 304670 Color: 7
Size: 280715 Color: 4

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 415245 Color: 7
Size: 314889 Color: 15
Size: 269867 Color: 7

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 415262 Color: 19
Size: 322035 Color: 1
Size: 262704 Color: 16

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 415382 Color: 19
Size: 298283 Color: 9
Size: 286336 Color: 10

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 415394 Color: 13
Size: 299410 Color: 3
Size: 285197 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 415877 Color: 15
Size: 316880 Color: 11
Size: 267244 Color: 16

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 416120 Color: 16
Size: 324641 Color: 9
Size: 259240 Color: 10

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 416241 Color: 0
Size: 316080 Color: 13
Size: 267680 Color: 18

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 416255 Color: 18
Size: 294727 Color: 11
Size: 289019 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 416400 Color: 7
Size: 325615 Color: 1
Size: 257986 Color: 18

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 416782 Color: 9
Size: 318691 Color: 19
Size: 264528 Color: 15

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 417210 Color: 0
Size: 306903 Color: 17
Size: 275888 Color: 16

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 417265 Color: 17
Size: 331581 Color: 4
Size: 251155 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 417435 Color: 10
Size: 294805 Color: 3
Size: 287761 Color: 15

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 417688 Color: 18
Size: 308457 Color: 0
Size: 273856 Color: 18

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 417758 Color: 19
Size: 295926 Color: 2
Size: 286317 Color: 7

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 418145 Color: 1
Size: 320038 Color: 15
Size: 261818 Color: 12

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 418733 Color: 9
Size: 297425 Color: 0
Size: 283843 Color: 16

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 419085 Color: 12
Size: 293483 Color: 0
Size: 287433 Color: 14

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 419232 Color: 16
Size: 324943 Color: 5
Size: 255826 Color: 11

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 419451 Color: 5
Size: 303016 Color: 6
Size: 277534 Color: 19

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 419722 Color: 3
Size: 316166 Color: 16
Size: 264113 Color: 18

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 419882 Color: 13
Size: 320355 Color: 10
Size: 259764 Color: 18

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 419925 Color: 7
Size: 292658 Color: 8
Size: 287418 Color: 2

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 419969 Color: 11
Size: 316562 Color: 0
Size: 263470 Color: 13

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 419948 Color: 19
Size: 307403 Color: 14
Size: 272650 Color: 14

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 420067 Color: 8
Size: 325552 Color: 1
Size: 254382 Color: 3

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 420319 Color: 3
Size: 308930 Color: 0
Size: 270752 Color: 10

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 420326 Color: 12
Size: 299725 Color: 17
Size: 279950 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 420709 Color: 1
Size: 290396 Color: 11
Size: 288896 Color: 10

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 420894 Color: 6
Size: 310619 Color: 9
Size: 268488 Color: 5

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 420787 Color: 11
Size: 326360 Color: 2
Size: 252854 Color: 10

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 420850 Color: 13
Size: 313392 Color: 9
Size: 265759 Color: 19

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 420932 Color: 5
Size: 324121 Color: 4
Size: 254948 Color: 15

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 420978 Color: 9
Size: 306366 Color: 7
Size: 272657 Color: 7

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 422233 Color: 15
Size: 306364 Color: 1
Size: 271404 Color: 11

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 422407 Color: 5
Size: 293455 Color: 14
Size: 284139 Color: 7

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 423234 Color: 18
Size: 303417 Color: 10
Size: 273350 Color: 1

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 423596 Color: 5
Size: 312327 Color: 2
Size: 264078 Color: 10

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 424141 Color: 12
Size: 299758 Color: 0
Size: 276102 Color: 16

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 424192 Color: 10
Size: 291572 Color: 2
Size: 284237 Color: 9

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 424262 Color: 4
Size: 300720 Color: 12
Size: 275019 Color: 14

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 424647 Color: 15
Size: 324662 Color: 16
Size: 250692 Color: 6

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 424850 Color: 15
Size: 292511 Color: 12
Size: 282640 Color: 15

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 425398 Color: 16
Size: 319714 Color: 17
Size: 254889 Color: 17

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 425636 Color: 4
Size: 300710 Color: 17
Size: 273655 Color: 15

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 425689 Color: 15
Size: 297227 Color: 12
Size: 277085 Color: 5

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 426152 Color: 9
Size: 298011 Color: 0
Size: 275838 Color: 16

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 426157 Color: 18
Size: 303722 Color: 12
Size: 270122 Color: 18

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 426189 Color: 2
Size: 291791 Color: 8
Size: 282021 Color: 16

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 426672 Color: 1
Size: 289505 Color: 7
Size: 283824 Color: 6

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 426865 Color: 3
Size: 291373 Color: 14
Size: 281763 Color: 19

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 427049 Color: 13
Size: 307457 Color: 14
Size: 265495 Color: 2

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 427087 Color: 1
Size: 312054 Color: 18
Size: 260860 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 427881 Color: 4
Size: 311524 Color: 14
Size: 260596 Color: 2

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 428136 Color: 6
Size: 298898 Color: 8
Size: 272967 Color: 10

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 427974 Color: 18
Size: 310678 Color: 5
Size: 261349 Color: 4

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 428047 Color: 8
Size: 290615 Color: 3
Size: 281339 Color: 14

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 428213 Color: 9
Size: 294752 Color: 10
Size: 277036 Color: 12

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 428369 Color: 11
Size: 320669 Color: 19
Size: 250963 Color: 18

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 428680 Color: 13
Size: 298714 Color: 17
Size: 272607 Color: 2

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 428691 Color: 11
Size: 300679 Color: 13
Size: 270631 Color: 13

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 428750 Color: 13
Size: 315011 Color: 16
Size: 256240 Color: 18

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 428985 Color: 9
Size: 304134 Color: 16
Size: 266882 Color: 19

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 429094 Color: 17
Size: 309780 Color: 12
Size: 261127 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 429246 Color: 0
Size: 296614 Color: 1
Size: 274141 Color: 14

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 430290 Color: 0
Size: 299015 Color: 1
Size: 270696 Color: 2

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 430537 Color: 11
Size: 307275 Color: 9
Size: 262189 Color: 7

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 430702 Color: 9
Size: 294063 Color: 13
Size: 275236 Color: 15

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 430944 Color: 19
Size: 289698 Color: 0
Size: 279359 Color: 19

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 431197 Color: 5
Size: 286989 Color: 15
Size: 281815 Color: 13

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 432439 Color: 6
Size: 299607 Color: 4
Size: 267955 Color: 5

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 432293 Color: 1
Size: 308262 Color: 9
Size: 259446 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 433087 Color: 13
Size: 300088 Color: 9
Size: 266826 Color: 11

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 433405 Color: 2
Size: 291456 Color: 8
Size: 275140 Color: 8

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 433847 Color: 10
Size: 313409 Color: 10
Size: 252745 Color: 11

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 434336 Color: 0
Size: 309428 Color: 13
Size: 256237 Color: 5

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 435380 Color: 13
Size: 287604 Color: 12
Size: 277017 Color: 5

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 435418 Color: 8
Size: 298379 Color: 19
Size: 266204 Color: 17

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 435572 Color: 13
Size: 294674 Color: 18
Size: 269755 Color: 14

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 435762 Color: 18
Size: 302938 Color: 19
Size: 261301 Color: 16

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 436590 Color: 7
Size: 302896 Color: 15
Size: 260515 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 436802 Color: 2
Size: 309537 Color: 5
Size: 253662 Color: 5

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 436998 Color: 8
Size: 310867 Color: 8
Size: 252136 Color: 16

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 437901 Color: 9
Size: 302297 Color: 10
Size: 259803 Color: 10

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 438066 Color: 5
Size: 311583 Color: 19
Size: 250352 Color: 19

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 438127 Color: 4
Size: 291638 Color: 12
Size: 270236 Color: 19

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 438480 Color: 1
Size: 284593 Color: 10
Size: 276928 Color: 10

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 438732 Color: 7
Size: 286330 Color: 4
Size: 274939 Color: 17

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 438829 Color: 14
Size: 281869 Color: 10
Size: 279303 Color: 3

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 438836 Color: 15
Size: 286155 Color: 18
Size: 275010 Color: 19

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 439148 Color: 9
Size: 310590 Color: 11
Size: 250263 Color: 14

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 439291 Color: 17
Size: 285560 Color: 15
Size: 275150 Color: 9

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 440042 Color: 10
Size: 300498 Color: 14
Size: 259461 Color: 16

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 440083 Color: 16
Size: 289671 Color: 9
Size: 270247 Color: 12

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 440152 Color: 15
Size: 301633 Color: 0
Size: 258216 Color: 9

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 440733 Color: 3
Size: 298761 Color: 11
Size: 260507 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 440998 Color: 1
Size: 301111 Color: 3
Size: 257892 Color: 5

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 441319 Color: 6
Size: 304295 Color: 13
Size: 254387 Color: 19

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 441288 Color: 16
Size: 280912 Color: 11
Size: 277801 Color: 14

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 441491 Color: 14
Size: 295429 Color: 9
Size: 263081 Color: 19

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 442005 Color: 0
Size: 306549 Color: 15
Size: 251447 Color: 11

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 442084 Color: 3
Size: 303443 Color: 0
Size: 254474 Color: 13

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 442554 Color: 12
Size: 302383 Color: 16
Size: 255064 Color: 16

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 443088 Color: 6
Size: 284700 Color: 0
Size: 272213 Color: 4

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 442894 Color: 10
Size: 291824 Color: 13
Size: 265283 Color: 7

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 442960 Color: 13
Size: 296135 Color: 1
Size: 260906 Color: 7

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 443675 Color: 15
Size: 291503 Color: 7
Size: 264823 Color: 5

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 444326 Color: 2
Size: 280250 Color: 14
Size: 275425 Color: 11

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 444446 Color: 7
Size: 305126 Color: 4
Size: 250429 Color: 9

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 444484 Color: 19
Size: 287980 Color: 0
Size: 267537 Color: 12

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 444687 Color: 15
Size: 298400 Color: 0
Size: 256914 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 444750 Color: 14
Size: 301952 Color: 14
Size: 253299 Color: 6

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 445213 Color: 13
Size: 299712 Color: 12
Size: 255076 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 445217 Color: 1
Size: 292410 Color: 5
Size: 262374 Color: 10

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 445319 Color: 12
Size: 296081 Color: 8
Size: 258601 Color: 10

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 445545 Color: 15
Size: 296816 Color: 19
Size: 257640 Color: 11

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 445997 Color: 2
Size: 296965 Color: 14
Size: 257039 Color: 4

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 446454 Color: 13
Size: 293253 Color: 6
Size: 260294 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 446522 Color: 0
Size: 290817 Color: 18
Size: 262662 Color: 19

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 446552 Color: 4
Size: 291549 Color: 13
Size: 261900 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 446723 Color: 3
Size: 296613 Color: 15
Size: 256665 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 447266 Color: 18
Size: 281842 Color: 10
Size: 270893 Color: 7

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 448476 Color: 2
Size: 301445 Color: 17
Size: 250080 Color: 15

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 449101 Color: 14
Size: 283470 Color: 16
Size: 267430 Color: 16

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 449165 Color: 10
Size: 280087 Color: 4
Size: 270749 Color: 5

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 449387 Color: 19
Size: 298014 Color: 11
Size: 252600 Color: 7

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 449984 Color: 1
Size: 296614 Color: 6
Size: 253403 Color: 14

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 450124 Color: 8
Size: 284829 Color: 1
Size: 265048 Color: 12

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 450275 Color: 2
Size: 298664 Color: 11
Size: 251062 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 450489 Color: 3
Size: 278350 Color: 5
Size: 271162 Color: 16

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 450728 Color: 13
Size: 295694 Color: 18
Size: 253579 Color: 19

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 451019 Color: 7
Size: 291042 Color: 17
Size: 257940 Color: 4

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 451044 Color: 11
Size: 276446 Color: 4
Size: 272511 Color: 16

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 451158 Color: 13
Size: 298120 Color: 13
Size: 250723 Color: 15

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 451434 Color: 0
Size: 275338 Color: 4
Size: 273229 Color: 11

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 452128 Color: 12
Size: 294784 Color: 17
Size: 253089 Color: 18

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 452775 Color: 6
Size: 293236 Color: 9
Size: 253990 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 452327 Color: 4
Size: 280451 Color: 10
Size: 267223 Color: 15

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 452337 Color: 16
Size: 277290 Color: 10
Size: 270374 Color: 9

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 453051 Color: 16
Size: 289454 Color: 12
Size: 257496 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 453952 Color: 8
Size: 289680 Color: 12
Size: 256369 Color: 13

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 455329 Color: 13
Size: 277916 Color: 7
Size: 266756 Color: 16

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 455689 Color: 13
Size: 292007 Color: 0
Size: 252305 Color: 5

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 455868 Color: 18
Size: 282263 Color: 4
Size: 261870 Color: 17

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 457009 Color: 12
Size: 287448 Color: 14
Size: 255544 Color: 19

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 457120 Color: 0
Size: 283108 Color: 10
Size: 259773 Color: 10

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 457184 Color: 15
Size: 285964 Color: 8
Size: 256853 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 458049 Color: 8
Size: 281835 Color: 2
Size: 260117 Color: 8

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 458107 Color: 8
Size: 276472 Color: 9
Size: 265422 Color: 13

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 459280 Color: 6
Size: 277003 Color: 9
Size: 263718 Color: 5

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 459241 Color: 14
Size: 273284 Color: 10
Size: 267476 Color: 5

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 459710 Color: 13
Size: 284425 Color: 8
Size: 255866 Color: 18

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 460094 Color: 18
Size: 270283 Color: 13
Size: 269624 Color: 10

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 460366 Color: 3
Size: 287653 Color: 10
Size: 251982 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 460675 Color: 12
Size: 281245 Color: 1
Size: 258081 Color: 7

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 461431 Color: 4
Size: 276817 Color: 11
Size: 261753 Color: 3

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 462644 Color: 12
Size: 284035 Color: 11
Size: 253322 Color: 19

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 462697 Color: 0
Size: 283910 Color: 19
Size: 253394 Color: 17

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 462761 Color: 14
Size: 278223 Color: 1
Size: 259017 Color: 5

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 462912 Color: 18
Size: 270313 Color: 8
Size: 266776 Color: 7

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 463569 Color: 17
Size: 277253 Color: 1
Size: 259179 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 464061 Color: 12
Size: 269816 Color: 4
Size: 266124 Color: 18

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 464716 Color: 3
Size: 284223 Color: 9
Size: 251062 Color: 8

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 464719 Color: 19
Size: 268590 Color: 17
Size: 266692 Color: 10

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 464945 Color: 16
Size: 270174 Color: 5
Size: 264882 Color: 9

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 465178 Color: 4
Size: 268734 Color: 13
Size: 266089 Color: 10

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 465238 Color: 10
Size: 278869 Color: 8
Size: 255894 Color: 3

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 465280 Color: 10
Size: 272784 Color: 14
Size: 261937 Color: 19

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 465473 Color: 2
Size: 273601 Color: 19
Size: 260927 Color: 3

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 465730 Color: 4
Size: 278944 Color: 11
Size: 255327 Color: 6

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 466312 Color: 5
Size: 282293 Color: 5
Size: 251396 Color: 16

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 466450 Color: 14
Size: 281445 Color: 19
Size: 252106 Color: 18

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 466567 Color: 1
Size: 276812 Color: 12
Size: 256622 Color: 9

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 466797 Color: 2
Size: 269586 Color: 18
Size: 263618 Color: 5

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 467477 Color: 18
Size: 269967 Color: 15
Size: 262557 Color: 9

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 468284 Color: 8
Size: 268720 Color: 12
Size: 262997 Color: 5

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 468618 Color: 11
Size: 276066 Color: 0
Size: 255317 Color: 2

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 469110 Color: 4
Size: 278514 Color: 18
Size: 252377 Color: 14

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 469506 Color: 7
Size: 274073 Color: 5
Size: 256422 Color: 11

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 469779 Color: 3
Size: 279972 Color: 18
Size: 250250 Color: 18

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 469785 Color: 19
Size: 279676 Color: 8
Size: 250540 Color: 12

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 470247 Color: 8
Size: 279049 Color: 0
Size: 250705 Color: 7

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 470409 Color: 18
Size: 274837 Color: 7
Size: 254755 Color: 7

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 470416 Color: 19
Size: 275053 Color: 4
Size: 254532 Color: 15

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 470730 Color: 16
Size: 278334 Color: 12
Size: 250937 Color: 6

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 470740 Color: 13
Size: 269304 Color: 12
Size: 259957 Color: 7

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 470752 Color: 12
Size: 278945 Color: 0
Size: 250304 Color: 16

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 470918 Color: 3
Size: 276544 Color: 18
Size: 252539 Color: 10

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 470979 Color: 14
Size: 273577 Color: 1
Size: 255445 Color: 11

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 471425 Color: 7
Size: 276290 Color: 10
Size: 252286 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 472132 Color: 18
Size: 272769 Color: 10
Size: 255100 Color: 10

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 472256 Color: 10
Size: 275464 Color: 10
Size: 252281 Color: 2

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 472266 Color: 4
Size: 274977 Color: 17
Size: 252758 Color: 17

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 473009 Color: 17
Size: 272130 Color: 4
Size: 254862 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 473851 Color: 8
Size: 269302 Color: 16
Size: 256848 Color: 5

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 474425 Color: 7
Size: 274659 Color: 15
Size: 250917 Color: 4

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 474641 Color: 15
Size: 272644 Color: 2
Size: 252716 Color: 12

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 474824 Color: 16
Size: 270517 Color: 12
Size: 254660 Color: 10

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 475057 Color: 17
Size: 274911 Color: 12
Size: 250033 Color: 16

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 475571 Color: 6
Size: 262365 Color: 1
Size: 262065 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 475747 Color: 2
Size: 263420 Color: 8
Size: 260834 Color: 3

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 475802 Color: 10
Size: 265157 Color: 8
Size: 259042 Color: 3

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 475875 Color: 16
Size: 266501 Color: 5
Size: 257625 Color: 19

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 475898 Color: 8
Size: 266168 Color: 19
Size: 257935 Color: 12

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 476817 Color: 15
Size: 265796 Color: 16
Size: 257388 Color: 6

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 476828 Color: 3
Size: 271627 Color: 1
Size: 251546 Color: 18

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 477460 Color: 3
Size: 269945 Color: 1
Size: 252596 Color: 4

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 477577 Color: 16
Size: 271353 Color: 3
Size: 251071 Color: 14

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 478153 Color: 16
Size: 265302 Color: 7
Size: 256546 Color: 17

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 478379 Color: 7
Size: 266619 Color: 6
Size: 255003 Color: 4

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 478491 Color: 14
Size: 263726 Color: 16
Size: 257784 Color: 14

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 478748 Color: 4
Size: 270185 Color: 14
Size: 251068 Color: 18

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 479249 Color: 17
Size: 270302 Color: 10
Size: 250450 Color: 9

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 479732 Color: 14
Size: 265821 Color: 12
Size: 254448 Color: 8

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 479779 Color: 13
Size: 260739 Color: 11
Size: 259483 Color: 6

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 479808 Color: 11
Size: 263347 Color: 19
Size: 256846 Color: 10

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 480421 Color: 12
Size: 263124 Color: 15
Size: 256456 Color: 11

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 481205 Color: 15
Size: 260790 Color: 14
Size: 258006 Color: 8

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 482117 Color: 19
Size: 264358 Color: 15
Size: 253526 Color: 3

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 482906 Color: 11
Size: 262613 Color: 4
Size: 254482 Color: 8

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 483261 Color: 6
Size: 264816 Color: 0
Size: 251924 Color: 16

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 483102 Color: 15
Size: 262239 Color: 16
Size: 254660 Color: 13

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 484000 Color: 9
Size: 265953 Color: 11
Size: 250048 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 484626 Color: 11
Size: 261949 Color: 3
Size: 253426 Color: 16

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 485166 Color: 0
Size: 263550 Color: 1
Size: 251285 Color: 16

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 485991 Color: 5
Size: 257458 Color: 10
Size: 256552 Color: 13

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 486219 Color: 0
Size: 260462 Color: 19
Size: 253320 Color: 4

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 486531 Color: 5
Size: 260578 Color: 16
Size: 252892 Color: 15

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 487063 Color: 1
Size: 257256 Color: 5
Size: 255682 Color: 14

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 487338 Color: 4
Size: 256715 Color: 4
Size: 255948 Color: 15

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 487694 Color: 6
Size: 256527 Color: 13
Size: 255780 Color: 5

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 487822 Color: 14
Size: 261120 Color: 12
Size: 251059 Color: 10

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 487843 Color: 3
Size: 259583 Color: 4
Size: 252575 Color: 15

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 488943 Color: 13
Size: 259083 Color: 0
Size: 251975 Color: 19

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 489116 Color: 19
Size: 260116 Color: 2
Size: 250769 Color: 10

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 489490 Color: 5
Size: 257577 Color: 8
Size: 252934 Color: 14

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 490001 Color: 18
Size: 256085 Color: 18
Size: 253915 Color: 7

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 490292 Color: 9
Size: 258674 Color: 15
Size: 251035 Color: 12

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 490903 Color: 6
Size: 258955 Color: 4
Size: 250143 Color: 2

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 490534 Color: 19
Size: 258594 Color: 3
Size: 250873 Color: 2

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 490541 Color: 7
Size: 259225 Color: 10
Size: 250235 Color: 2

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 490995 Color: 1
Size: 256196 Color: 8
Size: 252810 Color: 5

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 491040 Color: 12
Size: 257035 Color: 1
Size: 251926 Color: 15

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 491445 Color: 18
Size: 254576 Color: 9
Size: 253980 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 492422 Color: 0
Size: 256963 Color: 0
Size: 250616 Color: 7

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 492434 Color: 0
Size: 256072 Color: 14
Size: 251495 Color: 9

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 492778 Color: 9
Size: 254198 Color: 15
Size: 253025 Color: 1

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 493392 Color: 0
Size: 253370 Color: 3
Size: 253239 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 493414 Color: 1
Size: 256053 Color: 17
Size: 250534 Color: 2

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 493468 Color: 19
Size: 256097 Color: 8
Size: 250436 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 494111 Color: 9
Size: 255272 Color: 7
Size: 250618 Color: 16

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 494451 Color: 13
Size: 252805 Color: 17
Size: 252745 Color: 4

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 495084 Color: 14
Size: 253701 Color: 10
Size: 251216 Color: 17

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 496586 Color: 10
Size: 253259 Color: 14
Size: 250156 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 497862 Color: 2
Size: 251544 Color: 10
Size: 250595 Color: 11

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 359872 Color: 11
Size: 334457 Color: 16
Size: 305671 Color: 13

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 361506 Color: 13
Size: 344368 Color: 8
Size: 294126 Color: 3

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 365759 Color: 16
Size: 362797 Color: 7
Size: 271444 Color: 14

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 369686 Color: 0
Size: 364484 Color: 2
Size: 265830 Color: 15

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 371279 Color: 19
Size: 315300 Color: 17
Size: 313421 Color: 9

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 371508 Color: 10
Size: 319012 Color: 6
Size: 309480 Color: 9

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 372339 Color: 15
Size: 360241 Color: 19
Size: 267420 Color: 2

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 372439 Color: 18
Size: 354603 Color: 5
Size: 272958 Color: 18

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 372327 Color: 13
Size: 322054 Color: 10
Size: 305619 Color: 18

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 373395 Color: 19
Size: 369290 Color: 10
Size: 257315 Color: 5

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 374733 Color: 16
Size: 362546 Color: 0
Size: 262721 Color: 16

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 375189 Color: 19
Size: 342112 Color: 19
Size: 282699 Color: 15

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 376008 Color: 15
Size: 339817 Color: 18
Size: 284175 Color: 12

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 376075 Color: 11
Size: 341558 Color: 18
Size: 282367 Color: 13

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 376289 Color: 9
Size: 338125 Color: 10
Size: 285586 Color: 17

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 377292 Color: 0
Size: 313075 Color: 6
Size: 309633 Color: 2

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 377932 Color: 10
Size: 324339 Color: 5
Size: 297729 Color: 10

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 379645 Color: 10
Size: 360628 Color: 13
Size: 259727 Color: 5

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 380149 Color: 1
Size: 351518 Color: 2
Size: 268333 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 380372 Color: 11
Size: 319575 Color: 5
Size: 300053 Color: 7

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 380755 Color: 11
Size: 321040 Color: 9
Size: 298205 Color: 1

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 383528 Color: 15
Size: 344954 Color: 11
Size: 271518 Color: 9

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 383760 Color: 18
Size: 343951 Color: 14
Size: 272289 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 384003 Color: 16
Size: 359713 Color: 12
Size: 256284 Color: 5

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 384052 Color: 18
Size: 353720 Color: 10
Size: 262228 Color: 6

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 385862 Color: 0
Size: 323972 Color: 1
Size: 290166 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 386911 Color: 11
Size: 312493 Color: 12
Size: 300596 Color: 8

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 391153 Color: 7
Size: 337817 Color: 10
Size: 271030 Color: 9

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 393926 Color: 7
Size: 334067 Color: 3
Size: 272007 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 394253 Color: 7
Size: 329631 Color: 13
Size: 276116 Color: 5

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 394544 Color: 16
Size: 350400 Color: 14
Size: 255056 Color: 18

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 395023 Color: 4
Size: 324598 Color: 8
Size: 280379 Color: 12

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 395503 Color: 1
Size: 353853 Color: 13
Size: 250644 Color: 14

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 398202 Color: 9
Size: 340129 Color: 12
Size: 261669 Color: 11

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 398848 Color: 4
Size: 323811 Color: 18
Size: 277341 Color: 17

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 399945 Color: 6
Size: 336267 Color: 3
Size: 263788 Color: 16

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 402619 Color: 2
Size: 338998 Color: 9
Size: 258383 Color: 18

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 404051 Color: 10
Size: 340865 Color: 9
Size: 255084 Color: 17

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 404818 Color: 12
Size: 339926 Color: 15
Size: 255256 Color: 12

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 404821 Color: 14
Size: 311574 Color: 18
Size: 283605 Color: 8

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 404919 Color: 6
Size: 301191 Color: 19
Size: 293890 Color: 8

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 410013 Color: 0
Size: 312535 Color: 3
Size: 277452 Color: 2

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 411068 Color: 3
Size: 338753 Color: 12
Size: 250179 Color: 4

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 411419 Color: 7
Size: 332089 Color: 2
Size: 256492 Color: 7

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 411723 Color: 6
Size: 333613 Color: 1
Size: 254664 Color: 1

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 412475 Color: 11
Size: 310858 Color: 3
Size: 276667 Color: 1

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 412861 Color: 6
Size: 323859 Color: 17
Size: 263280 Color: 15

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 413354 Color: 14
Size: 320966 Color: 3
Size: 265680 Color: 12

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 413936 Color: 6
Size: 300200 Color: 15
Size: 285864 Color: 11

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 416152 Color: 18
Size: 310336 Color: 9
Size: 273512 Color: 7

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 422300 Color: 8
Size: 300895 Color: 7
Size: 276805 Color: 16

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 422952 Color: 12
Size: 302274 Color: 6
Size: 274774 Color: 12

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 425951 Color: 13
Size: 300077 Color: 9
Size: 273972 Color: 0

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 426035 Color: 11
Size: 289044 Color: 0
Size: 284921 Color: 10

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 426807 Color: 17
Size: 315014 Color: 14
Size: 258179 Color: 7

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 438483 Color: 11
Size: 304416 Color: 8
Size: 257101 Color: 6

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 448568 Color: 10
Size: 293645 Color: 3
Size: 257787 Color: 1

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 452235 Color: 11
Size: 288735 Color: 19
Size: 259030 Color: 7

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 459303 Color: 7
Size: 276692 Color: 17
Size: 264005 Color: 6

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 464933 Color: 11
Size: 271699 Color: 12
Size: 263368 Color: 6

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 479483 Color: 16
Size: 266236 Color: 15
Size: 254281 Color: 13

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 485372 Color: 3
Size: 259381 Color: 7
Size: 255247 Color: 6

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 490987 Color: 11
Size: 256579 Color: 6
Size: 252434 Color: 3

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 492493 Color: 6
Size: 256486 Color: 9
Size: 251021 Color: 14

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 361571 Color: 6
Size: 336929 Color: 0
Size: 301500 Color: 9

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 366288 Color: 0
Size: 329811 Color: 13
Size: 303901 Color: 2

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 360693 Color: 7
Size: 346995 Color: 12
Size: 292312 Color: 4

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 376106 Color: 15
Size: 373632 Color: 15
Size: 250262 Color: 16

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 346789 Color: 15
Size: 345405 Color: 16
Size: 307806 Color: 15

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 338228 Color: 14
Size: 333115 Color: 11
Size: 328657 Color: 4

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 363208 Color: 15
Size: 323721 Color: 4
Size: 313070 Color: 1

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 364701 Color: 14
Size: 329037 Color: 3
Size: 306261 Color: 2

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 368035 Color: 16
Size: 318877 Color: 13
Size: 313087 Color: 2

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 368236 Color: 4
Size: 326906 Color: 1
Size: 304857 Color: 13

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 366828 Color: 17
Size: 341356 Color: 6
Size: 291815 Color: 14

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 369676 Color: 10
Size: 325896 Color: 5
Size: 304427 Color: 11

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 375271 Color: 5
Size: 334695 Color: 11
Size: 290033 Color: 4

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 376544 Color: 5
Size: 340973 Color: 17
Size: 282482 Color: 1

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 378044 Color: 7
Size: 367384 Color: 0
Size: 254571 Color: 11

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 378589 Color: 0
Size: 369092 Color: 9
Size: 252318 Color: 6

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 378706 Color: 13
Size: 367429 Color: 19
Size: 253864 Color: 7

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 379620 Color: 2
Size: 342671 Color: 14
Size: 277708 Color: 12

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 381101 Color: 10
Size: 314896 Color: 10
Size: 304002 Color: 16

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 382764 Color: 11
Size: 336218 Color: 10
Size: 281017 Color: 19

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 383880 Color: 3
Size: 365512 Color: 6
Size: 250607 Color: 18

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 385226 Color: 0
Size: 307995 Color: 12
Size: 306778 Color: 13

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 386091 Color: 5
Size: 311038 Color: 1
Size: 302870 Color: 10

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 389554 Color: 9
Size: 352542 Color: 17
Size: 257903 Color: 4

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 391732 Color: 10
Size: 344374 Color: 10
Size: 263893 Color: 12

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 392056 Color: 16
Size: 314454 Color: 18
Size: 293489 Color: 18

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 394571 Color: 7
Size: 345031 Color: 3
Size: 260397 Color: 9

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 397423 Color: 12
Size: 313881 Color: 13
Size: 288695 Color: 17

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 397477 Color: 5
Size: 316016 Color: 6
Size: 286506 Color: 15

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 398352 Color: 16
Size: 343577 Color: 17
Size: 258070 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 398973 Color: 5
Size: 308172 Color: 9
Size: 292854 Color: 6

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 399723 Color: 11
Size: 330440 Color: 2
Size: 269836 Color: 17

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 401834 Color: 15
Size: 345642 Color: 18
Size: 252523 Color: 6

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 402450 Color: 11
Size: 346662 Color: 1
Size: 250887 Color: 10

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 404924 Color: 5
Size: 315290 Color: 0
Size: 279785 Color: 15

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 405066 Color: 17
Size: 305967 Color: 10
Size: 288966 Color: 13

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 415800 Color: 19
Size: 321354 Color: 18
Size: 262845 Color: 6

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 417477 Color: 3
Size: 297662 Color: 6
Size: 284860 Color: 12

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 418599 Color: 17
Size: 300286 Color: 6
Size: 281114 Color: 1

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 419072 Color: 18
Size: 319214 Color: 9
Size: 261713 Color: 0

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 419113 Color: 18
Size: 314964 Color: 9
Size: 265922 Color: 8

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 421631 Color: 13
Size: 303757 Color: 19
Size: 274611 Color: 3

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 429228 Color: 18
Size: 299902 Color: 6
Size: 270869 Color: 4

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 437113 Color: 18
Size: 286314 Color: 17
Size: 276572 Color: 6

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 448150 Color: 19
Size: 281893 Color: 16
Size: 269956 Color: 6

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 473503 Color: 19
Size: 276255 Color: 18
Size: 250241 Color: 6

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 493208 Color: 13
Size: 256662 Color: 11
Size: 250129 Color: 19

Bin 512: 2 of cap free
Amount of items: 3
Items: 
Size: 376643 Color: 13
Size: 334192 Color: 19
Size: 289164 Color: 5

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 351721 Color: 5
Size: 351240 Color: 9
Size: 297038 Color: 18

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 374928 Color: 2
Size: 366287 Color: 14
Size: 258783 Color: 3

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 369065 Color: 0
Size: 345460 Color: 19
Size: 285473 Color: 15

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 367371 Color: 13
Size: 322528 Color: 6
Size: 310099 Color: 15

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 371075 Color: 3
Size: 367788 Color: 11
Size: 261135 Color: 10

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 375627 Color: 2
Size: 371713 Color: 7
Size: 252658 Color: 9

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 378611 Color: 12
Size: 323053 Color: 7
Size: 298334 Color: 8

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 379947 Color: 16
Size: 337935 Color: 4
Size: 282116 Color: 6

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 380287 Color: 9
Size: 364258 Color: 0
Size: 255453 Color: 16

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 386178 Color: 9
Size: 363331 Color: 16
Size: 250489 Color: 6

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 386387 Color: 17
Size: 332148 Color: 16
Size: 281463 Color: 18

Bin 524: 3 of cap free
Amount of items: 3
Items: 
Size: 390729 Color: 15
Size: 328111 Color: 14
Size: 281158 Color: 18

Bin 525: 3 of cap free
Amount of items: 3
Items: 
Size: 399259 Color: 18
Size: 330729 Color: 4
Size: 270010 Color: 9

Bin 526: 3 of cap free
Amount of items: 3
Items: 
Size: 404030 Color: 11
Size: 338029 Color: 17
Size: 257939 Color: 0

Bin 527: 3 of cap free
Amount of items: 3
Items: 
Size: 404249 Color: 19
Size: 332768 Color: 2
Size: 262981 Color: 1

Bin 528: 3 of cap free
Amount of items: 3
Items: 
Size: 409508 Color: 13
Size: 319032 Color: 12
Size: 271458 Color: 1

Bin 529: 3 of cap free
Amount of items: 3
Items: 
Size: 419969 Color: 14
Size: 319859 Color: 17
Size: 260170 Color: 6

Bin 530: 3 of cap free
Amount of items: 3
Items: 
Size: 435558 Color: 5
Size: 299453 Color: 6
Size: 264987 Color: 7

Bin 531: 3 of cap free
Amount of items: 3
Items: 
Size: 454136 Color: 13
Size: 276323 Color: 6
Size: 269539 Color: 9

Bin 532: 3 of cap free
Amount of items: 3
Items: 
Size: 460928 Color: 1
Size: 279954 Color: 5
Size: 259116 Color: 6

Bin 533: 3 of cap free
Amount of items: 3
Items: 
Size: 467100 Color: 1
Size: 276786 Color: 19
Size: 256112 Color: 6

Bin 534: 3 of cap free
Amount of items: 3
Items: 
Size: 471355 Color: 6
Size: 269332 Color: 19
Size: 259311 Color: 2

Bin 535: 3 of cap free
Amount of items: 3
Items: 
Size: 342599 Color: 17
Size: 340035 Color: 5
Size: 317364 Color: 11

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 443850 Color: 11
Size: 301087 Color: 0
Size: 255060 Color: 6

Bin 537: 4 of cap free
Amount of items: 3
Items: 
Size: 371477 Color: 11
Size: 365528 Color: 7
Size: 262992 Color: 13

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 374825 Color: 2
Size: 326437 Color: 9
Size: 298735 Color: 8

Bin 539: 4 of cap free
Amount of items: 3
Items: 
Size: 382872 Color: 3
Size: 312421 Color: 0
Size: 304704 Color: 8

Bin 540: 4 of cap free
Amount of items: 3
Items: 
Size: 391095 Color: 8
Size: 342348 Color: 19
Size: 266554 Color: 5

Bin 541: 4 of cap free
Amount of items: 3
Items: 
Size: 405633 Color: 11
Size: 297690 Color: 5
Size: 296674 Color: 6

Bin 542: 4 of cap free
Amount of items: 3
Items: 
Size: 408483 Color: 12
Size: 306797 Color: 4
Size: 284717 Color: 9

Bin 543: 4 of cap free
Amount of items: 3
Items: 
Size: 409187 Color: 18
Size: 336456 Color: 7
Size: 254354 Color: 19

Bin 544: 4 of cap free
Amount of items: 3
Items: 
Size: 425786 Color: 11
Size: 299619 Color: 6
Size: 274592 Color: 1

Bin 545: 4 of cap free
Amount of items: 3
Items: 
Size: 442414 Color: 12
Size: 290840 Color: 18
Size: 266743 Color: 19

Bin 546: 4 of cap free
Amount of items: 3
Items: 
Size: 469737 Color: 3
Size: 268450 Color: 6
Size: 261810 Color: 17

Bin 547: 4 of cap free
Amount of items: 3
Items: 
Size: 357866 Color: 3
Size: 324389 Color: 15
Size: 317742 Color: 19

Bin 548: 5 of cap free
Amount of items: 3
Items: 
Size: 400491 Color: 2
Size: 304805 Color: 6
Size: 294700 Color: 2

Bin 549: 5 of cap free
Amount of items: 3
Items: 
Size: 368968 Color: 6
Size: 343929 Color: 6
Size: 287099 Color: 18

Bin 550: 5 of cap free
Amount of items: 3
Items: 
Size: 369816 Color: 2
Size: 359871 Color: 13
Size: 270309 Color: 18

Bin 551: 5 of cap free
Amount of items: 3
Items: 
Size: 376273 Color: 19
Size: 345648 Color: 16
Size: 278075 Color: 12

Bin 552: 5 of cap free
Amount of items: 3
Items: 
Size: 378926 Color: 16
Size: 366091 Color: 0
Size: 254979 Color: 0

Bin 553: 5 of cap free
Amount of items: 3
Items: 
Size: 428591 Color: 12
Size: 300586 Color: 6
Size: 270819 Color: 13

Bin 554: 5 of cap free
Amount of items: 3
Items: 
Size: 463292 Color: 9
Size: 283342 Color: 12
Size: 253362 Color: 6

Bin 555: 5 of cap free
Amount of items: 3
Items: 
Size: 349258 Color: 0
Size: 326472 Color: 4
Size: 324266 Color: 17

Bin 556: 5 of cap free
Amount of items: 3
Items: 
Size: 359252 Color: 4
Size: 329460 Color: 13
Size: 311284 Color: 9

Bin 557: 5 of cap free
Amount of items: 3
Items: 
Size: 344145 Color: 0
Size: 339433 Color: 9
Size: 316418 Color: 11

Bin 558: 6 of cap free
Amount of items: 3
Items: 
Size: 369557 Color: 0
Size: 315415 Color: 11
Size: 315023 Color: 6

Bin 559: 6 of cap free
Amount of items: 3
Items: 
Size: 372542 Color: 15
Size: 320393 Color: 19
Size: 307060 Color: 10

Bin 560: 6 of cap free
Amount of items: 3
Items: 
Size: 376484 Color: 0
Size: 348489 Color: 6
Size: 275022 Color: 17

Bin 561: 6 of cap free
Amount of items: 3
Items: 
Size: 378553 Color: 2
Size: 354392 Color: 3
Size: 267050 Color: 4

Bin 562: 6 of cap free
Amount of items: 3
Items: 
Size: 380216 Color: 19
Size: 323474 Color: 4
Size: 296305 Color: 16

Bin 563: 6 of cap free
Amount of items: 3
Items: 
Size: 396908 Color: 3
Size: 319467 Color: 15
Size: 283620 Color: 13

Bin 564: 6 of cap free
Amount of items: 3
Items: 
Size: 397181 Color: 1
Size: 349499 Color: 17
Size: 253315 Color: 3

Bin 565: 6 of cap free
Amount of items: 3
Items: 
Size: 421877 Color: 1
Size: 320105 Color: 6
Size: 258013 Color: 13

Bin 566: 6 of cap free
Amount of items: 3
Items: 
Size: 368395 Color: 2
Size: 339369 Color: 0
Size: 292231 Color: 10

Bin 567: 7 of cap free
Amount of items: 3
Items: 
Size: 361336 Color: 5
Size: 339514 Color: 2
Size: 299144 Color: 10

Bin 568: 7 of cap free
Amount of items: 3
Items: 
Size: 363910 Color: 7
Size: 360291 Color: 0
Size: 275793 Color: 14

Bin 569: 7 of cap free
Amount of items: 3
Items: 
Size: 364406 Color: 19
Size: 359932 Color: 3
Size: 275656 Color: 8

Bin 570: 7 of cap free
Amount of items: 3
Items: 
Size: 383898 Color: 5
Size: 314269 Color: 4
Size: 301827 Color: 13

Bin 571: 7 of cap free
Amount of items: 3
Items: 
Size: 393836 Color: 0
Size: 346033 Color: 4
Size: 260125 Color: 14

Bin 572: 7 of cap free
Amount of items: 3
Items: 
Size: 403627 Color: 0
Size: 345958 Color: 11
Size: 250409 Color: 6

Bin 573: 7 of cap free
Amount of items: 3
Items: 
Size: 407644 Color: 2
Size: 321331 Color: 5
Size: 271019 Color: 19

Bin 574: 7 of cap free
Amount of items: 3
Items: 
Size: 341393 Color: 18
Size: 339967 Color: 12
Size: 318634 Color: 6

Bin 575: 7 of cap free
Amount of items: 3
Items: 
Size: 356822 Color: 2
Size: 343954 Color: 3
Size: 299218 Color: 7

Bin 576: 8 of cap free
Amount of items: 3
Items: 
Size: 364508 Color: 5
Size: 362580 Color: 18
Size: 272905 Color: 7

Bin 577: 8 of cap free
Amount of items: 3
Items: 
Size: 368780 Color: 6
Size: 335504 Color: 10
Size: 295709 Color: 13

Bin 578: 8 of cap free
Amount of items: 3
Items: 
Size: 374809 Color: 0
Size: 374678 Color: 2
Size: 250506 Color: 17

Bin 579: 8 of cap free
Amount of items: 3
Items: 
Size: 376166 Color: 7
Size: 336545 Color: 13
Size: 287282 Color: 3

Bin 580: 9 of cap free
Amount of items: 3
Items: 
Size: 368496 Color: 8
Size: 355805 Color: 13
Size: 275691 Color: 15

Bin 581: 9 of cap free
Amount of items: 3
Items: 
Size: 372332 Color: 19
Size: 317284 Color: 16
Size: 310376 Color: 4

Bin 582: 9 of cap free
Amount of items: 3
Items: 
Size: 377393 Color: 12
Size: 347265 Color: 18
Size: 275334 Color: 4

Bin 583: 9 of cap free
Amount of items: 3
Items: 
Size: 439454 Color: 11
Size: 300736 Color: 13
Size: 259802 Color: 6

Bin 584: 10 of cap free
Amount of items: 3
Items: 
Size: 353316 Color: 15
Size: 348906 Color: 19
Size: 297769 Color: 4

Bin 585: 10 of cap free
Amount of items: 3
Items: 
Size: 381218 Color: 15
Size: 324253 Color: 4
Size: 294520 Color: 16

Bin 586: 10 of cap free
Amount of items: 3
Items: 
Size: 363619 Color: 12
Size: 332052 Color: 11
Size: 304320 Color: 11

Bin 587: 10 of cap free
Amount of items: 3
Items: 
Size: 376397 Color: 3
Size: 334423 Color: 4
Size: 289171 Color: 13

Bin 588: 11 of cap free
Amount of items: 3
Items: 
Size: 366044 Color: 18
Size: 342531 Color: 4
Size: 291415 Color: 15

Bin 589: 11 of cap free
Amount of items: 3
Items: 
Size: 366845 Color: 7
Size: 354509 Color: 17
Size: 278636 Color: 7

Bin 590: 11 of cap free
Amount of items: 3
Items: 
Size: 372460 Color: 6
Size: 354715 Color: 11
Size: 272815 Color: 14

Bin 591: 12 of cap free
Amount of items: 3
Items: 
Size: 358338 Color: 12
Size: 346879 Color: 9
Size: 294772 Color: 6

Bin 592: 13 of cap free
Amount of items: 3
Items: 
Size: 373166 Color: 7
Size: 349379 Color: 18
Size: 277443 Color: 4

Bin 593: 13 of cap free
Amount of items: 3
Items: 
Size: 385419 Color: 12
Size: 358303 Color: 13
Size: 256266 Color: 4

Bin 594: 13 of cap free
Amount of items: 3
Items: 
Size: 353343 Color: 1
Size: 334747 Color: 18
Size: 311898 Color: 12

Bin 595: 13 of cap free
Amount of items: 3
Items: 
Size: 367323 Color: 18
Size: 363667 Color: 2
Size: 268998 Color: 17

Bin 596: 13 of cap free
Amount of items: 3
Items: 
Size: 357052 Color: 17
Size: 338081 Color: 11
Size: 304855 Color: 7

Bin 597: 13 of cap free
Amount of items: 3
Items: 
Size: 345302 Color: 18
Size: 332301 Color: 10
Size: 322385 Color: 8

Bin 598: 14 of cap free
Amount of items: 3
Items: 
Size: 356766 Color: 5
Size: 337798 Color: 7
Size: 305423 Color: 10

Bin 599: 14 of cap free
Amount of items: 3
Items: 
Size: 370780 Color: 7
Size: 364270 Color: 8
Size: 264937 Color: 11

Bin 600: 14 of cap free
Amount of items: 3
Items: 
Size: 373966 Color: 19
Size: 358071 Color: 3
Size: 267950 Color: 4

Bin 601: 14 of cap free
Amount of items: 3
Items: 
Size: 411713 Color: 7
Size: 334340 Color: 2
Size: 253934 Color: 19

Bin 602: 15 of cap free
Amount of items: 3
Items: 
Size: 397753 Color: 14
Size: 329809 Color: 5
Size: 272424 Color: 19

Bin 603: 15 of cap free
Amount of items: 3
Items: 
Size: 379163 Color: 16
Size: 331965 Color: 4
Size: 288858 Color: 9

Bin 604: 15 of cap free
Amount of items: 3
Items: 
Size: 396900 Color: 14
Size: 335748 Color: 16
Size: 267338 Color: 11

Bin 605: 15 of cap free
Amount of items: 3
Items: 
Size: 363255 Color: 9
Size: 359496 Color: 15
Size: 277235 Color: 5

Bin 606: 16 of cap free
Amount of items: 3
Items: 
Size: 388473 Color: 19
Size: 326145 Color: 10
Size: 285367 Color: 4

Bin 607: 16 of cap free
Amount of items: 3
Items: 
Size: 422759 Color: 7
Size: 298456 Color: 19
Size: 278770 Color: 13

Bin 608: 16 of cap free
Amount of items: 3
Items: 
Size: 347949 Color: 19
Size: 337912 Color: 11
Size: 314124 Color: 6

Bin 609: 17 of cap free
Amount of items: 3
Items: 
Size: 369018 Color: 12
Size: 341692 Color: 13
Size: 289274 Color: 6

Bin 610: 17 of cap free
Amount of items: 3
Items: 
Size: 377528 Color: 6
Size: 317319 Color: 15
Size: 305137 Color: 3

Bin 611: 17 of cap free
Amount of items: 3
Items: 
Size: 393447 Color: 4
Size: 318891 Color: 3
Size: 287646 Color: 19

Bin 612: 17 of cap free
Amount of items: 3
Items: 
Size: 380662 Color: 12
Size: 364793 Color: 1
Size: 254529 Color: 18

Bin 613: 17 of cap free
Amount of items: 3
Items: 
Size: 380172 Color: 9
Size: 369231 Color: 12
Size: 250581 Color: 5

Bin 614: 18 of cap free
Amount of items: 3
Items: 
Size: 356216 Color: 1
Size: 352522 Color: 14
Size: 291245 Color: 17

Bin 615: 19 of cap free
Amount of items: 3
Items: 
Size: 362779 Color: 15
Size: 339423 Color: 16
Size: 297780 Color: 14

Bin 616: 19 of cap free
Amount of items: 3
Items: 
Size: 401123 Color: 3
Size: 317583 Color: 8
Size: 281276 Color: 6

Bin 617: 21 of cap free
Amount of items: 3
Items: 
Size: 376625 Color: 6
Size: 330864 Color: 16
Size: 292491 Color: 3

Bin 618: 21 of cap free
Amount of items: 3
Items: 
Size: 450887 Color: 14
Size: 291059 Color: 4
Size: 258034 Color: 6

Bin 619: 21 of cap free
Amount of items: 3
Items: 
Size: 370886 Color: 0
Size: 359981 Color: 4
Size: 269113 Color: 5

Bin 620: 23 of cap free
Amount of items: 3
Items: 
Size: 357269 Color: 7
Size: 356850 Color: 17
Size: 285859 Color: 1

Bin 621: 23 of cap free
Amount of items: 3
Items: 
Size: 361690 Color: 9
Size: 322597 Color: 4
Size: 315691 Color: 6

Bin 622: 24 of cap free
Amount of items: 3
Items: 
Size: 340418 Color: 4
Size: 340146 Color: 13
Size: 319413 Color: 3

Bin 623: 29 of cap free
Amount of items: 3
Items: 
Size: 367118 Color: 10
Size: 327966 Color: 5
Size: 304888 Color: 11

Bin 624: 29 of cap free
Amount of items: 3
Items: 
Size: 368313 Color: 4
Size: 341122 Color: 19
Size: 290537 Color: 5

Bin 625: 32 of cap free
Amount of items: 3
Items: 
Size: 361873 Color: 4
Size: 346828 Color: 6
Size: 291268 Color: 15

Bin 626: 34 of cap free
Amount of items: 3
Items: 
Size: 361100 Color: 17
Size: 354465 Color: 7
Size: 284402 Color: 7

Bin 627: 35 of cap free
Amount of items: 3
Items: 
Size: 360208 Color: 4
Size: 356967 Color: 15
Size: 282791 Color: 9

Bin 628: 38 of cap free
Amount of items: 3
Items: 
Size: 418239 Color: 0
Size: 319365 Color: 17
Size: 262359 Color: 2

Bin 629: 39 of cap free
Amount of items: 3
Items: 
Size: 353692 Color: 10
Size: 349933 Color: 0
Size: 296337 Color: 12

Bin 630: 41 of cap free
Amount of items: 3
Items: 
Size: 364782 Color: 4
Size: 318456 Color: 0
Size: 316722 Color: 10

Bin 631: 42 of cap free
Amount of items: 3
Items: 
Size: 364550 Color: 3
Size: 334018 Color: 9
Size: 301391 Color: 9

Bin 632: 48 of cap free
Amount of items: 3
Items: 
Size: 363978 Color: 5
Size: 333510 Color: 1
Size: 302465 Color: 3

Bin 633: 51 of cap free
Amount of items: 3
Items: 
Size: 341201 Color: 12
Size: 329377 Color: 4
Size: 329372 Color: 16

Bin 634: 51 of cap free
Amount of items: 3
Items: 
Size: 357629 Color: 3
Size: 337774 Color: 12
Size: 304547 Color: 10

Bin 635: 58 of cap free
Amount of items: 3
Items: 
Size: 365299 Color: 4
Size: 359273 Color: 13
Size: 275371 Color: 17

Bin 636: 61 of cap free
Amount of items: 3
Items: 
Size: 358477 Color: 15
Size: 328738 Color: 14
Size: 312725 Color: 2

Bin 637: 74 of cap free
Amount of items: 3
Items: 
Size: 357873 Color: 0
Size: 342191 Color: 17
Size: 299863 Color: 12

Bin 638: 75 of cap free
Amount of items: 3
Items: 
Size: 366066 Color: 17
Size: 362492 Color: 0
Size: 271368 Color: 8

Bin 639: 84 of cap free
Amount of items: 3
Items: 
Size: 360994 Color: 6
Size: 354609 Color: 8
Size: 284314 Color: 0

Bin 640: 88 of cap free
Amount of items: 3
Items: 
Size: 375532 Color: 1
Size: 371910 Color: 3
Size: 252471 Color: 12

Bin 641: 95 of cap free
Amount of items: 3
Items: 
Size: 363615 Color: 12
Size: 344137 Color: 18
Size: 292154 Color: 11

Bin 642: 103 of cap free
Amount of items: 3
Items: 
Size: 345521 Color: 0
Size: 327436 Color: 4
Size: 326941 Color: 4

Bin 643: 112 of cap free
Amount of items: 3
Items: 
Size: 352138 Color: 8
Size: 326933 Color: 7
Size: 320818 Color: 6

Bin 644: 115 of cap free
Amount of items: 3
Items: 
Size: 378932 Color: 8
Size: 349705 Color: 6
Size: 271249 Color: 15

Bin 645: 119 of cap free
Amount of items: 3
Items: 
Size: 344275 Color: 6
Size: 342146 Color: 16
Size: 313461 Color: 3

Bin 646: 134 of cap free
Amount of items: 3
Items: 
Size: 360517 Color: 19
Size: 336929 Color: 18
Size: 302421 Color: 10

Bin 647: 153 of cap free
Amount of items: 3
Items: 
Size: 338178 Color: 5
Size: 337118 Color: 18
Size: 324552 Color: 0

Bin 648: 160 of cap free
Amount of items: 3
Items: 
Size: 363312 Color: 4
Size: 331877 Color: 1
Size: 304652 Color: 0

Bin 649: 169 of cap free
Amount of items: 3
Items: 
Size: 364830 Color: 14
Size: 360498 Color: 8
Size: 274504 Color: 6

Bin 650: 179 of cap free
Amount of items: 3
Items: 
Size: 341925 Color: 4
Size: 328996 Color: 17
Size: 328901 Color: 13

Bin 651: 186 of cap free
Amount of items: 3
Items: 
Size: 368945 Color: 18
Size: 343174 Color: 7
Size: 287696 Color: 16

Bin 652: 233 of cap free
Amount of items: 3
Items: 
Size: 362927 Color: 3
Size: 326885 Color: 8
Size: 309956 Color: 5

Bin 653: 257 of cap free
Amount of items: 3
Items: 
Size: 343496 Color: 10
Size: 335967 Color: 9
Size: 320281 Color: 11

Bin 654: 291 of cap free
Amount of items: 3
Items: 
Size: 344944 Color: 2
Size: 344835 Color: 10
Size: 309931 Color: 12

Bin 655: 342 of cap free
Amount of items: 3
Items: 
Size: 368930 Color: 19
Size: 366317 Color: 16
Size: 264412 Color: 7

Bin 656: 346 of cap free
Amount of items: 3
Items: 
Size: 350670 Color: 3
Size: 347486 Color: 5
Size: 301499 Color: 16

Bin 657: 367 of cap free
Amount of items: 3
Items: 
Size: 370441 Color: 3
Size: 326122 Color: 5
Size: 303071 Color: 6

Bin 658: 638 of cap free
Amount of items: 3
Items: 
Size: 344094 Color: 10
Size: 341925 Color: 11
Size: 313344 Color: 14

Bin 659: 639 of cap free
Amount of items: 3
Items: 
Size: 359161 Color: 12
Size: 350428 Color: 6
Size: 289773 Color: 14

Bin 660: 727 of cap free
Amount of items: 2
Items: 
Size: 499819 Color: 11
Size: 499455 Color: 16

Bin 661: 909 of cap free
Amount of items: 3
Items: 
Size: 413460 Color: 1
Size: 330776 Color: 1
Size: 254856 Color: 12

Bin 662: 1084 of cap free
Amount of items: 3
Items: 
Size: 397293 Color: 4
Size: 339294 Color: 1
Size: 262330 Color: 15

Bin 663: 1495 of cap free
Amount of items: 3
Items: 
Size: 350211 Color: 5
Size: 325809 Color: 16
Size: 322486 Color: 16

Bin 664: 4038 of cap free
Amount of items: 3
Items: 
Size: 342773 Color: 17
Size: 332412 Color: 8
Size: 320778 Color: 13

Bin 665: 56980 of cap free
Amount of items: 3
Items: 
Size: 339253 Color: 3
Size: 319593 Color: 19
Size: 284175 Color: 19

Bin 666: 181962 of cap free
Amount of items: 3
Items: 
Size: 284060 Color: 16
Size: 272792 Color: 7
Size: 261187 Color: 19

Bin 667: 242364 of cap free
Amount of items: 3
Items: 
Size: 253779 Color: 6
Size: 253107 Color: 15
Size: 250751 Color: 0

Bin 668: 503789 of cap free
Amount of items: 1
Items: 
Size: 496212 Color: 14

Total size: 667000667
Total free space: 1000001

