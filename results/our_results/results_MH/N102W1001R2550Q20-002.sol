Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 12
Size: 300 Color: 7
Size: 284 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 316 Color: 15
Size: 310 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9
Size: 253 Color: 2
Size: 250 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6
Size: 334 Color: 11
Size: 311 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 15
Size: 289 Color: 9
Size: 285 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 11
Size: 276 Color: 6
Size: 250 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 14
Size: 338 Color: 10
Size: 322 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 7
Size: 288 Color: 11
Size: 359 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 10
Size: 309 Color: 6
Size: 307 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 341 Color: 6
Size: 277 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 0
Size: 321 Color: 15
Size: 255 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 15
Size: 344 Color: 3
Size: 273 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 330 Color: 13
Size: 287 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 3
Size: 335 Color: 12
Size: 323 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 259 Color: 17
Size: 251 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 9
Size: 323 Color: 1
Size: 296 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 304 Color: 0
Size: 259 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 6
Size: 347 Color: 12
Size: 273 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8
Size: 309 Color: 0
Size: 279 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 7
Size: 257 Color: 18
Size: 250 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 355 Color: 14
Size: 259 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 11
Size: 302 Color: 2
Size: 270 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 13
Size: 318 Color: 9
Size: 299 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 325 Color: 9
Size: 297 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 6
Size: 333 Color: 7
Size: 318 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 15
Size: 293 Color: 2
Size: 261 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 303 Color: 14
Size: 284 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 10
Size: 326 Color: 15
Size: 278 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 13
Size: 284 Color: 11
Size: 267 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 19
Size: 321 Color: 1
Size: 311 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 10
Size: 272 Color: 15
Size: 262 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 14
Size: 308 Color: 1
Size: 294 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 16
Size: 302 Color: 19
Size: 293 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 6
Size: 320 Color: 1
Size: 270 Color: 12

Total size: 34034
Total free space: 0

