Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11006 Color: 417
Size: 8176 Color: 396
Size: 466 Color: 61

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 11240 Color: 426
Size: 6992 Color: 378
Size: 520 Color: 75
Size: 448 Color: 55
Size: 448 Color: 54

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 458
Size: 5928 Color: 357
Size: 304 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 459
Size: 4880 Color: 335
Size: 1344 Color: 179

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 460
Size: 3530 Color: 293
Size: 2644 Color: 259

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 473
Size: 4620 Color: 330
Size: 920 Color: 143

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 476
Size: 4536 Color: 326
Size: 896 Color: 142

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14352 Color: 481
Size: 4016 Color: 307
Size: 1280 Color: 175

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 486
Size: 5160 Color: 344
Size: 32 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14803 Color: 490
Size: 3433 Color: 289
Size: 1412 Color: 185

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14816 Color: 491
Size: 4544 Color: 327
Size: 288 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14820 Color: 492
Size: 4358 Color: 319
Size: 470 Color: 62

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14860 Color: 493
Size: 3476 Color: 291
Size: 1312 Color: 177

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 494
Size: 4330 Color: 317
Size: 454 Color: 57

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14910 Color: 496
Size: 4620 Color: 331
Size: 118 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15136 Color: 499
Size: 4192 Color: 313
Size: 320 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15180 Color: 501
Size: 3680 Color: 299
Size: 788 Color: 123

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15192 Color: 502
Size: 2282 Color: 240
Size: 2174 Color: 233

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15322 Color: 507
Size: 3310 Color: 286
Size: 1016 Color: 153

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15484 Color: 511
Size: 2756 Color: 264
Size: 1408 Color: 183

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 512
Size: 3168 Color: 281
Size: 960 Color: 147

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15560 Color: 515
Size: 3512 Color: 292
Size: 576 Color: 85

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15704 Color: 518
Size: 2128 Color: 229
Size: 1816 Color: 211

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15728 Color: 520
Size: 3552 Color: 294
Size: 368 Color: 31

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15828 Color: 522
Size: 3416 Color: 288
Size: 404 Color: 46

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16016 Color: 528
Size: 3280 Color: 283
Size: 352 Color: 28

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16020 Color: 529
Size: 2760 Color: 265
Size: 868 Color: 134

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16278 Color: 536
Size: 1750 Color: 207
Size: 1620 Color: 194

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16288 Color: 537
Size: 2176 Color: 234
Size: 1184 Color: 166

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16387 Color: 540
Size: 2455 Color: 250
Size: 806 Color: 125

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16400 Color: 541
Size: 2704 Color: 261
Size: 544 Color: 79

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16484 Color: 543
Size: 2428 Color: 247
Size: 736 Color: 119

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16538 Color: 547
Size: 2454 Color: 249
Size: 656 Color: 98

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16712 Color: 553
Size: 2232 Color: 236
Size: 704 Color: 115

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16720 Color: 554
Size: 2240 Color: 237
Size: 688 Color: 108

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16740 Color: 555
Size: 1782 Color: 210
Size: 1126 Color: 162

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16848 Color: 559
Size: 2448 Color: 248
Size: 352 Color: 29

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16864 Color: 560
Size: 2144 Color: 232
Size: 640 Color: 96

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16908 Color: 561
Size: 2592 Color: 254
Size: 148 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16956 Color: 564
Size: 1772 Color: 208
Size: 920 Color: 144

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16958 Color: 565
Size: 1858 Color: 214
Size: 832 Color: 127

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17084 Color: 570
Size: 1632 Color: 196
Size: 932 Color: 145

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17104 Color: 572
Size: 1952 Color: 220
Size: 592 Color: 87

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17120 Color: 573
Size: 1696 Color: 205
Size: 832 Color: 126

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17202 Color: 575
Size: 1886 Color: 218
Size: 560 Color: 80

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17268 Color: 578
Size: 2284 Color: 241
Size: 96 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 579
Size: 1492 Color: 191
Size: 852 Color: 129

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17312 Color: 580
Size: 2000 Color: 223
Size: 336 Color: 23

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17328 Color: 581
Size: 1872 Color: 216
Size: 448 Color: 56

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17386 Color: 583
Size: 1914 Color: 219
Size: 348 Color: 24

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17418 Color: 585
Size: 1632 Color: 195
Size: 598 Color: 88

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17422 Color: 586
Size: 1662 Color: 203
Size: 564 Color: 81

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17424 Color: 587
Size: 1440 Color: 190
Size: 784 Color: 122

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17480 Color: 588
Size: 1720 Color: 206
Size: 448 Color: 53

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17524 Color: 590
Size: 1600 Color: 192
Size: 524 Color: 76

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17536 Color: 591
Size: 1780 Color: 209
Size: 332 Color: 22

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17550 Color: 592
Size: 1266 Color: 174
Size: 832 Color: 128

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17552 Color: 593
Size: 1632 Color: 198
Size: 464 Color: 60

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17592 Color: 595
Size: 1428 Color: 187
Size: 628 Color: 93

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 596
Size: 1392 Color: 181
Size: 624 Color: 92

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 17654 Color: 598
Size: 1258 Color: 173
Size: 736 Color: 118

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17668 Color: 599
Size: 1376 Color: 180
Size: 604 Color: 90

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 17680 Color: 600
Size: 1600 Color: 193
Size: 368 Color: 33

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 11122 Color: 423
Size: 8525 Color: 403

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 471
Size: 5635 Color: 353
Size: 192 Color: 11

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 485
Size: 2838 Color: 267
Size: 2355 Color: 244

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 16057 Color: 531
Size: 3080 Color: 278
Size: 510 Color: 68

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 16245 Color: 534
Size: 3022 Color: 274
Size: 380 Color: 35

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 17045 Color: 569
Size: 2602 Color: 256

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 17094 Color: 571
Size: 2553 Color: 252

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 463
Size: 6108 Color: 365

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 495
Size: 4640 Color: 332
Size: 128 Color: 8

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 506
Size: 4382 Color: 320

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 15414 Color: 510
Size: 4232 Color: 314

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 15536 Color: 514
Size: 4110 Color: 312

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 16094 Color: 532
Size: 3552 Color: 295

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 16530 Color: 546
Size: 2072 Color: 228
Size: 1044 Color: 158

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 16910 Color: 562
Size: 2736 Color: 263

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 17354 Color: 582
Size: 2292 Color: 242

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 17582 Color: 594
Size: 2064 Color: 227

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 12099 Color: 434
Size: 7146 Color: 383
Size: 400 Color: 42

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 437
Size: 4968 Color: 337
Size: 2352 Color: 243

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 438
Size: 4350 Color: 318
Size: 2962 Color: 271

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 457
Size: 5954 Color: 358
Size: 320 Color: 17

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 14576 Color: 488
Size: 5068 Color: 339

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 17514 Color: 589
Size: 2130 Color: 230

Bin 87: 5 of cap free
Amount of items: 7
Items: 
Size: 9836 Color: 410
Size: 2273 Color: 239
Size: 2200 Color: 235
Size: 2140 Color: 231
Size: 2042 Color: 226
Size: 640 Color: 95
Size: 512 Color: 72

Bin 88: 5 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 508
Size: 4299 Color: 316

Bin 89: 5 of cap free
Amount of items: 2
Items: 
Size: 16706 Color: 552
Size: 2937 Color: 270

Bin 90: 5 of cap free
Amount of items: 2
Items: 
Size: 16751 Color: 556
Size: 2892 Color: 269

Bin 91: 6 of cap free
Amount of items: 5
Items: 
Size: 9904 Color: 414
Size: 8168 Color: 395
Size: 608 Color: 91
Size: 482 Color: 66
Size: 480 Color: 65

Bin 92: 6 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 418
Size: 8160 Color: 394
Size: 456 Color: 59

Bin 93: 6 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 441
Size: 7202 Color: 387

Bin 94: 6 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 454
Size: 6322 Color: 367
Size: 332 Color: 21

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 13506 Color: 462
Size: 4960 Color: 336
Size: 1176 Color: 165

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 16570 Color: 548
Size: 3072 Color: 277

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 16674 Color: 551
Size: 2968 Color: 272

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 17638 Color: 597
Size: 2004 Color: 224

Bin 99: 7 of cap free
Amount of items: 7
Items: 
Size: 9840 Color: 411
Size: 2993 Color: 273
Size: 2618 Color: 257
Size: 2566 Color: 253
Size: 600 Color: 89
Size: 512 Color: 71
Size: 512 Color: 70

Bin 100: 7 of cap free
Amount of items: 2
Items: 
Size: 12464 Color: 443
Size: 7177 Color: 385

Bin 101: 7 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 461
Size: 5889 Color: 355
Size: 256 Color: 14

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 13641 Color: 468
Size: 6000 Color: 359

Bin 103: 7 of cap free
Amount of items: 3
Items: 
Size: 14942 Color: 497
Size: 4667 Color: 333
Size: 32 Color: 1

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 15208 Color: 503
Size: 4433 Color: 323

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 15602 Color: 516
Size: 4039 Color: 310

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 16585 Color: 549
Size: 3056 Color: 276

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 17226 Color: 576
Size: 2415 Color: 246

Bin 108: 8 of cap free
Amount of items: 7
Items: 
Size: 9834 Color: 409
Size: 2022 Color: 225
Size: 1988 Color: 222
Size: 1960 Color: 221
Size: 1884 Color: 217
Size: 1436 Color: 189
Size: 516 Color: 73

Bin 109: 8 of cap free
Amount of items: 5
Items: 
Size: 11272 Color: 427
Size: 7016 Color: 379
Size: 488 Color: 67
Size: 432 Color: 52
Size: 432 Color: 51

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 11408 Color: 429
Size: 7808 Color: 392
Size: 424 Color: 49

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 446
Size: 6720 Color: 374
Size: 384 Color: 37

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 14430 Color: 484
Size: 5210 Color: 346

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 15776 Color: 521
Size: 3864 Color: 304

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 15832 Color: 523
Size: 3808 Color: 303

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 15960 Color: 527
Size: 3680 Color: 298

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 16792 Color: 557
Size: 2848 Color: 268

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 16921 Color: 563
Size: 2719 Color: 262

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 16984 Color: 566
Size: 2656 Color: 260

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 15216 Color: 504
Size: 4422 Color: 322

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 17396 Color: 584
Size: 2242 Color: 238

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 419
Size: 8144 Color: 393
Size: 456 Color: 58

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 489
Size: 5053 Color: 338

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 16488 Color: 544
Size: 3149 Color: 280

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 17016 Color: 567
Size: 2621 Color: 258

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 15237 Color: 505
Size: 4399 Color: 321

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 16348 Color: 539
Size: 3288 Color: 285

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 16608 Color: 550
Size: 3028 Color: 275

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 568
Size: 2594 Color: 255

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 17244 Color: 577
Size: 2392 Color: 245

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 15119 Color: 498
Size: 4516 Color: 325

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 12448 Color: 442
Size: 7186 Color: 386

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 464
Size: 6082 Color: 364

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 16823 Color: 558
Size: 2810 Color: 266

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 430
Size: 7638 Color: 390
Size: 424 Color: 48

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 15392 Color: 509
Size: 4240 Color: 315

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 15710 Color: 519
Size: 3922 Color: 305

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 17176 Color: 574
Size: 2456 Color: 251

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 15871 Color: 524
Size: 3760 Color: 302

Bin 139: 18 of cap free
Amount of items: 3
Items: 
Size: 12350 Color: 439
Size: 6896 Color: 377
Size: 384 Color: 40

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 13728 Color: 469
Size: 5902 Color: 356

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 14422 Color: 483
Size: 5208 Color: 345

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 16503 Color: 545
Size: 3126 Color: 279

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 15678 Color: 517
Size: 3950 Color: 306

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 15904 Color: 526
Size: 3724 Color: 301

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 16022 Color: 530
Size: 3606 Color: 296

Bin 146: 22 of cap free
Amount of items: 2
Items: 
Size: 16344 Color: 538
Size: 3282 Color: 284

Bin 147: 23 of cap free
Amount of items: 9
Items: 
Size: 9829 Color: 407
Size: 1636 Color: 201
Size: 1632 Color: 200
Size: 1632 Color: 199
Size: 1632 Color: 197
Size: 1434 Color: 188
Size: 744 Color: 120
Size: 544 Color: 78
Size: 542 Color: 77

Bin 148: 23 of cap free
Amount of items: 2
Items: 
Size: 15529 Color: 513
Size: 4096 Color: 311

Bin 149: 24 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 445
Size: 6734 Color: 376
Size: 384 Color: 38

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 482
Size: 5234 Color: 347

Bin 151: 25 of cap free
Amount of items: 3
Items: 
Size: 12887 Color: 452
Size: 6384 Color: 370
Size: 352 Color: 26

Bin 152: 28 of cap free
Amount of items: 5
Items: 
Size: 9848 Color: 412
Size: 4028 Color: 309
Size: 4016 Color: 308
Size: 1216 Color: 172
Size: 512 Color: 69

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 466
Size: 6048 Color: 363

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 16180 Color: 533
Size: 3440 Color: 290

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 16246 Color: 535
Size: 3374 Color: 287

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 16432 Color: 542
Size: 3188 Color: 282

Bin 157: 29 of cap free
Amount of items: 2
Items: 
Size: 14491 Color: 487
Size: 5128 Color: 343

Bin 158: 29 of cap free
Amount of items: 2
Items: 
Size: 15147 Color: 500
Size: 4472 Color: 324

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 15898 Color: 525
Size: 3720 Color: 300

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 12288 Color: 436
Size: 7328 Color: 388

Bin 161: 34 of cap free
Amount of items: 3
Items: 
Size: 12971 Color: 453
Size: 6291 Color: 366
Size: 352 Color: 25

Bin 162: 34 of cap free
Amount of items: 2
Items: 
Size: 14049 Color: 472
Size: 5565 Color: 352

Bin 163: 36 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 475
Size: 5330 Color: 348
Size: 168 Color: 10

Bin 164: 39 of cap free
Amount of items: 3
Items: 
Size: 13585 Color: 467
Size: 5804 Color: 354
Size: 220 Color: 13

Bin 165: 40 of cap free
Amount of items: 2
Items: 
Size: 13568 Color: 465
Size: 6040 Color: 362

Bin 166: 42 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 456
Size: 6032 Color: 361
Size: 320 Color: 18

Bin 167: 43 of cap free
Amount of items: 3
Items: 
Size: 11744 Color: 432
Size: 7461 Color: 389
Size: 400 Color: 44

Bin 168: 47 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 449
Size: 6641 Color: 373
Size: 368 Color: 32

Bin 169: 52 of cap free
Amount of items: 2
Items: 
Size: 14236 Color: 477
Size: 5360 Color: 349

Bin 170: 56 of cap free
Amount of items: 4
Items: 
Size: 14342 Color: 480
Size: 5122 Color: 342
Size: 64 Color: 3
Size: 64 Color: 2

Bin 171: 58 of cap free
Amount of items: 3
Items: 
Size: 12474 Color: 444
Size: 6732 Color: 375
Size: 384 Color: 39

Bin 172: 64 of cap free
Amount of items: 2
Items: 
Size: 14112 Color: 474
Size: 5472 Color: 350

Bin 173: 68 of cap free
Amount of items: 4
Items: 
Size: 11720 Color: 431
Size: 7044 Color: 380
Size: 416 Color: 47
Size: 400 Color: 45

Bin 174: 69 of cap free
Amount of items: 3
Items: 
Size: 12579 Color: 448
Size: 6624 Color: 372
Size: 376 Color: 34

Bin 175: 74 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 416
Size: 8182 Color: 398
Size: 480 Color: 63

Bin 176: 74 of cap free
Amount of items: 3
Items: 
Size: 12043 Color: 433
Size: 7131 Color: 382
Size: 400 Color: 43

Bin 177: 77 of cap free
Amount of items: 3
Items: 
Size: 12127 Color: 435
Size: 7052 Color: 381
Size: 392 Color: 41

Bin 178: 78 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 440
Size: 7162 Color: 384

Bin 179: 78 of cap free
Amount of items: 3
Items: 
Size: 12570 Color: 447
Size: 6616 Color: 371
Size: 384 Color: 36

Bin 180: 84 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 470
Size: 5556 Color: 351
Size: 200 Color: 12

Bin 181: 114 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 479
Size: 5104 Color: 341
Size: 102 Color: 5

Bin 182: 130 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 478
Size: 5094 Color: 340
Size: 128 Color: 7

Bin 183: 161 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 425
Size: 4614 Color: 329
Size: 3677 Color: 297

Bin 184: 168 of cap free
Amount of items: 3
Items: 
Size: 13152 Color: 455
Size: 6008 Color: 360
Size: 320 Color: 20

Bin 185: 173 of cap free
Amount of items: 3
Items: 
Size: 12784 Color: 451
Size: 6339 Color: 369
Size: 352 Color: 27

Bin 186: 232 of cap free
Amount of items: 3
Items: 
Size: 11280 Color: 428
Size: 7704 Color: 391
Size: 432 Color: 50

Bin 187: 258 of cap free
Amount of items: 2
Items: 
Size: 11188 Color: 424
Size: 8202 Color: 402

Bin 188: 275 of cap free
Amount of items: 3
Items: 
Size: 12684 Color: 450
Size: 6333 Color: 368
Size: 356 Color: 30

Bin 189: 290 of cap free
Amount of items: 11
Items: 
Size: 9826 Color: 405
Size: 1200 Color: 170
Size: 1200 Color: 169
Size: 1188 Color: 168
Size: 1184 Color: 167
Size: 1160 Color: 164
Size: 1152 Color: 163
Size: 656 Color: 97
Size: 632 Color: 94
Size: 584 Color: 86
Size: 576 Color: 84

Bin 190: 293 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 415
Size: 8180 Color: 397
Size: 480 Color: 64

Bin 191: 308 of cap free
Amount of items: 3
Items: 
Size: 9888 Color: 413
Size: 4860 Color: 334
Size: 4592 Color: 328

Bin 192: 367 of cap free
Amount of items: 2
Items: 
Size: 11093 Color: 422
Size: 8188 Color: 401

Bin 193: 374 of cap free
Amount of items: 2
Items: 
Size: 11090 Color: 421
Size: 8184 Color: 400

Bin 194: 407 of cap free
Amount of items: 2
Items: 
Size: 11058 Color: 420
Size: 8183 Color: 399

Bin 195: 410 of cap free
Amount of items: 7
Items: 
Size: 9832 Color: 408
Size: 1862 Color: 215
Size: 1856 Color: 213
Size: 1846 Color: 212
Size: 1678 Color: 204
Size: 1648 Color: 202
Size: 516 Color: 74

Bin 196: 430 of cap free
Amount of items: 23
Items: 
Size: 1024 Color: 155
Size: 1020 Color: 154
Size: 1010 Color: 152
Size: 1008 Color: 151
Size: 992 Color: 150
Size: 968 Color: 149
Size: 964 Color: 148
Size: 960 Color: 146
Size: 896 Color: 141
Size: 896 Color: 140
Size: 888 Color: 139
Size: 884 Color: 138
Size: 734 Color: 117
Size: 720 Color: 116
Size: 704 Color: 114
Size: 704 Color: 113
Size: 704 Color: 112
Size: 704 Color: 111
Size: 704 Color: 110
Size: 704 Color: 109
Size: 686 Color: 107
Size: 672 Color: 106
Size: 672 Color: 105

Bin 197: 487 of cap free
Amount of items: 12
Items: 
Size: 9825 Color: 404
Size: 1112 Color: 161
Size: 1104 Color: 160
Size: 1064 Color: 159
Size: 1024 Color: 157
Size: 1024 Color: 156
Size: 672 Color: 104
Size: 672 Color: 103
Size: 672 Color: 102
Size: 672 Color: 101
Size: 660 Color: 100
Size: 660 Color: 99

Bin 198: 570 of cap free
Amount of items: 9
Items: 
Size: 9828 Color: 406
Size: 1424 Color: 186
Size: 1408 Color: 184
Size: 1408 Color: 182
Size: 1344 Color: 178
Size: 1298 Color: 176
Size: 1216 Color: 171
Size: 576 Color: 83
Size: 576 Color: 82

Bin 199: 11990 of cap free
Amount of items: 9
Items: 
Size: 880 Color: 137
Size: 880 Color: 136
Size: 876 Color: 135
Size: 868 Color: 133
Size: 864 Color: 132
Size: 864 Color: 131
Size: 858 Color: 130
Size: 800 Color: 124
Size: 768 Color: 121

Total size: 3890304
Total free space: 19648

