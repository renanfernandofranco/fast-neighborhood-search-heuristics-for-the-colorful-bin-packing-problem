Capicity Bin: 2328
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1152 Color: 138
Size: 324 Color: 85
Size: 288 Color: 79
Size: 188 Color: 66
Size: 144 Color: 57
Size: 128 Color: 52
Size: 104 Color: 41

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 170
Size: 462 Color: 102
Size: 88 Color: 32

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1871 Color: 181
Size: 381 Color: 93
Size: 56 Color: 17
Size: 20 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 193
Size: 282 Color: 78
Size: 52 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 187
Size: 323 Color: 84
Size: 64 Color: 22

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 196
Size: 258 Color: 75
Size: 48 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 152
Size: 741 Color: 124
Size: 120 Color: 49

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 162
Size: 537 Color: 110
Size: 106 Color: 44

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 143
Size: 886 Color: 131
Size: 176 Color: 64

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 151
Size: 723 Color: 122
Size: 144 Color: 56

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 177
Size: 469 Color: 104
Size: 4 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 158
Size: 565 Color: 115
Size: 112 Color: 48

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 160
Size: 554 Color: 113
Size: 108 Color: 46

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 147
Size: 829 Color: 127
Size: 164 Color: 62

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 142
Size: 926 Color: 132
Size: 184 Color: 65

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 1864 Color: 180
Size: 348 Color: 91
Size: 76 Color: 29
Size: 40 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 155
Size: 645 Color: 119
Size: 128 Color: 54

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 165
Size: 531 Color: 107
Size: 104 Color: 42

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 188
Size: 322 Color: 83
Size: 64 Color: 21

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 178
Size: 394 Color: 95
Size: 76 Color: 28

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 199
Size: 210 Color: 72
Size: 40 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 159
Size: 553 Color: 112
Size: 110 Color: 47

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 157
Size: 630 Color: 117
Size: 124 Color: 51

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 200
Size: 202 Color: 71
Size: 40 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 153
Size: 654 Color: 121
Size: 128 Color: 55

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 149
Size: 770 Color: 125
Size: 152 Color: 59

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 198
Size: 226 Color: 73
Size: 44 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 189
Size: 346 Color: 90
Size: 28 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 183
Size: 341 Color: 88
Size: 66 Color: 24

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 141
Size: 969 Color: 134
Size: 192 Color: 68

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 172
Size: 455 Color: 100
Size: 90 Color: 36

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 173
Size: 451 Color: 99
Size: 90 Color: 35

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 164
Size: 534 Color: 109
Size: 104 Color: 43

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 184
Size: 342 Color: 89
Size: 64 Color: 23

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 169
Size: 471 Color: 105
Size: 92 Color: 37

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 148
Size: 802 Color: 126
Size: 156 Color: 60

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 163
Size: 533 Color: 108
Size: 106 Color: 45

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 167
Size: 578 Color: 116
Size: 40 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 175
Size: 466 Color: 103
Size: 48 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 166
Size: 538 Color: 111
Size: 96 Color: 40

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 168
Size: 478 Color: 106
Size: 92 Color: 38

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 201
Size: 198 Color: 70
Size: 36 Color: 5

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 154
Size: 646 Color: 120
Size: 128 Color: 53

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 182
Size: 362 Color: 92
Size: 68 Color: 27

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1016 Color: 137
Size: 864 Color: 130
Size: 448 Color: 98

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1322 Color: 144
Size: 850 Color: 129
Size: 156 Color: 61

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1861 Color: 179
Size: 391 Color: 94
Size: 76 Color: 30

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 171
Size: 459 Color: 101
Size: 90 Color: 34

Bin 49: 0 of cap free
Amount of items: 4
Items: 
Size: 1974 Color: 191
Size: 298 Color: 81
Size: 28 Color: 4
Size: 28 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1929 Color: 186
Size: 333 Color: 86
Size: 66 Color: 25

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 150
Size: 738 Color: 123
Size: 144 Color: 58

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 190
Size: 302 Color: 82
Size: 56 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 192
Size: 291 Color: 80
Size: 58 Color: 20

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 156
Size: 631 Color: 118
Size: 124 Color: 50

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 146
Size: 835 Color: 128
Size: 166 Color: 63

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 185
Size: 337 Color: 87
Size: 66 Color: 26

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 139
Size: 971 Color: 136
Size: 192 Color: 69

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1854 Color: 176
Size: 398 Color: 96
Size: 76 Color: 31

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 195
Size: 262 Color: 76
Size: 52 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 174
Size: 441 Color: 97
Size: 88 Color: 33

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 161
Size: 561 Color: 114
Size: 94 Color: 39

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 145
Size: 947 Color: 133
Size: 56 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 140
Size: 970 Color: 135
Size: 192 Color: 67

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1999 Color: 194
Size: 275 Color: 77
Size: 54 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 197
Size: 246 Color: 74
Size: 48 Color: 12

Total size: 151320
Total free space: 0

