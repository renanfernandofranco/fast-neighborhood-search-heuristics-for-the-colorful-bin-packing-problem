Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 262 Color: 0
Size: 295 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 326 Color: 1
Size: 287 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 319 Color: 1
Size: 286 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 264 Color: 1
Size: 250 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 256 Color: 0
Size: 327 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 303 Color: 1
Size: 266 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 353 Color: 1
Size: 265 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 1
Size: 261 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 312 Color: 1
Size: 266 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 354 Color: 1
Size: 280 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 346 Color: 1
Size: 275 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 283 Color: 1
Size: 251 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 346 Color: 1
Size: 284 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 325 Color: 1
Size: 294 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 355 Color: 1
Size: 282 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 271 Color: 1
Size: 251 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 362 Color: 1
Size: 273 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 298 Color: 1
Size: 262 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 276 Color: 1
Size: 250 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 300 Color: 1
Size: 273 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 345 Color: 1
Size: 273 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 275 Color: 1
Size: 265 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 326 Color: 1
Size: 279 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 348 Color: 1
Size: 288 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 288 Color: 1
Size: 278 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 321 Color: 1
Size: 269 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 303 Color: 1
Size: 254 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 270 Color: 1
Size: 256 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 343 Color: 1
Size: 297 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 369 Color: 1
Size: 253 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 310 Color: 1
Size: 268 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 322 Color: 1
Size: 297 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 302 Color: 1
Size: 268 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 263 Color: 1
Size: 253 Color: 0

Total size: 34034
Total free space: 0

