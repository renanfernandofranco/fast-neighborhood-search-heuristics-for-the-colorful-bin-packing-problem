Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 81
Size: 322 Color: 49
Size: 288 Color: 31

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 70
Size: 293 Color: 35
Size: 347 Color: 65

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 96
Size: 288 Color: 32
Size: 263 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 99
Size: 250 Color: 2
Size: 253 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 84
Size: 325 Color: 51
Size: 263 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 86
Size: 325 Color: 52
Size: 263 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 79
Size: 309 Color: 44
Size: 301 Color: 40

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 97
Size: 273 Color: 23
Size: 273 Color: 22

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 78
Size: 312 Color: 45
Size: 299 Color: 39

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 87
Size: 330 Color: 56
Size: 257 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 89
Size: 317 Color: 46
Size: 263 Color: 15

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 6
Size: 250 Color: 3
Size: 250 Color: 1
Size: 250 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 62
Size: 332 Color: 57
Size: 328 Color: 54

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 61
Size: 340 Color: 60
Size: 320 Color: 48

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 93
Size: 304 Color: 41
Size: 258 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 95
Size: 293 Color: 34
Size: 266 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 72
Size: 343 Color: 64
Size: 294 Color: 37

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 76
Size: 340 Color: 59
Size: 284 Color: 28

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 75
Size: 372 Color: 74
Size: 254 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 85
Size: 329 Color: 55
Size: 259 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 73
Size: 355 Color: 67
Size: 274 Color: 24

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 71
Size: 356 Color: 68
Size: 283 Color: 27

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 83
Size: 306 Color: 42
Size: 283 Color: 26

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 80
Size: 354 Color: 66
Size: 256 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 98
Size: 270 Color: 21
Size: 251 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 94
Size: 293 Color: 36
Size: 267 Color: 20

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 90
Size: 325 Color: 53
Size: 251 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 91
Size: 289 Color: 33
Size: 284 Color: 29

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 88
Size: 297 Color: 38
Size: 286 Color: 30

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 77
Size: 333 Color: 58
Size: 280 Color: 25

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 101
Size: 500 Color: 100

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 324 Color: 50
Size: 360 Color: 69
Size: 317 Color: 47

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 82
Size: 343 Color: 63
Size: 255 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 92
Size: 309 Color: 43
Size: 259 Color: 14

Total size: 34034
Total free space: 0

