Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 399426 Color: 0
Size: 326901 Color: 0
Size: 273674 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 377061 Color: 4
Size: 268366 Color: 0
Size: 354574 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 270937 Color: 2
Size: 289265 Color: 3
Size: 439799 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 415274 Color: 2
Size: 277443 Color: 1
Size: 307284 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 448757 Color: 2
Size: 260867 Color: 4
Size: 290377 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 429950 Color: 2
Size: 297840 Color: 2
Size: 272211 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 488465 Color: 4
Size: 253713 Color: 0
Size: 257823 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 416198 Color: 1
Size: 325988 Color: 4
Size: 257815 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 453595 Color: 3
Size: 293318 Color: 0
Size: 253088 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 360025 Color: 3
Size: 327009 Color: 1
Size: 312967 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 407977 Color: 1
Size: 295284 Color: 3
Size: 296740 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 418335 Color: 4
Size: 280294 Color: 2
Size: 301372 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 453560 Color: 4
Size: 275265 Color: 1
Size: 271176 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 408713 Color: 4
Size: 339170 Color: 3
Size: 252118 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 365527 Color: 3
Size: 307622 Color: 3
Size: 326852 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 418870 Color: 3
Size: 325570 Color: 2
Size: 255561 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 429837 Color: 4
Size: 295277 Color: 3
Size: 274887 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 370090 Color: 3
Size: 348392 Color: 1
Size: 281519 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 326814 Color: 3
Size: 320991 Color: 3
Size: 352196 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 402812 Color: 0
Size: 280452 Color: 0
Size: 316737 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 412302 Color: 2
Size: 290901 Color: 0
Size: 296798 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 493013 Color: 2
Size: 254158 Color: 2
Size: 252830 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 395044 Color: 4
Size: 347396 Color: 1
Size: 257561 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 388274 Color: 1
Size: 341835 Color: 2
Size: 269892 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 341213 Color: 4
Size: 275450 Color: 3
Size: 383338 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 368495 Color: 2
Size: 308176 Color: 3
Size: 323330 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 257522 Color: 4
Size: 483225 Color: 2
Size: 259254 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 314318 Color: 1
Size: 320237 Color: 4
Size: 365446 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 453536 Color: 3
Size: 262311 Color: 0
Size: 284154 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 385331 Color: 4
Size: 333485 Color: 0
Size: 281185 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 412957 Color: 1
Size: 327367 Color: 0
Size: 259677 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 420334 Color: 1
Size: 311974 Color: 0
Size: 267693 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 430686 Color: 1
Size: 308811 Color: 3
Size: 260504 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 458829 Color: 3
Size: 278230 Color: 4
Size: 262942 Color: 1

Total size: 34000034
Total free space: 0

