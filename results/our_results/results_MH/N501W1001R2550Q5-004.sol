Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 297 Color: 4
Size: 268 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 300 Color: 0
Size: 256 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 266 Color: 0
Size: 260 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 350 Color: 4
Size: 286 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 297 Color: 3
Size: 282 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 301 Color: 3
Size: 275 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 311 Color: 3
Size: 288 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 304 Color: 0
Size: 264 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 354 Color: 3
Size: 256 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 369 Color: 0
Size: 263 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 2
Size: 339 Color: 0
Size: 254 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 306 Color: 1
Size: 275 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 269 Color: 0
Size: 256 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 281 Color: 3
Size: 274 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 341 Color: 4
Size: 264 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 323 Color: 4
Size: 277 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 3
Size: 273 Color: 1
Size: 255 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 4
Size: 334 Color: 0
Size: 324 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 0
Size: 285 Color: 4
Size: 266 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 289 Color: 4
Size: 274 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 358 Color: 1
Size: 250 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 3
Size: 299 Color: 1
Size: 283 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 288 Color: 3
Size: 255 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 3
Size: 339 Color: 0
Size: 314 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 317 Color: 1
Size: 267 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 352 Color: 0
Size: 277 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 290 Color: 3
Size: 268 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 351 Color: 2
Size: 275 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 318 Color: 4
Size: 284 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 362 Color: 0
Size: 262 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 279 Color: 1
Size: 270 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 324 Color: 4
Size: 308 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 312 Color: 2
Size: 264 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 271 Color: 0
Size: 258 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 317 Color: 2
Size: 288 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 309 Color: 2
Size: 254 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 3
Size: 308 Color: 4
Size: 276 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 289 Color: 1
Size: 263 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 4
Size: 333 Color: 1
Size: 285 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 4
Size: 267 Color: 1
Size: 262 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 320 Color: 4
Size: 313 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 4
Size: 347 Color: 3
Size: 299 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 319 Color: 4
Size: 282 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 4
Size: 345 Color: 4
Size: 293 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 330 Color: 0
Size: 287 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 311 Color: 0
Size: 294 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 3
Size: 330 Color: 4
Size: 254 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 2
Size: 306 Color: 1
Size: 287 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 341 Color: 0
Size: 284 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 329 Color: 1
Size: 300 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 338 Color: 1
Size: 276 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 369 Color: 3
Size: 254 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 2
Size: 299 Color: 4
Size: 286 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 314 Color: 0
Size: 288 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 256 Color: 3
Size: 251 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 268 Color: 3
Size: 260 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 313 Color: 0
Size: 278 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 2
Size: 408 Color: 0
Size: 258 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 354 Color: 2
Size: 261 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 339 Color: 2
Size: 282 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 272 Color: 0
Size: 250 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 334 Color: 1
Size: 270 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 257 Color: 0
Size: 252 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 322 Color: 3
Size: 260 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 284 Color: 2
Size: 280 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 356 Color: 1
Size: 252 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 359 Color: 0
Size: 265 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 4
Size: 269 Color: 3
Size: 269 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 345 Color: 1
Size: 262 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 2
Size: 309 Color: 2
Size: 287 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 330 Color: 1
Size: 282 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 367 Color: 0
Size: 262 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 310 Color: 3
Size: 290 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 318 Color: 1
Size: 296 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 280 Color: 2
Size: 252 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 339 Color: 3
Size: 252 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 281 Color: 0
Size: 253 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 2
Size: 282 Color: 3
Size: 280 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 299 Color: 3
Size: 257 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 259 Color: 1
Size: 252 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 320 Color: 4
Size: 253 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 306 Color: 3
Size: 303 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 3
Size: 308 Color: 1
Size: 255 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 350 Color: 1
Size: 257 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 370 Color: 4
Size: 259 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 351 Color: 3
Size: 284 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 320 Color: 4
Size: 289 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 305 Color: 3
Size: 278 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 3
Size: 334 Color: 4
Size: 319 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 365 Color: 4
Size: 250 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 329 Color: 2
Size: 258 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 314 Color: 3
Size: 313 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 335 Color: 3
Size: 253 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 0
Size: 316 Color: 4
Size: 284 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 349 Color: 3
Size: 250 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 342 Color: 0
Size: 302 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 309 Color: 3
Size: 284 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 341 Color: 2
Size: 265 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 0
Size: 326 Color: 1
Size: 319 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 301 Color: 0
Size: 295 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 295 Color: 3
Size: 268 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 316 Color: 2
Size: 283 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 342 Color: 0
Size: 254 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 307 Color: 0
Size: 275 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 0
Size: 250 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 310 Color: 2
Size: 261 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 364 Color: 1
Size: 268 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 3
Size: 257 Color: 1
Size: 254 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 2
Size: 324 Color: 4
Size: 314 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 349 Color: 0
Size: 256 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 307 Color: 0
Size: 266 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 272 Color: 0
Size: 271 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 254 Color: 1
Size: 253 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 276 Color: 3
Size: 257 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 307 Color: 3
Size: 294 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 4
Size: 355 Color: 1
Size: 283 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 2
Size: 347 Color: 1
Size: 307 Color: 3

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 336 Color: 3
Size: 261 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 347 Color: 0
Size: 258 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 322 Color: 0
Size: 275 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 290 Color: 0
Size: 252 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 370 Color: 0
Size: 259 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 3
Size: 321 Color: 4
Size: 312 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 308 Color: 3
Size: 282 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 4
Size: 283 Color: 3
Size: 263 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 3
Size: 330 Color: 2
Size: 258 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 297 Color: 3
Size: 268 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 282 Color: 4
Size: 275 Color: 2

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 333 Color: 3
Size: 254 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 267 Color: 2
Size: 250 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 303 Color: 3
Size: 294 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 304 Color: 3
Size: 282 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 313 Color: 0
Size: 275 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 4
Size: 289 Color: 3
Size: 260 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 0
Size: 306 Color: 3
Size: 270 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 327 Color: 4
Size: 292 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 328 Color: 1
Size: 259 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 366 Color: 1
Size: 259 Color: 4

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 337 Color: 1
Size: 262 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 287 Color: 1
Size: 266 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 345 Color: 4
Size: 299 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 271 Color: 4
Size: 257 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 312 Color: 2
Size: 307 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 355 Color: 0
Size: 254 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 0
Size: 338 Color: 1
Size: 311 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 280 Color: 2
Size: 275 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 338 Color: 2
Size: 307 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 282 Color: 0
Size: 265 Color: 2

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 326 Color: 0
Size: 265 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 303 Color: 3
Size: 258 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 289 Color: 3
Size: 268 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 1
Size: 251 Color: 4

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 277 Color: 0
Size: 264 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 4
Size: 319 Color: 4
Size: 319 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 318 Color: 1
Size: 273 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 300 Color: 4
Size: 261 Color: 3

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 2
Size: 301 Color: 1
Size: 277 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 260 Color: 2
Size: 257 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 335 Color: 1
Size: 295 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 4
Size: 335 Color: 2
Size: 268 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 333 Color: 0
Size: 255 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 4
Size: 344 Color: 1
Size: 295 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 295 Color: 3
Size: 275 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 261 Color: 4
Size: 257 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 292 Color: 2
Size: 260 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 2
Size: 266 Color: 1
Size: 251 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 292 Color: 3
Size: 277 Color: 3

Total size: 167167
Total free space: 0

