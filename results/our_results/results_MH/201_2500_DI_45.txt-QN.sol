Capicity Bin: 2404
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1188 Color: 138
Size: 364 Color: 93
Size: 340 Color: 88
Size: 204 Color: 69
Size: 144 Color: 57
Size: 104 Color: 44
Size: 60 Color: 22

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1338 Color: 145
Size: 890 Color: 127
Size: 128 Color: 52
Size: 36 Color: 8
Size: 12 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 140
Size: 1001 Color: 133
Size: 198 Color: 67

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 163
Size: 558 Color: 110
Size: 108 Color: 46

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 161
Size: 601 Color: 112
Size: 120 Color: 48

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 158
Size: 622 Color: 116
Size: 124 Color: 51

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 166
Size: 587 Color: 111
Size: 24 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 156
Size: 702 Color: 117
Size: 136 Color: 53

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 144
Size: 946 Color: 131
Size: 188 Color: 64

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 160
Size: 615 Color: 113
Size: 122 Color: 49

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 182
Size: 354 Color: 91
Size: 68 Color: 27

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 200
Size: 226 Color: 71
Size: 44 Color: 11

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 2022 Color: 185
Size: 322 Color: 85
Size: 40 Color: 9
Size: 20 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2059 Color: 191
Size: 333 Color: 87
Size: 12 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 154
Size: 713 Color: 119
Size: 142 Color: 56

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1203 Color: 139
Size: 1001 Color: 134
Size: 200 Color: 68

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 190
Size: 294 Color: 79
Size: 56 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 198
Size: 242 Color: 73
Size: 48 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 143
Size: 971 Color: 132
Size: 192 Color: 65

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 159
Size: 619 Color: 114
Size: 122 Color: 50

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 179
Size: 378 Color: 94
Size: 72 Color: 31

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 153
Size: 718 Color: 120
Size: 140 Color: 55

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 149
Size: 831 Color: 124
Size: 166 Color: 60

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 168
Size: 502 Color: 106
Size: 100 Color: 42

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 201
Size: 210 Color: 70
Size: 40 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 184
Size: 323 Color: 86
Size: 64 Color: 25

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 164
Size: 537 Color: 109
Size: 106 Color: 45

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1206 Color: 141
Size: 1002 Color: 135
Size: 196 Color: 66

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 178
Size: 296 Color: 81
Size: 176 Color: 63

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 147
Size: 915 Color: 130
Size: 110 Color: 47

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 199
Size: 242 Color: 72
Size: 44 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 176
Size: 406 Color: 97
Size: 80 Color: 33

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 148
Size: 842 Color: 125
Size: 168 Color: 61

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 193
Size: 282 Color: 78
Size: 56 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 155
Size: 707 Color: 118
Size: 140 Color: 54

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 146
Size: 859 Color: 126
Size: 170 Color: 62

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1987 Color: 183
Size: 349 Color: 90
Size: 68 Color: 28

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 162
Size: 622 Color: 115
Size: 64 Color: 26

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 151
Size: 805 Color: 122
Size: 160 Color: 59

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 142
Size: 1135 Color: 137
Size: 60 Color: 21

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 177
Size: 403 Color: 96
Size: 80 Color: 32

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 157
Size: 730 Color: 121
Size: 72 Color: 30

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 195
Size: 262 Color: 75
Size: 48 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 152
Size: 914 Color: 129
Size: 20 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 174
Size: 458 Color: 100
Size: 88 Color: 36

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1044 Color: 136
Size: 904 Color: 128
Size: 456 Color: 99

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1430 Color: 150
Size: 814 Color: 123
Size: 160 Color: 58

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1833 Color: 172
Size: 477 Color: 102
Size: 94 Color: 39

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 167
Size: 517 Color: 107
Size: 86 Color: 35

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2063 Color: 192
Size: 311 Color: 83
Size: 30 Color: 7

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 175
Size: 413 Color: 98
Size: 82 Color: 34

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 189
Size: 295 Color: 80
Size: 58 Color: 20

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 171
Size: 481 Color: 103
Size: 94 Color: 38

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 165
Size: 521 Color: 108
Size: 102 Color: 43

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 173
Size: 467 Color: 101
Size: 92 Color: 37

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 170
Size: 493 Color: 104
Size: 98 Color: 41

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 187
Size: 349 Color: 89
Size: 16 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 186
Size: 315 Color: 84
Size: 62 Color: 23

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 194
Size: 269 Color: 76
Size: 52 Color: 16

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 188
Size: 299 Color: 82
Size: 58 Color: 19

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 180
Size: 379 Color: 95
Size: 62 Color: 24

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 169
Size: 497 Color: 105
Size: 98 Color: 40

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 196
Size: 253 Color: 74
Size: 50 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1975 Color: 181
Size: 359 Color: 92
Size: 70 Color: 29

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2113 Color: 197
Size: 275 Color: 77
Size: 16 Color: 3

Total size: 156260
Total free space: 0

