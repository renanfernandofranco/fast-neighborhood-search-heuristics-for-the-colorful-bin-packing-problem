Capicity Bin: 7520
Lower Bound: 132

Bins used: 132
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3712 Color: 12
Size: 1040 Color: 4
Size: 920 Color: 6
Size: 608 Color: 4
Size: 472 Color: 1
Size: 408 Color: 5
Size: 360 Color: 19

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 6364 Color: 18
Size: 964 Color: 12
Size: 136 Color: 3
Size: 56 Color: 14

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 5398 Color: 18
Size: 1770 Color: 6
Size: 192 Color: 1
Size: 128 Color: 2
Size: 32 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5692 Color: 11
Size: 1524 Color: 6
Size: 304 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 2
Size: 1290 Color: 1
Size: 256 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3990 Color: 19
Size: 2942 Color: 12
Size: 588 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 0
Size: 644 Color: 2
Size: 120 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4918 Color: 3
Size: 2170 Color: 1
Size: 432 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 18
Size: 1654 Color: 1
Size: 328 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4538 Color: 13
Size: 2486 Color: 12
Size: 496 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 3766 Color: 8
Size: 3366 Color: 14
Size: 388 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4644 Color: 11
Size: 2404 Color: 19
Size: 472 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 14
Size: 710 Color: 17
Size: 140 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 8
Size: 842 Color: 7
Size: 88 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 7
Size: 669 Color: 9
Size: 132 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 5
Size: 882 Color: 12
Size: 176 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 5
Size: 1446 Color: 14
Size: 288 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6441 Color: 18
Size: 901 Color: 17
Size: 178 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 16
Size: 3012 Color: 17
Size: 600 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 6
Size: 1948 Color: 2
Size: 384 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 18
Size: 1878 Color: 18
Size: 128 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 3886 Color: 7
Size: 3030 Color: 10
Size: 604 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 10
Size: 721 Color: 7
Size: 144 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 13
Size: 1062 Color: 12
Size: 208 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6577 Color: 1
Size: 787 Color: 10
Size: 156 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6098 Color: 13
Size: 1186 Color: 5
Size: 236 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 16
Size: 1466 Color: 10
Size: 292 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 4
Size: 881 Color: 12
Size: 174 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5942 Color: 0
Size: 1318 Color: 16
Size: 260 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 17
Size: 2100 Color: 0
Size: 416 Color: 18

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4396 Color: 6
Size: 2604 Color: 12
Size: 520 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5353 Color: 2
Size: 1807 Color: 14
Size: 360 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 8
Size: 690 Color: 7
Size: 136 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4273 Color: 11
Size: 2707 Color: 18
Size: 540 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 7
Size: 1117 Color: 5
Size: 222 Color: 11

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6024 Color: 10
Size: 1144 Color: 12
Size: 352 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4695 Color: 16
Size: 2355 Color: 6
Size: 470 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 16
Size: 1132 Color: 9
Size: 120 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 4
Size: 1044 Color: 11
Size: 200 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 0
Size: 2868 Color: 18
Size: 568 Color: 14

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 6
Size: 684 Color: 11
Size: 136 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 3761 Color: 2
Size: 3133 Color: 2
Size: 626 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 2
Size: 868 Color: 5
Size: 168 Color: 13

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 9
Size: 830 Color: 9
Size: 164 Color: 16

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 13
Size: 1284 Color: 12
Size: 64 Color: 16

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 11
Size: 758 Color: 0
Size: 36 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 1
Size: 1291 Color: 11
Size: 256 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 12
Size: 642 Color: 15
Size: 128 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 16
Size: 892 Color: 17
Size: 176 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5061 Color: 2
Size: 2051 Color: 9
Size: 408 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6350 Color: 11
Size: 978 Color: 0
Size: 192 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5975 Color: 15
Size: 1365 Color: 12
Size: 180 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 3
Size: 1468 Color: 16
Size: 288 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6076 Color: 19
Size: 1204 Color: 16
Size: 240 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 19
Size: 742 Color: 5
Size: 148 Color: 13

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 1
Size: 1748 Color: 12
Size: 344 Color: 14

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 7
Size: 2874 Color: 19
Size: 496 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6683 Color: 5
Size: 699 Color: 16
Size: 138 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 4699 Color: 14
Size: 2351 Color: 14
Size: 470 Color: 16

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5803 Color: 8
Size: 1431 Color: 10
Size: 286 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 10
Size: 1121 Color: 17
Size: 224 Color: 5

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 19
Size: 2870 Color: 1
Size: 572 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5799 Color: 10
Size: 1435 Color: 4
Size: 286 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 3765 Color: 4
Size: 3131 Color: 1
Size: 624 Color: 5

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 18
Size: 873 Color: 4
Size: 174 Color: 5

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5754 Color: 2
Size: 1474 Color: 13
Size: 292 Color: 17

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6545 Color: 2
Size: 813 Color: 11
Size: 162 Color: 17

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4271 Color: 19
Size: 2709 Color: 17
Size: 540 Color: 13

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6653 Color: 14
Size: 723 Color: 16
Size: 144 Color: 5

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 10
Size: 1498 Color: 14
Size: 296 Color: 11

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5807 Color: 1
Size: 1429 Color: 6
Size: 284 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6047 Color: 6
Size: 1229 Color: 4
Size: 244 Color: 11

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 3762 Color: 12
Size: 3134 Color: 0
Size: 624 Color: 14

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 12
Size: 718 Color: 16
Size: 140 Color: 8

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6319 Color: 6
Size: 1001 Color: 15
Size: 200 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6043 Color: 8
Size: 1231 Color: 12
Size: 246 Color: 9

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5053 Color: 11
Size: 2057 Color: 15
Size: 410 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5324 Color: 18
Size: 1836 Color: 2
Size: 360 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6122 Color: 14
Size: 1166 Color: 11
Size: 232 Color: 6

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 3764 Color: 10
Size: 3132 Color: 9
Size: 624 Color: 6

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 4
Size: 660 Color: 17
Size: 128 Color: 8

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 4
Size: 1396 Color: 10
Size: 272 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 14
Size: 910 Color: 0
Size: 180 Color: 15

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 3
Size: 1204 Color: 19
Size: 232 Color: 18

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 1
Size: 1066 Color: 11
Size: 212 Color: 5

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5020 Color: 15
Size: 2084 Color: 14
Size: 416 Color: 13

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 5
Size: 1276 Color: 11
Size: 128 Color: 14

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 4
Size: 812 Color: 14
Size: 160 Color: 11

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6576 Color: 12
Size: 792 Color: 5
Size: 152 Color: 5

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 5
Size: 871 Color: 18
Size: 172 Color: 11

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4703 Color: 13
Size: 2349 Color: 17
Size: 468 Color: 10

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6209 Color: 13
Size: 1093 Color: 16
Size: 218 Color: 14

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 7
Size: 780 Color: 16
Size: 136 Color: 18

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 18
Size: 982 Color: 11
Size: 196 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 19
Size: 2492 Color: 5
Size: 496 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6445 Color: 16
Size: 897 Color: 11
Size: 178 Color: 5

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 14
Size: 1183 Color: 12
Size: 124 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6205 Color: 12
Size: 1097 Color: 18
Size: 218 Color: 14

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 2
Size: 1324 Color: 10
Size: 216 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 6558 Color: 0
Size: 870 Color: 6
Size: 92 Color: 7

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 3288 Color: 4
Size: 2800 Color: 15
Size: 1432 Color: 19

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 4922 Color: 13
Size: 2166 Color: 3
Size: 432 Color: 10

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 8
Size: 716 Color: 3
Size: 136 Color: 10

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5057 Color: 0
Size: 2053 Color: 0
Size: 410 Color: 18

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 2
Size: 751 Color: 11
Size: 148 Color: 19

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 4534 Color: 15
Size: 2490 Color: 18
Size: 496 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 7
Size: 971 Color: 9
Size: 192 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 10
Size: 1266 Color: 13
Size: 164 Color: 5

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6534 Color: 6
Size: 822 Color: 9
Size: 164 Color: 8

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6438 Color: 2
Size: 902 Color: 2
Size: 180 Color: 19

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6747 Color: 3
Size: 677 Color: 12
Size: 96 Color: 14

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6039 Color: 3
Size: 1235 Color: 18
Size: 246 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 7
Size: 905 Color: 0
Size: 180 Color: 14

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5506 Color: 16
Size: 1682 Color: 1
Size: 332 Color: 5

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 3
Size: 1420 Color: 19
Size: 280 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5351 Color: 0
Size: 1809 Color: 6
Size: 360 Color: 17

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6177 Color: 1
Size: 1121 Color: 1
Size: 222 Color: 5

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5238 Color: 14
Size: 2010 Color: 3
Size: 272 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6533 Color: 8
Size: 891 Color: 19
Size: 96 Color: 10

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 7
Size: 740 Color: 0
Size: 144 Color: 11

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 0
Size: 1556 Color: 2
Size: 304 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5242 Color: 6
Size: 1902 Color: 14
Size: 376 Color: 17

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6261 Color: 6
Size: 1051 Color: 9
Size: 208 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 6
Size: 2344 Color: 5
Size: 464 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 5595 Color: 5
Size: 1605 Color: 5
Size: 320 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6569 Color: 12
Size: 793 Color: 18
Size: 158 Color: 16

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6353 Color: 17
Size: 973 Color: 8
Size: 194 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 5
Size: 695 Color: 8
Size: 138 Color: 16

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5597 Color: 15
Size: 1603 Color: 6
Size: 320 Color: 11

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 14
Size: 2076 Color: 5
Size: 408 Color: 19

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6739 Color: 10
Size: 651 Color: 12
Size: 130 Color: 15

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6715 Color: 2
Size: 671 Color: 18
Size: 134 Color: 18

Total size: 992640
Total free space: 0

