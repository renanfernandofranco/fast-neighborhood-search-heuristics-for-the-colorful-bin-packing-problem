Capicity Bin: 16256
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9978 Color: 1
Size: 5206 Color: 1
Size: 1072 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 14354 Color: 1
Size: 1434 Color: 1
Size: 468 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13852 Color: 1
Size: 2004 Color: 1
Size: 400 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 14387 Color: 1
Size: 1389 Color: 1
Size: 480 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 1
Size: 1450 Color: 1
Size: 312 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12653 Color: 1
Size: 3273 Color: 1
Size: 330 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 1
Size: 2016 Color: 1
Size: 524 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14242 Color: 1
Size: 1822 Color: 1
Size: 192 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1168 Color: 1
Size: 556 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 1
Size: 3108 Color: 1
Size: 616 Color: 0

Bin 11: 0 of cap free
Amount of items: 5
Items: 
Size: 10678 Color: 1
Size: 2056 Color: 1
Size: 1882 Color: 1
Size: 1040 Color: 0
Size: 600 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 1
Size: 1802 Color: 1
Size: 392 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 8133 Color: 1
Size: 6771 Color: 1
Size: 720 Color: 0
Size: 632 Color: 0

Bin 14: 0 of cap free
Amount of items: 5
Items: 
Size: 6568 Color: 1
Size: 4650 Color: 1
Size: 3466 Color: 1
Size: 1160 Color: 0
Size: 412 Color: 0

Bin 15: 0 of cap free
Amount of items: 5
Items: 
Size: 8130 Color: 1
Size: 3957 Color: 1
Size: 3017 Color: 1
Size: 752 Color: 0
Size: 400 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 1
Size: 1564 Color: 1
Size: 424 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 1910 Color: 1
Size: 686 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14269 Color: 1
Size: 1699 Color: 1
Size: 288 Color: 0

Bin 19: 0 of cap free
Amount of items: 5
Items: 
Size: 9252 Color: 1
Size: 3110 Color: 1
Size: 2702 Color: 1
Size: 704 Color: 0
Size: 488 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 1
Size: 1544 Color: 1
Size: 384 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 1
Size: 1444 Color: 1
Size: 480 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 1
Size: 1546 Color: 1
Size: 192 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9138 Color: 1
Size: 5934 Color: 1
Size: 1184 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 1
Size: 1972 Color: 1
Size: 360 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8132 Color: 1
Size: 6772 Color: 1
Size: 1352 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 1
Size: 3596 Color: 1
Size: 632 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12526 Color: 1
Size: 3302 Color: 1
Size: 428 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11838 Color: 1
Size: 3546 Color: 1
Size: 872 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1604 Color: 1
Size: 262 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12657 Color: 1
Size: 3001 Color: 1
Size: 598 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14162 Color: 1
Size: 1662 Color: 1
Size: 432 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8129 Color: 1
Size: 6773 Color: 1
Size: 1354 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 1
Size: 2142 Color: 1
Size: 790 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14444 Color: 1
Size: 1688 Color: 1
Size: 124 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13766 Color: 1
Size: 2078 Color: 1
Size: 412 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 1
Size: 2356 Color: 1
Size: 532 Color: 0

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 9816 Color: 1
Size: 3052 Color: 1
Size: 2512 Color: 1
Size: 684 Color: 0
Size: 192 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 1
Size: 1864 Color: 1
Size: 352 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 1
Size: 3942 Color: 1
Size: 312 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 1
Size: 2380 Color: 1
Size: 364 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14094 Color: 1
Size: 1906 Color: 1
Size: 256 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14403 Color: 1
Size: 1545 Color: 1
Size: 308 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 1
Size: 1582 Color: 1
Size: 408 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 1
Size: 2481 Color: 1
Size: 496 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 1
Size: 1804 Color: 1
Size: 1160 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13515 Color: 1
Size: 2285 Color: 1
Size: 456 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13863 Color: 1
Size: 1945 Color: 1
Size: 448 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11521 Color: 1
Size: 3947 Color: 1
Size: 788 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 10244 Color: 1
Size: 4988 Color: 1
Size: 1024 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12115 Color: 1
Size: 3429 Color: 1
Size: 712 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 1
Size: 1948 Color: 1
Size: 904 Color: 0

Bin 52: 0 of cap free
Amount of items: 5
Items: 
Size: 10276 Color: 1
Size: 2725 Color: 1
Size: 1755 Color: 1
Size: 1172 Color: 0
Size: 328 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 1
Size: 4792 Color: 1
Size: 416 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12035 Color: 1
Size: 3431 Color: 1
Size: 790 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 3016 Color: 1
Size: 336 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14089 Color: 1
Size: 1655 Color: 1
Size: 512 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 8134 Color: 1
Size: 6770 Color: 1
Size: 1352 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 1
Size: 5012 Color: 1
Size: 20 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 11004 Color: 1
Size: 4260 Color: 1
Size: 992 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14226 Color: 1
Size: 1558 Color: 1
Size: 472 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14507 Color: 1
Size: 1459 Color: 1
Size: 290 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 11736 Color: 1
Size: 4344 Color: 1
Size: 176 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 1
Size: 4984 Color: 1
Size: 320 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 1
Size: 7120 Color: 1
Size: 1000 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 1
Size: 6616 Color: 1
Size: 848 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 1
Size: 3768 Color: 1
Size: 464 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 12637 Color: 1
Size: 2927 Color: 1
Size: 692 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 11509 Color: 1
Size: 3451 Color: 1
Size: 1296 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 1
Size: 4284 Color: 1
Size: 856 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 12098 Color: 1
Size: 3454 Color: 1
Size: 704 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 1
Size: 2476 Color: 1
Size: 816 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 1
Size: 2330 Color: 1
Size: 912 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13705 Color: 1
Size: 2127 Color: 1
Size: 424 Color: 0

Bin 74: 0 of cap free
Amount of items: 5
Items: 
Size: 8392 Color: 1
Size: 3953 Color: 1
Size: 2663 Color: 1
Size: 928 Color: 0
Size: 320 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 11788 Color: 1
Size: 4076 Color: 1
Size: 392 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 1
Size: 6088 Color: 1
Size: 924 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 1
Size: 1660 Color: 1
Size: 348 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 1
Size: 2174 Color: 1
Size: 292 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 11370 Color: 1
Size: 4074 Color: 1
Size: 812 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 1
Size: 2444 Color: 1
Size: 544 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 1
Size: 1978 Color: 1
Size: 350 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 1
Size: 1830 Color: 1
Size: 274 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 6232 Color: 1
Size: 688 Color: 0

Bin 84: 0 of cap free
Amount of items: 5
Items: 
Size: 9142 Color: 1
Size: 4546 Color: 1
Size: 2184 Color: 1
Size: 288 Color: 0
Size: 96 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 1
Size: 2408 Color: 1
Size: 412 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13430 Color: 1
Size: 2550 Color: 1
Size: 276 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 1
Size: 2568 Color: 1
Size: 592 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 1
Size: 2724 Color: 1
Size: 544 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 1
Size: 1706 Color: 1
Size: 864 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 1
Size: 3176 Color: 1
Size: 432 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14479 Color: 1
Size: 1481 Color: 1
Size: 296 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13966 Color: 1
Size: 1746 Color: 1
Size: 544 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 1
Size: 4200 Color: 1
Size: 908 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 1
Size: 3528 Color: 1
Size: 336 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 1
Size: 1516 Color: 1
Size: 352 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 1
Size: 4602 Color: 1
Size: 916 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 2448 Color: 1
Size: 1352 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 1
Size: 1818 Color: 1
Size: 656 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 13206 Color: 1
Size: 2358 Color: 1
Size: 692 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 1
Size: 1828 Color: 1
Size: 360 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 1
Size: 3204 Color: 1
Size: 632 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 10280 Color: 1
Size: 5368 Color: 1
Size: 608 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 10204 Color: 1
Size: 5144 Color: 1
Size: 908 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 1
Size: 1372 Color: 1
Size: 444 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 13507 Color: 1
Size: 2129 Color: 1
Size: 620 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 13886 Color: 1
Size: 2062 Color: 1
Size: 308 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 12264 Color: 1
Size: 3656 Color: 1
Size: 336 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 9215 Color: 1
Size: 5869 Color: 1
Size: 1172 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 8148 Color: 1
Size: 6764 Color: 1
Size: 1344 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 1
Size: 2630 Color: 1
Size: 524 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 1
Size: 1974 Color: 1
Size: 352 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 10127 Color: 1
Size: 5109 Color: 1
Size: 1020 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 11756 Color: 1
Size: 3756 Color: 1
Size: 744 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 13061 Color: 1
Size: 2443 Color: 1
Size: 752 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 12131 Color: 1
Size: 3435 Color: 1
Size: 690 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 11505 Color: 1
Size: 3961 Color: 1
Size: 790 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 12559 Color: 1
Size: 3081 Color: 1
Size: 616 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 1
Size: 2208 Color: 1
Size: 34 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 1
Size: 3524 Color: 1
Size: 784 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 13006 Color: 1
Size: 2710 Color: 1
Size: 540 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 1
Size: 4104 Color: 1
Size: 816 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 1
Size: 2296 Color: 1
Size: 432 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 1
Size: 1588 Color: 1
Size: 458 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 1
Size: 5832 Color: 1
Size: 1152 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 1
Size: 3682 Color: 1
Size: 1044 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 1
Size: 1476 Color: 1
Size: 288 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 10877 Color: 1
Size: 4483 Color: 1
Size: 896 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 1
Size: 4481 Color: 1
Size: 896 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 1
Size: 5812 Color: 1
Size: 284 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14044 Color: 1
Size: 1844 Color: 1
Size: 368 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 13062 Color: 1
Size: 2662 Color: 1
Size: 532 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 1
Size: 3172 Color: 1
Size: 1352 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 1
Size: 3166 Color: 1
Size: 1008 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 1
Size: 1366 Color: 1
Size: 272 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 10786 Color: 1
Size: 4542 Color: 1
Size: 928 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 2088 Color: 1
Size: 992 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 12590 Color: 1
Size: 2482 Color: 1
Size: 1184 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 12135 Color: 1
Size: 3519 Color: 1
Size: 602 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 8016 Color: 1
Size: 5104 Color: 1
Size: 3136 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 1
Size: 1400 Color: 1
Size: 272 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 12960 Color: 1
Size: 2808 Color: 1
Size: 488 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 13586 Color: 1
Size: 1470 Color: 1
Size: 1200 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 1
Size: 2226 Color: 1
Size: 832 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 13110 Color: 1
Size: 2124 Color: 1
Size: 1022 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 1
Size: 1768 Color: 1
Size: 496 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 1
Size: 2748 Color: 1
Size: 912 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 1
Size: 6776 Color: 1
Size: 512 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 12143 Color: 1
Size: 3772 Color: 1
Size: 340 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 1
Size: 1379 Color: 1
Size: 304 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 14151 Color: 1
Size: 1248 Color: 0
Size: 856 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 1
Size: 1708 Color: 1
Size: 624 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 14408 Color: 1
Size: 1559 Color: 1
Size: 288 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 14339 Color: 1
Size: 1528 Color: 1
Size: 388 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 1
Size: 5111 Color: 1
Size: 496 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 13703 Color: 1
Size: 2280 Color: 1
Size: 272 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 10123 Color: 1
Size: 5844 Color: 1
Size: 288 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 1
Size: 1873 Color: 1
Size: 304 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 12114 Color: 1
Size: 3439 Color: 1
Size: 702 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 12139 Color: 1
Size: 3752 Color: 1
Size: 364 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 1
Size: 2667 Color: 1
Size: 686 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 13278 Color: 1
Size: 2291 Color: 1
Size: 686 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 1
Size: 1930 Color: 1
Size: 112 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 1
Size: 2542 Color: 1
Size: 448 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 1
Size: 5844 Color: 1
Size: 322 Color: 0

Bin 165: 2 of cap free
Amount of items: 4
Items: 
Size: 11880 Color: 1
Size: 3482 Color: 1
Size: 508 Color: 0
Size: 384 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 1
Size: 4626 Color: 1
Size: 544 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 1
Size: 2492 Color: 1
Size: 112 Color: 0

Bin 168: 2 of cap free
Amount of items: 5
Items: 
Size: 5930 Color: 1
Size: 4680 Color: 1
Size: 4562 Color: 1
Size: 708 Color: 0
Size: 374 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 10706 Color: 1
Size: 4316 Color: 1
Size: 1232 Color: 0

Bin 170: 3 of cap free
Amount of items: 3
Items: 
Size: 11513 Color: 1
Size: 4380 Color: 1
Size: 360 Color: 0

Bin 171: 3 of cap free
Amount of items: 3
Items: 
Size: 13325 Color: 1
Size: 2648 Color: 1
Size: 280 Color: 0

Bin 172: 3 of cap free
Amount of items: 3
Items: 
Size: 12745 Color: 1
Size: 2164 Color: 1
Size: 1344 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 10810 Color: 1
Size: 5115 Color: 1
Size: 328 Color: 0

Bin 174: 4 of cap free
Amount of items: 3
Items: 
Size: 12452 Color: 1
Size: 3336 Color: 1
Size: 464 Color: 0

Bin 175: 5 of cap free
Amount of items: 3
Items: 
Size: 10119 Color: 1
Size: 5772 Color: 1
Size: 360 Color: 0

Bin 176: 7 of cap free
Amount of items: 3
Items: 
Size: 14009 Color: 1
Size: 1944 Color: 1
Size: 296 Color: 0

Bin 177: 8 of cap free
Amount of items: 3
Items: 
Size: 12458 Color: 1
Size: 2798 Color: 1
Size: 992 Color: 0

Bin 178: 9 of cap free
Amount of items: 3
Items: 
Size: 9217 Color: 1
Size: 6008 Color: 1
Size: 1022 Color: 0

Bin 179: 16 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 1
Size: 3010 Color: 1
Size: 584 Color: 0

Bin 180: 16 of cap free
Amount of items: 3
Items: 
Size: 14235 Color: 1
Size: 1685 Color: 1
Size: 320 Color: 0

Bin 181: 17 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 1
Size: 1807 Color: 1
Size: 540 Color: 0

Bin 182: 17 of cap free
Amount of items: 3
Items: 
Size: 9332 Color: 1
Size: 5867 Color: 1
Size: 1040 Color: 0

Bin 183: 18 of cap free
Amount of items: 3
Items: 
Size: 10802 Color: 1
Size: 5044 Color: 1
Size: 392 Color: 0

Bin 184: 32 of cap free
Amount of items: 3
Items: 
Size: 11402 Color: 1
Size: 4442 Color: 1
Size: 380 Color: 0

Bin 185: 34 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 1
Size: 1608 Color: 1
Size: 1152 Color: 0

Bin 186: 46 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 2058 Color: 1
Size: 512 Color: 0

Bin 187: 57 of cap free
Amount of items: 3
Items: 
Size: 13267 Color: 1
Size: 2622 Color: 1
Size: 310 Color: 0

Bin 188: 72 of cap free
Amount of items: 3
Items: 
Size: 9448 Color: 1
Size: 6000 Color: 1
Size: 736 Color: 0

Bin 189: 94 of cap free
Amount of items: 3
Items: 
Size: 9284 Color: 1
Size: 6774 Color: 1
Size: 104 Color: 0

Bin 190: 402 of cap free
Amount of items: 3
Items: 
Size: 12987 Color: 1
Size: 2359 Color: 1
Size: 508 Color: 0

Bin 191: 524 of cap free
Amount of items: 3
Items: 
Size: 10010 Color: 1
Size: 5234 Color: 1
Size: 488 Color: 0

Bin 192: 1644 of cap free
Amount of items: 1
Items: 
Size: 14612 Color: 1

Bin 193: 1650 of cap free
Amount of items: 1
Items: 
Size: 14606 Color: 1

Bin 194: 1653 of cap free
Amount of items: 1
Items: 
Size: 14603 Color: 1

Bin 195: 1665 of cap free
Amount of items: 1
Items: 
Size: 14591 Color: 1

Bin 196: 1718 of cap free
Amount of items: 1
Items: 
Size: 14538 Color: 1

Bin 197: 1894 of cap free
Amount of items: 1
Items: 
Size: 14362 Color: 1

Bin 198: 2156 of cap free
Amount of items: 1
Items: 
Size: 14100 Color: 1

Bin 199: 2456 of cap free
Amount of items: 1
Items: 
Size: 13800 Color: 1

Total size: 3218688
Total free space: 16256

