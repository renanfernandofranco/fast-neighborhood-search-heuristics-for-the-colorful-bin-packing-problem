Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3336
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 365799 Color: 12
Size: 338977 Color: 2
Size: 295225 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 440758 Color: 14
Size: 286125 Color: 9
Size: 273118 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 417918 Color: 11
Size: 295898 Color: 9
Size: 286185 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 402681 Color: 5
Size: 330859 Color: 6
Size: 266461 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 381704 Color: 15
Size: 316022 Color: 5
Size: 302275 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 476882 Color: 3
Size: 270728 Color: 2
Size: 252391 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 387720 Color: 0
Size: 328794 Color: 17
Size: 283487 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 427864 Color: 1
Size: 294351 Color: 18
Size: 277786 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 410509 Color: 11
Size: 303490 Color: 0
Size: 286002 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 432564 Color: 15
Size: 290458 Color: 5
Size: 276979 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 478213 Color: 14
Size: 266415 Color: 12
Size: 255373 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 485921 Color: 16
Size: 263053 Color: 13
Size: 251027 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 483918 Color: 1
Size: 260904 Color: 10
Size: 255179 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 424324 Color: 5
Size: 288071 Color: 14
Size: 287606 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 359917 Color: 2
Size: 335395 Color: 6
Size: 304689 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 422506 Color: 9
Size: 318988 Color: 9
Size: 258507 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 372448 Color: 1
Size: 354760 Color: 13
Size: 272793 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 338717 Color: 0
Size: 338165 Color: 17
Size: 323119 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 418568 Color: 14
Size: 309091 Color: 2
Size: 272342 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 422355 Color: 11
Size: 308909 Color: 13
Size: 268737 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 472334 Color: 10
Size: 272937 Color: 12
Size: 254730 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 378497 Color: 0
Size: 318668 Color: 6
Size: 302836 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 483172 Color: 7
Size: 262080 Color: 14
Size: 254749 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 384065 Color: 1
Size: 316332 Color: 0
Size: 299604 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 393070 Color: 19
Size: 343271 Color: 9
Size: 263660 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 419534 Color: 15
Size: 321983 Color: 7
Size: 258484 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 489440 Color: 11
Size: 256136 Color: 18
Size: 254425 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 415412 Color: 4
Size: 310066 Color: 10
Size: 274523 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 379177 Color: 5
Size: 313352 Color: 17
Size: 307472 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 374960 Color: 1
Size: 355851 Color: 19
Size: 269190 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 376886 Color: 7
Size: 335731 Color: 9
Size: 287384 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 367217 Color: 9
Size: 344429 Color: 12
Size: 288355 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 384962 Color: 10
Size: 332606 Color: 15
Size: 282433 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 430632 Color: 15
Size: 307172 Color: 10
Size: 262197 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 491191 Color: 10
Size: 257065 Color: 8
Size: 251745 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 423735 Color: 19
Size: 296415 Color: 1
Size: 279851 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 458075 Color: 13
Size: 275471 Color: 0
Size: 266455 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 377570 Color: 15
Size: 329153 Color: 11
Size: 293278 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 413126 Color: 10
Size: 309968 Color: 11
Size: 276907 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 388322 Color: 3
Size: 331048 Color: 11
Size: 280631 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 349181 Color: 8
Size: 329261 Color: 18
Size: 321559 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 459402 Color: 15
Size: 289405 Color: 8
Size: 251194 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 482379 Color: 4
Size: 263613 Color: 1
Size: 254009 Color: 15

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 427494 Color: 5
Size: 294322 Color: 13
Size: 278185 Color: 19

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 428122 Color: 2
Size: 308099 Color: 10
Size: 263780 Color: 17

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 472003 Color: 17
Size: 265860 Color: 15
Size: 262138 Color: 9

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 437379 Color: 12
Size: 308723 Color: 13
Size: 253899 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 428335 Color: 19
Size: 312561 Color: 5
Size: 259105 Color: 16

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 406756 Color: 2
Size: 328473 Color: 19
Size: 264772 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 425891 Color: 13
Size: 306157 Color: 12
Size: 267953 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 412697 Color: 3
Size: 304341 Color: 8
Size: 282963 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 413969 Color: 7
Size: 318718 Color: 14
Size: 267314 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 416815 Color: 15
Size: 312349 Color: 16
Size: 270837 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 375429 Color: 6
Size: 361911 Color: 10
Size: 262661 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 464532 Color: 13
Size: 274516 Color: 17
Size: 260953 Color: 8

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 455777 Color: 0
Size: 292260 Color: 13
Size: 251964 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 403876 Color: 3
Size: 344963 Color: 16
Size: 251162 Color: 18

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 395011 Color: 5
Size: 346335 Color: 17
Size: 258655 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 390674 Color: 9
Size: 306019 Color: 14
Size: 303308 Color: 16

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 453356 Color: 15
Size: 286148 Color: 10
Size: 260497 Color: 9

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 453170 Color: 12
Size: 294098 Color: 0
Size: 252733 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 476460 Color: 7
Size: 270766 Color: 4
Size: 252775 Color: 18

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 388217 Color: 17
Size: 359506 Color: 2
Size: 252278 Color: 16

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 388110 Color: 3
Size: 352218 Color: 16
Size: 259673 Color: 7

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 475928 Color: 16
Size: 268453 Color: 18
Size: 255620 Color: 17

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 436378 Color: 6
Size: 306907 Color: 10
Size: 256716 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 376286 Color: 13
Size: 331514 Color: 2
Size: 292201 Color: 19

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 472770 Color: 13
Size: 274116 Color: 7
Size: 253115 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 340884 Color: 19
Size: 335389 Color: 7
Size: 323728 Color: 14

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 399308 Color: 19
Size: 326435 Color: 16
Size: 274258 Color: 5

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 406592 Color: 1
Size: 313952 Color: 4
Size: 279457 Color: 10

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 486400 Color: 13
Size: 262731 Color: 9
Size: 250870 Color: 13

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 407643 Color: 19
Size: 327099 Color: 7
Size: 265259 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 385589 Color: 16
Size: 328773 Color: 4
Size: 285639 Color: 8

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 482197 Color: 13
Size: 258987 Color: 15
Size: 258817 Color: 7

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 379926 Color: 19
Size: 337136 Color: 12
Size: 282939 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 483081 Color: 16
Size: 260728 Color: 12
Size: 256192 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 480256 Color: 18
Size: 269553 Color: 7
Size: 250192 Color: 5

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 405973 Color: 7
Size: 322306 Color: 16
Size: 271722 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 380569 Color: 12
Size: 312118 Color: 4
Size: 307314 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 387088 Color: 5
Size: 315383 Color: 11
Size: 297530 Color: 11

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 467385 Color: 5
Size: 275455 Color: 2
Size: 257161 Color: 16

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 372531 Color: 19
Size: 350980 Color: 18
Size: 276490 Color: 14

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 408182 Color: 10
Size: 330309 Color: 11
Size: 261510 Color: 12

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 364945 Color: 7
Size: 322673 Color: 16
Size: 312383 Color: 18

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 435213 Color: 11
Size: 306057 Color: 16
Size: 258731 Color: 14

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 410861 Color: 19
Size: 320760 Color: 3
Size: 268380 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 359773 Color: 6
Size: 325201 Color: 0
Size: 315027 Color: 7

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 381219 Color: 15
Size: 359268 Color: 6
Size: 259514 Color: 18

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 496366 Color: 2
Size: 252768 Color: 12
Size: 250867 Color: 18

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 377417 Color: 13
Size: 348082 Color: 6
Size: 274502 Color: 16

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 382407 Color: 18
Size: 309001 Color: 6
Size: 308593 Color: 8

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 419994 Color: 4
Size: 311077 Color: 19
Size: 268930 Color: 5

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 376328 Color: 15
Size: 363789 Color: 11
Size: 259884 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 344236 Color: 4
Size: 328544 Color: 5
Size: 327221 Color: 15

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 444873 Color: 18
Size: 295148 Color: 12
Size: 259980 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 393682 Color: 7
Size: 327891 Color: 7
Size: 278428 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 399225 Color: 3
Size: 348400 Color: 1
Size: 252376 Color: 19

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 422891 Color: 16
Size: 300655 Color: 17
Size: 276455 Color: 15

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 388077 Color: 12
Size: 306965 Color: 15
Size: 304959 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 412020 Color: 19
Size: 315056 Color: 15
Size: 272925 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 416476 Color: 3
Size: 296512 Color: 9
Size: 287013 Color: 19

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 393012 Color: 9
Size: 349947 Color: 0
Size: 257042 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 380736 Color: 5
Size: 313773 Color: 10
Size: 305492 Color: 17

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 450985 Color: 15
Size: 286254 Color: 19
Size: 262762 Color: 13

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 352906 Color: 4
Size: 339807 Color: 17
Size: 307288 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 420996 Color: 2
Size: 295986 Color: 3
Size: 283019 Color: 16

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 462877 Color: 17
Size: 268988 Color: 13
Size: 268136 Color: 14

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 392706 Color: 4
Size: 347132 Color: 13
Size: 260163 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 448149 Color: 3
Size: 293977 Color: 6
Size: 257875 Color: 16

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 394873 Color: 3
Size: 339208 Color: 6
Size: 265920 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 489474 Color: 19
Size: 259877 Color: 8
Size: 250650 Color: 11

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 472190 Color: 1
Size: 271791 Color: 12
Size: 256020 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 441118 Color: 19
Size: 290201 Color: 5
Size: 268682 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 495525 Color: 17
Size: 254437 Color: 5
Size: 250039 Color: 9

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 430548 Color: 5
Size: 286745 Color: 4
Size: 282708 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 392049 Color: 18
Size: 313670 Color: 9
Size: 294282 Color: 12

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 404497 Color: 2
Size: 343839 Color: 16
Size: 251665 Color: 8

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 358444 Color: 9
Size: 339090 Color: 17
Size: 302467 Color: 8

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 458773 Color: 11
Size: 284737 Color: 3
Size: 256491 Color: 9

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 404489 Color: 4
Size: 314116 Color: 6
Size: 281396 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 419087 Color: 12
Size: 298521 Color: 9
Size: 282393 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 383010 Color: 17
Size: 357545 Color: 18
Size: 259446 Color: 12

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 408544 Color: 6
Size: 326156 Color: 10
Size: 265301 Color: 10

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 398087 Color: 5
Size: 314595 Color: 3
Size: 287319 Color: 8

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 461141 Color: 18
Size: 285242 Color: 0
Size: 253618 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 488818 Color: 2
Size: 257462 Color: 13
Size: 253721 Color: 5

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 389215 Color: 15
Size: 344947 Color: 2
Size: 265839 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 452008 Color: 1
Size: 293552 Color: 2
Size: 254441 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 419227 Color: 18
Size: 315994 Color: 11
Size: 264780 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 472398 Color: 0
Size: 273870 Color: 19
Size: 253733 Color: 10

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 386983 Color: 12
Size: 309002 Color: 10
Size: 304016 Color: 14

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 369953 Color: 16
Size: 328378 Color: 18
Size: 301670 Color: 13

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 481588 Color: 3
Size: 261870 Color: 5
Size: 256543 Color: 4

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 464853 Color: 15
Size: 284066 Color: 10
Size: 251082 Color: 19

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 481314 Color: 4
Size: 263760 Color: 15
Size: 254927 Color: 15

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 441919 Color: 10
Size: 308061 Color: 7
Size: 250021 Color: 4

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 387023 Color: 0
Size: 343065 Color: 9
Size: 269913 Color: 5

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 418554 Color: 11
Size: 300256 Color: 7
Size: 281191 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 373814 Color: 15
Size: 324502 Color: 0
Size: 301685 Color: 9

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 466527 Color: 17
Size: 272933 Color: 18
Size: 260541 Color: 10

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 361162 Color: 2
Size: 356775 Color: 15
Size: 282064 Color: 5

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 363147 Color: 19
Size: 338805 Color: 15
Size: 298049 Color: 16

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 461798 Color: 1
Size: 282731 Color: 2
Size: 255472 Color: 19

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 378392 Color: 0
Size: 336544 Color: 5
Size: 285065 Color: 8

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 435205 Color: 9
Size: 285391 Color: 3
Size: 279405 Color: 13

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 417955 Color: 10
Size: 291909 Color: 3
Size: 290137 Color: 16

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 355053 Color: 18
Size: 328894 Color: 8
Size: 316054 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 390723 Color: 13
Size: 346131 Color: 4
Size: 263147 Color: 11

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 388769 Color: 6
Size: 341925 Color: 15
Size: 269307 Color: 7

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 420419 Color: 8
Size: 307025 Color: 13
Size: 272557 Color: 9

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 441880 Color: 5
Size: 287714 Color: 1
Size: 270407 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 384472 Color: 5
Size: 352587 Color: 2
Size: 262942 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 362026 Color: 13
Size: 332595 Color: 0
Size: 305380 Color: 6

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 410758 Color: 12
Size: 323930 Color: 10
Size: 265313 Color: 14

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 381554 Color: 1
Size: 336743 Color: 8
Size: 281704 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 479259 Color: 14
Size: 269599 Color: 2
Size: 251143 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 407419 Color: 2
Size: 341573 Color: 7
Size: 251009 Color: 19

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 437792 Color: 13
Size: 307027 Color: 9
Size: 255182 Color: 16

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 427449 Color: 15
Size: 288124 Color: 2
Size: 284428 Color: 3

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 429370 Color: 5
Size: 305247 Color: 10
Size: 265384 Color: 5

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 387438 Color: 15
Size: 358503 Color: 8
Size: 254060 Color: 16

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 487152 Color: 5
Size: 260022 Color: 1
Size: 252827 Color: 9

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 391540 Color: 11
Size: 332020 Color: 8
Size: 276441 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 388343 Color: 7
Size: 350443 Color: 8
Size: 261215 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 380775 Color: 17
Size: 349097 Color: 8
Size: 270129 Color: 17

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 424889 Color: 13
Size: 292841 Color: 2
Size: 282271 Color: 10

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 401051 Color: 6
Size: 316739 Color: 7
Size: 282211 Color: 5

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 411056 Color: 6
Size: 294741 Color: 15
Size: 294204 Color: 18

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 370021 Color: 6
Size: 316839 Color: 8
Size: 313141 Color: 4

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 445277 Color: 17
Size: 286842 Color: 14
Size: 267882 Color: 7

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 489189 Color: 9
Size: 260069 Color: 11
Size: 250743 Color: 11

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 488399 Color: 5
Size: 260078 Color: 1
Size: 251524 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 435100 Color: 13
Size: 308311 Color: 9
Size: 256590 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 415853 Color: 8
Size: 292728 Color: 3
Size: 291420 Color: 11

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 472643 Color: 6
Size: 263981 Color: 12
Size: 263377 Color: 4

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 425108 Color: 9
Size: 302966 Color: 5
Size: 271927 Color: 11

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 419842 Color: 16
Size: 311731 Color: 8
Size: 268428 Color: 15

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 404229 Color: 15
Size: 344661 Color: 5
Size: 251111 Color: 18

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 376430 Color: 13
Size: 350401 Color: 1
Size: 273170 Color: 15

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 441651 Color: 4
Size: 298184 Color: 10
Size: 260166 Color: 3

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 348330 Color: 14
Size: 346998 Color: 4
Size: 304673 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 440331 Color: 9
Size: 296922 Color: 11
Size: 262748 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 365488 Color: 6
Size: 330697 Color: 19
Size: 303816 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 408469 Color: 11
Size: 322295 Color: 9
Size: 269237 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 385759 Color: 9
Size: 339515 Color: 14
Size: 274727 Color: 3

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 399079 Color: 13
Size: 311622 Color: 6
Size: 289300 Color: 12

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 483379 Color: 8
Size: 259328 Color: 14
Size: 257294 Color: 18

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 374793 Color: 4
Size: 334609 Color: 9
Size: 290599 Color: 15

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 447129 Color: 9
Size: 289471 Color: 6
Size: 263401 Color: 4

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 475418 Color: 17
Size: 272515 Color: 9
Size: 252068 Color: 4

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 419926 Color: 8
Size: 321021 Color: 1
Size: 259054 Color: 19

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 382798 Color: 8
Size: 324847 Color: 4
Size: 292356 Color: 9

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 447348 Color: 13
Size: 288136 Color: 12
Size: 264517 Color: 19

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 464511 Color: 10
Size: 272790 Color: 19
Size: 262700 Color: 3

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 406365 Color: 9
Size: 337923 Color: 4
Size: 255713 Color: 13

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 482115 Color: 17
Size: 266682 Color: 18
Size: 251204 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 399528 Color: 2
Size: 340920 Color: 11
Size: 259553 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 483756 Color: 5
Size: 261248 Color: 7
Size: 254997 Color: 5

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 484336 Color: 11
Size: 262517 Color: 15
Size: 253148 Color: 6

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 445493 Color: 2
Size: 280010 Color: 17
Size: 274498 Color: 19

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 446078 Color: 16
Size: 284155 Color: 15
Size: 269768 Color: 4

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 412948 Color: 1
Size: 325698 Color: 5
Size: 261355 Color: 10

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 451568 Color: 6
Size: 288212 Color: 12
Size: 260221 Color: 15

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 417098 Color: 14
Size: 330791 Color: 4
Size: 252112 Color: 16

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 402232 Color: 11
Size: 310481 Color: 18
Size: 287288 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 389309 Color: 9
Size: 323443 Color: 19
Size: 287249 Color: 4

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 405736 Color: 2
Size: 335260 Color: 7
Size: 259005 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 409276 Color: 6
Size: 315048 Color: 12
Size: 275677 Color: 2

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 373330 Color: 12
Size: 332417 Color: 17
Size: 294254 Color: 8

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 397869 Color: 2
Size: 333057 Color: 17
Size: 269075 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 398368 Color: 1
Size: 318221 Color: 0
Size: 283412 Color: 15

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 372695 Color: 18
Size: 356814 Color: 5
Size: 270492 Color: 4

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 369832 Color: 13
Size: 366429 Color: 0
Size: 263740 Color: 10

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 457985 Color: 7
Size: 283070 Color: 8
Size: 258946 Color: 11

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 437891 Color: 19
Size: 281409 Color: 13
Size: 280701 Color: 10

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 467511 Color: 6
Size: 268513 Color: 2
Size: 263977 Color: 3

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 394387 Color: 7
Size: 352660 Color: 7
Size: 252954 Color: 2

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 422856 Color: 18
Size: 323039 Color: 16
Size: 254106 Color: 19

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 450796 Color: 13
Size: 279592 Color: 0
Size: 269613 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 452355 Color: 11
Size: 294488 Color: 19
Size: 253158 Color: 11

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 384123 Color: 8
Size: 312982 Color: 16
Size: 302896 Color: 6

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 455753 Color: 0
Size: 280507 Color: 1
Size: 263741 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 373186 Color: 4
Size: 342446 Color: 0
Size: 284369 Color: 12

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 388924 Color: 16
Size: 313426 Color: 2
Size: 297651 Color: 14

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 389904 Color: 0
Size: 350481 Color: 4
Size: 259616 Color: 17

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 459520 Color: 10
Size: 270737 Color: 11
Size: 269744 Color: 7

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 385861 Color: 10
Size: 364137 Color: 8
Size: 250003 Color: 8

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 475406 Color: 6
Size: 262982 Color: 13
Size: 261613 Color: 12

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 400059 Color: 2
Size: 324100 Color: 19
Size: 275842 Color: 6

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 363456 Color: 3
Size: 358737 Color: 19
Size: 277808 Color: 6

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 382038 Color: 15
Size: 334795 Color: 4
Size: 283168 Color: 18

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 370087 Color: 11
Size: 318503 Color: 3
Size: 311411 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 365434 Color: 6
Size: 359282 Color: 8
Size: 275285 Color: 16

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 443849 Color: 2
Size: 299952 Color: 17
Size: 256200 Color: 16

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 367075 Color: 15
Size: 346716 Color: 4
Size: 286210 Color: 6

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 470341 Color: 4
Size: 274526 Color: 18
Size: 255134 Color: 13

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 376995 Color: 12
Size: 361842 Color: 4
Size: 261164 Color: 10

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 357549 Color: 13
Size: 351755 Color: 10
Size: 290697 Color: 19

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 397317 Color: 2
Size: 314909 Color: 19
Size: 287775 Color: 7

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 409446 Color: 9
Size: 313961 Color: 19
Size: 276594 Color: 6

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 457699 Color: 7
Size: 290223 Color: 19
Size: 252079 Color: 15

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 387291 Color: 1
Size: 337573 Color: 5
Size: 275137 Color: 15

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 346314 Color: 6
Size: 344845 Color: 1
Size: 308842 Color: 7

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 373443 Color: 17
Size: 328338 Color: 13
Size: 298220 Color: 5

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 465338 Color: 0
Size: 280339 Color: 6
Size: 254324 Color: 19

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 479276 Color: 11
Size: 261847 Color: 7
Size: 258878 Color: 7

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 466886 Color: 5
Size: 272018 Color: 7
Size: 261097 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 477436 Color: 10
Size: 264517 Color: 4
Size: 258048 Color: 16

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 408483 Color: 5
Size: 339916 Color: 0
Size: 251602 Color: 3

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 387895 Color: 0
Size: 308942 Color: 7
Size: 303164 Color: 5

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 375933 Color: 12
Size: 324872 Color: 18
Size: 299196 Color: 15

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 419459 Color: 7
Size: 319676 Color: 18
Size: 260866 Color: 6

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 491487 Color: 12
Size: 257683 Color: 18
Size: 250831 Color: 3

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 413914 Color: 0
Size: 310866 Color: 7
Size: 275221 Color: 9

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 384169 Color: 17
Size: 334751 Color: 8
Size: 281081 Color: 11

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 399787 Color: 14
Size: 302827 Color: 12
Size: 297387 Color: 9

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 445772 Color: 11
Size: 299511 Color: 15
Size: 254718 Color: 15

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 351714 Color: 18
Size: 343552 Color: 19
Size: 304735 Color: 6

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 403911 Color: 13
Size: 299336 Color: 10
Size: 296754 Color: 12

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 396276 Color: 5
Size: 310787 Color: 7
Size: 292938 Color: 16

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 374340 Color: 7
Size: 338546 Color: 11
Size: 287115 Color: 4

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 380295 Color: 7
Size: 326838 Color: 6
Size: 292868 Color: 5

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 465980 Color: 19
Size: 270267 Color: 4
Size: 263754 Color: 19

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 343092 Color: 18
Size: 340707 Color: 7
Size: 316202 Color: 3

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 398405 Color: 1
Size: 302590 Color: 16
Size: 299006 Color: 4

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 377852 Color: 6
Size: 327210 Color: 2
Size: 294939 Color: 9

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 447918 Color: 14
Size: 300309 Color: 12
Size: 251774 Color: 3

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 354850 Color: 11
Size: 325346 Color: 7
Size: 319805 Color: 2

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 477645 Color: 7
Size: 267190 Color: 10
Size: 255166 Color: 16

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 453121 Color: 4
Size: 275194 Color: 9
Size: 271686 Color: 4

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 410741 Color: 12
Size: 315382 Color: 19
Size: 273878 Color: 11

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 369929 Color: 7
Size: 364446 Color: 8
Size: 265626 Color: 11

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 450074 Color: 18
Size: 280602 Color: 2
Size: 269325 Color: 14

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 363462 Color: 13
Size: 322478 Color: 16
Size: 314061 Color: 17

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 489408 Color: 15
Size: 258865 Color: 15
Size: 251728 Color: 12

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 387400 Color: 8
Size: 344520 Color: 17
Size: 268081 Color: 19

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 361010 Color: 19
Size: 341881 Color: 1
Size: 297110 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 392136 Color: 8
Size: 317457 Color: 2
Size: 290408 Color: 5

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 390750 Color: 5
Size: 310554 Color: 7
Size: 298697 Color: 10

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 438025 Color: 3
Size: 290338 Color: 11
Size: 271638 Color: 9

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 418166 Color: 17
Size: 318213 Color: 18
Size: 263622 Color: 16

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 338391 Color: 19
Size: 334670 Color: 15
Size: 326940 Color: 12

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 385423 Color: 9
Size: 344820 Color: 10
Size: 269758 Color: 12

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 490500 Color: 10
Size: 255819 Color: 19
Size: 253682 Color: 11

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 483998 Color: 19
Size: 264612 Color: 7
Size: 251391 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 407779 Color: 1
Size: 338819 Color: 13
Size: 253403 Color: 13

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 497328 Color: 12
Size: 251857 Color: 13
Size: 250816 Color: 19

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 414162 Color: 15
Size: 293170 Color: 3
Size: 292669 Color: 16

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 494683 Color: 17
Size: 255287 Color: 4
Size: 250031 Color: 4

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 452153 Color: 0
Size: 286927 Color: 8
Size: 260921 Color: 7

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 461665 Color: 16
Size: 272590 Color: 7
Size: 265746 Color: 6

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 383144 Color: 1
Size: 317378 Color: 12
Size: 299479 Color: 5

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 477227 Color: 10
Size: 262826 Color: 6
Size: 259948 Color: 10

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 396272 Color: 19
Size: 324480 Color: 1
Size: 279249 Color: 7

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 487783 Color: 2
Size: 256661 Color: 14
Size: 255557 Color: 6

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 404034 Color: 7
Size: 315070 Color: 11
Size: 280897 Color: 5

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 428249 Color: 14
Size: 307300 Color: 17
Size: 264452 Color: 10

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 376067 Color: 0
Size: 325152 Color: 17
Size: 298782 Color: 19

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 416281 Color: 1
Size: 328472 Color: 7
Size: 255248 Color: 17

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 429357 Color: 13
Size: 319034 Color: 11
Size: 251610 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 434719 Color: 8
Size: 286183 Color: 9
Size: 279099 Color: 11

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 372433 Color: 4
Size: 359574 Color: 8
Size: 267994 Color: 13

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 458714 Color: 2
Size: 284934 Color: 10
Size: 256353 Color: 13

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 386759 Color: 19
Size: 333879 Color: 9
Size: 279363 Color: 7

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 386739 Color: 18
Size: 351747 Color: 11
Size: 261515 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 423467 Color: 1
Size: 299684 Color: 5
Size: 276850 Color: 18

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 436773 Color: 16
Size: 301617 Color: 15
Size: 261611 Color: 3

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 409161 Color: 16
Size: 320080 Color: 12
Size: 270760 Color: 14

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 426440 Color: 4
Size: 307795 Color: 16
Size: 265766 Color: 6

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 395545 Color: 12
Size: 311084 Color: 8
Size: 293372 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 442496 Color: 9
Size: 306793 Color: 8
Size: 250712 Color: 14

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 497104 Color: 0
Size: 251517 Color: 9
Size: 251380 Color: 2

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 442865 Color: 14
Size: 295160 Color: 0
Size: 261976 Color: 19

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 400063 Color: 6
Size: 338899 Color: 15
Size: 261039 Color: 4

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 497786 Color: 7
Size: 251189 Color: 15
Size: 251026 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 342877 Color: 10
Size: 340375 Color: 5
Size: 316749 Color: 13

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 472249 Color: 1
Size: 264573 Color: 8
Size: 263179 Color: 2

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 370438 Color: 18
Size: 362326 Color: 10
Size: 267237 Color: 9

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 418365 Color: 8
Size: 320763 Color: 5
Size: 260873 Color: 8

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 437093 Color: 12
Size: 290978 Color: 13
Size: 271930 Color: 18

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 384379 Color: 15
Size: 310295 Color: 4
Size: 305327 Color: 14

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 372135 Color: 17
Size: 319132 Color: 10
Size: 308734 Color: 13

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 486436 Color: 3
Size: 257220 Color: 10
Size: 256345 Color: 19

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 436261 Color: 16
Size: 301804 Color: 13
Size: 261936 Color: 12

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 448452 Color: 17
Size: 299944 Color: 6
Size: 251605 Color: 12

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 391164 Color: 3
Size: 347601 Color: 3
Size: 261236 Color: 19

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 435757 Color: 9
Size: 289998 Color: 18
Size: 274246 Color: 17

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 359671 Color: 9
Size: 322423 Color: 19
Size: 317907 Color: 2

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 381537 Color: 4
Size: 338825 Color: 13
Size: 279639 Color: 3

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 347285 Color: 7
Size: 330911 Color: 1
Size: 321805 Color: 6

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 360886 Color: 8
Size: 333119 Color: 12
Size: 305996 Color: 12

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 349826 Color: 13
Size: 333985 Color: 4
Size: 316190 Color: 15

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 347989 Color: 6
Size: 330841 Color: 18
Size: 321171 Color: 17

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 363065 Color: 11
Size: 362724 Color: 16
Size: 274212 Color: 11

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 357349 Color: 4
Size: 327201 Color: 15
Size: 315451 Color: 15

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 358103 Color: 0
Size: 321224 Color: 6
Size: 320674 Color: 7

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 372232 Color: 11
Size: 357619 Color: 19
Size: 270150 Color: 5

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 345674 Color: 19
Size: 329344 Color: 1
Size: 324983 Color: 16

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 376823 Color: 16
Size: 356891 Color: 14
Size: 266287 Color: 2

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 342176 Color: 19
Size: 338874 Color: 2
Size: 318951 Color: 12

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 384202 Color: 15
Size: 343764 Color: 16
Size: 272035 Color: 10

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 345767 Color: 12
Size: 344121 Color: 18
Size: 310113 Color: 15

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 365969 Color: 10
Size: 356905 Color: 19
Size: 277127 Color: 16

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 393168 Color: 5
Size: 353672 Color: 16
Size: 253161 Color: 15

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 357226 Color: 14
Size: 344422 Color: 15
Size: 298353 Color: 13

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 372246 Color: 1
Size: 358813 Color: 1
Size: 268942 Color: 19

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 359871 Color: 10
Size: 336137 Color: 16
Size: 303993 Color: 5

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 359505 Color: 17
Size: 351923 Color: 18
Size: 288573 Color: 7

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 353146 Color: 3
Size: 328847 Color: 3
Size: 318008 Color: 11

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 354288 Color: 7
Size: 326267 Color: 19
Size: 319446 Color: 10

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 336369 Color: 15
Size: 336010 Color: 6
Size: 327622 Color: 12

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 348138 Color: 8
Size: 327448 Color: 5
Size: 324415 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 361521 Color: 6
Size: 358999 Color: 12
Size: 279481 Color: 2

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 356710 Color: 5
Size: 336257 Color: 2
Size: 307034 Color: 12

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 347606 Color: 5
Size: 346848 Color: 17
Size: 305547 Color: 15

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 368674 Color: 15
Size: 335603 Color: 4
Size: 295724 Color: 18

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 359765 Color: 2
Size: 327033 Color: 11
Size: 313203 Color: 19

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 369520 Color: 17
Size: 315307 Color: 18
Size: 315174 Color: 15

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 351204 Color: 15
Size: 339805 Color: 0
Size: 308992 Color: 3

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 349159 Color: 16
Size: 348939 Color: 7
Size: 301903 Color: 19

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 376716 Color: 4
Size: 355655 Color: 10
Size: 267630 Color: 8

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 335413 Color: 17
Size: 333263 Color: 0
Size: 331325 Color: 9

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 353690 Color: 15
Size: 333681 Color: 10
Size: 312630 Color: 6

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 353410 Color: 14
Size: 333697 Color: 1
Size: 312894 Color: 14

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 362187 Color: 6
Size: 351044 Color: 11
Size: 286770 Color: 19

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 347772 Color: 4
Size: 338772 Color: 10
Size: 313457 Color: 18

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 372040 Color: 16
Size: 342039 Color: 1
Size: 285922 Color: 2

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 340125 Color: 10
Size: 337056 Color: 13
Size: 322820 Color: 13

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 359896 Color: 2
Size: 343995 Color: 16
Size: 296110 Color: 19

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 369285 Color: 2
Size: 363968 Color: 18
Size: 266748 Color: 5

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 368402 Color: 16
Size: 353354 Color: 13
Size: 278245 Color: 12

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 359505 Color: 8
Size: 331720 Color: 5
Size: 308776 Color: 4

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 352954 Color: 13
Size: 331280 Color: 9
Size: 315767 Color: 5

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 361194 Color: 0
Size: 353634 Color: 5
Size: 285173 Color: 5

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 376801 Color: 6
Size: 327363 Color: 9
Size: 295837 Color: 17

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 357016 Color: 7
Size: 330611 Color: 7
Size: 312374 Color: 15

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 351208 Color: 14
Size: 330257 Color: 19
Size: 318536 Color: 17

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 358024 Color: 6
Size: 333295 Color: 16
Size: 308682 Color: 3

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 344809 Color: 4
Size: 333324 Color: 3
Size: 321868 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 360730 Color: 4
Size: 349418 Color: 14
Size: 289853 Color: 13

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 352180 Color: 1
Size: 341676 Color: 15
Size: 306145 Color: 3

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 341826 Color: 19
Size: 335953 Color: 12
Size: 322222 Color: 8

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 363189 Color: 0
Size: 339538 Color: 9
Size: 297274 Color: 13

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 353041 Color: 3
Size: 324313 Color: 17
Size: 322647 Color: 16

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 362272 Color: 11
Size: 350336 Color: 14
Size: 287393 Color: 3

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 341764 Color: 6
Size: 331902 Color: 19
Size: 326335 Color: 6

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 360964 Color: 17
Size: 346706 Color: 3
Size: 292331 Color: 8

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 358920 Color: 13
Size: 323303 Color: 2
Size: 317778 Color: 17

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 355312 Color: 4
Size: 346205 Color: 11
Size: 298484 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 340802 Color: 2
Size: 333707 Color: 19
Size: 325492 Color: 13

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 357196 Color: 19
Size: 327971 Color: 16
Size: 314834 Color: 11

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 340389 Color: 13
Size: 338745 Color: 0
Size: 320867 Color: 13

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 347256 Color: 1
Size: 339649 Color: 14
Size: 313096 Color: 6

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 376232 Color: 15
Size: 360581 Color: 12
Size: 263188 Color: 8

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 355884 Color: 18
Size: 349640 Color: 12
Size: 294477 Color: 2

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 343394 Color: 18
Size: 328921 Color: 15
Size: 327686 Color: 19

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 361570 Color: 16
Size: 335433 Color: 10
Size: 302998 Color: 5

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 349945 Color: 10
Size: 346934 Color: 13
Size: 303122 Color: 6

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 351842 Color: 9
Size: 339306 Color: 1
Size: 308853 Color: 6

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 371448 Color: 1
Size: 321388 Color: 5
Size: 307165 Color: 2

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 336441 Color: 0
Size: 333911 Color: 3
Size: 329649 Color: 7

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 354287 Color: 18
Size: 353927 Color: 10
Size: 291787 Color: 2

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 352702 Color: 15
Size: 334476 Color: 4
Size: 312823 Color: 11

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 360199 Color: 1
Size: 349220 Color: 5
Size: 290582 Color: 7

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 359297 Color: 2
Size: 358091 Color: 16
Size: 282613 Color: 6

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 362786 Color: 6
Size: 330341 Color: 12
Size: 306874 Color: 3

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 335475 Color: 0
Size: 333661 Color: 12
Size: 330865 Color: 9

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 344861 Color: 1
Size: 332600 Color: 8
Size: 322540 Color: 5

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 371309 Color: 15
Size: 359224 Color: 12
Size: 269468 Color: 11

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 349892 Color: 3
Size: 335188 Color: 12
Size: 314921 Color: 5

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 344441 Color: 17
Size: 332080 Color: 19
Size: 323480 Color: 7

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 365750 Color: 3
Size: 355822 Color: 1
Size: 278429 Color: 14

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 348698 Color: 6
Size: 329141 Color: 2
Size: 322162 Color: 8

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 343040 Color: 14
Size: 338232 Color: 11
Size: 318729 Color: 10

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 371263 Color: 9
Size: 360616 Color: 11
Size: 268122 Color: 19

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 362820 Color: 0
Size: 338208 Color: 16
Size: 298973 Color: 9

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 336560 Color: 17
Size: 332288 Color: 7
Size: 331153 Color: 1

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 338193 Color: 10
Size: 335761 Color: 8
Size: 326047 Color: 11

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 343838 Color: 11
Size: 341903 Color: 8
Size: 314260 Color: 6

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 373162 Color: 8
Size: 362468 Color: 2
Size: 264371 Color: 3

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 352359 Color: 0
Size: 338633 Color: 18
Size: 309009 Color: 15

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 363023 Color: 3
Size: 360108 Color: 8
Size: 276870 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 342763 Color: 19
Size: 336525 Color: 5
Size: 320713 Color: 18

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 368928 Color: 1
Size: 353850 Color: 9
Size: 277223 Color: 8

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 364088 Color: 11
Size: 354744 Color: 8
Size: 281169 Color: 9

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 363939 Color: 8
Size: 354610 Color: 4
Size: 281452 Color: 12

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 356922 Color: 15
Size: 341706 Color: 12
Size: 301373 Color: 5

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 354599 Color: 3
Size: 351543 Color: 0
Size: 293859 Color: 7

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 354751 Color: 12
Size: 349187 Color: 4
Size: 296063 Color: 9

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 355227 Color: 4
Size: 349528 Color: 15
Size: 295246 Color: 4

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 387997 Color: 19
Size: 354685 Color: 17
Size: 257319 Color: 15

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 360072 Color: 8
Size: 338394 Color: 9
Size: 301535 Color: 18

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 344854 Color: 1
Size: 343952 Color: 15
Size: 311195 Color: 4

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 362022 Color: 3
Size: 357037 Color: 16
Size: 280942 Color: 17

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 353913 Color: 18
Size: 351676 Color: 13
Size: 294412 Color: 11

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 352213 Color: 6
Size: 344612 Color: 16
Size: 303176 Color: 19

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 353242 Color: 9
Size: 351685 Color: 14
Size: 295074 Color: 9

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 351026 Color: 0
Size: 345790 Color: 15
Size: 303185 Color: 14

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 348564 Color: 3
Size: 339357 Color: 13
Size: 312080 Color: 10

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 360417 Color: 14
Size: 350343 Color: 8
Size: 289241 Color: 19

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 352852 Color: 11
Size: 350126 Color: 8
Size: 297023 Color: 14

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 358851 Color: 4
Size: 347334 Color: 13
Size: 293816 Color: 15

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 346108 Color: 10
Size: 344049 Color: 12
Size: 309844 Color: 8

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 347667 Color: 11
Size: 335066 Color: 7
Size: 317268 Color: 4

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 356190 Color: 18
Size: 355961 Color: 1
Size: 287850 Color: 6

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 345755 Color: 5
Size: 329820 Color: 18
Size: 324426 Color: 12

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 352950 Color: 18
Size: 340227 Color: 12
Size: 306824 Color: 7

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 346002 Color: 14
Size: 344544 Color: 9
Size: 309455 Color: 18

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 376718 Color: 0
Size: 350161 Color: 1
Size: 273122 Color: 9

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 351720 Color: 12
Size: 341212 Color: 10
Size: 307069 Color: 9

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 343698 Color: 12
Size: 342451 Color: 11
Size: 313852 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 397710 Color: 10
Size: 348735 Color: 12
Size: 253556 Color: 13

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 353724 Color: 11
Size: 347147 Color: 8
Size: 299130 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 351303 Color: 2
Size: 343061 Color: 2
Size: 305637 Color: 15

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 356700 Color: 7
Size: 345227 Color: 2
Size: 298074 Color: 15

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 342277 Color: 10
Size: 338683 Color: 12
Size: 319041 Color: 2

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 341698 Color: 7
Size: 341364 Color: 18
Size: 316939 Color: 14

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 352180 Color: 6
Size: 350827 Color: 18
Size: 296994 Color: 3

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 344612 Color: 18
Size: 341908 Color: 2
Size: 313481 Color: 12

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 355623 Color: 4
Size: 344282 Color: 10
Size: 300096 Color: 4

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 347034 Color: 16
Size: 340632 Color: 2
Size: 312335 Color: 15

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 349193 Color: 15
Size: 341147 Color: 18
Size: 309661 Color: 12

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 344038 Color: 13
Size: 336679 Color: 4
Size: 319284 Color: 2

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 345692 Color: 13
Size: 333601 Color: 3
Size: 320708 Color: 17

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 350737 Color: 16
Size: 344032 Color: 4
Size: 305232 Color: 9

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 343323 Color: 2
Size: 335266 Color: 11
Size: 321412 Color: 10

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 344506 Color: 13
Size: 335289 Color: 3
Size: 320206 Color: 7

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 351295 Color: 12
Size: 340709 Color: 10
Size: 307997 Color: 9

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 348488 Color: 16
Size: 345130 Color: 12
Size: 306383 Color: 18

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 346276 Color: 8
Size: 342459 Color: 19
Size: 311266 Color: 14

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 352205 Color: 11
Size: 342857 Color: 9
Size: 304939 Color: 18

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 345722 Color: 4
Size: 342695 Color: 1
Size: 311584 Color: 10

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 354233 Color: 3
Size: 342003 Color: 15
Size: 303765 Color: 2

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 355765 Color: 2
Size: 341466 Color: 14
Size: 302770 Color: 16

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 344212 Color: 1
Size: 341653 Color: 1
Size: 314136 Color: 3

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 352751 Color: 10
Size: 341255 Color: 17
Size: 305995 Color: 5

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 348548 Color: 10
Size: 341215 Color: 1
Size: 310238 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 357225 Color: 8
Size: 341735 Color: 8
Size: 301041 Color: 13

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 360317 Color: 15
Size: 341146 Color: 18
Size: 298538 Color: 18

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 351138 Color: 11
Size: 341013 Color: 1
Size: 307850 Color: 16

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 347524 Color: 3
Size: 340817 Color: 13
Size: 311660 Color: 9

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 339530 Color: 16
Size: 331745 Color: 19
Size: 328726 Color: 9

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 344048 Color: 7
Size: 336361 Color: 2
Size: 319592 Color: 13

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 348833 Color: 8
Size: 326881 Color: 0
Size: 324287 Color: 4

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 340285 Color: 4
Size: 331974 Color: 9
Size: 327742 Color: 2

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 342010 Color: 11
Size: 339211 Color: 19
Size: 318780 Color: 2

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 341058 Color: 19
Size: 339912 Color: 8
Size: 319031 Color: 3

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 359331 Color: 4
Size: 338935 Color: 6
Size: 301735 Color: 13

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 340592 Color: 0
Size: 338688 Color: 6
Size: 320721 Color: 10

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 342580 Color: 17
Size: 338646 Color: 5
Size: 318775 Color: 12

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 344503 Color: 4
Size: 337930 Color: 17
Size: 317568 Color: 10

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 350199 Color: 3
Size: 336481 Color: 3
Size: 313321 Color: 7

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 339340 Color: 10
Size: 330511 Color: 18
Size: 330150 Color: 6

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 353324 Color: 5
Size: 337887 Color: 18
Size: 308790 Color: 4

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 339210 Color: 1
Size: 335159 Color: 13
Size: 325632 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 348494 Color: 12
Size: 336838 Color: 6
Size: 314669 Color: 6

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 337595 Color: 11
Size: 336586 Color: 11
Size: 325820 Color: 15

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 345929 Color: 0
Size: 336680 Color: 3
Size: 317392 Color: 15

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 340416 Color: 7
Size: 334634 Color: 13
Size: 324951 Color: 15

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 341192 Color: 10
Size: 330017 Color: 1
Size: 328792 Color: 18

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 335525 Color: 13
Size: 334843 Color: 16
Size: 329633 Color: 2

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 347064 Color: 1
Size: 339616 Color: 12
Size: 313321 Color: 4

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 334572 Color: 14
Size: 333605 Color: 14
Size: 331824 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 355136 Color: 11
Size: 325406 Color: 17
Size: 319459 Color: 5

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 355469 Color: 11
Size: 333409 Color: 3
Size: 311123 Color: 9

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 355732 Color: 12
Size: 332807 Color: 9
Size: 311462 Color: 7

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 357021 Color: 1
Size: 322782 Color: 0
Size: 320198 Color: 9

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 357828 Color: 3
Size: 339776 Color: 18
Size: 302397 Color: 1

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 358300 Color: 0
Size: 353328 Color: 16
Size: 288373 Color: 7

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 358700 Color: 15
Size: 333711 Color: 3
Size: 307590 Color: 14

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 358814 Color: 7
Size: 349385 Color: 16
Size: 291802 Color: 14

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 359309 Color: 8
Size: 340893 Color: 11
Size: 299799 Color: 7

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 359346 Color: 0
Size: 338160 Color: 0
Size: 302495 Color: 6

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 359394 Color: 14
Size: 325873 Color: 4
Size: 314734 Color: 6

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 359575 Color: 13
Size: 344749 Color: 8
Size: 295677 Color: 17

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 359592 Color: 3
Size: 335231 Color: 18
Size: 305178 Color: 15

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 359840 Color: 4
Size: 320719 Color: 1
Size: 319442 Color: 17

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 360227 Color: 11
Size: 338516 Color: 12
Size: 301258 Color: 18

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 360300 Color: 14
Size: 333697 Color: 10
Size: 306004 Color: 13

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 360387 Color: 17
Size: 338173 Color: 10
Size: 301441 Color: 17

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 360282 Color: 18
Size: 321794 Color: 13
Size: 317925 Color: 5

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 360648 Color: 9
Size: 333386 Color: 1
Size: 305967 Color: 3

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 360765 Color: 1
Size: 333562 Color: 8
Size: 305674 Color: 11

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 361009 Color: 14
Size: 357789 Color: 12
Size: 281203 Color: 5

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 361179 Color: 7
Size: 342882 Color: 14
Size: 295940 Color: 9

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 361301 Color: 17
Size: 339459 Color: 19
Size: 299241 Color: 6

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 361534 Color: 16
Size: 323994 Color: 4
Size: 314473 Color: 14

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 361547 Color: 9
Size: 333725 Color: 15
Size: 304729 Color: 5

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 361857 Color: 1
Size: 330976 Color: 11
Size: 307168 Color: 6

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 361680 Color: 18
Size: 327753 Color: 5
Size: 310568 Color: 18

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 362096 Color: 0
Size: 342790 Color: 19
Size: 295115 Color: 3

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 362146 Color: 19
Size: 324485 Color: 1
Size: 313370 Color: 5

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 362149 Color: 3
Size: 347640 Color: 16
Size: 290212 Color: 19

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 362187 Color: 11
Size: 324141 Color: 4
Size: 313673 Color: 5

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 362203 Color: 18
Size: 321056 Color: 10
Size: 316742 Color: 5

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 362230 Color: 12
Size: 349876 Color: 1
Size: 287895 Color: 6

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 362256 Color: 1
Size: 361460 Color: 13
Size: 276285 Color: 16

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 362360 Color: 17
Size: 319257 Color: 0
Size: 318384 Color: 13

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 362386 Color: 13
Size: 356716 Color: 7
Size: 280899 Color: 6

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 362443 Color: 15
Size: 331435 Color: 9
Size: 306123 Color: 9

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 362473 Color: 12
Size: 323325 Color: 9
Size: 314203 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 362518 Color: 17
Size: 342726 Color: 15
Size: 294757 Color: 19

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 362690 Color: 10
Size: 359108 Color: 13
Size: 278203 Color: 13

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 362756 Color: 19
Size: 319073 Color: 1
Size: 318172 Color: 8

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 362658 Color: 18
Size: 333604 Color: 0
Size: 303739 Color: 4

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 362990 Color: 1
Size: 357422 Color: 7
Size: 279589 Color: 5

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 362896 Color: 18
Size: 357414 Color: 12
Size: 279691 Color: 5

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 363183 Color: 1
Size: 331981 Color: 2
Size: 304837 Color: 14

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 363183 Color: 10
Size: 325382 Color: 7
Size: 311436 Color: 5

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 363242 Color: 12
Size: 351191 Color: 12
Size: 285568 Color: 16

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 362939 Color: 18
Size: 343125 Color: 0
Size: 293937 Color: 19

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 363310 Color: 3
Size: 363158 Color: 19
Size: 273533 Color: 2

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 363319 Color: 5
Size: 330519 Color: 3
Size: 306163 Color: 11

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 363347 Color: 14
Size: 322521 Color: 6
Size: 314133 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 363404 Color: 14
Size: 328044 Color: 11
Size: 308553 Color: 7

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 363486 Color: 5
Size: 336198 Color: 3
Size: 300317 Color: 1

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 363496 Color: 9
Size: 333295 Color: 0
Size: 303210 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 363500 Color: 1
Size: 351987 Color: 10
Size: 284514 Color: 18

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 363653 Color: 8
Size: 330631 Color: 2
Size: 305717 Color: 12

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 363714 Color: 2
Size: 351799 Color: 19
Size: 284488 Color: 11

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 363719 Color: 0
Size: 331710 Color: 17
Size: 304572 Color: 17

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 363888 Color: 10
Size: 327464 Color: 19
Size: 308649 Color: 7

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 363993 Color: 11
Size: 319047 Color: 7
Size: 316961 Color: 1

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 364033 Color: 10
Size: 326014 Color: 9
Size: 309954 Color: 7

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 364038 Color: 5
Size: 348963 Color: 7
Size: 287000 Color: 4

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 364079 Color: 14
Size: 330320 Color: 0
Size: 305602 Color: 3

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 364202 Color: 0
Size: 352392 Color: 18
Size: 283407 Color: 1

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 364093 Color: 16
Size: 350877 Color: 14
Size: 285031 Color: 7

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 364305 Color: 9
Size: 339974 Color: 10
Size: 295722 Color: 8

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 364153 Color: 19
Size: 330017 Color: 2
Size: 305831 Color: 12

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 364327 Color: 10
Size: 352390 Color: 6
Size: 283284 Color: 11

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 364350 Color: 15
Size: 323710 Color: 8
Size: 311941 Color: 16

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 364355 Color: 15
Size: 318582 Color: 5
Size: 317064 Color: 17

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 364438 Color: 15
Size: 342558 Color: 16
Size: 293005 Color: 2

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 364201 Color: 19
Size: 323646 Color: 16
Size: 312154 Color: 12

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 364636 Color: 11
Size: 350601 Color: 16
Size: 284764 Color: 6

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 364656 Color: 2
Size: 360249 Color: 15
Size: 275096 Color: 11

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 364748 Color: 2
Size: 354286 Color: 18
Size: 280967 Color: 7

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 364818 Color: 10
Size: 339716 Color: 9
Size: 295467 Color: 8

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 364856 Color: 15
Size: 342811 Color: 13
Size: 292334 Color: 16

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 365147 Color: 0
Size: 341457 Color: 17
Size: 293397 Color: 10

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 365005 Color: 14
Size: 335722 Color: 15
Size: 299274 Color: 13

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 365028 Color: 4
Size: 337432 Color: 19
Size: 297541 Color: 6

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 365037 Color: 11
Size: 322698 Color: 5
Size: 312266 Color: 6

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 365159 Color: 6
Size: 325631 Color: 16
Size: 309211 Color: 10

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 365383 Color: 11
Size: 354128 Color: 4
Size: 280490 Color: 14

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 365313 Color: 9
Size: 319933 Color: 13
Size: 314755 Color: 8

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 365458 Color: 8
Size: 359712 Color: 10
Size: 274831 Color: 4

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 365527 Color: 1
Size: 350516 Color: 3
Size: 283958 Color: 13

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 365575 Color: 3
Size: 332989 Color: 10
Size: 301437 Color: 12

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 365610 Color: 0
Size: 332487 Color: 7
Size: 301904 Color: 12

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 365706 Color: 15
Size: 320180 Color: 16
Size: 314115 Color: 9

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 365707 Color: 13
Size: 325607 Color: 4
Size: 308687 Color: 19

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 365752 Color: 0
Size: 334749 Color: 9
Size: 299500 Color: 14

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 365761 Color: 8
Size: 360238 Color: 14
Size: 274002 Color: 8

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 365804 Color: 10
Size: 325230 Color: 18
Size: 308967 Color: 16

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 365858 Color: 0
Size: 362844 Color: 14
Size: 271299 Color: 11

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 365833 Color: 18
Size: 327900 Color: 16
Size: 306268 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 365842 Color: 10
Size: 344388 Color: 9
Size: 289771 Color: 18

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 365908 Color: 7
Size: 361804 Color: 5
Size: 272289 Color: 4

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 365909 Color: 9
Size: 353308 Color: 15
Size: 280784 Color: 2

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 365937 Color: 4
Size: 321379 Color: 9
Size: 312685 Color: 9

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 366028 Color: 12
Size: 340854 Color: 15
Size: 293119 Color: 3

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 366036 Color: 15
Size: 354836 Color: 3
Size: 279129 Color: 7

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 366068 Color: 8
Size: 323938 Color: 8
Size: 309995 Color: 18

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 366204 Color: 5
Size: 330072 Color: 15
Size: 303725 Color: 6

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 366060 Color: 11
Size: 332199 Color: 18
Size: 301742 Color: 10

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 366293 Color: 13
Size: 362895 Color: 4
Size: 270813 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 366443 Color: 9
Size: 358035 Color: 9
Size: 275523 Color: 8

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 366453 Color: 9
Size: 320818 Color: 4
Size: 312730 Color: 13

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 366530 Color: 1
Size: 342391 Color: 13
Size: 291080 Color: 2

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 366589 Color: 2
Size: 343916 Color: 0
Size: 289496 Color: 14

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 366639 Color: 1
Size: 328608 Color: 9
Size: 304754 Color: 16

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 366660 Color: 19
Size: 365106 Color: 5
Size: 268235 Color: 13

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 366844 Color: 13
Size: 326313 Color: 10
Size: 306844 Color: 16

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 366848 Color: 19
Size: 338624 Color: 14
Size: 294529 Color: 15

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 366864 Color: 17
Size: 323717 Color: 19
Size: 309420 Color: 6

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 367004 Color: 0
Size: 322554 Color: 19
Size: 310443 Color: 2

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 366899 Color: 18
Size: 320123 Color: 1
Size: 312979 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 366904 Color: 13
Size: 319336 Color: 18
Size: 313761 Color: 5

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 367017 Color: 13
Size: 343607 Color: 9
Size: 289377 Color: 9

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 367031 Color: 19
Size: 361927 Color: 18
Size: 271043 Color: 17

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 367069 Color: 12
Size: 356827 Color: 0
Size: 276105 Color: 12

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 367123 Color: 1
Size: 357504 Color: 1
Size: 275374 Color: 6

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 367272 Color: 7
Size: 360790 Color: 5
Size: 271939 Color: 10

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 367309 Color: 8
Size: 344714 Color: 10
Size: 287978 Color: 7

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 367604 Color: 0
Size: 337973 Color: 13
Size: 294424 Color: 13

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 367312 Color: 13
Size: 335252 Color: 1
Size: 297437 Color: 19

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 367318 Color: 2
Size: 340822 Color: 8
Size: 291861 Color: 3

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 367362 Color: 9
Size: 349587 Color: 16
Size: 283052 Color: 17

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 367453 Color: 10
Size: 345080 Color: 3
Size: 287468 Color: 15

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 367793 Color: 12
Size: 354167 Color: 8
Size: 278041 Color: 11

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 367806 Color: 9
Size: 366813 Color: 13
Size: 265382 Color: 4

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 367814 Color: 8
Size: 335022 Color: 17
Size: 297165 Color: 14

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 367818 Color: 5
Size: 366191 Color: 1
Size: 265992 Color: 17

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 367848 Color: 16
Size: 340619 Color: 13
Size: 291534 Color: 9

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 367850 Color: 17
Size: 319957 Color: 0
Size: 312194 Color: 11

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 367940 Color: 3
Size: 343868 Color: 6
Size: 288193 Color: 10

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 367964 Color: 3
Size: 349741 Color: 10
Size: 282296 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 367976 Color: 15
Size: 336591 Color: 17
Size: 295434 Color: 10

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 368046 Color: 16
Size: 340588 Color: 8
Size: 291367 Color: 3

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 368084 Color: 4
Size: 352875 Color: 18
Size: 279042 Color: 2

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 368161 Color: 15
Size: 328780 Color: 5
Size: 303060 Color: 6

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 368183 Color: 19
Size: 343640 Color: 13
Size: 288178 Color: 2

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 368257 Color: 2
Size: 347210 Color: 1
Size: 284534 Color: 4

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 368350 Color: 19
Size: 330221 Color: 9
Size: 301430 Color: 9

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 368312 Color: 15
Size: 332777 Color: 16
Size: 298912 Color: 16

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 368427 Color: 18
Size: 336430 Color: 3
Size: 295144 Color: 4

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 368452 Color: 5
Size: 356283 Color: 19
Size: 275266 Color: 12

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 368557 Color: 11
Size: 329412 Color: 2
Size: 302032 Color: 1

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 368665 Color: 2
Size: 336764 Color: 6
Size: 294572 Color: 2

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 368709 Color: 4
Size: 328691 Color: 18
Size: 302601 Color: 8

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 368713 Color: 4
Size: 343835 Color: 11
Size: 287453 Color: 7

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 368758 Color: 10
Size: 330879 Color: 5
Size: 300364 Color: 3

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 368777 Color: 3
Size: 356611 Color: 4
Size: 274613 Color: 5

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 368822 Color: 17
Size: 352901 Color: 14
Size: 278278 Color: 9

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 369200 Color: 0
Size: 356220 Color: 2
Size: 274581 Color: 10

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 369325 Color: 0
Size: 336441 Color: 8
Size: 294235 Color: 8

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 369012 Color: 4
Size: 345502 Color: 11
Size: 285487 Color: 11

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 369041 Color: 3
Size: 316323 Color: 6
Size: 314637 Color: 15

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 369307 Color: 8
Size: 346434 Color: 11
Size: 284260 Color: 3

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 369422 Color: 5
Size: 345452 Color: 3
Size: 285127 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 369426 Color: 3
Size: 363141 Color: 18
Size: 267434 Color: 16

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 369471 Color: 15
Size: 316195 Color: 5
Size: 314335 Color: 6

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 369472 Color: 1
Size: 332338 Color: 10
Size: 298191 Color: 8

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 369506 Color: 18
Size: 320366 Color: 6
Size: 310129 Color: 1

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 369520 Color: 10
Size: 367569 Color: 8
Size: 262912 Color: 17

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 369533 Color: 18
Size: 323890 Color: 19
Size: 306578 Color: 19

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 369538 Color: 14
Size: 319119 Color: 19
Size: 311344 Color: 3

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 369648 Color: 10
Size: 321231 Color: 13
Size: 309122 Color: 18

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 369665 Color: 10
Size: 324003 Color: 4
Size: 306333 Color: 13

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 369665 Color: 5
Size: 358229 Color: 13
Size: 272107 Color: 16

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 369700 Color: 16
Size: 321144 Color: 0
Size: 309157 Color: 19

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 369745 Color: 18
Size: 324848 Color: 17
Size: 305408 Color: 18

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 369757 Color: 16
Size: 326197 Color: 12
Size: 304047 Color: 12

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 369787 Color: 9
Size: 347851 Color: 6
Size: 282363 Color: 1

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 369802 Color: 19
Size: 358939 Color: 15
Size: 271260 Color: 1

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 369836 Color: 13
Size: 354830 Color: 8
Size: 275335 Color: 8

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 369953 Color: 7
Size: 316568 Color: 6
Size: 313480 Color: 6

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 369989 Color: 7
Size: 317967 Color: 15
Size: 312045 Color: 19

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 370007 Color: 7
Size: 349500 Color: 0
Size: 280494 Color: 2

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 370019 Color: 14
Size: 355312 Color: 13
Size: 274670 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 370055 Color: 12
Size: 332983 Color: 17
Size: 296963 Color: 7

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 370060 Color: 17
Size: 341033 Color: 19
Size: 288908 Color: 15

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 370228 Color: 7
Size: 350788 Color: 12
Size: 278985 Color: 2

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 370242 Color: 16
Size: 321011 Color: 10
Size: 308748 Color: 17

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 370444 Color: 7
Size: 333959 Color: 15
Size: 295598 Color: 4

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 370664 Color: 0
Size: 339121 Color: 7
Size: 290216 Color: 18

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 370475 Color: 10
Size: 319106 Color: 5
Size: 310420 Color: 11

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 370518 Color: 5
Size: 330747 Color: 10
Size: 298736 Color: 2

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 370569 Color: 19
Size: 350678 Color: 1
Size: 278754 Color: 16

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 370573 Color: 17
Size: 365161 Color: 14
Size: 264267 Color: 4

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 370714 Color: 0
Size: 341609 Color: 16
Size: 287678 Color: 15

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 370597 Color: 14
Size: 315425 Color: 9
Size: 313979 Color: 2

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 370601 Color: 11
Size: 335671 Color: 9
Size: 293729 Color: 5

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 370771 Color: 11
Size: 336764 Color: 4
Size: 292466 Color: 8

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 370792 Color: 6
Size: 343328 Color: 19
Size: 285881 Color: 1

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 370796 Color: 18
Size: 316131 Color: 7
Size: 313074 Color: 8

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 370816 Color: 4
Size: 335477 Color: 12
Size: 293708 Color: 0

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 370888 Color: 9
Size: 319130 Color: 7
Size: 309983 Color: 8

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 370936 Color: 6
Size: 324912 Color: 13
Size: 304153 Color: 17

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 370972 Color: 11
Size: 363510 Color: 18
Size: 265519 Color: 15

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 370973 Color: 2
Size: 325147 Color: 13
Size: 303881 Color: 6

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 371000 Color: 18
Size: 317483 Color: 4
Size: 311518 Color: 8

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 370992 Color: 0
Size: 335187 Color: 10
Size: 293822 Color: 2

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 371008 Color: 13
Size: 325105 Color: 16
Size: 303888 Color: 9

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 371014 Color: 9
Size: 328907 Color: 4
Size: 300080 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 371104 Color: 2
Size: 335995 Color: 18
Size: 292902 Color: 8

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 371166 Color: 6
Size: 336151 Color: 18
Size: 292684 Color: 15

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 371285 Color: 13
Size: 348579 Color: 13
Size: 280137 Color: 12

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 371290 Color: 9
Size: 319980 Color: 2
Size: 308731 Color: 10

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 371375 Color: 9
Size: 316401 Color: 19
Size: 312225 Color: 14

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 371397 Color: 1
Size: 326319 Color: 5
Size: 302285 Color: 1

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 371415 Color: 8
Size: 356608 Color: 14
Size: 271978 Color: 0

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 371418 Color: 6
Size: 320667 Color: 13
Size: 307916 Color: 1

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 371444 Color: 5
Size: 363162 Color: 12
Size: 265395 Color: 19

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 371564 Color: 11
Size: 337153 Color: 6
Size: 291284 Color: 16

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 371578 Color: 14
Size: 333719 Color: 16
Size: 294704 Color: 12

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 371604 Color: 15
Size: 318318 Color: 12
Size: 310079 Color: 9

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 371734 Color: 14
Size: 317770 Color: 13
Size: 310497 Color: 7

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 371831 Color: 8
Size: 324997 Color: 17
Size: 303173 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 371832 Color: 7
Size: 329289 Color: 6
Size: 298880 Color: 9

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 371862 Color: 8
Size: 347838 Color: 18
Size: 280301 Color: 5

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 371889 Color: 4
Size: 359654 Color: 6
Size: 268458 Color: 1

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 371930 Color: 10
Size: 324696 Color: 10
Size: 303375 Color: 16

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 371932 Color: 9
Size: 369937 Color: 1
Size: 258132 Color: 11

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 371983 Color: 16
Size: 326541 Color: 9
Size: 301477 Color: 4

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 372001 Color: 8
Size: 338418 Color: 16
Size: 289582 Color: 3

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 372012 Color: 11
Size: 315709 Color: 14
Size: 312280 Color: 14

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 372022 Color: 12
Size: 363941 Color: 18
Size: 264038 Color: 6

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 372075 Color: 6
Size: 353255 Color: 17
Size: 274671 Color: 5

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 372079 Color: 19
Size: 366588 Color: 2
Size: 261334 Color: 8

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 372092 Color: 8
Size: 316651 Color: 10
Size: 311258 Color: 2

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 372122 Color: 5
Size: 353171 Color: 14
Size: 274708 Color: 19

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 372155 Color: 3
Size: 318617 Color: 12
Size: 309229 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 372156 Color: 12
Size: 330928 Color: 10
Size: 296917 Color: 18

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 372175 Color: 5
Size: 318736 Color: 11
Size: 309090 Color: 18

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 372221 Color: 11
Size: 366356 Color: 2
Size: 261424 Color: 16

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 372227 Color: 5
Size: 314753 Color: 3
Size: 313021 Color: 11

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 372283 Color: 6
Size: 358042 Color: 3
Size: 269676 Color: 1

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 372331 Color: 9
Size: 321859 Color: 3
Size: 305811 Color: 17

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 372399 Color: 2
Size: 329495 Color: 3
Size: 298107 Color: 14

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 372405 Color: 14
Size: 321251 Color: 17
Size: 306345 Color: 10

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 372314 Color: 18
Size: 323183 Color: 1
Size: 304504 Color: 17

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 372415 Color: 13
Size: 359934 Color: 17
Size: 267652 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 372453 Color: 8
Size: 347503 Color: 4
Size: 280045 Color: 12

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 372481 Color: 3
Size: 369372 Color: 2
Size: 258148 Color: 8

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 372481 Color: 7
Size: 340365 Color: 19
Size: 287155 Color: 5

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 372564 Color: 1
Size: 366516 Color: 2
Size: 260921 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 372840 Color: 1
Size: 360877 Color: 7
Size: 266284 Color: 17

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 372549 Color: 5
Size: 326979 Color: 19
Size: 300473 Color: 17

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 372552 Color: 5
Size: 357065 Color: 4
Size: 270384 Color: 3

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 372620 Color: 10
Size: 318144 Color: 11
Size: 309237 Color: 2

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 372663 Color: 6
Size: 356798 Color: 14
Size: 270540 Color: 10

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 372873 Color: 1
Size: 351177 Color: 3
Size: 275951 Color: 9

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 372760 Color: 3
Size: 342866 Color: 4
Size: 284375 Color: 4

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 372772 Color: 11
Size: 319311 Color: 10
Size: 307918 Color: 4

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 372813 Color: 12
Size: 369973 Color: 7
Size: 257215 Color: 13

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 372970 Color: 1
Size: 320665 Color: 3
Size: 306366 Color: 4

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 372913 Color: 0
Size: 316936 Color: 17
Size: 310152 Color: 2

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 372922 Color: 8
Size: 320136 Color: 10
Size: 306943 Color: 5

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 372944 Color: 14
Size: 322154 Color: 9
Size: 304903 Color: 2

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 372944 Color: 6
Size: 353790 Color: 4
Size: 273267 Color: 12

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 372947 Color: 18
Size: 332829 Color: 15
Size: 294225 Color: 12

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 373022 Color: 9
Size: 332305 Color: 18
Size: 294674 Color: 9

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 373185 Color: 7
Size: 349123 Color: 6
Size: 277693 Color: 6

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 373218 Color: 13
Size: 360254 Color: 19
Size: 266529 Color: 5

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 373447 Color: 2
Size: 317757 Color: 17
Size: 308797 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 373494 Color: 7
Size: 318224 Color: 17
Size: 308283 Color: 13

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 373529 Color: 8
Size: 364418 Color: 8
Size: 262054 Color: 3

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 373532 Color: 2
Size: 349997 Color: 4
Size: 276472 Color: 11

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 373537 Color: 16
Size: 330736 Color: 10
Size: 295728 Color: 12

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 373554 Color: 12
Size: 326934 Color: 17
Size: 299513 Color: 4

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 373662 Color: 0
Size: 346575 Color: 9
Size: 279764 Color: 1

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 373752 Color: 10
Size: 331292 Color: 7
Size: 294957 Color: 8

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 373760 Color: 3
Size: 315679 Color: 4
Size: 310562 Color: 11

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 373763 Color: 12
Size: 357935 Color: 18
Size: 268303 Color: 13

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 373821 Color: 0
Size: 328967 Color: 4
Size: 297213 Color: 4

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 374075 Color: 1
Size: 345531 Color: 18
Size: 280395 Color: 2

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 373911 Color: 6
Size: 346070 Color: 12
Size: 280020 Color: 13

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 373930 Color: 18
Size: 371861 Color: 9
Size: 254210 Color: 3

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 373937 Color: 19
Size: 373200 Color: 7
Size: 252864 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 373965 Color: 9
Size: 371357 Color: 2
Size: 254679 Color: 17

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 373969 Color: 6
Size: 344541 Color: 10
Size: 281491 Color: 17

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 374032 Color: 7
Size: 340051 Color: 15
Size: 285918 Color: 9

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 374293 Color: 1
Size: 332385 Color: 19
Size: 293323 Color: 11

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 374116 Color: 17
Size: 337043 Color: 12
Size: 288842 Color: 19

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 374165 Color: 3
Size: 338314 Color: 6
Size: 287522 Color: 4

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 374248 Color: 13
Size: 369386 Color: 18
Size: 256367 Color: 6

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 374294 Color: 15
Size: 332760 Color: 0
Size: 292947 Color: 18

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 374320 Color: 4
Size: 361940 Color: 1
Size: 263741 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 374338 Color: 19
Size: 328438 Color: 13
Size: 297225 Color: 12

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 374368 Color: 9
Size: 355740 Color: 15
Size: 269893 Color: 3

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 374417 Color: 8
Size: 320246 Color: 11
Size: 305338 Color: 17

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 374421 Color: 2
Size: 359422 Color: 19
Size: 266158 Color: 12

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 374552 Color: 1
Size: 342540 Color: 17
Size: 282909 Color: 7

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 374429 Color: 19
Size: 371743 Color: 15
Size: 253829 Color: 11

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 374434 Color: 15
Size: 371167 Color: 14
Size: 254400 Color: 17

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 374456 Color: 13
Size: 322963 Color: 2
Size: 302582 Color: 4

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 374537 Color: 5
Size: 320243 Color: 19
Size: 305221 Color: 2

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 374555 Color: 4
Size: 335928 Color: 6
Size: 289518 Color: 18

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 374582 Color: 4
Size: 323009 Color: 2
Size: 302410 Color: 14

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 374651 Color: 13
Size: 338924 Color: 18
Size: 286426 Color: 14

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 374665 Color: 7
Size: 368393 Color: 19
Size: 256943 Color: 15

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 374718 Color: 15
Size: 368736 Color: 17
Size: 256547 Color: 6

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 374745 Color: 16
Size: 314268 Color: 15
Size: 310988 Color: 15

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 374930 Color: 6
Size: 324131 Color: 10
Size: 300940 Color: 12

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 374932 Color: 18
Size: 317402 Color: 9
Size: 307667 Color: 9

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 374933 Color: 15
Size: 354528 Color: 16
Size: 270540 Color: 16

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 374945 Color: 19
Size: 365158 Color: 16
Size: 259898 Color: 19

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 375052 Color: 2
Size: 328078 Color: 0
Size: 296871 Color: 17

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 375005 Color: 3
Size: 327660 Color: 4
Size: 297336 Color: 13

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 375049 Color: 19
Size: 346033 Color: 11
Size: 278919 Color: 6

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 375080 Color: 11
Size: 374625 Color: 10
Size: 250296 Color: 0

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 375178 Color: 9
Size: 356923 Color: 13
Size: 267900 Color: 18

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 375190 Color: 9
Size: 353967 Color: 14
Size: 270844 Color: 11

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 375235 Color: 12
Size: 330985 Color: 15
Size: 293781 Color: 4

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 375266 Color: 3
Size: 357986 Color: 5
Size: 266749 Color: 9

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 375303 Color: 14
Size: 316575 Color: 1
Size: 308123 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 375304 Color: 15
Size: 337101 Color: 10
Size: 287596 Color: 1

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 375389 Color: 15
Size: 337907 Color: 13
Size: 286705 Color: 10

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 375405 Color: 12
Size: 340939 Color: 18
Size: 283657 Color: 1

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 375447 Color: 4
Size: 316182 Color: 3
Size: 308372 Color: 19

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 375476 Color: 12
Size: 327067 Color: 6
Size: 297458 Color: 13

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 375846 Color: 0
Size: 341856 Color: 7
Size: 282299 Color: 8

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 375928 Color: 14
Size: 327071 Color: 8
Size: 297002 Color: 10

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 375942 Color: 19
Size: 312860 Color: 2
Size: 311199 Color: 19

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 376111 Color: 7
Size: 353590 Color: 0
Size: 270300 Color: 19

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 376150 Color: 14
Size: 333964 Color: 0
Size: 289887 Color: 7

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 376210 Color: 16
Size: 336854 Color: 10
Size: 286937 Color: 17

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 376240 Color: 15
Size: 373130 Color: 18
Size: 250631 Color: 6

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 376271 Color: 11
Size: 349484 Color: 5
Size: 274246 Color: 8

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 376277 Color: 3
Size: 317510 Color: 12
Size: 306214 Color: 1

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 376344 Color: 4
Size: 361143 Color: 2
Size: 262514 Color: 6

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 376365 Color: 15
Size: 352016 Color: 4
Size: 271620 Color: 6

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 376366 Color: 10
Size: 368370 Color: 4
Size: 255265 Color: 11

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 376388 Color: 7
Size: 349339 Color: 0
Size: 274274 Color: 12

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 376414 Color: 6
Size: 346686 Color: 15
Size: 276901 Color: 17

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 376420 Color: 10
Size: 313338 Color: 0
Size: 310243 Color: 18

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 376612 Color: 0
Size: 353612 Color: 7
Size: 269777 Color: 9

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 376506 Color: 10
Size: 348434 Color: 6
Size: 275061 Color: 7

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 376586 Color: 4
Size: 361855 Color: 11
Size: 261560 Color: 6

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 376607 Color: 17
Size: 359806 Color: 13
Size: 263588 Color: 9

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 376620 Color: 4
Size: 342002 Color: 3
Size: 281379 Color: 6

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 376688 Color: 17
Size: 352532 Color: 14
Size: 270781 Color: 12

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 376692 Color: 9
Size: 346461 Color: 17
Size: 276848 Color: 16

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 376712 Color: 19
Size: 350912 Color: 4
Size: 272377 Color: 3

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 376829 Color: 13
Size: 369197 Color: 8
Size: 253975 Color: 5

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 376923 Color: 17
Size: 362277 Color: 7
Size: 260801 Color: 3

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 376934 Color: 13
Size: 344844 Color: 3
Size: 278223 Color: 1

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 377055 Color: 11
Size: 364395 Color: 0
Size: 258551 Color: 9

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 377112 Color: 11
Size: 335718 Color: 16
Size: 287171 Color: 2

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 377131 Color: 10
Size: 358828 Color: 9
Size: 264042 Color: 15

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 377172 Color: 13
Size: 363362 Color: 2
Size: 259467 Color: 14

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 377177 Color: 9
Size: 348383 Color: 10
Size: 274441 Color: 3

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 377178 Color: 18
Size: 351800 Color: 10
Size: 271023 Color: 6

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 377196 Color: 13
Size: 343269 Color: 3
Size: 279536 Color: 11

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 377224 Color: 13
Size: 339263 Color: 1
Size: 283514 Color: 4

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 377242 Color: 8
Size: 343390 Color: 8
Size: 279369 Color: 12

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 377278 Color: 16
Size: 349407 Color: 2
Size: 273316 Color: 1

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 377323 Color: 12
Size: 359039 Color: 9
Size: 263639 Color: 3

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 377327 Color: 18
Size: 316675 Color: 15
Size: 305999 Color: 14

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 377345 Color: 17
Size: 354214 Color: 0
Size: 268442 Color: 9

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 377393 Color: 19
Size: 366702 Color: 0
Size: 255906 Color: 2

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 377424 Color: 19
Size: 356246 Color: 5
Size: 266331 Color: 8

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 377449 Color: 12
Size: 343284 Color: 15
Size: 279268 Color: 2

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 377544 Color: 19
Size: 345004 Color: 6
Size: 277453 Color: 18

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 377578 Color: 11
Size: 320552 Color: 5
Size: 301871 Color: 15

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 377806 Color: 0
Size: 326127 Color: 19
Size: 296068 Color: 12

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 377596 Color: 16
Size: 347617 Color: 15
Size: 274788 Color: 14

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 377679 Color: 7
Size: 339724 Color: 19
Size: 282598 Color: 13

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 377762 Color: 2
Size: 356986 Color: 6
Size: 265253 Color: 5

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 377810 Color: 7
Size: 356426 Color: 2
Size: 265765 Color: 19

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 377859 Color: 10
Size: 349266 Color: 4
Size: 272876 Color: 14

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 377872 Color: 11
Size: 340431 Color: 16
Size: 281698 Color: 17

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 377887 Color: 14
Size: 320785 Color: 3
Size: 301329 Color: 11

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 377892 Color: 10
Size: 355179 Color: 0
Size: 266930 Color: 7

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 377902 Color: 6
Size: 325313 Color: 12
Size: 296786 Color: 6

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 377935 Color: 10
Size: 353928 Color: 5
Size: 268138 Color: 2

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 377957 Color: 6
Size: 311328 Color: 8
Size: 310716 Color: 1

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 378104 Color: 15
Size: 311791 Color: 6
Size: 310106 Color: 7

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 378157 Color: 1
Size: 331875 Color: 6
Size: 289969 Color: 6

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 378258 Color: 15
Size: 311437 Color: 4
Size: 310306 Color: 5

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 378322 Color: 11
Size: 320185 Color: 18
Size: 301494 Color: 10

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 378333 Color: 15
Size: 316830 Color: 10
Size: 304838 Color: 14

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 378482 Color: 8
Size: 362066 Color: 5
Size: 259453 Color: 16

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 378495 Color: 19
Size: 324631 Color: 4
Size: 296875 Color: 8

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 378629 Color: 17
Size: 341308 Color: 3
Size: 280064 Color: 15

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 378683 Color: 4
Size: 369671 Color: 6
Size: 251647 Color: 7

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 378696 Color: 18
Size: 337063 Color: 19
Size: 284242 Color: 6

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 378792 Color: 19
Size: 356949 Color: 1
Size: 264260 Color: 15

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 378803 Color: 1
Size: 359675 Color: 7
Size: 261523 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 378811 Color: 7
Size: 346870 Color: 14
Size: 274320 Color: 16

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 378841 Color: 3
Size: 323118 Color: 18
Size: 298042 Color: 16

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 378879 Color: 14
Size: 363269 Color: 9
Size: 257853 Color: 7

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 378938 Color: 9
Size: 345938 Color: 2
Size: 275125 Color: 18

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 378943 Color: 18
Size: 361538 Color: 19
Size: 259520 Color: 13

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 378987 Color: 0
Size: 363078 Color: 9
Size: 257936 Color: 14

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 378972 Color: 3
Size: 320155 Color: 1
Size: 300874 Color: 13

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 379005 Color: 12
Size: 357832 Color: 6
Size: 263164 Color: 7

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 379014 Color: 4
Size: 330194 Color: 11
Size: 290793 Color: 18

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 379046 Color: 4
Size: 318977 Color: 15
Size: 301978 Color: 5

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 378922 Color: 15
Size: 330288 Color: 19
Size: 290791 Color: 13

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 379089 Color: 6
Size: 342030 Color: 16
Size: 278882 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 379093 Color: 4
Size: 344490 Color: 1
Size: 276418 Color: 6

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 379133 Color: 2
Size: 322283 Color: 1
Size: 298585 Color: 16

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 379140 Color: 16
Size: 368257 Color: 14
Size: 252604 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 379153 Color: 17
Size: 354622 Color: 2
Size: 266226 Color: 8

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 379294 Color: 13
Size: 366702 Color: 12
Size: 254005 Color: 17

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 379304 Color: 14
Size: 322828 Color: 0
Size: 297869 Color: 5

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 379324 Color: 16
Size: 329731 Color: 7
Size: 290946 Color: 10

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 379431 Color: 14
Size: 346393 Color: 17
Size: 274177 Color: 12

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 379450 Color: 1
Size: 321962 Color: 15
Size: 298589 Color: 11

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 379469 Color: 10
Size: 343109 Color: 5
Size: 277423 Color: 6

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 379525 Color: 11
Size: 355015 Color: 3
Size: 265461 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 379552 Color: 19
Size: 364353 Color: 12
Size: 256096 Color: 14

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 379553 Color: 2
Size: 324901 Color: 5
Size: 295547 Color: 19

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 379675 Color: 9
Size: 364230 Color: 6
Size: 256096 Color: 18

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 379683 Color: 12
Size: 346319 Color: 18
Size: 273999 Color: 1

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 379709 Color: 6
Size: 330768 Color: 10
Size: 289524 Color: 18

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 379690 Color: 12
Size: 326317 Color: 1
Size: 293994 Color: 6

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 379728 Color: 12
Size: 324747 Color: 12
Size: 295526 Color: 11

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 379731 Color: 8
Size: 331944 Color: 1
Size: 288326 Color: 13

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 379766 Color: 8
Size: 348265 Color: 1
Size: 271970 Color: 2

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 379979 Color: 13
Size: 332464 Color: 1
Size: 287558 Color: 5

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 380094 Color: 0
Size: 337443 Color: 8
Size: 282464 Color: 6

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 380136 Color: 0
Size: 331649 Color: 14
Size: 288216 Color: 2

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 380003 Color: 19
Size: 321371 Color: 3
Size: 298627 Color: 3

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 380070 Color: 3
Size: 327027 Color: 13
Size: 292904 Color: 10

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 380151 Color: 16
Size: 357502 Color: 1
Size: 262348 Color: 1

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 380191 Color: 16
Size: 350138 Color: 0
Size: 269672 Color: 18

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 380200 Color: 15
Size: 344824 Color: 7
Size: 274977 Color: 18

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 380202 Color: 16
Size: 362687 Color: 4
Size: 257112 Color: 9

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 380262 Color: 5
Size: 317311 Color: 4
Size: 302428 Color: 11

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 380264 Color: 11
Size: 327416 Color: 10
Size: 292321 Color: 10

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 380277 Color: 17
Size: 310343 Color: 7
Size: 309381 Color: 10

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 380341 Color: 0
Size: 366673 Color: 10
Size: 252987 Color: 19

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 380308 Color: 9
Size: 365214 Color: 11
Size: 254479 Color: 10

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 380359 Color: 1
Size: 341120 Color: 10
Size: 278522 Color: 10

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 380399 Color: 9
Size: 331962 Color: 3
Size: 287640 Color: 7

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 380444 Color: 5
Size: 316352 Color: 10
Size: 303205 Color: 16

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 380473 Color: 16
Size: 326259 Color: 0
Size: 293269 Color: 16

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 380495 Color: 11
Size: 322767 Color: 8
Size: 296739 Color: 3

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 380534 Color: 18
Size: 322220 Color: 19
Size: 297247 Color: 16

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 380571 Color: 8
Size: 330724 Color: 5
Size: 288706 Color: 9

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 380655 Color: 13
Size: 352288 Color: 11
Size: 267058 Color: 12

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 380689 Color: 1
Size: 350164 Color: 19
Size: 269148 Color: 3

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 380770 Color: 14
Size: 312540 Color: 6
Size: 306691 Color: 17

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 380841 Color: 9
Size: 354449 Color: 16
Size: 264711 Color: 16

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 380852 Color: 2
Size: 334169 Color: 6
Size: 284980 Color: 9

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 380828 Color: 1
Size: 347562 Color: 19
Size: 271611 Color: 9

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 380946 Color: 18
Size: 315176 Color: 13
Size: 303879 Color: 10

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 380951 Color: 15
Size: 336309 Color: 7
Size: 282741 Color: 2

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 381001 Color: 2
Size: 324839 Color: 7
Size: 294161 Color: 16

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 381025 Color: 10
Size: 345924 Color: 6
Size: 273052 Color: 9

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 381033 Color: 2
Size: 357309 Color: 9
Size: 261659 Color: 16

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 381113 Color: 14
Size: 339232 Color: 12
Size: 279656 Color: 8

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 381142 Color: 0
Size: 359446 Color: 17
Size: 259413 Color: 11

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 381321 Color: 1
Size: 313219 Color: 17
Size: 305461 Color: 8

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 381189 Color: 10
Size: 323491 Color: 16
Size: 295321 Color: 4

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 381191 Color: 19
Size: 319342 Color: 5
Size: 299468 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 381359 Color: 0
Size: 344833 Color: 19
Size: 273809 Color: 18

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 381372 Color: 6
Size: 349195 Color: 0
Size: 269434 Color: 16

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 381462 Color: 17
Size: 342153 Color: 8
Size: 276386 Color: 10

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 381483 Color: 15
Size: 310976 Color: 12
Size: 307542 Color: 2

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 381488 Color: 13
Size: 328431 Color: 3
Size: 290082 Color: 12

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 381506 Color: 3
Size: 336064 Color: 5
Size: 282431 Color: 15

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 381580 Color: 3
Size: 352551 Color: 9
Size: 265870 Color: 10

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 381586 Color: 10
Size: 348667 Color: 1
Size: 269748 Color: 3

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 381638 Color: 4
Size: 334582 Color: 6
Size: 283781 Color: 3

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 381665 Color: 12
Size: 345693 Color: 18
Size: 272643 Color: 8

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 381840 Color: 0
Size: 347572 Color: 14
Size: 270589 Color: 12

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 381721 Color: 16
Size: 323620 Color: 18
Size: 294660 Color: 12

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 381737 Color: 17
Size: 340466 Color: 9
Size: 277798 Color: 18

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 381772 Color: 19
Size: 340573 Color: 6
Size: 277656 Color: 18

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 381801 Color: 13
Size: 310347 Color: 7
Size: 307853 Color: 11

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 381908 Color: 7
Size: 344654 Color: 4
Size: 273439 Color: 9

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 381930 Color: 11
Size: 342968 Color: 12
Size: 275103 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 381930 Color: 5
Size: 347727 Color: 7
Size: 270344 Color: 15

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 381992 Color: 1
Size: 365817 Color: 19
Size: 252192 Color: 14

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 382030 Color: 17
Size: 351954 Color: 15
Size: 266017 Color: 5

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 382030 Color: 10
Size: 333672 Color: 6
Size: 284299 Color: 17

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 382033 Color: 9
Size: 355357 Color: 17
Size: 262611 Color: 4

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 382202 Color: 0
Size: 341291 Color: 10
Size: 276508 Color: 12

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 382163 Color: 4
Size: 360973 Color: 4
Size: 256865 Color: 18

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 382179 Color: 10
Size: 362726 Color: 3
Size: 255096 Color: 12

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 382263 Color: 1
Size: 313900 Color: 8
Size: 303838 Color: 19

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 382267 Color: 14
Size: 356730 Color: 3
Size: 261004 Color: 18

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 382290 Color: 6
Size: 343860 Color: 6
Size: 273851 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 382372 Color: 17
Size: 358640 Color: 19
Size: 258989 Color: 16

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 382450 Color: 6
Size: 355471 Color: 2
Size: 262080 Color: 3

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 382516 Color: 12
Size: 351908 Color: 10
Size: 265577 Color: 7

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 382536 Color: 18
Size: 309845 Color: 9
Size: 307620 Color: 16

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 382593 Color: 0
Size: 315546 Color: 5
Size: 301862 Color: 12

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 382582 Color: 3
Size: 326247 Color: 8
Size: 291172 Color: 14

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 382620 Color: 14
Size: 365357 Color: 14
Size: 252024 Color: 3

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 382624 Color: 4
Size: 327973 Color: 1
Size: 289404 Color: 13

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 382640 Color: 13
Size: 328026 Color: 14
Size: 289335 Color: 4

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 382643 Color: 1
Size: 331528 Color: 15
Size: 285830 Color: 7

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 382677 Color: 15
Size: 355935 Color: 4
Size: 261389 Color: 10

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 382907 Color: 0
Size: 313182 Color: 6
Size: 303912 Color: 4

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 382901 Color: 19
Size: 366484 Color: 17
Size: 250616 Color: 18

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 382906 Color: 17
Size: 352207 Color: 10
Size: 264888 Color: 18

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 382930 Color: 3
Size: 312018 Color: 7
Size: 305053 Color: 13

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 383007 Color: 15
Size: 348715 Color: 4
Size: 268279 Color: 1

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 383091 Color: 12
Size: 316381 Color: 5
Size: 300529 Color: 7

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 383174 Color: 9
Size: 312787 Color: 8
Size: 304040 Color: 1

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 383204 Color: 10
Size: 337719 Color: 7
Size: 279078 Color: 4

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 383238 Color: 12
Size: 358023 Color: 4
Size: 258740 Color: 3

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 383258 Color: 16
Size: 332082 Color: 2
Size: 284661 Color: 13

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 383261 Color: 12
Size: 338078 Color: 19
Size: 278662 Color: 8

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 383275 Color: 7
Size: 346789 Color: 1
Size: 269937 Color: 17

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 383339 Color: 16
Size: 350818 Color: 18
Size: 265844 Color: 6

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 383340 Color: 7
Size: 311593 Color: 4
Size: 305068 Color: 8

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 383367 Color: 10
Size: 342073 Color: 2
Size: 274561 Color: 13

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 383411 Color: 14
Size: 341197 Color: 3
Size: 275393 Color: 18

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 383473 Color: 2
Size: 341056 Color: 13
Size: 275472 Color: 8

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 383534 Color: 13
Size: 331143 Color: 8
Size: 285324 Color: 19

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 383662 Color: 18
Size: 310549 Color: 11
Size: 305790 Color: 4

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 383666 Color: 5
Size: 344932 Color: 6
Size: 271403 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 383750 Color: 6
Size: 342512 Color: 13
Size: 273739 Color: 7

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 383786 Color: 15
Size: 355000 Color: 9
Size: 261215 Color: 16

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 383894 Color: 11
Size: 346304 Color: 14
Size: 269803 Color: 2

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 383948 Color: 4
Size: 308053 Color: 18
Size: 308000 Color: 5

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 384065 Color: 9
Size: 340935 Color: 19
Size: 275001 Color: 3

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 384068 Color: 19
Size: 351481 Color: 16
Size: 264452 Color: 8

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 384197 Color: 7
Size: 364701 Color: 6
Size: 251103 Color: 13

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 384297 Color: 19
Size: 341138 Color: 11
Size: 274566 Color: 12

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 384319 Color: 9
Size: 310928 Color: 8
Size: 304754 Color: 1

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 384354 Color: 17
Size: 350130 Color: 14
Size: 265517 Color: 18

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 384401 Color: 16
Size: 327171 Color: 0
Size: 288429 Color: 5

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 384629 Color: 14
Size: 315393 Color: 15
Size: 299979 Color: 15

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 384649 Color: 16
Size: 332882 Color: 3
Size: 282470 Color: 7

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 384664 Color: 9
Size: 359243 Color: 6
Size: 256094 Color: 7

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 384709 Color: 14
Size: 333993 Color: 4
Size: 281299 Color: 1

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 384731 Color: 9
Size: 309991 Color: 2
Size: 305279 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 353763 Color: 11
Size: 330692 Color: 11
Size: 315546 Color: 6

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 384775 Color: 3
Size: 321894 Color: 14
Size: 293332 Color: 5

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 384776 Color: 5
Size: 356188 Color: 15
Size: 259037 Color: 5

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 384813 Color: 1
Size: 313264 Color: 16
Size: 301924 Color: 18

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 384817 Color: 5
Size: 320424 Color: 17
Size: 294760 Color: 9

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 384877 Color: 8
Size: 352932 Color: 4
Size: 262192 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 384892 Color: 3
Size: 363982 Color: 13
Size: 251127 Color: 7

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 384917 Color: 10
Size: 312516 Color: 14
Size: 302568 Color: 14

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 384928 Color: 8
Size: 354936 Color: 16
Size: 260137 Color: 7

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 384933 Color: 18
Size: 326495 Color: 15
Size: 288573 Color: 5

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 384959 Color: 7
Size: 317123 Color: 5
Size: 297919 Color: 1

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 384983 Color: 3
Size: 343995 Color: 5
Size: 271023 Color: 11

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 385023 Color: 7
Size: 316643 Color: 11
Size: 298335 Color: 13

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 385024 Color: 0
Size: 332259 Color: 12
Size: 282718 Color: 6

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 385112 Color: 3
Size: 341832 Color: 15
Size: 273057 Color: 13

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 385211 Color: 6
Size: 313449 Color: 2
Size: 301341 Color: 9

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 385258 Color: 8
Size: 344115 Color: 15
Size: 270628 Color: 10

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 385554 Color: 4
Size: 322092 Color: 12
Size: 292355 Color: 12

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 385368 Color: 15
Size: 310106 Color: 14
Size: 304527 Color: 5

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 385403 Color: 15
Size: 361543 Color: 16
Size: 253055 Color: 11

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 385565 Color: 15
Size: 360702 Color: 11
Size: 253734 Color: 18

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 385669 Color: 16
Size: 310241 Color: 18
Size: 304091 Color: 9

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 385678 Color: 11
Size: 334183 Color: 6
Size: 280140 Color: 3

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 385715 Color: 2
Size: 353819 Color: 12
Size: 260467 Color: 14

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 385725 Color: 19
Size: 333084 Color: 11
Size: 281192 Color: 7

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 385760 Color: 15
Size: 331181 Color: 4
Size: 283060 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 385763 Color: 6
Size: 323354 Color: 1
Size: 290884 Color: 16

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 385788 Color: 5
Size: 360680 Color: 2
Size: 253533 Color: 8

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 385798 Color: 17
Size: 357715 Color: 6
Size: 256488 Color: 9

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 385819 Color: 11
Size: 343052 Color: 2
Size: 271130 Color: 12

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 385883 Color: 9
Size: 310177 Color: 8
Size: 303941 Color: 9

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 386099 Color: 4
Size: 356931 Color: 11
Size: 256971 Color: 14

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 385986 Color: 9
Size: 311099 Color: 2
Size: 302916 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 386016 Color: 15
Size: 363146 Color: 17
Size: 250839 Color: 1

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 386058 Color: 10
Size: 363485 Color: 5
Size: 250458 Color: 16

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 386067 Color: 5
Size: 349257 Color: 18
Size: 264677 Color: 5

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 386090 Color: 19
Size: 312269 Color: 17
Size: 301642 Color: 4

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 386118 Color: 11
Size: 358826 Color: 9
Size: 255057 Color: 15

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 386305 Color: 2
Size: 336196 Color: 3
Size: 277500 Color: 12

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 386310 Color: 6
Size: 335561 Color: 12
Size: 278130 Color: 10

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 386326 Color: 9
Size: 326469 Color: 3
Size: 287206 Color: 7

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 386333 Color: 15
Size: 311189 Color: 6
Size: 302479 Color: 8

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 386340 Color: 0
Size: 352866 Color: 4
Size: 260795 Color: 17

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 386396 Color: 14
Size: 323002 Color: 13
Size: 290603 Color: 2

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 386397 Color: 18
Size: 307533 Color: 13
Size: 306071 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 386415 Color: 11
Size: 359543 Color: 12
Size: 254043 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 386448 Color: 13
Size: 340301 Color: 9
Size: 273252 Color: 1

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 386462 Color: 11
Size: 340510 Color: 3
Size: 273029 Color: 9

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 386496 Color: 18
Size: 320963 Color: 17
Size: 292542 Color: 6

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 386522 Color: 18
Size: 348467 Color: 5
Size: 265012 Color: 12

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 386550 Color: 2
Size: 351290 Color: 0
Size: 262161 Color: 15

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 386552 Color: 14
Size: 306828 Color: 18
Size: 306621 Color: 2

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 386603 Color: 13
Size: 334198 Color: 8
Size: 279200 Color: 16

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 386613 Color: 16
Size: 360627 Color: 19
Size: 252761 Color: 4

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 386651 Color: 15
Size: 324569 Color: 6
Size: 288781 Color: 12

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 386659 Color: 19
Size: 326434 Color: 18
Size: 286908 Color: 9

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 386722 Color: 7
Size: 324233 Color: 5
Size: 289046 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 386722 Color: 13
Size: 316809 Color: 11
Size: 296470 Color: 2

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 386866 Color: 0
Size: 327854 Color: 0
Size: 285281 Color: 3

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 386759 Color: 19
Size: 341152 Color: 1
Size: 272090 Color: 17

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 386767 Color: 7
Size: 323657 Color: 1
Size: 289577 Color: 17

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 386767 Color: 1
Size: 307995 Color: 14
Size: 305239 Color: 7

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 386858 Color: 11
Size: 309598 Color: 14
Size: 303545 Color: 10

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 386874 Color: 17
Size: 336207 Color: 14
Size: 276920 Color: 10

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 386910 Color: 10
Size: 327903 Color: 1
Size: 285188 Color: 3

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 386978 Color: 1
Size: 332169 Color: 10
Size: 280854 Color: 14

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 387035 Color: 18
Size: 350040 Color: 2
Size: 262926 Color: 3

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 387073 Color: 18
Size: 342673 Color: 17
Size: 270255 Color: 17

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 387075 Color: 16
Size: 323122 Color: 0
Size: 289804 Color: 14

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 387253 Color: 14
Size: 356796 Color: 6
Size: 255952 Color: 6

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 387284 Color: 7
Size: 340025 Color: 14
Size: 272692 Color: 9

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 387298 Color: 18
Size: 334900 Color: 9
Size: 277803 Color: 15

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 387352 Color: 11
Size: 355562 Color: 0
Size: 257087 Color: 18

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 387376 Color: 1
Size: 349196 Color: 18
Size: 263429 Color: 7

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 387433 Color: 13
Size: 318189 Color: 6
Size: 294379 Color: 11

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 387434 Color: 19
Size: 355374 Color: 1
Size: 257193 Color: 12

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 387514 Color: 9
Size: 351049 Color: 6
Size: 261438 Color: 17

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 387438 Color: 4
Size: 352678 Color: 13
Size: 259885 Color: 12

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 387463 Color: 18
Size: 350136 Color: 8
Size: 262402 Color: 14

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 387480 Color: 19
Size: 309069 Color: 2
Size: 303452 Color: 16

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 387543 Color: 8
Size: 320233 Color: 10
Size: 292225 Color: 2

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 387543 Color: 18
Size: 348187 Color: 19
Size: 264271 Color: 4

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 387602 Color: 0
Size: 314332 Color: 19
Size: 298067 Color: 18

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 387625 Color: 2
Size: 330997 Color: 15
Size: 281379 Color: 8

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 387658 Color: 8
Size: 347406 Color: 1
Size: 264937 Color: 12

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 387678 Color: 3
Size: 322407 Color: 8
Size: 289916 Color: 11

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 387745 Color: 5
Size: 307513 Color: 3
Size: 304743 Color: 14

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 387766 Color: 13
Size: 308638 Color: 0
Size: 303597 Color: 3

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 387786 Color: 17
Size: 339328 Color: 15
Size: 272887 Color: 10

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 387786 Color: 2
Size: 314769 Color: 18
Size: 297446 Color: 6

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 387855 Color: 12
Size: 308221 Color: 2
Size: 303925 Color: 3

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 387861 Color: 16
Size: 307970 Color: 2
Size: 304170 Color: 16

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 387884 Color: 19
Size: 306781 Color: 11
Size: 305336 Color: 11

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 387945 Color: 6
Size: 328199 Color: 11
Size: 283857 Color: 18

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 387951 Color: 2
Size: 314891 Color: 15
Size: 297159 Color: 16

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 387973 Color: 4
Size: 341099 Color: 14
Size: 270929 Color: 13

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 387984 Color: 9
Size: 342726 Color: 7
Size: 269291 Color: 15

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 388002 Color: 1
Size: 317932 Color: 0
Size: 294067 Color: 17

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 388004 Color: 6
Size: 308030 Color: 12
Size: 303967 Color: 8

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 388107 Color: 8
Size: 317984 Color: 6
Size: 293910 Color: 11

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 388109 Color: 10
Size: 339335 Color: 15
Size: 272557 Color: 7

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 388173 Color: 4
Size: 326342 Color: 0
Size: 285486 Color: 3

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 388181 Color: 6
Size: 314186 Color: 17
Size: 297634 Color: 8

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 388192 Color: 15
Size: 333685 Color: 14
Size: 278124 Color: 16

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 388266 Color: 19
Size: 310081 Color: 9
Size: 301654 Color: 5

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 388272 Color: 14
Size: 320932 Color: 11
Size: 290797 Color: 1

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 388292 Color: 17
Size: 317481 Color: 2
Size: 294228 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 388346 Color: 12
Size: 308255 Color: 11
Size: 303400 Color: 1

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 388361 Color: 11
Size: 349563 Color: 17
Size: 262077 Color: 3

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 388388 Color: 1
Size: 323146 Color: 9
Size: 288467 Color: 7

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 388444 Color: 17
Size: 308092 Color: 17
Size: 303465 Color: 13

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 388460 Color: 2
Size: 355620 Color: 15
Size: 255921 Color: 2

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 388623 Color: 15
Size: 312393 Color: 14
Size: 298985 Color: 16

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 388716 Color: 15
Size: 308536 Color: 11
Size: 302749 Color: 11

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 388569 Color: 4
Size: 333467 Color: 3
Size: 277965 Color: 13

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 388633 Color: 9
Size: 326078 Color: 16
Size: 285290 Color: 10

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 388656 Color: 17
Size: 347590 Color: 18
Size: 263755 Color: 1

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 388783 Color: 4
Size: 312903 Color: 11
Size: 298315 Color: 6

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 388808 Color: 18
Size: 314479 Color: 6
Size: 296714 Color: 10

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 388839 Color: 1
Size: 347687 Color: 13
Size: 263475 Color: 10

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 388860 Color: 19
Size: 320946 Color: 2
Size: 290195 Color: 16

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 388873 Color: 11
Size: 347266 Color: 8
Size: 263862 Color: 17

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 389083 Color: 15
Size: 326034 Color: 16
Size: 284884 Color: 4

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 388968 Color: 10
Size: 357230 Color: 17
Size: 253803 Color: 8

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 388993 Color: 10
Size: 312506 Color: 2
Size: 298502 Color: 5

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 389002 Color: 18
Size: 310851 Color: 0
Size: 300148 Color: 11

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 389002 Color: 2
Size: 352721 Color: 5
Size: 258278 Color: 18

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 389035 Color: 19
Size: 344147 Color: 12
Size: 266819 Color: 12

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 389035 Color: 6
Size: 315560 Color: 2
Size: 295406 Color: 17

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 389078 Color: 10
Size: 354009 Color: 3
Size: 256914 Color: 2

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 389168 Color: 17
Size: 351784 Color: 6
Size: 259049 Color: 2

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 389206 Color: 3
Size: 331107 Color: 6
Size: 279688 Color: 2

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 389240 Color: 13
Size: 318479 Color: 15
Size: 292282 Color: 1

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 389246 Color: 11
Size: 347668 Color: 14
Size: 263087 Color: 2

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 389260 Color: 18
Size: 336211 Color: 6
Size: 274530 Color: 6

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 389287 Color: 7
Size: 347008 Color: 14
Size: 263706 Color: 1

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 389307 Color: 19
Size: 338698 Color: 5
Size: 271996 Color: 4

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 389324 Color: 5
Size: 305378 Color: 15
Size: 305299 Color: 10

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 389467 Color: 3
Size: 315387 Color: 2
Size: 295147 Color: 17

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 389495 Color: 7
Size: 339239 Color: 3
Size: 271267 Color: 10

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 389537 Color: 2
Size: 356071 Color: 9
Size: 254393 Color: 7

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 389575 Color: 4
Size: 350109 Color: 13
Size: 260317 Color: 11

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 389607 Color: 0
Size: 330355 Color: 6
Size: 280039 Color: 13

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 389611 Color: 1
Size: 321920 Color: 17
Size: 288470 Color: 6

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 389719 Color: 15
Size: 324290 Color: 16
Size: 285992 Color: 12

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 389707 Color: 16
Size: 307530 Color: 4
Size: 302764 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 389729 Color: 13
Size: 342072 Color: 17
Size: 268200 Color: 7

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 389750 Color: 11
Size: 326493 Color: 7
Size: 283758 Color: 9

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 389751 Color: 12
Size: 333249 Color: 8
Size: 277001 Color: 10

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 389820 Color: 12
Size: 341399 Color: 4
Size: 268782 Color: 16

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 389825 Color: 6
Size: 342883 Color: 17
Size: 267293 Color: 19

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 389899 Color: 0
Size: 305085 Color: 19
Size: 305017 Color: 10

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 389924 Color: 6
Size: 344494 Color: 12
Size: 265583 Color: 1

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 389988 Color: 9
Size: 325214 Color: 19
Size: 284799 Color: 8

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 389991 Color: 10
Size: 321850 Color: 1
Size: 288160 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 390165 Color: 15
Size: 334380 Color: 11
Size: 275456 Color: 18

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 390050 Color: 16
Size: 321981 Color: 5
Size: 287970 Color: 8

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 390071 Color: 2
Size: 323116 Color: 3
Size: 286814 Color: 4

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 390143 Color: 19
Size: 337880 Color: 1
Size: 271978 Color: 7

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 390196 Color: 4
Size: 328732 Color: 14
Size: 281073 Color: 5

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 390356 Color: 3
Size: 349956 Color: 1
Size: 259689 Color: 2

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 390399 Color: 19
Size: 351135 Color: 13
Size: 258467 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 390682 Color: 15
Size: 339756 Color: 16
Size: 269563 Color: 7

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 390410 Color: 18
Size: 311118 Color: 7
Size: 298473 Color: 19

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 390457 Color: 7
Size: 315443 Color: 7
Size: 294101 Color: 1

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 390686 Color: 3
Size: 327468 Color: 15
Size: 281847 Color: 17

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 390759 Color: 4
Size: 322281 Color: 11
Size: 286961 Color: 2

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 390857 Color: 9
Size: 343055 Color: 10
Size: 266089 Color: 18

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 390935 Color: 7
Size: 346113 Color: 3
Size: 262953 Color: 2

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 390969 Color: 1
Size: 343384 Color: 5
Size: 265648 Color: 15

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 390972 Color: 19
Size: 311728 Color: 13
Size: 297301 Color: 1

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 391037 Color: 11
Size: 349397 Color: 3
Size: 259567 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 391113 Color: 6
Size: 355758 Color: 4
Size: 253130 Color: 16

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 391185 Color: 17
Size: 337192 Color: 4
Size: 271624 Color: 5

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 391189 Color: 17
Size: 328170 Color: 15
Size: 280642 Color: 5

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 391191 Color: 19
Size: 342540 Color: 5
Size: 266270 Color: 13

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 391212 Color: 12
Size: 331993 Color: 9
Size: 276796 Color: 10

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 391226 Color: 17
Size: 310675 Color: 5
Size: 298100 Color: 19

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 391240 Color: 6
Size: 308375 Color: 10
Size: 300386 Color: 17

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 391250 Color: 12
Size: 352537 Color: 16
Size: 256214 Color: 19

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 391268 Color: 1
Size: 323061 Color: 14
Size: 285672 Color: 15

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 391294 Color: 12
Size: 305235 Color: 3
Size: 303472 Color: 14

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 391408 Color: 9
Size: 331505 Color: 14
Size: 277088 Color: 12

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 391446 Color: 11
Size: 335667 Color: 6
Size: 272888 Color: 18

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 391462 Color: 3
Size: 322676 Color: 16
Size: 285863 Color: 2

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 391580 Color: 10
Size: 319030 Color: 14
Size: 289391 Color: 15

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 391644 Color: 19
Size: 323651 Color: 14
Size: 284706 Color: 11

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 391672 Color: 6
Size: 354298 Color: 0
Size: 254031 Color: 4

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 391701 Color: 4
Size: 312727 Color: 2
Size: 295573 Color: 7

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 391774 Color: 2
Size: 324058 Color: 16
Size: 284169 Color: 17

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 391785 Color: 0
Size: 328141 Color: 19
Size: 280075 Color: 7

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 391862 Color: 17
Size: 349659 Color: 12
Size: 258480 Color: 1

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 391943 Color: 19
Size: 311083 Color: 0
Size: 296975 Color: 7

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 391950 Color: 1
Size: 338336 Color: 16
Size: 269715 Color: 9

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 392108 Color: 19
Size: 313094 Color: 11
Size: 294799 Color: 15

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 392135 Color: 2
Size: 347258 Color: 9
Size: 260608 Color: 3

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 392190 Color: 13
Size: 337099 Color: 2
Size: 270712 Color: 2

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 392195 Color: 19
Size: 323228 Color: 2
Size: 284578 Color: 3

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 392254 Color: 1
Size: 331314 Color: 14
Size: 276433 Color: 2

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 392278 Color: 6
Size: 321898 Color: 15
Size: 285825 Color: 17

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 392287 Color: 8
Size: 319528 Color: 10
Size: 288186 Color: 18

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 392414 Color: 8
Size: 324196 Color: 0
Size: 283391 Color: 19

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 392426 Color: 9
Size: 334836 Color: 2
Size: 272739 Color: 11

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 392468 Color: 13
Size: 346197 Color: 0
Size: 261336 Color: 6

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 392537 Color: 5
Size: 330512 Color: 14
Size: 276952 Color: 1

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 392589 Color: 1
Size: 345461 Color: 3
Size: 261951 Color: 15

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 392559 Color: 12
Size: 355721 Color: 10
Size: 251721 Color: 1

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 392699 Color: 13
Size: 304712 Color: 17
Size: 302590 Color: 5

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 392713 Color: 6
Size: 318213 Color: 15
Size: 289075 Color: 2

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 392748 Color: 19
Size: 329103 Color: 14
Size: 278150 Color: 3

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 392795 Color: 3
Size: 349789 Color: 4
Size: 257417 Color: 15

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 392814 Color: 18
Size: 306473 Color: 19
Size: 300714 Color: 1

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 392815 Color: 13
Size: 310506 Color: 5
Size: 296680 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 392828 Color: 0
Size: 303606 Color: 4
Size: 303567 Color: 3

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 392895 Color: 14
Size: 348571 Color: 7
Size: 258535 Color: 10

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 392979 Color: 4
Size: 349099 Color: 13
Size: 257923 Color: 13

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 392983 Color: 9
Size: 328466 Color: 15
Size: 278552 Color: 2

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 392992 Color: 9
Size: 354385 Color: 1
Size: 252624 Color: 11

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 393040 Color: 14
Size: 334979 Color: 13
Size: 271982 Color: 16

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 393142 Color: 6
Size: 312136 Color: 15
Size: 294723 Color: 18

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 393234 Color: 9
Size: 313597 Color: 6
Size: 293170 Color: 16

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 393238 Color: 8
Size: 336688 Color: 17
Size: 270075 Color: 3

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 393258 Color: 11
Size: 348630 Color: 7
Size: 258113 Color: 3

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 393264 Color: 4
Size: 304259 Color: 2
Size: 302478 Color: 12

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 393276 Color: 16
Size: 348156 Color: 12
Size: 258569 Color: 4

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 393298 Color: 9
Size: 306599 Color: 3
Size: 300104 Color: 1

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 393327 Color: 14
Size: 311745 Color: 17
Size: 294929 Color: 16

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 393371 Color: 15
Size: 348333 Color: 14
Size: 258297 Color: 19

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 393378 Color: 8
Size: 354665 Color: 10
Size: 251958 Color: 6

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 393403 Color: 2
Size: 314270 Color: 5
Size: 292328 Color: 3

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 393435 Color: 17
Size: 341731 Color: 14
Size: 264835 Color: 12

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 393503 Color: 2
Size: 309244 Color: 1
Size: 297254 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 393508 Color: 15
Size: 332730 Color: 10
Size: 273763 Color: 6

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 393579 Color: 8
Size: 352600 Color: 12
Size: 253822 Color: 4

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 393579 Color: 7
Size: 320920 Color: 13
Size: 285502 Color: 10

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 393581 Color: 16
Size: 352957 Color: 7
Size: 253463 Color: 19

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 393587 Color: 5
Size: 305900 Color: 11
Size: 300514 Color: 16

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 393620 Color: 1
Size: 343335 Color: 9
Size: 263046 Color: 13

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 393619 Color: 4
Size: 330473 Color: 2
Size: 275909 Color: 12

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 393663 Color: 11
Size: 346069 Color: 7
Size: 260269 Color: 17

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 393711 Color: 12
Size: 351523 Color: 2
Size: 254767 Color: 13

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 393734 Color: 6
Size: 343483 Color: 15
Size: 262784 Color: 4

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 393771 Color: 3
Size: 315194 Color: 1
Size: 291036 Color: 17

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 393773 Color: 9
Size: 341047 Color: 16
Size: 265181 Color: 18

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 393789 Color: 6
Size: 327034 Color: 10
Size: 279178 Color: 12

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 393862 Color: 14
Size: 331768 Color: 17
Size: 274371 Color: 10

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 393935 Color: 6
Size: 310119 Color: 3
Size: 295947 Color: 9

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 393941 Color: 10
Size: 343412 Color: 6
Size: 262648 Color: 9

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 393946 Color: 9
Size: 316068 Color: 8
Size: 289987 Color: 1

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 393986 Color: 11
Size: 343259 Color: 18
Size: 262756 Color: 16

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 394036 Color: 18
Size: 306487 Color: 5
Size: 299478 Color: 7

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 394040 Color: 19
Size: 303636 Color: 9
Size: 302325 Color: 9

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 394122 Color: 8
Size: 333562 Color: 17
Size: 272317 Color: 2

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 394298 Color: 2
Size: 353934 Color: 9
Size: 251769 Color: 4

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 394391 Color: 19
Size: 322324 Color: 17
Size: 283286 Color: 11

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 394479 Color: 8
Size: 330049 Color: 15
Size: 275473 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 394482 Color: 5
Size: 329731 Color: 0
Size: 275788 Color: 17

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 394584 Color: 15
Size: 324678 Color: 10
Size: 280739 Color: 11

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 394590 Color: 13
Size: 346782 Color: 4
Size: 258629 Color: 17

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 394629 Color: 19
Size: 307317 Color: 16
Size: 298055 Color: 7

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 394714 Color: 19
Size: 312743 Color: 16
Size: 292544 Color: 2

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 394732 Color: 4
Size: 336201 Color: 10
Size: 269068 Color: 9

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 394756 Color: 13
Size: 314709 Color: 6
Size: 290536 Color: 19

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 394758 Color: 19
Size: 333691 Color: 0
Size: 271552 Color: 9

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 394759 Color: 11
Size: 305740 Color: 11
Size: 299502 Color: 13

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 394768 Color: 7
Size: 330104 Color: 15
Size: 275129 Color: 9

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 394810 Color: 0
Size: 347648 Color: 15
Size: 257543 Color: 4

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 394963 Color: 2
Size: 323995 Color: 1
Size: 281043 Color: 18

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 394913 Color: 1
Size: 303890 Color: 14
Size: 301198 Color: 12

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 395005 Color: 18
Size: 331639 Color: 9
Size: 273357 Color: 10

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 395201 Color: 1
Size: 310021 Color: 6
Size: 294779 Color: 14

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 395399 Color: 1
Size: 336316 Color: 16
Size: 268286 Color: 5

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 395327 Color: 3
Size: 333718 Color: 2
Size: 270956 Color: 11

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 395416 Color: 10
Size: 334041 Color: 13
Size: 270544 Color: 9

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 395482 Color: 17
Size: 308416 Color: 9
Size: 296103 Color: 8

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 395634 Color: 17
Size: 313911 Color: 10
Size: 290456 Color: 12

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 395646 Color: 10
Size: 350336 Color: 0
Size: 254019 Color: 7

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 395664 Color: 17
Size: 323581 Color: 0
Size: 280756 Color: 7

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 395686 Color: 8
Size: 354177 Color: 15
Size: 250138 Color: 16

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 395688 Color: 14
Size: 327266 Color: 9
Size: 277047 Color: 4

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 395726 Color: 13
Size: 326793 Color: 8
Size: 277482 Color: 1

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 395732 Color: 7
Size: 331967 Color: 12
Size: 272302 Color: 18

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 395763 Color: 17
Size: 343877 Color: 0
Size: 260361 Color: 5

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 395881 Color: 10
Size: 352644 Color: 0
Size: 251476 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 395965 Color: 16
Size: 310460 Color: 15
Size: 293576 Color: 4

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 395992 Color: 0
Size: 311117 Color: 19
Size: 292892 Color: 15

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 396168 Color: 7
Size: 311701 Color: 2
Size: 292132 Color: 9

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 396178 Color: 11
Size: 337043 Color: 8
Size: 266780 Color: 19

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 396206 Color: 16
Size: 314020 Color: 8
Size: 289775 Color: 14

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 396246 Color: 10
Size: 308034 Color: 3
Size: 295721 Color: 14

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 396196 Color: 15
Size: 350110 Color: 16
Size: 253695 Color: 14

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 396288 Color: 12
Size: 308599 Color: 9
Size: 295114 Color: 5

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 396349 Color: 19
Size: 328701 Color: 17
Size: 274951 Color: 7

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 396407 Color: 6
Size: 305934 Color: 10
Size: 297660 Color: 7

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 396418 Color: 0
Size: 349829 Color: 19
Size: 253754 Color: 6

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 396504 Color: 12
Size: 344163 Color: 1
Size: 259334 Color: 7

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 396534 Color: 9
Size: 311820 Color: 0
Size: 291647 Color: 11

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 396534 Color: 17
Size: 321245 Color: 13
Size: 282222 Color: 6

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 396566 Color: 5
Size: 330279 Color: 17
Size: 273156 Color: 18

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 396621 Color: 5
Size: 348486 Color: 11
Size: 254894 Color: 7

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 396535 Color: 9
Size: 349213 Color: 3
Size: 254253 Color: 5

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 396725 Color: 7
Size: 310685 Color: 1
Size: 292591 Color: 12

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 396772 Color: 8
Size: 307808 Color: 14
Size: 295421 Color: 13

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 396798 Color: 0
Size: 336901 Color: 10
Size: 266302 Color: 2

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 396810 Color: 13
Size: 349606 Color: 15
Size: 253585 Color: 4

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 396817 Color: 8
Size: 334257 Color: 7
Size: 268927 Color: 18

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 396868 Color: 0
Size: 348912 Color: 17
Size: 254221 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 396855 Color: 9
Size: 312604 Color: 17
Size: 290542 Color: 2

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 396884 Color: 5
Size: 317227 Color: 1
Size: 285890 Color: 5

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 396903 Color: 19
Size: 309756 Color: 12
Size: 293342 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 396904 Color: 19
Size: 350935 Color: 6
Size: 252162 Color: 14

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 396978 Color: 7
Size: 308342 Color: 14
Size: 294681 Color: 12

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 397057 Color: 19
Size: 321920 Color: 18
Size: 281024 Color: 8

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 397229 Color: 3
Size: 308175 Color: 6
Size: 294597 Color: 19

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 397272 Color: 16
Size: 304926 Color: 15
Size: 297803 Color: 1

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 397311 Color: 19
Size: 301571 Color: 18
Size: 301119 Color: 4

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 397325 Color: 0
Size: 338494 Color: 8
Size: 264182 Color: 15

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 397356 Color: 4
Size: 351463 Color: 19
Size: 251182 Color: 8

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 397424 Color: 13
Size: 337825 Color: 8
Size: 264752 Color: 18

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 397430 Color: 7
Size: 305361 Color: 19
Size: 297210 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 397512 Color: 10
Size: 331774 Color: 18
Size: 270715 Color: 8

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 397548 Color: 3
Size: 320115 Color: 18
Size: 282338 Color: 6

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 397568 Color: 14
Size: 347643 Color: 16
Size: 254790 Color: 14

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 397589 Color: 15
Size: 335907 Color: 12
Size: 266505 Color: 13

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 397612 Color: 15
Size: 315338 Color: 14
Size: 287051 Color: 9

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 397721 Color: 14
Size: 315370 Color: 12
Size: 286910 Color: 16

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 397763 Color: 5
Size: 330376 Color: 8
Size: 271862 Color: 9

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 397777 Color: 11
Size: 304394 Color: 16
Size: 297830 Color: 6

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 397787 Color: 11
Size: 316639 Color: 9
Size: 285575 Color: 12

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 397863 Color: 17
Size: 334841 Color: 7
Size: 267297 Color: 11

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 397952 Color: 15
Size: 306288 Color: 11
Size: 295761 Color: 14

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 397953 Color: 16
Size: 307476 Color: 9
Size: 294572 Color: 10

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 397992 Color: 8
Size: 332467 Color: 12
Size: 269542 Color: 7

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 398044 Color: 14
Size: 315842 Color: 5
Size: 286115 Color: 5

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 398113 Color: 6
Size: 335198 Color: 9
Size: 266690 Color: 11

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 398144 Color: 19
Size: 303488 Color: 15
Size: 298369 Color: 13

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 398160 Color: 17
Size: 311562 Color: 3
Size: 290279 Color: 16

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 398224 Color: 3
Size: 313622 Color: 5
Size: 288155 Color: 13

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 398244 Color: 7
Size: 343438 Color: 19
Size: 258319 Color: 15

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 398256 Color: 0
Size: 343280 Color: 2
Size: 258465 Color: 16

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 398316 Color: 6
Size: 335481 Color: 5
Size: 266204 Color: 17

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 398335 Color: 18
Size: 339105 Color: 5
Size: 262561 Color: 4

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 398387 Color: 14
Size: 334959 Color: 11
Size: 266655 Color: 8

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 398431 Color: 10
Size: 304115 Color: 4
Size: 297455 Color: 12

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 398450 Color: 3
Size: 312011 Color: 15
Size: 289540 Color: 19

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 398560 Color: 1
Size: 321467 Color: 6
Size: 279974 Color: 6

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 398501 Color: 6
Size: 305153 Color: 11
Size: 296347 Color: 8

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 398522 Color: 17
Size: 338165 Color: 18
Size: 263314 Color: 8

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 398525 Color: 9
Size: 344790 Color: 16
Size: 256686 Color: 6

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 398580 Color: 0
Size: 328241 Color: 19
Size: 273180 Color: 7

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 398613 Color: 10
Size: 341396 Color: 1
Size: 259992 Color: 18

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 398666 Color: 11
Size: 317040 Color: 12
Size: 284295 Color: 4

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 398680 Color: 6
Size: 337206 Color: 11
Size: 264115 Color: 12

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 398726 Color: 14
Size: 320203 Color: 5
Size: 281072 Color: 15

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 398776 Color: 4
Size: 335420 Color: 8
Size: 265805 Color: 17

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 398913 Color: 7
Size: 322704 Color: 5
Size: 278384 Color: 15

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 398984 Color: 14
Size: 316815 Color: 9
Size: 284202 Color: 1

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 399016 Color: 2
Size: 337005 Color: 13
Size: 263980 Color: 5

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 399075 Color: 9
Size: 343161 Color: 4
Size: 257765 Color: 18

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 399075 Color: 8
Size: 314213 Color: 0
Size: 286713 Color: 13

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 399089 Color: 5
Size: 315152 Color: 19
Size: 285760 Color: 4

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 399147 Color: 5
Size: 320528 Color: 5
Size: 280326 Color: 4

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 399261 Color: 0
Size: 336572 Color: 12
Size: 264168 Color: 14

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 399304 Color: 9
Size: 346523 Color: 3
Size: 254174 Color: 7

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 399343 Color: 18
Size: 348233 Color: 9
Size: 252425 Color: 13

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 399404 Color: 12
Size: 335244 Color: 13
Size: 265353 Color: 7

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 399428 Color: 0
Size: 321576 Color: 17
Size: 278997 Color: 6

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 399432 Color: 3
Size: 304367 Color: 8
Size: 296202 Color: 8

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 399363 Color: 15
Size: 312466 Color: 18
Size: 288172 Color: 2

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 399452 Color: 19
Size: 332562 Color: 9
Size: 267987 Color: 3

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 399553 Color: 7
Size: 305830 Color: 8
Size: 294618 Color: 11

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 399600 Color: 12
Size: 309442 Color: 18
Size: 290959 Color: 7

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 399607 Color: 19
Size: 320106 Color: 18
Size: 280288 Color: 10

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 399621 Color: 10
Size: 302923 Color: 5
Size: 297457 Color: 3

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 399659 Color: 12
Size: 300224 Color: 4
Size: 300118 Color: 14

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 399761 Color: 1
Size: 300849 Color: 17
Size: 299391 Color: 10

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 399684 Color: 4
Size: 310642 Color: 6
Size: 289675 Color: 4

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 399904 Color: 5
Size: 330183 Color: 9
Size: 269914 Color: 7

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 399915 Color: 15
Size: 334425 Color: 5
Size: 265661 Color: 1

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 399918 Color: 14
Size: 301889 Color: 15
Size: 298194 Color: 14

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 399987 Color: 14
Size: 308246 Color: 17
Size: 291768 Color: 16

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 400007 Color: 17
Size: 321565 Color: 8
Size: 278429 Color: 19

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 400101 Color: 11
Size: 345848 Color: 9
Size: 254052 Color: 18

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 400141 Color: 16
Size: 343938 Color: 13
Size: 255922 Color: 17

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 400207 Color: 5
Size: 327506 Color: 6
Size: 272288 Color: 14

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 400208 Color: 13
Size: 335785 Color: 19
Size: 264008 Color: 5

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 400238 Color: 6
Size: 322029 Color: 12
Size: 277734 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 400244 Color: 11
Size: 319598 Color: 3
Size: 280159 Color: 13

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 400258 Color: 18
Size: 334096 Color: 19
Size: 265647 Color: 9

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 400276 Color: 4
Size: 344996 Color: 7
Size: 254729 Color: 5

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 400298 Color: 12
Size: 338927 Color: 1
Size: 260776 Color: 5

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 400433 Color: 18
Size: 319891 Color: 16
Size: 279677 Color: 14

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 400454 Color: 14
Size: 301524 Color: 6
Size: 298023 Color: 16

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 400471 Color: 3
Size: 322881 Color: 19
Size: 276649 Color: 13

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 400475 Color: 0
Size: 321696 Color: 9
Size: 277830 Color: 11

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 400500 Color: 14
Size: 325705 Color: 4
Size: 273796 Color: 12

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 400504 Color: 16
Size: 323087 Color: 14
Size: 276410 Color: 15

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 400539 Color: 14
Size: 323879 Color: 18
Size: 275583 Color: 13

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 400721 Color: 4
Size: 322712 Color: 8
Size: 276568 Color: 13

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 400736 Color: 0
Size: 348435 Color: 7
Size: 250830 Color: 5

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 400758 Color: 19
Size: 307296 Color: 9
Size: 291947 Color: 13

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 400830 Color: 18
Size: 331423 Color: 7
Size: 267748 Color: 15

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 400883 Color: 13
Size: 305922 Color: 19
Size: 293196 Color: 1

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 400935 Color: 18
Size: 342289 Color: 12
Size: 256777 Color: 10

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 400964 Color: 18
Size: 310589 Color: 19
Size: 288448 Color: 7

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 401001 Color: 2
Size: 317694 Color: 0
Size: 281306 Color: 11

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 401014 Color: 17
Size: 315992 Color: 19
Size: 282995 Color: 16

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 401042 Color: 17
Size: 330858 Color: 9
Size: 268101 Color: 19

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 401047 Color: 13
Size: 302984 Color: 5
Size: 295970 Color: 19

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 401049 Color: 11
Size: 328485 Color: 11
Size: 270467 Color: 18

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 401191 Color: 17
Size: 325070 Color: 17
Size: 273740 Color: 5

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 401251 Color: 12
Size: 306511 Color: 18
Size: 292239 Color: 7

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 401273 Color: 3
Size: 339370 Color: 15
Size: 259358 Color: 9

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 401287 Color: 11
Size: 309917 Color: 1
Size: 288797 Color: 7

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 401390 Color: 0
Size: 311124 Color: 19
Size: 287487 Color: 10

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 401423 Color: 4
Size: 339100 Color: 14
Size: 259478 Color: 11

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 401451 Color: 0
Size: 337870 Color: 11
Size: 260680 Color: 1

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 401483 Color: 13
Size: 330074 Color: 1
Size: 268444 Color: 9

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 401485 Color: 6
Size: 335634 Color: 8
Size: 262882 Color: 3

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 401527 Color: 3
Size: 327986 Color: 8
Size: 270488 Color: 17

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 401542 Color: 12
Size: 323156 Color: 12
Size: 275303 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 401593 Color: 11
Size: 299764 Color: 14
Size: 298644 Color: 15

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 401615 Color: 15
Size: 334635 Color: 4
Size: 263751 Color: 8

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 401691 Color: 14
Size: 321272 Color: 9
Size: 277038 Color: 15

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 401729 Color: 10
Size: 301995 Color: 4
Size: 296277 Color: 17

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 401806 Color: 18
Size: 330284 Color: 7
Size: 267911 Color: 11

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 401845 Color: 3
Size: 305775 Color: 16
Size: 292381 Color: 16

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 401867 Color: 8
Size: 304000 Color: 1
Size: 294134 Color: 6

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 401897 Color: 19
Size: 316896 Color: 14
Size: 281208 Color: 2

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 401916 Color: 9
Size: 312681 Color: 5
Size: 285404 Color: 13

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 401943 Color: 17
Size: 332248 Color: 15
Size: 265810 Color: 4

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 401950 Color: 3
Size: 337139 Color: 18
Size: 260912 Color: 7

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 401951 Color: 2
Size: 304637 Color: 12
Size: 293413 Color: 12

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 402011 Color: 2
Size: 309578 Color: 12
Size: 288412 Color: 14

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 402025 Color: 18
Size: 343410 Color: 19
Size: 254566 Color: 16

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 402063 Color: 4
Size: 303453 Color: 5
Size: 294485 Color: 7

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 402077 Color: 3
Size: 304960 Color: 2
Size: 292964 Color: 12

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 402093 Color: 1
Size: 319023 Color: 8
Size: 278885 Color: 19

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 402204 Color: 19
Size: 307288 Color: 14
Size: 290509 Color: 16

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 402362 Color: 9
Size: 299414 Color: 11
Size: 298225 Color: 17

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 402302 Color: 15
Size: 305827 Color: 6
Size: 291872 Color: 3

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 402375 Color: 1
Size: 334997 Color: 15
Size: 262629 Color: 6

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 402384 Color: 7
Size: 343123 Color: 16
Size: 254494 Color: 5

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 402390 Color: 14
Size: 308771 Color: 14
Size: 288840 Color: 19

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 402438 Color: 2
Size: 327955 Color: 15
Size: 269608 Color: 5

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 402499 Color: 1
Size: 328239 Color: 9
Size: 269263 Color: 11

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 402592 Color: 12
Size: 330088 Color: 13
Size: 267321 Color: 13

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 402592 Color: 6
Size: 308295 Color: 14
Size: 289114 Color: 18

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 402627 Color: 19
Size: 327677 Color: 8
Size: 269697 Color: 17

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 402660 Color: 13
Size: 315101 Color: 3
Size: 282240 Color: 18

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 402667 Color: 0
Size: 323219 Color: 17
Size: 274115 Color: 11

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 402682 Color: 10
Size: 311688 Color: 10
Size: 285631 Color: 6

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 402754 Color: 8
Size: 307546 Color: 0
Size: 289701 Color: 14

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 402812 Color: 14
Size: 329977 Color: 5
Size: 267212 Color: 10

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 402828 Color: 6
Size: 337727 Color: 15
Size: 259446 Color: 7

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 402829 Color: 6
Size: 306569 Color: 10
Size: 290603 Color: 8

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 402845 Color: 17
Size: 314420 Color: 16
Size: 282736 Color: 7

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 402847 Color: 17
Size: 340886 Color: 9
Size: 256268 Color: 17

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 402861 Color: 10
Size: 332391 Color: 8
Size: 264749 Color: 10

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 402866 Color: 19
Size: 334711 Color: 12
Size: 262424 Color: 6

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 402925 Color: 7
Size: 327423 Color: 8
Size: 269653 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 402931 Color: 12
Size: 329956 Color: 14
Size: 267114 Color: 18

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 402945 Color: 10
Size: 316390 Color: 18
Size: 280666 Color: 14

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 403005 Color: 8
Size: 333575 Color: 9
Size: 263421 Color: 14

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 403068 Color: 0
Size: 340337 Color: 8
Size: 256596 Color: 15

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 403077 Color: 14
Size: 342775 Color: 7
Size: 254149 Color: 2

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 403086 Color: 18
Size: 329587 Color: 8
Size: 267328 Color: 17

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 403095 Color: 5
Size: 318196 Color: 3
Size: 278710 Color: 12

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 403113 Color: 7
Size: 306609 Color: 6
Size: 290279 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 403132 Color: 4
Size: 317620 Color: 8
Size: 279249 Color: 9

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 403246 Color: 18
Size: 310297 Color: 16
Size: 286458 Color: 5

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 403312 Color: 2
Size: 339675 Color: 12
Size: 257014 Color: 16

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 403433 Color: 6
Size: 322503 Color: 2
Size: 274065 Color: 7

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 403459 Color: 11
Size: 331620 Color: 13
Size: 264922 Color: 15

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 403479 Color: 18
Size: 311389 Color: 15
Size: 285133 Color: 5

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 403501 Color: 11
Size: 325761 Color: 9
Size: 270739 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 403519 Color: 0
Size: 303856 Color: 18
Size: 292626 Color: 2

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 403597 Color: 6
Size: 345785 Color: 14
Size: 250619 Color: 13

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 403615 Color: 6
Size: 322885 Color: 17
Size: 273501 Color: 1

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 403621 Color: 0
Size: 345679 Color: 7
Size: 250701 Color: 8

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 403660 Color: 13
Size: 307813 Color: 11
Size: 288528 Color: 8

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 403755 Color: 15
Size: 330924 Color: 15
Size: 265322 Color: 12

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 403852 Color: 4
Size: 317321 Color: 16
Size: 278828 Color: 19

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 403892 Color: 11
Size: 339620 Color: 3
Size: 256489 Color: 1

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 403932 Color: 17
Size: 314545 Color: 14
Size: 281524 Color: 2

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 403952 Color: 2
Size: 306142 Color: 12
Size: 289907 Color: 1

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 403968 Color: 11
Size: 316170 Color: 12
Size: 279863 Color: 17

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 404009 Color: 13
Size: 328012 Color: 3
Size: 267980 Color: 19

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 404049 Color: 1
Size: 309872 Color: 0
Size: 286080 Color: 14

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 404230 Color: 15
Size: 343625 Color: 5
Size: 252146 Color: 18

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 404191 Color: 5
Size: 303194 Color: 6
Size: 292616 Color: 6

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 404206 Color: 18
Size: 316497 Color: 9
Size: 279298 Color: 5

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 404216 Color: 13
Size: 343334 Color: 18
Size: 252451 Color: 2

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 404350 Color: 10
Size: 322514 Color: 1
Size: 273137 Color: 5

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 404352 Color: 11
Size: 311950 Color: 5
Size: 283699 Color: 18

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 404390 Color: 16
Size: 337483 Color: 14
Size: 258128 Color: 15

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 404541 Color: 4
Size: 332133 Color: 7
Size: 263327 Color: 11

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 404546 Color: 10
Size: 317827 Color: 17
Size: 277628 Color: 6

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 404550 Color: 6
Size: 300103 Color: 12
Size: 295348 Color: 8

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 404557 Color: 17
Size: 345411 Color: 4
Size: 250033 Color: 15

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 404563 Color: 7
Size: 320649 Color: 8
Size: 274789 Color: 7

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 404601 Color: 12
Size: 345013 Color: 5
Size: 250387 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 404627 Color: 2
Size: 329261 Color: 6
Size: 266113 Color: 13

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 404713 Color: 10
Size: 312548 Color: 1
Size: 282740 Color: 8

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 404717 Color: 4
Size: 308279 Color: 13
Size: 287005 Color: 7

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 404740 Color: 19
Size: 344731 Color: 18
Size: 250530 Color: 15

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 404743 Color: 13
Size: 320757 Color: 10
Size: 274501 Color: 7

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 404750 Color: 19
Size: 297969 Color: 6
Size: 297282 Color: 12

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 404763 Color: 14
Size: 336168 Color: 11
Size: 259070 Color: 1

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 404768 Color: 16
Size: 302791 Color: 6
Size: 292442 Color: 5

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 404806 Color: 1
Size: 297606 Color: 6
Size: 297589 Color: 18

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 404998 Color: 6
Size: 314394 Color: 16
Size: 280609 Color: 10

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 405020 Color: 10
Size: 341068 Color: 0
Size: 253913 Color: 15

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 405027 Color: 6
Size: 338487 Color: 4
Size: 256487 Color: 3

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 405115 Color: 10
Size: 342029 Color: 3
Size: 252857 Color: 7

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 405138 Color: 7
Size: 312231 Color: 3
Size: 282632 Color: 7

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 405210 Color: 1
Size: 337117 Color: 0
Size: 257674 Color: 16

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 405292 Color: 6
Size: 312924 Color: 15
Size: 281785 Color: 3

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 405273 Color: 12
Size: 341529 Color: 2
Size: 253199 Color: 8

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 405287 Color: 18
Size: 303322 Color: 1
Size: 291392 Color: 12

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 405289 Color: 7
Size: 301323 Color: 0
Size: 293389 Color: 18

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 405290 Color: 14
Size: 329332 Color: 9
Size: 265379 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 405391 Color: 17
Size: 335130 Color: 5
Size: 259480 Color: 9

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 405608 Color: 8
Size: 337771 Color: 6
Size: 256622 Color: 12

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 405628 Color: 3
Size: 302968 Color: 4
Size: 291405 Color: 1

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 405803 Color: 4
Size: 321887 Color: 14
Size: 272311 Color: 17

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 405842 Color: 11
Size: 337133 Color: 5
Size: 257026 Color: 14

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 405855 Color: 13
Size: 322555 Color: 15
Size: 271591 Color: 14

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 405867 Color: 10
Size: 301579 Color: 6
Size: 292555 Color: 12

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 405914 Color: 10
Size: 332616 Color: 9
Size: 261471 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 405929 Color: 17
Size: 339767 Color: 5
Size: 254305 Color: 3

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 406045 Color: 9
Size: 310342 Color: 10
Size: 283614 Color: 8

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 406098 Color: 5
Size: 336685 Color: 18
Size: 257218 Color: 15

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 406104 Color: 4
Size: 333808 Color: 3
Size: 260089 Color: 12

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 406203 Color: 6
Size: 324807 Color: 4
Size: 268991 Color: 10

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 406215 Color: 16
Size: 315791 Color: 7
Size: 277995 Color: 7

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 406242 Color: 18
Size: 301631 Color: 11
Size: 292128 Color: 4

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 406251 Color: 12
Size: 325239 Color: 7
Size: 268511 Color: 1

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 406338 Color: 15
Size: 324515 Color: 0
Size: 269148 Color: 3

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 406396 Color: 19
Size: 307610 Color: 12
Size: 285995 Color: 9

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 406441 Color: 8
Size: 338989 Color: 0
Size: 254571 Color: 7

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 406499 Color: 13
Size: 316678 Color: 3
Size: 276824 Color: 13

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 406584 Color: 2
Size: 323106 Color: 6
Size: 270311 Color: 10

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 406598 Color: 1
Size: 310162 Color: 9
Size: 283241 Color: 7

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 406680 Color: 16
Size: 319582 Color: 8
Size: 273739 Color: 8

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 406761 Color: 5
Size: 325927 Color: 11
Size: 267313 Color: 18

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 406754 Color: 6
Size: 322379 Color: 10
Size: 270868 Color: 9

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 406786 Color: 7
Size: 308476 Color: 15
Size: 284739 Color: 7

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 406789 Color: 19
Size: 299914 Color: 15
Size: 293298 Color: 14

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 406819 Color: 9
Size: 301332 Color: 14
Size: 291850 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 406822 Color: 14
Size: 321737 Color: 10
Size: 271442 Color: 16

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 406930 Color: 17
Size: 314283 Color: 7
Size: 278788 Color: 5

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 406963 Color: 17
Size: 300415 Color: 0
Size: 292623 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 406969 Color: 13
Size: 298336 Color: 7
Size: 294696 Color: 8

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 406978 Color: 17
Size: 328502 Color: 8
Size: 264521 Color: 12

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 407193 Color: 6
Size: 304960 Color: 2
Size: 287848 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 407010 Color: 0
Size: 296542 Color: 12
Size: 296449 Color: 15

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 407020 Color: 13
Size: 319609 Color: 19
Size: 273372 Color: 2

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 407188 Color: 0
Size: 301179 Color: 3
Size: 291634 Color: 1

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 407216 Color: 13
Size: 311285 Color: 19
Size: 281500 Color: 19

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 407229 Color: 19
Size: 322686 Color: 7
Size: 270086 Color: 18

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 407252 Color: 15
Size: 342145 Color: 15
Size: 250604 Color: 9

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 407266 Color: 12
Size: 305778 Color: 2
Size: 286957 Color: 11

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 407292 Color: 4
Size: 318238 Color: 13
Size: 274471 Color: 15

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 407343 Color: 19
Size: 328853 Color: 1
Size: 263805 Color: 6

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 407379 Color: 12
Size: 320893 Color: 0
Size: 271729 Color: 3

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 407381 Color: 5
Size: 312641 Color: 3
Size: 279979 Color: 18

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 407483 Color: 5
Size: 340532 Color: 3
Size: 251986 Color: 10

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 407486 Color: 10
Size: 336855 Color: 6
Size: 255660 Color: 13

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 407493 Color: 11
Size: 308740 Color: 19
Size: 283768 Color: 19

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 407508 Color: 16
Size: 329848 Color: 19
Size: 262645 Color: 4

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 407558 Color: 1
Size: 328987 Color: 18
Size: 263456 Color: 4

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 407626 Color: 17
Size: 309955 Color: 11
Size: 282420 Color: 11

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 407726 Color: 8
Size: 304827 Color: 15
Size: 287448 Color: 19

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 407736 Color: 5
Size: 296603 Color: 17
Size: 295662 Color: 1

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 407749 Color: 5
Size: 336124 Color: 18
Size: 256128 Color: 2

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 407817 Color: 11
Size: 314480 Color: 14
Size: 277704 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 408037 Color: 4
Size: 309082 Color: 6
Size: 282882 Color: 1

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 408112 Color: 17
Size: 327600 Color: 1
Size: 264289 Color: 4

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 408196 Color: 11
Size: 339879 Color: 19
Size: 251926 Color: 12

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 408235 Color: 2
Size: 305106 Color: 11
Size: 286660 Color: 14

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 408257 Color: 17
Size: 327185 Color: 14
Size: 264559 Color: 4

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 408319 Color: 16
Size: 332187 Color: 17
Size: 259495 Color: 13

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 408322 Color: 10
Size: 321731 Color: 18
Size: 269948 Color: 1

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 408393 Color: 13
Size: 308911 Color: 3
Size: 282697 Color: 9

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 408396 Color: 14
Size: 317155 Color: 11
Size: 274450 Color: 18

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 408459 Color: 9
Size: 329054 Color: 9
Size: 262488 Color: 7

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 408464 Color: 6
Size: 298882 Color: 17
Size: 292655 Color: 15

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 408512 Color: 4
Size: 324208 Color: 2
Size: 267281 Color: 9

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 408515 Color: 12
Size: 336795 Color: 17
Size: 254691 Color: 1

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 408555 Color: 0
Size: 331216 Color: 16
Size: 260230 Color: 6

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 408558 Color: 8
Size: 310209 Color: 8
Size: 281234 Color: 7

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 408665 Color: 12
Size: 315218 Color: 13
Size: 276118 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 408687 Color: 2
Size: 316338 Color: 9
Size: 274976 Color: 8

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 408713 Color: 14
Size: 336931 Color: 19
Size: 254357 Color: 4

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 408723 Color: 13
Size: 315532 Color: 4
Size: 275746 Color: 13

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 408774 Color: 1
Size: 323389 Color: 15
Size: 267838 Color: 18

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 408804 Color: 9
Size: 321289 Color: 7
Size: 269908 Color: 12

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 408829 Color: 11
Size: 322671 Color: 8
Size: 268501 Color: 11

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 409042 Color: 6
Size: 319464 Color: 16
Size: 271495 Color: 13

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 408900 Color: 15
Size: 314936 Color: 11
Size: 276165 Color: 19

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 408935 Color: 12
Size: 317148 Color: 10
Size: 273918 Color: 17

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 409072 Color: 3
Size: 297043 Color: 15
Size: 293886 Color: 16

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 409177 Color: 18
Size: 313800 Color: 11
Size: 277024 Color: 2

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 409183 Color: 19
Size: 337697 Color: 12
Size: 253121 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 409190 Color: 19
Size: 297078 Color: 0
Size: 293733 Color: 4

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 409237 Color: 7
Size: 299850 Color: 7
Size: 290914 Color: 10

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 409508 Color: 10
Size: 301481 Color: 9
Size: 289012 Color: 3

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 409532 Color: 5
Size: 327437 Color: 7
Size: 263032 Color: 12

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 409538 Color: 8
Size: 339550 Color: 0
Size: 250913 Color: 11

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 409613 Color: 18
Size: 315578 Color: 15
Size: 274810 Color: 7

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 409624 Color: 7
Size: 313706 Color: 5
Size: 276671 Color: 10

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 409678 Color: 1
Size: 298713 Color: 19
Size: 291610 Color: 6

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 409684 Color: 5
Size: 297463 Color: 10
Size: 292854 Color: 7

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 409734 Color: 3
Size: 326024 Color: 4
Size: 264243 Color: 7

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 409759 Color: 5
Size: 324444 Color: 15
Size: 265798 Color: 3

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 409797 Color: 18
Size: 326389 Color: 18
Size: 263815 Color: 14

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 409836 Color: 13
Size: 308044 Color: 0
Size: 282121 Color: 7

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 409966 Color: 6
Size: 322798 Color: 14
Size: 267237 Color: 13

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 409927 Color: 3
Size: 328310 Color: 19
Size: 261764 Color: 7

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 409939 Color: 3
Size: 329320 Color: 14
Size: 260742 Color: 3

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 409968 Color: 0
Size: 327311 Color: 16
Size: 262722 Color: 7

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 409978 Color: 14
Size: 297682 Color: 3
Size: 292341 Color: 4

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 410067 Color: 4
Size: 316390 Color: 16
Size: 273544 Color: 11

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 410109 Color: 5
Size: 336625 Color: 6
Size: 253267 Color: 2

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 410140 Color: 12
Size: 331362 Color: 11
Size: 258499 Color: 2

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 410146 Color: 5
Size: 324233 Color: 19
Size: 265622 Color: 19

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 410161 Color: 19
Size: 323925 Color: 12
Size: 265915 Color: 15

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 410163 Color: 8
Size: 317078 Color: 3
Size: 272760 Color: 8

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 410273 Color: 3
Size: 324467 Color: 1
Size: 265261 Color: 8

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 410298 Color: 8
Size: 319417 Color: 6
Size: 270286 Color: 7

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 410346 Color: 12
Size: 324520 Color: 12
Size: 265135 Color: 7

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 410378 Color: 8
Size: 338366 Color: 16
Size: 251257 Color: 8

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 410590 Color: 2
Size: 335167 Color: 7
Size: 254244 Color: 5

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 410640 Color: 11
Size: 315512 Color: 15
Size: 273849 Color: 1

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 410688 Color: 10
Size: 326099 Color: 10
Size: 263214 Color: 6

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 410691 Color: 15
Size: 330131 Color: 3
Size: 259179 Color: 16

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 410770 Color: 15
Size: 311998 Color: 9
Size: 277233 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 410773 Color: 17
Size: 314833 Color: 11
Size: 274395 Color: 12

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 410797 Color: 13
Size: 295807 Color: 3
Size: 293397 Color: 6

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 410845 Color: 3
Size: 337703 Color: 2
Size: 251453 Color: 3

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 410853 Color: 4
Size: 321251 Color: 14
Size: 267897 Color: 19

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 410875 Color: 17
Size: 305395 Color: 11
Size: 283731 Color: 10

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 410897 Color: 3
Size: 317611 Color: 2
Size: 271493 Color: 2

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 410910 Color: 10
Size: 326790 Color: 17
Size: 262301 Color: 8

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 410954 Color: 19
Size: 313195 Color: 13
Size: 275852 Color: 12

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 411003 Color: 8
Size: 300638 Color: 11
Size: 288360 Color: 12

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 411042 Color: 10
Size: 332178 Color: 2
Size: 256781 Color: 18

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 411090 Color: 5
Size: 317015 Color: 2
Size: 271896 Color: 1

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 411121 Color: 8
Size: 321870 Color: 8
Size: 267010 Color: 6

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 411133 Color: 4
Size: 296825 Color: 10
Size: 292043 Color: 2

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 411162 Color: 2
Size: 308816 Color: 16
Size: 280023 Color: 10

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 411194 Color: 0
Size: 332166 Color: 2
Size: 256641 Color: 18

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 411198 Color: 5
Size: 319077 Color: 13
Size: 269726 Color: 11

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 411232 Color: 1
Size: 306265 Color: 4
Size: 282504 Color: 12

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 411350 Color: 3
Size: 302137 Color: 14
Size: 286514 Color: 14

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 411355 Color: 19
Size: 336776 Color: 11
Size: 251870 Color: 6

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 411367 Color: 5
Size: 331856 Color: 7
Size: 256778 Color: 7

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 411375 Color: 7
Size: 317578 Color: 17
Size: 271048 Color: 8

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 411401 Color: 4
Size: 329869 Color: 2
Size: 258731 Color: 2

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 411409 Color: 15
Size: 329527 Color: 5
Size: 259065 Color: 5

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 411423 Color: 3
Size: 334146 Color: 3
Size: 254432 Color: 7

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 411534 Color: 11
Size: 323586 Color: 0
Size: 264881 Color: 6

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 411554 Color: 18
Size: 325061 Color: 5
Size: 263386 Color: 11

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 411559 Color: 18
Size: 326709 Color: 0
Size: 261733 Color: 2

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 411576 Color: 2
Size: 325376 Color: 8
Size: 263049 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 411693 Color: 9
Size: 321812 Color: 15
Size: 266496 Color: 1

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 411783 Color: 15
Size: 328763 Color: 13
Size: 259455 Color: 19

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 411832 Color: 12
Size: 320314 Color: 13
Size: 267855 Color: 15

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 411958 Color: 6
Size: 328481 Color: 14
Size: 259562 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 411868 Color: 12
Size: 301299 Color: 16
Size: 286834 Color: 18

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 411893 Color: 7
Size: 329103 Color: 12
Size: 259005 Color: 15

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 411997 Color: 1
Size: 322016 Color: 18
Size: 265988 Color: 8

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 412070 Color: 7
Size: 328954 Color: 13
Size: 258977 Color: 6

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 412285 Color: 7
Size: 331335 Color: 4
Size: 256381 Color: 8

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 412413 Color: 7
Size: 302665 Color: 11
Size: 284923 Color: 11

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 412489 Color: 12
Size: 297426 Color: 9
Size: 290086 Color: 18

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 412543 Color: 10
Size: 299492 Color: 1
Size: 287966 Color: 16

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 412550 Color: 10
Size: 294134 Color: 15
Size: 293317 Color: 18

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 412554 Color: 2
Size: 316617 Color: 6
Size: 270830 Color: 3

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 412603 Color: 3
Size: 316171 Color: 14
Size: 271227 Color: 10

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 412759 Color: 16
Size: 309398 Color: 7
Size: 277844 Color: 12

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 412734 Color: 15
Size: 335426 Color: 2
Size: 251841 Color: 13

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 412777 Color: 14
Size: 303903 Color: 6
Size: 283321 Color: 2

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 412846 Color: 7
Size: 326640 Color: 19
Size: 260515 Color: 14

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 412857 Color: 10
Size: 320871 Color: 5
Size: 266273 Color: 7

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 412892 Color: 3
Size: 316559 Color: 14
Size: 270550 Color: 19

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 413140 Color: 7
Size: 300486 Color: 6
Size: 286375 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 413152 Color: 14
Size: 301733 Color: 16
Size: 285116 Color: 13

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 413195 Color: 19
Size: 303716 Color: 0
Size: 283090 Color: 10

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 413255 Color: 3
Size: 324038 Color: 7
Size: 262708 Color: 8

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 413350 Color: 0
Size: 321186 Color: 7
Size: 265465 Color: 19

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 413372 Color: 18
Size: 322507 Color: 5
Size: 264122 Color: 1

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 413499 Color: 11
Size: 321945 Color: 18
Size: 264557 Color: 6

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 413516 Color: 10
Size: 332788 Color: 15
Size: 253697 Color: 15

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 413563 Color: 14
Size: 336057 Color: 8
Size: 250381 Color: 7

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 413583 Color: 1
Size: 305831 Color: 6
Size: 280587 Color: 9

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 413583 Color: 0
Size: 314291 Color: 18
Size: 272127 Color: 2

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 413616 Color: 4
Size: 332269 Color: 12
Size: 254116 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 413628 Color: 4
Size: 300678 Color: 6
Size: 285695 Color: 2

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 413668 Color: 4
Size: 327700 Color: 12
Size: 258633 Color: 13

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 413680 Color: 12
Size: 295552 Color: 7
Size: 290769 Color: 14

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 413730 Color: 0
Size: 298943 Color: 8
Size: 287328 Color: 13

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 413771 Color: 15
Size: 316209 Color: 3
Size: 270021 Color: 5

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 413814 Color: 8
Size: 332033 Color: 11
Size: 254154 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 413846 Color: 8
Size: 299674 Color: 2
Size: 286481 Color: 6

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 413902 Color: 19
Size: 314401 Color: 10
Size: 271698 Color: 2

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 413927 Color: 2
Size: 323556 Color: 15
Size: 262518 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 414047 Color: 12
Size: 305170 Color: 5
Size: 280784 Color: 18

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 414086 Color: 5
Size: 295007 Color: 11
Size: 290908 Color: 13

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 414125 Color: 19
Size: 316160 Color: 4
Size: 269716 Color: 6

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 414148 Color: 14
Size: 302852 Color: 13
Size: 283001 Color: 10

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 414206 Color: 5
Size: 307658 Color: 18
Size: 278137 Color: 13

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 414213 Color: 10
Size: 319114 Color: 3
Size: 266674 Color: 12

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 414256 Color: 13
Size: 293007 Color: 9
Size: 292738 Color: 1

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 414340 Color: 14
Size: 304493 Color: 0
Size: 281168 Color: 6

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 414356 Color: 13
Size: 302750 Color: 2
Size: 282895 Color: 11

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 414369 Color: 15
Size: 294046 Color: 10
Size: 291586 Color: 18

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 414465 Color: 4
Size: 328894 Color: 6
Size: 256642 Color: 10

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 414492 Color: 7
Size: 318130 Color: 5
Size: 267379 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 414493 Color: 15
Size: 325239 Color: 7
Size: 260269 Color: 3

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 414536 Color: 14
Size: 295309 Color: 3
Size: 290156 Color: 6

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 414576 Color: 3
Size: 310389 Color: 9
Size: 275036 Color: 1

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 414586 Color: 11
Size: 312182 Color: 8
Size: 273233 Color: 5

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 414586 Color: 3
Size: 331562 Color: 7
Size: 253853 Color: 9

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 414625 Color: 10
Size: 300875 Color: 19
Size: 284501 Color: 1

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 414682 Color: 11
Size: 303292 Color: 15
Size: 282027 Color: 18

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 414699 Color: 11
Size: 330587 Color: 0
Size: 254715 Color: 6

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 414713 Color: 11
Size: 304570 Color: 4
Size: 280718 Color: 8

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 414750 Color: 1
Size: 323259 Color: 0
Size: 261992 Color: 2

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 414893 Color: 1
Size: 329466 Color: 12
Size: 255642 Color: 10

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 414982 Color: 11
Size: 331028 Color: 16
Size: 253991 Color: 7

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 414985 Color: 13
Size: 331506 Color: 5
Size: 253510 Color: 15

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 415048 Color: 11
Size: 332924 Color: 0
Size: 252029 Color: 6

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 415121 Color: 19
Size: 318306 Color: 13
Size: 266574 Color: 17

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 415176 Color: 7
Size: 301752 Color: 5
Size: 283073 Color: 10

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 415218 Color: 13
Size: 312171 Color: 18
Size: 272612 Color: 9

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 415240 Color: 14
Size: 310517 Color: 3
Size: 274244 Color: 16

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 415256 Color: 7
Size: 332483 Color: 9
Size: 252262 Color: 3

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 415260 Color: 3
Size: 293721 Color: 13
Size: 291020 Color: 6

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 415282 Color: 7
Size: 298010 Color: 1
Size: 286709 Color: 12

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 415407 Color: 15
Size: 330068 Color: 0
Size: 254526 Color: 10

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 415501 Color: 13
Size: 307064 Color: 8
Size: 277436 Color: 17

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 415548 Color: 8
Size: 305124 Color: 10
Size: 279329 Color: 12

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 415697 Color: 7
Size: 309807 Color: 12
Size: 274497 Color: 9

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 415808 Color: 15
Size: 304888 Color: 18
Size: 279305 Color: 9

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 415799 Color: 0
Size: 328415 Color: 17
Size: 255787 Color: 18

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 415902 Color: 5
Size: 333982 Color: 4
Size: 250117 Color: 17

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 415915 Color: 12
Size: 330546 Color: 5
Size: 253540 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 415983 Color: 1
Size: 331167 Color: 15
Size: 252851 Color: 5

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 416088 Color: 10
Size: 305289 Color: 14
Size: 278624 Color: 7

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 416170 Color: 0
Size: 312908 Color: 16
Size: 270923 Color: 19

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 416201 Color: 13
Size: 327851 Color: 18
Size: 255949 Color: 12

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 416268 Color: 7
Size: 293286 Color: 3
Size: 290447 Color: 14

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 416335 Color: 18
Size: 331438 Color: 15
Size: 252228 Color: 3

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 416340 Color: 4
Size: 315009 Color: 18
Size: 268652 Color: 16

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 416344 Color: 2
Size: 304796 Color: 6
Size: 278861 Color: 19

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 416346 Color: 9
Size: 298878 Color: 12
Size: 284777 Color: 14

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 416361 Color: 19
Size: 292934 Color: 0
Size: 290706 Color: 16

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 416368 Color: 1
Size: 308919 Color: 10
Size: 274714 Color: 16

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 416371 Color: 7
Size: 324747 Color: 4
Size: 258883 Color: 9

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 416375 Color: 9
Size: 310626 Color: 8
Size: 273000 Color: 15

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 416378 Color: 19
Size: 313957 Color: 3
Size: 269666 Color: 9

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 416422 Color: 17
Size: 296217 Color: 2
Size: 287362 Color: 9

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 416458 Color: 0
Size: 293332 Color: 1
Size: 290211 Color: 7

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 416489 Color: 16
Size: 313958 Color: 5
Size: 269554 Color: 16

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 416494 Color: 2
Size: 313584 Color: 15
Size: 269923 Color: 11

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 416512 Color: 10
Size: 315017 Color: 6
Size: 268472 Color: 5

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 416630 Color: 16
Size: 332268 Color: 16
Size: 251103 Color: 14

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 416644 Color: 18
Size: 312232 Color: 3
Size: 271125 Color: 13

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 416648 Color: 7
Size: 297937 Color: 19
Size: 285416 Color: 11

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 416678 Color: 8
Size: 299523 Color: 5
Size: 283800 Color: 16

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 416700 Color: 17
Size: 306980 Color: 1
Size: 276321 Color: 7

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 416772 Color: 11
Size: 322428 Color: 0
Size: 260801 Color: 9

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 416855 Color: 3
Size: 295345 Color: 2
Size: 287801 Color: 8

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 416911 Color: 4
Size: 300509 Color: 18
Size: 282581 Color: 1

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 416989 Color: 5
Size: 331206 Color: 17
Size: 251806 Color: 13

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 416997 Color: 17
Size: 304007 Color: 15
Size: 278997 Color: 8

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 417038 Color: 6
Size: 320786 Color: 6
Size: 262177 Color: 10

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 417050 Color: 19
Size: 308803 Color: 2
Size: 274148 Color: 18

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 417087 Color: 18
Size: 323304 Color: 1
Size: 259610 Color: 1

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 417169 Color: 10
Size: 312648 Color: 17
Size: 270184 Color: 15

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 417173 Color: 2
Size: 326221 Color: 8
Size: 256607 Color: 12

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 417299 Color: 0
Size: 308207 Color: 19
Size: 274495 Color: 7

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 417388 Color: 16
Size: 326648 Color: 19
Size: 255965 Color: 13

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 417389 Color: 8
Size: 313189 Color: 14
Size: 269423 Color: 11

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 417454 Color: 0
Size: 295866 Color: 8
Size: 286681 Color: 17

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 417543 Color: 1
Size: 301827 Color: 19
Size: 280631 Color: 15

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 417564 Color: 13
Size: 294437 Color: 0
Size: 288000 Color: 1

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 417630 Color: 5
Size: 307933 Color: 5
Size: 274438 Color: 19

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 417669 Color: 19
Size: 330348 Color: 12
Size: 251984 Color: 17

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 417812 Color: 4
Size: 312063 Color: 2
Size: 270126 Color: 17

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 417832 Color: 9
Size: 291179 Color: 12
Size: 290990 Color: 19

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 417843 Color: 11
Size: 313712 Color: 15
Size: 268446 Color: 6

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 418015 Color: 9
Size: 312619 Color: 11
Size: 269367 Color: 2

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 418041 Color: 19
Size: 329894 Color: 7
Size: 252066 Color: 19

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 418070 Color: 1
Size: 305845 Color: 3
Size: 276086 Color: 6

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 418187 Color: 12
Size: 322036 Color: 2
Size: 259778 Color: 7

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 418219 Color: 9
Size: 327734 Color: 10
Size: 254048 Color: 4

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 418308 Color: 0
Size: 310868 Color: 12
Size: 270825 Color: 7

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 418340 Color: 12
Size: 298840 Color: 5
Size: 282821 Color: 1

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 418403 Color: 9
Size: 304700 Color: 19
Size: 276898 Color: 15

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 418442 Color: 11
Size: 326322 Color: 3
Size: 255237 Color: 7

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 418488 Color: 10
Size: 323718 Color: 1
Size: 257795 Color: 11

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 418539 Color: 3
Size: 297340 Color: 6
Size: 284122 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 418554 Color: 4
Size: 327852 Color: 1
Size: 253595 Color: 9

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 418601 Color: 4
Size: 312848 Color: 10
Size: 268552 Color: 15

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 418648 Color: 6
Size: 311588 Color: 18
Size: 269765 Color: 1

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 418737 Color: 10
Size: 318062 Color: 19
Size: 263202 Color: 12

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 418806 Color: 16
Size: 303206 Color: 3
Size: 277989 Color: 1

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 418816 Color: 6
Size: 316352 Color: 7
Size: 264833 Color: 18

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 418819 Color: 7
Size: 312694 Color: 12
Size: 268488 Color: 5

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 418824 Color: 8
Size: 306615 Color: 7
Size: 274562 Color: 15

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 418913 Color: 1
Size: 290849 Color: 10
Size: 290239 Color: 6

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 418928 Color: 3
Size: 300411 Color: 13
Size: 280662 Color: 16

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 418933 Color: 0
Size: 325001 Color: 13
Size: 256067 Color: 8

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 418957 Color: 11
Size: 306076 Color: 0
Size: 274968 Color: 12

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 419121 Color: 15
Size: 297673 Color: 11
Size: 283207 Color: 17

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 419092 Color: 16
Size: 312759 Color: 11
Size: 268150 Color: 19

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 419107 Color: 7
Size: 328635 Color: 8
Size: 252259 Color: 6

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 419135 Color: 17
Size: 302179 Color: 7
Size: 278687 Color: 11

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 419155 Color: 12
Size: 298535 Color: 10
Size: 282311 Color: 16

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 419229 Color: 11
Size: 298186 Color: 16
Size: 282586 Color: 15

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 419284 Color: 5
Size: 315510 Color: 9
Size: 265207 Color: 2

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 419336 Color: 11
Size: 308117 Color: 19
Size: 272548 Color: 14

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 419390 Color: 3
Size: 322873 Color: 19
Size: 257738 Color: 18

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 419391 Color: 8
Size: 330055 Color: 9
Size: 250555 Color: 2

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 419415 Color: 12
Size: 322164 Color: 7
Size: 258422 Color: 10

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 419451 Color: 13
Size: 301620 Color: 19
Size: 278930 Color: 12

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 419473 Color: 13
Size: 310368 Color: 16
Size: 270160 Color: 5

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 419552 Color: 12
Size: 294305 Color: 8
Size: 286144 Color: 19

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 419634 Color: 0
Size: 294928 Color: 0
Size: 285439 Color: 13

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 419639 Color: 7
Size: 330083 Color: 15
Size: 250279 Color: 5

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 419711 Color: 7
Size: 301340 Color: 17
Size: 278950 Color: 12

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 419744 Color: 7
Size: 293301 Color: 6
Size: 286956 Color: 19

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 419766 Color: 11
Size: 305002 Color: 3
Size: 275233 Color: 17

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 419789 Color: 2
Size: 293261 Color: 18
Size: 286951 Color: 2

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 419834 Color: 4
Size: 311225 Color: 12
Size: 268942 Color: 5

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 419856 Color: 1
Size: 292723 Color: 16
Size: 287422 Color: 5

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 419857 Color: 16
Size: 294056 Color: 11
Size: 286088 Color: 9

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 419881 Color: 4
Size: 317236 Color: 11
Size: 262884 Color: 14

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 419893 Color: 17
Size: 326600 Color: 11
Size: 253508 Color: 11

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 419914 Color: 6
Size: 312387 Color: 7
Size: 267700 Color: 15

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 419950 Color: 13
Size: 301019 Color: 14
Size: 279032 Color: 17

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 420066 Color: 0
Size: 306471 Color: 6
Size: 273464 Color: 8

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 420146 Color: 10
Size: 319246 Color: 1
Size: 260609 Color: 9

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 420233 Color: 12
Size: 294488 Color: 17
Size: 285280 Color: 17

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 420281 Color: 14
Size: 325179 Color: 16
Size: 254541 Color: 7

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 420289 Color: 9
Size: 312400 Color: 16
Size: 267312 Color: 2

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 420298 Color: 6
Size: 293497 Color: 4
Size: 286206 Color: 5

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 420397 Color: 1
Size: 322634 Color: 4
Size: 256970 Color: 16

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 420406 Color: 18
Size: 320658 Color: 10
Size: 258937 Color: 19

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 420418 Color: 7
Size: 319007 Color: 16
Size: 260576 Color: 11

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 420432 Color: 15
Size: 300429 Color: 18
Size: 279140 Color: 8

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 420487 Color: 13
Size: 320039 Color: 6
Size: 259475 Color: 13

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 420497 Color: 0
Size: 305064 Color: 9
Size: 274440 Color: 14

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 420520 Color: 10
Size: 318503 Color: 11
Size: 260978 Color: 12

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 420521 Color: 11
Size: 328552 Color: 9
Size: 250928 Color: 6

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 420536 Color: 16
Size: 305693 Color: 3
Size: 273772 Color: 2

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 420542 Color: 13
Size: 313134 Color: 12
Size: 266325 Color: 7

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 420561 Color: 12
Size: 292879 Color: 15
Size: 286561 Color: 9

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 420631 Color: 13
Size: 295559 Color: 8
Size: 283811 Color: 16

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 420672 Color: 8
Size: 300604 Color: 10
Size: 278725 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 420751 Color: 19
Size: 325742 Color: 16
Size: 253508 Color: 13

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 420812 Color: 17
Size: 310812 Color: 14
Size: 268377 Color: 15

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 420814 Color: 19
Size: 293525 Color: 7
Size: 285662 Color: 6

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 420829 Color: 8
Size: 313292 Color: 18
Size: 265880 Color: 9

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 420853 Color: 17
Size: 306442 Color: 16
Size: 272706 Color: 19

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 420877 Color: 17
Size: 322748 Color: 0
Size: 256376 Color: 16

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 420974 Color: 1
Size: 314476 Color: 19
Size: 264551 Color: 11

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 421044 Color: 18
Size: 325879 Color: 7
Size: 253078 Color: 3

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 421045 Color: 14
Size: 302720 Color: 1
Size: 276236 Color: 13

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 421082 Color: 2
Size: 313033 Color: 19
Size: 265886 Color: 4

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 421087 Color: 17
Size: 302965 Color: 16
Size: 275949 Color: 12

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 421112 Color: 17
Size: 305922 Color: 15
Size: 272967 Color: 4

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 421165 Color: 12
Size: 304552 Color: 1
Size: 274284 Color: 5

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 421193 Color: 7
Size: 307357 Color: 17
Size: 271451 Color: 19

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 421167 Color: 18
Size: 292821 Color: 9
Size: 286013 Color: 5

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 421195 Color: 12
Size: 307044 Color: 18
Size: 271762 Color: 16

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 421294 Color: 2
Size: 313920 Color: 4
Size: 264787 Color: 9

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 421351 Color: 13
Size: 293278 Color: 10
Size: 285372 Color: 3

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 421400 Color: 14
Size: 298290 Color: 11
Size: 280311 Color: 3

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 421460 Color: 18
Size: 321444 Color: 7
Size: 257097 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 421573 Color: 17
Size: 324339 Color: 17
Size: 254089 Color: 2

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 421623 Color: 4
Size: 303859 Color: 10
Size: 274519 Color: 9

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 421657 Color: 18
Size: 324076 Color: 12
Size: 254268 Color: 5

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 421684 Color: 18
Size: 319033 Color: 3
Size: 259284 Color: 3

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 421688 Color: 18
Size: 296665 Color: 8
Size: 281648 Color: 8

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 421777 Color: 16
Size: 324450 Color: 7
Size: 253774 Color: 18

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 421860 Color: 9
Size: 323218 Color: 2
Size: 254923 Color: 11

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 421905 Color: 6
Size: 318222 Color: 4
Size: 259874 Color: 9

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 421994 Color: 4
Size: 316149 Color: 6
Size: 261858 Color: 1

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 421998 Color: 17
Size: 323036 Color: 9
Size: 254967 Color: 8

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 422115 Color: 11
Size: 303201 Color: 11
Size: 274685 Color: 16

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 422148 Color: 14
Size: 324025 Color: 7
Size: 253828 Color: 12

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 422277 Color: 10
Size: 321514 Color: 11
Size: 256210 Color: 2

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 422294 Color: 12
Size: 323283 Color: 14
Size: 254424 Color: 14

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 422300 Color: 0
Size: 313353 Color: 17
Size: 264348 Color: 8

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 422372 Color: 3
Size: 290341 Color: 2
Size: 287288 Color: 16

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 422379 Color: 11
Size: 299054 Color: 18
Size: 278568 Color: 9

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 422392 Color: 19
Size: 322597 Color: 6
Size: 255012 Color: 17

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 422442 Color: 12
Size: 293165 Color: 12
Size: 284394 Color: 4

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 422706 Color: 7
Size: 295180 Color: 14
Size: 282115 Color: 2

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 422600 Color: 3
Size: 292674 Color: 12
Size: 284727 Color: 8

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 422661 Color: 19
Size: 290259 Color: 0
Size: 287081 Color: 16

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 422673 Color: 0
Size: 310383 Color: 10
Size: 266945 Color: 10

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 422717 Color: 1
Size: 310780 Color: 3
Size: 266504 Color: 1

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 422797 Color: 8
Size: 324079 Color: 10
Size: 253125 Color: 19

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 423067 Color: 7
Size: 294649 Color: 14
Size: 282285 Color: 8

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 422870 Color: 18
Size: 312225 Color: 4
Size: 264906 Color: 10

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 422944 Color: 5
Size: 309837 Color: 9
Size: 267220 Color: 10

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 422981 Color: 16
Size: 317868 Color: 5
Size: 259152 Color: 15

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 423038 Color: 0
Size: 293144 Color: 4
Size: 283819 Color: 11

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 423071 Color: 14
Size: 320917 Color: 13
Size: 256013 Color: 19

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 423394 Color: 7
Size: 313656 Color: 1
Size: 262951 Color: 2

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 423207 Color: 13
Size: 313431 Color: 16
Size: 263363 Color: 13

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 423248 Color: 13
Size: 288972 Color: 6
Size: 287781 Color: 5

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 423308 Color: 8
Size: 312468 Color: 0
Size: 264225 Color: 19

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 423434 Color: 11
Size: 292915 Color: 5
Size: 283652 Color: 13

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 423446 Color: 18
Size: 322622 Color: 11
Size: 253933 Color: 6

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 423722 Color: 7
Size: 325383 Color: 0
Size: 250896 Color: 17

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 423516 Color: 14
Size: 321502 Color: 19
Size: 254983 Color: 1

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 423600 Color: 9
Size: 305750 Color: 18
Size: 270651 Color: 19

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 423778 Color: 7
Size: 325137 Color: 2
Size: 251086 Color: 4

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 423610 Color: 2
Size: 312865 Color: 17
Size: 263526 Color: 12

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 423683 Color: 14
Size: 312462 Color: 17
Size: 263856 Color: 11

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 423699 Color: 5
Size: 322489 Color: 12
Size: 253813 Color: 14

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 423900 Color: 7
Size: 325981 Color: 10
Size: 250120 Color: 1

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 423811 Color: 0
Size: 314126 Color: 19
Size: 262064 Color: 4

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 423819 Color: 5
Size: 324576 Color: 12
Size: 251606 Color: 9

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 423911 Color: 3
Size: 325503 Color: 19
Size: 250587 Color: 6

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 423930 Color: 15
Size: 298106 Color: 5
Size: 277965 Color: 7

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 423945 Color: 18
Size: 292706 Color: 13
Size: 283350 Color: 4

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 423997 Color: 5
Size: 324821 Color: 2
Size: 251183 Color: 14

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 424025 Color: 17
Size: 314165 Color: 4
Size: 261811 Color: 10

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 424088 Color: 17
Size: 303718 Color: 8
Size: 272195 Color: 4

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 424109 Color: 16
Size: 294935 Color: 15
Size: 280957 Color: 10

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 424140 Color: 14
Size: 303994 Color: 7
Size: 271867 Color: 15

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 424359 Color: 4
Size: 318653 Color: 18
Size: 256989 Color: 17

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 424379 Color: 3
Size: 306254 Color: 12
Size: 269368 Color: 14

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 424394 Color: 8
Size: 314506 Color: 17
Size: 261101 Color: 19

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 424407 Color: 12
Size: 294026 Color: 11
Size: 281568 Color: 14

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 424420 Color: 0
Size: 314466 Color: 7
Size: 261115 Color: 17

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 424481 Color: 13
Size: 321703 Color: 16
Size: 253817 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 424649 Color: 10
Size: 314091 Color: 5
Size: 261261 Color: 15

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 424661 Color: 8
Size: 303317 Color: 5
Size: 272023 Color: 16

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 424662 Color: 4
Size: 321344 Color: 19
Size: 253995 Color: 15

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 424708 Color: 10
Size: 320199 Color: 4
Size: 255094 Color: 2

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 424745 Color: 1
Size: 307534 Color: 19
Size: 267722 Color: 13

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 424837 Color: 7
Size: 291530 Color: 11
Size: 283634 Color: 3

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 424788 Color: 3
Size: 304945 Color: 8
Size: 270268 Color: 10

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 425014 Color: 9
Size: 295413 Color: 4
Size: 279574 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 425026 Color: 16
Size: 318906 Color: 11
Size: 256069 Color: 18

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 425065 Color: 7
Size: 301952 Color: 16
Size: 272984 Color: 19

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 425042 Color: 0
Size: 318368 Color: 16
Size: 256591 Color: 4

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 425078 Color: 8
Size: 320593 Color: 16
Size: 254330 Color: 13

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 425097 Color: 14
Size: 301941 Color: 12
Size: 272963 Color: 5

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 425170 Color: 0
Size: 289392 Color: 14
Size: 285439 Color: 5

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 425266 Color: 7
Size: 323660 Color: 11
Size: 251075 Color: 17

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 425248 Color: 15
Size: 287466 Color: 16
Size: 287287 Color: 18

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 425379 Color: 1
Size: 288186 Color: 13
Size: 286436 Color: 4

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 425482 Color: 14
Size: 308127 Color: 3
Size: 266392 Color: 10

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 425493 Color: 14
Size: 317844 Color: 15
Size: 256664 Color: 11

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 425512 Color: 12
Size: 301140 Color: 2
Size: 273349 Color: 5

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 425513 Color: 1
Size: 287596 Color: 14
Size: 286892 Color: 11

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 425561 Color: 8
Size: 309756 Color: 18
Size: 264684 Color: 7

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 425593 Color: 17
Size: 298765 Color: 18
Size: 275643 Color: 13

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 425615 Color: 16
Size: 316850 Color: 12
Size: 257536 Color: 17

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 425695 Color: 19
Size: 321006 Color: 13
Size: 253300 Color: 8

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 425697 Color: 11
Size: 294812 Color: 9
Size: 279492 Color: 9

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 425753 Color: 14
Size: 304949 Color: 15
Size: 269299 Color: 3

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 425809 Color: 3
Size: 300286 Color: 15
Size: 273906 Color: 7

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 425887 Color: 10
Size: 298575 Color: 8
Size: 275539 Color: 18

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 425963 Color: 11
Size: 319794 Color: 5
Size: 254244 Color: 8

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 426103 Color: 12
Size: 300058 Color: 9
Size: 273840 Color: 2

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 426120 Color: 11
Size: 303010 Color: 18
Size: 270871 Color: 11

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 426131 Color: 9
Size: 307631 Color: 7
Size: 266239 Color: 10

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 426137 Color: 15
Size: 293179 Color: 0
Size: 280685 Color: 13

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 426291 Color: 13
Size: 303010 Color: 6
Size: 270700 Color: 11

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 426302 Color: 18
Size: 309600 Color: 9
Size: 264099 Color: 5

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 426457 Color: 8
Size: 291755 Color: 1
Size: 281789 Color: 3

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 426483 Color: 2
Size: 298296 Color: 2
Size: 275222 Color: 7

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 426485 Color: 18
Size: 290684 Color: 9
Size: 282832 Color: 2

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 426558 Color: 5
Size: 315614 Color: 16
Size: 257829 Color: 6

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 426593 Color: 9
Size: 313864 Color: 12
Size: 259544 Color: 4

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 426622 Color: 14
Size: 305357 Color: 18
Size: 268022 Color: 1

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 426679 Color: 4
Size: 306114 Color: 1
Size: 267208 Color: 19

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 426742 Color: 5
Size: 300578 Color: 19
Size: 272681 Color: 19

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 426810 Color: 8
Size: 292077 Color: 14
Size: 281114 Color: 14

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 427056 Color: 7
Size: 310810 Color: 11
Size: 262135 Color: 12

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 426933 Color: 17
Size: 302446 Color: 10
Size: 270622 Color: 10

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 426958 Color: 12
Size: 309437 Color: 6
Size: 263606 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 427085 Color: 17
Size: 309372 Color: 18
Size: 263544 Color: 3

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 427088 Color: 10
Size: 297288 Color: 7
Size: 275625 Color: 17

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 427216 Color: 11
Size: 289312 Color: 0
Size: 283473 Color: 18

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 427241 Color: 2
Size: 302214 Color: 9
Size: 270546 Color: 9

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 427331 Color: 15
Size: 312290 Color: 16
Size: 260380 Color: 6

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 427354 Color: 4
Size: 304785 Color: 17
Size: 267862 Color: 5

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 427377 Color: 16
Size: 322539 Color: 14
Size: 250085 Color: 17

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 427392 Color: 19
Size: 318991 Color: 7
Size: 253618 Color: 5

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 427399 Color: 10
Size: 319086 Color: 15
Size: 253516 Color: 5

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 427403 Color: 16
Size: 295649 Color: 4
Size: 276949 Color: 19

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 427424 Color: 9
Size: 300746 Color: 14
Size: 271831 Color: 1

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 427430 Color: 3
Size: 288277 Color: 18
Size: 284294 Color: 13

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 427461 Color: 0
Size: 309251 Color: 17
Size: 263289 Color: 7

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 427579 Color: 1
Size: 317058 Color: 16
Size: 255364 Color: 4

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 427583 Color: 1
Size: 288003 Color: 8
Size: 284415 Color: 5

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 427584 Color: 8
Size: 321649 Color: 13
Size: 250768 Color: 15

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 427669 Color: 8
Size: 318877 Color: 1
Size: 253455 Color: 17

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 427686 Color: 16
Size: 305553 Color: 8
Size: 266762 Color: 7

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 427745 Color: 0
Size: 312379 Color: 19
Size: 259877 Color: 2

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 427791 Color: 17
Size: 291651 Color: 13
Size: 280559 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 427837 Color: 3
Size: 287259 Color: 5
Size: 284905 Color: 9

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 427858 Color: 5
Size: 319741 Color: 0
Size: 252402 Color: 10

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 427867 Color: 1
Size: 301989 Color: 14
Size: 270145 Color: 19

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 427950 Color: 19
Size: 295197 Color: 13
Size: 276854 Color: 15

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 427972 Color: 8
Size: 304897 Color: 5
Size: 267132 Color: 9

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 428121 Color: 19
Size: 302423 Color: 3
Size: 269457 Color: 3

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 427990 Color: 12
Size: 318191 Color: 15
Size: 253820 Color: 17

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 427992 Color: 8
Size: 305128 Color: 1
Size: 266881 Color: 12

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 428067 Color: 1
Size: 304211 Color: 16
Size: 267723 Color: 18

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 428106 Color: 10
Size: 308085 Color: 9
Size: 263810 Color: 19

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 428114 Color: 7
Size: 309022 Color: 8
Size: 262865 Color: 14

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 428122 Color: 4
Size: 316222 Color: 4
Size: 255657 Color: 14

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 428146 Color: 1
Size: 296909 Color: 2
Size: 274946 Color: 17

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 428150 Color: 8
Size: 301082 Color: 10
Size: 270769 Color: 14

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 428155 Color: 4
Size: 293327 Color: 2
Size: 278519 Color: 11

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 428157 Color: 0
Size: 293310 Color: 3
Size: 278534 Color: 15

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 428338 Color: 11
Size: 304587 Color: 16
Size: 267076 Color: 6

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 428441 Color: 5
Size: 308987 Color: 2
Size: 262573 Color: 15

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 428506 Color: 16
Size: 287275 Color: 7
Size: 284220 Color: 19

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 428600 Color: 2
Size: 320370 Color: 4
Size: 251031 Color: 14

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 428705 Color: 9
Size: 307205 Color: 4
Size: 264091 Color: 13

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 428786 Color: 18
Size: 319065 Color: 6
Size: 252150 Color: 1

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 428790 Color: 6
Size: 320997 Color: 0
Size: 250214 Color: 14

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 428841 Color: 17
Size: 318233 Color: 16
Size: 252927 Color: 9

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 428883 Color: 12
Size: 315665 Color: 10
Size: 255453 Color: 19

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 428959 Color: 16
Size: 314655 Color: 4
Size: 256387 Color: 8

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 428991 Color: 9
Size: 305837 Color: 7
Size: 265173 Color: 16

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 429101 Color: 15
Size: 305724 Color: 0
Size: 265176 Color: 14

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 429276 Color: 13
Size: 313008 Color: 11
Size: 257717 Color: 11

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 429292 Color: 16
Size: 292309 Color: 14
Size: 278400 Color: 1

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 429354 Color: 0
Size: 312302 Color: 8
Size: 258345 Color: 19

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 429361 Color: 17
Size: 306813 Color: 16
Size: 263827 Color: 12

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 429369 Color: 11
Size: 313163 Color: 18
Size: 257469 Color: 8

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 429416 Color: 7
Size: 299279 Color: 16
Size: 271306 Color: 11

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 429497 Color: 12
Size: 286442 Color: 16
Size: 284062 Color: 19

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 429508 Color: 6
Size: 292387 Color: 4
Size: 278106 Color: 13

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 429535 Color: 6
Size: 314772 Color: 3
Size: 255694 Color: 5

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 429588 Color: 12
Size: 293949 Color: 15
Size: 276464 Color: 5

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 429723 Color: 3
Size: 304409 Color: 1
Size: 265869 Color: 10

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 429740 Color: 4
Size: 309149 Color: 16
Size: 261112 Color: 8

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 429744 Color: 8
Size: 301695 Color: 0
Size: 268562 Color: 7

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 429887 Color: 19
Size: 285993 Color: 18
Size: 284121 Color: 17

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 429853 Color: 8
Size: 293186 Color: 17
Size: 276962 Color: 2

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 429894 Color: 0
Size: 304508 Color: 0
Size: 265599 Color: 17

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 429927 Color: 1
Size: 289547 Color: 18
Size: 280527 Color: 1

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 430022 Color: 5
Size: 310707 Color: 17
Size: 259272 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 430065 Color: 11
Size: 288083 Color: 19
Size: 281853 Color: 17

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 430082 Color: 17
Size: 304525 Color: 5
Size: 265394 Color: 12

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 430105 Color: 0
Size: 316941 Color: 5
Size: 252955 Color: 9

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 430131 Color: 3
Size: 289871 Color: 11
Size: 279999 Color: 6

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 430132 Color: 17
Size: 296146 Color: 9
Size: 273723 Color: 2

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 430147 Color: 12
Size: 293556 Color: 8
Size: 276298 Color: 7

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 430261 Color: 2
Size: 295096 Color: 0
Size: 274644 Color: 2

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 430274 Color: 11
Size: 285627 Color: 9
Size: 284100 Color: 19

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 430371 Color: 9
Size: 296267 Color: 7
Size: 273363 Color: 15

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 430387 Color: 10
Size: 285409 Color: 2
Size: 284205 Color: 12

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 430434 Color: 6
Size: 297436 Color: 3
Size: 272131 Color: 12

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 430449 Color: 9
Size: 297817 Color: 3
Size: 271735 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 430469 Color: 4
Size: 288216 Color: 3
Size: 281316 Color: 5

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 430473 Color: 2
Size: 299074 Color: 1
Size: 270454 Color: 19

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 430519 Color: 14
Size: 287092 Color: 16
Size: 282390 Color: 18

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 430566 Color: 16
Size: 289960 Color: 13
Size: 279475 Color: 18

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 430587 Color: 7
Size: 285422 Color: 15
Size: 283992 Color: 6

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 430623 Color: 1
Size: 308722 Color: 3
Size: 260656 Color: 6

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 430646 Color: 3
Size: 306571 Color: 12
Size: 262784 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 430933 Color: 19
Size: 290796 Color: 18
Size: 278272 Color: 10

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 430789 Color: 7
Size: 287408 Color: 10
Size: 281804 Color: 12

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 430804 Color: 2
Size: 301664 Color: 13
Size: 267533 Color: 3

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 430895 Color: 16
Size: 300170 Color: 7
Size: 268936 Color: 3

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 430921 Color: 13
Size: 317646 Color: 16
Size: 251434 Color: 19

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 431136 Color: 6
Size: 284943 Color: 15
Size: 283922 Color: 8

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 431143 Color: 17
Size: 298945 Color: 11
Size: 269913 Color: 1

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 431163 Color: 8
Size: 301509 Color: 7
Size: 267329 Color: 14

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 431183 Color: 3
Size: 284523 Color: 10
Size: 284295 Color: 7

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 431343 Color: 7
Size: 301139 Color: 6
Size: 267519 Color: 9

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 431504 Color: 18
Size: 314462 Color: 19
Size: 254035 Color: 2

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 431583 Color: 5
Size: 302633 Color: 14
Size: 265785 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 431599 Color: 10
Size: 300548 Color: 12
Size: 267854 Color: 5

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 431644 Color: 13
Size: 303210 Color: 4
Size: 265147 Color: 2

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 431841 Color: 3
Size: 306417 Color: 5
Size: 261743 Color: 9

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 431848 Color: 17
Size: 311321 Color: 9
Size: 256832 Color: 10

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 431886 Color: 6
Size: 317765 Color: 19
Size: 250350 Color: 9

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 431894 Color: 15
Size: 305599 Color: 0
Size: 262508 Color: 14

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 432029 Color: 11
Size: 315177 Color: 4
Size: 252795 Color: 15

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 432050 Color: 10
Size: 292227 Color: 8
Size: 275724 Color: 15

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 432052 Color: 2
Size: 311641 Color: 1
Size: 256308 Color: 12

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 432121 Color: 0
Size: 296090 Color: 15
Size: 271790 Color: 18

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 432261 Color: 11
Size: 302132 Color: 12
Size: 265608 Color: 5

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 432393 Color: 8
Size: 296920 Color: 4
Size: 270688 Color: 16

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 432487 Color: 17
Size: 301703 Color: 9
Size: 265811 Color: 2

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 432519 Color: 11
Size: 285851 Color: 9
Size: 281631 Color: 11

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 432705 Color: 19
Size: 292438 Color: 16
Size: 274858 Color: 18

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 432714 Color: 13
Size: 298771 Color: 19
Size: 268516 Color: 16

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 432726 Color: 6
Size: 293457 Color: 3
Size: 273818 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 432754 Color: 7
Size: 296264 Color: 1
Size: 270983 Color: 3

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 432804 Color: 10
Size: 293503 Color: 4
Size: 273694 Color: 7

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 432815 Color: 9
Size: 317092 Color: 13
Size: 250094 Color: 12

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 432842 Color: 18
Size: 287473 Color: 16
Size: 279686 Color: 11

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 432848 Color: 9
Size: 300387 Color: 6
Size: 266766 Color: 19

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 432919 Color: 9
Size: 308779 Color: 7
Size: 258303 Color: 16

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 432928 Color: 2
Size: 306492 Color: 18
Size: 260581 Color: 17

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 432938 Color: 10
Size: 308138 Color: 15
Size: 258925 Color: 7

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 432986 Color: 9
Size: 289979 Color: 16
Size: 277036 Color: 13

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 433030 Color: 6
Size: 283492 Color: 5
Size: 283479 Color: 5

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 433032 Color: 14
Size: 294327 Color: 19
Size: 272642 Color: 5

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 433102 Color: 5
Size: 304993 Color: 2
Size: 261906 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 433103 Color: 15
Size: 292342 Color: 5
Size: 274556 Color: 18

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 433200 Color: 12
Size: 305739 Color: 0
Size: 261062 Color: 17

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 433246 Color: 3
Size: 291797 Color: 13
Size: 274958 Color: 4

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 433259 Color: 11
Size: 285325 Color: 9
Size: 281417 Color: 5

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 433301 Color: 10
Size: 295790 Color: 9
Size: 270910 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 433340 Color: 16
Size: 289450 Color: 5
Size: 277211 Color: 19

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 433469 Color: 12
Size: 295232 Color: 12
Size: 271300 Color: 6

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 433474 Color: 10
Size: 297751 Color: 9
Size: 268776 Color: 14

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 433480 Color: 9
Size: 305045 Color: 15
Size: 261476 Color: 15

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 433484 Color: 1
Size: 290584 Color: 9
Size: 275933 Color: 4

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 433497 Color: 15
Size: 283402 Color: 1
Size: 283102 Color: 11

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 433497 Color: 6
Size: 295055 Color: 16
Size: 271449 Color: 12

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 433573 Color: 19
Size: 314885 Color: 15
Size: 251543 Color: 1

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 433544 Color: 5
Size: 294026 Color: 13
Size: 272431 Color: 10

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 433548 Color: 1
Size: 288055 Color: 8
Size: 278398 Color: 3

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 433574 Color: 0
Size: 295317 Color: 15
Size: 271110 Color: 3

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 433654 Color: 7
Size: 315410 Color: 14
Size: 250937 Color: 6

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 433745 Color: 6
Size: 307733 Color: 6
Size: 258523 Color: 12

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 433807 Color: 19
Size: 292191 Color: 12
Size: 274003 Color: 9

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 433837 Color: 14
Size: 295961 Color: 18
Size: 270203 Color: 1

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 433923 Color: 4
Size: 298434 Color: 0
Size: 267644 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 433933 Color: 4
Size: 287670 Color: 4
Size: 278398 Color: 17

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 434045 Color: 3
Size: 304923 Color: 11
Size: 261033 Color: 17

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 434100 Color: 9
Size: 287797 Color: 19
Size: 278104 Color: 4

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 434170 Color: 2
Size: 310770 Color: 7
Size: 255061 Color: 5

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 434181 Color: 3
Size: 306408 Color: 2
Size: 259412 Color: 12

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 434332 Color: 4
Size: 311608 Color: 8
Size: 254061 Color: 4

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 434360 Color: 5
Size: 304423 Color: 0
Size: 261218 Color: 10

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 434472 Color: 14
Size: 284272 Color: 8
Size: 281257 Color: 5

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 434572 Color: 9
Size: 305348 Color: 19
Size: 260081 Color: 17

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 434689 Color: 11
Size: 295805 Color: 8
Size: 269507 Color: 1

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 434699 Color: 8
Size: 289827 Color: 9
Size: 275475 Color: 5

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 434873 Color: 9
Size: 295618 Color: 16
Size: 269510 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 434914 Color: 18
Size: 301273 Color: 11
Size: 263814 Color: 6

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 434920 Color: 8
Size: 299960 Color: 19
Size: 265121 Color: 2

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 434990 Color: 7
Size: 290098 Color: 18
Size: 274913 Color: 12

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 435044 Color: 2
Size: 310605 Color: 13
Size: 254352 Color: 15

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 435050 Color: 12
Size: 293402 Color: 5
Size: 271549 Color: 15

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 435099 Color: 11
Size: 301449 Color: 11
Size: 263453 Color: 3

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 435134 Color: 5
Size: 310511 Color: 9
Size: 254356 Color: 19

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 435171 Color: 14
Size: 288998 Color: 1
Size: 275832 Color: 4

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 435180 Color: 9
Size: 295626 Color: 17
Size: 269195 Color: 3

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 435412 Color: 2
Size: 299447 Color: 13
Size: 265142 Color: 14

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 435441 Color: 15
Size: 301795 Color: 8
Size: 262765 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 435549 Color: 19
Size: 290754 Color: 2
Size: 273698 Color: 9

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 435552 Color: 17
Size: 313959 Color: 3
Size: 250490 Color: 17

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 435589 Color: 10
Size: 282285 Color: 18
Size: 282127 Color: 18

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 435602 Color: 17
Size: 312747 Color: 5
Size: 251652 Color: 17

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 435662 Color: 6
Size: 305763 Color: 15
Size: 258576 Color: 4

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 435799 Color: 6
Size: 286882 Color: 9
Size: 277320 Color: 1

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 435901 Color: 19
Size: 303406 Color: 10
Size: 260694 Color: 14

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 435815 Color: 8
Size: 282529 Color: 14
Size: 281657 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 435844 Color: 13
Size: 301943 Color: 3
Size: 262214 Color: 14

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 435928 Color: 2
Size: 297183 Color: 12
Size: 266890 Color: 7

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 435971 Color: 12
Size: 303519 Color: 18
Size: 260511 Color: 14

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 436035 Color: 9
Size: 290584 Color: 19
Size: 273382 Color: 16

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 436120 Color: 16
Size: 310971 Color: 5
Size: 252910 Color: 10

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 436224 Color: 12
Size: 302248 Color: 15
Size: 261529 Color: 18

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 436233 Color: 11
Size: 310760 Color: 12
Size: 253008 Color: 15

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 436245 Color: 15
Size: 295230 Color: 5
Size: 268526 Color: 6

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 436246 Color: 13
Size: 296466 Color: 9
Size: 267289 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 436393 Color: 19
Size: 306917 Color: 14
Size: 256691 Color: 1

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 436340 Color: 11
Size: 292091 Color: 14
Size: 271570 Color: 17

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 436519 Color: 18
Size: 310369 Color: 2
Size: 253113 Color: 8

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 436561 Color: 10
Size: 292901 Color: 12
Size: 270539 Color: 15

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 436577 Color: 4
Size: 285736 Color: 3
Size: 277688 Color: 18

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 436582 Color: 18
Size: 303760 Color: 6
Size: 259659 Color: 17

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 436760 Color: 14
Size: 310009 Color: 15
Size: 253232 Color: 9

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 436936 Color: 19
Size: 300043 Color: 4
Size: 263022 Color: 17

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 436848 Color: 4
Size: 286799 Color: 2
Size: 276354 Color: 8

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 436858 Color: 10
Size: 304652 Color: 4
Size: 258491 Color: 7

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 436879 Color: 17
Size: 285449 Color: 4
Size: 277673 Color: 16

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 436911 Color: 7
Size: 305518 Color: 12
Size: 257572 Color: 2

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 437028 Color: 8
Size: 300017 Color: 16
Size: 262956 Color: 2

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 437029 Color: 15
Size: 311046 Color: 4
Size: 251926 Color: 3

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 437321 Color: 19
Size: 302048 Color: 17
Size: 260632 Color: 9

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 437154 Color: 10
Size: 293774 Color: 8
Size: 269073 Color: 9

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 437165 Color: 3
Size: 311310 Color: 12
Size: 251526 Color: 16

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 437343 Color: 19
Size: 287093 Color: 8
Size: 275565 Color: 7

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 437180 Color: 7
Size: 288374 Color: 2
Size: 274447 Color: 16

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 437190 Color: 9
Size: 291147 Color: 13
Size: 271664 Color: 9

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 437301 Color: 6
Size: 292369 Color: 10
Size: 270331 Color: 18

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 437421 Color: 19
Size: 305673 Color: 15
Size: 256907 Color: 11

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 437383 Color: 12
Size: 307239 Color: 6
Size: 255379 Color: 18

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 437385 Color: 6
Size: 295459 Color: 16
Size: 267157 Color: 8

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 437386 Color: 13
Size: 307443 Color: 1
Size: 255172 Color: 11

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 437461 Color: 6
Size: 302516 Color: 19
Size: 260024 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 437584 Color: 3
Size: 287002 Color: 10
Size: 275415 Color: 3

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 437818 Color: 10
Size: 282669 Color: 6
Size: 279514 Color: 7

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 437825 Color: 3
Size: 292605 Color: 6
Size: 269571 Color: 2

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 437836 Color: 3
Size: 304545 Color: 2
Size: 257620 Color: 10

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 437925 Color: 18
Size: 294174 Color: 15
Size: 267902 Color: 3

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 437946 Color: 14
Size: 295742 Color: 5
Size: 266313 Color: 13

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 437950 Color: 14
Size: 282288 Color: 15
Size: 279763 Color: 1

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 437965 Color: 2
Size: 297513 Color: 1
Size: 264523 Color: 15

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 438048 Color: 4
Size: 298224 Color: 10
Size: 263729 Color: 19

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 438049 Color: 8
Size: 294319 Color: 1
Size: 267633 Color: 18

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 438084 Color: 15
Size: 282289 Color: 2
Size: 279628 Color: 9

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 438178 Color: 12
Size: 294505 Color: 11
Size: 267318 Color: 6

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 438285 Color: 15
Size: 281646 Color: 9
Size: 280070 Color: 13

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 438346 Color: 1
Size: 310843 Color: 11
Size: 250812 Color: 17

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 438379 Color: 18
Size: 281921 Color: 1
Size: 279701 Color: 19

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 438414 Color: 1
Size: 286468 Color: 2
Size: 275119 Color: 13

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 438463 Color: 0
Size: 311496 Color: 14
Size: 250042 Color: 15

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 438561 Color: 2
Size: 305908 Color: 18
Size: 255532 Color: 5

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 438577 Color: 2
Size: 286429 Color: 8
Size: 274995 Color: 13

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 438600 Color: 12
Size: 308652 Color: 18
Size: 252749 Color: 11

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 438627 Color: 10
Size: 285704 Color: 11
Size: 275670 Color: 13

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 438715 Color: 2
Size: 294519 Color: 6
Size: 266767 Color: 17

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 438719 Color: 15
Size: 304549 Color: 10
Size: 256733 Color: 3

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 438945 Color: 19
Size: 282683 Color: 14
Size: 278373 Color: 8

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 438830 Color: 2
Size: 297666 Color: 5
Size: 263505 Color: 15

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 438987 Color: 10
Size: 281827 Color: 1
Size: 279187 Color: 10

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 439001 Color: 12
Size: 297263 Color: 5
Size: 263737 Color: 5

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 439193 Color: 19
Size: 289335 Color: 7
Size: 271473 Color: 12

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 439200 Color: 4
Size: 301892 Color: 11
Size: 258909 Color: 12

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 439243 Color: 7
Size: 304992 Color: 4
Size: 255766 Color: 5

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 439275 Color: 12
Size: 300784 Color: 2
Size: 259942 Color: 15

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 439295 Color: 12
Size: 293998 Color: 8
Size: 266708 Color: 0

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 439298 Color: 13
Size: 301653 Color: 9
Size: 259050 Color: 5

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 439327 Color: 7
Size: 301829 Color: 15
Size: 258845 Color: 19

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 439378 Color: 7
Size: 299490 Color: 16
Size: 261133 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 439391 Color: 6
Size: 307920 Color: 17
Size: 252690 Color: 18

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 439421 Color: 14
Size: 302963 Color: 6
Size: 257617 Color: 13

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 439468 Color: 14
Size: 296653 Color: 15
Size: 263880 Color: 18

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 439538 Color: 15
Size: 281054 Color: 9
Size: 279409 Color: 16

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 439657 Color: 3
Size: 289343 Color: 8
Size: 271001 Color: 19

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 439715 Color: 4
Size: 291480 Color: 12
Size: 268806 Color: 2

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 439779 Color: 5
Size: 302948 Color: 8
Size: 257274 Color: 16

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 439815 Color: 1
Size: 306062 Color: 6
Size: 254124 Color: 17

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 439854 Color: 14
Size: 303420 Color: 8
Size: 256727 Color: 4

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 439883 Color: 15
Size: 299198 Color: 4
Size: 260920 Color: 9

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 439941 Color: 15
Size: 292869 Color: 4
Size: 267191 Color: 19

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 440028 Color: 9
Size: 295812 Color: 0
Size: 264161 Color: 18

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 440046 Color: 17
Size: 294306 Color: 9
Size: 265649 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 440268 Color: 4
Size: 285144 Color: 5
Size: 274589 Color: 3

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 440519 Color: 9
Size: 284512 Color: 16
Size: 274970 Color: 13

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 440612 Color: 5
Size: 308660 Color: 2
Size: 250729 Color: 18

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 440704 Color: 15
Size: 305345 Color: 18
Size: 253952 Color: 10

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 440791 Color: 2
Size: 289218 Color: 16
Size: 269992 Color: 13

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 440862 Color: 8
Size: 300580 Color: 15
Size: 258559 Color: 5

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 440982 Color: 14
Size: 305683 Color: 11
Size: 253336 Color: 5

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 440987 Color: 18
Size: 286334 Color: 2
Size: 272680 Color: 17

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 441010 Color: 6
Size: 303279 Color: 7
Size: 255712 Color: 16

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 441075 Color: 14
Size: 304732 Color: 4
Size: 254194 Color: 17

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 441143 Color: 6
Size: 291989 Color: 12
Size: 266869 Color: 16

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 441215 Color: 13
Size: 287167 Color: 8
Size: 271619 Color: 19

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 441386 Color: 0
Size: 282616 Color: 2
Size: 275999 Color: 11

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 441425 Color: 12
Size: 298867 Color: 11
Size: 259709 Color: 3

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 441488 Color: 0
Size: 307379 Color: 12
Size: 251134 Color: 18

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 441494 Color: 11
Size: 305143 Color: 6
Size: 253364 Color: 16

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 441507 Color: 9
Size: 294808 Color: 10
Size: 263686 Color: 3

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 441533 Color: 12
Size: 282534 Color: 9
Size: 275934 Color: 19

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 441573 Color: 16
Size: 307979 Color: 11
Size: 250449 Color: 7

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 441596 Color: 7
Size: 283708 Color: 0
Size: 274697 Color: 18

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 441614 Color: 16
Size: 294663 Color: 0
Size: 263724 Color: 17

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 441618 Color: 12
Size: 308287 Color: 11
Size: 250096 Color: 5

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 441669 Color: 13
Size: 281529 Color: 18
Size: 276803 Color: 15

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 441692 Color: 1
Size: 291311 Color: 18
Size: 266998 Color: 16

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 441818 Color: 19
Size: 286112 Color: 11
Size: 272071 Color: 1

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 441769 Color: 18
Size: 297802 Color: 13
Size: 260430 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 441787 Color: 4
Size: 287051 Color: 7
Size: 271163 Color: 14

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 441829 Color: 4
Size: 297570 Color: 8
Size: 260602 Color: 18

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 441838 Color: 13
Size: 305971 Color: 7
Size: 252192 Color: 8

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 441959 Color: 19
Size: 284269 Color: 6
Size: 273773 Color: 19

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 441896 Color: 16
Size: 283147 Color: 8
Size: 274958 Color: 6

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 441897 Color: 10
Size: 294896 Color: 13
Size: 263208 Color: 1

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 441910 Color: 5
Size: 300012 Color: 13
Size: 258079 Color: 5

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 441974 Color: 12
Size: 304022 Color: 6
Size: 254005 Color: 19

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 441997 Color: 9
Size: 283945 Color: 9
Size: 274059 Color: 6

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 442066 Color: 3
Size: 279060 Color: 10
Size: 278875 Color: 10

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 442121 Color: 7
Size: 292119 Color: 6
Size: 265761 Color: 1

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 442491 Color: 13
Size: 293513 Color: 14
Size: 263997 Color: 2

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 442615 Color: 16
Size: 290347 Color: 19
Size: 267039 Color: 12

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 442700 Color: 15
Size: 306655 Color: 2
Size: 250646 Color: 6

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 442780 Color: 2
Size: 299886 Color: 18
Size: 257335 Color: 10

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 442811 Color: 17
Size: 278841 Color: 16
Size: 278349 Color: 10

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 442827 Color: 7
Size: 278644 Color: 17
Size: 278530 Color: 14

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 442852 Color: 8
Size: 298043 Color: 1
Size: 259106 Color: 16

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 442918 Color: 11
Size: 289376 Color: 2
Size: 267707 Color: 17

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 443074 Color: 6
Size: 292495 Color: 12
Size: 264432 Color: 12

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 443095 Color: 11
Size: 278837 Color: 5
Size: 278069 Color: 12

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 443102 Color: 3
Size: 280744 Color: 3
Size: 276155 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 443316 Color: 3
Size: 303257 Color: 17
Size: 253428 Color: 16

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 443342 Color: 14
Size: 280276 Color: 0
Size: 276383 Color: 12

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 443488 Color: 19
Size: 279367 Color: 10
Size: 277146 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 443555 Color: 16
Size: 294014 Color: 14
Size: 262432 Color: 4

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 443737 Color: 18
Size: 294943 Color: 1
Size: 261321 Color: 11

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 443776 Color: 13
Size: 297971 Color: 14
Size: 258254 Color: 5

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 443880 Color: 8
Size: 294450 Color: 19
Size: 261671 Color: 13

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 443911 Color: 0
Size: 302960 Color: 17
Size: 253130 Color: 2

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 443912 Color: 8
Size: 299984 Color: 1
Size: 256105 Color: 5

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 444002 Color: 7
Size: 305582 Color: 13
Size: 250417 Color: 8

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 444038 Color: 3
Size: 283728 Color: 13
Size: 272235 Color: 4

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 444051 Color: 15
Size: 297094 Color: 9
Size: 258856 Color: 16

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 444121 Color: 7
Size: 279313 Color: 10
Size: 276567 Color: 9

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 444141 Color: 16
Size: 294965 Color: 8
Size: 260895 Color: 10

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 444152 Color: 11
Size: 294685 Color: 6
Size: 261164 Color: 17

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 444384 Color: 19
Size: 305575 Color: 13
Size: 250042 Color: 14

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 444254 Color: 15
Size: 279914 Color: 4
Size: 275833 Color: 7

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 444282 Color: 9
Size: 289896 Color: 2
Size: 265823 Color: 3

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 444286 Color: 10
Size: 280030 Color: 7
Size: 275685 Color: 8

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 444393 Color: 19
Size: 288550 Color: 2
Size: 267058 Color: 14

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 444370 Color: 17
Size: 298658 Color: 3
Size: 256973 Color: 9

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 444372 Color: 7
Size: 279623 Color: 18
Size: 276006 Color: 12

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 444406 Color: 1
Size: 284869 Color: 15
Size: 270726 Color: 14

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 444453 Color: 1
Size: 295572 Color: 13
Size: 259976 Color: 9

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 444486 Color: 17
Size: 282391 Color: 19
Size: 273124 Color: 10

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 444647 Color: 11
Size: 300288 Color: 2
Size: 255066 Color: 4

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 444690 Color: 15
Size: 281902 Color: 15
Size: 273409 Color: 12

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 444778 Color: 17
Size: 294072 Color: 2
Size: 261151 Color: 15

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 444800 Color: 2
Size: 283028 Color: 6
Size: 272173 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 444848 Color: 13
Size: 279314 Color: 2
Size: 275839 Color: 9

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 445027 Color: 8
Size: 291754 Color: 1
Size: 263220 Color: 19

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 445103 Color: 2
Size: 293963 Color: 16
Size: 260935 Color: 11

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 445115 Color: 12
Size: 286611 Color: 15
Size: 268275 Color: 10

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 445145 Color: 10
Size: 304668 Color: 5
Size: 250188 Color: 12

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 445191 Color: 9
Size: 283080 Color: 0
Size: 271730 Color: 8

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 445212 Color: 17
Size: 301299 Color: 5
Size: 253490 Color: 9

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 445405 Color: 19
Size: 304476 Color: 2
Size: 250120 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 445330 Color: 12
Size: 291029 Color: 16
Size: 263642 Color: 2

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 445384 Color: 12
Size: 300462 Color: 9
Size: 254155 Color: 17

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 445470 Color: 10
Size: 284132 Color: 18
Size: 270399 Color: 7

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 445471 Color: 4
Size: 284844 Color: 10
Size: 269686 Color: 11

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 445518 Color: 13
Size: 290375 Color: 0
Size: 264108 Color: 4

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 445558 Color: 6
Size: 300478 Color: 10
Size: 253965 Color: 10

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 445619 Color: 13
Size: 285171 Color: 5
Size: 269211 Color: 14

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 445843 Color: 18
Size: 300834 Color: 6
Size: 253324 Color: 16

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 445895 Color: 11
Size: 279440 Color: 9
Size: 274666 Color: 19

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 445939 Color: 9
Size: 279778 Color: 16
Size: 274284 Color: 7

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 445957 Color: 7
Size: 281943 Color: 6
Size: 272101 Color: 10

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 446076 Color: 7
Size: 281206 Color: 8
Size: 272719 Color: 17

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 446168 Color: 15
Size: 293281 Color: 2
Size: 260552 Color: 1

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 446172 Color: 4
Size: 280977 Color: 10
Size: 272852 Color: 17

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 446268 Color: 4
Size: 290507 Color: 11
Size: 263226 Color: 11

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 446355 Color: 16
Size: 277690 Color: 17
Size: 275956 Color: 7

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 446730 Color: 19
Size: 289206 Color: 3
Size: 264065 Color: 5

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 446686 Color: 12
Size: 280948 Color: 12
Size: 272367 Color: 4

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 446697 Color: 8
Size: 276704 Color: 1
Size: 276600 Color: 12

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 446792 Color: 11
Size: 292523 Color: 19
Size: 260686 Color: 4

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 446816 Color: 12
Size: 300458 Color: 10
Size: 252727 Color: 9

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 447043 Color: 11
Size: 291127 Color: 6
Size: 261831 Color: 1

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 447086 Color: 16
Size: 281318 Color: 18
Size: 271597 Color: 9

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 447347 Color: 0
Size: 276330 Color: 15
Size: 276324 Color: 2

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 447379 Color: 13
Size: 285340 Color: 10
Size: 267282 Color: 18

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 447398 Color: 6
Size: 301721 Color: 10
Size: 250882 Color: 9

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 447469 Color: 7
Size: 288425 Color: 3
Size: 264107 Color: 16

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 447521 Color: 3
Size: 289767 Color: 5
Size: 262713 Color: 3

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 447704 Color: 12
Size: 298623 Color: 12
Size: 253674 Color: 3

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 447763 Color: 16
Size: 279696 Color: 18
Size: 272542 Color: 19

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 447768 Color: 3
Size: 283894 Color: 11
Size: 268339 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 447908 Color: 5
Size: 289129 Color: 11
Size: 262964 Color: 2

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 448080 Color: 13
Size: 290461 Color: 5
Size: 261460 Color: 5

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 448100 Color: 2
Size: 279431 Color: 15
Size: 272470 Color: 13

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 448117 Color: 3
Size: 292625 Color: 12
Size: 259259 Color: 9

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 448138 Color: 7
Size: 295500 Color: 12
Size: 256363 Color: 3

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 448319 Color: 19
Size: 289991 Color: 7
Size: 261691 Color: 12

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 448177 Color: 13
Size: 295891 Color: 13
Size: 255933 Color: 16

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 448295 Color: 0
Size: 285889 Color: 1
Size: 265817 Color: 4

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 448328 Color: 3
Size: 286304 Color: 7
Size: 265369 Color: 15

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 448357 Color: 10
Size: 300294 Color: 19
Size: 251350 Color: 2

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 448583 Color: 1
Size: 276623 Color: 15
Size: 274795 Color: 8

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 448612 Color: 8
Size: 299807 Color: 10
Size: 251582 Color: 6

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 448616 Color: 6
Size: 292658 Color: 7
Size: 258727 Color: 17

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 448620 Color: 3
Size: 292604 Color: 3
Size: 258777 Color: 4

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 448645 Color: 3
Size: 296614 Color: 5
Size: 254742 Color: 9

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 448703 Color: 0
Size: 294880 Color: 1
Size: 256418 Color: 5

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 448811 Color: 6
Size: 287254 Color: 18
Size: 263936 Color: 18

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 448958 Color: 19
Size: 285157 Color: 1
Size: 265886 Color: 16

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 448917 Color: 3
Size: 278571 Color: 16
Size: 272513 Color: 4

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 449018 Color: 9
Size: 284960 Color: 17
Size: 266023 Color: 18

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 449121 Color: 13
Size: 285807 Color: 14
Size: 265073 Color: 19

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 449361 Color: 9
Size: 279402 Color: 9
Size: 271238 Color: 18

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 449733 Color: 17
Size: 291956 Color: 16
Size: 258312 Color: 11

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 449786 Color: 18
Size: 294080 Color: 14
Size: 256135 Color: 3

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 449847 Color: 15
Size: 287488 Color: 8
Size: 262666 Color: 7

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 449865 Color: 11
Size: 276275 Color: 14
Size: 273861 Color: 14

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 449867 Color: 16
Size: 287021 Color: 10
Size: 263113 Color: 19

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 449979 Color: 0
Size: 282830 Color: 4
Size: 267192 Color: 13

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 449988 Color: 1
Size: 288261 Color: 17
Size: 261752 Color: 17

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 450096 Color: 15
Size: 276974 Color: 14
Size: 272931 Color: 17

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 450116 Color: 2
Size: 289596 Color: 7
Size: 260289 Color: 12

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 450118 Color: 8
Size: 287580 Color: 11
Size: 262303 Color: 19

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 450253 Color: 13
Size: 281579 Color: 11
Size: 268169 Color: 7

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 450285 Color: 17
Size: 291046 Color: 5
Size: 258670 Color: 2

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 450313 Color: 4
Size: 276047 Color: 12
Size: 273641 Color: 1

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 450340 Color: 14
Size: 294137 Color: 2
Size: 255524 Color: 5

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 450347 Color: 14
Size: 295537 Color: 9
Size: 254117 Color: 4

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 450354 Color: 14
Size: 277572 Color: 19
Size: 272075 Color: 11

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 450438 Color: 14
Size: 285212 Color: 15
Size: 264351 Color: 3

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 450442 Color: 8
Size: 284663 Color: 13
Size: 264896 Color: 12

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 450515 Color: 3
Size: 292278 Color: 5
Size: 257208 Color: 7

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 450562 Color: 3
Size: 297202 Color: 1
Size: 252237 Color: 9

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 450674 Color: 12
Size: 296809 Color: 17
Size: 252518 Color: 7

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 450777 Color: 18
Size: 286283 Color: 19
Size: 262941 Color: 8

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 450778 Color: 7
Size: 276682 Color: 9
Size: 272541 Color: 10

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 450804 Color: 11
Size: 292025 Color: 14
Size: 257172 Color: 12

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 450811 Color: 12
Size: 297822 Color: 4
Size: 251368 Color: 2

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 450922 Color: 6
Size: 296763 Color: 3
Size: 252316 Color: 1

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 451122 Color: 15
Size: 281741 Color: 4
Size: 267138 Color: 6

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 451155 Color: 3
Size: 288162 Color: 15
Size: 260684 Color: 9

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 451162 Color: 16
Size: 298551 Color: 3
Size: 250288 Color: 18

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 451257 Color: 5
Size: 290256 Color: 2
Size: 258488 Color: 14

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 451261 Color: 3
Size: 277928 Color: 11
Size: 270812 Color: 8

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 451263 Color: 14
Size: 278521 Color: 19
Size: 270217 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 451278 Color: 10
Size: 275838 Color: 1
Size: 272885 Color: 7

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 451287 Color: 16
Size: 290619 Color: 9
Size: 258095 Color: 9

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 451355 Color: 0
Size: 284138 Color: 4
Size: 264508 Color: 17

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 451447 Color: 12
Size: 292263 Color: 1
Size: 256291 Color: 18

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 451452 Color: 9
Size: 295330 Color: 7
Size: 253219 Color: 10

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 451475 Color: 17
Size: 276074 Color: 12
Size: 272452 Color: 18

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 451533 Color: 9
Size: 278683 Color: 17
Size: 269785 Color: 19

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 451552 Color: 18
Size: 296905 Color: 2
Size: 251544 Color: 7

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 451554 Color: 1
Size: 276608 Color: 4
Size: 271839 Color: 5

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 451620 Color: 0
Size: 283457 Color: 15
Size: 264924 Color: 5

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 451639 Color: 4
Size: 295333 Color: 12
Size: 253029 Color: 5

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 451656 Color: 3
Size: 295008 Color: 14
Size: 253337 Color: 19

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 451678 Color: 1
Size: 294965 Color: 13
Size: 253358 Color: 10

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 451749 Color: 8
Size: 278757 Color: 6
Size: 269495 Color: 17

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 451769 Color: 9
Size: 281148 Color: 1
Size: 267084 Color: 5

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 451801 Color: 12
Size: 296203 Color: 9
Size: 251997 Color: 15

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 451805 Color: 10
Size: 275444 Color: 15
Size: 272752 Color: 8

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 451814 Color: 0
Size: 275297 Color: 8
Size: 272890 Color: 19

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 451839 Color: 2
Size: 275835 Color: 4
Size: 272327 Color: 16

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 451913 Color: 0
Size: 294876 Color: 0
Size: 253212 Color: 9

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 451914 Color: 12
Size: 276353 Color: 4
Size: 271734 Color: 3

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 451986 Color: 16
Size: 280003 Color: 10
Size: 268012 Color: 13

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 452141 Color: 9
Size: 286535 Color: 3
Size: 261325 Color: 19

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 452169 Color: 4
Size: 286634 Color: 14
Size: 261198 Color: 3

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 452278 Color: 8
Size: 275186 Color: 6
Size: 272537 Color: 9

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 452296 Color: 14
Size: 292013 Color: 9
Size: 255692 Color: 8

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 452322 Color: 12
Size: 282581 Color: 0
Size: 265098 Color: 5

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 452390 Color: 12
Size: 284901 Color: 9
Size: 262710 Color: 4

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 452530 Color: 14
Size: 289507 Color: 5
Size: 257964 Color: 3

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 452555 Color: 12
Size: 295254 Color: 0
Size: 252192 Color: 18

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 452610 Color: 17
Size: 287757 Color: 9
Size: 259634 Color: 13

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 452642 Color: 14
Size: 290841 Color: 0
Size: 256518 Color: 4

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 452647 Color: 2
Size: 296663 Color: 19
Size: 250691 Color: 1

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 452723 Color: 13
Size: 276805 Color: 3
Size: 270473 Color: 11

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 452743 Color: 14
Size: 289793 Color: 1
Size: 257465 Color: 2

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 452770 Color: 12
Size: 287641 Color: 18
Size: 259590 Color: 9

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 453085 Color: 9
Size: 287136 Color: 13
Size: 259780 Color: 6

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 453077 Color: 18
Size: 278651 Color: 13
Size: 268273 Color: 12

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 453135 Color: 1
Size: 280674 Color: 2
Size: 266192 Color: 12

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 453167 Color: 19
Size: 288278 Color: 16
Size: 258556 Color: 6

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 453182 Color: 6
Size: 281240 Color: 10
Size: 265579 Color: 1

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 453306 Color: 10
Size: 279771 Color: 13
Size: 266924 Color: 3

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 453371 Color: 18
Size: 286980 Color: 8
Size: 259650 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 453402 Color: 1
Size: 273851 Color: 10
Size: 272748 Color: 18

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 453413 Color: 2
Size: 286645 Color: 6
Size: 259943 Color: 10

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 453569 Color: 19
Size: 273566 Color: 3
Size: 272866 Color: 14

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 453708 Color: 12
Size: 279446 Color: 19
Size: 266847 Color: 13

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 453712 Color: 5
Size: 281338 Color: 9
Size: 264951 Color: 11

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 453823 Color: 10
Size: 286545 Color: 10
Size: 259633 Color: 7

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 453826 Color: 5
Size: 295446 Color: 4
Size: 250729 Color: 13

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 453855 Color: 4
Size: 278685 Color: 0
Size: 267461 Color: 12

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 453917 Color: 15
Size: 284487 Color: 8
Size: 261597 Color: 12

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 453929 Color: 4
Size: 291041 Color: 5
Size: 255031 Color: 10

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 454106 Color: 11
Size: 276375 Color: 19
Size: 269520 Color: 18

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 454118 Color: 7
Size: 284288 Color: 0
Size: 261595 Color: 11

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 454180 Color: 17
Size: 291485 Color: 14
Size: 254336 Color: 5

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 454375 Color: 9
Size: 286151 Color: 18
Size: 259475 Color: 8

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 454302 Color: 14
Size: 288437 Color: 4
Size: 257262 Color: 17

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 454382 Color: 6
Size: 277877 Color: 3
Size: 267742 Color: 16

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 454413 Color: 3
Size: 280574 Color: 19
Size: 265014 Color: 4

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 454456 Color: 18
Size: 294383 Color: 9
Size: 251162 Color: 18

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 454552 Color: 7
Size: 284516 Color: 11
Size: 260933 Color: 17

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 454619 Color: 17
Size: 282001 Color: 10
Size: 263381 Color: 15

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 454626 Color: 16
Size: 280617 Color: 0
Size: 264758 Color: 8

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 454628 Color: 18
Size: 283816 Color: 8
Size: 261557 Color: 3

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 454651 Color: 1
Size: 291009 Color: 3
Size: 254341 Color: 16

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 454662 Color: 4
Size: 289353 Color: 14
Size: 255986 Color: 16

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 454881 Color: 19
Size: 275970 Color: 5
Size: 269150 Color: 1

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 454913 Color: 7
Size: 286365 Color: 17
Size: 258723 Color: 7

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 455036 Color: 0
Size: 277039 Color: 0
Size: 267926 Color: 14

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 455082 Color: 7
Size: 280887 Color: 18
Size: 264032 Color: 14

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 455107 Color: 17
Size: 283815 Color: 1
Size: 261079 Color: 8

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 455122 Color: 2
Size: 280966 Color: 19
Size: 263913 Color: 4

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 455202 Color: 15
Size: 286834 Color: 14
Size: 257965 Color: 12

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 455215 Color: 6
Size: 290355 Color: 17
Size: 254431 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 455254 Color: 10
Size: 282345 Color: 11
Size: 262402 Color: 7

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 455281 Color: 9
Size: 276839 Color: 18
Size: 267881 Color: 11

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 455345 Color: 18
Size: 294037 Color: 15
Size: 250619 Color: 17

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 455377 Color: 9
Size: 276323 Color: 19
Size: 268301 Color: 12

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 455442 Color: 9
Size: 292123 Color: 7
Size: 252436 Color: 7

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 455425 Color: 3
Size: 293911 Color: 13
Size: 250665 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 455431 Color: 6
Size: 273020 Color: 14
Size: 271550 Color: 11

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 455551 Color: 12
Size: 292206 Color: 11
Size: 252244 Color: 10

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 455567 Color: 8
Size: 274787 Color: 2
Size: 269647 Color: 6

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 455580 Color: 15
Size: 279004 Color: 9
Size: 265417 Color: 2

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 455694 Color: 6
Size: 273920 Color: 4
Size: 270387 Color: 17

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 455695 Color: 11
Size: 274071 Color: 2
Size: 270235 Color: 8

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 455727 Color: 4
Size: 281955 Color: 6
Size: 262319 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 455780 Color: 16
Size: 273751 Color: 10
Size: 270470 Color: 9

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 455788 Color: 17
Size: 289550 Color: 16
Size: 254663 Color: 4

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 455899 Color: 7
Size: 285743 Color: 16
Size: 258359 Color: 3

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 455963 Color: 4
Size: 293445 Color: 4
Size: 250593 Color: 8

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 455981 Color: 6
Size: 283802 Color: 3
Size: 260218 Color: 16

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 456045 Color: 15
Size: 276584 Color: 10
Size: 267372 Color: 19

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 456203 Color: 5
Size: 275452 Color: 9
Size: 268346 Color: 1

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 456242 Color: 3
Size: 292865 Color: 14
Size: 250894 Color: 10

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 456269 Color: 14
Size: 280016 Color: 7
Size: 263716 Color: 6

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 456375 Color: 15
Size: 284720 Color: 5
Size: 258906 Color: 18

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 456392 Color: 13
Size: 291022 Color: 0
Size: 252587 Color: 8

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 456679 Color: 7
Size: 280767 Color: 5
Size: 262555 Color: 6

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 456726 Color: 2
Size: 286064 Color: 16
Size: 257211 Color: 6

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 456812 Color: 9
Size: 275674 Color: 6
Size: 267515 Color: 18

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 456805 Color: 16
Size: 272882 Color: 18
Size: 270314 Color: 16

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 456896 Color: 19
Size: 275156 Color: 15
Size: 267949 Color: 11

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 456954 Color: 4
Size: 277338 Color: 5
Size: 265709 Color: 11

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 456962 Color: 4
Size: 275707 Color: 17
Size: 267332 Color: 10

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 457025 Color: 2
Size: 276087 Color: 10
Size: 266889 Color: 9

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 457053 Color: 12
Size: 276804 Color: 7
Size: 266144 Color: 1

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 457063 Color: 13
Size: 284059 Color: 2
Size: 258879 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 457086 Color: 6
Size: 280032 Color: 1
Size: 262883 Color: 12

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 457098 Color: 5
Size: 280027 Color: 11
Size: 262876 Color: 16

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 457149 Color: 5
Size: 279671 Color: 1
Size: 263181 Color: 13

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 457199 Color: 19
Size: 290639 Color: 14
Size: 252163 Color: 6

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 457298 Color: 2
Size: 287073 Color: 14
Size: 255630 Color: 9

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 457330 Color: 2
Size: 283435 Color: 5
Size: 259236 Color: 18

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 457414 Color: 2
Size: 280637 Color: 19
Size: 261950 Color: 15

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 457465 Color: 15
Size: 283528 Color: 14
Size: 259008 Color: 5

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 457587 Color: 16
Size: 287371 Color: 15
Size: 255043 Color: 15

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 457591 Color: 3
Size: 273422 Color: 0
Size: 268988 Color: 4

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 457597 Color: 16
Size: 290929 Color: 9
Size: 251475 Color: 14

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 457603 Color: 19
Size: 292249 Color: 6
Size: 250149 Color: 16

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 457611 Color: 11
Size: 289110 Color: 1
Size: 253280 Color: 3

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 457638 Color: 14
Size: 283529 Color: 2
Size: 258834 Color: 6

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 457731 Color: 0
Size: 290915 Color: 16
Size: 251355 Color: 5

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 457751 Color: 2
Size: 292164 Color: 9
Size: 250086 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 457891 Color: 19
Size: 290288 Color: 16
Size: 251822 Color: 11

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 457979 Color: 15
Size: 274566 Color: 14
Size: 267456 Color: 17

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 457993 Color: 10
Size: 290589 Color: 17
Size: 251419 Color: 15

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 458041 Color: 17
Size: 280230 Color: 12
Size: 261730 Color: 12

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 458151 Color: 13
Size: 280886 Color: 14
Size: 260964 Color: 10

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 458183 Color: 18
Size: 286175 Color: 15
Size: 255643 Color: 11

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 458247 Color: 16
Size: 291049 Color: 8
Size: 250705 Color: 4

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 458305 Color: 4
Size: 282033 Color: 0
Size: 259663 Color: 16

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 458422 Color: 14
Size: 274962 Color: 12
Size: 266617 Color: 3

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 458558 Color: 9
Size: 271670 Color: 16
Size: 269773 Color: 10

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 458537 Color: 18
Size: 273388 Color: 8
Size: 268076 Color: 11

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 458554 Color: 6
Size: 273207 Color: 19
Size: 268240 Color: 8

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 458713 Color: 7
Size: 280438 Color: 1
Size: 260850 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 458770 Color: 15
Size: 286500 Color: 10
Size: 254731 Color: 13

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 458794 Color: 3
Size: 284520 Color: 1
Size: 256687 Color: 7

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 458842 Color: 12
Size: 284266 Color: 19
Size: 256893 Color: 6

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 458928 Color: 5
Size: 277086 Color: 12
Size: 263987 Color: 11

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 458979 Color: 5
Size: 285005 Color: 8
Size: 256017 Color: 16

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 459172 Color: 11
Size: 276546 Color: 1
Size: 264283 Color: 2

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 459302 Color: 13
Size: 280022 Color: 9
Size: 260677 Color: 15

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 459348 Color: 8
Size: 272062 Color: 3
Size: 268591 Color: 19

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 459373 Color: 14
Size: 278693 Color: 12
Size: 261935 Color: 10

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 459406 Color: 5
Size: 271096 Color: 11
Size: 269499 Color: 18

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 459487 Color: 18
Size: 282071 Color: 1
Size: 258443 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 459612 Color: 9
Size: 280639 Color: 17
Size: 259750 Color: 18

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 459613 Color: 4
Size: 286320 Color: 14
Size: 254068 Color: 18

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 459656 Color: 1
Size: 285832 Color: 6
Size: 254513 Color: 13

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 459745 Color: 1
Size: 270795 Color: 17
Size: 269461 Color: 16

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 459752 Color: 14
Size: 275277 Color: 0
Size: 264972 Color: 11

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 459780 Color: 16
Size: 283270 Color: 9
Size: 256951 Color: 11

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 459919 Color: 1
Size: 286634 Color: 10
Size: 253448 Color: 18

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 460138 Color: 18
Size: 288977 Color: 16
Size: 250886 Color: 17

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 460257 Color: 3
Size: 285503 Color: 10
Size: 254241 Color: 12

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 460340 Color: 19
Size: 280760 Color: 14
Size: 258901 Color: 14

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 460357 Color: 17
Size: 274422 Color: 4
Size: 265222 Color: 17

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 460568 Color: 18
Size: 277106 Color: 11
Size: 262327 Color: 3

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 460608 Color: 9
Size: 274809 Color: 10
Size: 264584 Color: 3

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 460631 Color: 11
Size: 277713 Color: 5
Size: 261657 Color: 19

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 460669 Color: 3
Size: 281449 Color: 7
Size: 257883 Color: 7

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 460681 Color: 1
Size: 283037 Color: 15
Size: 256283 Color: 4

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 460759 Color: 19
Size: 283662 Color: 18
Size: 255580 Color: 13

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 460768 Color: 7
Size: 284567 Color: 16
Size: 254666 Color: 8

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 460844 Color: 9
Size: 282734 Color: 1
Size: 256423 Color: 3

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 460833 Color: 17
Size: 280035 Color: 11
Size: 259133 Color: 13

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 460881 Color: 6
Size: 277861 Color: 16
Size: 261259 Color: 2

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 460925 Color: 10
Size: 280081 Color: 16
Size: 258995 Color: 11

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 460941 Color: 10
Size: 274742 Color: 7
Size: 264318 Color: 14

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 461020 Color: 19
Size: 281216 Color: 7
Size: 257765 Color: 6

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 461039 Color: 13
Size: 271016 Color: 9
Size: 267946 Color: 7

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 461097 Color: 6
Size: 272505 Color: 19
Size: 266399 Color: 17

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 461141 Color: 5
Size: 277323 Color: 2
Size: 261537 Color: 7

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 461143 Color: 2
Size: 288308 Color: 3
Size: 250550 Color: 8

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 461171 Color: 1
Size: 276392 Color: 6
Size: 262438 Color: 19

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 461258 Color: 5
Size: 282604 Color: 13
Size: 256139 Color: 9

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 461318 Color: 4
Size: 288315 Color: 0
Size: 250368 Color: 11

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 461342 Color: 2
Size: 275266 Color: 17
Size: 263393 Color: 16

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 461362 Color: 15
Size: 286530 Color: 4
Size: 252109 Color: 18

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 461487 Color: 11
Size: 271803 Color: 3
Size: 266711 Color: 14

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 461577 Color: 17
Size: 273855 Color: 4
Size: 264569 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 461628 Color: 17
Size: 281928 Color: 6
Size: 256445 Color: 9

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 461744 Color: 4
Size: 286124 Color: 19
Size: 252133 Color: 2

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 461795 Color: 11
Size: 287171 Color: 7
Size: 251035 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 461836 Color: 10
Size: 278457 Color: 3
Size: 259708 Color: 18

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 462097 Color: 19
Size: 279333 Color: 16
Size: 258571 Color: 9

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 462317 Color: 17
Size: 274935 Color: 13
Size: 262749 Color: 10

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 462425 Color: 13
Size: 277370 Color: 13
Size: 260206 Color: 5

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 462434 Color: 5
Size: 276343 Color: 14
Size: 261224 Color: 12

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 462522 Color: 6
Size: 275628 Color: 17
Size: 261851 Color: 16

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 462527 Color: 10
Size: 281856 Color: 15
Size: 255618 Color: 19

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 462564 Color: 4
Size: 268878 Color: 16
Size: 268559 Color: 9

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 462642 Color: 11
Size: 280164 Color: 3
Size: 257195 Color: 6

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 462776 Color: 13
Size: 277769 Color: 12
Size: 259456 Color: 12

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 462827 Color: 10
Size: 279071 Color: 6
Size: 258103 Color: 12

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 462848 Color: 5
Size: 281089 Color: 14
Size: 256064 Color: 6

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 462902 Color: 0
Size: 273927 Color: 9
Size: 263172 Color: 11

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 463017 Color: 13
Size: 280380 Color: 3
Size: 256604 Color: 8

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 463056 Color: 17
Size: 278974 Color: 0
Size: 257971 Color: 3

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 463104 Color: 15
Size: 283223 Color: 6
Size: 253674 Color: 16

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 463199 Color: 1
Size: 269103 Color: 2
Size: 267699 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 463243 Color: 12
Size: 273537 Color: 14
Size: 263221 Color: 11

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 463450 Color: 15
Size: 273310 Color: 9
Size: 263241 Color: 11

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 463687 Color: 14
Size: 281003 Color: 18
Size: 255311 Color: 7

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 463693 Color: 10
Size: 276804 Color: 12
Size: 259504 Color: 8

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 463833 Color: 19
Size: 278249 Color: 0
Size: 257919 Color: 11

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 463858 Color: 0
Size: 280582 Color: 13
Size: 255561 Color: 3

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 463926 Color: 15
Size: 276687 Color: 17
Size: 259388 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 463930 Color: 13
Size: 271910 Color: 7
Size: 264161 Color: 12

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 463957 Color: 17
Size: 279413 Color: 15
Size: 256631 Color: 1

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 464126 Color: 9
Size: 277987 Color: 1
Size: 257888 Color: 16

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 464078 Color: 4
Size: 282596 Color: 7
Size: 253327 Color: 18

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 464183 Color: 15
Size: 274792 Color: 2
Size: 261026 Color: 3

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 464235 Color: 3
Size: 283960 Color: 16
Size: 251806 Color: 10

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 464346 Color: 5
Size: 271064 Color: 19
Size: 264591 Color: 3

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 464422 Color: 11
Size: 274905 Color: 9
Size: 260674 Color: 4

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 464425 Color: 1
Size: 285139 Color: 2
Size: 250437 Color: 0

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 464445 Color: 8
Size: 281590 Color: 7
Size: 253966 Color: 11

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 464456 Color: 7
Size: 280119 Color: 3
Size: 255426 Color: 7

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 464457 Color: 18
Size: 282060 Color: 11
Size: 253484 Color: 1

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 464592 Color: 9
Size: 280790 Color: 18
Size: 254619 Color: 15

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 464562 Color: 3
Size: 273161 Color: 0
Size: 262278 Color: 15

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 464782 Color: 7
Size: 276951 Color: 0
Size: 258268 Color: 11

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 464846 Color: 5
Size: 273448 Color: 14
Size: 261707 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 464867 Color: 8
Size: 282451 Color: 9
Size: 252683 Color: 7

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 464884 Color: 11
Size: 268624 Color: 13
Size: 266493 Color: 19

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 464889 Color: 12
Size: 277770 Color: 3
Size: 257342 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 464923 Color: 6
Size: 269155 Color: 19
Size: 265923 Color: 8

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 464935 Color: 10
Size: 280915 Color: 15
Size: 254151 Color: 7

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 464948 Color: 17
Size: 271394 Color: 18
Size: 263659 Color: 1

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 465070 Color: 7
Size: 275883 Color: 9
Size: 259048 Color: 3

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 465074 Color: 5
Size: 272353 Color: 14
Size: 262574 Color: 1

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 465278 Color: 5
Size: 281463 Color: 12
Size: 253260 Color: 8

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 465279 Color: 0
Size: 284453 Color: 18
Size: 250269 Color: 8

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 465334 Color: 4
Size: 276115 Color: 7
Size: 258552 Color: 18

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 465452 Color: 2
Size: 275689 Color: 18
Size: 258860 Color: 9

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 465493 Color: 17
Size: 284498 Color: 16
Size: 250010 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 465500 Color: 19
Size: 275414 Color: 16
Size: 259087 Color: 11

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 465538 Color: 12
Size: 279446 Color: 11
Size: 255017 Color: 13

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 465679 Color: 0
Size: 282692 Color: 10
Size: 251630 Color: 1

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 465784 Color: 12
Size: 282794 Color: 15
Size: 251423 Color: 5

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 466043 Color: 4
Size: 274239 Color: 10
Size: 259719 Color: 18

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 466045 Color: 8
Size: 273879 Color: 0
Size: 260077 Color: 15

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 466136 Color: 16
Size: 275008 Color: 7
Size: 258857 Color: 10

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 466199 Color: 4
Size: 270142 Color: 11
Size: 263660 Color: 6

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 466268 Color: 10
Size: 280529 Color: 9
Size: 253204 Color: 19

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 466270 Color: 7
Size: 270164 Color: 9
Size: 263567 Color: 17

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 466283 Color: 16
Size: 272493 Color: 12
Size: 261225 Color: 13

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 466289 Color: 1
Size: 275247 Color: 14
Size: 258465 Color: 2

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 466340 Color: 2
Size: 278439 Color: 0
Size: 255222 Color: 6

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 466380 Color: 13
Size: 281534 Color: 1
Size: 252087 Color: 5

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 466572 Color: 13
Size: 283246 Color: 9
Size: 250183 Color: 3

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 466632 Color: 11
Size: 283078 Color: 10
Size: 250291 Color: 8

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 466634 Color: 0
Size: 275822 Color: 2
Size: 257545 Color: 3

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 466684 Color: 18
Size: 272458 Color: 16
Size: 260859 Color: 19

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 466722 Color: 13
Size: 275926 Color: 2
Size: 257353 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 466827 Color: 0
Size: 267523 Color: 8
Size: 265651 Color: 19

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 466831 Color: 6
Size: 280275 Color: 9
Size: 252895 Color: 10

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 466941 Color: 17
Size: 272643 Color: 2
Size: 260417 Color: 16

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 466973 Color: 3
Size: 280899 Color: 10
Size: 252129 Color: 11

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 467008 Color: 18
Size: 276239 Color: 17
Size: 256754 Color: 15

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 467190 Color: 6
Size: 268509 Color: 16
Size: 264302 Color: 3

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 467340 Color: 10
Size: 269631 Color: 2
Size: 263030 Color: 4

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 467432 Color: 2
Size: 281203 Color: 16
Size: 251366 Color: 18

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 467569 Color: 9
Size: 280632 Color: 2
Size: 251800 Color: 16

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 467688 Color: 14
Size: 280890 Color: 3
Size: 251423 Color: 11

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 467742 Color: 6
Size: 276597 Color: 16
Size: 255662 Color: 17

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 467823 Color: 18
Size: 266209 Color: 9
Size: 265969 Color: 13

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 467984 Color: 19
Size: 267088 Color: 5
Size: 264929 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 468014 Color: 3
Size: 271621 Color: 15
Size: 260366 Color: 4

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 468017 Color: 2
Size: 279861 Color: 16
Size: 252123 Color: 6

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 468054 Color: 15
Size: 280506 Color: 15
Size: 251441 Color: 5

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 468246 Color: 14
Size: 275892 Color: 1
Size: 255863 Color: 16

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 468264 Color: 11
Size: 281213 Color: 13
Size: 250524 Color: 9

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 468293 Color: 2
Size: 273779 Color: 0
Size: 257929 Color: 3

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 468365 Color: 7
Size: 272750 Color: 11
Size: 258886 Color: 19

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 468420 Color: 3
Size: 268980 Color: 13
Size: 262601 Color: 17

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 468422 Color: 15
Size: 278214 Color: 7
Size: 253365 Color: 18

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 468450 Color: 12
Size: 271112 Color: 5
Size: 260439 Color: 1

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 468485 Color: 7
Size: 274984 Color: 17
Size: 256532 Color: 0

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 468650 Color: 9
Size: 268372 Color: 6
Size: 262979 Color: 5

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 468566 Color: 8
Size: 268223 Color: 18
Size: 263212 Color: 2

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 468686 Color: 11
Size: 273418 Color: 19
Size: 257897 Color: 7

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 468688 Color: 3
Size: 271790 Color: 5
Size: 259523 Color: 11

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 468734 Color: 1
Size: 280759 Color: 5
Size: 250508 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 468851 Color: 12
Size: 271532 Color: 8
Size: 259618 Color: 12

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 468879 Color: 8
Size: 280025 Color: 8
Size: 251097 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 468906 Color: 18
Size: 277717 Color: 17
Size: 253378 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 468944 Color: 6
Size: 268992 Color: 3
Size: 262065 Color: 10

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 469130 Color: 9
Size: 277457 Color: 7
Size: 253414 Color: 6

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 469104 Color: 0
Size: 265551 Color: 12
Size: 265346 Color: 6

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 469156 Color: 19
Size: 273094 Color: 5
Size: 257751 Color: 9

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 469233 Color: 13
Size: 273773 Color: 7
Size: 256995 Color: 6

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 469578 Color: 13
Size: 279461 Color: 14
Size: 250962 Color: 16

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 469694 Color: 1
Size: 268314 Color: 5
Size: 261993 Color: 3

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 469774 Color: 18
Size: 269951 Color: 4
Size: 260276 Color: 14

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 469793 Color: 15
Size: 273526 Color: 9
Size: 256682 Color: 10

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 469810 Color: 5
Size: 271339 Color: 12
Size: 258852 Color: 6

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 469829 Color: 2
Size: 271501 Color: 3
Size: 258671 Color: 5

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 469932 Color: 16
Size: 271173 Color: 5
Size: 258896 Color: 4

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 469965 Color: 1
Size: 273950 Color: 5
Size: 256086 Color: 13

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 470021 Color: 15
Size: 275391 Color: 4
Size: 254589 Color: 19

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 470121 Color: 4
Size: 274004 Color: 17
Size: 255876 Color: 9

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 470291 Color: 14
Size: 270444 Color: 19
Size: 259266 Color: 17

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 470362 Color: 8
Size: 278332 Color: 10
Size: 251307 Color: 4

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 470385 Color: 2
Size: 266316 Color: 6
Size: 263300 Color: 5

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 470435 Color: 12
Size: 265264 Color: 7
Size: 264302 Color: 10

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 470445 Color: 4
Size: 275393 Color: 19
Size: 254163 Color: 9

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 470668 Color: 4
Size: 273194 Color: 0
Size: 256139 Color: 11

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 470719 Color: 13
Size: 267711 Color: 16
Size: 261571 Color: 5

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 470815 Color: 11
Size: 272105 Color: 8
Size: 257081 Color: 7

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 470896 Color: 12
Size: 265157 Color: 18
Size: 263948 Color: 1

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 470921 Color: 18
Size: 273375 Color: 13
Size: 255705 Color: 5

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 470958 Color: 18
Size: 275237 Color: 7
Size: 253806 Color: 4

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 471135 Color: 5
Size: 265196 Color: 1
Size: 263670 Color: 10

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 471140 Color: 17
Size: 273503 Color: 3
Size: 255358 Color: 17

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 471214 Color: 18
Size: 271994 Color: 7
Size: 256793 Color: 12

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 471230 Color: 0
Size: 276947 Color: 1
Size: 251824 Color: 12

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 471408 Color: 9
Size: 275244 Color: 13
Size: 253349 Color: 5

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 471322 Color: 14
Size: 264526 Color: 1
Size: 264153 Color: 4

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 471331 Color: 13
Size: 267818 Color: 17
Size: 260852 Color: 15

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 471453 Color: 9
Size: 272427 Color: 16
Size: 256121 Color: 6

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 471390 Color: 18
Size: 278597 Color: 15
Size: 250014 Color: 15

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 471415 Color: 7
Size: 270689 Color: 14
Size: 257897 Color: 17

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 471425 Color: 8
Size: 274016 Color: 10
Size: 254560 Color: 19

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 471470 Color: 15
Size: 270188 Color: 18
Size: 258343 Color: 5

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 471489 Color: 8
Size: 268423 Color: 17
Size: 260089 Color: 9

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 471620 Color: 5
Size: 268114 Color: 4
Size: 260267 Color: 17

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 471652 Color: 17
Size: 274505 Color: 4
Size: 253844 Color: 14

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 471771 Color: 1
Size: 267041 Color: 11
Size: 261189 Color: 8

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 471774 Color: 2
Size: 266821 Color: 0
Size: 261406 Color: 13

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 471822 Color: 10
Size: 271817 Color: 7
Size: 256362 Color: 13

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 471887 Color: 15
Size: 267322 Color: 9
Size: 260792 Color: 3

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 471916 Color: 14
Size: 265714 Color: 15
Size: 262371 Color: 13

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 471917 Color: 0
Size: 267610 Color: 2
Size: 260474 Color: 2

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 471935 Color: 19
Size: 267055 Color: 0
Size: 261011 Color: 15

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 471983 Color: 0
Size: 275200 Color: 6
Size: 252818 Color: 5

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 471985 Color: 18
Size: 271358 Color: 6
Size: 256658 Color: 5

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 472113 Color: 19
Size: 276738 Color: 7
Size: 251150 Color: 13

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 472122 Color: 7
Size: 277162 Color: 14
Size: 250717 Color: 12

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 472334 Color: 0
Size: 276074 Color: 6
Size: 251593 Color: 9

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 472449 Color: 5
Size: 269951 Color: 7
Size: 257601 Color: 3

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 472456 Color: 0
Size: 276701 Color: 19
Size: 250844 Color: 1

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 472594 Color: 5
Size: 276310 Color: 19
Size: 251097 Color: 17

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 472631 Color: 3
Size: 267926 Color: 6
Size: 259444 Color: 5

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 472731 Color: 16
Size: 271416 Color: 2
Size: 255854 Color: 19

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 472929 Color: 9
Size: 272687 Color: 15
Size: 254385 Color: 4

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 472870 Color: 13
Size: 272270 Color: 5
Size: 254861 Color: 18

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 472920 Color: 12
Size: 271205 Color: 13
Size: 255876 Color: 14

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 472988 Color: 8
Size: 265945 Color: 11
Size: 261068 Color: 9

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 473016 Color: 16
Size: 275600 Color: 10
Size: 251385 Color: 12

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 473044 Color: 17
Size: 268971 Color: 11
Size: 257986 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 473148 Color: 17
Size: 276225 Color: 4
Size: 250628 Color: 2

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 473233 Color: 16
Size: 270816 Color: 12
Size: 255952 Color: 6

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 473241 Color: 15
Size: 273864 Color: 0
Size: 252896 Color: 5

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 473367 Color: 9
Size: 271448 Color: 7
Size: 255186 Color: 10

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 473378 Color: 7
Size: 265952 Color: 19
Size: 260671 Color: 6

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 473535 Color: 3
Size: 263377 Color: 11
Size: 263089 Color: 3

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 473559 Color: 6
Size: 264579 Color: 18
Size: 261863 Color: 5

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 473785 Color: 16
Size: 273740 Color: 11
Size: 252476 Color: 5

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 473816 Color: 19
Size: 272672 Color: 14
Size: 253513 Color: 19

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 473828 Color: 11
Size: 271253 Color: 4
Size: 254920 Color: 9

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 473828 Color: 11
Size: 269219 Color: 12
Size: 256954 Color: 16

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 473840 Color: 2
Size: 271498 Color: 2
Size: 254663 Color: 13

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 474002 Color: 10
Size: 272245 Color: 16
Size: 253754 Color: 14

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 474061 Color: 15
Size: 275737 Color: 12
Size: 250203 Color: 10

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 474328 Color: 10
Size: 267337 Color: 14
Size: 258336 Color: 19

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 474420 Color: 1
Size: 271455 Color: 2
Size: 254126 Color: 16

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 474430 Color: 7
Size: 271470 Color: 9
Size: 254101 Color: 19

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 474511 Color: 4
Size: 270799 Color: 14
Size: 254691 Color: 18

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 474516 Color: 6
Size: 272954 Color: 4
Size: 252531 Color: 3

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 474692 Color: 4
Size: 273423 Color: 2
Size: 251886 Color: 12

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 474708 Color: 18
Size: 274840 Color: 13
Size: 250453 Color: 16

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 474752 Color: 12
Size: 265534 Color: 19
Size: 259715 Color: 5

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 474783 Color: 14
Size: 263308 Color: 1
Size: 261910 Color: 9

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 474910 Color: 17
Size: 274618 Color: 10
Size: 250473 Color: 7

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 475316 Color: 7
Size: 263136 Color: 8
Size: 261549 Color: 15

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 475372 Color: 5
Size: 272669 Color: 3
Size: 251960 Color: 7

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 475478 Color: 7
Size: 273692 Color: 1
Size: 250831 Color: 2

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 475588 Color: 12
Size: 270841 Color: 11
Size: 253572 Color: 8

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 475604 Color: 15
Size: 270712 Color: 14
Size: 253685 Color: 1

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 475679 Color: 11
Size: 269241 Color: 6
Size: 255081 Color: 6

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 475701 Color: 16
Size: 264250 Color: 7
Size: 260050 Color: 4

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 475780 Color: 2
Size: 271015 Color: 10
Size: 253206 Color: 9

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 475847 Color: 16
Size: 269830 Color: 8
Size: 254324 Color: 8

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 475863 Color: 1
Size: 270700 Color: 3
Size: 253438 Color: 7

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 475893 Color: 17
Size: 270131 Color: 14
Size: 253977 Color: 11

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 475949 Color: 8
Size: 267296 Color: 14
Size: 256756 Color: 6

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 476012 Color: 18
Size: 269068 Color: 5
Size: 254921 Color: 8

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 476013 Color: 2
Size: 267102 Color: 4
Size: 256886 Color: 17

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 476017 Color: 11
Size: 270458 Color: 17
Size: 253526 Color: 4

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 476033 Color: 2
Size: 267396 Color: 14
Size: 256572 Color: 16

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 476384 Color: 10
Size: 267163 Color: 2
Size: 256454 Color: 3

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 476405 Color: 16
Size: 267338 Color: 10
Size: 256258 Color: 10

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 476604 Color: 9
Size: 264119 Color: 18
Size: 259278 Color: 5

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 476547 Color: 14
Size: 272343 Color: 3
Size: 251111 Color: 17

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 476644 Color: 14
Size: 263795 Color: 14
Size: 259562 Color: 17

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 476724 Color: 13
Size: 270910 Color: 6
Size: 252367 Color: 3

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 476852 Color: 15
Size: 272128 Color: 5
Size: 251021 Color: 17

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 476871 Color: 13
Size: 267353 Color: 11
Size: 255777 Color: 3

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 477216 Color: 9
Size: 272432 Color: 14
Size: 250353 Color: 12

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 477112 Color: 4
Size: 262415 Color: 8
Size: 260474 Color: 13

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 477205 Color: 11
Size: 269356 Color: 12
Size: 253440 Color: 7

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 477287 Color: 8
Size: 265409 Color: 16
Size: 257305 Color: 3

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 477298 Color: 14
Size: 268836 Color: 15
Size: 253867 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 477408 Color: 9
Size: 272377 Color: 6
Size: 250216 Color: 14

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 477318 Color: 14
Size: 269627 Color: 19
Size: 253056 Color: 9

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 477344 Color: 13
Size: 265241 Color: 7
Size: 257416 Color: 11

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 477445 Color: 8
Size: 264902 Color: 12
Size: 257654 Color: 9

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 477469 Color: 18
Size: 262228 Color: 7
Size: 260304 Color: 2

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 477481 Color: 16
Size: 266586 Color: 4
Size: 255934 Color: 19

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 477491 Color: 19
Size: 268577 Color: 11
Size: 253933 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 477557 Color: 18
Size: 271783 Color: 5
Size: 250661 Color: 19

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 477561 Color: 13
Size: 265567 Color: 11
Size: 256873 Color: 19

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 477644 Color: 17
Size: 268914 Color: 14
Size: 253443 Color: 16

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 477987 Color: 18
Size: 268668 Color: 17
Size: 253346 Color: 15

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 478026 Color: 15
Size: 270070 Color: 3
Size: 251905 Color: 14

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 478206 Color: 9
Size: 270643 Color: 2
Size: 251152 Color: 2

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 478121 Color: 8
Size: 271142 Color: 0
Size: 250738 Color: 17

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 478184 Color: 8
Size: 271357 Color: 2
Size: 250460 Color: 3

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 478215 Color: 13
Size: 260934 Color: 10
Size: 260852 Color: 16

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 478222 Color: 6
Size: 266102 Color: 1
Size: 255677 Color: 9

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 478437 Color: 9
Size: 267288 Color: 2
Size: 254276 Color: 4

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 478290 Color: 19
Size: 264432 Color: 4
Size: 257279 Color: 10

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 478339 Color: 2
Size: 261491 Color: 19
Size: 260171 Color: 1

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 478343 Color: 17
Size: 268264 Color: 4
Size: 253394 Color: 19

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 478390 Color: 1
Size: 263979 Color: 16
Size: 257632 Color: 4

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 478421 Color: 17
Size: 263882 Color: 19
Size: 257698 Color: 16

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 478426 Color: 3
Size: 260877 Color: 2
Size: 260698 Color: 2

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 478605 Color: 9
Size: 267432 Color: 5
Size: 253964 Color: 14

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 478536 Color: 13
Size: 264440 Color: 4
Size: 257025 Color: 11

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 478714 Color: 12
Size: 270375 Color: 9
Size: 250912 Color: 19

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 478821 Color: 4
Size: 270352 Color: 1
Size: 250828 Color: 5

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 479048 Color: 8
Size: 268268 Color: 14
Size: 252685 Color: 5

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 479169 Color: 14
Size: 262959 Color: 0
Size: 257873 Color: 1

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 479320 Color: 16
Size: 266687 Color: 9
Size: 253994 Color: 15

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 479363 Color: 5
Size: 267359 Color: 12
Size: 253279 Color: 7

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 479546 Color: 5
Size: 264118 Color: 1
Size: 256337 Color: 2

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 479599 Color: 3
Size: 266690 Color: 2
Size: 253712 Color: 11

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 479744 Color: 0
Size: 262349 Color: 12
Size: 257908 Color: 11

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 479791 Color: 0
Size: 264233 Color: 6
Size: 255977 Color: 6

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 479848 Color: 6
Size: 265555 Color: 9
Size: 254598 Color: 4

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 479874 Color: 4
Size: 266387 Color: 3
Size: 253740 Color: 19

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 479936 Color: 5
Size: 269592 Color: 10
Size: 250473 Color: 16

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 480042 Color: 18
Size: 265029 Color: 1
Size: 254930 Color: 13

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 480056 Color: 2
Size: 265539 Color: 3
Size: 254406 Color: 10

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 480060 Color: 15
Size: 262594 Color: 18
Size: 257347 Color: 13

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 480151 Color: 2
Size: 267374 Color: 18
Size: 252476 Color: 3

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 480252 Color: 9
Size: 267134 Color: 5
Size: 252615 Color: 2

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 480202 Color: 2
Size: 260468 Color: 15
Size: 259331 Color: 6

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 480320 Color: 2
Size: 260097 Color: 1
Size: 259584 Color: 5

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 480324 Color: 15
Size: 268737 Color: 18
Size: 250940 Color: 8

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 480337 Color: 8
Size: 260644 Color: 9
Size: 259020 Color: 10

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 480337 Color: 6
Size: 264583 Color: 13
Size: 255081 Color: 10

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 480343 Color: 14
Size: 269075 Color: 18
Size: 250583 Color: 3

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 480368 Color: 19
Size: 260724 Color: 8
Size: 258909 Color: 12

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 480469 Color: 10
Size: 261184 Color: 10
Size: 258348 Color: 12

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 480595 Color: 16
Size: 263982 Color: 13
Size: 255424 Color: 8

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 480899 Color: 17
Size: 267928 Color: 9
Size: 251174 Color: 7

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 480949 Color: 10
Size: 261586 Color: 8
Size: 257466 Color: 2

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 481037 Color: 10
Size: 268603 Color: 15
Size: 250361 Color: 3

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 481133 Color: 7
Size: 260703 Color: 1
Size: 258165 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 481158 Color: 3
Size: 268481 Color: 10
Size: 250362 Color: 18

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 481241 Color: 11
Size: 264949 Color: 0
Size: 253811 Color: 4

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 481280 Color: 3
Size: 267718 Color: 9
Size: 251003 Color: 4

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 481301 Color: 15
Size: 260125 Color: 8
Size: 258575 Color: 17

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 481334 Color: 8
Size: 261397 Color: 1
Size: 257270 Color: 18

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 481347 Color: 10
Size: 265708 Color: 13
Size: 252946 Color: 3

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 481370 Color: 5
Size: 268237 Color: 18
Size: 250394 Color: 7

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 481428 Color: 13
Size: 261572 Color: 5
Size: 257001 Color: 6

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 481531 Color: 4
Size: 264591 Color: 1
Size: 253879 Color: 9

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 481564 Color: 15
Size: 267976 Color: 19
Size: 250461 Color: 16

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 481610 Color: 18
Size: 268312 Color: 8
Size: 250079 Color: 19

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 481633 Color: 6
Size: 266656 Color: 8
Size: 251712 Color: 7

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 481634 Color: 7
Size: 265102 Color: 3
Size: 253265 Color: 14

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 482028 Color: 16
Size: 262354 Color: 3
Size: 255619 Color: 9

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 482042 Color: 12
Size: 259891 Color: 2
Size: 258068 Color: 13

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 482070 Color: 19
Size: 261505 Color: 11
Size: 256426 Color: 3

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 482164 Color: 10
Size: 259495 Color: 14
Size: 258342 Color: 8

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 482339 Color: 1
Size: 261228 Color: 2
Size: 256434 Color: 9

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 482412 Color: 5
Size: 258996 Color: 12
Size: 258593 Color: 4

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 482417 Color: 1
Size: 267169 Color: 12
Size: 250415 Color: 11

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 482494 Color: 17
Size: 260618 Color: 19
Size: 256889 Color: 14

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 482510 Color: 17
Size: 261821 Color: 5
Size: 255670 Color: 11

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 482573 Color: 9
Size: 261328 Color: 8
Size: 256100 Color: 11

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 482611 Color: 17
Size: 259818 Color: 15
Size: 257572 Color: 18

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 482622 Color: 0
Size: 262023 Color: 8
Size: 255356 Color: 6

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 482637 Color: 10
Size: 265033 Color: 14
Size: 252331 Color: 19

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 482665 Color: 0
Size: 260044 Color: 8
Size: 257292 Color: 15

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 482742 Color: 11
Size: 262522 Color: 8
Size: 254737 Color: 17

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 482892 Color: 2
Size: 258592 Color: 9
Size: 258517 Color: 4

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 483022 Color: 19
Size: 261255 Color: 10
Size: 255724 Color: 15

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 483067 Color: 15
Size: 258472 Color: 8
Size: 258462 Color: 19

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 483103 Color: 19
Size: 260485 Color: 18
Size: 256413 Color: 17

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 483109 Color: 19
Size: 259100 Color: 1
Size: 257792 Color: 5

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 483238 Color: 13
Size: 258906 Color: 1
Size: 257857 Color: 14

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 483350 Color: 7
Size: 261524 Color: 12
Size: 255127 Color: 8

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 483403 Color: 19
Size: 265890 Color: 14
Size: 250708 Color: 18

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 483574 Color: 8
Size: 262528 Color: 9
Size: 253899 Color: 12

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 483896 Color: 15
Size: 259302 Color: 0
Size: 256803 Color: 12

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 483933 Color: 8
Size: 265511 Color: 11
Size: 250557 Color: 14

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 484139 Color: 2
Size: 260139 Color: 17
Size: 255723 Color: 9

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 484315 Color: 6
Size: 261928 Color: 0
Size: 253758 Color: 1

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 484496 Color: 14
Size: 259205 Color: 4
Size: 256300 Color: 6

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 484519 Color: 19
Size: 265471 Color: 19
Size: 250011 Color: 14

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 484554 Color: 17
Size: 265131 Color: 12
Size: 250316 Color: 7

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 484561 Color: 16
Size: 258872 Color: 19
Size: 256568 Color: 14

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 484648 Color: 9
Size: 260536 Color: 17
Size: 254817 Color: 16

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 484611 Color: 16
Size: 262811 Color: 3
Size: 252579 Color: 10

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 484662 Color: 1
Size: 262208 Color: 6
Size: 253131 Color: 4

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 484713 Color: 16
Size: 258486 Color: 2
Size: 256802 Color: 2

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 484714 Color: 18
Size: 260868 Color: 17
Size: 254419 Color: 0

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 484744 Color: 15
Size: 261994 Color: 19
Size: 253263 Color: 16

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 484787 Color: 15
Size: 263941 Color: 9
Size: 251273 Color: 15

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 484803 Color: 2
Size: 264644 Color: 3
Size: 250554 Color: 15

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 484863 Color: 11
Size: 258518 Color: 2
Size: 256620 Color: 17

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 484864 Color: 12
Size: 261819 Color: 17
Size: 253318 Color: 8

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 485349 Color: 1
Size: 258830 Color: 2
Size: 255822 Color: 5

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 485370 Color: 5
Size: 263392 Color: 0
Size: 251239 Color: 4

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 485372 Color: 12
Size: 258138 Color: 12
Size: 256491 Color: 19

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 485451 Color: 9
Size: 260229 Color: 10
Size: 254321 Color: 19

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 485404 Color: 3
Size: 263913 Color: 5
Size: 250684 Color: 13

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 485408 Color: 8
Size: 261076 Color: 18
Size: 253517 Color: 5

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 485501 Color: 3
Size: 263691 Color: 14
Size: 250809 Color: 18

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 485557 Color: 17
Size: 260716 Color: 0
Size: 253728 Color: 15

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 485591 Color: 16
Size: 263592 Color: 8
Size: 250818 Color: 19

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 485664 Color: 13
Size: 262896 Color: 17
Size: 251441 Color: 7

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 485708 Color: 1
Size: 261704 Color: 5
Size: 252589 Color: 3

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 485762 Color: 14
Size: 259968 Color: 4
Size: 254271 Color: 1

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 486004 Color: 15
Size: 263183 Color: 18
Size: 250814 Color: 0

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 486054 Color: 19
Size: 262732 Color: 18
Size: 251215 Color: 5

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 486221 Color: 14
Size: 257396 Color: 5
Size: 256384 Color: 0

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 486293 Color: 14
Size: 257093 Color: 6
Size: 256615 Color: 16

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 486342 Color: 1
Size: 257584 Color: 1
Size: 256075 Color: 8

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 486362 Color: 0
Size: 262678 Color: 5
Size: 250961 Color: 4

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 486504 Color: 15
Size: 260062 Color: 11
Size: 253435 Color: 6

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 486595 Color: 9
Size: 258795 Color: 10
Size: 254611 Color: 15

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 487089 Color: 9
Size: 256654 Color: 3
Size: 256258 Color: 14

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 487073 Color: 5
Size: 262702 Color: 18
Size: 250226 Color: 12

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 487114 Color: 1
Size: 257455 Color: 2
Size: 255432 Color: 2

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 487162 Color: 11
Size: 262813 Color: 10
Size: 250026 Color: 18

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 487187 Color: 6
Size: 257029 Color: 12
Size: 255785 Color: 7

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 487193 Color: 19
Size: 260156 Color: 2
Size: 252652 Color: 12

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 487217 Color: 15
Size: 259605 Color: 18
Size: 253179 Color: 4

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 487229 Color: 8
Size: 262476 Color: 12
Size: 250296 Color: 16

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 487318 Color: 15
Size: 259796 Color: 9
Size: 252887 Color: 10

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 487323 Color: 3
Size: 261585 Color: 13
Size: 251093 Color: 5

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 487390 Color: 18
Size: 258433 Color: 18
Size: 254178 Color: 4

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 487420 Color: 16
Size: 257769 Color: 4
Size: 254812 Color: 8

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 487487 Color: 16
Size: 258424 Color: 11
Size: 254090 Color: 16

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 487635 Color: 6
Size: 256873 Color: 14
Size: 255493 Color: 3

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 487744 Color: 12
Size: 256580 Color: 5
Size: 255677 Color: 14

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 487852 Color: 11
Size: 261186 Color: 3
Size: 250963 Color: 17

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 487874 Color: 11
Size: 256548 Color: 10
Size: 255579 Color: 17

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 487896 Color: 6
Size: 256451 Color: 0
Size: 255654 Color: 11

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 487970 Color: 14
Size: 261541 Color: 12
Size: 250490 Color: 16

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 488011 Color: 5
Size: 258249 Color: 14
Size: 253741 Color: 19

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 488056 Color: 18
Size: 260699 Color: 2
Size: 251246 Color: 3

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 488216 Color: 9
Size: 260504 Color: 5
Size: 251281 Color: 4

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 488193 Color: 13
Size: 258637 Color: 10
Size: 253171 Color: 1

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 488238 Color: 4
Size: 261688 Color: 13
Size: 250075 Color: 15

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 488243 Color: 14
Size: 260914 Color: 4
Size: 250844 Color: 8

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 488250 Color: 4
Size: 259356 Color: 9
Size: 252395 Color: 2

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 488443 Color: 18
Size: 258502 Color: 6
Size: 253056 Color: 15

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 488481 Color: 14
Size: 256415 Color: 7
Size: 255105 Color: 18

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 488529 Color: 17
Size: 257590 Color: 7
Size: 253882 Color: 8

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 488578 Color: 4
Size: 261373 Color: 8
Size: 250050 Color: 17

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 488733 Color: 13
Size: 260279 Color: 8
Size: 250989 Color: 1

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 488782 Color: 9
Size: 259032 Color: 8
Size: 252187 Color: 7

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 488908 Color: 4
Size: 260631 Color: 2
Size: 250462 Color: 10

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 489072 Color: 5
Size: 257784 Color: 11
Size: 253145 Color: 14

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 489091 Color: 15
Size: 258693 Color: 1
Size: 252217 Color: 0

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 489143 Color: 19
Size: 259150 Color: 6
Size: 251708 Color: 7

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 489151 Color: 8
Size: 256244 Color: 4
Size: 254606 Color: 18

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 489212 Color: 1
Size: 260152 Color: 4
Size: 250637 Color: 18

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 489430 Color: 6
Size: 257496 Color: 5
Size: 253075 Color: 8

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 489455 Color: 1
Size: 256023 Color: 9
Size: 254523 Color: 2

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 489539 Color: 0
Size: 259780 Color: 8
Size: 250682 Color: 14

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 489570 Color: 8
Size: 259726 Color: 18
Size: 250705 Color: 7

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 489591 Color: 10
Size: 256838 Color: 19
Size: 253572 Color: 11

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 489655 Color: 5
Size: 259046 Color: 17
Size: 251300 Color: 11

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 489711 Color: 14
Size: 257892 Color: 15
Size: 252398 Color: 4

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 489743 Color: 4
Size: 257678 Color: 19
Size: 252580 Color: 8

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 489775 Color: 1
Size: 255223 Color: 3
Size: 255003 Color: 8

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 489853 Color: 6
Size: 257559 Color: 7
Size: 252589 Color: 14

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 489914 Color: 8
Size: 255748 Color: 1
Size: 254339 Color: 15

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 490107 Color: 19
Size: 255809 Color: 0
Size: 254085 Color: 17

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 490114 Color: 2
Size: 259054 Color: 7
Size: 250833 Color: 6

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 490172 Color: 13
Size: 259556 Color: 8
Size: 250273 Color: 13

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 490233 Color: 12
Size: 256961 Color: 11
Size: 252807 Color: 4

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 490254 Color: 3
Size: 257568 Color: 11
Size: 252179 Color: 4

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 490289 Color: 5
Size: 254871 Color: 18
Size: 254841 Color: 4

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 490471 Color: 5
Size: 259327 Color: 12
Size: 250203 Color: 6

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 490662 Color: 7
Size: 257633 Color: 15
Size: 251706 Color: 8

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 490730 Color: 5
Size: 258803 Color: 18
Size: 250468 Color: 7

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 490992 Color: 15
Size: 256436 Color: 8
Size: 252573 Color: 3

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 491052 Color: 15
Size: 254745 Color: 17
Size: 254204 Color: 18

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 491068 Color: 17
Size: 255951 Color: 17
Size: 252982 Color: 1

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 491071 Color: 4
Size: 257180 Color: 15
Size: 251750 Color: 13

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 491206 Color: 19
Size: 255079 Color: 2
Size: 253716 Color: 11

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 491429 Color: 8
Size: 258426 Color: 0
Size: 250146 Color: 15

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 491507 Color: 16
Size: 255707 Color: 10
Size: 252787 Color: 8

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 491612 Color: 9
Size: 254625 Color: 18
Size: 253764 Color: 19

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 491761 Color: 11
Size: 255137 Color: 11
Size: 253103 Color: 4

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 491855 Color: 4
Size: 256653 Color: 15
Size: 251493 Color: 6

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 491937 Color: 15
Size: 256505 Color: 12
Size: 251559 Color: 13

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 491957 Color: 7
Size: 254084 Color: 12
Size: 253960 Color: 3

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 491993 Color: 14
Size: 257200 Color: 17
Size: 250808 Color: 14

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 492096 Color: 2
Size: 255827 Color: 19
Size: 252078 Color: 6

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 492180 Color: 0
Size: 254654 Color: 12
Size: 253167 Color: 9

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 492236 Color: 5
Size: 257015 Color: 7
Size: 250750 Color: 18

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 492359 Color: 17
Size: 255731 Color: 12
Size: 251911 Color: 4

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 492366 Color: 9
Size: 256248 Color: 17
Size: 251387 Color: 17

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 492595 Color: 18
Size: 254232 Color: 9
Size: 253174 Color: 12

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 492640 Color: 17
Size: 256703 Color: 7
Size: 250658 Color: 15

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 492664 Color: 1
Size: 254752 Color: 19
Size: 252585 Color: 13

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 492674 Color: 16
Size: 253825 Color: 2
Size: 253502 Color: 3

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 492941 Color: 6
Size: 255063 Color: 0
Size: 251997 Color: 4

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 493105 Color: 11
Size: 254888 Color: 6
Size: 252008 Color: 16

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 493151 Color: 0
Size: 254787 Color: 7
Size: 252063 Color: 3

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 493221 Color: 11
Size: 256761 Color: 9
Size: 250019 Color: 5

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 493253 Color: 8
Size: 253623 Color: 0
Size: 253125 Color: 0

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 493308 Color: 7
Size: 255089 Color: 3
Size: 251604 Color: 3

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 493398 Color: 14
Size: 255501 Color: 15
Size: 251102 Color: 0

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 493496 Color: 7
Size: 255128 Color: 15
Size: 251377 Color: 6

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 493566 Color: 7
Size: 256426 Color: 5
Size: 250009 Color: 2

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 493577 Color: 8
Size: 255033 Color: 18
Size: 251391 Color: 16

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 493647 Color: 15
Size: 256261 Color: 19
Size: 250093 Color: 18

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 493762 Color: 5
Size: 253790 Color: 1
Size: 252449 Color: 4

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 493792 Color: 8
Size: 255203 Color: 13
Size: 251006 Color: 4

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 493832 Color: 6
Size: 254760 Color: 13
Size: 251409 Color: 10

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 493881 Color: 3
Size: 253527 Color: 8
Size: 252593 Color: 6

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 493913 Color: 18
Size: 255737 Color: 15
Size: 250351 Color: 15

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 494005 Color: 5
Size: 254090 Color: 19
Size: 251906 Color: 10

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 494007 Color: 3
Size: 255853 Color: 10
Size: 250141 Color: 8

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 494026 Color: 18
Size: 255182 Color: 12
Size: 250793 Color: 11

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 494070 Color: 7
Size: 255920 Color: 2
Size: 250011 Color: 7

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 494204 Color: 0
Size: 255123 Color: 12
Size: 250674 Color: 9

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 494282 Color: 11
Size: 254944 Color: 11
Size: 250775 Color: 14

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 494376 Color: 9
Size: 254321 Color: 10
Size: 251304 Color: 13

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 494471 Color: 10
Size: 254451 Color: 13
Size: 251079 Color: 2

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 494483 Color: 1
Size: 255308 Color: 11
Size: 250210 Color: 4

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 494569 Color: 5
Size: 253547 Color: 0
Size: 251885 Color: 8

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 494606 Color: 9
Size: 254359 Color: 14
Size: 251036 Color: 5

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 494758 Color: 11
Size: 253426 Color: 6
Size: 251817 Color: 2

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 494860 Color: 19
Size: 254845 Color: 0
Size: 250296 Color: 19

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 494836 Color: 6
Size: 253038 Color: 13
Size: 252127 Color: 14

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 494945 Color: 10
Size: 253145 Color: 8
Size: 251911 Color: 18

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 495017 Color: 12
Size: 253383 Color: 13
Size: 251601 Color: 6

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 495210 Color: 12
Size: 253520 Color: 6
Size: 251271 Color: 17

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 495266 Color: 14
Size: 254470 Color: 19
Size: 250265 Color: 9

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 495325 Color: 18
Size: 254242 Color: 1
Size: 250434 Color: 1

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 495482 Color: 10
Size: 253858 Color: 1
Size: 250661 Color: 18

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 495492 Color: 1
Size: 253884 Color: 2
Size: 250625 Color: 11

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 495558 Color: 11
Size: 253719 Color: 14
Size: 250724 Color: 4

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 495833 Color: 18
Size: 252931 Color: 1
Size: 251237 Color: 19

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 495861 Color: 1
Size: 253950 Color: 10
Size: 250190 Color: 13

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 495869 Color: 13
Size: 253278 Color: 7
Size: 250854 Color: 16

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 495967 Color: 6
Size: 253659 Color: 15
Size: 250375 Color: 15

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 496144 Color: 11
Size: 252785 Color: 18
Size: 251072 Color: 14

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 496296 Color: 17
Size: 253203 Color: 10
Size: 250502 Color: 4

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 496309 Color: 11
Size: 252338 Color: 10
Size: 251354 Color: 19

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 496334 Color: 1
Size: 252738 Color: 14
Size: 250929 Color: 2

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 496340 Color: 14
Size: 252591 Color: 2
Size: 251070 Color: 7

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 496437 Color: 14
Size: 253241 Color: 12
Size: 250323 Color: 16

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 496448 Color: 10
Size: 253056 Color: 17
Size: 250497 Color: 0

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 496566 Color: 5
Size: 251965 Color: 19
Size: 251470 Color: 3

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 496572 Color: 1
Size: 252910 Color: 14
Size: 250519 Color: 18

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 496749 Color: 12
Size: 253052 Color: 16
Size: 250200 Color: 14

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 496752 Color: 8
Size: 252388 Color: 7
Size: 250861 Color: 2

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 496785 Color: 17
Size: 253054 Color: 10
Size: 250162 Color: 5

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 496848 Color: 6
Size: 251767 Color: 15
Size: 251386 Color: 13

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 496908 Color: 14
Size: 252257 Color: 10
Size: 250836 Color: 18

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 496956 Color: 12
Size: 252103 Color: 19
Size: 250942 Color: 0

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 497072 Color: 4
Size: 252224 Color: 4
Size: 250705 Color: 15

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 497091 Color: 4
Size: 251609 Color: 15
Size: 251301 Color: 1

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 497106 Color: 6
Size: 251748 Color: 8
Size: 251147 Color: 16

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 497258 Color: 14
Size: 252479 Color: 18
Size: 250264 Color: 11

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 497373 Color: 2
Size: 251440 Color: 1
Size: 251188 Color: 13

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 497502 Color: 11
Size: 252334 Color: 3
Size: 250165 Color: 9

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 497756 Color: 18
Size: 251739 Color: 9
Size: 250506 Color: 14

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 497756 Color: 12
Size: 251164 Color: 17
Size: 251081 Color: 8

Bin 3146: 0 of cap free
Amount of items: 3
Items: 
Size: 497816 Color: 7
Size: 251669 Color: 19
Size: 250516 Color: 5

Bin 3147: 0 of cap free
Amount of items: 3
Items: 
Size: 497900 Color: 6
Size: 251238 Color: 18
Size: 250863 Color: 12

Bin 3148: 0 of cap free
Amount of items: 3
Items: 
Size: 498143 Color: 3
Size: 251520 Color: 2
Size: 250338 Color: 5

Bin 3149: 0 of cap free
Amount of items: 3
Items: 
Size: 498168 Color: 12
Size: 251421 Color: 14
Size: 250412 Color: 11

Bin 3150: 0 of cap free
Amount of items: 3
Items: 
Size: 498277 Color: 15
Size: 251111 Color: 11
Size: 250613 Color: 7

Bin 3151: 0 of cap free
Amount of items: 3
Items: 
Size: 498341 Color: 12
Size: 251561 Color: 7
Size: 250099 Color: 2

Bin 3152: 0 of cap free
Amount of items: 3
Items: 
Size: 498357 Color: 14
Size: 250847 Color: 13
Size: 250797 Color: 19

Bin 3153: 0 of cap free
Amount of items: 3
Items: 
Size: 498454 Color: 8
Size: 250935 Color: 0
Size: 250612 Color: 11

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 498483 Color: 15
Size: 250953 Color: 0
Size: 250565 Color: 16

Bin 3155: 0 of cap free
Amount of items: 3
Items: 
Size: 498530 Color: 0
Size: 251112 Color: 2
Size: 250359 Color: 17

Bin 3156: 0 of cap free
Amount of items: 3
Items: 
Size: 498546 Color: 14
Size: 251008 Color: 2
Size: 250447 Color: 2

Bin 3157: 0 of cap free
Amount of items: 3
Items: 
Size: 498717 Color: 6
Size: 251279 Color: 9
Size: 250005 Color: 12

Bin 3158: 0 of cap free
Amount of items: 3
Items: 
Size: 498728 Color: 19
Size: 250767 Color: 4
Size: 250506 Color: 15

Bin 3159: 0 of cap free
Amount of items: 3
Items: 
Size: 498865 Color: 7
Size: 251100 Color: 13
Size: 250036 Color: 13

Bin 3160: 0 of cap free
Amount of items: 3
Items: 
Size: 499018 Color: 14
Size: 250693 Color: 3
Size: 250290 Color: 11

Bin 3161: 0 of cap free
Amount of items: 3
Items: 
Size: 499022 Color: 12
Size: 250907 Color: 18
Size: 250072 Color: 5

Bin 3162: 0 of cap free
Amount of items: 3
Items: 
Size: 499079 Color: 17
Size: 250465 Color: 3
Size: 250457 Color: 14

Bin 3163: 0 of cap free
Amount of items: 3
Items: 
Size: 499211 Color: 6
Size: 250554 Color: 17
Size: 250236 Color: 12

Bin 3164: 0 of cap free
Amount of items: 3
Items: 
Size: 499251 Color: 17
Size: 250640 Color: 11
Size: 250110 Color: 19

Bin 3165: 1 of cap free
Amount of items: 3
Items: 
Size: 371936 Color: 7
Size: 331248 Color: 0
Size: 296816 Color: 4

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 350374 Color: 8
Size: 327075 Color: 1
Size: 322551 Color: 7

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 344131 Color: 2
Size: 336668 Color: 15
Size: 319201 Color: 18

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 360096 Color: 0
Size: 350189 Color: 10
Size: 289715 Color: 1

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 408312 Color: 18
Size: 302258 Color: 15
Size: 289430 Color: 0

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 363248 Color: 16
Size: 360948 Color: 15
Size: 275804 Color: 13

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 348223 Color: 15
Size: 347890 Color: 5
Size: 303887 Color: 16

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 355876 Color: 16
Size: 325915 Color: 19
Size: 318209 Color: 12

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 356858 Color: 3
Size: 350013 Color: 2
Size: 293129 Color: 14

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 357292 Color: 19
Size: 340219 Color: 8
Size: 302489 Color: 12

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 360124 Color: 9
Size: 352402 Color: 15
Size: 287474 Color: 8

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 360879 Color: 5
Size: 333729 Color: 17
Size: 305392 Color: 12

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 361863 Color: 8
Size: 344961 Color: 0
Size: 293176 Color: 4

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 363437 Color: 19
Size: 325596 Color: 12
Size: 310967 Color: 11

Bin 3179: 1 of cap free
Amount of items: 3
Items: 
Size: 364184 Color: 0
Size: 351419 Color: 12
Size: 284397 Color: 5

Bin 3180: 1 of cap free
Amount of items: 3
Items: 
Size: 346184 Color: 18
Size: 341104 Color: 15
Size: 312712 Color: 0

Bin 3181: 1 of cap free
Amount of items: 3
Items: 
Size: 364298 Color: 0
Size: 357903 Color: 1
Size: 277799 Color: 16

Bin 3182: 1 of cap free
Amount of items: 3
Items: 
Size: 366003 Color: 9
Size: 363515 Color: 10
Size: 270482 Color: 0

Bin 3183: 1 of cap free
Amount of items: 3
Items: 
Size: 369334 Color: 6
Size: 358684 Color: 1
Size: 271982 Color: 2

Bin 3184: 1 of cap free
Amount of items: 3
Items: 
Size: 369812 Color: 4
Size: 336401 Color: 1
Size: 293787 Color: 3

Bin 3185: 1 of cap free
Amount of items: 3
Items: 
Size: 372067 Color: 13
Size: 349235 Color: 0
Size: 278698 Color: 6

Bin 3186: 1 of cap free
Amount of items: 3
Items: 
Size: 350699 Color: 10
Size: 340271 Color: 19
Size: 309030 Color: 14

Bin 3187: 1 of cap free
Amount of items: 3
Items: 
Size: 378203 Color: 5
Size: 316646 Color: 5
Size: 305151 Color: 9

Bin 3188: 1 of cap free
Amount of items: 3
Items: 
Size: 368687 Color: 6
Size: 338543 Color: 9
Size: 292770 Color: 19

Bin 3189: 1 of cap free
Amount of items: 3
Items: 
Size: 344708 Color: 4
Size: 335553 Color: 4
Size: 319739 Color: 0

Bin 3190: 1 of cap free
Amount of items: 3
Items: 
Size: 395186 Color: 12
Size: 333114 Color: 6
Size: 271700 Color: 15

Bin 3191: 1 of cap free
Amount of items: 3
Items: 
Size: 396099 Color: 17
Size: 331328 Color: 1
Size: 272573 Color: 0

Bin 3192: 1 of cap free
Amount of items: 3
Items: 
Size: 342488 Color: 4
Size: 331749 Color: 6
Size: 325763 Color: 9

Bin 3193: 1 of cap free
Amount of items: 3
Items: 
Size: 407200 Color: 17
Size: 331468 Color: 13
Size: 261332 Color: 6

Bin 3194: 1 of cap free
Amount of items: 3
Items: 
Size: 440558 Color: 15
Size: 301474 Color: 19
Size: 257968 Color: 5

Bin 3195: 1 of cap free
Amount of items: 3
Items: 
Size: 367288 Color: 8
Size: 356770 Color: 19
Size: 275942 Color: 12

Bin 3196: 1 of cap free
Amount of items: 3
Items: 
Size: 353451 Color: 19
Size: 352375 Color: 4
Size: 294174 Color: 11

Bin 3197: 1 of cap free
Amount of items: 3
Items: 
Size: 389791 Color: 19
Size: 359069 Color: 10
Size: 251140 Color: 3

Bin 3198: 1 of cap free
Amount of items: 3
Items: 
Size: 334940 Color: 11
Size: 333697 Color: 9
Size: 331363 Color: 19

Bin 3199: 1 of cap free
Amount of items: 3
Items: 
Size: 356481 Color: 18
Size: 352505 Color: 12
Size: 291014 Color: 8

Bin 3200: 1 of cap free
Amount of items: 3
Items: 
Size: 353398 Color: 0
Size: 331728 Color: 0
Size: 314874 Color: 18

Bin 3201: 1 of cap free
Amount of items: 3
Items: 
Size: 356826 Color: 10
Size: 339384 Color: 17
Size: 303790 Color: 0

Bin 3202: 1 of cap free
Amount of items: 3
Items: 
Size: 353039 Color: 5
Size: 347642 Color: 17
Size: 299319 Color: 10

Bin 3203: 1 of cap free
Amount of items: 3
Items: 
Size: 360740 Color: 16
Size: 356816 Color: 4
Size: 282444 Color: 3

Bin 3204: 1 of cap free
Amount of items: 3
Items: 
Size: 361805 Color: 5
Size: 357771 Color: 13
Size: 280424 Color: 19

Bin 3205: 1 of cap free
Amount of items: 3
Items: 
Size: 377812 Color: 9
Size: 357400 Color: 7
Size: 264788 Color: 8

Bin 3206: 1 of cap free
Amount of items: 3
Items: 
Size: 363663 Color: 14
Size: 351693 Color: 9
Size: 284644 Color: 9

Bin 3207: 1 of cap free
Amount of items: 3
Items: 
Size: 384937 Color: 14
Size: 342866 Color: 2
Size: 272197 Color: 2

Bin 3208: 1 of cap free
Amount of items: 3
Items: 
Size: 349879 Color: 13
Size: 343982 Color: 18
Size: 306139 Color: 15

Bin 3209: 1 of cap free
Amount of items: 3
Items: 
Size: 380185 Color: 19
Size: 363611 Color: 1
Size: 256204 Color: 6

Bin 3210: 1 of cap free
Amount of items: 3
Items: 
Size: 348930 Color: 2
Size: 339962 Color: 6
Size: 311108 Color: 1

Bin 3211: 1 of cap free
Amount of items: 3
Items: 
Size: 360357 Color: 13
Size: 326805 Color: 16
Size: 312838 Color: 5

Bin 3212: 1 of cap free
Amount of items: 3
Items: 
Size: 336308 Color: 11
Size: 336035 Color: 18
Size: 327657 Color: 1

Bin 3213: 1 of cap free
Amount of items: 3
Items: 
Size: 341740 Color: 7
Size: 341202 Color: 19
Size: 317058 Color: 5

Bin 3214: 1 of cap free
Amount of items: 3
Items: 
Size: 334459 Color: 18
Size: 333645 Color: 8
Size: 331896 Color: 17

Bin 3215: 1 of cap free
Amount of items: 3
Items: 
Size: 376285 Color: 18
Size: 367796 Color: 12
Size: 255919 Color: 5

Bin 3216: 1 of cap free
Amount of items: 3
Items: 
Size: 401447 Color: 8
Size: 301372 Color: 4
Size: 297181 Color: 4

Bin 3217: 2 of cap free
Amount of items: 3
Items: 
Size: 357687 Color: 10
Size: 323429 Color: 3
Size: 318883 Color: 9

Bin 3218: 2 of cap free
Amount of items: 3
Items: 
Size: 365564 Color: 4
Size: 354171 Color: 11
Size: 280264 Color: 9

Bin 3219: 2 of cap free
Amount of items: 3
Items: 
Size: 349859 Color: 16
Size: 337669 Color: 9
Size: 312471 Color: 3

Bin 3220: 2 of cap free
Amount of items: 3
Items: 
Size: 347623 Color: 17
Size: 336432 Color: 17
Size: 315944 Color: 14

Bin 3221: 2 of cap free
Amount of items: 3
Items: 
Size: 405269 Color: 14
Size: 343843 Color: 0
Size: 250887 Color: 13

Bin 3222: 2 of cap free
Amount of items: 3
Items: 
Size: 360433 Color: 8
Size: 320576 Color: 19
Size: 318990 Color: 10

Bin 3223: 2 of cap free
Amount of items: 3
Items: 
Size: 384742 Color: 18
Size: 355412 Color: 5
Size: 259845 Color: 4

Bin 3224: 2 of cap free
Amount of items: 3
Items: 
Size: 335894 Color: 14
Size: 334719 Color: 18
Size: 329386 Color: 11

Bin 3225: 2 of cap free
Amount of items: 3
Items: 
Size: 351475 Color: 11
Size: 337960 Color: 1
Size: 310564 Color: 16

Bin 3226: 2 of cap free
Amount of items: 3
Items: 
Size: 386494 Color: 0
Size: 351286 Color: 13
Size: 262219 Color: 0

Bin 3227: 2 of cap free
Amount of items: 3
Items: 
Size: 351688 Color: 14
Size: 350111 Color: 1
Size: 298200 Color: 19

Bin 3228: 2 of cap free
Amount of items: 3
Items: 
Size: 399379 Color: 14
Size: 345073 Color: 4
Size: 255547 Color: 15

Bin 3229: 2 of cap free
Amount of items: 3
Items: 
Size: 346463 Color: 1
Size: 335503 Color: 3
Size: 318033 Color: 12

Bin 3230: 3 of cap free
Amount of items: 3
Items: 
Size: 359452 Color: 0
Size: 352722 Color: 10
Size: 287824 Color: 19

Bin 3231: 3 of cap free
Amount of items: 3
Items: 
Size: 336658 Color: 11
Size: 333895 Color: 1
Size: 329445 Color: 0

Bin 3232: 3 of cap free
Amount of items: 3
Items: 
Size: 367687 Color: 14
Size: 365263 Color: 7
Size: 267048 Color: 13

Bin 3233: 3 of cap free
Amount of items: 3
Items: 
Size: 377841 Color: 9
Size: 367621 Color: 19
Size: 254536 Color: 13

Bin 3234: 3 of cap free
Amount of items: 3
Items: 
Size: 379479 Color: 10
Size: 359776 Color: 15
Size: 260743 Color: 10

Bin 3235: 3 of cap free
Amount of items: 3
Items: 
Size: 336907 Color: 0
Size: 335866 Color: 4
Size: 327225 Color: 0

Bin 3236: 3 of cap free
Amount of items: 3
Items: 
Size: 383350 Color: 10
Size: 347814 Color: 14
Size: 268834 Color: 5

Bin 3237: 3 of cap free
Amount of items: 3
Items: 
Size: 361169 Color: 9
Size: 360648 Color: 0
Size: 278181 Color: 15

Bin 3238: 3 of cap free
Amount of items: 3
Items: 
Size: 342549 Color: 11
Size: 338442 Color: 0
Size: 319007 Color: 7

Bin 3239: 3 of cap free
Amount of items: 3
Items: 
Size: 402072 Color: 9
Size: 342173 Color: 19
Size: 255753 Color: 19

Bin 3240: 4 of cap free
Amount of items: 3
Items: 
Size: 407822 Color: 19
Size: 303448 Color: 12
Size: 288727 Color: 2

Bin 3241: 4 of cap free
Amount of items: 3
Items: 
Size: 351073 Color: 16
Size: 342438 Color: 0
Size: 306486 Color: 19

Bin 3242: 4 of cap free
Amount of items: 3
Items: 
Size: 352040 Color: 16
Size: 328246 Color: 17
Size: 319711 Color: 12

Bin 3243: 4 of cap free
Amount of items: 3
Items: 
Size: 362355 Color: 1
Size: 336212 Color: 2
Size: 301430 Color: 18

Bin 3244: 4 of cap free
Amount of items: 3
Items: 
Size: 399572 Color: 4
Size: 345883 Color: 6
Size: 254542 Color: 0

Bin 3245: 4 of cap free
Amount of items: 3
Items: 
Size: 366308 Color: 1
Size: 350013 Color: 8
Size: 283676 Color: 9

Bin 3246: 4 of cap free
Amount of items: 3
Items: 
Size: 375256 Color: 17
Size: 357771 Color: 2
Size: 266970 Color: 11

Bin 3247: 4 of cap free
Amount of items: 3
Items: 
Size: 339369 Color: 18
Size: 335602 Color: 2
Size: 325026 Color: 14

Bin 3248: 5 of cap free
Amount of items: 3
Items: 
Size: 407425 Color: 19
Size: 340879 Color: 10
Size: 251692 Color: 4

Bin 3249: 5 of cap free
Amount of items: 3
Items: 
Size: 355726 Color: 12
Size: 329098 Color: 17
Size: 315172 Color: 0

Bin 3250: 5 of cap free
Amount of items: 3
Items: 
Size: 343284 Color: 11
Size: 342862 Color: 5
Size: 313850 Color: 15

Bin 3251: 5 of cap free
Amount of items: 3
Items: 
Size: 354037 Color: 16
Size: 325438 Color: 13
Size: 320521 Color: 13

Bin 3252: 5 of cap free
Amount of items: 3
Items: 
Size: 352630 Color: 13
Size: 335184 Color: 17
Size: 312182 Color: 11

Bin 3253: 5 of cap free
Amount of items: 3
Items: 
Size: 349299 Color: 3
Size: 346994 Color: 2
Size: 303703 Color: 0

Bin 3254: 6 of cap free
Amount of items: 3
Items: 
Size: 360885 Color: 6
Size: 343967 Color: 12
Size: 295143 Color: 4

Bin 3255: 6 of cap free
Amount of items: 3
Items: 
Size: 348475 Color: 12
Size: 335290 Color: 2
Size: 316230 Color: 4

Bin 3256: 6 of cap free
Amount of items: 3
Items: 
Size: 336369 Color: 18
Size: 333670 Color: 2
Size: 329956 Color: 8

Bin 3257: 7 of cap free
Amount of items: 3
Items: 
Size: 350265 Color: 18
Size: 334637 Color: 14
Size: 315092 Color: 6

Bin 3258: 7 of cap free
Amount of items: 3
Items: 
Size: 368147 Color: 5
Size: 336615 Color: 11
Size: 295232 Color: 15

Bin 3259: 8 of cap free
Amount of items: 3
Items: 
Size: 399662 Color: 8
Size: 347215 Color: 1
Size: 253116 Color: 11

Bin 3260: 8 of cap free
Amount of items: 3
Items: 
Size: 342107 Color: 8
Size: 335535 Color: 14
Size: 322351 Color: 7

Bin 3261: 8 of cap free
Amount of items: 3
Items: 
Size: 345823 Color: 1
Size: 342662 Color: 17
Size: 311508 Color: 7

Bin 3262: 9 of cap free
Amount of items: 3
Items: 
Size: 343045 Color: 2
Size: 328841 Color: 11
Size: 328106 Color: 9

Bin 3263: 9 of cap free
Amount of items: 3
Items: 
Size: 383238 Color: 6
Size: 362947 Color: 16
Size: 253807 Color: 18

Bin 3264: 10 of cap free
Amount of items: 3
Items: 
Size: 351445 Color: 5
Size: 328565 Color: 9
Size: 319981 Color: 1

Bin 3265: 10 of cap free
Amount of items: 3
Items: 
Size: 384368 Color: 19
Size: 350883 Color: 12
Size: 264740 Color: 8

Bin 3266: 11 of cap free
Amount of items: 3
Items: 
Size: 417156 Color: 1
Size: 319490 Color: 0
Size: 263344 Color: 0

Bin 3267: 12 of cap free
Amount of items: 3
Items: 
Size: 340340 Color: 15
Size: 333718 Color: 19
Size: 325931 Color: 15

Bin 3268: 13 of cap free
Amount of items: 3
Items: 
Size: 371275 Color: 13
Size: 352754 Color: 9
Size: 275959 Color: 10

Bin 3269: 14 of cap free
Amount of items: 3
Items: 
Size: 357023 Color: 13
Size: 328172 Color: 17
Size: 314792 Color: 18

Bin 3270: 14 of cap free
Amount of items: 3
Items: 
Size: 483446 Color: 2
Size: 264890 Color: 5
Size: 251651 Color: 9

Bin 3271: 15 of cap free
Amount of items: 3
Items: 
Size: 364319 Color: 14
Size: 360471 Color: 19
Size: 275196 Color: 3

Bin 3272: 16 of cap free
Amount of items: 3
Items: 
Size: 474980 Color: 17
Size: 272560 Color: 15
Size: 252445 Color: 9

Bin 3273: 18 of cap free
Amount of items: 3
Items: 
Size: 347641 Color: 19
Size: 346993 Color: 1
Size: 305349 Color: 4

Bin 3274: 20 of cap free
Amount of items: 3
Items: 
Size: 340207 Color: 15
Size: 340145 Color: 10
Size: 319629 Color: 17

Bin 3275: 22 of cap free
Amount of items: 3
Items: 
Size: 391893 Color: 10
Size: 353025 Color: 6
Size: 255061 Color: 2

Bin 3276: 22 of cap free
Amount of items: 3
Items: 
Size: 352489 Color: 4
Size: 351279 Color: 4
Size: 296211 Color: 16

Bin 3277: 23 of cap free
Amount of items: 3
Items: 
Size: 355746 Color: 15
Size: 351103 Color: 11
Size: 293129 Color: 11

Bin 3278: 24 of cap free
Amount of items: 3
Items: 
Size: 354626 Color: 19
Size: 353928 Color: 17
Size: 291423 Color: 3

Bin 3279: 31 of cap free
Amount of items: 3
Items: 
Size: 338039 Color: 4
Size: 337582 Color: 2
Size: 324349 Color: 5

Bin 3280: 32 of cap free
Amount of items: 2
Items: 
Size: 499987 Color: 3
Size: 499982 Color: 12

Bin 3281: 34 of cap free
Amount of items: 3
Items: 
Size: 360509 Color: 4
Size: 355514 Color: 11
Size: 283944 Color: 4

Bin 3282: 36 of cap free
Amount of items: 3
Items: 
Size: 370303 Color: 13
Size: 370072 Color: 7
Size: 259590 Color: 13

Bin 3283: 38 of cap free
Amount of items: 3
Items: 
Size: 365737 Color: 2
Size: 362187 Color: 5
Size: 272039 Color: 17

Bin 3284: 40 of cap free
Amount of items: 3
Items: 
Size: 370183 Color: 2
Size: 358151 Color: 15
Size: 271627 Color: 16

Bin 3285: 43 of cap free
Amount of items: 3
Items: 
Size: 349178 Color: 16
Size: 348768 Color: 4
Size: 302012 Color: 12

Bin 3286: 43 of cap free
Amount of items: 3
Items: 
Size: 342263 Color: 15
Size: 330174 Color: 4
Size: 327521 Color: 11

Bin 3287: 44 of cap free
Amount of items: 3
Items: 
Size: 350200 Color: 15
Size: 349383 Color: 16
Size: 300374 Color: 4

Bin 3288: 46 of cap free
Amount of items: 3
Items: 
Size: 412609 Color: 7
Size: 332259 Color: 0
Size: 255087 Color: 0

Bin 3289: 52 of cap free
Amount of items: 3
Items: 
Size: 374845 Color: 8
Size: 357066 Color: 6
Size: 268038 Color: 12

Bin 3290: 60 of cap free
Amount of items: 3
Items: 
Size: 391860 Color: 4
Size: 351475 Color: 4
Size: 256606 Color: 14

Bin 3291: 61 of cap free
Amount of items: 3
Items: 
Size: 383977 Color: 13
Size: 350744 Color: 5
Size: 265219 Color: 7

Bin 3292: 65 of cap free
Amount of items: 3
Items: 
Size: 354707 Color: 15
Size: 330765 Color: 16
Size: 314464 Color: 1

Bin 3293: 67 of cap free
Amount of items: 3
Items: 
Size: 406566 Color: 6
Size: 327327 Color: 14
Size: 266041 Color: 15

Bin 3294: 100 of cap free
Amount of items: 3
Items: 
Size: 399335 Color: 15
Size: 345769 Color: 9
Size: 254797 Color: 0

Bin 3295: 102 of cap free
Amount of items: 3
Items: 
Size: 355026 Color: 17
Size: 333261 Color: 2
Size: 311612 Color: 7

Bin 3296: 103 of cap free
Amount of items: 3
Items: 
Size: 393054 Color: 4
Size: 341855 Color: 7
Size: 264989 Color: 8

Bin 3297: 110 of cap free
Amount of items: 3
Items: 
Size: 355963 Color: 12
Size: 355623 Color: 9
Size: 288305 Color: 18

Bin 3298: 114 of cap free
Amount of items: 3
Items: 
Size: 364596 Color: 15
Size: 351821 Color: 9
Size: 283470 Color: 4

Bin 3299: 115 of cap free
Amount of items: 3
Items: 
Size: 349825 Color: 18
Size: 349583 Color: 15
Size: 300478 Color: 15

Bin 3300: 121 of cap free
Amount of items: 3
Items: 
Size: 336368 Color: 14
Size: 335843 Color: 11
Size: 327669 Color: 4

Bin 3301: 129 of cap free
Amount of items: 3
Items: 
Size: 334993 Color: 17
Size: 334117 Color: 16
Size: 330762 Color: 19

Bin 3302: 173 of cap free
Amount of items: 3
Items: 
Size: 338103 Color: 2
Size: 337480 Color: 9
Size: 324245 Color: 19

Bin 3303: 194 of cap free
Amount of items: 3
Items: 
Size: 361182 Color: 13
Size: 354991 Color: 15
Size: 283634 Color: 1

Bin 3304: 208 of cap free
Amount of items: 3
Items: 
Size: 345286 Color: 18
Size: 331751 Color: 9
Size: 322756 Color: 1

Bin 3305: 266 of cap free
Amount of items: 3
Items: 
Size: 359003 Color: 8
Size: 321007 Color: 11
Size: 319725 Color: 10

Bin 3306: 293 of cap free
Amount of items: 3
Items: 
Size: 364246 Color: 14
Size: 360615 Color: 19
Size: 274847 Color: 7

Bin 3307: 428 of cap free
Amount of items: 3
Items: 
Size: 362080 Color: 1
Size: 361746 Color: 2
Size: 275747 Color: 19

Bin 3308: 467 of cap free
Amount of items: 3
Items: 
Size: 345747 Color: 5
Size: 343653 Color: 2
Size: 310134 Color: 17

Bin 3309: 560 of cap free
Amount of items: 2
Items: 
Size: 499839 Color: 2
Size: 499602 Color: 6

Bin 3310: 651 of cap free
Amount of items: 2
Items: 
Size: 499776 Color: 2
Size: 499574 Color: 14

Bin 3311: 672 of cap free
Amount of items: 3
Items: 
Size: 338029 Color: 6
Size: 334105 Color: 0
Size: 327195 Color: 14

Bin 3312: 747 of cap free
Amount of items: 3
Items: 
Size: 469258 Color: 12
Size: 269270 Color: 11
Size: 260726 Color: 17

Bin 3313: 858 of cap free
Amount of items: 3
Items: 
Size: 358803 Color: 2
Size: 355773 Color: 14
Size: 284567 Color: 7

Bin 3314: 890 of cap free
Amount of items: 3
Items: 
Size: 366991 Color: 11
Size: 360612 Color: 17
Size: 271508 Color: 9

Bin 3315: 1161 of cap free
Amount of items: 3
Items: 
Size: 358791 Color: 15
Size: 358747 Color: 17
Size: 281302 Color: 5

Bin 3316: 1341 of cap free
Amount of items: 2
Items: 
Size: 499348 Color: 2
Size: 499312 Color: 16

Bin 3317: 2576 of cap free
Amount of items: 3
Items: 
Size: 366541 Color: 3
Size: 360343 Color: 11
Size: 270541 Color: 1

Bin 3318: 15351 of cap free
Amount of items: 3
Items: 
Size: 358596 Color: 3
Size: 358513 Color: 8
Size: 267541 Color: 2

Bin 3319: 19521 of cap free
Amount of items: 3
Items: 
Size: 358087 Color: 0
Size: 354984 Color: 6
Size: 267409 Color: 2

Bin 3320: 24417 of cap free
Amount of items: 3
Items: 
Size: 354853 Color: 2
Size: 354794 Color: 7
Size: 265937 Color: 7

Bin 3321: 26844 of cap free
Amount of items: 3
Items: 
Size: 354036 Color: 2
Size: 353827 Color: 3
Size: 265294 Color: 13

Bin 3322: 28579 of cap free
Amount of items: 3
Items: 
Size: 353826 Color: 11
Size: 353735 Color: 13
Size: 263861 Color: 10

Bin 3323: 29376 of cap free
Amount of items: 3
Items: 
Size: 353733 Color: 14
Size: 353690 Color: 0
Size: 263202 Color: 2

Bin 3324: 30537 of cap free
Amount of items: 3
Items: 
Size: 353278 Color: 2
Size: 353262 Color: 15
Size: 262924 Color: 18

Bin 3325: 41912 of cap free
Amount of items: 3
Items: 
Size: 348932 Color: 14
Size: 348876 Color: 8
Size: 260281 Color: 10

Bin 3326: 46663 of cap free
Amount of items: 3
Items: 
Size: 347810 Color: 18
Size: 347718 Color: 17
Size: 257810 Color: 8

Bin 3327: 47260 of cap free
Amount of items: 3
Items: 
Size: 347598 Color: 15
Size: 347483 Color: 8
Size: 257660 Color: 2

Bin 3328: 48656 of cap free
Amount of items: 3
Items: 
Size: 347408 Color: 13
Size: 346737 Color: 18
Size: 257200 Color: 2

Bin 3329: 50729 of cap free
Amount of items: 3
Items: 
Size: 346605 Color: 12
Size: 346594 Color: 19
Size: 256073 Color: 4

Bin 3330: 50928 of cap free
Amount of items: 3
Items: 
Size: 346542 Color: 0
Size: 346538 Color: 3
Size: 255993 Color: 7

Bin 3331: 54453 of cap free
Amount of items: 3
Items: 
Size: 345640 Color: 3
Size: 344520 Color: 17
Size: 255388 Color: 8

Bin 3332: 84460 of cap free
Amount of items: 3
Items: 
Size: 406233 Color: 2
Size: 254731 Color: 6
Size: 254577 Color: 13

Bin 3333: 148785 of cap free
Amount of items: 3
Items: 
Size: 343417 Color: 8
Size: 254150 Color: 15
Size: 253649 Color: 3

Bin 3334: 240771 of cap free
Amount of items: 3
Items: 
Size: 253352 Color: 16
Size: 253021 Color: 3
Size: 252857 Color: 0

Bin 3335: 247565 of cap free
Amount of items: 3
Items: 
Size: 251027 Color: 17
Size: 250867 Color: 10
Size: 250542 Color: 5

Bin 3336: 749636 of cap free
Amount of items: 1
Items: 
Size: 250365 Color: 2

Total size: 3334003334
Total free space: 2000002

