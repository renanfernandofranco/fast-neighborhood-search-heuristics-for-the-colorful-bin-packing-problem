Capicity Bin: 16160
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8922 Color: 0
Size: 5440 Color: 3
Size: 1798 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9148 Color: 4
Size: 6684 Color: 2
Size: 328 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9648 Color: 4
Size: 6032 Color: 0
Size: 480 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 3
Size: 5512 Color: 4
Size: 420 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11568 Color: 1
Size: 4296 Color: 3
Size: 296 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 0
Size: 3712 Color: 2
Size: 744 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 0
Size: 3248 Color: 1
Size: 632 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12272 Color: 1
Size: 3632 Color: 0
Size: 256 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 0
Size: 3314 Color: 4
Size: 364 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12604 Color: 1
Size: 3332 Color: 2
Size: 224 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 1
Size: 2936 Color: 4
Size: 384 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12900 Color: 4
Size: 2496 Color: 1
Size: 764 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12880 Color: 1
Size: 2408 Color: 4
Size: 872 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 1
Size: 2916 Color: 3
Size: 280 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 1
Size: 1768 Color: 3
Size: 1344 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 4
Size: 2516 Color: 1
Size: 540 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13169 Color: 3
Size: 2483 Color: 4
Size: 508 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13181 Color: 2
Size: 2299 Color: 1
Size: 680 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 1
Size: 2576 Color: 0
Size: 352 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 0
Size: 2416 Color: 4
Size: 476 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 0
Size: 2440 Color: 4
Size: 448 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 1
Size: 2256 Color: 3
Size: 608 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13338 Color: 3
Size: 2394 Color: 1
Size: 428 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13431 Color: 1
Size: 1891 Color: 4
Size: 838 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 1
Size: 2252 Color: 3
Size: 448 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 2
Size: 2448 Color: 1
Size: 296 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13468 Color: 1
Size: 2244 Color: 4
Size: 448 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13506 Color: 1
Size: 2554 Color: 2
Size: 100 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 0
Size: 2104 Color: 1
Size: 512 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13570 Color: 3
Size: 1862 Color: 2
Size: 728 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 1
Size: 1484 Color: 3
Size: 976 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13738 Color: 1
Size: 1680 Color: 4
Size: 742 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 1
Size: 1550 Color: 4
Size: 820 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13770 Color: 0
Size: 1922 Color: 1
Size: 468 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 2
Size: 1956 Color: 1
Size: 384 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 3
Size: 1952 Color: 2
Size: 354 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13888 Color: 1
Size: 1312 Color: 0
Size: 960 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13951 Color: 3
Size: 1657 Color: 1
Size: 552 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 2
Size: 1788 Color: 3
Size: 384 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 1
Size: 1432 Color: 3
Size: 688 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 3
Size: 1722 Color: 1
Size: 418 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 3
Size: 1072 Color: 1
Size: 1056 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 1660 Color: 2
Size: 440 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14096 Color: 1
Size: 1360 Color: 3
Size: 704 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 3
Size: 1534 Color: 0
Size: 528 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 1
Size: 1364 Color: 2
Size: 672 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1720 Color: 2
Size: 272 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14160 Color: 3
Size: 1344 Color: 3
Size: 656 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14173 Color: 1
Size: 1603 Color: 3
Size: 384 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 0
Size: 1630 Color: 1
Size: 352 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 0
Size: 1080 Color: 1
Size: 864 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14233 Color: 3
Size: 1799 Color: 3
Size: 128 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 1
Size: 1090 Color: 2
Size: 800 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 2
Size: 1840 Color: 1
Size: 28 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 1
Size: 1482 Color: 3
Size: 376 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14446 Color: 1
Size: 1384 Color: 0
Size: 330 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 1
Size: 1428 Color: 3
Size: 284 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14504 Color: 1
Size: 1000 Color: 4
Size: 656 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 1
Size: 1374 Color: 0
Size: 272 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 0
Size: 1372 Color: 2
Size: 336 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 1
Size: 1312 Color: 4
Size: 324 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 0
Size: 1164 Color: 3
Size: 480 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14544 Color: 2
Size: 1328 Color: 0
Size: 288 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 9167 Color: 0
Size: 6664 Color: 4
Size: 328 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11708 Color: 4
Size: 4147 Color: 2
Size: 304 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12209 Color: 0
Size: 3240 Color: 1
Size: 710 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 4
Size: 2731 Color: 1
Size: 760 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13098 Color: 3
Size: 1733 Color: 1
Size: 1328 Color: 2

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13165 Color: 0
Size: 2162 Color: 0
Size: 832 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 1
Size: 2412 Color: 4
Size: 344 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 1
Size: 1564 Color: 0
Size: 948 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13651 Color: 0
Size: 1756 Color: 3
Size: 752 Color: 2

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 2
Size: 2071 Color: 1
Size: 356 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13812 Color: 4
Size: 2095 Color: 1
Size: 252 Color: 3

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13893 Color: 0
Size: 1812 Color: 2
Size: 454 Color: 1

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 14033 Color: 1
Size: 1842 Color: 0
Size: 284 Color: 3

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 0
Size: 5748 Color: 3
Size: 736 Color: 1

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 4
Size: 4962 Color: 1
Size: 984 Color: 2

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 10250 Color: 1
Size: 5450 Color: 4
Size: 458 Color: 0

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 10435 Color: 3
Size: 5363 Color: 4
Size: 360 Color: 1

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11125 Color: 1
Size: 4745 Color: 0
Size: 288 Color: 0

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 11566 Color: 0
Size: 4248 Color: 2
Size: 344 Color: 1

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11774 Color: 2
Size: 4112 Color: 0
Size: 272 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12062 Color: 4
Size: 3756 Color: 4
Size: 340 Color: 1

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12541 Color: 1
Size: 3017 Color: 0
Size: 600 Color: 2

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 14382 Color: 0
Size: 1776 Color: 3

Bin 87: 3 of cap free
Amount of items: 5
Items: 
Size: 8092 Color: 2
Size: 3665 Color: 3
Size: 3658 Color: 1
Size: 396 Color: 2
Size: 346 Color: 0

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 11596 Color: 0
Size: 4193 Color: 0
Size: 368 Color: 4

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 11731 Color: 0
Size: 3768 Color: 4
Size: 658 Color: 1

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 12233 Color: 1
Size: 3352 Color: 3
Size: 572 Color: 2

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 1
Size: 3293 Color: 4
Size: 304 Color: 3

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 12557 Color: 4
Size: 3120 Color: 1
Size: 480 Color: 2

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 4
Size: 3024 Color: 3
Size: 250 Color: 0

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 8112 Color: 2
Size: 7724 Color: 2
Size: 320 Color: 4

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 10148 Color: 2
Size: 5406 Color: 4
Size: 602 Color: 1

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 10768 Color: 1
Size: 4924 Color: 2
Size: 464 Color: 0

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 10922 Color: 4
Size: 4926 Color: 2
Size: 308 Color: 4

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 11676 Color: 4
Size: 3288 Color: 0
Size: 1192 Color: 4

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 1
Size: 3384 Color: 4
Size: 556 Color: 3

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 4
Size: 1672 Color: 1
Size: 1654 Color: 0

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 13204 Color: 3
Size: 2952 Color: 0

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 13488 Color: 3
Size: 2668 Color: 2

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 0
Size: 1700 Color: 2

Bin 104: 5 of cap free
Amount of items: 11
Items: 
Size: 8081 Color: 3
Size: 984 Color: 4
Size: 984 Color: 3
Size: 960 Color: 2
Size: 954 Color: 0
Size: 856 Color: 0
Size: 848 Color: 1
Size: 838 Color: 1
Size: 752 Color: 0
Size: 480 Color: 0
Size: 418 Color: 1

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 9609 Color: 3
Size: 6034 Color: 2
Size: 512 Color: 0

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 9725 Color: 3
Size: 5982 Color: 1
Size: 448 Color: 0

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 10694 Color: 0
Size: 5461 Color: 3

Bin 108: 5 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 1
Size: 4771 Color: 4
Size: 368 Color: 4

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 13018 Color: 4
Size: 3137 Color: 0

Bin 110: 5 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 4
Size: 2275 Color: 3

Bin 111: 6 of cap free
Amount of items: 7
Items: 
Size: 8086 Color: 0
Size: 1792 Color: 3
Size: 1792 Color: 3
Size: 1484 Color: 1
Size: 1456 Color: 3
Size: 896 Color: 1
Size: 648 Color: 0

Bin 112: 6 of cap free
Amount of items: 5
Items: 
Size: 8088 Color: 4
Size: 3268 Color: 4
Size: 2106 Color: 0
Size: 1604 Color: 2
Size: 1088 Color: 4

Bin 113: 6 of cap free
Amount of items: 3
Items: 
Size: 8944 Color: 0
Size: 6730 Color: 4
Size: 480 Color: 1

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 10467 Color: 2
Size: 5414 Color: 0
Size: 272 Color: 1

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 12186 Color: 2
Size: 3355 Color: 1
Size: 612 Color: 2

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 0
Size: 1773 Color: 2

Bin 117: 8 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 1
Size: 6734 Color: 1
Size: 432 Color: 0

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 2
Size: 5456 Color: 4
Size: 1168 Color: 0

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 11824 Color: 0
Size: 3720 Color: 2
Size: 608 Color: 1

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 12308 Color: 0
Size: 3844 Color: 2

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 4
Size: 2344 Color: 3

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 13968 Color: 3
Size: 2184 Color: 4

Bin 123: 8 of cap free
Amount of items: 2
Items: 
Size: 14006 Color: 4
Size: 2146 Color: 3

Bin 124: 9 of cap free
Amount of items: 3
Items: 
Size: 9224 Color: 4
Size: 6287 Color: 0
Size: 640 Color: 2

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 3
Size: 3003 Color: 4

Bin 126: 9 of cap free
Amount of items: 2
Items: 
Size: 14206 Color: 2
Size: 1945 Color: 4

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 9622 Color: 3
Size: 6528 Color: 2

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 10948 Color: 2
Size: 2709 Color: 1
Size: 2493 Color: 4

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 0
Size: 2214 Color: 3

Bin 130: 10 of cap free
Amount of items: 2
Items: 
Size: 14081 Color: 4
Size: 2069 Color: 3

Bin 131: 10 of cap free
Amount of items: 2
Items: 
Size: 14526 Color: 2
Size: 1624 Color: 4

Bin 132: 11 of cap free
Amount of items: 3
Items: 
Size: 11660 Color: 0
Size: 2600 Color: 1
Size: 1889 Color: 3

Bin 133: 11 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 0
Size: 1912 Color: 3

Bin 134: 12 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 4
Size: 4976 Color: 1
Size: 828 Color: 3

Bin 135: 12 of cap free
Amount of items: 3
Items: 
Size: 11036 Color: 4
Size: 4840 Color: 1
Size: 272 Color: 2

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 14154 Color: 3
Size: 1994 Color: 4

Bin 137: 13 of cap free
Amount of items: 3
Items: 
Size: 11248 Color: 4
Size: 3715 Color: 2
Size: 1184 Color: 1

Bin 138: 13 of cap free
Amount of items: 2
Items: 
Size: 13679 Color: 3
Size: 2468 Color: 0

Bin 139: 14 of cap free
Amount of items: 3
Items: 
Size: 11129 Color: 0
Size: 2768 Color: 0
Size: 2249 Color: 1

Bin 140: 14 of cap free
Amount of items: 2
Items: 
Size: 13634 Color: 4
Size: 2512 Color: 3

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 14021 Color: 0
Size: 2124 Color: 2

Bin 142: 15 of cap free
Amount of items: 2
Items: 
Size: 14093 Color: 3
Size: 2052 Color: 2

Bin 143: 16 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 2
Size: 2008 Color: 3
Size: 32 Color: 3

Bin 144: 18 of cap free
Amount of items: 3
Items: 
Size: 8148 Color: 1
Size: 6648 Color: 3
Size: 1346 Color: 1

Bin 145: 18 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 3
Size: 1722 Color: 0
Size: 32 Color: 2

Bin 146: 19 of cap free
Amount of items: 6
Items: 
Size: 8085 Color: 2
Size: 2091 Color: 4
Size: 1964 Color: 0
Size: 1841 Color: 0
Size: 1744 Color: 1
Size: 416 Color: 2

Bin 147: 20 of cap free
Amount of items: 3
Items: 
Size: 9268 Color: 3
Size: 5528 Color: 2
Size: 1344 Color: 0

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 10360 Color: 2
Size: 5780 Color: 3

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 13168 Color: 4
Size: 2972 Color: 2

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 14172 Color: 3
Size: 1968 Color: 4

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 14236 Color: 0
Size: 1904 Color: 3

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 12778 Color: 2
Size: 3360 Color: 4

Bin 153: 23 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 1
Size: 4197 Color: 2
Size: 3740 Color: 2

Bin 154: 23 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 3
Size: 2497 Color: 2

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 1
Size: 4496 Color: 2

Bin 156: 25 of cap free
Amount of items: 3
Items: 
Size: 13675 Color: 4
Size: 2408 Color: 3
Size: 52 Color: 1

Bin 157: 25 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 0
Size: 1723 Color: 4

Bin 158: 28 of cap free
Amount of items: 3
Items: 
Size: 9560 Color: 2
Size: 5724 Color: 4
Size: 848 Color: 1

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 11168 Color: 0
Size: 4964 Color: 3

Bin 160: 28 of cap free
Amount of items: 2
Items: 
Size: 13408 Color: 2
Size: 2724 Color: 0

Bin 161: 29 of cap free
Amount of items: 2
Items: 
Size: 13827 Color: 4
Size: 2304 Color: 3

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 12028 Color: 1
Size: 4102 Color: 3

Bin 163: 30 of cap free
Amount of items: 2
Items: 
Size: 13352 Color: 3
Size: 2778 Color: 4

Bin 164: 31 of cap free
Amount of items: 2
Items: 
Size: 11763 Color: 1
Size: 4366 Color: 2

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 13954 Color: 3
Size: 2170 Color: 4

Bin 166: 37 of cap free
Amount of items: 2
Items: 
Size: 12432 Color: 2
Size: 3691 Color: 3

Bin 167: 37 of cap free
Amount of items: 2
Items: 
Size: 12911 Color: 4
Size: 3212 Color: 2

Bin 168: 38 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 3
Size: 2354 Color: 2

Bin 169: 39 of cap free
Amount of items: 3
Items: 
Size: 11185 Color: 4
Size: 4276 Color: 1
Size: 660 Color: 0

Bin 170: 44 of cap free
Amount of items: 2
Items: 
Size: 13290 Color: 4
Size: 2826 Color: 2

Bin 171: 45 of cap free
Amount of items: 3
Items: 
Size: 9163 Color: 0
Size: 6696 Color: 1
Size: 256 Color: 3

Bin 172: 56 of cap free
Amount of items: 2
Items: 
Size: 10260 Color: 3
Size: 5844 Color: 2

Bin 173: 60 of cap free
Amount of items: 2
Items: 
Size: 12244 Color: 0
Size: 3856 Color: 3

Bin 174: 66 of cap free
Amount of items: 8
Items: 
Size: 8082 Color: 0
Size: 1328 Color: 3
Size: 1204 Color: 2
Size: 1152 Color: 2
Size: 1144 Color: 4
Size: 1104 Color: 4
Size: 1088 Color: 1
Size: 992 Color: 1

Bin 175: 66 of cap free
Amount of items: 2
Items: 
Size: 11238 Color: 0
Size: 4856 Color: 2

Bin 176: 69 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 3
Size: 3418 Color: 0
Size: 128 Color: 1

Bin 177: 75 of cap free
Amount of items: 2
Items: 
Size: 13463 Color: 4
Size: 2622 Color: 2

Bin 178: 84 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 0
Size: 3444 Color: 4

Bin 179: 96 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 3
Size: 2776 Color: 4

Bin 180: 104 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 1
Size: 6728 Color: 2
Size: 1160 Color: 0

Bin 181: 107 of cap free
Amount of items: 2
Items: 
Size: 10224 Color: 0
Size: 5829 Color: 4

Bin 182: 110 of cap free
Amount of items: 2
Items: 
Size: 12984 Color: 3
Size: 3066 Color: 0

Bin 183: 120 of cap free
Amount of items: 30
Items: 
Size: 744 Color: 3
Size: 738 Color: 3
Size: 736 Color: 1
Size: 732 Color: 0
Size: 640 Color: 3
Size: 640 Color: 2
Size: 608 Color: 0
Size: 584 Color: 4
Size: 576 Color: 4
Size: 576 Color: 4
Size: 576 Color: 1
Size: 546 Color: 1
Size: 544 Color: 4
Size: 544 Color: 4
Size: 536 Color: 2
Size: 520 Color: 3
Size: 498 Color: 3
Size: 498 Color: 0
Size: 496 Color: 4
Size: 496 Color: 1
Size: 488 Color: 4
Size: 478 Color: 1
Size: 464 Color: 2
Size: 448 Color: 1
Size: 414 Color: 2
Size: 412 Color: 0
Size: 388 Color: 0
Size: 384 Color: 2
Size: 384 Color: 0
Size: 352 Color: 2

Bin 184: 129 of cap free
Amount of items: 2
Items: 
Size: 9300 Color: 1
Size: 6731 Color: 3

Bin 185: 132 of cap free
Amount of items: 2
Items: 
Size: 11080 Color: 3
Size: 4948 Color: 0

Bin 186: 132 of cap free
Amount of items: 2
Items: 
Size: 12196 Color: 2
Size: 3832 Color: 3

Bin 187: 141 of cap free
Amount of items: 2
Items: 
Size: 11703 Color: 3
Size: 4316 Color: 4

Bin 188: 142 of cap free
Amount of items: 2
Items: 
Size: 10206 Color: 2
Size: 5812 Color: 3

Bin 189: 160 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 3
Size: 6728 Color: 2
Size: 1136 Color: 0

Bin 190: 160 of cap free
Amount of items: 2
Items: 
Size: 10988 Color: 2
Size: 5012 Color: 3

Bin 191: 172 of cap free
Amount of items: 2
Items: 
Size: 9252 Color: 3
Size: 6736 Color: 1

Bin 192: 204 of cap free
Amount of items: 3
Items: 
Size: 8104 Color: 1
Size: 6724 Color: 3
Size: 1128 Color: 0

Bin 193: 204 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 4
Size: 3804 Color: 3

Bin 194: 210 of cap free
Amount of items: 7
Items: 
Size: 8084 Color: 2
Size: 1460 Color: 1
Size: 1430 Color: 3
Size: 1344 Color: 3
Size: 1344 Color: 3
Size: 1328 Color: 2
Size: 960 Color: 0

Bin 195: 226 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 3
Size: 3830 Color: 4

Bin 196: 232 of cap free
Amount of items: 2
Items: 
Size: 10144 Color: 1
Size: 5784 Color: 2

Bin 197: 236 of cap free
Amount of items: 2
Items: 
Size: 11576 Color: 4
Size: 4348 Color: 3

Bin 198: 239 of cap free
Amount of items: 2
Items: 
Size: 9188 Color: 4
Size: 6733 Color: 1

Bin 199: 11192 of cap free
Amount of items: 15
Items: 
Size: 416 Color: 1
Size: 408 Color: 4
Size: 352 Color: 4
Size: 352 Color: 3
Size: 336 Color: 2
Size: 320 Color: 4
Size: 320 Color: 2
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 304 Color: 2
Size: 240 Color: 2

Total size: 3199680
Total free space: 16160

