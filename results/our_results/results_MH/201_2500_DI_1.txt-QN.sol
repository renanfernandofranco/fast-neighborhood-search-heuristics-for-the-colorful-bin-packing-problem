Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 157
Size: 780 Color: 121
Size: 40 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 162
Size: 684 Color: 117
Size: 40 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 164
Size: 550 Color: 110
Size: 108 Color: 47

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 166
Size: 498 Color: 105
Size: 120 Color: 50

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 172
Size: 272 Color: 77
Size: 258 Color: 76

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1990 Color: 178
Size: 338 Color: 86
Size: 136 Color: 55

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 181
Size: 370 Color: 89
Size: 80 Color: 34

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 183
Size: 236 Color: 71
Size: 200 Color: 65

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 189
Size: 314 Color: 83
Size: 52 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 191
Size: 308 Color: 82
Size: 48 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2127 Color: 192
Size: 281 Color: 79
Size: 56 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 196
Size: 252 Color: 74
Size: 48 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 197
Size: 172 Color: 60
Size: 120 Color: 51

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 198
Size: 282 Color: 80
Size: 2 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 199
Size: 204 Color: 67
Size: 74 Color: 29

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 200
Size: 204 Color: 68
Size: 72 Color: 28

Bin 17: 1 of cap free
Amount of items: 8
Items: 
Size: 1233 Color: 138
Size: 272 Color: 78
Size: 244 Color: 73
Size: 244 Color: 72
Size: 234 Color: 70
Size: 80 Color: 35
Size: 80 Color: 33
Size: 76 Color: 31

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 160
Size: 411 Color: 98
Size: 338 Color: 85

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 161
Size: 665 Color: 116
Size: 76 Color: 32

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 163
Size: 698 Color: 118

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1915 Color: 170
Size: 482 Color: 102
Size: 66 Color: 24

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 173
Size: 515 Color: 107
Size: 8 Color: 2

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 177
Size: 339 Color: 87
Size: 136 Color: 54

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2019 Color: 182
Size: 444 Color: 99

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2059 Color: 185
Size: 404 Color: 96

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 2099 Color: 190
Size: 364 Color: 88

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 2158 Color: 194
Size: 305 Color: 81

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2161 Color: 195
Size: 214 Color: 69
Size: 88 Color: 38

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 2210 Color: 201
Size: 253 Color: 75

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 143
Size: 1079 Color: 137

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 151
Size: 882 Color: 127
Size: 48 Color: 11

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1544 Color: 152
Size: 874 Color: 125
Size: 44 Color: 10

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1922 Color: 171
Size: 540 Color: 109

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1978 Color: 176
Size: 484 Color: 103

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2130 Color: 193
Size: 332 Color: 84

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1870 Color: 168
Size: 583 Color: 112
Size: 8 Color: 1

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1971 Color: 175
Size: 490 Color: 104

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 2090 Color: 188
Size: 371 Color: 90

Bin 39: 4 of cap free
Amount of items: 4
Items: 
Size: 1598 Color: 155
Size: 782 Color: 122
Size: 40 Color: 8
Size: 40 Color: 7

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1884 Color: 169
Size: 576 Color: 111

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 174
Size: 518 Color: 108

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 180
Size: 454 Color: 100

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 184
Size: 406 Color: 97

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 186
Size: 398 Color: 95

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 150
Size: 881 Color: 126
Size: 48 Color: 12

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 159
Size: 763 Color: 120

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 146
Size: 1049 Color: 136

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 187
Size: 390 Color: 94

Bin 49: 7 of cap free
Amount of items: 5
Items: 
Size: 1242 Color: 142
Size: 1023 Color: 133
Size: 64 Color: 23
Size: 64 Color: 22
Size: 64 Color: 21

Bin 50: 7 of cap free
Amount of items: 2
Items: 
Size: 1998 Color: 179
Size: 459 Color: 101

Bin 51: 10 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 139
Size: 384 Color: 93
Size: 382 Color: 92
Size: 378 Color: 91
Size: 76 Color: 30

Bin 52: 11 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 158
Size: 758 Color: 119
Size: 28 Color: 3

Bin 53: 12 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 156
Size: 782 Color: 123
Size: 40 Color: 6

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 153
Size: 902 Color: 129

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1847 Color: 167
Size: 604 Color: 113

Bin 56: 18 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 148
Size: 978 Color: 131
Size: 50 Color: 16

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1820 Color: 165
Size: 626 Color: 115

Bin 58: 26 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 154
Size: 840 Color: 124
Size: 40 Color: 9

Bin 59: 30 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 140
Size: 622 Color: 114
Size: 504 Color: 106
Size: 72 Color: 27

Bin 60: 30 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 145
Size: 1028 Color: 135

Bin 61: 42 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 144
Size: 1026 Color: 134

Bin 62: 42 of cap free
Amount of items: 4
Items: 
Size: 1410 Color: 147
Size: 892 Color: 128
Size: 60 Color: 20
Size: 60 Color: 19

Bin 63: 46 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 149
Size: 920 Color: 130
Size: 48 Color: 14

Bin 64: 61 of cap free
Amount of items: 4
Items: 
Size: 1237 Color: 141
Size: 1022 Color: 132
Size: 72 Color: 26
Size: 72 Color: 25

Bin 65: 92 of cap free
Amount of items: 18
Items: 
Size: 200 Color: 66
Size: 182 Color: 64
Size: 176 Color: 63
Size: 174 Color: 62
Size: 172 Color: 61
Size: 152 Color: 59
Size: 152 Color: 58
Size: 152 Color: 57
Size: 148 Color: 56
Size: 132 Color: 53
Size: 100 Color: 44
Size: 96 Color: 43
Size: 96 Color: 42
Size: 96 Color: 41
Size: 90 Color: 40
Size: 88 Color: 39
Size: 84 Color: 37
Size: 82 Color: 36

Bin 66: 1906 of cap free
Amount of items: 5
Items: 
Size: 124 Color: 52
Size: 116 Color: 49
Size: 112 Color: 48
Size: 104 Color: 46
Size: 102 Color: 45

Total size: 160160
Total free space: 2464

