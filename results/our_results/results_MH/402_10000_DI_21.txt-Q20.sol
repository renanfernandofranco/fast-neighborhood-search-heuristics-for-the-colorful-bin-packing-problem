Capicity Bin: 8360
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 18
Items: 
Size: 600 Color: 19
Size: 588 Color: 10
Size: 588 Color: 7
Size: 544 Color: 12
Size: 536 Color: 16
Size: 534 Color: 3
Size: 528 Color: 6
Size: 512 Color: 16
Size: 504 Color: 7
Size: 474 Color: 1
Size: 440 Color: 19
Size: 424 Color: 0
Size: 402 Color: 13
Size: 400 Color: 9
Size: 398 Color: 18
Size: 368 Color: 11
Size: 288 Color: 4
Size: 232 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4724 Color: 2
Size: 3300 Color: 3
Size: 336 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5187 Color: 3
Size: 2645 Color: 1
Size: 528 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 2
Size: 1858 Color: 2
Size: 424 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 15
Size: 1876 Color: 16
Size: 368 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 14
Size: 1354 Color: 7
Size: 700 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6333 Color: 1
Size: 1331 Color: 2
Size: 696 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 11
Size: 1289 Color: 10
Size: 606 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 6
Size: 1576 Color: 17
Size: 212 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 2
Size: 1581 Color: 4
Size: 204 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 1
Size: 1435 Color: 2
Size: 264 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6614 Color: 2
Size: 1274 Color: 2
Size: 472 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 17
Size: 1458 Color: 14
Size: 164 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 14
Size: 1379 Color: 13
Size: 216 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6794 Color: 10
Size: 1396 Color: 7
Size: 170 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 9
Size: 1460 Color: 14
Size: 98 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6832 Color: 3
Size: 1306 Color: 17
Size: 222 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 14
Size: 998 Color: 1
Size: 528 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6932 Color: 0
Size: 1258 Color: 0
Size: 170 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 6
Size: 1305 Color: 10
Size: 118 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 15
Size: 1070 Color: 19
Size: 328 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7070 Color: 1
Size: 1042 Color: 5
Size: 248 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 11
Size: 1196 Color: 12
Size: 184 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 16
Size: 1181 Color: 5
Size: 194 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 14
Size: 1184 Color: 17
Size: 188 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 1
Size: 910 Color: 0
Size: 372 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6998 Color: 4
Size: 858 Color: 13
Size: 504 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 4
Size: 1147 Color: 10
Size: 186 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7060 Color: 11
Size: 696 Color: 5
Size: 604 Color: 11

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7157 Color: 6
Size: 975 Color: 8
Size: 228 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 8
Size: 746 Color: 12
Size: 448 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 17
Size: 696 Color: 0
Size: 444 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 10
Size: 938 Color: 14
Size: 200 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7299 Color: 10
Size: 861 Color: 9
Size: 200 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7316 Color: 18
Size: 804 Color: 2
Size: 240 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7353 Color: 15
Size: 841 Color: 3
Size: 166 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7406 Color: 1
Size: 640 Color: 5
Size: 314 Color: 7

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7402 Color: 5
Size: 614 Color: 12
Size: 344 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7460 Color: 1
Size: 640 Color: 17
Size: 260 Color: 4

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4181 Color: 9
Size: 2516 Color: 1
Size: 1662 Color: 19

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 4673 Color: 6
Size: 3254 Color: 3
Size: 432 Color: 10

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4965 Color: 9
Size: 2942 Color: 8
Size: 452 Color: 2

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 5286 Color: 0
Size: 3073 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5885 Color: 17
Size: 2262 Color: 19
Size: 212 Color: 8

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 9
Size: 1689 Color: 16
Size: 274 Color: 11

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 2
Size: 1381 Color: 14
Size: 380 Color: 8

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6812 Color: 2
Size: 1547 Color: 15

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6945 Color: 5
Size: 1266 Color: 11
Size: 148 Color: 10

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 1
Size: 648 Color: 14
Size: 584 Color: 19

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 7369 Color: 10
Size: 862 Color: 6
Size: 128 Color: 3

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 7474 Color: 14
Size: 885 Color: 5

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 4458 Color: 1
Size: 3144 Color: 16
Size: 756 Color: 7

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 4604 Color: 19
Size: 3224 Color: 1
Size: 530 Color: 9

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 5322 Color: 3
Size: 3036 Color: 18

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5511 Color: 4
Size: 2651 Color: 5
Size: 196 Color: 10

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6228 Color: 19
Size: 2130 Color: 10

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 11
Size: 1666 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 7230 Color: 2
Size: 876 Color: 18
Size: 252 Color: 1

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 7326 Color: 5
Size: 1032 Color: 9

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 7329 Color: 2
Size: 1029 Color: 11

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5179 Color: 7
Size: 2922 Color: 15
Size: 256 Color: 11

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6175 Color: 17
Size: 2182 Color: 18

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 1
Size: 1691 Color: 17
Size: 120 Color: 6

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6505 Color: 7
Size: 1780 Color: 18
Size: 72 Color: 3

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 12
Size: 1492 Color: 4

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 7191 Color: 6
Size: 1166 Color: 10

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 4726 Color: 16
Size: 3450 Color: 12
Size: 180 Color: 1

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5052 Color: 5
Size: 2562 Color: 16
Size: 742 Color: 11

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6335 Color: 4
Size: 2021 Color: 0

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 2
Size: 1438 Color: 1
Size: 234 Color: 8

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 7110 Color: 2
Size: 1246 Color: 14

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 5821 Color: 7
Size: 2534 Color: 16

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 4
Size: 1489 Color: 13

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 7063 Color: 0
Size: 1292 Color: 6

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 5702 Color: 1
Size: 2652 Color: 8

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 6134 Color: 17
Size: 2220 Color: 6

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 7270 Color: 7
Size: 1084 Color: 3

Bin 78: 7 of cap free
Amount of items: 4
Items: 
Size: 4186 Color: 5
Size: 2375 Color: 2
Size: 1280 Color: 4
Size: 512 Color: 15

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 4713 Color: 12
Size: 3640 Color: 13

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 5551 Color: 11
Size: 1863 Color: 10
Size: 939 Color: 16

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 5
Size: 2271 Color: 3
Size: 340 Color: 5

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 6532 Color: 10
Size: 1821 Color: 18

Bin 83: 8 of cap free
Amount of items: 7
Items: 
Size: 4182 Color: 2
Size: 921 Color: 3
Size: 857 Color: 0
Size: 704 Color: 10
Size: 688 Color: 5
Size: 564 Color: 13
Size: 436 Color: 9

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 14
Size: 3030 Color: 17
Size: 468 Color: 4

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 18
Size: 2028 Color: 13
Size: 624 Color: 13

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 0
Size: 1740 Color: 17

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 7396 Color: 11
Size: 956 Color: 0

Bin 88: 9 of cap free
Amount of items: 3
Items: 
Size: 4725 Color: 12
Size: 3482 Color: 13
Size: 144 Color: 12

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 7466 Color: 8
Size: 885 Color: 5

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 7524 Color: 8
Size: 827 Color: 4

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 4496 Color: 0
Size: 3750 Color: 2
Size: 104 Color: 13

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 6
Size: 2466 Color: 4
Size: 120 Color: 1

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 6370 Color: 7
Size: 1980 Color: 10

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 7
Size: 3481 Color: 14
Size: 336 Color: 11

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 6705 Color: 6
Size: 1644 Color: 10

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 4220 Color: 1
Size: 4128 Color: 7

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 15
Size: 2164 Color: 17
Size: 252 Color: 16

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 6877 Color: 2
Size: 1470 Color: 13

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 5155 Color: 18
Size: 3191 Color: 17

Bin 100: 14 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 9
Size: 1902 Color: 7
Size: 82 Color: 9

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 7235 Color: 5
Size: 1111 Color: 0

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 7238 Color: 0
Size: 1108 Color: 16

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 5781 Color: 11
Size: 2564 Color: 9

Bin 104: 15 of cap free
Amount of items: 3
Items: 
Size: 5937 Color: 8
Size: 1606 Color: 1
Size: 802 Color: 12

Bin 105: 15 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 14
Size: 2293 Color: 12
Size: 64 Color: 12

Bin 106: 16 of cap free
Amount of items: 4
Items: 
Size: 4188 Color: 7
Size: 2764 Color: 6
Size: 1288 Color: 13
Size: 104 Color: 8

Bin 107: 16 of cap free
Amount of items: 2
Items: 
Size: 7188 Color: 9
Size: 1156 Color: 5

Bin 108: 17 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 1
Size: 2831 Color: 12
Size: 332 Color: 16

Bin 109: 17 of cap free
Amount of items: 3
Items: 
Size: 6842 Color: 8
Size: 1461 Color: 11
Size: 40 Color: 2

Bin 110: 19 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 4
Size: 1404 Color: 3
Size: 812 Color: 6

Bin 111: 21 of cap free
Amount of items: 2
Items: 
Size: 6815 Color: 6
Size: 1524 Color: 17

Bin 112: 22 of cap free
Amount of items: 2
Items: 
Size: 7388 Color: 19
Size: 950 Color: 9

Bin 113: 23 of cap free
Amount of items: 2
Items: 
Size: 7334 Color: 17
Size: 1003 Color: 0

Bin 114: 25 of cap free
Amount of items: 2
Items: 
Size: 7393 Color: 9
Size: 942 Color: 2

Bin 115: 26 of cap free
Amount of items: 2
Items: 
Size: 5993 Color: 10
Size: 2341 Color: 11

Bin 116: 27 of cap free
Amount of items: 2
Items: 
Size: 5292 Color: 4
Size: 3041 Color: 12

Bin 117: 27 of cap free
Amount of items: 2
Items: 
Size: 7255 Color: 14
Size: 1078 Color: 18

Bin 118: 28 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 3
Size: 3132 Color: 8
Size: 60 Color: 10

Bin 119: 30 of cap free
Amount of items: 2
Items: 
Size: 4830 Color: 15
Size: 3500 Color: 8

Bin 120: 30 of cap free
Amount of items: 2
Items: 
Size: 5380 Color: 11
Size: 2950 Color: 8

Bin 121: 30 of cap free
Amount of items: 2
Items: 
Size: 5646 Color: 5
Size: 2684 Color: 3

Bin 122: 32 of cap free
Amount of items: 2
Items: 
Size: 7180 Color: 8
Size: 1148 Color: 6

Bin 123: 37 of cap free
Amount of items: 2
Items: 
Size: 6609 Color: 3
Size: 1714 Color: 19

Bin 124: 39 of cap free
Amount of items: 2
Items: 
Size: 6720 Color: 3
Size: 1601 Color: 19

Bin 125: 39 of cap free
Amount of items: 2
Items: 
Size: 7333 Color: 18
Size: 988 Color: 6

Bin 126: 45 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 15
Size: 2671 Color: 12

Bin 127: 46 of cap free
Amount of items: 2
Items: 
Size: 5806 Color: 17
Size: 2508 Color: 14

Bin 128: 52 of cap free
Amount of items: 32
Items: 
Size: 392 Color: 14
Size: 364 Color: 14
Size: 352 Color: 2
Size: 352 Color: 0
Size: 336 Color: 1
Size: 320 Color: 10
Size: 308 Color: 4
Size: 304 Color: 5
Size: 296 Color: 16
Size: 296 Color: 4
Size: 292 Color: 5
Size: 290 Color: 3
Size: 288 Color: 18
Size: 272 Color: 16
Size: 272 Color: 7
Size: 268 Color: 11
Size: 268 Color: 9
Size: 256 Color: 0
Size: 232 Color: 8
Size: 224 Color: 19
Size: 224 Color: 17
Size: 216 Color: 15
Size: 208 Color: 14
Size: 208 Color: 13
Size: 192 Color: 15
Size: 192 Color: 2
Size: 188 Color: 11
Size: 184 Color: 17
Size: 184 Color: 9
Size: 182 Color: 6
Size: 176 Color: 17
Size: 172 Color: 3

Bin 129: 53 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 1
Size: 2031 Color: 12

Bin 130: 54 of cap free
Amount of items: 2
Items: 
Size: 4822 Color: 5
Size: 3484 Color: 16

Bin 131: 95 of cap free
Amount of items: 2
Items: 
Size: 4782 Color: 12
Size: 3483 Color: 8

Bin 132: 117 of cap free
Amount of items: 4
Items: 
Size: 4183 Color: 16
Size: 2181 Color: 17
Size: 1081 Color: 1
Size: 798 Color: 6

Bin 133: 6984 of cap free
Amount of items: 9
Items: 
Size: 168 Color: 11
Size: 168 Color: 8
Size: 160 Color: 18
Size: 160 Color: 5
Size: 156 Color: 9
Size: 156 Color: 0
Size: 144 Color: 10
Size: 136 Color: 3
Size: 128 Color: 19

Total size: 1103520
Total free space: 8360

