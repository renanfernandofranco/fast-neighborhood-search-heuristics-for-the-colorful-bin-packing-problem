Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 410100 Color: 0
Size: 322178 Color: 1
Size: 267723 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 345193 Color: 1
Size: 341234 Color: 0
Size: 313574 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 390236 Color: 0
Size: 333115 Color: 1
Size: 276650 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 406778 Color: 0
Size: 325751 Color: 0
Size: 267472 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 381720 Color: 0
Size: 352283 Color: 0
Size: 265998 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 415385 Color: 0
Size: 300340 Color: 0
Size: 284276 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 398819 Color: 0
Size: 318840 Color: 1
Size: 282342 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 395299 Color: 0
Size: 318889 Color: 1
Size: 285813 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 474020 Color: 1
Size: 267188 Color: 0
Size: 258793 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374681 Color: 0
Size: 365802 Color: 1
Size: 259518 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 383617 Color: 1
Size: 327368 Color: 1
Size: 289016 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 367688 Color: 1
Size: 338405 Color: 1
Size: 293908 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 442201 Color: 1
Size: 281384 Color: 0
Size: 276416 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 476046 Color: 1
Size: 270132 Color: 0
Size: 253823 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 348439 Color: 0
Size: 333648 Color: 1
Size: 317914 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 451157 Color: 0
Size: 279457 Color: 0
Size: 269387 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 354671 Color: 0
Size: 322848 Color: 1
Size: 322482 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 430985 Color: 1
Size: 290818 Color: 1
Size: 278198 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 392416 Color: 1
Size: 356805 Color: 1
Size: 250780 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 409038 Color: 0
Size: 297695 Color: 0
Size: 293268 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 396221 Color: 1
Size: 351584 Color: 1
Size: 252196 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 420679 Color: 0
Size: 302020 Color: 1
Size: 277302 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 348915 Color: 1
Size: 348455 Color: 1
Size: 302631 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 391627 Color: 1
Size: 344233 Color: 0
Size: 264141 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 420263 Color: 1
Size: 314970 Color: 0
Size: 264768 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 477688 Color: 1
Size: 269203 Color: 0
Size: 253110 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 376647 Color: 1
Size: 367263 Color: 1
Size: 256091 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 376035 Color: 0
Size: 357235 Color: 1
Size: 266731 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 373145 Color: 1
Size: 362611 Color: 1
Size: 264245 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 403708 Color: 1
Size: 331998 Color: 0
Size: 264295 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 398204 Color: 1
Size: 302228 Color: 0
Size: 299569 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 359131 Color: 1
Size: 332014 Color: 0
Size: 308856 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 374765 Color: 0
Size: 372171 Color: 1
Size: 253065 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 416324 Color: 1
Size: 325828 Color: 1
Size: 257849 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 439430 Color: 0
Size: 294570 Color: 1
Size: 266001 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 398542 Color: 1
Size: 304353 Color: 0
Size: 297106 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381393 Color: 1
Size: 337267 Color: 1
Size: 281341 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 398117 Color: 1
Size: 312350 Color: 0
Size: 289534 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 439585 Color: 1
Size: 290090 Color: 0
Size: 270326 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 438875 Color: 1
Size: 294357 Color: 0
Size: 266769 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 400153 Color: 1
Size: 318915 Color: 0
Size: 280933 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 366293 Color: 1
Size: 362853 Color: 1
Size: 270855 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 444026 Color: 0
Size: 286633 Color: 1
Size: 269342 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 365051 Color: 0
Size: 335907 Color: 0
Size: 299043 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 377371 Color: 1
Size: 330441 Color: 0
Size: 292189 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 389297 Color: 0
Size: 309932 Color: 1
Size: 300772 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 423872 Color: 0
Size: 313216 Color: 1
Size: 262913 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 427373 Color: 0
Size: 286604 Color: 0
Size: 286024 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 415849 Color: 1
Size: 324035 Color: 0
Size: 260117 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 366468 Color: 1
Size: 350199 Color: 0
Size: 283334 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 352229 Color: 1
Size: 344401 Color: 1
Size: 303371 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 450225 Color: 1
Size: 298730 Color: 0
Size: 251046 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 452642 Color: 1
Size: 274160 Color: 0
Size: 273199 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 404115 Color: 0
Size: 303283 Color: 1
Size: 292603 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 404318 Color: 0
Size: 329138 Color: 1
Size: 266545 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 415351 Color: 0
Size: 293371 Color: 0
Size: 291279 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 489481 Color: 1
Size: 258130 Color: 1
Size: 252390 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 435180 Color: 1
Size: 299278 Color: 0
Size: 265543 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 400404 Color: 0
Size: 310497 Color: 0
Size: 289100 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 353526 Color: 0
Size: 344878 Color: 0
Size: 301597 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 374308 Color: 0
Size: 330485 Color: 1
Size: 295208 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 475438 Color: 0
Size: 267439 Color: 1
Size: 257124 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 396363 Color: 0
Size: 302562 Color: 1
Size: 301076 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 411125 Color: 0
Size: 301489 Color: 0
Size: 287387 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 437098 Color: 0
Size: 306221 Color: 1
Size: 256682 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 392901 Color: 0
Size: 308977 Color: 1
Size: 298123 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 384735 Color: 1
Size: 341830 Color: 1
Size: 273436 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 403765 Color: 0
Size: 314242 Color: 1
Size: 281994 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 383290 Color: 1
Size: 357648 Color: 1
Size: 259063 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 369675 Color: 1
Size: 339132 Color: 1
Size: 291194 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 493163 Color: 1
Size: 256339 Color: 0
Size: 250499 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 369114 Color: 1
Size: 333304 Color: 1
Size: 297583 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 370748 Color: 0
Size: 350756 Color: 0
Size: 278497 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 395778 Color: 1
Size: 323800 Color: 0
Size: 280423 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 435847 Color: 0
Size: 282942 Color: 1
Size: 281212 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 410706 Color: 0
Size: 300490 Color: 0
Size: 288805 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 427537 Color: 1
Size: 306743 Color: 0
Size: 265721 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 440161 Color: 0
Size: 302536 Color: 1
Size: 257304 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 370898 Color: 0
Size: 353121 Color: 1
Size: 275982 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 389868 Color: 0
Size: 341815 Color: 1
Size: 268318 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 368471 Color: 1
Size: 364876 Color: 0
Size: 266654 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 476466 Color: 1
Size: 272083 Color: 0
Size: 251452 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 390995 Color: 0
Size: 321647 Color: 0
Size: 287359 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 383712 Color: 1
Size: 320535 Color: 0
Size: 295754 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 422884 Color: 1
Size: 319391 Color: 1
Size: 257726 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 350055 Color: 1
Size: 345971 Color: 1
Size: 303975 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 456462 Color: 0
Size: 277186 Color: 0
Size: 266353 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 391063 Color: 0
Size: 349274 Color: 1
Size: 259664 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 495077 Color: 0
Size: 252951 Color: 0
Size: 251973 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 375666 Color: 1
Size: 356520 Color: 0
Size: 267815 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 365028 Color: 1
Size: 361073 Color: 1
Size: 273900 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 429852 Color: 0
Size: 294690 Color: 0
Size: 275459 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 372032 Color: 1
Size: 330842 Color: 1
Size: 297127 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 391231 Color: 0
Size: 314829 Color: 1
Size: 293941 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 399952 Color: 0
Size: 327307 Color: 1
Size: 272742 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 382936 Color: 1
Size: 352414 Color: 0
Size: 264651 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 359466 Color: 1
Size: 321784 Color: 0
Size: 318751 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 480306 Color: 1
Size: 260233 Color: 0
Size: 259462 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 401784 Color: 1
Size: 309103 Color: 1
Size: 289114 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 397787 Color: 0
Size: 315175 Color: 0
Size: 287039 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 462251 Color: 0
Size: 275701 Color: 0
Size: 262049 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 427362 Color: 0
Size: 305253 Color: 1
Size: 267386 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 349470 Color: 0
Size: 340352 Color: 1
Size: 310179 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 445662 Color: 0
Size: 285414 Color: 1
Size: 268925 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 426293 Color: 1
Size: 310258 Color: 0
Size: 263450 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 425572 Color: 1
Size: 297108 Color: 0
Size: 277321 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 412478 Color: 1
Size: 336152 Color: 0
Size: 251371 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 377566 Color: 0
Size: 320313 Color: 0
Size: 302122 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 455407 Color: 0
Size: 286273 Color: 1
Size: 258321 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 397507 Color: 0
Size: 309854 Color: 1
Size: 292640 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 418897 Color: 0
Size: 310097 Color: 0
Size: 271007 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 499059 Color: 1
Size: 250814 Color: 0
Size: 250128 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 396488 Color: 1
Size: 350059 Color: 0
Size: 253454 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 467843 Color: 0
Size: 273672 Color: 0
Size: 258486 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 457804 Color: 0
Size: 289351 Color: 1
Size: 252846 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 444839 Color: 0
Size: 299195 Color: 0
Size: 255967 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 463973 Color: 1
Size: 285470 Color: 0
Size: 250558 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 440134 Color: 0
Size: 292232 Color: 0
Size: 267635 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 450360 Color: 0
Size: 285116 Color: 0
Size: 264525 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 488279 Color: 1
Size: 259108 Color: 0
Size: 252614 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 491693 Color: 1
Size: 256985 Color: 0
Size: 251323 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 389338 Color: 1
Size: 357501 Color: 1
Size: 253162 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 372332 Color: 0
Size: 340367 Color: 1
Size: 287302 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 460721 Color: 0
Size: 280082 Color: 0
Size: 259198 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 417820 Color: 1
Size: 328460 Color: 0
Size: 253721 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 452322 Color: 1
Size: 288502 Color: 1
Size: 259177 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 469402 Color: 0
Size: 271523 Color: 1
Size: 259076 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 460333 Color: 0
Size: 287461 Color: 1
Size: 252207 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 379963 Color: 1
Size: 317844 Color: 0
Size: 302194 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 439452 Color: 1
Size: 285339 Color: 0
Size: 275210 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 494800 Color: 1
Size: 253645 Color: 1
Size: 251556 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 472091 Color: 0
Size: 268199 Color: 1
Size: 259711 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 368138 Color: 1
Size: 328476 Color: 0
Size: 303387 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 377237 Color: 1
Size: 345522 Color: 1
Size: 277242 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 409092 Color: 1
Size: 317778 Color: 0
Size: 273131 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 397957 Color: 1
Size: 323130 Color: 1
Size: 278914 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 491438 Color: 1
Size: 257579 Color: 0
Size: 250984 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 449799 Color: 1
Size: 277801 Color: 1
Size: 272401 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 367030 Color: 1
Size: 332674 Color: 1
Size: 300297 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 389736 Color: 1
Size: 352077 Color: 1
Size: 258188 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 379855 Color: 1
Size: 319630 Color: 1
Size: 300516 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 495788 Color: 1
Size: 253760 Color: 0
Size: 250453 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 366633 Color: 1
Size: 357417 Color: 0
Size: 275951 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 344782 Color: 0
Size: 329248 Color: 0
Size: 325971 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 397442 Color: 1
Size: 312364 Color: 0
Size: 290195 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 466107 Color: 1
Size: 273039 Color: 0
Size: 260855 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 462262 Color: 0
Size: 280310 Color: 1
Size: 257429 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 379075 Color: 0
Size: 325477 Color: 1
Size: 295449 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 409287 Color: 1
Size: 320948 Color: 0
Size: 269766 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 378395 Color: 0
Size: 353798 Color: 1
Size: 267808 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 416283 Color: 1
Size: 320639 Color: 0
Size: 263079 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 365174 Color: 1
Size: 352337 Color: 0
Size: 282490 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 354817 Color: 0
Size: 337975 Color: 1
Size: 307209 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 365225 Color: 1
Size: 361852 Color: 0
Size: 272924 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 443200 Color: 1
Size: 288658 Color: 0
Size: 268143 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 406210 Color: 0
Size: 325604 Color: 1
Size: 268187 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 392567 Color: 0
Size: 346177 Color: 1
Size: 261257 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 489261 Color: 0
Size: 256815 Color: 1
Size: 253925 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 387908 Color: 1
Size: 315241 Color: 1
Size: 296852 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 346004 Color: 1
Size: 345259 Color: 0
Size: 308738 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 479251 Color: 1
Size: 264199 Color: 0
Size: 256551 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 390520 Color: 1
Size: 309471 Color: 1
Size: 300010 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 422404 Color: 0
Size: 311053 Color: 1
Size: 266544 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 365540 Color: 1
Size: 320470 Color: 0
Size: 313991 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 451733 Color: 0
Size: 294829 Color: 0
Size: 253439 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 448318 Color: 0
Size: 297629 Color: 1
Size: 254054 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 476668 Color: 1
Size: 264375 Color: 1
Size: 258958 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 371173 Color: 0
Size: 369140 Color: 1
Size: 259688 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 396266 Color: 0
Size: 326731 Color: 0
Size: 277004 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 374850 Color: 0
Size: 358566 Color: 0
Size: 266585 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 481348 Color: 0
Size: 260165 Color: 1
Size: 258488 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 463196 Color: 0
Size: 284011 Color: 1
Size: 252794 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 409483 Color: 1
Size: 332433 Color: 0
Size: 258085 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 389323 Color: 1
Size: 345330 Color: 1
Size: 265348 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 487513 Color: 0
Size: 261725 Color: 0
Size: 250763 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 414682 Color: 1
Size: 310694 Color: 1
Size: 274625 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 454328 Color: 0
Size: 284414 Color: 1
Size: 261259 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 385766 Color: 1
Size: 313185 Color: 0
Size: 301050 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 365712 Color: 0
Size: 334160 Color: 1
Size: 300129 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 379052 Color: 0
Size: 356782 Color: 1
Size: 264167 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 359823 Color: 1
Size: 326368 Color: 1
Size: 313810 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 410930 Color: 1
Size: 319075 Color: 0
Size: 269996 Color: 1

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 391426 Color: 0
Size: 355086 Color: 1
Size: 253489 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 424375 Color: 1
Size: 315862 Color: 1
Size: 259764 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 493254 Color: 1
Size: 256140 Color: 0
Size: 250607 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 383678 Color: 1
Size: 329034 Color: 1
Size: 287289 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 379733 Color: 1
Size: 317686 Color: 1
Size: 302582 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 419250 Color: 1
Size: 314433 Color: 0
Size: 266318 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 469165 Color: 1
Size: 275516 Color: 0
Size: 255320 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 337910 Color: 1
Size: 335422 Color: 0
Size: 326669 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 358747 Color: 0
Size: 323034 Color: 1
Size: 318220 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 367302 Color: 0
Size: 316514 Color: 0
Size: 316185 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 454283 Color: 1
Size: 292864 Color: 0
Size: 252854 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 468271 Color: 1
Size: 281078 Color: 1
Size: 250652 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 476336 Color: 1
Size: 263107 Color: 1
Size: 260558 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 474238 Color: 1
Size: 264763 Color: 0
Size: 261000 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 400662 Color: 1
Size: 306488 Color: 1
Size: 292851 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 430366 Color: 0
Size: 310674 Color: 1
Size: 258961 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 474027 Color: 1
Size: 265055 Color: 0
Size: 260919 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 425683 Color: 1
Size: 300255 Color: 0
Size: 274063 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 392349 Color: 1
Size: 338841 Color: 0
Size: 268811 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 372451 Color: 1
Size: 357913 Color: 0
Size: 269637 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 476479 Color: 1
Size: 262494 Color: 1
Size: 261028 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 350991 Color: 0
Size: 344367 Color: 0
Size: 304643 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 468863 Color: 1
Size: 271684 Color: 0
Size: 259454 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 461950 Color: 0
Size: 270012 Color: 1
Size: 268039 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 436360 Color: 1
Size: 299457 Color: 0
Size: 264184 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 419598 Color: 1
Size: 304658 Color: 1
Size: 275745 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 377263 Color: 1
Size: 334257 Color: 0
Size: 288481 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 445743 Color: 0
Size: 295851 Color: 0
Size: 258407 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 468918 Color: 1
Size: 267547 Color: 1
Size: 263536 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 479411 Color: 1
Size: 269761 Color: 0
Size: 250829 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 355732 Color: 0
Size: 340293 Color: 1
Size: 303976 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 418101 Color: 1
Size: 323001 Color: 0
Size: 258899 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 457029 Color: 1
Size: 286552 Color: 1
Size: 256420 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 405822 Color: 1
Size: 301921 Color: 1
Size: 292258 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 391846 Color: 1
Size: 337073 Color: 0
Size: 271082 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 390665 Color: 0
Size: 304714 Color: 1
Size: 304622 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 484611 Color: 0
Size: 260659 Color: 1
Size: 254731 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 366781 Color: 1
Size: 320387 Color: 1
Size: 312833 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 400573 Color: 1
Size: 302811 Color: 0
Size: 296617 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 356227 Color: 0
Size: 322605 Color: 1
Size: 321169 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 367325 Color: 0
Size: 351023 Color: 1
Size: 281653 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 394372 Color: 1
Size: 347654 Color: 1
Size: 257975 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 488500 Color: 0
Size: 257717 Color: 0
Size: 253784 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 442683 Color: 1
Size: 280762 Color: 0
Size: 276556 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 413145 Color: 0
Size: 295049 Color: 1
Size: 291807 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 355411 Color: 1
Size: 347546 Color: 1
Size: 297044 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 365927 Color: 0
Size: 335300 Color: 0
Size: 298774 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 431015 Color: 0
Size: 294894 Color: 0
Size: 274092 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 373862 Color: 0
Size: 369638 Color: 1
Size: 256501 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 359869 Color: 0
Size: 349625 Color: 1
Size: 290507 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 473065 Color: 1
Size: 271333 Color: 1
Size: 255603 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 401008 Color: 0
Size: 342875 Color: 0
Size: 256118 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 487636 Color: 0
Size: 258962 Color: 1
Size: 253403 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 484759 Color: 1
Size: 259278 Color: 0
Size: 255964 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 413560 Color: 1
Size: 328262 Color: 0
Size: 258179 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 383126 Color: 0
Size: 350656 Color: 1
Size: 266219 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 414135 Color: 1
Size: 294160 Color: 1
Size: 291706 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 452031 Color: 1
Size: 294209 Color: 0
Size: 253761 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 446242 Color: 0
Size: 286854 Color: 1
Size: 266905 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 454246 Color: 0
Size: 295654 Color: 1
Size: 250101 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 455398 Color: 0
Size: 290811 Color: 1
Size: 253792 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 373278 Color: 0
Size: 324680 Color: 0
Size: 302043 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 405715 Color: 0
Size: 343514 Color: 1
Size: 250772 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 472749 Color: 0
Size: 266389 Color: 0
Size: 260863 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 449600 Color: 0
Size: 280564 Color: 1
Size: 269837 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 475940 Color: 0
Size: 270054 Color: 0
Size: 254007 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 422887 Color: 0
Size: 296175 Color: 1
Size: 280939 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 368605 Color: 1
Size: 344746 Color: 0
Size: 286650 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 479881 Color: 0
Size: 263008 Color: 0
Size: 257112 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 498770 Color: 0
Size: 250997 Color: 1
Size: 250234 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 443281 Color: 0
Size: 305594 Color: 1
Size: 251126 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 435108 Color: 1
Size: 310778 Color: 0
Size: 254115 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 449812 Color: 1
Size: 281839 Color: 0
Size: 268350 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 441638 Color: 1
Size: 283532 Color: 1
Size: 274831 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 373025 Color: 0
Size: 317864 Color: 1
Size: 309112 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 409409 Color: 0
Size: 332319 Color: 1
Size: 258273 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 447017 Color: 0
Size: 279098 Color: 1
Size: 273886 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 378218 Color: 0
Size: 353016 Color: 1
Size: 268767 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 384923 Color: 0
Size: 325569 Color: 0
Size: 289509 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 436311 Color: 1
Size: 307016 Color: 0
Size: 256674 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 375571 Color: 0
Size: 312853 Color: 0
Size: 311577 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 370109 Color: 1
Size: 340476 Color: 0
Size: 289416 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 460986 Color: 0
Size: 284321 Color: 1
Size: 254694 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 404369 Color: 0
Size: 299850 Color: 1
Size: 295782 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 391885 Color: 0
Size: 340008 Color: 0
Size: 268108 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 445423 Color: 0
Size: 295961 Color: 1
Size: 258617 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 366197 Color: 0
Size: 354618 Color: 0
Size: 279186 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 455654 Color: 0
Size: 275988 Color: 1
Size: 268359 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 440003 Color: 0
Size: 297510 Color: 1
Size: 262488 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 402982 Color: 1
Size: 345978 Color: 0
Size: 251041 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 426231 Color: 1
Size: 293464 Color: 0
Size: 280306 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 482855 Color: 0
Size: 258970 Color: 0
Size: 258176 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 374555 Color: 0
Size: 343799 Color: 0
Size: 281647 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 454698 Color: 1
Size: 279945 Color: 0
Size: 265358 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 361182 Color: 0
Size: 349116 Color: 0
Size: 289703 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 357831 Color: 1
Size: 333190 Color: 0
Size: 308980 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 471900 Color: 1
Size: 273354 Color: 1
Size: 254747 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 441518 Color: 1
Size: 279858 Color: 0
Size: 278625 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 412632 Color: 1
Size: 317239 Color: 1
Size: 270130 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 384595 Color: 0
Size: 313992 Color: 0
Size: 301414 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 393209 Color: 0
Size: 355960 Color: 0
Size: 250832 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 384263 Color: 0
Size: 309849 Color: 0
Size: 305889 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 412416 Color: 1
Size: 335101 Color: 0
Size: 252484 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 465429 Color: 0
Size: 271179 Color: 0
Size: 263393 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 367843 Color: 0
Size: 317242 Color: 0
Size: 314916 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 423900 Color: 0
Size: 309652 Color: 1
Size: 266449 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 430240 Color: 0
Size: 316343 Color: 0
Size: 253418 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 376439 Color: 0
Size: 314447 Color: 1
Size: 309115 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 499275 Color: 0
Size: 250426 Color: 1
Size: 250300 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 466208 Color: 1
Size: 271136 Color: 0
Size: 262657 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 415430 Color: 0
Size: 323747 Color: 1
Size: 260824 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 427588 Color: 1
Size: 294716 Color: 0
Size: 277697 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 402126 Color: 0
Size: 306263 Color: 0
Size: 291612 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 495755 Color: 0
Size: 254060 Color: 1
Size: 250186 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 400938 Color: 1
Size: 312965 Color: 1
Size: 286098 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 440086 Color: 0
Size: 297373 Color: 1
Size: 262542 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 472136 Color: 0
Size: 265294 Color: 0
Size: 262571 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 396136 Color: 0
Size: 312845 Color: 0
Size: 291020 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 457544 Color: 1
Size: 279500 Color: 0
Size: 262957 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 453413 Color: 1
Size: 273871 Color: 1
Size: 272717 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 363594 Color: 0
Size: 326398 Color: 0
Size: 310009 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 428537 Color: 1
Size: 307295 Color: 0
Size: 264169 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 380331 Color: 0
Size: 321532 Color: 1
Size: 298138 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 416947 Color: 1
Size: 332971 Color: 1
Size: 250083 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 356575 Color: 0
Size: 329814 Color: 1
Size: 313612 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 380826 Color: 1
Size: 356276 Color: 0
Size: 262899 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 386559 Color: 0
Size: 329065 Color: 0
Size: 284377 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 366136 Color: 0
Size: 317845 Color: 1
Size: 316020 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 392338 Color: 0
Size: 313863 Color: 0
Size: 293800 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 370218 Color: 0
Size: 326968 Color: 1
Size: 302815 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 451688 Color: 0
Size: 276545 Color: 0
Size: 271768 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 446667 Color: 1
Size: 290287 Color: 0
Size: 263047 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 362600 Color: 1
Size: 321351 Color: 0
Size: 316050 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 427638 Color: 0
Size: 301013 Color: 0
Size: 271350 Color: 1

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 409849 Color: 1
Size: 333784 Color: 0
Size: 256368 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 498570 Color: 0
Size: 251397 Color: 1
Size: 250034 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 431757 Color: 1
Size: 303706 Color: 0
Size: 264538 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 399240 Color: 1
Size: 327104 Color: 1
Size: 273657 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 358760 Color: 1
Size: 321610 Color: 1
Size: 319631 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 440742 Color: 1
Size: 287752 Color: 0
Size: 271507 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 358723 Color: 1
Size: 349563 Color: 1
Size: 291715 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 381908 Color: 0
Size: 363727 Color: 1
Size: 254366 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 439822 Color: 1
Size: 293706 Color: 1
Size: 266473 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 390961 Color: 1
Size: 314211 Color: 0
Size: 294829 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 376898 Color: 0
Size: 348113 Color: 1
Size: 274990 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 472717 Color: 0
Size: 273086 Color: 0
Size: 254198 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 482889 Color: 1
Size: 262127 Color: 1
Size: 254985 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 407099 Color: 0
Size: 323618 Color: 1
Size: 269284 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 373591 Color: 0
Size: 328376 Color: 1
Size: 298034 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 440178 Color: 0
Size: 287371 Color: 1
Size: 272452 Color: 1

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 374676 Color: 1
Size: 365178 Color: 0
Size: 260147 Color: 1

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 457387 Color: 0
Size: 273686 Color: 1
Size: 268928 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 423013 Color: 0
Size: 302850 Color: 1
Size: 274138 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 375936 Color: 1
Size: 350099 Color: 0
Size: 273966 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 406649 Color: 0
Size: 312865 Color: 1
Size: 280487 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 423949 Color: 0
Size: 300053 Color: 0
Size: 275999 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 430581 Color: 0
Size: 306331 Color: 1
Size: 263089 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 433032 Color: 0
Size: 294090 Color: 0
Size: 272879 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 405249 Color: 1
Size: 300384 Color: 0
Size: 294368 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 489360 Color: 0
Size: 258669 Color: 0
Size: 251972 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 463165 Color: 1
Size: 275641 Color: 1
Size: 261195 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 388727 Color: 0
Size: 352447 Color: 1
Size: 258827 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 460174 Color: 0
Size: 275251 Color: 1
Size: 264576 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 454281 Color: 0
Size: 274713 Color: 1
Size: 271007 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 352203 Color: 1
Size: 346322 Color: 0
Size: 301476 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 482140 Color: 0
Size: 259810 Color: 0
Size: 258051 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 391279 Color: 1
Size: 309731 Color: 1
Size: 298991 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 369154 Color: 1
Size: 364575 Color: 0
Size: 266272 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 373334 Color: 0
Size: 340060 Color: 0
Size: 286607 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 335973 Color: 0
Size: 335145 Color: 1
Size: 328883 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 403996 Color: 0
Size: 312246 Color: 1
Size: 283759 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 388591 Color: 0
Size: 338295 Color: 1
Size: 273115 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 376595 Color: 0
Size: 371836 Color: 1
Size: 251570 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 392193 Color: 1
Size: 344685 Color: 1
Size: 263123 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 381686 Color: 0
Size: 365231 Color: 0
Size: 253084 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 420877 Color: 0
Size: 310810 Color: 0
Size: 268314 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 468383 Color: 0
Size: 278026 Color: 1
Size: 253592 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 391532 Color: 0
Size: 319246 Color: 0
Size: 289223 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 431153 Color: 0
Size: 317283 Color: 1
Size: 251565 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 382754 Color: 1
Size: 366450 Color: 1
Size: 250797 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 375139 Color: 1
Size: 366775 Color: 0
Size: 258087 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 445773 Color: 0
Size: 294903 Color: 0
Size: 259325 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 473160 Color: 1
Size: 274964 Color: 1
Size: 251877 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 472982 Color: 1
Size: 274880 Color: 0
Size: 252139 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 429876 Color: 1
Size: 294665 Color: 1
Size: 275460 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 463694 Color: 0
Size: 270255 Color: 1
Size: 266052 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 439989 Color: 1
Size: 307060 Color: 0
Size: 252952 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 498700 Color: 0
Size: 251070 Color: 1
Size: 250231 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 439342 Color: 0
Size: 293099 Color: 1
Size: 267560 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 365780 Color: 1
Size: 331643 Color: 0
Size: 302578 Color: 1

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 384294 Color: 1
Size: 332270 Color: 0
Size: 283437 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 454191 Color: 1
Size: 282703 Color: 0
Size: 263107 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 394676 Color: 1
Size: 342971 Color: 1
Size: 262354 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 374230 Color: 1
Size: 355826 Color: 1
Size: 269945 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 373623 Color: 1
Size: 332107 Color: 1
Size: 294271 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 390494 Color: 1
Size: 337944 Color: 1
Size: 271563 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 372038 Color: 1
Size: 357723 Color: 1
Size: 270240 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 425096 Color: 0
Size: 298692 Color: 1
Size: 276213 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 371758 Color: 1
Size: 337177 Color: 0
Size: 291066 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 377374 Color: 1
Size: 324042 Color: 0
Size: 298585 Color: 1

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 379540 Color: 0
Size: 328764 Color: 1
Size: 291697 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 403796 Color: 1
Size: 313829 Color: 0
Size: 282376 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 411820 Color: 0
Size: 327865 Color: 0
Size: 260316 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 421028 Color: 1
Size: 300081 Color: 0
Size: 278892 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 458373 Color: 1
Size: 281557 Color: 1
Size: 260071 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 472714 Color: 0
Size: 275561 Color: 1
Size: 251726 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 398516 Color: 1
Size: 301618 Color: 0
Size: 299867 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 386884 Color: 0
Size: 343365 Color: 1
Size: 269752 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 347943 Color: 0
Size: 331087 Color: 1
Size: 320971 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 495898 Color: 1
Size: 252843 Color: 0
Size: 251260 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 425532 Color: 1
Size: 307764 Color: 0
Size: 266705 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 452243 Color: 1
Size: 279867 Color: 0
Size: 267891 Color: 1

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 346008 Color: 1
Size: 343562 Color: 0
Size: 310431 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 496122 Color: 1
Size: 252886 Color: 0
Size: 250993 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 476623 Color: 0
Size: 268675 Color: 0
Size: 254703 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 406439 Color: 1
Size: 297585 Color: 0
Size: 295977 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 371512 Color: 1
Size: 349869 Color: 0
Size: 278620 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 368922 Color: 0
Size: 357133 Color: 1
Size: 273946 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 394198 Color: 0
Size: 321222 Color: 0
Size: 284581 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 405374 Color: 0
Size: 338203 Color: 1
Size: 256424 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 461745 Color: 0
Size: 284294 Color: 1
Size: 253962 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 408865 Color: 0
Size: 326953 Color: 0
Size: 264183 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 423990 Color: 1
Size: 313726 Color: 1
Size: 262285 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 368843 Color: 0
Size: 366597 Color: 0
Size: 264561 Color: 1

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 442400 Color: 0
Size: 281505 Color: 1
Size: 276096 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 358528 Color: 0
Size: 355023 Color: 1
Size: 286450 Color: 1

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 416811 Color: 0
Size: 299073 Color: 1
Size: 284117 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 389114 Color: 1
Size: 331169 Color: 0
Size: 279718 Color: 1

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 388111 Color: 0
Size: 351757 Color: 1
Size: 260133 Color: 1

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 397147 Color: 0
Size: 323834 Color: 0
Size: 279020 Color: 1

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 423678 Color: 1
Size: 289188 Color: 0
Size: 287135 Color: 1

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 379381 Color: 0
Size: 362275 Color: 1
Size: 258345 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 413848 Color: 1
Size: 302832 Color: 0
Size: 283321 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 418970 Color: 0
Size: 307266 Color: 1
Size: 273765 Color: 1

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 391942 Color: 0
Size: 308577 Color: 0
Size: 299482 Color: 1

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 398579 Color: 1
Size: 338975 Color: 1
Size: 262447 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 395250 Color: 0
Size: 354133 Color: 0
Size: 250618 Color: 1

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 402237 Color: 0
Size: 324765 Color: 0
Size: 272999 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 499648 Color: 1
Size: 250191 Color: 0
Size: 250162 Color: 1

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 380679 Color: 1
Size: 365095 Color: 0
Size: 254227 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 460232 Color: 0
Size: 275398 Color: 1
Size: 264371 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 350697 Color: 1
Size: 333748 Color: 1
Size: 315556 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 479457 Color: 1
Size: 262790 Color: 0
Size: 257754 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 371971 Color: 0
Size: 334664 Color: 1
Size: 293366 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 358688 Color: 1
Size: 355606 Color: 0
Size: 285707 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 407846 Color: 0
Size: 338613 Color: 1
Size: 253542 Color: 1

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 400643 Color: 1
Size: 334289 Color: 0
Size: 265069 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 414986 Color: 1
Size: 309768 Color: 0
Size: 275247 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 367550 Color: 1
Size: 340096 Color: 0
Size: 292355 Color: 1

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 371969 Color: 0
Size: 322411 Color: 1
Size: 305621 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 367579 Color: 0
Size: 367159 Color: 1
Size: 265263 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 384450 Color: 0
Size: 344529 Color: 1
Size: 271022 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 390448 Color: 1
Size: 343290 Color: 0
Size: 266263 Color: 1

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 381001 Color: 1
Size: 310896 Color: 1
Size: 308104 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 423951 Color: 1
Size: 306722 Color: 0
Size: 269328 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 396859 Color: 1
Size: 305806 Color: 0
Size: 297336 Color: 1

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 462355 Color: 0
Size: 276667 Color: 1
Size: 260979 Color: 1

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 470131 Color: 0
Size: 275135 Color: 1
Size: 254735 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 457175 Color: 1
Size: 282486 Color: 1
Size: 260340 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 444859 Color: 0
Size: 299734 Color: 1
Size: 255408 Color: 1

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 380835 Color: 1
Size: 331990 Color: 0
Size: 287176 Color: 1

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 453316 Color: 0
Size: 282280 Color: 0
Size: 264405 Color: 1

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 466679 Color: 1
Size: 268289 Color: 0
Size: 265033 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 415894 Color: 1
Size: 325748 Color: 0
Size: 258359 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 485955 Color: 1
Size: 263648 Color: 1
Size: 250398 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 379727 Color: 1
Size: 310606 Color: 0
Size: 309668 Color: 1

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 441111 Color: 1
Size: 294030 Color: 0
Size: 264860 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 470444 Color: 1
Size: 279511 Color: 0
Size: 250046 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 429627 Color: 0
Size: 303248 Color: 1
Size: 267126 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 343595 Color: 1
Size: 341124 Color: 0
Size: 315282 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 496157 Color: 0
Size: 253040 Color: 1
Size: 250804 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 455856 Color: 0
Size: 288170 Color: 0
Size: 255975 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 490110 Color: 1
Size: 257370 Color: 1
Size: 252521 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 370916 Color: 0
Size: 354145 Color: 1
Size: 274940 Color: 1

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 429536 Color: 0
Size: 308217 Color: 0
Size: 262248 Color: 1

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 0
Size: 321192 Color: 1
Size: 256612 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 424440 Color: 1
Size: 313091 Color: 1
Size: 262470 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 404381 Color: 1
Size: 341553 Color: 0
Size: 254067 Color: 1

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 411887 Color: 1
Size: 309821 Color: 0
Size: 278293 Color: 1

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 378147 Color: 1
Size: 342965 Color: 0
Size: 278889 Color: 1

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 360720 Color: 1
Size: 330987 Color: 0
Size: 308294 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 448199 Color: 1
Size: 300248 Color: 1
Size: 251554 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 374507 Color: 0
Size: 358945 Color: 1
Size: 266549 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 487846 Color: 1
Size: 261662 Color: 1
Size: 250493 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 360773 Color: 1
Size: 340253 Color: 1
Size: 298975 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 421654 Color: 1
Size: 311335 Color: 1
Size: 267012 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 453627 Color: 1
Size: 285798 Color: 0
Size: 260576 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 459463 Color: 1
Size: 286258 Color: 0
Size: 254280 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 452984 Color: 0
Size: 289804 Color: 0
Size: 257213 Color: 1

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 362536 Color: 1
Size: 332769 Color: 0
Size: 304696 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 411769 Color: 0
Size: 300182 Color: 1
Size: 288050 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 403267 Color: 0
Size: 329926 Color: 1
Size: 266808 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 343702 Color: 1
Size: 332363 Color: 0
Size: 323936 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 485826 Color: 1
Size: 264143 Color: 0
Size: 250032 Color: 1

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 490818 Color: 0
Size: 257863 Color: 1
Size: 251320 Color: 1

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 401428 Color: 1
Size: 301704 Color: 0
Size: 296869 Color: 1

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 400193 Color: 0
Size: 328089 Color: 0
Size: 271719 Color: 1

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 439247 Color: 0
Size: 293233 Color: 1
Size: 267521 Color: 1

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 442857 Color: 0
Size: 287268 Color: 1
Size: 269876 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 390014 Color: 0
Size: 308578 Color: 0
Size: 301409 Color: 1

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 386890 Color: 0
Size: 326166 Color: 1
Size: 286945 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 495661 Color: 0
Size: 253667 Color: 1
Size: 250673 Color: 1

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 383706 Color: 1
Size: 356998 Color: 0
Size: 259297 Color: 1

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 360376 Color: 1
Size: 353454 Color: 1
Size: 286171 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 364723 Color: 0
Size: 346759 Color: 1
Size: 288519 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 437118 Color: 0
Size: 296844 Color: 0
Size: 266039 Color: 1

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 426201 Color: 0
Size: 316795 Color: 0
Size: 257005 Color: 1

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 339528 Color: 0
Size: 337449 Color: 1
Size: 323024 Color: 1

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 438058 Color: 1
Size: 296316 Color: 0
Size: 265627 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 368622 Color: 0
Size: 343906 Color: 1
Size: 287473 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 433533 Color: 1
Size: 290431 Color: 0
Size: 276037 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 351598 Color: 1
Size: 344802 Color: 1
Size: 303601 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 368048 Color: 1
Size: 352796 Color: 0
Size: 279157 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 441073 Color: 0
Size: 298184 Color: 0
Size: 260744 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 419669 Color: 1
Size: 321404 Color: 0
Size: 258928 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 469464 Color: 0
Size: 266123 Color: 0
Size: 264414 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 397616 Color: 0
Size: 328306 Color: 0
Size: 274079 Color: 1

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 471322 Color: 0
Size: 269845 Color: 1
Size: 258834 Color: 1

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 436415 Color: 1
Size: 300447 Color: 1
Size: 263139 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 405554 Color: 1
Size: 319834 Color: 0
Size: 274613 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 422507 Color: 0
Size: 325320 Color: 0
Size: 252174 Color: 1

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 469701 Color: 0
Size: 266514 Color: 0
Size: 263786 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 469669 Color: 1
Size: 274099 Color: 0
Size: 256233 Color: 1

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 399581 Color: 1
Size: 300758 Color: 0
Size: 299662 Color: 1

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 410483 Color: 1
Size: 327053 Color: 0
Size: 262465 Color: 1

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 433257 Color: 1
Size: 307064 Color: 1
Size: 259680 Color: 0

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 434039 Color: 0
Size: 306264 Color: 0
Size: 259698 Color: 1

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 492907 Color: 0
Size: 256627 Color: 1
Size: 250467 Color: 1

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 421425 Color: 0
Size: 292843 Color: 0
Size: 285733 Color: 1

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 366727 Color: 0
Size: 347732 Color: 1
Size: 285542 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 483921 Color: 0
Size: 263818 Color: 1
Size: 252262 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 386154 Color: 0
Size: 329163 Color: 1
Size: 284684 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 482964 Color: 1
Size: 265797 Color: 0
Size: 251240 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 434301 Color: 1
Size: 295226 Color: 0
Size: 270474 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 416702 Color: 1
Size: 305048 Color: 1
Size: 278251 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 395930 Color: 0
Size: 341746 Color: 0
Size: 262325 Color: 1

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 477585 Color: 1
Size: 262172 Color: 1
Size: 260244 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 445069 Color: 1
Size: 289900 Color: 0
Size: 265032 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 391291 Color: 1
Size: 331733 Color: 0
Size: 276977 Color: 1

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 405853 Color: 1
Size: 319790 Color: 0
Size: 274358 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 444979 Color: 1
Size: 286578 Color: 0
Size: 268444 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 384063 Color: 1
Size: 338922 Color: 0
Size: 277016 Color: 1

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 408076 Color: 1
Size: 304347 Color: 0
Size: 287578 Color: 1

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 417660 Color: 0
Size: 300631 Color: 1
Size: 281710 Color: 1

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 479622 Color: 0
Size: 264240 Color: 0
Size: 256139 Color: 1

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 420398 Color: 0
Size: 305208 Color: 1
Size: 274395 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 435665 Color: 1
Size: 282667 Color: 1
Size: 281669 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 358933 Color: 0
Size: 334426 Color: 1
Size: 306642 Color: 1

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 398876 Color: 0
Size: 306991 Color: 1
Size: 294134 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 430094 Color: 0
Size: 302507 Color: 0
Size: 267400 Color: 1

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 449614 Color: 0
Size: 279131 Color: 1
Size: 271256 Color: 1

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 463309 Color: 0
Size: 283534 Color: 1
Size: 253158 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 379492 Color: 0
Size: 312766 Color: 1
Size: 307743 Color: 1

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 409173 Color: 1
Size: 340498 Color: 1
Size: 250330 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 447281 Color: 0
Size: 280730 Color: 0
Size: 271990 Color: 1

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 362012 Color: 1
Size: 342588 Color: 0
Size: 295401 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 450162 Color: 1
Size: 291329 Color: 0
Size: 258510 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 454358 Color: 0
Size: 282905 Color: 0
Size: 262738 Color: 1

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 401768 Color: 1
Size: 300368 Color: 0
Size: 297865 Color: 1

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 433838 Color: 0
Size: 292379 Color: 0
Size: 273784 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 370210 Color: 0
Size: 320404 Color: 0
Size: 309387 Color: 1

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 386373 Color: 0
Size: 339732 Color: 1
Size: 273896 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 475120 Color: 1
Size: 269821 Color: 0
Size: 255060 Color: 1

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 363346 Color: 0
Size: 356499 Color: 1
Size: 280156 Color: 1

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 371133 Color: 1
Size: 357916 Color: 1
Size: 270952 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 445928 Color: 0
Size: 294987 Color: 1
Size: 259086 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 458727 Color: 0
Size: 277525 Color: 0
Size: 263749 Color: 1

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 467744 Color: 0
Size: 273697 Color: 0
Size: 258560 Color: 1

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 414345 Color: 1
Size: 314741 Color: 0
Size: 270915 Color: 1

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 419445 Color: 1
Size: 313700 Color: 0
Size: 266856 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 472244 Color: 0
Size: 269620 Color: 0
Size: 258137 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 393250 Color: 0
Size: 336144 Color: 1
Size: 270607 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 442365 Color: 0
Size: 305549 Color: 1
Size: 252087 Color: 1

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 497446 Color: 1
Size: 251748 Color: 1
Size: 250807 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 410815 Color: 0
Size: 327247 Color: 0
Size: 261939 Color: 1

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 481757 Color: 0
Size: 261860 Color: 1
Size: 256384 Color: 1

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 437768 Color: 0
Size: 296145 Color: 0
Size: 266088 Color: 1

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 377263 Color: 0
Size: 352216 Color: 0
Size: 270522 Color: 1

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 377126 Color: 1
Size: 329394 Color: 1
Size: 293481 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 492429 Color: 0
Size: 255527 Color: 1
Size: 252045 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 444193 Color: 0
Size: 289722 Color: 1
Size: 266086 Color: 1

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 422356 Color: 0
Size: 292761 Color: 1
Size: 284884 Color: 1

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 452864 Color: 0
Size: 283719 Color: 1
Size: 263418 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 377401 Color: 1
Size: 354404 Color: 0
Size: 268196 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 369110 Color: 0
Size: 317822 Color: 1
Size: 313069 Color: 1

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 410988 Color: 0
Size: 298816 Color: 0
Size: 290197 Color: 1

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 403035 Color: 1
Size: 344570 Color: 1
Size: 252396 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 359556 Color: 1
Size: 343318 Color: 1
Size: 297127 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 477598 Color: 0
Size: 269348 Color: 0
Size: 253055 Color: 1

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 373006 Color: 1
Size: 362992 Color: 0
Size: 264003 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 375820 Color: 0
Size: 312295 Color: 1
Size: 311886 Color: 1

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 393693 Color: 0
Size: 315640 Color: 1
Size: 290668 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 381963 Color: 0
Size: 314638 Color: 1
Size: 303400 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 354341 Color: 1
Size: 341573 Color: 0
Size: 304087 Color: 1

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 418524 Color: 0
Size: 325019 Color: 0
Size: 256458 Color: 1

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 419528 Color: 1
Size: 292584 Color: 0
Size: 287889 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 363281 Color: 0
Size: 325958 Color: 1
Size: 310762 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 436913 Color: 1
Size: 311424 Color: 1
Size: 251664 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 417021 Color: 0
Size: 309850 Color: 0
Size: 273130 Color: 1

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 373202 Color: 1
Size: 315007 Color: 1
Size: 311792 Color: 0

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 355958 Color: 0
Size: 350686 Color: 1
Size: 293357 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 457680 Color: 0
Size: 287855 Color: 0
Size: 254466 Color: 1

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 489017 Color: 0
Size: 260725 Color: 1
Size: 250259 Color: 1

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 381631 Color: 1
Size: 345833 Color: 0
Size: 272537 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 357990 Color: 0
Size: 325868 Color: 0
Size: 316143 Color: 1

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 481359 Color: 0
Size: 262915 Color: 0
Size: 255727 Color: 1

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 407203 Color: 0
Size: 338193 Color: 1
Size: 254605 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 441699 Color: 0
Size: 302020 Color: 0
Size: 256282 Color: 1

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 366521 Color: 1
Size: 362542 Color: 1
Size: 270938 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 494617 Color: 0
Size: 255174 Color: 1
Size: 250210 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 425247 Color: 0
Size: 320301 Color: 1
Size: 254453 Color: 1

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 415551 Color: 0
Size: 326180 Color: 0
Size: 258270 Color: 1

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 427502 Color: 0
Size: 300658 Color: 0
Size: 271841 Color: 1

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 396229 Color: 1
Size: 306372 Color: 0
Size: 297400 Color: 1

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 427172 Color: 0
Size: 316339 Color: 0
Size: 256490 Color: 1

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 373395 Color: 0
Size: 332663 Color: 0
Size: 293943 Color: 1

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 365750 Color: 0
Size: 319264 Color: 0
Size: 314987 Color: 1

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 391227 Color: 0
Size: 337659 Color: 0
Size: 271115 Color: 1

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 360122 Color: 0
Size: 327981 Color: 0
Size: 311898 Color: 1

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 363549 Color: 0
Size: 342994 Color: 0
Size: 293458 Color: 1

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 385613 Color: 0
Size: 351958 Color: 0
Size: 262430 Color: 1

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 432007 Color: 0
Size: 284108 Color: 1
Size: 283886 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 471642 Color: 1
Size: 277724 Color: 0
Size: 250635 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 390582 Color: 1
Size: 358436 Color: 0
Size: 250983 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 374255 Color: 0
Size: 333254 Color: 0
Size: 292492 Color: 1

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 435141 Color: 1
Size: 293191 Color: 0
Size: 271669 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 434923 Color: 0
Size: 289560 Color: 0
Size: 275518 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 404543 Color: 0
Size: 333310 Color: 0
Size: 262148 Color: 1

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 414726 Color: 1
Size: 330762 Color: 0
Size: 254513 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 488026 Color: 1
Size: 256145 Color: 0
Size: 255830 Color: 1

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 483388 Color: 1
Size: 258478 Color: 1
Size: 258135 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 393856 Color: 1
Size: 325551 Color: 0
Size: 280594 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 363328 Color: 1
Size: 330328 Color: 1
Size: 306345 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 373935 Color: 1
Size: 323736 Color: 0
Size: 302330 Color: 1

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 365891 Color: 0
Size: 359541 Color: 1
Size: 274569 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 364791 Color: 1
Size: 325913 Color: 1
Size: 309297 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 367136 Color: 1
Size: 355832 Color: 0
Size: 277033 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 414836 Color: 0
Size: 309128 Color: 0
Size: 276037 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 383031 Color: 1
Size: 340089 Color: 0
Size: 276881 Color: 1

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 364477 Color: 1
Size: 332774 Color: 0
Size: 302750 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 441038 Color: 0
Size: 281589 Color: 1
Size: 277374 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 381426 Color: 1
Size: 336049 Color: 0
Size: 282526 Color: 1

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 445752 Color: 0
Size: 303131 Color: 0
Size: 251118 Color: 1

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 424310 Color: 1
Size: 306856 Color: 0
Size: 268835 Color: 1

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 384640 Color: 1
Size: 328514 Color: 0
Size: 286847 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 434477 Color: 0
Size: 290938 Color: 1
Size: 274586 Color: 1

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 466845 Color: 0
Size: 280324 Color: 1
Size: 252832 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 374395 Color: 1
Size: 344337 Color: 1
Size: 281269 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 457459 Color: 0
Size: 289006 Color: 0
Size: 253536 Color: 1

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 491324 Color: 1
Size: 255931 Color: 0
Size: 252746 Color: 1

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 397855 Color: 1
Size: 336360 Color: 0
Size: 265786 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 394008 Color: 0
Size: 351162 Color: 0
Size: 254831 Color: 1

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 421069 Color: 1
Size: 325274 Color: 0
Size: 253658 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 463536 Color: 0
Size: 270171 Color: 0
Size: 266294 Color: 1

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 371830 Color: 0
Size: 315816 Color: 1
Size: 312355 Color: 0

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 367060 Color: 0
Size: 333599 Color: 0
Size: 299342 Color: 1

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 454875 Color: 0
Size: 280180 Color: 0
Size: 264946 Color: 1

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 397584 Color: 0
Size: 351475 Color: 1
Size: 250942 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 359136 Color: 1
Size: 349431 Color: 0
Size: 291434 Color: 1

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 473153 Color: 0
Size: 273844 Color: 0
Size: 253004 Color: 1

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 497181 Color: 0
Size: 251932 Color: 1
Size: 250888 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 372670 Color: 1
Size: 360767 Color: 1
Size: 266564 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 360783 Color: 0
Size: 332399 Color: 1
Size: 306819 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 476125 Color: 0
Size: 269527 Color: 1
Size: 254349 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 361512 Color: 1
Size: 340598 Color: 0
Size: 297891 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 446009 Color: 0
Size: 280593 Color: 1
Size: 273399 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 359797 Color: 1
Size: 322970 Color: 0
Size: 317234 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 404537 Color: 0
Size: 340481 Color: 0
Size: 254983 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 490931 Color: 1
Size: 256070 Color: 0
Size: 253000 Color: 1

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 448521 Color: 1
Size: 293551 Color: 0
Size: 257929 Color: 1

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 363868 Color: 0
Size: 324409 Color: 1
Size: 311724 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 445916 Color: 1
Size: 302480 Color: 0
Size: 251605 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 374893 Color: 1
Size: 360239 Color: 0
Size: 264869 Color: 1

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 359072 Color: 0
Size: 345966 Color: 1
Size: 294963 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 363354 Color: 0
Size: 340487 Color: 1
Size: 296160 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 436651 Color: 0
Size: 282872 Color: 1
Size: 280478 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 474845 Color: 1
Size: 263994 Color: 1
Size: 261162 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 423019 Color: 0
Size: 322413 Color: 0
Size: 254569 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 388297 Color: 0
Size: 339497 Color: 1
Size: 272207 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 479326 Color: 0
Size: 261949 Color: 1
Size: 258726 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 379761 Color: 0
Size: 314707 Color: 1
Size: 305533 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 410488 Color: 1
Size: 316862 Color: 1
Size: 272651 Color: 0

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 404324 Color: 1
Size: 334563 Color: 1
Size: 261114 Color: 0

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 484594 Color: 0
Size: 259550 Color: 0
Size: 255857 Color: 1

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 444560 Color: 0
Size: 295281 Color: 0
Size: 260160 Color: 1

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 362080 Color: 1
Size: 341144 Color: 0
Size: 296777 Color: 1

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 390554 Color: 1
Size: 357147 Color: 0
Size: 252300 Color: 1

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 343754 Color: 1
Size: 332298 Color: 0
Size: 323949 Color: 1

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 427433 Color: 1
Size: 286865 Color: 1
Size: 285703 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 393211 Color: 1
Size: 313489 Color: 1
Size: 293301 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 457875 Color: 0
Size: 289138 Color: 1
Size: 252988 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 412902 Color: 0
Size: 297641 Color: 1
Size: 289458 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 424559 Color: 0
Size: 312826 Color: 1
Size: 262616 Color: 1

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 378903 Color: 1
Size: 357962 Color: 0
Size: 263136 Color: 1

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 367577 Color: 0
Size: 330448 Color: 0
Size: 301976 Color: 1

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 351634 Color: 1
Size: 343506 Color: 0
Size: 304861 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 367234 Color: 0
Size: 331635 Color: 1
Size: 301132 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 412976 Color: 1
Size: 333020 Color: 1
Size: 254005 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 459022 Color: 1
Size: 281272 Color: 0
Size: 259707 Color: 1

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 389676 Color: 0
Size: 345750 Color: 1
Size: 264575 Color: 0

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 366848 Color: 0
Size: 342797 Color: 0
Size: 290356 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 370003 Color: 1
Size: 346433 Color: 0
Size: 283565 Color: 1

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 399954 Color: 1
Size: 332156 Color: 0
Size: 267891 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 398990 Color: 0
Size: 333285 Color: 1
Size: 267726 Color: 1

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 421927 Color: 1
Size: 327494 Color: 1
Size: 250580 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 451910 Color: 0
Size: 296032 Color: 1
Size: 252059 Color: 1

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 411779 Color: 0
Size: 331007 Color: 0
Size: 257215 Color: 1

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 419968 Color: 1
Size: 302968 Color: 1
Size: 277065 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 415999 Color: 1
Size: 330219 Color: 0
Size: 253783 Color: 1

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 457708 Color: 1
Size: 272162 Color: 0
Size: 270131 Color: 1

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 473663 Color: 1
Size: 268305 Color: 1
Size: 258033 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 366936 Color: 0
Size: 340190 Color: 0
Size: 292875 Color: 1

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 434996 Color: 0
Size: 314486 Color: 1
Size: 250519 Color: 1

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 347457 Color: 0
Size: 342676 Color: 0
Size: 309868 Color: 1

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 384139 Color: 1
Size: 308358 Color: 0
Size: 307504 Color: 1

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 411964 Color: 1
Size: 325625 Color: 0
Size: 262412 Color: 1

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 383707 Color: 1
Size: 323607 Color: 1
Size: 292687 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 382762 Color: 1
Size: 348562 Color: 1
Size: 268677 Color: 0

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 441646 Color: 1
Size: 291778 Color: 0
Size: 266577 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 491064 Color: 1
Size: 256776 Color: 0
Size: 252161 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 417909 Color: 0
Size: 320669 Color: 1
Size: 261423 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 452407 Color: 0
Size: 289394 Color: 1
Size: 258200 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 498678 Color: 1
Size: 251090 Color: 0
Size: 250233 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 366509 Color: 1
Size: 317386 Color: 1
Size: 316106 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 471886 Color: 1
Size: 264924 Color: 0
Size: 263191 Color: 0

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 464757 Color: 0
Size: 280318 Color: 0
Size: 254926 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 434022 Color: 1
Size: 306610 Color: 0
Size: 259369 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 412242 Color: 1
Size: 327223 Color: 0
Size: 260536 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 440411 Color: 1
Size: 295868 Color: 0
Size: 263722 Color: 1

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 455254 Color: 1
Size: 288291 Color: 0
Size: 256456 Color: 1

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 435282 Color: 0
Size: 299403 Color: 1
Size: 265316 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 436417 Color: 0
Size: 286239 Color: 1
Size: 277345 Color: 1

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 410133 Color: 0
Size: 321520 Color: 1
Size: 268348 Color: 0

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 442993 Color: 1
Size: 282668 Color: 0
Size: 274340 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 391514 Color: 1
Size: 323120 Color: 1
Size: 285367 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 392900 Color: 0
Size: 304750 Color: 0
Size: 302351 Color: 1

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 453417 Color: 1
Size: 278482 Color: 0
Size: 268102 Color: 1

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 338525 Color: 1
Size: 335504 Color: 0
Size: 325972 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 386721 Color: 0
Size: 342087 Color: 1
Size: 271193 Color: 1

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 400240 Color: 0
Size: 301910 Color: 1
Size: 297851 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 432551 Color: 0
Size: 301746 Color: 0
Size: 265704 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 417457 Color: 1
Size: 291473 Color: 0
Size: 291071 Color: 1

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 443236 Color: 1
Size: 296800 Color: 0
Size: 259965 Color: 1

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 454983 Color: 0
Size: 272578 Color: 0
Size: 272440 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 429001 Color: 0
Size: 318009 Color: 1
Size: 252991 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 382008 Color: 1
Size: 332075 Color: 1
Size: 285918 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 438038 Color: 1
Size: 303753 Color: 0
Size: 258210 Color: 1

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 417396 Color: 0
Size: 292548 Color: 1
Size: 290057 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 369039 Color: 1
Size: 330551 Color: 1
Size: 300411 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 386364 Color: 0
Size: 310241 Color: 0
Size: 303396 Color: 1

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 411286 Color: 0
Size: 329739 Color: 1
Size: 258976 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 390045 Color: 0
Size: 351241 Color: 0
Size: 258715 Color: 1

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 450513 Color: 1
Size: 294450 Color: 0
Size: 255038 Color: 1

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 395991 Color: 1
Size: 317506 Color: 0
Size: 286504 Color: 1

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 375226 Color: 0
Size: 314588 Color: 0
Size: 310187 Color: 1

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 393533 Color: 1
Size: 322502 Color: 1
Size: 283966 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 498311 Color: 1
Size: 250986 Color: 1
Size: 250704 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 478922 Color: 1
Size: 262021 Color: 0
Size: 259058 Color: 1

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 414810 Color: 0
Size: 323121 Color: 1
Size: 262070 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 471478 Color: 0
Size: 278277 Color: 1
Size: 250246 Color: 1

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 449538 Color: 0
Size: 278259 Color: 1
Size: 272204 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 386430 Color: 0
Size: 339415 Color: 1
Size: 274156 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 472620 Color: 1
Size: 265914 Color: 0
Size: 261467 Color: 1

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 382735 Color: 0
Size: 356090 Color: 0
Size: 261176 Color: 1

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 361009 Color: 0
Size: 352149 Color: 0
Size: 286843 Color: 1

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 447685 Color: 1
Size: 298804 Color: 1
Size: 253512 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 425830 Color: 0
Size: 298886 Color: 1
Size: 275285 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 399548 Color: 0
Size: 323516 Color: 1
Size: 276937 Color: 1

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 420693 Color: 0
Size: 314898 Color: 1
Size: 264410 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 419888 Color: 1
Size: 322612 Color: 1
Size: 257501 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 386546 Color: 1
Size: 339217 Color: 1
Size: 274238 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 441770 Color: 1
Size: 293144 Color: 1
Size: 265087 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 397626 Color: 0
Size: 307283 Color: 0
Size: 295092 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 357132 Color: 0
Size: 339241 Color: 1
Size: 303628 Color: 1

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 493714 Color: 0
Size: 253869 Color: 1
Size: 252418 Color: 1

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 379645 Color: 0
Size: 325258 Color: 0
Size: 295098 Color: 1

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 383189 Color: 1
Size: 316812 Color: 0
Size: 300000 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 405418 Color: 0
Size: 323169 Color: 1
Size: 271414 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 339278 Color: 1
Size: 332475 Color: 0
Size: 328248 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 495823 Color: 0
Size: 252693 Color: 1
Size: 251485 Color: 1

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 374062 Color: 0
Size: 373823 Color: 0
Size: 252116 Color: 1

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 374085 Color: 1
Size: 366696 Color: 0
Size: 259220 Color: 1

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 385545 Color: 1
Size: 326302 Color: 0
Size: 288154 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 377579 Color: 1
Size: 352320 Color: 0
Size: 270102 Color: 1

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 442944 Color: 0
Size: 294805 Color: 0
Size: 262252 Color: 1

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 367849 Color: 1
Size: 345903 Color: 1
Size: 286249 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 400893 Color: 1
Size: 325944 Color: 0
Size: 273164 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 439555 Color: 1
Size: 293450 Color: 1
Size: 266996 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 472165 Color: 0
Size: 274696 Color: 0
Size: 253140 Color: 1

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 378436 Color: 1
Size: 338684 Color: 1
Size: 282881 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 431761 Color: 1
Size: 316053 Color: 0
Size: 252187 Color: 1

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 385773 Color: 1
Size: 321888 Color: 1
Size: 292340 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 426764 Color: 1
Size: 319208 Color: 1
Size: 254029 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 352210 Color: 1
Size: 341303 Color: 0
Size: 306488 Color: 1

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 474293 Color: 0
Size: 265760 Color: 1
Size: 259948 Color: 1

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 419765 Color: 1
Size: 324662 Color: 1
Size: 255574 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 366597 Color: 1
Size: 347126 Color: 1
Size: 286278 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 393732 Color: 0
Size: 355460 Color: 0
Size: 250809 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 490099 Color: 1
Size: 258440 Color: 0
Size: 251462 Color: 1

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 405907 Color: 0
Size: 328938 Color: 0
Size: 265156 Color: 1

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 388086 Color: 0
Size: 340599 Color: 1
Size: 271316 Color: 1

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 464935 Color: 1
Size: 277545 Color: 0
Size: 257521 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 409651 Color: 0
Size: 334872 Color: 1
Size: 255478 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 463219 Color: 0
Size: 273986 Color: 1
Size: 262796 Color: 1

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 490708 Color: 1
Size: 256093 Color: 0
Size: 253200 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 396260 Color: 0
Size: 319463 Color: 0
Size: 284278 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 420116 Color: 0
Size: 324512 Color: 1
Size: 255373 Color: 1

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 386751 Color: 1
Size: 331305 Color: 0
Size: 281945 Color: 1

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 423518 Color: 1
Size: 304253 Color: 0
Size: 272230 Color: 1

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 374556 Color: 1
Size: 340365 Color: 0
Size: 285080 Color: 1

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 381455 Color: 1
Size: 354456 Color: 0
Size: 264090 Color: 1

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 386086 Color: 1
Size: 351569 Color: 0
Size: 262346 Color: 1

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 375772 Color: 0
Size: 317638 Color: 1
Size: 306591 Color: 1

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 429438 Color: 1
Size: 304416 Color: 0
Size: 266147 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 386305 Color: 1
Size: 358236 Color: 0
Size: 255460 Color: 1

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 443078 Color: 1
Size: 286307 Color: 0
Size: 270616 Color: 1

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 382441 Color: 1
Size: 335512 Color: 0
Size: 282048 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 376884 Color: 0
Size: 314686 Color: 0
Size: 308431 Color: 1

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 490363 Color: 0
Size: 257953 Color: 0
Size: 251685 Color: 1

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 484079 Color: 0
Size: 261713 Color: 1
Size: 254209 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 468574 Color: 0
Size: 273812 Color: 1
Size: 257615 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 360513 Color: 0
Size: 334998 Color: 1
Size: 304490 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 421106 Color: 0
Size: 324145 Color: 0
Size: 254750 Color: 1

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 337816 Color: 1
Size: 337433 Color: 0
Size: 324752 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 469811 Color: 1
Size: 278145 Color: 0
Size: 252045 Color: 0

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 495598 Color: 1
Size: 253784 Color: 0
Size: 250619 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 364157 Color: 0
Size: 319329 Color: 1
Size: 316515 Color: 1

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 372755 Color: 1
Size: 356822 Color: 1
Size: 270424 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 472042 Color: 1
Size: 277246 Color: 1
Size: 250713 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 373044 Color: 0
Size: 347893 Color: 0
Size: 279064 Color: 1

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 471270 Color: 0
Size: 275357 Color: 0
Size: 253374 Color: 1

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 392108 Color: 0
Size: 305007 Color: 1
Size: 302886 Color: 1

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 419714 Color: 0
Size: 308302 Color: 0
Size: 271985 Color: 1

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 363493 Color: 1
Size: 360697 Color: 1
Size: 275811 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 468735 Color: 0
Size: 281166 Color: 1
Size: 250100 Color: 1

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 384708 Color: 1
Size: 362688 Color: 1
Size: 252605 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 442546 Color: 1
Size: 286827 Color: 0
Size: 270628 Color: 1

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 373816 Color: 1
Size: 322599 Color: 0
Size: 303586 Color: 1

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 383124 Color: 0
Size: 326007 Color: 1
Size: 290870 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 424844 Color: 0
Size: 314755 Color: 1
Size: 260402 Color: 1

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 360351 Color: 0
Size: 355414 Color: 1
Size: 284236 Color: 1

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 474938 Color: 1
Size: 267423 Color: 1
Size: 257640 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 492336 Color: 1
Size: 253918 Color: 0
Size: 253747 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 356650 Color: 0
Size: 340597 Color: 1
Size: 302754 Color: 0

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 499034 Color: 1
Size: 250826 Color: 0
Size: 250141 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 355677 Color: 1
Size: 328389 Color: 0
Size: 315935 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 478807 Color: 1
Size: 267002 Color: 1
Size: 254192 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 499149 Color: 1
Size: 250683 Color: 0
Size: 250169 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 355807 Color: 0
Size: 326488 Color: 1
Size: 317706 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 356356 Color: 1
Size: 326128 Color: 0
Size: 317517 Color: 1

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 374064 Color: 0
Size: 358422 Color: 1
Size: 267515 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 499726 Color: 0
Size: 250186 Color: 1
Size: 250089 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 472325 Color: 1
Size: 277123 Color: 0
Size: 250553 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 449201 Color: 0
Size: 278581 Color: 0
Size: 272219 Color: 1

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 365935 Color: 0
Size: 326542 Color: 0
Size: 307524 Color: 1

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 421783 Color: 1
Size: 304456 Color: 0
Size: 273762 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 404160 Color: 1
Size: 331468 Color: 0
Size: 264373 Color: 1

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 410407 Color: 0
Size: 332596 Color: 0
Size: 256998 Color: 1

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 498108 Color: 1
Size: 251635 Color: 0
Size: 250258 Color: 1

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 484039 Color: 1
Size: 262681 Color: 0
Size: 253281 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 353385 Color: 0
Size: 323802 Color: 1
Size: 322814 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 478402 Color: 0
Size: 268104 Color: 1
Size: 253495 Color: 1

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 477583 Color: 0
Size: 269998 Color: 1
Size: 252420 Color: 1

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 420362 Color: 0
Size: 321254 Color: 1
Size: 258385 Color: 1

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 477085 Color: 0
Size: 263871 Color: 1
Size: 259045 Color: 1

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 404513 Color: 0
Size: 344246 Color: 1
Size: 251242 Color: 1

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 399201 Color: 0
Size: 346406 Color: 0
Size: 254394 Color: 1

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 481036 Color: 1
Size: 266996 Color: 0
Size: 251969 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 387195 Color: 0
Size: 353549 Color: 1
Size: 259257 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 397866 Color: 1
Size: 332193 Color: 1
Size: 269942 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 386746 Color: 0
Size: 331153 Color: 0
Size: 282102 Color: 1

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 395385 Color: 0
Size: 346011 Color: 1
Size: 258605 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 498700 Color: 0
Size: 251118 Color: 0
Size: 250183 Color: 1

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 376311 Color: 0
Size: 352208 Color: 1
Size: 271482 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 412584 Color: 0
Size: 307100 Color: 0
Size: 280317 Color: 1

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 389237 Color: 0
Size: 347932 Color: 1
Size: 262832 Color: 1

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 421391 Color: 0
Size: 303372 Color: 0
Size: 275238 Color: 1

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 396125 Color: 1
Size: 322477 Color: 1
Size: 281399 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 384698 Color: 1
Size: 349048 Color: 1
Size: 266255 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 406404 Color: 1
Size: 340602 Color: 0
Size: 252995 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 450469 Color: 0
Size: 285966 Color: 0
Size: 263566 Color: 1

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 1
Size: 334823 Color: 1
Size: 260035 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 358359 Color: 0
Size: 347958 Color: 1
Size: 293684 Color: 1

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 339923 Color: 1
Size: 337598 Color: 0
Size: 322480 Color: 1

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 421047 Color: 1
Size: 321443 Color: 0
Size: 257511 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 359995 Color: 0
Size: 331836 Color: 0
Size: 308170 Color: 1

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 444922 Color: 0
Size: 283795 Color: 1
Size: 271284 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 411138 Color: 1
Size: 302881 Color: 1
Size: 285982 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 364757 Color: 1
Size: 357526 Color: 0
Size: 277718 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 421441 Color: 1
Size: 313687 Color: 0
Size: 264873 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 356173 Color: 1
Size: 324026 Color: 0
Size: 319802 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 465404 Color: 1
Size: 279879 Color: 0
Size: 254718 Color: 1

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 389110 Color: 1
Size: 353578 Color: 1
Size: 257313 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 355125 Color: 0
Size: 353720 Color: 0
Size: 291156 Color: 1

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 386639 Color: 1
Size: 311982 Color: 0
Size: 301380 Color: 1

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 374013 Color: 1
Size: 361469 Color: 0
Size: 264519 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 419517 Color: 0
Size: 309487 Color: 0
Size: 270997 Color: 1

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 409992 Color: 1
Size: 330960 Color: 0
Size: 259049 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 364450 Color: 1
Size: 341580 Color: 0
Size: 293971 Color: 1

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 373675 Color: 1
Size: 328025 Color: 0
Size: 298301 Color: 1

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 361637 Color: 1
Size: 350367 Color: 0
Size: 287997 Color: 1

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 499535 Color: 0
Size: 250329 Color: 0
Size: 250137 Color: 1

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 368494 Color: 0
Size: 321848 Color: 1
Size: 309659 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 473247 Color: 0
Size: 274488 Color: 1
Size: 252266 Color: 1

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 492710 Color: 1
Size: 254863 Color: 0
Size: 252428 Color: 1

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 362778 Color: 0
Size: 325443 Color: 1
Size: 311780 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 343694 Color: 1
Size: 332142 Color: 0
Size: 324165 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 346641 Color: 0
Size: 346088 Color: 1
Size: 307272 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 346866 Color: 1
Size: 343283 Color: 0
Size: 309852 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 350873 Color: 0
Size: 327407 Color: 1
Size: 321721 Color: 1

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 470545 Color: 0
Size: 275390 Color: 1
Size: 254066 Color: 1

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 353856 Color: 0
Size: 351011 Color: 0
Size: 295134 Color: 1

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 383230 Color: 1
Size: 360879 Color: 0
Size: 255892 Color: 0

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 389744 Color: 0
Size: 330736 Color: 1
Size: 279521 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 416589 Color: 0
Size: 298435 Color: 0
Size: 284977 Color: 1

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 380107 Color: 0
Size: 324315 Color: 1
Size: 295579 Color: 1

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 490295 Color: 1
Size: 254863 Color: 0
Size: 254843 Color: 1

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 474445 Color: 0
Size: 271719 Color: 0
Size: 253837 Color: 1

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 337879 Color: 1
Size: 332088 Color: 1
Size: 330034 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 344062 Color: 0
Size: 335291 Color: 1
Size: 320648 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 359232 Color: 1
Size: 358530 Color: 0
Size: 282239 Color: 0

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 383991 Color: 1
Size: 337623 Color: 1
Size: 278387 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 473725 Color: 1
Size: 274922 Color: 0
Size: 251354 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 400547 Color: 1
Size: 333892 Color: 1
Size: 265562 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 486635 Color: 1
Size: 258259 Color: 0
Size: 255107 Color: 1

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 401813 Color: 0
Size: 315349 Color: 0
Size: 282839 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 348124 Color: 0
Size: 337983 Color: 1
Size: 313894 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 409278 Color: 0
Size: 304325 Color: 0
Size: 286398 Color: 1

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 439514 Color: 0
Size: 307703 Color: 1
Size: 252784 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 415333 Color: 0
Size: 329379 Color: 1
Size: 255289 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 407065 Color: 0
Size: 298450 Color: 1
Size: 294486 Color: 1

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 409547 Color: 0
Size: 332492 Color: 1
Size: 257962 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 466018 Color: 1
Size: 274572 Color: 0
Size: 259411 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 401331 Color: 0
Size: 330681 Color: 0
Size: 267989 Color: 1

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 405233 Color: 0
Size: 302444 Color: 0
Size: 292324 Color: 1

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 351558 Color: 0
Size: 328565 Color: 0
Size: 319878 Color: 1

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 478615 Color: 1
Size: 268378 Color: 0
Size: 253008 Color: 1

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 399329 Color: 1
Size: 344104 Color: 1
Size: 256568 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 399279 Color: 1
Size: 320677 Color: 1
Size: 280045 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 397455 Color: 1
Size: 326468 Color: 1
Size: 276078 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 379780 Color: 0
Size: 341687 Color: 1
Size: 278534 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 352741 Color: 0
Size: 352740 Color: 1
Size: 294520 Color: 1

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 397915 Color: 0
Size: 313055 Color: 0
Size: 289031 Color: 1

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 418735 Color: 1
Size: 327697 Color: 1
Size: 253569 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 355160 Color: 1
Size: 346312 Color: 0
Size: 298529 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 404970 Color: 1
Size: 321379 Color: 0
Size: 273652 Color: 1

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 345807 Color: 1
Size: 343645 Color: 0
Size: 310549 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 357581 Color: 1
Size: 324406 Color: 0
Size: 318014 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 347775 Color: 0
Size: 331953 Color: 0
Size: 320273 Color: 1

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 436440 Color: 0
Size: 311791 Color: 0
Size: 251770 Color: 1

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 403620 Color: 0
Size: 346007 Color: 0
Size: 250374 Color: 1

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 349298 Color: 0
Size: 330161 Color: 1
Size: 320542 Color: 1

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 341484 Color: 0
Size: 338123 Color: 1
Size: 320394 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 410295 Color: 0
Size: 314735 Color: 1
Size: 274971 Color: 1

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 357811 Color: 0
Size: 350265 Color: 0
Size: 291925 Color: 1

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 383091 Color: 0
Size: 333529 Color: 1
Size: 283381 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 347264 Color: 1
Size: 344844 Color: 1
Size: 307893 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 461912 Color: 0
Size: 277713 Color: 0
Size: 260376 Color: 1

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 344840 Color: 0
Size: 344660 Color: 1
Size: 310501 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 447408 Color: 1
Size: 299636 Color: 1
Size: 252957 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 480221 Color: 0
Size: 262104 Color: 1
Size: 257676 Color: 1

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 451430 Color: 0
Size: 287532 Color: 1
Size: 261039 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 347429 Color: 1
Size: 339322 Color: 0
Size: 313250 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 368031 Color: 1
Size: 324216 Color: 1
Size: 307754 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 344572 Color: 1
Size: 334977 Color: 0
Size: 320452 Color: 0

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 346590 Color: 1
Size: 345449 Color: 1
Size: 307962 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 403082 Color: 0
Size: 330167 Color: 1
Size: 266752 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 414345 Color: 0
Size: 293557 Color: 0
Size: 292099 Color: 1

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 349515 Color: 1
Size: 348371 Color: 0
Size: 302115 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 455334 Color: 1
Size: 283022 Color: 1
Size: 261645 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 466690 Color: 1
Size: 274670 Color: 0
Size: 258641 Color: 1

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 438288 Color: 1
Size: 299848 Color: 1
Size: 261865 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 360841 Color: 1
Size: 352603 Color: 0
Size: 286557 Color: 1

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 386307 Color: 1
Size: 361824 Color: 0
Size: 251870 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 422427 Color: 1
Size: 301856 Color: 0
Size: 275718 Color: 1

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 357628 Color: 0
Size: 343381 Color: 1
Size: 298992 Color: 1

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 429743 Color: 1
Size: 302224 Color: 0
Size: 268034 Color: 1

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 431290 Color: 0
Size: 288026 Color: 1
Size: 280685 Color: 1

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 462817 Color: 0
Size: 270673 Color: 0
Size: 266511 Color: 1

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 450316 Color: 1
Size: 282413 Color: 1
Size: 267272 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 396439 Color: 0
Size: 313050 Color: 1
Size: 290512 Color: 1

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 342419 Color: 1
Size: 333091 Color: 0
Size: 324491 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 404084 Color: 0
Size: 314799 Color: 1
Size: 281118 Color: 1

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 339642 Color: 1
Size: 339050 Color: 0
Size: 321309 Color: 1

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 385643 Color: 1
Size: 337201 Color: 0
Size: 277157 Color: 1

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 366987 Color: 0
Size: 325069 Color: 0
Size: 307945 Color: 1

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 386773 Color: 0
Size: 348065 Color: 1
Size: 265163 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 393434 Color: 0
Size: 337883 Color: 1
Size: 268684 Color: 1

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 402667 Color: 1
Size: 330447 Color: 0
Size: 266887 Color: 1

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 348693 Color: 1
Size: 340538 Color: 0
Size: 310770 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 339877 Color: 1
Size: 332172 Color: 0
Size: 327952 Color: 1

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 344686 Color: 1
Size: 333385 Color: 0
Size: 321930 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 476725 Color: 0
Size: 268265 Color: 0
Size: 255011 Color: 1

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 374907 Color: 0
Size: 356158 Color: 0
Size: 268936 Color: 1

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 462950 Color: 0
Size: 279853 Color: 1
Size: 257198 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 382237 Color: 1
Size: 339685 Color: 1
Size: 278079 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 445551 Color: 0
Size: 291054 Color: 0
Size: 263396 Color: 1

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 431656 Color: 0
Size: 315256 Color: 1
Size: 253089 Color: 1

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 351183 Color: 0
Size: 336921 Color: 1
Size: 311897 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 398260 Color: 0
Size: 331346 Color: 0
Size: 270395 Color: 1

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 480186 Color: 0
Size: 260082 Color: 1
Size: 259733 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 441470 Color: 0
Size: 283849 Color: 0
Size: 274682 Color: 1

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 357847 Color: 1
Size: 346412 Color: 0
Size: 295742 Color: 1

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 385075 Color: 1
Size: 331623 Color: 0
Size: 283303 Color: 1

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 397099 Color: 1
Size: 342600 Color: 1
Size: 260302 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 351975 Color: 1
Size: 332108 Color: 1
Size: 315918 Color: 0

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 344259 Color: 0
Size: 336821 Color: 0
Size: 318921 Color: 1

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 411845 Color: 1
Size: 315322 Color: 0
Size: 272834 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 492968 Color: 1
Size: 256921 Color: 0
Size: 250112 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 454721 Color: 1
Size: 285698 Color: 1
Size: 259582 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 468111 Color: 1
Size: 278910 Color: 0
Size: 252980 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 358271 Color: 1
Size: 354151 Color: 0
Size: 287579 Color: 1

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 494641 Color: 1
Size: 254986 Color: 0
Size: 250374 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 351994 Color: 0
Size: 331838 Color: 1
Size: 316169 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 450563 Color: 0
Size: 294603 Color: 0
Size: 254835 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 484149 Color: 0
Size: 261074 Color: 1
Size: 254778 Color: 1

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 360166 Color: 0
Size: 350957 Color: 1
Size: 288878 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 368524 Color: 1
Size: 316980 Color: 0
Size: 314497 Color: 1

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 386914 Color: 0
Size: 318591 Color: 1
Size: 294496 Color: 1

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 466468 Color: 0
Size: 273533 Color: 1
Size: 260000 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 356573 Color: 0
Size: 343653 Color: 1
Size: 299775 Color: 1

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 340271 Color: 1
Size: 331580 Color: 0
Size: 328150 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 361297 Color: 1
Size: 342781 Color: 1
Size: 295923 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 354092 Color: 1
Size: 327862 Color: 0
Size: 318047 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 348070 Color: 1
Size: 346156 Color: 0
Size: 305775 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 352955 Color: 1
Size: 344843 Color: 1
Size: 302203 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 396318 Color: 0
Size: 330902 Color: 1
Size: 272781 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 397001 Color: 0
Size: 314787 Color: 1
Size: 288213 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 437877 Color: 0
Size: 306246 Color: 0
Size: 255878 Color: 1

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 349557 Color: 0
Size: 342052 Color: 1
Size: 308392 Color: 1

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 422183 Color: 1
Size: 295936 Color: 1
Size: 281882 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 399175 Color: 0
Size: 322617 Color: 1
Size: 278209 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 421613 Color: 0
Size: 327621 Color: 0
Size: 250767 Color: 1

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 458111 Color: 1
Size: 282970 Color: 0
Size: 258920 Color: 1

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 393050 Color: 0
Size: 342614 Color: 1
Size: 264337 Color: 1

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 416469 Color: 0
Size: 312368 Color: 1
Size: 271164 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 355245 Color: 1
Size: 345152 Color: 0
Size: 299604 Color: 1

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 495991 Color: 1
Size: 252730 Color: 0
Size: 251280 Color: 1

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 359504 Color: 0
Size: 325125 Color: 0
Size: 315372 Color: 1

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 343952 Color: 0
Size: 341998 Color: 1
Size: 314051 Color: 1

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 492216 Color: 0
Size: 255008 Color: 1
Size: 252777 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 400159 Color: 0
Size: 338888 Color: 1
Size: 260954 Color: 1

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 379187 Color: 0
Size: 354278 Color: 0
Size: 266536 Color: 1

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 399755 Color: 0
Size: 302448 Color: 1
Size: 297798 Color: 1

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 493507 Color: 1
Size: 254848 Color: 1
Size: 251646 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 477493 Color: 1
Size: 271419 Color: 0
Size: 251089 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 438305 Color: 0
Size: 297808 Color: 1
Size: 263888 Color: 1

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 374898 Color: 0
Size: 361900 Color: 0
Size: 263203 Color: 1

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 389975 Color: 0
Size: 358039 Color: 1
Size: 251987 Color: 1

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 438264 Color: 0
Size: 308561 Color: 0
Size: 253176 Color: 1

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 357863 Color: 1
Size: 357577 Color: 0
Size: 284561 Color: 1

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 398442 Color: 1
Size: 315199 Color: 0
Size: 286360 Color: 1

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 470606 Color: 0
Size: 265909 Color: 0
Size: 263486 Color: 1

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 401063 Color: 1
Size: 348396 Color: 0
Size: 250542 Color: 1

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 458769 Color: 0
Size: 279388 Color: 0
Size: 261844 Color: 1

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 496289 Color: 0
Size: 252122 Color: 1
Size: 251590 Color: 1

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 358680 Color: 1
Size: 350169 Color: 0
Size: 291152 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 440799 Color: 0
Size: 308216 Color: 0
Size: 250986 Color: 1

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 454773 Color: 1
Size: 277789 Color: 0
Size: 267439 Color: 0

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 362204 Color: 1
Size: 341555 Color: 1
Size: 296242 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 427495 Color: 1
Size: 305925 Color: 1
Size: 266581 Color: 0

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 438725 Color: 1
Size: 304364 Color: 0
Size: 256912 Color: 1

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 390115 Color: 1
Size: 343151 Color: 1
Size: 266735 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 449045 Color: 1
Size: 296965 Color: 1
Size: 253991 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 392360 Color: 0
Size: 324023 Color: 1
Size: 283618 Color: 1

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 395710 Color: 1
Size: 350062 Color: 0
Size: 254229 Color: 1

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 391804 Color: 0
Size: 337607 Color: 1
Size: 270590 Color: 1

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 363852 Color: 1
Size: 340074 Color: 0
Size: 296075 Color: 1

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 354619 Color: 1
Size: 343837 Color: 0
Size: 301545 Color: 1

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 463914 Color: 1
Size: 281218 Color: 1
Size: 254869 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 434844 Color: 0
Size: 286615 Color: 1
Size: 278542 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 346967 Color: 1
Size: 338515 Color: 0
Size: 314519 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 394299 Color: 0
Size: 348026 Color: 1
Size: 257676 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 483511 Color: 1
Size: 263866 Color: 0
Size: 252624 Color: 1

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 347465 Color: 0
Size: 342062 Color: 0
Size: 310474 Color: 1

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 371369 Color: 0
Size: 333637 Color: 1
Size: 294995 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 409955 Color: 1
Size: 300381 Color: 1
Size: 289665 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 356337 Color: 0
Size: 334120 Color: 0
Size: 309544 Color: 1

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 382541 Color: 1
Size: 328375 Color: 1
Size: 289085 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 363361 Color: 1
Size: 328294 Color: 0
Size: 308346 Color: 1

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 452318 Color: 0
Size: 285393 Color: 1
Size: 262290 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 456774 Color: 0
Size: 287994 Color: 1
Size: 255233 Color: 1

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 435197 Color: 0
Size: 294267 Color: 0
Size: 270537 Color: 1

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 352945 Color: 0
Size: 346182 Color: 0
Size: 300874 Color: 1

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 417976 Color: 0
Size: 319377 Color: 0
Size: 262648 Color: 1

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 414809 Color: 0
Size: 305020 Color: 1
Size: 280172 Color: 1

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 401301 Color: 1
Size: 327796 Color: 0
Size: 270904 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 493322 Color: 0
Size: 254835 Color: 0
Size: 251844 Color: 1

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 443363 Color: 0
Size: 283838 Color: 1
Size: 272800 Color: 0

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 406208 Color: 0
Size: 330095 Color: 0
Size: 263698 Color: 1

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 375526 Color: 0
Size: 318312 Color: 1
Size: 306163 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 443655 Color: 1
Size: 284269 Color: 1
Size: 272077 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 445442 Color: 0
Size: 296264 Color: 1
Size: 258295 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 361331 Color: 0
Size: 319854 Color: 1
Size: 318816 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 350870 Color: 0
Size: 339217 Color: 1
Size: 309914 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 348524 Color: 0
Size: 339858 Color: 1
Size: 311619 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 385289 Color: 1
Size: 355595 Color: 0
Size: 259117 Color: 1

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 412766 Color: 0
Size: 315477 Color: 1
Size: 271758 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 366034 Color: 1
Size: 337780 Color: 1
Size: 296187 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 341618 Color: 1
Size: 340109 Color: 0
Size: 318274 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 393511 Color: 0
Size: 327668 Color: 1
Size: 278822 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 377813 Color: 0
Size: 335949 Color: 1
Size: 286239 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 499494 Color: 1
Size: 250279 Color: 1
Size: 250228 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 472404 Color: 1
Size: 269958 Color: 0
Size: 257639 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 474793 Color: 1
Size: 269958 Color: 1
Size: 255250 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 482076 Color: 1
Size: 262209 Color: 0
Size: 255716 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 489481 Color: 0
Size: 258514 Color: 1
Size: 252006 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 440113 Color: 0
Size: 281710 Color: 0
Size: 278178 Color: 1

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 401988 Color: 1
Size: 341794 Color: 0
Size: 256219 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 414142 Color: 0
Size: 329640 Color: 1
Size: 256219 Color: 1

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 478977 Color: 0
Size: 265546 Color: 1
Size: 255478 Color: 1

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 363651 Color: 1
Size: 358038 Color: 0
Size: 278312 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 436322 Color: 1
Size: 285367 Color: 0
Size: 278312 Color: 1

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 399105 Color: 0
Size: 334022 Color: 1
Size: 266874 Color: 1

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 409778 Color: 0
Size: 323349 Color: 1
Size: 266874 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 388787 Color: 0
Size: 352796 Color: 1
Size: 258418 Color: 1

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 383078 Color: 1
Size: 315920 Color: 1
Size: 301003 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 396626 Color: 0
Size: 315920 Color: 0
Size: 287455 Color: 1

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 499617 Color: 1
Size: 250364 Color: 0
Size: 250020 Color: 1

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 415255 Color: 0
Size: 304813 Color: 0
Size: 279933 Color: 1

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 415255 Color: 1
Size: 311250 Color: 1
Size: 273496 Color: 0

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 407041 Color: 1
Size: 317822 Color: 0
Size: 275138 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 479060 Color: 0
Size: 270707 Color: 1
Size: 250234 Color: 1

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 373536 Color: 1
Size: 333447 Color: 0
Size: 293018 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 415637 Color: 1
Size: 333447 Color: 1
Size: 250917 Color: 0

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 420647 Color: 1
Size: 327796 Color: 1
Size: 251558 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 457281 Color: 1
Size: 291816 Color: 0
Size: 250904 Color: 1

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 493372 Color: 0
Size: 255725 Color: 1
Size: 250904 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 460850 Color: 0
Size: 270383 Color: 1
Size: 268768 Color: 1

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 351172 Color: 1
Size: 329053 Color: 1
Size: 319776 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 338786 Color: 0
Size: 336516 Color: 0
Size: 324699 Color: 1

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 351622 Color: 1
Size: 331921 Color: 0
Size: 316458 Color: 0

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 354860 Color: 0
Size: 322652 Color: 0
Size: 322489 Color: 1

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 382835 Color: 0
Size: 366803 Color: 1
Size: 250363 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 490491 Color: 1
Size: 259147 Color: 0
Size: 250363 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 471177 Color: 1
Size: 271735 Color: 1
Size: 257089 Color: 0

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 471177 Color: 0
Size: 265712 Color: 1
Size: 263112 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 348461 Color: 0
Size: 334629 Color: 1
Size: 316911 Color: 1

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 352257 Color: 0
Size: 327083 Color: 0
Size: 320661 Color: 1

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 343041 Color: 1
Size: 339922 Color: 0
Size: 317038 Color: 1

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 359096 Color: 0
Size: 352357 Color: 1
Size: 288548 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 345356 Color: 0
Size: 332839 Color: 1
Size: 321806 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 346533 Color: 0
Size: 346528 Color: 1
Size: 306940 Color: 1

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 346057 Color: 0
Size: 337270 Color: 1
Size: 316674 Color: 1

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 347916 Color: 1
Size: 335868 Color: 0
Size: 316217 Color: 1

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 355007 Color: 0
Size: 333884 Color: 1
Size: 311110 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 355350 Color: 1
Size: 325580 Color: 0
Size: 319071 Color: 1

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 356204 Color: 0
Size: 335690 Color: 1
Size: 308107 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 357683 Color: 1
Size: 334840 Color: 0
Size: 307478 Color: 1

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 358173 Color: 1
Size: 336853 Color: 0
Size: 304975 Color: 1

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 359058 Color: 0
Size: 334629 Color: 1
Size: 306314 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 359156 Color: 0
Size: 349500 Color: 0
Size: 291345 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 359103 Color: 1
Size: 329610 Color: 0
Size: 311288 Color: 1

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 359177 Color: 0
Size: 321750 Color: 1
Size: 319074 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 359448 Color: 0
Size: 345424 Color: 1
Size: 295129 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 359180 Color: 1
Size: 336312 Color: 0
Size: 304509 Color: 1

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 359479 Color: 0
Size: 340554 Color: 1
Size: 299968 Color: 0

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 359197 Color: 1
Size: 353321 Color: 1
Size: 287483 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 359496 Color: 0
Size: 336197 Color: 0
Size: 304308 Color: 1

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 359197 Color: 1
Size: 354383 Color: 0
Size: 286421 Color: 1

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 359247 Color: 1
Size: 338322 Color: 1
Size: 302432 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 359615 Color: 0
Size: 336310 Color: 1
Size: 304076 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 359469 Color: 1
Size: 349205 Color: 0
Size: 291327 Color: 1

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 360015 Color: 0
Size: 331229 Color: 0
Size: 308757 Color: 1

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 360055 Color: 1
Size: 346434 Color: 1
Size: 293512 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 360276 Color: 0
Size: 351800 Color: 1
Size: 287925 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 360263 Color: 1
Size: 347928 Color: 1
Size: 291810 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 360389 Color: 1
Size: 349557 Color: 1
Size: 290055 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 360607 Color: 0
Size: 346221 Color: 0
Size: 293173 Color: 1

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 360576 Color: 1
Size: 356864 Color: 0
Size: 282561 Color: 1

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 360615 Color: 1
Size: 332913 Color: 0
Size: 306473 Color: 1

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 361020 Color: 1
Size: 360325 Color: 0
Size: 278656 Color: 1

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 361126 Color: 0
Size: 353292 Color: 0
Size: 285583 Color: 1

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 361137 Color: 0
Size: 335129 Color: 1
Size: 303735 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 361149 Color: 0
Size: 329463 Color: 0
Size: 309389 Color: 1

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 361178 Color: 1
Size: 342402 Color: 1
Size: 296421 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 361282 Color: 1
Size: 322845 Color: 1
Size: 315874 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 361382 Color: 1
Size: 336344 Color: 1
Size: 302275 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 361382 Color: 0
Size: 331570 Color: 0
Size: 307049 Color: 1

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 361497 Color: 1
Size: 326867 Color: 0
Size: 311637 Color: 1

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 361410 Color: 0
Size: 339080 Color: 0
Size: 299511 Color: 1

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 361669 Color: 1
Size: 349263 Color: 1
Size: 289069 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 361716 Color: 0
Size: 331781 Color: 1
Size: 306504 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 361832 Color: 1
Size: 326803 Color: 0
Size: 311366 Color: 1

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 361789 Color: 0
Size: 346375 Color: 0
Size: 291837 Color: 1

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 361901 Color: 1
Size: 328024 Color: 1
Size: 310076 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 361910 Color: 0
Size: 353765 Color: 1
Size: 284326 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 361955 Color: 0
Size: 333865 Color: 1
Size: 304181 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 362096 Color: 0
Size: 332078 Color: 0
Size: 305827 Color: 1

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 362678 Color: 0
Size: 327939 Color: 0
Size: 309384 Color: 1

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 362696 Color: 0
Size: 353203 Color: 1
Size: 284102 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 362959 Color: 0
Size: 359656 Color: 1
Size: 277386 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 363109 Color: 0
Size: 360640 Color: 1
Size: 276252 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 362869 Color: 1
Size: 342384 Color: 0
Size: 294748 Color: 1

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 362897 Color: 1
Size: 343849 Color: 1
Size: 293255 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 363189 Color: 0
Size: 339501 Color: 1
Size: 297311 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 363302 Color: 0
Size: 321939 Color: 0
Size: 314760 Color: 1

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 363558 Color: 1
Size: 336526 Color: 0
Size: 299917 Color: 1

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 363599 Color: 1
Size: 355012 Color: 1
Size: 281390 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 363633 Color: 1
Size: 345055 Color: 1
Size: 291313 Color: 0

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 363628 Color: 0
Size: 341538 Color: 1
Size: 294835 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 363737 Color: 1
Size: 349615 Color: 1
Size: 286649 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 363746 Color: 0
Size: 320898 Color: 1
Size: 315357 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 364025 Color: 0
Size: 359491 Color: 0
Size: 276485 Color: 1

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 363940 Color: 1
Size: 331567 Color: 1
Size: 304494 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 364178 Color: 1
Size: 339676 Color: 0
Size: 296147 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 364199 Color: 1
Size: 361062 Color: 1
Size: 274740 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 364140 Color: 0
Size: 336633 Color: 1
Size: 299228 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 364225 Color: 1
Size: 345842 Color: 1
Size: 289934 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 364366 Color: 1
Size: 362798 Color: 0
Size: 272837 Color: 1

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 364575 Color: 1
Size: 330621 Color: 1
Size: 304805 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 364598 Color: 0
Size: 357025 Color: 1
Size: 278378 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 364684 Color: 1
Size: 341911 Color: 0
Size: 293406 Color: 1

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 364906 Color: 0
Size: 328076 Color: 1
Size: 307019 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 364930 Color: 0
Size: 349915 Color: 1
Size: 285156 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 365031 Color: 0
Size: 335961 Color: 0
Size: 299009 Color: 1

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 365038 Color: 0
Size: 325075 Color: 0
Size: 309888 Color: 1

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 365041 Color: 1
Size: 318427 Color: 1
Size: 316533 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 365045 Color: 0
Size: 350841 Color: 0
Size: 284115 Color: 1

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 365108 Color: 0
Size: 339412 Color: 1
Size: 295481 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 365118 Color: 0
Size: 365110 Color: 0
Size: 269773 Color: 1

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 365178 Color: 0
Size: 317879 Color: 0
Size: 316944 Color: 1

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 365394 Color: 1
Size: 350843 Color: 1
Size: 283764 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 365696 Color: 0
Size: 332431 Color: 0
Size: 301874 Color: 1

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 365462 Color: 1
Size: 322646 Color: 0
Size: 311893 Color: 1

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 365486 Color: 1
Size: 332688 Color: 1
Size: 301827 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 365728 Color: 0
Size: 329261 Color: 1
Size: 305012 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 365807 Color: 1
Size: 321543 Color: 0
Size: 312651 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 365912 Color: 1
Size: 338103 Color: 0
Size: 295986 Color: 1

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 365783 Color: 0
Size: 334210 Color: 0
Size: 300008 Color: 1

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 366117 Color: 1
Size: 337564 Color: 0
Size: 296320 Color: 1

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 365849 Color: 0
Size: 319541 Color: 1
Size: 314611 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 366165 Color: 1
Size: 333215 Color: 1
Size: 300621 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 366207 Color: 1
Size: 357193 Color: 1
Size: 276601 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 366265 Color: 1
Size: 332127 Color: 1
Size: 301609 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 366051 Color: 0
Size: 326572 Color: 0
Size: 307378 Color: 1

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 366279 Color: 1
Size: 361201 Color: 1
Size: 272521 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 366421 Color: 1
Size: 358126 Color: 1
Size: 275454 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 366210 Color: 0
Size: 353371 Color: 1
Size: 280420 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 366440 Color: 1
Size: 318607 Color: 1
Size: 314954 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 366263 Color: 0
Size: 351296 Color: 0
Size: 282442 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 366372 Color: 0
Size: 346699 Color: 1
Size: 286930 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 366478 Color: 0
Size: 365430 Color: 1
Size: 268093 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 366514 Color: 1
Size: 319559 Color: 0
Size: 313928 Color: 1

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 366646 Color: 0
Size: 345427 Color: 1
Size: 287928 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 366750 Color: 0
Size: 341743 Color: 0
Size: 291508 Color: 1

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 486048 Color: 1
Size: 260952 Color: 1
Size: 253001 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 366810 Color: 0
Size: 341362 Color: 1
Size: 291829 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 366929 Color: 1
Size: 354254 Color: 0
Size: 278818 Color: 1

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 366998 Color: 1
Size: 323453 Color: 1
Size: 309550 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 367219 Color: 0
Size: 326323 Color: 0
Size: 306459 Color: 1

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 367146 Color: 1
Size: 354669 Color: 1
Size: 278186 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 367359 Color: 1
Size: 330248 Color: 0
Size: 302394 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 367291 Color: 0
Size: 335113 Color: 1
Size: 297597 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 367375 Color: 1
Size: 345787 Color: 1
Size: 286839 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 367291 Color: 0
Size: 342722 Color: 1
Size: 289988 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 367407 Color: 1
Size: 316634 Color: 1
Size: 315960 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 367412 Color: 1
Size: 316727 Color: 0
Size: 315862 Color: 1

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 367490 Color: 1
Size: 324080 Color: 1
Size: 308431 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 367357 Color: 0
Size: 320372 Color: 1
Size: 312272 Color: 0

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 367549 Color: 1
Size: 332666 Color: 0
Size: 299786 Color: 1

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 367429 Color: 0
Size: 337033 Color: 0
Size: 295539 Color: 1

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 367564 Color: 1
Size: 320684 Color: 0
Size: 311753 Color: 1

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 367693 Color: 0
Size: 340824 Color: 0
Size: 291484 Color: 1

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 367692 Color: 1
Size: 318577 Color: 0
Size: 313732 Color: 1

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 367758 Color: 0
Size: 360436 Color: 1
Size: 271807 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 367734 Color: 1
Size: 342013 Color: 1
Size: 290254 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 367852 Color: 0
Size: 339794 Color: 0
Size: 292355 Color: 1

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 367875 Color: 1
Size: 356524 Color: 1
Size: 275602 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 368000 Color: 0
Size: 341731 Color: 0
Size: 290270 Color: 1

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 368030 Color: 0
Size: 323839 Color: 1
Size: 308132 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 368020 Color: 1
Size: 331530 Color: 0
Size: 300451 Color: 1

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 368039 Color: 0
Size: 324803 Color: 0
Size: 307159 Color: 1

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 368111 Color: 0
Size: 319009 Color: 1
Size: 312881 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 368259 Color: 0
Size: 337593 Color: 0
Size: 294149 Color: 1

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 368197 Color: 1
Size: 327779 Color: 1
Size: 304025 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 368227 Color: 1
Size: 355483 Color: 1
Size: 276291 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 368392 Color: 0
Size: 361279 Color: 1
Size: 270330 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 368379 Color: 1
Size: 324874 Color: 1
Size: 306748 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 368457 Color: 1
Size: 367219 Color: 0
Size: 264325 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 368495 Color: 1
Size: 337852 Color: 0
Size: 293654 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 368394 Color: 0
Size: 336293 Color: 0
Size: 295314 Color: 1

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 368536 Color: 1
Size: 340789 Color: 1
Size: 290676 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 368620 Color: 0
Size: 337161 Color: 1
Size: 294220 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 368701 Color: 0
Size: 336914 Color: 1
Size: 294386 Color: 1

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 368766 Color: 0
Size: 326819 Color: 1
Size: 304416 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 368664 Color: 1
Size: 360834 Color: 0
Size: 270503 Color: 1

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 368826 Color: 0
Size: 347138 Color: 1
Size: 284037 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 368846 Color: 0
Size: 328460 Color: 1
Size: 302695 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 368983 Color: 0
Size: 324625 Color: 1
Size: 306393 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 368941 Color: 1
Size: 333698 Color: 1
Size: 297362 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 369214 Color: 0
Size: 326135 Color: 0
Size: 304652 Color: 1

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 369394 Color: 0
Size: 343770 Color: 1
Size: 286837 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 369477 Color: 0
Size: 360447 Color: 0
Size: 270077 Color: 1

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 369353 Color: 1
Size: 362631 Color: 1
Size: 268017 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 369567 Color: 0
Size: 316784 Color: 1
Size: 313650 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 369651 Color: 1
Size: 356310 Color: 1
Size: 274040 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 369748 Color: 0
Size: 357403 Color: 1
Size: 272850 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 369997 Color: 0
Size: 327669 Color: 0
Size: 302335 Color: 1

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 370029 Color: 0
Size: 356430 Color: 0
Size: 273542 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 369823 Color: 1
Size: 331122 Color: 0
Size: 299056 Color: 1

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 370048 Color: 0
Size: 327369 Color: 0
Size: 302584 Color: 1

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 370091 Color: 0
Size: 319920 Color: 0
Size: 309990 Color: 1

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 370180 Color: 0
Size: 340690 Color: 1
Size: 289131 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 370207 Color: 0
Size: 320462 Color: 0
Size: 309332 Color: 1

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 370213 Color: 0
Size: 334350 Color: 1
Size: 295438 Color: 1

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 370184 Color: 1
Size: 333642 Color: 0
Size: 296175 Color: 1

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 370244 Color: 0
Size: 326552 Color: 1
Size: 303205 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 370294 Color: 0
Size: 332706 Color: 1
Size: 297001 Color: 1

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 370318 Color: 0
Size: 331660 Color: 0
Size: 298023 Color: 1

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 370305 Color: 1
Size: 321179 Color: 0
Size: 308517 Color: 1

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 370352 Color: 0
Size: 342326 Color: 1
Size: 287323 Color: 1

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 370361 Color: 1
Size: 340932 Color: 1
Size: 288708 Color: 0

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 370390 Color: 0
Size: 326905 Color: 1
Size: 302706 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 370404 Color: 1
Size: 333563 Color: 1
Size: 296034 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 370394 Color: 0
Size: 370292 Color: 1
Size: 259315 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 370579 Color: 0
Size: 356714 Color: 1
Size: 272708 Color: 1

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 370701 Color: 0
Size: 350191 Color: 0
Size: 279109 Color: 1

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 370573 Color: 1
Size: 335270 Color: 0
Size: 294158 Color: 1

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 370610 Color: 1
Size: 324998 Color: 0
Size: 304393 Color: 1

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 370782 Color: 0
Size: 355548 Color: 1
Size: 273671 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 370668 Color: 1
Size: 329127 Color: 1
Size: 300206 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 370708 Color: 1
Size: 369677 Color: 1
Size: 259616 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 370940 Color: 0
Size: 356277 Color: 0
Size: 272784 Color: 1

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 370820 Color: 1
Size: 348974 Color: 0
Size: 280207 Color: 1

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 371079 Color: 0
Size: 336345 Color: 1
Size: 292577 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 370853 Color: 1
Size: 331028 Color: 1
Size: 298120 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 371093 Color: 0
Size: 347427 Color: 0
Size: 281481 Color: 1

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 371048 Color: 1
Size: 318233 Color: 0
Size: 310720 Color: 1

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 371117 Color: 1
Size: 362350 Color: 1
Size: 266534 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 371155 Color: 1
Size: 342042 Color: 0
Size: 286804 Color: 1

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 371167 Color: 1
Size: 367989 Color: 1
Size: 260845 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 371385 Color: 0
Size: 325222 Color: 1
Size: 303394 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 371361 Color: 1
Size: 359118 Color: 1
Size: 269522 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 371455 Color: 0
Size: 338801 Color: 0
Size: 289745 Color: 1

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 371540 Color: 1
Size: 366598 Color: 0
Size: 261863 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 371582 Color: 1
Size: 369721 Color: 1
Size: 258698 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 371468 Color: 0
Size: 348059 Color: 0
Size: 280474 Color: 1

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 371769 Color: 1
Size: 332307 Color: 0
Size: 295925 Color: 1

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 371629 Color: 0
Size: 363622 Color: 1
Size: 264750 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 371787 Color: 1
Size: 347346 Color: 0
Size: 280868 Color: 1

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 371891 Color: 0
Size: 335167 Color: 1
Size: 292943 Color: 1

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 371910 Color: 0
Size: 334701 Color: 0
Size: 293390 Color: 1

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 371915 Color: 0
Size: 327383 Color: 1
Size: 300703 Color: 1

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 371941 Color: 0
Size: 319737 Color: 1
Size: 308323 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 371883 Color: 1
Size: 362192 Color: 0
Size: 265926 Color: 1

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 372016 Color: 0
Size: 314637 Color: 1
Size: 313348 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 372023 Color: 0
Size: 335069 Color: 0
Size: 292909 Color: 1

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 372053 Color: 0
Size: 369414 Color: 1
Size: 258534 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 372104 Color: 0
Size: 350401 Color: 0
Size: 277496 Color: 1

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 372045 Color: 1
Size: 337933 Color: 0
Size: 290023 Color: 1

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 372114 Color: 0
Size: 330503 Color: 1
Size: 297384 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 372091 Color: 1
Size: 359602 Color: 1
Size: 268308 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 372157 Color: 0
Size: 350632 Color: 0
Size: 277212 Color: 1

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 372101 Color: 1
Size: 343225 Color: 1
Size: 284675 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 372215 Color: 0
Size: 371079 Color: 0
Size: 256707 Color: 1

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 372178 Color: 1
Size: 356121 Color: 0
Size: 271702 Color: 1

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 372266 Color: 0
Size: 366898 Color: 1
Size: 260837 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 372185 Color: 1
Size: 348123 Color: 1
Size: 279693 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 372272 Color: 1
Size: 367550 Color: 1
Size: 260179 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 372635 Color: 0
Size: 355822 Color: 1
Size: 271544 Color: 1

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 372637 Color: 0
Size: 350213 Color: 0
Size: 277151 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 372676 Color: 0
Size: 338356 Color: 0
Size: 288969 Color: 1

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 372787 Color: 0
Size: 352074 Color: 1
Size: 275140 Color: 1

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 372888 Color: 0
Size: 364615 Color: 0
Size: 262498 Color: 1

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 372956 Color: 0
Size: 366235 Color: 1
Size: 260810 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 372992 Color: 0
Size: 349852 Color: 0
Size: 277157 Color: 1

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 372984 Color: 1
Size: 342935 Color: 1
Size: 284082 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 373012 Color: 0
Size: 333714 Color: 1
Size: 293275 Color: 0

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 373026 Color: 0
Size: 350294 Color: 0
Size: 276681 Color: 1

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 373158 Color: 0
Size: 340659 Color: 0
Size: 286184 Color: 1

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 373276 Color: 0
Size: 354469 Color: 1
Size: 272256 Color: 1

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 373294 Color: 0
Size: 321301 Color: 1
Size: 305406 Color: 1

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 373386 Color: 0
Size: 329995 Color: 0
Size: 296620 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 373295 Color: 1
Size: 355887 Color: 0
Size: 270819 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 373435 Color: 0
Size: 352242 Color: 1
Size: 274324 Color: 1

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 373461 Color: 0
Size: 360742 Color: 1
Size: 265798 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 373318 Color: 1
Size: 353774 Color: 1
Size: 272909 Color: 0

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 373410 Color: 1
Size: 345435 Color: 1
Size: 281156 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 373533 Color: 1
Size: 316664 Color: 0
Size: 309804 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 373593 Color: 0
Size: 328069 Color: 0
Size: 298339 Color: 1

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 373619 Color: 0
Size: 316047 Color: 1
Size: 310335 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 373643 Color: 0
Size: 356307 Color: 1
Size: 270051 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 373672 Color: 0
Size: 330895 Color: 1
Size: 295434 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 373716 Color: 1
Size: 347093 Color: 1
Size: 279192 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 373693 Color: 0
Size: 340153 Color: 1
Size: 286155 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 373701 Color: 0
Size: 330994 Color: 1
Size: 295306 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 373822 Color: 1
Size: 365573 Color: 1
Size: 260606 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 373942 Color: 1
Size: 365141 Color: 0
Size: 260918 Color: 1

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 374116 Color: 1
Size: 367758 Color: 1
Size: 258127 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 374320 Color: 0
Size: 335997 Color: 0
Size: 289684 Color: 1

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 374337 Color: 1
Size: 344355 Color: 0
Size: 281309 Color: 0

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 374385 Color: 0
Size: 365624 Color: 0
Size: 259992 Color: 1

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 374647 Color: 1
Size: 346771 Color: 1
Size: 278583 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 374728 Color: 1
Size: 317570 Color: 1
Size: 307703 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 374763 Color: 1
Size: 324869 Color: 1
Size: 300369 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 374875 Color: 0
Size: 332527 Color: 1
Size: 292599 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 374764 Color: 1
Size: 362555 Color: 0
Size: 262682 Color: 1

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 374783 Color: 1
Size: 329424 Color: 0
Size: 295794 Color: 1

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 374914 Color: 0
Size: 314769 Color: 1
Size: 310318 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 374939 Color: 1
Size: 364937 Color: 1
Size: 260125 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 374957 Color: 0
Size: 367713 Color: 1
Size: 257331 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 375011 Color: 1
Size: 363069 Color: 1
Size: 261921 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 375106 Color: 0
Size: 332786 Color: 0
Size: 292109 Color: 1

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 375033 Color: 1
Size: 373222 Color: 1
Size: 251746 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 375164 Color: 0
Size: 358645 Color: 1
Size: 266192 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 375101 Color: 1
Size: 338364 Color: 0
Size: 286536 Color: 1

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 375356 Color: 0
Size: 332632 Color: 1
Size: 292013 Color: 0

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 375197 Color: 1
Size: 352110 Color: 1
Size: 272694 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 375250 Color: 1
Size: 362828 Color: 0
Size: 261923 Color: 1

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 375430 Color: 0
Size: 330424 Color: 0
Size: 294147 Color: 1

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 375262 Color: 1
Size: 320858 Color: 0
Size: 303881 Color: 1

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 375456 Color: 0
Size: 337345 Color: 0
Size: 287200 Color: 1

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 375286 Color: 1
Size: 328952 Color: 0
Size: 295763 Color: 1

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 375317 Color: 1
Size: 362800 Color: 0
Size: 261884 Color: 1

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 375475 Color: 0
Size: 334038 Color: 0
Size: 290488 Color: 1

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 375534 Color: 1
Size: 343042 Color: 0
Size: 281425 Color: 1

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 375565 Color: 1
Size: 316947 Color: 1
Size: 307489 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 375651 Color: 1
Size: 313078 Color: 0
Size: 311272 Color: 1

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 375692 Color: 0
Size: 315900 Color: 1
Size: 308409 Color: 1

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 375701 Color: 0
Size: 346224 Color: 1
Size: 278076 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 375804 Color: 0
Size: 320629 Color: 0
Size: 303568 Color: 1

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 375836 Color: 0
Size: 319245 Color: 1
Size: 304920 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 375821 Color: 1
Size: 321331 Color: 0
Size: 302849 Color: 1

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 375861 Color: 0
Size: 362429 Color: 1
Size: 261711 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 375895 Color: 1
Size: 318084 Color: 1
Size: 306022 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 375916 Color: 0
Size: 320991 Color: 0
Size: 303094 Color: 1

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 376047 Color: 0
Size: 330997 Color: 1
Size: 292957 Color: 1

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 376065 Color: 0
Size: 325957 Color: 0
Size: 297979 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 375974 Color: 1
Size: 317324 Color: 0
Size: 306703 Color: 1

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 376131 Color: 1
Size: 320982 Color: 0
Size: 302888 Color: 1

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 376040 Color: 1
Size: 350141 Color: 1
Size: 273820 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 376153 Color: 0
Size: 330184 Color: 1
Size: 293664 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 376066 Color: 1
Size: 369084 Color: 0
Size: 254851 Color: 1

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 376115 Color: 1
Size: 369166 Color: 0
Size: 254720 Color: 1

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 376289 Color: 0
Size: 318620 Color: 0
Size: 305092 Color: 1

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 376292 Color: 0
Size: 345534 Color: 0
Size: 278175 Color: 1

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 376131 Color: 0
Size: 337316 Color: 0
Size: 286554 Color: 1

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 376334 Color: 0
Size: 370461 Color: 1
Size: 253206 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 376145 Color: 1
Size: 315056 Color: 1
Size: 308800 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 376344 Color: 0
Size: 332429 Color: 0
Size: 291228 Color: 1

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 376413 Color: 0
Size: 359965 Color: 1
Size: 263623 Color: 1

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 376192 Color: 1
Size: 329760 Color: 0
Size: 294049 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 376373 Color: 1
Size: 344712 Color: 0
Size: 278916 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 376628 Color: 0
Size: 357521 Color: 1
Size: 265852 Color: 0

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 376423 Color: 1
Size: 361383 Color: 0
Size: 262195 Color: 1

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 376858 Color: 0
Size: 346895 Color: 0
Size: 276248 Color: 1

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 376652 Color: 1
Size: 371832 Color: 0
Size: 251517 Color: 1

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 376946 Color: 0
Size: 371788 Color: 0
Size: 251267 Color: 1

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 376950 Color: 1
Size: 354739 Color: 1
Size: 268312 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 376954 Color: 0
Size: 320692 Color: 1
Size: 302355 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 377003 Color: 1
Size: 314657 Color: 0
Size: 308341 Color: 1

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 376976 Color: 0
Size: 360221 Color: 1
Size: 262804 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 377016 Color: 1
Size: 353650 Color: 0
Size: 269335 Color: 1

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 377017 Color: 0
Size: 340541 Color: 0
Size: 282443 Color: 1

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 377022 Color: 0
Size: 319099 Color: 0
Size: 303880 Color: 1

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 377130 Color: 1
Size: 369899 Color: 0
Size: 252972 Color: 1

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 377064 Color: 0
Size: 354400 Color: 0
Size: 268537 Color: 1

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 377114 Color: 0
Size: 353392 Color: 0
Size: 269495 Color: 1

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 377382 Color: 1
Size: 319013 Color: 0
Size: 303606 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 377391 Color: 0
Size: 332441 Color: 0
Size: 290169 Color: 1

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 377549 Color: 1
Size: 313098 Color: 0
Size: 309354 Color: 1

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 377913 Color: 0
Size: 320566 Color: 0
Size: 301522 Color: 1

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 378009 Color: 0
Size: 346578 Color: 1
Size: 275414 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 378104 Color: 0
Size: 313804 Color: 0
Size: 308093 Color: 1

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 378152 Color: 1
Size: 315011 Color: 0
Size: 306838 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 378240 Color: 1
Size: 357410 Color: 1
Size: 264351 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 378240 Color: 0
Size: 363442 Color: 0
Size: 258319 Color: 1

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 378331 Color: 1
Size: 345827 Color: 0
Size: 275843 Color: 1

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 378301 Color: 0
Size: 330128 Color: 0
Size: 291572 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 378365 Color: 1
Size: 349969 Color: 1
Size: 271667 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 378473 Color: 1
Size: 331488 Color: 1
Size: 290040 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 378670 Color: 1
Size: 324129 Color: 0
Size: 297202 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 378520 Color: 0
Size: 333018 Color: 1
Size: 288463 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 378789 Color: 1
Size: 325124 Color: 1
Size: 296088 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 378602 Color: 0
Size: 313977 Color: 0
Size: 307422 Color: 1

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 378825 Color: 1
Size: 322015 Color: 0
Size: 299161 Color: 1

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 378642 Color: 0
Size: 364808 Color: 0
Size: 256551 Color: 1

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 378871 Color: 0
Size: 322650 Color: 1
Size: 298480 Color: 1

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 378889 Color: 0
Size: 314226 Color: 0
Size: 306886 Color: 1

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 378905 Color: 0
Size: 365317 Color: 0
Size: 255779 Color: 1

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 378946 Color: 1
Size: 313822 Color: 1
Size: 307233 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 379012 Color: 0
Size: 355903 Color: 1
Size: 265086 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 379061 Color: 0
Size: 346295 Color: 0
Size: 274645 Color: 1

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 379022 Color: 1
Size: 315138 Color: 0
Size: 305841 Color: 1

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 379024 Color: 1
Size: 343184 Color: 0
Size: 277793 Color: 1

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 379092 Color: 0
Size: 324958 Color: 1
Size: 295951 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 379036 Color: 1
Size: 328616 Color: 1
Size: 292349 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 379120 Color: 0
Size: 346980 Color: 1
Size: 273901 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 379094 Color: 1
Size: 320667 Color: 1
Size: 300240 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 379248 Color: 0
Size: 336987 Color: 1
Size: 283766 Color: 1

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 379318 Color: 0
Size: 367410 Color: 1
Size: 253273 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 379522 Color: 0
Size: 343716 Color: 0
Size: 276763 Color: 1

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 379585 Color: 0
Size: 347792 Color: 0
Size: 272624 Color: 1

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 379469 Color: 1
Size: 328235 Color: 0
Size: 292297 Color: 1

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 379671 Color: 0
Size: 368413 Color: 0
Size: 251917 Color: 1

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 379686 Color: 1
Size: 314868 Color: 1
Size: 305447 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 379702 Color: 0
Size: 366637 Color: 1
Size: 253662 Color: 0

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 379711 Color: 0
Size: 365227 Color: 1
Size: 255063 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 379747 Color: 1
Size: 337515 Color: 1
Size: 282739 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 379770 Color: 0
Size: 328247 Color: 0
Size: 291984 Color: 1

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 379890 Color: 1
Size: 342570 Color: 0
Size: 277541 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 379929 Color: 1
Size: 336099 Color: 0
Size: 283973 Color: 1

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 379938 Color: 1
Size: 321110 Color: 0
Size: 298953 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 379960 Color: 1
Size: 345666 Color: 0
Size: 274375 Color: 1

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 379967 Color: 1
Size: 361393 Color: 0
Size: 258641 Color: 1

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 379973 Color: 0
Size: 327084 Color: 0
Size: 292944 Color: 1

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 380013 Color: 1
Size: 342240 Color: 1
Size: 277748 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 380067 Color: 0
Size: 354029 Color: 0
Size: 265905 Color: 1

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 380077 Color: 0
Size: 336806 Color: 1
Size: 283118 Color: 1

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 380090 Color: 0
Size: 325001 Color: 1
Size: 294910 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 380109 Color: 0
Size: 318005 Color: 1
Size: 301887 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 380311 Color: 0
Size: 331483 Color: 1
Size: 288207 Color: 1

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 380297 Color: 1
Size: 333799 Color: 0
Size: 285905 Color: 1

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 380333 Color: 0
Size: 323186 Color: 0
Size: 296482 Color: 1

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 380467 Color: 0
Size: 321155 Color: 1
Size: 298379 Color: 1

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 380788 Color: 1
Size: 347860 Color: 0
Size: 271353 Color: 1

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 380794 Color: 1
Size: 313549 Color: 0
Size: 305658 Color: 0

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 380658 Color: 0
Size: 348787 Color: 1
Size: 270556 Color: 0

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 380939 Color: 1
Size: 353648 Color: 0
Size: 265414 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 380999 Color: 0
Size: 319355 Color: 0
Size: 299647 Color: 1

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 381031 Color: 1
Size: 314726 Color: 1
Size: 304244 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 381116 Color: 1
Size: 358507 Color: 0
Size: 260378 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 381201 Color: 0
Size: 353022 Color: 0
Size: 265778 Color: 1

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 381240 Color: 1
Size: 312160 Color: 0
Size: 306601 Color: 1

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 381336 Color: 0
Size: 344112 Color: 1
Size: 274553 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 381355 Color: 0
Size: 340986 Color: 1
Size: 277660 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 381486 Color: 1
Size: 353216 Color: 0
Size: 265299 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 381546 Color: 1
Size: 358452 Color: 0
Size: 260003 Color: 1

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 381559 Color: 1
Size: 357943 Color: 0
Size: 260499 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 381571 Color: 1
Size: 322063 Color: 1
Size: 296367 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 381583 Color: 0
Size: 331679 Color: 0
Size: 286739 Color: 1

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 381629 Color: 1
Size: 356443 Color: 0
Size: 261929 Color: 1

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 381694 Color: 1
Size: 346757 Color: 1
Size: 271550 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 381734 Color: 1
Size: 364406 Color: 0
Size: 253861 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 381787 Color: 1
Size: 317474 Color: 1
Size: 300740 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 381803 Color: 1
Size: 343949 Color: 0
Size: 274249 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 381680 Color: 0
Size: 333096 Color: 0
Size: 285225 Color: 1

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 381811 Color: 1
Size: 328328 Color: 1
Size: 289862 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 381876 Color: 1
Size: 351903 Color: 0
Size: 266222 Color: 1

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 382004 Color: 0
Size: 347926 Color: 1
Size: 270071 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 382044 Color: 1
Size: 356447 Color: 0
Size: 261510 Color: 1

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 382015 Color: 0
Size: 323761 Color: 1
Size: 294225 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 382165 Color: 1
Size: 367750 Color: 1
Size: 250086 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 382187 Color: 0
Size: 357109 Color: 1
Size: 260705 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 382243 Color: 0
Size: 351142 Color: 1
Size: 266616 Color: 0

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 382234 Color: 1
Size: 332949 Color: 0
Size: 284818 Color: 1

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 382335 Color: 0
Size: 341096 Color: 1
Size: 276570 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 374606 Color: 0
Size: 339602 Color: 0
Size: 285793 Color: 1

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 382361 Color: 1
Size: 347082 Color: 1
Size: 270558 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 382467 Color: 0
Size: 323708 Color: 0
Size: 293826 Color: 1

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 382508 Color: 0
Size: 348279 Color: 1
Size: 269214 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 382532 Color: 0
Size: 321127 Color: 0
Size: 296342 Color: 1

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 382569 Color: 0
Size: 332955 Color: 0
Size: 284477 Color: 1

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 382707 Color: 0
Size: 326145 Color: 1
Size: 291149 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 382610 Color: 1
Size: 349600 Color: 0
Size: 267791 Color: 1

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 382745 Color: 0
Size: 351030 Color: 0
Size: 266226 Color: 1

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 382646 Color: 1
Size: 308970 Color: 0
Size: 308385 Color: 1

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 382779 Color: 1
Size: 317209 Color: 0
Size: 300013 Color: 1

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 382890 Color: 0
Size: 366475 Color: 1
Size: 250636 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 382781 Color: 1
Size: 318503 Color: 1
Size: 298717 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 382917 Color: 1
Size: 322765 Color: 0
Size: 294319 Color: 1

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 382932 Color: 0
Size: 348298 Color: 1
Size: 268771 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 382930 Color: 1
Size: 323893 Color: 1
Size: 293178 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 383033 Color: 0
Size: 311184 Color: 0
Size: 305784 Color: 1

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 383186 Color: 1
Size: 319582 Color: 0
Size: 297233 Color: 1

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 383181 Color: 0
Size: 348233 Color: 0
Size: 268587 Color: 1

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 383361 Color: 0
Size: 356000 Color: 1
Size: 260640 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 383376 Color: 0
Size: 327541 Color: 0
Size: 289084 Color: 1

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 383431 Color: 1
Size: 334072 Color: 1
Size: 282498 Color: 0

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 383453 Color: 0
Size: 359324 Color: 1
Size: 257224 Color: 0

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 383501 Color: 0
Size: 328907 Color: 0
Size: 287593 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 383696 Color: 1
Size: 363192 Color: 0
Size: 253113 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 383650 Color: 0
Size: 358250 Color: 1
Size: 258101 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 383767 Color: 0
Size: 351849 Color: 1
Size: 264385 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 383781 Color: 0
Size: 319029 Color: 1
Size: 297191 Color: 0

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 383853 Color: 1
Size: 325666 Color: 0
Size: 290482 Color: 1

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 383907 Color: 0
Size: 312009 Color: 1
Size: 304085 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 383918 Color: 1
Size: 362906 Color: 0
Size: 253177 Color: 1

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 383990 Color: 0
Size: 341808 Color: 1
Size: 274203 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 384008 Color: 0
Size: 316924 Color: 1
Size: 299069 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 384099 Color: 0
Size: 361278 Color: 1
Size: 254624 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 384164 Color: 0
Size: 328117 Color: 1
Size: 287720 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 384121 Color: 1
Size: 320750 Color: 1
Size: 295130 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 0
Size: 349783 Color: 0
Size: 265989 Color: 1

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 384655 Color: 1
Size: 337516 Color: 1
Size: 277830 Color: 0

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 384671 Color: 1
Size: 361728 Color: 0
Size: 253602 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 384460 Color: 0
Size: 362986 Color: 0
Size: 252555 Color: 1

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 384688 Color: 1
Size: 357161 Color: 1
Size: 258152 Color: 0

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 384513 Color: 0
Size: 353135 Color: 1
Size: 262353 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 384529 Color: 0
Size: 318856 Color: 1
Size: 296616 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 384743 Color: 0
Size: 344819 Color: 1
Size: 270439 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 384813 Color: 1
Size: 338057 Color: 0
Size: 277131 Color: 1

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 384771 Color: 0
Size: 344575 Color: 1
Size: 270655 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 384834 Color: 1
Size: 322751 Color: 1
Size: 292416 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 384867 Color: 1
Size: 347526 Color: 1
Size: 267608 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 385033 Color: 0
Size: 332614 Color: 0
Size: 282354 Color: 1

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 385044 Color: 0
Size: 309922 Color: 1
Size: 305035 Color: 1

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 385050 Color: 1
Size: 319479 Color: 1
Size: 295472 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 385095 Color: 0
Size: 313280 Color: 1
Size: 301626 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 385271 Color: 1
Size: 348020 Color: 0
Size: 266710 Color: 1

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 385207 Color: 0
Size: 350161 Color: 1
Size: 264633 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 385347 Color: 1
Size: 346858 Color: 1
Size: 267796 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 385336 Color: 0
Size: 335206 Color: 1
Size: 279459 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 385387 Color: 1
Size: 334156 Color: 0
Size: 280458 Color: 1

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 385344 Color: 0
Size: 310654 Color: 1
Size: 304003 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 385387 Color: 1
Size: 324309 Color: 0
Size: 290305 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 385562 Color: 0
Size: 354357 Color: 0
Size: 260082 Color: 1

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 385589 Color: 0
Size: 333812 Color: 1
Size: 280600 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 385597 Color: 1
Size: 362771 Color: 0
Size: 251633 Color: 1

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 385760 Color: 0
Size: 315610 Color: 1
Size: 298631 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 385841 Color: 0
Size: 311150 Color: 1
Size: 303010 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 385900 Color: 0
Size: 351225 Color: 1
Size: 262876 Color: 0

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 385901 Color: 0
Size: 349224 Color: 1
Size: 264876 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 385799 Color: 1
Size: 344843 Color: 1
Size: 269359 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 385906 Color: 0
Size: 314902 Color: 1
Size: 299193 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 385810 Color: 1
Size: 362558 Color: 0
Size: 251633 Color: 1

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 385939 Color: 1
Size: 325821 Color: 0
Size: 288241 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 385976 Color: 0
Size: 307341 Color: 1
Size: 306684 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 386056 Color: 0
Size: 331822 Color: 0
Size: 282123 Color: 1

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 386174 Color: 1
Size: 318318 Color: 0
Size: 295509 Color: 1

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 386222 Color: 1
Size: 354740 Color: 0
Size: 259039 Color: 1

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 386200 Color: 0
Size: 317200 Color: 0
Size: 296601 Color: 1

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 386297 Color: 0
Size: 330672 Color: 1
Size: 283032 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 386280 Color: 1
Size: 321699 Color: 1
Size: 292022 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 386345 Color: 0
Size: 360253 Color: 1
Size: 253403 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 386357 Color: 0
Size: 323751 Color: 1
Size: 289893 Color: 1

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 386380 Color: 1
Size: 321872 Color: 0
Size: 291749 Color: 1

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 386486 Color: 0
Size: 356300 Color: 0
Size: 257215 Color: 1

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 386419 Color: 1
Size: 312301 Color: 0
Size: 301281 Color: 1

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 386733 Color: 0
Size: 313604 Color: 1
Size: 299664 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 386697 Color: 1
Size: 333486 Color: 0
Size: 279818 Color: 1

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 386933 Color: 0
Size: 309036 Color: 0
Size: 304032 Color: 1

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 387084 Color: 0
Size: 335879 Color: 1
Size: 277038 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 387110 Color: 0
Size: 350848 Color: 1
Size: 262043 Color: 1

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 387245 Color: 0
Size: 348458 Color: 1
Size: 264298 Color: 1

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 387402 Color: 0
Size: 327615 Color: 0
Size: 284984 Color: 1

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 387404 Color: 1
Size: 327167 Color: 1
Size: 285430 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 387681 Color: 1
Size: 316068 Color: 0
Size: 296252 Color: 1

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 387744 Color: 1
Size: 360820 Color: 0
Size: 251437 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 387769 Color: 1
Size: 331075 Color: 0
Size: 281157 Color: 1

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 387784 Color: 1
Size: 341995 Color: 0
Size: 270222 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 387549 Color: 0
Size: 312890 Color: 1
Size: 299562 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 387819 Color: 1
Size: 342364 Color: 0
Size: 269818 Color: 1

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 387860 Color: 1
Size: 321393 Color: 0
Size: 290748 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 387770 Color: 0
Size: 339046 Color: 1
Size: 273185 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 387912 Color: 1
Size: 339576 Color: 1
Size: 272513 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 388037 Color: 1
Size: 345593 Color: 0
Size: 266371 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 388164 Color: 1
Size: 317374 Color: 1
Size: 294463 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 388255 Color: 0
Size: 341411 Color: 0
Size: 270335 Color: 1

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 388212 Color: 1
Size: 322398 Color: 1
Size: 289391 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 388297 Color: 1
Size: 352567 Color: 1
Size: 259137 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 388401 Color: 0
Size: 309461 Color: 1
Size: 302139 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 388328 Color: 1
Size: 313726 Color: 1
Size: 297947 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 388496 Color: 0
Size: 329690 Color: 0
Size: 281815 Color: 1

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 388487 Color: 1
Size: 349753 Color: 0
Size: 261761 Color: 1

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 388751 Color: 1
Size: 334309 Color: 1
Size: 276941 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 388811 Color: 1
Size: 357368 Color: 0
Size: 253822 Color: 1

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 388943 Color: 1
Size: 346398 Color: 1
Size: 264660 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 388962 Color: 1
Size: 315517 Color: 0
Size: 295522 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 389021 Color: 1
Size: 338862 Color: 1
Size: 272118 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 389049 Color: 1
Size: 340965 Color: 0
Size: 269987 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 389051 Color: 1
Size: 337122 Color: 1
Size: 273828 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 389066 Color: 0
Size: 322129 Color: 0
Size: 288806 Color: 1

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 389123 Color: 0
Size: 322082 Color: 1
Size: 288796 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 389149 Color: 1
Size: 349897 Color: 1
Size: 260955 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 389153 Color: 1
Size: 322527 Color: 0
Size: 288321 Color: 1

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 389215 Color: 1
Size: 312966 Color: 0
Size: 297820 Color: 1

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 389299 Color: 0
Size: 358803 Color: 1
Size: 251899 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 389267 Color: 1
Size: 313408 Color: 0
Size: 297326 Color: 1

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 389360 Color: 0
Size: 323236 Color: 1
Size: 287405 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 389416 Color: 0
Size: 325323 Color: 1
Size: 285262 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 389471 Color: 1
Size: 347510 Color: 0
Size: 263020 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 389527 Color: 1
Size: 309018 Color: 0
Size: 301456 Color: 1

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 389719 Color: 1
Size: 346849 Color: 0
Size: 263433 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 389563 Color: 0
Size: 307886 Color: 1
Size: 302552 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 389748 Color: 1
Size: 341003 Color: 1
Size: 269250 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 389749 Color: 1
Size: 311339 Color: 1
Size: 298913 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 389891 Color: 1
Size: 358115 Color: 1
Size: 251995 Color: 0

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 389878 Color: 0
Size: 333545 Color: 0
Size: 276578 Color: 1

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 390006 Color: 1
Size: 345804 Color: 0
Size: 264191 Color: 1

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 389987 Color: 0
Size: 328877 Color: 0
Size: 281137 Color: 1

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 390030 Color: 1
Size: 331949 Color: 1
Size: 278022 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 390107 Color: 1
Size: 309780 Color: 0
Size: 300114 Color: 1

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 390068 Color: 0
Size: 328508 Color: 0
Size: 281425 Color: 1

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 390202 Color: 1
Size: 342393 Color: 0
Size: 267406 Color: 1

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 390100 Color: 0
Size: 317964 Color: 0
Size: 291937 Color: 1

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 390242 Color: 1
Size: 317454 Color: 1
Size: 292305 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 390284 Color: 1
Size: 305030 Color: 0
Size: 304687 Color: 0

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 390284 Color: 1
Size: 356666 Color: 0
Size: 253051 Color: 1

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 390307 Color: 1
Size: 344501 Color: 1
Size: 265193 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 390408 Color: 1
Size: 321161 Color: 0
Size: 288432 Color: 1

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 390434 Color: 1
Size: 320241 Color: 0
Size: 289326 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 390412 Color: 0
Size: 352538 Color: 1
Size: 257051 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 390532 Color: 0
Size: 332476 Color: 1
Size: 276993 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 390660 Color: 0
Size: 319681 Color: 0
Size: 289660 Color: 1

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 390699 Color: 0
Size: 324452 Color: 1
Size: 284850 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 390590 Color: 1
Size: 306456 Color: 0
Size: 302955 Color: 1

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 390948 Color: 1
Size: 329793 Color: 0
Size: 279260 Color: 1

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 390914 Color: 0
Size: 345532 Color: 1
Size: 263555 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 391056 Color: 1
Size: 342894 Color: 0
Size: 266051 Color: 1

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 390916 Color: 0
Size: 317418 Color: 1
Size: 291667 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 391099 Color: 1
Size: 357730 Color: 1
Size: 251172 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 391103 Color: 1
Size: 342899 Color: 1
Size: 265999 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 391135 Color: 1
Size: 336757 Color: 0
Size: 272109 Color: 1

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 391187 Color: 1
Size: 355216 Color: 0
Size: 253598 Color: 1

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 391384 Color: 0
Size: 335086 Color: 0
Size: 273531 Color: 1

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 391322 Color: 1
Size: 338718 Color: 1
Size: 269961 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 391398 Color: 0
Size: 331027 Color: 0
Size: 277576 Color: 1

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 391406 Color: 0
Size: 323972 Color: 1
Size: 284623 Color: 1

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 391346 Color: 1
Size: 330118 Color: 0
Size: 278537 Color: 1

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 391497 Color: 0
Size: 335128 Color: 1
Size: 273376 Color: 0

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 391484 Color: 1
Size: 321867 Color: 1
Size: 286650 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 391516 Color: 0
Size: 354670 Color: 0
Size: 253815 Color: 1

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 391610 Color: 1
Size: 313801 Color: 1
Size: 294590 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 391563 Color: 0
Size: 343335 Color: 0
Size: 265103 Color: 1

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 391595 Color: 0
Size: 343657 Color: 1
Size: 264749 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 391654 Color: 1
Size: 314104 Color: 0
Size: 294243 Color: 1

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 391798 Color: 0
Size: 327744 Color: 1
Size: 280459 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 391745 Color: 1
Size: 342593 Color: 0
Size: 265663 Color: 1

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 391995 Color: 0
Size: 330684 Color: 0
Size: 277322 Color: 1

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 391882 Color: 1
Size: 315854 Color: 0
Size: 292265 Color: 1

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 392002 Color: 0
Size: 338931 Color: 0
Size: 269068 Color: 1

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 0
Size: 335096 Color: 1
Size: 272751 Color: 1

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 392188 Color: 0
Size: 312923 Color: 1
Size: 294890 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 392167 Color: 1
Size: 312545 Color: 0
Size: 295289 Color: 1

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 392330 Color: 0
Size: 327581 Color: 0
Size: 280090 Color: 1

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 392361 Color: 0
Size: 311951 Color: 1
Size: 295689 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 392421 Color: 0
Size: 355233 Color: 1
Size: 252347 Color: 1

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 392427 Color: 0
Size: 326563 Color: 1
Size: 281011 Color: 0

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 392478 Color: 0
Size: 340767 Color: 1
Size: 266756 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 392506 Color: 1
Size: 337029 Color: 1
Size: 270466 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 392513 Color: 0
Size: 331014 Color: 1
Size: 276474 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 392668 Color: 0
Size: 342095 Color: 0
Size: 265238 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 392682 Color: 1
Size: 330391 Color: 1
Size: 276928 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 392769 Color: 0
Size: 330204 Color: 0
Size: 277028 Color: 1

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 392784 Color: 1
Size: 313879 Color: 0
Size: 293338 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 392784 Color: 1
Size: 349279 Color: 1
Size: 257938 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 392983 Color: 1
Size: 320265 Color: 0
Size: 286753 Color: 1

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 393087 Color: 0
Size: 319625 Color: 1
Size: 287289 Color: 1

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 393179 Color: 0
Size: 339221 Color: 1
Size: 267601 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 393136 Color: 1
Size: 312441 Color: 0
Size: 294424 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 393188 Color: 1
Size: 347996 Color: 1
Size: 258817 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 393232 Color: 0
Size: 310999 Color: 0
Size: 295770 Color: 1

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 393254 Color: 1
Size: 332211 Color: 0
Size: 274536 Color: 1

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 393332 Color: 0
Size: 344748 Color: 1
Size: 261921 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 393519 Color: 0
Size: 345794 Color: 1
Size: 260688 Color: 1

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 393532 Color: 0
Size: 355270 Color: 1
Size: 251199 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 393423 Color: 1
Size: 323426 Color: 0
Size: 283152 Color: 1

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 393566 Color: 0
Size: 355355 Color: 1
Size: 251080 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 393689 Color: 0
Size: 304093 Color: 1
Size: 302219 Color: 1

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 393743 Color: 0
Size: 322646 Color: 1
Size: 283612 Color: 1

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 393888 Color: 1
Size: 326726 Color: 0
Size: 279387 Color: 1

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 393937 Color: 0
Size: 308506 Color: 0
Size: 297558 Color: 1

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 393901 Color: 1
Size: 320850 Color: 1
Size: 285250 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 394053 Color: 1
Size: 330620 Color: 0
Size: 275328 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 393964 Color: 0
Size: 350398 Color: 0
Size: 255639 Color: 1

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 394137 Color: 1
Size: 355003 Color: 1
Size: 250861 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 394179 Color: 1
Size: 345658 Color: 1
Size: 260164 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 394325 Color: 1
Size: 331378 Color: 0
Size: 274298 Color: 1

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 394359 Color: 1
Size: 336296 Color: 0
Size: 269346 Color: 1

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 394306 Color: 0
Size: 320324 Color: 0
Size: 285371 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 394456 Color: 1
Size: 316888 Color: 0
Size: 288657 Color: 1

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 394582 Color: 1
Size: 316846 Color: 0
Size: 288573 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 394605 Color: 0
Size: 334396 Color: 0
Size: 271000 Color: 1

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 394743 Color: 1
Size: 329503 Color: 0
Size: 275755 Color: 1

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 394698 Color: 0
Size: 305663 Color: 1
Size: 299640 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 394762 Color: 1
Size: 324327 Color: 0
Size: 280912 Color: 1

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 394758 Color: 0
Size: 319643 Color: 0
Size: 285600 Color: 1

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 394903 Color: 1
Size: 345390 Color: 1
Size: 259708 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 394810 Color: 0
Size: 322258 Color: 0
Size: 282933 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 394912 Color: 1
Size: 353723 Color: 0
Size: 251366 Color: 1

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 394923 Color: 1
Size: 317951 Color: 0
Size: 287127 Color: 0

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 395143 Color: 0
Size: 335113 Color: 1
Size: 269745 Color: 1

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 395223 Color: 0
Size: 331312 Color: 0
Size: 273466 Color: 1

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 395128 Color: 1
Size: 353813 Color: 0
Size: 251060 Color: 1

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 395138 Color: 1
Size: 328836 Color: 0
Size: 276027 Color: 1

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 395350 Color: 0
Size: 311400 Color: 1
Size: 293251 Color: 1

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 395386 Color: 0
Size: 312592 Color: 1
Size: 292023 Color: 1

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 395455 Color: 0
Size: 325511 Color: 1
Size: 279035 Color: 1

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 395369 Color: 1
Size: 314242 Color: 1
Size: 290390 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 395456 Color: 0
Size: 319512 Color: 1
Size: 285033 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 395550 Color: 0
Size: 320366 Color: 1
Size: 284085 Color: 1

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 395567 Color: 0
Size: 333569 Color: 0
Size: 270865 Color: 1

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 395602 Color: 0
Size: 344172 Color: 1
Size: 260227 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 395629 Color: 0
Size: 321243 Color: 1
Size: 283129 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 395615 Color: 1
Size: 346028 Color: 0
Size: 258358 Color: 1

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 395641 Color: 0
Size: 317306 Color: 1
Size: 287054 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 395706 Color: 1
Size: 343975 Color: 1
Size: 260320 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 395697 Color: 0
Size: 305690 Color: 0
Size: 298614 Color: 1

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 395819 Color: 0
Size: 324902 Color: 0
Size: 279280 Color: 1

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 363126 Color: 0
Size: 344639 Color: 0
Size: 292236 Color: 1

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 396022 Color: 0
Size: 306259 Color: 0
Size: 297720 Color: 1

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 396222 Color: 1
Size: 347448 Color: 1
Size: 256331 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 396369 Color: 1
Size: 335095 Color: 0
Size: 268537 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 396443 Color: 0
Size: 324292 Color: 0
Size: 279266 Color: 1

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 396545 Color: 0
Size: 326633 Color: 0
Size: 276823 Color: 1

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 396554 Color: 0
Size: 310274 Color: 1
Size: 293173 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 396510 Color: 1
Size: 345857 Color: 0
Size: 257634 Color: 1

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 396630 Color: 0
Size: 346516 Color: 1
Size: 256855 Color: 1

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 396636 Color: 0
Size: 316569 Color: 1
Size: 286796 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 396582 Color: 1
Size: 325796 Color: 1
Size: 277623 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 396652 Color: 0
Size: 302266 Color: 0
Size: 301083 Color: 1

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 396643 Color: 1
Size: 329214 Color: 1
Size: 274144 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 396764 Color: 0
Size: 312348 Color: 1
Size: 290889 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 396742 Color: 1
Size: 306043 Color: 0
Size: 297216 Color: 1

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 396954 Color: 0
Size: 303851 Color: 1
Size: 299196 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 397087 Color: 0
Size: 319202 Color: 1
Size: 283712 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 397125 Color: 0
Size: 335614 Color: 1
Size: 267262 Color: 1

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 397180 Color: 0
Size: 337715 Color: 0
Size: 265106 Color: 1

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 397141 Color: 1
Size: 336703 Color: 1
Size: 266157 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 397284 Color: 1
Size: 309462 Color: 1
Size: 293255 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 397362 Color: 1
Size: 319183 Color: 1
Size: 283456 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 397368 Color: 1
Size: 339286 Color: 0
Size: 263347 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 397421 Color: 1
Size: 341254 Color: 0
Size: 261326 Color: 1

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 397448 Color: 1
Size: 333991 Color: 0
Size: 268562 Color: 1

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 397434 Color: 0
Size: 317150 Color: 0
Size: 285417 Color: 1

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 397491 Color: 0
Size: 302393 Color: 1
Size: 300117 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 397571 Color: 0
Size: 346200 Color: 1
Size: 256230 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 397632 Color: 1
Size: 315865 Color: 0
Size: 286504 Color: 1

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 397664 Color: 1
Size: 338602 Color: 0
Size: 263735 Color: 1

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 397741 Color: 0
Size: 313879 Color: 1
Size: 288381 Color: 1

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 397835 Color: 1
Size: 305656 Color: 0
Size: 296510 Color: 1

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 397799 Color: 0
Size: 313738 Color: 1
Size: 288464 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 397892 Color: 0
Size: 326524 Color: 0
Size: 275585 Color: 1

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 397970 Color: 1
Size: 341242 Color: 1
Size: 260789 Color: 0

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 398127 Color: 1
Size: 304997 Color: 1
Size: 296877 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 398203 Color: 1
Size: 349181 Color: 0
Size: 252617 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 398336 Color: 1
Size: 311553 Color: 0
Size: 290112 Color: 1

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 398354 Color: 1
Size: 333951 Color: 0
Size: 267696 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 398354 Color: 1
Size: 330394 Color: 0
Size: 271253 Color: 1

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 398358 Color: 1
Size: 322894 Color: 0
Size: 278749 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 398384 Color: 1
Size: 308139 Color: 1
Size: 293478 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 398378 Color: 0
Size: 337450 Color: 1
Size: 264173 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 398403 Color: 1
Size: 326118 Color: 1
Size: 275480 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 398444 Color: 0
Size: 305727 Color: 1
Size: 295830 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 398526 Color: 1
Size: 308783 Color: 0
Size: 292692 Color: 1

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 398511 Color: 0
Size: 319752 Color: 1
Size: 281738 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 398547 Color: 0
Size: 325204 Color: 1
Size: 276250 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 398632 Color: 0
Size: 314276 Color: 1
Size: 287093 Color: 0

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 398664 Color: 1
Size: 302178 Color: 0
Size: 299159 Color: 1

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 398764 Color: 0
Size: 342019 Color: 1
Size: 259218 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 398746 Color: 1
Size: 339990 Color: 1
Size: 261265 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 398805 Color: 1
Size: 302814 Color: 0
Size: 298382 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 398786 Color: 0
Size: 306396 Color: 1
Size: 294819 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 398874 Color: 1
Size: 317706 Color: 0
Size: 283421 Color: 1

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 398888 Color: 1
Size: 308841 Color: 1
Size: 292272 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 398920 Color: 0
Size: 331659 Color: 0
Size: 269422 Color: 1

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 398892 Color: 1
Size: 335801 Color: 0
Size: 265308 Color: 1

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 398964 Color: 0
Size: 303272 Color: 1
Size: 297765 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 399028 Color: 0
Size: 334895 Color: 0
Size: 266078 Color: 1

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 399314 Color: 1
Size: 309132 Color: 0
Size: 291555 Color: 1

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 399358 Color: 0
Size: 320785 Color: 1
Size: 279858 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 399612 Color: 0
Size: 344372 Color: 1
Size: 256017 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 399643 Color: 0
Size: 331405 Color: 1
Size: 268953 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 399843 Color: 1
Size: 329188 Color: 1
Size: 270970 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 399926 Color: 1
Size: 347593 Color: 0
Size: 252482 Color: 1

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 399985 Color: 0
Size: 319470 Color: 1
Size: 280546 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 400082 Color: 1
Size: 346721 Color: 0
Size: 253198 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 400238 Color: 0
Size: 303190 Color: 1
Size: 296573 Color: 1

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 400358 Color: 0
Size: 335890 Color: 1
Size: 263753 Color: 1

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 400466 Color: 1
Size: 323667 Color: 0
Size: 275868 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 400631 Color: 1
Size: 342480 Color: 1
Size: 256890 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 400478 Color: 0
Size: 347273 Color: 0
Size: 252250 Color: 1

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 400732 Color: 1
Size: 327558 Color: 1
Size: 271711 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 400570 Color: 0
Size: 348799 Color: 1
Size: 250632 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 400828 Color: 1
Size: 341112 Color: 1
Size: 258061 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 400677 Color: 0
Size: 303756 Color: 1
Size: 295568 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 400778 Color: 0
Size: 308336 Color: 0
Size: 290887 Color: 1

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 401016 Color: 0
Size: 311961 Color: 1
Size: 287024 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 401026 Color: 1
Size: 321402 Color: 1
Size: 277573 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 401106 Color: 0
Size: 318249 Color: 1
Size: 280646 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 401287 Color: 1
Size: 347766 Color: 1
Size: 250948 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 401314 Color: 1
Size: 306539 Color: 0
Size: 292148 Color: 1

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 401271 Color: 0
Size: 335363 Color: 0
Size: 263367 Color: 1

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 401383 Color: 1
Size: 321457 Color: 1
Size: 277161 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 401417 Color: 1
Size: 317521 Color: 0
Size: 281063 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 401333 Color: 0
Size: 312142 Color: 1
Size: 286526 Color: 0

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 401455 Color: 1
Size: 320256 Color: 0
Size: 278290 Color: 1

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 401756 Color: 0
Size: 337577 Color: 0
Size: 260668 Color: 1

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 401646 Color: 1
Size: 327741 Color: 1
Size: 270614 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 401872 Color: 0
Size: 317000 Color: 0
Size: 281129 Color: 1

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 401916 Color: 0
Size: 306387 Color: 0
Size: 291698 Color: 1

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 401791 Color: 1
Size: 334987 Color: 0
Size: 263223 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 401924 Color: 0
Size: 309915 Color: 0
Size: 288162 Color: 1

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 401978 Color: 0
Size: 303006 Color: 1
Size: 295017 Color: 1

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 402011 Color: 0
Size: 301444 Color: 0
Size: 296546 Color: 1

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 401941 Color: 1
Size: 328895 Color: 0
Size: 269165 Color: 1

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 402016 Color: 0
Size: 342352 Color: 1
Size: 255633 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 402028 Color: 0
Size: 327004 Color: 1
Size: 270969 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 402008 Color: 1
Size: 334176 Color: 0
Size: 263817 Color: 1

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 402148 Color: 1
Size: 298934 Color: 0
Size: 298919 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 402149 Color: 1
Size: 321211 Color: 0
Size: 276641 Color: 1

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 402168 Color: 1
Size: 347812 Color: 0
Size: 250021 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 402194 Color: 0
Size: 326831 Color: 0
Size: 270976 Color: 1

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 402296 Color: 1
Size: 333398 Color: 1
Size: 264307 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 402415 Color: 1
Size: 313518 Color: 0
Size: 284068 Color: 1

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 402522 Color: 1
Size: 334246 Color: 1
Size: 263233 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 402432 Color: 0
Size: 338648 Color: 1
Size: 258921 Color: 0

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 402531 Color: 1
Size: 324430 Color: 1
Size: 273040 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 402480 Color: 0
Size: 301423 Color: 1
Size: 296098 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 402614 Color: 1
Size: 335151 Color: 0
Size: 262236 Color: 1

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 402716 Color: 1
Size: 315315 Color: 0
Size: 281970 Color: 1

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 402694 Color: 0
Size: 317545 Color: 1
Size: 279762 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 402865 Color: 1
Size: 334646 Color: 0
Size: 262490 Color: 1

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 403257 Color: 0
Size: 314200 Color: 0
Size: 282544 Color: 1

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 403270 Color: 0
Size: 327322 Color: 1
Size: 269409 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 403251 Color: 1
Size: 337664 Color: 1
Size: 259086 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 403315 Color: 0
Size: 317664 Color: 1
Size: 279022 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 403291 Color: 1
Size: 304777 Color: 0
Size: 291933 Color: 1

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 403340 Color: 0
Size: 341944 Color: 1
Size: 254717 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 403541 Color: 1
Size: 324924 Color: 0
Size: 271536 Color: 1

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 403782 Color: 0
Size: 338254 Color: 0
Size: 257965 Color: 1

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 403710 Color: 1
Size: 332859 Color: 0
Size: 263432 Color: 1

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 403877 Color: 1
Size: 308456 Color: 1
Size: 287668 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 404073 Color: 0
Size: 338018 Color: 1
Size: 257910 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 404138 Color: 0
Size: 339062 Color: 1
Size: 256801 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 404117 Color: 1
Size: 326683 Color: 1
Size: 269201 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 404211 Color: 0
Size: 312885 Color: 1
Size: 282905 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 404221 Color: 0
Size: 310533 Color: 0
Size: 285247 Color: 1

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 404246 Color: 0
Size: 330530 Color: 0
Size: 265225 Color: 1

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 404289 Color: 0
Size: 315877 Color: 1
Size: 279835 Color: 1

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 404376 Color: 0
Size: 345370 Color: 1
Size: 250255 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 404463 Color: 1
Size: 308831 Color: 0
Size: 286707 Color: 1

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 404488 Color: 0
Size: 300744 Color: 0
Size: 294769 Color: 1

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 404523 Color: 1
Size: 340082 Color: 1
Size: 255396 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 404590 Color: 1
Size: 317623 Color: 1
Size: 277788 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 404549 Color: 0
Size: 333753 Color: 1
Size: 261699 Color: 0

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 404817 Color: 0
Size: 332687 Color: 1
Size: 262497 Color: 1

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 404908 Color: 0
Size: 327854 Color: 0
Size: 267239 Color: 1

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 405051 Color: 0
Size: 344223 Color: 0
Size: 250727 Color: 1

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 405078 Color: 0
Size: 305148 Color: 0
Size: 289775 Color: 1

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 405188 Color: 1
Size: 327955 Color: 0
Size: 266858 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 405205 Color: 1
Size: 323753 Color: 1
Size: 271043 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 405146 Color: 0
Size: 334032 Color: 0
Size: 260823 Color: 1

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 405213 Color: 1
Size: 334547 Color: 1
Size: 260241 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 405216 Color: 1
Size: 312537 Color: 0
Size: 282248 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 405265 Color: 1
Size: 342153 Color: 0
Size: 252583 Color: 1

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 405321 Color: 1
Size: 323348 Color: 1
Size: 271332 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 405467 Color: 0
Size: 298356 Color: 1
Size: 296178 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 405541 Color: 1
Size: 300961 Color: 0
Size: 293499 Color: 1

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 405718 Color: 0
Size: 339065 Color: 1
Size: 255218 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 405639 Color: 1
Size: 340195 Color: 1
Size: 254167 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 406146 Color: 1
Size: 338224 Color: 1
Size: 255631 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 406211 Color: 1
Size: 321166 Color: 0
Size: 272624 Color: 1

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 406260 Color: 1
Size: 328801 Color: 0
Size: 264940 Color: 1

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 406323 Color: 1
Size: 310681 Color: 1
Size: 282997 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 406388 Color: 1
Size: 342617 Color: 0
Size: 250996 Color: 1

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 406460 Color: 1
Size: 309953 Color: 0
Size: 283588 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 406700 Color: 0
Size: 311027 Color: 1
Size: 282274 Color: 1

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 406720 Color: 0
Size: 320277 Color: 0
Size: 273004 Color: 1

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 406683 Color: 1
Size: 316490 Color: 0
Size: 276828 Color: 1

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 406770 Color: 0
Size: 336871 Color: 0
Size: 256360 Color: 1

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 406818 Color: 1
Size: 334711 Color: 0
Size: 258472 Color: 1

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 406905 Color: 1
Size: 317733 Color: 1
Size: 275363 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 406909 Color: 1
Size: 297624 Color: 0
Size: 295468 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 407094 Color: 1
Size: 306578 Color: 1
Size: 286329 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 407217 Color: 1
Size: 334679 Color: 1
Size: 258105 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 407267 Color: 1
Size: 328899 Color: 0
Size: 263835 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 407361 Color: 1
Size: 336836 Color: 1
Size: 255804 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 407539 Color: 1
Size: 305881 Color: 0
Size: 286581 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 407636 Color: 1
Size: 315037 Color: 0
Size: 277328 Color: 1

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 407823 Color: 0
Size: 339268 Color: 0
Size: 252910 Color: 1

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 407859 Color: 0
Size: 317119 Color: 0
Size: 275023 Color: 1

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 407806 Color: 1
Size: 321530 Color: 0
Size: 270665 Color: 1

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 407997 Color: 0
Size: 314235 Color: 1
Size: 277769 Color: 0

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 407870 Color: 1
Size: 339122 Color: 1
Size: 253009 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 408075 Color: 0
Size: 329720 Color: 0
Size: 262206 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 408187 Color: 0
Size: 334839 Color: 1
Size: 256975 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 408238 Color: 0
Size: 299603 Color: 1
Size: 292160 Color: 1

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 408238 Color: 0
Size: 332925 Color: 1
Size: 258838 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 408300 Color: 0
Size: 338098 Color: 1
Size: 253603 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 408417 Color: 0
Size: 301437 Color: 1
Size: 290147 Color: 1

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 408662 Color: 1
Size: 319708 Color: 0
Size: 271631 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 408678 Color: 0
Size: 327685 Color: 1
Size: 263638 Color: 1

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 408690 Color: 0
Size: 320279 Color: 0
Size: 271032 Color: 1

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 408713 Color: 0
Size: 303966 Color: 0
Size: 287322 Color: 1

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 408842 Color: 0
Size: 341034 Color: 1
Size: 250125 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 408943 Color: 0
Size: 316292 Color: 1
Size: 274766 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 409112 Color: 1
Size: 322377 Color: 0
Size: 268512 Color: 1

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 409035 Color: 0
Size: 314461 Color: 1
Size: 276505 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 409144 Color: 1
Size: 313559 Color: 0
Size: 277298 Color: 1

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 409145 Color: 1
Size: 332426 Color: 0
Size: 258430 Color: 1

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 409089 Color: 0
Size: 311999 Color: 0
Size: 278913 Color: 1

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 409179 Color: 0
Size: 305662 Color: 0
Size: 285160 Color: 1

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 409187 Color: 1
Size: 313565 Color: 1
Size: 277249 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 409250 Color: 1
Size: 320735 Color: 0
Size: 270016 Color: 1

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 409453 Color: 1
Size: 319803 Color: 0
Size: 270745 Color: 1

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 409489 Color: 1
Size: 327617 Color: 0
Size: 262895 Color: 1

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 409523 Color: 1
Size: 334358 Color: 1
Size: 256120 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 409661 Color: 0
Size: 329716 Color: 0
Size: 260624 Color: 1

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 409821 Color: 0
Size: 331038 Color: 1
Size: 259142 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 409710 Color: 1
Size: 333707 Color: 1
Size: 256584 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 409829 Color: 0
Size: 316076 Color: 0
Size: 274096 Color: 1

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 409826 Color: 1
Size: 335232 Color: 1
Size: 254943 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 409867 Color: 0
Size: 301013 Color: 1
Size: 289121 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 409896 Color: 0
Size: 327078 Color: 0
Size: 263027 Color: 1

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 410000 Color: 1
Size: 335258 Color: 1
Size: 254743 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 410017 Color: 0
Size: 306886 Color: 1
Size: 283098 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 410028 Color: 0
Size: 301234 Color: 1
Size: 288739 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 410032 Color: 0
Size: 296522 Color: 1
Size: 293447 Color: 1

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 410095 Color: 0
Size: 295196 Color: 1
Size: 294710 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 410233 Color: 1
Size: 319969 Color: 1
Size: 269799 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 410249 Color: 0
Size: 295791 Color: 1
Size: 293961 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 410341 Color: 0
Size: 339165 Color: 0
Size: 250495 Color: 1

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 410435 Color: 1
Size: 319301 Color: 0
Size: 270265 Color: 1

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 410373 Color: 0
Size: 328153 Color: 1
Size: 261475 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 410403 Color: 0
Size: 320449 Color: 0
Size: 269149 Color: 1

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 410503 Color: 1
Size: 297107 Color: 1
Size: 292391 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 410464 Color: 0
Size: 328400 Color: 0
Size: 261137 Color: 1

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 410562 Color: 1
Size: 314403 Color: 1
Size: 275036 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 410487 Color: 0
Size: 323432 Color: 0
Size: 266082 Color: 1

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 410630 Color: 1
Size: 330635 Color: 0
Size: 258736 Color: 1

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 410561 Color: 0
Size: 338429 Color: 1
Size: 251011 Color: 0

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 410757 Color: 0
Size: 308126 Color: 1
Size: 281118 Color: 1

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 410910 Color: 1
Size: 306443 Color: 0
Size: 282648 Color: 1

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 410991 Color: 1
Size: 325469 Color: 1
Size: 263541 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 411016 Color: 0
Size: 326508 Color: 0
Size: 262477 Color: 1

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 411046 Color: 1
Size: 296851 Color: 0
Size: 292104 Color: 1

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 411316 Color: 1
Size: 324424 Color: 1
Size: 264261 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 411317 Color: 0
Size: 300234 Color: 1
Size: 288450 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 411322 Color: 1
Size: 299987 Color: 1
Size: 288692 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 411545 Color: 1
Size: 301810 Color: 1
Size: 286646 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 411615 Color: 0
Size: 307708 Color: 0
Size: 280678 Color: 1

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 411734 Color: 0
Size: 302151 Color: 1
Size: 286116 Color: 1

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 411757 Color: 0
Size: 323209 Color: 1
Size: 265035 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 411783 Color: 1
Size: 303548 Color: 0
Size: 284670 Color: 1

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 411978 Color: 1
Size: 297487 Color: 1
Size: 290536 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 412041 Color: 1
Size: 320874 Color: 0
Size: 267086 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 412044 Color: 1
Size: 306955 Color: 0
Size: 281002 Color: 1

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 412140 Color: 1
Size: 337645 Color: 0
Size: 250216 Color: 0

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 412174 Color: 1
Size: 305272 Color: 0
Size: 282555 Color: 1

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 412192 Color: 1
Size: 305551 Color: 0
Size: 282258 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 412315 Color: 1
Size: 318533 Color: 0
Size: 269153 Color: 1

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 412321 Color: 0
Size: 322475 Color: 1
Size: 265205 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 412571 Color: 0
Size: 322198 Color: 1
Size: 265232 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 412537 Color: 1
Size: 299751 Color: 1
Size: 287713 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 412720 Color: 0
Size: 333453 Color: 1
Size: 253828 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 412731 Color: 0
Size: 294103 Color: 1
Size: 293167 Color: 1

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 412745 Color: 0
Size: 305848 Color: 1
Size: 281408 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 412729 Color: 1
Size: 328740 Color: 0
Size: 258532 Color: 1

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 412952 Color: 1
Size: 319248 Color: 0
Size: 267801 Color: 1

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 412959 Color: 1
Size: 308768 Color: 1
Size: 278274 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 413003 Color: 0
Size: 298740 Color: 0
Size: 288258 Color: 1

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 413331 Color: 0
Size: 328772 Color: 1
Size: 257898 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 413232 Color: 1
Size: 318386 Color: 1
Size: 268383 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 413414 Color: 0
Size: 311016 Color: 1
Size: 275571 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 413441 Color: 0
Size: 312694 Color: 1
Size: 273866 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 413504 Color: 1
Size: 333957 Color: 0
Size: 252540 Color: 0

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 413511 Color: 1
Size: 305845 Color: 1
Size: 280645 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 413516 Color: 0
Size: 332571 Color: 1
Size: 253914 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 413602 Color: 1
Size: 322908 Color: 0
Size: 263491 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 413652 Color: 1
Size: 295671 Color: 1
Size: 290678 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 354574 Color: 0
Size: 336699 Color: 1
Size: 308728 Color: 1

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 413878 Color: 0
Size: 294021 Color: 0
Size: 292102 Color: 1

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 413936 Color: 0
Size: 324729 Color: 1
Size: 261336 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 413998 Color: 1
Size: 332166 Color: 1
Size: 253837 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 413942 Color: 0
Size: 326267 Color: 1
Size: 259792 Color: 0

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 413956 Color: 0
Size: 315379 Color: 1
Size: 270666 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 414100 Color: 1
Size: 307653 Color: 0
Size: 278248 Color: 1

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 414279 Color: 1
Size: 332394 Color: 1
Size: 253328 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 414239 Color: 0
Size: 323352 Color: 0
Size: 262410 Color: 1

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 414544 Color: 0
Size: 334539 Color: 1
Size: 250918 Color: 1

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 414592 Color: 0
Size: 297546 Color: 1
Size: 287863 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 414604 Color: 0
Size: 333101 Color: 0
Size: 252296 Color: 1

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 414785 Color: 0
Size: 323995 Color: 0
Size: 261221 Color: 1

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 415037 Color: 1
Size: 307807 Color: 0
Size: 277157 Color: 1

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 415029 Color: 0
Size: 331200 Color: 0
Size: 253772 Color: 1

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 415093 Color: 1
Size: 321048 Color: 1
Size: 263860 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 415227 Color: 0
Size: 328195 Color: 1
Size: 256579 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 415228 Color: 0
Size: 318063 Color: 1
Size: 266710 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 415264 Color: 0
Size: 303487 Color: 1
Size: 281250 Color: 1

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 415308 Color: 0
Size: 293514 Color: 0
Size: 291179 Color: 1

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 415217 Color: 1
Size: 292455 Color: 0
Size: 292329 Color: 1

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 415260 Color: 1
Size: 316092 Color: 0
Size: 268649 Color: 1

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 415298 Color: 1
Size: 303994 Color: 0
Size: 280709 Color: 1

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 415324 Color: 1
Size: 333143 Color: 0
Size: 251534 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 415473 Color: 0
Size: 308264 Color: 0
Size: 276264 Color: 1

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 415578 Color: 0
Size: 333926 Color: 1
Size: 250497 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 415716 Color: 1
Size: 304927 Color: 1
Size: 279358 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 415616 Color: 0
Size: 298038 Color: 1
Size: 286347 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 415718 Color: 0
Size: 293619 Color: 0
Size: 290664 Color: 1

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 415857 Color: 1
Size: 330330 Color: 0
Size: 253814 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 415979 Color: 1
Size: 311363 Color: 0
Size: 272659 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 416113 Color: 1
Size: 310742 Color: 0
Size: 273146 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 416257 Color: 1
Size: 302128 Color: 1
Size: 281616 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 416353 Color: 0
Size: 301357 Color: 0
Size: 282291 Color: 1

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 416398 Color: 1
Size: 320884 Color: 1
Size: 262719 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 416457 Color: 1
Size: 324400 Color: 1
Size: 259144 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 416505 Color: 0
Size: 297565 Color: 1
Size: 285931 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 416589 Color: 0
Size: 331493 Color: 1
Size: 251919 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 416618 Color: 0
Size: 303437 Color: 1
Size: 279946 Color: 1

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 416665 Color: 0
Size: 293533 Color: 0
Size: 289803 Color: 1

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 416718 Color: 1
Size: 332814 Color: 1
Size: 250469 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 416796 Color: 0
Size: 310532 Color: 0
Size: 272673 Color: 1

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 416830 Color: 0
Size: 309339 Color: 0
Size: 273832 Color: 1

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 416814 Color: 1
Size: 302196 Color: 0
Size: 280991 Color: 1

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 416839 Color: 0
Size: 324947 Color: 1
Size: 258215 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 416998 Color: 1
Size: 331588 Color: 0
Size: 251415 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 417020 Color: 1
Size: 303679 Color: 0
Size: 279302 Color: 1

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 417197 Color: 1
Size: 327022 Color: 1
Size: 255782 Color: 0

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 417201 Color: 0
Size: 321440 Color: 0
Size: 261360 Color: 1

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 417204 Color: 1
Size: 301934 Color: 1
Size: 280863 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 417342 Color: 0
Size: 295971 Color: 0
Size: 286688 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 417376 Color: 0
Size: 329534 Color: 1
Size: 253091 Color: 1

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 417482 Color: 1
Size: 298078 Color: 0
Size: 284441 Color: 1

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 417543 Color: 0
Size: 302735 Color: 1
Size: 279723 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 417595 Color: 0
Size: 305590 Color: 1
Size: 276816 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 417644 Color: 1
Size: 297230 Color: 1
Size: 285127 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 417622 Color: 0
Size: 314307 Color: 0
Size: 268072 Color: 1

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 417867 Color: 0
Size: 317946 Color: 1
Size: 264188 Color: 1

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 417899 Color: 1
Size: 294338 Color: 1
Size: 287764 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 417972 Color: 1
Size: 308589 Color: 1
Size: 273440 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 418106 Color: 1
Size: 308113 Color: 1
Size: 273782 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 418074 Color: 0
Size: 321280 Color: 0
Size: 260647 Color: 1

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 418302 Color: 1
Size: 317801 Color: 0
Size: 263898 Color: 1

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 418221 Color: 0
Size: 298853 Color: 1
Size: 282927 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 418346 Color: 1
Size: 314072 Color: 0
Size: 267583 Color: 1

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 418283 Color: 0
Size: 322185 Color: 0
Size: 259533 Color: 1

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 418485 Color: 1
Size: 298930 Color: 0
Size: 282586 Color: 1

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 418496 Color: 1
Size: 293711 Color: 0
Size: 287794 Color: 1

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 418593 Color: 0
Size: 314011 Color: 0
Size: 267397 Color: 1

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 418625 Color: 0
Size: 331043 Color: 1
Size: 250333 Color: 1

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 418728 Color: 1
Size: 314840 Color: 0
Size: 266433 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 418831 Color: 0
Size: 292881 Color: 1
Size: 288289 Color: 1

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 418851 Color: 0
Size: 298241 Color: 1
Size: 282909 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 418883 Color: 0
Size: 328087 Color: 1
Size: 253031 Color: 1

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 419063 Color: 0
Size: 313461 Color: 1
Size: 267477 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 418999 Color: 1
Size: 315441 Color: 1
Size: 265561 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 419307 Color: 0
Size: 312373 Color: 1
Size: 268321 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 419334 Color: 1
Size: 325816 Color: 1
Size: 254851 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 419462 Color: 1
Size: 311529 Color: 0
Size: 269010 Color: 1

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 419502 Color: 0
Size: 310848 Color: 1
Size: 269651 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 419478 Color: 1
Size: 304612 Color: 1
Size: 275911 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 419510 Color: 1
Size: 300176 Color: 0
Size: 280315 Color: 1

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 419544 Color: 0
Size: 307857 Color: 1
Size: 272600 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 419672 Color: 1
Size: 308614 Color: 0
Size: 271715 Color: 1

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 419740 Color: 1
Size: 323629 Color: 1
Size: 256632 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 419878 Color: 1
Size: 327573 Color: 0
Size: 252550 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 419880 Color: 1
Size: 304841 Color: 1
Size: 275280 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 419798 Color: 0
Size: 318415 Color: 1
Size: 261788 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 419912 Color: 0
Size: 319185 Color: 1
Size: 260904 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 419964 Color: 0
Size: 319445 Color: 1
Size: 260592 Color: 1

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 420059 Color: 0
Size: 311262 Color: 1
Size: 268680 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 420488 Color: 1
Size: 315831 Color: 0
Size: 263682 Color: 1

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 420560 Color: 1
Size: 293028 Color: 0
Size: 286413 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 420580 Color: 1
Size: 315196 Color: 1
Size: 264225 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 420600 Color: 0
Size: 292615 Color: 1
Size: 286786 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 420684 Color: 0
Size: 304084 Color: 1
Size: 275233 Color: 1

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 420820 Color: 1
Size: 304051 Color: 0
Size: 275130 Color: 0

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 420826 Color: 1
Size: 322680 Color: 0
Size: 256495 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 420951 Color: 1
Size: 303161 Color: 0
Size: 275889 Color: 1

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 420964 Color: 0
Size: 298184 Color: 0
Size: 280853 Color: 1

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 421088 Color: 1
Size: 307593 Color: 0
Size: 271320 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 421205 Color: 0
Size: 295331 Color: 1
Size: 283465 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 421097 Color: 1
Size: 315864 Color: 1
Size: 263040 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 421260 Color: 0
Size: 293896 Color: 1
Size: 284845 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 421545 Color: 0
Size: 308594 Color: 1
Size: 269862 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 421641 Color: 1
Size: 306075 Color: 1
Size: 272285 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 421578 Color: 0
Size: 325158 Color: 0
Size: 253265 Color: 1

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 421771 Color: 0
Size: 323653 Color: 1
Size: 254577 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 421957 Color: 0
Size: 293462 Color: 1
Size: 284582 Color: 0

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 421987 Color: 1
Size: 297570 Color: 1
Size: 280444 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 422041 Color: 0
Size: 327221 Color: 0
Size: 250739 Color: 1

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 422053 Color: 0
Size: 314843 Color: 1
Size: 263105 Color: 1

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 422092 Color: 0
Size: 309404 Color: 0
Size: 268505 Color: 1

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 422171 Color: 1
Size: 303880 Color: 0
Size: 273950 Color: 1

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 422100 Color: 0
Size: 308576 Color: 1
Size: 269325 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 422117 Color: 0
Size: 309833 Color: 0
Size: 268051 Color: 1

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 422268 Color: 1
Size: 316077 Color: 1
Size: 261656 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 422394 Color: 1
Size: 326501 Color: 0
Size: 251106 Color: 1

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 422542 Color: 1
Size: 317181 Color: 1
Size: 260278 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 422520 Color: 0
Size: 311201 Color: 1
Size: 266280 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 422676 Color: 1
Size: 292249 Color: 1
Size: 285076 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 422837 Color: 1
Size: 291826 Color: 0
Size: 285338 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 422848 Color: 1
Size: 292942 Color: 0
Size: 284211 Color: 1

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 422982 Color: 1
Size: 307061 Color: 1
Size: 269958 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 423043 Color: 1
Size: 302809 Color: 0
Size: 274149 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 423212 Color: 0
Size: 291034 Color: 1
Size: 285755 Color: 1

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 423216 Color: 0
Size: 323418 Color: 0
Size: 253367 Color: 1

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 423276 Color: 0
Size: 293635 Color: 1
Size: 283090 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 423300 Color: 1
Size: 308478 Color: 1
Size: 268223 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 423364 Color: 1
Size: 322470 Color: 0
Size: 254167 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 423297 Color: 0
Size: 292400 Color: 0
Size: 284304 Color: 1

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 423467 Color: 1
Size: 293872 Color: 1
Size: 282662 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 423370 Color: 0
Size: 314838 Color: 1
Size: 261793 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 423493 Color: 0
Size: 323247 Color: 1
Size: 253261 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 423582 Color: 0
Size: 289674 Color: 1
Size: 286745 Color: 0

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 423671 Color: 1
Size: 295882 Color: 0
Size: 280448 Color: 1

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 423609 Color: 0
Size: 298302 Color: 1
Size: 278090 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 423724 Color: 1
Size: 311274 Color: 0
Size: 265003 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 423920 Color: 0
Size: 315593 Color: 1
Size: 260488 Color: 0

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 424012 Color: 1
Size: 293756 Color: 0
Size: 282233 Color: 1

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 424101 Color: 1
Size: 292709 Color: 0
Size: 283191 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 424192 Color: 1
Size: 290908 Color: 0
Size: 284901 Color: 1

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 424124 Color: 0
Size: 313060 Color: 0
Size: 262817 Color: 1

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 424317 Color: 0
Size: 300343 Color: 0
Size: 275341 Color: 1

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 424388 Color: 0
Size: 292122 Color: 0
Size: 283491 Color: 1

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 424402 Color: 0
Size: 310637 Color: 1
Size: 264962 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 424592 Color: 0
Size: 307072 Color: 1
Size: 268337 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 424772 Color: 0
Size: 287835 Color: 1
Size: 287394 Color: 1

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 424673 Color: 1
Size: 320172 Color: 0
Size: 255156 Color: 1

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 424869 Color: 0
Size: 324937 Color: 1
Size: 250195 Color: 1

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 424874 Color: 0
Size: 315998 Color: 0
Size: 259129 Color: 1

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 424904 Color: 0
Size: 292365 Color: 0
Size: 282732 Color: 1

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 425001 Color: 0
Size: 299793 Color: 1
Size: 275207 Color: 1

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 425082 Color: 1
Size: 291860 Color: 0
Size: 283059 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 425105 Color: 1
Size: 290890 Color: 0
Size: 284006 Color: 1

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 425134 Color: 1
Size: 314906 Color: 0
Size: 259961 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 425140 Color: 1
Size: 289934 Color: 0
Size: 284927 Color: 1

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 425159 Color: 1
Size: 322847 Color: 0
Size: 251995 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 425189 Color: 1
Size: 318431 Color: 0
Size: 256381 Color: 1

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 425137 Color: 0
Size: 322623 Color: 1
Size: 252241 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 425290 Color: 0
Size: 311927 Color: 0
Size: 262784 Color: 1

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 425390 Color: 1
Size: 291395 Color: 1
Size: 283216 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 425343 Color: 0
Size: 287874 Color: 1
Size: 286784 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 425432 Color: 0
Size: 297568 Color: 1
Size: 277001 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 425534 Color: 1
Size: 298998 Color: 0
Size: 275469 Color: 1

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 425604 Color: 1
Size: 310314 Color: 1
Size: 264083 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 425580 Color: 0
Size: 306915 Color: 0
Size: 267506 Color: 1

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 425630 Color: 0
Size: 302700 Color: 0
Size: 271671 Color: 1

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 425781 Color: 0
Size: 309863 Color: 0
Size: 264357 Color: 1

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 425811 Color: 0
Size: 305365 Color: 1
Size: 268825 Color: 1

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 425798 Color: 1
Size: 307528 Color: 1
Size: 266675 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 425889 Color: 0
Size: 290045 Color: 0
Size: 284067 Color: 1

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 426136 Color: 1
Size: 299990 Color: 1
Size: 273875 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 426096 Color: 0
Size: 294030 Color: 1
Size: 279875 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 426244 Color: 1
Size: 292812 Color: 0
Size: 280945 Color: 1

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 426440 Color: 1
Size: 291257 Color: 0
Size: 282304 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 426654 Color: 0
Size: 310617 Color: 1
Size: 262730 Color: 1

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 426666 Color: 0
Size: 309500 Color: 1
Size: 263835 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 426781 Color: 0
Size: 322405 Color: 0
Size: 250815 Color: 1

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 426935 Color: 1
Size: 317573 Color: 0
Size: 255493 Color: 0

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 427002 Color: 1
Size: 320811 Color: 1
Size: 252188 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 427026 Color: 0
Size: 315649 Color: 1
Size: 257326 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 427188 Color: 1
Size: 287500 Color: 0
Size: 285313 Color: 1

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 427249 Color: 1
Size: 320247 Color: 1
Size: 252505 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 427322 Color: 0
Size: 314381 Color: 1
Size: 258298 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 427323 Color: 1
Size: 320014 Color: 1
Size: 252664 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 427477 Color: 1
Size: 310522 Color: 0
Size: 262002 Color: 1

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 427396 Color: 0
Size: 307641 Color: 1
Size: 264964 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 427576 Color: 1
Size: 291528 Color: 0
Size: 280897 Color: 1

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 427756 Color: 0
Size: 292436 Color: 1
Size: 279809 Color: 1

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 427807 Color: 0
Size: 311703 Color: 0
Size: 260491 Color: 1

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 427850 Color: 0
Size: 306053 Color: 1
Size: 266098 Color: 1

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 427955 Color: 0
Size: 299759 Color: 0
Size: 272287 Color: 1

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 428043 Color: 1
Size: 321755 Color: 1
Size: 250203 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 428137 Color: 0
Size: 302412 Color: 1
Size: 269452 Color: 1

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 428155 Color: 0
Size: 296081 Color: 0
Size: 275765 Color: 1

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 428162 Color: 0
Size: 299272 Color: 1
Size: 272567 Color: 1

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 428243 Color: 1
Size: 293267 Color: 0
Size: 278491 Color: 1

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 428265 Color: 0
Size: 286232 Color: 0
Size: 285504 Color: 1

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 428250 Color: 1
Size: 298148 Color: 0
Size: 273603 Color: 1

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 428327 Color: 0
Size: 302108 Color: 1
Size: 269566 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 428281 Color: 1
Size: 291610 Color: 0
Size: 280110 Color: 1

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 428303 Color: 1
Size: 291786 Color: 0
Size: 279912 Color: 1

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 428412 Color: 0
Size: 298190 Color: 1
Size: 273399 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 428399 Color: 1
Size: 301571 Color: 1
Size: 270031 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 428414 Color: 0
Size: 287634 Color: 1
Size: 283953 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 428436 Color: 1
Size: 310408 Color: 1
Size: 261157 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 428837 Color: 1
Size: 308790 Color: 0
Size: 262374 Color: 1

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 428863 Color: 1
Size: 305095 Color: 0
Size: 266043 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 428875 Color: 1
Size: 289618 Color: 1
Size: 281508 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 428959 Color: 0
Size: 301388 Color: 0
Size: 269654 Color: 1

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 428884 Color: 1
Size: 296653 Color: 1
Size: 274464 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 428945 Color: 1
Size: 290517 Color: 1
Size: 280539 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 429008 Color: 0
Size: 308341 Color: 1
Size: 262652 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 429009 Color: 1
Size: 310273 Color: 0
Size: 260719 Color: 1

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 429088 Color: 0
Size: 289397 Color: 0
Size: 281516 Color: 1

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 429235 Color: 0
Size: 311691 Color: 1
Size: 259075 Color: 1

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 429246 Color: 1
Size: 319579 Color: 0
Size: 251176 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 429387 Color: 1
Size: 301568 Color: 0
Size: 269046 Color: 1

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 429576 Color: 0
Size: 314103 Color: 0
Size: 256322 Color: 1

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 429746 Color: 1
Size: 317579 Color: 0
Size: 252676 Color: 1

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 429649 Color: 0
Size: 301753 Color: 1
Size: 268599 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 429871 Color: 1
Size: 303775 Color: 0
Size: 266355 Color: 1

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 429836 Color: 0
Size: 290190 Color: 1
Size: 279975 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 429979 Color: 1
Size: 303040 Color: 0
Size: 266982 Color: 1

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 429882 Color: 0
Size: 288741 Color: 1
Size: 281378 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 430009 Color: 1
Size: 295641 Color: 1
Size: 274351 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 429980 Color: 0
Size: 288621 Color: 1
Size: 281400 Color: 0

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 430066 Color: 0
Size: 286394 Color: 1
Size: 283541 Color: 1

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 430132 Color: 1
Size: 295512 Color: 1
Size: 274357 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 430162 Color: 0
Size: 300872 Color: 1
Size: 268967 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 430256 Color: 0
Size: 310981 Color: 0
Size: 258764 Color: 1

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 430360 Color: 0
Size: 297868 Color: 1
Size: 271773 Color: 1

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 430432 Color: 0
Size: 284947 Color: 1
Size: 284622 Color: 1

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 430436 Color: 0
Size: 290368 Color: 0
Size: 279197 Color: 1

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 430458 Color: 0
Size: 306312 Color: 1
Size: 263231 Color: 1

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 430465 Color: 0
Size: 302766 Color: 1
Size: 266770 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 430526 Color: 0
Size: 304653 Color: 1
Size: 264822 Color: 1

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 430576 Color: 1
Size: 297227 Color: 1
Size: 272198 Color: 0

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 430618 Color: 0
Size: 291340 Color: 1
Size: 278043 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 430701 Color: 1
Size: 298974 Color: 0
Size: 270326 Color: 1

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 430730 Color: 1
Size: 288257 Color: 0
Size: 281014 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 430736 Color: 1
Size: 290821 Color: 0
Size: 278444 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 430969 Color: 0
Size: 295832 Color: 1
Size: 273200 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 430926 Color: 1
Size: 312607 Color: 1
Size: 256468 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 431272 Color: 0
Size: 295476 Color: 1
Size: 273253 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 431309 Color: 0
Size: 301585 Color: 1
Size: 267107 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 431459 Color: 0
Size: 308937 Color: 0
Size: 259605 Color: 1

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 431512 Color: 0
Size: 285998 Color: 1
Size: 282491 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 431642 Color: 1
Size: 301150 Color: 1
Size: 267209 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 431559 Color: 0
Size: 293063 Color: 1
Size: 275379 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 431753 Color: 1
Size: 290107 Color: 1
Size: 278141 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 431794 Color: 1
Size: 302439 Color: 1
Size: 265768 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 431864 Color: 1
Size: 317054 Color: 0
Size: 251083 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 432018 Color: 0
Size: 299425 Color: 1
Size: 268558 Color: 1

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 432089 Color: 0
Size: 295838 Color: 0
Size: 272074 Color: 1

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 432100 Color: 0
Size: 296138 Color: 1
Size: 271763 Color: 1

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 432106 Color: 0
Size: 293618 Color: 1
Size: 274277 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 432097 Color: 1
Size: 301851 Color: 0
Size: 266053 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 432118 Color: 0
Size: 298261 Color: 0
Size: 269622 Color: 1

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 432270 Color: 0
Size: 293873 Color: 0
Size: 273858 Color: 1

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 432257 Color: 1
Size: 316232 Color: 1
Size: 251512 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 432425 Color: 1
Size: 296700 Color: 1
Size: 270876 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 432616 Color: 1
Size: 307677 Color: 0
Size: 259708 Color: 1

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 432784 Color: 0
Size: 285232 Color: 0
Size: 281985 Color: 1

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 432794 Color: 1
Size: 297044 Color: 1
Size: 270163 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 432878 Color: 0
Size: 285472 Color: 1
Size: 281651 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 432957 Color: 0
Size: 304649 Color: 1
Size: 262395 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 433002 Color: 1
Size: 291728 Color: 0
Size: 275271 Color: 1

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 433120 Color: 1
Size: 286913 Color: 0
Size: 279968 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 433335 Color: 1
Size: 296704 Color: 1
Size: 269962 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 433340 Color: 0
Size: 293102 Color: 1
Size: 273559 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 433403 Color: 1
Size: 300956 Color: 1
Size: 265642 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 433463 Color: 1
Size: 286146 Color: 0
Size: 280392 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 433599 Color: 1
Size: 300907 Color: 0
Size: 265495 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 433828 Color: 1
Size: 301230 Color: 0
Size: 264943 Color: 1

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 434413 Color: 0
Size: 298256 Color: 1
Size: 267332 Color: 1

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 434472 Color: 0
Size: 312777 Color: 0
Size: 252752 Color: 1

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 434533 Color: 0
Size: 311051 Color: 0
Size: 254417 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 434944 Color: 1
Size: 285835 Color: 1
Size: 279222 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 435046 Color: 0
Size: 288350 Color: 0
Size: 276605 Color: 1

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 435164 Color: 1
Size: 305116 Color: 0
Size: 259721 Color: 1

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 435440 Color: 1
Size: 312628 Color: 1
Size: 251933 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 435684 Color: 0
Size: 296822 Color: 1
Size: 267495 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 435759 Color: 0
Size: 304587 Color: 1
Size: 259655 Color: 1

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 435761 Color: 0
Size: 289898 Color: 0
Size: 274342 Color: 1

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 436001 Color: 1
Size: 284342 Color: 0
Size: 279658 Color: 1

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 435954 Color: 0
Size: 304456 Color: 1
Size: 259591 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 436021 Color: 1
Size: 310843 Color: 1
Size: 253137 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 436032 Color: 1
Size: 307946 Color: 0
Size: 256023 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 436057 Color: 1
Size: 296939 Color: 0
Size: 267005 Color: 1

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 436067 Color: 0
Size: 308726 Color: 1
Size: 255208 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 436420 Color: 1
Size: 286127 Color: 1
Size: 277454 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 436429 Color: 1
Size: 299085 Color: 1
Size: 264487 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 436739 Color: 1
Size: 302704 Color: 0
Size: 260558 Color: 1

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 436816 Color: 1
Size: 293058 Color: 0
Size: 270127 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 436835 Color: 1
Size: 304865 Color: 0
Size: 258301 Color: 1

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 436851 Color: 0
Size: 284436 Color: 1
Size: 278714 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 436863 Color: 0
Size: 289503 Color: 1
Size: 273635 Color: 0

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 436996 Color: 1
Size: 311081 Color: 0
Size: 251924 Color: 1

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 437000 Color: 1
Size: 284040 Color: 0
Size: 278961 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 437057 Color: 1
Size: 289991 Color: 0
Size: 272953 Color: 1

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 437076 Color: 0
Size: 312925 Color: 1
Size: 250000 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 437192 Color: 0
Size: 310028 Color: 1
Size: 252781 Color: 1

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 437205 Color: 0
Size: 298663 Color: 1
Size: 264133 Color: 1

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 437350 Color: 0
Size: 301041 Color: 0
Size: 261610 Color: 1

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 437427 Color: 1
Size: 282869 Color: 1
Size: 279705 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 437452 Color: 1
Size: 296838 Color: 0
Size: 265711 Color: 0

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 437569 Color: 1
Size: 286736 Color: 0
Size: 275696 Color: 1

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 437873 Color: 0
Size: 286814 Color: 1
Size: 275314 Color: 1

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 437865 Color: 1
Size: 309351 Color: 1
Size: 252785 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 437945 Color: 0
Size: 308928 Color: 1
Size: 253128 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 438102 Color: 1
Size: 304689 Color: 0
Size: 257210 Color: 1

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 438191 Color: 0
Size: 287543 Color: 0
Size: 274267 Color: 1

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 438220 Color: 1
Size: 303915 Color: 0
Size: 257866 Color: 1

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 438325 Color: 0
Size: 285113 Color: 0
Size: 276563 Color: 1

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 438407 Color: 0
Size: 300705 Color: 1
Size: 260889 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 438501 Color: 1
Size: 292414 Color: 1
Size: 269086 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 438607 Color: 1
Size: 300244 Color: 0
Size: 261150 Color: 1

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 438693 Color: 1
Size: 301940 Color: 0
Size: 259368 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 438913 Color: 1
Size: 298564 Color: 0
Size: 262524 Color: 1

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 438918 Color: 0
Size: 282371 Color: 0
Size: 278712 Color: 1

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 439124 Color: 0
Size: 285326 Color: 0
Size: 275551 Color: 1

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 439259 Color: 0
Size: 307134 Color: 0
Size: 253608 Color: 1

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 439269 Color: 0
Size: 308620 Color: 1
Size: 252112 Color: 1

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 439301 Color: 1
Size: 296134 Color: 0
Size: 264566 Color: 1

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 439443 Color: 1
Size: 300033 Color: 0
Size: 260525 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 439589 Color: 0
Size: 297770 Color: 1
Size: 262642 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 439650 Color: 1
Size: 286849 Color: 0
Size: 273502 Color: 1

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 439593 Color: 0
Size: 297746 Color: 0
Size: 262662 Color: 1

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 439718 Color: 0
Size: 307822 Color: 1
Size: 252461 Color: 1

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 439802 Color: 0
Size: 301221 Color: 1
Size: 258978 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 439929 Color: 0
Size: 292557 Color: 1
Size: 267515 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 440003 Color: 1
Size: 281451 Color: 0
Size: 278547 Color: 1

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 440134 Color: 0
Size: 291279 Color: 1
Size: 268588 Color: 1

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 440120 Color: 1
Size: 301390 Color: 0
Size: 258491 Color: 1

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 440193 Color: 0
Size: 307736 Color: 1
Size: 252072 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 440284 Color: 0
Size: 291203 Color: 1
Size: 268514 Color: 1

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 440372 Color: 0
Size: 288399 Color: 1
Size: 271230 Color: 0

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 440461 Color: 0
Size: 298983 Color: 1
Size: 260557 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 440644 Color: 0
Size: 309246 Color: 0
Size: 250111 Color: 1

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 440557 Color: 1
Size: 306516 Color: 0
Size: 252928 Color: 1

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 440670 Color: 0
Size: 283872 Color: 0
Size: 275459 Color: 1

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 440691 Color: 1
Size: 291899 Color: 1
Size: 267411 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 440760 Color: 1
Size: 293493 Color: 1
Size: 265748 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 440917 Color: 0
Size: 308949 Color: 1
Size: 250135 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 441017 Color: 0
Size: 300096 Color: 1
Size: 258888 Color: 1

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 440985 Color: 1
Size: 279597 Color: 0
Size: 279419 Color: 1

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 441054 Color: 0
Size: 296421 Color: 0
Size: 262526 Color: 1

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 441092 Color: 1
Size: 282005 Color: 1
Size: 276904 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 441541 Color: 1
Size: 293992 Color: 0
Size: 264468 Color: 1

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 441875 Color: 1
Size: 300543 Color: 1
Size: 257583 Color: 0

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 441897 Color: 1
Size: 303072 Color: 0
Size: 255032 Color: 1

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 441865 Color: 0
Size: 297025 Color: 0
Size: 261111 Color: 1

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 442064 Color: 1
Size: 284086 Color: 0
Size: 273851 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 442101 Color: 1
Size: 301105 Color: 1
Size: 256795 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 442211 Color: 1
Size: 290738 Color: 0
Size: 267052 Color: 1

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 442272 Color: 0
Size: 297897 Color: 0
Size: 259832 Color: 1

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 442432 Color: 0
Size: 286773 Color: 1
Size: 270796 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 442520 Color: 0
Size: 298069 Color: 1
Size: 259412 Color: 0

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 442577 Color: 1
Size: 287220 Color: 0
Size: 270204 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 442761 Color: 1
Size: 300199 Color: 0
Size: 257041 Color: 1

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 442746 Color: 0
Size: 294821 Color: 0
Size: 262434 Color: 1

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 442849 Color: 0
Size: 299047 Color: 1
Size: 258105 Color: 1

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 442827 Color: 1
Size: 285988 Color: 1
Size: 271186 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 442906 Color: 0
Size: 301313 Color: 1
Size: 255782 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 442890 Color: 1
Size: 302052 Color: 0
Size: 255059 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 442910 Color: 1
Size: 284651 Color: 1
Size: 272440 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 443106 Color: 0
Size: 299224 Color: 0
Size: 257671 Color: 1

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 443355 Color: 1
Size: 303982 Color: 0
Size: 252664 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 443358 Color: 1
Size: 288334 Color: 1
Size: 268309 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 443426 Color: 1
Size: 291629 Color: 0
Size: 264946 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 443564 Color: 1
Size: 289839 Color: 0
Size: 266598 Color: 1

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 443582 Color: 1
Size: 305594 Color: 0
Size: 250825 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 443704 Color: 1
Size: 302153 Color: 0
Size: 254144 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 443907 Color: 0
Size: 279948 Color: 1
Size: 276146 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 443986 Color: 0
Size: 279761 Color: 1
Size: 276254 Color: 1

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 443990 Color: 0
Size: 285328 Color: 1
Size: 270683 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 443970 Color: 1
Size: 285066 Color: 0
Size: 270965 Color: 1

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 443997 Color: 1
Size: 298235 Color: 1
Size: 257769 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 444170 Color: 0
Size: 280980 Color: 1
Size: 274851 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 444217 Color: 0
Size: 279391 Color: 0
Size: 276393 Color: 1

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 444437 Color: 1
Size: 287264 Color: 1
Size: 268300 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 444475 Color: 0
Size: 282143 Color: 0
Size: 273383 Color: 1

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 444544 Color: 0
Size: 301748 Color: 0
Size: 253709 Color: 1

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 444565 Color: 1
Size: 304049 Color: 0
Size: 251387 Color: 1

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 444565 Color: 1
Size: 283223 Color: 1
Size: 272213 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 444698 Color: 0
Size: 291011 Color: 0
Size: 264292 Color: 1

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 444694 Color: 1
Size: 279512 Color: 0
Size: 275795 Color: 1

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 444854 Color: 0
Size: 292318 Color: 1
Size: 262829 Color: 1

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 444824 Color: 1
Size: 284524 Color: 0
Size: 270653 Color: 1

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 444873 Color: 1
Size: 302055 Color: 1
Size: 253073 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 445024 Color: 1
Size: 294683 Color: 1
Size: 260294 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 445138 Color: 1
Size: 301637 Color: 0
Size: 253226 Color: 1

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 445174 Color: 1
Size: 288081 Color: 0
Size: 266746 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 445180 Color: 1
Size: 284817 Color: 1
Size: 270004 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 445282 Color: 0
Size: 301154 Color: 1
Size: 253565 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 445554 Color: 1
Size: 296091 Color: 1
Size: 258356 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 445609 Color: 1
Size: 298787 Color: 0
Size: 255605 Color: 1

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 445987 Color: 0
Size: 285264 Color: 1
Size: 268750 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 445995 Color: 0
Size: 285891 Color: 1
Size: 268115 Color: 1

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 446106 Color: 1
Size: 280477 Color: 0
Size: 273418 Color: 1

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 446016 Color: 0
Size: 289747 Color: 0
Size: 264238 Color: 1

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 446127 Color: 1
Size: 277832 Color: 1
Size: 276042 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 446217 Color: 0
Size: 292001 Color: 0
Size: 261783 Color: 1

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 446191 Color: 1
Size: 292370 Color: 1
Size: 261440 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 446284 Color: 1
Size: 276890 Color: 0
Size: 276827 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 446300 Color: 0
Size: 297220 Color: 0
Size: 256481 Color: 1

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 446557 Color: 1
Size: 287010 Color: 1
Size: 266434 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 446864 Color: 0
Size: 281093 Color: 1
Size: 272044 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 446908 Color: 1
Size: 292063 Color: 0
Size: 261030 Color: 1

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 446983 Color: 0
Size: 289204 Color: 0
Size: 263814 Color: 1

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 446986 Color: 0
Size: 284739 Color: 1
Size: 268276 Color: 1

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 446995 Color: 0
Size: 294538 Color: 0
Size: 258468 Color: 1

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 447078 Color: 0
Size: 294957 Color: 0
Size: 257966 Color: 1

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 447271 Color: 1
Size: 286583 Color: 0
Size: 266147 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 447355 Color: 1
Size: 290046 Color: 1
Size: 262600 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 447520 Color: 1
Size: 288523 Color: 0
Size: 263958 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 447586 Color: 1
Size: 281378 Color: 1
Size: 271037 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 447580 Color: 0
Size: 287311 Color: 0
Size: 265110 Color: 1

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 447591 Color: 1
Size: 296171 Color: 0
Size: 256239 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 447654 Color: 0
Size: 300314 Color: 0
Size: 252033 Color: 1

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 448026 Color: 0
Size: 283404 Color: 1
Size: 268571 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 448065 Color: 1
Size: 285890 Color: 1
Size: 266046 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 448225 Color: 0
Size: 295496 Color: 1
Size: 256280 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 448277 Color: 0
Size: 290670 Color: 0
Size: 261054 Color: 1

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 448293 Color: 1
Size: 279365 Color: 1
Size: 272343 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 448445 Color: 0
Size: 283940 Color: 1
Size: 267616 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 448650 Color: 1
Size: 291727 Color: 0
Size: 259624 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 448673 Color: 1
Size: 291266 Color: 0
Size: 260062 Color: 1

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 448694 Color: 1
Size: 293734 Color: 0
Size: 257573 Color: 0

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 448755 Color: 0
Size: 275654 Color: 1
Size: 275592 Color: 1

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 448760 Color: 0
Size: 279339 Color: 1
Size: 271902 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 448795 Color: 0
Size: 277511 Color: 1
Size: 273695 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 449223 Color: 1
Size: 295119 Color: 0
Size: 255659 Color: 1

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 449275 Color: 0
Size: 294759 Color: 1
Size: 255967 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 449345 Color: 1
Size: 296175 Color: 1
Size: 254481 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 449288 Color: 0
Size: 276713 Color: 1
Size: 274000 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 449416 Color: 0
Size: 277416 Color: 1
Size: 273169 Color: 1

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 449462 Color: 0
Size: 299263 Color: 1
Size: 251276 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 449650 Color: 0
Size: 298390 Color: 0
Size: 251961 Color: 1

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 449856 Color: 1
Size: 286722 Color: 1
Size: 263423 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 449836 Color: 0
Size: 291760 Color: 1
Size: 258405 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 449940 Color: 1
Size: 290874 Color: 0
Size: 259187 Color: 1

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 450405 Color: 0
Size: 289611 Color: 1
Size: 259985 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 450397 Color: 1
Size: 282630 Color: 1
Size: 266974 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 450458 Color: 1
Size: 296736 Color: 0
Size: 252807 Color: 1

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 450541 Color: 0
Size: 288988 Color: 0
Size: 260472 Color: 1

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 450613 Color: 0
Size: 281207 Color: 1
Size: 268181 Color: 1

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 450806 Color: 0
Size: 294206 Color: 1
Size: 254989 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 450841 Color: 1
Size: 287686 Color: 0
Size: 261474 Color: 1

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 450895 Color: 0
Size: 276129 Color: 1
Size: 272977 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 450898 Color: 1
Size: 275339 Color: 0
Size: 273764 Color: 1

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 450937 Color: 0
Size: 281608 Color: 1
Size: 267456 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 450928 Color: 1
Size: 282297 Color: 0
Size: 266776 Color: 1

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 450973 Color: 0
Size: 288757 Color: 0
Size: 260271 Color: 1

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 451002 Color: 1
Size: 294502 Color: 0
Size: 254497 Color: 1

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 451184 Color: 0
Size: 283796 Color: 0
Size: 265021 Color: 1

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 451233 Color: 0
Size: 292102 Color: 1
Size: 256666 Color: 1

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 451245 Color: 0
Size: 295189 Color: 0
Size: 253567 Color: 1

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 451326 Color: 0
Size: 274584 Color: 1
Size: 274091 Color: 1

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 451598 Color: 1
Size: 285486 Color: 0
Size: 262917 Color: 1

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 451574 Color: 0
Size: 278792 Color: 0
Size: 269635 Color: 1

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 451682 Color: 1
Size: 295082 Color: 1
Size: 253237 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 451714 Color: 0
Size: 287585 Color: 1
Size: 260702 Color: 1

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 451797 Color: 0
Size: 274949 Color: 1
Size: 273255 Color: 1

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 451893 Color: 0
Size: 287790 Color: 0
Size: 260318 Color: 1

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 451914 Color: 0
Size: 293542 Color: 0
Size: 254545 Color: 1

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 452335 Color: 0
Size: 291541 Color: 1
Size: 256125 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 452359 Color: 1
Size: 283343 Color: 1
Size: 264299 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 452529 Color: 1
Size: 288094 Color: 0
Size: 259378 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 452561 Color: 1
Size: 290115 Color: 0
Size: 257325 Color: 1

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 452647 Color: 0
Size: 288670 Color: 0
Size: 258684 Color: 1

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 452723 Color: 0
Size: 289766 Color: 1
Size: 257512 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 452666 Color: 1
Size: 280612 Color: 1
Size: 266723 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 452933 Color: 0
Size: 292376 Color: 1
Size: 254692 Color: 1

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 452934 Color: 0
Size: 289290 Color: 1
Size: 257777 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 453160 Color: 1
Size: 276108 Color: 0
Size: 270733 Color: 0

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 453371 Color: 0
Size: 295267 Color: 1
Size: 251363 Color: 1

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 453386 Color: 1
Size: 295717 Color: 0
Size: 250898 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 453597 Color: 1
Size: 273586 Color: 0
Size: 272818 Color: 1

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 454155 Color: 1
Size: 274929 Color: 0
Size: 270917 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 454189 Color: 1
Size: 281227 Color: 0
Size: 264585 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 454282 Color: 0
Size: 293405 Color: 1
Size: 252314 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 454459 Color: 0
Size: 285993 Color: 1
Size: 259549 Color: 1

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 454478 Color: 0
Size: 293739 Color: 0
Size: 251784 Color: 1

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 454514 Color: 1
Size: 278435 Color: 1
Size: 267052 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 454583 Color: 1
Size: 290029 Color: 0
Size: 255389 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 454585 Color: 1
Size: 290045 Color: 1
Size: 255371 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 454754 Color: 0
Size: 276081 Color: 0
Size: 269166 Color: 1

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 454836 Color: 1
Size: 278296 Color: 0
Size: 266869 Color: 1

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 455219 Color: 1
Size: 282326 Color: 0
Size: 262456 Color: 1

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 455195 Color: 0
Size: 273733 Color: 0
Size: 271073 Color: 1

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 455232 Color: 0
Size: 281505 Color: 0
Size: 263264 Color: 1

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 455333 Color: 0
Size: 279035 Color: 0
Size: 265633 Color: 1

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 455434 Color: 1
Size: 284841 Color: 0
Size: 259726 Color: 1

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 455469 Color: 0
Size: 286351 Color: 1
Size: 258181 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 455446 Color: 1
Size: 281026 Color: 1
Size: 263529 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 455601 Color: 0
Size: 283543 Color: 0
Size: 260857 Color: 1

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 455743 Color: 0
Size: 286319 Color: 1
Size: 257939 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 455697 Color: 1
Size: 290344 Color: 0
Size: 253960 Color: 1

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 455926 Color: 0
Size: 284748 Color: 1
Size: 259327 Color: 1

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 456045 Color: 0
Size: 291875 Color: 1
Size: 252081 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 456152 Color: 1
Size: 291703 Color: 1
Size: 252146 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 456166 Color: 1
Size: 291948 Color: 0
Size: 251887 Color: 1

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 456386 Color: 0
Size: 278656 Color: 1
Size: 264959 Color: 1

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 456275 Color: 1
Size: 278230 Color: 0
Size: 265496 Color: 1

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 456330 Color: 1
Size: 292809 Color: 0
Size: 250862 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 456363 Color: 1
Size: 275454 Color: 0
Size: 268184 Color: 1

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 456403 Color: 1
Size: 282624 Color: 0
Size: 260974 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 456434 Color: 1
Size: 286972 Color: 1
Size: 256595 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 456469 Color: 1
Size: 289814 Color: 0
Size: 253718 Color: 1

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 456622 Color: 1
Size: 272334 Color: 0
Size: 271045 Color: 1

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 456742 Color: 1
Size: 275110 Color: 0
Size: 268149 Color: 1

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 456728 Color: 0
Size: 287936 Color: 1
Size: 255337 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 456832 Color: 0
Size: 289916 Color: 0
Size: 253253 Color: 1

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 456866 Color: 1
Size: 288291 Color: 0
Size: 254844 Color: 1

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 456983 Color: 1
Size: 285677 Color: 0
Size: 257341 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 456972 Color: 0
Size: 280496 Color: 1
Size: 262533 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 457037 Color: 1
Size: 289740 Color: 0
Size: 253224 Color: 1

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 457124 Color: 1
Size: 283400 Color: 0
Size: 259477 Color: 1

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 457191 Color: 0
Size: 272808 Color: 1
Size: 270002 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 457401 Color: 1
Size: 277232 Color: 0
Size: 265368 Color: 1

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 457616 Color: 1
Size: 288078 Color: 0
Size: 254307 Color: 1

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 457827 Color: 1
Size: 290978 Color: 0
Size: 251196 Color: 1

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 457971 Color: 0
Size: 284090 Color: 0
Size: 257940 Color: 1

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 458208 Color: 1
Size: 276744 Color: 0
Size: 265049 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 458641 Color: 1
Size: 280922 Color: 0
Size: 260438 Color: 1

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 458927 Color: 0
Size: 284827 Color: 0
Size: 256247 Color: 1

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 459519 Color: 1
Size: 276966 Color: 0
Size: 263516 Color: 1

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 459887 Color: 1
Size: 280622 Color: 0
Size: 259492 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 460101 Color: 1
Size: 275169 Color: 0
Size: 264731 Color: 1

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 460277 Color: 1
Size: 275924 Color: 1
Size: 263800 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 460450 Color: 1
Size: 276412 Color: 0
Size: 263139 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 460481 Color: 1
Size: 278584 Color: 1
Size: 260936 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 460411 Color: 0
Size: 287443 Color: 0
Size: 252147 Color: 1

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 460615 Color: 0
Size: 276383 Color: 1
Size: 263003 Color: 1

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 460624 Color: 0
Size: 287222 Color: 0
Size: 252155 Color: 1

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 460597 Color: 1
Size: 278110 Color: 1
Size: 261294 Color: 0

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 460658 Color: 0
Size: 275454 Color: 1
Size: 263889 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 460698 Color: 0
Size: 284297 Color: 1
Size: 255006 Color: 1

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 460864 Color: 1
Size: 288603 Color: 0
Size: 250534 Color: 1

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 460881 Color: 1
Size: 285089 Color: 0
Size: 254031 Color: 1

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 460857 Color: 0
Size: 279682 Color: 1
Size: 259462 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 461065 Color: 1
Size: 275673 Color: 0
Size: 263263 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 461079 Color: 1
Size: 275430 Color: 1
Size: 263492 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 461202 Color: 0
Size: 277830 Color: 1
Size: 260969 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 461155 Color: 1
Size: 280419 Color: 1
Size: 258427 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 461265 Color: 1
Size: 279996 Color: 0
Size: 258740 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 461293 Color: 1
Size: 273184 Color: 1
Size: 265524 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 461391 Color: 0
Size: 282590 Color: 0
Size: 256020 Color: 1

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 461418 Color: 0
Size: 282667 Color: 1
Size: 255916 Color: 1

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 461475 Color: 1
Size: 283873 Color: 1
Size: 254653 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 461601 Color: 0
Size: 271275 Color: 1
Size: 267125 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 461741 Color: 1
Size: 277099 Color: 1
Size: 261161 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 461958 Color: 1
Size: 273797 Color: 0
Size: 264246 Color: 1

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 462036 Color: 1
Size: 277558 Color: 1
Size: 260407 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 462145 Color: 0
Size: 285433 Color: 0
Size: 252423 Color: 1

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 462147 Color: 1
Size: 277445 Color: 0
Size: 260409 Color: 1

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 462194 Color: 1
Size: 284433 Color: 1
Size: 253374 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 462383 Color: 0
Size: 281691 Color: 1
Size: 255927 Color: 0

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 462438 Color: 0
Size: 284819 Color: 1
Size: 252744 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 462580 Color: 1
Size: 286176 Color: 1
Size: 251245 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 462590 Color: 0
Size: 276675 Color: 1
Size: 260736 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 462719 Color: 1
Size: 282315 Color: 0
Size: 254967 Color: 1

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 462721 Color: 1
Size: 279683 Color: 0
Size: 257597 Color: 1

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 462977 Color: 0
Size: 284562 Color: 1
Size: 252462 Color: 1

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 463110 Color: 0
Size: 271214 Color: 0
Size: 265677 Color: 1

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 463236 Color: 0
Size: 282810 Color: 1
Size: 253955 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 463426 Color: 1
Size: 279433 Color: 0
Size: 257142 Color: 1

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 463555 Color: 1
Size: 274400 Color: 0
Size: 262046 Color: 1

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 463579 Color: 0
Size: 268526 Color: 1
Size: 267896 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 463629 Color: 1
Size: 285394 Color: 1
Size: 250978 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 463990 Color: 0
Size: 273191 Color: 0
Size: 262820 Color: 1

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 464083 Color: 0
Size: 284617 Color: 0
Size: 251301 Color: 1

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 464477 Color: 1
Size: 281784 Color: 0
Size: 253740 Color: 1

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 464533 Color: 1
Size: 282111 Color: 0
Size: 253357 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 464632 Color: 1
Size: 268567 Color: 1
Size: 266802 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 464728 Color: 0
Size: 285241 Color: 0
Size: 250032 Color: 1

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 464708 Color: 1
Size: 282460 Color: 1
Size: 252833 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 464768 Color: 0
Size: 282491 Color: 1
Size: 252742 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 464942 Color: 1
Size: 274597 Color: 0
Size: 260462 Color: 1

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 464952 Color: 0
Size: 278021 Color: 0
Size: 257028 Color: 1

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 465107 Color: 0
Size: 269075 Color: 1
Size: 265819 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 465320 Color: 0
Size: 283779 Color: 0
Size: 250902 Color: 1

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 465231 Color: 1
Size: 279942 Color: 1
Size: 254828 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 465440 Color: 0
Size: 280165 Color: 1
Size: 254396 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 466203 Color: 1
Size: 272281 Color: 1
Size: 261517 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 466155 Color: 0
Size: 282279 Color: 1
Size: 251567 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 466334 Color: 0
Size: 275310 Color: 0
Size: 258357 Color: 1

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 466661 Color: 1
Size: 268597 Color: 0
Size: 264743 Color: 1

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 466702 Color: 1
Size: 282968 Color: 0
Size: 250331 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 466861 Color: 0
Size: 269532 Color: 1
Size: 263608 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 467093 Color: 0
Size: 281144 Color: 1
Size: 251764 Color: 1

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 467323 Color: 1
Size: 277035 Color: 0
Size: 255643 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 467337 Color: 1
Size: 267086 Color: 1
Size: 265578 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 467539 Color: 1
Size: 269425 Color: 1
Size: 263037 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 467596 Color: 0
Size: 281480 Color: 1
Size: 250925 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 467710 Color: 1
Size: 274862 Color: 0
Size: 257429 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 467767 Color: 1
Size: 280974 Color: 0
Size: 251260 Color: 1

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 467799 Color: 1
Size: 270634 Color: 0
Size: 261568 Color: 1

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 467803 Color: 1
Size: 271668 Color: 0
Size: 260530 Color: 1

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 467964 Color: 1
Size: 276060 Color: 1
Size: 255977 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 468051 Color: 1
Size: 268902 Color: 0
Size: 263048 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 468067 Color: 1
Size: 266030 Color: 1
Size: 265904 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 468090 Color: 1
Size: 278120 Color: 0
Size: 253791 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 468107 Color: 1
Size: 277104 Color: 1
Size: 254790 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 468137 Color: 1
Size: 269190 Color: 0
Size: 262674 Color: 1

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 468152 Color: 0
Size: 280967 Color: 0
Size: 250882 Color: 1

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 468359 Color: 0
Size: 273601 Color: 0
Size: 258041 Color: 1

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 468434 Color: 1
Size: 265911 Color: 0
Size: 265656 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 468570 Color: 1
Size: 271697 Color: 0
Size: 259734 Color: 1

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 468663 Color: 0
Size: 266085 Color: 0
Size: 265253 Color: 1

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 469011 Color: 1
Size: 269377 Color: 0
Size: 261613 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 469267 Color: 0
Size: 271504 Color: 1
Size: 259230 Color: 1

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 469280 Color: 0
Size: 275639 Color: 0
Size: 255082 Color: 1

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 469290 Color: 0
Size: 271660 Color: 1
Size: 259051 Color: 0

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 469424 Color: 0
Size: 272575 Color: 1
Size: 258002 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 469427 Color: 1
Size: 280093 Color: 0
Size: 250481 Color: 1

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 469452 Color: 1
Size: 265432 Color: 0
Size: 265117 Color: 1

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 469646 Color: 1
Size: 274265 Color: 0
Size: 256090 Color: 0

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 469792 Color: 1
Size: 274481 Color: 0
Size: 255728 Color: 1

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 469794 Color: 1
Size: 266948 Color: 0
Size: 263259 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 469798 Color: 1
Size: 274909 Color: 0
Size: 255294 Color: 1

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 469971 Color: 0
Size: 272642 Color: 1
Size: 257388 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 470141 Color: 1
Size: 271065 Color: 0
Size: 258795 Color: 1

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 470204 Color: 1
Size: 275556 Color: 0
Size: 254241 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 470310 Color: 1
Size: 268741 Color: 0
Size: 260950 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 470374 Color: 1
Size: 274041 Color: 0
Size: 255586 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 470407 Color: 1
Size: 276824 Color: 0
Size: 252770 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 465639 Color: 1
Size: 279979 Color: 0
Size: 254383 Color: 1

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 470673 Color: 1
Size: 274375 Color: 1
Size: 254953 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 470678 Color: 0
Size: 265624 Color: 0
Size: 263699 Color: 1

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 470710 Color: 1
Size: 270394 Color: 0
Size: 258897 Color: 1

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 471056 Color: 1
Size: 273821 Color: 0
Size: 255124 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 471182 Color: 1
Size: 273679 Color: 0
Size: 255140 Color: 1

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 471247 Color: 0
Size: 266251 Color: 1
Size: 262503 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 471337 Color: 0
Size: 271069 Color: 0
Size: 257595 Color: 1

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 471662 Color: 0
Size: 267920 Color: 1
Size: 260419 Color: 1

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 472063 Color: 0
Size: 271266 Color: 0
Size: 256672 Color: 1

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 472087 Color: 0
Size: 277585 Color: 0
Size: 250329 Color: 1

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 472229 Color: 1
Size: 271343 Color: 1
Size: 256429 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 472482 Color: 1
Size: 274396 Color: 1
Size: 253123 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 472697 Color: 0
Size: 269194 Color: 1
Size: 258110 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 472751 Color: 1
Size: 268370 Color: 0
Size: 258880 Color: 1

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 472886 Color: 1
Size: 265612 Color: 1
Size: 261503 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 472885 Color: 0
Size: 271567 Color: 1
Size: 255549 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 473053 Color: 0
Size: 273811 Color: 1
Size: 253137 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 473154 Color: 0
Size: 276806 Color: 1
Size: 250041 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 473289 Color: 1
Size: 267938 Color: 0
Size: 258774 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 473396 Color: 1
Size: 266224 Color: 1
Size: 260381 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 473421 Color: 1
Size: 274218 Color: 0
Size: 252362 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 473824 Color: 0
Size: 265164 Color: 0
Size: 261013 Color: 1

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 474023 Color: 1
Size: 274777 Color: 0
Size: 251201 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 474224 Color: 0
Size: 274571 Color: 0
Size: 251206 Color: 1

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 474519 Color: 1
Size: 274557 Color: 0
Size: 250925 Color: 1

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 474761 Color: 1
Size: 270534 Color: 0
Size: 254706 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 474770 Color: 0
Size: 266139 Color: 1
Size: 259092 Color: 0

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 475274 Color: 0
Size: 270017 Color: 0
Size: 254710 Color: 1

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 475322 Color: 0
Size: 272022 Color: 1
Size: 252657 Color: 1

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 475373 Color: 0
Size: 265772 Color: 0
Size: 258856 Color: 1

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 475599 Color: 1
Size: 268798 Color: 0
Size: 255604 Color: 1

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 475646 Color: 1
Size: 273218 Color: 0
Size: 251137 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 476016 Color: 1
Size: 267685 Color: 1
Size: 256300 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 476035 Color: 0
Size: 272101 Color: 0
Size: 251865 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 476087 Color: 0
Size: 270081 Color: 1
Size: 253833 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 476243 Color: 1
Size: 271827 Color: 0
Size: 251931 Color: 1

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 476561 Color: 1
Size: 271587 Color: 0
Size: 251853 Color: 1

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 476646 Color: 0
Size: 272801 Color: 0
Size: 250554 Color: 1

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 476653 Color: 1
Size: 270863 Color: 1
Size: 252485 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 476763 Color: 0
Size: 267935 Color: 1
Size: 255303 Color: 0

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 476740 Color: 1
Size: 264162 Color: 1
Size: 259099 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 476988 Color: 0
Size: 269724 Color: 1
Size: 253289 Color: 1

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 477005 Color: 1
Size: 266013 Color: 0
Size: 256983 Color: 1

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 477121 Color: 0
Size: 268139 Color: 1
Size: 254741 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 477182 Color: 1
Size: 270540 Color: 0
Size: 252279 Color: 1

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 477525 Color: 0
Size: 270832 Color: 1
Size: 251644 Color: 1

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 477584 Color: 1
Size: 270564 Color: 0
Size: 251853 Color: 1

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 477753 Color: 0
Size: 268814 Color: 1
Size: 253434 Color: 1

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 477837 Color: 0
Size: 263971 Color: 1
Size: 258193 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 477811 Color: 1
Size: 262623 Color: 1
Size: 259567 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 477861 Color: 1
Size: 262118 Color: 1
Size: 260022 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 477959 Color: 0
Size: 263906 Color: 0
Size: 258136 Color: 1

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 477980 Color: 1
Size: 261177 Color: 1
Size: 260844 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 478054 Color: 0
Size: 261976 Color: 1
Size: 259971 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 478134 Color: 1
Size: 264523 Color: 0
Size: 257344 Color: 1

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 478158 Color: 0
Size: 270640 Color: 0
Size: 251203 Color: 1

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 478280 Color: 1
Size: 270792 Color: 1
Size: 250929 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 478331 Color: 1
Size: 263328 Color: 0
Size: 258342 Color: 1

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 478305 Color: 0
Size: 269785 Color: 1
Size: 251911 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 478467 Color: 1
Size: 264620 Color: 0
Size: 256914 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 478679 Color: 0
Size: 267634 Color: 1
Size: 253688 Color: 1

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 478705 Color: 0
Size: 265550 Color: 1
Size: 255746 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 478648 Color: 1
Size: 266370 Color: 0
Size: 254983 Color: 1

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 478726 Color: 0
Size: 269488 Color: 0
Size: 251787 Color: 1

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 478750 Color: 1
Size: 265676 Color: 0
Size: 255575 Color: 1

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 478996 Color: 0
Size: 264421 Color: 1
Size: 256584 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 479011 Color: 0
Size: 267639 Color: 1
Size: 253351 Color: 1

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 479049 Color: 0
Size: 263343 Color: 0
Size: 257609 Color: 1

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 479129 Color: 0
Size: 261694 Color: 1
Size: 259178 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 479044 Color: 1
Size: 270823 Color: 0
Size: 250134 Color: 1

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 479598 Color: 0
Size: 263099 Color: 1
Size: 257304 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 479612 Color: 1
Size: 270237 Color: 0
Size: 250152 Color: 1

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 479780 Color: 0
Size: 269998 Color: 0
Size: 250223 Color: 1

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 480092 Color: 1
Size: 269828 Color: 0
Size: 250081 Color: 1

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 480117 Color: 0
Size: 264263 Color: 1
Size: 255621 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 480178 Color: 0
Size: 267309 Color: 1
Size: 252514 Color: 1

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 480452 Color: 0
Size: 267731 Color: 1
Size: 251818 Color: 1

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 480554 Color: 1
Size: 262829 Color: 1
Size: 256618 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 480520 Color: 0
Size: 262249 Color: 0
Size: 257232 Color: 1

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 480609 Color: 1
Size: 267961 Color: 0
Size: 251431 Color: 1

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 480655 Color: 0
Size: 264687 Color: 1
Size: 254659 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 480909 Color: 1
Size: 269054 Color: 1
Size: 250038 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 481086 Color: 0
Size: 265876 Color: 1
Size: 253039 Color: 1

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 481094 Color: 0
Size: 264255 Color: 0
Size: 254652 Color: 1

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 481095 Color: 1
Size: 260418 Color: 1
Size: 258488 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 481160 Color: 1
Size: 259982 Color: 0
Size: 258859 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 481193 Color: 1
Size: 266187 Color: 1
Size: 252621 Color: 0

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 481195 Color: 1
Size: 260182 Color: 0
Size: 258624 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 481368 Color: 1
Size: 264953 Color: 1
Size: 253680 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 481487 Color: 1
Size: 259559 Color: 0
Size: 258955 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 481509 Color: 1
Size: 268169 Color: 1
Size: 250323 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 481488 Color: 0
Size: 260435 Color: 0
Size: 258078 Color: 1

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 481584 Color: 1
Size: 264560 Color: 1
Size: 253857 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 481779 Color: 0
Size: 266162 Color: 1
Size: 252060 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 481794 Color: 1
Size: 267877 Color: 0
Size: 250330 Color: 1

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 482222 Color: 0
Size: 259211 Color: 1
Size: 258568 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 482256 Color: 0
Size: 261520 Color: 1
Size: 256225 Color: 1

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 482299 Color: 0
Size: 259129 Color: 0
Size: 258573 Color: 1

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 482263 Color: 1
Size: 261575 Color: 0
Size: 256163 Color: 1

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 482439 Color: 0
Size: 259714 Color: 0
Size: 257848 Color: 1

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 482609 Color: 1
Size: 260294 Color: 0
Size: 257098 Color: 1

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 482768 Color: 1
Size: 264139 Color: 0
Size: 253094 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 482861 Color: 1
Size: 258733 Color: 0
Size: 258407 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 482950 Color: 1
Size: 266830 Color: 0
Size: 250221 Color: 1

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 482983 Color: 1
Size: 261050 Color: 0
Size: 255968 Color: 1

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 483118 Color: 1
Size: 261065 Color: 0
Size: 255818 Color: 0

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 483129 Color: 1
Size: 259367 Color: 1
Size: 257505 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 483095 Color: 0
Size: 259604 Color: 1
Size: 257302 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 483239 Color: 1
Size: 266418 Color: 0
Size: 250344 Color: 1

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 483320 Color: 1
Size: 264717 Color: 0
Size: 251964 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 483671 Color: 0
Size: 260296 Color: 1
Size: 256034 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 483647 Color: 1
Size: 265929 Color: 1
Size: 250425 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 483899 Color: 1
Size: 260349 Color: 0
Size: 255753 Color: 1

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 484037 Color: 0
Size: 260717 Color: 0
Size: 255247 Color: 1

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 484005 Color: 1
Size: 263508 Color: 0
Size: 252488 Color: 1

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 484112 Color: 0
Size: 262968 Color: 0
Size: 252921 Color: 1

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 484204 Color: 1
Size: 265185 Color: 0
Size: 250612 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 484295 Color: 0
Size: 257984 Color: 0
Size: 257722 Color: 1

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 484333 Color: 0
Size: 263059 Color: 1
Size: 252609 Color: 1

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 484513 Color: 0
Size: 261323 Color: 0
Size: 254165 Color: 1

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 484461 Color: 1
Size: 265033 Color: 0
Size: 250507 Color: 1

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 484680 Color: 0
Size: 261347 Color: 1
Size: 253974 Color: 1

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 484718 Color: 0
Size: 264855 Color: 1
Size: 250428 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 484756 Color: 0
Size: 262782 Color: 1
Size: 252463 Color: 1

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 484858 Color: 1
Size: 262706 Color: 0
Size: 252437 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 484886 Color: 1
Size: 261032 Color: 1
Size: 254083 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 484952 Color: 0
Size: 264668 Color: 0
Size: 250381 Color: 1

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 485063 Color: 1
Size: 261963 Color: 0
Size: 252975 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 485309 Color: 1
Size: 258078 Color: 1
Size: 256614 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 485462 Color: 0
Size: 257560 Color: 0
Size: 256979 Color: 1

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 485534 Color: 1
Size: 259317 Color: 1
Size: 255150 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 485588 Color: 0
Size: 263168 Color: 0
Size: 251245 Color: 1

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 485754 Color: 1
Size: 258986 Color: 1
Size: 255261 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 485775 Color: 1
Size: 259562 Color: 0
Size: 254664 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 485776 Color: 1
Size: 259802 Color: 1
Size: 254423 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 485799 Color: 0
Size: 258995 Color: 1
Size: 255207 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 485799 Color: 0
Size: 257733 Color: 0
Size: 256469 Color: 1

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 485881 Color: 0
Size: 264063 Color: 0
Size: 250057 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 485991 Color: 1
Size: 257028 Color: 0
Size: 256982 Color: 1

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 486075 Color: 0
Size: 261435 Color: 0
Size: 252491 Color: 1

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 486185 Color: 1
Size: 262676 Color: 0
Size: 251140 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 486390 Color: 0
Size: 263389 Color: 1
Size: 250222 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 486610 Color: 0
Size: 258411 Color: 0
Size: 254980 Color: 1

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 486618 Color: 0
Size: 263129 Color: 1
Size: 250254 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 486967 Color: 1
Size: 261258 Color: 0
Size: 251776 Color: 1

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 487011 Color: 1
Size: 256955 Color: 0
Size: 256035 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 487051 Color: 1
Size: 258285 Color: 1
Size: 254665 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 487178 Color: 0
Size: 261297 Color: 0
Size: 251526 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 487404 Color: 1
Size: 258998 Color: 0
Size: 253599 Color: 1

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 487471 Color: 0
Size: 258495 Color: 1
Size: 254035 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 487524 Color: 1
Size: 262183 Color: 0
Size: 250294 Color: 1

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 487617 Color: 0
Size: 260571 Color: 1
Size: 251813 Color: 1

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 487588 Color: 1
Size: 258256 Color: 0
Size: 254157 Color: 1

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 487736 Color: 0
Size: 258457 Color: 0
Size: 253808 Color: 1

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 487818 Color: 1
Size: 256130 Color: 0
Size: 256053 Color: 1

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 487802 Color: 0
Size: 262101 Color: 0
Size: 250098 Color: 1

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 487905 Color: 1
Size: 258937 Color: 0
Size: 253159 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 487999 Color: 0
Size: 261387 Color: 1
Size: 250615 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 488251 Color: 0
Size: 258257 Color: 0
Size: 253493 Color: 1

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 488414 Color: 0
Size: 258946 Color: 0
Size: 252641 Color: 1

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 488657 Color: 1
Size: 259936 Color: 0
Size: 251408 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 488905 Color: 1
Size: 256656 Color: 0
Size: 254440 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 488928 Color: 0
Size: 258689 Color: 1
Size: 252384 Color: 1

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 488934 Color: 0
Size: 260162 Color: 1
Size: 250905 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 489069 Color: 0
Size: 259999 Color: 0
Size: 250933 Color: 1

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 489692 Color: 1
Size: 258835 Color: 0
Size: 251474 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 489803 Color: 1
Size: 256722 Color: 0
Size: 253476 Color: 1

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 490110 Color: 1
Size: 255355 Color: 0
Size: 254536 Color: 1

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 490100 Color: 0
Size: 259598 Color: 0
Size: 250303 Color: 1

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 490289 Color: 0
Size: 258873 Color: 0
Size: 250839 Color: 1

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 490319 Color: 0
Size: 254950 Color: 1
Size: 254732 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 490344 Color: 1
Size: 258906 Color: 0
Size: 250751 Color: 1

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 490379 Color: 0
Size: 255148 Color: 0
Size: 254474 Color: 1

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 490420 Color: 1
Size: 256500 Color: 1
Size: 253081 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 490530 Color: 1
Size: 257772 Color: 0
Size: 251699 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 490866 Color: 0
Size: 255878 Color: 1
Size: 253257 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 491003 Color: 0
Size: 258533 Color: 1
Size: 250465 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 491086 Color: 1
Size: 257400 Color: 1
Size: 251515 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 491250 Color: 1
Size: 255510 Color: 0
Size: 253241 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 491349 Color: 0
Size: 258182 Color: 0
Size: 250470 Color: 1

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 491432 Color: 0
Size: 254630 Color: 0
Size: 253939 Color: 1

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 491864 Color: 0
Size: 257245 Color: 0
Size: 250892 Color: 1

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 491929 Color: 0
Size: 255185 Color: 1
Size: 252887 Color: 1

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 491958 Color: 1
Size: 256501 Color: 0
Size: 251542 Color: 1

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 491969 Color: 0
Size: 256586 Color: 0
Size: 251446 Color: 1

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 492042 Color: 1
Size: 256881 Color: 0
Size: 251078 Color: 1

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 492088 Color: 0
Size: 256890 Color: 1
Size: 251023 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 492175 Color: 1
Size: 257404 Color: 1
Size: 250422 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 492261 Color: 0
Size: 255275 Color: 0
Size: 252465 Color: 1

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 492460 Color: 0
Size: 255352 Color: 1
Size: 252189 Color: 1

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 492706 Color: 1
Size: 256324 Color: 0
Size: 250971 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 492847 Color: 0
Size: 257021 Color: 1
Size: 250133 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 493057 Color: 0
Size: 256779 Color: 1
Size: 250165 Color: 1

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 493201 Color: 0
Size: 256256 Color: 0
Size: 250544 Color: 1

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 493232 Color: 0
Size: 253875 Color: 1
Size: 252894 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 493480 Color: 1
Size: 256516 Color: 0
Size: 250005 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 493555 Color: 1
Size: 255914 Color: 0
Size: 250532 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 493852 Color: 1
Size: 253663 Color: 0
Size: 252486 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 494024 Color: 1
Size: 253858 Color: 1
Size: 252119 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 494180 Color: 0
Size: 254428 Color: 1
Size: 251393 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 494151 Color: 1
Size: 254148 Color: 1
Size: 251702 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 494636 Color: 0
Size: 254374 Color: 0
Size: 250991 Color: 1

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 494798 Color: 1
Size: 255116 Color: 0
Size: 250087 Color: 1

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 494901 Color: 0
Size: 254957 Color: 1
Size: 250143 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 494833 Color: 1
Size: 253809 Color: 1
Size: 251359 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 494919 Color: 1
Size: 254568 Color: 0
Size: 250514 Color: 0

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 495048 Color: 1
Size: 254608 Color: 1
Size: 250345 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 495171 Color: 0
Size: 253753 Color: 1
Size: 251077 Color: 1

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 495334 Color: 1
Size: 253512 Color: 0
Size: 251155 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 495384 Color: 1
Size: 253817 Color: 1
Size: 250800 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 495400 Color: 1
Size: 254288 Color: 0
Size: 250313 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 495426 Color: 1
Size: 253931 Color: 1
Size: 250644 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 496208 Color: 1
Size: 252579 Color: 1
Size: 251214 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 496241 Color: 1
Size: 252447 Color: 0
Size: 251313 Color: 1

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 496320 Color: 1
Size: 252924 Color: 1
Size: 250757 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 496569 Color: 1
Size: 251757 Color: 0
Size: 251675 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 496750 Color: 1
Size: 252844 Color: 0
Size: 250407 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 496753 Color: 1
Size: 251980 Color: 0
Size: 251268 Color: 1

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 497078 Color: 0
Size: 252387 Color: 0
Size: 250536 Color: 1

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 497081 Color: 1
Size: 252705 Color: 1
Size: 250215 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 497261 Color: 0
Size: 252222 Color: 1
Size: 250518 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 497903 Color: 0
Size: 251674 Color: 1
Size: 250424 Color: 1

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 497989 Color: 0
Size: 251436 Color: 0
Size: 250576 Color: 1

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 497942 Color: 1
Size: 251072 Color: 1
Size: 250987 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 498082 Color: 0
Size: 251590 Color: 1
Size: 250329 Color: 0

Bin 2892: 1 of cap free
Amount of items: 3
Items: 
Size: 359109 Color: 0
Size: 353789 Color: 1
Size: 287102 Color: 1

Bin 2893: 1 of cap free
Amount of items: 3
Items: 
Size: 476025 Color: 1
Size: 272784 Color: 1
Size: 251191 Color: 0

Bin 2894: 1 of cap free
Amount of items: 3
Items: 
Size: 348410 Color: 0
Size: 335125 Color: 1
Size: 316465 Color: 0

Bin 2895: 1 of cap free
Amount of items: 3
Items: 
Size: 391084 Color: 0
Size: 353990 Color: 1
Size: 254926 Color: 0

Bin 2896: 1 of cap free
Amount of items: 3
Items: 
Size: 460128 Color: 0
Size: 270296 Color: 1
Size: 269576 Color: 0

Bin 2897: 1 of cap free
Amount of items: 3
Items: 
Size: 418504 Color: 1
Size: 331203 Color: 0
Size: 250293 Color: 1

Bin 2898: 1 of cap free
Amount of items: 3
Items: 
Size: 467378 Color: 0
Size: 281946 Color: 1
Size: 250676 Color: 0

Bin 2899: 1 of cap free
Amount of items: 3
Items: 
Size: 371419 Color: 1
Size: 335076 Color: 1
Size: 293505 Color: 0

Bin 2900: 1 of cap free
Amount of items: 3
Items: 
Size: 420123 Color: 0
Size: 328630 Color: 0
Size: 251247 Color: 1

Bin 2901: 1 of cap free
Amount of items: 3
Items: 
Size: 369221 Color: 0
Size: 350876 Color: 0
Size: 279903 Color: 1

Bin 2902: 1 of cap free
Amount of items: 3
Items: 
Size: 344101 Color: 1
Size: 341798 Color: 0
Size: 314101 Color: 1

Bin 2903: 1 of cap free
Amount of items: 3
Items: 
Size: 375392 Color: 0
Size: 346293 Color: 1
Size: 278315 Color: 1

Bin 2904: 1 of cap free
Amount of items: 3
Items: 
Size: 355523 Color: 0
Size: 349664 Color: 1
Size: 294813 Color: 1

Bin 2905: 1 of cap free
Amount of items: 3
Items: 
Size: 354487 Color: 0
Size: 353726 Color: 1
Size: 291787 Color: 0

Bin 2906: 1 of cap free
Amount of items: 3
Items: 
Size: 373483 Color: 0
Size: 359018 Color: 0
Size: 267499 Color: 1

Bin 2907: 1 of cap free
Amount of items: 3
Items: 
Size: 397299 Color: 0
Size: 347077 Color: 0
Size: 255624 Color: 1

Bin 2908: 1 of cap free
Amount of items: 3
Items: 
Size: 350308 Color: 1
Size: 338142 Color: 0
Size: 311550 Color: 1

Bin 2909: 1 of cap free
Amount of items: 3
Items: 
Size: 367997 Color: 0
Size: 353245 Color: 1
Size: 278758 Color: 0

Bin 2910: 1 of cap free
Amount of items: 3
Items: 
Size: 361135 Color: 1
Size: 342296 Color: 0
Size: 296569 Color: 1

Bin 2911: 1 of cap free
Amount of items: 3
Items: 
Size: 363468 Color: 0
Size: 326642 Color: 0
Size: 309890 Color: 1

Bin 2912: 1 of cap free
Amount of items: 3
Items: 
Size: 355462 Color: 0
Size: 322940 Color: 1
Size: 321598 Color: 0

Bin 2913: 1 of cap free
Amount of items: 3
Items: 
Size: 456697 Color: 0
Size: 286792 Color: 1
Size: 256511 Color: 0

Bin 2914: 1 of cap free
Amount of items: 3
Items: 
Size: 469964 Color: 1
Size: 280014 Color: 0
Size: 250022 Color: 0

Bin 2915: 1 of cap free
Amount of items: 3
Items: 
Size: 366460 Color: 0
Size: 344161 Color: 1
Size: 289379 Color: 1

Bin 2916: 1 of cap free
Amount of items: 3
Items: 
Size: 499076 Color: 1
Size: 250480 Color: 0
Size: 250444 Color: 1

Bin 2917: 1 of cap free
Amount of items: 3
Items: 
Size: 368865 Color: 1
Size: 330919 Color: 0
Size: 300216 Color: 0

Bin 2918: 1 of cap free
Amount of items: 3
Items: 
Size: 369473 Color: 0
Size: 348475 Color: 1
Size: 282052 Color: 0

Bin 2919: 1 of cap free
Amount of items: 3
Items: 
Size: 360760 Color: 0
Size: 358803 Color: 0
Size: 280437 Color: 1

Bin 2920: 1 of cap free
Amount of items: 3
Items: 
Size: 356579 Color: 0
Size: 350300 Color: 1
Size: 293121 Color: 1

Bin 2921: 1 of cap free
Amount of items: 3
Items: 
Size: 491402 Color: 1
Size: 256337 Color: 0
Size: 252261 Color: 1

Bin 2922: 1 of cap free
Amount of items: 3
Items: 
Size: 465903 Color: 1
Size: 270119 Color: 0
Size: 263978 Color: 1

Bin 2923: 1 of cap free
Amount of items: 3
Items: 
Size: 384287 Color: 1
Size: 353641 Color: 0
Size: 262072 Color: 0

Bin 2924: 1 of cap free
Amount of items: 3
Items: 
Size: 463358 Color: 1
Size: 271529 Color: 0
Size: 265113 Color: 0

Bin 2925: 1 of cap free
Amount of items: 3
Items: 
Size: 396423 Color: 1
Size: 353386 Color: 1
Size: 250191 Color: 0

Bin 2926: 1 of cap free
Amount of items: 3
Items: 
Size: 362502 Color: 1
Size: 324094 Color: 1
Size: 313404 Color: 0

Bin 2927: 1 of cap free
Amount of items: 3
Items: 
Size: 360511 Color: 0
Size: 350781 Color: 0
Size: 288708 Color: 1

Bin 2928: 1 of cap free
Amount of items: 3
Items: 
Size: 495794 Color: 0
Size: 253067 Color: 0
Size: 251139 Color: 1

Bin 2929: 1 of cap free
Amount of items: 3
Items: 
Size: 363367 Color: 1
Size: 333899 Color: 0
Size: 302734 Color: 0

Bin 2930: 1 of cap free
Amount of items: 3
Items: 
Size: 338665 Color: 0
Size: 331919 Color: 1
Size: 329416 Color: 1

Bin 2931: 1 of cap free
Amount of items: 3
Items: 
Size: 376069 Color: 0
Size: 357470 Color: 1
Size: 266461 Color: 0

Bin 2932: 1 of cap free
Amount of items: 3
Items: 
Size: 353102 Color: 0
Size: 323928 Color: 1
Size: 322970 Color: 1

Bin 2933: 1 of cap free
Amount of items: 3
Items: 
Size: 360344 Color: 0
Size: 331539 Color: 0
Size: 308117 Color: 1

Bin 2934: 1 of cap free
Amount of items: 3
Items: 
Size: 362053 Color: 1
Size: 339925 Color: 0
Size: 298022 Color: 0

Bin 2935: 1 of cap free
Amount of items: 3
Items: 
Size: 382768 Color: 0
Size: 354580 Color: 1
Size: 262652 Color: 0

Bin 2936: 1 of cap free
Amount of items: 3
Items: 
Size: 357854 Color: 1
Size: 327334 Color: 0
Size: 314812 Color: 1

Bin 2937: 1 of cap free
Amount of items: 3
Items: 
Size: 371000 Color: 1
Size: 355548 Color: 0
Size: 273452 Color: 0

Bin 2938: 1 of cap free
Amount of items: 3
Items: 
Size: 359285 Color: 0
Size: 322009 Color: 1
Size: 318706 Color: 0

Bin 2939: 1 of cap free
Amount of items: 3
Items: 
Size: 440500 Color: 1
Size: 286810 Color: 1
Size: 272690 Color: 0

Bin 2940: 1 of cap free
Amount of items: 3
Items: 
Size: 350943 Color: 0
Size: 347419 Color: 0
Size: 301638 Color: 1

Bin 2941: 1 of cap free
Amount of items: 3
Items: 
Size: 348604 Color: 0
Size: 339806 Color: 1
Size: 311590 Color: 0

Bin 2942: 1 of cap free
Amount of items: 3
Items: 
Size: 365131 Color: 1
Size: 344382 Color: 0
Size: 290487 Color: 0

Bin 2943: 1 of cap free
Amount of items: 3
Items: 
Size: 434011 Color: 0
Size: 293559 Color: 1
Size: 272430 Color: 0

Bin 2944: 1 of cap free
Amount of items: 3
Items: 
Size: 359610 Color: 1
Size: 335840 Color: 1
Size: 304550 Color: 0

Bin 2945: 1 of cap free
Amount of items: 3
Items: 
Size: 366048 Color: 1
Size: 359137 Color: 0
Size: 274815 Color: 1

Bin 2946: 1 of cap free
Amount of items: 3
Items: 
Size: 350318 Color: 0
Size: 348157 Color: 1
Size: 301525 Color: 1

Bin 2947: 1 of cap free
Amount of items: 3
Items: 
Size: 357281 Color: 1
Size: 351487 Color: 0
Size: 291232 Color: 1

Bin 2948: 1 of cap free
Amount of items: 3
Items: 
Size: 358846 Color: 0
Size: 321272 Color: 1
Size: 319882 Color: 0

Bin 2949: 1 of cap free
Amount of items: 3
Items: 
Size: 359630 Color: 0
Size: 357298 Color: 1
Size: 283072 Color: 0

Bin 2950: 1 of cap free
Amount of items: 3
Items: 
Size: 387550 Color: 1
Size: 332383 Color: 0
Size: 280067 Color: 0

Bin 2951: 1 of cap free
Amount of items: 3
Items: 
Size: 406580 Color: 1
Size: 335298 Color: 1
Size: 258122 Color: 0

Bin 2952: 1 of cap free
Amount of items: 3
Items: 
Size: 376116 Color: 1
Size: 357941 Color: 0
Size: 265943 Color: 0

Bin 2953: 1 of cap free
Amount of items: 3
Items: 
Size: 350367 Color: 1
Size: 342406 Color: 0
Size: 307227 Color: 1

Bin 2954: 1 of cap free
Amount of items: 3
Items: 
Size: 366519 Color: 1
Size: 347056 Color: 0
Size: 286425 Color: 1

Bin 2955: 1 of cap free
Amount of items: 3
Items: 
Size: 364688 Color: 1
Size: 352296 Color: 1
Size: 283016 Color: 0

Bin 2956: 1 of cap free
Amount of items: 3
Items: 
Size: 361409 Color: 0
Size: 352151 Color: 0
Size: 286440 Color: 1

Bin 2957: 1 of cap free
Amount of items: 3
Items: 
Size: 337797 Color: 1
Size: 332721 Color: 0
Size: 329482 Color: 0

Bin 2958: 1 of cap free
Amount of items: 3
Items: 
Size: 355881 Color: 1
Size: 336009 Color: 1
Size: 308110 Color: 0

Bin 2959: 1 of cap free
Amount of items: 3
Items: 
Size: 377494 Color: 1
Size: 350257 Color: 0
Size: 272249 Color: 1

Bin 2960: 1 of cap free
Amount of items: 3
Items: 
Size: 350217 Color: 0
Size: 339532 Color: 0
Size: 310251 Color: 1

Bin 2961: 1 of cap free
Amount of items: 3
Items: 
Size: 359980 Color: 0
Size: 327964 Color: 1
Size: 312056 Color: 0

Bin 2962: 1 of cap free
Amount of items: 3
Items: 
Size: 367899 Color: 0
Size: 345725 Color: 1
Size: 286376 Color: 0

Bin 2963: 1 of cap free
Amount of items: 3
Items: 
Size: 419998 Color: 0
Size: 296786 Color: 1
Size: 283216 Color: 1

Bin 2964: 1 of cap free
Amount of items: 3
Items: 
Size: 367848 Color: 1
Size: 344941 Color: 0
Size: 287211 Color: 1

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 471957 Color: 0
Size: 276802 Color: 0
Size: 251241 Color: 1

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 494846 Color: 0
Size: 252991 Color: 0
Size: 252163 Color: 1

Bin 2967: 2 of cap free
Amount of items: 3
Items: 
Size: 346886 Color: 1
Size: 331824 Color: 1
Size: 321289 Color: 0

Bin 2968: 2 of cap free
Amount of items: 3
Items: 
Size: 394337 Color: 0
Size: 352582 Color: 0
Size: 253080 Color: 1

Bin 2969: 2 of cap free
Amount of items: 3
Items: 
Size: 497156 Color: 1
Size: 251566 Color: 1
Size: 251277 Color: 0

Bin 2970: 2 of cap free
Amount of items: 3
Items: 
Size: 365115 Color: 1
Size: 327405 Color: 1
Size: 307479 Color: 0

Bin 2971: 2 of cap free
Amount of items: 3
Items: 
Size: 479491 Color: 1
Size: 260454 Color: 0
Size: 260054 Color: 0

Bin 2972: 2 of cap free
Amount of items: 3
Items: 
Size: 441891 Color: 1
Size: 281230 Color: 0
Size: 276878 Color: 0

Bin 2973: 2 of cap free
Amount of items: 3
Items: 
Size: 421439 Color: 1
Size: 307822 Color: 1
Size: 270738 Color: 0

Bin 2974: 2 of cap free
Amount of items: 3
Items: 
Size: 486623 Color: 0
Size: 257235 Color: 1
Size: 256141 Color: 1

Bin 2975: 2 of cap free
Amount of items: 3
Items: 
Size: 467949 Color: 1
Size: 272795 Color: 0
Size: 259255 Color: 1

Bin 2976: 2 of cap free
Amount of items: 3
Items: 
Size: 403012 Color: 1
Size: 331361 Color: 1
Size: 265626 Color: 0

Bin 2977: 2 of cap free
Amount of items: 3
Items: 
Size: 439043 Color: 1
Size: 309836 Color: 0
Size: 251120 Color: 0

Bin 2978: 2 of cap free
Amount of items: 3
Items: 
Size: 356814 Color: 0
Size: 329232 Color: 0
Size: 313953 Color: 1

Bin 2979: 2 of cap free
Amount of items: 3
Items: 
Size: 398826 Color: 0
Size: 343852 Color: 0
Size: 257321 Color: 1

Bin 2980: 2 of cap free
Amount of items: 3
Items: 
Size: 382616 Color: 0
Size: 353734 Color: 0
Size: 263649 Color: 1

Bin 2981: 2 of cap free
Amount of items: 3
Items: 
Size: 424420 Color: 1
Size: 294920 Color: 0
Size: 280659 Color: 0

Bin 2982: 2 of cap free
Amount of items: 3
Items: 
Size: 408259 Color: 0
Size: 337365 Color: 1
Size: 254375 Color: 1

Bin 2983: 2 of cap free
Amount of items: 3
Items: 
Size: 344931 Color: 1
Size: 335238 Color: 1
Size: 319830 Color: 0

Bin 2984: 2 of cap free
Amount of items: 3
Items: 
Size: 477883 Color: 0
Size: 270016 Color: 1
Size: 252100 Color: 0

Bin 2985: 2 of cap free
Amount of items: 3
Items: 
Size: 426870 Color: 1
Size: 293063 Color: 0
Size: 280066 Color: 0

Bin 2986: 2 of cap free
Amount of items: 3
Items: 
Size: 396848 Color: 1
Size: 331323 Color: 0
Size: 271828 Color: 1

Bin 2987: 2 of cap free
Amount of items: 3
Items: 
Size: 345235 Color: 0
Size: 342603 Color: 0
Size: 312161 Color: 1

Bin 2988: 2 of cap free
Amount of items: 3
Items: 
Size: 351551 Color: 0
Size: 349902 Color: 0
Size: 298546 Color: 1

Bin 2989: 2 of cap free
Amount of items: 3
Items: 
Size: 341709 Color: 1
Size: 335097 Color: 1
Size: 323193 Color: 0

Bin 2990: 2 of cap free
Amount of items: 3
Items: 
Size: 375689 Color: 0
Size: 348293 Color: 1
Size: 276017 Color: 0

Bin 2991: 2 of cap free
Amount of items: 3
Items: 
Size: 370923 Color: 0
Size: 356029 Color: 1
Size: 273047 Color: 1

Bin 2992: 2 of cap free
Amount of items: 3
Items: 
Size: 399760 Color: 0
Size: 340378 Color: 1
Size: 259861 Color: 1

Bin 2993: 2 of cap free
Amount of items: 3
Items: 
Size: 368333 Color: 0
Size: 356551 Color: 1
Size: 275115 Color: 1

Bin 2994: 2 of cap free
Amount of items: 3
Items: 
Size: 462848 Color: 0
Size: 274203 Color: 1
Size: 262948 Color: 0

Bin 2995: 2 of cap free
Amount of items: 3
Items: 
Size: 346571 Color: 1
Size: 338596 Color: 0
Size: 314832 Color: 0

Bin 2996: 2 of cap free
Amount of items: 3
Items: 
Size: 355389 Color: 0
Size: 335064 Color: 1
Size: 309546 Color: 0

Bin 2997: 2 of cap free
Amount of items: 3
Items: 
Size: 341801 Color: 0
Size: 330913 Color: 1
Size: 327285 Color: 1

Bin 2998: 2 of cap free
Amount of items: 3
Items: 
Size: 448883 Color: 1
Size: 295530 Color: 0
Size: 255586 Color: 1

Bin 2999: 2 of cap free
Amount of items: 3
Items: 
Size: 351210 Color: 1
Size: 334889 Color: 1
Size: 313900 Color: 0

Bin 3000: 2 of cap free
Amount of items: 3
Items: 
Size: 352252 Color: 1
Size: 327710 Color: 0
Size: 320037 Color: 0

Bin 3001: 2 of cap free
Amount of items: 3
Items: 
Size: 385076 Color: 0
Size: 352979 Color: 1
Size: 261944 Color: 1

Bin 3002: 2 of cap free
Amount of items: 3
Items: 
Size: 498585 Color: 1
Size: 250707 Color: 1
Size: 250707 Color: 0

Bin 3003: 2 of cap free
Amount of items: 3
Items: 
Size: 390309 Color: 0
Size: 308117 Color: 1
Size: 301573 Color: 1

Bin 3004: 2 of cap free
Amount of items: 3
Items: 
Size: 478775 Color: 1
Size: 269875 Color: 0
Size: 251349 Color: 0

Bin 3005: 2 of cap free
Amount of items: 3
Items: 
Size: 427373 Color: 0
Size: 296365 Color: 1
Size: 276261 Color: 1

Bin 3006: 2 of cap free
Amount of items: 3
Items: 
Size: 434659 Color: 1
Size: 298799 Color: 0
Size: 266541 Color: 1

Bin 3007: 2 of cap free
Amount of items: 3
Items: 
Size: 342995 Color: 0
Size: 329648 Color: 1
Size: 327356 Color: 1

Bin 3008: 2 of cap free
Amount of items: 3
Items: 
Size: 365185 Color: 1
Size: 362691 Color: 1
Size: 272123 Color: 0

Bin 3009: 2 of cap free
Amount of items: 3
Items: 
Size: 421321 Color: 1
Size: 308594 Color: 0
Size: 270084 Color: 0

Bin 3010: 2 of cap free
Amount of items: 3
Items: 
Size: 345942 Color: 0
Size: 343871 Color: 1
Size: 310186 Color: 1

Bin 3011: 2 of cap free
Amount of items: 3
Items: 
Size: 471950 Color: 1
Size: 267769 Color: 0
Size: 260280 Color: 0

Bin 3012: 2 of cap free
Amount of items: 3
Items: 
Size: 372762 Color: 1
Size: 356114 Color: 1
Size: 271123 Color: 0

Bin 3013: 2 of cap free
Amount of items: 3
Items: 
Size: 372933 Color: 0
Size: 344264 Color: 1
Size: 282802 Color: 1

Bin 3014: 2 of cap free
Amount of items: 3
Items: 
Size: 484659 Color: 0
Size: 262112 Color: 1
Size: 253228 Color: 0

Bin 3015: 2 of cap free
Amount of items: 3
Items: 
Size: 358013 Color: 1
Size: 342326 Color: 0
Size: 299660 Color: 1

Bin 3016: 2 of cap free
Amount of items: 3
Items: 
Size: 428330 Color: 0
Size: 300562 Color: 1
Size: 271107 Color: 0

Bin 3017: 2 of cap free
Amount of items: 3
Items: 
Size: 357941 Color: 0
Size: 327386 Color: 1
Size: 314672 Color: 1

Bin 3018: 2 of cap free
Amount of items: 3
Items: 
Size: 358630 Color: 1
Size: 348450 Color: 1
Size: 292919 Color: 0

Bin 3019: 2 of cap free
Amount of items: 3
Items: 
Size: 444512 Color: 1
Size: 280204 Color: 1
Size: 275283 Color: 0

Bin 3020: 2 of cap free
Amount of items: 3
Items: 
Size: 462676 Color: 0
Size: 270682 Color: 0
Size: 266641 Color: 1

Bin 3021: 3 of cap free
Amount of items: 3
Items: 
Size: 397419 Color: 0
Size: 307807 Color: 1
Size: 294772 Color: 1

Bin 3022: 3 of cap free
Amount of items: 3
Items: 
Size: 407121 Color: 0
Size: 296481 Color: 1
Size: 296396 Color: 0

Bin 3023: 3 of cap free
Amount of items: 3
Items: 
Size: 442436 Color: 1
Size: 300883 Color: 1
Size: 256679 Color: 0

Bin 3024: 3 of cap free
Amount of items: 3
Items: 
Size: 470494 Color: 1
Size: 274567 Color: 1
Size: 254937 Color: 0

Bin 3025: 3 of cap free
Amount of items: 3
Items: 
Size: 476039 Color: 0
Size: 263829 Color: 0
Size: 260130 Color: 1

Bin 3026: 3 of cap free
Amount of items: 3
Items: 
Size: 488177 Color: 1
Size: 257173 Color: 0
Size: 254648 Color: 1

Bin 3027: 3 of cap free
Amount of items: 3
Items: 
Size: 475408 Color: 0
Size: 271309 Color: 1
Size: 253281 Color: 0

Bin 3028: 3 of cap free
Amount of items: 3
Items: 
Size: 469083 Color: 1
Size: 273579 Color: 1
Size: 257336 Color: 0

Bin 3029: 3 of cap free
Amount of items: 3
Items: 
Size: 413146 Color: 0
Size: 334435 Color: 1
Size: 252417 Color: 1

Bin 3030: 3 of cap free
Amount of items: 3
Items: 
Size: 439400 Color: 0
Size: 304238 Color: 1
Size: 256360 Color: 1

Bin 3031: 3 of cap free
Amount of items: 3
Items: 
Size: 349245 Color: 0
Size: 347233 Color: 1
Size: 303520 Color: 1

Bin 3032: 3 of cap free
Amount of items: 3
Items: 
Size: 435481 Color: 1
Size: 302612 Color: 0
Size: 261905 Color: 1

Bin 3033: 3 of cap free
Amount of items: 3
Items: 
Size: 385491 Color: 1
Size: 344936 Color: 0
Size: 269571 Color: 1

Bin 3034: 3 of cap free
Amount of items: 3
Items: 
Size: 360219 Color: 1
Size: 351757 Color: 0
Size: 288022 Color: 1

Bin 3035: 3 of cap free
Amount of items: 3
Items: 
Size: 343762 Color: 1
Size: 329795 Color: 0
Size: 326441 Color: 0

Bin 3036: 3 of cap free
Amount of items: 3
Items: 
Size: 409252 Color: 0
Size: 321618 Color: 1
Size: 269128 Color: 0

Bin 3037: 3 of cap free
Amount of items: 3
Items: 
Size: 374549 Color: 0
Size: 368845 Color: 1
Size: 256604 Color: 1

Bin 3038: 3 of cap free
Amount of items: 3
Items: 
Size: 403782 Color: 0
Size: 336394 Color: 1
Size: 259822 Color: 1

Bin 3039: 3 of cap free
Amount of items: 3
Items: 
Size: 497749 Color: 0
Size: 252166 Color: 1
Size: 250083 Color: 0

Bin 3040: 3 of cap free
Amount of items: 3
Items: 
Size: 392828 Color: 0
Size: 344590 Color: 1
Size: 262580 Color: 0

Bin 3041: 3 of cap free
Amount of items: 3
Items: 
Size: 386284 Color: 0
Size: 340729 Color: 0
Size: 272985 Color: 1

Bin 3042: 3 of cap free
Amount of items: 3
Items: 
Size: 397346 Color: 0
Size: 333810 Color: 1
Size: 268842 Color: 1

Bin 3043: 3 of cap free
Amount of items: 3
Items: 
Size: 492188 Color: 1
Size: 255867 Color: 1
Size: 251943 Color: 0

Bin 3044: 3 of cap free
Amount of items: 3
Items: 
Size: 362701 Color: 1
Size: 342431 Color: 1
Size: 294866 Color: 0

Bin 3045: 3 of cap free
Amount of items: 3
Items: 
Size: 367036 Color: 1
Size: 323512 Color: 1
Size: 309450 Color: 0

Bin 3046: 3 of cap free
Amount of items: 3
Items: 
Size: 358206 Color: 1
Size: 337691 Color: 0
Size: 304101 Color: 1

Bin 3047: 3 of cap free
Amount of items: 3
Items: 
Size: 361824 Color: 1
Size: 351844 Color: 1
Size: 286330 Color: 0

Bin 3048: 3 of cap free
Amount of items: 3
Items: 
Size: 476235 Color: 0
Size: 265418 Color: 0
Size: 258345 Color: 1

Bin 3049: 3 of cap free
Amount of items: 3
Items: 
Size: 378456 Color: 0
Size: 368058 Color: 0
Size: 253484 Color: 1

Bin 3050: 3 of cap free
Amount of items: 3
Items: 
Size: 345419 Color: 1
Size: 345208 Color: 1
Size: 309371 Color: 0

Bin 3051: 3 of cap free
Amount of items: 3
Items: 
Size: 466507 Color: 1
Size: 274866 Color: 0
Size: 258625 Color: 1

Bin 3052: 3 of cap free
Amount of items: 3
Items: 
Size: 383278 Color: 1
Size: 347369 Color: 1
Size: 269351 Color: 0

Bin 3053: 3 of cap free
Amount of items: 3
Items: 
Size: 353858 Color: 0
Size: 346630 Color: 1
Size: 299510 Color: 0

Bin 3054: 3 of cap free
Amount of items: 3
Items: 
Size: 349765 Color: 0
Size: 348513 Color: 1
Size: 301720 Color: 1

Bin 3055: 3 of cap free
Amount of items: 3
Items: 
Size: 456155 Color: 1
Size: 272493 Color: 0
Size: 271350 Color: 1

Bin 3056: 3 of cap free
Amount of items: 3
Items: 
Size: 363768 Color: 1
Size: 345755 Color: 1
Size: 290475 Color: 0

Bin 3057: 3 of cap free
Amount of items: 3
Items: 
Size: 428240 Color: 0
Size: 319987 Color: 1
Size: 251771 Color: 1

Bin 3058: 3 of cap free
Amount of items: 3
Items: 
Size: 430153 Color: 1
Size: 314508 Color: 0
Size: 255337 Color: 0

Bin 3059: 3 of cap free
Amount of items: 3
Items: 
Size: 345911 Color: 0
Size: 340195 Color: 1
Size: 313892 Color: 1

Bin 3060: 3 of cap free
Amount of items: 3
Items: 
Size: 445828 Color: 1
Size: 296723 Color: 0
Size: 257447 Color: 0

Bin 3061: 3 of cap free
Amount of items: 3
Items: 
Size: 489596 Color: 1
Size: 256230 Color: 0
Size: 254172 Color: 0

Bin 3062: 4 of cap free
Amount of items: 3
Items: 
Size: 354610 Color: 0
Size: 331043 Color: 0
Size: 314344 Color: 1

Bin 3063: 4 of cap free
Amount of items: 3
Items: 
Size: 349624 Color: 1
Size: 330675 Color: 0
Size: 319698 Color: 0

Bin 3064: 4 of cap free
Amount of items: 3
Items: 
Size: 361637 Color: 1
Size: 345863 Color: 0
Size: 292497 Color: 0

Bin 3065: 4 of cap free
Amount of items: 3
Items: 
Size: 342156 Color: 1
Size: 337158 Color: 0
Size: 320683 Color: 1

Bin 3066: 4 of cap free
Amount of items: 3
Items: 
Size: 490311 Color: 0
Size: 256642 Color: 0
Size: 253044 Color: 1

Bin 3067: 4 of cap free
Amount of items: 3
Items: 
Size: 452897 Color: 1
Size: 289744 Color: 0
Size: 257356 Color: 0

Bin 3068: 4 of cap free
Amount of items: 3
Items: 
Size: 352018 Color: 0
Size: 345849 Color: 0
Size: 302130 Color: 1

Bin 3069: 4 of cap free
Amount of items: 3
Items: 
Size: 382892 Color: 0
Size: 340746 Color: 1
Size: 276359 Color: 0

Bin 3070: 4 of cap free
Amount of items: 3
Items: 
Size: 406161 Color: 0
Size: 338289 Color: 1
Size: 255547 Color: 0

Bin 3071: 4 of cap free
Amount of items: 3
Items: 
Size: 436522 Color: 0
Size: 296262 Color: 1
Size: 267213 Color: 0

Bin 3072: 4 of cap free
Amount of items: 3
Items: 
Size: 410872 Color: 0
Size: 331544 Color: 1
Size: 257581 Color: 1

Bin 3073: 4 of cap free
Amount of items: 3
Items: 
Size: 354555 Color: 1
Size: 337045 Color: 0
Size: 308397 Color: 1

Bin 3074: 4 of cap free
Amount of items: 3
Items: 
Size: 361167 Color: 0
Size: 359735 Color: 1
Size: 279095 Color: 0

Bin 3075: 4 of cap free
Amount of items: 3
Items: 
Size: 371290 Color: 0
Size: 357283 Color: 0
Size: 271424 Color: 1

Bin 3076: 4 of cap free
Amount of items: 3
Items: 
Size: 350376 Color: 0
Size: 329750 Color: 0
Size: 319871 Color: 1

Bin 3077: 4 of cap free
Amount of items: 3
Items: 
Size: 483540 Color: 0
Size: 263549 Color: 1
Size: 252908 Color: 1

Bin 3078: 4 of cap free
Amount of items: 3
Items: 
Size: 372761 Color: 0
Size: 341558 Color: 1
Size: 285678 Color: 1

Bin 3079: 4 of cap free
Amount of items: 3
Items: 
Size: 490930 Color: 1
Size: 256666 Color: 1
Size: 252401 Color: 0

Bin 3080: 4 of cap free
Amount of items: 3
Items: 
Size: 442616 Color: 1
Size: 295172 Color: 0
Size: 262209 Color: 1

Bin 3081: 4 of cap free
Amount of items: 3
Items: 
Size: 489939 Color: 1
Size: 255820 Color: 0
Size: 254238 Color: 0

Bin 3082: 4 of cap free
Amount of items: 3
Items: 
Size: 437192 Color: 0
Size: 300647 Color: 1
Size: 262158 Color: 1

Bin 3083: 4 of cap free
Amount of items: 3
Items: 
Size: 390857 Color: 1
Size: 342115 Color: 1
Size: 267025 Color: 0

Bin 3084: 4 of cap free
Amount of items: 3
Items: 
Size: 381363 Color: 0
Size: 359649 Color: 0
Size: 258985 Color: 1

Bin 3085: 4 of cap free
Amount of items: 3
Items: 
Size: 409597 Color: 0
Size: 331464 Color: 1
Size: 258936 Color: 1

Bin 3086: 4 of cap free
Amount of items: 3
Items: 
Size: 411343 Color: 1
Size: 334632 Color: 0
Size: 254022 Color: 1

Bin 3087: 4 of cap free
Amount of items: 3
Items: 
Size: 465873 Color: 0
Size: 283945 Color: 0
Size: 250179 Color: 1

Bin 3088: 5 of cap free
Amount of items: 3
Items: 
Size: 445666 Color: 1
Size: 280340 Color: 0
Size: 273990 Color: 1

Bin 3089: 5 of cap free
Amount of items: 3
Items: 
Size: 423218 Color: 1
Size: 325136 Color: 0
Size: 251642 Color: 0

Bin 3090: 5 of cap free
Amount of items: 3
Items: 
Size: 489813 Color: 1
Size: 255316 Color: 0
Size: 254867 Color: 1

Bin 3091: 5 of cap free
Amount of items: 3
Items: 
Size: 364655 Color: 0
Size: 344907 Color: 0
Size: 290434 Color: 1

Bin 3092: 5 of cap free
Amount of items: 3
Items: 
Size: 446804 Color: 0
Size: 283167 Color: 1
Size: 270025 Color: 1

Bin 3093: 5 of cap free
Amount of items: 3
Items: 
Size: 366156 Color: 1
Size: 342597 Color: 0
Size: 291243 Color: 0

Bin 3094: 5 of cap free
Amount of items: 3
Items: 
Size: 353441 Color: 0
Size: 341215 Color: 1
Size: 305340 Color: 0

Bin 3095: 5 of cap free
Amount of items: 3
Items: 
Size: 378031 Color: 1
Size: 357087 Color: 0
Size: 264878 Color: 1

Bin 3096: 5 of cap free
Amount of items: 3
Items: 
Size: 379332 Color: 0
Size: 333603 Color: 1
Size: 287061 Color: 1

Bin 3097: 5 of cap free
Amount of items: 3
Items: 
Size: 354983 Color: 0
Size: 329184 Color: 1
Size: 315829 Color: 0

Bin 3098: 5 of cap free
Amount of items: 3
Items: 
Size: 350587 Color: 1
Size: 335494 Color: 1
Size: 313915 Color: 0

Bin 3099: 5 of cap free
Amount of items: 3
Items: 
Size: 346587 Color: 0
Size: 342773 Color: 0
Size: 310636 Color: 1

Bin 3100: 5 of cap free
Amount of items: 3
Items: 
Size: 491938 Color: 0
Size: 256649 Color: 1
Size: 251409 Color: 1

Bin 3101: 5 of cap free
Amount of items: 3
Items: 
Size: 350810 Color: 1
Size: 336292 Color: 0
Size: 312894 Color: 1

Bin 3102: 5 of cap free
Amount of items: 3
Items: 
Size: 384004 Color: 1
Size: 349212 Color: 1
Size: 266780 Color: 0

Bin 3103: 5 of cap free
Amount of items: 3
Items: 
Size: 351607 Color: 0
Size: 325364 Color: 0
Size: 323025 Color: 1

Bin 3104: 5 of cap free
Amount of items: 3
Items: 
Size: 358340 Color: 0
Size: 327997 Color: 1
Size: 313659 Color: 0

Bin 3105: 5 of cap free
Amount of items: 3
Items: 
Size: 351920 Color: 0
Size: 350849 Color: 0
Size: 297227 Color: 1

Bin 3106: 5 of cap free
Amount of items: 3
Items: 
Size: 487916 Color: 1
Size: 258461 Color: 0
Size: 253619 Color: 0

Bin 3107: 5 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 1
Size: 324391 Color: 0
Size: 270462 Color: 1

Bin 3108: 6 of cap free
Amount of items: 3
Items: 
Size: 369089 Color: 1
Size: 331921 Color: 1
Size: 298985 Color: 0

Bin 3109: 6 of cap free
Amount of items: 3
Items: 
Size: 385943 Color: 0
Size: 345113 Color: 1
Size: 268939 Color: 0

Bin 3110: 6 of cap free
Amount of items: 3
Items: 
Size: 354806 Color: 1
Size: 329153 Color: 1
Size: 316036 Color: 0

Bin 3111: 6 of cap free
Amount of items: 3
Items: 
Size: 346791 Color: 1
Size: 346634 Color: 0
Size: 306570 Color: 1

Bin 3112: 6 of cap free
Amount of items: 3
Items: 
Size: 403638 Color: 1
Size: 340880 Color: 0
Size: 255477 Color: 0

Bin 3113: 6 of cap free
Amount of items: 3
Items: 
Size: 456261 Color: 0
Size: 282511 Color: 1
Size: 261223 Color: 0

Bin 3114: 6 of cap free
Amount of items: 3
Items: 
Size: 357438 Color: 0
Size: 326141 Color: 0
Size: 316416 Color: 1

Bin 3115: 6 of cap free
Amount of items: 3
Items: 
Size: 373285 Color: 1
Size: 322717 Color: 0
Size: 303993 Color: 0

Bin 3116: 6 of cap free
Amount of items: 3
Items: 
Size: 382621 Color: 0
Size: 362012 Color: 1
Size: 255362 Color: 1

Bin 3117: 6 of cap free
Amount of items: 3
Items: 
Size: 408899 Color: 1
Size: 325201 Color: 0
Size: 265895 Color: 0

Bin 3118: 6 of cap free
Amount of items: 3
Items: 
Size: 487030 Color: 0
Size: 262692 Color: 0
Size: 250273 Color: 1

Bin 3119: 6 of cap free
Amount of items: 3
Items: 
Size: 499589 Color: 0
Size: 250397 Color: 0
Size: 250009 Color: 1

Bin 3120: 6 of cap free
Amount of items: 3
Items: 
Size: 414227 Color: 0
Size: 314680 Color: 1
Size: 271088 Color: 0

Bin 3121: 6 of cap free
Amount of items: 3
Items: 
Size: 338425 Color: 0
Size: 336139 Color: 0
Size: 325431 Color: 1

Bin 3122: 6 of cap free
Amount of items: 3
Items: 
Size: 436883 Color: 1
Size: 301825 Color: 1
Size: 261287 Color: 0

Bin 3123: 6 of cap free
Amount of items: 3
Items: 
Size: 464666 Color: 1
Size: 272503 Color: 0
Size: 262826 Color: 1

Bin 3124: 6 of cap free
Amount of items: 3
Items: 
Size: 363379 Color: 0
Size: 337427 Color: 1
Size: 299189 Color: 1

Bin 3125: 6 of cap free
Amount of items: 3
Items: 
Size: 366777 Color: 1
Size: 336168 Color: 0
Size: 297050 Color: 1

Bin 3126: 6 of cap free
Amount of items: 3
Items: 
Size: 389984 Color: 1
Size: 344041 Color: 0
Size: 265970 Color: 0

Bin 3127: 7 of cap free
Amount of items: 3
Items: 
Size: 486352 Color: 1
Size: 259114 Color: 0
Size: 254528 Color: 0

Bin 3128: 7 of cap free
Amount of items: 3
Items: 
Size: 490101 Color: 1
Size: 259086 Color: 1
Size: 250807 Color: 0

Bin 3129: 7 of cap free
Amount of items: 3
Items: 
Size: 490409 Color: 0
Size: 256216 Color: 1
Size: 253369 Color: 1

Bin 3130: 7 of cap free
Amount of items: 3
Items: 
Size: 475812 Color: 1
Size: 270637 Color: 1
Size: 253545 Color: 0

Bin 3131: 7 of cap free
Amount of items: 3
Items: 
Size: 432397 Color: 0
Size: 309384 Color: 0
Size: 258213 Color: 1

Bin 3132: 7 of cap free
Amount of items: 3
Items: 
Size: 382232 Color: 1
Size: 328091 Color: 0
Size: 289671 Color: 0

Bin 3133: 7 of cap free
Amount of items: 3
Items: 
Size: 358294 Color: 0
Size: 351904 Color: 0
Size: 289796 Color: 1

Bin 3134: 7 of cap free
Amount of items: 3
Items: 
Size: 484810 Color: 0
Size: 257654 Color: 1
Size: 257530 Color: 0

Bin 3135: 7 of cap free
Amount of items: 3
Items: 
Size: 351704 Color: 0
Size: 333784 Color: 1
Size: 314506 Color: 1

Bin 3136: 7 of cap free
Amount of items: 3
Items: 
Size: 376985 Color: 0
Size: 344969 Color: 0
Size: 278040 Color: 1

Bin 3137: 7 of cap free
Amount of items: 3
Items: 
Size: 414325 Color: 1
Size: 301905 Color: 0
Size: 283764 Color: 0

Bin 3138: 7 of cap free
Amount of items: 3
Items: 
Size: 345776 Color: 1
Size: 329438 Color: 0
Size: 324780 Color: 0

Bin 3139: 7 of cap free
Amount of items: 3
Items: 
Size: 412151 Color: 0
Size: 301936 Color: 0
Size: 285907 Color: 1

Bin 3140: 7 of cap free
Amount of items: 3
Items: 
Size: 416930 Color: 1
Size: 327362 Color: 0
Size: 255702 Color: 0

Bin 3141: 7 of cap free
Amount of items: 3
Items: 
Size: 371926 Color: 1
Size: 344077 Color: 1
Size: 283991 Color: 0

Bin 3142: 7 of cap free
Amount of items: 3
Items: 
Size: 422414 Color: 0
Size: 323943 Color: 1
Size: 253637 Color: 1

Bin 3143: 7 of cap free
Amount of items: 3
Items: 
Size: 377438 Color: 0
Size: 349077 Color: 0
Size: 273479 Color: 1

Bin 3144: 7 of cap free
Amount of items: 3
Items: 
Size: 353509 Color: 0
Size: 342583 Color: 0
Size: 303902 Color: 1

Bin 3145: 7 of cap free
Amount of items: 3
Items: 
Size: 361214 Color: 1
Size: 332037 Color: 0
Size: 306743 Color: 1

Bin 3146: 7 of cap free
Amount of items: 3
Items: 
Size: 478322 Color: 1
Size: 263001 Color: 1
Size: 258671 Color: 0

Bin 3147: 8 of cap free
Amount of items: 3
Items: 
Size: 386509 Color: 0
Size: 321462 Color: 0
Size: 292022 Color: 1

Bin 3148: 8 of cap free
Amount of items: 3
Items: 
Size: 447469 Color: 0
Size: 289187 Color: 1
Size: 263337 Color: 1

Bin 3149: 8 of cap free
Amount of items: 3
Items: 
Size: 478653 Color: 0
Size: 266782 Color: 1
Size: 254558 Color: 0

Bin 3150: 8 of cap free
Amount of items: 3
Items: 
Size: 382532 Color: 0
Size: 345587 Color: 1
Size: 271874 Color: 0

Bin 3151: 8 of cap free
Amount of items: 3
Items: 
Size: 460295 Color: 0
Size: 278361 Color: 0
Size: 261337 Color: 1

Bin 3152: 8 of cap free
Amount of items: 3
Items: 
Size: 367997 Color: 1
Size: 360049 Color: 1
Size: 271947 Color: 0

Bin 3153: 8 of cap free
Amount of items: 3
Items: 
Size: 357844 Color: 0
Size: 345513 Color: 1
Size: 296636 Color: 0

Bin 3154: 8 of cap free
Amount of items: 3
Items: 
Size: 405393 Color: 1
Size: 303966 Color: 1
Size: 290634 Color: 0

Bin 3155: 8 of cap free
Amount of items: 3
Items: 
Size: 358712 Color: 1
Size: 348710 Color: 1
Size: 292571 Color: 0

Bin 3156: 8 of cap free
Amount of items: 3
Items: 
Size: 375468 Color: 0
Size: 322502 Color: 1
Size: 302023 Color: 1

Bin 3157: 9 of cap free
Amount of items: 3
Items: 
Size: 418547 Color: 0
Size: 325531 Color: 0
Size: 255914 Color: 1

Bin 3158: 9 of cap free
Amount of items: 3
Items: 
Size: 370504 Color: 0
Size: 331403 Color: 1
Size: 298085 Color: 1

Bin 3159: 9 of cap free
Amount of items: 3
Items: 
Size: 451418 Color: 0
Size: 291012 Color: 1
Size: 257562 Color: 1

Bin 3160: 9 of cap free
Amount of items: 3
Items: 
Size: 461769 Color: 0
Size: 280548 Color: 0
Size: 257675 Color: 1

Bin 3161: 9 of cap free
Amount of items: 3
Items: 
Size: 346854 Color: 0
Size: 338863 Color: 1
Size: 314275 Color: 0

Bin 3162: 9 of cap free
Amount of items: 3
Items: 
Size: 353323 Color: 1
Size: 331416 Color: 0
Size: 315253 Color: 0

Bin 3163: 9 of cap free
Amount of items: 3
Items: 
Size: 427299 Color: 0
Size: 302096 Color: 0
Size: 270597 Color: 1

Bin 3164: 9 of cap free
Amount of items: 3
Items: 
Size: 369599 Color: 1
Size: 343126 Color: 1
Size: 287267 Color: 0

Bin 3165: 10 of cap free
Amount of items: 3
Items: 
Size: 430269 Color: 1
Size: 288755 Color: 1
Size: 280967 Color: 0

Bin 3166: 10 of cap free
Amount of items: 3
Items: 
Size: 343901 Color: 0
Size: 328561 Color: 0
Size: 327529 Color: 1

Bin 3167: 10 of cap free
Amount of items: 3
Items: 
Size: 456192 Color: 0
Size: 289243 Color: 1
Size: 254556 Color: 0

Bin 3168: 10 of cap free
Amount of items: 3
Items: 
Size: 494937 Color: 0
Size: 253846 Color: 1
Size: 251208 Color: 1

Bin 3169: 10 of cap free
Amount of items: 3
Items: 
Size: 343768 Color: 1
Size: 341708 Color: 0
Size: 314515 Color: 1

Bin 3170: 10 of cap free
Amount of items: 3
Items: 
Size: 381187 Color: 0
Size: 366919 Color: 1
Size: 251885 Color: 0

Bin 3171: 10 of cap free
Amount of items: 3
Items: 
Size: 423852 Color: 1
Size: 312511 Color: 1
Size: 263628 Color: 0

Bin 3172: 10 of cap free
Amount of items: 3
Items: 
Size: 431988 Color: 0
Size: 312435 Color: 1
Size: 255568 Color: 0

Bin 3173: 11 of cap free
Amount of items: 3
Items: 
Size: 347672 Color: 0
Size: 326349 Color: 1
Size: 325969 Color: 0

Bin 3174: 11 of cap free
Amount of items: 3
Items: 
Size: 341813 Color: 0
Size: 332684 Color: 1
Size: 325493 Color: 1

Bin 3175: 11 of cap free
Amount of items: 3
Items: 
Size: 355838 Color: 1
Size: 348116 Color: 0
Size: 296036 Color: 1

Bin 3176: 11 of cap free
Amount of items: 3
Items: 
Size: 470533 Color: 0
Size: 266148 Color: 0
Size: 263309 Color: 1

Bin 3177: 11 of cap free
Amount of items: 3
Items: 
Size: 473828 Color: 1
Size: 265208 Color: 1
Size: 260954 Color: 0

Bin 3178: 12 of cap free
Amount of items: 3
Items: 
Size: 424393 Color: 1
Size: 316398 Color: 0
Size: 259198 Color: 1

Bin 3179: 12 of cap free
Amount of items: 3
Items: 
Size: 480488 Color: 0
Size: 269388 Color: 1
Size: 250113 Color: 1

Bin 3180: 12 of cap free
Amount of items: 3
Items: 
Size: 485699 Color: 1
Size: 263751 Color: 0
Size: 250539 Color: 1

Bin 3181: 12 of cap free
Amount of items: 3
Items: 
Size: 416819 Color: 1
Size: 306634 Color: 0
Size: 276536 Color: 0

Bin 3182: 12 of cap free
Amount of items: 3
Items: 
Size: 420253 Color: 1
Size: 324763 Color: 1
Size: 254973 Color: 0

Bin 3183: 12 of cap free
Amount of items: 3
Items: 
Size: 349457 Color: 0
Size: 331160 Color: 1
Size: 319372 Color: 1

Bin 3184: 12 of cap free
Amount of items: 3
Items: 
Size: 476835 Color: 0
Size: 262954 Color: 1
Size: 260200 Color: 1

Bin 3185: 12 of cap free
Amount of items: 3
Items: 
Size: 481627 Color: 1
Size: 263434 Color: 1
Size: 254928 Color: 0

Bin 3186: 13 of cap free
Amount of items: 3
Items: 
Size: 378425 Color: 0
Size: 348415 Color: 1
Size: 273148 Color: 1

Bin 3187: 13 of cap free
Amount of items: 3
Items: 
Size: 358061 Color: 1
Size: 328241 Color: 0
Size: 313686 Color: 1

Bin 3188: 13 of cap free
Amount of items: 3
Items: 
Size: 498986 Color: 0
Size: 250959 Color: 0
Size: 250043 Color: 1

Bin 3189: 14 of cap free
Amount of items: 3
Items: 
Size: 469362 Color: 1
Size: 276875 Color: 1
Size: 253750 Color: 0

Bin 3190: 14 of cap free
Amount of items: 3
Items: 
Size: 486618 Color: 0
Size: 262021 Color: 1
Size: 251348 Color: 0

Bin 3191: 14 of cap free
Amount of items: 3
Items: 
Size: 488759 Color: 1
Size: 258054 Color: 0
Size: 253174 Color: 0

Bin 3192: 14 of cap free
Amount of items: 3
Items: 
Size: 365272 Color: 0
Size: 364756 Color: 0
Size: 269959 Color: 1

Bin 3193: 14 of cap free
Amount of items: 3
Items: 
Size: 335444 Color: 0
Size: 335142 Color: 1
Size: 329401 Color: 0

Bin 3194: 14 of cap free
Amount of items: 3
Items: 
Size: 343580 Color: 0
Size: 336796 Color: 1
Size: 319611 Color: 1

Bin 3195: 15 of cap free
Amount of items: 3
Items: 
Size: 472569 Color: 0
Size: 266380 Color: 1
Size: 261037 Color: 1

Bin 3196: 15 of cap free
Amount of items: 3
Items: 
Size: 405732 Color: 0
Size: 303323 Color: 1
Size: 290931 Color: 0

Bin 3197: 15 of cap free
Amount of items: 3
Items: 
Size: 390730 Color: 0
Size: 337236 Color: 1
Size: 272020 Color: 1

Bin 3198: 15 of cap free
Amount of items: 3
Items: 
Size: 395820 Color: 0
Size: 344054 Color: 1
Size: 260112 Color: 0

Bin 3199: 15 of cap free
Amount of items: 3
Items: 
Size: 342082 Color: 1
Size: 332349 Color: 0
Size: 325555 Color: 1

Bin 3200: 15 of cap free
Amount of items: 3
Items: 
Size: 419140 Color: 0
Size: 314553 Color: 0
Size: 266293 Color: 1

Bin 3201: 15 of cap free
Amount of items: 3
Items: 
Size: 351099 Color: 1
Size: 348423 Color: 0
Size: 300464 Color: 1

Bin 3202: 15 of cap free
Amount of items: 3
Items: 
Size: 465895 Color: 1
Size: 283798 Color: 1
Size: 250293 Color: 0

Bin 3203: 16 of cap free
Amount of items: 3
Items: 
Size: 466151 Color: 1
Size: 274958 Color: 0
Size: 258876 Color: 0

Bin 3204: 16 of cap free
Amount of items: 3
Items: 
Size: 425616 Color: 1
Size: 315340 Color: 0
Size: 259029 Color: 1

Bin 3205: 16 of cap free
Amount of items: 3
Items: 
Size: 356496 Color: 0
Size: 354645 Color: 1
Size: 288844 Color: 0

Bin 3206: 16 of cap free
Amount of items: 3
Items: 
Size: 371537 Color: 1
Size: 316496 Color: 0
Size: 311952 Color: 0

Bin 3207: 17 of cap free
Amount of items: 3
Items: 
Size: 484251 Color: 1
Size: 262702 Color: 0
Size: 253031 Color: 1

Bin 3208: 17 of cap free
Amount of items: 3
Items: 
Size: 470573 Color: 0
Size: 268849 Color: 1
Size: 260562 Color: 1

Bin 3209: 17 of cap free
Amount of items: 3
Items: 
Size: 376096 Color: 0
Size: 364122 Color: 0
Size: 259766 Color: 1

Bin 3210: 17 of cap free
Amount of items: 3
Items: 
Size: 364054 Color: 1
Size: 356919 Color: 1
Size: 279011 Color: 0

Bin 3211: 18 of cap free
Amount of items: 3
Items: 
Size: 440060 Color: 0
Size: 306981 Color: 1
Size: 252942 Color: 0

Bin 3212: 18 of cap free
Amount of items: 3
Items: 
Size: 353703 Color: 0
Size: 324535 Color: 1
Size: 321745 Color: 0

Bin 3213: 18 of cap free
Amount of items: 3
Items: 
Size: 408025 Color: 1
Size: 305725 Color: 1
Size: 286233 Color: 0

Bin 3214: 18 of cap free
Amount of items: 3
Items: 
Size: 400277 Color: 0
Size: 339095 Color: 1
Size: 260611 Color: 1

Bin 3215: 18 of cap free
Amount of items: 3
Items: 
Size: 367892 Color: 1
Size: 358494 Color: 0
Size: 273597 Color: 0

Bin 3216: 18 of cap free
Amount of items: 3
Items: 
Size: 412550 Color: 0
Size: 329239 Color: 1
Size: 258194 Color: 0

Bin 3217: 18 of cap free
Amount of items: 3
Items: 
Size: 359527 Color: 0
Size: 331054 Color: 1
Size: 309402 Color: 0

Bin 3218: 19 of cap free
Amount of items: 3
Items: 
Size: 489806 Color: 0
Size: 257823 Color: 0
Size: 252353 Color: 1

Bin 3219: 19 of cap free
Amount of items: 3
Items: 
Size: 414374 Color: 0
Size: 314396 Color: 1
Size: 271212 Color: 1

Bin 3220: 19 of cap free
Amount of items: 3
Items: 
Size: 433103 Color: 1
Size: 310304 Color: 1
Size: 256575 Color: 0

Bin 3221: 19 of cap free
Amount of items: 3
Items: 
Size: 405122 Color: 1
Size: 317846 Color: 0
Size: 277014 Color: 0

Bin 3222: 21 of cap free
Amount of items: 3
Items: 
Size: 372714 Color: 1
Size: 341115 Color: 0
Size: 286151 Color: 1

Bin 3223: 21 of cap free
Amount of items: 3
Items: 
Size: 351870 Color: 1
Size: 334659 Color: 0
Size: 313451 Color: 0

Bin 3224: 21 of cap free
Amount of items: 3
Items: 
Size: 459912 Color: 0
Size: 270132 Color: 1
Size: 269936 Color: 1

Bin 3225: 22 of cap free
Amount of items: 3
Items: 
Size: 432159 Color: 1
Size: 299315 Color: 1
Size: 268505 Color: 0

Bin 3226: 22 of cap free
Amount of items: 3
Items: 
Size: 427149 Color: 1
Size: 316601 Color: 0
Size: 256229 Color: 0

Bin 3227: 22 of cap free
Amount of items: 3
Items: 
Size: 363123 Color: 1
Size: 337016 Color: 1
Size: 299840 Color: 0

Bin 3228: 23 of cap free
Amount of items: 3
Items: 
Size: 438599 Color: 0
Size: 295242 Color: 0
Size: 266137 Color: 1

Bin 3229: 24 of cap free
Amount of items: 3
Items: 
Size: 453280 Color: 0
Size: 288923 Color: 1
Size: 257774 Color: 1

Bin 3230: 24 of cap free
Amount of items: 3
Items: 
Size: 354349 Color: 1
Size: 330320 Color: 0
Size: 315308 Color: 0

Bin 3231: 24 of cap free
Amount of items: 3
Items: 
Size: 440713 Color: 0
Size: 298849 Color: 1
Size: 260415 Color: 0

Bin 3232: 25 of cap free
Amount of items: 3
Items: 
Size: 490810 Color: 0
Size: 257006 Color: 1
Size: 252160 Color: 1

Bin 3233: 25 of cap free
Amount of items: 3
Items: 
Size: 357119 Color: 0
Size: 344422 Color: 1
Size: 298435 Color: 1

Bin 3234: 25 of cap free
Amount of items: 3
Items: 
Size: 451481 Color: 1
Size: 277990 Color: 1
Size: 270505 Color: 0

Bin 3235: 25 of cap free
Amount of items: 3
Items: 
Size: 372628 Color: 0
Size: 369120 Color: 0
Size: 258228 Color: 1

Bin 3236: 25 of cap free
Amount of items: 3
Items: 
Size: 498853 Color: 0
Size: 251041 Color: 1
Size: 250082 Color: 0

Bin 3237: 25 of cap free
Amount of items: 3
Items: 
Size: 392982 Color: 0
Size: 348212 Color: 1
Size: 258782 Color: 1

Bin 3238: 26 of cap free
Amount of items: 3
Items: 
Size: 400364 Color: 1
Size: 327527 Color: 1
Size: 272084 Color: 0

Bin 3239: 27 of cap free
Amount of items: 3
Items: 
Size: 472189 Color: 1
Size: 270452 Color: 0
Size: 257333 Color: 0

Bin 3240: 27 of cap free
Amount of items: 3
Items: 
Size: 342279 Color: 1
Size: 331498 Color: 1
Size: 326197 Color: 0

Bin 3241: 27 of cap free
Amount of items: 3
Items: 
Size: 476105 Color: 1
Size: 265645 Color: 1
Size: 258224 Color: 0

Bin 3242: 27 of cap free
Amount of items: 3
Items: 
Size: 378515 Color: 1
Size: 356582 Color: 1
Size: 264877 Color: 0

Bin 3243: 27 of cap free
Amount of items: 3
Items: 
Size: 421357 Color: 1
Size: 293330 Color: 1
Size: 285287 Color: 0

Bin 3244: 28 of cap free
Amount of items: 3
Items: 
Size: 344573 Color: 1
Size: 339099 Color: 0
Size: 316301 Color: 1

Bin 3245: 29 of cap free
Amount of items: 3
Items: 
Size: 447301 Color: 1
Size: 281791 Color: 1
Size: 270880 Color: 0

Bin 3246: 29 of cap free
Amount of items: 3
Items: 
Size: 426398 Color: 1
Size: 315878 Color: 0
Size: 257696 Color: 1

Bin 3247: 29 of cap free
Amount of items: 3
Items: 
Size: 343303 Color: 1
Size: 340334 Color: 1
Size: 316335 Color: 0

Bin 3248: 29 of cap free
Amount of items: 3
Items: 
Size: 452980 Color: 0
Size: 293903 Color: 1
Size: 253089 Color: 0

Bin 3249: 30 of cap free
Amount of items: 3
Items: 
Size: 432970 Color: 1
Size: 304595 Color: 0
Size: 262406 Color: 0

Bin 3250: 30 of cap free
Amount of items: 3
Items: 
Size: 424818 Color: 1
Size: 317923 Color: 1
Size: 257230 Color: 0

Bin 3251: 30 of cap free
Amount of items: 3
Items: 
Size: 404107 Color: 0
Size: 319054 Color: 1
Size: 276810 Color: 0

Bin 3252: 30 of cap free
Amount of items: 3
Items: 
Size: 467669 Color: 1
Size: 269363 Color: 0
Size: 262939 Color: 1

Bin 3253: 30 of cap free
Amount of items: 3
Items: 
Size: 340551 Color: 1
Size: 335129 Color: 0
Size: 324291 Color: 1

Bin 3254: 30 of cap free
Amount of items: 3
Items: 
Size: 348617 Color: 0
Size: 326697 Color: 0
Size: 324657 Color: 1

Bin 3255: 30 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 1
Size: 315804 Color: 0
Size: 279024 Color: 1

Bin 3256: 31 of cap free
Amount of items: 3
Items: 
Size: 362567 Color: 1
Size: 346070 Color: 0
Size: 291333 Color: 0

Bin 3257: 34 of cap free
Amount of items: 3
Items: 
Size: 454022 Color: 0
Size: 280919 Color: 0
Size: 265026 Color: 1

Bin 3258: 34 of cap free
Amount of items: 3
Items: 
Size: 461544 Color: 0
Size: 278922 Color: 0
Size: 259501 Color: 1

Bin 3259: 34 of cap free
Amount of items: 3
Items: 
Size: 376136 Color: 1
Size: 369476 Color: 0
Size: 254355 Color: 0

Bin 3260: 34 of cap free
Amount of items: 3
Items: 
Size: 406207 Color: 0
Size: 309585 Color: 1
Size: 284175 Color: 1

Bin 3261: 35 of cap free
Amount of items: 3
Items: 
Size: 443951 Color: 1
Size: 290760 Color: 0
Size: 265255 Color: 1

Bin 3262: 35 of cap free
Amount of items: 3
Items: 
Size: 401462 Color: 0
Size: 336746 Color: 1
Size: 261758 Color: 1

Bin 3263: 36 of cap free
Amount of items: 3
Items: 
Size: 485042 Color: 1
Size: 258815 Color: 0
Size: 256108 Color: 0

Bin 3264: 38 of cap free
Amount of items: 3
Items: 
Size: 454307 Color: 0
Size: 288149 Color: 0
Size: 257507 Color: 1

Bin 3265: 38 of cap free
Amount of items: 3
Items: 
Size: 457083 Color: 0
Size: 277236 Color: 1
Size: 265644 Color: 1

Bin 3266: 38 of cap free
Amount of items: 3
Items: 
Size: 382780 Color: 0
Size: 351116 Color: 0
Size: 266067 Color: 1

Bin 3267: 38 of cap free
Amount of items: 3
Items: 
Size: 418737 Color: 0
Size: 313274 Color: 0
Size: 267952 Color: 1

Bin 3268: 39 of cap free
Amount of items: 3
Items: 
Size: 400998 Color: 1
Size: 323152 Color: 0
Size: 275812 Color: 0

Bin 3269: 39 of cap free
Amount of items: 3
Items: 
Size: 402207 Color: 1
Size: 337059 Color: 0
Size: 260696 Color: 0

Bin 3270: 39 of cap free
Amount of items: 3
Items: 
Size: 464900 Color: 1
Size: 279141 Color: 1
Size: 255921 Color: 0

Bin 3271: 39 of cap free
Amount of items: 3
Items: 
Size: 369676 Color: 1
Size: 328061 Color: 0
Size: 302225 Color: 1

Bin 3272: 44 of cap free
Amount of items: 3
Items: 
Size: 353110 Color: 1
Size: 340611 Color: 1
Size: 306236 Color: 0

Bin 3273: 45 of cap free
Amount of items: 3
Items: 
Size: 428064 Color: 0
Size: 296137 Color: 1
Size: 275755 Color: 1

Bin 3274: 45 of cap free
Amount of items: 3
Items: 
Size: 435901 Color: 0
Size: 301930 Color: 1
Size: 262125 Color: 0

Bin 3275: 46 of cap free
Amount of items: 3
Items: 
Size: 379401 Color: 1
Size: 340282 Color: 0
Size: 280272 Color: 1

Bin 3276: 48 of cap free
Amount of items: 3
Items: 
Size: 360912 Color: 0
Size: 359775 Color: 1
Size: 279266 Color: 0

Bin 3277: 50 of cap free
Amount of items: 3
Items: 
Size: 352409 Color: 1
Size: 346903 Color: 1
Size: 300639 Color: 0

Bin 3278: 56 of cap free
Amount of items: 3
Items: 
Size: 419700 Color: 0
Size: 330016 Color: 1
Size: 250229 Color: 1

Bin 3279: 57 of cap free
Amount of items: 3
Items: 
Size: 413660 Color: 0
Size: 298979 Color: 1
Size: 287305 Color: 1

Bin 3280: 59 of cap free
Amount of items: 3
Items: 
Size: 424016 Color: 0
Size: 320021 Color: 1
Size: 255905 Color: 1

Bin 3281: 61 of cap free
Amount of items: 3
Items: 
Size: 410370 Color: 0
Size: 300351 Color: 0
Size: 289219 Color: 1

Bin 3282: 61 of cap free
Amount of items: 3
Items: 
Size: 390592 Color: 1
Size: 342531 Color: 0
Size: 266817 Color: 0

Bin 3283: 61 of cap free
Amount of items: 3
Items: 
Size: 402403 Color: 0
Size: 322827 Color: 1
Size: 274710 Color: 0

Bin 3284: 61 of cap free
Amount of items: 3
Items: 
Size: 420588 Color: 1
Size: 315451 Color: 1
Size: 263901 Color: 0

Bin 3285: 62 of cap free
Amount of items: 3
Items: 
Size: 487139 Color: 1
Size: 258321 Color: 0
Size: 254479 Color: 1

Bin 3286: 64 of cap free
Amount of items: 3
Items: 
Size: 417511 Color: 0
Size: 313770 Color: 1
Size: 268656 Color: 0

Bin 3287: 64 of cap free
Amount of items: 3
Items: 
Size: 356296 Color: 0
Size: 338992 Color: 1
Size: 304649 Color: 0

Bin 3288: 67 of cap free
Amount of items: 3
Items: 
Size: 416721 Color: 0
Size: 325019 Color: 0
Size: 258194 Color: 1

Bin 3289: 69 of cap free
Amount of items: 3
Items: 
Size: 345805 Color: 0
Size: 345535 Color: 1
Size: 308592 Color: 0

Bin 3290: 70 of cap free
Amount of items: 3
Items: 
Size: 410666 Color: 1
Size: 321386 Color: 1
Size: 267879 Color: 0

Bin 3291: 75 of cap free
Amount of items: 3
Items: 
Size: 429595 Color: 1
Size: 307053 Color: 1
Size: 263278 Color: 0

Bin 3292: 83 of cap free
Amount of items: 3
Items: 
Size: 441162 Color: 1
Size: 306736 Color: 1
Size: 252020 Color: 0

Bin 3293: 83 of cap free
Amount of items: 3
Items: 
Size: 352978 Color: 0
Size: 342128 Color: 1
Size: 304812 Color: 0

Bin 3294: 85 of cap free
Amount of items: 3
Items: 
Size: 400516 Color: 0
Size: 318067 Color: 0
Size: 281333 Color: 1

Bin 3295: 87 of cap free
Amount of items: 3
Items: 
Size: 486501 Color: 1
Size: 259599 Color: 1
Size: 253814 Color: 0

Bin 3296: 87 of cap free
Amount of items: 3
Items: 
Size: 431052 Color: 0
Size: 316954 Color: 0
Size: 251908 Color: 1

Bin 3297: 95 of cap free
Amount of items: 3
Items: 
Size: 431434 Color: 0
Size: 288865 Color: 1
Size: 279607 Color: 0

Bin 3298: 97 of cap free
Amount of items: 3
Items: 
Size: 422900 Color: 0
Size: 323491 Color: 0
Size: 253513 Color: 1

Bin 3299: 98 of cap free
Amount of items: 3
Items: 
Size: 377063 Color: 1
Size: 344350 Color: 0
Size: 278490 Color: 1

Bin 3300: 99 of cap free
Amount of items: 3
Items: 
Size: 382967 Color: 0
Size: 327251 Color: 1
Size: 289684 Color: 1

Bin 3301: 102 of cap free
Amount of items: 3
Items: 
Size: 392140 Color: 0
Size: 314441 Color: 1
Size: 293318 Color: 1

Bin 3302: 112 of cap free
Amount of items: 3
Items: 
Size: 382367 Color: 0
Size: 339791 Color: 1
Size: 277731 Color: 0

Bin 3303: 119 of cap free
Amount of items: 3
Items: 
Size: 424780 Color: 0
Size: 294104 Color: 1
Size: 280998 Color: 1

Bin 3304: 120 of cap free
Amount of items: 3
Items: 
Size: 339073 Color: 0
Size: 336521 Color: 1
Size: 324287 Color: 0

Bin 3305: 126 of cap free
Amount of items: 3
Items: 
Size: 389052 Color: 1
Size: 307217 Color: 0
Size: 303606 Color: 1

Bin 3306: 131 of cap free
Amount of items: 3
Items: 
Size: 361080 Color: 1
Size: 340414 Color: 1
Size: 298376 Color: 0

Bin 3307: 137 of cap free
Amount of items: 3
Items: 
Size: 360429 Color: 1
Size: 337144 Color: 1
Size: 302291 Color: 0

Bin 3308: 141 of cap free
Amount of items: 3
Items: 
Size: 433257 Color: 0
Size: 294820 Color: 1
Size: 271783 Color: 1

Bin 3309: 158 of cap free
Amount of items: 3
Items: 
Size: 496715 Color: 1
Size: 251858 Color: 1
Size: 251270 Color: 0

Bin 3310: 159 of cap free
Amount of items: 3
Items: 
Size: 414073 Color: 1
Size: 322744 Color: 0
Size: 263025 Color: 0

Bin 3311: 171 of cap free
Amount of items: 3
Items: 
Size: 369857 Color: 1
Size: 352689 Color: 0
Size: 277284 Color: 0

Bin 3312: 191 of cap free
Amount of items: 3
Items: 
Size: 445308 Color: 0
Size: 284461 Color: 1
Size: 270041 Color: 0

Bin 3313: 208 of cap free
Amount of items: 3
Items: 
Size: 457758 Color: 0
Size: 272886 Color: 1
Size: 269149 Color: 0

Bin 3314: 209 of cap free
Amount of items: 3
Items: 
Size: 441926 Color: 1
Size: 280207 Color: 1
Size: 277659 Color: 0

Bin 3315: 218 of cap free
Amount of items: 3
Items: 
Size: 458809 Color: 1
Size: 283641 Color: 0
Size: 257333 Color: 1

Bin 3316: 265 of cap free
Amount of items: 3
Items: 
Size: 442497 Color: 1
Size: 279581 Color: 1
Size: 277658 Color: 0

Bin 3317: 272 of cap free
Amount of items: 3
Items: 
Size: 477157 Color: 0
Size: 269104 Color: 1
Size: 253468 Color: 0

Bin 3318: 289 of cap free
Amount of items: 2
Items: 
Size: 499868 Color: 0
Size: 499844 Color: 1

Bin 3319: 304 of cap free
Amount of items: 3
Items: 
Size: 410339 Color: 1
Size: 323837 Color: 1
Size: 265521 Color: 0

Bin 3320: 353 of cap free
Amount of items: 3
Items: 
Size: 415930 Color: 1
Size: 326631 Color: 1
Size: 257087 Color: 0

Bin 3321: 386 of cap free
Amount of items: 2
Items: 
Size: 499867 Color: 0
Size: 499748 Color: 1

Bin 3322: 404 of cap free
Amount of items: 3
Items: 
Size: 386129 Color: 1
Size: 313786 Color: 0
Size: 299682 Color: 1

Bin 3323: 427 of cap free
Amount of items: 3
Items: 
Size: 434114 Color: 0
Size: 283559 Color: 0
Size: 281901 Color: 1

Bin 3324: 441 of cap free
Amount of items: 3
Items: 
Size: 348355 Color: 1
Size: 345734 Color: 1
Size: 305471 Color: 0

Bin 3325: 550 of cap free
Amount of items: 3
Items: 
Size: 401063 Color: 0
Size: 334025 Color: 0
Size: 264363 Color: 1

Bin 3326: 705 of cap free
Amount of items: 3
Items: 
Size: 423534 Color: 1
Size: 298970 Color: 1
Size: 276792 Color: 0

Bin 3327: 772 of cap free
Amount of items: 3
Items: 
Size: 355344 Color: 1
Size: 354084 Color: 0
Size: 289801 Color: 0

Bin 3328: 1335 of cap free
Amount of items: 3
Items: 
Size: 376179 Color: 0
Size: 347577 Color: 0
Size: 274910 Color: 1

Bin 3329: 3806 of cap free
Amount of items: 3
Items: 
Size: 447180 Color: 1
Size: 293604 Color: 0
Size: 255411 Color: 1

Bin 3330: 3893 of cap free
Amount of items: 3
Items: 
Size: 360692 Color: 0
Size: 321526 Color: 1
Size: 313890 Color: 1

Bin 3331: 36245 of cap free
Amount of items: 3
Items: 
Size: 371267 Color: 0
Size: 309932 Color: 0
Size: 282557 Color: 1

Bin 3332: 204694 of cap free
Amount of items: 3
Items: 
Size: 273780 Color: 1
Size: 260859 Color: 0
Size: 260668 Color: 1

Bin 3333: 239981 of cap free
Amount of items: 3
Items: 
Size: 253966 Color: 0
Size: 253264 Color: 1
Size: 252790 Color: 1

Bin 3334: 247353 of cap free
Amount of items: 3
Items: 
Size: 251579 Color: 0
Size: 250723 Color: 1
Size: 250346 Color: 1

Bin 3335: 250051 of cap free
Amount of items: 2
Items: 
Size: 499724 Color: 0
Size: 250226 Color: 1

Total size: 3334003334
Total free space: 1000001

