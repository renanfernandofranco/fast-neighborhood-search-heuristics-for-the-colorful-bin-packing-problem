Capicity Bin: 2020
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1505 Color: 165
Size: 439 Color: 111
Size: 44 Color: 15
Size: 32 Color: 7

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 1674 Color: 180
Size: 112 Color: 53
Size: 96 Color: 49
Size: 74 Color: 40
Size: 40 Color: 12
Size: 24 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 200
Size: 128 Color: 58
Size: 86 Color: 44

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 160
Size: 526 Color: 118
Size: 36 Color: 9

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1114 Color: 145
Size: 654 Color: 123
Size: 224 Color: 77
Size: 28 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 178
Size: 310 Color: 94
Size: 60 Color: 29

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 195
Size: 211 Color: 74
Size: 40 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 197
Size: 182 Color: 70
Size: 56 Color: 27

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 155
Size: 416 Color: 108
Size: 274 Color: 90

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 171
Size: 406 Color: 106
Size: 24 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 199
Size: 189 Color: 71
Size: 36 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 201
Size: 164 Color: 63
Size: 44 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 196
Size: 200 Color: 72
Size: 40 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 175
Size: 349 Color: 98
Size: 68 Color: 34

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 1396 Color: 157
Size: 468 Color: 114
Size: 88 Color: 47
Size: 68 Color: 33

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1013 Color: 139
Size: 841 Color: 136
Size: 166 Color: 67

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 150
Size: 470 Color: 115
Size: 312 Color: 96

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1497 Color: 163
Size: 375 Color: 102
Size: 148 Color: 62

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 188
Size: 242 Color: 84
Size: 44 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 166
Size: 414 Color: 107
Size: 72 Color: 38

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1014 Color: 140
Size: 826 Color: 131
Size: 180 Color: 69

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 910 Color: 137
Size: 820 Color: 130
Size: 290 Color: 92

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 156
Size: 578 Color: 119
Size: 52 Color: 24

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 162
Size: 437 Color: 110
Size: 92 Color: 48

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 161
Size: 462 Color: 113
Size: 88 Color: 46

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 167
Size: 383 Color: 105
Size: 76 Color: 43

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 168
Size: 379 Color: 104
Size: 74 Color: 41

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1268 Color: 151
Size: 676 Color: 124
Size: 76 Color: 42

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 141
Size: 837 Color: 134
Size: 166 Color: 65

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1501 Color: 164
Size: 433 Color: 109
Size: 86 Color: 45

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 192
Size: 202 Color: 73
Size: 60 Color: 31

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 169
Size: 378 Color: 103
Size: 72 Color: 39

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 177
Size: 311 Color: 95
Size: 60 Color: 28

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 193
Size: 218 Color: 75
Size: 40 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1195 Color: 149
Size: 689 Color: 125
Size: 136 Color: 60

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1182 Color: 148
Size: 702 Color: 126
Size: 136 Color: 59

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 179
Size: 309 Color: 93
Size: 60 Color: 30

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 147
Size: 731 Color: 128
Size: 118 Color: 56

Bin 39: 0 of cap free
Amount of items: 4
Items: 
Size: 1571 Color: 170
Size: 441 Color: 112
Size: 4 Color: 3
Size: 4 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1030 Color: 144
Size: 758 Color: 129
Size: 232 Color: 80

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 185
Size: 267 Color: 87
Size: 52 Color: 23

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 142
Size: 833 Color: 133
Size: 166 Color: 66

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 186
Size: 245 Color: 85
Size: 48 Color: 21

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 194
Size: 249 Color: 86
Size: 4 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1011 Color: 138
Size: 841 Color: 135
Size: 168 Color: 68

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 183
Size: 222 Color: 76
Size: 104 Color: 52

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 153
Size: 591 Color: 121
Size: 118 Color: 55

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 143
Size: 831 Color: 132
Size: 164 Color: 64

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 146
Size: 711 Color: 127
Size: 142 Color: 61

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 159
Size: 503 Color: 116
Size: 100 Color: 51

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 176
Size: 334 Color: 97
Size: 64 Color: 32

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 181
Size: 277 Color: 91
Size: 54 Color: 25

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 172
Size: 359 Color: 101
Size: 70 Color: 35

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 190
Size: 233 Color: 81
Size: 46 Color: 20

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 182
Size: 273 Color: 89
Size: 54 Color: 26

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 184
Size: 273 Color: 88
Size: 50 Color: 22

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 191
Size: 231 Color: 79
Size: 44 Color: 14

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 187
Size: 241 Color: 83
Size: 46 Color: 18

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 154
Size: 589 Color: 120
Size: 116 Color: 54

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 173
Size: 355 Color: 100
Size: 70 Color: 36

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 189
Size: 237 Color: 82
Size: 46 Color: 19

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 174
Size: 351 Color: 99
Size: 70 Color: 37

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 152
Size: 607 Color: 122
Size: 120 Color: 57

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 158
Size: 509 Color: 117
Size: 100 Color: 50

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 198
Size: 227 Color: 78
Size: 2 Color: 0

Total size: 131300
Total free space: 0

