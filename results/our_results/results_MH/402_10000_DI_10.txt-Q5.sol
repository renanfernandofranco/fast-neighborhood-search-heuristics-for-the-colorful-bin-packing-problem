Capicity Bin: 7568
Lower Bound: 132

Bins used: 132
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3736 Color: 1
Size: 1032 Color: 1
Size: 944 Color: 4
Size: 600 Color: 4
Size: 464 Color: 0
Size: 424 Color: 0
Size: 368 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 4
Size: 3052 Color: 0
Size: 608 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4729 Color: 3
Size: 2367 Color: 1
Size: 472 Color: 4

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 5220 Color: 0
Size: 1964 Color: 1
Size: 240 Color: 2
Size: 96 Color: 0
Size: 48 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 1
Size: 1046 Color: 2
Size: 48 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 2
Size: 755 Color: 0
Size: 150 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 2
Size: 749 Color: 1
Size: 148 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 2
Size: 1942 Color: 1
Size: 168 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3786 Color: 1
Size: 3154 Color: 2
Size: 628 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6192 Color: 3
Size: 1152 Color: 3
Size: 224 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4332 Color: 3
Size: 2700 Color: 4
Size: 536 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 4
Size: 828 Color: 0
Size: 72 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 3
Size: 830 Color: 0
Size: 164 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4758 Color: 2
Size: 2342 Color: 4
Size: 468 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6810 Color: 3
Size: 634 Color: 1
Size: 124 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 4
Size: 886 Color: 4
Size: 176 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6174 Color: 0
Size: 1162 Color: 0
Size: 232 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 3
Size: 652 Color: 1
Size: 128 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 1
Size: 2061 Color: 3
Size: 412 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6338 Color: 4
Size: 1026 Color: 4
Size: 204 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 0
Size: 2066 Color: 3
Size: 412 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 1
Size: 789 Color: 3
Size: 156 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5091 Color: 3
Size: 2357 Color: 0
Size: 120 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 1
Size: 2308 Color: 0
Size: 456 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 0
Size: 676 Color: 3
Size: 128 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 3789 Color: 2
Size: 3151 Color: 2
Size: 628 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6324 Color: 2
Size: 1044 Color: 1
Size: 200 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 0
Size: 1156 Color: 2
Size: 224 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6431 Color: 4
Size: 949 Color: 1
Size: 188 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6596 Color: 4
Size: 812 Color: 2
Size: 160 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 4
Size: 822 Color: 0
Size: 164 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6191 Color: 0
Size: 1149 Color: 0
Size: 228 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 3
Size: 1164 Color: 2
Size: 232 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6563 Color: 2
Size: 957 Color: 4
Size: 48 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 2
Size: 1786 Color: 4
Size: 244 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 4
Size: 1510 Color: 0
Size: 300 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6754 Color: 3
Size: 682 Color: 3
Size: 132 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 2
Size: 724 Color: 2
Size: 144 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 4
Size: 962 Color: 4
Size: 148 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6325 Color: 1
Size: 1037 Color: 1
Size: 206 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6725 Color: 3
Size: 703 Color: 4
Size: 140 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 3
Size: 1308 Color: 4
Size: 256 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 2
Size: 1654 Color: 1
Size: 328 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 3788 Color: 4
Size: 3156 Color: 0
Size: 624 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 4
Size: 1620 Color: 3
Size: 320 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 0
Size: 2148 Color: 0
Size: 240 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6372 Color: 0
Size: 1004 Color: 1
Size: 192 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 3
Size: 1860 Color: 1
Size: 368 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 4
Size: 829 Color: 1
Size: 164 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6041 Color: 4
Size: 1273 Color: 4
Size: 254 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 3794 Color: 3
Size: 3158 Color: 4
Size: 616 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 3796 Color: 4
Size: 3676 Color: 0
Size: 96 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 4
Size: 1082 Color: 1
Size: 216 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6145 Color: 4
Size: 1187 Color: 1
Size: 236 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6022 Color: 1
Size: 1290 Color: 1
Size: 256 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 2
Size: 951 Color: 0
Size: 190 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 0
Size: 1708 Color: 1
Size: 336 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 0
Size: 1419 Color: 1
Size: 282 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 4
Size: 1478 Color: 3
Size: 292 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 3
Size: 1022 Color: 2
Size: 64 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6622 Color: 1
Size: 790 Color: 0
Size: 156 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6573 Color: 3
Size: 831 Color: 4
Size: 164 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4727 Color: 3
Size: 2369 Color: 4
Size: 472 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5162 Color: 4
Size: 2006 Color: 3
Size: 400 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6757 Color: 4
Size: 677 Color: 1
Size: 134 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 4
Size: 892 Color: 3
Size: 176 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 2
Size: 1316 Color: 2
Size: 256 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 4
Size: 1006 Color: 3
Size: 200 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 0
Size: 870 Color: 2
Size: 172 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 3304 Color: 2
Size: 2816 Color: 2
Size: 1448 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 2
Size: 1057 Color: 1
Size: 210 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4722 Color: 1
Size: 2374 Color: 4
Size: 472 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 2
Size: 673 Color: 0
Size: 134 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5796 Color: 0
Size: 1484 Color: 1
Size: 288 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 3
Size: 2722 Color: 1
Size: 540 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 3785 Color: 3
Size: 3153 Color: 0
Size: 630 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6187 Color: 3
Size: 1151 Color: 0
Size: 230 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 4
Size: 666 Color: 1
Size: 132 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4380 Color: 4
Size: 2660 Color: 3
Size: 528 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6329 Color: 1
Size: 1033 Color: 4
Size: 206 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 2
Size: 1794 Color: 4
Size: 48 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6179 Color: 2
Size: 1159 Color: 0
Size: 230 Color: 0

Bin 83: 0 of cap free
Amount of items: 4
Items: 
Size: 6114 Color: 3
Size: 1214 Color: 4
Size: 128 Color: 4
Size: 112 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5990 Color: 1
Size: 1318 Color: 1
Size: 260 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 3
Size: 1492 Color: 2
Size: 296 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5083 Color: 0
Size: 2071 Color: 2
Size: 414 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6142 Color: 0
Size: 1190 Color: 1
Size: 236 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 3
Size: 1786 Color: 2
Size: 356 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 4
Size: 2324 Color: 4
Size: 456 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 4
Size: 907 Color: 2
Size: 100 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 3
Size: 1028 Color: 2
Size: 200 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 2
Size: 2710 Color: 3
Size: 528 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 2
Size: 2408 Color: 4
Size: 464 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6501 Color: 0
Size: 891 Color: 1
Size: 176 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 4
Size: 1126 Color: 1
Size: 168 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5838 Color: 0
Size: 1442 Color: 0
Size: 288 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 2
Size: 1502 Color: 1
Size: 260 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 3
Size: 2706 Color: 1
Size: 540 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 2
Size: 706 Color: 2
Size: 140 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5138 Color: 3
Size: 2026 Color: 4
Size: 404 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 3
Size: 1611 Color: 2
Size: 320 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6040 Color: 1
Size: 1144 Color: 4
Size: 384 Color: 4

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 2
Size: 988 Color: 4
Size: 120 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6773 Color: 0
Size: 663 Color: 1
Size: 132 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 4852 Color: 1
Size: 2268 Color: 0
Size: 448 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 3
Size: 764 Color: 2
Size: 152 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 3
Size: 836 Color: 4
Size: 160 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 2
Size: 654 Color: 3
Size: 128 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5522 Color: 2
Size: 1706 Color: 4
Size: 340 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 0
Size: 721 Color: 3
Size: 144 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 6638 Color: 1
Size: 778 Color: 2
Size: 152 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 4
Size: 674 Color: 2
Size: 132 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 4782 Color: 4
Size: 2322 Color: 2
Size: 464 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 3
Size: 889 Color: 2
Size: 176 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 0
Size: 1409 Color: 2
Size: 280 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 1
Size: 791 Color: 1
Size: 156 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 1
Size: 738 Color: 4
Size: 144 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 0
Size: 689 Color: 4
Size: 136 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 1
Size: 954 Color: 2
Size: 188 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 5391 Color: 2
Size: 1815 Color: 0
Size: 362 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 1
Size: 1591 Color: 4
Size: 318 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5381 Color: 1
Size: 1823 Color: 3
Size: 364 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 2
Size: 1023 Color: 0
Size: 204 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 4299 Color: 3
Size: 2725 Color: 0
Size: 544 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 4
Size: 2746 Color: 4
Size: 548 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 1
Size: 727 Color: 2
Size: 144 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5875 Color: 0
Size: 1411 Color: 0
Size: 282 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 4301 Color: 2
Size: 2723 Color: 2
Size: 544 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 3
Size: 1793 Color: 0
Size: 132 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5399 Color: 3
Size: 1809 Color: 4
Size: 360 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 2
Size: 1585 Color: 4
Size: 316 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 1
Size: 1307 Color: 4
Size: 212 Color: 1

Total size: 998976
Total free space: 0

