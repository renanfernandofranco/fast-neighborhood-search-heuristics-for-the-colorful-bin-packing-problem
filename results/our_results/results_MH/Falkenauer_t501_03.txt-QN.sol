Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 363
Size: 360 Color: 334
Size: 264 Color: 94

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 438
Size: 287 Color: 187
Size: 275 Color: 144

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 351
Size: 360 Color: 337
Size: 269 Color: 117

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 359
Size: 351 Color: 314
Size: 274 Color: 142

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 432
Size: 302 Color: 232
Size: 263 Color: 91

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 360
Size: 352 Color: 316
Size: 272 Color: 134

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 419
Size: 308 Color: 248
Size: 267 Color: 109

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 481
Size: 271 Color: 126
Size: 254 Color: 33

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 499
Size: 252 Color: 17
Size: 250 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 422
Size: 287 Color: 185
Size: 285 Color: 176

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 484
Size: 272 Color: 130
Size: 251 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 420
Size: 320 Color: 267
Size: 254 Color: 31

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 345
Size: 334 Color: 294
Size: 297 Color: 216

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 382
Size: 306 Color: 244
Size: 303 Color: 236

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 340
Size: 364 Color: 339
Size: 271 Color: 127

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 414
Size: 326 Color: 282
Size: 255 Color: 40

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 490
Size: 258 Color: 62
Size: 255 Color: 46

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 474
Size: 278 Color: 154
Size: 254 Color: 30

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 469
Size: 272 Color: 132
Size: 263 Color: 89

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 442
Size: 293 Color: 202
Size: 267 Color: 106

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 393
Size: 302 Color: 230
Size: 299 Color: 220

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 333
Size: 358 Color: 329
Size: 283 Color: 171

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 415
Size: 328 Color: 287
Size: 253 Color: 25

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 441
Size: 302 Color: 231
Size: 258 Color: 61

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 372
Size: 315 Color: 258
Size: 303 Color: 235

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 428
Size: 311 Color: 251
Size: 255 Color: 45

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 453
Size: 299 Color: 223
Size: 252 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 409
Size: 327 Color: 286
Size: 257 Color: 55

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 493
Size: 256 Color: 49
Size: 252 Color: 22

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 394
Size: 319 Color: 265
Size: 281 Color: 161

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 401
Size: 331 Color: 291
Size: 261 Color: 82

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 405
Size: 335 Color: 295
Size: 255 Color: 42

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 476
Size: 267 Color: 112
Size: 263 Color: 87

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 437
Size: 289 Color: 191
Size: 273 Color: 139

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 424
Size: 318 Color: 264
Size: 254 Color: 36

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 390
Size: 325 Color: 279
Size: 279 Color: 158

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 354
Size: 345 Color: 306
Size: 282 Color: 168

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 440
Size: 282 Color: 166
Size: 278 Color: 156

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 425
Size: 301 Color: 228
Size: 270 Color: 122

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 355
Size: 353 Color: 317
Size: 274 Color: 143

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 349
Size: 369 Color: 348
Size: 262 Color: 83

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 439
Size: 300 Color: 225
Size: 261 Color: 76

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 326
Size: 349 Color: 311
Size: 295 Color: 210

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 389
Size: 333 Color: 293
Size: 272 Color: 128

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 479
Size: 266 Color: 102
Size: 260 Color: 73

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 381
Size: 356 Color: 325
Size: 257 Color: 57

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 328
Size: 357 Color: 327
Size: 286 Color: 180

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 466
Size: 287 Color: 184
Size: 250 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 445
Size: 290 Color: 194
Size: 267 Color: 111

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 491
Size: 261 Color: 79
Size: 250 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 444
Size: 302 Color: 234
Size: 257 Color: 54

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 386
Size: 322 Color: 268
Size: 286 Color: 179

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 369
Size: 325 Color: 276
Size: 296 Color: 213

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 358
Size: 348 Color: 309
Size: 277 Color: 153

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 387
Size: 335 Color: 297
Size: 273 Color: 137

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 483
Size: 266 Color: 105
Size: 258 Color: 59

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 370
Size: 368 Color: 343
Size: 252 Color: 20

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 324
Size: 354 Color: 321
Size: 291 Color: 197

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 465
Size: 278 Color: 155
Size: 260 Color: 75

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 318
Size: 324 Color: 275
Size: 323 Color: 274

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 413
Size: 291 Color: 198
Size: 291 Color: 196

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 477
Size: 266 Color: 103
Size: 261 Color: 77

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 459
Size: 286 Color: 178
Size: 260 Color: 72

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 421
Size: 322 Color: 272
Size: 252 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 457
Size: 288 Color: 190
Size: 259 Color: 66

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 408
Size: 305 Color: 241
Size: 281 Color: 163

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 427
Size: 305 Color: 240
Size: 261 Color: 81

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 412
Size: 299 Color: 222
Size: 284 Color: 173

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 463
Size: 281 Color: 162
Size: 259 Color: 70

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 356
Size: 353 Color: 319
Size: 274 Color: 141

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 384
Size: 353 Color: 320
Size: 255 Color: 41

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 450
Size: 295 Color: 208
Size: 259 Color: 67

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 373
Size: 355 Color: 323
Size: 263 Color: 88

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 446
Size: 305 Color: 243
Size: 252 Color: 21

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 461
Size: 277 Color: 148
Size: 266 Color: 104

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 458
Size: 281 Color: 165
Size: 265 Color: 96

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 452
Size: 291 Color: 195
Size: 261 Color: 80

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 398
Size: 329 Color: 288
Size: 263 Color: 90

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 435
Size: 287 Color: 188
Size: 276 Color: 146

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 433
Size: 299 Color: 221
Size: 266 Color: 101

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 451
Size: 293 Color: 203
Size: 260 Color: 74

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 368
Size: 368 Color: 344
Size: 254 Color: 39

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 377
Size: 313 Color: 256
Size: 303 Color: 237

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 402
Size: 297 Color: 215
Size: 294 Color: 206

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 392
Size: 318 Color: 263
Size: 284 Color: 175

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 448
Size: 283 Color: 170
Size: 273 Color: 138

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 378
Size: 332 Color: 292
Size: 284 Color: 174

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 460
Size: 289 Color: 193
Size: 256 Color: 50

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 449
Size: 283 Color: 169
Size: 272 Color: 133

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 496
Size: 254 Color: 34
Size: 251 Color: 8

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 497
Size: 253 Color: 23
Size: 250 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 347
Size: 326 Color: 285
Size: 305 Color: 242

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 467
Size: 279 Color: 157
Size: 258 Color: 65

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 411
Size: 326 Color: 281
Size: 257 Color: 52

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 456
Size: 283 Color: 172
Size: 265 Color: 100

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 410
Size: 304 Color: 239
Size: 280 Color: 159

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 454
Size: 297 Color: 217
Size: 253 Color: 27

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 388
Size: 313 Color: 255
Size: 293 Color: 201

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 423
Size: 295 Color: 207
Size: 277 Color: 149

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 478
Size: 277 Color: 151
Size: 250 Color: 6

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 346
Size: 367 Color: 342
Size: 264 Color: 92

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 375
Size: 316 Color: 260
Size: 301 Color: 229

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 407
Size: 325 Color: 277
Size: 262 Color: 85

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 399
Size: 330 Color: 290
Size: 262 Color: 86

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 376
Size: 322 Color: 271
Size: 295 Color: 209

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 418
Size: 322 Color: 273
Size: 255 Color: 48

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 357
Size: 371 Color: 353
Size: 255 Color: 44

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 365
Size: 355 Color: 322
Size: 268 Color: 114

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 455
Size: 296 Color: 214
Size: 252 Color: 15

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 431
Size: 293 Color: 200
Size: 272 Color: 129

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 475
Size: 273 Color: 140
Size: 258 Color: 64

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 315
Size: 326 Color: 283
Size: 322 Color: 269

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 336
Size: 340 Color: 302
Size: 300 Color: 224

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 494
Size: 254 Color: 37
Size: 252 Color: 14

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 367
Size: 350 Color: 312
Size: 272 Color: 131

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 385
Size: 307 Color: 246
Size: 301 Color: 227

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 361
Size: 338 Color: 300
Size: 286 Color: 177

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 417
Size: 289 Color: 192
Size: 288 Color: 189

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 338
Size: 325 Color: 280
Size: 314 Color: 257

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 492
Size: 259 Color: 71
Size: 252 Color: 19

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 472
Size: 270 Color: 121
Size: 264 Color: 93

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 443
Size: 307 Color: 247
Size: 252 Color: 16

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 485
Size: 267 Color: 107
Size: 254 Color: 38

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 480
Size: 271 Color: 124
Size: 254 Color: 32

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 498
Size: 253 Color: 24
Size: 250 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 486
Size: 265 Color: 99
Size: 256 Color: 51

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 473
Size: 281 Color: 164
Size: 253 Color: 26

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 371
Size: 349 Color: 310
Size: 269 Color: 118

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 447
Size: 292 Color: 199
Size: 265 Color: 98

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 404
Size: 319 Color: 266
Size: 271 Color: 125

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 468
Size: 268 Color: 113
Size: 267 Color: 110

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 380
Size: 326 Color: 284
Size: 287 Color: 183

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 482
Size: 268 Color: 115
Size: 257 Color: 58

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 395
Size: 325 Color: 278
Size: 273 Color: 136

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 487
Size: 261 Color: 78
Size: 259 Color: 68

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 434
Size: 312 Color: 253
Size: 251 Color: 12

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 341
Size: 322 Color: 270
Size: 311 Color: 250

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 488
Size: 262 Color: 84
Size: 253 Color: 29

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 430
Size: 312 Color: 254
Size: 253 Color: 28

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 429
Size: 296 Color: 212
Size: 270 Color: 123

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 397
Size: 298 Color: 218
Size: 295 Color: 211

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 335
Size: 341 Color: 303
Size: 299 Color: 219

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 383
Size: 316 Color: 261
Size: 293 Color: 205

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 332
Size: 359 Color: 331
Size: 282 Color: 167

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 495
Size: 255 Color: 43
Size: 251 Color: 11

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 362
Size: 370 Color: 350
Size: 254 Color: 35

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 307
Size: 345 Color: 305
Size: 309 Color: 249

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 470
Size: 277 Color: 147
Size: 257 Color: 56

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 489
Size: 264 Color: 95
Size: 250 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 391
Size: 346 Color: 308
Size: 257 Color: 53

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 396
Size: 306 Color: 245
Size: 287 Color: 186

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 352
Size: 336 Color: 298
Size: 293 Color: 204

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 403
Size: 315 Color: 259
Size: 275 Color: 145

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 462
Size: 287 Color: 182
Size: 255 Color: 47

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 471
Size: 269 Color: 119
Size: 265 Color: 97

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 436
Size: 304 Color: 238
Size: 258 Color: 63

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 364
Size: 350 Color: 313
Size: 273 Color: 135

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 500
Size: 251 Color: 9
Size: 250 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 374
Size: 359 Color: 330
Size: 259 Color: 69

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 416
Size: 302 Color: 233
Size: 277 Color: 152

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 304
Size: 340 Color: 301
Size: 318 Color: 262

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 366
Size: 335 Color: 296
Size: 287 Color: 181

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 400
Size: 312 Color: 252
Size: 280 Color: 160

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 379
Size: 337 Color: 299
Size: 277 Color: 150

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 406
Size: 330 Color: 289
Size: 258 Color: 60

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 426
Size: 301 Color: 226
Size: 267 Color: 108

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 464
Size: 270 Color: 120
Size: 268 Color: 116

Total size: 167000
Total free space: 0

