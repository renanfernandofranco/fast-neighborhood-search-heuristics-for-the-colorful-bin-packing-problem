Capicity Bin: 19232
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9621 Color: 0
Size: 3647 Color: 4
Size: 3628 Color: 7
Size: 1600 Color: 10
Size: 736 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 8
Size: 6884 Color: 7
Size: 2468 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10542 Color: 8
Size: 7242 Color: 0
Size: 1448 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10865 Color: 11
Size: 7599 Color: 2
Size: 768 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11440 Color: 2
Size: 7120 Color: 19
Size: 672 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 5
Size: 6746 Color: 7
Size: 594 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 12
Size: 5544 Color: 6
Size: 884 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 9
Size: 5924 Color: 4
Size: 472 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 17
Size: 4676 Color: 16
Size: 960 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 19
Size: 3282 Color: 10
Size: 1942 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 8
Size: 3598 Color: 12
Size: 1520 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14170 Color: 18
Size: 4134 Color: 12
Size: 928 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 16
Size: 4580 Color: 13
Size: 368 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14348 Color: 10
Size: 4076 Color: 18
Size: 808 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 19
Size: 2576 Color: 1
Size: 2088 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14798 Color: 17
Size: 3906 Color: 18
Size: 528 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14832 Color: 14
Size: 3648 Color: 9
Size: 752 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14918 Color: 7
Size: 2920 Color: 19
Size: 1394 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14998 Color: 18
Size: 3146 Color: 11
Size: 1088 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15294 Color: 19
Size: 3234 Color: 5
Size: 704 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15332 Color: 5
Size: 1964 Color: 1
Size: 1936 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15362 Color: 3
Size: 3606 Color: 7
Size: 264 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15364 Color: 5
Size: 3284 Color: 16
Size: 584 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15600 Color: 16
Size: 2352 Color: 11
Size: 1280 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15665 Color: 1
Size: 2973 Color: 14
Size: 594 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15676 Color: 12
Size: 3228 Color: 10
Size: 328 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15698 Color: 0
Size: 2332 Color: 19
Size: 1202 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 15721 Color: 9
Size: 2927 Color: 19
Size: 584 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 15732 Color: 9
Size: 2912 Color: 7
Size: 588 Color: 9

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 15736 Color: 11
Size: 3280 Color: 17
Size: 216 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 15778 Color: 19
Size: 2056 Color: 9
Size: 1398 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 15824 Color: 0
Size: 3056 Color: 18
Size: 352 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 15962 Color: 1
Size: 2882 Color: 12
Size: 388 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16036 Color: 11
Size: 2864 Color: 17
Size: 332 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16120 Color: 15
Size: 1764 Color: 2
Size: 1348 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16144 Color: 1
Size: 2600 Color: 17
Size: 488 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16166 Color: 0
Size: 1850 Color: 6
Size: 1216 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16294 Color: 8
Size: 2450 Color: 12
Size: 488 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16304 Color: 18
Size: 2448 Color: 10
Size: 480 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16436 Color: 15
Size: 2236 Color: 8
Size: 560 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16482 Color: 15
Size: 1682 Color: 5
Size: 1068 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16664 Color: 18
Size: 1704 Color: 2
Size: 864 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16684 Color: 14
Size: 1972 Color: 10
Size: 576 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16696 Color: 18
Size: 1600 Color: 8
Size: 936 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 16752 Color: 14
Size: 1280 Color: 5
Size: 1200 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 6
Size: 1936 Color: 2
Size: 520 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 16780 Color: 18
Size: 1876 Color: 8
Size: 576 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 16846 Color: 5
Size: 1656 Color: 4
Size: 730 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 16868 Color: 12
Size: 1736 Color: 5
Size: 628 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 16884 Color: 17
Size: 1804 Color: 2
Size: 544 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 16944 Color: 19
Size: 1152 Color: 16
Size: 1136 Color: 14

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 16964 Color: 18
Size: 1504 Color: 12
Size: 764 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 16988 Color: 19
Size: 1988 Color: 9
Size: 256 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17076 Color: 8
Size: 1404 Color: 5
Size: 752 Color: 19

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17028 Color: 6
Size: 1560 Color: 12
Size: 644 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17032 Color: 12
Size: 1552 Color: 7
Size: 648 Color: 11

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17050 Color: 2
Size: 2046 Color: 8
Size: 136 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17124 Color: 13
Size: 1692 Color: 19
Size: 416 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17160 Color: 0
Size: 1680 Color: 4
Size: 392 Color: 10

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17214 Color: 8
Size: 1666 Color: 3
Size: 352 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 17234 Color: 0
Size: 1402 Color: 16
Size: 596 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17256 Color: 16
Size: 1632 Color: 8
Size: 344 Color: 13

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 0
Size: 7021 Color: 7
Size: 656 Color: 18

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11882 Color: 10
Size: 7013 Color: 13
Size: 336 Color: 8

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12015 Color: 9
Size: 6928 Color: 0
Size: 288 Color: 10

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 11
Size: 5357 Color: 5
Size: 1620 Color: 12

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 14
Size: 5351 Color: 5
Size: 384 Color: 1

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13934 Color: 7
Size: 3965 Color: 15
Size: 1332 Color: 9

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 15657 Color: 0
Size: 2946 Color: 17
Size: 628 Color: 18

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 15651 Color: 8
Size: 2924 Color: 5
Size: 656 Color: 8

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 16068 Color: 1
Size: 3163 Color: 8

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10462 Color: 19
Size: 8472 Color: 5
Size: 296 Color: 19

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 10841 Color: 4
Size: 6981 Color: 15
Size: 1408 Color: 18

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12805 Color: 17
Size: 6005 Color: 3
Size: 420 Color: 7

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 13458 Color: 6
Size: 5332 Color: 5
Size: 440 Color: 6

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13628 Color: 13
Size: 4354 Color: 5
Size: 1248 Color: 11

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 14010 Color: 13
Size: 4156 Color: 14
Size: 1064 Color: 0

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 15
Size: 3252 Color: 14
Size: 1402 Color: 14

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 15300 Color: 3
Size: 3162 Color: 15
Size: 768 Color: 13

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 15312 Color: 12
Size: 2862 Color: 0
Size: 1056 Color: 11

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 15431 Color: 1
Size: 3383 Color: 0
Size: 416 Color: 9

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 15458 Color: 18
Size: 2964 Color: 14
Size: 808 Color: 16

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 15482 Color: 1
Size: 3256 Color: 15
Size: 492 Color: 8

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 15700 Color: 3
Size: 3530 Color: 5

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 16046 Color: 11
Size: 1808 Color: 0
Size: 1376 Color: 15

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 16276 Color: 19
Size: 2954 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 16534 Color: 8
Size: 1592 Color: 7
Size: 1104 Color: 18

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 16520 Color: 11
Size: 2614 Color: 4
Size: 96 Color: 6

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 17258 Color: 5
Size: 1972 Color: 16

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 17292 Color: 11
Size: 1938 Color: 0

Bin 91: 3 of cap free
Amount of items: 4
Items: 
Size: 9624 Color: 14
Size: 6671 Color: 14
Size: 2082 Color: 13
Size: 852 Color: 17

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 11229 Color: 2
Size: 7232 Color: 17
Size: 768 Color: 16

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 14884 Color: 15
Size: 4345 Color: 10

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 7
Size: 7996 Color: 13
Size: 1160 Color: 10

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 10392 Color: 16
Size: 8012 Color: 18
Size: 824 Color: 7

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 6
Size: 4816 Color: 3

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 16744 Color: 5
Size: 2484 Color: 17

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 17220 Color: 4
Size: 2008 Color: 7

Bin 99: 5 of cap free
Amount of items: 8
Items: 
Size: 9617 Color: 0
Size: 1608 Color: 9
Size: 1602 Color: 8
Size: 1600 Color: 9
Size: 1568 Color: 15
Size: 1368 Color: 15
Size: 1224 Color: 13
Size: 640 Color: 15

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 10807 Color: 0
Size: 7956 Color: 1
Size: 464 Color: 7

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 14019 Color: 17
Size: 5208 Color: 15

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 0
Size: 5122 Color: 3
Size: 360 Color: 10

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 8
Size: 4592 Color: 9
Size: 424 Color: 0

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 16100 Color: 19
Size: 3126 Color: 1

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 16328 Color: 15
Size: 2250 Color: 0
Size: 648 Color: 1

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 16502 Color: 17
Size: 2724 Color: 11

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 16818 Color: 11
Size: 2408 Color: 10

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 17102 Color: 1
Size: 2124 Color: 19

Bin 109: 7 of cap free
Amount of items: 5
Items: 
Size: 9620 Color: 3
Size: 3604 Color: 9
Size: 3169 Color: 15
Size: 1488 Color: 3
Size: 1344 Color: 1

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 0
Size: 7992 Color: 10
Size: 528 Color: 9

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 15336 Color: 8
Size: 3888 Color: 14

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 15456 Color: 18
Size: 3768 Color: 7

Bin 113: 9 of cap free
Amount of items: 3
Items: 
Size: 14849 Color: 6
Size: 3830 Color: 0
Size: 544 Color: 1

Bin 114: 10 of cap free
Amount of items: 3
Items: 
Size: 13456 Color: 18
Size: 5446 Color: 5
Size: 320 Color: 0

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 14252 Color: 7
Size: 4970 Color: 1

Bin 116: 10 of cap free
Amount of items: 3
Items: 
Size: 15167 Color: 9
Size: 2985 Color: 0
Size: 1070 Color: 8

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 17232 Color: 0
Size: 1990 Color: 19

Bin 118: 12 of cap free
Amount of items: 3
Items: 
Size: 10972 Color: 8
Size: 7800 Color: 12
Size: 448 Color: 0

Bin 119: 14 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 15
Size: 4266 Color: 0
Size: 572 Color: 11

Bin 120: 14 of cap free
Amount of items: 2
Items: 
Size: 16270 Color: 12
Size: 2948 Color: 15

Bin 121: 14 of cap free
Amount of items: 3
Items: 
Size: 16388 Color: 13
Size: 2726 Color: 8
Size: 104 Color: 16

Bin 122: 15 of cap free
Amount of items: 2
Items: 
Size: 14857 Color: 17
Size: 4360 Color: 6

Bin 123: 15 of cap free
Amount of items: 2
Items: 
Size: 15173 Color: 3
Size: 4044 Color: 7

Bin 124: 16 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 5
Size: 7512 Color: 3
Size: 1472 Color: 14

Bin 125: 16 of cap free
Amount of items: 2
Items: 
Size: 15912 Color: 5
Size: 3304 Color: 12

Bin 126: 17 of cap free
Amount of items: 3
Items: 
Size: 12368 Color: 8
Size: 6015 Color: 5
Size: 832 Color: 4

Bin 127: 18 of cap free
Amount of items: 2
Items: 
Size: 16556 Color: 15
Size: 2658 Color: 3

Bin 128: 19 of cap free
Amount of items: 5
Items: 
Size: 9622 Color: 16
Size: 3976 Color: 1
Size: 3971 Color: 9
Size: 1120 Color: 3
Size: 524 Color: 1

Bin 129: 19 of cap free
Amount of items: 2
Items: 
Size: 13469 Color: 19
Size: 5744 Color: 17

Bin 130: 19 of cap free
Amount of items: 2
Items: 
Size: 16232 Color: 7
Size: 2981 Color: 18

Bin 131: 20 of cap free
Amount of items: 2
Items: 
Size: 12868 Color: 4
Size: 6344 Color: 7

Bin 132: 20 of cap free
Amount of items: 2
Items: 
Size: 13086 Color: 17
Size: 6126 Color: 4

Bin 133: 20 of cap free
Amount of items: 2
Items: 
Size: 13904 Color: 10
Size: 5308 Color: 18

Bin 134: 20 of cap free
Amount of items: 2
Items: 
Size: 15192 Color: 18
Size: 4020 Color: 8

Bin 135: 20 of cap free
Amount of items: 2
Items: 
Size: 17192 Color: 15
Size: 2020 Color: 4

Bin 136: 22 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 5
Size: 6512 Color: 14

Bin 137: 22 of cap free
Amount of items: 2
Items: 
Size: 17014 Color: 16
Size: 2196 Color: 1

Bin 138: 24 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 12
Size: 7568 Color: 1

Bin 139: 24 of cap free
Amount of items: 2
Items: 
Size: 16432 Color: 17
Size: 2776 Color: 19

Bin 140: 24 of cap free
Amount of items: 2
Items: 
Size: 16840 Color: 12
Size: 2368 Color: 7

Bin 141: 24 of cap free
Amount of items: 3
Items: 
Size: 17000 Color: 3
Size: 2096 Color: 11
Size: 112 Color: 1

Bin 142: 24 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 0
Size: 1864 Color: 3
Size: 40 Color: 3

Bin 143: 25 of cap free
Amount of items: 3
Items: 
Size: 12813 Color: 9
Size: 5818 Color: 2
Size: 576 Color: 0

Bin 144: 26 of cap free
Amount of items: 2
Items: 
Size: 16702 Color: 6
Size: 2504 Color: 17

Bin 145: 26 of cap free
Amount of items: 2
Items: 
Size: 16912 Color: 1
Size: 2294 Color: 6

Bin 146: 27 of cap free
Amount of items: 3
Items: 
Size: 13378 Color: 15
Size: 4803 Color: 7
Size: 1024 Color: 18

Bin 147: 28 of cap free
Amount of items: 2
Items: 
Size: 16734 Color: 3
Size: 2470 Color: 17

Bin 148: 29 of cap free
Amount of items: 2
Items: 
Size: 14011 Color: 17
Size: 5192 Color: 1

Bin 149: 30 of cap free
Amount of items: 2
Items: 
Size: 17188 Color: 0
Size: 2014 Color: 3

Bin 150: 32 of cap free
Amount of items: 2
Items: 
Size: 14664 Color: 16
Size: 4536 Color: 15

Bin 151: 35 of cap free
Amount of items: 3
Items: 
Size: 14228 Color: 7
Size: 4809 Color: 14
Size: 160 Color: 12

Bin 152: 36 of cap free
Amount of items: 2
Items: 
Size: 9692 Color: 14
Size: 9504 Color: 8

Bin 153: 36 of cap free
Amount of items: 2
Items: 
Size: 11152 Color: 3
Size: 8044 Color: 16

Bin 154: 38 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 9
Size: 4172 Color: 16
Size: 384 Color: 5

Bin 155: 39 of cap free
Amount of items: 2
Items: 
Size: 14475 Color: 16
Size: 4718 Color: 5

Bin 156: 40 of cap free
Amount of items: 2
Items: 
Size: 14728 Color: 15
Size: 4464 Color: 3

Bin 157: 40 of cap free
Amount of items: 2
Items: 
Size: 17072 Color: 17
Size: 2120 Color: 3

Bin 158: 41 of cap free
Amount of items: 2
Items: 
Size: 15802 Color: 6
Size: 3389 Color: 8

Bin 159: 44 of cap free
Amount of items: 2
Items: 
Size: 16910 Color: 4
Size: 2278 Color: 2

Bin 160: 48 of cap free
Amount of items: 2
Items: 
Size: 15272 Color: 2
Size: 3912 Color: 17

Bin 161: 48 of cap free
Amount of items: 2
Items: 
Size: 15442 Color: 3
Size: 3742 Color: 11

Bin 162: 51 of cap free
Amount of items: 4
Items: 
Size: 9644 Color: 14
Size: 6993 Color: 6
Size: 1584 Color: 15
Size: 960 Color: 16

Bin 163: 53 of cap free
Amount of items: 2
Items: 
Size: 10817 Color: 17
Size: 8362 Color: 12

Bin 164: 54 of cap free
Amount of items: 2
Items: 
Size: 14472 Color: 16
Size: 4706 Color: 4

Bin 165: 56 of cap free
Amount of items: 2
Items: 
Size: 17066 Color: 18
Size: 2110 Color: 12

Bin 166: 57 of cap free
Amount of items: 3
Items: 
Size: 13463 Color: 16
Size: 5552 Color: 4
Size: 160 Color: 3

Bin 167: 62 of cap free
Amount of items: 7
Items: 
Size: 9618 Color: 7
Size: 1848 Color: 12
Size: 1806 Color: 1
Size: 1708 Color: 2
Size: 1646 Color: 3
Size: 1584 Color: 10
Size: 960 Color: 7

Bin 168: 62 of cap free
Amount of items: 2
Items: 
Size: 15354 Color: 6
Size: 3816 Color: 4

Bin 169: 63 of cap free
Amount of items: 2
Items: 
Size: 14469 Color: 17
Size: 4700 Color: 11

Bin 170: 64 of cap free
Amount of items: 2
Items: 
Size: 11528 Color: 9
Size: 7640 Color: 11

Bin 171: 64 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 9
Size: 6424 Color: 12
Size: 160 Color: 16

Bin 172: 64 of cap free
Amount of items: 2
Items: 
Size: 16424 Color: 11
Size: 2744 Color: 8

Bin 173: 66 of cap free
Amount of items: 2
Items: 
Size: 16902 Color: 4
Size: 2264 Color: 14

Bin 174: 68 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 6
Size: 5364 Color: 19

Bin 175: 70 of cap free
Amount of items: 22
Items: 
Size: 1184 Color: 12
Size: 1184 Color: 2
Size: 1088 Color: 8
Size: 1064 Color: 17
Size: 1024 Color: 5
Size: 1024 Color: 1
Size: 896 Color: 12
Size: 896 Color: 10
Size: 870 Color: 8
Size: 868 Color: 15
Size: 868 Color: 14
Size: 864 Color: 11
Size: 836 Color: 0
Size: 832 Color: 5
Size: 824 Color: 13
Size: 800 Color: 11
Size: 792 Color: 18
Size: 792 Color: 0
Size: 720 Color: 16
Size: 704 Color: 10
Size: 520 Color: 16
Size: 512 Color: 2

Bin 176: 71 of cap free
Amount of items: 2
Items: 
Size: 10809 Color: 8
Size: 8352 Color: 5

Bin 177: 72 of cap free
Amount of items: 2
Items: 
Size: 12124 Color: 1
Size: 7036 Color: 3

Bin 178: 74 of cap free
Amount of items: 2
Items: 
Size: 16098 Color: 2
Size: 3060 Color: 9

Bin 179: 76 of cap free
Amount of items: 2
Items: 
Size: 16812 Color: 11
Size: 2344 Color: 19

Bin 180: 77 of cap free
Amount of items: 2
Items: 
Size: 12027 Color: 4
Size: 7128 Color: 13

Bin 181: 78 of cap free
Amount of items: 2
Items: 
Size: 11138 Color: 17
Size: 8016 Color: 10

Bin 182: 80 of cap free
Amount of items: 2
Items: 
Size: 15784 Color: 18
Size: 3368 Color: 17

Bin 183: 89 of cap free
Amount of items: 4
Items: 
Size: 9648 Color: 7
Size: 7021 Color: 1
Size: 2090 Color: 14
Size: 384 Color: 3

Bin 184: 90 of cap free
Amount of items: 2
Items: 
Size: 12120 Color: 7
Size: 7022 Color: 9

Bin 185: 96 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 16
Size: 6868 Color: 4
Size: 2612 Color: 2

Bin 186: 99 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 0
Size: 3653 Color: 17
Size: 2888 Color: 10

Bin 187: 99 of cap free
Amount of items: 2
Items: 
Size: 15437 Color: 3
Size: 3696 Color: 1

Bin 188: 101 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 7
Size: 8011 Color: 15
Size: 960 Color: 10

Bin 189: 104 of cap free
Amount of items: 2
Items: 
Size: 11760 Color: 3
Size: 7368 Color: 8

Bin 190: 108 of cap free
Amount of items: 2
Items: 
Size: 13000 Color: 9
Size: 6124 Color: 6

Bin 191: 129 of cap free
Amount of items: 2
Items: 
Size: 10857 Color: 0
Size: 8246 Color: 13

Bin 192: 138 of cap free
Amount of items: 2
Items: 
Size: 14908 Color: 4
Size: 4186 Color: 15

Bin 193: 139 of cap free
Amount of items: 2
Items: 
Size: 14742 Color: 0
Size: 4351 Color: 15

Bin 194: 144 of cap free
Amount of items: 2
Items: 
Size: 14274 Color: 5
Size: 4814 Color: 16

Bin 195: 148 of cap free
Amount of items: 3
Items: 
Size: 9628 Color: 14
Size: 6736 Color: 18
Size: 2720 Color: 11

Bin 196: 206 of cap free
Amount of items: 2
Items: 
Size: 11012 Color: 18
Size: 8014 Color: 5

Bin 197: 223 of cap free
Amount of items: 2
Items: 
Size: 10996 Color: 19
Size: 8013 Color: 12

Bin 198: 248 of cap free
Amount of items: 35
Items: 
Size: 784 Color: 9
Size: 748 Color: 1
Size: 728 Color: 11
Size: 720 Color: 5
Size: 716 Color: 8
Size: 676 Color: 19
Size: 676 Color: 13
Size: 656 Color: 10
Size: 640 Color: 13
Size: 640 Color: 3
Size: 632 Color: 17
Size: 632 Color: 12
Size: 624 Color: 19
Size: 592 Color: 18
Size: 592 Color: 16
Size: 568 Color: 6
Size: 544 Color: 6
Size: 544 Color: 3
Size: 544 Color: 1
Size: 512 Color: 15
Size: 496 Color: 2
Size: 464 Color: 10
Size: 464 Color: 2
Size: 456 Color: 16
Size: 452 Color: 12
Size: 448 Color: 16
Size: 448 Color: 0
Size: 400 Color: 13
Size: 400 Color: 11
Size: 400 Color: 9
Size: 396 Color: 18
Size: 384 Color: 18
Size: 352 Color: 17
Size: 336 Color: 4
Size: 320 Color: 4

Bin 199: 14336 of cap free
Amount of items: 14
Items: 
Size: 400 Color: 10
Size: 384 Color: 15
Size: 368 Color: 14
Size: 368 Color: 6
Size: 368 Color: 2
Size: 360 Color: 5
Size: 352 Color: 0
Size: 344 Color: 17
Size: 336 Color: 12
Size: 336 Color: 3
Size: 320 Color: 19
Size: 320 Color: 18
Size: 320 Color: 9
Size: 320 Color: 4

Total size: 3807936
Total free space: 19232

