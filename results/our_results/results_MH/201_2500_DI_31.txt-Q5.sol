Capicity Bin: 2492
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1088 Color: 4
Size: 852 Color: 3
Size: 550 Color: 0
Size: 2 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 0
Size: 542 Color: 2
Size: 116 Color: 3

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1846 Color: 4
Size: 346 Color: 1
Size: 212 Color: 0
Size: 52 Color: 2
Size: 36 Color: 4

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 2222 Color: 0
Size: 124 Color: 3
Size: 102 Color: 4
Size: 44 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1946 Color: 0
Size: 458 Color: 4
Size: 88 Color: 3

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 2000 Color: 2
Size: 352 Color: 3
Size: 100 Color: 0
Size: 40 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 2
Size: 405 Color: 0
Size: 80 Color: 3

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1228 Color: 4
Size: 932 Color: 3
Size: 204 Color: 1
Size: 128 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2029 Color: 1
Size: 387 Color: 4
Size: 76 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2021 Color: 2
Size: 447 Color: 4
Size: 24 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2149 Color: 2
Size: 287 Color: 0
Size: 56 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2197 Color: 0
Size: 247 Color: 2
Size: 48 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1247 Color: 1
Size: 1133 Color: 1
Size: 112 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 4
Size: 511 Color: 3
Size: 188 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1253 Color: 2
Size: 1033 Color: 2
Size: 206 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2173 Color: 3
Size: 267 Color: 4
Size: 52 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2126 Color: 0
Size: 306 Color: 1
Size: 60 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 1
Size: 345 Color: 0
Size: 68 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2217 Color: 4
Size: 271 Color: 3
Size: 4 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 4
Size: 338 Color: 4
Size: 64 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 2
Size: 406 Color: 0
Size: 80 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 0
Size: 650 Color: 3
Size: 104 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 4
Size: 226 Color: 3
Size: 108 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 0
Size: 398 Color: 3
Size: 76 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 2
Size: 934 Color: 2
Size: 184 Color: 4

Bin 26: 0 of cap free
Amount of items: 4
Items: 
Size: 1362 Color: 0
Size: 1038 Color: 1
Size: 68 Color: 0
Size: 24 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 2
Size: 254 Color: 2
Size: 48 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1879 Color: 4
Size: 505 Color: 0
Size: 108 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 4
Size: 887 Color: 3
Size: 176 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 4
Size: 1031 Color: 1
Size: 204 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1875 Color: 2
Size: 515 Color: 4
Size: 102 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2109 Color: 2
Size: 321 Color: 4
Size: 62 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 0
Size: 273 Color: 4
Size: 68 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2175 Color: 1
Size: 265 Color: 1
Size: 52 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 2
Size: 466 Color: 0
Size: 92 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 2
Size: 530 Color: 0
Size: 104 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 0
Size: 282 Color: 2
Size: 144 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2117 Color: 0
Size: 313 Color: 1
Size: 62 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2199 Color: 0
Size: 285 Color: 4
Size: 8 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2234 Color: 1
Size: 218 Color: 1
Size: 40 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 630 Color: 2
Size: 148 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 0
Size: 274 Color: 3
Size: 52 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 2
Size: 583 Color: 4
Size: 22 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 4
Size: 994 Color: 1
Size: 196 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 3
Size: 358 Color: 0
Size: 56 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 3
Size: 614 Color: 2
Size: 120 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 3
Size: 806 Color: 1
Size: 112 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 1
Size: 750 Color: 0
Size: 148 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 3
Size: 397 Color: 0
Size: 78 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 1
Size: 942 Color: 0
Size: 300 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2067 Color: 0
Size: 355 Color: 1
Size: 70 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 2
Size: 443 Color: 1
Size: 88 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 3
Size: 889 Color: 0
Size: 176 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1278 Color: 1
Size: 1014 Color: 3
Size: 200 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 3
Size: 1037 Color: 1
Size: 206 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 0
Size: 585 Color: 4
Size: 116 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 4
Size: 802 Color: 4
Size: 160 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1949 Color: 2
Size: 453 Color: 3
Size: 90 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 2121 Color: 2
Size: 311 Color: 1
Size: 60 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 1
Size: 650 Color: 3
Size: 116 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 4
Size: 782 Color: 3
Size: 156 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 2
Size: 669 Color: 3
Size: 132 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 2
Size: 667 Color: 3
Size: 132 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 3
Size: 769 Color: 2
Size: 152 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 1
Size: 767 Color: 1
Size: 152 Color: 3

Total size: 161980
Total free space: 0

