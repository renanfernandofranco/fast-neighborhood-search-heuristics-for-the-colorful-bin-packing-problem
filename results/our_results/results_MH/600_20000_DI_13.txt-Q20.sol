Capicity Bin: 16128
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 8080 Color: 0
Size: 1272 Color: 3
Size: 1228 Color: 18
Size: 1136 Color: 9
Size: 1080 Color: 19
Size: 1076 Color: 3
Size: 1024 Color: 17
Size: 754 Color: 5
Size: 478 Color: 12

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8072 Color: 11
Size: 4040 Color: 2
Size: 2372 Color: 12
Size: 1172 Color: 19
Size: 472 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 15
Size: 6664 Color: 15
Size: 1328 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9104 Color: 10
Size: 6648 Color: 7
Size: 376 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 18
Size: 5798 Color: 11
Size: 1156 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9182 Color: 5
Size: 5790 Color: 13
Size: 1156 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9238 Color: 5
Size: 5742 Color: 13
Size: 1148 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 0
Size: 5368 Color: 7
Size: 1088 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10404 Color: 14
Size: 5172 Color: 5
Size: 552 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11110 Color: 2
Size: 3600 Color: 12
Size: 1418 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11134 Color: 18
Size: 4162 Color: 17
Size: 832 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 12
Size: 4238 Color: 15
Size: 666 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 12
Size: 3504 Color: 10
Size: 1336 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11639 Color: 17
Size: 3741 Color: 8
Size: 748 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11772 Color: 3
Size: 2672 Color: 16
Size: 1684 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 6
Size: 3512 Color: 13
Size: 448 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 16
Size: 3152 Color: 17
Size: 440 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12600 Color: 13
Size: 3000 Color: 0
Size: 528 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12742 Color: 10
Size: 3236 Color: 11
Size: 150 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 9
Size: 2952 Color: 1
Size: 272 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12944 Color: 3
Size: 2344 Color: 1
Size: 840 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 18
Size: 1838 Color: 2
Size: 1328 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 18
Size: 2604 Color: 13
Size: 544 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12997 Color: 12
Size: 2427 Color: 15
Size: 704 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13122 Color: 17
Size: 2670 Color: 5
Size: 336 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 17
Size: 2408 Color: 9
Size: 464 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 16
Size: 2620 Color: 9
Size: 206 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 16
Size: 2364 Color: 9
Size: 444 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 2
Size: 2358 Color: 15
Size: 346 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13463 Color: 6
Size: 2327 Color: 13
Size: 338 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 18
Size: 2440 Color: 13
Size: 148 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13580 Color: 2
Size: 1936 Color: 3
Size: 612 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13597 Color: 0
Size: 1731 Color: 18
Size: 800 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 5
Size: 1792 Color: 5
Size: 728 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 9
Size: 2102 Color: 15
Size: 416 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13758 Color: 2
Size: 1528 Color: 1
Size: 842 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 12
Size: 2058 Color: 9
Size: 408 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 13
Size: 1372 Color: 1
Size: 1084 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13703 Color: 15
Size: 2051 Color: 6
Size: 374 Color: 19

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13708 Color: 19
Size: 1428 Color: 16
Size: 992 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13712 Color: 13
Size: 1688 Color: 18
Size: 728 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13840 Color: 2
Size: 1584 Color: 3
Size: 704 Color: 16

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 9
Size: 1788 Color: 14
Size: 492 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13874 Color: 2
Size: 1374 Color: 19
Size: 880 Color: 10

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13912 Color: 18
Size: 1792 Color: 0
Size: 424 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 14
Size: 1480 Color: 11
Size: 672 Color: 13

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 10
Size: 1340 Color: 4
Size: 800 Color: 17

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13990 Color: 17
Size: 1738 Color: 4
Size: 400 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14048 Color: 19
Size: 1760 Color: 12
Size: 320 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14116 Color: 19
Size: 1152 Color: 16
Size: 860 Color: 13

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14142 Color: 13
Size: 1312 Color: 2
Size: 674 Color: 10

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 18
Size: 1168 Color: 9
Size: 764 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 14
Size: 1312 Color: 13
Size: 524 Color: 15

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 17
Size: 1524 Color: 11
Size: 308 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14354 Color: 0
Size: 1482 Color: 12
Size: 292 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 10
Size: 928 Color: 16
Size: 844 Color: 19

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 5
Size: 1612 Color: 13
Size: 156 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 1
Size: 1096 Color: 11
Size: 608 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 4
Size: 1342 Color: 7
Size: 356 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 2
Size: 1340 Color: 9
Size: 304 Color: 10

Bin 61: 1 of cap free
Amount of items: 5
Items: 
Size: 8082 Color: 13
Size: 4148 Color: 2
Size: 2891 Color: 11
Size: 680 Color: 19
Size: 326 Color: 18

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 8749 Color: 14
Size: 6706 Color: 12
Size: 672 Color: 8

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 17
Size: 5861 Color: 17
Size: 562 Color: 3

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10429 Color: 8
Size: 5410 Color: 9
Size: 288 Color: 15

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10452 Color: 15
Size: 5387 Color: 8
Size: 288 Color: 10

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11065 Color: 18
Size: 2671 Color: 4
Size: 2391 Color: 13

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11607 Color: 8
Size: 4092 Color: 14
Size: 428 Color: 14

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 3
Size: 4221 Color: 2
Size: 256 Color: 14

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12086 Color: 19
Size: 3369 Color: 4
Size: 672 Color: 12

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12447 Color: 8
Size: 2032 Color: 2
Size: 1648 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12747 Color: 13
Size: 2900 Color: 2
Size: 480 Color: 18

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 6
Size: 2569 Color: 2
Size: 384 Color: 17

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13247 Color: 10
Size: 2596 Color: 19
Size: 284 Color: 13

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13451 Color: 12
Size: 2380 Color: 5
Size: 296 Color: 18

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13546 Color: 2
Size: 1765 Color: 7
Size: 816 Color: 14

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 0
Size: 2611 Color: 16

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13667 Color: 10
Size: 1512 Color: 0
Size: 948 Color: 19

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13795 Color: 2
Size: 1912 Color: 1
Size: 420 Color: 6

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 13909 Color: 8
Size: 2218 Color: 14

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 14051 Color: 14
Size: 1592 Color: 2
Size: 484 Color: 3

Bin 81: 2 of cap free
Amount of items: 11
Items: 
Size: 8066 Color: 16
Size: 1072 Color: 15
Size: 952 Color: 18
Size: 952 Color: 9
Size: 848 Color: 7
Size: 836 Color: 15
Size: 816 Color: 7
Size: 816 Color: 2
Size: 688 Color: 19
Size: 608 Color: 2
Size: 472 Color: 5

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 8090 Color: 14
Size: 6696 Color: 10
Size: 1340 Color: 4

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 8468 Color: 13
Size: 6714 Color: 9
Size: 944 Color: 11

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 9580 Color: 0
Size: 6194 Color: 8
Size: 352 Color: 4

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 10394 Color: 15
Size: 5460 Color: 17
Size: 272 Color: 7

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 10704 Color: 14
Size: 5422 Color: 18

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 7
Size: 4732 Color: 1
Size: 360 Color: 3

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 11062 Color: 12
Size: 4680 Color: 17
Size: 384 Color: 5

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11136 Color: 12
Size: 3560 Color: 3
Size: 1430 Color: 13

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 11156 Color: 15
Size: 3368 Color: 14
Size: 1602 Color: 10

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 11944 Color: 14
Size: 4182 Color: 1

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 9
Size: 3644 Color: 17
Size: 400 Color: 2

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 12392 Color: 1
Size: 3734 Color: 12

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 9
Size: 2154 Color: 2
Size: 1328 Color: 13

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 17
Size: 2642 Color: 2
Size: 480 Color: 18

Bin 96: 2 of cap free
Amount of items: 4
Items: 
Size: 13918 Color: 3
Size: 2124 Color: 5
Size: 52 Color: 14
Size: 32 Color: 11

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 14206 Color: 15
Size: 1264 Color: 0
Size: 656 Color: 2

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 11046 Color: 2
Size: 4751 Color: 11
Size: 328 Color: 3

Bin 99: 3 of cap free
Amount of items: 2
Items: 
Size: 12037 Color: 19
Size: 4088 Color: 12

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12075 Color: 16
Size: 3730 Color: 4
Size: 320 Color: 17

Bin 101: 3 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 0
Size: 3334 Color: 16

Bin 102: 3 of cap free
Amount of items: 2
Items: 
Size: 13217 Color: 17
Size: 2908 Color: 8

Bin 103: 3 of cap free
Amount of items: 2
Items: 
Size: 14104 Color: 17
Size: 2021 Color: 10

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 12
Size: 1978 Color: 4
Size: 32 Color: 10

Bin 105: 3 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 17
Size: 1637 Color: 11

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 9114 Color: 6
Size: 6722 Color: 15
Size: 288 Color: 11

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 9246 Color: 15
Size: 5782 Color: 4
Size: 1096 Color: 3

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 5
Size: 6200 Color: 7
Size: 392 Color: 4

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 10388 Color: 3
Size: 5736 Color: 10

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 11048 Color: 16
Size: 5076 Color: 4

Bin 111: 4 of cap free
Amount of items: 3
Items: 
Size: 11824 Color: 0
Size: 3244 Color: 18
Size: 1056 Color: 2

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 18
Size: 2696 Color: 3
Size: 128 Color: 17

Bin 113: 4 of cap free
Amount of items: 2
Items: 
Size: 13584 Color: 19
Size: 2540 Color: 10

Bin 114: 4 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 19
Size: 2128 Color: 1
Size: 72 Color: 17

Bin 115: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 9
Size: 1892 Color: 10

Bin 116: 5 of cap free
Amount of items: 5
Items: 
Size: 8069 Color: 7
Size: 2365 Color: 18
Size: 2056 Color: 2
Size: 1851 Color: 8
Size: 1782 Color: 11

Bin 117: 5 of cap free
Amount of items: 3
Items: 
Size: 8500 Color: 11
Size: 7067 Color: 5
Size: 556 Color: 15

Bin 118: 5 of cap free
Amount of items: 2
Items: 
Size: 11595 Color: 6
Size: 4528 Color: 0

Bin 119: 5 of cap free
Amount of items: 2
Items: 
Size: 12968 Color: 19
Size: 3155 Color: 13

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 14278 Color: 6
Size: 1844 Color: 16

Bin 121: 7 of cap free
Amount of items: 2
Items: 
Size: 13047 Color: 0
Size: 3074 Color: 1

Bin 122: 7 of cap free
Amount of items: 2
Items: 
Size: 13659 Color: 4
Size: 2462 Color: 11

Bin 123: 7 of cap free
Amount of items: 2
Items: 
Size: 13873 Color: 10
Size: 2248 Color: 12

Bin 124: 8 of cap free
Amount of items: 3
Items: 
Size: 10488 Color: 3
Size: 4808 Color: 2
Size: 824 Color: 6

Bin 125: 8 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 19
Size: 4016 Color: 4

Bin 126: 8 of cap free
Amount of items: 3
Items: 
Size: 12240 Color: 17
Size: 2448 Color: 2
Size: 1432 Color: 7

Bin 127: 8 of cap free
Amount of items: 2
Items: 
Size: 14009 Color: 6
Size: 2111 Color: 14

Bin 128: 9 of cap free
Amount of items: 14
Items: 
Size: 8065 Color: 13
Size: 744 Color: 17
Size: 744 Color: 8
Size: 720 Color: 19
Size: 640 Color: 19
Size: 640 Color: 10
Size: 624 Color: 4
Size: 612 Color: 2
Size: 608 Color: 9
Size: 608 Color: 4
Size: 608 Color: 2
Size: 576 Color: 9
Size: 520 Color: 0
Size: 410 Color: 5

Bin 129: 9 of cap free
Amount of items: 2
Items: 
Size: 9968 Color: 15
Size: 6151 Color: 18

Bin 130: 9 of cap free
Amount of items: 2
Items: 
Size: 13259 Color: 18
Size: 2860 Color: 12

Bin 131: 9 of cap free
Amount of items: 2
Items: 
Size: 13888 Color: 9
Size: 2231 Color: 6

Bin 132: 9 of cap free
Amount of items: 2
Items: 
Size: 14256 Color: 18
Size: 1863 Color: 0

Bin 133: 10 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 9
Size: 4211 Color: 11
Size: 1028 Color: 11

Bin 134: 10 of cap free
Amount of items: 2
Items: 
Size: 12458 Color: 4
Size: 3660 Color: 13

Bin 135: 10 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 0
Size: 2648 Color: 9

Bin 136: 11 of cap free
Amount of items: 2
Items: 
Size: 14011 Color: 14
Size: 2106 Color: 13

Bin 137: 11 of cap free
Amount of items: 2
Items: 
Size: 14350 Color: 13
Size: 1767 Color: 16

Bin 138: 12 of cap free
Amount of items: 3
Items: 
Size: 12442 Color: 9
Size: 3370 Color: 12
Size: 304 Color: 8

Bin 139: 12 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 8
Size: 3128 Color: 6

Bin 140: 12 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 9
Size: 2256 Color: 1

Bin 141: 13 of cap free
Amount of items: 2
Items: 
Size: 14095 Color: 7
Size: 2020 Color: 11

Bin 142: 13 of cap free
Amount of items: 2
Items: 
Size: 14420 Color: 13
Size: 1695 Color: 0

Bin 143: 14 of cap free
Amount of items: 3
Items: 
Size: 10406 Color: 5
Size: 5500 Color: 10
Size: 208 Color: 14

Bin 144: 14 of cap free
Amount of items: 3
Items: 
Size: 11740 Color: 6
Size: 4246 Color: 1
Size: 128 Color: 4

Bin 145: 14 of cap free
Amount of items: 2
Items: 
Size: 14194 Color: 9
Size: 1920 Color: 10

Bin 146: 16 of cap free
Amount of items: 2
Items: 
Size: 11864 Color: 4
Size: 4248 Color: 15

Bin 147: 16 of cap free
Amount of items: 2
Items: 
Size: 12368 Color: 19
Size: 3744 Color: 18

Bin 148: 17 of cap free
Amount of items: 2
Items: 
Size: 10671 Color: 7
Size: 5440 Color: 9

Bin 149: 18 of cap free
Amount of items: 3
Items: 
Size: 11192 Color: 18
Size: 4770 Color: 10
Size: 148 Color: 2

Bin 150: 18 of cap free
Amount of items: 2
Items: 
Size: 13291 Color: 4
Size: 2819 Color: 12

Bin 151: 19 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 11
Size: 2781 Color: 4
Size: 128 Color: 12

Bin 152: 19 of cap free
Amount of items: 2
Items: 
Size: 13384 Color: 0
Size: 2725 Color: 9

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 15
Size: 3855 Color: 18

Bin 154: 21 of cap free
Amount of items: 2
Items: 
Size: 14165 Color: 10
Size: 1942 Color: 5

Bin 155: 23 of cap free
Amount of items: 2
Items: 
Size: 14046 Color: 15
Size: 2059 Color: 14

Bin 156: 23 of cap free
Amount of items: 2
Items: 
Size: 14160 Color: 17
Size: 1945 Color: 10

Bin 157: 23 of cap free
Amount of items: 2
Items: 
Size: 14224 Color: 0
Size: 1881 Color: 6

Bin 158: 24 of cap free
Amount of items: 2
Items: 
Size: 13035 Color: 12
Size: 3069 Color: 0

Bin 159: 24 of cap free
Amount of items: 2
Items: 
Size: 13208 Color: 3
Size: 2896 Color: 9

Bin 160: 24 of cap free
Amount of items: 2
Items: 
Size: 14414 Color: 7
Size: 1690 Color: 14

Bin 161: 25 of cap free
Amount of items: 3
Items: 
Size: 9622 Color: 1
Size: 5145 Color: 12
Size: 1336 Color: 2

Bin 162: 28 of cap free
Amount of items: 2
Items: 
Size: 14300 Color: 11
Size: 1800 Color: 3

Bin 163: 29 of cap free
Amount of items: 2
Items: 
Size: 12688 Color: 13
Size: 3411 Color: 18

Bin 164: 30 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 5
Size: 3328 Color: 10

Bin 165: 30 of cap free
Amount of items: 2
Items: 
Size: 13802 Color: 14
Size: 2296 Color: 5

Bin 166: 30 of cap free
Amount of items: 2
Items: 
Size: 14482 Color: 3
Size: 1616 Color: 12

Bin 167: 31 of cap free
Amount of items: 3
Items: 
Size: 10812 Color: 1
Size: 4389 Color: 14
Size: 896 Color: 18

Bin 168: 34 of cap free
Amount of items: 2
Items: 
Size: 11312 Color: 5
Size: 4782 Color: 6

Bin 169: 37 of cap free
Amount of items: 2
Items: 
Size: 13595 Color: 4
Size: 2496 Color: 14

Bin 170: 38 of cap free
Amount of items: 2
Items: 
Size: 11654 Color: 0
Size: 4436 Color: 18

Bin 171: 40 of cap free
Amount of items: 7
Items: 
Size: 8068 Color: 11
Size: 1842 Color: 17
Size: 1532 Color: 1
Size: 1484 Color: 2
Size: 1482 Color: 10
Size: 1156 Color: 13
Size: 524 Color: 0

Bin 172: 42 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 14
Size: 2802 Color: 18

Bin 173: 43 of cap free
Amount of items: 2
Items: 
Size: 12449 Color: 17
Size: 3636 Color: 1

Bin 174: 46 of cap free
Amount of items: 2
Items: 
Size: 13020 Color: 14
Size: 3062 Color: 7

Bin 175: 49 of cap free
Amount of items: 3
Items: 
Size: 9095 Color: 14
Size: 5136 Color: 4
Size: 1848 Color: 2

Bin 176: 50 of cap free
Amount of items: 3
Items: 
Size: 8104 Color: 6
Size: 6702 Color: 3
Size: 1272 Color: 4

Bin 177: 55 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 10
Size: 5768 Color: 19
Size: 2221 Color: 2

Bin 178: 70 of cap free
Amount of items: 2
Items: 
Size: 11756 Color: 18
Size: 4302 Color: 9

Bin 179: 75 of cap free
Amount of items: 2
Items: 
Size: 9665 Color: 14
Size: 6388 Color: 16

Bin 180: 80 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 18
Size: 6728 Color: 15

Bin 181: 84 of cap free
Amount of items: 2
Items: 
Size: 11220 Color: 1
Size: 4824 Color: 9

Bin 182: 88 of cap free
Amount of items: 2
Items: 
Size: 12661 Color: 16
Size: 3379 Color: 1

Bin 183: 92 of cap free
Amount of items: 2
Items: 
Size: 10520 Color: 3
Size: 5516 Color: 8

Bin 184: 102 of cap free
Amount of items: 2
Items: 
Size: 12652 Color: 5
Size: 3374 Color: 0

Bin 185: 103 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 13
Size: 6721 Color: 7
Size: 1136 Color: 2

Bin 186: 105 of cap free
Amount of items: 2
Items: 
Size: 12244 Color: 8
Size: 3779 Color: 3

Bin 187: 106 of cap free
Amount of items: 36
Items: 
Size: 592 Color: 1
Size: 576 Color: 14
Size: 576 Color: 8
Size: 576 Color: 4
Size: 556 Color: 7
Size: 520 Color: 18
Size: 520 Color: 8
Size: 520 Color: 0
Size: 512 Color: 16
Size: 512 Color: 13
Size: 512 Color: 12
Size: 512 Color: 8
Size: 512 Color: 0
Size: 468 Color: 16
Size: 464 Color: 18
Size: 464 Color: 9
Size: 448 Color: 16
Size: 448 Color: 9
Size: 446 Color: 15
Size: 422 Color: 7
Size: 416 Color: 17
Size: 410 Color: 15
Size: 404 Color: 19
Size: 388 Color: 7
Size: 384 Color: 3
Size: 368 Color: 19
Size: 368 Color: 5
Size: 368 Color: 5
Size: 368 Color: 4
Size: 352 Color: 13
Size: 352 Color: 13
Size: 352 Color: 10
Size: 352 Color: 6
Size: 344 Color: 0
Size: 320 Color: 6
Size: 320 Color: 5

Bin 188: 112 of cap free
Amount of items: 2
Items: 
Size: 10144 Color: 14
Size: 5872 Color: 3

Bin 189: 113 of cap free
Amount of items: 2
Items: 
Size: 12087 Color: 18
Size: 3928 Color: 15

Bin 190: 114 of cap free
Amount of items: 2
Items: 
Size: 11077 Color: 4
Size: 4937 Color: 17

Bin 191: 116 of cap free
Amount of items: 2
Items: 
Size: 9516 Color: 4
Size: 6496 Color: 12

Bin 192: 118 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 13
Size: 6372 Color: 0

Bin 193: 125 of cap free
Amount of items: 3
Items: 
Size: 9955 Color: 11
Size: 5384 Color: 4
Size: 664 Color: 2

Bin 194: 148 of cap free
Amount of items: 3
Items: 
Size: 8074 Color: 15
Size: 6364 Color: 0
Size: 1542 Color: 18

Bin 195: 168 of cap free
Amount of items: 2
Items: 
Size: 9224 Color: 1
Size: 6736 Color: 18

Bin 196: 194 of cap free
Amount of items: 3
Items: 
Size: 9256 Color: 1
Size: 5846 Color: 17
Size: 832 Color: 2

Bin 197: 214 of cap free
Amount of items: 2
Items: 
Size: 9190 Color: 10
Size: 6724 Color: 18

Bin 198: 224 of cap free
Amount of items: 2
Items: 
Size: 8484 Color: 15
Size: 7420 Color: 14

Bin 199: 12236 of cap free
Amount of items: 13
Items: 
Size: 352 Color: 11
Size: 352 Color: 10
Size: 336 Color: 1
Size: 320 Color: 0
Size: 304 Color: 10
Size: 304 Color: 9
Size: 288 Color: 6
Size: 288 Color: 5
Size: 280 Color: 13
Size: 280 Color: 4
Size: 272 Color: 8
Size: 272 Color: 3
Size: 244 Color: 15

Total size: 3193344
Total free space: 16128

