Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 444665 Color: 0
Size: 278639 Color: 1
Size: 276697 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 409097 Color: 1
Size: 325450 Color: 0
Size: 265454 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 467479 Color: 1
Size: 268185 Color: 0
Size: 264337 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 426256 Color: 0
Size: 287073 Color: 0
Size: 286672 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 374765 Color: 1
Size: 330277 Color: 0
Size: 294959 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 371137 Color: 0
Size: 349775 Color: 1
Size: 279089 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 459885 Color: 0
Size: 287145 Color: 1
Size: 252971 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 493763 Color: 1
Size: 254026 Color: 0
Size: 252212 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 409290 Color: 0
Size: 326880 Color: 1
Size: 263831 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 372516 Color: 0
Size: 360499 Color: 1
Size: 266986 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 451043 Color: 1
Size: 291437 Color: 0
Size: 257521 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 381935 Color: 1
Size: 343300 Color: 1
Size: 274766 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 409891 Color: 0
Size: 309527 Color: 0
Size: 280583 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 404119 Color: 1
Size: 305456 Color: 0
Size: 290426 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 447764 Color: 1
Size: 296637 Color: 1
Size: 255600 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 452284 Color: 1
Size: 278709 Color: 0
Size: 269008 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 445230 Color: 0
Size: 302682 Color: 0
Size: 252089 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 470723 Color: 1
Size: 268807 Color: 0
Size: 260471 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 426633 Color: 0
Size: 312101 Color: 0
Size: 261267 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 444506 Color: 0
Size: 293211 Color: 1
Size: 262284 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 484783 Color: 0
Size: 262669 Color: 0
Size: 252549 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 437901 Color: 1
Size: 282669 Color: 0
Size: 279431 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 406321 Color: 1
Size: 319497 Color: 0
Size: 274183 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 429995 Color: 1
Size: 306138 Color: 0
Size: 263868 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 371955 Color: 0
Size: 365413 Color: 1
Size: 262633 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 347141 Color: 0
Size: 326532 Color: 1
Size: 326328 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 499362 Color: 0
Size: 250572 Color: 1
Size: 250067 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 359183 Color: 0
Size: 352801 Color: 1
Size: 288017 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 377281 Color: 1
Size: 339069 Color: 1
Size: 283651 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 442840 Color: 1
Size: 288254 Color: 0
Size: 268907 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 413230 Color: 1
Size: 324091 Color: 1
Size: 262680 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 379742 Color: 1
Size: 347082 Color: 0
Size: 273177 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 406915 Color: 0
Size: 339683 Color: 0
Size: 253403 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 369758 Color: 0
Size: 349774 Color: 1
Size: 280469 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 456441 Color: 1
Size: 275108 Color: 0
Size: 268452 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 379793 Color: 0
Size: 327106 Color: 1
Size: 293102 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 360862 Color: 1
Size: 343833 Color: 1
Size: 295306 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 431491 Color: 0
Size: 314610 Color: 1
Size: 253900 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 439266 Color: 1
Size: 293734 Color: 1
Size: 267001 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 499558 Color: 1
Size: 250325 Color: 0
Size: 250118 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 375805 Color: 1
Size: 370324 Color: 1
Size: 253872 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 383519 Color: 1
Size: 320134 Color: 0
Size: 296348 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 451260 Color: 0
Size: 295132 Color: 0
Size: 253609 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 408216 Color: 0
Size: 316350 Color: 1
Size: 275435 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 496577 Color: 0
Size: 251791 Color: 1
Size: 251633 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 399444 Color: 1
Size: 328855 Color: 0
Size: 271702 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 392898 Color: 0
Size: 305602 Color: 1
Size: 301501 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 391801 Color: 1
Size: 314488 Color: 0
Size: 293712 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 475995 Color: 0
Size: 263941 Color: 1
Size: 260065 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 399745 Color: 0
Size: 316623 Color: 1
Size: 283633 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 404790 Color: 0
Size: 337166 Color: 0
Size: 258045 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 436766 Color: 1
Size: 312825 Color: 0
Size: 250410 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 383945 Color: 1
Size: 365662 Color: 0
Size: 250394 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 474779 Color: 0
Size: 267896 Color: 1
Size: 257326 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 402593 Color: 1
Size: 345702 Color: 1
Size: 251706 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 410670 Color: 1
Size: 323887 Color: 0
Size: 265444 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 460359 Color: 0
Size: 286646 Color: 1
Size: 252996 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 435586 Color: 1
Size: 289275 Color: 1
Size: 275140 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 397201 Color: 1
Size: 339857 Color: 0
Size: 262943 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 404085 Color: 0
Size: 298098 Color: 0
Size: 297818 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 471402 Color: 1
Size: 273037 Color: 0
Size: 255562 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 431018 Color: 1
Size: 316472 Color: 0
Size: 252511 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 378792 Color: 1
Size: 340621 Color: 0
Size: 280588 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 378191 Color: 1
Size: 329315 Color: 0
Size: 292495 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 366191 Color: 1
Size: 354244 Color: 0
Size: 279566 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 387669 Color: 1
Size: 319196 Color: 0
Size: 293136 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 462515 Color: 1
Size: 274416 Color: 0
Size: 263070 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 349199 Color: 0
Size: 345185 Color: 1
Size: 305617 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 402386 Color: 0
Size: 308291 Color: 1
Size: 289324 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 498581 Color: 1
Size: 250932 Color: 1
Size: 250488 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 436059 Color: 1
Size: 299017 Color: 0
Size: 264925 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 438037 Color: 1
Size: 285954 Color: 0
Size: 276010 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 375706 Color: 1
Size: 357489 Color: 1
Size: 266806 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 402707 Color: 1
Size: 329451 Color: 1
Size: 267843 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 460919 Color: 1
Size: 287102 Color: 0
Size: 251980 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 406184 Color: 1
Size: 333595 Color: 0
Size: 260222 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 498990 Color: 1
Size: 250646 Color: 0
Size: 250365 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 486717 Color: 1
Size: 257435 Color: 0
Size: 255849 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 379409 Color: 0
Size: 323181 Color: 0
Size: 297411 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 466715 Color: 0
Size: 275881 Color: 1
Size: 257405 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 448640 Color: 0
Size: 293408 Color: 1
Size: 257953 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 343753 Color: 0
Size: 331473 Color: 1
Size: 324775 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 441539 Color: 0
Size: 298169 Color: 1
Size: 260293 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 433229 Color: 0
Size: 312674 Color: 1
Size: 254098 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 410378 Color: 0
Size: 316125 Color: 1
Size: 273498 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 348578 Color: 1
Size: 348549 Color: 0
Size: 302874 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 347609 Color: 0
Size: 342623 Color: 1
Size: 309769 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 406832 Color: 0
Size: 333104 Color: 1
Size: 260065 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 435770 Color: 1
Size: 291348 Color: 0
Size: 272883 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 348977 Color: 0
Size: 327815 Color: 1
Size: 323209 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 342305 Color: 0
Size: 336669 Color: 1
Size: 321027 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 361312 Color: 1
Size: 321497 Color: 0
Size: 317192 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 463374 Color: 1
Size: 275219 Color: 0
Size: 261408 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 457209 Color: 1
Size: 287398 Color: 0
Size: 255394 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 394827 Color: 1
Size: 327262 Color: 0
Size: 277912 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 407217 Color: 1
Size: 309304 Color: 0
Size: 283480 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 430786 Color: 1
Size: 293872 Color: 1
Size: 275343 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 360847 Color: 0
Size: 336162 Color: 0
Size: 302992 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 362900 Color: 1
Size: 334788 Color: 1
Size: 302313 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 404657 Color: 0
Size: 326358 Color: 0
Size: 268986 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 435321 Color: 1
Size: 282763 Color: 1
Size: 281917 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 401264 Color: 1
Size: 324150 Color: 1
Size: 274587 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 367136 Color: 1
Size: 352989 Color: 0
Size: 279876 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 364263 Color: 0
Size: 344779 Color: 0
Size: 290959 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 491199 Color: 1
Size: 258091 Color: 0
Size: 250711 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 426354 Color: 0
Size: 300755 Color: 1
Size: 272892 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 384803 Color: 0
Size: 324753 Color: 0
Size: 290445 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 423235 Color: 0
Size: 300423 Color: 0
Size: 276343 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 467512 Color: 1
Size: 277289 Color: 0
Size: 255200 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 374957 Color: 1
Size: 353153 Color: 0
Size: 271891 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 373372 Color: 1
Size: 349285 Color: 0
Size: 277344 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 429461 Color: 0
Size: 317916 Color: 0
Size: 252624 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 380671 Color: 1
Size: 350764 Color: 1
Size: 268566 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 371887 Color: 0
Size: 316444 Color: 1
Size: 311670 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 383947 Color: 1
Size: 327503 Color: 1
Size: 288551 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 382934 Color: 1
Size: 333136 Color: 1
Size: 283931 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 426361 Color: 0
Size: 303302 Color: 1
Size: 270338 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 426287 Color: 0
Size: 308962 Color: 1
Size: 264752 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 401445 Color: 0
Size: 345983 Color: 1
Size: 252573 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 418122 Color: 0
Size: 329468 Color: 1
Size: 252411 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 479419 Color: 1
Size: 265789 Color: 0
Size: 254793 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 409764 Color: 0
Size: 309980 Color: 1
Size: 280257 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 491840 Color: 0
Size: 255777 Color: 1
Size: 252384 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 402471 Color: 0
Size: 300222 Color: 1
Size: 297308 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 446623 Color: 1
Size: 302906 Color: 0
Size: 250472 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 404884 Color: 0
Size: 321650 Color: 0
Size: 273467 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 485551 Color: 0
Size: 262979 Color: 1
Size: 251471 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 419418 Color: 1
Size: 291385 Color: 0
Size: 289198 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 430525 Color: 0
Size: 314085 Color: 1
Size: 255391 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 391903 Color: 0
Size: 353123 Color: 1
Size: 254975 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 345741 Color: 1
Size: 331755 Color: 0
Size: 322505 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 366317 Color: 0
Size: 344323 Color: 1
Size: 289361 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 445520 Color: 1
Size: 286752 Color: 0
Size: 267729 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 379199 Color: 1
Size: 370681 Color: 0
Size: 250121 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 404447 Color: 0
Size: 316561 Color: 1
Size: 278993 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 461588 Color: 1
Size: 271767 Color: 0
Size: 266646 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 455118 Color: 1
Size: 282159 Color: 0
Size: 262724 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 401827 Color: 0
Size: 328396 Color: 0
Size: 269778 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 444638 Color: 1
Size: 282584 Color: 0
Size: 272779 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 459238 Color: 1
Size: 277214 Color: 0
Size: 263549 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 372697 Color: 1
Size: 355019 Color: 0
Size: 272285 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 478123 Color: 0
Size: 263373 Color: 1
Size: 258505 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 419087 Color: 0
Size: 330799 Color: 1
Size: 250115 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 390656 Color: 1
Size: 339223 Color: 0
Size: 270122 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 382828 Color: 0
Size: 323460 Color: 0
Size: 293713 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 395361 Color: 1
Size: 337317 Color: 0
Size: 267323 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 392948 Color: 0
Size: 328725 Color: 1
Size: 278328 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 413747 Color: 1
Size: 294211 Color: 0
Size: 292043 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 401519 Color: 1
Size: 309814 Color: 1
Size: 288668 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 395990 Color: 0
Size: 333401 Color: 1
Size: 270610 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 343525 Color: 0
Size: 330162 Color: 1
Size: 326314 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 380567 Color: 0
Size: 344026 Color: 0
Size: 275408 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 400644 Color: 0
Size: 313934 Color: 1
Size: 285423 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 486414 Color: 1
Size: 257456 Color: 0
Size: 256131 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 391499 Color: 0
Size: 308636 Color: 0
Size: 299866 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 421926 Color: 0
Size: 319668 Color: 0
Size: 258407 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 473590 Color: 1
Size: 273011 Color: 0
Size: 253400 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 378012 Color: 1
Size: 320146 Color: 0
Size: 301843 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 423861 Color: 0
Size: 303291 Color: 1
Size: 272849 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 393645 Color: 0
Size: 325925 Color: 1
Size: 280431 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 410981 Color: 0
Size: 335146 Color: 0
Size: 253874 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 393172 Color: 1
Size: 342221 Color: 0
Size: 264608 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 458691 Color: 1
Size: 271305 Color: 0
Size: 270005 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 405404 Color: 0
Size: 299321 Color: 0
Size: 295276 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 373535 Color: 0
Size: 322054 Color: 1
Size: 304412 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 387542 Color: 1
Size: 358299 Color: 0
Size: 254160 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 384831 Color: 1
Size: 325807 Color: 0
Size: 289363 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 463562 Color: 1
Size: 283656 Color: 0
Size: 252783 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 479074 Color: 0
Size: 266733 Color: 0
Size: 254194 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 398439 Color: 1
Size: 307262 Color: 1
Size: 294300 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 377641 Color: 0
Size: 353669 Color: 0
Size: 268691 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 374041 Color: 0
Size: 351428 Color: 1
Size: 274532 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 493856 Color: 0
Size: 254946 Color: 1
Size: 251199 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 490066 Color: 1
Size: 259515 Color: 0
Size: 250420 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 377964 Color: 0
Size: 320383 Color: 1
Size: 301654 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 443229 Color: 1
Size: 282965 Color: 0
Size: 273807 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 394035 Color: 0
Size: 346398 Color: 0
Size: 259568 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 398953 Color: 1
Size: 306337 Color: 1
Size: 294711 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 397254 Color: 1
Size: 315887 Color: 0
Size: 286860 Color: 1

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 490556 Color: 0
Size: 256529 Color: 1
Size: 252916 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 479036 Color: 1
Size: 263389 Color: 0
Size: 257576 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 420281 Color: 1
Size: 296987 Color: 1
Size: 282733 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 449324 Color: 0
Size: 291607 Color: 1
Size: 259070 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 395032 Color: 1
Size: 347740 Color: 0
Size: 257229 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 405870 Color: 0
Size: 308402 Color: 1
Size: 285729 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 358103 Color: 0
Size: 338242 Color: 1
Size: 303656 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 385085 Color: 1
Size: 318882 Color: 0
Size: 296034 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 404380 Color: 0
Size: 330248 Color: 1
Size: 265373 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 438937 Color: 0
Size: 290793 Color: 0
Size: 270271 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 371884 Color: 0
Size: 320926 Color: 1
Size: 307191 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 436265 Color: 0
Size: 313437 Color: 1
Size: 250299 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 427041 Color: 0
Size: 293020 Color: 1
Size: 279940 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 396219 Color: 0
Size: 341316 Color: 1
Size: 262466 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 390006 Color: 1
Size: 334191 Color: 0
Size: 275804 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 341380 Color: 0
Size: 337583 Color: 1
Size: 321038 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 423739 Color: 0
Size: 307035 Color: 1
Size: 269227 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 421118 Color: 0
Size: 306149 Color: 0
Size: 272734 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 459166 Color: 1
Size: 277645 Color: 1
Size: 263190 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 379240 Color: 0
Size: 321709 Color: 1
Size: 299052 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 387475 Color: 0
Size: 328642 Color: 0
Size: 283884 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 356814 Color: 0
Size: 344106 Color: 0
Size: 299081 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 380187 Color: 0
Size: 318725 Color: 1
Size: 301089 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 471982 Color: 0
Size: 273733 Color: 1
Size: 254286 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 485947 Color: 1
Size: 259387 Color: 1
Size: 254667 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 410142 Color: 0
Size: 319122 Color: 1
Size: 270737 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 398614 Color: 0
Size: 331755 Color: 1
Size: 269632 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 481008 Color: 1
Size: 263850 Color: 0
Size: 255143 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 389274 Color: 0
Size: 338604 Color: 1
Size: 272123 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 411628 Color: 0
Size: 320107 Color: 0
Size: 268266 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 371623 Color: 1
Size: 330151 Color: 0
Size: 298227 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 380999 Color: 0
Size: 341061 Color: 1
Size: 277941 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 393334 Color: 1
Size: 326649 Color: 0
Size: 280018 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 441889 Color: 1
Size: 302245 Color: 1
Size: 255867 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 423968 Color: 0
Size: 320133 Color: 1
Size: 255900 Color: 1

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 415646 Color: 0
Size: 328234 Color: 0
Size: 256121 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 435373 Color: 0
Size: 297263 Color: 0
Size: 267365 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 399642 Color: 0
Size: 345009 Color: 1
Size: 255350 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 400257 Color: 0
Size: 309693 Color: 0
Size: 290051 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 378267 Color: 0
Size: 353782 Color: 1
Size: 267952 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 449349 Color: 0
Size: 299739 Color: 0
Size: 250913 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 409531 Color: 1
Size: 334069 Color: 1
Size: 256401 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 489490 Color: 0
Size: 260242 Color: 1
Size: 250269 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 480695 Color: 0
Size: 266915 Color: 1
Size: 252391 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 386675 Color: 0
Size: 357667 Color: 1
Size: 255659 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 484007 Color: 1
Size: 262451 Color: 0
Size: 253543 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 499874 Color: 1
Size: 250067 Color: 0
Size: 250060 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 490703 Color: 0
Size: 255444 Color: 1
Size: 253854 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 426117 Color: 0
Size: 323867 Color: 0
Size: 250017 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 379217 Color: 1
Size: 327274 Color: 1
Size: 293510 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 466503 Color: 1
Size: 280243 Color: 0
Size: 253255 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 402230 Color: 1
Size: 342539 Color: 1
Size: 255232 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 381920 Color: 0
Size: 361279 Color: 0
Size: 256802 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 368549 Color: 1
Size: 326736 Color: 1
Size: 304716 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 366486 Color: 1
Size: 352959 Color: 0
Size: 280556 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 493126 Color: 1
Size: 255787 Color: 0
Size: 251088 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 394948 Color: 0
Size: 312045 Color: 1
Size: 293008 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 444944 Color: 0
Size: 299459 Color: 1
Size: 255598 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 420117 Color: 0
Size: 315894 Color: 1
Size: 263990 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 470077 Color: 1
Size: 269988 Color: 0
Size: 259936 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 464935 Color: 1
Size: 283014 Color: 0
Size: 252052 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 449121 Color: 1
Size: 283483 Color: 0
Size: 267397 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 419634 Color: 1
Size: 293929 Color: 0
Size: 286438 Color: 1

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 374860 Color: 0
Size: 360914 Color: 1
Size: 264227 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 475352 Color: 0
Size: 269494 Color: 0
Size: 255155 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 381884 Color: 1
Size: 316844 Color: 1
Size: 301273 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 379242 Color: 0
Size: 339309 Color: 1
Size: 281450 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 436479 Color: 1
Size: 284113 Color: 1
Size: 279409 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 405430 Color: 0
Size: 315884 Color: 0
Size: 278687 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 478151 Color: 0
Size: 266832 Color: 1
Size: 255018 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 484798 Color: 1
Size: 261309 Color: 1
Size: 253894 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 436871 Color: 0
Size: 290360 Color: 0
Size: 272770 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 385642 Color: 0
Size: 356730 Color: 1
Size: 257629 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 432495 Color: 0
Size: 294672 Color: 0
Size: 272834 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 412323 Color: 1
Size: 296819 Color: 0
Size: 290859 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 361943 Color: 0
Size: 349241 Color: 1
Size: 288817 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 372113 Color: 0
Size: 343051 Color: 0
Size: 284837 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 466263 Color: 1
Size: 272723 Color: 0
Size: 261015 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 392448 Color: 0
Size: 355262 Color: 1
Size: 252291 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 416805 Color: 0
Size: 307701 Color: 1
Size: 275495 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 375111 Color: 0
Size: 347972 Color: 1
Size: 276918 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 413851 Color: 0
Size: 314002 Color: 1
Size: 272148 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 352798 Color: 1
Size: 335119 Color: 0
Size: 312084 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 352708 Color: 0
Size: 328819 Color: 0
Size: 318474 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 431331 Color: 1
Size: 310401 Color: 0
Size: 258269 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 476466 Color: 1
Size: 264554 Color: 0
Size: 258981 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 373755 Color: 1
Size: 347728 Color: 0
Size: 278518 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 421687 Color: 1
Size: 315875 Color: 1
Size: 262439 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 412855 Color: 0
Size: 314476 Color: 0
Size: 272670 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 461203 Color: 0
Size: 271883 Color: 0
Size: 266915 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 389896 Color: 1
Size: 318903 Color: 0
Size: 291202 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 343369 Color: 0
Size: 334419 Color: 1
Size: 322213 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 370053 Color: 0
Size: 315205 Color: 1
Size: 314743 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 387361 Color: 1
Size: 332519 Color: 0
Size: 280121 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 460781 Color: 1
Size: 270912 Color: 0
Size: 268308 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 367850 Color: 1
Size: 318854 Color: 1
Size: 313297 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 480394 Color: 0
Size: 268486 Color: 0
Size: 251121 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 396344 Color: 0
Size: 330939 Color: 0
Size: 272718 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 443360 Color: 0
Size: 289757 Color: 0
Size: 266884 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 408248 Color: 0
Size: 333156 Color: 1
Size: 258597 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 420626 Color: 0
Size: 314520 Color: 1
Size: 264855 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 376868 Color: 0
Size: 341295 Color: 1
Size: 281838 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 478058 Color: 1
Size: 264227 Color: 0
Size: 257716 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 483167 Color: 0
Size: 264014 Color: 1
Size: 252820 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 404275 Color: 1
Size: 319032 Color: 0
Size: 276694 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 499307 Color: 0
Size: 250451 Color: 1
Size: 250243 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 409677 Color: 1
Size: 337316 Color: 1
Size: 253008 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 472966 Color: 0
Size: 270658 Color: 0
Size: 256377 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 435607 Color: 0
Size: 289078 Color: 0
Size: 275316 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 408587 Color: 0
Size: 322611 Color: 1
Size: 268803 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 371500 Color: 0
Size: 347483 Color: 0
Size: 281018 Color: 1

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 382468 Color: 1
Size: 329553 Color: 0
Size: 287980 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 473929 Color: 0
Size: 275676 Color: 1
Size: 250396 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 495377 Color: 0
Size: 252899 Color: 1
Size: 251725 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 461960 Color: 0
Size: 269935 Color: 0
Size: 268106 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 476276 Color: 1
Size: 265592 Color: 0
Size: 258133 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 453874 Color: 1
Size: 291936 Color: 1
Size: 254191 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 476839 Color: 0
Size: 267755 Color: 1
Size: 255407 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 444184 Color: 1
Size: 301065 Color: 0
Size: 254752 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 450497 Color: 1
Size: 291427 Color: 0
Size: 258077 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 374497 Color: 1
Size: 320857 Color: 0
Size: 304647 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 462833 Color: 0
Size: 271435 Color: 1
Size: 265733 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 426143 Color: 0
Size: 301833 Color: 1
Size: 272025 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 380868 Color: 1
Size: 340649 Color: 0
Size: 278484 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 383390 Color: 1
Size: 323242 Color: 0
Size: 293369 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 467005 Color: 0
Size: 277995 Color: 1
Size: 255001 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 492472 Color: 0
Size: 254290 Color: 0
Size: 253239 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 426189 Color: 0
Size: 295657 Color: 1
Size: 278155 Color: 1

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 365259 Color: 1
Size: 357299 Color: 0
Size: 277443 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 444519 Color: 0
Size: 278982 Color: 1
Size: 276500 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 375967 Color: 1
Size: 355812 Color: 0
Size: 268222 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 378294 Color: 1
Size: 340089 Color: 0
Size: 281618 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 386080 Color: 0
Size: 324323 Color: 0
Size: 289598 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 445479 Color: 0
Size: 282318 Color: 0
Size: 272204 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 375831 Color: 1
Size: 329125 Color: 0
Size: 295045 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 355941 Color: 1
Size: 350421 Color: 0
Size: 293639 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 410418 Color: 0
Size: 320236 Color: 1
Size: 269347 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 465294 Color: 1
Size: 271055 Color: 1
Size: 263652 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 370992 Color: 1
Size: 333080 Color: 0
Size: 295929 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 476460 Color: 0
Size: 265115 Color: 0
Size: 258426 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 448099 Color: 0
Size: 300731 Color: 1
Size: 251171 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 488730 Color: 0
Size: 257478 Color: 1
Size: 253793 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 388508 Color: 1
Size: 329512 Color: 0
Size: 281981 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 387292 Color: 0
Size: 349598 Color: 1
Size: 263111 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 468829 Color: 1
Size: 280432 Color: 0
Size: 250740 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 402462 Color: 0
Size: 335220 Color: 1
Size: 262319 Color: 1

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 363107 Color: 0
Size: 336663 Color: 1
Size: 300231 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 478549 Color: 0
Size: 263887 Color: 0
Size: 257565 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 392709 Color: 0
Size: 335368 Color: 1
Size: 271924 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 481866 Color: 1
Size: 264588 Color: 0
Size: 253547 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 431412 Color: 0
Size: 307822 Color: 1
Size: 260767 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 398034 Color: 0
Size: 319155 Color: 1
Size: 282812 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 359328 Color: 1
Size: 356611 Color: 0
Size: 284062 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 450148 Color: 0
Size: 296853 Color: 0
Size: 253000 Color: 1

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 421066 Color: 1
Size: 300228 Color: 0
Size: 278707 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 458775 Color: 1
Size: 273583 Color: 0
Size: 267643 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 448831 Color: 1
Size: 289099 Color: 0
Size: 262071 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 430468 Color: 1
Size: 284894 Color: 0
Size: 284639 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 392891 Color: 1
Size: 342963 Color: 0
Size: 264147 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 372763 Color: 0
Size: 367277 Color: 0
Size: 259961 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 402050 Color: 0
Size: 299524 Color: 1
Size: 298427 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 380945 Color: 1
Size: 317843 Color: 1
Size: 301213 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 407099 Color: 0
Size: 300608 Color: 1
Size: 292294 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 397940 Color: 0
Size: 314256 Color: 1
Size: 287805 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 412677 Color: 1
Size: 314150 Color: 1
Size: 273174 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 387281 Color: 1
Size: 326681 Color: 1
Size: 286039 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 460148 Color: 0
Size: 277277 Color: 1
Size: 262576 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 356640 Color: 0
Size: 344727 Color: 1
Size: 298634 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 400647 Color: 1
Size: 333586 Color: 0
Size: 265768 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 377300 Color: 0
Size: 359101 Color: 0
Size: 263600 Color: 1

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 432789 Color: 1
Size: 306848 Color: 0
Size: 260364 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 485394 Color: 1
Size: 261514 Color: 0
Size: 253093 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 383697 Color: 0
Size: 348287 Color: 1
Size: 268017 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 446170 Color: 1
Size: 277890 Color: 0
Size: 275941 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 386087 Color: 1
Size: 332395 Color: 1
Size: 281519 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 440771 Color: 1
Size: 309020 Color: 0
Size: 250210 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 396203 Color: 1
Size: 317173 Color: 1
Size: 286625 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 345475 Color: 0
Size: 332813 Color: 1
Size: 321713 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 408594 Color: 1
Size: 316526 Color: 0
Size: 274881 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 487650 Color: 0
Size: 256672 Color: 0
Size: 255679 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 413216 Color: 1
Size: 309755 Color: 0
Size: 277030 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 343709 Color: 0
Size: 338226 Color: 1
Size: 318066 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 348159 Color: 1
Size: 340878 Color: 0
Size: 310964 Color: 1

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 407905 Color: 1
Size: 333045 Color: 0
Size: 259051 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 391267 Color: 1
Size: 357515 Color: 0
Size: 251219 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 434649 Color: 0
Size: 286349 Color: 1
Size: 279003 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 461513 Color: 0
Size: 270435 Color: 0
Size: 268053 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 472920 Color: 1
Size: 274689 Color: 0
Size: 252392 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 412456 Color: 0
Size: 327640 Color: 0
Size: 259905 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 447398 Color: 0
Size: 287218 Color: 0
Size: 265385 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 414846 Color: 1
Size: 327759 Color: 0
Size: 257396 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 399324 Color: 1
Size: 306094 Color: 1
Size: 294583 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 369219 Color: 1
Size: 334164 Color: 0
Size: 296618 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 366397 Color: 1
Size: 364551 Color: 1
Size: 269053 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 462388 Color: 1
Size: 281103 Color: 0
Size: 256510 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 465855 Color: 0
Size: 281359 Color: 1
Size: 252787 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 403840 Color: 0
Size: 339411 Color: 1
Size: 256750 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 408642 Color: 1
Size: 338193 Color: 1
Size: 253166 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 436449 Color: 0
Size: 313522 Color: 1
Size: 250030 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 424708 Color: 1
Size: 292309 Color: 0
Size: 282984 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 400022 Color: 0
Size: 308249 Color: 0
Size: 291730 Color: 1

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 358481 Color: 1
Size: 335543 Color: 1
Size: 305977 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 400527 Color: 0
Size: 345946 Color: 1
Size: 253528 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 347233 Color: 1
Size: 346237 Color: 1
Size: 306531 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 374574 Color: 0
Size: 355600 Color: 0
Size: 269827 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 396983 Color: 1
Size: 327305 Color: 0
Size: 275713 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 372894 Color: 1
Size: 315157 Color: 1
Size: 311950 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 435888 Color: 0
Size: 293838 Color: 0
Size: 270275 Color: 1

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 368570 Color: 0
Size: 360611 Color: 1
Size: 270820 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 449837 Color: 0
Size: 281715 Color: 1
Size: 268449 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 411716 Color: 0
Size: 332198 Color: 1
Size: 256087 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 439181 Color: 1
Size: 285661 Color: 1
Size: 275159 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 442589 Color: 1
Size: 298186 Color: 0
Size: 259226 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 470219 Color: 0
Size: 269668 Color: 1
Size: 260114 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 350821 Color: 1
Size: 324918 Color: 0
Size: 324262 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 361251 Color: 1
Size: 319647 Color: 0
Size: 319103 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 355425 Color: 1
Size: 329152 Color: 0
Size: 315424 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 477937 Color: 0
Size: 270812 Color: 0
Size: 251252 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 339788 Color: 1
Size: 333111 Color: 0
Size: 327102 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 465469 Color: 0
Size: 276136 Color: 1
Size: 258396 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 413198 Color: 0
Size: 295447 Color: 1
Size: 291355 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 445568 Color: 1
Size: 295804 Color: 0
Size: 258628 Color: 1

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 471065 Color: 1
Size: 274603 Color: 0
Size: 254332 Color: 1

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 484384 Color: 1
Size: 257879 Color: 0
Size: 257737 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 447106 Color: 0
Size: 280662 Color: 1
Size: 272232 Color: 1

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 376477 Color: 0
Size: 338933 Color: 0
Size: 284590 Color: 1

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 355307 Color: 1
Size: 344898 Color: 0
Size: 299795 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 412487 Color: 1
Size: 304380 Color: 0
Size: 283133 Color: 1

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 431313 Color: 1
Size: 294032 Color: 0
Size: 274655 Color: 1

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 400068 Color: 1
Size: 330683 Color: 1
Size: 269249 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 418684 Color: 1
Size: 294002 Color: 0
Size: 287314 Color: 1

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 428928 Color: 1
Size: 290379 Color: 1
Size: 280693 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 365471 Color: 1
Size: 362830 Color: 0
Size: 271699 Color: 1

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 453536 Color: 0
Size: 280169 Color: 1
Size: 266295 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 392979 Color: 0
Size: 356662 Color: 1
Size: 250359 Color: 1

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 372531 Color: 0
Size: 329750 Color: 0
Size: 297719 Color: 1

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 491217 Color: 0
Size: 258639 Color: 0
Size: 250144 Color: 1

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 420985 Color: 1
Size: 320883 Color: 1
Size: 258132 Color: 0

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 468715 Color: 0
Size: 268167 Color: 1
Size: 263118 Color: 1

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 419259 Color: 1
Size: 305979 Color: 0
Size: 274762 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 448785 Color: 1
Size: 278276 Color: 1
Size: 272939 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 402181 Color: 1
Size: 318698 Color: 0
Size: 279121 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 383064 Color: 1
Size: 329473 Color: 0
Size: 287463 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 402744 Color: 1
Size: 343795 Color: 1
Size: 253461 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 421746 Color: 0
Size: 294958 Color: 1
Size: 283296 Color: 1

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 354821 Color: 1
Size: 334738 Color: 0
Size: 310441 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 394998 Color: 0
Size: 341504 Color: 0
Size: 263498 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 369646 Color: 0
Size: 338423 Color: 1
Size: 291931 Color: 1

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 415924 Color: 1
Size: 333630 Color: 0
Size: 250446 Color: 1

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 433745 Color: 1
Size: 300034 Color: 0
Size: 266221 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 457875 Color: 1
Size: 289839 Color: 1
Size: 252286 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 406290 Color: 1
Size: 302890 Color: 1
Size: 290820 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 396284 Color: 0
Size: 335541 Color: 0
Size: 268175 Color: 1

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 388026 Color: 1
Size: 320492 Color: 0
Size: 291482 Color: 1

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 393617 Color: 0
Size: 333438 Color: 1
Size: 272945 Color: 1

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 457747 Color: 1
Size: 278598 Color: 0
Size: 263655 Color: 0

Bin 436: 2 of cap free
Amount of items: 3
Items: 
Size: 424926 Color: 1
Size: 303765 Color: 1
Size: 271308 Color: 0

Bin 437: 2 of cap free
Amount of items: 3
Items: 
Size: 386035 Color: 1
Size: 312629 Color: 0
Size: 301335 Color: 0

Bin 438: 2 of cap free
Amount of items: 3
Items: 
Size: 444694 Color: 1
Size: 293903 Color: 0
Size: 261402 Color: 1

Bin 439: 2 of cap free
Amount of items: 3
Items: 
Size: 465923 Color: 1
Size: 269472 Color: 0
Size: 264604 Color: 0

Bin 440: 2 of cap free
Amount of items: 3
Items: 
Size: 379758 Color: 1
Size: 329424 Color: 0
Size: 290817 Color: 0

Bin 441: 2 of cap free
Amount of items: 3
Items: 
Size: 394115 Color: 1
Size: 311516 Color: 0
Size: 294368 Color: 0

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 424886 Color: 0
Size: 318889 Color: 1
Size: 256224 Color: 1

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 367671 Color: 1
Size: 317337 Color: 0
Size: 314991 Color: 0

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 363663 Color: 1
Size: 319006 Color: 1
Size: 317330 Color: 0

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 425913 Color: 0
Size: 313864 Color: 1
Size: 260222 Color: 1

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 344534 Color: 1
Size: 336690 Color: 0
Size: 318775 Color: 0

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 383226 Color: 1
Size: 314660 Color: 1
Size: 302113 Color: 0

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 467328 Color: 0
Size: 276903 Color: 1
Size: 255768 Color: 1

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 378327 Color: 1
Size: 319752 Color: 0
Size: 301920 Color: 0

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 379827 Color: 0
Size: 355086 Color: 1
Size: 265086 Color: 1

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 353134 Color: 0
Size: 337413 Color: 0
Size: 309452 Color: 1

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 420008 Color: 0
Size: 329120 Color: 1
Size: 250871 Color: 0

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 431577 Color: 0
Size: 309357 Color: 1
Size: 259065 Color: 0

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 400238 Color: 0
Size: 329327 Color: 1
Size: 270434 Color: 1

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 395481 Color: 1
Size: 307132 Color: 0
Size: 297386 Color: 1

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 426300 Color: 1
Size: 300586 Color: 0
Size: 273113 Color: 0

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 387551 Color: 0
Size: 348579 Color: 1
Size: 263869 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 377055 Color: 0
Size: 312216 Color: 1
Size: 310728 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 436242 Color: 1
Size: 299813 Color: 1
Size: 263944 Color: 0

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 368455 Color: 0
Size: 355579 Color: 0
Size: 275965 Color: 1

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 392278 Color: 1
Size: 352883 Color: 0
Size: 254838 Color: 0

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 371227 Color: 0
Size: 350928 Color: 1
Size: 277844 Color: 1

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 372721 Color: 0
Size: 320479 Color: 1
Size: 306799 Color: 0

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 383177 Color: 0
Size: 323645 Color: 1
Size: 293177 Color: 1

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 340259 Color: 1
Size: 332986 Color: 1
Size: 326754 Color: 0

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 492192 Color: 1
Size: 257578 Color: 0
Size: 250229 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 447556 Color: 0
Size: 296304 Color: 1
Size: 256139 Color: 0

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 354154 Color: 0
Size: 324521 Color: 0
Size: 321324 Color: 1

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 365073 Color: 0
Size: 317503 Color: 1
Size: 317423 Color: 0

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 431527 Color: 0
Size: 313313 Color: 1
Size: 255159 Color: 0

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 426417 Color: 1
Size: 311818 Color: 0
Size: 261764 Color: 0

Bin 472: 3 of cap free
Amount of items: 3
Items: 
Size: 446253 Color: 0
Size: 301585 Color: 0
Size: 252160 Color: 1

Bin 473: 3 of cap free
Amount of items: 3
Items: 
Size: 410989 Color: 1
Size: 315018 Color: 0
Size: 273991 Color: 1

Bin 474: 3 of cap free
Amount of items: 3
Items: 
Size: 396793 Color: 0
Size: 325611 Color: 1
Size: 277594 Color: 0

Bin 475: 3 of cap free
Amount of items: 3
Items: 
Size: 370351 Color: 0
Size: 367662 Color: 0
Size: 261985 Color: 1

Bin 476: 3 of cap free
Amount of items: 3
Items: 
Size: 423932 Color: 1
Size: 310943 Color: 0
Size: 265123 Color: 0

Bin 477: 3 of cap free
Amount of items: 3
Items: 
Size: 372606 Color: 1
Size: 327186 Color: 1
Size: 300206 Color: 0

Bin 478: 3 of cap free
Amount of items: 3
Items: 
Size: 432948 Color: 0
Size: 291262 Color: 1
Size: 275788 Color: 1

Bin 479: 3 of cap free
Amount of items: 3
Items: 
Size: 346235 Color: 1
Size: 327357 Color: 0
Size: 326406 Color: 0

Bin 480: 3 of cap free
Amount of items: 3
Items: 
Size: 411244 Color: 1
Size: 323197 Color: 0
Size: 265557 Color: 1

Bin 481: 3 of cap free
Amount of items: 3
Items: 
Size: 372155 Color: 1
Size: 350159 Color: 0
Size: 277684 Color: 1

Bin 482: 3 of cap free
Amount of items: 3
Items: 
Size: 386569 Color: 0
Size: 355577 Color: 1
Size: 257852 Color: 0

Bin 483: 3 of cap free
Amount of items: 3
Items: 
Size: 360758 Color: 1
Size: 339432 Color: 0
Size: 299808 Color: 1

Bin 484: 3 of cap free
Amount of items: 3
Items: 
Size: 395177 Color: 1
Size: 337901 Color: 1
Size: 266920 Color: 0

Bin 485: 3 of cap free
Amount of items: 3
Items: 
Size: 420704 Color: 0
Size: 313172 Color: 1
Size: 266122 Color: 0

Bin 486: 3 of cap free
Amount of items: 3
Items: 
Size: 373199 Color: 0
Size: 317566 Color: 0
Size: 309233 Color: 1

Bin 487: 3 of cap free
Amount of items: 3
Items: 
Size: 364702 Color: 0
Size: 327523 Color: 1
Size: 307773 Color: 0

Bin 488: 3 of cap free
Amount of items: 3
Items: 
Size: 438989 Color: 1
Size: 283796 Color: 0
Size: 277213 Color: 0

Bin 489: 3 of cap free
Amount of items: 3
Items: 
Size: 465165 Color: 1
Size: 269523 Color: 0
Size: 265310 Color: 1

Bin 490: 3 of cap free
Amount of items: 3
Items: 
Size: 358297 Color: 0
Size: 353922 Color: 1
Size: 287779 Color: 0

Bin 491: 4 of cap free
Amount of items: 3
Items: 
Size: 488254 Color: 1
Size: 256050 Color: 0
Size: 255693 Color: 0

Bin 492: 4 of cap free
Amount of items: 3
Items: 
Size: 350944 Color: 1
Size: 328269 Color: 0
Size: 320784 Color: 0

Bin 493: 4 of cap free
Amount of items: 3
Items: 
Size: 462518 Color: 1
Size: 280267 Color: 0
Size: 257212 Color: 0

Bin 494: 4 of cap free
Amount of items: 3
Items: 
Size: 435458 Color: 1
Size: 305390 Color: 0
Size: 259149 Color: 1

Bin 495: 4 of cap free
Amount of items: 3
Items: 
Size: 408733 Color: 0
Size: 337812 Color: 1
Size: 253452 Color: 0

Bin 496: 4 of cap free
Amount of items: 3
Items: 
Size: 380592 Color: 0
Size: 358455 Color: 1
Size: 260950 Color: 0

Bin 497: 4 of cap free
Amount of items: 3
Items: 
Size: 458368 Color: 0
Size: 284373 Color: 1
Size: 257256 Color: 0

Bin 498: 4 of cap free
Amount of items: 3
Items: 
Size: 357664 Color: 1
Size: 357294 Color: 0
Size: 285039 Color: 1

Bin 499: 4 of cap free
Amount of items: 3
Items: 
Size: 429031 Color: 0
Size: 313498 Color: 1
Size: 257468 Color: 0

Bin 500: 4 of cap free
Amount of items: 3
Items: 
Size: 400182 Color: 0
Size: 334515 Color: 1
Size: 265300 Color: 1

Bin 501: 4 of cap free
Amount of items: 3
Items: 
Size: 383356 Color: 1
Size: 353964 Color: 1
Size: 262677 Color: 0

Bin 502: 4 of cap free
Amount of items: 3
Items: 
Size: 365889 Color: 1
Size: 323427 Color: 1
Size: 310681 Color: 0

Bin 503: 4 of cap free
Amount of items: 3
Items: 
Size: 371528 Color: 1
Size: 370106 Color: 1
Size: 258363 Color: 0

Bin 504: 4 of cap free
Amount of items: 3
Items: 
Size: 407177 Color: 1
Size: 303343 Color: 1
Size: 289477 Color: 0

Bin 505: 4 of cap free
Amount of items: 3
Items: 
Size: 400760 Color: 0
Size: 305211 Color: 1
Size: 294026 Color: 0

Bin 506: 4 of cap free
Amount of items: 3
Items: 
Size: 470612 Color: 1
Size: 279361 Color: 0
Size: 250024 Color: 0

Bin 507: 4 of cap free
Amount of items: 3
Items: 
Size: 415906 Color: 1
Size: 316766 Color: 0
Size: 267325 Color: 0

Bin 508: 4 of cap free
Amount of items: 3
Items: 
Size: 410269 Color: 0
Size: 319995 Color: 1
Size: 269733 Color: 1

Bin 509: 5 of cap free
Amount of items: 3
Items: 
Size: 497359 Color: 0
Size: 251507 Color: 1
Size: 251130 Color: 1

Bin 510: 5 of cap free
Amount of items: 3
Items: 
Size: 378628 Color: 1
Size: 352279 Color: 1
Size: 269089 Color: 0

Bin 511: 5 of cap free
Amount of items: 3
Items: 
Size: 378830 Color: 0
Size: 341261 Color: 0
Size: 279905 Color: 1

Bin 512: 5 of cap free
Amount of items: 3
Items: 
Size: 423315 Color: 1
Size: 318044 Color: 1
Size: 258637 Color: 0

Bin 513: 5 of cap free
Amount of items: 3
Items: 
Size: 441835 Color: 1
Size: 300304 Color: 0
Size: 257857 Color: 1

Bin 514: 5 of cap free
Amount of items: 3
Items: 
Size: 474444 Color: 0
Size: 273715 Color: 1
Size: 251837 Color: 1

Bin 515: 5 of cap free
Amount of items: 3
Items: 
Size: 398510 Color: 0
Size: 308086 Color: 0
Size: 293400 Color: 1

Bin 516: 5 of cap free
Amount of items: 3
Items: 
Size: 423467 Color: 1
Size: 300407 Color: 1
Size: 276122 Color: 0

Bin 517: 5 of cap free
Amount of items: 3
Items: 
Size: 389633 Color: 1
Size: 337063 Color: 0
Size: 273300 Color: 0

Bin 518: 6 of cap free
Amount of items: 3
Items: 
Size: 373670 Color: 1
Size: 366177 Color: 1
Size: 260148 Color: 0

Bin 519: 6 of cap free
Amount of items: 3
Items: 
Size: 489995 Color: 1
Size: 256394 Color: 0
Size: 253606 Color: 1

Bin 520: 6 of cap free
Amount of items: 3
Items: 
Size: 410587 Color: 1
Size: 329140 Color: 1
Size: 260268 Color: 0

Bin 521: 6 of cap free
Amount of items: 3
Items: 
Size: 492446 Color: 0
Size: 257443 Color: 1
Size: 250106 Color: 0

Bin 522: 6 of cap free
Amount of items: 3
Items: 
Size: 414118 Color: 1
Size: 314576 Color: 1
Size: 271301 Color: 0

Bin 523: 6 of cap free
Amount of items: 3
Items: 
Size: 374867 Color: 0
Size: 317392 Color: 1
Size: 307736 Color: 1

Bin 524: 6 of cap free
Amount of items: 3
Items: 
Size: 386071 Color: 1
Size: 354605 Color: 0
Size: 259319 Color: 1

Bin 525: 7 of cap free
Amount of items: 3
Items: 
Size: 434862 Color: 0
Size: 305864 Color: 1
Size: 259268 Color: 0

Bin 526: 7 of cap free
Amount of items: 3
Items: 
Size: 372331 Color: 0
Size: 336928 Color: 1
Size: 290735 Color: 1

Bin 527: 7 of cap free
Amount of items: 3
Items: 
Size: 392190 Color: 1
Size: 344311 Color: 1
Size: 263493 Color: 0

Bin 528: 7 of cap free
Amount of items: 3
Items: 
Size: 347484 Color: 0
Size: 341981 Color: 1
Size: 310529 Color: 1

Bin 529: 7 of cap free
Amount of items: 3
Items: 
Size: 403992 Color: 0
Size: 320198 Color: 1
Size: 275804 Color: 0

Bin 530: 7 of cap free
Amount of items: 3
Items: 
Size: 411248 Color: 0
Size: 316672 Color: 1
Size: 272074 Color: 1

Bin 531: 8 of cap free
Amount of items: 3
Items: 
Size: 477391 Color: 0
Size: 266488 Color: 1
Size: 256114 Color: 0

Bin 532: 8 of cap free
Amount of items: 3
Items: 
Size: 478485 Color: 1
Size: 265578 Color: 0
Size: 255930 Color: 1

Bin 533: 8 of cap free
Amount of items: 3
Items: 
Size: 430982 Color: 1
Size: 301156 Color: 0
Size: 267855 Color: 1

Bin 534: 8 of cap free
Amount of items: 3
Items: 
Size: 378271 Color: 1
Size: 317952 Color: 1
Size: 303770 Color: 0

Bin 535: 8 of cap free
Amount of items: 3
Items: 
Size: 361116 Color: 0
Size: 354413 Color: 1
Size: 284464 Color: 1

Bin 536: 8 of cap free
Amount of items: 3
Items: 
Size: 417834 Color: 0
Size: 327348 Color: 1
Size: 254811 Color: 0

Bin 537: 8 of cap free
Amount of items: 3
Items: 
Size: 371318 Color: 1
Size: 322382 Color: 0
Size: 306293 Color: 1

Bin 538: 8 of cap free
Amount of items: 3
Items: 
Size: 350106 Color: 1
Size: 334398 Color: 1
Size: 315489 Color: 0

Bin 539: 8 of cap free
Amount of items: 3
Items: 
Size: 475549 Color: 1
Size: 269017 Color: 0
Size: 255427 Color: 0

Bin 540: 8 of cap free
Amount of items: 3
Items: 
Size: 377352 Color: 0
Size: 336684 Color: 1
Size: 285957 Color: 1

Bin 541: 9 of cap free
Amount of items: 3
Items: 
Size: 338799 Color: 0
Size: 336043 Color: 1
Size: 325150 Color: 0

Bin 542: 9 of cap free
Amount of items: 3
Items: 
Size: 391917 Color: 1
Size: 343279 Color: 0
Size: 264796 Color: 0

Bin 543: 9 of cap free
Amount of items: 3
Items: 
Size: 355148 Color: 1
Size: 326730 Color: 0
Size: 318114 Color: 1

Bin 544: 9 of cap free
Amount of items: 3
Items: 
Size: 447777 Color: 1
Size: 284646 Color: 0
Size: 267569 Color: 1

Bin 545: 9 of cap free
Amount of items: 3
Items: 
Size: 464334 Color: 0
Size: 273138 Color: 1
Size: 262520 Color: 1

Bin 546: 10 of cap free
Amount of items: 3
Items: 
Size: 384953 Color: 1
Size: 364548 Color: 0
Size: 250490 Color: 1

Bin 547: 10 of cap free
Amount of items: 3
Items: 
Size: 445273 Color: 1
Size: 280572 Color: 0
Size: 274146 Color: 0

Bin 548: 10 of cap free
Amount of items: 3
Items: 
Size: 403573 Color: 1
Size: 325734 Color: 0
Size: 270684 Color: 1

Bin 549: 10 of cap free
Amount of items: 3
Items: 
Size: 399476 Color: 0
Size: 307821 Color: 1
Size: 292694 Color: 1

Bin 550: 11 of cap free
Amount of items: 3
Items: 
Size: 406088 Color: 1
Size: 305931 Color: 0
Size: 287971 Color: 0

Bin 551: 11 of cap free
Amount of items: 3
Items: 
Size: 496253 Color: 1
Size: 252138 Color: 0
Size: 251599 Color: 1

Bin 552: 11 of cap free
Amount of items: 3
Items: 
Size: 360797 Color: 0
Size: 326412 Color: 1
Size: 312781 Color: 0

Bin 553: 12 of cap free
Amount of items: 3
Items: 
Size: 443691 Color: 1
Size: 305701 Color: 0
Size: 250597 Color: 1

Bin 554: 12 of cap free
Amount of items: 3
Items: 
Size: 358193 Color: 1
Size: 335662 Color: 1
Size: 306134 Color: 0

Bin 555: 12 of cap free
Amount of items: 3
Items: 
Size: 360626 Color: 0
Size: 321974 Color: 1
Size: 317389 Color: 0

Bin 556: 12 of cap free
Amount of items: 3
Items: 
Size: 499827 Color: 0
Size: 250141 Color: 1
Size: 250021 Color: 0

Bin 557: 12 of cap free
Amount of items: 3
Items: 
Size: 408909 Color: 0
Size: 323881 Color: 1
Size: 267199 Color: 0

Bin 558: 12 of cap free
Amount of items: 3
Items: 
Size: 456642 Color: 0
Size: 273209 Color: 1
Size: 270138 Color: 0

Bin 559: 12 of cap free
Amount of items: 3
Items: 
Size: 425746 Color: 1
Size: 292574 Color: 0
Size: 281669 Color: 1

Bin 560: 12 of cap free
Amount of items: 3
Items: 
Size: 485463 Color: 0
Size: 263345 Color: 1
Size: 251181 Color: 0

Bin 561: 13 of cap free
Amount of items: 3
Items: 
Size: 454383 Color: 1
Size: 294768 Color: 1
Size: 250837 Color: 0

Bin 562: 13 of cap free
Amount of items: 3
Items: 
Size: 421100 Color: 1
Size: 302475 Color: 0
Size: 276413 Color: 0

Bin 563: 13 of cap free
Amount of items: 3
Items: 
Size: 441373 Color: 0
Size: 306169 Color: 1
Size: 252446 Color: 1

Bin 564: 14 of cap free
Amount of items: 3
Items: 
Size: 465514 Color: 0
Size: 281301 Color: 0
Size: 253172 Color: 1

Bin 565: 14 of cap free
Amount of items: 3
Items: 
Size: 349090 Color: 1
Size: 346548 Color: 1
Size: 304349 Color: 0

Bin 566: 15 of cap free
Amount of items: 3
Items: 
Size: 402101 Color: 1
Size: 346567 Color: 0
Size: 251318 Color: 0

Bin 567: 15 of cap free
Amount of items: 3
Items: 
Size: 406376 Color: 1
Size: 326632 Color: 0
Size: 266978 Color: 1

Bin 568: 16 of cap free
Amount of items: 3
Items: 
Size: 460906 Color: 0
Size: 279479 Color: 1
Size: 259600 Color: 1

Bin 569: 16 of cap free
Amount of items: 3
Items: 
Size: 423751 Color: 0
Size: 312050 Color: 0
Size: 264184 Color: 1

Bin 570: 16 of cap free
Amount of items: 3
Items: 
Size: 408139 Color: 0
Size: 325434 Color: 1
Size: 266412 Color: 0

Bin 571: 16 of cap free
Amount of items: 3
Items: 
Size: 385501 Color: 1
Size: 345224 Color: 1
Size: 269260 Color: 0

Bin 572: 17 of cap free
Amount of items: 3
Items: 
Size: 380117 Color: 1
Size: 327068 Color: 1
Size: 292799 Color: 0

Bin 573: 18 of cap free
Amount of items: 3
Items: 
Size: 419645 Color: 0
Size: 325567 Color: 1
Size: 254771 Color: 1

Bin 574: 18 of cap free
Amount of items: 3
Items: 
Size: 475315 Color: 1
Size: 265554 Color: 0
Size: 259114 Color: 1

Bin 575: 18 of cap free
Amount of items: 3
Items: 
Size: 401163 Color: 1
Size: 307344 Color: 0
Size: 291476 Color: 0

Bin 576: 19 of cap free
Amount of items: 3
Items: 
Size: 435942 Color: 1
Size: 303769 Color: 0
Size: 260271 Color: 1

Bin 577: 20 of cap free
Amount of items: 3
Items: 
Size: 487745 Color: 1
Size: 257963 Color: 0
Size: 254273 Color: 0

Bin 578: 20 of cap free
Amount of items: 3
Items: 
Size: 367670 Color: 1
Size: 324990 Color: 1
Size: 307321 Color: 0

Bin 579: 20 of cap free
Amount of items: 3
Items: 
Size: 455381 Color: 0
Size: 275378 Color: 1
Size: 269222 Color: 1

Bin 580: 21 of cap free
Amount of items: 3
Items: 
Size: 458068 Color: 0
Size: 291428 Color: 1
Size: 250484 Color: 1

Bin 581: 21 of cap free
Amount of items: 3
Items: 
Size: 382509 Color: 1
Size: 362469 Color: 0
Size: 255002 Color: 1

Bin 582: 21 of cap free
Amount of items: 3
Items: 
Size: 465777 Color: 1
Size: 275625 Color: 0
Size: 258578 Color: 0

Bin 583: 23 of cap free
Amount of items: 3
Items: 
Size: 398180 Color: 1
Size: 343743 Color: 1
Size: 258055 Color: 0

Bin 584: 23 of cap free
Amount of items: 3
Items: 
Size: 355076 Color: 1
Size: 343119 Color: 0
Size: 301783 Color: 1

Bin 585: 24 of cap free
Amount of items: 3
Items: 
Size: 467009 Color: 1
Size: 272442 Color: 0
Size: 260526 Color: 1

Bin 586: 24 of cap free
Amount of items: 3
Items: 
Size: 413599 Color: 0
Size: 304516 Color: 1
Size: 281862 Color: 1

Bin 587: 24 of cap free
Amount of items: 3
Items: 
Size: 370910 Color: 1
Size: 331392 Color: 0
Size: 297675 Color: 1

Bin 588: 25 of cap free
Amount of items: 3
Items: 
Size: 440978 Color: 1
Size: 306165 Color: 0
Size: 252833 Color: 1

Bin 589: 26 of cap free
Amount of items: 3
Items: 
Size: 410901 Color: 0
Size: 322803 Color: 1
Size: 266271 Color: 1

Bin 590: 26 of cap free
Amount of items: 3
Items: 
Size: 428745 Color: 1
Size: 291581 Color: 1
Size: 279649 Color: 0

Bin 591: 26 of cap free
Amount of items: 3
Items: 
Size: 437493 Color: 1
Size: 288882 Color: 0
Size: 273600 Color: 1

Bin 592: 28 of cap free
Amount of items: 3
Items: 
Size: 382062 Color: 1
Size: 366063 Color: 0
Size: 251848 Color: 0

Bin 593: 30 of cap free
Amount of items: 3
Items: 
Size: 450095 Color: 0
Size: 286837 Color: 1
Size: 263039 Color: 0

Bin 594: 32 of cap free
Amount of items: 3
Items: 
Size: 381940 Color: 1
Size: 326820 Color: 0
Size: 291209 Color: 0

Bin 595: 32 of cap free
Amount of items: 3
Items: 
Size: 433109 Color: 1
Size: 302211 Color: 0
Size: 264649 Color: 1

Bin 596: 34 of cap free
Amount of items: 3
Items: 
Size: 369717 Color: 1
Size: 354529 Color: 0
Size: 275721 Color: 1

Bin 597: 35 of cap free
Amount of items: 3
Items: 
Size: 455623 Color: 1
Size: 280257 Color: 0
Size: 264086 Color: 0

Bin 598: 36 of cap free
Amount of items: 3
Items: 
Size: 377976 Color: 1
Size: 321075 Color: 1
Size: 300914 Color: 0

Bin 599: 37 of cap free
Amount of items: 3
Items: 
Size: 459091 Color: 1
Size: 278198 Color: 1
Size: 262675 Color: 0

Bin 600: 37 of cap free
Amount of items: 3
Items: 
Size: 386702 Color: 0
Size: 311137 Color: 0
Size: 302125 Color: 1

Bin 601: 37 of cap free
Amount of items: 3
Items: 
Size: 409921 Color: 1
Size: 308193 Color: 0
Size: 281850 Color: 0

Bin 602: 38 of cap free
Amount of items: 3
Items: 
Size: 399843 Color: 0
Size: 301615 Color: 1
Size: 298505 Color: 1

Bin 603: 38 of cap free
Amount of items: 3
Items: 
Size: 414403 Color: 0
Size: 294187 Color: 1
Size: 291373 Color: 0

Bin 604: 38 of cap free
Amount of items: 3
Items: 
Size: 464040 Color: 0
Size: 274677 Color: 1
Size: 261246 Color: 0

Bin 605: 39 of cap free
Amount of items: 3
Items: 
Size: 396816 Color: 1
Size: 339181 Color: 1
Size: 263965 Color: 0

Bin 606: 41 of cap free
Amount of items: 3
Items: 
Size: 412630 Color: 0
Size: 314689 Color: 1
Size: 272641 Color: 1

Bin 607: 43 of cap free
Amount of items: 3
Items: 
Size: 431993 Color: 1
Size: 290443 Color: 0
Size: 277522 Color: 1

Bin 608: 44 of cap free
Amount of items: 3
Items: 
Size: 343990 Color: 0
Size: 339796 Color: 1
Size: 316171 Color: 0

Bin 609: 47 of cap free
Amount of items: 3
Items: 
Size: 402604 Color: 1
Size: 298852 Color: 0
Size: 298498 Color: 0

Bin 610: 47 of cap free
Amount of items: 3
Items: 
Size: 472895 Color: 1
Size: 276313 Color: 0
Size: 250746 Color: 1

Bin 611: 48 of cap free
Amount of items: 3
Items: 
Size: 355564 Color: 0
Size: 326612 Color: 0
Size: 317777 Color: 1

Bin 612: 48 of cap free
Amount of items: 3
Items: 
Size: 492576 Color: 0
Size: 254037 Color: 1
Size: 253340 Color: 0

Bin 613: 51 of cap free
Amount of items: 3
Items: 
Size: 465285 Color: 1
Size: 278193 Color: 1
Size: 256472 Color: 0

Bin 614: 51 of cap free
Amount of items: 3
Items: 
Size: 394181 Color: 1
Size: 341489 Color: 0
Size: 264280 Color: 0

Bin 615: 51 of cap free
Amount of items: 3
Items: 
Size: 481982 Color: 0
Size: 261312 Color: 1
Size: 256656 Color: 1

Bin 616: 53 of cap free
Amount of items: 3
Items: 
Size: 408548 Color: 0
Size: 317440 Color: 0
Size: 273960 Color: 1

Bin 617: 54 of cap free
Amount of items: 3
Items: 
Size: 398217 Color: 0
Size: 334428 Color: 0
Size: 267302 Color: 1

Bin 618: 61 of cap free
Amount of items: 3
Items: 
Size: 424140 Color: 1
Size: 316544 Color: 0
Size: 259256 Color: 0

Bin 619: 62 of cap free
Amount of items: 3
Items: 
Size: 404057 Color: 0
Size: 334011 Color: 1
Size: 261871 Color: 0

Bin 620: 64 of cap free
Amount of items: 3
Items: 
Size: 417136 Color: 1
Size: 325126 Color: 1
Size: 257675 Color: 0

Bin 621: 66 of cap free
Amount of items: 3
Items: 
Size: 462908 Color: 0
Size: 274132 Color: 1
Size: 262895 Color: 1

Bin 622: 66 of cap free
Amount of items: 3
Items: 
Size: 453702 Color: 0
Size: 286047 Color: 1
Size: 260186 Color: 0

Bin 623: 66 of cap free
Amount of items: 3
Items: 
Size: 396230 Color: 0
Size: 324780 Color: 1
Size: 278925 Color: 1

Bin 624: 71 of cap free
Amount of items: 3
Items: 
Size: 371126 Color: 1
Size: 317789 Color: 0
Size: 311015 Color: 0

Bin 625: 75 of cap free
Amount of items: 3
Items: 
Size: 422276 Color: 0
Size: 290832 Color: 0
Size: 286818 Color: 1

Bin 626: 90 of cap free
Amount of items: 3
Items: 
Size: 403710 Color: 0
Size: 317581 Color: 1
Size: 278620 Color: 0

Bin 627: 98 of cap free
Amount of items: 3
Items: 
Size: 386437 Color: 1
Size: 358140 Color: 1
Size: 255326 Color: 0

Bin 628: 101 of cap free
Amount of items: 3
Items: 
Size: 476672 Color: 1
Size: 267770 Color: 0
Size: 255458 Color: 0

Bin 629: 112 of cap free
Amount of items: 3
Items: 
Size: 366635 Color: 1
Size: 320923 Color: 0
Size: 312331 Color: 1

Bin 630: 113 of cap free
Amount of items: 3
Items: 
Size: 379643 Color: 0
Size: 310598 Color: 0
Size: 309647 Color: 1

Bin 631: 114 of cap free
Amount of items: 3
Items: 
Size: 387900 Color: 0
Size: 338966 Color: 1
Size: 273021 Color: 0

Bin 632: 117 of cap free
Amount of items: 3
Items: 
Size: 424719 Color: 1
Size: 306412 Color: 0
Size: 268753 Color: 0

Bin 633: 134 of cap free
Amount of items: 3
Items: 
Size: 368404 Color: 0
Size: 316797 Color: 0
Size: 314666 Color: 1

Bin 634: 135 of cap free
Amount of items: 3
Items: 
Size: 427940 Color: 1
Size: 300198 Color: 0
Size: 271728 Color: 0

Bin 635: 138 of cap free
Amount of items: 3
Items: 
Size: 451206 Color: 1
Size: 276288 Color: 0
Size: 272369 Color: 1

Bin 636: 148 of cap free
Amount of items: 3
Items: 
Size: 465133 Color: 0
Size: 275663 Color: 1
Size: 259057 Color: 1

Bin 637: 163 of cap free
Amount of items: 3
Items: 
Size: 478658 Color: 1
Size: 270723 Color: 0
Size: 250457 Color: 1

Bin 638: 189 of cap free
Amount of items: 3
Items: 
Size: 388744 Color: 0
Size: 359502 Color: 0
Size: 251566 Color: 1

Bin 639: 202 of cap free
Amount of items: 3
Items: 
Size: 454109 Color: 0
Size: 283987 Color: 0
Size: 261703 Color: 1

Bin 640: 214 of cap free
Amount of items: 3
Items: 
Size: 376276 Color: 0
Size: 348138 Color: 1
Size: 275373 Color: 1

Bin 641: 255 of cap free
Amount of items: 3
Items: 
Size: 458354 Color: 1
Size: 285328 Color: 0
Size: 256064 Color: 0

Bin 642: 281 of cap free
Amount of items: 3
Items: 
Size: 386295 Color: 0
Size: 314636 Color: 1
Size: 298789 Color: 0

Bin 643: 281 of cap free
Amount of items: 3
Items: 
Size: 356779 Color: 0
Size: 322574 Color: 1
Size: 320367 Color: 0

Bin 644: 287 of cap free
Amount of items: 3
Items: 
Size: 338545 Color: 0
Size: 331820 Color: 1
Size: 329349 Color: 1

Bin 645: 303 of cap free
Amount of items: 3
Items: 
Size: 499466 Color: 0
Size: 250141 Color: 1
Size: 250091 Color: 1

Bin 646: 309 of cap free
Amount of items: 3
Items: 
Size: 421195 Color: 0
Size: 304545 Color: 1
Size: 273952 Color: 0

Bin 647: 350 of cap free
Amount of items: 3
Items: 
Size: 395117 Color: 1
Size: 345291 Color: 0
Size: 259243 Color: 0

Bin 648: 366 of cap free
Amount of items: 3
Items: 
Size: 355426 Color: 0
Size: 339111 Color: 1
Size: 305098 Color: 1

Bin 649: 369 of cap free
Amount of items: 3
Items: 
Size: 370466 Color: 0
Size: 330497 Color: 0
Size: 298669 Color: 1

Bin 650: 382 of cap free
Amount of items: 3
Items: 
Size: 382593 Color: 0
Size: 309439 Color: 1
Size: 307587 Color: 0

Bin 651: 430 of cap free
Amount of items: 3
Items: 
Size: 464647 Color: 1
Size: 278337 Color: 0
Size: 256587 Color: 0

Bin 652: 441 of cap free
Amount of items: 3
Items: 
Size: 404709 Color: 1
Size: 341651 Color: 0
Size: 253200 Color: 0

Bin 653: 451 of cap free
Amount of items: 3
Items: 
Size: 440394 Color: 0
Size: 280509 Color: 0
Size: 278647 Color: 1

Bin 654: 453 of cap free
Amount of items: 3
Items: 
Size: 381710 Color: 0
Size: 340332 Color: 0
Size: 277506 Color: 1

Bin 655: 462 of cap free
Amount of items: 3
Items: 
Size: 373658 Color: 0
Size: 329308 Color: 0
Size: 296573 Color: 1

Bin 656: 471 of cap free
Amount of items: 3
Items: 
Size: 358779 Color: 1
Size: 344514 Color: 1
Size: 296237 Color: 0

Bin 657: 478 of cap free
Amount of items: 3
Items: 
Size: 394362 Color: 1
Size: 323210 Color: 0
Size: 281951 Color: 1

Bin 658: 693 of cap free
Amount of items: 3
Items: 
Size: 419774 Color: 0
Size: 306366 Color: 0
Size: 273168 Color: 1

Bin 659: 848 of cap free
Amount of items: 3
Items: 
Size: 382257 Color: 1
Size: 357987 Color: 0
Size: 258909 Color: 1

Bin 660: 858 of cap free
Amount of items: 3
Items: 
Size: 389347 Color: 0
Size: 334565 Color: 1
Size: 275231 Color: 1

Bin 661: 879 of cap free
Amount of items: 3
Items: 
Size: 433508 Color: 0
Size: 310564 Color: 0
Size: 255050 Color: 1

Bin 662: 930 of cap free
Amount of items: 3
Items: 
Size: 402876 Color: 0
Size: 335543 Color: 1
Size: 260652 Color: 0

Bin 663: 1225 of cap free
Amount of items: 3
Items: 
Size: 492986 Color: 0
Size: 253110 Color: 1
Size: 252680 Color: 0

Bin 664: 9734 of cap free
Amount of items: 3
Items: 
Size: 389355 Color: 1
Size: 315088 Color: 0
Size: 285824 Color: 1

Bin 665: 21956 of cap free
Amount of items: 3
Items: 
Size: 356754 Color: 1
Size: 311947 Color: 0
Size: 309344 Color: 1

Bin 666: 206002 of cap free
Amount of items: 3
Items: 
Size: 267995 Color: 1
Size: 263818 Color: 0
Size: 262186 Color: 0

Bin 667: 245298 of cap free
Amount of items: 2
Items: 
Size: 499774 Color: 1
Size: 254929 Color: 0

Bin 668: 500286 of cap free
Amount of items: 1
Items: 
Size: 499715 Color: 1

Total size: 667000667
Total free space: 1000001

