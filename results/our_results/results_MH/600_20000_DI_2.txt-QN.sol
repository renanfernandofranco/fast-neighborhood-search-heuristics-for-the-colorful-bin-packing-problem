Capicity Bin: 15744
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8990 Color: 429
Size: 5618 Color: 374
Size: 400 Color: 64
Size: 368 Color: 52
Size: 368 Color: 51

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 9006 Color: 432
Size: 5630 Color: 377
Size: 416 Color: 69
Size: 348 Color: 43
Size: 344 Color: 42

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 9856 Color: 441
Size: 5888 Color: 385

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11220 Color: 465
Size: 3542 Color: 332
Size: 982 Color: 169

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 472
Size: 3616 Color: 335
Size: 656 Color: 133

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 474
Size: 3378 Color: 329
Size: 704 Color: 138

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 476
Size: 2652 Color: 295
Size: 1360 Color: 205

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 478
Size: 3632 Color: 336
Size: 332 Color: 38

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11805 Color: 479
Size: 3283 Color: 325
Size: 656 Color: 134

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11815 Color: 480
Size: 2351 Color: 273
Size: 1578 Color: 222

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11819 Color: 481
Size: 3271 Color: 322
Size: 654 Color: 132

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11832 Color: 483
Size: 2354 Color: 274
Size: 1558 Color: 220

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11888 Color: 484
Size: 3096 Color: 311
Size: 760 Color: 147

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 487
Size: 3272 Color: 323
Size: 528 Color: 100

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12224 Color: 497
Size: 3208 Color: 319
Size: 312 Color: 34

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 501
Size: 3144 Color: 315
Size: 276 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 503
Size: 2944 Color: 306
Size: 424 Color: 71

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12400 Color: 505
Size: 2768 Color: 297
Size: 576 Color: 113

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12682 Color: 515
Size: 2554 Color: 288
Size: 508 Color: 95

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12690 Color: 516
Size: 1922 Color: 247
Size: 1132 Color: 183

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 517
Size: 1832 Color: 241
Size: 1200 Color: 186

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 518
Size: 2536 Color: 287
Size: 464 Color: 83

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 521
Size: 2188 Color: 268
Size: 704 Color: 137

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12893 Color: 524
Size: 2377 Color: 277
Size: 474 Color: 87

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12906 Color: 525
Size: 2482 Color: 284
Size: 356 Color: 48

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13008 Color: 531
Size: 2432 Color: 282
Size: 304 Color: 32

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 533
Size: 1388 Color: 209
Size: 1308 Color: 198

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13108 Color: 534
Size: 1512 Color: 216
Size: 1124 Color: 180

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13123 Color: 535
Size: 2185 Color: 267
Size: 436 Color: 76

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 541
Size: 2488 Color: 285
Size: 24 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 543
Size: 2004 Color: 253
Size: 468 Color: 84

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 548
Size: 1548 Color: 219
Size: 860 Color: 158

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 551
Size: 1760 Color: 235
Size: 584 Color: 117

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13401 Color: 552
Size: 1951 Color: 250
Size: 392 Color: 59

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 554
Size: 1456 Color: 213
Size: 864 Color: 159

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 556
Size: 1304 Color: 193
Size: 976 Color: 166

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 557
Size: 1310 Color: 200
Size: 960 Color: 163

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 559
Size: 1308 Color: 195
Size: 928 Color: 160

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13582 Color: 562
Size: 1304 Color: 192
Size: 858 Color: 157

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 563
Size: 1786 Color: 239
Size: 356 Color: 49

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13616 Color: 564
Size: 1296 Color: 190
Size: 832 Color: 151

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 565
Size: 1474 Color: 215
Size: 652 Color: 130

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 573
Size: 1368 Color: 206
Size: 604 Color: 119

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 574
Size: 1584 Color: 223
Size: 384 Color: 56

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 578
Size: 1730 Color: 232
Size: 160 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 579
Size: 1304 Color: 194
Size: 580 Color: 114

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13886 Color: 581
Size: 1382 Color: 207
Size: 476 Color: 88

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 582
Size: 1308 Color: 197
Size: 544 Color: 104

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13932 Color: 583
Size: 1468 Color: 214
Size: 344 Color: 41

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 585
Size: 1280 Color: 188
Size: 520 Color: 98

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 587
Size: 1324 Color: 202
Size: 432 Color: 73

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14000 Color: 589
Size: 1520 Color: 218
Size: 224 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 591
Size: 1438 Color: 211
Size: 284 Color: 20

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 592
Size: 1056 Color: 172
Size: 624 Color: 124

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14084 Color: 593
Size: 1308 Color: 196
Size: 352 Color: 47

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14086 Color: 594
Size: 858 Color: 156
Size: 800 Color: 149

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 595
Size: 1336 Color: 203
Size: 320 Color: 36

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 596
Size: 1358 Color: 204
Size: 268 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14128 Color: 598
Size: 1136 Color: 184
Size: 480 Color: 89

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 599
Size: 1128 Color: 181
Size: 464 Color: 82

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14156 Color: 600
Size: 1296 Color: 189
Size: 292 Color: 28

Bin 62: 1 of cap free
Amount of items: 5
Items: 
Size: 7932 Color: 419
Size: 6553 Color: 394
Size: 480 Color: 90
Size: 390 Color: 58
Size: 388 Color: 57

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11211 Color: 464
Size: 4276 Color: 351
Size: 256 Color: 10

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11831 Color: 482
Size: 3608 Color: 334
Size: 304 Color: 31

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 491
Size: 3261 Color: 321
Size: 480 Color: 91

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12212 Color: 496
Size: 3275 Color: 324
Size: 256 Color: 11

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12285 Color: 500
Size: 1776 Color: 238
Size: 1682 Color: 228

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 502
Size: 2925 Color: 305
Size: 448 Color: 80

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 11432 Color: 471
Size: 4310 Color: 357

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 11694 Color: 475
Size: 4048 Color: 347

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 11970 Color: 489
Size: 3772 Color: 342

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 12500 Color: 508
Size: 3242 Color: 320

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 542
Size: 2504 Color: 286

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 560
Size: 2182 Color: 266

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 571
Size: 2022 Color: 255

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13978 Color: 586
Size: 1764 Color: 236

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 588
Size: 1752 Color: 234

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 11182 Color: 460
Size: 4295 Color: 355
Size: 264 Color: 15

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 11201 Color: 463
Size: 4284 Color: 353
Size: 256 Color: 12

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 11954 Color: 488
Size: 3787 Color: 343

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 11327 Color: 470
Size: 4285 Color: 354
Size: 128 Color: 5

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 490
Size: 3764 Color: 341

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 12432 Color: 507
Size: 3308 Color: 327

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 509
Size: 3176 Color: 317

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 13032 Color: 532
Size: 2708 Color: 296

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 13150 Color: 540
Size: 2590 Color: 291

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 13476 Color: 558
Size: 2264 Color: 270

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13668 Color: 567
Size: 2072 Color: 261

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 568
Size: 2040 Color: 259
Size: 12 Color: 0

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13700 Color: 569
Size: 2040 Color: 260

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 575
Size: 1936 Color: 249

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13872 Color: 580
Size: 1868 Color: 242

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 590
Size: 1732 Color: 233

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 597
Size: 1620 Color: 224

Bin 95: 5 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 461
Size: 4299 Color: 356
Size: 256 Color: 14

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 12235 Color: 498
Size: 3504 Color: 331

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 12617 Color: 512
Size: 3122 Color: 314

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 513
Size: 3107 Color: 312

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 12925 Color: 529
Size: 2814 Color: 302

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 13405 Color: 553
Size: 2334 Color: 272

Bin 101: 6 of cap free
Amount of items: 5
Items: 
Size: 7900 Color: 416
Size: 6542 Color: 391
Size: 464 Color: 81
Size: 416 Color: 68
Size: 416 Color: 67

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 12662 Color: 514
Size: 3076 Color: 309

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 12855 Color: 522
Size: 2883 Color: 304

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 544
Size: 2434 Color: 283

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 570
Size: 2028 Color: 258

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13730 Color: 572
Size: 2008 Color: 254

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 576
Size: 1922 Color: 248

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 584
Size: 1802 Color: 240

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13130 Color: 537
Size: 2607 Color: 293

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 11912 Color: 485
Size: 3824 Color: 345

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 492
Size: 3688 Color: 338
Size: 32 Color: 2

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12244 Color: 499
Size: 3492 Color: 330

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 550
Size: 2362 Color: 276

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13824 Color: 577
Size: 1912 Color: 246

Bin 115: 9 of cap free
Amount of items: 5
Items: 
Size: 7916 Color: 418
Size: 6551 Color: 393
Size: 468 Color: 86
Size: 400 Color: 61
Size: 400 Color: 60

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 12040 Color: 494
Size: 3695 Color: 340

Bin 117: 10 of cap free
Amount of items: 5
Items: 
Size: 7898 Color: 415
Size: 6540 Color: 390
Size: 448 Color: 79
Size: 432 Color: 72
Size: 416 Color: 70

Bin 118: 10 of cap free
Amount of items: 5
Items: 
Size: 8998 Color: 431
Size: 5628 Color: 376
Size: 404 Color: 66
Size: 352 Color: 45
Size: 352 Color: 44

Bin 119: 10 of cap free
Amount of items: 3
Items: 
Size: 11315 Color: 468
Size: 3681 Color: 337
Size: 738 Color: 143

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13322 Color: 547
Size: 2412 Color: 280

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 549
Size: 2394 Color: 278

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13570 Color: 561
Size: 2164 Color: 265

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 12613 Color: 511
Size: 3120 Color: 313

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 11194 Color: 462
Size: 4282 Color: 352
Size: 256 Color: 13

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 12752 Color: 519
Size: 2980 Color: 308

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 12924 Color: 528
Size: 2808 Color: 301

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 566
Size: 2096 Color: 263

Bin 128: 14 of cap free
Amount of items: 5
Items: 
Size: 8996 Color: 430
Size: 5622 Color: 375
Size: 400 Color: 65
Size: 360 Color: 50
Size: 352 Color: 46

Bin 129: 14 of cap free
Amount of items: 3
Items: 
Size: 10856 Color: 457
Size: 3186 Color: 318
Size: 1688 Color: 229

Bin 130: 14 of cap free
Amount of items: 3
Items: 
Size: 11311 Color: 467
Size: 2645 Color: 294
Size: 1774 Color: 237

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 504
Size: 3348 Color: 328

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 555
Size: 2288 Color: 271

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 546
Size: 2412 Color: 281

Bin 134: 18 of cap free
Amount of items: 5
Items: 
Size: 7890 Color: 414
Size: 6524 Color: 389
Size: 448 Color: 78
Size: 432 Color: 75
Size: 432 Color: 74

Bin 135: 18 of cap free
Amount of items: 5
Items: 
Size: 7912 Color: 417
Size: 6546 Color: 392
Size: 468 Color: 85
Size: 400 Color: 63
Size: 400 Color: 62

Bin 136: 18 of cap free
Amount of items: 3
Items: 
Size: 10546 Color: 447
Size: 4892 Color: 363
Size: 288 Color: 23

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 12874 Color: 523
Size: 2852 Color: 303

Bin 138: 19 of cap free
Amount of items: 3
Items: 
Size: 10606 Color: 454
Size: 3094 Color: 310
Size: 2025 Color: 257

Bin 139: 19 of cap free
Amount of items: 2
Items: 
Size: 12034 Color: 493
Size: 3691 Color: 339

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 13149 Color: 539
Size: 2576 Color: 290

Bin 141: 20 of cap free
Amount of items: 2
Items: 
Size: 11922 Color: 486
Size: 3802 Color: 344

Bin 142: 20 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 520
Size: 2948 Color: 307

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 12922 Color: 527
Size: 2802 Color: 300

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 536
Size: 2600 Color: 292

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 13315 Color: 545
Size: 2409 Color: 279

Bin 146: 26 of cap free
Amount of items: 2
Items: 
Size: 12946 Color: 530
Size: 2772 Color: 298

Bin 147: 26 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 538
Size: 2570 Color: 289

Bin 148: 27 of cap free
Amount of items: 2
Items: 
Size: 12571 Color: 510
Size: 3146 Color: 316

Bin 149: 28 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 455
Size: 4808 Color: 361
Size: 288 Color: 22

Bin 150: 28 of cap free
Amount of items: 3
Items: 
Size: 11320 Color: 469
Size: 4200 Color: 350
Size: 196 Color: 7

Bin 151: 28 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 506
Size: 3296 Color: 326

Bin 152: 30 of cap free
Amount of items: 4
Items: 
Size: 11498 Color: 473
Size: 4088 Color: 348
Size: 64 Color: 4
Size: 64 Color: 3

Bin 153: 30 of cap free
Amount of items: 2
Items: 
Size: 11748 Color: 477
Size: 3966 Color: 346

Bin 154: 30 of cap free
Amount of items: 2
Items: 
Size: 12172 Color: 495
Size: 3542 Color: 333

Bin 155: 30 of cap free
Amount of items: 2
Items: 
Size: 12914 Color: 526
Size: 2800 Color: 299

Bin 156: 34 of cap free
Amount of items: 3
Items: 
Size: 9878 Color: 444
Size: 5544 Color: 373
Size: 288 Color: 27

Bin 157: 36 of cap free
Amount of items: 7
Items: 
Size: 7882 Color: 410
Size: 2090 Color: 262
Size: 2022 Color: 256
Size: 1978 Color: 252
Size: 672 Color: 136
Size: 536 Color: 101
Size: 528 Color: 99

Bin 158: 52 of cap free
Amount of items: 7
Items: 
Size: 7881 Color: 409
Size: 1953 Color: 251
Size: 1896 Color: 245
Size: 1894 Color: 244
Size: 984 Color: 170
Size: 544 Color: 103
Size: 540 Color: 102

Bin 159: 69 of cap free
Amount of items: 6
Items: 
Size: 7884 Color: 411
Size: 2356 Color: 275
Size: 2248 Color: 269
Size: 2163 Color: 264
Size: 512 Color: 97
Size: 512 Color: 96

Bin 160: 79 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 466
Size: 4185 Color: 349
Size: 252 Color: 9

Bin 161: 80 of cap free
Amount of items: 3
Items: 
Size: 10712 Color: 456
Size: 4664 Color: 360
Size: 288 Color: 21

Bin 162: 87 of cap free
Amount of items: 7
Items: 
Size: 7877 Color: 407
Size: 1644 Color: 226
Size: 1632 Color: 225
Size: 1572 Color: 221
Size: 1516 Color: 217
Size: 856 Color: 155
Size: 560 Color: 106

Bin 163: 88 of cap free
Amount of items: 2
Items: 
Size: 8988 Color: 428
Size: 6668 Color: 402

Bin 164: 88 of cap free
Amount of items: 3
Items: 
Size: 9976 Color: 445
Size: 5392 Color: 372
Size: 288 Color: 26

Bin 165: 92 of cap free
Amount of items: 4
Items: 
Size: 8008 Color: 420
Size: 6888 Color: 403
Size: 380 Color: 55
Size: 376 Color: 54

Bin 166: 126 of cap free
Amount of items: 4
Items: 
Size: 10152 Color: 446
Size: 4890 Color: 362
Size: 288 Color: 25
Size: 288 Color: 24

Bin 167: 158 of cap free
Amount of items: 3
Items: 
Size: 8656 Color: 421
Size: 6554 Color: 395
Size: 376 Color: 53

Bin 168: 162 of cap free
Amount of items: 4
Items: 
Size: 9296 Color: 434
Size: 5638 Color: 379
Size: 328 Color: 37
Size: 320 Color: 35

Bin 169: 174 of cap free
Amount of items: 9
Items: 
Size: 7874 Color: 405
Size: 1304 Color: 191
Size: 1280 Color: 187
Size: 1152 Color: 185
Size: 1132 Color: 182
Size: 1124 Color: 179
Size: 576 Color: 110
Size: 568 Color: 109
Size: 560 Color: 108

Bin 170: 176 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 435
Size: 5936 Color: 386
Size: 312 Color: 33

Bin 171: 186 of cap free
Amount of items: 2
Items: 
Size: 8982 Color: 427
Size: 6576 Color: 401

Bin 172: 196 of cap free
Amount of items: 2
Items: 
Size: 10604 Color: 453
Size: 4944 Color: 369

Bin 173: 203 of cap free
Amount of items: 2
Items: 
Size: 9855 Color: 440
Size: 5686 Color: 384

Bin 174: 204 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 443
Size: 5368 Color: 371
Size: 296 Color: 29

Bin 175: 205 of cap free
Amount of items: 2
Items: 
Size: 8971 Color: 426
Size: 6568 Color: 400

Bin 176: 210 of cap free
Amount of items: 2
Items: 
Size: 10603 Color: 452
Size: 4931 Color: 368

Bin 177: 221 of cap free
Amount of items: 2
Items: 
Size: 9851 Color: 439
Size: 5672 Color: 383

Bin 178: 224 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 459
Size: 4336 Color: 359
Size: 272 Color: 17

Bin 179: 228 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 442
Size: 5344 Color: 370
Size: 304 Color: 30

Bin 180: 230 of cap free
Amount of items: 2
Items: 
Size: 8952 Color: 425
Size: 6562 Color: 399

Bin 181: 231 of cap free
Amount of items: 2
Items: 
Size: 9846 Color: 438
Size: 5667 Color: 382

Bin 182: 233 of cap free
Amount of items: 2
Items: 
Size: 8950 Color: 424
Size: 6561 Color: 398

Bin 183: 235 of cap free
Amount of items: 2
Items: 
Size: 10591 Color: 451
Size: 4918 Color: 367

Bin 184: 242 of cap free
Amount of items: 2
Items: 
Size: 8945 Color: 423
Size: 6557 Color: 397

Bin 185: 242 of cap free
Amount of items: 2
Items: 
Size: 9840 Color: 437
Size: 5662 Color: 381

Bin 186: 242 of cap free
Amount of items: 3
Items: 
Size: 10896 Color: 458
Size: 4334 Color: 358
Size: 272 Color: 18

Bin 187: 246 of cap free
Amount of items: 2
Items: 
Size: 10587 Color: 450
Size: 4911 Color: 366

Bin 188: 259 of cap free
Amount of items: 2
Items: 
Size: 10576 Color: 449
Size: 4909 Color: 365

Bin 189: 266 of cap free
Amount of items: 2
Items: 
Size: 8922 Color: 422
Size: 6556 Color: 396

Bin 190: 270 of cap free
Amount of items: 2
Items: 
Size: 9829 Color: 436
Size: 5645 Color: 380

Bin 191: 270 of cap free
Amount of items: 2
Items: 
Size: 10574 Color: 448
Size: 4900 Color: 364

Bin 192: 332 of cap free
Amount of items: 19
Items: 
Size: 1104 Color: 173
Size: 1056 Color: 171
Size: 980 Color: 168
Size: 980 Color: 167
Size: 976 Color: 165
Size: 976 Color: 164
Size: 960 Color: 162
Size: 944 Color: 161
Size: 856 Color: 154
Size: 856 Color: 153
Size: 848 Color: 152
Size: 624 Color: 126
Size: 624 Color: 125
Size: 620 Color: 123
Size: 616 Color: 122
Size: 608 Color: 121
Size: 608 Color: 120
Size: 592 Color: 118
Size: 584 Color: 116

Bin 193: 336 of cap free
Amount of items: 6
Items: 
Size: 7880 Color: 408
Size: 1892 Color: 243
Size: 1728 Color: 231
Size: 1708 Color: 230
Size: 1648 Color: 227
Size: 552 Color: 105

Bin 194: 340 of cap free
Amount of items: 4
Items: 
Size: 9096 Color: 433
Size: 5636 Color: 378
Size: 336 Color: 40
Size: 336 Color: 39

Bin 195: 396 of cap free
Amount of items: 4
Items: 
Size: 7888 Color: 413
Size: 6516 Color: 388
Size: 496 Color: 92
Size: 448 Color: 77

Bin 196: 403 of cap free
Amount of items: 4
Items: 
Size: 7885 Color: 412
Size: 6456 Color: 387
Size: 504 Color: 94
Size: 496 Color: 93

Bin 197: 432 of cap free
Amount of items: 7
Items: 
Size: 7876 Color: 406
Size: 1448 Color: 212
Size: 1424 Color: 210
Size: 1384 Color: 208
Size: 1310 Color: 201
Size: 1310 Color: 199
Size: 560 Color: 107

Bin 198: 531 of cap free
Amount of items: 9
Items: 
Size: 7873 Color: 404
Size: 1124 Color: 178
Size: 1120 Color: 177
Size: 1120 Color: 176
Size: 1120 Color: 175
Size: 1120 Color: 174
Size: 584 Color: 115
Size: 576 Color: 112
Size: 576 Color: 111

Bin 199: 5680 of cap free
Amount of items: 14
Items: 
Size: 832 Color: 150
Size: 800 Color: 148
Size: 756 Color: 146
Size: 752 Color: 145
Size: 752 Color: 144
Size: 738 Color: 142
Size: 736 Color: 141
Size: 736 Color: 140
Size: 736 Color: 139
Size: 664 Color: 135
Size: 654 Color: 131
Size: 640 Color: 129
Size: 640 Color: 128
Size: 628 Color: 127

Total size: 3117312
Total free space: 15744

