Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 386783 Color: 0
Size: 341319 Color: 0
Size: 271899 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 423752 Color: 0
Size: 316373 Color: 1
Size: 259876 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 366875 Color: 0
Size: 332300 Color: 1
Size: 300826 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 455286 Color: 1
Size: 272650 Color: 0
Size: 272065 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 439187 Color: 0
Size: 298203 Color: 0
Size: 262611 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 420727 Color: 1
Size: 315921 Color: 1
Size: 263353 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 378002 Color: 1
Size: 371961 Color: 0
Size: 250038 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 386271 Color: 1
Size: 361047 Color: 0
Size: 252683 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 443505 Color: 0
Size: 288737 Color: 1
Size: 267759 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 494411 Color: 1
Size: 254398 Color: 0
Size: 251192 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 440026 Color: 1
Size: 295264 Color: 1
Size: 264711 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 429814 Color: 1
Size: 311433 Color: 0
Size: 258754 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 379423 Color: 0
Size: 316448 Color: 1
Size: 304130 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 474432 Color: 0
Size: 268511 Color: 1
Size: 257058 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 474778 Color: 1
Size: 275114 Color: 0
Size: 250109 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 392381 Color: 0
Size: 346989 Color: 1
Size: 260631 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 460740 Color: 0
Size: 282238 Color: 0
Size: 257023 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 478991 Color: 1
Size: 265868 Color: 1
Size: 255142 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 424294 Color: 1
Size: 288655 Color: 1
Size: 287052 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 383759 Color: 0
Size: 338162 Color: 1
Size: 278080 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 440846 Color: 1
Size: 288266 Color: 0
Size: 270889 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 429952 Color: 1
Size: 307606 Color: 0
Size: 262443 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 466768 Color: 1
Size: 280985 Color: 1
Size: 252248 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 372401 Color: 1
Size: 360409 Color: 0
Size: 267191 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 453742 Color: 1
Size: 280388 Color: 1
Size: 265871 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 416300 Color: 0
Size: 316229 Color: 1
Size: 267472 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 397957 Color: 1
Size: 302957 Color: 1
Size: 299087 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 405771 Color: 0
Size: 326796 Color: 1
Size: 267434 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 474200 Color: 1
Size: 275718 Color: 0
Size: 250083 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 461237 Color: 0
Size: 269651 Color: 1
Size: 269113 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 479775 Color: 1
Size: 268238 Color: 0
Size: 251988 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 453600 Color: 0
Size: 276424 Color: 0
Size: 269977 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 413750 Color: 1
Size: 299145 Color: 0
Size: 287106 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 407863 Color: 1
Size: 310244 Color: 0
Size: 281894 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 425708 Color: 1
Size: 290564 Color: 0
Size: 283729 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 447302 Color: 0
Size: 281736 Color: 0
Size: 270963 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 355696 Color: 0
Size: 352891 Color: 1
Size: 291414 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 461852 Color: 0
Size: 281101 Color: 1
Size: 257048 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 383773 Color: 0
Size: 340586 Color: 1
Size: 275642 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 413476 Color: 0
Size: 318607 Color: 1
Size: 267918 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 402589 Color: 1
Size: 318966 Color: 1
Size: 278446 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 395772 Color: 1
Size: 322505 Color: 0
Size: 281724 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 357260 Color: 1
Size: 338673 Color: 0
Size: 304068 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 345688 Color: 0
Size: 342205 Color: 1
Size: 312108 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 465326 Color: 1
Size: 271580 Color: 0
Size: 263095 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 439890 Color: 0
Size: 309781 Color: 1
Size: 250330 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 421457 Color: 0
Size: 290483 Color: 0
Size: 288061 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 435795 Color: 1
Size: 298889 Color: 0
Size: 265317 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 381224 Color: 0
Size: 342315 Color: 1
Size: 276462 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 442512 Color: 1
Size: 305748 Color: 1
Size: 251741 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 388501 Color: 1
Size: 348258 Color: 1
Size: 263242 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 393573 Color: 1
Size: 343772 Color: 1
Size: 262656 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 495809 Color: 0
Size: 252825 Color: 1
Size: 251367 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 358455 Color: 1
Size: 334598 Color: 0
Size: 306948 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 479429 Color: 0
Size: 263122 Color: 1
Size: 257450 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 477873 Color: 0
Size: 267710 Color: 1
Size: 254418 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 377875 Color: 0
Size: 316893 Color: 1
Size: 305233 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 424127 Color: 1
Size: 305891 Color: 0
Size: 269983 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 454649 Color: 0
Size: 282301 Color: 1
Size: 263051 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 414654 Color: 0
Size: 298813 Color: 1
Size: 286534 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 421590 Color: 0
Size: 291204 Color: 1
Size: 287207 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 338618 Color: 0
Size: 330912 Color: 0
Size: 330471 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 497794 Color: 0
Size: 252082 Color: 1
Size: 250125 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 467633 Color: 1
Size: 276669 Color: 1
Size: 255699 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 448239 Color: 1
Size: 291518 Color: 0
Size: 260244 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 475923 Color: 1
Size: 269674 Color: 0
Size: 254404 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 408436 Color: 0
Size: 318704 Color: 1
Size: 272861 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 400794 Color: 0
Size: 333015 Color: 1
Size: 266192 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 458145 Color: 0
Size: 274490 Color: 0
Size: 267366 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 470563 Color: 0
Size: 268032 Color: 0
Size: 261406 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 359368 Color: 1
Size: 329702 Color: 0
Size: 310931 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 360639 Color: 0
Size: 340721 Color: 1
Size: 298641 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 371901 Color: 0
Size: 358698 Color: 1
Size: 269402 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 385892 Color: 0
Size: 319009 Color: 0
Size: 295100 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 449870 Color: 0
Size: 275108 Color: 1
Size: 275023 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 419244 Color: 0
Size: 328578 Color: 0
Size: 252179 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 404034 Color: 0
Size: 323726 Color: 0
Size: 272241 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 463337 Color: 0
Size: 276987 Color: 1
Size: 259677 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 426549 Color: 1
Size: 300737 Color: 1
Size: 272715 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 376374 Color: 0
Size: 350510 Color: 1
Size: 273117 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 483101 Color: 1
Size: 262423 Color: 1
Size: 254477 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 458395 Color: 1
Size: 283110 Color: 0
Size: 258496 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 441367 Color: 1
Size: 306949 Color: 1
Size: 251685 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 372768 Color: 1
Size: 357758 Color: 0
Size: 269475 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 413441 Color: 0
Size: 296196 Color: 0
Size: 290364 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 395332 Color: 0
Size: 346956 Color: 1
Size: 257713 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 389647 Color: 0
Size: 343177 Color: 1
Size: 267177 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 337127 Color: 1
Size: 336376 Color: 1
Size: 326498 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 428299 Color: 0
Size: 305703 Color: 1
Size: 265999 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 487667 Color: 1
Size: 258957 Color: 1
Size: 253377 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 388340 Color: 0
Size: 325054 Color: 0
Size: 286607 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 438605 Color: 0
Size: 307129 Color: 1
Size: 254267 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 391846 Color: 0
Size: 327758 Color: 1
Size: 280397 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 358794 Color: 0
Size: 348188 Color: 1
Size: 293019 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 373837 Color: 0
Size: 315191 Color: 0
Size: 310973 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 408010 Color: 0
Size: 308602 Color: 0
Size: 283389 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 496260 Color: 1
Size: 252042 Color: 1
Size: 251699 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 445202 Color: 1
Size: 285192 Color: 0
Size: 269607 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 400601 Color: 0
Size: 349260 Color: 0
Size: 250140 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 415657 Color: 1
Size: 312741 Color: 0
Size: 271603 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 477101 Color: 1
Size: 270431 Color: 0
Size: 252469 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 374461 Color: 0
Size: 364390 Color: 0
Size: 261150 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 466526 Color: 1
Size: 282602 Color: 0
Size: 250873 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 485240 Color: 1
Size: 263247 Color: 0
Size: 251514 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 497746 Color: 1
Size: 251708 Color: 0
Size: 250547 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 480870 Color: 0
Size: 266243 Color: 0
Size: 252888 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 435298 Color: 1
Size: 311322 Color: 0
Size: 253381 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 440794 Color: 0
Size: 290671 Color: 1
Size: 268536 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 381616 Color: 0
Size: 368280 Color: 1
Size: 250105 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 405324 Color: 0
Size: 303592 Color: 1
Size: 291085 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 482291 Color: 1
Size: 260819 Color: 0
Size: 256891 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 482533 Color: 1
Size: 266386 Color: 0
Size: 251082 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 473892 Color: 0
Size: 272399 Color: 0
Size: 253710 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 399895 Color: 1
Size: 311128 Color: 0
Size: 288978 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 353969 Color: 0
Size: 324311 Color: 1
Size: 321721 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 448498 Color: 0
Size: 298551 Color: 1
Size: 252952 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 479176 Color: 1
Size: 265567 Color: 0
Size: 255258 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 470680 Color: 0
Size: 266930 Color: 1
Size: 262391 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 431402 Color: 0
Size: 313260 Color: 0
Size: 255339 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 458541 Color: 1
Size: 287733 Color: 0
Size: 253727 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 377690 Color: 1
Size: 344777 Color: 0
Size: 277534 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 445056 Color: 0
Size: 284990 Color: 1
Size: 269955 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 443182 Color: 0
Size: 289580 Color: 1
Size: 267239 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 434484 Color: 1
Size: 290684 Color: 0
Size: 274833 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 354929 Color: 1
Size: 345936 Color: 0
Size: 299136 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 474081 Color: 1
Size: 264696 Color: 0
Size: 261224 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 380470 Color: 1
Size: 330422 Color: 0
Size: 289109 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 362222 Color: 1
Size: 323447 Color: 0
Size: 314332 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 372219 Color: 1
Size: 326087 Color: 1
Size: 301695 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 375803 Color: 0
Size: 372871 Color: 1
Size: 251327 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 378023 Color: 0
Size: 345502 Color: 1
Size: 276476 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 375115 Color: 1
Size: 342426 Color: 0
Size: 282460 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 366523 Color: 1
Size: 355312 Color: 0
Size: 278166 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 373997 Color: 1
Size: 314305 Color: 0
Size: 311699 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 429873 Color: 1
Size: 304326 Color: 0
Size: 265802 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 444066 Color: 1
Size: 304446 Color: 0
Size: 251489 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 414593 Color: 1
Size: 331616 Color: 0
Size: 253792 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 379650 Color: 0
Size: 343732 Color: 1
Size: 276619 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 381660 Color: 1
Size: 356414 Color: 1
Size: 261927 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 380387 Color: 1
Size: 336531 Color: 1
Size: 283083 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 482650 Color: 1
Size: 261159 Color: 0
Size: 256192 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 458612 Color: 1
Size: 272587 Color: 0
Size: 268802 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 389620 Color: 0
Size: 349265 Color: 1
Size: 261116 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 353467 Color: 0
Size: 352604 Color: 0
Size: 293930 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 382875 Color: 1
Size: 364816 Color: 0
Size: 252310 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 381255 Color: 0
Size: 348642 Color: 1
Size: 270104 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 341704 Color: 1
Size: 340173 Color: 0
Size: 318124 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 352446 Color: 0
Size: 350678 Color: 0
Size: 296877 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 385997 Color: 1
Size: 358860 Color: 0
Size: 255144 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 469960 Color: 0
Size: 273377 Color: 0
Size: 256664 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 399791 Color: 0
Size: 332353 Color: 1
Size: 267857 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 459816 Color: 0
Size: 281272 Color: 1
Size: 258913 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 367528 Color: 1
Size: 361229 Color: 1
Size: 271244 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 349856 Color: 1
Size: 325210 Color: 1
Size: 324935 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 371793 Color: 1
Size: 348799 Color: 0
Size: 279409 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 364241 Color: 1
Size: 344495 Color: 0
Size: 291265 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 357174 Color: 0
Size: 354351 Color: 1
Size: 288476 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 375627 Color: 1
Size: 353380 Color: 1
Size: 270994 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 371811 Color: 0
Size: 347013 Color: 1
Size: 281177 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 406255 Color: 0
Size: 319073 Color: 0
Size: 274673 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 338621 Color: 0
Size: 334359 Color: 1
Size: 327021 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 363403 Color: 0
Size: 321826 Color: 1
Size: 314772 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 358947 Color: 0
Size: 351444 Color: 0
Size: 289610 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 347325 Color: 0
Size: 329731 Color: 1
Size: 322945 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 362808 Color: 0
Size: 346770 Color: 0
Size: 290423 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 399389 Color: 1
Size: 305027 Color: 1
Size: 295585 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 408557 Color: 1
Size: 318722 Color: 0
Size: 272722 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 406057 Color: 1
Size: 299855 Color: 0
Size: 294089 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 359315 Color: 0
Size: 340465 Color: 0
Size: 300221 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 371753 Color: 1
Size: 316788 Color: 0
Size: 311460 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 365424 Color: 1
Size: 332493 Color: 1
Size: 302084 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 384298 Color: 0
Size: 327947 Color: 1
Size: 287756 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 345003 Color: 0
Size: 339899 Color: 1
Size: 315099 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 350358 Color: 1
Size: 343088 Color: 0
Size: 306555 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 351382 Color: 0
Size: 349499 Color: 0
Size: 299120 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 346987 Color: 0
Size: 337355 Color: 1
Size: 315659 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 491210 Color: 0
Size: 258501 Color: 1
Size: 250290 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 365716 Color: 0
Size: 354876 Color: 0
Size: 279409 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 394400 Color: 0
Size: 339815 Color: 1
Size: 265786 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 377117 Color: 0
Size: 330957 Color: 1
Size: 291927 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 396274 Color: 0
Size: 344184 Color: 1
Size: 259543 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 416826 Color: 1
Size: 294743 Color: 0
Size: 288432 Color: 1

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 433642 Color: 0
Size: 313495 Color: 0
Size: 252864 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 372636 Color: 0
Size: 362921 Color: 0
Size: 264444 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 362709 Color: 1
Size: 353330 Color: 0
Size: 283962 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 369195 Color: 0
Size: 365879 Color: 0
Size: 264927 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 370533 Color: 0
Size: 319995 Color: 1
Size: 309473 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 372780 Color: 0
Size: 364475 Color: 1
Size: 262746 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 372967 Color: 0
Size: 362873 Color: 0
Size: 264161 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 373686 Color: 1
Size: 316721 Color: 1
Size: 309594 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 377215 Color: 0
Size: 368024 Color: 0
Size: 254762 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 377707 Color: 0
Size: 322787 Color: 1
Size: 299507 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 379392 Color: 1
Size: 348074 Color: 1
Size: 272535 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 379724 Color: 1
Size: 346762 Color: 1
Size: 273515 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 380871 Color: 1
Size: 361042 Color: 0
Size: 258088 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 381114 Color: 1
Size: 315622 Color: 1
Size: 303265 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 381747 Color: 0
Size: 323288 Color: 1
Size: 294966 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 383527 Color: 0
Size: 344144 Color: 1
Size: 272330 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 386044 Color: 0
Size: 341822 Color: 0
Size: 272135 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 386589 Color: 0
Size: 343810 Color: 0
Size: 269602 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 386523 Color: 1
Size: 362594 Color: 0
Size: 250884 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 386727 Color: 1
Size: 326627 Color: 1
Size: 286647 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 388438 Color: 0
Size: 334901 Color: 1
Size: 276662 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 389827 Color: 0
Size: 310305 Color: 1
Size: 299869 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 390106 Color: 1
Size: 320629 Color: 0
Size: 289266 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 391338 Color: 1
Size: 332735 Color: 0
Size: 275928 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 392474 Color: 0
Size: 328418 Color: 0
Size: 279109 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 391671 Color: 1
Size: 346556 Color: 1
Size: 261774 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 392388 Color: 1
Size: 305377 Color: 1
Size: 302236 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 393545 Color: 0
Size: 330335 Color: 0
Size: 276121 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 393710 Color: 1
Size: 329256 Color: 0
Size: 277035 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 394660 Color: 1
Size: 353901 Color: 0
Size: 251440 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 395325 Color: 1
Size: 338335 Color: 0
Size: 266341 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 395926 Color: 0
Size: 321045 Color: 1
Size: 283030 Color: 1

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 396343 Color: 0
Size: 303794 Color: 1
Size: 299864 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 396503 Color: 0
Size: 308524 Color: 1
Size: 294974 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 397639 Color: 1
Size: 303996 Color: 0
Size: 298366 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 397976 Color: 1
Size: 350049 Color: 0
Size: 251976 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 398069 Color: 1
Size: 351636 Color: 1
Size: 250296 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 398536 Color: 0
Size: 348596 Color: 1
Size: 252869 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 398892 Color: 1
Size: 320596 Color: 1
Size: 280513 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 399986 Color: 1
Size: 337656 Color: 1
Size: 262359 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 400780 Color: 1
Size: 333606 Color: 0
Size: 265615 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 401396 Color: 0
Size: 320513 Color: 1
Size: 278092 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 402107 Color: 0
Size: 345745 Color: 1
Size: 252149 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 402379 Color: 0
Size: 346466 Color: 1
Size: 251156 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 404204 Color: 1
Size: 299783 Color: 1
Size: 296014 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 405116 Color: 0
Size: 302460 Color: 1
Size: 292425 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 405117 Color: 0
Size: 343052 Color: 1
Size: 251832 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 405224 Color: 0
Size: 337397 Color: 1
Size: 257380 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 404678 Color: 1
Size: 336143 Color: 1
Size: 259180 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 405605 Color: 0
Size: 322980 Color: 0
Size: 271416 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 405726 Color: 1
Size: 320876 Color: 0
Size: 273399 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 406185 Color: 0
Size: 298974 Color: 1
Size: 294842 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 407020 Color: 0
Size: 330866 Color: 1
Size: 262115 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 407507 Color: 0
Size: 320885 Color: 0
Size: 271609 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 408213 Color: 0
Size: 318964 Color: 0
Size: 272824 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 408161 Color: 1
Size: 301370 Color: 1
Size: 290470 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 408695 Color: 0
Size: 322627 Color: 0
Size: 268679 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 408572 Color: 1
Size: 333536 Color: 0
Size: 257893 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 408788 Color: 0
Size: 324037 Color: 1
Size: 267176 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 409602 Color: 0
Size: 322511 Color: 1
Size: 267888 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 411196 Color: 1
Size: 313370 Color: 0
Size: 275435 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 411547 Color: 1
Size: 306800 Color: 0
Size: 281654 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 412599 Color: 0
Size: 310739 Color: 1
Size: 276663 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 412949 Color: 0
Size: 306438 Color: 1
Size: 280614 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 413010 Color: 1
Size: 296448 Color: 0
Size: 290543 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 414810 Color: 1
Size: 334526 Color: 0
Size: 250665 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 414973 Color: 1
Size: 309574 Color: 0
Size: 275454 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 415121 Color: 1
Size: 316497 Color: 0
Size: 268383 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 415734 Color: 0
Size: 327570 Color: 1
Size: 256697 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 415974 Color: 1
Size: 322804 Color: 0
Size: 261223 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 416882 Color: 0
Size: 321995 Color: 0
Size: 261124 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 417592 Color: 0
Size: 326328 Color: 1
Size: 256081 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 418048 Color: 1
Size: 303271 Color: 0
Size: 278682 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 417815 Color: 0
Size: 330682 Color: 0
Size: 251504 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 418759 Color: 0
Size: 306596 Color: 1
Size: 274646 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 419479 Color: 1
Size: 297957 Color: 1
Size: 282565 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 418960 Color: 0
Size: 315626 Color: 0
Size: 265415 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 420241 Color: 1
Size: 304864 Color: 0
Size: 274896 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 420698 Color: 1
Size: 306467 Color: 1
Size: 272836 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 420610 Color: 0
Size: 314108 Color: 0
Size: 265283 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 421443 Color: 0
Size: 307256 Color: 1
Size: 271302 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 422182 Color: 1
Size: 301093 Color: 1
Size: 276726 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 423064 Color: 1
Size: 325673 Color: 0
Size: 251264 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 423172 Color: 0
Size: 307310 Color: 0
Size: 269519 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 423138 Color: 1
Size: 311101 Color: 1
Size: 265762 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 423820 Color: 1
Size: 307526 Color: 1
Size: 268655 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 424399 Color: 1
Size: 318449 Color: 0
Size: 257153 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 424940 Color: 0
Size: 316541 Color: 0
Size: 258520 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 425411 Color: 1
Size: 299590 Color: 0
Size: 275000 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 425328 Color: 0
Size: 288056 Color: 0
Size: 286617 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 425733 Color: 1
Size: 293023 Color: 1
Size: 281245 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 425998 Color: 0
Size: 307164 Color: 0
Size: 266839 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 426080 Color: 1
Size: 320086 Color: 0
Size: 253835 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 426270 Color: 0
Size: 286878 Color: 1
Size: 286853 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 426330 Color: 1
Size: 304675 Color: 1
Size: 268996 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 426829 Color: 0
Size: 289056 Color: 0
Size: 284116 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 426818 Color: 1
Size: 297011 Color: 1
Size: 276172 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 427693 Color: 0
Size: 291689 Color: 1
Size: 280619 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 427804 Color: 0
Size: 316405 Color: 1
Size: 255792 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 427984 Color: 1
Size: 294079 Color: 1
Size: 277938 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 428485 Color: 1
Size: 311035 Color: 1
Size: 260481 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 428601 Color: 1
Size: 286856 Color: 0
Size: 284544 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 428587 Color: 0
Size: 294740 Color: 0
Size: 276674 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 429036 Color: 1
Size: 307822 Color: 1
Size: 263143 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 429677 Color: 0
Size: 305112 Color: 1
Size: 265212 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 430524 Color: 0
Size: 292450 Color: 0
Size: 277027 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 431398 Color: 0
Size: 293253 Color: 0
Size: 275350 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 432092 Color: 0
Size: 298200 Color: 1
Size: 269709 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 432362 Color: 0
Size: 303402 Color: 0
Size: 264237 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 432499 Color: 0
Size: 316942 Color: 1
Size: 250560 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 432823 Color: 0
Size: 307025 Color: 0
Size: 260153 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 433656 Color: 0
Size: 292935 Color: 1
Size: 273410 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 433770 Color: 1
Size: 296136 Color: 1
Size: 270095 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 434378 Color: 1
Size: 311310 Color: 0
Size: 254313 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 434784 Color: 0
Size: 292250 Color: 1
Size: 272967 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 435028 Color: 0
Size: 300856 Color: 1
Size: 264117 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 438162 Color: 0
Size: 311505 Color: 1
Size: 250334 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 438521 Color: 1
Size: 301960 Color: 0
Size: 259520 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 439380 Color: 1
Size: 296462 Color: 1
Size: 264159 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 439702 Color: 1
Size: 285270 Color: 0
Size: 275029 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 441489 Color: 0
Size: 288093 Color: 1
Size: 270419 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 443449 Color: 1
Size: 293525 Color: 0
Size: 263027 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 444187 Color: 1
Size: 304954 Color: 1
Size: 250860 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 445289 Color: 0
Size: 296834 Color: 0
Size: 257878 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 445657 Color: 0
Size: 296405 Color: 1
Size: 257939 Color: 1

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 445996 Color: 0
Size: 292457 Color: 1
Size: 261548 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 447155 Color: 0
Size: 299325 Color: 1
Size: 253521 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 448805 Color: 1
Size: 279546 Color: 0
Size: 271650 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 448710 Color: 0
Size: 276955 Color: 0
Size: 274336 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 449317 Color: 1
Size: 284171 Color: 0
Size: 266513 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 449747 Color: 1
Size: 286662 Color: 0
Size: 263592 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 450555 Color: 1
Size: 282221 Color: 0
Size: 267225 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 450788 Color: 1
Size: 283609 Color: 0
Size: 265604 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 450806 Color: 1
Size: 282942 Color: 0
Size: 266253 Color: 1

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 452051 Color: 0
Size: 295065 Color: 0
Size: 252885 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 452469 Color: 0
Size: 277338 Color: 1
Size: 270194 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 453084 Color: 1
Size: 283526 Color: 0
Size: 263391 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 453023 Color: 0
Size: 285718 Color: 0
Size: 261260 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 453668 Color: 1
Size: 275672 Color: 0
Size: 270661 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 454963 Color: 0
Size: 283612 Color: 0
Size: 261426 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 455467 Color: 0
Size: 273072 Color: 0
Size: 271462 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 456736 Color: 0
Size: 285011 Color: 0
Size: 258254 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 457274 Color: 1
Size: 282328 Color: 0
Size: 260399 Color: 1

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 458260 Color: 1
Size: 287876 Color: 1
Size: 253865 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 458793 Color: 1
Size: 276445 Color: 1
Size: 264763 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 459204 Color: 0
Size: 280410 Color: 1
Size: 260387 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 459107 Color: 1
Size: 283745 Color: 1
Size: 257149 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 459990 Color: 1
Size: 271376 Color: 0
Size: 268635 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 461124 Color: 1
Size: 285222 Color: 1
Size: 253655 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 461316 Color: 1
Size: 270878 Color: 0
Size: 267807 Color: 1

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 461687 Color: 0
Size: 270119 Color: 1
Size: 268195 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 462030 Color: 1
Size: 279832 Color: 1
Size: 258139 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 461992 Color: 0
Size: 283483 Color: 0
Size: 254526 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 462216 Color: 1
Size: 277352 Color: 0
Size: 260433 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 462547 Color: 1
Size: 274992 Color: 0
Size: 262462 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 462677 Color: 1
Size: 286290 Color: 0
Size: 251034 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 462559 Color: 0
Size: 269961 Color: 0
Size: 267481 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 462805 Color: 1
Size: 273366 Color: 0
Size: 263830 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 463526 Color: 0
Size: 271201 Color: 1
Size: 265274 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 464467 Color: 1
Size: 270381 Color: 0
Size: 265153 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 464863 Color: 1
Size: 271294 Color: 1
Size: 263844 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 465512 Color: 0
Size: 275755 Color: 1
Size: 258734 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 465965 Color: 1
Size: 280835 Color: 0
Size: 253201 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 466406 Color: 1
Size: 274420 Color: 0
Size: 259175 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 466365 Color: 0
Size: 277945 Color: 1
Size: 255691 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 467260 Color: 0
Size: 275515 Color: 1
Size: 257226 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 484769 Color: 0
Size: 258155 Color: 1
Size: 257077 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 467885 Color: 1
Size: 279471 Color: 0
Size: 252645 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 469272 Color: 0
Size: 273903 Color: 0
Size: 256826 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 469529 Color: 0
Size: 269897 Color: 1
Size: 260575 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 471353 Color: 1
Size: 270654 Color: 0
Size: 257994 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 472362 Color: 1
Size: 270381 Color: 0
Size: 257258 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 472837 Color: 0
Size: 273989 Color: 1
Size: 253175 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 473360 Color: 0
Size: 274037 Color: 1
Size: 252604 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 473770 Color: 1
Size: 272560 Color: 0
Size: 253671 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 474185 Color: 0
Size: 267060 Color: 0
Size: 258756 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 475808 Color: 1
Size: 271102 Color: 1
Size: 253091 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 475514 Color: 0
Size: 271305 Color: 1
Size: 253182 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 475978 Color: 1
Size: 265580 Color: 0
Size: 258443 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 476697 Color: 1
Size: 273115 Color: 1
Size: 250189 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 476943 Color: 1
Size: 265232 Color: 0
Size: 257826 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 477104 Color: 1
Size: 262341 Color: 1
Size: 260556 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 477668 Color: 0
Size: 267269 Color: 0
Size: 255064 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 477736 Color: 1
Size: 271311 Color: 1
Size: 250954 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 477809 Color: 1
Size: 272087 Color: 1
Size: 250105 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 478410 Color: 0
Size: 262224 Color: 1
Size: 259367 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 479020 Color: 0
Size: 269625 Color: 1
Size: 251356 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 479684 Color: 0
Size: 268726 Color: 0
Size: 251591 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 480217 Color: 1
Size: 267313 Color: 0
Size: 252471 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 480470 Color: 1
Size: 260128 Color: 1
Size: 259403 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 482024 Color: 0
Size: 266642 Color: 0
Size: 251335 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 482047 Color: 0
Size: 260429 Color: 0
Size: 257525 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 482548 Color: 1
Size: 265078 Color: 0
Size: 252375 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 482987 Color: 0
Size: 258899 Color: 1
Size: 258115 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 483730 Color: 1
Size: 265507 Color: 0
Size: 250764 Color: 1

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 486185 Color: 1
Size: 260564 Color: 1
Size: 253252 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 487326 Color: 1
Size: 260027 Color: 1
Size: 252648 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 487728 Color: 0
Size: 258967 Color: 1
Size: 253306 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 488268 Color: 0
Size: 259243 Color: 0
Size: 252490 Color: 1

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 488942 Color: 1
Size: 260430 Color: 0
Size: 250629 Color: 1

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 489988 Color: 0
Size: 256406 Color: 0
Size: 253607 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 489627 Color: 1
Size: 257315 Color: 1
Size: 253059 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 491520 Color: 1
Size: 255675 Color: 1
Size: 252806 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 493743 Color: 0
Size: 255580 Color: 1
Size: 250678 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 493457 Color: 1
Size: 255329 Color: 1
Size: 251215 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 495433 Color: 1
Size: 252577 Color: 0
Size: 251991 Color: 0

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 466604 Color: 1
Size: 279866 Color: 1
Size: 253530 Color: 0

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 353249 Color: 1
Size: 352248 Color: 0
Size: 294503 Color: 1

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 369885 Color: 0
Size: 337102 Color: 1
Size: 293013 Color: 0

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 398584 Color: 0
Size: 343679 Color: 0
Size: 257737 Color: 1

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 369407 Color: 1
Size: 367667 Color: 0
Size: 262926 Color: 0

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 392710 Color: 0
Size: 327533 Color: 0
Size: 279757 Color: 1

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 391749 Color: 1
Size: 326846 Color: 0
Size: 281405 Color: 1

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 398420 Color: 0
Size: 337106 Color: 0
Size: 264474 Color: 1

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 432456 Color: 1
Size: 315454 Color: 1
Size: 252090 Color: 0

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 390603 Color: 1
Size: 350821 Color: 0
Size: 258576 Color: 1

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 381312 Color: 0
Size: 341692 Color: 1
Size: 276996 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 400066 Color: 1
Size: 314784 Color: 0
Size: 285150 Color: 1

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 403355 Color: 0
Size: 339780 Color: 1
Size: 256865 Color: 1

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 485708 Color: 0
Size: 262131 Color: 0
Size: 252161 Color: 1

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 377243 Color: 1
Size: 354127 Color: 0
Size: 268630 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 441742 Color: 0
Size: 282423 Color: 1
Size: 275835 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 425743 Color: 0
Size: 323288 Color: 0
Size: 250969 Color: 1

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 485619 Color: 1
Size: 263212 Color: 0
Size: 251169 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 444573 Color: 1
Size: 298232 Color: 0
Size: 257195 Color: 1

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 389112 Color: 1
Size: 352879 Color: 0
Size: 258009 Color: 1

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 383331 Color: 0
Size: 352014 Color: 1
Size: 264655 Color: 1

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 375921 Color: 1
Size: 327598 Color: 0
Size: 296481 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 443418 Color: 0
Size: 294982 Color: 0
Size: 261600 Color: 1

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 381910 Color: 0
Size: 347184 Color: 1
Size: 270906 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 431023 Color: 1
Size: 286805 Color: 0
Size: 282172 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 424506 Color: 1
Size: 292007 Color: 0
Size: 283487 Color: 1

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 386906 Color: 0
Size: 322997 Color: 0
Size: 290097 Color: 1

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 366074 Color: 1
Size: 337538 Color: 0
Size: 296388 Color: 1

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 367465 Color: 1
Size: 367456 Color: 0
Size: 265079 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 368808 Color: 0
Size: 340965 Color: 1
Size: 290227 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 369445 Color: 1
Size: 352139 Color: 1
Size: 278416 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 371754 Color: 0
Size: 344879 Color: 0
Size: 283367 Color: 1

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 372835 Color: 0
Size: 328511 Color: 1
Size: 298654 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 375000 Color: 1
Size: 344978 Color: 1
Size: 280022 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 377781 Color: 0
Size: 333640 Color: 0
Size: 288579 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 379776 Color: 0
Size: 366716 Color: 0
Size: 253508 Color: 1

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 380807 Color: 1
Size: 365169 Color: 0
Size: 254024 Color: 1

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 381070 Color: 1
Size: 355413 Color: 0
Size: 263517 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 389591 Color: 1
Size: 348416 Color: 0
Size: 261993 Color: 1

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 398009 Color: 1
Size: 326962 Color: 1
Size: 275029 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 404259 Color: 1
Size: 321997 Color: 1
Size: 273744 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 404429 Color: 0
Size: 300673 Color: 0
Size: 294898 Color: 1

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 405392 Color: 1
Size: 306398 Color: 0
Size: 288210 Color: 1

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 405682 Color: 1
Size: 341858 Color: 0
Size: 252460 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 406434 Color: 0
Size: 335565 Color: 1
Size: 258001 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 406909 Color: 1
Size: 341676 Color: 1
Size: 251415 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 408879 Color: 1
Size: 315150 Color: 0
Size: 275971 Color: 1

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 412306 Color: 0
Size: 305487 Color: 0
Size: 282207 Color: 1

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 414563 Color: 0
Size: 310041 Color: 1
Size: 275396 Color: 0

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 415223 Color: 1
Size: 311582 Color: 0
Size: 273195 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 416171 Color: 1
Size: 300436 Color: 0
Size: 283393 Color: 1

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 419842 Color: 1
Size: 319095 Color: 0
Size: 261063 Color: 1

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 421995 Color: 1
Size: 299977 Color: 1
Size: 278028 Color: 0

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 422184 Color: 1
Size: 321077 Color: 0
Size: 256739 Color: 0

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 422403 Color: 0
Size: 313007 Color: 0
Size: 264590 Color: 1

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 498523 Color: 1
Size: 251081 Color: 0
Size: 250396 Color: 0

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 423520 Color: 1
Size: 309130 Color: 1
Size: 267350 Color: 0

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 426359 Color: 0
Size: 293265 Color: 0
Size: 280376 Color: 1

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 428114 Color: 0
Size: 317111 Color: 1
Size: 254775 Color: 0

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 428636 Color: 0
Size: 318832 Color: 1
Size: 252532 Color: 0

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 431073 Color: 1
Size: 318847 Color: 0
Size: 250080 Color: 1

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 438924 Color: 0
Size: 302447 Color: 1
Size: 258629 Color: 1

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 450003 Color: 0
Size: 275668 Color: 1
Size: 274329 Color: 0

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 454896 Color: 1
Size: 272938 Color: 1
Size: 272166 Color: 0

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 461617 Color: 0
Size: 278479 Color: 1
Size: 259904 Color: 1

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 473939 Color: 1
Size: 268093 Color: 0
Size: 257968 Color: 1

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 483241 Color: 1
Size: 264767 Color: 0
Size: 251992 Color: 0

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 345405 Color: 1
Size: 332493 Color: 0
Size: 322102 Color: 1

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 345398 Color: 0
Size: 334219 Color: 1
Size: 320383 Color: 1

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 382744 Color: 1
Size: 322456 Color: 1
Size: 294800 Color: 0

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 456068 Color: 0
Size: 286264 Color: 0
Size: 257668 Color: 1

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 439604 Color: 0
Size: 285085 Color: 1
Size: 275311 Color: 1

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 376365 Color: 1
Size: 345168 Color: 0
Size: 278467 Color: 0

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 368863 Color: 1
Size: 345198 Color: 0
Size: 285939 Color: 0

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 356960 Color: 1
Size: 347349 Color: 0
Size: 295691 Color: 0

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 384510 Color: 1
Size: 350489 Color: 0
Size: 265001 Color: 1

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 352334 Color: 1
Size: 326302 Color: 0
Size: 321364 Color: 1

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 387354 Color: 0
Size: 362244 Color: 1
Size: 250401 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 459819 Color: 1
Size: 273107 Color: 0
Size: 267073 Color: 0

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 389380 Color: 1
Size: 311537 Color: 1
Size: 299082 Color: 0

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 418384 Color: 0
Size: 325496 Color: 1
Size: 256119 Color: 1

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 361624 Color: 1
Size: 333833 Color: 0
Size: 304542 Color: 0

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 399741 Color: 0
Size: 320628 Color: 1
Size: 279630 Color: 0

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 476907 Color: 1
Size: 261797 Color: 0
Size: 261295 Color: 0

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 387123 Color: 0
Size: 327505 Color: 1
Size: 285371 Color: 1

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 420344 Color: 0
Size: 314595 Color: 0
Size: 265060 Color: 1

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 494539 Color: 1
Size: 253214 Color: 0
Size: 252246 Color: 1

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 385524 Color: 0
Size: 315622 Color: 1
Size: 298853 Color: 1

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 460000 Color: 1
Size: 288834 Color: 0
Size: 251165 Color: 1

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 474866 Color: 1
Size: 266332 Color: 1
Size: 258801 Color: 0

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 352787 Color: 0
Size: 352410 Color: 0
Size: 294802 Color: 1

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 370466 Color: 0
Size: 370229 Color: 1
Size: 259304 Color: 1

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 410098 Color: 1
Size: 300022 Color: 1
Size: 289879 Color: 0

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 401649 Color: 0
Size: 347810 Color: 1
Size: 250540 Color: 0

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 422366 Color: 1
Size: 326845 Color: 0
Size: 250788 Color: 1

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 358839 Color: 0
Size: 331895 Color: 1
Size: 309265 Color: 0

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 359727 Color: 1
Size: 355331 Color: 1
Size: 284941 Color: 0

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 380239 Color: 1
Size: 319942 Color: 0
Size: 299818 Color: 0

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 388214 Color: 0
Size: 322961 Color: 0
Size: 288824 Color: 1

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 391666 Color: 1
Size: 331317 Color: 0
Size: 277016 Color: 1

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 392596 Color: 0
Size: 342782 Color: 0
Size: 264621 Color: 1

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 396387 Color: 0
Size: 336350 Color: 1
Size: 267262 Color: 0

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 400873 Color: 1
Size: 342875 Color: 1
Size: 256251 Color: 0

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 404820 Color: 1
Size: 340748 Color: 0
Size: 254431 Color: 1

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 418189 Color: 0
Size: 302778 Color: 1
Size: 279032 Color: 1

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 420040 Color: 0
Size: 304056 Color: 1
Size: 275903 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 460904 Color: 1
Size: 282349 Color: 0
Size: 256746 Color: 0

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 348950 Color: 1
Size: 347699 Color: 0
Size: 303350 Color: 1

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 371165 Color: 1
Size: 342095 Color: 0
Size: 286739 Color: 1

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 349076 Color: 1
Size: 340718 Color: 0
Size: 310205 Color: 1

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 345243 Color: 0
Size: 331461 Color: 1
Size: 323295 Color: 1

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 412130 Color: 1
Size: 316325 Color: 0
Size: 271543 Color: 1

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 417998 Color: 1
Size: 295713 Color: 0
Size: 286287 Color: 0

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 481679 Color: 0
Size: 268170 Color: 1
Size: 250149 Color: 0

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 357856 Color: 1
Size: 328353 Color: 1
Size: 313789 Color: 0

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 365016 Color: 0
Size: 361035 Color: 0
Size: 273947 Color: 1

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 368909 Color: 0
Size: 319045 Color: 1
Size: 312044 Color: 0

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 381997 Color: 1
Size: 348154 Color: 0
Size: 269847 Color: 1

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 350678 Color: 0
Size: 334318 Color: 0
Size: 315002 Color: 1

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 361041 Color: 0
Size: 322295 Color: 1
Size: 316662 Color: 0

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 344214 Color: 1
Size: 341694 Color: 0
Size: 314090 Color: 0

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 455279 Color: 1
Size: 290427 Color: 0
Size: 254292 Color: 0

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 385403 Color: 1
Size: 325289 Color: 1
Size: 289306 Color: 0

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 365893 Color: 1
Size: 346035 Color: 1
Size: 288070 Color: 0

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 392705 Color: 1
Size: 333640 Color: 1
Size: 273653 Color: 0

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 393711 Color: 1
Size: 317155 Color: 1
Size: 289132 Color: 0

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 409508 Color: 0
Size: 310352 Color: 1
Size: 280138 Color: 0

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 444177 Color: 1
Size: 281563 Color: 0
Size: 274258 Color: 0

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 345832 Color: 1
Size: 328788 Color: 0
Size: 325378 Color: 0

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 356153 Color: 0
Size: 347336 Color: 1
Size: 296509 Color: 0

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 343985 Color: 1
Size: 339760 Color: 1
Size: 316253 Color: 0

Bin 520: 4 of cap free
Amount of items: 3
Items: 
Size: 381092 Color: 0
Size: 316376 Color: 1
Size: 302529 Color: 0

Bin 521: 4 of cap free
Amount of items: 3
Items: 
Size: 409432 Color: 0
Size: 319017 Color: 1
Size: 271548 Color: 1

Bin 522: 4 of cap free
Amount of items: 3
Items: 
Size: 405395 Color: 0
Size: 301964 Color: 0
Size: 292638 Color: 1

Bin 523: 4 of cap free
Amount of items: 3
Items: 
Size: 370552 Color: 0
Size: 346936 Color: 1
Size: 282509 Color: 0

Bin 524: 4 of cap free
Amount of items: 3
Items: 
Size: 397254 Color: 0
Size: 352504 Color: 1
Size: 250239 Color: 0

Bin 525: 4 of cap free
Amount of items: 3
Items: 
Size: 389913 Color: 0
Size: 314416 Color: 1
Size: 295668 Color: 0

Bin 526: 4 of cap free
Amount of items: 3
Items: 
Size: 390044 Color: 0
Size: 343227 Color: 0
Size: 266726 Color: 1

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 441439 Color: 1
Size: 307465 Color: 0
Size: 251093 Color: 0

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 362283 Color: 0
Size: 327889 Color: 1
Size: 309825 Color: 1

Bin 529: 4 of cap free
Amount of items: 3
Items: 
Size: 343437 Color: 1
Size: 331356 Color: 0
Size: 325204 Color: 0

Bin 530: 5 of cap free
Amount of items: 3
Items: 
Size: 435849 Color: 1
Size: 312940 Color: 0
Size: 251207 Color: 1

Bin 531: 5 of cap free
Amount of items: 3
Items: 
Size: 487000 Color: 1
Size: 257842 Color: 0
Size: 255154 Color: 0

Bin 532: 5 of cap free
Amount of items: 3
Items: 
Size: 429448 Color: 0
Size: 296471 Color: 0
Size: 274077 Color: 1

Bin 533: 5 of cap free
Amount of items: 3
Items: 
Size: 373140 Color: 0
Size: 348531 Color: 0
Size: 278325 Color: 1

Bin 534: 5 of cap free
Amount of items: 3
Items: 
Size: 354265 Color: 0
Size: 341495 Color: 0
Size: 304236 Color: 1

Bin 535: 5 of cap free
Amount of items: 3
Items: 
Size: 355765 Color: 1
Size: 329672 Color: 1
Size: 314559 Color: 0

Bin 536: 5 of cap free
Amount of items: 3
Items: 
Size: 394641 Color: 1
Size: 329284 Color: 0
Size: 276071 Color: 0

Bin 537: 6 of cap free
Amount of items: 3
Items: 
Size: 443723 Color: 0
Size: 286236 Color: 0
Size: 270036 Color: 1

Bin 538: 6 of cap free
Amount of items: 3
Items: 
Size: 366651 Color: 1
Size: 336630 Color: 0
Size: 296714 Color: 1

Bin 539: 6 of cap free
Amount of items: 3
Items: 
Size: 384334 Color: 0
Size: 338477 Color: 1
Size: 277184 Color: 1

Bin 540: 6 of cap free
Amount of items: 3
Items: 
Size: 451802 Color: 1
Size: 281795 Color: 1
Size: 266398 Color: 0

Bin 541: 6 of cap free
Amount of items: 3
Items: 
Size: 392195 Color: 0
Size: 344630 Color: 1
Size: 263170 Color: 0

Bin 542: 6 of cap free
Amount of items: 3
Items: 
Size: 413152 Color: 1
Size: 315296 Color: 0
Size: 271547 Color: 0

Bin 543: 6 of cap free
Amount of items: 3
Items: 
Size: 397617 Color: 0
Size: 350932 Color: 1
Size: 251446 Color: 0

Bin 544: 6 of cap free
Amount of items: 3
Items: 
Size: 358455 Color: 1
Size: 322243 Color: 0
Size: 319297 Color: 1

Bin 545: 6 of cap free
Amount of items: 3
Items: 
Size: 379202 Color: 1
Size: 337906 Color: 0
Size: 282887 Color: 1

Bin 546: 6 of cap free
Amount of items: 3
Items: 
Size: 403845 Color: 1
Size: 300389 Color: 1
Size: 295761 Color: 0

Bin 547: 7 of cap free
Amount of items: 3
Items: 
Size: 484765 Color: 1
Size: 261378 Color: 1
Size: 253851 Color: 0

Bin 548: 7 of cap free
Amount of items: 3
Items: 
Size: 451204 Color: 1
Size: 294031 Color: 1
Size: 254759 Color: 0

Bin 549: 7 of cap free
Amount of items: 3
Items: 
Size: 358937 Color: 0
Size: 346138 Color: 1
Size: 294919 Color: 1

Bin 550: 7 of cap free
Amount of items: 3
Items: 
Size: 394392 Color: 0
Size: 306614 Color: 1
Size: 298988 Color: 0

Bin 551: 7 of cap free
Amount of items: 3
Items: 
Size: 375781 Color: 1
Size: 314459 Color: 0
Size: 309754 Color: 1

Bin 552: 8 of cap free
Amount of items: 3
Items: 
Size: 409389 Color: 0
Size: 313526 Color: 1
Size: 277078 Color: 0

Bin 553: 8 of cap free
Amount of items: 3
Items: 
Size: 356417 Color: 1
Size: 322255 Color: 0
Size: 321321 Color: 0

Bin 554: 9 of cap free
Amount of items: 3
Items: 
Size: 365770 Color: 0
Size: 350180 Color: 1
Size: 284042 Color: 0

Bin 555: 9 of cap free
Amount of items: 3
Items: 
Size: 364906 Color: 1
Size: 354459 Color: 0
Size: 280627 Color: 1

Bin 556: 9 of cap free
Amount of items: 3
Items: 
Size: 353072 Color: 0
Size: 351220 Color: 1
Size: 295700 Color: 1

Bin 557: 10 of cap free
Amount of items: 3
Items: 
Size: 347205 Color: 0
Size: 329742 Color: 0
Size: 323044 Color: 1

Bin 558: 10 of cap free
Amount of items: 3
Items: 
Size: 370241 Color: 1
Size: 370147 Color: 0
Size: 259603 Color: 1

Bin 559: 10 of cap free
Amount of items: 3
Items: 
Size: 373632 Color: 1
Size: 336769 Color: 0
Size: 289590 Color: 1

Bin 560: 10 of cap free
Amount of items: 3
Items: 
Size: 416757 Color: 1
Size: 304475 Color: 0
Size: 278759 Color: 1

Bin 561: 11 of cap free
Amount of items: 3
Items: 
Size: 445276 Color: 1
Size: 284715 Color: 1
Size: 269999 Color: 0

Bin 562: 12 of cap free
Amount of items: 3
Items: 
Size: 358859 Color: 0
Size: 327920 Color: 1
Size: 313210 Color: 0

Bin 563: 12 of cap free
Amount of items: 3
Items: 
Size: 390412 Color: 0
Size: 306567 Color: 0
Size: 303010 Color: 1

Bin 564: 12 of cap free
Amount of items: 3
Items: 
Size: 498753 Color: 0
Size: 250850 Color: 1
Size: 250386 Color: 0

Bin 565: 12 of cap free
Amount of items: 3
Items: 
Size: 499071 Color: 0
Size: 250873 Color: 0
Size: 250045 Color: 1

Bin 566: 12 of cap free
Amount of items: 3
Items: 
Size: 402588 Color: 0
Size: 310559 Color: 0
Size: 286842 Color: 1

Bin 567: 12 of cap free
Amount of items: 3
Items: 
Size: 442626 Color: 0
Size: 279772 Color: 1
Size: 277591 Color: 1

Bin 568: 12 of cap free
Amount of items: 3
Items: 
Size: 465834 Color: 1
Size: 272455 Color: 1
Size: 261700 Color: 0

Bin 569: 13 of cap free
Amount of items: 3
Items: 
Size: 358817 Color: 1
Size: 358130 Color: 1
Size: 283041 Color: 0

Bin 570: 13 of cap free
Amount of items: 3
Items: 
Size: 368352 Color: 1
Size: 321886 Color: 0
Size: 309750 Color: 0

Bin 571: 14 of cap free
Amount of items: 3
Items: 
Size: 457936 Color: 0
Size: 278917 Color: 0
Size: 263134 Color: 1

Bin 572: 14 of cap free
Amount of items: 3
Items: 
Size: 411039 Color: 1
Size: 319534 Color: 0
Size: 269414 Color: 1

Bin 573: 14 of cap free
Amount of items: 3
Items: 
Size: 439468 Color: 1
Size: 297474 Color: 0
Size: 263045 Color: 0

Bin 574: 15 of cap free
Amount of items: 3
Items: 
Size: 471821 Color: 1
Size: 271660 Color: 0
Size: 256505 Color: 1

Bin 575: 15 of cap free
Amount of items: 3
Items: 
Size: 348627 Color: 1
Size: 333095 Color: 0
Size: 318264 Color: 1

Bin 576: 16 of cap free
Amount of items: 3
Items: 
Size: 433176 Color: 1
Size: 291640 Color: 1
Size: 275169 Color: 0

Bin 577: 16 of cap free
Amount of items: 3
Items: 
Size: 435075 Color: 0
Size: 286203 Color: 1
Size: 278707 Color: 0

Bin 578: 16 of cap free
Amount of items: 3
Items: 
Size: 476435 Color: 1
Size: 270144 Color: 0
Size: 253406 Color: 1

Bin 579: 16 of cap free
Amount of items: 3
Items: 
Size: 404310 Color: 0
Size: 301759 Color: 1
Size: 293916 Color: 0

Bin 580: 16 of cap free
Amount of items: 3
Items: 
Size: 381627 Color: 1
Size: 354385 Color: 1
Size: 263973 Color: 0

Bin 581: 16 of cap free
Amount of items: 3
Items: 
Size: 417539 Color: 1
Size: 295827 Color: 1
Size: 286619 Color: 0

Bin 582: 16 of cap free
Amount of items: 3
Items: 
Size: 359312 Color: 0
Size: 322211 Color: 0
Size: 318462 Color: 1

Bin 583: 16 of cap free
Amount of items: 3
Items: 
Size: 353961 Color: 1
Size: 328540 Color: 0
Size: 317484 Color: 0

Bin 584: 17 of cap free
Amount of items: 3
Items: 
Size: 494711 Color: 0
Size: 252872 Color: 1
Size: 252401 Color: 0

Bin 585: 17 of cap free
Amount of items: 3
Items: 
Size: 462078 Color: 1
Size: 279259 Color: 1
Size: 258647 Color: 0

Bin 586: 17 of cap free
Amount of items: 3
Items: 
Size: 416536 Color: 0
Size: 296176 Color: 0
Size: 287272 Color: 1

Bin 587: 18 of cap free
Amount of items: 3
Items: 
Size: 369270 Color: 0
Size: 338315 Color: 1
Size: 292398 Color: 0

Bin 588: 18 of cap free
Amount of items: 3
Items: 
Size: 385538 Color: 0
Size: 339303 Color: 1
Size: 275142 Color: 1

Bin 589: 19 of cap free
Amount of items: 3
Items: 
Size: 371958 Color: 0
Size: 338053 Color: 1
Size: 289971 Color: 0

Bin 590: 19 of cap free
Amount of items: 3
Items: 
Size: 371043 Color: 0
Size: 346669 Color: 1
Size: 282270 Color: 0

Bin 591: 19 of cap free
Amount of items: 3
Items: 
Size: 412596 Color: 0
Size: 295491 Color: 0
Size: 291895 Color: 1

Bin 592: 20 of cap free
Amount of items: 3
Items: 
Size: 340965 Color: 1
Size: 331783 Color: 0
Size: 327233 Color: 0

Bin 593: 21 of cap free
Amount of items: 3
Items: 
Size: 351544 Color: 1
Size: 340266 Color: 1
Size: 308170 Color: 0

Bin 594: 21 of cap free
Amount of items: 3
Items: 
Size: 350735 Color: 0
Size: 326552 Color: 1
Size: 322693 Color: 0

Bin 595: 22 of cap free
Amount of items: 3
Items: 
Size: 383374 Color: 0
Size: 350341 Color: 1
Size: 266264 Color: 0

Bin 596: 23 of cap free
Amount of items: 3
Items: 
Size: 479418 Color: 0
Size: 261131 Color: 1
Size: 259429 Color: 1

Bin 597: 24 of cap free
Amount of items: 3
Items: 
Size: 337564 Color: 0
Size: 335273 Color: 1
Size: 327140 Color: 1

Bin 598: 24 of cap free
Amount of items: 3
Items: 
Size: 392743 Color: 0
Size: 319738 Color: 0
Size: 287496 Color: 1

Bin 599: 24 of cap free
Amount of items: 3
Items: 
Size: 453684 Color: 1
Size: 273275 Color: 1
Size: 273018 Color: 0

Bin 600: 26 of cap free
Amount of items: 3
Items: 
Size: 410479 Color: 1
Size: 301216 Color: 0
Size: 288280 Color: 1

Bin 601: 27 of cap free
Amount of items: 3
Items: 
Size: 448249 Color: 1
Size: 289534 Color: 0
Size: 262191 Color: 1

Bin 602: 28 of cap free
Amount of items: 3
Items: 
Size: 432423 Color: 1
Size: 295414 Color: 1
Size: 272136 Color: 0

Bin 603: 28 of cap free
Amount of items: 3
Items: 
Size: 463905 Color: 1
Size: 271030 Color: 0
Size: 265038 Color: 1

Bin 604: 29 of cap free
Amount of items: 3
Items: 
Size: 407771 Color: 0
Size: 331661 Color: 1
Size: 260540 Color: 1

Bin 605: 29 of cap free
Amount of items: 3
Items: 
Size: 366807 Color: 0
Size: 335994 Color: 1
Size: 297171 Color: 0

Bin 606: 32 of cap free
Amount of items: 3
Items: 
Size: 349747 Color: 0
Size: 329366 Color: 0
Size: 320856 Color: 1

Bin 607: 34 of cap free
Amount of items: 3
Items: 
Size: 445020 Color: 0
Size: 282918 Color: 1
Size: 272029 Color: 0

Bin 608: 41 of cap free
Amount of items: 3
Items: 
Size: 457710 Color: 1
Size: 273328 Color: 0
Size: 268922 Color: 0

Bin 609: 42 of cap free
Amount of items: 3
Items: 
Size: 456134 Color: 1
Size: 285624 Color: 1
Size: 258201 Color: 0

Bin 610: 42 of cap free
Amount of items: 3
Items: 
Size: 464916 Color: 0
Size: 270754 Color: 0
Size: 264289 Color: 1

Bin 611: 47 of cap free
Amount of items: 3
Items: 
Size: 374193 Color: 0
Size: 329070 Color: 1
Size: 296691 Color: 1

Bin 612: 48 of cap free
Amount of items: 3
Items: 
Size: 441611 Color: 1
Size: 297858 Color: 1
Size: 260484 Color: 0

Bin 613: 52 of cap free
Amount of items: 3
Items: 
Size: 406837 Color: 1
Size: 318859 Color: 1
Size: 274253 Color: 0

Bin 614: 64 of cap free
Amount of items: 3
Items: 
Size: 456406 Color: 1
Size: 273800 Color: 0
Size: 269731 Color: 0

Bin 615: 65 of cap free
Amount of items: 3
Items: 
Size: 380711 Color: 1
Size: 334689 Color: 0
Size: 284536 Color: 0

Bin 616: 66 of cap free
Amount of items: 3
Items: 
Size: 447325 Color: 1
Size: 280403 Color: 0
Size: 272207 Color: 1

Bin 617: 72 of cap free
Amount of items: 3
Items: 
Size: 398994 Color: 1
Size: 301402 Color: 1
Size: 299533 Color: 0

Bin 618: 76 of cap free
Amount of items: 3
Items: 
Size: 369962 Color: 1
Size: 319729 Color: 0
Size: 310234 Color: 1

Bin 619: 76 of cap free
Amount of items: 3
Items: 
Size: 370850 Color: 1
Size: 323263 Color: 0
Size: 305812 Color: 0

Bin 620: 77 of cap free
Amount of items: 3
Items: 
Size: 431336 Color: 1
Size: 297684 Color: 0
Size: 270904 Color: 0

Bin 621: 77 of cap free
Amount of items: 3
Items: 
Size: 445018 Color: 1
Size: 280834 Color: 0
Size: 274072 Color: 1

Bin 622: 91 of cap free
Amount of items: 3
Items: 
Size: 351015 Color: 1
Size: 327966 Color: 0
Size: 320929 Color: 1

Bin 623: 94 of cap free
Amount of items: 3
Items: 
Size: 430510 Color: 1
Size: 285885 Color: 1
Size: 283512 Color: 0

Bin 624: 96 of cap free
Amount of items: 3
Items: 
Size: 417870 Color: 1
Size: 310664 Color: 1
Size: 271371 Color: 0

Bin 625: 97 of cap free
Amount of items: 3
Items: 
Size: 494563 Color: 0
Size: 254792 Color: 0
Size: 250549 Color: 1

Bin 626: 104 of cap free
Amount of items: 3
Items: 
Size: 441589 Color: 1
Size: 285177 Color: 1
Size: 273131 Color: 0

Bin 627: 111 of cap free
Amount of items: 3
Items: 
Size: 376524 Color: 1
Size: 321968 Color: 0
Size: 301398 Color: 1

Bin 628: 121 of cap free
Amount of items: 3
Items: 
Size: 351666 Color: 1
Size: 329161 Color: 0
Size: 319053 Color: 0

Bin 629: 128 of cap free
Amount of items: 3
Items: 
Size: 496079 Color: 1
Size: 252720 Color: 1
Size: 251074 Color: 0

Bin 630: 133 of cap free
Amount of items: 3
Items: 
Size: 432734 Color: 1
Size: 304444 Color: 0
Size: 262690 Color: 0

Bin 631: 136 of cap free
Amount of items: 3
Items: 
Size: 419493 Color: 1
Size: 314951 Color: 1
Size: 265421 Color: 0

Bin 632: 138 of cap free
Amount of items: 3
Items: 
Size: 414931 Color: 1
Size: 318779 Color: 1
Size: 266153 Color: 0

Bin 633: 145 of cap free
Amount of items: 3
Items: 
Size: 376153 Color: 0
Size: 346091 Color: 1
Size: 277612 Color: 0

Bin 634: 154 of cap free
Amount of items: 3
Items: 
Size: 412952 Color: 0
Size: 317362 Color: 1
Size: 269533 Color: 1

Bin 635: 156 of cap free
Amount of items: 3
Items: 
Size: 373710 Color: 1
Size: 322333 Color: 0
Size: 303802 Color: 1

Bin 636: 163 of cap free
Amount of items: 3
Items: 
Size: 467967 Color: 0
Size: 272307 Color: 0
Size: 259564 Color: 1

Bin 637: 167 of cap free
Amount of items: 3
Items: 
Size: 495675 Color: 1
Size: 252856 Color: 0
Size: 251303 Color: 1

Bin 638: 180 of cap free
Amount of items: 3
Items: 
Size: 392498 Color: 0
Size: 304693 Color: 1
Size: 302630 Color: 1

Bin 639: 185 of cap free
Amount of items: 3
Items: 
Size: 354529 Color: 0
Size: 345488 Color: 1
Size: 299799 Color: 0

Bin 640: 193 of cap free
Amount of items: 3
Items: 
Size: 470214 Color: 0
Size: 272774 Color: 1
Size: 256820 Color: 1

Bin 641: 211 of cap free
Amount of items: 3
Items: 
Size: 421454 Color: 1
Size: 316826 Color: 0
Size: 261510 Color: 0

Bin 642: 242 of cap free
Amount of items: 3
Items: 
Size: 363080 Color: 0
Size: 327672 Color: 1
Size: 309007 Color: 0

Bin 643: 245 of cap free
Amount of items: 3
Items: 
Size: 338907 Color: 1
Size: 338117 Color: 0
Size: 322732 Color: 0

Bin 644: 251 of cap free
Amount of items: 3
Items: 
Size: 348300 Color: 1
Size: 338599 Color: 1
Size: 312851 Color: 0

Bin 645: 300 of cap free
Amount of items: 3
Items: 
Size: 412527 Color: 1
Size: 306474 Color: 0
Size: 280700 Color: 1

Bin 646: 302 of cap free
Amount of items: 3
Items: 
Size: 358397 Color: 1
Size: 350615 Color: 0
Size: 290687 Color: 0

Bin 647: 306 of cap free
Amount of items: 3
Items: 
Size: 474289 Color: 1
Size: 264363 Color: 0
Size: 261043 Color: 1

Bin 648: 318 of cap free
Amount of items: 3
Items: 
Size: 365118 Color: 1
Size: 326811 Color: 1
Size: 307754 Color: 0

Bin 649: 345 of cap free
Amount of items: 3
Items: 
Size: 366604 Color: 0
Size: 346436 Color: 0
Size: 286616 Color: 1

Bin 650: 365 of cap free
Amount of items: 3
Items: 
Size: 418400 Color: 1
Size: 293134 Color: 0
Size: 288102 Color: 1

Bin 651: 401 of cap free
Amount of items: 3
Items: 
Size: 404860 Color: 0
Size: 341881 Color: 0
Size: 252859 Color: 1

Bin 652: 430 of cap free
Amount of items: 2
Items: 
Size: 499918 Color: 1
Size: 499653 Color: 0

Bin 653: 468 of cap free
Amount of items: 3
Items: 
Size: 407078 Color: 1
Size: 334068 Color: 0
Size: 258387 Color: 1

Bin 654: 503 of cap free
Amount of items: 3
Items: 
Size: 390501 Color: 1
Size: 316694 Color: 0
Size: 292303 Color: 1

Bin 655: 631 of cap free
Amount of items: 3
Items: 
Size: 460573 Color: 1
Size: 275290 Color: 0
Size: 263507 Color: 0

Bin 656: 679 of cap free
Amount of items: 3
Items: 
Size: 379471 Color: 1
Size: 315554 Color: 0
Size: 304297 Color: 1

Bin 657: 690 of cap free
Amount of items: 3
Items: 
Size: 395481 Color: 0
Size: 335478 Color: 1
Size: 268352 Color: 0

Bin 658: 890 of cap free
Amount of items: 3
Items: 
Size: 423810 Color: 0
Size: 293722 Color: 1
Size: 281579 Color: 1

Bin 659: 1019 of cap free
Amount of items: 3
Items: 
Size: 386128 Color: 1
Size: 329481 Color: 0
Size: 283373 Color: 0

Bin 660: 1574 of cap free
Amount of items: 3
Items: 
Size: 497673 Color: 0
Size: 250461 Color: 1
Size: 250293 Color: 0

Bin 661: 1606 of cap free
Amount of items: 3
Items: 
Size: 452887 Color: 0
Size: 275602 Color: 1
Size: 269906 Color: 0

Bin 662: 3682 of cap free
Amount of items: 3
Items: 
Size: 464477 Color: 1
Size: 275277 Color: 0
Size: 256565 Color: 0

Bin 663: 4391 of cap free
Amount of items: 3
Items: 
Size: 352744 Color: 0
Size: 340382 Color: 1
Size: 302484 Color: 1

Bin 664: 13744 of cap free
Amount of items: 3
Items: 
Size: 404267 Color: 1
Size: 307740 Color: 1
Size: 274250 Color: 0

Bin 665: 227243 of cap free
Amount of items: 3
Items: 
Size: 261497 Color: 1
Size: 255896 Color: 0
Size: 255365 Color: 0

Bin 666: 240206 of cap free
Amount of items: 3
Items: 
Size: 254999 Color: 1
Size: 254649 Color: 0
Size: 250147 Color: 1

Bin 667: 246204 of cap free
Amount of items: 2
Items: 
Size: 499336 Color: 1
Size: 254461 Color: 0

Bin 668: 247983 of cap free
Amount of items: 2
Items: 
Size: 498838 Color: 1
Size: 253180 Color: 0

Total size: 667000667
Total free space: 1000001

