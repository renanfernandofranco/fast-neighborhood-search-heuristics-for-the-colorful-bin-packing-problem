Capicity Bin: 8184
Lower Bound: 132

Bins used: 132
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 6528 Color: 1
Size: 1016 Color: 2
Size: 456 Color: 2
Size: 136 Color: 4
Size: 48 Color: 3

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 4040 Color: 2
Size: 1224 Color: 2
Size: 1112 Color: 1
Size: 624 Color: 3
Size: 520 Color: 0
Size: 416 Color: 2
Size: 152 Color: 1
Size: 96 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 4
Size: 1252 Color: 4
Size: 248 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5649 Color: 2
Size: 2467 Color: 4
Size: 68 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 2
Size: 804 Color: 2
Size: 160 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6303 Color: 0
Size: 1719 Color: 4
Size: 162 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5285 Color: 4
Size: 2417 Color: 0
Size: 482 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7342 Color: 4
Size: 702 Color: 0
Size: 140 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5084 Color: 2
Size: 2796 Color: 1
Size: 304 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6374 Color: 2
Size: 1510 Color: 4
Size: 300 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 0
Size: 1249 Color: 0
Size: 248 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 2
Size: 714 Color: 0
Size: 140 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 4
Size: 2372 Color: 2
Size: 200 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 4
Size: 2153 Color: 3
Size: 430 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6390 Color: 2
Size: 1498 Color: 3
Size: 296 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 3
Size: 1202 Color: 3
Size: 236 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 1
Size: 1052 Color: 1
Size: 208 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 2
Size: 2860 Color: 2
Size: 568 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6973 Color: 3
Size: 1011 Color: 1
Size: 200 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7060 Color: 4
Size: 940 Color: 2
Size: 184 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5661 Color: 0
Size: 2103 Color: 2
Size: 420 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 2
Size: 781 Color: 3
Size: 156 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6437 Color: 2
Size: 1457 Color: 1
Size: 290 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 0
Size: 1412 Color: 2
Size: 280 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 2
Size: 882 Color: 0
Size: 176 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 0
Size: 1092 Color: 2
Size: 216 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 0
Size: 1187 Color: 0
Size: 74 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4487 Color: 0
Size: 3081 Color: 4
Size: 616 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 4
Size: 2990 Color: 0
Size: 596 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7278 Color: 1
Size: 846 Color: 2
Size: 60 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 0
Size: 1018 Color: 1
Size: 200 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7038 Color: 4
Size: 958 Color: 3
Size: 188 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 0
Size: 938 Color: 1
Size: 184 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6941 Color: 2
Size: 1037 Color: 3
Size: 206 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4094 Color: 2
Size: 3410 Color: 4
Size: 680 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 4
Size: 754 Color: 1
Size: 80 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5617 Color: 1
Size: 2141 Color: 2
Size: 426 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7202 Color: 3
Size: 822 Color: 1
Size: 160 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 0
Size: 764 Color: 4
Size: 144 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 3
Size: 1700 Color: 4
Size: 336 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 3
Size: 1450 Color: 0
Size: 188 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 2
Size: 1627 Color: 1
Size: 324 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 0
Size: 2494 Color: 0
Size: 176 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6028 Color: 4
Size: 2092 Color: 2
Size: 64 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 3
Size: 1564 Color: 2
Size: 312 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 2
Size: 1524 Color: 3
Size: 296 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 4
Size: 2550 Color: 0
Size: 508 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4444 Color: 4
Size: 3124 Color: 2
Size: 616 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 3
Size: 1020 Color: 0
Size: 200 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6828 Color: 3
Size: 1132 Color: 3
Size: 224 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 4
Size: 1948 Color: 4
Size: 384 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 0
Size: 1844 Color: 3
Size: 360 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6675 Color: 0
Size: 1291 Color: 4
Size: 218 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 3
Size: 2076 Color: 0
Size: 408 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7053 Color: 3
Size: 943 Color: 3
Size: 188 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 0
Size: 876 Color: 3
Size: 168 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 3
Size: 1256 Color: 2
Size: 248 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7183 Color: 3
Size: 835 Color: 4
Size: 166 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 0
Size: 1894 Color: 3
Size: 376 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6311 Color: 2
Size: 1561 Color: 1
Size: 312 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6162 Color: 4
Size: 1686 Color: 4
Size: 336 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 7146 Color: 3
Size: 866 Color: 2
Size: 172 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4708 Color: 1
Size: 3252 Color: 0
Size: 224 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 1
Size: 1086 Color: 0
Size: 216 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4288 Color: 3
Size: 3256 Color: 1
Size: 640 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 1
Size: 2660 Color: 2
Size: 528 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5382 Color: 4
Size: 2338 Color: 0
Size: 464 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5078 Color: 0
Size: 2590 Color: 4
Size: 516 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5618 Color: 4
Size: 2142 Color: 3
Size: 424 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 0
Size: 748 Color: 3
Size: 144 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 2
Size: 1853 Color: 3
Size: 370 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 3592 Color: 0
Size: 3016 Color: 2
Size: 1576 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5150 Color: 0
Size: 2626 Color: 4
Size: 408 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 4095 Color: 2
Size: 3577 Color: 1
Size: 512 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4388 Color: 3
Size: 3164 Color: 4
Size: 632 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5562 Color: 2
Size: 2186 Color: 1
Size: 436 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 0
Size: 1318 Color: 4
Size: 260 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 1
Size: 2244 Color: 4
Size: 440 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4093 Color: 2
Size: 3411 Color: 1
Size: 680 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7154 Color: 4
Size: 862 Color: 1
Size: 168 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5301 Color: 3
Size: 2403 Color: 4
Size: 480 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 4
Size: 1014 Color: 4
Size: 200 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 2
Size: 914 Color: 3
Size: 76 Color: 4

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 3
Size: 1334 Color: 4
Size: 264 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7138 Color: 2
Size: 874 Color: 1
Size: 172 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 4
Size: 1188 Color: 4
Size: 232 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 1
Size: 1427 Color: 4
Size: 284 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5587 Color: 0
Size: 2165 Color: 0
Size: 432 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4124 Color: 3
Size: 3388 Color: 1
Size: 672 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 2
Size: 924 Color: 3
Size: 184 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 2
Size: 2356 Color: 1
Size: 464 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 1
Size: 1380 Color: 1
Size: 272 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5989 Color: 4
Size: 1831 Color: 1
Size: 364 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 4
Size: 2146 Color: 1
Size: 428 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6158 Color: 3
Size: 1690 Color: 2
Size: 336 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 1
Size: 684 Color: 0
Size: 136 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 7230 Color: 1
Size: 798 Color: 1
Size: 156 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4098 Color: 0
Size: 3406 Color: 0
Size: 680 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 1
Size: 744 Color: 0
Size: 136 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 1
Size: 2492 Color: 0
Size: 496 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6646 Color: 1
Size: 1282 Color: 3
Size: 256 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 6366 Color: 0
Size: 1518 Color: 0
Size: 300 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4654 Color: 1
Size: 2942 Color: 3
Size: 588 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 4475 Color: 4
Size: 3091 Color: 3
Size: 618 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 7282 Color: 1
Size: 754 Color: 3
Size: 148 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 4670 Color: 3
Size: 2930 Color: 4
Size: 584 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 1
Size: 1090 Color: 4
Size: 216 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 2
Size: 3484 Color: 3
Size: 600 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 0
Size: 1186 Color: 1
Size: 236 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 7309 Color: 2
Size: 817 Color: 0
Size: 58 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 4443 Color: 3
Size: 3119 Color: 3
Size: 622 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5134 Color: 2
Size: 2542 Color: 2
Size: 508 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 0
Size: 1898 Color: 4
Size: 376 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 4
Size: 836 Color: 4
Size: 160 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 3
Size: 2950 Color: 4
Size: 588 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 0
Size: 2719 Color: 3
Size: 542 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 1
Size: 2154 Color: 4
Size: 428 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 3
Size: 1123 Color: 1
Size: 224 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6607 Color: 3
Size: 1315 Color: 0
Size: 262 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 3
Size: 1174 Color: 0
Size: 232 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 0
Size: 1141 Color: 3
Size: 228 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4939 Color: 0
Size: 2705 Color: 4
Size: 540 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 2
Size: 881 Color: 1
Size: 176 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6635 Color: 0
Size: 1291 Color: 3
Size: 258 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 2
Size: 965 Color: 1
Size: 192 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 6243 Color: 4
Size: 1739 Color: 4
Size: 202 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5977 Color: 4
Size: 1841 Color: 3
Size: 366 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 5981 Color: 1
Size: 1837 Color: 2
Size: 366 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 7203 Color: 4
Size: 819 Color: 0
Size: 162 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 4427 Color: 1
Size: 3131 Color: 3
Size: 626 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 0
Size: 891 Color: 4
Size: 178 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7277 Color: 4
Size: 757 Color: 3
Size: 150 Color: 1

Total size: 1080288
Total free space: 0

