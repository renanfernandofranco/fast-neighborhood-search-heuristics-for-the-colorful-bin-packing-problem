Capicity Bin: 19296
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 440
Size: 6124 Color: 371
Size: 364 Color: 41

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 14769 Color: 479
Size: 3773 Color: 324
Size: 754 Color: 134

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 483
Size: 3824 Color: 328
Size: 396 Color: 57

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 15108 Color: 484
Size: 3796 Color: 325
Size: 392 Color: 53

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 485
Size: 2736 Color: 287
Size: 1440 Color: 193

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 15138 Color: 487
Size: 3400 Color: 314
Size: 758 Color: 135

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 15162 Color: 488
Size: 2890 Color: 292
Size: 1244 Color: 182

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 15208 Color: 489
Size: 3996 Color: 333
Size: 92 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15344 Color: 493
Size: 2896 Color: 293
Size: 1056 Color: 168

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15540 Color: 497
Size: 3236 Color: 308
Size: 520 Color: 99

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15578 Color: 499
Size: 1936 Color: 233
Size: 1782 Color: 222

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15656 Color: 503
Size: 3056 Color: 301
Size: 584 Color: 110

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15661 Color: 504
Size: 3031 Color: 299
Size: 604 Color: 113

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15667 Color: 505
Size: 3025 Color: 298
Size: 604 Color: 112

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15688 Color: 506
Size: 3132 Color: 306
Size: 476 Color: 87

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15989 Color: 516
Size: 1667 Color: 209
Size: 1640 Color: 207

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15997 Color: 517
Size: 2751 Color: 288
Size: 548 Color: 104

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16016 Color: 518
Size: 1680 Color: 211
Size: 1600 Color: 196

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16100 Color: 520
Size: 2904 Color: 295
Size: 292 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16166 Color: 522
Size: 2720 Color: 286
Size: 410 Color: 62

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16204 Color: 526
Size: 1844 Color: 228
Size: 1248 Color: 183

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16420 Color: 533
Size: 1808 Color: 224
Size: 1068 Color: 169

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16434 Color: 534
Size: 1758 Color: 218
Size: 1104 Color: 174

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16456 Color: 535
Size: 1600 Color: 197
Size: 1240 Color: 181

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16485 Color: 537
Size: 1803 Color: 223
Size: 1008 Color: 163

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16488 Color: 538
Size: 2352 Color: 266
Size: 456 Color: 79

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 539
Size: 1432 Color: 192
Size: 1368 Color: 189

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16500 Color: 540
Size: 2332 Color: 263
Size: 464 Color: 80

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16648 Color: 544
Size: 1912 Color: 231
Size: 736 Color: 132

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16668 Color: 546
Size: 1604 Color: 201
Size: 1024 Color: 164

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16724 Color: 552
Size: 1612 Color: 204
Size: 960 Color: 159

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16756 Color: 554
Size: 1868 Color: 229
Size: 672 Color: 122

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16816 Color: 557
Size: 2084 Color: 245
Size: 396 Color: 55

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16856 Color: 558
Size: 1748 Color: 217
Size: 692 Color: 127

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16866 Color: 559
Size: 2026 Color: 243
Size: 404 Color: 60

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16900 Color: 561
Size: 1972 Color: 237
Size: 424 Color: 67

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 569
Size: 1606 Color: 203
Size: 688 Color: 126

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17007 Color: 570
Size: 1673 Color: 210
Size: 616 Color: 115

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17008 Color: 571
Size: 1936 Color: 234
Size: 352 Color: 34

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 572
Size: 1660 Color: 208
Size: 620 Color: 116

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17156 Color: 581
Size: 1708 Color: 212
Size: 432 Color: 72

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17179 Color: 584
Size: 1765 Color: 220
Size: 352 Color: 33

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17231 Color: 588
Size: 1721 Color: 216
Size: 344 Color: 29

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17240 Color: 589
Size: 1832 Color: 226
Size: 224 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17242 Color: 590
Size: 1376 Color: 190
Size: 678 Color: 124

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17296 Color: 594
Size: 1248 Color: 184
Size: 752 Color: 133

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17334 Color: 597
Size: 1638 Color: 206
Size: 324 Color: 19

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17336 Color: 598
Size: 1448 Color: 194
Size: 512 Color: 98

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17364 Color: 600
Size: 1600 Color: 198
Size: 332 Color: 22

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 14925 Color: 481
Size: 3862 Color: 330
Size: 508 Color: 93

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 15223 Color: 490
Size: 3848 Color: 329
Size: 224 Color: 6

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 15554 Color: 498
Size: 1914 Color: 232
Size: 1827 Color: 225

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 15625 Color: 501
Size: 3416 Color: 315
Size: 254 Color: 7

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 16279 Color: 529
Size: 2456 Color: 274
Size: 560 Color: 107

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 16311 Color: 530
Size: 2600 Color: 280
Size: 384 Color: 51

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 16538 Color: 542
Size: 2757 Color: 289

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 16970 Color: 567
Size: 2325 Color: 262

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 17084 Color: 575
Size: 2211 Color: 257

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 17105 Color: 577
Size: 2190 Color: 254

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 17136 Color: 579
Size: 2159 Color: 252

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 17340 Color: 599
Size: 1955 Color: 235

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 444
Size: 6390 Color: 377

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 448
Size: 5762 Color: 369
Size: 352 Color: 36

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 14169 Color: 465
Size: 4805 Color: 351
Size: 320 Color: 17

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 469
Size: 4680 Color: 349
Size: 312 Color: 13

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 15534 Color: 496
Size: 3760 Color: 323

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 15965 Color: 514
Size: 3329 Color: 311

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 15982 Color: 515
Size: 3312 Color: 310

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 16172 Color: 524
Size: 3122 Color: 305

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 16370 Color: 532
Size: 2924 Color: 297

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 16678 Color: 549
Size: 2616 Color: 283

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 17060 Color: 574
Size: 2234 Color: 259

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 17098 Color: 576
Size: 2196 Color: 255

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 17112 Color: 578
Size: 2182 Color: 253

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 17190 Color: 586
Size: 2104 Color: 248

Bin 76: 3 of cap free
Amount of items: 5
Items: 
Size: 12713 Color: 437
Size: 2900 Color: 294
Size: 2448 Color: 273
Size: 864 Color: 150
Size: 368 Color: 42

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 15898 Color: 512
Size: 3395 Color: 313

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 16245 Color: 528
Size: 3048 Color: 300

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 16917 Color: 564
Size: 2376 Color: 269

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 17163 Color: 583
Size: 2130 Color: 250

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 17289 Color: 593
Size: 2004 Color: 241

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 16906 Color: 562
Size: 2386 Color: 270

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 17252 Color: 591
Size: 2040 Color: 244

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 17272 Color: 592
Size: 2020 Color: 242

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 16776 Color: 555
Size: 2515 Color: 276

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 16948 Color: 566
Size: 2343 Color: 265

Bin 87: 5 of cap free
Amount of items: 2
Items: 
Size: 17297 Color: 595
Size: 1994 Color: 240

Bin 88: 5 of cap free
Amount of items: 2
Items: 
Size: 17308 Color: 596
Size: 1983 Color: 238

Bin 89: 6 of cap free
Amount of items: 2
Items: 
Size: 16184 Color: 525
Size: 3106 Color: 304

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 15062 Color: 482
Size: 4227 Color: 337

Bin 91: 7 of cap free
Amount of items: 2
Items: 
Size: 15488 Color: 495
Size: 3801 Color: 326

Bin 92: 7 of cap free
Amount of items: 2
Items: 
Size: 15605 Color: 500
Size: 3684 Color: 322

Bin 93: 7 of cap free
Amount of items: 2
Items: 
Size: 17204 Color: 587
Size: 2085 Color: 246

Bin 94: 8 of cap free
Amount of items: 3
Items: 
Size: 10384 Color: 418
Size: 8504 Color: 400
Size: 400 Color: 59

Bin 95: 8 of cap free
Amount of items: 2
Items: 
Size: 16048 Color: 519
Size: 3240 Color: 309

Bin 96: 8 of cap free
Amount of items: 2
Items: 
Size: 16208 Color: 527
Size: 3080 Color: 302

Bin 97: 8 of cap free
Amount of items: 2
Items: 
Size: 16474 Color: 536
Size: 2814 Color: 291

Bin 98: 9 of cap free
Amount of items: 6
Items: 
Size: 9655 Color: 409
Size: 2442 Color: 272
Size: 2339 Color: 264
Size: 2216 Color: 258
Size: 2203 Color: 256
Size: 432 Color: 71

Bin 99: 9 of cap free
Amount of items: 3
Items: 
Size: 13531 Color: 453
Size: 5416 Color: 363
Size: 340 Color: 27

Bin 100: 9 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 464
Size: 4808 Color: 352
Size: 320 Color: 18

Bin 101: 9 of cap free
Amount of items: 2
Items: 
Size: 15820 Color: 510
Size: 3467 Color: 318

Bin 102: 9 of cap free
Amount of items: 2
Items: 
Size: 16707 Color: 551
Size: 2580 Color: 279

Bin 103: 9 of cap free
Amount of items: 2
Items: 
Size: 17139 Color: 580
Size: 2148 Color: 251

Bin 104: 10 of cap free
Amount of items: 2
Items: 
Size: 15124 Color: 486
Size: 4162 Color: 335

Bin 105: 10 of cap free
Amount of items: 2
Items: 
Size: 15840 Color: 511
Size: 3446 Color: 316

Bin 106: 10 of cap free
Amount of items: 2
Items: 
Size: 16932 Color: 565
Size: 2354 Color: 267

Bin 107: 10 of cap free
Amount of items: 2
Items: 
Size: 17162 Color: 582
Size: 2124 Color: 249

Bin 108: 11 of cap free
Amount of items: 2
Items: 
Size: 16675 Color: 548
Size: 2610 Color: 282

Bin 109: 11 of cap free
Amount of items: 2
Items: 
Size: 16742 Color: 553
Size: 2543 Color: 277

Bin 110: 12 of cap free
Amount of items: 2
Items: 
Size: 11760 Color: 428
Size: 7524 Color: 388

Bin 111: 12 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 455
Size: 5596 Color: 367

Bin 112: 12 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 460
Size: 5348 Color: 360

Bin 113: 12 of cap free
Amount of items: 2
Items: 
Size: 15420 Color: 494
Size: 3864 Color: 331

Bin 114: 12 of cap free
Amount of items: 2
Items: 
Size: 16507 Color: 541
Size: 2777 Color: 290

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 16616 Color: 543
Size: 2668 Color: 285

Bin 116: 12 of cap free
Amount of items: 2
Items: 
Size: 16795 Color: 556
Size: 2489 Color: 275

Bin 117: 12 of cap free
Amount of items: 2
Items: 
Size: 17188 Color: 585
Size: 2096 Color: 247

Bin 118: 13 of cap free
Amount of items: 11
Items: 
Size: 9649 Color: 405
Size: 1312 Color: 187
Size: 1312 Color: 186
Size: 1250 Color: 185
Size: 1224 Color: 180
Size: 1184 Color: 179
Size: 1176 Color: 178
Size: 800 Color: 142
Size: 464 Color: 82
Size: 464 Color: 81
Size: 448 Color: 78

Bin 119: 13 of cap free
Amount of items: 2
Items: 
Size: 16653 Color: 545
Size: 2630 Color: 284

Bin 120: 13 of cap free
Amount of items: 2
Items: 
Size: 17035 Color: 573
Size: 2248 Color: 260

Bin 121: 14 of cap free
Amount of items: 4
Items: 
Size: 13242 Color: 449
Size: 5336 Color: 359
Size: 352 Color: 35
Size: 352 Color: 32

Bin 122: 14 of cap free
Amount of items: 2
Items: 
Size: 15752 Color: 507
Size: 3530 Color: 320

Bin 123: 14 of cap free
Amount of items: 2
Items: 
Size: 15816 Color: 509
Size: 3466 Color: 317

Bin 124: 15 of cap free
Amount of items: 4
Items: 
Size: 11344 Color: 426
Size: 7169 Color: 384
Size: 384 Color: 49
Size: 384 Color: 48

Bin 125: 16 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 458
Size: 5168 Color: 357
Size: 336 Color: 26

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 16142 Color: 521
Size: 3138 Color: 307

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 531
Size: 2912 Color: 296

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 16876 Color: 560
Size: 2404 Color: 271

Bin 129: 17 of cap free
Amount of items: 2
Items: 
Size: 14448 Color: 470
Size: 4831 Color: 353

Bin 130: 17 of cap free
Amount of items: 2
Items: 
Size: 16703 Color: 550
Size: 2576 Color: 278

Bin 131: 17 of cap free
Amount of items: 2
Items: 
Size: 16911 Color: 563
Size: 2368 Color: 268

Bin 132: 18 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 568
Size: 2302 Color: 261

Bin 133: 19 of cap free
Amount of items: 3
Items: 
Size: 13477 Color: 451
Size: 5456 Color: 364
Size: 344 Color: 30

Bin 134: 21 of cap free
Amount of items: 2
Items: 
Size: 15632 Color: 502
Size: 3643 Color: 321

Bin 135: 22 of cap free
Amount of items: 2
Items: 
Size: 16670 Color: 547
Size: 2604 Color: 281

Bin 136: 24 of cap free
Amount of items: 2
Items: 
Size: 15224 Color: 491
Size: 4048 Color: 334

Bin 137: 24 of cap free
Amount of items: 2
Items: 
Size: 15788 Color: 508
Size: 3484 Color: 319

Bin 138: 26 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 435
Size: 6888 Color: 382

Bin 139: 26 of cap free
Amount of items: 2
Items: 
Size: 16168 Color: 523
Size: 3102 Color: 303

Bin 140: 27 of cap free
Amount of items: 2
Items: 
Size: 14773 Color: 480
Size: 4496 Color: 345

Bin 141: 28 of cap free
Amount of items: 27
Items: 
Size: 912 Color: 154
Size: 896 Color: 153
Size: 888 Color: 152
Size: 880 Color: 151
Size: 864 Color: 149
Size: 864 Color: 148
Size: 856 Color: 147
Size: 854 Color: 146
Size: 848 Color: 145
Size: 844 Color: 144
Size: 832 Color: 143
Size: 792 Color: 141
Size: 788 Color: 140
Size: 768 Color: 139
Size: 768 Color: 138
Size: 768 Color: 137
Size: 768 Color: 136
Size: 524 Color: 101
Size: 520 Color: 100
Size: 512 Color: 97
Size: 512 Color: 96
Size: 512 Color: 95
Size: 512 Color: 94
Size: 504 Color: 92
Size: 502 Color: 91
Size: 496 Color: 90
Size: 484 Color: 89

Bin 142: 28 of cap free
Amount of items: 5
Items: 
Size: 9674 Color: 416
Size: 8034 Color: 392
Size: 728 Color: 131
Size: 416 Color: 65
Size: 416 Color: 64

Bin 143: 28 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 456
Size: 3812 Color: 327
Size: 1720 Color: 215

Bin 144: 31 of cap free
Amount of items: 2
Items: 
Size: 15303 Color: 492
Size: 3962 Color: 332

Bin 145: 32 of cap free
Amount of items: 9
Items: 
Size: 9650 Color: 406
Size: 1606 Color: 202
Size: 1604 Color: 200
Size: 1604 Color: 199
Size: 1600 Color: 195
Size: 1344 Color: 188
Size: 976 Color: 162
Size: 440 Color: 77
Size: 440 Color: 76

Bin 146: 33 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 433
Size: 6947 Color: 383
Size: 368 Color: 44

Bin 147: 35 of cap free
Amount of items: 2
Items: 
Size: 15922 Color: 513
Size: 3339 Color: 312

Bin 148: 38 of cap free
Amount of items: 5
Items: 
Size: 9672 Color: 415
Size: 8026 Color: 391
Size: 720 Color: 130
Size: 424 Color: 68
Size: 416 Color: 66

Bin 149: 38 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 478
Size: 4454 Color: 344
Size: 56 Color: 0

Bin 150: 41 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 457
Size: 5487 Color: 366

Bin 151: 46 of cap free
Amount of items: 5
Items: 
Size: 9666 Color: 414
Size: 8016 Color: 390
Size: 714 Color: 129
Size: 430 Color: 70
Size: 424 Color: 69

Bin 152: 52 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 463
Size: 4892 Color: 355
Size: 328 Color: 20

Bin 153: 53 of cap free
Amount of items: 3
Items: 
Size: 14225 Color: 468
Size: 4698 Color: 350
Size: 320 Color: 14

Bin 154: 56 of cap free
Amount of items: 3
Items: 
Size: 14736 Color: 476
Size: 4408 Color: 342
Size: 96 Color: 3

Bin 155: 60 of cap free
Amount of items: 3
Items: 
Size: 12944 Color: 445
Size: 5928 Color: 370
Size: 364 Color: 40

Bin 156: 65 of cap free
Amount of items: 3
Items: 
Size: 14666 Color: 474
Size: 4281 Color: 340
Size: 284 Color: 8

Bin 157: 66 of cap free
Amount of items: 2
Items: 
Size: 10961 Color: 423
Size: 8269 Color: 398

Bin 158: 68 of cap free
Amount of items: 2
Items: 
Size: 12588 Color: 436
Size: 6640 Color: 379

Bin 159: 69 of cap free
Amount of items: 3
Items: 
Size: 13499 Color: 452
Size: 5384 Color: 362
Size: 344 Color: 28

Bin 160: 71 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 473
Size: 4273 Color: 339
Size: 288 Color: 9

Bin 161: 71 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 475
Size: 4385 Color: 341
Size: 160 Color: 4

Bin 162: 71 of cap free
Amount of items: 3
Items: 
Size: 14737 Color: 477
Size: 4424 Color: 343
Size: 64 Color: 1

Bin 163: 88 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 447
Size: 5740 Color: 368
Size: 352 Color: 37

Bin 164: 94 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 459
Size: 5046 Color: 356
Size: 336 Color: 25

Bin 165: 110 of cap free
Amount of items: 4
Items: 
Size: 13954 Color: 461
Size: 4564 Color: 346
Size: 334 Color: 24
Size: 334 Color: 23

Bin 166: 120 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 434
Size: 6624 Color: 378
Size: 368 Color: 43

Bin 167: 121 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 462
Size: 4851 Color: 354
Size: 332 Color: 21

Bin 168: 124 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 425
Size: 7600 Color: 389
Size: 384 Color: 50

Bin 169: 131 of cap free
Amount of items: 3
Items: 
Size: 14197 Color: 467
Size: 4648 Color: 348
Size: 320 Color: 15

Bin 170: 132 of cap free
Amount of items: 2
Items: 
Size: 9660 Color: 413
Size: 9504 Color: 404

Bin 171: 142 of cap free
Amount of items: 2
Items: 
Size: 12866 Color: 443
Size: 6288 Color: 376

Bin 172: 152 of cap free
Amount of items: 7
Items: 
Size: 9652 Color: 408
Size: 1989 Color: 239
Size: 1964 Color: 236
Size: 1885 Color: 230
Size: 1834 Color: 227
Size: 1388 Color: 191
Size: 432 Color: 73

Bin 173: 158 of cap free
Amount of items: 3
Items: 
Size: 13428 Color: 450
Size: 5362 Color: 361
Size: 348 Color: 31

Bin 174: 160 of cap free
Amount of items: 8
Items: 
Size: 9651 Color: 407
Size: 1779 Color: 221
Size: 1764 Color: 219
Size: 1720 Color: 214
Size: 1714 Color: 213
Size: 1636 Color: 205
Size: 436 Color: 75
Size: 436 Color: 74

Bin 175: 167 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 422
Size: 8042 Color: 396
Size: 392 Color: 52

Bin 176: 169 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 454
Size: 5465 Color: 365

Bin 177: 172 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 466
Size: 4624 Color: 347
Size: 320 Color: 16

Bin 178: 180 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 472
Size: 4268 Color: 338
Size: 302 Color: 11

Bin 179: 188 of cap free
Amount of items: 4
Items: 
Size: 13104 Color: 446
Size: 5296 Color: 358
Size: 354 Color: 39
Size: 354 Color: 38

Bin 180: 190 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 442
Size: 6264 Color: 375

Bin 181: 192 of cap free
Amount of items: 3
Items: 
Size: 10668 Color: 421
Size: 8040 Color: 395
Size: 396 Color: 54

Bin 182: 205 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 441
Size: 6251 Color: 374

Bin 183: 213 of cap free
Amount of items: 2
Items: 
Size: 11795 Color: 430
Size: 7288 Color: 387

Bin 184: 220 of cap free
Amount of items: 2
Items: 
Size: 11032 Color: 424
Size: 8044 Color: 397

Bin 185: 258 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 429
Size: 7254 Color: 386

Bin 186: 263 of cap free
Amount of items: 3
Items: 
Size: 14508 Color: 471
Size: 4221 Color: 336
Size: 304 Color: 12

Bin 187: 267 of cap free
Amount of items: 3
Items: 
Size: 10594 Color: 420
Size: 8039 Color: 394
Size: 396 Color: 56

Bin 188: 274 of cap free
Amount of items: 2
Items: 
Size: 9659 Color: 412
Size: 9363 Color: 403

Bin 189: 287 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 439
Size: 6225 Color: 373

Bin 190: 293 of cap free
Amount of items: 3
Items: 
Size: 10568 Color: 419
Size: 8035 Color: 393
Size: 400 Color: 58

Bin 191: 314 of cap free
Amount of items: 2
Items: 
Size: 9658 Color: 411
Size: 9324 Color: 402

Bin 192: 324 of cap free
Amount of items: 3
Items: 
Size: 11834 Color: 432
Size: 6770 Color: 381
Size: 368 Color: 45

Bin 193: 329 of cap free
Amount of items: 3
Items: 
Size: 11827 Color: 431
Size: 6764 Color: 380
Size: 376 Color: 46

Bin 194: 334 of cap free
Amount of items: 3
Items: 
Size: 11350 Color: 427
Size: 7232 Color: 385
Size: 380 Color: 47

Bin 195: 335 of cap free
Amount of items: 2
Items: 
Size: 12739 Color: 438
Size: 6222 Color: 372

Bin 196: 390 of cap free
Amount of items: 21
Items: 
Size: 1152 Color: 177
Size: 1120 Color: 176
Size: 1112 Color: 175
Size: 1096 Color: 173
Size: 1092 Color: 172
Size: 1072 Color: 171
Size: 1072 Color: 170
Size: 1056 Color: 167
Size: 1056 Color: 166
Size: 1056 Color: 165
Size: 968 Color: 161
Size: 966 Color: 160
Size: 960 Color: 158
Size: 936 Color: 157
Size: 928 Color: 156
Size: 912 Color: 155
Size: 480 Color: 88
Size: 472 Color: 86
Size: 468 Color: 85
Size: 468 Color: 84
Size: 464 Color: 83

Bin 197: 408 of cap free
Amount of items: 4
Items: 
Size: 9680 Color: 417
Size: 8384 Color: 399
Size: 416 Color: 63
Size: 408 Color: 61

Bin 198: 414 of cap free
Amount of items: 2
Items: 
Size: 9656 Color: 410
Size: 9226 Color: 401

Bin 199: 9512 of cap free
Amount of items: 16
Items: 
Size: 704 Color: 128
Size: 688 Color: 125
Size: 672 Color: 123
Size: 664 Color: 121
Size: 640 Color: 120
Size: 640 Color: 119
Size: 624 Color: 118
Size: 624 Color: 117
Size: 608 Color: 114
Size: 592 Color: 111
Size: 576 Color: 109
Size: 576 Color: 108
Size: 554 Color: 106
Size: 550 Color: 105
Size: 544 Color: 103
Size: 528 Color: 102

Total size: 3820608
Total free space: 19296

