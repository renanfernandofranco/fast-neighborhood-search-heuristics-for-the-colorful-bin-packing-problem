Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 269 Color: 19
Size: 269 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 12
Size: 325 Color: 6
Size: 257 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 289 Color: 19
Size: 268 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 5
Size: 312 Color: 2
Size: 264 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 19
Size: 266 Color: 3
Size: 251 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 360 Color: 7
Size: 268 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 6
Size: 326 Color: 19
Size: 263 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 6
Size: 267 Color: 9
Size: 250 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 358 Color: 17
Size: 267 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 281 Color: 13
Size: 259 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 8
Size: 273 Color: 14
Size: 252 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9
Size: 289 Color: 6
Size: 257 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 284 Color: 7
Size: 280 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 322 Color: 1
Size: 321 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 8
Size: 351 Color: 14
Size: 251 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 6
Size: 336 Color: 6
Size: 285 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 13
Size: 305 Color: 16
Size: 284 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 18
Size: 346 Color: 5
Size: 290 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 16
Size: 288 Color: 10
Size: 251 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 18
Size: 315 Color: 16
Size: 315 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 5
Size: 301 Color: 12
Size: 270 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 8
Size: 301 Color: 16
Size: 297 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 6
Size: 309 Color: 6
Size: 261 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 316 Color: 3
Size: 276 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 10
Size: 304 Color: 7
Size: 269 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 18
Size: 280 Color: 6
Size: 271 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 8
Size: 263 Color: 0
Size: 261 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 271 Color: 5
Size: 258 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 9
Size: 334 Color: 16
Size: 284 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 11
Size: 268 Color: 5
Size: 255 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 289 Color: 19
Size: 264 Color: 14
Size: 447 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 13
Size: 309 Color: 17
Size: 285 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 16
Size: 368 Color: 7
Size: 254 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 13
Size: 268 Color: 4
Size: 251 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 16
Size: 317 Color: 6
Size: 288 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 15
Size: 255 Color: 4
Size: 251 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 346 Color: 13
Size: 257 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 296 Color: 10
Size: 286 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 354 Color: 13
Size: 277 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 15
Size: 290 Color: 1
Size: 250 Color: 13

Total size: 40000
Total free space: 0

