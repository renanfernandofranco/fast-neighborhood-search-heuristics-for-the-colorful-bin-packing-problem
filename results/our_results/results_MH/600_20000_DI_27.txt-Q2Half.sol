Capicity Bin: 19712
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 12272 Color: 1
Size: 6224 Color: 1
Size: 1216 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 13682 Color: 1
Size: 4450 Color: 1
Size: 888 Color: 0
Size: 692 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13887 Color: 1
Size: 4855 Color: 1
Size: 970 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 1
Size: 7100 Color: 1
Size: 1416 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9864 Color: 1
Size: 8216 Color: 1
Size: 1632 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 13163 Color: 1
Size: 4821 Color: 1
Size: 1344 Color: 0
Size: 384 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11056 Color: 1
Size: 7216 Color: 1
Size: 1440 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9860 Color: 1
Size: 8212 Color: 1
Size: 1640 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10654 Color: 1
Size: 7550 Color: 1
Size: 1508 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 9857 Color: 1
Size: 8213 Color: 1
Size: 1642 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9861 Color: 1
Size: 8211 Color: 1
Size: 1640 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9872 Color: 1
Size: 8208 Color: 1
Size: 1632 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 1
Size: 8204 Color: 1
Size: 1632 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 1
Size: 8200 Color: 1
Size: 1632 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 1
Size: 8538 Color: 1
Size: 512 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11208 Color: 1
Size: 7984 Color: 1
Size: 520 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 1
Size: 7084 Color: 1
Size: 1416 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 1
Size: 7080 Color: 1
Size: 1408 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 1
Size: 7076 Color: 1
Size: 1408 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11922 Color: 1
Size: 6546 Color: 1
Size: 1244 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12205 Color: 1
Size: 6257 Color: 1
Size: 1250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 1
Size: 6720 Color: 1
Size: 664 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 6152 Color: 1
Size: 1216 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 1
Size: 6140 Color: 1
Size: 1224 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13072 Color: 1
Size: 6168 Color: 1
Size: 472 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 1
Size: 5503 Color: 1
Size: 1100 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13179 Color: 1
Size: 5445 Color: 1
Size: 1088 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 1
Size: 5026 Color: 1
Size: 1430 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 1
Size: 5392 Color: 1
Size: 1056 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13690 Color: 1
Size: 5022 Color: 1
Size: 1000 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13872 Color: 1
Size: 5444 Color: 1
Size: 396 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14384 Color: 1
Size: 4080 Color: 1
Size: 1248 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14386 Color: 1
Size: 4442 Color: 1
Size: 884 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 1
Size: 4412 Color: 1
Size: 880 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 1
Size: 4362 Color: 1
Size: 868 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 1
Size: 4510 Color: 1
Size: 656 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 1
Size: 4248 Color: 1
Size: 848 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 1
Size: 4696 Color: 1
Size: 336 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 1
Size: 3332 Color: 1
Size: 1408 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14988 Color: 1
Size: 3940 Color: 1
Size: 784 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 15031 Color: 1
Size: 3901 Color: 1
Size: 780 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 15037 Color: 1
Size: 3897 Color: 1
Size: 778 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 15085 Color: 1
Size: 3857 Color: 1
Size: 770 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 15086 Color: 1
Size: 3858 Color: 1
Size: 768 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 15101 Color: 1
Size: 3843 Color: 1
Size: 768 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 15176 Color: 1
Size: 4200 Color: 1
Size: 336 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 15240 Color: 1
Size: 3368 Color: 1
Size: 1104 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 15280 Color: 1
Size: 3696 Color: 1
Size: 736 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 15378 Color: 1
Size: 3438 Color: 1
Size: 896 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 15396 Color: 1
Size: 3604 Color: 1
Size: 712 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 15542 Color: 1
Size: 3418 Color: 1
Size: 752 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 15568 Color: 1
Size: 3736 Color: 1
Size: 408 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 15606 Color: 1
Size: 3422 Color: 1
Size: 684 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 15614 Color: 1
Size: 3238 Color: 1
Size: 860 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 15640 Color: 1
Size: 3108 Color: 1
Size: 964 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 15688 Color: 1
Size: 3304 Color: 1
Size: 720 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 15752 Color: 1
Size: 3470 Color: 1
Size: 490 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 15776 Color: 1
Size: 3070 Color: 1
Size: 866 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 15792 Color: 1
Size: 3280 Color: 1
Size: 640 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 15816 Color: 1
Size: 3480 Color: 1
Size: 416 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 15845 Color: 1
Size: 3223 Color: 1
Size: 644 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 15851 Color: 1
Size: 3219 Color: 1
Size: 642 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 15879 Color: 1
Size: 3195 Color: 1
Size: 638 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 15915 Color: 1
Size: 3165 Color: 1
Size: 632 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 15944 Color: 1
Size: 3400 Color: 1
Size: 368 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 15982 Color: 1
Size: 3096 Color: 1
Size: 634 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 16008 Color: 1
Size: 3020 Color: 1
Size: 684 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 16046 Color: 1
Size: 3058 Color: 1
Size: 608 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 16144 Color: 1
Size: 2992 Color: 1
Size: 576 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 16167 Color: 1
Size: 2457 Color: 1
Size: 1088 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 16192 Color: 1
Size: 2944 Color: 1
Size: 576 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 16328 Color: 1
Size: 2824 Color: 1
Size: 560 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 16350 Color: 1
Size: 2706 Color: 1
Size: 656 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 16377 Color: 1
Size: 2781 Color: 1
Size: 554 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 16392 Color: 1
Size: 2456 Color: 1
Size: 864 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 16417 Color: 1
Size: 2747 Color: 1
Size: 548 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 16466 Color: 1
Size: 2622 Color: 1
Size: 624 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 1
Size: 2600 Color: 1
Size: 616 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 16570 Color: 1
Size: 2504 Color: 1
Size: 638 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 16585 Color: 1
Size: 2607 Color: 1
Size: 520 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 16591 Color: 1
Size: 2601 Color: 1
Size: 520 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 16600 Color: 1
Size: 2720 Color: 1
Size: 392 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 16648 Color: 1
Size: 2704 Color: 1
Size: 360 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 16712 Color: 1
Size: 2432 Color: 0
Size: 568 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 16731 Color: 1
Size: 2441 Color: 1
Size: 540 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 16764 Color: 1
Size: 2460 Color: 1
Size: 488 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 16765 Color: 1
Size: 2403 Color: 1
Size: 544 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 1
Size: 2448 Color: 1
Size: 488 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 16784 Color: 1
Size: 2776 Color: 1
Size: 152 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 16786 Color: 1
Size: 2442 Color: 1
Size: 484 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 16793 Color: 1
Size: 2433 Color: 1
Size: 486 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 16829 Color: 1
Size: 2299 Color: 1
Size: 584 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 16948 Color: 1
Size: 2308 Color: 1
Size: 456 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 16966 Color: 1
Size: 2288 Color: 1
Size: 458 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 16968 Color: 1
Size: 2296 Color: 1
Size: 448 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 16976 Color: 1
Size: 2568 Color: 1
Size: 168 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 17027 Color: 1
Size: 2065 Color: 1
Size: 620 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 17042 Color: 1
Size: 2058 Color: 1
Size: 612 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 17067 Color: 1
Size: 2205 Color: 1
Size: 440 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 17092 Color: 1
Size: 2188 Color: 1
Size: 432 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 17106 Color: 1
Size: 2094 Color: 1
Size: 512 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 17109 Color: 1
Size: 2171 Color: 1
Size: 432 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 17176 Color: 1
Size: 2168 Color: 1
Size: 368 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 17201 Color: 1
Size: 2093 Color: 1
Size: 418 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 17220 Color: 1
Size: 2084 Color: 1
Size: 408 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 17222 Color: 1
Size: 2058 Color: 1
Size: 432 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 17246 Color: 1
Size: 1970 Color: 1
Size: 496 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 17249 Color: 1
Size: 2053 Color: 1
Size: 410 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 17265 Color: 1
Size: 2041 Color: 1
Size: 406 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 17273 Color: 1
Size: 2033 Color: 1
Size: 406 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 1
Size: 2008 Color: 1
Size: 400 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 17316 Color: 1
Size: 2004 Color: 1
Size: 392 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 17328 Color: 1
Size: 2000 Color: 1
Size: 384 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 17329 Color: 1
Size: 1935 Color: 1
Size: 448 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 17350 Color: 1
Size: 2226 Color: 1
Size: 136 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 17368 Color: 1
Size: 2290 Color: 1
Size: 54 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 17444 Color: 1
Size: 1892 Color: 1
Size: 376 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 17445 Color: 1
Size: 1891 Color: 1
Size: 376 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 17464 Color: 1
Size: 2072 Color: 1
Size: 176 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 17488 Color: 1
Size: 1880 Color: 1
Size: 344 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 17494 Color: 1
Size: 1850 Color: 1
Size: 368 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 17508 Color: 1
Size: 1748 Color: 1
Size: 456 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 17528 Color: 1
Size: 1832 Color: 1
Size: 352 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 17530 Color: 1
Size: 1822 Color: 1
Size: 360 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 17552 Color: 1
Size: 1716 Color: 1
Size: 444 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 17559 Color: 1
Size: 1795 Color: 1
Size: 358 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 17592 Color: 1
Size: 1768 Color: 1
Size: 352 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 17598 Color: 1
Size: 1762 Color: 1
Size: 352 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 1
Size: 1768 Color: 1
Size: 336 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 17611 Color: 1
Size: 1775 Color: 1
Size: 326 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 1
Size: 1776 Color: 1
Size: 320 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 17620 Color: 1
Size: 1960 Color: 1
Size: 132 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 17655 Color: 1
Size: 1715 Color: 1
Size: 342 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 17656 Color: 1
Size: 1184 Color: 1
Size: 872 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 17660 Color: 1
Size: 1808 Color: 1
Size: 244 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 17682 Color: 1
Size: 1672 Color: 1
Size: 358 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 17702 Color: 1
Size: 1678 Color: 1
Size: 332 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 17720 Color: 1
Size: 1720 Color: 1
Size: 272 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 17739 Color: 1
Size: 1645 Color: 1
Size: 328 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 11119 Color: 1
Size: 7760 Color: 1
Size: 832 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 11858 Color: 1
Size: 7161 Color: 1
Size: 692 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 1
Size: 7284 Color: 1
Size: 256 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 1
Size: 5499 Color: 1
Size: 352 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 13927 Color: 1
Size: 4528 Color: 1
Size: 1256 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 1
Size: 4363 Color: 1
Size: 1308 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 14288 Color: 1
Size: 4335 Color: 1
Size: 1088 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 14511 Color: 1
Size: 4880 Color: 1
Size: 320 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 14551 Color: 1
Size: 4808 Color: 1
Size: 352 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 14890 Color: 1
Size: 4301 Color: 1
Size: 520 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 15489 Color: 1
Size: 3614 Color: 1
Size: 608 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 15529 Color: 1
Size: 3110 Color: 1
Size: 1072 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 15875 Color: 1
Size: 3144 Color: 1
Size: 692 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 15899 Color: 1
Size: 2852 Color: 1
Size: 960 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 15988 Color: 1
Size: 2291 Color: 1
Size: 1432 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 15990 Color: 1
Size: 2921 Color: 1
Size: 800 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 16207 Color: 1
Size: 2802 Color: 1
Size: 702 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 16292 Color: 1
Size: 2329 Color: 1
Size: 1090 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 16468 Color: 1
Size: 3179 Color: 1
Size: 64 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 16826 Color: 1
Size: 2485 Color: 1
Size: 400 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 16963 Color: 1
Size: 2644 Color: 1
Size: 104 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 17081 Color: 1
Size: 2406 Color: 1
Size: 224 Color: 0

Bin 162: 1 of cap free
Amount of items: 3
Items: 
Size: 17240 Color: 1
Size: 2033 Color: 1
Size: 438 Color: 0

Bin 163: 1 of cap free
Amount of items: 3
Items: 
Size: 17277 Color: 1
Size: 1844 Color: 1
Size: 590 Color: 0

Bin 164: 1 of cap free
Amount of items: 3
Items: 
Size: 17313 Color: 1
Size: 2174 Color: 1
Size: 224 Color: 0

Bin 165: 1 of cap free
Amount of items: 3
Items: 
Size: 17321 Color: 1
Size: 1694 Color: 1
Size: 696 Color: 0

Bin 166: 1 of cap free
Amount of items: 3
Items: 
Size: 17505 Color: 1
Size: 1872 Color: 1
Size: 334 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 12854 Color: 1
Size: 5384 Color: 1
Size: 1472 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 1
Size: 5602 Color: 1
Size: 928 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 1
Size: 4332 Color: 1
Size: 1004 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 15590 Color: 1
Size: 3784 Color: 1
Size: 336 Color: 0

Bin 171: 3 of cap free
Amount of items: 3
Items: 
Size: 16548 Color: 1
Size: 2193 Color: 1
Size: 968 Color: 0

Bin 172: 3 of cap free
Amount of items: 3
Items: 
Size: 12990 Color: 1
Size: 6223 Color: 1
Size: 496 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 14477 Color: 1
Size: 4464 Color: 1
Size: 768 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 14892 Color: 1
Size: 3521 Color: 1
Size: 1296 Color: 0

Bin 175: 3 of cap free
Amount of items: 3
Items: 
Size: 15550 Color: 1
Size: 3487 Color: 1
Size: 672 Color: 0

Bin 176: 3 of cap free
Amount of items: 3
Items: 
Size: 16030 Color: 1
Size: 3199 Color: 1
Size: 480 Color: 0

Bin 177: 3 of cap free
Amount of items: 3
Items: 
Size: 16342 Color: 1
Size: 2955 Color: 1
Size: 412 Color: 0

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 12862 Color: 1
Size: 6494 Color: 1
Size: 352 Color: 0

Bin 179: 4 of cap free
Amount of items: 3
Items: 
Size: 13164 Color: 1
Size: 6416 Color: 1
Size: 128 Color: 0

Bin 180: 4 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 1
Size: 4884 Color: 1
Size: 736 Color: 0

Bin 181: 5 of cap free
Amount of items: 3
Items: 
Size: 11121 Color: 1
Size: 7370 Color: 1
Size: 1216 Color: 0

Bin 182: 5 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 1
Size: 7127 Color: 1
Size: 256 Color: 0

Bin 183: 5 of cap free
Amount of items: 4
Items: 
Size: 11161 Color: 1
Size: 6158 Color: 1
Size: 1248 Color: 0
Size: 1140 Color: 0

Bin 184: 5 of cap free
Amount of items: 5
Items: 
Size: 10160 Color: 1
Size: 6285 Color: 1
Size: 1508 Color: 0
Size: 1098 Color: 0
Size: 656 Color: 0

Bin 185: 8 of cap free
Amount of items: 3
Items: 
Size: 12048 Color: 1
Size: 7096 Color: 1
Size: 560 Color: 0

Bin 186: 12 of cap free
Amount of items: 2
Items: 
Size: 15716 Color: 1
Size: 3984 Color: 0

Bin 187: 15 of cap free
Amount of items: 3
Items: 
Size: 13115 Color: 1
Size: 5718 Color: 1
Size: 864 Color: 0

Bin 188: 18 of cap free
Amount of items: 3
Items: 
Size: 14960 Color: 1
Size: 3310 Color: 1
Size: 1424 Color: 0

Bin 189: 27 of cap free
Amount of items: 5
Items: 
Size: 8214 Color: 1
Size: 5459 Color: 1
Size: 4588 Color: 1
Size: 928 Color: 0
Size: 496 Color: 0

Bin 190: 31 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 1
Size: 6380 Color: 1
Size: 1056 Color: 0

Bin 191: 41 of cap free
Amount of items: 3
Items: 
Size: 10870 Color: 1
Size: 7161 Color: 1
Size: 1640 Color: 0

Bin 192: 86 of cap free
Amount of items: 3
Items: 
Size: 9760 Color: 1
Size: 8746 Color: 1
Size: 1120 Color: 0

Bin 193: 90 of cap free
Amount of items: 3
Items: 
Size: 9862 Color: 1
Size: 8640 Color: 1
Size: 1120 Color: 0

Bin 194: 144 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 1
Size: 7296 Color: 1
Size: 1568 Color: 0

Bin 195: 160 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 1
Size: 5552 Color: 1
Size: 680 Color: 0

Bin 196: 988 of cap free
Amount of items: 3
Items: 
Size: 10702 Color: 1
Size: 7542 Color: 1
Size: 480 Color: 0

Bin 197: 2234 of cap free
Amount of items: 1
Items: 
Size: 17478 Color: 1

Bin 198: 4038 of cap free
Amount of items: 3
Items: 
Size: 9858 Color: 1
Size: 5336 Color: 1
Size: 480 Color: 0

Bin 199: 11732 of cap free
Amount of items: 3
Items: 
Size: 4022 Color: 1
Size: 3478 Color: 1
Size: 480 Color: 0

Total size: 3902976
Total free space: 19712

