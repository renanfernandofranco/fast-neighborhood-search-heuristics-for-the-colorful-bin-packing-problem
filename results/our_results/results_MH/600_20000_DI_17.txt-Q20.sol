Capicity Bin: 15872
Lower Bound: 198

Bins used: 198
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 7888 Color: 4
Size: 5392 Color: 6
Size: 960 Color: 18
Size: 864 Color: 7
Size: 768 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 19
Size: 1647 Color: 15
Size: 328 Color: 5

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 7944 Color: 14
Size: 6680 Color: 2
Size: 1024 Color: 17
Size: 144 Color: 16
Size: 80 Color: 8

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 11356 Color: 4
Size: 3764 Color: 13
Size: 288 Color: 17
Size: 272 Color: 14
Size: 192 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 2
Size: 2164 Color: 11
Size: 432 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 4
Size: 2184 Color: 11
Size: 432 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13698 Color: 0
Size: 1814 Color: 18
Size: 360 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 13
Size: 2116 Color: 14
Size: 144 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 18
Size: 2276 Color: 8
Size: 80 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 15
Size: 1656 Color: 18
Size: 320 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9924 Color: 12
Size: 4964 Color: 13
Size: 984 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 14
Size: 5160 Color: 15
Size: 752 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11623 Color: 2
Size: 3541 Color: 14
Size: 708 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9605 Color: 1
Size: 5223 Color: 0
Size: 1044 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14126 Color: 0
Size: 1458 Color: 9
Size: 288 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 8
Size: 4168 Color: 15
Size: 832 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9026 Color: 5
Size: 5706 Color: 4
Size: 1140 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 1
Size: 2404 Color: 1
Size: 472 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10700 Color: 17
Size: 4316 Color: 3
Size: 856 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14075 Color: 12
Size: 1499 Color: 3
Size: 298 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 10994 Color: 17
Size: 4066 Color: 15
Size: 812 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 2
Size: 2198 Color: 2
Size: 436 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14079 Color: 17
Size: 1495 Color: 3
Size: 298 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14069 Color: 16
Size: 1503 Color: 1
Size: 300 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 12
Size: 1764 Color: 19
Size: 344 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14154 Color: 4
Size: 1434 Color: 18
Size: 284 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 9938 Color: 13
Size: 4946 Color: 6
Size: 988 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 12
Size: 3048 Color: 17
Size: 592 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 17
Size: 3372 Color: 0
Size: 672 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 3
Size: 1945 Color: 4
Size: 388 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 9028 Color: 11
Size: 5708 Color: 13
Size: 1136 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 2
Size: 1429 Color: 10
Size: 284 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13565 Color: 5
Size: 1923 Color: 0
Size: 384 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 8
Size: 1348 Color: 18
Size: 264 Color: 5

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 9010 Color: 18
Size: 5722 Color: 14
Size: 1140 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 2
Size: 2692 Color: 18
Size: 536 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12699 Color: 0
Size: 2761 Color: 13
Size: 412 Color: 7

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 16
Size: 3768 Color: 17
Size: 736 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11938 Color: 0
Size: 3282 Color: 10
Size: 652 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11677 Color: 11
Size: 3799 Color: 15
Size: 396 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 19
Size: 1394 Color: 1
Size: 276 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 9688 Color: 2
Size: 4936 Color: 10
Size: 1248 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11868 Color: 13
Size: 3532 Color: 6
Size: 472 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 6
Size: 1636 Color: 8
Size: 320 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 1
Size: 2648 Color: 17
Size: 512 Color: 7

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11558 Color: 16
Size: 3850 Color: 17
Size: 464 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 18
Size: 2104 Color: 6
Size: 416 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14218 Color: 12
Size: 1382 Color: 17
Size: 272 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 10040 Color: 8
Size: 4872 Color: 3
Size: 960 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 6
Size: 4056 Color: 18
Size: 800 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 3
Size: 2334 Color: 2
Size: 464 Color: 8

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 11919 Color: 5
Size: 3295 Color: 10
Size: 658 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 10335 Color: 11
Size: 4615 Color: 9
Size: 922 Color: 19

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 4
Size: 2238 Color: 3
Size: 444 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12315 Color: 6
Size: 2965 Color: 19
Size: 592 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13332 Color: 6
Size: 2124 Color: 15
Size: 416 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13580 Color: 18
Size: 1916 Color: 5
Size: 376 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12495 Color: 6
Size: 2815 Color: 15
Size: 562 Color: 7

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7941 Color: 0
Size: 6611 Color: 11
Size: 1320 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 17
Size: 1444 Color: 2
Size: 288 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 4
Size: 1560 Color: 10
Size: 304 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 9972 Color: 2
Size: 4924 Color: 17
Size: 976 Color: 17

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 12819 Color: 0
Size: 2813 Color: 2
Size: 240 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 6
Size: 2570 Color: 9
Size: 512 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 13
Size: 2434 Color: 1
Size: 484 Color: 19

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 18
Size: 5125 Color: 13
Size: 1024 Color: 5

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 0
Size: 6392 Color: 15
Size: 1264 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 2
Size: 1412 Color: 9
Size: 280 Color: 12

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 9916 Color: 5
Size: 5348 Color: 7
Size: 608 Color: 15

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 12645 Color: 8
Size: 2691 Color: 17
Size: 536 Color: 18

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 5
Size: 1484 Color: 16
Size: 288 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 4
Size: 2468 Color: 17
Size: 488 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 16
Size: 2348 Color: 4
Size: 464 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 12092 Color: 15
Size: 3156 Color: 19
Size: 624 Color: 16

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13466 Color: 6
Size: 2006 Color: 18
Size: 400 Color: 14

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13825 Color: 5
Size: 1707 Color: 2
Size: 340 Color: 6

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 11
Size: 2164 Color: 9
Size: 424 Color: 19

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 14044 Color: 12
Size: 1524 Color: 9
Size: 304 Color: 19

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 12
Size: 2632 Color: 3
Size: 176 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 7
Size: 1747 Color: 3
Size: 348 Color: 17

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 15
Size: 2920 Color: 15
Size: 576 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 15
Size: 2488 Color: 0
Size: 480 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 14220 Color: 17
Size: 1492 Color: 8
Size: 160 Color: 16

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 11
Size: 3320 Color: 18
Size: 656 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13902 Color: 13
Size: 1642 Color: 6
Size: 328 Color: 11

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 9020 Color: 17
Size: 5716 Color: 8
Size: 1136 Color: 8

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 12559 Color: 15
Size: 2761 Color: 6
Size: 552 Color: 6

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7940 Color: 6
Size: 6612 Color: 8
Size: 1320 Color: 7

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 3
Size: 3244 Color: 10
Size: 648 Color: 14

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 12
Size: 2670 Color: 15
Size: 532 Color: 11

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 7942 Color: 11
Size: 6610 Color: 8
Size: 1320 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13375 Color: 4
Size: 2081 Color: 15
Size: 416 Color: 15

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 13426 Color: 11
Size: 2042 Color: 1
Size: 404 Color: 13

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 19
Size: 1960 Color: 10
Size: 384 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14282 Color: 6
Size: 1326 Color: 17
Size: 264 Color: 8

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 13
Size: 2130 Color: 17
Size: 424 Color: 15

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 11741 Color: 0
Size: 3443 Color: 15
Size: 688 Color: 18

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 6
Size: 1400 Color: 13
Size: 272 Color: 9

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 10248 Color: 3
Size: 4696 Color: 10
Size: 928 Color: 10

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 9
Size: 2952 Color: 14
Size: 576 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 7
Size: 1646 Color: 5
Size: 176 Color: 11

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13159 Color: 16
Size: 2261 Color: 1
Size: 452 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 9050 Color: 15
Size: 5686 Color: 9
Size: 1136 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 6
Size: 4808 Color: 10
Size: 960 Color: 17

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 11333 Color: 7
Size: 3783 Color: 13
Size: 756 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 9872 Color: 4
Size: 5008 Color: 17
Size: 992 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 10899 Color: 5
Size: 4145 Color: 14
Size: 828 Color: 14

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 3
Size: 1768 Color: 3
Size: 352 Color: 18

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 4
Size: 1565 Color: 7
Size: 312 Color: 5

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 15
Size: 5754 Color: 1
Size: 164 Color: 8

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 13758 Color: 9
Size: 1914 Color: 15
Size: 200 Color: 8

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 14
Size: 1336 Color: 14
Size: 256 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 2
Size: 1498 Color: 5
Size: 296 Color: 8

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 13085 Color: 6
Size: 2323 Color: 6
Size: 464 Color: 16

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 4
Size: 1404 Color: 16
Size: 272 Color: 13

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 14
Size: 2408 Color: 3
Size: 480 Color: 14

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 1
Size: 1860 Color: 15
Size: 368 Color: 11

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 12888 Color: 8
Size: 2488 Color: 2
Size: 496 Color: 19

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 13465 Color: 1
Size: 2007 Color: 13
Size: 400 Color: 7

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 3
Size: 3180 Color: 2
Size: 216 Color: 10

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 3
Size: 1928 Color: 4
Size: 368 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 11496 Color: 12
Size: 3656 Color: 7
Size: 720 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 11394 Color: 16
Size: 4022 Color: 0
Size: 456 Color: 10

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 11983 Color: 5
Size: 3241 Color: 17
Size: 648 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 2
Size: 4276 Color: 19
Size: 848 Color: 6

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 10399 Color: 12
Size: 4561 Color: 2
Size: 912 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 11269 Color: 19
Size: 3837 Color: 18
Size: 766 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 9544 Color: 19
Size: 5288 Color: 9
Size: 1040 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 9918 Color: 4
Size: 4962 Color: 4
Size: 992 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 13704 Color: 12
Size: 1816 Color: 12
Size: 352 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 12863 Color: 5
Size: 2509 Color: 4
Size: 500 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 12299 Color: 11
Size: 2979 Color: 10
Size: 594 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 0
Size: 2532 Color: 3
Size: 504 Color: 12

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 13659 Color: 1
Size: 1845 Color: 9
Size: 368 Color: 2

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 12851 Color: 6
Size: 2519 Color: 2
Size: 502 Color: 11

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 9956 Color: 7
Size: 4932 Color: 14
Size: 984 Color: 11

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 13998 Color: 9
Size: 1562 Color: 0
Size: 312 Color: 7

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 0
Size: 1980 Color: 12
Size: 392 Color: 6

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 11768 Color: 1
Size: 3432 Color: 10
Size: 672 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 11316 Color: 13
Size: 3804 Color: 2
Size: 752 Color: 8

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 11
Size: 3023 Color: 4
Size: 604 Color: 7

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 12811 Color: 9
Size: 2551 Color: 0
Size: 510 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 8024 Color: 5
Size: 7208 Color: 6
Size: 640 Color: 4

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 7937 Color: 19
Size: 6613 Color: 9
Size: 1322 Color: 15

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 19
Size: 3294 Color: 19
Size: 400 Color: 9

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 1
Size: 1708 Color: 0
Size: 336 Color: 4

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 12330 Color: 18
Size: 2954 Color: 9
Size: 588 Color: 15

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 13806 Color: 2
Size: 1722 Color: 3
Size: 344 Color: 11

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 4
Size: 3180 Color: 16
Size: 632 Color: 0

Bin 150: 0 of cap free
Amount of items: 4
Items: 
Size: 6976 Color: 6
Size: 5872 Color: 7
Size: 1936 Color: 2
Size: 1088 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 12
Size: 2806 Color: 15
Size: 560 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 11
Size: 5512 Color: 16
Size: 1088 Color: 19

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 13658 Color: 8
Size: 2102 Color: 5
Size: 112 Color: 8

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 8847 Color: 2
Size: 5855 Color: 6
Size: 1170 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 13996 Color: 12
Size: 1564 Color: 9
Size: 312 Color: 8

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 8911 Color: 6
Size: 5801 Color: 6
Size: 1160 Color: 9

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 13850 Color: 8
Size: 1686 Color: 6
Size: 336 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 3
Size: 1614 Color: 10
Size: 320 Color: 10

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 12724 Color: 12
Size: 2628 Color: 5
Size: 520 Color: 5

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 13745 Color: 5
Size: 1853 Color: 16
Size: 274 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 14276 Color: 11
Size: 1332 Color: 7
Size: 264 Color: 9

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 5
Size: 3112 Color: 7
Size: 144 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 3
Size: 2176 Color: 15
Size: 976 Color: 8

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 9032 Color: 0
Size: 5704 Color: 0
Size: 1136 Color: 7

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 5
Size: 2545 Color: 7
Size: 48 Color: 8

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 13249 Color: 18
Size: 2187 Color: 17
Size: 436 Color: 15

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 6
Size: 1464 Color: 0
Size: 288 Color: 12

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 19
Size: 6152 Color: 12
Size: 1216 Color: 19

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 13015 Color: 5
Size: 2381 Color: 9
Size: 476 Color: 10

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 13984 Color: 17
Size: 1600 Color: 16
Size: 288 Color: 5

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 15
Size: 3652 Color: 0
Size: 320 Color: 6

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 12508 Color: 19
Size: 2804 Color: 0
Size: 560 Color: 7

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 0
Size: 6904 Color: 1
Size: 512 Color: 11

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 13061 Color: 1
Size: 2619 Color: 11
Size: 192 Color: 10

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 16
Size: 1882 Color: 12
Size: 372 Color: 6

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 19
Size: 1644 Color: 10
Size: 328 Color: 11

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 13809 Color: 13
Size: 1721 Color: 18
Size: 342 Color: 14

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 13295 Color: 13
Size: 2149 Color: 0
Size: 428 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 12283 Color: 18
Size: 2991 Color: 10
Size: 598 Color: 8

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 7956 Color: 6
Size: 6604 Color: 15
Size: 1312 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 10514 Color: 11
Size: 4466 Color: 19
Size: 892 Color: 13

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 9659 Color: 17
Size: 5179 Color: 6
Size: 1034 Color: 2

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 9
Size: 3522 Color: 4
Size: 700 Color: 12

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 12779 Color: 10
Size: 2579 Color: 13
Size: 514 Color: 6

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 18
Size: 1459 Color: 5
Size: 290 Color: 2

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 13769 Color: 10
Size: 1753 Color: 17
Size: 350 Color: 18

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 3
Size: 1761 Color: 14
Size: 352 Color: 18

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 7938 Color: 11
Size: 6614 Color: 5
Size: 1320 Color: 19

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 6
Size: 1867 Color: 0
Size: 372 Color: 16

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 13993 Color: 15
Size: 1567 Color: 6
Size: 312 Color: 2

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 10
Size: 1625 Color: 9
Size: 324 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 14090 Color: 0
Size: 1486 Color: 12
Size: 296 Color: 5

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 10845 Color: 11
Size: 4191 Color: 18
Size: 836 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 13931 Color: 5
Size: 1619 Color: 6
Size: 322 Color: 7

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 17
Size: 2175 Color: 10
Size: 434 Color: 17

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 13143 Color: 12
Size: 2275 Color: 11
Size: 454 Color: 13

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 18
Size: 1455 Color: 8
Size: 290 Color: 8

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 14155 Color: 6
Size: 1431 Color: 19
Size: 286 Color: 14

Total size: 3142656
Total free space: 0

