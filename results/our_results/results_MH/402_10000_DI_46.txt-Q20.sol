Capicity Bin: 7360
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3683 Color: 15
Size: 811 Color: 10
Size: 781 Color: 17
Size: 776 Color: 16
Size: 741 Color: 9
Size: 296 Color: 6
Size: 272 Color: 3

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 3690 Color: 12
Size: 1256 Color: 1
Size: 1078 Color: 8
Size: 786 Color: 4
Size: 550 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3925 Color: 6
Size: 3291 Color: 17
Size: 144 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 4
Size: 3064 Color: 7
Size: 176 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 7
Size: 2642 Color: 16
Size: 528 Color: 8

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 4608 Color: 4
Size: 2752 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 1
Size: 2284 Color: 3
Size: 136 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 5
Size: 2132 Color: 11
Size: 120 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5584 Color: 13
Size: 1592 Color: 9
Size: 184 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5592 Color: 1
Size: 1564 Color: 10
Size: 204 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 14
Size: 1528 Color: 2
Size: 148 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 17
Size: 1588 Color: 16
Size: 74 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5784 Color: 3
Size: 1404 Color: 19
Size: 172 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 18
Size: 1265 Color: 15
Size: 252 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 11
Size: 1136 Color: 2
Size: 322 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5997 Color: 13
Size: 1137 Color: 19
Size: 226 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6168 Color: 7
Size: 1000 Color: 15
Size: 192 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 7
Size: 808 Color: 17
Size: 368 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 13
Size: 723 Color: 1
Size: 416 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 2
Size: 812 Color: 16
Size: 304 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6408 Color: 5
Size: 728 Color: 14
Size: 224 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6418 Color: 16
Size: 566 Color: 15
Size: 376 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6440 Color: 9
Size: 904 Color: 19
Size: 16 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 17
Size: 574 Color: 10
Size: 336 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6466 Color: 5
Size: 702 Color: 7
Size: 192 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 3
Size: 464 Color: 14
Size: 392 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 18
Size: 714 Color: 16
Size: 140 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 9
Size: 675 Color: 7
Size: 134 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 13
Size: 452 Color: 16
Size: 336 Color: 1

Bin 30: 1 of cap free
Amount of items: 10
Items: 
Size: 3681 Color: 8
Size: 488 Color: 12
Size: 480 Color: 12
Size: 456 Color: 11
Size: 456 Color: 6
Size: 448 Color: 19
Size: 422 Color: 7
Size: 416 Color: 9
Size: 304 Color: 18
Size: 208 Color: 15

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 4662 Color: 19
Size: 2409 Color: 3
Size: 288 Color: 13

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 4857 Color: 8
Size: 2262 Color: 15
Size: 240 Color: 10

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 1
Size: 2087 Color: 2
Size: 224 Color: 19

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5086 Color: 14
Size: 2113 Color: 17
Size: 160 Color: 16

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5421 Color: 18
Size: 1586 Color: 10
Size: 352 Color: 8

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5827 Color: 12
Size: 1320 Color: 9
Size: 212 Color: 4

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6068 Color: 8
Size: 1291 Color: 19

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 5
Size: 945 Color: 2
Size: 312 Color: 3

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 2
Size: 985 Color: 18
Size: 32 Color: 15

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6423 Color: 16
Size: 608 Color: 1
Size: 328 Color: 13

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6471 Color: 16
Size: 660 Color: 14
Size: 228 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6493 Color: 13
Size: 612 Color: 17
Size: 254 Color: 8

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6622 Color: 9
Size: 737 Color: 17

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 3961 Color: 15
Size: 2869 Color: 12
Size: 528 Color: 3

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 4964 Color: 4
Size: 2294 Color: 16
Size: 100 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 0
Size: 1784 Color: 3
Size: 284 Color: 2

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 5528 Color: 18
Size: 1668 Color: 4
Size: 162 Color: 17

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 13
Size: 1324 Color: 19
Size: 320 Color: 17

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 16
Size: 1244 Color: 5
Size: 200 Color: 9

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 5
Size: 1218 Color: 1
Size: 136 Color: 9

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 6280 Color: 6
Size: 934 Color: 12
Size: 144 Color: 1

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 13
Size: 1025 Color: 4
Size: 32 Color: 4

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6508 Color: 7
Size: 850 Color: 13

Bin 54: 3 of cap free
Amount of items: 4
Items: 
Size: 3941 Color: 14
Size: 3060 Color: 19
Size: 236 Color: 1
Size: 120 Color: 5

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 10
Size: 1208 Color: 1
Size: 140 Color: 14

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 6307 Color: 9
Size: 1050 Color: 12

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 5336 Color: 15
Size: 2020 Color: 4

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 8
Size: 1224 Color: 14

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 15
Size: 618 Color: 1
Size: 384 Color: 5

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 6472 Color: 17
Size: 868 Color: 14
Size: 16 Color: 0

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 6477 Color: 10
Size: 879 Color: 4

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 6540 Color: 14
Size: 816 Color: 5

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 5
Size: 770 Color: 0

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 11
Size: 2644 Color: 3
Size: 240 Color: 15

Bin 65: 5 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 17
Size: 2101 Color: 19
Size: 608 Color: 17

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 5437 Color: 1
Size: 1726 Color: 16
Size: 192 Color: 11

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6324 Color: 7
Size: 1031 Color: 17

Bin 68: 6 of cap free
Amount of items: 3
Items: 
Size: 3945 Color: 3
Size: 3261 Color: 9
Size: 148 Color: 8

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 15
Size: 1374 Color: 1

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 6086 Color: 8
Size: 1268 Color: 3

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 10
Size: 941 Color: 1
Size: 288 Color: 18

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 19
Size: 1206 Color: 15

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 6226 Color: 9
Size: 1096 Color: 11
Size: 32 Color: 15

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 11
Size: 1127 Color: 10

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6403 Color: 13
Size: 951 Color: 8

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 5928 Color: 2
Size: 1425 Color: 1

Bin 77: 8 of cap free
Amount of items: 7
Items: 
Size: 3684 Color: 14
Size: 728 Color: 0
Size: 716 Color: 3
Size: 684 Color: 9
Size: 528 Color: 15
Size: 528 Color: 5
Size: 484 Color: 11

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 4841 Color: 9
Size: 2511 Color: 18

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 5864 Color: 9
Size: 1488 Color: 13

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 6536 Color: 6
Size: 816 Color: 18

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 9
Size: 746 Color: 2

Bin 82: 9 of cap free
Amount of items: 2
Items: 
Size: 5987 Color: 15
Size: 1364 Color: 2

Bin 83: 10 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 15
Size: 3066 Color: 7
Size: 96 Color: 13

Bin 84: 10 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 7
Size: 2652 Color: 2
Size: 146 Color: 11

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 5876 Color: 5
Size: 1474 Color: 1

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 6242 Color: 18
Size: 1108 Color: 14

Bin 87: 11 of cap free
Amount of items: 3
Items: 
Size: 4578 Color: 10
Size: 2427 Color: 9
Size: 344 Color: 16

Bin 88: 11 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 0
Size: 1145 Color: 10
Size: 744 Color: 3

Bin 89: 11 of cap free
Amount of items: 2
Items: 
Size: 5813 Color: 13
Size: 1536 Color: 0

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 6070 Color: 13
Size: 1279 Color: 12

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 4280 Color: 2
Size: 3068 Color: 16

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 5464 Color: 13
Size: 1884 Color: 12

Bin 93: 12 of cap free
Amount of items: 2
Items: 
Size: 6522 Color: 10
Size: 826 Color: 13

Bin 94: 13 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 9
Size: 2803 Color: 15
Size: 364 Color: 16

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 5651 Color: 13
Size: 1696 Color: 2

Bin 96: 13 of cap free
Amount of items: 3
Items: 
Size: 6434 Color: 17
Size: 865 Color: 5
Size: 48 Color: 16

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 6388 Color: 5
Size: 958 Color: 12

Bin 98: 15 of cap free
Amount of items: 3
Items: 
Size: 3997 Color: 0
Size: 3062 Color: 9
Size: 286 Color: 3

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 4776 Color: 2
Size: 2568 Color: 13

Bin 100: 17 of cap free
Amount of items: 2
Items: 
Size: 5175 Color: 7
Size: 2168 Color: 19

Bin 101: 19 of cap free
Amount of items: 2
Items: 
Size: 5724 Color: 12
Size: 1617 Color: 1

Bin 102: 20 of cap free
Amount of items: 3
Items: 
Size: 3909 Color: 1
Size: 3343 Color: 1
Size: 88 Color: 9

Bin 103: 20 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 1
Size: 2344 Color: 4

Bin 104: 22 of cap free
Amount of items: 2
Items: 
Size: 5650 Color: 15
Size: 1688 Color: 11

Bin 105: 23 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 9
Size: 1603 Color: 4
Size: 646 Color: 16

Bin 106: 24 of cap free
Amount of items: 2
Items: 
Size: 5364 Color: 1
Size: 1972 Color: 10

Bin 107: 25 of cap free
Amount of items: 2
Items: 
Size: 5896 Color: 12
Size: 1439 Color: 14

Bin 108: 25 of cap free
Amount of items: 2
Items: 
Size: 6323 Color: 4
Size: 1012 Color: 13

Bin 109: 26 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 9
Size: 1898 Color: 3
Size: 212 Color: 16

Bin 110: 27 of cap free
Amount of items: 2
Items: 
Size: 6387 Color: 16
Size: 946 Color: 2

Bin 111: 28 of cap free
Amount of items: 2
Items: 
Size: 4455 Color: 14
Size: 2877 Color: 6

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 6056 Color: 17
Size: 1276 Color: 4

Bin 113: 33 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 14
Size: 2833 Color: 19
Size: 300 Color: 4

Bin 114: 36 of cap free
Amount of items: 8
Items: 
Size: 3682 Color: 16
Size: 630 Color: 18
Size: 608 Color: 14
Size: 608 Color: 3
Size: 560 Color: 14
Size: 560 Color: 4
Size: 502 Color: 19
Size: 174 Color: 6

Bin 115: 36 of cap free
Amount of items: 2
Items: 
Size: 5844 Color: 10
Size: 1480 Color: 5

Bin 116: 37 of cap free
Amount of items: 2
Items: 
Size: 5492 Color: 12
Size: 1831 Color: 9

Bin 117: 38 of cap free
Amount of items: 2
Items: 
Size: 4610 Color: 18
Size: 2712 Color: 0

Bin 118: 39 of cap free
Amount of items: 2
Items: 
Size: 4449 Color: 11
Size: 2872 Color: 10

Bin 119: 39 of cap free
Amount of items: 2
Items: 
Size: 4825 Color: 17
Size: 2496 Color: 10

Bin 120: 42 of cap free
Amount of items: 2
Items: 
Size: 5390 Color: 14
Size: 1928 Color: 13

Bin 121: 44 of cap free
Amount of items: 2
Items: 
Size: 4822 Color: 0
Size: 2494 Color: 8

Bin 122: 54 of cap free
Amount of items: 6
Items: 
Size: 3688 Color: 5
Size: 1062 Color: 4
Size: 984 Color: 18
Size: 932 Color: 5
Size: 512 Color: 6
Size: 128 Color: 11

Bin 123: 54 of cap free
Amount of items: 2
Items: 
Size: 4628 Color: 3
Size: 2678 Color: 5

Bin 124: 55 of cap free
Amount of items: 2
Items: 
Size: 5635 Color: 1
Size: 1670 Color: 18

Bin 125: 60 of cap free
Amount of items: 27
Items: 
Size: 418 Color: 17
Size: 416 Color: 2
Size: 400 Color: 6
Size: 396 Color: 16
Size: 394 Color: 19
Size: 364 Color: 13
Size: 304 Color: 13
Size: 276 Color: 3
Size: 272 Color: 18
Size: 272 Color: 14
Size: 272 Color: 9
Size: 272 Color: 5
Size: 264 Color: 16
Size: 256 Color: 16
Size: 256 Color: 0
Size: 248 Color: 11
Size: 240 Color: 18
Size: 240 Color: 17
Size: 240 Color: 8
Size: 224 Color: 3
Size: 208 Color: 15
Size: 208 Color: 4
Size: 204 Color: 11
Size: 204 Color: 1
Size: 188 Color: 8
Size: 136 Color: 4
Size: 128 Color: 15

Bin 126: 61 of cap free
Amount of items: 2
Items: 
Size: 5478 Color: 14
Size: 1821 Color: 19

Bin 127: 70 of cap free
Amount of items: 2
Items: 
Size: 4596 Color: 7
Size: 2694 Color: 17

Bin 128: 73 of cap free
Amount of items: 2
Items: 
Size: 6131 Color: 19
Size: 1156 Color: 15

Bin 129: 77 of cap free
Amount of items: 2
Items: 
Size: 5165 Color: 11
Size: 2118 Color: 15

Bin 130: 79 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 12
Size: 2933 Color: 14
Size: 420 Color: 4

Bin 131: 86 of cap free
Amount of items: 2
Items: 
Size: 4966 Color: 0
Size: 2308 Color: 18

Bin 132: 96 of cap free
Amount of items: 4
Items: 
Size: 3692 Color: 1
Size: 1998 Color: 11
Size: 1386 Color: 17
Size: 188 Color: 6

Bin 133: 5548 of cap free
Amount of items: 12
Items: 
Size: 188 Color: 9
Size: 184 Color: 1
Size: 168 Color: 18
Size: 168 Color: 5
Size: 156 Color: 17
Size: 156 Color: 8
Size: 144 Color: 6
Size: 144 Color: 2
Size: 128 Color: 7
Size: 128 Color: 4
Size: 124 Color: 15
Size: 124 Color: 11

Total size: 971520
Total free space: 7360

