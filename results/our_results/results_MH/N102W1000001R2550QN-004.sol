Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 370565 Color: 72
Size: 321556 Color: 53
Size: 307880 Color: 46

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 393903 Color: 80
Size: 347572 Color: 64
Size: 258526 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 380385 Color: 76
Size: 337563 Color: 60
Size: 282053 Color: 29

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 361459 Color: 69
Size: 325982 Color: 56
Size: 312560 Color: 50

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 381242 Color: 77
Size: 339174 Color: 61
Size: 279585 Color: 28

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 370196 Color: 71
Size: 351855 Color: 67
Size: 277950 Color: 26

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 499313 Color: 101
Size: 250582 Color: 2
Size: 250106 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 398227 Color: 82
Size: 346861 Color: 63
Size: 254913 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 348898 Color: 65
Size: 373193 Color: 74
Size: 277910 Color: 25

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 496151 Color: 100
Size: 253632 Color: 5
Size: 250218 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 360729 Color: 68
Size: 328886 Color: 58
Size: 310386 Color: 48

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 322607 Color: 54
Size: 294573 Color: 39
Size: 382821 Color: 78

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 342207 Color: 62
Size: 372787 Color: 73
Size: 285007 Color: 31

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 378489 Color: 75
Size: 289109 Color: 35
Size: 332403 Color: 59

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 405193 Color: 83
Size: 297666 Color: 42
Size: 297142 Color: 41

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 396986 Color: 81
Size: 318078 Color: 52
Size: 284937 Color: 30

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 369107 Color: 70
Size: 306261 Color: 45
Size: 324633 Color: 55

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 392324 Color: 79
Size: 351446 Color: 66
Size: 256231 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 408103 Color: 84
Size: 328462 Color: 57
Size: 263436 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 411035 Color: 85
Size: 295579 Color: 40
Size: 293387 Color: 37

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 425078 Color: 86
Size: 317850 Color: 51
Size: 257073 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 429748 Color: 87
Size: 293827 Color: 38
Size: 276426 Color: 24

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 430134 Color: 88
Size: 299744 Color: 43
Size: 270123 Color: 21

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 431102 Color: 89
Size: 312454 Color: 49
Size: 256445 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 432634 Color: 90
Size: 310104 Color: 47
Size: 257263 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 439009 Color: 91
Size: 287763 Color: 34
Size: 273229 Color: 22

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 440540 Color: 92
Size: 300214 Color: 44
Size: 259247 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 448097 Color: 93
Size: 289713 Color: 36
Size: 262191 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 460874 Color: 94
Size: 278963 Color: 27
Size: 260164 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 461069 Color: 95
Size: 286465 Color: 33
Size: 252467 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 462349 Color: 96
Size: 286352 Color: 32
Size: 251300 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 464546 Color: 97
Size: 273738 Color: 23
Size: 261717 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 470626 Color: 98
Size: 264989 Color: 20
Size: 264386 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 491679 Color: 99
Size: 254415 Color: 7
Size: 253907 Color: 6

Total size: 34000034
Total free space: 0

