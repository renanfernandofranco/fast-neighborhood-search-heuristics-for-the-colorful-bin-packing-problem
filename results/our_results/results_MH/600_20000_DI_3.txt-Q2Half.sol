Capicity Bin: 16752
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 1
Size: 2860 Color: 0
Size: 668 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12764 Color: 1
Size: 3532 Color: 1
Size: 456 Color: 0

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 6980 Color: 1
Size: 5353 Color: 1
Size: 2300 Color: 0
Size: 1735 Color: 1
Size: 384 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 10460 Color: 1
Size: 5156 Color: 1
Size: 960 Color: 0
Size: 176 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8380 Color: 1
Size: 6978 Color: 1
Size: 1394 Color: 0

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 8504 Color: 1
Size: 5736 Color: 1
Size: 1032 Color: 0
Size: 1024 Color: 0
Size: 456 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 13870 Color: 1
Size: 1826 Color: 1
Size: 736 Color: 0
Size: 320 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8381 Color: 1
Size: 6977 Color: 1
Size: 1394 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8385 Color: 1
Size: 6973 Color: 1
Size: 1394 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8388 Color: 1
Size: 6972 Color: 1
Size: 1392 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8389 Color: 1
Size: 6971 Color: 1
Size: 1392 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 1
Size: 6968 Color: 1
Size: 1392 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9446 Color: 1
Size: 6090 Color: 1
Size: 1216 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 1
Size: 6036 Color: 1
Size: 1200 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 1
Size: 6028 Color: 1
Size: 1200 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 1
Size: 6488 Color: 1
Size: 688 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 1
Size: 5528 Color: 1
Size: 1088 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11070 Color: 1
Size: 4738 Color: 1
Size: 944 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 1
Size: 4904 Color: 1
Size: 544 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11703 Color: 1
Size: 4209 Color: 1
Size: 840 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 1
Size: 4520 Color: 1
Size: 512 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11750 Color: 1
Size: 4170 Color: 1
Size: 832 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 1
Size: 3924 Color: 1
Size: 784 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12115 Color: 1
Size: 3865 Color: 1
Size: 772 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 1
Size: 3863 Color: 1
Size: 772 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 1
Size: 3710 Color: 1
Size: 740 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12318 Color: 1
Size: 3698 Color: 1
Size: 736 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12459 Color: 1
Size: 3579 Color: 1
Size: 714 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12516 Color: 1
Size: 3736 Color: 1
Size: 500 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 1
Size: 3436 Color: 1
Size: 748 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12571 Color: 1
Size: 3485 Color: 1
Size: 696 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12572 Color: 1
Size: 3284 Color: 1
Size: 896 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12636 Color: 1
Size: 3324 Color: 1
Size: 792 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12664 Color: 1
Size: 3416 Color: 1
Size: 672 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 1
Size: 3660 Color: 1
Size: 424 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 1
Size: 3098 Color: 1
Size: 864 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 1
Size: 3386 Color: 1
Size: 568 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12839 Color: 1
Size: 2777 Color: 1
Size: 1136 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12888 Color: 1
Size: 3484 Color: 1
Size: 380 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12945 Color: 1
Size: 3173 Color: 1
Size: 634 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 1
Size: 3176 Color: 1
Size: 624 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12953 Color: 1
Size: 3167 Color: 1
Size: 632 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13038 Color: 1
Size: 2666 Color: 1
Size: 1048 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13157 Color: 1
Size: 2399 Color: 1
Size: 1196 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 1
Size: 2934 Color: 1
Size: 584 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13269 Color: 1
Size: 2299 Color: 1
Size: 1184 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 1
Size: 3208 Color: 1
Size: 236 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13326 Color: 1
Size: 2722 Color: 1
Size: 704 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13327 Color: 1
Size: 3201 Color: 1
Size: 224 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 1
Size: 2804 Color: 1
Size: 564 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 1
Size: 2402 Color: 1
Size: 962 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13421 Color: 1
Size: 2691 Color: 1
Size: 640 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 1
Size: 2682 Color: 1
Size: 580 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13525 Color: 1
Size: 2203 Color: 1
Size: 1024 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13554 Color: 1
Size: 2662 Color: 1
Size: 536 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 1
Size: 2808 Color: 1
Size: 384 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 1
Size: 2354 Color: 1
Size: 836 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 1
Size: 2601 Color: 1
Size: 518 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 2744 Color: 1
Size: 368 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13692 Color: 1
Size: 2500 Color: 1
Size: 560 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13745 Color: 1
Size: 2555 Color: 1
Size: 452 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 1
Size: 2336 Color: 1
Size: 660 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13762 Color: 1
Size: 1922 Color: 1
Size: 1068 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 1
Size: 2600 Color: 1
Size: 352 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13817 Color: 1
Size: 2319 Color: 1
Size: 616 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 1
Size: 2494 Color: 1
Size: 362 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 1
Size: 2472 Color: 1
Size: 340 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13947 Color: 1
Size: 2339 Color: 1
Size: 466 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13951 Color: 1
Size: 2335 Color: 1
Size: 466 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13996 Color: 1
Size: 2288 Color: 1
Size: 468 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14002 Color: 1
Size: 2510 Color: 1
Size: 240 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 14007 Color: 1
Size: 2265 Color: 1
Size: 480 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 1
Size: 2274 Color: 1
Size: 452 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 1
Size: 2262 Color: 1
Size: 458 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 14035 Color: 1
Size: 2019 Color: 1
Size: 698 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 2348 Color: 1
Size: 344 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 1
Size: 2392 Color: 1
Size: 288 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 14163 Color: 1
Size: 2159 Color: 1
Size: 430 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 1
Size: 2026 Color: 1
Size: 528 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 1
Size: 1512 Color: 0
Size: 1040 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 14207 Color: 1
Size: 2121 Color: 1
Size: 424 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 1
Size: 1944 Color: 1
Size: 598 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 14226 Color: 1
Size: 2106 Color: 1
Size: 420 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 14234 Color: 1
Size: 1986 Color: 1
Size: 532 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 1
Size: 1852 Color: 1
Size: 652 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 1
Size: 2244 Color: 1
Size: 248 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 14285 Color: 1
Size: 1979 Color: 1
Size: 488 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 14293 Color: 1
Size: 2051 Color: 1
Size: 408 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 1
Size: 2102 Color: 1
Size: 328 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 1
Size: 1484 Color: 1
Size: 944 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14370 Color: 1
Size: 2122 Color: 1
Size: 260 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14386 Color: 1
Size: 2130 Color: 1
Size: 236 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14403 Color: 1
Size: 1959 Color: 1
Size: 390 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14415 Color: 1
Size: 1949 Color: 1
Size: 388 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 1
Size: 1880 Color: 1
Size: 448 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14468 Color: 1
Size: 1580 Color: 1
Size: 704 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 1
Size: 1700 Color: 1
Size: 554 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14503 Color: 1
Size: 1965 Color: 1
Size: 284 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 1
Size: 1978 Color: 1
Size: 260 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 1
Size: 1862 Color: 1
Size: 368 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1724 Color: 1
Size: 496 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 1
Size: 1804 Color: 1
Size: 396 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 1
Size: 1694 Color: 1
Size: 496 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14571 Color: 1
Size: 1819 Color: 1
Size: 362 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14575 Color: 1
Size: 2149 Color: 1
Size: 28 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14588 Color: 1
Size: 1702 Color: 1
Size: 462 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14591 Color: 1
Size: 1801 Color: 1
Size: 360 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 1
Size: 1594 Color: 1
Size: 532 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 1
Size: 1804 Color: 1
Size: 296 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 1
Size: 1752 Color: 1
Size: 336 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 1
Size: 1756 Color: 1
Size: 312 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14698 Color: 1
Size: 1714 Color: 1
Size: 340 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 1
Size: 1774 Color: 1
Size: 278 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14710 Color: 1
Size: 1974 Color: 1
Size: 68 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 1
Size: 1636 Color: 1
Size: 400 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14722 Color: 1
Size: 1670 Color: 1
Size: 360 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14744 Color: 1
Size: 1662 Color: 1
Size: 346 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14782 Color: 1
Size: 1592 Color: 1
Size: 378 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14796 Color: 1
Size: 1562 Color: 1
Size: 394 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14846 Color: 1
Size: 1498 Color: 1
Size: 408 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14854 Color: 1
Size: 1642 Color: 1
Size: 256 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14856 Color: 1
Size: 1448 Color: 1
Size: 448 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 1
Size: 1470 Color: 1
Size: 404 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14898 Color: 1
Size: 1070 Color: 1
Size: 784 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14952 Color: 1
Size: 1136 Color: 1
Size: 664 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14958 Color: 1
Size: 1392 Color: 1
Size: 402 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 1
Size: 1476 Color: 1
Size: 304 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14974 Color: 1
Size: 1482 Color: 1
Size: 296 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14988 Color: 1
Size: 1392 Color: 1
Size: 372 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14990 Color: 1
Size: 1392 Color: 1
Size: 370 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 15002 Color: 1
Size: 1462 Color: 1
Size: 288 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 15012 Color: 1
Size: 1452 Color: 1
Size: 288 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 15026 Color: 1
Size: 1198 Color: 1
Size: 528 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 15032 Color: 1
Size: 1216 Color: 1
Size: 504 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 1
Size: 1592 Color: 1
Size: 112 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 9559 Color: 1
Size: 5992 Color: 1
Size: 1200 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 10378 Color: 1
Size: 5981 Color: 1
Size: 392 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 11075 Color: 1
Size: 5244 Color: 1
Size: 432 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 11083 Color: 1
Size: 4824 Color: 1
Size: 844 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 11705 Color: 1
Size: 4614 Color: 1
Size: 432 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 11734 Color: 1
Size: 4725 Color: 1
Size: 292 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 1
Size: 3491 Color: 1
Size: 1280 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 1
Size: 4207 Color: 1
Size: 504 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 12563 Color: 1
Size: 2956 Color: 1
Size: 1232 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 1
Size: 2507 Color: 1
Size: 920 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 1
Size: 2447 Color: 1
Size: 880 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 1
Size: 2664 Color: 1
Size: 364 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 13919 Color: 1
Size: 2248 Color: 1
Size: 584 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 1
Size: 2525 Color: 1
Size: 296 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13971 Color: 1
Size: 2120 Color: 1
Size: 660 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 1
Size: 2289 Color: 1
Size: 420 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 14331 Color: 1
Size: 2136 Color: 1
Size: 284 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 1
Size: 2084 Color: 1
Size: 288 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 14479 Color: 1
Size: 2032 Color: 1
Size: 240 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 14523 Color: 1
Size: 1688 Color: 1
Size: 540 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 14671 Color: 1
Size: 1908 Color: 1
Size: 172 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 9442 Color: 1
Size: 6256 Color: 1
Size: 1052 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 10556 Color: 1
Size: 5354 Color: 1
Size: 840 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 10971 Color: 1
Size: 4819 Color: 1
Size: 960 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 11686 Color: 1
Size: 4408 Color: 1
Size: 656 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 1
Size: 3318 Color: 1
Size: 1152 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 1
Size: 3750 Color: 1
Size: 560 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 12726 Color: 1
Size: 3608 Color: 1
Size: 416 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 1
Size: 3496 Color: 1
Size: 480 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 13538 Color: 1
Size: 2876 Color: 1
Size: 336 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 1
Size: 1859 Color: 1
Size: 896 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 1
Size: 6982 Color: 1
Size: 1312 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 14450 Color: 1
Size: 2028 Color: 1
Size: 272 Color: 0

Bin 169: 4 of cap free
Amount of items: 3
Items: 
Size: 10433 Color: 1
Size: 5995 Color: 1
Size: 320 Color: 0

Bin 170: 4 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 1
Size: 5760 Color: 1
Size: 464 Color: 0

Bin 171: 4 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 1
Size: 4516 Color: 1
Size: 1360 Color: 0

Bin 172: 4 of cap free
Amount of items: 3
Items: 
Size: 13212 Color: 1
Size: 3224 Color: 1
Size: 312 Color: 0

Bin 173: 4 of cap free
Amount of items: 3
Items: 
Size: 8382 Color: 1
Size: 5810 Color: 1
Size: 2556 Color: 0

Bin 174: 5 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 1
Size: 5267 Color: 1
Size: 512 Color: 0

Bin 175: 6 of cap free
Amount of items: 3
Items: 
Size: 11599 Color: 1
Size: 4731 Color: 1
Size: 416 Color: 0

Bin 176: 6 of cap free
Amount of items: 3
Items: 
Size: 10330 Color: 1
Size: 6072 Color: 1
Size: 344 Color: 0

Bin 177: 7 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 1
Size: 4981 Color: 1
Size: 336 Color: 0

Bin 178: 8 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 1
Size: 6640 Color: 1
Size: 224 Color: 0

Bin 179: 8 of cap free
Amount of items: 3
Items: 
Size: 10572 Color: 1
Size: 5196 Color: 1
Size: 976 Color: 0

Bin 180: 9 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 2903 Color: 1
Size: 376 Color: 0

Bin 181: 9 of cap free
Amount of items: 3
Items: 
Size: 10329 Color: 1
Size: 6094 Color: 1
Size: 320 Color: 0

Bin 182: 10 of cap free
Amount of items: 3
Items: 
Size: 10632 Color: 1
Size: 5164 Color: 1
Size: 946 Color: 0

Bin 183: 14 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 1
Size: 7328 Color: 1
Size: 1032 Color: 0

Bin 184: 14 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 1
Size: 3358 Color: 1
Size: 568 Color: 0

Bin 185: 16 of cap free
Amount of items: 3
Items: 
Size: 8256 Color: 1
Size: 4552 Color: 1
Size: 3928 Color: 0

Bin 186: 26 of cap free
Amount of items: 3
Items: 
Size: 14860 Color: 1
Size: 1442 Color: 1
Size: 424 Color: 0

Bin 187: 31 of cap free
Amount of items: 3
Items: 
Size: 9480 Color: 1
Size: 3980 Color: 0
Size: 3261 Color: 1

Bin 188: 34 of cap free
Amount of items: 3
Items: 
Size: 9575 Color: 1
Size: 6167 Color: 1
Size: 976 Color: 0

Bin 189: 47 of cap free
Amount of items: 3
Items: 
Size: 9353 Color: 1
Size: 6888 Color: 1
Size: 464 Color: 0

Bin 190: 51 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 1
Size: 6981 Color: 1
Size: 736 Color: 0

Bin 191: 54 of cap free
Amount of items: 3
Items: 
Size: 11218 Color: 1
Size: 5160 Color: 1
Size: 320 Color: 0

Bin 192: 102 of cap free
Amount of items: 3
Items: 
Size: 14187 Color: 1
Size: 1895 Color: 1
Size: 568 Color: 0

Bin 193: 112 of cap free
Amount of items: 3
Items: 
Size: 8800 Color: 1
Size: 7144 Color: 1
Size: 696 Color: 0

Bin 194: 142 of cap free
Amount of items: 3
Items: 
Size: 10506 Color: 1
Size: 4222 Color: 1
Size: 1882 Color: 0

Bin 195: 148 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 1
Size: 4444 Color: 1
Size: 680 Color: 0

Bin 196: 172 of cap free
Amount of items: 3
Items: 
Size: 8377 Color: 1
Size: 5206 Color: 0
Size: 2997 Color: 1

Bin 197: 220 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 1
Size: 4182 Color: 1
Size: 96 Color: 0

Bin 198: 1542 of cap free
Amount of items: 3
Items: 
Size: 11340 Color: 1
Size: 3302 Color: 1
Size: 568 Color: 0

Bin 199: 13894 of cap free
Amount of items: 1
Items: 
Size: 2858 Color: 1

Total size: 3316896
Total free space: 16752

