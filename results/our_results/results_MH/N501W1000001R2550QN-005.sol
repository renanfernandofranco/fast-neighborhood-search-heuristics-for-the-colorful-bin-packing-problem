Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 400489 Color: 402
Size: 304932 Color: 218
Size: 294580 Color: 185

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 455107 Color: 467
Size: 287317 Color: 163
Size: 257577 Color: 47

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 452480 Color: 464
Size: 281932 Color: 146
Size: 265589 Color: 85

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 375128 Color: 369
Size: 371361 Color: 366
Size: 253512 Color: 21

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370127 Color: 362
Size: 364282 Color: 347
Size: 265592 Color: 86

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 476222 Color: 483
Size: 268382 Color: 101
Size: 255397 Color: 33

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 391903 Color: 391
Size: 332378 Color: 284
Size: 275720 Color: 123

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 371097 Color: 364
Size: 369599 Color: 361
Size: 259305 Color: 53

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 467108 Color: 474
Size: 266892 Color: 95
Size: 266001 Color: 90

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 362851 Color: 344
Size: 351226 Color: 326
Size: 285924 Color: 159

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 385065 Color: 381
Size: 357428 Color: 340
Size: 257508 Color: 45

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 370471 Color: 363
Size: 350798 Color: 324
Size: 278732 Color: 137

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 454192 Color: 465
Size: 282000 Color: 147
Size: 263809 Color: 76

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 353171 Color: 328
Size: 348925 Color: 320
Size: 297905 Color: 196

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 381765 Color: 377
Size: 312547 Color: 236
Size: 305689 Color: 222

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 447823 Color: 457
Size: 291931 Color: 178
Size: 260247 Color: 58

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 385838 Color: 384
Size: 313039 Color: 237
Size: 301124 Color: 209

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 450729 Color: 460
Size: 281364 Color: 145
Size: 267908 Color: 97

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 368713 Color: 358
Size: 340979 Color: 304
Size: 290309 Color: 172

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 401169 Color: 405
Size: 332865 Color: 285
Size: 265967 Color: 89

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 410175 Color: 420
Size: 313779 Color: 240
Size: 276047 Color: 124

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 484212 Color: 494
Size: 260801 Color: 62
Size: 254988 Color: 30

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 489375 Color: 497
Size: 260360 Color: 60
Size: 250266 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 477978 Color: 487
Size: 262287 Color: 69
Size: 259736 Color: 54

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 482289 Color: 489
Size: 262293 Color: 70
Size: 255419 Color: 34

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 398832 Color: 395
Size: 348536 Color: 318
Size: 252633 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 494522 Color: 499
Size: 253456 Color: 20
Size: 252023 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 374516 Color: 368
Size: 347750 Color: 317
Size: 277735 Color: 134

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 342733 Color: 308
Size: 339675 Color: 303
Size: 317593 Color: 248

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 395263 Color: 393
Size: 322392 Color: 263
Size: 282346 Color: 148

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 404150 Color: 410
Size: 303569 Color: 215
Size: 292282 Color: 180

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 393579 Color: 392
Size: 303759 Color: 216
Size: 302663 Color: 212

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 399128 Color: 396
Size: 306318 Color: 224
Size: 294555 Color: 184

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 383434 Color: 378
Size: 315540 Color: 244
Size: 301027 Color: 208

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 342812 Color: 309
Size: 333064 Color: 286
Size: 324125 Color: 269

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 358181 Color: 343
Size: 327180 Color: 276
Size: 314640 Color: 242

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 378480 Color: 373
Size: 334049 Color: 291
Size: 287472 Color: 165

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 350964 Color: 325
Size: 346834 Color: 315
Size: 302203 Color: 210

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 407936 Color: 417
Size: 305170 Color: 219
Size: 286895 Color: 162

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 451731 Color: 462
Size: 289417 Color: 171
Size: 258853 Color: 51

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 400935 Color: 404
Size: 299971 Color: 204
Size: 299095 Color: 202

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 432776 Color: 450
Size: 299103 Color: 203
Size: 268122 Color: 98

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 415766 Color: 429
Size: 315992 Color: 246
Size: 268243 Color: 99

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 400150 Color: 401
Size: 345170 Color: 312
Size: 254681 Color: 28

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 457229 Color: 469
Size: 287696 Color: 166
Size: 255076 Color: 31

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 431250 Color: 448
Size: 285349 Color: 158
Size: 283402 Color: 152

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 404995 Color: 413
Size: 314763 Color: 243
Size: 280243 Color: 141

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 430161 Color: 446
Size: 292248 Color: 179
Size: 277592 Color: 133

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 476474 Color: 485
Size: 271992 Color: 110
Size: 251535 Color: 7

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 451149 Color: 461
Size: 297771 Color: 195
Size: 251081 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 482723 Color: 491
Size: 265495 Color: 84
Size: 251783 Color: 9

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 467915 Color: 476
Size: 280106 Color: 139
Size: 251980 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 497197 Color: 500
Size: 251849 Color: 10
Size: 250955 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 451737 Color: 463
Size: 288518 Color: 167
Size: 259746 Color: 55

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 456499 Color: 468
Size: 288870 Color: 170
Size: 254632 Color: 27

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 428353 Color: 442
Size: 306429 Color: 225
Size: 265219 Color: 82

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 397609 Color: 394
Size: 338851 Color: 301
Size: 263541 Color: 75

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 419945 Color: 433
Size: 324344 Color: 270
Size: 255712 Color: 37

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 356903 Color: 339
Size: 345715 Color: 313
Size: 297383 Color: 191

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 385904 Color: 385
Size: 331264 Color: 282
Size: 282833 Color: 150

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 364221 Color: 346
Size: 347126 Color: 316
Size: 288654 Color: 168

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 399786 Color: 400
Size: 324086 Color: 268
Size: 276129 Color: 125

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 389365 Color: 389
Size: 357595 Color: 341
Size: 253041 Color: 18

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 426377 Color: 438
Size: 307472 Color: 227
Size: 266152 Color: 91

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 429292 Color: 444
Size: 296736 Color: 190
Size: 273973 Color: 116

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 400687 Color: 403
Size: 308822 Color: 231
Size: 290492 Color: 173

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 476299 Color: 484
Size: 269699 Color: 104
Size: 254003 Color: 23

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 368790 Color: 359
Size: 325860 Color: 272
Size: 305351 Color: 220

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 479299 Color: 488
Size: 268301 Color: 100
Size: 252401 Color: 14

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 441728 Color: 453
Size: 283697 Color: 153
Size: 274576 Color: 118

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 385556 Color: 382
Size: 354141 Color: 332
Size: 260304 Color: 59

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 412202 Color: 425
Size: 321489 Color: 260
Size: 266310 Color: 92

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 401441 Color: 406
Size: 318483 Color: 252
Size: 280077 Color: 138

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 399245 Color: 398
Size: 309780 Color: 233
Size: 290976 Color: 175

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 369513 Color: 360
Size: 368393 Color: 356
Size: 262095 Color: 67

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 419930 Color: 432
Size: 325979 Color: 273
Size: 254092 Color: 24

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 482451 Color: 490
Size: 261976 Color: 66
Size: 255574 Color: 36

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 430715 Color: 447
Size: 307686 Color: 228
Size: 261600 Color: 65

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 493956 Color: 498
Size: 253707 Color: 22
Size: 252338 Color: 13

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 427750 Color: 441
Size: 297709 Color: 194
Size: 274542 Color: 117

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 470270 Color: 481
Size: 273299 Color: 113
Size: 256432 Color: 40

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 468167 Color: 477
Size: 275059 Color: 120
Size: 256775 Color: 43

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 418465 Color: 430
Size: 329989 Color: 280
Size: 251547 Color: 8

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 350071 Color: 322
Size: 332129 Color: 283
Size: 317801 Color: 249

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 377255 Color: 372
Size: 339626 Color: 302
Size: 283120 Color: 151

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 405642 Color: 415
Size: 333968 Color: 290
Size: 260391 Color: 61

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 366065 Color: 351
Size: 343018 Color: 310
Size: 290918 Color: 174

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 421501 Color: 434
Size: 294026 Color: 183
Size: 284474 Color: 155

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 437500 Color: 452
Size: 287407 Color: 164
Size: 275094 Color: 121

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 387238 Color: 387
Size: 321481 Color: 259
Size: 291282 Color: 177

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 353337 Color: 330
Size: 346560 Color: 314
Size: 300104 Color: 205

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 487618 Color: 495
Size: 256453 Color: 41
Size: 255930 Color: 38

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 399228 Color: 397
Size: 324057 Color: 267
Size: 276716 Color: 129

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 466212 Color: 472
Size: 269624 Color: 103
Size: 264165 Color: 77

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 457327 Color: 470
Size: 284768 Color: 157
Size: 257906 Color: 49

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 443159 Color: 454
Size: 302256 Color: 211
Size: 254586 Color: 26

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 355030 Color: 333
Size: 322951 Color: 265
Size: 322020 Color: 262

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 432670 Color: 449
Size: 294775 Color: 186
Size: 272556 Color: 112

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 355475 Color: 334
Size: 327129 Color: 275
Size: 317397 Color: 247

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 406629 Color: 416
Size: 313208 Color: 239
Size: 280164 Color: 140

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 454685 Color: 466
Size: 284291 Color: 154
Size: 261025 Color: 63

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 366404 Color: 352
Size: 327564 Color: 277
Size: 306033 Color: 223

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 405227 Color: 414
Size: 333243 Color: 287
Size: 261531 Color: 64

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 353201 Color: 329
Size: 336864 Color: 295
Size: 309936 Color: 234

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 345
Size: 328419 Color: 278
Size: 308100 Color: 229

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 424611 Color: 436
Size: 298634 Color: 201
Size: 276756 Color: 130

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 477487 Color: 486
Size: 263243 Color: 73
Size: 259271 Color: 52

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 408621 Color: 418
Size: 308647 Color: 230
Size: 282733 Color: 149

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 463863 Color: 471
Size: 280567 Color: 142
Size: 255571 Color: 35

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 379334 Color: 375
Size: 357635 Color: 342
Size: 263032 Color: 71

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 413442 Color: 427
Size: 305454 Color: 221
Size: 281105 Color: 144

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 368074 Color: 354
Size: 367021 Color: 353
Size: 264906 Color: 81

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 428884 Color: 443
Size: 294794 Color: 187
Size: 276323 Color: 128

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 488504 Color: 496
Size: 257223 Color: 44
Size: 254274 Color: 25

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 371112 Color: 365
Size: 321422 Color: 257
Size: 307467 Color: 226

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 388701 Color: 388
Size: 337906 Color: 299
Size: 273394 Color: 114

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 411070 Color: 422
Size: 322575 Color: 264
Size: 266356 Color: 93

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 387152 Color: 386
Size: 356311 Color: 337
Size: 256538 Color: 42

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 467737 Color: 475
Size: 267797 Color: 96
Size: 264467 Color: 78

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 483763 Color: 493
Size: 263194 Color: 72
Size: 253044 Color: 19

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 411801 Color: 424
Size: 330628 Color: 281
Size: 257572 Color: 46

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 364545 Color: 348
Size: 337856 Color: 298
Size: 297600 Color: 193

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 418621 Color: 431
Size: 321567 Color: 261
Size: 259813 Color: 56

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 402718 Color: 408
Size: 344546 Color: 311
Size: 252737 Color: 17

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 383897 Color: 379
Size: 329781 Color: 279
Size: 286323 Color: 161

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 445038 Color: 455
Size: 278644 Color: 135
Size: 276319 Color: 127

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 411177 Color: 423
Size: 297558 Color: 192
Size: 291266 Color: 176

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 385031 Color: 380
Size: 314073 Color: 241
Size: 300897 Color: 207

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 422027 Color: 435
Size: 317963 Color: 250
Size: 260011 Color: 57

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 356303 Color: 336
Size: 348572 Color: 319
Size: 295126 Color: 188

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 448258 Color: 458
Size: 293705 Color: 181
Size: 258038 Color: 50

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 470066 Color: 480
Size: 265369 Color: 83
Size: 264566 Color: 79

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 410618 Color: 421
Size: 318310 Color: 251
Size: 271073 Color: 107

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 425049 Color: 437
Size: 304868 Color: 217
Size: 270084 Color: 105

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 482983 Color: 492
Size: 265661 Color: 87
Size: 251357 Color: 6

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 402908 Color: 409
Size: 342164 Color: 306
Size: 254929 Color: 29

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 399364 Color: 399
Size: 337352 Color: 297
Size: 263285 Color: 74

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 352366 Color: 327
Size: 336408 Color: 294
Size: 311227 Color: 235

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 385673 Color: 383
Size: 315935 Color: 245
Size: 298393 Color: 200

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 414162 Color: 428
Size: 319291 Color: 253
Size: 266548 Color: 94

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 466577 Color: 473
Size: 280732 Color: 143
Size: 252692 Color: 16

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 350567 Color: 323
Size: 336313 Color: 292
Size: 313121 Color: 238

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 378764 Color: 374
Size: 323233 Color: 266
Size: 298004 Color: 197

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 404380 Color: 411
Size: 300371 Color: 206
Size: 295250 Color: 189

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 408765 Color: 419
Size: 341088 Color: 305
Size: 250148 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 434366 Color: 451
Size: 293761 Color: 182
Size: 271874 Color: 109

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 365698 Color: 349
Size: 355609 Color: 335
Size: 278694 Color: 136

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 353966 Color: 331
Size: 326324 Color: 274
Size: 319711 Color: 254

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 427477 Color: 439
Size: 321446 Color: 258
Size: 251078 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 404840 Color: 412
Size: 337273 Color: 296
Size: 257888 Color: 48

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 448908 Color: 459
Size: 286208 Color: 160
Size: 264885 Color: 80

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 338107 Color: 300
Size: 336363 Color: 293
Size: 325531 Color: 271

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 380731 Color: 376
Size: 342499 Color: 307
Size: 276771 Color: 131

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 368498 Color: 357
Size: 365751 Color: 350
Size: 265752 Color: 88

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 391620 Color: 390
Size: 333335 Color: 288
Size: 275046 Color: 119

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 472572 Color: 482
Size: 271191 Color: 108
Size: 256238 Color: 39

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 377220 Color: 371
Size: 349198 Color: 321
Size: 273583 Color: 115

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 376719 Color: 370
Size: 320080 Color: 255
Size: 303202 Color: 214

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 368363 Color: 355
Size: 333513 Color: 289
Size: 298125 Color: 199

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 402141 Color: 407
Size: 309077 Color: 232
Size: 288783 Color: 169

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 372944 Color: 367
Size: 356551 Color: 338
Size: 270506 Color: 106

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 412350 Color: 426
Size: 302903 Color: 213
Size: 284748 Color: 156

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 427635 Color: 440
Size: 321151 Color: 256
Size: 251215 Color: 5

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 429579 Color: 445
Size: 298044 Color: 198
Size: 272378 Color: 111

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 447627 Color: 456
Size: 276788 Color: 132
Size: 275586 Color: 122

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 468610 Color: 478
Size: 276283 Color: 126
Size: 255108 Color: 32

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 468622 Color: 479
Size: 269104 Color: 102
Size: 262275 Color: 68

Total size: 167000167
Total free space: 0

