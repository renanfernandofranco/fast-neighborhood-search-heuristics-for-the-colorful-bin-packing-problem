Capicity Bin: 15328
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 437
Size: 5272 Color: 378
Size: 336 Color: 47

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 466
Size: 3530 Color: 337
Size: 766 Color: 151

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11338 Color: 472
Size: 3326 Color: 334
Size: 664 Color: 137

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 473
Size: 3320 Color: 333
Size: 656 Color: 136

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 486
Size: 1940 Color: 251
Size: 1540 Color: 226

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 489
Size: 2844 Color: 311
Size: 584 Color: 123

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11913 Color: 490
Size: 3007 Color: 322
Size: 408 Color: 72

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11915 Color: 491
Size: 2845 Color: 312
Size: 568 Color: 119

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11932 Color: 493
Size: 2860 Color: 314
Size: 536 Color: 110

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12069 Color: 496
Size: 2623 Color: 302
Size: 636 Color: 131

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12149 Color: 500
Size: 2717 Color: 307
Size: 462 Color: 91

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 501
Size: 2008 Color: 259
Size: 1168 Color: 193

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12274 Color: 506
Size: 2212 Color: 270
Size: 842 Color: 161

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 508
Size: 2096 Color: 264
Size: 908 Color: 170

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 509
Size: 1520 Color: 223
Size: 1464 Color: 217

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12354 Color: 510
Size: 1610 Color: 231
Size: 1364 Color: 209

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12367 Color: 511
Size: 2469 Color: 292
Size: 492 Color: 101

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 512
Size: 2434 Color: 288
Size: 524 Color: 107

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12386 Color: 513
Size: 2482 Color: 293
Size: 460 Color: 90

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12524 Color: 520
Size: 2244 Color: 275
Size: 560 Color: 115

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 521
Size: 1983 Color: 256
Size: 800 Color: 157

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12562 Color: 522
Size: 2486 Color: 294
Size: 280 Color: 23

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 524
Size: 2488 Color: 295
Size: 272 Color: 20

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 528
Size: 2232 Color: 274
Size: 452 Color: 88

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 529
Size: 1964 Color: 253
Size: 706 Color: 144

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12663 Color: 530
Size: 2221 Color: 272
Size: 444 Color: 84

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12674 Color: 532
Size: 2214 Color: 271
Size: 440 Color: 83

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 534
Size: 2340 Color: 283
Size: 280 Color: 26

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 545
Size: 2152 Color: 267
Size: 240 Color: 9

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12937 Color: 546
Size: 1903 Color: 247
Size: 488 Color: 98

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 548
Size: 1982 Color: 255
Size: 392 Color: 64

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 549
Size: 1480 Color: 218
Size: 892 Color: 168

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 556
Size: 1272 Color: 198
Size: 1008 Color: 181

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 557
Size: 1898 Color: 246
Size: 376 Color: 56

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 559
Size: 1602 Color: 229
Size: 608 Color: 127

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13186 Color: 561
Size: 1746 Color: 239
Size: 396 Color: 67

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 564
Size: 1546 Color: 227
Size: 548 Color: 114

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 569
Size: 1272 Color: 199
Size: 690 Color: 139

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 570
Size: 1000 Color: 178
Size: 960 Color: 176

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 572
Size: 1486 Color: 220
Size: 444 Color: 85

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 575
Size: 1390 Color: 211
Size: 464 Color: 92

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 578
Size: 1534 Color: 225
Size: 304 Color: 38

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 580
Size: 1528 Color: 224
Size: 288 Color: 27

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13546 Color: 582
Size: 1358 Color: 208
Size: 424 Color: 77

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13550 Color: 583
Size: 1482 Color: 219
Size: 296 Color: 32

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13598 Color: 585
Size: 1314 Color: 205
Size: 416 Color: 74

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 588
Size: 1388 Color: 210
Size: 304 Color: 37

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13642 Color: 589
Size: 1406 Color: 212
Size: 280 Color: 22

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 590
Size: 1264 Color: 196
Size: 396 Color: 68

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13670 Color: 591
Size: 1444 Color: 216
Size: 214 Color: 7

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 592
Size: 976 Color: 177
Size: 680 Color: 138

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 593
Size: 1308 Color: 204
Size: 320 Color: 43

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13734 Color: 596
Size: 842 Color: 162
Size: 752 Color: 149

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 597
Size: 1016 Color: 183
Size: 576 Color: 121

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13754 Color: 598
Size: 1302 Color: 203
Size: 272 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 599
Size: 1072 Color: 187
Size: 492 Color: 99

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13766 Color: 600
Size: 1032 Color: 184
Size: 530 Color: 109

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 463
Size: 2312 Color: 280
Size: 2047 Color: 262

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 11511 Color: 478
Size: 3816 Color: 345

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 12402 Color: 515
Size: 2925 Color: 321

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 523
Size: 2466 Color: 291
Size: 296 Color: 33

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12949 Color: 547
Size: 1980 Color: 254
Size: 398 Color: 69

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13004 Color: 552
Size: 2323 Color: 282

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13045 Color: 555
Size: 2282 Color: 277

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 13406 Color: 574
Size: 1921 Color: 250

Bin 66: 2 of cap free
Amount of items: 7
Items: 
Size: 7670 Color: 409
Size: 1694 Color: 236
Size: 1676 Color: 235
Size: 1640 Color: 234
Size: 1638 Color: 233
Size: 568 Color: 116
Size: 440 Color: 82

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 10026 Color: 445
Size: 5004 Color: 373
Size: 296 Color: 34

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 10500 Color: 453
Size: 4562 Color: 366
Size: 264 Color: 16

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 11820 Color: 485
Size: 3114 Color: 326
Size: 392 Color: 66

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 11870 Color: 488
Size: 2836 Color: 310
Size: 620 Color: 129

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 12230 Color: 503
Size: 3096 Color: 325

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 504
Size: 2882 Color: 315
Size: 192 Color: 5

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12293 Color: 507
Size: 3033 Color: 323

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 12479 Color: 519
Size: 2847 Color: 313

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12900 Color: 544
Size: 2426 Color: 287

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13023 Color: 553
Size: 2303 Color: 279

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13298 Color: 567
Size: 2028 Color: 261

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13484 Color: 577
Size: 1842 Color: 243

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 9660 Color: 436
Size: 5665 Color: 386

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 11594 Color: 480
Size: 3539 Color: 338
Size: 192 Color: 4

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 12181 Color: 502
Size: 3144 Color: 327

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 12664 Color: 531
Size: 2661 Color: 304

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13033 Color: 554
Size: 2292 Color: 278

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 8788 Color: 422
Size: 6152 Color: 390
Size: 384 Color: 60

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 11179 Color: 470
Size: 4145 Color: 353

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 12056 Color: 495
Size: 3268 Color: 331

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 517
Size: 2906 Color: 319

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 12676 Color: 533
Size: 2648 Color: 303

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 536
Size: 2564 Color: 300

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 12873 Color: 542
Size: 2451 Color: 290

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 12972 Color: 550
Size: 2352 Color: 284

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 571
Size: 1942 Color: 252

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13404 Color: 573
Size: 1920 Color: 249

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13504 Color: 579
Size: 1820 Color: 242

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 581
Size: 1784 Color: 241

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 584
Size: 1764 Color: 240

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13620 Color: 587
Size: 1704 Color: 237

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 13702 Color: 594
Size: 1622 Color: 232

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 595
Size: 1604 Color: 230

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 13002 Color: 551
Size: 2321 Color: 281

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 565
Size: 2053 Color: 263

Bin 102: 6 of cap free
Amount of items: 9
Items: 
Size: 7666 Color: 406
Size: 1272 Color: 197
Size: 1264 Color: 195
Size: 1248 Color: 194
Size: 1132 Color: 192
Size: 1128 Color: 191
Size: 648 Color: 134
Size: 484 Color: 95
Size: 480 Color: 94

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 12426 Color: 518
Size: 2896 Color: 317

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 525
Size: 2742 Color: 309

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 12594 Color: 526
Size: 2728 Color: 308

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 539
Size: 2531 Color: 297

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13476 Color: 576
Size: 1846 Color: 244

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 586
Size: 1718 Color: 238

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 11916 Color: 492
Size: 3405 Color: 335

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 12775 Color: 538
Size: 2546 Color: 298

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 562
Size: 2129 Color: 266

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 11030 Color: 465
Size: 2394 Color: 285
Size: 1896 Color: 245

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 12038 Color: 494
Size: 3282 Color: 332

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 12898 Color: 543
Size: 2422 Color: 286

Bin 115: 9 of cap free
Amount of items: 3
Items: 
Size: 11412 Color: 475
Size: 3811 Color: 344
Size: 96 Color: 1

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 12137 Color: 499
Size: 3181 Color: 328

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 12256 Color: 505
Size: 3062 Color: 324

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 563
Size: 2106 Color: 265

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 11858 Color: 487
Size: 3459 Color: 336

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 527
Size: 2690 Color: 305

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 568
Size: 1993 Color: 257

Bin 122: 12 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 467
Size: 4058 Color: 352
Size: 224 Color: 8

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 537
Size: 2546 Color: 299

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 11083 Color: 468
Size: 4024 Color: 350
Size: 208 Color: 6

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 558
Size: 2251 Color: 276

Bin 126: 14 of cap free
Amount of items: 3
Items: 
Size: 10924 Color: 462
Size: 2894 Color: 316
Size: 1496 Color: 222

Bin 127: 14 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 469
Size: 4028 Color: 351
Size: 192 Color: 3

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 12102 Color: 498
Size: 3212 Color: 329

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 12410 Color: 516
Size: 2904 Color: 318

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 566
Size: 2026 Color: 260

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 10666 Color: 456
Size: 4647 Color: 368

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 11476 Color: 477
Size: 3837 Color: 347

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 484
Size: 3592 Color: 340

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 12389 Color: 514
Size: 2924 Color: 320

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 10988 Color: 464
Size: 4324 Color: 359

Bin 136: 17 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 560
Size: 2163 Color: 268

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 12733 Color: 535
Size: 2577 Color: 301

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 12802 Color: 540
Size: 2508 Color: 296

Bin 139: 19 of cap free
Amount of items: 2
Items: 
Size: 11689 Color: 483
Size: 3620 Color: 341

Bin 140: 20 of cap free
Amount of items: 3
Items: 
Size: 10312 Color: 450
Size: 4724 Color: 370
Size: 272 Color: 21

Bin 141: 21 of cap free
Amount of items: 3
Items: 
Size: 9922 Color: 441
Size: 5065 Color: 376
Size: 320 Color: 42

Bin 142: 21 of cap free
Amount of items: 2
Items: 
Size: 12865 Color: 541
Size: 2442 Color: 289

Bin 143: 22 of cap free
Amount of items: 23
Items: 
Size: 832 Color: 159
Size: 824 Color: 158
Size: 800 Color: 156
Size: 800 Color: 155
Size: 776 Color: 154
Size: 776 Color: 153
Size: 766 Color: 152
Size: 760 Color: 150
Size: 728 Color: 148
Size: 720 Color: 147
Size: 720 Color: 146
Size: 656 Color: 135
Size: 600 Color: 125
Size: 592 Color: 124
Size: 576 Color: 122
Size: 576 Color: 120
Size: 568 Color: 118
Size: 568 Color: 117
Size: 544 Color: 113
Size: 542 Color: 112
Size: 542 Color: 111
Size: 528 Color: 108
Size: 512 Color: 106

Bin 144: 22 of cap free
Amount of items: 5
Items: 
Size: 7678 Color: 412
Size: 6308 Color: 391
Size: 456 Color: 89
Size: 432 Color: 79
Size: 432 Color: 78

Bin 145: 22 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 420
Size: 6380 Color: 396
Size: 384 Color: 61

Bin 146: 22 of cap free
Amount of items: 3
Items: 
Size: 9262 Color: 432
Size: 5692 Color: 387
Size: 352 Color: 51

Bin 147: 24 of cap free
Amount of items: 4
Items: 
Size: 7692 Color: 413
Size: 6768 Color: 401
Size: 424 Color: 76
Size: 420 Color: 75

Bin 148: 26 of cap free
Amount of items: 7
Items: 
Size: 7668 Color: 408
Size: 1548 Color: 228
Size: 1492 Color: 221
Size: 1442 Color: 215
Size: 1428 Color: 214
Size: 1276 Color: 202
Size: 448 Color: 86

Bin 149: 28 of cap free
Amount of items: 3
Items: 
Size: 8538 Color: 419
Size: 6378 Color: 395
Size: 384 Color: 62

Bin 150: 28 of cap free
Amount of items: 3
Items: 
Size: 9512 Color: 435
Size: 5452 Color: 382
Size: 336 Color: 48

Bin 151: 28 of cap free
Amount of items: 3
Items: 
Size: 10148 Color: 446
Size: 4856 Color: 371
Size: 296 Color: 31

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 11624 Color: 481
Size: 3676 Color: 343

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 12073 Color: 497
Size: 3227 Color: 330

Bin 154: 30 of cap free
Amount of items: 2
Items: 
Size: 11658 Color: 482
Size: 3640 Color: 342

Bin 155: 35 of cap free
Amount of items: 3
Items: 
Size: 11243 Color: 471
Size: 3886 Color: 348
Size: 164 Color: 2

Bin 156: 38 of cap free
Amount of items: 2
Items: 
Size: 11457 Color: 476
Size: 3833 Color: 346

Bin 157: 40 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 451
Size: 4680 Color: 369
Size: 268 Color: 18

Bin 158: 40 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 455
Size: 2713 Color: 306
Size: 1913 Color: 248

Bin 159: 41 of cap free
Amount of items: 4
Items: 
Size: 10273 Color: 448
Size: 4446 Color: 363
Size: 288 Color: 28
Size: 280 Color: 25

Bin 160: 43 of cap free
Amount of items: 4
Items: 
Size: 10269 Color: 447
Size: 4440 Color: 362
Size: 288 Color: 30
Size: 288 Color: 29

Bin 161: 44 of cap free
Amount of items: 3
Items: 
Size: 9133 Color: 428
Size: 5783 Color: 388
Size: 368 Color: 55

Bin 162: 48 of cap free
Amount of items: 2
Items: 
Size: 11390 Color: 474
Size: 3890 Color: 349

Bin 163: 49 of cap free
Amount of items: 3
Items: 
Size: 8531 Color: 418
Size: 6364 Color: 394
Size: 384 Color: 63

Bin 164: 51 of cap free
Amount of items: 3
Items: 
Size: 8529 Color: 417
Size: 6356 Color: 393
Size: 392 Color: 65

Bin 165: 54 of cap free
Amount of items: 3
Items: 
Size: 9258 Color: 431
Size: 5664 Color: 385
Size: 352 Color: 52

Bin 166: 54 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 454
Size: 4506 Color: 365
Size: 264 Color: 15

Bin 167: 55 of cap free
Amount of items: 3
Items: 
Size: 9251 Color: 430
Size: 5662 Color: 384
Size: 360 Color: 53

Bin 168: 56 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 427
Size: 5876 Color: 389
Size: 380 Color: 57

Bin 169: 68 of cap free
Amount of items: 2
Items: 
Size: 7676 Color: 411
Size: 7584 Color: 404

Bin 170: 70 of cap free
Amount of items: 3
Items: 
Size: 10018 Color: 444
Size: 4936 Color: 372
Size: 304 Color: 35

Bin 171: 76 of cap free
Amount of items: 2
Items: 
Size: 8860 Color: 425
Size: 6392 Color: 400

Bin 172: 80 of cap free
Amount of items: 3
Items: 
Size: 10498 Color: 452
Size: 4482 Color: 364
Size: 268 Color: 17

Bin 173: 84 of cap free
Amount of items: 3
Items: 
Size: 9753 Color: 438
Size: 5163 Color: 377
Size: 328 Color: 46

Bin 174: 84 of cap free
Amount of items: 3
Items: 
Size: 9858 Color: 440
Size: 5062 Color: 375
Size: 324 Color: 44

Bin 175: 85 of cap free
Amount of items: 8
Items: 
Size: 7667 Color: 407
Size: 1412 Color: 213
Size: 1352 Color: 207
Size: 1330 Color: 206
Size: 1276 Color: 201
Size: 1276 Color: 200
Size: 480 Color: 93
Size: 450 Color: 87

Bin 176: 90 of cap free
Amount of items: 3
Items: 
Size: 9212 Color: 429
Size: 5658 Color: 383
Size: 368 Color: 54

Bin 177: 90 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 479
Size: 3582 Color: 339
Size: 96 Color: 0

Bin 178: 92 of cap free
Amount of items: 19
Items: 
Size: 1008 Color: 180
Size: 1008 Color: 179
Size: 944 Color: 175
Size: 928 Color: 174
Size: 928 Color: 173
Size: 928 Color: 172
Size: 912 Color: 171
Size: 900 Color: 169
Size: 884 Color: 167
Size: 880 Color: 166
Size: 880 Color: 165
Size: 856 Color: 164
Size: 848 Color: 163
Size: 836 Color: 160
Size: 508 Color: 105
Size: 504 Color: 104
Size: 496 Color: 103
Size: 496 Color: 102
Size: 492 Color: 100

Bin 179: 95 of cap free
Amount of items: 3
Items: 
Size: 9851 Color: 439
Size: 5058 Color: 374
Size: 324 Color: 45

Bin 180: 97 of cap free
Amount of items: 2
Items: 
Size: 8844 Color: 424
Size: 6387 Color: 399

Bin 181: 97 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 461
Size: 4217 Color: 358
Size: 254 Color: 10

Bin 182: 102 of cap free
Amount of items: 3
Items: 
Size: 10757 Color: 460
Size: 4213 Color: 357
Size: 256 Color: 11

Bin 183: 119 of cap free
Amount of items: 2
Items: 
Size: 8824 Color: 423
Size: 6385 Color: 398

Bin 184: 140 of cap free
Amount of items: 3
Items: 
Size: 9416 Color: 434
Size: 5432 Color: 381
Size: 340 Color: 49

Bin 185: 152 of cap free
Amount of items: 3
Items: 
Size: 10729 Color: 459
Size: 4191 Color: 356
Size: 256 Color: 12

Bin 186: 159 of cap free
Amount of items: 3
Items: 
Size: 10725 Color: 458
Size: 4184 Color: 355
Size: 260 Color: 13

Bin 187: 182 of cap free
Amount of items: 3
Items: 
Size: 10301 Color: 449
Size: 4565 Color: 367
Size: 280 Color: 24

Bin 188: 201 of cap free
Amount of items: 3
Items: 
Size: 10703 Color: 457
Size: 4164 Color: 354
Size: 260 Color: 14

Bin 189: 214 of cap free
Amount of items: 2
Items: 
Size: 8732 Color: 421
Size: 6382 Color: 397

Bin 190: 216 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 415
Size: 6956 Color: 403
Size: 404 Color: 71

Bin 191: 252 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 433
Size: 5404 Color: 380
Size: 348 Color: 50

Bin 192: 282 of cap free
Amount of items: 4
Items: 
Size: 10008 Color: 443
Size: 4426 Color: 361
Size: 308 Color: 39
Size: 304 Color: 36

Bin 193: 290 of cap free
Amount of items: 4
Items: 
Size: 8920 Color: 426
Size: 5352 Color: 379
Size: 384 Color: 59
Size: 382 Color: 58

Bin 194: 291 of cap free
Amount of items: 9
Items: 
Size: 7665 Color: 405
Size: 1128 Color: 190
Size: 1088 Color: 189
Size: 1080 Color: 188
Size: 1056 Color: 186
Size: 1040 Color: 185
Size: 1012 Color: 182
Size: 484 Color: 97
Size: 484 Color: 96

Bin 195: 312 of cap free
Amount of items: 4
Items: 
Size: 9954 Color: 442
Size: 4422 Color: 360
Size: 320 Color: 41
Size: 320 Color: 40

Bin 196: 348 of cap free
Amount of items: 3
Items: 
Size: 7708 Color: 414
Size: 6862 Color: 402
Size: 410 Color: 73

Bin 197: 352 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 416
Size: 6328 Color: 392
Size: 400 Color: 70

Bin 198: 362 of cap free
Amount of items: 6
Items: 
Size: 7672 Color: 410
Size: 2226 Color: 273
Size: 2188 Color: 269
Size: 2008 Color: 258
Size: 440 Color: 81
Size: 432 Color: 80

Bin 199: 8678 of cap free
Amount of items: 10
Items: 
Size: 712 Color: 145
Size: 704 Color: 143
Size: 704 Color: 142
Size: 704 Color: 141
Size: 704 Color: 140
Size: 644 Color: 133
Size: 640 Color: 132
Size: 624 Color: 130
Size: 608 Color: 128
Size: 606 Color: 126

Total size: 3034944
Total free space: 15328

