Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 410852 Color: 2
Size: 309465 Color: 0
Size: 279684 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 346092 Color: 1
Size: 317312 Color: 0
Size: 336597 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 378616 Color: 2
Size: 364580 Color: 3
Size: 256805 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 345101 Color: 3
Size: 333451 Color: 2
Size: 321449 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370577 Color: 2
Size: 359730 Color: 4
Size: 269694 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 456256 Color: 0
Size: 278813 Color: 1
Size: 264932 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 409607 Color: 4
Size: 331687 Color: 0
Size: 258707 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 356952 Color: 2
Size: 390497 Color: 3
Size: 252552 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 498387 Color: 2
Size: 251097 Color: 1
Size: 250517 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 495218 Color: 3
Size: 253290 Color: 4
Size: 251493 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 496330 Color: 3
Size: 253336 Color: 3
Size: 250335 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 322475 Color: 0
Size: 374304 Color: 0
Size: 303222 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 362525 Color: 0
Size: 337858 Color: 4
Size: 299618 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 404513 Color: 1
Size: 333529 Color: 4
Size: 261959 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 415209 Color: 4
Size: 298356 Color: 1
Size: 286436 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 392777 Color: 1
Size: 336968 Color: 3
Size: 270256 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 351078 Color: 1
Size: 345071 Color: 3
Size: 303852 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 348883 Color: 3
Size: 343469 Color: 2
Size: 307649 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 448786 Color: 1
Size: 295585 Color: 3
Size: 255630 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 471463 Color: 4
Size: 274172 Color: 3
Size: 254366 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 387860 Color: 0
Size: 324153 Color: 3
Size: 287988 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 464735 Color: 4
Size: 269460 Color: 4
Size: 265806 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 416778 Color: 4
Size: 296057 Color: 2
Size: 287166 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 406146 Color: 3
Size: 330500 Color: 0
Size: 263355 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 405189 Color: 0
Size: 331346 Color: 2
Size: 263466 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 433856 Color: 3
Size: 292401 Color: 3
Size: 273744 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 339956 Color: 1
Size: 332217 Color: 1
Size: 327828 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 456057 Color: 0
Size: 272455 Color: 2
Size: 271489 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 362355 Color: 4
Size: 339732 Color: 2
Size: 297914 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 449588 Color: 1
Size: 292000 Color: 3
Size: 258413 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 420858 Color: 1
Size: 299626 Color: 0
Size: 279517 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 477393 Color: 0
Size: 262412 Color: 1
Size: 260196 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 338982 Color: 1
Size: 336091 Color: 3
Size: 324928 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 496386 Color: 0
Size: 252030 Color: 0
Size: 251585 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 412125 Color: 2
Size: 299729 Color: 4
Size: 288147 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 375420 Color: 1
Size: 323431 Color: 0
Size: 301150 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 446147 Color: 1
Size: 296271 Color: 0
Size: 257583 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 424539 Color: 0
Size: 318391 Color: 2
Size: 257071 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 380241 Color: 2
Size: 367205 Color: 1
Size: 252555 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 413016 Color: 1
Size: 333376 Color: 3
Size: 253609 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 374722 Color: 0
Size: 354897 Color: 1
Size: 270382 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 438188 Color: 3
Size: 307166 Color: 1
Size: 254647 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 423483 Color: 4
Size: 320648 Color: 2
Size: 255870 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 491244 Color: 4
Size: 258517 Color: 3
Size: 250240 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 389914 Color: 3
Size: 342443 Color: 2
Size: 267644 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 426569 Color: 4
Size: 304119 Color: 4
Size: 269313 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 416298 Color: 4
Size: 302647 Color: 3
Size: 281056 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 425651 Color: 1
Size: 322890 Color: 0
Size: 251460 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 458680 Color: 2
Size: 285460 Color: 3
Size: 255861 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 378797 Color: 2
Size: 325986 Color: 4
Size: 295218 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 478042 Color: 3
Size: 263810 Color: 1
Size: 258149 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 355359 Color: 1
Size: 346586 Color: 0
Size: 298056 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 413764 Color: 1
Size: 295229 Color: 2
Size: 291008 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 422776 Color: 4
Size: 301344 Color: 4
Size: 275881 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 358112 Color: 4
Size: 327260 Color: 4
Size: 314629 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 451997 Color: 2
Size: 287555 Color: 4
Size: 260449 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 422554 Color: 1
Size: 325382 Color: 2
Size: 252065 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 466202 Color: 0
Size: 275887 Color: 2
Size: 257912 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 462796 Color: 3
Size: 276919 Color: 2
Size: 260286 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 469726 Color: 4
Size: 271905 Color: 0
Size: 258370 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 347666 Color: 1
Size: 330323 Color: 2
Size: 322012 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 368868 Color: 1
Size: 330638 Color: 3
Size: 300495 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 395623 Color: 4
Size: 334681 Color: 4
Size: 269697 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 372434 Color: 2
Size: 328098 Color: 4
Size: 299469 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 430030 Color: 2
Size: 293497 Color: 1
Size: 276474 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 364053 Color: 4
Size: 323767 Color: 0
Size: 312181 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 481539 Color: 0
Size: 261081 Color: 3
Size: 257381 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 357603 Color: 4
Size: 332467 Color: 2
Size: 309931 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 364224 Color: 4
Size: 324474 Color: 3
Size: 311303 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 392832 Color: 2
Size: 318467 Color: 3
Size: 288702 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 353511 Color: 4
Size: 330486 Color: 3
Size: 316004 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 468925 Color: 1
Size: 278011 Color: 4
Size: 253065 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 431462 Color: 4
Size: 305566 Color: 2
Size: 262973 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 383733 Color: 0
Size: 321722 Color: 2
Size: 294546 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 478860 Color: 2
Size: 269990 Color: 0
Size: 251151 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 418396 Color: 0
Size: 295400 Color: 0
Size: 286205 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 407434 Color: 1
Size: 313208 Color: 3
Size: 279359 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 467056 Color: 0
Size: 273255 Color: 2
Size: 259690 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 408165 Color: 3
Size: 341049 Color: 1
Size: 250787 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 365767 Color: 4
Size: 356880 Color: 1
Size: 277354 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 462065 Color: 2
Size: 277348 Color: 1
Size: 260588 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 465886 Color: 2
Size: 267327 Color: 3
Size: 266788 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 363001 Color: 4
Size: 357735 Color: 1
Size: 279265 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 412945 Color: 3
Size: 297009 Color: 0
Size: 290047 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 360183 Color: 2
Size: 326196 Color: 3
Size: 313622 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 395446 Color: 2
Size: 310528 Color: 4
Size: 294027 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 360300 Color: 4
Size: 349376 Color: 0
Size: 290325 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 361621 Color: 4
Size: 337457 Color: 2
Size: 300923 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 442060 Color: 2
Size: 296148 Color: 1
Size: 261793 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 418937 Color: 1
Size: 326006 Color: 2
Size: 255058 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 398577 Color: 1
Size: 307896 Color: 1
Size: 293528 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 364890 Color: 1
Size: 318871 Color: 3
Size: 316240 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 418709 Color: 3
Size: 308811 Color: 3
Size: 272481 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 397668 Color: 2
Size: 352215 Color: 2
Size: 250118 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 468658 Color: 0
Size: 273654 Color: 3
Size: 257689 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 438332 Color: 0
Size: 281901 Color: 2
Size: 279768 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 395876 Color: 1
Size: 351526 Color: 1
Size: 252599 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 443105 Color: 1
Size: 306745 Color: 1
Size: 250151 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 432395 Color: 4
Size: 312997 Color: 3
Size: 254609 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 374568 Color: 4
Size: 319669 Color: 3
Size: 305764 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 428957 Color: 0
Size: 318330 Color: 3
Size: 252714 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 486040 Color: 0
Size: 257749 Color: 2
Size: 256212 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 423173 Color: 1
Size: 294524 Color: 3
Size: 282304 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 441452 Color: 1
Size: 300712 Color: 2
Size: 257837 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 420346 Color: 4
Size: 310095 Color: 2
Size: 269560 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 415083 Color: 4
Size: 329836 Color: 2
Size: 255082 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 398684 Color: 4
Size: 319005 Color: 3
Size: 282312 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 494146 Color: 3
Size: 254086 Color: 2
Size: 251769 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 395070 Color: 1
Size: 338193 Color: 4
Size: 266738 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 457965 Color: 1
Size: 288718 Color: 4
Size: 253318 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 471703 Color: 0
Size: 266801 Color: 4
Size: 261497 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 441454 Color: 2
Size: 294714 Color: 2
Size: 263833 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 498547 Color: 4
Size: 251364 Color: 4
Size: 250090 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 402264 Color: 4
Size: 341804 Color: 2
Size: 255933 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 429591 Color: 0
Size: 307785 Color: 4
Size: 262625 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 499719 Color: 1
Size: 250149 Color: 2
Size: 250133 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 466719 Color: 1
Size: 281547 Color: 4
Size: 251735 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 402614 Color: 3
Size: 320577 Color: 0
Size: 276810 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 445422 Color: 2
Size: 283238 Color: 3
Size: 271341 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 384720 Color: 0
Size: 331462 Color: 0
Size: 283819 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 397239 Color: 0
Size: 312379 Color: 2
Size: 290383 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 417315 Color: 2
Size: 310838 Color: 3
Size: 271848 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 492242 Color: 1
Size: 254395 Color: 3
Size: 253364 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 369077 Color: 1
Size: 330814 Color: 0
Size: 300110 Color: 2

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 474472 Color: 1
Size: 274713 Color: 3
Size: 250816 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 399724 Color: 3
Size: 313933 Color: 3
Size: 286344 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 349172 Color: 3
Size: 343710 Color: 0
Size: 307119 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 452046 Color: 2
Size: 279301 Color: 3
Size: 268654 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 372448 Color: 2
Size: 350775 Color: 1
Size: 276778 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 355212 Color: 2
Size: 337881 Color: 1
Size: 306908 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 408354 Color: 0
Size: 307880 Color: 3
Size: 283767 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 447664 Color: 3
Size: 301342 Color: 1
Size: 250995 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 486295 Color: 1
Size: 260002 Color: 0
Size: 253704 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 494849 Color: 0
Size: 252837 Color: 2
Size: 252315 Color: 4

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 425562 Color: 3
Size: 316060 Color: 1
Size: 258379 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 372057 Color: 4
Size: 318836 Color: 4
Size: 309108 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 490340 Color: 3
Size: 258510 Color: 0
Size: 251151 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 377229 Color: 0
Size: 355430 Color: 1
Size: 267342 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 433049 Color: 2
Size: 297340 Color: 0
Size: 269612 Color: 2

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 369158 Color: 2
Size: 341377 Color: 4
Size: 289466 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 346245 Color: 1
Size: 328294 Color: 2
Size: 325462 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 471002 Color: 0
Size: 275847 Color: 3
Size: 253152 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 365122 Color: 2
Size: 348558 Color: 4
Size: 286321 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 400762 Color: 2
Size: 309255 Color: 1
Size: 289984 Color: 3

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 416220 Color: 1
Size: 307770 Color: 2
Size: 276011 Color: 3

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 473159 Color: 4
Size: 268832 Color: 3
Size: 258010 Color: 4

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 399179 Color: 3
Size: 318550 Color: 1
Size: 282272 Color: 2

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 350775 Color: 4
Size: 342374 Color: 1
Size: 306852 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 360768 Color: 1
Size: 340259 Color: 4
Size: 298974 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 392049 Color: 1
Size: 310470 Color: 3
Size: 297482 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 393939 Color: 3
Size: 331407 Color: 2
Size: 274655 Color: 4

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 480284 Color: 0
Size: 263486 Color: 4
Size: 256231 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 369480 Color: 1
Size: 315870 Color: 4
Size: 314651 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 464189 Color: 2
Size: 273077 Color: 1
Size: 262735 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 396811 Color: 0
Size: 341243 Color: 1
Size: 261947 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 464298 Color: 1
Size: 270765 Color: 3
Size: 264938 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 443204 Color: 2
Size: 288938 Color: 4
Size: 267859 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 383452 Color: 1
Size: 333468 Color: 2
Size: 283081 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 443218 Color: 4
Size: 305457 Color: 1
Size: 251326 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 395485 Color: 3
Size: 346986 Color: 0
Size: 257530 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 443451 Color: 4
Size: 298829 Color: 2
Size: 257721 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 416865 Color: 1
Size: 306379 Color: 0
Size: 276757 Color: 2

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 421622 Color: 3
Size: 299841 Color: 1
Size: 278538 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 421390 Color: 0
Size: 291105 Color: 0
Size: 287506 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 374073 Color: 0
Size: 370204 Color: 3
Size: 255724 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 395627 Color: 4
Size: 310377 Color: 4
Size: 293997 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 477607 Color: 1
Size: 266749 Color: 2
Size: 255645 Color: 1

Total size: 167000167
Total free space: 0

