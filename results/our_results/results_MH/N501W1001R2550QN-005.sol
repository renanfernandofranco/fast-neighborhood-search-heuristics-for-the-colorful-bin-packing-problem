Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 426
Size: 329 Color: 282
Size: 253 Color: 21

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 379
Size: 312 Color: 243
Size: 301 Color: 209

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 492
Size: 262 Color: 58
Size: 252 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 391
Size: 306 Color: 224
Size: 300 Color: 203

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 395
Size: 328 Color: 280
Size: 273 Color: 116

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 489
Size: 260 Color: 52
Size: 260 Color: 51

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 394
Size: 328 Color: 281
Size: 274 Color: 119

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 334
Size: 334 Color: 292
Size: 311 Color: 241

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 441
Size: 303 Color: 219
Size: 268 Color: 91

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 494
Size: 255 Color: 31
Size: 254 Color: 22

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 414
Size: 337 Color: 300
Size: 253 Color: 20

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 474
Size: 273 Color: 117
Size: 267 Color: 82

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 337
Size: 355 Color: 333
Size: 289 Color: 167

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 424
Size: 295 Color: 189
Size: 289 Color: 169

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 458
Size: 299 Color: 200
Size: 258 Color: 45

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 346
Size: 267 Color: 85
Size: 369 Color: 352

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 453
Size: 293 Color: 184
Size: 268 Color: 95

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 360
Size: 350 Color: 326
Size: 273 Color: 114

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 451
Size: 312 Color: 244
Size: 251 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 422
Size: 326 Color: 278
Size: 258 Color: 43

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 404
Size: 330 Color: 284
Size: 265 Color: 72

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 369
Size: 346 Color: 319
Size: 272 Color: 110

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 435
Size: 306 Color: 226
Size: 267 Color: 88

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 392
Size: 339 Color: 305
Size: 265 Color: 76

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 459
Size: 293 Color: 185
Size: 263 Color: 65

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 416
Size: 339 Color: 306
Size: 250 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 330
Size: 339 Color: 303
Size: 310 Color: 237

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 384
Size: 332 Color: 289
Size: 277 Color: 134

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 403
Size: 299 Color: 199
Size: 297 Color: 193

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 343
Size: 334 Color: 291
Size: 304 Color: 222

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 354
Size: 332 Color: 288
Size: 299 Color: 201

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 364
Size: 354 Color: 332
Size: 268 Color: 93

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 417
Size: 337 Color: 299
Size: 250 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 378
Size: 349 Color: 322
Size: 265 Color: 78

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 447
Size: 308 Color: 236
Size: 259 Color: 49

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 478
Size: 284 Color: 159
Size: 250 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 493
Size: 257 Color: 41
Size: 257 Color: 40

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 470
Size: 273 Color: 113
Size: 271 Color: 108

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 443
Size: 303 Color: 216
Size: 267 Color: 89

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 376
Size: 349 Color: 323
Size: 265 Color: 75

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 412
Size: 336 Color: 297
Size: 255 Color: 29

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 386
Size: 324 Color: 272
Size: 283 Color: 154

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 353
Size: 369 Color: 351
Size: 263 Color: 64

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 461
Size: 290 Color: 175
Size: 262 Color: 57

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 409
Size: 315 Color: 255
Size: 277 Color: 131

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 491
Size: 265 Color: 74
Size: 250 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 340
Size: 338 Color: 302
Size: 304 Color: 220

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 350
Size: 366 Color: 349
Size: 267 Color: 87

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 405
Size: 330 Color: 285
Size: 265 Color: 73

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 307
Size: 338 Color: 301
Size: 324 Color: 273

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 406
Size: 321 Color: 267
Size: 273 Color: 115

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 398
Size: 323 Color: 271
Size: 276 Color: 126

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 476
Size: 271 Color: 104
Size: 268 Color: 92

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 373
Size: 315 Color: 253
Size: 300 Color: 205

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 425
Size: 305 Color: 223
Size: 278 Color: 138

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 439
Size: 301 Color: 212
Size: 270 Color: 102

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 429
Size: 324 Color: 275
Size: 254 Color: 23

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 372
Size: 308 Color: 235
Size: 307 Color: 232

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 471
Size: 277 Color: 132
Size: 267 Color: 84

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 465
Size: 283 Color: 152
Size: 267 Color: 83

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 497
Size: 254 Color: 24
Size: 252 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 344
Size: 334 Color: 290
Size: 304 Color: 221

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 363
Size: 312 Color: 247
Size: 310 Color: 238

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 450
Size: 289 Color: 172
Size: 275 Color: 123

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 448
Size: 284 Color: 156
Size: 283 Color: 151

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 468
Size: 276 Color: 128
Size: 269 Color: 98

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 464
Size: 288 Color: 165
Size: 263 Color: 62

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 317 Color: 257
Size: 296 Color: 190
Size: 388 Color: 382

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 495
Size: 258 Color: 44
Size: 250 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 341
Size: 359 Color: 339
Size: 283 Color: 155

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 455
Size: 291 Color: 180
Size: 269 Color: 97

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 380
Size: 312 Color: 242
Size: 301 Color: 207

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 415
Size: 335 Color: 294
Size: 255 Color: 28

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 347
Size: 351 Color: 328
Size: 285 Color: 160

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 481
Size: 270 Color: 103
Size: 262 Color: 56

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 477
Size: 274 Color: 120
Size: 263 Color: 60

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 311
Size: 340 Color: 310
Size: 321 Color: 268

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 496
Size: 255 Color: 32
Size: 252 Color: 17

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 390
Size: 308 Color: 234
Size: 298 Color: 196

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 500
Size: 250 Color: 7
Size: 250 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 381
Size: 317 Color: 259
Size: 296 Color: 192

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 401
Size: 300 Color: 204
Size: 298 Color: 195

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 385
Size: 320 Color: 265
Size: 289 Color: 168

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 348
Size: 345 Color: 316
Size: 291 Color: 178

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 444
Size: 294 Color: 187
Size: 275 Color: 125

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 485
Size: 272 Color: 112
Size: 256 Color: 37

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 367
Size: 356 Color: 336
Size: 264 Color: 70

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 365
Size: 351 Color: 329
Size: 270 Color: 101

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 368
Size: 312 Color: 248
Size: 307 Color: 231

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 433
Size: 297 Color: 194
Size: 279 Color: 143

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 355
Size: 364 Color: 345
Size: 263 Color: 61

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 383
Size: 336 Color: 296
Size: 276 Color: 130

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 487
Size: 272 Color: 111
Size: 252 Color: 14

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 488
Size: 268 Color: 94
Size: 256 Color: 35

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 400
Size: 329 Color: 283
Size: 269 Color: 96

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 460
Size: 296 Color: 191
Size: 256 Color: 34

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 411
Size: 335 Color: 293
Size: 256 Color: 39

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 499
Size: 253 Color: 19
Size: 250 Color: 4

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 361
Size: 336 Color: 298
Size: 287 Color: 164

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 483
Size: 277 Color: 136
Size: 253 Color: 18

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 393
Size: 312 Color: 250
Size: 291 Color: 179

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 410
Size: 328 Color: 279
Size: 264 Color: 69

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 359
Size: 374 Color: 357
Size: 251 Color: 10

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 362
Size: 349 Color: 324
Size: 274 Color: 121

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 312
Size: 340 Color: 308
Size: 320 Color: 266

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 442
Size: 312 Color: 245
Size: 259 Color: 47

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 375
Size: 312 Color: 246
Size: 302 Color: 214

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 423
Size: 301 Color: 208
Size: 283 Color: 153

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 466
Size: 278 Color: 137
Size: 270 Color: 99

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 486
Size: 266 Color: 80
Size: 261 Color: 53

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 456
Size: 292 Color: 183
Size: 267 Color: 86

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 480
Size: 281 Color: 145
Size: 252 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 358
Size: 343 Color: 314
Size: 282 Color: 150

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 318
Size: 345 Color: 317
Size: 311 Color: 240

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 356
Size: 346 Color: 321
Size: 281 Color: 146

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 387
Size: 314 Color: 251
Size: 293 Color: 186

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 463
Size: 284 Color: 158
Size: 267 Color: 81

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 449
Size: 301 Color: 210
Size: 264 Color: 68

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 434
Size: 323 Color: 270
Size: 252 Color: 16

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 421
Size: 307 Color: 229
Size: 278 Color: 139

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 457
Size: 303 Color: 217
Size: 255 Color: 27

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 446
Size: 302 Color: 213
Size: 266 Color: 79

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 327
Size: 350 Color: 325
Size: 301 Color: 211

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 437
Size: 319 Color: 263
Size: 254 Color: 25

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 498
Size: 256 Color: 36
Size: 250 Color: 8

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 479
Size: 268 Color: 90
Size: 265 Color: 77

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 408
Size: 319 Color: 262
Size: 274 Color: 122

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 377
Size: 325 Color: 277
Size: 289 Color: 171

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 402
Size: 310 Color: 239
Size: 287 Color: 162

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 418
Size: 331 Color: 286
Size: 255 Color: 30

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 490
Size: 261 Color: 54
Size: 258 Color: 42

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 335
Size: 353 Color: 331
Size: 292 Color: 182

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 427
Size: 318 Color: 260
Size: 263 Color: 67

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 462
Size: 288 Color: 166
Size: 263 Color: 63

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 440
Size: 300 Color: 206
Size: 271 Color: 105

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 432
Size: 315 Color: 254
Size: 262 Color: 55

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 371
Size: 346 Color: 320
Size: 270 Color: 100

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 388
Size: 317 Color: 258
Size: 290 Color: 177

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 374
Size: 324 Color: 274
Size: 290 Color: 174

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 397
Size: 320 Color: 264
Size: 280 Color: 144

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 370
Size: 339 Color: 304
Size: 277 Color: 133

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 431
Size: 322 Color: 269
Size: 255 Color: 33

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 469
Size: 290 Color: 176
Size: 254 Color: 26

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 436
Size: 287 Color: 163
Size: 286 Color: 161

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 342
Size: 340 Color: 309
Size: 300 Color: 202

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 399
Size: 307 Color: 230
Size: 291 Color: 181

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 430
Size: 303 Color: 218
Size: 274 Color: 118

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 420
Size: 307 Color: 233
Size: 279 Color: 142

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 484
Size: 271 Color: 107
Size: 258 Color: 46

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 482
Size: 272 Color: 109
Size: 259 Color: 48

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 413
Size: 312 Color: 249
Size: 279 Color: 141

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 366
Size: 332 Color: 287
Size: 289 Color: 173

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 389
Size: 307 Color: 228
Size: 299 Color: 197

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 472
Size: 282 Color: 148
Size: 260 Color: 50

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 419
Size: 315 Color: 252
Size: 271 Color: 106

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 473
Size: 289 Color: 170
Size: 251 Color: 9

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 467
Size: 282 Color: 149
Size: 264 Color: 71

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 428
Size: 303 Color: 215
Size: 276 Color: 127

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 438
Size: 295 Color: 188
Size: 277 Color: 135

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 396
Size: 324 Color: 276
Size: 276 Color: 129

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 475
Size: 284 Color: 157
Size: 256 Color: 38

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 445
Size: 306 Color: 225
Size: 263 Color: 66

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 315
Size: 341 Color: 313
Size: 317 Color: 256

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 407
Size: 319 Color: 261
Size: 275 Color: 124

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 452
Size: 299 Color: 198
Size: 262 Color: 59

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 338
Size: 335 Color: 295
Size: 307 Color: 227

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 454
Size: 282 Color: 147
Size: 279 Color: 140

Total size: 167167
Total free space: 0

