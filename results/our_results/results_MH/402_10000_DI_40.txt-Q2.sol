Capicity Bin: 7888
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 3912 Color: 0
Size: 1208 Color: 1
Size: 1064 Color: 0
Size: 640 Color: 0
Size: 480 Color: 1
Size: 384 Color: 0
Size: 120 Color: 1
Size: 80 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 0
Size: 1374 Color: 1
Size: 152 Color: 0

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 6304 Color: 1
Size: 960 Color: 0
Size: 448 Color: 0
Size: 128 Color: 1
Size: 48 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 1
Size: 666 Color: 1
Size: 132 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 0
Size: 1222 Color: 1
Size: 240 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 0
Size: 1862 Color: 1
Size: 372 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 1
Size: 1924 Color: 0
Size: 384 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 1
Size: 1332 Color: 0
Size: 264 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6170 Color: 0
Size: 1434 Color: 1
Size: 284 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6373 Color: 1
Size: 1263 Color: 0
Size: 252 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 0
Size: 810 Color: 0
Size: 160 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 0
Size: 1646 Color: 0
Size: 328 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 0
Size: 2124 Color: 0
Size: 424 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 0
Size: 2804 Color: 1
Size: 552 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 0
Size: 2524 Color: 1
Size: 496 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 0
Size: 1114 Color: 1
Size: 220 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 3948 Color: 0
Size: 3284 Color: 1
Size: 656 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 0
Size: 3124 Color: 0
Size: 616 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5948 Color: 0
Size: 1620 Color: 0
Size: 320 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 3950 Color: 0
Size: 3406 Color: 1
Size: 532 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 1
Size: 942 Color: 0
Size: 188 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 0
Size: 706 Color: 1
Size: 140 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 0
Size: 764 Color: 1
Size: 144 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 0
Size: 1234 Color: 0
Size: 244 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 1
Size: 1503 Color: 0
Size: 300 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 0
Size: 1175 Color: 0
Size: 58 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 1
Size: 1506 Color: 0
Size: 300 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 1
Size: 1054 Color: 0
Size: 208 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 1
Size: 1204 Color: 0
Size: 232 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4060 Color: 1
Size: 3652 Color: 0
Size: 176 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 3945 Color: 1
Size: 3645 Color: 0
Size: 298 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 0
Size: 1700 Color: 1
Size: 336 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 0
Size: 2028 Color: 1
Size: 384 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6862 Color: 1
Size: 858 Color: 1
Size: 168 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6407 Color: 0
Size: 1235 Color: 0
Size: 246 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 1
Size: 796 Color: 1
Size: 152 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5466 Color: 1
Size: 2022 Color: 1
Size: 400 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 1
Size: 3270 Color: 0
Size: 652 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 1
Size: 702 Color: 1
Size: 136 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 0
Size: 916 Color: 0
Size: 176 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6390 Color: 0
Size: 1250 Color: 1
Size: 248 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5774 Color: 1
Size: 1762 Color: 0
Size: 352 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 1
Size: 662 Color: 1
Size: 128 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4815 Color: 1
Size: 2561 Color: 0
Size: 512 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5459 Color: 0
Size: 2073 Color: 1
Size: 356 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 0
Size: 1245 Color: 1
Size: 76 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 0
Size: 1348 Color: 1
Size: 200 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4438 Color: 1
Size: 2878 Color: 1
Size: 572 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 1
Size: 884 Color: 1
Size: 168 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 1
Size: 1972 Color: 0
Size: 392 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 1
Size: 770 Color: 0
Size: 152 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 0
Size: 989 Color: 1
Size: 196 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 0
Size: 754 Color: 1
Size: 148 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5963 Color: 0
Size: 1605 Color: 1
Size: 320 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 1
Size: 1819 Color: 1
Size: 342 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 0
Size: 1028 Color: 0
Size: 200 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 3958 Color: 0
Size: 3278 Color: 0
Size: 652 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 0
Size: 2562 Color: 1
Size: 512 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 0
Size: 1423 Color: 0
Size: 284 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 1
Size: 1134 Color: 0
Size: 80 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 0
Size: 1188 Color: 1
Size: 232 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6570 Color: 1
Size: 1102 Color: 0
Size: 216 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 1
Size: 722 Color: 1
Size: 144 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5775 Color: 0
Size: 1761 Color: 0
Size: 352 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 0
Size: 1537 Color: 0
Size: 306 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 1
Size: 1366 Color: 1
Size: 272 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 0
Size: 1660 Color: 0
Size: 328 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4695 Color: 1
Size: 2969 Color: 0
Size: 224 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 1
Size: 2122 Color: 0
Size: 420 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 1
Size: 1106 Color: 1
Size: 220 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 0
Size: 724 Color: 0
Size: 144 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 1
Size: 862 Color: 1
Size: 172 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 3949 Color: 0
Size: 3283 Color: 1
Size: 656 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 1
Size: 700 Color: 1
Size: 136 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 0
Size: 1154 Color: 1
Size: 228 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 0
Size: 2076 Color: 1
Size: 408 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 1
Size: 974 Color: 0
Size: 192 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6299 Color: 1
Size: 1325 Color: 0
Size: 264 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 1
Size: 1124 Color: 0
Size: 216 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 0
Size: 2388 Color: 0
Size: 472 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 0
Size: 956 Color: 0
Size: 184 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6922 Color: 1
Size: 806 Color: 0
Size: 160 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 0
Size: 3286 Color: 1
Size: 656 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 0
Size: 1484 Color: 0
Size: 288 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 1
Size: 724 Color: 0
Size: 136 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6838 Color: 1
Size: 878 Color: 1
Size: 172 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 1
Size: 988 Color: 1
Size: 192 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 1
Size: 1165 Color: 1
Size: 148 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4755 Color: 1
Size: 2611 Color: 0
Size: 522 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 1
Size: 1884 Color: 0
Size: 376 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 4143 Color: 1
Size: 3121 Color: 0
Size: 624 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 3464 Color: 0
Size: 2928 Color: 1
Size: 1496 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 0
Size: 2242 Color: 0
Size: 444 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 0
Size: 1351 Color: 1
Size: 270 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 0
Size: 2452 Color: 0
Size: 488 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 6164 Color: 0
Size: 1444 Color: 1
Size: 280 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 4510 Color: 0
Size: 3238 Color: 1
Size: 140 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 1
Size: 2124 Color: 0
Size: 416 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 1
Size: 2386 Color: 0
Size: 476 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 3974 Color: 1
Size: 3290 Color: 1
Size: 624 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 0
Size: 776 Color: 1
Size: 144 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5865 Color: 1
Size: 1687 Color: 1
Size: 336 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5923 Color: 0
Size: 1639 Color: 0
Size: 326 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 0
Size: 938 Color: 1
Size: 184 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 1
Size: 870 Color: 1
Size: 172 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 6210 Color: 0
Size: 1402 Color: 1
Size: 276 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 1
Size: 1466 Color: 0
Size: 292 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 1
Size: 1068 Color: 1
Size: 208 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4548 Color: 1
Size: 2788 Color: 1
Size: 552 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 1
Size: 1359 Color: 0
Size: 270 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 4494 Color: 0
Size: 2830 Color: 1
Size: 564 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 0
Size: 852 Color: 0
Size: 168 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 1
Size: 2838 Color: 0
Size: 564 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4611 Color: 0
Size: 3037 Color: 0
Size: 240 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4401 Color: 1
Size: 2907 Color: 0
Size: 580 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4502 Color: 0
Size: 2822 Color: 0
Size: 564 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 0
Size: 2265 Color: 1
Size: 452 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 6670 Color: 1
Size: 1018 Color: 1
Size: 200 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 6735 Color: 1
Size: 1049 Color: 0
Size: 104 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 6014 Color: 1
Size: 1562 Color: 1
Size: 312 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 5419 Color: 0
Size: 2059 Color: 1
Size: 410 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 0
Size: 1023 Color: 1
Size: 204 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 1
Size: 1859 Color: 0
Size: 370 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 1
Size: 1139 Color: 0
Size: 226 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 6607 Color: 1
Size: 1069 Color: 0
Size: 212 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5073 Color: 1
Size: 2347 Color: 0
Size: 468 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6187 Color: 0
Size: 1465 Color: 1
Size: 236 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 5611 Color: 1
Size: 2099 Color: 0
Size: 178 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 5337 Color: 1
Size: 2127 Color: 0
Size: 424 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 1
Size: 682 Color: 0
Size: 132 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 0
Size: 1209 Color: 1
Size: 240 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 1
Size: 1111 Color: 1
Size: 222 Color: 0

Total size: 1041216
Total free space: 0

