Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 20
Items: 
Size: 672 Color: 0
Size: 672 Color: 0
Size: 584 Color: 0
Size: 504 Color: 0
Size: 448 Color: 0
Size: 444 Color: 0
Size: 432 Color: 0
Size: 432 Color: 0
Size: 432 Color: 0
Size: 380 Color: 1
Size: 372 Color: 1
Size: 364 Color: 1
Size: 356 Color: 1
Size: 356 Color: 1
Size: 336 Color: 1
Size: 328 Color: 1
Size: 308 Color: 1
Size: 300 Color: 1
Size: 264 Color: 1
Size: 152 Color: 0

Bin 2: 0 of cap free
Amount of items: 8
Items: 
Size: 4086 Color: 0
Size: 874 Color: 1
Size: 724 Color: 0
Size: 676 Color: 0
Size: 676 Color: 0
Size: 464 Color: 1
Size: 424 Color: 1
Size: 212 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 5466 Color: 1
Size: 1150 Color: 0
Size: 852 Color: 0
Size: 512 Color: 1
Size: 156 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 0
Size: 2542 Color: 1
Size: 132 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5994 Color: 0
Size: 1914 Color: 1
Size: 228 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 1
Size: 1922 Color: 1
Size: 180 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 1
Size: 1642 Color: 0
Size: 378 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 0
Size: 1554 Color: 0
Size: 128 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 1
Size: 892 Color: 1
Size: 626 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6685 Color: 1
Size: 1107 Color: 0
Size: 344 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6777 Color: 0
Size: 1133 Color: 0
Size: 226 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6782 Color: 1
Size: 1158 Color: 0
Size: 196 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 0
Size: 1092 Color: 0
Size: 224 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6805 Color: 1
Size: 931 Color: 0
Size: 400 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6951 Color: 0
Size: 881 Color: 1
Size: 304 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 0
Size: 630 Color: 1
Size: 552 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 676 Color: 0
Size: 508 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 1
Size: 820 Color: 0
Size: 360 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7114 Color: 0
Size: 734 Color: 0
Size: 288 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 1
Size: 804 Color: 0
Size: 160 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 0
Size: 548 Color: 1
Size: 384 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 1
Size: 682 Color: 0
Size: 184 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 1
Size: 706 Color: 1
Size: 140 Color: 0

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 4071 Color: 1
Size: 3372 Color: 0
Size: 480 Color: 1
Size: 212 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 4299 Color: 0
Size: 3388 Color: 1
Size: 448 Color: 1

Bin 26: 1 of cap free
Amount of items: 4
Items: 
Size: 4387 Color: 0
Size: 3390 Color: 1
Size: 184 Color: 1
Size: 174 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 4839 Color: 0
Size: 3024 Color: 1
Size: 272 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 5077 Color: 0
Size: 2922 Color: 1
Size: 136 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 1
Size: 2385 Color: 1
Size: 220 Color: 0

Bin 30: 1 of cap free
Amount of items: 4
Items: 
Size: 5782 Color: 0
Size: 1511 Color: 0
Size: 602 Color: 1
Size: 240 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 5842 Color: 0
Size: 1412 Color: 0
Size: 881 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 6269 Color: 0
Size: 1866 Color: 1

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 6346 Color: 1
Size: 1789 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6479 Color: 0
Size: 1484 Color: 0
Size: 172 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6502 Color: 0
Size: 1221 Color: 1
Size: 412 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 0
Size: 1144 Color: 0
Size: 334 Color: 1

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 1
Size: 1363 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6943 Color: 1
Size: 882 Color: 0
Size: 310 Color: 1

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 4355 Color: 1
Size: 3131 Color: 1
Size: 648 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 5991 Color: 0
Size: 1891 Color: 1
Size: 252 Color: 0

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 0
Size: 1554 Color: 1

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 0
Size: 1266 Color: 1

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 7004 Color: 0
Size: 1130 Color: 1

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 7034 Color: 0
Size: 1100 Color: 1

Bin 45: 3 of cap free
Amount of items: 4
Items: 
Size: 4070 Color: 1
Size: 3235 Color: 0
Size: 476 Color: 1
Size: 352 Color: 0

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 4500 Color: 1
Size: 3633 Color: 0

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 4700 Color: 0
Size: 3149 Color: 1
Size: 284 Color: 0

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 0
Size: 1613 Color: 1

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 6721 Color: 0
Size: 1244 Color: 1
Size: 168 Color: 0

Bin 50: 3 of cap free
Amount of items: 2
Items: 
Size: 7022 Color: 0
Size: 1111 Color: 1

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 0
Size: 1051 Color: 1

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 6026 Color: 1
Size: 1398 Color: 0
Size: 708 Color: 0

Bin 53: 4 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 1
Size: 1042 Color: 0

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 4379 Color: 1
Size: 3560 Color: 0
Size: 192 Color: 0

Bin 55: 5 of cap free
Amount of items: 2
Items: 
Size: 6750 Color: 1
Size: 1381 Color: 0

Bin 56: 6 of cap free
Amount of items: 3
Items: 
Size: 5082 Color: 1
Size: 2776 Color: 1
Size: 272 Color: 0

Bin 57: 6 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 0
Size: 2410 Color: 1
Size: 308 Color: 0

Bin 58: 6 of cap free
Amount of items: 3
Items: 
Size: 5537 Color: 1
Size: 2395 Color: 0
Size: 198 Color: 0

Bin 59: 6 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 1
Size: 2127 Color: 0
Size: 136 Color: 1

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 1
Size: 1062 Color: 0

Bin 61: 6 of cap free
Amount of items: 2
Items: 
Size: 7142 Color: 0
Size: 988 Color: 1

Bin 62: 7 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 1
Size: 2749 Color: 1
Size: 144 Color: 0

Bin 63: 7 of cap free
Amount of items: 2
Items: 
Size: 7060 Color: 0
Size: 1069 Color: 1

Bin 64: 7 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 1
Size: 933 Color: 0
Size: 40 Color: 1

Bin 65: 8 of cap free
Amount of items: 4
Items: 
Size: 4078 Color: 1
Size: 3382 Color: 0
Size: 508 Color: 1
Size: 160 Color: 0

Bin 66: 8 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 0
Size: 1684 Color: 1

Bin 67: 8 of cap free
Amount of items: 2
Items: 
Size: 6828 Color: 1
Size: 1300 Color: 0

Bin 68: 8 of cap free
Amount of items: 3
Items: 
Size: 7258 Color: 1
Size: 854 Color: 0
Size: 16 Color: 1

Bin 69: 8 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 0
Size: 786 Color: 1
Size: 50 Color: 0

Bin 70: 9 of cap free
Amount of items: 3
Items: 
Size: 4352 Color: 1
Size: 3151 Color: 0
Size: 624 Color: 0

Bin 71: 9 of cap free
Amount of items: 3
Items: 
Size: 7021 Color: 0
Size: 1000 Color: 1
Size: 106 Color: 0

Bin 72: 10 of cap free
Amount of items: 2
Items: 
Size: 6282 Color: 1
Size: 1844 Color: 0

Bin 73: 10 of cap free
Amount of items: 2
Items: 
Size: 6364 Color: 0
Size: 1762 Color: 1

Bin 74: 10 of cap free
Amount of items: 3
Items: 
Size: 7268 Color: 0
Size: 766 Color: 1
Size: 92 Color: 0

Bin 75: 11 of cap free
Amount of items: 3
Items: 
Size: 4779 Color: 0
Size: 3160 Color: 1
Size: 186 Color: 1

Bin 76: 12 of cap free
Amount of items: 3
Items: 
Size: 5845 Color: 1
Size: 1557 Color: 0
Size: 722 Color: 1

Bin 77: 12 of cap free
Amount of items: 2
Items: 
Size: 5850 Color: 1
Size: 2274 Color: 0

Bin 78: 12 of cap free
Amount of items: 2
Items: 
Size: 5898 Color: 0
Size: 2226 Color: 1

Bin 79: 12 of cap free
Amount of items: 2
Items: 
Size: 7194 Color: 0
Size: 930 Color: 1

Bin 80: 12 of cap free
Amount of items: 2
Items: 
Size: 7322 Color: 0
Size: 802 Color: 1

Bin 81: 14 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 1
Size: 1236 Color: 0

Bin 82: 14 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 1
Size: 948 Color: 0

Bin 83: 15 of cap free
Amount of items: 3
Items: 
Size: 6501 Color: 0
Size: 1524 Color: 1
Size: 96 Color: 1

Bin 84: 16 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 0
Size: 1494 Color: 1

Bin 85: 18 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 1
Size: 1362 Color: 0

Bin 86: 18 of cap free
Amount of items: 2
Items: 
Size: 6937 Color: 1
Size: 1181 Color: 0

Bin 87: 18 of cap free
Amount of items: 2
Items: 
Size: 7218 Color: 1
Size: 900 Color: 0

Bin 88: 19 of cap free
Amount of items: 2
Items: 
Size: 6673 Color: 0
Size: 1444 Color: 1

Bin 89: 19 of cap free
Amount of items: 2
Items: 
Size: 6855 Color: 0
Size: 1262 Color: 1

Bin 90: 20 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 0
Size: 982 Color: 1
Size: 672 Color: 1

Bin 91: 20 of cap free
Amount of items: 3
Items: 
Size: 7017 Color: 0
Size: 995 Color: 1
Size: 104 Color: 0

Bin 92: 21 of cap free
Amount of items: 2
Items: 
Size: 6050 Color: 0
Size: 2065 Color: 1

Bin 93: 21 of cap free
Amount of items: 2
Items: 
Size: 7089 Color: 1
Size: 1026 Color: 0

Bin 94: 23 of cap free
Amount of items: 2
Items: 
Size: 6551 Color: 0
Size: 1562 Color: 1

Bin 95: 23 of cap free
Amount of items: 2
Items: 
Size: 7116 Color: 1
Size: 997 Color: 0

Bin 96: 25 of cap free
Amount of items: 2
Items: 
Size: 6325 Color: 1
Size: 1786 Color: 0

Bin 97: 26 of cap free
Amount of items: 2
Items: 
Size: 5474 Color: 1
Size: 2636 Color: 0

Bin 98: 26 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 0
Size: 1962 Color: 1

Bin 99: 26 of cap free
Amount of items: 2
Items: 
Size: 6877 Color: 1
Size: 1233 Color: 0

Bin 100: 28 of cap free
Amount of items: 2
Items: 
Size: 6266 Color: 1
Size: 1842 Color: 0

Bin 101: 29 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 1
Size: 1906 Color: 0

Bin 102: 30 of cap free
Amount of items: 2
Items: 
Size: 5932 Color: 1
Size: 2174 Color: 0

Bin 103: 34 of cap free
Amount of items: 2
Items: 
Size: 6421 Color: 0
Size: 1681 Color: 1

Bin 104: 34 of cap free
Amount of items: 2
Items: 
Size: 6962 Color: 0
Size: 1140 Color: 1

Bin 105: 34 of cap free
Amount of items: 2
Items: 
Size: 7042 Color: 0
Size: 1060 Color: 1

Bin 106: 36 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 0
Size: 1874 Color: 1
Size: 392 Color: 0

Bin 107: 37 of cap free
Amount of items: 6
Items: 
Size: 4069 Color: 1
Size: 922 Color: 1
Size: 914 Color: 1
Size: 830 Color: 0
Size: 780 Color: 0
Size: 584 Color: 0

Bin 108: 39 of cap free
Amount of items: 2
Items: 
Size: 5930 Color: 0
Size: 2167 Color: 1

Bin 109: 39 of cap free
Amount of items: 2
Items: 
Size: 7081 Color: 0
Size: 1016 Color: 1

Bin 110: 42 of cap free
Amount of items: 2
Items: 
Size: 5548 Color: 1
Size: 2546 Color: 0

Bin 111: 44 of cap free
Amount of items: 2
Items: 
Size: 4076 Color: 1
Size: 4016 Color: 0

Bin 112: 45 of cap free
Amount of items: 2
Items: 
Size: 6660 Color: 0
Size: 1431 Color: 1

Bin 113: 57 of cap free
Amount of items: 2
Items: 
Size: 4436 Color: 0
Size: 3643 Color: 1

Bin 114: 57 of cap free
Amount of items: 2
Items: 
Size: 5659 Color: 1
Size: 2420 Color: 0

Bin 115: 57 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 0
Size: 1321 Color: 1

Bin 116: 59 of cap free
Amount of items: 2
Items: 
Size: 6121 Color: 0
Size: 1956 Color: 1

Bin 117: 59 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 1
Size: 1211 Color: 0

Bin 118: 64 of cap free
Amount of items: 2
Items: 
Size: 5036 Color: 0
Size: 3036 Color: 1

Bin 119: 64 of cap free
Amount of items: 2
Items: 
Size: 5796 Color: 0
Size: 2276 Color: 1

Bin 120: 66 of cap free
Amount of items: 3
Items: 
Size: 4092 Color: 0
Size: 3378 Color: 1
Size: 600 Color: 0

Bin 121: 68 of cap free
Amount of items: 2
Items: 
Size: 7079 Color: 1
Size: 989 Color: 0

Bin 122: 72 of cap free
Amount of items: 31
Items: 
Size: 380 Color: 0
Size: 380 Color: 0
Size: 372 Color: 0
Size: 348 Color: 0
Size: 348 Color: 0
Size: 322 Color: 0
Size: 296 Color: 0
Size: 288 Color: 0
Size: 280 Color: 1
Size: 276 Color: 1
Size: 276 Color: 1
Size: 272 Color: 1
Size: 256 Color: 0
Size: 252 Color: 0
Size: 248 Color: 0
Size: 246 Color: 1
Size: 244 Color: 1
Size: 242 Color: 0
Size: 240 Color: 1
Size: 234 Color: 1
Size: 228 Color: 0
Size: 224 Color: 0
Size: 224 Color: 0
Size: 216 Color: 1
Size: 216 Color: 1
Size: 208 Color: 1
Size: 208 Color: 1
Size: 208 Color: 0
Size: 184 Color: 1
Size: 176 Color: 1
Size: 172 Color: 1

Bin 123: 72 of cap free
Amount of items: 2
Items: 
Size: 6404 Color: 1
Size: 1660 Color: 0

Bin 124: 73 of cap free
Amount of items: 2
Items: 
Size: 5275 Color: 0
Size: 2788 Color: 1

Bin 125: 78 of cap free
Amount of items: 2
Items: 
Size: 4630 Color: 1
Size: 3428 Color: 0

Bin 126: 82 of cap free
Amount of items: 2
Items: 
Size: 5890 Color: 1
Size: 2164 Color: 0

Bin 127: 86 of cap free
Amount of items: 2
Items: 
Size: 6308 Color: 1
Size: 1742 Color: 0

Bin 128: 88 of cap free
Amount of items: 2
Items: 
Size: 5497 Color: 0
Size: 2551 Color: 1

Bin 129: 108 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 0
Size: 1754 Color: 1

Bin 130: 119 of cap free
Amount of items: 2
Items: 
Size: 4626 Color: 1
Size: 3391 Color: 0

Bin 131: 120 of cap free
Amount of items: 2
Items: 
Size: 5090 Color: 1
Size: 2926 Color: 0

Bin 132: 128 of cap free
Amount of items: 2
Items: 
Size: 4796 Color: 0
Size: 3212 Color: 1

Bin 133: 5424 of cap free
Amount of items: 16
Items: 
Size: 208 Color: 0
Size: 208 Color: 0
Size: 192 Color: 0
Size: 184 Color: 0
Size: 180 Color: 0
Size: 176 Color: 0
Size: 176 Color: 0
Size: 168 Color: 1
Size: 164 Color: 1
Size: 164 Color: 1
Size: 160 Color: 1
Size: 156 Color: 1
Size: 152 Color: 1
Size: 144 Color: 1
Size: 144 Color: 0
Size: 136 Color: 1

Total size: 1073952
Total free space: 8136

