Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 108
Size: 281 Color: 42
Size: 257 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 4
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 85
Size: 340 Color: 73
Size: 285 Color: 48

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 92
Size: 334 Color: 71
Size: 264 Color: 25

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 113
Size: 268 Color: 26
Size: 258 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 86
Size: 358 Color: 79
Size: 259 Color: 21

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 303 Color: 57
Size: 282 Color: 46
Size: 415 Color: 98

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 82
Size: 355 Color: 77
Size: 282 Color: 45

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 93
Size: 303 Color: 58
Size: 289 Color: 50

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 95
Size: 309 Color: 62
Size: 280 Color: 40

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 105
Size: 289 Color: 51
Size: 258 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 116
Size: 255 Color: 10
Size: 254 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 76
Size: 306 Color: 60
Size: 343 Color: 74

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 117
Size: 257 Color: 15
Size: 250 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 103
Size: 312 Color: 63
Size: 253 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 90
Size: 355 Color: 78
Size: 252 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 88
Size: 313 Color: 64
Size: 298 Color: 54

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 112
Size: 269 Color: 28
Size: 260 Color: 22

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 81
Size: 349 Color: 75
Size: 288 Color: 49

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 102
Size: 290 Color: 52
Size: 277 Color: 37

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 106
Size: 275 Color: 36
Size: 268 Color: 27

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 114
Size: 270 Color: 29
Size: 250 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 80
Size: 335 Color: 72
Size: 307 Color: 61

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 111
Size: 271 Color: 30
Size: 260 Color: 23

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 87
Size: 329 Color: 67
Size: 282 Color: 44

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 118
Size: 255 Color: 11
Size: 251 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 101
Size: 298 Color: 55
Size: 278 Color: 38

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 97
Size: 332 Color: 68
Size: 254 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 109
Size: 280 Color: 41
Size: 258 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 99
Size: 305 Color: 59
Size: 275 Color: 34

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 104
Size: 281 Color: 43
Size: 272 Color: 31

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 84
Size: 371 Color: 83
Size: 256 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 115
Size: 261 Color: 24
Size: 257 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 91
Size: 333 Color: 70
Size: 273 Color: 33

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 94
Size: 299 Color: 56
Size: 291 Color: 53

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 110
Size: 279 Color: 39
Size: 256 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 89
Size: 332 Color: 69
Size: 275 Color: 35

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 96
Size: 315 Color: 65
Size: 272 Color: 32

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 100
Size: 318 Color: 66
Size: 259 Color: 20

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 107
Size: 284 Color: 47
Size: 254 Color: 8

Total size: 40000
Total free space: 0

