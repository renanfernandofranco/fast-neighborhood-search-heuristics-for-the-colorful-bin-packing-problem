Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 365 Color: 3
Size: 269 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 3
Size: 267 Color: 4
Size: 254 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 274 Color: 2
Size: 260 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 307 Color: 0
Size: 272 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 305 Color: 1
Size: 298 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 0
Size: 272 Color: 4
Size: 283 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 4
Size: 336 Color: 0
Size: 297 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 266 Color: 1
Size: 254 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 255 Color: 2
Size: 250 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 347 Color: 0
Size: 287 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 333 Color: 2
Size: 275 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 267 Color: 3
Size: 252 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 318 Color: 2
Size: 263 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 253 Color: 4
Size: 250 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 258 Color: 4
Size: 254 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 372 Color: 1
Size: 252 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 2
Size: 366 Color: 3
Size: 264 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 2
Size: 253 Color: 3
Size: 250 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 2
Size: 255 Color: 1
Size: 253 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 4
Size: 260 Color: 4
Size: 255 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 294 Color: 1
Size: 272 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 330 Color: 2
Size: 263 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 260 Color: 1
Size: 255 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 308 Color: 2
Size: 282 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 363 Color: 1
Size: 271 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 298 Color: 2
Size: 252 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 312 Color: 0
Size: 302 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 258 Color: 1
Size: 252 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 318 Color: 1
Size: 287 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 313 Color: 4
Size: 257 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 280 Color: 0
Size: 274 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 325 Color: 3
Size: 253 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 356 Color: 3
Size: 278 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 294 Color: 0
Size: 269 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 357 Color: 2
Size: 261 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 3
Size: 271 Color: 2
Size: 255 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 282 Color: 3
Size: 273 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 264 Color: 4
Size: 251 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 310 Color: 4
Size: 271 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 258 Color: 3
Size: 251 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 275 Color: 2
Size: 254 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 3
Size: 250 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 4
Size: 295 Color: 3
Size: 290 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 281 Color: 1
Size: 250 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 350 Color: 1
Size: 250 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 351 Color: 1
Size: 292 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 350 Color: 4
Size: 293 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 287 Color: 4
Size: 269 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 294 Color: 4
Size: 265 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 303 Color: 1
Size: 258 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 264 Color: 2
Size: 251 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 308 Color: 1
Size: 251 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 4
Size: 320 Color: 3
Size: 254 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 352 Color: 3
Size: 256 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 2
Size: 281 Color: 2
Size: 273 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 3
Size: 268 Color: 0
Size: 259 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 329 Color: 3
Size: 251 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 3
Size: 350 Color: 1
Size: 291 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 263 Color: 1
Size: 259 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 255 Color: 4
Size: 250 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 362 Color: 3
Size: 271 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 276 Color: 2
Size: 252 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 269 Color: 0
Size: 258 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 0
Size: 350 Color: 3
Size: 299 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 274 Color: 3
Size: 262 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 2
Size: 253 Color: 3
Size: 250 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 337 Color: 0
Size: 271 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 277 Color: 3
Size: 253 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 0
Size: 344 Color: 0
Size: 301 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 314 Color: 1
Size: 293 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 356 Color: 1
Size: 259 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 276 Color: 1
Size: 262 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 2
Size: 333 Color: 4
Size: 262 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 342 Color: 3
Size: 252 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 315 Color: 0
Size: 259 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 3
Size: 314 Color: 2
Size: 274 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 355 Color: 2
Size: 275 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 259 Color: 4
Size: 250 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 361 Color: 2
Size: 276 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 298 Color: 0
Size: 288 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 253 Color: 0
Size: 251 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 346 Color: 2
Size: 250 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 261 Color: 2
Size: 259 Color: 3

Total size: 83000
Total free space: 0

