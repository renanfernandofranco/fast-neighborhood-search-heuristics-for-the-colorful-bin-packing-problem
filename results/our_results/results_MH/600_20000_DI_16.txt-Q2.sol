Capicity Bin: 16336
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8170 Color: 1
Size: 1581 Color: 1
Size: 1580 Color: 1
Size: 1557 Color: 0
Size: 1528 Color: 0
Size: 992 Color: 1
Size: 928 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8680 Color: 1
Size: 6788 Color: 0
Size: 868 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9234 Color: 0
Size: 6772 Color: 1
Size: 330 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9682 Color: 1
Size: 6374 Color: 1
Size: 280 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9698 Color: 1
Size: 6354 Color: 1
Size: 284 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 0
Size: 5016 Color: 0
Size: 292 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 0
Size: 4008 Color: 1
Size: 1116 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11589 Color: 0
Size: 4185 Color: 1
Size: 562 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 0
Size: 4008 Color: 1
Size: 288 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 1
Size: 2750 Color: 0
Size: 714 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13258 Color: 1
Size: 2372 Color: 1
Size: 706 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 0
Size: 2648 Color: 0
Size: 432 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13277 Color: 1
Size: 2551 Color: 1
Size: 508 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 2548 Color: 1
Size: 472 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 0
Size: 2490 Color: 1
Size: 512 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 1
Size: 2648 Color: 0
Size: 352 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13350 Color: 0
Size: 2258 Color: 1
Size: 728 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 0
Size: 1600 Color: 1
Size: 1000 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13750 Color: 0
Size: 1426 Color: 0
Size: 1160 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 1
Size: 1908 Color: 1
Size: 648 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 0
Size: 2261 Color: 1
Size: 316 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 0
Size: 2329 Color: 1
Size: 110 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13977 Color: 1
Size: 1879 Color: 1
Size: 480 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13962 Color: 0
Size: 1562 Color: 0
Size: 812 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13991 Color: 1
Size: 1955 Color: 0
Size: 390 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14058 Color: 0
Size: 1982 Color: 0
Size: 296 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 1
Size: 1858 Color: 1
Size: 454 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 0
Size: 1898 Color: 1
Size: 376 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 0
Size: 1372 Color: 1
Size: 864 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 0
Size: 1473 Color: 1
Size: 704 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14227 Color: 1
Size: 1815 Color: 1
Size: 294 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 1
Size: 1380 Color: 0
Size: 704 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14378 Color: 1
Size: 1078 Color: 1
Size: 880 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14391 Color: 1
Size: 1481 Color: 0
Size: 464 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 1
Size: 1556 Color: 1
Size: 376 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 1
Size: 1546 Color: 0
Size: 384 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14416 Color: 1
Size: 1072 Color: 1
Size: 848 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14355 Color: 0
Size: 1521 Color: 1
Size: 460 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 0
Size: 1008 Color: 1
Size: 782 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14642 Color: 1
Size: 1216 Color: 0
Size: 478 Color: 1

Bin 41: 1 of cap free
Amount of items: 10
Items: 
Size: 8169 Color: 0
Size: 1328 Color: 0
Size: 1188 Color: 0
Size: 1108 Color: 0
Size: 1108 Color: 0
Size: 790 Color: 1
Size: 736 Color: 1
Size: 700 Color: 1
Size: 660 Color: 1
Size: 548 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 1
Size: 7511 Color: 0
Size: 596 Color: 1

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 9181 Color: 0
Size: 6802 Color: 0
Size: 352 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 11637 Color: 1
Size: 4374 Color: 0
Size: 324 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 1
Size: 3957 Color: 0
Size: 288 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 12468 Color: 1
Size: 3573 Color: 0
Size: 294 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 0
Size: 2711 Color: 0
Size: 356 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 13459 Color: 0
Size: 2092 Color: 0
Size: 784 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 13727 Color: 1
Size: 2288 Color: 1
Size: 320 Color: 0

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 14039 Color: 0
Size: 2296 Color: 1

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 1
Size: 2149 Color: 0

Bin 52: 2 of cap free
Amount of items: 37
Items: 
Size: 550 Color: 1
Size: 544 Color: 1
Size: 542 Color: 1
Size: 528 Color: 1
Size: 528 Color: 1
Size: 504 Color: 1
Size: 504 Color: 1
Size: 496 Color: 1
Size: 496 Color: 1
Size: 488 Color: 1
Size: 486 Color: 0
Size: 480 Color: 1
Size: 476 Color: 1
Size: 464 Color: 1
Size: 462 Color: 1
Size: 448 Color: 0
Size: 436 Color: 0
Size: 434 Color: 1
Size: 434 Color: 0
Size: 432 Color: 1
Size: 428 Color: 0
Size: 424 Color: 1
Size: 424 Color: 0
Size: 416 Color: 1
Size: 416 Color: 0
Size: 416 Color: 0
Size: 414 Color: 0
Size: 408 Color: 0
Size: 392 Color: 1
Size: 392 Color: 0
Size: 382 Color: 0
Size: 368 Color: 0
Size: 368 Color: 0
Size: 350 Color: 0
Size: 348 Color: 0
Size: 336 Color: 0
Size: 320 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 1
Size: 6764 Color: 0
Size: 554 Color: 1

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 12129 Color: 0
Size: 3917 Color: 1
Size: 288 Color: 0

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 12436 Color: 1
Size: 3898 Color: 0

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 0
Size: 3262 Color: 1
Size: 652 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 0
Size: 2741 Color: 0
Size: 364 Color: 1

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 1
Size: 2452 Color: 0

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 9867 Color: 1
Size: 6104 Color: 1
Size: 362 Color: 0

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 10718 Color: 1
Size: 5391 Color: 0
Size: 224 Color: 0

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 11117 Color: 0
Size: 4344 Color: 0
Size: 872 Color: 1

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 11180 Color: 0
Size: 5153 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 12049 Color: 1
Size: 3964 Color: 0
Size: 320 Color: 1

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 1
Size: 3533 Color: 0
Size: 344 Color: 0

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 1
Size: 2310 Color: 1
Size: 1168 Color: 0

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 12953 Color: 1
Size: 2952 Color: 0
Size: 428 Color: 1

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 1
Size: 2987 Color: 0
Size: 374 Color: 0

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 2781 Color: 0
Size: 376 Color: 1

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 14482 Color: 1
Size: 1851 Color: 0

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 14511 Color: 1
Size: 1782 Color: 0
Size: 40 Color: 1

Bin 71: 4 of cap free
Amount of items: 8
Items: 
Size: 8172 Color: 0
Size: 1360 Color: 1
Size: 1360 Color: 1
Size: 1360 Color: 0
Size: 1352 Color: 1
Size: 1352 Color: 0
Size: 992 Color: 1
Size: 384 Color: 0

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 9471 Color: 0
Size: 6549 Color: 1
Size: 312 Color: 0

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 12892 Color: 0
Size: 2096 Color: 0
Size: 1344 Color: 1

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 13104 Color: 0
Size: 3228 Color: 1

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 0
Size: 2564 Color: 1
Size: 276 Color: 1

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 1
Size: 2504 Color: 0

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 14202 Color: 0
Size: 2130 Color: 1

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 9675 Color: 1
Size: 5546 Color: 1
Size: 1110 Color: 0

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 11315 Color: 0
Size: 4680 Color: 1
Size: 336 Color: 1

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 14083 Color: 1
Size: 2248 Color: 0

Bin 81: 5 of cap free
Amount of items: 3
Items: 
Size: 14569 Color: 1
Size: 1682 Color: 0
Size: 80 Color: 1

Bin 82: 5 of cap free
Amount of items: 2
Items: 
Size: 14580 Color: 1
Size: 1751 Color: 0

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 14680 Color: 0
Size: 1651 Color: 1

Bin 84: 6 of cap free
Amount of items: 7
Items: 
Size: 8180 Color: 0
Size: 1494 Color: 1
Size: 1482 Color: 1
Size: 1460 Color: 0
Size: 1458 Color: 0
Size: 1264 Color: 0
Size: 992 Color: 1

Bin 85: 6 of cap free
Amount of items: 3
Items: 
Size: 9666 Color: 1
Size: 6392 Color: 1
Size: 272 Color: 0

Bin 86: 6 of cap free
Amount of items: 5
Items: 
Size: 10808 Color: 0
Size: 1902 Color: 1
Size: 1678 Color: 1
Size: 1610 Color: 0
Size: 332 Color: 1

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 1
Size: 2888 Color: 0

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 14590 Color: 1
Size: 1740 Color: 0

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 8479 Color: 0
Size: 7540 Color: 0
Size: 310 Color: 1

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 11978 Color: 1
Size: 4351 Color: 0

Bin 91: 7 of cap free
Amount of items: 2
Items: 
Size: 13508 Color: 0
Size: 2821 Color: 1

Bin 92: 7 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 2381 Color: 0
Size: 132 Color: 1

Bin 93: 8 of cap free
Amount of items: 3
Items: 
Size: 8264 Color: 0
Size: 7152 Color: 1
Size: 912 Color: 0

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 1
Size: 2689 Color: 0

Bin 95: 8 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 0
Size: 1606 Color: 1
Size: 48 Color: 0

Bin 96: 9 of cap free
Amount of items: 9
Items: 
Size: 8171 Color: 0
Size: 1352 Color: 0
Size: 1344 Color: 0
Size: 1344 Color: 0
Size: 896 Color: 1
Size: 856 Color: 1
Size: 836 Color: 1
Size: 776 Color: 1
Size: 752 Color: 1

Bin 97: 9 of cap free
Amount of items: 3
Items: 
Size: 10787 Color: 1
Size: 5204 Color: 0
Size: 336 Color: 1

Bin 98: 9 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 0
Size: 2431 Color: 1

Bin 99: 9 of cap free
Amount of items: 2
Items: 
Size: 14052 Color: 0
Size: 2275 Color: 1

Bin 100: 9 of cap free
Amount of items: 2
Items: 
Size: 14012 Color: 1
Size: 2315 Color: 0

Bin 101: 10 of cap free
Amount of items: 3
Items: 
Size: 9202 Color: 0
Size: 6804 Color: 1
Size: 320 Color: 0

Bin 102: 10 of cap free
Amount of items: 3
Items: 
Size: 11090 Color: 1
Size: 4980 Color: 0
Size: 256 Color: 1

Bin 103: 10 of cap free
Amount of items: 2
Items: 
Size: 14280 Color: 0
Size: 2046 Color: 1

Bin 104: 12 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 0
Size: 6728 Color: 1
Size: 240 Color: 0

Bin 105: 12 of cap free
Amount of items: 2
Items: 
Size: 11084 Color: 1
Size: 5240 Color: 0

Bin 106: 12 of cap free
Amount of items: 3
Items: 
Size: 12422 Color: 1
Size: 3302 Color: 1
Size: 600 Color: 0

Bin 107: 12 of cap free
Amount of items: 2
Items: 
Size: 12433 Color: 0
Size: 3891 Color: 1

Bin 108: 12 of cap free
Amount of items: 2
Items: 
Size: 14284 Color: 1
Size: 2040 Color: 0

Bin 109: 12 of cap free
Amount of items: 2
Items: 
Size: 14476 Color: 1
Size: 1848 Color: 0

Bin 110: 13 of cap free
Amount of items: 2
Items: 
Size: 13447 Color: 0
Size: 2876 Color: 1

Bin 111: 14 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 0
Size: 1696 Color: 1

Bin 112: 15 of cap free
Amount of items: 2
Items: 
Size: 10056 Color: 0
Size: 6265 Color: 1

Bin 113: 15 of cap free
Amount of items: 3
Items: 
Size: 13001 Color: 1
Size: 3120 Color: 0
Size: 200 Color: 1

Bin 114: 15 of cap free
Amount of items: 2
Items: 
Size: 13081 Color: 0
Size: 3240 Color: 1

Bin 115: 15 of cap free
Amount of items: 2
Items: 
Size: 14562 Color: 0
Size: 1759 Color: 1

Bin 116: 16 of cap free
Amount of items: 26
Items: 
Size: 936 Color: 0
Size: 924 Color: 0
Size: 752 Color: 0
Size: 744 Color: 0
Size: 724 Color: 0
Size: 654 Color: 1
Size: 650 Color: 1
Size: 648 Color: 1
Size: 642 Color: 0
Size: 640 Color: 0
Size: 640 Color: 0
Size: 636 Color: 1
Size: 628 Color: 1
Size: 624 Color: 1
Size: 600 Color: 1
Size: 580 Color: 0
Size: 576 Color: 1
Size: 576 Color: 1
Size: 576 Color: 1
Size: 568 Color: 1
Size: 560 Color: 1
Size: 546 Color: 0
Size: 540 Color: 0
Size: 512 Color: 0
Size: 512 Color: 0
Size: 332 Color: 1

Bin 117: 16 of cap free
Amount of items: 2
Items: 
Size: 13419 Color: 0
Size: 2901 Color: 1

Bin 118: 16 of cap free
Amount of items: 2
Items: 
Size: 14239 Color: 0
Size: 2081 Color: 1

Bin 119: 16 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 1
Size: 1720 Color: 0

Bin 120: 17 of cap free
Amount of items: 2
Items: 
Size: 14319 Color: 1
Size: 2000 Color: 0

Bin 121: 18 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 1
Size: 1634 Color: 0

Bin 122: 21 of cap free
Amount of items: 2
Items: 
Size: 12808 Color: 0
Size: 3507 Color: 1

Bin 123: 21 of cap free
Amount of items: 2
Items: 
Size: 14410 Color: 1
Size: 1905 Color: 0

Bin 124: 22 of cap free
Amount of items: 2
Items: 
Size: 14520 Color: 1
Size: 1794 Color: 0

Bin 125: 23 of cap free
Amount of items: 2
Items: 
Size: 10549 Color: 1
Size: 5764 Color: 0

Bin 126: 23 of cap free
Amount of items: 2
Items: 
Size: 14138 Color: 0
Size: 2175 Color: 1

Bin 127: 24 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 0
Size: 4276 Color: 1
Size: 192 Color: 1

Bin 128: 24 of cap free
Amount of items: 2
Items: 
Size: 11884 Color: 0
Size: 4428 Color: 1

Bin 129: 24 of cap free
Amount of items: 2
Items: 
Size: 13559 Color: 1
Size: 2753 Color: 0

Bin 130: 24 of cap free
Amount of items: 2
Items: 
Size: 14235 Color: 0
Size: 2077 Color: 1

Bin 131: 24 of cap free
Amount of items: 2
Items: 
Size: 14444 Color: 1
Size: 1868 Color: 0

Bin 132: 25 of cap free
Amount of items: 3
Items: 
Size: 8196 Color: 1
Size: 6807 Color: 0
Size: 1308 Color: 1

Bin 133: 25 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 0
Size: 1967 Color: 1

Bin 134: 26 of cap free
Amount of items: 2
Items: 
Size: 12718 Color: 0
Size: 3592 Color: 1

Bin 135: 26 of cap free
Amount of items: 2
Items: 
Size: 13160 Color: 1
Size: 3150 Color: 0

Bin 136: 26 of cap free
Amount of items: 2
Items: 
Size: 14561 Color: 0
Size: 1749 Color: 1

Bin 137: 29 of cap free
Amount of items: 2
Items: 
Size: 13054 Color: 1
Size: 3253 Color: 0

Bin 138: 30 of cap free
Amount of items: 2
Items: 
Size: 13038 Color: 0
Size: 3268 Color: 1

Bin 139: 32 of cap free
Amount of items: 2
Items: 
Size: 13566 Color: 0
Size: 2738 Color: 1

Bin 140: 32 of cap free
Amount of items: 2
Items: 
Size: 14588 Color: 0
Size: 1716 Color: 1

Bin 141: 32 of cap free
Amount of items: 2
Items: 
Size: 14692 Color: 0
Size: 1612 Color: 1

Bin 142: 33 of cap free
Amount of items: 2
Items: 
Size: 13630 Color: 1
Size: 2673 Color: 0

Bin 143: 33 of cap free
Amount of items: 2
Items: 
Size: 14469 Color: 1
Size: 1834 Color: 0

Bin 144: 34 of cap free
Amount of items: 3
Items: 
Size: 9895 Color: 1
Size: 5551 Color: 0
Size: 856 Color: 0

Bin 145: 34 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 0
Size: 3018 Color: 1

Bin 146: 35 of cap free
Amount of items: 2
Items: 
Size: 13049 Color: 1
Size: 3252 Color: 0

Bin 147: 36 of cap free
Amount of items: 2
Items: 
Size: 12858 Color: 0
Size: 3442 Color: 1

Bin 148: 38 of cap free
Amount of items: 2
Items: 
Size: 13085 Color: 1
Size: 3213 Color: 0

Bin 149: 38 of cap free
Amount of items: 2
Items: 
Size: 13396 Color: 1
Size: 2902 Color: 0

Bin 150: 38 of cap free
Amount of items: 2
Items: 
Size: 14514 Color: 1
Size: 1784 Color: 0

Bin 151: 41 of cap free
Amount of items: 2
Items: 
Size: 12753 Color: 0
Size: 3542 Color: 1

Bin 152: 41 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 1
Size: 2815 Color: 0

Bin 153: 45 of cap free
Amount of items: 2
Items: 
Size: 10328 Color: 0
Size: 5963 Color: 1

Bin 154: 46 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 0
Size: 5562 Color: 1

Bin 155: 47 of cap free
Amount of items: 2
Items: 
Size: 10703 Color: 1
Size: 5586 Color: 0

Bin 156: 47 of cap free
Amount of items: 2
Items: 
Size: 13479 Color: 1
Size: 2810 Color: 0

Bin 157: 49 of cap free
Amount of items: 2
Items: 
Size: 11662 Color: 1
Size: 4625 Color: 0

Bin 158: 51 of cap free
Amount of items: 2
Items: 
Size: 11462 Color: 0
Size: 4823 Color: 1

Bin 159: 51 of cap free
Amount of items: 2
Items: 
Size: 11669 Color: 0
Size: 4616 Color: 1

Bin 160: 53 of cap free
Amount of items: 2
Items: 
Size: 14115 Color: 1
Size: 2168 Color: 0

Bin 161: 54 of cap free
Amount of items: 2
Items: 
Size: 12558 Color: 0
Size: 3724 Color: 1

Bin 162: 55 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 1
Size: 5369 Color: 0
Size: 1192 Color: 0

Bin 163: 56 of cap free
Amount of items: 3
Items: 
Size: 14439 Color: 1
Size: 1681 Color: 0
Size: 160 Color: 0

Bin 164: 58 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 0
Size: 2158 Color: 1

Bin 165: 59 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 1
Size: 2432 Color: 0

Bin 166: 60 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 0
Size: 4316 Color: 1

Bin 167: 62 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 0
Size: 2074 Color: 1

Bin 168: 66 of cap free
Amount of items: 7
Items: 
Size: 8174 Color: 0
Size: 1468 Color: 1
Size: 1448 Color: 1
Size: 1416 Color: 1
Size: 1414 Color: 0
Size: 1386 Color: 0
Size: 964 Color: 0

Bin 169: 68 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 0
Size: 6064 Color: 1
Size: 784 Color: 0

Bin 170: 70 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 0
Size: 1940 Color: 1

Bin 171: 71 of cap free
Amount of items: 2
Items: 
Size: 12481 Color: 1
Size: 3784 Color: 0

Bin 172: 72 of cap free
Amount of items: 2
Items: 
Size: 8184 Color: 1
Size: 8080 Color: 0

Bin 173: 83 of cap free
Amount of items: 2
Items: 
Size: 13839 Color: 1
Size: 2414 Color: 0

Bin 174: 86 of cap free
Amount of items: 2
Items: 
Size: 14322 Color: 0
Size: 1928 Color: 1

Bin 175: 90 of cap free
Amount of items: 2
Items: 
Size: 10300 Color: 0
Size: 5946 Color: 1

Bin 176: 94 of cap free
Amount of items: 3
Items: 
Size: 9634 Color: 1
Size: 5832 Color: 1
Size: 776 Color: 0

Bin 177: 94 of cap free
Amount of items: 2
Items: 
Size: 13850 Color: 0
Size: 2392 Color: 1

Bin 178: 94 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 1
Size: 2132 Color: 0

Bin 179: 96 of cap free
Amount of items: 2
Items: 
Size: 12606 Color: 1
Size: 3634 Color: 0

Bin 180: 97 of cap free
Amount of items: 3
Items: 
Size: 12374 Color: 0
Size: 2713 Color: 0
Size: 1152 Color: 1

Bin 181: 101 of cap free
Amount of items: 2
Items: 
Size: 14320 Color: 0
Size: 1915 Color: 1

Bin 182: 107 of cap free
Amount of items: 2
Items: 
Size: 12513 Color: 0
Size: 3716 Color: 1

Bin 183: 110 of cap free
Amount of items: 2
Items: 
Size: 11544 Color: 0
Size: 4682 Color: 1

Bin 184: 112 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 0
Size: 2568 Color: 1

Bin 185: 113 of cap free
Amount of items: 2
Items: 
Size: 13824 Color: 1
Size: 2399 Color: 0

Bin 186: 116 of cap free
Amount of items: 2
Items: 
Size: 13033 Color: 1
Size: 3187 Color: 0

Bin 187: 118 of cap free
Amount of items: 2
Items: 
Size: 11164 Color: 0
Size: 5054 Color: 1

Bin 188: 143 of cap free
Amount of items: 2
Items: 
Size: 13389 Color: 1
Size: 2804 Color: 0

Bin 189: 152 of cap free
Amount of items: 2
Items: 
Size: 10364 Color: 1
Size: 5820 Color: 0

Bin 190: 163 of cap free
Amount of items: 2
Items: 
Size: 13607 Color: 0
Size: 2566 Color: 1

Bin 191: 168 of cap free
Amount of items: 2
Items: 
Size: 11868 Color: 0
Size: 4300 Color: 1

Bin 192: 172 of cap free
Amount of items: 2
Items: 
Size: 11128 Color: 0
Size: 5036 Color: 1

Bin 193: 177 of cap free
Amount of items: 2
Items: 
Size: 12097 Color: 1
Size: 4062 Color: 0

Bin 194: 190 of cap free
Amount of items: 2
Items: 
Size: 13782 Color: 1
Size: 2364 Color: 0

Bin 195: 194 of cap free
Amount of items: 2
Items: 
Size: 10274 Color: 0
Size: 5868 Color: 1

Bin 196: 204 of cap free
Amount of items: 3
Items: 
Size: 11800 Color: 1
Size: 2660 Color: 0
Size: 1672 Color: 0

Bin 197: 228 of cap free
Amount of items: 2
Items: 
Size: 9300 Color: 1
Size: 6808 Color: 0

Bin 198: 278 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 0
Size: 6806 Color: 0
Size: 1040 Color: 1

Bin 199: 10168 of cap free
Amount of items: 19
Items: 
Size: 412 Color: 1
Size: 400 Color: 1
Size: 370 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 360 Color: 1
Size: 350 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 308 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 284 Color: 1
Size: 272 Color: 0
Size: 260 Color: 0

Total size: 3234528
Total free space: 16336

