Capicity Bin: 1000001
Lower Bound: 899

Bins used: 900
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 609264 Color: 0
Size: 225448 Color: 1
Size: 165289 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 646741 Color: 0
Size: 184665 Color: 1
Size: 168595 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 515430 Color: 1
Size: 352265 Color: 0
Size: 132306 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 709142 Color: 1
Size: 189592 Color: 0
Size: 101267 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 493262 Color: 1
Size: 343317 Color: 0
Size: 163422 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 552922 Color: 1
Size: 285225 Color: 1
Size: 161854 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 574452 Color: 0
Size: 305019 Color: 1
Size: 120530 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 673908 Color: 1
Size: 179379 Color: 0
Size: 146714 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 570665 Color: 1
Size: 241186 Color: 0
Size: 188150 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 554092 Color: 1
Size: 302799 Color: 0
Size: 143110 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 472020 Color: 0
Size: 331464 Color: 1
Size: 196517 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 542688 Color: 1
Size: 287711 Color: 0
Size: 169602 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 563436 Color: 0
Size: 275803 Color: 1
Size: 160762 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 544214 Color: 1
Size: 349309 Color: 0
Size: 106478 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 468716 Color: 0
Size: 365306 Color: 1
Size: 165979 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 456152 Color: 1
Size: 360959 Color: 0
Size: 182890 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 759512 Color: 0
Size: 125717 Color: 0
Size: 114772 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 471493 Color: 0
Size: 383136 Color: 1
Size: 145372 Color: 1

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 574425 Color: 0
Size: 425576 Color: 1

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 718625 Color: 0
Size: 281376 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 562112 Color: 0
Size: 319713 Color: 1
Size: 118175 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 663567 Color: 1
Size: 174676 Color: 1
Size: 161757 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 551718 Color: 1
Size: 257989 Color: 0
Size: 190293 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 698982 Color: 1
Size: 171007 Color: 1
Size: 130011 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 457488 Color: 0
Size: 294138 Color: 1
Size: 248374 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 739105 Color: 1
Size: 136804 Color: 1
Size: 124091 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 570586 Color: 1
Size: 285928 Color: 1
Size: 143486 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 350280 Color: 0
Size: 330099 Color: 1
Size: 319621 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 745789 Color: 0
Size: 141302 Color: 0
Size: 112909 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 699218 Color: 1
Size: 168963 Color: 1
Size: 131819 Color: 0

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 570695 Color: 0
Size: 429305 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 594829 Color: 0
Size: 405171 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 661174 Color: 1
Size: 186901 Color: 1
Size: 151925 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 737360 Color: 0
Size: 134104 Color: 1
Size: 128536 Color: 1

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 794491 Color: 1
Size: 205509 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 705222 Color: 1
Size: 169083 Color: 0
Size: 125694 Color: 0

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 702567 Color: 1
Size: 150340 Color: 0
Size: 147092 Color: 1

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 724652 Color: 1
Size: 139889 Color: 0
Size: 135458 Color: 1

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 683785 Color: 0
Size: 159532 Color: 1
Size: 156682 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 748825 Color: 0
Size: 143170 Color: 1
Size: 108004 Color: 1

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 609062 Color: 0
Size: 197920 Color: 1
Size: 193017 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 506404 Color: 1
Size: 359044 Color: 0
Size: 134551 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 594058 Color: 0
Size: 243323 Color: 0
Size: 162618 Color: 1

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 542541 Color: 0
Size: 457458 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 595547 Color: 1
Size: 215071 Color: 1
Size: 189381 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 777056 Color: 1
Size: 222943 Color: 0

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 525212 Color: 0
Size: 319637 Color: 1
Size: 155149 Color: 0

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 726402 Color: 1
Size: 139746 Color: 0
Size: 133850 Color: 0

Bin 49: 3 of cap free
Amount of items: 2
Items: 
Size: 622463 Color: 1
Size: 377535 Color: 0

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 739378 Color: 0
Size: 148945 Color: 0
Size: 111675 Color: 1

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 727478 Color: 0
Size: 138615 Color: 1
Size: 133904 Color: 1

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 638897 Color: 1
Size: 205058 Color: 1
Size: 156042 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 502315 Color: 1
Size: 349615 Color: 0
Size: 148067 Color: 1

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 533695 Color: 1
Size: 291282 Color: 0
Size: 175020 Color: 0

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 544370 Color: 1
Size: 455627 Color: 0

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 770503 Color: 0
Size: 229494 Color: 1

Bin 57: 5 of cap free
Amount of items: 3
Items: 
Size: 763211 Color: 0
Size: 132994 Color: 1
Size: 103791 Color: 1

Bin 58: 5 of cap free
Amount of items: 3
Items: 
Size: 784771 Color: 1
Size: 112786 Color: 0
Size: 102439 Color: 1

Bin 59: 5 of cap free
Amount of items: 3
Items: 
Size: 638455 Color: 1
Size: 191551 Color: 0
Size: 169990 Color: 1

Bin 60: 5 of cap free
Amount of items: 3
Items: 
Size: 493115 Color: 1
Size: 324175 Color: 1
Size: 182706 Color: 0

Bin 61: 5 of cap free
Amount of items: 3
Items: 
Size: 438493 Color: 1
Size: 377597 Color: 0
Size: 183906 Color: 0

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 655615 Color: 0
Size: 344381 Color: 1

Bin 63: 6 of cap free
Amount of items: 3
Items: 
Size: 708947 Color: 1
Size: 171108 Color: 1
Size: 119940 Color: 0

Bin 64: 6 of cap free
Amount of items: 3
Items: 
Size: 563321 Color: 0
Size: 285370 Color: 1
Size: 151304 Color: 1

Bin 65: 6 of cap free
Amount of items: 3
Items: 
Size: 702414 Color: 1
Size: 163831 Color: 0
Size: 133750 Color: 0

Bin 66: 6 of cap free
Amount of items: 3
Items: 
Size: 557117 Color: 1
Size: 306154 Color: 0
Size: 136724 Color: 0

Bin 67: 6 of cap free
Amount of items: 3
Items: 
Size: 546769 Color: 1
Size: 284410 Color: 0
Size: 168816 Color: 1

Bin 68: 6 of cap free
Amount of items: 3
Items: 
Size: 448769 Color: 1
Size: 370573 Color: 1
Size: 180653 Color: 0

Bin 69: 7 of cap free
Amount of items: 3
Items: 
Size: 759847 Color: 0
Size: 132133 Color: 0
Size: 108014 Color: 1

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 759549 Color: 1
Size: 125955 Color: 0
Size: 114490 Color: 1

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 750202 Color: 0
Size: 249792 Color: 1

Bin 72: 7 of cap free
Amount of items: 3
Items: 
Size: 661854 Color: 0
Size: 171043 Color: 1
Size: 167097 Color: 0

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 718940 Color: 1
Size: 146582 Color: 1
Size: 134471 Color: 0

Bin 74: 8 of cap free
Amount of items: 3
Items: 
Size: 724072 Color: 1
Size: 146393 Color: 1
Size: 129528 Color: 0

Bin 75: 9 of cap free
Amount of items: 3
Items: 
Size: 683540 Color: 1
Size: 161670 Color: 0
Size: 154782 Color: 0

Bin 76: 9 of cap free
Amount of items: 3
Items: 
Size: 707433 Color: 1
Size: 147807 Color: 0
Size: 144752 Color: 1

Bin 77: 9 of cap free
Amount of items: 3
Items: 
Size: 493903 Color: 1
Size: 324902 Color: 0
Size: 181187 Color: 1

Bin 78: 10 of cap free
Amount of items: 3
Items: 
Size: 739722 Color: 1
Size: 132020 Color: 0
Size: 128249 Color: 0

Bin 79: 10 of cap free
Amount of items: 3
Items: 
Size: 660379 Color: 0
Size: 176758 Color: 1
Size: 162854 Color: 0

Bin 80: 10 of cap free
Amount of items: 3
Items: 
Size: 686078 Color: 1
Size: 195664 Color: 0
Size: 118249 Color: 0

Bin 81: 10 of cap free
Amount of items: 3
Items: 
Size: 638257 Color: 0
Size: 181104 Color: 1
Size: 180630 Color: 1

Bin 82: 11 of cap free
Amount of items: 3
Items: 
Size: 727506 Color: 1
Size: 145436 Color: 1
Size: 127048 Color: 0

Bin 83: 11 of cap free
Amount of items: 3
Items: 
Size: 574035 Color: 0
Size: 271417 Color: 0
Size: 154538 Color: 1

Bin 84: 11 of cap free
Amount of items: 3
Items: 
Size: 641197 Color: 0
Size: 186358 Color: 1
Size: 172435 Color: 1

Bin 85: 11 of cap free
Amount of items: 3
Items: 
Size: 558215 Color: 0
Size: 303557 Color: 1
Size: 138218 Color: 1

Bin 86: 11 of cap free
Amount of items: 3
Items: 
Size: 582836 Color: 1
Size: 274214 Color: 1
Size: 142940 Color: 0

Bin 87: 12 of cap free
Amount of items: 3
Items: 
Size: 454556 Color: 1
Size: 380882 Color: 0
Size: 164551 Color: 1

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 733890 Color: 1
Size: 266099 Color: 0

Bin 89: 13 of cap free
Amount of items: 3
Items: 
Size: 658174 Color: 1
Size: 185881 Color: 0
Size: 155933 Color: 0

Bin 90: 13 of cap free
Amount of items: 2
Items: 
Size: 507647 Color: 1
Size: 492341 Color: 0

Bin 91: 13 of cap free
Amount of items: 2
Items: 
Size: 543728 Color: 0
Size: 456260 Color: 1

Bin 92: 13 of cap free
Amount of items: 3
Items: 
Size: 663213 Color: 0
Size: 170767 Color: 1
Size: 166008 Color: 0

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 720736 Color: 0
Size: 279252 Color: 1

Bin 94: 15 of cap free
Amount of items: 3
Items: 
Size: 495471 Color: 1
Size: 324678 Color: 0
Size: 179837 Color: 0

Bin 95: 15 of cap free
Amount of items: 3
Items: 
Size: 593983 Color: 0
Size: 245730 Color: 0
Size: 160273 Color: 1

Bin 96: 15 of cap free
Amount of items: 2
Items: 
Size: 554070 Color: 0
Size: 445916 Color: 1

Bin 97: 16 of cap free
Amount of items: 2
Items: 
Size: 599740 Color: 1
Size: 400245 Color: 0

Bin 98: 16 of cap free
Amount of items: 3
Items: 
Size: 718167 Color: 0
Size: 145953 Color: 0
Size: 135865 Color: 1

Bin 99: 17 of cap free
Amount of items: 3
Items: 
Size: 760323 Color: 0
Size: 139375 Color: 0
Size: 100286 Color: 1

Bin 100: 17 of cap free
Amount of items: 2
Items: 
Size: 779805 Color: 0
Size: 220179 Color: 1

Bin 101: 17 of cap free
Amount of items: 2
Items: 
Size: 759408 Color: 1
Size: 240576 Color: 0

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 529384 Color: 1
Size: 470600 Color: 0

Bin 103: 17 of cap free
Amount of items: 2
Items: 
Size: 560595 Color: 1
Size: 439389 Color: 0

Bin 104: 18 of cap free
Amount of items: 2
Items: 
Size: 552311 Color: 1
Size: 447672 Color: 0

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 649490 Color: 0
Size: 350492 Color: 1

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 738519 Color: 0
Size: 133180 Color: 1
Size: 128282 Color: 1

Bin 107: 20 of cap free
Amount of items: 3
Items: 
Size: 580096 Color: 0
Size: 227540 Color: 1
Size: 192345 Color: 0

Bin 108: 20 of cap free
Amount of items: 3
Items: 
Size: 662862 Color: 1
Size: 174577 Color: 1
Size: 162542 Color: 0

Bin 109: 21 of cap free
Amount of items: 3
Items: 
Size: 762438 Color: 0
Size: 131028 Color: 0
Size: 106514 Color: 1

Bin 110: 21 of cap free
Amount of items: 3
Items: 
Size: 701038 Color: 0
Size: 154984 Color: 0
Size: 143958 Color: 1

Bin 111: 21 of cap free
Amount of items: 3
Items: 
Size: 578315 Color: 0
Size: 229938 Color: 1
Size: 191727 Color: 1

Bin 112: 22 of cap free
Amount of items: 2
Items: 
Size: 614580 Color: 0
Size: 385399 Color: 1

Bin 113: 23 of cap free
Amount of items: 3
Items: 
Size: 504011 Color: 1
Size: 327775 Color: 0
Size: 168192 Color: 1

Bin 114: 23 of cap free
Amount of items: 2
Items: 
Size: 541921 Color: 1
Size: 458057 Color: 0

Bin 115: 23 of cap free
Amount of items: 3
Items: 
Size: 579781 Color: 0
Size: 242949 Color: 0
Size: 177248 Color: 1

Bin 116: 24 of cap free
Amount of items: 3
Items: 
Size: 574351 Color: 1
Size: 243682 Color: 0
Size: 181944 Color: 1

Bin 117: 24 of cap free
Amount of items: 2
Items: 
Size: 602997 Color: 1
Size: 396980 Color: 0

Bin 118: 25 of cap free
Amount of items: 2
Items: 
Size: 640355 Color: 1
Size: 359621 Color: 0

Bin 119: 25 of cap free
Amount of items: 2
Items: 
Size: 603929 Color: 0
Size: 396047 Color: 1

Bin 120: 25 of cap free
Amount of items: 2
Items: 
Size: 685159 Color: 1
Size: 314817 Color: 0

Bin 121: 25 of cap free
Amount of items: 2
Items: 
Size: 796089 Color: 1
Size: 203887 Color: 0

Bin 122: 27 of cap free
Amount of items: 3
Items: 
Size: 650619 Color: 1
Size: 193051 Color: 0
Size: 156304 Color: 0

Bin 123: 27 of cap free
Amount of items: 2
Items: 
Size: 653649 Color: 0
Size: 346325 Color: 1

Bin 124: 28 of cap free
Amount of items: 3
Items: 
Size: 760986 Color: 1
Size: 124465 Color: 1
Size: 114522 Color: 0

Bin 125: 28 of cap free
Amount of items: 3
Items: 
Size: 698371 Color: 1
Size: 154212 Color: 0
Size: 147390 Color: 1

Bin 126: 28 of cap free
Amount of items: 3
Items: 
Size: 640532 Color: 0
Size: 180529 Color: 0
Size: 178912 Color: 1

Bin 127: 28 of cap free
Amount of items: 2
Items: 
Size: 723340 Color: 0
Size: 276633 Color: 1

Bin 128: 28 of cap free
Amount of items: 2
Items: 
Size: 692029 Color: 1
Size: 307944 Color: 0

Bin 129: 29 of cap free
Amount of items: 2
Items: 
Size: 584840 Color: 0
Size: 415132 Color: 1

Bin 130: 29 of cap free
Amount of items: 3
Items: 
Size: 638462 Color: 0
Size: 187752 Color: 0
Size: 173758 Color: 1

Bin 131: 29 of cap free
Amount of items: 2
Items: 
Size: 790941 Color: 0
Size: 209031 Color: 1

Bin 132: 30 of cap free
Amount of items: 3
Items: 
Size: 662365 Color: 0
Size: 183593 Color: 0
Size: 154013 Color: 1

Bin 133: 30 of cap free
Amount of items: 3
Items: 
Size: 593139 Color: 1
Size: 214678 Color: 1
Size: 192154 Color: 0

Bin 134: 31 of cap free
Amount of items: 2
Items: 
Size: 668782 Color: 0
Size: 331188 Color: 1

Bin 135: 33 of cap free
Amount of items: 3
Items: 
Size: 760009 Color: 0
Size: 138416 Color: 1
Size: 101543 Color: 1

Bin 136: 33 of cap free
Amount of items: 2
Items: 
Size: 740857 Color: 0
Size: 259111 Color: 1

Bin 137: 34 of cap free
Amount of items: 3
Items: 
Size: 641475 Color: 0
Size: 193487 Color: 1
Size: 165005 Color: 1

Bin 138: 34 of cap free
Amount of items: 2
Items: 
Size: 603665 Color: 0
Size: 396302 Color: 1

Bin 139: 35 of cap free
Amount of items: 3
Items: 
Size: 729890 Color: 1
Size: 145894 Color: 1
Size: 124182 Color: 0

Bin 140: 36 of cap free
Amount of items: 2
Items: 
Size: 570106 Color: 0
Size: 429859 Color: 1

Bin 141: 36 of cap free
Amount of items: 2
Items: 
Size: 773603 Color: 0
Size: 226362 Color: 1

Bin 142: 37 of cap free
Amount of items: 3
Items: 
Size: 702525 Color: 1
Size: 170556 Color: 0
Size: 126883 Color: 1

Bin 143: 37 of cap free
Amount of items: 2
Items: 
Size: 696794 Color: 0
Size: 303170 Color: 1

Bin 144: 38 of cap free
Amount of items: 2
Items: 
Size: 629404 Color: 1
Size: 370559 Color: 0

Bin 145: 39 of cap free
Amount of items: 2
Items: 
Size: 585376 Color: 0
Size: 414586 Color: 1

Bin 146: 40 of cap free
Amount of items: 2
Items: 
Size: 763687 Color: 0
Size: 236274 Color: 1

Bin 147: 40 of cap free
Amount of items: 3
Items: 
Size: 697518 Color: 1
Size: 156391 Color: 1
Size: 146052 Color: 0

Bin 148: 41 of cap free
Amount of items: 2
Items: 
Size: 526831 Color: 0
Size: 473129 Color: 1

Bin 149: 43 of cap free
Amount of items: 2
Items: 
Size: 699667 Color: 1
Size: 300291 Color: 0

Bin 150: 43 of cap free
Amount of items: 2
Items: 
Size: 680651 Color: 0
Size: 319307 Color: 1

Bin 151: 44 of cap free
Amount of items: 3
Items: 
Size: 439400 Color: 1
Size: 383451 Color: 0
Size: 177106 Color: 1

Bin 152: 44 of cap free
Amount of items: 2
Items: 
Size: 791654 Color: 1
Size: 208303 Color: 0

Bin 153: 45 of cap free
Amount of items: 3
Items: 
Size: 593617 Color: 1
Size: 246402 Color: 0
Size: 159937 Color: 1

Bin 154: 45 of cap free
Amount of items: 3
Items: 
Size: 495168 Color: 1
Size: 308141 Color: 0
Size: 196647 Color: 0

Bin 155: 45 of cap free
Amount of items: 2
Items: 
Size: 616171 Color: 0
Size: 383785 Color: 1

Bin 156: 46 of cap free
Amount of items: 3
Items: 
Size: 765199 Color: 1
Size: 117640 Color: 1
Size: 117116 Color: 0

Bin 157: 47 of cap free
Amount of items: 3
Items: 
Size: 698780 Color: 0
Size: 162909 Color: 0
Size: 138265 Color: 1

Bin 158: 47 of cap free
Amount of items: 3
Items: 
Size: 760801 Color: 1
Size: 121766 Color: 0
Size: 117387 Color: 0

Bin 159: 47 of cap free
Amount of items: 3
Items: 
Size: 778343 Color: 0
Size: 112378 Color: 0
Size: 109233 Color: 1

Bin 160: 47 of cap free
Amount of items: 2
Items: 
Size: 619530 Color: 0
Size: 380424 Color: 1

Bin 161: 47 of cap free
Amount of items: 2
Items: 
Size: 580879 Color: 1
Size: 419075 Color: 0

Bin 162: 49 of cap free
Amount of items: 2
Items: 
Size: 574138 Color: 0
Size: 425814 Color: 1

Bin 163: 49 of cap free
Amount of items: 2
Items: 
Size: 775424 Color: 0
Size: 224528 Color: 1

Bin 164: 50 of cap free
Amount of items: 3
Items: 
Size: 788520 Color: 1
Size: 106838 Color: 1
Size: 104593 Color: 0

Bin 165: 50 of cap free
Amount of items: 3
Items: 
Size: 456020 Color: 1
Size: 375120 Color: 0
Size: 168811 Color: 0

Bin 166: 52 of cap free
Amount of items: 3
Items: 
Size: 775217 Color: 0
Size: 112961 Color: 1
Size: 111771 Color: 1

Bin 167: 52 of cap free
Amount of items: 3
Items: 
Size: 594061 Color: 0
Size: 264981 Color: 0
Size: 140907 Color: 1

Bin 168: 52 of cap free
Amount of items: 2
Items: 
Size: 756782 Color: 0
Size: 243167 Color: 1

Bin 169: 53 of cap free
Amount of items: 3
Items: 
Size: 350683 Color: 0
Size: 344640 Color: 1
Size: 304625 Color: 0

Bin 170: 53 of cap free
Amount of items: 2
Items: 
Size: 625936 Color: 1
Size: 374012 Color: 0

Bin 171: 54 of cap free
Amount of items: 2
Items: 
Size: 709070 Color: 1
Size: 290877 Color: 0

Bin 172: 55 of cap free
Amount of items: 2
Items: 
Size: 542456 Color: 1
Size: 457490 Color: 0

Bin 173: 55 of cap free
Amount of items: 2
Items: 
Size: 556406 Color: 0
Size: 443540 Color: 1

Bin 174: 55 of cap free
Amount of items: 2
Items: 
Size: 670655 Color: 0
Size: 329291 Color: 1

Bin 175: 56 of cap free
Amount of items: 2
Items: 
Size: 539492 Color: 1
Size: 460453 Color: 0

Bin 176: 58 of cap free
Amount of items: 2
Items: 
Size: 760327 Color: 1
Size: 239616 Color: 0

Bin 177: 59 of cap free
Amount of items: 3
Items: 
Size: 552077 Color: 1
Size: 294231 Color: 1
Size: 153634 Color: 0

Bin 178: 60 of cap free
Amount of items: 2
Items: 
Size: 773352 Color: 1
Size: 226589 Color: 0

Bin 179: 60 of cap free
Amount of items: 2
Items: 
Size: 507802 Color: 1
Size: 492139 Color: 0

Bin 180: 60 of cap free
Amount of items: 2
Items: 
Size: 519451 Color: 1
Size: 480490 Color: 0

Bin 181: 60 of cap free
Amount of items: 2
Items: 
Size: 743361 Color: 1
Size: 256580 Color: 0

Bin 182: 61 of cap free
Amount of items: 2
Items: 
Size: 793174 Color: 1
Size: 206766 Color: 0

Bin 183: 62 of cap free
Amount of items: 2
Items: 
Size: 715179 Color: 1
Size: 284760 Color: 0

Bin 184: 62 of cap free
Amount of items: 3
Items: 
Size: 699713 Color: 0
Size: 193180 Color: 0
Size: 107046 Color: 1

Bin 185: 62 of cap free
Amount of items: 3
Items: 
Size: 728255 Color: 0
Size: 163486 Color: 0
Size: 108198 Color: 1

Bin 186: 62 of cap free
Amount of items: 2
Items: 
Size: 608395 Color: 1
Size: 391544 Color: 0

Bin 187: 63 of cap free
Amount of items: 3
Items: 
Size: 655734 Color: 0
Size: 178372 Color: 0
Size: 165832 Color: 1

Bin 188: 63 of cap free
Amount of items: 2
Items: 
Size: 708220 Color: 0
Size: 291718 Color: 1

Bin 189: 64 of cap free
Amount of items: 2
Items: 
Size: 776035 Color: 1
Size: 223902 Color: 0

Bin 190: 65 of cap free
Amount of items: 3
Items: 
Size: 640876 Color: 0
Size: 196361 Color: 1
Size: 162699 Color: 0

Bin 191: 65 of cap free
Amount of items: 2
Items: 
Size: 799921 Color: 1
Size: 200015 Color: 0

Bin 192: 68 of cap free
Amount of items: 3
Items: 
Size: 595836 Color: 0
Size: 244475 Color: 1
Size: 159622 Color: 0

Bin 193: 68 of cap free
Amount of items: 2
Items: 
Size: 758933 Color: 1
Size: 241000 Color: 0

Bin 194: 69 of cap free
Amount of items: 2
Items: 
Size: 594798 Color: 0
Size: 405134 Color: 1

Bin 195: 69 of cap free
Amount of items: 2
Items: 
Size: 602605 Color: 1
Size: 397327 Color: 0

Bin 196: 69 of cap free
Amount of items: 2
Items: 
Size: 796132 Color: 1
Size: 203800 Color: 0

Bin 197: 71 of cap free
Amount of items: 3
Items: 
Size: 724607 Color: 1
Size: 151206 Color: 0
Size: 124117 Color: 1

Bin 198: 71 of cap free
Amount of items: 3
Items: 
Size: 637454 Color: 0
Size: 197882 Color: 0
Size: 164594 Color: 1

Bin 199: 71 of cap free
Amount of items: 2
Items: 
Size: 527395 Color: 1
Size: 472535 Color: 0

Bin 200: 72 of cap free
Amount of items: 2
Items: 
Size: 506169 Color: 1
Size: 493760 Color: 0

Bin 201: 72 of cap free
Amount of items: 2
Items: 
Size: 607352 Color: 1
Size: 392577 Color: 0

Bin 202: 73 of cap free
Amount of items: 2
Items: 
Size: 564975 Color: 1
Size: 434953 Color: 0

Bin 203: 75 of cap free
Amount of items: 2
Items: 
Size: 766611 Color: 0
Size: 233315 Color: 1

Bin 204: 76 of cap free
Amount of items: 2
Items: 
Size: 718696 Color: 1
Size: 281229 Color: 0

Bin 205: 77 of cap free
Amount of items: 2
Items: 
Size: 737181 Color: 0
Size: 262743 Color: 1

Bin 206: 77 of cap free
Amount of items: 2
Items: 
Size: 744439 Color: 0
Size: 255485 Color: 1

Bin 207: 79 of cap free
Amount of items: 3
Items: 
Size: 528132 Color: 0
Size: 305589 Color: 1
Size: 166201 Color: 1

Bin 208: 80 of cap free
Amount of items: 3
Items: 
Size: 698189 Color: 1
Size: 166223 Color: 0
Size: 135509 Color: 0

Bin 209: 85 of cap free
Amount of items: 2
Items: 
Size: 675892 Color: 1
Size: 324024 Color: 0

Bin 210: 86 of cap free
Amount of items: 2
Items: 
Size: 631091 Color: 0
Size: 368824 Color: 1

Bin 211: 86 of cap free
Amount of items: 2
Items: 
Size: 781424 Color: 0
Size: 218491 Color: 1

Bin 212: 87 of cap free
Amount of items: 2
Items: 
Size: 685876 Color: 1
Size: 314038 Color: 0

Bin 213: 87 of cap free
Amount of items: 2
Items: 
Size: 787911 Color: 0
Size: 212003 Color: 1

Bin 214: 88 of cap free
Amount of items: 3
Items: 
Size: 521007 Color: 1
Size: 294762 Color: 1
Size: 184144 Color: 0

Bin 215: 89 of cap free
Amount of items: 2
Items: 
Size: 519678 Color: 0
Size: 480234 Color: 1

Bin 216: 90 of cap free
Amount of items: 2
Items: 
Size: 770340 Color: 0
Size: 229571 Color: 1

Bin 217: 90 of cap free
Amount of items: 2
Items: 
Size: 643251 Color: 0
Size: 356660 Color: 1

Bin 218: 91 of cap free
Amount of items: 3
Items: 
Size: 637967 Color: 1
Size: 185193 Color: 0
Size: 176750 Color: 0

Bin 219: 92 of cap free
Amount of items: 2
Items: 
Size: 608764 Color: 0
Size: 391145 Color: 1

Bin 220: 92 of cap free
Amount of items: 2
Items: 
Size: 616904 Color: 1
Size: 383005 Color: 0

Bin 221: 95 of cap free
Amount of items: 3
Items: 
Size: 534401 Color: 0
Size: 305418 Color: 1
Size: 160087 Color: 0

Bin 222: 95 of cap free
Amount of items: 3
Items: 
Size: 573579 Color: 0
Size: 287018 Color: 1
Size: 139309 Color: 0

Bin 223: 98 of cap free
Amount of items: 3
Items: 
Size: 710908 Color: 1
Size: 152000 Color: 1
Size: 136995 Color: 0

Bin 224: 98 of cap free
Amount of items: 2
Items: 
Size: 607928 Color: 1
Size: 391975 Color: 0

Bin 225: 99 of cap free
Amount of items: 2
Items: 
Size: 736688 Color: 0
Size: 263214 Color: 1

Bin 226: 99 of cap free
Amount of items: 2
Items: 
Size: 766997 Color: 0
Size: 232905 Color: 1

Bin 227: 100 of cap free
Amount of items: 2
Items: 
Size: 531664 Color: 1
Size: 468237 Color: 0

Bin 228: 100 of cap free
Amount of items: 2
Items: 
Size: 596441 Color: 1
Size: 403460 Color: 0

Bin 229: 104 of cap free
Amount of items: 2
Items: 
Size: 686626 Color: 1
Size: 313271 Color: 0

Bin 230: 104 of cap free
Amount of items: 2
Items: 
Size: 599177 Color: 0
Size: 400720 Color: 1

Bin 231: 105 of cap free
Amount of items: 2
Items: 
Size: 730373 Color: 0
Size: 269523 Color: 1

Bin 232: 106 of cap free
Amount of items: 2
Items: 
Size: 731597 Color: 0
Size: 268298 Color: 1

Bin 233: 106 of cap free
Amount of items: 2
Items: 
Size: 619266 Color: 0
Size: 380629 Color: 1

Bin 234: 106 of cap free
Amount of items: 2
Items: 
Size: 782361 Color: 0
Size: 217534 Color: 1

Bin 235: 107 of cap free
Amount of items: 2
Items: 
Size: 514941 Color: 1
Size: 484953 Color: 0

Bin 236: 108 of cap free
Amount of items: 2
Items: 
Size: 529111 Color: 1
Size: 470782 Color: 0

Bin 237: 108 of cap free
Amount of items: 2
Items: 
Size: 751628 Color: 0
Size: 248265 Color: 1

Bin 238: 109 of cap free
Amount of items: 2
Items: 
Size: 513393 Color: 0
Size: 486499 Color: 1

Bin 239: 110 of cap free
Amount of items: 2
Items: 
Size: 666493 Color: 0
Size: 333398 Color: 1

Bin 240: 110 of cap free
Amount of items: 3
Items: 
Size: 758970 Color: 0
Size: 128577 Color: 0
Size: 112344 Color: 1

Bin 241: 111 of cap free
Amount of items: 3
Items: 
Size: 592897 Color: 1
Size: 246316 Color: 0
Size: 160677 Color: 1

Bin 242: 111 of cap free
Amount of items: 2
Items: 
Size: 677965 Color: 0
Size: 321925 Color: 1

Bin 243: 111 of cap free
Amount of items: 2
Items: 
Size: 710048 Color: 1
Size: 289842 Color: 0

Bin 244: 111 of cap free
Amount of items: 2
Items: 
Size: 764528 Color: 0
Size: 235362 Color: 1

Bin 245: 112 of cap free
Amount of items: 2
Items: 
Size: 611160 Color: 0
Size: 388729 Color: 1

Bin 246: 113 of cap free
Amount of items: 2
Items: 
Size: 527743 Color: 1
Size: 472145 Color: 0

Bin 247: 114 of cap free
Amount of items: 2
Items: 
Size: 602919 Color: 0
Size: 396968 Color: 1

Bin 248: 114 of cap free
Amount of items: 2
Items: 
Size: 656323 Color: 0
Size: 343564 Color: 1

Bin 249: 115 of cap free
Amount of items: 2
Items: 
Size: 773026 Color: 1
Size: 226860 Color: 0

Bin 250: 116 of cap free
Amount of items: 2
Items: 
Size: 687030 Color: 1
Size: 312855 Color: 0

Bin 251: 118 of cap free
Amount of items: 3
Items: 
Size: 661308 Color: 0
Size: 193764 Color: 1
Size: 144811 Color: 1

Bin 252: 118 of cap free
Amount of items: 2
Items: 
Size: 565041 Color: 0
Size: 434842 Color: 1

Bin 253: 119 of cap free
Amount of items: 3
Items: 
Size: 477138 Color: 1
Size: 351473 Color: 0
Size: 171271 Color: 0

Bin 254: 119 of cap free
Amount of items: 2
Items: 
Size: 560654 Color: 0
Size: 439228 Color: 1

Bin 255: 120 of cap free
Amount of items: 2
Items: 
Size: 794619 Color: 1
Size: 205262 Color: 0

Bin 256: 121 of cap free
Amount of items: 2
Items: 
Size: 779048 Color: 1
Size: 220832 Color: 0

Bin 257: 123 of cap free
Amount of items: 2
Items: 
Size: 583243 Color: 0
Size: 416635 Color: 1

Bin 258: 124 of cap free
Amount of items: 3
Items: 
Size: 661936 Color: 0
Size: 170609 Color: 1
Size: 167332 Color: 1

Bin 259: 124 of cap free
Amount of items: 2
Items: 
Size: 770076 Color: 1
Size: 229801 Color: 0

Bin 260: 124 of cap free
Amount of items: 2
Items: 
Size: 787736 Color: 1
Size: 212141 Color: 0

Bin 261: 126 of cap free
Amount of items: 3
Items: 
Size: 561262 Color: 1
Size: 288045 Color: 1
Size: 150568 Color: 0

Bin 262: 127 of cap free
Amount of items: 3
Items: 
Size: 772531 Color: 1
Size: 117463 Color: 0
Size: 109880 Color: 1

Bin 263: 127 of cap free
Amount of items: 2
Items: 
Size: 713388 Color: 1
Size: 286486 Color: 0

Bin 264: 127 of cap free
Amount of items: 2
Items: 
Size: 772239 Color: 1
Size: 227635 Color: 0

Bin 265: 128 of cap free
Amount of items: 2
Items: 
Size: 754106 Color: 1
Size: 245767 Color: 0

Bin 266: 128 of cap free
Amount of items: 2
Items: 
Size: 716729 Color: 0
Size: 283144 Color: 1

Bin 267: 129 of cap free
Amount of items: 3
Items: 
Size: 724156 Color: 1
Size: 138366 Color: 0
Size: 137350 Color: 1

Bin 268: 129 of cap free
Amount of items: 2
Items: 
Size: 726636 Color: 0
Size: 273236 Color: 1

Bin 269: 129 of cap free
Amount of items: 3
Items: 
Size: 718698 Color: 0
Size: 166508 Color: 1
Size: 114666 Color: 1

Bin 270: 129 of cap free
Amount of items: 2
Items: 
Size: 577090 Color: 1
Size: 422782 Color: 0

Bin 271: 129 of cap free
Amount of items: 2
Items: 
Size: 743209 Color: 0
Size: 256663 Color: 1

Bin 272: 131 of cap free
Amount of items: 2
Items: 
Size: 687039 Color: 0
Size: 312831 Color: 1

Bin 273: 132 of cap free
Amount of items: 2
Items: 
Size: 635573 Color: 0
Size: 364296 Color: 1

Bin 274: 132 of cap free
Amount of items: 2
Items: 
Size: 568307 Color: 1
Size: 431562 Color: 0

Bin 275: 133 of cap free
Amount of items: 2
Items: 
Size: 518075 Color: 0
Size: 481793 Color: 1

Bin 276: 136 of cap free
Amount of items: 2
Items: 
Size: 540372 Color: 0
Size: 459493 Color: 1

Bin 277: 137 of cap free
Amount of items: 3
Items: 
Size: 679694 Color: 0
Size: 182891 Color: 0
Size: 137279 Color: 1

Bin 278: 137 of cap free
Amount of items: 2
Items: 
Size: 587537 Color: 1
Size: 412327 Color: 0

Bin 279: 138 of cap free
Amount of items: 2
Items: 
Size: 503949 Color: 0
Size: 495914 Color: 1

Bin 280: 138 of cap free
Amount of items: 2
Items: 
Size: 548141 Color: 0
Size: 451722 Color: 1

Bin 281: 141 of cap free
Amount of items: 3
Items: 
Size: 478948 Color: 1
Size: 375027 Color: 0
Size: 145885 Color: 1

Bin 282: 144 of cap free
Amount of items: 2
Items: 
Size: 696082 Color: 1
Size: 303775 Color: 0

Bin 283: 146 of cap free
Amount of items: 2
Items: 
Size: 696954 Color: 1
Size: 302901 Color: 0

Bin 284: 147 of cap free
Amount of items: 2
Items: 
Size: 797006 Color: 1
Size: 202848 Color: 0

Bin 285: 148 of cap free
Amount of items: 2
Items: 
Size: 677618 Color: 0
Size: 322235 Color: 1

Bin 286: 150 of cap free
Amount of items: 2
Items: 
Size: 531497 Color: 0
Size: 468354 Color: 1

Bin 287: 152 of cap free
Amount of items: 2
Items: 
Size: 756890 Color: 1
Size: 242959 Color: 0

Bin 288: 154 of cap free
Amount of items: 3
Items: 
Size: 717745 Color: 1
Size: 145082 Color: 0
Size: 137020 Color: 0

Bin 289: 154 of cap free
Amount of items: 2
Items: 
Size: 527150 Color: 1
Size: 472697 Color: 0

Bin 290: 154 of cap free
Amount of items: 2
Items: 
Size: 591555 Color: 1
Size: 408292 Color: 0

Bin 291: 157 of cap free
Amount of items: 3
Items: 
Size: 489615 Color: 1
Size: 337390 Color: 0
Size: 172839 Color: 0

Bin 292: 160 of cap free
Amount of items: 2
Items: 
Size: 566800 Color: 1
Size: 433041 Color: 0

Bin 293: 162 of cap free
Amount of items: 2
Items: 
Size: 733048 Color: 0
Size: 266791 Color: 1

Bin 294: 164 of cap free
Amount of items: 2
Items: 
Size: 600729 Color: 1
Size: 399108 Color: 0

Bin 295: 166 of cap free
Amount of items: 2
Items: 
Size: 531990 Color: 1
Size: 467845 Color: 0

Bin 296: 166 of cap free
Amount of items: 2
Items: 
Size: 584860 Color: 1
Size: 414975 Color: 0

Bin 297: 167 of cap free
Amount of items: 3
Items: 
Size: 738633 Color: 0
Size: 143381 Color: 0
Size: 117820 Color: 1

Bin 298: 167 of cap free
Amount of items: 2
Items: 
Size: 699316 Color: 0
Size: 300518 Color: 1

Bin 299: 169 of cap free
Amount of items: 3
Items: 
Size: 477455 Color: 1
Size: 363728 Color: 1
Size: 158649 Color: 0

Bin 300: 171 of cap free
Amount of items: 2
Items: 
Size: 507125 Color: 1
Size: 492705 Color: 0

Bin 301: 171 of cap free
Amount of items: 2
Items: 
Size: 597063 Color: 0
Size: 402767 Color: 1

Bin 302: 171 of cap free
Amount of items: 2
Items: 
Size: 696597 Color: 0
Size: 303233 Color: 1

Bin 303: 171 of cap free
Amount of items: 2
Items: 
Size: 722117 Color: 0
Size: 277713 Color: 1

Bin 304: 172 of cap free
Amount of items: 2
Items: 
Size: 551316 Color: 1
Size: 448513 Color: 0

Bin 305: 173 of cap free
Amount of items: 2
Items: 
Size: 604650 Color: 0
Size: 395178 Color: 1

Bin 306: 174 of cap free
Amount of items: 2
Items: 
Size: 677250 Color: 1
Size: 322577 Color: 0

Bin 307: 175 of cap free
Amount of items: 2
Items: 
Size: 693078 Color: 1
Size: 306748 Color: 0

Bin 308: 183 of cap free
Amount of items: 3
Items: 
Size: 529276 Color: 1
Size: 313849 Color: 1
Size: 156693 Color: 0

Bin 309: 185 of cap free
Amount of items: 2
Items: 
Size: 637504 Color: 0
Size: 362312 Color: 1

Bin 310: 186 of cap free
Amount of items: 2
Items: 
Size: 783113 Color: 0
Size: 216702 Color: 1

Bin 311: 190 of cap free
Amount of items: 2
Items: 
Size: 789047 Color: 1
Size: 210764 Color: 0

Bin 312: 191 of cap free
Amount of items: 2
Items: 
Size: 705057 Color: 0
Size: 294753 Color: 1

Bin 313: 192 of cap free
Amount of items: 2
Items: 
Size: 712414 Color: 0
Size: 287395 Color: 1

Bin 314: 193 of cap free
Amount of items: 2
Items: 
Size: 554887 Color: 1
Size: 444921 Color: 0

Bin 315: 195 of cap free
Amount of items: 2
Items: 
Size: 585589 Color: 0
Size: 414217 Color: 1

Bin 316: 197 of cap free
Amount of items: 2
Items: 
Size: 570151 Color: 1
Size: 429653 Color: 0

Bin 317: 200 of cap free
Amount of items: 2
Items: 
Size: 550247 Color: 1
Size: 449554 Color: 0

Bin 318: 201 of cap free
Amount of items: 2
Items: 
Size: 650346 Color: 0
Size: 349454 Color: 1

Bin 319: 202 of cap free
Amount of items: 3
Items: 
Size: 544322 Color: 1
Size: 275172 Color: 1
Size: 180305 Color: 0

Bin 320: 202 of cap free
Amount of items: 2
Items: 
Size: 517107 Color: 0
Size: 482692 Color: 1

Bin 321: 202 of cap free
Amount of items: 2
Items: 
Size: 737373 Color: 0
Size: 262426 Color: 1

Bin 322: 202 of cap free
Amount of items: 2
Items: 
Size: 742569 Color: 0
Size: 257230 Color: 1

Bin 323: 204 of cap free
Amount of items: 2
Items: 
Size: 739541 Color: 1
Size: 260256 Color: 0

Bin 324: 204 of cap free
Amount of items: 2
Items: 
Size: 514545 Color: 0
Size: 485252 Color: 1

Bin 325: 205 of cap free
Amount of items: 2
Items: 
Size: 765619 Color: 0
Size: 234177 Color: 1

Bin 326: 206 of cap free
Amount of items: 2
Items: 
Size: 573456 Color: 0
Size: 426339 Color: 1

Bin 327: 207 of cap free
Amount of items: 3
Items: 
Size: 699575 Color: 1
Size: 169153 Color: 0
Size: 131066 Color: 1

Bin 328: 211 of cap free
Amount of items: 3
Items: 
Size: 788601 Color: 1
Size: 108608 Color: 0
Size: 102581 Color: 0

Bin 329: 212 of cap free
Amount of items: 3
Items: 
Size: 565896 Color: 1
Size: 305812 Color: 0
Size: 128081 Color: 0

Bin 330: 212 of cap free
Amount of items: 2
Items: 
Size: 618231 Color: 1
Size: 381558 Color: 0

Bin 331: 212 of cap free
Amount of items: 2
Items: 
Size: 689424 Color: 0
Size: 310365 Color: 1

Bin 332: 212 of cap free
Amount of items: 2
Items: 
Size: 714668 Color: 1
Size: 285121 Color: 0

Bin 333: 213 of cap free
Amount of items: 2
Items: 
Size: 570142 Color: 1
Size: 429646 Color: 0

Bin 334: 214 of cap free
Amount of items: 3
Items: 
Size: 684516 Color: 1
Size: 186402 Color: 0
Size: 128869 Color: 1

Bin 335: 214 of cap free
Amount of items: 2
Items: 
Size: 690668 Color: 0
Size: 309119 Color: 1

Bin 336: 215 of cap free
Amount of items: 2
Items: 
Size: 785591 Color: 1
Size: 214195 Color: 0

Bin 337: 216 of cap free
Amount of items: 2
Items: 
Size: 792053 Color: 0
Size: 207732 Color: 1

Bin 338: 216 of cap free
Amount of items: 2
Items: 
Size: 606999 Color: 0
Size: 392786 Color: 1

Bin 339: 216 of cap free
Amount of items: 2
Items: 
Size: 664956 Color: 1
Size: 334829 Color: 0

Bin 340: 217 of cap free
Amount of items: 2
Items: 
Size: 522292 Color: 1
Size: 477492 Color: 0

Bin 341: 217 of cap free
Amount of items: 2
Items: 
Size: 553099 Color: 0
Size: 446685 Color: 1

Bin 342: 217 of cap free
Amount of items: 2
Items: 
Size: 575424 Color: 0
Size: 424360 Color: 1

Bin 343: 218 of cap free
Amount of items: 3
Items: 
Size: 789572 Color: 1
Size: 108890 Color: 0
Size: 101321 Color: 1

Bin 344: 219 of cap free
Amount of items: 2
Items: 
Size: 641844 Color: 1
Size: 357938 Color: 0

Bin 345: 222 of cap free
Amount of items: 2
Items: 
Size: 774874 Color: 0
Size: 224905 Color: 1

Bin 346: 222 of cap free
Amount of items: 3
Items: 
Size: 567549 Color: 1
Size: 246785 Color: 0
Size: 185445 Color: 1

Bin 347: 224 of cap free
Amount of items: 2
Items: 
Size: 746195 Color: 1
Size: 253582 Color: 0

Bin 348: 224 of cap free
Amount of items: 2
Items: 
Size: 730320 Color: 1
Size: 269457 Color: 0

Bin 349: 224 of cap free
Amount of items: 2
Items: 
Size: 673236 Color: 0
Size: 326541 Color: 1

Bin 350: 224 of cap free
Amount of items: 2
Items: 
Size: 788034 Color: 1
Size: 211743 Color: 0

Bin 351: 225 of cap free
Amount of items: 2
Items: 
Size: 535260 Color: 0
Size: 464516 Color: 1

Bin 352: 226 of cap free
Amount of items: 2
Items: 
Size: 651513 Color: 1
Size: 348262 Color: 0

Bin 353: 228 of cap free
Amount of items: 3
Items: 
Size: 784265 Color: 1
Size: 109575 Color: 0
Size: 105933 Color: 0

Bin 354: 231 of cap free
Amount of items: 2
Items: 
Size: 691969 Color: 1
Size: 307801 Color: 0

Bin 355: 233 of cap free
Amount of items: 2
Items: 
Size: 557225 Color: 1
Size: 442543 Color: 0

Bin 356: 233 of cap free
Amount of items: 2
Items: 
Size: 694395 Color: 1
Size: 305373 Color: 0

Bin 357: 235 of cap free
Amount of items: 2
Items: 
Size: 786506 Color: 1
Size: 213260 Color: 0

Bin 358: 236 of cap free
Amount of items: 3
Items: 
Size: 529764 Color: 1
Size: 285800 Color: 1
Size: 184201 Color: 0

Bin 359: 236 of cap free
Amount of items: 2
Items: 
Size: 747488 Color: 0
Size: 252277 Color: 1

Bin 360: 237 of cap free
Amount of items: 2
Items: 
Size: 506707 Color: 1
Size: 493057 Color: 0

Bin 361: 237 of cap free
Amount of items: 2
Items: 
Size: 708343 Color: 1
Size: 291421 Color: 0

Bin 362: 239 of cap free
Amount of items: 3
Items: 
Size: 699861 Color: 1
Size: 185730 Color: 0
Size: 114171 Color: 1

Bin 363: 242 of cap free
Amount of items: 2
Items: 
Size: 610404 Color: 1
Size: 389355 Color: 0

Bin 364: 242 of cap free
Amount of items: 2
Items: 
Size: 744196 Color: 0
Size: 255563 Color: 1

Bin 365: 245 of cap free
Amount of items: 2
Items: 
Size: 680939 Color: 0
Size: 318817 Color: 1

Bin 366: 247 of cap free
Amount of items: 2
Items: 
Size: 538508 Color: 0
Size: 461246 Color: 1

Bin 367: 249 of cap free
Amount of items: 2
Items: 
Size: 507117 Color: 1
Size: 492635 Color: 0

Bin 368: 249 of cap free
Amount of items: 2
Items: 
Size: 618395 Color: 0
Size: 381357 Color: 1

Bin 369: 250 of cap free
Amount of items: 3
Items: 
Size: 699216 Color: 1
Size: 160678 Color: 1
Size: 139857 Color: 0

Bin 370: 250 of cap free
Amount of items: 2
Items: 
Size: 676385 Color: 1
Size: 323366 Color: 0

Bin 371: 252 of cap free
Amount of items: 2
Items: 
Size: 692132 Color: 0
Size: 307617 Color: 1

Bin 372: 252 of cap free
Amount of items: 2
Items: 
Size: 700943 Color: 1
Size: 298806 Color: 0

Bin 373: 253 of cap free
Amount of items: 2
Items: 
Size: 510112 Color: 1
Size: 489636 Color: 0

Bin 374: 253 of cap free
Amount of items: 2
Items: 
Size: 747952 Color: 1
Size: 251796 Color: 0

Bin 375: 254 of cap free
Amount of items: 2
Items: 
Size: 799770 Color: 0
Size: 199977 Color: 1

Bin 376: 254 of cap free
Amount of items: 2
Items: 
Size: 754182 Color: 0
Size: 245565 Color: 1

Bin 377: 255 of cap free
Amount of items: 2
Items: 
Size: 722229 Color: 1
Size: 277517 Color: 0

Bin 378: 256 of cap free
Amount of items: 2
Items: 
Size: 619847 Color: 0
Size: 379898 Color: 1

Bin 379: 267 of cap free
Amount of items: 2
Items: 
Size: 525594 Color: 1
Size: 474140 Color: 0

Bin 380: 267 of cap free
Amount of items: 2
Items: 
Size: 795867 Color: 0
Size: 203867 Color: 1

Bin 381: 268 of cap free
Amount of items: 3
Items: 
Size: 776489 Color: 0
Size: 111997 Color: 1
Size: 111247 Color: 0

Bin 382: 268 of cap free
Amount of items: 2
Items: 
Size: 587861 Color: 1
Size: 411872 Color: 0

Bin 383: 268 of cap free
Amount of items: 2
Items: 
Size: 632390 Color: 0
Size: 367343 Color: 1

Bin 384: 271 of cap free
Amount of items: 2
Items: 
Size: 708150 Color: 0
Size: 291580 Color: 1

Bin 385: 272 of cap free
Amount of items: 2
Items: 
Size: 771224 Color: 1
Size: 228505 Color: 0

Bin 386: 273 of cap free
Amount of items: 2
Items: 
Size: 519862 Color: 0
Size: 479866 Color: 1

Bin 387: 278 of cap free
Amount of items: 2
Items: 
Size: 578989 Color: 1
Size: 420734 Color: 0

Bin 388: 280 of cap free
Amount of items: 2
Items: 
Size: 598794 Color: 0
Size: 400927 Color: 1

Bin 389: 282 of cap free
Amount of items: 2
Items: 
Size: 584279 Color: 0
Size: 415440 Color: 1

Bin 390: 282 of cap free
Amount of items: 2
Items: 
Size: 603183 Color: 0
Size: 396536 Color: 1

Bin 391: 284 of cap free
Amount of items: 2
Items: 
Size: 548904 Color: 0
Size: 450813 Color: 1

Bin 392: 285 of cap free
Amount of items: 2
Items: 
Size: 568277 Color: 0
Size: 431439 Color: 1

Bin 393: 287 of cap free
Amount of items: 2
Items: 
Size: 577773 Color: 0
Size: 421941 Color: 1

Bin 394: 288 of cap free
Amount of items: 2
Items: 
Size: 704959 Color: 0
Size: 294754 Color: 1

Bin 395: 288 of cap free
Amount of items: 2
Items: 
Size: 525873 Color: 1
Size: 473840 Color: 0

Bin 396: 288 of cap free
Amount of items: 2
Items: 
Size: 542645 Color: 0
Size: 457068 Color: 1

Bin 397: 290 of cap free
Amount of items: 3
Items: 
Size: 567173 Color: 1
Size: 291909 Color: 1
Size: 140629 Color: 0

Bin 398: 290 of cap free
Amount of items: 2
Items: 
Size: 635960 Color: 1
Size: 363751 Color: 0

Bin 399: 291 of cap free
Amount of items: 3
Items: 
Size: 450280 Color: 1
Size: 377799 Color: 0
Size: 171631 Color: 0

Bin 400: 291 of cap free
Amount of items: 2
Items: 
Size: 567782 Color: 1
Size: 431928 Color: 0

Bin 401: 292 of cap free
Amount of items: 2
Items: 
Size: 667187 Color: 0
Size: 332522 Color: 1

Bin 402: 292 of cap free
Amount of items: 2
Items: 
Size: 790103 Color: 0
Size: 209606 Color: 1

Bin 403: 293 of cap free
Amount of items: 2
Items: 
Size: 581006 Color: 1
Size: 418702 Color: 0

Bin 404: 294 of cap free
Amount of items: 2
Items: 
Size: 554110 Color: 0
Size: 445597 Color: 1

Bin 405: 298 of cap free
Amount of items: 2
Items: 
Size: 541275 Color: 0
Size: 458428 Color: 1

Bin 406: 300 of cap free
Amount of items: 2
Items: 
Size: 501411 Color: 0
Size: 498290 Color: 1

Bin 407: 302 of cap free
Amount of items: 2
Items: 
Size: 678759 Color: 0
Size: 320940 Color: 1

Bin 408: 303 of cap free
Amount of items: 2
Items: 
Size: 543690 Color: 0
Size: 456008 Color: 1

Bin 409: 303 of cap free
Amount of items: 2
Items: 
Size: 533325 Color: 1
Size: 466373 Color: 0

Bin 410: 303 of cap free
Amount of items: 3
Items: 
Size: 553043 Color: 0
Size: 308270 Color: 0
Size: 138385 Color: 1

Bin 411: 304 of cap free
Amount of items: 2
Items: 
Size: 736615 Color: 1
Size: 263082 Color: 0

Bin 412: 308 of cap free
Amount of items: 2
Items: 
Size: 715039 Color: 0
Size: 284654 Color: 1

Bin 413: 316 of cap free
Amount of items: 2
Items: 
Size: 644444 Color: 1
Size: 355241 Color: 0

Bin 414: 316 of cap free
Amount of items: 2
Items: 
Size: 648184 Color: 0
Size: 351501 Color: 1

Bin 415: 317 of cap free
Amount of items: 2
Items: 
Size: 795314 Color: 0
Size: 204370 Color: 1

Bin 416: 319 of cap free
Amount of items: 2
Items: 
Size: 768492 Color: 0
Size: 231190 Color: 1

Bin 417: 320 of cap free
Amount of items: 2
Items: 
Size: 602993 Color: 1
Size: 396688 Color: 0

Bin 418: 326 of cap free
Amount of items: 2
Items: 
Size: 594716 Color: 0
Size: 404959 Color: 1

Bin 419: 327 of cap free
Amount of items: 2
Items: 
Size: 523085 Color: 0
Size: 476589 Color: 1

Bin 420: 330 of cap free
Amount of items: 2
Items: 
Size: 680912 Color: 1
Size: 318759 Color: 0

Bin 421: 330 of cap free
Amount of items: 2
Items: 
Size: 762887 Color: 1
Size: 236784 Color: 0

Bin 422: 332 of cap free
Amount of items: 2
Items: 
Size: 624753 Color: 1
Size: 374916 Color: 0

Bin 423: 333 of cap free
Amount of items: 2
Items: 
Size: 517761 Color: 1
Size: 481907 Color: 0

Bin 424: 335 of cap free
Amount of items: 2
Items: 
Size: 695575 Color: 0
Size: 304091 Color: 1

Bin 425: 335 of cap free
Amount of items: 2
Items: 
Size: 552217 Color: 0
Size: 447449 Color: 1

Bin 426: 336 of cap free
Amount of items: 2
Items: 
Size: 596390 Color: 0
Size: 403275 Color: 1

Bin 427: 338 of cap free
Amount of items: 3
Items: 
Size: 450389 Color: 1
Size: 363985 Color: 1
Size: 185289 Color: 0

Bin 428: 338 of cap free
Amount of items: 2
Items: 
Size: 678229 Color: 1
Size: 321434 Color: 0

Bin 429: 339 of cap free
Amount of items: 2
Items: 
Size: 646699 Color: 1
Size: 352963 Color: 0

Bin 430: 339 of cap free
Amount of items: 2
Items: 
Size: 569034 Color: 1
Size: 430628 Color: 0

Bin 431: 340 of cap free
Amount of items: 2
Items: 
Size: 759597 Color: 0
Size: 240064 Color: 1

Bin 432: 343 of cap free
Amount of items: 2
Items: 
Size: 674780 Color: 0
Size: 324878 Color: 1

Bin 433: 345 of cap free
Amount of items: 2
Items: 
Size: 767811 Color: 1
Size: 231845 Color: 0

Bin 434: 347 of cap free
Amount of items: 3
Items: 
Size: 456762 Color: 1
Size: 377936 Color: 0
Size: 164956 Color: 1

Bin 435: 347 of cap free
Amount of items: 2
Items: 
Size: 720300 Color: 0
Size: 279354 Color: 1

Bin 436: 350 of cap free
Amount of items: 2
Items: 
Size: 587628 Color: 0
Size: 412023 Color: 1

Bin 437: 353 of cap free
Amount of items: 3
Items: 
Size: 689049 Color: 1
Size: 174995 Color: 0
Size: 135604 Color: 0

Bin 438: 353 of cap free
Amount of items: 2
Items: 
Size: 568152 Color: 1
Size: 431496 Color: 0

Bin 439: 353 of cap free
Amount of items: 2
Items: 
Size: 714312 Color: 1
Size: 285336 Color: 0

Bin 440: 355 of cap free
Amount of items: 2
Items: 
Size: 640959 Color: 1
Size: 358687 Color: 0

Bin 441: 357 of cap free
Amount of items: 2
Items: 
Size: 537769 Color: 0
Size: 461875 Color: 1

Bin 442: 360 of cap free
Amount of items: 3
Items: 
Size: 766592 Color: 1
Size: 126112 Color: 0
Size: 106937 Color: 0

Bin 443: 362 of cap free
Amount of items: 2
Items: 
Size: 658552 Color: 1
Size: 341087 Color: 0

Bin 444: 364 of cap free
Amount of items: 2
Items: 
Size: 725649 Color: 1
Size: 273988 Color: 0

Bin 445: 367 of cap free
Amount of items: 2
Items: 
Size: 540353 Color: 1
Size: 459281 Color: 0

Bin 446: 367 of cap free
Amount of items: 2
Items: 
Size: 606510 Color: 0
Size: 393124 Color: 1

Bin 447: 374 of cap free
Amount of items: 2
Items: 
Size: 655961 Color: 1
Size: 343666 Color: 0

Bin 448: 374 of cap free
Amount of items: 2
Items: 
Size: 599107 Color: 0
Size: 400520 Color: 1

Bin 449: 381 of cap free
Amount of items: 2
Items: 
Size: 767459 Color: 0
Size: 232161 Color: 1

Bin 450: 382 of cap free
Amount of items: 2
Items: 
Size: 601167 Color: 1
Size: 398452 Color: 0

Bin 451: 384 of cap free
Amount of items: 3
Items: 
Size: 531854 Color: 1
Size: 361245 Color: 0
Size: 106518 Color: 1

Bin 452: 387 of cap free
Amount of items: 2
Items: 
Size: 731617 Color: 1
Size: 267997 Color: 0

Bin 453: 389 of cap free
Amount of items: 2
Items: 
Size: 553696 Color: 0
Size: 445916 Color: 1

Bin 454: 389 of cap free
Amount of items: 2
Items: 
Size: 523764 Color: 0
Size: 475848 Color: 1

Bin 455: 391 of cap free
Amount of items: 3
Items: 
Size: 702717 Color: 0
Size: 188806 Color: 0
Size: 108087 Color: 1

Bin 456: 391 of cap free
Amount of items: 2
Items: 
Size: 518884 Color: 0
Size: 480726 Color: 1

Bin 457: 391 of cap free
Amount of items: 2
Items: 
Size: 571131 Color: 1
Size: 428479 Color: 0

Bin 458: 392 of cap free
Amount of items: 2
Items: 
Size: 623694 Color: 1
Size: 375915 Color: 0

Bin 459: 392 of cap free
Amount of items: 2
Items: 
Size: 541742 Color: 0
Size: 457867 Color: 1

Bin 460: 393 of cap free
Amount of items: 2
Items: 
Size: 638918 Color: 0
Size: 360690 Color: 1

Bin 461: 394 of cap free
Amount of items: 2
Items: 
Size: 544667 Color: 1
Size: 454940 Color: 0

Bin 462: 396 of cap free
Amount of items: 2
Items: 
Size: 645343 Color: 0
Size: 354262 Color: 1

Bin 463: 398 of cap free
Amount of items: 2
Items: 
Size: 592329 Color: 1
Size: 407274 Color: 0

Bin 464: 400 of cap free
Amount of items: 3
Items: 
Size: 574575 Color: 1
Size: 291939 Color: 0
Size: 133087 Color: 1

Bin 465: 401 of cap free
Amount of items: 2
Items: 
Size: 733689 Color: 1
Size: 265911 Color: 0

Bin 466: 403 of cap free
Amount of items: 2
Items: 
Size: 649216 Color: 1
Size: 350382 Color: 0

Bin 467: 407 of cap free
Amount of items: 2
Items: 
Size: 505521 Color: 0
Size: 494073 Color: 1

Bin 468: 414 of cap free
Amount of items: 2
Items: 
Size: 623225 Color: 1
Size: 376362 Color: 0

Bin 469: 415 of cap free
Amount of items: 2
Items: 
Size: 758420 Color: 0
Size: 241166 Color: 1

Bin 470: 415 of cap free
Amount of items: 2
Items: 
Size: 615708 Color: 1
Size: 383878 Color: 0

Bin 471: 418 of cap free
Amount of items: 2
Items: 
Size: 729586 Color: 0
Size: 269997 Color: 1

Bin 472: 418 of cap free
Amount of items: 2
Items: 
Size: 563456 Color: 1
Size: 436127 Color: 0

Bin 473: 418 of cap free
Amount of items: 2
Items: 
Size: 668444 Color: 1
Size: 331139 Color: 0

Bin 474: 424 of cap free
Amount of items: 2
Items: 
Size: 631498 Color: 0
Size: 368079 Color: 1

Bin 475: 425 of cap free
Amount of items: 2
Items: 
Size: 664283 Color: 1
Size: 335293 Color: 0

Bin 476: 425 of cap free
Amount of items: 2
Items: 
Size: 510786 Color: 0
Size: 488790 Color: 1

Bin 477: 426 of cap free
Amount of items: 2
Items: 
Size: 512665 Color: 1
Size: 486910 Color: 0

Bin 478: 430 of cap free
Amount of items: 2
Items: 
Size: 608202 Color: 1
Size: 391369 Color: 0

Bin 479: 432 of cap free
Amount of items: 2
Items: 
Size: 617240 Color: 1
Size: 382329 Color: 0

Bin 480: 434 of cap free
Amount of items: 2
Items: 
Size: 693311 Color: 0
Size: 306256 Color: 1

Bin 481: 435 of cap free
Amount of items: 3
Items: 
Size: 575857 Color: 1
Size: 246089 Color: 1
Size: 177620 Color: 0

Bin 482: 436 of cap free
Amount of items: 2
Items: 
Size: 542644 Color: 0
Size: 456921 Color: 1

Bin 483: 439 of cap free
Amount of items: 2
Items: 
Size: 721109 Color: 0
Size: 278453 Color: 1

Bin 484: 443 of cap free
Amount of items: 2
Items: 
Size: 641847 Color: 0
Size: 357711 Color: 1

Bin 485: 445 of cap free
Amount of items: 2
Items: 
Size: 544075 Color: 0
Size: 455481 Color: 1

Bin 486: 446 of cap free
Amount of items: 3
Items: 
Size: 727727 Color: 1
Size: 141998 Color: 0
Size: 129830 Color: 0

Bin 487: 446 of cap free
Amount of items: 3
Items: 
Size: 542894 Color: 1
Size: 263671 Color: 0
Size: 192990 Color: 1

Bin 488: 446 of cap free
Amount of items: 2
Items: 
Size: 752958 Color: 0
Size: 246597 Color: 1

Bin 489: 447 of cap free
Amount of items: 2
Items: 
Size: 780832 Color: 1
Size: 218722 Color: 0

Bin 490: 461 of cap free
Amount of items: 2
Items: 
Size: 731360 Color: 0
Size: 268180 Color: 1

Bin 491: 461 of cap free
Amount of items: 2
Items: 
Size: 504871 Color: 1
Size: 494669 Color: 0

Bin 492: 469 of cap free
Amount of items: 2
Items: 
Size: 619604 Color: 1
Size: 379928 Color: 0

Bin 493: 472 of cap free
Amount of items: 2
Items: 
Size: 723189 Color: 1
Size: 276340 Color: 0

Bin 494: 472 of cap free
Amount of items: 2
Items: 
Size: 602383 Color: 1
Size: 397146 Color: 0

Bin 495: 472 of cap free
Amount of items: 2
Items: 
Size: 687943 Color: 1
Size: 311586 Color: 0

Bin 496: 474 of cap free
Amount of items: 2
Items: 
Size: 550373 Color: 0
Size: 449154 Color: 1

Bin 497: 481 of cap free
Amount of items: 2
Items: 
Size: 656020 Color: 0
Size: 343500 Color: 1

Bin 498: 484 of cap free
Amount of items: 2
Items: 
Size: 558325 Color: 0
Size: 441192 Color: 1

Bin 499: 487 of cap free
Amount of items: 2
Items: 
Size: 570193 Color: 0
Size: 429321 Color: 1

Bin 500: 488 of cap free
Amount of items: 2
Items: 
Size: 758959 Color: 1
Size: 240554 Color: 0

Bin 501: 491 of cap free
Amount of items: 2
Items: 
Size: 557586 Color: 1
Size: 441924 Color: 0

Bin 502: 495 of cap free
Amount of items: 2
Items: 
Size: 770038 Color: 1
Size: 229468 Color: 0

Bin 503: 499 of cap free
Amount of items: 2
Items: 
Size: 621348 Color: 1
Size: 378154 Color: 0

Bin 504: 499 of cap free
Amount of items: 2
Items: 
Size: 711686 Color: 1
Size: 287816 Color: 0

Bin 505: 505 of cap free
Amount of items: 2
Items: 
Size: 686834 Color: 1
Size: 312662 Color: 0

Bin 506: 505 of cap free
Amount of items: 2
Items: 
Size: 700757 Color: 0
Size: 298739 Color: 1

Bin 507: 507 of cap free
Amount of items: 2
Items: 
Size: 591601 Color: 0
Size: 407893 Color: 1

Bin 508: 507 of cap free
Amount of items: 2
Items: 
Size: 601616 Color: 1
Size: 397878 Color: 0

Bin 509: 515 of cap free
Amount of items: 2
Items: 
Size: 786938 Color: 0
Size: 212548 Color: 1

Bin 510: 517 of cap free
Amount of items: 3
Items: 
Size: 643199 Color: 1
Size: 195658 Color: 0
Size: 160627 Color: 0

Bin 511: 519 of cap free
Amount of items: 2
Items: 
Size: 700761 Color: 1
Size: 298721 Color: 0

Bin 512: 530 of cap free
Amount of items: 2
Items: 
Size: 539629 Color: 0
Size: 459842 Color: 1

Bin 513: 542 of cap free
Amount of items: 2
Items: 
Size: 675673 Color: 0
Size: 323786 Color: 1

Bin 514: 548 of cap free
Amount of items: 2
Items: 
Size: 542557 Color: 0
Size: 456896 Color: 1

Bin 515: 548 of cap free
Amount of items: 2
Items: 
Size: 537458 Color: 1
Size: 461995 Color: 0

Bin 516: 551 of cap free
Amount of items: 3
Items: 
Size: 490433 Color: 1
Size: 344337 Color: 1
Size: 164680 Color: 0

Bin 517: 555 of cap free
Amount of items: 2
Items: 
Size: 786830 Color: 1
Size: 212616 Color: 0

Bin 518: 565 of cap free
Amount of items: 2
Items: 
Size: 562740 Color: 1
Size: 436696 Color: 0

Bin 519: 575 of cap free
Amount of items: 2
Items: 
Size: 507919 Color: 0
Size: 491507 Color: 1

Bin 520: 575 of cap free
Amount of items: 2
Items: 
Size: 559086 Color: 1
Size: 440340 Color: 0

Bin 521: 577 of cap free
Amount of items: 3
Items: 
Size: 764473 Color: 0
Size: 127025 Color: 1
Size: 107926 Color: 1

Bin 522: 580 of cap free
Amount of items: 2
Items: 
Size: 734772 Color: 1
Size: 264649 Color: 0

Bin 523: 582 of cap free
Amount of items: 2
Items: 
Size: 565984 Color: 1
Size: 433435 Color: 0

Bin 524: 583 of cap free
Amount of items: 2
Items: 
Size: 544583 Color: 0
Size: 454835 Color: 1

Bin 525: 583 of cap free
Amount of items: 2
Items: 
Size: 761773 Color: 0
Size: 237645 Color: 1

Bin 526: 585 of cap free
Amount of items: 2
Items: 
Size: 704705 Color: 1
Size: 294711 Color: 0

Bin 527: 587 of cap free
Amount of items: 2
Items: 
Size: 682627 Color: 1
Size: 316787 Color: 0

Bin 528: 590 of cap free
Amount of items: 2
Items: 
Size: 706708 Color: 0
Size: 292703 Color: 1

Bin 529: 591 of cap free
Amount of items: 2
Items: 
Size: 505393 Color: 0
Size: 494017 Color: 1

Bin 530: 592 of cap free
Amount of items: 2
Items: 
Size: 683822 Color: 1
Size: 315587 Color: 0

Bin 531: 593 of cap free
Amount of items: 2
Items: 
Size: 645278 Color: 0
Size: 354130 Color: 1

Bin 532: 593 of cap free
Amount of items: 2
Items: 
Size: 636734 Color: 0
Size: 362674 Color: 1

Bin 533: 596 of cap free
Amount of items: 2
Items: 
Size: 733392 Color: 0
Size: 266013 Color: 1

Bin 534: 599 of cap free
Amount of items: 2
Items: 
Size: 775835 Color: 0
Size: 223567 Color: 1

Bin 535: 604 of cap free
Amount of items: 3
Items: 
Size: 571923 Color: 0
Size: 242924 Color: 0
Size: 184550 Color: 1

Bin 536: 608 of cap free
Amount of items: 3
Items: 
Size: 789445 Color: 1
Size: 107088 Color: 0
Size: 102860 Color: 0

Bin 537: 608 of cap free
Amount of items: 2
Items: 
Size: 505776 Color: 1
Size: 493617 Color: 0

Bin 538: 611 of cap free
Amount of items: 2
Items: 
Size: 508743 Color: 0
Size: 490647 Color: 1

Bin 539: 614 of cap free
Amount of items: 2
Items: 
Size: 762167 Color: 1
Size: 237220 Color: 0

Bin 540: 617 of cap free
Amount of items: 2
Items: 
Size: 548600 Color: 1
Size: 450784 Color: 0

Bin 541: 618 of cap free
Amount of items: 2
Items: 
Size: 763616 Color: 0
Size: 235767 Color: 1

Bin 542: 619 of cap free
Amount of items: 2
Items: 
Size: 661150 Color: 1
Size: 338232 Color: 0

Bin 543: 620 of cap free
Amount of items: 2
Items: 
Size: 503230 Color: 0
Size: 496151 Color: 1

Bin 544: 621 of cap free
Amount of items: 2
Items: 
Size: 578900 Color: 1
Size: 420480 Color: 0

Bin 545: 621 of cap free
Amount of items: 2
Items: 
Size: 654026 Color: 1
Size: 345354 Color: 0

Bin 546: 629 of cap free
Amount of items: 2
Items: 
Size: 756078 Color: 1
Size: 243294 Color: 0

Bin 547: 630 of cap free
Amount of items: 2
Items: 
Size: 521098 Color: 0
Size: 478273 Color: 1

Bin 548: 631 of cap free
Amount of items: 2
Items: 
Size: 664735 Color: 0
Size: 334635 Color: 1

Bin 549: 632 of cap free
Amount of items: 2
Items: 
Size: 613011 Color: 1
Size: 386358 Color: 0

Bin 550: 635 of cap free
Amount of items: 2
Items: 
Size: 765082 Color: 1
Size: 234284 Color: 0

Bin 551: 635 of cap free
Amount of items: 2
Items: 
Size: 713056 Color: 0
Size: 286310 Color: 1

Bin 552: 635 of cap free
Amount of items: 2
Items: 
Size: 550063 Color: 1
Size: 449303 Color: 0

Bin 553: 647 of cap free
Amount of items: 3
Items: 
Size: 785486 Color: 1
Size: 113634 Color: 0
Size: 100234 Color: 0

Bin 554: 648 of cap free
Amount of items: 2
Items: 
Size: 634802 Color: 1
Size: 364551 Color: 0

Bin 555: 649 of cap free
Amount of items: 2
Items: 
Size: 755816 Color: 0
Size: 243536 Color: 1

Bin 556: 649 of cap free
Amount of items: 2
Items: 
Size: 628171 Color: 1
Size: 371181 Color: 0

Bin 557: 654 of cap free
Amount of items: 2
Items: 
Size: 646767 Color: 0
Size: 352580 Color: 1

Bin 558: 655 of cap free
Amount of items: 2
Items: 
Size: 764393 Color: 1
Size: 234953 Color: 0

Bin 559: 657 of cap free
Amount of items: 2
Items: 
Size: 509295 Color: 1
Size: 490049 Color: 0

Bin 560: 674 of cap free
Amount of items: 2
Items: 
Size: 666344 Color: 0
Size: 332983 Color: 1

Bin 561: 674 of cap free
Amount of items: 2
Items: 
Size: 536821 Color: 0
Size: 462506 Color: 1

Bin 562: 684 of cap free
Amount of items: 2
Items: 
Size: 590527 Color: 1
Size: 408790 Color: 0

Bin 563: 689 of cap free
Amount of items: 2
Items: 
Size: 774010 Color: 1
Size: 225302 Color: 0

Bin 564: 690 of cap free
Amount of items: 2
Items: 
Size: 755192 Color: 1
Size: 244119 Color: 0

Bin 565: 694 of cap free
Amount of items: 2
Items: 
Size: 582941 Color: 0
Size: 416366 Color: 1

Bin 566: 694 of cap free
Amount of items: 2
Items: 
Size: 555994 Color: 1
Size: 443313 Color: 0

Bin 567: 697 of cap free
Amount of items: 2
Items: 
Size: 726924 Color: 1
Size: 272380 Color: 0

Bin 568: 702 of cap free
Amount of items: 2
Items: 
Size: 607766 Color: 0
Size: 391533 Color: 1

Bin 569: 708 of cap free
Amount of items: 2
Items: 
Size: 648103 Color: 0
Size: 351190 Color: 1

Bin 570: 714 of cap free
Amount of items: 2
Items: 
Size: 734909 Color: 0
Size: 264378 Color: 1

Bin 571: 722 of cap free
Amount of items: 2
Items: 
Size: 741724 Color: 0
Size: 257555 Color: 1

Bin 572: 723 of cap free
Amount of items: 2
Items: 
Size: 752082 Color: 1
Size: 247196 Color: 0

Bin 573: 730 of cap free
Amount of items: 2
Items: 
Size: 791366 Color: 1
Size: 207905 Color: 0

Bin 574: 735 of cap free
Amount of items: 2
Items: 
Size: 772329 Color: 0
Size: 226937 Color: 1

Bin 575: 742 of cap free
Amount of items: 2
Items: 
Size: 782715 Color: 0
Size: 216544 Color: 1

Bin 576: 744 of cap free
Amount of items: 2
Items: 
Size: 640152 Color: 1
Size: 359105 Color: 0

Bin 577: 756 of cap free
Amount of items: 2
Items: 
Size: 499720 Color: 1
Size: 499525 Color: 0

Bin 578: 761 of cap free
Amount of items: 2
Items: 
Size: 703576 Color: 1
Size: 295664 Color: 0

Bin 579: 766 of cap free
Amount of items: 2
Items: 
Size: 614290 Color: 1
Size: 384945 Color: 0

Bin 580: 768 of cap free
Amount of items: 2
Items: 
Size: 734369 Color: 1
Size: 264864 Color: 0

Bin 581: 770 of cap free
Amount of items: 2
Items: 
Size: 740551 Color: 1
Size: 258680 Color: 0

Bin 582: 773 of cap free
Amount of items: 2
Items: 
Size: 700501 Color: 0
Size: 298727 Color: 1

Bin 583: 776 of cap free
Amount of items: 2
Items: 
Size: 559677 Color: 1
Size: 439548 Color: 0

Bin 584: 786 of cap free
Amount of items: 2
Items: 
Size: 565180 Color: 0
Size: 434035 Color: 1

Bin 585: 795 of cap free
Amount of items: 2
Items: 
Size: 604254 Color: 0
Size: 394952 Color: 1

Bin 586: 798 of cap free
Amount of items: 2
Items: 
Size: 781552 Color: 1
Size: 217651 Color: 0

Bin 587: 798 of cap free
Amount of items: 2
Items: 
Size: 611088 Color: 1
Size: 388115 Color: 0

Bin 588: 804 of cap free
Amount of items: 2
Items: 
Size: 716250 Color: 0
Size: 282947 Color: 1

Bin 589: 805 of cap free
Amount of items: 2
Items: 
Size: 609717 Color: 0
Size: 389479 Color: 1

Bin 590: 809 of cap free
Amount of items: 2
Items: 
Size: 517353 Color: 1
Size: 481839 Color: 0

Bin 591: 810 of cap free
Amount of items: 3
Items: 
Size: 685122 Color: 0
Size: 166434 Color: 0
Size: 147635 Color: 1

Bin 592: 832 of cap free
Amount of items: 2
Items: 
Size: 541220 Color: 1
Size: 457949 Color: 0

Bin 593: 837 of cap free
Amount of items: 2
Items: 
Size: 536792 Color: 0
Size: 462372 Color: 1

Bin 594: 837 of cap free
Amount of items: 2
Items: 
Size: 596801 Color: 0
Size: 402363 Color: 1

Bin 595: 852 of cap free
Amount of items: 2
Items: 
Size: 762014 Color: 1
Size: 237135 Color: 0

Bin 596: 873 of cap free
Amount of items: 3
Items: 
Size: 547575 Color: 1
Size: 295077 Color: 1
Size: 156476 Color: 0

Bin 597: 878 of cap free
Amount of items: 2
Items: 
Size: 783185 Color: 1
Size: 215938 Color: 0

Bin 598: 879 of cap free
Amount of items: 2
Items: 
Size: 623208 Color: 0
Size: 375914 Color: 1

Bin 599: 893 of cap free
Amount of items: 2
Items: 
Size: 698750 Color: 0
Size: 300358 Color: 1

Bin 600: 893 of cap free
Amount of items: 2
Items: 
Size: 535691 Color: 1
Size: 463417 Color: 0

Bin 601: 914 of cap free
Amount of items: 2
Items: 
Size: 516116 Color: 1
Size: 482971 Color: 0

Bin 602: 927 of cap free
Amount of items: 2
Items: 
Size: 796772 Color: 1
Size: 202302 Color: 0

Bin 603: 929 of cap free
Amount of items: 2
Items: 
Size: 611770 Color: 0
Size: 387302 Color: 1

Bin 604: 938 of cap free
Amount of items: 2
Items: 
Size: 722997 Color: 0
Size: 276066 Color: 1

Bin 605: 940 of cap free
Amount of items: 2
Items: 
Size: 650977 Color: 1
Size: 348084 Color: 0

Bin 606: 941 of cap free
Amount of items: 2
Items: 
Size: 500787 Color: 0
Size: 498273 Color: 1

Bin 607: 943 of cap free
Amount of items: 2
Items: 
Size: 528566 Color: 0
Size: 470492 Color: 1

Bin 608: 945 of cap free
Amount of items: 2
Items: 
Size: 768405 Color: 0
Size: 230651 Color: 1

Bin 609: 949 of cap free
Amount of items: 2
Items: 
Size: 798249 Color: 0
Size: 200803 Color: 1

Bin 610: 950 of cap free
Amount of items: 2
Items: 
Size: 596789 Color: 0
Size: 402262 Color: 1

Bin 611: 950 of cap free
Amount of items: 2
Items: 
Size: 658042 Color: 0
Size: 341009 Color: 1

Bin 612: 953 of cap free
Amount of items: 2
Items: 
Size: 647976 Color: 0
Size: 351072 Color: 1

Bin 613: 953 of cap free
Amount of items: 2
Items: 
Size: 650683 Color: 0
Size: 348365 Color: 1

Bin 614: 954 of cap free
Amount of items: 2
Items: 
Size: 695536 Color: 1
Size: 303511 Color: 0

Bin 615: 955 of cap free
Amount of items: 2
Items: 
Size: 578430 Color: 0
Size: 420616 Color: 1

Bin 616: 956 of cap free
Amount of items: 2
Items: 
Size: 748701 Color: 1
Size: 250344 Color: 0

Bin 617: 971 of cap free
Amount of items: 2
Items: 
Size: 767398 Color: 1
Size: 231632 Color: 0

Bin 618: 973 of cap free
Amount of items: 2
Items: 
Size: 580961 Color: 1
Size: 418067 Color: 0

Bin 619: 975 of cap free
Amount of items: 2
Items: 
Size: 600720 Color: 0
Size: 398306 Color: 1

Bin 620: 977 of cap free
Amount of items: 2
Items: 
Size: 585438 Color: 0
Size: 413586 Color: 1

Bin 621: 985 of cap free
Amount of items: 2
Items: 
Size: 617165 Color: 1
Size: 381851 Color: 0

Bin 622: 986 of cap free
Amount of items: 2
Items: 
Size: 660811 Color: 0
Size: 338204 Color: 1

Bin 623: 991 of cap free
Amount of items: 2
Items: 
Size: 568009 Color: 0
Size: 431001 Color: 1

Bin 624: 1011 of cap free
Amount of items: 2
Items: 
Size: 714817 Color: 0
Size: 284173 Color: 1

Bin 625: 1017 of cap free
Amount of items: 2
Items: 
Size: 783116 Color: 1
Size: 215868 Color: 0

Bin 626: 1026 of cap free
Amount of items: 2
Items: 
Size: 669984 Color: 0
Size: 328991 Color: 1

Bin 627: 1028 of cap free
Amount of items: 2
Items: 
Size: 684743 Color: 1
Size: 314230 Color: 0

Bin 628: 1032 of cap free
Amount of items: 2
Items: 
Size: 681483 Color: 0
Size: 317486 Color: 1

Bin 629: 1035 of cap free
Amount of items: 2
Items: 
Size: 623117 Color: 1
Size: 375849 Color: 0

Bin 630: 1038 of cap free
Amount of items: 2
Items: 
Size: 695144 Color: 0
Size: 303819 Color: 1

Bin 631: 1043 of cap free
Amount of items: 2
Items: 
Size: 539740 Color: 1
Size: 459218 Color: 0

Bin 632: 1055 of cap free
Amount of items: 2
Items: 
Size: 556521 Color: 0
Size: 442425 Color: 1

Bin 633: 1066 of cap free
Amount of items: 2
Items: 
Size: 657978 Color: 1
Size: 340957 Color: 0

Bin 634: 1069 of cap free
Amount of items: 2
Items: 
Size: 720835 Color: 1
Size: 278097 Color: 0

Bin 635: 1077 of cap free
Amount of items: 2
Items: 
Size: 531845 Color: 1
Size: 467079 Color: 0

Bin 636: 1081 of cap free
Amount of items: 2
Items: 
Size: 544281 Color: 1
Size: 454639 Color: 0

Bin 637: 1083 of cap free
Amount of items: 2
Items: 
Size: 671156 Color: 1
Size: 327762 Color: 0

Bin 638: 1092 of cap free
Amount of items: 2
Items: 
Size: 682769 Color: 0
Size: 316140 Color: 1

Bin 639: 1092 of cap free
Amount of items: 2
Items: 
Size: 749900 Color: 0
Size: 249009 Color: 1

Bin 640: 1093 of cap free
Amount of items: 2
Items: 
Size: 716033 Color: 0
Size: 282875 Color: 1

Bin 641: 1097 of cap free
Amount of items: 2
Items: 
Size: 517340 Color: 0
Size: 481564 Color: 1

Bin 642: 1098 of cap free
Amount of items: 2
Items: 
Size: 518574 Color: 0
Size: 480329 Color: 1

Bin 643: 1110 of cap free
Amount of items: 2
Items: 
Size: 756962 Color: 0
Size: 241929 Color: 1

Bin 644: 1114 of cap free
Amount of items: 2
Items: 
Size: 779424 Color: 1
Size: 219463 Color: 0

Bin 645: 1129 of cap free
Amount of items: 2
Items: 
Size: 789385 Color: 0
Size: 209487 Color: 1

Bin 646: 1132 of cap free
Amount of items: 2
Items: 
Size: 570061 Color: 0
Size: 428808 Color: 1

Bin 647: 1149 of cap free
Amount of items: 2
Items: 
Size: 668043 Color: 0
Size: 330809 Color: 1

Bin 648: 1151 of cap free
Amount of items: 2
Items: 
Size: 730976 Color: 0
Size: 267874 Color: 1

Bin 649: 1160 of cap free
Amount of items: 2
Items: 
Size: 745939 Color: 0
Size: 252902 Color: 1

Bin 650: 1162 of cap free
Amount of items: 2
Items: 
Size: 632117 Color: 0
Size: 366722 Color: 1

Bin 651: 1174 of cap free
Amount of items: 2
Items: 
Size: 559982 Color: 0
Size: 438845 Color: 1

Bin 652: 1205 of cap free
Amount of items: 2
Items: 
Size: 744076 Color: 1
Size: 254720 Color: 0

Bin 653: 1217 of cap free
Amount of items: 2
Items: 
Size: 523179 Color: 1
Size: 475605 Color: 0

Bin 654: 1219 of cap free
Amount of items: 2
Items: 
Size: 716030 Color: 0
Size: 282752 Color: 1

Bin 655: 1231 of cap free
Amount of items: 2
Items: 
Size: 690332 Color: 0
Size: 308438 Color: 1

Bin 656: 1247 of cap free
Amount of items: 2
Items: 
Size: 665763 Color: 1
Size: 332991 Color: 0

Bin 657: 1255 of cap free
Amount of items: 3
Items: 
Size: 453635 Color: 1
Size: 381315 Color: 0
Size: 163796 Color: 0

Bin 658: 1256 of cap free
Amount of items: 2
Items: 
Size: 614076 Color: 1
Size: 384669 Color: 0

Bin 659: 1265 of cap free
Amount of items: 3
Items: 
Size: 554674 Color: 0
Size: 331170 Color: 1
Size: 112892 Color: 0

Bin 660: 1268 of cap free
Amount of items: 2
Items: 
Size: 602238 Color: 1
Size: 396495 Color: 0

Bin 661: 1269 of cap free
Amount of items: 2
Items: 
Size: 620956 Color: 0
Size: 377776 Color: 1

Bin 662: 1295 of cap free
Amount of items: 2
Items: 
Size: 689426 Color: 1
Size: 309280 Color: 0

Bin 663: 1320 of cap free
Amount of items: 2
Items: 
Size: 734871 Color: 0
Size: 263810 Color: 1

Bin 664: 1321 of cap free
Amount of items: 2
Items: 
Size: 676889 Color: 1
Size: 321791 Color: 0

Bin 665: 1323 of cap free
Amount of items: 2
Items: 
Size: 525040 Color: 1
Size: 473638 Color: 0

Bin 666: 1328 of cap free
Amount of items: 2
Items: 
Size: 652394 Color: 0
Size: 346279 Color: 1

Bin 667: 1336 of cap free
Amount of items: 2
Items: 
Size: 729387 Color: 0
Size: 269278 Color: 1

Bin 668: 1337 of cap free
Amount of items: 3
Items: 
Size: 664252 Color: 0
Size: 193142 Color: 1
Size: 141270 Color: 0

Bin 669: 1355 of cap free
Amount of items: 2
Items: 
Size: 643044 Color: 0
Size: 355602 Color: 1

Bin 670: 1380 of cap free
Amount of items: 2
Items: 
Size: 746119 Color: 1
Size: 252502 Color: 0

Bin 671: 1381 of cap free
Amount of items: 2
Items: 
Size: 581099 Color: 0
Size: 417521 Color: 1

Bin 672: 1389 of cap free
Amount of items: 2
Items: 
Size: 759560 Color: 0
Size: 239052 Color: 1

Bin 673: 1393 of cap free
Amount of items: 2
Items: 
Size: 705703 Color: 1
Size: 292905 Color: 0

Bin 674: 1404 of cap free
Amount of items: 2
Items: 
Size: 632917 Color: 1
Size: 365680 Color: 0

Bin 675: 1405 of cap free
Amount of items: 2
Items: 
Size: 702767 Color: 0
Size: 295829 Color: 1

Bin 676: 1430 of cap free
Amount of items: 2
Items: 
Size: 616788 Color: 1
Size: 381783 Color: 0

Bin 677: 1455 of cap free
Amount of items: 2
Items: 
Size: 713986 Color: 1
Size: 284560 Color: 0

Bin 678: 1455 of cap free
Amount of items: 2
Items: 
Size: 709848 Color: 0
Size: 288698 Color: 1

Bin 679: 1477 of cap free
Amount of items: 3
Items: 
Size: 681942 Color: 0
Size: 160834 Color: 1
Size: 155748 Color: 1

Bin 680: 1477 of cap free
Amount of items: 2
Items: 
Size: 584183 Color: 1
Size: 414341 Color: 0

Bin 681: 1488 of cap free
Amount of items: 2
Items: 
Size: 520254 Color: 1
Size: 478259 Color: 0

Bin 682: 1495 of cap free
Amount of items: 2
Items: 
Size: 645711 Color: 1
Size: 352795 Color: 0

Bin 683: 1506 of cap free
Amount of items: 2
Items: 
Size: 629721 Color: 1
Size: 368774 Color: 0

Bin 684: 1508 of cap free
Amount of items: 2
Items: 
Size: 744059 Color: 0
Size: 254434 Color: 1

Bin 685: 1510 of cap free
Amount of items: 2
Items: 
Size: 546379 Color: 0
Size: 452112 Color: 1

Bin 686: 1512 of cap free
Amount of items: 2
Items: 
Size: 532448 Color: 0
Size: 466041 Color: 1

Bin 687: 1528 of cap free
Amount of items: 2
Items: 
Size: 586603 Color: 1
Size: 411870 Color: 0

Bin 688: 1529 of cap free
Amount of items: 2
Items: 
Size: 549495 Color: 1
Size: 448977 Color: 0

Bin 689: 1562 of cap free
Amount of items: 2
Items: 
Size: 508468 Color: 1
Size: 489971 Color: 0

Bin 690: 1566 of cap free
Amount of items: 2
Items: 
Size: 792682 Color: 1
Size: 205753 Color: 0

Bin 691: 1571 of cap free
Amount of items: 2
Items: 
Size: 575214 Color: 0
Size: 423216 Color: 1

Bin 692: 1583 of cap free
Amount of items: 3
Items: 
Size: 737839 Color: 0
Size: 147106 Color: 1
Size: 113473 Color: 0

Bin 693: 1608 of cap free
Amount of items: 2
Items: 
Size: 570019 Color: 0
Size: 428374 Color: 1

Bin 694: 1613 of cap free
Amount of items: 2
Items: 
Size: 503799 Color: 1
Size: 494589 Color: 0

Bin 695: 1623 of cap free
Amount of items: 2
Items: 
Size: 655336 Color: 1
Size: 343042 Color: 0

Bin 696: 1629 of cap free
Amount of items: 2
Items: 
Size: 610895 Color: 1
Size: 387477 Color: 0

Bin 697: 1632 of cap free
Amount of items: 2
Items: 
Size: 715804 Color: 1
Size: 282565 Color: 0

Bin 698: 1648 of cap free
Amount of items: 2
Items: 
Size: 691646 Color: 0
Size: 306707 Color: 1

Bin 699: 1673 of cap free
Amount of items: 2
Items: 
Size: 677457 Color: 0
Size: 320871 Color: 1

Bin 700: 1675 of cap free
Amount of items: 2
Items: 
Size: 789066 Color: 0
Size: 209260 Color: 1

Bin 701: 1687 of cap free
Amount of items: 2
Items: 
Size: 686822 Color: 1
Size: 311492 Color: 0

Bin 702: 1691 of cap free
Amount of items: 2
Items: 
Size: 515548 Color: 1
Size: 482762 Color: 0

Bin 703: 1705 of cap free
Amount of items: 2
Items: 
Size: 711092 Color: 1
Size: 287204 Color: 0

Bin 704: 1717 of cap free
Amount of items: 2
Items: 
Size: 715965 Color: 0
Size: 282319 Color: 1

Bin 705: 1733 of cap free
Amount of items: 2
Items: 
Size: 525809 Color: 0
Size: 472459 Color: 1

Bin 706: 1735 of cap free
Amount of items: 2
Items: 
Size: 619469 Color: 1
Size: 378797 Color: 0

Bin 707: 1741 of cap free
Amount of items: 2
Items: 
Size: 569988 Color: 0
Size: 428272 Color: 1

Bin 708: 1741 of cap free
Amount of items: 2
Items: 
Size: 775425 Color: 1
Size: 222835 Color: 0

Bin 709: 1771 of cap free
Amount of items: 2
Items: 
Size: 772943 Color: 1
Size: 225287 Color: 0

Bin 710: 1793 of cap free
Amount of items: 2
Items: 
Size: 529980 Color: 1
Size: 468228 Color: 0

Bin 711: 1820 of cap free
Amount of items: 2
Items: 
Size: 773377 Color: 0
Size: 224804 Color: 1

Bin 712: 1821 of cap free
Amount of items: 2
Items: 
Size: 590843 Color: 0
Size: 407337 Color: 1

Bin 713: 1831 of cap free
Amount of items: 2
Items: 
Size: 532334 Color: 0
Size: 465836 Color: 1

Bin 714: 1835 of cap free
Amount of items: 2
Items: 
Size: 513225 Color: 0
Size: 484941 Color: 1

Bin 715: 1839 of cap free
Amount of items: 2
Items: 
Size: 564352 Color: 0
Size: 433810 Color: 1

Bin 716: 1840 of cap free
Amount of items: 2
Items: 
Size: 769757 Color: 1
Size: 228404 Color: 0

Bin 717: 1891 of cap free
Amount of items: 2
Items: 
Size: 577690 Color: 0
Size: 420420 Color: 1

Bin 718: 1954 of cap free
Amount of items: 2
Items: 
Size: 588256 Color: 1
Size: 409791 Color: 0

Bin 719: 1959 of cap free
Amount of items: 2
Items: 
Size: 605014 Color: 1
Size: 393028 Color: 0

Bin 720: 1971 of cap free
Amount of items: 2
Items: 
Size: 657735 Color: 0
Size: 340295 Color: 1

Bin 721: 1982 of cap free
Amount of items: 2
Items: 
Size: 631949 Color: 0
Size: 366070 Color: 1

Bin 722: 2001 of cap free
Amount of items: 2
Items: 
Size: 782528 Color: 1
Size: 215472 Color: 0

Bin 723: 2093 of cap free
Amount of items: 2
Items: 
Size: 673575 Color: 1
Size: 324333 Color: 0

Bin 724: 2095 of cap free
Amount of items: 2
Items: 
Size: 629650 Color: 1
Size: 368256 Color: 0

Bin 725: 2095 of cap free
Amount of items: 2
Items: 
Size: 590633 Color: 0
Size: 407273 Color: 1

Bin 726: 2109 of cap free
Amount of items: 2
Items: 
Size: 766873 Color: 1
Size: 231019 Color: 0

Bin 727: 2118 of cap free
Amount of items: 2
Items: 
Size: 700324 Color: 0
Size: 297559 Color: 1

Bin 728: 2122 of cap free
Amount of items: 2
Items: 
Size: 715916 Color: 0
Size: 281963 Color: 1

Bin 729: 2123 of cap free
Amount of items: 2
Items: 
Size: 739424 Color: 0
Size: 258454 Color: 1

Bin 730: 2130 of cap free
Amount of items: 2
Items: 
Size: 730752 Color: 0
Size: 267119 Color: 1

Bin 731: 2133 of cap free
Amount of items: 2
Items: 
Size: 667496 Color: 0
Size: 330372 Color: 1

Bin 732: 2152 of cap free
Amount of items: 2
Items: 
Size: 667099 Color: 1
Size: 330750 Color: 0

Bin 733: 2157 of cap free
Amount of items: 2
Items: 
Size: 747893 Color: 1
Size: 249951 Color: 0

Bin 734: 2162 of cap free
Amount of items: 2
Items: 
Size: 614017 Color: 1
Size: 383822 Color: 0

Bin 735: 2162 of cap free
Amount of items: 2
Items: 
Size: 559000 Color: 1
Size: 438839 Color: 0

Bin 736: 2163 of cap free
Amount of items: 2
Items: 
Size: 593649 Color: 0
Size: 404189 Color: 1

Bin 737: 2189 of cap free
Amount of items: 2
Items: 
Size: 503545 Color: 1
Size: 494267 Color: 0

Bin 738: 2190 of cap free
Amount of items: 2
Items: 
Size: 717645 Color: 1
Size: 280166 Color: 0

Bin 739: 2194 of cap free
Amount of items: 2
Items: 
Size: 697328 Color: 0
Size: 300479 Color: 1

Bin 740: 2198 of cap free
Amount of items: 3
Items: 
Size: 565889 Color: 1
Size: 313784 Color: 1
Size: 118130 Color: 0

Bin 741: 2201 of cap free
Amount of items: 2
Items: 
Size: 525363 Color: 0
Size: 472437 Color: 1

Bin 742: 2210 of cap free
Amount of items: 2
Items: 
Size: 799102 Color: 1
Size: 198689 Color: 0

Bin 743: 2239 of cap free
Amount of items: 2
Items: 
Size: 604791 Color: 1
Size: 392971 Color: 0

Bin 744: 2251 of cap free
Amount of items: 2
Items: 
Size: 671443 Color: 0
Size: 326307 Color: 1

Bin 745: 2298 of cap free
Amount of items: 2
Items: 
Size: 709227 Color: 0
Size: 288476 Color: 1

Bin 746: 2312 of cap free
Amount of items: 2
Items: 
Size: 719883 Color: 1
Size: 277806 Color: 0

Bin 747: 2326 of cap free
Amount of items: 2
Items: 
Size: 654746 Color: 1
Size: 342929 Color: 0

Bin 748: 2328 of cap free
Amount of items: 2
Items: 
Size: 629540 Color: 1
Size: 368133 Color: 0

Bin 749: 2350 of cap free
Amount of items: 3
Items: 
Size: 573690 Color: 1
Size: 319294 Color: 0
Size: 104667 Color: 0

Bin 750: 2350 of cap free
Amount of items: 2
Items: 
Size: 610656 Color: 0
Size: 386995 Color: 1

Bin 751: 2378 of cap free
Amount of items: 2
Items: 
Size: 743508 Color: 1
Size: 254115 Color: 0

Bin 752: 2380 of cap free
Amount of items: 2
Items: 
Size: 756734 Color: 0
Size: 240887 Color: 1

Bin 753: 2405 of cap free
Amount of items: 2
Items: 
Size: 619455 Color: 1
Size: 378141 Color: 0

Bin 754: 2412 of cap free
Amount of items: 2
Items: 
Size: 734212 Color: 0
Size: 263377 Color: 1

Bin 755: 2472 of cap free
Amount of items: 2
Items: 
Size: 652208 Color: 1
Size: 345321 Color: 0

Bin 756: 2490 of cap free
Amount of items: 2
Items: 
Size: 510460 Color: 0
Size: 487051 Color: 1

Bin 757: 2500 of cap free
Amount of items: 2
Items: 
Size: 756581 Color: 0
Size: 240920 Color: 1

Bin 758: 2508 of cap free
Amount of items: 2
Items: 
Size: 692222 Color: 1
Size: 305271 Color: 0

Bin 759: 2514 of cap free
Amount of items: 2
Items: 
Size: 676224 Color: 1
Size: 321263 Color: 0

Bin 760: 2516 of cap free
Amount of items: 2
Items: 
Size: 534171 Color: 1
Size: 463314 Color: 0

Bin 761: 2589 of cap free
Amount of items: 2
Items: 
Size: 798978 Color: 1
Size: 198434 Color: 0

Bin 762: 2596 of cap free
Amount of items: 2
Items: 
Size: 654941 Color: 1
Size: 342464 Color: 0

Bin 763: 2608 of cap free
Amount of items: 2
Items: 
Size: 713139 Color: 1
Size: 284254 Color: 0

Bin 764: 2615 of cap free
Amount of items: 2
Items: 
Size: 614541 Color: 0
Size: 382845 Color: 1

Bin 765: 2634 of cap free
Amount of items: 2
Items: 
Size: 637576 Color: 1
Size: 359791 Color: 0

Bin 766: 2651 of cap free
Amount of items: 2
Items: 
Size: 711785 Color: 0
Size: 285565 Color: 1

Bin 767: 2688 of cap free
Amount of items: 2
Items: 
Size: 577314 Color: 1
Size: 419999 Color: 0

Bin 768: 2704 of cap free
Amount of items: 2
Items: 
Size: 614512 Color: 0
Size: 382785 Color: 1

Bin 769: 2705 of cap free
Amount of items: 2
Items: 
Size: 604510 Color: 1
Size: 392786 Color: 0

Bin 770: 2745 of cap free
Amount of items: 2
Items: 
Size: 524739 Color: 1
Size: 472517 Color: 0

Bin 771: 2806 of cap free
Amount of items: 2
Items: 
Size: 659189 Color: 1
Size: 338006 Color: 0

Bin 772: 2836 of cap free
Amount of items: 2
Items: 
Size: 675998 Color: 1
Size: 321167 Color: 0

Bin 773: 2845 of cap free
Amount of items: 2
Items: 
Size: 725684 Color: 1
Size: 271472 Color: 0

Bin 774: 2845 of cap free
Amount of items: 2
Items: 
Size: 626066 Color: 1
Size: 371090 Color: 0

Bin 775: 2859 of cap free
Amount of items: 2
Items: 
Size: 673996 Color: 0
Size: 323146 Color: 1

Bin 776: 2885 of cap free
Amount of items: 2
Items: 
Size: 499214 Color: 0
Size: 497902 Color: 1

Bin 777: 2916 of cap free
Amount of items: 2
Items: 
Size: 528269 Color: 0
Size: 468816 Color: 1

Bin 778: 2935 of cap free
Amount of items: 2
Items: 
Size: 592314 Color: 1
Size: 404752 Color: 0

Bin 779: 2937 of cap free
Amount of items: 2
Items: 
Size: 798003 Color: 0
Size: 199061 Color: 1

Bin 780: 2938 of cap free
Amount of items: 2
Items: 
Size: 510720 Color: 1
Size: 486343 Color: 0

Bin 781: 2950 of cap free
Amount of items: 2
Items: 
Size: 669729 Color: 0
Size: 327322 Color: 1

Bin 782: 2964 of cap free
Amount of items: 2
Items: 
Size: 763222 Color: 0
Size: 233815 Color: 1

Bin 783: 2977 of cap free
Amount of items: 2
Items: 
Size: 525141 Color: 0
Size: 471883 Color: 1

Bin 784: 2995 of cap free
Amount of items: 2
Items: 
Size: 524698 Color: 1
Size: 472308 Color: 0

Bin 785: 2999 of cap free
Amount of items: 2
Items: 
Size: 726148 Color: 1
Size: 270854 Color: 0

Bin 786: 3024 of cap free
Amount of items: 2
Items: 
Size: 738174 Color: 0
Size: 258803 Color: 1

Bin 787: 3085 of cap free
Amount of items: 2
Items: 
Size: 605632 Color: 0
Size: 391284 Color: 1

Bin 788: 3108 of cap free
Amount of items: 2
Items: 
Size: 517029 Color: 0
Size: 479864 Color: 1

Bin 789: 3134 of cap free
Amount of items: 2
Items: 
Size: 614124 Color: 0
Size: 382743 Color: 1

Bin 790: 3137 of cap free
Amount of items: 2
Items: 
Size: 656972 Color: 0
Size: 339892 Color: 1

Bin 791: 3142 of cap free
Amount of items: 2
Items: 
Size: 610664 Color: 1
Size: 386195 Color: 0

Bin 792: 3171 of cap free
Amount of items: 2
Items: 
Size: 749592 Color: 0
Size: 247238 Color: 1

Bin 793: 3221 of cap free
Amount of items: 2
Items: 
Size: 721907 Color: 0
Size: 274873 Color: 1

Bin 794: 3242 of cap free
Amount of items: 2
Items: 
Size: 642882 Color: 0
Size: 353877 Color: 1

Bin 795: 3280 of cap free
Amount of items: 2
Items: 
Size: 551943 Color: 0
Size: 444778 Color: 1

Bin 796: 3281 of cap free
Amount of items: 2
Items: 
Size: 625441 Color: 0
Size: 371279 Color: 1

Bin 797: 3301 of cap free
Amount of items: 2
Items: 
Size: 730160 Color: 1
Size: 266540 Color: 0

Bin 798: 3309 of cap free
Amount of items: 2
Items: 
Size: 564030 Color: 0
Size: 432662 Color: 1

Bin 799: 3336 of cap free
Amount of items: 2
Items: 
Size: 583767 Color: 0
Size: 412898 Color: 1

Bin 800: 3338 of cap free
Amount of items: 2
Items: 
Size: 547970 Color: 1
Size: 448693 Color: 0

Bin 801: 3339 of cap free
Amount of items: 2
Items: 
Size: 537654 Color: 0
Size: 459008 Color: 1

Bin 802: 3428 of cap free
Amount of items: 2
Items: 
Size: 555735 Color: 0
Size: 440838 Color: 1

Bin 803: 3429 of cap free
Amount of items: 2
Items: 
Size: 663714 Color: 0
Size: 332858 Color: 1

Bin 804: 3503 of cap free
Amount of items: 2
Items: 
Size: 592215 Color: 1
Size: 404283 Color: 0

Bin 805: 3533 of cap free
Amount of items: 2
Items: 
Size: 777702 Color: 0
Size: 218766 Color: 1

Bin 806: 3584 of cap free
Amount of items: 2
Items: 
Size: 663506 Color: 0
Size: 332911 Color: 1

Bin 807: 3666 of cap free
Amount of items: 2
Items: 
Size: 547909 Color: 1
Size: 448426 Color: 0

Bin 808: 3686 of cap free
Amount of items: 2
Items: 
Size: 778600 Color: 0
Size: 217715 Color: 1

Bin 809: 3771 of cap free
Amount of items: 2
Items: 
Size: 686304 Color: 0
Size: 309926 Color: 1

Bin 810: 3789 of cap free
Amount of items: 2
Items: 
Size: 625155 Color: 0
Size: 371057 Color: 1

Bin 811: 3869 of cap free
Amount of items: 2
Items: 
Size: 625604 Color: 1
Size: 370528 Color: 0

Bin 812: 3894 of cap free
Amount of items: 2
Items: 
Size: 650240 Color: 0
Size: 345867 Color: 1

Bin 813: 3900 of cap free
Amount of items: 2
Items: 
Size: 630407 Color: 0
Size: 365694 Color: 1

Bin 814: 4012 of cap free
Amount of items: 2
Items: 
Size: 663070 Color: 1
Size: 332919 Color: 0

Bin 815: 4084 of cap free
Amount of items: 2
Items: 
Size: 516426 Color: 0
Size: 479491 Color: 1

Bin 816: 4186 of cap free
Amount of items: 2
Items: 
Size: 632698 Color: 1
Size: 363117 Color: 0

Bin 817: 4194 of cap free
Amount of items: 2
Items: 
Size: 798085 Color: 1
Size: 197722 Color: 0

Bin 818: 4202 of cap free
Amount of items: 2
Items: 
Size: 791226 Color: 1
Size: 204573 Color: 0

Bin 819: 4285 of cap free
Amount of items: 2
Items: 
Size: 778416 Color: 0
Size: 217300 Color: 1

Bin 820: 4297 of cap free
Amount of items: 2
Items: 
Size: 786700 Color: 0
Size: 209004 Color: 1

Bin 821: 4427 of cap free
Amount of items: 2
Items: 
Size: 498105 Color: 0
Size: 497469 Color: 1

Bin 822: 4446 of cap free
Amount of items: 2
Items: 
Size: 624715 Color: 0
Size: 370840 Color: 1

Bin 823: 4465 of cap free
Amount of items: 2
Items: 
Size: 551245 Color: 0
Size: 444291 Color: 1

Bin 824: 4498 of cap free
Amount of items: 2
Items: 
Size: 558871 Color: 1
Size: 436632 Color: 0

Bin 825: 4507 of cap free
Amount of items: 2
Items: 
Size: 643094 Color: 1
Size: 352400 Color: 0

Bin 826: 4601 of cap free
Amount of items: 2
Items: 
Size: 728445 Color: 0
Size: 266955 Color: 1

Bin 827: 4634 of cap free
Amount of items: 2
Items: 
Size: 575521 Color: 1
Size: 419846 Color: 0

Bin 828: 4649 of cap free
Amount of items: 2
Items: 
Size: 724697 Color: 1
Size: 270655 Color: 0

Bin 829: 4774 of cap free
Amount of items: 2
Items: 
Size: 583719 Color: 0
Size: 411508 Color: 1

Bin 830: 5014 of cap free
Amount of items: 2
Items: 
Size: 680887 Color: 0
Size: 314100 Color: 1

Bin 831: 5015 of cap free
Amount of items: 2
Items: 
Size: 649465 Color: 0
Size: 345521 Color: 1

Bin 832: 5065 of cap free
Amount of items: 2
Items: 
Size: 778510 Color: 0
Size: 216426 Color: 1

Bin 833: 5147 of cap free
Amount of items: 2
Items: 
Size: 563892 Color: 0
Size: 430962 Color: 1

Bin 834: 5157 of cap free
Amount of items: 2
Items: 
Size: 597900 Color: 0
Size: 396944 Color: 1

Bin 835: 5263 of cap free
Amount of items: 2
Items: 
Size: 779030 Color: 0
Size: 215708 Color: 1

Bin 836: 5274 of cap free
Amount of items: 2
Items: 
Size: 575424 Color: 1
Size: 419303 Color: 0

Bin 837: 5279 of cap free
Amount of items: 2
Items: 
Size: 522653 Color: 1
Size: 472069 Color: 0

Bin 838: 5297 of cap free
Amount of items: 2
Items: 
Size: 497809 Color: 0
Size: 496895 Color: 1

Bin 839: 5303 of cap free
Amount of items: 2
Items: 
Size: 786599 Color: 0
Size: 208099 Color: 1

Bin 840: 5461 of cap free
Amount of items: 2
Items: 
Size: 721561 Color: 0
Size: 272979 Color: 1

Bin 841: 5635 of cap free
Amount of items: 2
Items: 
Size: 771708 Color: 1
Size: 222658 Color: 0

Bin 842: 5652 of cap free
Amount of items: 2
Items: 
Size: 631914 Color: 1
Size: 362435 Color: 0

Bin 843: 5659 of cap free
Amount of items: 2
Items: 
Size: 515400 Color: 0
Size: 478942 Color: 1

Bin 844: 5878 of cap free
Amount of items: 2
Items: 
Size: 563886 Color: 0
Size: 430237 Color: 1

Bin 845: 5891 of cap free
Amount of items: 2
Items: 
Size: 719924 Color: 0
Size: 274186 Color: 1

Bin 846: 5961 of cap free
Amount of items: 2
Items: 
Size: 777969 Color: 0
Size: 216071 Color: 1

Bin 847: 6257 of cap free
Amount of items: 2
Items: 
Size: 584100 Color: 1
Size: 409644 Color: 0

Bin 848: 6332 of cap free
Amount of items: 2
Items: 
Size: 613368 Color: 0
Size: 380301 Color: 1

Bin 849: 6432 of cap free
Amount of items: 2
Items: 
Size: 661249 Color: 0
Size: 332320 Color: 1

Bin 850: 6476 of cap free
Amount of items: 2
Items: 
Size: 613667 Color: 0
Size: 379858 Color: 1

Bin 851: 6625 of cap free
Amount of items: 2
Items: 
Size: 751780 Color: 0
Size: 241596 Color: 1

Bin 852: 6670 of cap free
Amount of items: 2
Items: 
Size: 794359 Color: 0
Size: 198972 Color: 1

Bin 853: 6725 of cap free
Amount of items: 2
Items: 
Size: 687485 Color: 0
Size: 305791 Color: 1

Bin 854: 6769 of cap free
Amount of items: 2
Items: 
Size: 508072 Color: 1
Size: 485160 Color: 0

Bin 855: 6830 of cap free
Amount of items: 2
Items: 
Size: 730126 Color: 1
Size: 263045 Color: 0

Bin 856: 6965 of cap free
Amount of items: 2
Items: 
Size: 524381 Color: 0
Size: 468655 Color: 1

Bin 857: 7068 of cap free
Amount of items: 2
Items: 
Size: 629529 Color: 0
Size: 363404 Color: 1

Bin 858: 7215 of cap free
Amount of items: 2
Items: 
Size: 591962 Color: 1
Size: 400824 Color: 0

Bin 859: 7326 of cap free
Amount of items: 2
Items: 
Size: 573384 Color: 1
Size: 419291 Color: 0

Bin 860: 7339 of cap free
Amount of items: 4
Items: 
Size: 445757 Color: 0
Size: 207436 Color: 0
Size: 186878 Color: 1
Size: 152591 Color: 1

Bin 861: 7472 of cap free
Amount of items: 2
Items: 
Size: 600292 Color: 1
Size: 392237 Color: 0

Bin 862: 7628 of cap free
Amount of items: 2
Items: 
Size: 777948 Color: 0
Size: 214425 Color: 1

Bin 863: 7776 of cap free
Amount of items: 2
Items: 
Size: 520246 Color: 1
Size: 471979 Color: 0

Bin 864: 7843 of cap free
Amount of items: 2
Items: 
Size: 776642 Color: 0
Size: 215516 Color: 1

Bin 865: 7886 of cap free
Amount of items: 3
Items: 
Size: 542969 Color: 1
Size: 330640 Color: 0
Size: 118506 Color: 1

Bin 866: 7917 of cap free
Amount of items: 2
Items: 
Size: 794624 Color: 0
Size: 197460 Color: 1

Bin 867: 7972 of cap free
Amount of items: 2
Items: 
Size: 595932 Color: 0
Size: 396097 Color: 1

Bin 868: 8152 of cap free
Amount of items: 2
Items: 
Size: 795785 Color: 1
Size: 196064 Color: 0

Bin 869: 8181 of cap free
Amount of items: 2
Items: 
Size: 762929 Color: 0
Size: 228891 Color: 1

Bin 870: 8194 of cap free
Amount of items: 2
Items: 
Size: 776602 Color: 0
Size: 215205 Color: 1

Bin 871: 8340 of cap free
Amount of items: 2
Items: 
Size: 742769 Color: 1
Size: 248892 Color: 0

Bin 872: 8419 of cap free
Amount of items: 2
Items: 
Size: 769182 Color: 1
Size: 222400 Color: 0

Bin 873: 8564 of cap free
Amount of items: 2
Items: 
Size: 533738 Color: 1
Size: 457699 Color: 0

Bin 874: 8993 of cap free
Amount of items: 2
Items: 
Size: 646461 Color: 0
Size: 344547 Color: 1

Bin 875: 9014 of cap free
Amount of items: 2
Items: 
Size: 562790 Color: 0
Size: 428197 Color: 1

Bin 876: 9131 of cap free
Amount of items: 2
Items: 
Size: 534876 Color: 0
Size: 455994 Color: 1

Bin 877: 9285 of cap free
Amount of items: 3
Items: 
Size: 567333 Color: 1
Size: 298806 Color: 1
Size: 124577 Color: 0

Bin 878: 9582 of cap free
Amount of items: 2
Items: 
Size: 776802 Color: 0
Size: 213617 Color: 1

Bin 879: 9925 of cap free
Amount of items: 2
Items: 
Size: 572844 Color: 1
Size: 417232 Color: 0

Bin 880: 10087 of cap free
Amount of items: 2
Items: 
Size: 599335 Color: 1
Size: 390579 Color: 0

Bin 881: 10942 of cap free
Amount of items: 2
Items: 
Size: 613633 Color: 0
Size: 375426 Color: 1

Bin 882: 11356 of cap free
Amount of items: 2
Items: 
Size: 742020 Color: 1
Size: 246625 Color: 0

Bin 883: 11633 of cap free
Amount of items: 4
Items: 
Size: 502791 Color: 1
Size: 185676 Color: 1
Size: 179144 Color: 0
Size: 120757 Color: 0

Bin 884: 11655 of cap free
Amount of items: 2
Items: 
Size: 609189 Color: 0
Size: 379157 Color: 1

Bin 885: 11950 of cap free
Amount of items: 2
Items: 
Size: 503271 Color: 1
Size: 484780 Color: 0

Bin 886: 12482 of cap free
Amount of items: 2
Items: 
Size: 662721 Color: 1
Size: 324798 Color: 0

Bin 887: 12648 of cap free
Amount of items: 2
Items: 
Size: 792630 Color: 0
Size: 194723 Color: 1

Bin 888: 13160 of cap free
Amount of items: 2
Items: 
Size: 580287 Color: 0
Size: 406554 Color: 1

Bin 889: 14469 of cap free
Amount of items: 6
Items: 
Size: 178417 Color: 1
Size: 164816 Color: 1
Size: 163657 Color: 0
Size: 162215 Color: 1
Size: 161322 Color: 0
Size: 155105 Color: 0

Bin 890: 16575 of cap free
Amount of items: 2
Items: 
Size: 680387 Color: 0
Size: 303039 Color: 1

Bin 891: 16869 of cap free
Amount of items: 2
Items: 
Size: 738483 Color: 0
Size: 244649 Color: 1

Bin 892: 16876 of cap free
Amount of items: 2
Items: 
Size: 788800 Color: 1
Size: 194325 Color: 0

Bin 893: 19246 of cap free
Amount of items: 2
Items: 
Size: 522969 Color: 0
Size: 457786 Color: 1

Bin 894: 41590 of cap free
Amount of items: 3
Items: 
Size: 539035 Color: 0
Size: 220776 Color: 0
Size: 198600 Color: 1

Bin 895: 42590 of cap free
Amount of items: 2
Items: 
Size: 641318 Color: 0
Size: 316093 Color: 1

Bin 896: 56712 of cap free
Amount of items: 3
Items: 
Size: 563386 Color: 0
Size: 190044 Color: 0
Size: 189859 Color: 1

Bin 897: 57592 of cap free
Amount of items: 2
Items: 
Size: 697534 Color: 0
Size: 244875 Color: 1

Bin 898: 62834 of cap free
Amount of items: 2
Items: 
Size: 573682 Color: 0
Size: 363485 Color: 1

Bin 899: 71353 of cap free
Amount of items: 2
Items: 
Size: 574197 Color: 1
Size: 354451 Color: 0

Bin 900: 184806 of cap free
Amount of items: 6
Items: 
Size: 144712 Color: 0
Size: 144241 Color: 1
Size: 138059 Color: 1
Size: 135466 Color: 0
Size: 134370 Color: 0
Size: 118347 Color: 1

Total size: 898317736
Total free space: 1683164

