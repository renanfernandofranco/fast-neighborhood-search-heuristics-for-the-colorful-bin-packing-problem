Capicity Bin: 16480
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8276 Color: 412
Size: 6844 Color: 395
Size: 560 Color: 105
Size: 404 Color: 69
Size: 396 Color: 67

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 9358 Color: 421
Size: 5938 Color: 385
Size: 432 Color: 78
Size: 376 Color: 58
Size: 376 Color: 57

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 465
Size: 2678 Color: 295
Size: 1672 Color: 223

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12161 Color: 467
Size: 3601 Color: 338
Size: 718 Color: 139

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 484
Size: 3012 Color: 317
Size: 672 Color: 132

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 489
Size: 2942 Color: 314
Size: 584 Color: 116

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12957 Color: 490
Size: 2937 Color: 313
Size: 586 Color: 118

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12974 Color: 491
Size: 1978 Color: 255
Size: 1528 Color: 213

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13025 Color: 494
Size: 2567 Color: 288
Size: 888 Color: 161

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 496
Size: 2402 Color: 282
Size: 1046 Color: 179

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 498
Size: 3139 Color: 319
Size: 288 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13077 Color: 500
Size: 3227 Color: 324
Size: 176 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13097 Color: 502
Size: 1881 Color: 246
Size: 1502 Color: 211

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 503
Size: 2808 Color: 301
Size: 560 Color: 106

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13117 Color: 504
Size: 2803 Color: 300
Size: 560 Color: 107

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13128 Color: 505
Size: 3004 Color: 316
Size: 348 Color: 43

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13361 Color: 513
Size: 2431 Color: 285
Size: 688 Color: 134

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13385 Color: 514
Size: 2581 Color: 290
Size: 514 Color: 96

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 516
Size: 2260 Color: 274
Size: 804 Color: 151

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 517
Size: 2552 Color: 287
Size: 496 Color: 94

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 519
Size: 2428 Color: 284
Size: 480 Color: 90

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 527
Size: 2328 Color: 278
Size: 464 Color: 86

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 530
Size: 1792 Color: 234
Size: 916 Color: 167

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 534
Size: 2336 Color: 279
Size: 296 Color: 25

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 535
Size: 2190 Color: 269
Size: 436 Color: 80

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 538
Size: 1372 Color: 202
Size: 1168 Color: 183

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 539
Size: 1524 Color: 212
Size: 992 Color: 172

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 540
Size: 1692 Color: 224
Size: 812 Color: 153

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 542
Size: 1928 Color: 251
Size: 540 Color: 102

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14021 Color: 543
Size: 2051 Color: 261
Size: 408 Color: 72

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14086 Color: 545
Size: 1998 Color: 258
Size: 396 Color: 66

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 548
Size: 2008 Color: 259
Size: 362 Color: 50

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14113 Color: 549
Size: 1973 Color: 254
Size: 394 Color: 65

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14161 Color: 550
Size: 1933 Color: 252
Size: 386 Color: 63

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 551
Size: 1752 Color: 230
Size: 560 Color: 108

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14185 Color: 553
Size: 1777 Color: 233
Size: 518 Color: 97

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 558
Size: 1564 Color: 216
Size: 680 Color: 133

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14250 Color: 559
Size: 1622 Color: 220
Size: 608 Color: 122

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14270 Color: 561
Size: 1796 Color: 235
Size: 414 Color: 74

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14301 Color: 563
Size: 1837 Color: 240
Size: 342 Color: 39

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 565
Size: 1368 Color: 198
Size: 800 Color: 150

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 569
Size: 1196 Color: 190
Size: 924 Color: 169

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14369 Color: 570
Size: 1859 Color: 243
Size: 252 Color: 12

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14370 Color: 571
Size: 1732 Color: 227
Size: 378 Color: 59

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 572
Size: 1588 Color: 218
Size: 512 Color: 95

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14393 Color: 574
Size: 1741 Color: 228
Size: 346 Color: 42

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14394 Color: 575
Size: 1554 Color: 215
Size: 532 Color: 100

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 578
Size: 1762 Color: 231
Size: 280 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 582
Size: 1376 Color: 203
Size: 570 Color: 112

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 584
Size: 1484 Color: 208
Size: 416 Color: 75

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14646 Color: 589
Size: 1742 Color: 229
Size: 92 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14648 Color: 590
Size: 1184 Color: 186
Size: 648 Color: 130

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14694 Color: 593
Size: 1450 Color: 207
Size: 336 Color: 37

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 594
Size: 1184 Color: 187
Size: 600 Color: 121

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14742 Color: 596
Size: 1368 Color: 199
Size: 370 Color: 55

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14824 Color: 599
Size: 1176 Color: 185
Size: 480 Color: 91

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14828 Color: 600
Size: 1196 Color: 191
Size: 456 Color: 85

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12593 Color: 478
Size: 3566 Color: 334
Size: 320 Color: 34

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 483
Size: 2857 Color: 306
Size: 910 Color: 163

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12923 Color: 488
Size: 2724 Color: 298
Size: 832 Color: 156

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 495
Size: 2922 Color: 312
Size: 528 Color: 98

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13073 Color: 499
Size: 2670 Color: 293
Size: 736 Color: 142

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13582 Color: 520
Size: 2897 Color: 311

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13602 Color: 523
Size: 2877 Color: 308

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13829 Color: 532
Size: 1882 Color: 247
Size: 768 Color: 146

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 536
Size: 2601 Color: 291

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 14225 Color: 557
Size: 2254 Color: 273

Bin 68: 2 of cap free
Amount of items: 5
Items: 
Size: 9380 Color: 422
Size: 5958 Color: 387
Size: 398 Color: 68
Size: 374 Color: 56
Size: 368 Color: 54

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 10961 Color: 441
Size: 5213 Color: 371
Size: 304 Color: 29

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 12586 Color: 477
Size: 3892 Color: 343

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 481
Size: 3688 Color: 340
Size: 144 Color: 6

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 13597 Color: 522
Size: 2881 Color: 309

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 573
Size: 2088 Color: 264

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 14776 Color: 598
Size: 1702 Color: 225

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 10201 Color: 428
Size: 5924 Color: 384
Size: 352 Color: 45

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 13049 Color: 497
Size: 3428 Color: 331

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 13401 Color: 515
Size: 3076 Color: 318

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 13767 Color: 529
Size: 2710 Color: 297

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 13993 Color: 541
Size: 2484 Color: 286

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 14102 Color: 547
Size: 2375 Color: 281

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 14277 Color: 562
Size: 2200 Color: 270

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 576
Size: 2073 Color: 263

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 14417 Color: 577
Size: 2060 Color: 262

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 585
Size: 1893 Color: 248

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 591
Size: 1817 Color: 239

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 11772 Color: 460
Size: 4440 Color: 355
Size: 264 Color: 13

Bin 87: 4 of cap free
Amount of items: 4
Items: 
Size: 11860 Color: 463
Size: 4184 Color: 352
Size: 216 Color: 11
Size: 216 Color: 10

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 468
Size: 2808 Color: 302
Size: 1490 Color: 209

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 12552 Color: 475
Size: 3924 Color: 345

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 12633 Color: 480
Size: 3571 Color: 335
Size: 272 Color: 18

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13230 Color: 508
Size: 3246 Color: 327

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13278 Color: 511
Size: 3198 Color: 322

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 586
Size: 1876 Color: 245

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14708 Color: 595
Size: 1768 Color: 232

Bin 95: 5 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 472
Size: 4051 Color: 349
Size: 92 Color: 3

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 13210 Color: 506
Size: 3265 Color: 328

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 13268 Color: 510
Size: 3207 Color: 323

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 13634 Color: 525
Size: 2841 Color: 305

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 14305 Color: 564
Size: 2170 Color: 268

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 14678 Color: 592
Size: 1797 Color: 236

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 14754 Color: 597
Size: 1721 Color: 226

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 452
Size: 4730 Color: 365
Size: 296 Color: 24

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 521
Size: 2888 Color: 310

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 14174 Color: 552
Size: 2300 Color: 277

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 14325 Color: 566
Size: 2149 Color: 267

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14336 Color: 567
Size: 2138 Color: 266

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 579
Size: 2022 Color: 260

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 583
Size: 1916 Color: 250

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 587
Size: 1862 Color: 244

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 10177 Color: 427
Size: 5944 Color: 386
Size: 352 Color: 46

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 10237 Color: 433
Size: 5892 Color: 383
Size: 344 Color: 41

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 458
Size: 4543 Color: 358
Size: 272 Color: 15

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 474
Size: 4005 Color: 346
Size: 48 Color: 0

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 14349 Color: 568
Size: 2124 Color: 265

Bin 115: 8 of cap free
Amount of items: 9
Items: 
Size: 8242 Color: 406
Size: 1368 Color: 197
Size: 1360 Color: 196
Size: 1360 Color: 195
Size: 1296 Color: 194
Size: 1232 Color: 193
Size: 742 Color: 143
Size: 440 Color: 81
Size: 432 Color: 79

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 11634 Color: 456
Size: 4566 Color: 360
Size: 272 Color: 17

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13651 Color: 526
Size: 2821 Color: 303

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13830 Color: 533
Size: 2642 Color: 292

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 14054 Color: 544
Size: 2418 Color: 283

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 554
Size: 2284 Color: 276

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 581
Size: 1984 Color: 257

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 14630 Color: 588
Size: 1842 Color: 241

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 13631 Color: 524
Size: 2840 Color: 304

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 13903 Color: 537
Size: 2568 Color: 289

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 9286 Color: 416
Size: 7184 Color: 403

Bin 126: 10 of cap free
Amount of items: 3
Items: 
Size: 11621 Color: 455
Size: 4561 Color: 359
Size: 288 Color: 20

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 12884 Color: 487
Size: 3586 Color: 336

Bin 128: 10 of cap free
Amount of items: 2
Items: 
Size: 14209 Color: 555
Size: 2261 Color: 275

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 12617 Color: 479
Size: 3852 Color: 342

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 12868 Color: 486
Size: 3601 Color: 337

Bin 131: 11 of cap free
Amount of items: 2
Items: 
Size: 13005 Color: 493
Size: 3464 Color: 333

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 432
Size: 5884 Color: 382
Size: 352 Color: 44

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 13080 Color: 501
Size: 3388 Color: 330

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 14222 Color: 556
Size: 2246 Color: 272

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 14486 Color: 580
Size: 1982 Color: 256

Bin 136: 13 of cap free
Amount of items: 3
Items: 
Size: 12159 Color: 466
Size: 3236 Color: 325
Size: 1072 Color: 181

Bin 137: 14 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 464
Size: 4200 Color: 353
Size: 210 Color: 9

Bin 138: 14 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 471
Size: 4042 Color: 348
Size: 96 Color: 4

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 13740 Color: 528
Size: 2726 Color: 299

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 518
Size: 2965 Color: 315

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 14253 Color: 560
Size: 2211 Color: 271

Bin 142: 17 of cap free
Amount of items: 3
Items: 
Size: 11001 Color: 444
Size: 4412 Color: 354
Size: 1050 Color: 180

Bin 143: 18 of cap free
Amount of items: 7
Items: 
Size: 8244 Color: 408
Size: 1658 Color: 221
Size: 1602 Color: 219
Size: 1576 Color: 217
Size: 1542 Color: 214
Size: 1432 Color: 206
Size: 408 Color: 73

Bin 144: 18 of cap free
Amount of items: 3
Items: 
Size: 12372 Color: 473
Size: 4022 Color: 347
Size: 68 Color: 1

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 12836 Color: 485
Size: 3626 Color: 339

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 531
Size: 2684 Color: 296

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 546
Size: 2374 Color: 280

Bin 148: 19 of cap free
Amount of items: 2
Items: 
Size: 13220 Color: 507
Size: 3241 Color: 326

Bin 149: 20 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 454
Size: 4586 Color: 362
Size: 288 Color: 21

Bin 150: 21 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 476
Size: 3896 Color: 344

Bin 151: 22 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 448
Size: 5346 Color: 376

Bin 152: 24 of cap free
Amount of items: 7
Items: 
Size: 8248 Color: 409
Size: 1857 Color: 242
Size: 1816 Color: 238
Size: 1813 Color: 237
Size: 1662 Color: 222
Size: 652 Color: 131
Size: 408 Color: 71

Bin 153: 24 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 440
Size: 5222 Color: 373
Size: 304 Color: 30

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 13310 Color: 512
Size: 3144 Color: 320

Bin 155: 28 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 451
Size: 4964 Color: 367
Size: 300 Color: 26

Bin 156: 33 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 469
Size: 4082 Color: 350
Size: 168 Color: 7

Bin 157: 38 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 462
Size: 4626 Color: 364

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 509
Size: 3178 Color: 321

Bin 159: 39 of cap free
Amount of items: 2
Items: 
Size: 12670 Color: 482
Size: 3771 Color: 341

Bin 160: 39 of cap free
Amount of items: 2
Items: 
Size: 12981 Color: 492
Size: 3460 Color: 332

Bin 161: 45 of cap free
Amount of items: 3
Items: 
Size: 11675 Color: 459
Size: 4488 Color: 357
Size: 272 Color: 14

Bin 162: 55 of cap free
Amount of items: 2
Items: 
Size: 11009 Color: 446
Size: 5416 Color: 378

Bin 163: 63 of cap free
Amount of items: 3
Items: 
Size: 11029 Color: 447
Size: 5084 Color: 368
Size: 304 Color: 28

Bin 164: 67 of cap free
Amount of items: 2
Items: 
Size: 11812 Color: 461
Size: 4601 Color: 363

Bin 165: 69 of cap free
Amount of items: 4
Items: 
Size: 9704 Color: 425
Size: 5987 Color: 389
Size: 362 Color: 49
Size: 358 Color: 48

Bin 166: 71 of cap free
Amount of items: 2
Items: 
Size: 10225 Color: 431
Size: 6184 Color: 393

Bin 167: 71 of cap free
Amount of items: 3
Items: 
Size: 12202 Color: 470
Size: 4087 Color: 351
Size: 120 Color: 5

Bin 168: 87 of cap free
Amount of items: 2
Items: 
Size: 11140 Color: 449
Size: 5253 Color: 375

Bin 169: 91 of cap free
Amount of items: 2
Items: 
Size: 11002 Color: 445
Size: 5387 Color: 377

Bin 170: 92 of cap free
Amount of items: 2
Items: 
Size: 8260 Color: 411
Size: 8128 Color: 404

Bin 171: 103 of cap free
Amount of items: 2
Items: 
Size: 10217 Color: 430
Size: 6160 Color: 391

Bin 172: 104 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 437
Size: 5532 Color: 379
Size: 320 Color: 33

Bin 173: 105 of cap free
Amount of items: 3
Items: 
Size: 11651 Color: 457
Size: 4452 Color: 356
Size: 272 Color: 16

Bin 174: 106 of cap free
Amount of items: 2
Items: 
Size: 10214 Color: 429
Size: 6160 Color: 392

Bin 175: 120 of cap free
Amount of items: 3
Items: 
Size: 10380 Color: 436
Size: 5656 Color: 381
Size: 324 Color: 35

Bin 176: 129 of cap free
Amount of items: 3
Items: 
Size: 9104 Color: 415
Size: 6865 Color: 398
Size: 382 Color: 60

Bin 177: 134 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 424
Size: 6552 Color: 394
Size: 366 Color: 51

Bin 178: 136 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 426
Size: 5998 Color: 390
Size: 354 Color: 47

Bin 179: 145 of cap free
Amount of items: 3
Items: 
Size: 10806 Color: 439
Size: 5221 Color: 372
Size: 308 Color: 31

Bin 180: 147 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 453
Size: 4581 Color: 361
Size: 288 Color: 22

Bin 181: 168 of cap free
Amount of items: 3
Items: 
Size: 10332 Color: 435
Size: 5648 Color: 380
Size: 332 Color: 36

Bin 182: 170 of cap free
Amount of items: 3
Items: 
Size: 9064 Color: 414
Size: 6862 Color: 397
Size: 384 Color: 61

Bin 183: 200 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 438
Size: 5208 Color: 370
Size: 312 Color: 32

Bin 184: 220 of cap free
Amount of items: 4
Items: 
Size: 8632 Color: 413
Size: 6852 Color: 396
Size: 392 Color: 64
Size: 384 Color: 62

Bin 185: 237 of cap free
Amount of items: 3
Items: 
Size: 10985 Color: 443
Size: 3288 Color: 329
Size: 1970 Color: 253

Bin 186: 240 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 450
Size: 4776 Color: 366
Size: 304 Color: 27

Bin 187: 256 of cap free
Amount of items: 2
Items: 
Size: 9352 Color: 420
Size: 6872 Color: 402

Bin 188: 269 of cap free
Amount of items: 2
Items: 
Size: 10978 Color: 442
Size: 5233 Color: 374

Bin 189: 278 of cap free
Amount of items: 2
Items: 
Size: 9334 Color: 419
Size: 6868 Color: 401

Bin 190: 314 of cap free
Amount of items: 2
Items: 
Size: 9299 Color: 418
Size: 6867 Color: 400

Bin 191: 317 of cap free
Amount of items: 2
Items: 
Size: 9297 Color: 417
Size: 6866 Color: 399

Bin 192: 331 of cap free
Amount of items: 4
Items: 
Size: 10262 Color: 434
Size: 5203 Color: 369
Size: 344 Color: 40
Size: 340 Color: 38

Bin 193: 334 of cap free
Amount of items: 19
Items: 
Size: 1042 Color: 177
Size: 1042 Color: 176
Size: 1040 Color: 175
Size: 1040 Color: 174
Size: 1016 Color: 173
Size: 944 Color: 171
Size: 944 Color: 170
Size: 918 Color: 168
Size: 914 Color: 166
Size: 912 Color: 165
Size: 912 Color: 164
Size: 908 Color: 162
Size: 880 Color: 160
Size: 880 Color: 159
Size: 880 Color: 158
Size: 476 Color: 89
Size: 474 Color: 88
Size: 472 Color: 87
Size: 452 Color: 84

Bin 194: 347 of cap free
Amount of items: 4
Items: 
Size: 9412 Color: 423
Size: 5985 Color: 388
Size: 368 Color: 53
Size: 368 Color: 52

Bin 195: 379 of cap free
Amount of items: 5
Items: 
Size: 8250 Color: 410
Size: 2861 Color: 307
Size: 2673 Color: 294
Size: 1913 Color: 249
Size: 404 Color: 70

Bin 196: 389 of cap free
Amount of items: 8
Items: 
Size: 8243 Color: 407
Size: 1496 Color: 210
Size: 1384 Color: 205
Size: 1380 Color: 204
Size: 1372 Color: 201
Size: 1372 Color: 200
Size: 428 Color: 77
Size: 416 Color: 76

Bin 197: 403 of cap free
Amount of items: 9
Items: 
Size: 8241 Color: 405
Size: 1216 Color: 192
Size: 1196 Color: 189
Size: 1188 Color: 188
Size: 1176 Color: 184
Size: 1120 Color: 182
Size: 1044 Color: 178
Size: 448 Color: 83
Size: 448 Color: 82

Bin 198: 410 of cap free
Amount of items: 24
Items: 
Size: 872 Color: 157
Size: 832 Color: 155
Size: 816 Color: 154
Size: 808 Color: 152
Size: 800 Color: 149
Size: 784 Color: 148
Size: 776 Color: 147
Size: 768 Color: 145
Size: 752 Color: 144
Size: 724 Color: 141
Size: 720 Color: 140
Size: 716 Color: 138
Size: 712 Color: 137
Size: 574 Color: 114
Size: 574 Color: 113
Size: 570 Color: 111
Size: 566 Color: 110
Size: 562 Color: 109
Size: 544 Color: 104
Size: 544 Color: 103
Size: 536 Color: 101
Size: 528 Color: 99
Size: 496 Color: 93
Size: 496 Color: 92

Bin 199: 8300 of cap free
Amount of items: 13
Items: 
Size: 712 Color: 136
Size: 688 Color: 135
Size: 646 Color: 129
Size: 640 Color: 128
Size: 640 Color: 127
Size: 636 Color: 126
Size: 632 Color: 125
Size: 624 Color: 124
Size: 616 Color: 123
Size: 592 Color: 120
Size: 592 Color: 119
Size: 584 Color: 117
Size: 578 Color: 115

Total size: 3263040
Total free space: 16480

