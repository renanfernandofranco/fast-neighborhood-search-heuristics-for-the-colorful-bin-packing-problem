Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 12
Size: 352 Color: 5
Size: 256 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 17
Size: 266 Color: 8
Size: 253 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 257 Color: 1
Size: 251 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 10
Size: 278 Color: 16
Size: 252 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 7
Size: 256 Color: 11
Size: 251 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 280 Color: 18
Size: 331 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 3
Size: 253 Color: 1
Size: 252 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9
Size: 283 Color: 16
Size: 270 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 8
Size: 352 Color: 18
Size: 267 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 11
Size: 255 Color: 4
Size: 253 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 284 Color: 0
Size: 266 Color: 9
Size: 450 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 369 Color: 7
Size: 253 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 12
Size: 336 Color: 5
Size: 268 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 12
Size: 323 Color: 17
Size: 268 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 9
Size: 307 Color: 14
Size: 295 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 12
Size: 331 Color: 0
Size: 284 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 17
Size: 370 Color: 10
Size: 258 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 327 Color: 15
Size: 278 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 18
Size: 313 Color: 13
Size: 288 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 5
Size: 313 Color: 0
Size: 296 Color: 15

Total size: 20000
Total free space: 0

