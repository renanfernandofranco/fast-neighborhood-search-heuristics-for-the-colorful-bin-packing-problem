Capicity Bin: 15744
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 7876 Color: 10
Size: 1382 Color: 3
Size: 1358 Color: 3
Size: 1304 Color: 0
Size: 1120 Color: 17
Size: 1120 Color: 16
Size: 1104 Color: 6
Size: 480 Color: 10

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 7881 Color: 13
Size: 2883 Color: 0
Size: 2768 Color: 9
Size: 1572 Color: 14
Size: 640 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7900 Color: 6
Size: 6540 Color: 9
Size: 1304 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8982 Color: 17
Size: 5638 Color: 12
Size: 1124 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8988 Color: 17
Size: 5636 Color: 2
Size: 1120 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 4
Size: 5672 Color: 11
Size: 976 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9846 Color: 2
Size: 4918 Color: 1
Size: 980 Color: 16

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 9856 Color: 5
Size: 5888 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 3
Size: 5368 Color: 10
Size: 508 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10604 Color: 5
Size: 4284 Color: 1
Size: 856 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10856 Color: 6
Size: 3688 Color: 2
Size: 1200 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11194 Color: 3
Size: 4282 Color: 4
Size: 268 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11320 Color: 10
Size: 4048 Color: 14
Size: 376 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 10
Size: 3632 Color: 4
Size: 332 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11815 Color: 2
Size: 1978 Color: 2
Size: 1951 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11888 Color: 19
Size: 3272 Color: 15
Size: 584 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 3504 Color: 9
Size: 328 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 10
Size: 3208 Color: 15
Size: 560 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 15
Size: 3122 Color: 4
Size: 620 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12212 Color: 11
Size: 3096 Color: 10
Size: 436 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12224 Color: 5
Size: 3120 Color: 11
Size: 400 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 4
Size: 2288 Color: 0
Size: 1132 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 6
Size: 2772 Color: 3
Size: 540 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12500 Color: 7
Size: 2188 Color: 0
Size: 1056 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 5
Size: 2536 Color: 18
Size: 576 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12662 Color: 1
Size: 2554 Color: 17
Size: 528 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 6
Size: 2652 Color: 11
Size: 380 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 8
Size: 2708 Color: 19
Size: 292 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 0
Size: 2377 Color: 19
Size: 512 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12874 Color: 14
Size: 1894 Color: 6
Size: 976 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 18
Size: 1512 Color: 6
Size: 1308 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12925 Color: 8
Size: 2163 Color: 9
Size: 656 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13008 Color: 19
Size: 1936 Color: 2
Size: 800 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13108 Color: 0
Size: 2412 Color: 3
Size: 224 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 8
Size: 1728 Color: 4
Size: 984 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 15
Size: 2040 Color: 11
Size: 656 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 0
Size: 1310 Color: 16
Size: 1310 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 16
Size: 2008 Color: 1
Size: 504 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 19
Size: 1304 Color: 14
Size: 1124 Color: 15

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 3
Size: 2022 Color: 14
Size: 400 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13401 Color: 12
Size: 1953 Color: 11
Size: 390 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 18
Size: 1310 Color: 10
Size: 960 Color: 14

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13476 Color: 15
Size: 1324 Color: 6
Size: 944 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 18
Size: 1892 Color: 4
Size: 344 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 0
Size: 1474 Color: 12
Size: 652 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13570 Color: 19
Size: 1558 Color: 17
Size: 616 Color: 11

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13582 Color: 16
Size: 1774 Color: 8
Size: 388 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 17
Size: 1760 Color: 5
Size: 296 Color: 16

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 16
Size: 1548 Color: 14
Size: 496 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 0
Size: 1620 Color: 2
Size: 352 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13730 Color: 12
Size: 1438 Color: 8
Size: 576 Color: 13

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 11
Size: 1448 Color: 6
Size: 520 Color: 10

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 9
Size: 1336 Color: 0
Size: 604 Color: 12

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 1
Size: 1308 Color: 14
Size: 576 Color: 12

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13872 Color: 4
Size: 1520 Color: 10
Size: 352 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 6
Size: 1368 Color: 10
Size: 432 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 0
Size: 1132 Color: 9
Size: 624 Color: 15

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 1
Size: 864 Color: 4
Size: 858 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 18
Size: 1424 Color: 17
Size: 256 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 10
Size: 1152 Color: 6
Size: 474 Color: 17

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 2
Size: 1304 Color: 18
Size: 320 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14128 Color: 6
Size: 1456 Color: 0
Size: 160 Color: 15

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 9829 Color: 10
Size: 5630 Color: 13
Size: 284 Color: 9

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10576 Color: 14
Size: 4911 Color: 12
Size: 256 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 17
Size: 4295 Color: 15
Size: 536 Color: 7

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11211 Color: 15
Size: 4276 Color: 18
Size: 256 Color: 10

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11311 Color: 14
Size: 4088 Color: 12
Size: 344 Color: 17

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11694 Color: 16
Size: 3681 Color: 2
Size: 368 Color: 13

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 14
Size: 3275 Color: 0
Size: 736 Color: 13

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 11805 Color: 14
Size: 3186 Color: 9
Size: 752 Color: 15

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 11831 Color: 14
Size: 3608 Color: 9
Size: 304 Color: 16

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12172 Color: 7
Size: 3107 Color: 17
Size: 464 Color: 0

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12235 Color: 9
Size: 3076 Color: 6
Size: 432 Color: 6

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 6
Size: 2925 Color: 6
Size: 448 Color: 19

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12613 Color: 13
Size: 2570 Color: 7
Size: 560 Color: 9

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 12893 Color: 3
Size: 2354 Color: 15
Size: 496 Color: 16

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 5
Size: 2025 Color: 8
Size: 568 Color: 9

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13405 Color: 3
Size: 1922 Color: 10
Size: 416 Color: 0

Bin 79: 2 of cap free
Amount of items: 11
Items: 
Size: 7874 Color: 11
Size: 982 Color: 9
Size: 980 Color: 4
Size: 976 Color: 2
Size: 960 Color: 7
Size: 928 Color: 12
Size: 860 Color: 14
Size: 738 Color: 13
Size: 624 Color: 1
Size: 468 Color: 14
Size: 352 Color: 8

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 8656 Color: 5
Size: 6542 Color: 14
Size: 544 Color: 8

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 1
Size: 5662 Color: 6
Size: 760 Color: 14

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 10546 Color: 12
Size: 4892 Color: 10
Size: 304 Color: 8

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11201 Color: 5
Size: 3261 Color: 19
Size: 1280 Color: 13

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11220 Color: 1
Size: 2482 Color: 17
Size: 2040 Color: 12

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 11432 Color: 19
Size: 4310 Color: 8

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 18
Size: 3616 Color: 16
Size: 464 Color: 8

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 15
Size: 3094 Color: 13
Size: 272 Color: 18

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 12617 Color: 3
Size: 2645 Color: 8
Size: 480 Color: 18

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 12690 Color: 7
Size: 2800 Color: 5
Size: 252 Color: 19

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 0
Size: 1516 Color: 18
Size: 1280 Color: 15

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 3
Size: 1468 Color: 9
Size: 672 Color: 0

Bin 92: 2 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 5
Size: 2022 Color: 11

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 13978 Color: 9
Size: 1764 Color: 17

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 5
Size: 1752 Color: 13

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 18
Size: 1578 Color: 5
Size: 12 Color: 19

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 9296 Color: 2
Size: 5645 Color: 12
Size: 800 Color: 15

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 9851 Color: 12
Size: 5618 Color: 9
Size: 272 Color: 3

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 10587 Color: 7
Size: 4890 Color: 0
Size: 264 Color: 14

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 11315 Color: 0
Size: 3802 Color: 14
Size: 624 Color: 6

Bin 100: 3 of cap free
Amount of items: 2
Items: 
Size: 11954 Color: 8
Size: 3787 Color: 18

Bin 101: 4 of cap free
Amount of items: 5
Items: 
Size: 7882 Color: 5
Size: 5622 Color: 19
Size: 1308 Color: 0
Size: 528 Color: 11
Size: 400 Color: 7

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 7898 Color: 17
Size: 6546 Color: 8
Size: 1296 Color: 19

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 11922 Color: 1
Size: 3542 Color: 13
Size: 276 Color: 8

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 5
Size: 3308 Color: 13
Size: 416 Color: 5

Bin 105: 4 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 9
Size: 3176 Color: 3

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 13668 Color: 1
Size: 2072 Color: 3

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 1
Size: 1732 Color: 12

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 14084 Color: 15
Size: 1632 Color: 8
Size: 24 Color: 19

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 14156 Color: 9
Size: 1584 Color: 19

Bin 110: 5 of cap free
Amount of items: 7
Items: 
Size: 7877 Color: 2
Size: 1688 Color: 7
Size: 1682 Color: 5
Size: 1388 Color: 14
Size: 1384 Color: 16
Size: 1056 Color: 6
Size: 664 Color: 9

Bin 111: 5 of cap free
Amount of items: 4
Items: 
Size: 7884 Color: 2
Size: 5667 Color: 4
Size: 1708 Color: 16
Size: 480 Color: 2

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 10574 Color: 18
Size: 4909 Color: 5
Size: 256 Color: 19

Bin 113: 5 of cap free
Amount of items: 3
Items: 
Size: 10606 Color: 5
Size: 4285 Color: 11
Size: 848 Color: 2

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 13149 Color: 6
Size: 2590 Color: 9

Bin 115: 6 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 17
Size: 2434 Color: 15

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 13616 Color: 11
Size: 2090 Color: 12
Size: 32 Color: 10

Bin 117: 6 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 4
Size: 2028 Color: 2

Bin 118: 6 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 11
Size: 1922 Color: 5

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 2
Size: 1802 Color: 11

Bin 120: 7 of cap free
Amount of items: 13
Items: 
Size: 7873 Color: 4
Size: 858 Color: 19
Size: 856 Color: 11
Size: 756 Color: 10
Size: 752 Color: 0
Size: 738 Color: 5
Size: 736 Color: 12
Size: 736 Color: 6
Size: 704 Color: 19
Size: 704 Color: 10
Size: 424 Color: 1
Size: 312 Color: 18
Size: 288 Color: 7

Bin 121: 7 of cap free
Amount of items: 3
Items: 
Size: 7888 Color: 17
Size: 6553 Color: 11
Size: 1296 Color: 12

Bin 122: 7 of cap free
Amount of items: 2
Items: 
Size: 13130 Color: 6
Size: 2607 Color: 13

Bin 123: 8 of cap free
Amount of items: 2
Items: 
Size: 12244 Color: 9
Size: 3492 Color: 18

Bin 124: 8 of cap free
Amount of items: 2
Items: 
Size: 12922 Color: 11
Size: 2814 Color: 6

Bin 125: 8 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 14
Size: 2362 Color: 13

Bin 126: 8 of cap free
Amount of items: 2
Items: 
Size: 13824 Color: 11
Size: 1912 Color: 2

Bin 127: 8 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 19
Size: 1648 Color: 14

Bin 128: 9 of cap free
Amount of items: 3
Items: 
Size: 8990 Color: 6
Size: 4336 Color: 0
Size: 2409 Color: 13

Bin 129: 9 of cap free
Amount of items: 2
Items: 
Size: 12040 Color: 10
Size: 3695 Color: 18

Bin 130: 10 of cap free
Amount of items: 3
Items: 
Size: 8922 Color: 16
Size: 6524 Color: 9
Size: 288 Color: 19

Bin 131: 10 of cap free
Amount of items: 3
Items: 
Size: 10603 Color: 13
Size: 4299 Color: 3
Size: 832 Color: 1

Bin 132: 10 of cap free
Amount of items: 2
Items: 
Size: 11970 Color: 2
Size: 3764 Color: 5

Bin 133: 10 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 1
Size: 2394 Color: 8

Bin 134: 10 of cap free
Amount of items: 2
Items: 
Size: 13400 Color: 7
Size: 2334 Color: 18

Bin 135: 10 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 9
Size: 2182 Color: 10
Size: 128 Color: 9

Bin 136: 12 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 3
Size: 5344 Color: 1
Size: 512 Color: 14

Bin 137: 12 of cap free
Amount of items: 3
Items: 
Size: 9976 Color: 17
Size: 4900 Color: 8
Size: 856 Color: 0

Bin 138: 12 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 17
Size: 4664 Color: 5
Size: 448 Color: 0

Bin 139: 12 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 7
Size: 3144 Color: 2
Size: 1360 Color: 10

Bin 140: 12 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 18
Size: 2096 Color: 3

Bin 141: 14 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 0
Size: 3348 Color: 9

Bin 142: 14 of cap free
Amount of items: 2
Items: 
Size: 14000 Color: 1
Size: 1730 Color: 5

Bin 143: 14 of cap free
Amount of items: 2
Items: 
Size: 14086 Color: 4
Size: 1644 Color: 10

Bin 144: 15 of cap free
Amount of items: 3
Items: 
Size: 11819 Color: 8
Size: 3542 Color: 13
Size: 368 Color: 2

Bin 145: 16 of cap free
Amount of items: 3
Items: 
Size: 8008 Color: 12
Size: 6888 Color: 11
Size: 832 Color: 13

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 13315 Color: 18
Size: 2412 Color: 1

Bin 147: 18 of cap free
Amount of items: 3
Items: 
Size: 11327 Color: 11
Size: 3271 Color: 17
Size: 1128 Color: 0

Bin 148: 18 of cap free
Amount of items: 3
Items: 
Size: 12682 Color: 9
Size: 2980 Color: 18
Size: 64 Color: 0

Bin 149: 18 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 9
Size: 2488 Color: 7

Bin 150: 18 of cap free
Amount of items: 3
Items: 
Size: 13886 Color: 12
Size: 1776 Color: 6
Size: 64 Color: 8

Bin 151: 19 of cap free
Amount of items: 2
Items: 
Size: 12034 Color: 8
Size: 3691 Color: 10

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 12
Size: 2948 Color: 4

Bin 153: 20 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 16
Size: 2576 Color: 14

Bin 154: 20 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 10
Size: 2164 Color: 1

Bin 155: 20 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 1
Size: 1832 Color: 9

Bin 156: 21 of cap free
Amount of items: 2
Items: 
Size: 13123 Color: 10
Size: 2600 Color: 1

Bin 157: 22 of cap free
Amount of items: 3
Items: 
Size: 7890 Color: 12
Size: 5936 Color: 13
Size: 1896 Color: 0

Bin 158: 22 of cap free
Amount of items: 2
Items: 
Size: 12914 Color: 6
Size: 2808 Color: 4

Bin 159: 22 of cap free
Amount of items: 2
Items: 
Size: 13854 Color: 13
Size: 1868 Color: 1

Bin 160: 26 of cap free
Amount of items: 3
Items: 
Size: 11182 Color: 2
Size: 2351 Color: 16
Size: 2185 Color: 0

Bin 161: 26 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 3
Size: 1786 Color: 15

Bin 162: 27 of cap free
Amount of items: 3
Items: 
Size: 8945 Color: 2
Size: 6516 Color: 0
Size: 256 Color: 1

Bin 163: 27 of cap free
Amount of items: 2
Items: 
Size: 12571 Color: 0
Size: 3146 Color: 6

Bin 164: 28 of cap free
Amount of items: 2
Items: 
Size: 11944 Color: 12
Size: 3772 Color: 19

Bin 165: 28 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 19
Size: 3296 Color: 6

Bin 166: 30 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 12
Size: 4334 Color: 3
Size: 196 Color: 7

Bin 167: 30 of cap free
Amount of items: 2
Items: 
Size: 11748 Color: 13
Size: 3966 Color: 1

Bin 168: 32 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 5
Size: 2248 Color: 1

Bin 169: 36 of cap free
Amount of items: 2
Items: 
Size: 12906 Color: 16
Size: 2802 Color: 4

Bin 170: 38 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 17
Size: 2264 Color: 5

Bin 171: 40 of cap free
Amount of items: 2
Items: 
Size: 10896 Color: 9
Size: 4808 Color: 10

Bin 172: 40 of cap free
Amount of items: 2
Items: 
Size: 12852 Color: 1
Size: 2852 Color: 19

Bin 173: 40 of cap free
Amount of items: 2
Items: 
Size: 13272 Color: 6
Size: 2432 Color: 3

Bin 174: 46 of cap free
Amount of items: 2
Items: 
Size: 11498 Color: 2
Size: 4200 Color: 10

Bin 175: 48 of cap free
Amount of items: 3
Items: 
Size: 7932 Color: 18
Size: 6456 Color: 17
Size: 1308 Color: 8

Bin 176: 48 of cap free
Amount of items: 2
Items: 
Size: 10152 Color: 19
Size: 5544 Color: 3

Bin 177: 48 of cap free
Amount of items: 2
Items: 
Size: 12752 Color: 0
Size: 2944 Color: 15

Bin 178: 52 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 19
Size: 2356 Color: 11

Bin 179: 61 of cap free
Amount of items: 2
Items: 
Size: 12400 Color: 18
Size: 3283 Color: 11

Bin 180: 70 of cap free
Amount of items: 2
Items: 
Size: 9006 Color: 5
Size: 6668 Color: 7

Bin 181: 81 of cap free
Amount of items: 2
Items: 
Size: 12285 Color: 16
Size: 3378 Color: 11

Bin 182: 87 of cap free
Amount of items: 2
Items: 
Size: 11472 Color: 6
Size: 4185 Color: 4

Bin 183: 88 of cap free
Amount of items: 2
Items: 
Size: 10712 Color: 11
Size: 4944 Color: 8

Bin 184: 88 of cap free
Amount of items: 2
Items: 
Size: 11832 Color: 13
Size: 3824 Color: 3

Bin 185: 138 of cap free
Amount of items: 3
Items: 
Size: 7916 Color: 2
Size: 6554 Color: 0
Size: 1136 Color: 15

Bin 186: 143 of cap free
Amount of items: 3
Items: 
Size: 9855 Color: 7
Size: 3242 Color: 2
Size: 2504 Color: 0

Bin 187: 152 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 5
Size: 6556 Color: 6
Size: 1124 Color: 4

Bin 188: 162 of cap free
Amount of items: 3
Items: 
Size: 9878 Color: 10
Size: 5392 Color: 13
Size: 312 Color: 9

Bin 189: 170 of cap free
Amount of items: 2
Items: 
Size: 8998 Color: 11
Size: 6576 Color: 19

Bin 190: 180 of cap free
Amount of items: 2
Items: 
Size: 8996 Color: 11
Size: 6568 Color: 9

Bin 191: 188 of cap free
Amount of items: 3
Items: 
Size: 7885 Color: 5
Size: 6551 Color: 6
Size: 1120 Color: 3

Bin 192: 200 of cap free
Amount of items: 31
Items: 
Size: 654 Color: 16
Size: 654 Color: 9
Size: 640 Color: 0
Size: 628 Color: 7
Size: 608 Color: 14
Size: 608 Color: 10
Size: 592 Color: 18
Size: 584 Color: 9
Size: 584 Color: 5
Size: 580 Color: 9
Size: 576 Color: 4
Size: 560 Color: 8
Size: 552 Color: 3
Size: 544 Color: 5
Size: 476 Color: 0
Size: 468 Color: 16
Size: 468 Color: 6
Size: 464 Color: 8
Size: 448 Color: 10
Size: 448 Color: 2
Size: 432 Color: 13
Size: 432 Color: 13
Size: 416 Color: 14
Size: 416 Color: 11
Size: 404 Color: 13
Size: 400 Color: 19
Size: 400 Color: 7
Size: 400 Color: 1
Size: 392 Color: 2
Size: 360 Color: 1
Size: 356 Color: 6

Bin 193: 211 of cap free
Amount of items: 2
Items: 
Size: 8971 Color: 13
Size: 6562 Color: 1

Bin 194: 218 of cap free
Amount of items: 2
Items: 
Size: 9840 Color: 0
Size: 5686 Color: 16

Bin 195: 222 of cap free
Amount of items: 2
Items: 
Size: 10591 Color: 15
Size: 4931 Color: 4

Bin 196: 231 of cap free
Amount of items: 2
Items: 
Size: 8952 Color: 5
Size: 6561 Color: 6

Bin 197: 232 of cap free
Amount of items: 3
Items: 
Size: 7880 Color: 7
Size: 5628 Color: 9
Size: 2004 Color: 15

Bin 198: 237 of cap free
Amount of items: 2
Items: 
Size: 8950 Color: 13
Size: 6557 Color: 10

Bin 199: 11192 of cap free
Amount of items: 14
Items: 
Size: 384 Color: 4
Size: 376 Color: 11
Size: 356 Color: 15
Size: 352 Color: 5
Size: 348 Color: 0
Size: 336 Color: 6
Size: 336 Color: 1
Size: 320 Color: 17
Size: 304 Color: 7
Size: 288 Color: 16
Size: 288 Color: 14
Size: 288 Color: 13
Size: 288 Color: 12
Size: 288 Color: 9

Total size: 3117312
Total free space: 15744

