Capicity Bin: 1860
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 748 Color: 133
Size: 622 Color: 124
Size: 216 Color: 82
Size: 208 Color: 78
Size: 66 Color: 41

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1284 Color: 159
Size: 432 Color: 112
Size: 76 Color: 44
Size: 68 Color: 43

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1077 Color: 144
Size: 653 Color: 130
Size: 50 Color: 28
Size: 40 Color: 20
Size: 40 Color: 17

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1543 Color: 180
Size: 291 Color: 98
Size: 22 Color: 3
Size: 4 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1461 Color: 171
Size: 275 Color: 94
Size: 120 Color: 57
Size: 4 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1168 Color: 150
Size: 628 Color: 125
Size: 64 Color: 38

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 198
Size: 171 Color: 71
Size: 34 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1398 Color: 166
Size: 288 Color: 97
Size: 174 Color: 72

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 939 Color: 141
Size: 769 Color: 134
Size: 152 Color: 65

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 169
Size: 341 Color: 104
Size: 66 Color: 40

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 173
Size: 322 Color: 100
Size: 60 Color: 36

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1101 Color: 147
Size: 633 Color: 127
Size: 126 Color: 59

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 158
Size: 502 Color: 116
Size: 96 Color: 50

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1097 Color: 146
Size: 637 Color: 128
Size: 126 Color: 61

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 154
Size: 537 Color: 119
Size: 106 Color: 52

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 187
Size: 230 Color: 85
Size: 44 Color: 22

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 181
Size: 242 Color: 87
Size: 68 Color: 42

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1322 Color: 162
Size: 474 Color: 115
Size: 64 Color: 37

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 174
Size: 294 Color: 99
Size: 56 Color: 34

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 189
Size: 218 Color: 83
Size: 40 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 160
Size: 473 Color: 114
Size: 76 Color: 45

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 178
Size: 271 Color: 91
Size: 54 Color: 31

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 192
Size: 213 Color: 81
Size: 38 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 176
Size: 278 Color: 95
Size: 52 Color: 29

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1473 Color: 172
Size: 333 Color: 102
Size: 54 Color: 30

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 168
Size: 386 Color: 107
Size: 36 Color: 12

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1604 Color: 190
Size: 208 Color: 79
Size: 48 Color: 26

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1105 Color: 148
Size: 631 Color: 126
Size: 124 Color: 58

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 170
Size: 337 Color: 103
Size: 66 Color: 39

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 934 Color: 139
Size: 774 Color: 136
Size: 152 Color: 66

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 143
Size: 657 Color: 131
Size: 130 Color: 63

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 157
Size: 566 Color: 123
Size: 48 Color: 25

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 185
Size: 271 Color: 92
Size: 16 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 193
Size: 207 Color: 77
Size: 40 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 175
Size: 323 Color: 101
Size: 26 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1600 Color: 188
Size: 220 Color: 84
Size: 40 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1211 Color: 152
Size: 541 Color: 121
Size: 108 Color: 55

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 183
Size: 245 Color: 88
Size: 48 Color: 24

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 197
Size: 158 Color: 70
Size: 48 Color: 27

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 196
Size: 195 Color: 76
Size: 28 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1186 Color: 151
Size: 562 Color: 122
Size: 112 Color: 56

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 149
Size: 388 Color: 108
Size: 354 Color: 105

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 194
Size: 190 Color: 75
Size: 36 Color: 13

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 156
Size: 523 Color: 117
Size: 104 Color: 51

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 184
Size: 262 Color: 89
Size: 28 Color: 7

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1093 Color: 145
Size: 641 Color: 129
Size: 126 Color: 60

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 167
Size: 369 Color: 106
Size: 56 Color: 32

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 177
Size: 271 Color: 93
Size: 58 Color: 35

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 200
Size: 157 Color: 69
Size: 30 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 155
Size: 533 Color: 118
Size: 106 Color: 53

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1213 Color: 153
Size: 541 Color: 120
Size: 106 Color: 54

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1371 Color: 164
Size: 409 Color: 110
Size: 80 Color: 47

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 195
Size: 189 Color: 74
Size: 36 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 931 Color: 138
Size: 775 Color: 137
Size: 154 Color: 67

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 201
Size: 130 Color: 62
Size: 56 Color: 33

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 182
Size: 265 Color: 90
Size: 32 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 165
Size: 391 Color: 109
Size: 76 Color: 46

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 191
Size: 213 Color: 80
Size: 42 Color: 21

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 163
Size: 426 Color: 111
Size: 84 Color: 48

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 935 Color: 140
Size: 771 Color: 135
Size: 154 Color: 68

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 199
Size: 178 Color: 73
Size: 24 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 179
Size: 281 Color: 96
Size: 40 Color: 18

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 186
Size: 237 Color: 86
Size: 46 Color: 23

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 142
Size: 690 Color: 132
Size: 136 Color: 64

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1321 Color: 161
Size: 451 Color: 113
Size: 88 Color: 49

Total size: 120900
Total free space: 0

