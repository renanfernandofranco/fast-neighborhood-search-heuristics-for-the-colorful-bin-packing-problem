Capicity Bin: 1000001
Lower Bound: 231

Bins used: 235
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 688823 Color: 0
Size: 311178 Color: 1

Bin 2: 1 of cap free
Amount of items: 3
Items: 
Size: 785494 Color: 1
Size: 110877 Color: 0
Size: 103629 Color: 1

Bin 3: 1 of cap free
Amount of items: 4
Items: 
Size: 464939 Color: 1
Size: 209809 Color: 0
Size: 197144 Color: 1
Size: 128108 Color: 0

Bin 4: 2 of cap free
Amount of items: 3
Items: 
Size: 704683 Color: 0
Size: 193002 Color: 1
Size: 102314 Color: 1

Bin 5: 4 of cap free
Amount of items: 3
Items: 
Size: 478504 Color: 1
Size: 401680 Color: 1
Size: 119813 Color: 0

Bin 6: 6 of cap free
Amount of items: 3
Items: 
Size: 702411 Color: 0
Size: 158829 Color: 0
Size: 138755 Color: 1

Bin 7: 6 of cap free
Amount of items: 3
Items: 
Size: 700709 Color: 0
Size: 187138 Color: 0
Size: 112148 Color: 1

Bin 8: 8 of cap free
Amount of items: 3
Items: 
Size: 489073 Color: 1
Size: 358592 Color: 0
Size: 152328 Color: 1

Bin 9: 16 of cap free
Amount of items: 3
Items: 
Size: 544876 Color: 1
Size: 323162 Color: 0
Size: 131947 Color: 1

Bin 10: 19 of cap free
Amount of items: 2
Items: 
Size: 599839 Color: 1
Size: 400143 Color: 0

Bin 11: 21 of cap free
Amount of items: 3
Items: 
Size: 536284 Color: 1
Size: 318454 Color: 0
Size: 145242 Color: 1

Bin 12: 24 of cap free
Amount of items: 2
Items: 
Size: 687357 Color: 1
Size: 312620 Color: 0

Bin 13: 25 of cap free
Amount of items: 2
Items: 
Size: 638553 Color: 1
Size: 361423 Color: 0

Bin 14: 25 of cap free
Amount of items: 3
Items: 
Size: 453269 Color: 1
Size: 428663 Color: 1
Size: 118044 Color: 0

Bin 15: 28 of cap free
Amount of items: 2
Items: 
Size: 692544 Color: 1
Size: 307429 Color: 0

Bin 16: 35 of cap free
Amount of items: 3
Items: 
Size: 487842 Color: 1
Size: 401905 Color: 1
Size: 110219 Color: 0

Bin 17: 38 of cap free
Amount of items: 2
Items: 
Size: 634853 Color: 0
Size: 365110 Color: 1

Bin 18: 39 of cap free
Amount of items: 2
Items: 
Size: 550796 Color: 0
Size: 449166 Color: 1

Bin 19: 45 of cap free
Amount of items: 3
Items: 
Size: 489542 Color: 1
Size: 311186 Color: 0
Size: 199228 Color: 1

Bin 20: 52 of cap free
Amount of items: 3
Items: 
Size: 598016 Color: 0
Size: 229843 Color: 1
Size: 172090 Color: 0

Bin 21: 54 of cap free
Amount of items: 3
Items: 
Size: 547661 Color: 1
Size: 273406 Color: 1
Size: 178880 Color: 0

Bin 22: 61 of cap free
Amount of items: 3
Items: 
Size: 715178 Color: 1
Size: 161595 Color: 1
Size: 123167 Color: 0

Bin 23: 62 of cap free
Amount of items: 3
Items: 
Size: 603510 Color: 1
Size: 261614 Color: 0
Size: 134815 Color: 1

Bin 24: 69 of cap free
Amount of items: 3
Items: 
Size: 721171 Color: 0
Size: 160214 Color: 1
Size: 118547 Color: 0

Bin 25: 71 of cap free
Amount of items: 3
Items: 
Size: 610920 Color: 0
Size: 210392 Color: 1
Size: 178618 Color: 0

Bin 26: 71 of cap free
Amount of items: 2
Items: 
Size: 727070 Color: 1
Size: 272860 Color: 0

Bin 27: 71 of cap free
Amount of items: 2
Items: 
Size: 640028 Color: 0
Size: 359902 Color: 1

Bin 28: 74 of cap free
Amount of items: 2
Items: 
Size: 666161 Color: 0
Size: 333766 Color: 1

Bin 29: 81 of cap free
Amount of items: 3
Items: 
Size: 606014 Color: 1
Size: 285012 Color: 1
Size: 108894 Color: 0

Bin 30: 83 of cap free
Amount of items: 2
Items: 
Size: 516103 Color: 0
Size: 483815 Color: 1

Bin 31: 100 of cap free
Amount of items: 2
Items: 
Size: 679671 Color: 1
Size: 320230 Color: 0

Bin 32: 106 of cap free
Amount of items: 2
Items: 
Size: 792999 Color: 0
Size: 206896 Color: 1

Bin 33: 109 of cap free
Amount of items: 2
Items: 
Size: 611209 Color: 0
Size: 388683 Color: 1

Bin 34: 109 of cap free
Amount of items: 2
Items: 
Size: 617996 Color: 0
Size: 381896 Color: 1

Bin 35: 128 of cap free
Amount of items: 2
Items: 
Size: 645092 Color: 0
Size: 354781 Color: 1

Bin 36: 130 of cap free
Amount of items: 3
Items: 
Size: 757313 Color: 0
Size: 139251 Color: 1
Size: 103307 Color: 0

Bin 37: 143 of cap free
Amount of items: 2
Items: 
Size: 522688 Color: 1
Size: 477170 Color: 0

Bin 38: 148 of cap free
Amount of items: 3
Items: 
Size: 763510 Color: 1
Size: 126887 Color: 0
Size: 109456 Color: 1

Bin 39: 156 of cap free
Amount of items: 2
Items: 
Size: 572974 Color: 0
Size: 426871 Color: 1

Bin 40: 161 of cap free
Amount of items: 3
Items: 
Size: 641230 Color: 1
Size: 197411 Color: 1
Size: 161199 Color: 0

Bin 41: 163 of cap free
Amount of items: 2
Items: 
Size: 580935 Color: 1
Size: 418903 Color: 0

Bin 42: 171 of cap free
Amount of items: 3
Items: 
Size: 593381 Color: 0
Size: 257836 Color: 1
Size: 148613 Color: 1

Bin 43: 177 of cap free
Amount of items: 2
Items: 
Size: 660155 Color: 0
Size: 339669 Color: 1

Bin 44: 179 of cap free
Amount of items: 2
Items: 
Size: 636694 Color: 1
Size: 363128 Color: 0

Bin 45: 179 of cap free
Amount of items: 3
Items: 
Size: 690802 Color: 1
Size: 173589 Color: 0
Size: 135431 Color: 1

Bin 46: 195 of cap free
Amount of items: 2
Items: 
Size: 675760 Color: 1
Size: 324046 Color: 0

Bin 47: 198 of cap free
Amount of items: 2
Items: 
Size: 785554 Color: 1
Size: 214249 Color: 0

Bin 48: 215 of cap free
Amount of items: 2
Items: 
Size: 577463 Color: 0
Size: 422323 Color: 1

Bin 49: 215 of cap free
Amount of items: 2
Items: 
Size: 657104 Color: 1
Size: 342682 Color: 0

Bin 50: 235 of cap free
Amount of items: 2
Items: 
Size: 614821 Color: 1
Size: 384945 Color: 0

Bin 51: 237 of cap free
Amount of items: 3
Items: 
Size: 699349 Color: 1
Size: 158264 Color: 0
Size: 142151 Color: 1

Bin 52: 240 of cap free
Amount of items: 2
Items: 
Size: 670078 Color: 0
Size: 329683 Color: 1

Bin 53: 245 of cap free
Amount of items: 2
Items: 
Size: 567688 Color: 0
Size: 432068 Color: 1

Bin 54: 249 of cap free
Amount of items: 3
Items: 
Size: 633861 Color: 1
Size: 205141 Color: 0
Size: 160750 Color: 1

Bin 55: 270 of cap free
Amount of items: 2
Items: 
Size: 578182 Color: 0
Size: 421549 Color: 1

Bin 56: 273 of cap free
Amount of items: 3
Items: 
Size: 771528 Color: 1
Size: 119569 Color: 0
Size: 108631 Color: 1

Bin 57: 296 of cap free
Amount of items: 3
Items: 
Size: 667800 Color: 1
Size: 208907 Color: 1
Size: 122998 Color: 0

Bin 58: 321 of cap free
Amount of items: 3
Items: 
Size: 490793 Color: 1
Size: 293010 Color: 0
Size: 215877 Color: 0

Bin 59: 350 of cap free
Amount of items: 2
Items: 
Size: 668016 Color: 0
Size: 331635 Color: 1

Bin 60: 366 of cap free
Amount of items: 3
Items: 
Size: 499393 Color: 1
Size: 294080 Color: 0
Size: 206162 Color: 0

Bin 61: 379 of cap free
Amount of items: 2
Items: 
Size: 524379 Color: 0
Size: 475243 Color: 1

Bin 62: 388 of cap free
Amount of items: 3
Items: 
Size: 672995 Color: 1
Size: 216742 Color: 1
Size: 109876 Color: 0

Bin 63: 388 of cap free
Amount of items: 3
Items: 
Size: 541252 Color: 1
Size: 345813 Color: 0
Size: 112548 Color: 1

Bin 64: 406 of cap free
Amount of items: 3
Items: 
Size: 483550 Color: 1
Size: 283374 Color: 1
Size: 232671 Color: 0

Bin 65: 410 of cap free
Amount of items: 2
Items: 
Size: 676910 Color: 1
Size: 322681 Color: 0

Bin 66: 443 of cap free
Amount of items: 2
Items: 
Size: 664454 Color: 1
Size: 335104 Color: 0

Bin 67: 451 of cap free
Amount of items: 3
Items: 
Size: 449867 Color: 1
Size: 427277 Color: 1
Size: 122406 Color: 0

Bin 68: 464 of cap free
Amount of items: 2
Items: 
Size: 750238 Color: 1
Size: 249299 Color: 0

Bin 69: 467 of cap free
Amount of items: 2
Items: 
Size: 517388 Color: 0
Size: 482146 Color: 1

Bin 70: 484 of cap free
Amount of items: 3
Items: 
Size: 528387 Color: 1
Size: 327400 Color: 0
Size: 143730 Color: 1

Bin 71: 486 of cap free
Amount of items: 2
Items: 
Size: 621773 Color: 0
Size: 377742 Color: 1

Bin 72: 502 of cap free
Amount of items: 2
Items: 
Size: 788610 Color: 0
Size: 210889 Color: 1

Bin 73: 529 of cap free
Amount of items: 2
Items: 
Size: 544293 Color: 1
Size: 455179 Color: 0

Bin 74: 551 of cap free
Amount of items: 2
Items: 
Size: 574655 Color: 0
Size: 424795 Color: 1

Bin 75: 553 of cap free
Amount of items: 2
Items: 
Size: 570195 Color: 0
Size: 429253 Color: 1

Bin 76: 619 of cap free
Amount of items: 2
Items: 
Size: 612527 Color: 1
Size: 386855 Color: 0

Bin 77: 657 of cap free
Amount of items: 2
Items: 
Size: 563437 Color: 1
Size: 435907 Color: 0

Bin 78: 662 of cap free
Amount of items: 2
Items: 
Size: 682152 Color: 1
Size: 317187 Color: 0

Bin 79: 708 of cap free
Amount of items: 2
Items: 
Size: 781137 Color: 0
Size: 218156 Color: 1

Bin 80: 736 of cap free
Amount of items: 2
Items: 
Size: 767525 Color: 0
Size: 231740 Color: 1

Bin 81: 777 of cap free
Amount of items: 2
Items: 
Size: 639726 Color: 1
Size: 359498 Color: 0

Bin 82: 783 of cap free
Amount of items: 2
Items: 
Size: 623672 Color: 0
Size: 375546 Color: 1

Bin 83: 785 of cap free
Amount of items: 4
Items: 
Size: 477522 Color: 1
Size: 206612 Color: 1
Size: 180718 Color: 0
Size: 134364 Color: 0

Bin 84: 834 of cap free
Amount of items: 3
Items: 
Size: 694221 Color: 0
Size: 185892 Color: 1
Size: 119054 Color: 1

Bin 85: 838 of cap free
Amount of items: 2
Items: 
Size: 581859 Color: 0
Size: 417304 Color: 1

Bin 86: 851 of cap free
Amount of items: 2
Items: 
Size: 712854 Color: 1
Size: 286296 Color: 0

Bin 87: 885 of cap free
Amount of items: 2
Items: 
Size: 756193 Color: 0
Size: 242923 Color: 1

Bin 88: 932 of cap free
Amount of items: 2
Items: 
Size: 769601 Color: 0
Size: 229468 Color: 1

Bin 89: 939 of cap free
Amount of items: 2
Items: 
Size: 538362 Color: 0
Size: 460700 Color: 1

Bin 90: 958 of cap free
Amount of items: 2
Items: 
Size: 562227 Color: 0
Size: 436816 Color: 1

Bin 91: 989 of cap free
Amount of items: 2
Items: 
Size: 745636 Color: 1
Size: 253376 Color: 0

Bin 92: 990 of cap free
Amount of items: 2
Items: 
Size: 542310 Color: 0
Size: 456701 Color: 1

Bin 93: 1004 of cap free
Amount of items: 2
Items: 
Size: 766016 Color: 0
Size: 232981 Color: 1

Bin 94: 1005 of cap free
Amount of items: 2
Items: 
Size: 671083 Color: 0
Size: 327913 Color: 1

Bin 95: 1047 of cap free
Amount of items: 2
Items: 
Size: 551298 Color: 0
Size: 447656 Color: 1

Bin 96: 1051 of cap free
Amount of items: 2
Items: 
Size: 608288 Color: 0
Size: 390662 Color: 1

Bin 97: 1087 of cap free
Amount of items: 2
Items: 
Size: 511680 Color: 1
Size: 487234 Color: 0

Bin 98: 1102 of cap free
Amount of items: 2
Items: 
Size: 537533 Color: 1
Size: 461366 Color: 0

Bin 99: 1127 of cap free
Amount of items: 2
Items: 
Size: 734660 Color: 0
Size: 264214 Color: 1

Bin 100: 1186 of cap free
Amount of items: 2
Items: 
Size: 563348 Color: 1
Size: 435467 Color: 0

Bin 101: 1190 of cap free
Amount of items: 2
Items: 
Size: 613332 Color: 1
Size: 385479 Color: 0

Bin 102: 1200 of cap free
Amount of items: 2
Items: 
Size: 648067 Color: 0
Size: 350734 Color: 1

Bin 103: 1250 of cap free
Amount of items: 2
Items: 
Size: 597372 Color: 0
Size: 401379 Color: 1

Bin 104: 1250 of cap free
Amount of items: 2
Items: 
Size: 636678 Color: 0
Size: 362073 Color: 1

Bin 105: 1278 of cap free
Amount of items: 2
Items: 
Size: 695052 Color: 1
Size: 303671 Color: 0

Bin 106: 1297 of cap free
Amount of items: 2
Items: 
Size: 690404 Color: 0
Size: 308300 Color: 1

Bin 107: 1340 of cap free
Amount of items: 2
Items: 
Size: 591993 Color: 0
Size: 406668 Color: 1

Bin 108: 1352 of cap free
Amount of items: 2
Items: 
Size: 735681 Color: 1
Size: 262968 Color: 0

Bin 109: 1359 of cap free
Amount of items: 2
Items: 
Size: 557690 Color: 1
Size: 440952 Color: 0

Bin 110: 1384 of cap free
Amount of items: 3
Items: 
Size: 729000 Color: 1
Size: 135261 Color: 1
Size: 134356 Color: 0

Bin 111: 1397 of cap free
Amount of items: 2
Items: 
Size: 796767 Color: 1
Size: 201837 Color: 0

Bin 112: 1423 of cap free
Amount of items: 2
Items: 
Size: 757959 Color: 1
Size: 240619 Color: 0

Bin 113: 1460 of cap free
Amount of items: 3
Items: 
Size: 702305 Color: 0
Size: 153230 Color: 0
Size: 143006 Color: 1

Bin 114: 1492 of cap free
Amount of items: 2
Items: 
Size: 585757 Color: 0
Size: 412752 Color: 1

Bin 115: 1515 of cap free
Amount of items: 2
Items: 
Size: 711909 Color: 0
Size: 286577 Color: 1

Bin 116: 1590 of cap free
Amount of items: 2
Items: 
Size: 749195 Color: 0
Size: 249216 Color: 1

Bin 117: 1598 of cap free
Amount of items: 2
Items: 
Size: 578325 Color: 1
Size: 420078 Color: 0

Bin 118: 1680 of cap free
Amount of items: 2
Items: 
Size: 518591 Color: 0
Size: 479730 Color: 1

Bin 119: 1721 of cap free
Amount of items: 2
Items: 
Size: 619454 Color: 1
Size: 378826 Color: 0

Bin 120: 1725 of cap free
Amount of items: 2
Items: 
Size: 541773 Color: 1
Size: 456503 Color: 0

Bin 121: 1752 of cap free
Amount of items: 2
Items: 
Size: 661560 Color: 1
Size: 336689 Color: 0

Bin 122: 1810 of cap free
Amount of items: 2
Items: 
Size: 528470 Color: 0
Size: 469721 Color: 1

Bin 123: 1912 of cap free
Amount of items: 2
Items: 
Size: 644409 Color: 0
Size: 353680 Color: 1

Bin 124: 1957 of cap free
Amount of items: 2
Items: 
Size: 523279 Color: 0
Size: 474765 Color: 1

Bin 125: 2057 of cap free
Amount of items: 3
Items: 
Size: 636326 Color: 0
Size: 234957 Color: 1
Size: 126661 Color: 1

Bin 126: 2106 of cap free
Amount of items: 2
Items: 
Size: 592699 Color: 1
Size: 405196 Color: 0

Bin 127: 2107 of cap free
Amount of items: 2
Items: 
Size: 724062 Color: 1
Size: 273832 Color: 0

Bin 128: 2109 of cap free
Amount of items: 2
Items: 
Size: 652924 Color: 0
Size: 344968 Color: 1

Bin 129: 2182 of cap free
Amount of items: 2
Items: 
Size: 686684 Color: 1
Size: 311135 Color: 0

Bin 130: 2233 of cap free
Amount of items: 2
Items: 
Size: 599208 Color: 0
Size: 398560 Color: 1

Bin 131: 2277 of cap free
Amount of items: 2
Items: 
Size: 618994 Color: 1
Size: 378730 Color: 0

Bin 132: 2362 of cap free
Amount of items: 2
Items: 
Size: 629311 Color: 0
Size: 368328 Color: 1

Bin 133: 2397 of cap free
Amount of items: 2
Items: 
Size: 711229 Color: 0
Size: 286375 Color: 1

Bin 134: 2592 of cap free
Amount of items: 2
Items: 
Size: 763034 Color: 1
Size: 234375 Color: 0

Bin 135: 2684 of cap free
Amount of items: 2
Items: 
Size: 632347 Color: 0
Size: 364970 Color: 1

Bin 136: 3018 of cap free
Amount of items: 2
Items: 
Size: 790074 Color: 1
Size: 206909 Color: 0

Bin 137: 3167 of cap free
Amount of items: 2
Items: 
Size: 655139 Color: 0
Size: 341695 Color: 1

Bin 138: 3231 of cap free
Amount of items: 2
Items: 
Size: 762300 Color: 0
Size: 234470 Color: 1

Bin 139: 3275 of cap free
Amount of items: 2
Items: 
Size: 685909 Color: 0
Size: 310817 Color: 1

Bin 140: 3284 of cap free
Amount of items: 2
Items: 
Size: 607754 Color: 0
Size: 388963 Color: 1

Bin 141: 3308 of cap free
Amount of items: 2
Items: 
Size: 637452 Color: 1
Size: 359241 Color: 0

Bin 142: 3331 of cap free
Amount of items: 2
Items: 
Size: 668911 Color: 0
Size: 327759 Color: 1

Bin 143: 3351 of cap free
Amount of items: 2
Items: 
Size: 540922 Color: 0
Size: 455728 Color: 1

Bin 144: 3478 of cap free
Amount of items: 2
Items: 
Size: 786233 Color: 1
Size: 210290 Color: 0

Bin 145: 3495 of cap free
Amount of items: 2
Items: 
Size: 752166 Color: 0
Size: 244340 Color: 1

Bin 146: 3543 of cap free
Amount of items: 2
Items: 
Size: 568046 Color: 0
Size: 428412 Color: 1

Bin 147: 3582 of cap free
Amount of items: 2
Items: 
Size: 677721 Color: 0
Size: 318698 Color: 1

Bin 148: 3586 of cap free
Amount of items: 2
Items: 
Size: 674010 Color: 0
Size: 322405 Color: 1

Bin 149: 3601 of cap free
Amount of items: 2
Items: 
Size: 611938 Color: 1
Size: 384462 Color: 0

Bin 150: 3613 of cap free
Amount of items: 2
Items: 
Size: 504611 Color: 0
Size: 491777 Color: 1

Bin 151: 3700 of cap free
Amount of items: 2
Items: 
Size: 524669 Color: 1
Size: 471632 Color: 0

Bin 152: 3786 of cap free
Amount of items: 2
Items: 
Size: 714272 Color: 0
Size: 281943 Color: 1

Bin 153: 3805 of cap free
Amount of items: 2
Items: 
Size: 564175 Color: 0
Size: 432021 Color: 1

Bin 154: 3830 of cap free
Amount of items: 2
Items: 
Size: 636947 Color: 1
Size: 359224 Color: 0

Bin 155: 4021 of cap free
Amount of items: 3
Items: 
Size: 579135 Color: 0
Size: 230088 Color: 0
Size: 186757 Color: 1

Bin 156: 4256 of cap free
Amount of items: 2
Items: 
Size: 508714 Color: 1
Size: 487031 Color: 0

Bin 157: 4319 of cap free
Amount of items: 2
Items: 
Size: 627442 Color: 1
Size: 368240 Color: 0

Bin 158: 4324 of cap free
Amount of items: 2
Items: 
Size: 548270 Color: 0
Size: 447407 Color: 1

Bin 159: 4615 of cap free
Amount of items: 2
Items: 
Size: 711287 Color: 1
Size: 284099 Color: 0

Bin 160: 4713 of cap free
Amount of items: 2
Items: 
Size: 591486 Color: 0
Size: 403802 Color: 1

Bin 161: 4981 of cap free
Amount of items: 2
Items: 
Size: 531380 Color: 1
Size: 463640 Color: 0

Bin 162: 4983 of cap free
Amount of items: 2
Items: 
Size: 742375 Color: 1
Size: 252643 Color: 0

Bin 163: 5126 of cap free
Amount of items: 2
Items: 
Size: 799647 Color: 1
Size: 195228 Color: 0

Bin 164: 5197 of cap free
Amount of items: 2
Items: 
Size: 765880 Color: 1
Size: 228924 Color: 0

Bin 165: 5575 of cap free
Amount of items: 2
Items: 
Size: 654524 Color: 1
Size: 339902 Color: 0

Bin 166: 6092 of cap free
Amount of items: 2
Items: 
Size: 620515 Color: 0
Size: 373394 Color: 1

Bin 167: 6522 of cap free
Amount of items: 2
Items: 
Size: 738252 Color: 0
Size: 255227 Color: 1

Bin 168: 6548 of cap free
Amount of items: 2
Items: 
Size: 683976 Color: 1
Size: 309477 Color: 0

Bin 169: 6848 of cap free
Amount of items: 2
Items: 
Size: 753609 Color: 1
Size: 239544 Color: 0

Bin 170: 7445 of cap free
Amount of items: 2
Items: 
Size: 596558 Color: 0
Size: 395998 Color: 1

Bin 171: 7545 of cap free
Amount of items: 2
Items: 
Size: 797905 Color: 0
Size: 194551 Color: 1

Bin 172: 8100 of cap free
Amount of items: 2
Items: 
Size: 737575 Color: 0
Size: 254326 Color: 1

Bin 173: 8247 of cap free
Amount of items: 2
Items: 
Size: 701478 Color: 1
Size: 290276 Color: 0

Bin 174: 8729 of cap free
Amount of items: 2
Items: 
Size: 752760 Color: 1
Size: 238512 Color: 0

Bin 175: 8748 of cap free
Amount of items: 2
Items: 
Size: 742059 Color: 1
Size: 249194 Color: 0

Bin 176: 8816 of cap free
Amount of items: 2
Items: 
Size: 633142 Color: 1
Size: 358043 Color: 0

Bin 177: 8899 of cap free
Amount of items: 2
Items: 
Size: 627357 Color: 0
Size: 363745 Color: 1

Bin 178: 9010 of cap free
Amount of items: 2
Items: 
Size: 552415 Color: 1
Size: 438576 Color: 0

Bin 179: 9114 of cap free
Amount of items: 2
Items: 
Size: 587232 Color: 1
Size: 403655 Color: 0

Bin 180: 9171 of cap free
Amount of items: 2
Items: 
Size: 780870 Color: 0
Size: 209960 Color: 1

Bin 181: 9856 of cap free
Amount of items: 2
Items: 
Size: 709466 Color: 0
Size: 280679 Color: 1

Bin 182: 9991 of cap free
Amount of items: 2
Items: 
Size: 653574 Color: 1
Size: 336436 Color: 0

Bin 183: 10104 of cap free
Amount of items: 2
Items: 
Size: 519153 Color: 1
Size: 470744 Color: 0

Bin 184: 10117 of cap free
Amount of items: 2
Items: 
Size: 551926 Color: 1
Size: 437958 Color: 0

Bin 185: 11193 of cap free
Amount of items: 2
Items: 
Size: 547677 Color: 0
Size: 441131 Color: 1

Bin 186: 11249 of cap free
Amount of items: 2
Items: 
Size: 737562 Color: 0
Size: 251190 Color: 1

Bin 187: 11381 of cap free
Amount of items: 2
Items: 
Size: 683008 Color: 1
Size: 305612 Color: 0

Bin 188: 11827 of cap free
Amount of items: 2
Items: 
Size: 780608 Color: 0
Size: 207566 Color: 1

Bin 189: 12032 of cap free
Amount of items: 2
Items: 
Size: 517294 Color: 1
Size: 470675 Color: 0

Bin 190: 12213 of cap free
Amount of items: 2
Items: 
Size: 720154 Color: 1
Size: 267634 Color: 0

Bin 191: 12404 of cap free
Amount of items: 2
Items: 
Size: 573254 Color: 1
Size: 414343 Color: 0

Bin 192: 13242 of cap free
Amount of items: 2
Items: 
Size: 707182 Color: 0
Size: 279577 Color: 1

Bin 193: 14788 of cap free
Amount of items: 2
Items: 
Size: 590623 Color: 0
Size: 394590 Color: 1

Bin 194: 15193 of cap free
Amount of items: 2
Items: 
Size: 650206 Color: 0
Size: 334602 Color: 1

Bin 195: 15868 of cap free
Amount of items: 2
Items: 
Size: 779797 Color: 0
Size: 204336 Color: 1

Bin 196: 16679 of cap free
Amount of items: 2
Items: 
Size: 504270 Color: 0
Size: 479052 Color: 1

Bin 197: 16816 of cap free
Amount of items: 2
Items: 
Size: 605270 Color: 1
Size: 377915 Color: 0

Bin 198: 17383 of cap free
Amount of items: 2
Items: 
Size: 733787 Color: 0
Size: 248831 Color: 1

Bin 199: 17757 of cap free
Amount of items: 2
Items: 
Size: 497947 Color: 1
Size: 484297 Color: 0

Bin 200: 18918 of cap free
Amount of items: 2
Items: 
Size: 604846 Color: 1
Size: 376237 Color: 0

Bin 201: 19249 of cap free
Amount of items: 2
Items: 
Size: 627153 Color: 1
Size: 353599 Color: 0

Bin 202: 19499 of cap free
Amount of items: 2
Items: 
Size: 777352 Color: 0
Size: 203150 Color: 1

Bin 203: 20472 of cap free
Amount of items: 2
Items: 
Size: 540144 Color: 0
Size: 439385 Color: 1

Bin 204: 20637 of cap free
Amount of items: 2
Items: 
Size: 652051 Color: 1
Size: 327313 Color: 0

Bin 205: 21125 of cap free
Amount of items: 2
Items: 
Size: 740899 Color: 1
Size: 237977 Color: 0

Bin 206: 21972 of cap free
Amount of items: 2
Items: 
Size: 676456 Color: 1
Size: 301573 Color: 0

Bin 207: 22549 of cap free
Amount of items: 2
Items: 
Size: 590025 Color: 0
Size: 387427 Color: 1

Bin 208: 23273 of cap free
Amount of items: 2
Items: 
Size: 739179 Color: 1
Size: 237549 Color: 0

Bin 209: 26832 of cap free
Amount of items: 2
Items: 
Size: 798686 Color: 1
Size: 174483 Color: 0

Bin 210: 27347 of cap free
Amount of items: 2
Items: 
Size: 706111 Color: 0
Size: 266543 Color: 1

Bin 211: 28459 of cap free
Amount of items: 2
Items: 
Size: 589897 Color: 0
Size: 381645 Color: 1

Bin 212: 31994 of cap free
Amount of items: 2
Items: 
Size: 776339 Color: 0
Size: 191668 Color: 1

Bin 213: 32546 of cap free
Amount of items: 2
Items: 
Size: 539355 Color: 0
Size: 428100 Color: 1

Bin 214: 35241 of cap free
Amount of items: 2
Items: 
Size: 798179 Color: 1
Size: 166581 Color: 0

Bin 215: 35451 of cap free
Amount of items: 2
Items: 
Size: 675645 Color: 1
Size: 288905 Color: 0

Bin 216: 38314 of cap free
Amount of items: 2
Items: 
Size: 627352 Color: 0
Size: 334335 Color: 1

Bin 217: 41726 of cap free
Amount of items: 2
Items: 
Size: 536440 Color: 0
Size: 421835 Color: 1

Bin 218: 47307 of cap free
Amount of items: 2
Items: 
Size: 704442 Color: 0
Size: 248252 Color: 1

Bin 219: 50359 of cap free
Amount of items: 2
Items: 
Size: 768849 Color: 0
Size: 180793 Color: 1

Bin 220: 59092 of cap free
Amount of items: 2
Items: 
Size: 767103 Color: 0
Size: 173806 Color: 1

Bin 221: 63138 of cap free
Amount of items: 2
Items: 
Size: 618871 Color: 0
Size: 317992 Color: 1

Bin 222: 90115 of cap free
Amount of items: 2
Items: 
Size: 626146 Color: 1
Size: 283740 Color: 0

Bin 223: 94022 of cap free
Amount of items: 2
Items: 
Size: 472065 Color: 1
Size: 433914 Color: 0

Bin 224: 203659 of cap free
Amount of items: 1
Items: 
Size: 796342 Color: 1

Bin 225: 204317 of cap free
Amount of items: 1
Items: 
Size: 795684 Color: 1

Bin 226: 206816 of cap free
Amount of items: 1
Items: 
Size: 793185 Color: 1

Bin 227: 215351 of cap free
Amount of items: 1
Items: 
Size: 784650 Color: 1

Bin 228: 221385 of cap free
Amount of items: 1
Items: 
Size: 778616 Color: 1

Bin 229: 223794 of cap free
Amount of items: 1
Items: 
Size: 776207 Color: 1

Bin 230: 224833 of cap free
Amount of items: 1
Items: 
Size: 775168 Color: 1

Bin 231: 226108 of cap free
Amount of items: 1
Items: 
Size: 773893 Color: 1

Bin 232: 238895 of cap free
Amount of items: 1
Items: 
Size: 761106 Color: 0

Bin 233: 240302 of cap free
Amount of items: 1
Items: 
Size: 759699 Color: 0

Bin 234: 241012 of cap free
Amount of items: 1
Items: 
Size: 758989 Color: 0

Bin 235: 266930 of cap free
Amount of items: 1
Items: 
Size: 733071 Color: 1

Total size: 230811528
Total free space: 4188707

