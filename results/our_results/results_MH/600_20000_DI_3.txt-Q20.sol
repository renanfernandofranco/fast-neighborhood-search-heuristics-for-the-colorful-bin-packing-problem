Capicity Bin: 16752
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 21
Items: 
Size: 1024 Color: 18
Size: 976 Color: 12
Size: 976 Color: 8
Size: 962 Color: 5
Size: 944 Color: 2
Size: 896 Color: 7
Size: 880 Color: 11
Size: 840 Color: 17
Size: 840 Color: 0
Size: 792 Color: 19
Size: 772 Color: 1
Size: 748 Color: 12
Size: 740 Color: 13
Size: 736 Color: 4
Size: 736 Color: 3
Size: 696 Color: 16
Size: 672 Color: 1
Size: 652 Color: 17
Size: 640 Color: 10
Size: 632 Color: 8
Size: 598 Color: 10

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 8389 Color: 4
Size: 6971 Color: 9
Size: 1032 Color: 17
Size: 360 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 11
Size: 7328 Color: 9
Size: 920 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 13
Size: 6888 Color: 16
Size: 288 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10330 Color: 11
Size: 6028 Color: 6
Size: 394 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10460 Color: 10
Size: 5760 Color: 12
Size: 532 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11070 Color: 8
Size: 5164 Color: 6
Size: 518 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11075 Color: 16
Size: 4731 Color: 8
Size: 946 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11218 Color: 12
Size: 5156 Color: 13
Size: 378 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12516 Color: 3
Size: 3736 Color: 4
Size: 500 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 17
Size: 3224 Color: 4
Size: 960 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 1
Size: 3660 Color: 4
Size: 424 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12764 Color: 0
Size: 3484 Color: 17
Size: 504 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 13
Size: 3608 Color: 8
Size: 370 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 5
Size: 3284 Color: 19
Size: 656 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13326 Color: 19
Size: 3098 Color: 6
Size: 328 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13327 Color: 8
Size: 3201 Color: 3
Size: 224 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 3
Size: 2858 Color: 12
Size: 404 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 18
Size: 2102 Color: 19
Size: 1088 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13692 Color: 12
Size: 2288 Color: 7
Size: 772 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 14
Size: 2600 Color: 19
Size: 396 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 14
Size: 2120 Color: 14
Size: 736 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13971 Color: 4
Size: 2319 Color: 1
Size: 462 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 17
Size: 2472 Color: 0
Size: 248 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14234 Color: 3
Size: 1804 Color: 17
Size: 714 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 17
Size: 2244 Color: 0
Size: 260 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 1
Size: 1592 Color: 11
Size: 836 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14415 Color: 14
Size: 1801 Color: 8
Size: 536 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 19
Size: 1670 Color: 14
Size: 568 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 19
Size: 1974 Color: 18
Size: 256 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14503 Color: 17
Size: 1819 Color: 2
Size: 430 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 0
Size: 1642 Color: 19
Size: 458 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 2
Size: 1700 Color: 4
Size: 388 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 18
Size: 1392 Color: 13
Size: 660 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14710 Color: 19
Size: 1198 Color: 6
Size: 844 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 9
Size: 1752 Color: 5
Size: 284 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14846 Color: 12
Size: 1394 Color: 7
Size: 512 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14854 Color: 14
Size: 1562 Color: 5
Size: 336 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14952 Color: 11
Size: 1448 Color: 13
Size: 352 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14958 Color: 17
Size: 1482 Color: 5
Size: 312 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14990 Color: 7
Size: 1442 Color: 2
Size: 320 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 15002 Color: 6
Size: 1070 Color: 15
Size: 680 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 15012 Color: 6
Size: 1360 Color: 17
Size: 380 Color: 16

Bin 44: 1 of cap free
Amount of items: 7
Items: 
Size: 8382 Color: 15
Size: 1922 Color: 3
Size: 1908 Color: 3
Size: 1880 Color: 19
Size: 1859 Color: 11
Size: 504 Color: 5
Size: 296 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 12572 Color: 11
Size: 3491 Color: 17
Size: 688 Color: 6

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 13
Size: 3863 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 13269 Color: 2
Size: 3386 Color: 14
Size: 96 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 13
Size: 2494 Color: 0
Size: 624 Color: 16

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 13817 Color: 8
Size: 2934 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 13919 Color: 16
Size: 1636 Color: 8
Size: 1196 Color: 3

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 13947 Color: 10
Size: 2804 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 14
Size: 2336 Color: 19
Size: 420 Color: 6

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 13996 Color: 5
Size: 2289 Color: 14
Size: 466 Color: 16

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 14007 Color: 9
Size: 2744 Color: 6

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 5
Size: 2691 Color: 17

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 14226 Color: 0
Size: 2525 Color: 9

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 14331 Color: 4
Size: 1804 Color: 17
Size: 616 Color: 4

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 14370 Color: 19
Size: 1979 Color: 2
Size: 402 Color: 16

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 14403 Color: 4
Size: 2348 Color: 3

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 10
Size: 1959 Color: 17
Size: 368 Color: 5

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 14523 Color: 7
Size: 1688 Color: 7
Size: 540 Color: 5

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 14571 Color: 1
Size: 1512 Color: 5
Size: 668 Color: 10

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 14856 Color: 13
Size: 1895 Color: 2

Bin 64: 2 of cap free
Amount of items: 8
Items: 
Size: 8380 Color: 16
Size: 1470 Color: 19
Size: 1394 Color: 15
Size: 1394 Color: 7
Size: 1392 Color: 16
Size: 1392 Color: 4
Size: 960 Color: 1
Size: 368 Color: 5

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 9353 Color: 7
Size: 6973 Color: 7
Size: 424 Color: 3

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 9446 Color: 14
Size: 6968 Color: 8
Size: 336 Color: 12

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 10433 Color: 12
Size: 5981 Color: 12
Size: 336 Color: 1

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 11
Size: 5206 Color: 14
Size: 240 Color: 3

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 11750 Color: 16
Size: 4520 Color: 5
Size: 480 Color: 15

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 12636 Color: 5
Size: 3698 Color: 14
Size: 416 Color: 8

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 12945 Color: 4
Size: 3261 Color: 17
Size: 544 Color: 13

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 16
Size: 3302 Color: 13
Size: 224 Color: 8

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 7
Size: 1694 Color: 10
Size: 1592 Color: 14

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 13762 Color: 0
Size: 2500 Color: 5
Size: 488 Color: 17

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 13
Size: 2300 Color: 16
Size: 408 Color: 5

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 16
Size: 2130 Color: 5
Size: 420 Color: 7

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 18
Size: 1484 Color: 16
Size: 704 Color: 19

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14591 Color: 6
Size: 2159 Color: 3

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14722 Color: 15
Size: 2028 Color: 6

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14898 Color: 10
Size: 1852 Color: 15

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 15026 Color: 11
Size: 1724 Color: 17

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 15048 Color: 12
Size: 1702 Color: 19

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 9480 Color: 5
Size: 6977 Color: 17
Size: 292 Color: 16

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 10329 Color: 2
Size: 6036 Color: 18
Size: 384 Color: 3

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 1
Size: 4209 Color: 5
Size: 560 Color: 13

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 14
Size: 2299 Color: 17
Size: 1216 Color: 5

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 13525 Color: 1
Size: 2664 Color: 18
Size: 560 Color: 16

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 11
Size: 2447 Color: 13
Size: 372 Color: 6

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 14035 Color: 9
Size: 1662 Color: 13
Size: 1052 Color: 15

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 14698 Color: 4
Size: 2051 Color: 12

Bin 91: 4 of cap free
Amount of items: 9
Items: 
Size: 8378 Color: 0
Size: 1392 Color: 4
Size: 1392 Color: 0
Size: 1312 Color: 15
Size: 1232 Color: 18
Size: 896 Color: 17
Size: 784 Color: 13
Size: 698 Color: 1
Size: 664 Color: 3

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 10506 Color: 5
Size: 5810 Color: 11
Size: 432 Color: 3

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 11703 Color: 10
Size: 4725 Color: 3
Size: 320 Color: 17

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 8
Size: 4207 Color: 14
Size: 424 Color: 6

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 13038 Color: 16
Size: 3710 Color: 12

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 7
Size: 3324 Color: 16

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 16
Size: 2808 Color: 17

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 14026 Color: 0
Size: 2722 Color: 6

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 18
Size: 2262 Color: 9
Size: 288 Color: 13

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 14450 Color: 8
Size: 1594 Color: 12
Size: 704 Color: 5

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 0
Size: 2122 Color: 15

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 14974 Color: 11
Size: 1774 Color: 7

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 11480 Color: 16
Size: 5267 Color: 15

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 14782 Color: 17
Size: 1965 Color: 0

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 11
Size: 6978 Color: 15
Size: 784 Color: 4

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 12302 Color: 6
Size: 4444 Color: 11

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 16
Size: 3358 Color: 14

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 5
Size: 3208 Color: 10

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 9
Size: 2876 Color: 8

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 13
Size: 1462 Color: 15
Size: 1024 Color: 16

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 4
Size: 2248 Color: 2

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 15032 Color: 13
Size: 1714 Color: 1

Bin 113: 7 of cap free
Amount of items: 7
Items: 
Size: 8381 Color: 11
Size: 1826 Color: 17
Size: 1498 Color: 14
Size: 1476 Color: 14
Size: 1452 Color: 5
Size: 1280 Color: 4
Size: 832 Color: 13

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 11599 Color: 13
Size: 4738 Color: 5
Size: 408 Color: 1

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 11686 Color: 5
Size: 4819 Color: 15
Size: 240 Color: 19

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 16
Size: 4182 Color: 17

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 14796 Color: 1
Size: 1949 Color: 16

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 6
Size: 3496 Color: 16
Size: 296 Color: 5

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 11
Size: 3532 Color: 16

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13308 Color: 16
Size: 3436 Color: 2

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14479 Color: 6
Size: 2265 Color: 0

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 14988 Color: 2
Size: 1756 Color: 10

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 15
Size: 3167 Color: 15
Size: 1136 Color: 9

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 14187 Color: 12
Size: 2556 Color: 3

Bin 125: 10 of cap free
Amount of items: 3
Items: 
Size: 9442 Color: 14
Size: 6980 Color: 16
Size: 320 Color: 16

Bin 126: 10 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 3
Size: 2662 Color: 0
Size: 696 Color: 13

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 14
Size: 2997 Color: 2

Bin 128: 10 of cap free
Amount of items: 2
Items: 
Size: 14468 Color: 9
Size: 2274 Color: 16

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 14860 Color: 9
Size: 1882 Color: 2

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 12571 Color: 13
Size: 4170 Color: 8

Bin 131: 11 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 15
Size: 2121 Color: 2
Size: 68 Color: 9

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 12664 Color: 15
Size: 2860 Color: 12
Size: 1216 Color: 5

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 11
Size: 3416 Color: 0

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 14386 Color: 7
Size: 2354 Color: 9

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 14878 Color: 11
Size: 1862 Color: 14

Bin 136: 13 of cap free
Amount of items: 2
Items: 
Size: 10572 Color: 2
Size: 6167 Color: 19

Bin 137: 13 of cap free
Amount of items: 2
Items: 
Size: 13421 Color: 10
Size: 3318 Color: 4

Bin 138: 14 of cap free
Amount of items: 3
Items: 
Size: 12459 Color: 19
Size: 2335 Color: 17
Size: 1944 Color: 4

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 5
Size: 2666 Color: 18

Bin 140: 15 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 6
Size: 5353 Color: 7
Size: 512 Color: 13

Bin 141: 15 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 10
Size: 2601 Color: 18
Size: 496 Color: 13

Bin 142: 15 of cap free
Amount of items: 2
Items: 
Size: 14588 Color: 6
Size: 2149 Color: 4

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 13157 Color: 2
Size: 3579 Color: 18

Bin 144: 16 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 12
Size: 3176 Color: 6

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 19
Size: 2203 Color: 8

Bin 146: 17 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 10
Size: 1735 Color: 2
Size: 28 Color: 19

Bin 147: 19 of cap free
Amount of items: 9
Items: 
Size: 8377 Color: 3
Size: 1200 Color: 19
Size: 1200 Color: 10
Size: 1200 Color: 7
Size: 1068 Color: 11
Size: 1048 Color: 3
Size: 1040 Color: 12
Size: 1032 Color: 19
Size: 568 Color: 13

Bin 148: 20 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 16
Size: 6488 Color: 15
Size: 364 Color: 17

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 14744 Color: 14
Size: 1986 Color: 2

Bin 150: 23 of cap free
Amount of items: 2
Items: 
Size: 12115 Color: 8
Size: 4614 Color: 13

Bin 151: 24 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 0
Size: 5196 Color: 18
Size: 564 Color: 12

Bin 152: 24 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 12
Size: 4516 Color: 18
Size: 172 Color: 5

Bin 153: 24 of cap free
Amount of items: 2
Items: 
Size: 13951 Color: 14
Size: 2777 Color: 0

Bin 154: 25 of cap free
Amount of items: 2
Items: 
Size: 13554 Color: 1
Size: 3173 Color: 2

Bin 155: 26 of cap free
Amount of items: 2
Items: 
Size: 10632 Color: 5
Size: 6094 Color: 1

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 14
Size: 3928 Color: 2

Bin 157: 28 of cap free
Amount of items: 2
Items: 
Size: 14322 Color: 9
Size: 2402 Color: 15

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 14210 Color: 15
Size: 2510 Color: 4

Bin 159: 34 of cap free
Amount of items: 2
Items: 
Size: 14163 Color: 5
Size: 2555 Color: 1

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 14379 Color: 10
Size: 2339 Color: 11

Bin 161: 36 of cap free
Amount of items: 3
Items: 
Size: 8800 Color: 3
Size: 6972 Color: 9
Size: 944 Color: 17

Bin 162: 36 of cap free
Amount of items: 3
Items: 
Size: 12318 Color: 3
Size: 4222 Color: 15
Size: 176 Color: 10

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 16
Size: 2032 Color: 4

Bin 164: 37 of cap free
Amount of items: 2
Items: 
Size: 11734 Color: 19
Size: 4981 Color: 4

Bin 165: 37 of cap free
Amount of items: 4
Items: 
Size: 12254 Color: 9
Size: 3485 Color: 3
Size: 864 Color: 13
Size: 112 Color: 11

Bin 166: 38 of cap free
Amount of items: 2
Items: 
Size: 12790 Color: 15
Size: 3924 Color: 3

Bin 167: 38 of cap free
Amount of items: 2
Items: 
Size: 14207 Color: 2
Size: 2507 Color: 12

Bin 168: 40 of cap free
Amount of items: 2
Items: 
Size: 8456 Color: 9
Size: 8256 Color: 4

Bin 169: 41 of cap free
Amount of items: 2
Items: 
Size: 14575 Color: 12
Size: 2136 Color: 2

Bin 170: 45 of cap free
Amount of items: 2
Items: 
Size: 10971 Color: 14
Size: 5736 Color: 9

Bin 171: 46 of cap free
Amount of items: 2
Items: 
Size: 12726 Color: 11
Size: 3980 Color: 9

Bin 172: 48 of cap free
Amount of items: 2
Items: 
Size: 12839 Color: 9
Size: 3865 Color: 16

Bin 173: 49 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 13
Size: 5995 Color: 8
Size: 1184 Color: 3

Bin 174: 49 of cap free
Amount of items: 2
Items: 
Size: 12953 Color: 14
Size: 3750 Color: 17

Bin 175: 49 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 13
Size: 2903 Color: 6

Bin 176: 55 of cap free
Amount of items: 2
Items: 
Size: 14671 Color: 6
Size: 2026 Color: 14

Bin 177: 58 of cap free
Amount of items: 2
Items: 
Size: 11340 Color: 0
Size: 5354 Color: 18

Bin 178: 64 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 12
Size: 5160 Color: 6
Size: 1392 Color: 4

Bin 179: 64 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 6
Size: 4408 Color: 5

Bin 180: 67 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 19
Size: 2392 Color: 4

Bin 181: 68 of cap free
Amount of items: 2
Items: 
Size: 14002 Color: 3
Size: 2682 Color: 10

Bin 182: 68 of cap free
Amount of items: 2
Items: 
Size: 14285 Color: 12
Size: 2399 Color: 19

Bin 183: 73 of cap free
Amount of items: 2
Items: 
Size: 13723 Color: 8
Size: 2956 Color: 17

Bin 184: 80 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 6
Size: 7144 Color: 3
Size: 1136 Color: 1

Bin 185: 80 of cap free
Amount of items: 2
Items: 
Size: 11428 Color: 10
Size: 5244 Color: 17

Bin 186: 84 of cap free
Amount of items: 36
Items: 
Size: 660 Color: 1
Size: 634 Color: 1
Size: 584 Color: 8
Size: 584 Color: 5
Size: 580 Color: 15
Size: 568 Color: 7
Size: 568 Color: 2
Size: 554 Color: 6
Size: 532 Color: 11
Size: 528 Color: 18
Size: 528 Color: 17
Size: 496 Color: 13
Size: 480 Color: 17
Size: 468 Color: 12
Size: 466 Color: 5
Size: 464 Color: 13
Size: 464 Color: 9
Size: 456 Color: 18
Size: 456 Color: 10
Size: 452 Color: 6
Size: 452 Color: 1
Size: 448 Color: 10
Size: 448 Color: 0
Size: 432 Color: 17
Size: 416 Color: 3
Size: 400 Color: 6
Size: 392 Color: 13
Size: 390 Color: 18
Size: 384 Color: 13
Size: 376 Color: 11
Size: 362 Color: 4
Size: 346 Color: 7
Size: 344 Color: 12
Size: 340 Color: 8
Size: 312 Color: 2
Size: 304 Color: 7

Bin 187: 92 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 2
Size: 5992 Color: 14
Size: 1152 Color: 4

Bin 188: 106 of cap free
Amount of items: 2
Items: 
Size: 10556 Color: 4
Size: 6090 Color: 18

Bin 189: 118 of cap free
Amount of items: 2
Items: 
Size: 10378 Color: 16
Size: 6256 Color: 0

Bin 190: 128 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 7
Size: 4904 Color: 12

Bin 191: 141 of cap free
Amount of items: 2
Items: 
Size: 11083 Color: 3
Size: 5528 Color: 16

Bin 192: 144 of cap free
Amount of items: 3
Items: 
Size: 8388 Color: 6
Size: 6640 Color: 8
Size: 1580 Color: 12

Bin 193: 156 of cap free
Amount of items: 2
Items: 
Size: 10524 Color: 5
Size: 6072 Color: 9

Bin 194: 156 of cap free
Amount of items: 2
Items: 
Size: 12044 Color: 14
Size: 4552 Color: 7

Bin 195: 180 of cap free
Amount of items: 5
Items: 
Size: 8385 Color: 14
Size: 2106 Color: 18
Size: 2084 Color: 12
Size: 2019 Color: 9
Size: 1978 Color: 7

Bin 196: 195 of cap free
Amount of items: 2
Items: 
Size: 9575 Color: 1
Size: 6982 Color: 16

Bin 197: 212 of cap free
Amount of items: 2
Items: 
Size: 9559 Color: 19
Size: 6981 Color: 16

Bin 198: 223 of cap free
Amount of items: 2
Items: 
Size: 11705 Color: 4
Size: 4824 Color: 13

Bin 199: 12588 of cap free
Amount of items: 14
Items: 
Size: 362 Color: 10
Size: 360 Color: 9
Size: 344 Color: 15
Size: 340 Color: 15
Size: 320 Color: 11
Size: 296 Color: 14
Size: 288 Color: 5
Size: 288 Color: 2
Size: 284 Color: 17
Size: 278 Color: 7
Size: 272 Color: 4
Size: 260 Color: 12
Size: 236 Color: 19
Size: 236 Color: 8

Total size: 3316896
Total free space: 16752

