Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 13
Size: 261 Color: 4
Size: 251 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 16
Size: 329 Color: 12
Size: 303 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 15
Size: 342 Color: 12
Size: 292 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 10
Size: 343 Color: 3
Size: 252 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 10
Size: 361 Color: 16
Size: 257 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 350 Color: 4
Size: 259 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 286 Color: 17
Size: 255 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 15
Size: 316 Color: 8
Size: 257 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 18
Size: 276 Color: 15
Size: 254 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 15
Size: 342 Color: 6
Size: 263 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 11
Size: 367 Color: 12
Size: 270 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 6
Size: 279 Color: 13
Size: 261 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 305 Color: 15
Size: 288 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9
Size: 282 Color: 18
Size: 259 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 6
Size: 254 Color: 8
Size: 253 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 14
Size: 273 Color: 11
Size: 267 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 251 Color: 13
Size: 250 Color: 7
Size: 499 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7
Size: 358 Color: 13
Size: 258 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 298 Color: 15
Size: 287 Color: 13
Size: 415 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 5
Size: 324 Color: 9
Size: 253 Color: 15

Total size: 20000
Total free space: 0

