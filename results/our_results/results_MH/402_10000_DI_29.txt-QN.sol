Capicity Bin: 7512
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5402 Color: 314
Size: 1742 Color: 225
Size: 368 Color: 100

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5410 Color: 315
Size: 1746 Color: 226
Size: 356 Color: 97

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5621 Color: 323
Size: 1577 Color: 217
Size: 314 Color: 86

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 324
Size: 1563 Color: 215
Size: 320 Color: 88

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5634 Color: 325
Size: 1194 Color: 196
Size: 684 Color: 141

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5810 Color: 329
Size: 1566 Color: 216
Size: 136 Color: 20

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5846 Color: 332
Size: 1610 Color: 219
Size: 56 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 334
Size: 1620 Color: 221
Size: 32 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6000 Color: 338
Size: 1380 Color: 207
Size: 132 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6073 Color: 343
Size: 745 Color: 149
Size: 694 Color: 142

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6098 Color: 347
Size: 1136 Color: 191
Size: 278 Color: 77

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6194 Color: 350
Size: 962 Color: 176
Size: 356 Color: 98

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 351
Size: 1087 Color: 188
Size: 222 Color: 61

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6204 Color: 352
Size: 1092 Color: 189
Size: 216 Color: 59

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 354
Size: 1066 Color: 186
Size: 216 Color: 60

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6238 Color: 355
Size: 962 Color: 177
Size: 312 Color: 84

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 357
Size: 724 Color: 146
Size: 504 Color: 119

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 359
Size: 980 Color: 180
Size: 192 Color: 50

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 361
Size: 963 Color: 178
Size: 192 Color: 49

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 362
Size: 954 Color: 175
Size: 200 Color: 53

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6378 Color: 365
Size: 778 Color: 151
Size: 356 Color: 96

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6391 Color: 366
Size: 897 Color: 167
Size: 224 Color: 63

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6394 Color: 367
Size: 782 Color: 153
Size: 336 Color: 90

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 368
Size: 932 Color: 170
Size: 184 Color: 44

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6469 Color: 371
Size: 651 Color: 139
Size: 392 Color: 101

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 375
Size: 608 Color: 131
Size: 412 Color: 107

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6498 Color: 376
Size: 846 Color: 162
Size: 168 Color: 38

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 377
Size: 871 Color: 165
Size: 138 Color: 21

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 378
Size: 737 Color: 147
Size: 268 Color: 74

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 380
Size: 815 Color: 158
Size: 162 Color: 33

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 386
Size: 576 Color: 129
Size: 348 Color: 92

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 388
Size: 737 Color: 148
Size: 156 Color: 29

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 389
Size: 699 Color: 143
Size: 184 Color: 43

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 391
Size: 789 Color: 155
Size: 66 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 397
Size: 701 Color: 144
Size: 108 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 398
Size: 644 Color: 138
Size: 158 Color: 31

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6740 Color: 400
Size: 468 Color: 116
Size: 304 Color: 83

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6742 Color: 401
Size: 538 Color: 126
Size: 232 Color: 65

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6011 Color: 339
Size: 1372 Color: 206
Size: 128 Color: 16

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 340
Size: 1338 Color: 203
Size: 154 Color: 28

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6052 Color: 342
Size: 1347 Color: 204
Size: 112 Color: 11

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 353
Size: 1292 Color: 202

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 360
Size: 1163 Color: 194

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6483 Color: 373
Size: 1028 Color: 184

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 374
Size: 1021 Color: 182

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6539 Color: 381
Size: 972 Color: 179

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6582 Color: 385
Size: 929 Color: 169

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6700 Color: 396
Size: 811 Color: 157

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6731 Color: 399
Size: 780 Color: 152

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 402
Size: 544 Color: 128
Size: 208 Color: 56

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 5418 Color: 316
Size: 2092 Color: 240

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 5484 Color: 318
Size: 2026 Color: 235

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 328
Size: 1754 Color: 227

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 5849 Color: 333
Size: 1661 Color: 222

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6117 Color: 348
Size: 1393 Color: 209

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6370 Color: 364
Size: 1140 Color: 192

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 384
Size: 936 Color: 173

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 387
Size: 914 Color: 168

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 392
Size: 852 Color: 163

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 293
Size: 2693 Color: 257
Size: 156 Color: 30

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 5841 Color: 331
Size: 1668 Color: 223

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6563 Color: 382
Size: 946 Color: 174

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 390
Size: 859 Color: 164

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 5678 Color: 327
Size: 1802 Color: 231
Size: 28 Color: 1

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 6247 Color: 356
Size: 1245 Color: 201
Size: 16 Color: 0

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6682 Color: 395
Size: 826 Color: 159

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6287 Color: 358
Size: 1220 Color: 199

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 370
Size: 1055 Color: 185

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 393
Size: 839 Color: 161

Bin 70: 6 of cap free
Amount of items: 7
Items: 
Size: 3758 Color: 274
Size: 884 Color: 166
Size: 791 Color: 156
Size: 785 Color: 154
Size: 764 Color: 150
Size: 288 Color: 81
Size: 236 Color: 66

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 5373 Color: 313
Size: 2037 Color: 237
Size: 96 Color: 8

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 5894 Color: 335
Size: 1612 Color: 220

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6074 Color: 344
Size: 1432 Color: 212

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 346
Size: 1422 Color: 211

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6571 Color: 383
Size: 935 Color: 172

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 6675 Color: 394
Size: 831 Color: 160

Bin 77: 7 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 363
Size: 1143 Color: 193

Bin 78: 7 of cap free
Amount of items: 2
Items: 
Size: 6515 Color: 379
Size: 990 Color: 181

Bin 79: 8 of cap free
Amount of items: 3
Items: 
Size: 5034 Color: 302
Size: 2342 Color: 245
Size: 128 Color: 17

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 309
Size: 2148 Color: 241
Size: 120 Color: 14

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 330
Size: 1692 Color: 224

Bin 82: 10 of cap free
Amount of items: 4
Items: 
Size: 4706 Color: 296
Size: 2492 Color: 249
Size: 152 Color: 27
Size: 152 Color: 26

Bin 83: 10 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 312
Size: 2031 Color: 236
Size: 104 Color: 9

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 6082 Color: 345
Size: 1420 Color: 210

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 6478 Color: 372
Size: 1024 Color: 183

Bin 86: 11 of cap free
Amount of items: 3
Items: 
Size: 5077 Color: 305
Size: 1242 Color: 200
Size: 1182 Color: 195

Bin 87: 11 of cap free
Amount of items: 2
Items: 
Size: 6399 Color: 369
Size: 1102 Color: 190

Bin 88: 13 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 292
Size: 2687 Color: 256
Size: 162 Color: 32

Bin 89: 14 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 349
Size: 1350 Color: 205

Bin 90: 15 of cap free
Amount of items: 2
Items: 
Size: 5637 Color: 326
Size: 1860 Color: 232

Bin 91: 16 of cap free
Amount of items: 3
Items: 
Size: 5354 Color: 311
Size: 2022 Color: 234
Size: 120 Color: 12

Bin 92: 18 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 282
Size: 3052 Color: 265
Size: 192 Color: 52

Bin 93: 18 of cap free
Amount of items: 3
Items: 
Size: 4412 Color: 290
Size: 2916 Color: 263
Size: 166 Color: 35

Bin 94: 18 of cap free
Amount of items: 2
Items: 
Size: 5964 Color: 337
Size: 1530 Color: 214

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 6026 Color: 341
Size: 1468 Color: 213

Bin 96: 20 of cap free
Amount of items: 2
Items: 
Size: 5426 Color: 317
Size: 2066 Color: 239

Bin 97: 21 of cap free
Amount of items: 2
Items: 
Size: 5910 Color: 336
Size: 1581 Color: 218

Bin 98: 23 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 280
Size: 3129 Color: 268
Size: 204 Color: 54

Bin 99: 30 of cap free
Amount of items: 2
Items: 
Size: 3762 Color: 276
Size: 3720 Color: 272

Bin 100: 30 of cap free
Amount of items: 2
Items: 
Size: 4698 Color: 295
Size: 2784 Color: 262

Bin 101: 32 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 319
Size: 1964 Color: 233

Bin 102: 36 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 289
Size: 2988 Color: 264
Size: 166 Color: 36

Bin 103: 36 of cap free
Amount of items: 2
Items: 
Size: 5090 Color: 307
Size: 2386 Color: 248

Bin 104: 40 of cap free
Amount of items: 3
Items: 
Size: 5069 Color: 304
Size: 1202 Color: 198
Size: 1201 Color: 197

Bin 105: 43 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 301
Size: 2335 Color: 244
Size: 130 Color: 18

Bin 106: 48 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 299
Size: 2524 Color: 251
Size: 144 Color: 23

Bin 107: 48 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 308
Size: 2180 Color: 242
Size: 128 Color: 15

Bin 108: 50 of cap free
Amount of items: 2
Items: 
Size: 5082 Color: 306
Size: 2380 Color: 247

Bin 109: 52 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 322
Size: 1789 Color: 230
Size: 56 Color: 4

Bin 110: 59 of cap free
Amount of items: 3
Items: 
Size: 4719 Color: 298
Size: 2588 Color: 252
Size: 146 Color: 24

Bin 111: 61 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 321
Size: 1783 Color: 229
Size: 56 Color: 5

Bin 112: 64 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 310
Size: 2044 Color: 238
Size: 120 Color: 13

Bin 113: 74 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 320
Size: 1762 Color: 228
Size: 96 Color: 7

Bin 114: 80 of cap free
Amount of items: 20
Items: 
Size: 472 Color: 117
Size: 466 Color: 115
Size: 464 Color: 114
Size: 464 Color: 113
Size: 464 Color: 112
Size: 464 Color: 111
Size: 424 Color: 110
Size: 424 Color: 109
Size: 416 Color: 108
Size: 408 Color: 106
Size: 406 Color: 105
Size: 404 Color: 104
Size: 280 Color: 80
Size: 280 Color: 79
Size: 280 Color: 78
Size: 276 Color: 76
Size: 272 Color: 75
Size: 264 Color: 73
Size: 256 Color: 72
Size: 248 Color: 71

Bin 115: 90 of cap free
Amount of items: 2
Items: 
Size: 4696 Color: 294
Size: 2726 Color: 261

Bin 116: 103 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 300
Size: 2329 Color: 243
Size: 140 Color: 22

Bin 117: 106 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 303
Size: 2346 Color: 246

Bin 118: 108 of cap free
Amount of items: 2
Items: 
Size: 4273 Color: 283
Size: 3131 Color: 270

Bin 119: 128 of cap free
Amount of items: 15
Items: 
Size: 624 Color: 134
Size: 624 Color: 133
Size: 624 Color: 132
Size: 592 Color: 130
Size: 540 Color: 127
Size: 538 Color: 125
Size: 536 Color: 124
Size: 532 Color: 123
Size: 532 Color: 122
Size: 532 Color: 121
Size: 512 Color: 120
Size: 476 Color: 118
Size: 244 Color: 70
Size: 240 Color: 69
Size: 238 Color: 68

Bin 120: 130 of cap free
Amount of items: 5
Items: 
Size: 3759 Color: 275
Size: 1387 Color: 208
Size: 1070 Color: 187
Size: 934 Color: 171
Size: 232 Color: 64

Bin 121: 133 of cap free
Amount of items: 3
Items: 
Size: 4711 Color: 297
Size: 2520 Color: 250
Size: 148 Color: 25

Bin 122: 136 of cap free
Amount of items: 2
Items: 
Size: 4246 Color: 281
Size: 3130 Color: 269

Bin 123: 138 of cap free
Amount of items: 4
Items: 
Size: 4314 Color: 288
Size: 2722 Color: 259
Size: 170 Color: 39
Size: 168 Color: 37

Bin 124: 140 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 291
Size: 2722 Color: 260
Size: 166 Color: 34

Bin 125: 154 of cap free
Amount of items: 3
Items: 
Size: 3852 Color: 278
Size: 3296 Color: 271
Size: 210 Color: 57

Bin 126: 157 of cap free
Amount of items: 4
Items: 
Size: 4306 Color: 287
Size: 2701 Color: 258
Size: 176 Color: 41
Size: 172 Color: 40

Bin 127: 160 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 279
Size: 3126 Color: 267
Size: 206 Color: 55

Bin 128: 162 of cap free
Amount of items: 4
Items: 
Size: 4298 Color: 286
Size: 2682 Color: 255
Size: 186 Color: 45
Size: 184 Color: 42

Bin 129: 173 of cap free
Amount of items: 4
Items: 
Size: 4289 Color: 285
Size: 2674 Color: 254
Size: 188 Color: 47
Size: 188 Color: 46

Bin 130: 185 of cap free
Amount of items: 4
Items: 
Size: 4281 Color: 284
Size: 2666 Color: 253
Size: 192 Color: 51
Size: 188 Color: 48

Bin 131: 188 of cap free
Amount of items: 4
Items: 
Size: 3764 Color: 277
Size: 3124 Color: 266
Size: 224 Color: 62
Size: 212 Color: 58

Bin 132: 237 of cap free
Amount of items: 7
Items: 
Size: 3757 Color: 273
Size: 722 Color: 145
Size: 670 Color: 140
Size: 642 Color: 137
Size: 624 Color: 136
Size: 624 Color: 135
Size: 236 Color: 67

Bin 133: 3696 of cap free
Amount of items: 11
Items: 
Size: 404 Color: 103
Size: 400 Color: 102
Size: 368 Color: 99
Size: 352 Color: 95
Size: 348 Color: 94
Size: 348 Color: 93
Size: 344 Color: 91
Size: 328 Color: 89
Size: 316 Color: 87
Size: 312 Color: 85
Size: 296 Color: 82

Total size: 991584
Total free space: 7512

