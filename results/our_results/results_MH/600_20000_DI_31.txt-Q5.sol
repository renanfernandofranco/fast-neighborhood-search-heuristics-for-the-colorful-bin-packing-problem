Capicity Bin: 16608
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9459 Color: 3
Size: 5763 Color: 3
Size: 1386 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9655 Color: 3
Size: 5955 Color: 0
Size: 998 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10358 Color: 0
Size: 5950 Color: 4
Size: 300 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11384 Color: 1
Size: 4042 Color: 2
Size: 1182 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 0
Size: 4360 Color: 2
Size: 400 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 4
Size: 4004 Color: 0
Size: 372 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12472 Color: 2
Size: 3656 Color: 4
Size: 480 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 0
Size: 3466 Color: 3
Size: 312 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 2
Size: 3144 Color: 4
Size: 464 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13146 Color: 2
Size: 2262 Color: 4
Size: 1200 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 1
Size: 2810 Color: 3
Size: 640 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 2
Size: 2988 Color: 1
Size: 384 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 1
Size: 1699 Color: 2
Size: 1649 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 2
Size: 2404 Color: 0
Size: 908 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 4
Size: 3016 Color: 1
Size: 320 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13345 Color: 4
Size: 1642 Color: 2
Size: 1621 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 2
Size: 2079 Color: 3
Size: 1136 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 4
Size: 2812 Color: 1
Size: 364 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 4
Size: 2090 Color: 3
Size: 956 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 2056 Color: 4
Size: 912 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13649 Color: 1
Size: 2099 Color: 4
Size: 860 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 1
Size: 2405 Color: 3
Size: 480 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13710 Color: 2
Size: 2602 Color: 3
Size: 296 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 1
Size: 2244 Color: 4
Size: 564 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 1
Size: 2210 Color: 0
Size: 520 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 1
Size: 2022 Color: 2
Size: 692 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 0
Size: 2594 Color: 1
Size: 72 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 0
Size: 2344 Color: 3
Size: 320 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 3
Size: 2260 Color: 4
Size: 344 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 3
Size: 1476 Color: 4
Size: 1064 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14081 Color: 1
Size: 2075 Color: 4
Size: 452 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14099 Color: 1
Size: 2211 Color: 1
Size: 298 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 3
Size: 1320 Color: 2
Size: 1186 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 3
Size: 1644 Color: 1
Size: 850 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 2
Size: 1296 Color: 1
Size: 1190 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 0
Size: 2423 Color: 3
Size: 58 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 2
Size: 1564 Color: 0
Size: 912 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 1
Size: 2107 Color: 4
Size: 378 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 4
Size: 2232 Color: 1
Size: 212 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 2
Size: 2074 Color: 2
Size: 322 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 2
Size: 1672 Color: 1
Size: 688 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 3
Size: 2004 Color: 2
Size: 312 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 0
Size: 1576 Color: 1
Size: 704 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 4
Size: 1188 Color: 1
Size: 1024 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 1376 Color: 2
Size: 744 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 1
Size: 1088 Color: 4
Size: 998 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14525 Color: 1
Size: 1651 Color: 0
Size: 432 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14556 Color: 4
Size: 1380 Color: 1
Size: 672 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14581 Color: 2
Size: 1493 Color: 1
Size: 534 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 1
Size: 1684 Color: 3
Size: 280 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 2
Size: 1072 Color: 2
Size: 920 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14665 Color: 3
Size: 1467 Color: 1
Size: 476 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14712 Color: 3
Size: 1800 Color: 1
Size: 96 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 4
Size: 1190 Color: 1
Size: 704 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 2
Size: 1496 Color: 1
Size: 384 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14773 Color: 1
Size: 1531 Color: 3
Size: 304 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14800 Color: 1
Size: 1312 Color: 2
Size: 496 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14812 Color: 1
Size: 1380 Color: 4
Size: 416 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14824 Color: 1
Size: 1520 Color: 4
Size: 264 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14814 Color: 3
Size: 1382 Color: 2
Size: 412 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14908 Color: 1
Size: 1190 Color: 3
Size: 510 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 2
Size: 1162 Color: 4
Size: 560 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14946 Color: 0
Size: 1382 Color: 4
Size: 280 Color: 1

Bin 64: 1 of cap free
Amount of items: 19
Items: 
Size: 1673 Color: 2
Size: 1498 Color: 1
Size: 1184 Color: 4
Size: 1040 Color: 4
Size: 1040 Color: 2
Size: 1040 Color: 1
Size: 890 Color: 4
Size: 864 Color: 3
Size: 816 Color: 1
Size: 804 Color: 0
Size: 784 Color: 0
Size: 754 Color: 1
Size: 720 Color: 1
Size: 720 Color: 1
Size: 688 Color: 0
Size: 614 Color: 3
Size: 604 Color: 3
Size: 538 Color: 0
Size: 336 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 8314 Color: 3
Size: 6917 Color: 0
Size: 1376 Color: 2

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 9531 Color: 2
Size: 6504 Color: 4
Size: 572 Color: 0

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 10156 Color: 4
Size: 6187 Color: 3
Size: 264 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 10861 Color: 2
Size: 5332 Color: 2
Size: 414 Color: 0

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 11170 Color: 1
Size: 4997 Color: 2
Size: 440 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12450 Color: 0
Size: 3773 Color: 4
Size: 384 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 0
Size: 2983 Color: 4
Size: 450 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 1
Size: 2681 Color: 1
Size: 704 Color: 4

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13265 Color: 3
Size: 3150 Color: 4
Size: 192 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 1
Size: 3138 Color: 2
Size: 148 Color: 2

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13866 Color: 2
Size: 2257 Color: 4
Size: 484 Color: 1

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 2
Size: 2391 Color: 1
Size: 292 Color: 4

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 2
Size: 1532 Color: 4
Size: 960 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 14319 Color: 0
Size: 1438 Color: 1
Size: 850 Color: 3

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 14817 Color: 1
Size: 1376 Color: 3
Size: 414 Color: 2

Bin 80: 2 of cap free
Amount of items: 7
Items: 
Size: 8305 Color: 0
Size: 2082 Color: 1
Size: 1912 Color: 1
Size: 1691 Color: 0
Size: 1184 Color: 3
Size: 1064 Color: 3
Size: 368 Color: 4

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 9140 Color: 1
Size: 6906 Color: 0
Size: 560 Color: 2

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 10609 Color: 1
Size: 4941 Color: 0
Size: 1056 Color: 3

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 4
Size: 5210 Color: 0
Size: 304 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 2
Size: 4026 Color: 3
Size: 800 Color: 4

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 11826 Color: 1
Size: 4152 Color: 1
Size: 628 Color: 4

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 1
Size: 3846 Color: 3

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 0
Size: 3371 Color: 1
Size: 352 Color: 4

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 13958 Color: 0
Size: 2648 Color: 3

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 0
Size: 1976 Color: 4
Size: 256 Color: 1

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 14470 Color: 0
Size: 2136 Color: 2

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 3
Size: 2048 Color: 0

Bin 92: 3 of cap free
Amount of items: 6
Items: 
Size: 8306 Color: 1
Size: 2733 Color: 0
Size: 2288 Color: 1
Size: 1558 Color: 3
Size: 1240 Color: 3
Size: 480 Color: 2

Bin 93: 3 of cap free
Amount of items: 5
Items: 
Size: 8308 Color: 1
Size: 5938 Color: 4
Size: 1691 Color: 3
Size: 340 Color: 1
Size: 328 Color: 0

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 9791 Color: 1
Size: 6014 Color: 0
Size: 800 Color: 3

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 1
Size: 5939 Color: 0
Size: 232 Color: 4

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 11447 Color: 3
Size: 4318 Color: 3
Size: 840 Color: 4

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 11557 Color: 0
Size: 4856 Color: 0
Size: 192 Color: 1

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 1
Size: 3073 Color: 2
Size: 256 Color: 0

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 13373 Color: 4
Size: 2776 Color: 2
Size: 456 Color: 2

Bin 100: 3 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 2
Size: 3208 Color: 1

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 13437 Color: 2
Size: 2822 Color: 4
Size: 346 Color: 3

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 14844 Color: 3
Size: 1737 Color: 4
Size: 24 Color: 1

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 10024 Color: 2
Size: 6188 Color: 0
Size: 392 Color: 4

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 10196 Color: 1
Size: 6144 Color: 0
Size: 264 Color: 4

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 0
Size: 3448 Color: 1
Size: 320 Color: 4

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 2
Size: 2796 Color: 4

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 2
Size: 2418 Color: 4

Bin 108: 4 of cap free
Amount of items: 2
Items: 
Size: 14742 Color: 2
Size: 1862 Color: 0

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 14888 Color: 0
Size: 1716 Color: 4

Bin 110: 5 of cap free
Amount of items: 3
Items: 
Size: 8681 Color: 1
Size: 5854 Color: 2
Size: 2068 Color: 0

Bin 111: 5 of cap free
Amount of items: 2
Items: 
Size: 10792 Color: 1
Size: 5811 Color: 4

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 12081 Color: 2
Size: 3618 Color: 4
Size: 904 Color: 3

Bin 113: 5 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 0
Size: 3027 Color: 4
Size: 1232 Color: 3

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 13741 Color: 4
Size: 2862 Color: 0

Bin 115: 5 of cap free
Amount of items: 2
Items: 
Size: 14423 Color: 2
Size: 2180 Color: 4

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 9486 Color: 0
Size: 5928 Color: 1
Size: 1188 Color: 4

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 4
Size: 4546 Color: 1
Size: 294 Color: 3

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 2
Size: 3454 Color: 4
Size: 768 Color: 2

Bin 119: 6 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 3
Size: 3124 Color: 1
Size: 240 Color: 3

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 3
Size: 2878 Color: 0

Bin 121: 7 of cap free
Amount of items: 5
Items: 
Size: 8313 Color: 4
Size: 4534 Color: 2
Size: 1710 Color: 3
Size: 1240 Color: 4
Size: 804 Color: 0

Bin 122: 7 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 3
Size: 3409 Color: 1

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 10545 Color: 4
Size: 5431 Color: 0
Size: 624 Color: 2

Bin 124: 8 of cap free
Amount of items: 2
Items: 
Size: 10681 Color: 2
Size: 5919 Color: 4

Bin 125: 8 of cap free
Amount of items: 3
Items: 
Size: 12921 Color: 4
Size: 2703 Color: 1
Size: 976 Color: 0

Bin 126: 8 of cap free
Amount of items: 2
Items: 
Size: 13957 Color: 4
Size: 2643 Color: 2

Bin 127: 9 of cap free
Amount of items: 3
Items: 
Size: 8317 Color: 3
Size: 6902 Color: 2
Size: 1380 Color: 2

Bin 128: 9 of cap free
Amount of items: 3
Items: 
Size: 10712 Color: 0
Size: 3816 Color: 1
Size: 2071 Color: 3

Bin 129: 10 of cap free
Amount of items: 3
Items: 
Size: 9394 Color: 1
Size: 6916 Color: 2
Size: 288 Color: 4

Bin 130: 10 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 2
Size: 4580 Color: 4
Size: 864 Color: 0

Bin 131: 10 of cap free
Amount of items: 2
Items: 
Size: 11994 Color: 2
Size: 4604 Color: 0

Bin 132: 10 of cap free
Amount of items: 2
Items: 
Size: 13901 Color: 3
Size: 2697 Color: 4

Bin 133: 11 of cap free
Amount of items: 3
Items: 
Size: 13588 Color: 1
Size: 2833 Color: 2
Size: 176 Color: 1

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 0
Size: 2286 Color: 4

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 14601 Color: 2
Size: 1995 Color: 3

Bin 136: 13 of cap free
Amount of items: 2
Items: 
Size: 11804 Color: 3
Size: 4791 Color: 2

Bin 137: 13 of cap free
Amount of items: 2
Items: 
Size: 14769 Color: 0
Size: 1826 Color: 3

Bin 138: 14 of cap free
Amount of items: 4
Items: 
Size: 8309 Color: 1
Size: 4920 Color: 3
Size: 2741 Color: 1
Size: 624 Color: 2

Bin 139: 14 of cap free
Amount of items: 3
Items: 
Size: 8312 Color: 0
Size: 7296 Color: 4
Size: 986 Color: 1

Bin 140: 14 of cap free
Amount of items: 3
Items: 
Size: 9507 Color: 0
Size: 5951 Color: 4
Size: 1136 Color: 1

Bin 141: 15 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 3
Size: 2677 Color: 1
Size: 2276 Color: 0

Bin 142: 16 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 0
Size: 4301 Color: 3
Size: 1582 Color: 2

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 14697 Color: 4
Size: 1895 Color: 3

Bin 144: 17 of cap free
Amount of items: 3
Items: 
Size: 9483 Color: 4
Size: 5688 Color: 4
Size: 1420 Color: 0

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 13486 Color: 0
Size: 3105 Color: 3

Bin 146: 17 of cap free
Amount of items: 3
Items: 
Size: 14727 Color: 3
Size: 1768 Color: 0
Size: 96 Color: 3

Bin 147: 17 of cap free
Amount of items: 2
Items: 
Size: 14849 Color: 4
Size: 1742 Color: 2

Bin 148: 18 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 4
Size: 6914 Color: 3
Size: 488 Color: 1

Bin 149: 20 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 4
Size: 2091 Color: 0
Size: 1932 Color: 3

Bin 150: 21 of cap free
Amount of items: 2
Items: 
Size: 12860 Color: 2
Size: 3727 Color: 0

Bin 151: 21 of cap free
Amount of items: 2
Items: 
Size: 14091 Color: 0
Size: 2496 Color: 2

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 14765 Color: 2
Size: 1821 Color: 4

Bin 153: 24 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 0
Size: 2194 Color: 3

Bin 154: 24 of cap free
Amount of items: 2
Items: 
Size: 14596 Color: 2
Size: 1988 Color: 3

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 14740 Color: 2
Size: 1844 Color: 3

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 11086 Color: 4
Size: 5496 Color: 1

Bin 157: 26 of cap free
Amount of items: 2
Items: 
Size: 14152 Color: 3
Size: 2430 Color: 2

Bin 158: 28 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 0
Size: 2886 Color: 4

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 14056 Color: 2
Size: 2524 Color: 3

Bin 160: 28 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 2
Size: 2352 Color: 0

Bin 161: 29 of cap free
Amount of items: 2
Items: 
Size: 14119 Color: 4
Size: 2460 Color: 0

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 4
Size: 2126 Color: 3

Bin 163: 33 of cap free
Amount of items: 2
Items: 
Size: 12519 Color: 2
Size: 4056 Color: 1

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 10613 Color: 4
Size: 5959 Color: 1

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 14638 Color: 2
Size: 1934 Color: 0

Bin 166: 40 of cap free
Amount of items: 2
Items: 
Size: 14290 Color: 2
Size: 2278 Color: 3

Bin 167: 41 of cap free
Amount of items: 2
Items: 
Size: 12356 Color: 0
Size: 4211 Color: 1

Bin 168: 42 of cap free
Amount of items: 3
Items: 
Size: 9470 Color: 4
Size: 5292 Color: 0
Size: 1804 Color: 3

Bin 169: 45 of cap free
Amount of items: 2
Items: 
Size: 14335 Color: 4
Size: 2228 Color: 3

Bin 170: 46 of cap free
Amount of items: 3
Items: 
Size: 12846 Color: 1
Size: 3524 Color: 3
Size: 192 Color: 2

Bin 171: 51 of cap free
Amount of items: 2
Items: 
Size: 9635 Color: 2
Size: 6922 Color: 3

Bin 172: 56 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 3
Size: 4602 Color: 4
Size: 172 Color: 3

Bin 173: 62 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 0
Size: 2606 Color: 3

Bin 174: 63 of cap free
Amount of items: 2
Items: 
Size: 10442 Color: 4
Size: 6103 Color: 1

Bin 175: 68 of cap free
Amount of items: 2
Items: 
Size: 8316 Color: 4
Size: 8224 Color: 0

Bin 176: 71 of cap free
Amount of items: 2
Items: 
Size: 12137 Color: 2
Size: 4400 Color: 3

Bin 177: 81 of cap free
Amount of items: 2
Items: 
Size: 12270 Color: 3
Size: 4257 Color: 0

Bin 178: 82 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 0
Size: 5681 Color: 4
Size: 1382 Color: 1

Bin 179: 83 of cap free
Amount of items: 2
Items: 
Size: 12977 Color: 3
Size: 3548 Color: 1

Bin 180: 92 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 1
Size: 2856 Color: 0

Bin 181: 94 of cap free
Amount of items: 3
Items: 
Size: 9124 Color: 4
Size: 5942 Color: 3
Size: 1448 Color: 3

Bin 182: 104 of cap free
Amount of items: 2
Items: 
Size: 10260 Color: 3
Size: 6244 Color: 4

Bin 183: 106 of cap free
Amount of items: 3
Items: 
Size: 8330 Color: 4
Size: 7260 Color: 0
Size: 912 Color: 2

Bin 184: 106 of cap free
Amount of items: 2
Items: 
Size: 11501 Color: 3
Size: 5001 Color: 2

Bin 185: 111 of cap free
Amount of items: 2
Items: 
Size: 13329 Color: 3
Size: 3168 Color: 1

Bin 186: 114 of cap free
Amount of items: 2
Items: 
Size: 12466 Color: 2
Size: 4028 Color: 1

Bin 187: 122 of cap free
Amount of items: 2
Items: 
Size: 11344 Color: 4
Size: 5142 Color: 2

Bin 188: 136 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 0
Size: 3148 Color: 1

Bin 189: 144 of cap free
Amount of items: 2
Items: 
Size: 11116 Color: 1
Size: 5348 Color: 3

Bin 190: 166 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 1
Size: 5380 Color: 0
Size: 2740 Color: 3

Bin 191: 168 of cap free
Amount of items: 2
Items: 
Size: 10212 Color: 4
Size: 6228 Color: 1

Bin 192: 180 of cap free
Amount of items: 33
Items: 
Size: 688 Color: 4
Size: 680 Color: 0
Size: 624 Color: 1
Size: 620 Color: 2
Size: 592 Color: 3
Size: 576 Color: 3
Size: 572 Color: 1
Size: 560 Color: 2
Size: 552 Color: 4
Size: 546 Color: 4
Size: 546 Color: 2
Size: 544 Color: 2
Size: 534 Color: 2
Size: 528 Color: 4
Size: 528 Color: 3
Size: 468 Color: 2
Size: 464 Color: 0
Size: 452 Color: 1
Size: 448 Color: 0
Size: 448 Color: 0
Size: 440 Color: 4
Size: 440 Color: 4
Size: 440 Color: 2
Size: 440 Color: 0
Size: 420 Color: 3
Size: 418 Color: 3
Size: 418 Color: 0
Size: 416 Color: 0
Size: 414 Color: 0
Size: 412 Color: 0
Size: 408 Color: 2
Size: 400 Color: 1
Size: 392 Color: 3

Bin 193: 191 of cap free
Amount of items: 2
Items: 
Size: 9496 Color: 1
Size: 6921 Color: 2

Bin 194: 197 of cap free
Amount of items: 2
Items: 
Size: 8808 Color: 0
Size: 7603 Color: 2

Bin 195: 217 of cap free
Amount of items: 2
Items: 
Size: 9478 Color: 4
Size: 6913 Color: 1

Bin 196: 217 of cap free
Amount of items: 2
Items: 
Size: 9784 Color: 4
Size: 6607 Color: 3

Bin 197: 224 of cap free
Amount of items: 2
Items: 
Size: 8328 Color: 1
Size: 8056 Color: 3

Bin 198: 237 of cap free
Amount of items: 2
Items: 
Size: 9467 Color: 2
Size: 6904 Color: 1

Bin 199: 11894 of cap free
Amount of items: 15
Items: 
Size: 368 Color: 1
Size: 352 Color: 2
Size: 344 Color: 1
Size: 344 Color: 0
Size: 336 Color: 3
Size: 334 Color: 3
Size: 328 Color: 4
Size: 308 Color: 4
Size: 304 Color: 2
Size: 288 Color: 4
Size: 288 Color: 3
Size: 288 Color: 2
Size: 284 Color: 4
Size: 276 Color: 3
Size: 272 Color: 1

Total size: 3288384
Total free space: 16608

