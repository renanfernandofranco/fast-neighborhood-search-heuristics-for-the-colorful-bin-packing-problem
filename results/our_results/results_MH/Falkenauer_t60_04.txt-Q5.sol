Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 276 Color: 4
Size: 254 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 281 Color: 4
Size: 264 Color: 3
Size: 455 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 252 Color: 0
Size: 252 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 250 Color: 4
Size: 256 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 4
Size: 336 Color: 3
Size: 253 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 264 Color: 3
Size: 258 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 256 Color: 0
Size: 253 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 4
Size: 291 Color: 3
Size: 284 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 310 Color: 3
Size: 281 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 296 Color: 2
Size: 286 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 314 Color: 2
Size: 283 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 319 Color: 0
Size: 253 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 304 Color: 0
Size: 282 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 287 Color: 0
Size: 279 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 346 Color: 3
Size: 252 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 357 Color: 3
Size: 264 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 251 Color: 3
Size: 251 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 326 Color: 2
Size: 296 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 315 Color: 0
Size: 284 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 328 Color: 3
Size: 293 Color: 2

Total size: 20000
Total free space: 0

