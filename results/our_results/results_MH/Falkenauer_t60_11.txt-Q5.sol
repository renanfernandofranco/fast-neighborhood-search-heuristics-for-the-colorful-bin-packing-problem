Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 266 Color: 3
Size: 284 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 3
Size: 253 Color: 0
Size: 252 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 372 Color: 4
Size: 258 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 313 Color: 3
Size: 288 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 266 Color: 4
Size: 253 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 352 Color: 0
Size: 267 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 369 Color: 1
Size: 253 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 313 Color: 3
Size: 296 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 327 Color: 0
Size: 278 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 331 Color: 3
Size: 280 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 257 Color: 2
Size: 251 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 253 Color: 0
Size: 255 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 307 Color: 0
Size: 295 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 323 Color: 4
Size: 268 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 283 Color: 4
Size: 270 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 331 Color: 4
Size: 284 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 4
Size: 256 Color: 1
Size: 251 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 278 Color: 0
Size: 252 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 352 Color: 0
Size: 256 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 3
Size: 336 Color: 4
Size: 268 Color: 1

Total size: 20000
Total free space: 0

