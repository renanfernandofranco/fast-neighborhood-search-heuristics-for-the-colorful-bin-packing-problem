Capicity Bin: 1000001
Lower Bound: 4514

Bins used: 4515
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 745775 Color: 1
Size: 147963 Color: 9
Size: 106263 Color: 8

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 684373 Color: 6
Size: 315628 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 731900 Color: 18
Size: 141042 Color: 13
Size: 127059 Color: 10

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 559051 Color: 1
Size: 163268 Color: 14
Size: 154920 Color: 15
Size: 122762 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 735777 Color: 5
Size: 141553 Color: 19
Size: 122671 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 786538 Color: 8
Size: 111715 Color: 14
Size: 101748 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 777926 Color: 7
Size: 111285 Color: 6
Size: 110790 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 708119 Color: 4
Size: 164349 Color: 6
Size: 127533 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 520404 Color: 17
Size: 271770 Color: 0
Size: 207827 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 734541 Color: 15
Size: 144162 Color: 0
Size: 121298 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 643915 Color: 4
Size: 208185 Color: 10
Size: 147901 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 777600 Color: 16
Size: 113967 Color: 6
Size: 108434 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 713222 Color: 14
Size: 143900 Color: 9
Size: 142879 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 751706 Color: 2
Size: 129637 Color: 4
Size: 118658 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 694131 Color: 9
Size: 171055 Color: 6
Size: 134815 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 731029 Color: 1
Size: 140693 Color: 3
Size: 128279 Color: 2

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 606312 Color: 14
Size: 140556 Color: 1
Size: 140378 Color: 18
Size: 112755 Color: 6

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 584825 Color: 18
Size: 162725 Color: 3
Size: 150368 Color: 0
Size: 102083 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 531197 Color: 10
Size: 275223 Color: 18
Size: 193581 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 708127 Color: 8
Size: 166414 Color: 5
Size: 125460 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 775802 Color: 7
Size: 118754 Color: 16
Size: 105445 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 721715 Color: 16
Size: 141106 Color: 18
Size: 137180 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 706099 Color: 14
Size: 160714 Color: 11
Size: 133188 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 717356 Color: 17
Size: 167292 Color: 12
Size: 115353 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 767794 Color: 5
Size: 127864 Color: 17
Size: 104343 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 717532 Color: 11
Size: 155178 Color: 4
Size: 127291 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 608101 Color: 12
Size: 240570 Color: 8
Size: 151330 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 748415 Color: 6
Size: 131393 Color: 2
Size: 120193 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 757591 Color: 18
Size: 121908 Color: 16
Size: 120502 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 596393 Color: 14
Size: 237258 Color: 7
Size: 166350 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 697817 Color: 7
Size: 155182 Color: 14
Size: 147002 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 720740 Color: 10
Size: 157124 Color: 18
Size: 122137 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 692087 Color: 8
Size: 159516 Color: 3
Size: 148398 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 471132 Color: 4
Size: 266489 Color: 13
Size: 262380 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 778202 Color: 4
Size: 118085 Color: 15
Size: 103714 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 747434 Color: 11
Size: 132756 Color: 6
Size: 119811 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 670071 Color: 2
Size: 165717 Color: 1
Size: 164213 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 739333 Color: 4
Size: 132750 Color: 8
Size: 127918 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 651490 Color: 0
Size: 199263 Color: 9
Size: 149248 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 667796 Color: 18
Size: 167755 Color: 6
Size: 164450 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 700900 Color: 3
Size: 169408 Color: 1
Size: 129693 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 620677 Color: 16
Size: 231191 Color: 2
Size: 148133 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 682611 Color: 17
Size: 171453 Color: 11
Size: 145937 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 744261 Color: 10
Size: 132575 Color: 10
Size: 123165 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 596884 Color: 3
Size: 228490 Color: 6
Size: 174627 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 711956 Color: 1
Size: 167699 Color: 18
Size: 120346 Color: 11

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 706490 Color: 8
Size: 146837 Color: 6
Size: 146674 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 399364 Color: 17
Size: 328849 Color: 18
Size: 271788 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 697102 Color: 10
Size: 154561 Color: 10
Size: 148338 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 757587 Color: 19
Size: 133516 Color: 16
Size: 108898 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 748480 Color: 12
Size: 141857 Color: 3
Size: 109664 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 665611 Color: 9
Size: 176097 Color: 6
Size: 158293 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 707790 Color: 1
Size: 147957 Color: 5
Size: 144254 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 738595 Color: 0
Size: 131772 Color: 15
Size: 129634 Color: 15

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 727030 Color: 7
Size: 148490 Color: 17
Size: 124481 Color: 8

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 750122 Color: 5
Size: 130256 Color: 2
Size: 119623 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 750768 Color: 6
Size: 126679 Color: 5
Size: 122554 Color: 11

Bin 58: 0 of cap free
Amount of items: 4
Items: 
Size: 526922 Color: 6
Size: 213990 Color: 12
Size: 131681 Color: 7
Size: 127408 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 759452 Color: 6
Size: 132307 Color: 6
Size: 108242 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 765910 Color: 0
Size: 125851 Color: 15
Size: 108240 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 747849 Color: 13
Size: 127680 Color: 3
Size: 124472 Color: 14

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 679908 Color: 4
Size: 166779 Color: 1
Size: 153314 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 746817 Color: 18
Size: 141766 Color: 13
Size: 111418 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 665610 Color: 11
Size: 199556 Color: 16
Size: 134835 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 766212 Color: 10
Size: 122280 Color: 13
Size: 111509 Color: 17

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 643895 Color: 16
Size: 182285 Color: 6
Size: 173821 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 594057 Color: 11
Size: 222511 Color: 0
Size: 183433 Color: 10

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 537252 Color: 1
Size: 321458 Color: 9
Size: 141291 Color: 6

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 750639 Color: 1
Size: 126286 Color: 13
Size: 123076 Color: 10

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 372219 Color: 4
Size: 323038 Color: 15
Size: 304744 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 742343 Color: 6
Size: 140849 Color: 5
Size: 116809 Color: 9

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 721565 Color: 14
Size: 147950 Color: 0
Size: 130486 Color: 10

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 708870 Color: 4
Size: 156284 Color: 10
Size: 134847 Color: 3

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 712900 Color: 3
Size: 145464 Color: 9
Size: 141637 Color: 12

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 596582 Color: 18
Size: 289617 Color: 11
Size: 113802 Color: 11

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 644084 Color: 7
Size: 255678 Color: 10
Size: 100239 Color: 8

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 386135 Color: 17
Size: 311435 Color: 19
Size: 302431 Color: 18

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 747148 Color: 15
Size: 140710 Color: 16
Size: 112143 Color: 9

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 765348 Color: 17
Size: 122448 Color: 14
Size: 112205 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 762332 Color: 11
Size: 119958 Color: 14
Size: 117711 Color: 10

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 767461 Color: 2
Size: 126259 Color: 16
Size: 106281 Color: 19

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 692646 Color: 3
Size: 166220 Color: 13
Size: 141135 Color: 7

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 688097 Color: 18
Size: 170325 Color: 2
Size: 141579 Color: 5

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 689130 Color: 9
Size: 190925 Color: 8
Size: 119946 Color: 14

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 713351 Color: 3
Size: 177236 Color: 8
Size: 109414 Color: 9

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 620801 Color: 8
Size: 219662 Color: 6
Size: 159538 Color: 12

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 739206 Color: 7
Size: 142592 Color: 18
Size: 118203 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 758030 Color: 12
Size: 131728 Color: 12
Size: 110243 Color: 8

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 529847 Color: 6
Size: 470154 Color: 7

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 789449 Color: 3
Size: 106267 Color: 16
Size: 104285 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 539690 Color: 14
Size: 350762 Color: 2
Size: 109549 Color: 19

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 722455 Color: 1
Size: 158043 Color: 6
Size: 119503 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 486731 Color: 14
Size: 275965 Color: 19
Size: 237305 Color: 17

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 752562 Color: 6
Size: 135559 Color: 18
Size: 111880 Color: 13

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 558923 Color: 2
Size: 227782 Color: 14
Size: 213296 Color: 16

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 758551 Color: 10
Size: 130921 Color: 3
Size: 110529 Color: 19

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 659869 Color: 5
Size: 172078 Color: 18
Size: 168054 Color: 13

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 668029 Color: 16
Size: 172554 Color: 6
Size: 159418 Color: 17

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 643887 Color: 5
Size: 180401 Color: 4
Size: 175713 Color: 5

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 751337 Color: 7
Size: 131854 Color: 17
Size: 116810 Color: 19

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 547000 Color: 7
Size: 340394 Color: 1
Size: 112607 Color: 8

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 770164 Color: 2
Size: 117364 Color: 8
Size: 112473 Color: 11

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 711224 Color: 10
Size: 160390 Color: 18
Size: 128387 Color: 16

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 673823 Color: 18
Size: 165515 Color: 13
Size: 160663 Color: 19

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 669254 Color: 4
Size: 168679 Color: 14
Size: 162068 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 782698 Color: 10
Size: 114899 Color: 10
Size: 102404 Color: 15

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 751229 Color: 16
Size: 126319 Color: 15
Size: 122453 Color: 8

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 779090 Color: 8
Size: 119550 Color: 2
Size: 101361 Color: 5

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 734588 Color: 8
Size: 133158 Color: 4
Size: 132255 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 709768 Color: 9
Size: 148023 Color: 16
Size: 142210 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 727065 Color: 5
Size: 140134 Color: 2
Size: 132802 Color: 8

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 761948 Color: 19
Size: 134819 Color: 7
Size: 103234 Color: 7

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 720782 Color: 2
Size: 145943 Color: 9
Size: 133276 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 701507 Color: 18
Size: 149460 Color: 5
Size: 149034 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 622774 Color: 6
Size: 209080 Color: 4
Size: 168147 Color: 17

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 678994 Color: 10
Size: 193172 Color: 9
Size: 127835 Color: 13

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 682137 Color: 17
Size: 162455 Color: 17
Size: 155409 Color: 10

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 597930 Color: 19
Size: 272434 Color: 14
Size: 129637 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 596399 Color: 9
Size: 251043 Color: 9
Size: 152559 Color: 5

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 679907 Color: 8
Size: 200182 Color: 17
Size: 119912 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 646041 Color: 14
Size: 180431 Color: 13
Size: 173529 Color: 19

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 777979 Color: 12
Size: 117431 Color: 15
Size: 104591 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 778782 Color: 11
Size: 117627 Color: 13
Size: 103592 Color: 5

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 753309 Color: 18
Size: 126056 Color: 4
Size: 120636 Color: 16

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 667446 Color: 6
Size: 197339 Color: 6
Size: 135216 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 717154 Color: 13
Size: 143634 Color: 13
Size: 139213 Color: 9

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 774417 Color: 13
Size: 117160 Color: 0
Size: 108424 Color: 9

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 588691 Color: 16
Size: 273132 Color: 2
Size: 138178 Color: 8

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 748684 Color: 7
Size: 144629 Color: 0
Size: 106688 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 606177 Color: 6
Size: 219578 Color: 6
Size: 174246 Color: 16

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 747144 Color: 4
Size: 133236 Color: 14
Size: 119621 Color: 8

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 737368 Color: 18
Size: 137412 Color: 11
Size: 125221 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 717751 Color: 18
Size: 142672 Color: 1
Size: 139578 Color: 6

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 652003 Color: 6
Size: 174324 Color: 15
Size: 173674 Color: 17

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 754828 Color: 16
Size: 125037 Color: 17
Size: 120136 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 705005 Color: 0
Size: 150681 Color: 7
Size: 144315 Color: 15

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 674266 Color: 13
Size: 165132 Color: 7
Size: 160603 Color: 14

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 709422 Color: 7
Size: 150977 Color: 18
Size: 139602 Color: 5

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 664657 Color: 17
Size: 191624 Color: 15
Size: 143720 Color: 10

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 702440 Color: 5
Size: 157029 Color: 18
Size: 140532 Color: 5

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 731836 Color: 0
Size: 145254 Color: 6
Size: 122911 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 730285 Color: 11
Size: 135783 Color: 10
Size: 133933 Color: 13

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 643720 Color: 13
Size: 193847 Color: 0
Size: 162434 Color: 19

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 749609 Color: 6
Size: 127053 Color: 14
Size: 123339 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 542839 Color: 11
Size: 262721 Color: 11
Size: 194441 Color: 7

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 738629 Color: 10
Size: 143407 Color: 12
Size: 117965 Color: 14

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 604643 Color: 7
Size: 263360 Color: 10
Size: 131998 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 786214 Color: 10
Size: 111282 Color: 13
Size: 102505 Color: 5

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 643680 Color: 11
Size: 193857 Color: 0
Size: 162464 Color: 19

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 770665 Color: 8
Size: 118574 Color: 16
Size: 110762 Color: 14

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 679964 Color: 4
Size: 169004 Color: 17
Size: 151033 Color: 11

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 731282 Color: 15
Size: 140428 Color: 10
Size: 128291 Color: 17

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 639006 Color: 10
Size: 197147 Color: 2
Size: 163848 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 513993 Color: 3
Size: 256520 Color: 15
Size: 229488 Color: 5

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 490106 Color: 17
Size: 273284 Color: 2
Size: 236611 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 681385 Color: 3
Size: 166781 Color: 0
Size: 151835 Color: 17

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 516365 Color: 17
Size: 364603 Color: 19
Size: 119033 Color: 8

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 450239 Color: 9
Size: 317444 Color: 15
Size: 232318 Color: 16

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 654192 Color: 17
Size: 176079 Color: 14
Size: 169730 Color: 2

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 406557 Color: 15
Size: 346001 Color: 15
Size: 247443 Color: 9

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 623775 Color: 16
Size: 193834 Color: 9
Size: 182392 Color: 12

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 488724 Color: 7
Size: 349744 Color: 7
Size: 161533 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 513547 Color: 10
Size: 315310 Color: 0
Size: 171144 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 643719 Color: 4
Size: 201070 Color: 2
Size: 155212 Color: 8

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 643492 Color: 16
Size: 245027 Color: 4
Size: 111482 Color: 13

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 488511 Color: 13
Size: 302683 Color: 3
Size: 208807 Color: 12

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 488345 Color: 16
Size: 332298 Color: 6
Size: 179358 Color: 7

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 660535 Color: 5
Size: 183467 Color: 1
Size: 155999 Color: 14

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 664774 Color: 4
Size: 191020 Color: 5
Size: 144207 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 744107 Color: 13
Size: 144112 Color: 3
Size: 111782 Color: 5

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 708930 Color: 9
Size: 155645 Color: 12
Size: 135426 Color: 12

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 646034 Color: 7
Size: 179107 Color: 3
Size: 174860 Color: 8

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 489587 Color: 13
Size: 298527 Color: 10
Size: 211887 Color: 9

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 654673 Color: 14
Size: 180293 Color: 9
Size: 165035 Color: 11

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 511673 Color: 1
Size: 289732 Color: 1
Size: 198596 Color: 6

Bin 176: 0 of cap free
Amount of items: 4
Items: 
Size: 525783 Color: 0
Size: 250953 Color: 19
Size: 114692 Color: 17
Size: 108573 Color: 10

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 712208 Color: 9
Size: 159006 Color: 14
Size: 128787 Color: 14

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 642616 Color: 2
Size: 210747 Color: 11
Size: 146638 Color: 9

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 682880 Color: 16
Size: 168190 Color: 8
Size: 148931 Color: 8

Bin 180: 0 of cap free
Amount of items: 4
Items: 
Size: 489750 Color: 3
Size: 280761 Color: 6
Size: 116505 Color: 2
Size: 112985 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 622692 Color: 11
Size: 237944 Color: 14
Size: 139365 Color: 16

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 389309 Color: 1
Size: 308451 Color: 6
Size: 302241 Color: 13

Bin 183: 0 of cap free
Amount of items: 4
Items: 
Size: 488430 Color: 14
Size: 301671 Color: 8
Size: 105513 Color: 13
Size: 104387 Color: 15

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 789202 Color: 14
Size: 107434 Color: 0
Size: 103365 Color: 7

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 488330 Color: 12
Size: 376398 Color: 19
Size: 135273 Color: 2

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 411563 Color: 9
Size: 294304 Color: 15
Size: 294134 Color: 15

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 531804 Color: 13
Size: 286134 Color: 1
Size: 182063 Color: 13

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 650182 Color: 10
Size: 189840 Color: 17
Size: 159979 Color: 7

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 703489 Color: 3
Size: 165687 Color: 11
Size: 130825 Color: 18

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 679900 Color: 15
Size: 200274 Color: 14
Size: 119827 Color: 13

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 596320 Color: 10
Size: 201996 Color: 3
Size: 201685 Color: 19

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 642591 Color: 10
Size: 216020 Color: 2
Size: 141390 Color: 11

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 762318 Color: 5
Size: 126691 Color: 6
Size: 110992 Color: 16

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 722418 Color: 8
Size: 141110 Color: 1
Size: 136473 Color: 8

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 679880 Color: 11
Size: 207394 Color: 7
Size: 112727 Color: 5

Bin 196: 0 of cap free
Amount of items: 4
Items: 
Size: 489813 Color: 7
Size: 254394 Color: 19
Size: 132177 Color: 16
Size: 123617 Color: 10

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 746626 Color: 2
Size: 138498 Color: 13
Size: 114877 Color: 9

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 778566 Color: 14
Size: 115923 Color: 17
Size: 105512 Color: 7

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 769737 Color: 1
Size: 121446 Color: 3
Size: 108818 Color: 12

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 691442 Color: 3
Size: 161174 Color: 11
Size: 147385 Color: 5

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 596039 Color: 14
Size: 242777 Color: 14
Size: 161185 Color: 8

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 654087 Color: 15
Size: 178030 Color: 7
Size: 167884 Color: 4

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 560115 Color: 16
Size: 238804 Color: 18
Size: 201082 Color: 5

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 766288 Color: 14
Size: 117359 Color: 5
Size: 116354 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 489951 Color: 2
Size: 280905 Color: 15
Size: 229145 Color: 18

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 782304 Color: 0
Size: 112020 Color: 15
Size: 105677 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 776341 Color: 6
Size: 115151 Color: 9
Size: 108509 Color: 2

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 654256 Color: 15
Size: 173127 Color: 14
Size: 172618 Color: 17

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 786571 Color: 9
Size: 108068 Color: 19
Size: 105362 Color: 10

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 787487 Color: 1
Size: 107137 Color: 6
Size: 105377 Color: 15

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 663111 Color: 8
Size: 168511 Color: 16
Size: 168379 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 789984 Color: 6
Size: 109669 Color: 19
Size: 100348 Color: 11

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 609142 Color: 5
Size: 390859 Color: 16

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 789087 Color: 17
Size: 107777 Color: 16
Size: 103137 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 386600 Color: 5
Size: 320119 Color: 6
Size: 293282 Color: 16

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 781022 Color: 10
Size: 118867 Color: 5
Size: 100112 Color: 18

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 784356 Color: 2
Size: 110543 Color: 11
Size: 105102 Color: 15

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 774892 Color: 7
Size: 115765 Color: 17
Size: 109344 Color: 16

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 727562 Color: 14
Size: 136867 Color: 18
Size: 135572 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 783430 Color: 7
Size: 112965 Color: 8
Size: 103606 Color: 15

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 700213 Color: 6
Size: 160591 Color: 19
Size: 139197 Color: 6

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 788471 Color: 3
Size: 108490 Color: 5
Size: 103040 Color: 8

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 727369 Color: 8
Size: 155392 Color: 5
Size: 117240 Color: 3

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 766622 Color: 18
Size: 233379 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 635848 Color: 6
Size: 182081 Color: 8
Size: 182072 Color: 8

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 721452 Color: 5
Size: 142444 Color: 14
Size: 136105 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 795091 Color: 8
Size: 103982 Color: 12
Size: 100928 Color: 15

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 637181 Color: 11
Size: 362820 Color: 2

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 795399 Color: 18
Size: 103607 Color: 13
Size: 100995 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 518178 Color: 4
Size: 246459 Color: 19
Size: 235364 Color: 16

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 677597 Color: 17
Size: 322404 Color: 2

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 748222 Color: 14
Size: 147132 Color: 8
Size: 104647 Color: 12

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 682646 Color: 10
Size: 158916 Color: 17
Size: 158439 Color: 5

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 785428 Color: 17
Size: 107636 Color: 11
Size: 106937 Color: 11

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 778813 Color: 3
Size: 111592 Color: 4
Size: 109596 Color: 6

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 787536 Color: 16
Size: 212465 Color: 17

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 764909 Color: 14
Size: 134336 Color: 11
Size: 100756 Color: 8

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 777918 Color: 9
Size: 111719 Color: 18
Size: 110364 Color: 4

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 797120 Color: 11
Size: 101740 Color: 10
Size: 101141 Color: 14

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 665219 Color: 10
Size: 167427 Color: 11
Size: 167355 Color: 8

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 777130 Color: 11
Size: 111517 Color: 19
Size: 111354 Color: 8

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 685029 Color: 6
Size: 158809 Color: 2
Size: 156163 Color: 18

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 776144 Color: 14
Size: 113521 Color: 8
Size: 110336 Color: 3

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 787514 Color: 8
Size: 109655 Color: 4
Size: 102832 Color: 16

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 663327 Color: 19
Size: 168918 Color: 18
Size: 167756 Color: 3

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 779522 Color: 11
Size: 117441 Color: 5
Size: 103038 Color: 10

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 773233 Color: 8
Size: 116757 Color: 1
Size: 110011 Color: 7

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 527320 Color: 9
Size: 472681 Color: 13

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 773059 Color: 19
Size: 115220 Color: 10
Size: 111722 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 798825 Color: 6
Size: 100955 Color: 6
Size: 100221 Color: 13

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 779675 Color: 2
Size: 111818 Color: 15
Size: 108508 Color: 3

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 767714 Color: 4
Size: 120437 Color: 8
Size: 111850 Color: 16

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 794539 Color: 10
Size: 104004 Color: 1
Size: 101458 Color: 6

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 707879 Color: 6
Size: 292122 Color: 11

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 596789 Color: 0
Size: 300030 Color: 8
Size: 103182 Color: 3

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 599645 Color: 15
Size: 266067 Color: 6
Size: 134289 Color: 17

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 787831 Color: 14
Size: 108376 Color: 13
Size: 103794 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 610768 Color: 8
Size: 195273 Color: 4
Size: 193960 Color: 3

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 523979 Color: 6
Size: 476022 Color: 4

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 733282 Color: 17
Size: 149396 Color: 6
Size: 117323 Color: 19

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 782944 Color: 14
Size: 115671 Color: 16
Size: 101386 Color: 15

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 729151 Color: 11
Size: 270850 Color: 17

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 384915 Color: 4
Size: 362929 Color: 13
Size: 252157 Color: 18

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 370434 Color: 3
Size: 335180 Color: 19
Size: 294387 Color: 6

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 500789 Color: 17
Size: 259006 Color: 18
Size: 240206 Color: 14

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 794320 Color: 18
Size: 102980 Color: 10
Size: 102701 Color: 7

Bin 267: 0 of cap free
Amount of items: 4
Items: 
Size: 523790 Color: 19
Size: 259957 Color: 2
Size: 113384 Color: 14
Size: 102870 Color: 13

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 788748 Color: 18
Size: 110759 Color: 3
Size: 100494 Color: 17

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 519267 Color: 9
Size: 270834 Color: 13
Size: 209900 Color: 15

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 766675 Color: 15
Size: 233326 Color: 12

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 531660 Color: 17
Size: 265229 Color: 3
Size: 203112 Color: 4

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 641907 Color: 4
Size: 358094 Color: 6

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 731193 Color: 6
Size: 143082 Color: 3
Size: 125726 Color: 6

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 614059 Color: 9
Size: 385942 Color: 15

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 790336 Color: 15
Size: 107087 Color: 17
Size: 102578 Color: 3

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 590489 Color: 18
Size: 291774 Color: 5
Size: 117738 Color: 18

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 363035 Color: 6
Size: 331735 Color: 6
Size: 305231 Color: 11

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 386649 Color: 5
Size: 309523 Color: 2
Size: 303829 Color: 12

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 419306 Color: 16
Size: 293032 Color: 3
Size: 287663 Color: 5

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 502437 Color: 13
Size: 497564 Color: 10

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 508363 Color: 13
Size: 491638 Color: 2

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 511455 Color: 16
Size: 488546 Color: 0

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 514475 Color: 4
Size: 485526 Color: 11

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 517306 Color: 13
Size: 482695 Color: 11

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 525644 Color: 2
Size: 474357 Color: 9

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 531305 Color: 16
Size: 265600 Color: 12
Size: 203096 Color: 18

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 533704 Color: 8
Size: 466297 Color: 13

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 536175 Color: 2
Size: 258825 Color: 15
Size: 205001 Color: 5

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 543550 Color: 13
Size: 456451 Color: 1

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 554574 Color: 19
Size: 445427 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 561196 Color: 0
Size: 223500 Color: 12
Size: 215305 Color: 17

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 558830 Color: 6
Size: 441171 Color: 11

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 569201 Color: 17
Size: 430800 Color: 2

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 578087 Color: 9
Size: 421914 Color: 6

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 580043 Color: 0
Size: 419958 Color: 7

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 589762 Color: 17
Size: 410239 Color: 15

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 589089 Color: 6
Size: 208875 Color: 16
Size: 202037 Color: 18

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 590950 Color: 5
Size: 409051 Color: 15

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 600687 Color: 13
Size: 199665 Color: 4
Size: 199649 Color: 9

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 601609 Color: 10
Size: 199425 Color: 0
Size: 198967 Color: 12

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 608883 Color: 8
Size: 391118 Color: 12

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 617092 Color: 1
Size: 191700 Color: 10
Size: 191209 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 621973 Color: 8
Size: 189075 Color: 18
Size: 188953 Color: 18

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 624556 Color: 7
Size: 375445 Color: 17

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 630814 Color: 13
Size: 369187 Color: 4

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 637139 Color: 6
Size: 181578 Color: 13
Size: 181284 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 652623 Color: 3
Size: 175874 Color: 4
Size: 171504 Color: 6

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 653456 Color: 5
Size: 346545 Color: 3

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 666658 Color: 8
Size: 333343 Color: 18

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 673915 Color: 14
Size: 326086 Color: 4

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 674508 Color: 12
Size: 162809 Color: 19
Size: 162684 Color: 15

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 675225 Color: 5
Size: 162425 Color: 8
Size: 162351 Color: 11

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 688818 Color: 16
Size: 155854 Color: 14
Size: 155329 Color: 12

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 689170 Color: 6
Size: 155923 Color: 19
Size: 154908 Color: 10

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 689842 Color: 2
Size: 310159 Color: 12

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 690870 Color: 8
Size: 309131 Color: 9

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 693579 Color: 9
Size: 153527 Color: 16
Size: 152895 Color: 16

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 695610 Color: 1
Size: 304391 Color: 13

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 697395 Color: 8
Size: 302606 Color: 1

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 705439 Color: 5
Size: 294562 Color: 11

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 714202 Color: 17
Size: 285799 Color: 12

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 718828 Color: 5
Size: 281173 Color: 12

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 731556 Color: 17
Size: 268445 Color: 5

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 733798 Color: 2
Size: 266203 Color: 15

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 734912 Color: 14
Size: 265089 Color: 0

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 741471 Color: 11
Size: 258530 Color: 19

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 742128 Color: 3
Size: 128996 Color: 18
Size: 128877 Color: 16

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 748524 Color: 11
Size: 130860 Color: 16
Size: 120617 Color: 10

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 748614 Color: 13
Size: 127697 Color: 4
Size: 123690 Color: 8

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 748625 Color: 14
Size: 131029 Color: 13
Size: 120347 Color: 2

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 748769 Color: 4
Size: 127848 Color: 16
Size: 123384 Color: 10

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 750076 Color: 18
Size: 127400 Color: 13
Size: 122525 Color: 9

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 750589 Color: 4
Size: 129224 Color: 0
Size: 120188 Color: 15

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 752708 Color: 7
Size: 125317 Color: 17
Size: 121976 Color: 10

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 755252 Color: 13
Size: 244749 Color: 8

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 762362 Color: 6
Size: 118853 Color: 10
Size: 118786 Color: 5

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 765414 Color: 1
Size: 117380 Color: 18
Size: 117207 Color: 18

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 765672 Color: 10
Size: 234329 Color: 0

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 768739 Color: 8
Size: 231262 Color: 2

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 769544 Color: 4
Size: 115378 Color: 19
Size: 115079 Color: 9

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 770694 Color: 13
Size: 229307 Color: 4

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 773950 Color: 10
Size: 113133 Color: 8
Size: 112918 Color: 17

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 774861 Color: 15
Size: 112572 Color: 12
Size: 112568 Color: 13

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 776533 Color: 13
Size: 111778 Color: 10
Size: 111690 Color: 11

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 777311 Color: 1
Size: 222690 Color: 2

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 780582 Color: 19
Size: 110222 Color: 13
Size: 109197 Color: 3

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 780753 Color: 2
Size: 109672 Color: 7
Size: 109576 Color: 9

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 781471 Color: 16
Size: 111597 Color: 0
Size: 106933 Color: 12

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 781516 Color: 17
Size: 111393 Color: 18
Size: 107092 Color: 11

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 782436 Color: 0
Size: 109868 Color: 4
Size: 107697 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 783829 Color: 0
Size: 111889 Color: 8
Size: 104283 Color: 19

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 784502 Color: 11
Size: 110788 Color: 1
Size: 104711 Color: 12

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 785036 Color: 18
Size: 108943 Color: 9
Size: 106022 Color: 19

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 786253 Color: 12
Size: 110209 Color: 0
Size: 103539 Color: 13

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 786326 Color: 8
Size: 106957 Color: 9
Size: 106718 Color: 14

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 786602 Color: 5
Size: 108330 Color: 14
Size: 105069 Color: 4

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 788157 Color: 13
Size: 106337 Color: 9
Size: 105507 Color: 4

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 789806 Color: 12
Size: 105295 Color: 4
Size: 104900 Color: 19

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 790065 Color: 15
Size: 106957 Color: 11
Size: 102979 Color: 7

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 790196 Color: 6
Size: 105335 Color: 18
Size: 104470 Color: 18

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 790845 Color: 15
Size: 104804 Color: 3
Size: 104352 Color: 14

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 792111 Color: 2
Size: 207890 Color: 17

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 671171 Color: 15
Size: 170842 Color: 15
Size: 157987 Color: 12

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 769934 Color: 16
Size: 115058 Color: 16
Size: 115008 Color: 13

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 379821 Color: 4
Size: 326607 Color: 17
Size: 293572 Color: 10

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 622185 Color: 9
Size: 191170 Color: 16
Size: 186645 Color: 7

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 746900 Color: 6
Size: 139845 Color: 12
Size: 113255 Color: 4

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 788737 Color: 15
Size: 110008 Color: 9
Size: 101255 Color: 13

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 419556 Color: 0
Size: 293044 Color: 6
Size: 287400 Color: 15

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 635726 Color: 5
Size: 182605 Color: 10
Size: 181669 Color: 19

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 784896 Color: 6
Size: 110396 Color: 18
Size: 104708 Color: 17

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 623621 Color: 13
Size: 189072 Color: 10
Size: 187307 Color: 9

Bin 373: 1 of cap free
Amount of items: 2
Items: 
Size: 698535 Color: 14
Size: 301465 Color: 18

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 761197 Color: 18
Size: 136133 Color: 6
Size: 102670 Color: 12

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 684955 Color: 16
Size: 158406 Color: 13
Size: 156639 Color: 1

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 677196 Color: 18
Size: 200671 Color: 8
Size: 122133 Color: 5

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 748323 Color: 9
Size: 125974 Color: 12
Size: 125703 Color: 11

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 530800 Color: 8
Size: 267014 Color: 2
Size: 202186 Color: 12

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 702869 Color: 12
Size: 156768 Color: 12
Size: 140363 Color: 13

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 722102 Color: 9
Size: 145849 Color: 11
Size: 132049 Color: 0

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 528975 Color: 18
Size: 296818 Color: 1
Size: 174207 Color: 14

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 688122 Color: 7
Size: 156537 Color: 16
Size: 155341 Color: 3

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 618176 Color: 4
Size: 195725 Color: 4
Size: 186099 Color: 5

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 647111 Color: 3
Size: 176466 Color: 12
Size: 176423 Color: 3

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 686203 Color: 15
Size: 171145 Color: 16
Size: 142652 Color: 11

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 608706 Color: 12
Size: 196708 Color: 3
Size: 194586 Color: 2

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 664867 Color: 6
Size: 177191 Color: 10
Size: 157942 Color: 6

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 761497 Color: 8
Size: 119262 Color: 9
Size: 119241 Color: 17

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 724197 Color: 14
Size: 137937 Color: 13
Size: 137866 Color: 9

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 721994 Color: 14
Size: 141768 Color: 1
Size: 136238 Color: 8

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 534453 Color: 0
Size: 247115 Color: 6
Size: 218432 Color: 4

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 654373 Color: 14
Size: 177144 Color: 2
Size: 168483 Color: 13

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 631958 Color: 13
Size: 229858 Color: 13
Size: 138184 Color: 16

Bin 394: 1 of cap free
Amount of items: 4
Items: 
Size: 526653 Color: 8
Size: 259339 Color: 9
Size: 108320 Color: 14
Size: 105688 Color: 7

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 671590 Color: 4
Size: 181008 Color: 14
Size: 147402 Color: 8

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 737910 Color: 5
Size: 131259 Color: 2
Size: 130831 Color: 10

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 702427 Color: 18
Size: 153166 Color: 17
Size: 144407 Color: 2

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 643754 Color: 2
Size: 189602 Color: 10
Size: 166644 Color: 14

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 777993 Color: 19
Size: 111051 Color: 19
Size: 110956 Color: 10

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 702645 Color: 5
Size: 156757 Color: 11
Size: 140598 Color: 11

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 672809 Color: 10
Size: 163766 Color: 4
Size: 163425 Color: 4

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 525802 Color: 19
Size: 271039 Color: 9
Size: 203159 Color: 16

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 655344 Color: 11
Size: 173365 Color: 1
Size: 171291 Color: 7

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 718986 Color: 5
Size: 180818 Color: 12
Size: 100196 Color: 8

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 531931 Color: 15
Size: 238323 Color: 0
Size: 229746 Color: 18

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 513175 Color: 14
Size: 252295 Color: 13
Size: 234530 Color: 5

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 679958 Color: 4
Size: 174445 Color: 1
Size: 145597 Color: 4

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 611226 Color: 6
Size: 195007 Color: 7
Size: 193767 Color: 10

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 791825 Color: 6
Size: 107469 Color: 6
Size: 100706 Color: 3

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 744463 Color: 0
Size: 135564 Color: 10
Size: 119973 Color: 1

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 773548 Color: 9
Size: 113233 Color: 14
Size: 113219 Color: 13

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 660518 Color: 2
Size: 175165 Color: 19
Size: 164317 Color: 9

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 656371 Color: 9
Size: 171917 Color: 6
Size: 171712 Color: 8

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 655458 Color: 18
Size: 172462 Color: 12
Size: 172080 Color: 3

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 779055 Color: 15
Size: 112036 Color: 12
Size: 108909 Color: 5

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 684809 Color: 18
Size: 180118 Color: 17
Size: 135073 Color: 1

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 745278 Color: 16
Size: 128761 Color: 13
Size: 125961 Color: 10

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 516403 Color: 16
Size: 250456 Color: 7
Size: 233141 Color: 9

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 607445 Color: 16
Size: 198344 Color: 9
Size: 194211 Color: 11

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 597243 Color: 18
Size: 204119 Color: 1
Size: 198638 Color: 7

Bin 421: 1 of cap free
Amount of items: 2
Items: 
Size: 782168 Color: 0
Size: 217832 Color: 17

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 793308 Color: 5
Size: 104849 Color: 12
Size: 101843 Color: 8

Bin 423: 1 of cap free
Amount of items: 2
Items: 
Size: 641285 Color: 1
Size: 358715 Color: 16

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 412492 Color: 13
Size: 335983 Color: 7
Size: 251525 Color: 11

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 449793 Color: 4
Size: 298679 Color: 13
Size: 251528 Color: 1

Bin 426: 1 of cap free
Amount of items: 2
Items: 
Size: 508194 Color: 6
Size: 491806 Color: 5

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 710172 Color: 6
Size: 145859 Color: 17
Size: 143969 Color: 19

Bin 428: 1 of cap free
Amount of items: 2
Items: 
Size: 787878 Color: 6
Size: 212122 Color: 14

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 493266 Color: 11
Size: 272190 Color: 13
Size: 234544 Color: 13

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 680248 Color: 5
Size: 159878 Color: 15
Size: 159874 Color: 15

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 717319 Color: 4
Size: 142532 Color: 8
Size: 140149 Color: 19

Bin 432: 1 of cap free
Amount of items: 2
Items: 
Size: 766291 Color: 15
Size: 233709 Color: 5

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 527248 Color: 1
Size: 267869 Color: 2
Size: 204883 Color: 7

Bin 434: 1 of cap free
Amount of items: 2
Items: 
Size: 720679 Color: 19
Size: 279321 Color: 16

Bin 435: 1 of cap free
Amount of items: 2
Items: 
Size: 657299 Color: 9
Size: 342701 Color: 12

Bin 436: 1 of cap free
Amount of items: 2
Items: 
Size: 635375 Color: 11
Size: 364625 Color: 19

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 525945 Color: 10
Size: 239317 Color: 9
Size: 234738 Color: 10

Bin 438: 1 of cap free
Amount of items: 2
Items: 
Size: 713415 Color: 13
Size: 286585 Color: 12

Bin 439: 1 of cap free
Amount of items: 2
Items: 
Size: 621432 Color: 13
Size: 378568 Color: 5

Bin 440: 1 of cap free
Amount of items: 2
Items: 
Size: 519638 Color: 7
Size: 480362 Color: 6

Bin 441: 1 of cap free
Amount of items: 2
Items: 
Size: 640233 Color: 12
Size: 359767 Color: 17

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 364527 Color: 5
Size: 341405 Color: 15
Size: 294068 Color: 0

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 370341 Color: 17
Size: 337949 Color: 14
Size: 291710 Color: 10

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 383059 Color: 17
Size: 308787 Color: 1
Size: 308154 Color: 19

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 395666 Color: 15
Size: 310126 Color: 15
Size: 294208 Color: 9

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 413109 Color: 4
Size: 304095 Color: 10
Size: 282796 Color: 8

Bin 447: 1 of cap free
Amount of items: 2
Items: 
Size: 505764 Color: 10
Size: 494236 Color: 19

Bin 448: 1 of cap free
Amount of items: 2
Items: 
Size: 520089 Color: 12
Size: 479911 Color: 14

Bin 449: 1 of cap free
Amount of items: 2
Items: 
Size: 521838 Color: 19
Size: 478162 Color: 13

Bin 450: 1 of cap free
Amount of items: 2
Items: 
Size: 522660 Color: 16
Size: 477340 Color: 2

Bin 451: 1 of cap free
Amount of items: 2
Items: 
Size: 526556 Color: 6
Size: 473444 Color: 2

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 529588 Color: 19
Size: 261447 Color: 2
Size: 208965 Color: 12

Bin 453: 1 of cap free
Amount of items: 2
Items: 
Size: 531614 Color: 11
Size: 468386 Color: 14

Bin 454: 1 of cap free
Amount of items: 2
Items: 
Size: 534954 Color: 17
Size: 465046 Color: 7

Bin 455: 1 of cap free
Amount of items: 2
Items: 
Size: 548198 Color: 4
Size: 451802 Color: 17

Bin 456: 1 of cap free
Amount of items: 2
Items: 
Size: 551463 Color: 3
Size: 448537 Color: 1

Bin 457: 1 of cap free
Amount of items: 2
Items: 
Size: 551683 Color: 4
Size: 448317 Color: 13

Bin 458: 1 of cap free
Amount of items: 2
Items: 
Size: 555930 Color: 18
Size: 444070 Color: 3

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 561082 Color: 16
Size: 223555 Color: 15
Size: 215363 Color: 16

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 562084 Color: 4
Size: 219091 Color: 7
Size: 218825 Color: 13

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 562131 Color: 1
Size: 233508 Color: 7
Size: 204361 Color: 11

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 563261 Color: 3
Size: 224908 Color: 18
Size: 211831 Color: 8

Bin 463: 1 of cap free
Amount of items: 2
Items: 
Size: 564962 Color: 9
Size: 435038 Color: 1

Bin 464: 1 of cap free
Amount of items: 2
Items: 
Size: 566587 Color: 16
Size: 433413 Color: 3

Bin 465: 1 of cap free
Amount of items: 2
Items: 
Size: 579380 Color: 6
Size: 420620 Color: 9

Bin 466: 1 of cap free
Amount of items: 2
Items: 
Size: 597986 Color: 8
Size: 402014 Color: 10

Bin 467: 1 of cap free
Amount of items: 2
Items: 
Size: 598289 Color: 12
Size: 401711 Color: 19

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 603247 Color: 8
Size: 198469 Color: 7
Size: 198284 Color: 10

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 603251 Color: 3
Size: 198590 Color: 9
Size: 198159 Color: 9

Bin 470: 1 of cap free
Amount of items: 2
Items: 
Size: 604681 Color: 19
Size: 395319 Color: 15

Bin 471: 1 of cap free
Amount of items: 2
Items: 
Size: 612878 Color: 5
Size: 387122 Color: 4

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 613115 Color: 1
Size: 193571 Color: 1
Size: 193314 Color: 16

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 616226 Color: 8
Size: 192152 Color: 16
Size: 191622 Color: 1

Bin 474: 1 of cap free
Amount of items: 2
Items: 
Size: 631495 Color: 1
Size: 368505 Color: 13

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 633916 Color: 7
Size: 183068 Color: 13
Size: 183016 Color: 4

Bin 476: 1 of cap free
Amount of items: 2
Items: 
Size: 640708 Color: 17
Size: 359292 Color: 16

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 645064 Color: 10
Size: 177677 Color: 0
Size: 177259 Color: 17

Bin 478: 1 of cap free
Amount of items: 2
Items: 
Size: 653971 Color: 0
Size: 346029 Color: 2

Bin 479: 1 of cap free
Amount of items: 2
Items: 
Size: 667005 Color: 14
Size: 332995 Color: 3

Bin 480: 1 of cap free
Amount of items: 2
Items: 
Size: 682096 Color: 16
Size: 317904 Color: 2

Bin 481: 1 of cap free
Amount of items: 2
Items: 
Size: 684544 Color: 14
Size: 315456 Color: 15

Bin 482: 1 of cap free
Amount of items: 2
Items: 
Size: 685196 Color: 14
Size: 314804 Color: 12

Bin 483: 1 of cap free
Amount of items: 2
Items: 
Size: 687730 Color: 17
Size: 312270 Color: 4

Bin 484: 1 of cap free
Amount of items: 2
Items: 
Size: 687988 Color: 18
Size: 312012 Color: 16

Bin 485: 1 of cap free
Amount of items: 2
Items: 
Size: 706441 Color: 4
Size: 293559 Color: 5

Bin 486: 1 of cap free
Amount of items: 2
Items: 
Size: 710228 Color: 1
Size: 289772 Color: 3

Bin 487: 1 of cap free
Amount of items: 2
Items: 
Size: 722046 Color: 0
Size: 277954 Color: 5

Bin 488: 1 of cap free
Amount of items: 2
Items: 
Size: 729394 Color: 0
Size: 270606 Color: 15

Bin 489: 1 of cap free
Amount of items: 2
Items: 
Size: 729735 Color: 16
Size: 270265 Color: 5

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 747732 Color: 9
Size: 130931 Color: 14
Size: 121337 Color: 12

Bin 491: 1 of cap free
Amount of items: 2
Items: 
Size: 756129 Color: 2
Size: 243871 Color: 0

Bin 492: 1 of cap free
Amount of items: 2
Items: 
Size: 758557 Color: 17
Size: 241443 Color: 18

Bin 493: 1 of cap free
Amount of items: 2
Items: 
Size: 771911 Color: 9
Size: 228089 Color: 18

Bin 494: 1 of cap free
Amount of items: 2
Items: 
Size: 779264 Color: 16
Size: 220736 Color: 4

Bin 495: 1 of cap free
Amount of items: 2
Items: 
Size: 781389 Color: 15
Size: 218611 Color: 10

Bin 496: 1 of cap free
Amount of items: 2
Items: 
Size: 794030 Color: 1
Size: 205970 Color: 7

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 625964 Color: 18
Size: 187775 Color: 14
Size: 186260 Color: 13

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 697919 Color: 2
Size: 152995 Color: 1
Size: 149085 Color: 4

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 751186 Color: 5
Size: 127553 Color: 3
Size: 121260 Color: 13

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 713933 Color: 9
Size: 154456 Color: 7
Size: 131610 Color: 0

Bin 501: 2 of cap free
Amount of items: 2
Items: 
Size: 743545 Color: 13
Size: 256454 Color: 14

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 757001 Color: 17
Size: 133522 Color: 13
Size: 109476 Color: 12

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 397239 Color: 17
Size: 311588 Color: 10
Size: 291172 Color: 8

Bin 504: 2 of cap free
Amount of items: 2
Items: 
Size: 694852 Color: 13
Size: 305147 Color: 6

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 644143 Color: 6
Size: 196096 Color: 4
Size: 159760 Color: 1

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 715419 Color: 14
Size: 147705 Color: 18
Size: 136875 Color: 3

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 409769 Color: 10
Size: 302666 Color: 9
Size: 287564 Color: 7

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 685469 Color: 4
Size: 157679 Color: 16
Size: 156851 Color: 7

Bin 509: 2 of cap free
Amount of items: 2
Items: 
Size: 785061 Color: 14
Size: 214938 Color: 2

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 772646 Color: 12
Size: 113771 Color: 13
Size: 113582 Color: 19

Bin 511: 2 of cap free
Amount of items: 2
Items: 
Size: 733145 Color: 11
Size: 266854 Color: 6

Bin 512: 2 of cap free
Amount of items: 2
Items: 
Size: 747087 Color: 1
Size: 252912 Color: 17

Bin 513: 2 of cap free
Amount of items: 2
Items: 
Size: 730552 Color: 0
Size: 269447 Color: 9

Bin 514: 2 of cap free
Amount of items: 3
Items: 
Size: 753831 Color: 7
Size: 129282 Color: 17
Size: 116886 Color: 9

Bin 515: 2 of cap free
Amount of items: 3
Items: 
Size: 667441 Color: 9
Size: 190870 Color: 7
Size: 141688 Color: 18

Bin 516: 2 of cap free
Amount of items: 3
Items: 
Size: 746793 Color: 1
Size: 134859 Color: 1
Size: 118347 Color: 13

Bin 517: 2 of cap free
Amount of items: 3
Items: 
Size: 664971 Color: 18
Size: 171502 Color: 15
Size: 163526 Color: 5

Bin 518: 2 of cap free
Amount of items: 3
Items: 
Size: 784395 Color: 5
Size: 109666 Color: 4
Size: 105938 Color: 3

Bin 519: 2 of cap free
Amount of items: 3
Items: 
Size: 665617 Color: 3
Size: 174460 Color: 19
Size: 159922 Color: 17

Bin 520: 2 of cap free
Amount of items: 3
Items: 
Size: 601236 Color: 1
Size: 199459 Color: 3
Size: 199304 Color: 8

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 367961 Color: 3
Size: 333773 Color: 13
Size: 298265 Color: 1

Bin 522: 2 of cap free
Amount of items: 3
Items: 
Size: 415683 Color: 4
Size: 334588 Color: 5
Size: 249728 Color: 11

Bin 523: 2 of cap free
Amount of items: 3
Items: 
Size: 524536 Color: 7
Size: 371936 Color: 9
Size: 103527 Color: 0

Bin 524: 2 of cap free
Amount of items: 3
Items: 
Size: 419624 Color: 14
Size: 308039 Color: 15
Size: 272336 Color: 17

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 536317 Color: 16
Size: 308497 Color: 9
Size: 155185 Color: 8

Bin 526: 2 of cap free
Amount of items: 3
Items: 
Size: 689141 Color: 6
Size: 156501 Color: 19
Size: 154357 Color: 17

Bin 527: 2 of cap free
Amount of items: 3
Items: 
Size: 539086 Color: 0
Size: 308452 Color: 6
Size: 152461 Color: 11

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 670026 Color: 10
Size: 165096 Color: 2
Size: 164877 Color: 4

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 625474 Color: 17
Size: 189796 Color: 14
Size: 184729 Color: 17

Bin 530: 2 of cap free
Amount of items: 2
Items: 
Size: 706721 Color: 18
Size: 293278 Color: 8

Bin 531: 2 of cap free
Amount of items: 2
Items: 
Size: 774225 Color: 9
Size: 225774 Color: 14

Bin 532: 2 of cap free
Amount of items: 3
Items: 
Size: 616742 Color: 4
Size: 192040 Color: 8
Size: 191217 Color: 17

Bin 533: 2 of cap free
Amount of items: 2
Items: 
Size: 799578 Color: 5
Size: 200421 Color: 18

Bin 534: 2 of cap free
Amount of items: 2
Items: 
Size: 524866 Color: 18
Size: 475133 Color: 4

Bin 535: 2 of cap free
Amount of items: 3
Items: 
Size: 628447 Color: 1
Size: 186571 Color: 3
Size: 184981 Color: 17

Bin 536: 2 of cap free
Amount of items: 2
Items: 
Size: 703824 Color: 8
Size: 296175 Color: 0

Bin 537: 2 of cap free
Amount of items: 3
Items: 
Size: 469095 Color: 2
Size: 300393 Color: 2
Size: 230511 Color: 17

Bin 538: 2 of cap free
Amount of items: 3
Items: 
Size: 518155 Color: 10
Size: 260391 Color: 8
Size: 221453 Color: 15

Bin 539: 2 of cap free
Amount of items: 3
Items: 
Size: 419269 Color: 6
Size: 300128 Color: 2
Size: 280602 Color: 15

Bin 540: 2 of cap free
Amount of items: 3
Items: 
Size: 379702 Color: 2
Size: 321136 Color: 8
Size: 299161 Color: 0

Bin 541: 2 of cap free
Amount of items: 2
Items: 
Size: 648674 Color: 12
Size: 351325 Color: 0

Bin 542: 2 of cap free
Amount of items: 3
Items: 
Size: 358279 Color: 2
Size: 340483 Color: 16
Size: 301237 Color: 16

Bin 543: 2 of cap free
Amount of items: 3
Items: 
Size: 419085 Color: 3
Size: 311064 Color: 18
Size: 269850 Color: 13

Bin 544: 2 of cap free
Amount of items: 3
Items: 
Size: 442338 Color: 4
Size: 287106 Color: 1
Size: 270555 Color: 11

Bin 545: 2 of cap free
Amount of items: 2
Items: 
Size: 502328 Color: 7
Size: 497671 Color: 9

Bin 546: 2 of cap free
Amount of items: 2
Items: 
Size: 502926 Color: 10
Size: 497073 Color: 19

Bin 547: 2 of cap free
Amount of items: 2
Items: 
Size: 522426 Color: 6
Size: 477573 Color: 15

Bin 548: 2 of cap free
Amount of items: 3
Items: 
Size: 523822 Color: 0
Size: 268905 Color: 12
Size: 207272 Color: 5

Bin 549: 2 of cap free
Amount of items: 2
Items: 
Size: 523845 Color: 17
Size: 476154 Color: 10

Bin 550: 2 of cap free
Amount of items: 3
Items: 
Size: 526630 Color: 0
Size: 253698 Color: 12
Size: 219671 Color: 18

Bin 551: 2 of cap free
Amount of items: 2
Items: 
Size: 527935 Color: 5
Size: 472064 Color: 18

Bin 552: 2 of cap free
Amount of items: 2
Items: 
Size: 533981 Color: 9
Size: 466018 Color: 13

Bin 553: 2 of cap free
Amount of items: 3
Items: 
Size: 534116 Color: 5
Size: 235051 Color: 18
Size: 230832 Color: 0

Bin 554: 2 of cap free
Amount of items: 2
Items: 
Size: 542842 Color: 3
Size: 457157 Color: 7

Bin 555: 2 of cap free
Amount of items: 2
Items: 
Size: 553342 Color: 3
Size: 446657 Color: 7

Bin 556: 2 of cap free
Amount of items: 2
Items: 
Size: 558781 Color: 8
Size: 441218 Color: 0

Bin 557: 2 of cap free
Amount of items: 3
Items: 
Size: 560167 Color: 17
Size: 223325 Color: 10
Size: 216507 Color: 14

Bin 558: 2 of cap free
Amount of items: 2
Items: 
Size: 562713 Color: 7
Size: 437286 Color: 4

Bin 559: 2 of cap free
Amount of items: 2
Items: 
Size: 563265 Color: 17
Size: 436734 Color: 3

Bin 560: 2 of cap free
Amount of items: 2
Items: 
Size: 563980 Color: 8
Size: 436019 Color: 14

Bin 561: 2 of cap free
Amount of items: 2
Items: 
Size: 573094 Color: 10
Size: 426905 Color: 0

Bin 562: 2 of cap free
Amount of items: 2
Items: 
Size: 581611 Color: 4
Size: 418388 Color: 7

Bin 563: 2 of cap free
Amount of items: 2
Items: 
Size: 578821 Color: 6
Size: 421178 Color: 18

Bin 564: 2 of cap free
Amount of items: 2
Items: 
Size: 583634 Color: 11
Size: 416365 Color: 15

Bin 565: 2 of cap free
Amount of items: 2
Items: 
Size: 588635 Color: 4
Size: 411364 Color: 13

Bin 566: 2 of cap free
Amount of items: 2
Items: 
Size: 591385 Color: 19
Size: 408614 Color: 11

Bin 567: 2 of cap free
Amount of items: 2
Items: 
Size: 598046 Color: 18
Size: 401953 Color: 9

Bin 568: 2 of cap free
Amount of items: 2
Items: 
Size: 600188 Color: 4
Size: 399811 Color: 12

Bin 569: 2 of cap free
Amount of items: 2
Items: 
Size: 600718 Color: 4
Size: 399281 Color: 7

Bin 570: 2 of cap free
Amount of items: 3
Items: 
Size: 607378 Color: 7
Size: 196455 Color: 2
Size: 196166 Color: 5

Bin 571: 2 of cap free
Amount of items: 2
Items: 
Size: 607639 Color: 9
Size: 392360 Color: 3

Bin 572: 2 of cap free
Amount of items: 2
Items: 
Size: 608008 Color: 8
Size: 391991 Color: 2

Bin 573: 2 of cap free
Amount of items: 2
Items: 
Size: 609742 Color: 14
Size: 390257 Color: 18

Bin 574: 2 of cap free
Amount of items: 3
Items: 
Size: 628875 Color: 4
Size: 185677 Color: 8
Size: 185447 Color: 15

Bin 575: 2 of cap free
Amount of items: 2
Items: 
Size: 634277 Color: 3
Size: 365722 Color: 16

Bin 576: 2 of cap free
Amount of items: 3
Items: 
Size: 656411 Color: 5
Size: 172093 Color: 9
Size: 171495 Color: 17

Bin 577: 2 of cap free
Amount of items: 2
Items: 
Size: 663773 Color: 14
Size: 336226 Color: 12

Bin 578: 2 of cap free
Amount of items: 2
Items: 
Size: 666924 Color: 7
Size: 333075 Color: 19

Bin 579: 2 of cap free
Amount of items: 3
Items: 
Size: 670250 Color: 12
Size: 164875 Color: 7
Size: 164874 Color: 18

Bin 580: 2 of cap free
Amount of items: 3
Items: 
Size: 672764 Color: 12
Size: 163922 Color: 7
Size: 163313 Color: 9

Bin 581: 2 of cap free
Amount of items: 2
Items: 
Size: 682105 Color: 17
Size: 317894 Color: 12

Bin 582: 2 of cap free
Amount of items: 2
Items: 
Size: 683502 Color: 15
Size: 316497 Color: 11

Bin 583: 2 of cap free
Amount of items: 2
Items: 
Size: 695214 Color: 14
Size: 304785 Color: 12

Bin 584: 2 of cap free
Amount of items: 2
Items: 
Size: 699282 Color: 14
Size: 300717 Color: 11

Bin 585: 2 of cap free
Amount of items: 2
Items: 
Size: 699732 Color: 7
Size: 300267 Color: 17

Bin 586: 2 of cap free
Amount of items: 2
Items: 
Size: 704452 Color: 7
Size: 295547 Color: 19

Bin 587: 2 of cap free
Amount of items: 2
Items: 
Size: 708282 Color: 15
Size: 291717 Color: 7

Bin 588: 2 of cap free
Amount of items: 2
Items: 
Size: 714883 Color: 15
Size: 285116 Color: 9

Bin 589: 2 of cap free
Amount of items: 2
Items: 
Size: 717909 Color: 0
Size: 282090 Color: 16

Bin 590: 2 of cap free
Amount of items: 2
Items: 
Size: 739166 Color: 18
Size: 260833 Color: 13

Bin 591: 2 of cap free
Amount of items: 2
Items: 
Size: 749997 Color: 0
Size: 250002 Color: 14

Bin 592: 2 of cap free
Amount of items: 2
Items: 
Size: 750886 Color: 12
Size: 249113 Color: 15

Bin 593: 2 of cap free
Amount of items: 2
Items: 
Size: 752910 Color: 5
Size: 247089 Color: 1

Bin 594: 2 of cap free
Amount of items: 2
Items: 
Size: 756515 Color: 15
Size: 243484 Color: 4

Bin 595: 2 of cap free
Amount of items: 2
Items: 
Size: 757532 Color: 0
Size: 242467 Color: 2

Bin 596: 2 of cap free
Amount of items: 2
Items: 
Size: 783275 Color: 11
Size: 216724 Color: 8

Bin 597: 2 of cap free
Amount of items: 2
Items: 
Size: 794475 Color: 17
Size: 205524 Color: 0

Bin 598: 2 of cap free
Amount of items: 2
Items: 
Size: 799387 Color: 4
Size: 200612 Color: 12

Bin 599: 3 of cap free
Amount of items: 3
Items: 
Size: 657800 Color: 16
Size: 171128 Color: 15
Size: 171070 Color: 8

Bin 600: 3 of cap free
Amount of items: 3
Items: 
Size: 538376 Color: 2
Size: 287787 Color: 3
Size: 173835 Color: 4

Bin 601: 3 of cap free
Amount of items: 3
Items: 
Size: 518445 Color: 7
Size: 253519 Color: 16
Size: 228034 Color: 11

Bin 602: 3 of cap free
Amount of items: 3
Items: 
Size: 724306 Color: 16
Size: 139497 Color: 0
Size: 136195 Color: 7

Bin 603: 3 of cap free
Amount of items: 3
Items: 
Size: 789900 Color: 10
Size: 106513 Color: 5
Size: 103585 Color: 1

Bin 604: 3 of cap free
Amount of items: 2
Items: 
Size: 559847 Color: 2
Size: 440151 Color: 15

Bin 605: 3 of cap free
Amount of items: 3
Items: 
Size: 536180 Color: 9
Size: 243933 Color: 17
Size: 219885 Color: 17

Bin 606: 3 of cap free
Amount of items: 3
Items: 
Size: 695785 Color: 13
Size: 152110 Color: 13
Size: 152103 Color: 0

Bin 607: 3 of cap free
Amount of items: 3
Items: 
Size: 636250 Color: 0
Size: 182593 Color: 10
Size: 181155 Color: 1

Bin 608: 3 of cap free
Amount of items: 3
Items: 
Size: 620691 Color: 14
Size: 191537 Color: 2
Size: 187770 Color: 9

Bin 609: 3 of cap free
Amount of items: 3
Items: 
Size: 610245 Color: 11
Size: 196897 Color: 0
Size: 192856 Color: 10

Bin 610: 3 of cap free
Amount of items: 3
Items: 
Size: 721879 Color: 2
Size: 142035 Color: 3
Size: 136084 Color: 16

Bin 611: 3 of cap free
Amount of items: 3
Items: 
Size: 711616 Color: 7
Size: 161615 Color: 16
Size: 126767 Color: 2

Bin 612: 3 of cap free
Amount of items: 2
Items: 
Size: 737970 Color: 16
Size: 262028 Color: 13

Bin 613: 3 of cap free
Amount of items: 3
Items: 
Size: 657384 Color: 13
Size: 171328 Color: 18
Size: 171286 Color: 8

Bin 614: 3 of cap free
Amount of items: 2
Items: 
Size: 783294 Color: 1
Size: 216704 Color: 11

Bin 615: 3 of cap free
Amount of items: 3
Items: 
Size: 606930 Color: 6
Size: 219544 Color: 10
Size: 173524 Color: 5

Bin 616: 3 of cap free
Amount of items: 3
Items: 
Size: 713140 Color: 17
Size: 153891 Color: 0
Size: 132967 Color: 6

Bin 617: 3 of cap free
Amount of items: 3
Items: 
Size: 377743 Color: 5
Size: 311376 Color: 8
Size: 310879 Color: 7

Bin 618: 3 of cap free
Amount of items: 2
Items: 
Size: 747841 Color: 17
Size: 252157 Color: 15

Bin 619: 3 of cap free
Amount of items: 3
Items: 
Size: 621543 Color: 5
Size: 190261 Color: 0
Size: 188194 Color: 5

Bin 620: 3 of cap free
Amount of items: 3
Items: 
Size: 493021 Color: 1
Size: 332728 Color: 6
Size: 174249 Color: 4

Bin 621: 3 of cap free
Amount of items: 3
Items: 
Size: 647411 Color: 0
Size: 177161 Color: 6
Size: 175426 Color: 18

Bin 622: 3 of cap free
Amount of items: 3
Items: 
Size: 616731 Color: 19
Size: 192321 Color: 6
Size: 190946 Color: 11

Bin 623: 3 of cap free
Amount of items: 3
Items: 
Size: 788865 Color: 0
Size: 106039 Color: 15
Size: 105094 Color: 10

Bin 624: 3 of cap free
Amount of items: 3
Items: 
Size: 441093 Color: 2
Size: 285643 Color: 4
Size: 273262 Color: 3

Bin 625: 3 of cap free
Amount of items: 3
Items: 
Size: 678920 Color: 13
Size: 187951 Color: 12
Size: 133127 Color: 3

Bin 626: 3 of cap free
Amount of items: 3
Items: 
Size: 406552 Color: 17
Size: 301391 Color: 12
Size: 292055 Color: 19

Bin 627: 3 of cap free
Amount of items: 3
Items: 
Size: 673958 Color: 19
Size: 181227 Color: 4
Size: 144813 Color: 6

Bin 628: 3 of cap free
Amount of items: 3
Items: 
Size: 492308 Color: 4
Size: 261802 Color: 16
Size: 245888 Color: 16

Bin 629: 3 of cap free
Amount of items: 3
Items: 
Size: 564340 Color: 19
Size: 236956 Color: 8
Size: 198702 Color: 2

Bin 630: 3 of cap free
Amount of items: 3
Items: 
Size: 646196 Color: 15
Size: 179247 Color: 19
Size: 174555 Color: 18

Bin 631: 3 of cap free
Amount of items: 2
Items: 
Size: 676974 Color: 3
Size: 323024 Color: 4

Bin 632: 3 of cap free
Amount of items: 3
Items: 
Size: 662051 Color: 6
Size: 170200 Color: 13
Size: 167747 Color: 10

Bin 633: 3 of cap free
Amount of items: 2
Items: 
Size: 509903 Color: 4
Size: 490095 Color: 2

Bin 634: 3 of cap free
Amount of items: 3
Items: 
Size: 632608 Color: 1
Size: 185882 Color: 9
Size: 181508 Color: 8

Bin 635: 3 of cap free
Amount of items: 2
Items: 
Size: 644950 Color: 7
Size: 355048 Color: 6

Bin 636: 3 of cap free
Amount of items: 3
Items: 
Size: 634165 Color: 9
Size: 183403 Color: 19
Size: 182430 Color: 0

Bin 637: 3 of cap free
Amount of items: 3
Items: 
Size: 395934 Color: 9
Size: 335241 Color: 16
Size: 268823 Color: 11

Bin 638: 3 of cap free
Amount of items: 2
Items: 
Size: 788728 Color: 12
Size: 211270 Color: 0

Bin 639: 3 of cap free
Amount of items: 2
Items: 
Size: 772756 Color: 13
Size: 227242 Color: 11

Bin 640: 3 of cap free
Amount of items: 2
Items: 
Size: 680769 Color: 14
Size: 319229 Color: 7

Bin 641: 3 of cap free
Amount of items: 3
Items: 
Size: 404601 Color: 11
Size: 302391 Color: 18
Size: 293006 Color: 8

Bin 642: 3 of cap free
Amount of items: 3
Items: 
Size: 619964 Color: 7
Size: 190122 Color: 11
Size: 189912 Color: 16

Bin 643: 3 of cap free
Amount of items: 2
Items: 
Size: 750747 Color: 8
Size: 249251 Color: 9

Bin 644: 3 of cap free
Amount of items: 2
Items: 
Size: 501222 Color: 19
Size: 498776 Color: 5

Bin 645: 3 of cap free
Amount of items: 2
Items: 
Size: 703482 Color: 6
Size: 296516 Color: 11

Bin 646: 3 of cap free
Amount of items: 2
Items: 
Size: 756384 Color: 14
Size: 243614 Color: 18

Bin 647: 3 of cap free
Amount of items: 2
Items: 
Size: 505231 Color: 18
Size: 494767 Color: 9

Bin 648: 3 of cap free
Amount of items: 3
Items: 
Size: 446392 Color: 9
Size: 302005 Color: 10
Size: 251601 Color: 14

Bin 649: 3 of cap free
Amount of items: 3
Items: 
Size: 448425 Color: 9
Size: 281584 Color: 2
Size: 269989 Color: 18

Bin 650: 3 of cap free
Amount of items: 3
Items: 
Size: 492437 Color: 15
Size: 280615 Color: 2
Size: 226946 Color: 18

Bin 651: 3 of cap free
Amount of items: 2
Items: 
Size: 507320 Color: 12
Size: 492678 Color: 8

Bin 652: 3 of cap free
Amount of items: 3
Items: 
Size: 511954 Color: 3
Size: 269296 Color: 19
Size: 218748 Color: 18

Bin 653: 3 of cap free
Amount of items: 2
Items: 
Size: 512793 Color: 8
Size: 487205 Color: 16

Bin 654: 3 of cap free
Amount of items: 2
Items: 
Size: 524237 Color: 4
Size: 475761 Color: 16

Bin 655: 3 of cap free
Amount of items: 3
Items: 
Size: 527802 Color: 4
Size: 268250 Color: 6
Size: 203946 Color: 14

Bin 656: 3 of cap free
Amount of items: 3
Items: 
Size: 535590 Color: 10
Size: 250519 Color: 10
Size: 213889 Color: 7

Bin 657: 3 of cap free
Amount of items: 2
Items: 
Size: 535947 Color: 9
Size: 464051 Color: 12

Bin 658: 3 of cap free
Amount of items: 3
Items: 
Size: 538612 Color: 3
Size: 249184 Color: 15
Size: 212202 Color: 0

Bin 659: 3 of cap free
Amount of items: 2
Items: 
Size: 542487 Color: 13
Size: 457511 Color: 8

Bin 660: 3 of cap free
Amount of items: 2
Items: 
Size: 543309 Color: 12
Size: 456689 Color: 15

Bin 661: 3 of cap free
Amount of items: 2
Items: 
Size: 547966 Color: 10
Size: 452032 Color: 0

Bin 662: 3 of cap free
Amount of items: 2
Items: 
Size: 557518 Color: 16
Size: 442480 Color: 14

Bin 663: 3 of cap free
Amount of items: 2
Items: 
Size: 580066 Color: 7
Size: 419932 Color: 8

Bin 664: 3 of cap free
Amount of items: 2
Items: 
Size: 590473 Color: 12
Size: 409525 Color: 18

Bin 665: 3 of cap free
Amount of items: 2
Items: 
Size: 594189 Color: 1
Size: 405809 Color: 6

Bin 666: 3 of cap free
Amount of items: 2
Items: 
Size: 604695 Color: 19
Size: 395303 Color: 7

Bin 667: 3 of cap free
Amount of items: 2
Items: 
Size: 620114 Color: 19
Size: 379884 Color: 15

Bin 668: 3 of cap free
Amount of items: 2
Items: 
Size: 623610 Color: 5
Size: 376388 Color: 14

Bin 669: 3 of cap free
Amount of items: 2
Items: 
Size: 631137 Color: 11
Size: 368861 Color: 9

Bin 670: 3 of cap free
Amount of items: 2
Items: 
Size: 633062 Color: 4
Size: 366936 Color: 15

Bin 671: 3 of cap free
Amount of items: 2
Items: 
Size: 639994 Color: 13
Size: 360004 Color: 18

Bin 672: 3 of cap free
Amount of items: 2
Items: 
Size: 658628 Color: 12
Size: 341370 Color: 11

Bin 673: 3 of cap free
Amount of items: 3
Items: 
Size: 671090 Color: 6
Size: 164934 Color: 2
Size: 163974 Color: 13

Bin 674: 3 of cap free
Amount of items: 2
Items: 
Size: 672844 Color: 15
Size: 327154 Color: 16

Bin 675: 3 of cap free
Amount of items: 2
Items: 
Size: 674203 Color: 0
Size: 325795 Color: 14

Bin 676: 3 of cap free
Amount of items: 2
Items: 
Size: 677529 Color: 18
Size: 322469 Color: 5

Bin 677: 3 of cap free
Amount of items: 3
Items: 
Size: 724285 Color: 19
Size: 138415 Color: 2
Size: 137298 Color: 10

Bin 678: 3 of cap free
Amount of items: 2
Items: 
Size: 734941 Color: 13
Size: 265057 Color: 19

Bin 679: 3 of cap free
Amount of items: 2
Items: 
Size: 737812 Color: 5
Size: 262186 Color: 15

Bin 680: 3 of cap free
Amount of items: 2
Items: 
Size: 743121 Color: 3
Size: 256877 Color: 9

Bin 681: 3 of cap free
Amount of items: 2
Items: 
Size: 744582 Color: 13
Size: 255416 Color: 11

Bin 682: 3 of cap free
Amount of items: 2
Items: 
Size: 767253 Color: 12
Size: 232745 Color: 13

Bin 683: 3 of cap free
Amount of items: 2
Items: 
Size: 773354 Color: 7
Size: 226644 Color: 11

Bin 684: 3 of cap free
Amount of items: 3
Items: 
Size: 785539 Color: 11
Size: 109331 Color: 1
Size: 105128 Color: 11

Bin 685: 3 of cap free
Amount of items: 2
Items: 
Size: 786127 Color: 15
Size: 213871 Color: 12

Bin 686: 4 of cap free
Amount of items: 3
Items: 
Size: 747636 Color: 16
Size: 133062 Color: 18
Size: 119299 Color: 14

Bin 687: 4 of cap free
Amount of items: 3
Items: 
Size: 379404 Color: 3
Size: 360228 Color: 18
Size: 260365 Color: 15

Bin 688: 4 of cap free
Amount of items: 3
Items: 
Size: 651829 Color: 7
Size: 196356 Color: 6
Size: 151812 Color: 17

Bin 689: 4 of cap free
Amount of items: 2
Items: 
Size: 791844 Color: 2
Size: 208153 Color: 15

Bin 690: 4 of cap free
Amount of items: 3
Items: 
Size: 495575 Color: 0
Size: 269572 Color: 1
Size: 234850 Color: 9

Bin 691: 4 of cap free
Amount of items: 3
Items: 
Size: 627243 Color: 6
Size: 189096 Color: 7
Size: 183658 Color: 7

Bin 692: 4 of cap free
Amount of items: 3
Items: 
Size: 702751 Color: 7
Size: 157020 Color: 7
Size: 140226 Color: 1

Bin 693: 4 of cap free
Amount of items: 3
Items: 
Size: 684733 Color: 8
Size: 160181 Color: 9
Size: 155083 Color: 13

Bin 694: 4 of cap free
Amount of items: 3
Items: 
Size: 383845 Color: 5
Size: 311402 Color: 3
Size: 304750 Color: 6

Bin 695: 4 of cap free
Amount of items: 3
Items: 
Size: 747477 Color: 13
Size: 128712 Color: 8
Size: 123808 Color: 9

Bin 696: 4 of cap free
Amount of items: 3
Items: 
Size: 646880 Color: 11
Size: 183121 Color: 14
Size: 169996 Color: 16

Bin 697: 4 of cap free
Amount of items: 3
Items: 
Size: 514080 Color: 1
Size: 254076 Color: 2
Size: 231841 Color: 1

Bin 698: 4 of cap free
Amount of items: 3
Items: 
Size: 668684 Color: 7
Size: 166179 Color: 14
Size: 165134 Color: 6

Bin 699: 4 of cap free
Amount of items: 3
Items: 
Size: 535154 Color: 11
Size: 239941 Color: 1
Size: 224902 Color: 5

Bin 700: 4 of cap free
Amount of items: 3
Items: 
Size: 696383 Color: 10
Size: 182871 Color: 18
Size: 120743 Color: 12

Bin 701: 4 of cap free
Amount of items: 3
Items: 
Size: 526927 Color: 14
Size: 276353 Color: 17
Size: 196717 Color: 5

Bin 702: 4 of cap free
Amount of items: 3
Items: 
Size: 533927 Color: 2
Size: 250769 Color: 13
Size: 215301 Color: 19

Bin 703: 4 of cap free
Amount of items: 3
Items: 
Size: 709816 Color: 3
Size: 145623 Color: 18
Size: 144558 Color: 4

Bin 704: 4 of cap free
Amount of items: 3
Items: 
Size: 766671 Color: 7
Size: 116765 Color: 11
Size: 116561 Color: 4

Bin 705: 4 of cap free
Amount of items: 3
Items: 
Size: 714660 Color: 14
Size: 142849 Color: 12
Size: 142488 Color: 17

Bin 706: 4 of cap free
Amount of items: 3
Items: 
Size: 516711 Color: 12
Size: 252982 Color: 9
Size: 230304 Color: 12

Bin 707: 4 of cap free
Amount of items: 3
Items: 
Size: 655488 Color: 2
Size: 175935 Color: 11
Size: 168574 Color: 6

Bin 708: 4 of cap free
Amount of items: 2
Items: 
Size: 680740 Color: 2
Size: 319257 Color: 9

Bin 709: 4 of cap free
Amount of items: 3
Items: 
Size: 688541 Color: 18
Size: 156030 Color: 2
Size: 155426 Color: 14

Bin 710: 4 of cap free
Amount of items: 3
Items: 
Size: 370489 Color: 18
Size: 336255 Color: 17
Size: 293253 Color: 17

Bin 711: 4 of cap free
Amount of items: 2
Items: 
Size: 632974 Color: 12
Size: 367023 Color: 15

Bin 712: 4 of cap free
Amount of items: 3
Items: 
Size: 643851 Color: 13
Size: 187209 Color: 5
Size: 168937 Color: 5

Bin 713: 4 of cap free
Amount of items: 3
Items: 
Size: 632548 Color: 14
Size: 186327 Color: 13
Size: 181122 Color: 18

Bin 714: 4 of cap free
Amount of items: 2
Items: 
Size: 779078 Color: 2
Size: 220919 Color: 11

Bin 715: 4 of cap free
Amount of items: 3
Items: 
Size: 653375 Color: 5
Size: 176172 Color: 14
Size: 170450 Color: 1

Bin 716: 4 of cap free
Amount of items: 3
Items: 
Size: 644179 Color: 8
Size: 178104 Color: 15
Size: 177714 Color: 8

Bin 717: 4 of cap free
Amount of items: 2
Items: 
Size: 637358 Color: 9
Size: 362639 Color: 12

Bin 718: 4 of cap free
Amount of items: 2
Items: 
Size: 753198 Color: 3
Size: 246799 Color: 14

Bin 719: 4 of cap free
Amount of items: 2
Items: 
Size: 767221 Color: 12
Size: 232776 Color: 13

Bin 720: 4 of cap free
Amount of items: 2
Items: 
Size: 770331 Color: 0
Size: 229666 Color: 18

Bin 721: 4 of cap free
Amount of items: 2
Items: 
Size: 618158 Color: 16
Size: 381839 Color: 6

Bin 722: 4 of cap free
Amount of items: 2
Items: 
Size: 545647 Color: 19
Size: 454350 Color: 9

Bin 723: 4 of cap free
Amount of items: 2
Items: 
Size: 547475 Color: 2
Size: 452522 Color: 8

Bin 724: 4 of cap free
Amount of items: 2
Items: 
Size: 558355 Color: 11
Size: 441642 Color: 16

Bin 725: 4 of cap free
Amount of items: 2
Items: 
Size: 581542 Color: 9
Size: 418455 Color: 3

Bin 726: 4 of cap free
Amount of items: 2
Items: 
Size: 584285 Color: 10
Size: 415712 Color: 1

Bin 727: 4 of cap free
Amount of items: 2
Items: 
Size: 602859 Color: 19
Size: 397138 Color: 10

Bin 728: 4 of cap free
Amount of items: 2
Items: 
Size: 603529 Color: 3
Size: 396468 Color: 7

Bin 729: 4 of cap free
Amount of items: 2
Items: 
Size: 611250 Color: 1
Size: 388747 Color: 5

Bin 730: 4 of cap free
Amount of items: 2
Items: 
Size: 618626 Color: 17
Size: 381371 Color: 3

Bin 731: 4 of cap free
Amount of items: 2
Items: 
Size: 626259 Color: 4
Size: 373738 Color: 12

Bin 732: 4 of cap free
Amount of items: 3
Items: 
Size: 627211 Color: 8
Size: 186468 Color: 15
Size: 186318 Color: 18

Bin 733: 4 of cap free
Amount of items: 2
Items: 
Size: 628185 Color: 0
Size: 371812 Color: 18

Bin 734: 4 of cap free
Amount of items: 2
Items: 
Size: 630432 Color: 3
Size: 369565 Color: 19

Bin 735: 4 of cap free
Amount of items: 3
Items: 
Size: 636567 Color: 14
Size: 182308 Color: 3
Size: 181122 Color: 18

Bin 736: 4 of cap free
Amount of items: 2
Items: 
Size: 649152 Color: 15
Size: 350845 Color: 7

Bin 737: 4 of cap free
Amount of items: 2
Items: 
Size: 650471 Color: 19
Size: 349526 Color: 12

Bin 738: 4 of cap free
Amount of items: 2
Items: 
Size: 664048 Color: 10
Size: 335949 Color: 16

Bin 739: 4 of cap free
Amount of items: 3
Items: 
Size: 664059 Color: 7
Size: 168063 Color: 13
Size: 167875 Color: 8

Bin 740: 4 of cap free
Amount of items: 2
Items: 
Size: 669123 Color: 2
Size: 330874 Color: 19

Bin 741: 4 of cap free
Amount of items: 2
Items: 
Size: 684072 Color: 8
Size: 315925 Color: 17

Bin 742: 4 of cap free
Amount of items: 3
Items: 
Size: 689708 Color: 13
Size: 155199 Color: 7
Size: 155090 Color: 9

Bin 743: 4 of cap free
Amount of items: 2
Items: 
Size: 695024 Color: 14
Size: 304973 Color: 9

Bin 744: 4 of cap free
Amount of items: 2
Items: 
Size: 699727 Color: 12
Size: 300270 Color: 17

Bin 745: 4 of cap free
Amount of items: 2
Items: 
Size: 708344 Color: 3
Size: 291653 Color: 19

Bin 746: 4 of cap free
Amount of items: 2
Items: 
Size: 720177 Color: 7
Size: 279820 Color: 11

Bin 747: 4 of cap free
Amount of items: 2
Items: 
Size: 726651 Color: 7
Size: 273346 Color: 19

Bin 748: 4 of cap free
Amount of items: 2
Items: 
Size: 729316 Color: 13
Size: 270681 Color: 15

Bin 749: 4 of cap free
Amount of items: 2
Items: 
Size: 738259 Color: 18
Size: 261738 Color: 13

Bin 750: 4 of cap free
Amount of items: 3
Items: 
Size: 751193 Color: 3
Size: 129034 Color: 10
Size: 119770 Color: 17

Bin 751: 4 of cap free
Amount of items: 2
Items: 
Size: 751237 Color: 19
Size: 248760 Color: 16

Bin 752: 4 of cap free
Amount of items: 2
Items: 
Size: 755852 Color: 2
Size: 244145 Color: 9

Bin 753: 4 of cap free
Amount of items: 2
Items: 
Size: 756206 Color: 14
Size: 243791 Color: 13

Bin 754: 4 of cap free
Amount of items: 2
Items: 
Size: 768393 Color: 10
Size: 231604 Color: 9

Bin 755: 4 of cap free
Amount of items: 2
Items: 
Size: 787766 Color: 10
Size: 212231 Color: 15

Bin 756: 5 of cap free
Amount of items: 2
Items: 
Size: 731669 Color: 6
Size: 268327 Color: 2

Bin 757: 5 of cap free
Amount of items: 2
Items: 
Size: 678400 Color: 16
Size: 321596 Color: 3

Bin 758: 5 of cap free
Amount of items: 3
Items: 
Size: 533969 Color: 12
Size: 351520 Color: 17
Size: 114507 Color: 5

Bin 759: 5 of cap free
Amount of items: 3
Items: 
Size: 488434 Color: 2
Size: 273012 Color: 3
Size: 238550 Color: 7

Bin 760: 5 of cap free
Amount of items: 2
Items: 
Size: 507528 Color: 12
Size: 492468 Color: 17

Bin 761: 5 of cap free
Amount of items: 3
Items: 
Size: 695136 Color: 10
Size: 152530 Color: 1
Size: 152330 Color: 4

Bin 762: 5 of cap free
Amount of items: 3
Items: 
Size: 752862 Color: 0
Size: 134421 Color: 15
Size: 112713 Color: 9

Bin 763: 5 of cap free
Amount of items: 4
Items: 
Size: 354160 Color: 1
Size: 302276 Color: 7
Size: 171964 Color: 12
Size: 171596 Color: 5

Bin 764: 5 of cap free
Amount of items: 2
Items: 
Size: 518357 Color: 5
Size: 481639 Color: 17

Bin 765: 5 of cap free
Amount of items: 3
Items: 
Size: 666816 Color: 10
Size: 166680 Color: 6
Size: 166500 Color: 10

Bin 766: 5 of cap free
Amount of items: 3
Items: 
Size: 368036 Color: 3
Size: 344414 Color: 18
Size: 287546 Color: 11

Bin 767: 5 of cap free
Amount of items: 3
Items: 
Size: 707120 Color: 5
Size: 173436 Color: 18
Size: 119440 Color: 0

Bin 768: 5 of cap free
Amount of items: 2
Items: 
Size: 665308 Color: 16
Size: 334688 Color: 3

Bin 769: 5 of cap free
Amount of items: 2
Items: 
Size: 747503 Color: 7
Size: 252493 Color: 14

Bin 770: 5 of cap free
Amount of items: 3
Items: 
Size: 690872 Color: 19
Size: 154618 Color: 5
Size: 154506 Color: 7

Bin 771: 5 of cap free
Amount of items: 3
Items: 
Size: 622785 Color: 0
Size: 188798 Color: 14
Size: 188413 Color: 10

Bin 772: 5 of cap free
Amount of items: 3
Items: 
Size: 626769 Color: 7
Size: 187084 Color: 4
Size: 186143 Color: 19

Bin 773: 5 of cap free
Amount of items: 2
Items: 
Size: 653029 Color: 15
Size: 346967 Color: 6

Bin 774: 5 of cap free
Amount of items: 3
Items: 
Size: 749599 Color: 7
Size: 129350 Color: 12
Size: 121047 Color: 15

Bin 775: 5 of cap free
Amount of items: 3
Items: 
Size: 666582 Color: 17
Size: 169044 Color: 17
Size: 164370 Color: 3

Bin 776: 5 of cap free
Amount of items: 3
Items: 
Size: 386206 Color: 15
Size: 311760 Color: 2
Size: 302030 Color: 12

Bin 777: 5 of cap free
Amount of items: 3
Items: 
Size: 516949 Color: 14
Size: 255167 Color: 1
Size: 227880 Color: 10

Bin 778: 5 of cap free
Amount of items: 3
Items: 
Size: 406690 Color: 6
Size: 301802 Color: 5
Size: 291504 Color: 19

Bin 779: 5 of cap free
Amount of items: 2
Items: 
Size: 724094 Color: 17
Size: 275902 Color: 7

Bin 780: 5 of cap free
Amount of items: 3
Items: 
Size: 377486 Color: 16
Size: 321036 Color: 3
Size: 301474 Color: 5

Bin 781: 5 of cap free
Amount of items: 2
Items: 
Size: 516480 Color: 10
Size: 483516 Color: 9

Bin 782: 5 of cap free
Amount of items: 3
Items: 
Size: 412452 Color: 7
Size: 304025 Color: 15
Size: 283519 Color: 1

Bin 783: 5 of cap free
Amount of items: 2
Items: 
Size: 502817 Color: 16
Size: 497179 Color: 14

Bin 784: 5 of cap free
Amount of items: 2
Items: 
Size: 516325 Color: 11
Size: 483671 Color: 5

Bin 785: 5 of cap free
Amount of items: 2
Items: 
Size: 526686 Color: 14
Size: 473310 Color: 4

Bin 786: 5 of cap free
Amount of items: 2
Items: 
Size: 530005 Color: 14
Size: 469991 Color: 12

Bin 787: 5 of cap free
Amount of items: 2
Items: 
Size: 544745 Color: 9
Size: 455251 Color: 8

Bin 788: 5 of cap free
Amount of items: 2
Items: 
Size: 560681 Color: 10
Size: 439315 Color: 15

Bin 789: 5 of cap free
Amount of items: 2
Items: 
Size: 561199 Color: 5
Size: 438797 Color: 11

Bin 790: 5 of cap free
Amount of items: 3
Items: 
Size: 563098 Color: 13
Size: 219462 Color: 13
Size: 217436 Color: 16

Bin 791: 5 of cap free
Amount of items: 2
Items: 
Size: 581837 Color: 16
Size: 418159 Color: 14

Bin 792: 5 of cap free
Amount of items: 2
Items: 
Size: 589755 Color: 4
Size: 410241 Color: 2

Bin 793: 5 of cap free
Amount of items: 2
Items: 
Size: 598034 Color: 8
Size: 401962 Color: 3

Bin 794: 5 of cap free
Amount of items: 2
Items: 
Size: 598880 Color: 19
Size: 401116 Color: 4

Bin 795: 5 of cap free
Amount of items: 2
Items: 
Size: 598943 Color: 15
Size: 401053 Color: 14

Bin 796: 5 of cap free
Amount of items: 2
Items: 
Size: 599396 Color: 0
Size: 400600 Color: 15

Bin 797: 5 of cap free
Amount of items: 2
Items: 
Size: 600898 Color: 11
Size: 399098 Color: 17

Bin 798: 5 of cap free
Amount of items: 2
Items: 
Size: 613857 Color: 19
Size: 386139 Color: 1

Bin 799: 5 of cap free
Amount of items: 2
Items: 
Size: 639275 Color: 4
Size: 360721 Color: 7

Bin 800: 5 of cap free
Amount of items: 2
Items: 
Size: 641024 Color: 17
Size: 358972 Color: 10

Bin 801: 5 of cap free
Amount of items: 2
Items: 
Size: 648514 Color: 9
Size: 351482 Color: 0

Bin 802: 5 of cap free
Amount of items: 3
Items: 
Size: 659261 Color: 2
Size: 170542 Color: 0
Size: 170193 Color: 3

Bin 803: 5 of cap free
Amount of items: 2
Items: 
Size: 667368 Color: 7
Size: 332628 Color: 19

Bin 804: 5 of cap free
Amount of items: 2
Items: 
Size: 672444 Color: 2
Size: 327552 Color: 0

Bin 805: 5 of cap free
Amount of items: 2
Items: 
Size: 682299 Color: 5
Size: 317697 Color: 9

Bin 806: 5 of cap free
Amount of items: 2
Items: 
Size: 690483 Color: 4
Size: 309513 Color: 14

Bin 807: 5 of cap free
Amount of items: 2
Items: 
Size: 690504 Color: 9
Size: 309492 Color: 15

Bin 808: 5 of cap free
Amount of items: 2
Items: 
Size: 693449 Color: 12
Size: 306547 Color: 11

Bin 809: 5 of cap free
Amount of items: 2
Items: 
Size: 703640 Color: 19
Size: 296356 Color: 11

Bin 810: 5 of cap free
Amount of items: 2
Items: 
Size: 704583 Color: 17
Size: 295413 Color: 2

Bin 811: 5 of cap free
Amount of items: 2
Items: 
Size: 707587 Color: 11
Size: 292409 Color: 12

Bin 812: 5 of cap free
Amount of items: 2
Items: 
Size: 721841 Color: 19
Size: 278155 Color: 11

Bin 813: 5 of cap free
Amount of items: 2
Items: 
Size: 738800 Color: 12
Size: 261196 Color: 9

Bin 814: 5 of cap free
Amount of items: 2
Items: 
Size: 758518 Color: 16
Size: 241478 Color: 8

Bin 815: 5 of cap free
Amount of items: 2
Items: 
Size: 766002 Color: 15
Size: 233994 Color: 3

Bin 816: 5 of cap free
Amount of items: 2
Items: 
Size: 771302 Color: 11
Size: 228694 Color: 16

Bin 817: 5 of cap free
Amount of items: 2
Items: 
Size: 774409 Color: 8
Size: 225587 Color: 0

Bin 818: 5 of cap free
Amount of items: 2
Items: 
Size: 775776 Color: 11
Size: 224220 Color: 18

Bin 819: 5 of cap free
Amount of items: 2
Items: 
Size: 778801 Color: 19
Size: 221195 Color: 3

Bin 820: 6 of cap free
Amount of items: 4
Items: 
Size: 287503 Color: 10
Size: 282283 Color: 18
Size: 255723 Color: 12
Size: 174486 Color: 11

Bin 821: 6 of cap free
Amount of items: 3
Items: 
Size: 662309 Color: 16
Size: 176163 Color: 9
Size: 161523 Color: 13

Bin 822: 6 of cap free
Amount of items: 2
Items: 
Size: 674695 Color: 10
Size: 325300 Color: 0

Bin 823: 6 of cap free
Amount of items: 3
Items: 
Size: 660381 Color: 1
Size: 170248 Color: 12
Size: 169366 Color: 7

Bin 824: 6 of cap free
Amount of items: 3
Items: 
Size: 692493 Color: 0
Size: 153776 Color: 7
Size: 153726 Color: 3

Bin 825: 6 of cap free
Amount of items: 3
Items: 
Size: 712078 Color: 8
Size: 144003 Color: 12
Size: 143914 Color: 17

Bin 826: 6 of cap free
Amount of items: 3
Items: 
Size: 766945 Color: 0
Size: 128076 Color: 5
Size: 104974 Color: 4

Bin 827: 6 of cap free
Amount of items: 3
Items: 
Size: 561101 Color: 16
Size: 227756 Color: 11
Size: 211138 Color: 2

Bin 828: 6 of cap free
Amount of items: 3
Items: 
Size: 630135 Color: 5
Size: 186176 Color: 10
Size: 183684 Color: 17

Bin 829: 6 of cap free
Amount of items: 3
Items: 
Size: 789542 Color: 2
Size: 110059 Color: 12
Size: 100394 Color: 2

Bin 830: 6 of cap free
Amount of items: 3
Items: 
Size: 602061 Color: 17
Size: 198981 Color: 11
Size: 198953 Color: 11

Bin 831: 6 of cap free
Amount of items: 3
Items: 
Size: 368599 Color: 18
Size: 332812 Color: 9
Size: 298584 Color: 19

Bin 832: 6 of cap free
Amount of items: 3
Items: 
Size: 532948 Color: 9
Size: 340344 Color: 13
Size: 126703 Color: 18

Bin 833: 6 of cap free
Amount of items: 2
Items: 
Size: 749101 Color: 6
Size: 250894 Color: 10

Bin 834: 6 of cap free
Amount of items: 3
Items: 
Size: 689164 Color: 6
Size: 155961 Color: 1
Size: 154870 Color: 7

Bin 835: 6 of cap free
Amount of items: 3
Items: 
Size: 675764 Color: 11
Size: 177247 Color: 16
Size: 146984 Color: 7

Bin 836: 6 of cap free
Amount of items: 2
Items: 
Size: 737259 Color: 12
Size: 262736 Color: 6

Bin 837: 6 of cap free
Amount of items: 3
Items: 
Size: 512020 Color: 5
Size: 339813 Color: 1
Size: 148162 Color: 14

Bin 838: 6 of cap free
Amount of items: 3
Items: 
Size: 495190 Color: 5
Size: 388247 Color: 5
Size: 116558 Color: 7

Bin 839: 6 of cap free
Amount of items: 2
Items: 
Size: 730337 Color: 1
Size: 269658 Color: 12

Bin 840: 6 of cap free
Amount of items: 2
Items: 
Size: 762984 Color: 8
Size: 237011 Color: 7

Bin 841: 6 of cap free
Amount of items: 2
Items: 
Size: 518423 Color: 0
Size: 481572 Color: 3

Bin 842: 6 of cap free
Amount of items: 3
Items: 
Size: 643282 Color: 17
Size: 178475 Color: 9
Size: 178238 Color: 4

Bin 843: 6 of cap free
Amount of items: 2
Items: 
Size: 769036 Color: 0
Size: 230959 Color: 19

Bin 844: 6 of cap free
Amount of items: 2
Items: 
Size: 500811 Color: 4
Size: 499184 Color: 12

Bin 845: 6 of cap free
Amount of items: 2
Items: 
Size: 512904 Color: 11
Size: 487091 Color: 10

Bin 846: 6 of cap free
Amount of items: 2
Items: 
Size: 514198 Color: 6
Size: 485797 Color: 9

Bin 847: 6 of cap free
Amount of items: 2
Items: 
Size: 528434 Color: 5
Size: 471561 Color: 11

Bin 848: 6 of cap free
Amount of items: 2
Items: 
Size: 530962 Color: 10
Size: 469033 Color: 15

Bin 849: 6 of cap free
Amount of items: 2
Items: 
Size: 539594 Color: 9
Size: 460401 Color: 4

Bin 850: 6 of cap free
Amount of items: 2
Items: 
Size: 558414 Color: 0
Size: 441581 Color: 7

Bin 851: 6 of cap free
Amount of items: 2
Items: 
Size: 564231 Color: 7
Size: 435764 Color: 13

Bin 852: 6 of cap free
Amount of items: 2
Items: 
Size: 567264 Color: 8
Size: 432731 Color: 14

Bin 853: 6 of cap free
Amount of items: 2
Items: 
Size: 570064 Color: 15
Size: 429931 Color: 11

Bin 854: 6 of cap free
Amount of items: 2
Items: 
Size: 583672 Color: 12
Size: 416323 Color: 14

Bin 855: 6 of cap free
Amount of items: 2
Items: 
Size: 588093 Color: 18
Size: 411902 Color: 2

Bin 856: 6 of cap free
Amount of items: 2
Items: 
Size: 588108 Color: 17
Size: 411887 Color: 5

Bin 857: 6 of cap free
Amount of items: 2
Items: 
Size: 596515 Color: 9
Size: 403480 Color: 11

Bin 858: 6 of cap free
Amount of items: 2
Items: 
Size: 599287 Color: 9
Size: 400708 Color: 14

Bin 859: 6 of cap free
Amount of items: 2
Items: 
Size: 602698 Color: 11
Size: 397297 Color: 15

Bin 860: 6 of cap free
Amount of items: 2
Items: 
Size: 608992 Color: 9
Size: 391003 Color: 18

Bin 861: 6 of cap free
Amount of items: 2
Items: 
Size: 624724 Color: 7
Size: 375271 Color: 1

Bin 862: 6 of cap free
Amount of items: 2
Items: 
Size: 631353 Color: 16
Size: 368642 Color: 3

Bin 863: 6 of cap free
Amount of items: 2
Items: 
Size: 635168 Color: 8
Size: 364827 Color: 19

Bin 864: 6 of cap free
Amount of items: 2
Items: 
Size: 641680 Color: 6
Size: 358315 Color: 2

Bin 865: 6 of cap free
Amount of items: 2
Items: 
Size: 644022 Color: 5
Size: 355973 Color: 13

Bin 866: 6 of cap free
Amount of items: 2
Items: 
Size: 649450 Color: 19
Size: 350545 Color: 8

Bin 867: 6 of cap free
Amount of items: 2
Items: 
Size: 649794 Color: 7
Size: 350201 Color: 2

Bin 868: 6 of cap free
Amount of items: 2
Items: 
Size: 654480 Color: 2
Size: 345515 Color: 7

Bin 869: 6 of cap free
Amount of items: 3
Items: 
Size: 654976 Color: 9
Size: 177282 Color: 7
Size: 167737 Color: 6

Bin 870: 6 of cap free
Amount of items: 2
Items: 
Size: 657577 Color: 3
Size: 342418 Color: 1

Bin 871: 6 of cap free
Amount of items: 2
Items: 
Size: 664485 Color: 4
Size: 335510 Color: 19

Bin 872: 6 of cap free
Amount of items: 2
Items: 
Size: 686448 Color: 17
Size: 313547 Color: 7

Bin 873: 6 of cap free
Amount of items: 2
Items: 
Size: 690316 Color: 1
Size: 309679 Color: 15

Bin 874: 6 of cap free
Amount of items: 3
Items: 
Size: 694508 Color: 8
Size: 153004 Color: 19
Size: 152483 Color: 3

Bin 875: 6 of cap free
Amount of items: 2
Items: 
Size: 723267 Color: 19
Size: 276728 Color: 7

Bin 876: 6 of cap free
Amount of items: 2
Items: 
Size: 742616 Color: 8
Size: 257379 Color: 1

Bin 877: 6 of cap free
Amount of items: 2
Items: 
Size: 747168 Color: 4
Size: 252827 Color: 9

Bin 878: 6 of cap free
Amount of items: 2
Items: 
Size: 780305 Color: 12
Size: 219690 Color: 13

Bin 879: 6 of cap free
Amount of items: 2
Items: 
Size: 792416 Color: 7
Size: 207579 Color: 13

Bin 880: 6 of cap free
Amount of items: 2
Items: 
Size: 796770 Color: 3
Size: 203225 Color: 19

Bin 881: 7 of cap free
Amount of items: 3
Items: 
Size: 759228 Color: 4
Size: 124102 Color: 18
Size: 116664 Color: 9

Bin 882: 7 of cap free
Amount of items: 3
Items: 
Size: 362931 Color: 18
Size: 358221 Color: 0
Size: 278842 Color: 11

Bin 883: 7 of cap free
Amount of items: 3
Items: 
Size: 607912 Color: 18
Size: 196177 Color: 16
Size: 195905 Color: 0

Bin 884: 7 of cap free
Amount of items: 3
Items: 
Size: 417525 Color: 17
Size: 301644 Color: 18
Size: 280825 Color: 9

Bin 885: 7 of cap free
Amount of items: 3
Items: 
Size: 683957 Color: 13
Size: 161988 Color: 17
Size: 154049 Color: 14

Bin 886: 7 of cap free
Amount of items: 3
Items: 
Size: 731632 Color: 3
Size: 151280 Color: 19
Size: 117082 Color: 3

Bin 887: 7 of cap free
Amount of items: 3
Items: 
Size: 656383 Color: 15
Size: 186863 Color: 0
Size: 156748 Color: 9

Bin 888: 7 of cap free
Amount of items: 3
Items: 
Size: 676835 Color: 18
Size: 162699 Color: 15
Size: 160460 Color: 2

Bin 889: 7 of cap free
Amount of items: 2
Items: 
Size: 737811 Color: 4
Size: 262183 Color: 6

Bin 890: 7 of cap free
Amount of items: 3
Items: 
Size: 728747 Color: 0
Size: 136133 Color: 5
Size: 135114 Color: 9

Bin 891: 7 of cap free
Amount of items: 2
Items: 
Size: 665703 Color: 5
Size: 334291 Color: 6

Bin 892: 7 of cap free
Amount of items: 3
Items: 
Size: 655707 Color: 7
Size: 181601 Color: 15
Size: 162686 Color: 13

Bin 893: 7 of cap free
Amount of items: 3
Items: 
Size: 619492 Color: 15
Size: 190499 Color: 3
Size: 190003 Color: 6

Bin 894: 7 of cap free
Amount of items: 3
Items: 
Size: 764356 Color: 7
Size: 117987 Color: 14
Size: 117651 Color: 18

Bin 895: 7 of cap free
Amount of items: 3
Items: 
Size: 721921 Color: 16
Size: 172628 Color: 16
Size: 105445 Color: 12

Bin 896: 7 of cap free
Amount of items: 3
Items: 
Size: 748979 Color: 3
Size: 129707 Color: 0
Size: 121308 Color: 18

Bin 897: 7 of cap free
Amount of items: 3
Items: 
Size: 721762 Color: 14
Size: 145313 Color: 3
Size: 132919 Color: 17

Bin 898: 7 of cap free
Amount of items: 3
Items: 
Size: 715761 Color: 2
Size: 144173 Color: 4
Size: 140060 Color: 6

Bin 899: 7 of cap free
Amount of items: 2
Items: 
Size: 521573 Color: 2
Size: 478421 Color: 11

Bin 900: 7 of cap free
Amount of items: 3
Items: 
Size: 641173 Color: 14
Size: 179417 Color: 14
Size: 179404 Color: 12

Bin 901: 7 of cap free
Amount of items: 2
Items: 
Size: 626700 Color: 17
Size: 373294 Color: 6

Bin 902: 7 of cap free
Amount of items: 2
Items: 
Size: 633922 Color: 2
Size: 366072 Color: 12

Bin 903: 7 of cap free
Amount of items: 3
Items: 
Size: 650201 Color: 1
Size: 181930 Color: 1
Size: 167863 Color: 6

Bin 904: 7 of cap free
Amount of items: 3
Items: 
Size: 372107 Color: 12
Size: 346338 Color: 16
Size: 281549 Color: 15

Bin 905: 7 of cap free
Amount of items: 3
Items: 
Size: 388979 Color: 7
Size: 361826 Color: 16
Size: 249189 Color: 18

Bin 906: 7 of cap free
Amount of items: 3
Items: 
Size: 401067 Color: 19
Size: 301228 Color: 12
Size: 297699 Color: 5

Bin 907: 7 of cap free
Amount of items: 3
Items: 
Size: 383222 Color: 17
Size: 365150 Color: 2
Size: 251622 Color: 11

Bin 908: 7 of cap free
Amount of items: 3
Items: 
Size: 441226 Color: 14
Size: 286864 Color: 16
Size: 271904 Color: 5

Bin 909: 7 of cap free
Amount of items: 2
Items: 
Size: 507787 Color: 10
Size: 492207 Color: 14

Bin 910: 7 of cap free
Amount of items: 2
Items: 
Size: 514039 Color: 2
Size: 485955 Color: 7

Bin 911: 7 of cap free
Amount of items: 2
Items: 
Size: 521435 Color: 14
Size: 478559 Color: 12

Bin 912: 7 of cap free
Amount of items: 2
Items: 
Size: 532782 Color: 19
Size: 467212 Color: 11

Bin 913: 7 of cap free
Amount of items: 2
Items: 
Size: 546101 Color: 18
Size: 453893 Color: 11

Bin 914: 7 of cap free
Amount of items: 2
Items: 
Size: 559100 Color: 0
Size: 440894 Color: 16

Bin 915: 7 of cap free
Amount of items: 2
Items: 
Size: 577969 Color: 8
Size: 422025 Color: 3

Bin 916: 7 of cap free
Amount of items: 2
Items: 
Size: 580656 Color: 6
Size: 419338 Color: 17

Bin 917: 7 of cap free
Amount of items: 2
Items: 
Size: 583877 Color: 2
Size: 416117 Color: 4

Bin 918: 7 of cap free
Amount of items: 2
Items: 
Size: 595764 Color: 3
Size: 404230 Color: 9

Bin 919: 7 of cap free
Amount of items: 2
Items: 
Size: 596366 Color: 2
Size: 403628 Color: 19

Bin 920: 7 of cap free
Amount of items: 2
Items: 
Size: 597069 Color: 19
Size: 402925 Color: 8

Bin 921: 7 of cap free
Amount of items: 2
Items: 
Size: 596808 Color: 6
Size: 403186 Color: 14

Bin 922: 7 of cap free
Amount of items: 2
Items: 
Size: 600697 Color: 14
Size: 399297 Color: 10

Bin 923: 7 of cap free
Amount of items: 2
Items: 
Size: 604237 Color: 19
Size: 395757 Color: 4

Bin 924: 7 of cap free
Amount of items: 2
Items: 
Size: 636429 Color: 9
Size: 363565 Color: 0

Bin 925: 7 of cap free
Amount of items: 3
Items: 
Size: 648140 Color: 12
Size: 176236 Color: 15
Size: 175618 Color: 9

Bin 926: 7 of cap free
Amount of items: 2
Items: 
Size: 651807 Color: 14
Size: 348187 Color: 7

Bin 927: 7 of cap free
Amount of items: 2
Items: 
Size: 657240 Color: 13
Size: 342754 Color: 4

Bin 928: 7 of cap free
Amount of items: 2
Items: 
Size: 668941 Color: 0
Size: 331053 Color: 6

Bin 929: 7 of cap free
Amount of items: 2
Items: 
Size: 676199 Color: 2
Size: 323795 Color: 3

Bin 930: 7 of cap free
Amount of items: 2
Items: 
Size: 705588 Color: 17
Size: 294406 Color: 5

Bin 931: 7 of cap free
Amount of items: 2
Items: 
Size: 712207 Color: 3
Size: 287787 Color: 17

Bin 932: 7 of cap free
Amount of items: 2
Items: 
Size: 715871 Color: 18
Size: 284123 Color: 5

Bin 933: 7 of cap free
Amount of items: 2
Items: 
Size: 726465 Color: 13
Size: 273529 Color: 17

Bin 934: 7 of cap free
Amount of items: 2
Items: 
Size: 728139 Color: 13
Size: 271855 Color: 12

Bin 935: 7 of cap free
Amount of items: 2
Items: 
Size: 735200 Color: 19
Size: 264794 Color: 9

Bin 936: 7 of cap free
Amount of items: 2
Items: 
Size: 752925 Color: 3
Size: 247069 Color: 4

Bin 937: 7 of cap free
Amount of items: 2
Items: 
Size: 756433 Color: 8
Size: 243561 Color: 16

Bin 938: 7 of cap free
Amount of items: 2
Items: 
Size: 756659 Color: 19
Size: 243335 Color: 1

Bin 939: 7 of cap free
Amount of items: 2
Items: 
Size: 758816 Color: 8
Size: 241178 Color: 1

Bin 940: 7 of cap free
Amount of items: 2
Items: 
Size: 770334 Color: 18
Size: 229660 Color: 19

Bin 941: 7 of cap free
Amount of items: 2
Items: 
Size: 794887 Color: 3
Size: 205107 Color: 7

Bin 942: 8 of cap free
Amount of items: 3
Items: 
Size: 667319 Color: 9
Size: 177796 Color: 19
Size: 154878 Color: 2

Bin 943: 8 of cap free
Amount of items: 3
Items: 
Size: 721954 Color: 3
Size: 148942 Color: 7
Size: 129097 Color: 9

Bin 944: 8 of cap free
Amount of items: 3
Items: 
Size: 727334 Color: 7
Size: 143280 Color: 12
Size: 129379 Color: 14

Bin 945: 8 of cap free
Amount of items: 3
Items: 
Size: 699962 Color: 5
Size: 150025 Color: 14
Size: 150006 Color: 1

Bin 946: 8 of cap free
Amount of items: 2
Items: 
Size: 775471 Color: 9
Size: 224522 Color: 1

Bin 947: 8 of cap free
Amount of items: 3
Items: 
Size: 379766 Color: 5
Size: 357103 Color: 5
Size: 263124 Color: 13

Bin 948: 8 of cap free
Amount of items: 3
Items: 
Size: 441451 Color: 11
Size: 306660 Color: 11
Size: 251882 Color: 6

Bin 949: 8 of cap free
Amount of items: 3
Items: 
Size: 514614 Color: 13
Size: 306050 Color: 7
Size: 179329 Color: 1

Bin 950: 8 of cap free
Amount of items: 3
Items: 
Size: 677015 Color: 18
Size: 164801 Color: 7
Size: 158177 Color: 6

Bin 951: 8 of cap free
Amount of items: 3
Items: 
Size: 362125 Color: 13
Size: 339434 Color: 17
Size: 298434 Color: 10

Bin 952: 8 of cap free
Amount of items: 2
Items: 
Size: 748047 Color: 7
Size: 251946 Color: 12

Bin 953: 8 of cap free
Amount of items: 3
Items: 
Size: 648613 Color: 4
Size: 175834 Color: 14
Size: 175546 Color: 17

Bin 954: 8 of cap free
Amount of items: 2
Items: 
Size: 796113 Color: 12
Size: 203880 Color: 6

Bin 955: 8 of cap free
Amount of items: 3
Items: 
Size: 513730 Color: 3
Size: 254868 Color: 16
Size: 231395 Color: 16

Bin 956: 8 of cap free
Amount of items: 2
Items: 
Size: 628255 Color: 18
Size: 371738 Color: 8

Bin 957: 8 of cap free
Amount of items: 3
Items: 
Size: 526677 Color: 18
Size: 238296 Color: 15
Size: 235020 Color: 15

Bin 958: 8 of cap free
Amount of items: 2
Items: 
Size: 682243 Color: 11
Size: 317750 Color: 10

Bin 959: 8 of cap free
Amount of items: 2
Items: 
Size: 601381 Color: 0
Size: 398612 Color: 16

Bin 960: 8 of cap free
Amount of items: 2
Items: 
Size: 601914 Color: 2
Size: 398079 Color: 6

Bin 961: 8 of cap free
Amount of items: 2
Items: 
Size: 525036 Color: 3
Size: 474957 Color: 0

Bin 962: 8 of cap free
Amount of items: 2
Items: 
Size: 625497 Color: 11
Size: 374496 Color: 9

Bin 963: 8 of cap free
Amount of items: 2
Items: 
Size: 612725 Color: 19
Size: 387268 Color: 18

Bin 964: 8 of cap free
Amount of items: 2
Items: 
Size: 508330 Color: 1
Size: 491663 Color: 5

Bin 965: 8 of cap free
Amount of items: 2
Items: 
Size: 516692 Color: 1
Size: 483301 Color: 10

Bin 966: 8 of cap free
Amount of items: 2
Items: 
Size: 563423 Color: 17
Size: 436570 Color: 6

Bin 967: 8 of cap free
Amount of items: 2
Items: 
Size: 565177 Color: 0
Size: 434816 Color: 14

Bin 968: 8 of cap free
Amount of items: 2
Items: 
Size: 570129 Color: 15
Size: 429864 Color: 7

Bin 969: 8 of cap free
Amount of items: 2
Items: 
Size: 572906 Color: 3
Size: 427087 Color: 10

Bin 970: 8 of cap free
Amount of items: 2
Items: 
Size: 599334 Color: 9
Size: 400659 Color: 13

Bin 971: 8 of cap free
Amount of items: 2
Items: 
Size: 599357 Color: 7
Size: 400636 Color: 11

Bin 972: 8 of cap free
Amount of items: 2
Items: 
Size: 599499 Color: 13
Size: 400494 Color: 11

Bin 973: 8 of cap free
Amount of items: 2
Items: 
Size: 601146 Color: 17
Size: 398847 Color: 7

Bin 974: 8 of cap free
Amount of items: 2
Items: 
Size: 605345 Color: 1
Size: 394648 Color: 15

Bin 975: 8 of cap free
Amount of items: 2
Items: 
Size: 642310 Color: 2
Size: 357683 Color: 14

Bin 976: 8 of cap free
Amount of items: 2
Items: 
Size: 645810 Color: 19
Size: 354183 Color: 18

Bin 977: 8 of cap free
Amount of items: 2
Items: 
Size: 675401 Color: 1
Size: 324592 Color: 4

Bin 978: 8 of cap free
Amount of items: 2
Items: 
Size: 675861 Color: 6
Size: 324132 Color: 0

Bin 979: 8 of cap free
Amount of items: 2
Items: 
Size: 689500 Color: 14
Size: 310493 Color: 2

Bin 980: 8 of cap free
Amount of items: 2
Items: 
Size: 695541 Color: 7
Size: 304452 Color: 1

Bin 981: 8 of cap free
Amount of items: 2
Items: 
Size: 720870 Color: 4
Size: 279123 Color: 16

Bin 982: 8 of cap free
Amount of items: 2
Items: 
Size: 742431 Color: 14
Size: 257562 Color: 7

Bin 983: 8 of cap free
Amount of items: 2
Items: 
Size: 744939 Color: 0
Size: 255054 Color: 19

Bin 984: 9 of cap free
Amount of items: 3
Items: 
Size: 708570 Color: 5
Size: 169505 Color: 4
Size: 121917 Color: 3

Bin 985: 9 of cap free
Amount of items: 3
Items: 
Size: 723975 Color: 17
Size: 140834 Color: 5
Size: 135183 Color: 17

Bin 986: 9 of cap free
Amount of items: 2
Items: 
Size: 721284 Color: 11
Size: 278708 Color: 12

Bin 987: 9 of cap free
Amount of items: 3
Items: 
Size: 639430 Color: 17
Size: 180343 Color: 11
Size: 180219 Color: 9

Bin 988: 9 of cap free
Amount of items: 2
Items: 
Size: 626772 Color: 12
Size: 373220 Color: 2

Bin 989: 9 of cap free
Amount of items: 3
Items: 
Size: 527787 Color: 17
Size: 253385 Color: 14
Size: 218820 Color: 16

Bin 990: 9 of cap free
Amount of items: 3
Items: 
Size: 692840 Color: 18
Size: 185987 Color: 11
Size: 121165 Color: 11

Bin 991: 9 of cap free
Amount of items: 3
Items: 
Size: 665833 Color: 16
Size: 170424 Color: 6
Size: 163735 Color: 11

Bin 992: 9 of cap free
Amount of items: 3
Items: 
Size: 597118 Color: 2
Size: 236168 Color: 14
Size: 166706 Color: 8

Bin 993: 9 of cap free
Amount of items: 2
Items: 
Size: 736787 Color: 3
Size: 263205 Color: 10

Bin 994: 9 of cap free
Amount of items: 2
Items: 
Size: 741932 Color: 3
Size: 258060 Color: 8

Bin 995: 9 of cap free
Amount of items: 2
Items: 
Size: 730717 Color: 13
Size: 269275 Color: 17

Bin 996: 9 of cap free
Amount of items: 3
Items: 
Size: 723031 Color: 5
Size: 141675 Color: 0
Size: 135286 Color: 18

Bin 997: 9 of cap free
Amount of items: 3
Items: 
Size: 558922 Color: 7
Size: 273397 Color: 6
Size: 167673 Color: 2

Bin 998: 9 of cap free
Amount of items: 2
Items: 
Size: 706647 Color: 17
Size: 293345 Color: 9

Bin 999: 9 of cap free
Amount of items: 2
Items: 
Size: 631782 Color: 8
Size: 368210 Color: 10

Bin 1000: 9 of cap free
Amount of items: 2
Items: 
Size: 612527 Color: 13
Size: 387465 Color: 4

Bin 1001: 9 of cap free
Amount of items: 3
Items: 
Size: 711304 Color: 9
Size: 144730 Color: 17
Size: 143958 Color: 7

Bin 1002: 9 of cap free
Amount of items: 3
Items: 
Size: 670013 Color: 6
Size: 165797 Color: 18
Size: 164182 Color: 8

Bin 1003: 9 of cap free
Amount of items: 3
Items: 
Size: 694002 Color: 10
Size: 153122 Color: 11
Size: 152868 Color: 5

Bin 1004: 9 of cap free
Amount of items: 3
Items: 
Size: 713810 Color: 4
Size: 149634 Color: 18
Size: 136548 Color: 7

Bin 1005: 9 of cap free
Amount of items: 2
Items: 
Size: 760613 Color: 13
Size: 239379 Color: 4

Bin 1006: 9 of cap free
Amount of items: 3
Items: 
Size: 413763 Color: 11
Size: 297646 Color: 1
Size: 288583 Color: 12

Bin 1007: 9 of cap free
Amount of items: 2
Items: 
Size: 534457 Color: 3
Size: 465535 Color: 0

Bin 1008: 9 of cap free
Amount of items: 2
Items: 
Size: 719114 Color: 17
Size: 280878 Color: 9

Bin 1009: 9 of cap free
Amount of items: 2
Items: 
Size: 794683 Color: 2
Size: 205309 Color: 16

Bin 1010: 9 of cap free
Amount of items: 2
Items: 
Size: 798769 Color: 9
Size: 201223 Color: 6

Bin 1011: 9 of cap free
Amount of items: 3
Items: 
Size: 390599 Color: 7
Size: 305066 Color: 4
Size: 304327 Color: 2

Bin 1012: 9 of cap free
Amount of items: 3
Items: 
Size: 361795 Color: 8
Size: 334563 Color: 14
Size: 303634 Color: 4

Bin 1013: 9 of cap free
Amount of items: 2
Items: 
Size: 505993 Color: 10
Size: 493999 Color: 12

Bin 1014: 9 of cap free
Amount of items: 2
Items: 
Size: 511899 Color: 7
Size: 488093 Color: 16

Bin 1015: 9 of cap free
Amount of items: 2
Items: 
Size: 520620 Color: 15
Size: 479372 Color: 3

Bin 1016: 9 of cap free
Amount of items: 2
Items: 
Size: 523155 Color: 8
Size: 476837 Color: 10

Bin 1017: 9 of cap free
Amount of items: 2
Items: 
Size: 544664 Color: 14
Size: 455328 Color: 11

Bin 1018: 9 of cap free
Amount of items: 2
Items: 
Size: 545725 Color: 14
Size: 454267 Color: 16

Bin 1019: 9 of cap free
Amount of items: 2
Items: 
Size: 547998 Color: 3
Size: 451994 Color: 4

Bin 1020: 9 of cap free
Amount of items: 2
Items: 
Size: 558253 Color: 10
Size: 441739 Color: 6

Bin 1021: 9 of cap free
Amount of items: 2
Items: 
Size: 569848 Color: 2
Size: 430144 Color: 7

Bin 1022: 9 of cap free
Amount of items: 2
Items: 
Size: 569868 Color: 10
Size: 430124 Color: 13

Bin 1023: 9 of cap free
Amount of items: 2
Items: 
Size: 578037 Color: 11
Size: 421955 Color: 0

Bin 1024: 9 of cap free
Amount of items: 2
Items: 
Size: 584183 Color: 3
Size: 415809 Color: 14

Bin 1025: 9 of cap free
Amount of items: 2
Items: 
Size: 600997 Color: 16
Size: 398995 Color: 7

Bin 1026: 9 of cap free
Amount of items: 2
Items: 
Size: 606009 Color: 14
Size: 393983 Color: 11

Bin 1027: 9 of cap free
Amount of items: 2
Items: 
Size: 633671 Color: 0
Size: 366321 Color: 11

Bin 1028: 9 of cap free
Amount of items: 2
Items: 
Size: 643481 Color: 9
Size: 356511 Color: 17

Bin 1029: 9 of cap free
Amount of items: 2
Items: 
Size: 644860 Color: 6
Size: 355132 Color: 1

Bin 1030: 9 of cap free
Amount of items: 2
Items: 
Size: 646950 Color: 9
Size: 353042 Color: 12

Bin 1031: 9 of cap free
Amount of items: 3
Items: 
Size: 657034 Color: 13
Size: 173261 Color: 14
Size: 169697 Color: 6

Bin 1032: 9 of cap free
Amount of items: 3
Items: 
Size: 667845 Color: 17
Size: 166162 Color: 14
Size: 165985 Color: 19

Bin 1033: 9 of cap free
Amount of items: 2
Items: 
Size: 667870 Color: 14
Size: 332122 Color: 10

Bin 1034: 9 of cap free
Amount of items: 2
Items: 
Size: 672132 Color: 12
Size: 327860 Color: 8

Bin 1035: 9 of cap free
Amount of items: 2
Items: 
Size: 693373 Color: 0
Size: 306619 Color: 14

Bin 1036: 9 of cap free
Amount of items: 2
Items: 
Size: 697213 Color: 15
Size: 302779 Color: 18

Bin 1037: 9 of cap free
Amount of items: 2
Items: 
Size: 699440 Color: 9
Size: 300552 Color: 2

Bin 1038: 9 of cap free
Amount of items: 2
Items: 
Size: 709702 Color: 14
Size: 290290 Color: 5

Bin 1039: 9 of cap free
Amount of items: 2
Items: 
Size: 718801 Color: 16
Size: 281191 Color: 10

Bin 1040: 9 of cap free
Amount of items: 2
Items: 
Size: 750462 Color: 10
Size: 249530 Color: 2

Bin 1041: 9 of cap free
Amount of items: 2
Items: 
Size: 759461 Color: 1
Size: 240531 Color: 14

Bin 1042: 9 of cap free
Amount of items: 2
Items: 
Size: 778901 Color: 10
Size: 221091 Color: 13

Bin 1043: 9 of cap free
Amount of items: 2
Items: 
Size: 780603 Color: 7
Size: 219389 Color: 1

Bin 1044: 10 of cap free
Amount of items: 3
Items: 
Size: 660777 Color: 8
Size: 169814 Color: 3
Size: 169400 Color: 10

Bin 1045: 10 of cap free
Amount of items: 2
Items: 
Size: 628283 Color: 3
Size: 371708 Color: 8

Bin 1046: 10 of cap free
Amount of items: 3
Items: 
Size: 417710 Color: 12
Size: 301617 Color: 11
Size: 280664 Color: 16

Bin 1047: 10 of cap free
Amount of items: 3
Items: 
Size: 775452 Color: 19
Size: 117146 Color: 3
Size: 107393 Color: 8

Bin 1048: 10 of cap free
Amount of items: 3
Items: 
Size: 622950 Color: 12
Size: 189034 Color: 3
Size: 188007 Color: 15

Bin 1049: 10 of cap free
Amount of items: 3
Items: 
Size: 742504 Color: 14
Size: 129270 Color: 6
Size: 128217 Color: 2

Bin 1050: 10 of cap free
Amount of items: 3
Items: 
Size: 769361 Color: 8
Size: 124272 Color: 5
Size: 106358 Color: 15

Bin 1051: 10 of cap free
Amount of items: 3
Items: 
Size: 671670 Color: 16
Size: 164369 Color: 5
Size: 163952 Color: 16

Bin 1052: 10 of cap free
Amount of items: 2
Items: 
Size: 614402 Color: 11
Size: 385589 Color: 5

Bin 1053: 10 of cap free
Amount of items: 2
Items: 
Size: 688460 Color: 17
Size: 311531 Color: 8

Bin 1054: 10 of cap free
Amount of items: 3
Items: 
Size: 723235 Color: 6
Size: 138397 Color: 4
Size: 138359 Color: 8

Bin 1055: 10 of cap free
Amount of items: 3
Items: 
Size: 614731 Color: 16
Size: 192876 Color: 9
Size: 192384 Color: 13

Bin 1056: 10 of cap free
Amount of items: 3
Items: 
Size: 623369 Color: 3
Size: 197339 Color: 4
Size: 179283 Color: 6

Bin 1057: 10 of cap free
Amount of items: 2
Items: 
Size: 655854 Color: 3
Size: 344137 Color: 7

Bin 1058: 10 of cap free
Amount of items: 3
Items: 
Size: 469067 Color: 3
Size: 281490 Color: 0
Size: 249434 Color: 8

Bin 1059: 10 of cap free
Amount of items: 2
Items: 
Size: 774785 Color: 6
Size: 225206 Color: 19

Bin 1060: 10 of cap free
Amount of items: 3
Items: 
Size: 662422 Color: 3
Size: 169199 Color: 18
Size: 168370 Color: 1

Bin 1061: 10 of cap free
Amount of items: 2
Items: 
Size: 619550 Color: 6
Size: 380441 Color: 9

Bin 1062: 10 of cap free
Amount of items: 3
Items: 
Size: 406730 Color: 2
Size: 323265 Color: 11
Size: 269996 Color: 2

Bin 1063: 10 of cap free
Amount of items: 2
Items: 
Size: 680084 Color: 6
Size: 319907 Color: 12

Bin 1064: 10 of cap free
Amount of items: 2
Items: 
Size: 735888 Color: 14
Size: 264103 Color: 19

Bin 1065: 10 of cap free
Amount of items: 2
Items: 
Size: 749881 Color: 16
Size: 250110 Color: 6

Bin 1066: 10 of cap free
Amount of items: 2
Items: 
Size: 781774 Color: 1
Size: 218217 Color: 6

Bin 1067: 10 of cap free
Amount of items: 2
Items: 
Size: 692549 Color: 2
Size: 307442 Color: 6

Bin 1068: 10 of cap free
Amount of items: 2
Items: 
Size: 504459 Color: 6
Size: 495532 Color: 16

Bin 1069: 10 of cap free
Amount of items: 3
Items: 
Size: 383821 Color: 5
Size: 311327 Color: 9
Size: 304843 Color: 9

Bin 1070: 10 of cap free
Amount of items: 3
Items: 
Size: 413375 Color: 0
Size: 335814 Color: 6
Size: 250802 Color: 2

Bin 1071: 10 of cap free
Amount of items: 2
Items: 
Size: 514846 Color: 17
Size: 485145 Color: 9

Bin 1072: 10 of cap free
Amount of items: 2
Items: 
Size: 515442 Color: 16
Size: 484549 Color: 5

Bin 1073: 10 of cap free
Amount of items: 2
Items: 
Size: 532107 Color: 11
Size: 467884 Color: 10

Bin 1074: 10 of cap free
Amount of items: 2
Items: 
Size: 548090 Color: 4
Size: 451901 Color: 15

Bin 1075: 10 of cap free
Amount of items: 2
Items: 
Size: 553886 Color: 7
Size: 446105 Color: 14

Bin 1076: 10 of cap free
Amount of items: 2
Items: 
Size: 559789 Color: 19
Size: 440202 Color: 12

Bin 1077: 10 of cap free
Amount of items: 2
Items: 
Size: 560741 Color: 7
Size: 439250 Color: 16

Bin 1078: 10 of cap free
Amount of items: 2
Items: 
Size: 560782 Color: 4
Size: 439209 Color: 13

Bin 1079: 10 of cap free
Amount of items: 2
Items: 
Size: 569070 Color: 16
Size: 430921 Color: 13

Bin 1080: 10 of cap free
Amount of items: 2
Items: 
Size: 578043 Color: 0
Size: 421948 Color: 8

Bin 1081: 10 of cap free
Amount of items: 2
Items: 
Size: 602793 Color: 4
Size: 397198 Color: 7

Bin 1082: 10 of cap free
Amount of items: 2
Items: 
Size: 618401 Color: 7
Size: 381590 Color: 2

Bin 1083: 10 of cap free
Amount of items: 3
Items: 
Size: 627741 Color: 4
Size: 186476 Color: 3
Size: 185774 Color: 8

Bin 1084: 10 of cap free
Amount of items: 2
Items: 
Size: 631608 Color: 7
Size: 368383 Color: 17

Bin 1085: 10 of cap free
Amount of items: 2
Items: 
Size: 634926 Color: 15
Size: 365065 Color: 2

Bin 1086: 10 of cap free
Amount of items: 2
Items: 
Size: 636500 Color: 13
Size: 363491 Color: 14

Bin 1087: 10 of cap free
Amount of items: 2
Items: 
Size: 650030 Color: 9
Size: 349961 Color: 5

Bin 1088: 10 of cap free
Amount of items: 2
Items: 
Size: 657165 Color: 8
Size: 342826 Color: 4

Bin 1089: 10 of cap free
Amount of items: 2
Items: 
Size: 669181 Color: 5
Size: 330810 Color: 3

Bin 1090: 10 of cap free
Amount of items: 2
Items: 
Size: 682353 Color: 18
Size: 317638 Color: 17

Bin 1091: 10 of cap free
Amount of items: 2
Items: 
Size: 685350 Color: 10
Size: 314641 Color: 4

Bin 1092: 10 of cap free
Amount of items: 2
Items: 
Size: 687780 Color: 11
Size: 312211 Color: 1

Bin 1093: 10 of cap free
Amount of items: 2
Items: 
Size: 690641 Color: 15
Size: 309350 Color: 8

Bin 1094: 10 of cap free
Amount of items: 3
Items: 
Size: 701851 Color: 7
Size: 149447 Color: 4
Size: 148693 Color: 16

Bin 1095: 10 of cap free
Amount of items: 2
Items: 
Size: 716210 Color: 18
Size: 283781 Color: 3

Bin 1096: 10 of cap free
Amount of items: 2
Items: 
Size: 717306 Color: 15
Size: 282685 Color: 11

Bin 1097: 10 of cap free
Amount of items: 2
Items: 
Size: 723740 Color: 12
Size: 276251 Color: 19

Bin 1098: 10 of cap free
Amount of items: 2
Items: 
Size: 725985 Color: 12
Size: 274006 Color: 11

Bin 1099: 10 of cap free
Amount of items: 3
Items: 
Size: 763617 Color: 0
Size: 118691 Color: 17
Size: 117683 Color: 6

Bin 1100: 10 of cap free
Amount of items: 2
Items: 
Size: 771395 Color: 12
Size: 228596 Color: 0

Bin 1101: 10 of cap free
Amount of items: 2
Items: 
Size: 788751 Color: 3
Size: 211240 Color: 1

Bin 1102: 10 of cap free
Amount of items: 2
Items: 
Size: 790836 Color: 16
Size: 209155 Color: 9

Bin 1103: 11 of cap free
Amount of items: 3
Items: 
Size: 668385 Color: 1
Size: 166917 Color: 13
Size: 164688 Color: 2

Bin 1104: 11 of cap free
Amount of items: 3
Items: 
Size: 698253 Color: 6
Size: 151054 Color: 1
Size: 150683 Color: 18

Bin 1105: 11 of cap free
Amount of items: 3
Items: 
Size: 667767 Color: 13
Size: 174493 Color: 19
Size: 157730 Color: 18

Bin 1106: 11 of cap free
Amount of items: 3
Items: 
Size: 790365 Color: 17
Size: 105670 Color: 3
Size: 103955 Color: 16

Bin 1107: 11 of cap free
Amount of items: 3
Items: 
Size: 698345 Color: 2
Size: 151034 Color: 2
Size: 150611 Color: 17

Bin 1108: 11 of cap free
Amount of items: 3
Items: 
Size: 698435 Color: 18
Size: 150899 Color: 13
Size: 150656 Color: 11

Bin 1109: 11 of cap free
Amount of items: 3
Items: 
Size: 368177 Color: 17
Size: 326714 Color: 6
Size: 305099 Color: 5

Bin 1110: 11 of cap free
Amount of items: 2
Items: 
Size: 651332 Color: 6
Size: 348658 Color: 12

Bin 1111: 11 of cap free
Amount of items: 2
Items: 
Size: 793626 Color: 8
Size: 206364 Color: 19

Bin 1112: 11 of cap free
Amount of items: 3
Items: 
Size: 671616 Color: 6
Size: 164299 Color: 1
Size: 164075 Color: 14

Bin 1113: 11 of cap free
Amount of items: 2
Items: 
Size: 612069 Color: 0
Size: 387921 Color: 15

Bin 1114: 11 of cap free
Amount of items: 3
Items: 
Size: 685020 Color: 6
Size: 158245 Color: 1
Size: 156725 Color: 19

Bin 1115: 11 of cap free
Amount of items: 3
Items: 
Size: 513010 Color: 18
Size: 256005 Color: 14
Size: 230975 Color: 19

Bin 1116: 11 of cap free
Amount of items: 2
Items: 
Size: 500139 Color: 6
Size: 499851 Color: 9

Bin 1117: 11 of cap free
Amount of items: 3
Items: 
Size: 536815 Color: 13
Size: 239378 Color: 0
Size: 223797 Color: 16

Bin 1118: 11 of cap free
Amount of items: 2
Items: 
Size: 664797 Color: 3
Size: 335193 Color: 6

Bin 1119: 11 of cap free
Amount of items: 2
Items: 
Size: 506519 Color: 4
Size: 493471 Color: 8

Bin 1120: 11 of cap free
Amount of items: 2
Items: 
Size: 618229 Color: 3
Size: 381761 Color: 1

Bin 1121: 11 of cap free
Amount of items: 2
Items: 
Size: 643258 Color: 0
Size: 356732 Color: 6

Bin 1122: 11 of cap free
Amount of items: 2
Items: 
Size: 689800 Color: 10
Size: 310190 Color: 16

Bin 1123: 11 of cap free
Amount of items: 3
Items: 
Size: 418989 Color: 10
Size: 299813 Color: 4
Size: 281188 Color: 7

Bin 1124: 11 of cap free
Amount of items: 2
Items: 
Size: 505595 Color: 4
Size: 494395 Color: 10

Bin 1125: 11 of cap free
Amount of items: 2
Items: 
Size: 516336 Color: 4
Size: 483654 Color: 10

Bin 1126: 11 of cap free
Amount of items: 2
Items: 
Size: 548913 Color: 3
Size: 451077 Color: 2

Bin 1127: 11 of cap free
Amount of items: 2
Items: 
Size: 557152 Color: 18
Size: 442838 Color: 19

Bin 1128: 11 of cap free
Amount of items: 2
Items: 
Size: 566643 Color: 9
Size: 433347 Color: 14

Bin 1129: 11 of cap free
Amount of items: 2
Items: 
Size: 569701 Color: 8
Size: 430289 Color: 6

Bin 1130: 11 of cap free
Amount of items: 2
Items: 
Size: 575205 Color: 11
Size: 424785 Color: 15

Bin 1131: 11 of cap free
Amount of items: 2
Items: 
Size: 587714 Color: 5
Size: 412276 Color: 1

Bin 1132: 11 of cap free
Amount of items: 2
Items: 
Size: 596115 Color: 8
Size: 403875 Color: 6

Bin 1133: 11 of cap free
Amount of items: 2
Items: 
Size: 597683 Color: 13
Size: 402307 Color: 14

Bin 1134: 11 of cap free
Amount of items: 2
Items: 
Size: 598818 Color: 6
Size: 401172 Color: 10

Bin 1135: 11 of cap free
Amount of items: 2
Items: 
Size: 615454 Color: 2
Size: 384536 Color: 13

Bin 1136: 11 of cap free
Amount of items: 2
Items: 
Size: 620782 Color: 11
Size: 379208 Color: 16

Bin 1137: 11 of cap free
Amount of items: 2
Items: 
Size: 624942 Color: 6
Size: 375048 Color: 4

Bin 1138: 11 of cap free
Amount of items: 2
Items: 
Size: 633969 Color: 13
Size: 366021 Color: 8

Bin 1139: 11 of cap free
Amount of items: 2
Items: 
Size: 656566 Color: 5
Size: 343424 Color: 11

Bin 1140: 11 of cap free
Amount of items: 2
Items: 
Size: 706277 Color: 11
Size: 293713 Color: 19

Bin 1141: 11 of cap free
Amount of items: 2
Items: 
Size: 718787 Color: 15
Size: 281203 Color: 13

Bin 1142: 11 of cap free
Amount of items: 2
Items: 
Size: 719896 Color: 12
Size: 280094 Color: 0

Bin 1143: 11 of cap free
Amount of items: 2
Items: 
Size: 764887 Color: 13
Size: 235103 Color: 4

Bin 1144: 11 of cap free
Amount of items: 2
Items: 
Size: 772477 Color: 5
Size: 227513 Color: 18

Bin 1145: 11 of cap free
Amount of items: 2
Items: 
Size: 783752 Color: 1
Size: 216238 Color: 7

Bin 1146: 12 of cap free
Amount of items: 3
Items: 
Size: 658826 Color: 18
Size: 204541 Color: 14
Size: 136622 Color: 1

Bin 1147: 12 of cap free
Amount of items: 3
Items: 
Size: 688552 Color: 6
Size: 172735 Color: 9
Size: 138702 Color: 14

Bin 1148: 12 of cap free
Amount of items: 3
Items: 
Size: 708676 Color: 13
Size: 164114 Color: 9
Size: 127199 Color: 1

Bin 1149: 12 of cap free
Amount of items: 3
Items: 
Size: 782157 Color: 6
Size: 109680 Color: 5
Size: 108152 Color: 7

Bin 1150: 12 of cap free
Amount of items: 2
Items: 
Size: 772224 Color: 0
Size: 227765 Color: 16

Bin 1151: 12 of cap free
Amount of items: 2
Items: 
Size: 641481 Color: 10
Size: 358508 Color: 12

Bin 1152: 12 of cap free
Amount of items: 2
Items: 
Size: 651476 Color: 11
Size: 348513 Color: 14

Bin 1153: 12 of cap free
Amount of items: 3
Items: 
Size: 597394 Color: 0
Size: 236181 Color: 6
Size: 166414 Color: 2

Bin 1154: 12 of cap free
Amount of items: 3
Items: 
Size: 698448 Color: 13
Size: 150970 Color: 6
Size: 150571 Color: 5

Bin 1155: 12 of cap free
Amount of items: 3
Items: 
Size: 518592 Color: 19
Size: 251086 Color: 10
Size: 230311 Color: 15

Bin 1156: 12 of cap free
Amount of items: 3
Items: 
Size: 619387 Color: 12
Size: 190464 Color: 9
Size: 190138 Color: 0

Bin 1157: 12 of cap free
Amount of items: 3
Items: 
Size: 667403 Color: 3
Size: 169972 Color: 9
Size: 162614 Color: 11

Bin 1158: 12 of cap free
Amount of items: 3
Items: 
Size: 742544 Color: 1
Size: 144090 Color: 4
Size: 113355 Color: 17

Bin 1159: 12 of cap free
Amount of items: 3
Items: 
Size: 775259 Color: 19
Size: 112379 Color: 19
Size: 112351 Color: 4

Bin 1160: 12 of cap free
Amount of items: 2
Items: 
Size: 718194 Color: 5
Size: 281795 Color: 9

Bin 1161: 12 of cap free
Amount of items: 3
Items: 
Size: 639502 Color: 19
Size: 180633 Color: 1
Size: 179854 Color: 6

Bin 1162: 12 of cap free
Amount of items: 2
Items: 
Size: 719820 Color: 9
Size: 280169 Color: 15

Bin 1163: 12 of cap free
Amount of items: 3
Items: 
Size: 632997 Color: 10
Size: 185825 Color: 6
Size: 181167 Color: 19

Bin 1164: 12 of cap free
Amount of items: 2
Items: 
Size: 760035 Color: 10
Size: 239954 Color: 13

Bin 1165: 12 of cap free
Amount of items: 3
Items: 
Size: 640367 Color: 6
Size: 179948 Color: 8
Size: 179674 Color: 17

Bin 1166: 12 of cap free
Amount of items: 2
Items: 
Size: 697898 Color: 17
Size: 302091 Color: 3

Bin 1167: 12 of cap free
Amount of items: 3
Items: 
Size: 620370 Color: 8
Size: 271112 Color: 8
Size: 108507 Color: 1

Bin 1168: 12 of cap free
Amount of items: 2
Items: 
Size: 772035 Color: 6
Size: 227954 Color: 15

Bin 1169: 12 of cap free
Amount of items: 2
Items: 
Size: 686308 Color: 12
Size: 313681 Color: 16

Bin 1170: 12 of cap free
Amount of items: 2
Items: 
Size: 697371 Color: 19
Size: 302618 Color: 6

Bin 1171: 12 of cap free
Amount of items: 3
Items: 
Size: 642787 Color: 6
Size: 178801 Color: 19
Size: 178401 Color: 2

Bin 1172: 12 of cap free
Amount of items: 2
Items: 
Size: 796721 Color: 8
Size: 203268 Color: 2

Bin 1173: 12 of cap free
Amount of items: 2
Items: 
Size: 781559 Color: 4
Size: 218430 Color: 17

Bin 1174: 12 of cap free
Amount of items: 2
Items: 
Size: 513562 Color: 3
Size: 486427 Color: 0

Bin 1175: 12 of cap free
Amount of items: 3
Items: 
Size: 384974 Color: 4
Size: 311091 Color: 8
Size: 303924 Color: 19

Bin 1176: 12 of cap free
Amount of items: 3
Items: 
Size: 384001 Color: 17
Size: 321791 Color: 10
Size: 294197 Color: 9

Bin 1177: 12 of cap free
Amount of items: 2
Items: 
Size: 500441 Color: 12
Size: 499548 Color: 19

Bin 1178: 12 of cap free
Amount of items: 2
Items: 
Size: 501731 Color: 10
Size: 498258 Color: 3

Bin 1179: 12 of cap free
Amount of items: 2
Items: 
Size: 505476 Color: 9
Size: 494513 Color: 0

Bin 1180: 12 of cap free
Amount of items: 2
Items: 
Size: 529598 Color: 13
Size: 470391 Color: 15

Bin 1181: 12 of cap free
Amount of items: 2
Items: 
Size: 539428 Color: 13
Size: 460561 Color: 1

Bin 1182: 12 of cap free
Amount of items: 2
Items: 
Size: 548748 Color: 13
Size: 451241 Color: 9

Bin 1183: 12 of cap free
Amount of items: 2
Items: 
Size: 549915 Color: 2
Size: 450074 Color: 0

Bin 1184: 12 of cap free
Amount of items: 2
Items: 
Size: 563881 Color: 12
Size: 436108 Color: 0

Bin 1185: 12 of cap free
Amount of items: 2
Items: 
Size: 567001 Color: 4
Size: 432988 Color: 12

Bin 1186: 12 of cap free
Amount of items: 2
Items: 
Size: 571239 Color: 15
Size: 428750 Color: 7

Bin 1187: 12 of cap free
Amount of items: 2
Items: 
Size: 577924 Color: 16
Size: 422065 Color: 4

Bin 1188: 12 of cap free
Amount of items: 2
Items: 
Size: 578635 Color: 1
Size: 421354 Color: 13

Bin 1189: 12 of cap free
Amount of items: 2
Items: 
Size: 579387 Color: 9
Size: 420602 Color: 11

Bin 1190: 12 of cap free
Amount of items: 2
Items: 
Size: 582973 Color: 17
Size: 417016 Color: 3

Bin 1191: 12 of cap free
Amount of items: 2
Items: 
Size: 590306 Color: 11
Size: 409683 Color: 6

Bin 1192: 12 of cap free
Amount of items: 2
Items: 
Size: 606870 Color: 7
Size: 393119 Color: 12

Bin 1193: 12 of cap free
Amount of items: 3
Items: 
Size: 628958 Color: 19
Size: 185635 Color: 13
Size: 185396 Color: 11

Bin 1194: 12 of cap free
Amount of items: 2
Items: 
Size: 640948 Color: 8
Size: 359041 Color: 14

Bin 1195: 12 of cap free
Amount of items: 2
Items: 
Size: 650863 Color: 11
Size: 349126 Color: 15

Bin 1196: 12 of cap free
Amount of items: 2
Items: 
Size: 652558 Color: 5
Size: 347431 Color: 10

Bin 1197: 12 of cap free
Amount of items: 2
Items: 
Size: 678121 Color: 9
Size: 321868 Color: 18

Bin 1198: 12 of cap free
Amount of items: 2
Items: 
Size: 681248 Color: 9
Size: 318741 Color: 10

Bin 1199: 12 of cap free
Amount of items: 2
Items: 
Size: 691552 Color: 7
Size: 308437 Color: 14

Bin 1200: 12 of cap free
Amount of items: 2
Items: 
Size: 693182 Color: 14
Size: 306807 Color: 18

Bin 1201: 12 of cap free
Amount of items: 2
Items: 
Size: 725496 Color: 17
Size: 274493 Color: 4

Bin 1202: 12 of cap free
Amount of items: 2
Items: 
Size: 731589 Color: 4
Size: 268400 Color: 12

Bin 1203: 12 of cap free
Amount of items: 2
Items: 
Size: 738994 Color: 5
Size: 260995 Color: 0

Bin 1204: 12 of cap free
Amount of items: 2
Items: 
Size: 753489 Color: 6
Size: 246500 Color: 0

Bin 1205: 12 of cap free
Amount of items: 2
Items: 
Size: 760585 Color: 12
Size: 239404 Color: 9

Bin 1206: 13 of cap free
Amount of items: 2
Items: 
Size: 698547 Color: 1
Size: 301441 Color: 11

Bin 1207: 13 of cap free
Amount of items: 3
Items: 
Size: 766218 Color: 10
Size: 126067 Color: 12
Size: 107703 Color: 15

Bin 1208: 13 of cap free
Amount of items: 3
Items: 
Size: 664149 Color: 19
Size: 168341 Color: 16
Size: 167498 Color: 1

Bin 1209: 13 of cap free
Amount of items: 3
Items: 
Size: 756341 Color: 6
Size: 127141 Color: 19
Size: 116506 Color: 19

Bin 1210: 13 of cap free
Amount of items: 2
Items: 
Size: 773664 Color: 4
Size: 226324 Color: 9

Bin 1211: 13 of cap free
Amount of items: 2
Items: 
Size: 770601 Color: 13
Size: 229387 Color: 4

Bin 1212: 13 of cap free
Amount of items: 2
Items: 
Size: 783970 Color: 6
Size: 216018 Color: 3

Bin 1213: 13 of cap free
Amount of items: 2
Items: 
Size: 701604 Color: 4
Size: 298384 Color: 5

Bin 1214: 13 of cap free
Amount of items: 2
Items: 
Size: 793751 Color: 9
Size: 206237 Color: 13

Bin 1215: 13 of cap free
Amount of items: 2
Items: 
Size: 654478 Color: 11
Size: 345510 Color: 8

Bin 1216: 13 of cap free
Amount of items: 2
Items: 
Size: 695227 Color: 9
Size: 304761 Color: 0

Bin 1217: 13 of cap free
Amount of items: 3
Items: 
Size: 493355 Color: 13
Size: 284798 Color: 6
Size: 221835 Color: 0

Bin 1218: 13 of cap free
Amount of items: 3
Items: 
Size: 614025 Color: 8
Size: 193410 Color: 12
Size: 192553 Color: 1

Bin 1219: 13 of cap free
Amount of items: 2
Items: 
Size: 713280 Color: 13
Size: 286708 Color: 6

Bin 1220: 13 of cap free
Amount of items: 3
Items: 
Size: 611032 Color: 5
Size: 194898 Color: 17
Size: 194058 Color: 13

Bin 1221: 13 of cap free
Amount of items: 2
Items: 
Size: 788476 Color: 6
Size: 211512 Color: 14

Bin 1222: 13 of cap free
Amount of items: 3
Items: 
Size: 370230 Color: 2
Size: 320895 Color: 4
Size: 308863 Color: 18

Bin 1223: 13 of cap free
Amount of items: 3
Items: 
Size: 368162 Color: 11
Size: 362795 Color: 1
Size: 269031 Color: 13

Bin 1224: 13 of cap free
Amount of items: 3
Items: 
Size: 443459 Color: 19
Size: 287576 Color: 17
Size: 268953 Color: 4

Bin 1225: 13 of cap free
Amount of items: 2
Items: 
Size: 710145 Color: 18
Size: 289843 Color: 6

Bin 1226: 13 of cap free
Amount of items: 3
Items: 
Size: 370101 Color: 4
Size: 320920 Color: 16
Size: 308967 Color: 19

Bin 1227: 13 of cap free
Amount of items: 2
Items: 
Size: 504199 Color: 1
Size: 495789 Color: 17

Bin 1228: 13 of cap free
Amount of items: 2
Items: 
Size: 527426 Color: 17
Size: 472562 Color: 11

Bin 1229: 13 of cap free
Amount of items: 2
Items: 
Size: 552015 Color: 3
Size: 447973 Color: 17

Bin 1230: 13 of cap free
Amount of items: 2
Items: 
Size: 557372 Color: 16
Size: 442616 Color: 9

Bin 1231: 13 of cap free
Amount of items: 2
Items: 
Size: 559501 Color: 7
Size: 440487 Color: 6

Bin 1232: 13 of cap free
Amount of items: 2
Items: 
Size: 566335 Color: 10
Size: 433653 Color: 4

Bin 1233: 13 of cap free
Amount of items: 2
Items: 
Size: 568317 Color: 3
Size: 431671 Color: 19

Bin 1234: 13 of cap free
Amount of items: 2
Items: 
Size: 596568 Color: 0
Size: 403420 Color: 5

Bin 1235: 13 of cap free
Amount of items: 2
Items: 
Size: 605529 Color: 11
Size: 394459 Color: 19

Bin 1236: 13 of cap free
Amount of items: 2
Items: 
Size: 611865 Color: 8
Size: 388123 Color: 14

Bin 1237: 13 of cap free
Amount of items: 3
Items: 
Size: 618586 Color: 3
Size: 190731 Color: 2
Size: 190671 Color: 7

Bin 1238: 13 of cap free
Amount of items: 2
Items: 
Size: 621325 Color: 9
Size: 378663 Color: 1

Bin 1239: 13 of cap free
Amount of items: 3
Items: 
Size: 636131 Color: 12
Size: 182087 Color: 11
Size: 181770 Color: 19

Bin 1240: 13 of cap free
Amount of items: 2
Items: 
Size: 637407 Color: 18
Size: 362581 Color: 12

Bin 1241: 13 of cap free
Amount of items: 2
Items: 
Size: 646116 Color: 17
Size: 353872 Color: 16

Bin 1242: 13 of cap free
Amount of items: 2
Items: 
Size: 647679 Color: 12
Size: 352309 Color: 7

Bin 1243: 13 of cap free
Amount of items: 2
Items: 
Size: 650990 Color: 10
Size: 348998 Color: 11

Bin 1244: 13 of cap free
Amount of items: 2
Items: 
Size: 660887 Color: 13
Size: 339101 Color: 5

Bin 1245: 13 of cap free
Amount of items: 2
Items: 
Size: 660993 Color: 5
Size: 338995 Color: 19

Bin 1246: 13 of cap free
Amount of items: 2
Items: 
Size: 675540 Color: 14
Size: 324448 Color: 1

Bin 1247: 13 of cap free
Amount of items: 2
Items: 
Size: 687321 Color: 13
Size: 312667 Color: 18

Bin 1248: 13 of cap free
Amount of items: 2
Items: 
Size: 722371 Color: 19
Size: 277617 Color: 5

Bin 1249: 13 of cap free
Amount of items: 2
Items: 
Size: 733074 Color: 0
Size: 266914 Color: 7

Bin 1250: 13 of cap free
Amount of items: 2
Items: 
Size: 772621 Color: 11
Size: 227367 Color: 1

Bin 1251: 13 of cap free
Amount of items: 2
Items: 
Size: 778863 Color: 8
Size: 221125 Color: 15

Bin 1252: 13 of cap free
Amount of items: 2
Items: 
Size: 782018 Color: 10
Size: 217970 Color: 1

Bin 1253: 13 of cap free
Amount of items: 2
Items: 
Size: 783193 Color: 1
Size: 216795 Color: 5

Bin 1254: 13 of cap free
Amount of items: 2
Items: 
Size: 790271 Color: 9
Size: 209717 Color: 3

Bin 1255: 14 of cap free
Amount of items: 3
Items: 
Size: 732464 Color: 1
Size: 146259 Color: 19
Size: 121264 Color: 18

Bin 1256: 14 of cap free
Amount of items: 2
Items: 
Size: 649779 Color: 9
Size: 350208 Color: 13

Bin 1257: 14 of cap free
Amount of items: 3
Items: 
Size: 789610 Color: 0
Size: 107308 Color: 6
Size: 103069 Color: 5

Bin 1258: 14 of cap free
Amount of items: 2
Items: 
Size: 728009 Color: 13
Size: 271978 Color: 8

Bin 1259: 14 of cap free
Amount of items: 3
Items: 
Size: 748274 Color: 1
Size: 127618 Color: 16
Size: 124095 Color: 0

Bin 1260: 14 of cap free
Amount of items: 3
Items: 
Size: 721676 Color: 9
Size: 141718 Color: 17
Size: 136593 Color: 6

Bin 1261: 14 of cap free
Amount of items: 3
Items: 
Size: 384270 Color: 0
Size: 346799 Color: 1
Size: 268918 Color: 13

Bin 1262: 14 of cap free
Amount of items: 2
Items: 
Size: 536207 Color: 19
Size: 463780 Color: 0

Bin 1263: 14 of cap free
Amount of items: 3
Items: 
Size: 630211 Color: 7
Size: 185264 Color: 0
Size: 184512 Color: 2

Bin 1264: 14 of cap free
Amount of items: 2
Items: 
Size: 639467 Color: 7
Size: 360520 Color: 18

Bin 1265: 14 of cap free
Amount of items: 2
Items: 
Size: 673879 Color: 18
Size: 326108 Color: 16

Bin 1266: 14 of cap free
Amount of items: 2
Items: 
Size: 524502 Color: 19
Size: 475485 Color: 3

Bin 1267: 14 of cap free
Amount of items: 2
Items: 
Size: 683110 Color: 9
Size: 316877 Color: 2

Bin 1268: 14 of cap free
Amount of items: 2
Items: 
Size: 702954 Color: 8
Size: 297033 Color: 17

Bin 1269: 14 of cap free
Amount of items: 2
Items: 
Size: 502547 Color: 4
Size: 497440 Color: 10

Bin 1270: 14 of cap free
Amount of items: 2
Items: 
Size: 504007 Color: 17
Size: 495980 Color: 8

Bin 1271: 14 of cap free
Amount of items: 2
Items: 
Size: 507131 Color: 0
Size: 492856 Color: 7

Bin 1272: 14 of cap free
Amount of items: 2
Items: 
Size: 520056 Color: 12
Size: 479931 Color: 8

Bin 1273: 14 of cap free
Amount of items: 2
Items: 
Size: 524124 Color: 17
Size: 475863 Color: 2

Bin 1274: 14 of cap free
Amount of items: 2
Items: 
Size: 525660 Color: 5
Size: 474327 Color: 0

Bin 1275: 14 of cap free
Amount of items: 2
Items: 
Size: 526444 Color: 16
Size: 473543 Color: 8

Bin 1276: 14 of cap free
Amount of items: 2
Items: 
Size: 532467 Color: 10
Size: 467520 Color: 16

Bin 1277: 14 of cap free
Amount of items: 2
Items: 
Size: 533763 Color: 14
Size: 466224 Color: 12

Bin 1278: 14 of cap free
Amount of items: 2
Items: 
Size: 544777 Color: 13
Size: 455210 Color: 14

Bin 1279: 14 of cap free
Amount of items: 2
Items: 
Size: 560179 Color: 0
Size: 439808 Color: 13

Bin 1280: 14 of cap free
Amount of items: 2
Items: 
Size: 564855 Color: 18
Size: 435132 Color: 1

Bin 1281: 14 of cap free
Amount of items: 2
Items: 
Size: 574301 Color: 14
Size: 425686 Color: 8

Bin 1282: 14 of cap free
Amount of items: 2
Items: 
Size: 577670 Color: 15
Size: 422317 Color: 7

Bin 1283: 14 of cap free
Amount of items: 2
Items: 
Size: 585109 Color: 0
Size: 414878 Color: 10

Bin 1284: 14 of cap free
Amount of items: 2
Items: 
Size: 591885 Color: 7
Size: 408102 Color: 0

Bin 1285: 14 of cap free
Amount of items: 2
Items: 
Size: 593457 Color: 7
Size: 406530 Color: 14

Bin 1286: 14 of cap free
Amount of items: 2
Items: 
Size: 594666 Color: 7
Size: 405321 Color: 19

Bin 1287: 14 of cap free
Amount of items: 2
Items: 
Size: 600620 Color: 12
Size: 399367 Color: 7

Bin 1288: 14 of cap free
Amount of items: 2
Items: 
Size: 631650 Color: 0
Size: 368337 Color: 4

Bin 1289: 14 of cap free
Amount of items: 2
Items: 
Size: 652909 Color: 5
Size: 347078 Color: 11

Bin 1290: 14 of cap free
Amount of items: 2
Items: 
Size: 682991 Color: 13
Size: 316996 Color: 3

Bin 1291: 14 of cap free
Amount of items: 2
Items: 
Size: 709373 Color: 14
Size: 290614 Color: 12

Bin 1292: 14 of cap free
Amount of items: 2
Items: 
Size: 715980 Color: 5
Size: 284007 Color: 10

Bin 1293: 14 of cap free
Amount of items: 2
Items: 
Size: 719476 Color: 7
Size: 280511 Color: 13

Bin 1294: 14 of cap free
Amount of items: 2
Items: 
Size: 727283 Color: 8
Size: 272704 Color: 5

Bin 1295: 14 of cap free
Amount of items: 2
Items: 
Size: 731800 Color: 14
Size: 268187 Color: 7

Bin 1296: 14 of cap free
Amount of items: 2
Items: 
Size: 741523 Color: 12
Size: 258464 Color: 9

Bin 1297: 14 of cap free
Amount of items: 2
Items: 
Size: 746865 Color: 8
Size: 253122 Color: 10

Bin 1298: 14 of cap free
Amount of items: 2
Items: 
Size: 770487 Color: 0
Size: 229500 Color: 9

Bin 1299: 15 of cap free
Amount of items: 3
Items: 
Size: 742529 Color: 6
Size: 150312 Color: 13
Size: 107145 Color: 16

Bin 1300: 15 of cap free
Amount of items: 2
Items: 
Size: 779604 Color: 1
Size: 220382 Color: 9

Bin 1301: 15 of cap free
Amount of items: 3
Items: 
Size: 653578 Color: 13
Size: 173248 Color: 2
Size: 173160 Color: 18

Bin 1302: 15 of cap free
Amount of items: 3
Items: 
Size: 676207 Color: 5
Size: 161935 Color: 7
Size: 161844 Color: 14

Bin 1303: 15 of cap free
Amount of items: 2
Items: 
Size: 732550 Color: 9
Size: 267436 Color: 0

Bin 1304: 15 of cap free
Amount of items: 3
Items: 
Size: 792860 Color: 17
Size: 106476 Color: 12
Size: 100650 Color: 10

Bin 1305: 15 of cap free
Amount of items: 3
Items: 
Size: 716418 Color: 8
Size: 143734 Color: 4
Size: 139834 Color: 6

Bin 1306: 15 of cap free
Amount of items: 3
Items: 
Size: 381907 Color: 1
Size: 310028 Color: 1
Size: 308051 Color: 9

Bin 1307: 15 of cap free
Amount of items: 3
Items: 
Size: 475784 Color: 14
Size: 362723 Color: 15
Size: 161479 Color: 18

Bin 1308: 15 of cap free
Amount of items: 2
Items: 
Size: 645036 Color: 14
Size: 354950 Color: 6

Bin 1309: 15 of cap free
Amount of items: 2
Items: 
Size: 673128 Color: 13
Size: 326858 Color: 5

Bin 1310: 15 of cap free
Amount of items: 2
Items: 
Size: 699853 Color: 4
Size: 300133 Color: 0

Bin 1311: 15 of cap free
Amount of items: 2
Items: 
Size: 622144 Color: 16
Size: 377842 Color: 17

Bin 1312: 15 of cap free
Amount of items: 2
Items: 
Size: 606438 Color: 18
Size: 393548 Color: 6

Bin 1313: 15 of cap free
Amount of items: 2
Items: 
Size: 602610 Color: 6
Size: 397376 Color: 11

Bin 1314: 15 of cap free
Amount of items: 2
Items: 
Size: 638446 Color: 9
Size: 361540 Color: 6

Bin 1315: 15 of cap free
Amount of items: 2
Items: 
Size: 523804 Color: 12
Size: 476182 Color: 18

Bin 1316: 15 of cap free
Amount of items: 2
Items: 
Size: 526428 Color: 11
Size: 473558 Color: 3

Bin 1317: 15 of cap free
Amount of items: 2
Items: 
Size: 530497 Color: 4
Size: 469489 Color: 3

Bin 1318: 15 of cap free
Amount of items: 2
Items: 
Size: 533224 Color: 12
Size: 466762 Color: 6

Bin 1319: 15 of cap free
Amount of items: 2
Items: 
Size: 540049 Color: 19
Size: 459937 Color: 4

Bin 1320: 15 of cap free
Amount of items: 2
Items: 
Size: 544288 Color: 19
Size: 455698 Color: 0

Bin 1321: 15 of cap free
Amount of items: 2
Items: 
Size: 547895 Color: 0
Size: 452091 Color: 14

Bin 1322: 15 of cap free
Amount of items: 2
Items: 
Size: 559001 Color: 9
Size: 440985 Color: 11

Bin 1323: 15 of cap free
Amount of items: 2
Items: 
Size: 570550 Color: 1
Size: 429436 Color: 9

Bin 1324: 15 of cap free
Amount of items: 2
Items: 
Size: 572527 Color: 11
Size: 427459 Color: 14

Bin 1325: 15 of cap free
Amount of items: 2
Items: 
Size: 575720 Color: 4
Size: 424266 Color: 6

Bin 1326: 15 of cap free
Amount of items: 2
Items: 
Size: 576414 Color: 2
Size: 423572 Color: 1

Bin 1327: 15 of cap free
Amount of items: 2
Items: 
Size: 578918 Color: 5
Size: 421068 Color: 3

Bin 1328: 15 of cap free
Amount of items: 2
Items: 
Size: 581541 Color: 13
Size: 418445 Color: 8

Bin 1329: 15 of cap free
Amount of items: 2
Items: 
Size: 589324 Color: 1
Size: 410662 Color: 17

Bin 1330: 15 of cap free
Amount of items: 2
Items: 
Size: 591953 Color: 10
Size: 408033 Color: 7

Bin 1331: 15 of cap free
Amount of items: 2
Items: 
Size: 599776 Color: 16
Size: 400210 Color: 7

Bin 1332: 15 of cap free
Amount of items: 2
Items: 
Size: 613868 Color: 3
Size: 386118 Color: 0

Bin 1333: 15 of cap free
Amount of items: 2
Items: 
Size: 617338 Color: 9
Size: 382648 Color: 2

Bin 1334: 15 of cap free
Amount of items: 2
Items: 
Size: 632708 Color: 13
Size: 367278 Color: 9

Bin 1335: 15 of cap free
Amount of items: 3
Items: 
Size: 639104 Color: 8
Size: 180469 Color: 0
Size: 180413 Color: 10

Bin 1336: 15 of cap free
Amount of items: 2
Items: 
Size: 642227 Color: 16
Size: 357759 Color: 18

Bin 1337: 15 of cap free
Amount of items: 2
Items: 
Size: 653567 Color: 12
Size: 346419 Color: 5

Bin 1338: 15 of cap free
Amount of items: 2
Items: 
Size: 659724 Color: 19
Size: 340262 Color: 9

Bin 1339: 15 of cap free
Amount of items: 2
Items: 
Size: 672582 Color: 2
Size: 327404 Color: 0

Bin 1340: 15 of cap free
Amount of items: 2
Items: 
Size: 680005 Color: 5
Size: 319981 Color: 0

Bin 1341: 15 of cap free
Amount of items: 2
Items: 
Size: 680383 Color: 13
Size: 319603 Color: 9

Bin 1342: 15 of cap free
Amount of items: 2
Items: 
Size: 706355 Color: 4
Size: 293631 Color: 3

Bin 1343: 15 of cap free
Amount of items: 2
Items: 
Size: 710458 Color: 3
Size: 289528 Color: 14

Bin 1344: 15 of cap free
Amount of items: 2
Items: 
Size: 714228 Color: 17
Size: 285758 Color: 3

Bin 1345: 15 of cap free
Amount of items: 2
Items: 
Size: 716856 Color: 8
Size: 283130 Color: 17

Bin 1346: 15 of cap free
Amount of items: 2
Items: 
Size: 736728 Color: 11
Size: 263258 Color: 10

Bin 1347: 15 of cap free
Amount of items: 2
Items: 
Size: 738417 Color: 3
Size: 261569 Color: 15

Bin 1348: 15 of cap free
Amount of items: 2
Items: 
Size: 745106 Color: 10
Size: 254880 Color: 15

Bin 1349: 15 of cap free
Amount of items: 2
Items: 
Size: 782441 Color: 5
Size: 217545 Color: 19

Bin 1350: 16 of cap free
Amount of items: 2
Items: 
Size: 778177 Color: 15
Size: 221808 Color: 6

Bin 1351: 16 of cap free
Amount of items: 3
Items: 
Size: 718989 Color: 17
Size: 140620 Color: 13
Size: 140376 Color: 19

Bin 1352: 16 of cap free
Amount of items: 3
Items: 
Size: 699624 Color: 11
Size: 150199 Color: 10
Size: 150162 Color: 0

Bin 1353: 16 of cap free
Amount of items: 3
Items: 
Size: 527034 Color: 0
Size: 324586 Color: 6
Size: 148365 Color: 15

Bin 1354: 16 of cap free
Amount of items: 2
Items: 
Size: 721045 Color: 12
Size: 278940 Color: 2

Bin 1355: 16 of cap free
Amount of items: 2
Items: 
Size: 664937 Color: 6
Size: 335048 Color: 12

Bin 1356: 16 of cap free
Amount of items: 2
Items: 
Size: 675973 Color: 5
Size: 324012 Color: 12

Bin 1357: 16 of cap free
Amount of items: 3
Items: 
Size: 673452 Color: 17
Size: 179201 Color: 7
Size: 147332 Color: 10

Bin 1358: 16 of cap free
Amount of items: 3
Items: 
Size: 668362 Color: 19
Size: 179251 Color: 16
Size: 152372 Color: 11

Bin 1359: 16 of cap free
Amount of items: 2
Items: 
Size: 689745 Color: 11
Size: 310240 Color: 16

Bin 1360: 16 of cap free
Amount of items: 3
Items: 
Size: 419373 Color: 5
Size: 308776 Color: 14
Size: 271836 Color: 4

Bin 1361: 16 of cap free
Amount of items: 2
Items: 
Size: 688407 Color: 10
Size: 311578 Color: 7

Bin 1362: 16 of cap free
Amount of items: 2
Items: 
Size: 500689 Color: 12
Size: 499296 Color: 1

Bin 1363: 16 of cap free
Amount of items: 2
Items: 
Size: 503549 Color: 17
Size: 496436 Color: 5

Bin 1364: 16 of cap free
Amount of items: 2
Items: 
Size: 504653 Color: 7
Size: 495332 Color: 17

Bin 1365: 16 of cap free
Amount of items: 2
Items: 
Size: 505669 Color: 12
Size: 494316 Color: 8

Bin 1366: 16 of cap free
Amount of items: 2
Items: 
Size: 522844 Color: 7
Size: 477141 Color: 8

Bin 1367: 16 of cap free
Amount of items: 2
Items: 
Size: 526399 Color: 17
Size: 473586 Color: 16

Bin 1368: 16 of cap free
Amount of items: 2
Items: 
Size: 527402 Color: 9
Size: 472583 Color: 8

Bin 1369: 16 of cap free
Amount of items: 2
Items: 
Size: 548401 Color: 13
Size: 451584 Color: 7

Bin 1370: 16 of cap free
Amount of items: 2
Items: 
Size: 552122 Color: 9
Size: 447863 Color: 19

Bin 1371: 16 of cap free
Amount of items: 2
Items: 
Size: 572492 Color: 6
Size: 427493 Color: 13

Bin 1372: 16 of cap free
Amount of items: 2
Items: 
Size: 577860 Color: 17
Size: 422125 Color: 16

Bin 1373: 16 of cap free
Amount of items: 2
Items: 
Size: 592676 Color: 1
Size: 407309 Color: 18

Bin 1374: 16 of cap free
Amount of items: 2
Items: 
Size: 593841 Color: 9
Size: 406144 Color: 19

Bin 1375: 16 of cap free
Amount of items: 2
Items: 
Size: 595911 Color: 2
Size: 404074 Color: 8

Bin 1376: 16 of cap free
Amount of items: 2
Items: 
Size: 614468 Color: 0
Size: 385517 Color: 1

Bin 1377: 16 of cap free
Amount of items: 2
Items: 
Size: 616395 Color: 19
Size: 383590 Color: 3

Bin 1378: 16 of cap free
Amount of items: 2
Items: 
Size: 629465 Color: 4
Size: 370520 Color: 19

Bin 1379: 16 of cap free
Amount of items: 2
Items: 
Size: 647372 Color: 17
Size: 352613 Color: 5

Bin 1380: 16 of cap free
Amount of items: 2
Items: 
Size: 666615 Color: 3
Size: 333370 Color: 14

Bin 1381: 16 of cap free
Amount of items: 2
Items: 
Size: 666966 Color: 9
Size: 333019 Color: 17

Bin 1382: 16 of cap free
Amount of items: 2
Items: 
Size: 679254 Color: 17
Size: 320731 Color: 16

Bin 1383: 16 of cap free
Amount of items: 2
Items: 
Size: 714429 Color: 2
Size: 285556 Color: 3

Bin 1384: 16 of cap free
Amount of items: 2
Items: 
Size: 722831 Color: 0
Size: 277154 Color: 3

Bin 1385: 16 of cap free
Amount of items: 2
Items: 
Size: 725113 Color: 3
Size: 274872 Color: 5

Bin 1386: 16 of cap free
Amount of items: 2
Items: 
Size: 730913 Color: 8
Size: 269072 Color: 9

Bin 1387: 16 of cap free
Amount of items: 2
Items: 
Size: 763094 Color: 17
Size: 236891 Color: 19

Bin 1388: 16 of cap free
Amount of items: 2
Items: 
Size: 796009 Color: 4
Size: 203976 Color: 16

Bin 1389: 17 of cap free
Amount of items: 3
Items: 
Size: 642888 Color: 2
Size: 189454 Color: 12
Size: 167642 Color: 5

Bin 1390: 17 of cap free
Amount of items: 3
Items: 
Size: 642718 Color: 13
Size: 189580 Color: 0
Size: 167686 Color: 17

Bin 1391: 17 of cap free
Amount of items: 3
Items: 
Size: 702811 Color: 8
Size: 148988 Color: 19
Size: 148185 Color: 19

Bin 1392: 17 of cap free
Amount of items: 2
Items: 
Size: 617306 Color: 3
Size: 382678 Color: 1

Bin 1393: 17 of cap free
Amount of items: 3
Items: 
Size: 718440 Color: 12
Size: 142105 Color: 18
Size: 139439 Color: 8

Bin 1394: 17 of cap free
Amount of items: 2
Items: 
Size: 759995 Color: 14
Size: 239989 Color: 19

Bin 1395: 17 of cap free
Amount of items: 3
Items: 
Size: 747376 Color: 12
Size: 127224 Color: 3
Size: 125384 Color: 2

Bin 1396: 17 of cap free
Amount of items: 3
Items: 
Size: 642683 Color: 15
Size: 189248 Color: 15
Size: 168053 Color: 3

Bin 1397: 17 of cap free
Amount of items: 3
Items: 
Size: 619431 Color: 1
Size: 191157 Color: 18
Size: 189396 Color: 12

Bin 1398: 17 of cap free
Amount of items: 2
Items: 
Size: 692707 Color: 4
Size: 307277 Color: 11

Bin 1399: 17 of cap free
Amount of items: 2
Items: 
Size: 670120 Color: 0
Size: 329864 Color: 8

Bin 1400: 17 of cap free
Amount of items: 3
Items: 
Size: 621206 Color: 10
Size: 189488 Color: 4
Size: 189290 Color: 7

Bin 1401: 17 of cap free
Amount of items: 2
Items: 
Size: 603295 Color: 11
Size: 396689 Color: 19

Bin 1402: 17 of cap free
Amount of items: 2
Items: 
Size: 796598 Color: 19
Size: 203386 Color: 6

Bin 1403: 17 of cap free
Amount of items: 2
Items: 
Size: 798524 Color: 19
Size: 201460 Color: 6

Bin 1404: 17 of cap free
Amount of items: 2
Items: 
Size: 507890 Color: 12
Size: 492094 Color: 17

Bin 1405: 17 of cap free
Amount of items: 2
Items: 
Size: 519115 Color: 5
Size: 480869 Color: 18

Bin 1406: 17 of cap free
Amount of items: 2
Items: 
Size: 535208 Color: 10
Size: 464776 Color: 14

Bin 1407: 17 of cap free
Amount of items: 2
Items: 
Size: 552216 Color: 6
Size: 447768 Color: 13

Bin 1408: 17 of cap free
Amount of items: 2
Items: 
Size: 565173 Color: 12
Size: 434811 Color: 1

Bin 1409: 17 of cap free
Amount of items: 2
Items: 
Size: 565941 Color: 4
Size: 434043 Color: 15

Bin 1410: 17 of cap free
Amount of items: 2
Items: 
Size: 571574 Color: 12
Size: 428410 Color: 8

Bin 1411: 17 of cap free
Amount of items: 2
Items: 
Size: 579669 Color: 15
Size: 420315 Color: 5

Bin 1412: 17 of cap free
Amount of items: 2
Items: 
Size: 585348 Color: 8
Size: 414636 Color: 12

Bin 1413: 17 of cap free
Amount of items: 2
Items: 
Size: 586819 Color: 12
Size: 413165 Color: 8

Bin 1414: 17 of cap free
Amount of items: 2
Items: 
Size: 607770 Color: 2
Size: 392214 Color: 13

Bin 1415: 17 of cap free
Amount of items: 2
Items: 
Size: 620488 Color: 9
Size: 379496 Color: 11

Bin 1416: 17 of cap free
Amount of items: 2
Items: 
Size: 647043 Color: 7
Size: 352941 Color: 10

Bin 1417: 17 of cap free
Amount of items: 2
Items: 
Size: 660712 Color: 11
Size: 339272 Color: 2

Bin 1418: 17 of cap free
Amount of items: 2
Items: 
Size: 669195 Color: 4
Size: 330789 Color: 0

Bin 1419: 17 of cap free
Amount of items: 2
Items: 
Size: 685251 Color: 11
Size: 314733 Color: 8

Bin 1420: 17 of cap free
Amount of items: 2
Items: 
Size: 737149 Color: 16
Size: 262835 Color: 15

Bin 1421: 17 of cap free
Amount of items: 2
Items: 
Size: 738868 Color: 2
Size: 261116 Color: 12

Bin 1422: 17 of cap free
Amount of items: 2
Items: 
Size: 751355 Color: 15
Size: 248629 Color: 19

Bin 1423: 17 of cap free
Amount of items: 2
Items: 
Size: 762419 Color: 1
Size: 237565 Color: 15

Bin 1424: 17 of cap free
Amount of items: 2
Items: 
Size: 785207 Color: 17
Size: 214777 Color: 14

Bin 1425: 18 of cap free
Amount of items: 3
Items: 
Size: 535169 Color: 14
Size: 349463 Color: 11
Size: 115351 Color: 14

Bin 1426: 18 of cap free
Amount of items: 2
Items: 
Size: 686232 Color: 14
Size: 313751 Color: 8

Bin 1427: 18 of cap free
Amount of items: 3
Items: 
Size: 618663 Color: 8
Size: 190693 Color: 3
Size: 190627 Color: 16

Bin 1428: 18 of cap free
Amount of items: 3
Items: 
Size: 713061 Color: 9
Size: 146115 Color: 13
Size: 140807 Color: 6

Bin 1429: 18 of cap free
Amount of items: 3
Items: 
Size: 523627 Color: 14
Size: 337235 Color: 11
Size: 139121 Color: 6

Bin 1430: 18 of cap free
Amount of items: 3
Items: 
Size: 407078 Color: 0
Size: 338485 Color: 16
Size: 254420 Color: 17

Bin 1431: 18 of cap free
Amount of items: 2
Items: 
Size: 612029 Color: 9
Size: 387954 Color: 11

Bin 1432: 18 of cap free
Amount of items: 2
Items: 
Size: 620843 Color: 1
Size: 379140 Color: 19

Bin 1433: 18 of cap free
Amount of items: 2
Items: 
Size: 797123 Color: 16
Size: 202860 Color: 5

Bin 1434: 18 of cap free
Amount of items: 2
Items: 
Size: 744149 Color: 6
Size: 255834 Color: 14

Bin 1435: 18 of cap free
Amount of items: 2
Items: 
Size: 762982 Color: 0
Size: 237001 Color: 16

Bin 1436: 18 of cap free
Amount of items: 3
Items: 
Size: 415476 Color: 0
Size: 292362 Color: 6
Size: 292145 Color: 3

Bin 1437: 18 of cap free
Amount of items: 3
Items: 
Size: 605969 Color: 4
Size: 197072 Color: 6
Size: 196942 Color: 0

Bin 1438: 18 of cap free
Amount of items: 2
Items: 
Size: 664319 Color: 13
Size: 335664 Color: 16

Bin 1439: 18 of cap free
Amount of items: 2
Items: 
Size: 662628 Color: 14
Size: 337355 Color: 13

Bin 1440: 18 of cap free
Amount of items: 2
Items: 
Size: 524435 Color: 14
Size: 475548 Color: 15

Bin 1441: 18 of cap free
Amount of items: 2
Items: 
Size: 519837 Color: 13
Size: 480146 Color: 10

Bin 1442: 18 of cap free
Amount of items: 2
Items: 
Size: 537110 Color: 11
Size: 462873 Color: 17

Bin 1443: 18 of cap free
Amount of items: 2
Items: 
Size: 553514 Color: 12
Size: 446469 Color: 6

Bin 1444: 18 of cap free
Amount of items: 2
Items: 
Size: 571596 Color: 7
Size: 428387 Color: 14

Bin 1445: 18 of cap free
Amount of items: 2
Items: 
Size: 574322 Color: 8
Size: 425661 Color: 18

Bin 1446: 18 of cap free
Amount of items: 2
Items: 
Size: 586329 Color: 1
Size: 413654 Color: 5

Bin 1447: 18 of cap free
Amount of items: 2
Items: 
Size: 592564 Color: 11
Size: 407419 Color: 13

Bin 1448: 18 of cap free
Amount of items: 2
Items: 
Size: 621410 Color: 19
Size: 378573 Color: 16

Bin 1449: 18 of cap free
Amount of items: 2
Items: 
Size: 630422 Color: 3
Size: 369561 Color: 15

Bin 1450: 18 of cap free
Amount of items: 3
Items: 
Size: 645145 Color: 6
Size: 177633 Color: 7
Size: 177205 Color: 16

Bin 1451: 18 of cap free
Amount of items: 2
Items: 
Size: 653863 Color: 8
Size: 346120 Color: 0

Bin 1452: 18 of cap free
Amount of items: 2
Items: 
Size: 692424 Color: 17
Size: 307559 Color: 2

Bin 1453: 18 of cap free
Amount of items: 2
Items: 
Size: 720029 Color: 3
Size: 279954 Color: 11

Bin 1454: 18 of cap free
Amount of items: 2
Items: 
Size: 721350 Color: 11
Size: 278633 Color: 3

Bin 1455: 18 of cap free
Amount of items: 2
Items: 
Size: 723515 Color: 7
Size: 276468 Color: 4

Bin 1456: 18 of cap free
Amount of items: 2
Items: 
Size: 738744 Color: 18
Size: 261239 Color: 2

Bin 1457: 18 of cap free
Amount of items: 2
Items: 
Size: 745680 Color: 7
Size: 254303 Color: 5

Bin 1458: 18 of cap free
Amount of items: 2
Items: 
Size: 759039 Color: 15
Size: 240944 Color: 14

Bin 1459: 19 of cap free
Amount of items: 3
Items: 
Size: 709667 Color: 14
Size: 150072 Color: 7
Size: 140243 Color: 12

Bin 1460: 19 of cap free
Amount of items: 3
Items: 
Size: 514028 Color: 4
Size: 250254 Color: 0
Size: 235700 Color: 10

Bin 1461: 19 of cap free
Amount of items: 3
Items: 
Size: 616250 Color: 3
Size: 192955 Color: 9
Size: 190777 Color: 5

Bin 1462: 19 of cap free
Amount of items: 3
Items: 
Size: 670963 Color: 6
Size: 165171 Color: 5
Size: 163848 Color: 2

Bin 1463: 19 of cap free
Amount of items: 2
Items: 
Size: 657780 Color: 16
Size: 342202 Color: 3

Bin 1464: 19 of cap free
Amount of items: 3
Items: 
Size: 709114 Color: 0
Size: 150164 Color: 12
Size: 140704 Color: 17

Bin 1465: 19 of cap free
Amount of items: 3
Items: 
Size: 516381 Color: 11
Size: 302545 Color: 4
Size: 181056 Color: 18

Bin 1466: 19 of cap free
Amount of items: 2
Items: 
Size: 644774 Color: 5
Size: 355208 Color: 12

Bin 1467: 19 of cap free
Amount of items: 3
Items: 
Size: 524761 Color: 5
Size: 258770 Color: 12
Size: 216451 Color: 1

Bin 1468: 19 of cap free
Amount of items: 2
Items: 
Size: 715544 Color: 19
Size: 284438 Color: 6

Bin 1469: 19 of cap free
Amount of items: 2
Items: 
Size: 607050 Color: 19
Size: 392932 Color: 6

Bin 1470: 19 of cap free
Amount of items: 2
Items: 
Size: 674582 Color: 6
Size: 325400 Color: 15

Bin 1471: 19 of cap free
Amount of items: 2
Items: 
Size: 611165 Color: 6
Size: 388817 Color: 4

Bin 1472: 19 of cap free
Amount of items: 2
Items: 
Size: 768344 Color: 8
Size: 231638 Color: 6

Bin 1473: 19 of cap free
Amount of items: 2
Items: 
Size: 596327 Color: 9
Size: 403655 Color: 17

Bin 1474: 19 of cap free
Amount of items: 3
Items: 
Size: 386690 Color: 1
Size: 309331 Color: 5
Size: 303961 Color: 8

Bin 1475: 19 of cap free
Amount of items: 2
Items: 
Size: 500526 Color: 10
Size: 499456 Color: 7

Bin 1476: 19 of cap free
Amount of items: 2
Items: 
Size: 525240 Color: 2
Size: 474742 Color: 0

Bin 1477: 19 of cap free
Amount of items: 2
Items: 
Size: 526173 Color: 16
Size: 473809 Color: 3

Bin 1478: 19 of cap free
Amount of items: 2
Items: 
Size: 542025 Color: 7
Size: 457957 Color: 8

Bin 1479: 19 of cap free
Amount of items: 2
Items: 
Size: 556952 Color: 12
Size: 443030 Color: 4

Bin 1480: 19 of cap free
Amount of items: 2
Items: 
Size: 569245 Color: 0
Size: 430737 Color: 14

Bin 1481: 19 of cap free
Amount of items: 2
Items: 
Size: 575067 Color: 16
Size: 424915 Color: 19

Bin 1482: 19 of cap free
Amount of items: 2
Items: 
Size: 583521 Color: 0
Size: 416461 Color: 9

Bin 1483: 19 of cap free
Amount of items: 2
Items: 
Size: 591338 Color: 11
Size: 408644 Color: 7

Bin 1484: 19 of cap free
Amount of items: 2
Items: 
Size: 600557 Color: 9
Size: 399425 Color: 13

Bin 1485: 19 of cap free
Amount of items: 2
Items: 
Size: 616832 Color: 3
Size: 383150 Color: 4

Bin 1486: 19 of cap free
Amount of items: 2
Items: 
Size: 624216 Color: 14
Size: 375766 Color: 2

Bin 1487: 19 of cap free
Amount of items: 3
Items: 
Size: 648538 Color: 18
Size: 175851 Color: 19
Size: 175593 Color: 12

Bin 1488: 19 of cap free
Amount of items: 2
Items: 
Size: 665937 Color: 0
Size: 334045 Color: 4

Bin 1489: 19 of cap free
Amount of items: 2
Items: 
Size: 669960 Color: 13
Size: 330022 Color: 8

Bin 1490: 19 of cap free
Amount of items: 2
Items: 
Size: 677489 Color: 16
Size: 322493 Color: 13

Bin 1491: 19 of cap free
Amount of items: 2
Items: 
Size: 677922 Color: 0
Size: 322060 Color: 2

Bin 1492: 19 of cap free
Amount of items: 2
Items: 
Size: 689308 Color: 19
Size: 310674 Color: 14

Bin 1493: 19 of cap free
Amount of items: 2
Items: 
Size: 703277 Color: 14
Size: 296705 Color: 17

Bin 1494: 19 of cap free
Amount of items: 2
Items: 
Size: 742473 Color: 19
Size: 257509 Color: 5

Bin 1495: 19 of cap free
Amount of items: 2
Items: 
Size: 773588 Color: 2
Size: 226394 Color: 7

Bin 1496: 19 of cap free
Amount of items: 2
Items: 
Size: 792013 Color: 5
Size: 207969 Color: 3

Bin 1497: 19 of cap free
Amount of items: 2
Items: 
Size: 795596 Color: 13
Size: 204386 Color: 9

Bin 1498: 20 of cap free
Amount of items: 3
Items: 
Size: 723897 Color: 3
Size: 138361 Color: 16
Size: 137723 Color: 3

Bin 1499: 20 of cap free
Amount of items: 2
Items: 
Size: 636199 Color: 5
Size: 363782 Color: 10

Bin 1500: 20 of cap free
Amount of items: 2
Items: 
Size: 518096 Color: 16
Size: 481885 Color: 3

Bin 1501: 20 of cap free
Amount of items: 2
Items: 
Size: 671541 Color: 12
Size: 328440 Color: 0

Bin 1502: 20 of cap free
Amount of items: 2
Items: 
Size: 728192 Color: 18
Size: 271789 Color: 5

Bin 1503: 20 of cap free
Amount of items: 2
Items: 
Size: 758620 Color: 3
Size: 241361 Color: 15

Bin 1504: 20 of cap free
Amount of items: 3
Items: 
Size: 651007 Color: 19
Size: 174614 Color: 15
Size: 174360 Color: 2

Bin 1505: 20 of cap free
Amount of items: 2
Items: 
Size: 726982 Color: 1
Size: 272999 Color: 9

Bin 1506: 20 of cap free
Amount of items: 2
Items: 
Size: 678389 Color: 0
Size: 321592 Color: 9

Bin 1507: 20 of cap free
Amount of items: 2
Items: 
Size: 504253 Color: 6
Size: 495728 Color: 13

Bin 1508: 20 of cap free
Amount of items: 2
Items: 
Size: 688198 Color: 8
Size: 311783 Color: 1

Bin 1509: 20 of cap free
Amount of items: 2
Items: 
Size: 636749 Color: 5
Size: 363232 Color: 4

Bin 1510: 20 of cap free
Amount of items: 2
Items: 
Size: 680208 Color: 8
Size: 319773 Color: 15

Bin 1511: 20 of cap free
Amount of items: 2
Items: 
Size: 520304 Color: 18
Size: 479677 Color: 19

Bin 1512: 20 of cap free
Amount of items: 2
Items: 
Size: 535021 Color: 18
Size: 464960 Color: 4

Bin 1513: 20 of cap free
Amount of items: 2
Items: 
Size: 539983 Color: 11
Size: 459998 Color: 6

Bin 1514: 20 of cap free
Amount of items: 2
Items: 
Size: 549490 Color: 16
Size: 450491 Color: 7

Bin 1515: 20 of cap free
Amount of items: 2
Items: 
Size: 570702 Color: 11
Size: 429279 Color: 8

Bin 1516: 20 of cap free
Amount of items: 2
Items: 
Size: 576385 Color: 14
Size: 423596 Color: 10

Bin 1517: 20 of cap free
Amount of items: 2
Items: 
Size: 605740 Color: 14
Size: 394241 Color: 2

Bin 1518: 20 of cap free
Amount of items: 2
Items: 
Size: 611838 Color: 2
Size: 388143 Color: 14

Bin 1519: 20 of cap free
Amount of items: 2
Items: 
Size: 625510 Color: 7
Size: 374471 Color: 13

Bin 1520: 20 of cap free
Amount of items: 2
Items: 
Size: 652200 Color: 0
Size: 347781 Color: 4

Bin 1521: 20 of cap free
Amount of items: 2
Items: 
Size: 672939 Color: 16
Size: 327042 Color: 10

Bin 1522: 20 of cap free
Amount of items: 2
Items: 
Size: 707976 Color: 14
Size: 292005 Color: 5

Bin 1523: 20 of cap free
Amount of items: 2
Items: 
Size: 778928 Color: 8
Size: 221053 Color: 19

Bin 1524: 21 of cap free
Amount of items: 2
Items: 
Size: 733313 Color: 6
Size: 266667 Color: 7

Bin 1525: 21 of cap free
Amount of items: 3
Items: 
Size: 624092 Color: 14
Size: 237709 Color: 14
Size: 138179 Color: 10

Bin 1526: 21 of cap free
Amount of items: 3
Items: 
Size: 607373 Color: 5
Size: 196432 Color: 9
Size: 196175 Color: 13

Bin 1527: 21 of cap free
Amount of items: 2
Items: 
Size: 734073 Color: 4
Size: 265907 Color: 7

Bin 1528: 21 of cap free
Amount of items: 3
Items: 
Size: 560363 Color: 12
Size: 265929 Color: 10
Size: 173688 Color: 15

Bin 1529: 21 of cap free
Amount of items: 2
Items: 
Size: 735375 Color: 14
Size: 264605 Color: 8

Bin 1530: 21 of cap free
Amount of items: 2
Items: 
Size: 719058 Color: 1
Size: 280922 Color: 7

Bin 1531: 21 of cap free
Amount of items: 3
Items: 
Size: 606804 Color: 16
Size: 239612 Color: 8
Size: 153564 Color: 19

Bin 1532: 21 of cap free
Amount of items: 3
Items: 
Size: 563115 Color: 11
Size: 229135 Color: 13
Size: 207730 Color: 18

Bin 1533: 21 of cap free
Amount of items: 2
Items: 
Size: 640535 Color: 3
Size: 359445 Color: 17

Bin 1534: 21 of cap free
Amount of items: 2
Items: 
Size: 795258 Color: 9
Size: 204722 Color: 11

Bin 1535: 21 of cap free
Amount of items: 2
Items: 
Size: 619923 Color: 17
Size: 380057 Color: 5

Bin 1536: 21 of cap free
Amount of items: 2
Items: 
Size: 500971 Color: 6
Size: 499009 Color: 0

Bin 1537: 21 of cap free
Amount of items: 2
Items: 
Size: 588150 Color: 15
Size: 411830 Color: 16

Bin 1538: 21 of cap free
Amount of items: 2
Items: 
Size: 547921 Color: 6
Size: 452059 Color: 11

Bin 1539: 21 of cap free
Amount of items: 2
Items: 
Size: 508620 Color: 8
Size: 491360 Color: 12

Bin 1540: 21 of cap free
Amount of items: 3
Items: 
Size: 524147 Color: 6
Size: 259228 Color: 16
Size: 216605 Color: 9

Bin 1541: 21 of cap free
Amount of items: 2
Items: 
Size: 543515 Color: 13
Size: 456465 Color: 3

Bin 1542: 21 of cap free
Amount of items: 2
Items: 
Size: 548041 Color: 11
Size: 451939 Color: 15

Bin 1543: 21 of cap free
Amount of items: 2
Items: 
Size: 548158 Color: 17
Size: 451822 Color: 3

Bin 1544: 21 of cap free
Amount of items: 2
Items: 
Size: 549961 Color: 12
Size: 450019 Color: 4

Bin 1545: 21 of cap free
Amount of items: 2
Items: 
Size: 554089 Color: 16
Size: 445891 Color: 0

Bin 1546: 21 of cap free
Amount of items: 2
Items: 
Size: 565522 Color: 3
Size: 434458 Color: 6

Bin 1547: 21 of cap free
Amount of items: 2
Items: 
Size: 572232 Color: 9
Size: 427748 Color: 14

Bin 1548: 21 of cap free
Amount of items: 2
Items: 
Size: 591455 Color: 19
Size: 408525 Color: 13

Bin 1549: 21 of cap free
Amount of items: 2
Items: 
Size: 598507 Color: 2
Size: 401473 Color: 1

Bin 1550: 21 of cap free
Amount of items: 2
Items: 
Size: 603565 Color: 14
Size: 396415 Color: 15

Bin 1551: 21 of cap free
Amount of items: 2
Items: 
Size: 624996 Color: 6
Size: 374984 Color: 16

Bin 1552: 21 of cap free
Amount of items: 2
Items: 
Size: 639908 Color: 3
Size: 360072 Color: 0

Bin 1553: 21 of cap free
Amount of items: 2
Items: 
Size: 668843 Color: 12
Size: 331137 Color: 8

Bin 1554: 21 of cap free
Amount of items: 2
Items: 
Size: 684497 Color: 9
Size: 315483 Color: 1

Bin 1555: 21 of cap free
Amount of items: 2
Items: 
Size: 696984 Color: 11
Size: 302996 Color: 1

Bin 1556: 21 of cap free
Amount of items: 2
Items: 
Size: 702923 Color: 8
Size: 297057 Color: 13

Bin 1557: 21 of cap free
Amount of items: 2
Items: 
Size: 716519 Color: 4
Size: 283461 Color: 2

Bin 1558: 21 of cap free
Amount of items: 2
Items: 
Size: 725394 Color: 2
Size: 274586 Color: 5

Bin 1559: 21 of cap free
Amount of items: 2
Items: 
Size: 732627 Color: 0
Size: 267353 Color: 1

Bin 1560: 21 of cap free
Amount of items: 2
Items: 
Size: 744115 Color: 9
Size: 255865 Color: 2

Bin 1561: 21 of cap free
Amount of items: 2
Items: 
Size: 750580 Color: 0
Size: 249400 Color: 15

Bin 1562: 21 of cap free
Amount of items: 2
Items: 
Size: 789050 Color: 14
Size: 210930 Color: 12

Bin 1563: 21 of cap free
Amount of items: 2
Items: 
Size: 791773 Color: 8
Size: 208207 Color: 5

Bin 1564: 22 of cap free
Amount of items: 3
Items: 
Size: 721078 Color: 2
Size: 139656 Color: 6
Size: 139245 Color: 8

Bin 1565: 22 of cap free
Amount of items: 2
Items: 
Size: 722431 Color: 18
Size: 277548 Color: 5

Bin 1566: 22 of cap free
Amount of items: 3
Items: 
Size: 383350 Color: 13
Size: 322883 Color: 0
Size: 293746 Color: 6

Bin 1567: 22 of cap free
Amount of items: 3
Items: 
Size: 614138 Color: 16
Size: 193355 Color: 15
Size: 192486 Color: 9

Bin 1568: 22 of cap free
Amount of items: 2
Items: 
Size: 527449 Color: 14
Size: 472530 Color: 13

Bin 1569: 22 of cap free
Amount of items: 3
Items: 
Size: 638298 Color: 16
Size: 181825 Color: 12
Size: 179856 Color: 12

Bin 1570: 22 of cap free
Amount of items: 2
Items: 
Size: 660513 Color: 16
Size: 339466 Color: 5

Bin 1571: 22 of cap free
Amount of items: 2
Items: 
Size: 620887 Color: 16
Size: 379092 Color: 18

Bin 1572: 22 of cap free
Amount of items: 2
Items: 
Size: 792506 Color: 5
Size: 207473 Color: 8

Bin 1573: 22 of cap free
Amount of items: 2
Items: 
Size: 672099 Color: 18
Size: 327880 Color: 4

Bin 1574: 22 of cap free
Amount of items: 2
Items: 
Size: 501845 Color: 6
Size: 498134 Color: 9

Bin 1575: 22 of cap free
Amount of items: 2
Items: 
Size: 507244 Color: 19
Size: 492735 Color: 9

Bin 1576: 22 of cap free
Amount of items: 2
Items: 
Size: 519065 Color: 3
Size: 480914 Color: 9

Bin 1577: 22 of cap free
Amount of items: 2
Items: 
Size: 528774 Color: 8
Size: 471205 Color: 12

Bin 1578: 22 of cap free
Amount of items: 2
Items: 
Size: 529944 Color: 15
Size: 470035 Color: 6

Bin 1579: 22 of cap free
Amount of items: 2
Items: 
Size: 531113 Color: 0
Size: 468866 Color: 19

Bin 1580: 22 of cap free
Amount of items: 2
Items: 
Size: 531693 Color: 18
Size: 468286 Color: 15

Bin 1581: 22 of cap free
Amount of items: 2
Items: 
Size: 538934 Color: 12
Size: 461045 Color: 6

Bin 1582: 22 of cap free
Amount of items: 2
Items: 
Size: 552804 Color: 8
Size: 447175 Color: 14

Bin 1583: 22 of cap free
Amount of items: 2
Items: 
Size: 561527 Color: 9
Size: 438452 Color: 14

Bin 1584: 22 of cap free
Amount of items: 2
Items: 
Size: 562232 Color: 15
Size: 437747 Color: 1

Bin 1585: 22 of cap free
Amount of items: 2
Items: 
Size: 576861 Color: 8
Size: 423118 Color: 7

Bin 1586: 22 of cap free
Amount of items: 2
Items: 
Size: 580509 Color: 16
Size: 419470 Color: 13

Bin 1587: 22 of cap free
Amount of items: 2
Items: 
Size: 590122 Color: 7
Size: 409857 Color: 4

Bin 1588: 22 of cap free
Amount of items: 2
Items: 
Size: 601873 Color: 13
Size: 398106 Color: 17

Bin 1589: 22 of cap free
Amount of items: 2
Items: 
Size: 602978 Color: 16
Size: 397001 Color: 10

Bin 1590: 22 of cap free
Amount of items: 2
Items: 
Size: 620642 Color: 14
Size: 379337 Color: 15

Bin 1591: 22 of cap free
Amount of items: 2
Items: 
Size: 626833 Color: 1
Size: 373146 Color: 15

Bin 1592: 22 of cap free
Amount of items: 2
Items: 
Size: 642549 Color: 3
Size: 357430 Color: 9

Bin 1593: 22 of cap free
Amount of items: 2
Items: 
Size: 664808 Color: 6
Size: 335171 Color: 19

Bin 1594: 22 of cap free
Amount of items: 2
Items: 
Size: 690984 Color: 17
Size: 308995 Color: 11

Bin 1595: 22 of cap free
Amount of items: 2
Items: 
Size: 734909 Color: 4
Size: 265070 Color: 8

Bin 1596: 22 of cap free
Amount of items: 2
Items: 
Size: 751233 Color: 11
Size: 248746 Color: 3

Bin 1597: 22 of cap free
Amount of items: 2
Items: 
Size: 763855 Color: 11
Size: 236124 Color: 1

Bin 1598: 22 of cap free
Amount of items: 2
Items: 
Size: 778517 Color: 17
Size: 221462 Color: 5

Bin 1599: 22 of cap free
Amount of items: 2
Items: 
Size: 797332 Color: 12
Size: 202647 Color: 7

Bin 1600: 23 of cap free
Amount of items: 3
Items: 
Size: 785087 Color: 7
Size: 112733 Color: 18
Size: 102158 Color: 8

Bin 1601: 23 of cap free
Amount of items: 2
Items: 
Size: 647213 Color: 0
Size: 352765 Color: 6

Bin 1602: 23 of cap free
Amount of items: 2
Items: 
Size: 718987 Color: 2
Size: 280991 Color: 8

Bin 1603: 23 of cap free
Amount of items: 2
Items: 
Size: 694889 Color: 13
Size: 305089 Color: 0

Bin 1604: 23 of cap free
Amount of items: 2
Items: 
Size: 740807 Color: 10
Size: 259171 Color: 9

Bin 1605: 23 of cap free
Amount of items: 3
Items: 
Size: 617070 Color: 9
Size: 191641 Color: 7
Size: 191267 Color: 13

Bin 1606: 23 of cap free
Amount of items: 3
Items: 
Size: 417376 Color: 2
Size: 301315 Color: 13
Size: 281287 Color: 8

Bin 1607: 23 of cap free
Amount of items: 3
Items: 
Size: 679969 Color: 11
Size: 160063 Color: 10
Size: 159946 Color: 9

Bin 1608: 23 of cap free
Amount of items: 2
Items: 
Size: 794157 Color: 16
Size: 205821 Color: 11

Bin 1609: 23 of cap free
Amount of items: 2
Items: 
Size: 665215 Color: 6
Size: 334763 Color: 10

Bin 1610: 23 of cap free
Amount of items: 2
Items: 
Size: 609139 Color: 15
Size: 390839 Color: 19

Bin 1611: 23 of cap free
Amount of items: 2
Items: 
Size: 508687 Color: 16
Size: 491291 Color: 13

Bin 1612: 23 of cap free
Amount of items: 2
Items: 
Size: 522124 Color: 17
Size: 477854 Color: 16

Bin 1613: 23 of cap free
Amount of items: 2
Items: 
Size: 523937 Color: 4
Size: 476041 Color: 11

Bin 1614: 23 of cap free
Amount of items: 2
Items: 
Size: 551316 Color: 2
Size: 448662 Color: 18

Bin 1615: 23 of cap free
Amount of items: 2
Items: 
Size: 558234 Color: 12
Size: 441744 Color: 13

Bin 1616: 23 of cap free
Amount of items: 2
Items: 
Size: 571571 Color: 14
Size: 428407 Color: 12

Bin 1617: 23 of cap free
Amount of items: 2
Items: 
Size: 581999 Color: 8
Size: 417979 Color: 7

Bin 1618: 23 of cap free
Amount of items: 2
Items: 
Size: 592498 Color: 5
Size: 407480 Color: 8

Bin 1619: 23 of cap free
Amount of items: 2
Items: 
Size: 593109 Color: 8
Size: 406869 Color: 2

Bin 1620: 23 of cap free
Amount of items: 2
Items: 
Size: 603954 Color: 5
Size: 396024 Color: 15

Bin 1621: 23 of cap free
Amount of items: 2
Items: 
Size: 624568 Color: 9
Size: 375410 Color: 0

Bin 1622: 23 of cap free
Amount of items: 2
Items: 
Size: 629262 Color: 18
Size: 370716 Color: 9

Bin 1623: 23 of cap free
Amount of items: 2
Items: 
Size: 640154 Color: 1
Size: 359824 Color: 12

Bin 1624: 23 of cap free
Amount of items: 2
Items: 
Size: 640856 Color: 7
Size: 359122 Color: 10

Bin 1625: 23 of cap free
Amount of items: 2
Items: 
Size: 657192 Color: 19
Size: 342786 Color: 18

Bin 1626: 23 of cap free
Amount of items: 2
Items: 
Size: 665415 Color: 6
Size: 334563 Color: 19

Bin 1627: 23 of cap free
Amount of items: 2
Items: 
Size: 678701 Color: 7
Size: 321277 Color: 5

Bin 1628: 23 of cap free
Amount of items: 2
Items: 
Size: 702046 Color: 18
Size: 297932 Color: 17

Bin 1629: 23 of cap free
Amount of items: 2
Items: 
Size: 708751 Color: 15
Size: 291227 Color: 2

Bin 1630: 23 of cap free
Amount of items: 2
Items: 
Size: 734609 Color: 18
Size: 265369 Color: 10

Bin 1631: 23 of cap free
Amount of items: 2
Items: 
Size: 783009 Color: 12
Size: 216969 Color: 17

Bin 1632: 24 of cap free
Amount of items: 2
Items: 
Size: 640212 Color: 3
Size: 359765 Color: 5

Bin 1633: 24 of cap free
Amount of items: 2
Items: 
Size: 638332 Color: 4
Size: 361645 Color: 18

Bin 1634: 24 of cap free
Amount of items: 3
Items: 
Size: 652307 Color: 5
Size: 173900 Color: 8
Size: 173770 Color: 6

Bin 1635: 24 of cap free
Amount of items: 3
Items: 
Size: 700440 Color: 6
Size: 149840 Color: 19
Size: 149697 Color: 3

Bin 1636: 24 of cap free
Amount of items: 3
Items: 
Size: 617470 Color: 16
Size: 191270 Color: 13
Size: 191237 Color: 12

Bin 1637: 24 of cap free
Amount of items: 2
Items: 
Size: 692223 Color: 9
Size: 307754 Color: 19

Bin 1638: 24 of cap free
Amount of items: 2
Items: 
Size: 698938 Color: 16
Size: 301039 Color: 19

Bin 1639: 24 of cap free
Amount of items: 2
Items: 
Size: 537020 Color: 18
Size: 462957 Color: 5

Bin 1640: 24 of cap free
Amount of items: 2
Items: 
Size: 709158 Color: 16
Size: 290819 Color: 17

Bin 1641: 24 of cap free
Amount of items: 2
Items: 
Size: 799597 Color: 4
Size: 200380 Color: 12

Bin 1642: 24 of cap free
Amount of items: 2
Items: 
Size: 794620 Color: 16
Size: 205357 Color: 7

Bin 1643: 24 of cap free
Amount of items: 2
Items: 
Size: 532776 Color: 10
Size: 467201 Color: 12

Bin 1644: 24 of cap free
Amount of items: 2
Items: 
Size: 543196 Color: 19
Size: 456781 Color: 6

Bin 1645: 24 of cap free
Amount of items: 2
Items: 
Size: 545217 Color: 11
Size: 454760 Color: 14

Bin 1646: 24 of cap free
Amount of items: 2
Items: 
Size: 567669 Color: 6
Size: 432308 Color: 14

Bin 1647: 24 of cap free
Amount of items: 2
Items: 
Size: 572360 Color: 2
Size: 427617 Color: 16

Bin 1648: 24 of cap free
Amount of items: 2
Items: 
Size: 575709 Color: 1
Size: 424268 Color: 8

Bin 1649: 24 of cap free
Amount of items: 2
Items: 
Size: 615509 Color: 19
Size: 384468 Color: 15

Bin 1650: 24 of cap free
Amount of items: 2
Items: 
Size: 618491 Color: 9
Size: 381486 Color: 1

Bin 1651: 24 of cap free
Amount of items: 2
Items: 
Size: 620102 Color: 4
Size: 379875 Color: 11

Bin 1652: 24 of cap free
Amount of items: 2
Items: 
Size: 626378 Color: 7
Size: 373599 Color: 9

Bin 1653: 24 of cap free
Amount of items: 2
Items: 
Size: 639679 Color: 10
Size: 360298 Color: 1

Bin 1654: 24 of cap free
Amount of items: 2
Items: 
Size: 640654 Color: 4
Size: 359323 Color: 9

Bin 1655: 24 of cap free
Amount of items: 2
Items: 
Size: 640718 Color: 19
Size: 359259 Color: 15

Bin 1656: 24 of cap free
Amount of items: 2
Items: 
Size: 682387 Color: 13
Size: 317590 Color: 2

Bin 1657: 24 of cap free
Amount of items: 2
Items: 
Size: 682611 Color: 14
Size: 317366 Color: 7

Bin 1658: 24 of cap free
Amount of items: 2
Items: 
Size: 728700 Color: 0
Size: 271277 Color: 9

Bin 1659: 24 of cap free
Amount of items: 2
Items: 
Size: 776737 Color: 15
Size: 223240 Color: 11

Bin 1660: 25 of cap free
Amount of items: 3
Items: 
Size: 760968 Color: 12
Size: 127657 Color: 1
Size: 111351 Color: 3

Bin 1661: 25 of cap free
Amount of items: 2
Items: 
Size: 710185 Color: 10
Size: 289791 Color: 11

Bin 1662: 25 of cap free
Amount of items: 3
Items: 
Size: 760766 Color: 1
Size: 119907 Color: 16
Size: 119303 Color: 1

Bin 1663: 25 of cap free
Amount of items: 2
Items: 
Size: 697148 Color: 7
Size: 302828 Color: 16

Bin 1664: 25 of cap free
Amount of items: 2
Items: 
Size: 785414 Color: 3
Size: 214562 Color: 7

Bin 1665: 25 of cap free
Amount of items: 2
Items: 
Size: 641281 Color: 5
Size: 358695 Color: 15

Bin 1666: 25 of cap free
Amount of items: 2
Items: 
Size: 605640 Color: 17
Size: 394336 Color: 6

Bin 1667: 25 of cap free
Amount of items: 2
Items: 
Size: 503687 Color: 0
Size: 496289 Color: 16

Bin 1668: 25 of cap free
Amount of items: 2
Items: 
Size: 515001 Color: 16
Size: 484975 Color: 13

Bin 1669: 25 of cap free
Amount of items: 2
Items: 
Size: 522209 Color: 19
Size: 477767 Color: 0

Bin 1670: 25 of cap free
Amount of items: 2
Items: 
Size: 524289 Color: 8
Size: 475687 Color: 7

Bin 1671: 25 of cap free
Amount of items: 2
Items: 
Size: 525458 Color: 13
Size: 474518 Color: 2

Bin 1672: 25 of cap free
Amount of items: 2
Items: 
Size: 534065 Color: 7
Size: 465911 Color: 15

Bin 1673: 25 of cap free
Amount of items: 2
Items: 
Size: 573279 Color: 19
Size: 426697 Color: 17

Bin 1674: 25 of cap free
Amount of items: 2
Items: 
Size: 578719 Color: 6
Size: 421257 Color: 9

Bin 1675: 25 of cap free
Amount of items: 2
Items: 
Size: 586640 Color: 17
Size: 413336 Color: 2

Bin 1676: 25 of cap free
Amount of items: 2
Items: 
Size: 597579 Color: 0
Size: 402397 Color: 7

Bin 1677: 25 of cap free
Amount of items: 2
Items: 
Size: 601472 Color: 3
Size: 398504 Color: 13

Bin 1678: 25 of cap free
Amount of items: 2
Items: 
Size: 608584 Color: 7
Size: 391392 Color: 0

Bin 1679: 25 of cap free
Amount of items: 2
Items: 
Size: 633868 Color: 19
Size: 366108 Color: 16

Bin 1680: 25 of cap free
Amount of items: 2
Items: 
Size: 635185 Color: 18
Size: 364791 Color: 4

Bin 1681: 25 of cap free
Amount of items: 2
Items: 
Size: 643647 Color: 10
Size: 356329 Color: 7

Bin 1682: 25 of cap free
Amount of items: 2
Items: 
Size: 643727 Color: 19
Size: 356249 Color: 0

Bin 1683: 25 of cap free
Amount of items: 2
Items: 
Size: 685766 Color: 14
Size: 314210 Color: 7

Bin 1684: 25 of cap free
Amount of items: 2
Items: 
Size: 698243 Color: 17
Size: 301733 Color: 14

Bin 1685: 25 of cap free
Amount of items: 2
Items: 
Size: 718105 Color: 9
Size: 281871 Color: 0

Bin 1686: 25 of cap free
Amount of items: 2
Items: 
Size: 724120 Color: 8
Size: 275856 Color: 18

Bin 1687: 25 of cap free
Amount of items: 2
Items: 
Size: 738819 Color: 11
Size: 261157 Color: 2

Bin 1688: 25 of cap free
Amount of items: 2
Items: 
Size: 745961 Color: 13
Size: 254015 Color: 4

Bin 1689: 25 of cap free
Amount of items: 2
Items: 
Size: 758534 Color: 2
Size: 241442 Color: 10

Bin 1690: 25 of cap free
Amount of items: 2
Items: 
Size: 774060 Color: 15
Size: 225916 Color: 0

Bin 1691: 25 of cap free
Amount of items: 2
Items: 
Size: 779171 Color: 1
Size: 220805 Color: 2

Bin 1692: 26 of cap free
Amount of items: 3
Items: 
Size: 596073 Color: 8
Size: 231511 Color: 6
Size: 172391 Color: 2

Bin 1693: 26 of cap free
Amount of items: 3
Items: 
Size: 722434 Color: 11
Size: 139526 Color: 0
Size: 138015 Color: 0

Bin 1694: 26 of cap free
Amount of items: 3
Items: 
Size: 763525 Color: 0
Size: 118269 Color: 7
Size: 118181 Color: 8

Bin 1695: 26 of cap free
Amount of items: 2
Items: 
Size: 701946 Color: 9
Size: 298029 Color: 8

Bin 1696: 26 of cap free
Amount of items: 3
Items: 
Size: 636881 Color: 14
Size: 181591 Color: 17
Size: 181503 Color: 13

Bin 1697: 26 of cap free
Amount of items: 2
Items: 
Size: 699202 Color: 15
Size: 300773 Color: 8

Bin 1698: 26 of cap free
Amount of items: 3
Items: 
Size: 642690 Color: 11
Size: 246699 Color: 11
Size: 110586 Color: 1

Bin 1699: 26 of cap free
Amount of items: 2
Items: 
Size: 786551 Color: 6
Size: 213424 Color: 9

Bin 1700: 26 of cap free
Amount of items: 3
Items: 
Size: 493385 Color: 18
Size: 272153 Color: 8
Size: 234437 Color: 12

Bin 1701: 26 of cap free
Amount of items: 3
Items: 
Size: 489783 Color: 0
Size: 282095 Color: 3
Size: 228097 Color: 13

Bin 1702: 26 of cap free
Amount of items: 2
Items: 
Size: 670424 Color: 6
Size: 329551 Color: 3

Bin 1703: 26 of cap free
Amount of items: 2
Items: 
Size: 700923 Color: 0
Size: 299052 Color: 13

Bin 1704: 26 of cap free
Amount of items: 2
Items: 
Size: 767619 Color: 3
Size: 232356 Color: 8

Bin 1705: 26 of cap free
Amount of items: 2
Items: 
Size: 780038 Color: 18
Size: 219937 Color: 12

Bin 1706: 26 of cap free
Amount of items: 2
Items: 
Size: 550206 Color: 1
Size: 449769 Color: 11

Bin 1707: 26 of cap free
Amount of items: 2
Items: 
Size: 541275 Color: 10
Size: 458700 Color: 12

Bin 1708: 26 of cap free
Amount of items: 2
Items: 
Size: 544093 Color: 17
Size: 455882 Color: 7

Bin 1709: 26 of cap free
Amount of items: 2
Items: 
Size: 548560 Color: 2
Size: 451415 Color: 13

Bin 1710: 26 of cap free
Amount of items: 2
Items: 
Size: 554164 Color: 1
Size: 445811 Color: 12

Bin 1711: 26 of cap free
Amount of items: 2
Items: 
Size: 561463 Color: 11
Size: 438512 Color: 18

Bin 1712: 26 of cap free
Amount of items: 2
Items: 
Size: 572040 Color: 2
Size: 427935 Color: 0

Bin 1713: 26 of cap free
Amount of items: 2
Items: 
Size: 579027 Color: 11
Size: 420948 Color: 3

Bin 1714: 26 of cap free
Amount of items: 2
Items: 
Size: 584414 Color: 17
Size: 415561 Color: 6

Bin 1715: 26 of cap free
Amount of items: 2
Items: 
Size: 606395 Color: 7
Size: 393580 Color: 3

Bin 1716: 26 of cap free
Amount of items: 2
Items: 
Size: 607217 Color: 9
Size: 392758 Color: 19

Bin 1717: 26 of cap free
Amount of items: 2
Items: 
Size: 611699 Color: 14
Size: 388276 Color: 18

Bin 1718: 26 of cap free
Amount of items: 2
Items: 
Size: 676388 Color: 15
Size: 323587 Color: 11

Bin 1719: 26 of cap free
Amount of items: 2
Items: 
Size: 686912 Color: 1
Size: 313063 Color: 17

Bin 1720: 26 of cap free
Amount of items: 2
Items: 
Size: 692883 Color: 13
Size: 307092 Color: 1

Bin 1721: 26 of cap free
Amount of items: 2
Items: 
Size: 726130 Color: 8
Size: 273845 Color: 14

Bin 1722: 26 of cap free
Amount of items: 2
Items: 
Size: 733984 Color: 9
Size: 265991 Color: 17

Bin 1723: 26 of cap free
Amount of items: 2
Items: 
Size: 740738 Color: 5
Size: 259237 Color: 2

Bin 1724: 27 of cap free
Amount of items: 3
Items: 
Size: 625811 Color: 8
Size: 188658 Color: 2
Size: 185505 Color: 5

Bin 1725: 27 of cap free
Amount of items: 3
Items: 
Size: 614627 Color: 17
Size: 192829 Color: 12
Size: 192518 Color: 10

Bin 1726: 27 of cap free
Amount of items: 2
Items: 
Size: 756465 Color: 19
Size: 243509 Color: 18

Bin 1727: 27 of cap free
Amount of items: 2
Items: 
Size: 551236 Color: 12
Size: 448738 Color: 8

Bin 1728: 27 of cap free
Amount of items: 3
Items: 
Size: 693272 Color: 19
Size: 153492 Color: 16
Size: 153210 Color: 13

Bin 1729: 27 of cap free
Amount of items: 2
Items: 
Size: 611417 Color: 7
Size: 388557 Color: 17

Bin 1730: 27 of cap free
Amount of items: 2
Items: 
Size: 798332 Color: 16
Size: 201642 Color: 0

Bin 1731: 27 of cap free
Amount of items: 2
Items: 
Size: 741235 Color: 13
Size: 258739 Color: 5

Bin 1732: 27 of cap free
Amount of items: 3
Items: 
Size: 673534 Color: 19
Size: 170818 Color: 15
Size: 155622 Color: 18

Bin 1733: 27 of cap free
Amount of items: 2
Items: 
Size: 629801 Color: 2
Size: 370173 Color: 0

Bin 1734: 27 of cap free
Amount of items: 2
Items: 
Size: 719238 Color: 15
Size: 280736 Color: 14

Bin 1735: 27 of cap free
Amount of items: 2
Items: 
Size: 527246 Color: 12
Size: 472728 Color: 5

Bin 1736: 27 of cap free
Amount of items: 2
Items: 
Size: 709949 Color: 13
Size: 290025 Color: 2

Bin 1737: 27 of cap free
Amount of items: 2
Items: 
Size: 662618 Color: 19
Size: 337356 Color: 14

Bin 1738: 27 of cap free
Amount of items: 2
Items: 
Size: 630142 Color: 13
Size: 369832 Color: 6

Bin 1739: 27 of cap free
Amount of items: 2
Items: 
Size: 501505 Color: 9
Size: 498469 Color: 19

Bin 1740: 27 of cap free
Amount of items: 2
Items: 
Size: 517188 Color: 12
Size: 482786 Color: 2

Bin 1741: 27 of cap free
Amount of items: 2
Items: 
Size: 527718 Color: 5
Size: 472256 Color: 7

Bin 1742: 27 of cap free
Amount of items: 2
Items: 
Size: 535508 Color: 18
Size: 464466 Color: 9

Bin 1743: 27 of cap free
Amount of items: 2
Items: 
Size: 542857 Color: 17
Size: 457117 Color: 16

Bin 1744: 27 of cap free
Amount of items: 2
Items: 
Size: 629322 Color: 2
Size: 370652 Color: 12

Bin 1745: 27 of cap free
Amount of items: 2
Items: 
Size: 634502 Color: 0
Size: 365472 Color: 5

Bin 1746: 27 of cap free
Amount of items: 2
Items: 
Size: 638965 Color: 12
Size: 361009 Color: 2

Bin 1747: 27 of cap free
Amount of items: 2
Items: 
Size: 642021 Color: 16
Size: 357953 Color: 5

Bin 1748: 27 of cap free
Amount of items: 2
Items: 
Size: 687158 Color: 7
Size: 312816 Color: 18

Bin 1749: 27 of cap free
Amount of items: 2
Items: 
Size: 696525 Color: 8
Size: 303449 Color: 16

Bin 1750: 27 of cap free
Amount of items: 2
Items: 
Size: 739515 Color: 19
Size: 260459 Color: 16

Bin 1751: 27 of cap free
Amount of items: 2
Items: 
Size: 747319 Color: 16
Size: 252655 Color: 8

Bin 1752: 27 of cap free
Amount of items: 2
Items: 
Size: 776521 Color: 7
Size: 223453 Color: 11

Bin 1753: 27 of cap free
Amount of items: 2
Items: 
Size: 789411 Color: 8
Size: 210563 Color: 11

Bin 1754: 27 of cap free
Amount of items: 2
Items: 
Size: 797659 Color: 7
Size: 202315 Color: 2

Bin 1755: 27 of cap free
Amount of items: 2
Items: 
Size: 797737 Color: 12
Size: 202237 Color: 2

Bin 1756: 28 of cap free
Amount of items: 3
Items: 
Size: 729891 Color: 7
Size: 139906 Color: 9
Size: 130176 Color: 16

Bin 1757: 28 of cap free
Amount of items: 3
Items: 
Size: 725455 Color: 1
Size: 159003 Color: 19
Size: 115515 Color: 7

Bin 1758: 28 of cap free
Amount of items: 2
Items: 
Size: 737793 Color: 6
Size: 262180 Color: 16

Bin 1759: 28 of cap free
Amount of items: 3
Items: 
Size: 386728 Color: 3
Size: 363933 Color: 12
Size: 249312 Color: 13

Bin 1760: 28 of cap free
Amount of items: 2
Items: 
Size: 768601 Color: 16
Size: 231372 Color: 17

Bin 1761: 28 of cap free
Amount of items: 3
Items: 
Size: 634155 Color: 8
Size: 182949 Color: 5
Size: 182869 Color: 19

Bin 1762: 28 of cap free
Amount of items: 3
Items: 
Size: 635010 Color: 7
Size: 182536 Color: 17
Size: 182427 Color: 1

Bin 1763: 28 of cap free
Amount of items: 3
Items: 
Size: 763405 Color: 4
Size: 126462 Color: 17
Size: 110106 Color: 11

Bin 1764: 28 of cap free
Amount of items: 2
Items: 
Size: 779976 Color: 16
Size: 219997 Color: 17

Bin 1765: 28 of cap free
Amount of items: 3
Items: 
Size: 503726 Color: 6
Size: 271103 Color: 0
Size: 225144 Color: 18

Bin 1766: 28 of cap free
Amount of items: 2
Items: 
Size: 542793 Color: 6
Size: 457180 Color: 12

Bin 1767: 28 of cap free
Amount of items: 2
Items: 
Size: 560455 Color: 16
Size: 439518 Color: 12

Bin 1768: 28 of cap free
Amount of items: 2
Items: 
Size: 566122 Color: 8
Size: 433851 Color: 0

Bin 1769: 28 of cap free
Amount of items: 2
Items: 
Size: 567052 Color: 18
Size: 432921 Color: 1

Bin 1770: 28 of cap free
Amount of items: 2
Items: 
Size: 568681 Color: 5
Size: 431292 Color: 3

Bin 1771: 28 of cap free
Amount of items: 2
Items: 
Size: 568832 Color: 14
Size: 431141 Color: 4

Bin 1772: 28 of cap free
Amount of items: 2
Items: 
Size: 577493 Color: 19
Size: 422480 Color: 18

Bin 1773: 28 of cap free
Amount of items: 2
Items: 
Size: 631491 Color: 17
Size: 368482 Color: 10

Bin 1774: 28 of cap free
Amount of items: 3
Items: 
Size: 631787 Color: 16
Size: 184290 Color: 5
Size: 183896 Color: 11

Bin 1775: 28 of cap free
Amount of items: 2
Items: 
Size: 683767 Color: 4
Size: 316206 Color: 11

Bin 1776: 28 of cap free
Amount of items: 2
Items: 
Size: 688086 Color: 3
Size: 311887 Color: 17

Bin 1777: 28 of cap free
Amount of items: 2
Items: 
Size: 698105 Color: 6
Size: 301868 Color: 10

Bin 1778: 29 of cap free
Amount of items: 2
Items: 
Size: 667626 Color: 11
Size: 332346 Color: 15

Bin 1779: 29 of cap free
Amount of items: 3
Items: 
Size: 601563 Color: 5
Size: 199560 Color: 0
Size: 198849 Color: 9

Bin 1780: 29 of cap free
Amount of items: 3
Items: 
Size: 727739 Color: 4
Size: 137544 Color: 7
Size: 134689 Color: 1

Bin 1781: 29 of cap free
Amount of items: 2
Items: 
Size: 769587 Color: 4
Size: 230385 Color: 19

Bin 1782: 29 of cap free
Amount of items: 3
Items: 
Size: 722728 Color: 19
Size: 145702 Color: 12
Size: 131542 Color: 12

Bin 1783: 29 of cap free
Amount of items: 2
Items: 
Size: 726068 Color: 7
Size: 273904 Color: 1

Bin 1784: 29 of cap free
Amount of items: 2
Items: 
Size: 709948 Color: 9
Size: 290024 Color: 7

Bin 1785: 29 of cap free
Amount of items: 2
Items: 
Size: 633928 Color: 12
Size: 366044 Color: 19

Bin 1786: 29 of cap free
Amount of items: 2
Items: 
Size: 513843 Color: 12
Size: 486129 Color: 16

Bin 1787: 29 of cap free
Amount of items: 2
Items: 
Size: 528370 Color: 16
Size: 471602 Color: 8

Bin 1788: 29 of cap free
Amount of items: 2
Items: 
Size: 547674 Color: 2
Size: 452298 Color: 8

Bin 1789: 29 of cap free
Amount of items: 2
Items: 
Size: 547707 Color: 13
Size: 452265 Color: 6

Bin 1790: 29 of cap free
Amount of items: 2
Items: 
Size: 562778 Color: 5
Size: 437194 Color: 0

Bin 1791: 29 of cap free
Amount of items: 2
Items: 
Size: 571307 Color: 4
Size: 428665 Color: 6

Bin 1792: 29 of cap free
Amount of items: 2
Items: 
Size: 575139 Color: 17
Size: 424833 Color: 14

Bin 1793: 29 of cap free
Amount of items: 2
Items: 
Size: 585857 Color: 9
Size: 414115 Color: 5

Bin 1794: 29 of cap free
Amount of items: 2
Items: 
Size: 588836 Color: 12
Size: 411136 Color: 18

Bin 1795: 29 of cap free
Amount of items: 2
Items: 
Size: 596512 Color: 7
Size: 403460 Color: 4

Bin 1796: 29 of cap free
Amount of items: 2
Items: 
Size: 602390 Color: 15
Size: 397582 Color: 8

Bin 1797: 29 of cap free
Amount of items: 2
Items: 
Size: 603832 Color: 8
Size: 396140 Color: 3

Bin 1798: 29 of cap free
Amount of items: 3
Items: 
Size: 610384 Color: 0
Size: 195011 Color: 7
Size: 194577 Color: 16

Bin 1799: 29 of cap free
Amount of items: 2
Items: 
Size: 618607 Color: 13
Size: 381365 Color: 0

Bin 1800: 29 of cap free
Amount of items: 2
Items: 
Size: 637686 Color: 2
Size: 362286 Color: 10

Bin 1801: 29 of cap free
Amount of items: 2
Items: 
Size: 644281 Color: 4
Size: 355691 Color: 11

Bin 1802: 29 of cap free
Amount of items: 2
Items: 
Size: 678307 Color: 9
Size: 321665 Color: 7

Bin 1803: 29 of cap free
Amount of items: 2
Items: 
Size: 719467 Color: 7
Size: 280505 Color: 15

Bin 1804: 29 of cap free
Amount of items: 2
Items: 
Size: 739276 Color: 15
Size: 260696 Color: 17

Bin 1805: 29 of cap free
Amount of items: 2
Items: 
Size: 756657 Color: 14
Size: 243315 Color: 0

Bin 1806: 29 of cap free
Amount of items: 2
Items: 
Size: 783450 Color: 9
Size: 216522 Color: 16

Bin 1807: 29 of cap free
Amount of items: 2
Items: 
Size: 797553 Color: 5
Size: 202419 Color: 1

Bin 1808: 30 of cap free
Amount of items: 3
Items: 
Size: 722551 Color: 13
Size: 138808 Color: 1
Size: 138612 Color: 19

Bin 1809: 30 of cap free
Amount of items: 3
Items: 
Size: 371701 Color: 2
Size: 334727 Color: 16
Size: 293543 Color: 11

Bin 1810: 30 of cap free
Amount of items: 3
Items: 
Size: 381758 Color: 6
Size: 312665 Color: 12
Size: 305548 Color: 16

Bin 1811: 30 of cap free
Amount of items: 2
Items: 
Size: 762194 Color: 13
Size: 237777 Color: 4

Bin 1812: 30 of cap free
Amount of items: 2
Items: 
Size: 660679 Color: 6
Size: 339292 Color: 8

Bin 1813: 30 of cap free
Amount of items: 2
Items: 
Size: 645230 Color: 11
Size: 354741 Color: 2

Bin 1814: 30 of cap free
Amount of items: 2
Items: 
Size: 758387 Color: 13
Size: 241584 Color: 15

Bin 1815: 30 of cap free
Amount of items: 2
Items: 
Size: 528075 Color: 14
Size: 471896 Color: 6

Bin 1816: 30 of cap free
Amount of items: 2
Items: 
Size: 532865 Color: 17
Size: 467106 Color: 3

Bin 1817: 30 of cap free
Amount of items: 2
Items: 
Size: 543138 Color: 2
Size: 456833 Color: 1

Bin 1818: 30 of cap free
Amount of items: 2
Items: 
Size: 550611 Color: 5
Size: 449360 Color: 16

Bin 1819: 30 of cap free
Amount of items: 2
Items: 
Size: 560253 Color: 13
Size: 439718 Color: 14

Bin 1820: 30 of cap free
Amount of items: 2
Items: 
Size: 563415 Color: 10
Size: 436556 Color: 4

Bin 1821: 30 of cap free
Amount of items: 2
Items: 
Size: 565052 Color: 17
Size: 434919 Color: 4

Bin 1822: 30 of cap free
Amount of items: 2
Items: 
Size: 573613 Color: 15
Size: 426358 Color: 17

Bin 1823: 30 of cap free
Amount of items: 2
Items: 
Size: 579069 Color: 0
Size: 420902 Color: 17

Bin 1824: 30 of cap free
Amount of items: 2
Items: 
Size: 581798 Color: 19
Size: 418173 Color: 17

Bin 1825: 30 of cap free
Amount of items: 2
Items: 
Size: 583133 Color: 18
Size: 416838 Color: 16

Bin 1826: 30 of cap free
Amount of items: 2
Items: 
Size: 588750 Color: 3
Size: 411221 Color: 2

Bin 1827: 30 of cap free
Amount of items: 2
Items: 
Size: 603743 Color: 0
Size: 396228 Color: 16

Bin 1828: 30 of cap free
Amount of items: 2
Items: 
Size: 609691 Color: 9
Size: 390280 Color: 5

Bin 1829: 30 of cap free
Amount of items: 2
Items: 
Size: 609945 Color: 1
Size: 390026 Color: 8

Bin 1830: 30 of cap free
Amount of items: 2
Items: 
Size: 656311 Color: 7
Size: 343660 Color: 19

Bin 1831: 30 of cap free
Amount of items: 2
Items: 
Size: 665002 Color: 16
Size: 334969 Color: 7

Bin 1832: 30 of cap free
Amount of items: 2
Items: 
Size: 666187 Color: 14
Size: 333784 Color: 10

Bin 1833: 30 of cap free
Amount of items: 2
Items: 
Size: 668824 Color: 6
Size: 331147 Color: 10

Bin 1834: 30 of cap free
Amount of items: 2
Items: 
Size: 681676 Color: 7
Size: 318295 Color: 11

Bin 1835: 30 of cap free
Amount of items: 2
Items: 
Size: 743950 Color: 18
Size: 256021 Color: 5

Bin 1836: 30 of cap free
Amount of items: 2
Items: 
Size: 784982 Color: 10
Size: 214989 Color: 1

Bin 1837: 31 of cap free
Amount of items: 3
Items: 
Size: 379924 Color: 5
Size: 321476 Color: 8
Size: 298570 Color: 18

Bin 1838: 31 of cap free
Amount of items: 3
Items: 
Size: 647602 Color: 3
Size: 176689 Color: 9
Size: 175679 Color: 0

Bin 1839: 31 of cap free
Amount of items: 3
Items: 
Size: 627458 Color: 18
Size: 187094 Color: 16
Size: 185418 Color: 7

Bin 1840: 31 of cap free
Amount of items: 2
Items: 
Size: 768555 Color: 0
Size: 231415 Color: 6

Bin 1841: 31 of cap free
Amount of items: 3
Items: 
Size: 624759 Color: 3
Size: 188932 Color: 10
Size: 186279 Color: 14

Bin 1842: 31 of cap free
Amount of items: 2
Items: 
Size: 732476 Color: 17
Size: 267494 Color: 8

Bin 1843: 31 of cap free
Amount of items: 3
Items: 
Size: 448719 Color: 4
Size: 302124 Color: 15
Size: 249127 Color: 1

Bin 1844: 31 of cap free
Amount of items: 2
Items: 
Size: 630744 Color: 13
Size: 369226 Color: 0

Bin 1845: 31 of cap free
Amount of items: 2
Items: 
Size: 629931 Color: 0
Size: 370039 Color: 6

Bin 1846: 31 of cap free
Amount of items: 2
Items: 
Size: 540330 Color: 11
Size: 459640 Color: 5

Bin 1847: 31 of cap free
Amount of items: 2
Items: 
Size: 548513 Color: 7
Size: 451457 Color: 6

Bin 1848: 31 of cap free
Amount of items: 2
Items: 
Size: 550003 Color: 9
Size: 449967 Color: 14

Bin 1849: 31 of cap free
Amount of items: 2
Items: 
Size: 557730 Color: 4
Size: 442240 Color: 12

Bin 1850: 31 of cap free
Amount of items: 2
Items: 
Size: 559048 Color: 6
Size: 440922 Color: 11

Bin 1851: 31 of cap free
Amount of items: 2
Items: 
Size: 571167 Color: 16
Size: 428803 Color: 9

Bin 1852: 31 of cap free
Amount of items: 2
Items: 
Size: 573568 Color: 6
Size: 426402 Color: 12

Bin 1853: 31 of cap free
Amount of items: 2
Items: 
Size: 613554 Color: 13
Size: 386416 Color: 11

Bin 1854: 31 of cap free
Amount of items: 2
Items: 
Size: 629602 Color: 0
Size: 370368 Color: 12

Bin 1855: 31 of cap free
Amount of items: 2
Items: 
Size: 632463 Color: 1
Size: 367507 Color: 18

Bin 1856: 31 of cap free
Amount of items: 2
Items: 
Size: 655803 Color: 0
Size: 344167 Color: 12

Bin 1857: 31 of cap free
Amount of items: 2
Items: 
Size: 683493 Color: 17
Size: 316477 Color: 12

Bin 1858: 31 of cap free
Amount of items: 2
Items: 
Size: 702921 Color: 4
Size: 297049 Color: 0

Bin 1859: 31 of cap free
Amount of items: 2
Items: 
Size: 741448 Color: 15
Size: 258522 Color: 5

Bin 1860: 31 of cap free
Amount of items: 2
Items: 
Size: 743313 Color: 6
Size: 256657 Color: 18

Bin 1861: 31 of cap free
Amount of items: 2
Items: 
Size: 744283 Color: 0
Size: 255687 Color: 10

Bin 1862: 31 of cap free
Amount of items: 2
Items: 
Size: 790752 Color: 3
Size: 209218 Color: 0

Bin 1863: 31 of cap free
Amount of items: 2
Items: 
Size: 792985 Color: 17
Size: 206985 Color: 11

Bin 1864: 31 of cap free
Amount of items: 2
Items: 
Size: 799029 Color: 7
Size: 200941 Color: 15

Bin 1865: 32 of cap free
Amount of items: 2
Items: 
Size: 710693 Color: 10
Size: 289276 Color: 18

Bin 1866: 32 of cap free
Amount of items: 3
Items: 
Size: 383279 Color: 10
Size: 336061 Color: 13
Size: 280629 Color: 14

Bin 1867: 32 of cap free
Amount of items: 2
Items: 
Size: 724242 Color: 19
Size: 275727 Color: 8

Bin 1868: 32 of cap free
Amount of items: 3
Items: 
Size: 731450 Color: 10
Size: 134356 Color: 17
Size: 134163 Color: 4

Bin 1869: 32 of cap free
Amount of items: 2
Items: 
Size: 601274 Color: 3
Size: 398695 Color: 4

Bin 1870: 32 of cap free
Amount of items: 2
Items: 
Size: 641574 Color: 12
Size: 358395 Color: 13

Bin 1871: 32 of cap free
Amount of items: 2
Items: 
Size: 602175 Color: 10
Size: 397794 Color: 5

Bin 1872: 32 of cap free
Amount of items: 3
Items: 
Size: 381706 Color: 15
Size: 310645 Color: 5
Size: 307618 Color: 2

Bin 1873: 32 of cap free
Amount of items: 2
Items: 
Size: 504029 Color: 13
Size: 495940 Color: 10

Bin 1874: 32 of cap free
Amount of items: 2
Items: 
Size: 518190 Color: 13
Size: 481779 Color: 7

Bin 1875: 32 of cap free
Amount of items: 2
Items: 
Size: 534703 Color: 10
Size: 465266 Color: 12

Bin 1876: 32 of cap free
Amount of items: 2
Items: 
Size: 555902 Color: 2
Size: 444067 Color: 10

Bin 1877: 32 of cap free
Amount of items: 2
Items: 
Size: 556129 Color: 17
Size: 443840 Color: 12

Bin 1878: 32 of cap free
Amount of items: 2
Items: 
Size: 581295 Color: 15
Size: 418674 Color: 2

Bin 1879: 32 of cap free
Amount of items: 2
Items: 
Size: 581939 Color: 14
Size: 418030 Color: 18

Bin 1880: 32 of cap free
Amount of items: 2
Items: 
Size: 586122 Color: 4
Size: 413847 Color: 11

Bin 1881: 32 of cap free
Amount of items: 2
Items: 
Size: 600181 Color: 6
Size: 399788 Color: 9

Bin 1882: 32 of cap free
Amount of items: 2
Items: 
Size: 601466 Color: 5
Size: 398503 Color: 17

Bin 1883: 32 of cap free
Amount of items: 2
Items: 
Size: 602857 Color: 4
Size: 397112 Color: 19

Bin 1884: 32 of cap free
Amount of items: 2
Items: 
Size: 612515 Color: 4
Size: 387454 Color: 15

Bin 1885: 32 of cap free
Amount of items: 2
Items: 
Size: 619993 Color: 0
Size: 379976 Color: 11

Bin 1886: 32 of cap free
Amount of items: 2
Items: 
Size: 623154 Color: 15
Size: 376815 Color: 3

Bin 1887: 32 of cap free
Amount of items: 2
Items: 
Size: 643189 Color: 3
Size: 356780 Color: 10

Bin 1888: 32 of cap free
Amount of items: 2
Items: 
Size: 648203 Color: 2
Size: 351766 Color: 11

Bin 1889: 32 of cap free
Amount of items: 2
Items: 
Size: 684389 Color: 4
Size: 315580 Color: 3

Bin 1890: 32 of cap free
Amount of items: 2
Items: 
Size: 719676 Color: 5
Size: 280293 Color: 3

Bin 1891: 32 of cap free
Amount of items: 2
Items: 
Size: 728555 Color: 11
Size: 271414 Color: 12

Bin 1892: 32 of cap free
Amount of items: 2
Items: 
Size: 764746 Color: 1
Size: 235223 Color: 10

Bin 1893: 32 of cap free
Amount of items: 2
Items: 
Size: 767172 Color: 15
Size: 232797 Color: 10

Bin 1894: 32 of cap free
Amount of items: 2
Items: 
Size: 774024 Color: 4
Size: 225945 Color: 9

Bin 1895: 33 of cap free
Amount of items: 2
Items: 
Size: 764021 Color: 17
Size: 235947 Color: 11

Bin 1896: 33 of cap free
Amount of items: 2
Items: 
Size: 776133 Color: 2
Size: 223835 Color: 16

Bin 1897: 33 of cap free
Amount of items: 3
Items: 
Size: 773381 Color: 17
Size: 113919 Color: 1
Size: 112668 Color: 14

Bin 1898: 33 of cap free
Amount of items: 3
Items: 
Size: 632799 Color: 2
Size: 183692 Color: 9
Size: 183477 Color: 7

Bin 1899: 33 of cap free
Amount of items: 3
Items: 
Size: 757885 Color: 0
Size: 133745 Color: 0
Size: 108338 Color: 2

Bin 1900: 33 of cap free
Amount of items: 2
Items: 
Size: 634863 Color: 18
Size: 365105 Color: 12

Bin 1901: 33 of cap free
Amount of items: 2
Items: 
Size: 534468 Color: 6
Size: 465500 Color: 9

Bin 1902: 33 of cap free
Amount of items: 2
Items: 
Size: 604040 Color: 1
Size: 395928 Color: 0

Bin 1903: 33 of cap free
Amount of items: 2
Items: 
Size: 538181 Color: 2
Size: 461787 Color: 19

Bin 1904: 33 of cap free
Amount of items: 2
Items: 
Size: 550830 Color: 8
Size: 449138 Color: 6

Bin 1905: 33 of cap free
Amount of items: 2
Items: 
Size: 559963 Color: 15
Size: 440005 Color: 9

Bin 1906: 33 of cap free
Amount of items: 2
Items: 
Size: 580291 Color: 12
Size: 419677 Color: 3

Bin 1907: 33 of cap free
Amount of items: 2
Items: 
Size: 584389 Color: 15
Size: 415579 Color: 13

Bin 1908: 33 of cap free
Amount of items: 2
Items: 
Size: 595862 Color: 8
Size: 404106 Color: 14

Bin 1909: 33 of cap free
Amount of items: 2
Items: 
Size: 604075 Color: 18
Size: 395893 Color: 12

Bin 1910: 33 of cap free
Amount of items: 2
Items: 
Size: 622803 Color: 13
Size: 377165 Color: 0

Bin 1911: 33 of cap free
Amount of items: 2
Items: 
Size: 637892 Color: 14
Size: 362076 Color: 7

Bin 1912: 33 of cap free
Amount of items: 2
Items: 
Size: 642637 Color: 11
Size: 357331 Color: 12

Bin 1913: 33 of cap free
Amount of items: 2
Items: 
Size: 652341 Color: 19
Size: 347627 Color: 2

Bin 1914: 33 of cap free
Amount of items: 2
Items: 
Size: 711023 Color: 5
Size: 288945 Color: 11

Bin 1915: 33 of cap free
Amount of items: 2
Items: 
Size: 713185 Color: 7
Size: 286783 Color: 9

Bin 1916: 33 of cap free
Amount of items: 2
Items: 
Size: 743916 Color: 12
Size: 256052 Color: 5

Bin 1917: 33 of cap free
Amount of items: 2
Items: 
Size: 760434 Color: 10
Size: 239534 Color: 9

Bin 1918: 33 of cap free
Amount of items: 2
Items: 
Size: 767412 Color: 11
Size: 232556 Color: 12

Bin 1919: 33 of cap free
Amount of items: 2
Items: 
Size: 769268 Color: 19
Size: 230700 Color: 15

Bin 1920: 33 of cap free
Amount of items: 2
Items: 
Size: 771970 Color: 3
Size: 227998 Color: 11

Bin 1921: 34 of cap free
Amount of items: 3
Items: 
Size: 698958 Color: 19
Size: 194405 Color: 19
Size: 106604 Color: 14

Bin 1922: 34 of cap free
Amount of items: 2
Items: 
Size: 634906 Color: 19
Size: 365061 Color: 6

Bin 1923: 34 of cap free
Amount of items: 3
Items: 
Size: 668583 Color: 8
Size: 167404 Color: 14
Size: 163980 Color: 14

Bin 1924: 34 of cap free
Amount of items: 2
Items: 
Size: 705875 Color: 5
Size: 294092 Color: 11

Bin 1925: 34 of cap free
Amount of items: 2
Items: 
Size: 507501 Color: 17
Size: 492466 Color: 2

Bin 1926: 34 of cap free
Amount of items: 2
Items: 
Size: 521376 Color: 6
Size: 478591 Color: 0

Bin 1927: 34 of cap free
Amount of items: 2
Items: 
Size: 530136 Color: 15
Size: 469831 Color: 16

Bin 1928: 34 of cap free
Amount of items: 2
Items: 
Size: 547840 Color: 9
Size: 452127 Color: 15

Bin 1929: 34 of cap free
Amount of items: 2
Items: 
Size: 552973 Color: 0
Size: 446994 Color: 6

Bin 1930: 34 of cap free
Amount of items: 2
Items: 
Size: 574094 Color: 2
Size: 425873 Color: 14

Bin 1931: 34 of cap free
Amount of items: 2
Items: 
Size: 581883 Color: 4
Size: 418084 Color: 18

Bin 1932: 34 of cap free
Amount of items: 2
Items: 
Size: 595951 Color: 6
Size: 404016 Color: 13

Bin 1933: 34 of cap free
Amount of items: 2
Items: 
Size: 609534 Color: 7
Size: 390433 Color: 4

Bin 1934: 34 of cap free
Amount of items: 2
Items: 
Size: 628775 Color: 12
Size: 371192 Color: 15

Bin 1935: 34 of cap free
Amount of items: 2
Items: 
Size: 648156 Color: 15
Size: 351811 Color: 10

Bin 1936: 34 of cap free
Amount of items: 2
Items: 
Size: 669507 Color: 2
Size: 330460 Color: 9

Bin 1937: 34 of cap free
Amount of items: 2
Items: 
Size: 673736 Color: 13
Size: 326231 Color: 5

Bin 1938: 34 of cap free
Amount of items: 2
Items: 
Size: 775839 Color: 15
Size: 224128 Color: 8

Bin 1939: 35 of cap free
Amount of items: 2
Items: 
Size: 764883 Color: 16
Size: 235083 Color: 17

Bin 1940: 35 of cap free
Amount of items: 3
Items: 
Size: 449556 Color: 3
Size: 300045 Color: 6
Size: 250365 Color: 1

Bin 1941: 35 of cap free
Amount of items: 2
Items: 
Size: 646942 Color: 3
Size: 353024 Color: 12

Bin 1942: 35 of cap free
Amount of items: 2
Items: 
Size: 771524 Color: 14
Size: 228442 Color: 11

Bin 1943: 35 of cap free
Amount of items: 2
Items: 
Size: 644677 Color: 18
Size: 355289 Color: 19

Bin 1944: 35 of cap free
Amount of items: 3
Items: 
Size: 370141 Color: 9
Size: 320869 Color: 0
Size: 308956 Color: 18

Bin 1945: 35 of cap free
Amount of items: 3
Items: 
Size: 590490 Color: 0
Size: 228091 Color: 1
Size: 181385 Color: 14

Bin 1946: 35 of cap free
Amount of items: 3
Items: 
Size: 630678 Color: 12
Size: 184922 Color: 11
Size: 184366 Color: 9

Bin 1947: 35 of cap free
Amount of items: 2
Items: 
Size: 772370 Color: 7
Size: 227596 Color: 13

Bin 1948: 35 of cap free
Amount of items: 2
Items: 
Size: 506292 Color: 5
Size: 493674 Color: 11

Bin 1949: 35 of cap free
Amount of items: 2
Items: 
Size: 511099 Color: 17
Size: 488867 Color: 1

Bin 1950: 35 of cap free
Amount of items: 2
Items: 
Size: 520133 Color: 8
Size: 479833 Color: 16

Bin 1951: 35 of cap free
Amount of items: 2
Items: 
Size: 520485 Color: 15
Size: 479481 Color: 10

Bin 1952: 35 of cap free
Amount of items: 2
Items: 
Size: 530422 Color: 8
Size: 469544 Color: 9

Bin 1953: 35 of cap free
Amount of items: 2
Items: 
Size: 565229 Color: 8
Size: 434737 Color: 10

Bin 1954: 35 of cap free
Amount of items: 2
Items: 
Size: 593662 Color: 11
Size: 406304 Color: 6

Bin 1955: 35 of cap free
Amount of items: 2
Items: 
Size: 600080 Color: 12
Size: 399886 Color: 13

Bin 1956: 35 of cap free
Amount of items: 2
Items: 
Size: 630866 Color: 11
Size: 369100 Color: 4

Bin 1957: 35 of cap free
Amount of items: 2
Items: 
Size: 645676 Color: 19
Size: 354290 Color: 0

Bin 1958: 35 of cap free
Amount of items: 2
Items: 
Size: 659899 Color: 17
Size: 340067 Color: 11

Bin 1959: 35 of cap free
Amount of items: 2
Items: 
Size: 675710 Color: 17
Size: 324256 Color: 4

Bin 1960: 35 of cap free
Amount of items: 2
Items: 
Size: 705311 Color: 6
Size: 294655 Color: 2

Bin 1961: 35 of cap free
Amount of items: 2
Items: 
Size: 754372 Color: 5
Size: 245594 Color: 0

Bin 1962: 35 of cap free
Amount of items: 2
Items: 
Size: 754723 Color: 4
Size: 245243 Color: 6

Bin 1963: 36 of cap free
Amount of items: 2
Items: 
Size: 713543 Color: 9
Size: 286422 Color: 16

Bin 1964: 36 of cap free
Amount of items: 2
Items: 
Size: 638508 Color: 14
Size: 361457 Color: 18

Bin 1965: 36 of cap free
Amount of items: 2
Items: 
Size: 610098 Color: 6
Size: 389867 Color: 12

Bin 1966: 36 of cap free
Amount of items: 3
Items: 
Size: 606543 Color: 2
Size: 218376 Color: 8
Size: 175046 Color: 11

Bin 1967: 36 of cap free
Amount of items: 2
Items: 
Size: 771174 Color: 10
Size: 228791 Color: 15

Bin 1968: 36 of cap free
Amount of items: 2
Items: 
Size: 608705 Color: 9
Size: 391260 Color: 14

Bin 1969: 36 of cap free
Amount of items: 2
Items: 
Size: 720456 Color: 6
Size: 279509 Color: 13

Bin 1970: 36 of cap free
Amount of items: 2
Items: 
Size: 795483 Color: 15
Size: 204482 Color: 2

Bin 1971: 36 of cap free
Amount of items: 2
Items: 
Size: 623327 Color: 13
Size: 376638 Color: 4

Bin 1972: 36 of cap free
Amount of items: 2
Items: 
Size: 626031 Color: 16
Size: 373934 Color: 15

Bin 1973: 36 of cap free
Amount of items: 2
Items: 
Size: 614328 Color: 14
Size: 385637 Color: 4

Bin 1974: 36 of cap free
Amount of items: 2
Items: 
Size: 777462 Color: 13
Size: 222503 Color: 11

Bin 1975: 36 of cap free
Amount of items: 2
Items: 
Size: 699057 Color: 14
Size: 300908 Color: 6

Bin 1976: 36 of cap free
Amount of items: 2
Items: 
Size: 657958 Color: 6
Size: 342007 Color: 5

Bin 1977: 36 of cap free
Amount of items: 2
Items: 
Size: 501499 Color: 12
Size: 498466 Color: 15

Bin 1978: 36 of cap free
Amount of items: 2
Items: 
Size: 508889 Color: 17
Size: 491076 Color: 0

Bin 1979: 36 of cap free
Amount of items: 2
Items: 
Size: 536089 Color: 1
Size: 463876 Color: 7

Bin 1980: 36 of cap free
Amount of items: 2
Items: 
Size: 541393 Color: 19
Size: 458572 Color: 15

Bin 1981: 36 of cap free
Amount of items: 2
Items: 
Size: 545680 Color: 11
Size: 454285 Color: 0

Bin 1982: 36 of cap free
Amount of items: 2
Items: 
Size: 547166 Color: 4
Size: 452799 Color: 15

Bin 1983: 36 of cap free
Amount of items: 2
Items: 
Size: 598880 Color: 19
Size: 401085 Color: 14

Bin 1984: 36 of cap free
Amount of items: 2
Items: 
Size: 605384 Color: 4
Size: 394581 Color: 17

Bin 1985: 36 of cap free
Amount of items: 2
Items: 
Size: 608479 Color: 16
Size: 391486 Color: 3

Bin 1986: 36 of cap free
Amount of items: 2
Items: 
Size: 633414 Color: 12
Size: 366551 Color: 2

Bin 1987: 36 of cap free
Amount of items: 2
Items: 
Size: 640018 Color: 14
Size: 359947 Color: 18

Bin 1988: 36 of cap free
Amount of items: 2
Items: 
Size: 697228 Color: 4
Size: 302737 Color: 0

Bin 1989: 36 of cap free
Amount of items: 2
Items: 
Size: 741348 Color: 18
Size: 258617 Color: 15

Bin 1990: 36 of cap free
Amount of items: 2
Items: 
Size: 787834 Color: 12
Size: 212131 Color: 16

Bin 1991: 36 of cap free
Amount of items: 2
Items: 
Size: 791625 Color: 17
Size: 208340 Color: 7

Bin 1992: 37 of cap free
Amount of items: 2
Items: 
Size: 729847 Color: 8
Size: 270117 Color: 11

Bin 1993: 37 of cap free
Amount of items: 2
Items: 
Size: 690744 Color: 16
Size: 309220 Color: 3

Bin 1994: 37 of cap free
Amount of items: 3
Items: 
Size: 608876 Color: 0
Size: 196282 Color: 16
Size: 194806 Color: 5

Bin 1995: 37 of cap free
Amount of items: 2
Items: 
Size: 538492 Color: 15
Size: 461472 Color: 1

Bin 1996: 37 of cap free
Amount of items: 3
Items: 
Size: 738035 Color: 8
Size: 135089 Color: 4
Size: 126840 Color: 11

Bin 1997: 37 of cap free
Amount of items: 3
Items: 
Size: 681957 Color: 15
Size: 159230 Color: 10
Size: 158777 Color: 17

Bin 1998: 37 of cap free
Amount of items: 2
Items: 
Size: 657462 Color: 8
Size: 342502 Color: 0

Bin 1999: 37 of cap free
Amount of items: 2
Items: 
Size: 790569 Color: 0
Size: 209395 Color: 6

Bin 2000: 37 of cap free
Amount of items: 2
Items: 
Size: 594150 Color: 11
Size: 405814 Color: 3

Bin 2001: 37 of cap free
Amount of items: 2
Items: 
Size: 609325 Color: 9
Size: 390639 Color: 11

Bin 2002: 37 of cap free
Amount of items: 2
Items: 
Size: 507873 Color: 2
Size: 492091 Color: 7

Bin 2003: 37 of cap free
Amount of items: 2
Items: 
Size: 517021 Color: 10
Size: 482943 Color: 4

Bin 2004: 37 of cap free
Amount of items: 2
Items: 
Size: 526805 Color: 9
Size: 473159 Color: 14

Bin 2005: 37 of cap free
Amount of items: 2
Items: 
Size: 541078 Color: 16
Size: 458886 Color: 10

Bin 2006: 37 of cap free
Amount of items: 2
Items: 
Size: 546077 Color: 1
Size: 453887 Color: 18

Bin 2007: 37 of cap free
Amount of items: 2
Items: 
Size: 558036 Color: 4
Size: 441928 Color: 16

Bin 2008: 37 of cap free
Amount of items: 2
Items: 
Size: 581295 Color: 3
Size: 418669 Color: 4

Bin 2009: 37 of cap free
Amount of items: 2
Items: 
Size: 595587 Color: 12
Size: 404377 Color: 8

Bin 2010: 37 of cap free
Amount of items: 2
Items: 
Size: 598937 Color: 2
Size: 401027 Color: 17

Bin 2011: 37 of cap free
Amount of items: 2
Items: 
Size: 614520 Color: 3
Size: 385444 Color: 9

Bin 2012: 37 of cap free
Amount of items: 2
Items: 
Size: 638269 Color: 18
Size: 361695 Color: 16

Bin 2013: 37 of cap free
Amount of items: 2
Items: 
Size: 680327 Color: 1
Size: 319637 Color: 12

Bin 2014: 37 of cap free
Amount of items: 2
Items: 
Size: 693033 Color: 8
Size: 306931 Color: 9

Bin 2015: 37 of cap free
Amount of items: 2
Items: 
Size: 726459 Color: 7
Size: 273505 Color: 9

Bin 2016: 37 of cap free
Amount of items: 2
Items: 
Size: 773281 Color: 13
Size: 226683 Color: 11

Bin 2017: 37 of cap free
Amount of items: 2
Items: 
Size: 796761 Color: 5
Size: 203203 Color: 11

Bin 2018: 38 of cap free
Amount of items: 3
Items: 
Size: 640756 Color: 19
Size: 179683 Color: 9
Size: 179524 Color: 1

Bin 2019: 38 of cap free
Amount of items: 2
Items: 
Size: 785349 Color: 12
Size: 214614 Color: 16

Bin 2020: 38 of cap free
Amount of items: 2
Items: 
Size: 507356 Color: 9
Size: 492607 Color: 8

Bin 2021: 38 of cap free
Amount of items: 2
Items: 
Size: 664916 Color: 2
Size: 335047 Color: 19

Bin 2022: 38 of cap free
Amount of items: 3
Items: 
Size: 604561 Color: 6
Size: 197798 Color: 2
Size: 197604 Color: 8

Bin 2023: 38 of cap free
Amount of items: 2
Items: 
Size: 625119 Color: 9
Size: 374844 Color: 7

Bin 2024: 38 of cap free
Amount of items: 2
Items: 
Size: 763259 Color: 10
Size: 236704 Color: 3

Bin 2025: 38 of cap free
Amount of items: 2
Items: 
Size: 525301 Color: 6
Size: 474662 Color: 15

Bin 2026: 38 of cap free
Amount of items: 2
Items: 
Size: 501163 Color: 6
Size: 498800 Color: 11

Bin 2027: 38 of cap free
Amount of items: 2
Items: 
Size: 515032 Color: 17
Size: 484931 Color: 6

Bin 2028: 38 of cap free
Amount of items: 2
Items: 
Size: 511013 Color: 4
Size: 488950 Color: 15

Bin 2029: 38 of cap free
Amount of items: 2
Items: 
Size: 585136 Color: 5
Size: 414827 Color: 12

Bin 2030: 38 of cap free
Amount of items: 2
Items: 
Size: 626992 Color: 8
Size: 372971 Color: 10

Bin 2031: 38 of cap free
Amount of items: 2
Items: 
Size: 627976 Color: 18
Size: 371987 Color: 12

Bin 2032: 38 of cap free
Amount of items: 2
Items: 
Size: 633764 Color: 16
Size: 366199 Color: 3

Bin 2033: 38 of cap free
Amount of items: 2
Items: 
Size: 639850 Color: 15
Size: 360113 Color: 4

Bin 2034: 38 of cap free
Amount of items: 2
Items: 
Size: 643124 Color: 19
Size: 356839 Color: 9

Bin 2035: 38 of cap free
Amount of items: 2
Items: 
Size: 672157 Color: 2
Size: 327806 Color: 16

Bin 2036: 38 of cap free
Amount of items: 2
Items: 
Size: 673606 Color: 0
Size: 326357 Color: 8

Bin 2037: 38 of cap free
Amount of items: 2
Items: 
Size: 695570 Color: 16
Size: 304393 Color: 18

Bin 2038: 38 of cap free
Amount of items: 2
Items: 
Size: 717108 Color: 3
Size: 282855 Color: 19

Bin 2039: 38 of cap free
Amount of items: 2
Items: 
Size: 776398 Color: 3
Size: 223565 Color: 9

Bin 2040: 39 of cap free
Amount of items: 2
Items: 
Size: 784050 Color: 10
Size: 215912 Color: 14

Bin 2041: 39 of cap free
Amount of items: 3
Items: 
Size: 537378 Color: 7
Size: 253531 Color: 15
Size: 209053 Color: 17

Bin 2042: 39 of cap free
Amount of items: 3
Items: 
Size: 518440 Color: 12
Size: 272691 Color: 13
Size: 208831 Color: 5

Bin 2043: 39 of cap free
Amount of items: 2
Items: 
Size: 620835 Color: 5
Size: 379127 Color: 11

Bin 2044: 39 of cap free
Amount of items: 2
Items: 
Size: 663504 Color: 17
Size: 336458 Color: 11

Bin 2045: 39 of cap free
Amount of items: 2
Items: 
Size: 740636 Color: 14
Size: 259326 Color: 16

Bin 2046: 39 of cap free
Amount of items: 3
Items: 
Size: 417072 Color: 1
Size: 301256 Color: 9
Size: 281634 Color: 4

Bin 2047: 39 of cap free
Amount of items: 2
Items: 
Size: 505939 Color: 9
Size: 494023 Color: 1

Bin 2048: 39 of cap free
Amount of items: 2
Items: 
Size: 534564 Color: 9
Size: 465398 Color: 7

Bin 2049: 39 of cap free
Amount of items: 2
Items: 
Size: 568889 Color: 15
Size: 431073 Color: 10

Bin 2050: 39 of cap free
Amount of items: 2
Items: 
Size: 576938 Color: 1
Size: 423024 Color: 17

Bin 2051: 39 of cap free
Amount of items: 2
Items: 
Size: 593510 Color: 2
Size: 406452 Color: 16

Bin 2052: 39 of cap free
Amount of items: 2
Items: 
Size: 599020 Color: 9
Size: 400942 Color: 11

Bin 2053: 39 of cap free
Amount of items: 2
Items: 
Size: 616384 Color: 4
Size: 383578 Color: 8

Bin 2054: 39 of cap free
Amount of items: 2
Items: 
Size: 637487 Color: 14
Size: 362475 Color: 1

Bin 2055: 39 of cap free
Amount of items: 2
Items: 
Size: 668449 Color: 17
Size: 331513 Color: 1

Bin 2056: 39 of cap free
Amount of items: 2
Items: 
Size: 716929 Color: 0
Size: 283033 Color: 10

Bin 2057: 39 of cap free
Amount of items: 2
Items: 
Size: 720176 Color: 6
Size: 279786 Color: 9

Bin 2058: 39 of cap free
Amount of items: 3
Items: 
Size: 727997 Color: 17
Size: 136365 Color: 14
Size: 135600 Color: 1

Bin 2059: 39 of cap free
Amount of items: 2
Items: 
Size: 754089 Color: 13
Size: 245873 Color: 8

Bin 2060: 39 of cap free
Amount of items: 2
Items: 
Size: 770733 Color: 15
Size: 229229 Color: 12

Bin 2061: 39 of cap free
Amount of items: 2
Items: 
Size: 775875 Color: 3
Size: 224087 Color: 0

Bin 2062: 39 of cap free
Amount of items: 2
Items: 
Size: 786743 Color: 13
Size: 213219 Color: 10

Bin 2063: 40 of cap free
Amount of items: 3
Items: 
Size: 678483 Color: 2
Size: 160770 Color: 19
Size: 160708 Color: 12

Bin 2064: 40 of cap free
Amount of items: 2
Items: 
Size: 549513 Color: 0
Size: 450448 Color: 8

Bin 2065: 40 of cap free
Amount of items: 3
Items: 
Size: 358556 Color: 15
Size: 339115 Color: 0
Size: 302290 Color: 2

Bin 2066: 40 of cap free
Amount of items: 3
Items: 
Size: 606915 Color: 6
Size: 233864 Color: 16
Size: 159182 Color: 16

Bin 2067: 40 of cap free
Amount of items: 2
Items: 
Size: 504414 Color: 5
Size: 495547 Color: 1

Bin 2068: 40 of cap free
Amount of items: 3
Items: 
Size: 725183 Color: 9
Size: 137414 Color: 3
Size: 137364 Color: 5

Bin 2069: 40 of cap free
Amount of items: 2
Items: 
Size: 669234 Color: 2
Size: 330727 Color: 11

Bin 2070: 40 of cap free
Amount of items: 3
Items: 
Size: 783765 Color: 8
Size: 111161 Color: 18
Size: 105035 Color: 19

Bin 2071: 40 of cap free
Amount of items: 2
Items: 
Size: 710621 Color: 8
Size: 289340 Color: 18

Bin 2072: 40 of cap free
Amount of items: 2
Items: 
Size: 762462 Color: 4
Size: 237499 Color: 9

Bin 2073: 40 of cap free
Amount of items: 2
Items: 
Size: 504309 Color: 1
Size: 495652 Color: 19

Bin 2074: 40 of cap free
Amount of items: 2
Items: 
Size: 511234 Color: 3
Size: 488727 Color: 4

Bin 2075: 40 of cap free
Amount of items: 2
Items: 
Size: 525894 Color: 10
Size: 474067 Color: 3

Bin 2076: 40 of cap free
Amount of items: 2
Items: 
Size: 540991 Color: 19
Size: 458970 Color: 9

Bin 2077: 40 of cap free
Amount of items: 2
Items: 
Size: 572943 Color: 15
Size: 427018 Color: 8

Bin 2078: 40 of cap free
Amount of items: 2
Items: 
Size: 579332 Color: 1
Size: 420629 Color: 6

Bin 2079: 40 of cap free
Amount of items: 2
Items: 
Size: 593322 Color: 14
Size: 406639 Color: 18

Bin 2080: 40 of cap free
Amount of items: 2
Items: 
Size: 598700 Color: 5
Size: 401261 Color: 2

Bin 2081: 40 of cap free
Amount of items: 2
Items: 
Size: 602637 Color: 7
Size: 397324 Color: 11

Bin 2082: 40 of cap free
Amount of items: 2
Items: 
Size: 615829 Color: 6
Size: 384132 Color: 18

Bin 2083: 40 of cap free
Amount of items: 2
Items: 
Size: 630575 Color: 11
Size: 369386 Color: 17

Bin 2084: 40 of cap free
Amount of items: 2
Items: 
Size: 664174 Color: 8
Size: 335787 Color: 16

Bin 2085: 40 of cap free
Amount of items: 2
Items: 
Size: 677819 Color: 1
Size: 322142 Color: 16

Bin 2086: 40 of cap free
Amount of items: 2
Items: 
Size: 735818 Color: 17
Size: 264143 Color: 10

Bin 2087: 40 of cap free
Amount of items: 2
Items: 
Size: 765994 Color: 15
Size: 233967 Color: 2

Bin 2088: 40 of cap free
Amount of items: 2
Items: 
Size: 769848 Color: 14
Size: 230113 Color: 13

Bin 2089: 40 of cap free
Amount of items: 2
Items: 
Size: 785960 Color: 9
Size: 214001 Color: 13

Bin 2090: 41 of cap free
Amount of items: 3
Items: 
Size: 651233 Color: 10
Size: 195325 Color: 19
Size: 153402 Color: 8

Bin 2091: 41 of cap free
Amount of items: 3
Items: 
Size: 708619 Color: 1
Size: 145768 Color: 13
Size: 145573 Color: 1

Bin 2092: 41 of cap free
Amount of items: 3
Items: 
Size: 730554 Color: 4
Size: 134744 Color: 16
Size: 134662 Color: 8

Bin 2093: 41 of cap free
Amount of items: 2
Items: 
Size: 561231 Color: 0
Size: 438729 Color: 1

Bin 2094: 41 of cap free
Amount of items: 3
Items: 
Size: 604952 Color: 5
Size: 258891 Color: 6
Size: 136117 Color: 3

Bin 2095: 41 of cap free
Amount of items: 2
Items: 
Size: 586209 Color: 7
Size: 413751 Color: 5

Bin 2096: 41 of cap free
Amount of items: 2
Items: 
Size: 742083 Color: 3
Size: 257877 Color: 6

Bin 2097: 41 of cap free
Amount of items: 2
Items: 
Size: 510738 Color: 19
Size: 489222 Color: 15

Bin 2098: 41 of cap free
Amount of items: 2
Items: 
Size: 536929 Color: 15
Size: 463031 Color: 13

Bin 2099: 41 of cap free
Amount of items: 2
Items: 
Size: 545467 Color: 16
Size: 454493 Color: 12

Bin 2100: 41 of cap free
Amount of items: 2
Items: 
Size: 551731 Color: 19
Size: 448229 Color: 16

Bin 2101: 41 of cap free
Amount of items: 2
Items: 
Size: 564847 Color: 16
Size: 435113 Color: 13

Bin 2102: 41 of cap free
Amount of items: 2
Items: 
Size: 576287 Color: 1
Size: 423673 Color: 13

Bin 2103: 41 of cap free
Amount of items: 2
Items: 
Size: 585631 Color: 11
Size: 414329 Color: 9

Bin 2104: 41 of cap free
Amount of items: 2
Items: 
Size: 595808 Color: 5
Size: 404152 Color: 15

Bin 2105: 41 of cap free
Amount of items: 2
Items: 
Size: 595948 Color: 4
Size: 404012 Color: 12

Bin 2106: 41 of cap free
Amount of items: 2
Items: 
Size: 645512 Color: 17
Size: 354448 Color: 2

Bin 2107: 41 of cap free
Amount of items: 2
Items: 
Size: 673066 Color: 19
Size: 326894 Color: 2

Bin 2108: 41 of cap free
Amount of items: 2
Items: 
Size: 708615 Color: 8
Size: 291345 Color: 16

Bin 2109: 41 of cap free
Amount of items: 2
Items: 
Size: 753408 Color: 4
Size: 246552 Color: 14

Bin 2110: 41 of cap free
Amount of items: 2
Items: 
Size: 774972 Color: 18
Size: 224988 Color: 13

Bin 2111: 41 of cap free
Amount of items: 2
Items: 
Size: 782745 Color: 16
Size: 217215 Color: 4

Bin 2112: 42 of cap free
Amount of items: 3
Items: 
Size: 624620 Color: 1
Size: 188037 Color: 0
Size: 187302 Color: 13

Bin 2113: 42 of cap free
Amount of items: 3
Items: 
Size: 649725 Color: 1
Size: 178179 Color: 9
Size: 172055 Color: 12

Bin 2114: 42 of cap free
Amount of items: 2
Items: 
Size: 523551 Color: 8
Size: 476408 Color: 4

Bin 2115: 42 of cap free
Amount of items: 2
Items: 
Size: 668511 Color: 6
Size: 331448 Color: 1

Bin 2116: 42 of cap free
Amount of items: 2
Items: 
Size: 726067 Color: 5
Size: 273892 Color: 10

Bin 2117: 42 of cap free
Amount of items: 3
Items: 
Size: 610776 Color: 13
Size: 194636 Color: 2
Size: 194547 Color: 7

Bin 2118: 42 of cap free
Amount of items: 2
Items: 
Size: 740218 Color: 6
Size: 259741 Color: 15

Bin 2119: 42 of cap free
Amount of items: 2
Items: 
Size: 560828 Color: 6
Size: 439131 Color: 0

Bin 2120: 42 of cap free
Amount of items: 2
Items: 
Size: 573028 Color: 14
Size: 426931 Color: 0

Bin 2121: 42 of cap free
Amount of items: 2
Items: 
Size: 577545 Color: 5
Size: 422414 Color: 15

Bin 2122: 42 of cap free
Amount of items: 2
Items: 
Size: 578620 Color: 8
Size: 421339 Color: 11

Bin 2123: 42 of cap free
Amount of items: 2
Items: 
Size: 597843 Color: 12
Size: 402116 Color: 1

Bin 2124: 42 of cap free
Amount of items: 2
Items: 
Size: 743404 Color: 1
Size: 256555 Color: 10

Bin 2125: 42 of cap free
Amount of items: 2
Items: 
Size: 764235 Color: 5
Size: 235724 Color: 2

Bin 2126: 42 of cap free
Amount of items: 2
Items: 
Size: 782648 Color: 11
Size: 217311 Color: 16

Bin 2127: 42 of cap free
Amount of items: 2
Items: 
Size: 784233 Color: 0
Size: 215726 Color: 9

Bin 2128: 43 of cap free
Amount of items: 2
Items: 
Size: 783140 Color: 3
Size: 216818 Color: 11

Bin 2129: 43 of cap free
Amount of items: 2
Items: 
Size: 665063 Color: 16
Size: 334895 Color: 6

Bin 2130: 43 of cap free
Amount of items: 3
Items: 
Size: 632475 Color: 9
Size: 184089 Color: 17
Size: 183394 Color: 3

Bin 2131: 43 of cap free
Amount of items: 3
Items: 
Size: 729546 Color: 16
Size: 137239 Color: 1
Size: 133173 Color: 6

Bin 2132: 43 of cap free
Amount of items: 3
Items: 
Size: 722061 Color: 12
Size: 146616 Color: 3
Size: 131281 Color: 0

Bin 2133: 43 of cap free
Amount of items: 2
Items: 
Size: 783833 Color: 15
Size: 216125 Color: 7

Bin 2134: 43 of cap free
Amount of items: 2
Items: 
Size: 505136 Color: 5
Size: 494822 Color: 16

Bin 2135: 43 of cap free
Amount of items: 2
Items: 
Size: 563475 Color: 12
Size: 436483 Color: 17

Bin 2136: 43 of cap free
Amount of items: 2
Items: 
Size: 569243 Color: 7
Size: 430715 Color: 1

Bin 2137: 43 of cap free
Amount of items: 2
Items: 
Size: 570990 Color: 15
Size: 428968 Color: 19

Bin 2138: 43 of cap free
Amount of items: 2
Items: 
Size: 595670 Color: 17
Size: 404288 Color: 9

Bin 2139: 43 of cap free
Amount of items: 2
Items: 
Size: 597542 Color: 6
Size: 402416 Color: 16

Bin 2140: 43 of cap free
Amount of items: 2
Items: 
Size: 614021 Color: 15
Size: 385937 Color: 5

Bin 2141: 43 of cap free
Amount of items: 2
Items: 
Size: 626158 Color: 9
Size: 373800 Color: 3

Bin 2142: 43 of cap free
Amount of items: 2
Items: 
Size: 639113 Color: 1
Size: 360845 Color: 3

Bin 2143: 43 of cap free
Amount of items: 2
Items: 
Size: 669023 Color: 0
Size: 330935 Color: 9

Bin 2144: 43 of cap free
Amount of items: 2
Items: 
Size: 669635 Color: 1
Size: 330323 Color: 15

Bin 2145: 43 of cap free
Amount of items: 2
Items: 
Size: 681444 Color: 5
Size: 318514 Color: 8

Bin 2146: 43 of cap free
Amount of items: 2
Items: 
Size: 739447 Color: 10
Size: 260511 Color: 4

Bin 2147: 44 of cap free
Amount of items: 3
Items: 
Size: 651121 Color: 18
Size: 194982 Color: 12
Size: 153854 Color: 4

Bin 2148: 44 of cap free
Amount of items: 2
Items: 
Size: 629803 Color: 7
Size: 370154 Color: 17

Bin 2149: 44 of cap free
Amount of items: 2
Items: 
Size: 746893 Color: 9
Size: 253064 Color: 11

Bin 2150: 44 of cap free
Amount of items: 2
Items: 
Size: 769209 Color: 18
Size: 230748 Color: 16

Bin 2151: 44 of cap free
Amount of items: 2
Items: 
Size: 768450 Color: 9
Size: 231507 Color: 4

Bin 2152: 44 of cap free
Amount of items: 2
Items: 
Size: 665110 Color: 10
Size: 334847 Color: 14

Bin 2153: 44 of cap free
Amount of items: 2
Items: 
Size: 735367 Color: 5
Size: 264590 Color: 12

Bin 2154: 44 of cap free
Amount of items: 2
Items: 
Size: 517601 Color: 16
Size: 482356 Color: 18

Bin 2155: 44 of cap free
Amount of items: 3
Items: 
Size: 384886 Color: 7
Size: 310733 Color: 13
Size: 304338 Color: 7

Bin 2156: 44 of cap free
Amount of items: 2
Items: 
Size: 528675 Color: 17
Size: 471282 Color: 13

Bin 2157: 44 of cap free
Amount of items: 2
Items: 
Size: 545357 Color: 9
Size: 454600 Color: 0

Bin 2158: 44 of cap free
Amount of items: 2
Items: 
Size: 563323 Color: 14
Size: 436634 Color: 6

Bin 2159: 44 of cap free
Amount of items: 2
Items: 
Size: 566885 Color: 2
Size: 433072 Color: 1

Bin 2160: 44 of cap free
Amount of items: 2
Items: 
Size: 579836 Color: 16
Size: 420121 Color: 10

Bin 2161: 44 of cap free
Amount of items: 2
Items: 
Size: 580791 Color: 17
Size: 419166 Color: 15

Bin 2162: 44 of cap free
Amount of items: 2
Items: 
Size: 581774 Color: 6
Size: 418183 Color: 17

Bin 2163: 44 of cap free
Amount of items: 2
Items: 
Size: 628720 Color: 13
Size: 371237 Color: 11

Bin 2164: 44 of cap free
Amount of items: 2
Items: 
Size: 639964 Color: 11
Size: 359993 Color: 1

Bin 2165: 44 of cap free
Amount of items: 2
Items: 
Size: 733361 Color: 6
Size: 266596 Color: 0

Bin 2166: 44 of cap free
Amount of items: 2
Items: 
Size: 732996 Color: 3
Size: 266961 Color: 1

Bin 2167: 44 of cap free
Amount of items: 2
Items: 
Size: 740342 Color: 10
Size: 259615 Color: 8

Bin 2168: 45 of cap free
Amount of items: 2
Items: 
Size: 514929 Color: 5
Size: 485027 Color: 8

Bin 2169: 45 of cap free
Amount of items: 2
Items: 
Size: 511527 Color: 6
Size: 488429 Color: 16

Bin 2170: 45 of cap free
Amount of items: 2
Items: 
Size: 736293 Color: 12
Size: 263663 Color: 2

Bin 2171: 45 of cap free
Amount of items: 3
Items: 
Size: 607031 Color: 15
Size: 258989 Color: 1
Size: 133936 Color: 14

Bin 2172: 45 of cap free
Amount of items: 2
Items: 
Size: 707561 Color: 0
Size: 292395 Color: 1

Bin 2173: 45 of cap free
Amount of items: 2
Items: 
Size: 793280 Color: 18
Size: 206676 Color: 0

Bin 2174: 45 of cap free
Amount of items: 2
Items: 
Size: 526061 Color: 12
Size: 473895 Color: 10

Bin 2175: 45 of cap free
Amount of items: 2
Items: 
Size: 592950 Color: 1
Size: 407006 Color: 4

Bin 2176: 45 of cap free
Amount of items: 2
Items: 
Size: 500058 Color: 16
Size: 499898 Color: 19

Bin 2177: 45 of cap free
Amount of items: 2
Items: 
Size: 500236 Color: 11
Size: 499720 Color: 13

Bin 2178: 45 of cap free
Amount of items: 2
Items: 
Size: 512459 Color: 2
Size: 487497 Color: 4

Bin 2179: 45 of cap free
Amount of items: 2
Items: 
Size: 518620 Color: 7
Size: 481336 Color: 12

Bin 2180: 45 of cap free
Amount of items: 2
Items: 
Size: 544776 Color: 12
Size: 455180 Color: 2

Bin 2181: 45 of cap free
Amount of items: 2
Items: 
Size: 604407 Color: 10
Size: 395549 Color: 0

Bin 2182: 45 of cap free
Amount of items: 2
Items: 
Size: 609016 Color: 13
Size: 390940 Color: 19

Bin 2183: 45 of cap free
Amount of items: 2
Items: 
Size: 625165 Color: 13
Size: 374791 Color: 12

Bin 2184: 45 of cap free
Amount of items: 2
Items: 
Size: 631765 Color: 13
Size: 368191 Color: 5

Bin 2185: 45 of cap free
Amount of items: 2
Items: 
Size: 699293 Color: 17
Size: 300663 Color: 13

Bin 2186: 45 of cap free
Amount of items: 2
Items: 
Size: 701075 Color: 1
Size: 298881 Color: 8

Bin 2187: 45 of cap free
Amount of items: 2
Items: 
Size: 722531 Color: 5
Size: 277425 Color: 4

Bin 2188: 45 of cap free
Amount of items: 2
Items: 
Size: 734495 Color: 5
Size: 265461 Color: 14

Bin 2189: 45 of cap free
Amount of items: 2
Items: 
Size: 737469 Color: 10
Size: 262487 Color: 1

Bin 2190: 45 of cap free
Amount of items: 2
Items: 
Size: 794881 Color: 2
Size: 205075 Color: 14

Bin 2191: 46 of cap free
Amount of items: 3
Items: 
Size: 687853 Color: 19
Size: 159591 Color: 9
Size: 152511 Color: 8

Bin 2192: 46 of cap free
Amount of items: 2
Items: 
Size: 660608 Color: 1
Size: 339347 Color: 19

Bin 2193: 46 of cap free
Amount of items: 2
Items: 
Size: 608938 Color: 0
Size: 391017 Color: 7

Bin 2194: 46 of cap free
Amount of items: 2
Items: 
Size: 638505 Color: 13
Size: 361450 Color: 17

Bin 2195: 46 of cap free
Amount of items: 2
Items: 
Size: 752757 Color: 3
Size: 247198 Color: 4

Bin 2196: 46 of cap free
Amount of items: 2
Items: 
Size: 798939 Color: 3
Size: 201016 Color: 13

Bin 2197: 46 of cap free
Amount of items: 2
Items: 
Size: 507494 Color: 5
Size: 492461 Color: 0

Bin 2198: 46 of cap free
Amount of items: 2
Items: 
Size: 683932 Color: 19
Size: 316023 Color: 12

Bin 2199: 46 of cap free
Amount of items: 2
Items: 
Size: 737731 Color: 11
Size: 262224 Color: 19

Bin 2200: 46 of cap free
Amount of items: 2
Items: 
Size: 695116 Color: 9
Size: 304839 Color: 0

Bin 2201: 46 of cap free
Amount of items: 2
Items: 
Size: 727193 Color: 6
Size: 272762 Color: 10

Bin 2202: 46 of cap free
Amount of items: 2
Items: 
Size: 508185 Color: 7
Size: 491770 Color: 11

Bin 2203: 46 of cap free
Amount of items: 2
Items: 
Size: 635876 Color: 8
Size: 364079 Color: 5

Bin 2204: 46 of cap free
Amount of items: 2
Items: 
Size: 787605 Color: 13
Size: 212350 Color: 8

Bin 2205: 46 of cap free
Amount of items: 2
Items: 
Size: 501702 Color: 9
Size: 498253 Color: 17

Bin 2206: 46 of cap free
Amount of items: 2
Items: 
Size: 529173 Color: 4
Size: 470782 Color: 10

Bin 2207: 46 of cap free
Amount of items: 2
Items: 
Size: 536368 Color: 11
Size: 463587 Color: 2

Bin 2208: 46 of cap free
Amount of items: 2
Items: 
Size: 587354 Color: 10
Size: 412601 Color: 17

Bin 2209: 46 of cap free
Amount of items: 2
Items: 
Size: 604454 Color: 14
Size: 395501 Color: 3

Bin 2210: 46 of cap free
Amount of items: 2
Items: 
Size: 647787 Color: 12
Size: 352168 Color: 13

Bin 2211: 46 of cap free
Amount of items: 2
Items: 
Size: 651688 Color: 19
Size: 348267 Color: 11

Bin 2212: 46 of cap free
Amount of items: 2
Items: 
Size: 675781 Color: 19
Size: 324174 Color: 12

Bin 2213: 46 of cap free
Amount of items: 2
Items: 
Size: 684730 Color: 6
Size: 315225 Color: 8

Bin 2214: 46 of cap free
Amount of items: 2
Items: 
Size: 733233 Color: 13
Size: 266722 Color: 9

Bin 2215: 46 of cap free
Amount of items: 2
Items: 
Size: 767569 Color: 10
Size: 232386 Color: 17

Bin 2216: 46 of cap free
Amount of items: 2
Items: 
Size: 770882 Color: 18
Size: 229073 Color: 9

Bin 2217: 46 of cap free
Amount of items: 2
Items: 
Size: 777377 Color: 12
Size: 222578 Color: 1

Bin 2218: 47 of cap free
Amount of items: 2
Items: 
Size: 680522 Color: 4
Size: 319432 Color: 5

Bin 2219: 47 of cap free
Amount of items: 2
Items: 
Size: 665301 Color: 16
Size: 334653 Color: 2

Bin 2220: 47 of cap free
Amount of items: 2
Items: 
Size: 704026 Color: 12
Size: 295928 Color: 10

Bin 2221: 47 of cap free
Amount of items: 3
Items: 
Size: 761722 Color: 18
Size: 119121 Color: 13
Size: 119111 Color: 3

Bin 2222: 47 of cap free
Amount of items: 2
Items: 
Size: 794237 Color: 19
Size: 205717 Color: 4

Bin 2223: 47 of cap free
Amount of items: 2
Items: 
Size: 707912 Color: 13
Size: 292042 Color: 17

Bin 2224: 47 of cap free
Amount of items: 2
Items: 
Size: 587639 Color: 6
Size: 412315 Color: 4

Bin 2225: 47 of cap free
Amount of items: 2
Items: 
Size: 672761 Color: 9
Size: 327193 Color: 4

Bin 2226: 47 of cap free
Amount of items: 2
Items: 
Size: 550197 Color: 3
Size: 449757 Color: 6

Bin 2227: 47 of cap free
Amount of items: 2
Items: 
Size: 542425 Color: 18
Size: 457529 Color: 7

Bin 2228: 47 of cap free
Amount of items: 2
Items: 
Size: 519593 Color: 8
Size: 480361 Color: 14

Bin 2229: 47 of cap free
Amount of items: 2
Items: 
Size: 504922 Color: 0
Size: 495032 Color: 5

Bin 2230: 47 of cap free
Amount of items: 2
Items: 
Size: 509324 Color: 2
Size: 490630 Color: 18

Bin 2231: 47 of cap free
Amount of items: 2
Items: 
Size: 510977 Color: 3
Size: 488977 Color: 4

Bin 2232: 47 of cap free
Amount of items: 2
Items: 
Size: 518850 Color: 6
Size: 481104 Color: 16

Bin 2233: 47 of cap free
Amount of items: 2
Items: 
Size: 550137 Color: 15
Size: 449817 Color: 13

Bin 2234: 47 of cap free
Amount of items: 2
Items: 
Size: 561591 Color: 9
Size: 438363 Color: 10

Bin 2235: 47 of cap free
Amount of items: 2
Items: 
Size: 576072 Color: 7
Size: 423882 Color: 11

Bin 2236: 47 of cap free
Amount of items: 2
Items: 
Size: 594665 Color: 15
Size: 405289 Color: 3

Bin 2237: 47 of cap free
Amount of items: 2
Items: 
Size: 616968 Color: 12
Size: 382986 Color: 0

Bin 2238: 47 of cap free
Amount of items: 2
Items: 
Size: 621352 Color: 5
Size: 378602 Color: 3

Bin 2239: 47 of cap free
Amount of items: 2
Items: 
Size: 640933 Color: 7
Size: 359021 Color: 10

Bin 2240: 47 of cap free
Amount of items: 2
Items: 
Size: 650543 Color: 0
Size: 349411 Color: 19

Bin 2241: 47 of cap free
Amount of items: 2
Items: 
Size: 661701 Color: 3
Size: 338253 Color: 0

Bin 2242: 47 of cap free
Amount of items: 2
Items: 
Size: 730899 Color: 17
Size: 269055 Color: 8

Bin 2243: 47 of cap free
Amount of items: 2
Items: 
Size: 771408 Color: 17
Size: 228546 Color: 10

Bin 2244: 47 of cap free
Amount of items: 2
Items: 
Size: 776851 Color: 4
Size: 223103 Color: 0

Bin 2245: 48 of cap free
Amount of items: 2
Items: 
Size: 631363 Color: 10
Size: 368590 Color: 4

Bin 2246: 48 of cap free
Amount of items: 2
Items: 
Size: 628630 Color: 3
Size: 371323 Color: 0

Bin 2247: 48 of cap free
Amount of items: 2
Items: 
Size: 660776 Color: 4
Size: 339177 Color: 1

Bin 2248: 48 of cap free
Amount of items: 2
Items: 
Size: 728983 Color: 10
Size: 270970 Color: 0

Bin 2249: 48 of cap free
Amount of items: 3
Items: 
Size: 744810 Color: 6
Size: 129189 Color: 13
Size: 125954 Color: 2

Bin 2250: 48 of cap free
Amount of items: 2
Items: 
Size: 615325 Color: 4
Size: 384628 Color: 18

Bin 2251: 48 of cap free
Amount of items: 2
Items: 
Size: 738770 Color: 11
Size: 261183 Color: 18

Bin 2252: 48 of cap free
Amount of items: 3
Items: 
Size: 735521 Color: 10
Size: 132304 Color: 6
Size: 132128 Color: 14

Bin 2253: 48 of cap free
Amount of items: 2
Items: 
Size: 663773 Color: 17
Size: 336180 Color: 7

Bin 2254: 48 of cap free
Amount of items: 2
Items: 
Size: 723130 Color: 8
Size: 276823 Color: 14

Bin 2255: 48 of cap free
Amount of items: 2
Items: 
Size: 518307 Color: 0
Size: 481646 Color: 3

Bin 2256: 48 of cap free
Amount of items: 2
Items: 
Size: 697461 Color: 11
Size: 302492 Color: 8

Bin 2257: 48 of cap free
Amount of items: 2
Items: 
Size: 714106 Color: 8
Size: 285847 Color: 6

Bin 2258: 48 of cap free
Amount of items: 2
Items: 
Size: 540314 Color: 13
Size: 459639 Color: 2

Bin 2259: 48 of cap free
Amount of items: 2
Items: 
Size: 614510 Color: 17
Size: 385443 Color: 9

Bin 2260: 48 of cap free
Amount of items: 2
Items: 
Size: 642912 Color: 12
Size: 357041 Color: 0

Bin 2261: 48 of cap free
Amount of items: 2
Items: 
Size: 733145 Color: 5
Size: 266808 Color: 15

Bin 2262: 48 of cap free
Amount of items: 2
Items: 
Size: 751865 Color: 14
Size: 248088 Color: 7

Bin 2263: 49 of cap free
Amount of items: 2
Items: 
Size: 747360 Color: 0
Size: 252592 Color: 2

Bin 2264: 49 of cap free
Amount of items: 3
Items: 
Size: 722999 Color: 19
Size: 158528 Color: 19
Size: 118425 Color: 10

Bin 2265: 49 of cap free
Amount of items: 2
Items: 
Size: 531489 Color: 18
Size: 468463 Color: 4

Bin 2266: 49 of cap free
Amount of items: 3
Items: 
Size: 418712 Color: 15
Size: 338184 Color: 12
Size: 243056 Color: 1

Bin 2267: 49 of cap free
Amount of items: 2
Items: 
Size: 501213 Color: 10
Size: 498739 Color: 14

Bin 2268: 49 of cap free
Amount of items: 2
Items: 
Size: 524360 Color: 16
Size: 475592 Color: 18

Bin 2269: 49 of cap free
Amount of items: 2
Items: 
Size: 548301 Color: 18
Size: 451651 Color: 16

Bin 2270: 49 of cap free
Amount of items: 2
Items: 
Size: 566479 Color: 6
Size: 433473 Color: 18

Bin 2271: 49 of cap free
Amount of items: 2
Items: 
Size: 580480 Color: 4
Size: 419472 Color: 16

Bin 2272: 49 of cap free
Amount of items: 2
Items: 
Size: 600505 Color: 11
Size: 399447 Color: 0

Bin 2273: 49 of cap free
Amount of items: 2
Items: 
Size: 633038 Color: 19
Size: 366914 Color: 9

Bin 2274: 49 of cap free
Amount of items: 2
Items: 
Size: 684549 Color: 3
Size: 315403 Color: 2

Bin 2275: 49 of cap free
Amount of items: 2
Items: 
Size: 741582 Color: 19
Size: 258370 Color: 9

Bin 2276: 49 of cap free
Amount of items: 2
Items: 
Size: 755027 Color: 19
Size: 244925 Color: 16

Bin 2277: 50 of cap free
Amount of items: 3
Items: 
Size: 723773 Color: 18
Size: 151141 Color: 10
Size: 125037 Color: 8

Bin 2278: 50 of cap free
Amount of items: 2
Items: 
Size: 751143 Color: 4
Size: 248808 Color: 3

Bin 2279: 50 of cap free
Amount of items: 2
Items: 
Size: 788695 Color: 19
Size: 211256 Color: 0

Bin 2280: 50 of cap free
Amount of items: 2
Items: 
Size: 745275 Color: 7
Size: 254676 Color: 17

Bin 2281: 50 of cap free
Amount of items: 2
Items: 
Size: 736604 Color: 14
Size: 263347 Color: 18

Bin 2282: 50 of cap free
Amount of items: 2
Items: 
Size: 703513 Color: 10
Size: 296438 Color: 13

Bin 2283: 50 of cap free
Amount of items: 2
Items: 
Size: 749225 Color: 3
Size: 250726 Color: 6

Bin 2284: 50 of cap free
Amount of items: 2
Items: 
Size: 641713 Color: 9
Size: 358238 Color: 6

Bin 2285: 50 of cap free
Amount of items: 2
Items: 
Size: 758384 Color: 13
Size: 241567 Color: 7

Bin 2286: 50 of cap free
Amount of items: 2
Items: 
Size: 522344 Color: 8
Size: 477607 Color: 2

Bin 2287: 50 of cap free
Amount of items: 2
Items: 
Size: 587578 Color: 2
Size: 412373 Color: 9

Bin 2288: 50 of cap free
Amount of items: 2
Items: 
Size: 591786 Color: 16
Size: 408165 Color: 12

Bin 2289: 50 of cap free
Amount of items: 2
Items: 
Size: 621814 Color: 6
Size: 378137 Color: 4

Bin 2290: 50 of cap free
Amount of items: 2
Items: 
Size: 695392 Color: 3
Size: 304559 Color: 5

Bin 2291: 50 of cap free
Amount of items: 2
Items: 
Size: 715081 Color: 9
Size: 284870 Color: 17

Bin 2292: 50 of cap free
Amount of items: 2
Items: 
Size: 725772 Color: 5
Size: 274179 Color: 2

Bin 2293: 50 of cap free
Amount of items: 2
Items: 
Size: 739789 Color: 8
Size: 260162 Color: 15

Bin 2294: 50 of cap free
Amount of items: 2
Items: 
Size: 785736 Color: 14
Size: 214215 Color: 17

Bin 2295: 51 of cap free
Amount of items: 3
Items: 
Size: 710086 Color: 13
Size: 146041 Color: 14
Size: 143823 Color: 16

Bin 2296: 51 of cap free
Amount of items: 3
Items: 
Size: 709666 Color: 18
Size: 145228 Color: 0
Size: 145056 Color: 6

Bin 2297: 51 of cap free
Amount of items: 3
Items: 
Size: 381566 Color: 14
Size: 379367 Color: 4
Size: 239017 Color: 16

Bin 2298: 51 of cap free
Amount of items: 2
Items: 
Size: 723962 Color: 14
Size: 275988 Color: 18

Bin 2299: 51 of cap free
Amount of items: 2
Items: 
Size: 674429 Color: 18
Size: 325521 Color: 9

Bin 2300: 51 of cap free
Amount of items: 2
Items: 
Size: 759211 Color: 17
Size: 240739 Color: 19

Bin 2301: 51 of cap free
Amount of items: 2
Items: 
Size: 705102 Color: 1
Size: 294848 Color: 9

Bin 2302: 51 of cap free
Amount of items: 2
Items: 
Size: 611196 Color: 5
Size: 388754 Color: 10

Bin 2303: 51 of cap free
Amount of items: 2
Items: 
Size: 673124 Color: 2
Size: 326826 Color: 10

Bin 2304: 51 of cap free
Amount of items: 2
Items: 
Size: 657514 Color: 6
Size: 342436 Color: 3

Bin 2305: 51 of cap free
Amount of items: 2
Items: 
Size: 509057 Color: 9
Size: 490893 Color: 19

Bin 2306: 51 of cap free
Amount of items: 2
Items: 
Size: 567960 Color: 8
Size: 431990 Color: 11

Bin 2307: 51 of cap free
Amount of items: 2
Items: 
Size: 600683 Color: 12
Size: 399267 Color: 4

Bin 2308: 51 of cap free
Amount of items: 2
Items: 
Size: 607758 Color: 12
Size: 392192 Color: 13

Bin 2309: 51 of cap free
Amount of items: 2
Items: 
Size: 750909 Color: 17
Size: 249041 Color: 3

Bin 2310: 51 of cap free
Amount of items: 2
Items: 
Size: 777030 Color: 0
Size: 222920 Color: 10

Bin 2311: 51 of cap free
Amount of items: 2
Items: 
Size: 787156 Color: 0
Size: 212794 Color: 15

Bin 2312: 52 of cap free
Amount of items: 2
Items: 
Size: 646036 Color: 14
Size: 353913 Color: 3

Bin 2313: 52 of cap free
Amount of items: 2
Items: 
Size: 769522 Color: 14
Size: 230427 Color: 15

Bin 2314: 52 of cap free
Amount of items: 2
Items: 
Size: 784283 Color: 9
Size: 215666 Color: 16

Bin 2315: 52 of cap free
Amount of items: 2
Items: 
Size: 502932 Color: 5
Size: 497017 Color: 7

Bin 2316: 52 of cap free
Amount of items: 2
Items: 
Size: 510836 Color: 16
Size: 489113 Color: 4

Bin 2317: 52 of cap free
Amount of items: 2
Items: 
Size: 530583 Color: 8
Size: 469366 Color: 16

Bin 2318: 52 of cap free
Amount of items: 2
Items: 
Size: 562584 Color: 5
Size: 437365 Color: 2

Bin 2319: 52 of cap free
Amount of items: 2
Items: 
Size: 590320 Color: 11
Size: 409629 Color: 10

Bin 2320: 52 of cap free
Amount of items: 2
Items: 
Size: 591102 Color: 10
Size: 408847 Color: 4

Bin 2321: 52 of cap free
Amount of items: 2
Items: 
Size: 646216 Color: 13
Size: 353733 Color: 14

Bin 2322: 52 of cap free
Amount of items: 2
Items: 
Size: 727052 Color: 18
Size: 272897 Color: 8

Bin 2323: 52 of cap free
Amount of items: 2
Items: 
Size: 739926 Color: 3
Size: 260023 Color: 15

Bin 2324: 53 of cap free
Amount of items: 3
Items: 
Size: 678418 Color: 8
Size: 166688 Color: 10
Size: 154842 Color: 14

Bin 2325: 53 of cap free
Amount of items: 2
Items: 
Size: 745361 Color: 2
Size: 254587 Color: 14

Bin 2326: 53 of cap free
Amount of items: 2
Items: 
Size: 766491 Color: 6
Size: 233457 Color: 12

Bin 2327: 53 of cap free
Amount of items: 3
Items: 
Size: 492878 Color: 1
Size: 272222 Color: 13
Size: 234848 Color: 12

Bin 2328: 53 of cap free
Amount of items: 3
Items: 
Size: 617397 Color: 10
Size: 192218 Color: 1
Size: 190333 Color: 6

Bin 2329: 53 of cap free
Amount of items: 2
Items: 
Size: 556810 Color: 5
Size: 443138 Color: 8

Bin 2330: 53 of cap free
Amount of items: 2
Items: 
Size: 594804 Color: 14
Size: 405144 Color: 4

Bin 2331: 53 of cap free
Amount of items: 2
Items: 
Size: 599277 Color: 0
Size: 400671 Color: 10

Bin 2332: 53 of cap free
Amount of items: 3
Items: 
Size: 603963 Color: 15
Size: 198018 Color: 14
Size: 197967 Color: 15

Bin 2333: 53 of cap free
Amount of items: 2
Items: 
Size: 634750 Color: 10
Size: 365198 Color: 4

Bin 2334: 53 of cap free
Amount of items: 2
Items: 
Size: 654120 Color: 4
Size: 345828 Color: 5

Bin 2335: 53 of cap free
Amount of items: 2
Items: 
Size: 661068 Color: 13
Size: 338880 Color: 19

Bin 2336: 53 of cap free
Amount of items: 2
Items: 
Size: 684493 Color: 2
Size: 315455 Color: 16

Bin 2337: 53 of cap free
Amount of items: 2
Items: 
Size: 732604 Color: 19
Size: 267344 Color: 11

Bin 2338: 53 of cap free
Amount of items: 2
Items: 
Size: 737124 Color: 6
Size: 262824 Color: 7

Bin 2339: 53 of cap free
Amount of items: 2
Items: 
Size: 753203 Color: 10
Size: 246745 Color: 1

Bin 2340: 53 of cap free
Amount of items: 2
Items: 
Size: 753554 Color: 5
Size: 246394 Color: 16

Bin 2341: 53 of cap free
Amount of items: 2
Items: 
Size: 794058 Color: 11
Size: 205890 Color: 9

Bin 2342: 54 of cap free
Amount of items: 2
Items: 
Size: 682688 Color: 9
Size: 317259 Color: 16

Bin 2343: 54 of cap free
Amount of items: 2
Items: 
Size: 750345 Color: 18
Size: 249602 Color: 19

Bin 2344: 54 of cap free
Amount of items: 2
Items: 
Size: 648962 Color: 9
Size: 350985 Color: 3

Bin 2345: 54 of cap free
Amount of items: 2
Items: 
Size: 500132 Color: 14
Size: 499815 Color: 9

Bin 2346: 54 of cap free
Amount of items: 2
Items: 
Size: 664717 Color: 5
Size: 335230 Color: 9

Bin 2347: 54 of cap free
Amount of items: 3
Items: 
Size: 745953 Color: 2
Size: 128126 Color: 8
Size: 125868 Color: 1

Bin 2348: 54 of cap free
Amount of items: 2
Items: 
Size: 544877 Color: 6
Size: 455070 Color: 16

Bin 2349: 54 of cap free
Amount of items: 2
Items: 
Size: 553039 Color: 7
Size: 446908 Color: 3

Bin 2350: 54 of cap free
Amount of items: 2
Items: 
Size: 570888 Color: 2
Size: 429059 Color: 16

Bin 2351: 54 of cap free
Amount of items: 2
Items: 
Size: 576069 Color: 8
Size: 423878 Color: 16

Bin 2352: 54 of cap free
Amount of items: 2
Items: 
Size: 577140 Color: 12
Size: 422807 Color: 1

Bin 2353: 54 of cap free
Amount of items: 2
Items: 
Size: 606717 Color: 11
Size: 393230 Color: 19

Bin 2354: 54 of cap free
Amount of items: 2
Items: 
Size: 676848 Color: 6
Size: 323099 Color: 0

Bin 2355: 54 of cap free
Amount of items: 2
Items: 
Size: 709010 Color: 18
Size: 290937 Color: 12

Bin 2356: 54 of cap free
Amount of items: 2
Items: 
Size: 723409 Color: 18
Size: 276538 Color: 7

Bin 2357: 54 of cap free
Amount of items: 2
Items: 
Size: 757250 Color: 0
Size: 242697 Color: 9

Bin 2358: 54 of cap free
Amount of items: 2
Items: 
Size: 765324 Color: 9
Size: 234623 Color: 2

Bin 2359: 54 of cap free
Amount of items: 2
Items: 
Size: 766713 Color: 7
Size: 233234 Color: 12

Bin 2360: 54 of cap free
Amount of items: 2
Items: 
Size: 796122 Color: 14
Size: 203825 Color: 4

Bin 2361: 55 of cap free
Amount of items: 2
Items: 
Size: 666309 Color: 11
Size: 333637 Color: 19

Bin 2362: 55 of cap free
Amount of items: 2
Items: 
Size: 741274 Color: 7
Size: 258672 Color: 6

Bin 2363: 55 of cap free
Amount of items: 2
Items: 
Size: 790835 Color: 3
Size: 209111 Color: 15

Bin 2364: 55 of cap free
Amount of items: 2
Items: 
Size: 698780 Color: 1
Size: 301166 Color: 14

Bin 2365: 55 of cap free
Amount of items: 3
Items: 
Size: 795869 Color: 4
Size: 102208 Color: 15
Size: 101869 Color: 2

Bin 2366: 55 of cap free
Amount of items: 2
Items: 
Size: 676173 Color: 12
Size: 323773 Color: 18

Bin 2367: 55 of cap free
Amount of items: 2
Items: 
Size: 709790 Color: 11
Size: 290156 Color: 3

Bin 2368: 55 of cap free
Amount of items: 2
Items: 
Size: 688262 Color: 4
Size: 311684 Color: 5

Bin 2369: 55 of cap free
Amount of items: 2
Items: 
Size: 762730 Color: 4
Size: 237216 Color: 15

Bin 2370: 55 of cap free
Amount of items: 2
Items: 
Size: 615685 Color: 11
Size: 384261 Color: 1

Bin 2371: 55 of cap free
Amount of items: 3
Items: 
Size: 674090 Color: 8
Size: 183110 Color: 12
Size: 142746 Color: 19

Bin 2372: 55 of cap free
Amount of items: 2
Items: 
Size: 535951 Color: 13
Size: 463995 Color: 16

Bin 2373: 55 of cap free
Amount of items: 2
Items: 
Size: 539583 Color: 11
Size: 460363 Color: 14

Bin 2374: 55 of cap free
Amount of items: 2
Items: 
Size: 542123 Color: 11
Size: 457823 Color: 13

Bin 2375: 55 of cap free
Amount of items: 2
Items: 
Size: 754354 Color: 7
Size: 245592 Color: 9

Bin 2376: 55 of cap free
Amount of items: 2
Items: 
Size: 761613 Color: 1
Size: 238333 Color: 19

Bin 2377: 56 of cap free
Amount of items: 2
Items: 
Size: 517703 Color: 9
Size: 482242 Color: 8

Bin 2378: 56 of cap free
Amount of items: 2
Items: 
Size: 799665 Color: 3
Size: 200280 Color: 6

Bin 2379: 56 of cap free
Amount of items: 2
Items: 
Size: 666900 Color: 5
Size: 333045 Color: 14

Bin 2380: 56 of cap free
Amount of items: 2
Items: 
Size: 739043 Color: 14
Size: 260902 Color: 1

Bin 2381: 56 of cap free
Amount of items: 2
Items: 
Size: 508739 Color: 2
Size: 491206 Color: 1

Bin 2382: 56 of cap free
Amount of items: 2
Items: 
Size: 514301 Color: 4
Size: 485644 Color: 10

Bin 2383: 56 of cap free
Amount of items: 2
Items: 
Size: 521899 Color: 7
Size: 478046 Color: 9

Bin 2384: 56 of cap free
Amount of items: 2
Items: 
Size: 548151 Color: 12
Size: 451794 Color: 16

Bin 2385: 56 of cap free
Amount of items: 2
Items: 
Size: 561086 Color: 17
Size: 438859 Color: 16

Bin 2386: 56 of cap free
Amount of items: 2
Items: 
Size: 603127 Color: 9
Size: 396818 Color: 15

Bin 2387: 56 of cap free
Amount of items: 2
Items: 
Size: 617625 Color: 14
Size: 382320 Color: 15

Bin 2388: 56 of cap free
Amount of items: 2
Items: 
Size: 634395 Color: 12
Size: 365550 Color: 7

Bin 2389: 56 of cap free
Amount of items: 2
Items: 
Size: 639954 Color: 11
Size: 359991 Color: 3

Bin 2390: 56 of cap free
Amount of items: 2
Items: 
Size: 771645 Color: 8
Size: 228300 Color: 9

Bin 2391: 56 of cap free
Amount of items: 2
Items: 
Size: 772322 Color: 17
Size: 227623 Color: 13

Bin 2392: 57 of cap free
Amount of items: 2
Items: 
Size: 795462 Color: 17
Size: 204482 Color: 12

Bin 2393: 57 of cap free
Amount of items: 2
Items: 
Size: 796643 Color: 12
Size: 203301 Color: 14

Bin 2394: 57 of cap free
Amount of items: 2
Items: 
Size: 752302 Color: 5
Size: 247642 Color: 8

Bin 2395: 57 of cap free
Amount of items: 2
Items: 
Size: 709146 Color: 11
Size: 290798 Color: 0

Bin 2396: 57 of cap free
Amount of items: 2
Items: 
Size: 708900 Color: 0
Size: 291044 Color: 1

Bin 2397: 57 of cap free
Amount of items: 3
Items: 
Size: 617042 Color: 6
Size: 191819 Color: 7
Size: 191083 Color: 13

Bin 2398: 57 of cap free
Amount of items: 2
Items: 
Size: 786338 Color: 0
Size: 213606 Color: 3

Bin 2399: 57 of cap free
Amount of items: 2
Items: 
Size: 522985 Color: 13
Size: 476959 Color: 14

Bin 2400: 57 of cap free
Amount of items: 2
Items: 
Size: 546924 Color: 10
Size: 453020 Color: 19

Bin 2401: 57 of cap free
Amount of items: 2
Items: 
Size: 559414 Color: 1
Size: 440530 Color: 5

Bin 2402: 57 of cap free
Amount of items: 2
Items: 
Size: 564015 Color: 13
Size: 435929 Color: 11

Bin 2403: 57 of cap free
Amount of items: 2
Items: 
Size: 575436 Color: 4
Size: 424508 Color: 0

Bin 2404: 57 of cap free
Amount of items: 2
Items: 
Size: 662429 Color: 1
Size: 337515 Color: 3

Bin 2405: 57 of cap free
Amount of items: 2
Items: 
Size: 686908 Color: 18
Size: 313036 Color: 1

Bin 2406: 57 of cap free
Amount of items: 2
Items: 
Size: 740388 Color: 12
Size: 259556 Color: 14

Bin 2407: 57 of cap free
Amount of items: 2
Items: 
Size: 778700 Color: 15
Size: 221244 Color: 16

Bin 2408: 57 of cap free
Amount of items: 2
Items: 
Size: 782412 Color: 0
Size: 217532 Color: 12

Bin 2409: 58 of cap free
Amount of items: 2
Items: 
Size: 766409 Color: 10
Size: 233534 Color: 2

Bin 2410: 58 of cap free
Amount of items: 2
Items: 
Size: 501061 Color: 4
Size: 498882 Color: 3

Bin 2411: 58 of cap free
Amount of items: 2
Items: 
Size: 759291 Color: 10
Size: 240652 Color: 2

Bin 2412: 58 of cap free
Amount of items: 3
Items: 
Size: 516940 Color: 11
Size: 242495 Color: 5
Size: 240508 Color: 5

Bin 2413: 58 of cap free
Amount of items: 2
Items: 
Size: 664575 Color: 18
Size: 335368 Color: 0

Bin 2414: 58 of cap free
Amount of items: 2
Items: 
Size: 524782 Color: 17
Size: 475161 Color: 8

Bin 2415: 58 of cap free
Amount of items: 2
Items: 
Size: 543343 Color: 6
Size: 456600 Color: 0

Bin 2416: 58 of cap free
Amount of items: 2
Items: 
Size: 563010 Color: 3
Size: 436933 Color: 19

Bin 2417: 58 of cap free
Amount of items: 2
Items: 
Size: 567222 Color: 10
Size: 432721 Color: 9

Bin 2418: 58 of cap free
Amount of items: 2
Items: 
Size: 577658 Color: 17
Size: 422285 Color: 10

Bin 2419: 58 of cap free
Amount of items: 2
Items: 
Size: 600137 Color: 8
Size: 399806 Color: 13

Bin 2420: 58 of cap free
Amount of items: 2
Items: 
Size: 623149 Color: 18
Size: 376794 Color: 15

Bin 2421: 58 of cap free
Amount of items: 2
Items: 
Size: 667337 Color: 7
Size: 332606 Color: 10

Bin 2422: 58 of cap free
Amount of items: 2
Items: 
Size: 671972 Color: 2
Size: 327971 Color: 10

Bin 2423: 58 of cap free
Amount of items: 2
Items: 
Size: 717957 Color: 17
Size: 281986 Color: 4

Bin 2424: 59 of cap free
Amount of items: 3
Items: 
Size: 734037 Color: 10
Size: 137441 Color: 2
Size: 128464 Color: 0

Bin 2425: 59 of cap free
Amount of items: 2
Items: 
Size: 772874 Color: 0
Size: 227068 Color: 1

Bin 2426: 59 of cap free
Amount of items: 2
Items: 
Size: 773517 Color: 14
Size: 226425 Color: 2

Bin 2427: 59 of cap free
Amount of items: 2
Items: 
Size: 748652 Color: 6
Size: 251290 Color: 1

Bin 2428: 59 of cap free
Amount of items: 2
Items: 
Size: 517515 Color: 2
Size: 482427 Color: 0

Bin 2429: 59 of cap free
Amount of items: 2
Items: 
Size: 564389 Color: 18
Size: 435553 Color: 2

Bin 2430: 59 of cap free
Amount of items: 2
Items: 
Size: 593142 Color: 4
Size: 406800 Color: 14

Bin 2431: 59 of cap free
Amount of items: 2
Items: 
Size: 602233 Color: 5
Size: 397709 Color: 13

Bin 2432: 59 of cap free
Amount of items: 2
Items: 
Size: 652718 Color: 13
Size: 347224 Color: 1

Bin 2433: 59 of cap free
Amount of items: 2
Items: 
Size: 679420 Color: 15
Size: 320522 Color: 10

Bin 2434: 59 of cap free
Amount of items: 2
Items: 
Size: 723563 Color: 9
Size: 276379 Color: 5

Bin 2435: 60 of cap free
Amount of items: 2
Items: 
Size: 781094 Color: 0
Size: 218847 Color: 12

Bin 2436: 60 of cap free
Amount of items: 2
Items: 
Size: 681282 Color: 7
Size: 318659 Color: 2

Bin 2437: 60 of cap free
Amount of items: 2
Items: 
Size: 513834 Color: 3
Size: 486107 Color: 6

Bin 2438: 60 of cap free
Amount of items: 2
Items: 
Size: 506275 Color: 1
Size: 493666 Color: 10

Bin 2439: 60 of cap free
Amount of items: 2
Items: 
Size: 508586 Color: 10
Size: 491355 Color: 11

Bin 2440: 60 of cap free
Amount of items: 2
Items: 
Size: 528224 Color: 5
Size: 471717 Color: 19

Bin 2441: 60 of cap free
Amount of items: 2
Items: 
Size: 546148 Color: 1
Size: 453793 Color: 5

Bin 2442: 60 of cap free
Amount of items: 2
Items: 
Size: 568770 Color: 6
Size: 431171 Color: 9

Bin 2443: 60 of cap free
Amount of items: 2
Items: 
Size: 615253 Color: 1
Size: 384688 Color: 7

Bin 2444: 60 of cap free
Amount of items: 2
Items: 
Size: 621771 Color: 16
Size: 378170 Color: 12

Bin 2445: 60 of cap free
Amount of items: 2
Items: 
Size: 641093 Color: 0
Size: 358848 Color: 17

Bin 2446: 60 of cap free
Amount of items: 2
Items: 
Size: 765682 Color: 6
Size: 234259 Color: 13

Bin 2447: 61 of cap free
Amount of items: 2
Items: 
Size: 754867 Color: 8
Size: 245073 Color: 1

Bin 2448: 61 of cap free
Amount of items: 2
Items: 
Size: 746729 Color: 15
Size: 253211 Color: 17

Bin 2449: 61 of cap free
Amount of items: 2
Items: 
Size: 681944 Color: 17
Size: 317996 Color: 6

Bin 2450: 61 of cap free
Amount of items: 2
Items: 
Size: 561422 Color: 6
Size: 438518 Color: 5

Bin 2451: 61 of cap free
Amount of items: 2
Items: 
Size: 570754 Color: 16
Size: 429186 Color: 5

Bin 2452: 61 of cap free
Amount of items: 2
Items: 
Size: 683435 Color: 9
Size: 316505 Color: 17

Bin 2453: 61 of cap free
Amount of items: 2
Items: 
Size: 755707 Color: 14
Size: 244233 Color: 1

Bin 2454: 62 of cap free
Amount of items: 3
Items: 
Size: 764213 Color: 17
Size: 135257 Color: 6
Size: 100469 Color: 13

Bin 2455: 62 of cap free
Amount of items: 3
Items: 
Size: 411386 Color: 1
Size: 337113 Color: 0
Size: 251440 Color: 8

Bin 2456: 62 of cap free
Amount of items: 2
Items: 
Size: 667882 Color: 4
Size: 332057 Color: 13

Bin 2457: 62 of cap free
Amount of items: 2
Items: 
Size: 542967 Color: 4
Size: 456972 Color: 9

Bin 2458: 62 of cap free
Amount of items: 2
Items: 
Size: 665766 Color: 10
Size: 334173 Color: 0

Bin 2459: 62 of cap free
Amount of items: 2
Items: 
Size: 704961 Color: 12
Size: 294978 Color: 15

Bin 2460: 62 of cap free
Amount of items: 3
Items: 
Size: 627580 Color: 12
Size: 190534 Color: 13
Size: 181825 Color: 1

Bin 2461: 62 of cap free
Amount of items: 2
Items: 
Size: 512191 Color: 9
Size: 487748 Color: 2

Bin 2462: 62 of cap free
Amount of items: 2
Items: 
Size: 570661 Color: 4
Size: 429278 Color: 16

Bin 2463: 62 of cap free
Amount of items: 2
Items: 
Size: 610157 Color: 15
Size: 389782 Color: 8

Bin 2464: 62 of cap free
Amount of items: 2
Items: 
Size: 629694 Color: 19
Size: 370245 Color: 13

Bin 2465: 62 of cap free
Amount of items: 2
Items: 
Size: 633753 Color: 14
Size: 366186 Color: 17

Bin 2466: 62 of cap free
Amount of items: 2
Items: 
Size: 683760 Color: 15
Size: 316179 Color: 10

Bin 2467: 62 of cap free
Amount of items: 2
Items: 
Size: 791753 Color: 0
Size: 208186 Color: 3

Bin 2468: 63 of cap free
Amount of items: 3
Items: 
Size: 742861 Color: 2
Size: 151478 Color: 7
Size: 105599 Color: 14

Bin 2469: 63 of cap free
Amount of items: 2
Items: 
Size: 799943 Color: 12
Size: 199995 Color: 8

Bin 2470: 63 of cap free
Amount of items: 3
Items: 
Size: 684116 Color: 12
Size: 182418 Color: 1
Size: 133404 Color: 6

Bin 2471: 63 of cap free
Amount of items: 2
Items: 
Size: 517073 Color: 7
Size: 482865 Color: 6

Bin 2472: 63 of cap free
Amount of items: 2
Items: 
Size: 660607 Color: 6
Size: 339331 Color: 17

Bin 2473: 63 of cap free
Amount of items: 2
Items: 
Size: 549567 Color: 2
Size: 450371 Color: 6

Bin 2474: 63 of cap free
Amount of items: 2
Items: 
Size: 720402 Color: 4
Size: 279536 Color: 17

Bin 2475: 63 of cap free
Amount of items: 2
Items: 
Size: 703201 Color: 1
Size: 296737 Color: 10

Bin 2476: 63 of cap free
Amount of items: 2
Items: 
Size: 674229 Color: 16
Size: 325709 Color: 18

Bin 2477: 63 of cap free
Amount of items: 2
Items: 
Size: 618794 Color: 2
Size: 381144 Color: 6

Bin 2478: 63 of cap free
Amount of items: 2
Items: 
Size: 511764 Color: 11
Size: 488174 Color: 16

Bin 2479: 63 of cap free
Amount of items: 2
Items: 
Size: 544124 Color: 19
Size: 455814 Color: 9

Bin 2480: 63 of cap free
Amount of items: 2
Items: 
Size: 564071 Color: 11
Size: 435867 Color: 13

Bin 2481: 63 of cap free
Amount of items: 2
Items: 
Size: 566805 Color: 13
Size: 433133 Color: 14

Bin 2482: 63 of cap free
Amount of items: 2
Items: 
Size: 627701 Color: 3
Size: 372237 Color: 5

Bin 2483: 63 of cap free
Amount of items: 2
Items: 
Size: 672013 Color: 10
Size: 327925 Color: 8

Bin 2484: 63 of cap free
Amount of items: 2
Items: 
Size: 683172 Color: 5
Size: 316766 Color: 8

Bin 2485: 63 of cap free
Amount of items: 2
Items: 
Size: 689390 Color: 10
Size: 310548 Color: 2

Bin 2486: 63 of cap free
Amount of items: 2
Items: 
Size: 727849 Color: 16
Size: 272089 Color: 4

Bin 2487: 64 of cap free
Amount of items: 2
Items: 
Size: 716322 Color: 0
Size: 283615 Color: 4

Bin 2488: 64 of cap free
Amount of items: 2
Items: 
Size: 623662 Color: 6
Size: 376275 Color: 7

Bin 2489: 64 of cap free
Amount of items: 2
Items: 
Size: 739162 Color: 14
Size: 260775 Color: 11

Bin 2490: 64 of cap free
Amount of items: 2
Items: 
Size: 727247 Color: 6
Size: 272690 Color: 4

Bin 2491: 64 of cap free
Amount of items: 3
Items: 
Size: 413113 Color: 4
Size: 335993 Color: 17
Size: 250831 Color: 3

Bin 2492: 64 of cap free
Amount of items: 3
Items: 
Size: 654756 Color: 13
Size: 172739 Color: 8
Size: 172442 Color: 13

Bin 2493: 64 of cap free
Amount of items: 2
Items: 
Size: 779952 Color: 4
Size: 219985 Color: 18

Bin 2494: 64 of cap free
Amount of items: 2
Items: 
Size: 663168 Color: 12
Size: 336769 Color: 18

Bin 2495: 64 of cap free
Amount of items: 2
Items: 
Size: 522876 Color: 4
Size: 477061 Color: 19

Bin 2496: 64 of cap free
Amount of items: 2
Items: 
Size: 630840 Color: 7
Size: 369097 Color: 4

Bin 2497: 64 of cap free
Amount of items: 2
Items: 
Size: 635934 Color: 2
Size: 364003 Color: 11

Bin 2498: 64 of cap free
Amount of items: 2
Items: 
Size: 661361 Color: 17
Size: 338576 Color: 5

Bin 2499: 64 of cap free
Amount of items: 2
Items: 
Size: 768059 Color: 4
Size: 231878 Color: 13

Bin 2500: 64 of cap free
Amount of items: 2
Items: 
Size: 771786 Color: 4
Size: 228151 Color: 1

Bin 2501: 64 of cap free
Amount of items: 2
Items: 
Size: 791907 Color: 12
Size: 208030 Color: 4

Bin 2502: 65 of cap free
Amount of items: 2
Items: 
Size: 650106 Color: 9
Size: 349830 Color: 17

Bin 2503: 65 of cap free
Amount of items: 2
Items: 
Size: 630446 Color: 12
Size: 369490 Color: 7

Bin 2504: 65 of cap free
Amount of items: 2
Items: 
Size: 766605 Color: 15
Size: 233331 Color: 5

Bin 2505: 65 of cap free
Amount of items: 2
Items: 
Size: 522106 Color: 11
Size: 477830 Color: 5

Bin 2506: 65 of cap free
Amount of items: 2
Items: 
Size: 535860 Color: 13
Size: 464076 Color: 3

Bin 2507: 65 of cap free
Amount of items: 2
Items: 
Size: 552600 Color: 8
Size: 447336 Color: 3

Bin 2508: 65 of cap free
Amount of items: 2
Items: 
Size: 553966 Color: 10
Size: 445970 Color: 11

Bin 2509: 65 of cap free
Amount of items: 2
Items: 
Size: 566516 Color: 8
Size: 433420 Color: 7

Bin 2510: 65 of cap free
Amount of items: 2
Items: 
Size: 648199 Color: 0
Size: 351737 Color: 17

Bin 2511: 65 of cap free
Amount of items: 2
Items: 
Size: 649271 Color: 8
Size: 350665 Color: 16

Bin 2512: 65 of cap free
Amount of items: 2
Items: 
Size: 703058 Color: 9
Size: 296878 Color: 5

Bin 2513: 65 of cap free
Amount of items: 2
Items: 
Size: 780739 Color: 17
Size: 219197 Color: 3

Bin 2514: 66 of cap free
Amount of items: 2
Items: 
Size: 623398 Color: 11
Size: 376537 Color: 13

Bin 2515: 66 of cap free
Amount of items: 2
Items: 
Size: 670393 Color: 13
Size: 329542 Color: 17

Bin 2516: 66 of cap free
Amount of items: 2
Items: 
Size: 745944 Color: 19
Size: 253991 Color: 13

Bin 2517: 66 of cap free
Amount of items: 2
Items: 
Size: 509718 Color: 6
Size: 490217 Color: 2

Bin 2518: 66 of cap free
Amount of items: 2
Items: 
Size: 545756 Color: 19
Size: 454179 Color: 17

Bin 2519: 66 of cap free
Amount of items: 2
Items: 
Size: 548603 Color: 1
Size: 451332 Color: 4

Bin 2520: 66 of cap free
Amount of items: 2
Items: 
Size: 606189 Color: 8
Size: 393746 Color: 13

Bin 2521: 66 of cap free
Amount of items: 2
Items: 
Size: 611619 Color: 5
Size: 388316 Color: 3

Bin 2522: 66 of cap free
Amount of items: 2
Items: 
Size: 643771 Color: 16
Size: 356164 Color: 3

Bin 2523: 66 of cap free
Amount of items: 2
Items: 
Size: 710279 Color: 17
Size: 289656 Color: 7

Bin 2524: 66 of cap free
Amount of items: 2
Items: 
Size: 752989 Color: 7
Size: 246946 Color: 12

Bin 2525: 66 of cap free
Amount of items: 2
Items: 
Size: 766117 Color: 16
Size: 233818 Color: 1

Bin 2526: 67 of cap free
Amount of items: 6
Items: 
Size: 253048 Color: 3
Size: 173663 Color: 5
Size: 171847 Color: 15
Size: 156452 Color: 19
Size: 126469 Color: 8
Size: 118455 Color: 3

Bin 2527: 67 of cap free
Amount of items: 2
Items: 
Size: 646938 Color: 9
Size: 352996 Color: 3

Bin 2528: 67 of cap free
Amount of items: 2
Items: 
Size: 667067 Color: 1
Size: 332867 Color: 9

Bin 2529: 67 of cap free
Amount of items: 2
Items: 
Size: 730310 Color: 13
Size: 269624 Color: 16

Bin 2530: 67 of cap free
Amount of items: 2
Items: 
Size: 670172 Color: 2
Size: 329762 Color: 11

Bin 2531: 67 of cap free
Amount of items: 2
Items: 
Size: 508581 Color: 4
Size: 491353 Color: 15

Bin 2532: 67 of cap free
Amount of items: 2
Items: 
Size: 544916 Color: 9
Size: 455018 Color: 11

Bin 2533: 67 of cap free
Amount of items: 2
Items: 
Size: 560702 Color: 11
Size: 439232 Color: 19

Bin 2534: 67 of cap free
Amount of items: 2
Items: 
Size: 699410 Color: 10
Size: 300524 Color: 0

Bin 2535: 67 of cap free
Amount of items: 2
Items: 
Size: 716640 Color: 19
Size: 283294 Color: 8

Bin 2536: 68 of cap free
Amount of items: 2
Items: 
Size: 563763 Color: 6
Size: 436170 Color: 18

Bin 2537: 68 of cap free
Amount of items: 2
Items: 
Size: 690843 Color: 7
Size: 309090 Color: 14

Bin 2538: 68 of cap free
Amount of items: 3
Items: 
Size: 412314 Color: 18
Size: 305925 Color: 17
Size: 281694 Color: 15

Bin 2539: 68 of cap free
Amount of items: 2
Items: 
Size: 666409 Color: 10
Size: 333524 Color: 9

Bin 2540: 68 of cap free
Amount of items: 2
Items: 
Size: 551964 Color: 14
Size: 447969 Color: 12

Bin 2541: 68 of cap free
Amount of items: 2
Items: 
Size: 559162 Color: 7
Size: 440771 Color: 10

Bin 2542: 68 of cap free
Amount of items: 2
Items: 
Size: 585945 Color: 17
Size: 413988 Color: 7

Bin 2543: 68 of cap free
Amount of items: 2
Items: 
Size: 588728 Color: 11
Size: 411205 Color: 5

Bin 2544: 68 of cap free
Amount of items: 2
Items: 
Size: 669395 Color: 15
Size: 330538 Color: 7

Bin 2545: 68 of cap free
Amount of items: 2
Items: 
Size: 701630 Color: 4
Size: 298303 Color: 15

Bin 2546: 68 of cap free
Amount of items: 2
Items: 
Size: 736406 Color: 15
Size: 263527 Color: 2

Bin 2547: 68 of cap free
Amount of items: 2
Items: 
Size: 784021 Color: 12
Size: 215912 Color: 10

Bin 2548: 69 of cap free
Amount of items: 2
Items: 
Size: 738945 Color: 3
Size: 260987 Color: 7

Bin 2549: 69 of cap free
Amount of items: 2
Items: 
Size: 727635 Color: 5
Size: 272297 Color: 0

Bin 2550: 69 of cap free
Amount of items: 2
Items: 
Size: 582247 Color: 4
Size: 417685 Color: 12

Bin 2551: 69 of cap free
Amount of items: 2
Items: 
Size: 644568 Color: 0
Size: 355364 Color: 6

Bin 2552: 69 of cap free
Amount of items: 2
Items: 
Size: 502362 Color: 12
Size: 497570 Color: 9

Bin 2553: 69 of cap free
Amount of items: 3
Items: 
Size: 374224 Color: 6
Size: 323333 Color: 13
Size: 302375 Color: 16

Bin 2554: 69 of cap free
Amount of items: 3
Items: 
Size: 645907 Color: 13
Size: 177013 Color: 6
Size: 177012 Color: 7

Bin 2555: 69 of cap free
Amount of items: 2
Items: 
Size: 561987 Color: 9
Size: 437945 Color: 6

Bin 2556: 69 of cap free
Amount of items: 2
Items: 
Size: 585377 Color: 2
Size: 414555 Color: 8

Bin 2557: 69 of cap free
Amount of items: 2
Items: 
Size: 586694 Color: 12
Size: 413238 Color: 11

Bin 2558: 69 of cap free
Amount of items: 2
Items: 
Size: 614816 Color: 3
Size: 385116 Color: 8

Bin 2559: 69 of cap free
Amount of items: 2
Items: 
Size: 704487 Color: 11
Size: 295445 Color: 3

Bin 2560: 69 of cap free
Amount of items: 2
Items: 
Size: 757034 Color: 9
Size: 242898 Color: 19

Bin 2561: 70 of cap free
Amount of items: 2
Items: 
Size: 735722 Color: 9
Size: 264209 Color: 14

Bin 2562: 70 of cap free
Amount of items: 3
Items: 
Size: 543877 Color: 11
Size: 265740 Color: 8
Size: 190314 Color: 14

Bin 2563: 70 of cap free
Amount of items: 3
Items: 
Size: 641869 Color: 3
Size: 226046 Color: 15
Size: 132016 Color: 18

Bin 2564: 70 of cap free
Amount of items: 2
Items: 
Size: 794133 Color: 9
Size: 205798 Color: 1

Bin 2565: 70 of cap free
Amount of items: 2
Items: 
Size: 663380 Color: 4
Size: 336551 Color: 12

Bin 2566: 70 of cap free
Amount of items: 2
Items: 
Size: 619903 Color: 7
Size: 380028 Color: 13

Bin 2567: 70 of cap free
Amount of items: 2
Items: 
Size: 511317 Color: 17
Size: 488614 Color: 15

Bin 2568: 70 of cap free
Amount of items: 2
Items: 
Size: 515779 Color: 14
Size: 484152 Color: 10

Bin 2569: 70 of cap free
Amount of items: 2
Items: 
Size: 549477 Color: 7
Size: 450454 Color: 0

Bin 2570: 70 of cap free
Amount of items: 2
Items: 
Size: 551868 Color: 3
Size: 448063 Color: 6

Bin 2571: 70 of cap free
Amount of items: 2
Items: 
Size: 560960 Color: 4
Size: 438971 Color: 6

Bin 2572: 70 of cap free
Amount of items: 2
Items: 
Size: 565581 Color: 19
Size: 434350 Color: 11

Bin 2573: 70 of cap free
Amount of items: 2
Items: 
Size: 575335 Color: 11
Size: 424596 Color: 10

Bin 2574: 70 of cap free
Amount of items: 2
Items: 
Size: 578121 Color: 10
Size: 421810 Color: 15

Bin 2575: 70 of cap free
Amount of items: 2
Items: 
Size: 590932 Color: 1
Size: 408999 Color: 15

Bin 2576: 70 of cap free
Amount of items: 2
Items: 
Size: 634737 Color: 3
Size: 365194 Color: 15

Bin 2577: 70 of cap free
Amount of items: 2
Items: 
Size: 682936 Color: 0
Size: 316995 Color: 18

Bin 2578: 70 of cap free
Amount of items: 2
Items: 
Size: 751856 Color: 16
Size: 248075 Color: 13

Bin 2579: 71 of cap free
Amount of items: 3
Items: 
Size: 720260 Color: 3
Size: 150491 Color: 18
Size: 129179 Color: 9

Bin 2580: 71 of cap free
Amount of items: 2
Items: 
Size: 751552 Color: 4
Size: 248378 Color: 16

Bin 2581: 71 of cap free
Amount of items: 2
Items: 
Size: 712765 Color: 9
Size: 287165 Color: 2

Bin 2582: 71 of cap free
Amount of items: 2
Items: 
Size: 718388 Color: 12
Size: 281542 Color: 8

Bin 2583: 71 of cap free
Amount of items: 2
Items: 
Size: 724668 Color: 4
Size: 275262 Color: 2

Bin 2584: 71 of cap free
Amount of items: 2
Items: 
Size: 637161 Color: 11
Size: 362769 Color: 6

Bin 2585: 71 of cap free
Amount of items: 2
Items: 
Size: 766363 Color: 10
Size: 233567 Color: 8

Bin 2586: 71 of cap free
Amount of items: 2
Items: 
Size: 772715 Color: 5
Size: 227215 Color: 10

Bin 2587: 71 of cap free
Amount of items: 2
Items: 
Size: 504096 Color: 7
Size: 495834 Color: 0

Bin 2588: 71 of cap free
Amount of items: 2
Items: 
Size: 554656 Color: 9
Size: 445274 Color: 17

Bin 2589: 71 of cap free
Amount of items: 2
Items: 
Size: 557720 Color: 3
Size: 442210 Color: 14

Bin 2590: 71 of cap free
Amount of items: 2
Items: 
Size: 551846 Color: 6
Size: 448084 Color: 12

Bin 2591: 71 of cap free
Amount of items: 2
Items: 
Size: 577269 Color: 7
Size: 422661 Color: 12

Bin 2592: 71 of cap free
Amount of items: 2
Items: 
Size: 613430 Color: 11
Size: 386500 Color: 15

Bin 2593: 71 of cap free
Amount of items: 2
Items: 
Size: 640929 Color: 13
Size: 359001 Color: 10

Bin 2594: 71 of cap free
Amount of items: 2
Items: 
Size: 778097 Color: 14
Size: 221833 Color: 13

Bin 2595: 72 of cap free
Amount of items: 2
Items: 
Size: 721408 Color: 10
Size: 278521 Color: 14

Bin 2596: 72 of cap free
Amount of items: 2
Items: 
Size: 729683 Color: 8
Size: 270246 Color: 13

Bin 2597: 72 of cap free
Amount of items: 2
Items: 
Size: 675947 Color: 19
Size: 323982 Color: 5

Bin 2598: 72 of cap free
Amount of items: 2
Items: 
Size: 768634 Color: 4
Size: 231295 Color: 2

Bin 2599: 72 of cap free
Amount of items: 2
Items: 
Size: 790407 Color: 18
Size: 209522 Color: 8

Bin 2600: 72 of cap free
Amount of items: 2
Items: 
Size: 672082 Color: 5
Size: 327847 Color: 15

Bin 2601: 72 of cap free
Amount of items: 2
Items: 
Size: 790031 Color: 19
Size: 209898 Color: 7

Bin 2602: 72 of cap free
Amount of items: 2
Items: 
Size: 503159 Color: 7
Size: 496770 Color: 4

Bin 2603: 72 of cap free
Amount of items: 2
Items: 
Size: 557556 Color: 17
Size: 442373 Color: 1

Bin 2604: 72 of cap free
Amount of items: 2
Items: 
Size: 557942 Color: 1
Size: 441987 Color: 12

Bin 2605: 72 of cap free
Amount of items: 2
Items: 
Size: 564920 Color: 4
Size: 435009 Color: 19

Bin 2606: 72 of cap free
Amount of items: 2
Items: 
Size: 686904 Color: 10
Size: 313025 Color: 9

Bin 2607: 72 of cap free
Amount of items: 2
Items: 
Size: 705289 Color: 6
Size: 294640 Color: 4

Bin 2608: 72 of cap free
Amount of items: 2
Items: 
Size: 745196 Color: 14
Size: 254733 Color: 8

Bin 2609: 73 of cap free
Amount of items: 3
Items: 
Size: 708668 Color: 11
Size: 167390 Color: 9
Size: 123870 Color: 1

Bin 2610: 73 of cap free
Amount of items: 2
Items: 
Size: 693403 Color: 11
Size: 306525 Color: 4

Bin 2611: 73 of cap free
Amount of items: 2
Items: 
Size: 740792 Color: 17
Size: 259136 Color: 19

Bin 2612: 73 of cap free
Amount of items: 2
Items: 
Size: 702003 Color: 2
Size: 297925 Color: 9

Bin 2613: 73 of cap free
Amount of items: 2
Items: 
Size: 635778 Color: 8
Size: 364150 Color: 12

Bin 2614: 73 of cap free
Amount of items: 2
Items: 
Size: 524860 Color: 4
Size: 475068 Color: 9

Bin 2615: 73 of cap free
Amount of items: 2
Items: 
Size: 510036 Color: 17
Size: 489892 Color: 10

Bin 2616: 73 of cap free
Amount of items: 2
Items: 
Size: 731973 Color: 12
Size: 267955 Color: 1

Bin 2617: 73 of cap free
Amount of items: 2
Items: 
Size: 699054 Color: 19
Size: 300874 Color: 10

Bin 2618: 73 of cap free
Amount of items: 2
Items: 
Size: 531024 Color: 15
Size: 468904 Color: 6

Bin 2619: 73 of cap free
Amount of items: 2
Items: 
Size: 573582 Color: 11
Size: 426346 Color: 15

Bin 2620: 73 of cap free
Amount of items: 2
Items: 
Size: 595786 Color: 2
Size: 404142 Color: 9

Bin 2621: 73 of cap free
Amount of items: 2
Items: 
Size: 621954 Color: 11
Size: 377974 Color: 18

Bin 2622: 73 of cap free
Amount of items: 2
Items: 
Size: 656654 Color: 2
Size: 343274 Color: 8

Bin 2623: 73 of cap free
Amount of items: 2
Items: 
Size: 656942 Color: 16
Size: 342986 Color: 17

Bin 2624: 73 of cap free
Amount of items: 2
Items: 
Size: 721588 Color: 15
Size: 278340 Color: 16

Bin 2625: 73 of cap free
Amount of items: 2
Items: 
Size: 754339 Color: 7
Size: 245589 Color: 1

Bin 2626: 74 of cap free
Amount of items: 2
Items: 
Size: 717794 Color: 3
Size: 282133 Color: 12

Bin 2627: 74 of cap free
Amount of items: 2
Items: 
Size: 753323 Color: 1
Size: 246604 Color: 12

Bin 2628: 74 of cap free
Amount of items: 2
Items: 
Size: 748759 Color: 19
Size: 251168 Color: 18

Bin 2629: 74 of cap free
Amount of items: 2
Items: 
Size: 649774 Color: 18
Size: 350153 Color: 17

Bin 2630: 74 of cap free
Amount of items: 2
Items: 
Size: 511763 Color: 16
Size: 488164 Color: 12

Bin 2631: 74 of cap free
Amount of items: 2
Items: 
Size: 556037 Color: 7
Size: 443890 Color: 2

Bin 2632: 74 of cap free
Amount of items: 2
Items: 
Size: 557814 Color: 12
Size: 442113 Color: 2

Bin 2633: 74 of cap free
Amount of items: 2
Items: 
Size: 566712 Color: 0
Size: 433215 Color: 6

Bin 2634: 74 of cap free
Amount of items: 2
Items: 
Size: 578480 Color: 0
Size: 421447 Color: 3

Bin 2635: 74 of cap free
Amount of items: 2
Items: 
Size: 587764 Color: 15
Size: 412163 Color: 18

Bin 2636: 74 of cap free
Amount of items: 2
Items: 
Size: 722814 Color: 6
Size: 277113 Color: 1

Bin 2637: 75 of cap free
Amount of items: 2
Items: 
Size: 692070 Color: 5
Size: 307856 Color: 6

Bin 2638: 75 of cap free
Amount of items: 2
Items: 
Size: 651302 Color: 7
Size: 348624 Color: 12

Bin 2639: 75 of cap free
Amount of items: 2
Items: 
Size: 638328 Color: 17
Size: 361598 Color: 13

Bin 2640: 75 of cap free
Amount of items: 2
Items: 
Size: 625288 Color: 14
Size: 374638 Color: 0

Bin 2641: 75 of cap free
Amount of items: 3
Items: 
Size: 761990 Color: 17
Size: 118973 Color: 4
Size: 118963 Color: 2

Bin 2642: 75 of cap free
Amount of items: 2
Items: 
Size: 703730 Color: 5
Size: 296196 Color: 13

Bin 2643: 75 of cap free
Amount of items: 2
Items: 
Size: 743113 Color: 2
Size: 256813 Color: 6

Bin 2644: 75 of cap free
Amount of items: 2
Items: 
Size: 720608 Color: 0
Size: 279318 Color: 4

Bin 2645: 75 of cap free
Amount of items: 2
Items: 
Size: 502761 Color: 0
Size: 497165 Color: 11

Bin 2646: 75 of cap free
Amount of items: 2
Items: 
Size: 559610 Color: 10
Size: 440316 Color: 8

Bin 2647: 75 of cap free
Amount of items: 2
Items: 
Size: 582732 Color: 15
Size: 417194 Color: 9

Bin 2648: 75 of cap free
Amount of items: 2
Items: 
Size: 676043 Color: 15
Size: 323883 Color: 5

Bin 2649: 75 of cap free
Amount of items: 2
Items: 
Size: 725301 Color: 9
Size: 274625 Color: 5

Bin 2650: 75 of cap free
Amount of items: 2
Items: 
Size: 743235 Color: 4
Size: 256691 Color: 19

Bin 2651: 75 of cap free
Amount of items: 2
Items: 
Size: 758047 Color: 16
Size: 241879 Color: 12

Bin 2652: 75 of cap free
Amount of items: 2
Items: 
Size: 781798 Color: 2
Size: 218128 Color: 13

Bin 2653: 76 of cap free
Amount of items: 3
Items: 
Size: 606972 Color: 11
Size: 196547 Color: 15
Size: 196406 Color: 18

Bin 2654: 76 of cap free
Amount of items: 2
Items: 
Size: 704326 Color: 7
Size: 295599 Color: 3

Bin 2655: 76 of cap free
Amount of items: 2
Items: 
Size: 550570 Color: 6
Size: 449355 Color: 9

Bin 2656: 76 of cap free
Amount of items: 2
Items: 
Size: 653239 Color: 18
Size: 346686 Color: 0

Bin 2657: 76 of cap free
Amount of items: 2
Items: 
Size: 527885 Color: 10
Size: 472040 Color: 18

Bin 2658: 76 of cap free
Amount of items: 2
Items: 
Size: 546755 Color: 12
Size: 453170 Color: 17

Bin 2659: 76 of cap free
Amount of items: 2
Items: 
Size: 627077 Color: 2
Size: 372848 Color: 17

Bin 2660: 76 of cap free
Amount of items: 2
Items: 
Size: 650708 Color: 12
Size: 349217 Color: 14

Bin 2661: 76 of cap free
Amount of items: 2
Items: 
Size: 652444 Color: 16
Size: 347481 Color: 4

Bin 2662: 76 of cap free
Amount of items: 2
Items: 
Size: 750454 Color: 3
Size: 249471 Color: 12

Bin 2663: 77 of cap free
Amount of items: 2
Items: 
Size: 678607 Color: 19
Size: 321317 Color: 12

Bin 2664: 77 of cap free
Amount of items: 2
Items: 
Size: 618013 Color: 0
Size: 381911 Color: 1

Bin 2665: 77 of cap free
Amount of items: 2
Items: 
Size: 715597 Color: 6
Size: 284327 Color: 15

Bin 2666: 77 of cap free
Amount of items: 2
Items: 
Size: 630340 Color: 19
Size: 369584 Color: 13

Bin 2667: 77 of cap free
Amount of items: 3
Items: 
Size: 384841 Color: 0
Size: 320799 Color: 7
Size: 294284 Color: 11

Bin 2668: 77 of cap free
Amount of items: 2
Items: 
Size: 592304 Color: 13
Size: 407620 Color: 1

Bin 2669: 77 of cap free
Amount of items: 2
Items: 
Size: 599487 Color: 11
Size: 400437 Color: 0

Bin 2670: 77 of cap free
Amount of items: 2
Items: 
Size: 619618 Color: 11
Size: 380306 Color: 15

Bin 2671: 78 of cap free
Amount of items: 2
Items: 
Size: 696824 Color: 12
Size: 303099 Color: 1

Bin 2672: 78 of cap free
Amount of items: 3
Items: 
Size: 419028 Color: 10
Size: 293267 Color: 0
Size: 287628 Color: 7

Bin 2673: 78 of cap free
Amount of items: 2
Items: 
Size: 725683 Color: 10
Size: 274240 Color: 8

Bin 2674: 78 of cap free
Amount of items: 2
Items: 
Size: 504394 Color: 4
Size: 495529 Color: 17

Bin 2675: 78 of cap free
Amount of items: 2
Items: 
Size: 519271 Color: 19
Size: 480652 Color: 13

Bin 2676: 78 of cap free
Amount of items: 2
Items: 
Size: 528586 Color: 16
Size: 471337 Color: 6

Bin 2677: 78 of cap free
Amount of items: 2
Items: 
Size: 540132 Color: 6
Size: 459791 Color: 0

Bin 2678: 78 of cap free
Amount of items: 2
Items: 
Size: 552519 Color: 9
Size: 447404 Color: 15

Bin 2679: 78 of cap free
Amount of items: 2
Items: 
Size: 608308 Color: 0
Size: 391615 Color: 5

Bin 2680: 78 of cap free
Amount of items: 2
Items: 
Size: 615899 Color: 15
Size: 384024 Color: 9

Bin 2681: 78 of cap free
Amount of items: 2
Items: 
Size: 795694 Color: 10
Size: 204229 Color: 2

Bin 2682: 78 of cap free
Amount of items: 2
Items: 
Size: 798207 Color: 1
Size: 201716 Color: 5

Bin 2683: 79 of cap free
Amount of items: 3
Items: 
Size: 761919 Color: 1
Size: 128906 Color: 10
Size: 109097 Color: 17

Bin 2684: 79 of cap free
Amount of items: 3
Items: 
Size: 708906 Color: 7
Size: 146522 Color: 15
Size: 144494 Color: 19

Bin 2685: 79 of cap free
Amount of items: 2
Items: 
Size: 660369 Color: 17
Size: 339553 Color: 14

Bin 2686: 79 of cap free
Amount of items: 3
Items: 
Size: 663353 Color: 11
Size: 190974 Color: 1
Size: 145595 Color: 6

Bin 2687: 79 of cap free
Amount of items: 3
Items: 
Size: 751961 Color: 14
Size: 124359 Color: 11
Size: 123602 Color: 11

Bin 2688: 79 of cap free
Amount of items: 2
Items: 
Size: 670482 Color: 7
Size: 329440 Color: 9

Bin 2689: 79 of cap free
Amount of items: 2
Items: 
Size: 514716 Color: 0
Size: 485206 Color: 15

Bin 2690: 79 of cap free
Amount of items: 2
Items: 
Size: 528207 Color: 1
Size: 471715 Color: 5

Bin 2691: 79 of cap free
Amount of items: 2
Items: 
Size: 530999 Color: 9
Size: 468923 Color: 18

Bin 2692: 79 of cap free
Amount of items: 2
Items: 
Size: 535933 Color: 4
Size: 463989 Color: 8

Bin 2693: 79 of cap free
Amount of items: 2
Items: 
Size: 572490 Color: 12
Size: 427432 Color: 15

Bin 2694: 79 of cap free
Amount of items: 2
Items: 
Size: 572625 Color: 6
Size: 427297 Color: 5

Bin 2695: 79 of cap free
Amount of items: 2
Items: 
Size: 579094 Color: 9
Size: 420828 Color: 8

Bin 2696: 79 of cap free
Amount of items: 2
Items: 
Size: 598189 Color: 4
Size: 401733 Color: 0

Bin 2697: 79 of cap free
Amount of items: 2
Items: 
Size: 601742 Color: 5
Size: 398180 Color: 9

Bin 2698: 80 of cap free
Amount of items: 3
Items: 
Size: 759015 Color: 13
Size: 128680 Color: 18
Size: 112226 Color: 8

Bin 2699: 80 of cap free
Amount of items: 2
Items: 
Size: 664143 Color: 19
Size: 335778 Color: 8

Bin 2700: 80 of cap free
Amount of items: 2
Items: 
Size: 795927 Color: 13
Size: 203994 Color: 10

Bin 2701: 80 of cap free
Amount of items: 3
Items: 
Size: 674259 Color: 3
Size: 163335 Color: 9
Size: 162327 Color: 1

Bin 2702: 80 of cap free
Amount of items: 2
Items: 
Size: 729490 Color: 10
Size: 270431 Color: 18

Bin 2703: 80 of cap free
Amount of items: 2
Items: 
Size: 655543 Color: 14
Size: 344378 Color: 4

Bin 2704: 80 of cap free
Amount of items: 2
Items: 
Size: 513717 Color: 8
Size: 486204 Color: 0

Bin 2705: 80 of cap free
Amount of items: 2
Items: 
Size: 514217 Color: 13
Size: 485704 Color: 3

Bin 2706: 80 of cap free
Amount of items: 2
Items: 
Size: 573729 Color: 12
Size: 426192 Color: 9

Bin 2707: 80 of cap free
Amount of items: 2
Items: 
Size: 653447 Color: 17
Size: 346474 Color: 7

Bin 2708: 80 of cap free
Amount of items: 2
Items: 
Size: 725874 Color: 15
Size: 274047 Color: 17

Bin 2709: 80 of cap free
Amount of items: 2
Items: 
Size: 782407 Color: 14
Size: 217514 Color: 16

Bin 2710: 81 of cap free
Amount of items: 2
Items: 
Size: 690125 Color: 16
Size: 309795 Color: 2

Bin 2711: 81 of cap free
Amount of items: 2
Items: 
Size: 534547 Color: 8
Size: 465373 Color: 1

Bin 2712: 81 of cap free
Amount of items: 2
Items: 
Size: 761085 Color: 10
Size: 238835 Color: 14

Bin 2713: 81 of cap free
Amount of items: 2
Items: 
Size: 737703 Color: 15
Size: 262217 Color: 1

Bin 2714: 81 of cap free
Amount of items: 2
Items: 
Size: 659985 Color: 6
Size: 339935 Color: 18

Bin 2715: 81 of cap free
Amount of items: 2
Items: 
Size: 701798 Color: 3
Size: 298122 Color: 0

Bin 2716: 81 of cap free
Amount of items: 2
Items: 
Size: 759272 Color: 14
Size: 240648 Color: 15

Bin 2717: 81 of cap free
Amount of items: 3
Items: 
Size: 441209 Color: 15
Size: 308028 Color: 8
Size: 250683 Color: 12

Bin 2718: 81 of cap free
Amount of items: 3
Items: 
Size: 411588 Color: 1
Size: 358456 Color: 17
Size: 229876 Color: 16

Bin 2719: 81 of cap free
Amount of items: 2
Items: 
Size: 617690 Color: 6
Size: 382230 Color: 10

Bin 2720: 81 of cap free
Amount of items: 2
Items: 
Size: 672736 Color: 14
Size: 327184 Color: 10

Bin 2721: 81 of cap free
Amount of items: 2
Items: 
Size: 781716 Color: 10
Size: 218204 Color: 15

Bin 2722: 81 of cap free
Amount of items: 2
Items: 
Size: 731319 Color: 3
Size: 268601 Color: 2

Bin 2723: 81 of cap free
Amount of items: 2
Items: 
Size: 515300 Color: 15
Size: 484620 Color: 18

Bin 2724: 81 of cap free
Amount of items: 2
Items: 
Size: 722544 Color: 6
Size: 277376 Color: 14

Bin 2725: 82 of cap free
Amount of items: 2
Items: 
Size: 695030 Color: 4
Size: 304889 Color: 11

Bin 2726: 82 of cap free
Amount of items: 2
Items: 
Size: 670382 Color: 3
Size: 329537 Color: 0

Bin 2727: 82 of cap free
Amount of items: 2
Items: 
Size: 603238 Color: 13
Size: 396681 Color: 10

Bin 2728: 82 of cap free
Amount of items: 2
Items: 
Size: 532267 Color: 5
Size: 467652 Color: 15

Bin 2729: 82 of cap free
Amount of items: 2
Items: 
Size: 630745 Color: 0
Size: 369174 Color: 11

Bin 2730: 83 of cap free
Amount of items: 3
Items: 
Size: 601053 Color: 6
Size: 199562 Color: 17
Size: 199303 Color: 7

Bin 2731: 83 of cap free
Amount of items: 2
Items: 
Size: 609001 Color: 16
Size: 390917 Color: 7

Bin 2732: 83 of cap free
Amount of items: 3
Items: 
Size: 490341 Color: 12
Size: 260253 Color: 16
Size: 249324 Color: 1

Bin 2733: 83 of cap free
Amount of items: 2
Items: 
Size: 568583 Color: 8
Size: 431335 Color: 17

Bin 2734: 83 of cap free
Amount of items: 2
Items: 
Size: 635567 Color: 2
Size: 364351 Color: 12

Bin 2735: 84 of cap free
Amount of items: 3
Items: 
Size: 651799 Color: 12
Size: 192485 Color: 2
Size: 155633 Color: 13

Bin 2736: 84 of cap free
Amount of items: 2
Items: 
Size: 737302 Color: 6
Size: 262615 Color: 18

Bin 2737: 84 of cap free
Amount of items: 2
Items: 
Size: 658789 Color: 2
Size: 341128 Color: 12

Bin 2738: 84 of cap free
Amount of items: 2
Items: 
Size: 667482 Color: 8
Size: 332435 Color: 17

Bin 2739: 84 of cap free
Amount of items: 2
Items: 
Size: 656010 Color: 8
Size: 343907 Color: 15

Bin 2740: 84 of cap free
Amount of items: 3
Items: 
Size: 523964 Color: 6
Size: 292397 Color: 11
Size: 183556 Color: 7

Bin 2741: 84 of cap free
Amount of items: 2
Items: 
Size: 510262 Color: 10
Size: 489655 Color: 12

Bin 2742: 84 of cap free
Amount of items: 2
Items: 
Size: 541679 Color: 14
Size: 458238 Color: 10

Bin 2743: 84 of cap free
Amount of items: 2
Items: 
Size: 588830 Color: 9
Size: 411087 Color: 17

Bin 2744: 84 of cap free
Amount of items: 2
Items: 
Size: 658505 Color: 19
Size: 341412 Color: 12

Bin 2745: 84 of cap free
Amount of items: 2
Items: 
Size: 696392 Color: 5
Size: 303525 Color: 7

Bin 2746: 84 of cap free
Amount of items: 2
Items: 
Size: 775124 Color: 6
Size: 224793 Color: 1

Bin 2747: 85 of cap free
Amount of items: 2
Items: 
Size: 701954 Color: 2
Size: 297962 Color: 6

Bin 2748: 85 of cap free
Amount of items: 2
Items: 
Size: 692877 Color: 5
Size: 307039 Color: 2

Bin 2749: 85 of cap free
Amount of items: 2
Items: 
Size: 623033 Color: 7
Size: 376883 Color: 1

Bin 2750: 85 of cap free
Amount of items: 3
Items: 
Size: 371384 Color: 17
Size: 320131 Color: 13
Size: 308401 Color: 5

Bin 2751: 85 of cap free
Amount of items: 2
Items: 
Size: 542131 Color: 15
Size: 457785 Color: 6

Bin 2752: 85 of cap free
Amount of items: 2
Items: 
Size: 543500 Color: 1
Size: 456416 Color: 13

Bin 2753: 85 of cap free
Amount of items: 2
Items: 
Size: 559283 Color: 15
Size: 440633 Color: 3

Bin 2754: 85 of cap free
Amount of items: 2
Items: 
Size: 574557 Color: 0
Size: 425359 Color: 8

Bin 2755: 85 of cap free
Amount of items: 2
Items: 
Size: 647859 Color: 1
Size: 352057 Color: 7

Bin 2756: 85 of cap free
Amount of items: 2
Items: 
Size: 693651 Color: 6
Size: 306265 Color: 5

Bin 2757: 85 of cap free
Amount of items: 2
Items: 
Size: 707397 Color: 14
Size: 292519 Color: 11

Bin 2758: 86 of cap free
Amount of items: 3
Items: 
Size: 519410 Color: 10
Size: 271921 Color: 6
Size: 208584 Color: 6

Bin 2759: 86 of cap free
Amount of items: 2
Items: 
Size: 637933 Color: 1
Size: 361982 Color: 5

Bin 2760: 86 of cap free
Amount of items: 2
Items: 
Size: 772956 Color: 12
Size: 226959 Color: 5

Bin 2761: 86 of cap free
Amount of items: 2
Items: 
Size: 533969 Color: 13
Size: 465946 Color: 11

Bin 2762: 86 of cap free
Amount of items: 2
Items: 
Size: 536685 Color: 9
Size: 463230 Color: 7

Bin 2763: 86 of cap free
Amount of items: 2
Items: 
Size: 600857 Color: 5
Size: 399058 Color: 17

Bin 2764: 86 of cap free
Amount of items: 2
Items: 
Size: 607115 Color: 7
Size: 392800 Color: 18

Bin 2765: 86 of cap free
Amount of items: 2
Items: 
Size: 616025 Color: 16
Size: 383890 Color: 1

Bin 2766: 86 of cap free
Amount of items: 2
Items: 
Size: 646328 Color: 3
Size: 353587 Color: 18

Bin 2767: 87 of cap free
Amount of items: 2
Items: 
Size: 660364 Color: 14
Size: 339550 Color: 15

Bin 2768: 87 of cap free
Amount of items: 2
Items: 
Size: 654749 Color: 9
Size: 345165 Color: 15

Bin 2769: 87 of cap free
Amount of items: 2
Items: 
Size: 542478 Color: 2
Size: 457436 Color: 4

Bin 2770: 87 of cap free
Amount of items: 2
Items: 
Size: 725074 Color: 13
Size: 274840 Color: 16

Bin 2771: 87 of cap free
Amount of items: 2
Items: 
Size: 741567 Color: 7
Size: 258347 Color: 3

Bin 2772: 87 of cap free
Amount of items: 2
Items: 
Size: 779273 Color: 7
Size: 220641 Color: 19

Bin 2773: 88 of cap free
Amount of items: 2
Items: 
Size: 656379 Color: 12
Size: 343534 Color: 0

Bin 2774: 88 of cap free
Amount of items: 2
Items: 
Size: 697041 Color: 11
Size: 302872 Color: 18

Bin 2775: 88 of cap free
Amount of items: 2
Items: 
Size: 677005 Color: 18
Size: 322908 Color: 14

Bin 2776: 88 of cap free
Amount of items: 2
Items: 
Size: 601741 Color: 9
Size: 398172 Color: 10

Bin 2777: 88 of cap free
Amount of items: 2
Items: 
Size: 645906 Color: 3
Size: 354007 Color: 15

Bin 2778: 88 of cap free
Amount of items: 2
Items: 
Size: 545194 Color: 17
Size: 454719 Color: 19

Bin 2779: 88 of cap free
Amount of items: 2
Items: 
Size: 573221 Color: 3
Size: 426692 Color: 9

Bin 2780: 88 of cap free
Amount of items: 2
Items: 
Size: 595908 Color: 5
Size: 404005 Color: 13

Bin 2781: 88 of cap free
Amount of items: 2
Items: 
Size: 607492 Color: 3
Size: 392421 Color: 7

Bin 2782: 88 of cap free
Amount of items: 2
Items: 
Size: 624385 Color: 2
Size: 375528 Color: 10

Bin 2783: 88 of cap free
Amount of items: 2
Items: 
Size: 704221 Color: 11
Size: 295692 Color: 8

Bin 2784: 88 of cap free
Amount of items: 2
Items: 
Size: 739772 Color: 15
Size: 260141 Color: 10

Bin 2785: 88 of cap free
Amount of items: 2
Items: 
Size: 793517 Color: 4
Size: 206396 Color: 15

Bin 2786: 89 of cap free
Amount of items: 3
Items: 
Size: 674121 Color: 4
Size: 176105 Color: 0
Size: 149686 Color: 17

Bin 2787: 89 of cap free
Amount of items: 3
Items: 
Size: 763235 Color: 13
Size: 121800 Color: 2
Size: 114877 Color: 10

Bin 2788: 89 of cap free
Amount of items: 2
Items: 
Size: 717700 Color: 0
Size: 282212 Color: 11

Bin 2789: 89 of cap free
Amount of items: 2
Items: 
Size: 746199 Color: 1
Size: 253713 Color: 13

Bin 2790: 89 of cap free
Amount of items: 2
Items: 
Size: 783576 Color: 19
Size: 216336 Color: 10

Bin 2791: 89 of cap free
Amount of items: 2
Items: 
Size: 616479 Color: 3
Size: 383433 Color: 8

Bin 2792: 89 of cap free
Amount of items: 3
Items: 
Size: 412489 Color: 4
Size: 293844 Color: 6
Size: 293579 Color: 2

Bin 2793: 89 of cap free
Amount of items: 2
Items: 
Size: 567371 Color: 11
Size: 432541 Color: 15

Bin 2794: 89 of cap free
Amount of items: 2
Items: 
Size: 586079 Color: 18
Size: 413833 Color: 15

Bin 2795: 89 of cap free
Amount of items: 2
Items: 
Size: 594171 Color: 3
Size: 405741 Color: 6

Bin 2796: 89 of cap free
Amount of items: 2
Items: 
Size: 659013 Color: 15
Size: 340899 Color: 14

Bin 2797: 90 of cap free
Amount of items: 3
Items: 
Size: 648861 Color: 11
Size: 175879 Color: 15
Size: 175171 Color: 9

Bin 2798: 90 of cap free
Amount of items: 2
Items: 
Size: 690495 Color: 10
Size: 309416 Color: 16

Bin 2799: 90 of cap free
Amount of items: 2
Items: 
Size: 750180 Color: 16
Size: 249731 Color: 0

Bin 2800: 90 of cap free
Amount of items: 2
Items: 
Size: 616211 Color: 7
Size: 383700 Color: 4

Bin 2801: 90 of cap free
Amount of items: 3
Items: 
Size: 623117 Color: 3
Size: 188505 Color: 19
Size: 188289 Color: 13

Bin 2802: 90 of cap free
Amount of items: 2
Items: 
Size: 637144 Color: 11
Size: 362767 Color: 6

Bin 2803: 90 of cap free
Amount of items: 2
Items: 
Size: 677867 Color: 8
Size: 322044 Color: 0

Bin 2804: 90 of cap free
Amount of items: 2
Items: 
Size: 519501 Color: 17
Size: 480410 Color: 19

Bin 2805: 90 of cap free
Amount of items: 2
Items: 
Size: 570213 Color: 14
Size: 429698 Color: 4

Bin 2806: 90 of cap free
Amount of items: 2
Items: 
Size: 624169 Color: 0
Size: 375742 Color: 19

Bin 2807: 90 of cap free
Amount of items: 2
Items: 
Size: 632057 Color: 3
Size: 367854 Color: 14

Bin 2808: 90 of cap free
Amount of items: 2
Items: 
Size: 632763 Color: 13
Size: 367148 Color: 18

Bin 2809: 90 of cap free
Amount of items: 2
Items: 
Size: 641255 Color: 15
Size: 358656 Color: 8

Bin 2810: 90 of cap free
Amount of items: 2
Items: 
Size: 644278 Color: 7
Size: 355633 Color: 17

Bin 2811: 90 of cap free
Amount of items: 2
Items: 
Size: 696940 Color: 1
Size: 302971 Color: 3

Bin 2812: 90 of cap free
Amount of items: 2
Items: 
Size: 719646 Color: 0
Size: 280265 Color: 9

Bin 2813: 90 of cap free
Amount of items: 2
Items: 
Size: 799127 Color: 1
Size: 200784 Color: 15

Bin 2814: 91 of cap free
Amount of items: 2
Items: 
Size: 652612 Color: 18
Size: 347298 Color: 8

Bin 2815: 91 of cap free
Amount of items: 2
Items: 
Size: 524860 Color: 4
Size: 475050 Color: 5

Bin 2816: 91 of cap free
Amount of items: 2
Items: 
Size: 602055 Color: 17
Size: 397855 Color: 2

Bin 2817: 91 of cap free
Amount of items: 2
Items: 
Size: 588026 Color: 1
Size: 411884 Color: 16

Bin 2818: 91 of cap free
Amount of items: 2
Items: 
Size: 501365 Color: 9
Size: 498545 Color: 4

Bin 2819: 91 of cap free
Amount of items: 2
Items: 
Size: 530554 Color: 14
Size: 469356 Color: 1

Bin 2820: 91 of cap free
Amount of items: 2
Items: 
Size: 539733 Color: 6
Size: 460177 Color: 14

Bin 2821: 91 of cap free
Amount of items: 2
Items: 
Size: 574688 Color: 7
Size: 425222 Color: 5

Bin 2822: 91 of cap free
Amount of items: 2
Items: 
Size: 633661 Color: 14
Size: 366249 Color: 8

Bin 2823: 91 of cap free
Amount of items: 2
Items: 
Size: 657815 Color: 16
Size: 342095 Color: 11

Bin 2824: 91 of cap free
Amount of items: 2
Items: 
Size: 678009 Color: 8
Size: 321901 Color: 9

Bin 2825: 91 of cap free
Amount of items: 2
Items: 
Size: 708352 Color: 11
Size: 291558 Color: 5

Bin 2826: 91 of cap free
Amount of items: 2
Items: 
Size: 788537 Color: 16
Size: 211373 Color: 19

Bin 2827: 92 of cap free
Amount of items: 3
Items: 
Size: 769224 Color: 6
Size: 115535 Color: 11
Size: 115150 Color: 1

Bin 2828: 92 of cap free
Amount of items: 2
Items: 
Size: 718596 Color: 12
Size: 281313 Color: 19

Bin 2829: 92 of cap free
Amount of items: 2
Items: 
Size: 628975 Color: 12
Size: 370934 Color: 5

Bin 2830: 92 of cap free
Amount of items: 3
Items: 
Size: 713155 Color: 13
Size: 164182 Color: 13
Size: 122572 Color: 2

Bin 2831: 92 of cap free
Amount of items: 2
Items: 
Size: 767146 Color: 10
Size: 232763 Color: 2

Bin 2832: 92 of cap free
Amount of items: 2
Items: 
Size: 513103 Color: 10
Size: 486806 Color: 9

Bin 2833: 92 of cap free
Amount of items: 2
Items: 
Size: 601908 Color: 19
Size: 398001 Color: 6

Bin 2834: 92 of cap free
Amount of items: 2
Items: 
Size: 573691 Color: 19
Size: 426218 Color: 12

Bin 2835: 92 of cap free
Amount of items: 2
Items: 
Size: 585375 Color: 11
Size: 414534 Color: 10

Bin 2836: 92 of cap free
Amount of items: 2
Items: 
Size: 589963 Color: 15
Size: 409946 Color: 0

Bin 2837: 92 of cap free
Amount of items: 2
Items: 
Size: 636265 Color: 16
Size: 363644 Color: 14

Bin 2838: 92 of cap free
Amount of items: 2
Items: 
Size: 714805 Color: 6
Size: 285104 Color: 19

Bin 2839: 92 of cap free
Amount of items: 2
Items: 
Size: 718872 Color: 17
Size: 281037 Color: 4

Bin 2840: 92 of cap free
Amount of items: 2
Items: 
Size: 738461 Color: 1
Size: 261448 Color: 17

Bin 2841: 93 of cap free
Amount of items: 3
Items: 
Size: 677434 Color: 13
Size: 161413 Color: 7
Size: 161061 Color: 9

Bin 2842: 93 of cap free
Amount of items: 2
Items: 
Size: 506800 Color: 2
Size: 493108 Color: 9

Bin 2843: 93 of cap free
Amount of items: 2
Items: 
Size: 666557 Color: 11
Size: 333351 Color: 12

Bin 2844: 93 of cap free
Amount of items: 2
Items: 
Size: 731963 Color: 11
Size: 267945 Color: 3

Bin 2845: 93 of cap free
Amount of items: 2
Items: 
Size: 774732 Color: 7
Size: 225176 Color: 4

Bin 2846: 93 of cap free
Amount of items: 2
Items: 
Size: 508369 Color: 18
Size: 491539 Color: 0

Bin 2847: 93 of cap free
Amount of items: 2
Items: 
Size: 552773 Color: 7
Size: 447135 Color: 15

Bin 2848: 93 of cap free
Amount of items: 2
Items: 
Size: 580137 Color: 2
Size: 419771 Color: 4

Bin 2849: 93 of cap free
Amount of items: 2
Items: 
Size: 581093 Color: 5
Size: 418815 Color: 18

Bin 2850: 93 of cap free
Amount of items: 2
Items: 
Size: 681115 Color: 16
Size: 318793 Color: 10

Bin 2851: 94 of cap free
Amount of items: 3
Items: 
Size: 643507 Color: 18
Size: 188905 Color: 0
Size: 167495 Color: 14

Bin 2852: 94 of cap free
Amount of items: 2
Items: 
Size: 529718 Color: 4
Size: 470189 Color: 17

Bin 2853: 94 of cap free
Amount of items: 2
Items: 
Size: 668120 Color: 8
Size: 331787 Color: 14

Bin 2854: 94 of cap free
Amount of items: 2
Items: 
Size: 699730 Color: 6
Size: 300177 Color: 19

Bin 2855: 95 of cap free
Amount of items: 2
Items: 
Size: 551143 Color: 0
Size: 448763 Color: 6

Bin 2856: 95 of cap free
Amount of items: 3
Items: 
Size: 694110 Color: 0
Size: 153164 Color: 7
Size: 152632 Color: 7

Bin 2857: 95 of cap free
Amount of items: 2
Items: 
Size: 740780 Color: 19
Size: 259126 Color: 9

Bin 2858: 95 of cap free
Amount of items: 2
Items: 
Size: 711670 Color: 4
Size: 288236 Color: 11

Bin 2859: 95 of cap free
Amount of items: 2
Items: 
Size: 723219 Color: 16
Size: 276687 Color: 4

Bin 2860: 95 of cap free
Amount of items: 2
Items: 
Size: 564906 Color: 18
Size: 435000 Color: 19

Bin 2861: 95 of cap free
Amount of items: 2
Items: 
Size: 702479 Color: 11
Size: 297427 Color: 1

Bin 2862: 95 of cap free
Amount of items: 2
Items: 
Size: 754706 Color: 2
Size: 245200 Color: 5

Bin 2863: 96 of cap free
Amount of items: 2
Items: 
Size: 768409 Color: 17
Size: 231496 Color: 10

Bin 2864: 96 of cap free
Amount of items: 2
Items: 
Size: 729888 Color: 17
Size: 270017 Color: 8

Bin 2865: 96 of cap free
Amount of items: 2
Items: 
Size: 531972 Color: 8
Size: 467933 Color: 6

Bin 2866: 96 of cap free
Amount of items: 2
Items: 
Size: 778500 Color: 4
Size: 221405 Color: 9

Bin 2867: 96 of cap free
Amount of items: 3
Items: 
Size: 686511 Color: 17
Size: 168607 Color: 1
Size: 144787 Color: 6

Bin 2868: 96 of cap free
Amount of items: 2
Items: 
Size: 789202 Color: 7
Size: 210703 Color: 9

Bin 2869: 96 of cap free
Amount of items: 2
Items: 
Size: 505914 Color: 8
Size: 493991 Color: 1

Bin 2870: 96 of cap free
Amount of items: 2
Items: 
Size: 541218 Color: 0
Size: 458687 Color: 19

Bin 2871: 96 of cap free
Amount of items: 2
Items: 
Size: 547672 Color: 18
Size: 452233 Color: 16

Bin 2872: 96 of cap free
Amount of items: 2
Items: 
Size: 565803 Color: 9
Size: 434102 Color: 6

Bin 2873: 96 of cap free
Amount of items: 2
Items: 
Size: 611816 Color: 0
Size: 388089 Color: 2

Bin 2874: 96 of cap free
Amount of items: 2
Items: 
Size: 659008 Color: 5
Size: 340897 Color: 12

Bin 2875: 96 of cap free
Amount of items: 2
Items: 
Size: 738356 Color: 18
Size: 261549 Color: 10

Bin 2876: 97 of cap free
Amount of items: 3
Items: 
Size: 701770 Color: 4
Size: 168714 Color: 19
Size: 129420 Color: 15

Bin 2877: 97 of cap free
Amount of items: 2
Items: 
Size: 692519 Color: 12
Size: 307385 Color: 19

Bin 2878: 97 of cap free
Amount of items: 2
Items: 
Size: 704590 Color: 15
Size: 295314 Color: 4

Bin 2879: 97 of cap free
Amount of items: 2
Items: 
Size: 511353 Color: 15
Size: 488551 Color: 9

Bin 2880: 97 of cap free
Amount of items: 2
Items: 
Size: 592900 Color: 7
Size: 407004 Color: 6

Bin 2881: 97 of cap free
Amount of items: 2
Items: 
Size: 510530 Color: 4
Size: 489374 Color: 0

Bin 2882: 97 of cap free
Amount of items: 2
Items: 
Size: 540936 Color: 10
Size: 458968 Color: 7

Bin 2883: 97 of cap free
Amount of items: 2
Items: 
Size: 579696 Color: 3
Size: 420208 Color: 15

Bin 2884: 97 of cap free
Amount of items: 2
Items: 
Size: 668465 Color: 1
Size: 331439 Color: 16

Bin 2885: 98 of cap free
Amount of items: 3
Items: 
Size: 689126 Color: 13
Size: 190464 Color: 1
Size: 120313 Color: 17

Bin 2886: 98 of cap free
Amount of items: 3
Items: 
Size: 731688 Color: 14
Size: 134544 Color: 18
Size: 133671 Color: 16

Bin 2887: 98 of cap free
Amount of items: 2
Items: 
Size: 649530 Color: 4
Size: 350373 Color: 3

Bin 2888: 98 of cap free
Amount of items: 2
Items: 
Size: 622787 Color: 4
Size: 377116 Color: 9

Bin 2889: 98 of cap free
Amount of items: 2
Items: 
Size: 799622 Color: 7
Size: 200281 Color: 3

Bin 2890: 98 of cap free
Amount of items: 2
Items: 
Size: 750057 Color: 2
Size: 249846 Color: 18

Bin 2891: 98 of cap free
Amount of items: 2
Items: 
Size: 617685 Color: 5
Size: 382218 Color: 19

Bin 2892: 98 of cap free
Amount of items: 2
Items: 
Size: 727818 Color: 9
Size: 272085 Color: 2

Bin 2893: 98 of cap free
Amount of items: 2
Items: 
Size: 740603 Color: 16
Size: 259300 Color: 4

Bin 2894: 98 of cap free
Amount of items: 2
Items: 
Size: 527139 Color: 16
Size: 472764 Color: 4

Bin 2895: 98 of cap free
Amount of items: 2
Items: 
Size: 583032 Color: 19
Size: 416871 Color: 5

Bin 2896: 98 of cap free
Amount of items: 2
Items: 
Size: 586098 Color: 15
Size: 413805 Color: 19

Bin 2897: 98 of cap free
Amount of items: 2
Items: 
Size: 599958 Color: 16
Size: 399945 Color: 18

Bin 2898: 98 of cap free
Amount of items: 2
Items: 
Size: 655052 Color: 9
Size: 344851 Color: 17

Bin 2899: 98 of cap free
Amount of items: 2
Items: 
Size: 669723 Color: 13
Size: 330180 Color: 9

Bin 2900: 98 of cap free
Amount of items: 2
Items: 
Size: 784951 Color: 4
Size: 214952 Color: 17

Bin 2901: 99 of cap free
Amount of items: 2
Items: 
Size: 647619 Color: 8
Size: 352283 Color: 0

Bin 2902: 99 of cap free
Amount of items: 2
Items: 
Size: 686434 Color: 17
Size: 313468 Color: 12

Bin 2903: 99 of cap free
Amount of items: 2
Items: 
Size: 524855 Color: 19
Size: 475047 Color: 1

Bin 2904: 99 of cap free
Amount of items: 2
Items: 
Size: 620515 Color: 4
Size: 379387 Color: 5

Bin 2905: 99 of cap free
Amount of items: 2
Items: 
Size: 515173 Color: 16
Size: 484729 Color: 14

Bin 2906: 99 of cap free
Amount of items: 2
Items: 
Size: 503347 Color: 0
Size: 496555 Color: 10

Bin 2907: 99 of cap free
Amount of items: 2
Items: 
Size: 533749 Color: 15
Size: 466153 Color: 3

Bin 2908: 99 of cap free
Amount of items: 2
Items: 
Size: 543872 Color: 5
Size: 456030 Color: 11

Bin 2909: 99 of cap free
Amount of items: 2
Items: 
Size: 579835 Color: 2
Size: 420067 Color: 17

Bin 2910: 100 of cap free
Amount of items: 2
Items: 
Size: 657147 Color: 17
Size: 342754 Color: 0

Bin 2911: 100 of cap free
Amount of items: 2
Items: 
Size: 725545 Color: 11
Size: 274356 Color: 14

Bin 2912: 100 of cap free
Amount of items: 2
Items: 
Size: 662395 Color: 9
Size: 337506 Color: 2

Bin 2913: 100 of cap free
Amount of items: 2
Items: 
Size: 632601 Color: 17
Size: 367300 Color: 5

Bin 2914: 100 of cap free
Amount of items: 2
Items: 
Size: 794112 Color: 7
Size: 205789 Color: 3

Bin 2915: 100 of cap free
Amount of items: 2
Items: 
Size: 794549 Color: 0
Size: 205352 Color: 11

Bin 2916: 100 of cap free
Amount of items: 2
Items: 
Size: 522955 Color: 12
Size: 476946 Color: 9

Bin 2917: 100 of cap free
Amount of items: 2
Items: 
Size: 537976 Color: 11
Size: 461925 Color: 13

Bin 2918: 100 of cap free
Amount of items: 2
Items: 
Size: 559974 Color: 9
Size: 439927 Color: 18

Bin 2919: 100 of cap free
Amount of items: 2
Items: 
Size: 564660 Color: 19
Size: 435241 Color: 12

Bin 2920: 100 of cap free
Amount of items: 2
Items: 
Size: 576524 Color: 6
Size: 423377 Color: 8

Bin 2921: 100 of cap free
Amount of items: 2
Items: 
Size: 591988 Color: 19
Size: 407913 Color: 9

Bin 2922: 100 of cap free
Amount of items: 2
Items: 
Size: 736756 Color: 16
Size: 263145 Color: 3

Bin 2923: 101 of cap free
Amount of items: 3
Items: 
Size: 608973 Color: 12
Size: 195469 Color: 19
Size: 195458 Color: 16

Bin 2924: 101 of cap free
Amount of items: 2
Items: 
Size: 696557 Color: 11
Size: 303343 Color: 5

Bin 2925: 101 of cap free
Amount of items: 2
Items: 
Size: 503493 Color: 3
Size: 496407 Color: 17

Bin 2926: 101 of cap free
Amount of items: 2
Items: 
Size: 544209 Color: 13
Size: 455691 Color: 3

Bin 2927: 101 of cap free
Amount of items: 2
Items: 
Size: 584867 Color: 18
Size: 415033 Color: 2

Bin 2928: 101 of cap free
Amount of items: 2
Items: 
Size: 598531 Color: 15
Size: 401369 Color: 5

Bin 2929: 101 of cap free
Amount of items: 2
Items: 
Size: 732861 Color: 18
Size: 267039 Color: 13

Bin 2930: 102 of cap free
Amount of items: 2
Items: 
Size: 605565 Color: 13
Size: 394334 Color: 8

Bin 2931: 102 of cap free
Amount of items: 2
Items: 
Size: 563564 Color: 16
Size: 436335 Color: 1

Bin 2932: 102 of cap free
Amount of items: 2
Items: 
Size: 686891 Color: 16
Size: 313008 Color: 8

Bin 2933: 102 of cap free
Amount of items: 2
Items: 
Size: 696249 Color: 12
Size: 303650 Color: 8

Bin 2934: 103 of cap free
Amount of items: 2
Items: 
Size: 582236 Color: 12
Size: 417662 Color: 19

Bin 2935: 103 of cap free
Amount of items: 2
Items: 
Size: 669193 Color: 18
Size: 330705 Color: 3

Bin 2936: 103 of cap free
Amount of items: 2
Items: 
Size: 638127 Color: 9
Size: 361771 Color: 0

Bin 2937: 103 of cap free
Amount of items: 2
Items: 
Size: 779527 Color: 15
Size: 220371 Color: 11

Bin 2938: 104 of cap free
Amount of items: 2
Items: 
Size: 633962 Color: 18
Size: 365935 Color: 1

Bin 2939: 104 of cap free
Amount of items: 2
Items: 
Size: 634210 Color: 15
Size: 365687 Color: 6

Bin 2940: 104 of cap free
Amount of items: 2
Items: 
Size: 772150 Color: 12
Size: 227747 Color: 8

Bin 2941: 104 of cap free
Amount of items: 2
Items: 
Size: 559772 Color: 2
Size: 440125 Color: 10

Bin 2942: 104 of cap free
Amount of items: 2
Items: 
Size: 770541 Color: 4
Size: 229356 Color: 5

Bin 2943: 104 of cap free
Amount of items: 2
Items: 
Size: 639188 Color: 6
Size: 360709 Color: 10

Bin 2944: 104 of cap free
Amount of items: 2
Items: 
Size: 678701 Color: 4
Size: 321196 Color: 6

Bin 2945: 104 of cap free
Amount of items: 2
Items: 
Size: 512440 Color: 13
Size: 487457 Color: 19

Bin 2946: 104 of cap free
Amount of items: 2
Items: 
Size: 577343 Color: 10
Size: 422554 Color: 4

Bin 2947: 104 of cap free
Amount of items: 2
Items: 
Size: 600283 Color: 13
Size: 399614 Color: 0

Bin 2948: 104 of cap free
Amount of items: 2
Items: 
Size: 686593 Color: 13
Size: 313304 Color: 14

Bin 2949: 104 of cap free
Amount of items: 2
Items: 
Size: 742663 Color: 8
Size: 257234 Color: 15

Bin 2950: 104 of cap free
Amount of items: 2
Items: 
Size: 793378 Color: 15
Size: 206519 Color: 11

Bin 2951: 105 of cap free
Amount of items: 2
Items: 
Size: 722363 Color: 4
Size: 277533 Color: 9

Bin 2952: 105 of cap free
Amount of items: 3
Items: 
Size: 619442 Color: 14
Size: 190293 Color: 19
Size: 190161 Color: 19

Bin 2953: 105 of cap free
Amount of items: 2
Items: 
Size: 764915 Color: 13
Size: 234981 Color: 16

Bin 2954: 105 of cap free
Amount of items: 2
Items: 
Size: 519885 Color: 11
Size: 480011 Color: 9

Bin 2955: 105 of cap free
Amount of items: 2
Items: 
Size: 539875 Color: 1
Size: 460021 Color: 16

Bin 2956: 105 of cap free
Amount of items: 2
Items: 
Size: 558190 Color: 10
Size: 441706 Color: 12

Bin 2957: 105 of cap free
Amount of items: 2
Items: 
Size: 562143 Color: 6
Size: 437753 Color: 11

Bin 2958: 105 of cap free
Amount of items: 2
Items: 
Size: 573204 Color: 19
Size: 426692 Color: 3

Bin 2959: 105 of cap free
Amount of items: 2
Items: 
Size: 584148 Color: 12
Size: 415748 Color: 13

Bin 2960: 105 of cap free
Amount of items: 2
Items: 
Size: 608527 Color: 13
Size: 391369 Color: 19

Bin 2961: 105 of cap free
Amount of items: 2
Items: 
Size: 642156 Color: 5
Size: 357740 Color: 15

Bin 2962: 105 of cap free
Amount of items: 2
Items: 
Size: 718863 Color: 11
Size: 281033 Color: 17

Bin 2963: 106 of cap free
Amount of items: 2
Items: 
Size: 615375 Color: 4
Size: 384520 Color: 15

Bin 2964: 106 of cap free
Amount of items: 2
Items: 
Size: 691306 Color: 4
Size: 308589 Color: 1

Bin 2965: 106 of cap free
Amount of items: 2
Items: 
Size: 550795 Color: 3
Size: 449100 Color: 0

Bin 2966: 106 of cap free
Amount of items: 2
Items: 
Size: 633430 Color: 2
Size: 366465 Color: 9

Bin 2967: 106 of cap free
Amount of items: 2
Items: 
Size: 618765 Color: 19
Size: 381130 Color: 18

Bin 2968: 106 of cap free
Amount of items: 2
Items: 
Size: 524979 Color: 7
Size: 474916 Color: 0

Bin 2969: 106 of cap free
Amount of items: 2
Items: 
Size: 568565 Color: 8
Size: 431330 Color: 10

Bin 2970: 106 of cap free
Amount of items: 2
Items: 
Size: 637138 Color: 19
Size: 362757 Color: 3

Bin 2971: 107 of cap free
Amount of items: 2
Items: 
Size: 667479 Color: 0
Size: 332415 Color: 7

Bin 2972: 107 of cap free
Amount of items: 3
Items: 
Size: 700913 Color: 19
Size: 149503 Color: 1
Size: 149478 Color: 19

Bin 2973: 107 of cap free
Amount of items: 2
Items: 
Size: 716302 Color: 1
Size: 283592 Color: 12

Bin 2974: 107 of cap free
Amount of items: 2
Items: 
Size: 523996 Color: 2
Size: 475898 Color: 11

Bin 2975: 107 of cap free
Amount of items: 2
Items: 
Size: 545556 Color: 0
Size: 454338 Color: 16

Bin 2976: 107 of cap free
Amount of items: 2
Items: 
Size: 571157 Color: 14
Size: 428737 Color: 2

Bin 2977: 107 of cap free
Amount of items: 2
Items: 
Size: 578450 Color: 15
Size: 421444 Color: 9

Bin 2978: 107 of cap free
Amount of items: 2
Items: 
Size: 586688 Color: 16
Size: 413206 Color: 13

Bin 2979: 107 of cap free
Amount of items: 2
Items: 
Size: 631150 Color: 11
Size: 368744 Color: 3

Bin 2980: 108 of cap free
Amount of items: 3
Items: 
Size: 647457 Color: 11
Size: 187454 Color: 8
Size: 164982 Color: 4

Bin 2981: 108 of cap free
Amount of items: 3
Items: 
Size: 666294 Color: 12
Size: 167029 Color: 8
Size: 166570 Color: 7

Bin 2982: 108 of cap free
Amount of items: 3
Items: 
Size: 685467 Color: 14
Size: 171493 Color: 3
Size: 142933 Color: 5

Bin 2983: 108 of cap free
Amount of items: 2
Items: 
Size: 546910 Color: 15
Size: 452983 Color: 5

Bin 2984: 108 of cap free
Amount of items: 2
Items: 
Size: 782185 Color: 5
Size: 217708 Color: 8

Bin 2985: 108 of cap free
Amount of items: 2
Items: 
Size: 778151 Color: 13
Size: 221742 Color: 0

Bin 2986: 108 of cap free
Amount of items: 2
Items: 
Size: 717495 Color: 3
Size: 282398 Color: 19

Bin 2987: 108 of cap free
Amount of items: 2
Items: 
Size: 730886 Color: 3
Size: 269007 Color: 15

Bin 2988: 108 of cap free
Amount of items: 2
Items: 
Size: 515865 Color: 10
Size: 484028 Color: 15

Bin 2989: 108 of cap free
Amount of items: 2
Items: 
Size: 590926 Color: 17
Size: 408967 Color: 11

Bin 2990: 108 of cap free
Amount of items: 2
Items: 
Size: 591209 Color: 16
Size: 408684 Color: 6

Bin 2991: 108 of cap free
Amount of items: 2
Items: 
Size: 635553 Color: 18
Size: 364340 Color: 7

Bin 2992: 108 of cap free
Amount of items: 2
Items: 
Size: 716924 Color: 12
Size: 282969 Color: 10

Bin 2993: 108 of cap free
Amount of items: 2
Items: 
Size: 792272 Color: 11
Size: 207621 Color: 19

Bin 2994: 109 of cap free
Amount of items: 2
Items: 
Size: 735236 Color: 15
Size: 264656 Color: 12

Bin 2995: 109 of cap free
Amount of items: 2
Items: 
Size: 776126 Color: 6
Size: 223766 Color: 3

Bin 2996: 109 of cap free
Amount of items: 3
Items: 
Size: 729522 Color: 12
Size: 135836 Color: 11
Size: 134534 Color: 5

Bin 2997: 109 of cap free
Amount of items: 2
Items: 
Size: 651062 Color: 5
Size: 348830 Color: 15

Bin 2998: 109 of cap free
Amount of items: 2
Items: 
Size: 515399 Color: 0
Size: 484493 Color: 11

Bin 2999: 109 of cap free
Amount of items: 2
Items: 
Size: 650692 Color: 8
Size: 349200 Color: 6

Bin 3000: 109 of cap free
Amount of items: 3
Items: 
Size: 413425 Color: 9
Size: 304761 Color: 5
Size: 281706 Color: 13

Bin 3001: 109 of cap free
Amount of items: 2
Items: 
Size: 514333 Color: 10
Size: 485559 Color: 9

Bin 3002: 110 of cap free
Amount of items: 2
Items: 
Size: 657725 Color: 14
Size: 342166 Color: 16

Bin 3003: 110 of cap free
Amount of items: 2
Items: 
Size: 580582 Color: 1
Size: 419309 Color: 16

Bin 3004: 110 of cap free
Amount of items: 2
Items: 
Size: 578388 Color: 6
Size: 421503 Color: 2

Bin 3005: 110 of cap free
Amount of items: 2
Items: 
Size: 695515 Color: 0
Size: 304376 Color: 4

Bin 3006: 110 of cap free
Amount of items: 2
Items: 
Size: 696700 Color: 17
Size: 303191 Color: 3

Bin 3007: 111 of cap free
Amount of items: 3
Items: 
Size: 512856 Color: 13
Size: 311813 Color: 12
Size: 175221 Color: 4

Bin 3008: 111 of cap free
Amount of items: 2
Items: 
Size: 681708 Color: 12
Size: 318182 Color: 3

Bin 3009: 111 of cap free
Amount of items: 2
Items: 
Size: 625114 Color: 3
Size: 374776 Color: 14

Bin 3010: 111 of cap free
Amount of items: 2
Items: 
Size: 763242 Color: 2
Size: 236648 Color: 9

Bin 3011: 111 of cap free
Amount of items: 2
Items: 
Size: 768974 Color: 19
Size: 230916 Color: 5

Bin 3012: 111 of cap free
Amount of items: 2
Items: 
Size: 729495 Color: 11
Size: 270395 Color: 13

Bin 3013: 111 of cap free
Amount of items: 2
Items: 
Size: 707895 Color: 4
Size: 291995 Color: 13

Bin 3014: 111 of cap free
Amount of items: 2
Items: 
Size: 662037 Color: 11
Size: 337853 Color: 4

Bin 3015: 111 of cap free
Amount of items: 2
Items: 
Size: 517474 Color: 16
Size: 482416 Color: 3

Bin 3016: 111 of cap free
Amount of items: 2
Items: 
Size: 521867 Color: 15
Size: 478023 Color: 7

Bin 3017: 111 of cap free
Amount of items: 2
Items: 
Size: 523316 Color: 1
Size: 476574 Color: 9

Bin 3018: 111 of cap free
Amount of items: 2
Items: 
Size: 564179 Color: 15
Size: 435711 Color: 16

Bin 3019: 111 of cap free
Amount of items: 2
Items: 
Size: 568156 Color: 17
Size: 431734 Color: 19

Bin 3020: 111 of cap free
Amount of items: 2
Items: 
Size: 599485 Color: 2
Size: 400405 Color: 0

Bin 3021: 111 of cap free
Amount of items: 2
Items: 
Size: 652039 Color: 18
Size: 347851 Color: 12

Bin 3022: 111 of cap free
Amount of items: 2
Items: 
Size: 726421 Color: 14
Size: 273469 Color: 9

Bin 3023: 112 of cap free
Amount of items: 2
Items: 
Size: 767390 Color: 18
Size: 232499 Color: 9

Bin 3024: 112 of cap free
Amount of items: 2
Items: 
Size: 721756 Color: 8
Size: 278133 Color: 5

Bin 3025: 112 of cap free
Amount of items: 2
Items: 
Size: 719761 Color: 7
Size: 280128 Color: 2

Bin 3026: 112 of cap free
Amount of items: 2
Items: 
Size: 797843 Color: 10
Size: 202046 Color: 18

Bin 3027: 113 of cap free
Amount of items: 2
Items: 
Size: 785342 Color: 14
Size: 214546 Color: 3

Bin 3028: 113 of cap free
Amount of items: 2
Items: 
Size: 789827 Color: 3
Size: 210061 Color: 15

Bin 3029: 113 of cap free
Amount of items: 2
Items: 
Size: 509457 Color: 1
Size: 490431 Color: 17

Bin 3030: 113 of cap free
Amount of items: 2
Items: 
Size: 516769 Color: 8
Size: 483119 Color: 15

Bin 3031: 113 of cap free
Amount of items: 2
Items: 
Size: 553694 Color: 16
Size: 446194 Color: 10

Bin 3032: 113 of cap free
Amount of items: 2
Items: 
Size: 668802 Color: 18
Size: 331086 Color: 9

Bin 3033: 113 of cap free
Amount of items: 2
Items: 
Size: 724853 Color: 7
Size: 275035 Color: 1

Bin 3034: 114 of cap free
Amount of items: 3
Items: 
Size: 792925 Color: 10
Size: 106099 Color: 17
Size: 100863 Color: 14

Bin 3035: 114 of cap free
Amount of items: 2
Items: 
Size: 710751 Color: 11
Size: 289136 Color: 5

Bin 3036: 114 of cap free
Amount of items: 2
Items: 
Size: 575120 Color: 13
Size: 424767 Color: 18

Bin 3037: 114 of cap free
Amount of items: 2
Items: 
Size: 585915 Color: 18
Size: 413972 Color: 4

Bin 3038: 114 of cap free
Amount of items: 2
Items: 
Size: 798189 Color: 13
Size: 201698 Color: 16

Bin 3039: 115 of cap free
Amount of items: 2
Items: 
Size: 640923 Color: 2
Size: 358963 Color: 9

Bin 3040: 115 of cap free
Amount of items: 2
Items: 
Size: 615206 Color: 17
Size: 384680 Color: 6

Bin 3041: 115 of cap free
Amount of items: 2
Items: 
Size: 722993 Color: 4
Size: 276893 Color: 19

Bin 3042: 115 of cap free
Amount of items: 2
Items: 
Size: 762903 Color: 8
Size: 236983 Color: 10

Bin 3043: 115 of cap free
Amount of items: 2
Items: 
Size: 528510 Color: 7
Size: 471376 Color: 18

Bin 3044: 115 of cap free
Amount of items: 2
Items: 
Size: 540458 Color: 2
Size: 459428 Color: 6

Bin 3045: 115 of cap free
Amount of items: 2
Items: 
Size: 588458 Color: 4
Size: 411428 Color: 12

Bin 3046: 115 of cap free
Amount of items: 2
Items: 
Size: 593709 Color: 8
Size: 406177 Color: 14

Bin 3047: 115 of cap free
Amount of items: 2
Items: 
Size: 733733 Color: 6
Size: 266153 Color: 2

Bin 3048: 116 of cap free
Amount of items: 2
Items: 
Size: 623394 Color: 9
Size: 376491 Color: 10

Bin 3049: 116 of cap free
Amount of items: 2
Items: 
Size: 527345 Color: 3
Size: 472540 Color: 0

Bin 3050: 116 of cap free
Amount of items: 2
Items: 
Size: 640654 Color: 7
Size: 359231 Color: 5

Bin 3051: 116 of cap free
Amount of items: 3
Items: 
Size: 533109 Color: 4
Size: 234968 Color: 0
Size: 231808 Color: 15

Bin 3052: 116 of cap free
Amount of items: 2
Items: 
Size: 526771 Color: 13
Size: 473114 Color: 10

Bin 3053: 116 of cap free
Amount of items: 2
Items: 
Size: 535319 Color: 12
Size: 464566 Color: 18

Bin 3054: 116 of cap free
Amount of items: 2
Items: 
Size: 742870 Color: 7
Size: 257015 Color: 4

Bin 3055: 117 of cap free
Amount of items: 2
Items: 
Size: 686769 Color: 14
Size: 313115 Color: 3

Bin 3056: 117 of cap free
Amount of items: 3
Items: 
Size: 771269 Color: 11
Size: 116690 Color: 10
Size: 111925 Color: 9

Bin 3057: 117 of cap free
Amount of items: 2
Items: 
Size: 781572 Color: 18
Size: 218312 Color: 6

Bin 3058: 117 of cap free
Amount of items: 2
Items: 
Size: 724663 Color: 1
Size: 275221 Color: 17

Bin 3059: 117 of cap free
Amount of items: 2
Items: 
Size: 517311 Color: 19
Size: 482573 Color: 17

Bin 3060: 117 of cap free
Amount of items: 2
Items: 
Size: 621318 Color: 5
Size: 378566 Color: 16

Bin 3061: 117 of cap free
Amount of items: 2
Items: 
Size: 720788 Color: 2
Size: 279096 Color: 17

Bin 3062: 117 of cap free
Amount of items: 2
Items: 
Size: 792742 Color: 11
Size: 207142 Color: 8

Bin 3063: 118 of cap free
Amount of items: 2
Items: 
Size: 752673 Color: 7
Size: 247210 Color: 9

Bin 3064: 118 of cap free
Amount of items: 2
Items: 
Size: 790915 Color: 18
Size: 208968 Color: 8

Bin 3065: 118 of cap free
Amount of items: 2
Items: 
Size: 682260 Color: 19
Size: 317623 Color: 9

Bin 3066: 118 of cap free
Amount of items: 2
Items: 
Size: 726847 Color: 5
Size: 273036 Color: 19

Bin 3067: 118 of cap free
Amount of items: 2
Items: 
Size: 743332 Color: 3
Size: 256551 Color: 8

Bin 3068: 118 of cap free
Amount of items: 2
Items: 
Size: 525267 Color: 9
Size: 474616 Color: 19

Bin 3069: 118 of cap free
Amount of items: 2
Items: 
Size: 767534 Color: 2
Size: 232349 Color: 6

Bin 3070: 118 of cap free
Amount of items: 2
Items: 
Size: 506469 Color: 6
Size: 493414 Color: 17

Bin 3071: 118 of cap free
Amount of items: 2
Items: 
Size: 778663 Color: 7
Size: 221220 Color: 2

Bin 3072: 119 of cap free
Amount of items: 2
Items: 
Size: 693263 Color: 18
Size: 306619 Color: 6

Bin 3073: 119 of cap free
Amount of items: 2
Items: 
Size: 769187 Color: 6
Size: 230695 Color: 17

Bin 3074: 119 of cap free
Amount of items: 2
Items: 
Size: 591981 Color: 12
Size: 407901 Color: 13

Bin 3075: 119 of cap free
Amount of items: 2
Items: 
Size: 776252 Color: 19
Size: 223630 Color: 12

Bin 3076: 120 of cap free
Amount of items: 3
Items: 
Size: 386161 Color: 14
Size: 309676 Color: 14
Size: 304044 Color: 6

Bin 3077: 120 of cap free
Amount of items: 2
Items: 
Size: 796595 Color: 18
Size: 203286 Color: 9

Bin 3078: 120 of cap free
Amount of items: 2
Items: 
Size: 669828 Color: 10
Size: 330053 Color: 13

Bin 3079: 121 of cap free
Amount of items: 2
Items: 
Size: 644815 Color: 2
Size: 355065 Color: 14

Bin 3080: 121 of cap free
Amount of items: 2
Items: 
Size: 735038 Color: 13
Size: 264842 Color: 6

Bin 3081: 122 of cap free
Amount of items: 2
Items: 
Size: 507349 Color: 11
Size: 492530 Color: 12

Bin 3082: 122 of cap free
Amount of items: 2
Items: 
Size: 631625 Color: 0
Size: 368254 Color: 16

Bin 3083: 122 of cap free
Amount of items: 2
Items: 
Size: 554858 Color: 10
Size: 445021 Color: 13

Bin 3084: 122 of cap free
Amount of items: 2
Items: 
Size: 652423 Color: 14
Size: 347456 Color: 18

Bin 3085: 123 of cap free
Amount of items: 2
Items: 
Size: 655847 Color: 18
Size: 344031 Color: 16

Bin 3086: 123 of cap free
Amount of items: 2
Items: 
Size: 596128 Color: 7
Size: 403750 Color: 18

Bin 3087: 123 of cap free
Amount of items: 2
Items: 
Size: 755180 Color: 18
Size: 244698 Color: 1

Bin 3088: 123 of cap free
Amount of items: 2
Items: 
Size: 781408 Color: 6
Size: 218470 Color: 7

Bin 3089: 123 of cap free
Amount of items: 2
Items: 
Size: 731938 Color: 4
Size: 267940 Color: 13

Bin 3090: 123 of cap free
Amount of items: 2
Items: 
Size: 541074 Color: 14
Size: 458804 Color: 3

Bin 3091: 123 of cap free
Amount of items: 2
Items: 
Size: 560777 Color: 2
Size: 439101 Color: 16

Bin 3092: 123 of cap free
Amount of items: 2
Items: 
Size: 730549 Color: 10
Size: 269329 Color: 19

Bin 3093: 124 of cap free
Amount of items: 2
Items: 
Size: 691399 Color: 6
Size: 308478 Color: 0

Bin 3094: 124 of cap free
Amount of items: 2
Items: 
Size: 648151 Color: 9
Size: 351726 Color: 6

Bin 3095: 124 of cap free
Amount of items: 2
Items: 
Size: 598867 Color: 11
Size: 401010 Color: 7

Bin 3096: 124 of cap free
Amount of items: 2
Items: 
Size: 742078 Color: 11
Size: 257799 Color: 0

Bin 3097: 124 of cap free
Amount of items: 2
Items: 
Size: 530060 Color: 15
Size: 469817 Color: 17

Bin 3098: 125 of cap free
Amount of items: 2
Items: 
Size: 550920 Color: 0
Size: 448956 Color: 10

Bin 3099: 125 of cap free
Amount of items: 2
Items: 
Size: 624694 Color: 10
Size: 375182 Color: 16

Bin 3100: 125 of cap free
Amount of items: 2
Items: 
Size: 647438 Color: 5
Size: 352438 Color: 10

Bin 3101: 125 of cap free
Amount of items: 2
Items: 
Size: 723642 Color: 19
Size: 276234 Color: 13

Bin 3102: 125 of cap free
Amount of items: 2
Items: 
Size: 512281 Color: 6
Size: 487595 Color: 8

Bin 3103: 125 of cap free
Amount of items: 2
Items: 
Size: 511347 Color: 15
Size: 488529 Color: 7

Bin 3104: 125 of cap free
Amount of items: 2
Items: 
Size: 571906 Color: 17
Size: 427970 Color: 10

Bin 3105: 125 of cap free
Amount of items: 2
Items: 
Size: 576428 Color: 5
Size: 423448 Color: 6

Bin 3106: 125 of cap free
Amount of items: 2
Items: 
Size: 767919 Color: 15
Size: 231957 Color: 2

Bin 3107: 125 of cap free
Amount of items: 2
Items: 
Size: 798991 Color: 17
Size: 200885 Color: 3

Bin 3108: 126 of cap free
Amount of items: 3
Items: 
Size: 703541 Color: 10
Size: 155247 Color: 17
Size: 141087 Color: 16

Bin 3109: 126 of cap free
Amount of items: 2
Items: 
Size: 620344 Color: 9
Size: 379531 Color: 13

Bin 3110: 126 of cap free
Amount of items: 2
Items: 
Size: 612023 Color: 9
Size: 387852 Color: 17

Bin 3111: 126 of cap free
Amount of items: 2
Items: 
Size: 782366 Color: 6
Size: 217509 Color: 10

Bin 3112: 126 of cap free
Amount of items: 2
Items: 
Size: 564014 Color: 16
Size: 435861 Color: 13

Bin 3113: 127 of cap free
Amount of items: 2
Items: 
Size: 761445 Color: 7
Size: 238429 Color: 3

Bin 3114: 127 of cap free
Amount of items: 2
Items: 
Size: 638560 Color: 9
Size: 361314 Color: 6

Bin 3115: 127 of cap free
Amount of items: 2
Items: 
Size: 500719 Color: 9
Size: 499155 Color: 18

Bin 3116: 127 of cap free
Amount of items: 2
Items: 
Size: 671252 Color: 7
Size: 328622 Color: 12

Bin 3117: 128 of cap free
Amount of items: 2
Items: 
Size: 692703 Color: 7
Size: 307170 Color: 3

Bin 3118: 128 of cap free
Amount of items: 2
Items: 
Size: 742281 Color: 4
Size: 257592 Color: 3

Bin 3119: 128 of cap free
Amount of items: 2
Items: 
Size: 550982 Color: 1
Size: 448891 Color: 9

Bin 3120: 128 of cap free
Amount of items: 2
Items: 
Size: 654531 Color: 8
Size: 345342 Color: 1

Bin 3121: 128 of cap free
Amount of items: 2
Items: 
Size: 612431 Color: 8
Size: 387442 Color: 2

Bin 3122: 128 of cap free
Amount of items: 3
Items: 
Size: 417078 Color: 12
Size: 301764 Color: 0
Size: 281031 Color: 16

Bin 3123: 128 of cap free
Amount of items: 2
Items: 
Size: 578443 Color: 9
Size: 421430 Color: 7

Bin 3124: 128 of cap free
Amount of items: 2
Items: 
Size: 585370 Color: 1
Size: 414503 Color: 0

Bin 3125: 128 of cap free
Amount of items: 2
Items: 
Size: 693562 Color: 15
Size: 306311 Color: 13

Bin 3126: 129 of cap free
Amount of items: 3
Items: 
Size: 724222 Color: 0
Size: 152092 Color: 6
Size: 123558 Color: 4

Bin 3127: 129 of cap free
Amount of items: 2
Items: 
Size: 587032 Color: 18
Size: 412840 Color: 7

Bin 3128: 130 of cap free
Amount of items: 2
Items: 
Size: 712135 Color: 1
Size: 287736 Color: 9

Bin 3129: 130 of cap free
Amount of items: 2
Items: 
Size: 640121 Color: 17
Size: 359750 Color: 8

Bin 3130: 130 of cap free
Amount of items: 2
Items: 
Size: 539399 Color: 6
Size: 460472 Color: 14

Bin 3131: 130 of cap free
Amount of items: 2
Items: 
Size: 550771 Color: 2
Size: 449100 Color: 14

Bin 3132: 130 of cap free
Amount of items: 2
Items: 
Size: 554982 Color: 2
Size: 444889 Color: 6

Bin 3133: 131 of cap free
Amount of items: 2
Items: 
Size: 622566 Color: 17
Size: 377304 Color: 11

Bin 3134: 131 of cap free
Amount of items: 2
Items: 
Size: 709102 Color: 12
Size: 290768 Color: 15

Bin 3135: 131 of cap free
Amount of items: 2
Items: 
Size: 581746 Color: 17
Size: 418124 Color: 14

Bin 3136: 131 of cap free
Amount of items: 2
Items: 
Size: 587293 Color: 1
Size: 412577 Color: 7

Bin 3137: 131 of cap free
Amount of items: 2
Items: 
Size: 678003 Color: 4
Size: 321867 Color: 19

Bin 3138: 131 of cap free
Amount of items: 2
Items: 
Size: 793034 Color: 15
Size: 206836 Color: 0

Bin 3139: 132 of cap free
Amount of items: 2
Items: 
Size: 755174 Color: 15
Size: 244695 Color: 7

Bin 3140: 132 of cap free
Amount of items: 2
Items: 
Size: 794824 Color: 1
Size: 205045 Color: 6

Bin 3141: 132 of cap free
Amount of items: 2
Items: 
Size: 535597 Color: 13
Size: 464272 Color: 15

Bin 3142: 132 of cap free
Amount of items: 2
Items: 
Size: 544621 Color: 10
Size: 455248 Color: 13

Bin 3143: 132 of cap free
Amount of items: 2
Items: 
Size: 632218 Color: 3
Size: 367651 Color: 13

Bin 3144: 132 of cap free
Amount of items: 2
Items: 
Size: 672475 Color: 1
Size: 327394 Color: 13

Bin 3145: 132 of cap free
Amount of items: 2
Items: 
Size: 795034 Color: 17
Size: 204835 Color: 12

Bin 3146: 133 of cap free
Amount of items: 3
Items: 
Size: 770971 Color: 7
Size: 114594 Color: 14
Size: 114303 Color: 8

Bin 3147: 133 of cap free
Amount of items: 2
Items: 
Size: 693632 Color: 14
Size: 306236 Color: 0

Bin 3148: 133 of cap free
Amount of items: 2
Items: 
Size: 518835 Color: 8
Size: 481033 Color: 17

Bin 3149: 133 of cap free
Amount of items: 2
Items: 
Size: 566458 Color: 19
Size: 433410 Color: 11

Bin 3150: 133 of cap free
Amount of items: 2
Items: 
Size: 716584 Color: 9
Size: 283284 Color: 16

Bin 3151: 134 of cap free
Amount of items: 2
Items: 
Size: 776111 Color: 6
Size: 223756 Color: 17

Bin 3152: 134 of cap free
Amount of items: 2
Items: 
Size: 670993 Color: 7
Size: 328874 Color: 12

Bin 3153: 134 of cap free
Amount of items: 2
Items: 
Size: 606859 Color: 18
Size: 393008 Color: 12

Bin 3154: 134 of cap free
Amount of items: 2
Items: 
Size: 786562 Color: 9
Size: 213305 Color: 0

Bin 3155: 135 of cap free
Amount of items: 3
Items: 
Size: 642448 Color: 7
Size: 199334 Color: 17
Size: 158084 Color: 11

Bin 3156: 135 of cap free
Amount of items: 2
Items: 
Size: 520386 Color: 2
Size: 479480 Color: 19

Bin 3157: 135 of cap free
Amount of items: 2
Items: 
Size: 544616 Color: 17
Size: 455250 Color: 10

Bin 3158: 135 of cap free
Amount of items: 2
Items: 
Size: 574891 Color: 1
Size: 424975 Color: 8

Bin 3159: 135 of cap free
Amount of items: 2
Items: 
Size: 714679 Color: 18
Size: 285187 Color: 16

Bin 3160: 136 of cap free
Amount of items: 2
Items: 
Size: 704774 Color: 9
Size: 295091 Color: 14

Bin 3161: 136 of cap free
Amount of items: 2
Items: 
Size: 502059 Color: 6
Size: 497806 Color: 18

Bin 3162: 136 of cap free
Amount of items: 2
Items: 
Size: 610848 Color: 4
Size: 389017 Color: 11

Bin 3163: 136 of cap free
Amount of items: 2
Items: 
Size: 628047 Color: 9
Size: 371818 Color: 8

Bin 3164: 137 of cap free
Amount of items: 2
Items: 
Size: 647719 Color: 3
Size: 352145 Color: 10

Bin 3165: 137 of cap free
Amount of items: 2
Items: 
Size: 719611 Color: 11
Size: 280253 Color: 14

Bin 3166: 137 of cap free
Amount of items: 2
Items: 
Size: 712730 Color: 17
Size: 287134 Color: 0

Bin 3167: 137 of cap free
Amount of items: 2
Items: 
Size: 545168 Color: 15
Size: 454696 Color: 13

Bin 3168: 138 of cap free
Amount of items: 2
Items: 
Size: 678695 Color: 2
Size: 321168 Color: 11

Bin 3169: 138 of cap free
Amount of items: 2
Items: 
Size: 737296 Color: 6
Size: 262567 Color: 12

Bin 3170: 138 of cap free
Amount of items: 2
Items: 
Size: 643184 Color: 7
Size: 356679 Color: 6

Bin 3171: 138 of cap free
Amount of items: 2
Items: 
Size: 603502 Color: 5
Size: 396361 Color: 19

Bin 3172: 139 of cap free
Amount of items: 2
Items: 
Size: 663160 Color: 7
Size: 336702 Color: 11

Bin 3173: 139 of cap free
Amount of items: 2
Items: 
Size: 795209 Color: 13
Size: 204653 Color: 15

Bin 3174: 139 of cap free
Amount of items: 2
Items: 
Size: 569100 Color: 5
Size: 430762 Color: 17

Bin 3175: 139 of cap free
Amount of items: 2
Items: 
Size: 594424 Color: 1
Size: 405438 Color: 3

Bin 3176: 139 of cap free
Amount of items: 2
Items: 
Size: 635538 Color: 4
Size: 364324 Color: 18

Bin 3177: 139 of cap free
Amount of items: 2
Items: 
Size: 775293 Color: 4
Size: 224569 Color: 18

Bin 3178: 140 of cap free
Amount of items: 3
Items: 
Size: 515342 Color: 1
Size: 340598 Color: 4
Size: 143921 Color: 15

Bin 3179: 140 of cap free
Amount of items: 2
Items: 
Size: 529398 Color: 3
Size: 470463 Color: 4

Bin 3180: 140 of cap free
Amount of items: 3
Items: 
Size: 677574 Color: 1
Size: 174164 Color: 13
Size: 148123 Color: 15

Bin 3181: 140 of cap free
Amount of items: 2
Items: 
Size: 685421 Color: 13
Size: 314440 Color: 14

Bin 3182: 140 of cap free
Amount of items: 2
Items: 
Size: 511347 Color: 5
Size: 488514 Color: 16

Bin 3183: 140 of cap free
Amount of items: 2
Items: 
Size: 534944 Color: 17
Size: 464917 Color: 1

Bin 3184: 140 of cap free
Amount of items: 2
Items: 
Size: 547361 Color: 0
Size: 452500 Color: 10

Bin 3185: 140 of cap free
Amount of items: 2
Items: 
Size: 588102 Color: 16
Size: 411759 Color: 1

Bin 3186: 140 of cap free
Amount of items: 2
Items: 
Size: 695364 Color: 13
Size: 304497 Color: 15

Bin 3187: 140 of cap free
Amount of items: 2
Items: 
Size: 793878 Color: 13
Size: 205983 Color: 8

Bin 3188: 141 of cap free
Amount of items: 2
Items: 
Size: 639179 Color: 16
Size: 360681 Color: 15

Bin 3189: 141 of cap free
Amount of items: 2
Items: 
Size: 707869 Color: 19
Size: 291991 Color: 0

Bin 3190: 141 of cap free
Amount of items: 2
Items: 
Size: 548103 Color: 1
Size: 451757 Color: 16

Bin 3191: 141 of cap free
Amount of items: 2
Items: 
Size: 692512 Color: 7
Size: 307348 Color: 3

Bin 3192: 142 of cap free
Amount of items: 3
Items: 
Size: 712973 Color: 16
Size: 152048 Color: 0
Size: 134838 Color: 5

Bin 3193: 142 of cap free
Amount of items: 2
Items: 
Size: 741832 Color: 9
Size: 258027 Color: 4

Bin 3194: 142 of cap free
Amount of items: 2
Items: 
Size: 681071 Color: 8
Size: 318788 Color: 7

Bin 3195: 143 of cap free
Amount of items: 2
Items: 
Size: 527244 Color: 18
Size: 472614 Color: 11

Bin 3196: 143 of cap free
Amount of items: 2
Items: 
Size: 548818 Color: 16
Size: 451040 Color: 4

Bin 3197: 143 of cap free
Amount of items: 2
Items: 
Size: 660207 Color: 5
Size: 339651 Color: 3

Bin 3198: 144 of cap free
Amount of items: 2
Items: 
Size: 701765 Color: 17
Size: 298092 Color: 6

Bin 3199: 144 of cap free
Amount of items: 2
Items: 
Size: 604681 Color: 15
Size: 395176 Color: 18

Bin 3200: 144 of cap free
Amount of items: 2
Items: 
Size: 693250 Color: 0
Size: 306607 Color: 7

Bin 3201: 144 of cap free
Amount of items: 2
Items: 
Size: 706063 Color: 7
Size: 293794 Color: 6

Bin 3202: 144 of cap free
Amount of items: 2
Items: 
Size: 637610 Color: 16
Size: 362247 Color: 12

Bin 3203: 144 of cap free
Amount of items: 2
Items: 
Size: 640484 Color: 4
Size: 359373 Color: 3

Bin 3204: 144 of cap free
Amount of items: 2
Items: 
Size: 544612 Color: 14
Size: 455245 Color: 19

Bin 3205: 144 of cap free
Amount of items: 2
Items: 
Size: 612429 Color: 2
Size: 387428 Color: 8

Bin 3206: 144 of cap free
Amount of items: 2
Items: 
Size: 658978 Color: 15
Size: 340879 Color: 12

Bin 3207: 145 of cap free
Amount of items: 2
Items: 
Size: 671493 Color: 4
Size: 328363 Color: 18

Bin 3208: 145 of cap free
Amount of items: 2
Items: 
Size: 665893 Color: 12
Size: 333963 Color: 3

Bin 3209: 145 of cap free
Amount of items: 2
Items: 
Size: 540098 Color: 19
Size: 459758 Color: 10

Bin 3210: 145 of cap free
Amount of items: 2
Items: 
Size: 554352 Color: 9
Size: 445504 Color: 6

Bin 3211: 146 of cap free
Amount of items: 3
Items: 
Size: 693404 Color: 14
Size: 153240 Color: 19
Size: 153211 Color: 7

Bin 3212: 146 of cap free
Amount of items: 2
Items: 
Size: 606279 Color: 7
Size: 393576 Color: 14

Bin 3213: 146 of cap free
Amount of items: 2
Items: 
Size: 652820 Color: 13
Size: 347035 Color: 16

Bin 3214: 147 of cap free
Amount of items: 2
Items: 
Size: 744418 Color: 4
Size: 255436 Color: 0

Bin 3215: 147 of cap free
Amount of items: 2
Items: 
Size: 637381 Color: 16
Size: 362473 Color: 6

Bin 3216: 147 of cap free
Amount of items: 2
Items: 
Size: 611608 Color: 13
Size: 388246 Color: 8

Bin 3217: 147 of cap free
Amount of items: 2
Items: 
Size: 594142 Color: 2
Size: 405712 Color: 12

Bin 3218: 147 of cap free
Amount of items: 2
Items: 
Size: 720577 Color: 10
Size: 279277 Color: 19

Bin 3219: 147 of cap free
Amount of items: 2
Items: 
Size: 553446 Color: 11
Size: 446408 Color: 16

Bin 3220: 147 of cap free
Amount of items: 2
Items: 
Size: 574535 Color: 8
Size: 425319 Color: 15

Bin 3221: 148 of cap free
Amount of items: 2
Items: 
Size: 795392 Color: 14
Size: 204461 Color: 0

Bin 3222: 148 of cap free
Amount of items: 2
Items: 
Size: 674215 Color: 5
Size: 325638 Color: 1

Bin 3223: 148 of cap free
Amount of items: 2
Items: 
Size: 733956 Color: 10
Size: 265897 Color: 2

Bin 3224: 148 of cap free
Amount of items: 2
Items: 
Size: 546399 Color: 8
Size: 453454 Color: 4

Bin 3225: 148 of cap free
Amount of items: 2
Items: 
Size: 554248 Color: 2
Size: 445605 Color: 12

Bin 3226: 148 of cap free
Amount of items: 2
Items: 
Size: 599652 Color: 1
Size: 400201 Color: 9

Bin 3227: 148 of cap free
Amount of items: 2
Items: 
Size: 663336 Color: 3
Size: 336517 Color: 16

Bin 3228: 149 of cap free
Amount of items: 2
Items: 
Size: 714259 Color: 10
Size: 285593 Color: 0

Bin 3229: 149 of cap free
Amount of items: 2
Items: 
Size: 581448 Color: 11
Size: 418404 Color: 1

Bin 3230: 150 of cap free
Amount of items: 2
Items: 
Size: 701019 Color: 13
Size: 298832 Color: 5

Bin 3231: 150 of cap free
Amount of items: 2
Items: 
Size: 513395 Color: 0
Size: 486456 Color: 17

Bin 3232: 150 of cap free
Amount of items: 2
Items: 
Size: 515861 Color: 13
Size: 483990 Color: 2

Bin 3233: 150 of cap free
Amount of items: 2
Items: 
Size: 534268 Color: 2
Size: 465583 Color: 6

Bin 3234: 150 of cap free
Amount of items: 2
Items: 
Size: 705469 Color: 16
Size: 294382 Color: 9

Bin 3235: 151 of cap free
Amount of items: 3
Items: 
Size: 715183 Color: 0
Size: 174933 Color: 12
Size: 109734 Color: 14

Bin 3236: 151 of cap free
Amount of items: 2
Items: 
Size: 720108 Color: 6
Size: 279742 Color: 14

Bin 3237: 152 of cap free
Amount of items: 2
Items: 
Size: 511498 Color: 6
Size: 488351 Color: 10

Bin 3238: 152 of cap free
Amount of items: 2
Items: 
Size: 729841 Color: 15
Size: 270008 Color: 18

Bin 3239: 152 of cap free
Amount of items: 2
Items: 
Size: 760548 Color: 19
Size: 239301 Color: 16

Bin 3240: 152 of cap free
Amount of items: 2
Items: 
Size: 774213 Color: 5
Size: 225636 Color: 8

Bin 3241: 152 of cap free
Amount of items: 3
Items: 
Size: 469660 Color: 12
Size: 287719 Color: 0
Size: 242470 Color: 2

Bin 3242: 152 of cap free
Amount of items: 2
Items: 
Size: 722746 Color: 8
Size: 277103 Color: 7

Bin 3243: 153 of cap free
Amount of items: 2
Items: 
Size: 641210 Color: 13
Size: 358638 Color: 8

Bin 3244: 153 of cap free
Amount of items: 2
Items: 
Size: 677572 Color: 2
Size: 322276 Color: 11

Bin 3245: 153 of cap free
Amount of items: 2
Items: 
Size: 778447 Color: 11
Size: 221401 Color: 7

Bin 3246: 153 of cap free
Amount of items: 2
Items: 
Size: 551369 Color: 16
Size: 448479 Color: 18

Bin 3247: 153 of cap free
Amount of items: 2
Items: 
Size: 573182 Color: 10
Size: 426666 Color: 11

Bin 3248: 153 of cap free
Amount of items: 2
Items: 
Size: 576906 Color: 5
Size: 422942 Color: 10

Bin 3249: 153 of cap free
Amount of items: 2
Items: 
Size: 740306 Color: 2
Size: 259542 Color: 9

Bin 3250: 153 of cap free
Amount of items: 2
Items: 
Size: 780597 Color: 10
Size: 219251 Color: 6

Bin 3251: 154 of cap free
Amount of items: 2
Items: 
Size: 721016 Color: 1
Size: 278831 Color: 10

Bin 3252: 154 of cap free
Amount of items: 2
Items: 
Size: 709402 Color: 1
Size: 290445 Color: 13

Bin 3253: 154 of cap free
Amount of items: 2
Items: 
Size: 568795 Color: 1
Size: 431052 Color: 13

Bin 3254: 154 of cap free
Amount of items: 2
Items: 
Size: 585128 Color: 15
Size: 414719 Color: 1

Bin 3255: 154 of cap free
Amount of items: 2
Items: 
Size: 593878 Color: 9
Size: 405969 Color: 12

Bin 3256: 154 of cap free
Amount of items: 2
Items: 
Size: 597646 Color: 7
Size: 402201 Color: 19

Bin 3257: 155 of cap free
Amount of items: 2
Items: 
Size: 687302 Color: 9
Size: 312544 Color: 6

Bin 3258: 155 of cap free
Amount of items: 2
Items: 
Size: 674416 Color: 14
Size: 325430 Color: 17

Bin 3259: 156 of cap free
Amount of items: 3
Items: 
Size: 655447 Color: 5
Size: 197151 Color: 16
Size: 147247 Color: 9

Bin 3260: 156 of cap free
Amount of items: 2
Items: 
Size: 720324 Color: 19
Size: 279521 Color: 14

Bin 3261: 156 of cap free
Amount of items: 2
Items: 
Size: 508654 Color: 1
Size: 491191 Color: 10

Bin 3262: 156 of cap free
Amount of items: 2
Items: 
Size: 516755 Color: 16
Size: 483090 Color: 2

Bin 3263: 156 of cap free
Amount of items: 2
Items: 
Size: 555645 Color: 12
Size: 444200 Color: 7

Bin 3264: 156 of cap free
Amount of items: 3
Items: 
Size: 627896 Color: 4
Size: 186455 Color: 6
Size: 185494 Color: 0

Bin 3265: 157 of cap free
Amount of items: 2
Items: 
Size: 736442 Color: 2
Size: 263402 Color: 6

Bin 3266: 157 of cap free
Amount of items: 3
Items: 
Size: 364907 Color: 3
Size: 336105 Color: 3
Size: 298832 Color: 1

Bin 3267: 157 of cap free
Amount of items: 2
Items: 
Size: 726379 Color: 13
Size: 273465 Color: 11

Bin 3268: 158 of cap free
Amount of items: 2
Items: 
Size: 746320 Color: 2
Size: 253523 Color: 10

Bin 3269: 158 of cap free
Amount of items: 2
Items: 
Size: 615591 Color: 2
Size: 384252 Color: 12

Bin 3270: 158 of cap free
Amount of items: 2
Items: 
Size: 532502 Color: 16
Size: 467341 Color: 1

Bin 3271: 158 of cap free
Amount of items: 2
Items: 
Size: 547358 Color: 7
Size: 452485 Color: 3

Bin 3272: 158 of cap free
Amount of items: 2
Items: 
Size: 646696 Color: 18
Size: 353147 Color: 7

Bin 3273: 159 of cap free
Amount of items: 2
Items: 
Size: 712519 Color: 8
Size: 287323 Color: 14

Bin 3274: 159 of cap free
Amount of items: 2
Items: 
Size: 754091 Color: 19
Size: 245751 Color: 0

Bin 3275: 159 of cap free
Amount of items: 2
Items: 
Size: 509023 Color: 2
Size: 490819 Color: 8

Bin 3276: 159 of cap free
Amount of items: 2
Items: 
Size: 561356 Color: 19
Size: 438486 Color: 17

Bin 3277: 159 of cap free
Amount of items: 2
Items: 
Size: 609586 Color: 5
Size: 390256 Color: 19

Bin 3278: 159 of cap free
Amount of items: 2
Items: 
Size: 773420 Color: 12
Size: 226422 Color: 15

Bin 3279: 159 of cap free
Amount of items: 2
Items: 
Size: 779783 Color: 16
Size: 220059 Color: 11

Bin 3280: 160 of cap free
Amount of items: 2
Items: 
Size: 739237 Color: 14
Size: 260604 Color: 10

Bin 3281: 160 of cap free
Amount of items: 2
Items: 
Size: 692694 Color: 11
Size: 307147 Color: 18

Bin 3282: 160 of cap free
Amount of items: 2
Items: 
Size: 704310 Color: 6
Size: 295531 Color: 12

Bin 3283: 160 of cap free
Amount of items: 2
Items: 
Size: 701170 Color: 9
Size: 298671 Color: 0

Bin 3284: 160 of cap free
Amount of items: 2
Items: 
Size: 627037 Color: 8
Size: 372804 Color: 6

Bin 3285: 160 of cap free
Amount of items: 2
Items: 
Size: 695931 Color: 6
Size: 303910 Color: 10

Bin 3286: 160 of cap free
Amount of items: 2
Items: 
Size: 639934 Color: 6
Size: 359907 Color: 11

Bin 3287: 161 of cap free
Amount of items: 2
Items: 
Size: 745878 Color: 15
Size: 253962 Color: 3

Bin 3288: 161 of cap free
Amount of items: 2
Items: 
Size: 589685 Color: 13
Size: 410155 Color: 2

Bin 3289: 161 of cap free
Amount of items: 2
Items: 
Size: 618478 Color: 4
Size: 381362 Color: 8

Bin 3290: 162 of cap free
Amount of items: 2
Items: 
Size: 647154 Color: 0
Size: 352685 Color: 15

Bin 3291: 162 of cap free
Amount of items: 2
Items: 
Size: 674166 Color: 4
Size: 325673 Color: 18

Bin 3292: 162 of cap free
Amount of items: 2
Items: 
Size: 748576 Color: 14
Size: 251263 Color: 4

Bin 3293: 162 of cap free
Amount of items: 2
Items: 
Size: 584605 Color: 19
Size: 415234 Color: 2

Bin 3294: 162 of cap free
Amount of items: 2
Items: 
Size: 618569 Color: 6
Size: 381270 Color: 12

Bin 3295: 163 of cap free
Amount of items: 2
Items: 
Size: 538313 Color: 6
Size: 461525 Color: 17

Bin 3296: 163 of cap free
Amount of items: 2
Items: 
Size: 665892 Color: 19
Size: 333946 Color: 13

Bin 3297: 163 of cap free
Amount of items: 2
Items: 
Size: 532251 Color: 12
Size: 467587 Color: 14

Bin 3298: 163 of cap free
Amount of items: 2
Items: 
Size: 564844 Color: 16
Size: 434994 Color: 7

Bin 3299: 163 of cap free
Amount of items: 2
Items: 
Size: 598020 Color: 7
Size: 401818 Color: 5

Bin 3300: 163 of cap free
Amount of items: 2
Items: 
Size: 607097 Color: 10
Size: 392741 Color: 3

Bin 3301: 163 of cap free
Amount of items: 2
Items: 
Size: 767890 Color: 5
Size: 231948 Color: 12

Bin 3302: 164 of cap free
Amount of items: 2
Items: 
Size: 755898 Color: 16
Size: 243939 Color: 13

Bin 3303: 164 of cap free
Amount of items: 2
Items: 
Size: 769725 Color: 14
Size: 230112 Color: 13

Bin 3304: 164 of cap free
Amount of items: 2
Items: 
Size: 523305 Color: 9
Size: 476532 Color: 19

Bin 3305: 164 of cap free
Amount of items: 2
Items: 
Size: 621040 Color: 13
Size: 378797 Color: 4

Bin 3306: 164 of cap free
Amount of items: 2
Items: 
Size: 686893 Color: 8
Size: 312944 Color: 9

Bin 3307: 165 of cap free
Amount of items: 2
Items: 
Size: 732695 Color: 17
Size: 267141 Color: 16

Bin 3308: 165 of cap free
Amount of items: 2
Items: 
Size: 662533 Color: 7
Size: 337303 Color: 1

Bin 3309: 165 of cap free
Amount of items: 2
Items: 
Size: 653364 Color: 10
Size: 346472 Color: 4

Bin 3310: 165 of cap free
Amount of items: 2
Items: 
Size: 619869 Color: 4
Size: 379967 Color: 19

Bin 3311: 165 of cap free
Amount of items: 2
Items: 
Size: 521546 Color: 4
Size: 478290 Color: 10

Bin 3312: 166 of cap free
Amount of items: 2
Items: 
Size: 535276 Color: 15
Size: 464559 Color: 19

Bin 3313: 166 of cap free
Amount of items: 2
Items: 
Size: 556012 Color: 17
Size: 443823 Color: 18

Bin 3314: 168 of cap free
Amount of items: 2
Items: 
Size: 659130 Color: 19
Size: 340703 Color: 15

Bin 3315: 168 of cap free
Amount of items: 2
Items: 
Size: 584803 Color: 0
Size: 415030 Color: 8

Bin 3316: 168 of cap free
Amount of items: 2
Items: 
Size: 539077 Color: 3
Size: 460756 Color: 10

Bin 3317: 168 of cap free
Amount of items: 2
Items: 
Size: 724648 Color: 3
Size: 275185 Color: 5

Bin 3318: 168 of cap free
Amount of items: 2
Items: 
Size: 782346 Color: 0
Size: 217487 Color: 9

Bin 3319: 168 of cap free
Amount of items: 2
Items: 
Size: 529676 Color: 0
Size: 470157 Color: 6

Bin 3320: 168 of cap free
Amount of items: 2
Items: 
Size: 578900 Color: 5
Size: 420933 Color: 15

Bin 3321: 168 of cap free
Amount of items: 2
Items: 
Size: 744593 Color: 11
Size: 255240 Color: 14

Bin 3322: 169 of cap free
Amount of items: 2
Items: 
Size: 546138 Color: 0
Size: 453694 Color: 10

Bin 3323: 170 of cap free
Amount of items: 2
Items: 
Size: 627478 Color: 8
Size: 372353 Color: 3

Bin 3324: 170 of cap free
Amount of items: 2
Items: 
Size: 536289 Color: 2
Size: 463542 Color: 18

Bin 3325: 170 of cap free
Amount of items: 2
Items: 
Size: 586390 Color: 9
Size: 413441 Color: 5

Bin 3326: 171 of cap free
Amount of items: 2
Items: 
Size: 729921 Color: 3
Size: 269909 Color: 13

Bin 3327: 171 of cap free
Amount of items: 2
Items: 
Size: 687805 Color: 11
Size: 312025 Color: 0

Bin 3328: 171 of cap free
Amount of items: 2
Items: 
Size: 584083 Color: 3
Size: 415747 Color: 7

Bin 3329: 171 of cap free
Amount of items: 2
Items: 
Size: 638698 Color: 7
Size: 361132 Color: 17

Bin 3330: 172 of cap free
Amount of items: 2
Items: 
Size: 518402 Color: 12
Size: 481427 Color: 10

Bin 3331: 172 of cap free
Amount of items: 2
Items: 
Size: 588006 Color: 14
Size: 411823 Color: 16

Bin 3332: 173 of cap free
Amount of items: 2
Items: 
Size: 619124 Color: 5
Size: 380704 Color: 6

Bin 3333: 173 of cap free
Amount of items: 2
Items: 
Size: 732160 Color: 13
Size: 267668 Color: 18

Bin 3334: 173 of cap free
Amount of items: 2
Items: 
Size: 655670 Color: 15
Size: 344158 Color: 0

Bin 3335: 173 of cap free
Amount of items: 2
Items: 
Size: 664129 Color: 3
Size: 335699 Color: 19

Bin 3336: 174 of cap free
Amount of items: 2
Items: 
Size: 634918 Color: 3
Size: 364909 Color: 7

Bin 3337: 174 of cap free
Amount of items: 2
Items: 
Size: 756773 Color: 16
Size: 243054 Color: 17

Bin 3338: 174 of cap free
Amount of items: 2
Items: 
Size: 510735 Color: 18
Size: 489092 Color: 16

Bin 3339: 174 of cap free
Amount of items: 2
Items: 
Size: 514323 Color: 10
Size: 485504 Color: 2

Bin 3340: 175 of cap free
Amount of items: 3
Items: 
Size: 536300 Color: 3
Size: 305460 Color: 1
Size: 158066 Color: 13

Bin 3341: 175 of cap free
Amount of items: 2
Items: 
Size: 553640 Color: 16
Size: 446186 Color: 12

Bin 3342: 175 of cap free
Amount of items: 2
Items: 
Size: 597635 Color: 10
Size: 402191 Color: 5

Bin 3343: 175 of cap free
Amount of items: 2
Items: 
Size: 630517 Color: 15
Size: 369309 Color: 12

Bin 3344: 176 of cap free
Amount of items: 2
Items: 
Size: 634093 Color: 14
Size: 365732 Color: 0

Bin 3345: 176 of cap free
Amount of items: 2
Items: 
Size: 756441 Color: 9
Size: 243384 Color: 8

Bin 3346: 176 of cap free
Amount of items: 2
Items: 
Size: 670961 Color: 16
Size: 328864 Color: 0

Bin 3347: 176 of cap free
Amount of items: 2
Items: 
Size: 674485 Color: 17
Size: 325340 Color: 5

Bin 3348: 176 of cap free
Amount of items: 2
Items: 
Size: 682080 Color: 4
Size: 317745 Color: 18

Bin 3349: 177 of cap free
Amount of items: 2
Items: 
Size: 798720 Color: 1
Size: 201104 Color: 18

Bin 3350: 177 of cap free
Amount of items: 2
Items: 
Size: 550522 Color: 8
Size: 449302 Color: 16

Bin 3351: 177 of cap free
Amount of items: 2
Items: 
Size: 731894 Color: 11
Size: 267930 Color: 6

Bin 3352: 178 of cap free
Amount of items: 2
Items: 
Size: 789389 Color: 0
Size: 210434 Color: 7

Bin 3353: 178 of cap free
Amount of items: 2
Items: 
Size: 533880 Color: 11
Size: 465943 Color: 15

Bin 3354: 178 of cap free
Amount of items: 2
Items: 
Size: 710571 Color: 17
Size: 289252 Color: 12

Bin 3355: 178 of cap free
Amount of items: 3
Items: 
Size: 415463 Color: 3
Size: 332693 Color: 17
Size: 251667 Color: 14

Bin 3356: 178 of cap free
Amount of items: 2
Items: 
Size: 519826 Color: 1
Size: 479997 Color: 8

Bin 3357: 178 of cap free
Amount of items: 2
Items: 
Size: 556870 Color: 7
Size: 442953 Color: 12

Bin 3358: 178 of cap free
Amount of items: 2
Items: 
Size: 623830 Color: 16
Size: 375993 Color: 19

Bin 3359: 178 of cap free
Amount of items: 2
Items: 
Size: 699350 Color: 16
Size: 300473 Color: 15

Bin 3360: 179 of cap free
Amount of items: 2
Items: 
Size: 683356 Color: 4
Size: 316466 Color: 7

Bin 3361: 179 of cap free
Amount of items: 3
Items: 
Size: 501943 Color: 7
Size: 280045 Color: 7
Size: 217834 Color: 5

Bin 3362: 179 of cap free
Amount of items: 2
Items: 
Size: 565685 Color: 3
Size: 434137 Color: 13

Bin 3363: 179 of cap free
Amount of items: 2
Items: 
Size: 613407 Color: 8
Size: 386415 Color: 10

Bin 3364: 179 of cap free
Amount of items: 2
Items: 
Size: 653369 Color: 4
Size: 346453 Color: 2

Bin 3365: 179 of cap free
Amount of items: 2
Items: 
Size: 676358 Color: 9
Size: 323464 Color: 11

Bin 3366: 179 of cap free
Amount of items: 2
Items: 
Size: 765597 Color: 6
Size: 234225 Color: 18

Bin 3367: 180 of cap free
Amount of items: 2
Items: 
Size: 517806 Color: 18
Size: 482015 Color: 0

Bin 3368: 180 of cap free
Amount of items: 2
Items: 
Size: 528962 Color: 10
Size: 470859 Color: 0

Bin 3369: 180 of cap free
Amount of items: 2
Items: 
Size: 531343 Color: 10
Size: 468478 Color: 8

Bin 3370: 180 of cap free
Amount of items: 2
Items: 
Size: 660744 Color: 5
Size: 339077 Color: 15

Bin 3371: 180 of cap free
Amount of items: 2
Items: 
Size: 774190 Color: 12
Size: 225631 Color: 6

Bin 3372: 180 of cap free
Amount of items: 2
Items: 
Size: 522068 Color: 8
Size: 477753 Color: 1

Bin 3373: 181 of cap free
Amount of items: 3
Items: 
Size: 720057 Color: 6
Size: 148540 Color: 19
Size: 131223 Color: 12

Bin 3374: 181 of cap free
Amount of items: 2
Items: 
Size: 791697 Color: 1
Size: 208123 Color: 3

Bin 3375: 181 of cap free
Amount of items: 2
Items: 
Size: 774465 Color: 16
Size: 225355 Color: 9

Bin 3376: 181 of cap free
Amount of items: 2
Items: 
Size: 713696 Color: 11
Size: 286124 Color: 18

Bin 3377: 181 of cap free
Amount of items: 2
Items: 
Size: 790725 Color: 0
Size: 209095 Color: 8

Bin 3378: 181 of cap free
Amount of items: 2
Items: 
Size: 617293 Color: 8
Size: 382527 Color: 4

Bin 3379: 181 of cap free
Amount of items: 2
Items: 
Size: 589272 Color: 11
Size: 410548 Color: 14

Bin 3380: 181 of cap free
Amount of items: 2
Items: 
Size: 679299 Color: 14
Size: 320521 Color: 17

Bin 3381: 182 of cap free
Amount of items: 3
Items: 
Size: 610002 Color: 2
Size: 194965 Color: 16
Size: 194852 Color: 10

Bin 3382: 182 of cap free
Amount of items: 2
Items: 
Size: 622168 Color: 19
Size: 377651 Color: 5

Bin 3383: 182 of cap free
Amount of items: 2
Items: 
Size: 512823 Color: 1
Size: 486996 Color: 8

Bin 3384: 182 of cap free
Amount of items: 2
Items: 
Size: 533100 Color: 6
Size: 466719 Color: 5

Bin 3385: 182 of cap free
Amount of items: 2
Items: 
Size: 526319 Color: 7
Size: 473500 Color: 13

Bin 3386: 182 of cap free
Amount of items: 2
Items: 
Size: 535852 Color: 5
Size: 463967 Color: 14

Bin 3387: 182 of cap free
Amount of items: 2
Items: 
Size: 594346 Color: 3
Size: 405473 Color: 1

Bin 3388: 182 of cap free
Amount of items: 2
Items: 
Size: 705466 Color: 13
Size: 294353 Color: 2

Bin 3389: 183 of cap free
Amount of items: 2
Items: 
Size: 544886 Color: 2
Size: 454932 Color: 16

Bin 3390: 183 of cap free
Amount of items: 2
Items: 
Size: 613173 Color: 16
Size: 386645 Color: 6

Bin 3391: 183 of cap free
Amount of items: 2
Items: 
Size: 744843 Color: 13
Size: 254975 Color: 0

Bin 3392: 183 of cap free
Amount of items: 2
Items: 
Size: 785635 Color: 3
Size: 214183 Color: 7

Bin 3393: 184 of cap free
Amount of items: 2
Items: 
Size: 692603 Color: 12
Size: 307214 Color: 6

Bin 3394: 184 of cap free
Amount of items: 2
Items: 
Size: 793500 Color: 5
Size: 206317 Color: 15

Bin 3395: 184 of cap free
Amount of items: 2
Items: 
Size: 657145 Color: 17
Size: 342672 Color: 9

Bin 3396: 184 of cap free
Amount of items: 2
Items: 
Size: 782877 Color: 11
Size: 216940 Color: 12

Bin 3397: 184 of cap free
Amount of items: 2
Items: 
Size: 577543 Color: 0
Size: 422274 Color: 1

Bin 3398: 185 of cap free
Amount of items: 2
Items: 
Size: 673794 Color: 11
Size: 326022 Color: 19

Bin 3399: 185 of cap free
Amount of items: 2
Items: 
Size: 640853 Color: 19
Size: 358963 Color: 18

Bin 3400: 186 of cap free
Amount of items: 2
Items: 
Size: 627787 Color: 13
Size: 372028 Color: 19

Bin 3401: 186 of cap free
Amount of items: 2
Items: 
Size: 543415 Color: 18
Size: 456400 Color: 16

Bin 3402: 186 of cap free
Amount of items: 2
Items: 
Size: 685906 Color: 2
Size: 313909 Color: 18

Bin 3403: 187 of cap free
Amount of items: 2
Items: 
Size: 780085 Color: 8
Size: 219729 Color: 18

Bin 3404: 187 of cap free
Amount of items: 2
Items: 
Size: 596986 Color: 0
Size: 402828 Color: 10

Bin 3405: 187 of cap free
Amount of items: 2
Items: 
Size: 572294 Color: 9
Size: 427520 Color: 15

Bin 3406: 187 of cap free
Amount of items: 2
Items: 
Size: 618472 Color: 14
Size: 381342 Color: 9

Bin 3407: 187 of cap free
Amount of items: 2
Items: 
Size: 618911 Color: 3
Size: 380903 Color: 0

Bin 3408: 188 of cap free
Amount of items: 2
Items: 
Size: 771950 Color: 15
Size: 227863 Color: 14

Bin 3409: 188 of cap free
Amount of items: 2
Items: 
Size: 636322 Color: 14
Size: 363491 Color: 19

Bin 3410: 188 of cap free
Amount of items: 2
Items: 
Size: 651205 Color: 6
Size: 348608 Color: 14

Bin 3411: 189 of cap free
Amount of items: 2
Items: 
Size: 792437 Color: 12
Size: 207375 Color: 15

Bin 3412: 189 of cap free
Amount of items: 2
Items: 
Size: 520949 Color: 2
Size: 478863 Color: 7

Bin 3413: 189 of cap free
Amount of items: 2
Items: 
Size: 537892 Color: 1
Size: 461920 Color: 18

Bin 3414: 190 of cap free
Amount of items: 2
Items: 
Size: 632161 Color: 7
Size: 367650 Color: 11

Bin 3415: 191 of cap free
Amount of items: 2
Items: 
Size: 722140 Color: 5
Size: 277670 Color: 11

Bin 3416: 191 of cap free
Amount of items: 2
Items: 
Size: 749571 Color: 3
Size: 250239 Color: 13

Bin 3417: 191 of cap free
Amount of items: 2
Items: 
Size: 609798 Color: 9
Size: 390012 Color: 16

Bin 3418: 191 of cap free
Amount of items: 2
Items: 
Size: 634539 Color: 14
Size: 365271 Color: 0

Bin 3419: 192 of cap free
Amount of items: 2
Items: 
Size: 763434 Color: 12
Size: 236375 Color: 0

Bin 3420: 192 of cap free
Amount of items: 3
Items: 
Size: 681722 Color: 16
Size: 159199 Color: 9
Size: 158888 Color: 17

Bin 3421: 193 of cap free
Amount of items: 2
Items: 
Size: 730214 Color: 11
Size: 269594 Color: 18

Bin 3422: 193 of cap free
Amount of items: 2
Items: 
Size: 614448 Color: 2
Size: 385360 Color: 17

Bin 3423: 194 of cap free
Amount of items: 2
Items: 
Size: 727400 Color: 17
Size: 272407 Color: 2

Bin 3424: 194 of cap free
Amount of items: 3
Items: 
Size: 746688 Color: 9
Size: 143478 Color: 19
Size: 109641 Color: 16

Bin 3425: 194 of cap free
Amount of items: 2
Items: 
Size: 523484 Color: 9
Size: 476323 Color: 6

Bin 3426: 194 of cap free
Amount of items: 2
Items: 
Size: 788177 Color: 17
Size: 211630 Color: 4

Bin 3427: 194 of cap free
Amount of items: 2
Items: 
Size: 555613 Color: 3
Size: 444194 Color: 18

Bin 3428: 194 of cap free
Amount of items: 2
Items: 
Size: 570437 Color: 16
Size: 429370 Color: 7

Bin 3429: 194 of cap free
Amount of items: 2
Items: 
Size: 780662 Color: 4
Size: 219145 Color: 8

Bin 3430: 195 of cap free
Amount of items: 2
Items: 
Size: 547152 Color: 5
Size: 452654 Color: 11

Bin 3431: 195 of cap free
Amount of items: 2
Items: 
Size: 632545 Color: 5
Size: 367261 Color: 6

Bin 3432: 196 of cap free
Amount of items: 2
Items: 
Size: 784281 Color: 4
Size: 215524 Color: 0

Bin 3433: 196 of cap free
Amount of items: 2
Items: 
Size: 619478 Color: 10
Size: 380327 Color: 11

Bin 3434: 196 of cap free
Amount of items: 2
Items: 
Size: 607753 Color: 8
Size: 392052 Color: 1

Bin 3435: 197 of cap free
Amount of items: 2
Items: 
Size: 655037 Color: 3
Size: 344767 Color: 0

Bin 3436: 198 of cap free
Amount of items: 2
Items: 
Size: 679979 Color: 1
Size: 319824 Color: 6

Bin 3437: 198 of cap free
Amount of items: 2
Items: 
Size: 731882 Color: 19
Size: 267921 Color: 6

Bin 3438: 198 of cap free
Amount of items: 2
Items: 
Size: 608234 Color: 17
Size: 391569 Color: 18

Bin 3439: 198 of cap free
Amount of items: 2
Items: 
Size: 610377 Color: 8
Size: 389426 Color: 7

Bin 3440: 199 of cap free
Amount of items: 3
Items: 
Size: 531981 Color: 1
Size: 259079 Color: 9
Size: 208742 Color: 11

Bin 3441: 199 of cap free
Amount of items: 2
Items: 
Size: 514611 Color: 3
Size: 485191 Color: 19

Bin 3442: 199 of cap free
Amount of items: 2
Items: 
Size: 543036 Color: 2
Size: 456766 Color: 11

Bin 3443: 199 of cap free
Amount of items: 2
Items: 
Size: 597627 Color: 7
Size: 402175 Color: 16

Bin 3444: 199 of cap free
Amount of items: 2
Items: 
Size: 693560 Color: 6
Size: 306242 Color: 14

Bin 3445: 199 of cap free
Amount of items: 2
Items: 
Size: 729237 Color: 3
Size: 270565 Color: 17

Bin 3446: 200 of cap free
Amount of items: 2
Items: 
Size: 791263 Color: 5
Size: 208538 Color: 3

Bin 3447: 200 of cap free
Amount of items: 2
Items: 
Size: 736045 Color: 11
Size: 263756 Color: 8

Bin 3448: 200 of cap free
Amount of items: 2
Items: 
Size: 659435 Color: 7
Size: 340366 Color: 10

Bin 3449: 200 of cap free
Amount of items: 2
Items: 
Size: 778642 Color: 5
Size: 221159 Color: 14

Bin 3450: 200 of cap free
Amount of items: 2
Items: 
Size: 520326 Color: 13
Size: 479475 Color: 12

Bin 3451: 200 of cap free
Amount of items: 2
Items: 
Size: 507952 Color: 0
Size: 491849 Color: 3

Bin 3452: 201 of cap free
Amount of items: 2
Items: 
Size: 783507 Color: 6
Size: 216293 Color: 14

Bin 3453: 201 of cap free
Amount of items: 2
Items: 
Size: 759704 Color: 1
Size: 240096 Color: 13

Bin 3454: 201 of cap free
Amount of items: 2
Items: 
Size: 516737 Color: 5
Size: 483063 Color: 18

Bin 3455: 201 of cap free
Amount of items: 2
Items: 
Size: 557240 Color: 18
Size: 442560 Color: 4

Bin 3456: 201 of cap free
Amount of items: 2
Items: 
Size: 573967 Color: 9
Size: 425833 Color: 7

Bin 3457: 202 of cap free
Amount of items: 2
Items: 
Size: 503708 Color: 10
Size: 496091 Color: 6

Bin 3458: 202 of cap free
Amount of items: 2
Items: 
Size: 629242 Color: 15
Size: 370557 Color: 4

Bin 3459: 203 of cap free
Amount of items: 2
Items: 
Size: 645897 Color: 7
Size: 353901 Color: 2

Bin 3460: 203 of cap free
Amount of items: 2
Items: 
Size: 796318 Color: 1
Size: 203480 Color: 3

Bin 3461: 204 of cap free
Amount of items: 3
Items: 
Size: 621776 Color: 12
Size: 215438 Color: 6
Size: 162583 Color: 5

Bin 3462: 204 of cap free
Amount of items: 2
Items: 
Size: 680439 Color: 12
Size: 319358 Color: 13

Bin 3463: 204 of cap free
Amount of items: 2
Items: 
Size: 583815 Color: 2
Size: 415982 Color: 18

Bin 3464: 204 of cap free
Amount of items: 2
Items: 
Size: 590872 Color: 0
Size: 408925 Color: 14

Bin 3465: 204 of cap free
Amount of items: 2
Items: 
Size: 741495 Color: 13
Size: 258302 Color: 8

Bin 3466: 205 of cap free
Amount of items: 2
Items: 
Size: 518163 Color: 19
Size: 481633 Color: 4

Bin 3467: 205 of cap free
Amount of items: 2
Items: 
Size: 664102 Color: 18
Size: 335694 Color: 8

Bin 3468: 205 of cap free
Amount of items: 2
Items: 
Size: 757960 Color: 19
Size: 241836 Color: 17

Bin 3469: 205 of cap free
Amount of items: 2
Items: 
Size: 598858 Color: 13
Size: 400938 Color: 15

Bin 3470: 205 of cap free
Amount of items: 2
Items: 
Size: 594583 Color: 4
Size: 405213 Color: 13

Bin 3471: 205 of cap free
Amount of items: 2
Items: 
Size: 555165 Color: 7
Size: 444631 Color: 4

Bin 3472: 206 of cap free
Amount of items: 2
Items: 
Size: 537322 Color: 7
Size: 462473 Color: 18

Bin 3473: 206 of cap free
Amount of items: 2
Items: 
Size: 664275 Color: 6
Size: 335520 Color: 3

Bin 3474: 206 of cap free
Amount of items: 2
Items: 
Size: 791898 Color: 15
Size: 207897 Color: 13

Bin 3475: 206 of cap free
Amount of items: 2
Items: 
Size: 711155 Color: 4
Size: 288640 Color: 13

Bin 3476: 206 of cap free
Amount of items: 2
Items: 
Size: 562288 Color: 18
Size: 437507 Color: 8

Bin 3477: 206 of cap free
Amount of items: 2
Items: 
Size: 567115 Color: 16
Size: 432680 Color: 14

Bin 3478: 206 of cap free
Amount of items: 2
Items: 
Size: 582660 Color: 9
Size: 417135 Color: 16

Bin 3479: 206 of cap free
Amount of items: 2
Items: 
Size: 588082 Color: 16
Size: 411713 Color: 5

Bin 3480: 207 of cap free
Amount of items: 2
Items: 
Size: 659441 Color: 14
Size: 340353 Color: 16

Bin 3481: 207 of cap free
Amount of items: 2
Items: 
Size: 694375 Color: 6
Size: 305419 Color: 11

Bin 3482: 207 of cap free
Amount of items: 2
Items: 
Size: 586631 Color: 17
Size: 413163 Color: 14

Bin 3483: 208 of cap free
Amount of items: 2
Items: 
Size: 728318 Color: 3
Size: 271475 Color: 15

Bin 3484: 208 of cap free
Amount of items: 2
Items: 
Size: 706686 Color: 3
Size: 293107 Color: 16

Bin 3485: 208 of cap free
Amount of items: 2
Items: 
Size: 675921 Color: 15
Size: 323872 Color: 10

Bin 3486: 208 of cap free
Amount of items: 2
Items: 
Size: 612393 Color: 8
Size: 387400 Color: 18

Bin 3487: 208 of cap free
Amount of items: 2
Items: 
Size: 635510 Color: 4
Size: 364283 Color: 7

Bin 3488: 208 of cap free
Amount of items: 2
Items: 
Size: 669019 Color: 15
Size: 330774 Color: 18

Bin 3489: 209 of cap free
Amount of items: 2
Items: 
Size: 677556 Color: 10
Size: 322236 Color: 0

Bin 3490: 209 of cap free
Amount of items: 2
Items: 
Size: 742033 Color: 14
Size: 257759 Color: 18

Bin 3491: 209 of cap free
Amount of items: 2
Items: 
Size: 555984 Color: 2
Size: 443808 Color: 14

Bin 3492: 209 of cap free
Amount of items: 2
Items: 
Size: 552193 Color: 6
Size: 447599 Color: 15

Bin 3493: 210 of cap free
Amount of items: 2
Items: 
Size: 768885 Color: 19
Size: 230906 Color: 7

Bin 3494: 210 of cap free
Amount of items: 2
Items: 
Size: 690785 Color: 11
Size: 309006 Color: 17

Bin 3495: 211 of cap free
Amount of items: 2
Items: 
Size: 623311 Color: 9
Size: 376479 Color: 13

Bin 3496: 211 of cap free
Amount of items: 2
Items: 
Size: 524759 Color: 11
Size: 475031 Color: 4

Bin 3497: 211 of cap free
Amount of items: 2
Items: 
Size: 505834 Color: 19
Size: 493956 Color: 16

Bin 3498: 212 of cap free
Amount of items: 2
Items: 
Size: 666503 Color: 12
Size: 333286 Color: 0

Bin 3499: 212 of cap free
Amount of items: 2
Items: 
Size: 733954 Color: 6
Size: 265835 Color: 17

Bin 3500: 212 of cap free
Amount of items: 2
Items: 
Size: 583024 Color: 18
Size: 416765 Color: 10

Bin 3501: 212 of cap free
Amount of items: 2
Items: 
Size: 602907 Color: 10
Size: 396882 Color: 13

Bin 3502: 213 of cap free
Amount of items: 3
Items: 
Size: 694720 Color: 4
Size: 176252 Color: 16
Size: 128816 Color: 8

Bin 3503: 213 of cap free
Amount of items: 2
Items: 
Size: 532726 Color: 18
Size: 467062 Color: 11

Bin 3504: 213 of cap free
Amount of items: 2
Items: 
Size: 604651 Color: 1
Size: 395137 Color: 7

Bin 3505: 213 of cap free
Amount of items: 3
Items: 
Size: 762848 Color: 8
Size: 134822 Color: 15
Size: 102118 Color: 5

Bin 3506: 213 of cap free
Amount of items: 2
Items: 
Size: 573468 Color: 12
Size: 426320 Color: 9

Bin 3507: 213 of cap free
Amount of items: 2
Items: 
Size: 646656 Color: 0
Size: 353132 Color: 2

Bin 3508: 214 of cap free
Amount of items: 3
Items: 
Size: 748867 Color: 6
Size: 137083 Color: 10
Size: 113837 Color: 0

Bin 3509: 214 of cap free
Amount of items: 2
Items: 
Size: 502643 Color: 10
Size: 497144 Color: 16

Bin 3510: 214 of cap free
Amount of items: 2
Items: 
Size: 639679 Color: 13
Size: 360108 Color: 11

Bin 3511: 214 of cap free
Amount of items: 2
Items: 
Size: 556547 Color: 2
Size: 443240 Color: 4

Bin 3512: 214 of cap free
Amount of items: 2
Items: 
Size: 576143 Color: 9
Size: 423644 Color: 15

Bin 3513: 214 of cap free
Amount of items: 2
Items: 
Size: 681706 Color: 6
Size: 318081 Color: 16

Bin 3514: 215 of cap free
Amount of items: 2
Items: 
Size: 705829 Color: 0
Size: 293957 Color: 10

Bin 3515: 216 of cap free
Amount of items: 2
Items: 
Size: 577235 Color: 5
Size: 422550 Color: 12

Bin 3516: 217 of cap free
Amount of items: 2
Items: 
Size: 527269 Color: 5
Size: 472515 Color: 14

Bin 3517: 217 of cap free
Amount of items: 2
Items: 
Size: 569436 Color: 12
Size: 430348 Color: 14

Bin 3518: 217 of cap free
Amount of items: 2
Items: 
Size: 655033 Color: 7
Size: 344751 Color: 5

Bin 3519: 218 of cap free
Amount of items: 2
Items: 
Size: 538469 Color: 1
Size: 461314 Color: 3

Bin 3520: 218 of cap free
Amount of items: 2
Items: 
Size: 731434 Color: 15
Size: 268349 Color: 12

Bin 3521: 218 of cap free
Amount of items: 2
Items: 
Size: 633385 Color: 11
Size: 366398 Color: 7

Bin 3522: 218 of cap free
Amount of items: 2
Items: 
Size: 612987 Color: 9
Size: 386796 Color: 3

Bin 3523: 218 of cap free
Amount of items: 2
Items: 
Size: 679278 Color: 16
Size: 320505 Color: 5

Bin 3524: 219 of cap free
Amount of items: 2
Items: 
Size: 534722 Color: 11
Size: 465060 Color: 6

Bin 3525: 219 of cap free
Amount of items: 2
Items: 
Size: 745849 Color: 8
Size: 253933 Color: 11

Bin 3526: 219 of cap free
Amount of items: 2
Items: 
Size: 796527 Color: 4
Size: 203255 Color: 8

Bin 3527: 220 of cap free
Amount of items: 2
Items: 
Size: 708256 Color: 18
Size: 291525 Color: 3

Bin 3528: 220 of cap free
Amount of items: 2
Items: 
Size: 797322 Color: 1
Size: 202459 Color: 0

Bin 3529: 220 of cap free
Amount of items: 2
Items: 
Size: 537305 Color: 16
Size: 462476 Color: 3

Bin 3530: 220 of cap free
Amount of items: 2
Items: 
Size: 620812 Color: 6
Size: 378969 Color: 8

Bin 3531: 220 of cap free
Amount of items: 2
Items: 
Size: 554914 Color: 5
Size: 444867 Color: 0

Bin 3532: 221 of cap free
Amount of items: 2
Items: 
Size: 592261 Color: 13
Size: 407519 Color: 4

Bin 3533: 221 of cap free
Amount of items: 2
Items: 
Size: 599382 Color: 17
Size: 400398 Color: 14

Bin 3534: 223 of cap free
Amount of items: 2
Items: 
Size: 506761 Color: 10
Size: 493017 Color: 5

Bin 3535: 224 of cap free
Amount of items: 2
Items: 
Size: 684258 Color: 11
Size: 315519 Color: 12

Bin 3536: 224 of cap free
Amount of items: 2
Items: 
Size: 634088 Color: 14
Size: 365689 Color: 7

Bin 3537: 225 of cap free
Amount of items: 2
Items: 
Size: 618466 Color: 7
Size: 381310 Color: 15

Bin 3538: 225 of cap free
Amount of items: 2
Items: 
Size: 533404 Color: 3
Size: 466372 Color: 14

Bin 3539: 225 of cap free
Amount of items: 2
Items: 
Size: 528205 Color: 12
Size: 471571 Color: 3

Bin 3540: 225 of cap free
Amount of items: 2
Items: 
Size: 579462 Color: 8
Size: 420314 Color: 9

Bin 3541: 226 of cap free
Amount of items: 2
Items: 
Size: 705434 Color: 5
Size: 294341 Color: 12

Bin 3542: 227 of cap free
Amount of items: 2
Items: 
Size: 681615 Color: 1
Size: 318159 Color: 10

Bin 3543: 227 of cap free
Amount of items: 2
Items: 
Size: 716043 Color: 1
Size: 283731 Color: 9

Bin 3544: 228 of cap free
Amount of items: 2
Items: 
Size: 791464 Color: 16
Size: 208309 Color: 10

Bin 3545: 228 of cap free
Amount of items: 2
Items: 
Size: 752019 Color: 3
Size: 247754 Color: 11

Bin 3546: 228 of cap free
Amount of items: 2
Items: 
Size: 610360 Color: 3
Size: 389413 Color: 11

Bin 3547: 229 of cap free
Amount of items: 2
Items: 
Size: 596630 Color: 8
Size: 403142 Color: 3

Bin 3548: 230 of cap free
Amount of items: 2
Items: 
Size: 605383 Color: 9
Size: 394388 Color: 13

Bin 3549: 230 of cap free
Amount of items: 2
Items: 
Size: 537317 Color: 19
Size: 462454 Color: 3

Bin 3550: 230 of cap free
Amount of items: 2
Items: 
Size: 580796 Color: 11
Size: 418975 Color: 6

Bin 3551: 230 of cap free
Amount of items: 2
Items: 
Size: 607091 Color: 18
Size: 392680 Color: 9

Bin 3552: 230 of cap free
Amount of items: 2
Items: 
Size: 788399 Color: 15
Size: 211372 Color: 7

Bin 3553: 231 of cap free
Amount of items: 2
Items: 
Size: 532186 Color: 4
Size: 467584 Color: 7

Bin 3554: 231 of cap free
Amount of items: 2
Items: 
Size: 544122 Color: 11
Size: 455648 Color: 7

Bin 3555: 232 of cap free
Amount of items: 2
Items: 
Size: 799558 Color: 5
Size: 200211 Color: 11

Bin 3556: 232 of cap free
Amount of items: 2
Items: 
Size: 581431 Color: 11
Size: 418338 Color: 9

Bin 3557: 233 of cap free
Amount of items: 2
Items: 
Size: 625259 Color: 13
Size: 374509 Color: 18

Bin 3558: 234 of cap free
Amount of items: 2
Items: 
Size: 508577 Color: 13
Size: 491190 Color: 2

Bin 3559: 234 of cap free
Amount of items: 2
Items: 
Size: 569443 Color: 14
Size: 430324 Color: 0

Bin 3560: 234 of cap free
Amount of items: 2
Items: 
Size: 586627 Color: 14
Size: 413140 Color: 18

Bin 3561: 234 of cap free
Amount of items: 2
Items: 
Size: 591913 Color: 0
Size: 407854 Color: 7

Bin 3562: 235 of cap free
Amount of items: 3
Items: 
Size: 419409 Color: 12
Size: 380456 Color: 2
Size: 199901 Color: 18

Bin 3563: 235 of cap free
Amount of items: 2
Items: 
Size: 725039 Color: 15
Size: 274727 Color: 7

Bin 3564: 235 of cap free
Amount of items: 2
Items: 
Size: 509902 Color: 0
Size: 489864 Color: 12

Bin 3565: 235 of cap free
Amount of items: 2
Items: 
Size: 722739 Color: 3
Size: 277027 Color: 14

Bin 3566: 236 of cap free
Amount of items: 3
Items: 
Size: 513284 Color: 8
Size: 301317 Color: 14
Size: 185164 Color: 8

Bin 3567: 236 of cap free
Amount of items: 2
Items: 
Size: 764401 Color: 12
Size: 235364 Color: 11

Bin 3568: 236 of cap free
Amount of items: 2
Items: 
Size: 688500 Color: 9
Size: 311265 Color: 0

Bin 3569: 236 of cap free
Amount of items: 2
Items: 
Size: 587267 Color: 4
Size: 412498 Color: 15

Bin 3570: 236 of cap free
Amount of items: 2
Items: 
Size: 668686 Color: 2
Size: 331079 Color: 7

Bin 3571: 237 of cap free
Amount of items: 2
Items: 
Size: 763424 Color: 6
Size: 236340 Color: 3

Bin 3572: 237 of cap free
Amount of items: 2
Items: 
Size: 652992 Color: 7
Size: 346772 Color: 15

Bin 3573: 237 of cap free
Amount of items: 2
Items: 
Size: 568751 Color: 15
Size: 431013 Color: 14

Bin 3574: 238 of cap free
Amount of items: 2
Items: 
Size: 534171 Color: 4
Size: 465592 Color: 12

Bin 3575: 239 of cap free
Amount of items: 2
Items: 
Size: 584798 Color: 7
Size: 414964 Color: 10

Bin 3576: 239 of cap free
Amount of items: 2
Items: 
Size: 534439 Color: 18
Size: 465323 Color: 4

Bin 3577: 240 of cap free
Amount of items: 2
Items: 
Size: 798158 Color: 2
Size: 201603 Color: 13

Bin 3578: 242 of cap free
Amount of items: 2
Items: 
Size: 530488 Color: 12
Size: 469271 Color: 16

Bin 3579: 242 of cap free
Amount of items: 2
Items: 
Size: 570881 Color: 10
Size: 428878 Color: 5

Bin 3580: 242 of cap free
Amount of items: 2
Items: 
Size: 740229 Color: 6
Size: 259530 Color: 11

Bin 3581: 243 of cap free
Amount of items: 2
Items: 
Size: 742990 Color: 15
Size: 256768 Color: 6

Bin 3582: 245 of cap free
Amount of items: 2
Items: 
Size: 689660 Color: 2
Size: 310096 Color: 17

Bin 3583: 245 of cap free
Amount of items: 2
Items: 
Size: 775675 Color: 11
Size: 224081 Color: 10

Bin 3584: 245 of cap free
Amount of items: 2
Items: 
Size: 589634 Color: 6
Size: 410122 Color: 18

Bin 3585: 245 of cap free
Amount of items: 2
Items: 
Size: 525797 Color: 14
Size: 473959 Color: 18

Bin 3586: 246 of cap free
Amount of items: 2
Items: 
Size: 657122 Color: 4
Size: 342633 Color: 17

Bin 3587: 246 of cap free
Amount of items: 2
Items: 
Size: 562842 Color: 13
Size: 436913 Color: 12

Bin 3588: 247 of cap free
Amount of items: 2
Items: 
Size: 541586 Color: 0
Size: 458168 Color: 4

Bin 3589: 247 of cap free
Amount of items: 2
Items: 
Size: 556869 Color: 5
Size: 442885 Color: 4

Bin 3590: 248 of cap free
Amount of items: 2
Items: 
Size: 589021 Color: 2
Size: 410732 Color: 4

Bin 3591: 248 of cap free
Amount of items: 2
Items: 
Size: 621584 Color: 12
Size: 378169 Color: 18

Bin 3592: 249 of cap free
Amount of items: 2
Items: 
Size: 647093 Color: 7
Size: 352659 Color: 3

Bin 3593: 250 of cap free
Amount of items: 2
Items: 
Size: 719502 Color: 13
Size: 280249 Color: 0

Bin 3594: 250 of cap free
Amount of items: 2
Items: 
Size: 714202 Color: 1
Size: 285549 Color: 6

Bin 3595: 250 of cap free
Amount of items: 2
Items: 
Size: 602052 Color: 13
Size: 397699 Color: 6

Bin 3596: 251 of cap free
Amount of items: 2
Items: 
Size: 773361 Color: 16
Size: 226389 Color: 13

Bin 3597: 252 of cap free
Amount of items: 2
Items: 
Size: 674899 Color: 5
Size: 324850 Color: 14

Bin 3598: 252 of cap free
Amount of items: 2
Items: 
Size: 547635 Color: 17
Size: 452114 Color: 0

Bin 3599: 252 of cap free
Amount of items: 2
Items: 
Size: 783498 Color: 15
Size: 216251 Color: 10

Bin 3600: 252 of cap free
Amount of items: 2
Items: 
Size: 503345 Color: 19
Size: 496404 Color: 0

Bin 3601: 252 of cap free
Amount of items: 2
Items: 
Size: 586313 Color: 9
Size: 413436 Color: 11

Bin 3602: 252 of cap free
Amount of items: 2
Items: 
Size: 790228 Color: 13
Size: 209521 Color: 3

Bin 3603: 253 of cap free
Amount of items: 2
Items: 
Size: 534449 Color: 14
Size: 465299 Color: 1

Bin 3604: 253 of cap free
Amount of items: 2
Items: 
Size: 773872 Color: 6
Size: 225876 Color: 5

Bin 3605: 253 of cap free
Amount of items: 2
Items: 
Size: 709108 Color: 5
Size: 290640 Color: 16

Bin 3606: 254 of cap free
Amount of items: 2
Items: 
Size: 686300 Color: 3
Size: 313447 Color: 2

Bin 3607: 255 of cap free
Amount of items: 2
Items: 
Size: 683826 Color: 18
Size: 315920 Color: 4

Bin 3608: 256 of cap free
Amount of items: 2
Items: 
Size: 506433 Color: 1
Size: 493312 Color: 17

Bin 3609: 257 of cap free
Amount of items: 2
Items: 
Size: 541576 Color: 5
Size: 458168 Color: 18

Bin 3610: 257 of cap free
Amount of items: 2
Items: 
Size: 548937 Color: 0
Size: 450807 Color: 6

Bin 3611: 257 of cap free
Amount of items: 2
Items: 
Size: 575706 Color: 4
Size: 424038 Color: 7

Bin 3612: 257 of cap free
Amount of items: 2
Items: 
Size: 647083 Color: 5
Size: 352661 Color: 7

Bin 3613: 258 of cap free
Amount of items: 2
Items: 
Size: 513642 Color: 11
Size: 486101 Color: 10

Bin 3614: 259 of cap free
Amount of items: 2
Items: 
Size: 617266 Color: 7
Size: 382476 Color: 0

Bin 3615: 259 of cap free
Amount of items: 2
Items: 
Size: 582638 Color: 0
Size: 417104 Color: 12

Bin 3616: 259 of cap free
Amount of items: 2
Items: 
Size: 679564 Color: 17
Size: 320178 Color: 1

Bin 3617: 260 of cap free
Amount of items: 2
Items: 
Size: 702772 Color: 8
Size: 296969 Color: 1

Bin 3618: 260 of cap free
Amount of items: 2
Items: 
Size: 772612 Color: 3
Size: 227129 Color: 0

Bin 3619: 260 of cap free
Amount of items: 2
Items: 
Size: 520987 Color: 14
Size: 478754 Color: 10

Bin 3620: 261 of cap free
Amount of items: 2
Items: 
Size: 738035 Color: 19
Size: 261705 Color: 3

Bin 3621: 262 of cap free
Amount of items: 2
Items: 
Size: 617830 Color: 8
Size: 381909 Color: 13

Bin 3622: 262 of cap free
Amount of items: 2
Items: 
Size: 797281 Color: 9
Size: 202458 Color: 18

Bin 3623: 262 of cap free
Amount of items: 2
Items: 
Size: 721245 Color: 8
Size: 278494 Color: 13

Bin 3624: 262 of cap free
Amount of items: 2
Items: 
Size: 646275 Color: 13
Size: 353464 Color: 15

Bin 3625: 262 of cap free
Amount of items: 2
Items: 
Size: 558114 Color: 14
Size: 441625 Color: 15

Bin 3626: 263 of cap free
Amount of items: 2
Items: 
Size: 573079 Color: 15
Size: 426659 Color: 11

Bin 3627: 263 of cap free
Amount of items: 2
Items: 
Size: 594977 Color: 4
Size: 404761 Color: 5

Bin 3628: 264 of cap free
Amount of items: 2
Items: 
Size: 765388 Color: 14
Size: 234349 Color: 6

Bin 3629: 265 of cap free
Amount of items: 2
Items: 
Size: 521460 Color: 5
Size: 478276 Color: 3

Bin 3630: 265 of cap free
Amount of items: 2
Items: 
Size: 678565 Color: 2
Size: 321171 Color: 10

Bin 3631: 265 of cap free
Amount of items: 2
Items: 
Size: 520637 Color: 17
Size: 479099 Color: 14

Bin 3632: 266 of cap free
Amount of items: 2
Items: 
Size: 625355 Color: 13
Size: 374380 Color: 10

Bin 3633: 267 of cap free
Amount of items: 2
Items: 
Size: 670318 Color: 15
Size: 329416 Color: 16

Bin 3634: 267 of cap free
Amount of items: 2
Items: 
Size: 601415 Color: 15
Size: 398319 Color: 2

Bin 3635: 268 of cap free
Amount of items: 2
Items: 
Size: 761958 Color: 19
Size: 237775 Color: 8

Bin 3636: 268 of cap free
Amount of items: 2
Items: 
Size: 675453 Color: 9
Size: 324280 Color: 17

Bin 3637: 268 of cap free
Amount of items: 2
Items: 
Size: 579072 Color: 1
Size: 420661 Color: 12

Bin 3638: 268 of cap free
Amount of items: 2
Items: 
Size: 714195 Color: 17
Size: 285538 Color: 18

Bin 3639: 269 of cap free
Amount of items: 3
Items: 
Size: 700983 Color: 18
Size: 149728 Color: 6
Size: 149021 Color: 0

Bin 3640: 269 of cap free
Amount of items: 2
Items: 
Size: 651981 Color: 12
Size: 347751 Color: 0

Bin 3641: 270 of cap free
Amount of items: 2
Items: 
Size: 531878 Color: 12
Size: 467853 Color: 0

Bin 3642: 270 of cap free
Amount of items: 2
Items: 
Size: 685407 Color: 8
Size: 314324 Color: 19

Bin 3643: 271 of cap free
Amount of items: 2
Items: 
Size: 517183 Color: 15
Size: 482547 Color: 14

Bin 3644: 271 of cap free
Amount of items: 2
Items: 
Size: 546391 Color: 3
Size: 453339 Color: 10

Bin 3645: 272 of cap free
Amount of items: 2
Items: 
Size: 713630 Color: 18
Size: 286099 Color: 4

Bin 3646: 273 of cap free
Amount of items: 2
Items: 
Size: 501641 Color: 18
Size: 498087 Color: 11

Bin 3647: 274 of cap free
Amount of items: 2
Items: 
Size: 652283 Color: 15
Size: 347444 Color: 0

Bin 3648: 275 of cap free
Amount of items: 3
Items: 
Size: 710612 Color: 8
Size: 168300 Color: 6
Size: 120814 Color: 19

Bin 3649: 275 of cap free
Amount of items: 2
Items: 
Size: 519441 Color: 0
Size: 480285 Color: 2

Bin 3650: 275 of cap free
Amount of items: 2
Items: 
Size: 773370 Color: 13
Size: 226356 Color: 15

Bin 3651: 276 of cap free
Amount of items: 2
Items: 
Size: 574814 Color: 19
Size: 424911 Color: 4

Bin 3652: 277 of cap free
Amount of items: 2
Items: 
Size: 625113 Color: 16
Size: 374611 Color: 6

Bin 3653: 277 of cap free
Amount of items: 2
Items: 
Size: 538216 Color: 6
Size: 461508 Color: 13

Bin 3654: 277 of cap free
Amount of items: 2
Items: 
Size: 750990 Color: 15
Size: 248734 Color: 4

Bin 3655: 277 of cap free
Amount of items: 2
Items: 
Size: 554823 Color: 4
Size: 444901 Color: 19

Bin 3656: 277 of cap free
Amount of items: 2
Items: 
Size: 586904 Color: 11
Size: 412820 Color: 16

Bin 3657: 278 of cap free
Amount of items: 3
Items: 
Size: 701485 Color: 18
Size: 162580 Color: 18
Size: 135658 Color: 14

Bin 3658: 278 of cap free
Amount of items: 2
Items: 
Size: 787506 Color: 4
Size: 212217 Color: 6

Bin 3659: 279 of cap free
Amount of items: 2
Items: 
Size: 680673 Color: 17
Size: 319049 Color: 5

Bin 3660: 279 of cap free
Amount of items: 2
Items: 
Size: 764379 Color: 12
Size: 235343 Color: 1

Bin 3661: 279 of cap free
Amount of items: 2
Items: 
Size: 571851 Color: 7
Size: 427871 Color: 9

Bin 3662: 280 of cap free
Amount of items: 3
Items: 
Size: 534451 Color: 16
Size: 248960 Color: 6
Size: 216310 Color: 0

Bin 3663: 280 of cap free
Amount of items: 2
Items: 
Size: 667476 Color: 9
Size: 332245 Color: 14

Bin 3664: 280 of cap free
Amount of items: 2
Items: 
Size: 792979 Color: 15
Size: 206742 Color: 1

Bin 3665: 280 of cap free
Amount of items: 2
Items: 
Size: 612929 Color: 4
Size: 386792 Color: 11

Bin 3666: 283 of cap free
Amount of items: 2
Items: 
Size: 781270 Color: 19
Size: 218448 Color: 6

Bin 3667: 284 of cap free
Amount of items: 2
Items: 
Size: 591863 Color: 9
Size: 407854 Color: 14

Bin 3668: 285 of cap free
Amount of items: 2
Items: 
Size: 756422 Color: 13
Size: 243294 Color: 3

Bin 3669: 286 of cap free
Amount of items: 2
Items: 
Size: 735984 Color: 6
Size: 263731 Color: 13

Bin 3670: 286 of cap free
Amount of items: 2
Items: 
Size: 545749 Color: 10
Size: 453966 Color: 14

Bin 3671: 287 of cap free
Amount of items: 2
Items: 
Size: 696388 Color: 15
Size: 303326 Color: 8

Bin 3672: 288 of cap free
Amount of items: 2
Items: 
Size: 671409 Color: 2
Size: 328304 Color: 15

Bin 3673: 288 of cap free
Amount of items: 2
Items: 
Size: 533019 Color: 14
Size: 466694 Color: 3

Bin 3674: 288 of cap free
Amount of items: 2
Items: 
Size: 540273 Color: 14
Size: 459440 Color: 9

Bin 3675: 289 of cap free
Amount of items: 2
Items: 
Size: 771224 Color: 2
Size: 228488 Color: 5

Bin 3676: 289 of cap free
Amount of items: 2
Items: 
Size: 769791 Color: 13
Size: 229921 Color: 5

Bin 3677: 289 of cap free
Amount of items: 2
Items: 
Size: 507333 Color: 2
Size: 492379 Color: 16

Bin 3678: 290 of cap free
Amount of items: 3
Items: 
Size: 604579 Color: 19
Size: 292735 Color: 3
Size: 102397 Color: 11

Bin 3679: 290 of cap free
Amount of items: 2
Items: 
Size: 613041 Color: 11
Size: 386670 Color: 1

Bin 3680: 293 of cap free
Amount of items: 2
Items: 
Size: 533367 Color: 17
Size: 466341 Color: 11

Bin 3681: 294 of cap free
Amount of items: 2
Items: 
Size: 733353 Color: 8
Size: 266354 Color: 18

Bin 3682: 294 of cap free
Amount of items: 2
Items: 
Size: 522832 Color: 10
Size: 476875 Color: 4

Bin 3683: 295 of cap free
Amount of items: 2
Items: 
Size: 601409 Color: 5
Size: 398297 Color: 2

Bin 3684: 296 of cap free
Amount of items: 2
Items: 
Size: 528961 Color: 1
Size: 470744 Color: 14

Bin 3685: 297 of cap free
Amount of items: 2
Items: 
Size: 679207 Color: 7
Size: 320497 Color: 4

Bin 3686: 298 of cap free
Amount of items: 2
Items: 
Size: 693954 Color: 15
Size: 305749 Color: 11

Bin 3687: 298 of cap free
Amount of items: 2
Items: 
Size: 758016 Color: 13
Size: 241687 Color: 1

Bin 3688: 298 of cap free
Amount of items: 2
Items: 
Size: 742539 Color: 18
Size: 257164 Color: 19

Bin 3689: 298 of cap free
Amount of items: 2
Items: 
Size: 510542 Color: 0
Size: 489161 Color: 6

Bin 3690: 299 of cap free
Amount of items: 2
Items: 
Size: 677561 Color: 6
Size: 322141 Color: 7

Bin 3691: 299 of cap free
Amount of items: 2
Items: 
Size: 730208 Color: 1
Size: 269494 Color: 15

Bin 3692: 299 of cap free
Amount of items: 2
Items: 
Size: 726843 Color: 3
Size: 272859 Color: 5

Bin 3693: 299 of cap free
Amount of items: 2
Items: 
Size: 548719 Color: 9
Size: 450983 Color: 17

Bin 3694: 300 of cap free
Amount of items: 2
Items: 
Size: 562268 Color: 14
Size: 437433 Color: 3

Bin 3695: 301 of cap free
Amount of items: 2
Items: 
Size: 527865 Color: 0
Size: 471835 Color: 6

Bin 3696: 301 of cap free
Amount of items: 2
Items: 
Size: 779781 Color: 15
Size: 219919 Color: 18

Bin 3697: 301 of cap free
Amount of items: 2
Items: 
Size: 515742 Color: 11
Size: 483958 Color: 13

Bin 3698: 302 of cap free
Amount of items: 2
Items: 
Size: 785297 Color: 7
Size: 214402 Color: 5

Bin 3699: 302 of cap free
Amount of items: 2
Items: 
Size: 770361 Color: 13
Size: 229338 Color: 16

Bin 3700: 302 of cap free
Amount of items: 2
Items: 
Size: 727991 Color: 6
Size: 271708 Color: 8

Bin 3701: 302 of cap free
Amount of items: 2
Items: 
Size: 765791 Color: 0
Size: 233908 Color: 1

Bin 3702: 302 of cap free
Amount of items: 2
Items: 
Size: 593062 Color: 16
Size: 406637 Color: 17

Bin 3703: 303 of cap free
Amount of items: 2
Items: 
Size: 531374 Color: 3
Size: 468324 Color: 10

Bin 3704: 303 of cap free
Amount of items: 2
Items: 
Size: 777325 Color: 11
Size: 222373 Color: 8

Bin 3705: 304 of cap free
Amount of items: 2
Items: 
Size: 665762 Color: 14
Size: 333935 Color: 4

Bin 3706: 304 of cap free
Amount of items: 2
Items: 
Size: 671931 Color: 19
Size: 327766 Color: 14

Bin 3707: 305 of cap free
Amount of items: 2
Items: 
Size: 685390 Color: 7
Size: 314306 Color: 14

Bin 3708: 305 of cap free
Amount of items: 2
Items: 
Size: 589575 Color: 1
Size: 410121 Color: 12

Bin 3709: 306 of cap free
Amount of items: 2
Items: 
Size: 690003 Color: 9
Size: 309692 Color: 10

Bin 3710: 306 of cap free
Amount of items: 2
Items: 
Size: 616356 Color: 9
Size: 383339 Color: 3

Bin 3711: 307 of cap free
Amount of items: 2
Items: 
Size: 622982 Color: 16
Size: 376712 Color: 12

Bin 3712: 307 of cap free
Amount of items: 2
Items: 
Size: 755896 Color: 17
Size: 243798 Color: 7

Bin 3713: 308 of cap free
Amount of items: 2
Items: 
Size: 551776 Color: 3
Size: 447917 Color: 17

Bin 3714: 308 of cap free
Amount of items: 2
Items: 
Size: 564821 Color: 18
Size: 434872 Color: 8

Bin 3715: 308 of cap free
Amount of items: 2
Items: 
Size: 570822 Color: 8
Size: 428871 Color: 1

Bin 3716: 308 of cap free
Amount of items: 2
Items: 
Size: 778005 Color: 8
Size: 221688 Color: 18

Bin 3717: 310 of cap free
Amount of items: 2
Items: 
Size: 623136 Color: 0
Size: 376555 Color: 6

Bin 3718: 311 of cap free
Amount of items: 2
Items: 
Size: 693201 Color: 18
Size: 306489 Color: 10

Bin 3719: 311 of cap free
Amount of items: 2
Items: 
Size: 596051 Color: 19
Size: 403639 Color: 10

Bin 3720: 311 of cap free
Amount of items: 2
Items: 
Size: 560689 Color: 8
Size: 439001 Color: 11

Bin 3721: 312 of cap free
Amount of items: 2
Items: 
Size: 576053 Color: 7
Size: 423636 Color: 0

Bin 3722: 312 of cap free
Amount of items: 2
Items: 
Size: 686756 Color: 16
Size: 312933 Color: 4

Bin 3723: 313 of cap free
Amount of items: 2
Items: 
Size: 621311 Color: 5
Size: 378377 Color: 12

Bin 3724: 313 of cap free
Amount of items: 3
Items: 
Size: 709579 Color: 0
Size: 158367 Color: 4
Size: 131742 Color: 3

Bin 3725: 313 of cap free
Amount of items: 2
Items: 
Size: 578405 Color: 3
Size: 421283 Color: 4

Bin 3726: 314 of cap free
Amount of items: 2
Items: 
Size: 704119 Color: 5
Size: 295568 Color: 13

Bin 3727: 314 of cap free
Amount of items: 2
Items: 
Size: 670338 Color: 16
Size: 329349 Color: 3

Bin 3728: 314 of cap free
Amount of items: 2
Items: 
Size: 762269 Color: 6
Size: 237418 Color: 4

Bin 3729: 314 of cap free
Amount of items: 2
Items: 
Size: 546373 Color: 3
Size: 453314 Color: 17

Bin 3730: 317 of cap free
Amount of items: 2
Items: 
Size: 650064 Color: 9
Size: 349620 Color: 0

Bin 3731: 318 of cap free
Amount of items: 2
Items: 
Size: 772604 Color: 1
Size: 227079 Color: 18

Bin 3732: 318 of cap free
Amount of items: 2
Items: 
Size: 614433 Color: 1
Size: 385250 Color: 12

Bin 3733: 318 of cap free
Amount of items: 2
Items: 
Size: 581427 Color: 2
Size: 418256 Color: 10

Bin 3734: 319 of cap free
Amount of items: 2
Items: 
Size: 608918 Color: 19
Size: 390764 Color: 17

Bin 3735: 320 of cap free
Amount of items: 2
Items: 
Size: 715504 Color: 8
Size: 284177 Color: 2

Bin 3736: 321 of cap free
Amount of items: 2
Items: 
Size: 624160 Color: 9
Size: 375520 Color: 16

Bin 3737: 321 of cap free
Amount of items: 2
Items: 
Size: 595441 Color: 17
Size: 404239 Color: 2

Bin 3738: 322 of cap free
Amount of items: 2
Items: 
Size: 534336 Color: 6
Size: 465343 Color: 4

Bin 3739: 322 of cap free
Amount of items: 2
Items: 
Size: 608127 Color: 5
Size: 391552 Color: 4

Bin 3740: 322 of cap free
Amount of items: 2
Items: 
Size: 552295 Color: 13
Size: 447384 Color: 8

Bin 3741: 322 of cap free
Amount of items: 2
Items: 
Size: 592232 Color: 0
Size: 407447 Color: 10

Bin 3742: 324 of cap free
Amount of items: 2
Items: 
Size: 789157 Color: 16
Size: 210520 Color: 4

Bin 3743: 324 of cap free
Amount of items: 2
Items: 
Size: 530521 Color: 0
Size: 469156 Color: 11

Bin 3744: 326 of cap free
Amount of items: 2
Items: 
Size: 591088 Color: 1
Size: 408587 Color: 18

Bin 3745: 329 of cap free
Amount of items: 2
Items: 
Size: 593502 Color: 10
Size: 406170 Color: 12

Bin 3746: 330 of cap free
Amount of items: 2
Items: 
Size: 726221 Color: 19
Size: 273450 Color: 2

Bin 3747: 330 of cap free
Amount of items: 2
Items: 
Size: 563955 Color: 11
Size: 435716 Color: 15

Bin 3748: 330 of cap free
Amount of items: 2
Items: 
Size: 569514 Color: 0
Size: 430157 Color: 6

Bin 3749: 331 of cap free
Amount of items: 2
Items: 
Size: 554232 Color: 3
Size: 445438 Color: 16

Bin 3750: 332 of cap free
Amount of items: 2
Items: 
Size: 648014 Color: 11
Size: 351655 Color: 14

Bin 3751: 333 of cap free
Amount of items: 3
Items: 
Size: 590157 Color: 15
Size: 207899 Color: 16
Size: 201612 Color: 15

Bin 3752: 333 of cap free
Amount of items: 2
Items: 
Size: 577494 Color: 18
Size: 422174 Color: 11

Bin 3753: 334 of cap free
Amount of items: 2
Items: 
Size: 790730 Color: 8
Size: 208937 Color: 13

Bin 3754: 334 of cap free
Amount of items: 2
Items: 
Size: 744822 Color: 4
Size: 254845 Color: 18

Bin 3755: 334 of cap free
Amount of items: 2
Items: 
Size: 537824 Color: 14
Size: 461843 Color: 1

Bin 3756: 335 of cap free
Amount of items: 2
Items: 
Size: 794401 Color: 16
Size: 205265 Color: 3

Bin 3757: 337 of cap free
Amount of items: 2
Items: 
Size: 656199 Color: 14
Size: 343465 Color: 6

Bin 3758: 337 of cap free
Amount of items: 2
Items: 
Size: 683563 Color: 3
Size: 316101 Color: 18

Bin 3759: 337 of cap free
Amount of items: 2
Items: 
Size: 585722 Color: 11
Size: 413942 Color: 2

Bin 3760: 337 of cap free
Amount of items: 2
Items: 
Size: 612379 Color: 15
Size: 387285 Color: 12

Bin 3761: 337 of cap free
Amount of items: 2
Items: 
Size: 558449 Color: 12
Size: 441215 Color: 2

Bin 3762: 337 of cap free
Amount of items: 2
Items: 
Size: 586900 Color: 2
Size: 412764 Color: 1

Bin 3763: 339 of cap free
Amount of items: 2
Items: 
Size: 793375 Color: 13
Size: 206287 Color: 7

Bin 3764: 340 of cap free
Amount of items: 2
Items: 
Size: 724637 Color: 16
Size: 275024 Color: 18

Bin 3765: 340 of cap free
Amount of items: 2
Items: 
Size: 755501 Color: 11
Size: 244160 Color: 1

Bin 3766: 341 of cap free
Amount of items: 2
Items: 
Size: 714673 Color: 7
Size: 284987 Color: 10

Bin 3767: 341 of cap free
Amount of items: 2
Items: 
Size: 529567 Color: 16
Size: 470093 Color: 18

Bin 3768: 341 of cap free
Amount of items: 3
Items: 
Size: 415225 Color: 14
Size: 302495 Color: 11
Size: 281940 Color: 4

Bin 3769: 342 of cap free
Amount of items: 2
Items: 
Size: 682073 Color: 15
Size: 317586 Color: 17

Bin 3770: 342 of cap free
Amount of items: 2
Items: 
Size: 784175 Color: 10
Size: 215484 Color: 16

Bin 3771: 342 of cap free
Amount of items: 2
Items: 
Size: 500966 Color: 0
Size: 498693 Color: 4

Bin 3772: 343 of cap free
Amount of items: 2
Items: 
Size: 754086 Color: 3
Size: 245572 Color: 11

Bin 3773: 344 of cap free
Amount of items: 2
Items: 
Size: 759153 Color: 1
Size: 240504 Color: 0

Bin 3774: 346 of cap free
Amount of items: 2
Items: 
Size: 535812 Color: 14
Size: 463843 Color: 19

Bin 3775: 346 of cap free
Amount of items: 2
Items: 
Size: 557198 Color: 5
Size: 442457 Color: 7

Bin 3776: 347 of cap free
Amount of items: 3
Items: 
Size: 689358 Color: 5
Size: 163790 Color: 8
Size: 146506 Color: 9

Bin 3777: 348 of cap free
Amount of items: 2
Items: 
Size: 625917 Color: 4
Size: 373736 Color: 17

Bin 3778: 349 of cap free
Amount of items: 2
Items: 
Size: 567945 Color: 11
Size: 431707 Color: 9

Bin 3779: 353 of cap free
Amount of items: 2
Items: 
Size: 585721 Color: 16
Size: 413927 Color: 13

Bin 3780: 353 of cap free
Amount of items: 2
Items: 
Size: 719566 Color: 6
Size: 280082 Color: 2

Bin 3781: 353 of cap free
Amount of items: 2
Items: 
Size: 666887 Color: 9
Size: 332761 Color: 0

Bin 3782: 353 of cap free
Amount of items: 2
Items: 
Size: 699259 Color: 17
Size: 300389 Color: 2

Bin 3783: 354 of cap free
Amount of items: 2
Items: 
Size: 535808 Color: 2
Size: 463839 Color: 5

Bin 3784: 358 of cap free
Amount of items: 2
Items: 
Size: 710515 Color: 19
Size: 289128 Color: 1

Bin 3785: 359 of cap free
Amount of items: 2
Items: 
Size: 657101 Color: 4
Size: 342541 Color: 3

Bin 3786: 359 of cap free
Amount of items: 2
Items: 
Size: 681564 Color: 3
Size: 318078 Color: 1

Bin 3787: 359 of cap free
Amount of items: 2
Items: 
Size: 707283 Color: 6
Size: 292359 Color: 7

Bin 3788: 359 of cap free
Amount of items: 2
Items: 
Size: 740120 Color: 10
Size: 259522 Color: 11

Bin 3789: 361 of cap free
Amount of items: 2
Items: 
Size: 760181 Color: 10
Size: 239459 Color: 4

Bin 3790: 364 of cap free
Amount of items: 2
Items: 
Size: 637487 Color: 13
Size: 362150 Color: 19

Bin 3791: 364 of cap free
Amount of items: 2
Items: 
Size: 573887 Color: 4
Size: 425750 Color: 3

Bin 3792: 365 of cap free
Amount of items: 2
Items: 
Size: 509214 Color: 14
Size: 490422 Color: 18

Bin 3793: 366 of cap free
Amount of items: 2
Items: 
Size: 703509 Color: 18
Size: 296126 Color: 3

Bin 3794: 366 of cap free
Amount of items: 2
Items: 
Size: 780655 Color: 7
Size: 218980 Color: 11

Bin 3795: 369 of cap free
Amount of items: 2
Items: 
Size: 705367 Color: 9
Size: 294265 Color: 2

Bin 3796: 370 of cap free
Amount of items: 2
Items: 
Size: 642981 Color: 16
Size: 356650 Color: 1

Bin 3797: 370 of cap free
Amount of items: 2
Items: 
Size: 526630 Color: 5
Size: 473001 Color: 4

Bin 3798: 371 of cap free
Amount of items: 2
Items: 
Size: 729618 Color: 16
Size: 270012 Color: 0

Bin 3799: 372 of cap free
Amount of items: 2
Items: 
Size: 774500 Color: 13
Size: 225129 Color: 14

Bin 3800: 372 of cap free
Amount of items: 2
Items: 
Size: 508478 Color: 19
Size: 491151 Color: 2

Bin 3801: 373 of cap free
Amount of items: 2
Items: 
Size: 607663 Color: 18
Size: 391965 Color: 19

Bin 3802: 373 of cap free
Amount of items: 2
Items: 
Size: 501574 Color: 4
Size: 498054 Color: 2

Bin 3803: 373 of cap free
Amount of items: 2
Items: 
Size: 685740 Color: 8
Size: 313888 Color: 16

Bin 3804: 374 of cap free
Amount of items: 2
Items: 
Size: 576729 Color: 13
Size: 422898 Color: 11

Bin 3805: 374 of cap free
Amount of items: 2
Items: 
Size: 583018 Color: 14
Size: 416609 Color: 5

Bin 3806: 375 of cap free
Amount of items: 2
Items: 
Size: 743651 Color: 12
Size: 255975 Color: 4

Bin 3807: 375 of cap free
Amount of items: 2
Items: 
Size: 691151 Color: 0
Size: 308475 Color: 12

Bin 3808: 375 of cap free
Amount of items: 2
Items: 
Size: 518825 Color: 4
Size: 480801 Color: 8

Bin 3809: 376 of cap free
Amount of items: 2
Items: 
Size: 779042 Color: 6
Size: 220583 Color: 1

Bin 3810: 376 of cap free
Amount of items: 2
Items: 
Size: 545677 Color: 4
Size: 453948 Color: 16

Bin 3811: 376 of cap free
Amount of items: 2
Items: 
Size: 563318 Color: 2
Size: 436307 Color: 12

Bin 3812: 378 of cap free
Amount of items: 2
Items: 
Size: 660132 Color: 8
Size: 339491 Color: 18

Bin 3813: 378 of cap free
Amount of items: 2
Items: 
Size: 782909 Color: 12
Size: 216714 Color: 3

Bin 3814: 379 of cap free
Amount of items: 2
Items: 
Size: 562209 Color: 2
Size: 437413 Color: 5

Bin 3815: 380 of cap free
Amount of items: 3
Items: 
Size: 531881 Color: 6
Size: 320115 Color: 11
Size: 147625 Color: 12

Bin 3816: 381 of cap free
Amount of items: 2
Items: 
Size: 565519 Color: 12
Size: 434101 Color: 9

Bin 3817: 382 of cap free
Amount of items: 2
Items: 
Size: 519393 Color: 11
Size: 480226 Color: 19

Bin 3818: 383 of cap free
Amount of items: 2
Items: 
Size: 785619 Color: 19
Size: 213999 Color: 7

Bin 3819: 384 of cap free
Amount of items: 2
Items: 
Size: 680666 Color: 5
Size: 318951 Color: 1

Bin 3820: 384 of cap free
Amount of items: 2
Items: 
Size: 502480 Color: 9
Size: 497137 Color: 11

Bin 3821: 385 of cap free
Amount of items: 2
Items: 
Size: 611765 Color: 0
Size: 387851 Color: 5

Bin 3822: 387 of cap free
Amount of items: 2
Items: 
Size: 735029 Color: 5
Size: 264585 Color: 13

Bin 3823: 387 of cap free
Amount of items: 2
Items: 
Size: 711498 Color: 8
Size: 288116 Color: 12

Bin 3824: 387 of cap free
Amount of items: 2
Items: 
Size: 707281 Color: 16
Size: 292333 Color: 0

Bin 3825: 390 of cap free
Amount of items: 2
Items: 
Size: 500042 Color: 16
Size: 499569 Color: 7

Bin 3826: 391 of cap free
Amount of items: 2
Items: 
Size: 687191 Color: 4
Size: 312419 Color: 7

Bin 3827: 392 of cap free
Amount of items: 2
Items: 
Size: 784141 Color: 4
Size: 215468 Color: 13

Bin 3828: 393 of cap free
Amount of items: 2
Items: 
Size: 696903 Color: 11
Size: 302705 Color: 14

Bin 3829: 393 of cap free
Amount of items: 2
Items: 
Size: 687203 Color: 7
Size: 312405 Color: 15

Bin 3830: 394 of cap free
Amount of items: 2
Items: 
Size: 789794 Color: 7
Size: 209813 Color: 2

Bin 3831: 395 of cap free
Amount of items: 2
Items: 
Size: 658912 Color: 14
Size: 340694 Color: 8

Bin 3832: 395 of cap free
Amount of items: 2
Items: 
Size: 599240 Color: 14
Size: 400366 Color: 18

Bin 3833: 396 of cap free
Amount of items: 2
Items: 
Size: 514196 Color: 11
Size: 485409 Color: 19

Bin 3834: 396 of cap free
Amount of items: 2
Items: 
Size: 561350 Color: 4
Size: 438255 Color: 11

Bin 3835: 398 of cap free
Amount of items: 2
Items: 
Size: 737827 Color: 13
Size: 261776 Color: 9

Bin 3836: 398 of cap free
Amount of items: 2
Items: 
Size: 573415 Color: 8
Size: 426188 Color: 14

Bin 3837: 399 of cap free
Amount of items: 2
Items: 
Size: 777236 Color: 3
Size: 222366 Color: 9

Bin 3838: 402 of cap free
Amount of items: 2
Items: 
Size: 529541 Color: 2
Size: 470058 Color: 18

Bin 3839: 402 of cap free
Amount of items: 2
Items: 
Size: 679430 Color: 6
Size: 320169 Color: 5

Bin 3840: 403 of cap free
Amount of items: 2
Items: 
Size: 521457 Color: 13
Size: 478141 Color: 12

Bin 3841: 404 of cap free
Amount of items: 2
Items: 
Size: 612907 Color: 16
Size: 386690 Color: 18

Bin 3842: 404 of cap free
Amount of items: 2
Items: 
Size: 685311 Color: 5
Size: 314286 Color: 10

Bin 3843: 406 of cap free
Amount of items: 3
Items: 
Size: 648032 Color: 2
Size: 217765 Color: 3
Size: 133798 Color: 13

Bin 3844: 406 of cap free
Amount of items: 2
Items: 
Size: 612310 Color: 0
Size: 387285 Color: 17

Bin 3845: 407 of cap free
Amount of items: 2
Items: 
Size: 753094 Color: 4
Size: 246500 Color: 1

Bin 3846: 407 of cap free
Amount of items: 2
Items: 
Size: 662392 Color: 15
Size: 337202 Color: 5

Bin 3847: 407 of cap free
Amount of items: 2
Items: 
Size: 501610 Color: 14
Size: 497984 Color: 17

Bin 3848: 408 of cap free
Amount of items: 2
Items: 
Size: 751986 Color: 17
Size: 247607 Color: 9

Bin 3849: 409 of cap free
Amount of items: 2
Items: 
Size: 597984 Color: 5
Size: 401608 Color: 7

Bin 3850: 409 of cap free
Amount of items: 2
Items: 
Size: 686738 Color: 16
Size: 312854 Color: 14

Bin 3851: 410 of cap free
Amount of items: 2
Items: 
Size: 726165 Color: 3
Size: 273426 Color: 1

Bin 3852: 410 of cap free
Amount of items: 2
Items: 
Size: 756827 Color: 8
Size: 242764 Color: 17

Bin 3853: 410 of cap free
Amount of items: 2
Items: 
Size: 667859 Color: 18
Size: 331732 Color: 5

Bin 3854: 411 of cap free
Amount of items: 2
Items: 
Size: 567920 Color: 3
Size: 431670 Color: 8

Bin 3855: 412 of cap free
Amount of items: 2
Items: 
Size: 509019 Color: 12
Size: 490570 Color: 14

Bin 3856: 412 of cap free
Amount of items: 2
Items: 
Size: 654539 Color: 1
Size: 345050 Color: 4

Bin 3857: 413 of cap free
Amount of items: 2
Items: 
Size: 747717 Color: 7
Size: 251871 Color: 19

Bin 3858: 413 of cap free
Amount of items: 2
Items: 
Size: 605860 Color: 2
Size: 393728 Color: 6

Bin 3859: 413 of cap free
Amount of items: 2
Items: 
Size: 505732 Color: 15
Size: 493856 Color: 14

Bin 3860: 414 of cap free
Amount of items: 2
Items: 
Size: 690503 Color: 13
Size: 309084 Color: 6

Bin 3861: 414 of cap free
Amount of items: 2
Items: 
Size: 555529 Color: 10
Size: 444058 Color: 12

Bin 3862: 415 of cap free
Amount of items: 2
Items: 
Size: 644542 Color: 19
Size: 355044 Color: 17

Bin 3863: 416 of cap free
Amount of items: 2
Items: 
Size: 657615 Color: 13
Size: 341970 Color: 19

Bin 3864: 416 of cap free
Amount of items: 2
Items: 
Size: 754997 Color: 18
Size: 244588 Color: 4

Bin 3865: 417 of cap free
Amount of items: 2
Items: 
Size: 765783 Color: 0
Size: 233801 Color: 3

Bin 3866: 417 of cap free
Amount of items: 2
Items: 
Size: 554227 Color: 4
Size: 445357 Color: 7

Bin 3867: 417 of cap free
Amount of items: 2
Items: 
Size: 566670 Color: 10
Size: 432914 Color: 16

Bin 3868: 418 of cap free
Amount of items: 2
Items: 
Size: 652571 Color: 3
Size: 347012 Color: 14

Bin 3869: 418 of cap free
Amount of items: 2
Items: 
Size: 749857 Color: 16
Size: 249726 Color: 13

Bin 3870: 419 of cap free
Amount of items: 2
Items: 
Size: 521423 Color: 6
Size: 478159 Color: 5

Bin 3871: 419 of cap free
Amount of items: 2
Items: 
Size: 682028 Color: 3
Size: 317554 Color: 15

Bin 3872: 420 of cap free
Amount of items: 2
Items: 
Size: 616348 Color: 12
Size: 383233 Color: 7

Bin 3873: 420 of cap free
Amount of items: 2
Items: 
Size: 681507 Color: 13
Size: 318074 Color: 2

Bin 3874: 421 of cap free
Amount of items: 2
Items: 
Size: 715924 Color: 13
Size: 283656 Color: 5

Bin 3875: 421 of cap free
Amount of items: 2
Items: 
Size: 560607 Color: 4
Size: 438973 Color: 11

Bin 3876: 421 of cap free
Amount of items: 2
Items: 
Size: 563916 Color: 7
Size: 435664 Color: 0

Bin 3877: 423 of cap free
Amount of items: 2
Items: 
Size: 711459 Color: 15
Size: 288119 Color: 10

Bin 3878: 425 of cap free
Amount of items: 2
Items: 
Size: 773682 Color: 4
Size: 225894 Color: 6

Bin 3879: 428 of cap free
Amount of items: 2
Items: 
Size: 610234 Color: 4
Size: 389339 Color: 15

Bin 3880: 429 of cap free
Amount of items: 2
Items: 
Size: 550702 Color: 10
Size: 448870 Color: 8

Bin 3881: 430 of cap free
Amount of items: 2
Items: 
Size: 515726 Color: 8
Size: 483845 Color: 16

Bin 3882: 431 of cap free
Amount of items: 2
Items: 
Size: 612303 Color: 11
Size: 387267 Color: 8

Bin 3883: 431 of cap free
Amount of items: 2
Items: 
Size: 569293 Color: 17
Size: 430277 Color: 0

Bin 3884: 433 of cap free
Amount of items: 2
Items: 
Size: 658887 Color: 7
Size: 340681 Color: 16

Bin 3885: 434 of cap free
Amount of items: 3
Items: 
Size: 553842 Color: 1
Size: 290605 Color: 19
Size: 155120 Color: 17

Bin 3886: 435 of cap free
Amount of items: 2
Items: 
Size: 672248 Color: 10
Size: 327318 Color: 5

Bin 3887: 437 of cap free
Amount of items: 2
Items: 
Size: 740102 Color: 4
Size: 259462 Color: 9

Bin 3888: 438 of cap free
Amount of items: 3
Items: 
Size: 531676 Color: 2
Size: 258915 Color: 0
Size: 208972 Color: 13

Bin 3889: 438 of cap free
Amount of items: 2
Items: 
Size: 543326 Color: 8
Size: 456237 Color: 7

Bin 3890: 440 of cap free
Amount of items: 2
Items: 
Size: 705822 Color: 15
Size: 293739 Color: 1

Bin 3891: 440 of cap free
Amount of items: 2
Items: 
Size: 687192 Color: 7
Size: 312369 Color: 4

Bin 3892: 440 of cap free
Amount of items: 2
Items: 
Size: 578314 Color: 10
Size: 421247 Color: 1

Bin 3893: 440 of cap free
Amount of items: 2
Items: 
Size: 636097 Color: 10
Size: 363464 Color: 8

Bin 3894: 443 of cap free
Amount of items: 2
Items: 
Size: 619312 Color: 11
Size: 380246 Color: 18

Bin 3895: 444 of cap free
Amount of items: 2
Items: 
Size: 753063 Color: 6
Size: 246494 Color: 18

Bin 3896: 444 of cap free
Amount of items: 2
Items: 
Size: 651005 Color: 13
Size: 348552 Color: 0

Bin 3897: 447 of cap free
Amount of items: 2
Items: 
Size: 538273 Color: 1
Size: 461281 Color: 11

Bin 3898: 452 of cap free
Amount of items: 2
Items: 
Size: 754640 Color: 4
Size: 244909 Color: 8

Bin 3899: 452 of cap free
Amount of items: 2
Items: 
Size: 507209 Color: 6
Size: 492340 Color: 17

Bin 3900: 453 of cap free
Amount of items: 2
Items: 
Size: 715440 Color: 9
Size: 284108 Color: 1

Bin 3901: 457 of cap free
Amount of items: 2
Items: 
Size: 587984 Color: 16
Size: 411560 Color: 6

Bin 3902: 457 of cap free
Amount of items: 2
Items: 
Size: 609309 Color: 12
Size: 390235 Color: 6

Bin 3903: 458 of cap free
Amount of items: 2
Items: 
Size: 616200 Color: 2
Size: 383343 Color: 19

Bin 3904: 458 of cap free
Amount of items: 2
Items: 
Size: 797091 Color: 9
Size: 202452 Color: 15

Bin 3905: 458 of cap free
Amount of items: 2
Items: 
Size: 759126 Color: 4
Size: 240417 Color: 3

Bin 3906: 459 of cap free
Amount of items: 2
Items: 
Size: 656191 Color: 6
Size: 343351 Color: 16

Bin 3907: 460 of cap free
Amount of items: 2
Items: 
Size: 769713 Color: 7
Size: 229828 Color: 16

Bin 3908: 460 of cap free
Amount of items: 2
Items: 
Size: 633961 Color: 4
Size: 365580 Color: 12

Bin 3909: 462 of cap free
Amount of items: 2
Items: 
Size: 641972 Color: 1
Size: 357567 Color: 12

Bin 3910: 462 of cap free
Amount of items: 2
Items: 
Size: 680189 Color: 14
Size: 319350 Color: 19

Bin 3911: 463 of cap free
Amount of items: 2
Items: 
Size: 744810 Color: 8
Size: 254728 Color: 17

Bin 3912: 465 of cap free
Amount of items: 2
Items: 
Size: 734991 Color: 7
Size: 264545 Color: 12

Bin 3913: 467 of cap free
Amount of items: 2
Items: 
Size: 700279 Color: 11
Size: 299255 Color: 12

Bin 3914: 468 of cap free
Amount of items: 2
Items: 
Size: 719442 Color: 9
Size: 280091 Color: 8

Bin 3915: 468 of cap free
Amount of items: 2
Items: 
Size: 520746 Color: 18
Size: 478787 Color: 4

Bin 3916: 468 of cap free
Amount of items: 2
Items: 
Size: 527690 Color: 1
Size: 471843 Color: 8

Bin 3917: 470 of cap free
Amount of items: 2
Items: 
Size: 777176 Color: 14
Size: 222355 Color: 9

Bin 3918: 470 of cap free
Amount of items: 2
Items: 
Size: 763433 Color: 3
Size: 236098 Color: 14

Bin 3919: 473 of cap free
Amount of items: 2
Items: 
Size: 625889 Color: 3
Size: 373639 Color: 14

Bin 3920: 474 of cap free
Amount of items: 2
Items: 
Size: 602848 Color: 8
Size: 396679 Color: 11

Bin 3921: 475 of cap free
Amount of items: 2
Items: 
Size: 531120 Color: 0
Size: 468406 Color: 6

Bin 3922: 475 of cap free
Amount of items: 2
Items: 
Size: 657591 Color: 14
Size: 341935 Color: 18

Bin 3923: 478 of cap free
Amount of items: 2
Items: 
Size: 701762 Color: 17
Size: 297761 Color: 4

Bin 3924: 478 of cap free
Amount of items: 2
Items: 
Size: 627436 Color: 18
Size: 372087 Color: 2

Bin 3925: 481 of cap free
Amount of items: 2
Items: 
Size: 739032 Color: 12
Size: 260488 Color: 1

Bin 3926: 482 of cap free
Amount of items: 2
Items: 
Size: 523252 Color: 3
Size: 476267 Color: 2

Bin 3927: 483 of cap free
Amount of items: 2
Items: 
Size: 536626 Color: 17
Size: 462892 Color: 18

Bin 3928: 484 of cap free
Amount of items: 2
Items: 
Size: 567899 Color: 16
Size: 431618 Color: 1

Bin 3929: 484 of cap free
Amount of items: 2
Items: 
Size: 568534 Color: 17
Size: 430983 Color: 11

Bin 3930: 484 of cap free
Amount of items: 2
Items: 
Size: 786783 Color: 16
Size: 212734 Color: 15

Bin 3931: 485 of cap free
Amount of items: 2
Items: 
Size: 671230 Color: 4
Size: 328286 Color: 0

Bin 3932: 485 of cap free
Amount of items: 2
Items: 
Size: 628222 Color: 11
Size: 371294 Color: 9

Bin 3933: 486 of cap free
Amount of items: 2
Items: 
Size: 687738 Color: 3
Size: 311777 Color: 6

Bin 3934: 486 of cap free
Amount of items: 3
Items: 
Size: 740512 Color: 19
Size: 129538 Color: 18
Size: 129465 Color: 4

Bin 3935: 487 of cap free
Amount of items: 3
Items: 
Size: 608206 Color: 5
Size: 277529 Color: 6
Size: 113779 Color: 4

Bin 3936: 488 of cap free
Amount of items: 2
Items: 
Size: 691777 Color: 1
Size: 307736 Color: 8

Bin 3937: 490 of cap free
Amount of items: 2
Items: 
Size: 793815 Color: 15
Size: 205696 Color: 13

Bin 3938: 490 of cap free
Amount of items: 2
Items: 
Size: 573394 Color: 16
Size: 426117 Color: 1

Bin 3939: 492 of cap free
Amount of items: 2
Items: 
Size: 680163 Color: 6
Size: 319346 Color: 7

Bin 3940: 493 of cap free
Amount of items: 2
Items: 
Size: 632530 Color: 14
Size: 366978 Color: 2

Bin 3941: 493 of cap free
Amount of items: 2
Items: 
Size: 614264 Color: 16
Size: 385244 Color: 9

Bin 3942: 494 of cap free
Amount of items: 2
Items: 
Size: 663088 Color: 18
Size: 336419 Color: 9

Bin 3943: 496 of cap free
Amount of items: 2
Items: 
Size: 547527 Color: 2
Size: 451978 Color: 8

Bin 3944: 497 of cap free
Amount of items: 2
Items: 
Size: 504225 Color: 14
Size: 495279 Color: 3

Bin 3945: 500 of cap free
Amount of items: 2
Items: 
Size: 500849 Color: 1
Size: 498652 Color: 4

Bin 3946: 500 of cap free
Amount of items: 2
Items: 
Size: 515074 Color: 6
Size: 484427 Color: 19

Bin 3947: 501 of cap free
Amount of items: 2
Items: 
Size: 760306 Color: 15
Size: 239194 Color: 7

Bin 3948: 502 of cap free
Amount of items: 2
Items: 
Size: 773651 Color: 9
Size: 225848 Color: 11

Bin 3949: 503 of cap free
Amount of items: 2
Items: 
Size: 650971 Color: 8
Size: 348527 Color: 15

Bin 3950: 505 of cap free
Amount of items: 2
Items: 
Size: 552254 Color: 15
Size: 447242 Color: 7

Bin 3951: 506 of cap free
Amount of items: 2
Items: 
Size: 695739 Color: 18
Size: 303756 Color: 19

Bin 3952: 506 of cap free
Amount of items: 2
Items: 
Size: 724116 Color: 17
Size: 275379 Color: 9

Bin 3953: 507 of cap free
Amount of items: 2
Items: 
Size: 670142 Color: 1
Size: 329352 Color: 16

Bin 3954: 511 of cap free
Amount of items: 2
Items: 
Size: 617171 Color: 16
Size: 382319 Color: 6

Bin 3955: 512 of cap free
Amount of items: 2
Items: 
Size: 635220 Color: 17
Size: 364269 Color: 15

Bin 3956: 514 of cap free
Amount of items: 2
Items: 
Size: 608119 Color: 6
Size: 391368 Color: 10

Bin 3957: 514 of cap free
Amount of items: 2
Items: 
Size: 540071 Color: 17
Size: 459416 Color: 5

Bin 3958: 517 of cap free
Amount of items: 2
Items: 
Size: 589553 Color: 10
Size: 409931 Color: 12

Bin 3959: 518 of cap free
Amount of items: 3
Items: 
Size: 644759 Color: 19
Size: 177373 Color: 19
Size: 177351 Color: 5

Bin 3960: 518 of cap free
Amount of items: 2
Items: 
Size: 534425 Color: 15
Size: 465058 Color: 4

Bin 3961: 519 of cap free
Amount of items: 2
Items: 
Size: 741434 Color: 11
Size: 258048 Color: 9

Bin 3962: 519 of cap free
Amount of items: 2
Items: 
Size: 568507 Color: 1
Size: 430975 Color: 7

Bin 3963: 521 of cap free
Amount of items: 2
Items: 
Size: 725421 Color: 6
Size: 274059 Color: 15

Bin 3964: 522 of cap free
Amount of items: 2
Items: 
Size: 644426 Color: 9
Size: 355053 Color: 14

Bin 3965: 523 of cap free
Amount of items: 3
Items: 
Size: 614078 Color: 7
Size: 272837 Color: 7
Size: 112563 Color: 14

Bin 3966: 524 of cap free
Amount of items: 2
Items: 
Size: 685739 Color: 5
Size: 313738 Color: 8

Bin 3967: 526 of cap free
Amount of items: 2
Items: 
Size: 530422 Color: 9
Size: 469053 Color: 4

Bin 3968: 527 of cap free
Amount of items: 2
Items: 
Size: 782846 Color: 8
Size: 216628 Color: 12

Bin 3969: 529 of cap free
Amount of items: 4
Items: 
Size: 338052 Color: 7
Size: 327087 Color: 6
Size: 187418 Color: 17
Size: 146915 Color: 16

Bin 3970: 529 of cap free
Amount of items: 2
Items: 
Size: 567889 Color: 19
Size: 431583 Color: 18

Bin 3971: 530 of cap free
Amount of items: 2
Items: 
Size: 794309 Color: 16
Size: 205162 Color: 12

Bin 3972: 530 of cap free
Amount of items: 2
Items: 
Size: 674801 Color: 16
Size: 324670 Color: 3

Bin 3973: 532 of cap free
Amount of items: 2
Items: 
Size: 663077 Color: 16
Size: 336392 Color: 5

Bin 3974: 533 of cap free
Amount of items: 2
Items: 
Size: 678460 Color: 4
Size: 321008 Color: 16

Bin 3975: 535 of cap free
Amount of items: 2
Items: 
Size: 779678 Color: 5
Size: 219788 Color: 8

Bin 3976: 536 of cap free
Amount of items: 3
Items: 
Size: 654055 Color: 16
Size: 189629 Color: 6
Size: 155781 Color: 19

Bin 3977: 536 of cap free
Amount of items: 2
Items: 
Size: 543853 Color: 11
Size: 455612 Color: 14

Bin 3978: 536 of cap free
Amount of items: 2
Items: 
Size: 532450 Color: 16
Size: 467015 Color: 18

Bin 3979: 536 of cap free
Amount of items: 2
Items: 
Size: 722094 Color: 14
Size: 277371 Color: 4

Bin 3980: 540 of cap free
Amount of items: 2
Items: 
Size: 734967 Color: 17
Size: 264494 Color: 9

Bin 3981: 541 of cap free
Amount of items: 2
Items: 
Size: 777112 Color: 14
Size: 222348 Color: 12

Bin 3982: 543 of cap free
Amount of items: 2
Items: 
Size: 587899 Color: 10
Size: 411559 Color: 7

Bin 3983: 543 of cap free
Amount of items: 2
Items: 
Size: 562189 Color: 17
Size: 437269 Color: 8

Bin 3984: 546 of cap free
Amount of items: 2
Items: 
Size: 760168 Color: 5
Size: 239287 Color: 0

Bin 3985: 548 of cap free
Amount of items: 2
Items: 
Size: 570728 Color: 8
Size: 428725 Color: 1

Bin 3986: 554 of cap free
Amount of items: 2
Items: 
Size: 711431 Color: 13
Size: 288016 Color: 16

Bin 3987: 554 of cap free
Amount of items: 2
Items: 
Size: 518016 Color: 2
Size: 481431 Color: 18

Bin 3988: 554 of cap free
Amount of items: 3
Items: 
Size: 406524 Color: 12
Size: 340496 Color: 6
Size: 252427 Color: 11

Bin 3989: 556 of cap free
Amount of items: 2
Items: 
Size: 664899 Color: 5
Size: 334546 Color: 8

Bin 3990: 560 of cap free
Amount of items: 3
Items: 
Size: 450363 Color: 18
Size: 305962 Color: 4
Size: 243116 Color: 11

Bin 3991: 560 of cap free
Amount of items: 2
Items: 
Size: 591613 Color: 7
Size: 407828 Color: 13

Bin 3992: 561 of cap free
Amount of items: 2
Items: 
Size: 538318 Color: 17
Size: 461122 Color: 2

Bin 3993: 561 of cap free
Amount of items: 2
Items: 
Size: 750279 Color: 4
Size: 249161 Color: 10

Bin 3994: 563 of cap free
Amount of items: 2
Items: 
Size: 670109 Color: 14
Size: 329329 Color: 10

Bin 3995: 564 of cap free
Amount of items: 2
Items: 
Size: 791500 Color: 2
Size: 207937 Color: 13

Bin 3996: 564 of cap free
Amount of items: 2
Items: 
Size: 704008 Color: 16
Size: 295429 Color: 9

Bin 3997: 566 of cap free
Amount of items: 2
Items: 
Size: 524542 Color: 14
Size: 474893 Color: 9

Bin 3998: 566 of cap free
Amount of items: 2
Items: 
Size: 777105 Color: 19
Size: 222330 Color: 17

Bin 3999: 567 of cap free
Amount of items: 2
Items: 
Size: 523198 Color: 18
Size: 476236 Color: 12

Bin 4000: 567 of cap free
Amount of items: 2
Items: 
Size: 534208 Color: 8
Size: 465226 Color: 19

Bin 4001: 569 of cap free
Amount of items: 2
Items: 
Size: 761006 Color: 19
Size: 238426 Color: 15

Bin 4002: 569 of cap free
Amount of items: 2
Items: 
Size: 767116 Color: 5
Size: 232316 Color: 4

Bin 4003: 572 of cap free
Amount of items: 2
Items: 
Size: 625848 Color: 15
Size: 373581 Color: 19

Bin 4004: 577 of cap free
Amount of items: 2
Items: 
Size: 714031 Color: 0
Size: 285393 Color: 12

Bin 4005: 578 of cap free
Amount of items: 2
Items: 
Size: 751854 Color: 3
Size: 247569 Color: 4

Bin 4006: 578 of cap free
Amount of items: 2
Items: 
Size: 518112 Color: 3
Size: 481311 Color: 14

Bin 4007: 578 of cap free
Amount of items: 2
Items: 
Size: 567881 Color: 1
Size: 431542 Color: 12

Bin 4008: 581 of cap free
Amount of items: 2
Items: 
Size: 619191 Color: 8
Size: 380229 Color: 16

Bin 4009: 582 of cap free
Amount of items: 2
Items: 
Size: 705239 Color: 4
Size: 294180 Color: 9

Bin 4010: 587 of cap free
Amount of items: 2
Items: 
Size: 752949 Color: 0
Size: 246465 Color: 15

Bin 4011: 588 of cap free
Amount of items: 3
Items: 
Size: 535667 Color: 6
Size: 288670 Color: 19
Size: 175076 Color: 4

Bin 4012: 590 of cap free
Amount of items: 2
Items: 
Size: 741417 Color: 6
Size: 257994 Color: 5

Bin 4013: 590 of cap free
Amount of items: 2
Items: 
Size: 515630 Color: 4
Size: 483781 Color: 11

Bin 4014: 591 of cap free
Amount of items: 2
Items: 
Size: 786737 Color: 10
Size: 212673 Color: 12

Bin 4015: 592 of cap free
Amount of items: 2
Items: 
Size: 782847 Color: 14
Size: 216562 Color: 8

Bin 4016: 593 of cap free
Amount of items: 2
Items: 
Size: 757763 Color: 0
Size: 241645 Color: 10

Bin 4017: 596 of cap free
Amount of items: 2
Items: 
Size: 772581 Color: 6
Size: 226824 Color: 7

Bin 4018: 597 of cap free
Amount of items: 2
Items: 
Size: 529419 Color: 10
Size: 469985 Color: 0

Bin 4019: 598 of cap free
Amount of items: 3
Items: 
Size: 396244 Color: 0
Size: 371868 Color: 9
Size: 231291 Color: 13

Bin 4020: 601 of cap free
Amount of items: 2
Items: 
Size: 662253 Color: 10
Size: 337147 Color: 4

Bin 4021: 603 of cap free
Amount of items: 2
Items: 
Size: 778847 Color: 9
Size: 220551 Color: 1

Bin 4022: 604 of cap free
Amount of items: 2
Items: 
Size: 700160 Color: 18
Size: 299237 Color: 9

Bin 4023: 605 of cap free
Amount of items: 2
Items: 
Size: 644560 Color: 0
Size: 354836 Color: 5

Bin 4024: 606 of cap free
Amount of items: 3
Items: 
Size: 441239 Color: 11
Size: 305799 Color: 0
Size: 252357 Color: 7

Bin 4025: 607 of cap free
Amount of items: 2
Items: 
Size: 563899 Color: 0
Size: 435495 Color: 3

Bin 4026: 607 of cap free
Amount of items: 2
Items: 
Size: 510655 Color: 0
Size: 488739 Color: 17

Bin 4027: 608 of cap free
Amount of items: 2
Items: 
Size: 709775 Color: 9
Size: 289618 Color: 1

Bin 4028: 609 of cap free
Amount of items: 2
Items: 
Size: 738904 Color: 8
Size: 260488 Color: 9

Bin 4029: 609 of cap free
Amount of items: 2
Items: 
Size: 611556 Color: 3
Size: 387836 Color: 14

Bin 4030: 612 of cap free
Amount of items: 2
Items: 
Size: 565352 Color: 12
Size: 434037 Color: 19

Bin 4031: 614 of cap free
Amount of items: 2
Items: 
Size: 753953 Color: 18
Size: 245434 Color: 2

Bin 4032: 615 of cap free
Amount of items: 2
Items: 
Size: 545441 Color: 14
Size: 453945 Color: 3

Bin 4033: 616 of cap free
Amount of items: 2
Items: 
Size: 757757 Color: 12
Size: 241628 Color: 0

Bin 4034: 617 of cap free
Amount of items: 2
Items: 
Size: 543775 Color: 17
Size: 455609 Color: 3

Bin 4035: 617 of cap free
Amount of items: 2
Items: 
Size: 536518 Color: 15
Size: 462866 Color: 0

Bin 4036: 618 of cap free
Amount of items: 2
Items: 
Size: 731773 Color: 16
Size: 267610 Color: 3

Bin 4037: 619 of cap free
Amount of items: 2
Items: 
Size: 799872 Color: 13
Size: 199510 Color: 19

Bin 4038: 622 of cap free
Amount of items: 2
Items: 
Size: 732517 Color: 6
Size: 266862 Color: 17

Bin 4039: 622 of cap free
Amount of items: 2
Items: 
Size: 720911 Color: 18
Size: 278468 Color: 1

Bin 4040: 627 of cap free
Amount of items: 2
Items: 
Size: 672247 Color: 3
Size: 327127 Color: 0

Bin 4041: 628 of cap free
Amount of items: 2
Items: 
Size: 731773 Color: 2
Size: 267600 Color: 4

Bin 4042: 628 of cap free
Amount of items: 2
Items: 
Size: 594292 Color: 3
Size: 405081 Color: 10

Bin 4043: 629 of cap free
Amount of items: 2
Items: 
Size: 614134 Color: 3
Size: 385238 Color: 1

Bin 4044: 629 of cap free
Amount of items: 2
Items: 
Size: 701626 Color: 4
Size: 297746 Color: 0

Bin 4045: 631 of cap free
Amount of items: 2
Items: 
Size: 574487 Color: 11
Size: 424883 Color: 8

Bin 4046: 634 of cap free
Amount of items: 2
Items: 
Size: 735943 Color: 6
Size: 263424 Color: 0

Bin 4047: 635 of cap free
Amount of items: 2
Items: 
Size: 682903 Color: 13
Size: 316463 Color: 15

Bin 4048: 636 of cap free
Amount of items: 2
Items: 
Size: 731772 Color: 4
Size: 267593 Color: 7

Bin 4049: 637 of cap free
Amount of items: 2
Items: 
Size: 797050 Color: 4
Size: 202314 Color: 17

Bin 4050: 638 of cap free
Amount of items: 2
Items: 
Size: 500716 Color: 17
Size: 498647 Color: 14

Bin 4051: 638 of cap free
Amount of items: 2
Items: 
Size: 538251 Color: 3
Size: 461112 Color: 9

Bin 4052: 640 of cap free
Amount of items: 2
Items: 
Size: 727976 Color: 0
Size: 271385 Color: 5

Bin 4053: 640 of cap free
Amount of items: 2
Items: 
Size: 520651 Color: 2
Size: 478710 Color: 8

Bin 4054: 641 of cap free
Amount of items: 2
Items: 
Size: 687695 Color: 17
Size: 311665 Color: 2

Bin 4055: 643 of cap free
Amount of items: 2
Items: 
Size: 649849 Color: 12
Size: 349509 Color: 16

Bin 4056: 644 of cap free
Amount of items: 2
Items: 
Size: 771068 Color: 0
Size: 228289 Color: 12

Bin 4057: 644 of cap free
Amount of items: 2
Items: 
Size: 563901 Color: 3
Size: 435456 Color: 13

Bin 4058: 645 of cap free
Amount of items: 2
Items: 
Size: 500714 Color: 5
Size: 498642 Color: 11

Bin 4059: 645 of cap free
Amount of items: 2
Items: 
Size: 578800 Color: 12
Size: 420556 Color: 8

Bin 4060: 649 of cap free
Amount of items: 2
Items: 
Size: 687674 Color: 17
Size: 311678 Color: 7

Bin 4061: 649 of cap free
Amount of items: 2
Items: 
Size: 510437 Color: 5
Size: 488915 Color: 16

Bin 4062: 653 of cap free
Amount of items: 2
Items: 
Size: 725178 Color: 5
Size: 274170 Color: 7

Bin 4063: 653 of cap free
Amount of items: 2
Items: 
Size: 518088 Color: 19
Size: 481260 Color: 6

Bin 4064: 654 of cap free
Amount of items: 2
Items: 
Size: 692470 Color: 4
Size: 306877 Color: 17

Bin 4065: 656 of cap free
Amount of items: 2
Items: 
Size: 589536 Color: 11
Size: 409809 Color: 0

Bin 4066: 657 of cap free
Amount of items: 2
Items: 
Size: 726067 Color: 6
Size: 273277 Color: 11

Bin 4067: 658 of cap free
Amount of items: 2
Items: 
Size: 555555 Color: 12
Size: 443788 Color: 17

Bin 4068: 659 of cap free
Amount of items: 2
Items: 
Size: 656123 Color: 8
Size: 343219 Color: 10

Bin 4069: 660 of cap free
Amount of items: 2
Items: 
Size: 682885 Color: 0
Size: 316456 Color: 5

Bin 4070: 660 of cap free
Amount of items: 2
Items: 
Size: 503204 Color: 4
Size: 496137 Color: 9

Bin 4071: 671 of cap free
Amount of items: 2
Items: 
Size: 652298 Color: 0
Size: 347032 Color: 2

Bin 4072: 673 of cap free
Amount of items: 2
Items: 
Size: 518015 Color: 16
Size: 481313 Color: 17

Bin 4073: 675 of cap free
Amount of items: 2
Items: 
Size: 663001 Color: 10
Size: 336325 Color: 15

Bin 4074: 675 of cap free
Amount of items: 2
Items: 
Size: 702749 Color: 6
Size: 296577 Color: 0

Bin 4075: 677 of cap free
Amount of items: 2
Items: 
Size: 713960 Color: 16
Size: 285364 Color: 1

Bin 4076: 678 of cap free
Amount of items: 3
Items: 
Size: 536783 Color: 15
Size: 286552 Color: 1
Size: 175988 Color: 18

Bin 4077: 678 of cap free
Amount of items: 2
Items: 
Size: 777204 Color: 10
Size: 222119 Color: 6

Bin 4078: 681 of cap free
Amount of items: 2
Items: 
Size: 559245 Color: 11
Size: 440075 Color: 5

Bin 4079: 681 of cap free
Amount of items: 2
Items: 
Size: 570622 Color: 7
Size: 428698 Color: 0

Bin 4080: 681 of cap free
Amount of items: 2
Items: 
Size: 632406 Color: 13
Size: 366914 Color: 19

Bin 4081: 691 of cap free
Amount of items: 2
Items: 
Size: 608112 Color: 0
Size: 391198 Color: 8

Bin 4082: 691 of cap free
Amount of items: 2
Items: 
Size: 779491 Color: 4
Size: 219819 Color: 12

Bin 4083: 692 of cap free
Amount of items: 2
Items: 
Size: 791483 Color: 15
Size: 207826 Color: 2

Bin 4084: 693 of cap free
Amount of items: 2
Items: 
Size: 742859 Color: 16
Size: 256449 Color: 15

Bin 4085: 694 of cap free
Amount of items: 2
Items: 
Size: 771170 Color: 6
Size: 228137 Color: 9

Bin 4086: 695 of cap free
Amount of items: 2
Items: 
Size: 555518 Color: 11
Size: 443788 Color: 18

Bin 4087: 700 of cap free
Amount of items: 3
Items: 
Size: 514152 Color: 9
Size: 253381 Color: 3
Size: 231768 Color: 17

Bin 4088: 702 of cap free
Amount of items: 2
Items: 
Size: 783894 Color: 5
Size: 215405 Color: 11

Bin 4089: 704 of cap free
Amount of items: 2
Items: 
Size: 674747 Color: 10
Size: 324550 Color: 5

Bin 4090: 718 of cap free
Amount of items: 2
Items: 
Size: 738920 Color: 15
Size: 260363 Color: 14

Bin 4091: 720 of cap free
Amount of items: 2
Items: 
Size: 711883 Color: 17
Size: 287398 Color: 19

Bin 4092: 722 of cap free
Amount of items: 2
Items: 
Size: 740212 Color: 11
Size: 259067 Color: 6

Bin 4093: 722 of cap free
Amount of items: 2
Items: 
Size: 761027 Color: 17
Size: 238252 Color: 18

Bin 4094: 723 of cap free
Amount of items: 2
Items: 
Size: 505527 Color: 3
Size: 493751 Color: 9

Bin 4095: 725 of cap free
Amount of items: 2
Items: 
Size: 604942 Color: 12
Size: 394334 Color: 4

Bin 4096: 725 of cap free
Amount of items: 2
Items: 
Size: 608089 Color: 6
Size: 391187 Color: 4

Bin 4097: 728 of cap free
Amount of items: 2
Items: 
Size: 675108 Color: 9
Size: 324165 Color: 10

Bin 4098: 728 of cap free
Amount of items: 2
Items: 
Size: 745834 Color: 9
Size: 253439 Color: 4

Bin 4099: 729 of cap free
Amount of items: 2
Items: 
Size: 658703 Color: 14
Size: 340569 Color: 2

Bin 4100: 731 of cap free
Amount of items: 2
Items: 
Size: 699151 Color: 3
Size: 300119 Color: 2

Bin 4101: 733 of cap free
Amount of items: 2
Items: 
Size: 624091 Color: 15
Size: 375177 Color: 4

Bin 4102: 734 of cap free
Amount of items: 2
Items: 
Size: 717337 Color: 17
Size: 281930 Color: 5

Bin 4103: 735 of cap free
Amount of items: 2
Items: 
Size: 512008 Color: 19
Size: 487258 Color: 8

Bin 4104: 735 of cap free
Amount of items: 2
Items: 
Size: 723899 Color: 19
Size: 275367 Color: 13

Bin 4105: 735 of cap free
Amount of items: 2
Items: 
Size: 539868 Color: 7
Size: 459398 Color: 3

Bin 4106: 738 of cap free
Amount of items: 2
Items: 
Size: 505517 Color: 15
Size: 493746 Color: 19

Bin 4107: 739 of cap free
Amount of items: 2
Items: 
Size: 796991 Color: 2
Size: 202271 Color: 18

Bin 4108: 740 of cap free
Amount of items: 2
Items: 
Size: 580467 Color: 13
Size: 418794 Color: 8

Bin 4109: 740 of cap free
Amount of items: 2
Items: 
Size: 520572 Color: 19
Size: 478689 Color: 11

Bin 4110: 743 of cap free
Amount of items: 2
Items: 
Size: 782571 Color: 16
Size: 216687 Color: 1

Bin 4111: 745 of cap free
Amount of items: 2
Items: 
Size: 678306 Color: 0
Size: 320950 Color: 14

Bin 4112: 746 of cap free
Amount of items: 2
Items: 
Size: 538174 Color: 18
Size: 461081 Color: 19

Bin 4113: 746 of cap free
Amount of items: 2
Items: 
Size: 627298 Color: 15
Size: 371957 Color: 11

Bin 4114: 746 of cap free
Amount of items: 2
Items: 
Size: 515611 Color: 12
Size: 483644 Color: 14

Bin 4115: 754 of cap free
Amount of items: 2
Items: 
Size: 517988 Color: 12
Size: 481259 Color: 10

Bin 4116: 754 of cap free
Amount of items: 2
Items: 
Size: 687687 Color: 2
Size: 311560 Color: 10

Bin 4117: 754 of cap free
Amount of items: 2
Items: 
Size: 555465 Color: 15
Size: 443782 Color: 5

Bin 4118: 760 of cap free
Amount of items: 2
Items: 
Size: 581414 Color: 4
Size: 417827 Color: 1

Bin 4119: 766 of cap free
Amount of items: 2
Items: 
Size: 751768 Color: 5
Size: 247467 Color: 14

Bin 4120: 769 of cap free
Amount of items: 2
Items: 
Size: 693028 Color: 17
Size: 306204 Color: 9

Bin 4121: 773 of cap free
Amount of items: 2
Items: 
Size: 624083 Color: 3
Size: 375145 Color: 19

Bin 4122: 775 of cap free
Amount of items: 2
Items: 
Size: 707002 Color: 18
Size: 292224 Color: 15

Bin 4123: 776 of cap free
Amount of items: 2
Items: 
Size: 529415 Color: 0
Size: 469810 Color: 15

Bin 4124: 778 of cap free
Amount of items: 2
Items: 
Size: 587863 Color: 16
Size: 411360 Color: 5

Bin 4125: 785 of cap free
Amount of items: 2
Items: 
Size: 608041 Color: 18
Size: 391175 Color: 12

Bin 4126: 786 of cap free
Amount of items: 2
Items: 
Size: 594138 Color: 11
Size: 405077 Color: 14

Bin 4127: 789 of cap free
Amount of items: 2
Items: 
Size: 761912 Color: 8
Size: 237300 Color: 1

Bin 4128: 790 of cap free
Amount of items: 2
Items: 
Size: 644659 Color: 6
Size: 354552 Color: 18

Bin 4129: 790 of cap free
Amount of items: 2
Items: 
Size: 628714 Color: 4
Size: 370497 Color: 19

Bin 4130: 794 of cap free
Amount of items: 3
Items: 
Size: 370341 Color: 7
Size: 337175 Color: 1
Size: 291691 Color: 13

Bin 4131: 794 of cap free
Amount of items: 2
Items: 
Size: 660358 Color: 5
Size: 338849 Color: 8

Bin 4132: 797 of cap free
Amount of items: 2
Items: 
Size: 758909 Color: 13
Size: 240295 Color: 1

Bin 4133: 798 of cap free
Amount of items: 2
Items: 
Size: 650862 Color: 12
Size: 348341 Color: 5

Bin 4134: 802 of cap free
Amount of items: 2
Items: 
Size: 615184 Color: 18
Size: 384015 Color: 8

Bin 4135: 804 of cap free
Amount of items: 2
Items: 
Size: 798618 Color: 14
Size: 200579 Color: 8

Bin 4136: 807 of cap free
Amount of items: 2
Items: 
Size: 775682 Color: 18
Size: 223512 Color: 5

Bin 4137: 808 of cap free
Amount of items: 2
Items: 
Size: 511963 Color: 12
Size: 487230 Color: 8

Bin 4138: 808 of cap free
Amount of items: 2
Items: 
Size: 677378 Color: 0
Size: 321815 Color: 2

Bin 4139: 809 of cap free
Amount of items: 2
Items: 
Size: 717349 Color: 6
Size: 281843 Color: 8

Bin 4140: 811 of cap free
Amount of items: 2
Items: 
Size: 662034 Color: 14
Size: 337156 Color: 6

Bin 4141: 813 of cap free
Amount of items: 2
Items: 
Size: 701555 Color: 16
Size: 297633 Color: 19

Bin 4142: 813 of cap free
Amount of items: 2
Items: 
Size: 560219 Color: 8
Size: 438969 Color: 0

Bin 4143: 820 of cap free
Amount of items: 2
Items: 
Size: 581396 Color: 4
Size: 417785 Color: 3

Bin 4144: 824 of cap free
Amount of items: 2
Items: 
Size: 766998 Color: 5
Size: 232179 Color: 12

Bin 4145: 827 of cap free
Amount of items: 2
Items: 
Size: 686330 Color: 13
Size: 312844 Color: 3

Bin 4146: 832 of cap free
Amount of items: 2
Items: 
Size: 747107 Color: 8
Size: 252062 Color: 7

Bin 4147: 832 of cap free
Amount of items: 2
Items: 
Size: 503069 Color: 15
Size: 496100 Color: 1

Bin 4148: 834 of cap free
Amount of items: 3
Items: 
Size: 686759 Color: 10
Size: 189138 Color: 15
Size: 123270 Color: 15

Bin 4149: 834 of cap free
Amount of items: 2
Items: 
Size: 532483 Color: 18
Size: 466684 Color: 1

Bin 4150: 834 of cap free
Amount of items: 2
Items: 
Size: 565332 Color: 15
Size: 433835 Color: 12

Bin 4151: 837 of cap free
Amount of items: 2
Items: 
Size: 779479 Color: 2
Size: 219685 Color: 9

Bin 4152: 848 of cap free
Amount of items: 2
Items: 
Size: 619245 Color: 2
Size: 379908 Color: 8

Bin 4153: 854 of cap free
Amount of items: 2
Items: 
Size: 784023 Color: 17
Size: 215124 Color: 7

Bin 4154: 855 of cap free
Amount of items: 2
Items: 
Size: 745716 Color: 4
Size: 253430 Color: 6

Bin 4155: 860 of cap free
Amount of items: 2
Items: 
Size: 624065 Color: 17
Size: 375076 Color: 7

Bin 4156: 862 of cap free
Amount of items: 2
Items: 
Size: 685271 Color: 3
Size: 313868 Color: 1

Bin 4157: 862 of cap free
Amount of items: 2
Items: 
Size: 767012 Color: 19
Size: 232127 Color: 2

Bin 4158: 869 of cap free
Amount of items: 2
Items: 
Size: 741390 Color: 3
Size: 257742 Color: 9

Bin 4159: 870 of cap free
Amount of items: 2
Items: 
Size: 565317 Color: 2
Size: 433814 Color: 12

Bin 4160: 871 of cap free
Amount of items: 2
Items: 
Size: 612916 Color: 17
Size: 386214 Color: 16

Bin 4161: 872 of cap free
Amount of items: 2
Items: 
Size: 502057 Color: 9
Size: 497072 Color: 15

Bin 4162: 873 of cap free
Amount of items: 2
Items: 
Size: 526626 Color: 8
Size: 472502 Color: 4

Bin 4163: 880 of cap free
Amount of items: 2
Items: 
Size: 633854 Color: 6
Size: 365267 Color: 15

Bin 4164: 883 of cap free
Amount of items: 2
Items: 
Size: 503918 Color: 16
Size: 495200 Color: 9

Bin 4165: 884 of cap free
Amount of items: 2
Items: 
Size: 538167 Color: 4
Size: 460950 Color: 2

Bin 4166: 884 of cap free
Amount of items: 2
Items: 
Size: 529392 Color: 17
Size: 469725 Color: 7

Bin 4167: 885 of cap free
Amount of items: 2
Items: 
Size: 552130 Color: 0
Size: 446986 Color: 6

Bin 4168: 887 of cap free
Amount of items: 2
Items: 
Size: 756304 Color: 5
Size: 242810 Color: 0

Bin 4169: 889 of cap free
Amount of items: 2
Items: 
Size: 798438 Color: 2
Size: 200674 Color: 17

Bin 4170: 898 of cap free
Amount of items: 2
Items: 
Size: 636079 Color: 1
Size: 363024 Color: 17

Bin 4171: 900 of cap free
Amount of items: 2
Items: 
Size: 514026 Color: 18
Size: 485075 Color: 5

Bin 4172: 904 of cap free
Amount of items: 2
Items: 
Size: 660083 Color: 19
Size: 339014 Color: 5

Bin 4173: 905 of cap free
Amount of items: 2
Items: 
Size: 635118 Color: 14
Size: 363978 Color: 6

Bin 4174: 915 of cap free
Amount of items: 2
Items: 
Size: 559044 Color: 2
Size: 440042 Color: 6

Bin 4175: 919 of cap free
Amount of items: 2
Items: 
Size: 685235 Color: 6
Size: 313847 Color: 3

Bin 4176: 921 of cap free
Amount of items: 2
Items: 
Size: 649682 Color: 3
Size: 349398 Color: 2

Bin 4177: 921 of cap free
Amount of items: 2
Items: 
Size: 547359 Color: 3
Size: 451721 Color: 18

Bin 4178: 925 of cap free
Amount of items: 2
Items: 
Size: 619288 Color: 14
Size: 379788 Color: 7

Bin 4179: 935 of cap free
Amount of items: 2
Items: 
Size: 636089 Color: 3
Size: 362977 Color: 8

Bin 4180: 947 of cap free
Amount of items: 2
Items: 
Size: 709986 Color: 10
Size: 289068 Color: 18

Bin 4181: 956 of cap free
Amount of items: 2
Items: 
Size: 630471 Color: 7
Size: 368574 Color: 19

Bin 4182: 956 of cap free
Amount of items: 2
Items: 
Size: 545111 Color: 17
Size: 453934 Color: 8

Bin 4183: 959 of cap free
Amount of items: 2
Items: 
Size: 613938 Color: 15
Size: 385104 Color: 1

Bin 4184: 959 of cap free
Amount of items: 2
Items: 
Size: 554204 Color: 12
Size: 444838 Color: 10

Bin 4185: 959 of cap free
Amount of items: 2
Items: 
Size: 776771 Color: 17
Size: 222271 Color: 11

Bin 4186: 960 of cap free
Amount of items: 2
Items: 
Size: 534138 Color: 1
Size: 464903 Color: 17

Bin 4187: 961 of cap free
Amount of items: 2
Items: 
Size: 639353 Color: 18
Size: 359687 Color: 16

Bin 4188: 962 of cap free
Amount of items: 2
Items: 
Size: 505393 Color: 9
Size: 493646 Color: 19

Bin 4189: 967 of cap free
Amount of items: 2
Items: 
Size: 555351 Color: 11
Size: 443683 Color: 0

Bin 4190: 971 of cap free
Amount of items: 2
Items: 
Size: 499544 Color: 17
Size: 499486 Color: 18

Bin 4191: 974 of cap free
Amount of items: 2
Items: 
Size: 761806 Color: 5
Size: 237221 Color: 10

Bin 4192: 975 of cap free
Amount of items: 2
Items: 
Size: 523187 Color: 3
Size: 475839 Color: 11

Bin 4193: 975 of cap free
Amount of items: 2
Items: 
Size: 740003 Color: 17
Size: 259023 Color: 11

Bin 4194: 984 of cap free
Amount of items: 2
Items: 
Size: 763202 Color: 6
Size: 235815 Color: 4

Bin 4195: 984 of cap free
Amount of items: 2
Items: 
Size: 633833 Color: 19
Size: 365184 Color: 2

Bin 4196: 985 of cap free
Amount of items: 2
Items: 
Size: 565219 Color: 1
Size: 433797 Color: 9

Bin 4197: 986 of cap free
Amount of items: 2
Items: 
Size: 780585 Color: 11
Size: 218430 Color: 4

Bin 4198: 989 of cap free
Amount of items: 2
Items: 
Size: 709921 Color: 4
Size: 289091 Color: 17

Bin 4199: 991 of cap free
Amount of items: 2
Items: 
Size: 539819 Color: 5
Size: 459191 Color: 12

Bin 4200: 1004 of cap free
Amount of items: 2
Items: 
Size: 619157 Color: 6
Size: 379840 Color: 12

Bin 4201: 1005 of cap free
Amount of items: 3
Items: 
Size: 667620 Color: 7
Size: 182181 Color: 19
Size: 149195 Color: 17

Bin 4202: 1011 of cap free
Amount of items: 2
Items: 
Size: 740015 Color: 9
Size: 258975 Color: 19

Bin 4203: 1018 of cap free
Amount of items: 2
Items: 
Size: 658644 Color: 14
Size: 340339 Color: 6

Bin 4204: 1023 of cap free
Amount of items: 2
Items: 
Size: 799482 Color: 19
Size: 199496 Color: 2

Bin 4205: 1026 of cap free
Amount of items: 2
Items: 
Size: 598789 Color: 15
Size: 400186 Color: 14

Bin 4206: 1032 of cap free
Amount of items: 2
Items: 
Size: 773359 Color: 16
Size: 225610 Color: 4

Bin 4207: 1034 of cap free
Amount of items: 2
Items: 
Size: 673697 Color: 2
Size: 325270 Color: 15

Bin 4208: 1036 of cap free
Amount of items: 2
Items: 
Size: 681506 Color: 9
Size: 317459 Color: 5

Bin 4209: 1039 of cap free
Amount of items: 2
Items: 
Size: 628837 Color: 19
Size: 370125 Color: 9

Bin 4210: 1040 of cap free
Amount of items: 2
Items: 
Size: 513936 Color: 4
Size: 485025 Color: 0

Bin 4211: 1043 of cap free
Amount of items: 2
Items: 
Size: 658626 Color: 12
Size: 340332 Color: 0

Bin 4212: 1045 of cap free
Amount of items: 2
Items: 
Size: 677159 Color: 6
Size: 321797 Color: 8

Bin 4213: 1049 of cap free
Amount of items: 2
Items: 
Size: 556506 Color: 0
Size: 442446 Color: 16

Bin 4214: 1059 of cap free
Amount of items: 2
Items: 
Size: 625800 Color: 18
Size: 373142 Color: 14

Bin 4215: 1063 of cap free
Amount of items: 2
Items: 
Size: 696368 Color: 16
Size: 302570 Color: 11

Bin 4216: 1067 of cap free
Amount of items: 2
Items: 
Size: 752633 Color: 8
Size: 246301 Color: 12

Bin 4217: 1071 of cap free
Amount of items: 2
Items: 
Size: 611148 Color: 18
Size: 387782 Color: 16

Bin 4218: 1076 of cap free
Amount of items: 2
Items: 
Size: 578756 Color: 14
Size: 420169 Color: 4

Bin 4219: 1078 of cap free
Amount of items: 2
Items: 
Size: 669701 Color: 16
Size: 329222 Color: 2

Bin 4220: 1083 of cap free
Amount of items: 2
Items: 
Size: 713913 Color: 8
Size: 285005 Color: 9

Bin 4221: 1085 of cap free
Amount of items: 2
Items: 
Size: 644536 Color: 6
Size: 354380 Color: 3

Bin 4222: 1086 of cap free
Amount of items: 2
Items: 
Size: 611139 Color: 8
Size: 387776 Color: 1

Bin 4223: 1088 of cap free
Amount of items: 2
Items: 
Size: 727189 Color: 16
Size: 271724 Color: 6

Bin 4224: 1090 of cap free
Amount of items: 3
Items: 
Size: 718180 Color: 10
Size: 143421 Color: 9
Size: 137310 Color: 4

Bin 4225: 1116 of cap free
Amount of items: 2
Items: 
Size: 756393 Color: 19
Size: 242492 Color: 2

Bin 4226: 1118 of cap free
Amount of items: 2
Items: 
Size: 647350 Color: 7
Size: 351533 Color: 4

Bin 4227: 1125 of cap free
Amount of items: 2
Items: 
Size: 558950 Color: 8
Size: 439926 Color: 5

Bin 4228: 1126 of cap free
Amount of items: 3
Items: 
Size: 618989 Color: 8
Size: 237846 Color: 9
Size: 142040 Color: 1

Bin 4229: 1129 of cap free
Amount of items: 2
Items: 
Size: 758477 Color: 9
Size: 240395 Color: 1

Bin 4230: 1132 of cap free
Amount of items: 2
Items: 
Size: 604562 Color: 18
Size: 394307 Color: 7

Bin 4231: 1135 of cap free
Amount of items: 2
Items: 
Size: 665642 Color: 10
Size: 333224 Color: 7

Bin 4232: 1138 of cap free
Amount of items: 2
Items: 
Size: 665654 Color: 10
Size: 333209 Color: 19

Bin 4233: 1140 of cap free
Amount of items: 2
Items: 
Size: 657032 Color: 5
Size: 341829 Color: 4

Bin 4234: 1140 of cap free
Amount of items: 2
Items: 
Size: 565149 Color: 10
Size: 433712 Color: 16

Bin 4235: 1157 of cap free
Amount of items: 2
Items: 
Size: 621237 Color: 4
Size: 377607 Color: 16

Bin 4236: 1166 of cap free
Amount of items: 2
Items: 
Size: 629974 Color: 3
Size: 368861 Color: 15

Bin 4237: 1167 of cap free
Amount of items: 2
Items: 
Size: 604501 Color: 12
Size: 394333 Color: 13

Bin 4238: 1168 of cap free
Amount of items: 2
Items: 
Size: 651949 Color: 11
Size: 346884 Color: 7

Bin 4239: 1169 of cap free
Amount of items: 2
Items: 
Size: 702715 Color: 11
Size: 296117 Color: 1

Bin 4240: 1184 of cap free
Amount of items: 2
Items: 
Size: 570375 Color: 9
Size: 428442 Color: 4

Bin 4241: 1193 of cap free
Amount of items: 2
Items: 
Size: 776555 Color: 9
Size: 222253 Color: 4

Bin 4242: 1201 of cap free
Amount of items: 2
Items: 
Size: 673588 Color: 9
Size: 325212 Color: 10

Bin 4243: 1204 of cap free
Amount of items: 2
Items: 
Size: 704002 Color: 11
Size: 294795 Color: 1

Bin 4244: 1213 of cap free
Amount of items: 2
Items: 
Size: 780527 Color: 13
Size: 218261 Color: 10

Bin 4245: 1214 of cap free
Amount of items: 2
Items: 
Size: 506505 Color: 8
Size: 492282 Color: 12

Bin 4246: 1218 of cap free
Amount of items: 2
Items: 
Size: 574430 Color: 10
Size: 424353 Color: 17

Bin 4247: 1228 of cap free
Amount of items: 2
Items: 
Size: 766899 Color: 18
Size: 231874 Color: 12

Bin 4248: 1228 of cap free
Amount of items: 2
Items: 
Size: 547141 Color: 1
Size: 451632 Color: 7

Bin 4249: 1230 of cap free
Amount of items: 2
Items: 
Size: 626961 Color: 15
Size: 371810 Color: 1

Bin 4250: 1230 of cap free
Amount of items: 2
Items: 
Size: 554159 Color: 1
Size: 444612 Color: 18

Bin 4251: 1242 of cap free
Amount of items: 2
Items: 
Size: 623810 Color: 18
Size: 374949 Color: 5

Bin 4252: 1246 of cap free
Amount of items: 2
Items: 
Size: 660112 Color: 9
Size: 338643 Color: 14

Bin 4253: 1254 of cap free
Amount of items: 2
Items: 
Size: 619051 Color: 0
Size: 379696 Color: 16

Bin 4254: 1259 of cap free
Amount of items: 2
Items: 
Size: 709716 Color: 15
Size: 289026 Color: 5

Bin 4255: 1260 of cap free
Amount of items: 2
Items: 
Size: 657028 Color: 17
Size: 341713 Color: 11

Bin 4256: 1264 of cap free
Amount of items: 2
Items: 
Size: 506398 Color: 4
Size: 492339 Color: 6

Bin 4257: 1266 of cap free
Amount of items: 2
Items: 
Size: 669550 Color: 3
Size: 329185 Color: 5

Bin 4258: 1266 of cap free
Amount of items: 2
Items: 
Size: 644564 Color: 6
Size: 354171 Color: 11

Bin 4259: 1277 of cap free
Amount of items: 2
Items: 
Size: 551677 Color: 14
Size: 447047 Color: 8

Bin 4260: 1281 of cap free
Amount of items: 2
Items: 
Size: 665567 Color: 4
Size: 333153 Color: 5

Bin 4261: 1285 of cap free
Amount of items: 2
Items: 
Size: 601207 Color: 2
Size: 397509 Color: 12

Bin 4262: 1285 of cap free
Amount of items: 2
Items: 
Size: 690368 Color: 2
Size: 308348 Color: 15

Bin 4263: 1288 of cap free
Amount of items: 2
Items: 
Size: 739923 Color: 16
Size: 258790 Color: 7

Bin 4264: 1290 of cap free
Amount of items: 2
Items: 
Size: 604535 Color: 13
Size: 394176 Color: 5

Bin 4265: 1290 of cap free
Amount of items: 2
Items: 
Size: 578756 Color: 13
Size: 419955 Color: 4

Bin 4266: 1291 of cap free
Amount of items: 2
Items: 
Size: 623772 Color: 8
Size: 374938 Color: 6

Bin 4267: 1297 of cap free
Amount of items: 2
Items: 
Size: 656872 Color: 14
Size: 341832 Color: 2

Bin 4268: 1298 of cap free
Amount of items: 2
Items: 
Size: 516642 Color: 4
Size: 482061 Color: 10

Bin 4269: 1313 of cap free
Amount of items: 2
Items: 
Size: 670382 Color: 10
Size: 328306 Color: 14

Bin 4270: 1314 of cap free
Amount of items: 2
Items: 
Size: 601047 Color: 6
Size: 397640 Color: 8

Bin 4271: 1321 of cap free
Amount of items: 2
Items: 
Size: 625632 Color: 3
Size: 373048 Color: 15

Bin 4272: 1335 of cap free
Amount of items: 2
Items: 
Size: 601192 Color: 3
Size: 397474 Color: 12

Bin 4273: 1339 of cap free
Amount of items: 2
Items: 
Size: 639011 Color: 5
Size: 359651 Color: 2

Bin 4274: 1341 of cap free
Amount of items: 2
Items: 
Size: 574354 Color: 9
Size: 424306 Color: 4

Bin 4275: 1342 of cap free
Amount of items: 2
Items: 
Size: 683027 Color: 16
Size: 315632 Color: 1

Bin 4276: 1345 of cap free
Amount of items: 2
Items: 
Size: 651889 Color: 5
Size: 346767 Color: 8

Bin 4277: 1346 of cap free
Amount of items: 2
Items: 
Size: 616960 Color: 13
Size: 381695 Color: 1

Bin 4278: 1349 of cap free
Amount of items: 2
Items: 
Size: 614769 Color: 1
Size: 383883 Color: 9

Bin 4279: 1350 of cap free
Amount of items: 2
Items: 
Size: 608985 Color: 2
Size: 389666 Color: 6

Bin 4280: 1359 of cap free
Amount of items: 2
Items: 
Size: 567883 Color: 12
Size: 430759 Color: 19

Bin 4281: 1360 of cap free
Amount of items: 2
Items: 
Size: 658459 Color: 15
Size: 340182 Color: 14

Bin 4282: 1373 of cap free
Amount of items: 2
Items: 
Size: 651900 Color: 3
Size: 346728 Color: 18

Bin 4283: 1373 of cap free
Amount of items: 2
Items: 
Size: 590810 Color: 14
Size: 407818 Color: 10

Bin 4284: 1379 of cap free
Amount of items: 2
Items: 
Size: 501620 Color: 17
Size: 497002 Color: 13

Bin 4285: 1380 of cap free
Amount of items: 2
Items: 
Size: 665479 Color: 10
Size: 333142 Color: 14

Bin 4286: 1386 of cap free
Amount of items: 2
Items: 
Size: 591273 Color: 4
Size: 407342 Color: 16

Bin 4287: 1387 of cap free
Amount of items: 2
Items: 
Size: 763494 Color: 0
Size: 235120 Color: 15

Bin 4288: 1398 of cap free
Amount of items: 2
Items: 
Size: 656835 Color: 6
Size: 341768 Color: 8

Bin 4289: 1400 of cap free
Amount of items: 2
Items: 
Size: 582953 Color: 14
Size: 415648 Color: 2

Bin 4290: 1403 of cap free
Amount of items: 2
Items: 
Size: 686264 Color: 4
Size: 312334 Color: 7

Bin 4291: 1414 of cap free
Amount of items: 2
Items: 
Size: 658408 Color: 14
Size: 340179 Color: 4

Bin 4292: 1420 of cap free
Amount of items: 2
Items: 
Size: 776530 Color: 3
Size: 222051 Color: 11

Bin 4293: 1427 of cap free
Amount of items: 2
Items: 
Size: 647009 Color: 2
Size: 351565 Color: 19

Bin 4294: 1437 of cap free
Amount of items: 2
Items: 
Size: 629971 Color: 0
Size: 368593 Color: 12

Bin 4295: 1440 of cap free
Amount of items: 2
Items: 
Size: 747943 Color: 10
Size: 250618 Color: 9

Bin 4296: 1444 of cap free
Amount of items: 2
Items: 
Size: 528894 Color: 5
Size: 469663 Color: 12

Bin 4297: 1458 of cap free
Amount of items: 2
Items: 
Size: 526060 Color: 19
Size: 472483 Color: 7

Bin 4298: 1464 of cap free
Amount of items: 2
Items: 
Size: 758449 Color: 13
Size: 240088 Color: 0

Bin 4299: 1470 of cap free
Amount of items: 2
Items: 
Size: 570189 Color: 2
Size: 428342 Color: 13

Bin 4300: 1472 of cap free
Amount of items: 2
Items: 
Size: 771030 Color: 7
Size: 227499 Color: 2

Bin 4301: 1473 of cap free
Amount of items: 2
Items: 
Size: 522747 Color: 16
Size: 475781 Color: 12

Bin 4302: 1478 of cap free
Amount of items: 2
Items: 
Size: 543021 Color: 19
Size: 455502 Color: 1

Bin 4303: 1479 of cap free
Amount of items: 2
Items: 
Size: 639163 Color: 2
Size: 359359 Color: 19

Bin 4304: 1500 of cap free
Amount of items: 2
Items: 
Size: 669306 Color: 8
Size: 329195 Color: 15

Bin 4305: 1506 of cap free
Amount of items: 2
Items: 
Size: 570209 Color: 13
Size: 428286 Color: 9

Bin 4306: 1524 of cap free
Amount of items: 2
Items: 
Size: 539674 Color: 12
Size: 458803 Color: 13

Bin 4307: 1529 of cap free
Amount of items: 2
Items: 
Size: 784933 Color: 15
Size: 213539 Color: 19

Bin 4308: 1545 of cap free
Amount of items: 2
Items: 
Size: 674740 Color: 10
Size: 323716 Color: 9

Bin 4309: 1550 of cap free
Amount of items: 2
Items: 
Size: 600980 Color: 1
Size: 397471 Color: 4

Bin 4310: 1557 of cap free
Amount of items: 2
Items: 
Size: 651781 Color: 4
Size: 346663 Color: 5

Bin 4311: 1566 of cap free
Amount of items: 2
Items: 
Size: 793275 Color: 13
Size: 205160 Color: 7

Bin 4312: 1580 of cap free
Amount of items: 2
Items: 
Size: 522735 Color: 7
Size: 475686 Color: 6

Bin 4313: 1596 of cap free
Amount of items: 2
Items: 
Size: 753874 Color: 3
Size: 244531 Color: 5

Bin 4314: 1603 of cap free
Amount of items: 2
Items: 
Size: 735514 Color: 1
Size: 262884 Color: 13

Bin 4315: 1607 of cap free
Amount of items: 2
Items: 
Size: 646879 Color: 11
Size: 351515 Color: 0

Bin 4316: 1619 of cap free
Amount of items: 2
Items: 
Size: 780341 Color: 6
Size: 218041 Color: 3

Bin 4317: 1625 of cap free
Amount of items: 2
Items: 
Size: 638845 Color: 17
Size: 359531 Color: 6

Bin 4318: 1627 of cap free
Amount of items: 2
Items: 
Size: 600955 Color: 9
Size: 397419 Color: 17

Bin 4319: 1645 of cap free
Amount of items: 2
Items: 
Size: 753946 Color: 6
Size: 244410 Color: 11

Bin 4320: 1651 of cap free
Amount of items: 2
Items: 
Size: 522669 Color: 19
Size: 475681 Color: 17

Bin 4321: 1659 of cap free
Amount of items: 2
Items: 
Size: 732453 Color: 13
Size: 265889 Color: 2

Bin 4322: 1682 of cap free
Amount of items: 2
Items: 
Size: 659865 Color: 8
Size: 338454 Color: 12

Bin 4323: 1684 of cap free
Amount of items: 2
Items: 
Size: 574286 Color: 5
Size: 424031 Color: 16

Bin 4324: 1705 of cap free
Amount of items: 2
Items: 
Size: 522625 Color: 18
Size: 475671 Color: 19

Bin 4325: 1705 of cap free
Amount of items: 2
Items: 
Size: 551596 Color: 2
Size: 446700 Color: 11

Bin 4326: 1713 of cap free
Amount of items: 2
Items: 
Size: 622762 Color: 10
Size: 375526 Color: 6

Bin 4327: 1733 of cap free
Amount of items: 2
Items: 
Size: 766596 Color: 2
Size: 231672 Color: 18

Bin 4328: 1736 of cap free
Amount of items: 2
Items: 
Size: 659850 Color: 8
Size: 338415 Color: 4

Bin 4329: 1738 of cap free
Amount of items: 2
Items: 
Size: 709400 Color: 17
Size: 288863 Color: 1

Bin 4330: 1742 of cap free
Amount of items: 2
Items: 
Size: 763076 Color: 7
Size: 235183 Color: 11

Bin 4331: 1748 of cap free
Amount of items: 2
Items: 
Size: 644034 Color: 1
Size: 354219 Color: 9

Bin 4332: 1754 of cap free
Amount of items: 2
Items: 
Size: 539566 Color: 11
Size: 458681 Color: 8

Bin 4333: 1762 of cap free
Amount of items: 3
Items: 
Size: 658910 Color: 6
Size: 179850 Color: 13
Size: 159479 Color: 5

Bin 4334: 1775 of cap free
Amount of items: 2
Items: 
Size: 598482 Color: 17
Size: 399744 Color: 3

Bin 4335: 1776 of cap free
Amount of items: 2
Items: 
Size: 590780 Color: 2
Size: 407445 Color: 3

Bin 4336: 1788 of cap free
Amount of items: 2
Items: 
Size: 644003 Color: 9
Size: 354210 Color: 18

Bin 4337: 1792 of cap free
Amount of items: 2
Items: 
Size: 594085 Color: 16
Size: 404124 Color: 8

Bin 4338: 1792 of cap free
Amount of items: 2
Items: 
Size: 564652 Color: 11
Size: 433557 Color: 18

Bin 4339: 1796 of cap free
Amount of items: 2
Items: 
Size: 525754 Color: 3
Size: 472451 Color: 8

Bin 4340: 1797 of cap free
Amount of items: 2
Items: 
Size: 539550 Color: 14
Size: 458654 Color: 2

Bin 4341: 1806 of cap free
Amount of items: 2
Items: 
Size: 715371 Color: 14
Size: 282824 Color: 18

Bin 4342: 1806 of cap free
Amount of items: 2
Items: 
Size: 682825 Color: 15
Size: 315370 Color: 17

Bin 4343: 1812 of cap free
Amount of items: 2
Items: 
Size: 598457 Color: 10
Size: 399732 Color: 8

Bin 4344: 1815 of cap free
Amount of items: 2
Items: 
Size: 644045 Color: 17
Size: 354141 Color: 19

Bin 4345: 1829 of cap free
Amount of items: 2
Items: 
Size: 669311 Color: 15
Size: 328861 Color: 0

Bin 4346: 1834 of cap free
Amount of items: 2
Items: 
Size: 520048 Color: 12
Size: 478119 Color: 15

Bin 4347: 1853 of cap free
Amount of items: 2
Items: 
Size: 682792 Color: 13
Size: 315356 Color: 4

Bin 4348: 1871 of cap free
Amount of items: 2
Items: 
Size: 522539 Color: 6
Size: 475591 Color: 18

Bin 4349: 1872 of cap free
Amount of items: 2
Items: 
Size: 753773 Color: 15
Size: 244356 Color: 11

Bin 4350: 1913 of cap free
Amount of items: 3
Items: 
Size: 727264 Color: 15
Size: 139873 Color: 18
Size: 130951 Color: 3

Bin 4351: 1916 of cap free
Amount of items: 2
Items: 
Size: 567853 Color: 3
Size: 430232 Color: 0

Bin 4352: 1929 of cap free
Amount of items: 2
Items: 
Size: 696925 Color: 3
Size: 301147 Color: 12

Bin 4353: 1934 of cap free
Amount of items: 2
Items: 
Size: 623608 Color: 7
Size: 374459 Color: 9

Bin 4354: 1939 of cap free
Amount of items: 2
Items: 
Size: 610833 Color: 16
Size: 387229 Color: 11

Bin 4355: 1941 of cap free
Amount of items: 2
Items: 
Size: 651781 Color: 3
Size: 346279 Color: 6

Bin 4356: 1946 of cap free
Amount of items: 3
Items: 
Size: 448733 Color: 13
Size: 296407 Color: 6
Size: 252915 Color: 14

Bin 4357: 1950 of cap free
Amount of items: 2
Items: 
Size: 798559 Color: 9
Size: 199492 Color: 10

Bin 4358: 1968 of cap free
Amount of items: 2
Items: 
Size: 719515 Color: 7
Size: 278518 Color: 8

Bin 4359: 1986 of cap free
Amount of items: 2
Items: 
Size: 798814 Color: 11
Size: 199201 Color: 2

Bin 4360: 1992 of cap free
Amount of items: 2
Items: 
Size: 522473 Color: 15
Size: 475536 Color: 19

Bin 4361: 1996 of cap free
Amount of items: 2
Items: 
Size: 742031 Color: 9
Size: 255974 Color: 1

Bin 4362: 2001 of cap free
Amount of items: 2
Items: 
Size: 506150 Color: 16
Size: 491850 Color: 0

Bin 4363: 2019 of cap free
Amount of items: 2
Items: 
Size: 578220 Color: 13
Size: 419762 Color: 3

Bin 4364: 2023 of cap free
Amount of items: 2
Items: 
Size: 539349 Color: 14
Size: 458629 Color: 13

Bin 4365: 2031 of cap free
Amount of items: 2
Items: 
Size: 796926 Color: 19
Size: 201044 Color: 10

Bin 4366: 2061 of cap free
Amount of items: 2
Items: 
Size: 616814 Color: 9
Size: 381126 Color: 2

Bin 4367: 2061 of cap free
Amount of items: 2
Items: 
Size: 590695 Color: 5
Size: 407245 Color: 3

Bin 4368: 2065 of cap free
Amount of items: 2
Items: 
Size: 601309 Color: 7
Size: 396627 Color: 16

Bin 4369: 2073 of cap free
Amount of items: 2
Items: 
Size: 610747 Color: 19
Size: 387181 Color: 1

Bin 4370: 2075 of cap free
Amount of items: 2
Items: 
Size: 792797 Color: 8
Size: 205129 Color: 11

Bin 4371: 2092 of cap free
Amount of items: 2
Items: 
Size: 682605 Color: 14
Size: 315304 Color: 6

Bin 4372: 2098 of cap free
Amount of items: 2
Items: 
Size: 646560 Color: 7
Size: 351343 Color: 5

Bin 4373: 2118 of cap free
Amount of items: 2
Items: 
Size: 516629 Color: 6
Size: 481254 Color: 0

Bin 4374: 2120 of cap free
Amount of items: 2
Items: 
Size: 715093 Color: 17
Size: 282788 Color: 4

Bin 4375: 2120 of cap free
Amount of items: 2
Items: 
Size: 554299 Color: 1
Size: 443582 Color: 0

Bin 4376: 2126 of cap free
Amount of items: 2
Items: 
Size: 598397 Color: 19
Size: 399478 Color: 11

Bin 4377: 2129 of cap free
Amount of items: 2
Items: 
Size: 567683 Color: 4
Size: 430189 Color: 12

Bin 4378: 2137 of cap free
Amount of items: 2
Items: 
Size: 547028 Color: 16
Size: 450836 Color: 13

Bin 4379: 2138 of cap free
Amount of items: 2
Items: 
Size: 646631 Color: 0
Size: 351232 Color: 6

Bin 4380: 2140 of cap free
Amount of items: 2
Items: 
Size: 751375 Color: 9
Size: 246486 Color: 4

Bin 4381: 2145 of cap free
Amount of items: 2
Items: 
Size: 517110 Color: 18
Size: 480746 Color: 4

Bin 4382: 2148 of cap free
Amount of items: 2
Items: 
Size: 574260 Color: 9
Size: 423593 Color: 2

Bin 4383: 2152 of cap free
Amount of items: 2
Items: 
Size: 578095 Color: 19
Size: 419754 Color: 7

Bin 4384: 2157 of cap free
Amount of items: 2
Items: 
Size: 590610 Color: 9
Size: 407234 Color: 13

Bin 4385: 2160 of cap free
Amount of items: 2
Items: 
Size: 516600 Color: 8
Size: 481241 Color: 5

Bin 4386: 2172 of cap free
Amount of items: 2
Items: 
Size: 585342 Color: 2
Size: 412487 Color: 4

Bin 4387: 2194 of cap free
Amount of items: 2
Items: 
Size: 723807 Color: 19
Size: 274000 Color: 16

Bin 4388: 2242 of cap free
Amount of items: 2
Items: 
Size: 590579 Color: 1
Size: 407180 Color: 7

Bin 4389: 2248 of cap free
Amount of items: 2
Items: 
Size: 554053 Color: 7
Size: 443700 Color: 4

Bin 4390: 2258 of cap free
Amount of items: 2
Items: 
Size: 798635 Color: 14
Size: 199108 Color: 10

Bin 4391: 2266 of cap free
Amount of items: 2
Items: 
Size: 547031 Color: 6
Size: 450704 Color: 14

Bin 4392: 2274 of cap free
Amount of items: 2
Items: 
Size: 516501 Color: 11
Size: 481226 Color: 14

Bin 4393: 2277 of cap free
Amount of items: 3
Items: 
Size: 414666 Color: 5
Size: 358947 Color: 18
Size: 224111 Color: 3

Bin 4394: 2322 of cap free
Amount of items: 3
Items: 
Size: 500863 Color: 0
Size: 383361 Color: 3
Size: 113455 Color: 4

Bin 4395: 2327 of cap free
Amount of items: 2
Items: 
Size: 520103 Color: 15
Size: 477571 Color: 11

Bin 4396: 2379 of cap free
Amount of items: 2
Items: 
Size: 567635 Color: 19
Size: 429987 Color: 12

Bin 4397: 2461 of cap free
Amount of items: 2
Items: 
Size: 501469 Color: 15
Size: 496071 Color: 16

Bin 4398: 2465 of cap free
Amount of items: 2
Items: 
Size: 553900 Color: 14
Size: 443636 Color: 9

Bin 4399: 2471 of cap free
Amount of items: 2
Items: 
Size: 646535 Color: 19
Size: 350995 Color: 6

Bin 4400: 2481 of cap free
Amount of items: 2
Items: 
Size: 638689 Color: 6
Size: 358831 Color: 12

Bin 4401: 2493 of cap free
Amount of items: 2
Items: 
Size: 646627 Color: 12
Size: 350881 Color: 0

Bin 4402: 2494 of cap free
Amount of items: 2
Items: 
Size: 546838 Color: 18
Size: 450669 Color: 17

Bin 4403: 2533 of cap free
Amount of items: 3
Items: 
Size: 371589 Color: 7
Size: 338157 Color: 11
Size: 287722 Color: 12

Bin 4404: 2559 of cap free
Amount of items: 3
Items: 
Size: 492187 Color: 6
Size: 305005 Color: 9
Size: 200250 Color: 6

Bin 4405: 2562 of cap free
Amount of items: 2
Items: 
Size: 798634 Color: 7
Size: 198805 Color: 17

Bin 4406: 2584 of cap free
Amount of items: 3
Items: 
Size: 501808 Color: 5
Size: 304192 Color: 0
Size: 191417 Color: 18

Bin 4407: 2614 of cap free
Amount of items: 2
Items: 
Size: 553858 Color: 11
Size: 443529 Color: 13

Bin 4408: 2623 of cap free
Amount of items: 2
Items: 
Size: 539274 Color: 19
Size: 458104 Color: 11

Bin 4409: 2644 of cap free
Amount of items: 2
Items: 
Size: 673706 Color: 18
Size: 323651 Color: 15

Bin 4410: 2676 of cap free
Amount of items: 2
Items: 
Size: 608053 Color: 15
Size: 389272 Color: 18

Bin 4411: 2678 of cap free
Amount of items: 2
Items: 
Size: 546699 Color: 13
Size: 450624 Color: 8

Bin 4412: 2703 of cap free
Amount of items: 2
Items: 
Size: 658930 Color: 14
Size: 338368 Color: 16

Bin 4413: 2718 of cap free
Amount of items: 2
Items: 
Size: 504982 Color: 6
Size: 492301 Color: 15

Bin 4414: 2719 of cap free
Amount of items: 2
Items: 
Size: 505543 Color: 2
Size: 491739 Color: 0

Bin 4415: 2727 of cap free
Amount of items: 2
Items: 
Size: 525439 Color: 19
Size: 471835 Color: 14

Bin 4416: 2757 of cap free
Amount of items: 2
Items: 
Size: 525424 Color: 17
Size: 471820 Color: 5

Bin 4417: 2760 of cap free
Amount of items: 2
Items: 
Size: 668957 Color: 19
Size: 328284 Color: 15

Bin 4418: 2770 of cap free
Amount of items: 2
Items: 
Size: 539279 Color: 8
Size: 457952 Color: 18

Bin 4419: 2802 of cap free
Amount of items: 2
Items: 
Size: 704012 Color: 4
Size: 293187 Color: 3

Bin 4420: 2807 of cap free
Amount of items: 2
Items: 
Size: 783929 Color: 7
Size: 213265 Color: 16

Bin 4421: 2823 of cap free
Amount of items: 2
Items: 
Size: 590428 Color: 18
Size: 406750 Color: 5

Bin 4422: 2837 of cap free
Amount of items: 2
Items: 
Size: 623653 Color: 8
Size: 373511 Color: 7

Bin 4423: 2886 of cap free
Amount of items: 2
Items: 
Size: 650680 Color: 18
Size: 346435 Color: 16

Bin 4424: 2895 of cap free
Amount of items: 2
Items: 
Size: 747449 Color: 15
Size: 249657 Color: 5

Bin 4425: 2906 of cap free
Amount of items: 2
Items: 
Size: 723884 Color: 1
Size: 273211 Color: 4

Bin 4426: 2922 of cap free
Amount of items: 2
Items: 
Size: 650849 Color: 9
Size: 346230 Color: 14

Bin 4427: 2954 of cap free
Amount of items: 2
Items: 
Size: 668969 Color: 19
Size: 328078 Color: 4

Bin 4428: 2980 of cap free
Amount of items: 2
Items: 
Size: 650819 Color: 11
Size: 346202 Color: 10

Bin 4429: 2999 of cap free
Amount of items: 2
Items: 
Size: 607611 Color: 19
Size: 389391 Color: 0

Bin 4430: 3006 of cap free
Amount of items: 2
Items: 
Size: 673575 Color: 7
Size: 323420 Color: 15

Bin 4431: 3027 of cap free
Amount of items: 2
Items: 
Size: 638765 Color: 17
Size: 358209 Color: 14

Bin 4432: 3051 of cap free
Amount of items: 2
Items: 
Size: 543025 Color: 5
Size: 453925 Color: 14

Bin 4433: 3080 of cap free
Amount of items: 2
Items: 
Size: 646160 Color: 6
Size: 350761 Color: 5

Bin 4434: 3090 of cap free
Amount of items: 2
Items: 
Size: 525412 Color: 5
Size: 471499 Color: 15

Bin 4435: 3125 of cap free
Amount of items: 2
Items: 
Size: 731231 Color: 5
Size: 265645 Color: 15

Bin 4436: 3148 of cap free
Amount of items: 2
Items: 
Size: 573344 Color: 7
Size: 423509 Color: 13

Bin 4437: 3159 of cap free
Amount of items: 2
Items: 
Size: 597511 Color: 19
Size: 399331 Color: 16

Bin 4438: 3161 of cap free
Amount of items: 2
Items: 
Size: 731202 Color: 2
Size: 265638 Color: 18

Bin 4439: 3200 of cap free
Amount of items: 2
Items: 
Size: 655008 Color: 12
Size: 341793 Color: 11

Bin 4440: 3218 of cap free
Amount of items: 2
Items: 
Size: 685591 Color: 13
Size: 311192 Color: 3

Bin 4441: 3232 of cap free
Amount of items: 2
Items: 
Size: 650610 Color: 1
Size: 346159 Color: 9

Bin 4442: 3234 of cap free
Amount of items: 2
Items: 
Size: 668625 Color: 10
Size: 328142 Color: 17

Bin 4443: 3254 of cap free
Amount of items: 2
Items: 
Size: 516569 Color: 14
Size: 480178 Color: 2

Bin 4444: 3277 of cap free
Amount of items: 2
Items: 
Size: 723482 Color: 8
Size: 273242 Color: 17

Bin 4445: 3296 of cap free
Amount of items: 2
Items: 
Size: 646106 Color: 19
Size: 350599 Color: 15

Bin 4446: 3417 of cap free
Amount of items: 3
Items: 
Size: 377498 Color: 4
Size: 346250 Color: 18
Size: 272836 Color: 1

Bin 4447: 3442 of cap free
Amount of items: 2
Items: 
Size: 633635 Color: 10
Size: 362924 Color: 14

Bin 4448: 3517 of cap free
Amount of items: 2
Items: 
Size: 525219 Color: 6
Size: 471265 Color: 19

Bin 4449: 3583 of cap free
Amount of items: 2
Items: 
Size: 789140 Color: 8
Size: 207278 Color: 15

Bin 4450: 3621 of cap free
Amount of items: 2
Items: 
Size: 525182 Color: 13
Size: 471198 Color: 12

Bin 4451: 3758 of cap free
Amount of items: 2
Items: 
Size: 572867 Color: 9
Size: 423376 Color: 5

Bin 4452: 3858 of cap free
Amount of items: 2
Items: 
Size: 589536 Color: 7
Size: 406607 Color: 10

Bin 4453: 3878 of cap free
Amount of items: 2
Items: 
Size: 572776 Color: 3
Size: 423347 Color: 4

Bin 4454: 3942 of cap free
Amount of items: 2
Items: 
Size: 572747 Color: 9
Size: 423312 Color: 12

Bin 4455: 4063 of cap free
Amount of items: 2
Items: 
Size: 567600 Color: 17
Size: 428338 Color: 13

Bin 4456: 4189 of cap free
Amount of items: 2
Items: 
Size: 567555 Color: 14
Size: 428257 Color: 15

Bin 4457: 4226 of cap free
Amount of items: 3
Items: 
Size: 717604 Color: 2
Size: 155141 Color: 10
Size: 123030 Color: 10

Bin 4458: 4278 of cap free
Amount of items: 2
Items: 
Size: 567543 Color: 10
Size: 428180 Color: 11

Bin 4459: 4288 of cap free
Amount of items: 2
Items: 
Size: 567541 Color: 11
Size: 428172 Color: 0

Bin 4460: 4379 of cap free
Amount of items: 2
Items: 
Size: 572733 Color: 2
Size: 422889 Color: 18

Bin 4461: 4410 of cap free
Amount of items: 2
Items: 
Size: 576562 Color: 6
Size: 419029 Color: 11

Bin 4462: 4460 of cap free
Amount of items: 2
Items: 
Size: 667786 Color: 1
Size: 327755 Color: 13

Bin 4463: 4481 of cap free
Amount of items: 2
Items: 
Size: 668419 Color: 2
Size: 327101 Color: 9

Bin 4464: 4709 of cap free
Amount of items: 2
Items: 
Size: 537382 Color: 18
Size: 457910 Color: 8

Bin 4465: 4823 of cap free
Amount of items: 2
Items: 
Size: 544592 Color: 14
Size: 450586 Color: 17

Bin 4466: 4903 of cap free
Amount of items: 2
Items: 
Size: 721890 Color: 2
Size: 273208 Color: 1

Bin 4467: 5220 of cap free
Amount of items: 2
Items: 
Size: 667763 Color: 10
Size: 327018 Color: 12

Bin 4468: 5250 of cap free
Amount of items: 2
Items: 
Size: 607600 Color: 10
Size: 387151 Color: 18

Bin 4469: 5259 of cap free
Amount of items: 2
Items: 
Size: 597485 Color: 0
Size: 397257 Color: 13

Bin 4470: 5261 of cap free
Amount of items: 2
Items: 
Size: 607588 Color: 15
Size: 387152 Color: 2

Bin 4471: 5307 of cap free
Amount of items: 2
Items: 
Size: 797968 Color: 19
Size: 196726 Color: 0

Bin 4472: 5358 of cap free
Amount of items: 2
Items: 
Size: 597448 Color: 14
Size: 397195 Color: 11

Bin 4473: 5496 of cap free
Amount of items: 2
Items: 
Size: 597251 Color: 11
Size: 397254 Color: 0

Bin 4474: 5706 of cap free
Amount of items: 2
Items: 
Size: 566449 Color: 16
Size: 427846 Color: 11

Bin 4475: 6015 of cap free
Amount of items: 2
Items: 
Size: 531736 Color: 12
Size: 462250 Color: 14

Bin 4476: 6034 of cap free
Amount of items: 2
Items: 
Size: 597332 Color: 19
Size: 396635 Color: 4

Bin 4477: 6145 of cap free
Amount of items: 2
Items: 
Size: 566474 Color: 11
Size: 427382 Color: 8

Bin 4478: 6200 of cap free
Amount of items: 2
Items: 
Size: 597226 Color: 3
Size: 396575 Color: 15

Bin 4479: 6286 of cap free
Amount of items: 2
Items: 
Size: 606620 Color: 9
Size: 387095 Color: 17

Bin 4480: 6372 of cap free
Amount of items: 2
Items: 
Size: 700178 Color: 3
Size: 293451 Color: 2

Bin 4481: 6442 of cap free
Amount of items: 2
Items: 
Size: 631452 Color: 7
Size: 362107 Color: 14

Bin 4482: 6460 of cap free
Amount of items: 2
Items: 
Size: 596986 Color: 1
Size: 396555 Color: 5

Bin 4483: 6643 of cap free
Amount of items: 2
Items: 
Size: 543004 Color: 16
Size: 450354 Color: 0

Bin 4484: 6680 of cap free
Amount of items: 2
Items: 
Size: 747192 Color: 0
Size: 246129 Color: 5

Bin 4485: 6777 of cap free
Amount of items: 2
Items: 
Size: 542953 Color: 10
Size: 450271 Color: 14

Bin 4486: 7123 of cap free
Amount of items: 2
Items: 
Size: 542952 Color: 17
Size: 449926 Color: 19

Bin 4487: 7250 of cap free
Amount of items: 2
Items: 
Size: 500578 Color: 11
Size: 492173 Color: 16

Bin 4488: 7366 of cap free
Amount of items: 2
Items: 
Size: 596556 Color: 16
Size: 396079 Color: 12

Bin 4489: 7378 of cap free
Amount of items: 2
Items: 
Size: 542720 Color: 16
Size: 449903 Color: 10

Bin 4490: 7516 of cap free
Amount of items: 2
Items: 
Size: 542584 Color: 16
Size: 449901 Color: 18

Bin 4491: 7583 of cap free
Amount of items: 2
Items: 
Size: 596446 Color: 18
Size: 395972 Color: 16

Bin 4492: 7673 of cap free
Amount of items: 2
Items: 
Size: 542582 Color: 1
Size: 449746 Color: 0

Bin 4493: 7689 of cap free
Amount of items: 2
Items: 
Size: 654066 Color: 2
Size: 338246 Color: 11

Bin 4494: 7874 of cap free
Amount of items: 2
Items: 
Size: 542413 Color: 6
Size: 449714 Color: 18

Bin 4495: 7971 of cap free
Amount of items: 2
Items: 
Size: 542392 Color: 4
Size: 449638 Color: 0

Bin 4496: 8607 of cap free
Amount of items: 2
Items: 
Size: 793773 Color: 17
Size: 197621 Color: 8

Bin 4497: 8861 of cap free
Amount of items: 2
Items: 
Size: 730765 Color: 10
Size: 260375 Color: 7

Bin 4498: 9925 of cap free
Amount of items: 2
Items: 
Size: 771144 Color: 16
Size: 218932 Color: 0

Bin 4499: 10043 of cap free
Amount of items: 2
Items: 
Size: 643838 Color: 3
Size: 346120 Color: 11

Bin 4500: 10566 of cap free
Amount of items: 2
Items: 
Size: 795827 Color: 0
Size: 193608 Color: 17

Bin 4501: 10939 of cap free
Amount of items: 2
Items: 
Size: 642981 Color: 15
Size: 346081 Color: 13

Bin 4502: 11010 of cap free
Amount of items: 2
Items: 
Size: 642974 Color: 16
Size: 346017 Color: 5

Bin 4503: 11138 of cap free
Amount of items: 2
Items: 
Size: 539251 Color: 16
Size: 449612 Color: 0

Bin 4504: 12519 of cap free
Amount of items: 2
Items: 
Size: 564617 Color: 16
Size: 422865 Color: 8

Bin 4505: 12616 of cap free
Amount of items: 3
Items: 
Size: 666072 Color: 10
Size: 172288 Color: 17
Size: 149025 Color: 4

Bin 4506: 12890 of cap free
Amount of items: 2
Items: 
Size: 564568 Color: 10
Size: 422543 Color: 9

Bin 4507: 14147 of cap free
Amount of items: 2
Items: 
Size: 563775 Color: 14
Size: 422079 Color: 15

Bin 4508: 14717 of cap free
Amount of items: 2
Items: 
Size: 563160 Color: 7
Size: 422124 Color: 17

Bin 4509: 16733 of cap free
Amount of items: 2
Items: 
Size: 561223 Color: 6
Size: 422045 Color: 5

Bin 4510: 18601 of cap free
Amount of items: 2
Items: 
Size: 531872 Color: 19
Size: 449528 Color: 3

Bin 4511: 19325 of cap free
Amount of items: 2
Items: 
Size: 490340 Color: 8
Size: 490336 Color: 5

Bin 4512: 19525 of cap free
Amount of items: 2
Items: 
Size: 490328 Color: 1
Size: 490148 Color: 8

Bin 4513: 23733 of cap free
Amount of items: 2
Items: 
Size: 614076 Color: 14
Size: 362192 Color: 6

Bin 4514: 28387 of cap free
Amount of items: 2
Items: 
Size: 561789 Color: 17
Size: 409825 Color: 1

Bin 4515: 44427 of cap free
Amount of items: 3
Items: 
Size: 682639 Color: 19
Size: 168181 Color: 6
Size: 104754 Color: 4

Total size: 4513473731
Total free space: 1530784

