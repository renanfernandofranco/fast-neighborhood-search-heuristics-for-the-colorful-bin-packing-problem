Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 4
Size: 257 Color: 10
Size: 250 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 339 Color: 14
Size: 253 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 18
Size: 290 Color: 1
Size: 281 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 19
Size: 255 Color: 1
Size: 250 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 9
Size: 298 Color: 2
Size: 252 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 329 Color: 5
Size: 250 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 12
Size: 356 Color: 5
Size: 278 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 281 Color: 0
Size: 273 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 12
Size: 272 Color: 9
Size: 271 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 358 Color: 15
Size: 265 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 322 Color: 13
Size: 283 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 268 Color: 13
Size: 250 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 18
Size: 264 Color: 13
Size: 260 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 370 Color: 5
Size: 255 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 343 Color: 13
Size: 278 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 329 Color: 0
Size: 283 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 357 Color: 9
Size: 260 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9
Size: 266 Color: 17
Size: 261 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 13
Size: 309 Color: 18
Size: 293 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 15
Size: 266 Color: 11
Size: 250 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 5
Size: 361 Color: 18
Size: 264 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 12
Size: 280 Color: 18
Size: 260 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 14
Size: 293 Color: 5
Size: 252 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 18
Size: 340 Color: 12
Size: 269 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 272 Color: 15
Size: 255 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 5
Size: 252 Color: 18
Size: 250 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 6
Size: 323 Color: 13
Size: 266 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 10
Size: 296 Color: 10
Size: 254 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 326 Color: 8
Size: 277 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 13
Size: 261 Color: 19
Size: 251 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 13
Size: 348 Color: 14
Size: 298 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 260 Color: 1
Size: 254 Color: 4
Size: 486 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 13
Size: 284 Color: 2
Size: 269 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 14
Size: 347 Color: 16
Size: 253 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 18
Size: 255 Color: 12
Size: 250 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 18
Size: 267 Color: 5
Size: 253 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 12
Size: 348 Color: 12
Size: 302 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 16
Size: 257 Color: 12
Size: 251 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 8
Size: 294 Color: 18
Size: 259 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 341 Color: 12
Size: 282 Color: 5

Total size: 40000
Total free space: 0

