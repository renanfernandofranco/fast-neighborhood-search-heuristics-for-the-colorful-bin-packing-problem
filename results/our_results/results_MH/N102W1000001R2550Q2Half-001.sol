Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 343636 Color: 1
Size: 316374 Color: 0
Size: 339991 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 355113 Color: 1
Size: 338434 Color: 1
Size: 306454 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 357787 Color: 1
Size: 335039 Color: 1
Size: 307175 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 371630 Color: 1
Size: 317563 Color: 1
Size: 310808 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 376896 Color: 1
Size: 349872 Color: 1
Size: 273233 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 379447 Color: 1
Size: 315545 Color: 1
Size: 305009 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 381132 Color: 1
Size: 337061 Color: 1
Size: 281808 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 381493 Color: 1
Size: 315903 Color: 1
Size: 302605 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 383632 Color: 1
Size: 365743 Color: 1
Size: 250626 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 385457 Color: 1
Size: 355802 Color: 1
Size: 258742 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 386339 Color: 1
Size: 339695 Color: 1
Size: 273967 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 389326 Color: 1
Size: 327975 Color: 1
Size: 282700 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 390395 Color: 1
Size: 331056 Color: 1
Size: 278550 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 405400 Color: 1
Size: 307000 Color: 1
Size: 287601 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 407901 Color: 1
Size: 324995 Color: 1
Size: 267105 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 409534 Color: 1
Size: 332989 Color: 1
Size: 257478 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 410493 Color: 1
Size: 327836 Color: 1
Size: 261672 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 410649 Color: 1
Size: 316096 Color: 1
Size: 273256 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 415837 Color: 1
Size: 308123 Color: 1
Size: 276041 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 417369 Color: 1
Size: 313051 Color: 1
Size: 269581 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 435349 Color: 1
Size: 284235 Color: 1
Size: 280417 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 437665 Color: 1
Size: 307402 Color: 1
Size: 254934 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 438857 Color: 1
Size: 283378 Color: 1
Size: 277766 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 446234 Color: 1
Size: 289478 Color: 1
Size: 264289 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 446764 Color: 1
Size: 295338 Color: 1
Size: 257899 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 457630 Color: 1
Size: 278477 Color: 1
Size: 263894 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 461773 Color: 1
Size: 288045 Color: 1
Size: 250183 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 462366 Color: 1
Size: 281514 Color: 1
Size: 256121 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 463530 Color: 1
Size: 284371 Color: 1
Size: 252100 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 467080 Color: 1
Size: 276380 Color: 1
Size: 256541 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 477808 Color: 1
Size: 271241 Color: 1
Size: 250952 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 496293 Color: 1
Size: 251890 Color: 1
Size: 251818 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 496451 Color: 1
Size: 251912 Color: 1
Size: 251638 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 499234 Color: 1
Size: 250465 Color: 1
Size: 250302 Color: 0

Total size: 34000034
Total free space: 0

