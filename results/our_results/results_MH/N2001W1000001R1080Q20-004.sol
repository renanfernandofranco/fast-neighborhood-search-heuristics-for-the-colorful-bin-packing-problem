Capicity Bin: 1000001
Lower Bound: 887

Bins used: 887
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 598761 Color: 14
Size: 280725 Color: 15
Size: 120515 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 446362 Color: 19
Size: 377094 Color: 12
Size: 176545 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 598677 Color: 7
Size: 276613 Color: 7
Size: 124711 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 705695 Color: 12
Size: 161024 Color: 6
Size: 133282 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 699509 Color: 4
Size: 184210 Color: 3
Size: 116282 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 754030 Color: 8
Size: 126872 Color: 15
Size: 119099 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 693812 Color: 9
Size: 182705 Color: 6
Size: 123484 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 619649 Color: 19
Size: 230725 Color: 10
Size: 149627 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 598516 Color: 17
Size: 216518 Color: 12
Size: 184967 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 633179 Color: 19
Size: 217499 Color: 4
Size: 149323 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 481786 Color: 17
Size: 390307 Color: 18
Size: 127908 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 582943 Color: 11
Size: 311288 Color: 11
Size: 105770 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 603842 Color: 1
Size: 280976 Color: 15
Size: 115183 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 528715 Color: 14
Size: 254818 Color: 15
Size: 216468 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 553936 Color: 5
Size: 314266 Color: 0
Size: 131799 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 650348 Color: 3
Size: 179128 Color: 13
Size: 170525 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 532293 Color: 13
Size: 297727 Color: 11
Size: 169981 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 750408 Color: 16
Size: 135455 Color: 1
Size: 114138 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 622593 Color: 8
Size: 267726 Color: 14
Size: 109682 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 448533 Color: 17
Size: 371193 Color: 6
Size: 180275 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 713525 Color: 2
Size: 166874 Color: 15
Size: 119602 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 370287 Color: 2
Size: 338530 Color: 9
Size: 291184 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 418558 Color: 8
Size: 291956 Color: 0
Size: 289487 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 355654 Color: 14
Size: 329857 Color: 6
Size: 314490 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 482018 Color: 1
Size: 348688 Color: 13
Size: 169295 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 436454 Color: 7
Size: 411178 Color: 0
Size: 152369 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 603756 Color: 6
Size: 276883 Color: 10
Size: 119362 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 709676 Color: 17
Size: 161439 Color: 13
Size: 128886 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 567731 Color: 4
Size: 280051 Color: 14
Size: 152219 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 650618 Color: 11
Size: 192933 Color: 5
Size: 156450 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 530822 Color: 8
Size: 312710 Color: 15
Size: 156469 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 494473 Color: 14
Size: 374218 Color: 1
Size: 131310 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 509613 Color: 8
Size: 328212 Color: 6
Size: 162176 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 455483 Color: 16
Size: 417568 Color: 10
Size: 126950 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 604348 Color: 0
Size: 284109 Color: 18
Size: 111544 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 650449 Color: 3
Size: 193540 Color: 7
Size: 156012 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 604006 Color: 15
Size: 198951 Color: 14
Size: 197044 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 472201 Color: 11
Size: 373218 Color: 11
Size: 154582 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 675219 Color: 4
Size: 181578 Color: 4
Size: 143204 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 508942 Color: 19
Size: 372847 Color: 18
Size: 118212 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 677103 Color: 1
Size: 182364 Color: 4
Size: 140534 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 582761 Color: 19
Size: 311678 Color: 16
Size: 105562 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 566932 Color: 2
Size: 288288 Color: 14
Size: 144781 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 341083 Color: 3
Size: 333926 Color: 11
Size: 324992 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 598657 Color: 5
Size: 289224 Color: 9
Size: 112120 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 753147 Color: 16
Size: 142024 Color: 6
Size: 104830 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 562356 Color: 15
Size: 325067 Color: 11
Size: 112578 Color: 10

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 562303 Color: 11
Size: 309910 Color: 12
Size: 127788 Color: 14

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 566901 Color: 8
Size: 258384 Color: 1
Size: 174716 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 733887 Color: 16
Size: 165136 Color: 10
Size: 100978 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 724217 Color: 0
Size: 153934 Color: 4
Size: 121850 Color: 9

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 738037 Color: 18
Size: 142483 Color: 9
Size: 119481 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 733519 Color: 7
Size: 165925 Color: 12
Size: 100557 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 695981 Color: 5
Size: 190907 Color: 8
Size: 113113 Color: 18

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 696746 Color: 13
Size: 188768 Color: 4
Size: 114487 Color: 12

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 675538 Color: 4
Size: 185452 Color: 3
Size: 139011 Color: 10

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 721177 Color: 5
Size: 176712 Color: 1
Size: 102112 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 657419 Color: 15
Size: 175610 Color: 10
Size: 166972 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 352816 Color: 8
Size: 333471 Color: 18
Size: 313714 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 704410 Color: 1
Size: 179308 Color: 8
Size: 116283 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 762554 Color: 12
Size: 134998 Color: 19
Size: 102449 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 450984 Color: 14
Size: 446455 Color: 5
Size: 102562 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 605095 Color: 0
Size: 289893 Color: 16
Size: 105013 Color: 10

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 642302 Color: 14
Size: 357699 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 618613 Color: 6
Size: 191065 Color: 12
Size: 190323 Color: 16

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 683355 Color: 8
Size: 316646 Color: 10

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 684714 Color: 14
Size: 160794 Color: 17
Size: 154493 Color: 9

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 733874 Color: 7
Size: 140079 Color: 13
Size: 126048 Color: 2

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 733974 Color: 16
Size: 266027 Color: 18

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 761273 Color: 0
Size: 124373 Color: 14
Size: 114355 Color: 9

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 719790 Color: 19
Size: 141847 Color: 19
Size: 138363 Color: 9

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 720470 Color: 10
Size: 162336 Color: 14
Size: 117194 Color: 4

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 722285 Color: 13
Size: 152727 Color: 16
Size: 124988 Color: 3

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 680363 Color: 18
Size: 166079 Color: 9
Size: 153558 Color: 18

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 606252 Color: 19
Size: 288482 Color: 18
Size: 105266 Color: 17

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 508089 Color: 14
Size: 323572 Color: 6
Size: 168339 Color: 15

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 659319 Color: 19
Size: 194782 Color: 15
Size: 145899 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 675895 Color: 19
Size: 207722 Color: 0
Size: 116383 Color: 13

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 772301 Color: 0
Size: 125253 Color: 15
Size: 102446 Color: 7

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 738080 Color: 2
Size: 141446 Color: 12
Size: 120474 Color: 15

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 734065 Color: 16
Size: 134399 Color: 14
Size: 131536 Color: 6

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 453907 Color: 14
Size: 326183 Color: 5
Size: 219910 Color: 2

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 528610 Color: 10
Size: 340029 Color: 15
Size: 131361 Color: 16

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 632122 Color: 6
Size: 197117 Color: 3
Size: 170761 Color: 1

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 446741 Color: 6
Size: 354677 Color: 7
Size: 198582 Color: 5

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 493435 Color: 18
Size: 257049 Color: 0
Size: 249516 Color: 0

Bin 87: 1 of cap free
Amount of items: 2
Items: 
Size: 672968 Color: 7
Size: 327032 Color: 9

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 713167 Color: 18
Size: 157841 Color: 15
Size: 128991 Color: 8

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 695902 Color: 11
Size: 185304 Color: 2
Size: 118793 Color: 7

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 722080 Color: 12
Size: 148896 Color: 0
Size: 129023 Color: 16

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 694311 Color: 8
Size: 168399 Color: 4
Size: 137289 Color: 5

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 596288 Color: 11
Size: 214074 Color: 8
Size: 189637 Color: 17

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 509190 Color: 9
Size: 296012 Color: 1
Size: 194797 Color: 11

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 479406 Color: 2
Size: 406217 Color: 19
Size: 114376 Color: 4

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 653993 Color: 6
Size: 186958 Color: 1
Size: 159048 Color: 13

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 575701 Color: 7
Size: 307814 Color: 18
Size: 116484 Color: 1

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 654773 Color: 2
Size: 176908 Color: 4
Size: 168318 Color: 17

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 492654 Color: 4
Size: 335480 Color: 18
Size: 171865 Color: 16

Bin 99: 2 of cap free
Amount of items: 2
Items: 
Size: 649458 Color: 19
Size: 350541 Color: 3

Bin 100: 2 of cap free
Amount of items: 3
Items: 
Size: 408276 Color: 4
Size: 311520 Color: 3
Size: 280203 Color: 19

Bin 101: 2 of cap free
Amount of items: 2
Items: 
Size: 532791 Color: 11
Size: 467208 Color: 1

Bin 102: 2 of cap free
Amount of items: 2
Items: 
Size: 703902 Color: 5
Size: 296097 Color: 18

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 565696 Color: 7
Size: 268567 Color: 6
Size: 165735 Color: 19

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 439059 Color: 17
Size: 412348 Color: 8
Size: 148591 Color: 0

Bin 105: 3 of cap free
Amount of items: 3
Items: 
Size: 453782 Color: 12
Size: 413741 Color: 5
Size: 132475 Color: 12

Bin 106: 3 of cap free
Amount of items: 3
Items: 
Size: 675760 Color: 16
Size: 188491 Color: 9
Size: 135747 Color: 18

Bin 107: 3 of cap free
Amount of items: 3
Items: 
Size: 454947 Color: 9
Size: 359908 Color: 0
Size: 185143 Color: 10

Bin 108: 3 of cap free
Amount of items: 3
Items: 
Size: 713367 Color: 6
Size: 171506 Color: 19
Size: 115125 Color: 4

Bin 109: 3 of cap free
Amount of items: 2
Items: 
Size: 723274 Color: 2
Size: 276724 Color: 8

Bin 110: 3 of cap free
Amount of items: 3
Items: 
Size: 619209 Color: 13
Size: 192570 Color: 13
Size: 188219 Color: 12

Bin 111: 3 of cap free
Amount of items: 3
Items: 
Size: 617173 Color: 11
Size: 195802 Color: 9
Size: 187023 Color: 1

Bin 112: 3 of cap free
Amount of items: 2
Items: 
Size: 522305 Color: 0
Size: 477693 Color: 1

Bin 113: 4 of cap free
Amount of items: 4
Items: 
Size: 530889 Color: 4
Size: 189506 Color: 17
Size: 154936 Color: 2
Size: 124666 Color: 16

Bin 114: 4 of cap free
Amount of items: 3
Items: 
Size: 757671 Color: 2
Size: 130241 Color: 16
Size: 112085 Color: 2

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 516488 Color: 19
Size: 290038 Color: 6
Size: 193471 Color: 5

Bin 116: 4 of cap free
Amount of items: 2
Items: 
Size: 540223 Color: 11
Size: 459774 Color: 13

Bin 117: 5 of cap free
Amount of items: 3
Items: 
Size: 597249 Color: 7
Size: 252293 Color: 13
Size: 150454 Color: 3

Bin 118: 5 of cap free
Amount of items: 3
Items: 
Size: 739731 Color: 3
Size: 151469 Color: 1
Size: 108796 Color: 0

Bin 119: 5 of cap free
Amount of items: 3
Items: 
Size: 788227 Color: 2
Size: 108855 Color: 12
Size: 102914 Color: 17

Bin 120: 5 of cap free
Amount of items: 3
Items: 
Size: 603946 Color: 4
Size: 286839 Color: 0
Size: 109211 Color: 12

Bin 121: 5 of cap free
Amount of items: 3
Items: 
Size: 665988 Color: 8
Size: 169884 Color: 15
Size: 164124 Color: 16

Bin 122: 5 of cap free
Amount of items: 2
Items: 
Size: 720181 Color: 3
Size: 279815 Color: 13

Bin 123: 5 of cap free
Amount of items: 3
Items: 
Size: 340383 Color: 12
Size: 329809 Color: 13
Size: 329804 Color: 2

Bin 124: 5 of cap free
Amount of items: 2
Items: 
Size: 522410 Color: 5
Size: 477586 Color: 10

Bin 125: 5 of cap free
Amount of items: 2
Items: 
Size: 529264 Color: 11
Size: 470732 Color: 2

Bin 126: 5 of cap free
Amount of items: 2
Items: 
Size: 624407 Color: 15
Size: 375589 Color: 2

Bin 127: 6 of cap free
Amount of items: 3
Items: 
Size: 712285 Color: 5
Size: 152420 Color: 11
Size: 135290 Color: 9

Bin 128: 6 of cap free
Amount of items: 3
Items: 
Size: 757632 Color: 10
Size: 141965 Color: 17
Size: 100398 Color: 3

Bin 129: 6 of cap free
Amount of items: 2
Items: 
Size: 705031 Color: 9
Size: 294964 Color: 13

Bin 130: 7 of cap free
Amount of items: 3
Items: 
Size: 712998 Color: 11
Size: 149012 Color: 8
Size: 137984 Color: 2

Bin 131: 7 of cap free
Amount of items: 3
Items: 
Size: 762726 Color: 10
Size: 123673 Color: 3
Size: 113595 Color: 3

Bin 132: 8 of cap free
Amount of items: 3
Items: 
Size: 528461 Color: 10
Size: 281371 Color: 13
Size: 190161 Color: 3

Bin 133: 8 of cap free
Amount of items: 3
Items: 
Size: 631576 Color: 11
Size: 195303 Color: 10
Size: 173114 Color: 18

Bin 134: 8 of cap free
Amount of items: 2
Items: 
Size: 784560 Color: 10
Size: 215433 Color: 14

Bin 135: 8 of cap free
Amount of items: 2
Items: 
Size: 555808 Color: 19
Size: 444185 Color: 10

Bin 136: 8 of cap free
Amount of items: 2
Items: 
Size: 609623 Color: 14
Size: 390370 Color: 15

Bin 137: 8 of cap free
Amount of items: 2
Items: 
Size: 692912 Color: 4
Size: 307081 Color: 12

Bin 138: 9 of cap free
Amount of items: 2
Items: 
Size: 714593 Color: 4
Size: 285399 Color: 12

Bin 139: 10 of cap free
Amount of items: 3
Items: 
Size: 357292 Color: 1
Size: 329731 Color: 13
Size: 312968 Color: 16

Bin 140: 10 of cap free
Amount of items: 3
Items: 
Size: 536040 Color: 1
Size: 337900 Color: 15
Size: 126051 Color: 6

Bin 141: 10 of cap free
Amount of items: 3
Items: 
Size: 727515 Color: 3
Size: 143293 Color: 3
Size: 129183 Color: 8

Bin 142: 11 of cap free
Amount of items: 3
Items: 
Size: 765607 Color: 3
Size: 128902 Color: 1
Size: 105481 Color: 11

Bin 143: 11 of cap free
Amount of items: 3
Items: 
Size: 583036 Color: 8
Size: 279601 Color: 0
Size: 137353 Color: 1

Bin 144: 12 of cap free
Amount of items: 3
Items: 
Size: 573185 Color: 12
Size: 255719 Color: 18
Size: 171085 Color: 17

Bin 145: 12 of cap free
Amount of items: 3
Items: 
Size: 511706 Color: 0
Size: 311015 Color: 8
Size: 177268 Color: 18

Bin 146: 12 of cap free
Amount of items: 3
Items: 
Size: 604080 Color: 15
Size: 216537 Color: 4
Size: 179372 Color: 5

Bin 147: 12 of cap free
Amount of items: 3
Items: 
Size: 750984 Color: 15
Size: 132840 Color: 8
Size: 116165 Color: 19

Bin 148: 12 of cap free
Amount of items: 2
Items: 
Size: 530176 Color: 6
Size: 469813 Color: 14

Bin 149: 12 of cap free
Amount of items: 2
Items: 
Size: 699613 Color: 19
Size: 300376 Color: 15

Bin 150: 13 of cap free
Amount of items: 3
Items: 
Size: 757530 Color: 6
Size: 125322 Color: 0
Size: 117136 Color: 16

Bin 151: 14 of cap free
Amount of items: 3
Items: 
Size: 767986 Color: 1
Size: 120980 Color: 17
Size: 111021 Color: 9

Bin 152: 14 of cap free
Amount of items: 2
Items: 
Size: 620567 Color: 16
Size: 379420 Color: 18

Bin 153: 14 of cap free
Amount of items: 3
Items: 
Size: 343854 Color: 1
Size: 342197 Color: 14
Size: 313936 Color: 3

Bin 154: 14 of cap free
Amount of items: 2
Items: 
Size: 577054 Color: 8
Size: 422933 Color: 4

Bin 155: 14 of cap free
Amount of items: 2
Items: 
Size: 751762 Color: 13
Size: 248225 Color: 6

Bin 156: 15 of cap free
Amount of items: 3
Items: 
Size: 712170 Color: 3
Size: 157339 Color: 0
Size: 130477 Color: 15

Bin 157: 15 of cap free
Amount of items: 3
Items: 
Size: 354858 Color: 15
Size: 333619 Color: 13
Size: 311509 Color: 7

Bin 158: 15 of cap free
Amount of items: 2
Items: 
Size: 678772 Color: 14
Size: 321214 Color: 11

Bin 159: 15 of cap free
Amount of items: 2
Items: 
Size: 629156 Color: 14
Size: 370830 Color: 18

Bin 160: 15 of cap free
Amount of items: 2
Items: 
Size: 584790 Color: 15
Size: 415196 Color: 2

Bin 161: 17 of cap free
Amount of items: 3
Items: 
Size: 572848 Color: 9
Size: 289929 Color: 18
Size: 137207 Color: 9

Bin 162: 17 of cap free
Amount of items: 2
Items: 
Size: 614074 Color: 5
Size: 385910 Color: 16

Bin 163: 17 of cap free
Amount of items: 2
Items: 
Size: 733169 Color: 13
Size: 266815 Color: 18

Bin 164: 18 of cap free
Amount of items: 3
Items: 
Size: 345358 Color: 18
Size: 341373 Color: 7
Size: 313252 Color: 15

Bin 165: 18 of cap free
Amount of items: 2
Items: 
Size: 603156 Color: 0
Size: 396827 Color: 15

Bin 166: 18 of cap free
Amount of items: 2
Items: 
Size: 627457 Color: 13
Size: 372526 Color: 12

Bin 167: 18 of cap free
Amount of items: 2
Items: 
Size: 628829 Color: 9
Size: 371154 Color: 4

Bin 168: 19 of cap free
Amount of items: 3
Items: 
Size: 415746 Color: 17
Size: 294484 Color: 10
Size: 289752 Color: 16

Bin 169: 19 of cap free
Amount of items: 2
Items: 
Size: 767944 Color: 9
Size: 232038 Color: 13

Bin 170: 20 of cap free
Amount of items: 3
Items: 
Size: 739005 Color: 5
Size: 155608 Color: 4
Size: 105368 Color: 7

Bin 171: 20 of cap free
Amount of items: 3
Items: 
Size: 695975 Color: 1
Size: 154122 Color: 13
Size: 149884 Color: 12

Bin 172: 20 of cap free
Amount of items: 2
Items: 
Size: 589115 Color: 6
Size: 410866 Color: 7

Bin 173: 20 of cap free
Amount of items: 2
Items: 
Size: 617397 Color: 16
Size: 382584 Color: 12

Bin 174: 20 of cap free
Amount of items: 2
Items: 
Size: 757885 Color: 2
Size: 242096 Color: 14

Bin 175: 21 of cap free
Amount of items: 2
Items: 
Size: 676729 Color: 5
Size: 323251 Color: 17

Bin 176: 22 of cap free
Amount of items: 3
Items: 
Size: 620884 Color: 6
Size: 196365 Color: 16
Size: 182730 Color: 17

Bin 177: 22 of cap free
Amount of items: 2
Items: 
Size: 746743 Color: 18
Size: 253236 Color: 1

Bin 178: 23 of cap free
Amount of items: 3
Items: 
Size: 696283 Color: 7
Size: 157242 Color: 4
Size: 146453 Color: 8

Bin 179: 23 of cap free
Amount of items: 2
Items: 
Size: 549977 Color: 3
Size: 450001 Color: 1

Bin 180: 23 of cap free
Amount of items: 2
Items: 
Size: 614972 Color: 4
Size: 385006 Color: 7

Bin 181: 24 of cap free
Amount of items: 2
Items: 
Size: 605917 Color: 5
Size: 394060 Color: 4

Bin 182: 25 of cap free
Amount of items: 3
Items: 
Size: 351094 Color: 8
Size: 336153 Color: 13
Size: 312729 Color: 12

Bin 183: 25 of cap free
Amount of items: 2
Items: 
Size: 611722 Color: 5
Size: 388254 Color: 0

Bin 184: 25 of cap free
Amount of items: 2
Items: 
Size: 526566 Color: 3
Size: 473410 Color: 5

Bin 185: 26 of cap free
Amount of items: 3
Items: 
Size: 788446 Color: 1
Size: 107080 Color: 2
Size: 104449 Color: 0

Bin 186: 26 of cap free
Amount of items: 2
Items: 
Size: 572126 Color: 15
Size: 427849 Color: 13

Bin 187: 26 of cap free
Amount of items: 2
Items: 
Size: 647623 Color: 6
Size: 352352 Color: 4

Bin 188: 27 of cap free
Amount of items: 3
Items: 
Size: 521464 Color: 15
Size: 291467 Color: 3
Size: 187043 Color: 13

Bin 189: 27 of cap free
Amount of items: 2
Items: 
Size: 510773 Color: 13
Size: 489201 Color: 15

Bin 190: 27 of cap free
Amount of items: 2
Items: 
Size: 558225 Color: 7
Size: 441749 Color: 19

Bin 191: 27 of cap free
Amount of items: 2
Items: 
Size: 570442 Color: 12
Size: 429532 Color: 10

Bin 192: 28 of cap free
Amount of items: 2
Items: 
Size: 593797 Color: 0
Size: 406176 Color: 5

Bin 193: 28 of cap free
Amount of items: 2
Items: 
Size: 728085 Color: 6
Size: 271888 Color: 2

Bin 194: 29 of cap free
Amount of items: 2
Items: 
Size: 707619 Color: 15
Size: 292353 Color: 14

Bin 195: 29 of cap free
Amount of items: 3
Items: 
Size: 602521 Color: 10
Size: 223000 Color: 9
Size: 174451 Color: 7

Bin 196: 30 of cap free
Amount of items: 2
Items: 
Size: 539990 Color: 17
Size: 459981 Color: 6

Bin 197: 30 of cap free
Amount of items: 2
Items: 
Size: 739654 Color: 3
Size: 260317 Color: 7

Bin 198: 31 of cap free
Amount of items: 2
Items: 
Size: 612825 Color: 19
Size: 387145 Color: 15

Bin 199: 31 of cap free
Amount of items: 2
Items: 
Size: 699541 Color: 0
Size: 300429 Color: 13

Bin 200: 31 of cap free
Amount of items: 2
Items: 
Size: 715753 Color: 0
Size: 284217 Color: 13

Bin 201: 32 of cap free
Amount of items: 3
Items: 
Size: 654175 Color: 2
Size: 174304 Color: 9
Size: 171490 Color: 2

Bin 202: 32 of cap free
Amount of items: 2
Items: 
Size: 722532 Color: 3
Size: 277437 Color: 10

Bin 203: 32 of cap free
Amount of items: 2
Items: 
Size: 525687 Color: 5
Size: 474282 Color: 15

Bin 204: 32 of cap free
Amount of items: 3
Items: 
Size: 646065 Color: 5
Size: 193946 Color: 10
Size: 159958 Color: 17

Bin 205: 32 of cap free
Amount of items: 2
Items: 
Size: 611792 Color: 6
Size: 388177 Color: 4

Bin 206: 32 of cap free
Amount of items: 2
Items: 
Size: 763278 Color: 8
Size: 236691 Color: 7

Bin 207: 33 of cap free
Amount of items: 3
Items: 
Size: 566828 Color: 17
Size: 236054 Color: 2
Size: 197086 Color: 2

Bin 208: 33 of cap free
Amount of items: 2
Items: 
Size: 592812 Color: 7
Size: 407156 Color: 2

Bin 209: 33 of cap free
Amount of items: 2
Items: 
Size: 642471 Color: 9
Size: 357497 Color: 1

Bin 210: 34 of cap free
Amount of items: 2
Items: 
Size: 591657 Color: 12
Size: 408310 Color: 4

Bin 211: 35 of cap free
Amount of items: 3
Items: 
Size: 679934 Color: 3
Size: 164904 Color: 11
Size: 155128 Color: 8

Bin 212: 35 of cap free
Amount of items: 2
Items: 
Size: 552478 Color: 12
Size: 447488 Color: 8

Bin 213: 35 of cap free
Amount of items: 2
Items: 
Size: 740085 Color: 4
Size: 259881 Color: 12

Bin 214: 37 of cap free
Amount of items: 3
Items: 
Size: 384871 Color: 18
Size: 325826 Color: 19
Size: 289267 Color: 5

Bin 215: 37 of cap free
Amount of items: 3
Items: 
Size: 341337 Color: 7
Size: 332639 Color: 0
Size: 325988 Color: 8

Bin 216: 37 of cap free
Amount of items: 2
Items: 
Size: 704921 Color: 11
Size: 295043 Color: 9

Bin 217: 37 of cap free
Amount of items: 2
Items: 
Size: 514075 Color: 17
Size: 485889 Color: 2

Bin 218: 37 of cap free
Amount of items: 2
Items: 
Size: 774734 Color: 9
Size: 225230 Color: 14

Bin 219: 38 of cap free
Amount of items: 2
Items: 
Size: 562073 Color: 1
Size: 437890 Color: 14

Bin 220: 38 of cap free
Amount of items: 3
Items: 
Size: 340885 Color: 11
Size: 333501 Color: 8
Size: 325577 Color: 16

Bin 221: 39 of cap free
Amount of items: 2
Items: 
Size: 684206 Color: 19
Size: 315756 Color: 10

Bin 222: 40 of cap free
Amount of items: 3
Items: 
Size: 619198 Color: 6
Size: 190776 Color: 1
Size: 189987 Color: 14

Bin 223: 40 of cap free
Amount of items: 3
Items: 
Size: 370588 Color: 3
Size: 341247 Color: 10
Size: 288126 Color: 14

Bin 224: 40 of cap free
Amount of items: 2
Items: 
Size: 678535 Color: 14
Size: 321426 Color: 2

Bin 225: 41 of cap free
Amount of items: 3
Items: 
Size: 492303 Color: 6
Size: 291370 Color: 3
Size: 216287 Color: 7

Bin 226: 41 of cap free
Amount of items: 2
Items: 
Size: 714759 Color: 10
Size: 285201 Color: 5

Bin 227: 41 of cap free
Amount of items: 2
Items: 
Size: 501638 Color: 3
Size: 498322 Color: 15

Bin 228: 41 of cap free
Amount of items: 2
Items: 
Size: 504306 Color: 17
Size: 495654 Color: 16

Bin 229: 41 of cap free
Amount of items: 2
Items: 
Size: 700506 Color: 2
Size: 299454 Color: 3

Bin 230: 42 of cap free
Amount of items: 3
Items: 
Size: 678333 Color: 2
Size: 175035 Color: 0
Size: 146591 Color: 4

Bin 231: 42 of cap free
Amount of items: 2
Items: 
Size: 791173 Color: 8
Size: 208786 Color: 13

Bin 232: 42 of cap free
Amount of items: 3
Items: 
Size: 556732 Color: 18
Size: 297493 Color: 15
Size: 145734 Color: 10

Bin 233: 42 of cap free
Amount of items: 3
Items: 
Size: 738893 Color: 3
Size: 141720 Color: 14
Size: 119346 Color: 8

Bin 234: 42 of cap free
Amount of items: 2
Items: 
Size: 794586 Color: 14
Size: 205373 Color: 4

Bin 235: 43 of cap free
Amount of items: 2
Items: 
Size: 544005 Color: 7
Size: 455953 Color: 11

Bin 236: 43 of cap free
Amount of items: 2
Items: 
Size: 548352 Color: 3
Size: 451606 Color: 12

Bin 237: 44 of cap free
Amount of items: 2
Items: 
Size: 588265 Color: 2
Size: 411692 Color: 3

Bin 238: 44 of cap free
Amount of items: 2
Items: 
Size: 713079 Color: 19
Size: 286878 Color: 10

Bin 239: 46 of cap free
Amount of items: 3
Items: 
Size: 709281 Color: 17
Size: 175347 Color: 3
Size: 115327 Color: 15

Bin 240: 46 of cap free
Amount of items: 2
Items: 
Size: 601266 Color: 14
Size: 398689 Color: 15

Bin 241: 47 of cap free
Amount of items: 2
Items: 
Size: 534122 Color: 6
Size: 465832 Color: 13

Bin 242: 48 of cap free
Amount of items: 2
Items: 
Size: 500288 Color: 18
Size: 499665 Color: 9

Bin 243: 50 of cap free
Amount of items: 3
Items: 
Size: 621260 Color: 10
Size: 215751 Color: 2
Size: 162940 Color: 1

Bin 244: 50 of cap free
Amount of items: 2
Items: 
Size: 612407 Color: 2
Size: 387544 Color: 12

Bin 245: 51 of cap free
Amount of items: 2
Items: 
Size: 515926 Color: 18
Size: 484024 Color: 11

Bin 246: 51 of cap free
Amount of items: 3
Items: 
Size: 494783 Color: 0
Size: 313431 Color: 13
Size: 191736 Color: 4

Bin 247: 51 of cap free
Amount of items: 2
Items: 
Size: 560029 Color: 13
Size: 439921 Color: 14

Bin 248: 51 of cap free
Amount of items: 2
Items: 
Size: 798490 Color: 4
Size: 201460 Color: 2

Bin 249: 52 of cap free
Amount of items: 2
Items: 
Size: 682189 Color: 16
Size: 317760 Color: 11

Bin 250: 53 of cap free
Amount of items: 2
Items: 
Size: 759000 Color: 16
Size: 240948 Color: 19

Bin 251: 54 of cap free
Amount of items: 2
Items: 
Size: 601503 Color: 4
Size: 398444 Color: 13

Bin 252: 55 of cap free
Amount of items: 2
Items: 
Size: 600459 Color: 18
Size: 399487 Color: 4

Bin 253: 55 of cap free
Amount of items: 2
Items: 
Size: 656193 Color: 14
Size: 343753 Color: 6

Bin 254: 56 of cap free
Amount of items: 3
Items: 
Size: 566962 Color: 8
Size: 289579 Color: 18
Size: 143404 Color: 7

Bin 255: 56 of cap free
Amount of items: 2
Items: 
Size: 610329 Color: 11
Size: 389616 Color: 1

Bin 256: 59 of cap free
Amount of items: 3
Items: 
Size: 368927 Color: 16
Size: 341746 Color: 1
Size: 289269 Color: 11

Bin 257: 59 of cap free
Amount of items: 2
Items: 
Size: 646584 Color: 16
Size: 353358 Color: 1

Bin 258: 60 of cap free
Amount of items: 2
Items: 
Size: 739781 Color: 3
Size: 260160 Color: 4

Bin 259: 62 of cap free
Amount of items: 2
Items: 
Size: 592419 Color: 7
Size: 407520 Color: 14

Bin 260: 63 of cap free
Amount of items: 4
Items: 
Size: 328386 Color: 17
Size: 288973 Color: 13
Size: 239695 Color: 18
Size: 142884 Color: 16

Bin 261: 63 of cap free
Amount of items: 2
Items: 
Size: 540995 Color: 9
Size: 458943 Color: 8

Bin 262: 63 of cap free
Amount of items: 2
Items: 
Size: 633608 Color: 4
Size: 366330 Color: 16

Bin 263: 64 of cap free
Amount of items: 2
Items: 
Size: 788172 Color: 17
Size: 211765 Color: 8

Bin 264: 64 of cap free
Amount of items: 2
Items: 
Size: 564652 Color: 16
Size: 435285 Color: 8

Bin 265: 65 of cap free
Amount of items: 3
Items: 
Size: 678973 Color: 8
Size: 174572 Color: 1
Size: 146391 Color: 11

Bin 266: 65 of cap free
Amount of items: 2
Items: 
Size: 636091 Color: 9
Size: 363845 Color: 18

Bin 267: 65 of cap free
Amount of items: 2
Items: 
Size: 776107 Color: 17
Size: 223829 Color: 2

Bin 268: 66 of cap free
Amount of items: 3
Items: 
Size: 565737 Color: 13
Size: 290417 Color: 1
Size: 143781 Color: 2

Bin 269: 66 of cap free
Amount of items: 2
Items: 
Size: 528183 Color: 19
Size: 471752 Color: 1

Bin 270: 68 of cap free
Amount of items: 3
Items: 
Size: 774036 Color: 12
Size: 119661 Color: 8
Size: 106236 Color: 15

Bin 271: 68 of cap free
Amount of items: 2
Items: 
Size: 551038 Color: 10
Size: 448895 Color: 14

Bin 272: 69 of cap free
Amount of items: 2
Items: 
Size: 740748 Color: 14
Size: 259184 Color: 8

Bin 273: 70 of cap free
Amount of items: 2
Items: 
Size: 779089 Color: 0
Size: 220842 Color: 3

Bin 274: 70 of cap free
Amount of items: 3
Items: 
Size: 671071 Color: 16
Size: 178015 Color: 15
Size: 150845 Color: 6

Bin 275: 70 of cap free
Amount of items: 2
Items: 
Size: 529087 Color: 5
Size: 470844 Color: 9

Bin 276: 70 of cap free
Amount of items: 2
Items: 
Size: 729474 Color: 8
Size: 270457 Color: 15

Bin 277: 71 of cap free
Amount of items: 3
Items: 
Size: 648334 Color: 0
Size: 182666 Color: 3
Size: 168930 Color: 10

Bin 278: 72 of cap free
Amount of items: 3
Items: 
Size: 739041 Color: 13
Size: 145078 Color: 7
Size: 115810 Color: 15

Bin 279: 72 of cap free
Amount of items: 2
Items: 
Size: 603125 Color: 1
Size: 396804 Color: 6

Bin 280: 73 of cap free
Amount of items: 2
Items: 
Size: 653263 Color: 1
Size: 346665 Color: 9

Bin 281: 74 of cap free
Amount of items: 2
Items: 
Size: 770317 Color: 4
Size: 229610 Color: 10

Bin 282: 74 of cap free
Amount of items: 2
Items: 
Size: 524983 Color: 4
Size: 474944 Color: 6

Bin 283: 74 of cap free
Amount of items: 2
Items: 
Size: 640710 Color: 16
Size: 359217 Color: 10

Bin 284: 74 of cap free
Amount of items: 3
Items: 
Size: 795447 Color: 2
Size: 103643 Color: 17
Size: 100837 Color: 9

Bin 285: 75 of cap free
Amount of items: 3
Items: 
Size: 696422 Color: 15
Size: 155376 Color: 11
Size: 148128 Color: 16

Bin 286: 75 of cap free
Amount of items: 2
Items: 
Size: 724471 Color: 1
Size: 275455 Color: 4

Bin 287: 75 of cap free
Amount of items: 2
Items: 
Size: 585398 Color: 14
Size: 414528 Color: 19

Bin 288: 75 of cap free
Amount of items: 2
Items: 
Size: 793901 Color: 19
Size: 206025 Color: 0

Bin 289: 76 of cap free
Amount of items: 3
Items: 
Size: 567575 Color: 4
Size: 250227 Color: 9
Size: 182123 Color: 14

Bin 290: 76 of cap free
Amount of items: 2
Items: 
Size: 577823 Color: 7
Size: 422102 Color: 14

Bin 291: 79 of cap free
Amount of items: 2
Items: 
Size: 518840 Color: 19
Size: 481082 Color: 6

Bin 292: 79 of cap free
Amount of items: 2
Items: 
Size: 617002 Color: 10
Size: 382920 Color: 0

Bin 293: 79 of cap free
Amount of items: 3
Items: 
Size: 621724 Color: 5
Size: 195051 Color: 10
Size: 183147 Color: 17

Bin 294: 80 of cap free
Amount of items: 3
Items: 
Size: 677607 Color: 0
Size: 175350 Color: 19
Size: 146964 Color: 11

Bin 295: 80 of cap free
Amount of items: 2
Items: 
Size: 773352 Color: 14
Size: 226569 Color: 8

Bin 296: 81 of cap free
Amount of items: 2
Items: 
Size: 566098 Color: 7
Size: 433822 Color: 5

Bin 297: 81 of cap free
Amount of items: 2
Items: 
Size: 591779 Color: 4
Size: 408141 Color: 12

Bin 298: 82 of cap free
Amount of items: 3
Items: 
Size: 724561 Color: 5
Size: 151541 Color: 1
Size: 123817 Color: 17

Bin 299: 83 of cap free
Amount of items: 2
Items: 
Size: 589220 Color: 1
Size: 410698 Color: 9

Bin 300: 83 of cap free
Amount of items: 2
Items: 
Size: 661879 Color: 5
Size: 338039 Color: 7

Bin 301: 83 of cap free
Amount of items: 2
Items: 
Size: 704999 Color: 14
Size: 294919 Color: 13

Bin 302: 84 of cap free
Amount of items: 2
Items: 
Size: 622391 Color: 18
Size: 377526 Color: 15

Bin 303: 85 of cap free
Amount of items: 2
Items: 
Size: 652641 Color: 6
Size: 347275 Color: 17

Bin 304: 85 of cap free
Amount of items: 2
Items: 
Size: 537981 Color: 8
Size: 461935 Color: 18

Bin 305: 86 of cap free
Amount of items: 2
Items: 
Size: 749268 Color: 12
Size: 250647 Color: 14

Bin 306: 87 of cap free
Amount of items: 2
Items: 
Size: 513517 Color: 11
Size: 486397 Color: 19

Bin 307: 87 of cap free
Amount of items: 2
Items: 
Size: 576579 Color: 18
Size: 423335 Color: 13

Bin 308: 87 of cap free
Amount of items: 2
Items: 
Size: 588512 Color: 9
Size: 411402 Color: 11

Bin 309: 88 of cap free
Amount of items: 2
Items: 
Size: 764205 Color: 14
Size: 235708 Color: 5

Bin 310: 90 of cap free
Amount of items: 2
Items: 
Size: 774330 Color: 17
Size: 225581 Color: 3

Bin 311: 91 of cap free
Amount of items: 3
Items: 
Size: 495073 Color: 14
Size: 291070 Color: 5
Size: 213767 Color: 1

Bin 312: 92 of cap free
Amount of items: 3
Items: 
Size: 755599 Color: 5
Size: 126839 Color: 1
Size: 117471 Color: 13

Bin 313: 93 of cap free
Amount of items: 2
Items: 
Size: 578031 Color: 9
Size: 421877 Color: 10

Bin 314: 94 of cap free
Amount of items: 2
Items: 
Size: 793166 Color: 7
Size: 206741 Color: 5

Bin 315: 95 of cap free
Amount of items: 2
Items: 
Size: 751969 Color: 1
Size: 247937 Color: 16

Bin 316: 95 of cap free
Amount of items: 2
Items: 
Size: 565047 Color: 8
Size: 434859 Color: 10

Bin 317: 96 of cap free
Amount of items: 3
Items: 
Size: 573882 Color: 1
Size: 253129 Color: 0
Size: 172894 Color: 5

Bin 318: 98 of cap free
Amount of items: 2
Items: 
Size: 689926 Color: 12
Size: 309977 Color: 10

Bin 319: 99 of cap free
Amount of items: 3
Items: 
Size: 646657 Color: 14
Size: 185535 Color: 0
Size: 167710 Color: 8

Bin 320: 99 of cap free
Amount of items: 2
Items: 
Size: 780285 Color: 9
Size: 219617 Color: 14

Bin 321: 99 of cap free
Amount of items: 2
Items: 
Size: 554030 Color: 5
Size: 445872 Color: 18

Bin 322: 99 of cap free
Amount of items: 2
Items: 
Size: 604772 Color: 19
Size: 395130 Color: 8

Bin 323: 99 of cap free
Amount of items: 2
Items: 
Size: 759732 Color: 19
Size: 240170 Color: 7

Bin 324: 102 of cap free
Amount of items: 2
Items: 
Size: 548466 Color: 19
Size: 451433 Color: 16

Bin 325: 104 of cap free
Amount of items: 2
Items: 
Size: 688168 Color: 11
Size: 311729 Color: 15

Bin 326: 105 of cap free
Amount of items: 2
Items: 
Size: 790970 Color: 12
Size: 208926 Color: 18

Bin 327: 106 of cap free
Amount of items: 3
Items: 
Size: 739527 Color: 4
Size: 139948 Color: 19
Size: 120420 Color: 18

Bin 328: 106 of cap free
Amount of items: 2
Items: 
Size: 562126 Color: 11
Size: 437769 Color: 12

Bin 329: 107 of cap free
Amount of items: 2
Items: 
Size: 719179 Color: 17
Size: 280715 Color: 0

Bin 330: 107 of cap free
Amount of items: 2
Items: 
Size: 551658 Color: 15
Size: 448236 Color: 16

Bin 331: 107 of cap free
Amount of items: 2
Items: 
Size: 734878 Color: 4
Size: 265016 Color: 17

Bin 332: 108 of cap free
Amount of items: 2
Items: 
Size: 517465 Color: 12
Size: 482428 Color: 14

Bin 333: 110 of cap free
Amount of items: 2
Items: 
Size: 573330 Color: 18
Size: 426561 Color: 11

Bin 334: 110 of cap free
Amount of items: 2
Items: 
Size: 761969 Color: 15
Size: 237922 Color: 2

Bin 335: 111 of cap free
Amount of items: 2
Items: 
Size: 654946 Color: 8
Size: 344944 Color: 15

Bin 336: 113 of cap free
Amount of items: 2
Items: 
Size: 677590 Color: 8
Size: 322298 Color: 6

Bin 337: 113 of cap free
Amount of items: 2
Items: 
Size: 621673 Color: 19
Size: 378215 Color: 5

Bin 338: 113 of cap free
Amount of items: 3
Items: 
Size: 762483 Color: 5
Size: 120740 Color: 9
Size: 116665 Color: 15

Bin 339: 114 of cap free
Amount of items: 2
Items: 
Size: 556451 Color: 15
Size: 443436 Color: 9

Bin 340: 117 of cap free
Amount of items: 2
Items: 
Size: 786433 Color: 2
Size: 213451 Color: 12

Bin 341: 117 of cap free
Amount of items: 2
Items: 
Size: 590659 Color: 0
Size: 409225 Color: 16

Bin 342: 117 of cap free
Amount of items: 2
Items: 
Size: 754419 Color: 2
Size: 245465 Color: 19

Bin 343: 118 of cap free
Amount of items: 2
Items: 
Size: 510687 Color: 2
Size: 489196 Color: 5

Bin 344: 118 of cap free
Amount of items: 2
Items: 
Size: 797758 Color: 12
Size: 202125 Color: 7

Bin 345: 120 of cap free
Amount of items: 2
Items: 
Size: 697458 Color: 17
Size: 302423 Color: 14

Bin 346: 122 of cap free
Amount of items: 2
Items: 
Size: 526476 Color: 13
Size: 473403 Color: 18

Bin 347: 122 of cap free
Amount of items: 2
Items: 
Size: 605383 Color: 2
Size: 394496 Color: 8

Bin 348: 122 of cap free
Amount of items: 2
Items: 
Size: 692866 Color: 6
Size: 307013 Color: 14

Bin 349: 123 of cap free
Amount of items: 2
Items: 
Size: 760716 Color: 7
Size: 239162 Color: 4

Bin 350: 123 of cap free
Amount of items: 2
Items: 
Size: 582552 Color: 14
Size: 417326 Color: 19

Bin 351: 123 of cap free
Amount of items: 2
Items: 
Size: 618977 Color: 0
Size: 380901 Color: 14

Bin 352: 124 of cap free
Amount of items: 2
Items: 
Size: 629690 Color: 11
Size: 370187 Color: 15

Bin 353: 126 of cap free
Amount of items: 2
Items: 
Size: 566596 Color: 17
Size: 433279 Color: 0

Bin 354: 128 of cap free
Amount of items: 3
Items: 
Size: 603087 Color: 10
Size: 213302 Color: 1
Size: 183484 Color: 12

Bin 355: 129 of cap free
Amount of items: 2
Items: 
Size: 546889 Color: 17
Size: 452983 Color: 7

Bin 356: 131 of cap free
Amount of items: 2
Items: 
Size: 566191 Color: 4
Size: 433679 Color: 13

Bin 357: 131 of cap free
Amount of items: 2
Items: 
Size: 742314 Color: 8
Size: 257556 Color: 9

Bin 358: 132 of cap free
Amount of items: 2
Items: 
Size: 785127 Color: 3
Size: 214742 Color: 10

Bin 359: 132 of cap free
Amount of items: 2
Items: 
Size: 788308 Color: 6
Size: 211561 Color: 12

Bin 360: 136 of cap free
Amount of items: 2
Items: 
Size: 509292 Color: 1
Size: 490573 Color: 17

Bin 361: 136 of cap free
Amount of items: 2
Items: 
Size: 506131 Color: 14
Size: 493734 Color: 19

Bin 362: 137 of cap free
Amount of items: 2
Items: 
Size: 791275 Color: 14
Size: 208589 Color: 2

Bin 363: 138 of cap free
Amount of items: 2
Items: 
Size: 720576 Color: 9
Size: 279287 Color: 8

Bin 364: 138 of cap free
Amount of items: 2
Items: 
Size: 661147 Color: 4
Size: 338716 Color: 3

Bin 365: 139 of cap free
Amount of items: 2
Items: 
Size: 554186 Color: 5
Size: 445676 Color: 7

Bin 366: 140 of cap free
Amount of items: 2
Items: 
Size: 597952 Color: 3
Size: 401909 Color: 18

Bin 367: 142 of cap free
Amount of items: 2
Items: 
Size: 787042 Color: 9
Size: 212817 Color: 17

Bin 368: 143 of cap free
Amount of items: 2
Items: 
Size: 799408 Color: 1
Size: 200450 Color: 9

Bin 369: 144 of cap free
Amount of items: 2
Items: 
Size: 681510 Color: 11
Size: 318347 Color: 2

Bin 370: 144 of cap free
Amount of items: 2
Items: 
Size: 760151 Color: 6
Size: 239706 Color: 19

Bin 371: 146 of cap free
Amount of items: 2
Items: 
Size: 682183 Color: 8
Size: 317672 Color: 9

Bin 372: 146 of cap free
Amount of items: 2
Items: 
Size: 772119 Color: 10
Size: 227736 Color: 15

Bin 373: 149 of cap free
Amount of items: 2
Items: 
Size: 761937 Color: 19
Size: 237915 Color: 4

Bin 374: 150 of cap free
Amount of items: 2
Items: 
Size: 529187 Color: 0
Size: 470664 Color: 7

Bin 375: 150 of cap free
Amount of items: 2
Items: 
Size: 609163 Color: 7
Size: 390688 Color: 15

Bin 376: 151 of cap free
Amount of items: 2
Items: 
Size: 715035 Color: 7
Size: 284815 Color: 5

Bin 377: 151 of cap free
Amount of items: 2
Items: 
Size: 683933 Color: 9
Size: 315917 Color: 8

Bin 378: 152 of cap free
Amount of items: 2
Items: 
Size: 620466 Color: 1
Size: 379383 Color: 15

Bin 379: 152 of cap free
Amount of items: 2
Items: 
Size: 548794 Color: 6
Size: 451055 Color: 18

Bin 380: 152 of cap free
Amount of items: 2
Items: 
Size: 751019 Color: 7
Size: 248830 Color: 15

Bin 381: 153 of cap free
Amount of items: 2
Items: 
Size: 786596 Color: 6
Size: 213252 Color: 7

Bin 382: 153 of cap free
Amount of items: 2
Items: 
Size: 626123 Color: 6
Size: 373725 Color: 16

Bin 383: 154 of cap free
Amount of items: 2
Items: 
Size: 653819 Color: 9
Size: 346028 Color: 11

Bin 384: 154 of cap free
Amount of items: 2
Items: 
Size: 787283 Color: 9
Size: 212564 Color: 8

Bin 385: 155 of cap free
Amount of items: 7
Items: 
Size: 198443 Color: 11
Size: 171003 Color: 9
Size: 142179 Color: 0
Size: 136244 Color: 0
Size: 129077 Color: 4
Size: 118991 Color: 18
Size: 103909 Color: 7

Bin 386: 157 of cap free
Amount of items: 2
Items: 
Size: 618609 Color: 14
Size: 381235 Color: 19

Bin 387: 158 of cap free
Amount of items: 2
Items: 
Size: 781429 Color: 9
Size: 218414 Color: 15

Bin 388: 159 of cap free
Amount of items: 2
Items: 
Size: 501982 Color: 3
Size: 497860 Color: 6

Bin 389: 159 of cap free
Amount of items: 2
Items: 
Size: 524147 Color: 2
Size: 475695 Color: 8

Bin 390: 160 of cap free
Amount of items: 2
Items: 
Size: 533488 Color: 1
Size: 466353 Color: 8

Bin 391: 160 of cap free
Amount of items: 2
Items: 
Size: 701501 Color: 5
Size: 298340 Color: 3

Bin 392: 160 of cap free
Amount of items: 2
Items: 
Size: 655056 Color: 5
Size: 344785 Color: 16

Bin 393: 161 of cap free
Amount of items: 2
Items: 
Size: 703553 Color: 15
Size: 296287 Color: 5

Bin 394: 164 of cap free
Amount of items: 2
Items: 
Size: 733432 Color: 3
Size: 266405 Color: 7

Bin 395: 164 of cap free
Amount of items: 2
Items: 
Size: 729571 Color: 16
Size: 270266 Color: 12

Bin 396: 167 of cap free
Amount of items: 2
Items: 
Size: 576058 Color: 0
Size: 423776 Color: 18

Bin 397: 170 of cap free
Amount of items: 2
Items: 
Size: 707808 Color: 19
Size: 292023 Color: 15

Bin 398: 170 of cap free
Amount of items: 2
Items: 
Size: 531058 Color: 1
Size: 468773 Color: 7

Bin 399: 172 of cap free
Amount of items: 3
Items: 
Size: 341490 Color: 9
Size: 335711 Color: 1
Size: 322628 Color: 6

Bin 400: 174 of cap free
Amount of items: 2
Items: 
Size: 596874 Color: 18
Size: 402953 Color: 1

Bin 401: 174 of cap free
Amount of items: 3
Items: 
Size: 404415 Color: 7
Size: 306295 Color: 3
Size: 289117 Color: 6

Bin 402: 176 of cap free
Amount of items: 2
Items: 
Size: 540940 Color: 4
Size: 458885 Color: 8

Bin 403: 180 of cap free
Amount of items: 2
Items: 
Size: 555395 Color: 13
Size: 444426 Color: 3

Bin 404: 180 of cap free
Amount of items: 2
Items: 
Size: 557521 Color: 9
Size: 442300 Color: 16

Bin 405: 183 of cap free
Amount of items: 2
Items: 
Size: 792783 Color: 0
Size: 207035 Color: 19

Bin 406: 184 of cap free
Amount of items: 2
Items: 
Size: 601871 Color: 17
Size: 397946 Color: 19

Bin 407: 191 of cap free
Amount of items: 2
Items: 
Size: 799696 Color: 18
Size: 200114 Color: 17

Bin 408: 192 of cap free
Amount of items: 2
Items: 
Size: 626443 Color: 19
Size: 373366 Color: 9

Bin 409: 193 of cap free
Amount of items: 2
Items: 
Size: 743775 Color: 17
Size: 256033 Color: 3

Bin 410: 194 of cap free
Amount of items: 2
Items: 
Size: 581521 Color: 8
Size: 418286 Color: 7

Bin 411: 194 of cap free
Amount of items: 2
Items: 
Size: 617586 Color: 18
Size: 382221 Color: 6

Bin 412: 196 of cap free
Amount of items: 2
Items: 
Size: 679355 Color: 9
Size: 320450 Color: 13

Bin 413: 197 of cap free
Amount of items: 2
Items: 
Size: 533843 Color: 0
Size: 465961 Color: 9

Bin 414: 197 of cap free
Amount of items: 2
Items: 
Size: 568495 Color: 0
Size: 431309 Color: 8

Bin 415: 198 of cap free
Amount of items: 2
Items: 
Size: 549380 Color: 8
Size: 450423 Color: 4

Bin 416: 200 of cap free
Amount of items: 2
Items: 
Size: 604025 Color: 15
Size: 395776 Color: 13

Bin 417: 201 of cap free
Amount of items: 2
Items: 
Size: 705666 Color: 0
Size: 294134 Color: 17

Bin 418: 201 of cap free
Amount of items: 3
Items: 
Size: 597828 Color: 4
Size: 217982 Color: 12
Size: 183990 Color: 1

Bin 419: 202 of cap free
Amount of items: 2
Items: 
Size: 723942 Color: 3
Size: 275857 Color: 5

Bin 420: 203 of cap free
Amount of items: 2
Items: 
Size: 671895 Color: 19
Size: 327903 Color: 3

Bin 421: 205 of cap free
Amount of items: 2
Items: 
Size: 526204 Color: 11
Size: 473592 Color: 17

Bin 422: 206 of cap free
Amount of items: 2
Items: 
Size: 794932 Color: 7
Size: 204863 Color: 13

Bin 423: 207 of cap free
Amount of items: 2
Items: 
Size: 606828 Color: 3
Size: 392966 Color: 8

Bin 424: 208 of cap free
Amount of items: 2
Items: 
Size: 607859 Color: 4
Size: 391934 Color: 8

Bin 425: 208 of cap free
Amount of items: 2
Items: 
Size: 792843 Color: 19
Size: 206950 Color: 15

Bin 426: 209 of cap free
Amount of items: 2
Items: 
Size: 695040 Color: 14
Size: 304752 Color: 8

Bin 427: 209 of cap free
Amount of items: 2
Items: 
Size: 593796 Color: 17
Size: 405996 Color: 0

Bin 428: 212 of cap free
Amount of items: 2
Items: 
Size: 660073 Color: 11
Size: 339716 Color: 12

Bin 429: 213 of cap free
Amount of items: 2
Items: 
Size: 690943 Color: 15
Size: 308845 Color: 2

Bin 430: 214 of cap free
Amount of items: 2
Items: 
Size: 536261 Color: 8
Size: 463526 Color: 18

Bin 431: 216 of cap free
Amount of items: 2
Items: 
Size: 662398 Color: 1
Size: 337387 Color: 11

Bin 432: 217 of cap free
Amount of items: 2
Items: 
Size: 583314 Color: 15
Size: 416470 Color: 9

Bin 433: 220 of cap free
Amount of items: 2
Items: 
Size: 772825 Color: 9
Size: 226956 Color: 8

Bin 434: 221 of cap free
Amount of items: 3
Items: 
Size: 623189 Color: 9
Size: 218661 Color: 3
Size: 157930 Color: 1

Bin 435: 221 of cap free
Amount of items: 2
Items: 
Size: 628060 Color: 17
Size: 371720 Color: 0

Bin 436: 221 of cap free
Amount of items: 2
Items: 
Size: 526899 Color: 17
Size: 472881 Color: 15

Bin 437: 221 of cap free
Amount of items: 2
Items: 
Size: 774835 Color: 2
Size: 224945 Color: 18

Bin 438: 225 of cap free
Amount of items: 3
Items: 
Size: 418922 Color: 14
Size: 290433 Color: 7
Size: 290421 Color: 16

Bin 439: 225 of cap free
Amount of items: 2
Items: 
Size: 713804 Color: 2
Size: 285972 Color: 11

Bin 440: 225 of cap free
Amount of items: 2
Items: 
Size: 546811 Color: 5
Size: 452965 Color: 0

Bin 441: 227 of cap free
Amount of items: 2
Items: 
Size: 680083 Color: 19
Size: 319691 Color: 8

Bin 442: 229 of cap free
Amount of items: 2
Items: 
Size: 599387 Color: 12
Size: 400385 Color: 19

Bin 443: 229 of cap free
Amount of items: 3
Items: 
Size: 357191 Color: 18
Size: 335728 Color: 10
Size: 306853 Color: 3

Bin 444: 232 of cap free
Amount of items: 3
Items: 
Size: 768568 Color: 17
Size: 118571 Color: 7
Size: 112630 Color: 14

Bin 445: 233 of cap free
Amount of items: 3
Items: 
Size: 650040 Color: 17
Size: 179926 Color: 16
Size: 169802 Color: 0

Bin 446: 233 of cap free
Amount of items: 2
Items: 
Size: 636451 Color: 12
Size: 363317 Color: 11

Bin 447: 235 of cap free
Amount of items: 2
Items: 
Size: 718506 Color: 5
Size: 281260 Color: 15

Bin 448: 235 of cap free
Amount of items: 2
Items: 
Size: 622099 Color: 9
Size: 377667 Color: 0

Bin 449: 236 of cap free
Amount of items: 2
Items: 
Size: 724773 Color: 16
Size: 274992 Color: 11

Bin 450: 237 of cap free
Amount of items: 2
Items: 
Size: 744916 Color: 4
Size: 254848 Color: 9

Bin 451: 238 of cap free
Amount of items: 3
Items: 
Size: 773679 Color: 7
Size: 114546 Color: 18
Size: 111538 Color: 18

Bin 452: 239 of cap free
Amount of items: 2
Items: 
Size: 591133 Color: 9
Size: 408629 Color: 7

Bin 453: 241 of cap free
Amount of items: 2
Items: 
Size: 532911 Color: 10
Size: 466849 Color: 17

Bin 454: 242 of cap free
Amount of items: 2
Items: 
Size: 765686 Color: 2
Size: 234073 Color: 16

Bin 455: 243 of cap free
Amount of items: 2
Items: 
Size: 614107 Color: 5
Size: 385651 Color: 10

Bin 456: 245 of cap free
Amount of items: 2
Items: 
Size: 758139 Color: 19
Size: 241617 Color: 7

Bin 457: 246 of cap free
Amount of items: 2
Items: 
Size: 633482 Color: 8
Size: 366273 Color: 16

Bin 458: 247 of cap free
Amount of items: 2
Items: 
Size: 569901 Color: 1
Size: 429853 Color: 18

Bin 459: 251 of cap free
Amount of items: 2
Items: 
Size: 659816 Color: 19
Size: 339934 Color: 2

Bin 460: 251 of cap free
Amount of items: 2
Items: 
Size: 585071 Color: 3
Size: 414679 Color: 15

Bin 461: 254 of cap free
Amount of items: 2
Items: 
Size: 656360 Color: 4
Size: 343387 Color: 5

Bin 462: 255 of cap free
Amount of items: 2
Items: 
Size: 733557 Color: 7
Size: 266189 Color: 15

Bin 463: 256 of cap free
Amount of items: 3
Items: 
Size: 676010 Color: 18
Size: 166546 Color: 8
Size: 157189 Color: 7

Bin 464: 257 of cap free
Amount of items: 3
Items: 
Size: 492352 Color: 14
Size: 326211 Color: 6
Size: 181181 Color: 0

Bin 465: 257 of cap free
Amount of items: 2
Items: 
Size: 632570 Color: 13
Size: 367174 Color: 11

Bin 466: 261 of cap free
Amount of items: 2
Items: 
Size: 609644 Color: 10
Size: 390096 Color: 0

Bin 467: 262 of cap free
Amount of items: 2
Items: 
Size: 672842 Color: 19
Size: 326897 Color: 12

Bin 468: 264 of cap free
Amount of items: 2
Items: 
Size: 610231 Color: 6
Size: 389506 Color: 8

Bin 469: 264 of cap free
Amount of items: 2
Items: 
Size: 754970 Color: 14
Size: 244767 Color: 3

Bin 470: 265 of cap free
Amount of items: 2
Items: 
Size: 762181 Color: 2
Size: 237555 Color: 12

Bin 471: 266 of cap free
Amount of items: 2
Items: 
Size: 655546 Color: 1
Size: 344189 Color: 12

Bin 472: 270 of cap free
Amount of items: 2
Items: 
Size: 734225 Color: 9
Size: 265506 Color: 13

Bin 473: 270 of cap free
Amount of items: 2
Items: 
Size: 607906 Color: 17
Size: 391825 Color: 3

Bin 474: 271 of cap free
Amount of items: 2
Items: 
Size: 521579 Color: 18
Size: 478151 Color: 19

Bin 475: 277 of cap free
Amount of items: 2
Items: 
Size: 630717 Color: 14
Size: 369007 Color: 1

Bin 476: 278 of cap free
Amount of items: 2
Items: 
Size: 561573 Color: 0
Size: 438150 Color: 5

Bin 477: 280 of cap free
Amount of items: 2
Items: 
Size: 572239 Color: 15
Size: 427482 Color: 6

Bin 478: 280 of cap free
Amount of items: 2
Items: 
Size: 634719 Color: 4
Size: 365002 Color: 19

Bin 479: 281 of cap free
Amount of items: 2
Items: 
Size: 775582 Color: 7
Size: 224138 Color: 1

Bin 480: 281 of cap free
Amount of items: 2
Items: 
Size: 556257 Color: 19
Size: 443463 Color: 15

Bin 481: 283 of cap free
Amount of items: 2
Items: 
Size: 626030 Color: 8
Size: 373688 Color: 15

Bin 482: 284 of cap free
Amount of items: 2
Items: 
Size: 697617 Color: 6
Size: 302100 Color: 18

Bin 483: 284 of cap free
Amount of items: 3
Items: 
Size: 553715 Color: 1
Size: 276089 Color: 16
Size: 169913 Color: 4

Bin 484: 289 of cap free
Amount of items: 2
Items: 
Size: 513466 Color: 6
Size: 486246 Color: 19

Bin 485: 290 of cap free
Amount of items: 2
Items: 
Size: 592092 Color: 8
Size: 407619 Color: 0

Bin 486: 290 of cap free
Amount of items: 2
Items: 
Size: 771797 Color: 5
Size: 227914 Color: 16

Bin 487: 290 of cap free
Amount of items: 2
Items: 
Size: 506701 Color: 11
Size: 493010 Color: 13

Bin 488: 291 of cap free
Amount of items: 2
Items: 
Size: 762129 Color: 10
Size: 237581 Color: 2

Bin 489: 291 of cap free
Amount of items: 2
Items: 
Size: 766837 Color: 4
Size: 232873 Color: 18

Bin 490: 292 of cap free
Amount of items: 2
Items: 
Size: 771294 Color: 8
Size: 228415 Color: 6

Bin 491: 295 of cap free
Amount of items: 2
Items: 
Size: 601932 Color: 15
Size: 397774 Color: 9

Bin 492: 296 of cap free
Amount of items: 2
Items: 
Size: 608499 Color: 1
Size: 391206 Color: 8

Bin 493: 301 of cap free
Amount of items: 3
Items: 
Size: 583936 Color: 8
Size: 291282 Color: 12
Size: 124482 Color: 12

Bin 494: 304 of cap free
Amount of items: 2
Items: 
Size: 537958 Color: 6
Size: 461739 Color: 16

Bin 495: 304 of cap free
Amount of items: 2
Items: 
Size: 768913 Color: 0
Size: 230784 Color: 10

Bin 496: 305 of cap free
Amount of items: 2
Items: 
Size: 575980 Color: 1
Size: 423716 Color: 3

Bin 497: 306 of cap free
Amount of items: 2
Items: 
Size: 515439 Color: 7
Size: 484256 Color: 3

Bin 498: 307 of cap free
Amount of items: 3
Items: 
Size: 354413 Color: 19
Size: 335065 Color: 13
Size: 310216 Color: 12

Bin 499: 307 of cap free
Amount of items: 2
Items: 
Size: 523406 Color: 13
Size: 476288 Color: 5

Bin 500: 308 of cap free
Amount of items: 2
Items: 
Size: 703537 Color: 10
Size: 296156 Color: 14

Bin 501: 309 of cap free
Amount of items: 2
Items: 
Size: 528286 Color: 13
Size: 471406 Color: 4

Bin 502: 315 of cap free
Amount of items: 2
Items: 
Size: 730589 Color: 5
Size: 269097 Color: 6

Bin 503: 319 of cap free
Amount of items: 2
Items: 
Size: 539942 Color: 3
Size: 459740 Color: 10

Bin 504: 319 of cap free
Amount of items: 2
Items: 
Size: 665276 Color: 11
Size: 334406 Color: 6

Bin 505: 320 of cap free
Amount of items: 2
Items: 
Size: 671191 Color: 12
Size: 328490 Color: 6

Bin 506: 320 of cap free
Amount of items: 2
Items: 
Size: 776561 Color: 3
Size: 223120 Color: 4

Bin 507: 323 of cap free
Amount of items: 2
Items: 
Size: 563040 Color: 14
Size: 436638 Color: 6

Bin 508: 324 of cap free
Amount of items: 2
Items: 
Size: 632907 Color: 8
Size: 366770 Color: 16

Bin 509: 326 of cap free
Amount of items: 2
Items: 
Size: 532060 Color: 14
Size: 467615 Color: 9

Bin 510: 328 of cap free
Amount of items: 2
Items: 
Size: 637427 Color: 3
Size: 362246 Color: 5

Bin 511: 338 of cap free
Amount of items: 2
Items: 
Size: 618189 Color: 5
Size: 381474 Color: 4

Bin 512: 339 of cap free
Amount of items: 2
Items: 
Size: 750105 Color: 0
Size: 249557 Color: 17

Bin 513: 339 of cap free
Amount of items: 2
Items: 
Size: 784932 Color: 11
Size: 214730 Color: 0

Bin 514: 341 of cap free
Amount of items: 2
Items: 
Size: 641923 Color: 2
Size: 357737 Color: 9

Bin 515: 348 of cap free
Amount of items: 2
Items: 
Size: 563131 Color: 6
Size: 436522 Color: 19

Bin 516: 356 of cap free
Amount of items: 2
Items: 
Size: 595644 Color: 13
Size: 404001 Color: 16

Bin 517: 358 of cap free
Amount of items: 2
Items: 
Size: 687295 Color: 8
Size: 312348 Color: 2

Bin 518: 358 of cap free
Amount of items: 2
Items: 
Size: 504072 Color: 15
Size: 495571 Color: 16

Bin 519: 360 of cap free
Amount of items: 2
Items: 
Size: 780925 Color: 16
Size: 218716 Color: 18

Bin 520: 361 of cap free
Amount of items: 2
Items: 
Size: 555252 Color: 1
Size: 444388 Color: 0

Bin 521: 361 of cap free
Amount of items: 2
Items: 
Size: 560947 Color: 7
Size: 438693 Color: 17

Bin 522: 361 of cap free
Amount of items: 2
Items: 
Size: 625610 Color: 16
Size: 374030 Color: 2

Bin 523: 363 of cap free
Amount of items: 2
Items: 
Size: 576305 Color: 16
Size: 423333 Color: 14

Bin 524: 365 of cap free
Amount of items: 2
Items: 
Size: 586807 Color: 10
Size: 412829 Color: 11

Bin 525: 367 of cap free
Amount of items: 2
Items: 
Size: 636665 Color: 17
Size: 362969 Color: 1

Bin 526: 367 of cap free
Amount of items: 2
Items: 
Size: 500851 Color: 17
Size: 498783 Color: 5

Bin 527: 370 of cap free
Amount of items: 2
Items: 
Size: 601900 Color: 4
Size: 397731 Color: 16

Bin 528: 370 of cap free
Amount of items: 2
Items: 
Size: 774698 Color: 3
Size: 224933 Color: 1

Bin 529: 373 of cap free
Amount of items: 2
Items: 
Size: 557791 Color: 1
Size: 441837 Color: 4

Bin 530: 374 of cap free
Amount of items: 3
Items: 
Size: 620649 Color: 5
Size: 191535 Color: 17
Size: 187443 Color: 11

Bin 531: 376 of cap free
Amount of items: 2
Items: 
Size: 797648 Color: 1
Size: 201977 Color: 15

Bin 532: 380 of cap free
Amount of items: 2
Items: 
Size: 713772 Color: 1
Size: 285849 Color: 2

Bin 533: 381 of cap free
Amount of items: 2
Items: 
Size: 600710 Color: 19
Size: 398910 Color: 5

Bin 534: 382 of cap free
Amount of items: 2
Items: 
Size: 700238 Color: 4
Size: 299381 Color: 3

Bin 535: 386 of cap free
Amount of items: 2
Items: 
Size: 547817 Color: 4
Size: 451798 Color: 3

Bin 536: 388 of cap free
Amount of items: 3
Items: 
Size: 711624 Color: 12
Size: 172874 Color: 6
Size: 115115 Color: 8

Bin 537: 389 of cap free
Amount of items: 2
Items: 
Size: 762747 Color: 13
Size: 236865 Color: 11

Bin 538: 392 of cap free
Amount of items: 2
Items: 
Size: 548302 Color: 15
Size: 451307 Color: 8

Bin 539: 394 of cap free
Amount of items: 2
Items: 
Size: 778477 Color: 19
Size: 221130 Color: 9

Bin 540: 395 of cap free
Amount of items: 2
Items: 
Size: 741120 Color: 5
Size: 258486 Color: 12

Bin 541: 396 of cap free
Amount of items: 2
Items: 
Size: 649555 Color: 7
Size: 350050 Color: 18

Bin 542: 397 of cap free
Amount of items: 3
Items: 
Size: 454015 Color: 3
Size: 383533 Color: 5
Size: 162056 Color: 1

Bin 543: 397 of cap free
Amount of items: 2
Items: 
Size: 646803 Color: 3
Size: 352801 Color: 19

Bin 544: 399 of cap free
Amount of items: 2
Items: 
Size: 646146 Color: 3
Size: 353456 Color: 13

Bin 545: 399 of cap free
Amount of items: 2
Items: 
Size: 687696 Color: 4
Size: 311906 Color: 2

Bin 546: 401 of cap free
Amount of items: 2
Items: 
Size: 759312 Color: 15
Size: 240288 Color: 3

Bin 547: 406 of cap free
Amount of items: 2
Items: 
Size: 678671 Color: 4
Size: 320924 Color: 8

Bin 548: 407 of cap free
Amount of items: 2
Items: 
Size: 512775 Color: 16
Size: 486819 Color: 11

Bin 549: 415 of cap free
Amount of items: 2
Items: 
Size: 597401 Color: 5
Size: 402185 Color: 18

Bin 550: 415 of cap free
Amount of items: 2
Items: 
Size: 563566 Color: 6
Size: 436020 Color: 11

Bin 551: 417 of cap free
Amount of items: 2
Items: 
Size: 618544 Color: 0
Size: 381040 Color: 9

Bin 552: 421 of cap free
Amount of items: 2
Items: 
Size: 670344 Color: 14
Size: 329236 Color: 4

Bin 553: 422 of cap free
Amount of items: 2
Items: 
Size: 568110 Color: 2
Size: 431469 Color: 5

Bin 554: 425 of cap free
Amount of items: 2
Items: 
Size: 643926 Color: 15
Size: 355650 Color: 14

Bin 555: 428 of cap free
Amount of items: 2
Items: 
Size: 727968 Color: 9
Size: 271605 Color: 0

Bin 556: 428 of cap free
Amount of items: 2
Items: 
Size: 755436 Color: 10
Size: 244137 Color: 4

Bin 557: 435 of cap free
Amount of items: 2
Items: 
Size: 660707 Color: 0
Size: 338859 Color: 15

Bin 558: 437 of cap free
Amount of items: 2
Items: 
Size: 524777 Color: 8
Size: 474787 Color: 19

Bin 559: 437 of cap free
Amount of items: 2
Items: 
Size: 739953 Color: 7
Size: 259611 Color: 15

Bin 560: 443 of cap free
Amount of items: 2
Items: 
Size: 576973 Color: 0
Size: 422585 Color: 17

Bin 561: 443 of cap free
Amount of items: 2
Items: 
Size: 716237 Color: 17
Size: 283321 Color: 4

Bin 562: 445 of cap free
Amount of items: 2
Items: 
Size: 508307 Color: 10
Size: 491249 Color: 5

Bin 563: 457 of cap free
Amount of items: 2
Items: 
Size: 698908 Color: 3
Size: 300636 Color: 1

Bin 564: 460 of cap free
Amount of items: 3
Items: 
Size: 756956 Color: 2
Size: 128746 Color: 5
Size: 113839 Color: 13

Bin 565: 461 of cap free
Amount of items: 2
Items: 
Size: 634009 Color: 1
Size: 365531 Color: 2

Bin 566: 462 of cap free
Amount of items: 2
Items: 
Size: 704819 Color: 9
Size: 294720 Color: 7

Bin 567: 462 of cap free
Amount of items: 2
Items: 
Size: 631099 Color: 13
Size: 368440 Color: 7

Bin 568: 463 of cap free
Amount of items: 2
Items: 
Size: 620335 Color: 0
Size: 379203 Color: 7

Bin 569: 469 of cap free
Amount of items: 2
Items: 
Size: 563015 Color: 1
Size: 436517 Color: 18

Bin 570: 474 of cap free
Amount of items: 2
Items: 
Size: 680630 Color: 15
Size: 318897 Color: 16

Bin 571: 477 of cap free
Amount of items: 2
Items: 
Size: 681451 Color: 12
Size: 318073 Color: 11

Bin 572: 479 of cap free
Amount of items: 2
Items: 
Size: 570565 Color: 17
Size: 428957 Color: 1

Bin 573: 480 of cap free
Amount of items: 2
Items: 
Size: 569233 Color: 17
Size: 430288 Color: 7

Bin 574: 480 of cap free
Amount of items: 2
Items: 
Size: 518978 Color: 7
Size: 480543 Color: 17

Bin 575: 483 of cap free
Amount of items: 2
Items: 
Size: 712464 Color: 19
Size: 287054 Color: 11

Bin 576: 486 of cap free
Amount of items: 3
Items: 
Size: 679940 Color: 15
Size: 169478 Color: 18
Size: 150097 Color: 18

Bin 577: 486 of cap free
Amount of items: 2
Items: 
Size: 784862 Color: 3
Size: 214653 Color: 17

Bin 578: 487 of cap free
Amount of items: 2
Items: 
Size: 738143 Color: 9
Size: 261371 Color: 1

Bin 579: 494 of cap free
Amount of items: 2
Items: 
Size: 747976 Color: 7
Size: 251531 Color: 13

Bin 580: 495 of cap free
Amount of items: 2
Items: 
Size: 640133 Color: 15
Size: 359373 Color: 8

Bin 581: 496 of cap free
Amount of items: 2
Items: 
Size: 569879 Color: 16
Size: 429626 Color: 9

Bin 582: 498 of cap free
Amount of items: 2
Items: 
Size: 679158 Color: 9
Size: 320345 Color: 18

Bin 583: 499 of cap free
Amount of items: 2
Items: 
Size: 749332 Color: 12
Size: 250170 Color: 1

Bin 584: 500 of cap free
Amount of items: 2
Items: 
Size: 747088 Color: 16
Size: 252413 Color: 6

Bin 585: 506 of cap free
Amount of items: 2
Items: 
Size: 610694 Color: 16
Size: 388801 Color: 14

Bin 586: 506 of cap free
Amount of items: 2
Items: 
Size: 510320 Color: 5
Size: 489175 Color: 7

Bin 587: 508 of cap free
Amount of items: 2
Items: 
Size: 532647 Color: 17
Size: 466846 Color: 19

Bin 588: 508 of cap free
Amount of items: 2
Items: 
Size: 764451 Color: 12
Size: 235042 Color: 18

Bin 589: 510 of cap free
Amount of items: 2
Items: 
Size: 668722 Color: 9
Size: 330769 Color: 16

Bin 590: 510 of cap free
Amount of items: 2
Items: 
Size: 786276 Color: 18
Size: 213215 Color: 0

Bin 591: 514 of cap free
Amount of items: 2
Items: 
Size: 547248 Color: 8
Size: 452239 Color: 14

Bin 592: 518 of cap free
Amount of items: 2
Items: 
Size: 648006 Color: 3
Size: 351477 Color: 14

Bin 593: 522 of cap free
Amount of items: 2
Items: 
Size: 511660 Color: 11
Size: 487819 Color: 12

Bin 594: 522 of cap free
Amount of items: 2
Items: 
Size: 798983 Color: 2
Size: 200496 Color: 0

Bin 595: 535 of cap free
Amount of items: 2
Items: 
Size: 704229 Color: 3
Size: 295237 Color: 10

Bin 596: 544 of cap free
Amount of items: 2
Items: 
Size: 583685 Color: 6
Size: 415772 Color: 14

Bin 597: 545 of cap free
Amount of items: 2
Items: 
Size: 726247 Color: 12
Size: 273209 Color: 5

Bin 598: 548 of cap free
Amount of items: 2
Items: 
Size: 739870 Color: 8
Size: 259583 Color: 12

Bin 599: 548 of cap free
Amount of items: 2
Items: 
Size: 767268 Color: 13
Size: 232185 Color: 3

Bin 600: 555 of cap free
Amount of items: 2
Items: 
Size: 706961 Color: 6
Size: 292485 Color: 2

Bin 601: 557 of cap free
Amount of items: 2
Items: 
Size: 539846 Color: 11
Size: 459598 Color: 3

Bin 602: 563 of cap free
Amount of items: 2
Items: 
Size: 770575 Color: 19
Size: 228863 Color: 15

Bin 603: 566 of cap free
Amount of items: 2
Items: 
Size: 574326 Color: 14
Size: 425109 Color: 5

Bin 604: 568 of cap free
Amount of items: 2
Items: 
Size: 594069 Color: 17
Size: 405364 Color: 15

Bin 605: 568 of cap free
Amount of items: 2
Items: 
Size: 614733 Color: 12
Size: 384700 Color: 1

Bin 606: 568 of cap free
Amount of items: 2
Items: 
Size: 715442 Color: 5
Size: 283991 Color: 7

Bin 607: 578 of cap free
Amount of items: 3
Items: 
Size: 621576 Color: 5
Size: 218223 Color: 17
Size: 159624 Color: 6

Bin 608: 579 of cap free
Amount of items: 2
Items: 
Size: 541359 Color: 8
Size: 458063 Color: 15

Bin 609: 580 of cap free
Amount of items: 2
Items: 
Size: 731813 Color: 14
Size: 267608 Color: 15

Bin 610: 584 of cap free
Amount of items: 2
Items: 
Size: 717297 Color: 10
Size: 282120 Color: 13

Bin 611: 584 of cap free
Amount of items: 2
Items: 
Size: 508268 Color: 19
Size: 491149 Color: 4

Bin 612: 589 of cap free
Amount of items: 2
Items: 
Size: 785646 Color: 7
Size: 213766 Color: 10

Bin 613: 591 of cap free
Amount of items: 2
Items: 
Size: 675908 Color: 17
Size: 323502 Color: 13

Bin 614: 592 of cap free
Amount of items: 2
Items: 
Size: 633759 Color: 12
Size: 365650 Color: 1

Bin 615: 599 of cap free
Amount of items: 2
Items: 
Size: 580747 Color: 0
Size: 418655 Color: 15

Bin 616: 601 of cap free
Amount of items: 2
Items: 
Size: 690191 Color: 17
Size: 309209 Color: 19

Bin 617: 603 of cap free
Amount of items: 2
Items: 
Size: 744259 Color: 9
Size: 255139 Color: 14

Bin 618: 606 of cap free
Amount of items: 2
Items: 
Size: 500147 Color: 19
Size: 499248 Color: 12

Bin 619: 607 of cap free
Amount of items: 2
Items: 
Size: 582779 Color: 17
Size: 416615 Color: 15

Bin 620: 609 of cap free
Amount of items: 2
Items: 
Size: 755984 Color: 9
Size: 243408 Color: 6

Bin 621: 614 of cap free
Amount of items: 2
Items: 
Size: 713542 Color: 7
Size: 285845 Color: 5

Bin 622: 615 of cap free
Amount of items: 2
Items: 
Size: 516443 Color: 17
Size: 482943 Color: 19

Bin 623: 619 of cap free
Amount of items: 2
Items: 
Size: 715368 Color: 7
Size: 284014 Color: 0

Bin 624: 620 of cap free
Amount of items: 2
Items: 
Size: 642820 Color: 13
Size: 356561 Color: 0

Bin 625: 627 of cap free
Amount of items: 2
Items: 
Size: 741995 Color: 8
Size: 257379 Color: 9

Bin 626: 627 of cap free
Amount of items: 2
Items: 
Size: 778413 Color: 8
Size: 220961 Color: 1

Bin 627: 631 of cap free
Amount of items: 2
Items: 
Size: 677417 Color: 2
Size: 321953 Color: 11

Bin 628: 634 of cap free
Amount of items: 2
Items: 
Size: 610621 Color: 11
Size: 388746 Color: 8

Bin 629: 636 of cap free
Amount of items: 2
Items: 
Size: 624796 Color: 8
Size: 374569 Color: 11

Bin 630: 638 of cap free
Amount of items: 2
Items: 
Size: 552679 Color: 14
Size: 446684 Color: 1

Bin 631: 640 of cap free
Amount of items: 2
Items: 
Size: 506634 Color: 18
Size: 492727 Color: 4

Bin 632: 641 of cap free
Amount of items: 2
Items: 
Size: 512620 Color: 12
Size: 486740 Color: 14

Bin 633: 647 of cap free
Amount of items: 2
Items: 
Size: 727958 Color: 7
Size: 271396 Color: 13

Bin 634: 648 of cap free
Amount of items: 2
Items: 
Size: 545364 Color: 18
Size: 453989 Color: 17

Bin 635: 650 of cap free
Amount of items: 2
Items: 
Size: 640180 Color: 8
Size: 359171 Color: 3

Bin 636: 657 of cap free
Amount of items: 2
Items: 
Size: 590965 Color: 13
Size: 408379 Color: 11

Bin 637: 665 of cap free
Amount of items: 2
Items: 
Size: 590268 Color: 5
Size: 409068 Color: 7

Bin 638: 666 of cap free
Amount of items: 2
Items: 
Size: 536772 Color: 5
Size: 462563 Color: 17

Bin 639: 670 of cap free
Amount of items: 2
Items: 
Size: 791799 Color: 18
Size: 207532 Color: 3

Bin 640: 672 of cap free
Amount of items: 2
Items: 
Size: 531788 Color: 13
Size: 467541 Color: 8

Bin 641: 676 of cap free
Amount of items: 2
Items: 
Size: 523933 Color: 17
Size: 475392 Color: 5

Bin 642: 683 of cap free
Amount of items: 3
Items: 
Size: 651261 Color: 5
Size: 197091 Color: 0
Size: 150966 Color: 12

Bin 643: 683 of cap free
Amount of items: 2
Items: 
Size: 635161 Color: 17
Size: 364157 Color: 0

Bin 644: 691 of cap free
Amount of items: 2
Items: 
Size: 670976 Color: 7
Size: 328334 Color: 2

Bin 645: 692 of cap free
Amount of items: 2
Items: 
Size: 706119 Color: 16
Size: 293190 Color: 2

Bin 646: 694 of cap free
Amount of items: 2
Items: 
Size: 555099 Color: 19
Size: 444208 Color: 7

Bin 647: 700 of cap free
Amount of items: 2
Items: 
Size: 685648 Color: 12
Size: 313653 Color: 6

Bin 648: 706 of cap free
Amount of items: 2
Items: 
Size: 721183 Color: 13
Size: 278112 Color: 8

Bin 649: 708 of cap free
Amount of items: 2
Items: 
Size: 793425 Color: 1
Size: 205868 Color: 4

Bin 650: 709 of cap free
Amount of items: 2
Items: 
Size: 773093 Color: 9
Size: 226199 Color: 7

Bin 651: 712 of cap free
Amount of items: 2
Items: 
Size: 568037 Color: 3
Size: 431252 Color: 14

Bin 652: 718 of cap free
Amount of items: 2
Items: 
Size: 664951 Color: 7
Size: 334332 Color: 12

Bin 653: 726 of cap free
Amount of items: 2
Items: 
Size: 571807 Color: 3
Size: 427468 Color: 12

Bin 654: 727 of cap free
Amount of items: 2
Items: 
Size: 779753 Color: 5
Size: 219521 Color: 3

Bin 655: 729 of cap free
Amount of items: 2
Items: 
Size: 517263 Color: 5
Size: 482009 Color: 10

Bin 656: 730 of cap free
Amount of items: 2
Items: 
Size: 659372 Color: 7
Size: 339899 Color: 11

Bin 657: 733 of cap free
Amount of items: 2
Items: 
Size: 524760 Color: 15
Size: 474508 Color: 18

Bin 658: 736 of cap free
Amount of items: 2
Items: 
Size: 511504 Color: 12
Size: 487761 Color: 8

Bin 659: 738 of cap free
Amount of items: 2
Items: 
Size: 718271 Color: 15
Size: 280992 Color: 7

Bin 660: 739 of cap free
Amount of items: 2
Items: 
Size: 783747 Color: 0
Size: 215515 Color: 9

Bin 661: 744 of cap free
Amount of items: 2
Items: 
Size: 754032 Color: 15
Size: 245225 Color: 3

Bin 662: 745 of cap free
Amount of items: 2
Items: 
Size: 789217 Color: 2
Size: 210039 Color: 10

Bin 663: 752 of cap free
Amount of items: 3
Items: 
Size: 449610 Color: 14
Size: 371434 Color: 8
Size: 178205 Color: 5

Bin 664: 755 of cap free
Amount of items: 2
Items: 
Size: 614556 Color: 12
Size: 384690 Color: 6

Bin 665: 758 of cap free
Amount of items: 2
Items: 
Size: 796722 Color: 0
Size: 202521 Color: 6

Bin 666: 759 of cap free
Amount of items: 2
Items: 
Size: 506572 Color: 17
Size: 492670 Color: 12

Bin 667: 760 of cap free
Amount of items: 3
Items: 
Size: 551297 Color: 11
Size: 313019 Color: 9
Size: 134925 Color: 5

Bin 668: 760 of cap free
Amount of items: 2
Items: 
Size: 612113 Color: 9
Size: 387128 Color: 10

Bin 669: 765 of cap free
Amount of items: 2
Items: 
Size: 650339 Color: 3
Size: 348897 Color: 1

Bin 670: 771 of cap free
Amount of items: 2
Items: 
Size: 537542 Color: 0
Size: 461688 Color: 18

Bin 671: 775 of cap free
Amount of items: 2
Items: 
Size: 539756 Color: 9
Size: 459470 Color: 5

Bin 672: 781 of cap free
Amount of items: 2
Items: 
Size: 703233 Color: 4
Size: 295987 Color: 17

Bin 673: 788 of cap free
Amount of items: 2
Items: 
Size: 568928 Color: 11
Size: 430285 Color: 1

Bin 674: 790 of cap free
Amount of items: 3
Items: 
Size: 566035 Color: 8
Size: 296932 Color: 0
Size: 136244 Color: 7

Bin 675: 800 of cap free
Amount of items: 2
Items: 
Size: 735429 Color: 5
Size: 263772 Color: 9

Bin 676: 801 of cap free
Amount of items: 2
Items: 
Size: 739712 Color: 15
Size: 259488 Color: 13

Bin 677: 801 of cap free
Amount of items: 2
Items: 
Size: 648527 Color: 8
Size: 350673 Color: 10

Bin 678: 801 of cap free
Amount of items: 2
Items: 
Size: 571194 Color: 17
Size: 428006 Color: 8

Bin 679: 803 of cap free
Amount of items: 2
Items: 
Size: 609147 Color: 2
Size: 390051 Color: 12

Bin 680: 806 of cap free
Amount of items: 2
Items: 
Size: 621982 Color: 1
Size: 377213 Color: 16

Bin 681: 822 of cap free
Amount of items: 2
Items: 
Size: 683270 Color: 14
Size: 315909 Color: 1

Bin 682: 829 of cap free
Amount of items: 2
Items: 
Size: 512426 Color: 19
Size: 486746 Color: 0

Bin 683: 829 of cap free
Amount of items: 2
Items: 
Size: 724886 Color: 11
Size: 274286 Color: 5

Bin 684: 834 of cap free
Amount of items: 2
Items: 
Size: 768594 Color: 11
Size: 230573 Color: 6

Bin 685: 837 of cap free
Amount of items: 2
Items: 
Size: 606745 Color: 8
Size: 392419 Color: 10

Bin 686: 838 of cap free
Amount of items: 3
Items: 
Size: 565852 Color: 13
Size: 279848 Color: 0
Size: 153463 Color: 3

Bin 687: 839 of cap free
Amount of items: 2
Items: 
Size: 730460 Color: 2
Size: 268702 Color: 3

Bin 688: 850 of cap free
Amount of items: 2
Items: 
Size: 581460 Color: 3
Size: 417691 Color: 16

Bin 689: 861 of cap free
Amount of items: 2
Items: 
Size: 578400 Color: 10
Size: 420740 Color: 17

Bin 690: 864 of cap free
Amount of items: 2
Items: 
Size: 505122 Color: 14
Size: 494015 Color: 6

Bin 691: 869 of cap free
Amount of items: 2
Items: 
Size: 668267 Color: 7
Size: 330865 Color: 9

Bin 692: 878 of cap free
Amount of items: 2
Items: 
Size: 730446 Color: 8
Size: 268677 Color: 2

Bin 693: 880 of cap free
Amount of items: 2
Items: 
Size: 731774 Color: 17
Size: 267347 Color: 6

Bin 694: 883 of cap free
Amount of items: 3
Items: 
Size: 720578 Color: 8
Size: 145376 Color: 1
Size: 133164 Color: 2

Bin 695: 885 of cap free
Amount of items: 2
Items: 
Size: 559056 Color: 3
Size: 440060 Color: 12

Bin 696: 887 of cap free
Amount of items: 2
Items: 
Size: 599212 Color: 5
Size: 399902 Color: 11

Bin 697: 889 of cap free
Amount of items: 2
Items: 
Size: 778255 Color: 3
Size: 220857 Color: 6

Bin 698: 892 of cap free
Amount of items: 2
Items: 
Size: 793474 Color: 4
Size: 205635 Color: 10

Bin 699: 896 of cap free
Amount of items: 2
Items: 
Size: 713476 Color: 17
Size: 285629 Color: 7

Bin 700: 923 of cap free
Amount of items: 2
Items: 
Size: 511769 Color: 11
Size: 487309 Color: 14

Bin 701: 930 of cap free
Amount of items: 2
Items: 
Size: 534258 Color: 19
Size: 464813 Color: 1

Bin 702: 932 of cap free
Amount of items: 2
Items: 
Size: 586636 Color: 11
Size: 412433 Color: 18

Bin 703: 933 of cap free
Amount of items: 2
Items: 
Size: 672350 Color: 1
Size: 326718 Color: 2

Bin 704: 934 of cap free
Amount of items: 2
Items: 
Size: 727868 Color: 2
Size: 271199 Color: 9

Bin 705: 954 of cap free
Amount of items: 2
Items: 
Size: 656266 Color: 0
Size: 342781 Color: 18

Bin 706: 967 of cap free
Amount of items: 2
Items: 
Size: 741908 Color: 10
Size: 257126 Color: 17

Bin 707: 975 of cap free
Amount of items: 2
Items: 
Size: 746051 Color: 17
Size: 252975 Color: 11

Bin 708: 994 of cap free
Amount of items: 2
Items: 
Size: 754940 Color: 12
Size: 244067 Color: 4

Bin 709: 996 of cap free
Amount of items: 2
Items: 
Size: 726117 Color: 17
Size: 272888 Color: 2

Bin 710: 1005 of cap free
Amount of items: 2
Items: 
Size: 540794 Color: 16
Size: 458202 Color: 8

Bin 711: 1030 of cap free
Amount of items: 2
Items: 
Size: 542208 Color: 1
Size: 456763 Color: 18

Bin 712: 1033 of cap free
Amount of items: 2
Items: 
Size: 605304 Color: 4
Size: 393664 Color: 9

Bin 713: 1039 of cap free
Amount of items: 2
Items: 
Size: 614353 Color: 10
Size: 384609 Color: 9

Bin 714: 1039 of cap free
Amount of items: 2
Items: 
Size: 592072 Color: 6
Size: 406890 Color: 8

Bin 715: 1047 of cap free
Amount of items: 2
Items: 
Size: 621917 Color: 17
Size: 377037 Color: 1

Bin 716: 1095 of cap free
Amount of items: 2
Items: 
Size: 523361 Color: 7
Size: 475545 Color: 19

Bin 717: 1107 of cap free
Amount of items: 3
Items: 
Size: 507754 Color: 3
Size: 315483 Color: 11
Size: 175657 Color: 14

Bin 718: 1107 of cap free
Amount of items: 2
Items: 
Size: 522624 Color: 0
Size: 476270 Color: 7

Bin 719: 1111 of cap free
Amount of items: 2
Items: 
Size: 716820 Color: 4
Size: 282070 Color: 1

Bin 720: 1132 of cap free
Amount of items: 2
Items: 
Size: 699442 Color: 0
Size: 299427 Color: 7

Bin 721: 1142 of cap free
Amount of items: 2
Items: 
Size: 588209 Color: 10
Size: 410650 Color: 0

Bin 722: 1150 of cap free
Amount of items: 2
Items: 
Size: 741865 Color: 5
Size: 256986 Color: 7

Bin 723: 1154 of cap free
Amount of items: 2
Items: 
Size: 771611 Color: 2
Size: 227236 Color: 1

Bin 724: 1156 of cap free
Amount of items: 2
Items: 
Size: 768281 Color: 8
Size: 230564 Color: 15

Bin 725: 1161 of cap free
Amount of items: 2
Items: 
Size: 518713 Color: 12
Size: 480127 Color: 17

Bin 726: 1163 of cap free
Amount of items: 2
Items: 
Size: 775952 Color: 2
Size: 222886 Color: 4

Bin 727: 1165 of cap free
Amount of items: 2
Items: 
Size: 610130 Color: 0
Size: 388706 Color: 13

Bin 728: 1186 of cap free
Amount of items: 2
Items: 
Size: 529093 Color: 16
Size: 469722 Color: 0

Bin 729: 1186 of cap free
Amount of items: 2
Items: 
Size: 764139 Color: 1
Size: 234676 Color: 19

Bin 730: 1187 of cap free
Amount of items: 3
Items: 
Size: 697695 Color: 1
Size: 167573 Color: 19
Size: 133546 Color: 16

Bin 731: 1191 of cap free
Amount of items: 2
Items: 
Size: 731442 Color: 1
Size: 267368 Color: 2

Bin 732: 1198 of cap free
Amount of items: 3
Items: 
Size: 695724 Color: 5
Size: 171870 Color: 6
Size: 131209 Color: 9

Bin 733: 1204 of cap free
Amount of items: 3
Items: 
Size: 667775 Color: 4
Size: 220515 Color: 19
Size: 110507 Color: 18

Bin 734: 1227 of cap free
Amount of items: 2
Items: 
Size: 543675 Color: 8
Size: 455099 Color: 9

Bin 735: 1248 of cap free
Amount of items: 2
Items: 
Size: 757979 Color: 12
Size: 240774 Color: 11

Bin 736: 1268 of cap free
Amount of items: 2
Items: 
Size: 740521 Color: 14
Size: 258212 Color: 1

Bin 737: 1276 of cap free
Amount of items: 2
Items: 
Size: 598865 Color: 4
Size: 399860 Color: 17

Bin 738: 1279 of cap free
Amount of items: 2
Items: 
Size: 537956 Color: 6
Size: 460766 Color: 18

Bin 739: 1280 of cap free
Amount of items: 2
Items: 
Size: 667574 Color: 13
Size: 331147 Color: 0

Bin 740: 1294 of cap free
Amount of items: 2
Items: 
Size: 532560 Color: 8
Size: 466147 Color: 16

Bin 741: 1296 of cap free
Amount of items: 2
Items: 
Size: 757947 Color: 13
Size: 240758 Color: 18

Bin 742: 1327 of cap free
Amount of items: 2
Items: 
Size: 554702 Color: 8
Size: 443972 Color: 18

Bin 743: 1327 of cap free
Amount of items: 2
Items: 
Size: 518765 Color: 17
Size: 479909 Color: 0

Bin 744: 1352 of cap free
Amount of items: 2
Items: 
Size: 727390 Color: 9
Size: 271259 Color: 10

Bin 745: 1353 of cap free
Amount of items: 2
Items: 
Size: 550559 Color: 16
Size: 448089 Color: 5

Bin 746: 1358 of cap free
Amount of items: 2
Items: 
Size: 691889 Color: 3
Size: 306754 Color: 11

Bin 747: 1364 of cap free
Amount of items: 2
Items: 
Size: 515838 Color: 15
Size: 482799 Color: 5

Bin 748: 1365 of cap free
Amount of items: 2
Items: 
Size: 651170 Color: 17
Size: 347466 Color: 19

Bin 749: 1378 of cap free
Amount of items: 2
Items: 
Size: 600271 Color: 2
Size: 398352 Color: 1

Bin 750: 1384 of cap free
Amount of items: 2
Items: 
Size: 683361 Color: 11
Size: 315256 Color: 19

Bin 751: 1403 of cap free
Amount of items: 2
Items: 
Size: 500092 Color: 5
Size: 498506 Color: 11

Bin 752: 1413 of cap free
Amount of items: 2
Items: 
Size: 637255 Color: 13
Size: 361333 Color: 6

Bin 753: 1430 of cap free
Amount of items: 2
Items: 
Size: 556893 Color: 11
Size: 441678 Color: 6

Bin 754: 1433 of cap free
Amount of items: 2
Items: 
Size: 529341 Color: 0
Size: 469227 Color: 16

Bin 755: 1496 of cap free
Amount of items: 2
Items: 
Size: 675877 Color: 17
Size: 322628 Color: 15

Bin 756: 1497 of cap free
Amount of items: 2
Items: 
Size: 706059 Color: 1
Size: 292445 Color: 19

Bin 757: 1500 of cap free
Amount of items: 2
Items: 
Size: 724395 Color: 18
Size: 274106 Color: 5

Bin 758: 1511 of cap free
Amount of items: 2
Items: 
Size: 562739 Color: 9
Size: 435751 Color: 12

Bin 759: 1511 of cap free
Amount of items: 2
Items: 
Size: 692250 Color: 10
Size: 306240 Color: 3

Bin 760: 1518 of cap free
Amount of items: 2
Items: 
Size: 643780 Color: 8
Size: 354703 Color: 19

Bin 761: 1521 of cap free
Amount of items: 2
Items: 
Size: 641734 Color: 7
Size: 356746 Color: 1

Bin 762: 1524 of cap free
Amount of items: 2
Items: 
Size: 749159 Color: 19
Size: 249318 Color: 11

Bin 763: 1551 of cap free
Amount of items: 2
Items: 
Size: 510904 Color: 15
Size: 487546 Color: 10

Bin 764: 1559 of cap free
Amount of items: 2
Items: 
Size: 612701 Color: 16
Size: 385741 Color: 17

Bin 765: 1560 of cap free
Amount of items: 2
Items: 
Size: 568874 Color: 2
Size: 429567 Color: 15

Bin 766: 1567 of cap free
Amount of items: 2
Items: 
Size: 571243 Color: 2
Size: 427191 Color: 3

Bin 767: 1592 of cap free
Amount of items: 2
Items: 
Size: 621469 Color: 11
Size: 376940 Color: 18

Bin 768: 1625 of cap free
Amount of items: 2
Items: 
Size: 611402 Color: 18
Size: 386974 Color: 10

Bin 769: 1669 of cap free
Amount of items: 2
Items: 
Size: 500083 Color: 6
Size: 498249 Color: 12

Bin 770: 1690 of cap free
Amount of items: 2
Items: 
Size: 573522 Color: 4
Size: 424789 Color: 5

Bin 771: 1699 of cap free
Amount of items: 2
Items: 
Size: 568789 Color: 13
Size: 429513 Color: 12

Bin 772: 1763 of cap free
Amount of items: 2
Items: 
Size: 499991 Color: 15
Size: 498247 Color: 17

Bin 773: 1764 of cap free
Amount of items: 2
Items: 
Size: 775489 Color: 19
Size: 222748 Color: 13

Bin 774: 1769 of cap free
Amount of items: 2
Items: 
Size: 748862 Color: 10
Size: 249370 Color: 15

Bin 775: 1780 of cap free
Amount of items: 2
Items: 
Size: 518548 Color: 7
Size: 479673 Color: 9

Bin 776: 1807 of cap free
Amount of items: 2
Items: 
Size: 735073 Color: 9
Size: 263121 Color: 16

Bin 777: 1825 of cap free
Amount of items: 2
Items: 
Size: 748970 Color: 4
Size: 249206 Color: 5

Bin 778: 1829 of cap free
Amount of items: 2
Items: 
Size: 766386 Color: 12
Size: 231786 Color: 4

Bin 779: 1837 of cap free
Amount of items: 2
Items: 
Size: 772286 Color: 0
Size: 225878 Color: 5

Bin 780: 1846 of cap free
Amount of items: 2
Items: 
Size: 633245 Color: 16
Size: 364910 Color: 4

Bin 781: 1853 of cap free
Amount of items: 2
Items: 
Size: 798876 Color: 2
Size: 199272 Color: 13

Bin 782: 1889 of cap free
Amount of items: 2
Items: 
Size: 743313 Color: 1
Size: 254799 Color: 12

Bin 783: 1907 of cap free
Amount of items: 2
Items: 
Size: 536242 Color: 4
Size: 461852 Color: 6

Bin 784: 1911 of cap free
Amount of items: 2
Items: 
Size: 515296 Color: 5
Size: 482794 Color: 4

Bin 785: 1918 of cap free
Amount of items: 2
Items: 
Size: 550082 Color: 11
Size: 448001 Color: 17

Bin 786: 1928 of cap free
Amount of items: 2
Items: 
Size: 550047 Color: 8
Size: 448026 Color: 16

Bin 787: 1941 of cap free
Amount of items: 2
Items: 
Size: 682819 Color: 16
Size: 315241 Color: 5

Bin 788: 1945 of cap free
Amount of items: 2
Items: 
Size: 763549 Color: 1
Size: 234507 Color: 6

Bin 789: 1962 of cap free
Amount of items: 2
Items: 
Size: 653979 Color: 8
Size: 344060 Color: 2

Bin 790: 2032 of cap free
Amount of items: 2
Items: 
Size: 768238 Color: 19
Size: 229731 Color: 0

Bin 791: 2099 of cap free
Amount of items: 2
Items: 
Size: 659529 Color: 16
Size: 338373 Color: 5

Bin 792: 2116 of cap free
Amount of items: 2
Items: 
Size: 518443 Color: 9
Size: 479442 Color: 10

Bin 793: 2146 of cap free
Amount of items: 2
Items: 
Size: 562606 Color: 2
Size: 435249 Color: 9

Bin 794: 2187 of cap free
Amount of items: 2
Items: 
Size: 697424 Color: 2
Size: 300390 Color: 0

Bin 795: 2197 of cap free
Amount of items: 2
Items: 
Size: 757802 Color: 8
Size: 240002 Color: 14

Bin 796: 2238 of cap free
Amount of items: 2
Items: 
Size: 718017 Color: 2
Size: 279746 Color: 18

Bin 797: 2275 of cap free
Amount of items: 2
Items: 
Size: 787932 Color: 14
Size: 209794 Color: 2

Bin 798: 2289 of cap free
Amount of items: 2
Items: 
Size: 518341 Color: 5
Size: 479371 Color: 7

Bin 799: 2310 of cap free
Amount of items: 2
Items: 
Size: 788013 Color: 15
Size: 209678 Color: 12

Bin 800: 2344 of cap free
Amount of items: 2
Items: 
Size: 627597 Color: 15
Size: 370060 Color: 5

Bin 801: 2360 of cap free
Amount of items: 2
Items: 
Size: 775099 Color: 1
Size: 222542 Color: 11

Bin 802: 2369 of cap free
Amount of items: 2
Items: 
Size: 586609 Color: 17
Size: 411023 Color: 4

Bin 803: 2379 of cap free
Amount of items: 2
Items: 
Size: 707398 Color: 8
Size: 290224 Color: 6

Bin 804: 2404 of cap free
Amount of items: 2
Items: 
Size: 554209 Color: 7
Size: 443388 Color: 0

Bin 805: 2426 of cap free
Amount of items: 2
Items: 
Size: 745375 Color: 13
Size: 252200 Color: 18

Bin 806: 2463 of cap free
Amount of items: 2
Items: 
Size: 757671 Color: 12
Size: 239867 Color: 2

Bin 807: 2489 of cap free
Amount of items: 2
Items: 
Size: 531764 Color: 7
Size: 465748 Color: 19

Bin 808: 2504 of cap free
Amount of items: 2
Items: 
Size: 787761 Color: 5
Size: 209736 Color: 7

Bin 809: 2527 of cap free
Amount of items: 2
Items: 
Size: 660555 Color: 5
Size: 336919 Color: 14

Bin 810: 2559 of cap free
Amount of items: 2
Items: 
Size: 753841 Color: 3
Size: 243601 Color: 9

Bin 811: 2578 of cap free
Amount of items: 2
Items: 
Size: 715315 Color: 10
Size: 282108 Color: 1

Bin 812: 2583 of cap free
Amount of items: 2
Items: 
Size: 740544 Color: 16
Size: 256874 Color: 14

Bin 813: 2595 of cap free
Amount of items: 2
Items: 
Size: 779555 Color: 5
Size: 217851 Color: 15

Bin 814: 2603 of cap free
Amount of items: 2
Items: 
Size: 763516 Color: 12
Size: 233882 Color: 8

Bin 815: 2648 of cap free
Amount of items: 2
Items: 
Size: 730279 Color: 2
Size: 267074 Color: 1

Bin 816: 2656 of cap free
Amount of items: 2
Items: 
Size: 774696 Color: 8
Size: 222649 Color: 18

Bin 817: 2690 of cap free
Amount of items: 2
Items: 
Size: 714672 Color: 1
Size: 282639 Color: 0

Bin 818: 2701 of cap free
Amount of items: 2
Items: 
Size: 740610 Color: 13
Size: 256690 Color: 1

Bin 819: 2714 of cap free
Amount of items: 2
Items: 
Size: 562039 Color: 15
Size: 435248 Color: 10

Bin 820: 2734 of cap free
Amount of items: 2
Items: 
Size: 667291 Color: 5
Size: 329976 Color: 6

Bin 821: 2739 of cap free
Amount of items: 2
Items: 
Size: 583170 Color: 2
Size: 414092 Color: 15

Bin 822: 2754 of cap free
Amount of items: 2
Items: 
Size: 598898 Color: 1
Size: 398349 Color: 10

Bin 823: 2799 of cap free
Amount of items: 2
Items: 
Size: 734787 Color: 18
Size: 262415 Color: 7

Bin 824: 2815 of cap free
Amount of items: 2
Items: 
Size: 636211 Color: 10
Size: 360975 Color: 14

Bin 825: 2832 of cap free
Amount of items: 2
Items: 
Size: 540576 Color: 14
Size: 456593 Color: 0

Bin 826: 2856 of cap free
Amount of items: 2
Items: 
Size: 672152 Color: 9
Size: 324993 Color: 15

Bin 827: 2862 of cap free
Amount of items: 2
Items: 
Size: 636581 Color: 6
Size: 360558 Color: 13

Bin 828: 2951 of cap free
Amount of items: 2
Items: 
Size: 626319 Color: 1
Size: 370731 Color: 0

Bin 829: 2962 of cap free
Amount of items: 2
Items: 
Size: 625645 Color: 15
Size: 371394 Color: 3

Bin 830: 2985 of cap free
Amount of items: 2
Items: 
Size: 650138 Color: 18
Size: 346878 Color: 16

Bin 831: 2990 of cap free
Amount of items: 2
Items: 
Size: 510942 Color: 14
Size: 486069 Color: 10

Bin 832: 3073 of cap free
Amount of items: 2
Items: 
Size: 591904 Color: 12
Size: 405024 Color: 14

Bin 833: 3131 of cap free
Amount of items: 2
Items: 
Size: 510841 Color: 9
Size: 486029 Color: 2

Bin 834: 3183 of cap free
Amount of items: 2
Items: 
Size: 591936 Color: 0
Size: 404882 Color: 6

Bin 835: 3185 of cap free
Amount of items: 6
Items: 
Size: 229511 Color: 19
Size: 183826 Color: 16
Size: 183559 Color: 18
Size: 142562 Color: 4
Size: 141292 Color: 15
Size: 116066 Color: 13

Bin 836: 3204 of cap free
Amount of items: 2
Items: 
Size: 757441 Color: 11
Size: 239356 Color: 6

Bin 837: 3443 of cap free
Amount of items: 2
Items: 
Size: 556880 Color: 19
Size: 439678 Color: 13

Bin 838: 3456 of cap free
Amount of items: 2
Items: 
Size: 624514 Color: 9
Size: 372031 Color: 0

Bin 839: 3557 of cap free
Amount of items: 2
Items: 
Size: 575824 Color: 0
Size: 420620 Color: 16

Bin 840: 3598 of cap free
Amount of items: 2
Items: 
Size: 586079 Color: 0
Size: 410324 Color: 19

Bin 841: 3664 of cap free
Amount of items: 4
Items: 
Size: 315034 Color: 17
Size: 312431 Color: 1
Size: 218268 Color: 14
Size: 150604 Color: 4

Bin 842: 3712 of cap free
Amount of items: 2
Items: 
Size: 536027 Color: 15
Size: 460262 Color: 17

Bin 843: 3718 of cap free
Amount of items: 2
Items: 
Size: 531752 Color: 18
Size: 464531 Color: 2

Bin 844: 3724 of cap free
Amount of items: 2
Items: 
Size: 682789 Color: 9
Size: 313488 Color: 17

Bin 845: 3749 of cap free
Amount of items: 2
Items: 
Size: 582784 Color: 6
Size: 413468 Color: 0

Bin 846: 3809 of cap free
Amount of items: 2
Items: 
Size: 549774 Color: 0
Size: 446418 Color: 7

Bin 847: 4086 of cap free
Amount of items: 2
Items: 
Size: 795490 Color: 7
Size: 200425 Color: 2

Bin 848: 4139 of cap free
Amount of items: 2
Items: 
Size: 531159 Color: 7
Size: 464703 Color: 12

Bin 849: 4219 of cap free
Amount of items: 2
Items: 
Size: 729198 Color: 8
Size: 266584 Color: 7

Bin 850: 4237 of cap free
Amount of items: 2
Items: 
Size: 575270 Color: 16
Size: 420494 Color: 13

Bin 851: 4291 of cap free
Amount of items: 2
Items: 
Size: 510045 Color: 5
Size: 485665 Color: 18

Bin 852: 4309 of cap free
Amount of items: 3
Items: 
Size: 596457 Color: 17
Size: 208298 Color: 9
Size: 190937 Color: 5

Bin 853: 4424 of cap free
Amount of items: 2
Items: 
Size: 521361 Color: 5
Size: 474216 Color: 19

Bin 854: 4497 of cap free
Amount of items: 2
Items: 
Size: 761737 Color: 4
Size: 233767 Color: 6

Bin 855: 4503 of cap free
Amount of items: 2
Items: 
Size: 548987 Color: 17
Size: 446511 Color: 15

Bin 856: 4663 of cap free
Amount of items: 2
Items: 
Size: 498040 Color: 14
Size: 497298 Color: 13

Bin 857: 4749 of cap free
Amount of items: 2
Items: 
Size: 530826 Color: 18
Size: 464426 Color: 11

Bin 858: 4754 of cap free
Amount of items: 2
Items: 
Size: 510241 Color: 17
Size: 485006 Color: 4

Bin 859: 4754 of cap free
Amount of items: 2
Items: 
Size: 530882 Color: 15
Size: 464365 Color: 8

Bin 860: 5149 of cap free
Amount of items: 2
Items: 
Size: 635882 Color: 13
Size: 358970 Color: 17

Bin 861: 5157 of cap free
Amount of items: 2
Items: 
Size: 624158 Color: 14
Size: 370686 Color: 11

Bin 862: 5179 of cap free
Amount of items: 2
Items: 
Size: 509547 Color: 3
Size: 485275 Color: 17

Bin 863: 5397 of cap free
Amount of items: 2
Items: 
Size: 704790 Color: 17
Size: 289814 Color: 1

Bin 864: 5681 of cap free
Amount of items: 2
Items: 
Size: 796150 Color: 18
Size: 198170 Color: 3

Bin 865: 5938 of cap free
Amount of items: 2
Items: 
Size: 574665 Color: 5
Size: 419398 Color: 12

Bin 866: 6079 of cap free
Amount of items: 2
Items: 
Size: 521148 Color: 5
Size: 472774 Color: 16

Bin 867: 6349 of cap free
Amount of items: 2
Items: 
Size: 696263 Color: 9
Size: 297389 Color: 10

Bin 868: 6544 of cap free
Amount of items: 2
Items: 
Size: 794382 Color: 18
Size: 199075 Color: 14

Bin 869: 6602 of cap free
Amount of items: 2
Items: 
Size: 608808 Color: 18
Size: 384591 Color: 7

Bin 870: 6866 of cap free
Amount of items: 2
Items: 
Size: 795804 Color: 3
Size: 197331 Color: 16

Bin 871: 7218 of cap free
Amount of items: 2
Items: 
Size: 681444 Color: 11
Size: 311339 Color: 16

Bin 872: 7293 of cap free
Amount of items: 2
Items: 
Size: 546229 Color: 18
Size: 446479 Color: 11

Bin 873: 8196 of cap free
Amount of items: 2
Items: 
Size: 608239 Color: 15
Size: 383566 Color: 17

Bin 874: 8743 of cap free
Amount of items: 3
Items: 
Size: 422359 Color: 16
Size: 325765 Color: 19
Size: 243134 Color: 16

Bin 875: 8933 of cap free
Amount of items: 2
Items: 
Size: 496026 Color: 16
Size: 495042 Color: 15

Bin 876: 8954 of cap free
Amount of items: 2
Items: 
Size: 582774 Color: 2
Size: 408273 Color: 5

Bin 877: 9042 of cap free
Amount of items: 2
Items: 
Size: 535899 Color: 5
Size: 455060 Color: 10

Bin 878: 10697 of cap free
Amount of items: 2
Items: 
Size: 773892 Color: 19
Size: 215412 Color: 15

Bin 879: 13692 of cap free
Amount of items: 2
Items: 
Size: 616640 Color: 17
Size: 369669 Color: 3

Bin 880: 14010 of cap free
Amount of items: 2
Items: 
Size: 641805 Color: 6
Size: 344186 Color: 11

Bin 881: 14424 of cap free
Amount of items: 2
Items: 
Size: 733419 Color: 0
Size: 252158 Color: 9

Bin 882: 32954 of cap free
Amount of items: 2
Items: 
Size: 568788 Color: 5
Size: 398259 Color: 15

Bin 883: 36000 of cap free
Amount of items: 2
Items: 
Size: 720376 Color: 16
Size: 243625 Color: 19

Bin 884: 36141 of cap free
Amount of items: 2
Items: 
Size: 509222 Color: 13
Size: 454638 Color: 12

Bin 885: 39640 of cap free
Amount of items: 2
Items: 
Size: 783416 Color: 16
Size: 176945 Color: 19

Bin 886: 40561 of cap free
Amount of items: 3
Items: 
Size: 561896 Color: 5
Size: 288408 Color: 0
Size: 109136 Color: 5

Bin 887: 42266 of cap free
Amount of items: 2
Items: 
Size: 775878 Color: 19
Size: 181857 Color: 9

Total size: 886099195
Total free space: 901692

