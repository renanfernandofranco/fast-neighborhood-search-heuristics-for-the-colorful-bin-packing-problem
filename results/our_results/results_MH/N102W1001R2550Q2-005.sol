Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 338 Color: 1
Size: 274 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 343 Color: 1
Size: 279 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 0
Size: 250 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 325 Color: 1
Size: 309 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 362 Color: 0
Size: 269 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 302 Color: 0
Size: 278 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 265 Color: 0
Size: 252 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 292 Color: 0
Size: 278 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 280 Color: 1
Size: 262 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 355 Color: 0
Size: 250 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 261 Color: 0
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 359 Color: 1
Size: 263 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 266 Color: 1
Size: 252 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 349 Color: 0
Size: 280 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 361 Color: 0
Size: 271 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 355 Color: 0
Size: 271 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 338 Color: 0
Size: 287 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 309 Color: 1
Size: 281 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 299 Color: 0
Size: 275 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 285 Color: 1
Size: 284 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 265 Color: 0
Size: 250 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 271 Color: 1
Size: 251 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 263 Color: 0
Size: 254 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 303 Color: 0
Size: 290 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 294 Color: 1
Size: 293 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 312 Color: 1
Size: 279 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 273 Color: 0
Size: 265 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 323 Color: 1
Size: 280 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 0
Size: 344 Color: 1
Size: 269 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 293 Color: 1
Size: 275 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 261 Color: 0
Size: 253 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 349 Color: 0
Size: 290 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 305 Color: 1
Size: 284 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 315 Color: 0
Size: 301 Color: 1

Total size: 34034
Total free space: 0

