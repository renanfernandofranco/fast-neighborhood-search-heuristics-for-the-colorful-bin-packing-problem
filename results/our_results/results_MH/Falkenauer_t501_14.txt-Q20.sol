Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 13
Size: 254 Color: 18
Size: 250 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 15
Size: 361 Color: 6
Size: 251 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 10
Size: 365 Color: 13
Size: 268 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 3
Size: 253 Color: 13
Size: 250 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 7
Size: 292 Color: 15
Size: 264 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 6
Size: 329 Color: 2
Size: 263 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 5
Size: 312 Color: 8
Size: 261 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 330 Color: 5
Size: 260 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 5
Size: 326 Color: 14
Size: 285 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 13
Size: 291 Color: 17
Size: 288 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 11
Size: 260 Color: 9
Size: 254 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 9
Size: 358 Color: 10
Size: 261 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 19
Size: 332 Color: 2
Size: 299 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 13
Size: 348 Color: 9
Size: 280 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 10
Size: 353 Color: 11
Size: 278 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 305 Color: 6
Size: 285 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 16
Size: 333 Color: 3
Size: 256 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 16
Size: 351 Color: 16
Size: 256 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 19
Size: 320 Color: 15
Size: 252 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 19
Size: 282 Color: 9
Size: 262 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 19
Size: 260 Color: 11
Size: 254 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 0
Size: 342 Color: 7
Size: 315 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 14
Size: 253 Color: 12
Size: 252 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 6
Size: 273 Color: 0
Size: 265 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 18
Size: 319 Color: 7
Size: 252 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 5
Size: 325 Color: 3
Size: 284 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 10
Size: 339 Color: 0
Size: 276 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 268 Color: 16
Size: 250 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 317 Color: 0
Size: 271 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 353 Color: 9
Size: 268 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8
Size: 309 Color: 6
Size: 258 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 17
Size: 353 Color: 3
Size: 279 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 9
Size: 250 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 10
Size: 293 Color: 10
Size: 250 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 19
Size: 251 Color: 1
Size: 250 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 7
Size: 314 Color: 16
Size: 256 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 303 Color: 0
Size: 283 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 8
Size: 274 Color: 9
Size: 260 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 16
Size: 279 Color: 10
Size: 251 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 19
Size: 296 Color: 7
Size: 251 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 10
Size: 266 Color: 5
Size: 250 Color: 13

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 18
Size: 274 Color: 8
Size: 259 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 287 Color: 6
Size: 257 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 269 Color: 0
Size: 256 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 13
Size: 316 Color: 14
Size: 252 Color: 17

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 12
Size: 260 Color: 13
Size: 255 Color: 12

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 4
Size: 261 Color: 9
Size: 257 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 15
Size: 313 Color: 18
Size: 273 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 18
Size: 291 Color: 9
Size: 277 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 3
Size: 292 Color: 18
Size: 270 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 12
Size: 337 Color: 4
Size: 256 Color: 7

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 11
Size: 354 Color: 15
Size: 272 Color: 13

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 15
Size: 256 Color: 10
Size: 251 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 354 Color: 5
Size: 274 Color: 18

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 282 Color: 5
Size: 260 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 7
Size: 314 Color: 8
Size: 281 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 18
Size: 350 Color: 3
Size: 253 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 363 Color: 5
Size: 272 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 7
Size: 341 Color: 17
Size: 314 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 260 Color: 7
Size: 254 Color: 8

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 12
Size: 286 Color: 3
Size: 253 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 18
Size: 322 Color: 8
Size: 260 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 321 Color: 15
Size: 274 Color: 8

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 19
Size: 365 Color: 10
Size: 260 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 4
Size: 303 Color: 5
Size: 253 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 7
Size: 256 Color: 8
Size: 250 Color: 15

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 15
Size: 309 Color: 0
Size: 303 Color: 6

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 283 Color: 19
Size: 278 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 286 Color: 6
Size: 276 Color: 12

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 19
Size: 317 Color: 18
Size: 257 Color: 5

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 14
Size: 343 Color: 10
Size: 280 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 261 Color: 1
Size: 253 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 16
Size: 302 Color: 3
Size: 254 Color: 5

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 9
Size: 341 Color: 11
Size: 298 Color: 15

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 15
Size: 364 Color: 15
Size: 254 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 316 Color: 1
Size: 285 Color: 11

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 307 Color: 1
Size: 277 Color: 1
Size: 416 Color: 13

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 320 Color: 0
Size: 279 Color: 14

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 269 Color: 10
Size: 268 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 359 Color: 5
Size: 272 Color: 14

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 14
Size: 277 Color: 17
Size: 268 Color: 5

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 8
Size: 314 Color: 15
Size: 313 Color: 9

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7
Size: 350 Color: 0
Size: 279 Color: 14

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 362 Color: 6
Size: 262 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 353 Color: 16
Size: 273 Color: 14

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 355 Color: 2
Size: 280 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 16
Size: 305 Color: 18
Size: 272 Color: 9

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 306 Color: 16
Size: 290 Color: 7

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 295 Color: 7
Size: 270 Color: 8

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 325 Color: 8
Size: 285 Color: 15

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 10
Size: 343 Color: 11
Size: 310 Color: 10

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 11
Size: 287 Color: 9
Size: 259 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 294 Color: 16
Size: 271 Color: 11

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 18
Size: 359 Color: 15
Size: 272 Color: 6

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 7
Size: 275 Color: 0
Size: 250 Color: 16

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 306 Color: 2
Size: 250 Color: 8

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 18
Size: 343 Color: 14
Size: 257 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 19
Size: 296 Color: 4
Size: 283 Color: 4

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 7
Size: 263 Color: 12
Size: 250 Color: 6

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 15
Size: 260 Color: 16
Size: 250 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 9
Size: 301 Color: 3
Size: 262 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 341 Color: 18
Size: 299 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 7
Size: 296 Color: 4
Size: 294 Color: 5

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 11
Size: 344 Color: 15
Size: 299 Color: 16

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 11
Size: 294 Color: 6
Size: 293 Color: 3

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 18
Size: 347 Color: 18
Size: 263 Color: 7

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 5
Size: 333 Color: 4
Size: 299 Color: 11

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 8
Size: 331 Color: 15
Size: 304 Color: 8

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 8
Size: 332 Color: 13
Size: 304 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 4
Size: 343 Color: 5
Size: 310 Color: 5

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6
Size: 326 Color: 6
Size: 307 Color: 18

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 320 Color: 18
Size: 314 Color: 12

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 257 Color: 6
Size: 253 Color: 6

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 11
Size: 343 Color: 14
Size: 301 Color: 9

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 315 Color: 10
Size: 307 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 254 Color: 13
Size: 251 Color: 12

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 356 Color: 11
Size: 284 Color: 14

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 13
Size: 356 Color: 1
Size: 280 Color: 12

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 359 Color: 10
Size: 281 Color: 10

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 15
Size: 349 Color: 1
Size: 277 Color: 10

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 367 Color: 12
Size: 257 Color: 16

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 362 Color: 0
Size: 257 Color: 8

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 360 Color: 4
Size: 258 Color: 18

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 6
Size: 353 Color: 4
Size: 264 Color: 18

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 19
Size: 357 Color: 12
Size: 255 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 317 Color: 14
Size: 292 Color: 15

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 354 Color: 14
Size: 250 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 17
Size: 346 Color: 19
Size: 255 Color: 13

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 340 Color: 11
Size: 261 Color: 9

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 13
Size: 308 Color: 7
Size: 290 Color: 17

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 10
Size: 298 Color: 1
Size: 297 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 12
Size: 324 Color: 19
Size: 268 Color: 6

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 261 Color: 0
Size: 250 Color: 9

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 12
Size: 312 Color: 3
Size: 277 Color: 13

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 313 Color: 16
Size: 276 Color: 14

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8
Size: 328 Color: 4
Size: 257 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 322 Color: 12
Size: 259 Color: 3

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 12
Size: 295 Color: 2
Size: 281 Color: 17

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 320 Color: 7
Size: 253 Color: 14

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 10
Size: 317 Color: 13
Size: 255 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 14
Size: 312 Color: 9
Size: 257 Color: 18

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 287 Color: 19
Size: 277 Color: 18

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 3
Size: 297 Color: 9
Size: 262 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 5
Size: 295 Color: 9
Size: 264 Color: 14

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 18
Size: 296 Color: 0
Size: 260 Color: 15

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 290 Color: 15
Size: 264 Color: 9

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 7
Size: 300 Color: 11
Size: 251 Color: 18

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 6
Size: 284 Color: 13
Size: 264 Color: 9

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 4
Size: 289 Color: 9
Size: 258 Color: 9

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 14
Size: 280 Color: 18
Size: 264 Color: 3

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 17
Size: 289 Color: 5
Size: 253 Color: 13

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9
Size: 272 Color: 1
Size: 269 Color: 17

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 288 Color: 0
Size: 251 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 6
Size: 275 Color: 12
Size: 258 Color: 16

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 266 Color: 12
Size: 264 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 18
Size: 264 Color: 8
Size: 262 Color: 5

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 18
Size: 267 Color: 1
Size: 258 Color: 12

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 15
Size: 268 Color: 16
Size: 252 Color: 7

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 11
Size: 264 Color: 15
Size: 255 Color: 9

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 6
Size: 266 Color: 1
Size: 251 Color: 16

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 14
Size: 259 Color: 16
Size: 257 Color: 14

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 262 Color: 9
Size: 253 Color: 7

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 16
Size: 265 Color: 14
Size: 250 Color: 18

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 263 Color: 8
Size: 251 Color: 13

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 15
Size: 258 Color: 3
Size: 251 Color: 16

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 14
Size: 257 Color: 14
Size: 255 Color: 16

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 255 Color: 12
Size: 255 Color: 6

Total size: 167000
Total free space: 0

