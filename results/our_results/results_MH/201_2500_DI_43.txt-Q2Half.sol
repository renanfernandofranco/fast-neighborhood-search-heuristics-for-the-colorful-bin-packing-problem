Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1780 Color: 1
Size: 364 Color: 1
Size: 200 Color: 0
Size: 64 Color: 0
Size: 48 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 1
Size: 862 Color: 1
Size: 176 Color: 0
Size: 96 Color: 0
Size: 88 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 244 Color: 1
Size: 40 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 1
Size: 382 Color: 1
Size: 72 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 654 Color: 1
Size: 128 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 684 Color: 1
Size: 128 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 262 Color: 1
Size: 48 Color: 0

Bin 8: 0 of cap free
Amount of items: 5
Items: 
Size: 1696 Color: 1
Size: 512 Color: 1
Size: 112 Color: 1
Size: 88 Color: 0
Size: 48 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 1
Size: 212 Color: 1
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 1364 Color: 1
Size: 568 Color: 1
Size: 508 Color: 0
Size: 16 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 1
Size: 666 Color: 1
Size: 280 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 384 Color: 1
Size: 132 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 1
Size: 470 Color: 1
Size: 92 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 790 Color: 1
Size: 8 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 1
Size: 370 Color: 1
Size: 8 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 1
Size: 300 Color: 1
Size: 56 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 1
Size: 449 Color: 1
Size: 88 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 232 Color: 1
Size: 32 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1437 Color: 1
Size: 851 Color: 1
Size: 168 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 272 Color: 0
Size: 156 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 1
Size: 938 Color: 1
Size: 184 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 1
Size: 460 Color: 1
Size: 88 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 1114 Color: 1
Size: 112 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 1
Size: 260 Color: 1
Size: 48 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 1
Size: 538 Color: 1
Size: 8 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1926 Color: 1
Size: 442 Color: 1
Size: 88 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 1
Size: 541 Color: 1
Size: 108 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 1
Size: 324 Color: 1
Size: 16 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 1
Size: 620 Color: 1
Size: 72 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2012 Color: 1
Size: 372 Color: 1
Size: 72 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 542 Color: 1
Size: 104 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 1
Size: 756 Color: 1
Size: 144 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1020 Color: 1
Size: 200 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 467 Color: 1
Size: 92 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 840 Color: 1
Size: 80 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2015 Color: 1
Size: 369 Color: 1
Size: 72 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 1
Size: 1022 Color: 1
Size: 8 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 1
Size: 620 Color: 1
Size: 120 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 554 Color: 1
Size: 108 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 1
Size: 244 Color: 1
Size: 24 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 976 Color: 1
Size: 916 Color: 1
Size: 564 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 1
Size: 778 Color: 1
Size: 152 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 1
Size: 828 Color: 1
Size: 160 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 1
Size: 436 Color: 1
Size: 168 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 610 Color: 1
Size: 64 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 1
Size: 719 Color: 1
Size: 142 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 1
Size: 371 Color: 1
Size: 74 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2082 Color: 1
Size: 362 Color: 1
Size: 12 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 555 Color: 1
Size: 102 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 1
Size: 386 Color: 1
Size: 76 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1023 Color: 1
Size: 204 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 1
Size: 383 Color: 1
Size: 76 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 1
Size: 261 Color: 1
Size: 50 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 1
Size: 857 Color: 1
Size: 170 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 1
Size: 753 Color: 1
Size: 100 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 1
Size: 631 Color: 1
Size: 126 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 1
Size: 653 Color: 1
Size: 96 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1911 Color: 1
Size: 455 Color: 1
Size: 90 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2003 Color: 1
Size: 379 Color: 1
Size: 74 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2081 Color: 1
Size: 313 Color: 1
Size: 62 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2089 Color: 1
Size: 307 Color: 1
Size: 60 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 1
Size: 306 Color: 1
Size: 60 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2139 Color: 1
Size: 265 Color: 1
Size: 52 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 1
Size: 222 Color: 1
Size: 44 Color: 0

Total size: 159640
Total free space: 0

