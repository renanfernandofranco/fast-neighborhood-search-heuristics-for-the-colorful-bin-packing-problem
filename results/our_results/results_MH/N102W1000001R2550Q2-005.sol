Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 490315 Color: 0
Size: 253750 Color: 1
Size: 255936 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 362549 Color: 0
Size: 332999 Color: 1
Size: 304453 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 496617 Color: 0
Size: 252478 Color: 1
Size: 250906 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 372328 Color: 0
Size: 359310 Color: 1
Size: 268363 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 449820 Color: 1
Size: 285528 Color: 0
Size: 264653 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 416500 Color: 0
Size: 285912 Color: 0
Size: 297589 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370253 Color: 1
Size: 367697 Color: 0
Size: 262051 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 401307 Color: 1
Size: 324349 Color: 0
Size: 274345 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 392591 Color: 0
Size: 332909 Color: 0
Size: 274501 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 402399 Color: 1
Size: 341887 Color: 0
Size: 255715 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 412561 Color: 1
Size: 325348 Color: 0
Size: 262092 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 422183 Color: 0
Size: 319351 Color: 1
Size: 258467 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 349231 Color: 1
Size: 331163 Color: 0
Size: 319607 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 384924 Color: 1
Size: 335175 Color: 0
Size: 279902 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 384413 Color: 0
Size: 361663 Color: 1
Size: 253925 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 470873 Color: 1
Size: 272672 Color: 0
Size: 256456 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 401105 Color: 0
Size: 317235 Color: 0
Size: 281661 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 445680 Color: 0
Size: 303889 Color: 1
Size: 250432 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375015 Color: 1
Size: 317581 Color: 0
Size: 307405 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368806 Color: 1
Size: 335260 Color: 0
Size: 295935 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 378359 Color: 0
Size: 338238 Color: 0
Size: 283404 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 343823 Color: 1
Size: 341791 Color: 1
Size: 314387 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 365721 Color: 1
Size: 340175 Color: 0
Size: 294105 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 454967 Color: 0
Size: 290841 Color: 1
Size: 254193 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 430926 Color: 1
Size: 295375 Color: 1
Size: 273700 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 338116 Color: 0
Size: 337768 Color: 1
Size: 324117 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 460843 Color: 1
Size: 279245 Color: 1
Size: 259913 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 366170 Color: 1
Size: 360264 Color: 0
Size: 273567 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 423680 Color: 0
Size: 323092 Color: 1
Size: 253229 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 365974 Color: 0
Size: 364316 Color: 1
Size: 269711 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 485849 Color: 1
Size: 260513 Color: 0
Size: 253639 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 372785 Color: 0
Size: 366180 Color: 1
Size: 261036 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 433969 Color: 0
Size: 302977 Color: 0
Size: 263055 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 450839 Color: 0
Size: 278445 Color: 1
Size: 270717 Color: 1

Total size: 34000034
Total free space: 0

