Capicity Bin: 1000001
Lower Bound: 46

Bins used: 47
Amount of Colors: 2

Bin 1: 123 of cap free
Amount of items: 3
Items: 
Size: 768346 Color: 1
Size: 123115 Color: 0
Size: 108417 Color: 1

Bin 2: 303 of cap free
Amount of items: 2
Items: 
Size: 668617 Color: 1
Size: 331081 Color: 0

Bin 3: 681 of cap free
Amount of items: 2
Items: 
Size: 778226 Color: 1
Size: 221094 Color: 0

Bin 4: 865 of cap free
Amount of items: 3
Items: 
Size: 584334 Color: 0
Size: 283728 Color: 1
Size: 131074 Color: 1

Bin 5: 1298 of cap free
Amount of items: 2
Items: 
Size: 745119 Color: 1
Size: 253584 Color: 0

Bin 6: 2172 of cap free
Amount of items: 2
Items: 
Size: 786747 Color: 0
Size: 211082 Color: 1

Bin 7: 2245 of cap free
Amount of items: 3
Items: 
Size: 460018 Color: 0
Size: 385051 Color: 1
Size: 152687 Color: 1

Bin 8: 2296 of cap free
Amount of items: 2
Items: 
Size: 508848 Color: 0
Size: 488857 Color: 1

Bin 9: 2387 of cap free
Amount of items: 2
Items: 
Size: 746539 Color: 0
Size: 251075 Color: 1

Bin 10: 3440 of cap free
Amount of items: 3
Items: 
Size: 764712 Color: 0
Size: 122196 Color: 1
Size: 109653 Color: 0

Bin 11: 4117 of cap free
Amount of items: 2
Items: 
Size: 633416 Color: 1
Size: 362468 Color: 0

Bin 12: 4326 of cap free
Amount of items: 2
Items: 
Size: 576875 Color: 0
Size: 418800 Color: 1

Bin 13: 4751 of cap free
Amount of items: 2
Items: 
Size: 588052 Color: 1
Size: 407198 Color: 0

Bin 14: 4961 of cap free
Amount of items: 3
Items: 
Size: 440481 Color: 0
Size: 424265 Color: 0
Size: 130294 Color: 1

Bin 15: 5048 of cap free
Amount of items: 2
Items: 
Size: 719187 Color: 1
Size: 275766 Color: 0

Bin 16: 6249 of cap free
Amount of items: 2
Items: 
Size: 654054 Color: 1
Size: 339698 Color: 0

Bin 17: 7895 of cap free
Amount of items: 3
Items: 
Size: 448330 Color: 0
Size: 369800 Color: 0
Size: 173976 Color: 1

Bin 18: 7947 of cap free
Amount of items: 2
Items: 
Size: 587494 Color: 1
Size: 404560 Color: 0

Bin 19: 8276 of cap free
Amount of items: 2
Items: 
Size: 750604 Color: 0
Size: 241121 Color: 1

Bin 20: 8758 of cap free
Amount of items: 2
Items: 
Size: 608460 Color: 1
Size: 382783 Color: 0

Bin 21: 9111 of cap free
Amount of items: 3
Items: 
Size: 426976 Color: 1
Size: 422393 Color: 0
Size: 141521 Color: 0

Bin 22: 10217 of cap free
Amount of items: 2
Items: 
Size: 741205 Color: 1
Size: 248579 Color: 0

Bin 23: 12516 of cap free
Amount of items: 2
Items: 
Size: 655450 Color: 0
Size: 332035 Color: 1

Bin 24: 12724 of cap free
Amount of items: 2
Items: 
Size: 506925 Color: 1
Size: 480352 Color: 0

Bin 25: 13412 of cap free
Amount of items: 2
Items: 
Size: 782021 Color: 0
Size: 204568 Color: 1

Bin 26: 15063 of cap free
Amount of items: 2
Items: 
Size: 736635 Color: 1
Size: 248303 Color: 0

Bin 27: 15540 of cap free
Amount of items: 2
Items: 
Size: 635335 Color: 0
Size: 349126 Color: 1

Bin 28: 19762 of cap free
Amount of items: 2
Items: 
Size: 646869 Color: 1
Size: 333370 Color: 0

Bin 29: 20403 of cap free
Amount of items: 2
Items: 
Size: 731809 Color: 1
Size: 247789 Color: 0

Bin 30: 22684 of cap free
Amount of items: 2
Items: 
Size: 578054 Color: 0
Size: 399263 Color: 1

Bin 31: 22832 of cap free
Amount of items: 2
Items: 
Size: 719610 Color: 0
Size: 257559 Color: 1

Bin 32: 25267 of cap free
Amount of items: 2
Items: 
Size: 670122 Color: 0
Size: 304612 Color: 1

Bin 33: 27534 of cap free
Amount of items: 2
Items: 
Size: 678135 Color: 0
Size: 294332 Color: 1

Bin 34: 29357 of cap free
Amount of items: 2
Items: 
Size: 655772 Color: 1
Size: 314872 Color: 0

Bin 35: 30152 of cap free
Amount of items: 2
Items: 
Size: 696490 Color: 1
Size: 273359 Color: 0

Bin 36: 31164 of cap free
Amount of items: 2
Items: 
Size: 654848 Color: 0
Size: 313989 Color: 1

Bin 37: 36532 of cap free
Amount of items: 2
Items: 
Size: 761516 Color: 0
Size: 201953 Color: 1

Bin 38: 38585 of cap free
Amount of items: 2
Items: 
Size: 515846 Color: 0
Size: 445570 Color: 1

Bin 39: 44319 of cap free
Amount of items: 2
Items: 
Size: 633121 Color: 1
Size: 322561 Color: 0

Bin 40: 45223 of cap free
Amount of items: 2
Items: 
Size: 773661 Color: 0
Size: 181117 Color: 1

Bin 41: 46612 of cap free
Amount of items: 2
Items: 
Size: 792913 Color: 1
Size: 160476 Color: 0

Bin 42: 49563 of cap free
Amount of items: 2
Items: 
Size: 656393 Color: 0
Size: 294045 Color: 1

Bin 43: 60513 of cap free
Amount of items: 2
Items: 
Size: 669420 Color: 1
Size: 270068 Color: 0

Bin 44: 61042 of cap free
Amount of items: 2
Items: 
Size: 513795 Color: 0
Size: 425164 Color: 1

Bin 45: 85769 of cap free
Amount of items: 2
Items: 
Size: 760359 Color: 1
Size: 153873 Color: 0

Bin 46: 197755 of cap free
Amount of items: 3
Items: 
Size: 433771 Color: 0
Size: 203367 Color: 1
Size: 165108 Color: 1

Bin 47: 200433 of cap free
Amount of items: 2
Items: 
Size: 659874 Color: 0
Size: 139694 Color: 1

Total size: 45737825
Total free space: 1262222

