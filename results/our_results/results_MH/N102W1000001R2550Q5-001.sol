Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 492066 Color: 2
Size: 252880 Color: 4
Size: 255055 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 457133 Color: 0
Size: 283242 Color: 2
Size: 259626 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 392760 Color: 3
Size: 314002 Color: 3
Size: 293239 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 426161 Color: 0
Size: 301628 Color: 3
Size: 272212 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 497579 Color: 1
Size: 250809 Color: 4
Size: 251613 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 374078 Color: 3
Size: 359775 Color: 1
Size: 266148 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 424367 Color: 3
Size: 294379 Color: 2
Size: 281255 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 413614 Color: 3
Size: 332097 Color: 2
Size: 254290 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 363133 Color: 3
Size: 336694 Color: 4
Size: 300174 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 439079 Color: 1
Size: 296018 Color: 4
Size: 264904 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 480176 Color: 3
Size: 252326 Color: 4
Size: 267499 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 359115 Color: 1
Size: 353233 Color: 4
Size: 287653 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 368570 Color: 4
Size: 316496 Color: 1
Size: 314935 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 358354 Color: 2
Size: 339325 Color: 0
Size: 302322 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 347027 Color: 2
Size: 311585 Color: 1
Size: 341389 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 461845 Color: 4
Size: 269162 Color: 3
Size: 268994 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 376346 Color: 0
Size: 280765 Color: 0
Size: 342890 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 378440 Color: 2
Size: 328597 Color: 1
Size: 292964 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 451037 Color: 2
Size: 292865 Color: 2
Size: 256099 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 494846 Color: 1
Size: 253665 Color: 0
Size: 251490 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 429321 Color: 3
Size: 306861 Color: 2
Size: 263819 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 368455 Color: 2
Size: 316419 Color: 0
Size: 315127 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 368562 Color: 4
Size: 336967 Color: 0
Size: 294472 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 379394 Color: 4
Size: 362693 Color: 0
Size: 257914 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 389387 Color: 4
Size: 352476 Color: 2
Size: 258138 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 393630 Color: 4
Size: 346469 Color: 1
Size: 259902 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 397972 Color: 3
Size: 342049 Color: 4
Size: 259980 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 403577 Color: 3
Size: 306905 Color: 1
Size: 289519 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 421710 Color: 2
Size: 314673 Color: 1
Size: 263618 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 430102 Color: 3
Size: 311317 Color: 2
Size: 258582 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 429903 Color: 2
Size: 306184 Color: 0
Size: 263914 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 432535 Color: 0
Size: 315307 Color: 4
Size: 252159 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 441327 Color: 3
Size: 304506 Color: 4
Size: 254168 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 455833 Color: 3
Size: 274217 Color: 4
Size: 269951 Color: 1

Total size: 34000034
Total free space: 0

