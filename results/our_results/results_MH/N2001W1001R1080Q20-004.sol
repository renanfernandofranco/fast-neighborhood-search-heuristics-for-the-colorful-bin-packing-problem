Capicity Bin: 1001
Lower Bound: 901

Bins used: 901
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 625 Color: 8
Size: 238 Color: 19
Size: 138 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 7
Size: 238 Color: 19
Size: 137 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 9
Size: 182 Color: 16
Size: 176 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 10
Size: 174 Color: 13
Size: 148 Color: 19

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 11
Size: 305 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 0
Size: 154 Color: 2
Size: 140 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 6
Size: 151 Color: 12
Size: 143 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 10
Size: 298 Color: 2
Size: 239 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 4
Size: 147 Color: 0
Size: 121 Color: 14

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 17
Size: 242 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 19
Size: 161 Color: 18
Size: 127 Color: 9

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 8
Size: 484 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 18
Size: 197 Color: 17
Size: 173 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 15
Size: 412 Color: 12
Size: 105 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 10
Size: 235 Color: 11
Size: 191 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 6
Size: 281 Color: 3
Size: 232 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 559 Color: 1
Size: 313 Color: 9
Size: 129 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 16
Size: 193 Color: 0
Size: 154 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 307 Color: 9
Size: 281 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 2
Size: 194 Color: 4
Size: 161 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 13
Size: 178 Color: 14
Size: 170 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 7
Size: 196 Color: 5
Size: 177 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 784 Color: 16
Size: 109 Color: 2
Size: 108 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 772 Color: 13
Size: 119 Color: 3
Size: 110 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 11
Size: 331 Color: 16
Size: 280 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 0
Size: 148 Color: 8
Size: 146 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 772 Color: 7
Size: 116 Color: 8
Size: 113 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 19
Size: 128 Color: 18
Size: 103 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 17
Size: 157 Color: 12
Size: 136 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 0
Size: 313 Color: 15
Size: 156 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 11
Size: 137 Color: 15
Size: 111 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 11
Size: 335 Color: 6
Size: 188 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 1
Size: 182 Color: 5
Size: 176 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 625 Color: 2
Size: 190 Color: 13
Size: 186 Color: 13

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 5
Size: 423 Color: 4

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 4
Size: 305 Color: 17

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 18
Size: 499 Color: 11

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 11
Size: 412 Color: 7

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 16
Size: 368 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 18
Size: 368 Color: 14
Size: 149 Color: 14

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 9
Size: 305 Color: 7
Size: 273 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 13
Size: 324 Color: 14
Size: 195 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 16
Size: 113 Color: 12
Size: 106 Color: 15

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 7
Size: 305 Color: 15
Size: 273 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 19
Size: 413 Color: 3
Size: 105 Color: 16

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 14
Size: 205 Color: 6

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 10
Size: 230 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 799 Color: 8
Size: 101 Color: 9
Size: 101 Color: 8

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 9
Size: 230 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 778 Color: 3
Size: 119 Color: 11
Size: 104 Color: 13

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 13
Size: 205 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 2
Size: 126 Color: 9
Size: 122 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 6
Size: 104 Color: 6
Size: 101 Color: 9

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 8
Size: 442 Color: 10

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 13
Size: 361 Color: 3

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 7
Size: 469 Color: 18

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 9
Size: 361 Color: 15

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 8
Size: 361 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 5
Size: 174 Color: 9
Size: 168 Color: 18

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 4
Size: 313 Color: 3
Size: 156 Color: 8

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 559 Color: 11
Size: 312 Color: 14
Size: 130 Color: 7

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 8
Size: 190 Color: 19
Size: 171 Color: 12

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 16

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 1
Size: 375 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 3
Size: 238 Color: 5
Size: 137 Color: 8

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 17
Size: 173 Color: 7
Size: 146 Color: 17

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 13
Size: 158 Color: 15
Size: 136 Color: 9

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 716 Color: 10
Size: 148 Color: 15
Size: 137 Color: 11

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 8
Size: 285 Color: 9

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 10
Size: 280 Color: 8

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 17
Size: 177 Color: 2
Size: 142 Color: 14

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 477 Color: 12

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 13
Size: 412 Color: 16

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 18
Size: 112 Color: 17
Size: 110 Color: 17

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 2
Size: 412 Color: 11
Size: 112 Color: 5

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 625 Color: 5
Size: 196 Color: 12
Size: 180 Color: 1

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 17
Size: 253 Color: 19

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 149 Color: 14
Size: 104 Color: 12

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 3
Size: 280 Color: 8
Size: 170 Color: 14

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 8
Size: 377 Color: 17

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 6
Size: 243 Color: 9

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 6
Size: 243 Color: 8
Size: 134 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 778 Color: 15
Size: 120 Color: 10
Size: 103 Color: 7

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 16
Size: 359 Color: 2

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 7
Size: 359 Color: 8

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 7
Size: 182 Color: 3
Size: 177 Color: 4

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 10
Size: 413 Color: 7

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 8
Size: 348 Color: 7

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 8
Size: 175 Color: 4
Size: 173 Color: 4

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 2
Size: 298 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 8
Size: 298 Color: 9
Size: 239 Color: 16

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 10
Size: 391 Color: 5

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 18
Size: 475 Color: 13

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 482 Color: 8

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 269 Color: 8
Size: 257 Color: 15

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 8
Size: 281 Color: 6

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 18
Size: 422 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 16
Size: 422 Color: 6
Size: 141 Color: 5

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 14
Size: 183 Color: 19
Size: 164 Color: 8

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 10
Size: 474 Color: 12

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 18

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 4

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 13
Size: 381 Color: 15

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 13
Size: 323 Color: 12

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 17
Size: 269 Color: 2
Size: 245 Color: 1

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 0
Size: 323 Color: 11

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9
Size: 323 Color: 19
Size: 187 Color: 19

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 323 Color: 10
Size: 187 Color: 10

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 5
Size: 245 Color: 8
Size: 143 Color: 16

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 18
Size: 379 Color: 15
Size: 187 Color: 11

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 16
Size: 422 Color: 10
Size: 141 Color: 8

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 8
Size: 141 Color: 3
Size: 131 Color: 17

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 0
Size: 478 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 4
Size: 137 Color: 19
Size: 125 Color: 7

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 11
Size: 124 Color: 19
Size: 104 Color: 8

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 4
Size: 143 Color: 5
Size: 125 Color: 14

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 3
Size: 226 Color: 11

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 7
Size: 226 Color: 14

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 18
Size: 373 Color: 7

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 4
Size: 372 Color: 12

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 2
Size: 188 Color: 14
Size: 185 Color: 3

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 11
Size: 322 Color: 16

Bin 123: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 8
Size: 203 Color: 5

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 17
Size: 203 Color: 17
Size: 155 Color: 9

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 775 Color: 15
Size: 113 Color: 6
Size: 113 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 10
Size: 127 Color: 1
Size: 115 Color: 4

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 11

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 12
Size: 369 Color: 19

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 7
Size: 188 Color: 4
Size: 182 Color: 15

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 12
Size: 178 Color: 12
Size: 170 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 16
Size: 177 Color: 14
Size: 170 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 8
Size: 275 Color: 19
Size: 142 Color: 8

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 8
Size: 121 Color: 12
Size: 101 Color: 7

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 4
Size: 147 Color: 16
Size: 121 Color: 11

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 13
Size: 319 Color: 12

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 19
Size: 362 Color: 12

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 0
Size: 183 Color: 4
Size: 179 Color: 13

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 16
Size: 390 Color: 5

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 15
Size: 489 Color: 7

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 234 Color: 5

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 2

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 12
Size: 465 Color: 14

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 12
Size: 466 Color: 4

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 11
Size: 149 Color: 5
Size: 146 Color: 7

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 5
Size: 370 Color: 15

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 18
Size: 194 Color: 0
Size: 176 Color: 10

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 10
Size: 127 Color: 6
Size: 115 Color: 15

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 10
Size: 128 Color: 2
Size: 102 Color: 18

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 9
Size: 151 Color: 11
Size: 142 Color: 3

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 5
Size: 451 Color: 16

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 8
Size: 362 Color: 1

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 18

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 2
Size: 168 Color: 8
Size: 165 Color: 12

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 4
Size: 174 Color: 10
Size: 168 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 11
Size: 274 Color: 4
Size: 245 Color: 10

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 353 Color: 12
Size: 192 Color: 6

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 13
Size: 217 Color: 0

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 15
Size: 217 Color: 17

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 18
Size: 217 Color: 13

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 15
Size: 286 Color: 12

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 5
Size: 286 Color: 15

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 11
Size: 155 Color: 12
Size: 131 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 7
Size: 163 Color: 8
Size: 155 Color: 10

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 3
Size: 168 Color: 5
Size: 165 Color: 18

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 594 Color: 4
Size: 211 Color: 1
Size: 196 Color: 11

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 18
Size: 188 Color: 3
Size: 182 Color: 14

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 13
Size: 395 Color: 6

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 19
Size: 331 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 16
Size: 331 Color: 0
Size: 275 Color: 13

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 17
Size: 263 Color: 9

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9
Size: 313 Color: 15
Size: 209 Color: 13

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 0
Size: 135 Color: 15
Size: 128 Color: 16

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 15
Size: 118 Color: 4
Size: 103 Color: 17

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 18
Size: 151 Color: 1
Size: 137 Color: 16

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 14

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 5

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 17
Size: 263 Color: 8

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 14
Size: 305 Color: 8
Size: 275 Color: 7

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 13
Size: 230 Color: 15

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 12
Size: 412 Color: 5

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 5
Size: 133 Color: 17
Size: 129 Color: 6

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 10
Size: 135 Color: 17
Size: 132 Color: 9

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 421 Color: 17

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 19
Size: 275 Color: 8

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 6
Size: 140 Color: 17
Size: 122 Color: 6

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 18
Size: 412 Color: 14
Size: 106 Color: 16

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 10
Size: 153 Color: 12
Size: 140 Color: 10

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 11
Size: 135 Color: 10
Size: 120 Color: 9

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 14
Size: 135 Color: 1
Size: 132 Color: 17

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 19
Size: 128 Color: 5
Size: 102 Color: 10

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 6
Size: 243 Color: 9
Size: 135 Color: 19

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 15
Size: 412 Color: 4
Size: 132 Color: 8

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 7
Size: 312 Color: 1
Size: 139 Color: 6

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 14
Size: 208 Color: 12
Size: 181 Color: 11

Bin 195: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 15
Size: 208 Color: 14

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 13
Size: 208 Color: 1

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 19
Size: 333 Color: 4

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 19
Size: 169 Color: 11
Size: 164 Color: 3

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 16
Size: 291 Color: 11
Size: 152 Color: 4

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 17
Size: 167 Color: 6
Size: 155 Color: 3

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 11
Size: 394 Color: 1

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 389 Color: 5

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 6
Size: 289 Color: 2
Size: 100 Color: 5

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 2
Size: 214 Color: 6
Size: 175 Color: 18

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 680 Color: 13
Size: 164 Color: 0
Size: 157 Color: 4

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 9
Size: 324 Color: 18

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 324 Color: 1
Size: 185 Color: 17

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 17
Size: 149 Color: 0
Size: 146 Color: 17

Bin 209: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 4
Size: 288 Color: 16

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 14
Size: 385 Color: 10
Size: 146 Color: 12

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 559 Color: 10
Size: 312 Color: 9
Size: 130 Color: 0

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 4
Size: 374 Color: 5

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 627 Color: 11
Size: 199 Color: 11
Size: 175 Color: 4

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 223 Color: 6

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 19
Size: 223 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 598 Color: 13
Size: 209 Color: 0
Size: 194 Color: 15

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 13
Size: 285 Color: 1

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 8
Size: 285 Color: 6

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 8
Size: 150 Color: 4
Size: 138 Color: 6

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 12
Size: 313 Color: 17

Bin 221: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 10
Size: 314 Color: 2

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 7
Size: 314 Color: 18

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 3
Size: 322 Color: 15

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 19
Size: 161 Color: 11
Size: 161 Color: 3

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 6
Size: 161 Color: 15
Size: 161 Color: 14

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 9
Size: 332 Color: 3

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 5
Size: 332 Color: 19

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 3
Size: 332 Color: 13

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 7
Size: 332 Color: 9
Size: 172 Color: 3

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 14
Size: 414 Color: 18
Size: 151 Color: 6

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 5
Size: 232 Color: 19

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 281 Color: 11

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 3
Size: 116 Color: 18
Size: 106 Color: 0

Bin 234: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 11
Size: 257 Color: 14

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 6
Size: 291 Color: 19
Size: 235 Color: 6

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 12
Size: 319 Color: 7

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 19
Size: 128 Color: 2
Size: 102 Color: 11

Bin 238: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 388 Color: 2

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 223 Color: 18

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 3
Size: 470 Color: 17

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 14
Size: 414 Color: 9

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 18
Size: 414 Color: 19
Size: 134 Color: 8

Bin 243: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 1
Size: 226 Color: 13

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 2
Size: 328 Color: 11

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 7
Size: 328 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 673 Color: 16
Size: 173 Color: 17
Size: 155 Color: 15

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 239 Color: 5

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 6
Size: 300 Color: 12

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 2
Size: 388 Color: 1

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 9
Size: 300 Color: 18

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 19
Size: 438 Color: 6

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 8
Size: 420 Color: 6

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 13
Size: 422 Color: 2

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 7
Size: 422 Color: 7
Size: 141 Color: 3

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 300 Color: 17
Size: 279 Color: 8

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 8
Size: 403 Color: 11

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 5
Size: 194 Color: 18
Size: 166 Color: 3

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 4
Size: 311 Color: 11

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 18
Size: 475 Color: 16

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 18
Size: 446 Color: 17

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 7
Size: 257 Color: 1
Size: 189 Color: 11

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 4
Size: 487 Color: 1

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 18
Size: 487 Color: 12

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 594 Color: 15
Size: 275 Color: 5
Size: 132 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 583 Color: 9
Size: 275 Color: 12
Size: 143 Color: 7

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 413 Color: 16
Size: 124 Color: 18

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 17
Size: 413 Color: 3

Bin 268: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 8
Size: 307 Color: 4

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 19
Size: 391 Color: 10
Size: 197 Color: 5

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 2
Size: 133 Color: 10
Size: 115 Color: 10

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 2
Size: 139 Color: 19
Size: 110 Color: 13

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 13
Size: 126 Color: 15
Size: 116 Color: 15

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 419 Color: 14

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 8
Size: 235 Color: 3
Size: 184 Color: 3

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 2
Size: 291 Color: 3
Size: 152 Color: 19

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 19
Size: 329 Color: 18
Size: 184 Color: 11

Bin 277: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 7
Size: 363 Color: 5

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 18
Size: 363 Color: 5

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 363 Color: 10
Size: 154 Color: 4

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 12
Size: 347 Color: 19

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 9
Size: 345 Color: 17

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 6
Size: 420 Color: 16

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 10
Size: 326 Color: 19

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 18
Size: 394 Color: 17

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 10
Size: 324 Color: 9

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 3
Size: 324 Color: 19

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 19
Size: 170 Color: 7
Size: 156 Color: 6

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 5
Size: 163 Color: 17
Size: 159 Color: 13

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 10
Size: 278 Color: 3
Size: 173 Color: 4

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 7
Size: 390 Color: 7
Size: 159 Color: 16

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 14
Size: 202 Color: 9

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 6
Size: 202 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 18
Size: 308 Color: 16
Size: 208 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 16
Size: 316 Color: 16
Size: 200 Color: 2

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 1
Size: 426 Color: 16

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 17
Size: 426 Color: 12

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 625 Color: 7
Size: 190 Color: 17
Size: 186 Color: 1

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 0
Size: 214 Color: 11

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 12
Size: 304 Color: 11

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 9
Size: 304 Color: 11

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 305 Color: 11

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 13
Size: 304 Color: 8

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 13
Size: 304 Color: 6
Size: 199 Color: 2

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 304 Color: 3
Size: 214 Color: 14

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 12
Size: 375 Color: 5

Bin 306: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 358 Color: 5

Bin 307: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 7
Size: 358 Color: 11

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 8
Size: 436 Color: 2

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 3
Size: 229 Color: 13

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 15
Size: 229 Color: 0

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 256 Color: 3

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 14

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 8
Size: 134 Color: 16
Size: 122 Color: 3

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 3
Size: 455 Color: 2

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 455 Color: 0

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 19
Size: 382 Color: 12

Bin 317: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 8
Size: 382 Color: 12

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 4
Size: 195 Color: 17
Size: 188 Color: 3

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 14
Size: 111 Color: 5
Size: 110 Color: 6

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 10
Size: 414 Color: 13
Size: 134 Color: 4

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 399 Color: 8
Size: 116 Color: 16

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 13
Size: 396 Color: 11

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 19
Size: 430 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 14
Size: 396 Color: 10
Size: 175 Color: 13

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8
Size: 399 Color: 15
Size: 172 Color: 3

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 5
Size: 385 Color: 15

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 8
Size: 116 Color: 6
Size: 106 Color: 3

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 2
Size: 291 Color: 1

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 17
Size: 358 Color: 15

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 2
Size: 285 Color: 13

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 9
Size: 347 Color: 6

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 9
Size: 388 Color: 11

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 8
Size: 210 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 14
Size: 210 Color: 12
Size: 178 Color: 7

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 17
Size: 210 Color: 9

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 9
Size: 224 Color: 13

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 6
Size: 271 Color: 17

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 730 Color: 8
Size: 148 Color: 9
Size: 123 Color: 10

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 3
Size: 308 Color: 7
Size: 198 Color: 10

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 799 Color: 10
Size: 102 Color: 19
Size: 100 Color: 13

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 3
Size: 289 Color: 8

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 8
Size: 207 Color: 15

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 6
Size: 218 Color: 15

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 274 Color: 14
Size: 257 Color: 8

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 19
Size: 126 Color: 7
Size: 116 Color: 17

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 14

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 7
Size: 201 Color: 16

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 18
Size: 315 Color: 2

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 17
Size: 207 Color: 0

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 10
Size: 486 Color: 17

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 9
Size: 486 Color: 6

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 19
Size: 349 Color: 9
Size: 193 Color: 14

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 19

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 18
Size: 286 Color: 8

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 10
Size: 211 Color: 18

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 5
Size: 281 Color: 8

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 19
Size: 209 Color: 16

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 11
Size: 493 Color: 9

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 13
Size: 493 Color: 0

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 16
Size: 274 Color: 5

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 408 Color: 4

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 2
Size: 223 Color: 15

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 15
Size: 447 Color: 9

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 10
Size: 301 Color: 19

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 13
Size: 227 Color: 14

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 11
Size: 249 Color: 3

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 318 Color: 2

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 16
Size: 211 Color: 10

Bin 369: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 13
Size: 243 Color: 9

Bin 370: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 317 Color: 12

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 11
Size: 277 Color: 2

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 7
Size: 496 Color: 18

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 9

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 12

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 4
Size: 422 Color: 8

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 7
Size: 311 Color: 19

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 16
Size: 286 Color: 19

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 17
Size: 277 Color: 16

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 15
Size: 303 Color: 16

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 337 Color: 7

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 13
Size: 479 Color: 19

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 12
Size: 272 Color: 0

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 16
Size: 258 Color: 15

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 16
Size: 274 Color: 10

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 18
Size: 496 Color: 17

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 18
Size: 430 Color: 19

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 15

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 6
Size: 340 Color: 18

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 6
Size: 276 Color: 1

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 15
Size: 276 Color: 11

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 7
Size: 401 Color: 5

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 13
Size: 370 Color: 17

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 10
Size: 283 Color: 13

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 477 Color: 9

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 9
Size: 282 Color: 19

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 267 Color: 0

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 12
Size: 352 Color: 18

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 10
Size: 233 Color: 3

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 366 Color: 14

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 5
Size: 496 Color: 13

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 17
Size: 367 Color: 2

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 9
Size: 287 Color: 17

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 17

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 242 Color: 18

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 9
Size: 437 Color: 14

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 437 Color: 7

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 18
Size: 235 Color: 0

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 8

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 6
Size: 235 Color: 17

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 211 Color: 7

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 18
Size: 367 Color: 7

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 13
Size: 351 Color: 0

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 13
Size: 210 Color: 6

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 368 Color: 17

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 5
Size: 274 Color: 16

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 11
Size: 411 Color: 13

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 411 Color: 3

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 14
Size: 339 Color: 4

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 10
Size: 490 Color: 8

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 1
Size: 490 Color: 19

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 308 Color: 18

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 351 Color: 11

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 4
Size: 216 Color: 17

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 14
Size: 277 Color: 17

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 15
Size: 454 Color: 2

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 13
Size: 281 Color: 12

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 304 Color: 8

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 8
Size: 225 Color: 13

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 18
Size: 270 Color: 11

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 13
Size: 479 Color: 1

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 5
Size: 249 Color: 3

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 3
Size: 292 Color: 9

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 6
Size: 467 Color: 9

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 18
Size: 233 Color: 13

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 379 Color: 13

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 1
Size: 416 Color: 7

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 18
Size: 397 Color: 8

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 10
Size: 467 Color: 11

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 11
Size: 259 Color: 19

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 0

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 14
Size: 494 Color: 15

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 18
Size: 460 Color: 17

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 366 Color: 1

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9
Size: 349 Color: 12
Size: 193 Color: 0

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 7
Size: 425 Color: 2

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 16
Size: 473 Color: 0

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 9
Size: 473 Color: 2

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 19
Size: 490 Color: 18

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 13
Size: 486 Color: 19

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 12
Size: 292 Color: 10

Bin 451: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 19
Size: 315 Color: 0

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 3
Size: 292 Color: 1

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 4

Bin 454: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 13
Size: 445 Color: 6

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 7
Size: 421 Color: 15

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 18
Size: 282 Color: 8

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 15
Size: 352 Color: 12

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 15
Size: 378 Color: 18

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 1
Size: 236 Color: 16

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 12
Size: 325 Color: 18

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 18
Size: 400 Color: 4

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 15
Size: 400 Color: 7

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 2
Size: 427 Color: 15

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 11
Size: 454 Color: 17

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 11
Size: 267 Color: 3

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 18
Size: 352 Color: 10

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 12
Size: 489 Color: 4

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 13
Size: 264 Color: 12

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 18
Size: 300 Color: 11

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 6
Size: 227 Color: 3

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 18
Size: 298 Color: 10

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 14
Size: 292 Color: 3

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 6
Size: 366 Color: 12

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 3
Size: 135 Color: 11
Size: 132 Color: 1

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 5
Size: 272 Color: 18

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 15
Size: 258 Color: 4

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 19
Size: 306 Color: 0

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 2
Size: 320 Color: 11

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 17
Size: 283 Color: 3

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 18
Size: 329 Color: 10

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 18
Size: 431 Color: 3

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 11
Size: 492 Color: 4

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 18
Size: 492 Color: 6

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 431 Color: 1

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 19
Size: 408 Color: 10

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 350 Color: 19

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 9
Size: 446 Color: 11

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 9
Size: 446 Color: 17

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 19

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 19
Size: 354 Color: 8

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 2
Size: 249 Color: 18

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 1
Size: 443 Color: 12

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 11

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 469 Color: 11

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 12
Size: 340 Color: 11

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 0
Size: 408 Color: 2

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 18
Size: 408 Color: 12

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 401 Color: 7

Bin 499: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 13
Size: 466 Color: 6

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 6
Size: 402 Color: 5

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 11
Size: 402 Color: 15

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 7
Size: 315 Color: 10

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 14
Size: 266 Color: 4

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 13

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 297 Color: 10

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 8
Size: 297 Color: 7

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 10
Size: 497 Color: 13

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 5
Size: 325 Color: 19

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 9
Size: 303 Color: 14

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 7
Size: 453 Color: 3

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 14
Size: 258 Color: 15

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 11
Size: 468 Color: 8

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 15
Size: 443 Color: 9

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 8
Size: 336 Color: 7

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 2
Size: 225 Color: 7

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 6
Size: 352 Color: 14

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 6
Size: 309 Color: 5

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 491 Color: 11

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 12
Size: 400 Color: 9

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 9
Size: 414 Color: 19

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 16
Size: 353 Color: 0

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 6
Size: 331 Color: 0

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 8
Size: 278 Color: 6

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 19
Size: 420 Color: 13

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 2
Size: 315 Color: 18

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 19
Size: 246 Color: 0

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 11
Size: 246 Color: 18

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 9
Size: 260 Color: 4

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 8
Size: 271 Color: 9

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 397 Color: 0

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 3
Size: 318 Color: 15

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 401 Color: 4

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 14
Size: 228 Color: 11

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 19
Size: 238 Color: 4

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 12
Size: 380 Color: 1

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 14
Size: 380 Color: 8

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 11
Size: 219 Color: 18

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 14
Size: 361 Color: 10

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 8
Size: 384 Color: 4

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 19
Size: 357 Color: 14

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 5
Size: 325 Color: 0

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 5
Size: 449 Color: 15

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 390 Color: 17

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 6
Size: 303 Color: 0

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 11
Size: 225 Color: 4

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 470 Color: 3

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 10
Size: 444 Color: 15

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 19
Size: 444 Color: 14

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 15
Size: 228 Color: 9

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 14
Size: 384 Color: 19

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 6

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 4
Size: 234 Color: 14

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 19
Size: 282 Color: 6

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 19
Size: 399 Color: 15

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 15
Size: 483 Color: 16

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 18
Size: 434 Color: 7

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 215 Color: 11

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 12
Size: 372 Color: 3

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 4
Size: 400 Color: 12

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 18

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 9
Size: 351 Color: 14

Bin 562: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 14
Size: 496 Color: 8

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 462 Color: 17

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 2
Size: 474 Color: 8

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 366 Color: 8

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 13
Size: 320 Color: 1

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 8
Size: 290 Color: 2

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 7
Size: 350 Color: 2

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 11
Size: 465 Color: 1

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 12
Size: 210 Color: 5

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 19
Size: 461 Color: 6

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 8
Size: 461 Color: 14

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 17
Size: 350 Color: 0

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 16
Size: 301 Color: 4

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 10
Size: 344 Color: 17

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 16
Size: 296 Color: 2

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 15
Size: 457 Color: 19

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 6
Size: 467 Color: 4

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 11

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 8
Size: 264 Color: 13

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 16
Size: 403 Color: 11

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 19
Size: 292 Color: 13

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 13
Size: 367 Color: 18

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 9
Size: 458 Color: 11

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 14

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 19
Size: 357 Color: 7

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 14
Size: 460 Color: 12

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 1
Size: 316 Color: 18

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 18
Size: 327 Color: 19

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 16
Size: 227 Color: 17

Bin 591: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 10
Size: 329 Color: 0

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 18
Size: 340 Color: 17

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 3

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 410 Color: 14

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 15
Size: 251 Color: 4

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 12
Size: 410 Color: 1

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 13
Size: 251 Color: 1

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 18
Size: 284 Color: 8

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 16
Size: 248 Color: 17

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 3
Size: 276 Color: 2

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 13
Size: 255 Color: 17

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 8
Size: 472 Color: 0

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 18
Size: 336 Color: 12

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 13
Size: 392 Color: 12

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 17
Size: 458 Color: 9

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 18
Size: 303 Color: 12

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 2
Size: 260 Color: 7

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 11
Size: 403 Color: 7

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 0
Size: 476 Color: 5

Bin 610: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 8

Bin 611: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 10
Size: 284 Color: 5

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 8
Size: 409 Color: 2

Bin 613: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 8
Size: 449 Color: 10

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 18
Size: 409 Color: 12

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 10
Size: 277 Color: 0
Size: 239 Color: 6

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 0
Size: 452 Color: 8

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 19
Size: 384 Color: 18

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 13
Size: 365 Color: 15

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 8
Size: 365 Color: 6

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 6
Size: 264 Color: 8

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 15
Size: 409 Color: 1

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 12
Size: 409 Color: 13

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 15
Size: 381 Color: 18

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 19
Size: 133 Color: 11
Size: 129 Color: 13

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 17
Size: 357 Color: 6

Bin 626: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 0
Size: 316 Color: 10

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 2
Size: 270 Color: 18

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 5

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 6
Size: 310 Color: 8

Bin 630: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 6

Bin 631: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 8
Size: 415 Color: 13

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 8
Size: 266 Color: 17

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 354 Color: 14

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 11
Size: 337 Color: 15

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 6
Size: 386 Color: 17

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 7
Size: 386 Color: 0

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 13
Size: 500 Color: 4

Bin 638: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 8
Size: 500 Color: 18

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 7
Size: 457 Color: 1

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 234 Color: 6

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 9
Size: 421 Color: 8

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 16
Size: 249 Color: 10

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 7
Size: 256 Color: 8

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 9
Size: 256 Color: 3

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 10
Size: 416 Color: 0

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 251 Color: 12

Bin 647: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 5
Size: 467 Color: 3

Bin 648: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 12
Size: 343 Color: 3

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 5

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 18
Size: 304 Color: 14

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 8
Size: 157 Color: 15
Size: 146 Color: 14

Bin 652: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 10

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 16
Size: 261 Color: 2

Bin 654: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 9
Size: 408 Color: 7

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 13
Size: 408 Color: 8

Bin 656: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 17
Size: 392 Color: 4

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 9
Size: 297 Color: 11

Bin 658: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 11
Size: 237 Color: 3

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 18
Size: 406 Color: 2

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 17
Size: 208 Color: 7

Bin 661: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 5
Size: 463 Color: 8

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 16
Size: 419 Color: 19

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 2
Size: 351 Color: 0

Bin 664: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 8
Size: 234 Color: 12

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 8
Size: 316 Color: 5

Bin 666: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 7
Size: 316 Color: 5

Bin 667: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 3
Size: 441 Color: 10

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 9
Size: 350 Color: 8

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 5
Size: 338 Color: 8

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 18
Size: 495 Color: 8

Bin 671: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 19
Size: 302 Color: 4

Bin 672: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 12
Size: 499 Color: 6

Bin 673: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 17
Size: 495 Color: 2

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 18
Size: 488 Color: 5

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 12
Size: 480 Color: 11

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 6
Size: 478 Color: 2

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 11
Size: 472 Color: 9

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 16
Size: 472 Color: 17

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 12
Size: 471 Color: 9

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 19
Size: 465 Color: 5

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 17
Size: 463 Color: 3

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 456 Color: 18

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 9
Size: 452 Color: 8

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 3
Size: 450 Color: 1

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 19
Size: 449 Color: 6

Bin 686: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 449 Color: 3

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 448 Color: 15

Bin 688: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 18
Size: 447 Color: 12

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 12
Size: 441 Color: 3

Bin 690: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 19
Size: 437 Color: 7

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 11
Size: 436 Color: 16

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 18
Size: 435 Color: 7

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 15
Size: 434 Color: 6

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 14
Size: 431 Color: 6

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 6
Size: 428 Color: 16

Bin 696: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 7
Size: 429 Color: 5

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 5
Size: 429 Color: 10

Bin 698: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 13
Size: 411 Color: 1

Bin 699: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 406 Color: 19

Bin 700: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 19
Size: 405 Color: 14

Bin 701: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 17
Size: 405 Color: 7

Bin 702: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 7
Size: 404 Color: 15

Bin 703: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 8
Size: 407 Color: 19

Bin 704: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 11
Size: 402 Color: 4

Bin 705: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 5
Size: 401 Color: 4

Bin 706: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 18
Size: 398 Color: 9

Bin 707: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 17

Bin 708: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 14
Size: 397 Color: 13

Bin 709: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 8
Size: 398 Color: 12

Bin 710: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 11

Bin 711: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 18
Size: 393 Color: 10

Bin 712: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 16
Size: 392 Color: 19

Bin 713: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 8
Size: 393 Color: 3

Bin 714: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 387 Color: 8

Bin 715: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 6
Size: 382 Color: 8

Bin 716: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 1
Size: 380 Color: 0

Bin 717: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 8
Size: 381 Color: 15

Bin 718: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 1
Size: 374 Color: 12

Bin 719: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 16
Size: 371 Color: 10

Bin 720: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 0
Size: 364 Color: 3

Bin 721: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 2
Size: 360 Color: 5

Bin 722: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 0
Size: 356 Color: 4

Bin 723: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 356 Color: 0

Bin 724: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 353 Color: 1

Bin 725: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 19
Size: 346 Color: 8

Bin 726: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 19
Size: 343 Color: 17

Bin 727: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 4
Size: 342 Color: 5

Bin 728: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 6
Size: 341 Color: 10

Bin 729: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 7
Size: 341 Color: 11

Bin 730: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 15
Size: 340 Color: 1

Bin 731: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 9
Size: 339 Color: 16

Bin 732: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 5
Size: 338 Color: 15

Bin 733: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 11
Size: 334 Color: 13

Bin 734: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 6
Size: 327 Color: 7

Bin 735: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 17
Size: 309 Color: 5

Bin 736: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 10
Size: 301 Color: 12

Bin 737: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 16
Size: 299 Color: 7

Bin 738: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 11
Size: 297 Color: 18

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 19
Size: 151 Color: 18
Size: 136 Color: 18

Bin 740: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 15
Size: 282 Color: 6

Bin 741: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 267 Color: 15

Bin 742: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 19
Size: 265 Color: 12

Bin 743: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 16
Size: 261 Color: 17

Bin 744: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 10

Bin 745: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 16

Bin 746: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 5
Size: 255 Color: 10

Bin 747: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 11
Size: 254 Color: 16

Bin 748: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 1
Size: 253 Color: 10

Bin 749: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 252 Color: 9

Bin 750: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 252 Color: 15

Bin 751: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 15
Size: 244 Color: 14

Bin 752: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 5
Size: 244 Color: 17

Bin 753: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 12
Size: 244 Color: 3

Bin 754: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 10
Size: 241 Color: 13

Bin 755: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 17
Size: 241 Color: 12

Bin 756: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 10
Size: 240 Color: 3

Bin 757: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 14
Size: 240 Color: 15

Bin 758: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 9
Size: 236 Color: 13

Bin 759: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 5
Size: 231 Color: 19

Bin 760: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 6
Size: 228 Color: 4

Bin 761: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 15
Size: 227 Color: 13

Bin 762: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 6
Size: 225 Color: 11

Bin 763: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 10

Bin 764: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 19
Size: 220 Color: 7

Bin 765: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 5
Size: 220 Color: 10

Bin 766: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 6
Size: 213 Color: 12

Bin 767: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 16
Size: 207 Color: 3

Bin 768: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 9
Size: 206 Color: 5

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 795 Color: 2
Size: 103 Color: 13
Size: 103 Color: 11

Bin 770: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 12
Size: 200 Color: 16

Bin 771: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 7

Bin 772: 1 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 311 Color: 0
Size: 190 Color: 2

Bin 773: 1 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 8
Size: 279 Color: 10
Size: 171 Color: 15

Bin 774: 1 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 7
Size: 178 Color: 8
Size: 140 Color: 6

Bin 775: 1 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 14
Size: 498 Color: 8

Bin 776: 1 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 15
Size: 498 Color: 18

Bin 777: 1 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 372 Color: 6

Bin 778: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 203 Color: 15

Bin 779: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 203 Color: 13

Bin 780: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 14
Size: 203 Color: 2

Bin 781: 1 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 18
Size: 368 Color: 13

Bin 782: 1 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 19
Size: 369 Color: 18

Bin 783: 1 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 13
Size: 465 Color: 19

Bin 784: 1 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 7
Size: 465 Color: 2

Bin 785: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 6
Size: 488 Color: 9

Bin 786: 1 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 246 Color: 2

Bin 787: 1 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 18
Size: 317 Color: 4

Bin 788: 1 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 2
Size: 211 Color: 6

Bin 789: 1 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 9
Size: 220 Color: 19

Bin 790: 1 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 3
Size: 320 Color: 5

Bin 791: 1 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 19
Size: 353 Color: 0

Bin 792: 1 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 7
Size: 470 Color: 1

Bin 793: 1 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 14
Size: 314 Color: 13

Bin 794: 1 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 12
Size: 313 Color: 9

Bin 795: 1 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 12
Size: 332 Color: 2

Bin 796: 1 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 3
Size: 320 Color: 15

Bin 797: 1 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 2
Size: 439 Color: 8

Bin 798: 1 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 14
Size: 329 Color: 8

Bin 799: 1 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 329 Color: 1

Bin 800: 1 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 363 Color: 15

Bin 801: 1 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 13
Size: 201 Color: 4

Bin 802: 1 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 15
Size: 200 Color: 5

Bin 803: 1 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 3
Size: 308 Color: 6

Bin 804: 1 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 18
Size: 218 Color: 0

Bin 805: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 453 Color: 2

Bin 806: 1 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 382 Color: 0

Bin 807: 1 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 8
Size: 343 Color: 10

Bin 808: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 6
Size: 289 Color: 16

Bin 809: 1 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 15
Size: 336 Color: 5

Bin 810: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 11
Size: 289 Color: 13

Bin 811: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 16
Size: 289 Color: 2

Bin 812: 1 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 9
Size: 339 Color: 11

Bin 813: 1 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 17
Size: 211 Color: 19

Bin 814: 1 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 4
Size: 270 Color: 5

Bin 815: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 11
Size: 414 Color: 3

Bin 816: 1 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 7
Size: 283 Color: 13

Bin 817: 1 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 0
Size: 291 Color: 14

Bin 818: 1 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 8
Size: 214 Color: 18

Bin 819: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 9
Size: 264 Color: 3

Bin 820: 1 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 339 Color: 17

Bin 821: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 9
Size: 447 Color: 7

Bin 822: 1 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 0
Size: 218 Color: 11

Bin 823: 1 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 14
Size: 457 Color: 4

Bin 824: 1 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 7
Size: 493 Color: 8

Bin 825: 1 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 8
Size: 366 Color: 7

Bin 826: 1 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 16
Size: 327 Color: 11

Bin 827: 1 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 5
Size: 236 Color: 2

Bin 828: 1 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 8
Size: 283 Color: 10

Bin 829: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 259 Color: 19

Bin 830: 1 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 4
Size: 424 Color: 8

Bin 831: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 6
Size: 427 Color: 18

Bin 832: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 9
Size: 427 Color: 5

Bin 833: 1 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 10
Size: 317 Color: 19

Bin 834: 1 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 17
Size: 218 Color: 12

Bin 835: 1 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 15
Size: 416 Color: 3

Bin 836: 1 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 6
Size: 432 Color: 17

Bin 837: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 16
Size: 488 Color: 5

Bin 838: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 18
Size: 467 Color: 13

Bin 839: 1 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 493 Color: 7

Bin 840: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 447 Color: 14

Bin 841: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 10
Size: 259 Color: 18

Bin 842: 1 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 329 Color: 5

Bin 843: 1 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 12
Size: 465 Color: 5

Bin 844: 1 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 9
Size: 296 Color: 1

Bin 845: 1 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 3
Size: 283 Color: 5

Bin 846: 1 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 296 Color: 18

Bin 847: 1 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 14
Size: 470 Color: 10

Bin 848: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 10
Size: 264 Color: 18

Bin 849: 1 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 14
Size: 461 Color: 4

Bin 850: 1 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 460 Color: 7

Bin 851: 1 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 236 Color: 15

Bin 852: 1 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 13
Size: 441 Color: 0

Bin 853: 1 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 1
Size: 428 Color: 16

Bin 854: 1 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 15
Size: 416 Color: 5

Bin 855: 1 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 10
Size: 416 Color: 4

Bin 856: 2 of cap free
Amount of items: 3
Items: 
Size: 767 Color: 14
Size: 131 Color: 1
Size: 101 Color: 16

Bin 857: 2 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 3
Size: 119 Color: 16
Size: 109 Color: 8

Bin 858: 2 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 14
Size: 423 Color: 10

Bin 859: 2 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 15
Size: 250 Color: 13

Bin 860: 2 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 19
Size: 379 Color: 17

Bin 861: 2 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 438 Color: 17

Bin 862: 2 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 8
Size: 245 Color: 17

Bin 863: 2 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 10
Size: 211 Color: 15

Bin 864: 2 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 5
Size: 353 Color: 7

Bin 865: 2 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 7
Size: 438 Color: 10

Bin 866: 2 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 7
Size: 345 Color: 13

Bin 867: 2 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 11
Size: 324 Color: 8

Bin 868: 2 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 214 Color: 17

Bin 869: 2 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 9
Size: 214 Color: 4

Bin 870: 2 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 2
Size: 453 Color: 7

Bin 871: 2 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 15
Size: 396 Color: 9

Bin 872: 2 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 385 Color: 2

Bin 873: 2 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 224 Color: 19

Bin 874: 2 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 12
Size: 199 Color: 9

Bin 875: 2 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 16
Size: 409 Color: 18

Bin 876: 2 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 6
Size: 479 Color: 16

Bin 877: 2 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 11
Size: 460 Color: 1

Bin 878: 2 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 19
Size: 432 Color: 13

Bin 879: 2 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 7
Size: 432 Color: 15

Bin 880: 2 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 5
Size: 379 Color: 3

Bin 881: 2 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 404 Color: 1

Bin 882: 2 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 7
Size: 385 Color: 1

Bin 883: 3 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 17
Size: 199 Color: 4

Bin 884: 3 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 19
Size: 391 Color: 17

Bin 885: 3 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 2
Size: 423 Color: 14

Bin 886: 3 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 14
Size: 404 Color: 3

Bin 887: 4 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 12
Size: 399 Color: 1

Bin 888: 4 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 11
Size: 399 Color: 9

Bin 889: 5 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 16
Size: 239 Color: 5
Size: 126 Color: 3

Bin 890: 5 of cap free
Amount of items: 2
Items: 
Size: 498 Color: 19
Size: 498 Color: 15

Bin 891: 7 of cap free
Amount of items: 2
Items: 
Size: 498 Color: 2
Size: 496 Color: 14

Bin 892: 14 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 1
Size: 280 Color: 15

Bin 893: 16 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 13
Size: 192 Color: 12
Size: 180 Color: 0

Bin 894: 35 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 3
Size: 194 Color: 14

Bin 895: 45 of cap free
Amount of items: 4
Items: 
Size: 482 Color: 16
Size: 168 Color: 15
Size: 156 Color: 3
Size: 150 Color: 18

Bin 896: 45 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 6
Size: 313 Color: 14

Bin 897: 53 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9
Size: 269 Color: 1
Size: 192 Color: 11

Bin 898: 54 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 9
Size: 420 Color: 13

Bin 899: 62 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 5
Size: 412 Color: 8

Bin 900: 69 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 5
Size: 278 Color: 8

Bin 901: 151 of cap free
Amount of items: 7
Items: 
Size: 153 Color: 0
Size: 145 Color: 11
Size: 115 Color: 16
Size: 114 Color: 19
Size: 110 Color: 5
Size: 109 Color: 12
Size: 104 Color: 13

Total size: 901182
Total free space: 719

