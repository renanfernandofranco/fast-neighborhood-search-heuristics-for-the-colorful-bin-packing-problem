Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 320 Color: 2
Size: 268 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 263 Color: 1
Size: 257 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 1
Size: 250 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 299 Color: 0
Size: 272 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 360 Color: 0
Size: 270 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 294 Color: 3
Size: 271 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 286 Color: 0
Size: 274 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 2
Size: 250 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 251 Color: 1
Size: 250 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 0
Size: 349 Color: 4
Size: 300 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 279 Color: 2
Size: 277 Color: 3
Size: 444 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 3
Size: 344 Color: 2
Size: 258 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 362 Color: 2
Size: 257 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 350 Color: 1
Size: 277 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 343 Color: 4
Size: 281 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 4
Size: 329 Color: 3
Size: 304 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 350 Color: 3
Size: 255 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 283 Color: 1
Size: 254 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 306 Color: 3
Size: 267 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 4
Size: 287 Color: 1
Size: 283 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 332 Color: 2
Size: 300 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 4
Size: 265 Color: 0
Size: 250 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 344 Color: 4
Size: 286 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 3
Size: 330 Color: 2
Size: 309 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 255 Color: 3
Size: 250 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 256 Color: 1
Size: 253 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 347 Color: 4
Size: 254 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 286 Color: 3
Size: 280 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 274 Color: 1
Size: 262 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 357 Color: 4
Size: 273 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 288 Color: 3
Size: 261 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 297 Color: 4
Size: 258 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 256 Color: 3
Size: 250 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 289 Color: 4
Size: 253 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 290 Color: 3
Size: 282 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 323 Color: 4
Size: 251 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 275 Color: 0
Size: 259 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 311 Color: 0
Size: 263 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 315 Color: 4
Size: 272 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 358 Color: 1
Size: 274 Color: 0

Total size: 40000
Total free space: 0

