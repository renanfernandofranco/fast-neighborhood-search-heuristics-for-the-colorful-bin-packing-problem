Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3336
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 427958 Color: 3
Size: 317601 Color: 1
Size: 254442 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 394402 Color: 1
Size: 351359 Color: 0
Size: 254240 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 459127 Color: 0
Size: 289860 Color: 2
Size: 251014 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429885 Color: 0
Size: 290695 Color: 3
Size: 279421 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 452895 Color: 2
Size: 282328 Color: 3
Size: 264778 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 367626 Color: 4
Size: 318416 Color: 0
Size: 313959 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 382213 Color: 2
Size: 345604 Color: 4
Size: 272184 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 356347 Color: 1
Size: 355127 Color: 0
Size: 288527 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 373630 Color: 2
Size: 328793 Color: 1
Size: 297578 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 440193 Color: 2
Size: 291451 Color: 1
Size: 268357 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 388071 Color: 0
Size: 351520 Color: 2
Size: 260410 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 422358 Color: 4
Size: 294903 Color: 0
Size: 282740 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 350763 Color: 3
Size: 349593 Color: 2
Size: 299645 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 456843 Color: 4
Size: 288340 Color: 1
Size: 254818 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 435207 Color: 0
Size: 298863 Color: 4
Size: 265931 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 401421 Color: 1
Size: 332933 Color: 3
Size: 265647 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 432066 Color: 1
Size: 309500 Color: 3
Size: 258435 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 390933 Color: 2
Size: 313324 Color: 3
Size: 295744 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 377779 Color: 1
Size: 361679 Color: 2
Size: 260543 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 491794 Color: 3
Size: 256203 Color: 1
Size: 252004 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 411469 Color: 4
Size: 317771 Color: 2
Size: 270761 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 387357 Color: 2
Size: 346342 Color: 3
Size: 266302 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 481421 Color: 0
Size: 261739 Color: 3
Size: 256841 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 385825 Color: 3
Size: 320336 Color: 2
Size: 293840 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 459678 Color: 4
Size: 286492 Color: 1
Size: 253831 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 462314 Color: 0
Size: 273563 Color: 3
Size: 264124 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 374700 Color: 1
Size: 322564 Color: 4
Size: 302737 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 442023 Color: 0
Size: 279932 Color: 2
Size: 278046 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 474051 Color: 3
Size: 269421 Color: 1
Size: 256529 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 342241 Color: 1
Size: 341077 Color: 0
Size: 316683 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 373634 Color: 1
Size: 352248 Color: 2
Size: 274119 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 358568 Color: 0
Size: 322923 Color: 1
Size: 318510 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 398260 Color: 0
Size: 335098 Color: 4
Size: 266643 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 428711 Color: 2
Size: 287675 Color: 2
Size: 283615 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 379974 Color: 0
Size: 355711 Color: 1
Size: 264316 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 393620 Color: 0
Size: 327398 Color: 2
Size: 278983 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 383197 Color: 2
Size: 347481 Color: 4
Size: 269323 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 446593 Color: 4
Size: 291010 Color: 3
Size: 262398 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 460426 Color: 1
Size: 279852 Color: 3
Size: 259723 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 495319 Color: 1
Size: 254577 Color: 4
Size: 250105 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 375085 Color: 4
Size: 323729 Color: 3
Size: 301187 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 386154 Color: 2
Size: 338400 Color: 0
Size: 275447 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 370506 Color: 1
Size: 356369 Color: 4
Size: 273126 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 406892 Color: 4
Size: 304690 Color: 2
Size: 288419 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 387938 Color: 3
Size: 336141 Color: 0
Size: 275922 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 495582 Color: 0
Size: 253529 Color: 2
Size: 250890 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 404056 Color: 1
Size: 323147 Color: 4
Size: 272798 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 405667 Color: 0
Size: 324203 Color: 4
Size: 270131 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 426518 Color: 2
Size: 290218 Color: 0
Size: 283265 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 492573 Color: 0
Size: 254282 Color: 1
Size: 253146 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 404428 Color: 4
Size: 326891 Color: 1
Size: 268682 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 488271 Color: 4
Size: 259422 Color: 1
Size: 252308 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 373972 Color: 0
Size: 325939 Color: 1
Size: 300090 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 397600 Color: 2
Size: 340405 Color: 1
Size: 261996 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 370961 Color: 0
Size: 335384 Color: 1
Size: 293656 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 414577 Color: 4
Size: 292890 Color: 2
Size: 292534 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 479115 Color: 3
Size: 268539 Color: 2
Size: 252347 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 468759 Color: 4
Size: 268052 Color: 1
Size: 263190 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 416913 Color: 1
Size: 316163 Color: 1
Size: 266925 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 378338 Color: 4
Size: 351330 Color: 3
Size: 270333 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 467527 Color: 1
Size: 268994 Color: 4
Size: 263480 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 471325 Color: 0
Size: 268110 Color: 3
Size: 260566 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 379161 Color: 4
Size: 333902 Color: 2
Size: 286938 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 383506 Color: 1
Size: 320922 Color: 1
Size: 295573 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 385576 Color: 3
Size: 358001 Color: 1
Size: 256424 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 439498 Color: 4
Size: 297770 Color: 3
Size: 262733 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 455703 Color: 3
Size: 281586 Color: 4
Size: 262712 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 476358 Color: 4
Size: 268885 Color: 0
Size: 254758 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 372180 Color: 3
Size: 351919 Color: 2
Size: 275902 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 439345 Color: 4
Size: 288413 Color: 2
Size: 272243 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 354455 Color: 2
Size: 338712 Color: 3
Size: 306834 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 381565 Color: 2
Size: 341177 Color: 3
Size: 277259 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 463892 Color: 0
Size: 271518 Color: 3
Size: 264591 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 409990 Color: 4
Size: 304274 Color: 1
Size: 285737 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 381851 Color: 4
Size: 318775 Color: 4
Size: 299375 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 378965 Color: 2
Size: 357033 Color: 0
Size: 264003 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 384376 Color: 3
Size: 334701 Color: 4
Size: 280924 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 445691 Color: 2
Size: 299374 Color: 4
Size: 254936 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 390124 Color: 3
Size: 314259 Color: 0
Size: 295618 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 386644 Color: 1
Size: 345186 Color: 4
Size: 268171 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 431976 Color: 4
Size: 292250 Color: 1
Size: 275775 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 417735 Color: 0
Size: 294813 Color: 1
Size: 287453 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 447615 Color: 3
Size: 298137 Color: 2
Size: 254249 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 372314 Color: 3
Size: 355463 Color: 4
Size: 272224 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 484280 Color: 4
Size: 259750 Color: 1
Size: 255971 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 396452 Color: 0
Size: 352473 Color: 2
Size: 251076 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 442909 Color: 0
Size: 307040 Color: 2
Size: 250052 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 389746 Color: 2
Size: 310909 Color: 0
Size: 299346 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 420261 Color: 0
Size: 329691 Color: 1
Size: 250049 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 366090 Color: 3
Size: 317071 Color: 2
Size: 316840 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 435038 Color: 0
Size: 300295 Color: 3
Size: 264668 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 368889 Color: 1
Size: 360384 Color: 1
Size: 270728 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 495560 Color: 4
Size: 254197 Color: 2
Size: 250244 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 390378 Color: 2
Size: 344677 Color: 0
Size: 264946 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 361831 Color: 1
Size: 328069 Color: 3
Size: 310101 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 379005 Color: 0
Size: 322035 Color: 4
Size: 298961 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 392117 Color: 4
Size: 354248 Color: 2
Size: 253636 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 463800 Color: 3
Size: 270373 Color: 1
Size: 265828 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 421655 Color: 1
Size: 316803 Color: 0
Size: 261543 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 381484 Color: 1
Size: 352985 Color: 0
Size: 265532 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 405072 Color: 4
Size: 342257 Color: 0
Size: 252672 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 463390 Color: 2
Size: 271708 Color: 2
Size: 264903 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 379050 Color: 0
Size: 363553 Color: 4
Size: 257398 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 421050 Color: 1
Size: 317117 Color: 4
Size: 261834 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 487703 Color: 2
Size: 262075 Color: 3
Size: 250223 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 486877 Color: 2
Size: 262659 Color: 3
Size: 250465 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 402735 Color: 4
Size: 320323 Color: 3
Size: 276943 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 375785 Color: 0
Size: 344569 Color: 1
Size: 279647 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 484805 Color: 1
Size: 260630 Color: 1
Size: 254566 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 403766 Color: 4
Size: 318063 Color: 2
Size: 278172 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 427179 Color: 0
Size: 295783 Color: 2
Size: 277039 Color: 3

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 437076 Color: 1
Size: 302552 Color: 3
Size: 260373 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 488855 Color: 4
Size: 260826 Color: 1
Size: 250320 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 363156 Color: 3
Size: 338579 Color: 2
Size: 298266 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 339822 Color: 2
Size: 330718 Color: 4
Size: 329461 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 441883 Color: 1
Size: 295004 Color: 1
Size: 263114 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 395119 Color: 4
Size: 351755 Color: 1
Size: 253127 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 470590 Color: 3
Size: 265594 Color: 0
Size: 263817 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 493944 Color: 4
Size: 255898 Color: 0
Size: 250159 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 402988 Color: 4
Size: 308445 Color: 4
Size: 288568 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 363398 Color: 0
Size: 357249 Color: 1
Size: 279354 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 487919 Color: 0
Size: 260910 Color: 2
Size: 251172 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 357062 Color: 1
Size: 324155 Color: 1
Size: 318784 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 395540 Color: 4
Size: 306565 Color: 1
Size: 297896 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 485306 Color: 2
Size: 264381 Color: 3
Size: 250314 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 479576 Color: 3
Size: 263327 Color: 1
Size: 257098 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 432324 Color: 0
Size: 297617 Color: 4
Size: 270060 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 434067 Color: 3
Size: 289889 Color: 4
Size: 276045 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 418345 Color: 3
Size: 330559 Color: 0
Size: 251097 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 403447 Color: 2
Size: 346267 Color: 0
Size: 250287 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 437545 Color: 1
Size: 307565 Color: 2
Size: 254891 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 470862 Color: 0
Size: 275000 Color: 3
Size: 254139 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 373419 Color: 0
Size: 328776 Color: 3
Size: 297806 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 466755 Color: 1
Size: 281584 Color: 3
Size: 251662 Color: 2

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 388529 Color: 3
Size: 344753 Color: 1
Size: 266719 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 466062 Color: 3
Size: 277284 Color: 2
Size: 256655 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 489361 Color: 2
Size: 260448 Color: 4
Size: 250192 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 473694 Color: 2
Size: 270160 Color: 2
Size: 256147 Color: 4

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 383022 Color: 0
Size: 316637 Color: 3
Size: 300342 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 371493 Color: 3
Size: 328648 Color: 2
Size: 299860 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 489375 Color: 4
Size: 255561 Color: 2
Size: 255065 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 372489 Color: 0
Size: 313857 Color: 4
Size: 313655 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 416106 Color: 0
Size: 317052 Color: 4
Size: 266843 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 370091 Color: 1
Size: 342339 Color: 0
Size: 287571 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 351405 Color: 0
Size: 344843 Color: 1
Size: 303753 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 430134 Color: 0
Size: 286360 Color: 2
Size: 283507 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 362167 Color: 4
Size: 331433 Color: 3
Size: 306401 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 423251 Color: 4
Size: 314918 Color: 3
Size: 261832 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 413571 Color: 3
Size: 314050 Color: 4
Size: 272380 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 486835 Color: 2
Size: 259097 Color: 1
Size: 254069 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 433318 Color: 1
Size: 298180 Color: 4
Size: 268503 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 366109 Color: 2
Size: 335658 Color: 4
Size: 298234 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 458817 Color: 1
Size: 282764 Color: 3
Size: 258420 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 383065 Color: 2
Size: 331811 Color: 3
Size: 285125 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 474296 Color: 1
Size: 271542 Color: 2
Size: 254163 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 442011 Color: 3
Size: 288883 Color: 2
Size: 269107 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 461896 Color: 4
Size: 282715 Color: 0
Size: 255390 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 396716 Color: 1
Size: 319619 Color: 4
Size: 283666 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 369208 Color: 2
Size: 334411 Color: 3
Size: 296382 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 415611 Color: 1
Size: 312198 Color: 2
Size: 272192 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 452391 Color: 0
Size: 284451 Color: 3
Size: 263159 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 408781 Color: 2
Size: 316529 Color: 0
Size: 274691 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 472831 Color: 0
Size: 270641 Color: 2
Size: 256529 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 490014 Color: 3
Size: 258451 Color: 4
Size: 251536 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 460552 Color: 1
Size: 274125 Color: 0
Size: 265324 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 479567 Color: 3
Size: 268578 Color: 4
Size: 251856 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 436459 Color: 3
Size: 310408 Color: 2
Size: 253134 Color: 2

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 360492 Color: 3
Size: 339848 Color: 0
Size: 299661 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 472194 Color: 4
Size: 276615 Color: 4
Size: 251192 Color: 2

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 373155 Color: 1
Size: 369413 Color: 4
Size: 257433 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 476346 Color: 3
Size: 263446 Color: 4
Size: 260209 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 499347 Color: 3
Size: 250404 Color: 1
Size: 250250 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 445379 Color: 0
Size: 297085 Color: 2
Size: 257537 Color: 3

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 437908 Color: 3
Size: 306401 Color: 4
Size: 255692 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 450828 Color: 2
Size: 276606 Color: 0
Size: 272567 Color: 3

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 454065 Color: 3
Size: 285812 Color: 0
Size: 260124 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 407744 Color: 0
Size: 320592 Color: 1
Size: 271665 Color: 4

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 2
Size: 310125 Color: 3
Size: 267679 Color: 4

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 447616 Color: 1
Size: 295681 Color: 4
Size: 256704 Color: 3

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 424631 Color: 2
Size: 296401 Color: 4
Size: 278969 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 387296 Color: 4
Size: 346906 Color: 0
Size: 265799 Color: 2

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 455430 Color: 0
Size: 277729 Color: 3
Size: 266842 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 411871 Color: 3
Size: 329961 Color: 2
Size: 258169 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 434654 Color: 4
Size: 304750 Color: 3
Size: 260597 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 448145 Color: 2
Size: 287660 Color: 0
Size: 264196 Color: 3

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 397614 Color: 1
Size: 335018 Color: 2
Size: 267369 Color: 3

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 487709 Color: 2
Size: 257454 Color: 4
Size: 254838 Color: 4

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 429129 Color: 2
Size: 293095 Color: 1
Size: 277777 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 370605 Color: 2
Size: 368842 Color: 0
Size: 260554 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 382151 Color: 0
Size: 346783 Color: 2
Size: 271067 Color: 4

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 379187 Color: 2
Size: 357190 Color: 3
Size: 263624 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 468145 Color: 0
Size: 280310 Color: 2
Size: 251546 Color: 3

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 480437 Color: 3
Size: 265970 Color: 4
Size: 253594 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 389181 Color: 2
Size: 321947 Color: 0
Size: 288873 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 410414 Color: 1
Size: 325091 Color: 3
Size: 264496 Color: 3

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 383049 Color: 1
Size: 318799 Color: 1
Size: 298153 Color: 2

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 429119 Color: 0
Size: 318471 Color: 3
Size: 252411 Color: 2

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 386494 Color: 0
Size: 337635 Color: 3
Size: 275872 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 374543 Color: 0
Size: 360269 Color: 4
Size: 265189 Color: 3

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 362094 Color: 2
Size: 347489 Color: 0
Size: 290418 Color: 3

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 367048 Color: 4
Size: 331203 Color: 0
Size: 301750 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 438603 Color: 1
Size: 298245 Color: 0
Size: 263153 Color: 3

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 365993 Color: 0
Size: 330068 Color: 2
Size: 303940 Color: 4

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 460657 Color: 0
Size: 282966 Color: 1
Size: 256378 Color: 4

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 437714 Color: 4
Size: 303360 Color: 1
Size: 258927 Color: 3

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 398464 Color: 2
Size: 325921 Color: 4
Size: 275616 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 401952 Color: 2
Size: 346919 Color: 1
Size: 251130 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 478686 Color: 3
Size: 261108 Color: 0
Size: 260207 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 476159 Color: 3
Size: 270967 Color: 0
Size: 252875 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 403876 Color: 1
Size: 343398 Color: 3
Size: 252727 Color: 4

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 437931 Color: 3
Size: 293019 Color: 0
Size: 269051 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 474737 Color: 3
Size: 263309 Color: 4
Size: 261955 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 447266 Color: 1
Size: 293022 Color: 4
Size: 259713 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 436440 Color: 3
Size: 313369 Color: 4
Size: 250192 Color: 3

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 402490 Color: 1
Size: 316747 Color: 3
Size: 280764 Color: 3

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 420591 Color: 2
Size: 290338 Color: 1
Size: 289072 Color: 4

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 403682 Color: 3
Size: 323343 Color: 2
Size: 272976 Color: 4

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 381231 Color: 0
Size: 359871 Color: 1
Size: 258899 Color: 3

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 435430 Color: 2
Size: 283305 Color: 4
Size: 281266 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 475928 Color: 3
Size: 268740 Color: 1
Size: 255333 Color: 4

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 416508 Color: 4
Size: 301718 Color: 2
Size: 281775 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 445730 Color: 3
Size: 285401 Color: 2
Size: 268870 Color: 4

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 403366 Color: 4
Size: 306793 Color: 1
Size: 289842 Color: 3

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 372165 Color: 0
Size: 348503 Color: 2
Size: 279333 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 460441 Color: 1
Size: 283147 Color: 3
Size: 256413 Color: 4

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 362284 Color: 1
Size: 325715 Color: 3
Size: 312002 Color: 4

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 454980 Color: 0
Size: 273056 Color: 1
Size: 271965 Color: 2

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 362384 Color: 1
Size: 319711 Color: 0
Size: 317906 Color: 2

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 447382 Color: 4
Size: 285704 Color: 4
Size: 266915 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 364473 Color: 1
Size: 332853 Color: 0
Size: 302675 Color: 2

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 355717 Color: 1
Size: 348566 Color: 2
Size: 295718 Color: 4

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 425361 Color: 2
Size: 305599 Color: 0
Size: 269041 Color: 3

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 373829 Color: 3
Size: 336193 Color: 4
Size: 289979 Color: 2

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 459529 Color: 3
Size: 277365 Color: 0
Size: 263107 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 383122 Color: 0
Size: 312235 Color: 2
Size: 304644 Color: 3

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 443296 Color: 2
Size: 281159 Color: 0
Size: 275546 Color: 2

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 371020 Color: 0
Size: 357207 Color: 3
Size: 271774 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 449188 Color: 0
Size: 279315 Color: 1
Size: 271498 Color: 2

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 449762 Color: 3
Size: 294360 Color: 1
Size: 255879 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 424260 Color: 1
Size: 301216 Color: 3
Size: 274525 Color: 4

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 460712 Color: 3
Size: 287920 Color: 2
Size: 251369 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 351809 Color: 4
Size: 332890 Color: 3
Size: 315302 Color: 3

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 458806 Color: 4
Size: 271196 Color: 1
Size: 269999 Color: 2

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 373573 Color: 0
Size: 337160 Color: 1
Size: 289268 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 355583 Color: 2
Size: 327001 Color: 3
Size: 317417 Color: 3

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 383811 Color: 4
Size: 362562 Color: 0
Size: 253628 Color: 2

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 378528 Color: 1
Size: 316160 Color: 4
Size: 305313 Color: 2

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 438863 Color: 3
Size: 286916 Color: 2
Size: 274222 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 378572 Color: 1
Size: 364896 Color: 4
Size: 256533 Color: 3

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 387818 Color: 3
Size: 312707 Color: 1
Size: 299476 Color: 3

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 367231 Color: 2
Size: 326597 Color: 3
Size: 306173 Color: 3

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 379984 Color: 3
Size: 358492 Color: 0
Size: 261525 Color: 2

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 394667 Color: 1
Size: 331697 Color: 4
Size: 273637 Color: 2

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 422021 Color: 0
Size: 293281 Color: 1
Size: 284699 Color: 4

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 492974 Color: 2
Size: 254358 Color: 0
Size: 252669 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 482346 Color: 4
Size: 267172 Color: 4
Size: 250483 Color: 3

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 464544 Color: 3
Size: 283726 Color: 0
Size: 251731 Color: 4

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 409085 Color: 0
Size: 304499 Color: 0
Size: 286417 Color: 2

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 428901 Color: 3
Size: 288512 Color: 4
Size: 282588 Color: 4

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 472581 Color: 0
Size: 268741 Color: 4
Size: 258679 Color: 2

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 393619 Color: 4
Size: 343259 Color: 4
Size: 263123 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 448919 Color: 3
Size: 295708 Color: 2
Size: 255374 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 389377 Color: 0
Size: 321309 Color: 1
Size: 289315 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 416476 Color: 1
Size: 306976 Color: 4
Size: 276549 Color: 3

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 445437 Color: 4
Size: 284867 Color: 0
Size: 269697 Color: 3

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 479613 Color: 1
Size: 262422 Color: 0
Size: 257966 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 369917 Color: 3
Size: 330684 Color: 2
Size: 299400 Color: 4

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 412611 Color: 4
Size: 300153 Color: 2
Size: 287237 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 456285 Color: 3
Size: 276253 Color: 0
Size: 267463 Color: 4

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 445120 Color: 1
Size: 300913 Color: 2
Size: 253968 Color: 2

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 412197 Color: 1
Size: 307372 Color: 0
Size: 280432 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 422057 Color: 2
Size: 324612 Color: 1
Size: 253332 Color: 3

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 403981 Color: 4
Size: 322728 Color: 0
Size: 273292 Color: 3

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 391321 Color: 0
Size: 354195 Color: 3
Size: 254485 Color: 4

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 382640 Color: 4
Size: 360914 Color: 3
Size: 256447 Color: 4

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 440055 Color: 1
Size: 296549 Color: 0
Size: 263397 Color: 4

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 490900 Color: 2
Size: 256665 Color: 1
Size: 252436 Color: 3

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 368691 Color: 4
Size: 333901 Color: 0
Size: 297409 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 486563 Color: 0
Size: 258222 Color: 0
Size: 255216 Color: 4

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 415513 Color: 4
Size: 332976 Color: 2
Size: 251512 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 489585 Color: 3
Size: 260157 Color: 1
Size: 250259 Color: 2

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 492791 Color: 3
Size: 254791 Color: 4
Size: 252419 Color: 4

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 361103 Color: 1
Size: 360935 Color: 1
Size: 277963 Color: 3

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 483904 Color: 1
Size: 263109 Color: 0
Size: 252988 Color: 2

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 398105 Color: 4
Size: 316954 Color: 2
Size: 284942 Color: 3

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 423459 Color: 1
Size: 296583 Color: 0
Size: 279959 Color: 3

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 392256 Color: 3
Size: 350354 Color: 4
Size: 257391 Color: 4

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 472829 Color: 3
Size: 273982 Color: 0
Size: 253190 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 364763 Color: 4
Size: 321086 Color: 1
Size: 314152 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 481254 Color: 3
Size: 261482 Color: 1
Size: 257265 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 406448 Color: 3
Size: 333328 Color: 0
Size: 260225 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 378956 Color: 0
Size: 358592 Color: 4
Size: 262453 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 431030 Color: 4
Size: 298876 Color: 4
Size: 270095 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 434398 Color: 4
Size: 294404 Color: 2
Size: 271199 Color: 2

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 474683 Color: 0
Size: 275169 Color: 2
Size: 250149 Color: 3

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 364999 Color: 2
Size: 341226 Color: 0
Size: 293776 Color: 3

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 363434 Color: 3
Size: 350853 Color: 1
Size: 285714 Color: 2

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 426304 Color: 3
Size: 305416 Color: 4
Size: 268281 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 446445 Color: 2
Size: 285282 Color: 4
Size: 268274 Color: 3

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 404748 Color: 2
Size: 308014 Color: 4
Size: 287239 Color: 3

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 413693 Color: 2
Size: 329848 Color: 0
Size: 256460 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 387326 Color: 0
Size: 348712 Color: 1
Size: 263963 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 481669 Color: 2
Size: 263486 Color: 4
Size: 254846 Color: 3

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 399112 Color: 4
Size: 332299 Color: 0
Size: 268590 Color: 3

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 441750 Color: 3
Size: 290870 Color: 1
Size: 267381 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 452221 Color: 3
Size: 273911 Color: 4
Size: 273869 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 422853 Color: 2
Size: 321084 Color: 0
Size: 256064 Color: 1

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 469336 Color: 2
Size: 274056 Color: 4
Size: 256609 Color: 3

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 381271 Color: 3
Size: 321580 Color: 3
Size: 297150 Color: 4

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 381762 Color: 3
Size: 347824 Color: 1
Size: 270415 Color: 2

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 392005 Color: 3
Size: 326007 Color: 4
Size: 281989 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 436813 Color: 1
Size: 301697 Color: 1
Size: 261491 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 366798 Color: 2
Size: 362562 Color: 1
Size: 270641 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 374554 Color: 2
Size: 337263 Color: 2
Size: 288184 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 401653 Color: 0
Size: 300533 Color: 4
Size: 297815 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 478051 Color: 3
Size: 270147 Color: 4
Size: 251803 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 453655 Color: 1
Size: 291045 Color: 4
Size: 255301 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 413037 Color: 0
Size: 329697 Color: 3
Size: 257267 Color: 4

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 344374 Color: 1
Size: 329002 Color: 0
Size: 326625 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 365076 Color: 3
Size: 336809 Color: 4
Size: 298116 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 418152 Color: 2
Size: 300225 Color: 1
Size: 281624 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 451866 Color: 0
Size: 286230 Color: 0
Size: 261905 Color: 3

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 416178 Color: 2
Size: 332758 Color: 4
Size: 251065 Color: 2

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 343365 Color: 3
Size: 341779 Color: 3
Size: 314857 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 473369 Color: 3
Size: 274615 Color: 0
Size: 252017 Color: 1

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 435100 Color: 3
Size: 312799 Color: 0
Size: 252102 Color: 4

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 487003 Color: 1
Size: 260020 Color: 2
Size: 252978 Color: 3

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 484180 Color: 0
Size: 264788 Color: 3
Size: 251033 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 444254 Color: 0
Size: 290830 Color: 3
Size: 264917 Color: 2

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 404998 Color: 1
Size: 298941 Color: 1
Size: 296062 Color: 2

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 371827 Color: 2
Size: 342258 Color: 4
Size: 285916 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 368837 Color: 2
Size: 340173 Color: 3
Size: 290991 Color: 3

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 388971 Color: 1
Size: 349717 Color: 3
Size: 261313 Color: 1

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 409304 Color: 3
Size: 300535 Color: 2
Size: 290162 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 415495 Color: 4
Size: 331847 Color: 2
Size: 252659 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 380759 Color: 1
Size: 368332 Color: 3
Size: 250910 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 412067 Color: 4
Size: 306495 Color: 0
Size: 281439 Color: 2

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 378132 Color: 0
Size: 353935 Color: 0
Size: 267934 Color: 4

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 360365 Color: 3
Size: 327105 Color: 0
Size: 312531 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 481058 Color: 2
Size: 262673 Color: 1
Size: 256270 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 451575 Color: 0
Size: 282760 Color: 3
Size: 265666 Color: 2

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 466369 Color: 2
Size: 281334 Color: 3
Size: 252298 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 414091 Color: 0
Size: 314792 Color: 3
Size: 271118 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 401377 Color: 4
Size: 304064 Color: 1
Size: 294560 Color: 2

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 393640 Color: 0
Size: 310076 Color: 4
Size: 296285 Color: 4

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 446090 Color: 1
Size: 281351 Color: 2
Size: 272560 Color: 4

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 466369 Color: 4
Size: 267192 Color: 0
Size: 266440 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 432885 Color: 1
Size: 300109 Color: 1
Size: 267007 Color: 3

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 373056 Color: 2
Size: 331294 Color: 3
Size: 295651 Color: 3

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 430459 Color: 4
Size: 316347 Color: 3
Size: 253195 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 425754 Color: 1
Size: 319310 Color: 0
Size: 254937 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 465523 Color: 4
Size: 277020 Color: 3
Size: 257458 Color: 4

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 393959 Color: 1
Size: 324890 Color: 0
Size: 281152 Color: 2

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 479202 Color: 3
Size: 260569 Color: 0
Size: 260230 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 470365 Color: 3
Size: 277108 Color: 4
Size: 252528 Color: 2

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 372361 Color: 4
Size: 364773 Color: 2
Size: 262867 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 453544 Color: 1
Size: 291268 Color: 3
Size: 255189 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 443698 Color: 2
Size: 292291 Color: 3
Size: 264012 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 425505 Color: 3
Size: 297806 Color: 0
Size: 276690 Color: 2

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 393299 Color: 4
Size: 306409 Color: 2
Size: 300293 Color: 2

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 486308 Color: 2
Size: 257123 Color: 1
Size: 256570 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 438051 Color: 1
Size: 285233 Color: 3
Size: 276717 Color: 2

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 455834 Color: 0
Size: 276424 Color: 3
Size: 267743 Color: 4

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 367339 Color: 1
Size: 342186 Color: 1
Size: 290476 Color: 2

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 347016 Color: 4
Size: 342276 Color: 0
Size: 310709 Color: 3

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 392430 Color: 0
Size: 352139 Color: 4
Size: 255432 Color: 4

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 415688 Color: 3
Size: 300837 Color: 1
Size: 283476 Color: 2

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 410617 Color: 3
Size: 336861 Color: 1
Size: 252523 Color: 2

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 381845 Color: 0
Size: 309533 Color: 1
Size: 308623 Color: 4

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 458850 Color: 3
Size: 271774 Color: 3
Size: 269377 Color: 2

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 446745 Color: 0
Size: 302236 Color: 4
Size: 251020 Color: 3

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 491899 Color: 4
Size: 256341 Color: 2
Size: 251761 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 455549 Color: 1
Size: 293652 Color: 3
Size: 250800 Color: 4

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 384163 Color: 0
Size: 361915 Color: 3
Size: 253923 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 396734 Color: 4
Size: 302120 Color: 3
Size: 301147 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 421978 Color: 4
Size: 306919 Color: 3
Size: 271104 Color: 2

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 378248 Color: 3
Size: 334800 Color: 0
Size: 286953 Color: 4

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 472662 Color: 4
Size: 273491 Color: 4
Size: 253848 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 416789 Color: 3
Size: 333070 Color: 1
Size: 250142 Color: 4

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 470048 Color: 2
Size: 271120 Color: 1
Size: 258833 Color: 3

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 487348 Color: 0
Size: 262020 Color: 1
Size: 250633 Color: 2

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 338028 Color: 2
Size: 332787 Color: 4
Size: 329186 Color: 4

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 469539 Color: 3
Size: 270831 Color: 2
Size: 259631 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 367495 Color: 3
Size: 331140 Color: 2
Size: 301366 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 456712 Color: 1
Size: 289109 Color: 0
Size: 254180 Color: 2

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 446990 Color: 1
Size: 298011 Color: 2
Size: 255000 Color: 4

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 353284 Color: 4
Size: 341162 Color: 1
Size: 305555 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 394013 Color: 3
Size: 307482 Color: 2
Size: 298506 Color: 3

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 418734 Color: 0
Size: 316716 Color: 1
Size: 264551 Color: 2

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 448103 Color: 2
Size: 282248 Color: 2
Size: 269650 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 346822 Color: 4
Size: 329240 Color: 1
Size: 323939 Color: 4

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 381799 Color: 4
Size: 352860 Color: 3
Size: 265342 Color: 4

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 400372 Color: 2
Size: 337789 Color: 4
Size: 261840 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 345779 Color: 3
Size: 343636 Color: 1
Size: 310586 Color: 4

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 379542 Color: 2
Size: 353150 Color: 1
Size: 267309 Color: 3

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 388845 Color: 3
Size: 318834 Color: 3
Size: 292322 Color: 2

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 440995 Color: 3
Size: 294137 Color: 1
Size: 264869 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 379248 Color: 1
Size: 365344 Color: 0
Size: 255409 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 438856 Color: 2
Size: 309858 Color: 0
Size: 251287 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 472717 Color: 3
Size: 264331 Color: 4
Size: 262953 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 385193 Color: 0
Size: 310426 Color: 4
Size: 304382 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 491494 Color: 3
Size: 257043 Color: 2
Size: 251464 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 484616 Color: 0
Size: 262000 Color: 2
Size: 253385 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 370772 Color: 4
Size: 370135 Color: 3
Size: 259094 Color: 4

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 494004 Color: 1
Size: 255884 Color: 0
Size: 250113 Color: 4

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 435402 Color: 0
Size: 287996 Color: 2
Size: 276603 Color: 3

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 441934 Color: 0
Size: 293070 Color: 1
Size: 264997 Color: 2

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 465276 Color: 1
Size: 279352 Color: 0
Size: 255373 Color: 3

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 407928 Color: 2
Size: 324705 Color: 0
Size: 267368 Color: 4

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 438906 Color: 2
Size: 282482 Color: 3
Size: 278613 Color: 4

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 430724 Color: 4
Size: 304796 Color: 2
Size: 264481 Color: 2

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 460818 Color: 4
Size: 285849 Color: 3
Size: 253334 Color: 4

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 435322 Color: 3
Size: 286145 Color: 0
Size: 278534 Color: 2

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 427038 Color: 2
Size: 292793 Color: 1
Size: 280170 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 373724 Color: 0
Size: 345675 Color: 2
Size: 280602 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 431072 Color: 0
Size: 301895 Color: 0
Size: 267034 Color: 3

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 486999 Color: 3
Size: 258827 Color: 4
Size: 254175 Color: 2

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 483800 Color: 4
Size: 264846 Color: 1
Size: 251355 Color: 3

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 363167 Color: 3
Size: 323514 Color: 4
Size: 313320 Color: 3

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 492181 Color: 4
Size: 254727 Color: 3
Size: 253093 Color: 2

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 441261 Color: 0
Size: 300474 Color: 3
Size: 258266 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 435360 Color: 3
Size: 298708 Color: 0
Size: 265933 Color: 2

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 367027 Color: 4
Size: 351545 Color: 1
Size: 281429 Color: 3

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 370801 Color: 3
Size: 338041 Color: 1
Size: 291159 Color: 2

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 468037 Color: 1
Size: 269918 Color: 0
Size: 262046 Color: 2

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 410728 Color: 3
Size: 298191 Color: 2
Size: 291082 Color: 4

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 493444 Color: 2
Size: 254694 Color: 0
Size: 251863 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 496255 Color: 3
Size: 253181 Color: 2
Size: 250565 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 497533 Color: 4
Size: 251830 Color: 1
Size: 250638 Color: 2

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 401172 Color: 4
Size: 334266 Color: 1
Size: 264563 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 469937 Color: 4
Size: 277385 Color: 3
Size: 252679 Color: 1

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 418282 Color: 2
Size: 298016 Color: 0
Size: 283703 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 426737 Color: 4
Size: 320278 Color: 0
Size: 252986 Color: 3

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 441121 Color: 3
Size: 307494 Color: 1
Size: 251386 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 496648 Color: 4
Size: 252068 Color: 1
Size: 251285 Color: 3

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 361647 Color: 0
Size: 338461 Color: 1
Size: 299893 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 356978 Color: 4
Size: 342525 Color: 0
Size: 300498 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 351996 Color: 0
Size: 333953 Color: 3
Size: 314052 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 360593 Color: 2
Size: 331352 Color: 3
Size: 308056 Color: 2

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 357954 Color: 2
Size: 350787 Color: 0
Size: 291260 Color: 4

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 373235 Color: 0
Size: 358964 Color: 2
Size: 267802 Color: 2

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 363238 Color: 3
Size: 359467 Color: 1
Size: 277296 Color: 4

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 348164 Color: 3
Size: 331067 Color: 2
Size: 320770 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 354099 Color: 2
Size: 344983 Color: 1
Size: 300919 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 348433 Color: 0
Size: 334838 Color: 4
Size: 316730 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 360399 Color: 1
Size: 329766 Color: 3
Size: 309836 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 398389 Color: 2
Size: 345225 Color: 0
Size: 256387 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 346225 Color: 4
Size: 329541 Color: 0
Size: 324235 Color: 4

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 345653 Color: 3
Size: 328310 Color: 3
Size: 326038 Color: 2

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 364054 Color: 3
Size: 319257 Color: 4
Size: 316690 Color: 4

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 365243 Color: 4
Size: 364857 Color: 0
Size: 269901 Color: 1

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 343284 Color: 1
Size: 343225 Color: 0
Size: 313492 Color: 4

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 345927 Color: 2
Size: 336799 Color: 1
Size: 317275 Color: 3

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 368115 Color: 1
Size: 361966 Color: 1
Size: 269920 Color: 2

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 405543 Color: 3
Size: 339313 Color: 4
Size: 255145 Color: 4

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 356309 Color: 2
Size: 322124 Color: 0
Size: 321568 Color: 2

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 370784 Color: 3
Size: 358502 Color: 1
Size: 270715 Color: 1

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 343928 Color: 3
Size: 331736 Color: 2
Size: 324337 Color: 4

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 356696 Color: 1
Size: 325405 Color: 3
Size: 317900 Color: 2

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 358222 Color: 3
Size: 354857 Color: 4
Size: 286922 Color: 3

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 358264 Color: 2
Size: 349626 Color: 0
Size: 292111 Color: 1

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 358569 Color: 2
Size: 331309 Color: 2
Size: 310123 Color: 4

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 360792 Color: 2
Size: 333418 Color: 2
Size: 305791 Color: 4

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 360668 Color: 3
Size: 346012 Color: 2
Size: 293321 Color: 4

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 353584 Color: 3
Size: 332962 Color: 1
Size: 313455 Color: 2

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 368858 Color: 3
Size: 357475 Color: 2
Size: 273668 Color: 2

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 352933 Color: 1
Size: 325214 Color: 0
Size: 321854 Color: 1

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 357307 Color: 0
Size: 348055 Color: 3
Size: 294639 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 359981 Color: 3
Size: 332019 Color: 1
Size: 308001 Color: 4

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 349872 Color: 0
Size: 342941 Color: 2
Size: 307188 Color: 3

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 354158 Color: 3
Size: 351726 Color: 3
Size: 294117 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 359849 Color: 1
Size: 352933 Color: 4
Size: 287219 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 349435 Color: 0
Size: 327998 Color: 4
Size: 322568 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 336977 Color: 2
Size: 332096 Color: 2
Size: 330928 Color: 4

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 378679 Color: 3
Size: 351144 Color: 0
Size: 270178 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 349757 Color: 3
Size: 344734 Color: 3
Size: 305510 Color: 1

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 352574 Color: 3
Size: 336752 Color: 2
Size: 310675 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 359613 Color: 4
Size: 329153 Color: 3
Size: 311235 Color: 2

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 360205 Color: 3
Size: 337023 Color: 4
Size: 302773 Color: 1

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 369896 Color: 4
Size: 356069 Color: 1
Size: 274036 Color: 4

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 351743 Color: 3
Size: 337078 Color: 1
Size: 311180 Color: 2

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 353720 Color: 1
Size: 334490 Color: 2
Size: 311791 Color: 4

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 356650 Color: 0
Size: 334110 Color: 3
Size: 309241 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 349594 Color: 4
Size: 346600 Color: 4
Size: 303807 Color: 1

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 349105 Color: 4
Size: 338701 Color: 4
Size: 312195 Color: 2

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 354765 Color: 4
Size: 322734 Color: 4
Size: 322502 Color: 1

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 356287 Color: 1
Size: 338917 Color: 0
Size: 304797 Color: 2

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 351091 Color: 1
Size: 328254 Color: 2
Size: 320656 Color: 3

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 346299 Color: 2
Size: 328669 Color: 0
Size: 325033 Color: 3

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 356211 Color: 2
Size: 333068 Color: 0
Size: 310722 Color: 2

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 337801 Color: 0
Size: 333093 Color: 0
Size: 329107 Color: 4

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 353219 Color: 0
Size: 352598 Color: 3
Size: 294184 Color: 1

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 342995 Color: 0
Size: 329176 Color: 0
Size: 327830 Color: 1

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 348018 Color: 0
Size: 342049 Color: 1
Size: 309934 Color: 2

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 359944 Color: 0
Size: 336885 Color: 4
Size: 303172 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 342856 Color: 3
Size: 336933 Color: 1
Size: 320212 Color: 4

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 358338 Color: 3
Size: 351571 Color: 4
Size: 290092 Color: 4

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 339989 Color: 3
Size: 333833 Color: 4
Size: 326179 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 356819 Color: 4
Size: 342566 Color: 3
Size: 300616 Color: 4

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 355932 Color: 1
Size: 344475 Color: 3
Size: 299594 Color: 4

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 337432 Color: 1
Size: 333264 Color: 3
Size: 329305 Color: 4

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 354050 Color: 0
Size: 334156 Color: 4
Size: 311795 Color: 3

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 337684 Color: 2
Size: 333867 Color: 2
Size: 328450 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 335271 Color: 3
Size: 335018 Color: 4
Size: 329712 Color: 4

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 359232 Color: 4
Size: 324670 Color: 2
Size: 316099 Color: 1

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 352479 Color: 4
Size: 326481 Color: 4
Size: 321041 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 351708 Color: 4
Size: 346792 Color: 2
Size: 301501 Color: 2

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 345285 Color: 4
Size: 331089 Color: 2
Size: 323627 Color: 4

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 347606 Color: 1
Size: 336702 Color: 4
Size: 315693 Color: 4

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 342720 Color: 2
Size: 336337 Color: 2
Size: 320944 Color: 3

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 351965 Color: 1
Size: 343783 Color: 3
Size: 304253 Color: 3

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 356556 Color: 2
Size: 355316 Color: 1
Size: 288129 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 385187 Color: 2
Size: 356685 Color: 3
Size: 258129 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 355810 Color: 4
Size: 355725 Color: 1
Size: 288466 Color: 3

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 368472 Color: 4
Size: 355002 Color: 0
Size: 276527 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 350880 Color: 2
Size: 338995 Color: 0
Size: 310126 Color: 2

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 396675 Color: 4
Size: 347571 Color: 0
Size: 255755 Color: 3

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 353751 Color: 2
Size: 351755 Color: 4
Size: 294495 Color: 1

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 353056 Color: 0
Size: 351905 Color: 4
Size: 295040 Color: 3

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 361561 Color: 1
Size: 352137 Color: 3
Size: 286303 Color: 1

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 352112 Color: 0
Size: 352071 Color: 3
Size: 295818 Color: 2

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 494961 Color: 0
Size: 253700 Color: 0
Size: 251340 Color: 2

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 381338 Color: 3
Size: 354977 Color: 2
Size: 263686 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 349424 Color: 4
Size: 335867 Color: 4
Size: 314710 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 355174 Color: 0
Size: 346049 Color: 3
Size: 298778 Color: 2

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 344863 Color: 1
Size: 339544 Color: 3
Size: 315594 Color: 1

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 346034 Color: 2
Size: 345831 Color: 3
Size: 308136 Color: 1

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 380995 Color: 2
Size: 361555 Color: 4
Size: 257451 Color: 1

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 344124 Color: 4
Size: 337388 Color: 3
Size: 318489 Color: 4

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 377815 Color: 1
Size: 348858 Color: 1
Size: 273328 Color: 4

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 343062 Color: 3
Size: 330761 Color: 2
Size: 326178 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 348648 Color: 1
Size: 343621 Color: 3
Size: 307732 Color: 3

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 349057 Color: 0
Size: 343718 Color: 1
Size: 307226 Color: 4

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 351245 Color: 2
Size: 344333 Color: 3
Size: 304423 Color: 1

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 351936 Color: 1
Size: 341283 Color: 2
Size: 306782 Color: 3

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 342079 Color: 4
Size: 342047 Color: 0
Size: 315875 Color: 2

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 343533 Color: 3
Size: 337534 Color: 3
Size: 318934 Color: 4

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 339760 Color: 2
Size: 339687 Color: 4
Size: 320554 Color: 3

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 351739 Color: 1
Size: 342727 Color: 1
Size: 305535 Color: 2

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 345392 Color: 1
Size: 341718 Color: 1
Size: 312891 Color: 2

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 359754 Color: 0
Size: 342945 Color: 2
Size: 297302 Color: 2

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 351615 Color: 0
Size: 342608 Color: 3
Size: 305778 Color: 4

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 346327 Color: 2
Size: 342766 Color: 3
Size: 310908 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 340298 Color: 2
Size: 337305 Color: 0
Size: 322398 Color: 4

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 352138 Color: 0
Size: 333111 Color: 1
Size: 314752 Color: 2

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 341829 Color: 1
Size: 330175 Color: 2
Size: 327997 Color: 2

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 359038 Color: 0
Size: 342132 Color: 4
Size: 298831 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 342413 Color: 3
Size: 338886 Color: 0
Size: 318702 Color: 1

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 340766 Color: 2
Size: 331539 Color: 4
Size: 327696 Color: 1

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 340738 Color: 4
Size: 338172 Color: 4
Size: 321091 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 340980 Color: 1
Size: 340658 Color: 4
Size: 318363 Color: 4

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 341773 Color: 4
Size: 339287 Color: 2
Size: 318941 Color: 2

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 355293 Color: 4
Size: 340563 Color: 4
Size: 304145 Color: 2

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 352692 Color: 3
Size: 339540 Color: 4
Size: 307769 Color: 3

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 352040 Color: 4
Size: 339972 Color: 1
Size: 307989 Color: 2

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 343831 Color: 2
Size: 339994 Color: 1
Size: 316176 Color: 3

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 351083 Color: 3
Size: 339390 Color: 2
Size: 309528 Color: 4

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 353577 Color: 2
Size: 337908 Color: 4
Size: 308516 Color: 2

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 341880 Color: 1
Size: 336085 Color: 2
Size: 322036 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 346659 Color: 4
Size: 330661 Color: 1
Size: 322681 Color: 3

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 354549 Color: 0
Size: 325508 Color: 2
Size: 319944 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 344342 Color: 1
Size: 338583 Color: 0
Size: 317076 Color: 2

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 345144 Color: 2
Size: 333399 Color: 4
Size: 321458 Color: 3

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 336459 Color: 0
Size: 334503 Color: 0
Size: 329039 Color: 2

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 356247 Color: 3
Size: 333286 Color: 0
Size: 310468 Color: 1

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 353476 Color: 1
Size: 331245 Color: 4
Size: 315280 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 356633 Color: 0
Size: 336502 Color: 3
Size: 306866 Color: 1

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 356881 Color: 3
Size: 326841 Color: 4
Size: 316279 Color: 1

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 357150 Color: 2
Size: 338856 Color: 0
Size: 303995 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 357772 Color: 2
Size: 356598 Color: 3
Size: 285631 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 357780 Color: 3
Size: 323836 Color: 4
Size: 318385 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 357851 Color: 3
Size: 323637 Color: 2
Size: 318513 Color: 3

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 357897 Color: 1
Size: 348652 Color: 4
Size: 293452 Color: 3

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 358270 Color: 0
Size: 330261 Color: 0
Size: 311470 Color: 4

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 358644 Color: 4
Size: 357663 Color: 2
Size: 283694 Color: 3

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 358670 Color: 0
Size: 325520 Color: 0
Size: 315811 Color: 3

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 358840 Color: 1
Size: 337585 Color: 4
Size: 303576 Color: 1

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 359100 Color: 1
Size: 326610 Color: 1
Size: 314291 Color: 2

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 359477 Color: 2
Size: 347999 Color: 1
Size: 292525 Color: 4

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 359113 Color: 3
Size: 321927 Color: 3
Size: 318961 Color: 4

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 359246 Color: 3
Size: 354830 Color: 1
Size: 285925 Color: 2

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 359602 Color: 0
Size: 324874 Color: 3
Size: 315525 Color: 4

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 359628 Color: 4
Size: 336526 Color: 2
Size: 303847 Color: 1

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 359685 Color: 1
Size: 324971 Color: 4
Size: 315345 Color: 3

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 359718 Color: 4
Size: 331260 Color: 2
Size: 309023 Color: 1

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 360031 Color: 2
Size: 338283 Color: 0
Size: 301687 Color: 3

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 359937 Color: 3
Size: 350041 Color: 4
Size: 290023 Color: 4

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 360093 Color: 3
Size: 354466 Color: 0
Size: 285442 Color: 1

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 360153 Color: 0
Size: 351245 Color: 4
Size: 288603 Color: 2

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 360143 Color: 2
Size: 350067 Color: 0
Size: 289791 Color: 3

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 360353 Color: 4
Size: 335282 Color: 4
Size: 304366 Color: 3

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 360492 Color: 2
Size: 323772 Color: 2
Size: 315737 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 360569 Color: 1
Size: 333052 Color: 3
Size: 306380 Color: 2

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 360692 Color: 1
Size: 336138 Color: 2
Size: 303171 Color: 1

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 360842 Color: 4
Size: 320007 Color: 0
Size: 319152 Color: 2

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 360937 Color: 1
Size: 351091 Color: 0
Size: 287973 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 360886 Color: 2
Size: 351886 Color: 0
Size: 287229 Color: 1

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 361152 Color: 3
Size: 346804 Color: 4
Size: 292045 Color: 1

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 361173 Color: 1
Size: 349229 Color: 2
Size: 289599 Color: 4

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 361284 Color: 2
Size: 348855 Color: 1
Size: 289862 Color: 4

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 361225 Color: 0
Size: 335062 Color: 3
Size: 303714 Color: 2

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 361304 Color: 4
Size: 328128 Color: 3
Size: 310569 Color: 1

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 361339 Color: 3
Size: 337494 Color: 2
Size: 301168 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 361422 Color: 4
Size: 348417 Color: 1
Size: 290162 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 361482 Color: 2
Size: 339948 Color: 0
Size: 298571 Color: 3

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 361543 Color: 1
Size: 354929 Color: 4
Size: 283529 Color: 4

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 361545 Color: 1
Size: 333655 Color: 2
Size: 304801 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 361561 Color: 0
Size: 356806 Color: 4
Size: 281634 Color: 3

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 361673 Color: 2
Size: 323420 Color: 4
Size: 314908 Color: 3

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 361664 Color: 0
Size: 326035 Color: 2
Size: 312302 Color: 4

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 361732 Color: 3
Size: 338172 Color: 2
Size: 300097 Color: 4

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 362110 Color: 2
Size: 323774 Color: 1
Size: 314117 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 361821 Color: 0
Size: 328093 Color: 1
Size: 310087 Color: 3

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 362186 Color: 2
Size: 337170 Color: 0
Size: 300645 Color: 4

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 361875 Color: 0
Size: 346924 Color: 3
Size: 291202 Color: 4

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 362282 Color: 2
Size: 334918 Color: 1
Size: 302801 Color: 3

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 362313 Color: 2
Size: 341191 Color: 0
Size: 296497 Color: 1

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 361994 Color: 0
Size: 330563 Color: 4
Size: 307444 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 362328 Color: 2
Size: 323590 Color: 0
Size: 314083 Color: 4

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 362046 Color: 3
Size: 338946 Color: 4
Size: 299009 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 362393 Color: 2
Size: 318984 Color: 4
Size: 318624 Color: 1

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 362097 Color: 3
Size: 351427 Color: 1
Size: 286477 Color: 4

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 362161 Color: 3
Size: 322332 Color: 1
Size: 315508 Color: 2

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 362194 Color: 0
Size: 347575 Color: 2
Size: 290232 Color: 3

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 362445 Color: 2
Size: 349200 Color: 0
Size: 288356 Color: 3

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 362244 Color: 1
Size: 328282 Color: 3
Size: 309475 Color: 4

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 362464 Color: 2
Size: 344874 Color: 3
Size: 292663 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 362295 Color: 3
Size: 356274 Color: 3
Size: 281432 Color: 2

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 362357 Color: 1
Size: 327257 Color: 3
Size: 310387 Color: 3

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 362570 Color: 3
Size: 339364 Color: 3
Size: 298067 Color: 4

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 362625 Color: 1
Size: 352404 Color: 0
Size: 284972 Color: 4

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 362817 Color: 4
Size: 324513 Color: 0
Size: 312671 Color: 4

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 362834 Color: 0
Size: 323185 Color: 2
Size: 313982 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 362855 Color: 2
Size: 357751 Color: 3
Size: 279395 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 362892 Color: 3
Size: 320852 Color: 4
Size: 316257 Color: 3

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 362979 Color: 3
Size: 341735 Color: 4
Size: 295287 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 363006 Color: 3
Size: 334558 Color: 2
Size: 302437 Color: 3

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 363053 Color: 1
Size: 327942 Color: 2
Size: 309006 Color: 4

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 363073 Color: 4
Size: 354018 Color: 0
Size: 282910 Color: 3

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 363092 Color: 1
Size: 326021 Color: 2
Size: 310888 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 363018 Color: 2
Size: 347744 Color: 3
Size: 289239 Color: 3

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 363345 Color: 1
Size: 338690 Color: 4
Size: 297966 Color: 2

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 363366 Color: 4
Size: 338673 Color: 3
Size: 297962 Color: 1

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 363410 Color: 0
Size: 337865 Color: 4
Size: 298726 Color: 3

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 363463 Color: 0
Size: 330172 Color: 4
Size: 306366 Color: 1

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 363614 Color: 0
Size: 327905 Color: 0
Size: 308482 Color: 2

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 363727 Color: 4
Size: 325678 Color: 2
Size: 310596 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 363768 Color: 0
Size: 335563 Color: 4
Size: 300670 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 363899 Color: 0
Size: 333823 Color: 2
Size: 302279 Color: 4

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 363945 Color: 4
Size: 334857 Color: 2
Size: 301199 Color: 0

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 364025 Color: 3
Size: 330874 Color: 4
Size: 305102 Color: 4

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 364216 Color: 0
Size: 322980 Color: 4
Size: 312805 Color: 3

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 364220 Color: 0
Size: 322279 Color: 3
Size: 313502 Color: 2

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 364456 Color: 2
Size: 363969 Color: 3
Size: 271576 Color: 4

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 364223 Color: 4
Size: 332627 Color: 3
Size: 303151 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 364451 Color: 0
Size: 323684 Color: 3
Size: 311866 Color: 4

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 364455 Color: 0
Size: 336118 Color: 0
Size: 299428 Color: 2

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 364607 Color: 0
Size: 337056 Color: 3
Size: 298338 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 364613 Color: 3
Size: 331665 Color: 4
Size: 303723 Color: 2

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 364683 Color: 3
Size: 338101 Color: 2
Size: 297217 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 364826 Color: 3
Size: 336795 Color: 1
Size: 298380 Color: 4

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 364839 Color: 2
Size: 349705 Color: 4
Size: 285457 Color: 1

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 365037 Color: 4
Size: 339256 Color: 3
Size: 295708 Color: 1

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 365075 Color: 4
Size: 354208 Color: 1
Size: 280718 Color: 2

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 365088 Color: 2
Size: 344329 Color: 3
Size: 290584 Color: 1

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 365148 Color: 0
Size: 321143 Color: 4
Size: 313710 Color: 2

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 365218 Color: 3
Size: 339207 Color: 4
Size: 295576 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 365279 Color: 3
Size: 317888 Color: 1
Size: 316834 Color: 1

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 365351 Color: 3
Size: 351871 Color: 2
Size: 282779 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 365396 Color: 1
Size: 328091 Color: 3
Size: 306514 Color: 2

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 365400 Color: 4
Size: 325954 Color: 3
Size: 308647 Color: 4

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 365406 Color: 0
Size: 322508 Color: 1
Size: 312087 Color: 2

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 365442 Color: 0
Size: 333335 Color: 1
Size: 301224 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 365515 Color: 1
Size: 359947 Color: 3
Size: 274539 Color: 2

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 365586 Color: 1
Size: 322633 Color: 0
Size: 311782 Color: 3

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 365702 Color: 0
Size: 319547 Color: 1
Size: 314752 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 365592 Color: 2
Size: 356708 Color: 0
Size: 277701 Color: 3

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 365746 Color: 0
Size: 342385 Color: 4
Size: 291870 Color: 3

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 365854 Color: 1
Size: 356373 Color: 2
Size: 277774 Color: 4

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 366023 Color: 2
Size: 318783 Color: 3
Size: 315195 Color: 4

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 365983 Color: 0
Size: 343091 Color: 1
Size: 290927 Color: 3

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 366000 Color: 0
Size: 330968 Color: 1
Size: 303033 Color: 3

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 366113 Color: 1
Size: 341054 Color: 4
Size: 292834 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 366279 Color: 4
Size: 365648 Color: 2
Size: 268074 Color: 4

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 366298 Color: 3
Size: 326246 Color: 0
Size: 307457 Color: 3

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 366341 Color: 3
Size: 323361 Color: 1
Size: 310299 Color: 2

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 366388 Color: 1
Size: 351356 Color: 1
Size: 282257 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 366396 Color: 3
Size: 325436 Color: 2
Size: 308169 Color: 1

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 366438 Color: 1
Size: 322617 Color: 3
Size: 310946 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 366866 Color: 2
Size: 317969 Color: 3
Size: 315166 Color: 3

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 366851 Color: 4
Size: 338093 Color: 1
Size: 295057 Color: 4

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 367013 Color: 3
Size: 337114 Color: 0
Size: 295874 Color: 2

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 367051 Color: 2
Size: 322238 Color: 1
Size: 310712 Color: 1

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 367088 Color: 0
Size: 355374 Color: 1
Size: 277539 Color: 1

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 367205 Color: 4
Size: 346114 Color: 1
Size: 286682 Color: 3

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 367217 Color: 3
Size: 326483 Color: 2
Size: 306301 Color: 1

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 367258 Color: 2
Size: 346337 Color: 4
Size: 286406 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 367262 Color: 3
Size: 350274 Color: 4
Size: 282465 Color: 3

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 367364 Color: 1
Size: 359877 Color: 3
Size: 272760 Color: 1

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 367380 Color: 4
Size: 354194 Color: 4
Size: 278427 Color: 2

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 367415 Color: 0
Size: 355179 Color: 2
Size: 277407 Color: 1

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 367430 Color: 4
Size: 345812 Color: 0
Size: 286759 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 367433 Color: 1
Size: 322065 Color: 0
Size: 310503 Color: 2

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 367538 Color: 2
Size: 344526 Color: 4
Size: 287937 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 367451 Color: 0
Size: 357225 Color: 4
Size: 275325 Color: 3

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 367527 Color: 1
Size: 360790 Color: 1
Size: 271684 Color: 4

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 367565 Color: 4
Size: 321511 Color: 2
Size: 310925 Color: 1

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 367623 Color: 4
Size: 340741 Color: 0
Size: 291637 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 367545 Color: 2
Size: 343939 Color: 0
Size: 288517 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 367655 Color: 1
Size: 351794 Color: 0
Size: 280552 Color: 3

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 367888 Color: 2
Size: 348313 Color: 3
Size: 283800 Color: 1

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 367767 Color: 0
Size: 325082 Color: 1
Size: 307152 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 368005 Color: 2
Size: 362622 Color: 4
Size: 269374 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 367853 Color: 3
Size: 353038 Color: 0
Size: 279110 Color: 3

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 367866 Color: 3
Size: 318532 Color: 2
Size: 313603 Color: 4

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 368223 Color: 2
Size: 327311 Color: 4
Size: 304467 Color: 3

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 367870 Color: 4
Size: 325449 Color: 0
Size: 306682 Color: 4

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 368264 Color: 2
Size: 326050 Color: 0
Size: 305687 Color: 3

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 367988 Color: 0
Size: 325817 Color: 4
Size: 306196 Color: 4

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 368295 Color: 3
Size: 356360 Color: 0
Size: 275346 Color: 3

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 368354 Color: 1
Size: 348643 Color: 3
Size: 283004 Color: 2

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 368382 Color: 1
Size: 367924 Color: 0
Size: 263695 Color: 3

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 368394 Color: 4
Size: 347760 Color: 0
Size: 283847 Color: 2

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 368397 Color: 0
Size: 351761 Color: 2
Size: 279843 Color: 3

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 368480 Color: 0
Size: 364066 Color: 4
Size: 267455 Color: 2

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 368398 Color: 2
Size: 338574 Color: 3
Size: 293029 Color: 4

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 368664 Color: 0
Size: 346295 Color: 0
Size: 285042 Color: 3

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 368679 Color: 0
Size: 324254 Color: 3
Size: 307068 Color: 2

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 368795 Color: 1
Size: 327812 Color: 0
Size: 303394 Color: 2

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 368795 Color: 3
Size: 337767 Color: 1
Size: 293439 Color: 2

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 368840 Color: 3
Size: 327803 Color: 3
Size: 303358 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 368848 Color: 4
Size: 338214 Color: 2
Size: 292939 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 368873 Color: 1
Size: 337614 Color: 1
Size: 293514 Color: 2

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 368928 Color: 1
Size: 356266 Color: 2
Size: 274807 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 369001 Color: 3
Size: 330570 Color: 2
Size: 300430 Color: 3

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 369013 Color: 0
Size: 341901 Color: 3
Size: 289087 Color: 4

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 369083 Color: 0
Size: 329754 Color: 1
Size: 301164 Color: 2

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 369226 Color: 4
Size: 367044 Color: 3
Size: 263731 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 369573 Color: 2
Size: 326111 Color: 3
Size: 304317 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 369628 Color: 2
Size: 357633 Color: 0
Size: 272740 Color: 3

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 369345 Color: 3
Size: 344355 Color: 4
Size: 286301 Color: 4

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 369640 Color: 2
Size: 319651 Color: 4
Size: 310710 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 369478 Color: 1
Size: 331860 Color: 3
Size: 298663 Color: 4

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 369733 Color: 2
Size: 363581 Color: 4
Size: 266687 Color: 3

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 369682 Color: 3
Size: 359964 Color: 4
Size: 270355 Color: 4

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 369697 Color: 4
Size: 343264 Color: 2
Size: 287040 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 369764 Color: 2
Size: 354735 Color: 0
Size: 275502 Color: 3

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 369728 Color: 1
Size: 322225 Color: 0
Size: 308048 Color: 4

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 369850 Color: 1
Size: 324255 Color: 0
Size: 305896 Color: 1

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 369853 Color: 0
Size: 338021 Color: 1
Size: 292127 Color: 2

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 369914 Color: 4
Size: 326060 Color: 0
Size: 304027 Color: 3

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 369985 Color: 2
Size: 329893 Color: 4
Size: 300123 Color: 3

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 369958 Color: 0
Size: 348722 Color: 1
Size: 281321 Color: 4

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 369971 Color: 1
Size: 346936 Color: 4
Size: 283094 Color: 2

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 369990 Color: 3
Size: 330877 Color: 0
Size: 299134 Color: 3

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 370050 Color: 0
Size: 317445 Color: 1
Size: 312506 Color: 2

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 370006 Color: 2
Size: 333735 Color: 3
Size: 296260 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 370114 Color: 4
Size: 369198 Color: 3
Size: 260689 Color: 2

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 370154 Color: 0
Size: 357261 Color: 4
Size: 272586 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 370358 Color: 2
Size: 368909 Color: 1
Size: 260734 Color: 4

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 370289 Color: 0
Size: 342705 Color: 4
Size: 287007 Color: 3

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 370468 Color: 3
Size: 360620 Color: 0
Size: 268913 Color: 2

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 370454 Color: 2
Size: 360823 Color: 3
Size: 268724 Color: 3

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 370489 Color: 0
Size: 348373 Color: 1
Size: 281139 Color: 3

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 370516 Color: 1
Size: 324482 Color: 0
Size: 305003 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 370545 Color: 1
Size: 352786 Color: 0
Size: 276670 Color: 2

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 370525 Color: 2
Size: 344237 Color: 0
Size: 285239 Color: 3

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 370559 Color: 4
Size: 364197 Color: 3
Size: 265245 Color: 1

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 370649 Color: 1
Size: 318011 Color: 3
Size: 311341 Color: 2

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 370789 Color: 0
Size: 365870 Color: 1
Size: 263342 Color: 4

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 370948 Color: 2
Size: 316902 Color: 1
Size: 312151 Color: 4

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 370821 Color: 0
Size: 317730 Color: 4
Size: 311450 Color: 1

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 371072 Color: 1
Size: 359088 Color: 2
Size: 269841 Color: 3

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 371114 Color: 3
Size: 350558 Color: 0
Size: 278329 Color: 1

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 371159 Color: 3
Size: 342560 Color: 0
Size: 286282 Color: 2

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 371175 Color: 4
Size: 356270 Color: 2
Size: 272556 Color: 3

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 371291 Color: 0
Size: 331575 Color: 3
Size: 297135 Color: 3

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 371301 Color: 1
Size: 316785 Color: 4
Size: 311915 Color: 3

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 371514 Color: 1
Size: 314632 Color: 1
Size: 313855 Color: 4

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 371598 Color: 4
Size: 328243 Color: 3
Size: 300160 Color: 2

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 371684 Color: 4
Size: 321552 Color: 3
Size: 306765 Color: 2

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 371783 Color: 2
Size: 347199 Color: 3
Size: 281019 Color: 4

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 371685 Color: 1
Size: 348462 Color: 2
Size: 279854 Color: 1

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 371790 Color: 1
Size: 315722 Color: 1
Size: 312489 Color: 3

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 371827 Color: 0
Size: 348013 Color: 1
Size: 280161 Color: 3

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 371901 Color: 1
Size: 329121 Color: 2
Size: 298979 Color: 3

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 371903 Color: 0
Size: 359368 Color: 0
Size: 268730 Color: 2

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 371991 Color: 0
Size: 324062 Color: 1
Size: 303948 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 371999 Color: 1
Size: 314065 Color: 3
Size: 313937 Color: 2

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 371902 Color: 2
Size: 320502 Color: 4
Size: 307597 Color: 3

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 371631 Color: 3
Size: 356452 Color: 4
Size: 271918 Color: 4

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 372012 Color: 1
Size: 333974 Color: 2
Size: 294015 Color: 4

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 372038 Color: 4
Size: 335456 Color: 0
Size: 292507 Color: 3

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 372019 Color: 3
Size: 351958 Color: 2
Size: 276024 Color: 4

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 372168 Color: 0
Size: 356557 Color: 1
Size: 271276 Color: 4

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 372246 Color: 4
Size: 318895 Color: 3
Size: 308860 Color: 2

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 372339 Color: 0
Size: 352270 Color: 3
Size: 275392 Color: 4

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 372207 Color: 3
Size: 328943 Color: 1
Size: 298851 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 372308 Color: 3
Size: 368711 Color: 0
Size: 258982 Color: 2

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 372373 Color: 4
Size: 324617 Color: 0
Size: 303011 Color: 2

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 372410 Color: 1
Size: 319967 Color: 2
Size: 307624 Color: 1

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 372449 Color: 3
Size: 314448 Color: 1
Size: 313104 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 372470 Color: 0
Size: 339682 Color: 0
Size: 287849 Color: 2

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 372524 Color: 4
Size: 339570 Color: 0
Size: 287907 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 372540 Color: 0
Size: 357262 Color: 4
Size: 270199 Color: 2

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 372416 Color: 2
Size: 338799 Color: 0
Size: 288786 Color: 1

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 372608 Color: 4
Size: 317609 Color: 0
Size: 309784 Color: 3

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 372717 Color: 2
Size: 339073 Color: 1
Size: 288211 Color: 4

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 372654 Color: 3
Size: 331333 Color: 0
Size: 296014 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 372663 Color: 0
Size: 323548 Color: 2
Size: 303790 Color: 3

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 372871 Color: 2
Size: 369768 Color: 3
Size: 257362 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 372914 Color: 0
Size: 314365 Color: 1
Size: 312722 Color: 4

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 373024 Color: 4
Size: 358553 Color: 3
Size: 268424 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 373036 Color: 1
Size: 342146 Color: 3
Size: 284819 Color: 2

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 373044 Color: 1
Size: 346301 Color: 1
Size: 280656 Color: 2

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 373060 Color: 4
Size: 350291 Color: 3
Size: 276650 Color: 1

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 373082 Color: 0
Size: 361262 Color: 1
Size: 265657 Color: 2

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 373146 Color: 1
Size: 314596 Color: 3
Size: 312259 Color: 3

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 373153 Color: 0
Size: 339468 Color: 2
Size: 287380 Color: 3

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 373194 Color: 0
Size: 362943 Color: 3
Size: 263864 Color: 4

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 373209 Color: 4
Size: 324345 Color: 3
Size: 302447 Color: 4

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 373213 Color: 4
Size: 324393 Color: 1
Size: 302395 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 373250 Color: 1
Size: 346561 Color: 3
Size: 280190 Color: 4

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 373339 Color: 2
Size: 372466 Color: 3
Size: 254196 Color: 2

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 373339 Color: 4
Size: 317860 Color: 4
Size: 308802 Color: 2

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 373429 Color: 1
Size: 317936 Color: 0
Size: 308636 Color: 2

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 373506 Color: 4
Size: 318342 Color: 3
Size: 308153 Color: 2

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 373577 Color: 1
Size: 354917 Color: 3
Size: 271507 Color: 2

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 373588 Color: 0
Size: 361179 Color: 2
Size: 265234 Color: 3

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 373509 Color: 3
Size: 333409 Color: 4
Size: 293083 Color: 1

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 373594 Color: 3
Size: 337000 Color: 0
Size: 289407 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 373711 Color: 4
Size: 363488 Color: 1
Size: 262802 Color: 2

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 373644 Color: 3
Size: 342112 Color: 0
Size: 284245 Color: 1

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 373800 Color: 0
Size: 319014 Color: 4
Size: 307187 Color: 4

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 373843 Color: 1
Size: 319113 Color: 0
Size: 307045 Color: 4

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 373894 Color: 3
Size: 339742 Color: 1
Size: 286365 Color: 2

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 373936 Color: 2
Size: 319408 Color: 4
Size: 306657 Color: 3

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 373906 Color: 4
Size: 329178 Color: 1
Size: 296917 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 373957 Color: 4
Size: 340003 Color: 4
Size: 286041 Color: 2

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 373960 Color: 4
Size: 337271 Color: 0
Size: 288770 Color: 3

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 374030 Color: 4
Size: 314407 Color: 3
Size: 311564 Color: 2

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 374043 Color: 1
Size: 364998 Color: 1
Size: 260960 Color: 3

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 374098 Color: 3
Size: 350248 Color: 0
Size: 275655 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 374057 Color: 0
Size: 356999 Color: 0
Size: 268945 Color: 4

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 374087 Color: 4
Size: 372678 Color: 1
Size: 253236 Color: 3

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 374190 Color: 1
Size: 321084 Color: 0
Size: 304727 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 374247 Color: 4
Size: 327783 Color: 1
Size: 297971 Color: 3

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 374211 Color: 3
Size: 347939 Color: 1
Size: 277851 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 374294 Color: 4
Size: 330711 Color: 0
Size: 294996 Color: 4

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 374294 Color: 1
Size: 327753 Color: 3
Size: 297954 Color: 2

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 374339 Color: 0
Size: 366429 Color: 0
Size: 259233 Color: 1

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 374352 Color: 1
Size: 316970 Color: 0
Size: 308679 Color: 3

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 374417 Color: 3
Size: 354386 Color: 4
Size: 271198 Color: 4

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 374358 Color: 0
Size: 327852 Color: 0
Size: 297791 Color: 1

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 374372 Color: 2
Size: 351289 Color: 3
Size: 274340 Color: 1

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 374573 Color: 3
Size: 362865 Color: 0
Size: 262563 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 374461 Color: 1
Size: 331080 Color: 4
Size: 294460 Color: 2

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 374641 Color: 4
Size: 353353 Color: 3
Size: 272007 Color: 2

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 374704 Color: 0
Size: 349712 Color: 2
Size: 275585 Color: 4

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 374752 Color: 4
Size: 313245 Color: 0
Size: 312004 Color: 3

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 374801 Color: 1
Size: 353386 Color: 3
Size: 271814 Color: 4

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 374801 Color: 2
Size: 349595 Color: 0
Size: 275605 Color: 2

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 374831 Color: 4
Size: 326548 Color: 3
Size: 298622 Color: 1

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 374839 Color: 0
Size: 364968 Color: 2
Size: 260194 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 374860 Color: 2
Size: 374859 Color: 3
Size: 250282 Color: 4

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 374920 Color: 0
Size: 352444 Color: 3
Size: 272637 Color: 1

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 374930 Color: 4
Size: 317043 Color: 1
Size: 308028 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 374947 Color: 0
Size: 338251 Color: 3
Size: 286803 Color: 2

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 374901 Color: 3
Size: 333099 Color: 2
Size: 292001 Color: 1

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 374950 Color: 2
Size: 325495 Color: 0
Size: 299556 Color: 2

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 374969 Color: 4
Size: 353426 Color: 3
Size: 271606 Color: 4

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 374970 Color: 2
Size: 349868 Color: 1
Size: 275163 Color: 2

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 375044 Color: 2
Size: 362110 Color: 2
Size: 262847 Color: 3

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 375100 Color: 2
Size: 317651 Color: 0
Size: 307250 Color: 1

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 375124 Color: 2
Size: 361013 Color: 3
Size: 263864 Color: 2

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 375160 Color: 0
Size: 373581 Color: 1
Size: 251260 Color: 4

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 375186 Color: 1
Size: 352886 Color: 3
Size: 271929 Color: 2

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 375209 Color: 2
Size: 338025 Color: 2
Size: 286767 Color: 3

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 375218 Color: 2
Size: 321271 Color: 4
Size: 303512 Color: 3

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 375190 Color: 0
Size: 335580 Color: 0
Size: 289231 Color: 2

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 375317 Color: 2
Size: 346151 Color: 0
Size: 278533 Color: 1

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 375362 Color: 3
Size: 321895 Color: 4
Size: 302744 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 375408 Color: 3
Size: 369502 Color: 4
Size: 255091 Color: 1

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 375472 Color: 0
Size: 355923 Color: 1
Size: 268606 Color: 2

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 375584 Color: 2
Size: 361787 Color: 0
Size: 262630 Color: 4

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 375580 Color: 4
Size: 326931 Color: 1
Size: 297490 Color: 1

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 375604 Color: 3
Size: 332431 Color: 2
Size: 291966 Color: 3

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 375612 Color: 2
Size: 333674 Color: 3
Size: 290715 Color: 1

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 375605 Color: 0
Size: 320443 Color: 4
Size: 303953 Color: 4

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 375611 Color: 1
Size: 332092 Color: 4
Size: 292298 Color: 2

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 375614 Color: 1
Size: 344342 Color: 0
Size: 280045 Color: 4

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 375660 Color: 1
Size: 317505 Color: 2
Size: 306836 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 375709 Color: 4
Size: 370063 Color: 2
Size: 254229 Color: 3

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 375714 Color: 4
Size: 345881 Color: 0
Size: 278406 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 375761 Color: 0
Size: 359617 Color: 2
Size: 264623 Color: 4

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 375779 Color: 4
Size: 352389 Color: 0
Size: 271833 Color: 1

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 375806 Color: 0
Size: 325061 Color: 2
Size: 299134 Color: 3

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 375856 Color: 1
Size: 330409 Color: 4
Size: 293736 Color: 4

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 375865 Color: 3
Size: 349571 Color: 2
Size: 274565 Color: 4

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 375958 Color: 3
Size: 330265 Color: 4
Size: 293778 Color: 2

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 375991 Color: 0
Size: 331292 Color: 4
Size: 292718 Color: 3

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 376001 Color: 1
Size: 365821 Color: 2
Size: 258179 Color: 3

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 376024 Color: 4
Size: 371646 Color: 1
Size: 252331 Color: 3

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 376079 Color: 0
Size: 368160 Color: 3
Size: 255762 Color: 2

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 376062 Color: 2
Size: 334063 Color: 3
Size: 289876 Color: 4

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 376129 Color: 3
Size: 355689 Color: 0
Size: 268183 Color: 4

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 376135 Color: 3
Size: 363255 Color: 2
Size: 260611 Color: 4

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 376246 Color: 2
Size: 345809 Color: 1
Size: 277946 Color: 4

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 376214 Color: 3
Size: 357670 Color: 0
Size: 266117 Color: 4

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 376217 Color: 0
Size: 348238 Color: 1
Size: 275546 Color: 3

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 376249 Color: 4
Size: 366879 Color: 1
Size: 256873 Color: 3

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 376292 Color: 3
Size: 372309 Color: 2
Size: 251400 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 376417 Color: 4
Size: 368255 Color: 4
Size: 255329 Color: 2

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 376510 Color: 4
Size: 330690 Color: 2
Size: 292801 Color: 1

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 376539 Color: 0
Size: 354988 Color: 0
Size: 268474 Color: 1

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 376569 Color: 0
Size: 345500 Color: 3
Size: 277932 Color: 2

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 376606 Color: 1
Size: 355927 Color: 3
Size: 267468 Color: 3

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 376611 Color: 3
Size: 339903 Color: 2
Size: 283487 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 376537 Color: 2
Size: 352657 Color: 1
Size: 270807 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 376677 Color: 1
Size: 353221 Color: 0
Size: 270103 Color: 1

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 376701 Color: 4
Size: 363860 Color: 0
Size: 259440 Color: 2

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 376735 Color: 3
Size: 357525 Color: 0
Size: 265741 Color: 1

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 376763 Color: 1
Size: 362648 Color: 2
Size: 260590 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 376764 Color: 1
Size: 312415 Color: 3
Size: 310822 Color: 2

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 474178 Color: 4
Size: 270035 Color: 0
Size: 255788 Color: 3

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 376925 Color: 1
Size: 336462 Color: 2
Size: 286614 Color: 3

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 376874 Color: 2
Size: 362396 Color: 3
Size: 260731 Color: 4

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 376969 Color: 4
Size: 342871 Color: 3
Size: 280161 Color: 4

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 377140 Color: 2
Size: 353067 Color: 4
Size: 269794 Color: 1

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 376990 Color: 3
Size: 345980 Color: 4
Size: 277031 Color: 1

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 377094 Color: 4
Size: 333479 Color: 2
Size: 289428 Color: 1

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 377160 Color: 2
Size: 371434 Color: 0
Size: 251407 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 377122 Color: 4
Size: 336510 Color: 1
Size: 286369 Color: 4

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 377188 Color: 3
Size: 333176 Color: 2
Size: 289637 Color: 4

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 377251 Color: 2
Size: 324407 Color: 1
Size: 298343 Color: 2

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 377213 Color: 4
Size: 358536 Color: 1
Size: 264252 Color: 1

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 377226 Color: 3
Size: 349831 Color: 2
Size: 272944 Color: 3

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 377230 Color: 0
Size: 353827 Color: 4
Size: 268944 Color: 4

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 377269 Color: 0
Size: 360979 Color: 3
Size: 261753 Color: 2

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 377470 Color: 2
Size: 350063 Color: 4
Size: 272468 Color: 4

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 377417 Color: 3
Size: 341880 Color: 3
Size: 280704 Color: 4

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 377483 Color: 4
Size: 328541 Color: 2
Size: 293977 Color: 3

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 377571 Color: 3
Size: 319400 Color: 4
Size: 303030 Color: 4

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 377589 Color: 3
Size: 369131 Color: 2
Size: 253281 Color: 1

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 377612 Color: 3
Size: 337463 Color: 2
Size: 284926 Color: 4

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 377627 Color: 3
Size: 334971 Color: 0
Size: 287403 Color: 1

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 377588 Color: 4
Size: 365198 Color: 1
Size: 257215 Color: 3

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 377685 Color: 3
Size: 337496 Color: 4
Size: 284820 Color: 1

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 377676 Color: 1
Size: 342153 Color: 1
Size: 280172 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 377760 Color: 0
Size: 365730 Color: 2
Size: 256511 Color: 3

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 377803 Color: 0
Size: 323571 Color: 1
Size: 298627 Color: 3

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 377880 Color: 4
Size: 320526 Color: 0
Size: 301595 Color: 2

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 377957 Color: 0
Size: 315211 Color: 4
Size: 306833 Color: 3

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 377945 Color: 3
Size: 315691 Color: 2
Size: 306365 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 378007 Color: 0
Size: 350902 Color: 0
Size: 271092 Color: 2

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 378206 Color: 3
Size: 310922 Color: 0
Size: 310873 Color: 1

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 378080 Color: 2
Size: 316346 Color: 0
Size: 305575 Color: 1

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 378090 Color: 0
Size: 315657 Color: 3
Size: 306254 Color: 2

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 378208 Color: 3
Size: 320301 Color: 4
Size: 301492 Color: 2

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 378141 Color: 1
Size: 320589 Color: 2
Size: 301271 Color: 3

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 378184 Color: 4
Size: 345039 Color: 0
Size: 276778 Color: 4

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 378231 Color: 0
Size: 358447 Color: 3
Size: 263323 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 378269 Color: 1
Size: 314968 Color: 1
Size: 306764 Color: 4

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 378298 Color: 4
Size: 326971 Color: 1
Size: 294732 Color: 3

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 378395 Color: 1
Size: 367911 Color: 0
Size: 253695 Color: 1

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 378587 Color: 3
Size: 331436 Color: 4
Size: 289978 Color: 2

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 378437 Color: 0
Size: 343014 Color: 1
Size: 278550 Color: 2

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 378445 Color: 1
Size: 355147 Color: 2
Size: 266409 Color: 3

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 378648 Color: 3
Size: 369240 Color: 2
Size: 252113 Color: 2

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 378580 Color: 4
Size: 342476 Color: 1
Size: 278945 Color: 4

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 378662 Color: 4
Size: 361456 Color: 3
Size: 259883 Color: 2

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 378690 Color: 1
Size: 335700 Color: 4
Size: 285611 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 378718 Color: 2
Size: 361698 Color: 0
Size: 259585 Color: 3

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 378783 Color: 3
Size: 314165 Color: 1
Size: 307053 Color: 1

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 378745 Color: 1
Size: 322335 Color: 4
Size: 298921 Color: 2

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 378809 Color: 0
Size: 340893 Color: 0
Size: 280299 Color: 3

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 378815 Color: 3
Size: 310945 Color: 1
Size: 310241 Color: 1

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 378859 Color: 1
Size: 334153 Color: 0
Size: 286989 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 378936 Color: 2
Size: 316095 Color: 4
Size: 304970 Color: 3

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 378971 Color: 2
Size: 313049 Color: 3
Size: 307981 Color: 1

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 379003 Color: 1
Size: 358765 Color: 2
Size: 262233 Color: 4

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 379013 Color: 2
Size: 322932 Color: 1
Size: 298056 Color: 0

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 379160 Color: 2
Size: 331421 Color: 3
Size: 289420 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 379375 Color: 3
Size: 344250 Color: 2
Size: 276376 Color: 1

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 379223 Color: 2
Size: 359257 Color: 2
Size: 261521 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 379419 Color: 3
Size: 342196 Color: 2
Size: 278386 Color: 4

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 379293 Color: 0
Size: 342659 Color: 3
Size: 278049 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 379461 Color: 3
Size: 331622 Color: 4
Size: 288918 Color: 4

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 379319 Color: 1
Size: 339026 Color: 0
Size: 281656 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 379438 Color: 2
Size: 315302 Color: 2
Size: 305261 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 379465 Color: 3
Size: 343671 Color: 2
Size: 276865 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 379441 Color: 1
Size: 336747 Color: 4
Size: 283813 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 379499 Color: 0
Size: 345220 Color: 2
Size: 275282 Color: 3

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 379501 Color: 4
Size: 323469 Color: 2
Size: 297031 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 379547 Color: 4
Size: 338693 Color: 2
Size: 281761 Color: 3

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 379561 Color: 1
Size: 311023 Color: 2
Size: 309417 Color: 1

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 379567 Color: 4
Size: 339909 Color: 3
Size: 280525 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 379608 Color: 4
Size: 365369 Color: 2
Size: 255024 Color: 3

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 379691 Color: 4
Size: 354322 Color: 4
Size: 265988 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 379731 Color: 4
Size: 322192 Color: 2
Size: 298078 Color: 3

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 379759 Color: 2
Size: 352132 Color: 4
Size: 268110 Color: 1

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 379791 Color: 1
Size: 355729 Color: 3
Size: 264481 Color: 1

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 379944 Color: 3
Size: 316587 Color: 2
Size: 303470 Color: 1

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 379832 Color: 2
Size: 335445 Color: 4
Size: 284724 Color: 2

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 379881 Color: 1
Size: 314077 Color: 0
Size: 306043 Color: 3

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 379934 Color: 4
Size: 367436 Color: 0
Size: 252631 Color: 4

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 380079 Color: 4
Size: 361949 Color: 0
Size: 257973 Color: 3

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 380234 Color: 3
Size: 335887 Color: 4
Size: 283880 Color: 4

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 380270 Color: 0
Size: 334387 Color: 4
Size: 285344 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 380271 Color: 1
Size: 324250 Color: 0
Size: 295480 Color: 3

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 380275 Color: 4
Size: 333174 Color: 1
Size: 286552 Color: 3

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 380303 Color: 4
Size: 311311 Color: 3
Size: 308387 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 380330 Color: 2
Size: 344642 Color: 3
Size: 275029 Color: 4

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 380400 Color: 0
Size: 355212 Color: 4
Size: 264389 Color: 2

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 380401 Color: 1
Size: 361556 Color: 3
Size: 258044 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 380418 Color: 1
Size: 321690 Color: 1
Size: 297893 Color: 3

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 380455 Color: 0
Size: 312003 Color: 4
Size: 307543 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 380462 Color: 0
Size: 334089 Color: 3
Size: 285450 Color: 4

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 380494 Color: 4
Size: 366896 Color: 1
Size: 252611 Color: 2

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 380554 Color: 4
Size: 346357 Color: 3
Size: 273090 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 380526 Color: 3
Size: 344100 Color: 4
Size: 275375 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 380566 Color: 0
Size: 330280 Color: 2
Size: 289155 Color: 2

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 380615 Color: 4
Size: 318726 Color: 2
Size: 300660 Color: 3

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 380700 Color: 4
Size: 348012 Color: 1
Size: 271289 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 380704 Color: 1
Size: 338279 Color: 2
Size: 281018 Color: 3

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 380738 Color: 2
Size: 358701 Color: 1
Size: 260562 Color: 3

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 380741 Color: 4
Size: 323825 Color: 2
Size: 295435 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 380705 Color: 3
Size: 315821 Color: 1
Size: 303475 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 381001 Color: 1
Size: 316575 Color: 0
Size: 302425 Color: 3

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 381014 Color: 1
Size: 318390 Color: 4
Size: 300597 Color: 4

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 381021 Color: 1
Size: 333113 Color: 1
Size: 285867 Color: 3

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 381059 Color: 0
Size: 351024 Color: 3
Size: 267918 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 381105 Color: 1
Size: 335450 Color: 0
Size: 283446 Color: 2

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 381129 Color: 4
Size: 324800 Color: 3
Size: 294072 Color: 4

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 381130 Color: 2
Size: 341443 Color: 0
Size: 277428 Color: 3

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 381147 Color: 0
Size: 328843 Color: 2
Size: 290011 Color: 2

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 381153 Color: 0
Size: 335425 Color: 1
Size: 283423 Color: 3

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 381211 Color: 4
Size: 333758 Color: 2
Size: 285032 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 381313 Color: 2
Size: 357963 Color: 4
Size: 260725 Color: 3

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 381362 Color: 0
Size: 323840 Color: 3
Size: 294799 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 381081 Color: 3
Size: 323054 Color: 0
Size: 295866 Color: 2

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 381351 Color: 2
Size: 312422 Color: 0
Size: 306228 Color: 1

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 381271 Color: 0
Size: 340952 Color: 2
Size: 277778 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 381372 Color: 1
Size: 330620 Color: 0
Size: 288009 Color: 2

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 381399 Color: 0
Size: 316012 Color: 4
Size: 302590 Color: 3

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 381423 Color: 2
Size: 351018 Color: 0
Size: 267560 Color: 1

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 381445 Color: 4
Size: 337880 Color: 2
Size: 280676 Color: 4

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 381475 Color: 1
Size: 363205 Color: 0
Size: 255321 Color: 2

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 381666 Color: 4
Size: 351006 Color: 3
Size: 267329 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 381671 Color: 0
Size: 352112 Color: 1
Size: 266218 Color: 2

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 381413 Color: 3
Size: 319169 Color: 3
Size: 299419 Color: 1

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 381676 Color: 4
Size: 360906 Color: 4
Size: 257419 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 381688 Color: 2
Size: 366905 Color: 3
Size: 251408 Color: 4

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 381689 Color: 1
Size: 346766 Color: 4
Size: 271546 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 381732 Color: 3
Size: 355895 Color: 0
Size: 262374 Color: 4

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 381788 Color: 4
Size: 355264 Color: 0
Size: 262949 Color: 4

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 381931 Color: 0
Size: 340258 Color: 3
Size: 277812 Color: 2

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 381814 Color: 3
Size: 311418 Color: 1
Size: 306769 Color: 4

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 381981 Color: 0
Size: 326464 Color: 4
Size: 291556 Color: 3

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 381916 Color: 2
Size: 364053 Color: 4
Size: 254032 Color: 3

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 381928 Color: 2
Size: 365297 Color: 1
Size: 252776 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 381979 Color: 4
Size: 336640 Color: 3
Size: 281382 Color: 1

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 381979 Color: 1
Size: 315097 Color: 3
Size: 302925 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 381982 Color: 1
Size: 310732 Color: 2
Size: 307287 Color: 3

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 382167 Color: 0
Size: 362389 Color: 1
Size: 255445 Color: 4

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 382199 Color: 3
Size: 354008 Color: 4
Size: 263794 Color: 1

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 382325 Color: 0
Size: 315385 Color: 3
Size: 302291 Color: 2

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 382350 Color: 2
Size: 309873 Color: 0
Size: 307778 Color: 3

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 382353 Color: 1
Size: 328120 Color: 4
Size: 289528 Color: 3

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 382372 Color: 1
Size: 319630 Color: 3
Size: 297999 Color: 4

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 382393 Color: 4
Size: 338723 Color: 0
Size: 278885 Color: 2

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 382503 Color: 0
Size: 337937 Color: 1
Size: 279561 Color: 3

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 382409 Color: 4
Size: 311654 Color: 1
Size: 305938 Color: 2

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 382467 Color: 1
Size: 351938 Color: 3
Size: 265596 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 382490 Color: 1
Size: 320731 Color: 2
Size: 296780 Color: 4

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 382536 Color: 3
Size: 354209 Color: 3
Size: 263256 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 382570 Color: 1
Size: 359990 Color: 1
Size: 257441 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 382581 Color: 2
Size: 352116 Color: 4
Size: 265304 Color: 4

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 382749 Color: 0
Size: 330356 Color: 4
Size: 286896 Color: 1

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 382711 Color: 2
Size: 346596 Color: 0
Size: 270694 Color: 2

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 382737 Color: 2
Size: 318537 Color: 4
Size: 298727 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 382767 Color: 1
Size: 354698 Color: 2
Size: 262536 Color: 2

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 382836 Color: 2
Size: 346338 Color: 0
Size: 270827 Color: 3

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 382784 Color: 0
Size: 311876 Color: 2
Size: 305341 Color: 4

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 382912 Color: 2
Size: 316594 Color: 4
Size: 300495 Color: 2

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 382960 Color: 4
Size: 345104 Color: 0
Size: 271937 Color: 2

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 382974 Color: 2
Size: 361009 Color: 4
Size: 256018 Color: 3

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 382984 Color: 2
Size: 339901 Color: 4
Size: 277116 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 382989 Color: 4
Size: 317968 Color: 4
Size: 299044 Color: 3

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 383021 Color: 2
Size: 309640 Color: 0
Size: 307340 Color: 4

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 383060 Color: 0
Size: 339221 Color: 3
Size: 277720 Color: 2

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 383027 Color: 2
Size: 338501 Color: 1
Size: 278473 Color: 3

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 383184 Color: 2
Size: 363159 Color: 0
Size: 253658 Color: 4

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 383207 Color: 2
Size: 310406 Color: 0
Size: 306388 Color: 4

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 383156 Color: 0
Size: 355549 Color: 3
Size: 261296 Color: 4

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 383251 Color: 1
Size: 349240 Color: 3
Size: 267510 Color: 3

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 383330 Color: 0
Size: 325300 Color: 3
Size: 291371 Color: 3

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 383408 Color: 2
Size: 325049 Color: 4
Size: 291544 Color: 3

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 383013 Color: 3
Size: 358725 Color: 3
Size: 258263 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 383410 Color: 4
Size: 311179 Color: 2
Size: 305412 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 383416 Color: 2
Size: 347651 Color: 3
Size: 268934 Color: 1

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 383427 Color: 2
Size: 338009 Color: 0
Size: 278565 Color: 1

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 383456 Color: 4
Size: 314095 Color: 3
Size: 302450 Color: 2

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 383463 Color: 4
Size: 355386 Color: 0
Size: 261152 Color: 2

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 383488 Color: 1
Size: 320737 Color: 0
Size: 295776 Color: 2

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 383508 Color: 4
Size: 351103 Color: 2
Size: 265390 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 383446 Color: 0
Size: 311481 Color: 4
Size: 305074 Color: 3

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 383270 Color: 3
Size: 328655 Color: 3
Size: 288076 Color: 4

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 383691 Color: 4
Size: 325071 Color: 1
Size: 291239 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 383761 Color: 4
Size: 365025 Color: 2
Size: 251215 Color: 1

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 383786 Color: 2
Size: 320090 Color: 0
Size: 296125 Color: 2

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 383896 Color: 1
Size: 341089 Color: 3
Size: 275016 Color: 4

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 383953 Color: 1
Size: 343075 Color: 0
Size: 272973 Color: 3

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 383952 Color: 0
Size: 332462 Color: 2
Size: 283587 Color: 3

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 384046 Color: 3
Size: 345019 Color: 1
Size: 270936 Color: 2

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 384049 Color: 2
Size: 308482 Color: 2
Size: 307470 Color: 4

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 384063 Color: 4
Size: 326066 Color: 0
Size: 289872 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 384219 Color: 0
Size: 337847 Color: 4
Size: 277935 Color: 2

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 384106 Color: 3
Size: 324019 Color: 2
Size: 291876 Color: 1

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 384203 Color: 1
Size: 340725 Color: 0
Size: 275073 Color: 4

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 1
Size: 328609 Color: 4
Size: 287163 Color: 4

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 384308 Color: 3
Size: 328290 Color: 0
Size: 287403 Color: 4

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 384316 Color: 4
Size: 337390 Color: 0
Size: 278295 Color: 3

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 384319 Color: 3
Size: 338119 Color: 4
Size: 277563 Color: 4

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 384346 Color: 1
Size: 364013 Color: 2
Size: 251642 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 384401 Color: 2
Size: 310283 Color: 3
Size: 305317 Color: 3

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 384467 Color: 1
Size: 321620 Color: 0
Size: 293914 Color: 4

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 384512 Color: 3
Size: 354756 Color: 1
Size: 260733 Color: 2

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 384517 Color: 1
Size: 326220 Color: 0
Size: 289264 Color: 3

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 384520 Color: 0
Size: 338904 Color: 1
Size: 276577 Color: 1

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 384534 Color: 1
Size: 321531 Color: 1
Size: 293936 Color: 2

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 384538 Color: 1
Size: 310220 Color: 0
Size: 305243 Color: 1

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 384695 Color: 0
Size: 361128 Color: 2
Size: 254178 Color: 3

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 384658 Color: 3
Size: 353207 Color: 1
Size: 262136 Color: 4

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 384745 Color: 0
Size: 336971 Color: 2
Size: 278285 Color: 4

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 384702 Color: 4
Size: 361474 Color: 2
Size: 253825 Color: 2

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 384765 Color: 4
Size: 328801 Color: 0
Size: 286435 Color: 1

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 384882 Color: 2
Size: 357710 Color: 1
Size: 257409 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 384995 Color: 4
Size: 326641 Color: 3
Size: 288365 Color: 2

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 385001 Color: 3
Size: 346681 Color: 2
Size: 268319 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 385193 Color: 0
Size: 349572 Color: 3
Size: 265236 Color: 2

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 385077 Color: 4
Size: 350694 Color: 1
Size: 264230 Color: 3

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 385164 Color: 3
Size: 358177 Color: 2
Size: 256660 Color: 4

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 385172 Color: 3
Size: 351049 Color: 0
Size: 263780 Color: 2

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 385385 Color: 0
Size: 352944 Color: 1
Size: 261672 Color: 3

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 385424 Color: 0
Size: 312377 Color: 3
Size: 302200 Color: 2

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 385233 Color: 4
Size: 343042 Color: 3
Size: 271726 Color: 1

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 385298 Color: 4
Size: 309646 Color: 3
Size: 305057 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 385538 Color: 0
Size: 322548 Color: 4
Size: 291915 Color: 2

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 385442 Color: 1
Size: 348604 Color: 4
Size: 265955 Color: 1

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 385490 Color: 3
Size: 353822 Color: 4
Size: 260689 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 385599 Color: 0
Size: 333382 Color: 3
Size: 281020 Color: 4

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 385537 Color: 3
Size: 325895 Color: 3
Size: 288569 Color: 2

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 385682 Color: 4
Size: 338779 Color: 0
Size: 275540 Color: 1

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 385727 Color: 4
Size: 359886 Color: 0
Size: 254388 Color: 1

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 385752 Color: 4
Size: 312732 Color: 3
Size: 301517 Color: 1

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 385779 Color: 1
Size: 323590 Color: 2
Size: 290632 Color: 4

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 385864 Color: 1
Size: 338322 Color: 4
Size: 275815 Color: 3

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 385879 Color: 4
Size: 310396 Color: 0
Size: 303726 Color: 3

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 385942 Color: 2
Size: 325619 Color: 1
Size: 288440 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 385962 Color: 0
Size: 344978 Color: 1
Size: 269061 Color: 3

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 385988 Color: 0
Size: 343748 Color: 1
Size: 270265 Color: 3

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 385966 Color: 2
Size: 348577 Color: 0
Size: 265458 Color: 1

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 386013 Color: 0
Size: 316435 Color: 1
Size: 297553 Color: 3

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 386014 Color: 1
Size: 348576 Color: 4
Size: 265411 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 386051 Color: 4
Size: 315287 Color: 2
Size: 298663 Color: 3

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 385860 Color: 3
Size: 314298 Color: 0
Size: 299843 Color: 4

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 386052 Color: 2
Size: 320796 Color: 4
Size: 293153 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 386066 Color: 4
Size: 335928 Color: 2
Size: 278007 Color: 1

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 386078 Color: 4
Size: 358636 Color: 3
Size: 255287 Color: 2

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 386083 Color: 2
Size: 325701 Color: 1
Size: 288217 Color: 3

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 386124 Color: 2
Size: 342465 Color: 0
Size: 271412 Color: 4

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 386178 Color: 1
Size: 363182 Color: 0
Size: 250641 Color: 3

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 386148 Color: 3
Size: 346357 Color: 1
Size: 267496 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 386182 Color: 4
Size: 351649 Color: 0
Size: 262170 Color: 2

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 386188 Color: 2
Size: 322220 Color: 4
Size: 291593 Color: 2

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 386221 Color: 1
Size: 335333 Color: 0
Size: 278447 Color: 3

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 386161 Color: 0
Size: 326431 Color: 4
Size: 287409 Color: 4

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 386345 Color: 1
Size: 345565 Color: 3
Size: 268091 Color: 4

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 386351 Color: 4
Size: 326565 Color: 3
Size: 287085 Color: 2

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 386431 Color: 2
Size: 358642 Color: 1
Size: 254928 Color: 4

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 386468 Color: 0
Size: 361281 Color: 2
Size: 252252 Color: 3

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 386504 Color: 2
Size: 351683 Color: 2
Size: 261814 Color: 1

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 386512 Color: 1
Size: 351338 Color: 3
Size: 262151 Color: 2

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 386551 Color: 0
Size: 354653 Color: 1
Size: 258797 Color: 3

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 386638 Color: 0
Size: 360372 Color: 3
Size: 252991 Color: 1

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 386615 Color: 4
Size: 338843 Color: 1
Size: 274543 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 386679 Color: 4
Size: 317745 Color: 0
Size: 295577 Color: 3

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 386659 Color: 0
Size: 330582 Color: 4
Size: 282760 Color: 2

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 386501 Color: 3
Size: 360447 Color: 3
Size: 253053 Color: 2

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 386707 Color: 2
Size: 314659 Color: 0
Size: 298635 Color: 2

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 386647 Color: 3
Size: 312112 Color: 4
Size: 301242 Color: 2

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 386764 Color: 1
Size: 347562 Color: 0
Size: 265675 Color: 2

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 386822 Color: 0
Size: 335235 Color: 2
Size: 277944 Color: 3

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 386709 Color: 3
Size: 319086 Color: 1
Size: 294206 Color: 2

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 386821 Color: 2
Size: 309682 Color: 0
Size: 303498 Color: 1

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 386938 Color: 4
Size: 349159 Color: 3
Size: 263904 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 387027 Color: 4
Size: 337160 Color: 4
Size: 275814 Color: 2

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 387082 Color: 2
Size: 354030 Color: 3
Size: 258889 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 386975 Color: 3
Size: 307596 Color: 2
Size: 305430 Color: 4

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 386990 Color: 3
Size: 338733 Color: 0
Size: 274278 Color: 2

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 387118 Color: 2
Size: 310321 Color: 2
Size: 302562 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 387019 Color: 3
Size: 313550 Color: 1
Size: 299432 Color: 2

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 387153 Color: 1
Size: 333715 Color: 2
Size: 279133 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 387169 Color: 4
Size: 326230 Color: 0
Size: 286602 Color: 1

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 387184 Color: 4
Size: 341782 Color: 1
Size: 271035 Color: 3

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 387189 Color: 2
Size: 337120 Color: 4
Size: 275692 Color: 1

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 387206 Color: 2
Size: 349035 Color: 0
Size: 263760 Color: 2

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 387342 Color: 0
Size: 316453 Color: 2
Size: 296206 Color: 2

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 387266 Color: 3
Size: 315702 Color: 1
Size: 297033 Color: 2

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 387280 Color: 4
Size: 328680 Color: 1
Size: 284041 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 387283 Color: 1
Size: 334194 Color: 1
Size: 278524 Color: 4

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 387474 Color: 2
Size: 324874 Color: 1
Size: 287653 Color: 3

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 387479 Color: 3
Size: 327314 Color: 1
Size: 285208 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 387586 Color: 0
Size: 355530 Color: 2
Size: 256885 Color: 1

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 387517 Color: 2
Size: 319740 Color: 4
Size: 292744 Color: 3

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 387651 Color: 0
Size: 332290 Color: 2
Size: 280060 Color: 2

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 387572 Color: 1
Size: 319610 Color: 1
Size: 292819 Color: 2

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 387666 Color: 1
Size: 310223 Color: 0
Size: 302112 Color: 4

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 387757 Color: 0
Size: 358249 Color: 1
Size: 253995 Color: 1

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 387821 Color: 3
Size: 359896 Color: 1
Size: 252284 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 387846 Color: 2
Size: 343921 Color: 1
Size: 268234 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 387853 Color: 4
Size: 308211 Color: 3
Size: 303937 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 387860 Color: 2
Size: 347860 Color: 4
Size: 264281 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 387872 Color: 3
Size: 332076 Color: 2
Size: 280053 Color: 4

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 387912 Color: 4
Size: 321629 Color: 2
Size: 290460 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 387956 Color: 1
Size: 354484 Color: 2
Size: 257561 Color: 3

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 387973 Color: 4
Size: 353559 Color: 0
Size: 258469 Color: 3

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 387980 Color: 2
Size: 320673 Color: 0
Size: 291348 Color: 1

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 387990 Color: 2
Size: 312899 Color: 3
Size: 299112 Color: 1

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 388027 Color: 0
Size: 315430 Color: 3
Size: 296544 Color: 2

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 387991 Color: 1
Size: 358487 Color: 3
Size: 253523 Color: 4

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 388080 Color: 1
Size: 307302 Color: 0
Size: 304619 Color: 2

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 388089 Color: 4
Size: 319195 Color: 2
Size: 292717 Color: 1

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 388105 Color: 3
Size: 323259 Color: 0
Size: 288637 Color: 2

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 388106 Color: 2
Size: 360869 Color: 4
Size: 251026 Color: 1

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 388152 Color: 4
Size: 313090 Color: 0
Size: 298759 Color: 2

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 388218 Color: 1
Size: 353075 Color: 3
Size: 258708 Color: 1

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 388266 Color: 3
Size: 348994 Color: 0
Size: 262741 Color: 3

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 388293 Color: 2
Size: 346110 Color: 0
Size: 265598 Color: 1

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 388390 Color: 1
Size: 309249 Color: 2
Size: 302362 Color: 4

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 388430 Color: 4
Size: 356553 Color: 0
Size: 255018 Color: 4

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 388609 Color: 4
Size: 317635 Color: 2
Size: 293757 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 388635 Color: 4
Size: 315140 Color: 4
Size: 296226 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 388739 Color: 2
Size: 344568 Color: 1
Size: 266694 Color: 4

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 388728 Color: 0
Size: 355477 Color: 1
Size: 255796 Color: 2

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 388794 Color: 2
Size: 352414 Color: 3
Size: 258793 Color: 3

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 388823 Color: 2
Size: 321920 Color: 1
Size: 289258 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 388917 Color: 4
Size: 308876 Color: 2
Size: 302208 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 388954 Color: 1
Size: 348187 Color: 3
Size: 262860 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 389057 Color: 2
Size: 332805 Color: 0
Size: 278139 Color: 3

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 389176 Color: 2
Size: 337590 Color: 4
Size: 273235 Color: 3

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 389281 Color: 0
Size: 330930 Color: 2
Size: 279790 Color: 4

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 389201 Color: 4
Size: 305553 Color: 2
Size: 305247 Color: 3

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 389351 Color: 4
Size: 352986 Color: 1
Size: 257664 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 389445 Color: 1
Size: 341693 Color: 2
Size: 268863 Color: 4

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 389533 Color: 0
Size: 312725 Color: 4
Size: 297743 Color: 4

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 389465 Color: 1
Size: 329591 Color: 4
Size: 280945 Color: 2

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 389534 Color: 4
Size: 353643 Color: 0
Size: 256824 Color: 2

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 389626 Color: 0
Size: 318435 Color: 2
Size: 291940 Color: 4

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 389551 Color: 1
Size: 355552 Color: 2
Size: 254898 Color: 3

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 389677 Color: 1
Size: 338501 Color: 4
Size: 271823 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 389716 Color: 0
Size: 342613 Color: 2
Size: 267672 Color: 1

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 389709 Color: 1
Size: 311935 Color: 3
Size: 298357 Color: 4

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 389735 Color: 1
Size: 325525 Color: 0
Size: 284741 Color: 3

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 389742 Color: 3
Size: 337413 Color: 4
Size: 272846 Color: 4

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 389797 Color: 0
Size: 336990 Color: 1
Size: 273214 Color: 3

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 389755 Color: 3
Size: 327632 Color: 2
Size: 282614 Color: 1

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 389779 Color: 4
Size: 335899 Color: 0
Size: 274323 Color: 2

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 389825 Color: 0
Size: 353016 Color: 2
Size: 257160 Color: 3

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 389801 Color: 3
Size: 338968 Color: 2
Size: 271232 Color: 4

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 389896 Color: 1
Size: 319656 Color: 0
Size: 290449 Color: 3

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 389942 Color: 4
Size: 332108 Color: 4
Size: 277951 Color: 2

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 390029 Color: 2
Size: 309786 Color: 2
Size: 300186 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 390042 Color: 4
Size: 348682 Color: 0
Size: 261277 Color: 4

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 390082 Color: 4
Size: 326354 Color: 3
Size: 283565 Color: 2

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 390132 Color: 1
Size: 308982 Color: 1
Size: 300887 Color: 4

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 390174 Color: 2
Size: 339063 Color: 1
Size: 270764 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 390194 Color: 0
Size: 348279 Color: 4
Size: 261528 Color: 4

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 390194 Color: 1
Size: 351491 Color: 4
Size: 258316 Color: 2

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 390222 Color: 0
Size: 344358 Color: 2
Size: 265421 Color: 2

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 390320 Color: 4
Size: 346361 Color: 3
Size: 263320 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 390430 Color: 2
Size: 329836 Color: 3
Size: 279735 Color: 3

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 390434 Color: 3
Size: 320873 Color: 3
Size: 288694 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 390527 Color: 4
Size: 349842 Color: 4
Size: 259632 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 390592 Color: 1
Size: 347588 Color: 2
Size: 261821 Color: 3

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 390640 Color: 2
Size: 305870 Color: 0
Size: 303491 Color: 3

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 390698 Color: 3
Size: 312196 Color: 0
Size: 297107 Color: 4

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 390763 Color: 2
Size: 354350 Color: 3
Size: 254888 Color: 3

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 390800 Color: 3
Size: 330276 Color: 0
Size: 278925 Color: 2

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 390828 Color: 3
Size: 318115 Color: 1
Size: 291058 Color: 1

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 390926 Color: 2
Size: 313716 Color: 3
Size: 295359 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 390966 Color: 0
Size: 349494 Color: 1
Size: 259541 Color: 2

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 391113 Color: 0
Size: 332186 Color: 1
Size: 276702 Color: 3

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 391005 Color: 3
Size: 326276 Color: 1
Size: 282720 Color: 1

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 391099 Color: 1
Size: 320864 Color: 0
Size: 288038 Color: 2

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 391120 Color: 3
Size: 358537 Color: 0
Size: 250344 Color: 3

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 391156 Color: 2
Size: 313453 Color: 4
Size: 295392 Color: 2

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 391170 Color: 2
Size: 324646 Color: 4
Size: 284185 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 391171 Color: 3
Size: 326790 Color: 4
Size: 282040 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 391250 Color: 2
Size: 323222 Color: 3
Size: 285529 Color: 3

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 391253 Color: 3
Size: 354927 Color: 0
Size: 253821 Color: 2

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 391260 Color: 2
Size: 347167 Color: 1
Size: 261574 Color: 3

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 391273 Color: 3
Size: 343885 Color: 0
Size: 264843 Color: 1

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 391273 Color: 0
Size: 351012 Color: 2
Size: 257716 Color: 3

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 391341 Color: 4
Size: 323609 Color: 2
Size: 285051 Color: 3

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 391365 Color: 4
Size: 336716 Color: 1
Size: 271920 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 391370 Color: 1
Size: 324118 Color: 1
Size: 284513 Color: 4

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 391413 Color: 2
Size: 333989 Color: 0
Size: 274599 Color: 1

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 391439 Color: 2
Size: 325466 Color: 3
Size: 283096 Color: 4

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 391496 Color: 3
Size: 332727 Color: 2
Size: 275778 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 391580 Color: 0
Size: 356537 Color: 3
Size: 251884 Color: 4

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 391559 Color: 4
Size: 325930 Color: 3
Size: 282512 Color: 1

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 391587 Color: 2
Size: 338366 Color: 4
Size: 270048 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 391624 Color: 0
Size: 330025 Color: 1
Size: 278352 Color: 2

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 391645 Color: 1
Size: 350216 Color: 2
Size: 258140 Color: 2

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 391716 Color: 0
Size: 330419 Color: 4
Size: 277866 Color: 4

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 391799 Color: 1
Size: 322681 Color: 2
Size: 285521 Color: 3

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 391879 Color: 3
Size: 315691 Color: 1
Size: 292431 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 392038 Color: 0
Size: 307931 Color: 3
Size: 300032 Color: 4

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 391926 Color: 2
Size: 343080 Color: 2
Size: 264995 Color: 3

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 392175 Color: 0
Size: 312596 Color: 3
Size: 295230 Color: 2

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 392101 Color: 3
Size: 312870 Color: 4
Size: 295030 Color: 2

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 392160 Color: 1
Size: 307077 Color: 1
Size: 300764 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 392293 Color: 0
Size: 355583 Color: 3
Size: 252125 Color: 1

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 392220 Color: 4
Size: 339301 Color: 1
Size: 268480 Color: 4

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 392316 Color: 0
Size: 335060 Color: 1
Size: 272625 Color: 3

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 392396 Color: 1
Size: 351014 Color: 0
Size: 256591 Color: 1

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 392436 Color: 1
Size: 330654 Color: 4
Size: 276911 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 392470 Color: 1
Size: 335647 Color: 2
Size: 271884 Color: 4

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 392494 Color: 2
Size: 316993 Color: 0
Size: 290514 Color: 3

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 392501 Color: 3
Size: 329949 Color: 4
Size: 277551 Color: 1

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 392590 Color: 0
Size: 316142 Color: 2
Size: 291269 Color: 2

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 392503 Color: 2
Size: 324305 Color: 4
Size: 283193 Color: 4

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 392729 Color: 0
Size: 349759 Color: 2
Size: 257513 Color: 2

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 392820 Color: 0
Size: 305988 Color: 2
Size: 301193 Color: 4

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 392641 Color: 4
Size: 343083 Color: 2
Size: 264277 Color: 3

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 392754 Color: 1
Size: 328960 Color: 0
Size: 278287 Color: 3

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 392773 Color: 1
Size: 313825 Color: 2
Size: 293403 Color: 3

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 392790 Color: 2
Size: 344493 Color: 0
Size: 262718 Color: 1

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 392953 Color: 0
Size: 356269 Color: 4
Size: 250779 Color: 4

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 392825 Color: 4
Size: 313674 Color: 1
Size: 293502 Color: 4

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 392861 Color: 1
Size: 324865 Color: 2
Size: 282275 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 393022 Color: 3
Size: 337683 Color: 1
Size: 269296 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 393055 Color: 4
Size: 331992 Color: 3
Size: 274954 Color: 4

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 393094 Color: 1
Size: 343374 Color: 0
Size: 263533 Color: 1

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 393332 Color: 0
Size: 305491 Color: 2
Size: 301178 Color: 3

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 393270 Color: 4
Size: 336672 Color: 4
Size: 270059 Color: 3

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 393285 Color: 2
Size: 346183 Color: 3
Size: 260533 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 393381 Color: 0
Size: 342611 Color: 4
Size: 264009 Color: 2

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 393365 Color: 4
Size: 348764 Color: 0
Size: 257872 Color: 2

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 393402 Color: 2
Size: 334241 Color: 1
Size: 272358 Color: 1

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 393453 Color: 4
Size: 307289 Color: 3
Size: 299259 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 393466 Color: 0
Size: 343996 Color: 2
Size: 262539 Color: 1

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 393465 Color: 2
Size: 320902 Color: 3
Size: 285634 Color: 4

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 393555 Color: 0
Size: 315393 Color: 2
Size: 291053 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 393572 Color: 0
Size: 322164 Color: 1
Size: 284265 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 393477 Color: 4
Size: 342759 Color: 2
Size: 263765 Color: 2

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 393539 Color: 3
Size: 337808 Color: 1
Size: 268654 Color: 2

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 393557 Color: 1
Size: 303271 Color: 3
Size: 303173 Color: 0

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 393610 Color: 1
Size: 311202 Color: 1
Size: 295189 Color: 2

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 393659 Color: 1
Size: 352023 Color: 1
Size: 254319 Color: 4

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 393671 Color: 3
Size: 310000 Color: 2
Size: 296330 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 393783 Color: 2
Size: 323471 Color: 0
Size: 282747 Color: 1

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 393797 Color: 3
Size: 322836 Color: 1
Size: 283368 Color: 2

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 393810 Color: 4
Size: 342505 Color: 0
Size: 263686 Color: 3

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 393859 Color: 0
Size: 335086 Color: 2
Size: 271056 Color: 2

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 393863 Color: 1
Size: 312897 Color: 1
Size: 293241 Color: 3

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 393878 Color: 4
Size: 341971 Color: 0
Size: 264152 Color: 2

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 393885 Color: 1
Size: 343885 Color: 2
Size: 262231 Color: 2

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 393964 Color: 0
Size: 330487 Color: 3
Size: 275550 Color: 1

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 394060 Color: 1
Size: 327990 Color: 2
Size: 277951 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 394200 Color: 0
Size: 345454 Color: 2
Size: 260347 Color: 1

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 394307 Color: 3
Size: 312402 Color: 2
Size: 293292 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 394364 Color: 4
Size: 341027 Color: 3
Size: 264610 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 394383 Color: 3
Size: 309679 Color: 4
Size: 295939 Color: 2

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 394092 Color: 2
Size: 327962 Color: 4
Size: 277947 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 394405 Color: 1
Size: 324895 Color: 1
Size: 280701 Color: 3

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 394406 Color: 1
Size: 303652 Color: 2
Size: 301943 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 394455 Color: 3
Size: 330077 Color: 2
Size: 275469 Color: 1

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 394495 Color: 2
Size: 336528 Color: 3
Size: 268978 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 394512 Color: 1
Size: 337374 Color: 0
Size: 268115 Color: 4

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 394554 Color: 2
Size: 335205 Color: 1
Size: 270242 Color: 1

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 394584 Color: 2
Size: 312660 Color: 3
Size: 292757 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 394606 Color: 3
Size: 310061 Color: 0
Size: 295334 Color: 1

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 394624 Color: 4
Size: 350072 Color: 1
Size: 255305 Color: 3

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 394657 Color: 4
Size: 326867 Color: 0
Size: 278477 Color: 4

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 394709 Color: 2
Size: 351941 Color: 4
Size: 253351 Color: 3

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 394833 Color: 0
Size: 318944 Color: 4
Size: 286224 Color: 2

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 394837 Color: 3
Size: 307462 Color: 4
Size: 297702 Color: 3

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 394856 Color: 4
Size: 336465 Color: 0
Size: 268680 Color: 2

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 394875 Color: 3
Size: 323920 Color: 1
Size: 281206 Color: 1

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 394882 Color: 3
Size: 331687 Color: 0
Size: 273432 Color: 2

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 394913 Color: 3
Size: 327951 Color: 0
Size: 277137 Color: 2

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 394999 Color: 1
Size: 325953 Color: 0
Size: 279049 Color: 1

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 395026 Color: 0
Size: 330178 Color: 1
Size: 274797 Color: 2

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 395098 Color: 3
Size: 306176 Color: 1
Size: 298727 Color: 2

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 395116 Color: 3
Size: 307256 Color: 1
Size: 297629 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 395149 Color: 2
Size: 314499 Color: 3
Size: 290353 Color: 3

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 395197 Color: 3
Size: 307703 Color: 4
Size: 297101 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 395341 Color: 1
Size: 308956 Color: 3
Size: 295704 Color: 3

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 395349 Color: 3
Size: 332714 Color: 0
Size: 271938 Color: 3

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 395324 Color: 0
Size: 308099 Color: 4
Size: 296578 Color: 2

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 395368 Color: 4
Size: 348439 Color: 1
Size: 256194 Color: 1

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 395399 Color: 4
Size: 317661 Color: 2
Size: 286941 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 395444 Color: 4
Size: 324810 Color: 0
Size: 279747 Color: 1

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 395460 Color: 4
Size: 326155 Color: 1
Size: 278386 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 395492 Color: 3
Size: 347164 Color: 1
Size: 257345 Color: 4

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 395527 Color: 1
Size: 351027 Color: 0
Size: 253447 Color: 3

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 395494 Color: 0
Size: 325395 Color: 3
Size: 279112 Color: 1

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 395546 Color: 1
Size: 350509 Color: 2
Size: 253946 Color: 0

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 395596 Color: 4
Size: 352415 Color: 2
Size: 251990 Color: 1

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 395624 Color: 1
Size: 343198 Color: 0
Size: 261179 Color: 4

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 395738 Color: 0
Size: 349195 Color: 2
Size: 255068 Color: 2

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 395700 Color: 2
Size: 309012 Color: 1
Size: 295289 Color: 2

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 395831 Color: 0
Size: 306368 Color: 3
Size: 297802 Color: 2

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 395846 Color: 0
Size: 309485 Color: 2
Size: 294670 Color: 3

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 395806 Color: 1
Size: 353748 Color: 2
Size: 250447 Color: 3

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 395937 Color: 0
Size: 312755 Color: 2
Size: 291309 Color: 4

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 395878 Color: 1
Size: 328563 Color: 3
Size: 275560 Color: 4

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 396029 Color: 0
Size: 311341 Color: 2
Size: 292631 Color: 1

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 396100 Color: 2
Size: 309229 Color: 2
Size: 294672 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 396196 Color: 4
Size: 307276 Color: 2
Size: 296529 Color: 3

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 396213 Color: 4
Size: 312856 Color: 3
Size: 290932 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 396161 Color: 2
Size: 343090 Color: 2
Size: 260750 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 396394 Color: 0
Size: 348911 Color: 2
Size: 254696 Color: 3

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 396380 Color: 1
Size: 327944 Color: 4
Size: 275677 Color: 4

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 396408 Color: 3
Size: 334217 Color: 2
Size: 269376 Color: 4

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 396444 Color: 0
Size: 320193 Color: 2
Size: 283364 Color: 4

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 396413 Color: 3
Size: 326336 Color: 0
Size: 277252 Color: 1

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 396469 Color: 3
Size: 320741 Color: 2
Size: 282791 Color: 4

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 396375 Color: 2
Size: 338507 Color: 0
Size: 265119 Color: 3

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 396502 Color: 3
Size: 326906 Color: 0
Size: 276593 Color: 1

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 396581 Color: 1
Size: 336572 Color: 2
Size: 266848 Color: 3

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 396395 Color: 2
Size: 305606 Color: 1
Size: 298000 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 396621 Color: 1
Size: 333448 Color: 3
Size: 269932 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 396488 Color: 2
Size: 317083 Color: 3
Size: 286430 Color: 4

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 396629 Color: 3
Size: 306882 Color: 0
Size: 296490 Color: 1

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 396637 Color: 3
Size: 310089 Color: 4
Size: 293275 Color: 1

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 396655 Color: 2
Size: 301820 Color: 3
Size: 301526 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 396730 Color: 1
Size: 323020 Color: 4
Size: 280251 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 396766 Color: 0
Size: 350534 Color: 1
Size: 252701 Color: 3

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 396756 Color: 3
Size: 323624 Color: 3
Size: 279621 Color: 0

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 396802 Color: 1
Size: 317533 Color: 2
Size: 285666 Color: 2

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 396835 Color: 1
Size: 335943 Color: 3
Size: 267223 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 397017 Color: 0
Size: 348344 Color: 4
Size: 254640 Color: 3

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 397036 Color: 3
Size: 350364 Color: 4
Size: 252601 Color: 2

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 397169 Color: 0
Size: 309440 Color: 1
Size: 293392 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 397082 Color: 2
Size: 318718 Color: 1
Size: 284201 Color: 2

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 397132 Color: 4
Size: 327940 Color: 0
Size: 274929 Color: 2

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 397170 Color: 2
Size: 331764 Color: 0
Size: 271067 Color: 4

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 397176 Color: 3
Size: 301531 Color: 4
Size: 301294 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 397319 Color: 2
Size: 320956 Color: 3
Size: 281726 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 397331 Color: 4
Size: 339374 Color: 1
Size: 263296 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 397336 Color: 2
Size: 314427 Color: 2
Size: 288238 Color: 1

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 397460 Color: 0
Size: 336652 Color: 3
Size: 265889 Color: 3

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 397486 Color: 4
Size: 335271 Color: 1
Size: 267244 Color: 2

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 397587 Color: 3
Size: 321229 Color: 4
Size: 281185 Color: 2

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 397598 Color: 1
Size: 328966 Color: 3
Size: 273437 Color: 4

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 397748 Color: 4
Size: 324436 Color: 1
Size: 277817 Color: 2

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 397754 Color: 0
Size: 344852 Color: 0
Size: 257395 Color: 1

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 397781 Color: 0
Size: 326591 Color: 3
Size: 275629 Color: 2

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 397845 Color: 1
Size: 324308 Color: 3
Size: 277848 Color: 3

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 397849 Color: 3
Size: 335982 Color: 2
Size: 266170 Color: 4

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 397857 Color: 4
Size: 330855 Color: 2
Size: 271289 Color: 3

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 397861 Color: 1
Size: 314986 Color: 4
Size: 287154 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 398076 Color: 2
Size: 331704 Color: 3
Size: 270221 Color: 4

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 397894 Color: 0
Size: 320808 Color: 3
Size: 281299 Color: 1

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 398211 Color: 1
Size: 339359 Color: 4
Size: 262431 Color: 2

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 398226 Color: 3
Size: 335752 Color: 4
Size: 266023 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 398258 Color: 1
Size: 342484 Color: 0
Size: 259259 Color: 2

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 398318 Color: 2
Size: 312611 Color: 1
Size: 289072 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 398325 Color: 2
Size: 333066 Color: 1
Size: 268610 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 398329 Color: 0
Size: 329580 Color: 4
Size: 272092 Color: 3

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 398331 Color: 1
Size: 340436 Color: 2
Size: 261234 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 398335 Color: 4
Size: 311421 Color: 3
Size: 290245 Color: 3

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 398441 Color: 3
Size: 309224 Color: 2
Size: 292336 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 398489 Color: 4
Size: 350140 Color: 3
Size: 251372 Color: 1

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 398516 Color: 4
Size: 301022 Color: 4
Size: 300463 Color: 2

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 398534 Color: 1
Size: 344857 Color: 0
Size: 256610 Color: 3

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 398599 Color: 3
Size: 322794 Color: 3
Size: 278608 Color: 2

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 398628 Color: 2
Size: 323558 Color: 4
Size: 277815 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 398688 Color: 3
Size: 311972 Color: 1
Size: 289341 Color: 4

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 398732 Color: 3
Size: 322124 Color: 2
Size: 279145 Color: 3

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 398740 Color: 1
Size: 309307 Color: 4
Size: 291954 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 398819 Color: 2
Size: 329389 Color: 1
Size: 271793 Color: 3

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 398755 Color: 0
Size: 301316 Color: 3
Size: 299930 Color: 4

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 398782 Color: 1
Size: 319703 Color: 2
Size: 281516 Color: 4

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 398876 Color: 0
Size: 337471 Color: 4
Size: 263654 Color: 2

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 398907 Color: 3
Size: 303662 Color: 4
Size: 297432 Color: 1

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 398907 Color: 3
Size: 308029 Color: 2
Size: 293065 Color: 0

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 399012 Color: 4
Size: 320484 Color: 1
Size: 280505 Color: 3

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 399032 Color: 4
Size: 325806 Color: 3
Size: 275163 Color: 2

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 399104 Color: 1
Size: 339119 Color: 2
Size: 261778 Color: 3

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 399136 Color: 1
Size: 340961 Color: 2
Size: 259904 Color: 3

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 399140 Color: 2
Size: 334604 Color: 0
Size: 266257 Color: 4

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 399145 Color: 1
Size: 348150 Color: 0
Size: 252706 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 399215 Color: 4
Size: 320882 Color: 4
Size: 279904 Color: 2

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 399219 Color: 1
Size: 317495 Color: 0
Size: 283287 Color: 1

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 399268 Color: 0
Size: 343415 Color: 2
Size: 257318 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 399316 Color: 3
Size: 332912 Color: 2
Size: 267773 Color: 4

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 399320 Color: 3
Size: 337053 Color: 4
Size: 263628 Color: 3

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 399352 Color: 0
Size: 323745 Color: 2
Size: 276904 Color: 1

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 399374 Color: 4
Size: 328437 Color: 1
Size: 272190 Color: 1

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 399412 Color: 2
Size: 306382 Color: 1
Size: 294207 Color: 3

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 399409 Color: 4
Size: 332953 Color: 1
Size: 267639 Color: 3

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 399589 Color: 4
Size: 327226 Color: 3
Size: 273186 Color: 2

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 399661 Color: 2
Size: 324298 Color: 0
Size: 276042 Color: 1

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 399655 Color: 0
Size: 324268 Color: 3
Size: 276078 Color: 4

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 399682 Color: 3
Size: 349428 Color: 2
Size: 250891 Color: 3

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 399876 Color: 0
Size: 315281 Color: 4
Size: 284844 Color: 1

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 399886 Color: 4
Size: 323947 Color: 2
Size: 276168 Color: 1

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 399942 Color: 2
Size: 312397 Color: 2
Size: 287662 Color: 1

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 399926 Color: 3
Size: 343580 Color: 1
Size: 256495 Color: 3

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 399934 Color: 0
Size: 314709 Color: 0
Size: 285358 Color: 2

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 400009 Color: 3
Size: 337870 Color: 4
Size: 262122 Color: 2

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 400038 Color: 4
Size: 305177 Color: 0
Size: 294786 Color: 1

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 400208 Color: 2
Size: 349476 Color: 3
Size: 250317 Color: 1

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 400041 Color: 1
Size: 303375 Color: 0
Size: 296585 Color: 4

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 400205 Color: 3
Size: 336131 Color: 1
Size: 263665 Color: 2

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 400297 Color: 0
Size: 311803 Color: 1
Size: 287901 Color: 2

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 400345 Color: 1
Size: 323320 Color: 3
Size: 276336 Color: 3

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 400378 Color: 4
Size: 310052 Color: 2
Size: 289571 Color: 3

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 400386 Color: 3
Size: 306326 Color: 4
Size: 293289 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 400633 Color: 2
Size: 330600 Color: 3
Size: 268768 Color: 1

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 400590 Color: 3
Size: 339660 Color: 4
Size: 259751 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 400678 Color: 3
Size: 330602 Color: 2
Size: 268721 Color: 3

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 400947 Color: 2
Size: 342357 Color: 4
Size: 256697 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 400903 Color: 1
Size: 302713 Color: 4
Size: 296385 Color: 4

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 400907 Color: 0
Size: 321413 Color: 2
Size: 277681 Color: 4

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 400957 Color: 2
Size: 330041 Color: 1
Size: 269003 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 400996 Color: 4
Size: 320398 Color: 1
Size: 278607 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 401282 Color: 2
Size: 299640 Color: 3
Size: 299079 Color: 3

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 401280 Color: 4
Size: 335066 Color: 2
Size: 263655 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 401313 Color: 2
Size: 299649 Color: 3
Size: 299039 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 401283 Color: 0
Size: 330552 Color: 3
Size: 268166 Color: 3

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 401369 Color: 2
Size: 305378 Color: 3
Size: 293254 Color: 3

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 401312 Color: 0
Size: 299781 Color: 3
Size: 298908 Color: 4

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 401348 Color: 1
Size: 311316 Color: 4
Size: 287337 Color: 2

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 401430 Color: 4
Size: 338319 Color: 3
Size: 260252 Color: 2

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 401472 Color: 2
Size: 306845 Color: 1
Size: 291684 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 401512 Color: 0
Size: 312439 Color: 4
Size: 286050 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 401565 Color: 1
Size: 335956 Color: 3
Size: 262480 Color: 3

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 401569 Color: 1
Size: 300805 Color: 2
Size: 297627 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 401578 Color: 3
Size: 329033 Color: 2
Size: 269390 Color: 3

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 401588 Color: 3
Size: 317987 Color: 4
Size: 280426 Color: 3

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 401628 Color: 3
Size: 299617 Color: 2
Size: 298756 Color: 3

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 401630 Color: 4
Size: 317002 Color: 3
Size: 281369 Color: 2

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 401728 Color: 2
Size: 301877 Color: 4
Size: 296396 Color: 3

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 401686 Color: 4
Size: 341266 Color: 0
Size: 257049 Color: 1

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 401735 Color: 0
Size: 334338 Color: 2
Size: 263928 Color: 4

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 401815 Color: 2
Size: 315074 Color: 0
Size: 283112 Color: 3

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 401796 Color: 4
Size: 326437 Color: 1
Size: 271768 Color: 4

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 401840 Color: 3
Size: 332036 Color: 4
Size: 266125 Color: 4

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 401997 Color: 1
Size: 305436 Color: 2
Size: 292568 Color: 3

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 402071 Color: 1
Size: 310489 Color: 3
Size: 287441 Color: 2

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 402088 Color: 3
Size: 310067 Color: 0
Size: 287846 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 402105 Color: 1
Size: 336726 Color: 3
Size: 261170 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 402118 Color: 4
Size: 336756 Color: 0
Size: 261127 Color: 2

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 402184 Color: 1
Size: 325010 Color: 0
Size: 272807 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 402314 Color: 1
Size: 316815 Color: 2
Size: 280872 Color: 1

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 402315 Color: 3
Size: 322402 Color: 4
Size: 275284 Color: 1

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 402446 Color: 0
Size: 334265 Color: 2
Size: 263290 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 402440 Color: 2
Size: 312475 Color: 4
Size: 285086 Color: 3

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 402510 Color: 3
Size: 304608 Color: 2
Size: 292883 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 402516 Color: 2
Size: 342031 Color: 3
Size: 255454 Color: 4

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 402613 Color: 4
Size: 332457 Color: 4
Size: 264931 Color: 1

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 402698 Color: 2
Size: 334843 Color: 3
Size: 262460 Color: 4

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 402787 Color: 4
Size: 324037 Color: 0
Size: 273177 Color: 2

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 402802 Color: 1
Size: 327576 Color: 3
Size: 269623 Color: 1

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 402930 Color: 4
Size: 339483 Color: 2
Size: 257588 Color: 3

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 402939 Color: 4
Size: 343903 Color: 2
Size: 253159 Color: 1

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 402971 Color: 0
Size: 335303 Color: 2
Size: 261727 Color: 4

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 403065 Color: 2
Size: 312897 Color: 2
Size: 284039 Color: 4

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 402997 Color: 3
Size: 338773 Color: 1
Size: 258231 Color: 1

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 403118 Color: 2
Size: 308325 Color: 3
Size: 288558 Color: 1

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 403213 Color: 2
Size: 336988 Color: 4
Size: 259800 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 403032 Color: 1
Size: 330414 Color: 3
Size: 266555 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 403289 Color: 2
Size: 327860 Color: 4
Size: 268852 Color: 4

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 403185 Color: 4
Size: 323555 Color: 3
Size: 273261 Color: 4

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 403267 Color: 3
Size: 328045 Color: 0
Size: 268689 Color: 2

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 403375 Color: 2
Size: 298573 Color: 0
Size: 298053 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 403278 Color: 3
Size: 333268 Color: 1
Size: 263455 Color: 1

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 403318 Color: 4
Size: 316592 Color: 4
Size: 280091 Color: 2

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 403436 Color: 3
Size: 322413 Color: 2
Size: 274152 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 403468 Color: 0
Size: 321875 Color: 0
Size: 274658 Color: 1

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 403526 Color: 0
Size: 341801 Color: 2
Size: 254674 Color: 4

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 403537 Color: 3
Size: 300885 Color: 1
Size: 295579 Color: 4

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 403641 Color: 4
Size: 311639 Color: 2
Size: 284721 Color: 3

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 403668 Color: 2
Size: 306446 Color: 4
Size: 289887 Color: 1

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 403657 Color: 1
Size: 318396 Color: 3
Size: 277948 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 403689 Color: 4
Size: 344084 Color: 3
Size: 252228 Color: 1

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 403985 Color: 2
Size: 328788 Color: 0
Size: 267228 Color: 4

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 403963 Color: 4
Size: 332194 Color: 3
Size: 263844 Color: 2

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 404023 Color: 3
Size: 344244 Color: 2
Size: 251734 Color: 1

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 404048 Color: 4
Size: 308931 Color: 0
Size: 287022 Color: 2

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 404150 Color: 3
Size: 328583 Color: 1
Size: 267268 Color: 2

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 404127 Color: 2
Size: 323647 Color: 4
Size: 272227 Color: 1

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 404181 Color: 0
Size: 343992 Color: 0
Size: 251828 Color: 3

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 404252 Color: 1
Size: 314594 Color: 3
Size: 281155 Color: 2

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 404310 Color: 0
Size: 316778 Color: 4
Size: 278913 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 404363 Color: 1
Size: 305751 Color: 2
Size: 289887 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 404407 Color: 2
Size: 328108 Color: 4
Size: 267486 Color: 4

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 404378 Color: 0
Size: 315868 Color: 4
Size: 279755 Color: 1

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 404389 Color: 1
Size: 343425 Color: 2
Size: 252187 Color: 3

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 404726 Color: 2
Size: 320530 Color: 1
Size: 274745 Color: 3

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 404624 Color: 3
Size: 343003 Color: 0
Size: 252374 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 404698 Color: 3
Size: 317571 Color: 2
Size: 277732 Color: 4

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 404915 Color: 2
Size: 297823 Color: 3
Size: 297263 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 404720 Color: 4
Size: 332085 Color: 1
Size: 263196 Color: 4

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 404856 Color: 3
Size: 315530 Color: 2
Size: 279615 Color: 1

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 405019 Color: 4
Size: 323670 Color: 0
Size: 271312 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 405024 Color: 1
Size: 325986 Color: 4
Size: 268991 Color: 2

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 405190 Color: 2
Size: 335061 Color: 1
Size: 259750 Color: 1

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 405314 Color: 2
Size: 308159 Color: 4
Size: 286528 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 405208 Color: 1
Size: 338893 Color: 0
Size: 255900 Color: 3

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 405316 Color: 2
Size: 297629 Color: 4
Size: 297056 Color: 3

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 405246 Color: 3
Size: 327150 Color: 4
Size: 267605 Color: 1

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 405372 Color: 4
Size: 340783 Color: 2
Size: 253846 Color: 1

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 405380 Color: 4
Size: 319247 Color: 4
Size: 275374 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 405393 Color: 1
Size: 308882 Color: 4
Size: 285726 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 405449 Color: 0
Size: 322268 Color: 4
Size: 272284 Color: 2

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 405480 Color: 3
Size: 343941 Color: 2
Size: 250580 Color: 1

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 405607 Color: 2
Size: 303342 Color: 4
Size: 291052 Color: 1

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 405544 Color: 4
Size: 319307 Color: 0
Size: 275150 Color: 3

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 405753 Color: 1
Size: 343335 Color: 4
Size: 250913 Color: 2

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 405758 Color: 3
Size: 329875 Color: 1
Size: 264368 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 405764 Color: 3
Size: 298583 Color: 4
Size: 295654 Color: 2

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 405765 Color: 1
Size: 329113 Color: 4
Size: 265123 Color: 2

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 405811 Color: 0
Size: 342036 Color: 1
Size: 252154 Color: 3

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 405833 Color: 0
Size: 318974 Color: 2
Size: 275194 Color: 1

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 405849 Color: 0
Size: 307906 Color: 1
Size: 286246 Color: 1

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 405921 Color: 3
Size: 340258 Color: 0
Size: 253822 Color: 2

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 405980 Color: 2
Size: 336462 Color: 3
Size: 257559 Color: 3

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 405922 Color: 0
Size: 318293 Color: 1
Size: 275786 Color: 1

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 405928 Color: 0
Size: 315980 Color: 2
Size: 278093 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 405979 Color: 4
Size: 312040 Color: 1
Size: 281982 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 405994 Color: 0
Size: 312862 Color: 2
Size: 281145 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 406058 Color: 4
Size: 330694 Color: 1
Size: 263249 Color: 2

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 406070 Color: 3
Size: 320457 Color: 0
Size: 273474 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 406118 Color: 4
Size: 331671 Color: 4
Size: 262212 Color: 2

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 406204 Color: 2
Size: 299278 Color: 0
Size: 294519 Color: 1

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 406153 Color: 3
Size: 313232 Color: 4
Size: 280616 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 406226 Color: 4
Size: 339853 Color: 2
Size: 253922 Color: 3

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 406305 Color: 3
Size: 321035 Color: 0
Size: 272661 Color: 4

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 406398 Color: 0
Size: 317661 Color: 2
Size: 275942 Color: 3

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 406409 Color: 4
Size: 324153 Color: 3
Size: 269439 Color: 3

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 406538 Color: 2
Size: 319800 Color: 0
Size: 273663 Color: 3

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 406700 Color: 2
Size: 332382 Color: 3
Size: 260919 Color: 4

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 406767 Color: 2
Size: 322531 Color: 1
Size: 270703 Color: 1

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 406673 Color: 1
Size: 299012 Color: 0
Size: 294316 Color: 4

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 406806 Color: 1
Size: 297669 Color: 2
Size: 295526 Color: 4

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 406792 Color: 2
Size: 309725 Color: 0
Size: 283484 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 406815 Color: 4
Size: 333625 Color: 0
Size: 259561 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 406825 Color: 3
Size: 304920 Color: 0
Size: 288256 Color: 2

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 406794 Color: 2
Size: 304578 Color: 1
Size: 288629 Color: 3

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 406844 Color: 0
Size: 303889 Color: 0
Size: 289268 Color: 4

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 406857 Color: 4
Size: 326620 Color: 2
Size: 266524 Color: 3

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 406865 Color: 2
Size: 316979 Color: 1
Size: 276157 Color: 4

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 406915 Color: 3
Size: 301880 Color: 0
Size: 291206 Color: 3

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 406997 Color: 2
Size: 337654 Color: 0
Size: 255350 Color: 4

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 406961 Color: 4
Size: 310377 Color: 1
Size: 282663 Color: 1

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 407000 Color: 3
Size: 321668 Color: 2
Size: 271333 Color: 3

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 407005 Color: 2
Size: 341169 Color: 0
Size: 251827 Color: 1

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 407050 Color: 1
Size: 312036 Color: 3
Size: 280915 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 407054 Color: 1
Size: 334778 Color: 2
Size: 258169 Color: 4

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 407017 Color: 2
Size: 334956 Color: 0
Size: 258028 Color: 3

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 407112 Color: 4
Size: 317323 Color: 0
Size: 275566 Color: 1

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 407188 Color: 2
Size: 314140 Color: 3
Size: 278673 Color: 1

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 407124 Color: 1
Size: 333780 Color: 3
Size: 259097 Color: 3

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 407141 Color: 4
Size: 338201 Color: 2
Size: 254659 Color: 1

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 407221 Color: 3
Size: 341365 Color: 1
Size: 251415 Color: 2

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 407221 Color: 3
Size: 337675 Color: 3
Size: 255105 Color: 4

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 407232 Color: 1
Size: 317724 Color: 4
Size: 275045 Color: 2

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 407308 Color: 1
Size: 330791 Color: 2
Size: 261902 Color: 3

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 407334 Color: 0
Size: 324968 Color: 3
Size: 267699 Color: 3

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 407481 Color: 2
Size: 321546 Color: 4
Size: 270974 Color: 4

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 407467 Color: 0
Size: 325598 Color: 0
Size: 266936 Color: 3

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 407639 Color: 3
Size: 304096 Color: 2
Size: 288266 Color: 4

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 407611 Color: 2
Size: 298316 Color: 4
Size: 294074 Color: 1

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 407672 Color: 0
Size: 341407 Color: 4
Size: 250922 Color: 4

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 407709 Color: 1
Size: 337918 Color: 2
Size: 254374 Color: 4

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 407761 Color: 0
Size: 333217 Color: 1
Size: 259023 Color: 2

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 407822 Color: 2
Size: 311872 Color: 3
Size: 280307 Color: 1

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 407853 Color: 3
Size: 327673 Color: 4
Size: 264475 Color: 1

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 407855 Color: 3
Size: 335816 Color: 3
Size: 256330 Color: 2

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 407909 Color: 2
Size: 337960 Color: 1
Size: 254132 Color: 1

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 407880 Color: 3
Size: 313674 Color: 1
Size: 278447 Color: 3

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 407933 Color: 0
Size: 303027 Color: 3
Size: 289041 Color: 4

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 407974 Color: 1
Size: 327469 Color: 2
Size: 264558 Color: 3

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 407983 Color: 4
Size: 319802 Color: 3
Size: 272216 Color: 2

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 408015 Color: 3
Size: 301918 Color: 3
Size: 290068 Color: 4

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 408050 Color: 0
Size: 314016 Color: 1
Size: 277935 Color: 2

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 408085 Color: 2
Size: 332645 Color: 0
Size: 259271 Color: 1

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 408080 Color: 1
Size: 297849 Color: 3
Size: 294072 Color: 4

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 408112 Color: 1
Size: 328672 Color: 2
Size: 263217 Color: 1

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 408260 Color: 1
Size: 341572 Color: 3
Size: 250169 Color: 2

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 408321 Color: 2
Size: 329041 Color: 4
Size: 262639 Color: 1

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 408335 Color: 1
Size: 334539 Color: 0
Size: 257127 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 408365 Color: 4
Size: 329015 Color: 2
Size: 262621 Color: 1

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 408344 Color: 2
Size: 317154 Color: 4
Size: 274503 Color: 3

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 408503 Color: 4
Size: 318886 Color: 1
Size: 272612 Color: 4

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 408511 Color: 0
Size: 322881 Color: 2
Size: 268609 Color: 3

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 408561 Color: 0
Size: 334147 Color: 1
Size: 257293 Color: 1

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 408585 Color: 3
Size: 320522 Color: 2
Size: 270894 Color: 1

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 408653 Color: 2
Size: 323417 Color: 0
Size: 267931 Color: 3

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 408663 Color: 0
Size: 327549 Color: 3
Size: 263789 Color: 4

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 408695 Color: 0
Size: 325985 Color: 2
Size: 265321 Color: 3

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 408736 Color: 1
Size: 324416 Color: 4
Size: 266849 Color: 4

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 408772 Color: 3
Size: 311007 Color: 2
Size: 280222 Color: 4

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 408800 Color: 4
Size: 332667 Color: 1
Size: 258534 Color: 4

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 408851 Color: 4
Size: 331320 Color: 1
Size: 259830 Color: 2

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 408872 Color: 3
Size: 305416 Color: 2
Size: 285713 Color: 3

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 408966 Color: 1
Size: 313151 Color: 0
Size: 277884 Color: 3

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 409035 Color: 0
Size: 340945 Color: 0
Size: 250021 Color: 2

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 409041 Color: 1
Size: 305495 Color: 1
Size: 285465 Color: 0

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 409148 Color: 2
Size: 332634 Color: 1
Size: 258219 Color: 1

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 409146 Color: 3
Size: 305084 Color: 1
Size: 285771 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 409160 Color: 2
Size: 337790 Color: 3
Size: 253051 Color: 2

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 409160 Color: 3
Size: 337080 Color: 3
Size: 253761 Color: 4

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 409170 Color: 3
Size: 332502 Color: 0
Size: 258329 Color: 4

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 409202 Color: 0
Size: 325896 Color: 3
Size: 264903 Color: 2

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 409270 Color: 1
Size: 296869 Color: 1
Size: 293862 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 409379 Color: 2
Size: 322466 Color: 1
Size: 268156 Color: 0

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 409309 Color: 4
Size: 321383 Color: 1
Size: 269309 Color: 3

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 409408 Color: 2
Size: 326792 Color: 3
Size: 263801 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 409366 Color: 3
Size: 313308 Color: 1
Size: 277327 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 409497 Color: 0
Size: 330122 Color: 4
Size: 260382 Color: 2

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 409549 Color: 2
Size: 329830 Color: 0
Size: 260622 Color: 3

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 409599 Color: 3
Size: 322379 Color: 1
Size: 268023 Color: 4

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 409620 Color: 1
Size: 329044 Color: 2
Size: 261337 Color: 3

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 409631 Color: 3
Size: 298761 Color: 3
Size: 291609 Color: 2

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 409727 Color: 0
Size: 316622 Color: 1
Size: 273652 Color: 4

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 409737 Color: 3
Size: 340082 Color: 1
Size: 250182 Color: 2

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 409802 Color: 0
Size: 335733 Color: 4
Size: 254466 Color: 4

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 409846 Color: 1
Size: 317785 Color: 2
Size: 272370 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 409914 Color: 4
Size: 321057 Color: 3
Size: 269030 Color: 2

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 409915 Color: 0
Size: 320404 Color: 1
Size: 269682 Color: 1

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 409934 Color: 4
Size: 331914 Color: 3
Size: 258153 Color: 2

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 409939 Color: 2
Size: 330060 Color: 3
Size: 260002 Color: 1

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 409958 Color: 4
Size: 318708 Color: 4
Size: 271335 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 410030 Color: 2
Size: 339841 Color: 3
Size: 250130 Color: 4

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 410001 Color: 3
Size: 338027 Color: 2
Size: 251973 Color: 4

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 410058 Color: 3
Size: 310266 Color: 2
Size: 279677 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 410091 Color: 0
Size: 331804 Color: 0
Size: 258106 Color: 1

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 410116 Color: 3
Size: 331751 Color: 2
Size: 258134 Color: 3

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 410193 Color: 4
Size: 321856 Color: 3
Size: 267952 Color: 2

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 410231 Color: 1
Size: 311862 Color: 4
Size: 277908 Color: 3

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 410330 Color: 4
Size: 320782 Color: 4
Size: 268889 Color: 1

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 410332 Color: 0
Size: 296148 Color: 4
Size: 293521 Color: 2

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 410491 Color: 2
Size: 338068 Color: 0
Size: 251442 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 410545 Color: 2
Size: 320012 Color: 4
Size: 269444 Color: 4

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 410564 Color: 4
Size: 303041 Color: 1
Size: 286396 Color: 3

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 410586 Color: 1
Size: 319020 Color: 2
Size: 270395 Color: 4

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 410631 Color: 1
Size: 319423 Color: 3
Size: 269947 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 410644 Color: 4
Size: 298948 Color: 2
Size: 290409 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 410757 Color: 0
Size: 335057 Color: 4
Size: 254187 Color: 3

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 410779 Color: 4
Size: 326267 Color: 0
Size: 262955 Color: 2

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 410782 Color: 0
Size: 336963 Color: 3
Size: 252256 Color: 3

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 410841 Color: 3
Size: 299196 Color: 2
Size: 289964 Color: 4

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 411095 Color: 2
Size: 316824 Color: 4
Size: 272082 Color: 3

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 411141 Color: 4
Size: 315663 Color: 0
Size: 273197 Color: 2

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 411275 Color: 2
Size: 300104 Color: 4
Size: 288622 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 411269 Color: 0
Size: 310065 Color: 3
Size: 278667 Color: 1

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 411297 Color: 2
Size: 327093 Color: 3
Size: 261611 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 411333 Color: 4
Size: 331159 Color: 0
Size: 257509 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 411488 Color: 0
Size: 337424 Color: 2
Size: 251089 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 411571 Color: 3
Size: 336711 Color: 1
Size: 251719 Color: 1

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 411574 Color: 0
Size: 333455 Color: 4
Size: 254972 Color: 2

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 411587 Color: 1
Size: 331831 Color: 0
Size: 256583 Color: 4

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 411618 Color: 0
Size: 331307 Color: 2
Size: 257076 Color: 3

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 411640 Color: 0
Size: 315551 Color: 3
Size: 272810 Color: 2

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 411659 Color: 0
Size: 332860 Color: 4
Size: 255482 Color: 3

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 411692 Color: 1
Size: 321094 Color: 3
Size: 267215 Color: 2

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 411712 Color: 4
Size: 331479 Color: 2
Size: 256810 Color: 2

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 411809 Color: 1
Size: 325088 Color: 4
Size: 263104 Color: 3

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 411949 Color: 3
Size: 307359 Color: 0
Size: 280693 Color: 0

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 412044 Color: 3
Size: 316141 Color: 1
Size: 271816 Color: 1

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 412057 Color: 1
Size: 326576 Color: 4
Size: 261368 Color: 3

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 412151 Color: 2
Size: 333537 Color: 1
Size: 254313 Color: 3

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 412155 Color: 4
Size: 323233 Color: 3
Size: 264613 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 412384 Color: 3
Size: 295089 Color: 4
Size: 292528 Color: 4

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 412320 Color: 4
Size: 294519 Color: 0
Size: 293162 Color: 2

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 412566 Color: 0
Size: 299390 Color: 3
Size: 288045 Color: 1

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 412658 Color: 3
Size: 323387 Color: 0
Size: 263956 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 412578 Color: 4
Size: 306518 Color: 1
Size: 280905 Color: 2

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 412658 Color: 3
Size: 299000 Color: 2
Size: 288343 Color: 1

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 412664 Color: 4
Size: 311576 Color: 3
Size: 275761 Color: 1

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 412723 Color: 3
Size: 333402 Color: 4
Size: 253876 Color: 1

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 412704 Color: 1
Size: 297578 Color: 4
Size: 289719 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 412731 Color: 2
Size: 328182 Color: 4
Size: 259088 Color: 3

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 412817 Color: 3
Size: 319509 Color: 1
Size: 267675 Color: 0

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 412830 Color: 4
Size: 322166 Color: 0
Size: 265005 Color: 4

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 412964 Color: 3
Size: 329025 Color: 2
Size: 258012 Color: 4

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 412855 Color: 1
Size: 304888 Color: 1
Size: 282258 Color: 4

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 412864 Color: 4
Size: 294521 Color: 3
Size: 292616 Color: 2

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 412981 Color: 3
Size: 297721 Color: 0
Size: 289299 Color: 1

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 412962 Color: 4
Size: 300171 Color: 1
Size: 286868 Color: 4

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 413043 Color: 1
Size: 323569 Color: 4
Size: 263389 Color: 3

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 413279 Color: 0
Size: 326779 Color: 1
Size: 259943 Color: 4

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 413446 Color: 3
Size: 319316 Color: 1
Size: 267239 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 413401 Color: 0
Size: 333262 Color: 4
Size: 253338 Color: 2

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 413440 Color: 1
Size: 335493 Color: 3
Size: 251068 Color: 2

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 413453 Color: 4
Size: 307356 Color: 0
Size: 279192 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 413540 Color: 2
Size: 326248 Color: 1
Size: 260213 Color: 3

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 413640 Color: 4
Size: 308310 Color: 0
Size: 278051 Color: 3

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 413726 Color: 3
Size: 293508 Color: 2
Size: 292767 Color: 2

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 413742 Color: 2
Size: 293356 Color: 0
Size: 292903 Color: 2

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 413745 Color: 1
Size: 318237 Color: 3
Size: 268019 Color: 2

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 413834 Color: 1
Size: 327414 Color: 0
Size: 258753 Color: 3

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 413891 Color: 4
Size: 320665 Color: 0
Size: 265445 Color: 2

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 413918 Color: 0
Size: 309968 Color: 2
Size: 276115 Color: 3

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 413955 Color: 4
Size: 314354 Color: 1
Size: 271692 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 414096 Color: 0
Size: 298021 Color: 2
Size: 287884 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 414216 Color: 4
Size: 296121 Color: 1
Size: 289664 Color: 3

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 414227 Color: 1
Size: 328094 Color: 2
Size: 257680 Color: 4

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 414485 Color: 3
Size: 307876 Color: 1
Size: 277640 Color: 1

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 414530 Color: 3
Size: 296454 Color: 1
Size: 289017 Color: 2

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 414317 Color: 4
Size: 305705 Color: 2
Size: 279979 Color: 4

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 414491 Color: 4
Size: 318100 Color: 3
Size: 267410 Color: 1

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 414595 Color: 2
Size: 312247 Color: 1
Size: 273159 Color: 1

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 414661 Color: 4
Size: 297895 Color: 3
Size: 287445 Color: 4

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 414719 Color: 1
Size: 329202 Color: 1
Size: 256080 Color: 2

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 414736 Color: 1
Size: 310451 Color: 2
Size: 274814 Color: 3

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 414834 Color: 0
Size: 321633 Color: 4
Size: 263534 Color: 4

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 415122 Color: 4
Size: 297513 Color: 0
Size: 287366 Color: 3

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 415128 Color: 4
Size: 304838 Color: 3
Size: 280035 Color: 1

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 415141 Color: 0
Size: 311601 Color: 4
Size: 273259 Color: 4

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 415218 Color: 0
Size: 329956 Color: 3
Size: 254827 Color: 4

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 415278 Color: 2
Size: 301492 Color: 1
Size: 283231 Color: 4

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 415291 Color: 1
Size: 307473 Color: 4
Size: 277237 Color: 3

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 415355 Color: 3
Size: 324164 Color: 4
Size: 260482 Color: 2

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 415353 Color: 1
Size: 302415 Color: 0
Size: 282233 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 415407 Color: 4
Size: 322420 Color: 3
Size: 262174 Color: 1

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 415461 Color: 4
Size: 325517 Color: 2
Size: 259023 Color: 3

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 415557 Color: 3
Size: 328120 Color: 0
Size: 256324 Color: 4

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 415526 Color: 4
Size: 323188 Color: 3
Size: 261287 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 415661 Color: 4
Size: 305834 Color: 4
Size: 278506 Color: 3

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 415765 Color: 1
Size: 331377 Color: 3
Size: 252859 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 415842 Color: 2
Size: 302198 Color: 1
Size: 281961 Color: 2

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 415859 Color: 4
Size: 297903 Color: 3
Size: 286239 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 415904 Color: 4
Size: 321693 Color: 0
Size: 262404 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 415905 Color: 1
Size: 303495 Color: 2
Size: 280601 Color: 3

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 415934 Color: 3
Size: 309039 Color: 2
Size: 275028 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 415985 Color: 4
Size: 304243 Color: 4
Size: 279773 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 416087 Color: 3
Size: 310382 Color: 0
Size: 273532 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 416106 Color: 0
Size: 321147 Color: 1
Size: 262748 Color: 4

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 416263 Color: 3
Size: 302107 Color: 0
Size: 281631 Color: 4

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 416273 Color: 0
Size: 317191 Color: 4
Size: 266537 Color: 3

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 416284 Color: 1
Size: 301809 Color: 2
Size: 281908 Color: 4

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 416338 Color: 3
Size: 314159 Color: 0
Size: 269504 Color: 2

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 416299 Color: 1
Size: 332136 Color: 4
Size: 251566 Color: 4

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 416334 Color: 4
Size: 294052 Color: 4
Size: 289615 Color: 3

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 416382 Color: 1
Size: 326763 Color: 3
Size: 256856 Color: 1

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 416414 Color: 1
Size: 326735 Color: 2
Size: 256852 Color: 2

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 416466 Color: 1
Size: 310907 Color: 0
Size: 272628 Color: 3

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 416479 Color: 1
Size: 292523 Color: 4
Size: 290999 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 416481 Color: 1
Size: 295127 Color: 3
Size: 288393 Color: 2

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 416563 Color: 0
Size: 326968 Color: 3
Size: 256470 Color: 2

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 416578 Color: 3
Size: 314263 Color: 1
Size: 269160 Color: 4

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 416636 Color: 2
Size: 319690 Color: 1
Size: 263675 Color: 4

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 416706 Color: 3
Size: 291994 Color: 2
Size: 291301 Color: 4

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 416709 Color: 3
Size: 299364 Color: 0
Size: 283928 Color: 2

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 416685 Color: 4
Size: 295024 Color: 1
Size: 288292 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 416746 Color: 1
Size: 323501 Color: 3
Size: 259754 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 416756 Color: 1
Size: 299376 Color: 2
Size: 283869 Color: 2

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 416800 Color: 0
Size: 318480 Color: 2
Size: 264721 Color: 3

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 432666 Color: 1
Size: 306739 Color: 1
Size: 260596 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 416966 Color: 4
Size: 297138 Color: 0
Size: 285897 Color: 1

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 417120 Color: 0
Size: 314382 Color: 3
Size: 268499 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 417146 Color: 3
Size: 313942 Color: 1
Size: 268913 Color: 2

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 417136 Color: 4
Size: 323759 Color: 1
Size: 259106 Color: 2

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 417150 Color: 1
Size: 295494 Color: 4
Size: 287357 Color: 3

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 417285 Color: 3
Size: 298799 Color: 2
Size: 283917 Color: 2

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 417160 Color: 1
Size: 331405 Color: 2
Size: 251436 Color: 4

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 417264 Color: 1
Size: 307219 Color: 3
Size: 275518 Color: 4

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 417293 Color: 2
Size: 309779 Color: 2
Size: 272929 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 417338 Color: 2
Size: 306424 Color: 3
Size: 276239 Color: 4

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 417352 Color: 3
Size: 327878 Color: 4
Size: 254771 Color: 4

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 417357 Color: 2
Size: 331263 Color: 4
Size: 251381 Color: 4

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 417550 Color: 3
Size: 305321 Color: 4
Size: 277130 Color: 1

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 417580 Color: 3
Size: 301958 Color: 4
Size: 280463 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 417565 Color: 2
Size: 325897 Color: 1
Size: 256539 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 417634 Color: 3
Size: 315838 Color: 4
Size: 266529 Color: 4

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 417571 Color: 0
Size: 326381 Color: 2
Size: 256049 Color: 2

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 417593 Color: 1
Size: 319253 Color: 4
Size: 263155 Color: 3

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 417635 Color: 3
Size: 309204 Color: 4
Size: 273162 Color: 2

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 417634 Color: 2
Size: 305726 Color: 1
Size: 276641 Color: 1

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 417698 Color: 3
Size: 301260 Color: 1
Size: 281043 Color: 2

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 417636 Color: 1
Size: 307187 Color: 0
Size: 275178 Color: 4

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 417783 Color: 3
Size: 329183 Color: 0
Size: 253035 Color: 1

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 417788 Color: 3
Size: 317700 Color: 0
Size: 264513 Color: 4

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 417779 Color: 0
Size: 318759 Color: 2
Size: 263463 Color: 3

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 417868 Color: 0
Size: 294005 Color: 3
Size: 288128 Color: 2

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 417892 Color: 0
Size: 299084 Color: 0
Size: 283025 Color: 4

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 418028 Color: 3
Size: 325573 Color: 0
Size: 256400 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 417908 Color: 2
Size: 320752 Color: 0
Size: 261341 Color: 4

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 417971 Color: 1
Size: 316029 Color: 4
Size: 266001 Color: 3

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 418033 Color: 2
Size: 292443 Color: 0
Size: 289525 Color: 3

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 418094 Color: 1
Size: 329531 Color: 2
Size: 252376 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 418109 Color: 4
Size: 331869 Color: 3
Size: 250023 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 418121 Color: 4
Size: 308757 Color: 3
Size: 273123 Color: 1

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 418163 Color: 4
Size: 324427 Color: 0
Size: 257411 Color: 3

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 418218 Color: 0
Size: 300672 Color: 2
Size: 281111 Color: 1

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 418221 Color: 2
Size: 328774 Color: 3
Size: 253006 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 418256 Color: 0
Size: 324573 Color: 3
Size: 257172 Color: 4

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 418318 Color: 1
Size: 298702 Color: 3
Size: 282981 Color: 2

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 418380 Color: 4
Size: 305439 Color: 0
Size: 276182 Color: 2

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 418509 Color: 4
Size: 310543 Color: 4
Size: 270949 Color: 3

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 418570 Color: 0
Size: 312138 Color: 2
Size: 269293 Color: 1

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 418602 Color: 0
Size: 321645 Color: 3
Size: 259754 Color: 1

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 418631 Color: 3
Size: 302946 Color: 4
Size: 278424 Color: 1

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 418720 Color: 2
Size: 317592 Color: 0
Size: 263689 Color: 4

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 418722 Color: 2
Size: 313093 Color: 3
Size: 268186 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 418741 Color: 1
Size: 294708 Color: 2
Size: 286552 Color: 1

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 418850 Color: 0
Size: 295266 Color: 1
Size: 285885 Color: 3

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 418918 Color: 2
Size: 324555 Color: 0
Size: 256528 Color: 4

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 418939 Color: 2
Size: 304925 Color: 1
Size: 276137 Color: 3

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 419095 Color: 3
Size: 304431 Color: 1
Size: 276475 Color: 1

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 418942 Color: 2
Size: 304079 Color: 1
Size: 276980 Color: 2

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 419099 Color: 3
Size: 310528 Color: 0
Size: 270374 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 419111 Color: 2
Size: 328003 Color: 2
Size: 252887 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 419361 Color: 3
Size: 300463 Color: 4
Size: 280177 Color: 2

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 419523 Color: 0
Size: 295035 Color: 2
Size: 285443 Color: 3

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 419770 Color: 1
Size: 303335 Color: 0
Size: 276896 Color: 4

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 419873 Color: 3
Size: 326558 Color: 2
Size: 253570 Color: 2

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 419839 Color: 4
Size: 296234 Color: 2
Size: 283928 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 419929 Color: 0
Size: 321414 Color: 4
Size: 258658 Color: 3

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 419933 Color: 0
Size: 329270 Color: 2
Size: 250798 Color: 1

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 419936 Color: 4
Size: 312909 Color: 2
Size: 267156 Color: 3

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 420005 Color: 2
Size: 296423 Color: 3
Size: 283573 Color: 4

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 420007 Color: 4
Size: 308229 Color: 1
Size: 271765 Color: 1

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 420024 Color: 4
Size: 324584 Color: 2
Size: 255393 Color: 3

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 420043 Color: 3
Size: 320929 Color: 1
Size: 259029 Color: 1

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 420086 Color: 2
Size: 299934 Color: 0
Size: 279981 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 420181 Color: 3
Size: 294571 Color: 0
Size: 285249 Color: 4

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 420377 Color: 0
Size: 305431 Color: 3
Size: 274193 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 420394 Color: 0
Size: 297800 Color: 3
Size: 281807 Color: 1

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 420449 Color: 0
Size: 297593 Color: 3
Size: 281959 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 420475 Color: 0
Size: 315378 Color: 4
Size: 264148 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 420502 Color: 4
Size: 317816 Color: 2
Size: 261683 Color: 3

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 420549 Color: 4
Size: 296259 Color: 3
Size: 283193 Color: 4

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 420708 Color: 0
Size: 311182 Color: 2
Size: 268111 Color: 3

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 420773 Color: 3
Size: 326779 Color: 1
Size: 252449 Color: 1

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 420751 Color: 4
Size: 326705 Color: 0
Size: 252545 Color: 4

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 420824 Color: 3
Size: 324539 Color: 2
Size: 254638 Color: 4

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 420759 Color: 0
Size: 292211 Color: 0
Size: 287031 Color: 1

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 420918 Color: 4
Size: 303939 Color: 0
Size: 275144 Color: 3

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 421143 Color: 3
Size: 304419 Color: 2
Size: 274439 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 421134 Color: 2
Size: 297238 Color: 3
Size: 281629 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 421310 Color: 3
Size: 315384 Color: 0
Size: 263307 Color: 4

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 421257 Color: 0
Size: 313534 Color: 1
Size: 265210 Color: 4

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 421519 Color: 3
Size: 292898 Color: 2
Size: 285584 Color: 1

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 421369 Color: 0
Size: 326846 Color: 1
Size: 251786 Color: 4

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 421428 Color: 1
Size: 316194 Color: 1
Size: 262379 Color: 3

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 421630 Color: 4
Size: 306289 Color: 3
Size: 272082 Color: 4

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 421688 Color: 1
Size: 301538 Color: 4
Size: 276775 Color: 3

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 421711 Color: 2
Size: 316998 Color: 4
Size: 261292 Color: 1

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 421736 Color: 4
Size: 302499 Color: 1
Size: 275766 Color: 3

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 421945 Color: 0
Size: 319276 Color: 0
Size: 258780 Color: 3

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 421972 Color: 4
Size: 325742 Color: 0
Size: 252287 Color: 2

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 421963 Color: 3
Size: 322842 Color: 0
Size: 255196 Color: 1

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 422022 Color: 4
Size: 297541 Color: 3
Size: 280438 Color: 4

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 422024 Color: 0
Size: 302785 Color: 4
Size: 275192 Color: 4

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 422140 Color: 0
Size: 313825 Color: 2
Size: 264036 Color: 3

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 422146 Color: 4
Size: 310222 Color: 0
Size: 267633 Color: 1

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 422237 Color: 2
Size: 303687 Color: 1
Size: 274077 Color: 3

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 422261 Color: 4
Size: 326643 Color: 1
Size: 251097 Color: 4

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 422348 Color: 0
Size: 301718 Color: 4
Size: 275935 Color: 3

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 422360 Color: 4
Size: 321890 Color: 2
Size: 255751 Color: 3

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 422389 Color: 3
Size: 305854 Color: 1
Size: 271758 Color: 0

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 422380 Color: 1
Size: 315086 Color: 2
Size: 262535 Color: 1

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 422467 Color: 2
Size: 300175 Color: 3
Size: 277359 Color: 4

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 422435 Color: 3
Size: 308990 Color: 4
Size: 268576 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 422510 Color: 2
Size: 297640 Color: 1
Size: 279851 Color: 1

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 422636 Color: 3
Size: 301296 Color: 2
Size: 276069 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 422543 Color: 0
Size: 297727 Color: 2
Size: 279731 Color: 1

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 422666 Color: 3
Size: 311502 Color: 4
Size: 265833 Color: 2

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 422802 Color: 3
Size: 317323 Color: 4
Size: 259876 Color: 4

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 422696 Color: 0
Size: 294197 Color: 4
Size: 283108 Color: 4

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 422833 Color: 4
Size: 320122 Color: 1
Size: 257046 Color: 2

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 422838 Color: 2
Size: 322285 Color: 4
Size: 254878 Color: 3

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 422842 Color: 2
Size: 303974 Color: 0
Size: 273185 Color: 3

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 422866 Color: 4
Size: 318111 Color: 1
Size: 259024 Color: 3

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 422984 Color: 3
Size: 295864 Color: 4
Size: 281153 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 422909 Color: 4
Size: 323267 Color: 0
Size: 253825 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 422953 Color: 0
Size: 323588 Color: 3
Size: 253460 Color: 1

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 422970 Color: 4
Size: 309883 Color: 0
Size: 267148 Color: 4

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 423076 Color: 0
Size: 311691 Color: 2
Size: 265234 Color: 3

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 423067 Color: 3
Size: 316844 Color: 1
Size: 260090 Color: 4

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 423159 Color: 2
Size: 300689 Color: 0
Size: 276153 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 423179 Color: 4
Size: 317522 Color: 3
Size: 259300 Color: 1

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 423216 Color: 2
Size: 324348 Color: 3
Size: 252437 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 423240 Color: 2
Size: 313954 Color: 0
Size: 262807 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 423260 Color: 2
Size: 306075 Color: 1
Size: 270666 Color: 1

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 423294 Color: 1
Size: 294616 Color: 3
Size: 282091 Color: 2

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 423368 Color: 3
Size: 297976 Color: 2
Size: 278657 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 423384 Color: 2
Size: 323286 Color: 4
Size: 253331 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 423389 Color: 0
Size: 293022 Color: 1
Size: 283590 Color: 3

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 423419 Color: 2
Size: 317814 Color: 1
Size: 258768 Color: 4

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 423494 Color: 3
Size: 294790 Color: 2
Size: 281717 Color: 1

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 423469 Color: 1
Size: 293388 Color: 4
Size: 283144 Color: 4

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 423500 Color: 1
Size: 309526 Color: 2
Size: 266975 Color: 3

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 423645 Color: 2
Size: 305886 Color: 4
Size: 270470 Color: 3

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 423688 Color: 1
Size: 320715 Color: 1
Size: 255598 Color: 4

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 423880 Color: 3
Size: 292804 Color: 2
Size: 283317 Color: 1

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 423837 Color: 1
Size: 317072 Color: 0
Size: 259092 Color: 2

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 424007 Color: 1
Size: 307332 Color: 1
Size: 268662 Color: 3

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 424059 Color: 4
Size: 302537 Color: 3
Size: 273405 Color: 4

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 424158 Color: 2
Size: 306029 Color: 2
Size: 269814 Color: 4

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 424366 Color: 3
Size: 294343 Color: 4
Size: 281292 Color: 2

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 424291 Color: 1
Size: 314277 Color: 0
Size: 261433 Color: 1

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 424357 Color: 1
Size: 313250 Color: 3
Size: 262394 Color: 4

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 424420 Color: 1
Size: 305304 Color: 2
Size: 270277 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 424445 Color: 2
Size: 289782 Color: 1
Size: 285774 Color: 3

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 424451 Color: 3
Size: 307078 Color: 0
Size: 268472 Color: 1

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 424738 Color: 1
Size: 324100 Color: 2
Size: 251163 Color: 3

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 424756 Color: 4
Size: 324536 Color: 4
Size: 250709 Color: 3

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 424789 Color: 1
Size: 304740 Color: 1
Size: 270472 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 424789 Color: 2
Size: 322866 Color: 0
Size: 252346 Color: 3

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 424792 Color: 2
Size: 298668 Color: 1
Size: 276541 Color: 1

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 424806 Color: 4
Size: 323109 Color: 1
Size: 252086 Color: 3

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 424847 Color: 4
Size: 316029 Color: 1
Size: 259125 Color: 3

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 424889 Color: 0
Size: 311041 Color: 0
Size: 264071 Color: 1

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 424909 Color: 0
Size: 323631 Color: 4
Size: 251461 Color: 3

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 424975 Color: 0
Size: 320844 Color: 0
Size: 254182 Color: 3

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 425007 Color: 3
Size: 315570 Color: 4
Size: 259424 Color: 2

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 425020 Color: 0
Size: 323629 Color: 1
Size: 251352 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 425063 Color: 0
Size: 316194 Color: 3
Size: 258744 Color: 4

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 425103 Color: 3
Size: 288367 Color: 0
Size: 286531 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 425144 Color: 1
Size: 289751 Color: 0
Size: 285106 Color: 2

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 425151 Color: 2
Size: 321863 Color: 3
Size: 252987 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 425243 Color: 1
Size: 318374 Color: 4
Size: 256384 Color: 4

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 425397 Color: 2
Size: 305349 Color: 4
Size: 269255 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 425540 Color: 3
Size: 289782 Color: 2
Size: 284679 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 425601 Color: 3
Size: 319145 Color: 4
Size: 255255 Color: 4

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 425484 Color: 2
Size: 309303 Color: 4
Size: 265214 Color: 1

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 425673 Color: 3
Size: 315657 Color: 0
Size: 258671 Color: 2

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 425557 Color: 2
Size: 298556 Color: 0
Size: 275888 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 425607 Color: 0
Size: 302002 Color: 3
Size: 272392 Color: 4

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 425898 Color: 3
Size: 323060 Color: 1
Size: 251043 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 425987 Color: 3
Size: 306413 Color: 2
Size: 267601 Color: 4

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 425875 Color: 4
Size: 312268 Color: 0
Size: 261858 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 425995 Color: 4
Size: 319273 Color: 0
Size: 254733 Color: 3

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 426045 Color: 4
Size: 292613 Color: 3
Size: 281343 Color: 1

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 426050 Color: 2
Size: 289756 Color: 2
Size: 284195 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 426055 Color: 1
Size: 300427 Color: 1
Size: 273519 Color: 3

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 426081 Color: 0
Size: 294581 Color: 1
Size: 279339 Color: 4

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 426205 Color: 2
Size: 291335 Color: 0
Size: 282461 Color: 1

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 426214 Color: 0
Size: 305318 Color: 2
Size: 268469 Color: 4

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 426252 Color: 2
Size: 313613 Color: 3
Size: 260136 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 426316 Color: 1
Size: 321644 Color: 1
Size: 252041 Color: 3

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 426364 Color: 0
Size: 296204 Color: 2
Size: 277433 Color: 4

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 426380 Color: 2
Size: 298981 Color: 4
Size: 274640 Color: 3

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 426529 Color: 2
Size: 322444 Color: 4
Size: 251028 Color: 4

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 426589 Color: 3
Size: 287989 Color: 4
Size: 285423 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 426684 Color: 4
Size: 287738 Color: 4
Size: 285579 Color: 0

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 426732 Color: 2
Size: 308425 Color: 3
Size: 264844 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 426909 Color: 2
Size: 297752 Color: 0
Size: 275340 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 426978 Color: 2
Size: 291493 Color: 2
Size: 281530 Color: 3

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 426967 Color: 3
Size: 316069 Color: 1
Size: 256965 Color: 1

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 427159 Color: 2
Size: 318534 Color: 3
Size: 254308 Color: 1

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 427159 Color: 4
Size: 319568 Color: 1
Size: 253274 Color: 4

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 427195 Color: 0
Size: 295248 Color: 3
Size: 277558 Color: 1

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 427204 Color: 2
Size: 299450 Color: 0
Size: 273347 Color: 4

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 427279 Color: 1
Size: 297043 Color: 3
Size: 275679 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 427287 Color: 0
Size: 307596 Color: 1
Size: 265118 Color: 2

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 427357 Color: 0
Size: 317071 Color: 3
Size: 255573 Color: 1

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 427476 Color: 2
Size: 301974 Color: 4
Size: 270551 Color: 1

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 427501 Color: 0
Size: 313565 Color: 3
Size: 258935 Color: 2

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 427526 Color: 1
Size: 299781 Color: 3
Size: 272694 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 427619 Color: 2
Size: 306497 Color: 1
Size: 265885 Color: 1

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 427704 Color: 3
Size: 316746 Color: 4
Size: 255551 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 427747 Color: 1
Size: 303657 Color: 0
Size: 268597 Color: 4

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 427835 Color: 2
Size: 286975 Color: 0
Size: 285191 Color: 3

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 427881 Color: 1
Size: 295680 Color: 3
Size: 276440 Color: 4

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 427882 Color: 2
Size: 286841 Color: 1
Size: 285278 Color: 1

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 427904 Color: 0
Size: 312001 Color: 3
Size: 260096 Color: 1

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 427912 Color: 4
Size: 301514 Color: 2
Size: 270575 Color: 2

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 427984 Color: 3
Size: 301694 Color: 2
Size: 270323 Color: 3

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 427944 Color: 4
Size: 291729 Color: 2
Size: 280328 Color: 2

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 427947 Color: 0
Size: 291896 Color: 4
Size: 280158 Color: 3

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 428011 Color: 3
Size: 305813 Color: 1
Size: 266177 Color: 1

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 427965 Color: 0
Size: 300001 Color: 0
Size: 272035 Color: 4

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 427992 Color: 1
Size: 318994 Color: 3
Size: 253015 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 428019 Color: 1
Size: 316258 Color: 2
Size: 255724 Color: 4

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 428063 Color: 4
Size: 296341 Color: 0
Size: 275597 Color: 3

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 428159 Color: 0
Size: 286859 Color: 0
Size: 284983 Color: 3

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 428267 Color: 1
Size: 306884 Color: 2
Size: 264850 Color: 4

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 428309 Color: 4
Size: 308609 Color: 2
Size: 263083 Color: 3

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 428334 Color: 3
Size: 306632 Color: 2
Size: 265035 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 428349 Color: 2
Size: 320711 Color: 1
Size: 250941 Color: 2

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 428369 Color: 1
Size: 321220 Color: 0
Size: 250412 Color: 3

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 428371 Color: 0
Size: 319961 Color: 1
Size: 251669 Color: 4

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 428379 Color: 0
Size: 296538 Color: 3
Size: 275084 Color: 1

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 428439 Color: 2
Size: 304839 Color: 1
Size: 266723 Color: 3

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 428474 Color: 4
Size: 290774 Color: 2
Size: 280753 Color: 2

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 428543 Color: 3
Size: 291569 Color: 2
Size: 279889 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 428500 Color: 1
Size: 310270 Color: 0
Size: 261231 Color: 4

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 428635 Color: 3
Size: 291855 Color: 1
Size: 279511 Color: 1

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 428814 Color: 3
Size: 316551 Color: 2
Size: 254636 Color: 2

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 428825 Color: 4
Size: 317793 Color: 0
Size: 253383 Color: 3

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 428911 Color: 1
Size: 309157 Color: 2
Size: 261933 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 428914 Color: 1
Size: 311963 Color: 3
Size: 259124 Color: 4

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 429039 Color: 2
Size: 288401 Color: 0
Size: 282561 Color: 4

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 429293 Color: 3
Size: 293524 Color: 4
Size: 277184 Color: 2

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 429265 Color: 0
Size: 302059 Color: 0
Size: 268677 Color: 1

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 429398 Color: 1
Size: 305277 Color: 4
Size: 265326 Color: 3

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 429492 Color: 2
Size: 314590 Color: 0
Size: 255919 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 429523 Color: 2
Size: 297102 Color: 3
Size: 273376 Color: 1

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 429548 Color: 3
Size: 299967 Color: 4
Size: 270486 Color: 1

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 429606 Color: 4
Size: 309124 Color: 4
Size: 261271 Color: 1

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 429666 Color: 1
Size: 301186 Color: 2
Size: 269149 Color: 3

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 429701 Color: 4
Size: 316569 Color: 1
Size: 253731 Color: 3

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 429716 Color: 1
Size: 308217 Color: 4
Size: 262068 Color: 2

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 429764 Color: 0
Size: 293282 Color: 4
Size: 276955 Color: 3

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 429797 Color: 4
Size: 298981 Color: 2
Size: 271223 Color: 2

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 429876 Color: 4
Size: 287733 Color: 0
Size: 282392 Color: 3

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 430006 Color: 2
Size: 312559 Color: 4
Size: 257436 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 430035 Color: 4
Size: 304812 Color: 0
Size: 265154 Color: 3

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 430122 Color: 1
Size: 310156 Color: 3
Size: 259723 Color: 2

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 430130 Color: 2
Size: 316840 Color: 4
Size: 253031 Color: 2

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 430134 Color: 0
Size: 319370 Color: 4
Size: 250497 Color: 2

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 430157 Color: 2
Size: 297604 Color: 0
Size: 272240 Color: 3

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 430213 Color: 3
Size: 317761 Color: 4
Size: 252027 Color: 4

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 430386 Color: 1
Size: 319403 Color: 1
Size: 250212 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 430411 Color: 1
Size: 316119 Color: 3
Size: 253471 Color: 2

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 430453 Color: 0
Size: 313409 Color: 4
Size: 256139 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 430529 Color: 4
Size: 309971 Color: 3
Size: 259501 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 430572 Color: 2
Size: 303629 Color: 4
Size: 265800 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 430604 Color: 2
Size: 308144 Color: 4
Size: 261253 Color: 3

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 430628 Color: 0
Size: 285430 Color: 3
Size: 283943 Color: 2

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 430672 Color: 4
Size: 305739 Color: 0
Size: 263590 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 430685 Color: 1
Size: 318418 Color: 1
Size: 250898 Color: 3

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 430786 Color: 0
Size: 314438 Color: 3
Size: 254777 Color: 4

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 430788 Color: 0
Size: 299439 Color: 3
Size: 269774 Color: 2

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 430800 Color: 0
Size: 303685 Color: 2
Size: 265516 Color: 2

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 430823 Color: 2
Size: 286390 Color: 3
Size: 282788 Color: 1

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 430873 Color: 1
Size: 297913 Color: 1
Size: 271215 Color: 3

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 430973 Color: 0
Size: 314629 Color: 1
Size: 254399 Color: 1

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 431013 Color: 2
Size: 300554 Color: 0
Size: 268434 Color: 3

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 431133 Color: 3
Size: 296123 Color: 2
Size: 272745 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 431150 Color: 2
Size: 290077 Color: 1
Size: 278774 Color: 1

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 431177 Color: 4
Size: 302027 Color: 0
Size: 266797 Color: 3

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 431144 Color: 3
Size: 307742 Color: 4
Size: 261115 Color: 4

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 431286 Color: 1
Size: 290882 Color: 2
Size: 277833 Color: 1

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 431359 Color: 3
Size: 307903 Color: 4
Size: 260739 Color: 2

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 431324 Color: 1
Size: 301761 Color: 4
Size: 266916 Color: 2

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 431366 Color: 1
Size: 297479 Color: 2
Size: 271156 Color: 3

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 431390 Color: 3
Size: 316151 Color: 0
Size: 252460 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 431379 Color: 0
Size: 289480 Color: 4
Size: 279142 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 431462 Color: 0
Size: 297933 Color: 3
Size: 270606 Color: 1

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 431503 Color: 1
Size: 292570 Color: 4
Size: 275928 Color: 2

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 431513 Color: 2
Size: 301552 Color: 1
Size: 266936 Color: 3

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 431514 Color: 1
Size: 286042 Color: 0
Size: 282445 Color: 3

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 431547 Color: 4
Size: 293580 Color: 2
Size: 274874 Color: 1

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 431579 Color: 4
Size: 288098 Color: 3
Size: 280324 Color: 2

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 431553 Color: 3
Size: 293297 Color: 1
Size: 275151 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 431587 Color: 2
Size: 296052 Color: 1
Size: 272362 Color: 2

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 431593 Color: 4
Size: 286775 Color: 3
Size: 281633 Color: 2

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 431629 Color: 0
Size: 301816 Color: 2
Size: 266556 Color: 2

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 431747 Color: 1
Size: 293600 Color: 2
Size: 274654 Color: 3

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 431762 Color: 1
Size: 284131 Color: 0
Size: 284108 Color: 3

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 431772 Color: 0
Size: 294098 Color: 0
Size: 274131 Color: 4

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 431790 Color: 4
Size: 293169 Color: 3
Size: 275042 Color: 2

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 431835 Color: 2
Size: 304582 Color: 1
Size: 263584 Color: 3

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 431868 Color: 2
Size: 305082 Color: 4
Size: 263051 Color: 1

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 431917 Color: 2
Size: 308613 Color: 3
Size: 259471 Color: 4

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 431946 Color: 4
Size: 284061 Color: 2
Size: 283994 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 431973 Color: 4
Size: 285123 Color: 4
Size: 282905 Color: 3

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 431968 Color: 3
Size: 301124 Color: 2
Size: 266909 Color: 1

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 432161 Color: 2
Size: 289256 Color: 2
Size: 278584 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 432375 Color: 0
Size: 298184 Color: 1
Size: 269442 Color: 3

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 432391 Color: 2
Size: 288129 Color: 3
Size: 279481 Color: 1

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 432483 Color: 0
Size: 306923 Color: 3
Size: 260595 Color: 2

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 432505 Color: 3
Size: 313624 Color: 2
Size: 253872 Color: 2

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 432564 Color: 1
Size: 293935 Color: 2
Size: 273502 Color: 2

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 432633 Color: 0
Size: 308756 Color: 2
Size: 258612 Color: 3

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 432676 Color: 2
Size: 309518 Color: 3
Size: 257807 Color: 4

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 432677 Color: 1
Size: 308391 Color: 0
Size: 258933 Color: 3

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 432877 Color: 1
Size: 316736 Color: 4
Size: 250388 Color: 4

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 432889 Color: 4
Size: 308906 Color: 2
Size: 258206 Color: 3

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 432902 Color: 0
Size: 284197 Color: 1
Size: 282902 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 432948 Color: 3
Size: 308966 Color: 2
Size: 258087 Color: 1

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 432915 Color: 4
Size: 297309 Color: 0
Size: 269777 Color: 2

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 433066 Color: 3
Size: 295845 Color: 2
Size: 271090 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 432932 Color: 2
Size: 303159 Color: 0
Size: 263910 Color: 1

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 433169 Color: 3
Size: 296454 Color: 4
Size: 270378 Color: 1

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 433123 Color: 1
Size: 292597 Color: 3
Size: 274281 Color: 4

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 433199 Color: 3
Size: 291063 Color: 4
Size: 275739 Color: 1

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 433137 Color: 2
Size: 288380 Color: 2
Size: 278484 Color: 1

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 433272 Color: 3
Size: 285475 Color: 1
Size: 281254 Color: 2

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 433204 Color: 1
Size: 295994 Color: 2
Size: 270803 Color: 4

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 433354 Color: 1
Size: 297807 Color: 2
Size: 268840 Color: 3

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 433413 Color: 0
Size: 316206 Color: 4
Size: 250382 Color: 2

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 433517 Color: 3
Size: 285958 Color: 2
Size: 280526 Color: 2

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 433484 Color: 1
Size: 309352 Color: 0
Size: 257165 Color: 1

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 433484 Color: 1
Size: 311352 Color: 1
Size: 255165 Color: 3

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 433562 Color: 3
Size: 303467 Color: 0
Size: 262972 Color: 4

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 433534 Color: 4
Size: 310875 Color: 2
Size: 255592 Color: 4

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 433723 Color: 3
Size: 284244 Color: 2
Size: 282034 Color: 2

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 433787 Color: 3
Size: 283652 Color: 1
Size: 282562 Color: 1

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 433773 Color: 2
Size: 307092 Color: 4
Size: 259136 Color: 4

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 433884 Color: 1
Size: 305600 Color: 3
Size: 260517 Color: 4

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 433885 Color: 4
Size: 296300 Color: 0
Size: 269816 Color: 1

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 434033 Color: 3
Size: 306198 Color: 2
Size: 259770 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 433919 Color: 0
Size: 288263 Color: 2
Size: 277819 Color: 2

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 434055 Color: 4
Size: 299408 Color: 0
Size: 266538 Color: 3

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 434128 Color: 2
Size: 284630 Color: 1
Size: 281243 Color: 3

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 434158 Color: 2
Size: 296659 Color: 4
Size: 269184 Color: 4

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 434183 Color: 4
Size: 305813 Color: 0
Size: 260005 Color: 3

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 434235 Color: 4
Size: 307913 Color: 0
Size: 257853 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 434241 Color: 1
Size: 291494 Color: 4
Size: 274266 Color: 3

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 434298 Color: 2
Size: 286364 Color: 3
Size: 279339 Color: 3

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 434477 Color: 1
Size: 314578 Color: 3
Size: 250946 Color: 2

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 434513 Color: 4
Size: 292278 Color: 2
Size: 273210 Color: 4

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 434559 Color: 1
Size: 289162 Color: 3
Size: 276280 Color: 4

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 434596 Color: 1
Size: 288602 Color: 2
Size: 276803 Color: 3

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 434653 Color: 2
Size: 311489 Color: 4
Size: 253859 Color: 2

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 434695 Color: 1
Size: 286172 Color: 1
Size: 279134 Color: 3

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 434898 Color: 4
Size: 299222 Color: 2
Size: 265881 Color: 2

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 434977 Color: 3
Size: 299098 Color: 1
Size: 265926 Color: 4

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 434970 Color: 1
Size: 288717 Color: 4
Size: 276314 Color: 2

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 435070 Color: 1
Size: 312867 Color: 2
Size: 252064 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 435181 Color: 4
Size: 303578 Color: 3
Size: 261242 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 435142 Color: 3
Size: 307497 Color: 0
Size: 257362 Color: 1

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 435195 Color: 4
Size: 302522 Color: 0
Size: 262284 Color: 4

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 435431 Color: 2
Size: 290583 Color: 4
Size: 273987 Color: 3

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 435386 Color: 3
Size: 301300 Color: 2
Size: 263315 Color: 2

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 435456 Color: 2
Size: 308529 Color: 1
Size: 256016 Color: 4

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 435632 Color: 3
Size: 286481 Color: 0
Size: 277888 Color: 1

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 435595 Color: 0
Size: 309194 Color: 2
Size: 255212 Color: 4

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 435652 Color: 0
Size: 289295 Color: 3
Size: 275054 Color: 1

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 435721 Color: 3
Size: 296712 Color: 1
Size: 267568 Color: 4

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 435717 Color: 2
Size: 303257 Color: 0
Size: 261027 Color: 1

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 435734 Color: 4
Size: 308932 Color: 1
Size: 255335 Color: 3

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 435854 Color: 2
Size: 290571 Color: 0
Size: 273576 Color: 2

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 435887 Color: 0
Size: 296191 Color: 0
Size: 267923 Color: 3

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 436031 Color: 0
Size: 287487 Color: 1
Size: 276483 Color: 3

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 436141 Color: 4
Size: 304414 Color: 2
Size: 259446 Color: 4

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 436304 Color: 3
Size: 298425 Color: 2
Size: 265272 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 436212 Color: 0
Size: 300109 Color: 0
Size: 263680 Color: 2

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 436414 Color: 3
Size: 297331 Color: 1
Size: 266256 Color: 4

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 436301 Color: 1
Size: 292375 Color: 0
Size: 271325 Color: 4

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 436477 Color: 3
Size: 297087 Color: 1
Size: 266437 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 436491 Color: 4
Size: 286961 Color: 4
Size: 276549 Color: 3

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 436597 Color: 2
Size: 306655 Color: 2
Size: 256749 Color: 3

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 436690 Color: 1
Size: 284804 Color: 4
Size: 278507 Color: 2

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 436702 Color: 0
Size: 302372 Color: 4
Size: 260927 Color: 3

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 436763 Color: 1
Size: 295454 Color: 2
Size: 267784 Color: 2

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 436817 Color: 0
Size: 289748 Color: 3
Size: 273436 Color: 4

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 436828 Color: 4
Size: 306689 Color: 4
Size: 256484 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 436940 Color: 4
Size: 300039 Color: 3
Size: 263022 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 436948 Color: 2
Size: 307362 Color: 4
Size: 255691 Color: 1

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 436958 Color: 1
Size: 300298 Color: 3
Size: 262745 Color: 4

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 436990 Color: 1
Size: 293229 Color: 0
Size: 269782 Color: 3

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 436992 Color: 4
Size: 303106 Color: 2
Size: 259903 Color: 4

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 437164 Color: 3
Size: 294318 Color: 0
Size: 268519 Color: 4

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 437175 Color: 2
Size: 307212 Color: 2
Size: 255614 Color: 1

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 437197 Color: 1
Size: 308386 Color: 4
Size: 254418 Color: 3

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 437243 Color: 4
Size: 289493 Color: 2
Size: 273265 Color: 1

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 437265 Color: 2
Size: 286372 Color: 4
Size: 276364 Color: 3

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 437309 Color: 1
Size: 296306 Color: 3
Size: 266386 Color: 4

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 437462 Color: 1
Size: 310487 Color: 0
Size: 252052 Color: 4

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 437480 Color: 0
Size: 299553 Color: 3
Size: 262968 Color: 4

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 437509 Color: 0
Size: 286818 Color: 1
Size: 275674 Color: 1

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 437568 Color: 2
Size: 289415 Color: 2
Size: 273018 Color: 4

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 437748 Color: 0
Size: 302414 Color: 3
Size: 259839 Color: 1

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 437802 Color: 0
Size: 307698 Color: 4
Size: 254501 Color: 4

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 437842 Color: 0
Size: 298329 Color: 1
Size: 263830 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 438047 Color: 3
Size: 311211 Color: 4
Size: 250743 Color: 1

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 437996 Color: 1
Size: 292785 Color: 2
Size: 269220 Color: 2

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 438094 Color: 3
Size: 287754 Color: 4
Size: 274153 Color: 2

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 438088 Color: 2
Size: 296223 Color: 0
Size: 265690 Color: 2

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 438264 Color: 3
Size: 305863 Color: 1
Size: 255874 Color: 2

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 438093 Color: 0
Size: 306775 Color: 1
Size: 255133 Color: 4

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 438292 Color: 3
Size: 307584 Color: 4
Size: 254125 Color: 4

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 438220 Color: 2
Size: 293745 Color: 1
Size: 268036 Color: 4

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 438424 Color: 3
Size: 304548 Color: 1
Size: 257029 Color: 2

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 438396 Color: 1
Size: 303036 Color: 0
Size: 258569 Color: 3

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 438441 Color: 3
Size: 285069 Color: 0
Size: 276491 Color: 1

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 438465 Color: 4
Size: 306586 Color: 1
Size: 254950 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 438522 Color: 2
Size: 286012 Color: 3
Size: 275467 Color: 2

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 438541 Color: 0
Size: 291465 Color: 2
Size: 269995 Color: 4

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 438631 Color: 3
Size: 281301 Color: 1
Size: 280069 Color: 4

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 438630 Color: 4
Size: 289236 Color: 0
Size: 272135 Color: 2

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 438647 Color: 0
Size: 293754 Color: 1
Size: 267600 Color: 3

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 438733 Color: 3
Size: 282833 Color: 0
Size: 278435 Color: 2

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 438651 Color: 1
Size: 296593 Color: 2
Size: 264757 Color: 4

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 438779 Color: 4
Size: 300456 Color: 0
Size: 260766 Color: 1

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 438877 Color: 3
Size: 291170 Color: 0
Size: 269954 Color: 4

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 438942 Color: 3
Size: 282016 Color: 0
Size: 279043 Color: 4

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 438811 Color: 1
Size: 295197 Color: 0
Size: 265993 Color: 1

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 438960 Color: 3
Size: 280535 Color: 1
Size: 280506 Color: 2

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 438981 Color: 0
Size: 284847 Color: 3
Size: 276173 Color: 1

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 439028 Color: 0
Size: 309522 Color: 1
Size: 251451 Color: 4

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 439068 Color: 1
Size: 303216 Color: 1
Size: 257717 Color: 3

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 439147 Color: 1
Size: 282778 Color: 4
Size: 278076 Color: 3

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 439170 Color: 0
Size: 307815 Color: 2
Size: 253016 Color: 1

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 439224 Color: 2
Size: 293017 Color: 3
Size: 267760 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 439226 Color: 0
Size: 292168 Color: 1
Size: 268607 Color: 1

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 439285 Color: 1
Size: 307970 Color: 3
Size: 252746 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 439366 Color: 3
Size: 281697 Color: 0
Size: 278938 Color: 2

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 439342 Color: 1
Size: 303244 Color: 1
Size: 257415 Color: 4

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 439524 Color: 4
Size: 287285 Color: 2
Size: 273192 Color: 2

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 439611 Color: 3
Size: 305562 Color: 1
Size: 254828 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 439670 Color: 4
Size: 305068 Color: 2
Size: 255263 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 439693 Color: 4
Size: 305043 Color: 1
Size: 255265 Color: 3

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 439871 Color: 1
Size: 290534 Color: 2
Size: 269596 Color: 3

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 440149 Color: 4
Size: 298994 Color: 0
Size: 260858 Color: 3

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 440184 Color: 3
Size: 296606 Color: 2
Size: 263211 Color: 4

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 440158 Color: 0
Size: 308277 Color: 1
Size: 251566 Color: 2

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 440188 Color: 0
Size: 306027 Color: 2
Size: 253786 Color: 3

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 440252 Color: 2
Size: 281049 Color: 3
Size: 278700 Color: 0

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 440381 Color: 3
Size: 288375 Color: 2
Size: 271245 Color: 2

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 440310 Color: 4
Size: 293859 Color: 1
Size: 265832 Color: 4

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 440418 Color: 4
Size: 306450 Color: 0
Size: 253133 Color: 3

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 440497 Color: 4
Size: 305022 Color: 0
Size: 254482 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 440518 Color: 0
Size: 293907 Color: 1
Size: 265576 Color: 3

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 440564 Color: 1
Size: 297484 Color: 1
Size: 261953 Color: 3

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 440883 Color: 0
Size: 301931 Color: 4
Size: 257187 Color: 2

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 440887 Color: 0
Size: 293080 Color: 3
Size: 266034 Color: 2

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 440927 Color: 0
Size: 287273 Color: 0
Size: 271801 Color: 2

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 441045 Color: 1
Size: 287383 Color: 3
Size: 271573 Color: 4

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 441064 Color: 4
Size: 287187 Color: 4
Size: 271750 Color: 2

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 441130 Color: 2
Size: 289316 Color: 3
Size: 269555 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 441219 Color: 0
Size: 305981 Color: 2
Size: 252801 Color: 4

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 441241 Color: 0
Size: 279924 Color: 3
Size: 278836 Color: 1

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 441307 Color: 1
Size: 298014 Color: 4
Size: 260680 Color: 2

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 441352 Color: 4
Size: 302080 Color: 3
Size: 256569 Color: 2

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 441473 Color: 0
Size: 286050 Color: 1
Size: 272478 Color: 2

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 441476 Color: 1
Size: 293324 Color: 3
Size: 265201 Color: 2

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 441527 Color: 3
Size: 299049 Color: 4
Size: 259425 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 441501 Color: 2
Size: 304868 Color: 4
Size: 253632 Color: 2

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 441566 Color: 0
Size: 288973 Color: 4
Size: 269462 Color: 3

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 441601 Color: 2
Size: 301036 Color: 0
Size: 257364 Color: 4

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 441605 Color: 0
Size: 280804 Color: 3
Size: 277592 Color: 4

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 441644 Color: 4
Size: 292648 Color: 2
Size: 265709 Color: 3

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 441664 Color: 1
Size: 305244 Color: 2
Size: 253093 Color: 1

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 441696 Color: 2
Size: 303410 Color: 0
Size: 254895 Color: 3

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 441733 Color: 2
Size: 288561 Color: 4
Size: 269707 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 441925 Color: 3
Size: 305963 Color: 0
Size: 252113 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 441765 Color: 0
Size: 287944 Color: 2
Size: 270292 Color: 4

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 442008 Color: 3
Size: 307371 Color: 0
Size: 250622 Color: 1

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 442165 Color: 1
Size: 286687 Color: 1
Size: 271149 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 442214 Color: 1
Size: 285726 Color: 2
Size: 272061 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 442226 Color: 1
Size: 292942 Color: 3
Size: 264833 Color: 2

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 442225 Color: 3
Size: 305811 Color: 4
Size: 251965 Color: 1

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 442227 Color: 2
Size: 287418 Color: 1
Size: 270356 Color: 4

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 442250 Color: 2
Size: 292187 Color: 4
Size: 265564 Color: 3

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 442238 Color: 3
Size: 288386 Color: 0
Size: 269377 Color: 2

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 442260 Color: 2
Size: 295108 Color: 0
Size: 262633 Color: 2

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 442308 Color: 3
Size: 294511 Color: 1
Size: 263182 Color: 2

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 442462 Color: 4
Size: 307095 Color: 0
Size: 250444 Color: 2

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 442688 Color: 1
Size: 305156 Color: 0
Size: 252157 Color: 3

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 442799 Color: 3
Size: 283154 Color: 0
Size: 274048 Color: 4

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 442768 Color: 1
Size: 278766 Color: 4
Size: 278467 Color: 2

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 442819 Color: 2
Size: 287710 Color: 3
Size: 269472 Color: 1

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 442879 Color: 2
Size: 289886 Color: 4
Size: 267236 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 442885 Color: 2
Size: 300726 Color: 3
Size: 256390 Color: 4

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 442885 Color: 2
Size: 290737 Color: 3
Size: 266379 Color: 1

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 442946 Color: 2
Size: 296939 Color: 4
Size: 260116 Color: 3

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 443092 Color: 1
Size: 282734 Color: 0
Size: 274175 Color: 3

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 443122 Color: 2
Size: 279506 Color: 1
Size: 277373 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 443233 Color: 1
Size: 298834 Color: 3
Size: 257934 Color: 4

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 443299 Color: 0
Size: 286379 Color: 2
Size: 270323 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 443337 Color: 3
Size: 291783 Color: 4
Size: 264881 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 443320 Color: 2
Size: 278816 Color: 0
Size: 277865 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 443414 Color: 0
Size: 280649 Color: 3
Size: 275938 Color: 4

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 443379 Color: 3
Size: 302941 Color: 4
Size: 253681 Color: 4

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 443470 Color: 2
Size: 280537 Color: 2
Size: 275994 Color: 4

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 443517 Color: 4
Size: 280786 Color: 3
Size: 275698 Color: 2

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 443522 Color: 1
Size: 293106 Color: 1
Size: 263373 Color: 4

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 443543 Color: 2
Size: 292696 Color: 4
Size: 263762 Color: 3

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 443653 Color: 1
Size: 301757 Color: 3
Size: 254591 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 443665 Color: 1
Size: 293268 Color: 2
Size: 263068 Color: 2

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 443839 Color: 2
Size: 293070 Color: 1
Size: 263092 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 443889 Color: 1
Size: 294617 Color: 3
Size: 261495 Color: 2

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 443953 Color: 3
Size: 305519 Color: 1
Size: 250529 Color: 1

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 443892 Color: 4
Size: 292217 Color: 1
Size: 263892 Color: 2

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 444066 Color: 3
Size: 300389 Color: 2
Size: 255546 Color: 2

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 444068 Color: 3
Size: 280307 Color: 2
Size: 275626 Color: 4

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 444070 Color: 2
Size: 292834 Color: 1
Size: 263097 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 444071 Color: 2
Size: 300915 Color: 3
Size: 255015 Color: 1

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 444074 Color: 2
Size: 305808 Color: 4
Size: 250119 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 444125 Color: 4
Size: 278307 Color: 3
Size: 277569 Color: 4

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 444230 Color: 4
Size: 290825 Color: 4
Size: 264946 Color: 3

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 444248 Color: 2
Size: 282528 Color: 1
Size: 273225 Color: 4

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 444351 Color: 3
Size: 296599 Color: 1
Size: 259051 Color: 4

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 444478 Color: 1
Size: 278416 Color: 1
Size: 277107 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 420402 Color: 1
Size: 311660 Color: 1
Size: 267939 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 444534 Color: 0
Size: 297162 Color: 0
Size: 258305 Color: 1

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 444583 Color: 4
Size: 277767 Color: 3
Size: 277651 Color: 2

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 444616 Color: 3
Size: 303008 Color: 1
Size: 252377 Color: 1

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 444593 Color: 4
Size: 279793 Color: 1
Size: 275615 Color: 0

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 444691 Color: 3
Size: 291326 Color: 4
Size: 263984 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 444718 Color: 1
Size: 280307 Color: 4
Size: 274976 Color: 4

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 445004 Color: 3
Size: 278461 Color: 0
Size: 276536 Color: 1

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 445047 Color: 4
Size: 300497 Color: 3
Size: 254457 Color: 2

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 445379 Color: 1
Size: 285878 Color: 3
Size: 268744 Color: 4

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 445417 Color: 4
Size: 303759 Color: 0
Size: 250825 Color: 2

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 445443 Color: 2
Size: 298459 Color: 2
Size: 256099 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 445445 Color: 4
Size: 295882 Color: 3
Size: 258674 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 445459 Color: 1
Size: 303804 Color: 3
Size: 250738 Color: 1

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 445492 Color: 1
Size: 289361 Color: 0
Size: 265148 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 445500 Color: 2
Size: 300469 Color: 4
Size: 254032 Color: 4

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 445571 Color: 3
Size: 287043 Color: 2
Size: 267387 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 445545 Color: 2
Size: 280257 Color: 2
Size: 274199 Color: 4

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 445576 Color: 3
Size: 278832 Color: 2
Size: 275593 Color: 1

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 445587 Color: 2
Size: 288660 Color: 0
Size: 265754 Color: 4

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 445607 Color: 2
Size: 300999 Color: 3
Size: 253395 Color: 4

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 445701 Color: 4
Size: 300944 Color: 3
Size: 253356 Color: 0

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 445800 Color: 4
Size: 278655 Color: 1
Size: 275546 Color: 1

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 445804 Color: 0
Size: 282273 Color: 1
Size: 271924 Color: 1

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 445983 Color: 3
Size: 283999 Color: 4
Size: 270019 Color: 1

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 445894 Color: 2
Size: 280147 Color: 0
Size: 273960 Color: 1

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 446088 Color: 4
Size: 283858 Color: 2
Size: 270055 Color: 3

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 446089 Color: 3
Size: 292694 Color: 1
Size: 261218 Color: 2

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 446157 Color: 3
Size: 288838 Color: 0
Size: 265006 Color: 1

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 446093 Color: 4
Size: 277043 Color: 2
Size: 276865 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 446112 Color: 1
Size: 290854 Color: 3
Size: 263035 Color: 4

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 446241 Color: 3
Size: 280487 Color: 2
Size: 273273 Color: 1

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 446157 Color: 1
Size: 277671 Color: 0
Size: 276173 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 446239 Color: 0
Size: 293802 Color: 1
Size: 259960 Color: 3

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 446402 Color: 1
Size: 301460 Color: 4
Size: 252139 Color: 3

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 446419 Color: 4
Size: 286506 Color: 2
Size: 267076 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 446536 Color: 4
Size: 287811 Color: 2
Size: 265654 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 446554 Color: 2
Size: 277109 Color: 3
Size: 276338 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 446666 Color: 4
Size: 282076 Color: 2
Size: 271259 Color: 2

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 446766 Color: 0
Size: 291277 Color: 3
Size: 261958 Color: 2

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 446825 Color: 1
Size: 294829 Color: 4
Size: 258347 Color: 1

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 446952 Color: 3
Size: 297765 Color: 1
Size: 255284 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 446853 Color: 0
Size: 280737 Color: 1
Size: 272411 Color: 4

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 446982 Color: 2
Size: 284270 Color: 0
Size: 268749 Color: 3

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 446985 Color: 4
Size: 292271 Color: 3
Size: 260745 Color: 2

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 447104 Color: 3
Size: 288153 Color: 4
Size: 264744 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 447176 Color: 0
Size: 282745 Color: 3
Size: 270080 Color: 1

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 447290 Color: 1
Size: 295860 Color: 3
Size: 256851 Color: 4

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 447346 Color: 2
Size: 286287 Color: 1
Size: 266368 Color: 4

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 447365 Color: 4
Size: 293207 Color: 1
Size: 259429 Color: 3

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 447498 Color: 3
Size: 299945 Color: 2
Size: 252558 Color: 2

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 447558 Color: 3
Size: 281099 Color: 2
Size: 271344 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 447401 Color: 1
Size: 297788 Color: 2
Size: 254812 Color: 1

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 447641 Color: 3
Size: 277410 Color: 2
Size: 274950 Color: 1

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 447531 Color: 4
Size: 301890 Color: 2
Size: 250580 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 447658 Color: 3
Size: 296778 Color: 2
Size: 255565 Color: 2

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 447620 Color: 1
Size: 298600 Color: 1
Size: 253781 Color: 4

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 447868 Color: 3
Size: 296316 Color: 0
Size: 255817 Color: 1

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 447780 Color: 4
Size: 289297 Color: 2
Size: 262924 Color: 4

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 447849 Color: 0
Size: 279368 Color: 1
Size: 272784 Color: 3

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 447981 Color: 3
Size: 279040 Color: 4
Size: 272980 Color: 1

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 447857 Color: 1
Size: 282391 Color: 0
Size: 269753 Color: 4

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 447968 Color: 0
Size: 281470 Color: 1
Size: 270563 Color: 3

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 448033 Color: 3
Size: 281039 Color: 2
Size: 270929 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 448011 Color: 1
Size: 291599 Color: 4
Size: 260391 Color: 2

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 448129 Color: 3
Size: 291578 Color: 0
Size: 260294 Color: 2

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 448016 Color: 0
Size: 282232 Color: 4
Size: 269753 Color: 2

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 448100 Color: 1
Size: 278520 Color: 3
Size: 273381 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 448168 Color: 3
Size: 294445 Color: 1
Size: 257388 Color: 2

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 448165 Color: 4
Size: 285358 Color: 1
Size: 266478 Color: 1

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 448184 Color: 1
Size: 278690 Color: 3
Size: 273127 Color: 2

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 448238 Color: 2
Size: 284712 Color: 4
Size: 267051 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 448291 Color: 1
Size: 290415 Color: 0
Size: 261295 Color: 2

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 448324 Color: 4
Size: 284201 Color: 2
Size: 267476 Color: 3

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 448334 Color: 4
Size: 298192 Color: 2
Size: 253475 Color: 3

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 448489 Color: 2
Size: 280022 Color: 1
Size: 271490 Color: 4

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 448503 Color: 2
Size: 290636 Color: 3
Size: 260862 Color: 4

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 448532 Color: 1
Size: 297152 Color: 2
Size: 254317 Color: 2

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 448795 Color: 3
Size: 284155 Color: 1
Size: 267051 Color: 3

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 448816 Color: 3
Size: 287656 Color: 2
Size: 263529 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 448771 Color: 2
Size: 286315 Color: 4
Size: 264915 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 448833 Color: 3
Size: 289241 Color: 0
Size: 261927 Color: 2

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 448841 Color: 2
Size: 298877 Color: 1
Size: 252283 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 448957 Color: 3
Size: 288103 Color: 0
Size: 262941 Color: 1

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 448872 Color: 0
Size: 277565 Color: 2
Size: 273564 Color: 2

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 448997 Color: 1
Size: 279452 Color: 3
Size: 271552 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 449141 Color: 3
Size: 290780 Color: 2
Size: 260080 Color: 4

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 449135 Color: 1
Size: 299379 Color: 4
Size: 251487 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 449310 Color: 3
Size: 299309 Color: 2
Size: 251382 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 449384 Color: 4
Size: 280153 Color: 4
Size: 270464 Color: 3

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 449406 Color: 0
Size: 297178 Color: 3
Size: 253417 Color: 4

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 449459 Color: 2
Size: 300531 Color: 4
Size: 250011 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 449644 Color: 3
Size: 282927 Color: 1
Size: 267430 Color: 2

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 449533 Color: 2
Size: 278367 Color: 1
Size: 272101 Color: 2

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 449712 Color: 3
Size: 294210 Color: 0
Size: 256079 Color: 2

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 449546 Color: 0
Size: 277615 Color: 4
Size: 272840 Color: 1

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 449719 Color: 2
Size: 288971 Color: 3
Size: 261311 Color: 1

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 449733 Color: 4
Size: 298343 Color: 1
Size: 251925 Color: 1

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 449838 Color: 0
Size: 275135 Color: 4
Size: 275028 Color: 3

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 449909 Color: 0
Size: 275543 Color: 2
Size: 274549 Color: 3

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 450044 Color: 1
Size: 296483 Color: 0
Size: 253474 Color: 2

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 450213 Color: 3
Size: 276924 Color: 2
Size: 272864 Color: 1

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 450259 Color: 4
Size: 276924 Color: 0
Size: 272818 Color: 3

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 450311 Color: 1
Size: 299210 Color: 4
Size: 250480 Color: 3

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 450361 Color: 1
Size: 294352 Color: 0
Size: 255288 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 450366 Color: 4
Size: 294737 Color: 4
Size: 254898 Color: 3

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 450443 Color: 0
Size: 280292 Color: 3
Size: 269266 Color: 1

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 450449 Color: 4
Size: 285630 Color: 2
Size: 263922 Color: 1

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 450690 Color: 0
Size: 298095 Color: 3
Size: 251216 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 450773 Color: 0
Size: 276995 Color: 2
Size: 272233 Color: 4

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 450823 Color: 1
Size: 276721 Color: 3
Size: 272457 Color: 2

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 450955 Color: 2
Size: 295937 Color: 1
Size: 253109 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 451021 Color: 3
Size: 276542 Color: 2
Size: 272438 Color: 1

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 450957 Color: 4
Size: 279991 Color: 1
Size: 269053 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 451180 Color: 3
Size: 292855 Color: 4
Size: 255966 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 450995 Color: 4
Size: 289875 Color: 0
Size: 259131 Color: 2

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 451299 Color: 3
Size: 287032 Color: 0
Size: 261670 Color: 4

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 451363 Color: 1
Size: 292259 Color: 4
Size: 256379 Color: 3

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 451426 Color: 0
Size: 275498 Color: 3
Size: 273077 Color: 1

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 451451 Color: 0
Size: 293456 Color: 2
Size: 255094 Color: 3

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 451494 Color: 3
Size: 280508 Color: 2
Size: 267999 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 451543 Color: 0
Size: 285773 Color: 4
Size: 262685 Color: 2

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 451590 Color: 3
Size: 287641 Color: 0
Size: 260770 Color: 1

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 451639 Color: 1
Size: 296616 Color: 4
Size: 251746 Color: 4

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 451657 Color: 1
Size: 288670 Color: 1
Size: 259674 Color: 3

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 451691 Color: 3
Size: 290594 Color: 1
Size: 257716 Color: 1

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 451680 Color: 4
Size: 296913 Color: 4
Size: 251408 Color: 1

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 451684 Color: 0
Size: 276109 Color: 2
Size: 272208 Color: 3

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 451693 Color: 0
Size: 290064 Color: 1
Size: 258244 Color: 4

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 451696 Color: 2
Size: 292496 Color: 0
Size: 255809 Color: 3

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 451762 Color: 0
Size: 292182 Color: 2
Size: 256057 Color: 3

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 451769 Color: 0
Size: 281271 Color: 2
Size: 266961 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 451860 Color: 3
Size: 279240 Color: 4
Size: 268901 Color: 2

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 451824 Color: 2
Size: 279811 Color: 1
Size: 268366 Color: 4

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 451928 Color: 0
Size: 295550 Color: 3
Size: 252523 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 451931 Color: 0
Size: 280435 Color: 2
Size: 267635 Color: 1

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 452005 Color: 3
Size: 277894 Color: 2
Size: 270102 Color: 1

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 452027 Color: 3
Size: 288859 Color: 1
Size: 259115 Color: 1

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 452065 Color: 4
Size: 287955 Color: 1
Size: 259981 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 452097 Color: 2
Size: 290191 Color: 2
Size: 257713 Color: 4

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 452310 Color: 1
Size: 278440 Color: 4
Size: 269251 Color: 3

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 452492 Color: 4
Size: 280895 Color: 2
Size: 266614 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 452508 Color: 0
Size: 295357 Color: 3
Size: 252136 Color: 1

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 452662 Color: 4
Size: 293097 Color: 0
Size: 254242 Color: 3

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 452724 Color: 2
Size: 279257 Color: 2
Size: 268020 Color: 4

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 452770 Color: 1
Size: 283615 Color: 2
Size: 263616 Color: 3

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 452790 Color: 0
Size: 291125 Color: 1
Size: 256086 Color: 2

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 452791 Color: 2
Size: 274135 Color: 3
Size: 273075 Color: 1

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 452936 Color: 0
Size: 294569 Color: 1
Size: 252496 Color: 3

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 452907 Color: 3
Size: 292448 Color: 1
Size: 254646 Color: 1

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 452940 Color: 4
Size: 280918 Color: 0
Size: 266143 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 453048 Color: 3
Size: 280315 Color: 0
Size: 266638 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 453099 Color: 4
Size: 280241 Color: 2
Size: 266661 Color: 2

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 453142 Color: 4
Size: 290420 Color: 3
Size: 256439 Color: 1

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 453250 Color: 4
Size: 280384 Color: 0
Size: 266367 Color: 3

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 453294 Color: 2
Size: 281604 Color: 4
Size: 265103 Color: 4

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 453323 Color: 2
Size: 286253 Color: 1
Size: 260425 Color: 3

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 453471 Color: 2
Size: 288559 Color: 0
Size: 257971 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 453589 Color: 1
Size: 276920 Color: 4
Size: 269492 Color: 3

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 453667 Color: 4
Size: 278299 Color: 3
Size: 268035 Color: 1

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 453903 Color: 2
Size: 279632 Color: 4
Size: 266466 Color: 3

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 453928 Color: 0
Size: 288148 Color: 1
Size: 257925 Color: 2

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 453978 Color: 2
Size: 288900 Color: 4
Size: 257123 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 454088 Color: 4
Size: 292653 Color: 3
Size: 253260 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 454226 Color: 4
Size: 286640 Color: 2
Size: 259135 Color: 3

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 454257 Color: 4
Size: 273495 Color: 1
Size: 272249 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 454314 Color: 4
Size: 295187 Color: 0
Size: 250500 Color: 3

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 454402 Color: 2
Size: 280808 Color: 1
Size: 264791 Color: 4

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 454645 Color: 3
Size: 293627 Color: 0
Size: 251729 Color: 1

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 454655 Color: 3
Size: 294445 Color: 1
Size: 250901 Color: 1

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 454584 Color: 4
Size: 273506 Color: 2
Size: 271911 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 454703 Color: 2
Size: 290167 Color: 3
Size: 255131 Color: 4

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 454822 Color: 3
Size: 277614 Color: 1
Size: 267565 Color: 2

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 454798 Color: 2
Size: 282474 Color: 4
Size: 262729 Color: 1

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 454915 Color: 4
Size: 278868 Color: 4
Size: 266218 Color: 3

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 454990 Color: 4
Size: 281581 Color: 3
Size: 263430 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 455064 Color: 0
Size: 285970 Color: 4
Size: 258967 Color: 3

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 455266 Color: 4
Size: 293183 Color: 0
Size: 251552 Color: 2

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 455469 Color: 2
Size: 293352 Color: 3
Size: 251180 Color: 4

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 455524 Color: 1
Size: 289834 Color: 4
Size: 254643 Color: 4

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 455555 Color: 4
Size: 286047 Color: 2
Size: 258399 Color: 1

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 455651 Color: 3
Size: 277862 Color: 0
Size: 266488 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 455679 Color: 4
Size: 292703 Color: 1
Size: 251619 Color: 2

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 455718 Color: 2
Size: 277823 Color: 3
Size: 266460 Color: 1

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 455745 Color: 1
Size: 283115 Color: 2
Size: 261141 Color: 3

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 455772 Color: 4
Size: 292465 Color: 0
Size: 251764 Color: 2

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 455867 Color: 3
Size: 284141 Color: 2
Size: 259993 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 455818 Color: 1
Size: 287537 Color: 1
Size: 256646 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 455895 Color: 3
Size: 275207 Color: 4
Size: 268899 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 455909 Color: 2
Size: 276370 Color: 4
Size: 267722 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 455934 Color: 1
Size: 281829 Color: 2
Size: 262238 Color: 3

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 455961 Color: 2
Size: 280242 Color: 1
Size: 263798 Color: 3

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 455967 Color: 0
Size: 276944 Color: 1
Size: 267090 Color: 4

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 456171 Color: 3
Size: 277684 Color: 1
Size: 266146 Color: 1

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 456080 Color: 1
Size: 287749 Color: 4
Size: 256172 Color: 4

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 456199 Color: 3
Size: 280628 Color: 1
Size: 263174 Color: 4

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 456232 Color: 2
Size: 281739 Color: 4
Size: 262030 Color: 1

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 456255 Color: 1
Size: 292836 Color: 2
Size: 250910 Color: 3

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 456292 Color: 0
Size: 280524 Color: 1
Size: 263185 Color: 2

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 456428 Color: 0
Size: 282247 Color: 4
Size: 261326 Color: 3

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 456444 Color: 4
Size: 284682 Color: 3
Size: 258875 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 456595 Color: 4
Size: 281129 Color: 4
Size: 262277 Color: 1

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 456684 Color: 2
Size: 285202 Color: 1
Size: 258115 Color: 3

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 456710 Color: 1
Size: 275282 Color: 1
Size: 268009 Color: 3

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 456739 Color: 3
Size: 293006 Color: 1
Size: 250256 Color: 1

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 456714 Color: 0
Size: 289786 Color: 2
Size: 253501 Color: 2

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 456818 Color: 3
Size: 282670 Color: 0
Size: 260513 Color: 1

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 456835 Color: 3
Size: 290080 Color: 2
Size: 253086 Color: 2

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 456791 Color: 4
Size: 289930 Color: 1
Size: 253280 Color: 1

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 457218 Color: 3
Size: 273250 Color: 0
Size: 269533 Color: 4

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 457238 Color: 3
Size: 282653 Color: 0
Size: 260110 Color: 2

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 457225 Color: 0
Size: 280225 Color: 1
Size: 262551 Color: 3

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 457364 Color: 0
Size: 284633 Color: 3
Size: 258004 Color: 1

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 457375 Color: 1
Size: 280540 Color: 4
Size: 262086 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 457408 Color: 0
Size: 282835 Color: 3
Size: 259758 Color: 2

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 457553 Color: 0
Size: 291998 Color: 4
Size: 250450 Color: 4

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 457649 Color: 2
Size: 283166 Color: 3
Size: 259186 Color: 1

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 457727 Color: 0
Size: 275316 Color: 1
Size: 266958 Color: 3

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 457763 Color: 1
Size: 271745 Color: 2
Size: 270493 Color: 4

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 457820 Color: 2
Size: 275825 Color: 3
Size: 266356 Color: 1

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 457831 Color: 4
Size: 274056 Color: 3
Size: 268114 Color: 4

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 457836 Color: 0
Size: 281278 Color: 2
Size: 260887 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 457959 Color: 3
Size: 281683 Color: 4
Size: 260359 Color: 4

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 457936 Color: 2
Size: 288177 Color: 4
Size: 253888 Color: 1

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 458065 Color: 4
Size: 289092 Color: 2
Size: 252844 Color: 3

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 458213 Color: 3
Size: 285449 Color: 2
Size: 256339 Color: 2

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 458114 Color: 4
Size: 286553 Color: 0
Size: 255334 Color: 2

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 458241 Color: 4
Size: 277203 Color: 3
Size: 264557 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 458514 Color: 4
Size: 274794 Color: 2
Size: 266693 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 458570 Color: 3
Size: 281153 Color: 2
Size: 260278 Color: 2

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 458540 Color: 2
Size: 270862 Color: 1
Size: 270599 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 458577 Color: 2
Size: 275081 Color: 4
Size: 266343 Color: 3

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 458659 Color: 3
Size: 288827 Color: 2
Size: 252515 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 458845 Color: 2
Size: 283625 Color: 4
Size: 257531 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 458982 Color: 3
Size: 288095 Color: 2
Size: 252924 Color: 1

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 458954 Color: 4
Size: 284922 Color: 0
Size: 256125 Color: 2

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 458960 Color: 0
Size: 271028 Color: 0
Size: 270013 Color: 3

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 459128 Color: 3
Size: 279169 Color: 2
Size: 261704 Color: 1

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 459010 Color: 0
Size: 284455 Color: 2
Size: 256536 Color: 2

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 459129 Color: 3
Size: 284918 Color: 0
Size: 255954 Color: 2

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 459180 Color: 0
Size: 288695 Color: 4
Size: 252126 Color: 3

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 459215 Color: 2
Size: 288313 Color: 1
Size: 252473 Color: 3

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 459258 Color: 2
Size: 275096 Color: 0
Size: 265647 Color: 3

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 459274 Color: 0
Size: 273663 Color: 4
Size: 267064 Color: 3

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 459341 Color: 4
Size: 277757 Color: 2
Size: 262903 Color: 2

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 459572 Color: 3
Size: 283944 Color: 2
Size: 256485 Color: 1

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 459456 Color: 2
Size: 285825 Color: 1
Size: 254720 Color: 1

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 459543 Color: 1
Size: 282028 Color: 3
Size: 258430 Color: 4

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 459672 Color: 3
Size: 284822 Color: 0
Size: 255507 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 459782 Color: 3
Size: 280761 Color: 0
Size: 259458 Color: 1

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 459866 Color: 2
Size: 279415 Color: 2
Size: 260720 Color: 3

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 459916 Color: 0
Size: 281957 Color: 2
Size: 258128 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 459993 Color: 4
Size: 272636 Color: 3
Size: 267372 Color: 2

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 460096 Color: 0
Size: 283324 Color: 2
Size: 256581 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 460106 Color: 0
Size: 275374 Color: 2
Size: 264521 Color: 3

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 460166 Color: 4
Size: 275437 Color: 3
Size: 264398 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 460309 Color: 0
Size: 272819 Color: 1
Size: 266873 Color: 4

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 460384 Color: 3
Size: 287965 Color: 1
Size: 251652 Color: 1

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 460411 Color: 0
Size: 289577 Color: 0
Size: 250013 Color: 1

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 460486 Color: 2
Size: 289450 Color: 2
Size: 250065 Color: 4

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 460497 Color: 2
Size: 272091 Color: 3
Size: 267413 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 460540 Color: 4
Size: 284347 Color: 4
Size: 255114 Color: 3

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 460572 Color: 0
Size: 275426 Color: 4
Size: 264003 Color: 3

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 460681 Color: 2
Size: 276565 Color: 0
Size: 262755 Color: 3

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 460754 Color: 2
Size: 288055 Color: 1
Size: 251192 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 460893 Color: 4
Size: 280150 Color: 0
Size: 258958 Color: 4

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 460977 Color: 2
Size: 277468 Color: 3
Size: 261556 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 460993 Color: 0
Size: 273437 Color: 1
Size: 265571 Color: 2

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 461035 Color: 4
Size: 273901 Color: 3
Size: 265065 Color: 2

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 461079 Color: 2
Size: 273707 Color: 3
Size: 265215 Color: 1

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 461079 Color: 1
Size: 279723 Color: 2
Size: 259199 Color: 4

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 461124 Color: 2
Size: 272330 Color: 3
Size: 266547 Color: 0

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 461186 Color: 3
Size: 282005 Color: 2
Size: 256810 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 461277 Color: 2
Size: 285540 Color: 2
Size: 253184 Color: 4

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 461310 Color: 3
Size: 282302 Color: 4
Size: 256389 Color: 2

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 461284 Color: 1
Size: 283501 Color: 0
Size: 255216 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 461322 Color: 4
Size: 272366 Color: 2
Size: 266313 Color: 3

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 461373 Color: 3
Size: 275243 Color: 1
Size: 263385 Color: 1

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 461329 Color: 4
Size: 278858 Color: 4
Size: 259814 Color: 1

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 461374 Color: 2
Size: 277568 Color: 0
Size: 261059 Color: 3

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 461438 Color: 1
Size: 283520 Color: 0
Size: 255043 Color: 4

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 461497 Color: 1
Size: 271756 Color: 2
Size: 266748 Color: 3

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 461586 Color: 4
Size: 272020 Color: 3
Size: 266395 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 461600 Color: 1
Size: 275182 Color: 2
Size: 263219 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 461607 Color: 1
Size: 286327 Color: 0
Size: 252067 Color: 3

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 461698 Color: 3
Size: 277892 Color: 0
Size: 260411 Color: 1

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 461806 Color: 4
Size: 280577 Color: 4
Size: 257618 Color: 2

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 462017 Color: 0
Size: 281646 Color: 2
Size: 256338 Color: 1

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 462211 Color: 4
Size: 274277 Color: 0
Size: 263513 Color: 3

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 462391 Color: 1
Size: 278662 Color: 0
Size: 258948 Color: 2

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 462450 Color: 4
Size: 283748 Color: 3
Size: 253803 Color: 2

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 462596 Color: 3
Size: 273053 Color: 4
Size: 264352 Color: 1

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 462587 Color: 2
Size: 284033 Color: 4
Size: 253381 Color: 2

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 462653 Color: 3
Size: 273505 Color: 0
Size: 263843 Color: 4

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 462638 Color: 4
Size: 280480 Color: 0
Size: 256883 Color: 4

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 462651 Color: 4
Size: 287256 Color: 3
Size: 250094 Color: 1

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 462903 Color: 4
Size: 282875 Color: 3
Size: 254223 Color: 2

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 462995 Color: 1
Size: 282481 Color: 4
Size: 254525 Color: 1

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 463035 Color: 3
Size: 284234 Color: 0
Size: 252732 Color: 4

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 463190 Color: 4
Size: 274369 Color: 0
Size: 262442 Color: 2

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 463282 Color: 1
Size: 273922 Color: 3
Size: 262797 Color: 4

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 463301 Color: 4
Size: 283283 Color: 3
Size: 253417 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 463407 Color: 4
Size: 278777 Color: 3
Size: 257817 Color: 4

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 463414 Color: 0
Size: 283160 Color: 1
Size: 253427 Color: 4

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 463448 Color: 3
Size: 281695 Color: 1
Size: 254858 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 463564 Color: 1
Size: 271653 Color: 4
Size: 264784 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 463632 Color: 4
Size: 276019 Color: 3
Size: 260350 Color: 0

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 463706 Color: 1
Size: 284534 Color: 4
Size: 251761 Color: 2

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 463937 Color: 4
Size: 278133 Color: 4
Size: 257931 Color: 3

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 464038 Color: 2
Size: 273990 Color: 2
Size: 261973 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 464090 Color: 3
Size: 282820 Color: 4
Size: 253091 Color: 1

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 464211 Color: 0
Size: 285558 Color: 4
Size: 250232 Color: 4

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 464245 Color: 0
Size: 276534 Color: 3
Size: 259222 Color: 2

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 464274 Color: 1
Size: 278877 Color: 0
Size: 256850 Color: 3

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 464310 Color: 0
Size: 273953 Color: 2
Size: 261738 Color: 1

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 464381 Color: 3
Size: 276902 Color: 2
Size: 258718 Color: 2

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 464397 Color: 2
Size: 281757 Color: 4
Size: 253847 Color: 1

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 464546 Color: 2
Size: 283942 Color: 0
Size: 251513 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 464694 Color: 3
Size: 270071 Color: 1
Size: 265236 Color: 1

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 464732 Color: 3
Size: 276619 Color: 0
Size: 258650 Color: 4

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 464788 Color: 1
Size: 275662 Color: 3
Size: 259551 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 464798 Color: 1
Size: 269494 Color: 0
Size: 265709 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 464831 Color: 2
Size: 280076 Color: 2
Size: 255094 Color: 1

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 464836 Color: 0
Size: 273474 Color: 0
Size: 261691 Color: 2

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 465019 Color: 3
Size: 271593 Color: 2
Size: 263389 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 464844 Color: 0
Size: 274519 Color: 2
Size: 260638 Color: 1

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 465136 Color: 3
Size: 281791 Color: 1
Size: 253074 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 465187 Color: 3
Size: 282270 Color: 2
Size: 252544 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 465278 Color: 4
Size: 284047 Color: 0
Size: 250676 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 465393 Color: 1
Size: 283244 Color: 1
Size: 251364 Color: 3

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 465398 Color: 4
Size: 278827 Color: 3
Size: 255776 Color: 4

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 465476 Color: 0
Size: 271840 Color: 1
Size: 262685 Color: 2

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 465548 Color: 3
Size: 273156 Color: 2
Size: 261297 Color: 4

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 465576 Color: 2
Size: 282980 Color: 4
Size: 251445 Color: 2

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 465646 Color: 2
Size: 274107 Color: 3
Size: 260248 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 465705 Color: 4
Size: 277860 Color: 0
Size: 256436 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 465912 Color: 0
Size: 281026 Color: 1
Size: 253063 Color: 3

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 466044 Color: 1
Size: 269159 Color: 2
Size: 264798 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 466045 Color: 2
Size: 269270 Color: 2
Size: 264686 Color: 1

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 466265 Color: 3
Size: 276152 Color: 1
Size: 257584 Color: 2

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 466307 Color: 1
Size: 280713 Color: 4
Size: 252981 Color: 3

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 466340 Color: 3
Size: 281732 Color: 1
Size: 251929 Color: 2

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 466374 Color: 3
Size: 278694 Color: 1
Size: 254933 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 466493 Color: 0
Size: 269316 Color: 4
Size: 264192 Color: 2

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 466637 Color: 2
Size: 267430 Color: 3
Size: 265934 Color: 4

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 466654 Color: 4
Size: 275518 Color: 4
Size: 257829 Color: 2

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 466892 Color: 3
Size: 281756 Color: 4
Size: 251353 Color: 2

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 466761 Color: 2
Size: 277373 Color: 4
Size: 255867 Color: 4

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 466914 Color: 3
Size: 278502 Color: 2
Size: 254585 Color: 4

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 467061 Color: 2
Size: 275126 Color: 2
Size: 257814 Color: 3

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 467159 Color: 0
Size: 276407 Color: 1
Size: 256435 Color: 4

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 467193 Color: 1
Size: 270591 Color: 1
Size: 262217 Color: 3

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 467366 Color: 0
Size: 279724 Color: 1
Size: 252911 Color: 2

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 467433 Color: 0
Size: 271773 Color: 4
Size: 260795 Color: 3

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 467506 Color: 3
Size: 273155 Color: 4
Size: 259340 Color: 2

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 467570 Color: 1
Size: 266244 Color: 3
Size: 266187 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 467587 Color: 1
Size: 271330 Color: 1
Size: 261084 Color: 3

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 467880 Color: 2
Size: 272822 Color: 4
Size: 259299 Color: 4

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 468006 Color: 3
Size: 280465 Color: 1
Size: 251530 Color: 2

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 468149 Color: 4
Size: 278333 Color: 1
Size: 253519 Color: 3

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 468164 Color: 0
Size: 269658 Color: 4
Size: 262179 Color: 4

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 468321 Color: 3
Size: 278618 Color: 1
Size: 253062 Color: 2

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 468545 Color: 1
Size: 266431 Color: 1
Size: 265025 Color: 2

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 468708 Color: 2
Size: 271479 Color: 0
Size: 259814 Color: 3

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 468750 Color: 1
Size: 274028 Color: 2
Size: 257223 Color: 4

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 468905 Color: 3
Size: 280372 Color: 2
Size: 250724 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 468813 Color: 1
Size: 269752 Color: 0
Size: 261436 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 468937 Color: 3
Size: 269024 Color: 2
Size: 262040 Color: 4

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 468960 Color: 1
Size: 269922 Color: 0
Size: 261119 Color: 3

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 469015 Color: 1
Size: 272866 Color: 2
Size: 258120 Color: 1

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 469142 Color: 1
Size: 276240 Color: 1
Size: 254619 Color: 3

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 469251 Color: 3
Size: 271095 Color: 2
Size: 259655 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 469157 Color: 1
Size: 272929 Color: 0
Size: 257915 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 469388 Color: 2
Size: 275640 Color: 4
Size: 254973 Color: 4

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 469457 Color: 1
Size: 274714 Color: 4
Size: 255830 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 469672 Color: 3
Size: 279442 Color: 2
Size: 250887 Color: 4

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 469631 Color: 2
Size: 265766 Color: 3
Size: 264604 Color: 1

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 469706 Color: 3
Size: 265687 Color: 0
Size: 264608 Color: 1

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 469887 Color: 0
Size: 273834 Color: 1
Size: 256280 Color: 2

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 469985 Color: 4
Size: 266972 Color: 4
Size: 263044 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 470171 Color: 3
Size: 265030 Color: 0
Size: 264800 Color: 2

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 470049 Color: 4
Size: 277803 Color: 0
Size: 252149 Color: 1

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 470498 Color: 4
Size: 275856 Color: 0
Size: 253647 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 470650 Color: 4
Size: 272443 Color: 3
Size: 256908 Color: 1

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 470850 Color: 2
Size: 276971 Color: 0
Size: 252180 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 470865 Color: 2
Size: 267987 Color: 3
Size: 261149 Color: 1

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 470908 Color: 2
Size: 268612 Color: 1
Size: 260481 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 471020 Color: 1
Size: 275971 Color: 3
Size: 253010 Color: 2

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 471066 Color: 4
Size: 278932 Color: 1
Size: 250003 Color: 2

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 471238 Color: 3
Size: 270811 Color: 2
Size: 257952 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 471246 Color: 3
Size: 275494 Color: 4
Size: 253261 Color: 2

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 471234 Color: 1
Size: 269948 Color: 4
Size: 258819 Color: 2

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 471275 Color: 2
Size: 273415 Color: 0
Size: 255311 Color: 3

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 471333 Color: 2
Size: 275959 Color: 0
Size: 252709 Color: 0

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 471385 Color: 1
Size: 265160 Color: 3
Size: 263456 Color: 2

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 471393 Color: 4
Size: 269467 Color: 3
Size: 259141 Color: 1

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 471471 Color: 4
Size: 269501 Color: 2
Size: 259029 Color: 2

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 471513 Color: 1
Size: 270859 Color: 1
Size: 257629 Color: 3

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 471602 Color: 0
Size: 267626 Color: 4
Size: 260773 Color: 2

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 471694 Color: 2
Size: 271200 Color: 3
Size: 257107 Color: 1

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 471705 Color: 4
Size: 267140 Color: 3
Size: 261156 Color: 4

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 471748 Color: 4
Size: 272464 Color: 4
Size: 255789 Color: 2

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 471871 Color: 2
Size: 269095 Color: 3
Size: 259035 Color: 1

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 471880 Color: 4
Size: 264461 Color: 1
Size: 263660 Color: 1

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 471898 Color: 1
Size: 277905 Color: 2
Size: 250198 Color: 3

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 472082 Color: 3
Size: 276997 Color: 4
Size: 250922 Color: 3

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 472098 Color: 4
Size: 267514 Color: 3
Size: 260389 Color: 4

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 472149 Color: 4
Size: 272950 Color: 0
Size: 254902 Color: 3

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 472230 Color: 2
Size: 276055 Color: 1
Size: 251716 Color: 3

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 472381 Color: 0
Size: 270986 Color: 1
Size: 256634 Color: 2

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 472434 Color: 2
Size: 275906 Color: 3
Size: 251661 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 472458 Color: 0
Size: 276938 Color: 0
Size: 250605 Color: 3

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 472847 Color: 3
Size: 272698 Color: 2
Size: 254456 Color: 1

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 472778 Color: 1
Size: 269424 Color: 1
Size: 257799 Color: 4

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 473034 Color: 1
Size: 268021 Color: 0
Size: 258946 Color: 3

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 473078 Color: 4
Size: 276271 Color: 1
Size: 250652 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 473104 Color: 4
Size: 268579 Color: 2
Size: 258318 Color: 1

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 473322 Color: 2
Size: 272758 Color: 3
Size: 253921 Color: 1

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 473372 Color: 3
Size: 274136 Color: 1
Size: 252493 Color: 2

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 473359 Color: 4
Size: 270884 Color: 0
Size: 255758 Color: 1

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 473385 Color: 4
Size: 268193 Color: 1
Size: 258423 Color: 3

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 473434 Color: 0
Size: 264083 Color: 3
Size: 262484 Color: 2

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 473436 Color: 1
Size: 264397 Color: 2
Size: 262168 Color: 4

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 473446 Color: 4
Size: 270920 Color: 3
Size: 255635 Color: 0

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 473532 Color: 0
Size: 272861 Color: 4
Size: 253608 Color: 1

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 473585 Color: 4
Size: 273716 Color: 0
Size: 252700 Color: 3

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 473631 Color: 3
Size: 267345 Color: 4
Size: 259025 Color: 1

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 473768 Color: 3
Size: 271559 Color: 4
Size: 254674 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 473869 Color: 1
Size: 275396 Color: 4
Size: 250736 Color: 4

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 474212 Color: 0
Size: 270208 Color: 2
Size: 255581 Color: 2

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 474286 Color: 3
Size: 272913 Color: 0
Size: 252802 Color: 4

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 474582 Color: 1
Size: 273094 Color: 0
Size: 252325 Color: 2

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 474783 Color: 1
Size: 266825 Color: 4
Size: 258393 Color: 4

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 474816 Color: 2
Size: 273383 Color: 1
Size: 251802 Color: 3

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 474868 Color: 4
Size: 262600 Color: 2
Size: 262533 Color: 4

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 474906 Color: 0
Size: 265110 Color: 3
Size: 259985 Color: 4

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 474932 Color: 2
Size: 272847 Color: 3
Size: 252222 Color: 1

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 474960 Color: 4
Size: 264722 Color: 1
Size: 260319 Color: 2

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 475015 Color: 3
Size: 270163 Color: 4
Size: 254823 Color: 1

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 475078 Color: 4
Size: 273580 Color: 2
Size: 251343 Color: 4

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 475264 Color: 4
Size: 272029 Color: 3
Size: 252708 Color: 1

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 475368 Color: 3
Size: 271245 Color: 4
Size: 253388 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 475345 Color: 1
Size: 269846 Color: 2
Size: 254810 Color: 1

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 475420 Color: 4
Size: 262861 Color: 3
Size: 261720 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 475503 Color: 4
Size: 268975 Color: 3
Size: 255523 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 475521 Color: 1
Size: 270667 Color: 2
Size: 253813 Color: 4

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 475688 Color: 3
Size: 270661 Color: 1
Size: 253652 Color: 2

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 475685 Color: 2
Size: 272234 Color: 1
Size: 252082 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 475893 Color: 3
Size: 268139 Color: 1
Size: 255969 Color: 4

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 476173 Color: 2
Size: 266841 Color: 0
Size: 256987 Color: 4

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 476178 Color: 2
Size: 268047 Color: 3
Size: 255776 Color: 2

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 476209 Color: 0
Size: 266444 Color: 2
Size: 257348 Color: 1

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 476221 Color: 4
Size: 267252 Color: 0
Size: 256528 Color: 3

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 476234 Color: 0
Size: 265678 Color: 1
Size: 258089 Color: 4

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 476270 Color: 0
Size: 272351 Color: 0
Size: 251380 Color: 3

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 476328 Color: 1
Size: 273442 Color: 4
Size: 250231 Color: 4

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 476380 Color: 4
Size: 269331 Color: 1
Size: 254290 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 476421 Color: 4
Size: 273487 Color: 3
Size: 250093 Color: 2

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 476429 Color: 1
Size: 267816 Color: 3
Size: 255756 Color: 4

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 476440 Color: 4
Size: 271142 Color: 2
Size: 252419 Color: 2

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 476543 Color: 1
Size: 272928 Color: 3
Size: 250530 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 476593 Color: 0
Size: 264165 Color: 3
Size: 259243 Color: 2

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 476700 Color: 0
Size: 269982 Color: 2
Size: 253319 Color: 4

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 476760 Color: 1
Size: 263949 Color: 0
Size: 259292 Color: 3

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 476780 Color: 2
Size: 267381 Color: 2
Size: 255840 Color: 4

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 476807 Color: 1
Size: 267722 Color: 0
Size: 255472 Color: 3

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 476952 Color: 3
Size: 272684 Color: 1
Size: 250365 Color: 2

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 476922 Color: 1
Size: 269452 Color: 1
Size: 253627 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 476934 Color: 2
Size: 266710 Color: 2
Size: 256357 Color: 3

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 477025 Color: 1
Size: 265248 Color: 3
Size: 257728 Color: 4

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 477519 Color: 1
Size: 265581 Color: 4
Size: 256901 Color: 1

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 477567 Color: 0
Size: 264647 Color: 1
Size: 257787 Color: 2

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 477764 Color: 3
Size: 266341 Color: 4
Size: 255896 Color: 2

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 477785 Color: 2
Size: 267524 Color: 1
Size: 254692 Color: 3

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 477853 Color: 4
Size: 271938 Color: 1
Size: 250210 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 478037 Color: 3
Size: 264766 Color: 0
Size: 257198 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 477954 Color: 2
Size: 265577 Color: 0
Size: 256470 Color: 4

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 478070 Color: 3
Size: 269116 Color: 1
Size: 252815 Color: 1

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 478163 Color: 3
Size: 271537 Color: 2
Size: 250301 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 478149 Color: 2
Size: 268524 Color: 4
Size: 253328 Color: 1

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 478251 Color: 0
Size: 271390 Color: 2
Size: 250360 Color: 3

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 478356 Color: 0
Size: 270490 Color: 1
Size: 251155 Color: 3

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 478540 Color: 1
Size: 267603 Color: 2
Size: 253858 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 478613 Color: 1
Size: 267537 Color: 2
Size: 253851 Color: 4

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 478679 Color: 1
Size: 267202 Color: 3
Size: 254120 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 478703 Color: 1
Size: 267052 Color: 3
Size: 254246 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 478739 Color: 4
Size: 261929 Color: 4
Size: 259333 Color: 1

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 479009 Color: 1
Size: 261885 Color: 0
Size: 259107 Color: 2

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 479129 Color: 3
Size: 264203 Color: 1
Size: 256669 Color: 2

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 479193 Color: 0
Size: 265387 Color: 1
Size: 255421 Color: 1

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 479211 Color: 1
Size: 265586 Color: 3
Size: 255204 Color: 4

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 479280 Color: 2
Size: 269545 Color: 1
Size: 251176 Color: 3

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 479312 Color: 1
Size: 269404 Color: 2
Size: 251285 Color: 4

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 479571 Color: 1
Size: 267435 Color: 4
Size: 252995 Color: 1

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 479754 Color: 3
Size: 261896 Color: 2
Size: 258351 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 479917 Color: 4
Size: 264895 Color: 3
Size: 255189 Color: 4

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 479922 Color: 3
Size: 264498 Color: 2
Size: 255581 Color: 1

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 479921 Color: 0
Size: 265802 Color: 2
Size: 254278 Color: 1

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 479965 Color: 1
Size: 264905 Color: 2
Size: 255131 Color: 3

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 479979 Color: 0
Size: 264331 Color: 2
Size: 255691 Color: 1

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 480068 Color: 2
Size: 265444 Color: 0
Size: 254489 Color: 3

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 480237 Color: 3
Size: 267592 Color: 1
Size: 252172 Color: 4

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 480105 Color: 4
Size: 269422 Color: 2
Size: 250474 Color: 4

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 480393 Color: 2
Size: 265924 Color: 0
Size: 253684 Color: 3

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 480527 Color: 1
Size: 262597 Color: 0
Size: 256877 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 480642 Color: 3
Size: 266206 Color: 1
Size: 253153 Color: 4

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 480603 Color: 4
Size: 264509 Color: 0
Size: 254889 Color: 2

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 480675 Color: 2
Size: 259896 Color: 4
Size: 259430 Color: 3

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 480698 Color: 1
Size: 269244 Color: 2
Size: 250059 Color: 3

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 480715 Color: 2
Size: 260102 Color: 4
Size: 259184 Color: 1

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 480788 Color: 1
Size: 268794 Color: 4
Size: 250419 Color: 3

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 480804 Color: 3
Size: 261354 Color: 4
Size: 257843 Color: 1

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 480883 Color: 1
Size: 264151 Color: 4
Size: 254967 Color: 1

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 480995 Color: 3
Size: 268275 Color: 0
Size: 250731 Color: 4

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 480937 Color: 4
Size: 262106 Color: 0
Size: 256958 Color: 4

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 481243 Color: 2
Size: 261396 Color: 3
Size: 257362 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 481309 Color: 3
Size: 268172 Color: 2
Size: 250520 Color: 3

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 481294 Color: 2
Size: 260247 Color: 4
Size: 258460 Color: 1

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 481310 Color: 2
Size: 268171 Color: 1
Size: 250520 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 481403 Color: 4
Size: 262795 Color: 2
Size: 255803 Color: 2

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 481414 Color: 4
Size: 265284 Color: 3
Size: 253303 Color: 4

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 481503 Color: 4
Size: 265204 Color: 1
Size: 253294 Color: 2

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 481531 Color: 1
Size: 259695 Color: 3
Size: 258775 Color: 2

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 481587 Color: 4
Size: 263155 Color: 4
Size: 255259 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 481610 Color: 4
Size: 268039 Color: 1
Size: 250352 Color: 3

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 481630 Color: 0
Size: 263003 Color: 3
Size: 255368 Color: 2

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 481655 Color: 4
Size: 261364 Color: 2
Size: 256982 Color: 2

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 481661 Color: 4
Size: 268181 Color: 3
Size: 250159 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 481674 Color: 1
Size: 268153 Color: 2
Size: 250174 Color: 4

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 481740 Color: 1
Size: 266511 Color: 4
Size: 251750 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 481820 Color: 2
Size: 266398 Color: 0
Size: 251783 Color: 4

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 481884 Color: 2
Size: 260879 Color: 3
Size: 257238 Color: 1

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 481911 Color: 0
Size: 261148 Color: 3
Size: 256942 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 481992 Color: 1
Size: 261274 Color: 4
Size: 256735 Color: 4

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 482048 Color: 3
Size: 266851 Color: 2
Size: 251102 Color: 4

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 482118 Color: 0
Size: 259809 Color: 3
Size: 258074 Color: 1

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 482159 Color: 2
Size: 261170 Color: 2
Size: 256672 Color: 0

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 482206 Color: 1
Size: 265086 Color: 0
Size: 252709 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 482235 Color: 2
Size: 259322 Color: 4
Size: 258444 Color: 3

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 482384 Color: 1
Size: 260740 Color: 0
Size: 256877 Color: 1

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 482547 Color: 3
Size: 264119 Color: 2
Size: 253335 Color: 1

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 482472 Color: 0
Size: 262578 Color: 4
Size: 254951 Color: 1

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 482541 Color: 0
Size: 259150 Color: 2
Size: 258310 Color: 3

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 482638 Color: 0
Size: 261873 Color: 3
Size: 255490 Color: 4

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 482639 Color: 0
Size: 266196 Color: 2
Size: 251166 Color: 1

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 482677 Color: 4
Size: 261222 Color: 3
Size: 256102 Color: 1

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 482798 Color: 4
Size: 260982 Color: 3
Size: 256221 Color: 3

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 482822 Color: 4
Size: 259447 Color: 0
Size: 257732 Color: 2

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 482959 Color: 3
Size: 260766 Color: 4
Size: 256276 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 482965 Color: 1
Size: 258748 Color: 4
Size: 258288 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 483004 Color: 4
Size: 261321 Color: 3
Size: 255676 Color: 4

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 483236 Color: 3
Size: 260698 Color: 1
Size: 256067 Color: 4

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 483317 Color: 0
Size: 258815 Color: 1
Size: 257869 Color: 1

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 483356 Color: 1
Size: 261838 Color: 0
Size: 254807 Color: 3

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 483414 Color: 1
Size: 259382 Color: 0
Size: 257205 Color: 3

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 483419 Color: 0
Size: 262699 Color: 4
Size: 253883 Color: 1

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 483459 Color: 3
Size: 259905 Color: 2
Size: 256637 Color: 0

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 483500 Color: 0
Size: 265396 Color: 1
Size: 251105 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 483857 Color: 4
Size: 259790 Color: 0
Size: 256354 Color: 3

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 483951 Color: 0
Size: 258708 Color: 3
Size: 257342 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 484007 Color: 2
Size: 259361 Color: 1
Size: 256633 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 484143 Color: 3
Size: 265325 Color: 4
Size: 250533 Color: 2

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 484387 Color: 0
Size: 262400 Color: 3
Size: 253214 Color: 0

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 484393 Color: 4
Size: 265134 Color: 3
Size: 250474 Color: 4

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 484418 Color: 2
Size: 258812 Color: 0
Size: 256771 Color: 0

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 484433 Color: 1
Size: 264843 Color: 1
Size: 250725 Color: 3

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 484458 Color: 2
Size: 265502 Color: 4
Size: 250041 Color: 4

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 484569 Color: 4
Size: 259444 Color: 2
Size: 255988 Color: 3

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 484572 Color: 2
Size: 260717 Color: 3
Size: 254712 Color: 0

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 484644 Color: 1
Size: 262003 Color: 3
Size: 253354 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 484775 Color: 2
Size: 258966 Color: 1
Size: 256260 Color: 3

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 484868 Color: 4
Size: 263413 Color: 3
Size: 251720 Color: 0

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 485005 Color: 0
Size: 263583 Color: 1
Size: 251413 Color: 1

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 485086 Color: 1
Size: 263987 Color: 3
Size: 250928 Color: 1

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 485178 Color: 3
Size: 258118 Color: 0
Size: 256705 Color: 4

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 485177 Color: 1
Size: 260206 Color: 2
Size: 254618 Color: 4

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 485264 Color: 1
Size: 259224 Color: 4
Size: 255513 Color: 3

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 485309 Color: 2
Size: 257511 Color: 2
Size: 257181 Color: 1

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 485328 Color: 4
Size: 259296 Color: 3
Size: 255377 Color: 4

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 485406 Color: 2
Size: 263619 Color: 2
Size: 250976 Color: 0

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 485414 Color: 1
Size: 259525 Color: 3
Size: 255062 Color: 0

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 485524 Color: 2
Size: 263367 Color: 0
Size: 251110 Color: 4

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 485799 Color: 3
Size: 261329 Color: 1
Size: 252873 Color: 1

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 485861 Color: 2
Size: 263425 Color: 1
Size: 250715 Color: 4

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 485721 Color: 4
Size: 262919 Color: 0
Size: 251361 Color: 0

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 485861 Color: 3
Size: 259846 Color: 3
Size: 254294 Color: 0

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 485901 Color: 3
Size: 258288 Color: 0
Size: 255812 Color: 2

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 485875 Color: 1
Size: 263467 Color: 2
Size: 250659 Color: 0

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 486128 Color: 4
Size: 262128 Color: 2
Size: 251745 Color: 3

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 486152 Color: 2
Size: 261516 Color: 1
Size: 252333 Color: 3

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 486472 Color: 3
Size: 257063 Color: 1
Size: 256466 Color: 1

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 486427 Color: 0
Size: 258002 Color: 0
Size: 255572 Color: 2

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 486492 Color: 4
Size: 261766 Color: 3
Size: 251743 Color: 1

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 486506 Color: 1
Size: 262057 Color: 3
Size: 251438 Color: 2

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 486648 Color: 2
Size: 260197 Color: 3
Size: 253156 Color: 1

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 486697 Color: 0
Size: 258137 Color: 2
Size: 255167 Color: 3

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 486773 Color: 4
Size: 256786 Color: 2
Size: 256442 Color: 4

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 486780 Color: 4
Size: 263143 Color: 3
Size: 250078 Color: 4

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 486970 Color: 0
Size: 262994 Color: 3
Size: 250037 Color: 4

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 486993 Color: 2
Size: 257933 Color: 0
Size: 255075 Color: 1

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 487324 Color: 2
Size: 261056 Color: 0
Size: 251621 Color: 2

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 487364 Color: 3
Size: 258849 Color: 1
Size: 253788 Color: 0

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 487377 Color: 2
Size: 258455 Color: 0
Size: 254169 Color: 3

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 487433 Color: 4
Size: 259670 Color: 1
Size: 252898 Color: 3

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 487611 Color: 0
Size: 257866 Color: 1
Size: 254524 Color: 1

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 487716 Color: 2
Size: 256143 Color: 4
Size: 256142 Color: 3

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 487792 Color: 0
Size: 261078 Color: 3
Size: 251131 Color: 0

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 487876 Color: 2
Size: 256844 Color: 4
Size: 255281 Color: 2

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 487894 Color: 4
Size: 256586 Color: 3
Size: 255521 Color: 1

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 488131 Color: 4
Size: 259201 Color: 2
Size: 252669 Color: 3

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 488214 Color: 0
Size: 260778 Color: 3
Size: 251009 Color: 2

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 488290 Color: 4
Size: 259441 Color: 3
Size: 252270 Color: 2

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 488296 Color: 4
Size: 260931 Color: 2
Size: 250774 Color: 3

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 488456 Color: 0
Size: 260942 Color: 2
Size: 250603 Color: 1

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 488456 Color: 2
Size: 258984 Color: 3
Size: 252561 Color: 1

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 488476 Color: 4
Size: 259727 Color: 1
Size: 251798 Color: 0

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 488629 Color: 3
Size: 258555 Color: 1
Size: 252817 Color: 1

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 488799 Color: 3
Size: 256694 Color: 2
Size: 254508 Color: 0

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 488778 Color: 2
Size: 258593 Color: 2
Size: 252630 Color: 4

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 488949 Color: 3
Size: 259598 Color: 1
Size: 251454 Color: 0

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 489020 Color: 0
Size: 258760 Color: 1
Size: 252221 Color: 2

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 489055 Color: 1
Size: 257882 Color: 3
Size: 253064 Color: 2

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 489085 Color: 0
Size: 260238 Color: 2
Size: 250678 Color: 1

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 489170 Color: 3
Size: 257292 Color: 2
Size: 253539 Color: 4

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 489251 Color: 3
Size: 258112 Color: 0
Size: 252638 Color: 0

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 489127 Color: 1
Size: 260048 Color: 2
Size: 250826 Color: 4

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 489630 Color: 3
Size: 258948 Color: 0
Size: 251423 Color: 4

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 489739 Color: 2
Size: 257588 Color: 2
Size: 252674 Color: 4

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 489761 Color: 2
Size: 255393 Color: 3
Size: 254847 Color: 0

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 489811 Color: 0
Size: 256559 Color: 1
Size: 253631 Color: 2

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 490099 Color: 0
Size: 259641 Color: 2
Size: 250261 Color: 3

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 490164 Color: 0
Size: 258366 Color: 3
Size: 251471 Color: 1

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 490169 Color: 2
Size: 257134 Color: 4
Size: 252698 Color: 2

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 490350 Color: 3
Size: 257612 Color: 4
Size: 252039 Color: 2

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 490299 Color: 1
Size: 258175 Color: 4
Size: 251527 Color: 0

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 490306 Color: 2
Size: 256988 Color: 3
Size: 252707 Color: 1

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 490380 Color: 3
Size: 255084 Color: 2
Size: 254537 Color: 4

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 490320 Color: 0
Size: 257223 Color: 3
Size: 252458 Color: 2

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 490396 Color: 2
Size: 255967 Color: 0
Size: 253638 Color: 3

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 490569 Color: 1
Size: 255442 Color: 0
Size: 253990 Color: 3

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 490613 Color: 0
Size: 259323 Color: 2
Size: 250065 Color: 4

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 490714 Color: 3
Size: 258567 Color: 1
Size: 250720 Color: 1

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 490784 Color: 4
Size: 257052 Color: 2
Size: 252165 Color: 0

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 490813 Color: 2
Size: 257338 Color: 3
Size: 251850 Color: 4

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 490818 Color: 3
Size: 254805 Color: 2
Size: 254378 Color: 4

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 490870 Color: 0
Size: 258316 Color: 0
Size: 250815 Color: 1

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 491066 Color: 4
Size: 256780 Color: 0
Size: 252155 Color: 0

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 491160 Color: 4
Size: 256907 Color: 2
Size: 251934 Color: 3

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 491240 Color: 3
Size: 255177 Color: 4
Size: 253584 Color: 1

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 491262 Color: 0
Size: 254665 Color: 4
Size: 254074 Color: 1

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 491282 Color: 2
Size: 256077 Color: 4
Size: 252642 Color: 3

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 491370 Color: 4
Size: 257947 Color: 3
Size: 250684 Color: 1

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 491420 Color: 2
Size: 254292 Color: 2
Size: 254289 Color: 0

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 491423 Color: 4
Size: 258401 Color: 2
Size: 250177 Color: 4

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 491504 Color: 2
Size: 257490 Color: 3
Size: 251007 Color: 4

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 491566 Color: 2
Size: 255225 Color: 4
Size: 253210 Color: 3

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 491739 Color: 0
Size: 256980 Color: 2
Size: 251282 Color: 0

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 491800 Color: 2
Size: 255711 Color: 2
Size: 252490 Color: 3

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 492012 Color: 0
Size: 254018 Color: 2
Size: 253971 Color: 3

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 492073 Color: 0
Size: 257082 Color: 2
Size: 250846 Color: 1

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 492228 Color: 3
Size: 254908 Color: 1
Size: 252865 Color: 0

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 492251 Color: 0
Size: 257093 Color: 4
Size: 250657 Color: 4

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 492290 Color: 3
Size: 256309 Color: 0
Size: 251402 Color: 1

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 492275 Color: 1
Size: 255818 Color: 0
Size: 251908 Color: 2

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 492408 Color: 0
Size: 255724 Color: 3
Size: 251869 Color: 2

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 492429 Color: 1
Size: 257167 Color: 2
Size: 250405 Color: 3

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 492496 Color: 4
Size: 255755 Color: 2
Size: 251750 Color: 3

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 492785 Color: 3
Size: 254672 Color: 0
Size: 252544 Color: 0

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 492715 Color: 4
Size: 255210 Color: 2
Size: 252076 Color: 1

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 492820 Color: 4
Size: 257083 Color: 1
Size: 250098 Color: 1

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 492997 Color: 2
Size: 255632 Color: 0
Size: 251372 Color: 4

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 493004 Color: 0
Size: 255451 Color: 2
Size: 251546 Color: 3

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 493050 Color: 4
Size: 256938 Color: 3
Size: 250013 Color: 2

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 493554 Color: 3
Size: 255314 Color: 1
Size: 251133 Color: 0

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 493488 Color: 4
Size: 254093 Color: 0
Size: 252420 Color: 2

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 493602 Color: 3
Size: 255413 Color: 2
Size: 250986 Color: 1

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 493736 Color: 1
Size: 254467 Color: 3
Size: 251798 Color: 0

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 493754 Color: 0
Size: 253641 Color: 0
Size: 252606 Color: 4

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 493780 Color: 1
Size: 256221 Color: 1
Size: 250000 Color: 4

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 493937 Color: 2
Size: 254057 Color: 0
Size: 252007 Color: 3

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 493970 Color: 4
Size: 255836 Color: 3
Size: 250195 Color: 0

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 494027 Color: 1
Size: 255375 Color: 4
Size: 250599 Color: 3

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 494095 Color: 3
Size: 255617 Color: 0
Size: 250289 Color: 2

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 494060 Color: 2
Size: 255766 Color: 0
Size: 250175 Color: 3

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 494074 Color: 0
Size: 254425 Color: 3
Size: 251502 Color: 2

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 494089 Color: 2
Size: 255703 Color: 0
Size: 250209 Color: 4

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 494335 Color: 0
Size: 253810 Color: 3
Size: 251856 Color: 1

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 494485 Color: 3
Size: 254124 Color: 0
Size: 251392 Color: 2

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 494589 Color: 4
Size: 255322 Color: 4
Size: 250090 Color: 2

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 494619 Color: 0
Size: 253875 Color: 2
Size: 251507 Color: 3

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 494624 Color: 1
Size: 255289 Color: 3
Size: 250088 Color: 2

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 494677 Color: 1
Size: 252886 Color: 4
Size: 252438 Color: 0

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 494876 Color: 3
Size: 253524 Color: 2
Size: 251601 Color: 1

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 494850 Color: 4
Size: 254055 Color: 4
Size: 251096 Color: 1

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 494894 Color: 0
Size: 254337 Color: 4
Size: 250770 Color: 3

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 495148 Color: 3
Size: 254782 Color: 1
Size: 250071 Color: 4

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 495129 Color: 2
Size: 254686 Color: 3
Size: 250186 Color: 1

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 495241 Color: 0
Size: 253638 Color: 4
Size: 251122 Color: 3

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 495340 Color: 4
Size: 253055 Color: 3
Size: 251606 Color: 1

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 495442 Color: 0
Size: 252926 Color: 4
Size: 251633 Color: 4

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 495468 Color: 2
Size: 253527 Color: 3
Size: 251006 Color: 2

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 495493 Color: 2
Size: 253170 Color: 2
Size: 251338 Color: 3

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 495627 Color: 3
Size: 252845 Color: 2
Size: 251529 Color: 4

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 495598 Color: 2
Size: 252478 Color: 3
Size: 251925 Color: 2

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 495836 Color: 3
Size: 253130 Color: 1
Size: 251035 Color: 2

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 495729 Color: 4
Size: 252775 Color: 2
Size: 251497 Color: 0

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 495922 Color: 1
Size: 252474 Color: 3
Size: 251605 Color: 0

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 495967 Color: 0
Size: 253368 Color: 3
Size: 250666 Color: 0

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 496077 Color: 4
Size: 253242 Color: 1
Size: 250682 Color: 0

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 496118 Color: 2
Size: 252923 Color: 1
Size: 250960 Color: 3

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 496224 Color: 1
Size: 252822 Color: 2
Size: 250955 Color: 2

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 496243 Color: 0
Size: 253356 Color: 3
Size: 250402 Color: 4

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 496268 Color: 2
Size: 252782 Color: 0
Size: 250951 Color: 0

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 496291 Color: 1
Size: 251997 Color: 3
Size: 251713 Color: 0

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 496427 Color: 0
Size: 252514 Color: 1
Size: 251060 Color: 3

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 496429 Color: 4
Size: 252547 Color: 1
Size: 251025 Color: 4

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 496440 Color: 2
Size: 252351 Color: 0
Size: 251210 Color: 3

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 496518 Color: 4
Size: 252366 Color: 4
Size: 251117 Color: 1

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 496583 Color: 3
Size: 251921 Color: 1
Size: 251497 Color: 1

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 497135 Color: 0
Size: 252827 Color: 2
Size: 250039 Color: 0

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 497270 Color: 1
Size: 252556 Color: 1
Size: 250175 Color: 0

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 497528 Color: 3
Size: 251333 Color: 4
Size: 251140 Color: 1

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 497740 Color: 3
Size: 251873 Color: 4
Size: 250388 Color: 2

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 497722 Color: 1
Size: 251304 Color: 0
Size: 250975 Color: 0

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 497875 Color: 0
Size: 251513 Color: 2
Size: 250613 Color: 3

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 498149 Color: 2
Size: 251439 Color: 3
Size: 250413 Color: 4

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 498191 Color: 4
Size: 251419 Color: 2
Size: 250391 Color: 2

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 498375 Color: 3
Size: 251557 Color: 1
Size: 250069 Color: 1

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 498361 Color: 0
Size: 251396 Color: 2
Size: 250244 Color: 2

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 498369 Color: 0
Size: 251317 Color: 3
Size: 250315 Color: 2

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 498445 Color: 4
Size: 251556 Color: 0
Size: 250000 Color: 4

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 498796 Color: 3
Size: 251062 Color: 1
Size: 250143 Color: 1

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 498835 Color: 3
Size: 250981 Color: 1
Size: 250185 Color: 2

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 499155 Color: 3
Size: 250616 Color: 1
Size: 250230 Color: 1

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 499062 Color: 1
Size: 250723 Color: 2
Size: 250216 Color: 2

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 499224 Color: 3
Size: 250761 Color: 4
Size: 250016 Color: 4

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 499317 Color: 2
Size: 250406 Color: 3
Size: 250278 Color: 0

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 499695 Color: 3
Size: 250171 Color: 2
Size: 250135 Color: 1

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 499664 Color: 1
Size: 250216 Color: 2
Size: 250121 Color: 3

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 499842 Color: 0
Size: 250114 Color: 1
Size: 250045 Color: 3

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 499864 Color: 3
Size: 250091 Color: 2
Size: 250046 Color: 2

Bin 3132: 1 of cap free
Amount of items: 3
Items: 
Size: 360227 Color: 4
Size: 335228 Color: 1
Size: 304545 Color: 2

Bin 3133: 1 of cap free
Amount of items: 3
Items: 
Size: 348428 Color: 3
Size: 335601 Color: 2
Size: 315971 Color: 3

Bin 3134: 1 of cap free
Amount of items: 3
Items: 
Size: 429257 Color: 2
Size: 300824 Color: 3
Size: 269919 Color: 2

Bin 3135: 1 of cap free
Amount of items: 3
Items: 
Size: 358217 Color: 1
Size: 335282 Color: 1
Size: 306501 Color: 4

Bin 3136: 1 of cap free
Amount of items: 3
Items: 
Size: 376451 Color: 2
Size: 367558 Color: 4
Size: 255991 Color: 3

Bin 3137: 1 of cap free
Amount of items: 3
Items: 
Size: 368928 Color: 1
Size: 361999 Color: 3
Size: 269073 Color: 4

Bin 3138: 1 of cap free
Amount of items: 3
Items: 
Size: 335477 Color: 3
Size: 334523 Color: 2
Size: 330000 Color: 0

Bin 3139: 1 of cap free
Amount of items: 3
Items: 
Size: 388197 Color: 0
Size: 354195 Color: 0
Size: 257608 Color: 3

Bin 3140: 1 of cap free
Amount of items: 3
Items: 
Size: 356509 Color: 2
Size: 327387 Color: 3
Size: 316104 Color: 4

Bin 3141: 1 of cap free
Amount of items: 3
Items: 
Size: 357833 Color: 1
Size: 344495 Color: 1
Size: 297672 Color: 2

Bin 3142: 1 of cap free
Amount of items: 3
Items: 
Size: 359756 Color: 2
Size: 357816 Color: 0
Size: 282428 Color: 1

Bin 3143: 1 of cap free
Amount of items: 3
Items: 
Size: 361599 Color: 3
Size: 348738 Color: 1
Size: 289663 Color: 2

Bin 3144: 1 of cap free
Amount of items: 3
Items: 
Size: 362252 Color: 4
Size: 320557 Color: 4
Size: 317191 Color: 2

Bin 3145: 1 of cap free
Amount of items: 3
Items: 
Size: 362581 Color: 2
Size: 356627 Color: 0
Size: 280792 Color: 2

Bin 3146: 1 of cap free
Amount of items: 3
Items: 
Size: 364767 Color: 0
Size: 362641 Color: 2
Size: 272592 Color: 2

Bin 3147: 1 of cap free
Amount of items: 3
Items: 
Size: 362917 Color: 0
Size: 357886 Color: 2
Size: 279197 Color: 4

Bin 3148: 1 of cap free
Amount of items: 3
Items: 
Size: 363149 Color: 4
Size: 351491 Color: 3
Size: 285360 Color: 0

Bin 3149: 1 of cap free
Amount of items: 3
Items: 
Size: 364307 Color: 1
Size: 352200 Color: 2
Size: 283493 Color: 0

Bin 3150: 1 of cap free
Amount of items: 3
Items: 
Size: 364595 Color: 2
Size: 324849 Color: 0
Size: 310556 Color: 3

Bin 3151: 1 of cap free
Amount of items: 3
Items: 
Size: 364859 Color: 4
Size: 338045 Color: 1
Size: 297096 Color: 0

Bin 3152: 1 of cap free
Amount of items: 3
Items: 
Size: 374080 Color: 3
Size: 365794 Color: 1
Size: 260126 Color: 3

Bin 3153: 1 of cap free
Amount of items: 3
Items: 
Size: 365825 Color: 0
Size: 346952 Color: 0
Size: 287223 Color: 3

Bin 3154: 1 of cap free
Amount of items: 3
Items: 
Size: 366675 Color: 2
Size: 346541 Color: 3
Size: 286784 Color: 0

Bin 3155: 1 of cap free
Amount of items: 3
Items: 
Size: 370826 Color: 0
Size: 367748 Color: 2
Size: 261426 Color: 0

Bin 3156: 1 of cap free
Amount of items: 3
Items: 
Size: 355044 Color: 2
Size: 331132 Color: 1
Size: 313824 Color: 3

Bin 3157: 1 of cap free
Amount of items: 3
Items: 
Size: 347816 Color: 3
Size: 339980 Color: 1
Size: 312204 Color: 2

Bin 3158: 1 of cap free
Amount of items: 3
Items: 
Size: 343702 Color: 3
Size: 331101 Color: 0
Size: 325197 Color: 4

Bin 3159: 1 of cap free
Amount of items: 3
Items: 
Size: 355609 Color: 1
Size: 344287 Color: 0
Size: 300104 Color: 4

Bin 3160: 1 of cap free
Amount of items: 3
Items: 
Size: 357121 Color: 3
Size: 341978 Color: 1
Size: 300901 Color: 4

Bin 3161: 1 of cap free
Amount of items: 3
Items: 
Size: 339442 Color: 0
Size: 330338 Color: 1
Size: 330220 Color: 1

Bin 3162: 1 of cap free
Amount of items: 3
Items: 
Size: 348501 Color: 1
Size: 346223 Color: 0
Size: 305276 Color: 4

Bin 3163: 1 of cap free
Amount of items: 3
Items: 
Size: 356253 Color: 3
Size: 355758 Color: 0
Size: 287989 Color: 4

Bin 3164: 1 of cap free
Amount of items: 3
Items: 
Size: 344402 Color: 0
Size: 336386 Color: 3
Size: 319212 Color: 0

Bin 3165: 1 of cap free
Amount of items: 3
Items: 
Size: 356287 Color: 4
Size: 350910 Color: 2
Size: 292803 Color: 1

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 334847 Color: 0
Size: 333788 Color: 2
Size: 331365 Color: 4

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 361448 Color: 0
Size: 345342 Color: 0
Size: 293210 Color: 2

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 355073 Color: 4
Size: 347681 Color: 2
Size: 297246 Color: 3

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 355637 Color: 0
Size: 325171 Color: 1
Size: 319192 Color: 3

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 357396 Color: 4
Size: 355654 Color: 2
Size: 286950 Color: 2

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 349043 Color: 0
Size: 338560 Color: 0
Size: 312397 Color: 1

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 366564 Color: 1
Size: 357638 Color: 3
Size: 275798 Color: 1

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 352380 Color: 1
Size: 350759 Color: 4
Size: 296861 Color: 3

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 342732 Color: 4
Size: 335586 Color: 1
Size: 321682 Color: 1

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 349370 Color: 1
Size: 325756 Color: 4
Size: 324874 Color: 3

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 349972 Color: 1
Size: 344995 Color: 1
Size: 305033 Color: 0

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 492980 Color: 1
Size: 255754 Color: 4
Size: 251266 Color: 2

Bin 3178: 2 of cap free
Amount of items: 3
Items: 
Size: 356311 Color: 4
Size: 348766 Color: 1
Size: 294922 Color: 2

Bin 3179: 2 of cap free
Amount of items: 3
Items: 
Size: 398752 Color: 2
Size: 348998 Color: 2
Size: 252249 Color: 3

Bin 3180: 2 of cap free
Amount of items: 3
Items: 
Size: 406878 Color: 4
Size: 341982 Color: 2
Size: 251139 Color: 2

Bin 3181: 2 of cap free
Amount of items: 3
Items: 
Size: 342569 Color: 4
Size: 334623 Color: 4
Size: 322807 Color: 3

Bin 3182: 2 of cap free
Amount of items: 3
Items: 
Size: 339761 Color: 3
Size: 334341 Color: 1
Size: 325897 Color: 0

Bin 3183: 2 of cap free
Amount of items: 3
Items: 
Size: 360482 Color: 0
Size: 348740 Color: 4
Size: 290777 Color: 2

Bin 3184: 2 of cap free
Amount of items: 3
Items: 
Size: 347399 Color: 1
Size: 326868 Color: 2
Size: 325732 Color: 0

Bin 3185: 2 of cap free
Amount of items: 3
Items: 
Size: 336583 Color: 4
Size: 331774 Color: 2
Size: 331642 Color: 2

Bin 3186: 2 of cap free
Amount of items: 3
Items: 
Size: 357474 Color: 4
Size: 343252 Color: 3
Size: 299273 Color: 0

Bin 3187: 2 of cap free
Amount of items: 3
Items: 
Size: 355720 Color: 0
Size: 331192 Color: 1
Size: 313087 Color: 0

Bin 3188: 2 of cap free
Amount of items: 3
Items: 
Size: 370281 Color: 3
Size: 360416 Color: 2
Size: 269302 Color: 2

Bin 3189: 2 of cap free
Amount of items: 3
Items: 
Size: 359253 Color: 1
Size: 353204 Color: 3
Size: 287542 Color: 4

Bin 3190: 2 of cap free
Amount of items: 3
Items: 
Size: 368904 Color: 4
Size: 358238 Color: 0
Size: 272857 Color: 2

Bin 3191: 2 of cap free
Amount of items: 3
Items: 
Size: 351344 Color: 2
Size: 350559 Color: 3
Size: 298096 Color: 1

Bin 3192: 2 of cap free
Amount of items: 3
Items: 
Size: 353094 Color: 1
Size: 336002 Color: 2
Size: 310903 Color: 2

Bin 3193: 2 of cap free
Amount of items: 3
Items: 
Size: 367770 Color: 3
Size: 362089 Color: 4
Size: 270140 Color: 4

Bin 3194: 2 of cap free
Amount of items: 3
Items: 
Size: 339026 Color: 4
Size: 332019 Color: 4
Size: 328954 Color: 1

Bin 3195: 2 of cap free
Amount of items: 3
Items: 
Size: 399345 Color: 2
Size: 338850 Color: 3
Size: 261804 Color: 3

Bin 3196: 3 of cap free
Amount of items: 3
Items: 
Size: 340432 Color: 0
Size: 339245 Color: 4
Size: 320321 Color: 1

Bin 3197: 3 of cap free
Amount of items: 3
Items: 
Size: 365544 Color: 3
Size: 364683 Color: 1
Size: 269771 Color: 0

Bin 3198: 3 of cap free
Amount of items: 3
Items: 
Size: 345565 Color: 3
Size: 327457 Color: 3
Size: 326976 Color: 4

Bin 3199: 3 of cap free
Amount of items: 3
Items: 
Size: 349410 Color: 2
Size: 338603 Color: 1
Size: 311985 Color: 2

Bin 3200: 3 of cap free
Amount of items: 3
Items: 
Size: 354494 Color: 2
Size: 351920 Color: 2
Size: 293584 Color: 3

Bin 3201: 3 of cap free
Amount of items: 3
Items: 
Size: 356175 Color: 1
Size: 354736 Color: 2
Size: 289087 Color: 2

Bin 3202: 3 of cap free
Amount of items: 3
Items: 
Size: 379955 Color: 2
Size: 351758 Color: 4
Size: 268285 Color: 0

Bin 3203: 3 of cap free
Amount of items: 3
Items: 
Size: 338183 Color: 3
Size: 334686 Color: 0
Size: 327129 Color: 1

Bin 3204: 4 of cap free
Amount of items: 3
Items: 
Size: 359381 Color: 3
Size: 359143 Color: 2
Size: 281473 Color: 0

Bin 3205: 4 of cap free
Amount of items: 3
Items: 
Size: 355177 Color: 2
Size: 354211 Color: 2
Size: 290609 Color: 1

Bin 3206: 4 of cap free
Amount of items: 3
Items: 
Size: 416996 Color: 3
Size: 331186 Color: 1
Size: 251815 Color: 2

Bin 3207: 4 of cap free
Amount of items: 3
Items: 
Size: 499249 Color: 2
Size: 250390 Color: 1
Size: 250358 Color: 1

Bin 3208: 4 of cap free
Amount of items: 3
Items: 
Size: 357715 Color: 0
Size: 343231 Color: 0
Size: 299051 Color: 4

Bin 3209: 4 of cap free
Amount of items: 3
Items: 
Size: 414075 Color: 1
Size: 333842 Color: 3
Size: 252080 Color: 3

Bin 3210: 4 of cap free
Amount of items: 3
Items: 
Size: 355416 Color: 1
Size: 352421 Color: 0
Size: 292160 Color: 1

Bin 3211: 4 of cap free
Amount of items: 3
Items: 
Size: 444520 Color: 3
Size: 299426 Color: 1
Size: 256051 Color: 2

Bin 3212: 4 of cap free
Amount of items: 3
Items: 
Size: 343699 Color: 3
Size: 339934 Color: 3
Size: 316364 Color: 4

Bin 3213: 5 of cap free
Amount of items: 3
Items: 
Size: 408185 Color: 0
Size: 338809 Color: 0
Size: 253002 Color: 3

Bin 3214: 5 of cap free
Amount of items: 3
Items: 
Size: 365228 Color: 0
Size: 358825 Color: 4
Size: 275943 Color: 1

Bin 3215: 5 of cap free
Amount of items: 3
Items: 
Size: 334597 Color: 0
Size: 334028 Color: 0
Size: 331371 Color: 2

Bin 3216: 5 of cap free
Amount of items: 3
Items: 
Size: 352431 Color: 4
Size: 349255 Color: 0
Size: 298310 Color: 0

Bin 3217: 6 of cap free
Amount of items: 3
Items: 
Size: 464335 Color: 0
Size: 277819 Color: 1
Size: 257841 Color: 1

Bin 3218: 6 of cap free
Amount of items: 3
Items: 
Size: 339867 Color: 3
Size: 335893 Color: 4
Size: 324235 Color: 4

Bin 3219: 6 of cap free
Amount of items: 3
Items: 
Size: 366901 Color: 4
Size: 360765 Color: 2
Size: 272329 Color: 4

Bin 3220: 6 of cap free
Amount of items: 3
Items: 
Size: 339621 Color: 1
Size: 335707 Color: 1
Size: 324667 Color: 4

Bin 3221: 6 of cap free
Amount of items: 3
Items: 
Size: 362623 Color: 1
Size: 360737 Color: 4
Size: 276635 Color: 1

Bin 3222: 6 of cap free
Amount of items: 3
Items: 
Size: 357150 Color: 0
Size: 355121 Color: 4
Size: 287724 Color: 2

Bin 3223: 6 of cap free
Amount of items: 3
Items: 
Size: 355146 Color: 1
Size: 343244 Color: 3
Size: 301605 Color: 1

Bin 3224: 7 of cap free
Amount of items: 3
Items: 
Size: 428907 Color: 2
Size: 315939 Color: 4
Size: 255148 Color: 0

Bin 3225: 7 of cap free
Amount of items: 3
Items: 
Size: 390208 Color: 2
Size: 342408 Color: 4
Size: 267378 Color: 1

Bin 3226: 7 of cap free
Amount of items: 3
Items: 
Size: 346065 Color: 1
Size: 334479 Color: 0
Size: 319450 Color: 2

Bin 3227: 7 of cap free
Amount of items: 3
Items: 
Size: 356623 Color: 0
Size: 344503 Color: 1
Size: 298868 Color: 2

Bin 3228: 7 of cap free
Amount of items: 3
Items: 
Size: 376816 Color: 1
Size: 355039 Color: 4
Size: 268139 Color: 4

Bin 3229: 8 of cap free
Amount of items: 3
Items: 
Size: 358126 Color: 2
Size: 350841 Color: 2
Size: 291026 Color: 1

Bin 3230: 8 of cap free
Amount of items: 3
Items: 
Size: 369736 Color: 3
Size: 357344 Color: 1
Size: 272913 Color: 4

Bin 3231: 8 of cap free
Amount of items: 3
Items: 
Size: 340170 Color: 0
Size: 333422 Color: 0
Size: 326401 Color: 4

Bin 3232: 9 of cap free
Amount of items: 3
Items: 
Size: 458443 Color: 3
Size: 277801 Color: 3
Size: 263748 Color: 2

Bin 3233: 9 of cap free
Amount of items: 3
Items: 
Size: 388783 Color: 4
Size: 354403 Color: 1
Size: 256806 Color: 0

Bin 3234: 10 of cap free
Amount of items: 3
Items: 
Size: 482224 Color: 3
Size: 263139 Color: 0
Size: 254628 Color: 1

Bin 3235: 10 of cap free
Amount of items: 3
Items: 
Size: 396344 Color: 4
Size: 342639 Color: 0
Size: 261008 Color: 4

Bin 3236: 10 of cap free
Amount of items: 3
Items: 
Size: 392099 Color: 3
Size: 346159 Color: 0
Size: 261733 Color: 2

Bin 3237: 10 of cap free
Amount of items: 3
Items: 
Size: 382022 Color: 0
Size: 353484 Color: 0
Size: 264485 Color: 2

Bin 3238: 10 of cap free
Amount of items: 3
Items: 
Size: 416320 Color: 3
Size: 327290 Color: 0
Size: 256381 Color: 4

Bin 3239: 10 of cap free
Amount of items: 3
Items: 
Size: 457190 Color: 4
Size: 284732 Color: 2
Size: 258069 Color: 4

Bin 3240: 10 of cap free
Amount of items: 3
Items: 
Size: 360398 Color: 4
Size: 347551 Color: 3
Size: 292042 Color: 2

Bin 3241: 11 of cap free
Amount of items: 3
Items: 
Size: 481266 Color: 3
Size: 263503 Color: 1
Size: 255221 Color: 1

Bin 3242: 11 of cap free
Amount of items: 3
Items: 
Size: 348848 Color: 4
Size: 329832 Color: 3
Size: 321310 Color: 0

Bin 3243: 11 of cap free
Amount of items: 3
Items: 
Size: 359609 Color: 3
Size: 357382 Color: 3
Size: 282999 Color: 2

Bin 3244: 11 of cap free
Amount of items: 3
Items: 
Size: 340379 Color: 0
Size: 338921 Color: 0
Size: 320690 Color: 4

Bin 3245: 11 of cap free
Amount of items: 3
Items: 
Size: 376421 Color: 1
Size: 345662 Color: 4
Size: 277907 Color: 2

Bin 3246: 12 of cap free
Amount of items: 3
Items: 
Size: 472045 Color: 1
Size: 266064 Color: 1
Size: 261880 Color: 3

Bin 3247: 13 of cap free
Amount of items: 3
Items: 
Size: 366212 Color: 1
Size: 353647 Color: 4
Size: 280129 Color: 4

Bin 3248: 14 of cap free
Amount of items: 3
Items: 
Size: 364825 Color: 1
Size: 353299 Color: 0
Size: 281863 Color: 1

Bin 3249: 14 of cap free
Amount of items: 3
Items: 
Size: 357794 Color: 2
Size: 356349 Color: 2
Size: 285844 Color: 1

Bin 3250: 15 of cap free
Amount of items: 3
Items: 
Size: 357740 Color: 1
Size: 357551 Color: 1
Size: 284695 Color: 2

Bin 3251: 15 of cap free
Amount of items: 3
Items: 
Size: 436334 Color: 4
Size: 295016 Color: 0
Size: 268636 Color: 4

Bin 3252: 15 of cap free
Amount of items: 3
Items: 
Size: 361259 Color: 2
Size: 358992 Color: 4
Size: 279735 Color: 2

Bin 3253: 16 of cap free
Amount of items: 3
Items: 
Size: 358124 Color: 0
Size: 353694 Color: 4
Size: 288167 Color: 0

Bin 3254: 16 of cap free
Amount of items: 3
Items: 
Size: 427378 Color: 3
Size: 309932 Color: 0
Size: 262675 Color: 0

Bin 3255: 17 of cap free
Amount of items: 3
Items: 
Size: 393206 Color: 3
Size: 340063 Color: 3
Size: 266715 Color: 1

Bin 3256: 17 of cap free
Amount of items: 3
Items: 
Size: 395421 Color: 3
Size: 350940 Color: 0
Size: 253623 Color: 3

Bin 3257: 17 of cap free
Amount of items: 3
Items: 
Size: 358472 Color: 3
Size: 357845 Color: 0
Size: 283667 Color: 4

Bin 3258: 17 of cap free
Amount of items: 3
Items: 
Size: 343972 Color: 1
Size: 341002 Color: 2
Size: 315010 Color: 3

Bin 3259: 18 of cap free
Amount of items: 3
Items: 
Size: 357446 Color: 0
Size: 345053 Color: 4
Size: 297484 Color: 4

Bin 3260: 18 of cap free
Amount of items: 3
Items: 
Size: 460996 Color: 3
Size: 280876 Color: 1
Size: 258111 Color: 0

Bin 3261: 20 of cap free
Amount of items: 3
Items: 
Size: 354204 Color: 2
Size: 338829 Color: 4
Size: 306948 Color: 1

Bin 3262: 20 of cap free
Amount of items: 3
Items: 
Size: 346261 Color: 3
Size: 346065 Color: 2
Size: 307655 Color: 0

Bin 3263: 20 of cap free
Amount of items: 3
Items: 
Size: 350455 Color: 0
Size: 340544 Color: 0
Size: 308982 Color: 2

Bin 3264: 23 of cap free
Amount of items: 3
Items: 
Size: 499758 Color: 1
Size: 250202 Color: 4
Size: 250018 Color: 2

Bin 3265: 24 of cap free
Amount of items: 3
Items: 
Size: 340214 Color: 1
Size: 330654 Color: 4
Size: 329109 Color: 2

Bin 3266: 26 of cap free
Amount of items: 3
Items: 
Size: 341671 Color: 2
Size: 330257 Color: 4
Size: 328047 Color: 0

Bin 3267: 26 of cap free
Amount of items: 3
Items: 
Size: 356838 Color: 3
Size: 355261 Color: 4
Size: 287876 Color: 3

Bin 3268: 26 of cap free
Amount of items: 3
Items: 
Size: 393243 Color: 3
Size: 341974 Color: 4
Size: 264758 Color: 3

Bin 3269: 27 of cap free
Amount of items: 3
Items: 
Size: 350622 Color: 0
Size: 328367 Color: 0
Size: 320985 Color: 1

Bin 3270: 31 of cap free
Amount of items: 3
Items: 
Size: 360824 Color: 2
Size: 350186 Color: 2
Size: 288960 Color: 0

Bin 3271: 31 of cap free
Amount of items: 3
Items: 
Size: 361317 Color: 3
Size: 360553 Color: 1
Size: 278100 Color: 4

Bin 3272: 32 of cap free
Amount of items: 3
Items: 
Size: 499475 Color: 0
Size: 250450 Color: 2
Size: 250044 Color: 3

Bin 3273: 33 of cap free
Amount of items: 3
Items: 
Size: 334492 Color: 3
Size: 333042 Color: 2
Size: 332434 Color: 4

Bin 3274: 35 of cap free
Amount of items: 3
Items: 
Size: 362763 Color: 2
Size: 356608 Color: 2
Size: 280595 Color: 4

Bin 3275: 36 of cap free
Amount of items: 3
Items: 
Size: 348642 Color: 3
Size: 347373 Color: 3
Size: 303950 Color: 0

Bin 3276: 37 of cap free
Amount of items: 3
Items: 
Size: 450118 Color: 4
Size: 275041 Color: 2
Size: 274805 Color: 0

Bin 3277: 37 of cap free
Amount of items: 3
Items: 
Size: 355254 Color: 2
Size: 354664 Color: 2
Size: 290046 Color: 3

Bin 3278: 39 of cap free
Amount of items: 3
Items: 
Size: 394862 Color: 0
Size: 348238 Color: 2
Size: 256862 Color: 1

Bin 3279: 39 of cap free
Amount of items: 3
Items: 
Size: 352837 Color: 1
Size: 342729 Color: 3
Size: 304396 Color: 2

Bin 3280: 39 of cap free
Amount of items: 3
Items: 
Size: 357774 Color: 0
Size: 349937 Color: 2
Size: 292251 Color: 0

Bin 3281: 43 of cap free
Amount of items: 3
Items: 
Size: 341780 Color: 3
Size: 335988 Color: 3
Size: 322190 Color: 4

Bin 3282: 45 of cap free
Amount of items: 3
Items: 
Size: 351002 Color: 0
Size: 345800 Color: 3
Size: 303154 Color: 3

Bin 3283: 47 of cap free
Amount of items: 3
Items: 
Size: 347748 Color: 0
Size: 339618 Color: 0
Size: 312588 Color: 3

Bin 3284: 49 of cap free
Amount of items: 3
Items: 
Size: 410291 Color: 4
Size: 297193 Color: 1
Size: 292468 Color: 3

Bin 3285: 49 of cap free
Amount of items: 3
Items: 
Size: 356336 Color: 2
Size: 354961 Color: 0
Size: 288655 Color: 2

Bin 3286: 49 of cap free
Amount of items: 3
Items: 
Size: 424928 Color: 2
Size: 318845 Color: 4
Size: 256179 Color: 2

Bin 3287: 49 of cap free
Amount of items: 3
Items: 
Size: 347581 Color: 0
Size: 344388 Color: 1
Size: 307983 Color: 3

Bin 3288: 50 of cap free
Amount of items: 3
Items: 
Size: 403534 Color: 3
Size: 340588 Color: 4
Size: 255829 Color: 1

Bin 3289: 51 of cap free
Amount of items: 3
Items: 
Size: 442185 Color: 4
Size: 286694 Color: 0
Size: 271071 Color: 4

Bin 3290: 58 of cap free
Amount of items: 3
Items: 
Size: 499517 Color: 2
Size: 250382 Color: 3
Size: 250044 Color: 3

Bin 3291: 61 of cap free
Amount of items: 3
Items: 
Size: 343525 Color: 4
Size: 331436 Color: 0
Size: 324979 Color: 4

Bin 3292: 65 of cap free
Amount of items: 3
Items: 
Size: 369511 Color: 4
Size: 357649 Color: 2
Size: 272776 Color: 1

Bin 3293: 67 of cap free
Amount of items: 3
Items: 
Size: 464577 Color: 0
Size: 270626 Color: 0
Size: 264731 Color: 2

Bin 3294: 79 of cap free
Amount of items: 3
Items: 
Size: 356332 Color: 4
Size: 345679 Color: 2
Size: 297911 Color: 1

Bin 3295: 97 of cap free
Amount of items: 3
Items: 
Size: 361218 Color: 3
Size: 359892 Color: 2
Size: 278794 Color: 3

Bin 3296: 97 of cap free
Amount of items: 3
Items: 
Size: 463401 Color: 1
Size: 274592 Color: 0
Size: 261911 Color: 4

Bin 3297: 111 of cap free
Amount of items: 3
Items: 
Size: 351367 Color: 2
Size: 348971 Color: 0
Size: 299552 Color: 4

Bin 3298: 112 of cap free
Amount of items: 3
Items: 
Size: 419852 Color: 3
Size: 316490 Color: 4
Size: 263547 Color: 0

Bin 3299: 128 of cap free
Amount of items: 3
Items: 
Size: 452915 Color: 4
Size: 279596 Color: 3
Size: 267362 Color: 1

Bin 3300: 140 of cap free
Amount of items: 3
Items: 
Size: 401539 Color: 4
Size: 326378 Color: 1
Size: 271944 Color: 1

Bin 3301: 145 of cap free
Amount of items: 2
Items: 
Size: 499985 Color: 1
Size: 499871 Color: 2

Bin 3302: 146 of cap free
Amount of items: 3
Items: 
Size: 363403 Color: 3
Size: 352562 Color: 2
Size: 283890 Color: 4

Bin 3303: 147 of cap free
Amount of items: 3
Items: 
Size: 346908 Color: 0
Size: 339867 Color: 3
Size: 313079 Color: 0

Bin 3304: 149 of cap free
Amount of items: 3
Items: 
Size: 375528 Color: 2
Size: 344994 Color: 0
Size: 279330 Color: 0

Bin 3305: 162 of cap free
Amount of items: 3
Items: 
Size: 411128 Color: 0
Size: 331888 Color: 1
Size: 256823 Color: 0

Bin 3306: 170 of cap free
Amount of items: 3
Items: 
Size: 351686 Color: 2
Size: 341095 Color: 0
Size: 307050 Color: 1

Bin 3307: 174 of cap free
Amount of items: 3
Items: 
Size: 338141 Color: 0
Size: 332587 Color: 2
Size: 329099 Color: 4

Bin 3308: 177 of cap free
Amount of items: 3
Items: 
Size: 368316 Color: 2
Size: 356981 Color: 3
Size: 274527 Color: 1

Bin 3309: 206 of cap free
Amount of items: 3
Items: 
Size: 352543 Color: 0
Size: 347329 Color: 2
Size: 299923 Color: 3

Bin 3310: 215 of cap free
Amount of items: 3
Items: 
Size: 373818 Color: 4
Size: 360256 Color: 3
Size: 265712 Color: 4

Bin 3311: 224 of cap free
Amount of items: 3
Items: 
Size: 449815 Color: 3
Size: 285284 Color: 2
Size: 264678 Color: 2

Bin 3312: 238 of cap free
Amount of items: 3
Items: 
Size: 366789 Color: 2
Size: 348278 Color: 1
Size: 284696 Color: 1

Bin 3313: 279 of cap free
Amount of items: 3
Items: 
Size: 349169 Color: 2
Size: 349169 Color: 1
Size: 301384 Color: 4

Bin 3314: 319 of cap free
Amount of items: 3
Items: 
Size: 354608 Color: 2
Size: 348814 Color: 3
Size: 296260 Color: 0

Bin 3315: 348 of cap free
Amount of items: 3
Items: 
Size: 457001 Color: 3
Size: 280044 Color: 0
Size: 262608 Color: 4

Bin 3316: 387 of cap free
Amount of items: 3
Items: 
Size: 347581 Color: 4
Size: 333797 Color: 4
Size: 318236 Color: 3

Bin 3317: 509 of cap free
Amount of items: 3
Items: 
Size: 353946 Color: 2
Size: 330646 Color: 3
Size: 314900 Color: 4

Bin 3318: 595 of cap free
Amount of items: 2
Items: 
Size: 499785 Color: 2
Size: 499621 Color: 4

Bin 3319: 630 of cap free
Amount of items: 3
Items: 
Size: 422801 Color: 0
Size: 317201 Color: 3
Size: 259369 Color: 1

Bin 3320: 678 of cap free
Amount of items: 3
Items: 
Size: 381288 Color: 4
Size: 349935 Color: 3
Size: 268100 Color: 2

Bin 3321: 725 of cap free
Amount of items: 3
Items: 
Size: 352267 Color: 2
Size: 323990 Color: 0
Size: 323019 Color: 2

Bin 3322: 750 of cap free
Amount of items: 3
Items: 
Size: 406356 Color: 3
Size: 322101 Color: 3
Size: 270794 Color: 0

Bin 3323: 761 of cap free
Amount of items: 3
Items: 
Size: 414731 Color: 3
Size: 330469 Color: 0
Size: 254040 Color: 0

Bin 3324: 1356 of cap free
Amount of items: 2
Items: 
Size: 499375 Color: 2
Size: 499270 Color: 1

Bin 3325: 1400 of cap free
Amount of items: 3
Items: 
Size: 372991 Color: 1
Size: 348225 Color: 0
Size: 277385 Color: 2

Bin 3326: 1786 of cap free
Amount of items: 3
Items: 
Size: 361033 Color: 1
Size: 356512 Color: 2
Size: 280670 Color: 1

Bin 3327: 2233 of cap free
Amount of items: 2
Items: 
Size: 499048 Color: 2
Size: 498720 Color: 4

Bin 3328: 18896 of cap free
Amount of items: 3
Items: 
Size: 437470 Color: 0
Size: 273127 Color: 1
Size: 270508 Color: 2

Bin 3329: 38566 of cap free
Amount of items: 3
Items: 
Size: 346662 Color: 1
Size: 345764 Color: 2
Size: 269009 Color: 0

Bin 3330: 51822 of cap free
Amount of items: 3
Items: 
Size: 343238 Color: 4
Size: 337226 Color: 1
Size: 267715 Color: 0

Bin 3331: 213903 of cap free
Amount of items: 3
Items: 
Size: 263331 Color: 3
Size: 261531 Color: 1
Size: 261236 Color: 2

Bin 3332: 217835 of cap free
Amount of items: 3
Items: 
Size: 261215 Color: 2
Size: 260619 Color: 4
Size: 260332 Color: 3

Bin 3333: 220884 of cap free
Amount of items: 3
Items: 
Size: 260326 Color: 2
Size: 260029 Color: 3
Size: 258762 Color: 2

Bin 3334: 230017 of cap free
Amount of items: 3
Items: 
Size: 257766 Color: 4
Size: 256471 Color: 3
Size: 255747 Color: 1

Bin 3335: 241608 of cap free
Amount of items: 3
Items: 
Size: 254835 Color: 0
Size: 252202 Color: 1
Size: 251356 Color: 4

Bin 3336: 748874 of cap free
Amount of items: 1
Items: 
Size: 251127 Color: 3

Total size: 3334003334
Total free space: 2000002

