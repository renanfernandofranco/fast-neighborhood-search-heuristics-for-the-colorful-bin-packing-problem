Capicity Bin: 1001
Lower Bound: 228

Bins used: 228
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 1
Size: 143 Color: 0
Size: 117 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 1
Size: 162 Color: 1
Size: 156 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 0
Size: 263 Color: 1
Size: 172 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 1
Size: 186 Color: 0
Size: 184 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 1
Size: 150 Color: 1
Size: 119 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 680 Color: 0
Size: 176 Color: 1
Size: 145 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 1
Size: 216 Color: 0
Size: 107 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 1
Size: 288 Color: 0
Size: 144 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 0
Size: 199 Color: 1
Size: 114 Color: 1

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 0
Size: 255 Color: 1

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 318 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 0
Size: 148 Color: 0
Size: 105 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 1
Size: 191 Color: 0
Size: 182 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 333 Color: 1
Size: 174 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 577 Color: 1
Size: 241 Color: 0
Size: 183 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 1
Size: 187 Color: 0
Size: 130 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 0
Size: 193 Color: 1
Size: 126 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 590 Color: 1
Size: 227 Color: 1
Size: 184 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 1
Size: 192 Color: 0
Size: 135 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 730 Color: 1
Size: 161 Color: 1
Size: 110 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 593 Color: 1
Size: 267 Color: 1
Size: 141 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 751 Color: 0
Size: 139 Color: 0
Size: 111 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 343 Color: 0
Size: 195 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 0
Size: 174 Color: 0
Size: 140 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 1
Size: 131 Color: 0
Size: 129 Color: 0

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 252 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 596 Color: 1
Size: 260 Color: 1
Size: 145 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 0
Size: 229 Color: 1
Size: 100 Color: 1

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 594 Color: 1
Size: 272 Color: 1
Size: 135 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 0
Size: 274 Color: 1
Size: 114 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 436 Color: 1
Size: 107 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 1
Size: 274 Color: 1
Size: 152 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 407 Color: 1
Size: 126 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 0
Size: 187 Color: 0
Size: 106 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 1
Size: 195 Color: 1
Size: 174 Color: 0

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 1
Size: 378 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 296 Color: 0
Size: 241 Color: 0

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 260 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 1
Size: 108 Color: 1
Size: 107 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 775 Color: 0
Size: 116 Color: 1
Size: 110 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 447 Color: 1
Size: 102 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 434 Color: 0
Size: 109 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 434 Color: 0
Size: 110 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 0
Size: 191 Color: 1
Size: 177 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 434 Color: 0
Size: 118 Color: 0

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 212 Color: 1

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 212 Color: 1

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 332 Color: 0

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 397 Color: 0

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 0
Size: 322 Color: 1

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 0

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 0
Size: 467 Color: 1

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 479 Color: 0

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 0
Size: 448 Color: 1

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 1

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 454 Color: 0

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 0

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 1
Size: 433 Color: 0

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 419 Color: 0

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 0
Size: 414 Color: 1

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 0

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 1
Size: 413 Color: 0

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 0

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 399 Color: 0

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 0
Size: 396 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 374 Color: 1

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 369 Color: 0

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 362 Color: 0

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 0
Size: 350 Color: 1

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 0
Size: 347 Color: 1

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 328 Color: 0

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 0

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 1

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 1
Size: 289 Color: 0

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 283 Color: 1

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 261 Color: 1

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 254 Color: 1

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 237 Color: 0

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 1

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 222 Color: 1

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 0

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 215 Color: 1

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 211 Color: 1

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 207 Color: 0

Bin 88: 1 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 296 Color: 0

Bin 89: 1 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 321 Color: 0

Bin 90: 1 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 282 Color: 0

Bin 91: 1 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 302 Color: 1

Bin 92: 1 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 0
Size: 382 Color: 1

Bin 93: 1 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 1
Size: 487 Color: 0

Bin 94: 1 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 380 Color: 1

Bin 95: 1 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 0
Size: 491 Color: 1

Bin 96: 1 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 1
Size: 466 Color: 0

Bin 97: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 444 Color: 0

Bin 98: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 259 Color: 0

Bin 99: 1 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 495 Color: 1

Bin 100: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 444 Color: 0

Bin 101: 1 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 0
Size: 322 Color: 1

Bin 102: 1 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 498 Color: 0

Bin 103: 1 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 1
Size: 489 Color: 0

Bin 104: 1 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 442 Color: 1

Bin 105: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 447 Color: 0

Bin 106: 1 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 1
Size: 439 Color: 0

Bin 107: 1 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 0
Size: 431 Color: 1

Bin 108: 1 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 420 Color: 0

Bin 109: 1 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 403 Color: 1

Bin 110: 1 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 348 Color: 0

Bin 111: 1 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 340 Color: 0

Bin 112: 1 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 0
Size: 324 Color: 1

Bin 113: 1 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 310 Color: 1

Bin 114: 1 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 263 Color: 0

Bin 115: 1 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 238 Color: 0

Bin 116: 1 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 1
Size: 232 Color: 0

Bin 117: 1 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 209 Color: 1

Bin 118: 1 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 202 Color: 0

Bin 119: 2 of cap free
Amount of items: 3
Items: 
Size: 578 Color: 1
Size: 288 Color: 0
Size: 133 Color: 1

Bin 120: 2 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 1
Size: 271 Color: 1
Size: 137 Color: 0

Bin 121: 2 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 0
Size: 251 Color: 1

Bin 122: 2 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 320 Color: 0

Bin 123: 2 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 420 Color: 0

Bin 124: 2 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 262 Color: 1

Bin 125: 2 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 1
Size: 198 Color: 0

Bin 126: 2 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 0
Size: 343 Color: 1

Bin 127: 2 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 380 Color: 1

Bin 128: 2 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 228 Color: 0

Bin 129: 2 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 0
Size: 365 Color: 1

Bin 130: 2 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 1
Size: 444 Color: 0

Bin 131: 2 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 491 Color: 1

Bin 132: 2 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 335 Color: 0

Bin 133: 2 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 422 Color: 1

Bin 134: 2 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 400 Color: 0

Bin 135: 2 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 1
Size: 363 Color: 0

Bin 136: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 0
Size: 358 Color: 1

Bin 137: 2 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 286 Color: 1

Bin 138: 2 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 1
Size: 234 Color: 0

Bin 139: 2 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 0
Size: 223 Color: 1

Bin 140: 2 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 210 Color: 0

Bin 141: 2 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 0
Size: 202 Color: 1

Bin 142: 2 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 201 Color: 0

Bin 143: 3 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 287 Color: 0
Size: 266 Color: 1

Bin 144: 3 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 273 Color: 0
Size: 255 Color: 1

Bin 145: 3 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 308 Color: 0

Bin 146: 3 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 0
Size: 477 Color: 1

Bin 147: 3 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 492 Color: 0

Bin 148: 3 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 1
Size: 464 Color: 0

Bin 149: 3 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 1
Size: 435 Color: 0

Bin 150: 3 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 415 Color: 0

Bin 151: 3 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 389 Color: 1

Bin 152: 3 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 0
Size: 376 Color: 1

Bin 153: 3 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 0
Size: 364 Color: 1

Bin 154: 3 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 359 Color: 0

Bin 155: 3 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 1
Size: 355 Color: 0

Bin 156: 3 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 338 Color: 1

Bin 157: 4 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 0
Size: 417 Color: 1

Bin 158: 4 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 1
Size: 376 Color: 0

Bin 159: 4 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 383 Color: 0

Bin 160: 4 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 394 Color: 0

Bin 161: 4 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 1
Size: 278 Color: 0

Bin 162: 4 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 298 Color: 1

Bin 163: 4 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 332 Color: 0

Bin 164: 4 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 393 Color: 0

Bin 165: 4 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 280 Color: 0

Bin 166: 4 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 236 Color: 1

Bin 167: 4 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 338 Color: 0

Bin 168: 4 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 482 Color: 1

Bin 169: 4 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 278 Color: 1

Bin 170: 4 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 363 Color: 0

Bin 171: 4 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 284 Color: 1

Bin 172: 5 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 244 Color: 0

Bin 173: 5 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 0
Size: 425 Color: 1

Bin 174: 5 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 463 Color: 0

Bin 175: 5 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 449 Color: 1

Bin 176: 5 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 0
Size: 380 Color: 1

Bin 177: 5 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 481 Color: 1

Bin 178: 5 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 479 Color: 0

Bin 179: 5 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 439 Color: 1

Bin 180: 5 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 347 Color: 0

Bin 181: 6 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 1
Size: 379 Color: 0

Bin 182: 6 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 0
Size: 428 Color: 1

Bin 183: 6 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 1
Size: 328 Color: 0

Bin 184: 6 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 300 Color: 1

Bin 185: 6 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 206 Color: 1

Bin 186: 6 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 331 Color: 0

Bin 187: 6 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 1
Size: 261 Color: 0

Bin 188: 7 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 294 Color: 1

Bin 189: 7 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 250 Color: 0

Bin 190: 7 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 337 Color: 1

Bin 191: 7 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 1
Size: 439 Color: 0

Bin 192: 7 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 395 Color: 1

Bin 193: 7 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 253 Color: 0

Bin 194: 7 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 380 Color: 1

Bin 195: 7 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 205 Color: 0

Bin 196: 7 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 197 Color: 0

Bin 197: 8 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 207 Color: 1

Bin 198: 8 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 219 Color: 0

Bin 199: 8 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 1
Size: 299 Color: 0

Bin 200: 8 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 0
Size: 334 Color: 1

Bin 201: 8 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 1
Size: 217 Color: 0

Bin 202: 8 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 1
Size: 308 Color: 0

Bin 203: 8 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 231 Color: 0

Bin 204: 9 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 1
Size: 462 Color: 0

Bin 205: 9 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 488 Color: 1

Bin 206: 9 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 238 Color: 1

Bin 207: 9 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 1
Size: 298 Color: 0

Bin 208: 9 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 0
Size: 389 Color: 1

Bin 209: 9 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 345 Color: 0

Bin 210: 9 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 0
Size: 451 Color: 1

Bin 211: 9 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 280 Color: 1

Bin 212: 9 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 251 Color: 0

Bin 213: 10 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 1
Size: 375 Color: 0

Bin 214: 10 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 332 Color: 0

Bin 215: 11 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 1
Size: 274 Color: 0

Bin 216: 11 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 408 Color: 1

Bin 217: 11 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 0
Size: 355 Color: 1

Bin 218: 12 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 474 Color: 0

Bin 219: 12 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 439 Color: 0

Bin 220: 13 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 1
Size: 302 Color: 0

Bin 221: 13 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 331 Color: 1

Bin 222: 15 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 372 Color: 1

Bin 223: 16 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 231 Color: 1

Bin 224: 17 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 201 Color: 1

Bin 225: 25 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 1
Size: 188 Color: 0
Size: 185 Color: 0

Bin 226: 35 of cap free
Amount of items: 2
Items: 
Size: 491 Color: 1
Size: 475 Color: 0

Bin 227: 50 of cap free
Amount of items: 2
Items: 
Size: 488 Color: 1
Size: 463 Color: 0

Bin 228: 55 of cap free
Amount of items: 2
Items: 
Size: 487 Color: 1
Size: 459 Color: 0

Total size: 227444
Total free space: 784

