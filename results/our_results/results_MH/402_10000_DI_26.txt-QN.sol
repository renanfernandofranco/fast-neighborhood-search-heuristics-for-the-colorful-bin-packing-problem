Capicity Bin: 6240
Lower Bound: 132

Bins used: 132
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 392
Size: 712 Color: 155
Size: 48 Color: 4

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 2528 Color: 261
Size: 1280 Color: 211
Size: 960 Color: 186
Size: 688 Color: 151
Size: 416 Color: 115
Size: 224 Color: 77
Size: 144 Color: 34

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 295
Size: 1960 Color: 243
Size: 384 Color: 114

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 4648 Color: 325
Size: 1112 Color: 198
Size: 256 Color: 90
Size: 224 Color: 76

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 5548 Color: 396
Size: 580 Color: 140
Size: 80 Color: 13
Size: 32 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3128 Color: 276
Size: 2648 Color: 269
Size: 464 Color: 122

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5094 Color: 354
Size: 958 Color: 185
Size: 188 Color: 58

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5336 Color: 379
Size: 840 Color: 170
Size: 64 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 333
Size: 1254 Color: 206
Size: 224 Color: 75

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 305
Size: 1762 Color: 237
Size: 352 Color: 108

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 398
Size: 564 Color: 138
Size: 104 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4422 Color: 317
Size: 1518 Color: 225
Size: 300 Color: 98

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 3920 Color: 296
Size: 2112 Color: 251
Size: 208 Color: 67

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 3394 Color: 287
Size: 2374 Color: 255
Size: 472 Color: 123

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5321 Color: 376
Size: 767 Color: 160
Size: 152 Color: 39

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5294 Color: 372
Size: 790 Color: 165
Size: 156 Color: 42

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 307
Size: 1736 Color: 235
Size: 336 Color: 106

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4700 Color: 327
Size: 1284 Color: 212
Size: 256 Color: 89

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 306
Size: 1740 Color: 236
Size: 344 Color: 107

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 3741 Color: 294
Size: 2083 Color: 247
Size: 416 Color: 117

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 390
Size: 666 Color: 148
Size: 132 Color: 30

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 389
Size: 676 Color: 149
Size: 128 Color: 28

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4983 Color: 349
Size: 1049 Color: 192
Size: 208 Color: 69

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 400
Size: 534 Color: 137
Size: 104 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 3588 Color: 288
Size: 2212 Color: 254
Size: 440 Color: 121

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 320
Size: 1438 Color: 221
Size: 284 Color: 95

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5608 Color: 402
Size: 600 Color: 143
Size: 32 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 358
Size: 930 Color: 179
Size: 184 Color: 56

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5312 Color: 375
Size: 688 Color: 150
Size: 240 Color: 83

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 322
Size: 1417 Color: 220
Size: 282 Color: 94

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 3352 Color: 285
Size: 2104 Color: 250
Size: 784 Color: 162

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5384 Color: 382
Size: 776 Color: 161
Size: 80 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 369
Size: 911 Color: 177
Size: 78 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5222 Color: 365
Size: 934 Color: 180
Size: 84 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 326
Size: 1324 Color: 214
Size: 256 Color: 91

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 348
Size: 1060 Color: 193
Size: 208 Color: 68

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 321
Size: 1532 Color: 227
Size: 176 Color: 49

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 391
Size: 652 Color: 147
Size: 128 Color: 29

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 324
Size: 1370 Color: 218
Size: 272 Color: 92

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4012 Color: 299
Size: 1996 Color: 246
Size: 232 Color: 79

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 292
Size: 2097 Color: 249
Size: 418 Color: 118

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4792 Color: 335
Size: 1336 Color: 216
Size: 112 Color: 23

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4930 Color: 344
Size: 1094 Color: 196
Size: 216 Color: 71

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4320 Color: 312
Size: 1440 Color: 222
Size: 480 Color: 124

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 3130 Color: 277
Size: 2594 Color: 264
Size: 516 Color: 133

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5326 Color: 377
Size: 762 Color: 158
Size: 152 Color: 40

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4323 Color: 313
Size: 1725 Color: 234
Size: 192 Color: 61

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 3353 Color: 286
Size: 2407 Color: 256
Size: 480 Color: 125

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 378
Size: 764 Color: 159
Size: 144 Color: 35

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5544 Color: 395
Size: 584 Color: 142
Size: 112 Color: 22

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4407 Color: 316
Size: 1529 Color: 226
Size: 304 Color: 101

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 3620 Color: 289
Size: 2188 Color: 253
Size: 432 Color: 120

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5110 Color: 356
Size: 942 Color: 182
Size: 188 Color: 60

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 332
Size: 1353 Color: 217
Size: 140 Color: 33

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5190 Color: 363
Size: 982 Color: 188
Size: 68 Color: 8

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4731 Color: 330
Size: 1259 Color: 208
Size: 250 Color: 85

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 5416 Color: 385
Size: 648 Color: 146
Size: 176 Color: 52

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5229 Color: 366
Size: 843 Color: 171
Size: 168 Color: 48

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 364
Size: 888 Color: 174
Size: 160 Color: 43

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 343
Size: 1208 Color: 204
Size: 112 Color: 21

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 362
Size: 881 Color: 173
Size: 176 Color: 50

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5486 Color: 393
Size: 630 Color: 145
Size: 124 Color: 27

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 399
Size: 582 Color: 141
Size: 72 Color: 10

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 4293 Color: 310
Size: 1623 Color: 230
Size: 324 Color: 103

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 401
Size: 532 Color: 136
Size: 104 Color: 16

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 315
Size: 1544 Color: 228
Size: 304 Color: 100

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 3734 Color: 293
Size: 2090 Color: 248
Size: 416 Color: 116

Bin 68: 0 of cap free
Amount of items: 4
Items: 
Size: 5112 Color: 357
Size: 952 Color: 184
Size: 144 Color: 36
Size: 32 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5419 Color: 386
Size: 797 Color: 166
Size: 24 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4452 Color: 318
Size: 1492 Color: 223
Size: 296 Color: 97

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 3121 Color: 272
Size: 2601 Color: 265
Size: 518 Color: 135

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 340
Size: 1148 Color: 200
Size: 224 Color: 78

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 3321 Color: 282
Size: 2433 Color: 260
Size: 486 Color: 128

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 3720 Color: 291
Size: 2408 Color: 257
Size: 112 Color: 20

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5016 Color: 350
Size: 1032 Color: 191
Size: 192 Color: 62

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5240 Color: 368
Size: 936 Color: 181
Size: 64 Color: 7

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 345
Size: 1084 Color: 195
Size: 216 Color: 72

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 4258 Color: 309
Size: 1654 Color: 231
Size: 328 Color: 104

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 353
Size: 982 Color: 187
Size: 196 Color: 64

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 3132 Color: 278
Size: 2604 Color: 268
Size: 504 Color: 129

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 4390 Color: 314
Size: 1662 Color: 232
Size: 188 Color: 57

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5232 Color: 367
Size: 848 Color: 172
Size: 160 Color: 44

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 3138 Color: 279
Size: 2586 Color: 263
Size: 516 Color: 134

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4846 Color: 339
Size: 1162 Color: 201
Size: 232 Color: 80

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4035 Color: 300
Size: 1839 Color: 240
Size: 366 Color: 111

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 3337 Color: 283
Size: 2421 Color: 259
Size: 482 Color: 126

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 3124 Color: 274
Size: 2604 Color: 267
Size: 512 Color: 130

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 381
Size: 729 Color: 156
Size: 144 Color: 37

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 3980 Color: 298
Size: 1884 Color: 241
Size: 376 Color: 112

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 303
Size: 1804 Color: 238
Size: 360 Color: 109

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 3148 Color: 281
Size: 2580 Color: 262
Size: 512 Color: 131

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 373
Size: 785 Color: 163
Size: 156 Color: 41

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 4094 Color: 304
Size: 1966 Color: 244
Size: 180 Color: 55

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 5518 Color: 394
Size: 602 Color: 144
Size: 120 Color: 26

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 308
Size: 1684 Color: 233
Size: 336 Color: 105

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 4040 Color: 301
Size: 1976 Color: 245
Size: 224 Color: 74

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5254 Color: 370
Size: 822 Color: 169
Size: 164 Color: 47

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 341
Size: 1284 Color: 213
Size: 48 Color: 5

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 3341 Color: 284
Size: 2417 Color: 258
Size: 482 Color: 127

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4051 Color: 302
Size: 1825 Color: 239
Size: 364 Color: 110

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 351
Size: 1014 Color: 190
Size: 200 Color: 66

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5053 Color: 352
Size: 991 Color: 189
Size: 196 Color: 63

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 361
Size: 900 Color: 175
Size: 176 Color: 53

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 397
Size: 578 Color: 139
Size: 112 Color: 19

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 3122 Color: 273
Size: 2602 Color: 266
Size: 516 Color: 132

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 4914 Color: 342
Size: 1106 Color: 197
Size: 220 Color: 73

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5358 Color: 380
Size: 810 Color: 167
Size: 72 Color: 9

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 328
Size: 1274 Color: 210
Size: 252 Color: 87

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 4456 Color: 319
Size: 1496 Color: 224
Size: 288 Color: 96

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 3970 Color: 297
Size: 1894 Color: 242
Size: 376 Color: 113

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 4307 Color: 311
Size: 1611 Color: 229
Size: 322 Color: 102

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5300 Color: 374
Size: 788 Color: 164
Size: 152 Color: 38

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 387
Size: 738 Color: 157
Size: 80 Color: 12

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 359
Size: 924 Color: 178
Size: 176 Color: 51

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 4715 Color: 329
Size: 1271 Color: 209
Size: 254 Color: 88

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 4735 Color: 331
Size: 1255 Color: 207
Size: 250 Color: 86

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 3146 Color: 280
Size: 2858 Color: 271
Size: 236 Color: 81

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 4811 Color: 337
Size: 1191 Color: 202
Size: 238 Color: 82

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 4947 Color: 346
Size: 1079 Color: 194
Size: 214 Color: 70

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4779 Color: 334
Size: 1219 Color: 205
Size: 242 Color: 84

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4573 Color: 323
Size: 1391 Color: 219
Size: 276 Color: 93

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 4795 Color: 336
Size: 1327 Color: 215
Size: 118 Color: 25

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 4951 Color: 347
Size: 1127 Color: 199
Size: 162 Color: 46

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 5435 Color: 388
Size: 691 Color: 152
Size: 114 Color: 24

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 3125 Color: 275
Size: 2811 Color: 270
Size: 304 Color: 99

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5390 Color: 383
Size: 710 Color: 154
Size: 140 Color: 32

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5267 Color: 371
Size: 811 Color: 168
Size: 162 Color: 45

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 3662 Color: 290
Size: 2150 Color: 252
Size: 428 Color: 119

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4843 Color: 338
Size: 1201 Color: 203
Size: 196 Color: 65

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5103 Color: 355
Size: 949 Color: 183
Size: 188 Color: 59

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 360
Size: 903 Color: 176
Size: 180 Color: 54

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5403 Color: 384
Size: 699 Color: 153
Size: 138 Color: 31

Total size: 823680
Total free space: 0

