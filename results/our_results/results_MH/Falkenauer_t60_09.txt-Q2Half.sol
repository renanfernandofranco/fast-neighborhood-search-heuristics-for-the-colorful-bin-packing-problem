Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 288 Color: 1
Size: 261 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 319 Color: 1
Size: 304 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 332 Color: 1
Size: 256 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 252 Color: 0
Size: 319 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 343 Color: 1
Size: 294 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 280 Color: 1
Size: 275 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 258 Color: 0
Size: 391 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 289 Color: 1
Size: 258 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 337 Color: 1
Size: 266 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 273 Color: 1
Size: 259 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 304 Color: 1
Size: 253 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 359 Color: 1
Size: 261 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 359 Color: 1
Size: 259 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 364 Color: 1
Size: 263 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 369 Color: 1
Size: 262 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 298 Color: 1
Size: 276 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 316 Color: 1
Size: 308 Color: 0
Size: 376 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 307 Color: 1
Size: 276 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 254 Color: 0
Size: 263 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 304 Color: 1
Size: 254 Color: 0

Total size: 20000
Total free space: 0

