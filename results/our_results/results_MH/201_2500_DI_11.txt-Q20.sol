Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1000 Color: 19
Size: 840 Color: 15
Size: 504 Color: 1
Size: 128 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 2212 Color: 0
Size: 220 Color: 18
Size: 32 Color: 16
Size: 8 Color: 9

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1704 Color: 2
Size: 384 Color: 5
Size: 272 Color: 12
Size: 112 Color: 9

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1552 Color: 11
Size: 576 Color: 6
Size: 272 Color: 7
Size: 72 Color: 14

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1797 Color: 16
Size: 563 Color: 11
Size: 96 Color: 17
Size: 16 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 11
Size: 604 Color: 10
Size: 120 Color: 5

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 1686 Color: 7
Size: 658 Color: 14
Size: 88 Color: 5
Size: 40 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1652 Color: 8
Size: 684 Color: 6
Size: 136 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 6
Size: 846 Color: 2
Size: 168 Color: 17

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 2030 Color: 11
Size: 370 Color: 18
Size: 64 Color: 9
Size: 8 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 5
Size: 311 Color: 3
Size: 60 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 8
Size: 268 Color: 18
Size: 40 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1357 Color: 6
Size: 931 Color: 18
Size: 184 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 12
Size: 892 Color: 6
Size: 176 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 19
Size: 751 Color: 8
Size: 150 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1540 Color: 3
Size: 780 Color: 8
Size: 152 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 10
Size: 830 Color: 12
Size: 164 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 8
Size: 1028 Color: 16
Size: 200 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2093 Color: 4
Size: 317 Color: 7
Size: 62 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 4
Size: 230 Color: 15
Size: 44 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1903 Color: 12
Size: 475 Color: 16
Size: 94 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 7
Size: 838 Color: 9
Size: 164 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 12
Size: 387 Color: 6
Size: 66 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 4
Size: 364 Color: 4
Size: 72 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 9
Size: 283 Color: 10
Size: 38 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 0
Size: 621 Color: 4
Size: 122 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 13
Size: 302 Color: 5
Size: 60 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 4
Size: 316 Color: 19
Size: 40 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 9
Size: 1022 Color: 13
Size: 204 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 18
Size: 662 Color: 16
Size: 132 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 8
Size: 556 Color: 10
Size: 24 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1985 Color: 1
Size: 407 Color: 14
Size: 80 Color: 5

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 10
Size: 366 Color: 13
Size: 72 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 17
Size: 889 Color: 1
Size: 176 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 7
Size: 275 Color: 6
Size: 54 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 16
Size: 739 Color: 13
Size: 146 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 3
Size: 787 Color: 3
Size: 156 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 3
Size: 233 Color: 6
Size: 38 Color: 7

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 9
Size: 955 Color: 1
Size: 130 Color: 19

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 10
Size: 670 Color: 16
Size: 132 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 10
Size: 818 Color: 10
Size: 160 Color: 5

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 19
Size: 361 Color: 5
Size: 72 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1943 Color: 6
Size: 441 Color: 3
Size: 88 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2073 Color: 7
Size: 333 Color: 16
Size: 66 Color: 19

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2179 Color: 7
Size: 245 Color: 2
Size: 48 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 19
Size: 540 Color: 14
Size: 104 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 5
Size: 1093 Color: 14
Size: 142 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 5
Size: 546 Color: 17
Size: 108 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 2
Size: 446 Color: 14
Size: 88 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 17
Size: 1030 Color: 11
Size: 204 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 6
Size: 1103 Color: 12
Size: 128 Color: 15

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 11
Size: 444 Color: 18
Size: 88 Color: 9

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 1
Size: 1010 Color: 0
Size: 200 Color: 5

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 16
Size: 822 Color: 6
Size: 164 Color: 5

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 2
Size: 729 Color: 11
Size: 144 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 8
Size: 1022 Color: 1
Size: 196 Color: 15

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 14
Size: 693 Color: 4
Size: 42 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2163 Color: 6
Size: 259 Color: 2
Size: 50 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 12
Size: 450 Color: 5
Size: 88 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 2
Size: 758 Color: 8
Size: 56 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 12
Size: 515 Color: 9
Size: 102 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 19
Size: 678 Color: 19
Size: 132 Color: 16

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 13
Size: 542 Color: 12
Size: 104 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 14
Size: 334 Color: 7
Size: 20 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 16
Size: 262 Color: 17
Size: 52 Color: 5

Total size: 160680
Total free space: 0

