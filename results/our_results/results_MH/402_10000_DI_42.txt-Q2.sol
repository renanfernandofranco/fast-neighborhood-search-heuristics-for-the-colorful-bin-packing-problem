Capicity Bin: 6624
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5552 Color: 1
Size: 904 Color: 0
Size: 168 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 0
Size: 594 Color: 1
Size: 116 Color: 0

Bin 3: 0 of cap free
Amount of items: 7
Items: 
Size: 2480 Color: 1
Size: 2280 Color: 0
Size: 800 Color: 0
Size: 392 Color: 1
Size: 392 Color: 0
Size: 240 Color: 1
Size: 40 Color: 1

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 3264 Color: 0
Size: 2888 Color: 0
Size: 296 Color: 0
Size: 104 Color: 1
Size: 72 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 0
Size: 602 Color: 1
Size: 116 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5546 Color: 0
Size: 902 Color: 0
Size: 176 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 0
Size: 788 Color: 1
Size: 152 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 1
Size: 1644 Color: 1
Size: 328 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4564 Color: 1
Size: 1724 Color: 0
Size: 336 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 3632 Color: 0
Size: 2504 Color: 0
Size: 488 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5759 Color: 1
Size: 721 Color: 0
Size: 144 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5470 Color: 1
Size: 962 Color: 0
Size: 192 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 1
Size: 1145 Color: 1
Size: 228 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5515 Color: 1
Size: 925 Color: 0
Size: 184 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 1
Size: 580 Color: 0
Size: 112 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 1
Size: 613 Color: 0
Size: 122 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 0
Size: 1868 Color: 0
Size: 216 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 1
Size: 818 Color: 0
Size: 160 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 1
Size: 1510 Color: 1
Size: 300 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 1
Size: 1124 Color: 0
Size: 224 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5715 Color: 0
Size: 759 Color: 1
Size: 150 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5851 Color: 0
Size: 645 Color: 0
Size: 128 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5858 Color: 0
Size: 642 Color: 1
Size: 124 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 1
Size: 738 Color: 0
Size: 144 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 3325 Color: 0
Size: 2751 Color: 0
Size: 548 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 0
Size: 1478 Color: 1
Size: 292 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 1
Size: 2417 Color: 1
Size: 482 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 0
Size: 1275 Color: 1
Size: 254 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 1
Size: 755 Color: 0
Size: 26 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 3476 Color: 1
Size: 2628 Color: 0
Size: 520 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 3866 Color: 0
Size: 2302 Color: 1
Size: 456 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 3330 Color: 0
Size: 3138 Color: 0
Size: 156 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 3844 Color: 0
Size: 2508 Color: 0
Size: 272 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5471 Color: 1
Size: 961 Color: 0
Size: 192 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5076 Color: 0
Size: 1292 Color: 0
Size: 256 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4935 Color: 0
Size: 1409 Color: 1
Size: 280 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5397 Color: 0
Size: 1023 Color: 1
Size: 204 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 3314 Color: 1
Size: 2762 Color: 0
Size: 548 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4593 Color: 0
Size: 1693 Color: 1
Size: 338 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 1
Size: 1690 Color: 1
Size: 336 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4698 Color: 0
Size: 1606 Color: 1
Size: 320 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5459 Color: 1
Size: 971 Color: 0
Size: 194 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 0
Size: 974 Color: 0
Size: 192 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 1
Size: 1564 Color: 0
Size: 312 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 0
Size: 717 Color: 0
Size: 142 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5919 Color: 1
Size: 589 Color: 1
Size: 116 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5577 Color: 1
Size: 977 Color: 1
Size: 70 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 3940 Color: 0
Size: 2244 Color: 0
Size: 440 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4093 Color: 1
Size: 2111 Color: 1
Size: 420 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4228 Color: 1
Size: 2004 Color: 1
Size: 392 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4300 Color: 0
Size: 1940 Color: 1
Size: 384 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 3313 Color: 0
Size: 2761 Color: 0
Size: 550 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 0
Size: 1250 Color: 0
Size: 248 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5818 Color: 1
Size: 674 Color: 1
Size: 132 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5930 Color: 0
Size: 582 Color: 1
Size: 112 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5816 Color: 1
Size: 680 Color: 1
Size: 128 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 1
Size: 1196 Color: 0
Size: 72 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 0
Size: 914 Color: 0
Size: 180 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5801 Color: 0
Size: 687 Color: 0
Size: 136 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4458 Color: 0
Size: 1806 Color: 0
Size: 360 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5644 Color: 1
Size: 820 Color: 0
Size: 160 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 0
Size: 854 Color: 1
Size: 168 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 1
Size: 662 Color: 0
Size: 128 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 0
Size: 876 Color: 0
Size: 168 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5597 Color: 1
Size: 857 Color: 0
Size: 170 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 0
Size: 724 Color: 1
Size: 144 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 3964 Color: 1
Size: 2220 Color: 0
Size: 440 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 3753 Color: 1
Size: 2393 Color: 0
Size: 478 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5605 Color: 1
Size: 851 Color: 0
Size: 168 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 782 Color: 1
Size: 152 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5569 Color: 0
Size: 881 Color: 1
Size: 174 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 0
Size: 1204 Color: 1
Size: 232 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 3650 Color: 0
Size: 2482 Color: 1
Size: 492 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 0
Size: 858 Color: 0
Size: 160 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5304 Color: 0
Size: 1224 Color: 1
Size: 96 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5370 Color: 1
Size: 1046 Color: 0
Size: 208 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4974 Color: 0
Size: 1378 Color: 0
Size: 272 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5239 Color: 0
Size: 1159 Color: 0
Size: 226 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5898 Color: 0
Size: 686 Color: 0
Size: 40 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4621 Color: 1
Size: 1671 Color: 1
Size: 332 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5431 Color: 0
Size: 995 Color: 1
Size: 198 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 0
Size: 1028 Color: 1
Size: 200 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 0
Size: 708 Color: 1
Size: 136 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5739 Color: 0
Size: 739 Color: 1
Size: 146 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 0
Size: 920 Color: 1
Size: 544 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5665 Color: 0
Size: 801 Color: 0
Size: 158 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 0
Size: 2182 Color: 0
Size: 176 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5298 Color: 1
Size: 1106 Color: 1
Size: 220 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 3317 Color: 0
Size: 2757 Color: 1
Size: 550 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5162 Color: 1
Size: 1222 Color: 1
Size: 240 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5702 Color: 0
Size: 826 Color: 0
Size: 96 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 1
Size: 988 Color: 0
Size: 192 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 1
Size: 1356 Color: 0
Size: 264 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 3316 Color: 1
Size: 2804 Color: 1
Size: 504 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 5935 Color: 0
Size: 575 Color: 1
Size: 114 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5553 Color: 0
Size: 893 Color: 1
Size: 178 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 1
Size: 644 Color: 0
Size: 128 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 5107 Color: 1
Size: 1265 Color: 0
Size: 252 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 5211 Color: 1
Size: 1179 Color: 0
Size: 234 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 4338 Color: 1
Size: 1906 Color: 0
Size: 380 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 5738 Color: 0
Size: 742 Color: 1
Size: 144 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5138 Color: 1
Size: 1242 Color: 0
Size: 244 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 3765 Color: 1
Size: 2383 Color: 1
Size: 476 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5826 Color: 1
Size: 666 Color: 1
Size: 132 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 3321 Color: 0
Size: 2753 Color: 1
Size: 550 Color: 0

Bin 106: 0 of cap free
Amount of items: 4
Items: 
Size: 3348 Color: 0
Size: 2732 Color: 0
Size: 456 Color: 1
Size: 88 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5331 Color: 0
Size: 1079 Color: 0
Size: 214 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 1
Size: 1430 Color: 1
Size: 284 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 1
Size: 722 Color: 0
Size: 140 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 0
Size: 802 Color: 0
Size: 156 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 0
Size: 924 Color: 1
Size: 184 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5338 Color: 1
Size: 1074 Color: 1
Size: 212 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 3322 Color: 1
Size: 2754 Color: 0
Size: 548 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 1428 Color: 0
Size: 280 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 5282 Color: 1
Size: 1122 Color: 0
Size: 220 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5924 Color: 1
Size: 588 Color: 0
Size: 112 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 5669 Color: 1
Size: 877 Color: 0
Size: 78 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 5371 Color: 1
Size: 1045 Color: 0
Size: 208 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 4123 Color: 0
Size: 2085 Color: 1
Size: 416 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 4907 Color: 0
Size: 1431 Color: 0
Size: 286 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 4811 Color: 1
Size: 1569 Color: 1
Size: 244 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 5778 Color: 1
Size: 706 Color: 1
Size: 140 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 5709 Color: 1
Size: 811 Color: 1
Size: 104 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 4365 Color: 1
Size: 1883 Color: 1
Size: 376 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4783 Color: 1
Size: 1535 Color: 1
Size: 306 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 0
Size: 673 Color: 0
Size: 134 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 1
Size: 643 Color: 1
Size: 94 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 4062 Color: 1
Size: 2138 Color: 1
Size: 424 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 4391 Color: 0
Size: 1861 Color: 0
Size: 372 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 5045 Color: 0
Size: 1383 Color: 1
Size: 196 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 5927 Color: 1
Size: 581 Color: 0
Size: 116 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 1
Size: 1299 Color: 1
Size: 258 Color: 0

Total size: 874368
Total free space: 0

