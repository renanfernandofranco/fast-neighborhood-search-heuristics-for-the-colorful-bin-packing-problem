Capicity Bin: 1001
Lower Bound: 228

Bins used: 229
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 548 Color: 0
Size: 313 Color: 0
Size: 140 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 0
Size: 127 Color: 0
Size: 110 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 1
Size: 288 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 1
Size: 269 Color: 0
Size: 121 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 0
Size: 317 Color: 1
Size: 159 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 0
Size: 313 Color: 1
Size: 156 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 0
Size: 154 Color: 0
Size: 101 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 0
Size: 198 Color: 1
Size: 154 Color: 0

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 1
Size: 233 Color: 1
Size: 147 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 1
Size: 119 Color: 0
Size: 108 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 772 Color: 0
Size: 115 Color: 1
Size: 114 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 0
Size: 144 Color: 1
Size: 101 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 541 Color: 0
Size: 272 Color: 1
Size: 188 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 159 Color: 1
Size: 122 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 0
Size: 262 Color: 0
Size: 136 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 577 Color: 0
Size: 323 Color: 1
Size: 101 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 392 Color: 0
Size: 141 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 0
Size: 267 Color: 1
Size: 167 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 0
Size: 194 Color: 0
Size: 101 Color: 1

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 1
Size: 150 Color: 0
Size: 144 Color: 1

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 419 Color: 0

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 389 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 1
Size: 202 Color: 1
Size: 167 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 0
Size: 161 Color: 0
Size: 146 Color: 1

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 322 Color: 0

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 0

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 0
Size: 233 Color: 1

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 1
Size: 460 Color: 0

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 399 Color: 0

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 462 Color: 0

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 208 Color: 0

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 357 Color: 0

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 483 Color: 1

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 0
Size: 481 Color: 1

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 1
Size: 481 Color: 0

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 1
Size: 480 Color: 0

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 475 Color: 1

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 462 Color: 1

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 0

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 1

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 444 Color: 0

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 0
Size: 436 Color: 1

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 417 Color: 0

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 1
Size: 410 Color: 0

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 1
Size: 410 Color: 0

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 0
Size: 405 Color: 1

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 0
Size: 400 Color: 1

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 0

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 0
Size: 377 Color: 1

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 1
Size: 375 Color: 0

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 1
Size: 373 Color: 0

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 0
Size: 358 Color: 1

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 340 Color: 0

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 1

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 304 Color: 1

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 1

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 0
Size: 273 Color: 1

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 1
Size: 270 Color: 0

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 0

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 0
Size: 263 Color: 1

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 0

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 0
Size: 253 Color: 1

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 251 Color: 0

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 0
Size: 243 Color: 1

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 1
Size: 236 Color: 0

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 1

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 1
Size: 211 Color: 0

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 0
Size: 207 Color: 1

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 0
Size: 320 Color: 1
Size: 137 Color: 1

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 603 Color: 0
Size: 278 Color: 0
Size: 119 Color: 1

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 464 Color: 1

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 667 Color: 1
Size: 182 Color: 1
Size: 151 Color: 0

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 247 Color: 1

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 0
Size: 205 Color: 1
Size: 156 Color: 0

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 248 Color: 1

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 414 Color: 0

Bin 82: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 483 Color: 0

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 292 Color: 1

Bin 84: 1 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 226 Color: 1

Bin 85: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 305 Color: 1

Bin 86: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 0
Size: 268 Color: 1

Bin 87: 1 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 380 Color: 1

Bin 88: 1 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 428 Color: 1

Bin 89: 1 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 0
Size: 349 Color: 0
Size: 127 Color: 1

Bin 90: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 199 Color: 1

Bin 91: 1 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 387 Color: 1

Bin 92: 1 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 229 Color: 0

Bin 93: 1 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 336 Color: 0

Bin 94: 1 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 398 Color: 0

Bin 95: 1 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 495 Color: 1

Bin 96: 1 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 494 Color: 0

Bin 97: 1 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 477 Color: 0

Bin 98: 1 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 1
Size: 472 Color: 0

Bin 99: 1 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 463 Color: 0

Bin 100: 1 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 454 Color: 0

Bin 101: 1 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 438 Color: 1

Bin 102: 1 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 0
Size: 425 Color: 1

Bin 103: 1 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 393 Color: 0

Bin 104: 1 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 335 Color: 1

Bin 105: 1 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 0
Size: 265 Color: 1

Bin 106: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 259 Color: 1

Bin 107: 1 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 250 Color: 1

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 1
Size: 181 Color: 1
Size: 163 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 0
Size: 163 Color: 0
Size: 143 Color: 1

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 1
Size: 125 Color: 0
Size: 122 Color: 0

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 0
Size: 320 Color: 0
Size: 139 Color: 1

Bin 112: 2 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 439 Color: 0

Bin 113: 2 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 0
Size: 428 Color: 1

Bin 114: 2 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 433 Color: 1

Bin 115: 2 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 1
Size: 190 Color: 0
Size: 105 Color: 1

Bin 116: 2 of cap free
Amount of items: 3
Items: 
Size: 716 Color: 0
Size: 182 Color: 1
Size: 101 Color: 0

Bin 117: 2 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 1
Size: 398 Color: 0

Bin 118: 2 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 373 Color: 1

Bin 119: 2 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 336 Color: 0

Bin 120: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 1
Size: 358 Color: 0

Bin 121: 2 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 336 Color: 0

Bin 122: 2 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 1
Size: 430 Color: 0

Bin 123: 2 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 426 Color: 0

Bin 124: 3 of cap free
Amount of items: 3
Items: 
Size: 572 Color: 1
Size: 316 Color: 1
Size: 110 Color: 0

Bin 125: 3 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 1
Size: 264 Color: 0

Bin 126: 3 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 445 Color: 0

Bin 127: 3 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 0
Size: 346 Color: 1

Bin 128: 3 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 0
Size: 242 Color: 1

Bin 129: 3 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 274 Color: 1

Bin 130: 3 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 1
Size: 257 Color: 1
Size: 117 Color: 0

Bin 131: 3 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 292 Color: 0

Bin 132: 3 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 290 Color: 1

Bin 133: 3 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 1
Size: 272 Color: 0

Bin 134: 3 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 484 Color: 1

Bin 135: 3 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 488 Color: 0

Bin 136: 3 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 438 Color: 0

Bin 137: 3 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 0
Size: 424 Color: 1

Bin 138: 4 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 1
Size: 190 Color: 1
Size: 124 Color: 0

Bin 139: 4 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 1
Size: 443 Color: 0

Bin 140: 4 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 0
Size: 359 Color: 1

Bin 141: 4 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 0
Size: 366 Color: 1

Bin 142: 4 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 0
Size: 214 Color: 1
Size: 165 Color: 1

Bin 143: 4 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 0
Size: 190 Color: 1
Size: 176 Color: 0

Bin 144: 4 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 223 Color: 0

Bin 145: 4 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 257 Color: 0

Bin 146: 4 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 1
Size: 488 Color: 0

Bin 147: 4 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 1
Size: 196 Color: 0

Bin 148: 4 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 0
Size: 228 Color: 1

Bin 149: 4 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 230 Color: 0

Bin 150: 4 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 462 Color: 0

Bin 151: 5 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 1
Size: 267 Color: 0
Size: 112 Color: 1

Bin 152: 5 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 468 Color: 1

Bin 153: 5 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 0
Size: 324 Color: 1

Bin 154: 6 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 263 Color: 0

Bin 155: 6 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 372 Color: 1

Bin 156: 6 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 290 Color: 1

Bin 157: 6 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 280 Color: 1

Bin 158: 6 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 302 Color: 0

Bin 159: 6 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 290 Color: 1

Bin 160: 6 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 0
Size: 445 Color: 1

Bin 161: 6 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 1
Size: 371 Color: 0

Bin 162: 6 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 296 Color: 1

Bin 163: 6 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 316 Color: 0

Bin 164: 6 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 336 Color: 0

Bin 165: 7 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 335 Color: 0

Bin 166: 7 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 386 Color: 1

Bin 167: 7 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 417 Color: 1

Bin 168: 7 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 194 Color: 0

Bin 169: 8 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 489 Color: 1

Bin 170: 8 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 212 Color: 1

Bin 171: 8 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 325 Color: 1

Bin 172: 8 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 0
Size: 479 Color: 1

Bin 173: 8 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 0
Size: 340 Color: 1

Bin 174: 9 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 469 Color: 0

Bin 175: 9 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 464 Color: 1

Bin 176: 9 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 323 Color: 0

Bin 177: 9 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 1
Size: 276 Color: 0

Bin 178: 9 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 0
Size: 399 Color: 1

Bin 179: 10 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 318 Color: 0

Bin 180: 10 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 1
Size: 358 Color: 0

Bin 181: 10 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 0
Size: 442 Color: 1

Bin 182: 10 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 1
Size: 231 Color: 0

Bin 183: 10 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 1
Size: 443 Color: 0

Bin 184: 10 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 210 Color: 0

Bin 185: 11 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 0
Size: 380 Color: 1

Bin 186: 11 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 1
Size: 233 Color: 0

Bin 187: 12 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 316 Color: 0

Bin 188: 12 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 317 Color: 0

Bin 189: 12 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 467 Color: 0

Bin 190: 12 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 210 Color: 0

Bin 191: 13 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 272 Color: 1

Bin 192: 14 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 1
Size: 487 Color: 0

Bin 193: 14 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 480 Color: 1

Bin 194: 14 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 187 Color: 0

Bin 195: 15 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 1
Size: 258 Color: 0

Bin 196: 15 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 286 Color: 0

Bin 197: 15 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 171 Color: 0
Size: 117 Color: 0

Bin 198: 15 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 483 Color: 0

Bin 199: 15 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 420 Color: 1

Bin 200: 16 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 1
Size: 486 Color: 0

Bin 201: 16 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 1
Size: 291 Color: 0

Bin 202: 17 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 1
Size: 313 Color: 0

Bin 203: 17 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 402 Color: 0

Bin 204: 17 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 365 Color: 1

Bin 205: 19 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 478 Color: 1

Bin 206: 20 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 350 Color: 0

Bin 207: 21 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 0
Size: 140 Color: 1
Size: 118 Color: 1

Bin 208: 21 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 1
Size: 312 Color: 0

Bin 209: 21 of cap free
Amount of items: 3
Items: 
Size: 616 Color: 0
Size: 188 Color: 1
Size: 176 Color: 1

Bin 210: 21 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 1
Size: 279 Color: 0

Bin 211: 21 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 0
Size: 208 Color: 1

Bin 212: 22 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 1
Size: 362 Color: 0

Bin 213: 23 of cap free
Amount of items: 3
Items: 
Size: 548 Color: 1
Size: 258 Color: 0
Size: 172 Color: 0

Bin 214: 25 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 364 Color: 1

Bin 215: 29 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 180 Color: 0

Bin 216: 29 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 1
Size: 396 Color: 0

Bin 217: 31 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 0
Size: 409 Color: 1

Bin 218: 32 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 0
Size: 204 Color: 1

Bin 219: 32 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 171 Color: 0

Bin 220: 33 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 0
Size: 259 Color: 0
Size: 157 Color: 1

Bin 221: 33 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 1
Size: 402 Color: 0

Bin 222: 34 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 410 Color: 1

Bin 223: 38 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 208 Color: 1

Bin 224: 39 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 0
Size: 412 Color: 1

Bin 225: 40 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 0
Size: 198 Color: 1

Bin 226: 43 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 0
Size: 178 Color: 1

Bin 227: 44 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 0
Size: 412 Color: 1

Bin 228: 48 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 1
Size: 258 Color: 0

Bin 229: 225 of cap free
Amount of items: 2
Items: 
Size: 467 Color: 0
Size: 309 Color: 1

Total size: 227588
Total free space: 1641

