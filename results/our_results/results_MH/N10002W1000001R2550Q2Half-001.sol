Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 457384 Color: 1
Size: 283745 Color: 1
Size: 258872 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 361299 Color: 1
Size: 328886 Color: 1
Size: 309816 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 418555 Color: 1
Size: 319085 Color: 1
Size: 262361 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 499763 Color: 1
Size: 250196 Color: 1
Size: 250042 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 475717 Color: 1
Size: 270702 Color: 1
Size: 253582 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 485260 Color: 1
Size: 262889 Color: 1
Size: 251852 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 463752 Color: 1
Size: 270242 Color: 1
Size: 266007 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 416940 Color: 1
Size: 308894 Color: 1
Size: 274167 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 438836 Color: 1
Size: 308966 Color: 1
Size: 252199 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 419100 Color: 1
Size: 293775 Color: 0
Size: 287126 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 414687 Color: 1
Size: 331311 Color: 1
Size: 254003 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 498174 Color: 1
Size: 251572 Color: 1
Size: 250255 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 468248 Color: 1
Size: 276864 Color: 1
Size: 254889 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 397408 Color: 1
Size: 318706 Color: 0
Size: 283887 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 382408 Color: 1
Size: 330132 Color: 1
Size: 287461 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 494098 Color: 1
Size: 254165 Color: 1
Size: 251738 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 420680 Color: 1
Size: 317590 Color: 1
Size: 261731 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375605 Color: 1
Size: 337244 Color: 1
Size: 287152 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 419231 Color: 1
Size: 311138 Color: 1
Size: 269632 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 365655 Color: 1
Size: 353342 Color: 1
Size: 281004 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 496712 Color: 1
Size: 252582 Color: 1
Size: 250707 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 371923 Color: 1
Size: 338642 Color: 1
Size: 289436 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 467511 Color: 1
Size: 281965 Color: 1
Size: 250525 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 424677 Color: 1
Size: 291796 Color: 1
Size: 283528 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 459516 Color: 1
Size: 278823 Color: 1
Size: 261662 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 495520 Color: 1
Size: 252394 Color: 1
Size: 252087 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 372049 Color: 1
Size: 351715 Color: 1
Size: 276237 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 422086 Color: 1
Size: 306632 Color: 1
Size: 271283 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 368933 Color: 1
Size: 333273 Color: 1
Size: 297795 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 393485 Color: 1
Size: 336559 Color: 1
Size: 269957 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 434546 Color: 1
Size: 297771 Color: 1
Size: 267684 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 474807 Color: 1
Size: 269894 Color: 1
Size: 255300 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 415005 Color: 1
Size: 313742 Color: 1
Size: 271254 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 399898 Color: 1
Size: 305393 Color: 1
Size: 294710 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 443300 Color: 1
Size: 301388 Color: 1
Size: 255313 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 421840 Color: 1
Size: 313870 Color: 1
Size: 264291 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 358171 Color: 1
Size: 351628 Color: 1
Size: 290202 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 394872 Color: 1
Size: 322170 Color: 1
Size: 282959 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 457960 Color: 1
Size: 274262 Color: 0
Size: 267779 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 446748 Color: 1
Size: 287319 Color: 0
Size: 265934 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 417363 Color: 1
Size: 296408 Color: 0
Size: 286230 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 383144 Color: 1
Size: 339781 Color: 1
Size: 277076 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 447372 Color: 1
Size: 301144 Color: 1
Size: 251485 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 421082 Color: 1
Size: 311480 Color: 1
Size: 267439 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 485871 Color: 1
Size: 257075 Color: 1
Size: 257055 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 405645 Color: 1
Size: 326917 Color: 1
Size: 267439 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 456086 Color: 1
Size: 285932 Color: 1
Size: 257983 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 471799 Color: 1
Size: 265744 Color: 1
Size: 262458 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 458551 Color: 1
Size: 274114 Color: 0
Size: 267336 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 386883 Color: 1
Size: 309355 Color: 1
Size: 303763 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 367921 Color: 1
Size: 322757 Color: 0
Size: 309323 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 366837 Color: 1
Size: 332115 Color: 1
Size: 301049 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 377643 Color: 1
Size: 345683 Color: 1
Size: 276675 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 390479 Color: 1
Size: 338468 Color: 1
Size: 271054 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 456500 Color: 1
Size: 281068 Color: 1
Size: 262433 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 466482 Color: 1
Size: 272072 Color: 0
Size: 261447 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 476989 Color: 1
Size: 271687 Color: 1
Size: 251325 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 476425 Color: 1
Size: 272589 Color: 1
Size: 250987 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 497217 Color: 1
Size: 252651 Color: 1
Size: 250133 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 369498 Color: 1
Size: 330356 Color: 1
Size: 300147 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 406491 Color: 1
Size: 298692 Color: 0
Size: 294818 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 388521 Color: 1
Size: 339771 Color: 1
Size: 271709 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 386276 Color: 1
Size: 335132 Color: 1
Size: 278593 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 437669 Color: 1
Size: 293293 Color: 1
Size: 269039 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 496378 Color: 1
Size: 252175 Color: 1
Size: 251448 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 488359 Color: 1
Size: 261615 Color: 1
Size: 250027 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 479500 Color: 1
Size: 269297 Color: 1
Size: 251204 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 408555 Color: 1
Size: 311165 Color: 1
Size: 280281 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 410546 Color: 1
Size: 335894 Color: 1
Size: 253561 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 415044 Color: 1
Size: 325319 Color: 1
Size: 259638 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 379768 Color: 1
Size: 327876 Color: 1
Size: 292357 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 400204 Color: 1
Size: 339190 Color: 1
Size: 260607 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 351237 Color: 1
Size: 337450 Color: 1
Size: 311314 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 437561 Color: 1
Size: 281476 Color: 1
Size: 280964 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 495999 Color: 1
Size: 253326 Color: 1
Size: 250676 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 488800 Color: 1
Size: 255810 Color: 1
Size: 255391 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 424922 Color: 1
Size: 302965 Color: 1
Size: 272114 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 455878 Color: 1
Size: 289636 Color: 1
Size: 254487 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 455512 Color: 1
Size: 292102 Color: 1
Size: 252387 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 394254 Color: 1
Size: 350936 Color: 1
Size: 254811 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 378276 Color: 1
Size: 340214 Color: 1
Size: 281511 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 373790 Color: 1
Size: 358520 Color: 1
Size: 267691 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 403095 Color: 1
Size: 327869 Color: 1
Size: 269037 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 409296 Color: 1
Size: 316920 Color: 0
Size: 273785 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 426351 Color: 1
Size: 318337 Color: 1
Size: 255313 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 435130 Color: 1
Size: 298022 Color: 1
Size: 266849 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 405254 Color: 1
Size: 332055 Color: 1
Size: 262692 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 450356 Color: 1
Size: 297656 Color: 1
Size: 251989 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 398358 Color: 1
Size: 305395 Color: 1
Size: 296248 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 449456 Color: 1
Size: 299069 Color: 1
Size: 251476 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 362976 Color: 1
Size: 345562 Color: 1
Size: 291463 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 430862 Color: 1
Size: 307930 Color: 1
Size: 261209 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 471127 Color: 1
Size: 270107 Color: 1
Size: 258767 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 445008 Color: 1
Size: 296985 Color: 1
Size: 258008 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 442043 Color: 1
Size: 304465 Color: 1
Size: 253493 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 433345 Color: 1
Size: 299918 Color: 1
Size: 266738 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 388856 Color: 1
Size: 308390 Color: 0
Size: 302755 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 370318 Color: 1
Size: 336978 Color: 1
Size: 292705 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 428598 Color: 1
Size: 300833 Color: 1
Size: 270570 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 372826 Color: 1
Size: 354834 Color: 1
Size: 272341 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 406649 Color: 1
Size: 299048 Color: 1
Size: 294304 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 462371 Color: 1
Size: 287142 Color: 1
Size: 250488 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 437476 Color: 1
Size: 300390 Color: 1
Size: 262135 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 410290 Color: 1
Size: 333338 Color: 1
Size: 256373 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 461246 Color: 1
Size: 288331 Color: 1
Size: 250424 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 473970 Color: 1
Size: 274325 Color: 1
Size: 251706 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 415463 Color: 1
Size: 308881 Color: 1
Size: 275657 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 487907 Color: 1
Size: 261550 Color: 1
Size: 250544 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 356100 Color: 1
Size: 355969 Color: 1
Size: 287932 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 419336 Color: 1
Size: 315801 Color: 1
Size: 264864 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 477649 Color: 1
Size: 262368 Color: 0
Size: 259984 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 421387 Color: 1
Size: 318665 Color: 1
Size: 259949 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 370943 Color: 1
Size: 340930 Color: 1
Size: 288128 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 425174 Color: 1
Size: 302000 Color: 1
Size: 272827 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 459257 Color: 1
Size: 282865 Color: 1
Size: 257879 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 483241 Color: 1
Size: 265115 Color: 1
Size: 251645 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 400463 Color: 1
Size: 319327 Color: 0
Size: 280211 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 397320 Color: 1
Size: 339599 Color: 1
Size: 263082 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 364869 Color: 1
Size: 341977 Color: 1
Size: 293155 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 477688 Color: 1
Size: 264531 Color: 1
Size: 257782 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 365073 Color: 1
Size: 324620 Color: 1
Size: 310308 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 492748 Color: 1
Size: 256738 Color: 1
Size: 250515 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 385271 Color: 1
Size: 363451 Color: 1
Size: 251279 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 412409 Color: 1
Size: 321416 Color: 1
Size: 266176 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 455427 Color: 1
Size: 287388 Color: 1
Size: 257186 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 387783 Color: 1
Size: 358569 Color: 1
Size: 253649 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 365158 Color: 1
Size: 340295 Color: 1
Size: 294548 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 401729 Color: 1
Size: 342857 Color: 1
Size: 255415 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 454186 Color: 1
Size: 292932 Color: 1
Size: 252883 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 488947 Color: 1
Size: 258672 Color: 1
Size: 252382 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 366837 Color: 1
Size: 323872 Color: 0
Size: 309292 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 394721 Color: 1
Size: 352558 Color: 1
Size: 252722 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 475885 Color: 1
Size: 271214 Color: 1
Size: 252902 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 491925 Color: 1
Size: 256660 Color: 1
Size: 251416 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 489180 Color: 1
Size: 259430 Color: 1
Size: 251391 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 404562 Color: 1
Size: 332371 Color: 1
Size: 263068 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 370509 Color: 1
Size: 340042 Color: 1
Size: 289450 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 362237 Color: 1
Size: 335282 Color: 1
Size: 302482 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 484209 Color: 1
Size: 262220 Color: 1
Size: 253572 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 480253 Color: 1
Size: 267171 Color: 1
Size: 252577 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 358880 Color: 1
Size: 357150 Color: 1
Size: 283971 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 480446 Color: 1
Size: 261847 Color: 1
Size: 257708 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 474244 Color: 1
Size: 274689 Color: 1
Size: 251068 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 417636 Color: 1
Size: 293418 Color: 1
Size: 288947 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 433743 Color: 1
Size: 308649 Color: 1
Size: 257609 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 408062 Color: 1
Size: 318388 Color: 1
Size: 273551 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 401744 Color: 1
Size: 322833 Color: 1
Size: 275424 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 465185 Color: 1
Size: 267712 Color: 0
Size: 267104 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 458486 Color: 1
Size: 285802 Color: 1
Size: 255713 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 380482 Color: 1
Size: 342097 Color: 1
Size: 277422 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 449791 Color: 1
Size: 289876 Color: 1
Size: 260334 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 447091 Color: 1
Size: 283190 Color: 1
Size: 269720 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 438258 Color: 1
Size: 295646 Color: 1
Size: 266097 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 397198 Color: 1
Size: 340363 Color: 1
Size: 262440 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 435401 Color: 1
Size: 310531 Color: 1
Size: 254069 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 435907 Color: 1
Size: 291644 Color: 1
Size: 272450 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 384837 Color: 1
Size: 361957 Color: 1
Size: 253207 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 437234 Color: 1
Size: 304513 Color: 1
Size: 258254 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 413420 Color: 1
Size: 306639 Color: 1
Size: 279942 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 391235 Color: 1
Size: 322588 Color: 1
Size: 286178 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 378718 Color: 1
Size: 344743 Color: 1
Size: 276540 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 350773 Color: 1
Size: 341696 Color: 1
Size: 307532 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 492147 Color: 1
Size: 256918 Color: 1
Size: 250936 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 414777 Color: 1
Size: 324649 Color: 1
Size: 260575 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 376532 Color: 1
Size: 350915 Color: 1
Size: 272554 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 353642 Color: 1
Size: 343296 Color: 1
Size: 303063 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 424497 Color: 1
Size: 301725 Color: 1
Size: 273779 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 440335 Color: 1
Size: 297375 Color: 1
Size: 262291 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 404139 Color: 1
Size: 330654 Color: 1
Size: 265208 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 369603 Color: 1
Size: 320913 Color: 0
Size: 309485 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 456567 Color: 1
Size: 286775 Color: 1
Size: 256659 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 444990 Color: 1
Size: 298251 Color: 1
Size: 256760 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 387170 Color: 1
Size: 329489 Color: 1
Size: 283342 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 391957 Color: 1
Size: 357176 Color: 1
Size: 250868 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 451267 Color: 1
Size: 296925 Color: 1
Size: 251809 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 350642 Color: 1
Size: 347687 Color: 1
Size: 301672 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 484062 Color: 1
Size: 262287 Color: 1
Size: 253652 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 448844 Color: 1
Size: 293398 Color: 1
Size: 257759 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 492664 Color: 1
Size: 256155 Color: 1
Size: 251182 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 434918 Color: 1
Size: 291949 Color: 1
Size: 273134 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 373343 Color: 1
Size: 350541 Color: 1
Size: 276117 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 490680 Color: 1
Size: 256923 Color: 1
Size: 252398 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 499859 Color: 1
Size: 250079 Color: 1
Size: 250063 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 388151 Color: 1
Size: 323711 Color: 1
Size: 288139 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 430779 Color: 1
Size: 311700 Color: 1
Size: 257522 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 402329 Color: 1
Size: 327507 Color: 1
Size: 270165 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 496704 Color: 1
Size: 251970 Color: 1
Size: 251327 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 444692 Color: 1
Size: 289349 Color: 1
Size: 265960 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 477401 Color: 1
Size: 269491 Color: 0
Size: 253109 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 416633 Color: 1
Size: 318786 Color: 1
Size: 264582 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 364740 Color: 1
Size: 329987 Color: 1
Size: 305274 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 488172 Color: 1
Size: 256357 Color: 1
Size: 255472 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 390211 Color: 1
Size: 314225 Color: 1
Size: 295565 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 365524 Color: 1
Size: 337598 Color: 1
Size: 296879 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 490297 Color: 1
Size: 258329 Color: 1
Size: 251375 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 453069 Color: 1
Size: 277365 Color: 0
Size: 269567 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 376201 Color: 1
Size: 356617 Color: 1
Size: 267183 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 435809 Color: 1
Size: 301211 Color: 1
Size: 262981 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 452596 Color: 1
Size: 277349 Color: 1
Size: 270056 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 378498 Color: 1
Size: 319590 Color: 1
Size: 301913 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 428910 Color: 1
Size: 291487 Color: 1
Size: 279604 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 365639 Color: 1
Size: 332479 Color: 1
Size: 301883 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 488581 Color: 1
Size: 260532 Color: 1
Size: 250888 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 354034 Color: 1
Size: 326355 Color: 0
Size: 319612 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 483936 Color: 1
Size: 264592 Color: 1
Size: 251473 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 380913 Color: 1
Size: 327021 Color: 1
Size: 292067 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 436610 Color: 1
Size: 303245 Color: 1
Size: 260146 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 481968 Color: 1
Size: 260320 Color: 1
Size: 257713 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 387876 Color: 1
Size: 328267 Color: 1
Size: 283858 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 415565 Color: 1
Size: 314037 Color: 1
Size: 270399 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 496943 Color: 1
Size: 252892 Color: 1
Size: 250166 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 445700 Color: 1
Size: 301803 Color: 1
Size: 252498 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 433076 Color: 1
Size: 307272 Color: 1
Size: 259653 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 406742 Color: 1
Size: 298511 Color: 1
Size: 294748 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 449676 Color: 1
Size: 294044 Color: 1
Size: 256281 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 473850 Color: 1
Size: 272449 Color: 1
Size: 253702 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 478813 Color: 1
Size: 266137 Color: 1
Size: 255051 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 386138 Color: 1
Size: 330546 Color: 1
Size: 283317 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 475154 Color: 1
Size: 268431 Color: 1
Size: 256416 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 498980 Color: 1
Size: 251002 Color: 1
Size: 250019 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 486578 Color: 1
Size: 257557 Color: 0
Size: 255866 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 488959 Color: 1
Size: 257499 Color: 1
Size: 253543 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 375514 Color: 1
Size: 327432 Color: 0
Size: 297055 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 410523 Color: 1
Size: 322290 Color: 1
Size: 267188 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 435676 Color: 1
Size: 299015 Color: 1
Size: 265310 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 482672 Color: 1
Size: 263556 Color: 1
Size: 253773 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 466140 Color: 1
Size: 282777 Color: 1
Size: 251084 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 370199 Color: 1
Size: 357653 Color: 1
Size: 272149 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 481568 Color: 1
Size: 266296 Color: 1
Size: 252137 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 375241 Color: 1
Size: 330357 Color: 1
Size: 294403 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 354372 Color: 1
Size: 342272 Color: 1
Size: 303357 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 462027 Color: 1
Size: 272578 Color: 1
Size: 265396 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 398622 Color: 1
Size: 312106 Color: 1
Size: 289273 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 488619 Color: 1
Size: 256125 Color: 1
Size: 255257 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 397953 Color: 1
Size: 320491 Color: 1
Size: 281557 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 493638 Color: 1
Size: 253388 Color: 0
Size: 252975 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 496714 Color: 1
Size: 253153 Color: 1
Size: 250134 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 487576 Color: 1
Size: 257138 Color: 1
Size: 255287 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 475887 Color: 1
Size: 273983 Color: 1
Size: 250131 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 388613 Color: 1
Size: 343242 Color: 1
Size: 268146 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 415265 Color: 1
Size: 320062 Color: 1
Size: 264674 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 370375 Color: 1
Size: 351704 Color: 1
Size: 277922 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 491537 Color: 1
Size: 255429 Color: 1
Size: 253035 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 420922 Color: 1
Size: 319052 Color: 1
Size: 260027 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 404318 Color: 1
Size: 329504 Color: 1
Size: 266179 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 472981 Color: 1
Size: 273208 Color: 1
Size: 253812 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 385274 Color: 1
Size: 357691 Color: 1
Size: 257036 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 423228 Color: 1
Size: 299618 Color: 1
Size: 277155 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 445123 Color: 1
Size: 302901 Color: 1
Size: 251977 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 471370 Color: 1
Size: 270270 Color: 1
Size: 258361 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 377128 Color: 1
Size: 334896 Color: 1
Size: 287977 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 414741 Color: 1
Size: 295408 Color: 0
Size: 289852 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 457797 Color: 1
Size: 277405 Color: 1
Size: 264799 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 384247 Color: 1
Size: 313199 Color: 1
Size: 302555 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 358746 Color: 1
Size: 330440 Color: 1
Size: 310815 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 441433 Color: 1
Size: 294247 Color: 1
Size: 264321 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 450274 Color: 1
Size: 290165 Color: 1
Size: 259562 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 466942 Color: 1
Size: 279204 Color: 1
Size: 253855 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 470705 Color: 1
Size: 274342 Color: 1
Size: 254954 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 433980 Color: 1
Size: 302324 Color: 1
Size: 263697 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 394885 Color: 1
Size: 320612 Color: 1
Size: 284504 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 415240 Color: 1
Size: 311405 Color: 1
Size: 273356 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 387671 Color: 1
Size: 354064 Color: 1
Size: 258266 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 355637 Color: 1
Size: 336450 Color: 1
Size: 307914 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 426165 Color: 1
Size: 319997 Color: 1
Size: 253839 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 409631 Color: 1
Size: 332760 Color: 1
Size: 257610 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 372958 Color: 1
Size: 332483 Color: 1
Size: 294560 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 486692 Color: 1
Size: 256661 Color: 1
Size: 256648 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 463078 Color: 1
Size: 279837 Color: 1
Size: 257086 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 363636 Color: 1
Size: 330322 Color: 1
Size: 306043 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 409534 Color: 1
Size: 314652 Color: 1
Size: 275815 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 470736 Color: 1
Size: 268992 Color: 1
Size: 260273 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 464378 Color: 1
Size: 274862 Color: 1
Size: 260761 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 421640 Color: 1
Size: 311284 Color: 1
Size: 267077 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 444557 Color: 1
Size: 283387 Color: 1
Size: 272057 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 424035 Color: 1
Size: 301305 Color: 0
Size: 274661 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 380398 Color: 1
Size: 334963 Color: 1
Size: 284640 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 374028 Color: 1
Size: 340049 Color: 1
Size: 285924 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 348985 Color: 1
Size: 344108 Color: 1
Size: 306908 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 399388 Color: 1
Size: 341112 Color: 1
Size: 259501 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 499356 Color: 1
Size: 250559 Color: 1
Size: 250086 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 386178 Color: 1
Size: 334544 Color: 1
Size: 279279 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 348454 Color: 1
Size: 339003 Color: 1
Size: 312544 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 428419 Color: 1
Size: 304252 Color: 1
Size: 267330 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 416858 Color: 1
Size: 313865 Color: 1
Size: 269278 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 442754 Color: 1
Size: 292033 Color: 1
Size: 265214 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 365929 Color: 1
Size: 340342 Color: 1
Size: 293730 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 385329 Color: 1
Size: 360771 Color: 1
Size: 253901 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 412204 Color: 1
Size: 327227 Color: 1
Size: 260570 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 480125 Color: 1
Size: 268957 Color: 1
Size: 250919 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 377219 Color: 1
Size: 336304 Color: 1
Size: 286478 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 440054 Color: 1
Size: 298665 Color: 1
Size: 261282 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 434890 Color: 1
Size: 298958 Color: 1
Size: 266153 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 474676 Color: 1
Size: 264154 Color: 1
Size: 261171 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 361234 Color: 1
Size: 322367 Color: 0
Size: 316400 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 373932 Color: 1
Size: 360790 Color: 1
Size: 265279 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 485633 Color: 1
Size: 257547 Color: 1
Size: 256821 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 424435 Color: 1
Size: 308384 Color: 1
Size: 267182 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 411095 Color: 1
Size: 328243 Color: 1
Size: 260663 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 489692 Color: 1
Size: 257946 Color: 1
Size: 252363 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 485965 Color: 1
Size: 260423 Color: 1
Size: 253613 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 431629 Color: 1
Size: 298024 Color: 1
Size: 270348 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 474047 Color: 1
Size: 268254 Color: 1
Size: 257700 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 437306 Color: 1
Size: 290392 Color: 1
Size: 272303 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 459513 Color: 1
Size: 280364 Color: 1
Size: 260124 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 387014 Color: 1
Size: 333209 Color: 1
Size: 279778 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 443135 Color: 1
Size: 291044 Color: 1
Size: 265822 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 472044 Color: 1
Size: 270152 Color: 1
Size: 257805 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 359312 Color: 1
Size: 327498 Color: 1
Size: 313191 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 394606 Color: 1
Size: 346192 Color: 1
Size: 259203 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 478029 Color: 1
Size: 271361 Color: 1
Size: 250611 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 409818 Color: 1
Size: 336315 Color: 1
Size: 253868 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 440248 Color: 1
Size: 286186 Color: 1
Size: 273567 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 405737 Color: 1
Size: 304008 Color: 1
Size: 290256 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 495598 Color: 1
Size: 252677 Color: 1
Size: 251726 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 433261 Color: 1
Size: 285444 Color: 1
Size: 281296 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 378198 Color: 1
Size: 327967 Color: 1
Size: 293836 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 467306 Color: 1
Size: 276698 Color: 1
Size: 255997 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 384719 Color: 1
Size: 342584 Color: 1
Size: 272698 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 370618 Color: 1
Size: 354055 Color: 1
Size: 275328 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 391284 Color: 1
Size: 306929 Color: 0
Size: 301788 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 368561 Color: 1
Size: 333148 Color: 1
Size: 298292 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 450669 Color: 1
Size: 288516 Color: 1
Size: 260816 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 461068 Color: 1
Size: 279493 Color: 1
Size: 259440 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 405586 Color: 1
Size: 330150 Color: 1
Size: 264265 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 362380 Color: 1
Size: 362063 Color: 1
Size: 275558 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 498391 Color: 1
Size: 251223 Color: 1
Size: 250387 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 451429 Color: 1
Size: 281790 Color: 1
Size: 266782 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 386900 Color: 1
Size: 307730 Color: 0
Size: 305371 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 436305 Color: 1
Size: 300965 Color: 1
Size: 262731 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 404347 Color: 1
Size: 314624 Color: 1
Size: 281030 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 494306 Color: 1
Size: 253270 Color: 1
Size: 252425 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 413483 Color: 1
Size: 307244 Color: 1
Size: 279274 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 459946 Color: 1
Size: 285436 Color: 1
Size: 254619 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 366570 Color: 1
Size: 357222 Color: 1
Size: 276209 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 346653 Color: 1
Size: 345102 Color: 1
Size: 308246 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 419518 Color: 1
Size: 297895 Color: 0
Size: 282588 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 410488 Color: 1
Size: 313787 Color: 1
Size: 275726 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 365281 Color: 1
Size: 347674 Color: 1
Size: 287046 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 483363 Color: 1
Size: 263756 Color: 1
Size: 252882 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 471841 Color: 1
Size: 269991 Color: 1
Size: 258169 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 383317 Color: 1
Size: 351974 Color: 1
Size: 264710 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 386634 Color: 1
Size: 344901 Color: 1
Size: 268466 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 446080 Color: 1
Size: 297304 Color: 1
Size: 256617 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 473510 Color: 1
Size: 268804 Color: 1
Size: 257687 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 368929 Color: 1
Size: 356983 Color: 1
Size: 274089 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 382416 Color: 1
Size: 322009 Color: 1
Size: 295576 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 457364 Color: 1
Size: 291679 Color: 1
Size: 250958 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 402652 Color: 1
Size: 319626 Color: 1
Size: 277723 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 437968 Color: 1
Size: 296480 Color: 0
Size: 265553 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 410545 Color: 1
Size: 312937 Color: 1
Size: 276519 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 374362 Color: 1
Size: 317910 Color: 1
Size: 307729 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 383422 Color: 1
Size: 354867 Color: 1
Size: 261712 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 490720 Color: 1
Size: 258585 Color: 1
Size: 250696 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 484977 Color: 1
Size: 263835 Color: 1
Size: 251189 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 379743 Color: 1
Size: 348963 Color: 1
Size: 271295 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 360148 Color: 1
Size: 336696 Color: 1
Size: 303157 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 430218 Color: 1
Size: 310080 Color: 1
Size: 259703 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 474302 Color: 1
Size: 272826 Color: 1
Size: 252873 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 453762 Color: 1
Size: 275879 Color: 1
Size: 270360 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 410071 Color: 1
Size: 322398 Color: 1
Size: 267532 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 434792 Color: 1
Size: 292816 Color: 1
Size: 272393 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 496343 Color: 1
Size: 252365 Color: 1
Size: 251293 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 439610 Color: 1
Size: 290662 Color: 1
Size: 269729 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 486511 Color: 1
Size: 263235 Color: 1
Size: 250255 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 492093 Color: 1
Size: 257891 Color: 1
Size: 250017 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 364442 Color: 1
Size: 341894 Color: 1
Size: 293665 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 441344 Color: 1
Size: 291471 Color: 0
Size: 267186 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 488204 Color: 1
Size: 258978 Color: 1
Size: 252819 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 461657 Color: 1
Size: 270080 Color: 1
Size: 268264 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 388171 Color: 1
Size: 358707 Color: 1
Size: 253123 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 366305 Color: 1
Size: 349413 Color: 1
Size: 284283 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 385260 Color: 1
Size: 314971 Color: 1
Size: 299770 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 467376 Color: 1
Size: 282401 Color: 1
Size: 250224 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 417690 Color: 1
Size: 295833 Color: 1
Size: 286478 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 382764 Color: 1
Size: 326494 Color: 1
Size: 290743 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 410690 Color: 1
Size: 316935 Color: 1
Size: 272376 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 415813 Color: 1
Size: 330421 Color: 1
Size: 253767 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 477882 Color: 1
Size: 270135 Color: 1
Size: 251984 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 437700 Color: 1
Size: 304393 Color: 1
Size: 257908 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 372334 Color: 1
Size: 346900 Color: 1
Size: 280767 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 430005 Color: 1
Size: 286080 Color: 1
Size: 283916 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 458228 Color: 1
Size: 281426 Color: 1
Size: 260347 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 439579 Color: 1
Size: 307359 Color: 1
Size: 253063 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 386976 Color: 1
Size: 347911 Color: 1
Size: 265114 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 380686 Color: 1
Size: 345693 Color: 1
Size: 273622 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 374129 Color: 1
Size: 324200 Color: 1
Size: 301672 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 380993 Color: 1
Size: 341873 Color: 1
Size: 277135 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 384356 Color: 1
Size: 326737 Color: 1
Size: 288908 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 454909 Color: 1
Size: 285513 Color: 1
Size: 259579 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 395988 Color: 1
Size: 336605 Color: 1
Size: 267408 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 490241 Color: 1
Size: 257009 Color: 1
Size: 252751 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 479145 Color: 1
Size: 261515 Color: 1
Size: 259341 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 378865 Color: 1
Size: 320456 Color: 1
Size: 300680 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 381839 Color: 1
Size: 340159 Color: 1
Size: 278003 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 461161 Color: 1
Size: 281978 Color: 1
Size: 256862 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 475906 Color: 1
Size: 270960 Color: 1
Size: 253135 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 499251 Color: 1
Size: 250534 Color: 1
Size: 250216 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 441550 Color: 1
Size: 297621 Color: 1
Size: 260830 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 440774 Color: 1
Size: 304907 Color: 1
Size: 254320 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 377136 Color: 1
Size: 364184 Color: 1
Size: 258681 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 483389 Color: 1
Size: 266503 Color: 1
Size: 250109 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 457893 Color: 1
Size: 280353 Color: 1
Size: 261755 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 376787 Color: 1
Size: 337565 Color: 1
Size: 285649 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 1
Size: 314690 Color: 0
Size: 301082 Color: 1

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 389254 Color: 1
Size: 338524 Color: 1
Size: 272223 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 399723 Color: 1
Size: 328528 Color: 1
Size: 271750 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 437751 Color: 1
Size: 300799 Color: 1
Size: 261451 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 418863 Color: 1
Size: 293794 Color: 1
Size: 287344 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 376669 Color: 1
Size: 321088 Color: 1
Size: 302244 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 447637 Color: 1
Size: 296283 Color: 1
Size: 256081 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 410702 Color: 1
Size: 299784 Color: 1
Size: 289515 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 372749 Color: 1
Size: 348664 Color: 1
Size: 278588 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 481671 Color: 1
Size: 260003 Color: 1
Size: 258327 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 363920 Color: 1
Size: 327498 Color: 1
Size: 308583 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 398517 Color: 1
Size: 329252 Color: 1
Size: 272232 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 449320 Color: 1
Size: 298871 Color: 1
Size: 251810 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 480243 Color: 1
Size: 264753 Color: 1
Size: 255005 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 483733 Color: 1
Size: 265855 Color: 1
Size: 250413 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 385446 Color: 1
Size: 348353 Color: 1
Size: 266202 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 367592 Color: 1
Size: 351565 Color: 1
Size: 280844 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 433823 Color: 1
Size: 283803 Color: 0
Size: 282375 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 395175 Color: 1
Size: 339411 Color: 1
Size: 265415 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 399835 Color: 1
Size: 318336 Color: 1
Size: 281830 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 373276 Color: 1
Size: 324926 Color: 1
Size: 301799 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 442364 Color: 1
Size: 299652 Color: 1
Size: 257985 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 360878 Color: 1
Size: 359777 Color: 1
Size: 279346 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 489973 Color: 1
Size: 258107 Color: 1
Size: 251921 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 495552 Color: 1
Size: 253709 Color: 1
Size: 250740 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 389849 Color: 1
Size: 323157 Color: 1
Size: 286995 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 372320 Color: 1
Size: 336473 Color: 1
Size: 291208 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 420855 Color: 1
Size: 320375 Color: 1
Size: 258771 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 357936 Color: 1
Size: 342763 Color: 1
Size: 299302 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 487399 Color: 1
Size: 261770 Color: 1
Size: 250832 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 399047 Color: 1
Size: 340266 Color: 1
Size: 260688 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 481068 Color: 1
Size: 263063 Color: 1
Size: 255870 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 468555 Color: 1
Size: 279896 Color: 1
Size: 251550 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 380675 Color: 1
Size: 314073 Color: 1
Size: 305253 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 363590 Color: 1
Size: 345970 Color: 1
Size: 290441 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 393362 Color: 1
Size: 352255 Color: 1
Size: 254384 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 392407 Color: 1
Size: 347009 Color: 1
Size: 260585 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 406724 Color: 1
Size: 315320 Color: 1
Size: 277957 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 413415 Color: 1
Size: 316509 Color: 1
Size: 270077 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 384847 Color: 1
Size: 318055 Color: 0
Size: 297099 Color: 1

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 399583 Color: 1
Size: 323369 Color: 1
Size: 277049 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 437091 Color: 1
Size: 285317 Color: 0
Size: 277593 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 380635 Color: 1
Size: 341750 Color: 1
Size: 277616 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 380490 Color: 1
Size: 332448 Color: 1
Size: 287063 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 414314 Color: 1
Size: 302994 Color: 1
Size: 282693 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 398988 Color: 1
Size: 347579 Color: 1
Size: 253434 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 437992 Color: 1
Size: 297502 Color: 1
Size: 264507 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 393729 Color: 1
Size: 321411 Color: 1
Size: 284861 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 357918 Color: 1
Size: 323249 Color: 1
Size: 318834 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 360547 Color: 1
Size: 325484 Color: 0
Size: 313970 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 373359 Color: 1
Size: 344953 Color: 1
Size: 281689 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 475780 Color: 1
Size: 267303 Color: 1
Size: 256918 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 426478 Color: 1
Size: 308927 Color: 1
Size: 264596 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 427044 Color: 1
Size: 301882 Color: 1
Size: 271075 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 397404 Color: 1
Size: 302931 Color: 1
Size: 299666 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 386912 Color: 1
Size: 313709 Color: 1
Size: 299380 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 388862 Color: 1
Size: 347574 Color: 1
Size: 263565 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 427844 Color: 1
Size: 319738 Color: 1
Size: 252419 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 379497 Color: 1
Size: 313685 Color: 0
Size: 306819 Color: 1

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 384682 Color: 1
Size: 328300 Color: 1
Size: 287019 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 454217 Color: 1
Size: 276007 Color: 1
Size: 269777 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 384488 Color: 1
Size: 319864 Color: 1
Size: 295649 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 456242 Color: 1
Size: 286883 Color: 1
Size: 256876 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 383562 Color: 1
Size: 348579 Color: 1
Size: 267860 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 462324 Color: 1
Size: 284470 Color: 1
Size: 253207 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 367323 Color: 1
Size: 329576 Color: 1
Size: 303102 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 488641 Color: 1
Size: 260045 Color: 1
Size: 251315 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 429974 Color: 1
Size: 306009 Color: 1
Size: 264018 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 427728 Color: 1
Size: 299870 Color: 1
Size: 272403 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 433169 Color: 1
Size: 295972 Color: 1
Size: 270860 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 419550 Color: 1
Size: 306868 Color: 1
Size: 273583 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 400595 Color: 1
Size: 317144 Color: 1
Size: 282262 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 365155 Color: 1
Size: 348551 Color: 1
Size: 286295 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 422517 Color: 1
Size: 293740 Color: 0
Size: 283744 Color: 1

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 485794 Color: 1
Size: 263461 Color: 1
Size: 250746 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 422626 Color: 1
Size: 323396 Color: 1
Size: 253979 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 465111 Color: 1
Size: 281728 Color: 1
Size: 253162 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 381655 Color: 1
Size: 348970 Color: 1
Size: 269376 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 404352 Color: 1
Size: 301703 Color: 1
Size: 293946 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 399275 Color: 1
Size: 322489 Color: 1
Size: 278237 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 483885 Color: 1
Size: 258539 Color: 1
Size: 257577 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 405671 Color: 1
Size: 320661 Color: 1
Size: 273669 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 498014 Color: 1
Size: 251816 Color: 1
Size: 250171 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 480480 Color: 1
Size: 266354 Color: 1
Size: 253167 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 493174 Color: 1
Size: 253864 Color: 1
Size: 252963 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 498792 Color: 1
Size: 250981 Color: 1
Size: 250228 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 429158 Color: 1
Size: 300290 Color: 1
Size: 270553 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 391815 Color: 1
Size: 351073 Color: 1
Size: 257113 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 399532 Color: 1
Size: 308865 Color: 1
Size: 291604 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 373280 Color: 1
Size: 342599 Color: 1
Size: 284122 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 402765 Color: 1
Size: 311326 Color: 0
Size: 285910 Color: 1

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 370793 Color: 1
Size: 318893 Color: 1
Size: 310315 Color: 0

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 441007 Color: 1
Size: 292371 Color: 1
Size: 266623 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 439308 Color: 1
Size: 289532 Color: 0
Size: 271161 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 358899 Color: 1
Size: 354071 Color: 1
Size: 287031 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 451003 Color: 1
Size: 289870 Color: 1
Size: 259128 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 440591 Color: 1
Size: 304222 Color: 1
Size: 255188 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 362641 Color: 1
Size: 356903 Color: 1
Size: 280457 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 489105 Color: 1
Size: 255675 Color: 0
Size: 255221 Color: 1

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 423039 Color: 1
Size: 298568 Color: 0
Size: 278394 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 464024 Color: 1
Size: 283469 Color: 1
Size: 252508 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 369902 Color: 1
Size: 341175 Color: 1
Size: 288924 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 434618 Color: 1
Size: 292520 Color: 1
Size: 272863 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 369423 Color: 1
Size: 337475 Color: 1
Size: 293103 Color: 0

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 387364 Color: 1
Size: 328368 Color: 1
Size: 284269 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 466422 Color: 1
Size: 282702 Color: 1
Size: 250877 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 367327 Color: 1
Size: 344951 Color: 1
Size: 287723 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 371269 Color: 1
Size: 341920 Color: 1
Size: 286812 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 398188 Color: 1
Size: 347039 Color: 1
Size: 254774 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 422930 Color: 1
Size: 292698 Color: 0
Size: 284373 Color: 1

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 433487 Color: 1
Size: 299018 Color: 1
Size: 267496 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 383528 Color: 1
Size: 343845 Color: 1
Size: 272628 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 384624 Color: 1
Size: 333196 Color: 1
Size: 282181 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 450848 Color: 1
Size: 298356 Color: 1
Size: 250797 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 387105 Color: 1
Size: 332784 Color: 1
Size: 280112 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 449288 Color: 1
Size: 287772 Color: 1
Size: 262941 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 492474 Color: 1
Size: 254818 Color: 0
Size: 252709 Color: 1

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 471224 Color: 1
Size: 271537 Color: 1
Size: 257240 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 395313 Color: 1
Size: 305338 Color: 0
Size: 299350 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 376262 Color: 1
Size: 342253 Color: 1
Size: 281486 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 401929 Color: 1
Size: 329845 Color: 1
Size: 268227 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 422503 Color: 1
Size: 323769 Color: 1
Size: 253729 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 492888 Color: 1
Size: 255526 Color: 1
Size: 251587 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 372324 Color: 1
Size: 339402 Color: 1
Size: 288275 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 422512 Color: 1
Size: 310598 Color: 1
Size: 266891 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 478397 Color: 1
Size: 265577 Color: 1
Size: 256027 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 380193 Color: 1
Size: 336039 Color: 1
Size: 283769 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 372281 Color: 1
Size: 348343 Color: 1
Size: 279377 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 380452 Color: 1
Size: 334574 Color: 1
Size: 284975 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 405911 Color: 1
Size: 327688 Color: 1
Size: 266402 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 364798 Color: 1
Size: 337553 Color: 1
Size: 297650 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 403494 Color: 1
Size: 336801 Color: 1
Size: 259706 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 447509 Color: 1
Size: 288759 Color: 0
Size: 263733 Color: 1

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 478464 Color: 1
Size: 269899 Color: 1
Size: 251638 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 438309 Color: 1
Size: 288566 Color: 1
Size: 273126 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 458184 Color: 1
Size: 287093 Color: 1
Size: 254724 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 462583 Color: 1
Size: 273916 Color: 1
Size: 263502 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 366252 Color: 1
Size: 335579 Color: 1
Size: 298170 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 352545 Color: 1
Size: 333457 Color: 1
Size: 313999 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 472221 Color: 1
Size: 274917 Color: 1
Size: 252863 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 468536 Color: 1
Size: 273752 Color: 1
Size: 257713 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 468257 Color: 1
Size: 276409 Color: 1
Size: 255335 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 433162 Color: 1
Size: 294517 Color: 1
Size: 272322 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 395835 Color: 1
Size: 342532 Color: 1
Size: 261634 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 376305 Color: 1
Size: 349485 Color: 1
Size: 274211 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 475500 Color: 1
Size: 271222 Color: 1
Size: 253279 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 388330 Color: 1
Size: 345333 Color: 1
Size: 266338 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 493965 Color: 1
Size: 255044 Color: 1
Size: 250992 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 425935 Color: 1
Size: 308626 Color: 1
Size: 265440 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 415622 Color: 1
Size: 320134 Color: 1
Size: 264245 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 394351 Color: 1
Size: 328595 Color: 1
Size: 277055 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 459416 Color: 1
Size: 275117 Color: 1
Size: 265468 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 400337 Color: 1
Size: 341941 Color: 1
Size: 257723 Color: 0

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 358642 Color: 1
Size: 347175 Color: 1
Size: 294184 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 383208 Color: 1
Size: 332673 Color: 1
Size: 284120 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 432557 Color: 1
Size: 304318 Color: 1
Size: 263126 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 441892 Color: 1
Size: 306014 Color: 1
Size: 252095 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 393139 Color: 1
Size: 320247 Color: 1
Size: 286615 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 402384 Color: 1
Size: 343552 Color: 1
Size: 254065 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 389099 Color: 1
Size: 311394 Color: 1
Size: 299508 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 433062 Color: 1
Size: 309235 Color: 1
Size: 257704 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 382001 Color: 1
Size: 328155 Color: 1
Size: 289845 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 376220 Color: 1
Size: 360225 Color: 1
Size: 263556 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 465067 Color: 1
Size: 276544 Color: 1
Size: 258390 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 438323 Color: 1
Size: 302411 Color: 1
Size: 259267 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 478478 Color: 1
Size: 271468 Color: 1
Size: 250055 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 447562 Color: 1
Size: 293125 Color: 1
Size: 259314 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 384710 Color: 1
Size: 314685 Color: 1
Size: 300606 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 443451 Color: 1
Size: 303183 Color: 1
Size: 253367 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 455526 Color: 1
Size: 293833 Color: 1
Size: 250642 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 474465 Color: 1
Size: 271657 Color: 1
Size: 253879 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 491355 Color: 1
Size: 256335 Color: 1
Size: 252311 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 498801 Color: 1
Size: 250974 Color: 1
Size: 250226 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 461557 Color: 1
Size: 287921 Color: 1
Size: 250523 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 458146 Color: 1
Size: 288447 Color: 1
Size: 253408 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 428452 Color: 1
Size: 315440 Color: 1
Size: 256109 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 438049 Color: 1
Size: 296208 Color: 0
Size: 265744 Color: 1

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 405728 Color: 1
Size: 300195 Color: 1
Size: 294078 Color: 0

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 471415 Color: 1
Size: 276028 Color: 1
Size: 252558 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 396640 Color: 1
Size: 337969 Color: 1
Size: 265392 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 422624 Color: 1
Size: 305593 Color: 1
Size: 271784 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 471473 Color: 1
Size: 267201 Color: 1
Size: 261327 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 397312 Color: 1
Size: 324341 Color: 1
Size: 278348 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 432667 Color: 1
Size: 292241 Color: 0
Size: 275093 Color: 1

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 474694 Color: 1
Size: 272649 Color: 1
Size: 252658 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 436852 Color: 1
Size: 304638 Color: 1
Size: 258511 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 449014 Color: 1
Size: 280928 Color: 1
Size: 270059 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 484701 Color: 1
Size: 259036 Color: 1
Size: 256264 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 372568 Color: 1
Size: 336808 Color: 1
Size: 290625 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 387785 Color: 1
Size: 350133 Color: 1
Size: 262083 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 382435 Color: 1
Size: 329694 Color: 1
Size: 287872 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 422353 Color: 1
Size: 320506 Color: 1
Size: 257142 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 393462 Color: 1
Size: 320400 Color: 1
Size: 286139 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 483848 Color: 1
Size: 265254 Color: 1
Size: 250899 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 393347 Color: 1
Size: 340354 Color: 1
Size: 266300 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 378299 Color: 1
Size: 324685 Color: 0
Size: 297017 Color: 1

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 482921 Color: 1
Size: 265866 Color: 1
Size: 251214 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 443527 Color: 1
Size: 296139 Color: 1
Size: 260335 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 458093 Color: 1
Size: 290343 Color: 1
Size: 251565 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 369998 Color: 1
Size: 341974 Color: 1
Size: 288029 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 460306 Color: 1
Size: 273552 Color: 1
Size: 266143 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 365952 Color: 1
Size: 356283 Color: 1
Size: 277766 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 477403 Color: 1
Size: 269919 Color: 1
Size: 252679 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 487303 Color: 1
Size: 261626 Color: 1
Size: 251072 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 433930 Color: 1
Size: 315939 Color: 1
Size: 250132 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 477742 Color: 1
Size: 263662 Color: 0
Size: 258597 Color: 1

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 380359 Color: 1
Size: 334725 Color: 1
Size: 284917 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 354024 Color: 1
Size: 352803 Color: 1
Size: 293174 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 350862 Color: 1
Size: 326431 Color: 0
Size: 322708 Color: 1

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 494130 Color: 1
Size: 255467 Color: 1
Size: 250404 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 354530 Color: 1
Size: 337092 Color: 1
Size: 308379 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 376593 Color: 1
Size: 356094 Color: 1
Size: 267314 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 390100 Color: 1
Size: 321383 Color: 1
Size: 288518 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 485611 Color: 1
Size: 263820 Color: 1
Size: 250570 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 417488 Color: 1
Size: 308486 Color: 1
Size: 274027 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 492846 Color: 1
Size: 254747 Color: 0
Size: 252408 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 487423 Color: 1
Size: 261881 Color: 1
Size: 250697 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 349851 Color: 1
Size: 330428 Color: 1
Size: 319722 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 486318 Color: 1
Size: 258919 Color: 0
Size: 254764 Color: 1

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 431945 Color: 1
Size: 315483 Color: 1
Size: 252573 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 422518 Color: 1
Size: 320666 Color: 1
Size: 256817 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 411111 Color: 1
Size: 309001 Color: 1
Size: 279889 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 440849 Color: 1
Size: 303317 Color: 1
Size: 255835 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 481518 Color: 1
Size: 259484 Color: 0
Size: 258999 Color: 1

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 477168 Color: 1
Size: 269561 Color: 1
Size: 253272 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 404036 Color: 1
Size: 310491 Color: 1
Size: 285474 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 446177 Color: 1
Size: 292328 Color: 1
Size: 261496 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 369404 Color: 1
Size: 327088 Color: 1
Size: 303509 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 381537 Color: 1
Size: 328618 Color: 1
Size: 289846 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 401134 Color: 1
Size: 331482 Color: 1
Size: 267385 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 396935 Color: 1
Size: 307313 Color: 0
Size: 295753 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 478398 Color: 1
Size: 268876 Color: 1
Size: 252727 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 388345 Color: 1
Size: 334759 Color: 1
Size: 276897 Color: 0

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 475101 Color: 1
Size: 270252 Color: 1
Size: 254648 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 349586 Color: 1
Size: 333293 Color: 1
Size: 317122 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 356271 Color: 1
Size: 330030 Color: 1
Size: 313700 Color: 0

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 490088 Color: 1
Size: 258503 Color: 1
Size: 251410 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 468628 Color: 1
Size: 278173 Color: 1
Size: 253200 Color: 0

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 439947 Color: 1
Size: 309646 Color: 1
Size: 250408 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 403141 Color: 1
Size: 338203 Color: 1
Size: 258657 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 439484 Color: 1
Size: 284004 Color: 1
Size: 276513 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 450192 Color: 1
Size: 293356 Color: 1
Size: 256453 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 499175 Color: 1
Size: 250670 Color: 1
Size: 250156 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 384120 Color: 1
Size: 352674 Color: 1
Size: 263207 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 442281 Color: 1
Size: 294364 Color: 1
Size: 263356 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 381216 Color: 1
Size: 317790 Color: 1
Size: 300995 Color: 0

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 379464 Color: 1
Size: 344323 Color: 1
Size: 276214 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 416225 Color: 1
Size: 314025 Color: 1
Size: 269751 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 431341 Color: 1
Size: 314209 Color: 1
Size: 254451 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 429215 Color: 1
Size: 302413 Color: 1
Size: 268373 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 470221 Color: 1
Size: 279068 Color: 1
Size: 250712 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 420784 Color: 1
Size: 309924 Color: 1
Size: 269293 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 378394 Color: 1
Size: 361983 Color: 1
Size: 259624 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 448731 Color: 1
Size: 300418 Color: 1
Size: 250852 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 378756 Color: 1
Size: 358629 Color: 1
Size: 262616 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 393433 Color: 1
Size: 304175 Color: 1
Size: 302393 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 394040 Color: 1
Size: 322715 Color: 0
Size: 283246 Color: 1

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 387968 Color: 1
Size: 327236 Color: 1
Size: 284797 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 461154 Color: 1
Size: 281878 Color: 1
Size: 256969 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 428634 Color: 1
Size: 300886 Color: 1
Size: 270481 Color: 0

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 472251 Color: 1
Size: 270146 Color: 1
Size: 257604 Color: 0

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 424924 Color: 1
Size: 311984 Color: 1
Size: 263093 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 441976 Color: 1
Size: 285071 Color: 1
Size: 272954 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 390513 Color: 1
Size: 308012 Color: 0
Size: 301476 Color: 1

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 367994 Color: 1
Size: 346092 Color: 1
Size: 285915 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 370169 Color: 1
Size: 340928 Color: 1
Size: 288904 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 410172 Color: 1
Size: 328162 Color: 1
Size: 261667 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 466444 Color: 1
Size: 283080 Color: 1
Size: 250477 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 499700 Color: 1
Size: 250199 Color: 1
Size: 250102 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 494257 Color: 1
Size: 254087 Color: 1
Size: 251657 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 411216 Color: 1
Size: 335114 Color: 1
Size: 253671 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 407219 Color: 1
Size: 303233 Color: 1
Size: 289549 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 384738 Color: 1
Size: 317281 Color: 0
Size: 297982 Color: 1

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 395790 Color: 1
Size: 317537 Color: 1
Size: 286674 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 495174 Color: 1
Size: 253179 Color: 1
Size: 251648 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 468365 Color: 1
Size: 279649 Color: 1
Size: 251987 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 433351 Color: 1
Size: 305216 Color: 1
Size: 261434 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 375040 Color: 1
Size: 344083 Color: 1
Size: 280878 Color: 0

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 489223 Color: 1
Size: 255813 Color: 0
Size: 254965 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 482745 Color: 1
Size: 263172 Color: 1
Size: 254084 Color: 0

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 488104 Color: 1
Size: 256451 Color: 1
Size: 255446 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 395425 Color: 1
Size: 343098 Color: 1
Size: 261478 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 368102 Color: 1
Size: 318714 Color: 0
Size: 313185 Color: 1

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 475267 Color: 1
Size: 271980 Color: 1
Size: 252754 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 431700 Color: 1
Size: 306637 Color: 1
Size: 261664 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 455704 Color: 1
Size: 281660 Color: 1
Size: 262637 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 485066 Color: 1
Size: 262066 Color: 1
Size: 252869 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 453251 Color: 1
Size: 285391 Color: 1
Size: 261359 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 449584 Color: 1
Size: 294287 Color: 1
Size: 256130 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 410042 Color: 1
Size: 333764 Color: 1
Size: 256195 Color: 0

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 411343 Color: 1
Size: 319999 Color: 1
Size: 268659 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 372862 Color: 1
Size: 356577 Color: 1
Size: 270562 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 438981 Color: 1
Size: 282898 Color: 1
Size: 278122 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 423780 Color: 1
Size: 301019 Color: 1
Size: 275202 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 401065 Color: 1
Size: 322404 Color: 1
Size: 276532 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 442739 Color: 1
Size: 287094 Color: 1
Size: 270168 Color: 0

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 439822 Color: 1
Size: 306969 Color: 1
Size: 253210 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 396488 Color: 1
Size: 326299 Color: 1
Size: 277214 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 419935 Color: 1
Size: 317535 Color: 1
Size: 262531 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 441409 Color: 1
Size: 307865 Color: 1
Size: 250727 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 372357 Color: 1
Size: 368190 Color: 1
Size: 259454 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 386754 Color: 1
Size: 338752 Color: 1
Size: 274495 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 382478 Color: 1
Size: 322412 Color: 1
Size: 295111 Color: 0

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 443466 Color: 1
Size: 299355 Color: 1
Size: 257180 Color: 0

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 477853 Color: 1
Size: 266191 Color: 1
Size: 255957 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 415919 Color: 1
Size: 331613 Color: 1
Size: 252469 Color: 0

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 444262 Color: 1
Size: 296145 Color: 1
Size: 259594 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 353044 Color: 1
Size: 348740 Color: 1
Size: 298217 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 429838 Color: 1
Size: 313890 Color: 1
Size: 256273 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 398627 Color: 1
Size: 320751 Color: 1
Size: 280623 Color: 0

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 409609 Color: 1
Size: 312025 Color: 1
Size: 278367 Color: 0

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 434955 Color: 1
Size: 303543 Color: 1
Size: 261503 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 429779 Color: 1
Size: 317684 Color: 0
Size: 252538 Color: 1

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 415629 Color: 1
Size: 313356 Color: 1
Size: 271016 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 398195 Color: 1
Size: 342854 Color: 1
Size: 258952 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 474381 Color: 1
Size: 273380 Color: 1
Size: 252240 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 447507 Color: 1
Size: 288867 Color: 1
Size: 263627 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 464202 Color: 1
Size: 283277 Color: 1
Size: 252522 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 400507 Color: 1
Size: 320297 Color: 1
Size: 279197 Color: 0

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 414360 Color: 1
Size: 294136 Color: 1
Size: 291505 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 342519 Color: 1
Size: 338250 Color: 1
Size: 319232 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 357590 Color: 1
Size: 321363 Color: 0
Size: 321048 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 439342 Color: 1
Size: 292635 Color: 1
Size: 268024 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 368542 Color: 1
Size: 347803 Color: 1
Size: 283656 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 373584 Color: 1
Size: 354019 Color: 1
Size: 272398 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 372011 Color: 1
Size: 343910 Color: 1
Size: 284080 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 380162 Color: 1
Size: 339278 Color: 1
Size: 280561 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 428617 Color: 1
Size: 298502 Color: 1
Size: 272882 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 397428 Color: 1
Size: 331163 Color: 1
Size: 271410 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 498313 Color: 1
Size: 251130 Color: 1
Size: 250558 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 368524 Color: 1
Size: 348455 Color: 1
Size: 283022 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 362217 Color: 1
Size: 322388 Color: 0
Size: 315396 Color: 1

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 440808 Color: 1
Size: 298918 Color: 1
Size: 260275 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 453476 Color: 1
Size: 295270 Color: 1
Size: 251255 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 438946 Color: 1
Size: 302360 Color: 1
Size: 258695 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 432796 Color: 1
Size: 304464 Color: 1
Size: 262741 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 375332 Color: 1
Size: 354182 Color: 1
Size: 270487 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 355418 Color: 1
Size: 328328 Color: 1
Size: 316255 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 364255 Color: 1
Size: 318227 Color: 0
Size: 317519 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 404840 Color: 1
Size: 317268 Color: 1
Size: 277893 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 396483 Color: 1
Size: 336359 Color: 1
Size: 267159 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 387208 Color: 1
Size: 334032 Color: 1
Size: 278761 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 390108 Color: 1
Size: 332477 Color: 1
Size: 277416 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 468899 Color: 1
Size: 280583 Color: 1
Size: 250519 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 499220 Color: 1
Size: 250500 Color: 1
Size: 250281 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 444223 Color: 1
Size: 293062 Color: 1
Size: 262716 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 440302 Color: 1
Size: 299356 Color: 1
Size: 260343 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 425678 Color: 1
Size: 319969 Color: 1
Size: 254354 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 380891 Color: 1
Size: 334179 Color: 1
Size: 284931 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 446706 Color: 1
Size: 299952 Color: 1
Size: 253343 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 475884 Color: 1
Size: 263903 Color: 0
Size: 260214 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 359466 Color: 1
Size: 333507 Color: 1
Size: 307028 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 496468 Color: 1
Size: 253091 Color: 1
Size: 250442 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 411983 Color: 1
Size: 331622 Color: 1
Size: 256396 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 441496 Color: 1
Size: 298405 Color: 1
Size: 260100 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 395685 Color: 1
Size: 347401 Color: 1
Size: 256915 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 470797 Color: 1
Size: 276465 Color: 1
Size: 252739 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 421573 Color: 1
Size: 313905 Color: 1
Size: 264523 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 431469 Color: 1
Size: 311615 Color: 1
Size: 256917 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 437840 Color: 1
Size: 296570 Color: 1
Size: 265591 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 365822 Color: 1
Size: 320204 Color: 0
Size: 313975 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 361448 Color: 1
Size: 360545 Color: 1
Size: 278008 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 456647 Color: 1
Size: 274464 Color: 1
Size: 268890 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 361668 Color: 1
Size: 323302 Color: 1
Size: 315031 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 468983 Color: 1
Size: 265995 Color: 0
Size: 265023 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 430235 Color: 1
Size: 304388 Color: 1
Size: 265378 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 461288 Color: 1
Size: 283197 Color: 1
Size: 255516 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 446471 Color: 1
Size: 291255 Color: 1
Size: 262275 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 442353 Color: 1
Size: 295672 Color: 1
Size: 261976 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 493217 Color: 1
Size: 256581 Color: 1
Size: 250203 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 385010 Color: 1
Size: 358491 Color: 1
Size: 256500 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 427993 Color: 1
Size: 301295 Color: 1
Size: 270713 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 401074 Color: 1
Size: 306406 Color: 0
Size: 292521 Color: 1

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 397383 Color: 1
Size: 338728 Color: 1
Size: 263890 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 438882 Color: 1
Size: 305679 Color: 1
Size: 255440 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 443545 Color: 1
Size: 280225 Color: 1
Size: 276231 Color: 0

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 347522 Color: 1
Size: 338108 Color: 1
Size: 314371 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 497564 Color: 1
Size: 252071 Color: 1
Size: 250366 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 395253 Color: 1
Size: 342496 Color: 1
Size: 262252 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 463991 Color: 1
Size: 283502 Color: 1
Size: 252508 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 365642 Color: 1
Size: 343802 Color: 1
Size: 290557 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 369623 Color: 1
Size: 348510 Color: 1
Size: 281868 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 393513 Color: 1
Size: 306908 Color: 1
Size: 299580 Color: 0

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 367788 Color: 1
Size: 355467 Color: 1
Size: 276746 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 382057 Color: 1
Size: 343706 Color: 1
Size: 274238 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 406904 Color: 1
Size: 302867 Color: 0
Size: 290230 Color: 1

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 387867 Color: 1
Size: 335562 Color: 1
Size: 276572 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 368365 Color: 1
Size: 346217 Color: 1
Size: 285419 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 493448 Color: 1
Size: 254401 Color: 1
Size: 252152 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 376409 Color: 1
Size: 349481 Color: 1
Size: 274111 Color: 0

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 390275 Color: 1
Size: 329838 Color: 1
Size: 279888 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 496115 Color: 1
Size: 252616 Color: 0
Size: 251270 Color: 1

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 429891 Color: 1
Size: 298306 Color: 1
Size: 271804 Color: 0

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 365916 Color: 1
Size: 340893 Color: 1
Size: 293192 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 393004 Color: 1
Size: 329177 Color: 1
Size: 277820 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 490493 Color: 1
Size: 257076 Color: 1
Size: 252432 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 406992 Color: 1
Size: 306108 Color: 0
Size: 286901 Color: 1

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 438774 Color: 1
Size: 306800 Color: 1
Size: 254427 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 360227 Color: 1
Size: 343665 Color: 1
Size: 296109 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 466059 Color: 1
Size: 281366 Color: 1
Size: 252576 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 489652 Color: 1
Size: 257235 Color: 0
Size: 253114 Color: 1

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 355382 Color: 1
Size: 351953 Color: 1
Size: 292666 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 414980 Color: 1
Size: 331364 Color: 1
Size: 253657 Color: 0

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 372956 Color: 1
Size: 350077 Color: 1
Size: 276968 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 350658 Color: 1
Size: 338404 Color: 1
Size: 310939 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 405447 Color: 1
Size: 334799 Color: 1
Size: 259755 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 429262 Color: 1
Size: 310032 Color: 1
Size: 260707 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 457423 Color: 1
Size: 288099 Color: 1
Size: 254479 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 431867 Color: 1
Size: 299030 Color: 1
Size: 269104 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 372116 Color: 1
Size: 328591 Color: 1
Size: 299294 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 459221 Color: 1
Size: 288700 Color: 1
Size: 252080 Color: 0

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 374323 Color: 1
Size: 353926 Color: 1
Size: 271752 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 487894 Color: 1
Size: 257359 Color: 1
Size: 254748 Color: 0

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 414791 Color: 1
Size: 306587 Color: 0
Size: 278623 Color: 1

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 392319 Color: 1
Size: 346864 Color: 1
Size: 260818 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 461563 Color: 1
Size: 270977 Color: 1
Size: 267461 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 372324 Color: 1
Size: 330865 Color: 1
Size: 296812 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 464044 Color: 1
Size: 282847 Color: 1
Size: 253110 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 359021 Color: 1
Size: 349100 Color: 1
Size: 291880 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 427255 Color: 1
Size: 308377 Color: 1
Size: 264369 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 384957 Color: 1
Size: 314242 Color: 1
Size: 300802 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 416214 Color: 1
Size: 332225 Color: 1
Size: 251562 Color: 0

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 350177 Color: 1
Size: 342152 Color: 1
Size: 307672 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 396098 Color: 1
Size: 349208 Color: 1
Size: 254695 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 420466 Color: 1
Size: 300674 Color: 1
Size: 278861 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 453323 Color: 1
Size: 282283 Color: 1
Size: 264395 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 430222 Color: 1
Size: 310904 Color: 1
Size: 258875 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 393043 Color: 1
Size: 339434 Color: 1
Size: 267524 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 495395 Color: 1
Size: 254059 Color: 1
Size: 250547 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 470546 Color: 1
Size: 278636 Color: 1
Size: 250819 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 459009 Color: 1
Size: 271739 Color: 1
Size: 269253 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 390623 Color: 1
Size: 342442 Color: 1
Size: 266936 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 473646 Color: 1
Size: 273896 Color: 1
Size: 252459 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 480139 Color: 1
Size: 269636 Color: 1
Size: 250226 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 461943 Color: 1
Size: 277628 Color: 1
Size: 260430 Color: 0

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 370299 Color: 1
Size: 364619 Color: 1
Size: 265083 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 463431 Color: 1
Size: 278743 Color: 1
Size: 257827 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 371179 Color: 1
Size: 333280 Color: 1
Size: 295542 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 364293 Color: 1
Size: 344903 Color: 1
Size: 290805 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 374642 Color: 1
Size: 358249 Color: 1
Size: 267110 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 467848 Color: 1
Size: 271891 Color: 1
Size: 260262 Color: 0

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 426073 Color: 1
Size: 313257 Color: 1
Size: 260671 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 437043 Color: 1
Size: 289664 Color: 0
Size: 273294 Color: 1

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 453549 Color: 1
Size: 284196 Color: 1
Size: 262256 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 473428 Color: 1
Size: 270944 Color: 0
Size: 255629 Color: 1

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 409095 Color: 1
Size: 296926 Color: 0
Size: 293980 Color: 1

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 409312 Color: 1
Size: 333657 Color: 1
Size: 257032 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 448343 Color: 1
Size: 298849 Color: 1
Size: 252809 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 367692 Color: 1
Size: 323217 Color: 1
Size: 309092 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 373098 Color: 1
Size: 350413 Color: 1
Size: 276490 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 363793 Color: 1
Size: 322495 Color: 0
Size: 313713 Color: 1

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 414906 Color: 1
Size: 304667 Color: 1
Size: 280428 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 375629 Color: 1
Size: 364317 Color: 1
Size: 260055 Color: 0

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 398620 Color: 1
Size: 314099 Color: 0
Size: 287282 Color: 1

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 378465 Color: 1
Size: 312013 Color: 1
Size: 309523 Color: 0

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 373537 Color: 1
Size: 342079 Color: 1
Size: 284385 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 470858 Color: 1
Size: 276245 Color: 1
Size: 252898 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 416732 Color: 1
Size: 313928 Color: 1
Size: 269341 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 408259 Color: 1
Size: 324459 Color: 1
Size: 267283 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 464045 Color: 1
Size: 278606 Color: 1
Size: 257350 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 397225 Color: 1
Size: 318864 Color: 0
Size: 283912 Color: 1

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 350028 Color: 1
Size: 347734 Color: 1
Size: 302239 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 456669 Color: 1
Size: 276968 Color: 1
Size: 266364 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 358637 Color: 1
Size: 353294 Color: 1
Size: 288070 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 399322 Color: 1
Size: 337446 Color: 1
Size: 263233 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 485453 Color: 1
Size: 264159 Color: 1
Size: 250389 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 493345 Color: 1
Size: 253408 Color: 1
Size: 253248 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 467724 Color: 1
Size: 271170 Color: 1
Size: 261107 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 347668 Color: 1
Size: 344267 Color: 1
Size: 308066 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 437940 Color: 1
Size: 305811 Color: 1
Size: 256250 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 492800 Color: 1
Size: 256323 Color: 1
Size: 250878 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 377123 Color: 1
Size: 351397 Color: 1
Size: 271481 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 374194 Color: 1
Size: 359881 Color: 1
Size: 265926 Color: 0

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 365466 Color: 1
Size: 338608 Color: 1
Size: 295927 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 432606 Color: 1
Size: 289741 Color: 1
Size: 277654 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 357830 Color: 1
Size: 357377 Color: 1
Size: 284794 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 372437 Color: 1
Size: 348315 Color: 1
Size: 279249 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 453155 Color: 1
Size: 287547 Color: 1
Size: 259299 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 481179 Color: 1
Size: 261792 Color: 1
Size: 257030 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 402812 Color: 1
Size: 314368 Color: 1
Size: 282821 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 375947 Color: 1
Size: 358826 Color: 1
Size: 265228 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 480119 Color: 1
Size: 266484 Color: 1
Size: 253398 Color: 0

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 477026 Color: 1
Size: 269708 Color: 1
Size: 253267 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 417710 Color: 1
Size: 313870 Color: 1
Size: 268421 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 354160 Color: 1
Size: 343168 Color: 1
Size: 302673 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 412119 Color: 1
Size: 331001 Color: 1
Size: 256881 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 498622 Color: 1
Size: 251220 Color: 1
Size: 250159 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 384991 Color: 1
Size: 327867 Color: 1
Size: 287143 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 389048 Color: 1
Size: 339269 Color: 1
Size: 271684 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 444359 Color: 1
Size: 293818 Color: 1
Size: 261824 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 436783 Color: 1
Size: 302253 Color: 1
Size: 260965 Color: 0

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 408598 Color: 1
Size: 319807 Color: 1
Size: 271596 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 442660 Color: 1
Size: 294448 Color: 1
Size: 262893 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 490643 Color: 1
Size: 255026 Color: 1
Size: 254332 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 390337 Color: 1
Size: 343476 Color: 1
Size: 266188 Color: 0

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 411426 Color: 1
Size: 317758 Color: 1
Size: 270817 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 349806 Color: 1
Size: 346848 Color: 1
Size: 303347 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 414664 Color: 1
Size: 327328 Color: 1
Size: 258009 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 363132 Color: 1
Size: 353674 Color: 1
Size: 283195 Color: 0

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 413235 Color: 1
Size: 316761 Color: 1
Size: 270005 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 440892 Color: 1
Size: 302835 Color: 1
Size: 256274 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 463074 Color: 1
Size: 279200 Color: 1
Size: 257727 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 361139 Color: 1
Size: 358220 Color: 1
Size: 280642 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 370049 Color: 1
Size: 328610 Color: 0
Size: 301342 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 378293 Color: 1
Size: 348194 Color: 1
Size: 273514 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 402092 Color: 1
Size: 335672 Color: 1
Size: 262237 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 353084 Color: 1
Size: 344051 Color: 1
Size: 302866 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 445803 Color: 1
Size: 283225 Color: 1
Size: 270973 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 471521 Color: 1
Size: 271476 Color: 1
Size: 257004 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 397960 Color: 1
Size: 320671 Color: 1
Size: 281370 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 480862 Color: 1
Size: 264082 Color: 1
Size: 255057 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 435881 Color: 1
Size: 307014 Color: 1
Size: 257106 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 351226 Color: 1
Size: 344504 Color: 1
Size: 304271 Color: 0

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 373254 Color: 1
Size: 332286 Color: 1
Size: 294461 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 439695 Color: 1
Size: 280848 Color: 0
Size: 279458 Color: 1

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 399550 Color: 1
Size: 329580 Color: 1
Size: 270871 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 495777 Color: 1
Size: 253634 Color: 1
Size: 250590 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 362649 Color: 1
Size: 341211 Color: 1
Size: 296141 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 366730 Color: 1
Size: 341142 Color: 1
Size: 292129 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 373875 Color: 1
Size: 345523 Color: 1
Size: 280603 Color: 0

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 435238 Color: 1
Size: 282642 Color: 1
Size: 282121 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 381077 Color: 1
Size: 339194 Color: 1
Size: 279730 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 470798 Color: 1
Size: 270105 Color: 1
Size: 259098 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 477180 Color: 1
Size: 272175 Color: 1
Size: 250646 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 486124 Color: 1
Size: 263431 Color: 1
Size: 250446 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 499099 Color: 1
Size: 250551 Color: 1
Size: 250351 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 440613 Color: 1
Size: 295024 Color: 1
Size: 264364 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 431287 Color: 1
Size: 317267 Color: 1
Size: 251447 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 355081 Color: 1
Size: 325977 Color: 1
Size: 318943 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 437515 Color: 1
Size: 299005 Color: 1
Size: 263481 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 426854 Color: 1
Size: 306655 Color: 1
Size: 266492 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 379492 Color: 1
Size: 357976 Color: 1
Size: 262533 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 374754 Color: 1
Size: 346305 Color: 1
Size: 278942 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 495690 Color: 1
Size: 254263 Color: 1
Size: 250048 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 402456 Color: 1
Size: 326694 Color: 1
Size: 270851 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 452441 Color: 1
Size: 285519 Color: 1
Size: 262041 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 414197 Color: 1
Size: 326154 Color: 1
Size: 259650 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 390613 Color: 1
Size: 329778 Color: 1
Size: 279610 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 356456 Color: 1
Size: 349488 Color: 1
Size: 294057 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 428909 Color: 1
Size: 314351 Color: 1
Size: 256741 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 482009 Color: 1
Size: 264399 Color: 1
Size: 253593 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 403471 Color: 1
Size: 310906 Color: 1
Size: 285624 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 377777 Color: 1
Size: 337355 Color: 1
Size: 284869 Color: 0

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 479844 Color: 1
Size: 265986 Color: 1
Size: 254171 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 366316 Color: 1
Size: 361747 Color: 1
Size: 271938 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 347736 Color: 1
Size: 330171 Color: 1
Size: 322094 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 464371 Color: 1
Size: 275965 Color: 1
Size: 259665 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 391823 Color: 1
Size: 347364 Color: 1
Size: 260814 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 431678 Color: 1
Size: 311688 Color: 1
Size: 256635 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 463215 Color: 1
Size: 286148 Color: 1
Size: 250638 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 470005 Color: 1
Size: 276025 Color: 1
Size: 253971 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 414788 Color: 1
Size: 309381 Color: 1
Size: 275832 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 403168 Color: 1
Size: 325670 Color: 1
Size: 271163 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 388922 Color: 1
Size: 351889 Color: 1
Size: 259190 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 442762 Color: 1
Size: 301890 Color: 1
Size: 255349 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 466246 Color: 1
Size: 273720 Color: 1
Size: 260035 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 364109 Color: 1
Size: 344338 Color: 1
Size: 291554 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 444969 Color: 1
Size: 281411 Color: 1
Size: 273621 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 496898 Color: 1
Size: 252214 Color: 1
Size: 250889 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 448811 Color: 1
Size: 278476 Color: 0
Size: 272714 Color: 1

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 430026 Color: 1
Size: 309769 Color: 1
Size: 260206 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 407115 Color: 1
Size: 329258 Color: 1
Size: 263628 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 478670 Color: 1
Size: 263798 Color: 0
Size: 257533 Color: 1

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 408300 Color: 1
Size: 309939 Color: 1
Size: 281762 Color: 0

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 409199 Color: 1
Size: 315970 Color: 1
Size: 274832 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 380151 Color: 1
Size: 330914 Color: 1
Size: 288936 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 435205 Color: 1
Size: 297079 Color: 1
Size: 267717 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 371620 Color: 1
Size: 351510 Color: 1
Size: 276871 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 1
Size: 348187 Color: 1
Size: 279004 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 410973 Color: 1
Size: 322088 Color: 1
Size: 266940 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 464614 Color: 1
Size: 284804 Color: 1
Size: 250583 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 472928 Color: 1
Size: 266306 Color: 1
Size: 260767 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 388701 Color: 1
Size: 355365 Color: 1
Size: 255935 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 403723 Color: 1
Size: 300386 Color: 0
Size: 295892 Color: 1

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 495096 Color: 1
Size: 253931 Color: 1
Size: 250974 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 429359 Color: 1
Size: 285423 Color: 1
Size: 285219 Color: 0

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 370471 Color: 1
Size: 337243 Color: 1
Size: 292287 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 406027 Color: 1
Size: 329938 Color: 1
Size: 264036 Color: 0

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 386880 Color: 1
Size: 338934 Color: 1
Size: 274187 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 451874 Color: 1
Size: 296317 Color: 1
Size: 251810 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 448486 Color: 1
Size: 293925 Color: 1
Size: 257590 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 478755 Color: 1
Size: 270740 Color: 1
Size: 250506 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 369903 Color: 1
Size: 321665 Color: 0
Size: 308433 Color: 1

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 367101 Color: 1
Size: 357100 Color: 1
Size: 275800 Color: 0

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 368990 Color: 1
Size: 360814 Color: 1
Size: 270197 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 437255 Color: 1
Size: 299147 Color: 1
Size: 263599 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 456366 Color: 1
Size: 280950 Color: 1
Size: 262685 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 451046 Color: 1
Size: 276923 Color: 1
Size: 272032 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 466649 Color: 1
Size: 273637 Color: 1
Size: 259715 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 380600 Color: 1
Size: 360365 Color: 1
Size: 259036 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 395976 Color: 1
Size: 335558 Color: 1
Size: 268467 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 440638 Color: 1
Size: 298607 Color: 1
Size: 260756 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 423791 Color: 1
Size: 297384 Color: 1
Size: 278826 Color: 0

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 403386 Color: 1
Size: 315271 Color: 1
Size: 281344 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 378091 Color: 1
Size: 314967 Color: 1
Size: 306943 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 391665 Color: 1
Size: 310508 Color: 1
Size: 297828 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 492356 Color: 1
Size: 255093 Color: 1
Size: 252552 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 354370 Color: 1
Size: 325421 Color: 1
Size: 320210 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 458010 Color: 1
Size: 283562 Color: 1
Size: 258429 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 456119 Color: 1
Size: 288732 Color: 1
Size: 255150 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 430417 Color: 1
Size: 308427 Color: 1
Size: 261157 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 479789 Color: 1
Size: 266127 Color: 1
Size: 254085 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 381600 Color: 1
Size: 357813 Color: 1
Size: 260588 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 463319 Color: 1
Size: 277748 Color: 1
Size: 258934 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 481308 Color: 1
Size: 264131 Color: 1
Size: 254562 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 382985 Color: 1
Size: 337606 Color: 1
Size: 279410 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 425605 Color: 1
Size: 314004 Color: 1
Size: 260392 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 367809 Color: 1
Size: 330383 Color: 1
Size: 301809 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 411778 Color: 1
Size: 318158 Color: 1
Size: 270065 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 362703 Color: 1
Size: 329566 Color: 1
Size: 307732 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 476367 Color: 1
Size: 267702 Color: 1
Size: 255932 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 466944 Color: 1
Size: 276079 Color: 1
Size: 256978 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 402934 Color: 1
Size: 318270 Color: 1
Size: 278797 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 401388 Color: 1
Size: 335682 Color: 1
Size: 262931 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 455125 Color: 1
Size: 282557 Color: 1
Size: 262319 Color: 0

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 419147 Color: 1
Size: 304161 Color: 1
Size: 276693 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 496780 Color: 1
Size: 251689 Color: 0
Size: 251532 Color: 1

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 458634 Color: 1
Size: 286477 Color: 1
Size: 254890 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 409422 Color: 1
Size: 318918 Color: 1
Size: 271661 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 495299 Color: 1
Size: 254509 Color: 1
Size: 250193 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 375137 Color: 1
Size: 339724 Color: 1
Size: 285140 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 430711 Color: 1
Size: 288607 Color: 1
Size: 280683 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 490080 Color: 1
Size: 255603 Color: 1
Size: 254318 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 358768 Color: 1
Size: 357890 Color: 1
Size: 283343 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 420805 Color: 1
Size: 309310 Color: 1
Size: 269886 Color: 0

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 457449 Color: 1
Size: 283484 Color: 1
Size: 259068 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 382189 Color: 1
Size: 345487 Color: 1
Size: 272325 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 373995 Color: 1
Size: 342602 Color: 1
Size: 283404 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 433453 Color: 1
Size: 308449 Color: 1
Size: 258099 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 387076 Color: 1
Size: 332361 Color: 1
Size: 280564 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 463002 Color: 1
Size: 285906 Color: 1
Size: 251093 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 454173 Color: 1
Size: 292562 Color: 1
Size: 253266 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 404369 Color: 1
Size: 342472 Color: 1
Size: 253160 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 442297 Color: 1
Size: 295070 Color: 1
Size: 262634 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 410237 Color: 1
Size: 332593 Color: 1
Size: 257171 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 398043 Color: 1
Size: 323508 Color: 1
Size: 278450 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 354181 Color: 1
Size: 346652 Color: 1
Size: 299168 Color: 0

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 450180 Color: 1
Size: 283354 Color: 1
Size: 266467 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 444520 Color: 1
Size: 288253 Color: 0
Size: 267228 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 470570 Color: 1
Size: 274595 Color: 1
Size: 254836 Color: 0

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 363581 Color: 1
Size: 362038 Color: 1
Size: 274382 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 361085 Color: 1
Size: 326869 Color: 1
Size: 312047 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 379232 Color: 1
Size: 310604 Color: 1
Size: 310165 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 363145 Color: 1
Size: 348768 Color: 1
Size: 288088 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 448491 Color: 1
Size: 294032 Color: 1
Size: 257478 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 498693 Color: 1
Size: 251272 Color: 1
Size: 250036 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 440416 Color: 1
Size: 283051 Color: 1
Size: 276534 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 428269 Color: 1
Size: 311506 Color: 1
Size: 260226 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 353275 Color: 1
Size: 329472 Color: 0
Size: 317254 Color: 1

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 395374 Color: 1
Size: 336871 Color: 1
Size: 267756 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 365072 Color: 1
Size: 334560 Color: 1
Size: 300369 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 369914 Color: 1
Size: 364624 Color: 1
Size: 265463 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 369373 Color: 1
Size: 339633 Color: 1
Size: 290995 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 416282 Color: 1
Size: 326021 Color: 1
Size: 257698 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 436988 Color: 1
Size: 294563 Color: 1
Size: 268450 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 356773 Color: 1
Size: 333345 Color: 1
Size: 309883 Color: 0

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 362095 Color: 1
Size: 321222 Color: 1
Size: 316684 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 418131 Color: 1
Size: 315426 Color: 1
Size: 266444 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 368180 Color: 1
Size: 350176 Color: 1
Size: 281645 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 396368 Color: 1
Size: 343988 Color: 1
Size: 259645 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 348244 Color: 1
Size: 345059 Color: 1
Size: 306698 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 459160 Color: 1
Size: 281814 Color: 1
Size: 259027 Color: 0

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 497941 Color: 1
Size: 251745 Color: 1
Size: 250315 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 487221 Color: 1
Size: 258857 Color: 1
Size: 253923 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 378166 Color: 1
Size: 334238 Color: 1
Size: 287597 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 435586 Color: 1
Size: 284746 Color: 1
Size: 279669 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 405315 Color: 1
Size: 331588 Color: 1
Size: 263098 Color: 0

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 359650 Color: 1
Size: 349481 Color: 1
Size: 290870 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 479749 Color: 1
Size: 266624 Color: 1
Size: 253628 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 393898 Color: 1
Size: 332409 Color: 1
Size: 273694 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 345468 Color: 1
Size: 340881 Color: 1
Size: 313652 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 394515 Color: 1
Size: 335317 Color: 1
Size: 270169 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 432942 Color: 1
Size: 305678 Color: 1
Size: 261381 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 378435 Color: 1
Size: 340136 Color: 1
Size: 281430 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 354350 Color: 1
Size: 327488 Color: 0
Size: 318163 Color: 1

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 456621 Color: 1
Size: 275641 Color: 1
Size: 267739 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 409541 Color: 1
Size: 311689 Color: 1
Size: 278771 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 417041 Color: 1
Size: 317525 Color: 1
Size: 265435 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 380587 Color: 1
Size: 340714 Color: 1
Size: 278700 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 458879 Color: 1
Size: 290227 Color: 1
Size: 250895 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 420697 Color: 1
Size: 327524 Color: 1
Size: 251780 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 372612 Color: 1
Size: 362673 Color: 1
Size: 264716 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 460503 Color: 1
Size: 276854 Color: 1
Size: 262644 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 370844 Color: 1
Size: 343988 Color: 1
Size: 285169 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 382363 Color: 1
Size: 328960 Color: 1
Size: 288678 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 400178 Color: 1
Size: 300808 Color: 0
Size: 299015 Color: 1

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 376891 Color: 1
Size: 357390 Color: 1
Size: 265720 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 459316 Color: 1
Size: 287743 Color: 1
Size: 252942 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 379074 Color: 1
Size: 363652 Color: 1
Size: 257275 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 359897 Color: 1
Size: 354946 Color: 1
Size: 285158 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 384045 Color: 1
Size: 357175 Color: 1
Size: 258781 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 412317 Color: 1
Size: 304856 Color: 0
Size: 282828 Color: 1

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 405434 Color: 1
Size: 312379 Color: 0
Size: 282188 Color: 1

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 373651 Color: 1
Size: 334462 Color: 1
Size: 291888 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 435826 Color: 1
Size: 311935 Color: 1
Size: 252240 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 494561 Color: 1
Size: 253089 Color: 1
Size: 252351 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 452367 Color: 1
Size: 282563 Color: 1
Size: 265071 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 388716 Color: 1
Size: 333128 Color: 1
Size: 278157 Color: 0

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 353972 Color: 1
Size: 335502 Color: 1
Size: 310527 Color: 0

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 432291 Color: 1
Size: 317346 Color: 1
Size: 250364 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 432926 Color: 1
Size: 302168 Color: 1
Size: 264907 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 360798 Color: 1
Size: 356362 Color: 1
Size: 282841 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 384042 Color: 1
Size: 334130 Color: 1
Size: 281829 Color: 0

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 469594 Color: 1
Size: 277362 Color: 1
Size: 253045 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 351573 Color: 1
Size: 339833 Color: 1
Size: 308595 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 428392 Color: 1
Size: 319711 Color: 1
Size: 251898 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 467882 Color: 1
Size: 278139 Color: 1
Size: 253980 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 440977 Color: 1
Size: 288782 Color: 1
Size: 270242 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 478737 Color: 1
Size: 266718 Color: 1
Size: 254546 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 354875 Color: 1
Size: 325622 Color: 0
Size: 319504 Color: 1

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 424250 Color: 1
Size: 324822 Color: 1
Size: 250929 Color: 0

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 390369 Color: 1
Size: 349892 Color: 1
Size: 259740 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 445202 Color: 1
Size: 277509 Color: 0
Size: 277290 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 469565 Color: 1
Size: 269407 Color: 1
Size: 261029 Color: 0

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 447325 Color: 1
Size: 294236 Color: 1
Size: 258440 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 374285 Color: 1
Size: 356604 Color: 1
Size: 269112 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 428946 Color: 1
Size: 298033 Color: 1
Size: 273022 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 495855 Color: 1
Size: 253986 Color: 1
Size: 250160 Color: 0

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 424104 Color: 1
Size: 301436 Color: 1
Size: 274461 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 443569 Color: 1
Size: 292075 Color: 1
Size: 264357 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 495980 Color: 1
Size: 253608 Color: 1
Size: 250413 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 400909 Color: 1
Size: 311200 Color: 0
Size: 287892 Color: 1

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 375392 Color: 1
Size: 313782 Color: 1
Size: 310827 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 426702 Color: 1
Size: 318871 Color: 1
Size: 254428 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 468087 Color: 1
Size: 278732 Color: 1
Size: 253182 Color: 0

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 440116 Color: 1
Size: 291120 Color: 1
Size: 268765 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 454818 Color: 1
Size: 288651 Color: 1
Size: 256532 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 357811 Color: 1
Size: 331207 Color: 1
Size: 310983 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 455616 Color: 1
Size: 286321 Color: 1
Size: 258064 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 396087 Color: 1
Size: 341632 Color: 1
Size: 262282 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 396286 Color: 1
Size: 322406 Color: 1
Size: 281309 Color: 0

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 358364 Color: 1
Size: 334593 Color: 1
Size: 307044 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 414549 Color: 1
Size: 304751 Color: 1
Size: 280701 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 430659 Color: 1
Size: 304431 Color: 1
Size: 264911 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 435838 Color: 1
Size: 298497 Color: 1
Size: 265666 Color: 0

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 471819 Color: 1
Size: 270585 Color: 1
Size: 257597 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 469594 Color: 1
Size: 279559 Color: 1
Size: 250848 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 492627 Color: 1
Size: 255242 Color: 1
Size: 252132 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 380505 Color: 1
Size: 319014 Color: 1
Size: 300482 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 403761 Color: 1
Size: 321928 Color: 1
Size: 274312 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 371888 Color: 1
Size: 344096 Color: 1
Size: 284017 Color: 0

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 420056 Color: 1
Size: 314958 Color: 1
Size: 264987 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 384131 Color: 1
Size: 339958 Color: 1
Size: 275912 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 454674 Color: 1
Size: 276229 Color: 1
Size: 269098 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 363676 Color: 1
Size: 320422 Color: 1
Size: 315903 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 434570 Color: 1
Size: 311787 Color: 1
Size: 253644 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 396961 Color: 1
Size: 305975 Color: 1
Size: 297065 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 348319 Color: 1
Size: 345635 Color: 1
Size: 306047 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 408743 Color: 1
Size: 323074 Color: 1
Size: 268184 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 448367 Color: 1
Size: 276125 Color: 1
Size: 275509 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 434463 Color: 1
Size: 306892 Color: 1
Size: 258646 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 410023 Color: 1
Size: 312910 Color: 1
Size: 277068 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 424615 Color: 1
Size: 312705 Color: 1
Size: 262681 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 353961 Color: 1
Size: 323786 Color: 1
Size: 322254 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 441655 Color: 1
Size: 292328 Color: 1
Size: 266018 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 363828 Color: 1
Size: 336301 Color: 1
Size: 299872 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 471080 Color: 1
Size: 271292 Color: 1
Size: 257629 Color: 0

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 432094 Color: 1
Size: 298864 Color: 1
Size: 269043 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 363561 Color: 1
Size: 350326 Color: 1
Size: 286114 Color: 0

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 487210 Color: 1
Size: 261517 Color: 1
Size: 251274 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 425339 Color: 1
Size: 322320 Color: 1
Size: 252342 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 359182 Color: 1
Size: 327223 Color: 1
Size: 313596 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 407597 Color: 1
Size: 300355 Color: 1
Size: 292049 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 375292 Color: 1
Size: 340449 Color: 1
Size: 284260 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 404983 Color: 1
Size: 328693 Color: 1
Size: 266325 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 369607 Color: 1
Size: 334104 Color: 1
Size: 296290 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 347079 Color: 1
Size: 346449 Color: 1
Size: 306473 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 437210 Color: 1
Size: 305557 Color: 1
Size: 257234 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 447895 Color: 1
Size: 289184 Color: 1
Size: 262922 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 415285 Color: 1
Size: 314640 Color: 1
Size: 270076 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 421297 Color: 1
Size: 325696 Color: 1
Size: 253008 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 388476 Color: 1
Size: 322757 Color: 1
Size: 288768 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 400847 Color: 1
Size: 318574 Color: 1
Size: 280580 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 381986 Color: 1
Size: 337588 Color: 1
Size: 280427 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 493049 Color: 1
Size: 255830 Color: 1
Size: 251122 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 465584 Color: 1
Size: 277196 Color: 1
Size: 257221 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 351724 Color: 1
Size: 343142 Color: 1
Size: 305135 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 424681 Color: 1
Size: 313453 Color: 0
Size: 261867 Color: 1

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 380061 Color: 1
Size: 344542 Color: 1
Size: 275398 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 448399 Color: 1
Size: 295975 Color: 1
Size: 255627 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 426797 Color: 1
Size: 319911 Color: 1
Size: 253293 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 461143 Color: 1
Size: 283738 Color: 1
Size: 255120 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 360942 Color: 1
Size: 353263 Color: 1
Size: 285796 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 357941 Color: 1
Size: 350775 Color: 1
Size: 291285 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 357158 Color: 1
Size: 351530 Color: 1
Size: 291313 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 471722 Color: 1
Size: 267089 Color: 1
Size: 261190 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 475409 Color: 1
Size: 271554 Color: 1
Size: 253038 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 488305 Color: 1
Size: 260563 Color: 1
Size: 251133 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 423920 Color: 1
Size: 290869 Color: 1
Size: 285212 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 461924 Color: 1
Size: 283546 Color: 1
Size: 254531 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 418100 Color: 1
Size: 322845 Color: 1
Size: 259056 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 427914 Color: 1
Size: 315061 Color: 1
Size: 257026 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 491062 Color: 1
Size: 258183 Color: 1
Size: 250756 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 451623 Color: 1
Size: 288398 Color: 1
Size: 259980 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 426307 Color: 1
Size: 302598 Color: 1
Size: 271096 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 384590 Color: 1
Size: 335522 Color: 1
Size: 279889 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 367063 Color: 1
Size: 328362 Color: 1
Size: 304576 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 400433 Color: 1
Size: 337389 Color: 1
Size: 262179 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 489115 Color: 1
Size: 257983 Color: 1
Size: 252903 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 421839 Color: 1
Size: 299872 Color: 1
Size: 278290 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 387802 Color: 1
Size: 349308 Color: 1
Size: 262891 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 429452 Color: 1
Size: 297991 Color: 1
Size: 272558 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 455671 Color: 1
Size: 288921 Color: 1
Size: 255409 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 369216 Color: 1
Size: 320516 Color: 1
Size: 310269 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 416373 Color: 1
Size: 295601 Color: 1
Size: 288027 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 359080 Color: 1
Size: 351333 Color: 1
Size: 289588 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 413295 Color: 1
Size: 330276 Color: 1
Size: 256430 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 470806 Color: 1
Size: 273396 Color: 1
Size: 255799 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 358128 Color: 1
Size: 343232 Color: 1
Size: 298641 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 453672 Color: 1
Size: 293414 Color: 1
Size: 252915 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 427363 Color: 1
Size: 310494 Color: 1
Size: 262144 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 411846 Color: 1
Size: 330998 Color: 1
Size: 257157 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 408824 Color: 1
Size: 318025 Color: 1
Size: 273152 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 425121 Color: 1
Size: 300278 Color: 1
Size: 274602 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 368779 Color: 1
Size: 340871 Color: 1
Size: 290351 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 482964 Color: 1
Size: 264479 Color: 1
Size: 252558 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 368680 Color: 1
Size: 332185 Color: 1
Size: 299136 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 404835 Color: 1
Size: 322987 Color: 1
Size: 272179 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 405386 Color: 1
Size: 315953 Color: 1
Size: 278662 Color: 0

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 431221 Color: 1
Size: 313652 Color: 1
Size: 255128 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 419295 Color: 1
Size: 296710 Color: 1
Size: 283996 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 399380 Color: 1
Size: 321356 Color: 1
Size: 279265 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 428288 Color: 1
Size: 318587 Color: 1
Size: 253126 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 496464 Color: 1
Size: 253234 Color: 1
Size: 250303 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 392316 Color: 1
Size: 343383 Color: 1
Size: 264302 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 446406 Color: 1
Size: 300013 Color: 1
Size: 253582 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 436442 Color: 1
Size: 299815 Color: 1
Size: 263744 Color: 0

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 371738 Color: 1
Size: 367482 Color: 1
Size: 260781 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 430345 Color: 1
Size: 298753 Color: 1
Size: 270903 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 469137 Color: 1
Size: 268716 Color: 1
Size: 262148 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 465995 Color: 1
Size: 281444 Color: 1
Size: 252562 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 497323 Color: 1
Size: 251915 Color: 1
Size: 250763 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 489292 Color: 1
Size: 259072 Color: 1
Size: 251637 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 419060 Color: 1
Size: 319367 Color: 1
Size: 261574 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 361517 Color: 1
Size: 330973 Color: 1
Size: 307511 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 391017 Color: 1
Size: 357099 Color: 1
Size: 251885 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 492799 Color: 1
Size: 256530 Color: 1
Size: 250672 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 360017 Color: 1
Size: 328059 Color: 1
Size: 311925 Color: 0

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 497784 Color: 1
Size: 251886 Color: 1
Size: 250331 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 387956 Color: 1
Size: 349969 Color: 1
Size: 262076 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 410682 Color: 1
Size: 330633 Color: 1
Size: 258686 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 476263 Color: 1
Size: 271224 Color: 1
Size: 252514 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 361658 Color: 1
Size: 320386 Color: 0
Size: 317957 Color: 1

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 360474 Color: 1
Size: 326022 Color: 1
Size: 313505 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 365096 Color: 1
Size: 346817 Color: 1
Size: 288088 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 463875 Color: 1
Size: 279490 Color: 1
Size: 256636 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 449784 Color: 1
Size: 296591 Color: 1
Size: 253626 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 485634 Color: 1
Size: 260178 Color: 1
Size: 254189 Color: 0

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 426863 Color: 1
Size: 310477 Color: 1
Size: 262661 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 402588 Color: 1
Size: 322025 Color: 1
Size: 275388 Color: 0

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 378946 Color: 1
Size: 358184 Color: 1
Size: 262871 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 488534 Color: 1
Size: 259960 Color: 1
Size: 251507 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 405709 Color: 1
Size: 340525 Color: 1
Size: 253767 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 484000 Color: 1
Size: 263978 Color: 1
Size: 252023 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 451568 Color: 1
Size: 275504 Color: 1
Size: 272929 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 390293 Color: 1
Size: 348263 Color: 1
Size: 261445 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 453665 Color: 1
Size: 289339 Color: 1
Size: 256997 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 378714 Color: 1
Size: 315037 Color: 1
Size: 306250 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 368888 Color: 1
Size: 335030 Color: 1
Size: 296083 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 360379 Color: 1
Size: 343454 Color: 1
Size: 296168 Color: 0

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 428290 Color: 1
Size: 309205 Color: 1
Size: 262506 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 419231 Color: 1
Size: 312852 Color: 1
Size: 267918 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 389037 Color: 1
Size: 322156 Color: 1
Size: 288808 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 491396 Color: 1
Size: 258143 Color: 1
Size: 250462 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 455220 Color: 1
Size: 283576 Color: 1
Size: 261205 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 479930 Color: 1
Size: 266376 Color: 1
Size: 253695 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 473838 Color: 1
Size: 272681 Color: 1
Size: 253482 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 428618 Color: 1
Size: 310173 Color: 1
Size: 261210 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 373946 Color: 1
Size: 348837 Color: 1
Size: 277218 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 455887 Color: 1
Size: 293883 Color: 1
Size: 250231 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 415875 Color: 1
Size: 315502 Color: 1
Size: 268624 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 439336 Color: 1
Size: 298845 Color: 1
Size: 261820 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 445511 Color: 1
Size: 285002 Color: 1
Size: 269488 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 405512 Color: 1
Size: 331254 Color: 1
Size: 263235 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 438736 Color: 1
Size: 295088 Color: 1
Size: 266177 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 482617 Color: 1
Size: 263365 Color: 1
Size: 254019 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 457176 Color: 1
Size: 292415 Color: 1
Size: 250410 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 483926 Color: 1
Size: 258827 Color: 0
Size: 257248 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 396121 Color: 1
Size: 332443 Color: 1
Size: 271437 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 422506 Color: 1
Size: 300825 Color: 1
Size: 276670 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 345389 Color: 1
Size: 344922 Color: 1
Size: 309690 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 414254 Color: 1
Size: 313035 Color: 1
Size: 272712 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 374597 Color: 1
Size: 336876 Color: 1
Size: 288528 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 359386 Color: 1
Size: 324944 Color: 0
Size: 315671 Color: 1

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 351561 Color: 1
Size: 324239 Color: 1
Size: 324201 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 385045 Color: 1
Size: 344342 Color: 1
Size: 270614 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 380752 Color: 1
Size: 360161 Color: 1
Size: 259088 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 428562 Color: 1
Size: 306819 Color: 1
Size: 264620 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 397179 Color: 1
Size: 330794 Color: 1
Size: 272028 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 486477 Color: 1
Size: 260432 Color: 1
Size: 253092 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 395301 Color: 1
Size: 339125 Color: 1
Size: 265575 Color: 0

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 371787 Color: 1
Size: 353583 Color: 1
Size: 274631 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 436848 Color: 1
Size: 309154 Color: 1
Size: 253999 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 448990 Color: 1
Size: 292525 Color: 1
Size: 258486 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 409581 Color: 1
Size: 319317 Color: 1
Size: 271103 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 441619 Color: 1
Size: 303847 Color: 1
Size: 254535 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 461229 Color: 1
Size: 280587 Color: 1
Size: 258185 Color: 0

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 462798 Color: 1
Size: 286521 Color: 1
Size: 250682 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 492264 Color: 1
Size: 255914 Color: 1
Size: 251823 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 481469 Color: 1
Size: 264356 Color: 1
Size: 254176 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 417685 Color: 1
Size: 312337 Color: 1
Size: 269979 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 475388 Color: 1
Size: 267072 Color: 1
Size: 257541 Color: 0

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 455333 Color: 1
Size: 274034 Color: 1
Size: 270634 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 381565 Color: 1
Size: 340664 Color: 1
Size: 277772 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 428896 Color: 1
Size: 303129 Color: 1
Size: 267976 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 385315 Color: 1
Size: 339363 Color: 1
Size: 275323 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 414412 Color: 1
Size: 318861 Color: 1
Size: 266728 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 407089 Color: 1
Size: 307486 Color: 1
Size: 285426 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 346722 Color: 1
Size: 337324 Color: 1
Size: 315955 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 378131 Color: 1
Size: 353209 Color: 1
Size: 268661 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 348519 Color: 1
Size: 345032 Color: 1
Size: 306450 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 417985 Color: 1
Size: 314237 Color: 1
Size: 267779 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 371955 Color: 1
Size: 353959 Color: 1
Size: 274087 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 430733 Color: 1
Size: 303203 Color: 1
Size: 266065 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 448198 Color: 1
Size: 297818 Color: 1
Size: 253985 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 471647 Color: 1
Size: 273596 Color: 1
Size: 254758 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 473071 Color: 1
Size: 272284 Color: 1
Size: 254646 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 480196 Color: 1
Size: 262944 Color: 1
Size: 256861 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 489505 Color: 1
Size: 259359 Color: 1
Size: 251137 Color: 0

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 416669 Color: 1
Size: 321163 Color: 1
Size: 262169 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 345820 Color: 1
Size: 343573 Color: 1
Size: 310608 Color: 0

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 392095 Color: 1
Size: 321755 Color: 1
Size: 286151 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 470184 Color: 1
Size: 271785 Color: 1
Size: 258032 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 393709 Color: 1
Size: 344004 Color: 1
Size: 262288 Color: 0

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 399013 Color: 1
Size: 329979 Color: 1
Size: 271009 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 382940 Color: 1
Size: 313836 Color: 1
Size: 303225 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 405364 Color: 1
Size: 342452 Color: 1
Size: 252185 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 431965 Color: 1
Size: 317647 Color: 1
Size: 250389 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 384807 Color: 1
Size: 340348 Color: 1
Size: 274846 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 403176 Color: 1
Size: 300766 Color: 1
Size: 296059 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 462270 Color: 1
Size: 275634 Color: 1
Size: 262097 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 406268 Color: 1
Size: 334182 Color: 1
Size: 259551 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 435186 Color: 1
Size: 314495 Color: 1
Size: 250320 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 467958 Color: 1
Size: 269466 Color: 1
Size: 262577 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 418372 Color: 1
Size: 331110 Color: 1
Size: 250519 Color: 0

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 400620 Color: 1
Size: 324151 Color: 1
Size: 275230 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 459189 Color: 1
Size: 284228 Color: 1
Size: 256584 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 427470 Color: 1
Size: 298787 Color: 1
Size: 273744 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 348413 Color: 1
Size: 346598 Color: 1
Size: 304990 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 370922 Color: 1
Size: 362377 Color: 1
Size: 266702 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 456259 Color: 1
Size: 285602 Color: 1
Size: 258140 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 347206 Color: 1
Size: 329692 Color: 0
Size: 323103 Color: 1

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 431820 Color: 1
Size: 298796 Color: 0
Size: 269385 Color: 1

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 448800 Color: 1
Size: 286370 Color: 1
Size: 264831 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 367203 Color: 1
Size: 328183 Color: 1
Size: 304615 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 365365 Color: 1
Size: 336528 Color: 1
Size: 298108 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 455899 Color: 1
Size: 287565 Color: 1
Size: 256537 Color: 0

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 499021 Color: 1
Size: 250936 Color: 1
Size: 250044 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 354719 Color: 1
Size: 350783 Color: 1
Size: 294499 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 427909 Color: 1
Size: 321074 Color: 1
Size: 251018 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 357265 Color: 1
Size: 357236 Color: 1
Size: 285500 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 432621 Color: 1
Size: 303752 Color: 1
Size: 263628 Color: 0

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 478487 Color: 1
Size: 268007 Color: 1
Size: 253507 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 376683 Color: 1
Size: 342004 Color: 1
Size: 281314 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 395703 Color: 1
Size: 344392 Color: 1
Size: 259906 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 366048 Color: 1
Size: 317541 Color: 0
Size: 316412 Color: 1

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 391632 Color: 1
Size: 335181 Color: 1
Size: 273188 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 476565 Color: 1
Size: 271854 Color: 1
Size: 251582 Color: 0

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 480099 Color: 1
Size: 263442 Color: 1
Size: 256460 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 416383 Color: 1
Size: 297540 Color: 0
Size: 286078 Color: 1

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 449417 Color: 1
Size: 290334 Color: 1
Size: 260250 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 411850 Color: 1
Size: 315384 Color: 1
Size: 272767 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 495651 Color: 1
Size: 252340 Color: 0
Size: 252010 Color: 1

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 477934 Color: 1
Size: 262186 Color: 1
Size: 259881 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 471129 Color: 1
Size: 271498 Color: 1
Size: 257374 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 362391 Color: 1
Size: 349607 Color: 1
Size: 288003 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 498025 Color: 1
Size: 251046 Color: 0
Size: 250930 Color: 1

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 485082 Color: 1
Size: 260648 Color: 1
Size: 254271 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 371750 Color: 1
Size: 345544 Color: 1
Size: 282707 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 360959 Color: 1
Size: 347382 Color: 1
Size: 291660 Color: 0

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 390048 Color: 1
Size: 331206 Color: 1
Size: 278747 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 379076 Color: 1
Size: 357436 Color: 1
Size: 263489 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 473255 Color: 1
Size: 276016 Color: 1
Size: 250730 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 384035 Color: 1
Size: 350613 Color: 1
Size: 265353 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 463735 Color: 1
Size: 280586 Color: 1
Size: 255680 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 474425 Color: 1
Size: 269532 Color: 1
Size: 256044 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 396828 Color: 1
Size: 319111 Color: 1
Size: 284062 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 434717 Color: 1
Size: 284620 Color: 1
Size: 280664 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 492733 Color: 1
Size: 255145 Color: 1
Size: 252123 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 362958 Color: 1
Size: 329910 Color: 1
Size: 307133 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 386185 Color: 1
Size: 356752 Color: 1
Size: 257064 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 436705 Color: 1
Size: 306253 Color: 1
Size: 257043 Color: 0

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 456376 Color: 1
Size: 290980 Color: 1
Size: 252645 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 413908 Color: 1
Size: 301525 Color: 1
Size: 284568 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 350774 Color: 1
Size: 334951 Color: 1
Size: 314276 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 401325 Color: 1
Size: 324067 Color: 0
Size: 274609 Color: 1

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 422774 Color: 1
Size: 311970 Color: 1
Size: 265257 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 407629 Color: 1
Size: 315342 Color: 1
Size: 277030 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 375939 Color: 1
Size: 344686 Color: 1
Size: 279376 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 381657 Color: 1
Size: 343351 Color: 1
Size: 274993 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 350632 Color: 1
Size: 331081 Color: 1
Size: 318288 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 356697 Color: 1
Size: 351058 Color: 1
Size: 292246 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 419647 Color: 1
Size: 303845 Color: 0
Size: 276509 Color: 1

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 405224 Color: 1
Size: 343912 Color: 1
Size: 250865 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 464224 Color: 1
Size: 280525 Color: 1
Size: 255252 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 404856 Color: 1
Size: 335179 Color: 1
Size: 259966 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 1
Size: 359710 Color: 1
Size: 267481 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 355817 Color: 1
Size: 349817 Color: 1
Size: 294367 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 498570 Color: 1
Size: 250950 Color: 1
Size: 250481 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 410779 Color: 1
Size: 330603 Color: 1
Size: 258619 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 390707 Color: 1
Size: 353136 Color: 1
Size: 256158 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 442428 Color: 1
Size: 291340 Color: 1
Size: 266233 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 457965 Color: 1
Size: 282171 Color: 1
Size: 259865 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 351125 Color: 1
Size: 346819 Color: 1
Size: 302057 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 455640 Color: 1
Size: 288714 Color: 1
Size: 255647 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 349164 Color: 1
Size: 333808 Color: 1
Size: 317029 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 467151 Color: 1
Size: 268089 Color: 0
Size: 264761 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 386698 Color: 1
Size: 346415 Color: 1
Size: 266888 Color: 0

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 349693 Color: 1
Size: 347749 Color: 1
Size: 302559 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 354106 Color: 1
Size: 338316 Color: 1
Size: 307579 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 368281 Color: 1
Size: 338495 Color: 1
Size: 293225 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 354387 Color: 1
Size: 348662 Color: 1
Size: 296952 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 431591 Color: 1
Size: 287880 Color: 0
Size: 280530 Color: 1

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 360584 Color: 1
Size: 325665 Color: 0
Size: 313752 Color: 1

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 448857 Color: 1
Size: 281352 Color: 1
Size: 269792 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 366278 Color: 1
Size: 335974 Color: 1
Size: 297749 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 383240 Color: 1
Size: 358512 Color: 1
Size: 258249 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 355231 Color: 1
Size: 354461 Color: 1
Size: 290309 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 386337 Color: 1
Size: 316092 Color: 0
Size: 297572 Color: 1

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 439929 Color: 1
Size: 301150 Color: 1
Size: 258922 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 420756 Color: 1
Size: 319778 Color: 1
Size: 259467 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 365291 Color: 1
Size: 321641 Color: 0
Size: 313069 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 411084 Color: 1
Size: 318415 Color: 0
Size: 270502 Color: 1

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 422981 Color: 1
Size: 302130 Color: 0
Size: 274890 Color: 1

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 463203 Color: 1
Size: 273318 Color: 0
Size: 263480 Color: 1

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 434358 Color: 1
Size: 297193 Color: 1
Size: 268450 Color: 0

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 463686 Color: 1
Size: 278928 Color: 1
Size: 257387 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 438804 Color: 1
Size: 305554 Color: 1
Size: 255643 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 454943 Color: 1
Size: 280007 Color: 1
Size: 265051 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 351770 Color: 1
Size: 343995 Color: 1
Size: 304236 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 387736 Color: 1
Size: 307117 Color: 0
Size: 305148 Color: 1

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 423860 Color: 1
Size: 314236 Color: 1
Size: 261905 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 382481 Color: 1
Size: 360210 Color: 1
Size: 257310 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 351650 Color: 1
Size: 341141 Color: 1
Size: 307210 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 404857 Color: 1
Size: 330177 Color: 1
Size: 264967 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 353706 Color: 1
Size: 345572 Color: 1
Size: 300723 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 473121 Color: 1
Size: 268057 Color: 1
Size: 258823 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 472907 Color: 1
Size: 267385 Color: 1
Size: 259709 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 473314 Color: 1
Size: 265997 Color: 1
Size: 260690 Color: 0

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 349380 Color: 1
Size: 343674 Color: 1
Size: 306947 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 426724 Color: 1
Size: 296440 Color: 1
Size: 276837 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 361671 Color: 1
Size: 357215 Color: 1
Size: 281115 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 362892 Color: 1
Size: 360979 Color: 1
Size: 276130 Color: 0

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 411792 Color: 1
Size: 328015 Color: 1
Size: 260194 Color: 0

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 364344 Color: 1
Size: 318111 Color: 1
Size: 317546 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 484885 Color: 1
Size: 261386 Color: 1
Size: 253730 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 398447 Color: 1
Size: 339100 Color: 1
Size: 262454 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 356010 Color: 1
Size: 322553 Color: 1
Size: 321438 Color: 0

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 352543 Color: 1
Size: 342301 Color: 1
Size: 305157 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 352757 Color: 1
Size: 348239 Color: 1
Size: 299005 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 352834 Color: 1
Size: 326659 Color: 1
Size: 320508 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 361635 Color: 1
Size: 320205 Color: 0
Size: 318161 Color: 1

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 359018 Color: 1
Size: 344984 Color: 1
Size: 295999 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 359252 Color: 1
Size: 342484 Color: 1
Size: 298265 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 362353 Color: 1
Size: 348520 Color: 1
Size: 289128 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 357068 Color: 1
Size: 354648 Color: 1
Size: 288285 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 356540 Color: 1
Size: 347310 Color: 1
Size: 296151 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 421348 Color: 1
Size: 311599 Color: 1
Size: 267054 Color: 0

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 386267 Color: 1
Size: 346562 Color: 1
Size: 267172 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 373061 Color: 1
Size: 324432 Color: 1
Size: 302508 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 361075 Color: 1
Size: 326734 Color: 1
Size: 312192 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 354415 Color: 1
Size: 346467 Color: 1
Size: 299119 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 362096 Color: 1
Size: 320570 Color: 1
Size: 317335 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 495091 Color: 1
Size: 254416 Color: 1
Size: 250494 Color: 0

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 352772 Color: 1
Size: 343297 Color: 1
Size: 303932 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 375675 Color: 1
Size: 356112 Color: 1
Size: 268214 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 350913 Color: 1
Size: 343615 Color: 1
Size: 305473 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 355895 Color: 1
Size: 335990 Color: 1
Size: 308116 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 360816 Color: 1
Size: 330352 Color: 1
Size: 308833 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 355726 Color: 1
Size: 352385 Color: 1
Size: 291890 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 376725 Color: 1
Size: 343494 Color: 1
Size: 279782 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 355739 Color: 1
Size: 338718 Color: 1
Size: 305544 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 367552 Color: 1
Size: 344849 Color: 1
Size: 287600 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 370002 Color: 1
Size: 316661 Color: 1
Size: 313338 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 368683 Color: 1
Size: 358578 Color: 1
Size: 272740 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 412483 Color: 1
Size: 318400 Color: 0
Size: 269118 Color: 1

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 380764 Color: 1
Size: 337678 Color: 1
Size: 281559 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 355642 Color: 1
Size: 346174 Color: 1
Size: 298185 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 362021 Color: 1
Size: 329859 Color: 1
Size: 308121 Color: 0

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 358940 Color: 1
Size: 321146 Color: 1
Size: 319915 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 387219 Color: 1
Size: 357844 Color: 1
Size: 254938 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 357177 Color: 1
Size: 324897 Color: 1
Size: 317927 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 356049 Color: 1
Size: 331760 Color: 1
Size: 312192 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 471808 Color: 1
Size: 273837 Color: 1
Size: 254356 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 464205 Color: 1
Size: 284119 Color: 1
Size: 251677 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 362384 Color: 1
Size: 326898 Color: 1
Size: 310719 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 367836 Color: 1
Size: 316241 Color: 1
Size: 315924 Color: 0

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 368101 Color: 1
Size: 326546 Color: 0
Size: 305354 Color: 1

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 358180 Color: 1
Size: 330506 Color: 1
Size: 311315 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 379015 Color: 1
Size: 317385 Color: 0
Size: 303601 Color: 1

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 358030 Color: 1
Size: 343756 Color: 1
Size: 298215 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 360360 Color: 1
Size: 358379 Color: 1
Size: 281262 Color: 0

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 365737 Color: 1
Size: 360979 Color: 1
Size: 273285 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 355902 Color: 1
Size: 335180 Color: 1
Size: 308919 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 357237 Color: 1
Size: 321960 Color: 0
Size: 320804 Color: 1

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 357755 Color: 1
Size: 330641 Color: 0
Size: 311605 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 415338 Color: 1
Size: 306393 Color: 0
Size: 278270 Color: 1

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 346133 Color: 1
Size: 327446 Color: 0
Size: 326422 Color: 1

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 359628 Color: 1
Size: 323881 Color: 1
Size: 316492 Color: 0

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 376521 Color: 1
Size: 343696 Color: 1
Size: 279784 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 386685 Color: 1
Size: 319261 Color: 0
Size: 294055 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 493449 Color: 1
Size: 254705 Color: 1
Size: 251847 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 420876 Color: 1
Size: 293404 Color: 1
Size: 285721 Color: 0

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 357416 Color: 1
Size: 332496 Color: 1
Size: 310089 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 365097 Color: 1
Size: 357962 Color: 1
Size: 276942 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 479467 Color: 1
Size: 268659 Color: 1
Size: 251875 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 355100 Color: 1
Size: 332161 Color: 0
Size: 312740 Color: 1

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 344685 Color: 1
Size: 344420 Color: 1
Size: 310896 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 352869 Color: 1
Size: 337965 Color: 1
Size: 309167 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 352688 Color: 1
Size: 339573 Color: 1
Size: 307740 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 372153 Color: 1
Size: 370501 Color: 1
Size: 257347 Color: 0

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 390923 Color: 1
Size: 339611 Color: 1
Size: 269467 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 353537 Color: 1
Size: 353289 Color: 1
Size: 293175 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 379132 Color: 1
Size: 341431 Color: 1
Size: 279438 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 359707 Color: 1
Size: 354233 Color: 1
Size: 286061 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 378413 Color: 1
Size: 330541 Color: 1
Size: 291047 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 362363 Color: 1
Size: 329549 Color: 1
Size: 308089 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 371480 Color: 1
Size: 350256 Color: 1
Size: 278265 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 355251 Color: 1
Size: 332326 Color: 1
Size: 312424 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 354216 Color: 1
Size: 327343 Color: 1
Size: 318442 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 366719 Color: 1
Size: 324928 Color: 1
Size: 308354 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 358729 Color: 1
Size: 353516 Color: 1
Size: 287756 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 361263 Color: 1
Size: 327368 Color: 1
Size: 311370 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 356866 Color: 1
Size: 347150 Color: 1
Size: 295985 Color: 0

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 355142 Color: 1
Size: 325258 Color: 1
Size: 319601 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 424441 Color: 1
Size: 294504 Color: 0
Size: 281056 Color: 1

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 393547 Color: 1
Size: 347028 Color: 1
Size: 259426 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 354166 Color: 1
Size: 332127 Color: 1
Size: 313708 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 387995 Color: 1
Size: 353938 Color: 1
Size: 258068 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 366166 Color: 1
Size: 334885 Color: 1
Size: 298950 Color: 0

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 434157 Color: 1
Size: 312495 Color: 1
Size: 253349 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 353045 Color: 1
Size: 326800 Color: 1
Size: 320156 Color: 0

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 352936 Color: 1
Size: 336997 Color: 1
Size: 310068 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 352808 Color: 1
Size: 342316 Color: 1
Size: 304877 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 352544 Color: 1
Size: 337115 Color: 1
Size: 310342 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 385503 Color: 1
Size: 361072 Color: 1
Size: 253426 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 351475 Color: 1
Size: 351256 Color: 1
Size: 297270 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 351919 Color: 1
Size: 346540 Color: 1
Size: 301542 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 349722 Color: 1
Size: 341052 Color: 1
Size: 309227 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 387808 Color: 1
Size: 308204 Color: 0
Size: 303989 Color: 1

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 361181 Color: 1
Size: 320478 Color: 0
Size: 318342 Color: 1

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 420885 Color: 1
Size: 302934 Color: 1
Size: 276182 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 377042 Color: 1
Size: 359514 Color: 1
Size: 263445 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 355214 Color: 1
Size: 343671 Color: 1
Size: 301116 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 355769 Color: 1
Size: 343256 Color: 1
Size: 300976 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 357842 Color: 1
Size: 342287 Color: 1
Size: 299872 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 361068 Color: 1
Size: 340518 Color: 1
Size: 298415 Color: 0

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 361651 Color: 1
Size: 347820 Color: 1
Size: 290530 Color: 0

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 362186 Color: 1
Size: 335889 Color: 1
Size: 301926 Color: 0

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 362452 Color: 1
Size: 338565 Color: 1
Size: 298984 Color: 0

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 362523 Color: 1
Size: 354072 Color: 1
Size: 283406 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 362619 Color: 1
Size: 352695 Color: 1
Size: 284687 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 362730 Color: 1
Size: 352793 Color: 1
Size: 284478 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 362759 Color: 1
Size: 339448 Color: 1
Size: 297794 Color: 0

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 362794 Color: 1
Size: 338314 Color: 1
Size: 298893 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 362820 Color: 1
Size: 341950 Color: 1
Size: 295231 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 362993 Color: 1
Size: 323065 Color: 0
Size: 313943 Color: 1

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 363158 Color: 1
Size: 342018 Color: 1
Size: 294825 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 363452 Color: 1
Size: 334469 Color: 1
Size: 302080 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 363638 Color: 1
Size: 349019 Color: 1
Size: 287344 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 363736 Color: 1
Size: 341143 Color: 1
Size: 295122 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 363750 Color: 1
Size: 356914 Color: 1
Size: 279337 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 363773 Color: 1
Size: 342201 Color: 1
Size: 294027 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 363823 Color: 1
Size: 361735 Color: 1
Size: 274443 Color: 0

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 363859 Color: 1
Size: 348592 Color: 1
Size: 287550 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 363891 Color: 1
Size: 320915 Color: 0
Size: 315195 Color: 1

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 363941 Color: 1
Size: 345843 Color: 1
Size: 290217 Color: 0

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 364111 Color: 1
Size: 328558 Color: 1
Size: 307332 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 364213 Color: 1
Size: 340796 Color: 1
Size: 294992 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 364291 Color: 1
Size: 325269 Color: 1
Size: 310441 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 364323 Color: 1
Size: 340008 Color: 1
Size: 295670 Color: 0

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 364357 Color: 1
Size: 348292 Color: 1
Size: 287352 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 364363 Color: 1
Size: 328798 Color: 1
Size: 306840 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 364408 Color: 1
Size: 328797 Color: 1
Size: 306796 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 364503 Color: 1
Size: 319532 Color: 1
Size: 315966 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 364529 Color: 1
Size: 342039 Color: 1
Size: 293433 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 388931 Color: 1
Size: 314132 Color: 1
Size: 296938 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 365022 Color: 1
Size: 332331 Color: 1
Size: 302648 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 365076 Color: 1
Size: 337780 Color: 1
Size: 297145 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 365217 Color: 1
Size: 341291 Color: 1
Size: 293493 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 365251 Color: 1
Size: 346251 Color: 1
Size: 288499 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 365329 Color: 1
Size: 333128 Color: 1
Size: 301544 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 365356 Color: 1
Size: 326621 Color: 1
Size: 308024 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 365464 Color: 1
Size: 333508 Color: 1
Size: 301029 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 365506 Color: 1
Size: 340630 Color: 1
Size: 293865 Color: 0

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 365673 Color: 1
Size: 352315 Color: 1
Size: 282013 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 365748 Color: 1
Size: 318333 Color: 1
Size: 315920 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 365829 Color: 1
Size: 338193 Color: 1
Size: 295979 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 365835 Color: 1
Size: 328789 Color: 1
Size: 305377 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 365894 Color: 1
Size: 328413 Color: 1
Size: 305694 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 366024 Color: 1
Size: 330555 Color: 1
Size: 303422 Color: 0

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 366063 Color: 1
Size: 338605 Color: 1
Size: 295333 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 366100 Color: 1
Size: 361897 Color: 1
Size: 272004 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 366394 Color: 1
Size: 319071 Color: 1
Size: 314536 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 366420 Color: 1
Size: 344064 Color: 1
Size: 289517 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 366525 Color: 1
Size: 353994 Color: 1
Size: 279482 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 366530 Color: 1
Size: 346230 Color: 1
Size: 287241 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 366610 Color: 1
Size: 323076 Color: 1
Size: 310315 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 366725 Color: 1
Size: 333784 Color: 1
Size: 299492 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 366805 Color: 1
Size: 330714 Color: 1
Size: 302482 Color: 0

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 366826 Color: 1
Size: 357585 Color: 1
Size: 275590 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 366836 Color: 1
Size: 341510 Color: 1
Size: 291655 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 366876 Color: 1
Size: 342096 Color: 1
Size: 291029 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 366940 Color: 1
Size: 359189 Color: 1
Size: 273872 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 367043 Color: 1
Size: 330080 Color: 1
Size: 302878 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 367103 Color: 1
Size: 341791 Color: 1
Size: 291107 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 367221 Color: 1
Size: 350999 Color: 1
Size: 281781 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 367508 Color: 1
Size: 340341 Color: 1
Size: 292152 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 367556 Color: 1
Size: 355666 Color: 1
Size: 276779 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 367557 Color: 1
Size: 317166 Color: 1
Size: 315278 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 367650 Color: 1
Size: 347025 Color: 1
Size: 285326 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 367711 Color: 1
Size: 332312 Color: 1
Size: 299978 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 367779 Color: 1
Size: 316898 Color: 0
Size: 315324 Color: 1

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 367845 Color: 1
Size: 331415 Color: 1
Size: 300741 Color: 0

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 367864 Color: 1
Size: 337858 Color: 1
Size: 294279 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 367992 Color: 1
Size: 317806 Color: 0
Size: 314203 Color: 1

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 368039 Color: 1
Size: 336653 Color: 1
Size: 295309 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 368108 Color: 1
Size: 361073 Color: 1
Size: 270820 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 368306 Color: 1
Size: 332896 Color: 1
Size: 298799 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 368315 Color: 1
Size: 358609 Color: 1
Size: 273077 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 368446 Color: 1
Size: 340906 Color: 1
Size: 290649 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 368557 Color: 1
Size: 355679 Color: 1
Size: 275765 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 368591 Color: 1
Size: 331191 Color: 1
Size: 300219 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 368623 Color: 1
Size: 345896 Color: 1
Size: 285482 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 368673 Color: 1
Size: 349841 Color: 1
Size: 281487 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 360551 Color: 1
Size: 359643 Color: 1
Size: 279807 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 368706 Color: 1
Size: 348119 Color: 1
Size: 283176 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 368707 Color: 1
Size: 354212 Color: 1
Size: 277082 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 368962 Color: 1
Size: 334233 Color: 1
Size: 296806 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 368970 Color: 1
Size: 364903 Color: 1
Size: 266128 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 369012 Color: 1
Size: 316961 Color: 1
Size: 314028 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 369035 Color: 1
Size: 352440 Color: 1
Size: 278526 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 369042 Color: 1
Size: 335753 Color: 1
Size: 295206 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 369070 Color: 1
Size: 342118 Color: 1
Size: 288813 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 369144 Color: 1
Size: 357461 Color: 1
Size: 273396 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 369246 Color: 1
Size: 321548 Color: 1
Size: 309207 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 369296 Color: 1
Size: 333464 Color: 1
Size: 297241 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 369348 Color: 1
Size: 330951 Color: 1
Size: 299702 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 369433 Color: 1
Size: 334912 Color: 1
Size: 295656 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 369496 Color: 1
Size: 329893 Color: 1
Size: 300612 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 369536 Color: 1
Size: 341457 Color: 1
Size: 289008 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 369560 Color: 1
Size: 350014 Color: 1
Size: 280427 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 369665 Color: 1
Size: 322117 Color: 1
Size: 308219 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 369696 Color: 1
Size: 363155 Color: 1
Size: 267150 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 369703 Color: 1
Size: 348917 Color: 1
Size: 281381 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 369845 Color: 1
Size: 351170 Color: 1
Size: 278986 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 369911 Color: 1
Size: 344331 Color: 1
Size: 285759 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 369931 Color: 1
Size: 323731 Color: 1
Size: 306339 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 369955 Color: 1
Size: 352997 Color: 1
Size: 277049 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 370082 Color: 1
Size: 363362 Color: 1
Size: 266557 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 370098 Color: 1
Size: 333721 Color: 1
Size: 296182 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 370142 Color: 1
Size: 345737 Color: 1
Size: 284122 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 370210 Color: 1
Size: 342506 Color: 1
Size: 287285 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 370713 Color: 1
Size: 346719 Color: 1
Size: 282569 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 370795 Color: 1
Size: 328364 Color: 1
Size: 300842 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 370856 Color: 1
Size: 363118 Color: 1
Size: 266027 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 370936 Color: 1
Size: 347287 Color: 1
Size: 281778 Color: 0

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 370964 Color: 1
Size: 324866 Color: 1
Size: 304171 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 370977 Color: 1
Size: 327482 Color: 1
Size: 301542 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 371018 Color: 1
Size: 354848 Color: 1
Size: 274135 Color: 0

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 371173 Color: 1
Size: 329570 Color: 1
Size: 299258 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 371253 Color: 1
Size: 327395 Color: 1
Size: 301353 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 371257 Color: 1
Size: 337950 Color: 1
Size: 290794 Color: 0

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 371406 Color: 1
Size: 339312 Color: 1
Size: 289283 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 371427 Color: 1
Size: 316929 Color: 1
Size: 311645 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 371483 Color: 1
Size: 333321 Color: 1
Size: 295197 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 371550 Color: 1
Size: 322760 Color: 1
Size: 305691 Color: 0

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 371585 Color: 1
Size: 359152 Color: 1
Size: 269264 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 371657 Color: 1
Size: 332376 Color: 1
Size: 295968 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 371661 Color: 1
Size: 335482 Color: 1
Size: 292858 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 371675 Color: 1
Size: 360906 Color: 1
Size: 267420 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 371768 Color: 1
Size: 342534 Color: 1
Size: 285699 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 371779 Color: 1
Size: 342694 Color: 1
Size: 285528 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 371781 Color: 1
Size: 328103 Color: 1
Size: 300117 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 371835 Color: 1
Size: 347272 Color: 1
Size: 280894 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 371885 Color: 1
Size: 330069 Color: 1
Size: 298047 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 371963 Color: 1
Size: 360851 Color: 1
Size: 267187 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 372050 Color: 1
Size: 345401 Color: 1
Size: 282550 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 372071 Color: 1
Size: 349410 Color: 1
Size: 278520 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 372077 Color: 1
Size: 336650 Color: 1
Size: 291274 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 372187 Color: 1
Size: 355672 Color: 1
Size: 272142 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 372226 Color: 1
Size: 333731 Color: 1
Size: 294044 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 372250 Color: 1
Size: 318301 Color: 0
Size: 309450 Color: 1

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 383787 Color: 1
Size: 344227 Color: 1
Size: 271987 Color: 0

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 372316 Color: 1
Size: 343798 Color: 1
Size: 283887 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 372401 Color: 1
Size: 340991 Color: 1
Size: 286609 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 372456 Color: 1
Size: 351686 Color: 1
Size: 275859 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 372622 Color: 1
Size: 329682 Color: 1
Size: 297697 Color: 0

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 372684 Color: 1
Size: 332536 Color: 1
Size: 294781 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 372772 Color: 1
Size: 316005 Color: 0
Size: 311224 Color: 1

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 372863 Color: 1
Size: 338192 Color: 1
Size: 288946 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 373009 Color: 1
Size: 343214 Color: 1
Size: 283778 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 373018 Color: 1
Size: 367697 Color: 1
Size: 259286 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 373023 Color: 1
Size: 338512 Color: 1
Size: 288466 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 373293 Color: 1
Size: 339525 Color: 1
Size: 287183 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 373412 Color: 1
Size: 332517 Color: 1
Size: 294072 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 373457 Color: 1
Size: 334884 Color: 1
Size: 291660 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 373487 Color: 1
Size: 317077 Color: 1
Size: 309437 Color: 0

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 373676 Color: 1
Size: 314765 Color: 0
Size: 311560 Color: 1

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 373815 Color: 1
Size: 320048 Color: 1
Size: 306138 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 373820 Color: 1
Size: 357699 Color: 1
Size: 268482 Color: 0

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 373833 Color: 1
Size: 357926 Color: 1
Size: 268242 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 373842 Color: 1
Size: 346916 Color: 1
Size: 279243 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 373906 Color: 1
Size: 313869 Color: 0
Size: 312226 Color: 1

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 374166 Color: 1
Size: 344732 Color: 1
Size: 281103 Color: 0

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 374167 Color: 1
Size: 359477 Color: 1
Size: 266357 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 374236 Color: 1
Size: 352726 Color: 1
Size: 273039 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 374236 Color: 1
Size: 335518 Color: 1
Size: 290247 Color: 0

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 374352 Color: 1
Size: 345948 Color: 1
Size: 279701 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 374443 Color: 1
Size: 344117 Color: 1
Size: 281441 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 374571 Color: 1
Size: 324364 Color: 1
Size: 301066 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 374674 Color: 1
Size: 332292 Color: 1
Size: 293035 Color: 0

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 374718 Color: 1
Size: 353427 Color: 1
Size: 271856 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 374947 Color: 1
Size: 338721 Color: 1
Size: 286333 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 374991 Color: 1
Size: 326518 Color: 1
Size: 298492 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 375024 Color: 1
Size: 330722 Color: 0
Size: 294255 Color: 1

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 375118 Color: 1
Size: 346219 Color: 1
Size: 278664 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 375138 Color: 1
Size: 339745 Color: 1
Size: 285118 Color: 0

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 375248 Color: 1
Size: 339896 Color: 1
Size: 284857 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 375257 Color: 1
Size: 335559 Color: 1
Size: 289185 Color: 0

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 375270 Color: 1
Size: 353901 Color: 1
Size: 270830 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 375416 Color: 1
Size: 357362 Color: 1
Size: 267223 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 375417 Color: 1
Size: 352431 Color: 1
Size: 272153 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 375519 Color: 1
Size: 321964 Color: 0
Size: 302518 Color: 1

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 375527 Color: 1
Size: 332028 Color: 1
Size: 292446 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 375584 Color: 1
Size: 335976 Color: 1
Size: 288441 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 375916 Color: 1
Size: 345565 Color: 1
Size: 278520 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 375941 Color: 1
Size: 367121 Color: 1
Size: 256939 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 376196 Color: 1
Size: 359750 Color: 1
Size: 264055 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 376285 Color: 1
Size: 326081 Color: 1
Size: 297635 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 376294 Color: 1
Size: 338346 Color: 1
Size: 285361 Color: 0

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 376315 Color: 1
Size: 355893 Color: 1
Size: 267793 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 376336 Color: 1
Size: 333551 Color: 1
Size: 290114 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 376373 Color: 1
Size: 317740 Color: 1
Size: 305888 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 376404 Color: 1
Size: 350460 Color: 1
Size: 273137 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 376568 Color: 1
Size: 324695 Color: 1
Size: 298738 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 376618 Color: 1
Size: 345035 Color: 1
Size: 278348 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 376635 Color: 1
Size: 369320 Color: 1
Size: 254046 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 376669 Color: 1
Size: 317438 Color: 0
Size: 305894 Color: 1

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 376736 Color: 1
Size: 355613 Color: 1
Size: 267652 Color: 0

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 376772 Color: 1
Size: 326967 Color: 1
Size: 296262 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 376798 Color: 1
Size: 313996 Color: 0
Size: 309207 Color: 1

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 376902 Color: 1
Size: 313548 Color: 0
Size: 309551 Color: 1

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 376920 Color: 1
Size: 355665 Color: 1
Size: 267416 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 376966 Color: 1
Size: 317503 Color: 0
Size: 305532 Color: 1

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 377166 Color: 1
Size: 322478 Color: 1
Size: 300357 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 377293 Color: 1
Size: 356076 Color: 1
Size: 266632 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 377452 Color: 1
Size: 333709 Color: 1
Size: 288840 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 377512 Color: 1
Size: 338142 Color: 1
Size: 284347 Color: 0

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 377538 Color: 1
Size: 335019 Color: 1
Size: 287444 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 377640 Color: 1
Size: 340835 Color: 1
Size: 281526 Color: 0

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 377645 Color: 1
Size: 341663 Color: 1
Size: 280693 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 377913 Color: 1
Size: 329644 Color: 1
Size: 292444 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 377967 Color: 1
Size: 345531 Color: 1
Size: 276503 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 377986 Color: 1
Size: 316587 Color: 1
Size: 305428 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 377989 Color: 1
Size: 315201 Color: 0
Size: 306811 Color: 1

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 378009 Color: 1
Size: 339017 Color: 1
Size: 282975 Color: 0

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 378029 Color: 1
Size: 341401 Color: 1
Size: 280571 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 378039 Color: 1
Size: 317946 Color: 1
Size: 304016 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 378164 Color: 1
Size: 312383 Color: 1
Size: 309454 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 378221 Color: 1
Size: 328881 Color: 0
Size: 292899 Color: 1

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 378394 Color: 1
Size: 337721 Color: 1
Size: 283886 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 378452 Color: 1
Size: 316985 Color: 0
Size: 304564 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 378570 Color: 1
Size: 351767 Color: 1
Size: 269664 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 378676 Color: 1
Size: 331599 Color: 1
Size: 289726 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 378689 Color: 1
Size: 327731 Color: 1
Size: 293581 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 378745 Color: 1
Size: 333933 Color: 1
Size: 287323 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 378774 Color: 1
Size: 318492 Color: 1
Size: 302735 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 379044 Color: 1
Size: 321650 Color: 1
Size: 299307 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 379071 Color: 1
Size: 343407 Color: 1
Size: 277523 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 379226 Color: 1
Size: 351837 Color: 1
Size: 268938 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 379307 Color: 1
Size: 350173 Color: 1
Size: 270521 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 379518 Color: 1
Size: 344343 Color: 1
Size: 276140 Color: 0

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 379730 Color: 1
Size: 335642 Color: 1
Size: 284629 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 379786 Color: 1
Size: 342324 Color: 1
Size: 277891 Color: 0

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 379787 Color: 1
Size: 325837 Color: 1
Size: 294377 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 379850 Color: 1
Size: 360716 Color: 1
Size: 259435 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 380009 Color: 1
Size: 337298 Color: 1
Size: 282694 Color: 0

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 380017 Color: 1
Size: 342403 Color: 1
Size: 277581 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 380036 Color: 1
Size: 329646 Color: 1
Size: 290319 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 380263 Color: 1
Size: 329380 Color: 1
Size: 290358 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 380486 Color: 1
Size: 322563 Color: 1
Size: 296952 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 380488 Color: 1
Size: 340777 Color: 1
Size: 278736 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 380526 Color: 1
Size: 347525 Color: 1
Size: 271950 Color: 0

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 380528 Color: 1
Size: 349277 Color: 1
Size: 270196 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 380536 Color: 1
Size: 349948 Color: 1
Size: 269517 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 380594 Color: 1
Size: 326897 Color: 1
Size: 292510 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 380601 Color: 1
Size: 335015 Color: 1
Size: 284385 Color: 0

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 380668 Color: 1
Size: 310072 Color: 0
Size: 309261 Color: 1

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 380679 Color: 1
Size: 332817 Color: 1
Size: 286505 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 380693 Color: 1
Size: 333850 Color: 1
Size: 285458 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 380787 Color: 1
Size: 317000 Color: 0
Size: 302214 Color: 1

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 380843 Color: 1
Size: 344862 Color: 1
Size: 274296 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 380947 Color: 1
Size: 324683 Color: 1
Size: 294371 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 381009 Color: 1
Size: 314073 Color: 1
Size: 304919 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 381163 Color: 1
Size: 353187 Color: 1
Size: 265651 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 381251 Color: 1
Size: 324985 Color: 1
Size: 293765 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 381263 Color: 1
Size: 361723 Color: 1
Size: 257015 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 381320 Color: 1
Size: 344676 Color: 1
Size: 274005 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 381403 Color: 1
Size: 364692 Color: 1
Size: 253906 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 381482 Color: 1
Size: 327800 Color: 1
Size: 290719 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 381520 Color: 1
Size: 361929 Color: 1
Size: 256552 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 381569 Color: 1
Size: 309826 Color: 0
Size: 308606 Color: 1

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 381591 Color: 1
Size: 333000 Color: 1
Size: 285410 Color: 0

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 381598 Color: 1
Size: 320667 Color: 1
Size: 297736 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 381615 Color: 1
Size: 335908 Color: 1
Size: 282478 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 381786 Color: 1
Size: 359476 Color: 1
Size: 258739 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 381821 Color: 1
Size: 342474 Color: 1
Size: 275706 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 382018 Color: 1
Size: 341154 Color: 1
Size: 276829 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 382072 Color: 1
Size: 325706 Color: 1
Size: 292223 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 382166 Color: 1
Size: 335645 Color: 1
Size: 282190 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 382198 Color: 1
Size: 332053 Color: 1
Size: 285750 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 382220 Color: 1
Size: 316890 Color: 1
Size: 300891 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 382254 Color: 1
Size: 342588 Color: 1
Size: 275159 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 382294 Color: 1
Size: 326049 Color: 1
Size: 291658 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 382299 Color: 1
Size: 343462 Color: 1
Size: 274240 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 382314 Color: 1
Size: 342428 Color: 1
Size: 275259 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 382380 Color: 1
Size: 336068 Color: 1
Size: 281553 Color: 0

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 382545 Color: 1
Size: 326926 Color: 1
Size: 290530 Color: 0

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 382561 Color: 1
Size: 336816 Color: 1
Size: 280624 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 371507 Color: 1
Size: 355211 Color: 1
Size: 273283 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 382628 Color: 1
Size: 330207 Color: 1
Size: 287166 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 382661 Color: 1
Size: 357377 Color: 1
Size: 259963 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 382677 Color: 1
Size: 332460 Color: 1
Size: 284864 Color: 0

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 382721 Color: 1
Size: 346553 Color: 1
Size: 270727 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 382736 Color: 1
Size: 344230 Color: 1
Size: 273035 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 382745 Color: 1
Size: 338226 Color: 1
Size: 279030 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 382751 Color: 1
Size: 345689 Color: 1
Size: 271561 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 382889 Color: 1
Size: 308744 Color: 1
Size: 308368 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 383132 Color: 1
Size: 309267 Color: 0
Size: 307602 Color: 1

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 383197 Color: 1
Size: 350466 Color: 1
Size: 266338 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 383404 Color: 1
Size: 335175 Color: 1
Size: 281422 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 364708 Color: 1
Size: 345060 Color: 1
Size: 290233 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 383478 Color: 1
Size: 351333 Color: 1
Size: 265190 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 383507 Color: 1
Size: 329675 Color: 1
Size: 286819 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 383521 Color: 1
Size: 359308 Color: 1
Size: 257172 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 383527 Color: 1
Size: 332927 Color: 1
Size: 283547 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 383536 Color: 1
Size: 319333 Color: 1
Size: 297132 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 383571 Color: 1
Size: 344537 Color: 1
Size: 271893 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 383583 Color: 1
Size: 317868 Color: 1
Size: 298550 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 383644 Color: 1
Size: 319671 Color: 1
Size: 296686 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 383652 Color: 1
Size: 344215 Color: 1
Size: 272134 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 383722 Color: 1
Size: 349904 Color: 1
Size: 266375 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 383735 Color: 1
Size: 327408 Color: 1
Size: 288858 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 383739 Color: 1
Size: 313805 Color: 1
Size: 302457 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 383746 Color: 1
Size: 320936 Color: 1
Size: 295319 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 384026 Color: 1
Size: 321358 Color: 1
Size: 294617 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 384123 Color: 1
Size: 317689 Color: 1
Size: 298189 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 384174 Color: 1
Size: 345856 Color: 1
Size: 269971 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 384367 Color: 1
Size: 353717 Color: 1
Size: 261917 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 384507 Color: 1
Size: 327495 Color: 1
Size: 287999 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 384517 Color: 1
Size: 345978 Color: 1
Size: 269506 Color: 0

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 384674 Color: 1
Size: 359033 Color: 1
Size: 256294 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 384677 Color: 1
Size: 352030 Color: 1
Size: 263294 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 384939 Color: 1
Size: 313254 Color: 0
Size: 301808 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 384956 Color: 1
Size: 342566 Color: 1
Size: 272479 Color: 0

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 384959 Color: 1
Size: 309365 Color: 1
Size: 305677 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 385064 Color: 1
Size: 325680 Color: 1
Size: 289257 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 385121 Color: 1
Size: 330359 Color: 1
Size: 284521 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 385172 Color: 1
Size: 338574 Color: 1
Size: 276255 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 385204 Color: 1
Size: 309072 Color: 1
Size: 305725 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 385337 Color: 1
Size: 345532 Color: 1
Size: 269132 Color: 0

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 385371 Color: 1
Size: 333602 Color: 1
Size: 281028 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 385673 Color: 1
Size: 340611 Color: 1
Size: 273717 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 385697 Color: 1
Size: 348063 Color: 1
Size: 266241 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 385711 Color: 1
Size: 346434 Color: 1
Size: 267856 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 385736 Color: 1
Size: 318370 Color: 1
Size: 295895 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 385747 Color: 1
Size: 321762 Color: 1
Size: 292492 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 385831 Color: 1
Size: 324548 Color: 1
Size: 289622 Color: 0

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 385906 Color: 1
Size: 335093 Color: 1
Size: 279002 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 386045 Color: 1
Size: 327333 Color: 1
Size: 286623 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 386062 Color: 1
Size: 338821 Color: 1
Size: 275118 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 386182 Color: 1
Size: 347635 Color: 1
Size: 266184 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 386327 Color: 1
Size: 362124 Color: 1
Size: 251550 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 386386 Color: 1
Size: 319302 Color: 0
Size: 294313 Color: 1

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 386403 Color: 1
Size: 325140 Color: 1
Size: 288458 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 386404 Color: 1
Size: 330480 Color: 1
Size: 283117 Color: 0

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 386476 Color: 1
Size: 337582 Color: 1
Size: 275943 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 386572 Color: 1
Size: 340031 Color: 1
Size: 273398 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 386597 Color: 1
Size: 317208 Color: 1
Size: 296196 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 386608 Color: 1
Size: 327664 Color: 1
Size: 285729 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 386642 Color: 1
Size: 352654 Color: 1
Size: 260705 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 386818 Color: 1
Size: 312930 Color: 0
Size: 300253 Color: 1

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 386846 Color: 1
Size: 337446 Color: 1
Size: 275709 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 386882 Color: 1
Size: 355937 Color: 1
Size: 257182 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 386895 Color: 1
Size: 334825 Color: 1
Size: 278281 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 386941 Color: 1
Size: 321068 Color: 1
Size: 291992 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 386950 Color: 1
Size: 336416 Color: 1
Size: 276635 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 387234 Color: 1
Size: 334496 Color: 1
Size: 278271 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 387302 Color: 1
Size: 338393 Color: 1
Size: 274306 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 387332 Color: 1
Size: 329200 Color: 1
Size: 283469 Color: 0

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 387562 Color: 1
Size: 339135 Color: 1
Size: 273304 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 387652 Color: 1
Size: 330046 Color: 1
Size: 282303 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 387659 Color: 1
Size: 328430 Color: 1
Size: 283912 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 387661 Color: 1
Size: 330209 Color: 1
Size: 282131 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 387733 Color: 1
Size: 344187 Color: 1
Size: 268081 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 387816 Color: 1
Size: 328903 Color: 1
Size: 283282 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 387839 Color: 1
Size: 322724 Color: 0
Size: 289438 Color: 1

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 387922 Color: 1
Size: 319577 Color: 1
Size: 292502 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 388096 Color: 1
Size: 339812 Color: 1
Size: 272093 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 388169 Color: 1
Size: 338892 Color: 1
Size: 272940 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 388333 Color: 1
Size: 315902 Color: 1
Size: 295766 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 388505 Color: 1
Size: 318253 Color: 1
Size: 293243 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 388571 Color: 1
Size: 329859 Color: 1
Size: 281571 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 388663 Color: 1
Size: 310046 Color: 1
Size: 301292 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 388738 Color: 1
Size: 326606 Color: 1
Size: 284657 Color: 0

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 388786 Color: 1
Size: 326235 Color: 1
Size: 284980 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 388810 Color: 1
Size: 350081 Color: 1
Size: 261110 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 388818 Color: 1
Size: 330536 Color: 1
Size: 280647 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 388955 Color: 1
Size: 316436 Color: 0
Size: 294610 Color: 1

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 388992 Color: 1
Size: 324463 Color: 1
Size: 286546 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 389048 Color: 1
Size: 342945 Color: 1
Size: 268008 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 389180 Color: 1
Size: 317362 Color: 1
Size: 293459 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 389199 Color: 1
Size: 326501 Color: 1
Size: 284301 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 389300 Color: 1
Size: 315510 Color: 1
Size: 295191 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 389336 Color: 1
Size: 323691 Color: 1
Size: 286974 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 389400 Color: 1
Size: 335586 Color: 1
Size: 275015 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 389480 Color: 1
Size: 307290 Color: 1
Size: 303231 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 389555 Color: 1
Size: 321461 Color: 1
Size: 288985 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 389634 Color: 1
Size: 319924 Color: 1
Size: 290443 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 389859 Color: 1
Size: 337090 Color: 1
Size: 273052 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 389970 Color: 1
Size: 339873 Color: 1
Size: 270158 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 389979 Color: 1
Size: 306904 Color: 1
Size: 303118 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 389992 Color: 1
Size: 307562 Color: 1
Size: 302447 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 389996 Color: 1
Size: 328673 Color: 1
Size: 281332 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 389998 Color: 1
Size: 339020 Color: 1
Size: 270983 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 390044 Color: 1
Size: 325060 Color: 1
Size: 284897 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 390084 Color: 1
Size: 334868 Color: 1
Size: 275049 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 390144 Color: 1
Size: 310704 Color: 1
Size: 299153 Color: 0

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 390233 Color: 1
Size: 345045 Color: 1
Size: 264723 Color: 0

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 390328 Color: 1
Size: 334951 Color: 1
Size: 274722 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 390336 Color: 1
Size: 312096 Color: 1
Size: 297569 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 390508 Color: 1
Size: 340158 Color: 1
Size: 269335 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 390515 Color: 1
Size: 324786 Color: 1
Size: 284700 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 390531 Color: 1
Size: 338531 Color: 1
Size: 270939 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 390610 Color: 1
Size: 314675 Color: 1
Size: 294716 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 390636 Color: 1
Size: 333566 Color: 1
Size: 275799 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 390667 Color: 1
Size: 328689 Color: 1
Size: 280645 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 390704 Color: 1
Size: 341742 Color: 1
Size: 267555 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 390740 Color: 1
Size: 327978 Color: 1
Size: 281283 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 390826 Color: 1
Size: 326969 Color: 1
Size: 282206 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 390948 Color: 1
Size: 335010 Color: 1
Size: 274043 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 391092 Color: 1
Size: 325339 Color: 1
Size: 283570 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 391093 Color: 1
Size: 344626 Color: 1
Size: 264282 Color: 0

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 391119 Color: 1
Size: 326167 Color: 1
Size: 282715 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 391313 Color: 1
Size: 331379 Color: 1
Size: 277309 Color: 0

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 391389 Color: 1
Size: 339123 Color: 1
Size: 269489 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 391427 Color: 1
Size: 323398 Color: 1
Size: 285176 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 391439 Color: 1
Size: 344946 Color: 1
Size: 263616 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 391458 Color: 1
Size: 307885 Color: 1
Size: 300658 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 391551 Color: 1
Size: 320484 Color: 0
Size: 287966 Color: 1

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 391591 Color: 1
Size: 305127 Color: 0
Size: 303283 Color: 1

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 391670 Color: 1
Size: 324990 Color: 0
Size: 283341 Color: 1

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 391745 Color: 1
Size: 343985 Color: 1
Size: 264271 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 391798 Color: 1
Size: 337383 Color: 1
Size: 270820 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 391885 Color: 1
Size: 337805 Color: 1
Size: 270311 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 392098 Color: 1
Size: 346310 Color: 1
Size: 261593 Color: 0

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 392210 Color: 1
Size: 342146 Color: 1
Size: 265645 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 392356 Color: 1
Size: 342141 Color: 1
Size: 265504 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 392358 Color: 1
Size: 333716 Color: 1
Size: 273927 Color: 0

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 392425 Color: 1
Size: 336091 Color: 1
Size: 271485 Color: 0

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 392433 Color: 1
Size: 330787 Color: 1
Size: 276781 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 392534 Color: 1
Size: 336691 Color: 1
Size: 270776 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 392779 Color: 1
Size: 314935 Color: 0
Size: 292287 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 392780 Color: 1
Size: 321632 Color: 1
Size: 285589 Color: 0

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 392786 Color: 1
Size: 330655 Color: 1
Size: 276560 Color: 0

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 392850 Color: 1
Size: 306336 Color: 1
Size: 300815 Color: 0

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 392885 Color: 1
Size: 319625 Color: 1
Size: 287491 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 393042 Color: 1
Size: 320184 Color: 1
Size: 286775 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 393089 Color: 1
Size: 337730 Color: 1
Size: 269182 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 393121 Color: 1
Size: 336276 Color: 1
Size: 270604 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 393182 Color: 1
Size: 329107 Color: 1
Size: 277712 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 393332 Color: 1
Size: 329162 Color: 1
Size: 277507 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 393349 Color: 1
Size: 318634 Color: 1
Size: 288018 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 393562 Color: 1
Size: 330414 Color: 1
Size: 276025 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 393616 Color: 1
Size: 348184 Color: 1
Size: 258201 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 393807 Color: 1
Size: 318176 Color: 1
Size: 288018 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 393910 Color: 1
Size: 309966 Color: 0
Size: 296125 Color: 1

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 394218 Color: 1
Size: 332204 Color: 1
Size: 273579 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 394489 Color: 1
Size: 335466 Color: 1
Size: 270046 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 394597 Color: 1
Size: 342396 Color: 1
Size: 263008 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 394655 Color: 1
Size: 310156 Color: 1
Size: 295190 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 394665 Color: 1
Size: 326223 Color: 1
Size: 279113 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 394745 Color: 1
Size: 337942 Color: 1
Size: 267314 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 394750 Color: 1
Size: 342341 Color: 1
Size: 262910 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 394757 Color: 1
Size: 351931 Color: 1
Size: 253313 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 394824 Color: 1
Size: 318987 Color: 1
Size: 286190 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 394868 Color: 1
Size: 319498 Color: 1
Size: 285635 Color: 0

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 394895 Color: 1
Size: 345543 Color: 1
Size: 259563 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 394990 Color: 1
Size: 333862 Color: 1
Size: 271149 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 394991 Color: 1
Size: 303512 Color: 1
Size: 301498 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 395043 Color: 1
Size: 339598 Color: 1
Size: 265360 Color: 0

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 395174 Color: 1
Size: 311262 Color: 1
Size: 293565 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 395245 Color: 1
Size: 316967 Color: 1
Size: 287789 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 395313 Color: 1
Size: 338526 Color: 1
Size: 266162 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 395366 Color: 1
Size: 343708 Color: 1
Size: 260927 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 395445 Color: 1
Size: 321067 Color: 1
Size: 283489 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 395485 Color: 1
Size: 335950 Color: 1
Size: 268566 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 395646 Color: 1
Size: 320005 Color: 1
Size: 284350 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 395770 Color: 1
Size: 316537 Color: 1
Size: 287694 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 395788 Color: 1
Size: 335769 Color: 1
Size: 268444 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 395933 Color: 1
Size: 349752 Color: 1
Size: 254316 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 396030 Color: 1
Size: 331446 Color: 1
Size: 272525 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 396030 Color: 1
Size: 323252 Color: 1
Size: 280719 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 396131 Color: 1
Size: 333890 Color: 1
Size: 269980 Color: 0

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 396142 Color: 1
Size: 321838 Color: 1
Size: 282021 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 396246 Color: 1
Size: 324482 Color: 1
Size: 279273 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 396316 Color: 1
Size: 350963 Color: 1
Size: 252722 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 396593 Color: 1
Size: 325736 Color: 1
Size: 277672 Color: 0

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 396630 Color: 1
Size: 306974 Color: 1
Size: 296397 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 396631 Color: 1
Size: 307617 Color: 1
Size: 295753 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 396677 Color: 1
Size: 328805 Color: 1
Size: 274519 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 396744 Color: 1
Size: 322360 Color: 1
Size: 280897 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 396777 Color: 1
Size: 306918 Color: 0
Size: 296306 Color: 1

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 396985 Color: 1
Size: 336041 Color: 1
Size: 266975 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 397064 Color: 1
Size: 331041 Color: 1
Size: 271896 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 397065 Color: 1
Size: 351644 Color: 1
Size: 251292 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 397087 Color: 1
Size: 334602 Color: 1
Size: 268312 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 397120 Color: 1
Size: 317011 Color: 1
Size: 285870 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 397260 Color: 1
Size: 346693 Color: 1
Size: 256048 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 397266 Color: 1
Size: 348211 Color: 1
Size: 254524 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 397415 Color: 1
Size: 313176 Color: 1
Size: 289410 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 394895 Color: 1
Size: 322454 Color: 1
Size: 282652 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 397435 Color: 1
Size: 338033 Color: 1
Size: 264533 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 397462 Color: 1
Size: 308376 Color: 1
Size: 294163 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 397531 Color: 1
Size: 331729 Color: 1
Size: 270741 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 397540 Color: 1
Size: 331963 Color: 1
Size: 270498 Color: 0

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 397592 Color: 1
Size: 338921 Color: 1
Size: 263488 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 397912 Color: 1
Size: 325792 Color: 1
Size: 276297 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 397969 Color: 1
Size: 306974 Color: 1
Size: 295058 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 397998 Color: 1
Size: 330297 Color: 1
Size: 271706 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 398030 Color: 1
Size: 332594 Color: 1
Size: 269377 Color: 0

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 398034 Color: 1
Size: 332658 Color: 1
Size: 269309 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 375996 Color: 1
Size: 318545 Color: 1
Size: 305460 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 398474 Color: 1
Size: 310551 Color: 1
Size: 290976 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 398522 Color: 1
Size: 320351 Color: 1
Size: 281128 Color: 0

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 398573 Color: 1
Size: 325905 Color: 1
Size: 275523 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 398577 Color: 1
Size: 334246 Color: 1
Size: 267178 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 398634 Color: 1
Size: 324376 Color: 1
Size: 276991 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 398654 Color: 1
Size: 343732 Color: 1
Size: 257615 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 398655 Color: 1
Size: 327936 Color: 1
Size: 273410 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 398680 Color: 1
Size: 345460 Color: 1
Size: 255861 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 398683 Color: 1
Size: 330039 Color: 1
Size: 271279 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 398707 Color: 1
Size: 328409 Color: 1
Size: 272885 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 398802 Color: 1
Size: 310317 Color: 1
Size: 290882 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 398822 Color: 1
Size: 336300 Color: 1
Size: 264879 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 399039 Color: 1
Size: 334960 Color: 1
Size: 266002 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 399081 Color: 1
Size: 348774 Color: 1
Size: 252146 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 399184 Color: 1
Size: 331718 Color: 1
Size: 269099 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 399272 Color: 1
Size: 320142 Color: 1
Size: 280587 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 399386 Color: 1
Size: 324690 Color: 1
Size: 275925 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 399412 Color: 1
Size: 304218 Color: 1
Size: 296371 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 399417 Color: 1
Size: 314826 Color: 0
Size: 285758 Color: 1

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 399474 Color: 1
Size: 301797 Color: 0
Size: 298730 Color: 1

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 399509 Color: 1
Size: 332044 Color: 1
Size: 268448 Color: 0

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 399547 Color: 1
Size: 330412 Color: 1
Size: 270042 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 399943 Color: 1
Size: 333618 Color: 1
Size: 266440 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 400017 Color: 1
Size: 326161 Color: 1
Size: 273823 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 400092 Color: 1
Size: 339376 Color: 1
Size: 260533 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 400108 Color: 1
Size: 334540 Color: 1
Size: 265353 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 400357 Color: 1
Size: 334026 Color: 1
Size: 265618 Color: 0

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 400376 Color: 1
Size: 327672 Color: 1
Size: 271953 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 400503 Color: 1
Size: 301667 Color: 1
Size: 297831 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 400525 Color: 1
Size: 326960 Color: 1
Size: 272516 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 400726 Color: 1
Size: 337509 Color: 1
Size: 261766 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 400805 Color: 1
Size: 333968 Color: 1
Size: 265228 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 400811 Color: 1
Size: 326172 Color: 1
Size: 273018 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 400918 Color: 1
Size: 320319 Color: 1
Size: 278764 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 400942 Color: 1
Size: 328925 Color: 1
Size: 270134 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 400950 Color: 1
Size: 303102 Color: 0
Size: 295949 Color: 1

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 401036 Color: 1
Size: 342173 Color: 1
Size: 256792 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 401063 Color: 1
Size: 324083 Color: 1
Size: 274855 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 401091 Color: 1
Size: 333593 Color: 1
Size: 265317 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 401260 Color: 1
Size: 329393 Color: 1
Size: 269348 Color: 0

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 401279 Color: 1
Size: 330530 Color: 1
Size: 268192 Color: 0

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 401413 Color: 1
Size: 309312 Color: 1
Size: 289276 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 401546 Color: 1
Size: 320718 Color: 1
Size: 277737 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 401826 Color: 1
Size: 305190 Color: 1
Size: 292985 Color: 0

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 401905 Color: 1
Size: 317840 Color: 1
Size: 280256 Color: 0

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 401937 Color: 1
Size: 322850 Color: 1
Size: 275214 Color: 0

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 402012 Color: 1
Size: 308960 Color: 1
Size: 289029 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 402102 Color: 1
Size: 319693 Color: 1
Size: 278206 Color: 0

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 402189 Color: 1
Size: 320079 Color: 1
Size: 277733 Color: 0

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 402206 Color: 1
Size: 327188 Color: 1
Size: 270607 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 402247 Color: 1
Size: 324761 Color: 1
Size: 272993 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 402337 Color: 1
Size: 302912 Color: 1
Size: 294752 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 402461 Color: 1
Size: 310554 Color: 0
Size: 286986 Color: 1

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 402489 Color: 1
Size: 340046 Color: 1
Size: 257466 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 402663 Color: 1
Size: 321471 Color: 1
Size: 275867 Color: 0

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 359301 Color: 1
Size: 357042 Color: 1
Size: 283658 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 402789 Color: 1
Size: 310185 Color: 1
Size: 287027 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 402802 Color: 1
Size: 327752 Color: 1
Size: 269447 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 402803 Color: 1
Size: 321617 Color: 1
Size: 275581 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 402825 Color: 1
Size: 305351 Color: 1
Size: 291825 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 402837 Color: 1
Size: 329305 Color: 1
Size: 267859 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 403100 Color: 1
Size: 325588 Color: 1
Size: 271313 Color: 0

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 403226 Color: 1
Size: 331217 Color: 1
Size: 265558 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 403269 Color: 1
Size: 334712 Color: 1
Size: 262020 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 403282 Color: 1
Size: 315477 Color: 1
Size: 281242 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 403323 Color: 1
Size: 324441 Color: 1
Size: 272237 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 403410 Color: 1
Size: 323833 Color: 1
Size: 272758 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 403517 Color: 1
Size: 319284 Color: 1
Size: 277200 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 403538 Color: 1
Size: 312427 Color: 1
Size: 284036 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 403556 Color: 1
Size: 324495 Color: 1
Size: 271950 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 403622 Color: 1
Size: 313498 Color: 1
Size: 282881 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 403821 Color: 1
Size: 337675 Color: 1
Size: 258505 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 403910 Color: 1
Size: 329515 Color: 1
Size: 266576 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 404018 Color: 1
Size: 324301 Color: 1
Size: 271682 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 404052 Color: 1
Size: 325600 Color: 1
Size: 270349 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 404289 Color: 1
Size: 334055 Color: 1
Size: 261657 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 404427 Color: 1
Size: 342577 Color: 1
Size: 252997 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 404623 Color: 1
Size: 344358 Color: 1
Size: 251020 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 404687 Color: 1
Size: 324946 Color: 1
Size: 270368 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 404746 Color: 1
Size: 338037 Color: 1
Size: 257218 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 404752 Color: 1
Size: 328950 Color: 1
Size: 266299 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 404803 Color: 1
Size: 325029 Color: 0
Size: 270169 Color: 1

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 404823 Color: 1
Size: 333749 Color: 1
Size: 261429 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 404863 Color: 1
Size: 331083 Color: 1
Size: 264055 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 404964 Color: 1
Size: 304705 Color: 1
Size: 290332 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 405066 Color: 1
Size: 340967 Color: 1
Size: 253968 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 405236 Color: 1
Size: 306234 Color: 1
Size: 288531 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 405315 Color: 1
Size: 325086 Color: 1
Size: 269600 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 380447 Color: 1
Size: 313626 Color: 1
Size: 305928 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 405390 Color: 1
Size: 322999 Color: 1
Size: 271612 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 405398 Color: 1
Size: 309683 Color: 1
Size: 284920 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 405433 Color: 1
Size: 316736 Color: 1
Size: 277832 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 405585 Color: 1
Size: 322201 Color: 1
Size: 272215 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 405628 Color: 1
Size: 320560 Color: 1
Size: 273813 Color: 0

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 405675 Color: 1
Size: 321650 Color: 1
Size: 272676 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 405777 Color: 1
Size: 305706 Color: 1
Size: 288518 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 405843 Color: 1
Size: 323502 Color: 1
Size: 270656 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 405852 Color: 1
Size: 302169 Color: 0
Size: 291980 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 405915 Color: 1
Size: 316344 Color: 1
Size: 277742 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 405942 Color: 1
Size: 312051 Color: 0
Size: 282008 Color: 1

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 406279 Color: 1
Size: 331324 Color: 1
Size: 262398 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 406468 Color: 1
Size: 318782 Color: 1
Size: 274751 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 406658 Color: 1
Size: 329154 Color: 1
Size: 264189 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 406834 Color: 1
Size: 336184 Color: 1
Size: 256983 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 406835 Color: 1
Size: 311338 Color: 1
Size: 281828 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 407097 Color: 1
Size: 317996 Color: 1
Size: 274908 Color: 0

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 407143 Color: 1
Size: 324278 Color: 1
Size: 268580 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 407145 Color: 1
Size: 335839 Color: 1
Size: 257017 Color: 0

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 407220 Color: 1
Size: 330071 Color: 1
Size: 262710 Color: 0

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 407450 Color: 1
Size: 321632 Color: 1
Size: 270919 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 407474 Color: 1
Size: 318552 Color: 1
Size: 273975 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 407486 Color: 1
Size: 308575 Color: 1
Size: 283940 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 407497 Color: 1
Size: 326266 Color: 1
Size: 266238 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 407564 Color: 1
Size: 318526 Color: 1
Size: 273911 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 407569 Color: 1
Size: 305893 Color: 1
Size: 286539 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 407590 Color: 1
Size: 329338 Color: 1
Size: 263073 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 407729 Color: 1
Size: 315347 Color: 0
Size: 276925 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 407787 Color: 1
Size: 323156 Color: 1
Size: 269058 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 407926 Color: 1
Size: 312187 Color: 1
Size: 279888 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 407956 Color: 1
Size: 324116 Color: 1
Size: 267929 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 408031 Color: 1
Size: 319172 Color: 1
Size: 272798 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 408044 Color: 1
Size: 317473 Color: 1
Size: 274484 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 408046 Color: 1
Size: 322845 Color: 1
Size: 269110 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 408066 Color: 1
Size: 333637 Color: 1
Size: 258298 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 408130 Color: 1
Size: 320625 Color: 1
Size: 271246 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 408205 Color: 1
Size: 336794 Color: 1
Size: 255002 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 408461 Color: 1
Size: 324669 Color: 1
Size: 266871 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 408503 Color: 1
Size: 302652 Color: 1
Size: 288846 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 408560 Color: 1
Size: 321311 Color: 1
Size: 270130 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 408617 Color: 1
Size: 337607 Color: 1
Size: 253777 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 408695 Color: 1
Size: 340204 Color: 1
Size: 251102 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 408719 Color: 1
Size: 320572 Color: 1
Size: 270710 Color: 0

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 408732 Color: 1
Size: 335226 Color: 1
Size: 256043 Color: 0

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 408796 Color: 1
Size: 336777 Color: 1
Size: 254428 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 449138 Color: 1
Size: 297001 Color: 1
Size: 253862 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 408909 Color: 1
Size: 326963 Color: 1
Size: 264129 Color: 0

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 408998 Color: 1
Size: 340574 Color: 1
Size: 250429 Color: 0

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 409070 Color: 1
Size: 330451 Color: 1
Size: 260480 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 409321 Color: 1
Size: 335789 Color: 1
Size: 254891 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 409667 Color: 1
Size: 322728 Color: 1
Size: 267606 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 409669 Color: 1
Size: 315142 Color: 1
Size: 275190 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 409763 Color: 1
Size: 309078 Color: 1
Size: 281160 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 409767 Color: 1
Size: 333103 Color: 1
Size: 257131 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 409768 Color: 1
Size: 300968 Color: 1
Size: 289265 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 409859 Color: 1
Size: 324553 Color: 1
Size: 265589 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 409879 Color: 1
Size: 328457 Color: 1
Size: 261665 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 410270 Color: 1
Size: 310759 Color: 1
Size: 278972 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 410342 Color: 1
Size: 316093 Color: 1
Size: 273566 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 410467 Color: 1
Size: 306342 Color: 1
Size: 283192 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 410512 Color: 1
Size: 310058 Color: 1
Size: 279431 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 410598 Color: 1
Size: 322714 Color: 1
Size: 266689 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 410600 Color: 1
Size: 325546 Color: 1
Size: 263855 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 410639 Color: 1
Size: 319459 Color: 1
Size: 269903 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 410652 Color: 1
Size: 333484 Color: 1
Size: 255865 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 410689 Color: 1
Size: 328274 Color: 1
Size: 261038 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 410693 Color: 1
Size: 313166 Color: 1
Size: 276142 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 410741 Color: 1
Size: 327171 Color: 1
Size: 262089 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 410789 Color: 1
Size: 296461 Color: 1
Size: 292751 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 410934 Color: 1
Size: 333884 Color: 1
Size: 255183 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 410940 Color: 1
Size: 305314 Color: 1
Size: 283747 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 410962 Color: 1
Size: 327587 Color: 1
Size: 261452 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 410985 Color: 1
Size: 314055 Color: 1
Size: 274961 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 410996 Color: 1
Size: 335237 Color: 1
Size: 253768 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 411175 Color: 1
Size: 304335 Color: 1
Size: 284491 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 411288 Color: 1
Size: 330754 Color: 1
Size: 257959 Color: 0

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 411402 Color: 1
Size: 299237 Color: 0
Size: 289362 Color: 1

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 411454 Color: 1
Size: 327274 Color: 1
Size: 261273 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 411524 Color: 1
Size: 304764 Color: 1
Size: 283713 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 411541 Color: 1
Size: 327139 Color: 1
Size: 261321 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 411591 Color: 1
Size: 314821 Color: 1
Size: 273589 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 411614 Color: 1
Size: 326788 Color: 1
Size: 261599 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 411759 Color: 1
Size: 315753 Color: 1
Size: 272489 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 411802 Color: 1
Size: 313672 Color: 1
Size: 274527 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 411848 Color: 1
Size: 305314 Color: 1
Size: 282839 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 411866 Color: 1
Size: 319719 Color: 1
Size: 268416 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 411870 Color: 1
Size: 316446 Color: 1
Size: 271685 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 411946 Color: 1
Size: 295551 Color: 0
Size: 292504 Color: 1

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 411966 Color: 1
Size: 331847 Color: 1
Size: 256188 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 412395 Color: 1
Size: 305969 Color: 1
Size: 281637 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 412445 Color: 1
Size: 319619 Color: 1
Size: 267937 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 412475 Color: 1
Size: 298257 Color: 1
Size: 289269 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 412575 Color: 1
Size: 327411 Color: 1
Size: 260015 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 412688 Color: 1
Size: 297035 Color: 1
Size: 290278 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 412759 Color: 1
Size: 334948 Color: 1
Size: 252294 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 412785 Color: 1
Size: 330482 Color: 1
Size: 256734 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 412935 Color: 1
Size: 298499 Color: 1
Size: 288567 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 412995 Color: 1
Size: 331920 Color: 1
Size: 255086 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 413078 Color: 1
Size: 299256 Color: 1
Size: 287667 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 413100 Color: 1
Size: 306716 Color: 1
Size: 280185 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 413397 Color: 1
Size: 334998 Color: 1
Size: 251606 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 413551 Color: 1
Size: 326887 Color: 1
Size: 259563 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 413616 Color: 1
Size: 334360 Color: 1
Size: 252025 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 413679 Color: 1
Size: 314382 Color: 1
Size: 271940 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 413687 Color: 1
Size: 309718 Color: 1
Size: 276596 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 413766 Color: 1
Size: 323982 Color: 1
Size: 262253 Color: 0

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 413922 Color: 1
Size: 324595 Color: 1
Size: 261484 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 413966 Color: 1
Size: 315350 Color: 1
Size: 270685 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 414090 Color: 1
Size: 321218 Color: 1
Size: 264693 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 414145 Color: 1
Size: 325711 Color: 1
Size: 260145 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 414203 Color: 1
Size: 332667 Color: 1
Size: 253131 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 414262 Color: 1
Size: 320810 Color: 1
Size: 264929 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 414275 Color: 1
Size: 305751 Color: 1
Size: 279975 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 414373 Color: 1
Size: 320793 Color: 1
Size: 264835 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 414452 Color: 1
Size: 311962 Color: 1
Size: 273587 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 414472 Color: 1
Size: 322793 Color: 1
Size: 262736 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 414712 Color: 1
Size: 301582 Color: 1
Size: 283707 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 414760 Color: 1
Size: 331322 Color: 1
Size: 253919 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 414805 Color: 1
Size: 326930 Color: 1
Size: 258266 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 414964 Color: 1
Size: 314648 Color: 1
Size: 270389 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 415053 Color: 1
Size: 324713 Color: 1
Size: 260235 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 415109 Color: 1
Size: 329710 Color: 1
Size: 255182 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 415266 Color: 1
Size: 329186 Color: 1
Size: 255549 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 415456 Color: 1
Size: 309683 Color: 1
Size: 274862 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 416006 Color: 1
Size: 305555 Color: 1
Size: 278440 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 416107 Color: 1
Size: 309214 Color: 1
Size: 274680 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 416226 Color: 1
Size: 324348 Color: 1
Size: 259427 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 416237 Color: 1
Size: 300523 Color: 1
Size: 283241 Color: 0

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 416442 Color: 1
Size: 298719 Color: 0
Size: 284840 Color: 1

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 416569 Color: 1
Size: 309276 Color: 1
Size: 274156 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 416995 Color: 1
Size: 317735 Color: 1
Size: 265271 Color: 0

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 417060 Color: 1
Size: 323737 Color: 1
Size: 259204 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 417189 Color: 1
Size: 324171 Color: 1
Size: 258641 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 417411 Color: 1
Size: 297713 Color: 1
Size: 284877 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 417421 Color: 1
Size: 324097 Color: 1
Size: 258483 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 417452 Color: 1
Size: 317919 Color: 1
Size: 264630 Color: 0

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 417596 Color: 1
Size: 331652 Color: 1
Size: 250753 Color: 0

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 417629 Color: 1
Size: 322123 Color: 1
Size: 260249 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 417651 Color: 1
Size: 306542 Color: 1
Size: 275808 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 417674 Color: 1
Size: 308120 Color: 1
Size: 274207 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 417733 Color: 1
Size: 297722 Color: 1
Size: 284546 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 417767 Color: 1
Size: 305816 Color: 1
Size: 276418 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 417799 Color: 1
Size: 308689 Color: 1
Size: 273513 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 417824 Color: 1
Size: 307137 Color: 1
Size: 275040 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 417878 Color: 1
Size: 309773 Color: 1
Size: 272350 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 418019 Color: 1
Size: 331097 Color: 1
Size: 250885 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 418219 Color: 1
Size: 315677 Color: 1
Size: 266105 Color: 0

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 418320 Color: 1
Size: 306101 Color: 1
Size: 275580 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 418465 Color: 1
Size: 318753 Color: 1
Size: 262783 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 418484 Color: 1
Size: 326332 Color: 1
Size: 255185 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 418500 Color: 1
Size: 310423 Color: 1
Size: 271078 Color: 0

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 418606 Color: 1
Size: 323111 Color: 1
Size: 258284 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 418645 Color: 1
Size: 290783 Color: 1
Size: 290573 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 418689 Color: 1
Size: 307853 Color: 1
Size: 273459 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 418727 Color: 1
Size: 311825 Color: 1
Size: 269449 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 418760 Color: 1
Size: 329491 Color: 1
Size: 251750 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 418838 Color: 1
Size: 320052 Color: 1
Size: 261111 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 418971 Color: 1
Size: 312474 Color: 1
Size: 268556 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 419035 Color: 1
Size: 307772 Color: 1
Size: 273194 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 419077 Color: 1
Size: 318329 Color: 1
Size: 262595 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 419081 Color: 1
Size: 297051 Color: 1
Size: 283869 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 419104 Color: 1
Size: 311313 Color: 1
Size: 269584 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 419258 Color: 1
Size: 297910 Color: 1
Size: 282833 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 419300 Color: 1
Size: 313527 Color: 1
Size: 267174 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 419526 Color: 1
Size: 318751 Color: 1
Size: 261724 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 419864 Color: 1
Size: 325022 Color: 1
Size: 255115 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 419937 Color: 1
Size: 298361 Color: 1
Size: 281703 Color: 0

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 420025 Color: 1
Size: 321219 Color: 1
Size: 258757 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 420284 Color: 1
Size: 299046 Color: 1
Size: 280671 Color: 0

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 420290 Color: 1
Size: 321827 Color: 1
Size: 257884 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 420448 Color: 1
Size: 322125 Color: 1
Size: 257428 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 420501 Color: 1
Size: 319950 Color: 1
Size: 259550 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 420809 Color: 1
Size: 307444 Color: 1
Size: 271748 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 420832 Color: 1
Size: 328893 Color: 1
Size: 250276 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 421014 Color: 1
Size: 291518 Color: 1
Size: 287469 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 421140 Color: 1
Size: 321248 Color: 1
Size: 257613 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 421289 Color: 1
Size: 325696 Color: 1
Size: 253016 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 421382 Color: 1
Size: 322894 Color: 1
Size: 255725 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 421460 Color: 1
Size: 319928 Color: 1
Size: 258613 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 421709 Color: 1
Size: 327586 Color: 1
Size: 250706 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 421764 Color: 1
Size: 315106 Color: 1
Size: 263131 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 421777 Color: 1
Size: 302585 Color: 1
Size: 275639 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 421828 Color: 1
Size: 307241 Color: 1
Size: 270932 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 421980 Color: 1
Size: 307020 Color: 1
Size: 271001 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 422054 Color: 1
Size: 303410 Color: 1
Size: 274537 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 422178 Color: 1
Size: 290954 Color: 1
Size: 286869 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 422218 Color: 1
Size: 291620 Color: 0
Size: 286163 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 422476 Color: 1
Size: 321033 Color: 1
Size: 256492 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 422480 Color: 1
Size: 314445 Color: 1
Size: 263076 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 422656 Color: 1
Size: 318842 Color: 1
Size: 258503 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 422817 Color: 1
Size: 318954 Color: 1
Size: 258230 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 422879 Color: 1
Size: 290721 Color: 1
Size: 286401 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 423014 Color: 1
Size: 305941 Color: 1
Size: 271046 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 423023 Color: 1
Size: 311545 Color: 1
Size: 265433 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 423100 Color: 1
Size: 317593 Color: 1
Size: 259308 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 423427 Color: 1
Size: 319634 Color: 1
Size: 256940 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 423613 Color: 1
Size: 321116 Color: 1
Size: 255272 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 423677 Color: 1
Size: 297014 Color: 1
Size: 279310 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 423723 Color: 1
Size: 305288 Color: 1
Size: 270990 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 434748 Color: 1
Size: 302609 Color: 1
Size: 262644 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 423785 Color: 1
Size: 300754 Color: 1
Size: 275462 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 423945 Color: 1
Size: 298185 Color: 1
Size: 277871 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 424148 Color: 1
Size: 306885 Color: 1
Size: 268968 Color: 0

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 424335 Color: 1
Size: 324092 Color: 1
Size: 251574 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 424404 Color: 1
Size: 315148 Color: 1
Size: 260449 Color: 0

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 424578 Color: 1
Size: 293258 Color: 1
Size: 282165 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 424789 Color: 1
Size: 290334 Color: 1
Size: 284878 Color: 0

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 424942 Color: 1
Size: 300675 Color: 1
Size: 274384 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 424948 Color: 1
Size: 299171 Color: 1
Size: 275882 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 365068 Color: 1
Size: 338922 Color: 1
Size: 296011 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 425120 Color: 1
Size: 309851 Color: 1
Size: 265030 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 425138 Color: 1
Size: 299965 Color: 1
Size: 274898 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 425284 Color: 1
Size: 298261 Color: 1
Size: 276456 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 425388 Color: 1
Size: 313167 Color: 1
Size: 261446 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 425478 Color: 1
Size: 291632 Color: 0
Size: 282891 Color: 1

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 425584 Color: 1
Size: 301393 Color: 1
Size: 273024 Color: 0

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 425684 Color: 1
Size: 313116 Color: 1
Size: 261201 Color: 0

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 425850 Color: 1
Size: 319400 Color: 1
Size: 254751 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 425880 Color: 1
Size: 303242 Color: 1
Size: 270879 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 425896 Color: 1
Size: 308362 Color: 1
Size: 265743 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 425991 Color: 1
Size: 304787 Color: 1
Size: 269223 Color: 0

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 426024 Color: 1
Size: 319401 Color: 1
Size: 254576 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 426028 Color: 1
Size: 323637 Color: 1
Size: 250336 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 426124 Color: 1
Size: 290571 Color: 1
Size: 283306 Color: 0

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 426451 Color: 1
Size: 317784 Color: 1
Size: 255766 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 426722 Color: 1
Size: 294203 Color: 1
Size: 279076 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 426910 Color: 1
Size: 322314 Color: 1
Size: 250777 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 426985 Color: 1
Size: 315812 Color: 1
Size: 257204 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 427016 Color: 1
Size: 318898 Color: 1
Size: 254087 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 427390 Color: 1
Size: 317653 Color: 1
Size: 254958 Color: 0

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 427461 Color: 1
Size: 314413 Color: 1
Size: 258127 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 427471 Color: 1
Size: 309928 Color: 1
Size: 262602 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 427532 Color: 1
Size: 291648 Color: 1
Size: 280821 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 427790 Color: 1
Size: 318258 Color: 1
Size: 253953 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 427811 Color: 1
Size: 301272 Color: 1
Size: 270918 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 427869 Color: 1
Size: 314357 Color: 1
Size: 257775 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 427889 Color: 1
Size: 311161 Color: 1
Size: 260951 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 428126 Color: 1
Size: 314565 Color: 1
Size: 257310 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 428226 Color: 1
Size: 312847 Color: 1
Size: 258928 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 428349 Color: 1
Size: 314590 Color: 1
Size: 257062 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 428354 Color: 1
Size: 308810 Color: 1
Size: 262837 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 428439 Color: 1
Size: 305634 Color: 1
Size: 265928 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 428529 Color: 1
Size: 286645 Color: 1
Size: 284827 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 428727 Color: 1
Size: 320982 Color: 1
Size: 250292 Color: 0

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 428759 Color: 1
Size: 312292 Color: 1
Size: 258950 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 429078 Color: 1
Size: 294233 Color: 0
Size: 276690 Color: 1

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 429248 Color: 1
Size: 290662 Color: 0
Size: 280091 Color: 1

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 429523 Color: 1
Size: 307343 Color: 1
Size: 263135 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 429536 Color: 1
Size: 309844 Color: 1
Size: 260621 Color: 0

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 429688 Color: 1
Size: 303637 Color: 1
Size: 266676 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 429703 Color: 1
Size: 315729 Color: 1
Size: 254569 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 429739 Color: 1
Size: 313392 Color: 1
Size: 256870 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 429797 Color: 1
Size: 309057 Color: 1
Size: 261147 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 429802 Color: 1
Size: 308558 Color: 1
Size: 261641 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 429848 Color: 1
Size: 316879 Color: 1
Size: 253274 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 429953 Color: 1
Size: 291302 Color: 1
Size: 278746 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 429970 Color: 1
Size: 301601 Color: 1
Size: 268430 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 430029 Color: 1
Size: 285103 Color: 1
Size: 284869 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 350251 Color: 1
Size: 330860 Color: 1
Size: 318890 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 430212 Color: 1
Size: 290331 Color: 0
Size: 279458 Color: 1

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 430284 Color: 1
Size: 310057 Color: 1
Size: 259660 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 430384 Color: 1
Size: 302454 Color: 1
Size: 267163 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 430416 Color: 1
Size: 314341 Color: 1
Size: 255244 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 430417 Color: 1
Size: 290496 Color: 1
Size: 279088 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 430543 Color: 1
Size: 288367 Color: 0
Size: 281091 Color: 1

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 430543 Color: 1
Size: 285732 Color: 1
Size: 283726 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 430737 Color: 1
Size: 299477 Color: 1
Size: 269787 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 430751 Color: 1
Size: 305736 Color: 1
Size: 263514 Color: 0

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 430861 Color: 1
Size: 290575 Color: 0
Size: 278565 Color: 1

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 430974 Color: 1
Size: 297042 Color: 1
Size: 271985 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 431155 Color: 1
Size: 316148 Color: 1
Size: 252698 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 431244 Color: 1
Size: 314021 Color: 1
Size: 254736 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 431612 Color: 1
Size: 307069 Color: 1
Size: 261320 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 431628 Color: 1
Size: 316490 Color: 1
Size: 251883 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 431694 Color: 1
Size: 291104 Color: 1
Size: 277203 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 431896 Color: 1
Size: 317003 Color: 1
Size: 251102 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 432007 Color: 1
Size: 294326 Color: 1
Size: 273668 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 432094 Color: 1
Size: 313683 Color: 1
Size: 254224 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 432129 Color: 1
Size: 293944 Color: 1
Size: 273928 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 432131 Color: 1
Size: 310973 Color: 1
Size: 256897 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 432224 Color: 1
Size: 300706 Color: 1
Size: 267071 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 432257 Color: 1
Size: 315386 Color: 1
Size: 252358 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 432325 Color: 1
Size: 315190 Color: 1
Size: 252486 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 432355 Color: 1
Size: 308606 Color: 1
Size: 259040 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 432382 Color: 1
Size: 308938 Color: 1
Size: 258681 Color: 0

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 432508 Color: 1
Size: 286206 Color: 0
Size: 281287 Color: 1

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 432581 Color: 1
Size: 313179 Color: 1
Size: 254241 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 432634 Color: 1
Size: 312728 Color: 1
Size: 254639 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 432663 Color: 1
Size: 306335 Color: 1
Size: 261003 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 440685 Color: 1
Size: 304785 Color: 1
Size: 254531 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 432857 Color: 1
Size: 310209 Color: 1
Size: 256935 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 432877 Color: 1
Size: 309212 Color: 1
Size: 257912 Color: 0

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 432899 Color: 1
Size: 315848 Color: 1
Size: 251254 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 432960 Color: 1
Size: 306457 Color: 1
Size: 260584 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 433049 Color: 1
Size: 295833 Color: 1
Size: 271119 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 433089 Color: 1
Size: 301480 Color: 1
Size: 265432 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 433292 Color: 1
Size: 300022 Color: 1
Size: 266687 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 433453 Color: 1
Size: 297816 Color: 1
Size: 268732 Color: 0

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 433482 Color: 1
Size: 315102 Color: 1
Size: 251417 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 433596 Color: 1
Size: 306016 Color: 1
Size: 260389 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 433684 Color: 1
Size: 302168 Color: 1
Size: 264149 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 433724 Color: 1
Size: 310238 Color: 1
Size: 256039 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 433828 Color: 1
Size: 304318 Color: 1
Size: 261855 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 397963 Color: 1
Size: 335907 Color: 1
Size: 266131 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 434027 Color: 1
Size: 302091 Color: 1
Size: 263883 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 434389 Color: 1
Size: 311453 Color: 1
Size: 254159 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 434555 Color: 1
Size: 311008 Color: 1
Size: 254438 Color: 0

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 434914 Color: 1
Size: 306922 Color: 1
Size: 258165 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 434917 Color: 1
Size: 307354 Color: 1
Size: 257730 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 434936 Color: 1
Size: 301365 Color: 1
Size: 263700 Color: 0

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 435081 Color: 1
Size: 308271 Color: 1
Size: 256649 Color: 0

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 435166 Color: 1
Size: 306231 Color: 1
Size: 258604 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 435252 Color: 1
Size: 299727 Color: 1
Size: 265022 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 435313 Color: 1
Size: 288887 Color: 1
Size: 275801 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 435570 Color: 1
Size: 288723 Color: 1
Size: 275708 Color: 0

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 436233 Color: 1
Size: 302874 Color: 1
Size: 260894 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 436362 Color: 1
Size: 298011 Color: 1
Size: 265628 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 436403 Color: 1
Size: 297749 Color: 1
Size: 265849 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 436585 Color: 1
Size: 308172 Color: 1
Size: 255244 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 436621 Color: 1
Size: 298803 Color: 1
Size: 264577 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 436707 Color: 1
Size: 286840 Color: 0
Size: 276454 Color: 1

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 436831 Color: 1
Size: 298022 Color: 1
Size: 265148 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 436837 Color: 1
Size: 282969 Color: 0
Size: 280195 Color: 1

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 436861 Color: 1
Size: 303496 Color: 1
Size: 259644 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 436921 Color: 1
Size: 297326 Color: 1
Size: 265754 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 436959 Color: 1
Size: 300195 Color: 1
Size: 262847 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 437119 Color: 1
Size: 306122 Color: 1
Size: 256760 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 437138 Color: 1
Size: 285878 Color: 1
Size: 276985 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 437163 Color: 1
Size: 295318 Color: 1
Size: 267520 Color: 0

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 437217 Color: 1
Size: 294693 Color: 1
Size: 268091 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 437374 Color: 1
Size: 288653 Color: 1
Size: 273974 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 437436 Color: 1
Size: 307538 Color: 1
Size: 255027 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 437446 Color: 1
Size: 283033 Color: 0
Size: 279522 Color: 1

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 437482 Color: 1
Size: 310504 Color: 1
Size: 252015 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 437528 Color: 1
Size: 307411 Color: 1
Size: 255062 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 350660 Color: 1
Size: 335861 Color: 1
Size: 313480 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 437820 Color: 1
Size: 288680 Color: 1
Size: 273501 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 437939 Color: 1
Size: 287562 Color: 1
Size: 274500 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 438074 Color: 1
Size: 282374 Color: 0
Size: 279553 Color: 1

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 438097 Color: 1
Size: 303108 Color: 1
Size: 258796 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 438134 Color: 1
Size: 301538 Color: 1
Size: 260329 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 438153 Color: 1
Size: 304811 Color: 1
Size: 257037 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 438404 Color: 1
Size: 295527 Color: 1
Size: 266070 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 438713 Color: 1
Size: 307780 Color: 1
Size: 253508 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 438780 Color: 1
Size: 303071 Color: 1
Size: 258150 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 438817 Color: 1
Size: 295323 Color: 1
Size: 265861 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 438857 Color: 1
Size: 292780 Color: 1
Size: 268364 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 438860 Color: 1
Size: 306953 Color: 1
Size: 254188 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 438892 Color: 1
Size: 302088 Color: 1
Size: 259021 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 438936 Color: 1
Size: 299147 Color: 1
Size: 261918 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 438976 Color: 1
Size: 285036 Color: 0
Size: 275989 Color: 1

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 438988 Color: 1
Size: 302304 Color: 1
Size: 258709 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 439021 Color: 1
Size: 282860 Color: 1
Size: 278120 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 439034 Color: 1
Size: 303090 Color: 1
Size: 257877 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 439062 Color: 1
Size: 290112 Color: 1
Size: 270827 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 439127 Color: 1
Size: 294182 Color: 1
Size: 266692 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 439147 Color: 1
Size: 306750 Color: 1
Size: 254104 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 439183 Color: 1
Size: 299299 Color: 1
Size: 261519 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 439228 Color: 1
Size: 298667 Color: 1
Size: 262106 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 439443 Color: 1
Size: 300324 Color: 1
Size: 260234 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 439469 Color: 1
Size: 289963 Color: 1
Size: 270569 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 439515 Color: 1
Size: 309341 Color: 1
Size: 251145 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 439646 Color: 1
Size: 280684 Color: 1
Size: 279671 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 439702 Color: 1
Size: 294901 Color: 1
Size: 265398 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 439723 Color: 1
Size: 282631 Color: 1
Size: 277647 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 439811 Color: 1
Size: 299255 Color: 1
Size: 260935 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 439979 Color: 1
Size: 303797 Color: 1
Size: 256225 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 440324 Color: 1
Size: 301979 Color: 1
Size: 257698 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 440360 Color: 1
Size: 299609 Color: 1
Size: 260032 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 440438 Color: 1
Size: 299663 Color: 1
Size: 259900 Color: 0

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 440491 Color: 1
Size: 296698 Color: 1
Size: 262812 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 440547 Color: 1
Size: 291976 Color: 0
Size: 267478 Color: 1

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 440628 Color: 1
Size: 307691 Color: 1
Size: 251682 Color: 0

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 440771 Color: 1
Size: 306682 Color: 1
Size: 252548 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 440940 Color: 1
Size: 301419 Color: 1
Size: 257642 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 440945 Color: 1
Size: 299956 Color: 1
Size: 259100 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 441058 Color: 1
Size: 282893 Color: 0
Size: 276050 Color: 1

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 441150 Color: 1
Size: 302326 Color: 1
Size: 256525 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 441426 Color: 1
Size: 303022 Color: 1
Size: 255553 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 441588 Color: 1
Size: 306303 Color: 1
Size: 252110 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 441607 Color: 1
Size: 284961 Color: 1
Size: 273433 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 441651 Color: 1
Size: 304217 Color: 1
Size: 254133 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 441831 Color: 1
Size: 299233 Color: 1
Size: 258937 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 441905 Color: 1
Size: 297154 Color: 1
Size: 260942 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 441987 Color: 1
Size: 305710 Color: 1
Size: 252304 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 442311 Color: 1
Size: 305847 Color: 1
Size: 251843 Color: 0

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 442467 Color: 1
Size: 283074 Color: 0
Size: 274460 Color: 1

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 442470 Color: 1
Size: 292591 Color: 1
Size: 264940 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 443238 Color: 1
Size: 290829 Color: 0
Size: 265934 Color: 1

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 443259 Color: 1
Size: 297517 Color: 1
Size: 259225 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 443434 Color: 1
Size: 283601 Color: 0
Size: 272966 Color: 1

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 443459 Color: 1
Size: 292155 Color: 1
Size: 264387 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 443737 Color: 1
Size: 285782 Color: 1
Size: 270482 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 443754 Color: 1
Size: 278670 Color: 1
Size: 277577 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 443831 Color: 1
Size: 298352 Color: 1
Size: 257818 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 443891 Color: 1
Size: 295831 Color: 1
Size: 260279 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 444008 Color: 1
Size: 292763 Color: 1
Size: 263230 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 444028 Color: 1
Size: 287231 Color: 1
Size: 268742 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 444095 Color: 1
Size: 287704 Color: 1
Size: 268202 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 444210 Color: 1
Size: 280887 Color: 0
Size: 274904 Color: 1

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 444220 Color: 1
Size: 297459 Color: 1
Size: 258322 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 444519 Color: 1
Size: 299069 Color: 1
Size: 256413 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 444584 Color: 1
Size: 297235 Color: 1
Size: 258182 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 444592 Color: 1
Size: 300291 Color: 1
Size: 255118 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 444692 Color: 1
Size: 295618 Color: 1
Size: 259691 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 444701 Color: 1
Size: 296686 Color: 1
Size: 258614 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 444753 Color: 1
Size: 296469 Color: 1
Size: 258779 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 444960 Color: 1
Size: 292506 Color: 1
Size: 262535 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 445071 Color: 1
Size: 299997 Color: 1
Size: 254933 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 445123 Color: 1
Size: 297853 Color: 1
Size: 257025 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 445131 Color: 1
Size: 299173 Color: 1
Size: 255697 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 445186 Color: 1
Size: 301168 Color: 1
Size: 253647 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 445278 Color: 1
Size: 298629 Color: 1
Size: 256094 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 445369 Color: 1
Size: 280237 Color: 1
Size: 274395 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 445815 Color: 1
Size: 300951 Color: 1
Size: 253235 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 445958 Color: 1
Size: 296212 Color: 1
Size: 257831 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 446008 Color: 1
Size: 299104 Color: 1
Size: 254889 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 446348 Color: 1
Size: 295502 Color: 1
Size: 258151 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 446472 Color: 1
Size: 277376 Color: 0
Size: 276153 Color: 1

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 446505 Color: 1
Size: 292918 Color: 1
Size: 260578 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 446556 Color: 1
Size: 289442 Color: 1
Size: 264003 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 446605 Color: 1
Size: 287560 Color: 1
Size: 265836 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 446640 Color: 1
Size: 291182 Color: 1
Size: 262179 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 446655 Color: 1
Size: 282062 Color: 1
Size: 271284 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 446682 Color: 1
Size: 281175 Color: 1
Size: 272144 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 446709 Color: 1
Size: 298888 Color: 1
Size: 254404 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 446752 Color: 1
Size: 284739 Color: 1
Size: 268510 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 447082 Color: 1
Size: 302185 Color: 1
Size: 250734 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 447166 Color: 1
Size: 287589 Color: 0
Size: 265246 Color: 1

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 447191 Color: 1
Size: 282830 Color: 1
Size: 269980 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 447192 Color: 1
Size: 278046 Color: 1
Size: 274763 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 447201 Color: 1
Size: 295393 Color: 1
Size: 257407 Color: 0

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 447250 Color: 1
Size: 295806 Color: 1
Size: 256945 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 359348 Color: 1
Size: 340352 Color: 1
Size: 300301 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 447353 Color: 1
Size: 280804 Color: 1
Size: 271844 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 447399 Color: 1
Size: 297554 Color: 1
Size: 255048 Color: 0

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 376639 Color: 1
Size: 320353 Color: 1
Size: 303009 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 447872 Color: 1
Size: 295815 Color: 1
Size: 256314 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 447913 Color: 1
Size: 298962 Color: 1
Size: 253126 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 447926 Color: 1
Size: 293294 Color: 1
Size: 258781 Color: 0

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 448094 Color: 1
Size: 283589 Color: 1
Size: 268318 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 448189 Color: 1
Size: 291192 Color: 1
Size: 260620 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 448292 Color: 1
Size: 294945 Color: 1
Size: 256764 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 448300 Color: 1
Size: 298210 Color: 1
Size: 253491 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 448486 Color: 1
Size: 291317 Color: 1
Size: 260198 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 448663 Color: 1
Size: 292305 Color: 1
Size: 259033 Color: 0

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 448713 Color: 1
Size: 295527 Color: 1
Size: 255761 Color: 0

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 358369 Color: 1
Size: 341790 Color: 1
Size: 299842 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 448816 Color: 1
Size: 294968 Color: 1
Size: 256217 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 448869 Color: 1
Size: 283016 Color: 1
Size: 268116 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 449110 Color: 1
Size: 297897 Color: 1
Size: 252994 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 449144 Color: 1
Size: 298892 Color: 1
Size: 251965 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 449209 Color: 1
Size: 294529 Color: 1
Size: 256263 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 449290 Color: 1
Size: 284009 Color: 0
Size: 266702 Color: 1

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 449332 Color: 1
Size: 289637 Color: 1
Size: 261032 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 449345 Color: 1
Size: 300086 Color: 1
Size: 250570 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 449425 Color: 1
Size: 298865 Color: 1
Size: 251711 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 449444 Color: 1
Size: 299206 Color: 1
Size: 251351 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 449498 Color: 1
Size: 286780 Color: 1
Size: 263723 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 449519 Color: 1
Size: 291598 Color: 1
Size: 258884 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 449528 Color: 1
Size: 291138 Color: 1
Size: 259335 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 449536 Color: 1
Size: 289242 Color: 1
Size: 261223 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 449630 Color: 1
Size: 291416 Color: 1
Size: 258955 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 449798 Color: 1
Size: 288874 Color: 1
Size: 261329 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 449809 Color: 1
Size: 294163 Color: 1
Size: 256029 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 449908 Color: 1
Size: 291243 Color: 1
Size: 258850 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 449909 Color: 1
Size: 296369 Color: 1
Size: 253723 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 449991 Color: 1
Size: 293251 Color: 1
Size: 256759 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 450094 Color: 1
Size: 292405 Color: 1
Size: 257502 Color: 0

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 450407 Color: 1
Size: 292834 Color: 1
Size: 256760 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 450417 Color: 1
Size: 295101 Color: 1
Size: 254483 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 450457 Color: 1
Size: 278536 Color: 1
Size: 271008 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 450638 Color: 1
Size: 291263 Color: 1
Size: 258100 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 450701 Color: 1
Size: 296112 Color: 1
Size: 253188 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 450711 Color: 1
Size: 296223 Color: 1
Size: 253067 Color: 0

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 450732 Color: 1
Size: 278870 Color: 1
Size: 270399 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 450890 Color: 1
Size: 296872 Color: 1
Size: 252239 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 451022 Color: 1
Size: 289447 Color: 1
Size: 259532 Color: 0

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 451092 Color: 1
Size: 298256 Color: 1
Size: 250653 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 451098 Color: 1
Size: 296302 Color: 1
Size: 252601 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 451126 Color: 1
Size: 288894 Color: 1
Size: 259981 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 451146 Color: 1
Size: 290200 Color: 1
Size: 258655 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 451148 Color: 1
Size: 281042 Color: 1
Size: 267811 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 451379 Color: 1
Size: 292326 Color: 1
Size: 256296 Color: 0

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 451663 Color: 1
Size: 281023 Color: 1
Size: 267315 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 451806 Color: 1
Size: 285672 Color: 1
Size: 262523 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 451996 Color: 1
Size: 289961 Color: 1
Size: 258044 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 452164 Color: 1
Size: 288991 Color: 1
Size: 258846 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 452365 Color: 1
Size: 279446 Color: 0
Size: 268190 Color: 1

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 452383 Color: 1
Size: 294324 Color: 1
Size: 253294 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 452529 Color: 1
Size: 276040 Color: 0
Size: 271432 Color: 1

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 452728 Color: 1
Size: 294383 Color: 1
Size: 252890 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 452746 Color: 1
Size: 289230 Color: 1
Size: 258025 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 453027 Color: 1
Size: 290308 Color: 1
Size: 256666 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 453788 Color: 1
Size: 280644 Color: 1
Size: 265569 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 453808 Color: 1
Size: 282059 Color: 1
Size: 264134 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 453962 Color: 1
Size: 286176 Color: 1
Size: 259863 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 454019 Color: 1
Size: 293956 Color: 1
Size: 252026 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 454042 Color: 1
Size: 289821 Color: 1
Size: 256138 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 454044 Color: 1
Size: 277867 Color: 1
Size: 268090 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 454601 Color: 1
Size: 292472 Color: 1
Size: 252928 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 454804 Color: 1
Size: 293451 Color: 1
Size: 251746 Color: 0

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 455089 Color: 1
Size: 288320 Color: 1
Size: 256592 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 455129 Color: 1
Size: 285407 Color: 1
Size: 259465 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 455289 Color: 1
Size: 290146 Color: 1
Size: 254566 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 455394 Color: 1
Size: 283102 Color: 1
Size: 261505 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 455772 Color: 1
Size: 282325 Color: 1
Size: 261904 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 455981 Color: 1
Size: 290549 Color: 1
Size: 253471 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 456017 Color: 1
Size: 291953 Color: 1
Size: 252031 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 456153 Color: 1
Size: 278863 Color: 0
Size: 264985 Color: 1

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 456189 Color: 1
Size: 290966 Color: 1
Size: 252846 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 456214 Color: 1
Size: 280973 Color: 1
Size: 262814 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 456652 Color: 1
Size: 289946 Color: 1
Size: 253403 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 456720 Color: 1
Size: 285808 Color: 1
Size: 257473 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 456723 Color: 1
Size: 286917 Color: 1
Size: 256361 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 456939 Color: 1
Size: 278047 Color: 1
Size: 265015 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 456989 Color: 1
Size: 289641 Color: 1
Size: 253371 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 457033 Color: 1
Size: 280804 Color: 1
Size: 262164 Color: 0

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 457142 Color: 1
Size: 285344 Color: 1
Size: 257515 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 457221 Color: 1
Size: 286297 Color: 1
Size: 256483 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 457342 Color: 1
Size: 292618 Color: 1
Size: 250041 Color: 0

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 457418 Color: 1
Size: 288494 Color: 1
Size: 254089 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 457418 Color: 1
Size: 281223 Color: 1
Size: 261360 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 457528 Color: 1
Size: 288332 Color: 1
Size: 254141 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 457541 Color: 1
Size: 283245 Color: 1
Size: 259215 Color: 0

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 457588 Color: 1
Size: 281686 Color: 1
Size: 260727 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 457783 Color: 1
Size: 280581 Color: 1
Size: 261637 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 457920 Color: 1
Size: 290614 Color: 1
Size: 251467 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 457962 Color: 1
Size: 284362 Color: 1
Size: 257677 Color: 0

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 458087 Color: 1
Size: 288409 Color: 1
Size: 253505 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 458108 Color: 1
Size: 279756 Color: 1
Size: 262137 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 458485 Color: 1
Size: 281976 Color: 1
Size: 259540 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 458519 Color: 1
Size: 280949 Color: 1
Size: 260533 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 458540 Color: 1
Size: 285820 Color: 1
Size: 255641 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 458802 Color: 1
Size: 288276 Color: 1
Size: 252923 Color: 0

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 458855 Color: 1
Size: 279089 Color: 1
Size: 262057 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 458997 Color: 1
Size: 276211 Color: 1
Size: 264793 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 459298 Color: 1
Size: 281514 Color: 1
Size: 259189 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 459314 Color: 1
Size: 280181 Color: 0
Size: 260506 Color: 1

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 459343 Color: 1
Size: 289232 Color: 1
Size: 251426 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 459401 Color: 1
Size: 273061 Color: 0
Size: 267539 Color: 1

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 391280 Color: 1
Size: 358240 Color: 1
Size: 250481 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 459498 Color: 1
Size: 275630 Color: 1
Size: 264873 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 459507 Color: 1
Size: 273129 Color: 1
Size: 267365 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 459537 Color: 1
Size: 274500 Color: 1
Size: 265964 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 459539 Color: 1
Size: 288176 Color: 1
Size: 252286 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 459682 Color: 1
Size: 272458 Color: 1
Size: 267861 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 459812 Color: 1
Size: 285157 Color: 1
Size: 255032 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 459946 Color: 1
Size: 289558 Color: 1
Size: 250497 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 353842 Color: 1
Size: 328327 Color: 0
Size: 317832 Color: 1

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 460083 Color: 1
Size: 286610 Color: 1
Size: 253308 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 460215 Color: 1
Size: 277243 Color: 1
Size: 262543 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 460266 Color: 1
Size: 284624 Color: 1
Size: 255111 Color: 0

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 460279 Color: 1
Size: 282311 Color: 1
Size: 257411 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 460469 Color: 1
Size: 287925 Color: 1
Size: 251607 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 460675 Color: 1
Size: 279809 Color: 1
Size: 259517 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 461020 Color: 1
Size: 278086 Color: 1
Size: 260895 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 461148 Color: 1
Size: 278455 Color: 1
Size: 260398 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 461212 Color: 1
Size: 287071 Color: 1
Size: 251718 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 461249 Color: 1
Size: 282614 Color: 1
Size: 256138 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 461465 Color: 1
Size: 270977 Color: 0
Size: 267559 Color: 1

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 461476 Color: 1
Size: 283716 Color: 1
Size: 254809 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 461515 Color: 1
Size: 284409 Color: 1
Size: 254077 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 461526 Color: 1
Size: 277309 Color: 1
Size: 261166 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 461533 Color: 1
Size: 274283 Color: 1
Size: 264185 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 461552 Color: 1
Size: 269595 Color: 0
Size: 268854 Color: 1

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 461589 Color: 1
Size: 284512 Color: 1
Size: 253900 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 461590 Color: 1
Size: 284213 Color: 1
Size: 254198 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 461761 Color: 1
Size: 287031 Color: 1
Size: 251209 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 461870 Color: 1
Size: 271689 Color: 1
Size: 266442 Color: 0

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 462042 Color: 1
Size: 276379 Color: 1
Size: 261580 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 462255 Color: 1
Size: 284079 Color: 1
Size: 253667 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 462468 Color: 1
Size: 278347 Color: 1
Size: 259186 Color: 0

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 462504 Color: 1
Size: 284182 Color: 1
Size: 253315 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 462769 Color: 1
Size: 284145 Color: 1
Size: 253087 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 462908 Color: 1
Size: 285833 Color: 1
Size: 251260 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 462969 Color: 1
Size: 275000 Color: 1
Size: 262032 Color: 0

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 463098 Color: 1
Size: 271395 Color: 1
Size: 265508 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 463181 Color: 1
Size: 280401 Color: 1
Size: 256419 Color: 0

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 463249 Color: 1
Size: 272544 Color: 1
Size: 264208 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 463307 Color: 1
Size: 275261 Color: 1
Size: 261433 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 463441 Color: 1
Size: 285078 Color: 1
Size: 251482 Color: 0

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 463506 Color: 1
Size: 279366 Color: 1
Size: 257129 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 463522 Color: 1
Size: 282083 Color: 1
Size: 254396 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 463637 Color: 1
Size: 282776 Color: 1
Size: 253588 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 463680 Color: 1
Size: 280640 Color: 1
Size: 255681 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 463851 Color: 1
Size: 285398 Color: 1
Size: 250752 Color: 0

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 463935 Color: 1
Size: 276490 Color: 1
Size: 259576 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 464056 Color: 1
Size: 280681 Color: 1
Size: 255264 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 464206 Color: 1
Size: 270260 Color: 1
Size: 265535 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 464285 Color: 1
Size: 284315 Color: 1
Size: 251401 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 464321 Color: 1
Size: 270326 Color: 1
Size: 265354 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 464433 Color: 1
Size: 270916 Color: 1
Size: 264652 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 464764 Color: 1
Size: 275377 Color: 1
Size: 259860 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 464845 Color: 1
Size: 279943 Color: 1
Size: 255213 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 464848 Color: 1
Size: 269867 Color: 1
Size: 265286 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 464915 Color: 1
Size: 273121 Color: 1
Size: 261965 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 443104 Color: 1
Size: 301329 Color: 1
Size: 255568 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 465074 Color: 1
Size: 284341 Color: 1
Size: 250586 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 465251 Color: 1
Size: 281445 Color: 1
Size: 253305 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 465371 Color: 1
Size: 271474 Color: 1
Size: 263156 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 465375 Color: 1
Size: 275353 Color: 1
Size: 259273 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 465387 Color: 1
Size: 272793 Color: 1
Size: 261821 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 465554 Color: 1
Size: 271484 Color: 1
Size: 262963 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 465633 Color: 1
Size: 271663 Color: 1
Size: 262705 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 465794 Color: 1
Size: 277543 Color: 1
Size: 256664 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 466037 Color: 1
Size: 278604 Color: 1
Size: 255360 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 466074 Color: 1
Size: 279009 Color: 1
Size: 254918 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 466101 Color: 1
Size: 272364 Color: 1
Size: 261536 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 466247 Color: 1
Size: 274057 Color: 1
Size: 259697 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 466262 Color: 1
Size: 281186 Color: 1
Size: 252553 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 466329 Color: 1
Size: 283249 Color: 1
Size: 250423 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 466333 Color: 1
Size: 276953 Color: 1
Size: 256715 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 466555 Color: 1
Size: 277155 Color: 1
Size: 256291 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 466573 Color: 1
Size: 282475 Color: 1
Size: 250953 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 467017 Color: 1
Size: 280352 Color: 1
Size: 252632 Color: 0

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 467131 Color: 1
Size: 279767 Color: 1
Size: 253103 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 467337 Color: 1
Size: 268527 Color: 1
Size: 264137 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 467392 Color: 1
Size: 274683 Color: 1
Size: 257926 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 467587 Color: 1
Size: 281848 Color: 1
Size: 250566 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 467766 Color: 1
Size: 274142 Color: 1
Size: 258093 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 468039 Color: 1
Size: 272285 Color: 1
Size: 259677 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 468129 Color: 1
Size: 281449 Color: 1
Size: 250423 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 468230 Color: 1
Size: 266176 Color: 1
Size: 265595 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 468284 Color: 1
Size: 267059 Color: 1
Size: 264658 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 468441 Color: 1
Size: 271389 Color: 0
Size: 260171 Color: 1

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 468537 Color: 1
Size: 270124 Color: 0
Size: 261340 Color: 1

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 468539 Color: 1
Size: 280496 Color: 1
Size: 250966 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 468754 Color: 1
Size: 278884 Color: 1
Size: 252363 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 468898 Color: 1
Size: 271897 Color: 0
Size: 259206 Color: 1

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 468955 Color: 1
Size: 280185 Color: 1
Size: 250861 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 469101 Color: 1
Size: 276913 Color: 1
Size: 253987 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 469333 Color: 1
Size: 274045 Color: 1
Size: 256623 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 469587 Color: 1
Size: 277767 Color: 1
Size: 252647 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 469760 Color: 1
Size: 274143 Color: 1
Size: 256098 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 469852 Color: 1
Size: 276319 Color: 1
Size: 253830 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 470080 Color: 1
Size: 279285 Color: 1
Size: 250636 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 470308 Color: 1
Size: 277944 Color: 1
Size: 251749 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 470403 Color: 1
Size: 274650 Color: 1
Size: 254948 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 470491 Color: 1
Size: 277693 Color: 1
Size: 251817 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 470628 Color: 1
Size: 271611 Color: 1
Size: 257762 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 470753 Color: 1
Size: 270763 Color: 1
Size: 258485 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 470823 Color: 1
Size: 274875 Color: 1
Size: 254303 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 470837 Color: 1
Size: 276106 Color: 1
Size: 253058 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 471250 Color: 1
Size: 268712 Color: 0
Size: 260039 Color: 1

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 471281 Color: 1
Size: 273966 Color: 1
Size: 254754 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 471297 Color: 1
Size: 272888 Color: 1
Size: 255816 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 471327 Color: 1
Size: 276730 Color: 1
Size: 251944 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 471613 Color: 1
Size: 264779 Color: 1
Size: 263609 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 471634 Color: 1
Size: 265795 Color: 1
Size: 262572 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 471709 Color: 1
Size: 268354 Color: 1
Size: 259938 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 471727 Color: 1
Size: 268839 Color: 1
Size: 259435 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 472000 Color: 1
Size: 264996 Color: 1
Size: 263005 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 472145 Color: 1
Size: 269630 Color: 1
Size: 258226 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 472516 Color: 1
Size: 274641 Color: 1
Size: 252844 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 472663 Color: 1
Size: 268613 Color: 1
Size: 258725 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 473240 Color: 1
Size: 270167 Color: 1
Size: 256594 Color: 0

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 473371 Color: 1
Size: 273737 Color: 1
Size: 252893 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 473471 Color: 1
Size: 268190 Color: 0
Size: 258340 Color: 1

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 473511 Color: 1
Size: 270375 Color: 1
Size: 256115 Color: 0

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 473679 Color: 1
Size: 276136 Color: 1
Size: 250186 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 474046 Color: 1
Size: 275288 Color: 1
Size: 250667 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 474184 Color: 1
Size: 271973 Color: 1
Size: 253844 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 474423 Color: 1
Size: 273752 Color: 1
Size: 251826 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 474517 Color: 1
Size: 274066 Color: 1
Size: 251418 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 474519 Color: 1
Size: 269225 Color: 1
Size: 256257 Color: 0

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 474549 Color: 1
Size: 274418 Color: 1
Size: 251034 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 474584 Color: 1
Size: 266882 Color: 1
Size: 258535 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 474989 Color: 1
Size: 270468 Color: 1
Size: 254544 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 475138 Color: 1
Size: 265597 Color: 1
Size: 259266 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 475174 Color: 1
Size: 263031 Color: 1
Size: 261796 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 475227 Color: 1
Size: 271204 Color: 1
Size: 253570 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 475350 Color: 1
Size: 270337 Color: 1
Size: 254314 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 475436 Color: 1
Size: 269139 Color: 1
Size: 255426 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 475542 Color: 1
Size: 272683 Color: 1
Size: 251776 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 475593 Color: 1
Size: 272174 Color: 1
Size: 252234 Color: 0

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 475610 Color: 1
Size: 265243 Color: 0
Size: 259148 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 475688 Color: 1
Size: 270676 Color: 1
Size: 253637 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 475977 Color: 1
Size: 273145 Color: 1
Size: 250879 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 475985 Color: 1
Size: 268540 Color: 1
Size: 255476 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 475990 Color: 1
Size: 263740 Color: 1
Size: 260271 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 476022 Color: 1
Size: 272518 Color: 1
Size: 251461 Color: 0

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 476149 Color: 1
Size: 268653 Color: 1
Size: 255199 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 476257 Color: 1
Size: 268696 Color: 1
Size: 255048 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 476607 Color: 1
Size: 270300 Color: 1
Size: 253094 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 476611 Color: 1
Size: 271950 Color: 1
Size: 251440 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 476622 Color: 1
Size: 271676 Color: 1
Size: 251703 Color: 0

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 476908 Color: 1
Size: 267407 Color: 1
Size: 255686 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 476945 Color: 1
Size: 272891 Color: 1
Size: 250165 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 477233 Color: 1
Size: 268851 Color: 1
Size: 253917 Color: 0

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 477616 Color: 1
Size: 267471 Color: 1
Size: 254914 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 477685 Color: 1
Size: 263651 Color: 1
Size: 258665 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 477783 Color: 1
Size: 267380 Color: 1
Size: 254838 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 477807 Color: 1
Size: 266498 Color: 1
Size: 255696 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 478131 Color: 1
Size: 269862 Color: 1
Size: 252008 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 478143 Color: 1
Size: 269455 Color: 1
Size: 252403 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 478445 Color: 1
Size: 270043 Color: 1
Size: 251513 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 478747 Color: 1
Size: 260942 Color: 1
Size: 260312 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 478769 Color: 1
Size: 261770 Color: 1
Size: 259462 Color: 0

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 479362 Color: 1
Size: 269376 Color: 1
Size: 251263 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 479648 Color: 1
Size: 260505 Color: 1
Size: 259848 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 479828 Color: 1
Size: 264414 Color: 1
Size: 255759 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 479963 Color: 1
Size: 267923 Color: 1
Size: 252115 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 480218 Color: 1
Size: 265993 Color: 1
Size: 253790 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 480274 Color: 1
Size: 266289 Color: 1
Size: 253438 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 480296 Color: 1
Size: 264303 Color: 1
Size: 255402 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 480302 Color: 1
Size: 262888 Color: 1
Size: 256811 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 480331 Color: 1
Size: 260345 Color: 0
Size: 259325 Color: 1

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 480597 Color: 1
Size: 268774 Color: 1
Size: 250630 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 480598 Color: 1
Size: 260771 Color: 0
Size: 258632 Color: 1

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 480618 Color: 1
Size: 260484 Color: 0
Size: 258899 Color: 1

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 480642 Color: 1
Size: 268544 Color: 1
Size: 250815 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 480663 Color: 1
Size: 262010 Color: 1
Size: 257328 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 480823 Color: 1
Size: 265565 Color: 1
Size: 253613 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 480852 Color: 1
Size: 262598 Color: 1
Size: 256551 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 480877 Color: 1
Size: 269097 Color: 1
Size: 250027 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 481094 Color: 1
Size: 260961 Color: 0
Size: 257946 Color: 1

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 481102 Color: 1
Size: 263861 Color: 1
Size: 255038 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 481147 Color: 1
Size: 261009 Color: 1
Size: 257845 Color: 0

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 481509 Color: 1
Size: 262691 Color: 1
Size: 255801 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 481615 Color: 1
Size: 259288 Color: 1
Size: 259098 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 481668 Color: 1
Size: 266426 Color: 1
Size: 251907 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 481737 Color: 1
Size: 263368 Color: 1
Size: 254896 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 482093 Color: 1
Size: 267383 Color: 1
Size: 250525 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 482200 Color: 1
Size: 263473 Color: 1
Size: 254328 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 482286 Color: 1
Size: 266933 Color: 1
Size: 250782 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 482319 Color: 1
Size: 266924 Color: 1
Size: 250758 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 482351 Color: 1
Size: 265275 Color: 1
Size: 252375 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 482794 Color: 1
Size: 267056 Color: 1
Size: 250151 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 482826 Color: 1
Size: 266591 Color: 1
Size: 250584 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 483034 Color: 1
Size: 260031 Color: 1
Size: 256936 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 483138 Color: 1
Size: 264927 Color: 1
Size: 251936 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 483178 Color: 1
Size: 266784 Color: 1
Size: 250039 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 483219 Color: 1
Size: 264050 Color: 1
Size: 252732 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 483241 Color: 1
Size: 263987 Color: 1
Size: 252773 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 483363 Color: 1
Size: 265762 Color: 1
Size: 250876 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 483699 Color: 1
Size: 264311 Color: 1
Size: 251991 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 483758 Color: 1
Size: 262716 Color: 1
Size: 253527 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 483928 Color: 1
Size: 263376 Color: 1
Size: 252697 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 484173 Color: 1
Size: 264605 Color: 1
Size: 251223 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 484370 Color: 1
Size: 263431 Color: 1
Size: 252200 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 484425 Color: 1
Size: 264015 Color: 1
Size: 251561 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 484598 Color: 1
Size: 263245 Color: 1
Size: 252158 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 484628 Color: 1
Size: 263102 Color: 1
Size: 252271 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 484668 Color: 1
Size: 264108 Color: 1
Size: 251225 Color: 0

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 484677 Color: 1
Size: 263257 Color: 1
Size: 252067 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 484735 Color: 1
Size: 261767 Color: 1
Size: 253499 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 484777 Color: 1
Size: 260671 Color: 1
Size: 254553 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 484989 Color: 1
Size: 262871 Color: 1
Size: 252141 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 485045 Color: 1
Size: 258749 Color: 0
Size: 256207 Color: 1

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 485059 Color: 1
Size: 257988 Color: 1
Size: 256954 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 352966 Color: 1
Size: 352098 Color: 1
Size: 294937 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 485097 Color: 1
Size: 260368 Color: 1
Size: 254536 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 485238 Color: 1
Size: 261145 Color: 1
Size: 253618 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 485583 Color: 1
Size: 260679 Color: 1
Size: 253739 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 485655 Color: 1
Size: 261763 Color: 1
Size: 252583 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 485836 Color: 1
Size: 257778 Color: 1
Size: 256387 Color: 0

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 485936 Color: 1
Size: 259036 Color: 0
Size: 255029 Color: 1

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 486215 Color: 1
Size: 261637 Color: 1
Size: 252149 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 486433 Color: 1
Size: 260886 Color: 1
Size: 252682 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 486550 Color: 1
Size: 261265 Color: 1
Size: 252186 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 486633 Color: 1
Size: 257332 Color: 0
Size: 256036 Color: 1

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 486818 Color: 1
Size: 262087 Color: 1
Size: 251096 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 486890 Color: 1
Size: 261906 Color: 1
Size: 251205 Color: 0

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 486952 Color: 1
Size: 259312 Color: 1
Size: 253737 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 487023 Color: 1
Size: 257507 Color: 1
Size: 255471 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 487136 Color: 1
Size: 259555 Color: 1
Size: 253310 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 487627 Color: 1
Size: 260305 Color: 1
Size: 252069 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 449012 Color: 1
Size: 290810 Color: 1
Size: 260179 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 488592 Color: 1
Size: 260942 Color: 1
Size: 250467 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 488872 Color: 1
Size: 260280 Color: 1
Size: 250849 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 489241 Color: 1
Size: 258722 Color: 1
Size: 252038 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 489321 Color: 1
Size: 259727 Color: 1
Size: 250953 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 489502 Color: 1
Size: 258494 Color: 1
Size: 252005 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 489697 Color: 1
Size: 255469 Color: 0
Size: 254835 Color: 1

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 489993 Color: 1
Size: 257407 Color: 1
Size: 252601 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 490336 Color: 1
Size: 258729 Color: 1
Size: 250936 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 490364 Color: 1
Size: 259310 Color: 1
Size: 250327 Color: 0

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 490373 Color: 1
Size: 256973 Color: 1
Size: 252655 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 491008 Color: 1
Size: 256342 Color: 1
Size: 252651 Color: 0

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 491204 Color: 1
Size: 255882 Color: 1
Size: 252915 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 491580 Color: 1
Size: 257246 Color: 1
Size: 251175 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 491627 Color: 1
Size: 256355 Color: 1
Size: 252019 Color: 0

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 491651 Color: 1
Size: 255804 Color: 1
Size: 252546 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 492024 Color: 1
Size: 257448 Color: 1
Size: 250529 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 492076 Color: 1
Size: 255892 Color: 1
Size: 252033 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 492116 Color: 1
Size: 256745 Color: 1
Size: 251140 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 492271 Color: 1
Size: 255341 Color: 1
Size: 252389 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 492379 Color: 1
Size: 256598 Color: 1
Size: 251024 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 492689 Color: 1
Size: 254088 Color: 1
Size: 253224 Color: 0

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 493097 Color: 1
Size: 255348 Color: 1
Size: 251556 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 493252 Color: 1
Size: 255979 Color: 1
Size: 250770 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 493286 Color: 1
Size: 254203 Color: 1
Size: 252512 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 493360 Color: 1
Size: 255529 Color: 1
Size: 251112 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 493382 Color: 1
Size: 255495 Color: 1
Size: 251124 Color: 0

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 493403 Color: 1
Size: 256321 Color: 1
Size: 250277 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 493556 Color: 1
Size: 255457 Color: 1
Size: 250988 Color: 0

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 493818 Color: 1
Size: 253732 Color: 1
Size: 252451 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 493990 Color: 1
Size: 255597 Color: 1
Size: 250414 Color: 0

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 494081 Color: 1
Size: 255407 Color: 1
Size: 250513 Color: 0

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 494163 Color: 1
Size: 255342 Color: 1
Size: 250496 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 494171 Color: 1
Size: 253727 Color: 1
Size: 252103 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 494316 Color: 1
Size: 255490 Color: 1
Size: 250195 Color: 0

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 494952 Color: 1
Size: 253842 Color: 1
Size: 251207 Color: 0

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 494967 Color: 1
Size: 254782 Color: 1
Size: 250252 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 495510 Color: 1
Size: 252975 Color: 1
Size: 251516 Color: 0

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 495591 Color: 1
Size: 252630 Color: 1
Size: 251780 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 495605 Color: 1
Size: 252993 Color: 1
Size: 251403 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 495827 Color: 1
Size: 253943 Color: 1
Size: 250231 Color: 0

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 495874 Color: 1
Size: 252704 Color: 1
Size: 251423 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 495877 Color: 1
Size: 252237 Color: 1
Size: 251887 Color: 0

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 495926 Color: 1
Size: 253758 Color: 1
Size: 250317 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 495985 Color: 1
Size: 253891 Color: 1
Size: 250125 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 496275 Color: 1
Size: 253091 Color: 1
Size: 250635 Color: 0

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 496335 Color: 1
Size: 253342 Color: 1
Size: 250324 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 358865 Color: 1
Size: 342837 Color: 1
Size: 298299 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 496662 Color: 1
Size: 253171 Color: 1
Size: 250168 Color: 0

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 497084 Color: 1
Size: 251991 Color: 1
Size: 250926 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 497283 Color: 1
Size: 252188 Color: 1
Size: 250530 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 497435 Color: 1
Size: 252126 Color: 1
Size: 250440 Color: 0

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 497583 Color: 1
Size: 252102 Color: 1
Size: 250316 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 497697 Color: 1
Size: 252190 Color: 1
Size: 250114 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 497861 Color: 1
Size: 252080 Color: 1
Size: 250060 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 498282 Color: 1
Size: 251413 Color: 1
Size: 250306 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 498430 Color: 1
Size: 251272 Color: 1
Size: 250299 Color: 0

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 498735 Color: 1
Size: 251200 Color: 1
Size: 250066 Color: 0

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 498783 Color: 1
Size: 250993 Color: 1
Size: 250225 Color: 0

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 498941 Color: 1
Size: 250986 Color: 1
Size: 250074 Color: 0

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 499055 Color: 1
Size: 250547 Color: 1
Size: 250399 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 346771 Color: 1
Size: 335951 Color: 1
Size: 317279 Color: 0

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 499205 Color: 1
Size: 250598 Color: 1
Size: 250198 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 499317 Color: 1
Size: 250455 Color: 1
Size: 250229 Color: 0

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 499354 Color: 1
Size: 250564 Color: 1
Size: 250083 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 499568 Color: 1
Size: 250242 Color: 1
Size: 250191 Color: 0

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 499731 Color: 1
Size: 250254 Color: 1
Size: 250016 Color: 0

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 499779 Color: 1
Size: 250181 Color: 1
Size: 250041 Color: 0

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 499968 Color: 1
Size: 250025 Color: 1
Size: 250008 Color: 0

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 420422 Color: 1
Size: 299811 Color: 0
Size: 279767 Color: 1

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 416399 Color: 1
Size: 323896 Color: 1
Size: 259705 Color: 0

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 374540 Color: 1
Size: 353698 Color: 1
Size: 271762 Color: 0

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 361909 Color: 1
Size: 319697 Color: 1
Size: 318394 Color: 0

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 425531 Color: 1
Size: 306427 Color: 1
Size: 268042 Color: 0

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 411626 Color: 1
Size: 311541 Color: 1
Size: 276833 Color: 0

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 441616 Color: 1
Size: 283144 Color: 1
Size: 275240 Color: 0

Bin 2972: 1 of cap free
Amount of items: 3
Items: 
Size: 356571 Color: 1
Size: 355314 Color: 1
Size: 288115 Color: 0

Bin 2973: 1 of cap free
Amount of items: 3
Items: 
Size: 378301 Color: 1
Size: 340953 Color: 1
Size: 280746 Color: 0

Bin 2974: 1 of cap free
Amount of items: 3
Items: 
Size: 360326 Color: 1
Size: 339713 Color: 1
Size: 299961 Color: 0

Bin 2975: 1 of cap free
Amount of items: 3
Items: 
Size: 418382 Color: 1
Size: 309457 Color: 1
Size: 272161 Color: 0

Bin 2976: 1 of cap free
Amount of items: 3
Items: 
Size: 382609 Color: 1
Size: 337606 Color: 1
Size: 279785 Color: 0

Bin 2977: 1 of cap free
Amount of items: 3
Items: 
Size: 393641 Color: 1
Size: 342020 Color: 1
Size: 264339 Color: 0

Bin 2978: 1 of cap free
Amount of items: 3
Items: 
Size: 498497 Color: 1
Size: 251011 Color: 1
Size: 250492 Color: 0

Bin 2979: 1 of cap free
Amount of items: 3
Items: 
Size: 365896 Color: 1
Size: 319817 Color: 1
Size: 314287 Color: 0

Bin 2980: 1 of cap free
Amount of items: 3
Items: 
Size: 424962 Color: 1
Size: 288604 Color: 0
Size: 286434 Color: 1

Bin 2981: 1 of cap free
Amount of items: 3
Items: 
Size: 359259 Color: 1
Size: 339738 Color: 1
Size: 301003 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 441079 Color: 1
Size: 299577 Color: 1
Size: 259344 Color: 0

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 423954 Color: 1
Size: 322943 Color: 1
Size: 253103 Color: 0

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 374914 Color: 1
Size: 353917 Color: 1
Size: 271169 Color: 0

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 430825 Color: 1
Size: 299647 Color: 1
Size: 269528 Color: 0

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 381479 Color: 1
Size: 353412 Color: 1
Size: 265109 Color: 0

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 350241 Color: 1
Size: 349208 Color: 1
Size: 300551 Color: 0

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 364636 Color: 1
Size: 347676 Color: 1
Size: 287688 Color: 0

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 422282 Color: 1
Size: 303420 Color: 1
Size: 274298 Color: 0

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 358005 Color: 1
Size: 321477 Color: 0
Size: 320518 Color: 1

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 416657 Color: 1
Size: 309887 Color: 1
Size: 273456 Color: 0

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 414961 Color: 1
Size: 292744 Color: 1
Size: 292295 Color: 0

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 405200 Color: 1
Size: 315175 Color: 1
Size: 279625 Color: 0

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 371270 Color: 1
Size: 322900 Color: 1
Size: 305830 Color: 0

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 407969 Color: 1
Size: 306511 Color: 1
Size: 285520 Color: 0

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 388943 Color: 1
Size: 354135 Color: 1
Size: 256922 Color: 0

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 363174 Color: 1
Size: 362695 Color: 1
Size: 274131 Color: 0

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 380307 Color: 1
Size: 327431 Color: 1
Size: 292262 Color: 0

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 422329 Color: 1
Size: 291638 Color: 0
Size: 286033 Color: 1

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 391748 Color: 1
Size: 329069 Color: 1
Size: 279183 Color: 0

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 355038 Color: 1
Size: 354895 Color: 1
Size: 290067 Color: 0

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 462596 Color: 1
Size: 287273 Color: 1
Size: 250131 Color: 0

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 377766 Color: 1
Size: 359678 Color: 1
Size: 262556 Color: 0

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 366834 Color: 1
Size: 363842 Color: 1
Size: 269324 Color: 0

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 370130 Color: 1
Size: 342463 Color: 1
Size: 287407 Color: 0

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 421749 Color: 1
Size: 320414 Color: 1
Size: 257837 Color: 0

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 397963 Color: 1
Size: 330075 Color: 1
Size: 271962 Color: 0

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 365974 Color: 1
Size: 360176 Color: 1
Size: 273850 Color: 0

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 361012 Color: 1
Size: 359423 Color: 1
Size: 279565 Color: 0

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 354140 Color: 1
Size: 338038 Color: 1
Size: 307822 Color: 0

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 362365 Color: 1
Size: 339119 Color: 1
Size: 298516 Color: 0

Bin 3012: 1 of cap free
Amount of items: 3
Items: 
Size: 356823 Color: 1
Size: 335642 Color: 1
Size: 307535 Color: 0

Bin 3013: 1 of cap free
Amount of items: 3
Items: 
Size: 410769 Color: 1
Size: 327495 Color: 1
Size: 261736 Color: 0

Bin 3014: 1 of cap free
Amount of items: 3
Items: 
Size: 363810 Color: 1
Size: 351107 Color: 1
Size: 285083 Color: 0

Bin 3015: 1 of cap free
Amount of items: 3
Items: 
Size: 364636 Color: 1
Size: 325506 Color: 0
Size: 309858 Color: 1

Bin 3016: 1 of cap free
Amount of items: 3
Items: 
Size: 369360 Color: 1
Size: 362091 Color: 1
Size: 268549 Color: 0

Bin 3017: 1 of cap free
Amount of items: 3
Items: 
Size: 348309 Color: 1
Size: 348031 Color: 1
Size: 303660 Color: 0

Bin 3018: 1 of cap free
Amount of items: 3
Items: 
Size: 381270 Color: 1
Size: 324516 Color: 1
Size: 294214 Color: 0

Bin 3019: 1 of cap free
Amount of items: 3
Items: 
Size: 360788 Color: 1
Size: 341842 Color: 1
Size: 297370 Color: 0

Bin 3020: 1 of cap free
Amount of items: 3
Items: 
Size: 354548 Color: 1
Size: 342974 Color: 1
Size: 302478 Color: 0

Bin 3021: 1 of cap free
Amount of items: 3
Items: 
Size: 361127 Color: 1
Size: 342475 Color: 1
Size: 296398 Color: 0

Bin 3022: 1 of cap free
Amount of items: 3
Items: 
Size: 370531 Color: 1
Size: 357334 Color: 1
Size: 272135 Color: 0

Bin 3023: 1 of cap free
Amount of items: 3
Items: 
Size: 363532 Color: 1
Size: 348183 Color: 1
Size: 288285 Color: 0

Bin 3024: 1 of cap free
Amount of items: 3
Items: 
Size: 368601 Color: 1
Size: 317816 Color: 1
Size: 313583 Color: 0

Bin 3025: 1 of cap free
Amount of items: 3
Items: 
Size: 495121 Color: 1
Size: 252868 Color: 1
Size: 252011 Color: 0

Bin 3026: 1 of cap free
Amount of items: 3
Items: 
Size: 498114 Color: 1
Size: 251235 Color: 1
Size: 250651 Color: 0

Bin 3027: 1 of cap free
Amount of items: 3
Items: 
Size: 498275 Color: 1
Size: 251129 Color: 1
Size: 250596 Color: 0

Bin 3028: 1 of cap free
Amount of items: 3
Items: 
Size: 350722 Color: 1
Size: 341729 Color: 1
Size: 307549 Color: 0

Bin 3029: 1 of cap free
Amount of items: 3
Items: 
Size: 361612 Color: 1
Size: 355441 Color: 1
Size: 282947 Color: 0

Bin 3030: 1 of cap free
Amount of items: 3
Items: 
Size: 363859 Color: 1
Size: 361147 Color: 1
Size: 274994 Color: 0

Bin 3031: 1 of cap free
Amount of items: 3
Items: 
Size: 358750 Color: 1
Size: 328865 Color: 0
Size: 312385 Color: 1

Bin 3032: 1 of cap free
Amount of items: 3
Items: 
Size: 355322 Color: 1
Size: 344922 Color: 1
Size: 299756 Color: 0

Bin 3033: 1 of cap free
Amount of items: 3
Items: 
Size: 384403 Color: 1
Size: 326436 Color: 1
Size: 289161 Color: 0

Bin 3034: 1 of cap free
Amount of items: 3
Items: 
Size: 348433 Color: 1
Size: 337218 Color: 1
Size: 314349 Color: 0

Bin 3035: 1 of cap free
Amount of items: 3
Items: 
Size: 375584 Color: 1
Size: 344027 Color: 1
Size: 280389 Color: 0

Bin 3036: 1 of cap free
Amount of items: 3
Items: 
Size: 418881 Color: 1
Size: 303300 Color: 1
Size: 277819 Color: 0

Bin 3037: 1 of cap free
Amount of items: 3
Items: 
Size: 416583 Color: 1
Size: 317393 Color: 1
Size: 266024 Color: 0

Bin 3038: 2 of cap free
Amount of items: 3
Items: 
Size: 417763 Color: 1
Size: 304370 Color: 1
Size: 277866 Color: 0

Bin 3039: 2 of cap free
Amount of items: 3
Items: 
Size: 419576 Color: 1
Size: 298182 Color: 0
Size: 282241 Color: 1

Bin 3040: 2 of cap free
Amount of items: 3
Items: 
Size: 407454 Color: 1
Size: 296483 Color: 1
Size: 296062 Color: 0

Bin 3041: 2 of cap free
Amount of items: 3
Items: 
Size: 416772 Color: 1
Size: 313630 Color: 1
Size: 269597 Color: 0

Bin 3042: 2 of cap free
Amount of items: 3
Items: 
Size: 478019 Color: 1
Size: 271163 Color: 1
Size: 250817 Color: 0

Bin 3043: 2 of cap free
Amount of items: 3
Items: 
Size: 432850 Color: 1
Size: 312482 Color: 1
Size: 254667 Color: 0

Bin 3044: 2 of cap free
Amount of items: 3
Items: 
Size: 448900 Color: 1
Size: 280703 Color: 1
Size: 270396 Color: 0

Bin 3045: 2 of cap free
Amount of items: 3
Items: 
Size: 406231 Color: 1
Size: 305257 Color: 1
Size: 288511 Color: 0

Bin 3046: 2 of cap free
Amount of items: 3
Items: 
Size: 396161 Color: 1
Size: 349766 Color: 1
Size: 254072 Color: 0

Bin 3047: 2 of cap free
Amount of items: 3
Items: 
Size: 460040 Color: 1
Size: 285754 Color: 1
Size: 254205 Color: 0

Bin 3048: 2 of cap free
Amount of items: 3
Items: 
Size: 395280 Color: 1
Size: 343232 Color: 1
Size: 261487 Color: 0

Bin 3049: 2 of cap free
Amount of items: 3
Items: 
Size: 406398 Color: 1
Size: 328056 Color: 1
Size: 265545 Color: 0

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 382398 Color: 1
Size: 328315 Color: 0
Size: 289286 Color: 1

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 346692 Color: 1
Size: 340175 Color: 1
Size: 313132 Color: 0

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 358375 Color: 1
Size: 357900 Color: 1
Size: 283724 Color: 0

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 400022 Color: 1
Size: 328195 Color: 0
Size: 271782 Color: 1

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 428007 Color: 1
Size: 321818 Color: 1
Size: 250174 Color: 0

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 456736 Color: 1
Size: 286478 Color: 1
Size: 256785 Color: 0

Bin 3056: 2 of cap free
Amount of items: 3
Items: 
Size: 418012 Color: 1
Size: 301688 Color: 1
Size: 280299 Color: 0

Bin 3057: 2 of cap free
Amount of items: 3
Items: 
Size: 359333 Color: 1
Size: 352693 Color: 1
Size: 287973 Color: 0

Bin 3058: 2 of cap free
Amount of items: 3
Items: 
Size: 358047 Color: 1
Size: 342168 Color: 1
Size: 299784 Color: 0

Bin 3059: 2 of cap free
Amount of items: 3
Items: 
Size: 438611 Color: 1
Size: 307527 Color: 1
Size: 253861 Color: 0

Bin 3060: 2 of cap free
Amount of items: 3
Items: 
Size: 418427 Color: 1
Size: 309855 Color: 1
Size: 271717 Color: 0

Bin 3061: 2 of cap free
Amount of items: 3
Items: 
Size: 408897 Color: 1
Size: 323892 Color: 1
Size: 267210 Color: 0

Bin 3062: 2 of cap free
Amount of items: 3
Items: 
Size: 411834 Color: 1
Size: 324281 Color: 1
Size: 263884 Color: 0

Bin 3063: 2 of cap free
Amount of items: 3
Items: 
Size: 355512 Color: 1
Size: 339442 Color: 1
Size: 305045 Color: 0

Bin 3064: 2 of cap free
Amount of items: 3
Items: 
Size: 446065 Color: 1
Size: 292737 Color: 1
Size: 261197 Color: 0

Bin 3065: 2 of cap free
Amount of items: 3
Items: 
Size: 354861 Color: 1
Size: 348734 Color: 1
Size: 296404 Color: 0

Bin 3066: 2 of cap free
Amount of items: 3
Items: 
Size: 367123 Color: 1
Size: 321410 Color: 0
Size: 311466 Color: 1

Bin 3067: 2 of cap free
Amount of items: 3
Items: 
Size: 358998 Color: 1
Size: 329626 Color: 1
Size: 311375 Color: 0

Bin 3068: 2 of cap free
Amount of items: 3
Items: 
Size: 391240 Color: 1
Size: 315499 Color: 0
Size: 293260 Color: 1

Bin 3069: 2 of cap free
Amount of items: 3
Items: 
Size: 396188 Color: 1
Size: 305469 Color: 0
Size: 298342 Color: 1

Bin 3070: 2 of cap free
Amount of items: 3
Items: 
Size: 416264 Color: 1
Size: 332761 Color: 1
Size: 250974 Color: 0

Bin 3071: 2 of cap free
Amount of items: 3
Items: 
Size: 423062 Color: 1
Size: 297461 Color: 1
Size: 279476 Color: 0

Bin 3072: 2 of cap free
Amount of items: 3
Items: 
Size: 364646 Color: 1
Size: 345135 Color: 1
Size: 290218 Color: 0

Bin 3073: 2 of cap free
Amount of items: 3
Items: 
Size: 358836 Color: 1
Size: 353677 Color: 1
Size: 287486 Color: 0

Bin 3074: 2 of cap free
Amount of items: 3
Items: 
Size: 360637 Color: 1
Size: 323800 Color: 1
Size: 315562 Color: 0

Bin 3075: 2 of cap free
Amount of items: 3
Items: 
Size: 353656 Color: 1
Size: 337070 Color: 1
Size: 309273 Color: 0

Bin 3076: 2 of cap free
Amount of items: 3
Items: 
Size: 391406 Color: 1
Size: 353478 Color: 1
Size: 255115 Color: 0

Bin 3077: 2 of cap free
Amount of items: 3
Items: 
Size: 362714 Color: 1
Size: 324394 Color: 1
Size: 312891 Color: 0

Bin 3078: 2 of cap free
Amount of items: 3
Items: 
Size: 365841 Color: 1
Size: 352817 Color: 1
Size: 281341 Color: 0

Bin 3079: 2 of cap free
Amount of items: 3
Items: 
Size: 433188 Color: 1
Size: 299650 Color: 1
Size: 267161 Color: 0

Bin 3080: 3 of cap free
Amount of items: 3
Items: 
Size: 439747 Color: 1
Size: 291819 Color: 0
Size: 268432 Color: 1

Bin 3081: 3 of cap free
Amount of items: 3
Items: 
Size: 365700 Color: 1
Size: 325795 Color: 0
Size: 308503 Color: 1

Bin 3082: 3 of cap free
Amount of items: 3
Items: 
Size: 340759 Color: 1
Size: 339876 Color: 1
Size: 319363 Color: 0

Bin 3083: 3 of cap free
Amount of items: 3
Items: 
Size: 419904 Color: 1
Size: 319786 Color: 1
Size: 260308 Color: 0

Bin 3084: 3 of cap free
Amount of items: 3
Items: 
Size: 458436 Color: 1
Size: 283661 Color: 1
Size: 257901 Color: 0

Bin 3085: 3 of cap free
Amount of items: 3
Items: 
Size: 401086 Color: 1
Size: 325487 Color: 1
Size: 273425 Color: 0

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 438980 Color: 1
Size: 303432 Color: 1
Size: 257586 Color: 0

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 355298 Color: 1
Size: 346943 Color: 1
Size: 297757 Color: 0

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 378892 Color: 1
Size: 355944 Color: 1
Size: 265162 Color: 0

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 358099 Color: 1
Size: 326475 Color: 0
Size: 315424 Color: 1

Bin 3090: 3 of cap free
Amount of items: 3
Items: 
Size: 397715 Color: 1
Size: 345666 Color: 1
Size: 256617 Color: 0

Bin 3091: 3 of cap free
Amount of items: 3
Items: 
Size: 424953 Color: 1
Size: 309636 Color: 1
Size: 265409 Color: 0

Bin 3092: 3 of cap free
Amount of items: 3
Items: 
Size: 392896 Color: 1
Size: 309748 Color: 1
Size: 297354 Color: 0

Bin 3093: 3 of cap free
Amount of items: 3
Items: 
Size: 416575 Color: 1
Size: 316563 Color: 0
Size: 266860 Color: 1

Bin 3094: 3 of cap free
Amount of items: 3
Items: 
Size: 349195 Color: 1
Size: 342303 Color: 1
Size: 308500 Color: 0

Bin 3095: 3 of cap free
Amount of items: 3
Items: 
Size: 423005 Color: 1
Size: 314974 Color: 1
Size: 262019 Color: 0

Bin 3096: 3 of cap free
Amount of items: 3
Items: 
Size: 372887 Color: 1
Size: 357949 Color: 1
Size: 269162 Color: 0

Bin 3097: 3 of cap free
Amount of items: 3
Items: 
Size: 461589 Color: 1
Size: 281541 Color: 1
Size: 256868 Color: 0

Bin 3098: 3 of cap free
Amount of items: 3
Items: 
Size: 356399 Color: 1
Size: 338328 Color: 1
Size: 305271 Color: 0

Bin 3099: 3 of cap free
Amount of items: 3
Items: 
Size: 354860 Color: 1
Size: 351994 Color: 1
Size: 293144 Color: 0

Bin 3100: 3 of cap free
Amount of items: 3
Items: 
Size: 384184 Color: 1
Size: 342864 Color: 1
Size: 272950 Color: 0

Bin 3101: 3 of cap free
Amount of items: 3
Items: 
Size: 351956 Color: 1
Size: 347498 Color: 1
Size: 300544 Color: 0

Bin 3102: 3 of cap free
Amount of items: 3
Items: 
Size: 351849 Color: 1
Size: 343070 Color: 1
Size: 305079 Color: 0

Bin 3103: 3 of cap free
Amount of items: 3
Items: 
Size: 381557 Color: 1
Size: 326460 Color: 1
Size: 291981 Color: 0

Bin 3104: 3 of cap free
Amount of items: 3
Items: 
Size: 376161 Color: 1
Size: 348982 Color: 1
Size: 274855 Color: 0

Bin 3105: 3 of cap free
Amount of items: 3
Items: 
Size: 376304 Color: 1
Size: 319215 Color: 0
Size: 304479 Color: 1

Bin 3106: 3 of cap free
Amount of items: 3
Items: 
Size: 383160 Color: 1
Size: 324322 Color: 1
Size: 292516 Color: 0

Bin 3107: 4 of cap free
Amount of items: 3
Items: 
Size: 402158 Color: 1
Size: 303303 Color: 0
Size: 294536 Color: 1

Bin 3108: 4 of cap free
Amount of items: 3
Items: 
Size: 360384 Color: 1
Size: 327121 Color: 0
Size: 312492 Color: 1

Bin 3109: 4 of cap free
Amount of items: 3
Items: 
Size: 411245 Color: 1
Size: 330483 Color: 1
Size: 258269 Color: 0

Bin 3110: 4 of cap free
Amount of items: 3
Items: 
Size: 444420 Color: 1
Size: 283629 Color: 0
Size: 271948 Color: 1

Bin 3111: 4 of cap free
Amount of items: 3
Items: 
Size: 398833 Color: 1
Size: 310570 Color: 1
Size: 290594 Color: 0

Bin 3112: 4 of cap free
Amount of items: 3
Items: 
Size: 442841 Color: 1
Size: 296164 Color: 1
Size: 260992 Color: 0

Bin 3113: 4 of cap free
Amount of items: 3
Items: 
Size: 406548 Color: 1
Size: 319212 Color: 1
Size: 274237 Color: 0

Bin 3114: 4 of cap free
Amount of items: 3
Items: 
Size: 358117 Color: 1
Size: 334786 Color: 1
Size: 307094 Color: 0

Bin 3115: 4 of cap free
Amount of items: 3
Items: 
Size: 376769 Color: 1
Size: 320069 Color: 1
Size: 303159 Color: 0

Bin 3116: 4 of cap free
Amount of items: 3
Items: 
Size: 395768 Color: 1
Size: 347612 Color: 1
Size: 256617 Color: 0

Bin 3117: 4 of cap free
Amount of items: 3
Items: 
Size: 460809 Color: 1
Size: 273344 Color: 0
Size: 265844 Color: 1

Bin 3118: 4 of cap free
Amount of items: 3
Items: 
Size: 369444 Color: 1
Size: 350250 Color: 1
Size: 280303 Color: 0

Bin 3119: 4 of cap free
Amount of items: 3
Items: 
Size: 363193 Color: 1
Size: 358592 Color: 1
Size: 278212 Color: 0

Bin 3120: 4 of cap free
Amount of items: 3
Items: 
Size: 460445 Color: 1
Size: 283316 Color: 1
Size: 256236 Color: 0

Bin 3121: 4 of cap free
Amount of items: 3
Items: 
Size: 354834 Color: 1
Size: 351260 Color: 1
Size: 293903 Color: 0

Bin 3122: 4 of cap free
Amount of items: 3
Items: 
Size: 413431 Color: 1
Size: 306674 Color: 1
Size: 279892 Color: 0

Bin 3123: 4 of cap free
Amount of items: 3
Items: 
Size: 376322 Color: 1
Size: 339630 Color: 1
Size: 284045 Color: 0

Bin 3124: 4 of cap free
Amount of items: 3
Items: 
Size: 372704 Color: 1
Size: 353876 Color: 1
Size: 273417 Color: 0

Bin 3125: 4 of cap free
Amount of items: 3
Items: 
Size: 388468 Color: 1
Size: 346127 Color: 1
Size: 265402 Color: 0

Bin 3126: 4 of cap free
Amount of items: 3
Items: 
Size: 401321 Color: 1
Size: 329820 Color: 1
Size: 268856 Color: 0

Bin 3127: 4 of cap free
Amount of items: 3
Items: 
Size: 349664 Color: 1
Size: 343431 Color: 1
Size: 306902 Color: 0

Bin 3128: 4 of cap free
Amount of items: 3
Items: 
Size: 437528 Color: 1
Size: 308416 Color: 1
Size: 254053 Color: 0

Bin 3129: 5 of cap free
Amount of items: 3
Items: 
Size: 383048 Color: 1
Size: 344410 Color: 1
Size: 272538 Color: 0

Bin 3130: 5 of cap free
Amount of items: 3
Items: 
Size: 378915 Color: 1
Size: 350466 Color: 1
Size: 270615 Color: 0

Bin 3131: 5 of cap free
Amount of items: 3
Items: 
Size: 422662 Color: 1
Size: 289556 Color: 1
Size: 287778 Color: 0

Bin 3132: 5 of cap free
Amount of items: 3
Items: 
Size: 371075 Color: 1
Size: 321415 Color: 1
Size: 307506 Color: 0

Bin 3133: 5 of cap free
Amount of items: 3
Items: 
Size: 399963 Color: 1
Size: 340965 Color: 1
Size: 259068 Color: 0

Bin 3134: 5 of cap free
Amount of items: 3
Items: 
Size: 403029 Color: 1
Size: 304791 Color: 0
Size: 292176 Color: 1

Bin 3135: 5 of cap free
Amount of items: 3
Items: 
Size: 399067 Color: 1
Size: 340649 Color: 1
Size: 260280 Color: 0

Bin 3136: 5 of cap free
Amount of items: 3
Items: 
Size: 361135 Color: 1
Size: 341412 Color: 1
Size: 297449 Color: 0

Bin 3137: 5 of cap free
Amount of items: 3
Items: 
Size: 368097 Color: 1
Size: 324643 Color: 0
Size: 307256 Color: 1

Bin 3138: 5 of cap free
Amount of items: 3
Items: 
Size: 423740 Color: 1
Size: 318339 Color: 1
Size: 257917 Color: 0

Bin 3139: 5 of cap free
Amount of items: 3
Items: 
Size: 389871 Color: 1
Size: 331550 Color: 1
Size: 278575 Color: 0

Bin 3140: 5 of cap free
Amount of items: 3
Items: 
Size: 388162 Color: 1
Size: 356149 Color: 1
Size: 255685 Color: 0

Bin 3141: 5 of cap free
Amount of items: 3
Items: 
Size: 388534 Color: 1
Size: 349222 Color: 1
Size: 262240 Color: 0

Bin 3142: 5 of cap free
Amount of items: 3
Items: 
Size: 362318 Color: 1
Size: 347001 Color: 1
Size: 290677 Color: 0

Bin 3143: 5 of cap free
Amount of items: 3
Items: 
Size: 457718 Color: 1
Size: 282937 Color: 1
Size: 259341 Color: 0

Bin 3144: 5 of cap free
Amount of items: 3
Items: 
Size: 382085 Color: 1
Size: 367482 Color: 1
Size: 250429 Color: 0

Bin 3145: 5 of cap free
Amount of items: 3
Items: 
Size: 372046 Color: 1
Size: 346572 Color: 1
Size: 281378 Color: 0

Bin 3146: 5 of cap free
Amount of items: 3
Items: 
Size: 446163 Color: 1
Size: 279678 Color: 0
Size: 274155 Color: 1

Bin 3147: 5 of cap free
Amount of items: 3
Items: 
Size: 350538 Color: 1
Size: 331398 Color: 1
Size: 318060 Color: 0

Bin 3148: 6 of cap free
Amount of items: 3
Items: 
Size: 424336 Color: 1
Size: 307511 Color: 1
Size: 268148 Color: 0

Bin 3149: 6 of cap free
Amount of items: 3
Items: 
Size: 407457 Color: 1
Size: 299057 Color: 1
Size: 293481 Color: 0

Bin 3150: 6 of cap free
Amount of items: 3
Items: 
Size: 360187 Color: 1
Size: 347243 Color: 1
Size: 292565 Color: 0

Bin 3151: 6 of cap free
Amount of items: 3
Items: 
Size: 361451 Color: 1
Size: 322001 Color: 0
Size: 316543 Color: 1

Bin 3152: 6 of cap free
Amount of items: 3
Items: 
Size: 369277 Color: 1
Size: 340021 Color: 1
Size: 290697 Color: 0

Bin 3153: 6 of cap free
Amount of items: 3
Items: 
Size: 398209 Color: 1
Size: 335832 Color: 1
Size: 265954 Color: 0

Bin 3154: 6 of cap free
Amount of items: 3
Items: 
Size: 405211 Color: 1
Size: 314482 Color: 1
Size: 280302 Color: 0

Bin 3155: 6 of cap free
Amount of items: 3
Items: 
Size: 402383 Color: 1
Size: 329889 Color: 1
Size: 267723 Color: 0

Bin 3156: 6 of cap free
Amount of items: 3
Items: 
Size: 365219 Color: 1
Size: 354249 Color: 1
Size: 280527 Color: 0

Bin 3157: 6 of cap free
Amount of items: 3
Items: 
Size: 425772 Color: 1
Size: 314644 Color: 1
Size: 259579 Color: 0

Bin 3158: 6 of cap free
Amount of items: 3
Items: 
Size: 417210 Color: 1
Size: 295907 Color: 0
Size: 286878 Color: 1

Bin 3159: 6 of cap free
Amount of items: 3
Items: 
Size: 358078 Color: 1
Size: 349819 Color: 1
Size: 292098 Color: 0

Bin 3160: 6 of cap free
Amount of items: 3
Items: 
Size: 349644 Color: 1
Size: 335179 Color: 1
Size: 315172 Color: 0

Bin 3161: 6 of cap free
Amount of items: 3
Items: 
Size: 424114 Color: 1
Size: 309728 Color: 0
Size: 266153 Color: 1

Bin 3162: 6 of cap free
Amount of items: 3
Items: 
Size: 353610 Color: 1
Size: 344012 Color: 1
Size: 302373 Color: 0

Bin 3163: 7 of cap free
Amount of items: 3
Items: 
Size: 358259 Color: 1
Size: 336085 Color: 1
Size: 305650 Color: 0

Bin 3164: 7 of cap free
Amount of items: 3
Items: 
Size: 347702 Color: 1
Size: 337968 Color: 1
Size: 314324 Color: 0

Bin 3165: 7 of cap free
Amount of items: 3
Items: 
Size: 449115 Color: 1
Size: 289130 Color: 1
Size: 261749 Color: 0

Bin 3166: 7 of cap free
Amount of items: 3
Items: 
Size: 456663 Color: 1
Size: 287586 Color: 1
Size: 255745 Color: 0

Bin 3167: 7 of cap free
Amount of items: 3
Items: 
Size: 338057 Color: 1
Size: 336610 Color: 1
Size: 325327 Color: 0

Bin 3168: 7 of cap free
Amount of items: 3
Items: 
Size: 346918 Color: 1
Size: 340104 Color: 1
Size: 312972 Color: 0

Bin 3169: 7 of cap free
Amount of items: 3
Items: 
Size: 429366 Color: 1
Size: 303201 Color: 1
Size: 267427 Color: 0

Bin 3170: 7 of cap free
Amount of items: 3
Items: 
Size: 349362 Color: 1
Size: 347577 Color: 1
Size: 303055 Color: 0

Bin 3171: 7 of cap free
Amount of items: 3
Items: 
Size: 369593 Color: 1
Size: 347273 Color: 1
Size: 283128 Color: 0

Bin 3172: 7 of cap free
Amount of items: 3
Items: 
Size: 368480 Color: 1
Size: 334105 Color: 1
Size: 297409 Color: 0

Bin 3173: 8 of cap free
Amount of items: 3
Items: 
Size: 365909 Color: 1
Size: 319364 Color: 1
Size: 314720 Color: 0

Bin 3174: 8 of cap free
Amount of items: 3
Items: 
Size: 421778 Color: 1
Size: 289695 Color: 0
Size: 288520 Color: 1

Bin 3175: 8 of cap free
Amount of items: 3
Items: 
Size: 366518 Color: 1
Size: 353531 Color: 1
Size: 279944 Color: 0

Bin 3176: 8 of cap free
Amount of items: 3
Items: 
Size: 378800 Color: 1
Size: 336269 Color: 1
Size: 284924 Color: 0

Bin 3177: 8 of cap free
Amount of items: 3
Items: 
Size: 356004 Color: 1
Size: 348305 Color: 1
Size: 295684 Color: 0

Bin 3178: 8 of cap free
Amount of items: 3
Items: 
Size: 346157 Color: 1
Size: 328743 Color: 0
Size: 325093 Color: 1

Bin 3179: 9 of cap free
Amount of items: 3
Items: 
Size: 343475 Color: 1
Size: 342288 Color: 1
Size: 314229 Color: 0

Bin 3180: 9 of cap free
Amount of items: 3
Items: 
Size: 376419 Color: 1
Size: 323968 Color: 1
Size: 299605 Color: 0

Bin 3181: 9 of cap free
Amount of items: 3
Items: 
Size: 395334 Color: 1
Size: 316371 Color: 1
Size: 288287 Color: 0

Bin 3182: 9 of cap free
Amount of items: 3
Items: 
Size: 359425 Color: 1
Size: 327014 Color: 1
Size: 313553 Color: 0

Bin 3183: 9 of cap free
Amount of items: 3
Items: 
Size: 441606 Color: 1
Size: 290066 Color: 1
Size: 268320 Color: 0

Bin 3184: 9 of cap free
Amount of items: 3
Items: 
Size: 403187 Color: 1
Size: 337782 Color: 1
Size: 259023 Color: 0

Bin 3185: 9 of cap free
Amount of items: 3
Items: 
Size: 359535 Color: 1
Size: 351328 Color: 1
Size: 289129 Color: 0

Bin 3186: 10 of cap free
Amount of items: 3
Items: 
Size: 408334 Color: 1
Size: 324527 Color: 1
Size: 267130 Color: 0

Bin 3187: 10 of cap free
Amount of items: 3
Items: 
Size: 405344 Color: 1
Size: 299894 Color: 1
Size: 294753 Color: 0

Bin 3188: 10 of cap free
Amount of items: 3
Items: 
Size: 371751 Color: 1
Size: 347387 Color: 1
Size: 280853 Color: 0

Bin 3189: 10 of cap free
Amount of items: 3
Items: 
Size: 412651 Color: 1
Size: 304135 Color: 0
Size: 283205 Color: 1

Bin 3190: 10 of cap free
Amount of items: 3
Items: 
Size: 371229 Color: 1
Size: 344770 Color: 1
Size: 283992 Color: 0

Bin 3191: 10 of cap free
Amount of items: 3
Items: 
Size: 408720 Color: 1
Size: 323792 Color: 1
Size: 267479 Color: 0

Bin 3192: 10 of cap free
Amount of items: 3
Items: 
Size: 386422 Color: 1
Size: 309853 Color: 1
Size: 303716 Color: 0

Bin 3193: 10 of cap free
Amount of items: 3
Items: 
Size: 355416 Color: 1
Size: 323261 Color: 1
Size: 321314 Color: 0

Bin 3194: 11 of cap free
Amount of items: 3
Items: 
Size: 362318 Color: 1
Size: 328042 Color: 1
Size: 309630 Color: 0

Bin 3195: 11 of cap free
Amount of items: 3
Items: 
Size: 430175 Color: 1
Size: 295172 Color: 0
Size: 274643 Color: 1

Bin 3196: 11 of cap free
Amount of items: 3
Items: 
Size: 399896 Color: 1
Size: 326415 Color: 1
Size: 273679 Color: 0

Bin 3197: 11 of cap free
Amount of items: 3
Items: 
Size: 371630 Color: 1
Size: 352801 Color: 1
Size: 275559 Color: 0

Bin 3198: 11 of cap free
Amount of items: 3
Items: 
Size: 445905 Color: 1
Size: 303638 Color: 1
Size: 250447 Color: 0

Bin 3199: 11 of cap free
Amount of items: 3
Items: 
Size: 355851 Color: 1
Size: 350510 Color: 1
Size: 293629 Color: 0

Bin 3200: 11 of cap free
Amount of items: 3
Items: 
Size: 424599 Color: 1
Size: 312503 Color: 1
Size: 262888 Color: 0

Bin 3201: 12 of cap free
Amount of items: 3
Items: 
Size: 376519 Color: 1
Size: 344214 Color: 1
Size: 279256 Color: 0

Bin 3202: 12 of cap free
Amount of items: 3
Items: 
Size: 432429 Color: 1
Size: 300468 Color: 1
Size: 267092 Color: 0

Bin 3203: 12 of cap free
Amount of items: 3
Items: 
Size: 393075 Color: 1
Size: 333971 Color: 1
Size: 272943 Color: 0

Bin 3204: 12 of cap free
Amount of items: 3
Items: 
Size: 430619 Color: 1
Size: 298641 Color: 1
Size: 270729 Color: 0

Bin 3205: 12 of cap free
Amount of items: 3
Items: 
Size: 394770 Color: 1
Size: 305853 Color: 0
Size: 299366 Color: 1

Bin 3206: 12 of cap free
Amount of items: 3
Items: 
Size: 363876 Color: 1
Size: 348224 Color: 1
Size: 287889 Color: 0

Bin 3207: 12 of cap free
Amount of items: 3
Items: 
Size: 350997 Color: 1
Size: 348063 Color: 1
Size: 300929 Color: 0

Bin 3208: 12 of cap free
Amount of items: 3
Items: 
Size: 356078 Color: 1
Size: 323791 Color: 1
Size: 320120 Color: 0

Bin 3209: 12 of cap free
Amount of items: 3
Items: 
Size: 445724 Color: 1
Size: 290822 Color: 1
Size: 263443 Color: 0

Bin 3210: 13 of cap free
Amount of items: 3
Items: 
Size: 374356 Color: 1
Size: 319798 Color: 1
Size: 305834 Color: 0

Bin 3211: 13 of cap free
Amount of items: 3
Items: 
Size: 368466 Color: 1
Size: 356613 Color: 1
Size: 274909 Color: 0

Bin 3212: 13 of cap free
Amount of items: 3
Items: 
Size: 410676 Color: 1
Size: 315225 Color: 0
Size: 274087 Color: 1

Bin 3213: 13 of cap free
Amount of items: 3
Items: 
Size: 409585 Color: 1
Size: 316186 Color: 1
Size: 274217 Color: 0

Bin 3214: 13 of cap free
Amount of items: 3
Items: 
Size: 358038 Color: 1
Size: 338777 Color: 1
Size: 303173 Color: 0

Bin 3215: 13 of cap free
Amount of items: 3
Items: 
Size: 411289 Color: 1
Size: 306375 Color: 1
Size: 282324 Color: 0

Bin 3216: 14 of cap free
Amount of items: 3
Items: 
Size: 418684 Color: 1
Size: 314115 Color: 1
Size: 267188 Color: 0

Bin 3217: 14 of cap free
Amount of items: 3
Items: 
Size: 378826 Color: 1
Size: 343840 Color: 1
Size: 277321 Color: 0

Bin 3218: 15 of cap free
Amount of items: 3
Items: 
Size: 399304 Color: 1
Size: 324795 Color: 1
Size: 275887 Color: 0

Bin 3219: 15 of cap free
Amount of items: 3
Items: 
Size: 351656 Color: 1
Size: 331216 Color: 1
Size: 317114 Color: 0

Bin 3220: 15 of cap free
Amount of items: 3
Items: 
Size: 408696 Color: 1
Size: 313252 Color: 1
Size: 278038 Color: 0

Bin 3221: 16 of cap free
Amount of items: 3
Items: 
Size: 349600 Color: 1
Size: 346378 Color: 1
Size: 304007 Color: 0

Bin 3222: 16 of cap free
Amount of items: 3
Items: 
Size: 383238 Color: 1
Size: 327352 Color: 1
Size: 289395 Color: 0

Bin 3223: 16 of cap free
Amount of items: 3
Items: 
Size: 432337 Color: 1
Size: 298074 Color: 1
Size: 269574 Color: 0

Bin 3224: 16 of cap free
Amount of items: 3
Items: 
Size: 465108 Color: 1
Size: 273628 Color: 1
Size: 261249 Color: 0

Bin 3225: 16 of cap free
Amount of items: 3
Items: 
Size: 357026 Color: 1
Size: 355619 Color: 1
Size: 287340 Color: 0

Bin 3226: 17 of cap free
Amount of items: 3
Items: 
Size: 447732 Color: 1
Size: 288389 Color: 1
Size: 263863 Color: 0

Bin 3227: 18 of cap free
Amount of items: 3
Items: 
Size: 361450 Color: 1
Size: 352393 Color: 1
Size: 286140 Color: 0

Bin 3228: 18 of cap free
Amount of items: 3
Items: 
Size: 432317 Color: 1
Size: 313389 Color: 1
Size: 254277 Color: 0

Bin 3229: 18 of cap free
Amount of items: 3
Items: 
Size: 350744 Color: 1
Size: 343237 Color: 1
Size: 306002 Color: 0

Bin 3230: 19 of cap free
Amount of items: 3
Items: 
Size: 379058 Color: 1
Size: 351699 Color: 1
Size: 269225 Color: 0

Bin 3231: 19 of cap free
Amount of items: 3
Items: 
Size: 480718 Color: 1
Size: 268510 Color: 0
Size: 250754 Color: 1

Bin 3232: 19 of cap free
Amount of items: 3
Items: 
Size: 415537 Color: 1
Size: 321043 Color: 1
Size: 263402 Color: 0

Bin 3233: 19 of cap free
Amount of items: 3
Items: 
Size: 369438 Color: 1
Size: 348666 Color: 1
Size: 281878 Color: 0

Bin 3234: 20 of cap free
Amount of items: 3
Items: 
Size: 436772 Color: 1
Size: 290934 Color: 0
Size: 272275 Color: 1

Bin 3235: 20 of cap free
Amount of items: 3
Items: 
Size: 363455 Color: 1
Size: 346952 Color: 1
Size: 289574 Color: 0

Bin 3236: 21 of cap free
Amount of items: 3
Items: 
Size: 472308 Color: 1
Size: 266669 Color: 1
Size: 261003 Color: 0

Bin 3237: 21 of cap free
Amount of items: 3
Items: 
Size: 405697 Color: 1
Size: 314041 Color: 1
Size: 280242 Color: 0

Bin 3238: 22 of cap free
Amount of items: 3
Items: 
Size: 404960 Color: 1
Size: 324551 Color: 1
Size: 270468 Color: 0

Bin 3239: 22 of cap free
Amount of items: 3
Items: 
Size: 365592 Color: 1
Size: 330747 Color: 1
Size: 303640 Color: 0

Bin 3240: 23 of cap free
Amount of items: 3
Items: 
Size: 369483 Color: 1
Size: 322346 Color: 0
Size: 308149 Color: 1

Bin 3241: 23 of cap free
Amount of items: 3
Items: 
Size: 499068 Color: 1
Size: 250586 Color: 1
Size: 250324 Color: 0

Bin 3242: 23 of cap free
Amount of items: 3
Items: 
Size: 392389 Color: 1
Size: 328930 Color: 1
Size: 278659 Color: 0

Bin 3243: 23 of cap free
Amount of items: 3
Items: 
Size: 397065 Color: 1
Size: 304513 Color: 1
Size: 298400 Color: 0

Bin 3244: 23 of cap free
Amount of items: 3
Items: 
Size: 394853 Color: 1
Size: 344758 Color: 1
Size: 260367 Color: 0

Bin 3245: 24 of cap free
Amount of items: 3
Items: 
Size: 439140 Color: 1
Size: 300112 Color: 1
Size: 260725 Color: 0

Bin 3246: 24 of cap free
Amount of items: 3
Items: 
Size: 464975 Color: 1
Size: 277170 Color: 1
Size: 257832 Color: 0

Bin 3247: 24 of cap free
Amount of items: 3
Items: 
Size: 382437 Color: 1
Size: 338207 Color: 1
Size: 279333 Color: 0

Bin 3248: 24 of cap free
Amount of items: 3
Items: 
Size: 414791 Color: 1
Size: 308260 Color: 1
Size: 276926 Color: 0

Bin 3249: 24 of cap free
Amount of items: 3
Items: 
Size: 495174 Color: 1
Size: 252408 Color: 1
Size: 252395 Color: 0

Bin 3250: 25 of cap free
Amount of items: 3
Items: 
Size: 386590 Color: 1
Size: 322387 Color: 1
Size: 290999 Color: 0

Bin 3251: 25 of cap free
Amount of items: 3
Items: 
Size: 351510 Color: 1
Size: 335070 Color: 1
Size: 313396 Color: 0

Bin 3252: 26 of cap free
Amount of items: 3
Items: 
Size: 419345 Color: 1
Size: 312558 Color: 1
Size: 268072 Color: 0

Bin 3253: 26 of cap free
Amount of items: 3
Items: 
Size: 415964 Color: 1
Size: 312063 Color: 1
Size: 271948 Color: 0

Bin 3254: 26 of cap free
Amount of items: 3
Items: 
Size: 496646 Color: 1
Size: 252614 Color: 0
Size: 250715 Color: 1

Bin 3255: 26 of cap free
Amount of items: 3
Items: 
Size: 367677 Color: 1
Size: 319920 Color: 0
Size: 312378 Color: 1

Bin 3256: 26 of cap free
Amount of items: 3
Items: 
Size: 349137 Color: 1
Size: 345967 Color: 1
Size: 304871 Color: 0

Bin 3257: 29 of cap free
Amount of items: 3
Items: 
Size: 392075 Color: 1
Size: 342544 Color: 1
Size: 265353 Color: 0

Bin 3258: 29 of cap free
Amount of items: 3
Items: 
Size: 365556 Color: 1
Size: 326883 Color: 1
Size: 307533 Color: 0

Bin 3259: 29 of cap free
Amount of items: 3
Items: 
Size: 364619 Color: 1
Size: 359383 Color: 1
Size: 275970 Color: 0

Bin 3260: 29 of cap free
Amount of items: 3
Items: 
Size: 348489 Color: 1
Size: 327402 Color: 0
Size: 324081 Color: 1

Bin 3261: 29 of cap free
Amount of items: 3
Items: 
Size: 366319 Color: 1
Size: 323961 Color: 1
Size: 309692 Color: 0

Bin 3262: 30 of cap free
Amount of items: 3
Items: 
Size: 456937 Color: 1
Size: 290858 Color: 1
Size: 252176 Color: 0

Bin 3263: 30 of cap free
Amount of items: 3
Items: 
Size: 349810 Color: 1
Size: 328468 Color: 1
Size: 321693 Color: 0

Bin 3264: 30 of cap free
Amount of items: 3
Items: 
Size: 410086 Color: 1
Size: 304393 Color: 0
Size: 285492 Color: 1

Bin 3265: 30 of cap free
Amount of items: 3
Items: 
Size: 372111 Color: 1
Size: 335782 Color: 1
Size: 292078 Color: 0

Bin 3266: 30 of cap free
Amount of items: 3
Items: 
Size: 426162 Color: 1
Size: 308597 Color: 1
Size: 265212 Color: 0

Bin 3267: 31 of cap free
Amount of items: 3
Items: 
Size: 393280 Color: 1
Size: 323745 Color: 1
Size: 282945 Color: 0

Bin 3268: 32 of cap free
Amount of items: 3
Items: 
Size: 419575 Color: 1
Size: 324593 Color: 1
Size: 255801 Color: 0

Bin 3269: 32 of cap free
Amount of items: 3
Items: 
Size: 380789 Color: 1
Size: 343870 Color: 1
Size: 275310 Color: 0

Bin 3270: 33 of cap free
Amount of items: 3
Items: 
Size: 429717 Color: 1
Size: 300018 Color: 1
Size: 270233 Color: 0

Bin 3271: 33 of cap free
Amount of items: 3
Items: 
Size: 360722 Color: 1
Size: 342714 Color: 1
Size: 296532 Color: 0

Bin 3272: 34 of cap free
Amount of items: 3
Items: 
Size: 429975 Color: 1
Size: 306879 Color: 1
Size: 263113 Color: 0

Bin 3273: 35 of cap free
Amount of items: 3
Items: 
Size: 415059 Color: 1
Size: 305407 Color: 1
Size: 279500 Color: 0

Bin 3274: 38 of cap free
Amount of items: 3
Items: 
Size: 349453 Color: 1
Size: 348525 Color: 1
Size: 301985 Color: 0

Bin 3275: 39 of cap free
Amount of items: 3
Items: 
Size: 361079 Color: 1
Size: 348118 Color: 1
Size: 290765 Color: 0

Bin 3276: 40 of cap free
Amount of items: 3
Items: 
Size: 406925 Color: 1
Size: 334772 Color: 1
Size: 258264 Color: 0

Bin 3277: 42 of cap free
Amount of items: 3
Items: 
Size: 365056 Color: 1
Size: 352638 Color: 1
Size: 282265 Color: 0

Bin 3278: 45 of cap free
Amount of items: 3
Items: 
Size: 375088 Color: 1
Size: 317859 Color: 1
Size: 307009 Color: 0

Bin 3279: 45 of cap free
Amount of items: 3
Items: 
Size: 464827 Color: 1
Size: 268458 Color: 1
Size: 266671 Color: 0

Bin 3280: 49 of cap free
Amount of items: 3
Items: 
Size: 444566 Color: 1
Size: 279001 Color: 1
Size: 276385 Color: 0

Bin 3281: 51 of cap free
Amount of items: 3
Items: 
Size: 430704 Color: 1
Size: 288743 Color: 1
Size: 280503 Color: 0

Bin 3282: 51 of cap free
Amount of items: 3
Items: 
Size: 424231 Color: 1
Size: 312661 Color: 1
Size: 263058 Color: 0

Bin 3283: 54 of cap free
Amount of items: 3
Items: 
Size: 354510 Color: 1
Size: 350597 Color: 1
Size: 294840 Color: 0

Bin 3284: 54 of cap free
Amount of items: 3
Items: 
Size: 420611 Color: 1
Size: 309301 Color: 1
Size: 270035 Color: 0

Bin 3285: 55 of cap free
Amount of items: 3
Items: 
Size: 377967 Color: 1
Size: 326783 Color: 1
Size: 295196 Color: 0

Bin 3286: 58 of cap free
Amount of items: 3
Items: 
Size: 386227 Color: 1
Size: 355733 Color: 1
Size: 257983 Color: 0

Bin 3287: 58 of cap free
Amount of items: 3
Items: 
Size: 352954 Color: 1
Size: 346407 Color: 1
Size: 300582 Color: 0

Bin 3288: 59 of cap free
Amount of items: 3
Items: 
Size: 381512 Color: 1
Size: 314366 Color: 1
Size: 304064 Color: 0

Bin 3289: 60 of cap free
Amount of items: 3
Items: 
Size: 371855 Color: 1
Size: 319717 Color: 1
Size: 308369 Color: 0

Bin 3290: 61 of cap free
Amount of items: 3
Items: 
Size: 373317 Color: 1
Size: 335874 Color: 1
Size: 290749 Color: 0

Bin 3291: 63 of cap free
Amount of items: 3
Items: 
Size: 371128 Color: 1
Size: 357565 Color: 1
Size: 271245 Color: 0

Bin 3292: 65 of cap free
Amount of items: 3
Items: 
Size: 409471 Color: 1
Size: 321089 Color: 1
Size: 269376 Color: 0

Bin 3293: 69 of cap free
Amount of items: 3
Items: 
Size: 407849 Color: 1
Size: 313250 Color: 1
Size: 278833 Color: 0

Bin 3294: 71 of cap free
Amount of items: 3
Items: 
Size: 388880 Color: 1
Size: 317504 Color: 1
Size: 293546 Color: 0

Bin 3295: 75 of cap free
Amount of items: 3
Items: 
Size: 399384 Color: 1
Size: 346943 Color: 1
Size: 253599 Color: 0

Bin 3296: 78 of cap free
Amount of items: 3
Items: 
Size: 367027 Color: 1
Size: 347303 Color: 1
Size: 285593 Color: 0

Bin 3297: 79 of cap free
Amount of items: 3
Items: 
Size: 444106 Color: 1
Size: 287560 Color: 1
Size: 268256 Color: 0

Bin 3298: 81 of cap free
Amount of items: 3
Items: 
Size: 373643 Color: 1
Size: 369262 Color: 1
Size: 257015 Color: 0

Bin 3299: 82 of cap free
Amount of items: 3
Items: 
Size: 410410 Color: 1
Size: 328873 Color: 1
Size: 260636 Color: 0

Bin 3300: 83 of cap free
Amount of items: 3
Items: 
Size: 408635 Color: 1
Size: 339043 Color: 1
Size: 252240 Color: 0

Bin 3301: 84 of cap free
Amount of items: 3
Items: 
Size: 380554 Color: 1
Size: 358561 Color: 1
Size: 260802 Color: 0

Bin 3302: 85 of cap free
Amount of items: 3
Items: 
Size: 417641 Color: 1
Size: 309986 Color: 1
Size: 272289 Color: 0

Bin 3303: 86 of cap free
Amount of items: 3
Items: 
Size: 400911 Color: 1
Size: 340780 Color: 1
Size: 258224 Color: 0

Bin 3304: 88 of cap free
Amount of items: 3
Items: 
Size: 368792 Color: 1
Size: 324753 Color: 1
Size: 306368 Color: 0

Bin 3305: 97 of cap free
Amount of items: 3
Items: 
Size: 398483 Color: 1
Size: 348102 Color: 1
Size: 253319 Color: 0

Bin 3306: 122 of cap free
Amount of items: 3
Items: 
Size: 462265 Color: 1
Size: 274949 Color: 1
Size: 262665 Color: 0

Bin 3307: 122 of cap free
Amount of items: 3
Items: 
Size: 434280 Color: 1
Size: 304508 Color: 1
Size: 261091 Color: 0

Bin 3308: 124 of cap free
Amount of items: 3
Items: 
Size: 475783 Color: 1
Size: 273123 Color: 1
Size: 250971 Color: 0

Bin 3309: 125 of cap free
Amount of items: 3
Items: 
Size: 457982 Color: 1
Size: 286705 Color: 1
Size: 255189 Color: 0

Bin 3310: 133 of cap free
Amount of items: 3
Items: 
Size: 424689 Color: 1
Size: 311193 Color: 1
Size: 263986 Color: 0

Bin 3311: 137 of cap free
Amount of items: 3
Items: 
Size: 417873 Color: 1
Size: 330647 Color: 1
Size: 251344 Color: 0

Bin 3312: 138 of cap free
Amount of items: 3
Items: 
Size: 353700 Color: 1
Size: 352233 Color: 1
Size: 293930 Color: 0

Bin 3313: 142 of cap free
Amount of items: 3
Items: 
Size: 380874 Color: 1
Size: 328300 Color: 1
Size: 290685 Color: 0

Bin 3314: 162 of cap free
Amount of items: 3
Items: 
Size: 347637 Color: 1
Size: 335325 Color: 1
Size: 316877 Color: 0

Bin 3315: 165 of cap free
Amount of items: 3
Items: 
Size: 399848 Color: 1
Size: 326009 Color: 1
Size: 273979 Color: 0

Bin 3316: 171 of cap free
Amount of items: 3
Items: 
Size: 378975 Color: 1
Size: 327804 Color: 1
Size: 293051 Color: 0

Bin 3317: 176 of cap free
Amount of items: 3
Items: 
Size: 357062 Color: 1
Size: 337559 Color: 1
Size: 305204 Color: 0

Bin 3318: 180 of cap free
Amount of items: 3
Items: 
Size: 424295 Color: 1
Size: 309908 Color: 1
Size: 265618 Color: 0

Bin 3319: 185 of cap free
Amount of items: 3
Items: 
Size: 440772 Color: 1
Size: 300699 Color: 1
Size: 258345 Color: 0

Bin 3320: 343 of cap free
Amount of items: 3
Items: 
Size: 360496 Color: 1
Size: 334031 Color: 1
Size: 305131 Color: 0

Bin 3321: 454 of cap free
Amount of items: 3
Items: 
Size: 403229 Color: 1
Size: 315499 Color: 1
Size: 280819 Color: 0

Bin 3322: 488 of cap free
Amount of items: 3
Items: 
Size: 397429 Color: 1
Size: 323956 Color: 1
Size: 278128 Color: 0

Bin 3323: 623 of cap free
Amount of items: 3
Items: 
Size: 442913 Color: 1
Size: 296037 Color: 1
Size: 260428 Color: 0

Bin 3324: 746 of cap free
Amount of items: 3
Items: 
Size: 361786 Color: 1
Size: 349274 Color: 1
Size: 288195 Color: 0

Bin 3325: 762 of cap free
Amount of items: 3
Items: 
Size: 461470 Color: 1
Size: 283967 Color: 1
Size: 253802 Color: 0

Bin 3326: 861 of cap free
Amount of items: 3
Items: 
Size: 436445 Color: 1
Size: 298464 Color: 1
Size: 264231 Color: 0

Bin 3327: 996 of cap free
Amount of items: 3
Items: 
Size: 363664 Color: 1
Size: 324684 Color: 1
Size: 310657 Color: 0

Bin 3328: 1079 of cap free
Amount of items: 3
Items: 
Size: 440213 Color: 1
Size: 295853 Color: 1
Size: 262856 Color: 0

Bin 3329: 1663 of cap free
Amount of items: 3
Items: 
Size: 363110 Color: 1
Size: 343576 Color: 1
Size: 291652 Color: 0

Bin 3330: 2865 of cap free
Amount of items: 3
Items: 
Size: 451803 Color: 1
Size: 277341 Color: 0
Size: 267992 Color: 1

Bin 3331: 22788 of cap free
Amount of items: 3
Items: 
Size: 356742 Color: 1
Size: 323955 Color: 1
Size: 296516 Color: 0

Bin 3332: 71115 of cap free
Amount of items: 3
Items: 
Size: 347882 Color: 1
Size: 319431 Color: 1
Size: 261573 Color: 0

Bin 3333: 138418 of cap free
Amount of items: 3
Items: 
Size: 318859 Color: 1
Size: 281400 Color: 1
Size: 261324 Color: 0

Bin 3334: 249149 of cap free
Amount of items: 2
Items: 
Size: 498931 Color: 1
Size: 251921 Color: 0

Bin 3335: 501123 of cap free
Amount of items: 1
Items: 
Size: 498878 Color: 1

Total size: 3334003334
Total free space: 1000001

