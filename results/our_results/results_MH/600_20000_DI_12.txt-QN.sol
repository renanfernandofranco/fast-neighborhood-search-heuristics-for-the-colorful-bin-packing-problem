Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 441
Size: 6666 Color: 364
Size: 1332 Color: 169

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 13814 Color: 476
Size: 3792 Color: 287
Size: 2042 Color: 222

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13862 Color: 478
Size: 3506 Color: 277
Size: 2280 Color: 238

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 480
Size: 4424 Color: 313
Size: 1332 Color: 170

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 484
Size: 4692 Color: 322
Size: 936 Color: 134

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 485
Size: 5284 Color: 336
Size: 312 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 487
Size: 4570 Color: 316
Size: 976 Color: 143

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 492
Size: 4656 Color: 320
Size: 416 Color: 45

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 493
Size: 3878 Color: 291
Size: 1180 Color: 160

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 494
Size: 4668 Color: 321
Size: 368 Color: 31

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 496
Size: 4096 Color: 301
Size: 888 Color: 125

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 499
Size: 3862 Color: 289
Size: 1072 Color: 149

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 500
Size: 4544 Color: 315
Size: 376 Color: 32

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14751 Color: 501
Size: 4081 Color: 300
Size: 816 Color: 117

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14810 Color: 504
Size: 4034 Color: 298
Size: 804 Color: 114

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14876 Color: 508
Size: 3524 Color: 278
Size: 1248 Color: 164

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 516
Size: 4104 Color: 302
Size: 424 Color: 49

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15136 Color: 517
Size: 4136 Color: 304
Size: 376 Color: 33

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15240 Color: 518
Size: 3076 Color: 266
Size: 1332 Color: 171

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15264 Color: 519
Size: 3996 Color: 296
Size: 388 Color: 40

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15336 Color: 520
Size: 3608 Color: 283
Size: 704 Color: 94

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15346 Color: 521
Size: 3586 Color: 282
Size: 716 Color: 102

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15392 Color: 522
Size: 4064 Color: 299
Size: 192 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15800 Color: 533
Size: 3484 Color: 275
Size: 364 Color: 30

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15874 Color: 535
Size: 2820 Color: 256
Size: 954 Color: 138

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15916 Color: 536
Size: 3196 Color: 271
Size: 536 Color: 68

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15920 Color: 537
Size: 3024 Color: 263
Size: 704 Color: 97

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16008 Color: 540
Size: 2904 Color: 259
Size: 736 Color: 104

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16168 Color: 543
Size: 3072 Color: 265
Size: 408 Color: 43

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16224 Color: 544
Size: 2488 Color: 244
Size: 936 Color: 135

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16240 Color: 545
Size: 3312 Color: 274
Size: 96 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16268 Color: 546
Size: 1824 Color: 211
Size: 1556 Color: 190

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16288 Color: 547
Size: 2864 Color: 258
Size: 496 Color: 60

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16342 Color: 548
Size: 2758 Color: 255
Size: 548 Color: 71

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16484 Color: 551
Size: 1636 Color: 200
Size: 1528 Color: 184

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 552
Size: 2400 Color: 240
Size: 752 Color: 105

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16680 Color: 558
Size: 2136 Color: 227
Size: 832 Color: 123

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16720 Color: 559
Size: 2848 Color: 257
Size: 80 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16756 Color: 561
Size: 2412 Color: 242
Size: 480 Color: 58

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16864 Color: 562
Size: 2224 Color: 233
Size: 560 Color: 72

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16920 Color: 564
Size: 1632 Color: 198
Size: 1096 Color: 151

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16928 Color: 565
Size: 1636 Color: 201
Size: 1084 Color: 150

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16930 Color: 566
Size: 1902 Color: 217
Size: 816 Color: 115

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17082 Color: 573
Size: 1846 Color: 213
Size: 720 Color: 103

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17264 Color: 578
Size: 1904 Color: 218
Size: 480 Color: 59

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17306 Color: 579
Size: 1798 Color: 210
Size: 544 Color: 70

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17336 Color: 580
Size: 1928 Color: 220
Size: 384 Color: 35

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17352 Color: 582
Size: 1344 Color: 173
Size: 952 Color: 137

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17380 Color: 584
Size: 1564 Color: 191
Size: 704 Color: 96

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17534 Color: 590
Size: 1730 Color: 207
Size: 384 Color: 37

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17558 Color: 593
Size: 1152 Color: 157
Size: 938 Color: 136

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17584 Color: 595
Size: 1424 Color: 182
Size: 640 Color: 87

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 596
Size: 1408 Color: 181
Size: 632 Color: 84

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 597
Size: 1600 Color: 194
Size: 416 Color: 47

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17636 Color: 598
Size: 1548 Color: 189
Size: 464 Color: 56

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17640 Color: 599
Size: 1320 Color: 166
Size: 688 Color: 92

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17670 Color: 600
Size: 1540 Color: 188
Size: 438 Color: 50

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 14013 Color: 483
Size: 5442 Color: 339
Size: 192 Color: 10

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 14313 Color: 491
Size: 5334 Color: 337

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 15112 Color: 515
Size: 4447 Color: 314
Size: 88 Color: 4

Bin 61: 2 of cap free
Amount of items: 5
Items: 
Size: 9880 Color: 413
Size: 3784 Color: 286
Size: 3536 Color: 279
Size: 1838 Color: 212
Size: 608 Color: 78

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 436
Size: 7654 Color: 380
Size: 448 Color: 51

Bin 63: 2 of cap free
Amount of items: 4
Items: 
Size: 13188 Color: 466
Size: 5978 Color: 352
Size: 256 Color: 13
Size: 224 Color: 12

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 13917 Color: 481
Size: 5185 Color: 335
Size: 544 Color: 69

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 15442 Color: 526
Size: 4204 Color: 310

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 15682 Color: 530
Size: 3964 Color: 295

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 16500 Color: 553
Size: 3146 Color: 270

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 17504 Color: 589
Size: 2142 Color: 228

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 17574 Color: 594
Size: 2072 Color: 224

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 11634 Color: 439
Size: 4140 Color: 305
Size: 3871 Color: 290

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 15005 Color: 512
Size: 4640 Color: 319

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 15048 Color: 514
Size: 4597 Color: 317

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 437
Size: 7666 Color: 382
Size: 424 Color: 48

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 13152 Color: 465
Size: 5168 Color: 334
Size: 1324 Color: 168

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 482
Size: 5388 Color: 338
Size: 268 Color: 16

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 498
Size: 4960 Color: 332

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 15476 Color: 528
Size: 4168 Color: 308

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 15730 Color: 532
Size: 3914 Color: 292

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 15964 Color: 539
Size: 3680 Color: 284

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 17000 Color: 568
Size: 2644 Color: 252

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 17056 Color: 571
Size: 2588 Color: 249

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 17076 Color: 572
Size: 2568 Color: 248

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 17536 Color: 591
Size: 2108 Color: 226

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 14866 Color: 507
Size: 4777 Color: 326

Bin 85: 6 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 434
Size: 7746 Color: 386
Size: 448 Color: 53

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 11602 Color: 438
Size: 8040 Color: 390

Bin 87: 6 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 458
Size: 6626 Color: 362
Size: 288 Color: 17

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 13028 Color: 460
Size: 6614 Color: 360

Bin 89: 6 of cap free
Amount of items: 2
Items: 
Size: 14780 Color: 503
Size: 4862 Color: 329

Bin 90: 6 of cap free
Amount of items: 2
Items: 
Size: 16730 Color: 560
Size: 2912 Color: 260

Bin 91: 6 of cap free
Amount of items: 2
Items: 
Size: 17124 Color: 575
Size: 2518 Color: 246

Bin 92: 6 of cap free
Amount of items: 2
Items: 
Size: 17136 Color: 576
Size: 2506 Color: 245

Bin 93: 6 of cap free
Amount of items: 2
Items: 
Size: 17370 Color: 583
Size: 2272 Color: 237

Bin 94: 6 of cap free
Amount of items: 2
Items: 
Size: 17494 Color: 588
Size: 2148 Color: 229

Bin 95: 8 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 451
Size: 6840 Color: 372
Size: 336 Color: 25

Bin 96: 8 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 463
Size: 4142 Color: 306
Size: 2406 Color: 241

Bin 97: 8 of cap free
Amount of items: 2
Items: 
Size: 15018 Color: 513
Size: 4622 Color: 318

Bin 98: 8 of cap free
Amount of items: 2
Items: 
Size: 15440 Color: 525
Size: 4200 Color: 309

Bin 99: 8 of cap free
Amount of items: 2
Items: 
Size: 17028 Color: 570
Size: 2612 Color: 250

Bin 100: 8 of cap free
Amount of items: 2
Items: 
Size: 17392 Color: 585
Size: 2248 Color: 236

Bin 101: 8 of cap free
Amount of items: 2
Items: 
Size: 17544 Color: 592
Size: 2096 Color: 225

Bin 102: 10 of cap free
Amount of items: 5
Items: 
Size: 9888 Color: 414
Size: 4017 Color: 297
Size: 3957 Color: 294
Size: 1168 Color: 159
Size: 608 Color: 77

Bin 103: 10 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 490
Size: 5472 Color: 340

Bin 104: 10 of cap free
Amount of items: 2
Items: 
Size: 14816 Color: 505
Size: 4822 Color: 328

Bin 105: 10 of cap free
Amount of items: 2
Items: 
Size: 15420 Color: 524
Size: 4218 Color: 311

Bin 106: 10 of cap free
Amount of items: 2
Items: 
Size: 16430 Color: 550
Size: 3208 Color: 272

Bin 107: 10 of cap free
Amount of items: 2
Items: 
Size: 16956 Color: 567
Size: 2682 Color: 253

Bin 108: 10 of cap free
Amount of items: 2
Items: 
Size: 17190 Color: 577
Size: 2448 Color: 243

Bin 109: 10 of cap free
Amount of items: 2
Items: 
Size: 17340 Color: 581
Size: 2298 Color: 239

Bin 110: 10 of cap free
Amount of items: 2
Items: 
Size: 17446 Color: 587
Size: 2192 Color: 231

Bin 111: 11 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 461
Size: 5483 Color: 341
Size: 1096 Color: 152

Bin 112: 11 of cap free
Amount of items: 2
Items: 
Size: 14940 Color: 511
Size: 4697 Color: 323

Bin 113: 12 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 435
Size: 7682 Color: 383
Size: 448 Color: 52

Bin 114: 12 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 448
Size: 6996 Color: 376
Size: 352 Color: 28

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 470
Size: 6084 Color: 355

Bin 116: 12 of cap free
Amount of items: 2
Items: 
Size: 14112 Color: 488
Size: 5524 Color: 343

Bin 117: 12 of cap free
Amount of items: 2
Items: 
Size: 15948 Color: 538
Size: 3688 Color: 285

Bin 118: 12 of cap free
Amount of items: 2
Items: 
Size: 16516 Color: 554
Size: 3120 Color: 269

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 17008 Color: 569
Size: 2628 Color: 251

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 17096 Color: 574
Size: 2540 Color: 247

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 17428 Color: 586
Size: 2208 Color: 232

Bin 122: 13 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 456
Size: 6667 Color: 365
Size: 320 Color: 20

Bin 123: 14 of cap free
Amount of items: 3
Items: 
Size: 12546 Color: 452
Size: 6760 Color: 370
Size: 328 Color: 24

Bin 124: 14 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 454
Size: 6720 Color: 368
Size: 320 Color: 22

Bin 125: 14 of cap free
Amount of items: 3
Items: 
Size: 12690 Color: 457
Size: 6624 Color: 361
Size: 320 Color: 19

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 15394 Color: 523
Size: 4240 Color: 312

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 15520 Color: 529
Size: 4114 Color: 303

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 16082 Color: 542
Size: 3552 Color: 281

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 549
Size: 3266 Color: 273

Bin 130: 15 of cap free
Amount of items: 3
Items: 
Size: 12631 Color: 455
Size: 6682 Color: 366
Size: 320 Color: 21

Bin 131: 15 of cap free
Amount of items: 3
Items: 
Size: 13250 Color: 467
Size: 6159 Color: 356
Size: 224 Color: 11

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 13865 Color: 479
Size: 5768 Color: 345

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 14829 Color: 506
Size: 4804 Color: 327

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 13456 Color: 469
Size: 6048 Color: 354
Size: 128 Color: 7

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 473
Size: 5848 Color: 347
Size: 64 Color: 1

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 14616 Color: 495
Size: 5016 Color: 333

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 14760 Color: 502
Size: 4872 Color: 330

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 16548 Color: 555
Size: 3084 Color: 267

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 16584 Color: 556
Size: 3048 Color: 264

Bin 140: 18 of cap free
Amount of items: 4
Items: 
Size: 11698 Color: 443
Size: 7132 Color: 377
Size: 400 Color: 42
Size: 400 Color: 41

Bin 141: 18 of cap free
Amount of items: 3
Items: 
Size: 13122 Color: 464
Size: 6252 Color: 357
Size: 256 Color: 14

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 16894 Color: 563
Size: 2736 Color: 254

Bin 143: 19 of cap free
Amount of items: 3
Items: 
Size: 13069 Color: 462
Size: 6300 Color: 358
Size: 260 Color: 15

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 12986 Color: 459
Size: 6642 Color: 363

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 13756 Color: 475
Size: 5872 Color: 349

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 14133 Color: 489
Size: 5495 Color: 342

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 15474 Color: 527
Size: 4154 Color: 307

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 15820 Color: 534
Size: 3808 Color: 288

Bin 149: 21 of cap free
Amount of items: 3
Items: 
Size: 11649 Color: 440
Size: 6378 Color: 359
Size: 1600 Color: 193

Bin 150: 22 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 453
Size: 6706 Color: 367
Size: 328 Color: 23

Bin 151: 22 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 472
Size: 5922 Color: 351
Size: 64 Color: 2

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 16642 Color: 557
Size: 2984 Color: 262

Bin 153: 23 of cap free
Amount of items: 3
Items: 
Size: 13728 Color: 474
Size: 5849 Color: 348
Size: 48 Color: 0

Bin 154: 23 of cap free
Amount of items: 2
Items: 
Size: 14901 Color: 510
Size: 4724 Color: 324

Bin 155: 25 of cap free
Amount of items: 4
Items: 
Size: 11714 Color: 444
Size: 7141 Color: 378
Size: 384 Color: 39
Size: 384 Color: 38

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 15696 Color: 531
Size: 3924 Color: 293

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 477
Size: 5802 Color: 346

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 14064 Color: 486
Size: 5554 Color: 344

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 497
Size: 4952 Color: 331

Bin 160: 30 of cap free
Amount of items: 2
Items: 
Size: 16072 Color: 541
Size: 3546 Color: 280

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 10008 Color: 416
Size: 9608 Color: 402

Bin 162: 32 of cap free
Amount of items: 2
Items: 
Size: 14892 Color: 509
Size: 4724 Color: 325

Bin 163: 34 of cap free
Amount of items: 7
Items: 
Size: 9836 Color: 410
Size: 1954 Color: 221
Size: 1924 Color: 219
Size: 1892 Color: 216
Size: 1856 Color: 215
Size: 1540 Color: 187
Size: 612 Color: 80

Bin 164: 38 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 425
Size: 8186 Color: 398
Size: 512 Color: 64

Bin 165: 41 of cap free
Amount of items: 3
Items: 
Size: 12259 Color: 447
Size: 6992 Color: 375
Size: 356 Color: 29

Bin 166: 48 of cap free
Amount of items: 3
Items: 
Size: 13568 Color: 471
Size: 5904 Color: 350
Size: 128 Color: 6

Bin 167: 54 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 432
Size: 7826 Color: 389
Size: 464 Color: 55

Bin 168: 60 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 449
Size: 6896 Color: 373
Size: 344 Color: 27

Bin 169: 62 of cap free
Amount of items: 3
Items: 
Size: 11408 Color: 433
Size: 7722 Color: 385
Size: 456 Color: 54

Bin 170: 72 of cap free
Amount of items: 2
Items: 
Size: 9904 Color: 415
Size: 9672 Color: 403

Bin 171: 77 of cap free
Amount of items: 3
Items: 
Size: 13427 Color: 468
Size: 6000 Color: 353
Size: 144 Color: 8

Bin 172: 78 of cap free
Amount of items: 3
Items: 
Size: 12448 Color: 450
Size: 6786 Color: 371
Size: 336 Color: 26

Bin 173: 80 of cap free
Amount of items: 3
Items: 
Size: 11280 Color: 431
Size: 7808 Color: 388
Size: 480 Color: 57

Bin 174: 102 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 430
Size: 7786 Color: 387
Size: 500 Color: 61

Bin 175: 148 of cap free
Amount of items: 3
Items: 
Size: 12148 Color: 446
Size: 6968 Color: 374
Size: 384 Color: 34

Bin 176: 192 of cap free
Amount of items: 3
Items: 
Size: 11744 Color: 445
Size: 7328 Color: 379
Size: 384 Color: 36

Bin 177: 243 of cap free
Amount of items: 9
Items: 
Size: 9827 Color: 406
Size: 1426 Color: 183
Size: 1392 Color: 180
Size: 1376 Color: 179
Size: 1376 Color: 178
Size: 1360 Color: 177
Size: 1356 Color: 176
Size: 652 Color: 89
Size: 640 Color: 88

Bin 178: 256 of cap free
Amount of items: 21
Items: 
Size: 1152 Color: 155
Size: 1152 Color: 154
Size: 1108 Color: 153
Size: 1064 Color: 148
Size: 1036 Color: 147
Size: 1024 Color: 146
Size: 1024 Color: 145
Size: 992 Color: 144
Size: 972 Color: 142
Size: 964 Color: 141
Size: 960 Color: 140
Size: 824 Color: 119
Size: 820 Color: 118
Size: 816 Color: 116
Size: 802 Color: 113
Size: 792 Color: 112
Size: 790 Color: 111
Size: 784 Color: 110
Size: 776 Color: 109
Size: 772 Color: 108
Size: 768 Color: 107

Bin 179: 282 of cap free
Amount of items: 8
Items: 
Size: 9832 Color: 408
Size: 1688 Color: 204
Size: 1684 Color: 203
Size: 1650 Color: 202
Size: 1636 Color: 199
Size: 1632 Color: 197
Size: 628 Color: 83
Size: 616 Color: 82

Bin 180: 297 of cap free
Amount of items: 11
Items: 
Size: 9825 Color: 404
Size: 1230 Color: 163
Size: 1216 Color: 162
Size: 1184 Color: 161
Size: 1156 Color: 158
Size: 1152 Color: 156
Size: 768 Color: 106
Size: 708 Color: 101
Size: 704 Color: 100
Size: 704 Color: 99
Size: 704 Color: 98

Bin 181: 330 of cap free
Amount of items: 3
Items: 
Size: 11100 Color: 429
Size: 7706 Color: 384
Size: 512 Color: 62

Bin 182: 344 of cap free
Amount of items: 5
Items: 
Size: 9848 Color: 412
Size: 3488 Color: 276
Size: 3116 Color: 268
Size: 2244 Color: 235
Size: 608 Color: 79

Bin 183: 368 of cap free
Amount of items: 5
Items: 
Size: 9840 Color: 411
Size: 2974 Color: 261
Size: 2240 Color: 234
Size: 2176 Color: 230
Size: 2050 Color: 223

Bin 184: 379 of cap free
Amount of items: 2
Items: 
Size: 11081 Color: 427
Size: 8188 Color: 400

Bin 185: 384 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 428
Size: 7660 Color: 381
Size: 512 Color: 63

Bin 186: 388 of cap free
Amount of items: 4
Items: 
Size: 11682 Color: 442
Size: 6746 Color: 369
Size: 416 Color: 46
Size: 416 Color: 44

Bin 187: 420 of cap free
Amount of items: 2
Items: 
Size: 11041 Color: 426
Size: 8187 Color: 399

Bin 188: 434 of cap free
Amount of items: 10
Items: 
Size: 9826 Color: 405
Size: 1348 Color: 175
Size: 1344 Color: 174
Size: 1340 Color: 172
Size: 1324 Color: 167
Size: 1280 Color: 165
Size: 704 Color: 95
Size: 700 Color: 93
Size: 676 Color: 91
Size: 672 Color: 90

Bin 189: 434 of cap free
Amount of items: 7
Items: 
Size: 9834 Color: 409
Size: 1852 Color: 214
Size: 1768 Color: 209
Size: 1744 Color: 208
Size: 1704 Color: 206
Size: 1696 Color: 205
Size: 616 Color: 81

Bin 190: 477 of cap free
Amount of items: 3
Items: 
Size: 10466 Color: 424
Size: 8185 Color: 397
Size: 520 Color: 65

Bin 191: 488 of cap free
Amount of items: 2
Items: 
Size: 10258 Color: 417
Size: 8902 Color: 401

Bin 192: 494 of cap free
Amount of items: 3
Items: 
Size: 10402 Color: 421
Size: 8176 Color: 394
Size: 576 Color: 73

Bin 193: 494 of cap free
Amount of items: 3
Items: 
Size: 10450 Color: 423
Size: 8184 Color: 396
Size: 520 Color: 66

Bin 194: 501 of cap free
Amount of items: 3
Items: 
Size: 10386 Color: 420
Size: 8169 Color: 393
Size: 592 Color: 74

Bin 195: 514 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 422
Size: 8180 Color: 395
Size: 520 Color: 67

Bin 196: 542 of cap free
Amount of items: 3
Items: 
Size: 10354 Color: 419
Size: 8160 Color: 392
Size: 592 Color: 75

Bin 197: 606 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 418
Size: 8144 Color: 391
Size: 592 Color: 76

Bin 198: 612 of cap free
Amount of items: 8
Items: 
Size: 9828 Color: 407
Size: 1632 Color: 196
Size: 1632 Color: 195
Size: 1600 Color: 192
Size: 1532 Color: 186
Size: 1532 Color: 185
Size: 640 Color: 86
Size: 640 Color: 85

Bin 199: 8050 of cap free
Amount of items: 13
Items: 
Size: 960 Color: 139
Size: 928 Color: 133
Size: 928 Color: 132
Size: 924 Color: 131
Size: 918 Color: 130
Size: 912 Color: 129
Size: 912 Color: 128
Size: 896 Color: 127
Size: 888 Color: 126
Size: 840 Color: 124
Size: 832 Color: 122
Size: 832 Color: 121
Size: 828 Color: 120

Total size: 3890304
Total free space: 19648

