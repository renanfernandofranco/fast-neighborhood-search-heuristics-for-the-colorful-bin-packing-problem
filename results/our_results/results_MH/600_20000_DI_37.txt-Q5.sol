Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9028 Color: 1
Size: 6660 Color: 4
Size: 312 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9748 Color: 0
Size: 5746 Color: 1
Size: 506 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10728 Color: 2
Size: 4996 Color: 1
Size: 276 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10834 Color: 2
Size: 4778 Color: 1
Size: 388 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 1
Size: 4364 Color: 0
Size: 348 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 1
Size: 4308 Color: 0
Size: 360 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11386 Color: 0
Size: 4322 Color: 1
Size: 292 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 2
Size: 3660 Color: 0
Size: 360 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12214 Color: 0
Size: 3082 Color: 3
Size: 704 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 4
Size: 3288 Color: 3
Size: 256 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 2
Size: 3278 Color: 3
Size: 470 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 4
Size: 2786 Color: 0
Size: 556 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12818 Color: 3
Size: 2542 Color: 1
Size: 640 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 3
Size: 2728 Color: 1
Size: 428 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 4
Size: 2118 Color: 3
Size: 928 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 4
Size: 2530 Color: 0
Size: 504 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 3
Size: 1564 Color: 4
Size: 1452 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 3
Size: 2628 Color: 1
Size: 312 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13156 Color: 1
Size: 1984 Color: 2
Size: 860 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 3
Size: 2520 Color: 0
Size: 276 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 1
Size: 2358 Color: 4
Size: 420 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 4
Size: 1444 Color: 0
Size: 1332 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 3
Size: 1950 Color: 1
Size: 812 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 3
Size: 2290 Color: 1
Size: 456 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 0
Size: 1348 Color: 1
Size: 1332 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 2
Size: 1588 Color: 3
Size: 996 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13305 Color: 1
Size: 2119 Color: 1
Size: 576 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13484 Color: 4
Size: 2176 Color: 0
Size: 340 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 3
Size: 2090 Color: 4
Size: 314 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13631 Color: 3
Size: 1733 Color: 0
Size: 636 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13632 Color: 2
Size: 1984 Color: 3
Size: 384 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13694 Color: 4
Size: 1912 Color: 0
Size: 394 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 1
Size: 1844 Color: 3
Size: 440 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 1
Size: 1908 Color: 4
Size: 372 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 4
Size: 1412 Color: 2
Size: 788 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13818 Color: 2
Size: 2102 Color: 1
Size: 80 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13830 Color: 0
Size: 1506 Color: 4
Size: 664 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13895 Color: 1
Size: 1577 Color: 3
Size: 528 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 4
Size: 1312 Color: 1
Size: 792 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 1
Size: 1432 Color: 3
Size: 626 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13958 Color: 1
Size: 1390 Color: 3
Size: 652 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 4
Size: 1720 Color: 1
Size: 336 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14061 Color: 1
Size: 1517 Color: 2
Size: 422 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 1
Size: 1586 Color: 2
Size: 352 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14089 Color: 2
Size: 1511 Color: 3
Size: 400 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14085 Color: 1
Size: 1593 Color: 4
Size: 322 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 0
Size: 1494 Color: 2
Size: 382 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14172 Color: 3
Size: 1588 Color: 1
Size: 240 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14157 Color: 1
Size: 1471 Color: 4
Size: 372 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14204 Color: 1
Size: 1328 Color: 3
Size: 468 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 0
Size: 1160 Color: 4
Size: 646 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 1
Size: 1438 Color: 4
Size: 352 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14263 Color: 0
Size: 1449 Color: 1
Size: 288 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14278 Color: 0
Size: 1358 Color: 1
Size: 364 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14276 Color: 1
Size: 1378 Color: 0
Size: 346 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14294 Color: 0
Size: 1422 Color: 4
Size: 284 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 1
Size: 1342 Color: 4
Size: 284 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14326 Color: 0
Size: 1368 Color: 3
Size: 306 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 1
Size: 976 Color: 2
Size: 648 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 1
Size: 1148 Color: 3
Size: 464 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 2
Size: 1156 Color: 1
Size: 488 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 4
Size: 1264 Color: 1
Size: 346 Color: 2

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 9707 Color: 3
Size: 5732 Color: 1
Size: 560 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 2
Size: 4715 Color: 1
Size: 512 Color: 2

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 10818 Color: 0
Size: 5181 Color: 2

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12070 Color: 2
Size: 2793 Color: 4
Size: 1136 Color: 0

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12239 Color: 0
Size: 3240 Color: 1
Size: 520 Color: 2

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12649 Color: 2
Size: 3000 Color: 1
Size: 350 Color: 2

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 3
Size: 2663 Color: 1
Size: 416 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13063 Color: 0
Size: 2648 Color: 1
Size: 288 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 3
Size: 2449 Color: 0
Size: 376 Color: 2

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13459 Color: 0
Size: 2332 Color: 0
Size: 208 Color: 2

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13494 Color: 3
Size: 2209 Color: 0
Size: 296 Color: 4

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13525 Color: 2
Size: 1562 Color: 3
Size: 912 Color: 0

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 2
Size: 1917 Color: 2
Size: 420 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 4
Size: 1922 Color: 3
Size: 300 Color: 0

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13913 Color: 1
Size: 1822 Color: 3
Size: 264 Color: 2

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 14041 Color: 0
Size: 1176 Color: 4
Size: 782 Color: 1

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 14109 Color: 2
Size: 1574 Color: 4
Size: 316 Color: 1

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 14189 Color: 2
Size: 1810 Color: 0

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 14237 Color: 2
Size: 1428 Color: 1
Size: 334 Color: 4

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 1
Size: 5882 Color: 0
Size: 268 Color: 4

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11106 Color: 2
Size: 4452 Color: 1
Size: 440 Color: 0

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 1
Size: 4306 Color: 3
Size: 448 Color: 2

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12452 Color: 3
Size: 1788 Color: 1
Size: 1758 Color: 0

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 4
Size: 3158 Color: 1
Size: 96 Color: 3

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 2
Size: 1676 Color: 1
Size: 860 Color: 0

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 13985 Color: 4
Size: 1741 Color: 0
Size: 272 Color: 1

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 4
Size: 1702 Color: 3

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 14308 Color: 4
Size: 1690 Color: 0

Bin 91: 3 of cap free
Amount of items: 7
Items: 
Size: 8002 Color: 3
Size: 1748 Color: 1
Size: 1537 Color: 3
Size: 1512 Color: 3
Size: 1454 Color: 4
Size: 1120 Color: 1
Size: 624 Color: 2

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 1
Size: 1812 Color: 2
Size: 1012 Color: 4

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 9124 Color: 4
Size: 6600 Color: 3
Size: 272 Color: 2

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 10076 Color: 2
Size: 5608 Color: 3
Size: 312 Color: 4

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 10644 Color: 0
Size: 5128 Color: 4
Size: 224 Color: 1

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 10960 Color: 4
Size: 5036 Color: 3

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 1
Size: 4120 Color: 4
Size: 448 Color: 2

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 0
Size: 3956 Color: 0
Size: 320 Color: 1

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 11996 Color: 1
Size: 2828 Color: 1
Size: 1172 Color: 2

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 0
Size: 2964 Color: 2
Size: 192 Color: 0

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 0
Size: 2438 Color: 2
Size: 240 Color: 3

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 13624 Color: 1
Size: 2372 Color: 3

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 2
Size: 2168 Color: 1

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 3
Size: 1992 Color: 4

Bin 105: 5 of cap free
Amount of items: 5
Items: 
Size: 8004 Color: 4
Size: 2802 Color: 1
Size: 2357 Color: 0
Size: 1848 Color: 3
Size: 984 Color: 4

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 9042 Color: 1
Size: 6665 Color: 4
Size: 288 Color: 2

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 4
Size: 5891 Color: 2

Bin 108: 5 of cap free
Amount of items: 3
Items: 
Size: 11971 Color: 0
Size: 2580 Color: 3
Size: 1444 Color: 2

Bin 109: 5 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 4
Size: 3135 Color: 1
Size: 752 Color: 0

Bin 110: 5 of cap free
Amount of items: 2
Items: 
Size: 13460 Color: 3
Size: 2535 Color: 1

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 10660 Color: 1
Size: 4934 Color: 4
Size: 400 Color: 1

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 10850 Color: 1
Size: 4920 Color: 0
Size: 224 Color: 3

Bin 113: 6 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 3
Size: 4042 Color: 1
Size: 888 Color: 0

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 0
Size: 3538 Color: 2
Size: 336 Color: 1

Bin 115: 6 of cap free
Amount of items: 2
Items: 
Size: 13692 Color: 2
Size: 2302 Color: 1

Bin 116: 6 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 0
Size: 2100 Color: 3

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 10639 Color: 3
Size: 4986 Color: 2
Size: 368 Color: 0

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 12674 Color: 4
Size: 2949 Color: 1
Size: 370 Color: 0

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 13921 Color: 2
Size: 2072 Color: 0

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 8088 Color: 3
Size: 7904 Color: 0

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 1
Size: 2238 Color: 2

Bin 122: 9 of cap free
Amount of items: 3
Items: 
Size: 8933 Color: 2
Size: 6666 Color: 1
Size: 392 Color: 2

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 10417 Color: 2
Size: 4946 Color: 0
Size: 628 Color: 1

Bin 124: 10 of cap free
Amount of items: 8
Items: 
Size: 8008 Color: 2
Size: 1398 Color: 4
Size: 1368 Color: 3
Size: 1328 Color: 3
Size: 1328 Color: 0
Size: 1176 Color: 1
Size: 1024 Color: 2
Size: 360 Color: 4

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 14334 Color: 0
Size: 1656 Color: 4

Bin 126: 11 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 4
Size: 3949 Color: 1
Size: 512 Color: 0

Bin 127: 11 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 1
Size: 3359 Color: 2
Size: 328 Color: 2

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 2
Size: 3846 Color: 0
Size: 988 Color: 2

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 14114 Color: 3
Size: 1874 Color: 2

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 13351 Color: 1
Size: 2636 Color: 3

Bin 131: 14 of cap free
Amount of items: 5
Items: 
Size: 8001 Color: 1
Size: 2898 Color: 1
Size: 2654 Color: 3
Size: 1617 Color: 1
Size: 816 Color: 4

Bin 132: 14 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 1
Size: 4442 Color: 4
Size: 528 Color: 0

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 0
Size: 1718 Color: 2

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 3
Size: 1961 Color: 4

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 10018 Color: 1
Size: 5686 Color: 0
Size: 280 Color: 4

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 11304 Color: 4
Size: 4680 Color: 2

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 11649 Color: 4
Size: 4335 Color: 0

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 1
Size: 3576 Color: 2

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 3
Size: 2456 Color: 4

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 3
Size: 2124 Color: 4

Bin 141: 17 of cap free
Amount of items: 4
Items: 
Size: 8012 Color: 0
Size: 3928 Color: 1
Size: 3627 Color: 0
Size: 416 Color: 4

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 14350 Color: 4
Size: 1633 Color: 2

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 10770 Color: 2
Size: 5212 Color: 4

Bin 144: 18 of cap free
Amount of items: 3
Items: 
Size: 11858 Color: 0
Size: 3804 Color: 3
Size: 320 Color: 2

Bin 145: 18 of cap free
Amount of items: 3
Items: 
Size: 12642 Color: 4
Size: 3244 Color: 2
Size: 96 Color: 3

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 2
Size: 3198 Color: 3

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 13414 Color: 3
Size: 2568 Color: 0

Bin 148: 20 of cap free
Amount of items: 3
Items: 
Size: 9178 Color: 2
Size: 5762 Color: 3
Size: 1040 Color: 1

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 12526 Color: 2
Size: 3454 Color: 1

Bin 150: 22 of cap free
Amount of items: 3
Items: 
Size: 8856 Color: 3
Size: 6662 Color: 1
Size: 460 Color: 4

Bin 151: 22 of cap free
Amount of items: 2
Items: 
Size: 12166 Color: 3
Size: 3812 Color: 4

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 13974 Color: 3
Size: 2004 Color: 2

Bin 153: 22 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 2
Size: 1618 Color: 0

Bin 154: 24 of cap free
Amount of items: 3
Items: 
Size: 9140 Color: 0
Size: 5952 Color: 2
Size: 884 Color: 1

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 14052 Color: 4
Size: 1924 Color: 3

Bin 156: 25 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 1
Size: 4959 Color: 2
Size: 512 Color: 4

Bin 157: 25 of cap free
Amount of items: 2
Items: 
Size: 13647 Color: 4
Size: 2328 Color: 2

Bin 158: 26 of cap free
Amount of items: 3
Items: 
Size: 9122 Color: 4
Size: 5812 Color: 0
Size: 1040 Color: 0

Bin 159: 27 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 3
Size: 1681 Color: 0

Bin 160: 28 of cap free
Amount of items: 3
Items: 
Size: 14258 Color: 0
Size: 1668 Color: 4
Size: 46 Color: 3

Bin 161: 29 of cap free
Amount of items: 2
Items: 
Size: 12959 Color: 0
Size: 3012 Color: 2

Bin 162: 29 of cap free
Amount of items: 2
Items: 
Size: 13908 Color: 4
Size: 2063 Color: 3

Bin 163: 30 of cap free
Amount of items: 2
Items: 
Size: 8946 Color: 4
Size: 7024 Color: 3

Bin 164: 31 of cap free
Amount of items: 3
Items: 
Size: 10066 Color: 2
Size: 3928 Color: 0
Size: 1975 Color: 1

Bin 165: 32 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 0
Size: 3736 Color: 2
Size: 176 Color: 1

Bin 166: 32 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 2
Size: 3356 Color: 3

Bin 167: 33 of cap free
Amount of items: 4
Items: 
Size: 8003 Color: 1
Size: 4408 Color: 3
Size: 2968 Color: 0
Size: 588 Color: 4

Bin 168: 34 of cap free
Amount of items: 2
Items: 
Size: 11884 Color: 4
Size: 4082 Color: 0

Bin 169: 35 of cap free
Amount of items: 2
Items: 
Size: 14130 Color: 3
Size: 1835 Color: 2

Bin 170: 36 of cap free
Amount of items: 2
Items: 
Size: 10082 Color: 0
Size: 5882 Color: 2

Bin 171: 36 of cap free
Amount of items: 2
Items: 
Size: 13078 Color: 4
Size: 2886 Color: 0

Bin 172: 37 of cap free
Amount of items: 2
Items: 
Size: 8010 Color: 2
Size: 7953 Color: 0

Bin 173: 39 of cap free
Amount of items: 3
Items: 
Size: 9090 Color: 1
Size: 5837 Color: 3
Size: 1034 Color: 4

Bin 174: 42 of cap free
Amount of items: 2
Items: 
Size: 13478 Color: 3
Size: 2480 Color: 1

Bin 175: 45 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 0
Size: 1755 Color: 3

Bin 176: 46 of cap free
Amount of items: 2
Items: 
Size: 13796 Color: 4
Size: 2158 Color: 3

Bin 177: 49 of cap free
Amount of items: 2
Items: 
Size: 14098 Color: 4
Size: 1853 Color: 3

Bin 178: 51 of cap free
Amount of items: 2
Items: 
Size: 10883 Color: 3
Size: 5066 Color: 4

Bin 179: 51 of cap free
Amount of items: 2
Items: 
Size: 14181 Color: 2
Size: 1768 Color: 0

Bin 180: 52 of cap free
Amount of items: 2
Items: 
Size: 13701 Color: 1
Size: 2247 Color: 4

Bin 181: 60 of cap free
Amount of items: 2
Items: 
Size: 9272 Color: 0
Size: 6668 Color: 2

Bin 182: 60 of cap free
Amount of items: 2
Items: 
Size: 9922 Color: 3
Size: 6018 Color: 0

Bin 183: 71 of cap free
Amount of items: 2
Items: 
Size: 12805 Color: 4
Size: 3124 Color: 3

Bin 184: 71 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 3
Size: 2120 Color: 1

Bin 185: 74 of cap free
Amount of items: 2
Items: 
Size: 11758 Color: 2
Size: 4168 Color: 0

Bin 186: 74 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 1
Size: 2462 Color: 3

Bin 187: 81 of cap free
Amount of items: 3
Items: 
Size: 12908 Color: 3
Size: 1827 Color: 2
Size: 1184 Color: 2

Bin 188: 82 of cap free
Amount of items: 19
Items: 
Size: 1148 Color: 3
Size: 1144 Color: 0
Size: 1024 Color: 3
Size: 896 Color: 2
Size: 868 Color: 3
Size: 864 Color: 4
Size: 864 Color: 2
Size: 860 Color: 2
Size: 816 Color: 0
Size: 804 Color: 3
Size: 784 Color: 3
Size: 768 Color: 4
Size: 768 Color: 1
Size: 760 Color: 2
Size: 752 Color: 1
Size: 736 Color: 1
Size: 704 Color: 2
Size: 688 Color: 0
Size: 670 Color: 4

Bin 189: 84 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 0
Size: 3024 Color: 1

Bin 190: 101 of cap free
Amount of items: 2
Items: 
Size: 12463 Color: 4
Size: 3436 Color: 0

Bin 191: 102 of cap free
Amount of items: 33
Items: 
Size: 724 Color: 1
Size: 712 Color: 1
Size: 680 Color: 1
Size: 656 Color: 2
Size: 624 Color: 0
Size: 616 Color: 3
Size: 592 Color: 2
Size: 584 Color: 2
Size: 576 Color: 1
Size: 558 Color: 4
Size: 556 Color: 4
Size: 532 Color: 1
Size: 512 Color: 1
Size: 504 Color: 4
Size: 496 Color: 3
Size: 484 Color: 3
Size: 472 Color: 1
Size: 456 Color: 3
Size: 448 Color: 1
Size: 444 Color: 0
Size: 416 Color: 4
Size: 416 Color: 4
Size: 416 Color: 2
Size: 384 Color: 4
Size: 384 Color: 2
Size: 384 Color: 0
Size: 360 Color: 4
Size: 344 Color: 4
Size: 344 Color: 2
Size: 320 Color: 0
Size: 316 Color: 0
Size: 300 Color: 0
Size: 288 Color: 0

Bin 192: 106 of cap free
Amount of items: 2
Items: 
Size: 8942 Color: 0
Size: 6952 Color: 3

Bin 193: 130 of cap free
Amount of items: 3
Items: 
Size: 9106 Color: 2
Size: 5948 Color: 4
Size: 816 Color: 1

Bin 194: 138 of cap free
Amount of items: 2
Items: 
Size: 10060 Color: 3
Size: 5802 Color: 4

Bin 195: 178 of cap free
Amount of items: 2
Items: 
Size: 10674 Color: 2
Size: 5148 Color: 4

Bin 196: 194 of cap free
Amount of items: 2
Items: 
Size: 11444 Color: 0
Size: 4362 Color: 2

Bin 197: 209 of cap free
Amount of items: 2
Items: 
Size: 11263 Color: 2
Size: 4528 Color: 0

Bin 198: 255 of cap free
Amount of items: 2
Items: 
Size: 9785 Color: 2
Size: 5960 Color: 0

Bin 199: 12286 of cap free
Amount of items: 12
Items: 
Size: 412 Color: 1
Size: 340 Color: 2
Size: 336 Color: 4
Size: 326 Color: 4
Size: 318 Color: 2
Size: 308 Color: 4
Size: 302 Color: 3
Size: 280 Color: 1
Size: 280 Color: 0
Size: 272 Color: 2
Size: 272 Color: 0
Size: 268 Color: 0

Total size: 3168000
Total free space: 16000

