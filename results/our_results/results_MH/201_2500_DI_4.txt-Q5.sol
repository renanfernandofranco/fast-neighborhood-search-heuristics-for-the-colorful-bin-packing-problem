Capicity Bin: 2020
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1013 Color: 2
Size: 841 Color: 1
Size: 166 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 4
Size: 462 Color: 0
Size: 88 Color: 1

Bin 3: 0 of cap free
Amount of items: 6
Items: 
Size: 1674 Color: 4
Size: 182 Color: 3
Size: 56 Color: 3
Size: 44 Color: 0
Size: 40 Color: 0
Size: 24 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 2
Size: 218 Color: 0
Size: 40 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 4
Size: 310 Color: 2
Size: 60 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 3
Size: 711 Color: 2
Size: 142 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 578 Color: 1
Size: 52 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1195 Color: 1
Size: 689 Color: 3
Size: 136 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1501 Color: 0
Size: 433 Color: 2
Size: 86 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1017 Color: 3
Size: 837 Color: 3
Size: 166 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 2
Size: 311 Color: 1
Size: 60 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1182 Color: 4
Size: 702 Color: 0
Size: 136 Color: 3

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 1571 Color: 0
Size: 441 Color: 0
Size: 4 Color: 4
Size: 4 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 4
Size: 414 Color: 2
Size: 72 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 0
Size: 164 Color: 0
Size: 74 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 1
Size: 309 Color: 3
Size: 60 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 4
Size: 503 Color: 0
Size: 100 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 4
Size: 437 Color: 1
Size: 92 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 3
Size: 833 Color: 0
Size: 166 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1497 Color: 1
Size: 375 Color: 3
Size: 148 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 2
Size: 470 Color: 0
Size: 312 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 4
Size: 231 Color: 3
Size: 44 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 4
Size: 222 Color: 0
Size: 104 Color: 3

Bin 24: 0 of cap free
Amount of items: 4
Items: 
Size: 1114 Color: 3
Size: 654 Color: 0
Size: 224 Color: 0
Size: 28 Color: 4

Bin 25: 0 of cap free
Amount of items: 4
Items: 
Size: 1396 Color: 4
Size: 468 Color: 1
Size: 88 Color: 3
Size: 68 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 4
Size: 406 Color: 0
Size: 24 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 2
Size: 202 Color: 2
Size: 60 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 3
Size: 731 Color: 4
Size: 118 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 1
Size: 526 Color: 4
Size: 36 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 1
Size: 211 Color: 1
Size: 40 Color: 2

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 1268 Color: 2
Size: 676 Color: 3
Size: 44 Color: 1
Size: 32 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 1
Size: 416 Color: 2
Size: 274 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 2
Size: 189 Color: 0
Size: 36 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 4
Size: 128 Color: 0
Size: 86 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 2
Size: 831 Color: 0
Size: 164 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 3
Size: 334 Color: 1
Size: 64 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 3
Size: 267 Color: 4
Size: 52 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 910 Color: 3
Size: 820 Color: 0
Size: 290 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1505 Color: 2
Size: 439 Color: 3
Size: 76 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 0
Size: 383 Color: 2
Size: 76 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 2
Size: 607 Color: 4
Size: 120 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1030 Color: 0
Size: 758 Color: 4
Size: 232 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1011 Color: 1
Size: 841 Color: 2
Size: 168 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 2
Size: 200 Color: 1
Size: 40 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 3
Size: 112 Color: 4
Size: 96 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 0
Size: 349 Color: 2
Size: 68 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 0
Size: 242 Color: 2
Size: 44 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1014 Color: 1
Size: 826 Color: 4
Size: 180 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 2
Size: 249 Color: 3
Size: 4 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 2
Size: 591 Color: 4
Size: 118 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 2
Size: 273 Color: 3
Size: 50 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 4
Size: 277 Color: 4
Size: 54 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 2
Size: 359 Color: 4
Size: 70 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 0
Size: 378 Color: 4
Size: 72 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 2
Size: 509 Color: 3
Size: 100 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 2
Size: 227 Color: 2
Size: 2 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 2
Size: 233 Color: 1
Size: 46 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 2
Size: 245 Color: 2
Size: 48 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 4
Size: 355 Color: 2
Size: 70 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 0
Size: 237 Color: 4
Size: 46 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 0
Size: 351 Color: 0
Size: 70 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 4
Size: 241 Color: 0
Size: 46 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 4
Size: 379 Color: 4
Size: 74 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 3
Size: 589 Color: 4
Size: 116 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 2
Size: 273 Color: 4
Size: 54 Color: 3

Total size: 131300
Total free space: 0

