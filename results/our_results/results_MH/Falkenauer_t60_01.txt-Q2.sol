Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 334 Color: 1
Size: 270 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 277 Color: 1
Size: 261 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 304 Color: 1
Size: 300 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 297 Color: 1
Size: 256 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 307 Color: 0
Size: 282 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 361 Color: 1
Size: 257 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 269 Color: 0
Size: 258 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 319 Color: 0
Size: 305 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 280 Color: 0
Size: 252 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 333 Color: 1
Size: 255 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 366 Color: 0
Size: 265 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 267 Color: 0
Size: 258 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 314 Color: 1
Size: 260 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 308 Color: 0
Size: 289 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 275 Color: 0
Size: 260 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 347 Color: 0
Size: 251 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 340 Color: 1
Size: 251 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 339 Color: 1
Size: 262 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 313 Color: 1
Size: 264 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 302 Color: 1
Size: 254 Color: 1

Total size: 20000
Total free space: 0

