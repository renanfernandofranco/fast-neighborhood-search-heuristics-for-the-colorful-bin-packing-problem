Capicity Bin: 2036
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 684 Color: 9
Size: 472 Color: 12
Size: 424 Color: 17
Size: 228 Color: 14
Size: 76 Color: 16
Size: 64 Color: 13
Size: 48 Color: 17
Size: 40 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 18
Size: 194 Color: 1
Size: 36 Color: 6

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1498 Color: 9
Size: 450 Color: 14
Size: 64 Color: 12
Size: 20 Color: 15
Size: 4 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 18
Size: 295 Color: 6
Size: 58 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 19
Size: 391 Color: 2
Size: 78 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 2
Size: 849 Color: 18
Size: 168 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 14
Size: 581 Color: 7
Size: 116 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 8
Size: 278 Color: 15
Size: 52 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 17
Size: 395 Color: 14
Size: 78 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 8
Size: 591 Color: 8
Size: 118 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 14
Size: 606 Color: 15
Size: 40 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 0
Size: 334 Color: 17
Size: 64 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 10
Size: 382 Color: 8
Size: 76 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 6
Size: 717 Color: 19
Size: 142 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1752 Color: 17
Size: 244 Color: 14
Size: 40 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 19
Size: 402 Color: 13
Size: 76 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 17
Size: 214 Color: 7
Size: 40 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 1
Size: 361 Color: 3
Size: 72 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 15
Size: 522 Color: 7
Size: 100 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 2
Size: 294 Color: 17
Size: 56 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 4
Size: 701 Color: 10
Size: 138 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 5
Size: 366 Color: 17
Size: 72 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 12
Size: 311 Color: 11
Size: 46 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 17
Size: 493 Color: 9
Size: 4 Color: 5

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 7
Size: 678 Color: 1
Size: 132 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 4
Size: 201 Color: 7
Size: 38 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 19
Size: 242 Color: 4
Size: 48 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 16
Size: 846 Color: 11
Size: 168 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 12
Size: 218 Color: 3
Size: 40 Color: 11

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 17
Size: 417 Color: 13
Size: 48 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 19
Size: 289 Color: 14
Size: 48 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 10
Size: 178 Color: 6
Size: 32 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 6
Size: 491 Color: 11
Size: 98 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 0
Size: 243 Color: 4
Size: 48 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 11
Size: 482 Color: 4
Size: 96 Color: 11

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 12
Size: 323 Color: 17
Size: 64 Color: 16

Bin 37: 0 of cap free
Amount of items: 4
Items: 
Size: 1404 Color: 4
Size: 316 Color: 9
Size: 228 Color: 6
Size: 88 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 18
Size: 229 Color: 4
Size: 44 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 16
Size: 847 Color: 16
Size: 168 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 4
Size: 713 Color: 19
Size: 130 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 17
Size: 193 Color: 18
Size: 38 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 13
Size: 311 Color: 4
Size: 62 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 15
Size: 495 Color: 11
Size: 98 Color: 12

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 3
Size: 590 Color: 4
Size: 116 Color: 16

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 13
Size: 217 Color: 19
Size: 42 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 3
Size: 173 Color: 5
Size: 34 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 6
Size: 507 Color: 8
Size: 100 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1173 Color: 10
Size: 721 Color: 0
Size: 142 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1134 Color: 17
Size: 754 Color: 18
Size: 148 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 2
Size: 481 Color: 4
Size: 96 Color: 13

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 11
Size: 191 Color: 6
Size: 36 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 18
Size: 691 Color: 1
Size: 136 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 18
Size: 721 Color: 8
Size: 144 Color: 15

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 14
Size: 257 Color: 1
Size: 50 Color: 12

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 6
Size: 197 Color: 6
Size: 38 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 7
Size: 650 Color: 15
Size: 84 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 10
Size: 499 Color: 18
Size: 98 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 5
Size: 213 Color: 1
Size: 42 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 13
Size: 399 Color: 8
Size: 78 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 16
Size: 334 Color: 14
Size: 28 Color: 18

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 19
Size: 220 Color: 14
Size: 40 Color: 11

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 13
Size: 713 Color: 1
Size: 142 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 10
Size: 315 Color: 14
Size: 62 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 16
Size: 407 Color: 2
Size: 80 Color: 19

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 1280 Color: 17
Size: 756 Color: 14

Total size: 132340
Total free space: 0

