Capicity Bin: 19296
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 9650 Color: 15
Size: 1720 Color: 12
Size: 1638 Color: 4
Size: 1612 Color: 2
Size: 1600 Color: 7
Size: 1388 Color: 19
Size: 1072 Color: 8
Size: 616 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 9
Size: 8016 Color: 12
Size: 1606 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11760 Color: 16
Size: 7232 Color: 0
Size: 304 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 2
Size: 7288 Color: 1
Size: 224 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12739 Color: 12
Size: 6225 Color: 8
Size: 332 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 15
Size: 6264 Color: 1
Size: 224 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 15
Size: 5336 Color: 16
Size: 1120 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 14
Size: 5928 Color: 8
Size: 464 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 7
Size: 5296 Color: 5
Size: 896 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 14
Size: 5362 Color: 2
Size: 754 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 6
Size: 4273 Color: 18
Size: 864 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 15
Size: 4648 Color: 9
Size: 468 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 4
Size: 3812 Color: 19
Size: 364 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15138 Color: 19
Size: 3824 Color: 17
Size: 334 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15224 Color: 18
Size: 2896 Color: 14
Size: 1176 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15554 Color: 15
Size: 3312 Color: 7
Size: 430 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15656 Color: 17
Size: 3048 Color: 7
Size: 592 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15661 Color: 17
Size: 3031 Color: 2
Size: 604 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15788 Color: 2
Size: 2196 Color: 12
Size: 1312 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15816 Color: 0
Size: 1844 Color: 17
Size: 1636 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15982 Color: 19
Size: 1708 Color: 7
Size: 1606 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16100 Color: 16
Size: 2352 Color: 10
Size: 844 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16142 Color: 5
Size: 2604 Color: 16
Size: 550 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16166 Color: 0
Size: 2026 Color: 13
Size: 1104 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16208 Color: 17
Size: 2736 Color: 18
Size: 352 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16420 Color: 19
Size: 1868 Color: 13
Size: 1008 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16434 Color: 12
Size: 2190 Color: 10
Size: 672 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 19
Size: 1834 Color: 3
Size: 966 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16500 Color: 6
Size: 1964 Color: 7
Size: 832 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16507 Color: 4
Size: 1721 Color: 16
Size: 1068 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16648 Color: 18
Size: 1680 Color: 18
Size: 968 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16653 Color: 13
Size: 2203 Color: 3
Size: 440 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16703 Color: 16
Size: 1989 Color: 13
Size: 604 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16856 Color: 17
Size: 1912 Color: 11
Size: 528 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16900 Color: 4
Size: 1244 Color: 9
Size: 1152 Color: 16

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16917 Color: 8
Size: 1803 Color: 10
Size: 576 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16932 Color: 10
Size: 1972 Color: 16
Size: 392 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17007 Color: 11
Size: 1765 Color: 2
Size: 524 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17008 Color: 5
Size: 1832 Color: 17
Size: 456 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 17
Size: 1368 Color: 4
Size: 912 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17098 Color: 13
Size: 1344 Color: 5
Size: 854 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17112 Color: 5
Size: 1808 Color: 7
Size: 376 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17163 Color: 7
Size: 1779 Color: 19
Size: 354 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17188 Color: 4
Size: 1764 Color: 16
Size: 344 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17242 Color: 2
Size: 1720 Color: 13
Size: 334 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17231 Color: 16
Size: 1673 Color: 18
Size: 392 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17252 Color: 5
Size: 1604 Color: 15
Size: 440 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17297 Color: 8
Size: 1667 Color: 10
Size: 332 Color: 5

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17308 Color: 13
Size: 1604 Color: 12
Size: 384 Color: 17

Bin 50: 1 of cap free
Amount of items: 5
Items: 
Size: 12784 Color: 0
Size: 4227 Color: 8
Size: 1600 Color: 9
Size: 348 Color: 4
Size: 336 Color: 6

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 9
Size: 4385 Color: 1
Size: 1248 Color: 8

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 14666 Color: 6
Size: 4221 Color: 8
Size: 408 Color: 9

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 15578 Color: 4
Size: 3025 Color: 8
Size: 692 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 15997 Color: 16
Size: 3138 Color: 13
Size: 160 Color: 11

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 16245 Color: 18
Size: 2386 Color: 4
Size: 664 Color: 16

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 16538 Color: 3
Size: 2757 Color: 13

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 16948 Color: 17
Size: 1827 Color: 19
Size: 520 Color: 4

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 16970 Color: 2
Size: 2325 Color: 10

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 17084 Color: 5
Size: 2211 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 17105 Color: 17
Size: 1936 Color: 16
Size: 254 Color: 11

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 17136 Color: 15
Size: 2159 Color: 2

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 17340 Color: 8
Size: 1955 Color: 18

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 16
Size: 8034 Color: 12
Size: 1604 Color: 12

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 9659 Color: 11
Size: 8035 Color: 19
Size: 1600 Color: 14

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 3
Size: 4624 Color: 0
Size: 678 Color: 19

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 14197 Color: 9
Size: 4805 Color: 10
Size: 292 Color: 4

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 14225 Color: 18
Size: 4281 Color: 18
Size: 788 Color: 0

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 15534 Color: 13
Size: 3760 Color: 3

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 15540 Color: 2
Size: 3400 Color: 15
Size: 354 Color: 7

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 15965 Color: 8
Size: 3329 Color: 7

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 16172 Color: 1
Size: 3122 Color: 18

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 16370 Color: 13
Size: 2924 Color: 6

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 16456 Color: 1
Size: 2442 Color: 7
Size: 396 Color: 16

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 16742 Color: 15
Size: 1312 Color: 13
Size: 1240 Color: 19

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 16866 Color: 6
Size: 1660 Color: 9
Size: 768 Color: 18

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 17060 Color: 8
Size: 2234 Color: 13

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 17334 Color: 14
Size: 1096 Color: 16
Size: 864 Color: 11

Bin 78: 3 of cap free
Amount of items: 7
Items: 
Size: 9651 Color: 17
Size: 2332 Color: 9
Size: 2020 Color: 13
Size: 1758 Color: 15
Size: 1748 Color: 18
Size: 1432 Color: 4
Size: 352 Color: 3

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 13531 Color: 4
Size: 5762 Color: 16

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 15625 Color: 16
Size: 2900 Color: 1
Size: 768 Color: 19

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 15898 Color: 13
Size: 3395 Color: 5

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 17289 Color: 19
Size: 2004 Color: 18

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 14
Size: 4268 Color: 4
Size: 1782 Color: 12

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 6
Size: 3848 Color: 2
Size: 368 Color: 18

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 13
Size: 3132 Color: 16
Size: 340 Color: 9

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 15989 Color: 13
Size: 2343 Color: 11
Size: 960 Color: 8

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 16485 Color: 1
Size: 2751 Color: 12
Size: 56 Color: 9

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 16668 Color: 18
Size: 2104 Color: 4
Size: 520 Color: 14

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 17162 Color: 19
Size: 2130 Color: 10

Bin 90: 5 of cap free
Amount of items: 3
Items: 
Size: 15108 Color: 5
Size: 3773 Color: 15
Size: 410 Color: 19

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 16279 Color: 12
Size: 2580 Color: 3
Size: 432 Color: 9

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 16675 Color: 13
Size: 2616 Color: 10

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 16776 Color: 10
Size: 2515 Color: 18

Bin 94: 6 of cap free
Amount of items: 3
Items: 
Size: 10384 Color: 15
Size: 8026 Color: 12
Size: 880 Color: 11

Bin 95: 6 of cap free
Amount of items: 2
Items: 
Size: 16184 Color: 3
Size: 3106 Color: 13

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 17296 Color: 18
Size: 1994 Color: 15

Bin 97: 7 of cap free
Amount of items: 2
Items: 
Size: 15488 Color: 1
Size: 3801 Color: 19

Bin 98: 7 of cap free
Amount of items: 2
Items: 
Size: 15605 Color: 9
Size: 3684 Color: 11

Bin 99: 7 of cap free
Amount of items: 2
Items: 
Size: 17204 Color: 14
Size: 2085 Color: 9

Bin 100: 8 of cap free
Amount of items: 3
Items: 
Size: 10594 Color: 7
Size: 7254 Color: 13
Size: 1440 Color: 14

Bin 101: 8 of cap free
Amount of items: 3
Items: 
Size: 10568 Color: 15
Size: 8384 Color: 1
Size: 336 Color: 17

Bin 102: 8 of cap free
Amount of items: 3
Items: 
Size: 14773 Color: 3
Size: 2630 Color: 14
Size: 1885 Color: 2

Bin 103: 8 of cap free
Amount of items: 3
Items: 
Size: 15124 Color: 13
Size: 3796 Color: 4
Size: 368 Color: 19

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 16048 Color: 10
Size: 3240 Color: 2

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 16474 Color: 4
Size: 2814 Color: 5

Bin 106: 9 of cap free
Amount of items: 9
Items: 
Size: 9649 Color: 11
Size: 1600 Color: 7
Size: 1448 Color: 9
Size: 1376 Color: 6
Size: 1250 Color: 11
Size: 1248 Color: 9
Size: 1224 Color: 18
Size: 1056 Color: 0
Size: 436 Color: 8

Bin 107: 9 of cap free
Amount of items: 3
Items: 
Size: 12713 Color: 7
Size: 6222 Color: 4
Size: 352 Color: 5

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 16911 Color: 8
Size: 2376 Color: 7

Bin 109: 9 of cap free
Amount of items: 2
Items: 
Size: 17139 Color: 12
Size: 2148 Color: 17

Bin 110: 10 of cap free
Amount of items: 2
Items: 
Size: 15840 Color: 6
Size: 3446 Color: 7

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 17190 Color: 11
Size: 2096 Color: 1

Bin 112: 11 of cap free
Amount of items: 2
Items: 
Size: 13820 Color: 13
Size: 5465 Color: 8

Bin 113: 12 of cap free
Amount of items: 40
Items: 
Size: 704 Color: 5
Size: 688 Color: 10
Size: 640 Color: 13
Size: 624 Color: 4
Size: 620 Color: 7
Size: 608 Color: 5
Size: 560 Color: 16
Size: 554 Color: 3
Size: 548 Color: 15
Size: 544 Color: 16
Size: 512 Color: 16
Size: 512 Color: 13
Size: 512 Color: 10
Size: 504 Color: 19
Size: 502 Color: 15
Size: 496 Color: 17
Size: 484 Color: 9
Size: 480 Color: 9
Size: 476 Color: 3
Size: 464 Color: 18
Size: 464 Color: 17
Size: 464 Color: 15
Size: 448 Color: 18
Size: 436 Color: 12
Size: 432 Color: 17
Size: 432 Color: 1
Size: 424 Color: 13
Size: 424 Color: 7
Size: 424 Color: 3
Size: 416 Color: 19
Size: 416 Color: 19
Size: 416 Color: 11
Size: 404 Color: 0
Size: 400 Color: 4
Size: 396 Color: 8
Size: 396 Color: 2
Size: 384 Color: 8
Size: 368 Color: 4
Size: 364 Color: 0
Size: 344 Color: 0

Bin 114: 12 of cap free
Amount of items: 3
Items: 
Size: 9658 Color: 16
Size: 9324 Color: 5
Size: 302 Color: 17

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 16
Size: 5348 Color: 5

Bin 116: 12 of cap free
Amount of items: 2
Items: 
Size: 15420 Color: 19
Size: 3864 Color: 9

Bin 117: 12 of cap free
Amount of items: 2
Items: 
Size: 16204 Color: 5
Size: 3080 Color: 16

Bin 118: 12 of cap free
Amount of items: 2
Items: 
Size: 16616 Color: 2
Size: 2668 Color: 18

Bin 119: 13 of cap free
Amount of items: 2
Items: 
Size: 16707 Color: 3
Size: 2576 Color: 17

Bin 120: 13 of cap free
Amount of items: 2
Items: 
Size: 17035 Color: 15
Size: 2248 Color: 9

Bin 121: 14 of cap free
Amount of items: 3
Items: 
Size: 14925 Color: 9
Size: 3643 Color: 4
Size: 714 Color: 8

Bin 122: 14 of cap free
Amount of items: 3
Items: 
Size: 15303 Color: 2
Size: 2339 Color: 8
Size: 1640 Color: 19

Bin 123: 14 of cap free
Amount of items: 2
Items: 
Size: 15752 Color: 11
Size: 3530 Color: 9

Bin 124: 14 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 14
Size: 2216 Color: 5
Size: 64 Color: 17

Bin 125: 16 of cap free
Amount of items: 3
Items: 
Size: 13428 Color: 15
Size: 5384 Color: 3
Size: 468 Color: 0

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 18
Size: 2912 Color: 12

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 16670 Color: 6
Size: 2610 Color: 9

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 16876 Color: 10
Size: 2404 Color: 3

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 17156 Color: 17
Size: 2124 Color: 9

Bin 130: 16 of cap free
Amount of items: 2
Items: 
Size: 17240 Color: 3
Size: 2040 Color: 19

Bin 131: 17 of cap free
Amount of items: 2
Items: 
Size: 14448 Color: 19
Size: 4831 Color: 16

Bin 132: 18 of cap free
Amount of items: 2
Items: 
Size: 16678 Color: 4
Size: 2600 Color: 15

Bin 133: 18 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 17
Size: 2302 Color: 13

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 17364 Color: 2
Size: 1914 Color: 3

Bin 135: 19 of cap free
Amount of items: 3
Items: 
Size: 14769 Color: 15
Size: 3416 Color: 0
Size: 1092 Color: 18

Bin 136: 22 of cap free
Amount of items: 2
Items: 
Size: 16906 Color: 10
Size: 2368 Color: 5

Bin 137: 24 of cap free
Amount of items: 2
Items: 
Size: 16816 Color: 0
Size: 2456 Color: 11

Bin 138: 24 of cap free
Amount of items: 2
Items: 
Size: 17336 Color: 3
Size: 1936 Color: 2

Bin 139: 25 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 11
Size: 6251 Color: 0
Size: 1072 Color: 10

Bin 140: 25 of cap free
Amount of items: 2
Items: 
Size: 15223 Color: 9
Size: 4048 Color: 12

Bin 141: 26 of cap free
Amount of items: 3
Items: 
Size: 11350 Color: 5
Size: 7600 Color: 15
Size: 320 Color: 9

Bin 142: 26 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 13
Size: 6888 Color: 14

Bin 143: 26 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 0
Size: 4808 Color: 16
Size: 508 Color: 15

Bin 144: 26 of cap free
Amount of items: 2
Items: 
Size: 16168 Color: 12
Size: 3102 Color: 3

Bin 145: 28 of cap free
Amount of items: 3
Items: 
Size: 15688 Color: 6
Size: 3484 Color: 7
Size: 96 Color: 11

Bin 146: 29 of cap free
Amount of items: 2
Items: 
Size: 16724 Color: 18
Size: 2543 Color: 16

Bin 147: 31 of cap free
Amount of items: 2
Items: 
Size: 16488 Color: 3
Size: 2777 Color: 2

Bin 148: 33 of cap free
Amount of items: 2
Items: 
Size: 13776 Color: 18
Size: 5487 Color: 3

Bin 149: 33 of cap free
Amount of items: 2
Items: 
Size: 17179 Color: 6
Size: 2084 Color: 1

Bin 150: 34 of cap free
Amount of items: 23
Items: 
Size: 1112 Color: 16
Size: 1056 Color: 18
Size: 1056 Color: 0
Size: 1024 Color: 7
Size: 976 Color: 17
Size: 960 Color: 7
Size: 936 Color: 16
Size: 912 Color: 8
Size: 888 Color: 16
Size: 864 Color: 1
Size: 856 Color: 1
Size: 800 Color: 19
Size: 792 Color: 7
Size: 768 Color: 6
Size: 768 Color: 4
Size: 758 Color: 11
Size: 752 Color: 8
Size: 736 Color: 6
Size: 720 Color: 10
Size: 688 Color: 13
Size: 640 Color: 19
Size: 624 Color: 15
Size: 576 Color: 3

Bin 151: 35 of cap free
Amount of items: 2
Items: 
Size: 15922 Color: 2
Size: 3339 Color: 0

Bin 152: 37 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 17
Size: 4851 Color: 13
Size: 672 Color: 4

Bin 153: 41 of cap free
Amount of items: 2
Items: 
Size: 17272 Color: 13
Size: 1983 Color: 7

Bin 154: 44 of cap free
Amount of items: 2
Items: 
Size: 16016 Color: 17
Size: 3236 Color: 13

Bin 155: 46 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 4
Size: 9226 Color: 6
Size: 352 Color: 9

Bin 156: 51 of cap free
Amount of items: 2
Items: 
Size: 16756 Color: 6
Size: 2489 Color: 2

Bin 157: 52 of cap free
Amount of items: 2
Items: 
Size: 14546 Color: 12
Size: 4698 Color: 9

Bin 158: 52 of cap free
Amount of items: 2
Items: 
Size: 14748 Color: 15
Size: 4496 Color: 5

Bin 159: 53 of cap free
Amount of items: 2
Items: 
Size: 16795 Color: 4
Size: 2448 Color: 0

Bin 160: 56 of cap free
Amount of items: 2
Items: 
Size: 13116 Color: 17
Size: 6124 Color: 16

Bin 161: 57 of cap free
Amount of items: 2
Items: 
Size: 13499 Color: 7
Size: 5740 Color: 13

Bin 162: 60 of cap free
Amount of items: 3
Items: 
Size: 11344 Color: 0
Size: 7524 Color: 14
Size: 368 Color: 12

Bin 163: 64 of cap free
Amount of items: 2
Items: 
Size: 11188 Color: 4
Size: 8044 Color: 12

Bin 164: 64 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 18
Size: 6390 Color: 6

Bin 165: 64 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 10
Size: 6288 Color: 18

Bin 166: 66 of cap free
Amount of items: 2
Items: 
Size: 10961 Color: 18
Size: 8269 Color: 0

Bin 167: 68 of cap free
Amount of items: 2
Items: 
Size: 12588 Color: 4
Size: 6640 Color: 17

Bin 168: 70 of cap free
Amount of items: 3
Items: 
Size: 15667 Color: 15
Size: 3467 Color: 0
Size: 92 Color: 4

Bin 169: 72 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 10
Size: 5456 Color: 18

Bin 170: 72 of cap free
Amount of items: 4
Items: 
Size: 14664 Color: 19
Size: 3056 Color: 13
Size: 1184 Color: 16
Size: 320 Color: 18

Bin 171: 72 of cap free
Amount of items: 2
Items: 
Size: 15062 Color: 1
Size: 4162 Color: 17

Bin 172: 74 of cap free
Amount of items: 4
Items: 
Size: 9655 Color: 15
Size: 8039 Color: 0
Size: 1056 Color: 3
Size: 472 Color: 7

Bin 173: 76 of cap free
Amount of items: 3
Items: 
Size: 10668 Color: 1
Size: 8040 Color: 9
Size: 512 Color: 7

Bin 174: 81 of cap free
Amount of items: 2
Items: 
Size: 14169 Color: 2
Size: 5046 Color: 19

Bin 175: 81 of cap free
Amount of items: 2
Items: 
Size: 16311 Color: 15
Size: 2904 Color: 17

Bin 176: 90 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 11
Size: 3862 Color: 13

Bin 177: 92 of cap free
Amount of items: 2
Items: 
Size: 15208 Color: 12
Size: 3996 Color: 13

Bin 178: 97 of cap free
Amount of items: 2
Items: 
Size: 10695 Color: 19
Size: 8504 Color: 5

Bin 179: 102 of cap free
Amount of items: 2
Items: 
Size: 14302 Color: 18
Size: 4892 Color: 8

Bin 180: 104 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 1
Size: 5168 Color: 16

Bin 181: 105 of cap free
Amount of items: 2
Items: 
Size: 14737 Color: 11
Size: 4454 Color: 14

Bin 182: 108 of cap free
Amount of items: 2
Items: 
Size: 14508 Color: 13
Size: 4680 Color: 11

Bin 183: 113 of cap free
Amount of items: 3
Items: 
Size: 9660 Color: 0
Size: 7169 Color: 16
Size: 2354 Color: 2

Bin 184: 116 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 7
Size: 4564 Color: 10
Size: 928 Color: 15

Bin 185: 121 of cap free
Amount of items: 3
Items: 
Size: 11827 Color: 10
Size: 6764 Color: 18
Size: 584 Color: 12

Bin 186: 126 of cap free
Amount of items: 2
Items: 
Size: 9666 Color: 14
Size: 9504 Color: 9

Bin 187: 136 of cap free
Amount of items: 2
Items: 
Size: 14736 Color: 12
Size: 4424 Color: 4

Bin 188: 138 of cap free
Amount of items: 5
Items: 
Size: 9652 Color: 17
Size: 2890 Color: 17
Size: 2720 Color: 1
Size: 2182 Color: 5
Size: 1714 Color: 8

Bin 189: 149 of cap free
Amount of items: 3
Items: 
Size: 11795 Color: 6
Size: 6624 Color: 18
Size: 728 Color: 15

Bin 190: 165 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 8
Size: 6947 Color: 10

Bin 191: 166 of cap free
Amount of items: 3
Items: 
Size: 12866 Color: 12
Size: 5416 Color: 10
Size: 848 Color: 9

Bin 192: 172 of cap free
Amount of items: 2
Items: 
Size: 15162 Color: 19
Size: 3962 Color: 10

Bin 193: 180 of cap free
Amount of items: 3
Items: 
Size: 11834 Color: 10
Size: 6770 Color: 11
Size: 512 Color: 9

Bin 194: 198 of cap free
Amount of items: 2
Items: 
Size: 15632 Color: 4
Size: 3466 Color: 10

Bin 195: 208 of cap free
Amount of items: 2
Items: 
Size: 14680 Color: 10
Size: 4408 Color: 12

Bin 196: 222 of cap free
Amount of items: 2
Items: 
Size: 11032 Color: 8
Size: 8042 Color: 11

Bin 197: 223 of cap free
Amount of items: 2
Items: 
Size: 13477 Color: 10
Size: 5596 Color: 4

Bin 198: 253 of cap free
Amount of items: 2
Items: 
Size: 9680 Color: 14
Size: 9363 Color: 2

Bin 199: 13392 of cap free
Amount of items: 17
Items: 
Size: 416 Color: 3
Size: 400 Color: 15
Size: 396 Color: 18
Size: 384 Color: 11
Size: 384 Color: 10
Size: 380 Color: 19
Size: 352 Color: 4
Size: 352 Color: 2
Size: 344 Color: 14
Size: 328 Color: 17
Size: 324 Color: 12
Size: 320 Color: 13
Size: 320 Color: 12
Size: 320 Color: 0
Size: 312 Color: 9
Size: 288 Color: 11
Size: 284 Color: 8

Total size: 3820608
Total free space: 19296

