Capicity Bin: 2328
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1152 Color: 16
Size: 348 Color: 17
Size: 324 Color: 18
Size: 188 Color: 12
Size: 184 Color: 17
Size: 104 Color: 6
Size: 28 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 13
Size: 969 Color: 12
Size: 192 Color: 2

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1266 Color: 8
Size: 886 Color: 17
Size: 128 Color: 8
Size: 28 Color: 7
Size: 20 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 15
Size: 262 Color: 0
Size: 52 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 10
Size: 578 Color: 5
Size: 40 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 11
Size: 291 Color: 19
Size: 58 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 7
Size: 631 Color: 18
Size: 124 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 15
Size: 947 Color: 13
Size: 56 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 441 Color: 2
Size: 88 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1322 Color: 8
Size: 850 Color: 15
Size: 156 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 5
Size: 554 Color: 5
Size: 108 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 19
Size: 654 Color: 8
Size: 128 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 13
Size: 455 Color: 6
Size: 90 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 3
Size: 202 Color: 17
Size: 40 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 6
Size: 394 Color: 1
Size: 76 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 7
Size: 323 Color: 2
Size: 64 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 5
Size: 198 Color: 16
Size: 36 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 6
Size: 533 Color: 10
Size: 106 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 9
Size: 346 Color: 1
Size: 28 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1929 Color: 17
Size: 333 Color: 6
Size: 66 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 15
Size: 630 Color: 4
Size: 124 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 15
Size: 971 Color: 6
Size: 192 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 17
Size: 459 Color: 19
Size: 90 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 3
Size: 537 Color: 13
Size: 106 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 16
Size: 802 Color: 7
Size: 156 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 18
Size: 738 Color: 8
Size: 144 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 18
Size: 246 Color: 19
Size: 48 Color: 13

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 1218 Color: 18
Size: 926 Color: 16
Size: 144 Color: 5
Size: 40 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 15
Size: 471 Color: 10
Size: 92 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 12
Size: 210 Color: 8
Size: 40 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 15
Size: 741 Color: 1
Size: 120 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 17
Size: 462 Color: 11
Size: 88 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 7
Size: 835 Color: 15
Size: 166 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 17
Size: 342 Color: 0
Size: 64 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 6
Size: 298 Color: 2
Size: 56 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 11
Size: 553 Color: 1
Size: 110 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 3
Size: 645 Color: 9
Size: 128 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1871 Color: 17
Size: 381 Color: 11
Size: 76 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 18
Size: 451 Color: 7
Size: 90 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 3
Size: 646 Color: 1
Size: 128 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 5
Size: 478 Color: 7
Size: 92 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 302 Color: 0
Size: 56 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 2
Size: 723 Color: 11
Size: 144 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 6
Size: 466 Color: 12
Size: 48 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 4
Size: 565 Color: 19
Size: 112 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1861 Color: 7
Size: 391 Color: 12
Size: 76 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 11
Size: 531 Color: 4
Size: 104 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 0
Size: 258 Color: 12
Size: 48 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 17
Size: 469 Color: 5
Size: 4 Color: 10

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1016 Color: 13
Size: 864 Color: 12
Size: 448 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 10
Size: 538 Color: 2
Size: 96 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 13
Size: 322 Color: 15
Size: 64 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 12
Size: 770 Color: 8
Size: 152 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 17
Size: 282 Color: 8
Size: 52 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 14
Size: 829 Color: 9
Size: 164 Color: 7

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1864 Color: 3
Size: 288 Color: 18
Size: 176 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 18
Size: 561 Color: 17
Size: 94 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 10
Size: 970 Color: 1
Size: 192 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1854 Color: 1
Size: 398 Color: 3
Size: 76 Color: 19

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 14
Size: 226 Color: 10
Size: 44 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 18
Size: 362 Color: 19
Size: 68 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 9
Size: 534 Color: 17
Size: 104 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 10
Size: 341 Color: 4
Size: 66 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 5
Size: 337 Color: 4
Size: 66 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1999 Color: 16
Size: 275 Color: 5
Size: 54 Color: 4

Total size: 151320
Total free space: 0

