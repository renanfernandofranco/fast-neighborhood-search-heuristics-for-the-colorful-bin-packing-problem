Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 275 Color: 0
Size: 253 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 281 Color: 1
Size: 269 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 253 Color: 1
Size: 262 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 0
Size: 343 Color: 0
Size: 313 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 311 Color: 0
Size: 288 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 350 Color: 1
Size: 273 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 276 Color: 0
Size: 261 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 0
Size: 250 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 347 Color: 1
Size: 288 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 354 Color: 0
Size: 268 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 256 Color: 1
Size: 256 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 329 Color: 1
Size: 288 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 0
Size: 252 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 320 Color: 0
Size: 258 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 360 Color: 1
Size: 268 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 295 Color: 1
Size: 262 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 271 Color: 1
Size: 264 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 349 Color: 1
Size: 266 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 291 Color: 1
Size: 280 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 256 Color: 0
Size: 251 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 258 Color: 1
Size: 251 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 325 Color: 1
Size: 263 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 305 Color: 1
Size: 300 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 338 Color: 0
Size: 262 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 310 Color: 1
Size: 266 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 287 Color: 1
Size: 264 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 308 Color: 0
Size: 292 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 301 Color: 1
Size: 264 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 277 Color: 0
Size: 267 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 296 Color: 1
Size: 296 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 275 Color: 0
Size: 260 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 326 Color: 0
Size: 275 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 291 Color: 1
Size: 266 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 255 Color: 1
Size: 252 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 361 Color: 1
Size: 265 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 310 Color: 1
Size: 297 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 251 Color: 1
Size: 372 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 0
Size: 337 Color: 1
Size: 308 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 332 Color: 0
Size: 268 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 261 Color: 1
Size: 256 Color: 1

Total size: 40000
Total free space: 0

