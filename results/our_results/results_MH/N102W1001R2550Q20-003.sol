Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 3
Size: 251 Color: 15
Size: 250 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 18
Size: 269 Color: 6
Size: 259 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 9
Size: 328 Color: 16
Size: 262 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 3
Size: 346 Color: 3
Size: 296 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 13
Size: 280 Color: 3
Size: 257 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 265 Color: 14
Size: 254 Color: 4
Size: 482 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 346 Color: 5
Size: 266 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 18
Size: 361 Color: 19
Size: 274 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 320 Color: 9
Size: 304 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 14
Size: 296 Color: 18
Size: 266 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 10
Size: 253 Color: 6
Size: 251 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 10
Size: 329 Color: 1
Size: 312 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 17
Size: 301 Color: 7
Size: 267 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7
Size: 347 Color: 16
Size: 286 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9
Size: 255 Color: 12
Size: 254 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 2
Size: 342 Color: 6
Size: 307 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 304 Color: 16
Size: 301 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 327 Color: 15
Size: 250 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 16
Size: 275 Color: 0
Size: 256 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 13
Size: 313 Color: 2
Size: 279 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 15
Size: 313 Color: 8
Size: 279 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 16
Size: 301 Color: 14
Size: 297 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 19
Size: 257 Color: 4
Size: 253 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 13
Size: 270 Color: 16
Size: 260 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9
Size: 276 Color: 7
Size: 254 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9
Size: 266 Color: 2
Size: 251 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 331 Color: 5
Size: 280 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 19
Size: 302 Color: 19
Size: 277 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 308 Color: 16
Size: 284 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 285 Color: 14
Size: 263 Color: 1
Size: 453 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 19
Size: 322 Color: 15
Size: 266 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 11
Size: 321 Color: 0
Size: 277 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 9
Size: 316 Color: 1
Size: 258 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 326 Color: 15
Size: 299 Color: 12

Total size: 34034
Total free space: 0

