Capicity Bin: 2456
Lower Bound: 65

Bins used: 65
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1442 Color: 19
Size: 484 Color: 18
Size: 358 Color: 9
Size: 172 Color: 4

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 2172 Color: 7
Size: 96 Color: 19
Size: 96 Color: 18
Size: 92 Color: 2

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 2030 Color: 13
Size: 204 Color: 19
Size: 170 Color: 1
Size: 52 Color: 2

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 2170 Color: 9
Size: 146 Color: 11
Size: 88 Color: 8
Size: 52 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 13
Size: 806 Color: 15
Size: 160 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 18
Size: 1045 Color: 5
Size: 182 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 16
Size: 604 Color: 10
Size: 120 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 9
Size: 576 Color: 4
Size: 68 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 15
Size: 1018 Color: 9
Size: 200 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 11
Size: 449 Color: 13
Size: 88 Color: 9

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1425 Color: 19
Size: 739 Color: 18
Size: 244 Color: 9
Size: 48 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 7
Size: 954 Color: 10
Size: 272 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 0
Size: 266 Color: 0
Size: 52 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 2
Size: 504 Color: 0
Size: 76 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 6
Size: 262 Color: 11
Size: 64 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 9
Size: 342 Color: 5
Size: 116 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 19
Size: 413 Color: 1
Size: 82 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1402 Color: 10
Size: 1022 Color: 19
Size: 32 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 4
Size: 234 Color: 14
Size: 12 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 8
Size: 611 Color: 19
Size: 120 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 3
Size: 718 Color: 18
Size: 140 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 18
Size: 330 Color: 13
Size: 64 Color: 17

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 2180 Color: 17
Size: 236 Color: 14
Size: 32 Color: 5
Size: 8 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 0
Size: 382 Color: 10
Size: 104 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 6
Size: 308 Color: 17
Size: 48 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 15
Size: 843 Color: 6
Size: 168 Color: 14

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 1571 Color: 19
Size: 861 Color: 10
Size: 16 Color: 2
Size: 8 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2156 Color: 3
Size: 252 Color: 16
Size: 48 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 15
Size: 513 Color: 13
Size: 102 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 19
Size: 585 Color: 2
Size: 50 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 9
Size: 514 Color: 3
Size: 100 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 13
Size: 404 Color: 2
Size: 72 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 6
Size: 1021 Color: 4
Size: 202 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 12
Size: 364 Color: 17
Size: 72 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 12
Size: 434 Color: 15
Size: 272 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 984 Color: 16
Size: 882 Color: 7
Size: 590 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 15
Size: 678 Color: 19
Size: 132 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2046 Color: 4
Size: 282 Color: 14
Size: 128 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 18
Size: 772 Color: 5
Size: 40 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1869 Color: 6
Size: 491 Color: 7
Size: 96 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1688 Color: 6
Size: 684 Color: 16
Size: 84 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 7
Size: 526 Color: 9
Size: 40 Color: 16

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 19
Size: 637 Color: 1
Size: 126 Color: 15

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 8
Size: 226 Color: 1
Size: 28 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 16
Size: 274 Color: 19
Size: 60 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 8
Size: 777 Color: 12
Size: 88 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 7
Size: 570 Color: 3
Size: 112 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 8
Size: 540 Color: 6
Size: 384 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 6
Size: 1020 Color: 2
Size: 200 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 12
Size: 444 Color: 14
Size: 80 Color: 15

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 8
Size: 522 Color: 8
Size: 100 Color: 10

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 19
Size: 884 Color: 3
Size: 176 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 19
Size: 244 Color: 11
Size: 48 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 18
Size: 654 Color: 14
Size: 116 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 7
Size: 874 Color: 6
Size: 172 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 4
Size: 840 Color: 15
Size: 80 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 11
Size: 332 Color: 7
Size: 64 Color: 15

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 7
Size: 406 Color: 2
Size: 152 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 10
Size: 669 Color: 15
Size: 132 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 2
Size: 242 Color: 2
Size: 68 Color: 8

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 4
Size: 298 Color: 3
Size: 56 Color: 11

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 17
Size: 474 Color: 11
Size: 44 Color: 18

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 16
Size: 1007 Color: 12
Size: 200 Color: 18

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 6
Size: 613 Color: 11
Size: 122 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 1
Size: 621 Color: 3
Size: 122 Color: 11

Total size: 159640
Total free space: 0

