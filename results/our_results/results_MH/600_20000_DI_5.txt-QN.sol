Capicity Bin: 15712
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11342 Color: 466
Size: 3602 Color: 335
Size: 768 Color: 149

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 471
Size: 4132 Color: 351
Size: 164 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11850 Color: 478
Size: 3592 Color: 333
Size: 270 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 479
Size: 3220 Color: 324
Size: 640 Color: 133

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11908 Color: 482
Size: 3592 Color: 334
Size: 212 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11929 Color: 485
Size: 3453 Color: 329
Size: 330 Color: 43

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12134 Color: 492
Size: 3160 Color: 320
Size: 418 Color: 78

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12150 Color: 493
Size: 3234 Color: 326
Size: 328 Color: 42

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 495
Size: 3148 Color: 319
Size: 380 Color: 60

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12264 Color: 496
Size: 3128 Color: 317
Size: 320 Color: 40

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 498
Size: 2804 Color: 306
Size: 560 Color: 115

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12386 Color: 499
Size: 1770 Color: 238
Size: 1556 Color: 222

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 500
Size: 2762 Color: 304
Size: 552 Color: 113

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12413 Color: 501
Size: 2691 Color: 302
Size: 608 Color: 123

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12424 Color: 502
Size: 2952 Color: 309
Size: 336 Color: 45

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 508
Size: 2094 Color: 263
Size: 1024 Color: 179

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12610 Color: 509
Size: 1564 Color: 223
Size: 1538 Color: 221

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12676 Color: 511
Size: 2684 Color: 301
Size: 352 Color: 49

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 515
Size: 2648 Color: 297
Size: 288 Color: 24

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12789 Color: 517
Size: 2437 Color: 288
Size: 486 Color: 96

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 519
Size: 2650 Color: 298
Size: 228 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 520
Size: 1864 Color: 244
Size: 1012 Color: 178

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12850 Color: 521
Size: 1494 Color: 217
Size: 1368 Color: 206

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 526
Size: 2022 Color: 257
Size: 728 Color: 145

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12969 Color: 527
Size: 2287 Color: 277
Size: 456 Color: 85

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 529
Size: 2028 Color: 258
Size: 610 Color: 124

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 531
Size: 2236 Color: 274
Size: 360 Color: 54

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13128 Color: 533
Size: 1822 Color: 241
Size: 762 Color: 148

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 535
Size: 1450 Color: 210
Size: 1104 Color: 184

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 536
Size: 2117 Color: 266
Size: 422 Color: 79

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13202 Color: 541
Size: 1794 Color: 239
Size: 716 Color: 141

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13237 Color: 544
Size: 2101 Color: 264
Size: 374 Color: 57

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13290 Color: 547
Size: 1606 Color: 228
Size: 816 Color: 156

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13317 Color: 549
Size: 1931 Color: 248
Size: 464 Color: 90

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 550
Size: 1996 Color: 254
Size: 398 Color: 67

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13524 Color: 562
Size: 1592 Color: 226
Size: 596 Color: 120

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13556 Color: 564
Size: 1196 Color: 192
Size: 960 Color: 173

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 565
Size: 1610 Color: 229
Size: 540 Color: 110

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 572
Size: 1492 Color: 216
Size: 484 Color: 95

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 577
Size: 1008 Color: 177
Size: 888 Color: 164

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 578
Size: 1596 Color: 227
Size: 284 Color: 22

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13834 Color: 579
Size: 1470 Color: 214
Size: 408 Color: 72

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13852 Color: 581
Size: 1468 Color: 213
Size: 392 Color: 63

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 582
Size: 1308 Color: 202
Size: 534 Color: 108

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 583
Size: 1480 Color: 215
Size: 336 Color: 44

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13922 Color: 585
Size: 1146 Color: 189
Size: 644 Color: 134

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 586
Size: 1236 Color: 195
Size: 552 Color: 112

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 587
Size: 1362 Color: 204
Size: 400 Color: 68

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13956 Color: 588
Size: 1404 Color: 208
Size: 352 Color: 51

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 590
Size: 1160 Color: 191
Size: 560 Color: 116

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14000 Color: 592
Size: 992 Color: 174
Size: 720 Color: 143

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 593
Size: 1502 Color: 218
Size: 184 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14046 Color: 595
Size: 1378 Color: 207
Size: 288 Color: 25

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 597
Size: 1136 Color: 187
Size: 504 Color: 102

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 598
Size: 1212 Color: 193
Size: 424 Color: 81

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 9646 Color: 434
Size: 5721 Color: 382
Size: 344 Color: 47

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 436
Size: 5687 Color: 381
Size: 320 Color: 41

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 448
Size: 5003 Color: 368
Size: 300 Color: 29

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11834 Color: 477
Size: 3461 Color: 330
Size: 416 Color: 76

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11940 Color: 486
Size: 3451 Color: 328
Size: 320 Color: 39

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 12069 Color: 490
Size: 3642 Color: 338

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 510
Size: 3037 Color: 315

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12782 Color: 516
Size: 2661 Color: 299
Size: 268 Color: 16

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13149 Color: 534
Size: 1936 Color: 249
Size: 626 Color: 127

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 13177 Color: 538
Size: 2534 Color: 294

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13193 Color: 540
Size: 1566 Color: 224
Size: 952 Color: 171

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13221 Color: 542
Size: 2130 Color: 268
Size: 360 Color: 53

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 546
Size: 2427 Color: 287

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 556
Size: 2314 Color: 280

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 13463 Color: 559
Size: 2248 Color: 275

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 580
Size: 1875 Color: 245

Bin 72: 2 of cap free
Amount of items: 9
Items: 
Size: 7858 Color: 406
Size: 1304 Color: 199
Size: 1300 Color: 198
Size: 1296 Color: 197
Size: 1248 Color: 196
Size: 1232 Color: 194
Size: 528 Color: 106
Size: 472 Color: 92
Size: 472 Color: 91

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11919 Color: 483
Size: 2967 Color: 310
Size: 824 Color: 158

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11928 Color: 484
Size: 2982 Color: 312
Size: 800 Color: 153

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 507
Size: 1656 Color: 232
Size: 1462 Color: 212

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 12868 Color: 522
Size: 2442 Color: 289
Size: 400 Color: 69

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 532
Size: 2586 Color: 295

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13254 Color: 545
Size: 2456 Color: 290

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 551
Size: 2386 Color: 285

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 555
Size: 2322 Color: 281

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 560
Size: 2230 Color: 273

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 566
Size: 2120 Color: 267

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 569
Size: 2050 Color: 259

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13974 Color: 589
Size: 1736 Color: 236

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 13994 Color: 591
Size: 1716 Color: 234

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 594
Size: 1682 Color: 233

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 599
Size: 1628 Color: 230

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 14134 Color: 600
Size: 1576 Color: 225

Bin 89: 3 of cap free
Amount of items: 7
Items: 
Size: 7864 Color: 410
Size: 2168 Color: 270
Size: 2164 Color: 269
Size: 2113 Color: 265
Size: 496 Color: 99
Size: 456 Color: 86
Size: 448 Color: 84

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 12340 Color: 497
Size: 3368 Color: 327

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 506
Size: 3172 Color: 322

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 537
Size: 2532 Color: 293

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 548
Size: 2404 Color: 286

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13414 Color: 557
Size: 2294 Color: 278

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 596
Size: 1646 Color: 231

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 12121 Color: 491
Size: 3586 Color: 332

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 12503 Color: 504
Size: 3204 Color: 323

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 13032 Color: 528
Size: 2675 Color: 300

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 9052 Color: 428
Size: 6284 Color: 390
Size: 370 Color: 56

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 13186 Color: 539
Size: 2520 Color: 292

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 13334 Color: 553
Size: 2372 Color: 284

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13530 Color: 563
Size: 2176 Color: 271

Bin 103: 7 of cap free
Amount of items: 3
Items: 
Size: 10935 Color: 460
Size: 4514 Color: 362
Size: 256 Color: 15

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 12483 Color: 503
Size: 3222 Color: 325

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 12735 Color: 514
Size: 2970 Color: 311

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 543
Size: 2481 Color: 291

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 554
Size: 2349 Color: 282

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 561
Size: 2193 Color: 272

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 568
Size: 2063 Color: 260

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 10946 Color: 461
Size: 4502 Color: 361
Size: 256 Color: 14

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 11356 Color: 467
Size: 3612 Color: 336
Size: 736 Color: 146

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12930 Color: 524
Size: 2774 Color: 305

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 12953 Color: 525
Size: 2751 Color: 303

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 574
Size: 1940 Color: 250

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 13626 Color: 567
Size: 2077 Color: 261

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 571
Size: 1983 Color: 253

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 570
Size: 2008 Color: 256

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 13738 Color: 573
Size: 1964 Color: 251

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 576
Size: 1898 Color: 246

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13898 Color: 584
Size: 1804 Color: 240

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 474
Size: 4048 Color: 348
Size: 80 Color: 0

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 481
Size: 3821 Color: 342

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 12708 Color: 513
Size: 2993 Color: 314

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 10756 Color: 452
Size: 4648 Color: 363
Size: 296 Color: 27

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 10788 Color: 453
Size: 4912 Color: 367

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13438 Color: 558
Size: 2262 Color: 276

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 13782 Color: 575
Size: 1918 Color: 247

Bin 128: 13 of cap free
Amount of items: 3
Items: 
Size: 10379 Color: 447
Size: 5016 Color: 369
Size: 304 Color: 30

Bin 129: 14 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 465
Size: 4218 Color: 354
Size: 208 Color: 8

Bin 130: 14 of cap free
Amount of items: 3
Items: 
Size: 11474 Color: 472
Size: 4108 Color: 350
Size: 116 Color: 2

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 475
Size: 4026 Color: 346

Bin 132: 15 of cap free
Amount of items: 3
Items: 
Size: 11559 Color: 473
Size: 4042 Color: 347
Size: 96 Color: 1

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 11945 Color: 487
Size: 3752 Color: 340

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 13333 Color: 552
Size: 2364 Color: 283

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 9528 Color: 431
Size: 6168 Color: 388

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 12884 Color: 523
Size: 2812 Color: 307

Bin 137: 17 of cap free
Amount of items: 2
Items: 
Size: 13093 Color: 530
Size: 2602 Color: 296

Bin 138: 18 of cap free
Amount of items: 7
Items: 
Size: 7860 Color: 408
Size: 1726 Color: 235
Size: 1528 Color: 220
Size: 1514 Color: 219
Size: 1456 Color: 211
Size: 1152 Color: 190
Size: 458 Color: 87

Bin 139: 19 of cap free
Amount of items: 3
Items: 
Size: 10724 Color: 451
Size: 3141 Color: 318
Size: 1828 Color: 242

Bin 140: 20 of cap free
Amount of items: 3
Items: 
Size: 10004 Color: 441
Size: 5376 Color: 376
Size: 312 Color: 35

Bin 141: 22 of cap free
Amount of items: 2
Items: 
Size: 9840 Color: 438
Size: 5850 Color: 385

Bin 142: 23 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 518
Size: 2888 Color: 308

Bin 143: 24 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 444
Size: 5070 Color: 372
Size: 304 Color: 32

Bin 144: 25 of cap free
Amount of items: 2
Items: 
Size: 12153 Color: 494
Size: 3534 Color: 331

Bin 145: 28 of cap free
Amount of items: 3
Items: 
Size: 10616 Color: 449
Size: 4772 Color: 365
Size: 296 Color: 28

Bin 146: 29 of cap free
Amount of items: 2
Items: 
Size: 11876 Color: 480
Size: 3807 Color: 341

Bin 147: 31 of cap free
Amount of items: 2
Items: 
Size: 8849 Color: 424
Size: 6832 Color: 399

Bin 148: 31 of cap free
Amount of items: 2
Items: 
Size: 10664 Color: 450
Size: 5017 Color: 370

Bin 149: 32 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 476
Size: 3896 Color: 343

Bin 150: 32 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 488
Size: 3704 Color: 339

Bin 151: 32 of cap free
Amount of items: 2
Items: 
Size: 12519 Color: 505
Size: 3161 Color: 321

Bin 152: 32 of cap free
Amount of items: 2
Items: 
Size: 12696 Color: 512
Size: 2984 Color: 313

Bin 153: 35 of cap free
Amount of items: 2
Items: 
Size: 12041 Color: 489
Size: 3636 Color: 337

Bin 154: 36 of cap free
Amount of items: 3
Items: 
Size: 9932 Color: 439
Size: 5432 Color: 377
Size: 312 Color: 37

Bin 155: 44 of cap free
Amount of items: 3
Items: 
Size: 7902 Color: 416
Size: 7350 Color: 402
Size: 416 Color: 77

Bin 156: 47 of cap free
Amount of items: 3
Items: 
Size: 10921 Color: 459
Size: 4472 Color: 360
Size: 272 Color: 18

Bin 157: 51 of cap free
Amount of items: 5
Items: 
Size: 7870 Color: 411
Size: 3061 Color: 316
Size: 2301 Color: 279
Size: 1997 Color: 255
Size: 432 Color: 83

Bin 158: 52 of cap free
Amount of items: 3
Items: 
Size: 10298 Color: 443
Size: 5058 Color: 371
Size: 304 Color: 33

Bin 159: 52 of cap free
Amount of items: 2
Items: 
Size: 10840 Color: 454
Size: 4820 Color: 366

Bin 160: 58 of cap free
Amount of items: 2
Items: 
Size: 7878 Color: 413
Size: 7776 Color: 404

Bin 161: 62 of cap free
Amount of items: 4
Items: 
Size: 8312 Color: 419
Size: 6542 Color: 393
Size: 400 Color: 70
Size: 396 Color: 66

Bin 162: 66 of cap free
Amount of items: 2
Items: 
Size: 10360 Color: 445
Size: 5286 Color: 375

Bin 163: 66 of cap free
Amount of items: 3
Items: 
Size: 11410 Color: 470
Size: 4072 Color: 349
Size: 164 Color: 4

Bin 164: 68 of cap free
Amount of items: 3
Items: 
Size: 8712 Color: 422
Size: 6548 Color: 396
Size: 384 Color: 62

Bin 165: 71 of cap free
Amount of items: 3
Items: 
Size: 8702 Color: 421
Size: 6547 Color: 395
Size: 392 Color: 64

Bin 166: 84 of cap free
Amount of items: 4
Items: 
Size: 9020 Color: 426
Size: 5848 Color: 384
Size: 384 Color: 61
Size: 376 Color: 59

Bin 167: 85 of cap free
Amount of items: 3
Items: 
Size: 10896 Color: 458
Size: 4459 Color: 359
Size: 272 Color: 19

Bin 168: 88 of cap free
Amount of items: 3
Items: 
Size: 7894 Color: 415
Size: 7308 Color: 401
Size: 422 Color: 80

Bin 169: 90 of cap free
Amount of items: 4
Items: 
Size: 8278 Color: 418
Size: 6532 Color: 392
Size: 412 Color: 73
Size: 400 Color: 71

Bin 170: 93 of cap free
Amount of items: 3
Items: 
Size: 9534 Color: 432
Size: 5733 Color: 383
Size: 352 Color: 50

Bin 171: 95 of cap free
Amount of items: 3
Items: 
Size: 11129 Color: 463
Size: 4248 Color: 355
Size: 240 Color: 12

Bin 172: 96 of cap free
Amount of items: 3
Items: 
Size: 10152 Color: 442
Size: 5160 Color: 374
Size: 304 Color: 34

Bin 173: 98 of cap free
Amount of items: 3
Items: 
Size: 9370 Color: 430
Size: 5888 Color: 386
Size: 356 Color: 52

Bin 174: 102 of cap free
Amount of items: 3
Items: 
Size: 9036 Color: 427
Size: 6198 Color: 389
Size: 376 Color: 58

Bin 175: 103 of cap free
Amount of items: 3
Items: 
Size: 9709 Color: 437
Size: 5580 Color: 380
Size: 320 Color: 38

Bin 176: 108 of cap free
Amount of items: 3
Items: 
Size: 11145 Color: 464
Size: 4251 Color: 356
Size: 208 Color: 9

Bin 177: 111 of cap free
Amount of items: 3
Items: 
Size: 9693 Color: 435
Size: 5564 Color: 379
Size: 344 Color: 46

Bin 178: 113 of cap free
Amount of items: 3
Items: 
Size: 10882 Color: 457
Size: 4445 Color: 358
Size: 272 Color: 20

Bin 179: 114 of cap free
Amount of items: 4
Items: 
Size: 10854 Color: 455
Size: 4164 Color: 352
Size: 292 Color: 26
Size: 288 Color: 23

Bin 180: 138 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 429
Size: 5998 Color: 387
Size: 368 Color: 55

Bin 181: 142 of cap free
Amount of items: 3
Items: 
Size: 10866 Color: 456
Size: 4424 Color: 357
Size: 280 Color: 21

Bin 182: 161 of cap free
Amount of items: 3
Items: 
Size: 11394 Color: 469
Size: 3981 Color: 345
Size: 176 Color: 5

Bin 183: 174 of cap free
Amount of items: 3
Items: 
Size: 9630 Color: 433
Size: 5556 Color: 378
Size: 352 Color: 48

Bin 184: 182 of cap free
Amount of items: 3
Items: 
Size: 11380 Color: 468
Size: 3974 Color: 344
Size: 176 Color: 6

Bin 185: 184 of cap free
Amount of items: 4
Items: 
Size: 8180 Color: 417
Size: 6518 Color: 391
Size: 416 Color: 75
Size: 414 Color: 74

Bin 186: 186 of cap free
Amount of items: 2
Items: 
Size: 7876 Color: 412
Size: 7650 Color: 403

Bin 187: 190 of cap free
Amount of items: 5
Items: 
Size: 7862 Color: 409
Size: 2088 Color: 262
Size: 1982 Color: 252
Size: 1848 Color: 243
Size: 1742 Color: 237

Bin 188: 192 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 462
Size: 4216 Color: 353
Size: 256 Color: 13

Bin 189: 193 of cap free
Amount of items: 2
Items: 
Size: 8889 Color: 425
Size: 6630 Color: 398

Bin 190: 200 of cap free
Amount of items: 23
Items: 
Size: 832 Color: 159
Size: 824 Color: 157
Size: 804 Color: 155
Size: 804 Color: 154
Size: 796 Color: 152
Size: 792 Color: 151
Size: 768 Color: 150
Size: 760 Color: 147
Size: 720 Color: 144
Size: 720 Color: 142
Size: 716 Color: 140
Size: 704 Color: 139
Size: 704 Color: 138
Size: 598 Color: 121
Size: 592 Color: 119
Size: 592 Color: 118
Size: 576 Color: 117
Size: 560 Color: 114
Size: 548 Color: 111
Size: 538 Color: 109
Size: 532 Color: 107
Size: 516 Color: 105
Size: 516 Color: 104

Bin 191: 211 of cap free
Amount of items: 8
Items: 
Size: 7859 Color: 407
Size: 1434 Color: 209
Size: 1364 Color: 205
Size: 1308 Color: 203
Size: 1308 Color: 201
Size: 1304 Color: 200
Size: 464 Color: 89
Size: 460 Color: 88

Bin 192: 246 of cap free
Amount of items: 3
Items: 
Size: 7892 Color: 414
Size: 7142 Color: 400
Size: 432 Color: 82

Bin 193: 253 of cap free
Amount of items: 3
Items: 
Size: 8518 Color: 420
Size: 6545 Color: 394
Size: 396 Color: 65

Bin 194: 262 of cap free
Amount of items: 3
Items: 
Size: 9988 Color: 440
Size: 5150 Color: 373
Size: 312 Color: 36

Bin 195: 276 of cap free
Amount of items: 19
Items: 
Size: 1028 Color: 180
Size: 1002 Color: 176
Size: 1000 Color: 175
Size: 960 Color: 172
Size: 944 Color: 170
Size: 912 Color: 169
Size: 912 Color: 168
Size: 900 Color: 167
Size: 896 Color: 166
Size: 890 Color: 165
Size: 880 Color: 163
Size: 880 Color: 162
Size: 880 Color: 161
Size: 848 Color: 160
Size: 512 Color: 103
Size: 504 Color: 101
Size: 504 Color: 100
Size: 496 Color: 98
Size: 488 Color: 97

Bin 196: 281 of cap free
Amount of items: 3
Items: 
Size: 10363 Color: 446
Size: 4764 Color: 364
Size: 304 Color: 31

Bin 197: 301 of cap free
Amount of items: 9
Items: 
Size: 7857 Color: 405
Size: 1142 Color: 188
Size: 1112 Color: 186
Size: 1112 Color: 185
Size: 1104 Color: 183
Size: 1072 Color: 182
Size: 1056 Color: 181
Size: 480 Color: 94
Size: 476 Color: 93

Bin 198: 327 of cap free
Amount of items: 2
Items: 
Size: 8833 Color: 423
Size: 6552 Color: 397

Bin 199: 8634 of cap free
Amount of items: 11
Items: 
Size: 692 Color: 137
Size: 688 Color: 136
Size: 668 Color: 135
Size: 640 Color: 132
Size: 640 Color: 131
Size: 632 Color: 130
Size: 632 Color: 129
Size: 632 Color: 128
Size: 624 Color: 126
Size: 624 Color: 125
Size: 606 Color: 122

Total size: 3110976
Total free space: 15712

