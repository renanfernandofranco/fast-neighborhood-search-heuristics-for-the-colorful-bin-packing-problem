Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 454777 Color: 469
Size: 291198 Color: 176
Size: 254026 Color: 26

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 372050 Color: 357
Size: 337414 Color: 305
Size: 290537 Color: 174

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 403135 Color: 405
Size: 312693 Color: 247
Size: 284173 Color: 156

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 410077 Color: 417
Size: 339203 Color: 308
Size: 250721 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 400619 Color: 398
Size: 311419 Color: 243
Size: 287963 Color: 167

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 448037 Color: 459
Size: 291978 Color: 181
Size: 259986 Color: 58

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 369053 Color: 353
Size: 317138 Color: 259
Size: 313810 Color: 250

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 406503 Color: 408
Size: 327015 Color: 282
Size: 266483 Color: 81

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 465780 Color: 476
Size: 277050 Color: 125
Size: 257171 Color: 45

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 407010 Color: 409
Size: 309509 Color: 236
Size: 283482 Color: 154

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 420315 Color: 429
Size: 299335 Color: 202
Size: 280351 Color: 140

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 483272 Color: 492
Size: 266142 Color: 80
Size: 250587 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 492185 Color: 497
Size: 255973 Color: 37
Size: 251843 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 452514 Color: 467
Size: 286110 Color: 161
Size: 261377 Color: 63

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 348927 Color: 323
Size: 340710 Color: 312
Size: 310364 Color: 239

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 467969 Color: 477
Size: 267822 Color: 91
Size: 264210 Color: 73

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 448220 Color: 460
Size: 291682 Color: 180
Size: 260099 Color: 59

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 410310 Color: 418
Size: 295734 Color: 192
Size: 293957 Color: 190

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 450159 Color: 463
Size: 299690 Color: 203
Size: 250152 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 391803 Color: 385
Size: 334849 Color: 299
Size: 273349 Color: 112

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 422030 Color: 437
Size: 309109 Color: 235
Size: 268862 Color: 93

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 468427 Color: 478
Size: 279555 Color: 134
Size: 252019 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 353045 Color: 331
Size: 342375 Color: 313
Size: 304581 Color: 224

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 400640 Color: 399
Size: 338310 Color: 307
Size: 261051 Color: 62

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 414401 Color: 422
Size: 308143 Color: 232
Size: 277457 Color: 128

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 432129 Color: 444
Size: 317346 Color: 260
Size: 250526 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 408282 Color: 412
Size: 310422 Color: 240
Size: 281297 Color: 143

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 390251 Color: 381
Size: 311633 Color: 245
Size: 298117 Color: 199

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 364393 Color: 348
Size: 356228 Color: 335
Size: 279380 Color: 133

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 473723 Color: 482
Size: 275395 Color: 121
Size: 250883 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 375914 Color: 361
Size: 335559 Color: 301
Size: 288528 Color: 169

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 355258 Color: 334
Size: 350868 Color: 325
Size: 293875 Color: 189

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 462455 Color: 475
Size: 274846 Color: 120
Size: 262700 Color: 69

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 401323 Color: 400
Size: 321682 Color: 273
Size: 276996 Color: 124

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 445656 Color: 456
Size: 280061 Color: 138
Size: 274284 Color: 115

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 368699 Color: 352
Size: 318095 Color: 263
Size: 313207 Color: 248

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 407257 Color: 410
Size: 319855 Color: 267
Size: 272889 Color: 110

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 383469 Color: 373
Size: 323292 Color: 276
Size: 293240 Color: 187

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 368652 Color: 351
Size: 351312 Color: 326
Size: 280037 Color: 137

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 410018 Color: 416
Size: 320475 Color: 271
Size: 269508 Color: 96

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 446412 Color: 457
Size: 286175 Color: 162
Size: 267414 Color: 85

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 410412 Color: 419
Size: 313258 Color: 249
Size: 276331 Color: 122

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 360348 Color: 344
Size: 346912 Color: 318
Size: 292741 Color: 184

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 479364 Color: 487
Size: 266756 Color: 83
Size: 253881 Color: 24

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 371700 Color: 356
Size: 330864 Color: 291
Size: 297437 Color: 196

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 471003 Color: 481
Size: 272583 Color: 109
Size: 256415 Color: 40

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 459070 Color: 472
Size: 277333 Color: 126
Size: 263598 Color: 72

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 384851 Color: 376
Size: 312557 Color: 246
Size: 302593 Color: 218

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 446500 Color: 458
Size: 282977 Color: 150
Size: 270524 Color: 99

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 380332 Color: 366
Size: 337238 Color: 304
Size: 282431 Color: 148

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 353705 Color: 332
Size: 331842 Color: 297
Size: 314454 Color: 251

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 469958 Color: 480
Size: 271893 Color: 108
Size: 258150 Color: 50

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 400425 Color: 397
Size: 328027 Color: 285
Size: 271549 Color: 105

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 370657 Color: 355
Size: 361910 Color: 347
Size: 267434 Color: 86

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 444604 Color: 454
Size: 300638 Color: 209
Size: 254759 Color: 28

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 340695 Color: 311
Size: 331629 Color: 295
Size: 327677 Color: 284

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 469301 Color: 479
Size: 277667 Color: 129
Size: 253033 Color: 19

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 476260 Color: 484
Size: 270968 Color: 102
Size: 252773 Color: 18

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 357000 Color: 337
Size: 335973 Color: 303
Size: 307028 Color: 229

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 359436 Color: 340
Size: 352750 Color: 329
Size: 287815 Color: 165

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 360430 Color: 345
Size: 320397 Color: 270
Size: 319174 Color: 266

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 396516 Color: 391
Size: 317613 Color: 261
Size: 285872 Color: 160

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 452510 Color: 466
Size: 276796 Color: 123
Size: 270695 Color: 100

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 373701 Color: 359
Size: 351646 Color: 327
Size: 274654 Color: 119

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 442911 Color: 449
Size: 300454 Color: 208
Size: 256636 Color: 41

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 499807 Color: 500
Size: 250123 Color: 1
Size: 250071 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 402228 Color: 402
Size: 334530 Color: 298
Size: 263243 Color: 70

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 453059 Color: 468
Size: 293822 Color: 188
Size: 253120 Color: 20

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 437856 Color: 446
Size: 284707 Color: 158
Size: 277438 Color: 127

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 408711 Color: 413
Size: 337796 Color: 306
Size: 253494 Color: 21

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 483929 Color: 493
Size: 261575 Color: 64
Size: 254497 Color: 27

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 381978 Color: 369
Size: 324790 Color: 279
Size: 293233 Color: 186

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 477212 Color: 485
Size: 268956 Color: 94
Size: 253833 Color: 23

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 414536 Color: 423
Size: 304140 Color: 222
Size: 281325 Color: 144

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 449811 Color: 462
Size: 292585 Color: 182
Size: 257605 Color: 46

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 424856 Color: 438
Size: 296242 Color: 194
Size: 278903 Color: 132

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 495713 Color: 499
Size: 253508 Color: 22
Size: 250780 Color: 6

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 420794 Color: 431
Size: 308496 Color: 234
Size: 270711 Color: 101

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 430437 Color: 441
Size: 301837 Color: 216
Size: 267727 Color: 90

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 397598 Color: 395
Size: 302357 Color: 217
Size: 300046 Color: 205

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 442955 Color: 450
Size: 305054 Color: 225
Size: 251992 Color: 13

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 451477 Color: 465
Size: 291614 Color: 179
Size: 256910 Color: 42

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 396573 Color: 392
Size: 302705 Color: 220
Size: 300723 Color: 210

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 361231 Color: 346
Size: 331249 Color: 292
Size: 307521 Color: 231

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 358667 Color: 339
Size: 326062 Color: 281
Size: 315272 Color: 254

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 343831 Color: 315
Size: 339301 Color: 309
Size: 316869 Color: 257

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 428597 Color: 439
Size: 287927 Color: 166
Size: 283477 Color: 153

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 383661 Color: 374
Size: 348798 Color: 322
Size: 267542 Color: 87

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 399406 Color: 396
Size: 339891 Color: 310
Size: 260704 Color: 60

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 392235 Color: 387
Size: 343465 Color: 314
Size: 264301 Color: 74

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 401426 Color: 401
Size: 300074 Color: 207
Size: 298501 Color: 200

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 490162 Color: 496
Size: 258036 Color: 49
Size: 251803 Color: 10

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 373734 Color: 360
Size: 348020 Color: 320
Size: 278247 Color: 130

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 415004 Color: 424
Size: 314956 Color: 253
Size: 270041 Color: 97

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 474490 Color: 483
Size: 273565 Color: 114
Size: 251946 Color: 12

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 429257 Color: 440
Size: 296275 Color: 195
Size: 274469 Color: 117

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 383395 Color: 372
Size: 314889 Color: 252
Size: 301717 Color: 215

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 378535 Color: 364
Size: 330803 Color: 290
Size: 290663 Color: 175

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 420563 Color: 430
Size: 300826 Color: 211
Size: 278612 Color: 131

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 381302 Color: 367
Size: 356460 Color: 336
Size: 262239 Color: 66

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 392494 Color: 388
Size: 306138 Color: 227
Size: 301369 Color: 213

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 369071 Color: 354
Size: 357904 Color: 338
Size: 273026 Color: 111

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 420944 Color: 432
Size: 297926 Color: 198
Size: 281131 Color: 142

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 451194 Color: 464
Size: 283034 Color: 151
Size: 265773 Color: 78

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 404294 Color: 406
Size: 315915 Color: 256
Size: 279792 Color: 135

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 421475 Color: 433
Size: 309543 Color: 237
Size: 268983 Color: 95

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 381375 Color: 368
Size: 344004 Color: 316
Size: 274622 Color: 118

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 396819 Color: 393
Size: 321806 Color: 274
Size: 281376 Color: 146

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 347992 Color: 319
Size: 331792 Color: 296
Size: 320217 Color: 268

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 382703 Color: 370
Size: 360224 Color: 343
Size: 257074 Color: 44

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 422000 Color: 436
Size: 289746 Color: 172
Size: 288255 Color: 168

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 408755 Color: 414
Size: 328744 Color: 287
Size: 262502 Color: 68

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 441110 Color: 447
Size: 303276 Color: 221
Size: 255615 Color: 34

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 460990 Color: 474
Size: 271401 Color: 103
Size: 267610 Color: 88

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 417157 Color: 426
Size: 330616 Color: 289
Size: 252228 Color: 16

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 368187 Color: 350
Size: 321313 Color: 272
Size: 310501 Color: 241

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 402239 Color: 403
Size: 308224 Color: 233
Size: 289538 Color: 171

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 417505 Color: 428
Size: 324686 Color: 278
Size: 257810 Color: 47

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 421499 Color: 434
Size: 311306 Color: 242
Size: 267196 Color: 84

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 492607 Color: 498
Size: 255221 Color: 32
Size: 252173 Color: 15

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 413634 Color: 420
Size: 324328 Color: 277
Size: 262039 Color: 65

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 430523 Color: 442
Size: 287653 Color: 163
Size: 281825 Color: 147

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 391527 Color: 384
Size: 317046 Color: 258
Size: 291428 Color: 178

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 353845 Color: 333
Size: 346118 Color: 317
Size: 300038 Color: 204

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 448769 Color: 461
Size: 288828 Color: 170
Size: 262404 Color: 67

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 413683 Color: 421
Size: 306238 Color: 228
Size: 280080 Color: 139

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 378812 Color: 365
Size: 352824 Color: 330
Size: 268365 Color: 92

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 431160 Color: 443
Size: 309722 Color: 238
Size: 259119 Color: 55

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 443975 Color: 453
Size: 284302 Color: 157
Size: 271724 Color: 106

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 395590 Color: 390
Size: 348190 Color: 321
Size: 256221 Color: 39

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 391953 Color: 386
Size: 349559 Color: 324
Size: 258489 Color: 51

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 481022 Color: 488
Size: 267627 Color: 89
Size: 251352 Color: 9

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 377026 Color: 363
Size: 331628 Color: 294
Size: 291347 Color: 177

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 417356 Color: 427
Size: 302652 Color: 219
Size: 279993 Color: 136

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 435715 Color: 445
Size: 301018 Color: 212
Size: 263268 Color: 71

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 482258 Color: 490
Size: 259920 Color: 57
Size: 257823 Color: 48

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 482448 Color: 491
Size: 259053 Color: 54
Size: 258500 Color: 52

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 382840 Color: 371
Size: 315700 Color: 255
Size: 301461 Color: 214

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 407485 Color: 411
Size: 307438 Color: 230
Size: 285078 Color: 159

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 458250 Color: 471
Size: 282443 Color: 149
Size: 259308 Color: 56

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 385088 Color: 377
Size: 318936 Color: 265
Size: 295977 Color: 193

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 455221 Color: 470
Size: 284070 Color: 155
Size: 260710 Color: 61

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 405270 Color: 407
Size: 320279 Color: 269
Size: 274452 Color: 116

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 375949 Color: 362
Size: 352200 Color: 328
Size: 271852 Color: 107

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 409186 Color: 415
Size: 335608 Color: 302
Size: 255207 Color: 30

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 397263 Color: 394
Size: 331333 Color: 293
Size: 271405 Color: 104

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 478771 Color: 486
Size: 265280 Color: 76
Size: 255950 Color: 36

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 459994 Color: 473
Size: 281368 Color: 145
Size: 258639 Color: 53

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 443127 Color: 451
Size: 305895 Color: 226
Size: 250979 Color: 8

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 488653 Color: 494
Size: 256131 Color: 38
Size: 255217 Color: 31

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 402302 Color: 404
Size: 327275 Color: 283
Size: 270424 Color: 98

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 445283 Color: 455
Size: 290241 Color: 173
Size: 264477 Color: 75

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 384523 Color: 375
Size: 322864 Color: 275
Size: 292614 Color: 183

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 441490 Color: 448
Size: 292895 Color: 185
Size: 265616 Color: 77

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 391187 Color: 383
Size: 335327 Color: 300
Size: 273487 Color: 113

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 390728 Color: 382
Size: 325932 Color: 280
Size: 283341 Color: 152

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 394611 Color: 389
Size: 317653 Color: 262
Size: 287737 Color: 164

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 373511 Color: 358
Size: 359786 Color: 341
Size: 266704 Color: 82

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 388527 Color: 380
Size: 311419 Color: 244
Size: 300055 Color: 206

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 386201 Color: 379
Size: 359880 Color: 342
Size: 253920 Color: 25

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 365616 Color: 349
Size: 330114 Color: 288
Size: 304271 Color: 223

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 386015 Color: 378
Size: 318443 Color: 264
Size: 295543 Color: 191

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 415994 Color: 425
Size: 328080 Color: 286
Size: 255927 Color: 35

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 421842 Color: 435
Size: 297559 Color: 197
Size: 280600 Color: 141

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 443711 Color: 452
Size: 299286 Color: 201
Size: 257004 Color: 43

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 481540 Color: 489
Size: 266004 Color: 79
Size: 252457 Color: 17

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 489521 Color: 495
Size: 255405 Color: 33
Size: 255075 Color: 29

Total size: 167000167
Total free space: 0

