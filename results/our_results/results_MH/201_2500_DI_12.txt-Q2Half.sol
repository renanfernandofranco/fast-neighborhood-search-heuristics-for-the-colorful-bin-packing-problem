Capicity Bin: 1864
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 934 Color: 1
Size: 778 Color: 1
Size: 92 Color: 0
Size: 60 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1497 Color: 1
Size: 275 Color: 1
Size: 40 Color: 0
Size: 36 Color: 0
Size: 16 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1470 Color: 1
Size: 230 Color: 1
Size: 100 Color: 0
Size: 64 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1475 Color: 1
Size: 309 Color: 1
Size: 80 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1597 Color: 1
Size: 223 Color: 1
Size: 44 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 1
Size: 163 Color: 1
Size: 32 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 1
Size: 269 Color: 1
Size: 34 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 1
Size: 358 Color: 0
Size: 136 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1604 Color: 1
Size: 220 Color: 1
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 1
Size: 253 Color: 1
Size: 68 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 937 Color: 1
Size: 773 Color: 1
Size: 154 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 1
Size: 189 Color: 1
Size: 40 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 1
Size: 241 Color: 1
Size: 48 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 1
Size: 293 Color: 1
Size: 58 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1454 Color: 1
Size: 342 Color: 1
Size: 68 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 414 Color: 1
Size: 32 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 1
Size: 247 Color: 1
Size: 48 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 1
Size: 170 Color: 1
Size: 44 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 1
Size: 181 Color: 1
Size: 36 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 1
Size: 374 Color: 1
Size: 52 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1297 Color: 1
Size: 517 Color: 1
Size: 50 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 222 Color: 1
Size: 52 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1361 Color: 1
Size: 421 Color: 1
Size: 82 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1279 Color: 1
Size: 489 Color: 1
Size: 96 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 1
Size: 210 Color: 1
Size: 40 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 1
Size: 261 Color: 1
Size: 50 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 531 Color: 1
Size: 96 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 1
Size: 199 Color: 1
Size: 8 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 1
Size: 283 Color: 1
Size: 56 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 1
Size: 387 Color: 1
Size: 76 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 1
Size: 262 Color: 1
Size: 72 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 1
Size: 622 Color: 1
Size: 208 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1065 Color: 1
Size: 667 Color: 1
Size: 132 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1449 Color: 1
Size: 347 Color: 1
Size: 68 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 1
Size: 432 Color: 1
Size: 182 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 1
Size: 514 Color: 1
Size: 52 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 1
Size: 474 Color: 1
Size: 272 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 1
Size: 211 Color: 1
Size: 4 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 1
Size: 621 Color: 1
Size: 80 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1057 Color: 1
Size: 673 Color: 1
Size: 134 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 1
Size: 218 Color: 1
Size: 40 Color: 0

Bin 42: 0 of cap free
Amount of items: 4
Items: 
Size: 1288 Color: 1
Size: 384 Color: 1
Size: 124 Color: 0
Size: 68 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 161 Color: 1
Size: 30 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 246 Color: 1
Size: 48 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 1
Size: 152 Color: 1
Size: 50 Color: 0

Bin 46: 0 of cap free
Amount of items: 5
Items: 
Size: 752 Color: 1
Size: 694 Color: 1
Size: 330 Color: 1
Size: 48 Color: 0
Size: 40 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 1
Size: 425 Color: 1
Size: 84 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 1
Size: 208 Color: 1
Size: 58 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 1
Size: 282 Color: 1
Size: 32 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 1
Size: 237 Color: 1
Size: 22 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 933 Color: 1
Size: 777 Color: 1
Size: 154 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1172 Color: 1
Size: 628 Color: 1
Size: 64 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1305 Color: 1
Size: 467 Color: 1
Size: 92 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 941 Color: 1
Size: 771 Color: 1
Size: 152 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 1
Size: 302 Color: 1
Size: 60 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1441 Color: 1
Size: 353 Color: 1
Size: 70 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 1
Size: 201 Color: 1
Size: 8 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 1
Size: 325 Color: 1
Size: 4 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1190 Color: 1
Size: 562 Color: 1
Size: 112 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 1
Size: 197 Color: 1
Size: 54 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1155 Color: 1
Size: 591 Color: 1
Size: 118 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 1
Size: 442 Color: 1
Size: 84 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1639 Color: 1
Size: 181 Color: 1
Size: 44 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1544 Color: 1
Size: 288 Color: 1
Size: 32 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1207 Color: 1
Size: 549 Color: 1
Size: 108 Color: 0

Total size: 121160
Total free space: 0

