Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1000 Color: 130
Size: 504 Color: 98
Size: 384 Color: 90
Size: 272 Color: 77
Size: 272 Color: 76
Size: 32 Color: 5
Size: 8 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1704 Color: 166
Size: 576 Color: 105
Size: 96 Color: 35
Size: 88 Color: 30
Size: 8 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 147
Size: 892 Color: 127
Size: 176 Color: 62

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 173
Size: 540 Color: 100
Size: 104 Color: 38

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 185
Size: 364 Color: 87
Size: 72 Color: 27

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 169
Size: 604 Color: 106
Size: 120 Color: 41

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 138
Size: 1093 Color: 136
Size: 142 Color: 50

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 189
Size: 311 Color: 81
Size: 60 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1540 Color: 155
Size: 780 Color: 118
Size: 152 Color: 54

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 159
Size: 729 Color: 114
Size: 144 Color: 51

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 163
Size: 670 Color: 110
Size: 132 Color: 47

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 194
Size: 283 Color: 79
Size: 38 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 151
Size: 830 Color: 122
Size: 164 Color: 58

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 146
Size: 955 Color: 129
Size: 130 Color: 45

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 200
Size: 233 Color: 71
Size: 38 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1652 Color: 160
Size: 684 Color: 112
Size: 136 Color: 49

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1357 Color: 145
Size: 931 Color: 128
Size: 184 Color: 63

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 192
Size: 334 Color: 85
Size: 20 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 143
Size: 1022 Color: 132
Size: 196 Color: 64

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 141
Size: 1028 Color: 134
Size: 200 Color: 65

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 199
Size: 230 Color: 70
Size: 44 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2163 Color: 196
Size: 259 Color: 73
Size: 50 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 154
Size: 787 Color: 119
Size: 156 Color: 55

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 174
Size: 515 Color: 99
Size: 102 Color: 36

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 142
Size: 1022 Color: 133
Size: 204 Color: 67

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 195
Size: 262 Color: 74
Size: 52 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 197
Size: 268 Color: 75
Size: 40 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 157
Size: 751 Color: 116
Size: 150 Color: 53

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 148
Size: 889 Color: 126
Size: 176 Color: 61

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 201
Size: 220 Color: 69
Size: 40 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 140
Size: 1103 Color: 137
Size: 128 Color: 44

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 184
Size: 366 Color: 88
Size: 72 Color: 24

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 152
Size: 822 Color: 121
Size: 164 Color: 57

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 164
Size: 662 Color: 109
Size: 132 Color: 46

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 171
Size: 546 Color: 102
Size: 108 Color: 39

Bin 36: 0 of cap free
Amount of items: 4
Items: 
Size: 1552 Color: 156
Size: 840 Color: 124
Size: 64 Color: 21
Size: 16 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 144
Size: 1010 Color: 131
Size: 200 Color: 66

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 165
Size: 658 Color: 108
Size: 128 Color: 43

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2179 Color: 198
Size: 245 Color: 72
Size: 48 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 186
Size: 361 Color: 86
Size: 72 Color: 25

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 168
Size: 693 Color: 113
Size: 42 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 158
Size: 739 Color: 115
Size: 146 Color: 52

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 149
Size: 846 Color: 125
Size: 168 Color: 60

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 178
Size: 446 Color: 95
Size: 88 Color: 32

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2073 Color: 187
Size: 333 Color: 84
Size: 66 Color: 22

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 179
Size: 444 Color: 94
Size: 88 Color: 31

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 150
Size: 838 Color: 123
Size: 164 Color: 59

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2030 Color: 183
Size: 370 Color: 89
Size: 72 Color: 26

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 182
Size: 387 Color: 91
Size: 66 Color: 23

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 162
Size: 678 Color: 111
Size: 132 Color: 48

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 139
Size: 1030 Color: 135
Size: 204 Color: 68

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 190
Size: 302 Color: 80
Size: 60 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 191
Size: 316 Color: 82
Size: 40 Color: 8

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 175
Size: 556 Color: 103
Size: 24 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1943 Color: 180
Size: 441 Color: 93
Size: 88 Color: 33

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 172
Size: 542 Color: 101
Size: 104 Color: 37

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 161
Size: 758 Color: 117
Size: 56 Color: 17

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 153
Size: 818 Color: 120
Size: 160 Color: 56

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1903 Color: 176
Size: 475 Color: 97
Size: 94 Color: 34

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 167
Size: 621 Color: 107
Size: 122 Color: 42

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 170
Size: 563 Color: 104
Size: 112 Color: 40

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1985 Color: 181
Size: 407 Color: 92
Size: 80 Color: 28

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2093 Color: 188
Size: 317 Color: 83
Size: 62 Color: 20

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 177
Size: 450 Color: 96
Size: 88 Color: 29

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 193
Size: 275 Color: 78
Size: 54 Color: 16

Total size: 160680
Total free space: 0

