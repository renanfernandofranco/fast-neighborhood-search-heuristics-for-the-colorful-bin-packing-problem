Capicity Bin: 14912
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8272 Color: 0
Size: 6216 Color: 14
Size: 424 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 11
Size: 6210 Color: 3
Size: 246 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8468 Color: 19
Size: 6184 Color: 11
Size: 260 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9236 Color: 8
Size: 5384 Color: 14
Size: 292 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9240 Color: 9
Size: 5372 Color: 10
Size: 300 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 11
Size: 5336 Color: 17
Size: 272 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9492 Color: 0
Size: 5060 Color: 3
Size: 360 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10194 Color: 9
Size: 4088 Color: 12
Size: 630 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10304 Color: 13
Size: 4084 Color: 10
Size: 524 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10584 Color: 9
Size: 3978 Color: 9
Size: 350 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10602 Color: 17
Size: 3608 Color: 9
Size: 702 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10733 Color: 9
Size: 3861 Color: 19
Size: 318 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 0
Size: 2920 Color: 3
Size: 976 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11023 Color: 10
Size: 3517 Color: 18
Size: 372 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 5
Size: 3560 Color: 8
Size: 288 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 0
Size: 3525 Color: 18
Size: 226 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 14
Size: 3536 Color: 6
Size: 212 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11330 Color: 2
Size: 3162 Color: 2
Size: 420 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11341 Color: 18
Size: 2907 Color: 11
Size: 664 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 15
Size: 2616 Color: 11
Size: 928 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 18
Size: 3208 Color: 9
Size: 288 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 9
Size: 2850 Color: 16
Size: 434 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11721 Color: 14
Size: 3013 Color: 19
Size: 178 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 5
Size: 2276 Color: 6
Size: 784 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11874 Color: 5
Size: 2444 Color: 9
Size: 594 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11970 Color: 0
Size: 2066 Color: 15
Size: 876 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 10
Size: 1514 Color: 10
Size: 1418 Color: 7

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12003 Color: 17
Size: 1493 Color: 3
Size: 1416 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 12
Size: 2384 Color: 18
Size: 448 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 14
Size: 1424 Color: 3
Size: 1380 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12155 Color: 9
Size: 2299 Color: 14
Size: 458 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12188 Color: 2
Size: 2348 Color: 12
Size: 376 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 19
Size: 2424 Color: 12
Size: 288 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12304 Color: 1
Size: 1664 Color: 19
Size: 944 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12352 Color: 19
Size: 2256 Color: 17
Size: 304 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12365 Color: 15
Size: 2217 Color: 19
Size: 330 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12388 Color: 18
Size: 1948 Color: 2
Size: 576 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 1
Size: 2166 Color: 5
Size: 312 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 9
Size: 2298 Color: 16
Size: 110 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 16
Size: 1324 Color: 17
Size: 1096 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12553 Color: 0
Size: 1757 Color: 8
Size: 602 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 18
Size: 1368 Color: 19
Size: 928 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12663 Color: 4
Size: 1607 Color: 16
Size: 642 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12680 Color: 2
Size: 1864 Color: 10
Size: 368 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12751 Color: 9
Size: 1769 Color: 11
Size: 392 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 5
Size: 1448 Color: 18
Size: 720 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12770 Color: 3
Size: 1768 Color: 15
Size: 374 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12832 Color: 9
Size: 1744 Color: 0
Size: 336 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12803 Color: 17
Size: 1661 Color: 0
Size: 448 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 2
Size: 1240 Color: 0
Size: 864 Color: 13

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 3
Size: 1286 Color: 5
Size: 792 Color: 9

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 5
Size: 1512 Color: 14
Size: 528 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12894 Color: 3
Size: 1232 Color: 3
Size: 786 Color: 14

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 9
Size: 1642 Color: 15
Size: 324 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 19
Size: 1492 Color: 14
Size: 484 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 13
Size: 1302 Color: 5
Size: 600 Color: 8

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13098 Color: 11
Size: 1470 Color: 13
Size: 344 Color: 16

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 9
Size: 1072 Color: 12
Size: 728 Color: 14

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 10
Size: 1464 Color: 19
Size: 298 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 11
Size: 1240 Color: 7
Size: 512 Color: 9

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13201 Color: 19
Size: 1427 Color: 15
Size: 284 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13214 Color: 8
Size: 1242 Color: 7
Size: 456 Color: 18

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 9
Size: 1240 Color: 18
Size: 412 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13253 Color: 15
Size: 1383 Color: 3
Size: 276 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13266 Color: 0
Size: 876 Color: 9
Size: 770 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13348 Color: 1
Size: 1308 Color: 16
Size: 256 Color: 3

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 7496 Color: 6
Size: 5487 Color: 3
Size: 1928 Color: 19

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 7996 Color: 5
Size: 6211 Color: 17
Size: 704 Color: 6

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 8329 Color: 0
Size: 6214 Color: 4
Size: 368 Color: 2

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 8327 Color: 16
Size: 6168 Color: 18
Size: 416 Color: 18

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 9706 Color: 13
Size: 4885 Color: 1
Size: 320 Color: 12

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 10297 Color: 17
Size: 4098 Color: 7
Size: 516 Color: 6

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 11097 Color: 9
Size: 2358 Color: 3
Size: 1456 Color: 5

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 11118 Color: 3
Size: 3161 Color: 5
Size: 632 Color: 16

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 11297 Color: 6
Size: 3342 Color: 19
Size: 272 Color: 0

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 11428 Color: 1
Size: 3483 Color: 15

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 11618 Color: 15
Size: 2977 Color: 15
Size: 316 Color: 2

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 11943 Color: 11
Size: 2968 Color: 6

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 12086 Color: 9
Size: 2337 Color: 18
Size: 488 Color: 14

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 12457 Color: 15
Size: 2454 Color: 7

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 12599 Color: 0
Size: 1444 Color: 13
Size: 868 Color: 7

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13057 Color: 9
Size: 1374 Color: 0
Size: 480 Color: 15

Bin 83: 2 of cap free
Amount of items: 11
Items: 
Size: 7458 Color: 12
Size: 816 Color: 11
Size: 816 Color: 8
Size: 816 Color: 0
Size: 802 Color: 6
Size: 800 Color: 18
Size: 778 Color: 14
Size: 768 Color: 15
Size: 768 Color: 9
Size: 672 Color: 2
Size: 416 Color: 9

Bin 84: 2 of cap free
Amount of items: 9
Items: 
Size: 7460 Color: 18
Size: 1056 Color: 12
Size: 1008 Color: 4
Size: 992 Color: 2
Size: 976 Color: 18
Size: 976 Color: 18
Size: 976 Color: 12
Size: 774 Color: 2
Size: 692 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 9
Size: 6204 Color: 2
Size: 384 Color: 8

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 8326 Color: 17
Size: 6224 Color: 19
Size: 360 Color: 6

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 10693 Color: 6
Size: 3881 Color: 2
Size: 336 Color: 12

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 3
Size: 3668 Color: 9
Size: 504 Color: 3

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11746 Color: 17
Size: 1842 Color: 9
Size: 1322 Color: 15

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 12324 Color: 14
Size: 2586 Color: 16

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 12562 Color: 8
Size: 2160 Color: 9
Size: 188 Color: 5

Bin 92: 2 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 4
Size: 1786 Color: 12

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 16
Size: 1734 Color: 3

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 13
Size: 1676 Color: 14

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 13354 Color: 14
Size: 1152 Color: 12
Size: 404 Color: 9

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 13362 Color: 6
Size: 1548 Color: 0

Bin 97: 3 of cap free
Amount of items: 14
Items: 
Size: 7457 Color: 18
Size: 648 Color: 13
Size: 640 Color: 14
Size: 640 Color: 2
Size: 640 Color: 0
Size: 626 Color: 17
Size: 624 Color: 8
Size: 596 Color: 15
Size: 576 Color: 0
Size: 548 Color: 7
Size: 546 Color: 1
Size: 544 Color: 17
Size: 488 Color: 4
Size: 336 Color: 4

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 8844 Color: 4
Size: 5489 Color: 13
Size: 576 Color: 8

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 9051 Color: 13
Size: 5494 Color: 3
Size: 364 Color: 12

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 10257 Color: 16
Size: 4524 Color: 5
Size: 128 Color: 14

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 10281 Color: 16
Size: 4272 Color: 11
Size: 356 Color: 7

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 10
Size: 3901 Color: 5
Size: 304 Color: 3

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 11425 Color: 9
Size: 2908 Color: 14
Size: 576 Color: 0

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 12627 Color: 19
Size: 1586 Color: 9
Size: 696 Color: 15

Bin 105: 3 of cap free
Amount of items: 3
Items: 
Size: 12977 Color: 19
Size: 1656 Color: 7
Size: 276 Color: 9

Bin 106: 3 of cap free
Amount of items: 2
Items: 
Size: 13034 Color: 19
Size: 1875 Color: 2

Bin 107: 3 of cap free
Amount of items: 2
Items: 
Size: 13133 Color: 16
Size: 1776 Color: 12

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 7528 Color: 16
Size: 6980 Color: 9
Size: 400 Color: 13

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 10193 Color: 11
Size: 2503 Color: 9
Size: 2212 Color: 10

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 10868 Color: 10
Size: 4040 Color: 1

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 11
Size: 3124 Color: 8

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 12305 Color: 2
Size: 2067 Color: 9
Size: 536 Color: 18

Bin 113: 4 of cap free
Amount of items: 3
Items: 
Size: 12368 Color: 16
Size: 2472 Color: 7
Size: 68 Color: 12

Bin 114: 4 of cap free
Amount of items: 2
Items: 
Size: 12716 Color: 0
Size: 2192 Color: 16

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 19
Size: 1888 Color: 2
Size: 32 Color: 14

Bin 116: 5 of cap free
Amount of items: 2
Items: 
Size: 12243 Color: 5
Size: 2664 Color: 8

Bin 117: 5 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 12
Size: 2123 Color: 19

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 9376 Color: 8
Size: 3933 Color: 9
Size: 1597 Color: 5

Bin 119: 6 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 5
Size: 4394 Color: 15
Size: 440 Color: 4

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 12481 Color: 1
Size: 2425 Color: 12

Bin 121: 6 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 14
Size: 2264 Color: 11

Bin 122: 6 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 8
Size: 1682 Color: 5

Bin 123: 7 of cap free
Amount of items: 8
Items: 
Size: 7461 Color: 16
Size: 1232 Color: 4
Size: 1232 Color: 2
Size: 1216 Color: 19
Size: 1096 Color: 10
Size: 1096 Color: 0
Size: 1072 Color: 9
Size: 500 Color: 19

Bin 124: 7 of cap free
Amount of items: 3
Items: 
Size: 7476 Color: 15
Size: 6213 Color: 6
Size: 1216 Color: 16

Bin 125: 7 of cap free
Amount of items: 3
Items: 
Size: 10233 Color: 4
Size: 4092 Color: 7
Size: 580 Color: 8

Bin 126: 7 of cap free
Amount of items: 2
Items: 
Size: 10452 Color: 3
Size: 4453 Color: 6

Bin 127: 7 of cap free
Amount of items: 2
Items: 
Size: 10558 Color: 7
Size: 4347 Color: 17

Bin 128: 7 of cap free
Amount of items: 3
Items: 
Size: 11121 Color: 2
Size: 3312 Color: 4
Size: 472 Color: 9

Bin 129: 7 of cap free
Amount of items: 2
Items: 
Size: 11664 Color: 8
Size: 3241 Color: 14

Bin 130: 7 of cap free
Amount of items: 2
Items: 
Size: 11692 Color: 10
Size: 3213 Color: 6

Bin 131: 7 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 11
Size: 1705 Color: 2

Bin 132: 7 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 2
Size: 1525 Color: 12
Size: 10 Color: 16

Bin 133: 8 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 4
Size: 5712 Color: 13
Size: 672 Color: 7

Bin 134: 8 of cap free
Amount of items: 3
Items: 
Size: 9896 Color: 16
Size: 4680 Color: 4
Size: 328 Color: 0

Bin 135: 8 of cap free
Amount of items: 2
Items: 
Size: 11057 Color: 0
Size: 3847 Color: 6

Bin 136: 8 of cap free
Amount of items: 2
Items: 
Size: 12008 Color: 16
Size: 2896 Color: 2

Bin 137: 8 of cap free
Amount of items: 2
Items: 
Size: 12158 Color: 17
Size: 2746 Color: 19

Bin 138: 9 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 7
Size: 1547 Color: 5
Size: 32 Color: 3

Bin 139: 10 of cap free
Amount of items: 3
Items: 
Size: 9046 Color: 17
Size: 5024 Color: 4
Size: 832 Color: 17

Bin 140: 10 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 8
Size: 2642 Color: 0

Bin 141: 10 of cap free
Amount of items: 3
Items: 
Size: 12314 Color: 1
Size: 2436 Color: 8
Size: 152 Color: 17

Bin 142: 10 of cap free
Amount of items: 2
Items: 
Size: 12780 Color: 2
Size: 2122 Color: 19

Bin 143: 10 of cap free
Amount of items: 2
Items: 
Size: 13086 Color: 17
Size: 1816 Color: 14

Bin 144: 11 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 15
Size: 3181 Color: 3

Bin 145: 11 of cap free
Amount of items: 2
Items: 
Size: 12240 Color: 5
Size: 2661 Color: 3

Bin 146: 11 of cap free
Amount of items: 2
Items: 
Size: 13121 Color: 12
Size: 1780 Color: 14

Bin 147: 12 of cap free
Amount of items: 3
Items: 
Size: 9042 Color: 8
Size: 5490 Color: 15
Size: 368 Color: 6

Bin 148: 12 of cap free
Amount of items: 2
Items: 
Size: 9360 Color: 0
Size: 5540 Color: 17

Bin 149: 12 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 16
Size: 2940 Color: 15

Bin 150: 12 of cap free
Amount of items: 2
Items: 
Size: 12660 Color: 10
Size: 2240 Color: 13

Bin 151: 13 of cap free
Amount of items: 2
Items: 
Size: 11145 Color: 15
Size: 3754 Color: 2

Bin 152: 14 of cap free
Amount of items: 2
Items: 
Size: 11757 Color: 15
Size: 3141 Color: 1

Bin 153: 15 of cap free
Amount of items: 2
Items: 
Size: 10516 Color: 15
Size: 4381 Color: 16

Bin 154: 15 of cap free
Amount of items: 2
Items: 
Size: 12930 Color: 6
Size: 1967 Color: 4

Bin 155: 16 of cap free
Amount of items: 3
Items: 
Size: 9655 Color: 4
Size: 3482 Color: 18
Size: 1759 Color: 17

Bin 156: 16 of cap free
Amount of items: 2
Items: 
Size: 11440 Color: 17
Size: 3456 Color: 5

Bin 157: 16 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 13
Size: 1836 Color: 15

Bin 158: 16 of cap free
Amount of items: 2
Items: 
Size: 13232 Color: 1
Size: 1664 Color: 13

Bin 159: 16 of cap free
Amount of items: 2
Items: 
Size: 13330 Color: 12
Size: 1566 Color: 0

Bin 160: 17 of cap free
Amount of items: 2
Items: 
Size: 11909 Color: 14
Size: 2986 Color: 15

Bin 161: 18 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 1
Size: 2733 Color: 11
Size: 2153 Color: 3

Bin 162: 18 of cap free
Amount of items: 2
Items: 
Size: 10960 Color: 11
Size: 3934 Color: 8

Bin 163: 18 of cap free
Amount of items: 2
Items: 
Size: 12867 Color: 11
Size: 2027 Color: 1

Bin 164: 20 of cap free
Amount of items: 3
Items: 
Size: 9657 Color: 4
Size: 4883 Color: 18
Size: 352 Color: 8

Bin 165: 20 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 3
Size: 2312 Color: 6

Bin 166: 20 of cap free
Amount of items: 2
Items: 
Size: 13188 Color: 14
Size: 1704 Color: 4

Bin 167: 20 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 16
Size: 1604 Color: 0

Bin 168: 21 of cap free
Amount of items: 2
Items: 
Size: 12997 Color: 10
Size: 1894 Color: 4

Bin 169: 23 of cap free
Amount of items: 2
Items: 
Size: 11633 Color: 14
Size: 3256 Color: 3

Bin 170: 24 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 14
Size: 2736 Color: 3

Bin 171: 25 of cap free
Amount of items: 2
Items: 
Size: 12203 Color: 6
Size: 2684 Color: 16

Bin 172: 27 of cap free
Amount of items: 2
Items: 
Size: 12329 Color: 14
Size: 2556 Color: 10

Bin 173: 28 of cap free
Amount of items: 6
Items: 
Size: 7462 Color: 18
Size: 2108 Color: 17
Size: 1522 Color: 4
Size: 1402 Color: 2
Size: 1294 Color: 19
Size: 1096 Color: 8

Bin 174: 28 of cap free
Amount of items: 3
Items: 
Size: 10224 Color: 13
Size: 4524 Color: 0
Size: 136 Color: 11

Bin 175: 28 of cap free
Amount of items: 2
Items: 
Size: 13083 Color: 10
Size: 1801 Color: 6

Bin 176: 29 of cap free
Amount of items: 2
Items: 
Size: 12921 Color: 4
Size: 1962 Color: 17

Bin 177: 30 of cap free
Amount of items: 3
Items: 
Size: 10012 Color: 13
Size: 4342 Color: 17
Size: 528 Color: 8

Bin 178: 30 of cap free
Amount of items: 2
Items: 
Size: 11810 Color: 5
Size: 3072 Color: 12

Bin 179: 33 of cap free
Amount of items: 2
Items: 
Size: 12706 Color: 3
Size: 2173 Color: 15

Bin 180: 38 of cap free
Amount of items: 5
Items: 
Size: 7464 Color: 16
Size: 2345 Color: 8
Size: 2225 Color: 13
Size: 2008 Color: 0
Size: 832 Color: 6

Bin 181: 40 of cap free
Amount of items: 2
Items: 
Size: 12568 Color: 18
Size: 2304 Color: 13

Bin 182: 42 of cap free
Amount of items: 2
Items: 
Size: 10142 Color: 13
Size: 4728 Color: 10

Bin 183: 43 of cap free
Amount of items: 2
Items: 
Size: 12985 Color: 12
Size: 1884 Color: 6

Bin 184: 44 of cap free
Amount of items: 2
Items: 
Size: 12848 Color: 5
Size: 2020 Color: 11

Bin 185: 44 of cap free
Amount of items: 2
Items: 
Size: 12908 Color: 6
Size: 1960 Color: 5

Bin 186: 45 of cap free
Amount of items: 5
Items: 
Size: 7472 Color: 15
Size: 2918 Color: 11
Size: 2631 Color: 19
Size: 1654 Color: 6
Size: 192 Color: 9

Bin 187: 47 of cap free
Amount of items: 2
Items: 
Size: 9697 Color: 9
Size: 5168 Color: 2

Bin 188: 56 of cap free
Amount of items: 3
Items: 
Size: 9808 Color: 15
Size: 4152 Color: 14
Size: 896 Color: 8

Bin 189: 67 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 14
Size: 2475 Color: 12

Bin 190: 70 of cap free
Amount of items: 2
Items: 
Size: 9948 Color: 12
Size: 4894 Color: 15

Bin 191: 74 of cap free
Amount of items: 2
Items: 
Size: 11466 Color: 16
Size: 3372 Color: 8

Bin 192: 78 of cap free
Amount of items: 2
Items: 
Size: 9944 Color: 19
Size: 4890 Color: 1

Bin 193: 80 of cap free
Amount of items: 2
Items: 
Size: 10648 Color: 16
Size: 4184 Color: 15

Bin 194: 86 of cap free
Amount of items: 2
Items: 
Size: 10906 Color: 11
Size: 3920 Color: 5

Bin 195: 95 of cap free
Amount of items: 2
Items: 
Size: 9053 Color: 6
Size: 5764 Color: 3

Bin 196: 112 of cap free
Amount of items: 2
Items: 
Size: 8784 Color: 8
Size: 6016 Color: 12

Bin 197: 134 of cap free
Amount of items: 2
Items: 
Size: 9642 Color: 9
Size: 5136 Color: 12

Bin 198: 184 of cap free
Amount of items: 37
Items: 
Size: 634 Color: 3
Size: 530 Color: 15
Size: 524 Color: 2
Size: 512 Color: 1
Size: 494 Color: 19
Size: 480 Color: 9
Size: 480 Color: 4
Size: 468 Color: 9
Size: 456 Color: 3
Size: 448 Color: 9
Size: 448 Color: 3
Size: 444 Color: 17
Size: 440 Color: 19
Size: 432 Color: 10
Size: 430 Color: 16
Size: 416 Color: 4
Size: 400 Color: 2
Size: 388 Color: 10
Size: 384 Color: 8
Size: 384 Color: 7
Size: 384 Color: 0
Size: 368 Color: 1
Size: 352 Color: 17
Size: 352 Color: 10
Size: 344 Color: 7
Size: 340 Color: 13
Size: 328 Color: 17
Size: 320 Color: 16
Size: 320 Color: 13
Size: 320 Color: 13
Size: 320 Color: 10
Size: 320 Color: 5
Size: 320 Color: 4
Size: 308 Color: 11
Size: 304 Color: 6
Size: 280 Color: 16
Size: 256 Color: 8

Bin 199: 12520 of cap free
Amount of items: 9
Items: 
Size: 296 Color: 7
Size: 280 Color: 13
Size: 272 Color: 12
Size: 264 Color: 18
Size: 256 Color: 16
Size: 256 Color: 15
Size: 256 Color: 8
Size: 256 Color: 3
Size: 256 Color: 0

Total size: 2952576
Total free space: 14912

