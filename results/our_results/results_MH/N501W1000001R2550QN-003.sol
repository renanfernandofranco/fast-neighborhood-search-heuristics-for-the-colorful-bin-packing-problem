Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 447016 Color: 452
Size: 302854 Color: 210
Size: 250131 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 449785 Color: 455
Size: 290548 Color: 167
Size: 259668 Color: 60

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 408104 Color: 419
Size: 323780 Color: 268
Size: 268117 Color: 93

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 388398 Color: 393
Size: 343994 Color: 318
Size: 267609 Color: 90

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 445314 Color: 451
Size: 300935 Color: 206
Size: 253752 Color: 20

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 352
Size: 344694 Color: 320
Size: 291825 Color: 170

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 426462 Color: 437
Size: 290701 Color: 169
Size: 282838 Color: 150

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 466840 Color: 472
Size: 259330 Color: 57
Size: 273831 Color: 118

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 407915 Color: 418
Size: 328856 Color: 282
Size: 263230 Color: 73

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374389 Color: 368
Size: 358165 Color: 344
Size: 267447 Color: 88

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 342186 Color: 315
Size: 335898 Color: 301
Size: 321917 Color: 262

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 458045 Color: 464
Size: 271512 Color: 110
Size: 270444 Color: 105

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 459662 Color: 466
Size: 272019 Color: 113
Size: 268320 Color: 95

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 409755 Color: 420
Size: 318004 Color: 251
Size: 272242 Color: 114

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 383168 Color: 382
Size: 316621 Color: 249
Size: 300212 Color: 202

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 371705 Color: 364
Size: 332269 Color: 293
Size: 296027 Color: 187

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 403757 Color: 414
Size: 341440 Color: 314
Size: 254804 Color: 26

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 384700 Color: 388
Size: 332133 Color: 292
Size: 283168 Color: 152

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 485707 Color: 490
Size: 260827 Color: 64
Size: 253467 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 472581 Color: 476
Size: 274799 Color: 123
Size: 252621 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 455860 Color: 462
Size: 277120 Color: 135
Size: 267021 Color: 86

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 477672 Color: 483
Size: 263707 Color: 75
Size: 258622 Color: 52

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 388897 Color: 394
Size: 325766 Color: 272
Size: 285338 Color: 156

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 440659 Color: 447
Size: 290172 Color: 165
Size: 269170 Color: 98

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 479456 Color: 485
Size: 264073 Color: 76
Size: 256472 Color: 36

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 365635 Color: 356
Size: 331872 Color: 289
Size: 302494 Color: 209

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 361982 Color: 351
Size: 322767 Color: 265
Size: 315252 Color: 245

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 416530 Color: 430
Size: 325864 Color: 274
Size: 257607 Color: 44

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 377290 Color: 375
Size: 316037 Color: 247
Size: 306674 Color: 217

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 498957 Color: 500
Size: 250831 Color: 6
Size: 250213 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 436472 Color: 444
Size: 300893 Color: 204
Size: 262636 Color: 71

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 487461 Color: 494
Size: 258720 Color: 53
Size: 253820 Color: 21

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 464889 Color: 470
Size: 278058 Color: 136
Size: 257054 Color: 41

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 348823 Color: 328
Size: 327702 Color: 280
Size: 323476 Color: 267

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 451019 Color: 457
Size: 290563 Color: 168
Size: 258419 Color: 51

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 340723 Color: 312
Size: 333555 Color: 296
Size: 325723 Color: 271

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 483225 Color: 488
Size: 259281 Color: 56
Size: 257495 Color: 43

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 338642 Color: 306
Size: 332397 Color: 294
Size: 328962 Color: 284

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 487344 Color: 493
Size: 257026 Color: 40
Size: 255631 Color: 31

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 398367 Color: 406
Size: 326825 Color: 278
Size: 274809 Color: 124

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 376375 Color: 371
Size: 312729 Color: 235
Size: 310897 Color: 227

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 365921 Color: 357
Size: 322798 Color: 266
Size: 311282 Color: 229

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 410559 Color: 423
Size: 320597 Color: 258
Size: 268845 Color: 96

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 494919 Color: 498
Size: 254517 Color: 24
Size: 250565 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 377794 Color: 376
Size: 314833 Color: 243
Size: 307374 Color: 219

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 473300 Color: 478
Size: 269803 Color: 101
Size: 256898 Color: 39

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 367682 Color: 360
Size: 342432 Color: 317
Size: 289887 Color: 162

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 412730 Color: 427
Size: 294661 Color: 180
Size: 292610 Color: 172

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 395445 Color: 402
Size: 314568 Color: 241
Size: 289988 Color: 164

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 423191 Color: 435
Size: 315623 Color: 246
Size: 261187 Color: 67

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 406486 Color: 417
Size: 296897 Color: 189
Size: 296618 Color: 188

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 357375 Color: 338
Size: 325409 Color: 270
Size: 317217 Color: 250

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 391153 Color: 397
Size: 333421 Color: 295
Size: 275427 Color: 126

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 358082 Color: 343
Size: 354081 Color: 335
Size: 287838 Color: 160

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 370941 Color: 363
Size: 321555 Color: 260
Size: 307505 Color: 220

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 436160 Color: 443
Size: 297997 Color: 198
Size: 265844 Color: 84

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 395568 Color: 404
Size: 309700 Color: 223
Size: 294733 Color: 181

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 448686 Color: 454
Size: 295983 Color: 185
Size: 255332 Color: 29

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 356478 Color: 337
Size: 331044 Color: 288
Size: 312479 Color: 234

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 381780 Color: 380
Size: 320935 Color: 259
Size: 297286 Color: 190

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 394029 Color: 401
Size: 329203 Color: 285
Size: 276769 Color: 133

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 474406 Color: 479
Size: 264632 Color: 78
Size: 260963 Color: 66

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 406474 Color: 416
Size: 338815 Color: 308
Size: 254712 Color: 25

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 448247 Color: 453
Size: 293892 Color: 177
Size: 257862 Color: 46

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 492611 Color: 496
Size: 255473 Color: 30
Size: 251917 Color: 10

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 361567 Color: 349
Size: 336038 Color: 303
Size: 302396 Color: 208

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 353237 Color: 334
Size: 333586 Color: 297
Size: 313178 Color: 237

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 465384 Color: 471
Size: 275581 Color: 128
Size: 259036 Color: 55

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 395478 Color: 403
Size: 309963 Color: 225
Size: 294560 Color: 179

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 462980 Color: 469
Size: 286200 Color: 158
Size: 250821 Color: 5

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 380129 Color: 377
Size: 329690 Color: 286
Size: 290182 Color: 166

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 452793 Color: 458
Size: 275745 Color: 129
Size: 271463 Color: 109

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 386118 Color: 390
Size: 314258 Color: 239
Size: 299625 Color: 199

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 372274 Color: 366
Size: 351619 Color: 331
Size: 276108 Color: 131

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 441760 Color: 448
Size: 293143 Color: 174
Size: 265098 Color: 80

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 487001 Color: 492
Size: 256604 Color: 37
Size: 256396 Color: 35

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 474966 Color: 480
Size: 269363 Color: 99
Size: 255672 Color: 32

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 357854 Color: 341
Size: 335602 Color: 300
Size: 306545 Color: 216

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 398738 Color: 407
Size: 303894 Color: 211
Size: 297369 Color: 191

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 392923 Color: 399
Size: 351279 Color: 330
Size: 255799 Color: 33

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 376424 Color: 372
Size: 361100 Color: 348
Size: 262477 Color: 70

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 365248 Color: 355
Size: 353115 Color: 333
Size: 281638 Color: 147

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 468001 Color: 473
Size: 273787 Color: 117
Size: 258213 Color: 50

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 438873 Color: 445
Size: 294842 Color: 184
Size: 266286 Color: 85

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 485204 Color: 489
Size: 258032 Color: 47
Size: 256765 Color: 38

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 361808 Color: 350
Size: 337276 Color: 304
Size: 300917 Color: 205

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 400407 Color: 410
Size: 307337 Color: 218
Size: 292257 Color: 171

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 382320 Color: 381
Size: 357485 Color: 339
Size: 260196 Color: 62

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 432982 Color: 439
Size: 294287 Color: 178
Size: 272732 Color: 115

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 472789 Color: 477
Size: 271291 Color: 108
Size: 255921 Color: 34

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 390457 Color: 396
Size: 357715 Color: 340
Size: 251829 Color: 9

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 450718 Color: 456
Size: 279467 Color: 139
Size: 269816 Color: 102

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 374430 Color: 369
Size: 345193 Color: 322
Size: 280378 Color: 143

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 411646 Color: 424
Size: 304854 Color: 213
Size: 283501 Color: 153

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 410170 Color: 422
Size: 335956 Color: 302
Size: 253875 Color: 22

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 342332 Color: 316
Size: 330898 Color: 287
Size: 326771 Color: 277

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 452907 Color: 460
Size: 276951 Color: 134
Size: 270143 Color: 103

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 405047 Color: 415
Size: 318400 Color: 252
Size: 276554 Color: 132

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 358444 Color: 345
Size: 335414 Color: 299
Size: 306143 Color: 215

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 413060 Color: 428
Size: 316360 Color: 248
Size: 270581 Color: 106

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 482841 Color: 487
Size: 264487 Color: 77
Size: 252673 Color: 14

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 383346 Color: 383
Size: 350950 Color: 329
Size: 265705 Color: 83

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 434431 Color: 440
Size: 294738 Color: 182
Size: 270832 Color: 107

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 409840 Color: 421
Size: 309519 Color: 222
Size: 280642 Color: 144

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 415320 Color: 429
Size: 315090 Color: 244
Size: 269591 Color: 100

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 442356 Color: 449
Size: 304951 Color: 214
Size: 252694 Color: 15

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 435884 Color: 442
Size: 312477 Color: 233
Size: 251640 Color: 7

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 393318 Color: 400
Size: 325802 Color: 273
Size: 280881 Color: 145

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 452898 Color: 459
Size: 274143 Color: 120
Size: 272960 Color: 116

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 412498 Color: 426
Size: 312067 Color: 232
Size: 275436 Color: 127

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 371790 Color: 365
Size: 345861 Color: 323
Size: 282350 Color: 149

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 488220 Color: 495
Size: 261722 Color: 69
Size: 250059 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 359685 Color: 347
Size: 320283 Color: 256
Size: 320033 Color: 255

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 355100 Color: 336
Size: 346914 Color: 325
Size: 297987 Color: 197

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 396616 Color: 405
Size: 321854 Color: 261
Size: 281531 Color: 146

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 400734 Color: 412
Size: 338716 Color: 307
Size: 260551 Color: 63

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 494473 Color: 497
Size: 253122 Color: 16
Size: 252406 Color: 11

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 392132 Color: 398
Size: 340128 Color: 309
Size: 267741 Color: 92

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 399898 Color: 409
Size: 331909 Color: 290
Size: 268194 Color: 94

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 368482 Color: 361
Size: 348362 Color: 326
Size: 283157 Color: 151

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 422781 Color: 434
Size: 297413 Color: 192
Size: 279807 Color: 141

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 383550 Color: 384
Size: 337302 Color: 305
Size: 279149 Color: 138

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 402302 Color: 413
Size: 326120 Color: 275
Size: 271579 Color: 112

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 400655 Color: 411
Size: 341135 Color: 313
Size: 258211 Color: 49

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 478591 Color: 484
Size: 263661 Color: 74
Size: 257749 Color: 45

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 399582 Color: 408
Size: 312964 Color: 236
Size: 287455 Color: 159

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 477134 Color: 482
Size: 264827 Color: 79
Size: 258040 Color: 48

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 497211 Color: 499
Size: 252449 Color: 12
Size: 250341 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 472490 Color: 475
Size: 274047 Color: 119
Size: 253464 Color: 17

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 417577 Color: 433
Size: 314771 Color: 242
Size: 267653 Color: 91

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 366409 Color: 359
Size: 322194 Color: 263
Size: 311398 Color: 230

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 359151 Color: 346
Size: 332002 Color: 291
Size: 308848 Color: 221

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 454756 Color: 461
Size: 283910 Color: 154
Size: 261335 Color: 68

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 456213 Color: 463
Size: 274925 Color: 125
Size: 268863 Color: 97

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 486361 Color: 491
Size: 259479 Color: 58
Size: 254161 Color: 23

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 481012 Color: 486
Size: 265417 Color: 81
Size: 253572 Color: 19

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 417529 Color: 432
Size: 297879 Color: 195
Size: 284593 Color: 155

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 357919 Color: 342
Size: 344537 Color: 319
Size: 297545 Color: 193

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 475838 Color: 481
Size: 265430 Color: 82
Size: 258733 Color: 54

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 351681 Color: 332
Size: 334113 Color: 298
Size: 314207 Color: 238

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 376425 Color: 373
Size: 366268 Color: 358
Size: 257308 Color: 42

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 440525 Color: 446
Size: 299953 Color: 200
Size: 259523 Color: 59

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 383675 Color: 385
Size: 320309 Color: 257
Size: 296017 Color: 186

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 444277 Color: 450
Size: 292916 Color: 173
Size: 262808 Color: 72

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 370375 Color: 362
Size: 328886 Color: 283
Size: 300740 Color: 203

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 365019 Color: 354
Size: 346337 Color: 324
Size: 288645 Color: 161

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 380593 Color: 378
Size: 345176 Color: 321
Size: 274232 Color: 121

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 376978 Color: 374
Size: 348420 Color: 327
Size: 274603 Color: 122

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 425517 Color: 436
Size: 319422 Color: 253
Size: 255062 Color: 27

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 374304 Color: 367
Size: 327806 Color: 281
Size: 297891 Color: 196

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 468224 Color: 474
Size: 280053 Color: 142
Size: 251724 Color: 8

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 428193 Color: 438
Size: 311816 Color: 231
Size: 259992 Color: 61

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 411785 Color: 425
Size: 309821 Color: 224
Size: 278395 Color: 137

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 384352 Color: 387
Size: 311265 Color: 228
Size: 304384 Color: 212

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 385589 Color: 389
Size: 314328 Color: 240
Size: 300084 Color: 201

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 391
Size: 319831 Color: 254
Size: 293343 Color: 176

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 461057 Color: 467
Size: 271515 Color: 111
Size: 267429 Color: 87

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 383748 Color: 386
Size: 340364 Color: 311
Size: 275889 Color: 130

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 462601 Color: 468
Size: 282315 Color: 148
Size: 255085 Color: 28

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 353
Size: 326512 Color: 276
Size: 310007 Color: 226

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 389305 Color: 395
Size: 340263 Color: 310
Size: 270433 Color: 104

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 459388 Color: 465
Size: 279710 Color: 140
Size: 260903 Color: 65

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 387018 Color: 392
Size: 327253 Color: 279
Size: 285730 Color: 157

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 380610 Color: 379
Size: 324625 Color: 269
Size: 294766 Color: 183

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 375992 Color: 370
Size: 322547 Color: 264
Size: 301462 Color: 207

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 416793 Color: 431
Size: 293293 Color: 175
Size: 289915 Color: 163

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 434772 Color: 441
Size: 297765 Color: 194
Size: 267464 Color: 89

Total size: 167000167
Total free space: 0

