Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 1000 Color: 4
Size: 840 Color: 2
Size: 272 Color: 3
Size: 272 Color: 1
Size: 64 Color: 0
Size: 16 Color: 0
Size: 8 Color: 2

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1704 Color: 2
Size: 576 Color: 0
Size: 104 Color: 4
Size: 88 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2077 Color: 2
Size: 331 Color: 3
Size: 64 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 3
Size: 236 Color: 4
Size: 24 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 0
Size: 881 Color: 4
Size: 176 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2025 Color: 0
Size: 373 Color: 1
Size: 74 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 3
Size: 1027 Color: 1
Size: 204 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 3
Size: 618 Color: 4
Size: 120 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1948 Color: 3
Size: 444 Color: 2
Size: 80 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 514 Color: 3
Size: 100 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 4
Size: 793 Color: 4
Size: 158 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 2
Size: 685 Color: 2
Size: 136 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 1
Size: 484 Color: 2
Size: 96 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 662 Color: 2
Size: 100 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 766 Color: 4
Size: 152 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1030 Color: 0
Size: 204 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 3
Size: 262 Color: 4
Size: 52 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 3
Size: 546 Color: 3
Size: 108 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 4
Size: 427 Color: 4
Size: 84 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 3
Size: 228 Color: 3
Size: 40 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 0
Size: 667 Color: 4
Size: 132 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1996 Color: 3
Size: 404 Color: 2
Size: 72 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 0
Size: 434 Color: 3
Size: 56 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1885 Color: 4
Size: 491 Color: 3
Size: 96 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 2
Size: 673 Color: 4
Size: 134 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 3
Size: 342 Color: 0
Size: 68 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 4
Size: 681 Color: 1
Size: 134 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 1
Size: 1028 Color: 4
Size: 200 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 4
Size: 300 Color: 2
Size: 56 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 0
Size: 1022 Color: 3
Size: 204 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 1
Size: 385 Color: 0
Size: 76 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 1
Size: 462 Color: 2
Size: 36 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 567 Color: 1
Size: 112 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 1
Size: 242 Color: 0
Size: 48 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 1
Size: 462 Color: 0
Size: 88 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 0
Size: 892 Color: 4
Size: 176 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 4
Size: 930 Color: 1
Size: 84 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 1
Size: 302 Color: 0
Size: 32 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1953 Color: 3
Size: 433 Color: 0
Size: 86 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 4
Size: 538 Color: 1
Size: 84 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 0
Size: 382 Color: 1
Size: 72 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 3
Size: 1018 Color: 3
Size: 200 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 4
Size: 604 Color: 0
Size: 120 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 581 Color: 1
Size: 114 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 2076 Color: 0
Size: 332 Color: 3
Size: 64 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 2218 Color: 0
Size: 214 Color: 4
Size: 40 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 2
Size: 573 Color: 4
Size: 114 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 3
Size: 276 Color: 4
Size: 48 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 3
Size: 770 Color: 1
Size: 64 Color: 2

Bin 50: 0 of cap free
Amount of items: 4
Items: 
Size: 1828 Color: 4
Size: 540 Color: 4
Size: 96 Color: 1
Size: 8 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 3
Size: 381 Color: 2
Size: 74 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2033 Color: 1
Size: 367 Color: 2
Size: 72 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1871 Color: 3
Size: 501 Color: 0
Size: 100 Color: 0

Bin 54: 0 of cap free
Amount of items: 4
Items: 
Size: 1552 Color: 0
Size: 504 Color: 1
Size: 384 Color: 2
Size: 32 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 2
Size: 364 Color: 0
Size: 72 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1466 Color: 0
Size: 842 Color: 3
Size: 164 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 0
Size: 322 Color: 4
Size: 48 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1877 Color: 4
Size: 497 Color: 4
Size: 98 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 0
Size: 923 Color: 3
Size: 132 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1652 Color: 4
Size: 684 Color: 0
Size: 136 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1540 Color: 0
Size: 780 Color: 4
Size: 152 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 1227 Color: 0
Size: 8 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 1
Size: 546 Color: 1
Size: 44 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 4
Size: 787 Color: 0
Size: 156 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 3
Size: 1029 Color: 2
Size: 204 Color: 3

Total size: 160680
Total free space: 0

