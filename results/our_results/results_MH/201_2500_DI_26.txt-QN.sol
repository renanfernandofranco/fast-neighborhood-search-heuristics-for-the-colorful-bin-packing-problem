Capicity Bin: 2068
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 696 Color: 128
Size: 480 Color: 114
Size: 428 Color: 109
Size: 232 Color: 80
Size: 76 Color: 41
Size: 68 Color: 37
Size: 48 Color: 19
Size: 40 Color: 10

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 147
Size: 694 Color: 127
Size: 64 Color: 31
Size: 56 Color: 29
Size: 16 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 179
Size: 291 Color: 93
Size: 58 Color: 30

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1373 Color: 152
Size: 581 Color: 122
Size: 114 Color: 56

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1641 Color: 172
Size: 357 Color: 101
Size: 70 Color: 40

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 151
Size: 609 Color: 123
Size: 120 Color: 57

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 182
Size: 282 Color: 92
Size: 52 Color: 23

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 194
Size: 218 Color: 77
Size: 40 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 166
Size: 439 Color: 112
Size: 24 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 197
Size: 201 Color: 73
Size: 38 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 158
Size: 513 Color: 117
Size: 102 Color: 52

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 201
Size: 178 Color: 69
Size: 32 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 199
Size: 187 Color: 71
Size: 36 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 161
Size: 461 Color: 113
Size: 90 Color: 48

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 198
Size: 193 Color: 72
Size: 38 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1817 Color: 196
Size: 211 Color: 74
Size: 40 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 145
Size: 737 Color: 129
Size: 146 Color: 63

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 191
Size: 244 Color: 81
Size: 48 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 164
Size: 417 Color: 107
Size: 82 Color: 45

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 1428 Color: 155
Size: 400 Color: 105
Size: 232 Color: 79
Size: 8 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 160
Size: 486 Color: 115
Size: 96 Color: 49

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 178
Size: 329 Color: 95
Size: 64 Color: 33

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 156
Size: 521 Color: 118
Size: 102 Color: 53

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 146
Size: 693 Color: 126
Size: 138 Color: 61

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1612 Color: 169
Size: 320 Color: 94
Size: 136 Color: 60

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 193
Size: 217 Color: 76
Size: 42 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 192
Size: 221 Color: 78
Size: 44 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 165
Size: 402 Color: 106
Size: 76 Color: 44

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 185
Size: 267 Color: 87
Size: 52 Color: 24

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 181
Size: 281 Color: 90
Size: 54 Color: 26

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 188
Size: 262 Color: 85
Size: 48 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 174
Size: 351 Color: 99
Size: 68 Color: 38

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 184
Size: 274 Color: 89
Size: 52 Color: 22

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 195
Size: 214 Color: 75
Size: 40 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1035 Color: 138
Size: 861 Color: 135
Size: 172 Color: 68

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 144
Size: 841 Color: 132
Size: 50 Color: 20

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 176
Size: 342 Color: 97
Size: 64 Color: 32

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 168
Size: 383 Color: 103
Size: 76 Color: 43

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 150
Size: 630 Color: 124
Size: 124 Color: 58

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 159
Size: 507 Color: 116
Size: 100 Color: 51

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 153
Size: 578 Color: 121
Size: 112 Color: 55

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 183
Size: 273 Color: 88
Size: 54 Color: 27

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 143
Size: 770 Color: 131
Size: 152 Color: 64

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 140
Size: 862 Color: 136
Size: 168 Color: 65

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1853 Color: 200
Size: 181 Color: 70
Size: 34 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 163
Size: 421 Color: 108
Size: 84 Color: 47

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 141
Size: 853 Color: 134
Size: 170 Color: 67

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 187
Size: 261 Color: 84
Size: 50 Color: 21

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 190
Size: 247 Color: 82
Size: 48 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 1053 Color: 142
Size: 847 Color: 133
Size: 168 Color: 66

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 170
Size: 438 Color: 111
Size: 8 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 173
Size: 354 Color: 100
Size: 68 Color: 36

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 162
Size: 434 Color: 110
Size: 84 Color: 46

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 177
Size: 331 Color: 96
Size: 64 Color: 34

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 186
Size: 266 Color: 86
Size: 52 Color: 25

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 180
Size: 282 Color: 91
Size: 56 Color: 28

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 171
Size: 362 Color: 102
Size: 68 Color: 39

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 175
Size: 346 Color: 98
Size: 68 Color: 35

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 189
Size: 261 Color: 83
Size: 42 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 157
Size: 522 Color: 119
Size: 100 Color: 50

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 154
Size: 537 Color: 120
Size: 106 Color: 54

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 139
Size: 889 Color: 137
Size: 142 Color: 62

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 167
Size: 386 Color: 104
Size: 76 Color: 42

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 148
Size: 651 Color: 125
Size: 128 Color: 59

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 1300 Color: 149
Size: 768 Color: 130

Total size: 134420
Total free space: 0

