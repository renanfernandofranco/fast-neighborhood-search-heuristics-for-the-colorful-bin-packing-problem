Capicity Bin: 6624
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 3314 Color: 5
Size: 1299 Color: 6
Size: 1222 Color: 17
Size: 673 Color: 12
Size: 116 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3964 Color: 10
Size: 2504 Color: 10
Size: 156 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 6
Size: 1478 Color: 9
Size: 332 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 3
Size: 1644 Color: 18
Size: 70 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 11
Size: 1564 Color: 0
Size: 144 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4974 Color: 6
Size: 1378 Color: 5
Size: 272 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5045 Color: 7
Size: 1409 Color: 5
Size: 170 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 6
Size: 1265 Color: 17
Size: 292 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5107 Color: 5
Size: 1179 Color: 2
Size: 338 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 12
Size: 904 Color: 18
Size: 594 Color: 12

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5211 Color: 14
Size: 893 Color: 17
Size: 520 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 17
Size: 1124 Color: 2
Size: 224 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5282 Color: 13
Size: 1122 Color: 13
Size: 220 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5298 Color: 18
Size: 1074 Color: 8
Size: 252 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5338 Color: 6
Size: 974 Color: 7
Size: 312 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 15
Size: 1196 Color: 16
Size: 72 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5370 Color: 16
Size: 666 Color: 4
Size: 588 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5431 Color: 6
Size: 645 Color: 3
Size: 548 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5397 Color: 15
Size: 971 Color: 12
Size: 256 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5470 Color: 5
Size: 738 Color: 12
Size: 416 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5471 Color: 12
Size: 881 Color: 11
Size: 272 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5546 Color: 15
Size: 962 Color: 11
Size: 116 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5553 Color: 6
Size: 877 Color: 14
Size: 194 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5597 Color: 2
Size: 851 Color: 12
Size: 176 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 4
Size: 818 Color: 6
Size: 204 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 19
Size: 602 Color: 7
Size: 376 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5665 Color: 6
Size: 801 Color: 18
Size: 158 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 550 Color: 5
Size: 384 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5702 Color: 6
Size: 482 Color: 7
Size: 440 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5738 Color: 15
Size: 782 Color: 18
Size: 104 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5739 Color: 14
Size: 739 Color: 18
Size: 146 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 9
Size: 642 Color: 6
Size: 240 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 14
Size: 643 Color: 1
Size: 216 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5778 Color: 2
Size: 742 Color: 12
Size: 104 Color: 10

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5826 Color: 6
Size: 544 Color: 5
Size: 254 Color: 16

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5816 Color: 15
Size: 580 Color: 5
Size: 228 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 12
Size: 575 Color: 6
Size: 232 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 7
Size: 589 Color: 12
Size: 192 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5851 Color: 10
Size: 613 Color: 0
Size: 160 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 4
Size: 644 Color: 19
Size: 128 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5898 Color: 3
Size: 420 Color: 14
Size: 306 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5930 Color: 6
Size: 550 Color: 19
Size: 144 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5919 Color: 8
Size: 581 Color: 13
Size: 124 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5924 Color: 3
Size: 492 Color: 11
Size: 208 Color: 0

Bin 45: 1 of cap free
Amount of items: 5
Items: 
Size: 3316 Color: 6
Size: 2111 Color: 12
Size: 876 Color: 3
Size: 180 Color: 6
Size: 140 Color: 8

Bin 46: 1 of cap free
Amount of items: 4
Items: 
Size: 3321 Color: 0
Size: 2732 Color: 14
Size: 336 Color: 17
Size: 234 Color: 12

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 18
Size: 2754 Color: 2
Size: 144 Color: 9

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 3866 Color: 13
Size: 2757 Color: 9

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 3940 Color: 11
Size: 2383 Color: 16
Size: 300 Color: 4

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 4062 Color: 16
Size: 2417 Color: 7
Size: 144 Color: 8

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 4391 Color: 2
Size: 2138 Color: 18
Size: 94 Color: 4

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 7
Size: 1569 Color: 12
Size: 456 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 4621 Color: 9
Size: 1868 Color: 9
Size: 134 Color: 4

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 2
Size: 1883 Color: 18
Size: 88 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 4783 Color: 17
Size: 1690 Color: 0
Size: 150 Color: 3

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 5331 Color: 9
Size: 1292 Color: 12

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 5577 Color: 9
Size: 1046 Color: 10

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 5709 Color: 12
Size: 914 Color: 16

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 5906 Color: 1
Size: 717 Color: 3

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 5927 Color: 18
Size: 476 Color: 6
Size: 220 Color: 3

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 3348 Color: 17
Size: 3138 Color: 16
Size: 136 Color: 5

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 9
Size: 2220 Color: 7
Size: 136 Color: 7

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5138 Color: 1
Size: 1204 Color: 6
Size: 280 Color: 11

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 5239 Color: 14
Size: 721 Color: 16
Size: 662 Color: 6

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 5515 Color: 0
Size: 811 Color: 5
Size: 296 Color: 6

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 18
Size: 1106 Color: 1

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 5834 Color: 10
Size: 788 Color: 2

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 5914 Color: 4
Size: 708 Color: 7

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 5935 Color: 17
Size: 687 Color: 13

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 4228 Color: 6
Size: 2393 Color: 16

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 5371 Color: 7
Size: 1250 Color: 4

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 7
Size: 977 Color: 14

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 5801 Color: 16
Size: 820 Color: 2

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 3753 Color: 11
Size: 2751 Color: 17
Size: 116 Color: 15

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 4564 Color: 15
Size: 1940 Color: 5
Size: 116 Color: 12

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 5396 Color: 14
Size: 1224 Color: 12

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 5762 Color: 1
Size: 858 Color: 18

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 5818 Color: 10
Size: 802 Color: 16

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 6
Size: 1383 Color: 0
Size: 488 Color: 0

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 5188 Color: 13
Size: 1431 Color: 7

Bin 81: 5 of cap free
Amount of items: 3
Items: 
Size: 5304 Color: 8
Size: 1275 Color: 0
Size: 40 Color: 0

Bin 82: 5 of cap free
Amount of items: 3
Items: 
Size: 5669 Color: 9
Size: 924 Color: 1
Size: 26 Color: 14

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 4338 Color: 13
Size: 2280 Color: 12

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 5459 Color: 0
Size: 1159 Color: 2

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 5932 Color: 11
Size: 686 Color: 10

Bin 86: 7 of cap free
Amount of items: 4
Items: 
Size: 3317 Color: 9
Size: 2302 Color: 15
Size: 800 Color: 11
Size: 198 Color: 1

Bin 87: 7 of cap free
Amount of items: 3
Items: 
Size: 3476 Color: 5
Size: 2761 Color: 2
Size: 380 Color: 8

Bin 88: 7 of cap free
Amount of items: 2
Items: 
Size: 4811 Color: 5
Size: 1806 Color: 19

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 14
Size: 961 Color: 8
Size: 212 Color: 6

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 5715 Color: 18
Size: 902 Color: 4

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 3650 Color: 7
Size: 2888 Color: 18
Size: 78 Color: 6

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 5759 Color: 10
Size: 857 Color: 4

Bin 93: 9 of cap free
Amount of items: 5
Items: 
Size: 3313 Color: 8
Size: 1242 Color: 14
Size: 706 Color: 16
Size: 680 Color: 16
Size: 674 Color: 15

Bin 94: 9 of cap free
Amount of items: 3
Items: 
Size: 4458 Color: 12
Size: 2085 Color: 13
Size: 72 Color: 11

Bin 95: 9 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 10
Size: 1861 Color: 6
Size: 214 Color: 4

Bin 96: 9 of cap free
Amount of items: 3
Items: 
Size: 5552 Color: 15
Size: 1023 Color: 18
Size: 40 Color: 4

Bin 97: 10 of cap free
Amount of items: 2
Items: 
Size: 5569 Color: 18
Size: 1045 Color: 16

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 5858 Color: 18
Size: 755 Color: 9

Bin 99: 11 of cap free
Amount of items: 2
Items: 
Size: 5889 Color: 11
Size: 724 Color: 13

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 5076 Color: 7
Size: 1535 Color: 9

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 5004 Color: 3
Size: 1606 Color: 2

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 10
Size: 854 Color: 19

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 4365 Color: 18
Size: 2244 Color: 2

Bin 104: 15 of cap free
Amount of items: 2
Items: 
Size: 5530 Color: 2
Size: 1079 Color: 5

Bin 105: 15 of cap free
Amount of items: 2
Items: 
Size: 5684 Color: 10
Size: 925 Color: 11

Bin 106: 15 of cap free
Amount of items: 2
Items: 
Size: 5887 Color: 8
Size: 722 Color: 4

Bin 107: 16 of cap free
Amount of items: 2
Items: 
Size: 5580 Color: 2
Size: 1028 Color: 3

Bin 108: 17 of cap free
Amount of items: 3
Items: 
Size: 3325 Color: 2
Size: 2804 Color: 10
Size: 478 Color: 4

Bin 109: 17 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 13
Size: 1356 Color: 10

Bin 110: 18 of cap free
Amount of items: 2
Items: 
Size: 3844 Color: 6
Size: 2762 Color: 18

Bin 111: 18 of cap free
Amount of items: 2
Items: 
Size: 4935 Color: 5
Size: 1671 Color: 3

Bin 112: 18 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 16
Size: 826 Color: 14

Bin 113: 19 of cap free
Amount of items: 2
Items: 
Size: 4123 Color: 15
Size: 2482 Color: 3

Bin 114: 19 of cap free
Amount of items: 2
Items: 
Size: 5095 Color: 0
Size: 1510 Color: 18

Bin 115: 20 of cap free
Amount of items: 3
Items: 
Size: 4300 Color: 19
Size: 2182 Color: 17
Size: 122 Color: 6

Bin 116: 20 of cap free
Amount of items: 2
Items: 
Size: 4698 Color: 9
Size: 1906 Color: 7

Bin 117: 21 of cap free
Amount of items: 2
Items: 
Size: 5458 Color: 10
Size: 1145 Color: 17

Bin 118: 23 of cap free
Amount of items: 2
Items: 
Size: 4093 Color: 10
Size: 2508 Color: 4

Bin 119: 23 of cap free
Amount of items: 2
Items: 
Size: 5606 Color: 2
Size: 995 Color: 11

Bin 120: 24 of cap free
Amount of items: 2
Items: 
Size: 4907 Color: 19
Size: 1693 Color: 1

Bin 121: 27 of cap free
Amount of items: 2
Items: 
Size: 4593 Color: 2
Size: 2004 Color: 3

Bin 122: 30 of cap free
Amount of items: 2
Items: 
Size: 3330 Color: 19
Size: 3264 Color: 14

Bin 123: 31 of cap free
Amount of items: 2
Items: 
Size: 5605 Color: 1
Size: 988 Color: 11

Bin 124: 32 of cap free
Amount of items: 16
Items: 
Size: 582 Color: 3
Size: 550 Color: 1
Size: 548 Color: 19
Size: 548 Color: 15
Size: 504 Color: 3
Size: 456 Color: 2
Size: 440 Color: 2
Size: 424 Color: 3
Size: 392 Color: 13
Size: 392 Color: 8
Size: 392 Color: 0
Size: 372 Color: 3
Size: 328 Color: 10
Size: 264 Color: 5
Size: 208 Color: 10
Size: 192 Color: 17

Bin 125: 32 of cap free
Amount of items: 2
Items: 
Size: 5162 Color: 4
Size: 1430 Color: 18

Bin 126: 36 of cap free
Amount of items: 32
Items: 
Size: 360 Color: 18
Size: 336 Color: 8
Size: 320 Color: 10
Size: 286 Color: 4
Size: 284 Color: 15
Size: 280 Color: 6
Size: 258 Color: 16
Size: 248 Color: 18
Size: 244 Color: 15
Size: 244 Color: 10
Size: 240 Color: 0
Size: 226 Color: 4
Size: 196 Color: 5
Size: 192 Color: 6
Size: 184 Color: 10
Size: 184 Color: 8
Size: 178 Color: 9
Size: 176 Color: 4
Size: 174 Color: 12
Size: 168 Color: 18
Size: 168 Color: 17
Size: 168 Color: 17
Size: 168 Color: 13
Size: 160 Color: 19
Size: 160 Color: 12
Size: 156 Color: 6
Size: 152 Color: 13
Size: 152 Color: 7
Size: 142 Color: 8
Size: 140 Color: 16
Size: 132 Color: 17
Size: 112 Color: 5

Bin 127: 36 of cap free
Amount of items: 2
Items: 
Size: 5160 Color: 0
Size: 1428 Color: 15

Bin 128: 38 of cap free
Amount of items: 2
Items: 
Size: 5666 Color: 3
Size: 920 Color: 4

Bin 129: 39 of cap free
Amount of items: 3
Items: 
Size: 3632 Color: 2
Size: 2753 Color: 11
Size: 200 Color: 5

Bin 130: 39 of cap free
Amount of items: 3
Items: 
Size: 3765 Color: 12
Size: 2628 Color: 7
Size: 192 Color: 8

Bin 131: 46 of cap free
Amount of items: 2
Items: 
Size: 4854 Color: 3
Size: 1724 Color: 7

Bin 132: 63 of cap free
Amount of items: 3
Items: 
Size: 3322 Color: 8
Size: 2480 Color: 10
Size: 759 Color: 6

Bin 133: 5578 of cap free
Amount of items: 9
Items: 
Size: 132 Color: 2
Size: 128 Color: 19
Size: 128 Color: 14
Size: 128 Color: 3
Size: 114 Color: 0
Size: 112 Color: 17
Size: 112 Color: 15
Size: 96 Color: 16
Size: 96 Color: 5

Total size: 874368
Total free space: 6624

