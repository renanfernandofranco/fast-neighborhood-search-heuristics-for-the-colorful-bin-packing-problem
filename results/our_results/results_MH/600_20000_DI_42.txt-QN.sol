Capicity Bin: 16400
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 10150 Color: 440
Size: 5210 Color: 366
Size: 432 Color: 86
Size: 304 Color: 33
Size: 304 Color: 32

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11398 Color: 457
Size: 3658 Color: 329
Size: 1344 Color: 195

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12441 Color: 484
Size: 2595 Color: 292
Size: 1364 Color: 201

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 487
Size: 2302 Color: 275
Size: 1550 Color: 220

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 491
Size: 2122 Color: 263
Size: 1502 Color: 214

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 492
Size: 3364 Color: 321
Size: 256 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 495
Size: 2008 Color: 251
Size: 1558 Color: 222

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12860 Color: 496
Size: 2012 Color: 252
Size: 1528 Color: 218

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13070 Color: 502
Size: 2982 Color: 309
Size: 348 Color: 53

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 505
Size: 3032 Color: 312
Size: 272 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 506
Size: 2918 Color: 304
Size: 378 Color: 63

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 510
Size: 2952 Color: 306
Size: 214 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13250 Color: 511
Size: 1638 Color: 227
Size: 1512 Color: 216

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13287 Color: 513
Size: 2743 Color: 298
Size: 370 Color: 61

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 514
Size: 2152 Color: 266
Size: 960 Color: 166

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 521
Size: 1768 Color: 238
Size: 1072 Color: 180

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 522
Size: 2402 Color: 283
Size: 434 Color: 87

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13579 Color: 523
Size: 2393 Color: 282
Size: 428 Color: 83

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 524
Size: 2210 Color: 269
Size: 596 Color: 126

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 528
Size: 2256 Color: 273
Size: 472 Color: 101

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13709 Color: 531
Size: 2099 Color: 259
Size: 592 Color: 124

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13731 Color: 532
Size: 2151 Color: 265
Size: 518 Color: 111

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13815 Color: 536
Size: 2155 Color: 267
Size: 430 Color: 84

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13819 Color: 537
Size: 2309 Color: 276
Size: 272 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 538
Size: 1832 Color: 240
Size: 736 Color: 145

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13858 Color: 540
Size: 1874 Color: 243
Size: 668 Color: 134

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 546
Size: 1558 Color: 223
Size: 904 Color: 159

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 547
Size: 1732 Color: 234
Size: 728 Color: 143

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13959 Color: 549
Size: 1945 Color: 249
Size: 496 Color: 107

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 555
Size: 1388 Color: 204
Size: 910 Color: 161

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 556
Size: 1640 Color: 228
Size: 640 Color: 131

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 558
Size: 1724 Color: 233
Size: 528 Color: 114

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14154 Color: 559
Size: 1722 Color: 232
Size: 524 Color: 113

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 562
Size: 1758 Color: 236
Size: 444 Color: 91

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14207 Color: 563
Size: 1829 Color: 239
Size: 364 Color: 56

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 569
Size: 1196 Color: 188
Size: 880 Color: 158

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14432 Color: 573
Size: 1040 Color: 172
Size: 928 Color: 164

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 574
Size: 1596 Color: 225
Size: 368 Color: 60

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 575
Size: 1556 Color: 221
Size: 406 Color: 71

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 576
Size: 1040 Color: 171
Size: 920 Color: 163

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14462 Color: 577
Size: 1364 Color: 200
Size: 574 Color: 120

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14540 Color: 581
Size: 1420 Color: 206
Size: 440 Color: 89

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14542 Color: 582
Size: 1542 Color: 219
Size: 316 Color: 40

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14582 Color: 584
Size: 1510 Color: 215
Size: 308 Color: 36

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 585
Size: 1072 Color: 181
Size: 744 Color: 146

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 586
Size: 1498 Color: 212
Size: 312 Color: 39

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14598 Color: 587
Size: 1422 Color: 207
Size: 380 Color: 64

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 588
Size: 1456 Color: 209
Size: 344 Color: 51

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14604 Color: 589
Size: 1500 Color: 213
Size: 296 Color: 27

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14606 Color: 590
Size: 1202 Color: 192
Size: 592 Color: 125

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 591
Size: 1460 Color: 211
Size: 288 Color: 22

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14654 Color: 592
Size: 1194 Color: 187
Size: 552 Color: 118

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 594
Size: 1032 Color: 169
Size: 672 Color: 135

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14698 Color: 595
Size: 1366 Color: 202
Size: 336 Color: 46

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 596
Size: 1360 Color: 196
Size: 340 Color: 49

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14702 Color: 597
Size: 1058 Color: 178
Size: 640 Color: 130

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 598
Size: 1192 Color: 186
Size: 480 Color: 102

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14740 Color: 599
Size: 1360 Color: 198
Size: 300 Color: 30

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 600
Size: 1222 Color: 193
Size: 430 Color: 85

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 424
Size: 5105 Color: 361
Size: 1970 Color: 250

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 9964 Color: 431
Size: 6111 Color: 389
Size: 324 Color: 45

Bin 62: 1 of cap free
Amount of items: 5
Items: 
Size: 10260 Color: 443
Size: 5211 Color: 367
Size: 344 Color: 50
Size: 296 Color: 26
Size: 288 Color: 25

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 11870 Color: 468
Size: 4529 Color: 353

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11982 Color: 472
Size: 3737 Color: 333
Size: 680 Color: 138

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 477
Size: 4142 Color: 345
Size: 140 Color: 2

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 12497 Color: 486
Size: 3902 Color: 338

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 12802 Color: 494
Size: 3597 Color: 328

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12891 Color: 497
Size: 3208 Color: 316
Size: 300 Color: 29

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13675 Color: 529
Size: 2316 Color: 277
Size: 408 Color: 73

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13955 Color: 548
Size: 2052 Color: 255
Size: 392 Color: 67

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 14123 Color: 557
Size: 2276 Color: 274

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 14156 Color: 560
Size: 2243 Color: 272

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 9222 Color: 420
Size: 6808 Color: 393
Size: 368 Color: 59

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 10202 Color: 442
Size: 5900 Color: 378
Size: 296 Color: 28

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 11371 Color: 456
Size: 3663 Color: 330
Size: 1364 Color: 199

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 12085 Color: 475
Size: 4145 Color: 346
Size: 168 Color: 4

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 12228 Color: 478
Size: 4170 Color: 347

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 501
Size: 1876 Color: 244
Size: 1458 Color: 210

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 504
Size: 2614 Color: 294
Size: 700 Color: 139

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 13396 Color: 516
Size: 3002 Color: 310

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 13638 Color: 527
Size: 2760 Color: 299

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14067 Color: 554
Size: 2331 Color: 278

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 14284 Color: 565
Size: 2114 Color: 262

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 567
Size: 2102 Color: 261

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 14334 Color: 570
Size: 2064 Color: 257

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 14514 Color: 579
Size: 1884 Color: 245

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 14656 Color: 593
Size: 1742 Color: 235

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 10874 Color: 449
Size: 5251 Color: 369
Size: 272 Color: 16

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 10967 Color: 452
Size: 5166 Color: 364
Size: 264 Color: 12

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 469
Size: 2478 Color: 285
Size: 2035 Color: 253

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 471
Size: 4421 Color: 350

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 14033 Color: 552
Size: 2364 Color: 280

Bin 93: 4 of cap free
Amount of items: 5
Items: 
Size: 9334 Color: 425
Size: 5958 Color: 379
Size: 372 Color: 62
Size: 368 Color: 58
Size: 364 Color: 57

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 11577 Color: 461
Size: 3571 Color: 326
Size: 1248 Color: 194

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 11633 Color: 465
Size: 4555 Color: 355
Size: 208 Color: 7

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 12469 Color: 485
Size: 3927 Color: 339

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 499
Size: 3444 Color: 325

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 13752 Color: 534
Size: 2644 Color: 296

Bin 99: 5 of cap free
Amount of items: 3
Items: 
Size: 11601 Color: 463
Size: 3394 Color: 323
Size: 1400 Color: 205

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 12329 Color: 481
Size: 4008 Color: 341
Size: 58 Color: 0

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 482
Size: 4013 Color: 342

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 13073 Color: 503
Size: 3322 Color: 319

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 13228 Color: 509
Size: 3167 Color: 315

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 13518 Color: 520
Size: 2877 Color: 303

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13884 Color: 544
Size: 2511 Color: 289

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 13899 Color: 545
Size: 2496 Color: 287

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 550
Size: 2407 Color: 284

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 14216 Color: 564
Size: 2179 Color: 268

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 568
Size: 2085 Color: 258

Bin 110: 5 of cap free
Amount of items: 2
Items: 
Size: 14534 Color: 580
Size: 1861 Color: 242

Bin 111: 6 of cap free
Amount of items: 5
Items: 
Size: 10147 Color: 439
Size: 5205 Color: 365
Size: 426 Color: 82
Size: 308 Color: 35
Size: 308 Color: 34

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 14169 Color: 561
Size: 2225 Color: 271

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 14294 Color: 566
Size: 2100 Color: 260

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 571
Size: 2054 Color: 256

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 9179 Color: 417
Size: 6830 Color: 397
Size: 384 Color: 65

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 10103 Color: 437
Size: 5982 Color: 382
Size: 308 Color: 37

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 12014 Color: 474
Size: 4379 Color: 349

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 9176 Color: 416
Size: 6828 Color: 396
Size: 388 Color: 66

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13467 Color: 518
Size: 2925 Color: 305

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13628 Color: 526
Size: 2764 Color: 300

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 13750 Color: 533
Size: 2642 Color: 295

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 13883 Color: 543
Size: 2508 Color: 288

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 14492 Color: 578
Size: 1899 Color: 246

Bin 124: 10 of cap free
Amount of items: 3
Items: 
Size: 10075 Color: 435
Size: 5995 Color: 384
Size: 320 Color: 41

Bin 125: 11 of cap free
Amount of items: 3
Items: 
Size: 11585 Color: 462
Size: 3750 Color: 334
Size: 1054 Color: 177

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 14350 Color: 572
Size: 2039 Color: 254

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 10043 Color: 432
Size: 6025 Color: 387
Size: 320 Color: 44

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 11689 Color: 467
Size: 4531 Color: 354
Size: 168 Color: 5

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13038 Color: 500
Size: 3350 Color: 320

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 13843 Color: 539
Size: 2545 Color: 291

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 14550 Color: 583
Size: 1838 Color: 241

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 12414 Color: 483
Size: 3973 Color: 340

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 13787 Color: 535
Size: 2600 Color: 293

Bin 134: 14 of cap free
Amount of items: 5
Items: 
Size: 8216 Color: 412
Size: 6818 Color: 394
Size: 520 Color: 112
Size: 416 Color: 75
Size: 416 Color: 74

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 13109 Color: 507
Size: 3277 Color: 318

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 13266 Color: 512
Size: 3120 Color: 314

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 517
Size: 2956 Color: 307

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 13866 Color: 541
Size: 2520 Color: 290

Bin 139: 15 of cap free
Amount of items: 3
Items: 
Size: 10830 Color: 448
Size: 5275 Color: 371
Size: 280 Color: 18

Bin 140: 16 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 414
Size: 7736 Color: 402
Size: 400 Color: 69

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 12276 Color: 479
Size: 4108 Color: 344

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 551
Size: 2376 Color: 281

Bin 143: 17 of cap free
Amount of items: 3
Items: 
Size: 10963 Color: 451
Size: 5148 Color: 363
Size: 272 Color: 13

Bin 144: 17 of cap free
Amount of items: 2
Items: 
Size: 11084 Color: 454
Size: 5299 Color: 373

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 13605 Color: 525
Size: 2778 Color: 301

Bin 146: 18 of cap free
Amount of items: 3
Items: 
Size: 10071 Color: 434
Size: 5991 Color: 383
Size: 320 Color: 42

Bin 147: 18 of cap free
Amount of items: 3
Items: 
Size: 10099 Color: 436
Size: 5971 Color: 381
Size: 312 Color: 38

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 12552 Color: 488
Size: 3830 Color: 336

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 12949 Color: 498
Size: 3432 Color: 324

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 14038 Color: 553
Size: 2342 Color: 279

Bin 151: 21 of cap free
Amount of items: 2
Items: 
Size: 11160 Color: 455
Size: 5219 Color: 368

Bin 152: 21 of cap free
Amount of items: 2
Items: 
Size: 13347 Color: 515
Size: 3032 Color: 313

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 13676 Color: 530
Size: 2703 Color: 297

Bin 154: 22 of cap free
Amount of items: 3
Items: 
Size: 11548 Color: 460
Size: 4606 Color: 356
Size: 224 Color: 9

Bin 155: 22 of cap free
Amount of items: 2
Items: 
Size: 12690 Color: 490
Size: 3688 Color: 332

Bin 156: 22 of cap free
Amount of items: 2
Items: 
Size: 13513 Color: 519
Size: 2865 Color: 302

Bin 157: 24 of cap free
Amount of items: 7
Items: 
Size: 8205 Color: 408
Size: 2216 Color: 270
Size: 2131 Color: 264
Size: 1912 Color: 247
Size: 1078 Color: 182
Size: 418 Color: 78
Size: 416 Color: 77

Bin 158: 25 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 542
Size: 2493 Color: 286

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 11076 Color: 453
Size: 5296 Color: 372

Bin 160: 31 of cap free
Amount of items: 5
Items: 
Size: 8206 Color: 409
Size: 3009 Color: 311
Size: 2974 Color: 308
Size: 1764 Color: 237
Size: 416 Color: 76

Bin 161: 31 of cap free
Amount of items: 2
Items: 
Size: 13157 Color: 508
Size: 3212 Color: 317

Bin 162: 34 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 446
Size: 5391 Color: 376
Size: 280 Color: 20

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 11990 Color: 473
Size: 4376 Color: 348

Bin 164: 35 of cap free
Amount of items: 2
Items: 
Size: 12601 Color: 489
Size: 3764 Color: 335

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 493
Size: 3573 Color: 327

Bin 166: 37 of cap free
Amount of items: 2
Items: 
Size: 11608 Color: 464
Size: 4755 Color: 358

Bin 167: 40 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 430
Size: 6064 Color: 388
Size: 336 Color: 47

Bin 168: 42 of cap free
Amount of items: 3
Items: 
Size: 10807 Color: 447
Size: 5271 Color: 370
Size: 280 Color: 19

Bin 169: 47 of cap free
Amount of items: 3
Items: 
Size: 10064 Color: 433
Size: 5969 Color: 380
Size: 320 Color: 43

Bin 170: 48 of cap free
Amount of items: 3
Items: 
Size: 9708 Color: 427
Size: 6292 Color: 391
Size: 352 Color: 54

Bin 171: 48 of cap free
Amount of items: 3
Items: 
Size: 12113 Color: 476
Size: 4073 Color: 343
Size: 166 Color: 3

Bin 172: 54 of cap free
Amount of items: 2
Items: 
Size: 11902 Color: 470
Size: 4444 Color: 352

Bin 173: 55 of cap free
Amount of items: 3
Items: 
Size: 10155 Color: 441
Size: 5890 Color: 377
Size: 300 Color: 31

Bin 174: 63 of cap free
Amount of items: 2
Items: 
Size: 11513 Color: 459
Size: 4824 Color: 359

Bin 175: 65 of cap free
Amount of items: 2
Items: 
Size: 10139 Color: 438
Size: 6196 Color: 390

Bin 176: 68 of cap free
Amount of items: 3
Items: 
Size: 11434 Color: 458
Size: 4642 Color: 357
Size: 256 Color: 11

Bin 177: 69 of cap free
Amount of items: 3
Items: 
Size: 10935 Color: 450
Size: 5124 Color: 362
Size: 272 Color: 15

Bin 178: 75 of cap free
Amount of items: 3
Items: 
Size: 9254 Color: 423
Size: 3678 Color: 331
Size: 3393 Color: 322

Bin 179: 84 of cap free
Amount of items: 7
Items: 
Size: 8204 Color: 407
Size: 1710 Color: 231
Size: 1648 Color: 230
Size: 1644 Color: 229
Size: 1618 Color: 226
Size: 1072 Color: 179
Size: 420 Color: 79

Bin 180: 90 of cap free
Amount of items: 2
Items: 
Size: 8214 Color: 411
Size: 8096 Color: 404

Bin 181: 94 of cap free
Amount of items: 8
Items: 
Size: 8202 Color: 406
Size: 1574 Color: 224
Size: 1518 Color: 217
Size: 1432 Color: 208
Size: 1380 Color: 203
Size: 1360 Color: 197
Size: 420 Color: 81
Size: 420 Color: 80

Bin 182: 100 of cap free
Amount of items: 3
Items: 
Size: 11665 Color: 466
Size: 4427 Color: 351
Size: 208 Color: 6

Bin 183: 100 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 480
Size: 3892 Color: 337
Size: 112 Color: 1

Bin 184: 109 of cap free
Amount of items: 3
Items: 
Size: 9931 Color: 429
Size: 6024 Color: 386
Size: 336 Color: 48

Bin 185: 111 of cap free
Amount of items: 3
Items: 
Size: 9067 Color: 415
Size: 6822 Color: 395
Size: 400 Color: 68

Bin 186: 120 of cap free
Amount of items: 26
Items: 
Size: 802 Color: 153
Size: 794 Color: 152
Size: 784 Color: 151
Size: 784 Color: 150
Size: 752 Color: 149
Size: 752 Color: 148
Size: 748 Color: 147
Size: 732 Color: 144
Size: 718 Color: 142
Size: 714 Color: 141
Size: 712 Color: 140
Size: 678 Color: 137
Size: 678 Color: 136
Size: 664 Color: 133
Size: 544 Color: 116
Size: 540 Color: 115
Size: 516 Color: 110
Size: 512 Color: 109
Size: 508 Color: 108
Size: 496 Color: 106
Size: 492 Color: 105
Size: 488 Color: 104
Size: 480 Color: 103
Size: 464 Color: 100
Size: 464 Color: 99
Size: 464 Color: 98

Bin 187: 128 of cap free
Amount of items: 3
Items: 
Size: 10616 Color: 445
Size: 5368 Color: 375
Size: 288 Color: 21

Bin 188: 148 of cap free
Amount of items: 4
Items: 
Size: 8222 Color: 413
Size: 7216 Color: 401
Size: 408 Color: 72
Size: 406 Color: 70

Bin 189: 152 of cap free
Amount of items: 4
Items: 
Size: 10308 Color: 444
Size: 5364 Color: 374
Size: 288 Color: 24
Size: 288 Color: 23

Bin 190: 159 of cap free
Amount of items: 3
Items: 
Size: 9239 Color: 422
Size: 5084 Color: 360
Size: 1918 Color: 248

Bin 191: 165 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 428
Size: 6019 Color: 385
Size: 348 Color: 52

Bin 192: 179 of cap free
Amount of items: 9
Items: 
Size: 8201 Color: 405
Size: 1200 Color: 191
Size: 1198 Color: 190
Size: 1198 Color: 189
Size: 1188 Color: 185
Size: 1176 Color: 184
Size: 1176 Color: 183
Size: 444 Color: 90
Size: 440 Color: 88

Bin 193: 224 of cap free
Amount of items: 3
Items: 
Size: 9372 Color: 426
Size: 6452 Color: 392
Size: 352 Color: 55

Bin 194: 254 of cap free
Amount of items: 2
Items: 
Size: 8212 Color: 410
Size: 7934 Color: 403

Bin 195: 324 of cap free
Amount of items: 20
Items: 
Size: 1054 Color: 176
Size: 1050 Color: 175
Size: 1042 Color: 174
Size: 1042 Color: 173
Size: 1040 Color: 170
Size: 1016 Color: 168
Size: 1008 Color: 167
Size: 950 Color: 165
Size: 912 Color: 162
Size: 906 Color: 160
Size: 864 Color: 157
Size: 832 Color: 156
Size: 824 Color: 155
Size: 814 Color: 154
Size: 462 Color: 97
Size: 460 Color: 96
Size: 456 Color: 95
Size: 448 Color: 94
Size: 448 Color: 93
Size: 448 Color: 92

Bin 196: 329 of cap free
Amount of items: 2
Items: 
Size: 9235 Color: 421
Size: 6836 Color: 400

Bin 197: 356 of cap free
Amount of items: 2
Items: 
Size: 9211 Color: 419
Size: 6833 Color: 399

Bin 198: 362 of cap free
Amount of items: 2
Items: 
Size: 9207 Color: 418
Size: 6831 Color: 398

Bin 199: 11062 of cap free
Amount of items: 9
Items: 
Size: 654 Color: 132
Size: 632 Color: 129
Size: 608 Color: 128
Size: 600 Color: 127
Size: 584 Color: 123
Size: 584 Color: 122
Size: 576 Color: 121
Size: 552 Color: 119
Size: 548 Color: 117

Total size: 3247200
Total free space: 16400

