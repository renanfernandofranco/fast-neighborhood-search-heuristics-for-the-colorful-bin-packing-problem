Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 284 Color: 3
Size: 287 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 252 Color: 4
Size: 252 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 0
Size: 317 Color: 3
Size: 256 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 271 Color: 2
Size: 254 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 322 Color: 2
Size: 269 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 286 Color: 1
Size: 253 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 278 Color: 0
Size: 254 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 349 Color: 3
Size: 250 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 346 Color: 0
Size: 253 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 344 Color: 0
Size: 282 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 359 Color: 3
Size: 254 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 290 Color: 3
Size: 251 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 305 Color: 0
Size: 250 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 362 Color: 1
Size: 259 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 264 Color: 4
Size: 254 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 355 Color: 4
Size: 280 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 343 Color: 0
Size: 261 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 318 Color: 0
Size: 268 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 351 Color: 3
Size: 252 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 302 Color: 2
Size: 283 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 322 Color: 3
Size: 269 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 325 Color: 3
Size: 262 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 251 Color: 3
Size: 250 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 322 Color: 0
Size: 255 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 2
Size: 335 Color: 2
Size: 319 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 4
Size: 250 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 275 Color: 1
Size: 274 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 270 Color: 2
Size: 270 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 4
Size: 283 Color: 4
Size: 265 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 252 Color: 1
Size: 250 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 273 Color: 4
Size: 267 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 2
Size: 335 Color: 0
Size: 320 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 273 Color: 4
Size: 269 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 2
Size: 250 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 301 Color: 0
Size: 253 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 286 Color: 1
Size: 253 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 317 Color: 4
Size: 262 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 3
Size: 281 Color: 4
Size: 286 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 3
Size: 295 Color: 0
Size: 285 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 2
Size: 305 Color: 3
Size: 287 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 262 Color: 1
Size: 252 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 0
Size: 351 Color: 1
Size: 296 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 273 Color: 3
Size: 272 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 4
Size: 281 Color: 2
Size: 252 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 368 Color: 3
Size: 258 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 289 Color: 4
Size: 266 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 337 Color: 4
Size: 285 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 358 Color: 1
Size: 252 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 268 Color: 2
Size: 256 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 4
Size: 360 Color: 3
Size: 254 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 0
Size: 263 Color: 1
Size: 261 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 350 Color: 3
Size: 250 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 4
Size: 303 Color: 1
Size: 294 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 308 Color: 4
Size: 294 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 276 Color: 3
Size: 271 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 289 Color: 2
Size: 263 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 296 Color: 2
Size: 277 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 303 Color: 4
Size: 255 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 251 Color: 3
Size: 251 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 269 Color: 1
Size: 265 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 281 Color: 2
Size: 258 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 321 Color: 0
Size: 296 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 322 Color: 1
Size: 257 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 264 Color: 4
Size: 256 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 258 Color: 4
Size: 254 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 265 Color: 3
Size: 252 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 261 Color: 4
Size: 258 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 298 Color: 2
Size: 263 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 294 Color: 0
Size: 266 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 267 Color: 0
Size: 250 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 340 Color: 3
Size: 250 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 371 Color: 3
Size: 255 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 259 Color: 1
Size: 255 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 275 Color: 1
Size: 250 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 294 Color: 3
Size: 258 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 263 Color: 1
Size: 263 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 2
Size: 250 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 315 Color: 4
Size: 260 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 276 Color: 0
Size: 257 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 267 Color: 1
Size: 254 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 0
Size: 278 Color: 3
Size: 255 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 300 Color: 3
Size: 253 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 317 Color: 1
Size: 308 Color: 0

Total size: 83000
Total free space: 0

