Capicity Bin: 2068
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 1300 Color: 149
Size: 768 Color: 129

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 164
Size: 429 Color: 111
Size: 50 Color: 21

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 165
Size: 342 Color: 100
Size: 136 Color: 60

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1597 Color: 166
Size: 393 Color: 107
Size: 78 Color: 46

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 167
Size: 387 Color: 106
Size: 76 Color: 43

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 168
Size: 386 Color: 105
Size: 76 Color: 44

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 171
Size: 232 Color: 82
Size: 189 Color: 72

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 174
Size: 317 Color: 97
Size: 62 Color: 32

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 176
Size: 311 Color: 96
Size: 60 Color: 31

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 177
Size: 320 Color: 99
Size: 50 Color: 22

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 178
Size: 310 Color: 95
Size: 52 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 182
Size: 281 Color: 90
Size: 56 Color: 25

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 183
Size: 230 Color: 81
Size: 100 Color: 52

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 189
Size: 198 Color: 74
Size: 76 Color: 41

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 190
Size: 227 Color: 80
Size: 44 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 192
Size: 213 Color: 78
Size: 42 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 196
Size: 182 Color: 70
Size: 44 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 198
Size: 183 Color: 71
Size: 36 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 199
Size: 178 Color: 69
Size: 40 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1851 Color: 200
Size: 203 Color: 75
Size: 14 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 201
Size: 170 Color: 65
Size: 40 Color: 13

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 144
Size: 861 Color: 136
Size: 60 Color: 28

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 148
Size: 770 Color: 130
Size: 56 Color: 24

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 179
Size: 345 Color: 101
Size: 8 Color: 0

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1716 Color: 180
Size: 351 Color: 102

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 181
Size: 242 Color: 85
Size: 96 Color: 50

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 186
Size: 172 Color: 68
Size: 120 Color: 56

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1789 Color: 188
Size: 278 Color: 89

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1816 Color: 193
Size: 251 Color: 87

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1834 Color: 195
Size: 233 Color: 84

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 163
Size: 511 Color: 116

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1821 Color: 194
Size: 245 Color: 86

Bin 33: 3 of cap free
Amount of items: 5
Items: 
Size: 1428 Color: 156
Size: 521 Color: 117
Size: 40 Color: 11
Size: 40 Color: 10
Size: 36 Color: 9

Bin 34: 3 of cap free
Amount of items: 4
Items: 
Size: 1529 Color: 161
Size: 480 Color: 114
Size: 32 Color: 4
Size: 24 Color: 3

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 185
Size: 298 Color: 93

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 191
Size: 259 Color: 88

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1843 Color: 197
Size: 222 Color: 79

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1523 Color: 160
Size: 541 Color: 119

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1662 Color: 173
Size: 402 Color: 109

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 175
Size: 374 Color: 103

Bin 41: 5 of cap free
Amount of items: 5
Items: 
Size: 1037 Color: 139
Size: 428 Color: 110
Size: 380 Color: 104
Size: 146 Color: 61
Size: 72 Color: 39

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1612 Color: 169
Size: 451 Color: 113

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1759 Color: 184
Size: 302 Color: 94

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 187
Size: 283 Color: 91

Bin 45: 8 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 154
Size: 609 Color: 122
Size: 46 Color: 17

Bin 46: 8 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 157
Size: 578 Color: 121
Size: 36 Color: 8

Bin 47: 10 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 146
Size: 805 Color: 131
Size: 56 Color: 27

Bin 48: 12 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 151
Size: 694 Color: 126
Size: 48 Color: 19

Bin 49: 12 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 153
Size: 630 Color: 123
Size: 48 Color: 18

Bin 50: 12 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 155
Size: 635 Color: 124

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1622 Color: 170
Size: 434 Color: 112

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 172
Size: 401 Color: 108

Bin 53: 16 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 162
Size: 486 Color: 115
Size: 16 Color: 2

Bin 54: 17 of cap free
Amount of items: 2
Items: 
Size: 1189 Color: 145
Size: 862 Color: 137

Bin 55: 22 of cap free
Amount of items: 3
Items: 
Size: 1307 Color: 150
Size: 691 Color: 125
Size: 48 Color: 20

Bin 56: 22 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 158
Size: 553 Color: 120
Size: 36 Color: 7

Bin 57: 24 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 159
Size: 522 Color: 118
Size: 36 Color: 5

Bin 58: 33 of cap free
Amount of items: 2
Items: 
Size: 1339 Color: 152
Size: 696 Color: 127

Bin 59: 35 of cap free
Amount of items: 6
Items: 
Size: 1035 Color: 138
Size: 318 Color: 98
Size: 296 Color: 92
Size: 232 Color: 83
Size: 76 Color: 42
Size: 76 Color: 40

Bin 60: 38 of cap free
Amount of items: 4
Items: 
Size: 1045 Color: 142
Size: 857 Color: 134
Size: 64 Color: 34
Size: 64 Color: 33

Bin 61: 38 of cap free
Amount of items: 4
Items: 
Size: 1049 Color: 143
Size: 861 Color: 135
Size: 60 Color: 30
Size: 60 Color: 29

Bin 62: 40 of cap free
Amount of items: 4
Items: 
Size: 1041 Color: 141
Size: 853 Color: 133
Size: 68 Color: 36
Size: 66 Color: 35

Bin 63: 41 of cap free
Amount of items: 4
Items: 
Size: 1038 Color: 140
Size: 851 Color: 132
Size: 70 Color: 38
Size: 68 Color: 37

Bin 64: 41 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 147
Size: 733 Color: 128
Size: 56 Color: 26

Bin 65: 91 of cap free
Amount of items: 14
Items: 
Size: 212 Color: 77
Size: 207 Color: 76
Size: 190 Color: 73
Size: 170 Color: 67
Size: 170 Color: 66
Size: 168 Color: 64
Size: 168 Color: 63
Size: 152 Color: 62
Size: 106 Color: 53
Size: 100 Color: 51
Size: 88 Color: 49
Size: 84 Color: 48
Size: 84 Color: 47
Size: 78 Color: 45

Bin 66: 1460 of cap free
Amount of items: 5
Items: 
Size: 136 Color: 59
Size: 126 Color: 58
Size: 124 Color: 57
Size: 112 Color: 55
Size: 110 Color: 54

Total size: 134420
Total free space: 2068

