Capicity Bin: 8360
Lower Bound: 132

Bins used: 132
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 4128 Color: 1
Size: 1184 Color: 1
Size: 1032 Color: 1
Size: 704 Color: 1
Size: 512 Color: 0
Size: 448 Color: 0
Size: 240 Color: 0
Size: 72 Color: 0
Size: 40 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 1
Size: 1460 Color: 1
Size: 288 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7316 Color: 1
Size: 876 Color: 1
Size: 168 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 6866 Color: 1
Size: 1246 Color: 1
Size: 128 Color: 0
Size: 120 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5821 Color: 1
Size: 2271 Color: 1
Size: 268 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7460 Color: 1
Size: 756 Color: 1
Size: 144 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 1
Size: 2942 Color: 1
Size: 588 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 1
Size: 1156 Color: 1
Size: 224 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7255 Color: 1
Size: 921 Color: 1
Size: 184 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 1
Size: 1292 Color: 1
Size: 256 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 1
Size: 1147 Color: 1
Size: 228 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 1
Size: 3300 Color: 1
Size: 528 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7369 Color: 1
Size: 827 Color: 1
Size: 164 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4183 Color: 1
Size: 3481 Color: 1
Size: 696 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 1
Size: 1029 Color: 1
Size: 204 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7406 Color: 1
Size: 798 Color: 1
Size: 156 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5322 Color: 1
Size: 2534 Color: 1
Size: 504 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7110 Color: 1
Size: 1042 Color: 1
Size: 208 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 1
Size: 1148 Color: 1
Size: 224 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 1
Size: 1381 Color: 1
Size: 274 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 3640 Color: 1
Size: 3144 Color: 1
Size: 1576 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4604 Color: 1
Size: 3132 Color: 1
Size: 624 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4458 Color: 1
Size: 3254 Color: 1
Size: 648 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4782 Color: 1
Size: 3450 Color: 1
Size: 128 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7333 Color: 1
Size: 857 Color: 1
Size: 170 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4181 Color: 1
Size: 3483 Color: 1
Size: 696 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 1
Size: 1404 Color: 1
Size: 272 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 1
Size: 1438 Color: 1
Size: 120 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 1
Size: 746 Color: 1
Size: 148 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 1
Size: 1070 Color: 1
Size: 212 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7070 Color: 1
Size: 1078 Color: 1
Size: 212 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 1
Size: 2028 Color: 1
Size: 400 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 1
Size: 1396 Color: 1
Size: 272 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7326 Color: 1
Size: 862 Color: 1
Size: 172 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7230 Color: 1
Size: 942 Color: 1
Size: 188 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6333 Color: 1
Size: 1691 Color: 1
Size: 336 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 1
Size: 1581 Color: 1
Size: 314 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 1
Size: 938 Color: 1
Size: 184 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 1
Size: 1524 Color: 1
Size: 304 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 1
Size: 988 Color: 1
Size: 192 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 1
Size: 1111 Color: 1
Size: 222 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5179 Color: 1
Size: 2651 Color: 1
Size: 530 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6614 Color: 1
Size: 1458 Color: 1
Size: 288 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6720 Color: 1
Size: 1288 Color: 1
Size: 352 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5292 Color: 1
Size: 2564 Color: 1
Size: 504 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6932 Color: 1
Size: 1196 Color: 1
Size: 232 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6832 Color: 1
Size: 1280 Color: 1
Size: 248 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 1
Size: 2922 Color: 1
Size: 584 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 1
Size: 2950 Color: 1
Size: 588 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 1
Size: 2220 Color: 1
Size: 440 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 1
Size: 1666 Color: 1
Size: 332 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 1492 Color: 1
Size: 296 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7334 Color: 1
Size: 858 Color: 1
Size: 168 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 7157 Color: 1
Size: 1003 Color: 1
Size: 200 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 1
Size: 2164 Color: 1
Size: 432 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6794 Color: 1
Size: 1306 Color: 1
Size: 260 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 1
Size: 1489 Color: 1
Size: 296 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 1
Size: 1108 Color: 1
Size: 64 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 1
Size: 1644 Color: 1
Size: 320 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7402 Color: 1
Size: 802 Color: 1
Size: 156 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 1
Size: 950 Color: 1
Size: 188 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4725 Color: 1
Size: 3191 Color: 1
Size: 444 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7396 Color: 1
Size: 804 Color: 1
Size: 160 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 1
Size: 2182 Color: 1
Size: 436 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 7524 Color: 1
Size: 700 Color: 1
Size: 136 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 1
Size: 1714 Color: 1
Size: 340 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5286 Color: 1
Size: 2562 Color: 1
Size: 512 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 1
Size: 910 Color: 1
Size: 180 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 1
Size: 3484 Color: 1
Size: 688 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 1
Size: 2652 Color: 1
Size: 528 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 1
Size: 1470 Color: 1
Size: 292 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 1
Size: 1863 Color: 1
Size: 372 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 7060 Color: 1
Size: 1084 Color: 1
Size: 216 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7474 Color: 1
Size: 742 Color: 1
Size: 144 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 1
Size: 1331 Color: 1
Size: 264 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 1
Size: 2684 Color: 1
Size: 536 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4724 Color: 1
Size: 3036 Color: 1
Size: 600 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 4182 Color: 1
Size: 3482 Color: 1
Size: 696 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 1
Size: 998 Color: 1
Size: 196 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 7063 Color: 1
Size: 1081 Color: 1
Size: 216 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 1
Size: 1740 Color: 1
Size: 344 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5781 Color: 1
Size: 2181 Color: 1
Size: 398 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 1
Size: 1461 Color: 1
Size: 290 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6842 Color: 1
Size: 1266 Color: 1
Size: 252 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4713 Color: 1
Size: 3041 Color: 1
Size: 606 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 1
Size: 1876 Color: 1
Size: 368 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5937 Color: 1
Size: 2021 Color: 1
Size: 402 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 4965 Color: 1
Size: 2831 Color: 1
Size: 564 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 1
Size: 1980 Color: 1
Size: 392 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 1
Size: 1166 Color: 1
Size: 232 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 1
Size: 1858 Color: 1
Size: 368 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 1
Size: 956 Color: 1
Size: 184 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 1
Size: 1274 Color: 1
Size: 252 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6505 Color: 1
Size: 1547 Color: 1
Size: 308 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 1
Size: 1606 Color: 1
Size: 208 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 5380 Color: 1
Size: 2508 Color: 1
Size: 472 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 1
Size: 3500 Color: 1
Size: 640 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 6865 Color: 1
Size: 1435 Color: 1
Size: 60 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 4726 Color: 1
Size: 3030 Color: 1
Size: 604 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 5511 Color: 1
Size: 2375 Color: 1
Size: 474 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 1
Size: 1780 Color: 1
Size: 352 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 5052 Color: 1
Size: 2764 Color: 1
Size: 544 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 4186 Color: 1
Size: 3750 Color: 1
Size: 424 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 1
Size: 2130 Color: 1
Size: 424 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 5551 Color: 1
Size: 2341 Color: 1
Size: 468 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 5644 Color: 1
Size: 2516 Color: 1
Size: 200 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 5702 Color: 1
Size: 2466 Color: 1
Size: 192 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 5187 Color: 1
Size: 2645 Color: 1
Size: 528 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 5885 Color: 1
Size: 2293 Color: 1
Size: 182 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 6945 Color: 1
Size: 1181 Color: 1
Size: 234 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 1
Size: 812 Color: 1
Size: 160 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 5993 Color: 1
Size: 2031 Color: 1
Size: 336 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 1
Size: 1354 Color: 1
Size: 268 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 1
Size: 1601 Color: 1
Size: 98 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 7299 Color: 1
Size: 885 Color: 1
Size: 176 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 1
Size: 2262 Color: 1
Size: 452 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 4496 Color: 1
Size: 3224 Color: 1
Size: 640 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 1
Size: 939 Color: 1
Size: 186 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 7393 Color: 1
Size: 885 Color: 1
Size: 82 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 7329 Color: 1
Size: 861 Color: 1
Size: 170 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 1
Size: 1305 Color: 1
Size: 118 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 6998 Color: 1
Size: 1258 Color: 1
Size: 104 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 6877 Color: 1
Size: 1379 Color: 1
Size: 104 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 6335 Color: 1
Size: 1689 Color: 1
Size: 336 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 4673 Color: 1
Size: 3073 Color: 1
Size: 614 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 5155 Color: 1
Size: 2671 Color: 1
Size: 534 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 1
Size: 1902 Color: 1
Size: 380 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 1
Size: 1821 Color: 1
Size: 364 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 1
Size: 1662 Color: 1
Size: 328 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 1
Size: 1289 Color: 1
Size: 256 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 7191 Color: 1
Size: 975 Color: 1
Size: 194 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 7353 Color: 1
Size: 841 Color: 1
Size: 166 Color: 0

Total size: 1103520
Total free space: 0

