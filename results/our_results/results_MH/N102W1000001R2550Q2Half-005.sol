Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 342668 Color: 1
Size: 322554 Color: 0
Size: 334779 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 345873 Color: 1
Size: 332019 Color: 1
Size: 322109 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 347989 Color: 1
Size: 339477 Color: 1
Size: 312535 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 353581 Color: 1
Size: 335178 Color: 1
Size: 311242 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 359736 Color: 1
Size: 339166 Color: 1
Size: 301099 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 361572 Color: 1
Size: 331807 Color: 1
Size: 306622 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 366959 Color: 1
Size: 362192 Color: 1
Size: 270850 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 368839 Color: 1
Size: 318560 Color: 1
Size: 312602 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374925 Color: 1
Size: 324733 Color: 1
Size: 300343 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 385469 Color: 1
Size: 344089 Color: 1
Size: 270443 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 386413 Color: 1
Size: 333126 Color: 1
Size: 280462 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 386540 Color: 1
Size: 328265 Color: 1
Size: 285196 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 388975 Color: 1
Size: 334245 Color: 1
Size: 276781 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 391468 Color: 1
Size: 350149 Color: 1
Size: 258384 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 406383 Color: 1
Size: 307726 Color: 1
Size: 285892 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 406438 Color: 1
Size: 306792 Color: 1
Size: 286771 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 412307 Color: 1
Size: 306182 Color: 1
Size: 281512 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 412438 Color: 1
Size: 297372 Color: 1
Size: 290191 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 431983 Color: 1
Size: 297272 Color: 1
Size: 270746 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 432043 Color: 1
Size: 303504 Color: 1
Size: 264454 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 441777 Color: 1
Size: 284176 Color: 1
Size: 274048 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 442090 Color: 1
Size: 291404 Color: 1
Size: 266507 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 444488 Color: 1
Size: 298880 Color: 1
Size: 256633 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 445714 Color: 1
Size: 283583 Color: 1
Size: 270704 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 446590 Color: 1
Size: 297509 Color: 1
Size: 255902 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 449280 Color: 1
Size: 293639 Color: 1
Size: 257082 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 450525 Color: 1
Size: 290196 Color: 1
Size: 259280 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 451039 Color: 1
Size: 292972 Color: 1
Size: 255990 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 452082 Color: 1
Size: 293688 Color: 1
Size: 254231 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 453124 Color: 1
Size: 286310 Color: 1
Size: 260567 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 454413 Color: 1
Size: 295439 Color: 1
Size: 250149 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 487295 Color: 1
Size: 256829 Color: 1
Size: 255877 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 488505 Color: 1
Size: 256244 Color: 1
Size: 255252 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 497217 Color: 1
Size: 252670 Color: 1
Size: 250114 Color: 0

Total size: 34000034
Total free space: 0

