Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3336
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 423412 Color: 11
Size: 289794 Color: 16
Size: 286795 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 485409 Color: 18
Size: 262218 Color: 0
Size: 252374 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 350045 Color: 11
Size: 342995 Color: 18
Size: 306961 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 389671 Color: 10
Size: 350818 Color: 11
Size: 259512 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 445757 Color: 5
Size: 280228 Color: 0
Size: 274016 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 467172 Color: 6
Size: 281643 Color: 13
Size: 251186 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 400986 Color: 2
Size: 299780 Color: 16
Size: 299235 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 402426 Color: 4
Size: 334171 Color: 19
Size: 263404 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 393480 Color: 1
Size: 307726 Color: 0
Size: 298795 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 490713 Color: 10
Size: 258375 Color: 1
Size: 250913 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 478564 Color: 8
Size: 262406 Color: 17
Size: 259031 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 376569 Color: 14
Size: 357646 Color: 12
Size: 265786 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 363734 Color: 17
Size: 349983 Color: 5
Size: 286284 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 371773 Color: 11
Size: 343074 Color: 10
Size: 285154 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 396691 Color: 14
Size: 306558 Color: 10
Size: 296752 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 477541 Color: 15
Size: 267413 Color: 4
Size: 255047 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 386820 Color: 14
Size: 328213 Color: 14
Size: 284968 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 377900 Color: 18
Size: 345007 Color: 2
Size: 277094 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 496359 Color: 11
Size: 253044 Color: 5
Size: 250598 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 376473 Color: 13
Size: 319691 Color: 9
Size: 303837 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 449987 Color: 18
Size: 296661 Color: 3
Size: 253353 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 364862 Color: 1
Size: 354857 Color: 2
Size: 280282 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 379887 Color: 14
Size: 357436 Color: 6
Size: 262678 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 470095 Color: 5
Size: 272665 Color: 14
Size: 257241 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 373851 Color: 3
Size: 368587 Color: 7
Size: 257563 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 494631 Color: 17
Size: 255103 Color: 18
Size: 250267 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 490828 Color: 5
Size: 257106 Color: 14
Size: 252067 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 416388 Color: 1
Size: 328650 Color: 4
Size: 254963 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 387291 Color: 12
Size: 312775 Color: 18
Size: 299935 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 353221 Color: 11
Size: 349548 Color: 14
Size: 297232 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 392448 Color: 15
Size: 350461 Color: 18
Size: 257092 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 391695 Color: 3
Size: 335002 Color: 10
Size: 273304 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 460408 Color: 15
Size: 281007 Color: 18
Size: 258586 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 442702 Color: 16
Size: 294984 Color: 10
Size: 262315 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 425617 Color: 8
Size: 302254 Color: 6
Size: 272130 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 493380 Color: 4
Size: 253902 Color: 15
Size: 252719 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 423852 Color: 6
Size: 311265 Color: 11
Size: 264884 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 367379 Color: 17
Size: 338337 Color: 14
Size: 294285 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 452499 Color: 17
Size: 276962 Color: 1
Size: 270540 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 454808 Color: 8
Size: 285633 Color: 13
Size: 259560 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 456090 Color: 18
Size: 287348 Color: 19
Size: 256563 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 387286 Color: 8
Size: 330412 Color: 18
Size: 282303 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 382728 Color: 5
Size: 362051 Color: 7
Size: 255222 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 445373 Color: 0
Size: 286321 Color: 5
Size: 268307 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 389464 Color: 4
Size: 358520 Color: 17
Size: 252017 Color: 13

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 492228 Color: 3
Size: 255538 Color: 16
Size: 252235 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 358478 Color: 5
Size: 325498 Color: 8
Size: 316025 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 427290 Color: 18
Size: 300057 Color: 12
Size: 272654 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 384288 Color: 19
Size: 317236 Color: 3
Size: 298477 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 438568 Color: 12
Size: 285975 Color: 5
Size: 275458 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 487893 Color: 4
Size: 259261 Color: 15
Size: 252847 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 400971 Color: 6
Size: 313623 Color: 5
Size: 285407 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 443705 Color: 15
Size: 304324 Color: 9
Size: 251972 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 367028 Color: 4
Size: 350470 Color: 3
Size: 282503 Color: 16

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 458184 Color: 3
Size: 291090 Color: 19
Size: 250727 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 428523 Color: 17
Size: 287913 Color: 11
Size: 283565 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 373171 Color: 7
Size: 319798 Color: 14
Size: 307032 Color: 12

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 361620 Color: 18
Size: 330777 Color: 3
Size: 307604 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 443630 Color: 2
Size: 286875 Color: 4
Size: 269496 Color: 9

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 467597 Color: 4
Size: 269281 Color: 5
Size: 263123 Color: 16

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 470856 Color: 19
Size: 273679 Color: 16
Size: 255466 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 468460 Color: 18
Size: 280095 Color: 19
Size: 251446 Color: 11

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 375824 Color: 6
Size: 332813 Color: 12
Size: 291364 Color: 7

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 382033 Color: 5
Size: 343844 Color: 16
Size: 274124 Color: 10

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 442484 Color: 14
Size: 284742 Color: 11
Size: 272775 Color: 17

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 357714 Color: 3
Size: 334897 Color: 8
Size: 307390 Color: 11

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 357370 Color: 9
Size: 334242 Color: 7
Size: 308389 Color: 19

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 450117 Color: 12
Size: 291851 Color: 8
Size: 258033 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 400545 Color: 1
Size: 338499 Color: 11
Size: 260957 Color: 15

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 405879 Color: 19
Size: 299431 Color: 5
Size: 294691 Color: 13

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 377976 Color: 9
Size: 345412 Color: 1
Size: 276613 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 410167 Color: 7
Size: 327516 Color: 4
Size: 262318 Color: 11

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 455971 Color: 17
Size: 291070 Color: 18
Size: 252960 Color: 19

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 431344 Color: 9
Size: 293261 Color: 6
Size: 275396 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 450350 Color: 11
Size: 276650 Color: 9
Size: 273001 Color: 17

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 420382 Color: 10
Size: 294417 Color: 4
Size: 285202 Color: 12

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 391464 Color: 11
Size: 318632 Color: 1
Size: 289905 Color: 6

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 477489 Color: 10
Size: 261311 Color: 9
Size: 261201 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 379243 Color: 7
Size: 324773 Color: 5
Size: 295985 Color: 5

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 390954 Color: 16
Size: 354634 Color: 13
Size: 254413 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 434833 Color: 19
Size: 307232 Color: 18
Size: 257936 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 481143 Color: 7
Size: 260868 Color: 8
Size: 257990 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 455894 Color: 6
Size: 275219 Color: 10
Size: 268888 Color: 12

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 385576 Color: 16
Size: 361727 Color: 4
Size: 252698 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 456912 Color: 10
Size: 275014 Color: 6
Size: 268075 Color: 12

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 460444 Color: 7
Size: 271383 Color: 19
Size: 268174 Color: 17

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 440421 Color: 1
Size: 284114 Color: 2
Size: 275466 Color: 11

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 411862 Color: 4
Size: 310579 Color: 5
Size: 277560 Color: 7

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 411520 Color: 11
Size: 335022 Color: 2
Size: 253459 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 451075 Color: 10
Size: 291824 Color: 13
Size: 257102 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 405244 Color: 8
Size: 305196 Color: 7
Size: 289561 Color: 11

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 468605 Color: 7
Size: 274458 Color: 1
Size: 256938 Color: 16

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 397756 Color: 19
Size: 338318 Color: 11
Size: 263927 Color: 16

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 413387 Color: 5
Size: 295940 Color: 17
Size: 290674 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 421707 Color: 16
Size: 311487 Color: 11
Size: 266807 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 427830 Color: 16
Size: 292703 Color: 17
Size: 279468 Color: 11

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 404189 Color: 11
Size: 317710 Color: 13
Size: 278102 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 411633 Color: 3
Size: 300585 Color: 7
Size: 287783 Color: 12

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 460539 Color: 19
Size: 270123 Color: 9
Size: 269339 Color: 10

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 403111 Color: 13
Size: 299410 Color: 16
Size: 297480 Color: 11

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 391576 Color: 7
Size: 328473 Color: 4
Size: 279952 Color: 16

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 432088 Color: 17
Size: 289859 Color: 5
Size: 278054 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 471104 Color: 14
Size: 268591 Color: 7
Size: 260306 Color: 9

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 381173 Color: 14
Size: 319417 Color: 19
Size: 299411 Color: 5

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 448825 Color: 18
Size: 289311 Color: 10
Size: 261865 Color: 6

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 451402 Color: 8
Size: 277466 Color: 2
Size: 271133 Color: 8

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 408314 Color: 4
Size: 341156 Color: 1
Size: 250531 Color: 8

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 369235 Color: 10
Size: 316694 Color: 6
Size: 314072 Color: 14

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 488357 Color: 17
Size: 259991 Color: 4
Size: 251653 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 433209 Color: 19
Size: 297000 Color: 19
Size: 269792 Color: 6

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 446401 Color: 18
Size: 292700 Color: 19
Size: 260900 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 457937 Color: 9
Size: 287179 Color: 12
Size: 254885 Color: 15

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 440920 Color: 2
Size: 292835 Color: 0
Size: 266246 Color: 7

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 487489 Color: 1
Size: 259995 Color: 11
Size: 252517 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 457684 Color: 13
Size: 283025 Color: 15
Size: 259292 Color: 7

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 408526 Color: 12
Size: 328714 Color: 4
Size: 262761 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 364311 Color: 17
Size: 339860 Color: 10
Size: 295830 Color: 13

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 438247 Color: 5
Size: 301837 Color: 15
Size: 259917 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 479523 Color: 0
Size: 266504 Color: 18
Size: 253974 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 375981 Color: 4
Size: 351350 Color: 11
Size: 272670 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 353930 Color: 0
Size: 339341 Color: 15
Size: 306730 Color: 5

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 436875 Color: 1
Size: 302557 Color: 5
Size: 260569 Color: 17

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 375573 Color: 19
Size: 370205 Color: 17
Size: 254223 Color: 12

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 385663 Color: 12
Size: 333460 Color: 14
Size: 280878 Color: 5

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 447963 Color: 4
Size: 283290 Color: 2
Size: 268748 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 346571 Color: 13
Size: 340361 Color: 7
Size: 313069 Color: 6

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 360807 Color: 17
Size: 356443 Color: 0
Size: 282751 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 486827 Color: 9
Size: 257672 Color: 3
Size: 255502 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 402187 Color: 9
Size: 300835 Color: 6
Size: 296979 Color: 10

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 403271 Color: 15
Size: 300666 Color: 5
Size: 296064 Color: 12

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 364179 Color: 3
Size: 362330 Color: 2
Size: 273492 Color: 9

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 382749 Color: 9
Size: 310655 Color: 6
Size: 306597 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 452578 Color: 4
Size: 276364 Color: 3
Size: 271059 Color: 16

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 363494 Color: 9
Size: 331318 Color: 19
Size: 305189 Color: 8

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 396331 Color: 8
Size: 306998 Color: 16
Size: 296672 Color: 7

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 435788 Color: 1
Size: 304060 Color: 19
Size: 260153 Color: 7

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 463997 Color: 18
Size: 276432 Color: 4
Size: 259572 Color: 11

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 459187 Color: 15
Size: 273815 Color: 16
Size: 266999 Color: 11

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 383198 Color: 3
Size: 335696 Color: 17
Size: 281107 Color: 18

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 418354 Color: 3
Size: 301203 Color: 18
Size: 280444 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 386272 Color: 8
Size: 308382 Color: 4
Size: 305347 Color: 9

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 475162 Color: 7
Size: 269599 Color: 12
Size: 255240 Color: 13

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 368361 Color: 2
Size: 317338 Color: 12
Size: 314302 Color: 10

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 465849 Color: 7
Size: 279406 Color: 5
Size: 254746 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 372546 Color: 5
Size: 320488 Color: 11
Size: 306967 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 450455 Color: 19
Size: 292428 Color: 18
Size: 257118 Color: 12

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 358495 Color: 19
Size: 329348 Color: 11
Size: 312158 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 404716 Color: 18
Size: 298419 Color: 17
Size: 296866 Color: 8

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 484320 Color: 16
Size: 263723 Color: 11
Size: 251958 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 428508 Color: 7
Size: 292424 Color: 18
Size: 279069 Color: 17

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 463564 Color: 16
Size: 278919 Color: 16
Size: 257518 Color: 17

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 381182 Color: 1
Size: 349588 Color: 5
Size: 269231 Color: 5

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 458457 Color: 3
Size: 289652 Color: 4
Size: 251892 Color: 5

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 353354 Color: 14
Size: 339509 Color: 2
Size: 307138 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 495478 Color: 3
Size: 253163 Color: 8
Size: 251360 Color: 10

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 446065 Color: 5
Size: 278723 Color: 6
Size: 275213 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 382358 Color: 7
Size: 347098 Color: 4
Size: 270545 Color: 10

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 449445 Color: 19
Size: 286755 Color: 11
Size: 263801 Color: 6

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 339386 Color: 17
Size: 337070 Color: 19
Size: 323545 Color: 18

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 390006 Color: 15
Size: 319097 Color: 2
Size: 290898 Color: 3

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 445746 Color: 3
Size: 292264 Color: 2
Size: 261991 Color: 6

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 436501 Color: 0
Size: 304107 Color: 17
Size: 259393 Color: 14

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 465049 Color: 16
Size: 274485 Color: 5
Size: 260467 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 383016 Color: 7
Size: 318404 Color: 12
Size: 298581 Color: 16

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 415329 Color: 2
Size: 300219 Color: 7
Size: 284453 Color: 13

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 424686 Color: 7
Size: 324250 Color: 9
Size: 251065 Color: 17

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 424032 Color: 12
Size: 298450 Color: 18
Size: 277519 Color: 18

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 426746 Color: 5
Size: 311206 Color: 19
Size: 262049 Color: 16

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 421037 Color: 13
Size: 310630 Color: 8
Size: 268334 Color: 14

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 403783 Color: 17
Size: 332379 Color: 12
Size: 263839 Color: 4

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 471002 Color: 12
Size: 270579 Color: 10
Size: 258420 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 443421 Color: 5
Size: 295660 Color: 0
Size: 260920 Color: 16

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 375164 Color: 13
Size: 337585 Color: 0
Size: 287252 Color: 16

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 498192 Color: 15
Size: 251691 Color: 12
Size: 250118 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 354197 Color: 12
Size: 346055 Color: 17
Size: 299749 Color: 3

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 363544 Color: 2
Size: 320692 Color: 0
Size: 315765 Color: 17

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 415043 Color: 2
Size: 307501 Color: 9
Size: 277457 Color: 15

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 408773 Color: 7
Size: 311848 Color: 4
Size: 279380 Color: 15

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 344005 Color: 10
Size: 339027 Color: 11
Size: 316969 Color: 7

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 387275 Color: 11
Size: 339719 Color: 19
Size: 273007 Color: 13

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 389194 Color: 3
Size: 316018 Color: 10
Size: 294789 Color: 7

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 398314 Color: 18
Size: 342852 Color: 11
Size: 258835 Color: 19

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 490337 Color: 11
Size: 258905 Color: 3
Size: 250759 Color: 9

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 378342 Color: 11
Size: 312388 Color: 2
Size: 309271 Color: 5

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 492382 Color: 9
Size: 255204 Color: 3
Size: 252415 Color: 15

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 357569 Color: 17
Size: 341021 Color: 19
Size: 301411 Color: 4

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 464402 Color: 18
Size: 282760 Color: 14
Size: 252839 Color: 11

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 484682 Color: 14
Size: 258143 Color: 7
Size: 257176 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 490229 Color: 9
Size: 259284 Color: 18
Size: 250488 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 413660 Color: 15
Size: 318278 Color: 2
Size: 268063 Color: 10

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 488988 Color: 15
Size: 255785 Color: 19
Size: 255228 Color: 16

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 441607 Color: 11
Size: 282031 Color: 0
Size: 276363 Color: 19

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 447884 Color: 1
Size: 287465 Color: 17
Size: 264652 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 387589 Color: 5
Size: 355325 Color: 1
Size: 257087 Color: 2

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 488404 Color: 13
Size: 256290 Color: 15
Size: 255307 Color: 4

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 476185 Color: 15
Size: 261983 Color: 14
Size: 261833 Color: 11

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 346342 Color: 13
Size: 327317 Color: 15
Size: 326342 Color: 7

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 492259 Color: 7
Size: 253886 Color: 6
Size: 253856 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 450298 Color: 11
Size: 287853 Color: 7
Size: 261850 Color: 8

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 450170 Color: 15
Size: 277842 Color: 3
Size: 271989 Color: 18

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 392908 Color: 0
Size: 310543 Color: 11
Size: 296550 Color: 7

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 443372 Color: 0
Size: 293208 Color: 7
Size: 263421 Color: 5

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 369156 Color: 9
Size: 349514 Color: 1
Size: 281331 Color: 16

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 425006 Color: 16
Size: 315219 Color: 17
Size: 259776 Color: 18

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 408924 Color: 0
Size: 332415 Color: 2
Size: 258662 Color: 14

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 383440 Color: 14
Size: 335195 Color: 11
Size: 281366 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 384465 Color: 19
Size: 356371 Color: 9
Size: 259165 Color: 18

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 395675 Color: 16
Size: 328511 Color: 4
Size: 275815 Color: 6

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 485052 Color: 1
Size: 264001 Color: 10
Size: 250948 Color: 7

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 443917 Color: 12
Size: 303393 Color: 10
Size: 252691 Color: 15

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 485683 Color: 4
Size: 260553 Color: 0
Size: 253765 Color: 7

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 402821 Color: 2
Size: 328987 Color: 0
Size: 268193 Color: 19

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 495897 Color: 2
Size: 253432 Color: 5
Size: 250672 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 473312 Color: 19
Size: 275530 Color: 9
Size: 251159 Color: 6

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 465164 Color: 14
Size: 272037 Color: 18
Size: 262800 Color: 3

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 357448 Color: 3
Size: 326673 Color: 5
Size: 315880 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 382580 Color: 16
Size: 356936 Color: 2
Size: 260485 Color: 12

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 398282 Color: 7
Size: 306993 Color: 4
Size: 294726 Color: 9

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 454856 Color: 1
Size: 289325 Color: 2
Size: 255820 Color: 17

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 405364 Color: 17
Size: 306475 Color: 14
Size: 288162 Color: 16

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 381876 Color: 17
Size: 358791 Color: 3
Size: 259334 Color: 11

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 360804 Color: 3
Size: 326314 Color: 13
Size: 312883 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 406858 Color: 15
Size: 299447 Color: 15
Size: 293696 Color: 9

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 372405 Color: 12
Size: 332464 Color: 6
Size: 295132 Color: 5

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 405074 Color: 7
Size: 305999 Color: 6
Size: 288928 Color: 8

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 380293 Color: 18
Size: 327537 Color: 11
Size: 292171 Color: 17

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 380984 Color: 1
Size: 364087 Color: 18
Size: 254930 Color: 16

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 479936 Color: 2
Size: 269383 Color: 0
Size: 250682 Color: 14

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 427551 Color: 0
Size: 303309 Color: 10
Size: 269141 Color: 9

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 403757 Color: 13
Size: 334951 Color: 3
Size: 261293 Color: 17

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 408154 Color: 17
Size: 313313 Color: 5
Size: 278534 Color: 3

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 376724 Color: 14
Size: 360998 Color: 0
Size: 262279 Color: 11

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 456666 Color: 4
Size: 290450 Color: 12
Size: 252885 Color: 2

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 377834 Color: 16
Size: 351816 Color: 1
Size: 270351 Color: 7

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 366209 Color: 4
Size: 330817 Color: 2
Size: 302975 Color: 2

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 438054 Color: 14
Size: 281146 Color: 17
Size: 280801 Color: 18

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 347825 Color: 10
Size: 326713 Color: 1
Size: 325463 Color: 15

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 442291 Color: 13
Size: 281859 Color: 11
Size: 275851 Color: 12

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 441198 Color: 1
Size: 291435 Color: 19
Size: 267368 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 477765 Color: 1
Size: 261336 Color: 19
Size: 260900 Color: 13

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 451786 Color: 10
Size: 282960 Color: 5
Size: 265255 Color: 12

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 381091 Color: 19
Size: 364138 Color: 16
Size: 254772 Color: 15

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 345538 Color: 18
Size: 337784 Color: 7
Size: 316679 Color: 11

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 405657 Color: 1
Size: 330713 Color: 10
Size: 263631 Color: 15

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 467303 Color: 11
Size: 274443 Color: 14
Size: 258255 Color: 7

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 481829 Color: 15
Size: 262608 Color: 15
Size: 255564 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 395068 Color: 3
Size: 334654 Color: 0
Size: 270279 Color: 10

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 474911 Color: 12
Size: 269586 Color: 13
Size: 255504 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 428790 Color: 14
Size: 301402 Color: 5
Size: 269809 Color: 7

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 401405 Color: 14
Size: 341262 Color: 19
Size: 257334 Color: 3

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 399934 Color: 5
Size: 304399 Color: 9
Size: 295668 Color: 6

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 473252 Color: 19
Size: 273246 Color: 14
Size: 253503 Color: 19

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 380539 Color: 9
Size: 311776 Color: 12
Size: 307686 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 426867 Color: 3
Size: 322926 Color: 16
Size: 250208 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 404962 Color: 0
Size: 342330 Color: 6
Size: 252709 Color: 13

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 464018 Color: 16
Size: 284358 Color: 2
Size: 251625 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 464705 Color: 17
Size: 278519 Color: 9
Size: 256777 Color: 15

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 446193 Color: 17
Size: 297210 Color: 16
Size: 256598 Color: 7

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 388843 Color: 16
Size: 351551 Color: 14
Size: 259607 Color: 3

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 413215 Color: 5
Size: 332373 Color: 9
Size: 254413 Color: 14

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 395915 Color: 0
Size: 353207 Color: 3
Size: 250879 Color: 12

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 484058 Color: 14
Size: 263063 Color: 1
Size: 252880 Color: 15

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 433733 Color: 10
Size: 295290 Color: 6
Size: 270978 Color: 7

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 436162 Color: 7
Size: 287280 Color: 14
Size: 276559 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 392564 Color: 5
Size: 335413 Color: 11
Size: 272024 Color: 15

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 406846 Color: 8
Size: 307064 Color: 12
Size: 286091 Color: 3

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 471946 Color: 1
Size: 273058 Color: 6
Size: 254997 Color: 19

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 380976 Color: 18
Size: 352681 Color: 15
Size: 266344 Color: 10

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 403660 Color: 9
Size: 335530 Color: 1
Size: 260811 Color: 11

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 384346 Color: 10
Size: 325834 Color: 13
Size: 289821 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 383897 Color: 16
Size: 349774 Color: 14
Size: 266330 Color: 8

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 451168 Color: 14
Size: 291172 Color: 4
Size: 257661 Color: 14

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 446680 Color: 8
Size: 296167 Color: 10
Size: 257154 Color: 13

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 368713 Color: 17
Size: 324276 Color: 0
Size: 307012 Color: 5

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 478453 Color: 9
Size: 269512 Color: 6
Size: 252036 Color: 4

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 381295 Color: 8
Size: 318214 Color: 3
Size: 300492 Color: 4

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 475257 Color: 9
Size: 267744 Color: 11
Size: 257000 Color: 2

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 489256 Color: 1
Size: 260000 Color: 2
Size: 250745 Color: 12

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 337418 Color: 15
Size: 336247 Color: 15
Size: 326336 Color: 14

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 368191 Color: 8
Size: 340051 Color: 1
Size: 291759 Color: 9

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 458306 Color: 19
Size: 281886 Color: 3
Size: 259809 Color: 7

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 373520 Color: 14
Size: 346285 Color: 3
Size: 280196 Color: 15

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 454918 Color: 19
Size: 277495 Color: 9
Size: 267588 Color: 13

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 427959 Color: 0
Size: 305759 Color: 7
Size: 266283 Color: 17

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 442199 Color: 3
Size: 283082 Color: 7
Size: 274720 Color: 11

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 496621 Color: 18
Size: 252658 Color: 17
Size: 250722 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 389646 Color: 11
Size: 313133 Color: 8
Size: 297222 Color: 8

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 384926 Color: 14
Size: 314328 Color: 10
Size: 300747 Color: 19

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 436146 Color: 13
Size: 284229 Color: 17
Size: 279626 Color: 7

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 389071 Color: 8
Size: 311158 Color: 15
Size: 299772 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 388367 Color: 1
Size: 342139 Color: 1
Size: 269495 Color: 3

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 377357 Color: 10
Size: 358790 Color: 14
Size: 263854 Color: 3

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 437762 Color: 15
Size: 308709 Color: 1
Size: 253530 Color: 19

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 397821 Color: 19
Size: 336916 Color: 11
Size: 265264 Color: 10

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 438967 Color: 7
Size: 293669 Color: 5
Size: 267365 Color: 3

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 382909 Color: 11
Size: 310174 Color: 5
Size: 306918 Color: 16

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 371293 Color: 11
Size: 370833 Color: 1
Size: 257875 Color: 16

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 413697 Color: 4
Size: 302808 Color: 10
Size: 283496 Color: 7

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 381563 Color: 8
Size: 326138 Color: 18
Size: 292300 Color: 16

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 415111 Color: 14
Size: 318010 Color: 11
Size: 266880 Color: 5

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 367739 Color: 18
Size: 337886 Color: 11
Size: 294376 Color: 6

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 429477 Color: 14
Size: 308967 Color: 4
Size: 261557 Color: 7

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 380576 Color: 10
Size: 333304 Color: 5
Size: 286121 Color: 4

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 372222 Color: 17
Size: 333155 Color: 11
Size: 294624 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 419220 Color: 6
Size: 319357 Color: 2
Size: 261424 Color: 7

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 420785 Color: 17
Size: 302907 Color: 6
Size: 276309 Color: 14

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 390076 Color: 12
Size: 326872 Color: 14
Size: 283053 Color: 10

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 391358 Color: 7
Size: 344619 Color: 2
Size: 264024 Color: 5

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 456570 Color: 8
Size: 284243 Color: 4
Size: 259188 Color: 5

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 471191 Color: 15
Size: 268065 Color: 12
Size: 260745 Color: 9

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 488287 Color: 15
Size: 257970 Color: 17
Size: 253744 Color: 4

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 428362 Color: 5
Size: 312591 Color: 0
Size: 259048 Color: 4

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 387163 Color: 19
Size: 309752 Color: 7
Size: 303086 Color: 10

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 431501 Color: 2
Size: 298322 Color: 18
Size: 270178 Color: 18

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 428557 Color: 13
Size: 320136 Color: 17
Size: 251308 Color: 4

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 414572 Color: 19
Size: 306022 Color: 9
Size: 279407 Color: 8

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 348316 Color: 4
Size: 334689 Color: 9
Size: 316996 Color: 12

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 461557 Color: 9
Size: 278683 Color: 7
Size: 259761 Color: 14

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 495556 Color: 19
Size: 253446 Color: 5
Size: 250999 Color: 18

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 398731 Color: 16
Size: 326928 Color: 8
Size: 274342 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 493407 Color: 11
Size: 256526 Color: 13
Size: 250068 Color: 11

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 461256 Color: 1
Size: 276645 Color: 14
Size: 262100 Color: 14

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 480655 Color: 7
Size: 264870 Color: 14
Size: 254476 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 372435 Color: 6
Size: 322947 Color: 2
Size: 304619 Color: 11

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 420060 Color: 9
Size: 304506 Color: 7
Size: 275435 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 393555 Color: 15
Size: 317148 Color: 19
Size: 289298 Color: 17

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 400163 Color: 3
Size: 301635 Color: 19
Size: 298203 Color: 5

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 452508 Color: 14
Size: 296069 Color: 2
Size: 251424 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 490530 Color: 19
Size: 257519 Color: 9
Size: 251952 Color: 2

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 472551 Color: 18
Size: 275552 Color: 10
Size: 251898 Color: 2

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 380317 Color: 6
Size: 323979 Color: 3
Size: 295705 Color: 18

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 410969 Color: 3
Size: 321065 Color: 10
Size: 267967 Color: 15

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 393342 Color: 3
Size: 326706 Color: 0
Size: 279953 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 442213 Color: 13
Size: 301509 Color: 5
Size: 256279 Color: 9

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 412678 Color: 14
Size: 315645 Color: 5
Size: 271678 Color: 8

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 356930 Color: 11
Size: 325430 Color: 19
Size: 317641 Color: 11

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 435409 Color: 5
Size: 288441 Color: 19
Size: 276151 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 478553 Color: 8
Size: 262285 Color: 19
Size: 259163 Color: 2

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 392848 Color: 9
Size: 350575 Color: 17
Size: 256578 Color: 6

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 435854 Color: 8
Size: 297685 Color: 16
Size: 266462 Color: 2

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 363741 Color: 11
Size: 363455 Color: 6
Size: 272805 Color: 19

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 351976 Color: 10
Size: 351247 Color: 12
Size: 296778 Color: 3

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 364762 Color: 7
Size: 354185 Color: 12
Size: 281054 Color: 5

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 494883 Color: 17
Size: 253515 Color: 4
Size: 251603 Color: 19

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 385684 Color: 1
Size: 347272 Color: 0
Size: 267045 Color: 16

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 419134 Color: 2
Size: 305014 Color: 6
Size: 275853 Color: 3

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 489202 Color: 8
Size: 260299 Color: 2
Size: 250500 Color: 2

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 401809 Color: 13
Size: 307033 Color: 2
Size: 291159 Color: 7

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 364088 Color: 7
Size: 328752 Color: 15
Size: 307161 Color: 14

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 419846 Color: 4
Size: 299064 Color: 11
Size: 281091 Color: 10

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 356931 Color: 3
Size: 327064 Color: 4
Size: 316006 Color: 8

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 460646 Color: 6
Size: 273907 Color: 5
Size: 265448 Color: 16

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 388651 Color: 6
Size: 315277 Color: 2
Size: 296073 Color: 19

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 468689 Color: 15
Size: 278126 Color: 9
Size: 253186 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 496692 Color: 2
Size: 251843 Color: 6
Size: 251466 Color: 17

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 369794 Color: 10
Size: 337271 Color: 4
Size: 292936 Color: 2

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 478118 Color: 4
Size: 263971 Color: 17
Size: 257912 Color: 10

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 499012 Color: 15
Size: 250542 Color: 19
Size: 250447 Color: 16

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 410532 Color: 10
Size: 297297 Color: 19
Size: 292172 Color: 7

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 469977 Color: 19
Size: 271462 Color: 9
Size: 258562 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 468775 Color: 7
Size: 268213 Color: 9
Size: 263013 Color: 2

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 356383 Color: 17
Size: 335457 Color: 3
Size: 308161 Color: 3

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 358644 Color: 9
Size: 336593 Color: 4
Size: 304764 Color: 4

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 351090 Color: 7
Size: 336859 Color: 3
Size: 312052 Color: 2

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 359034 Color: 14
Size: 335802 Color: 3
Size: 305165 Color: 5

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 360035 Color: 2
Size: 354374 Color: 8
Size: 285592 Color: 19

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 360050 Color: 0
Size: 344366 Color: 7
Size: 295585 Color: 3

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 359011 Color: 6
Size: 325183 Color: 1
Size: 315807 Color: 4

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 374498 Color: 3
Size: 322378 Color: 7
Size: 303125 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 356204 Color: 8
Size: 329124 Color: 1
Size: 314673 Color: 16

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 368635 Color: 13
Size: 327370 Color: 10
Size: 303996 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 373563 Color: 9
Size: 366021 Color: 11
Size: 260417 Color: 16

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 366472 Color: 5
Size: 349521 Color: 15
Size: 284008 Color: 19

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 358887 Color: 8
Size: 323939 Color: 8
Size: 317175 Color: 2

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 380132 Color: 11
Size: 357911 Color: 7
Size: 261958 Color: 5

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 367186 Color: 19
Size: 348143 Color: 9
Size: 284672 Color: 3

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 368834 Color: 18
Size: 361755 Color: 18
Size: 269412 Color: 1

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 338254 Color: 6
Size: 336420 Color: 4
Size: 325327 Color: 19

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 360008 Color: 10
Size: 331458 Color: 7
Size: 308535 Color: 13

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 370703 Color: 15
Size: 317821 Color: 13
Size: 311477 Color: 3

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 372489 Color: 10
Size: 357474 Color: 17
Size: 270038 Color: 10

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 349333 Color: 5
Size: 340102 Color: 7
Size: 310566 Color: 16

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 398486 Color: 17
Size: 330694 Color: 10
Size: 270821 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 376562 Color: 0
Size: 354747 Color: 6
Size: 268692 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 355098 Color: 6
Size: 337235 Color: 16
Size: 307668 Color: 11

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 377738 Color: 8
Size: 369783 Color: 13
Size: 252480 Color: 3

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 352274 Color: 13
Size: 330591 Color: 5
Size: 317136 Color: 14

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 379700 Color: 12
Size: 354108 Color: 12
Size: 266193 Color: 3

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 380607 Color: 0
Size: 317439 Color: 4
Size: 301955 Color: 3

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 387062 Color: 17
Size: 361692 Color: 11
Size: 251247 Color: 8

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 349722 Color: 9
Size: 331650 Color: 12
Size: 318629 Color: 19

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 357297 Color: 16
Size: 343530 Color: 14
Size: 299174 Color: 8

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 357445 Color: 2
Size: 339842 Color: 18
Size: 302714 Color: 3

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 347305 Color: 1
Size: 339398 Color: 14
Size: 313298 Color: 3

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 348553 Color: 18
Size: 333404 Color: 16
Size: 318044 Color: 9

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 363894 Color: 17
Size: 360085 Color: 6
Size: 276022 Color: 11

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 361204 Color: 12
Size: 329765 Color: 4
Size: 309032 Color: 12

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 386904 Color: 0
Size: 354299 Color: 19
Size: 258798 Color: 15

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 356686 Color: 9
Size: 342292 Color: 18
Size: 301023 Color: 6

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 367444 Color: 7
Size: 331247 Color: 8
Size: 301310 Color: 12

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 364662 Color: 7
Size: 318637 Color: 4
Size: 316702 Color: 8

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 364454 Color: 8
Size: 357575 Color: 13
Size: 277972 Color: 7

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 341602 Color: 3
Size: 335284 Color: 8
Size: 323115 Color: 9

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 370566 Color: 4
Size: 365107 Color: 5
Size: 264328 Color: 13

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 365535 Color: 4
Size: 338117 Color: 7
Size: 296349 Color: 1

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 363596 Color: 14
Size: 361278 Color: 6
Size: 275127 Color: 5

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 377814 Color: 16
Size: 319113 Color: 5
Size: 303074 Color: 7

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 353431 Color: 16
Size: 330701 Color: 12
Size: 315869 Color: 16

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 350290 Color: 13
Size: 332264 Color: 16
Size: 317447 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 345947 Color: 17
Size: 337133 Color: 5
Size: 316921 Color: 17

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 374424 Color: 1
Size: 356950 Color: 17
Size: 268627 Color: 11

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 354826 Color: 13
Size: 339964 Color: 14
Size: 305211 Color: 17

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 359085 Color: 6
Size: 339168 Color: 2
Size: 301748 Color: 4

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 358365 Color: 9
Size: 334409 Color: 10
Size: 307227 Color: 17

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 367462 Color: 5
Size: 356129 Color: 14
Size: 276410 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 356549 Color: 11
Size: 341599 Color: 8
Size: 301853 Color: 9

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 354044 Color: 13
Size: 340754 Color: 4
Size: 305203 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 451440 Color: 8
Size: 293106 Color: 4
Size: 255455 Color: 13

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 355644 Color: 2
Size: 330514 Color: 1
Size: 313843 Color: 4

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 354607 Color: 18
Size: 325775 Color: 3
Size: 319619 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 343119 Color: 4
Size: 341501 Color: 0
Size: 315381 Color: 8

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 366351 Color: 16
Size: 359321 Color: 9
Size: 274329 Color: 5

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 356378 Color: 3
Size: 356233 Color: 13
Size: 287390 Color: 18

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 376986 Color: 12
Size: 356385 Color: 6
Size: 266630 Color: 8

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 359467 Color: 14
Size: 331957 Color: 18
Size: 308577 Color: 3

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 355064 Color: 6
Size: 334244 Color: 19
Size: 310693 Color: 14

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 346610 Color: 6
Size: 345830 Color: 17
Size: 307561 Color: 11

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 359825 Color: 12
Size: 349752 Color: 8
Size: 290424 Color: 8

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 355201 Color: 19
Size: 331657 Color: 2
Size: 313143 Color: 3

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 360098 Color: 11
Size: 343500 Color: 13
Size: 296403 Color: 5

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 361847 Color: 12
Size: 357303 Color: 17
Size: 280851 Color: 15

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 351490 Color: 17
Size: 347186 Color: 10
Size: 301325 Color: 19

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 341616 Color: 19
Size: 336684 Color: 8
Size: 321701 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 351777 Color: 5
Size: 347688 Color: 19
Size: 300536 Color: 18

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 384022 Color: 10
Size: 363455 Color: 7
Size: 252524 Color: 14

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 381500 Color: 14
Size: 357652 Color: 18
Size: 260849 Color: 6

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 359897 Color: 4
Size: 349433 Color: 18
Size: 290671 Color: 6

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 351710 Color: 11
Size: 333956 Color: 1
Size: 314335 Color: 7

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 363888 Color: 0
Size: 320117 Color: 16
Size: 315996 Color: 19

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 352455 Color: 14
Size: 333786 Color: 4
Size: 313760 Color: 8

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 358470 Color: 19
Size: 347897 Color: 15
Size: 293634 Color: 10

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 360923 Color: 7
Size: 349156 Color: 14
Size: 289922 Color: 7

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 353522 Color: 14
Size: 327621 Color: 11
Size: 318858 Color: 7

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 346395 Color: 18
Size: 328366 Color: 2
Size: 325240 Color: 5

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 358140 Color: 10
Size: 323935 Color: 19
Size: 317926 Color: 15

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 355254 Color: 13
Size: 338603 Color: 0
Size: 306144 Color: 13

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 356899 Color: 18
Size: 352005 Color: 14
Size: 291097 Color: 13

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 354663 Color: 13
Size: 324453 Color: 4
Size: 320885 Color: 3

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 340948 Color: 16
Size: 340240 Color: 8
Size: 318813 Color: 15

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 356311 Color: 15
Size: 351905 Color: 4
Size: 291785 Color: 16

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 350392 Color: 19
Size: 336789 Color: 8
Size: 312820 Color: 5

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 366028 Color: 6
Size: 339751 Color: 5
Size: 294222 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 360936 Color: 14
Size: 340692 Color: 19
Size: 298373 Color: 5

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 358199 Color: 7
Size: 342710 Color: 9
Size: 299092 Color: 9

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 373972 Color: 10
Size: 361989 Color: 17
Size: 264040 Color: 17

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 350565 Color: 16
Size: 329310 Color: 1
Size: 320126 Color: 14

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 355663 Color: 3
Size: 334902 Color: 18
Size: 309436 Color: 7

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 355343 Color: 10
Size: 339462 Color: 3
Size: 305196 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 362456 Color: 14
Size: 358376 Color: 8
Size: 279169 Color: 8

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 336003 Color: 17
Size: 333361 Color: 15
Size: 330637 Color: 9

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 359940 Color: 3
Size: 336777 Color: 18
Size: 303284 Color: 5

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 356441 Color: 15
Size: 354768 Color: 17
Size: 288792 Color: 3

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 352184 Color: 0
Size: 346813 Color: 3
Size: 301004 Color: 14

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 351030 Color: 6
Size: 326209 Color: 10
Size: 322762 Color: 14

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 353049 Color: 4
Size: 328416 Color: 15
Size: 318536 Color: 18

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 354025 Color: 12
Size: 325916 Color: 10
Size: 320060 Color: 3

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 356286 Color: 8
Size: 351409 Color: 14
Size: 292306 Color: 12

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 345731 Color: 4
Size: 328519 Color: 4
Size: 325751 Color: 11

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 358235 Color: 15
Size: 340532 Color: 12
Size: 301234 Color: 3

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 340629 Color: 15
Size: 330525 Color: 1
Size: 328847 Color: 15

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 346054 Color: 9
Size: 343929 Color: 6
Size: 310018 Color: 17

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 355255 Color: 9
Size: 341800 Color: 4
Size: 302946 Color: 3

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 361719 Color: 7
Size: 334817 Color: 16
Size: 303465 Color: 12

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 347008 Color: 7
Size: 332897 Color: 15
Size: 320096 Color: 9

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 336523 Color: 17
Size: 334484 Color: 16
Size: 328994 Color: 12

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 342844 Color: 9
Size: 338520 Color: 7
Size: 318637 Color: 9

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 342427 Color: 3
Size: 341652 Color: 9
Size: 315922 Color: 5

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 351966 Color: 4
Size: 337150 Color: 19
Size: 310885 Color: 12

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 353248 Color: 16
Size: 330938 Color: 0
Size: 315815 Color: 11

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 346573 Color: 8
Size: 345905 Color: 18
Size: 307523 Color: 13

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 355312 Color: 14
Size: 338332 Color: 13
Size: 306357 Color: 4

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 362715 Color: 17
Size: 355263 Color: 19
Size: 282023 Color: 12

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 364072 Color: 5
Size: 353565 Color: 2
Size: 282364 Color: 4

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 377594 Color: 3
Size: 354899 Color: 6
Size: 267508 Color: 8

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 358538 Color: 7
Size: 355315 Color: 17
Size: 286148 Color: 5

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 359082 Color: 3
Size: 355250 Color: 9
Size: 285669 Color: 16

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 352646 Color: 14
Size: 342529 Color: 19
Size: 304826 Color: 4

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 353316 Color: 1
Size: 352863 Color: 19
Size: 293822 Color: 18

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 355214 Color: 12
Size: 340717 Color: 0
Size: 304070 Color: 17

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 348907 Color: 9
Size: 346631 Color: 5
Size: 304463 Color: 15

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 342999 Color: 1
Size: 341904 Color: 10
Size: 315098 Color: 17

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 359313 Color: 8
Size: 353500 Color: 19
Size: 287188 Color: 12

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 358657 Color: 2
Size: 351358 Color: 12
Size: 289986 Color: 11

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 358889 Color: 18
Size: 338507 Color: 18
Size: 302605 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 383321 Color: 15
Size: 355301 Color: 8
Size: 261379 Color: 4

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 347799 Color: 19
Size: 346455 Color: 6
Size: 305747 Color: 8

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 350011 Color: 16
Size: 346745 Color: 13
Size: 303245 Color: 12

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 362783 Color: 7
Size: 346148 Color: 3
Size: 291070 Color: 18

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 350953 Color: 2
Size: 349276 Color: 19
Size: 299772 Color: 19

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 347699 Color: 4
Size: 347274 Color: 12
Size: 305028 Color: 9

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 364244 Color: 6
Size: 352676 Color: 7
Size: 283081 Color: 17

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 350761 Color: 13
Size: 343282 Color: 7
Size: 305958 Color: 10

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 445877 Color: 19
Size: 287492 Color: 16
Size: 266632 Color: 7

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 351606 Color: 11
Size: 348252 Color: 3
Size: 300143 Color: 6

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 345353 Color: 19
Size: 339559 Color: 17
Size: 315089 Color: 3

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 344679 Color: 9
Size: 328769 Color: 4
Size: 326553 Color: 8

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 349638 Color: 15
Size: 344852 Color: 18
Size: 305511 Color: 18

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 354510 Color: 6
Size: 327519 Color: 7
Size: 317972 Color: 8

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 349926 Color: 13
Size: 344973 Color: 16
Size: 305102 Color: 16

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 344972 Color: 6
Size: 337810 Color: 9
Size: 317219 Color: 9

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 346125 Color: 4
Size: 328612 Color: 1
Size: 325264 Color: 17

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 346749 Color: 8
Size: 342793 Color: 9
Size: 310459 Color: 12

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 352822 Color: 3
Size: 343538 Color: 9
Size: 303641 Color: 13

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 343390 Color: 13
Size: 339682 Color: 12
Size: 316929 Color: 9

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 365762 Color: 3
Size: 343769 Color: 10
Size: 290470 Color: 3

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 349924 Color: 4
Size: 335339 Color: 19
Size: 314738 Color: 1

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 342689 Color: 16
Size: 341734 Color: 11
Size: 315578 Color: 15

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 357602 Color: 0
Size: 342521 Color: 7
Size: 299878 Color: 2

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 359325 Color: 6
Size: 336098 Color: 17
Size: 304578 Color: 11

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 347744 Color: 12
Size: 343209 Color: 2
Size: 309048 Color: 18

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 341808 Color: 14
Size: 341396 Color: 1
Size: 316797 Color: 12

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 345130 Color: 0
Size: 339917 Color: 8
Size: 314954 Color: 3

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 350772 Color: 5
Size: 341820 Color: 9
Size: 307409 Color: 3

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 345260 Color: 10
Size: 341149 Color: 7
Size: 313592 Color: 4

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 357391 Color: 16
Size: 354693 Color: 9
Size: 287917 Color: 12

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 340883 Color: 4
Size: 334199 Color: 3
Size: 324919 Color: 3

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 341467 Color: 11
Size: 339337 Color: 14
Size: 319197 Color: 13

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 340056 Color: 10
Size: 339419 Color: 15
Size: 320526 Color: 1

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 356005 Color: 4
Size: 340042 Color: 15
Size: 303954 Color: 7

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 339207 Color: 3
Size: 332222 Color: 7
Size: 328572 Color: 18

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 346709 Color: 16
Size: 341288 Color: 3
Size: 312004 Color: 9

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 350281 Color: 14
Size: 339094 Color: 2
Size: 310626 Color: 12

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 354339 Color: 7
Size: 336930 Color: 2
Size: 308732 Color: 16

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 339181 Color: 9
Size: 337087 Color: 10
Size: 323733 Color: 16

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 339225 Color: 9
Size: 337045 Color: 14
Size: 323731 Color: 19

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 337593 Color: 15
Size: 332906 Color: 17
Size: 329502 Color: 12

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 345672 Color: 11
Size: 337754 Color: 10
Size: 316575 Color: 10

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 340059 Color: 7
Size: 338388 Color: 10
Size: 321554 Color: 3

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 349527 Color: 0
Size: 325932 Color: 5
Size: 324542 Color: 4

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 339092 Color: 1
Size: 334282 Color: 4
Size: 326627 Color: 12

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 341752 Color: 18
Size: 330958 Color: 4
Size: 327291 Color: 9

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 350628 Color: 15
Size: 333488 Color: 2
Size: 315885 Color: 3

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 338388 Color: 16
Size: 334147 Color: 1
Size: 327466 Color: 12

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 350261 Color: 17
Size: 329350 Color: 19
Size: 320390 Color: 6

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 360664 Color: 0
Size: 333211 Color: 13
Size: 306126 Color: 2

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 347970 Color: 1
Size: 345339 Color: 16
Size: 306692 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 356973 Color: 7
Size: 331899 Color: 13
Size: 311129 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 357789 Color: 7
Size: 323387 Color: 11
Size: 318825 Color: 8

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 358427 Color: 7
Size: 324025 Color: 6
Size: 317549 Color: 1

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 358771 Color: 7
Size: 331289 Color: 10
Size: 309941 Color: 5

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 359067 Color: 18
Size: 351778 Color: 5
Size: 289156 Color: 12

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 359207 Color: 10
Size: 338352 Color: 13
Size: 302442 Color: 16

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 358495 Color: 5
Size: 337387 Color: 5
Size: 304119 Color: 17

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 359569 Color: 7
Size: 339222 Color: 9
Size: 301210 Color: 5

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 359576 Color: 19
Size: 334955 Color: 6
Size: 305470 Color: 13

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 359792 Color: 16
Size: 334560 Color: 10
Size: 305649 Color: 7

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 359944 Color: 15
Size: 353273 Color: 1
Size: 286784 Color: 5

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 359953 Color: 1
Size: 325230 Color: 10
Size: 314818 Color: 18

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 360435 Color: 12
Size: 351418 Color: 12
Size: 288148 Color: 7

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 360897 Color: 19
Size: 323734 Color: 2
Size: 315370 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 361101 Color: 2
Size: 351889 Color: 15
Size: 287011 Color: 10

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 361163 Color: 9
Size: 332224 Color: 5
Size: 306614 Color: 5

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 361227 Color: 17
Size: 351456 Color: 15
Size: 287318 Color: 18

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 361488 Color: 14
Size: 339275 Color: 19
Size: 299238 Color: 7

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 361554 Color: 1
Size: 342216 Color: 13
Size: 296231 Color: 14

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 361038 Color: 16
Size: 327136 Color: 11
Size: 311827 Color: 15

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 362078 Color: 7
Size: 348350 Color: 18
Size: 289573 Color: 6

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 362139 Color: 2
Size: 361264 Color: 8
Size: 276598 Color: 7

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 362192 Color: 2
Size: 320791 Color: 6
Size: 317018 Color: 14

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 362391 Color: 10
Size: 327022 Color: 0
Size: 310588 Color: 12

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 362448 Color: 14
Size: 357477 Color: 10
Size: 280076 Color: 13

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 362448 Color: 18
Size: 349148 Color: 0
Size: 288405 Color: 15

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 362457 Color: 2
Size: 345246 Color: 7
Size: 292298 Color: 9

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 362487 Color: 17
Size: 328673 Color: 15
Size: 308841 Color: 8

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 362511 Color: 6
Size: 336801 Color: 7
Size: 300689 Color: 10

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 362491 Color: 8
Size: 332588 Color: 14
Size: 304922 Color: 11

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 362748 Color: 16
Size: 349579 Color: 17
Size: 287674 Color: 15

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 362773 Color: 7
Size: 347510 Color: 19
Size: 289718 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 362783 Color: 16
Size: 326707 Color: 15
Size: 310511 Color: 1

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 362888 Color: 6
Size: 336606 Color: 7
Size: 300507 Color: 4

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 363256 Color: 3
Size: 360763 Color: 10
Size: 275982 Color: 13

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 363140 Color: 15
Size: 351109 Color: 4
Size: 285752 Color: 11

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 363339 Color: 1
Size: 340483 Color: 11
Size: 296179 Color: 12

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 363483 Color: 4
Size: 331059 Color: 1
Size: 305459 Color: 19

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 363521 Color: 1
Size: 362339 Color: 12
Size: 274141 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 363596 Color: 0
Size: 322554 Color: 11
Size: 313851 Color: 16

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 363596 Color: 19
Size: 357091 Color: 2
Size: 279314 Color: 9

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 363729 Color: 10
Size: 319493 Color: 11
Size: 316779 Color: 4

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 363756 Color: 0
Size: 325302 Color: 11
Size: 310943 Color: 9

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 363797 Color: 0
Size: 346010 Color: 9
Size: 290194 Color: 8

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 363850 Color: 2
Size: 362734 Color: 14
Size: 273417 Color: 2

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 363862 Color: 1
Size: 351531 Color: 1
Size: 284608 Color: 15

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 363894 Color: 15
Size: 349543 Color: 5
Size: 286564 Color: 18

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 363906 Color: 7
Size: 335315 Color: 13
Size: 300780 Color: 13

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 363941 Color: 17
Size: 329194 Color: 3
Size: 306866 Color: 16

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 364029 Color: 14
Size: 324737 Color: 9
Size: 311235 Color: 1

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 364064 Color: 9
Size: 323637 Color: 9
Size: 312300 Color: 7

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 364113 Color: 19
Size: 358605 Color: 6
Size: 277283 Color: 10

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 364159 Color: 19
Size: 338266 Color: 11
Size: 297576 Color: 1

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 364184 Color: 12
Size: 341781 Color: 1
Size: 294036 Color: 11

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 364185 Color: 1
Size: 321268 Color: 15
Size: 314548 Color: 11

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 364271 Color: 12
Size: 340624 Color: 6
Size: 295106 Color: 13

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 364280 Color: 8
Size: 353277 Color: 9
Size: 282444 Color: 18

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 364267 Color: 11
Size: 335618 Color: 17
Size: 300116 Color: 14

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 364543 Color: 18
Size: 320398 Color: 16
Size: 315060 Color: 19

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 364591 Color: 5
Size: 335495 Color: 19
Size: 299915 Color: 2

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 364591 Color: 13
Size: 317999 Color: 16
Size: 317411 Color: 17

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 364680 Color: 6
Size: 364296 Color: 13
Size: 271025 Color: 2

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 364757 Color: 5
Size: 364623 Color: 14
Size: 270621 Color: 2

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 364762 Color: 15
Size: 335634 Color: 16
Size: 299605 Color: 6

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 364823 Color: 4
Size: 330840 Color: 13
Size: 304338 Color: 10

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 364777 Color: 7
Size: 328011 Color: 8
Size: 307213 Color: 11

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 364931 Color: 4
Size: 360783 Color: 12
Size: 274287 Color: 14

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 364958 Color: 3
Size: 327129 Color: 17
Size: 307914 Color: 4

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 364973 Color: 6
Size: 324663 Color: 2
Size: 310365 Color: 13

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 365025 Color: 9
Size: 324294 Color: 6
Size: 310682 Color: 14

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 365063 Color: 14
Size: 322418 Color: 18
Size: 312520 Color: 18

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 365135 Color: 2
Size: 351349 Color: 19
Size: 283517 Color: 5

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 365161 Color: 13
Size: 330252 Color: 12
Size: 304588 Color: 2

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 365291 Color: 15
Size: 323327 Color: 9
Size: 311383 Color: 17

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 365376 Color: 2
Size: 339005 Color: 17
Size: 295620 Color: 17

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 365392 Color: 8
Size: 335667 Color: 0
Size: 298942 Color: 4

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 365422 Color: 8
Size: 335076 Color: 1
Size: 299503 Color: 2

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 365466 Color: 13
Size: 347681 Color: 15
Size: 286854 Color: 3

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 365498 Color: 14
Size: 321995 Color: 9
Size: 312508 Color: 10

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 365525 Color: 12
Size: 324997 Color: 19
Size: 309479 Color: 10

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 365607 Color: 14
Size: 344042 Color: 8
Size: 290352 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 365625 Color: 17
Size: 365472 Color: 7
Size: 268904 Color: 19

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 365657 Color: 5
Size: 326143 Color: 14
Size: 308201 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 365693 Color: 16
Size: 363599 Color: 1
Size: 270709 Color: 3

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 365823 Color: 5
Size: 317815 Color: 16
Size: 316363 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 365862 Color: 16
Size: 349418 Color: 18
Size: 284721 Color: 5

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 366008 Color: 3
Size: 348121 Color: 5
Size: 285872 Color: 4

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 366013 Color: 15
Size: 331706 Color: 5
Size: 302282 Color: 8

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 365862 Color: 14
Size: 338944 Color: 19
Size: 295195 Color: 9

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 366044 Color: 8
Size: 360253 Color: 10
Size: 273704 Color: 2

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 366055 Color: 17
Size: 327324 Color: 12
Size: 306622 Color: 6

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 366078 Color: 3
Size: 319621 Color: 4
Size: 314302 Color: 2

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 366093 Color: 13
Size: 337614 Color: 18
Size: 296294 Color: 4

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 365834 Color: 19
Size: 347165 Color: 5
Size: 287002 Color: 10

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 366169 Color: 5
Size: 351211 Color: 14
Size: 282621 Color: 6

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 366255 Color: 17
Size: 366134 Color: 10
Size: 267612 Color: 2

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 366271 Color: 16
Size: 339763 Color: 6
Size: 293967 Color: 12

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 366374 Color: 18
Size: 338817 Color: 17
Size: 294810 Color: 2

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 366398 Color: 6
Size: 323477 Color: 10
Size: 310126 Color: 15

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 366398 Color: 1
Size: 320338 Color: 18
Size: 313265 Color: 6

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 366439 Color: 12
Size: 343407 Color: 14
Size: 290155 Color: 6

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 366447 Color: 6
Size: 337681 Color: 9
Size: 295873 Color: 11

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 366465 Color: 7
Size: 340456 Color: 11
Size: 293080 Color: 12

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 366475 Color: 12
Size: 360716 Color: 0
Size: 272810 Color: 2

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 366522 Color: 17
Size: 346084 Color: 8
Size: 287395 Color: 10

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 366600 Color: 4
Size: 366475 Color: 12
Size: 266926 Color: 10

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 366610 Color: 3
Size: 321379 Color: 0
Size: 312012 Color: 9

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 366611 Color: 6
Size: 321898 Color: 7
Size: 311492 Color: 9

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 366771 Color: 5
Size: 319102 Color: 4
Size: 314128 Color: 18

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 366751 Color: 1
Size: 346470 Color: 9
Size: 286780 Color: 2

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 366811 Color: 3
Size: 320395 Color: 16
Size: 312795 Color: 3

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 366854 Color: 3
Size: 356073 Color: 8
Size: 277074 Color: 7

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 366857 Color: 1
Size: 335419 Color: 5
Size: 297725 Color: 17

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 366866 Color: 1
Size: 328835 Color: 4
Size: 304300 Color: 16

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 366897 Color: 16
Size: 342501 Color: 7
Size: 290603 Color: 18

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 366899 Color: 8
Size: 335669 Color: 1
Size: 297433 Color: 6

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 366911 Color: 15
Size: 346895 Color: 14
Size: 286195 Color: 6

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 366922 Color: 14
Size: 347556 Color: 13
Size: 285523 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 366985 Color: 6
Size: 323416 Color: 8
Size: 309600 Color: 9

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 367000 Color: 13
Size: 349877 Color: 17
Size: 283124 Color: 14

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 367026 Color: 19
Size: 341249 Color: 18
Size: 291726 Color: 16

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 367067 Color: 18
Size: 333689 Color: 3
Size: 299245 Color: 13

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 367243 Color: 17
Size: 364358 Color: 4
Size: 268400 Color: 13

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 367292 Color: 11
Size: 345975 Color: 12
Size: 286734 Color: 18

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 367314 Color: 11
Size: 347734 Color: 16
Size: 284953 Color: 15

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 367340 Color: 15
Size: 360531 Color: 13
Size: 272130 Color: 15

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 367343 Color: 10
Size: 325234 Color: 8
Size: 307424 Color: 17

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 367362 Color: 17
Size: 357761 Color: 4
Size: 274878 Color: 8

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 367441 Color: 2
Size: 357707 Color: 1
Size: 274853 Color: 11

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 367480 Color: 14
Size: 337362 Color: 6
Size: 295159 Color: 2

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 367499 Color: 12
Size: 356699 Color: 13
Size: 275803 Color: 10

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 367685 Color: 16
Size: 322694 Color: 4
Size: 309622 Color: 15

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 367694 Color: 16
Size: 365067 Color: 4
Size: 267240 Color: 6

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 367827 Color: 14
Size: 334943 Color: 11
Size: 297231 Color: 11

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 367846 Color: 18
Size: 336957 Color: 2
Size: 295198 Color: 11

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 367914 Color: 18
Size: 338320 Color: 1
Size: 293767 Color: 14

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 367944 Color: 7
Size: 335455 Color: 0
Size: 296602 Color: 15

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 368031 Color: 6
Size: 362321 Color: 1
Size: 269649 Color: 15

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 368309 Color: 3
Size: 328710 Color: 11
Size: 302982 Color: 10

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 368082 Color: 5
Size: 319470 Color: 7
Size: 312449 Color: 5

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 368184 Color: 16
Size: 334823 Color: 8
Size: 296994 Color: 4

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 368192 Color: 16
Size: 333397 Color: 13
Size: 298412 Color: 14

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 368267 Color: 12
Size: 323348 Color: 8
Size: 308386 Color: 13

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 368493 Color: 3
Size: 362062 Color: 16
Size: 269446 Color: 7

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 368397 Color: 6
Size: 323930 Color: 12
Size: 307674 Color: 16

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 368454 Color: 1
Size: 358723 Color: 17
Size: 272824 Color: 16

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 368532 Color: 19
Size: 316119 Color: 4
Size: 315350 Color: 5

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 368552 Color: 19
Size: 357315 Color: 13
Size: 274134 Color: 11

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 368660 Color: 10
Size: 318687 Color: 7
Size: 312654 Color: 15

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 368666 Color: 10
Size: 326509 Color: 4
Size: 304826 Color: 12

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 368669 Color: 5
Size: 362418 Color: 17
Size: 268914 Color: 1

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 368793 Color: 9
Size: 350241 Color: 14
Size: 280967 Color: 10

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 368877 Color: 1
Size: 319635 Color: 10
Size: 311489 Color: 4

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 369083 Color: 6
Size: 366311 Color: 2
Size: 264607 Color: 5

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 369119 Color: 9
Size: 329235 Color: 9
Size: 301647 Color: 15

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 369057 Color: 17
Size: 324662 Color: 8
Size: 306282 Color: 10

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 369180 Color: 9
Size: 333766 Color: 19
Size: 297055 Color: 5

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 369200 Color: 10
Size: 357394 Color: 13
Size: 273407 Color: 14

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 369268 Color: 16
Size: 365255 Color: 12
Size: 265478 Color: 18

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 369288 Color: 1
Size: 340816 Color: 11
Size: 289897 Color: 7

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 369310 Color: 15
Size: 339701 Color: 17
Size: 290990 Color: 16

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 369354 Color: 0
Size: 317600 Color: 2
Size: 313047 Color: 5

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 369410 Color: 6
Size: 339406 Color: 9
Size: 291185 Color: 15

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 369480 Color: 4
Size: 349316 Color: 12
Size: 281205 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 369500 Color: 11
Size: 361387 Color: 14
Size: 269114 Color: 2

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 369525 Color: 10
Size: 366155 Color: 0
Size: 264321 Color: 8

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 369529 Color: 16
Size: 366023 Color: 9
Size: 264449 Color: 9

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 369637 Color: 7
Size: 369019 Color: 2
Size: 261345 Color: 11

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 369553 Color: 14
Size: 339140 Color: 7
Size: 291308 Color: 13

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 369598 Color: 19
Size: 324433 Color: 1
Size: 305970 Color: 9

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 369648 Color: 13
Size: 328950 Color: 13
Size: 301403 Color: 4

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 369714 Color: 2
Size: 353392 Color: 10
Size: 276895 Color: 17

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 369698 Color: 16
Size: 350754 Color: 16
Size: 279549 Color: 15

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 369818 Color: 19
Size: 363648 Color: 0
Size: 266535 Color: 15

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 369849 Color: 11
Size: 348288 Color: 1
Size: 281864 Color: 2

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 369903 Color: 9
Size: 341223 Color: 5
Size: 288875 Color: 3

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 369908 Color: 2
Size: 332903 Color: 18
Size: 297190 Color: 19

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 369786 Color: 16
Size: 336866 Color: 9
Size: 293349 Color: 14

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 369955 Color: 2
Size: 362512 Color: 7
Size: 267534 Color: 17

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 369965 Color: 19
Size: 362949 Color: 18
Size: 267087 Color: 14

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 369973 Color: 19
Size: 363181 Color: 6
Size: 266847 Color: 12

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 369999 Color: 18
Size: 315077 Color: 13
Size: 314925 Color: 1

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 370003 Color: 15
Size: 358716 Color: 9
Size: 271282 Color: 1

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 370031 Color: 10
Size: 332883 Color: 0
Size: 297087 Color: 8

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 370074 Color: 11
Size: 334521 Color: 16
Size: 295406 Color: 15

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 370362 Color: 4
Size: 363178 Color: 14
Size: 266461 Color: 3

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 370158 Color: 5
Size: 325968 Color: 6
Size: 303875 Color: 3

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 370177 Color: 5
Size: 325498 Color: 2
Size: 304326 Color: 2

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 370379 Color: 3
Size: 333617 Color: 15
Size: 296005 Color: 15

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 370489 Color: 18
Size: 340627 Color: 12
Size: 288885 Color: 6

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 370503 Color: 5
Size: 358089 Color: 2
Size: 271409 Color: 12

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 370570 Color: 10
Size: 342950 Color: 8
Size: 286481 Color: 10

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 370592 Color: 11
Size: 316746 Color: 10
Size: 312663 Color: 12

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 370607 Color: 11
Size: 362118 Color: 16
Size: 267276 Color: 6

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 370708 Color: 5
Size: 325958 Color: 11
Size: 303335 Color: 3

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 370731 Color: 13
Size: 317776 Color: 7
Size: 311494 Color: 1

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 370752 Color: 13
Size: 326773 Color: 11
Size: 302476 Color: 16

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 370455 Color: 16
Size: 332880 Color: 3
Size: 296666 Color: 8

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 370950 Color: 0
Size: 349049 Color: 19
Size: 280002 Color: 17

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 370999 Color: 17
Size: 367974 Color: 3
Size: 261028 Color: 5

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 371072 Color: 6
Size: 320547 Color: 18
Size: 308382 Color: 1

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 371077 Color: 14
Size: 330442 Color: 0
Size: 298482 Color: 5

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 371099 Color: 13
Size: 329607 Color: 13
Size: 299295 Color: 2

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 371138 Color: 15
Size: 315931 Color: 11
Size: 312932 Color: 7

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 371178 Color: 8
Size: 326349 Color: 0
Size: 302474 Color: 17

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 371220 Color: 3
Size: 323160 Color: 14
Size: 305621 Color: 10

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 371353 Color: 10
Size: 365025 Color: 1
Size: 263623 Color: 8

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 371353 Color: 6
Size: 341825 Color: 1
Size: 286823 Color: 16

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 371401 Color: 14
Size: 329096 Color: 18
Size: 299504 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 371441 Color: 2
Size: 363907 Color: 17
Size: 264653 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 371589 Color: 14
Size: 314455 Color: 18
Size: 313957 Color: 8

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 371618 Color: 8
Size: 325364 Color: 12
Size: 303019 Color: 10

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 371674 Color: 0
Size: 328805 Color: 4
Size: 299522 Color: 17

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 371709 Color: 5
Size: 332676 Color: 1
Size: 295616 Color: 10

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 371725 Color: 0
Size: 351988 Color: 15
Size: 276288 Color: 17

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 371829 Color: 14
Size: 319983 Color: 17
Size: 308189 Color: 2

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 371876 Color: 6
Size: 341956 Color: 15
Size: 286169 Color: 19

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 371881 Color: 13
Size: 336093 Color: 10
Size: 292027 Color: 15

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 371904 Color: 19
Size: 319641 Color: 17
Size: 308456 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 371962 Color: 19
Size: 364653 Color: 6
Size: 263386 Color: 14

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 371976 Color: 8
Size: 340836 Color: 13
Size: 287189 Color: 17

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 372063 Color: 9
Size: 334004 Color: 18
Size: 293934 Color: 4

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 372115 Color: 9
Size: 346881 Color: 3
Size: 281005 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 372164 Color: 5
Size: 329161 Color: 6
Size: 298676 Color: 12

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 372208 Color: 6
Size: 318604 Color: 9
Size: 309189 Color: 16

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 372262 Color: 9
Size: 337339 Color: 0
Size: 290400 Color: 2

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 372297 Color: 10
Size: 356613 Color: 3
Size: 271091 Color: 18

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 372300 Color: 5
Size: 330058 Color: 2
Size: 297643 Color: 4

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 372332 Color: 18
Size: 368608 Color: 12
Size: 259061 Color: 18

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 372339 Color: 19
Size: 352300 Color: 14
Size: 275362 Color: 7

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 372387 Color: 7
Size: 315024 Color: 14
Size: 312590 Color: 16

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 372408 Color: 6
Size: 345514 Color: 8
Size: 282079 Color: 15

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 372503 Color: 3
Size: 355975 Color: 14
Size: 271523 Color: 5

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 372614 Color: 12
Size: 329300 Color: 10
Size: 298087 Color: 8

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 372620 Color: 17
Size: 350116 Color: 16
Size: 277265 Color: 11

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 372693 Color: 1
Size: 328958 Color: 9
Size: 298350 Color: 5

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 372625 Color: 11
Size: 362226 Color: 18
Size: 265150 Color: 6

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 372811 Color: 0
Size: 347503 Color: 15
Size: 279687 Color: 11

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 372821 Color: 14
Size: 331175 Color: 8
Size: 296005 Color: 7

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 372827 Color: 3
Size: 351752 Color: 4
Size: 275422 Color: 7

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 372851 Color: 3
Size: 355675 Color: 14
Size: 271475 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 372853 Color: 14
Size: 333318 Color: 7
Size: 293830 Color: 9

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 372908 Color: 17
Size: 342888 Color: 19
Size: 284205 Color: 13

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 373197 Color: 10
Size: 336055 Color: 3
Size: 290749 Color: 10

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 373248 Color: 9
Size: 359546 Color: 6
Size: 267207 Color: 3

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 373315 Color: 0
Size: 317586 Color: 7
Size: 309100 Color: 10

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 373344 Color: 17
Size: 318785 Color: 19
Size: 307872 Color: 4

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 373387 Color: 19
Size: 336517 Color: 13
Size: 290097 Color: 13

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 373463 Color: 18
Size: 362956 Color: 10
Size: 263582 Color: 8

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 373552 Color: 8
Size: 357410 Color: 19
Size: 269039 Color: 5

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 373582 Color: 11
Size: 367057 Color: 2
Size: 259362 Color: 14

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 373590 Color: 19
Size: 336042 Color: 6
Size: 290369 Color: 10

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 373596 Color: 18
Size: 360647 Color: 10
Size: 265758 Color: 12

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 373684 Color: 5
Size: 329101 Color: 17
Size: 297216 Color: 9

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 373695 Color: 7
Size: 323529 Color: 13
Size: 302777 Color: 16

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 373699 Color: 1
Size: 335014 Color: 5
Size: 291288 Color: 15

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 373884 Color: 1
Size: 325239 Color: 19
Size: 300878 Color: 12

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 373772 Color: 14
Size: 332819 Color: 8
Size: 293410 Color: 15

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 373865 Color: 17
Size: 318599 Color: 13
Size: 307537 Color: 10

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 373903 Color: 4
Size: 333571 Color: 12
Size: 292527 Color: 11

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 373970 Color: 1
Size: 336878 Color: 5
Size: 289153 Color: 14

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 373952 Color: 17
Size: 316007 Color: 7
Size: 310042 Color: 15

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 373976 Color: 18
Size: 314055 Color: 15
Size: 311970 Color: 19

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 374001 Color: 8
Size: 344608 Color: 11
Size: 281392 Color: 8

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 374002 Color: 18
Size: 315016 Color: 4
Size: 310983 Color: 13

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 374015 Color: 19
Size: 364413 Color: 11
Size: 261573 Color: 15

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 374102 Color: 18
Size: 337897 Color: 3
Size: 288002 Color: 1

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 374181 Color: 12
Size: 374125 Color: 18
Size: 251695 Color: 2

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 374194 Color: 9
Size: 366608 Color: 13
Size: 259199 Color: 2

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 374261 Color: 0
Size: 358143 Color: 8
Size: 267597 Color: 16

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 374276 Color: 3
Size: 319633 Color: 12
Size: 306092 Color: 6

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 374307 Color: 4
Size: 335655 Color: 17
Size: 290039 Color: 7

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 374335 Color: 3
Size: 332194 Color: 10
Size: 293472 Color: 18

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 374397 Color: 0
Size: 334308 Color: 3
Size: 291296 Color: 14

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 374415 Color: 9
Size: 358025 Color: 13
Size: 267561 Color: 18

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 374483 Color: 3
Size: 312948 Color: 9
Size: 312570 Color: 4

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 374686 Color: 16
Size: 333507 Color: 0
Size: 291808 Color: 9

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 374702 Color: 13
Size: 356807 Color: 2
Size: 268492 Color: 3

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 374712 Color: 18
Size: 314493 Color: 15
Size: 310796 Color: 6

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 374775 Color: 13
Size: 345503 Color: 17
Size: 279723 Color: 18

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 374915 Color: 4
Size: 350661 Color: 11
Size: 274425 Color: 16

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 375011 Color: 12
Size: 334779 Color: 17
Size: 290211 Color: 10

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 375033 Color: 15
Size: 372954 Color: 5
Size: 252014 Color: 1

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 375074 Color: 5
Size: 342687 Color: 13
Size: 282240 Color: 7

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 375102 Color: 16
Size: 334110 Color: 14
Size: 290789 Color: 19

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 375134 Color: 2
Size: 346580 Color: 16
Size: 278287 Color: 6

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 375299 Color: 13
Size: 338143 Color: 11
Size: 286559 Color: 15

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 375307 Color: 7
Size: 362348 Color: 1
Size: 262346 Color: 14

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 375321 Color: 15
Size: 360123 Color: 3
Size: 264557 Color: 10

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 375322 Color: 7
Size: 332621 Color: 3
Size: 292058 Color: 19

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 375403 Color: 19
Size: 334791 Color: 6
Size: 289807 Color: 19

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 375403 Color: 9
Size: 347876 Color: 11
Size: 276722 Color: 2

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 375493 Color: 8
Size: 325661 Color: 12
Size: 298847 Color: 11

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 375497 Color: 6
Size: 323796 Color: 16
Size: 300708 Color: 2

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 375512 Color: 8
Size: 374089 Color: 5
Size: 250400 Color: 19

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 375532 Color: 13
Size: 337858 Color: 9
Size: 286611 Color: 16

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 375634 Color: 7
Size: 322192 Color: 12
Size: 302175 Color: 11

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 375684 Color: 6
Size: 320557 Color: 10
Size: 303760 Color: 3

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 375674 Color: 1
Size: 349328 Color: 14
Size: 274999 Color: 10

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 375769 Color: 15
Size: 373531 Color: 17
Size: 250701 Color: 12

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 375790 Color: 9
Size: 361676 Color: 7
Size: 262535 Color: 19

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 375811 Color: 4
Size: 359583 Color: 12
Size: 264607 Color: 19

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 375821 Color: 2
Size: 339736 Color: 3
Size: 284444 Color: 5

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 375863 Color: 3
Size: 354443 Color: 19
Size: 269695 Color: 15

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 375995 Color: 6
Size: 320115 Color: 19
Size: 303891 Color: 19

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 376008 Color: 3
Size: 364540 Color: 6
Size: 259453 Color: 15

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 376020 Color: 2
Size: 372980 Color: 7
Size: 251001 Color: 15

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 376020 Color: 14
Size: 359885 Color: 4
Size: 264096 Color: 8

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 376058 Color: 6
Size: 318327 Color: 4
Size: 305616 Color: 15

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 376146 Color: 1
Size: 331531 Color: 7
Size: 292324 Color: 14

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 376075 Color: 9
Size: 353777 Color: 13
Size: 270149 Color: 11

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 376142 Color: 14
Size: 317468 Color: 16
Size: 306391 Color: 4

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 376143 Color: 12
Size: 332497 Color: 0
Size: 291361 Color: 13

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 376162 Color: 5
Size: 322054 Color: 10
Size: 301785 Color: 9

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 376194 Color: 19
Size: 347224 Color: 14
Size: 276583 Color: 12

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 376293 Color: 19
Size: 338850 Color: 4
Size: 284858 Color: 1

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 376303 Color: 17
Size: 356137 Color: 12
Size: 267561 Color: 13

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 376363 Color: 3
Size: 316872 Color: 15
Size: 306766 Color: 17

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 376364 Color: 3
Size: 321988 Color: 11
Size: 301649 Color: 13

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 376423 Color: 17
Size: 344785 Color: 0
Size: 278793 Color: 12

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 376461 Color: 6
Size: 336545 Color: 10
Size: 286995 Color: 7

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 376469 Color: 4
Size: 315540 Color: 19
Size: 307992 Color: 7

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 376515 Color: 17
Size: 360777 Color: 8
Size: 262709 Color: 2

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 376539 Color: 10
Size: 324037 Color: 16
Size: 299425 Color: 17

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 376554 Color: 18
Size: 361419 Color: 3
Size: 262028 Color: 9

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 376560 Color: 12
Size: 349341 Color: 2
Size: 274100 Color: 6

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 376567 Color: 11
Size: 338385 Color: 6
Size: 285049 Color: 1

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 376590 Color: 6
Size: 359551 Color: 18
Size: 263860 Color: 8

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 376601 Color: 13
Size: 344735 Color: 2
Size: 278665 Color: 12

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 376619 Color: 14
Size: 369326 Color: 15
Size: 254056 Color: 4

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 376634 Color: 13
Size: 316684 Color: 4
Size: 306683 Color: 11

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 376800 Color: 7
Size: 337537 Color: 15
Size: 285664 Color: 10

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 376834 Color: 18
Size: 347540 Color: 5
Size: 275627 Color: 13

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 376861 Color: 9
Size: 361930 Color: 14
Size: 261210 Color: 10

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 376893 Color: 2
Size: 315923 Color: 3
Size: 307185 Color: 5

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 377050 Color: 4
Size: 326663 Color: 16
Size: 296288 Color: 1

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 377131 Color: 2
Size: 348910 Color: 6
Size: 273960 Color: 16

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 377170 Color: 11
Size: 346679 Color: 14
Size: 276152 Color: 15

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 377187 Color: 10
Size: 372582 Color: 13
Size: 250232 Color: 9

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 377216 Color: 10
Size: 352287 Color: 6
Size: 270498 Color: 15

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 377240 Color: 19
Size: 330595 Color: 11
Size: 292166 Color: 4

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 377256 Color: 8
Size: 334372 Color: 1
Size: 288373 Color: 8

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 377272 Color: 9
Size: 353735 Color: 19
Size: 268994 Color: 12

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 377301 Color: 12
Size: 340637 Color: 7
Size: 282063 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 377312 Color: 13
Size: 330245 Color: 10
Size: 292444 Color: 5

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 377363 Color: 9
Size: 346090 Color: 17
Size: 276548 Color: 11

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 377376 Color: 14
Size: 368298 Color: 4
Size: 254327 Color: 10

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 377416 Color: 5
Size: 352698 Color: 9
Size: 269887 Color: 1

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 377431 Color: 13
Size: 350529 Color: 12
Size: 272041 Color: 19

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 377485 Color: 18
Size: 351512 Color: 12
Size: 271004 Color: 17

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 377493 Color: 17
Size: 328655 Color: 14
Size: 293853 Color: 18

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 377555 Color: 0
Size: 334475 Color: 7
Size: 287971 Color: 4

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 377665 Color: 9
Size: 333160 Color: 3
Size: 289176 Color: 8

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 377701 Color: 19
Size: 358202 Color: 2
Size: 264098 Color: 2

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 377672 Color: 14
Size: 359403 Color: 3
Size: 262926 Color: 19

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 377792 Color: 7
Size: 364668 Color: 6
Size: 257541 Color: 4

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 377905 Color: 2
Size: 318868 Color: 6
Size: 303228 Color: 5

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 377957 Color: 9
Size: 351067 Color: 12
Size: 270977 Color: 17

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 377917 Color: 10
Size: 313075 Color: 14
Size: 309009 Color: 12

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 378050 Color: 9
Size: 353158 Color: 15
Size: 268793 Color: 5

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 378068 Color: 6
Size: 328527 Color: 4
Size: 293406 Color: 15

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 378076 Color: 9
Size: 358588 Color: 12
Size: 263337 Color: 17

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 378082 Color: 6
Size: 337545 Color: 17
Size: 284374 Color: 9

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 377981 Color: 12
Size: 327730 Color: 3
Size: 294290 Color: 14

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 378086 Color: 13
Size: 343921 Color: 11
Size: 277994 Color: 3

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 378097 Color: 19
Size: 325980 Color: 5
Size: 295924 Color: 8

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 378118 Color: 2
Size: 314282 Color: 15
Size: 307601 Color: 18

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 378163 Color: 18
Size: 313944 Color: 15
Size: 307894 Color: 19

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 378268 Color: 17
Size: 344734 Color: 5
Size: 276999 Color: 13

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 378387 Color: 10
Size: 358838 Color: 12
Size: 262776 Color: 1

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 378391 Color: 18
Size: 322421 Color: 6
Size: 299189 Color: 2

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 378514 Color: 13
Size: 327308 Color: 4
Size: 294179 Color: 9

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 378517 Color: 19
Size: 311329 Color: 2
Size: 310155 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 378626 Color: 2
Size: 331663 Color: 6
Size: 289712 Color: 19

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 378631 Color: 5
Size: 339580 Color: 13
Size: 281790 Color: 14

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 378825 Color: 1
Size: 355389 Color: 16
Size: 265787 Color: 12

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 378767 Color: 3
Size: 323184 Color: 7
Size: 298050 Color: 15

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 378806 Color: 16
Size: 339921 Color: 6
Size: 281274 Color: 13

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 378807 Color: 0
Size: 343041 Color: 6
Size: 278153 Color: 3

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 378855 Color: 15
Size: 339031 Color: 4
Size: 282115 Color: 16

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 378864 Color: 4
Size: 325221 Color: 8
Size: 295916 Color: 12

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 378898 Color: 13
Size: 333280 Color: 1
Size: 287823 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 378917 Color: 15
Size: 336834 Color: 3
Size: 284250 Color: 16

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 378941 Color: 13
Size: 356563 Color: 10
Size: 264497 Color: 10

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 378996 Color: 13
Size: 323499 Color: 15
Size: 297506 Color: 18

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 379027 Color: 9
Size: 347906 Color: 0
Size: 273068 Color: 4

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 379052 Color: 6
Size: 361516 Color: 2
Size: 259433 Color: 4

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 379071 Color: 15
Size: 348238 Color: 17
Size: 272692 Color: 3

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 379089 Color: 9
Size: 336911 Color: 1
Size: 284001 Color: 15

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 379109 Color: 18
Size: 336575 Color: 16
Size: 284317 Color: 17

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 379126 Color: 13
Size: 365589 Color: 12
Size: 255286 Color: 10

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 379135 Color: 7
Size: 324176 Color: 0
Size: 296690 Color: 10

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 379268 Color: 13
Size: 353715 Color: 5
Size: 267018 Color: 18

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 379389 Color: 13
Size: 348394 Color: 8
Size: 272218 Color: 12

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 379481 Color: 6
Size: 326545 Color: 11
Size: 293975 Color: 11

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 379483 Color: 15
Size: 362087 Color: 17
Size: 258431 Color: 9

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 379521 Color: 14
Size: 358703 Color: 9
Size: 261777 Color: 16

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 379613 Color: 13
Size: 324502 Color: 17
Size: 295886 Color: 10

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 379708 Color: 11
Size: 322140 Color: 3
Size: 298153 Color: 7

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 379767 Color: 7
Size: 318984 Color: 4
Size: 301250 Color: 3

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 379786 Color: 5
Size: 364383 Color: 9
Size: 255832 Color: 2

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 379881 Color: 16
Size: 337724 Color: 6
Size: 282396 Color: 5

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 379881 Color: 17
Size: 344318 Color: 0
Size: 275802 Color: 16

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 379926 Color: 16
Size: 368808 Color: 13
Size: 251267 Color: 1

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 379931 Color: 12
Size: 343581 Color: 15
Size: 276489 Color: 17

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 379945 Color: 7
Size: 342568 Color: 17
Size: 277488 Color: 2

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 379959 Color: 8
Size: 363162 Color: 11
Size: 256880 Color: 7

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 380075 Color: 14
Size: 333779 Color: 3
Size: 286147 Color: 18

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 380115 Color: 11
Size: 329670 Color: 12
Size: 290216 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 380200 Color: 0
Size: 315627 Color: 8
Size: 304174 Color: 2

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 380209 Color: 14
Size: 324500 Color: 15
Size: 295292 Color: 10

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 380223 Color: 16
Size: 345930 Color: 9
Size: 273848 Color: 14

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 380240 Color: 7
Size: 331088 Color: 4
Size: 288673 Color: 8

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 380326 Color: 16
Size: 345834 Color: 0
Size: 273841 Color: 17

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 380334 Color: 15
Size: 331252 Color: 4
Size: 288415 Color: 19

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 380342 Color: 8
Size: 326363 Color: 11
Size: 293296 Color: 2

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 380344 Color: 4
Size: 357211 Color: 0
Size: 262446 Color: 17

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 380421 Color: 13
Size: 361520 Color: 10
Size: 258060 Color: 16

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 380455 Color: 17
Size: 319473 Color: 3
Size: 300073 Color: 14

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 380461 Color: 11
Size: 343829 Color: 7
Size: 275711 Color: 10

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 380482 Color: 14
Size: 358018 Color: 17
Size: 261501 Color: 8

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 380524 Color: 18
Size: 317710 Color: 12
Size: 301767 Color: 9

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 380532 Color: 8
Size: 324743 Color: 19
Size: 294726 Color: 4

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 380712 Color: 1
Size: 351772 Color: 15
Size: 267517 Color: 13

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 380786 Color: 14
Size: 335836 Color: 8
Size: 283379 Color: 19

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 380797 Color: 10
Size: 317243 Color: 17
Size: 301961 Color: 1

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 380835 Color: 15
Size: 325575 Color: 6
Size: 293591 Color: 9

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 381036 Color: 2
Size: 329570 Color: 17
Size: 289395 Color: 19

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 381045 Color: 18
Size: 322596 Color: 6
Size: 296360 Color: 18

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 381045 Color: 9
Size: 330348 Color: 10
Size: 288608 Color: 19

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 381059 Color: 2
Size: 357142 Color: 0
Size: 261800 Color: 15

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 381185 Color: 13
Size: 356885 Color: 16
Size: 261931 Color: 18

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 381194 Color: 12
Size: 338703 Color: 19
Size: 280104 Color: 8

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 381220 Color: 8
Size: 336448 Color: 10
Size: 282333 Color: 12

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 381278 Color: 19
Size: 321303 Color: 10
Size: 297420 Color: 4

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 381376 Color: 11
Size: 334656 Color: 5
Size: 283969 Color: 4

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 381406 Color: 9
Size: 325393 Color: 9
Size: 293202 Color: 7

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 381420 Color: 11
Size: 347627 Color: 18
Size: 270954 Color: 1

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 381429 Color: 1
Size: 332088 Color: 4
Size: 286484 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 381442 Color: 14
Size: 329380 Color: 3
Size: 289179 Color: 18

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 381627 Color: 10
Size: 322915 Color: 7
Size: 295459 Color: 14

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 381714 Color: 2
Size: 336729 Color: 18
Size: 281558 Color: 5

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 381756 Color: 12
Size: 365011 Color: 2
Size: 253234 Color: 4

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 381789 Color: 13
Size: 351566 Color: 1
Size: 266646 Color: 7

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 381844 Color: 2
Size: 312062 Color: 12
Size: 306095 Color: 16

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 381863 Color: 13
Size: 319994 Color: 19
Size: 298144 Color: 18

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 381876 Color: 19
Size: 324123 Color: 8
Size: 294002 Color: 17

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 381887 Color: 14
Size: 327624 Color: 12
Size: 290490 Color: 12

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 381900 Color: 11
Size: 314231 Color: 9
Size: 303870 Color: 4

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 381904 Color: 2
Size: 353159 Color: 0
Size: 264938 Color: 1

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 381920 Color: 12
Size: 339154 Color: 7
Size: 278927 Color: 8

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 381977 Color: 13
Size: 338751 Color: 16
Size: 279273 Color: 3

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 382050 Color: 0
Size: 335810 Color: 13
Size: 282141 Color: 16

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 382061 Color: 4
Size: 348946 Color: 6
Size: 268994 Color: 12

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 382095 Color: 5
Size: 353798 Color: 19
Size: 264108 Color: 7

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 382119 Color: 5
Size: 317892 Color: 0
Size: 299990 Color: 6

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 382172 Color: 19
Size: 338157 Color: 17
Size: 279672 Color: 17

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 382181 Color: 19
Size: 342718 Color: 3
Size: 275102 Color: 8

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 382272 Color: 5
Size: 360689 Color: 18
Size: 257040 Color: 7

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 382369 Color: 1
Size: 366323 Color: 16
Size: 251309 Color: 12

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 382335 Color: 5
Size: 356369 Color: 3
Size: 261297 Color: 10

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 382348 Color: 8
Size: 315815 Color: 0
Size: 301838 Color: 18

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 382397 Color: 11
Size: 345538 Color: 2
Size: 272066 Color: 17

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 382433 Color: 7
Size: 347112 Color: 17
Size: 270456 Color: 4

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 382706 Color: 19
Size: 314497 Color: 11
Size: 302798 Color: 11

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 382706 Color: 8
Size: 364959 Color: 17
Size: 252336 Color: 18

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 382706 Color: 0
Size: 329642 Color: 0
Size: 287653 Color: 15

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 382828 Color: 11
Size: 349382 Color: 1
Size: 267791 Color: 8

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 382832 Color: 7
Size: 327472 Color: 2
Size: 289697 Color: 2

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 383023 Color: 7
Size: 354944 Color: 18
Size: 262034 Color: 9

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 383151 Color: 11
Size: 364502 Color: 3
Size: 252348 Color: 12

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 383331 Color: 1
Size: 329348 Color: 13
Size: 287322 Color: 12

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 383202 Color: 7
Size: 317616 Color: 6
Size: 299183 Color: 14

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 383251 Color: 6
Size: 342972 Color: 10
Size: 273778 Color: 15

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 383272 Color: 8
Size: 314796 Color: 18
Size: 301933 Color: 10

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 383272 Color: 19
Size: 336939 Color: 8
Size: 279790 Color: 15

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 383353 Color: 1
Size: 365873 Color: 14
Size: 250775 Color: 15

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 383277 Color: 16
Size: 336666 Color: 6
Size: 280058 Color: 4

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 383302 Color: 10
Size: 320369 Color: 0
Size: 296330 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 383342 Color: 0
Size: 316680 Color: 4
Size: 299979 Color: 12

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 383380 Color: 8
Size: 329700 Color: 14
Size: 286921 Color: 10

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 383372 Color: 12
Size: 339416 Color: 19
Size: 277213 Color: 16

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 383382 Color: 15
Size: 321171 Color: 1
Size: 295448 Color: 17

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 383415 Color: 2
Size: 330313 Color: 15
Size: 286273 Color: 10

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 383434 Color: 11
Size: 308861 Color: 13
Size: 307706 Color: 13

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 383562 Color: 14
Size: 336536 Color: 5
Size: 279903 Color: 7

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 383532 Color: 12
Size: 332813 Color: 8
Size: 283656 Color: 15

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 383576 Color: 8
Size: 346086 Color: 13
Size: 270339 Color: 1

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 383637 Color: 5
Size: 342404 Color: 3
Size: 273960 Color: 8

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 383639 Color: 17
Size: 332688 Color: 2
Size: 283674 Color: 17

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 383658 Color: 17
Size: 351463 Color: 5
Size: 264880 Color: 4

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 383682 Color: 17
Size: 338789 Color: 18
Size: 277530 Color: 5

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 383684 Color: 6
Size: 339222 Color: 18
Size: 277095 Color: 10

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 383728 Color: 4
Size: 343672 Color: 12
Size: 272601 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 383731 Color: 2
Size: 336065 Color: 16
Size: 280205 Color: 16

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 383781 Color: 9
Size: 310801 Color: 7
Size: 305419 Color: 6

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 383816 Color: 17
Size: 308257 Color: 13
Size: 307928 Color: 14

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 383849 Color: 11
Size: 336132 Color: 14
Size: 280020 Color: 6

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 383891 Color: 17
Size: 350918 Color: 0
Size: 265192 Color: 18

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 384039 Color: 10
Size: 316381 Color: 3
Size: 299581 Color: 16

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 384093 Color: 11
Size: 310413 Color: 6
Size: 305495 Color: 9

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 384115 Color: 0
Size: 322321 Color: 1
Size: 293565 Color: 13

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 384134 Color: 7
Size: 341701 Color: 4
Size: 274166 Color: 17

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 384145 Color: 10
Size: 325961 Color: 1
Size: 289895 Color: 12

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 384166 Color: 11
Size: 364045 Color: 1
Size: 251790 Color: 11

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 384225 Color: 0
Size: 345019 Color: 8
Size: 270757 Color: 13

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 384236 Color: 2
Size: 364990 Color: 7
Size: 250775 Color: 19

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 384265 Color: 0
Size: 345022 Color: 4
Size: 270714 Color: 3

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 384300 Color: 6
Size: 310091 Color: 13
Size: 305610 Color: 17

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 384393 Color: 10
Size: 315360 Color: 4
Size: 300248 Color: 11

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 384494 Color: 6
Size: 311245 Color: 2
Size: 304262 Color: 14

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 384547 Color: 18
Size: 341354 Color: 14
Size: 274100 Color: 11

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 384622 Color: 11
Size: 313462 Color: 9
Size: 301917 Color: 4

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 384632 Color: 13
Size: 315602 Color: 17
Size: 299767 Color: 3

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 384669 Color: 15
Size: 356927 Color: 0
Size: 258405 Color: 7

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 384676 Color: 9
Size: 328007 Color: 11
Size: 287318 Color: 19

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 384751 Color: 13
Size: 341868 Color: 3
Size: 273382 Color: 7

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 384800 Color: 2
Size: 355392 Color: 0
Size: 259809 Color: 5

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 384838 Color: 7
Size: 309416 Color: 13
Size: 305747 Color: 10

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 384862 Color: 2
Size: 359750 Color: 19
Size: 255389 Color: 4

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 384889 Color: 7
Size: 333140 Color: 6
Size: 281972 Color: 5

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 384918 Color: 3
Size: 352540 Color: 18
Size: 262543 Color: 2

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 384945 Color: 8
Size: 349365 Color: 11
Size: 265691 Color: 3

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 385003 Color: 9
Size: 333613 Color: 10
Size: 281385 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 385022 Color: 10
Size: 362673 Color: 0
Size: 252306 Color: 14

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 385006 Color: 9
Size: 324759 Color: 14
Size: 290236 Color: 13

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 385025 Color: 6
Size: 337642 Color: 17
Size: 277334 Color: 11

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 385030 Color: 16
Size: 338101 Color: 17
Size: 276870 Color: 8

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 385039 Color: 0
Size: 314331 Color: 19
Size: 300631 Color: 12

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 385095 Color: 4
Size: 339414 Color: 17
Size: 275492 Color: 12

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 385101 Color: 7
Size: 338407 Color: 10
Size: 276493 Color: 16

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 385341 Color: 4
Size: 324896 Color: 15
Size: 289764 Color: 3

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 385198 Color: 3
Size: 333952 Color: 12
Size: 280851 Color: 1

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 385207 Color: 16
Size: 344031 Color: 12
Size: 270763 Color: 2

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 385300 Color: 11
Size: 307770 Color: 0
Size: 306931 Color: 8

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 385353 Color: 11
Size: 358558 Color: 9
Size: 256090 Color: 13

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 385431 Color: 5
Size: 314882 Color: 11
Size: 299688 Color: 19

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 385589 Color: 1
Size: 318953 Color: 9
Size: 295459 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 385590 Color: 0
Size: 323955 Color: 7
Size: 290456 Color: 12

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 385793 Color: 16
Size: 339131 Color: 3
Size: 275077 Color: 10

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 385829 Color: 2
Size: 343934 Color: 16
Size: 270238 Color: 5

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 385836 Color: 13
Size: 317528 Color: 8
Size: 296637 Color: 19

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 386081 Color: 4
Size: 311712 Color: 5
Size: 302208 Color: 18

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 385840 Color: 16
Size: 357731 Color: 10
Size: 256430 Color: 14

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 385891 Color: 0
Size: 357582 Color: 13
Size: 256528 Color: 13

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 386024 Color: 9
Size: 323548 Color: 3
Size: 290429 Color: 18

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 386092 Color: 4
Size: 362810 Color: 18
Size: 251099 Color: 14

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 386039 Color: 19
Size: 312155 Color: 16
Size: 301807 Color: 5

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 386167 Color: 11
Size: 357207 Color: 15
Size: 256627 Color: 2

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 386196 Color: 8
Size: 307674 Color: 15
Size: 306131 Color: 7

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 386197 Color: 10
Size: 358483 Color: 9
Size: 255321 Color: 19

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 386261 Color: 12
Size: 351452 Color: 13
Size: 262288 Color: 12

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 386270 Color: 9
Size: 354159 Color: 18
Size: 259572 Color: 3

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 386275 Color: 9
Size: 356205 Color: 7
Size: 257521 Color: 19

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 386283 Color: 17
Size: 339444 Color: 3
Size: 274274 Color: 13

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 386288 Color: 7
Size: 341493 Color: 15
Size: 272220 Color: 15

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 386295 Color: 7
Size: 327829 Color: 10
Size: 285877 Color: 9

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 386314 Color: 3
Size: 336634 Color: 9
Size: 277053 Color: 10

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 386362 Color: 15
Size: 318646 Color: 2
Size: 294993 Color: 8

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 386402 Color: 0
Size: 311015 Color: 12
Size: 302584 Color: 2

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 386410 Color: 0
Size: 360067 Color: 8
Size: 253524 Color: 18

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 386439 Color: 11
Size: 344326 Color: 16
Size: 269236 Color: 7

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 386461 Color: 16
Size: 319717 Color: 11
Size: 293823 Color: 3

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 386473 Color: 15
Size: 308764 Color: 19
Size: 304764 Color: 13

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 386499 Color: 14
Size: 348655 Color: 17
Size: 264847 Color: 16

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 386584 Color: 1
Size: 345986 Color: 8
Size: 267431 Color: 17

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 386592 Color: 13
Size: 309570 Color: 8
Size: 303839 Color: 9

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 386618 Color: 6
Size: 336452 Color: 4
Size: 276931 Color: 17

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 386698 Color: 16
Size: 362837 Color: 10
Size: 250466 Color: 8

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 386775 Color: 2
Size: 347932 Color: 13
Size: 265294 Color: 4

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 386794 Color: 15
Size: 339888 Color: 1
Size: 273319 Color: 14

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 386865 Color: 13
Size: 328138 Color: 15
Size: 284998 Color: 2

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 386880 Color: 8
Size: 346315 Color: 11
Size: 266806 Color: 9

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 386901 Color: 16
Size: 329019 Color: 9
Size: 284081 Color: 7

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 387047 Color: 3
Size: 337414 Color: 18
Size: 275540 Color: 14

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 386972 Color: 2
Size: 326099 Color: 1
Size: 286930 Color: 6

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 387022 Color: 19
Size: 316465 Color: 13
Size: 296514 Color: 4

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 387024 Color: 12
Size: 310767 Color: 1
Size: 302210 Color: 19

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 387045 Color: 11
Size: 320350 Color: 18
Size: 292606 Color: 18

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 387085 Color: 13
Size: 318925 Color: 12
Size: 293991 Color: 17

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 387087 Color: 17
Size: 330345 Color: 2
Size: 282569 Color: 5

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 387164 Color: 12
Size: 356544 Color: 4
Size: 256293 Color: 16

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 387167 Color: 19
Size: 352016 Color: 5
Size: 260818 Color: 19

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 387194 Color: 4
Size: 356426 Color: 2
Size: 256381 Color: 12

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 387211 Color: 2
Size: 321102 Color: 16
Size: 291688 Color: 6

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 387248 Color: 6
Size: 315248 Color: 9
Size: 297505 Color: 2

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 387322 Color: 17
Size: 351567 Color: 0
Size: 261112 Color: 17

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 387332 Color: 9
Size: 326011 Color: 4
Size: 286658 Color: 5

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 387362 Color: 5
Size: 333250 Color: 19
Size: 279389 Color: 11

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 387371 Color: 18
Size: 323944 Color: 11
Size: 288686 Color: 1

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 387384 Color: 13
Size: 347837 Color: 13
Size: 264780 Color: 8

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 387387 Color: 14
Size: 359425 Color: 5
Size: 253189 Color: 15

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 387406 Color: 0
Size: 351731 Color: 17
Size: 260864 Color: 10

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 387409 Color: 11
Size: 332794 Color: 4
Size: 279798 Color: 1

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 387422 Color: 18
Size: 343644 Color: 17
Size: 268935 Color: 16

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 387544 Color: 13
Size: 314043 Color: 17
Size: 298414 Color: 10

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 387575 Color: 5
Size: 321377 Color: 11
Size: 291049 Color: 19

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 387605 Color: 5
Size: 310535 Color: 17
Size: 301861 Color: 9

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 387613 Color: 8
Size: 359452 Color: 10
Size: 252936 Color: 4

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 387625 Color: 7
Size: 355267 Color: 11
Size: 257109 Color: 14

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 387634 Color: 6
Size: 345029 Color: 14
Size: 267338 Color: 8

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 387646 Color: 9
Size: 326999 Color: 14
Size: 285356 Color: 12

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 387682 Color: 7
Size: 334929 Color: 3
Size: 277390 Color: 3

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 387693 Color: 16
Size: 326231 Color: 2
Size: 286077 Color: 15

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 387714 Color: 10
Size: 328760 Color: 11
Size: 283527 Color: 7

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 387849 Color: 4
Size: 313225 Color: 10
Size: 298927 Color: 12

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 387720 Color: 7
Size: 311666 Color: 17
Size: 300615 Color: 5

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 387773 Color: 1
Size: 307418 Color: 16
Size: 304810 Color: 19

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 387880 Color: 19
Size: 306283 Color: 0
Size: 305838 Color: 8

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 387946 Color: 8
Size: 317984 Color: 0
Size: 294071 Color: 10

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 387954 Color: 0
Size: 316338 Color: 4
Size: 295709 Color: 12

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 388023 Color: 15
Size: 336724 Color: 3
Size: 275254 Color: 9

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 388082 Color: 9
Size: 352181 Color: 0
Size: 259738 Color: 15

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 388110 Color: 13
Size: 334209 Color: 5
Size: 277682 Color: 18

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 388167 Color: 18
Size: 340131 Color: 5
Size: 271703 Color: 19

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 388224 Color: 16
Size: 320189 Color: 17
Size: 291588 Color: 18

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 388252 Color: 4
Size: 318517 Color: 2
Size: 293232 Color: 10

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 388226 Color: 13
Size: 316280 Color: 3
Size: 295495 Color: 3

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 388245 Color: 3
Size: 343323 Color: 15
Size: 268433 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 388251 Color: 5
Size: 348540 Color: 0
Size: 263210 Color: 7

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 388252 Color: 14
Size: 337103 Color: 9
Size: 274646 Color: 2

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 388285 Color: 8
Size: 308145 Color: 13
Size: 303571 Color: 19

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 388342 Color: 15
Size: 343207 Color: 6
Size: 268452 Color: 12

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 388401 Color: 17
Size: 318030 Color: 15
Size: 293570 Color: 12

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 388411 Color: 6
Size: 328786 Color: 18
Size: 282804 Color: 14

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 388415 Color: 0
Size: 308157 Color: 7
Size: 303429 Color: 14

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 388532 Color: 16
Size: 333316 Color: 5
Size: 278153 Color: 18

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 388511 Color: 17
Size: 354749 Color: 7
Size: 256741 Color: 2

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 388568 Color: 7
Size: 354329 Color: 4
Size: 257104 Color: 19

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 388584 Color: 12
Size: 359121 Color: 19
Size: 252296 Color: 3

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 388643 Color: 16
Size: 329273 Color: 14
Size: 282085 Color: 1

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 388849 Color: 8
Size: 338558 Color: 17
Size: 272594 Color: 13

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 388860 Color: 19
Size: 336847 Color: 2
Size: 274294 Color: 4

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 388877 Color: 14
Size: 344917 Color: 12
Size: 266207 Color: 13

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 388894 Color: 13
Size: 348595 Color: 5
Size: 262512 Color: 14

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 388917 Color: 3
Size: 333808 Color: 9
Size: 277276 Color: 13

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 388925 Color: 15
Size: 307224 Color: 7
Size: 303852 Color: 17

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 388951 Color: 16
Size: 309563 Color: 4
Size: 301487 Color: 16

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 388954 Color: 10
Size: 352576 Color: 5
Size: 258471 Color: 8

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 388958 Color: 18
Size: 307939 Color: 6
Size: 303104 Color: 1

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 389055 Color: 14
Size: 340807 Color: 19
Size: 270139 Color: 5

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 389080 Color: 8
Size: 312822 Color: 3
Size: 298099 Color: 15

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 389088 Color: 2
Size: 358718 Color: 14
Size: 252195 Color: 1

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 389102 Color: 13
Size: 309459 Color: 4
Size: 301440 Color: 16

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 389135 Color: 5
Size: 306130 Color: 0
Size: 304736 Color: 12

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 389155 Color: 13
Size: 349812 Color: 6
Size: 261034 Color: 19

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 389164 Color: 1
Size: 333720 Color: 9
Size: 277117 Color: 10

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 389167 Color: 0
Size: 336677 Color: 13
Size: 274157 Color: 17

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 389226 Color: 0
Size: 360769 Color: 3
Size: 250006 Color: 4

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 389310 Color: 0
Size: 349118 Color: 17
Size: 261573 Color: 5

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 389330 Color: 12
Size: 334272 Color: 0
Size: 276399 Color: 6

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 389334 Color: 14
Size: 311746 Color: 10
Size: 298921 Color: 13

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 389399 Color: 15
Size: 337902 Color: 15
Size: 272700 Color: 7

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 389430 Color: 7
Size: 323961 Color: 2
Size: 286610 Color: 5

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 389527 Color: 2
Size: 341787 Color: 5
Size: 268687 Color: 18

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 389534 Color: 12
Size: 314159 Color: 18
Size: 296308 Color: 16

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 389546 Color: 3
Size: 328565 Color: 0
Size: 281890 Color: 2

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 389687 Color: 15
Size: 359922 Color: 5
Size: 250392 Color: 17

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 389824 Color: 3
Size: 349140 Color: 13
Size: 261037 Color: 4

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 389841 Color: 3
Size: 320656 Color: 12
Size: 289504 Color: 15

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 389703 Color: 18
Size: 322110 Color: 5
Size: 288188 Color: 1

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 389707 Color: 0
Size: 329646 Color: 9
Size: 280648 Color: 12

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 389763 Color: 1
Size: 308628 Color: 7
Size: 301610 Color: 12

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 389849 Color: 6
Size: 335548 Color: 12
Size: 274604 Color: 4

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 389860 Color: 16
Size: 335502 Color: 7
Size: 274639 Color: 3

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 389891 Color: 16
Size: 338188 Color: 13
Size: 271922 Color: 9

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 389923 Color: 10
Size: 318048 Color: 6
Size: 292030 Color: 6

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 389939 Color: 11
Size: 313546 Color: 15
Size: 296516 Color: 8

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 389947 Color: 8
Size: 346558 Color: 7
Size: 263496 Color: 18

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 389984 Color: 10
Size: 306610 Color: 6
Size: 303407 Color: 15

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 390029 Color: 17
Size: 335460 Color: 1
Size: 274512 Color: 7

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 390049 Color: 8
Size: 333214 Color: 11
Size: 276738 Color: 9

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 390146 Color: 12
Size: 335052 Color: 17
Size: 274803 Color: 11

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 390183 Color: 1
Size: 323033 Color: 6
Size: 286785 Color: 15

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 390215 Color: 2
Size: 312671 Color: 13
Size: 297115 Color: 8

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 390244 Color: 13
Size: 326670 Color: 3
Size: 283087 Color: 15

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 390287 Color: 16
Size: 324501 Color: 8
Size: 285213 Color: 18

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 390362 Color: 8
Size: 343316 Color: 5
Size: 266323 Color: 15

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 390460 Color: 8
Size: 347740 Color: 19
Size: 261801 Color: 9

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 390475 Color: 14
Size: 347377 Color: 17
Size: 262149 Color: 12

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 390505 Color: 14
Size: 311378 Color: 8
Size: 298118 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 390525 Color: 13
Size: 319126 Color: 14
Size: 290350 Color: 3

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 390598 Color: 19
Size: 304881 Color: 6
Size: 304522 Color: 8

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 390606 Color: 19
Size: 345593 Color: 7
Size: 263802 Color: 14

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 390648 Color: 14
Size: 347367 Color: 4
Size: 261986 Color: 12

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 390720 Color: 16
Size: 314532 Color: 9
Size: 294749 Color: 5

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 390730 Color: 8
Size: 310745 Color: 9
Size: 298526 Color: 7

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 390752 Color: 8
Size: 344508 Color: 0
Size: 264741 Color: 3

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 390822 Color: 4
Size: 337540 Color: 13
Size: 271639 Color: 11

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 390916 Color: 6
Size: 342786 Color: 19
Size: 266299 Color: 8

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 390928 Color: 7
Size: 305725 Color: 15
Size: 303348 Color: 1

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 390964 Color: 7
Size: 358240 Color: 14
Size: 250797 Color: 8

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 390968 Color: 12
Size: 344915 Color: 1
Size: 264118 Color: 18

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 391038 Color: 13
Size: 323318 Color: 10
Size: 285645 Color: 11

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 391189 Color: 3
Size: 314360 Color: 12
Size: 294452 Color: 7

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 391083 Color: 0
Size: 330843 Color: 13
Size: 278075 Color: 14

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 391128 Color: 8
Size: 330548 Color: 9
Size: 278325 Color: 11

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 391203 Color: 15
Size: 311150 Color: 10
Size: 297648 Color: 1

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 391217 Color: 5
Size: 308850 Color: 16
Size: 299934 Color: 6

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 391237 Color: 17
Size: 353209 Color: 11
Size: 255555 Color: 6

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 391349 Color: 3
Size: 328023 Color: 6
Size: 280629 Color: 10

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 391285 Color: 18
Size: 317292 Color: 16
Size: 291424 Color: 9

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 391313 Color: 1
Size: 332095 Color: 7
Size: 276593 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 391368 Color: 17
Size: 315541 Color: 11
Size: 293092 Color: 13

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 391461 Color: 17
Size: 354660 Color: 3
Size: 253880 Color: 13

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 391485 Color: 16
Size: 326633 Color: 9
Size: 281883 Color: 13

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 391510 Color: 15
Size: 355441 Color: 8
Size: 253050 Color: 15

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 391521 Color: 6
Size: 312924 Color: 5
Size: 295556 Color: 9

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 391555 Color: 15
Size: 309404 Color: 6
Size: 299042 Color: 13

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 391682 Color: 13
Size: 348629 Color: 1
Size: 259690 Color: 12

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 391809 Color: 13
Size: 327771 Color: 12
Size: 280421 Color: 4

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 391849 Color: 10
Size: 317752 Color: 17
Size: 290400 Color: 13

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 391854 Color: 14
Size: 343264 Color: 15
Size: 264883 Color: 12

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 391863 Color: 6
Size: 313287 Color: 15
Size: 294851 Color: 11

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 392062 Color: 4
Size: 304233 Color: 0
Size: 303706 Color: 3

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 392074 Color: 17
Size: 347806 Color: 1
Size: 260121 Color: 1

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 392084 Color: 16
Size: 329122 Color: 15
Size: 278795 Color: 5

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 392094 Color: 1
Size: 334583 Color: 10
Size: 273324 Color: 12

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 392134 Color: 12
Size: 327288 Color: 7
Size: 280579 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 392155 Color: 2
Size: 336359 Color: 1
Size: 271487 Color: 17

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 392163 Color: 18
Size: 346986 Color: 1
Size: 260852 Color: 3

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 392180 Color: 2
Size: 339731 Color: 12
Size: 268090 Color: 7

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 392307 Color: 18
Size: 338593 Color: 18
Size: 269101 Color: 10

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 392337 Color: 11
Size: 316287 Color: 16
Size: 291377 Color: 10

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 392410 Color: 16
Size: 341758 Color: 4
Size: 265833 Color: 7

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 392424 Color: 10
Size: 339246 Color: 16
Size: 268331 Color: 7

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 392480 Color: 16
Size: 318143 Color: 16
Size: 289378 Color: 5

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 392499 Color: 17
Size: 303842 Color: 14
Size: 303660 Color: 12

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 392528 Color: 6
Size: 343396 Color: 6
Size: 264077 Color: 14

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 392561 Color: 4
Size: 316431 Color: 1
Size: 291009 Color: 9

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 392591 Color: 5
Size: 354172 Color: 10
Size: 253238 Color: 7

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 392620 Color: 17
Size: 332328 Color: 12
Size: 275053 Color: 3

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 392643 Color: 8
Size: 303769 Color: 17
Size: 303589 Color: 11

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 392672 Color: 11
Size: 305962 Color: 1
Size: 301367 Color: 6

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 392726 Color: 4
Size: 348535 Color: 9
Size: 258740 Color: 5

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 392752 Color: 1
Size: 332899 Color: 10
Size: 274350 Color: 16

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 392815 Color: 9
Size: 322627 Color: 7
Size: 284559 Color: 8

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 392815 Color: 5
Size: 346920 Color: 3
Size: 260266 Color: 4

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 392862 Color: 18
Size: 339558 Color: 14
Size: 267581 Color: 12

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 392869 Color: 17
Size: 329914 Color: 9
Size: 277218 Color: 13

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 392896 Color: 19
Size: 325407 Color: 0
Size: 281698 Color: 18

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 393068 Color: 13
Size: 311712 Color: 9
Size: 295221 Color: 2

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 393139 Color: 0
Size: 325500 Color: 12
Size: 281362 Color: 12

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 393205 Color: 5
Size: 336747 Color: 1
Size: 270049 Color: 18

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 393208 Color: 9
Size: 304030 Color: 17
Size: 302763 Color: 6

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 393218 Color: 10
Size: 347554 Color: 12
Size: 259229 Color: 19

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 393262 Color: 18
Size: 304486 Color: 7
Size: 302253 Color: 6

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 393302 Color: 10
Size: 344457 Color: 0
Size: 262242 Color: 5

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 393320 Color: 0
Size: 356585 Color: 18
Size: 250096 Color: 4

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 393357 Color: 18
Size: 313261 Color: 10
Size: 293383 Color: 9

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 393291 Color: 12
Size: 342985 Color: 10
Size: 263725 Color: 17

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 393399 Color: 19
Size: 319133 Color: 7
Size: 287469 Color: 8

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 393436 Color: 1
Size: 346008 Color: 9
Size: 260557 Color: 3

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 393453 Color: 8
Size: 344429 Color: 15
Size: 262119 Color: 11

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 393484 Color: 0
Size: 355928 Color: 19
Size: 250589 Color: 14

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 393573 Color: 9
Size: 339709 Color: 7
Size: 266719 Color: 17

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 393588 Color: 19
Size: 314690 Color: 16
Size: 291723 Color: 3

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 393914 Color: 17
Size: 345666 Color: 6
Size: 260421 Color: 9

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 393938 Color: 8
Size: 317038 Color: 4
Size: 289025 Color: 10

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 393989 Color: 18
Size: 348705 Color: 14
Size: 257307 Color: 16

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 394039 Color: 13
Size: 339622 Color: 12
Size: 266340 Color: 2

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 394146 Color: 14
Size: 350957 Color: 3
Size: 254898 Color: 16

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 394177 Color: 4
Size: 316710 Color: 16
Size: 289114 Color: 13

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 394275 Color: 9
Size: 324858 Color: 5
Size: 280868 Color: 7

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 394288 Color: 16
Size: 309141 Color: 15
Size: 296572 Color: 11

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 394340 Color: 8
Size: 332024 Color: 14
Size: 273637 Color: 6

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 394356 Color: 11
Size: 326711 Color: 1
Size: 278934 Color: 14

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 394427 Color: 19
Size: 311003 Color: 2
Size: 294571 Color: 7

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 394428 Color: 11
Size: 313134 Color: 3
Size: 292439 Color: 5

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 394437 Color: 12
Size: 306319 Color: 6
Size: 299245 Color: 3

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 394458 Color: 2
Size: 320719 Color: 17
Size: 284824 Color: 15

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 394507 Color: 15
Size: 352085 Color: 1
Size: 253409 Color: 17

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 394510 Color: 9
Size: 351829 Color: 6
Size: 253662 Color: 15

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 394470 Color: 17
Size: 314865 Color: 4
Size: 290666 Color: 9

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 394526 Color: 18
Size: 311703 Color: 15
Size: 293772 Color: 3

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 394539 Color: 1
Size: 324011 Color: 1
Size: 281451 Color: 5

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 394571 Color: 11
Size: 338456 Color: 0
Size: 266974 Color: 9

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 394650 Color: 6
Size: 336947 Color: 11
Size: 268404 Color: 13

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 394663 Color: 8
Size: 302776 Color: 19
Size: 302562 Color: 11

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 394665 Color: 6
Size: 317870 Color: 14
Size: 287466 Color: 19

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 394783 Color: 3
Size: 325055 Color: 0
Size: 280163 Color: 1

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 394761 Color: 9
Size: 309700 Color: 0
Size: 295540 Color: 14

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 394774 Color: 19
Size: 321615 Color: 11
Size: 283612 Color: 2

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 394786 Color: 4
Size: 309391 Color: 9
Size: 295824 Color: 1

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 394817 Color: 11
Size: 338833 Color: 7
Size: 266351 Color: 18

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 395006 Color: 11
Size: 344498 Color: 16
Size: 260497 Color: 9

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 395024 Color: 11
Size: 329851 Color: 16
Size: 275126 Color: 9

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 395056 Color: 12
Size: 304773 Color: 17
Size: 300172 Color: 18

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 395058 Color: 7
Size: 325864 Color: 8
Size: 279079 Color: 16

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 395110 Color: 5
Size: 347990 Color: 17
Size: 256901 Color: 11

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 395124 Color: 11
Size: 312653 Color: 4
Size: 292224 Color: 19

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 395136 Color: 12
Size: 314380 Color: 17
Size: 290485 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 395147 Color: 19
Size: 346692 Color: 13
Size: 258162 Color: 11

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 395357 Color: 3
Size: 330683 Color: 5
Size: 273961 Color: 14

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 395272 Color: 14
Size: 330177 Color: 2
Size: 274552 Color: 7

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 395416 Color: 9
Size: 316432 Color: 14
Size: 288153 Color: 10

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 395441 Color: 8
Size: 311694 Color: 7
Size: 292866 Color: 15

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 395549 Color: 9
Size: 309064 Color: 16
Size: 295388 Color: 7

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 395612 Color: 8
Size: 326671 Color: 14
Size: 277718 Color: 10

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 395669 Color: 18
Size: 350566 Color: 12
Size: 253766 Color: 14

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 395736 Color: 9
Size: 327642 Color: 4
Size: 276623 Color: 5

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 395863 Color: 8
Size: 324220 Color: 11
Size: 279918 Color: 9

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 395899 Color: 2
Size: 346651 Color: 2
Size: 257451 Color: 7

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 395951 Color: 6
Size: 319633 Color: 5
Size: 284417 Color: 9

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 395993 Color: 14
Size: 319566 Color: 17
Size: 284442 Color: 6

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 396037 Color: 16
Size: 315977 Color: 0
Size: 287987 Color: 13

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 396045 Color: 4
Size: 317957 Color: 0
Size: 285999 Color: 15

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 396064 Color: 6
Size: 335192 Color: 13
Size: 268745 Color: 19

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 396090 Color: 17
Size: 319206 Color: 5
Size: 284705 Color: 3

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 396100 Color: 19
Size: 310477 Color: 8
Size: 293424 Color: 16

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 396122 Color: 11
Size: 341482 Color: 4
Size: 262397 Color: 19

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 396145 Color: 11
Size: 311856 Color: 16
Size: 292000 Color: 10

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 396217 Color: 19
Size: 304268 Color: 15
Size: 299516 Color: 17

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 396224 Color: 6
Size: 306760 Color: 17
Size: 297017 Color: 18

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 396286 Color: 10
Size: 343636 Color: 3
Size: 260079 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 396475 Color: 2
Size: 350915 Color: 4
Size: 252611 Color: 9

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 396487 Color: 2
Size: 336236 Color: 0
Size: 267278 Color: 14

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 396498 Color: 14
Size: 313844 Color: 1
Size: 289659 Color: 9

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 396508 Color: 10
Size: 322309 Color: 18
Size: 281184 Color: 5

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 396537 Color: 2
Size: 332229 Color: 17
Size: 271235 Color: 5

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 396592 Color: 19
Size: 341118 Color: 2
Size: 262291 Color: 7

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 396847 Color: 3
Size: 346985 Color: 16
Size: 256169 Color: 14

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 396683 Color: 5
Size: 330866 Color: 6
Size: 272452 Color: 12

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 396738 Color: 5
Size: 349036 Color: 2
Size: 254227 Color: 15

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 396759 Color: 12
Size: 350465 Color: 15
Size: 252777 Color: 9

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 396821 Color: 10
Size: 350312 Color: 3
Size: 252868 Color: 5

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 396844 Color: 10
Size: 346341 Color: 8
Size: 256816 Color: 6

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 396874 Color: 9
Size: 309556 Color: 14
Size: 293571 Color: 5

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 396880 Color: 16
Size: 309325 Color: 0
Size: 293796 Color: 17

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 396901 Color: 14
Size: 306823 Color: 19
Size: 296277 Color: 15

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 396945 Color: 10
Size: 348105 Color: 13
Size: 254951 Color: 6

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 396965 Color: 8
Size: 334215 Color: 3
Size: 268821 Color: 10

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 397017 Color: 18
Size: 332347 Color: 8
Size: 270637 Color: 15

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 397064 Color: 13
Size: 310001 Color: 4
Size: 292936 Color: 6

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 397081 Color: 15
Size: 301709 Color: 0
Size: 301211 Color: 19

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 397082 Color: 5
Size: 303135 Color: 4
Size: 299784 Color: 10

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 397089 Color: 19
Size: 314579 Color: 9
Size: 288333 Color: 13

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 397138 Color: 12
Size: 351164 Color: 4
Size: 251699 Color: 3

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 397148 Color: 18
Size: 345374 Color: 19
Size: 257479 Color: 10

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 397240 Color: 12
Size: 349618 Color: 1
Size: 253143 Color: 15

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 397330 Color: 7
Size: 325490 Color: 14
Size: 277181 Color: 19

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 397338 Color: 19
Size: 336046 Color: 9
Size: 266617 Color: 18

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 397371 Color: 12
Size: 314322 Color: 7
Size: 288308 Color: 13

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 397467 Color: 6
Size: 344350 Color: 1
Size: 258184 Color: 9

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 397585 Color: 4
Size: 332362 Color: 3
Size: 270054 Color: 1

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 397685 Color: 1
Size: 324648 Color: 12
Size: 277668 Color: 17

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 397724 Color: 13
Size: 330062 Color: 13
Size: 272215 Color: 18

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 397725 Color: 11
Size: 316019 Color: 7
Size: 286257 Color: 1

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 397791 Color: 14
Size: 305234 Color: 18
Size: 296976 Color: 1

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 397814 Color: 16
Size: 306194 Color: 5
Size: 295993 Color: 3

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 397822 Color: 16
Size: 313784 Color: 5
Size: 288395 Color: 18

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 397843 Color: 15
Size: 310521 Color: 2
Size: 291637 Color: 7

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 397981 Color: 4
Size: 309075 Color: 17
Size: 292945 Color: 12

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 398032 Color: 14
Size: 335308 Color: 1
Size: 266661 Color: 17

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 398097 Color: 3
Size: 321728 Color: 8
Size: 280176 Color: 16

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 398035 Color: 2
Size: 317575 Color: 9
Size: 284391 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 398179 Color: 15
Size: 348795 Color: 4
Size: 253027 Color: 4

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 398189 Color: 4
Size: 305340 Color: 1
Size: 296472 Color: 19

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 398261 Color: 15
Size: 313348 Color: 15
Size: 288392 Color: 9

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 398366 Color: 12
Size: 338374 Color: 3
Size: 263261 Color: 5

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 398372 Color: 17
Size: 348437 Color: 14
Size: 253192 Color: 10

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 398382 Color: 10
Size: 301898 Color: 16
Size: 299721 Color: 4

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 398454 Color: 17
Size: 325752 Color: 5
Size: 275795 Color: 15

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 398477 Color: 13
Size: 331553 Color: 12
Size: 269971 Color: 16

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 398508 Color: 7
Size: 301528 Color: 1
Size: 299965 Color: 17

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 398714 Color: 3
Size: 317436 Color: 9
Size: 283851 Color: 14

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 398600 Color: 1
Size: 308957 Color: 13
Size: 292444 Color: 4

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 398666 Color: 0
Size: 305801 Color: 18
Size: 295534 Color: 9

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 398697 Color: 1
Size: 339829 Color: 10
Size: 261475 Color: 5

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 398706 Color: 16
Size: 335110 Color: 11
Size: 266185 Color: 11

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 398785 Color: 0
Size: 339180 Color: 13
Size: 262036 Color: 1

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 398804 Color: 13
Size: 327011 Color: 14
Size: 274186 Color: 12

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 398927 Color: 11
Size: 326925 Color: 18
Size: 274149 Color: 17

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 398960 Color: 5
Size: 330968 Color: 7
Size: 270073 Color: 16

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 399042 Color: 14
Size: 338468 Color: 5
Size: 262491 Color: 10

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 399095 Color: 5
Size: 344013 Color: 18
Size: 256893 Color: 3

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 399111 Color: 1
Size: 318404 Color: 15
Size: 282486 Color: 9

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 399134 Color: 13
Size: 333058 Color: 18
Size: 267809 Color: 5

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 399138 Color: 19
Size: 327510 Color: 4
Size: 273353 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 399194 Color: 16
Size: 314581 Color: 12
Size: 286226 Color: 7

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 399222 Color: 13
Size: 303048 Color: 19
Size: 297731 Color: 7

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 399271 Color: 9
Size: 324850 Color: 8
Size: 275880 Color: 5

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 399286 Color: 7
Size: 324292 Color: 15
Size: 276423 Color: 3

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 399303 Color: 10
Size: 343603 Color: 5
Size: 257095 Color: 17

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 399311 Color: 13
Size: 332189 Color: 14
Size: 268501 Color: 10

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 399322 Color: 19
Size: 338905 Color: 9
Size: 261774 Color: 6

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 399389 Color: 11
Size: 329783 Color: 19
Size: 270829 Color: 19

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 399440 Color: 11
Size: 335411 Color: 1
Size: 265150 Color: 9

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 399480 Color: 16
Size: 322904 Color: 17
Size: 277617 Color: 11

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 399481 Color: 6
Size: 341132 Color: 7
Size: 259388 Color: 10

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 399531 Color: 13
Size: 314366 Color: 12
Size: 286104 Color: 18

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 399965 Color: 3
Size: 309290 Color: 13
Size: 290746 Color: 16

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 399640 Color: 8
Size: 318934 Color: 10
Size: 281427 Color: 5

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 399683 Color: 1
Size: 330451 Color: 11
Size: 269867 Color: 19

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 399778 Color: 7
Size: 347338 Color: 11
Size: 252885 Color: 15

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 399780 Color: 9
Size: 300522 Color: 15
Size: 299699 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 399787 Color: 16
Size: 333174 Color: 11
Size: 267040 Color: 19

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 400092 Color: 3
Size: 309459 Color: 10
Size: 290450 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 399803 Color: 6
Size: 325140 Color: 11
Size: 275058 Color: 6

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 399832 Color: 0
Size: 300843 Color: 7
Size: 299326 Color: 1

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 400019 Color: 13
Size: 313783 Color: 4
Size: 286199 Color: 11

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 400109 Color: 4
Size: 311235 Color: 5
Size: 288657 Color: 2

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 400127 Color: 0
Size: 302167 Color: 14
Size: 297707 Color: 10

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 400178 Color: 6
Size: 309926 Color: 16
Size: 289897 Color: 16

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 400390 Color: 18
Size: 322741 Color: 17
Size: 276870 Color: 3

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 400408 Color: 2
Size: 305098 Color: 18
Size: 294495 Color: 15

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 400443 Color: 16
Size: 306344 Color: 0
Size: 293214 Color: 17

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 400488 Color: 6
Size: 318308 Color: 18
Size: 281205 Color: 12

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 400535 Color: 13
Size: 323036 Color: 5
Size: 276430 Color: 12

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 400563 Color: 16
Size: 303277 Color: 3
Size: 296161 Color: 5

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 400577 Color: 12
Size: 345244 Color: 4
Size: 254180 Color: 16

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 400660 Color: 9
Size: 329499 Color: 7
Size: 269842 Color: 11

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 400662 Color: 4
Size: 342778 Color: 7
Size: 256561 Color: 5

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 400670 Color: 1
Size: 312145 Color: 14
Size: 287186 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 400676 Color: 14
Size: 314476 Color: 12
Size: 284849 Color: 18

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 400705 Color: 6
Size: 321603 Color: 14
Size: 277693 Color: 19

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 400850 Color: 16
Size: 343531 Color: 6
Size: 255620 Color: 3

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 400905 Color: 14
Size: 339148 Color: 15
Size: 259948 Color: 13

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 401047 Color: 14
Size: 318203 Color: 8
Size: 280751 Color: 14

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 401087 Color: 7
Size: 301870 Color: 12
Size: 297044 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 401093 Color: 10
Size: 330870 Color: 3
Size: 268038 Color: 18

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 401260 Color: 16
Size: 320577 Color: 15
Size: 278164 Color: 4

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 401364 Color: 14
Size: 311423 Color: 9
Size: 287214 Color: 10

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 401384 Color: 1
Size: 306958 Color: 12
Size: 291659 Color: 14

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 401390 Color: 0
Size: 342757 Color: 8
Size: 255854 Color: 15

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 401392 Color: 1
Size: 319986 Color: 5
Size: 278623 Color: 14

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 401412 Color: 14
Size: 304744 Color: 6
Size: 293845 Color: 18

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 401438 Color: 0
Size: 336106 Color: 7
Size: 262457 Color: 2

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 401569 Color: 16
Size: 329893 Color: 8
Size: 268539 Color: 9

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 401616 Color: 5
Size: 316342 Color: 12
Size: 282043 Color: 5

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 401638 Color: 11
Size: 302208 Color: 18
Size: 296155 Color: 1

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 401747 Color: 11
Size: 306626 Color: 4
Size: 291628 Color: 3

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 401755 Color: 0
Size: 302751 Color: 17
Size: 295495 Color: 10

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 401799 Color: 18
Size: 322234 Color: 4
Size: 275968 Color: 9

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 401816 Color: 0
Size: 328995 Color: 19
Size: 269190 Color: 1

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 401898 Color: 16
Size: 311052 Color: 1
Size: 287051 Color: 19

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 401915 Color: 16
Size: 332819 Color: 8
Size: 265267 Color: 15

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 402033 Color: 14
Size: 302753 Color: 3
Size: 295215 Color: 13

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 402078 Color: 14
Size: 314035 Color: 13
Size: 283888 Color: 5

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 402082 Color: 19
Size: 338772 Color: 13
Size: 259147 Color: 17

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 402082 Color: 5
Size: 299437 Color: 7
Size: 298482 Color: 5

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 402141 Color: 8
Size: 335774 Color: 12
Size: 262086 Color: 2

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 402305 Color: 19
Size: 326910 Color: 7
Size: 270786 Color: 18

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 402347 Color: 2
Size: 329047 Color: 7
Size: 268607 Color: 1

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 402378 Color: 2
Size: 299196 Color: 18
Size: 298427 Color: 4

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 402510 Color: 7
Size: 335426 Color: 13
Size: 262065 Color: 11

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 402566 Color: 3
Size: 307175 Color: 14
Size: 290260 Color: 3

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 402520 Color: 4
Size: 330397 Color: 17
Size: 267084 Color: 10

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 402573 Color: 11
Size: 300776 Color: 8
Size: 296652 Color: 9

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 402695 Color: 5
Size: 305883 Color: 6
Size: 291423 Color: 11

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 402758 Color: 7
Size: 316943 Color: 8
Size: 280300 Color: 2

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 402806 Color: 8
Size: 304148 Color: 1
Size: 293047 Color: 16

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 402856 Color: 7
Size: 335987 Color: 4
Size: 261158 Color: 3

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 402912 Color: 6
Size: 308062 Color: 5
Size: 289027 Color: 11

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 402966 Color: 15
Size: 302418 Color: 16
Size: 294617 Color: 0

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 402966 Color: 4
Size: 342600 Color: 11
Size: 254435 Color: 9

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 403010 Color: 8
Size: 336272 Color: 4
Size: 260719 Color: 16

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 403136 Color: 12
Size: 327121 Color: 3
Size: 269744 Color: 2

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 403161 Color: 2
Size: 321283 Color: 18
Size: 275557 Color: 15

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 403183 Color: 11
Size: 305570 Color: 2
Size: 291248 Color: 14

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 403251 Color: 18
Size: 316677 Color: 6
Size: 280073 Color: 15

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 403315 Color: 5
Size: 331043 Color: 11
Size: 265643 Color: 17

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 403362 Color: 10
Size: 305813 Color: 15
Size: 290826 Color: 3

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 403434 Color: 14
Size: 345170 Color: 0
Size: 251397 Color: 9

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 403437 Color: 0
Size: 312053 Color: 14
Size: 284511 Color: 18

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 403469 Color: 19
Size: 310048 Color: 13
Size: 286484 Color: 11

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 403514 Color: 14
Size: 346173 Color: 8
Size: 250314 Color: 9

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 403528 Color: 7
Size: 331446 Color: 6
Size: 265027 Color: 2

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 403514 Color: 8
Size: 311231 Color: 15
Size: 285256 Color: 19

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 403582 Color: 5
Size: 319666 Color: 7
Size: 276753 Color: 3

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 403641 Color: 19
Size: 312047 Color: 9
Size: 284313 Color: 18

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 403658 Color: 9
Size: 326045 Color: 15
Size: 270298 Color: 6

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 403697 Color: 5
Size: 308547 Color: 1
Size: 287757 Color: 18

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 403735 Color: 2
Size: 309823 Color: 9
Size: 286443 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 403811 Color: 4
Size: 301515 Color: 10
Size: 294675 Color: 11

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 403811 Color: 8
Size: 308936 Color: 14
Size: 287254 Color: 14

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 403822 Color: 4
Size: 304819 Color: 6
Size: 291360 Color: 13

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 403830 Color: 4
Size: 327401 Color: 2
Size: 268770 Color: 12

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 403834 Color: 0
Size: 326280 Color: 18
Size: 269887 Color: 9

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 403958 Color: 14
Size: 329862 Color: 8
Size: 266181 Color: 17

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 403838 Color: 5
Size: 333525 Color: 18
Size: 262638 Color: 18

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 403888 Color: 9
Size: 315252 Color: 17
Size: 280861 Color: 12

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 403904 Color: 15
Size: 319722 Color: 7
Size: 276375 Color: 17

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 403935 Color: 12
Size: 334727 Color: 7
Size: 261339 Color: 8

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 404056 Color: 2
Size: 325057 Color: 9
Size: 270888 Color: 14

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 404084 Color: 19
Size: 331034 Color: 3
Size: 264883 Color: 11

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 404319 Color: 11
Size: 307364 Color: 19
Size: 288318 Color: 15

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 404122 Color: 13
Size: 339383 Color: 16
Size: 256496 Color: 16

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 404129 Color: 6
Size: 298202 Color: 18
Size: 297670 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 404293 Color: 7
Size: 331304 Color: 12
Size: 264404 Color: 13

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 404326 Color: 2
Size: 304142 Color: 10
Size: 291533 Color: 14

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 404387 Color: 15
Size: 328876 Color: 1
Size: 266738 Color: 9

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 404432 Color: 9
Size: 312342 Color: 4
Size: 283227 Color: 7

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 404624 Color: 7
Size: 313961 Color: 4
Size: 281416 Color: 4

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 404766 Color: 5
Size: 339431 Color: 1
Size: 255804 Color: 12

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 404831 Color: 7
Size: 305847 Color: 14
Size: 289323 Color: 17

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 404852 Color: 3
Size: 340820 Color: 12
Size: 254329 Color: 1

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 404980 Color: 7
Size: 303257 Color: 16
Size: 291764 Color: 11

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 404880 Color: 16
Size: 327978 Color: 19
Size: 267143 Color: 4

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 404886 Color: 19
Size: 315713 Color: 8
Size: 279402 Color: 2

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 404893 Color: 5
Size: 341936 Color: 4
Size: 253172 Color: 3

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 405001 Color: 4
Size: 327544 Color: 13
Size: 267456 Color: 15

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 405035 Color: 17
Size: 308942 Color: 17
Size: 286024 Color: 5

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 405083 Color: 12
Size: 315813 Color: 13
Size: 279105 Color: 14

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 405192 Color: 15
Size: 318582 Color: 15
Size: 276227 Color: 6

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 405211 Color: 4
Size: 339778 Color: 12
Size: 255012 Color: 5

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 405288 Color: 16
Size: 318225 Color: 17
Size: 276488 Color: 15

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 405303 Color: 17
Size: 336915 Color: 4
Size: 257783 Color: 3

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 405443 Color: 13
Size: 314424 Color: 16
Size: 280134 Color: 15

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 405508 Color: 8
Size: 341682 Color: 19
Size: 252811 Color: 15

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 405558 Color: 5
Size: 316580 Color: 8
Size: 277863 Color: 6

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 405629 Color: 14
Size: 307570 Color: 17
Size: 286802 Color: 0

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 405598 Color: 17
Size: 329311 Color: 11
Size: 265092 Color: 18

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 405600 Color: 15
Size: 309806 Color: 19
Size: 284595 Color: 10

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 405656 Color: 19
Size: 344298 Color: 19
Size: 250047 Color: 17

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 405788 Color: 16
Size: 326419 Color: 2
Size: 267794 Color: 14

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 405966 Color: 11
Size: 331106 Color: 1
Size: 262929 Color: 5

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 406110 Color: 18
Size: 316595 Color: 12
Size: 277296 Color: 13

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 406123 Color: 18
Size: 341908 Color: 17
Size: 251970 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 406142 Color: 1
Size: 317952 Color: 3
Size: 275907 Color: 19

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 406150 Color: 15
Size: 311765 Color: 12
Size: 282086 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 406214 Color: 16
Size: 308888 Color: 18
Size: 284899 Color: 14

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 406294 Color: 8
Size: 331633 Color: 4
Size: 262074 Color: 15

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 406313 Color: 15
Size: 325968 Color: 1
Size: 267720 Color: 2

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 406319 Color: 4
Size: 297898 Color: 5
Size: 295784 Color: 17

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 406344 Color: 16
Size: 334596 Color: 18
Size: 259061 Color: 8

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 406529 Color: 2
Size: 316607 Color: 5
Size: 276865 Color: 14

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 406556 Color: 15
Size: 317561 Color: 16
Size: 275884 Color: 17

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 406595 Color: 6
Size: 334824 Color: 3
Size: 258582 Color: 19

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 406599 Color: 16
Size: 337736 Color: 13
Size: 255666 Color: 8

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 406691 Color: 16
Size: 336387 Color: 2
Size: 256923 Color: 2

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 406807 Color: 19
Size: 315993 Color: 18
Size: 277201 Color: 16

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 406805 Color: 14
Size: 337168 Color: 19
Size: 256028 Color: 19

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 406807 Color: 17
Size: 300526 Color: 10
Size: 292668 Color: 1

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 406817 Color: 4
Size: 331636 Color: 18
Size: 261548 Color: 15

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 406818 Color: 10
Size: 336858 Color: 0
Size: 256325 Color: 12

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 406983 Color: 18
Size: 325190 Color: 13
Size: 267828 Color: 17

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 407015 Color: 2
Size: 340167 Color: 1
Size: 252819 Color: 14

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 407043 Color: 4
Size: 333881 Color: 6
Size: 259077 Color: 17

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 407064 Color: 1
Size: 299858 Color: 9
Size: 293079 Color: 11

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 407120 Color: 0
Size: 325228 Color: 5
Size: 267653 Color: 2

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 407130 Color: 19
Size: 321409 Color: 12
Size: 271462 Color: 18

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 407352 Color: 15
Size: 335467 Color: 19
Size: 257182 Color: 2

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 407355 Color: 18
Size: 317462 Color: 3
Size: 275184 Color: 3

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 407390 Color: 12
Size: 297487 Color: 0
Size: 295124 Color: 11

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 407405 Color: 2
Size: 326581 Color: 5
Size: 266015 Color: 18

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 407472 Color: 9
Size: 339424 Color: 1
Size: 253105 Color: 12

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 407765 Color: 14
Size: 321770 Color: 5
Size: 270466 Color: 4

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 407500 Color: 1
Size: 314757 Color: 15
Size: 277744 Color: 13

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 407520 Color: 10
Size: 334074 Color: 11
Size: 258407 Color: 17

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 407544 Color: 4
Size: 331783 Color: 2
Size: 260674 Color: 15

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 407830 Color: 14
Size: 319563 Color: 19
Size: 272608 Color: 16

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 407761 Color: 3
Size: 310114 Color: 6
Size: 282126 Color: 18

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 407797 Color: 6
Size: 316711 Color: 15
Size: 275493 Color: 11

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 407852 Color: 17
Size: 297578 Color: 1
Size: 294571 Color: 7

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 407884 Color: 16
Size: 327735 Color: 14
Size: 264382 Color: 5

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 407893 Color: 1
Size: 316034 Color: 12
Size: 276074 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 407909 Color: 9
Size: 327062 Color: 9
Size: 265030 Color: 13

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 407930 Color: 8
Size: 301038 Color: 13
Size: 291033 Color: 5

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 407973 Color: 1
Size: 301292 Color: 5
Size: 290736 Color: 4

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 408049 Color: 15
Size: 340831 Color: 11
Size: 251121 Color: 12

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 408145 Color: 0
Size: 311050 Color: 16
Size: 280806 Color: 6

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 408146 Color: 12
Size: 330291 Color: 14
Size: 261564 Color: 3

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 408184 Color: 5
Size: 313037 Color: 0
Size: 278780 Color: 8

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 408209 Color: 10
Size: 321845 Color: 0
Size: 269947 Color: 19

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 408272 Color: 19
Size: 335745 Color: 9
Size: 255984 Color: 13

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 408279 Color: 16
Size: 328775 Color: 0
Size: 262947 Color: 3

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 408298 Color: 4
Size: 323627 Color: 7
Size: 268076 Color: 14

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 408348 Color: 18
Size: 302849 Color: 19
Size: 288804 Color: 19

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 408368 Color: 4
Size: 302849 Color: 11
Size: 288784 Color: 1

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 408396 Color: 2
Size: 316755 Color: 3
Size: 274850 Color: 10

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 408435 Color: 3
Size: 308487 Color: 11
Size: 283079 Color: 15

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 408473 Color: 11
Size: 300559 Color: 13
Size: 290969 Color: 14

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 408475 Color: 2
Size: 296052 Color: 3
Size: 295474 Color: 15

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 408546 Color: 0
Size: 309956 Color: 10
Size: 281499 Color: 13

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 408571 Color: 10
Size: 307734 Color: 17
Size: 283696 Color: 19

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 408640 Color: 17
Size: 311335 Color: 11
Size: 280026 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 408645 Color: 6
Size: 301120 Color: 12
Size: 290236 Color: 14

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 408688 Color: 15
Size: 310062 Color: 8
Size: 281251 Color: 1

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 408849 Color: 6
Size: 310973 Color: 11
Size: 280179 Color: 9

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 408875 Color: 0
Size: 333099 Color: 2
Size: 258027 Color: 6

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 408951 Color: 5
Size: 315346 Color: 10
Size: 275704 Color: 18

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 408969 Color: 15
Size: 310147 Color: 4
Size: 280885 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 408976 Color: 16
Size: 338271 Color: 3
Size: 252754 Color: 12

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 408980 Color: 3
Size: 336074 Color: 16
Size: 254947 Color: 8

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 408987 Color: 8
Size: 337012 Color: 19
Size: 254002 Color: 12

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 409004 Color: 10
Size: 323789 Color: 10
Size: 267208 Color: 14

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 409006 Color: 11
Size: 310038 Color: 9
Size: 280957 Color: 4

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 409166 Color: 4
Size: 327775 Color: 13
Size: 263060 Color: 1

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 409104 Color: 10
Size: 331380 Color: 13
Size: 259517 Color: 1

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 409112 Color: 1
Size: 308699 Color: 13
Size: 282190 Color: 5

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 409122 Color: 1
Size: 324310 Color: 18
Size: 266569 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 409141 Color: 8
Size: 296370 Color: 6
Size: 294490 Color: 7

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 409141 Color: 13
Size: 302902 Color: 4
Size: 287958 Color: 7

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 409272 Color: 5
Size: 334409 Color: 13
Size: 256320 Color: 7

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 409302 Color: 10
Size: 338223 Color: 9
Size: 252476 Color: 18

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 409405 Color: 2
Size: 309421 Color: 7
Size: 281175 Color: 3

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 409412 Color: 17
Size: 328395 Color: 9
Size: 262194 Color: 3

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 409432 Color: 12
Size: 314972 Color: 17
Size: 275597 Color: 16

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 409452 Color: 0
Size: 337732 Color: 6
Size: 252817 Color: 4

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 409459 Color: 11
Size: 336938 Color: 6
Size: 253604 Color: 7

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 409552 Color: 0
Size: 338979 Color: 12
Size: 251470 Color: 18

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 409606 Color: 12
Size: 301359 Color: 1
Size: 289036 Color: 5

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 409637 Color: 7
Size: 333622 Color: 0
Size: 256742 Color: 8

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 409647 Color: 16
Size: 310022 Color: 5
Size: 280332 Color: 18

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 409660 Color: 8
Size: 324705 Color: 2
Size: 265636 Color: 4

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 409687 Color: 1
Size: 323454 Color: 11
Size: 266860 Color: 11

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 409692 Color: 2
Size: 320188 Color: 19
Size: 270121 Color: 3

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 409718 Color: 3
Size: 310302 Color: 9
Size: 279981 Color: 19

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 409721 Color: 6
Size: 336119 Color: 12
Size: 254161 Color: 13

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 409728 Color: 15
Size: 298499 Color: 16
Size: 291774 Color: 17

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 409771 Color: 19
Size: 331834 Color: 4
Size: 258396 Color: 12

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 409783 Color: 5
Size: 323397 Color: 19
Size: 266821 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 409908 Color: 1
Size: 339580 Color: 17
Size: 250513 Color: 7

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 409923 Color: 11
Size: 336342 Color: 0
Size: 253736 Color: 11

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 409936 Color: 18
Size: 330606 Color: 5
Size: 259459 Color: 1

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 409954 Color: 1
Size: 330759 Color: 2
Size: 259288 Color: 15

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 409958 Color: 12
Size: 312590 Color: 13
Size: 277453 Color: 13

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 409961 Color: 6
Size: 330548 Color: 16
Size: 259492 Color: 4

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 409962 Color: 8
Size: 318941 Color: 10
Size: 271098 Color: 15

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 410033 Color: 16
Size: 339605 Color: 6
Size: 250363 Color: 1

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 410058 Color: 18
Size: 304358 Color: 9
Size: 285585 Color: 8

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 410087 Color: 0
Size: 325734 Color: 17
Size: 264180 Color: 3

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 410112 Color: 7
Size: 318999 Color: 7
Size: 270890 Color: 18

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 410215 Color: 16
Size: 321016 Color: 10
Size: 268770 Color: 19

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 410242 Color: 12
Size: 299175 Color: 7
Size: 290584 Color: 11

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 410261 Color: 1
Size: 302505 Color: 2
Size: 287235 Color: 17

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 410262 Color: 15
Size: 324776 Color: 19
Size: 264963 Color: 16

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 410373 Color: 15
Size: 319334 Color: 17
Size: 270294 Color: 15

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 410441 Color: 11
Size: 309212 Color: 10
Size: 280348 Color: 6

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 410446 Color: 10
Size: 306165 Color: 1
Size: 283390 Color: 18

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 410636 Color: 4
Size: 337152 Color: 11
Size: 252213 Color: 12

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 410613 Color: 5
Size: 297428 Color: 9
Size: 291960 Color: 15

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 410635 Color: 16
Size: 319901 Color: 7
Size: 269465 Color: 1

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 410643 Color: 18
Size: 298812 Color: 17
Size: 290546 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 410668 Color: 10
Size: 324774 Color: 6
Size: 264559 Color: 4

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 410722 Color: 6
Size: 338779 Color: 14
Size: 250500 Color: 11

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 410777 Color: 5
Size: 317412 Color: 13
Size: 271812 Color: 16

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 410792 Color: 17
Size: 312903 Color: 0
Size: 276306 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 410888 Color: 2
Size: 295577 Color: 3
Size: 293536 Color: 9

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 410914 Color: 0
Size: 295778 Color: 6
Size: 293309 Color: 18

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 410940 Color: 2
Size: 313541 Color: 18
Size: 275520 Color: 9

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 411062 Color: 4
Size: 317926 Color: 9
Size: 271013 Color: 2

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 411038 Color: 3
Size: 303285 Color: 13
Size: 285678 Color: 16

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 411061 Color: 16
Size: 327274 Color: 6
Size: 261666 Color: 9

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 411126 Color: 5
Size: 335940 Color: 11
Size: 252935 Color: 11

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 411172 Color: 18
Size: 335782 Color: 4
Size: 253047 Color: 16

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 411190 Color: 10
Size: 330958 Color: 0
Size: 257853 Color: 13

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 411193 Color: 14
Size: 298222 Color: 12
Size: 290586 Color: 16

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 411230 Color: 16
Size: 316292 Color: 12
Size: 272479 Color: 12

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 411304 Color: 8
Size: 338622 Color: 18
Size: 250075 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 411322 Color: 16
Size: 327679 Color: 1
Size: 261000 Color: 7

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 411333 Color: 2
Size: 312222 Color: 10
Size: 276446 Color: 11

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 411343 Color: 15
Size: 328055 Color: 4
Size: 260603 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 411348 Color: 1
Size: 313572 Color: 19
Size: 275081 Color: 13

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 411462 Color: 13
Size: 323914 Color: 7
Size: 264625 Color: 11

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 411495 Color: 8
Size: 326826 Color: 5
Size: 261680 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 411512 Color: 12
Size: 319561 Color: 18
Size: 268928 Color: 9

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 411566 Color: 2
Size: 317085 Color: 4
Size: 271350 Color: 3

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 411570 Color: 17
Size: 337606 Color: 18
Size: 250825 Color: 10

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 411671 Color: 14
Size: 313299 Color: 0
Size: 275031 Color: 5

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 411733 Color: 15
Size: 327839 Color: 10
Size: 260429 Color: 8

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 411762 Color: 8
Size: 315834 Color: 16
Size: 272405 Color: 3

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 411808 Color: 0
Size: 321614 Color: 18
Size: 266579 Color: 9

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 411894 Color: 8
Size: 305929 Color: 0
Size: 282178 Color: 6

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 411899 Color: 7
Size: 301294 Color: 0
Size: 286808 Color: 8

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 411975 Color: 7
Size: 335641 Color: 12
Size: 252385 Color: 7

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 411989 Color: 2
Size: 319847 Color: 3
Size: 268165 Color: 17

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 412047 Color: 11
Size: 336995 Color: 18
Size: 250959 Color: 4

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 412148 Color: 0
Size: 305478 Color: 8
Size: 282375 Color: 12

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 412153 Color: 8
Size: 314909 Color: 6
Size: 272939 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 412214 Color: 10
Size: 329288 Color: 13
Size: 258499 Color: 16

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 412230 Color: 7
Size: 317507 Color: 17
Size: 270264 Color: 1

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 412272 Color: 6
Size: 297728 Color: 8
Size: 290001 Color: 16

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 412396 Color: 7
Size: 294071 Color: 18
Size: 293534 Color: 6

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 412404 Color: 5
Size: 332115 Color: 6
Size: 255482 Color: 4

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 412405 Color: 16
Size: 311129 Color: 18
Size: 276467 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 412419 Color: 9
Size: 330956 Color: 19
Size: 256626 Color: 14

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 412443 Color: 15
Size: 295958 Color: 14
Size: 291600 Color: 16

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 412451 Color: 16
Size: 313223 Color: 7
Size: 274327 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 412453 Color: 6
Size: 333728 Color: 8
Size: 253820 Color: 9

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 412474 Color: 7
Size: 296113 Color: 4
Size: 291414 Color: 10

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 412526 Color: 3
Size: 306863 Color: 18
Size: 280612 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 412548 Color: 12
Size: 311794 Color: 11
Size: 275659 Color: 5

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 412562 Color: 1
Size: 324687 Color: 12
Size: 262752 Color: 16

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 412584 Color: 7
Size: 330696 Color: 16
Size: 256721 Color: 6

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 412677 Color: 4
Size: 328148 Color: 19
Size: 259176 Color: 3

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 412698 Color: 15
Size: 329131 Color: 19
Size: 258172 Color: 10

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 412708 Color: 13
Size: 332501 Color: 6
Size: 254792 Color: 1

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 412736 Color: 14
Size: 332887 Color: 0
Size: 254378 Color: 18

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 412749 Color: 10
Size: 331530 Color: 6
Size: 255722 Color: 18

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 412751 Color: 10
Size: 323891 Color: 18
Size: 263359 Color: 5

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 412760 Color: 16
Size: 332055 Color: 0
Size: 255186 Color: 8

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 412762 Color: 11
Size: 314672 Color: 13
Size: 272567 Color: 7

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 412763 Color: 17
Size: 330933 Color: 5
Size: 256305 Color: 6

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 412959 Color: 4
Size: 317891 Color: 18
Size: 269151 Color: 10

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 412790 Color: 9
Size: 325681 Color: 1
Size: 261530 Color: 14

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 412790 Color: 14
Size: 297432 Color: 1
Size: 289779 Color: 9

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 413010 Color: 19
Size: 324610 Color: 11
Size: 262381 Color: 15

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 413087 Color: 1
Size: 320012 Color: 7
Size: 266902 Color: 4

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 413098 Color: 1
Size: 299811 Color: 2
Size: 287092 Color: 17

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 413220 Color: 8
Size: 327035 Color: 12
Size: 259746 Color: 5

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 413231 Color: 19
Size: 307245 Color: 16
Size: 279525 Color: 6

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 413279 Color: 3
Size: 294784 Color: 8
Size: 291938 Color: 11

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 413351 Color: 2
Size: 295053 Color: 18
Size: 291597 Color: 17

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 413376 Color: 16
Size: 293592 Color: 9
Size: 293033 Color: 12

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 413504 Color: 4
Size: 314778 Color: 14
Size: 271719 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 413378 Color: 7
Size: 308580 Color: 10
Size: 278043 Color: 9

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 413512 Color: 0
Size: 303153 Color: 14
Size: 283336 Color: 8

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 413554 Color: 3
Size: 311411 Color: 15
Size: 275036 Color: 14

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 413608 Color: 8
Size: 335870 Color: 13
Size: 250523 Color: 17

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 413663 Color: 3
Size: 315779 Color: 10
Size: 270559 Color: 8

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 413789 Color: 13
Size: 314520 Color: 18
Size: 271692 Color: 18

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 413914 Color: 5
Size: 326474 Color: 3
Size: 259613 Color: 3

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 413959 Color: 4
Size: 311771 Color: 16
Size: 274271 Color: 2

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 413922 Color: 9
Size: 298180 Color: 12
Size: 287899 Color: 3

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 414065 Color: 6
Size: 296545 Color: 16
Size: 289391 Color: 5

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 414145 Color: 11
Size: 295183 Color: 8
Size: 290673 Color: 17

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 414225 Color: 10
Size: 327480 Color: 9
Size: 258296 Color: 12

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 414226 Color: 3
Size: 316805 Color: 11
Size: 268970 Color: 7

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 414269 Color: 11
Size: 316859 Color: 12
Size: 268873 Color: 17

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 414466 Color: 4
Size: 323466 Color: 14
Size: 262069 Color: 9

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 414378 Color: 11
Size: 333488 Color: 6
Size: 252135 Color: 12

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 414453 Color: 17
Size: 333872 Color: 18
Size: 251676 Color: 3

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 414497 Color: 0
Size: 311670 Color: 5
Size: 273834 Color: 19

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 414540 Color: 8
Size: 312054 Color: 11
Size: 273407 Color: 15

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 414544 Color: 1
Size: 322398 Color: 4
Size: 263059 Color: 11

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 414797 Color: 19
Size: 326695 Color: 16
Size: 258509 Color: 3

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 414861 Color: 17
Size: 295679 Color: 2
Size: 289461 Color: 19

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 414862 Color: 7
Size: 298924 Color: 14
Size: 286215 Color: 13

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 414869 Color: 16
Size: 317206 Color: 2
Size: 267926 Color: 14

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 414871 Color: 3
Size: 322740 Color: 14
Size: 262390 Color: 15

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 414939 Color: 3
Size: 301210 Color: 7
Size: 283852 Color: 4

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 414951 Color: 0
Size: 315561 Color: 18
Size: 269489 Color: 10

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 415065 Color: 5
Size: 324812 Color: 16
Size: 260124 Color: 19

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 415155 Color: 3
Size: 311298 Color: 4
Size: 273548 Color: 12

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 415181 Color: 16
Size: 305959 Color: 18
Size: 278861 Color: 3

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 415249 Color: 12
Size: 329890 Color: 17
Size: 254862 Color: 9

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 415320 Color: 17
Size: 301246 Color: 13
Size: 283435 Color: 8

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 415325 Color: 7
Size: 292575 Color: 5
Size: 292101 Color: 11

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 415345 Color: 15
Size: 315970 Color: 18
Size: 268686 Color: 2

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 415394 Color: 9
Size: 292951 Color: 5
Size: 291656 Color: 8

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 415425 Color: 11
Size: 318957 Color: 16
Size: 265619 Color: 19

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 415426 Color: 8
Size: 294404 Color: 13
Size: 290171 Color: 17

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 415584 Color: 3
Size: 293073 Color: 9
Size: 291344 Color: 7

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 415661 Color: 7
Size: 322791 Color: 4
Size: 261549 Color: 2

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 415679 Color: 6
Size: 309540 Color: 14
Size: 274782 Color: 14

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 415690 Color: 1
Size: 322767 Color: 12
Size: 261544 Color: 8

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 415713 Color: 8
Size: 301458 Color: 1
Size: 282830 Color: 2

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 415811 Color: 11
Size: 311944 Color: 9
Size: 272246 Color: 5

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 415853 Color: 18
Size: 310039 Color: 12
Size: 274109 Color: 6

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 416034 Color: 3
Size: 329664 Color: 16
Size: 254303 Color: 14

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 415929 Color: 13
Size: 295273 Color: 4
Size: 288799 Color: 9

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 415961 Color: 12
Size: 313295 Color: 16
Size: 270745 Color: 1

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 416037 Color: 17
Size: 318315 Color: 13
Size: 265649 Color: 3

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 416039 Color: 18
Size: 298160 Color: 13
Size: 285802 Color: 10

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 416065 Color: 19
Size: 322856 Color: 0
Size: 261080 Color: 8

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 416088 Color: 0
Size: 328169 Color: 14
Size: 255744 Color: 8

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 416184 Color: 2
Size: 330754 Color: 16
Size: 253063 Color: 6

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 416191 Color: 19
Size: 329492 Color: 5
Size: 254318 Color: 17

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 416199 Color: 11
Size: 329203 Color: 16
Size: 254599 Color: 5

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 416215 Color: 5
Size: 310095 Color: 10
Size: 273691 Color: 9

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 416381 Color: 3
Size: 300845 Color: 4
Size: 282775 Color: 13

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 416315 Color: 0
Size: 307294 Color: 5
Size: 276392 Color: 17

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 416375 Color: 1
Size: 307528 Color: 1
Size: 276098 Color: 12

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 416429 Color: 11
Size: 298001 Color: 13
Size: 285571 Color: 11

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 416462 Color: 7
Size: 330253 Color: 3
Size: 253286 Color: 8

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 416536 Color: 9
Size: 326502 Color: 6
Size: 256963 Color: 15

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 416580 Color: 6
Size: 307369 Color: 12
Size: 276052 Color: 9

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 416608 Color: 12
Size: 308104 Color: 15
Size: 275289 Color: 19

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 416620 Color: 1
Size: 307338 Color: 19
Size: 276043 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 416669 Color: 1
Size: 313094 Color: 9
Size: 270238 Color: 17

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 416738 Color: 0
Size: 307375 Color: 10
Size: 275888 Color: 3

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 416789 Color: 6
Size: 315135 Color: 2
Size: 268077 Color: 4

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 416793 Color: 1
Size: 294303 Color: 7
Size: 288905 Color: 17

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 416901 Color: 12
Size: 326324 Color: 10
Size: 256776 Color: 9

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 416935 Color: 15
Size: 323996 Color: 9
Size: 259070 Color: 7

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 416973 Color: 11
Size: 313312 Color: 14
Size: 269716 Color: 5

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 416978 Color: 2
Size: 315116 Color: 3
Size: 267907 Color: 6

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 417005 Color: 10
Size: 307689 Color: 13
Size: 275307 Color: 13

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 417005 Color: 7
Size: 332948 Color: 7
Size: 250048 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 417007 Color: 9
Size: 300943 Color: 2
Size: 282051 Color: 12

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 417008 Color: 16
Size: 325467 Color: 15
Size: 257526 Color: 15

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 417009 Color: 2
Size: 316785 Color: 10
Size: 266207 Color: 18

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 417088 Color: 7
Size: 309342 Color: 17
Size: 273571 Color: 1

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 417096 Color: 5
Size: 322066 Color: 3
Size: 260839 Color: 2

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 417159 Color: 2
Size: 302545 Color: 1
Size: 280297 Color: 12

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 417163 Color: 6
Size: 297326 Color: 13
Size: 285512 Color: 14

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 417250 Color: 12
Size: 322348 Color: 4
Size: 260403 Color: 2

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 417361 Color: 14
Size: 313520 Color: 9
Size: 269120 Color: 15

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 417369 Color: 19
Size: 316297 Color: 3
Size: 266335 Color: 2

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 417416 Color: 7
Size: 292712 Color: 17
Size: 289873 Color: 11

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 417437 Color: 11
Size: 295607 Color: 5
Size: 286957 Color: 5

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 417466 Color: 18
Size: 293101 Color: 5
Size: 289434 Color: 8

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 417622 Color: 18
Size: 316487 Color: 5
Size: 265892 Color: 5

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 417639 Color: 1
Size: 298771 Color: 5
Size: 283591 Color: 19

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 417658 Color: 3
Size: 309143 Color: 6
Size: 273200 Color: 10

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 417728 Color: 1
Size: 331962 Color: 1
Size: 250311 Color: 15

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 417812 Color: 1
Size: 318097 Color: 12
Size: 264092 Color: 14

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 417839 Color: 5
Size: 292684 Color: 16
Size: 289478 Color: 2

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 417911 Color: 15
Size: 295130 Color: 13
Size: 286960 Color: 17

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 417990 Color: 18
Size: 307183 Color: 19
Size: 274828 Color: 7

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 418201 Color: 14
Size: 311363 Color: 9
Size: 270437 Color: 1

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 418215 Color: 11
Size: 321072 Color: 5
Size: 260714 Color: 11

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 418248 Color: 14
Size: 302609 Color: 9
Size: 279144 Color: 13

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 418274 Color: 6
Size: 331096 Color: 9
Size: 250631 Color: 4

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 418510 Color: 13
Size: 312687 Color: 16
Size: 268804 Color: 10

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 418511 Color: 5
Size: 312946 Color: 0
Size: 268544 Color: 11

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 418570 Color: 9
Size: 308334 Color: 3
Size: 273097 Color: 17

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 418680 Color: 12
Size: 326081 Color: 5
Size: 255240 Color: 10

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 418854 Color: 7
Size: 329759 Color: 1
Size: 251388 Color: 17

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 418971 Color: 13
Size: 303389 Color: 2
Size: 277641 Color: 1

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 419009 Color: 15
Size: 299299 Color: 8
Size: 281693 Color: 18

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 419045 Color: 5
Size: 325940 Color: 6
Size: 255016 Color: 15

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 419259 Color: 14
Size: 311729 Color: 2
Size: 269013 Color: 6

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 419363 Color: 18
Size: 301092 Color: 12
Size: 279546 Color: 11

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 419393 Color: 18
Size: 308881 Color: 19
Size: 271727 Color: 11

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 419469 Color: 1
Size: 293655 Color: 4
Size: 286877 Color: 17

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 419528 Color: 6
Size: 315854 Color: 0
Size: 264619 Color: 3

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 419573 Color: 4
Size: 328792 Color: 7
Size: 251636 Color: 17

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 419599 Color: 16
Size: 308061 Color: 4
Size: 272341 Color: 8

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 419624 Color: 6
Size: 293170 Color: 13
Size: 287207 Color: 4

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 419687 Color: 19
Size: 312194 Color: 11
Size: 268120 Color: 13

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 419891 Color: 15
Size: 292905 Color: 11
Size: 287205 Color: 12

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 419961 Color: 15
Size: 315199 Color: 3
Size: 264841 Color: 18

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 420014 Color: 5
Size: 327661 Color: 17
Size: 252326 Color: 2

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 420069 Color: 5
Size: 294489 Color: 1
Size: 285443 Color: 19

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 420071 Color: 10
Size: 324699 Color: 1
Size: 255231 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 420263 Color: 2
Size: 329459 Color: 11
Size: 250279 Color: 16

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 420296 Color: 12
Size: 305227 Color: 3
Size: 274478 Color: 2

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 420317 Color: 18
Size: 298233 Color: 5
Size: 281451 Color: 1

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 420339 Color: 7
Size: 328306 Color: 14
Size: 251356 Color: 12

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 420451 Color: 9
Size: 323151 Color: 8
Size: 256399 Color: 5

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 420484 Color: 4
Size: 319012 Color: 19
Size: 260505 Color: 5

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 420535 Color: 4
Size: 310157 Color: 3
Size: 269309 Color: 16

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 420619 Color: 4
Size: 299061 Color: 7
Size: 280321 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 420711 Color: 6
Size: 326958 Color: 2
Size: 252332 Color: 11

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 420844 Color: 9
Size: 304492 Color: 12
Size: 274665 Color: 7

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 421100 Color: 13
Size: 323167 Color: 12
Size: 255734 Color: 1

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 421123 Color: 19
Size: 304089 Color: 3
Size: 274789 Color: 4

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 421163 Color: 18
Size: 309815 Color: 4
Size: 269023 Color: 17

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 421198 Color: 19
Size: 304206 Color: 7
Size: 274597 Color: 14

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 421219 Color: 13
Size: 306372 Color: 15
Size: 272410 Color: 8

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 421311 Color: 12
Size: 292247 Color: 14
Size: 286443 Color: 14

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 421339 Color: 1
Size: 293603 Color: 2
Size: 285059 Color: 17

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 421351 Color: 8
Size: 327774 Color: 0
Size: 250876 Color: 3

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 421379 Color: 14
Size: 305516 Color: 9
Size: 273106 Color: 6

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 421415 Color: 10
Size: 297454 Color: 6
Size: 281132 Color: 10

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 421431 Color: 15
Size: 292449 Color: 14
Size: 286121 Color: 5

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 421449 Color: 17
Size: 306298 Color: 15
Size: 272254 Color: 4

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 421528 Color: 15
Size: 325483 Color: 2
Size: 252990 Color: 14

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 421711 Color: 4
Size: 292143 Color: 12
Size: 286147 Color: 11

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 421853 Color: 0
Size: 308903 Color: 9
Size: 269245 Color: 10

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 421867 Color: 9
Size: 291382 Color: 14
Size: 286752 Color: 17

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 421972 Color: 12
Size: 324597 Color: 0
Size: 253432 Color: 1

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 421981 Color: 7
Size: 315063 Color: 4
Size: 262957 Color: 13

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 422071 Color: 18
Size: 324785 Color: 7
Size: 253145 Color: 3

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 422140 Color: 5
Size: 324923 Color: 9
Size: 252938 Color: 9

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 422199 Color: 15
Size: 318732 Color: 5
Size: 259070 Color: 15

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 422201 Color: 7
Size: 296577 Color: 19
Size: 281223 Color: 6

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 422229 Color: 13
Size: 308895 Color: 13
Size: 268877 Color: 2

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 422244 Color: 2
Size: 312610 Color: 14
Size: 265147 Color: 14

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 422271 Color: 13
Size: 313903 Color: 14
Size: 263827 Color: 16

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 422297 Color: 10
Size: 301863 Color: 14
Size: 275841 Color: 3

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 422333 Color: 16
Size: 293976 Color: 9
Size: 283692 Color: 4

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 422366 Color: 8
Size: 326792 Color: 11
Size: 250843 Color: 15

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 422373 Color: 17
Size: 301917 Color: 10
Size: 275711 Color: 15

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 422396 Color: 0
Size: 294744 Color: 9
Size: 282861 Color: 7

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 422541 Color: 16
Size: 298592 Color: 18
Size: 278868 Color: 16

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 422593 Color: 13
Size: 288924 Color: 15
Size: 288484 Color: 4

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 422649 Color: 16
Size: 308233 Color: 2
Size: 269119 Color: 1

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 422743 Color: 12
Size: 309102 Color: 9
Size: 268156 Color: 5

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 422772 Color: 15
Size: 292184 Color: 2
Size: 285045 Color: 18

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 422782 Color: 11
Size: 289772 Color: 5
Size: 287447 Color: 2

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 422833 Color: 15
Size: 310150 Color: 18
Size: 267018 Color: 13

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 422865 Color: 14
Size: 324190 Color: 4
Size: 252946 Color: 18

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 422944 Color: 7
Size: 316444 Color: 9
Size: 260613 Color: 1

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 422994 Color: 19
Size: 315163 Color: 5
Size: 261844 Color: 14

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 423010 Color: 11
Size: 305603 Color: 9
Size: 271388 Color: 15

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 423093 Color: 16
Size: 306777 Color: 13
Size: 270131 Color: 1

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 423115 Color: 14
Size: 306602 Color: 7
Size: 270284 Color: 10

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 423116 Color: 16
Size: 316562 Color: 0
Size: 260323 Color: 8

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 423156 Color: 7
Size: 288960 Color: 12
Size: 287885 Color: 4

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 423172 Color: 12
Size: 322812 Color: 14
Size: 254017 Color: 2

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 423215 Color: 0
Size: 310954 Color: 11
Size: 265832 Color: 13

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 423238 Color: 7
Size: 289176 Color: 17
Size: 287587 Color: 19

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 423263 Color: 5
Size: 323389 Color: 14
Size: 253349 Color: 15

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 423292 Color: 0
Size: 314204 Color: 7
Size: 262505 Color: 11

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 423368 Color: 12
Size: 298777 Color: 10
Size: 277856 Color: 4

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 423376 Color: 16
Size: 315440 Color: 1
Size: 261185 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 423432 Color: 17
Size: 315748 Color: 12
Size: 260821 Color: 9

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 423454 Color: 6
Size: 305294 Color: 0
Size: 271253 Color: 13

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 423472 Color: 11
Size: 310180 Color: 12
Size: 266349 Color: 1

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 423477 Color: 5
Size: 326054 Color: 8
Size: 250470 Color: 14

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 423479 Color: 2
Size: 308936 Color: 4
Size: 267586 Color: 15

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 423491 Color: 2
Size: 325327 Color: 6
Size: 251183 Color: 16

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 423499 Color: 16
Size: 289163 Color: 6
Size: 287339 Color: 8

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 423703 Color: 4
Size: 290407 Color: 2
Size: 285891 Color: 8

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 423571 Color: 6
Size: 300314 Color: 8
Size: 276116 Color: 16

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 423574 Color: 0
Size: 319168 Color: 5
Size: 257259 Color: 16

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 423603 Color: 5
Size: 308840 Color: 15
Size: 267558 Color: 2

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 423610 Color: 8
Size: 299538 Color: 12
Size: 276853 Color: 18

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 423746 Color: 4
Size: 309370 Color: 8
Size: 266885 Color: 17

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 423612 Color: 15
Size: 304516 Color: 14
Size: 271873 Color: 13

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 423739 Color: 11
Size: 324440 Color: 7
Size: 251822 Color: 13

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 423805 Color: 8
Size: 309228 Color: 11
Size: 266968 Color: 15

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 423964 Color: 4
Size: 318088 Color: 13
Size: 257949 Color: 14

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 423880 Color: 2
Size: 303338 Color: 5
Size: 272783 Color: 12

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 423881 Color: 9
Size: 312309 Color: 12
Size: 263811 Color: 12

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 423899 Color: 12
Size: 297943 Color: 6
Size: 278159 Color: 9

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 423976 Color: 8
Size: 304906 Color: 12
Size: 271119 Color: 16

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 424025 Color: 4
Size: 299755 Color: 16
Size: 276221 Color: 13

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 424048 Color: 7
Size: 292605 Color: 6
Size: 283348 Color: 3

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 424232 Color: 18
Size: 307853 Color: 16
Size: 267916 Color: 13

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 424326 Color: 13
Size: 315986 Color: 8
Size: 259689 Color: 11

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 424379 Color: 7
Size: 291544 Color: 0
Size: 284078 Color: 17

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 424567 Color: 7
Size: 294534 Color: 2
Size: 280900 Color: 1

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 424669 Color: 7
Size: 319589 Color: 14
Size: 255743 Color: 12

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 424855 Color: 4
Size: 297412 Color: 13
Size: 277734 Color: 11

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 424708 Color: 12
Size: 316004 Color: 15
Size: 259289 Color: 15

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 424756 Color: 10
Size: 313496 Color: 9
Size: 261749 Color: 1

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 424829 Color: 3
Size: 291820 Color: 16
Size: 283352 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 424954 Color: 17
Size: 320373 Color: 2
Size: 254674 Color: 4

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 425004 Color: 0
Size: 319640 Color: 0
Size: 255357 Color: 7

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 425038 Color: 13
Size: 315615 Color: 19
Size: 259348 Color: 3

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 425041 Color: 5
Size: 314012 Color: 17
Size: 260948 Color: 19

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 425167 Color: 9
Size: 294245 Color: 10
Size: 280589 Color: 7

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 425367 Color: 6
Size: 320162 Color: 16
Size: 254472 Color: 6

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 425495 Color: 17
Size: 323550 Color: 2
Size: 250956 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 425495 Color: 17
Size: 314606 Color: 2
Size: 259900 Color: 6

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 425513 Color: 6
Size: 300192 Color: 12
Size: 274296 Color: 11

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 425618 Color: 6
Size: 295954 Color: 8
Size: 278429 Color: 4

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 425729 Color: 6
Size: 315905 Color: 15
Size: 258367 Color: 16

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 425781 Color: 2
Size: 294638 Color: 2
Size: 279582 Color: 16

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 425792 Color: 19
Size: 314922 Color: 3
Size: 259287 Color: 3

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 425817 Color: 6
Size: 288828 Color: 13
Size: 285356 Color: 9

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 425829 Color: 18
Size: 291883 Color: 17
Size: 282289 Color: 3

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 425851 Color: 14
Size: 291345 Color: 0
Size: 282805 Color: 11

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 425874 Color: 16
Size: 309759 Color: 4
Size: 264368 Color: 14

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 425910 Color: 16
Size: 308438 Color: 8
Size: 265653 Color: 19

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 425927 Color: 19
Size: 320264 Color: 6
Size: 253810 Color: 13

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 425927 Color: 17
Size: 292527 Color: 7
Size: 281547 Color: 17

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 426063 Color: 15
Size: 315661 Color: 19
Size: 258277 Color: 6

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 426075 Color: 8
Size: 306096 Color: 7
Size: 267830 Color: 6

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 426105 Color: 18
Size: 289295 Color: 2
Size: 284601 Color: 4

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 426163 Color: 8
Size: 306189 Color: 15
Size: 267649 Color: 18

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 426294 Color: 17
Size: 302863 Color: 10
Size: 270844 Color: 2

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 426310 Color: 18
Size: 288404 Color: 8
Size: 285287 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 426393 Color: 2
Size: 315448 Color: 5
Size: 258160 Color: 2

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 426419 Color: 3
Size: 306339 Color: 19
Size: 267243 Color: 15

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 426444 Color: 14
Size: 310592 Color: 17
Size: 262965 Color: 4

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 426465 Color: 17
Size: 287030 Color: 1
Size: 286506 Color: 3

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 426497 Color: 13
Size: 310441 Color: 12
Size: 263063 Color: 1

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 426558 Color: 0
Size: 294357 Color: 2
Size: 279086 Color: 10

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 426617 Color: 0
Size: 320383 Color: 13
Size: 253001 Color: 2

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 426629 Color: 13
Size: 322053 Color: 17
Size: 251319 Color: 7

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 426639 Color: 8
Size: 298986 Color: 6
Size: 274376 Color: 6

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 426639 Color: 14
Size: 320427 Color: 11
Size: 252935 Color: 4

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 426674 Color: 12
Size: 304263 Color: 17
Size: 269064 Color: 1

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 426732 Color: 1
Size: 314537 Color: 3
Size: 258732 Color: 14

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 426776 Color: 19
Size: 308519 Color: 2
Size: 264706 Color: 6

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 426801 Color: 18
Size: 315093 Color: 10
Size: 258107 Color: 16

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 426845 Color: 11
Size: 286817 Color: 4
Size: 286339 Color: 5

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 426846 Color: 1
Size: 320601 Color: 18
Size: 252554 Color: 10

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 426859 Color: 10
Size: 310133 Color: 15
Size: 263009 Color: 19

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 426947 Color: 12
Size: 321008 Color: 15
Size: 252046 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 426979 Color: 12
Size: 291519 Color: 10
Size: 281503 Color: 16

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 426981 Color: 8
Size: 287285 Color: 18
Size: 285735 Color: 4

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 426983 Color: 17
Size: 320600 Color: 5
Size: 252418 Color: 19

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 427074 Color: 6
Size: 309722 Color: 9
Size: 263205 Color: 18

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 427126 Color: 16
Size: 298449 Color: 1
Size: 274426 Color: 2

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 427182 Color: 12
Size: 292438 Color: 9
Size: 280381 Color: 9

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 427236 Color: 14
Size: 302081 Color: 11
Size: 270684 Color: 1

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 427238 Color: 3
Size: 286480 Color: 4
Size: 286283 Color: 3

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 427285 Color: 7
Size: 314364 Color: 15
Size: 258352 Color: 18

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 427313 Color: 3
Size: 308619 Color: 6
Size: 264069 Color: 19

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 427399 Color: 13
Size: 290192 Color: 1
Size: 282410 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 427420 Color: 13
Size: 304782 Color: 5
Size: 267799 Color: 19

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 427420 Color: 19
Size: 320374 Color: 17
Size: 252207 Color: 7

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 427422 Color: 11
Size: 299147 Color: 4
Size: 273432 Color: 19

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 427577 Color: 14
Size: 319355 Color: 18
Size: 253069 Color: 12

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 427654 Color: 13
Size: 311789 Color: 1
Size: 260558 Color: 8

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 427666 Color: 9
Size: 294940 Color: 13
Size: 277395 Color: 2

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 427739 Color: 0
Size: 290692 Color: 11
Size: 281570 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 427767 Color: 16
Size: 313255 Color: 0
Size: 258979 Color: 4

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 427935 Color: 9
Size: 286720 Color: 8
Size: 285346 Color: 8

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 427949 Color: 0
Size: 310095 Color: 8
Size: 261957 Color: 11

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 427970 Color: 14
Size: 313177 Color: 13
Size: 258854 Color: 12

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 428020 Color: 6
Size: 310307 Color: 4
Size: 261674 Color: 12

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 428044 Color: 5
Size: 295342 Color: 7
Size: 276615 Color: 8

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 428152 Color: 0
Size: 297736 Color: 3
Size: 274113 Color: 15

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 428186 Color: 9
Size: 286891 Color: 11
Size: 284924 Color: 14

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 428245 Color: 18
Size: 319143 Color: 13
Size: 252613 Color: 2

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 428251 Color: 5
Size: 287693 Color: 13
Size: 284057 Color: 6

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 428280 Color: 11
Size: 289639 Color: 10
Size: 282082 Color: 3

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 428503 Color: 12
Size: 316136 Color: 11
Size: 255362 Color: 16

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 428525 Color: 17
Size: 310294 Color: 11
Size: 261182 Color: 3

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 428556 Color: 7
Size: 298433 Color: 11
Size: 273012 Color: 5

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 428576 Color: 1
Size: 318633 Color: 3
Size: 252792 Color: 15

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 428589 Color: 15
Size: 293894 Color: 12
Size: 277518 Color: 6

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 428608 Color: 14
Size: 307072 Color: 15
Size: 264321 Color: 5

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 428617 Color: 16
Size: 302579 Color: 8
Size: 268805 Color: 17

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 428659 Color: 14
Size: 307067 Color: 0
Size: 264275 Color: 11

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 428705 Color: 12
Size: 297191 Color: 4
Size: 274105 Color: 5

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 428735 Color: 7
Size: 317640 Color: 3
Size: 253626 Color: 6

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 428806 Color: 3
Size: 305306 Color: 6
Size: 265889 Color: 2

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 428930 Color: 5
Size: 314602 Color: 0
Size: 256469 Color: 6

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 428942 Color: 18
Size: 289426 Color: 10
Size: 281633 Color: 14

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 428954 Color: 14
Size: 298820 Color: 12
Size: 272227 Color: 11

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 429039 Color: 5
Size: 310004 Color: 2
Size: 260958 Color: 4

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 429053 Color: 5
Size: 307752 Color: 18
Size: 263196 Color: 6

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 429086 Color: 16
Size: 289189 Color: 5
Size: 281726 Color: 12

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 429096 Color: 12
Size: 292255 Color: 6
Size: 278650 Color: 19

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 429145 Color: 17
Size: 289756 Color: 16
Size: 281100 Color: 1

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 429173 Color: 14
Size: 314618 Color: 16
Size: 256210 Color: 6

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 429289 Color: 7
Size: 317226 Color: 10
Size: 253486 Color: 4

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 429349 Color: 17
Size: 308087 Color: 0
Size: 262565 Color: 7

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 429350 Color: 10
Size: 294748 Color: 2
Size: 275903 Color: 13

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 429352 Color: 11
Size: 288114 Color: 15
Size: 282535 Color: 0

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 429358 Color: 7
Size: 303614 Color: 15
Size: 267029 Color: 2

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 429540 Color: 17
Size: 305144 Color: 14
Size: 265317 Color: 12

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 429575 Color: 1
Size: 306016 Color: 15
Size: 264410 Color: 13

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 429668 Color: 8
Size: 296023 Color: 1
Size: 274310 Color: 3

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 429930 Color: 1
Size: 288000 Color: 3
Size: 282071 Color: 17

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 429935 Color: 17
Size: 302923 Color: 6
Size: 267143 Color: 18

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 430134 Color: 16
Size: 303834 Color: 13
Size: 266033 Color: 12

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 430239 Color: 8
Size: 304231 Color: 8
Size: 265531 Color: 4

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 430254 Color: 6
Size: 319697 Color: 7
Size: 250050 Color: 8

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 430374 Color: 10
Size: 319544 Color: 13
Size: 250083 Color: 13

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 430393 Color: 0
Size: 319475 Color: 3
Size: 250133 Color: 18

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 430429 Color: 17
Size: 294652 Color: 6
Size: 274920 Color: 11

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 430495 Color: 17
Size: 306137 Color: 12
Size: 263369 Color: 19

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 430516 Color: 9
Size: 294860 Color: 4
Size: 274625 Color: 10

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 430673 Color: 8
Size: 289639 Color: 6
Size: 279689 Color: 3

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 430715 Color: 7
Size: 302802 Color: 13
Size: 266484 Color: 1

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 430746 Color: 5
Size: 289004 Color: 3
Size: 280251 Color: 8

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 430809 Color: 18
Size: 312252 Color: 8
Size: 256940 Color: 6

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 430849 Color: 6
Size: 293142 Color: 17
Size: 276010 Color: 12

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 430957 Color: 14
Size: 311439 Color: 4
Size: 257605 Color: 7

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 430978 Color: 17
Size: 315325 Color: 16
Size: 253698 Color: 17

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 430983 Color: 10
Size: 307133 Color: 5
Size: 261885 Color: 16

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 431010 Color: 16
Size: 315122 Color: 15
Size: 253869 Color: 17

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 431057 Color: 10
Size: 285698 Color: 11
Size: 283246 Color: 16

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 431222 Color: 3
Size: 285384 Color: 10
Size: 283395 Color: 10

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 431291 Color: 14
Size: 314066 Color: 4
Size: 254644 Color: 18

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 431352 Color: 8
Size: 294644 Color: 19
Size: 274005 Color: 8

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 431358 Color: 15
Size: 291793 Color: 6
Size: 276850 Color: 9

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 431418 Color: 3
Size: 302978 Color: 1
Size: 265605 Color: 14

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 431471 Color: 11
Size: 300453 Color: 7
Size: 268077 Color: 16

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 431478 Color: 7
Size: 311502 Color: 13
Size: 257021 Color: 12

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 431496 Color: 7
Size: 293848 Color: 4
Size: 274657 Color: 2

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 431585 Color: 16
Size: 307603 Color: 17
Size: 260813 Color: 1

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 431590 Color: 18
Size: 305828 Color: 0
Size: 262583 Color: 15

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 431670 Color: 16
Size: 315835 Color: 5
Size: 252496 Color: 10

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 431710 Color: 15
Size: 298811 Color: 11
Size: 269480 Color: 16

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 431849 Color: 0
Size: 303086 Color: 15
Size: 265066 Color: 4

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 431860 Color: 5
Size: 300467 Color: 1
Size: 267674 Color: 3

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 431927 Color: 13
Size: 286119 Color: 19
Size: 281955 Color: 19

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 431932 Color: 13
Size: 285968 Color: 18
Size: 282101 Color: 1

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 431971 Color: 6
Size: 312773 Color: 15
Size: 255257 Color: 14

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 432019 Color: 0
Size: 301084 Color: 16
Size: 266898 Color: 10

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 432031 Color: 15
Size: 314126 Color: 7
Size: 253844 Color: 2

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 432111 Color: 7
Size: 308323 Color: 3
Size: 259567 Color: 15

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 432095 Color: 5
Size: 316418 Color: 0
Size: 251488 Color: 11

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 432142 Color: 3
Size: 299618 Color: 9
Size: 268241 Color: 18

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 432182 Color: 18
Size: 313561 Color: 18
Size: 254258 Color: 16

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 432310 Color: 0
Size: 314833 Color: 7
Size: 252858 Color: 15

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 432334 Color: 11
Size: 315123 Color: 14
Size: 252544 Color: 4

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 432381 Color: 12
Size: 305433 Color: 2
Size: 262187 Color: 12

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 432402 Color: 13
Size: 310448 Color: 19
Size: 257151 Color: 19

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 432422 Color: 8
Size: 304039 Color: 3
Size: 263540 Color: 19

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 432515 Color: 5
Size: 307691 Color: 11
Size: 259795 Color: 10

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 432580 Color: 9
Size: 311394 Color: 15
Size: 256027 Color: 8

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 432603 Color: 2
Size: 299404 Color: 4
Size: 267994 Color: 16

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 432691 Color: 2
Size: 307424 Color: 15
Size: 259886 Color: 3

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 432762 Color: 2
Size: 315658 Color: 13
Size: 251581 Color: 8

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 432911 Color: 19
Size: 311231 Color: 11
Size: 255859 Color: 5

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 433050 Color: 13
Size: 304541 Color: 19
Size: 262410 Color: 19

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 433069 Color: 1
Size: 297722 Color: 8
Size: 269210 Color: 8

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 433219 Color: 4
Size: 290131 Color: 13
Size: 276651 Color: 10

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 433141 Color: 7
Size: 291270 Color: 9
Size: 275590 Color: 15

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 433180 Color: 12
Size: 309646 Color: 11
Size: 257175 Color: 12

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 433199 Color: 19
Size: 297429 Color: 15
Size: 269373 Color: 5

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 433222 Color: 15
Size: 301304 Color: 18
Size: 265475 Color: 14

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 433233 Color: 13
Size: 307856 Color: 4
Size: 258912 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 433241 Color: 12
Size: 284729 Color: 17
Size: 282031 Color: 19

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 433390 Color: 15
Size: 293939 Color: 0
Size: 272672 Color: 12

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 433392 Color: 17
Size: 296583 Color: 18
Size: 270026 Color: 8

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 433405 Color: 11
Size: 288422 Color: 5
Size: 278174 Color: 17

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 433408 Color: 1
Size: 309195 Color: 0
Size: 257398 Color: 19

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 433446 Color: 5
Size: 306753 Color: 12
Size: 259802 Color: 4

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 433521 Color: 19
Size: 293867 Color: 16
Size: 272613 Color: 16

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 433556 Color: 5
Size: 316026 Color: 13
Size: 250419 Color: 19

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 433596 Color: 9
Size: 288037 Color: 3
Size: 278368 Color: 19

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 433635 Color: 6
Size: 296730 Color: 17
Size: 269636 Color: 8

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 433642 Color: 3
Size: 294693 Color: 18
Size: 271666 Color: 14

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 433699 Color: 4
Size: 295126 Color: 2
Size: 271176 Color: 7

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 433675 Color: 1
Size: 288600 Color: 2
Size: 277726 Color: 3

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 433681 Color: 17
Size: 294545 Color: 7
Size: 271775 Color: 8

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 433692 Color: 2
Size: 283357 Color: 17
Size: 282952 Color: 11

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 433728 Color: 17
Size: 296416 Color: 7
Size: 269857 Color: 0

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 433839 Color: 3
Size: 288455 Color: 4
Size: 277707 Color: 8

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 433840 Color: 2
Size: 301844 Color: 11
Size: 264317 Color: 17

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 433844 Color: 13
Size: 294736 Color: 5
Size: 271421 Color: 1

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 433858 Color: 5
Size: 284469 Color: 19
Size: 281674 Color: 19

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 433905 Color: 2
Size: 288373 Color: 9
Size: 277723 Color: 16

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 433933 Color: 10
Size: 283859 Color: 14
Size: 282209 Color: 17

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 433957 Color: 15
Size: 305991 Color: 2
Size: 260053 Color: 12

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 434034 Color: 4
Size: 310357 Color: 15
Size: 255610 Color: 7

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 433977 Color: 6
Size: 305092 Color: 5
Size: 260932 Color: 3

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 434003 Color: 10
Size: 289267 Color: 8
Size: 276731 Color: 1

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 434060 Color: 3
Size: 286690 Color: 16
Size: 279251 Color: 10

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 434128 Color: 18
Size: 303483 Color: 8
Size: 262390 Color: 4

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 434129 Color: 9
Size: 292834 Color: 8
Size: 273038 Color: 14

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 434140 Color: 19
Size: 315539 Color: 12
Size: 250322 Color: 18

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 434242 Color: 7
Size: 283644 Color: 19
Size: 282115 Color: 14

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 434170 Color: 8
Size: 297835 Color: 3
Size: 267996 Color: 15

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 434274 Color: 8
Size: 295184 Color: 13
Size: 270543 Color: 5

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 434284 Color: 5
Size: 287951 Color: 11
Size: 277766 Color: 13

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 434396 Color: 14
Size: 298812 Color: 12
Size: 266793 Color: 2

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 434501 Color: 6
Size: 307892 Color: 17
Size: 257608 Color: 7

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 434597 Color: 3
Size: 295608 Color: 9
Size: 269796 Color: 12

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 434730 Color: 5
Size: 303484 Color: 14
Size: 261787 Color: 8

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 434757 Color: 16
Size: 298929 Color: 14
Size: 266315 Color: 17

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 434831 Color: 3
Size: 302459 Color: 12
Size: 262711 Color: 16

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 434848 Color: 18
Size: 314275 Color: 13
Size: 250878 Color: 15

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 434874 Color: 7
Size: 302730 Color: 10
Size: 262397 Color: 19

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 434869 Color: 15
Size: 313469 Color: 8
Size: 251663 Color: 15

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 434907 Color: 6
Size: 288824 Color: 14
Size: 276270 Color: 12

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 435056 Color: 18
Size: 307287 Color: 15
Size: 257658 Color: 4

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 435116 Color: 5
Size: 290981 Color: 2
Size: 273904 Color: 2

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 435128 Color: 5
Size: 298946 Color: 10
Size: 265927 Color: 6

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 435262 Color: 5
Size: 285211 Color: 18
Size: 279528 Color: 7

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 435289 Color: 5
Size: 294187 Color: 15
Size: 270525 Color: 8

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 435402 Color: 5
Size: 286942 Color: 3
Size: 277657 Color: 3

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 435293 Color: 15
Size: 282638 Color: 12
Size: 282070 Color: 11

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 435326 Color: 14
Size: 282513 Color: 12
Size: 282162 Color: 9

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 435560 Color: 15
Size: 288713 Color: 7
Size: 275728 Color: 2

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 435662 Color: 5
Size: 292639 Color: 10
Size: 271700 Color: 18

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 435595 Color: 16
Size: 291009 Color: 8
Size: 273397 Color: 3

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 435623 Color: 4
Size: 314363 Color: 6
Size: 250015 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 435649 Color: 17
Size: 289197 Color: 12
Size: 275155 Color: 6

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 435674 Color: 3
Size: 305726 Color: 16
Size: 258601 Color: 15

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 435745 Color: 11
Size: 298142 Color: 12
Size: 266114 Color: 14

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 435774 Color: 6
Size: 297211 Color: 13
Size: 267016 Color: 5

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 435794 Color: 8
Size: 286098 Color: 1
Size: 278109 Color: 9

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 435799 Color: 16
Size: 283149 Color: 14
Size: 281053 Color: 11

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 436024 Color: 14
Size: 298384 Color: 9
Size: 265593 Color: 4

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 436055 Color: 6
Size: 297003 Color: 5
Size: 266943 Color: 2

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 436103 Color: 10
Size: 302604 Color: 16
Size: 261294 Color: 18

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 436124 Color: 15
Size: 286603 Color: 1
Size: 277274 Color: 12

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 436169 Color: 19
Size: 295880 Color: 10
Size: 267952 Color: 10

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 436182 Color: 2
Size: 291101 Color: 5
Size: 272718 Color: 3

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 436418 Color: 14
Size: 294354 Color: 1
Size: 269229 Color: 17

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 436514 Color: 9
Size: 287575 Color: 12
Size: 275912 Color: 13

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 436514 Color: 17
Size: 284518 Color: 8
Size: 278969 Color: 17

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 436533 Color: 9
Size: 291207 Color: 11
Size: 272261 Color: 15

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 436598 Color: 5
Size: 311878 Color: 17
Size: 251525 Color: 14

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 436577 Color: 0
Size: 289078 Color: 6
Size: 274346 Color: 4

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 436587 Color: 6
Size: 289941 Color: 13
Size: 273473 Color: 8

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 436621 Color: 2
Size: 291879 Color: 8
Size: 271501 Color: 9

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 436659 Color: 12
Size: 311199 Color: 10
Size: 252143 Color: 1

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 436667 Color: 13
Size: 305073 Color: 15
Size: 258261 Color: 6

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 436678 Color: 16
Size: 287382 Color: 2
Size: 275941 Color: 11

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 436705 Color: 8
Size: 299431 Color: 4
Size: 263865 Color: 16

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 436725 Color: 3
Size: 298022 Color: 10
Size: 265254 Color: 13

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 436726 Color: 13
Size: 286416 Color: 13
Size: 276859 Color: 18

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 436735 Color: 18
Size: 298704 Color: 2
Size: 264562 Color: 1

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 436758 Color: 6
Size: 312713 Color: 3
Size: 250530 Color: 14

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 436804 Color: 9
Size: 304275 Color: 15
Size: 258922 Color: 9

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 436942 Color: 18
Size: 310248 Color: 17
Size: 252811 Color: 18

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 437058 Color: 13
Size: 293704 Color: 2
Size: 269239 Color: 17

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 437125 Color: 4
Size: 285039 Color: 14
Size: 277837 Color: 3

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 437137 Color: 13
Size: 294882 Color: 10
Size: 267982 Color: 17

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 437250 Color: 14
Size: 291010 Color: 9
Size: 271741 Color: 9

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 437289 Color: 4
Size: 281450 Color: 0
Size: 281262 Color: 5

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 437437 Color: 9
Size: 285092 Color: 15
Size: 277472 Color: 8

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 437451 Color: 1
Size: 292794 Color: 12
Size: 269756 Color: 12

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 437606 Color: 19
Size: 295121 Color: 0
Size: 267274 Color: 14

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 437723 Color: 1
Size: 308661 Color: 19
Size: 253617 Color: 8

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 437727 Color: 15
Size: 288615 Color: 17
Size: 273659 Color: 14

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 437763 Color: 13
Size: 290273 Color: 4
Size: 271965 Color: 5

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 437881 Color: 10
Size: 298565 Color: 6
Size: 263555 Color: 7

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 437886 Color: 0
Size: 301425 Color: 4
Size: 260690 Color: 13

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 437954 Color: 9
Size: 308575 Color: 6
Size: 253472 Color: 18

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 437959 Color: 13
Size: 286981 Color: 16
Size: 275061 Color: 8

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 437996 Color: 17
Size: 293275 Color: 10
Size: 268730 Color: 10

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 438152 Color: 10
Size: 301536 Color: 3
Size: 260313 Color: 2

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 438388 Color: 6
Size: 295819 Color: 9
Size: 265794 Color: 4

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 438403 Color: 16
Size: 292258 Color: 2
Size: 269340 Color: 18

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 438469 Color: 2
Size: 310856 Color: 17
Size: 250676 Color: 3

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 438589 Color: 19
Size: 311408 Color: 8
Size: 250004 Color: 8

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 438601 Color: 4
Size: 299802 Color: 15
Size: 261598 Color: 11

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 438681 Color: 6
Size: 289971 Color: 4
Size: 271349 Color: 1

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 438773 Color: 9
Size: 284365 Color: 0
Size: 276863 Color: 4

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 438824 Color: 9
Size: 304211 Color: 13
Size: 256966 Color: 15

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 438857 Color: 13
Size: 285324 Color: 17
Size: 275820 Color: 14

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 439020 Color: 2
Size: 294618 Color: 9
Size: 266363 Color: 3

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 439099 Color: 6
Size: 300730 Color: 13
Size: 260172 Color: 14

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 439100 Color: 12
Size: 286943 Color: 12
Size: 273958 Color: 11

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 439114 Color: 13
Size: 309947 Color: 18
Size: 250940 Color: 14

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 439134 Color: 15
Size: 305672 Color: 17
Size: 255195 Color: 10

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 439183 Color: 1
Size: 308095 Color: 15
Size: 252723 Color: 5

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 439215 Color: 0
Size: 298555 Color: 4
Size: 262231 Color: 18

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 439238 Color: 1
Size: 295389 Color: 7
Size: 265374 Color: 11

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 439246 Color: 3
Size: 299150 Color: 11
Size: 261605 Color: 16

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 439273 Color: 3
Size: 299983 Color: 1
Size: 260745 Color: 19

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 439328 Color: 1
Size: 291026 Color: 6
Size: 269647 Color: 19

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 439330 Color: 7
Size: 307813 Color: 17
Size: 252858 Color: 4

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 439413 Color: 5
Size: 306522 Color: 12
Size: 254066 Color: 3

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 439348 Color: 8
Size: 287099 Color: 15
Size: 273554 Color: 12

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 439407 Color: 2
Size: 281331 Color: 8
Size: 279263 Color: 19

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 439415 Color: 3
Size: 302778 Color: 18
Size: 257808 Color: 15

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 439435 Color: 7
Size: 307282 Color: 14
Size: 253284 Color: 12

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 439451 Color: 15
Size: 295684 Color: 16
Size: 264866 Color: 7

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 439479 Color: 19
Size: 302051 Color: 5
Size: 258471 Color: 11

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 439491 Color: 15
Size: 286600 Color: 15
Size: 273910 Color: 3

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 439530 Color: 17
Size: 303401 Color: 12
Size: 257070 Color: 15

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 439532 Color: 3
Size: 291236 Color: 15
Size: 269233 Color: 19

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 439536 Color: 11
Size: 299349 Color: 1
Size: 261116 Color: 10

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 439670 Color: 9
Size: 300613 Color: 0
Size: 259718 Color: 4

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 439679 Color: 0
Size: 306711 Color: 1
Size: 253611 Color: 17

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 439719 Color: 0
Size: 304159 Color: 7
Size: 256123 Color: 3

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 439800 Color: 18
Size: 291197 Color: 10
Size: 269004 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 439849 Color: 19
Size: 294204 Color: 8
Size: 265948 Color: 6

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 440082 Color: 3
Size: 309571 Color: 14
Size: 250348 Color: 13

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 440084 Color: 9
Size: 293198 Color: 11
Size: 266719 Color: 18

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 440100 Color: 12
Size: 289356 Color: 10
Size: 270545 Color: 3

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 440134 Color: 4
Size: 287735 Color: 6
Size: 272132 Color: 12

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 440148 Color: 19
Size: 289262 Color: 15
Size: 270591 Color: 10

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 440227 Color: 7
Size: 306524 Color: 10
Size: 253250 Color: 15

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 440233 Color: 8
Size: 304285 Color: 1
Size: 255483 Color: 15

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 440314 Color: 16
Size: 302953 Color: 18
Size: 256734 Color: 4

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 440337 Color: 3
Size: 294041 Color: 9
Size: 265623 Color: 6

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 440315 Color: 2
Size: 300268 Color: 9
Size: 259418 Color: 1

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 440346 Color: 18
Size: 296281 Color: 1
Size: 263374 Color: 14

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 440514 Color: 19
Size: 303227 Color: 18
Size: 256260 Color: 8

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 440755 Color: 17
Size: 280497 Color: 15
Size: 278749 Color: 8

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 440944 Color: 7
Size: 297723 Color: 3
Size: 261334 Color: 8

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 441066 Color: 4
Size: 279579 Color: 17
Size: 279356 Color: 14

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 441311 Color: 19
Size: 293511 Color: 18
Size: 265179 Color: 6

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 441361 Color: 7
Size: 283398 Color: 1
Size: 275242 Color: 18

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 441391 Color: 9
Size: 297666 Color: 18
Size: 260944 Color: 14

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 441425 Color: 0
Size: 293748 Color: 18
Size: 264828 Color: 3

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 441425 Color: 7
Size: 298198 Color: 16
Size: 260378 Color: 6

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 441489 Color: 4
Size: 293074 Color: 17
Size: 265438 Color: 6

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 441495 Color: 18
Size: 282048 Color: 14
Size: 276458 Color: 8

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 441579 Color: 12
Size: 288950 Color: 7
Size: 269472 Color: 12

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 441633 Color: 7
Size: 308270 Color: 17
Size: 250098 Color: 2

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 441728 Color: 3
Size: 279664 Color: 11
Size: 278609 Color: 16

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 441690 Color: 16
Size: 282337 Color: 9
Size: 275974 Color: 18

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 441845 Color: 18
Size: 308020 Color: 10
Size: 250136 Color: 11

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 441948 Color: 19
Size: 298935 Color: 0
Size: 259118 Color: 11

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 441961 Color: 12
Size: 286131 Color: 15
Size: 271909 Color: 13

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 442096 Color: 7
Size: 303633 Color: 19
Size: 254272 Color: 16

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 442147 Color: 18
Size: 289850 Color: 16
Size: 268004 Color: 17

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 442291 Color: 14
Size: 291885 Color: 11
Size: 265825 Color: 5

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 442349 Color: 7
Size: 282309 Color: 9
Size: 275343 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 442668 Color: 3
Size: 286499 Color: 11
Size: 270834 Color: 1

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 442578 Color: 13
Size: 300664 Color: 13
Size: 256759 Color: 14

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 442622 Color: 2
Size: 302849 Color: 17
Size: 254530 Color: 16

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 442677 Color: 4
Size: 295332 Color: 1
Size: 261992 Color: 18

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 442690 Color: 13
Size: 293975 Color: 16
Size: 263336 Color: 9

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 442709 Color: 3
Size: 294953 Color: 6
Size: 262339 Color: 5

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 442724 Color: 17
Size: 294720 Color: 5
Size: 262557 Color: 1

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 442814 Color: 1
Size: 304871 Color: 9
Size: 252316 Color: 10

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 442893 Color: 5
Size: 303973 Color: 11
Size: 253135 Color: 15

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 442983 Color: 19
Size: 292829 Color: 0
Size: 264189 Color: 10

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 442986 Color: 8
Size: 283793 Color: 17
Size: 273222 Color: 5

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 443004 Color: 5
Size: 288012 Color: 3
Size: 268985 Color: 11

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 443035 Color: 8
Size: 291295 Color: 4
Size: 265671 Color: 1

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 443155 Color: 13
Size: 298746 Color: 9
Size: 258100 Color: 9

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 443222 Color: 6
Size: 302573 Color: 16
Size: 254206 Color: 12

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 443235 Color: 4
Size: 297497 Color: 18
Size: 259269 Color: 4

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 443250 Color: 11
Size: 293354 Color: 15
Size: 263397 Color: 2

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 443314 Color: 15
Size: 291188 Color: 9
Size: 265499 Color: 3

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 443390 Color: 13
Size: 292981 Color: 17
Size: 263630 Color: 19

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 443418 Color: 11
Size: 283611 Color: 13
Size: 272972 Color: 19

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 443436 Color: 2
Size: 282524 Color: 19
Size: 274041 Color: 6

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 443455 Color: 11
Size: 289903 Color: 5
Size: 266643 Color: 3

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 443463 Color: 0
Size: 303474 Color: 14
Size: 253064 Color: 14

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 443474 Color: 10
Size: 305233 Color: 11
Size: 251294 Color: 6

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 443506 Color: 4
Size: 284307 Color: 6
Size: 272188 Color: 5

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 443684 Color: 10
Size: 296099 Color: 19
Size: 260218 Color: 13

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 443800 Color: 3
Size: 282732 Color: 13
Size: 273469 Color: 7

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 443774 Color: 16
Size: 291739 Color: 2
Size: 264488 Color: 18

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 443785 Color: 11
Size: 301664 Color: 14
Size: 254552 Color: 16

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 443918 Color: 14
Size: 287036 Color: 16
Size: 269047 Color: 8

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 444036 Color: 4
Size: 293792 Color: 15
Size: 262173 Color: 15

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 444039 Color: 8
Size: 286501 Color: 5
Size: 269461 Color: 3

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 444190 Color: 7
Size: 297946 Color: 15
Size: 257865 Color: 17

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 444316 Color: 2
Size: 303480 Color: 6
Size: 252205 Color: 2

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 444421 Color: 16
Size: 302040 Color: 4
Size: 253540 Color: 10

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 444424 Color: 4
Size: 288924 Color: 18
Size: 266653 Color: 18

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 444552 Color: 14
Size: 289237 Color: 18
Size: 266212 Color: 18

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 444572 Color: 19
Size: 293555 Color: 3
Size: 261874 Color: 11

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 444723 Color: 19
Size: 284156 Color: 1
Size: 271122 Color: 12

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 444998 Color: 9
Size: 288464 Color: 15
Size: 266539 Color: 7

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 445001 Color: 15
Size: 304473 Color: 14
Size: 250527 Color: 12

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 445017 Color: 19
Size: 294604 Color: 8
Size: 260380 Color: 10

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 445049 Color: 12
Size: 277634 Color: 18
Size: 277318 Color: 16

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 445068 Color: 12
Size: 282528 Color: 3
Size: 272405 Color: 8

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 445075 Color: 18
Size: 283031 Color: 0
Size: 271895 Color: 13

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 445144 Color: 8
Size: 300226 Color: 5
Size: 254631 Color: 14

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 445210 Color: 11
Size: 282175 Color: 4
Size: 272616 Color: 12

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 445292 Color: 17
Size: 288345 Color: 6
Size: 266364 Color: 19

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 445335 Color: 11
Size: 302252 Color: 19
Size: 252414 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 445372 Color: 7
Size: 284889 Color: 12
Size: 269740 Color: 18

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 445433 Color: 2
Size: 283104 Color: 19
Size: 271464 Color: 4

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 445461 Color: 12
Size: 292498 Color: 18
Size: 262042 Color: 2

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 445608 Color: 0
Size: 304328 Color: 5
Size: 250065 Color: 15

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 445639 Color: 13
Size: 303106 Color: 4
Size: 251256 Color: 4

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 445782 Color: 3
Size: 278770 Color: 9
Size: 275449 Color: 13

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 445892 Color: 7
Size: 289778 Color: 4
Size: 264331 Color: 4

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 445896 Color: 17
Size: 303394 Color: 0
Size: 250711 Color: 13

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 445897 Color: 8
Size: 284536 Color: 0
Size: 269568 Color: 19

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 445925 Color: 3
Size: 303841 Color: 9
Size: 250235 Color: 7

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 446016 Color: 2
Size: 298234 Color: 15
Size: 255751 Color: 5

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 446070 Color: 11
Size: 279710 Color: 17
Size: 274221 Color: 8

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 446203 Color: 6
Size: 279403 Color: 17
Size: 274395 Color: 7

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 446232 Color: 5
Size: 285799 Color: 7
Size: 267970 Color: 10

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 446255 Color: 13
Size: 284080 Color: 3
Size: 269666 Color: 14

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 446315 Color: 11
Size: 278433 Color: 18
Size: 275253 Color: 1

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 446333 Color: 1
Size: 278183 Color: 9
Size: 275485 Color: 4

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 446372 Color: 8
Size: 301032 Color: 7
Size: 252597 Color: 7

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 446374 Color: 6
Size: 277598 Color: 4
Size: 276029 Color: 16

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 446381 Color: 17
Size: 280451 Color: 17
Size: 273169 Color: 14

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 446395 Color: 17
Size: 292798 Color: 0
Size: 260808 Color: 6

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 446568 Color: 3
Size: 297163 Color: 13
Size: 256270 Color: 8

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 446501 Color: 17
Size: 294514 Color: 15
Size: 258986 Color: 9

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 446527 Color: 15
Size: 298280 Color: 12
Size: 255194 Color: 13

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 446593 Color: 8
Size: 298912 Color: 17
Size: 254496 Color: 10

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 446600 Color: 19
Size: 290231 Color: 3
Size: 263170 Color: 2

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 446628 Color: 11
Size: 302187 Color: 8
Size: 251186 Color: 2

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 446655 Color: 8
Size: 283277 Color: 10
Size: 270069 Color: 2

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 446683 Color: 14
Size: 280660 Color: 11
Size: 272658 Color: 15

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 446647 Color: 17
Size: 294549 Color: 7
Size: 258805 Color: 18

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 446718 Color: 9
Size: 289242 Color: 3
Size: 264041 Color: 5

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 446781 Color: 12
Size: 302026 Color: 7
Size: 251194 Color: 14

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 446789 Color: 11
Size: 278852 Color: 14
Size: 274360 Color: 6

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 446806 Color: 11
Size: 282009 Color: 1
Size: 271186 Color: 4

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 446809 Color: 8
Size: 277772 Color: 0
Size: 275420 Color: 9

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 446845 Color: 15
Size: 284464 Color: 19
Size: 268692 Color: 19

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 446939 Color: 9
Size: 296293 Color: 13
Size: 256769 Color: 6

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 447049 Color: 3
Size: 277346 Color: 12
Size: 275606 Color: 6

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 447043 Color: 8
Size: 282292 Color: 2
Size: 270666 Color: 2

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 447103 Color: 2
Size: 294604 Color: 12
Size: 258294 Color: 9

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 447125 Color: 7
Size: 278974 Color: 10
Size: 273902 Color: 14

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 447141 Color: 15
Size: 298212 Color: 9
Size: 254648 Color: 13

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 447157 Color: 0
Size: 281764 Color: 19
Size: 271080 Color: 12

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 447182 Color: 5
Size: 285570 Color: 3
Size: 267249 Color: 16

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 447296 Color: 19
Size: 281014 Color: 11
Size: 271691 Color: 2

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 447388 Color: 10
Size: 278766 Color: 6
Size: 273847 Color: 18

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 447405 Color: 0
Size: 292730 Color: 13
Size: 259866 Color: 14

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 447410 Color: 18
Size: 299514 Color: 12
Size: 253077 Color: 6

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 447470 Color: 15
Size: 297579 Color: 16
Size: 254952 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 447474 Color: 10
Size: 282560 Color: 3
Size: 269967 Color: 5

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 447502 Color: 8
Size: 302094 Color: 18
Size: 250405 Color: 16

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 447505 Color: 4
Size: 285418 Color: 11
Size: 267078 Color: 10

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 447564 Color: 7
Size: 297011 Color: 17
Size: 255426 Color: 12

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 447640 Color: 18
Size: 287595 Color: 9
Size: 264766 Color: 14

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 447804 Color: 7
Size: 301103 Color: 6
Size: 251094 Color: 2

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 447885 Color: 18
Size: 299360 Color: 17
Size: 252756 Color: 19

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 447974 Color: 3
Size: 286885 Color: 12
Size: 265142 Color: 5

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 447975 Color: 18
Size: 277836 Color: 5
Size: 274190 Color: 11

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 448031 Color: 9
Size: 288683 Color: 7
Size: 263287 Color: 18

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 448039 Color: 14
Size: 300719 Color: 14
Size: 251243 Color: 8

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 448073 Color: 7
Size: 279962 Color: 18
Size: 271966 Color: 3

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 448125 Color: 6
Size: 295630 Color: 13
Size: 256246 Color: 16

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 448163 Color: 0
Size: 300032 Color: 6
Size: 251806 Color: 6

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 448165 Color: 9
Size: 280531 Color: 0
Size: 271305 Color: 18

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 448217 Color: 5
Size: 276767 Color: 7
Size: 275017 Color: 15

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 448299 Color: 6
Size: 280594 Color: 10
Size: 271108 Color: 13

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 448443 Color: 14
Size: 280270 Color: 5
Size: 271288 Color: 6

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 448501 Color: 3
Size: 283477 Color: 5
Size: 268023 Color: 16

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 448484 Color: 8
Size: 292804 Color: 6
Size: 258713 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 448492 Color: 10
Size: 279565 Color: 4
Size: 271944 Color: 13

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 448494 Color: 8
Size: 284855 Color: 0
Size: 266652 Color: 14

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 448496 Color: 11
Size: 276724 Color: 19
Size: 274781 Color: 7

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 448530 Color: 1
Size: 286407 Color: 2
Size: 265064 Color: 2

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 448536 Color: 7
Size: 281317 Color: 8
Size: 270148 Color: 15

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 448582 Color: 1
Size: 275785 Color: 5
Size: 275634 Color: 1

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 448617 Color: 19
Size: 292321 Color: 9
Size: 259063 Color: 17

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 448623 Color: 5
Size: 299727 Color: 6
Size: 251651 Color: 4

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 448628 Color: 15
Size: 292686 Color: 8
Size: 258687 Color: 16

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 448778 Color: 17
Size: 280941 Color: 16
Size: 270282 Color: 13

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 448931 Color: 17
Size: 285933 Color: 17
Size: 265137 Color: 12

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 448793 Color: 15
Size: 276841 Color: 4
Size: 274367 Color: 10

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 449007 Color: 17
Size: 288156 Color: 15
Size: 262838 Color: 8

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 449016 Color: 9
Size: 300411 Color: 5
Size: 250574 Color: 13

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 449045 Color: 11
Size: 288911 Color: 9
Size: 262045 Color: 3

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 449075 Color: 12
Size: 285530 Color: 4
Size: 265396 Color: 17

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 449151 Color: 14
Size: 296951 Color: 0
Size: 253899 Color: 1

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 449151 Color: 11
Size: 282788 Color: 6
Size: 268062 Color: 12

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 449173 Color: 16
Size: 287781 Color: 16
Size: 263047 Color: 6

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 449241 Color: 3
Size: 289919 Color: 14
Size: 260841 Color: 11

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 449243 Color: 1
Size: 298382 Color: 0
Size: 252376 Color: 18

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 449307 Color: 18
Size: 284700 Color: 8
Size: 265994 Color: 17

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 449348 Color: 10
Size: 277931 Color: 14
Size: 272722 Color: 19

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 449561 Color: 16
Size: 294232 Color: 3
Size: 256208 Color: 4

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 449568 Color: 5
Size: 288612 Color: 9
Size: 261821 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 449629 Color: 8
Size: 289386 Color: 7
Size: 260986 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 449637 Color: 11
Size: 295654 Color: 0
Size: 254710 Color: 10

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 449687 Color: 0
Size: 283087 Color: 14
Size: 267227 Color: 5

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 449739 Color: 10
Size: 284304 Color: 2
Size: 265958 Color: 14

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 449899 Color: 17
Size: 296053 Color: 18
Size: 254049 Color: 8

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 449778 Color: 11
Size: 294501 Color: 12
Size: 255722 Color: 9

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 449794 Color: 10
Size: 275110 Color: 14
Size: 275097 Color: 17

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 449906 Color: 9
Size: 294288 Color: 19
Size: 255807 Color: 11

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 449981 Color: 10
Size: 288836 Color: 17
Size: 261184 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 450005 Color: 12
Size: 296380 Color: 0
Size: 253616 Color: 8

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 450009 Color: 6
Size: 292204 Color: 16
Size: 257788 Color: 10

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 450009 Color: 16
Size: 285673 Color: 8
Size: 264319 Color: 1

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 450025 Color: 18
Size: 284264 Color: 13
Size: 265712 Color: 16

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 450026 Color: 5
Size: 279543 Color: 15
Size: 270432 Color: 17

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 450089 Color: 3
Size: 284343 Color: 1
Size: 265569 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 450129 Color: 3
Size: 284117 Color: 0
Size: 265755 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 450026 Color: 0
Size: 278439 Color: 7
Size: 271536 Color: 11

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 450072 Color: 18
Size: 291165 Color: 2
Size: 258764 Color: 6

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 450119 Color: 7
Size: 287058 Color: 12
Size: 262824 Color: 15

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 450175 Color: 16
Size: 294983 Color: 12
Size: 254843 Color: 1

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 450279 Color: 2
Size: 284256 Color: 18
Size: 265466 Color: 8

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 450344 Color: 6
Size: 285477 Color: 5
Size: 264180 Color: 4

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 450351 Color: 5
Size: 288154 Color: 17
Size: 261496 Color: 10

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 450452 Color: 13
Size: 288951 Color: 5
Size: 260598 Color: 1

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 450545 Color: 3
Size: 282134 Color: 18
Size: 267322 Color: 2

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 450467 Color: 15
Size: 276830 Color: 2
Size: 272704 Color: 6

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 450488 Color: 6
Size: 291734 Color: 5
Size: 257779 Color: 13

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 450546 Color: 16
Size: 284053 Color: 17
Size: 265402 Color: 9

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 450592 Color: 3
Size: 284623 Color: 15
Size: 264786 Color: 9

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 450555 Color: 19
Size: 286435 Color: 8
Size: 263011 Color: 12

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 450614 Color: 12
Size: 283717 Color: 4
Size: 265670 Color: 6

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 450669 Color: 5
Size: 289539 Color: 11
Size: 259793 Color: 7

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 450761 Color: 7
Size: 296796 Color: 5
Size: 252444 Color: 18

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 450774 Color: 19
Size: 294953 Color: 5
Size: 254274 Color: 8

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 450875 Color: 11
Size: 293701 Color: 9
Size: 255425 Color: 3

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 450976 Color: 10
Size: 278622 Color: 0
Size: 270403 Color: 9

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 450976 Color: 19
Size: 279112 Color: 11
Size: 269913 Color: 13

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 451097 Color: 8
Size: 298379 Color: 0
Size: 250525 Color: 9

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 451101 Color: 18
Size: 295240 Color: 18
Size: 253660 Color: 14

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 451162 Color: 7
Size: 275097 Color: 3
Size: 273742 Color: 13

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 451219 Color: 5
Size: 284196 Color: 12
Size: 264586 Color: 12

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 451205 Color: 16
Size: 298770 Color: 15
Size: 250026 Color: 2

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 451292 Color: 9
Size: 285867 Color: 14
Size: 262842 Color: 19

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 451296 Color: 1
Size: 277026 Color: 14
Size: 271679 Color: 16

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 451380 Color: 0
Size: 293166 Color: 9
Size: 255455 Color: 5

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 451472 Color: 8
Size: 286303 Color: 12
Size: 262226 Color: 2

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 451564 Color: 14
Size: 291944 Color: 11
Size: 256493 Color: 9

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 451602 Color: 9
Size: 296057 Color: 15
Size: 252342 Color: 11

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 451630 Color: 0
Size: 276700 Color: 17
Size: 271671 Color: 14

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 452052 Color: 16
Size: 295086 Color: 19
Size: 252863 Color: 11

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 452095 Color: 19
Size: 277331 Color: 19
Size: 270575 Color: 13

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 452139 Color: 10
Size: 291349 Color: 17
Size: 256513 Color: 2

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 452278 Color: 14
Size: 281688 Color: 15
Size: 266035 Color: 7

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 452297 Color: 10
Size: 282146 Color: 2
Size: 265558 Color: 17

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 452338 Color: 11
Size: 292913 Color: 5
Size: 254750 Color: 2

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 452484 Color: 1
Size: 295011 Color: 4
Size: 252506 Color: 4

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 452501 Color: 18
Size: 277403 Color: 16
Size: 270097 Color: 8

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 452502 Color: 2
Size: 278713 Color: 3
Size: 268786 Color: 8

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 452512 Color: 19
Size: 286158 Color: 5
Size: 261331 Color: 19

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 452531 Color: 13
Size: 278556 Color: 10
Size: 268914 Color: 11

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 452546 Color: 13
Size: 294101 Color: 6
Size: 253354 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 452626 Color: 8
Size: 278561 Color: 18
Size: 268814 Color: 9

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 452579 Color: 17
Size: 293894 Color: 7
Size: 253528 Color: 2

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 452642 Color: 4
Size: 290582 Color: 15
Size: 256777 Color: 5

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 452645 Color: 8
Size: 289301 Color: 16
Size: 258055 Color: 10

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 452680 Color: 19
Size: 287879 Color: 1
Size: 259442 Color: 4

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 452818 Color: 0
Size: 277028 Color: 7
Size: 270155 Color: 9

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 452824 Color: 7
Size: 289359 Color: 11
Size: 257818 Color: 3

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 452896 Color: 4
Size: 282358 Color: 11
Size: 264747 Color: 16

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 452919 Color: 8
Size: 286220 Color: 4
Size: 260862 Color: 13

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 452926 Color: 11
Size: 283546 Color: 5
Size: 263529 Color: 4

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 452935 Color: 9
Size: 290993 Color: 14
Size: 256073 Color: 13

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 452951 Color: 11
Size: 296029 Color: 14
Size: 251021 Color: 9

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 452976 Color: 13
Size: 290888 Color: 18
Size: 256137 Color: 17

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 453006 Color: 1
Size: 289394 Color: 10
Size: 257601 Color: 7

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 453037 Color: 8
Size: 296344 Color: 8
Size: 250620 Color: 17

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 453050 Color: 13
Size: 283243 Color: 5
Size: 263708 Color: 15

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 453065 Color: 3
Size: 292530 Color: 1
Size: 254406 Color: 6

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 453106 Color: 4
Size: 287391 Color: 2
Size: 259504 Color: 7

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 453183 Color: 8
Size: 288246 Color: 7
Size: 258572 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 453235 Color: 3
Size: 279732 Color: 1
Size: 267034 Color: 4

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 453256 Color: 0
Size: 280387 Color: 16
Size: 266358 Color: 17

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 453372 Color: 3
Size: 274851 Color: 12
Size: 271778 Color: 5

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 453391 Color: 15
Size: 286235 Color: 9
Size: 260375 Color: 1

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 453425 Color: 15
Size: 295348 Color: 18
Size: 251228 Color: 19

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 453477 Color: 10
Size: 278087 Color: 12
Size: 268437 Color: 15

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 453478 Color: 9
Size: 279614 Color: 15
Size: 266909 Color: 12

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 453499 Color: 11
Size: 287608 Color: 1
Size: 258894 Color: 6

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 453581 Color: 0
Size: 289069 Color: 4
Size: 257351 Color: 17

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 453635 Color: 6
Size: 274610 Color: 5
Size: 271756 Color: 13

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 453667 Color: 19
Size: 289378 Color: 15
Size: 256956 Color: 2

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 453693 Color: 16
Size: 274721 Color: 4
Size: 271587 Color: 18

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 453715 Color: 16
Size: 283735 Color: 0
Size: 262551 Color: 19

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 453735 Color: 15
Size: 283244 Color: 2
Size: 263022 Color: 13

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 453963 Color: 18
Size: 288735 Color: 19
Size: 257303 Color: 11

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 454013 Color: 11
Size: 289418 Color: 15
Size: 256570 Color: 5

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 454167 Color: 14
Size: 276367 Color: 10
Size: 269467 Color: 12

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 454282 Color: 8
Size: 285705 Color: 14
Size: 260014 Color: 7

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 454295 Color: 3
Size: 291979 Color: 19
Size: 253727 Color: 2

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 454358 Color: 10
Size: 273239 Color: 6
Size: 272404 Color: 16

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 454394 Color: 0
Size: 283204 Color: 6
Size: 262403 Color: 11

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 454434 Color: 9
Size: 283608 Color: 5
Size: 261959 Color: 2

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 454505 Color: 8
Size: 290488 Color: 12
Size: 255008 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 454609 Color: 19
Size: 290031 Color: 12
Size: 255361 Color: 14

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 454617 Color: 3
Size: 277870 Color: 18
Size: 267514 Color: 14

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 454693 Color: 8
Size: 278652 Color: 13
Size: 266656 Color: 13

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 454807 Color: 19
Size: 283454 Color: 2
Size: 261740 Color: 13

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 454875 Color: 5
Size: 281988 Color: 19
Size: 263138 Color: 14

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 454818 Color: 3
Size: 282039 Color: 17
Size: 263144 Color: 7

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 454844 Color: 4
Size: 282779 Color: 12
Size: 262378 Color: 7

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 454869 Color: 9
Size: 288046 Color: 16
Size: 257086 Color: 12

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 454941 Color: 15
Size: 291858 Color: 18
Size: 253202 Color: 5

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 454979 Color: 3
Size: 276698 Color: 7
Size: 268324 Color: 1

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 455005 Color: 7
Size: 279655 Color: 11
Size: 265341 Color: 4

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 455099 Color: 7
Size: 283368 Color: 13
Size: 261534 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 455106 Color: 7
Size: 285431 Color: 2
Size: 259464 Color: 6

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 455263 Color: 14
Size: 284255 Color: 6
Size: 260483 Color: 19

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 455272 Color: 1
Size: 292078 Color: 6
Size: 252651 Color: 5

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 455314 Color: 0
Size: 283872 Color: 15
Size: 260815 Color: 18

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 455328 Color: 16
Size: 273852 Color: 17
Size: 270821 Color: 19

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 455329 Color: 2
Size: 293724 Color: 7
Size: 250948 Color: 3

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 455340 Color: 19
Size: 280644 Color: 18
Size: 264017 Color: 9

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 455362 Color: 9
Size: 293174 Color: 6
Size: 251465 Color: 4

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 455371 Color: 0
Size: 292139 Color: 15
Size: 252491 Color: 5

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 455574 Color: 1
Size: 275099 Color: 13
Size: 269328 Color: 9

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 455666 Color: 13
Size: 292254 Color: 14
Size: 252081 Color: 0

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 455726 Color: 2
Size: 278595 Color: 16
Size: 265680 Color: 17

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 455762 Color: 8
Size: 279692 Color: 15
Size: 264547 Color: 4

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 455778 Color: 10
Size: 275021 Color: 8
Size: 269202 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 455781 Color: 2
Size: 274282 Color: 10
Size: 269938 Color: 18

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 455787 Color: 9
Size: 286238 Color: 3
Size: 257976 Color: 5

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 455806 Color: 13
Size: 277960 Color: 9
Size: 266235 Color: 15

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 455819 Color: 19
Size: 293387 Color: 14
Size: 250795 Color: 2

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 456098 Color: 4
Size: 291361 Color: 7
Size: 252542 Color: 5

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 456246 Color: 0
Size: 283430 Color: 12
Size: 260325 Color: 17

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 456281 Color: 15
Size: 277754 Color: 4
Size: 265966 Color: 8

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 456287 Color: 10
Size: 282635 Color: 1
Size: 261079 Color: 14

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 456348 Color: 10
Size: 291130 Color: 18
Size: 252523 Color: 19

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 456517 Color: 18
Size: 280514 Color: 4
Size: 262970 Color: 10

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 456611 Color: 6
Size: 290901 Color: 4
Size: 252489 Color: 19

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 456666 Color: 16
Size: 287222 Color: 1
Size: 256113 Color: 3

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 456844 Color: 2
Size: 274069 Color: 13
Size: 269088 Color: 1

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 456930 Color: 2
Size: 274905 Color: 13
Size: 268166 Color: 10

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 457013 Color: 19
Size: 275768 Color: 2
Size: 267220 Color: 18

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 457034 Color: 17
Size: 278737 Color: 12
Size: 264230 Color: 1

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 457174 Color: 5
Size: 277471 Color: 17
Size: 265356 Color: 2

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 457068 Color: 10
Size: 278127 Color: 6
Size: 264806 Color: 15

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 457082 Color: 16
Size: 282637 Color: 11
Size: 260282 Color: 14

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 457267 Color: 8
Size: 279438 Color: 0
Size: 263296 Color: 4

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 457429 Color: 14
Size: 273210 Color: 17
Size: 269362 Color: 5

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 457503 Color: 9
Size: 283238 Color: 0
Size: 259260 Color: 17

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 457600 Color: 15
Size: 290097 Color: 17
Size: 252304 Color: 2

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 457666 Color: 14
Size: 282747 Color: 13
Size: 259588 Color: 11

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 457912 Color: 10
Size: 278944 Color: 0
Size: 263145 Color: 16

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 457932 Color: 3
Size: 285539 Color: 5
Size: 256530 Color: 15

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 457964 Color: 4
Size: 273831 Color: 11
Size: 268206 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 457969 Color: 8
Size: 289121 Color: 12
Size: 252911 Color: 1

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 457985 Color: 8
Size: 291016 Color: 13
Size: 251000 Color: 8

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 457997 Color: 2
Size: 288363 Color: 9
Size: 253641 Color: 12

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 458111 Color: 14
Size: 283702 Color: 9
Size: 258188 Color: 15

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 458178 Color: 8
Size: 285061 Color: 10
Size: 256762 Color: 5

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 458222 Color: 8
Size: 291190 Color: 2
Size: 250589 Color: 3

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 458253 Color: 19
Size: 289791 Color: 16
Size: 251957 Color: 15

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 458261 Color: 14
Size: 272430 Color: 6
Size: 269310 Color: 9

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 458506 Color: 0
Size: 282646 Color: 19
Size: 258849 Color: 11

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 458533 Color: 8
Size: 282884 Color: 7
Size: 258584 Color: 9

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 458609 Color: 17
Size: 281558 Color: 19
Size: 259834 Color: 16

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 458738 Color: 10
Size: 285318 Color: 19
Size: 255945 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 458762 Color: 8
Size: 289574 Color: 1
Size: 251665 Color: 12

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 458808 Color: 15
Size: 279255 Color: 2
Size: 261938 Color: 5

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 458910 Color: 7
Size: 275450 Color: 7
Size: 265641 Color: 13

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 459045 Color: 14
Size: 272224 Color: 15
Size: 268732 Color: 10

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 459159 Color: 12
Size: 273894 Color: 14
Size: 266948 Color: 8

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 459193 Color: 8
Size: 280874 Color: 3
Size: 259934 Color: 2

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 459290 Color: 11
Size: 276535 Color: 3
Size: 264176 Color: 5

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 459348 Color: 2
Size: 286852 Color: 1
Size: 253801 Color: 15

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 459415 Color: 16
Size: 285737 Color: 4
Size: 254849 Color: 1

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 459427 Color: 16
Size: 287224 Color: 19
Size: 253350 Color: 18

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 459755 Color: 7
Size: 280093 Color: 0
Size: 260153 Color: 6

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 459858 Color: 13
Size: 275060 Color: 1
Size: 265083 Color: 12

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 459970 Color: 9
Size: 285193 Color: 3
Size: 254838 Color: 16

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 460039 Color: 13
Size: 271051 Color: 5
Size: 268911 Color: 15

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 460197 Color: 2
Size: 276435 Color: 0
Size: 263369 Color: 15

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 460208 Color: 6
Size: 270611 Color: 18
Size: 269182 Color: 13

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 460256 Color: 10
Size: 278598 Color: 17
Size: 261147 Color: 4

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 460335 Color: 8
Size: 287641 Color: 19
Size: 252025 Color: 14

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 460377 Color: 8
Size: 270833 Color: 18
Size: 268791 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 460481 Color: 6
Size: 278041 Color: 7
Size: 261479 Color: 8

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 460615 Color: 5
Size: 284120 Color: 11
Size: 255266 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 460530 Color: 3
Size: 284240 Color: 7
Size: 255231 Color: 10

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 460758 Color: 19
Size: 284207 Color: 0
Size: 255036 Color: 2

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 460800 Color: 16
Size: 285712 Color: 1
Size: 253489 Color: 16

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 460862 Color: 13
Size: 287886 Color: 3
Size: 251253 Color: 1

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 460883 Color: 14
Size: 285639 Color: 10
Size: 253479 Color: 3

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 460889 Color: 4
Size: 279833 Color: 3
Size: 259279 Color: 17

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 461198 Color: 2
Size: 274874 Color: 14
Size: 263929 Color: 2

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 461214 Color: 12
Size: 282109 Color: 0
Size: 256678 Color: 5

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 461250 Color: 19
Size: 276982 Color: 19
Size: 261769 Color: 3

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 461284 Color: 13
Size: 280179 Color: 17
Size: 258538 Color: 1

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 461300 Color: 12
Size: 284633 Color: 18
Size: 254068 Color: 6

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 461356 Color: 9
Size: 280954 Color: 18
Size: 257691 Color: 1

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 461513 Color: 10
Size: 279368 Color: 16
Size: 259120 Color: 5

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 461524 Color: 8
Size: 284112 Color: 10
Size: 254365 Color: 4

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 461609 Color: 18
Size: 277637 Color: 13
Size: 260755 Color: 15

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 461722 Color: 8
Size: 280475 Color: 14
Size: 257804 Color: 7

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 461773 Color: 18
Size: 273747 Color: 19
Size: 264481 Color: 4

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 461871 Color: 10
Size: 282124 Color: 4
Size: 256006 Color: 8

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 461885 Color: 17
Size: 271013 Color: 13
Size: 267103 Color: 2

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 461887 Color: 9
Size: 271427 Color: 15
Size: 266687 Color: 10

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 461916 Color: 12
Size: 275291 Color: 4
Size: 262794 Color: 3

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 461970 Color: 9
Size: 284578 Color: 11
Size: 253453 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 461973 Color: 0
Size: 287179 Color: 14
Size: 250849 Color: 17

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 462267 Color: 5
Size: 275902 Color: 3
Size: 261832 Color: 4

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 462195 Color: 14
Size: 278797 Color: 15
Size: 259009 Color: 16

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 462226 Color: 13
Size: 287279 Color: 6
Size: 250496 Color: 14

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 462262 Color: 0
Size: 272792 Color: 2
Size: 264947 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 462263 Color: 8
Size: 286362 Color: 8
Size: 251376 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 462317 Color: 17
Size: 271112 Color: 16
Size: 266572 Color: 6

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 462328 Color: 1
Size: 284129 Color: 13
Size: 253544 Color: 16

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 462608 Color: 5
Size: 274340 Color: 10
Size: 263053 Color: 8

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 462432 Color: 19
Size: 285621 Color: 17
Size: 251948 Color: 18

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 462496 Color: 18
Size: 286503 Color: 9
Size: 251002 Color: 9

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 462505 Color: 18
Size: 282998 Color: 7
Size: 254498 Color: 6

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 462536 Color: 2
Size: 277535 Color: 16
Size: 259930 Color: 18

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 462908 Color: 5
Size: 282192 Color: 2
Size: 254901 Color: 13

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 462649 Color: 14
Size: 277389 Color: 3
Size: 259963 Color: 18

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 462805 Color: 6
Size: 276158 Color: 8
Size: 261038 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 463023 Color: 5
Size: 275388 Color: 1
Size: 261590 Color: 13

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 462962 Color: 11
Size: 275303 Color: 2
Size: 261736 Color: 1

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 463068 Color: 18
Size: 279800 Color: 13
Size: 257133 Color: 7

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 463257 Color: 9
Size: 270926 Color: 11
Size: 265818 Color: 5

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 463679 Color: 8
Size: 280369 Color: 17
Size: 255953 Color: 15

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 463778 Color: 15
Size: 279623 Color: 6
Size: 256600 Color: 4

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 463793 Color: 18
Size: 281795 Color: 8
Size: 254413 Color: 18

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 463857 Color: 19
Size: 268267 Color: 10
Size: 267877 Color: 4

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 463885 Color: 10
Size: 272064 Color: 8
Size: 264052 Color: 7

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 463887 Color: 18
Size: 277223 Color: 19
Size: 258891 Color: 16

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 464150 Color: 5
Size: 269689 Color: 3
Size: 266162 Color: 17

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 464055 Color: 3
Size: 273547 Color: 14
Size: 262399 Color: 11

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 464112 Color: 17
Size: 275461 Color: 8
Size: 260428 Color: 19

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 464207 Color: 4
Size: 278163 Color: 17
Size: 257631 Color: 5

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 464244 Color: 3
Size: 276740 Color: 16
Size: 259017 Color: 8

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 464296 Color: 18
Size: 276527 Color: 7
Size: 259178 Color: 14

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 464356 Color: 16
Size: 283132 Color: 10
Size: 252513 Color: 10

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 464363 Color: 11
Size: 279591 Color: 3
Size: 256047 Color: 17

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 464365 Color: 3
Size: 275119 Color: 14
Size: 260517 Color: 15

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 464367 Color: 18
Size: 275839 Color: 11
Size: 259795 Color: 6

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 464434 Color: 5
Size: 279883 Color: 8
Size: 255684 Color: 2

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 464414 Color: 7
Size: 272168 Color: 16
Size: 263419 Color: 11

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 464467 Color: 0
Size: 280352 Color: 1
Size: 255182 Color: 8

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 464529 Color: 8
Size: 273063 Color: 19
Size: 262409 Color: 17

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 464587 Color: 10
Size: 276636 Color: 19
Size: 258778 Color: 4

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 464592 Color: 12
Size: 279270 Color: 2
Size: 256139 Color: 5

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 464664 Color: 4
Size: 282896 Color: 15
Size: 252441 Color: 7

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 464846 Color: 2
Size: 282666 Color: 8
Size: 252489 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 464867 Color: 18
Size: 275144 Color: 12
Size: 259990 Color: 10

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 464885 Color: 6
Size: 271359 Color: 17
Size: 263757 Color: 7

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 465128 Color: 6
Size: 271989 Color: 3
Size: 262884 Color: 15

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 465144 Color: 6
Size: 280137 Color: 13
Size: 254720 Color: 18

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 465197 Color: 11
Size: 282149 Color: 12
Size: 252655 Color: 16

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 465213 Color: 6
Size: 279503 Color: 11
Size: 255285 Color: 1

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 465238 Color: 9
Size: 269820 Color: 5
Size: 264943 Color: 3

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 465253 Color: 10
Size: 277522 Color: 17
Size: 257226 Color: 17

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 465259 Color: 16
Size: 269045 Color: 15
Size: 265697 Color: 13

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 465504 Color: 14
Size: 281894 Color: 6
Size: 252603 Color: 19

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 465513 Color: 10
Size: 269176 Color: 17
Size: 265312 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 465761 Color: 12
Size: 277709 Color: 10
Size: 256531 Color: 3

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 465845 Color: 6
Size: 271130 Color: 3
Size: 263026 Color: 3

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 465856 Color: 0
Size: 284044 Color: 2
Size: 250101 Color: 15

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 465870 Color: 19
Size: 277182 Color: 17
Size: 256949 Color: 17

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 465916 Color: 3
Size: 280083 Color: 18
Size: 254002 Color: 8

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 466193 Color: 7
Size: 272879 Color: 4
Size: 260929 Color: 4

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 466210 Color: 11
Size: 283415 Color: 0
Size: 250376 Color: 12

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 466236 Color: 17
Size: 275205 Color: 3
Size: 258560 Color: 6

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 466241 Color: 15
Size: 278382 Color: 14
Size: 255378 Color: 2

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 466386 Color: 2
Size: 282948 Color: 15
Size: 250667 Color: 6

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 466466 Color: 0
Size: 281293 Color: 15
Size: 252242 Color: 18

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 466468 Color: 10
Size: 270170 Color: 18
Size: 263363 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 466528 Color: 16
Size: 274637 Color: 3
Size: 258836 Color: 1

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 466745 Color: 5
Size: 271267 Color: 17
Size: 261989 Color: 10

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 466751 Color: 0
Size: 272628 Color: 3
Size: 260622 Color: 8

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 466829 Color: 3
Size: 269064 Color: 2
Size: 264108 Color: 6

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 466874 Color: 3
Size: 280843 Color: 8
Size: 252284 Color: 10

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 466875 Color: 1
Size: 272989 Color: 8
Size: 260137 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 466933 Color: 14
Size: 269398 Color: 5
Size: 263670 Color: 16

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 467050 Color: 15
Size: 276067 Color: 8
Size: 256884 Color: 7

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 467071 Color: 9
Size: 276763 Color: 15
Size: 256167 Color: 5

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 467245 Color: 13
Size: 276650 Color: 15
Size: 256106 Color: 17

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 467246 Color: 11
Size: 282402 Color: 4
Size: 250353 Color: 7

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 467263 Color: 13
Size: 282598 Color: 9
Size: 250140 Color: 18

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 467284 Color: 8
Size: 280972 Color: 5
Size: 251745 Color: 3

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 467287 Color: 7
Size: 270103 Color: 3
Size: 262611 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 467328 Color: 7
Size: 271767 Color: 12
Size: 260906 Color: 2

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 467468 Color: 14
Size: 279965 Color: 16
Size: 252568 Color: 11

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 467552 Color: 14
Size: 271382 Color: 10
Size: 261067 Color: 7

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 467628 Color: 17
Size: 281549 Color: 3
Size: 250824 Color: 9

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 467637 Color: 17
Size: 274444 Color: 14
Size: 257920 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 467712 Color: 19
Size: 267040 Color: 2
Size: 265249 Color: 12

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 467716 Color: 12
Size: 268679 Color: 7
Size: 263606 Color: 15

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 467766 Color: 11
Size: 273378 Color: 16
Size: 258857 Color: 7

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 467832 Color: 16
Size: 280656 Color: 16
Size: 251513 Color: 12

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 468209 Color: 4
Size: 276944 Color: 15
Size: 254848 Color: 3

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 468226 Color: 9
Size: 268214 Color: 10
Size: 263561 Color: 9

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 468248 Color: 6
Size: 280085 Color: 11
Size: 251668 Color: 17

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 468254 Color: 5
Size: 276949 Color: 8
Size: 254798 Color: 16

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 468320 Color: 4
Size: 267964 Color: 11
Size: 263717 Color: 8

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 468423 Color: 10
Size: 266602 Color: 1
Size: 264976 Color: 15

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 468431 Color: 16
Size: 277029 Color: 5
Size: 254541 Color: 2

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 468439 Color: 16
Size: 281538 Color: 3
Size: 250024 Color: 19

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 468499 Color: 18
Size: 277176 Color: 11
Size: 254326 Color: 9

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 468539 Color: 16
Size: 277712 Color: 18
Size: 253750 Color: 10

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 468605 Color: 5
Size: 272933 Color: 10
Size: 258463 Color: 13

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 468633 Color: 11
Size: 271547 Color: 3
Size: 259821 Color: 7

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 468709 Color: 2
Size: 270339 Color: 0
Size: 260953 Color: 9

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 468749 Color: 12
Size: 274220 Color: 9
Size: 257032 Color: 6

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 468927 Color: 15
Size: 268417 Color: 17
Size: 262657 Color: 6

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 468948 Color: 0
Size: 268695 Color: 16
Size: 262358 Color: 3

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 469161 Color: 10
Size: 271183 Color: 9
Size: 259657 Color: 18

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 469185 Color: 2
Size: 279852 Color: 7
Size: 250964 Color: 18

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 469216 Color: 1
Size: 269278 Color: 4
Size: 261507 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 469232 Color: 12
Size: 279557 Color: 2
Size: 251212 Color: 2

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 469467 Color: 13
Size: 277557 Color: 4
Size: 252977 Color: 1

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 469491 Color: 8
Size: 268556 Color: 12
Size: 261954 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 469517 Color: 0
Size: 279577 Color: 3
Size: 250907 Color: 6

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 469546 Color: 16
Size: 266680 Color: 7
Size: 263775 Color: 4

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 469563 Color: 8
Size: 270476 Color: 12
Size: 259962 Color: 1

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 469565 Color: 15
Size: 272842 Color: 11
Size: 257594 Color: 2

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 469656 Color: 8
Size: 268552 Color: 16
Size: 261793 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 469679 Color: 12
Size: 280023 Color: 0
Size: 250299 Color: 1

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 469696 Color: 8
Size: 267304 Color: 3
Size: 263001 Color: 1

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 469909 Color: 17
Size: 269883 Color: 11
Size: 260209 Color: 19

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 469930 Color: 19
Size: 269155 Color: 5
Size: 260916 Color: 9

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 469950 Color: 17
Size: 272819 Color: 8
Size: 257232 Color: 5

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 469971 Color: 8
Size: 270753 Color: 14
Size: 259277 Color: 5

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 470051 Color: 3
Size: 271922 Color: 6
Size: 258028 Color: 6

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 470058 Color: 0
Size: 274094 Color: 17
Size: 255849 Color: 8

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 470109 Color: 7
Size: 271528 Color: 19
Size: 258364 Color: 11

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 470121 Color: 18
Size: 272038 Color: 14
Size: 257842 Color: 2

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 470159 Color: 18
Size: 278610 Color: 2
Size: 251232 Color: 18

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 470194 Color: 2
Size: 265936 Color: 5
Size: 263871 Color: 8

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 470228 Color: 0
Size: 265495 Color: 9
Size: 264278 Color: 3

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 470241 Color: 10
Size: 273505 Color: 16
Size: 256255 Color: 8

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 470279 Color: 4
Size: 273381 Color: 15
Size: 256341 Color: 10

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 470299 Color: 10
Size: 267489 Color: 2
Size: 262213 Color: 9

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 470400 Color: 12
Size: 272161 Color: 11
Size: 257440 Color: 13

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 470404 Color: 12
Size: 265816 Color: 18
Size: 263781 Color: 19

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 470414 Color: 19
Size: 278056 Color: 1
Size: 251531 Color: 3

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 470500 Color: 14
Size: 277374 Color: 10
Size: 252127 Color: 4

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 470509 Color: 11
Size: 265486 Color: 0
Size: 264006 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 470509 Color: 11
Size: 272468 Color: 16
Size: 257024 Color: 1

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 470571 Color: 6
Size: 270391 Color: 2
Size: 259039 Color: 6

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 470547 Color: 5
Size: 277474 Color: 9
Size: 251980 Color: 13

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 470592 Color: 15
Size: 277794 Color: 3
Size: 251615 Color: 4

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 470636 Color: 11
Size: 265152 Color: 4
Size: 264213 Color: 2

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 470653 Color: 12
Size: 268706 Color: 4
Size: 260642 Color: 14

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 470672 Color: 11
Size: 272247 Color: 15
Size: 257082 Color: 18

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 470705 Color: 8
Size: 279279 Color: 15
Size: 250017 Color: 0

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 470714 Color: 6
Size: 269582 Color: 15
Size: 259705 Color: 12

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 471079 Color: 9
Size: 271603 Color: 12
Size: 257319 Color: 10

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 471142 Color: 0
Size: 274255 Color: 7
Size: 254604 Color: 7

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 471149 Color: 2
Size: 266988 Color: 12
Size: 261864 Color: 17

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 471244 Color: 5
Size: 266208 Color: 10
Size: 262549 Color: 3

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 471347 Color: 7
Size: 269912 Color: 2
Size: 258742 Color: 16

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 471381 Color: 16
Size: 268304 Color: 19
Size: 260316 Color: 8

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 471459 Color: 16
Size: 268624 Color: 12
Size: 259918 Color: 11

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 471471 Color: 10
Size: 268709 Color: 14
Size: 259821 Color: 17

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 471519 Color: 13
Size: 275400 Color: 8
Size: 253082 Color: 19

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 471787 Color: 18
Size: 269087 Color: 3
Size: 259127 Color: 8

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 471799 Color: 9
Size: 264928 Color: 6
Size: 263274 Color: 13

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 471838 Color: 9
Size: 277161 Color: 18
Size: 251002 Color: 2

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 471839 Color: 0
Size: 277798 Color: 5
Size: 250364 Color: 14

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 471991 Color: 1
Size: 264899 Color: 6
Size: 263111 Color: 4

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 472016 Color: 3
Size: 273767 Color: 7
Size: 254218 Color: 8

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 472008 Color: 17
Size: 276437 Color: 8
Size: 251556 Color: 10

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 472025 Color: 7
Size: 276417 Color: 0
Size: 251559 Color: 4

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 472039 Color: 9
Size: 270477 Color: 18
Size: 257485 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 472060 Color: 19
Size: 277401 Color: 2
Size: 250540 Color: 4

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 472138 Color: 6
Size: 276820 Color: 19
Size: 251043 Color: 4

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 472153 Color: 15
Size: 269027 Color: 17
Size: 258821 Color: 2

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 472157 Color: 19
Size: 264711 Color: 3
Size: 263133 Color: 4

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 472210 Color: 0
Size: 272182 Color: 4
Size: 255609 Color: 16

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 472248 Color: 13
Size: 275605 Color: 14
Size: 252148 Color: 17

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 472268 Color: 7
Size: 273773 Color: 11
Size: 253960 Color: 10

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 472306 Color: 2
Size: 272235 Color: 10
Size: 255460 Color: 18

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 472422 Color: 6
Size: 271555 Color: 4
Size: 256024 Color: 16

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 472429 Color: 17
Size: 268023 Color: 3
Size: 259549 Color: 14

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 472612 Color: 11
Size: 268737 Color: 16
Size: 258652 Color: 12

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 472648 Color: 0
Size: 275781 Color: 6
Size: 251572 Color: 5

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 472658 Color: 7
Size: 271339 Color: 4
Size: 256004 Color: 8

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 472687 Color: 13
Size: 274770 Color: 9
Size: 252544 Color: 14

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 472746 Color: 12
Size: 267403 Color: 15
Size: 259852 Color: 3

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 472894 Color: 4
Size: 267527 Color: 8
Size: 259580 Color: 13

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 473047 Color: 9
Size: 264365 Color: 1
Size: 262589 Color: 1

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 473151 Color: 1
Size: 265419 Color: 12
Size: 261431 Color: 4

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 473188 Color: 18
Size: 274018 Color: 16
Size: 252795 Color: 15

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 473363 Color: 5
Size: 268419 Color: 1
Size: 258219 Color: 18

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 473524 Color: 3
Size: 270602 Color: 7
Size: 255875 Color: 14

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 473456 Color: 13
Size: 272820 Color: 0
Size: 253725 Color: 14

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 473500 Color: 17
Size: 275928 Color: 18
Size: 250573 Color: 4

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 473522 Color: 18
Size: 268521 Color: 2
Size: 257958 Color: 6

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 473698 Color: 16
Size: 268659 Color: 2
Size: 257644 Color: 13

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 473762 Color: 19
Size: 270457 Color: 3
Size: 255782 Color: 8

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 473843 Color: 14
Size: 272159 Color: 2
Size: 253999 Color: 4

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 473853 Color: 18
Size: 274097 Color: 8
Size: 252051 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 473899 Color: 11
Size: 274692 Color: 13
Size: 251410 Color: 6

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 473905 Color: 17
Size: 275849 Color: 14
Size: 250247 Color: 8

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 473939 Color: 10
Size: 264688 Color: 8
Size: 261374 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 473994 Color: 14
Size: 265521 Color: 8
Size: 260486 Color: 3

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 474071 Color: 11
Size: 275410 Color: 9
Size: 250520 Color: 19

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 474122 Color: 13
Size: 263393 Color: 10
Size: 262486 Color: 12

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 474128 Color: 2
Size: 264069 Color: 18
Size: 261804 Color: 1

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 474178 Color: 13
Size: 273734 Color: 6
Size: 252089 Color: 14

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 474189 Color: 4
Size: 263846 Color: 15
Size: 261966 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 474217 Color: 7
Size: 270699 Color: 12
Size: 255085 Color: 3

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 474242 Color: 2
Size: 274725 Color: 18
Size: 251034 Color: 13

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 474466 Color: 4
Size: 271415 Color: 9
Size: 254120 Color: 11

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 474544 Color: 2
Size: 269256 Color: 6
Size: 256201 Color: 6

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 474595 Color: 14
Size: 269528 Color: 12
Size: 255878 Color: 10

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 474741 Color: 1
Size: 264708 Color: 11
Size: 260552 Color: 12

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 474826 Color: 16
Size: 271329 Color: 5
Size: 253846 Color: 1

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 475169 Color: 2
Size: 271140 Color: 9
Size: 253692 Color: 4

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 475241 Color: 16
Size: 268334 Color: 17
Size: 256426 Color: 19

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 475243 Color: 13
Size: 266754 Color: 1
Size: 258004 Color: 14

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 475290 Color: 5
Size: 267617 Color: 3
Size: 257094 Color: 10

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 475394 Color: 12
Size: 273107 Color: 10
Size: 251500 Color: 9

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 475416 Color: 13
Size: 266768 Color: 1
Size: 257817 Color: 5

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 475519 Color: 13
Size: 268227 Color: 1
Size: 256255 Color: 6

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 475537 Color: 12
Size: 266300 Color: 19
Size: 258164 Color: 11

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 475561 Color: 11
Size: 271849 Color: 10
Size: 252591 Color: 6

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 475605 Color: 14
Size: 264458 Color: 3
Size: 259938 Color: 15

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 475664 Color: 16
Size: 273240 Color: 11
Size: 251097 Color: 4

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 475674 Color: 11
Size: 265703 Color: 10
Size: 258624 Color: 7

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 475877 Color: 1
Size: 269307 Color: 8
Size: 254817 Color: 10

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 475911 Color: 18
Size: 273339 Color: 5
Size: 250751 Color: 12

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 475950 Color: 5
Size: 268629 Color: 11
Size: 255422 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 476068 Color: 11
Size: 272350 Color: 5
Size: 251583 Color: 14

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 476080 Color: 14
Size: 273553 Color: 8
Size: 250368 Color: 7

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 476102 Color: 10
Size: 263197 Color: 18
Size: 260702 Color: 8

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 476168 Color: 15
Size: 269317 Color: 7
Size: 254516 Color: 18

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 476369 Color: 3
Size: 267645 Color: 1
Size: 255987 Color: 2

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 476445 Color: 13
Size: 269827 Color: 14
Size: 253729 Color: 11

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 476675 Color: 15
Size: 272379 Color: 13
Size: 250947 Color: 7

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 476792 Color: 16
Size: 269326 Color: 8
Size: 253883 Color: 4

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 476778 Color: 10
Size: 264364 Color: 19
Size: 258859 Color: 14

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 476792 Color: 3
Size: 265565 Color: 18
Size: 257644 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 476820 Color: 15
Size: 264832 Color: 14
Size: 258349 Color: 7

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 476849 Color: 12
Size: 262946 Color: 1
Size: 260206 Color: 10

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 477007 Color: 2
Size: 266115 Color: 15
Size: 256879 Color: 3

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 477061 Color: 5
Size: 268550 Color: 14
Size: 254390 Color: 12

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 477090 Color: 8
Size: 272638 Color: 11
Size: 250273 Color: 16

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 477096 Color: 5
Size: 264273 Color: 17
Size: 258632 Color: 10

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 477137 Color: 2
Size: 269086 Color: 0
Size: 253778 Color: 7

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 477261 Color: 5
Size: 266199 Color: 18
Size: 256541 Color: 1

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 477353 Color: 1
Size: 270861 Color: 5
Size: 251787 Color: 15

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 477407 Color: 3
Size: 271057 Color: 9
Size: 251537 Color: 1

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 477382 Color: 0
Size: 270892 Color: 17
Size: 251727 Color: 16

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 477389 Color: 11
Size: 272408 Color: 2
Size: 250204 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 477407 Color: 1
Size: 271310 Color: 10
Size: 251284 Color: 9

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 477451 Color: 19
Size: 269776 Color: 11
Size: 252774 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 477591 Color: 9
Size: 266769 Color: 15
Size: 255641 Color: 13

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 477695 Color: 19
Size: 265620 Color: 13
Size: 256686 Color: 5

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 477869 Color: 17
Size: 268944 Color: 15
Size: 253188 Color: 18

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 477894 Color: 14
Size: 262791 Color: 19
Size: 259316 Color: 16

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 477897 Color: 11
Size: 264573 Color: 5
Size: 257531 Color: 18

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 477944 Color: 8
Size: 261883 Color: 2
Size: 260174 Color: 13

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 478104 Color: 3
Size: 267834 Color: 19
Size: 254063 Color: 16

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 478263 Color: 11
Size: 270115 Color: 16
Size: 251623 Color: 1

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 478411 Color: 4
Size: 270993 Color: 2
Size: 250597 Color: 19

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 478530 Color: 9
Size: 264221 Color: 0
Size: 257250 Color: 7

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 478534 Color: 15
Size: 268837 Color: 13
Size: 252630 Color: 7

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 478635 Color: 4
Size: 266723 Color: 9
Size: 254643 Color: 6

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 478636 Color: 4
Size: 263896 Color: 15
Size: 257469 Color: 2

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 478759 Color: 4
Size: 267652 Color: 8
Size: 253590 Color: 18

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 478840 Color: 14
Size: 262103 Color: 7
Size: 259058 Color: 18

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 479139 Color: 3
Size: 264081 Color: 10
Size: 256781 Color: 11

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 479208 Color: 3
Size: 266963 Color: 8
Size: 253830 Color: 18

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 479164 Color: 6
Size: 268298 Color: 9
Size: 252539 Color: 6

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 479291 Color: 19
Size: 269795 Color: 1
Size: 250915 Color: 5

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 479332 Color: 11
Size: 265626 Color: 4
Size: 255043 Color: 3

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 479395 Color: 14
Size: 267220 Color: 14
Size: 253386 Color: 13

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 479476 Color: 7
Size: 262469 Color: 16
Size: 258056 Color: 2

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 479476 Color: 5
Size: 268878 Color: 0
Size: 251647 Color: 14

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 479547 Color: 8
Size: 260994 Color: 14
Size: 259460 Color: 15

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 479608 Color: 16
Size: 270127 Color: 19
Size: 250266 Color: 11

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 479649 Color: 6
Size: 260228 Color: 1
Size: 260124 Color: 11

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 479743 Color: 3
Size: 269865 Color: 15
Size: 250393 Color: 8

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 479694 Color: 4
Size: 261713 Color: 1
Size: 258594 Color: 13

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 479887 Color: 14
Size: 265669 Color: 9
Size: 254445 Color: 12

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 480148 Color: 15
Size: 261420 Color: 18
Size: 258433 Color: 18

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 480191 Color: 6
Size: 263993 Color: 10
Size: 255817 Color: 3

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 480384 Color: 17
Size: 263473 Color: 15
Size: 256144 Color: 1

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 480404 Color: 1
Size: 269586 Color: 16
Size: 250011 Color: 8

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 480490 Color: 7
Size: 261207 Color: 15
Size: 258304 Color: 1

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 480496 Color: 9
Size: 269206 Color: 5
Size: 250299 Color: 6

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 480627 Color: 13
Size: 260684 Color: 9
Size: 258690 Color: 10

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 480643 Color: 14
Size: 269139 Color: 1
Size: 250219 Color: 4

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 480767 Color: 3
Size: 268008 Color: 12
Size: 251226 Color: 11

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 480685 Color: 15
Size: 262507 Color: 14
Size: 256809 Color: 8

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 480751 Color: 5
Size: 265853 Color: 14
Size: 253397 Color: 10

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 480778 Color: 7
Size: 260921 Color: 14
Size: 258302 Color: 7

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 480785 Color: 8
Size: 265769 Color: 10
Size: 253447 Color: 14

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 480823 Color: 3
Size: 268912 Color: 19
Size: 250266 Color: 15

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 480791 Color: 4
Size: 263890 Color: 15
Size: 255320 Color: 16

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 480797 Color: 16
Size: 261122 Color: 1
Size: 258082 Color: 18

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 480798 Color: 2
Size: 269109 Color: 9
Size: 250094 Color: 9

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 480855 Color: 9
Size: 264416 Color: 14
Size: 254730 Color: 5

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 480869 Color: 17
Size: 268923 Color: 19
Size: 250209 Color: 9

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 480895 Color: 13
Size: 262265 Color: 19
Size: 256841 Color: 3

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 480923 Color: 17
Size: 266569 Color: 3
Size: 252509 Color: 11

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 481101 Color: 2
Size: 262107 Color: 14
Size: 256793 Color: 10

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 481146 Color: 10
Size: 261563 Color: 11
Size: 257292 Color: 14

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 481147 Color: 10
Size: 267620 Color: 2
Size: 251234 Color: 17

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 481181 Color: 3
Size: 260066 Color: 14
Size: 258754 Color: 8

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 481159 Color: 16
Size: 259789 Color: 10
Size: 259053 Color: 12

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 481171 Color: 18
Size: 260929 Color: 5
Size: 257901 Color: 10

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 481330 Color: 11
Size: 265548 Color: 15
Size: 253123 Color: 12

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 481392 Color: 13
Size: 260238 Color: 10
Size: 258371 Color: 7

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 481480 Color: 5
Size: 267528 Color: 10
Size: 250993 Color: 19

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 481722 Color: 10
Size: 260346 Color: 1
Size: 257933 Color: 9

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 481820 Color: 14
Size: 263764 Color: 5
Size: 254417 Color: 18

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 481974 Color: 13
Size: 261936 Color: 6
Size: 256091 Color: 12

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 482384 Color: 3
Size: 261686 Color: 1
Size: 255931 Color: 0

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 482339 Color: 6
Size: 266470 Color: 12
Size: 251192 Color: 19

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 482361 Color: 11
Size: 260504 Color: 2
Size: 257136 Color: 16

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 482432 Color: 0
Size: 261224 Color: 0
Size: 256345 Color: 3

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 482441 Color: 0
Size: 259440 Color: 13
Size: 258120 Color: 18

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 482502 Color: 2
Size: 261241 Color: 15
Size: 256258 Color: 9

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 482625 Color: 7
Size: 258781 Color: 19
Size: 258595 Color: 9

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 482684 Color: 4
Size: 260863 Color: 9
Size: 256454 Color: 11

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 482866 Color: 0
Size: 260647 Color: 8
Size: 256488 Color: 16

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 482919 Color: 12
Size: 259148 Color: 3
Size: 257934 Color: 11

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 482935 Color: 7
Size: 261986 Color: 18
Size: 255080 Color: 6

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 482939 Color: 14
Size: 258744 Color: 11
Size: 258318 Color: 16

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 482976 Color: 7
Size: 262074 Color: 18
Size: 254951 Color: 11

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 483158 Color: 6
Size: 259604 Color: 7
Size: 257239 Color: 18

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 483246 Color: 18
Size: 265991 Color: 2
Size: 250764 Color: 5

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 483304 Color: 1
Size: 264457 Color: 0
Size: 252240 Color: 3

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 483317 Color: 5
Size: 260035 Color: 0
Size: 256649 Color: 5

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 483336 Color: 16
Size: 263049 Color: 6
Size: 253616 Color: 4

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 483337 Color: 13
Size: 259154 Color: 13
Size: 257510 Color: 1

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 483675 Color: 18
Size: 259196 Color: 12
Size: 257130 Color: 15

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 483718 Color: 5
Size: 266090 Color: 11
Size: 250193 Color: 2

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 483724 Color: 10
Size: 262849 Color: 3
Size: 253428 Color: 16

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 484038 Color: 14
Size: 260820 Color: 1
Size: 255143 Color: 10

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 484043 Color: 4
Size: 262282 Color: 16
Size: 253676 Color: 19

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 484061 Color: 10
Size: 260335 Color: 13
Size: 255605 Color: 11

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 484130 Color: 9
Size: 265692 Color: 10
Size: 250179 Color: 14

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 484199 Color: 15
Size: 265638 Color: 17
Size: 250164 Color: 10

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 484351 Color: 7
Size: 261024 Color: 2
Size: 254626 Color: 6

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 484470 Color: 10
Size: 263234 Color: 6
Size: 252297 Color: 8

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 484658 Color: 11
Size: 261520 Color: 9
Size: 253823 Color: 10

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 484869 Color: 1
Size: 261741 Color: 4
Size: 253391 Color: 12

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 485143 Color: 7
Size: 264501 Color: 10
Size: 250357 Color: 14

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 485277 Color: 15
Size: 263921 Color: 9
Size: 250803 Color: 18

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 485352 Color: 2
Size: 259092 Color: 15
Size: 255557 Color: 4

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 485472 Color: 7
Size: 259714 Color: 12
Size: 254815 Color: 9

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 485480 Color: 16
Size: 264167 Color: 17
Size: 250354 Color: 15

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 485604 Color: 12
Size: 257206 Color: 10
Size: 257191 Color: 0

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 485614 Color: 1
Size: 259714 Color: 15
Size: 254673 Color: 7

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 485676 Color: 17
Size: 263781 Color: 5
Size: 250544 Color: 19

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 485907 Color: 3
Size: 263190 Color: 9
Size: 250904 Color: 6

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 485780 Color: 18
Size: 260996 Color: 4
Size: 253225 Color: 0

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 485905 Color: 14
Size: 263153 Color: 9
Size: 250943 Color: 3

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 485922 Color: 6
Size: 262027 Color: 17
Size: 252052 Color: 10

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 485987 Color: 16
Size: 261766 Color: 13
Size: 252248 Color: 14

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 486043 Color: 7
Size: 259501 Color: 9
Size: 254457 Color: 2

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 486071 Color: 17
Size: 263393 Color: 13
Size: 250537 Color: 4

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 486308 Color: 9
Size: 262491 Color: 10
Size: 251202 Color: 10

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 486461 Color: 11
Size: 259624 Color: 3
Size: 253916 Color: 0

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 486497 Color: 10
Size: 257446 Color: 11
Size: 256058 Color: 12

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 486512 Color: 1
Size: 257584 Color: 18
Size: 255905 Color: 14

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 486525 Color: 14
Size: 257329 Color: 10
Size: 256147 Color: 13

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 486732 Color: 2
Size: 261960 Color: 10
Size: 251309 Color: 7

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 486788 Color: 16
Size: 262349 Color: 15
Size: 250864 Color: 11

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 486856 Color: 19
Size: 259787 Color: 16
Size: 253358 Color: 12

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 486909 Color: 2
Size: 258862 Color: 11
Size: 254230 Color: 10

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 486918 Color: 11
Size: 256874 Color: 8
Size: 256209 Color: 5

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 486969 Color: 13
Size: 262810 Color: 18
Size: 250222 Color: 17

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 486997 Color: 17
Size: 258719 Color: 8
Size: 254285 Color: 9

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 487060 Color: 1
Size: 262390 Color: 6
Size: 250551 Color: 6

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 487190 Color: 3
Size: 262788 Color: 16
Size: 250023 Color: 1

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 487218 Color: 17
Size: 256803 Color: 9
Size: 255980 Color: 9

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 487252 Color: 17
Size: 260808 Color: 7
Size: 251941 Color: 12

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 487265 Color: 12
Size: 260034 Color: 11
Size: 252702 Color: 5

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 487357 Color: 18
Size: 262098 Color: 19
Size: 250546 Color: 5

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 487523 Color: 8
Size: 262359 Color: 3
Size: 250119 Color: 16

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 487545 Color: 5
Size: 258397 Color: 6
Size: 254059 Color: 11

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 487545 Color: 13
Size: 257759 Color: 14
Size: 254697 Color: 6

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 487546 Color: 8
Size: 259173 Color: 15
Size: 253282 Color: 10

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 487569 Color: 8
Size: 262379 Color: 18
Size: 250053 Color: 16

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 487578 Color: 15
Size: 258994 Color: 14
Size: 253429 Color: 6

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 487595 Color: 13
Size: 256323 Color: 13
Size: 256083 Color: 18

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 487619 Color: 14
Size: 261539 Color: 16
Size: 250843 Color: 10

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 487629 Color: 18
Size: 256642 Color: 4
Size: 255730 Color: 8

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 487648 Color: 4
Size: 256618 Color: 5
Size: 255735 Color: 5

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 487889 Color: 3
Size: 261854 Color: 13
Size: 250258 Color: 6

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 488052 Color: 10
Size: 261662 Color: 12
Size: 250287 Color: 3

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 488057 Color: 13
Size: 259789 Color: 10
Size: 252155 Color: 10

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 488155 Color: 12
Size: 259399 Color: 7
Size: 252447 Color: 8

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 488175 Color: 4
Size: 260957 Color: 16
Size: 250869 Color: 1

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 488230 Color: 4
Size: 261252 Color: 0
Size: 250519 Color: 17

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 488365 Color: 12
Size: 256607 Color: 3
Size: 255029 Color: 2

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 488434 Color: 19
Size: 257979 Color: 4
Size: 253588 Color: 7

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 488450 Color: 15
Size: 261548 Color: 8
Size: 250003 Color: 10

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 488453 Color: 6
Size: 260802 Color: 11
Size: 250746 Color: 15

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 488586 Color: 4
Size: 259382 Color: 14
Size: 252033 Color: 8

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 488689 Color: 12
Size: 259096 Color: 14
Size: 252216 Color: 3

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 488710 Color: 9
Size: 260402 Color: 8
Size: 250889 Color: 13

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 488722 Color: 13
Size: 258421 Color: 5
Size: 252858 Color: 5

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 488874 Color: 16
Size: 258926 Color: 16
Size: 252201 Color: 8

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 488911 Color: 4
Size: 259972 Color: 5
Size: 251118 Color: 11

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 488913 Color: 11
Size: 260594 Color: 7
Size: 250494 Color: 17

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 489086 Color: 3
Size: 260863 Color: 14
Size: 250052 Color: 14

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 489090 Color: 8
Size: 259049 Color: 19
Size: 251862 Color: 15

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 489138 Color: 17
Size: 258802 Color: 7
Size: 252061 Color: 4

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 489180 Color: 15
Size: 255648 Color: 6
Size: 255173 Color: 19

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 489188 Color: 19
Size: 257659 Color: 16
Size: 253154 Color: 14

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 489301 Color: 3
Size: 260116 Color: 13
Size: 250584 Color: 4

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 489290 Color: 4
Size: 257994 Color: 6
Size: 252717 Color: 5

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 489351 Color: 0
Size: 259564 Color: 6
Size: 251086 Color: 1

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 489410 Color: 11
Size: 258280 Color: 13
Size: 252311 Color: 1

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 489453 Color: 1
Size: 258821 Color: 14
Size: 251727 Color: 6

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 489465 Color: 19
Size: 255979 Color: 5
Size: 254557 Color: 3

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 489525 Color: 16
Size: 258426 Color: 9
Size: 252050 Color: 1

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 489592 Color: 10
Size: 255926 Color: 17
Size: 254483 Color: 17

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 489673 Color: 7
Size: 257030 Color: 7
Size: 253298 Color: 5

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 489692 Color: 11
Size: 259534 Color: 10
Size: 250775 Color: 1

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 489733 Color: 0
Size: 259914 Color: 6
Size: 250354 Color: 1

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 489941 Color: 7
Size: 258388 Color: 19
Size: 251672 Color: 15

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 490011 Color: 18
Size: 258200 Color: 14
Size: 251790 Color: 4

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 490315 Color: 11
Size: 255315 Color: 11
Size: 254371 Color: 12

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 490240 Color: 2
Size: 256646 Color: 13
Size: 253115 Color: 13

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 490299 Color: 13
Size: 258117 Color: 14
Size: 251585 Color: 8

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 490315 Color: 3
Size: 259100 Color: 7
Size: 250586 Color: 13

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 490363 Color: 13
Size: 254934 Color: 7
Size: 254704 Color: 12

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 490425 Color: 11
Size: 256219 Color: 19
Size: 253357 Color: 15

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 490627 Color: 5
Size: 255268 Color: 0
Size: 254106 Color: 11

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 490634 Color: 0
Size: 258080 Color: 9
Size: 251287 Color: 10

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 490700 Color: 3
Size: 259127 Color: 13
Size: 250174 Color: 5

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 490729 Color: 10
Size: 255979 Color: 11
Size: 253293 Color: 9

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 490748 Color: 13
Size: 258102 Color: 19
Size: 251151 Color: 16

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 490904 Color: 6
Size: 257653 Color: 5
Size: 251444 Color: 15

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 491201 Color: 16
Size: 257419 Color: 14
Size: 251381 Color: 14

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 491264 Color: 12
Size: 254481 Color: 1
Size: 254256 Color: 16

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 491459 Color: 4
Size: 257208 Color: 19
Size: 251334 Color: 9

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 491761 Color: 3
Size: 254740 Color: 10
Size: 253500 Color: 11

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 491765 Color: 13
Size: 254515 Color: 16
Size: 253721 Color: 18

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 491784 Color: 17
Size: 256958 Color: 14
Size: 251259 Color: 16

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 491869 Color: 7
Size: 255596 Color: 1
Size: 252536 Color: 6

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 492034 Color: 7
Size: 256180 Color: 9
Size: 251787 Color: 5

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 492243 Color: 8
Size: 257080 Color: 10
Size: 250678 Color: 4

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 492255 Color: 15
Size: 256101 Color: 6
Size: 251645 Color: 17

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 492289 Color: 14
Size: 255630 Color: 5
Size: 252082 Color: 15

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 492382 Color: 14
Size: 255666 Color: 8
Size: 251953 Color: 8

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 492515 Color: 5
Size: 257373 Color: 4
Size: 250113 Color: 12

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 492522 Color: 1
Size: 254757 Color: 12
Size: 252722 Color: 12

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 492737 Color: 14
Size: 255900 Color: 14
Size: 251364 Color: 6

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 492757 Color: 12
Size: 256657 Color: 11
Size: 250587 Color: 4

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 492910 Color: 6
Size: 255973 Color: 3
Size: 251118 Color: 18

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 492943 Color: 5
Size: 256941 Color: 9
Size: 250117 Color: 19

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 492993 Color: 15
Size: 255546 Color: 5
Size: 251462 Color: 9

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 493107 Color: 16
Size: 256283 Color: 8
Size: 250611 Color: 8

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 493190 Color: 15
Size: 253818 Color: 13
Size: 252993 Color: 10

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 493203 Color: 10
Size: 256165 Color: 4
Size: 250633 Color: 2

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 493256 Color: 16
Size: 256722 Color: 11
Size: 250023 Color: 3

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 493411 Color: 0
Size: 254525 Color: 15
Size: 252065 Color: 8

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 493455 Color: 2
Size: 256350 Color: 8
Size: 250196 Color: 14

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 493457 Color: 11
Size: 255914 Color: 12
Size: 250630 Color: 11

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 493579 Color: 2
Size: 255949 Color: 11
Size: 250473 Color: 6

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 493658 Color: 17
Size: 255999 Color: 4
Size: 250344 Color: 18

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 493758 Color: 3
Size: 255753 Color: 12
Size: 250490 Color: 2

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 493716 Color: 0
Size: 256252 Color: 7
Size: 250033 Color: 1

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 493780 Color: 10
Size: 255294 Color: 11
Size: 250927 Color: 15

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 493941 Color: 6
Size: 255577 Color: 13
Size: 250483 Color: 19

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 494001 Color: 14
Size: 253490 Color: 17
Size: 252510 Color: 13

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 494029 Color: 18
Size: 254597 Color: 5
Size: 251375 Color: 16

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 494069 Color: 3
Size: 255778 Color: 15
Size: 250154 Color: 0

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 494099 Color: 5
Size: 254623 Color: 4
Size: 251279 Color: 0

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 494135 Color: 12
Size: 255185 Color: 0
Size: 250681 Color: 2

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 494150 Color: 11
Size: 253407 Color: 5
Size: 252444 Color: 17

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 494254 Color: 8
Size: 253890 Color: 12
Size: 251857 Color: 13

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 494329 Color: 17
Size: 253531 Color: 3
Size: 252141 Color: 16

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 494552 Color: 1
Size: 253656 Color: 4
Size: 251793 Color: 14

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 494598 Color: 19
Size: 253844 Color: 16
Size: 251559 Color: 12

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 494661 Color: 19
Size: 254779 Color: 13
Size: 250561 Color: 3

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 494728 Color: 4
Size: 254695 Color: 8
Size: 250578 Color: 16

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 494745 Color: 5
Size: 255218 Color: 19
Size: 250038 Color: 4

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 494762 Color: 10
Size: 254678 Color: 18
Size: 250561 Color: 2

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 494932 Color: 7
Size: 254577 Color: 4
Size: 250492 Color: 17

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 494962 Color: 15
Size: 254190 Color: 13
Size: 250849 Color: 16

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 494997 Color: 9
Size: 254736 Color: 13
Size: 250268 Color: 2

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 495075 Color: 11
Size: 253542 Color: 2
Size: 251384 Color: 18

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 495080 Color: 19
Size: 253742 Color: 17
Size: 251179 Color: 17

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 495195 Color: 6
Size: 252520 Color: 6
Size: 252286 Color: 13

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 495209 Color: 7
Size: 253534 Color: 4
Size: 251258 Color: 8

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 495324 Color: 13
Size: 253569 Color: 13
Size: 251108 Color: 8

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 495364 Color: 2
Size: 254599 Color: 10
Size: 250038 Color: 18

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 495493 Color: 2
Size: 253898 Color: 3
Size: 250610 Color: 1

Bin 3146: 0 of cap free
Amount of items: 3
Items: 
Size: 495555 Color: 12
Size: 253850 Color: 4
Size: 250596 Color: 12

Bin 3147: 0 of cap free
Amount of items: 3
Items: 
Size: 495677 Color: 10
Size: 254039 Color: 8
Size: 250285 Color: 14

Bin 3148: 0 of cap free
Amount of items: 3
Items: 
Size: 495832 Color: 18
Size: 253371 Color: 15
Size: 250798 Color: 2

Bin 3149: 0 of cap free
Amount of items: 3
Items: 
Size: 496038 Color: 0
Size: 252304 Color: 19
Size: 251659 Color: 10

Bin 3150: 0 of cap free
Amount of items: 3
Items: 
Size: 496191 Color: 9
Size: 252726 Color: 18
Size: 251084 Color: 3

Bin 3151: 0 of cap free
Amount of items: 3
Items: 
Size: 496293 Color: 19
Size: 252579 Color: 15
Size: 251129 Color: 13

Bin 3152: 0 of cap free
Amount of items: 3
Items: 
Size: 496435 Color: 4
Size: 252328 Color: 2
Size: 251238 Color: 17

Bin 3153: 0 of cap free
Amount of items: 3
Items: 
Size: 496454 Color: 1
Size: 251997 Color: 16
Size: 251550 Color: 12

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 496567 Color: 6
Size: 252311 Color: 12
Size: 251123 Color: 19

Bin 3155: 0 of cap free
Amount of items: 3
Items: 
Size: 496612 Color: 12
Size: 252767 Color: 3
Size: 250622 Color: 12

Bin 3156: 0 of cap free
Amount of items: 3
Items: 
Size: 496823 Color: 13
Size: 253081 Color: 4
Size: 250097 Color: 17

Bin 3157: 0 of cap free
Amount of items: 3
Items: 
Size: 496842 Color: 15
Size: 252762 Color: 1
Size: 250397 Color: 1

Bin 3158: 0 of cap free
Amount of items: 3
Items: 
Size: 497153 Color: 1
Size: 251685 Color: 16
Size: 251163 Color: 6

Bin 3159: 0 of cap free
Amount of items: 3
Items: 
Size: 497192 Color: 17
Size: 252042 Color: 3
Size: 250767 Color: 2

Bin 3160: 0 of cap free
Amount of items: 3
Items: 
Size: 497202 Color: 18
Size: 252770 Color: 16
Size: 250029 Color: 7

Bin 3161: 0 of cap free
Amount of items: 3
Items: 
Size: 497233 Color: 18
Size: 251657 Color: 8
Size: 251111 Color: 1

Bin 3162: 0 of cap free
Amount of items: 3
Items: 
Size: 497342 Color: 4
Size: 252644 Color: 14
Size: 250015 Color: 11

Bin 3163: 0 of cap free
Amount of items: 3
Items: 
Size: 497468 Color: 4
Size: 252034 Color: 8
Size: 250499 Color: 8

Bin 3164: 0 of cap free
Amount of items: 3
Items: 
Size: 497487 Color: 17
Size: 251849 Color: 13
Size: 250665 Color: 19

Bin 3165: 0 of cap free
Amount of items: 3
Items: 
Size: 497654 Color: 16
Size: 251976 Color: 4
Size: 250371 Color: 3

Bin 3166: 0 of cap free
Amount of items: 3
Items: 
Size: 497703 Color: 15
Size: 251861 Color: 0
Size: 250437 Color: 2

Bin 3167: 0 of cap free
Amount of items: 3
Items: 
Size: 497781 Color: 9
Size: 251528 Color: 8
Size: 250692 Color: 5

Bin 3168: 0 of cap free
Amount of items: 3
Items: 
Size: 497809 Color: 7
Size: 251491 Color: 0
Size: 250701 Color: 16

Bin 3169: 0 of cap free
Amount of items: 3
Items: 
Size: 497810 Color: 17
Size: 251501 Color: 4
Size: 250690 Color: 11

Bin 3170: 0 of cap free
Amount of items: 3
Items: 
Size: 498070 Color: 4
Size: 251550 Color: 6
Size: 250381 Color: 12

Bin 3171: 0 of cap free
Amount of items: 3
Items: 
Size: 498089 Color: 18
Size: 251906 Color: 17
Size: 250006 Color: 5

Bin 3172: 0 of cap free
Amount of items: 3
Items: 
Size: 498155 Color: 0
Size: 251088 Color: 3
Size: 250758 Color: 8

Bin 3173: 0 of cap free
Amount of items: 3
Items: 
Size: 498417 Color: 18
Size: 250969 Color: 15
Size: 250615 Color: 7

Bin 3174: 0 of cap free
Amount of items: 3
Items: 
Size: 498563 Color: 10
Size: 251247 Color: 0
Size: 250191 Color: 17

Bin 3175: 0 of cap free
Amount of items: 3
Items: 
Size: 498792 Color: 14
Size: 250661 Color: 8
Size: 250548 Color: 15

Bin 3176: 0 of cap free
Amount of items: 3
Items: 
Size: 498981 Color: 2
Size: 250605 Color: 8
Size: 250415 Color: 11

Bin 3177: 0 of cap free
Amount of items: 3
Items: 
Size: 498994 Color: 14
Size: 250627 Color: 3
Size: 250380 Color: 9

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 384424 Color: 13
Size: 363438 Color: 7
Size: 252138 Color: 8

Bin 3179: 1 of cap free
Amount of items: 3
Items: 
Size: 352018 Color: 16
Size: 330114 Color: 3
Size: 317868 Color: 7

Bin 3180: 1 of cap free
Amount of items: 3
Items: 
Size: 340759 Color: 13
Size: 336114 Color: 1
Size: 323127 Color: 6

Bin 3181: 1 of cap free
Amount of items: 3
Items: 
Size: 381144 Color: 8
Size: 335622 Color: 3
Size: 283234 Color: 17

Bin 3182: 1 of cap free
Amount of items: 3
Items: 
Size: 362560 Color: 0
Size: 346707 Color: 0
Size: 290733 Color: 13

Bin 3183: 1 of cap free
Amount of items: 3
Items: 
Size: 343249 Color: 18
Size: 331520 Color: 0
Size: 325231 Color: 7

Bin 3184: 1 of cap free
Amount of items: 3
Items: 
Size: 356825 Color: 8
Size: 337891 Color: 9
Size: 305284 Color: 4

Bin 3185: 1 of cap free
Amount of items: 3
Items: 
Size: 395580 Color: 1
Size: 345617 Color: 1
Size: 258803 Color: 3

Bin 3186: 1 of cap free
Amount of items: 3
Items: 
Size: 372834 Color: 11
Size: 331690 Color: 16
Size: 295476 Color: 2

Bin 3187: 1 of cap free
Amount of items: 3
Items: 
Size: 360207 Color: 2
Size: 334300 Color: 6
Size: 305493 Color: 4

Bin 3188: 1 of cap free
Amount of items: 3
Items: 
Size: 360381 Color: 10
Size: 335431 Color: 11
Size: 304188 Color: 16

Bin 3189: 1 of cap free
Amount of items: 3
Items: 
Size: 362704 Color: 8
Size: 319223 Color: 14
Size: 318073 Color: 19

Bin 3190: 1 of cap free
Amount of items: 3
Items: 
Size: 367171 Color: 3
Size: 363717 Color: 4
Size: 269112 Color: 7

Bin 3191: 1 of cap free
Amount of items: 3
Items: 
Size: 368809 Color: 5
Size: 318536 Color: 17
Size: 312655 Color: 8

Bin 3192: 1 of cap free
Amount of items: 3
Items: 
Size: 370419 Color: 15
Size: 365091 Color: 9
Size: 264490 Color: 13

Bin 3193: 1 of cap free
Amount of items: 3
Items: 
Size: 370913 Color: 1
Size: 353214 Color: 4
Size: 275873 Color: 1

Bin 3194: 1 of cap free
Amount of items: 3
Items: 
Size: 374519 Color: 8
Size: 341978 Color: 1
Size: 283503 Color: 7

Bin 3195: 1 of cap free
Amount of items: 3
Items: 
Size: 376712 Color: 7
Size: 356626 Color: 0
Size: 266662 Color: 1

Bin 3196: 1 of cap free
Amount of items: 3
Items: 
Size: 367726 Color: 18
Size: 357737 Color: 10
Size: 274537 Color: 15

Bin 3197: 1 of cap free
Amount of items: 3
Items: 
Size: 377967 Color: 13
Size: 326525 Color: 7
Size: 295508 Color: 12

Bin 3198: 1 of cap free
Amount of items: 3
Items: 
Size: 386345 Color: 3
Size: 354218 Color: 9
Size: 259437 Color: 4

Bin 3199: 1 of cap free
Amount of items: 3
Items: 
Size: 386447 Color: 7
Size: 346692 Color: 14
Size: 266861 Color: 12

Bin 3200: 1 of cap free
Amount of items: 3
Items: 
Size: 382441 Color: 5
Size: 338701 Color: 12
Size: 278858 Color: 6

Bin 3201: 1 of cap free
Amount of items: 3
Items: 
Size: 360803 Color: 12
Size: 352331 Color: 18
Size: 286866 Color: 19

Bin 3202: 1 of cap free
Amount of items: 3
Items: 
Size: 352304 Color: 8
Size: 325093 Color: 9
Size: 322603 Color: 8

Bin 3203: 1 of cap free
Amount of items: 3
Items: 
Size: 394125 Color: 19
Size: 307899 Color: 12
Size: 297976 Color: 12

Bin 3204: 1 of cap free
Amount of items: 3
Items: 
Size: 352572 Color: 3
Size: 345018 Color: 6
Size: 302410 Color: 0

Bin 3205: 1 of cap free
Amount of items: 3
Items: 
Size: 382073 Color: 14
Size: 364724 Color: 11
Size: 253203 Color: 4

Bin 3206: 1 of cap free
Amount of items: 3
Items: 
Size: 356842 Color: 12
Size: 342725 Color: 5
Size: 300433 Color: 18

Bin 3207: 1 of cap free
Amount of items: 3
Items: 
Size: 372059 Color: 10
Size: 341877 Color: 6
Size: 286064 Color: 10

Bin 3208: 1 of cap free
Amount of items: 3
Items: 
Size: 356686 Color: 2
Size: 346434 Color: 16
Size: 296880 Color: 3

Bin 3209: 1 of cap free
Amount of items: 3
Items: 
Size: 361742 Color: 18
Size: 354141 Color: 5
Size: 284117 Color: 0

Bin 3210: 1 of cap free
Amount of items: 3
Items: 
Size: 353034 Color: 8
Size: 352077 Color: 15
Size: 294889 Color: 8

Bin 3211: 1 of cap free
Amount of items: 3
Items: 
Size: 348867 Color: 10
Size: 340425 Color: 1
Size: 310708 Color: 15

Bin 3212: 1 of cap free
Amount of items: 3
Items: 
Size: 355965 Color: 15
Size: 335990 Color: 6
Size: 308045 Color: 3

Bin 3213: 1 of cap free
Amount of items: 3
Items: 
Size: 341642 Color: 0
Size: 337963 Color: 17
Size: 320395 Color: 0

Bin 3214: 1 of cap free
Amount of items: 3
Items: 
Size: 336282 Color: 11
Size: 335965 Color: 12
Size: 327753 Color: 4

Bin 3215: 2 of cap free
Amount of items: 3
Items: 
Size: 402246 Color: 6
Size: 339401 Color: 9
Size: 258352 Color: 11

Bin 3216: 2 of cap free
Amount of items: 3
Items: 
Size: 340288 Color: 11
Size: 338472 Color: 13
Size: 321239 Color: 11

Bin 3217: 2 of cap free
Amount of items: 3
Items: 
Size: 364836 Color: 14
Size: 346133 Color: 0
Size: 289030 Color: 4

Bin 3218: 2 of cap free
Amount of items: 3
Items: 
Size: 363257 Color: 5
Size: 336841 Color: 10
Size: 299901 Color: 18

Bin 3219: 2 of cap free
Amount of items: 3
Items: 
Size: 365806 Color: 16
Size: 359071 Color: 10
Size: 275122 Color: 12

Bin 3220: 2 of cap free
Amount of items: 3
Items: 
Size: 358041 Color: 4
Size: 325432 Color: 17
Size: 316526 Color: 4

Bin 3221: 2 of cap free
Amount of items: 3
Items: 
Size: 366361 Color: 1
Size: 330726 Color: 8
Size: 302912 Color: 5

Bin 3222: 2 of cap free
Amount of items: 3
Items: 
Size: 371895 Color: 11
Size: 364592 Color: 4
Size: 263512 Color: 18

Bin 3223: 2 of cap free
Amount of items: 3
Items: 
Size: 381743 Color: 8
Size: 309974 Color: 14
Size: 308282 Color: 18

Bin 3224: 2 of cap free
Amount of items: 3
Items: 
Size: 361389 Color: 8
Size: 337902 Color: 1
Size: 300708 Color: 1

Bin 3225: 2 of cap free
Amount of items: 3
Items: 
Size: 343900 Color: 2
Size: 333246 Color: 13
Size: 322853 Color: 5

Bin 3226: 2 of cap free
Amount of items: 3
Items: 
Size: 362233 Color: 3
Size: 335023 Color: 5
Size: 302743 Color: 11

Bin 3227: 2 of cap free
Amount of items: 3
Items: 
Size: 338288 Color: 16
Size: 331175 Color: 1
Size: 330536 Color: 6

Bin 3228: 2 of cap free
Amount of items: 3
Items: 
Size: 339385 Color: 18
Size: 330952 Color: 14
Size: 329662 Color: 13

Bin 3229: 2 of cap free
Amount of items: 3
Items: 
Size: 358319 Color: 6
Size: 355885 Color: 15
Size: 285795 Color: 9

Bin 3230: 2 of cap free
Amount of items: 3
Items: 
Size: 340837 Color: 5
Size: 339536 Color: 2
Size: 319626 Color: 1

Bin 3231: 2 of cap free
Amount of items: 3
Items: 
Size: 362446 Color: 9
Size: 339090 Color: 14
Size: 298463 Color: 14

Bin 3232: 3 of cap free
Amount of items: 3
Items: 
Size: 356875 Color: 10
Size: 349326 Color: 5
Size: 293797 Color: 7

Bin 3233: 3 of cap free
Amount of items: 3
Items: 
Size: 387867 Color: 11
Size: 361236 Color: 16
Size: 250895 Color: 0

Bin 3234: 3 of cap free
Amount of items: 3
Items: 
Size: 366962 Color: 2
Size: 366708 Color: 7
Size: 266328 Color: 0

Bin 3235: 3 of cap free
Amount of items: 3
Items: 
Size: 353374 Color: 4
Size: 325864 Color: 1
Size: 320760 Color: 17

Bin 3236: 3 of cap free
Amount of items: 3
Items: 
Size: 356556 Color: 19
Size: 347286 Color: 14
Size: 296156 Color: 3

Bin 3237: 3 of cap free
Amount of items: 3
Items: 
Size: 371143 Color: 17
Size: 359597 Color: 8
Size: 269258 Color: 8

Bin 3238: 3 of cap free
Amount of items: 3
Items: 
Size: 372001 Color: 16
Size: 358694 Color: 4
Size: 269303 Color: 12

Bin 3239: 3 of cap free
Amount of items: 3
Items: 
Size: 339615 Color: 17
Size: 330735 Color: 19
Size: 329648 Color: 15

Bin 3240: 3 of cap free
Amount of items: 3
Items: 
Size: 367314 Color: 4
Size: 365079 Color: 8
Size: 267605 Color: 17

Bin 3241: 3 of cap free
Amount of items: 3
Items: 
Size: 353598 Color: 13
Size: 347765 Color: 2
Size: 298635 Color: 8

Bin 3242: 4 of cap free
Amount of items: 3
Items: 
Size: 408641 Color: 4
Size: 332733 Color: 1
Size: 258623 Color: 10

Bin 3243: 4 of cap free
Amount of items: 3
Items: 
Size: 348577 Color: 6
Size: 337930 Color: 7
Size: 313490 Color: 18

Bin 3244: 4 of cap free
Amount of items: 3
Items: 
Size: 348805 Color: 6
Size: 338416 Color: 16
Size: 312776 Color: 15

Bin 3245: 4 of cap free
Amount of items: 3
Items: 
Size: 371619 Color: 8
Size: 353916 Color: 16
Size: 274462 Color: 12

Bin 3246: 4 of cap free
Amount of items: 3
Items: 
Size: 356891 Color: 10
Size: 340631 Color: 3
Size: 302475 Color: 1

Bin 3247: 4 of cap free
Amount of items: 3
Items: 
Size: 353936 Color: 4
Size: 336999 Color: 5
Size: 309062 Color: 16

Bin 3248: 4 of cap free
Amount of items: 3
Items: 
Size: 345439 Color: 10
Size: 331751 Color: 14
Size: 322807 Color: 18

Bin 3249: 4 of cap free
Amount of items: 3
Items: 
Size: 335050 Color: 12
Size: 333574 Color: 4
Size: 331373 Color: 14

Bin 3250: 4 of cap free
Amount of items: 3
Items: 
Size: 372637 Color: 1
Size: 329052 Color: 0
Size: 298308 Color: 8

Bin 3251: 5 of cap free
Amount of items: 3
Items: 
Size: 345199 Color: 14
Size: 335141 Color: 11
Size: 319656 Color: 11

Bin 3252: 5 of cap free
Amount of items: 3
Items: 
Size: 349877 Color: 3
Size: 325145 Color: 1
Size: 324974 Color: 18

Bin 3253: 5 of cap free
Amount of items: 3
Items: 
Size: 338760 Color: 19
Size: 331921 Color: 12
Size: 329315 Color: 11

Bin 3254: 5 of cap free
Amount of items: 3
Items: 
Size: 345543 Color: 15
Size: 343618 Color: 11
Size: 310835 Color: 4

Bin 3255: 5 of cap free
Amount of items: 3
Items: 
Size: 351492 Color: 8
Size: 329830 Color: 12
Size: 318674 Color: 3

Bin 3256: 5 of cap free
Amount of items: 3
Items: 
Size: 432809 Color: 9
Size: 303120 Color: 0
Size: 264067 Color: 7

Bin 3257: 6 of cap free
Amount of items: 3
Items: 
Size: 351226 Color: 19
Size: 349590 Color: 10
Size: 299179 Color: 0

Bin 3258: 6 of cap free
Amount of items: 3
Items: 
Size: 366186 Color: 11
Size: 361377 Color: 5
Size: 272432 Color: 2

Bin 3259: 6 of cap free
Amount of items: 3
Items: 
Size: 354157 Color: 9
Size: 348958 Color: 11
Size: 296880 Color: 14

Bin 3260: 6 of cap free
Amount of items: 3
Items: 
Size: 348899 Color: 5
Size: 330526 Color: 2
Size: 320570 Color: 9

Bin 3261: 7 of cap free
Amount of items: 3
Items: 
Size: 364036 Color: 0
Size: 361817 Color: 18
Size: 274141 Color: 0

Bin 3262: 7 of cap free
Amount of items: 3
Items: 
Size: 359626 Color: 16
Size: 343515 Color: 17
Size: 296853 Color: 11

Bin 3263: 7 of cap free
Amount of items: 3
Items: 
Size: 357771 Color: 3
Size: 356844 Color: 4
Size: 285379 Color: 7

Bin 3264: 8 of cap free
Amount of items: 3
Items: 
Size: 340918 Color: 11
Size: 335433 Color: 16
Size: 323642 Color: 3

Bin 3265: 8 of cap free
Amount of items: 3
Items: 
Size: 370822 Color: 8
Size: 353425 Color: 10
Size: 275746 Color: 18

Bin 3266: 8 of cap free
Amount of items: 3
Items: 
Size: 388869 Color: 10
Size: 351540 Color: 9
Size: 259584 Color: 13

Bin 3267: 8 of cap free
Amount of items: 3
Items: 
Size: 347719 Color: 0
Size: 331523 Color: 4
Size: 320751 Color: 8

Bin 3268: 9 of cap free
Amount of items: 3
Items: 
Size: 367282 Color: 9
Size: 359785 Color: 12
Size: 272925 Color: 5

Bin 3269: 10 of cap free
Amount of items: 3
Items: 
Size: 359260 Color: 4
Size: 358239 Color: 1
Size: 282492 Color: 16

Bin 3270: 10 of cap free
Amount of items: 3
Items: 
Size: 357701 Color: 7
Size: 352191 Color: 0
Size: 290099 Color: 14

Bin 3271: 11 of cap free
Amount of items: 3
Items: 
Size: 351456 Color: 4
Size: 345979 Color: 11
Size: 302555 Color: 0

Bin 3272: 12 of cap free
Amount of items: 3
Items: 
Size: 342668 Color: 11
Size: 339928 Color: 12
Size: 317393 Color: 19

Bin 3273: 12 of cap free
Amount of items: 3
Items: 
Size: 349237 Color: 14
Size: 346750 Color: 8
Size: 304002 Color: 0

Bin 3274: 14 of cap free
Amount of items: 3
Items: 
Size: 373562 Color: 1
Size: 325028 Color: 5
Size: 301397 Color: 5

Bin 3275: 16 of cap free
Amount of items: 3
Items: 
Size: 462318 Color: 18
Size: 278036 Color: 2
Size: 259631 Color: 10

Bin 3276: 18 of cap free
Amount of items: 3
Items: 
Size: 360049 Color: 13
Size: 328717 Color: 17
Size: 311217 Color: 10

Bin 3277: 19 of cap free
Amount of items: 3
Items: 
Size: 359781 Color: 2
Size: 359701 Color: 15
Size: 280500 Color: 17

Bin 3278: 21 of cap free
Amount of items: 3
Items: 
Size: 339265 Color: 13
Size: 335085 Color: 9
Size: 325630 Color: 18

Bin 3279: 21 of cap free
Amount of items: 3
Items: 
Size: 340946 Color: 18
Size: 334652 Color: 19
Size: 324382 Color: 19

Bin 3280: 22 of cap free
Amount of items: 3
Items: 
Size: 339374 Color: 8
Size: 336505 Color: 17
Size: 324100 Color: 12

Bin 3281: 22 of cap free
Amount of items: 3
Items: 
Size: 370324 Color: 4
Size: 359361 Color: 15
Size: 270294 Color: 8

Bin 3282: 25 of cap free
Amount of items: 3
Items: 
Size: 341245 Color: 15
Size: 329604 Color: 0
Size: 329127 Color: 17

Bin 3283: 26 of cap free
Amount of items: 3
Items: 
Size: 350204 Color: 15
Size: 330521 Color: 18
Size: 319250 Color: 7

Bin 3284: 26 of cap free
Amount of items: 3
Items: 
Size: 362231 Color: 3
Size: 362029 Color: 1
Size: 275715 Color: 7

Bin 3285: 28 of cap free
Amount of items: 3
Items: 
Size: 393058 Color: 2
Size: 348280 Color: 16
Size: 258635 Color: 5

Bin 3286: 29 of cap free
Amount of items: 3
Items: 
Size: 364652 Color: 9
Size: 359117 Color: 4
Size: 276203 Color: 9

Bin 3287: 33 of cap free
Amount of items: 3
Items: 
Size: 399389 Color: 4
Size: 349463 Color: 11
Size: 251116 Color: 15

Bin 3288: 39 of cap free
Amount of items: 3
Items: 
Size: 363044 Color: 19
Size: 359184 Color: 12
Size: 277734 Color: 9

Bin 3289: 42 of cap free
Amount of items: 3
Items: 
Size: 366347 Color: 6
Size: 364532 Color: 18
Size: 269080 Color: 17

Bin 3290: 43 of cap free
Amount of items: 3
Items: 
Size: 351962 Color: 18
Size: 351939 Color: 11
Size: 296057 Color: 10

Bin 3291: 45 of cap free
Amount of items: 3
Items: 
Size: 425304 Color: 17
Size: 323191 Color: 4
Size: 251461 Color: 9

Bin 3292: 53 of cap free
Amount of items: 3
Items: 
Size: 363389 Color: 15
Size: 318893 Color: 12
Size: 317666 Color: 19

Bin 3293: 53 of cap free
Amount of items: 3
Items: 
Size: 365396 Color: 0
Size: 360707 Color: 6
Size: 273845 Color: 16

Bin 3294: 56 of cap free
Amount of items: 3
Items: 
Size: 355279 Color: 0
Size: 354733 Color: 12
Size: 289933 Color: 2

Bin 3295: 60 of cap free
Amount of items: 3
Items: 
Size: 408815 Color: 0
Size: 336685 Color: 19
Size: 254441 Color: 0

Bin 3296: 61 of cap free
Amount of items: 3
Items: 
Size: 346505 Color: 15
Size: 330046 Color: 16
Size: 323389 Color: 12

Bin 3297: 61 of cap free
Amount of items: 3
Items: 
Size: 356435 Color: 15
Size: 330430 Color: 4
Size: 313075 Color: 5

Bin 3298: 61 of cap free
Amount of items: 3
Items: 
Size: 357400 Color: 8
Size: 356635 Color: 13
Size: 285905 Color: 13

Bin 3299: 64 of cap free
Amount of items: 3
Items: 
Size: 336897 Color: 2
Size: 331996 Color: 5
Size: 331044 Color: 0

Bin 3300: 66 of cap free
Amount of items: 3
Items: 
Size: 340843 Color: 1
Size: 332579 Color: 19
Size: 326513 Color: 13

Bin 3301: 68 of cap free
Amount of items: 3
Items: 
Size: 348460 Color: 14
Size: 328744 Color: 17
Size: 322729 Color: 6

Bin 3302: 71 of cap free
Amount of items: 3
Items: 
Size: 378401 Color: 1
Size: 354829 Color: 18
Size: 266700 Color: 14

Bin 3303: 73 of cap free
Amount of items: 3
Items: 
Size: 355793 Color: 5
Size: 355348 Color: 17
Size: 288787 Color: 13

Bin 3304: 78 of cap free
Amount of items: 3
Items: 
Size: 354107 Color: 14
Size: 353635 Color: 10
Size: 292181 Color: 9

Bin 3305: 83 of cap free
Amount of items: 3
Items: 
Size: 355902 Color: 7
Size: 331628 Color: 8
Size: 312388 Color: 7

Bin 3306: 85 of cap free
Amount of items: 3
Items: 
Size: 429301 Color: 0
Size: 319976 Color: 11
Size: 250639 Color: 2

Bin 3307: 93 of cap free
Amount of items: 3
Items: 
Size: 347213 Color: 14
Size: 346803 Color: 6
Size: 305892 Color: 0

Bin 3308: 94 of cap free
Amount of items: 3
Items: 
Size: 406477 Color: 19
Size: 341753 Color: 0
Size: 251677 Color: 11

Bin 3309: 155 of cap free
Amount of items: 3
Items: 
Size: 379307 Color: 4
Size: 353584 Color: 5
Size: 266955 Color: 17

Bin 3310: 161 of cap free
Amount of items: 3
Items: 
Size: 363208 Color: 1
Size: 360967 Color: 14
Size: 275665 Color: 19

Bin 3311: 167 of cap free
Amount of items: 3
Items: 
Size: 417243 Color: 18
Size: 324965 Color: 10
Size: 257626 Color: 7

Bin 3312: 169 of cap free
Amount of items: 3
Items: 
Size: 345119 Color: 6
Size: 333451 Color: 11
Size: 321262 Color: 6

Bin 3313: 252 of cap free
Amount of items: 2
Items: 
Size: 499966 Color: 14
Size: 499783 Color: 13

Bin 3314: 284 of cap free
Amount of items: 3
Items: 
Size: 367326 Color: 17
Size: 366772 Color: 16
Size: 265619 Color: 11

Bin 3315: 285 of cap free
Amount of items: 3
Items: 
Size: 338080 Color: 12
Size: 332510 Color: 10
Size: 329126 Color: 10

Bin 3316: 420 of cap free
Amount of items: 3
Items: 
Size: 339549 Color: 12
Size: 337154 Color: 4
Size: 322878 Color: 11

Bin 3317: 937 of cap free
Amount of items: 2
Items: 
Size: 499725 Color: 19
Size: 499339 Color: 5

Bin 3318: 1747 of cap free
Amount of items: 2
Items: 
Size: 499145 Color: 4
Size: 499109 Color: 16

Bin 3319: 3003 of cap free
Amount of items: 3
Items: 
Size: 362456 Color: 11
Size: 361961 Color: 7
Size: 272581 Color: 8

Bin 3320: 12442 of cap free
Amount of items: 3
Items: 
Size: 358417 Color: 18
Size: 358083 Color: 4
Size: 271059 Color: 14

Bin 3321: 17034 of cap free
Amount of items: 3
Items: 
Size: 356222 Color: 11
Size: 355768 Color: 13
Size: 270977 Color: 9

Bin 3322: 22360 of cap free
Amount of items: 3
Items: 
Size: 355165 Color: 3
Size: 352806 Color: 5
Size: 269670 Color: 10

Bin 3323: 27706 of cap free
Amount of items: 3
Items: 
Size: 352204 Color: 15
Size: 351869 Color: 6
Size: 268222 Color: 6

Bin 3324: 31610 of cap free
Amount of items: 3
Items: 
Size: 351604 Color: 17
Size: 349546 Color: 8
Size: 267241 Color: 3

Bin 3325: 36603 of cap free
Amount of items: 3
Items: 
Size: 349256 Color: 16
Size: 349040 Color: 0
Size: 265102 Color: 15

Bin 3326: 41524 of cap free
Amount of items: 3
Items: 
Size: 347665 Color: 9
Size: 346329 Color: 13
Size: 264483 Color: 18

Bin 3327: 46158 of cap free
Amount of items: 3
Items: 
Size: 344829 Color: 2
Size: 344791 Color: 1
Size: 264223 Color: 15

Bin 3328: 48960 of cap free
Amount of items: 3
Items: 
Size: 344368 Color: 14
Size: 343566 Color: 14
Size: 263107 Color: 10

Bin 3329: 52126 of cap free
Amount of items: 3
Items: 
Size: 343489 Color: 5
Size: 343268 Color: 15
Size: 261118 Color: 1

Bin 3330: 58614 of cap free
Amount of items: 3
Items: 
Size: 341524 Color: 10
Size: 339374 Color: 7
Size: 260489 Color: 17

Bin 3331: 65775 of cap free
Amount of items: 3
Items: 
Size: 338067 Color: 11
Size: 336197 Color: 12
Size: 259962 Color: 11

Bin 3332: 71361 of cap free
Amount of items: 3
Items: 
Size: 336197 Color: 8
Size: 332990 Color: 1
Size: 259453 Color: 5

Bin 3333: 225470 of cap free
Amount of items: 3
Items: 
Size: 258694 Color: 6
Size: 258580 Color: 1
Size: 257257 Color: 13

Bin 3334: 234509 of cap free
Amount of items: 3
Items: 
Size: 256170 Color: 12
Size: 255228 Color: 5
Size: 254094 Color: 16

Bin 3335: 249291 of cap free
Amount of items: 2
Items: 
Size: 499073 Color: 17
Size: 251637 Color: 7

Bin 3336: 748902 of cap free
Amount of items: 1
Items: 
Size: 251099 Color: 16

Total size: 3334003334
Total free space: 2000002

