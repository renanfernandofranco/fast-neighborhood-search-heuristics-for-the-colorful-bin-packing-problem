Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 315 Color: 16
Size: 259 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 299 Color: 18
Size: 274 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 271 Color: 14
Size: 263 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 6
Size: 344 Color: 14
Size: 286 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 9
Size: 344 Color: 6
Size: 306 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 294 Color: 18
Size: 262 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 304 Color: 10
Size: 270 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 320 Color: 7
Size: 250 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 5
Size: 256 Color: 14
Size: 250 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 347 Color: 1
Size: 280 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 263 Color: 2
Size: 257 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 19
Size: 343 Color: 15
Size: 258 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 12
Size: 256 Color: 12
Size: 253 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 11
Size: 255 Color: 10
Size: 250 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9
Size: 261 Color: 16
Size: 254 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 15
Size: 290 Color: 19
Size: 275 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 12
Size: 251 Color: 9
Size: 250 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 362 Color: 3
Size: 257 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 9
Size: 350 Color: 19
Size: 255 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 6
Size: 300 Color: 4
Size: 287 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 12
Size: 252 Color: 19
Size: 250 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 15
Size: 286 Color: 9
Size: 251 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 297 Color: 4
Size: 274 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 2
Size: 351 Color: 13
Size: 279 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 8
Size: 349 Color: 8
Size: 283 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 12
Size: 255 Color: 6
Size: 250 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 361 Color: 5
Size: 272 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 13
Size: 288 Color: 6
Size: 267 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 19
Size: 323 Color: 13
Size: 309 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 6
Size: 286 Color: 15
Size: 274 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 277 Color: 11
Size: 265 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 289 Color: 16
Size: 277 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 330 Color: 15
Size: 258 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9
Size: 283 Color: 6
Size: 253 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 370 Color: 10
Size: 254 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 282 Color: 9
Size: 358 Color: 16

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 281 Color: 11
Size: 268 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 6
Size: 300 Color: 11
Size: 272 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 19
Size: 329 Color: 16
Size: 273 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 332 Color: 14
Size: 311 Color: 0

Total size: 40000
Total free space: 0

