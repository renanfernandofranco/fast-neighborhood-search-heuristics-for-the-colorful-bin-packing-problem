Capicity Bin: 2472
Lower Bound: 65

Bins used: 65
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1000 Color: 131
Size: 504 Color: 102
Size: 384 Color: 88
Size: 272 Color: 77
Size: 168 Color: 59
Size: 144 Color: 56

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1926 Color: 177
Size: 466 Color: 96
Size: 64 Color: 20
Size: 16 Color: 3

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1882 Color: 173
Size: 494 Color: 99
Size: 88 Color: 32
Size: 8 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 2212 Color: 201
Size: 220 Color: 69
Size: 32 Color: 5
Size: 8 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1839 Color: 170
Size: 529 Color: 104
Size: 104 Color: 40

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 148
Size: 756 Color: 120
Size: 272 Color: 76

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 198
Size: 230 Color: 71
Size: 44 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 185
Size: 382 Color: 87
Size: 72 Color: 24

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 188
Size: 370 Color: 84
Size: 44 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1676 Color: 159
Size: 700 Color: 117
Size: 96 Color: 36

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 164
Size: 620 Color: 112
Size: 120 Color: 46

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1986 Color: 184
Size: 478 Color: 98
Size: 8 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 142
Size: 1022 Color: 133
Size: 204 Color: 65

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 166
Size: 601 Color: 109
Size: 120 Color: 48

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 168
Size: 556 Color: 106
Size: 104 Color: 42

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 145
Size: 993 Color: 130
Size: 70 Color: 21

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 138
Size: 1031 Color: 136
Size: 204 Color: 67

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 146
Size: 882 Color: 128
Size: 172 Color: 61

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 139
Size: 1030 Color: 135
Size: 204 Color: 66

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 147
Size: 877 Color: 127
Size: 174 Color: 62

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 165
Size: 606 Color: 110
Size: 120 Color: 47

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 189
Size: 340 Color: 83
Size: 64 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 187
Size: 372 Color: 85
Size: 72 Color: 23

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 194
Size: 282 Color: 78
Size: 44 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 140
Size: 1029 Color: 134
Size: 204 Color: 68

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 143
Size: 1018 Color: 132
Size: 200 Color: 64

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 179
Size: 443 Color: 92
Size: 88 Color: 33

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 156
Size: 709 Color: 118
Size: 140 Color: 54

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 141
Size: 1140 Color: 137
Size: 88 Color: 30

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 160
Size: 576 Color: 108
Size: 192 Color: 63

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 154
Size: 860 Color: 126
Size: 40 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 178
Size: 452 Color: 94
Size: 88 Color: 31

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1911 Color: 175
Size: 469 Color: 97
Size: 92 Color: 34

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 149
Size: 846 Color: 125
Size: 168 Color: 60

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 169
Size: 543 Color: 105
Size: 108 Color: 43

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 199
Size: 228 Color: 70
Size: 40 Color: 8

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 196
Size: 242 Color: 74
Size: 36 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 167
Size: 558 Color: 107
Size: 108 Color: 44

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 157
Size: 698 Color: 116
Size: 136 Color: 53

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 191
Size: 308 Color: 81
Size: 56 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 176
Size: 462 Color: 95
Size: 92 Color: 35

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 193
Size: 284 Color: 79
Size: 48 Color: 14

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 153
Size: 802 Color: 122
Size: 112 Color: 45

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 200
Size: 234 Color: 72
Size: 28 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 172
Size: 500 Color: 101
Size: 96 Color: 39

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 181
Size: 418 Color: 91
Size: 80 Color: 28

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 182
Size: 412 Color: 90
Size: 80 Color: 26

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 151
Size: 840 Color: 124
Size: 80 Color: 29

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 161
Size: 642 Color: 114
Size: 124 Color: 51

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 192
Size: 294 Color: 80
Size: 56 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 162
Size: 634 Color: 113
Size: 124 Color: 50

Bin 52: 0 of cap free
Amount of items: 4
Items: 
Size: 1292 Color: 144
Size: 988 Color: 129
Size: 96 Color: 38
Size: 96 Color: 37

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 190
Size: 314 Color: 82
Size: 60 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 171
Size: 522 Color: 103
Size: 104 Color: 41

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 183
Size: 409 Color: 89
Size: 80 Color: 27

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 152
Size: 767 Color: 121
Size: 152 Color: 57

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 155
Size: 734 Color: 119
Size: 144 Color: 55

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 195
Size: 242 Color: 75
Size: 44 Color: 11

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 174
Size: 499 Color: 100
Size: 76 Color: 25

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 186
Size: 378 Color: 86
Size: 72 Color: 22

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 163
Size: 619 Color: 111
Size: 122 Color: 49

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 197
Size: 236 Color: 73
Size: 40 Color: 7

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 150
Size: 817 Color: 123
Size: 162 Color: 58

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 180
Size: 447 Color: 93
Size: 54 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 158
Size: 667 Color: 115
Size: 132 Color: 52

Total size: 160680
Total free space: 0

