Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 118
Size: 254 Color: 15
Size: 251 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 90
Size: 253 Color: 12
Size: 356 Color: 77

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 100
Size: 298 Color: 59
Size: 255 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 114
Size: 259 Color: 23
Size: 253 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 97
Size: 329 Color: 67
Size: 250 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 92
Size: 343 Color: 71
Size: 260 Color: 24

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 89
Size: 340 Color: 69
Size: 272 Color: 42

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 106
Size: 290 Color: 54
Size: 250 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 82
Size: 347 Color: 72
Size: 283 Color: 52

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 102
Size: 281 Color: 48
Size: 269 Color: 38

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 99
Size: 293 Color: 55
Size: 261 Color: 29

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 108
Size: 277 Color: 44
Size: 250 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 109
Size: 264 Color: 30
Size: 260 Color: 25

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 98
Size: 293 Color: 56
Size: 278 Color: 45

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 83
Size: 354 Color: 76
Size: 271 Color: 40

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 86
Size: 366 Color: 81
Size: 257 Color: 22

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 87
Size: 361 Color: 80
Size: 260 Color: 26

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 88
Size: 357 Color: 78
Size: 260 Color: 27

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 113
Size: 264 Color: 31
Size: 250 Color: 5

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 115
Size: 257 Color: 21
Size: 251 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 104
Size: 278 Color: 46
Size: 267 Color: 36

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 116
Size: 255 Color: 19
Size: 252 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 107
Size: 272 Color: 41
Size: 255 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 110
Size: 265 Color: 32
Size: 255 Color: 20

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 111
Size: 268 Color: 37
Size: 250 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 84
Size: 329 Color: 66
Size: 296 Color: 58

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 96
Size: 323 Color: 64
Size: 266 Color: 33

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 94
Size: 302 Color: 61
Size: 298 Color: 60

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 117
Size: 253 Color: 13
Size: 252 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 79
Size: 348 Color: 74
Size: 294 Color: 57

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 112
Size: 266 Color: 35
Size: 250 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 103
Size: 281 Color: 49
Size: 269 Color: 39

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 75
Size: 341 Color: 70
Size: 309 Color: 62

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 91
Size: 322 Color: 63
Size: 283 Color: 51

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 119
Size: 252 Color: 9
Size: 250 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 85
Size: 339 Color: 68
Size: 284 Color: 53

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 93
Size: 348 Color: 73
Size: 254 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 95
Size: 326 Color: 65
Size: 266 Color: 34

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 101
Size: 280 Color: 47
Size: 273 Color: 43

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 105
Size: 282 Color: 50
Size: 261 Color: 28

Total size: 40000
Total free space: 0

