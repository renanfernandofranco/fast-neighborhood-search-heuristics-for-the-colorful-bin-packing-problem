Capicity Bin: 19296
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9652 Color: 4
Size: 9324 Color: 4
Size: 320 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 3
Size: 9226 Color: 0
Size: 396 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10568 Color: 1
Size: 8384 Color: 2
Size: 344 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 3
Size: 6124 Color: 2
Size: 364 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 2
Size: 5296 Color: 0
Size: 896 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 0
Size: 5046 Color: 1
Size: 430 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 2
Size: 5168 Color: 1
Size: 352 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 1
Size: 4385 Color: 2
Size: 752 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 2
Size: 4268 Color: 0
Size: 848 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14508 Color: 2
Size: 4454 Color: 2
Size: 334 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14769 Color: 2
Size: 4273 Color: 3
Size: 254 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15108 Color: 1
Size: 3796 Color: 4
Size: 392 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15420 Color: 0
Size: 3466 Color: 1
Size: 410 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15540 Color: 0
Size: 3132 Color: 3
Size: 624 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15578 Color: 4
Size: 1936 Color: 2
Size: 1782 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15656 Color: 1
Size: 2456 Color: 4
Size: 1184 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15661 Color: 0
Size: 3031 Color: 3
Size: 604 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15788 Color: 1
Size: 2720 Color: 2
Size: 788 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 3
Size: 3122 Color: 1
Size: 354 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16100 Color: 0
Size: 2912 Color: 1
Size: 284 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16142 Color: 3
Size: 2630 Color: 1
Size: 524 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16184 Color: 3
Size: 2354 Color: 1
Size: 758 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16245 Color: 1
Size: 2543 Color: 3
Size: 508 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16279 Color: 3
Size: 2489 Color: 4
Size: 528 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16368 Color: 0
Size: 2580 Color: 1
Size: 348 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16420 Color: 2
Size: 2332 Color: 0
Size: 544 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16456 Color: 2
Size: 2248 Color: 3
Size: 592 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 1
Size: 2196 Color: 1
Size: 604 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16500 Color: 1
Size: 2442 Color: 0
Size: 354 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16538 Color: 3
Size: 2302 Color: 1
Size: 456 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16616 Color: 0
Size: 1714 Color: 0
Size: 966 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16724 Color: 0
Size: 1612 Color: 2
Size: 960 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16756 Color: 0
Size: 1964 Color: 2
Size: 576 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16856 Color: 4
Size: 1832 Color: 0
Size: 608 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16911 Color: 2
Size: 1989 Color: 0
Size: 396 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16932 Color: 1
Size: 1844 Color: 3
Size: 520 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16948 Color: 1
Size: 1708 Color: 2
Size: 640 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16976 Color: 4
Size: 1912 Color: 3
Size: 408 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 2
Size: 1606 Color: 2
Size: 688 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 4
Size: 1176 Color: 0
Size: 1104 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17035 Color: 4
Size: 1885 Color: 1
Size: 376 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17098 Color: 1
Size: 1638 Color: 0
Size: 560 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 3
Size: 1248 Color: 0
Size: 912 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17163 Color: 0
Size: 1765 Color: 4
Size: 368 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17156 Color: 3
Size: 1748 Color: 4
Size: 392 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17252 Color: 0
Size: 1720 Color: 2
Size: 324 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17272 Color: 0
Size: 1600 Color: 4
Size: 424 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17231 Color: 4
Size: 1721 Color: 3
Size: 344 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17334 Color: 0
Size: 1248 Color: 4
Size: 714 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17289 Color: 3
Size: 1667 Color: 1
Size: 340 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17336 Color: 3
Size: 1344 Color: 1
Size: 616 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17340 Color: 2
Size: 1636 Color: 0
Size: 320 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17364 Color: 4
Size: 1312 Color: 3
Size: 620 Color: 0

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 10594 Color: 0
Size: 8269 Color: 1
Size: 432 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 11795 Color: 0
Size: 6764 Color: 3
Size: 736 Color: 4

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 3
Size: 6947 Color: 0
Size: 400 Color: 4

Bin 57: 1 of cap free
Amount of items: 5
Items: 
Size: 12866 Color: 1
Size: 3773 Color: 0
Size: 1764 Color: 4
Size: 476 Color: 3
Size: 416 Color: 4

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13499 Color: 1
Size: 5416 Color: 4
Size: 380 Color: 3

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13531 Color: 0
Size: 5348 Color: 4
Size: 416 Color: 0

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 14748 Color: 2
Size: 4227 Color: 3
Size: 320 Color: 2

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 15162 Color: 0
Size: 3801 Color: 0
Size: 332 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 15554 Color: 0
Size: 1914 Color: 2
Size: 1827 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 15982 Color: 3
Size: 3025 Color: 4
Size: 288 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 15989 Color: 3
Size: 2890 Color: 0
Size: 416 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 16168 Color: 1
Size: 2159 Color: 3
Size: 968 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 16648 Color: 2
Size: 1955 Color: 0
Size: 692 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 16703 Color: 1
Size: 2124 Color: 2
Size: 468 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 16707 Color: 0
Size: 1868 Color: 1
Size: 720 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 16742 Color: 0
Size: 1673 Color: 1
Size: 880 Color: 4

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 16795 Color: 2
Size: 1388 Color: 1
Size: 1112 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 16876 Color: 2
Size: 1779 Color: 0
Size: 640 Color: 1

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 16970 Color: 3
Size: 2325 Color: 1

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 17084 Color: 0
Size: 2211 Color: 1

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 17179 Color: 4
Size: 1720 Color: 0
Size: 396 Color: 2

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 2
Size: 4892 Color: 1
Size: 448 Color: 2

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 14169 Color: 4
Size: 4281 Color: 4
Size: 844 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 4
Size: 4680 Color: 3
Size: 312 Color: 2

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 15124 Color: 2
Size: 3416 Color: 2
Size: 754 Color: 4

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 15534 Color: 3
Size: 2896 Color: 4
Size: 864 Color: 0

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 15922 Color: 4
Size: 2148 Color: 2
Size: 1224 Color: 1

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 15965 Color: 4
Size: 3329 Color: 2

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 16474 Color: 1
Size: 2020 Color: 4
Size: 800 Color: 3

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 16370 Color: 4
Size: 2924 Color: 0

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 17060 Color: 3
Size: 2234 Color: 4

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 17112 Color: 1
Size: 2182 Color: 3

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 17190 Color: 4
Size: 2104 Color: 2

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 9651 Color: 0
Size: 8042 Color: 3
Size: 1600 Color: 2

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 0
Size: 4805 Color: 3
Size: 464 Color: 2

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 14225 Color: 1
Size: 3824 Color: 4
Size: 1244 Color: 3

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 14737 Color: 4
Size: 3484 Color: 1
Size: 1072 Color: 2

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 15898 Color: 4
Size: 3395 Color: 2

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 16016 Color: 2
Size: 2757 Color: 1
Size: 520 Color: 0

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 16917 Color: 4
Size: 2376 Color: 2

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 11834 Color: 3
Size: 6770 Color: 1
Size: 688 Color: 2

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 0
Size: 6640 Color: 3
Size: 468 Color: 4

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 12739 Color: 1
Size: 6225 Color: 0
Size: 328 Color: 4

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 2
Size: 5384 Color: 2
Size: 728 Color: 1

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 1
Size: 3848 Color: 3
Size: 368 Color: 2

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 15138 Color: 3
Size: 3862 Color: 1
Size: 292 Color: 0

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 15488 Color: 1
Size: 3400 Color: 3
Size: 404 Color: 4

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 16906 Color: 3
Size: 2386 Color: 2

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 17162 Color: 1
Size: 2130 Color: 4

Bin 103: 5 of cap free
Amount of items: 5
Items: 
Size: 9656 Color: 1
Size: 3106 Color: 1
Size: 2352 Color: 2
Size: 2343 Color: 2
Size: 1834 Color: 0

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 16776 Color: 2
Size: 2515 Color: 3

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 17139 Color: 2
Size: 2096 Color: 4
Size: 56 Color: 2

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 17297 Color: 1
Size: 1994 Color: 2

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 17308 Color: 1
Size: 1983 Color: 4

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 1
Size: 3962 Color: 3
Size: 664 Color: 4

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 15688 Color: 4
Size: 3138 Color: 1
Size: 464 Color: 3

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 1
Size: 7169 Color: 3
Size: 336 Color: 2

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 15605 Color: 2
Size: 3684 Color: 1

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 15625 Color: 1
Size: 3312 Color: 2
Size: 352 Color: 3

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 15667 Color: 0
Size: 3530 Color: 1
Size: 92 Color: 4

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 17204 Color: 1
Size: 2085 Color: 2

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 11344 Color: 4
Size: 7600 Color: 0
Size: 344 Color: 2

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 12588 Color: 1
Size: 6264 Color: 3
Size: 436 Color: 2

Bin 117: 9 of cap free
Amount of items: 3
Items: 
Size: 12713 Color: 2
Size: 6222 Color: 0
Size: 352 Color: 4

Bin 118: 10 of cap free
Amount of items: 4
Items: 
Size: 9666 Color: 2
Size: 8016 Color: 3
Size: 1120 Color: 0
Size: 484 Color: 4

Bin 119: 10 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 3
Size: 4648 Color: 1
Size: 976 Color: 4

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 15840 Color: 0
Size: 3446 Color: 2

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 1
Size: 8040 Color: 2
Size: 550 Color: 3

Bin 122: 11 of cap free
Amount of items: 3
Items: 
Size: 15223 Color: 0
Size: 3760 Color: 2
Size: 302 Color: 4

Bin 123: 12 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 2
Size: 7232 Color: 0
Size: 864 Color: 1

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 11760 Color: 4
Size: 7524 Color: 0

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 1
Size: 5596 Color: 3

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 16048 Color: 4
Size: 3236 Color: 3

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 16204 Color: 2
Size: 3080 Color: 0

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 16507 Color: 4
Size: 2777 Color: 3

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 16668 Color: 4
Size: 2616 Color: 0

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 15062 Color: 0
Size: 4221 Color: 4

Bin 131: 14 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 4
Size: 5362 Color: 2
Size: 678 Color: 1

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 15120 Color: 1
Size: 4162 Color: 0

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 16678 Color: 3
Size: 2604 Color: 4

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 17242 Color: 2
Size: 2040 Color: 3

Bin 135: 16 of cap free
Amount of items: 7
Items: 
Size: 9655 Color: 3
Size: 1936 Color: 0
Size: 1803 Color: 0
Size: 1758 Color: 1
Size: 1680 Color: 4
Size: 1440 Color: 2
Size: 1008 Color: 0

Bin 136: 17 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 2
Size: 4851 Color: 1
Size: 1312 Color: 1

Bin 137: 20 of cap free
Amount of items: 4
Items: 
Size: 9658 Color: 4
Size: 8034 Color: 1
Size: 1152 Color: 0
Size: 432 Color: 2

Bin 138: 20 of cap free
Amount of items: 3
Items: 
Size: 11350 Color: 2
Size: 7254 Color: 3
Size: 672 Color: 4

Bin 139: 21 of cap free
Amount of items: 3
Items: 
Size: 15303 Color: 3
Size: 3812 Color: 0
Size: 160 Color: 4

Bin 140: 21 of cap free
Amount of items: 2
Items: 
Size: 15632 Color: 4
Size: 3643 Color: 2

Bin 141: 21 of cap free
Amount of items: 2
Items: 
Size: 16675 Color: 3
Size: 2600 Color: 0

Bin 142: 23 of cap free
Amount of items: 3
Items: 
Size: 10961 Color: 3
Size: 7288 Color: 1
Size: 1024 Color: 3

Bin 143: 23 of cap free
Amount of items: 3
Items: 
Size: 14925 Color: 1
Size: 2900 Color: 2
Size: 1448 Color: 4

Bin 144: 24 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 5928 Color: 2
Size: 440 Color: 0

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 4
Size: 5336 Color: 2

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 17188 Color: 4
Size: 2084 Color: 2

Bin 147: 26 of cap free
Amount of items: 4
Items: 
Size: 9660 Color: 3
Size: 8026 Color: 2
Size: 1072 Color: 0
Size: 512 Color: 1

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 12382 Color: 2
Size: 6888 Color: 0

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 16866 Color: 3
Size: 2404 Color: 0

Bin 150: 27 of cap free
Amount of items: 2
Items: 
Size: 14773 Color: 3
Size: 4496 Color: 4

Bin 151: 27 of cap free
Amount of items: 4
Items: 
Size: 17105 Color: 2
Size: 2004 Color: 1
Size: 96 Color: 3
Size: 64 Color: 4

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 16166 Color: 4
Size: 3102 Color: 0

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 16900 Color: 0
Size: 2368 Color: 1

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 17296 Color: 3
Size: 1972 Color: 1

Bin 155: 30 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 1
Size: 4698 Color: 3
Size: 576 Color: 3

Bin 156: 30 of cap free
Amount of items: 2
Items: 
Size: 17240 Color: 3
Size: 2026 Color: 4

Bin 157: 32 of cap free
Amount of items: 2
Items: 
Size: 16208 Color: 0
Size: 3056 Color: 4

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 16816 Color: 2
Size: 2448 Color: 3

Bin 159: 33 of cap free
Amount of items: 2
Items: 
Size: 16653 Color: 4
Size: 2610 Color: 3

Bin 160: 37 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 3
Size: 9363 Color: 1
Size: 224 Color: 1

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 14448 Color: 4
Size: 4808 Color: 0

Bin 162: 40 of cap free
Amount of items: 2
Items: 
Size: 15208 Color: 1
Size: 4048 Color: 3

Bin 163: 44 of cap free
Amount of items: 3
Items: 
Size: 14197 Color: 0
Size: 4831 Color: 4
Size: 224 Color: 1

Bin 164: 48 of cap free
Amount of items: 2
Items: 
Size: 16434 Color: 4
Size: 2814 Color: 3

Bin 165: 50 of cap free
Amount of items: 2
Items: 
Size: 16670 Color: 3
Size: 2576 Color: 0

Bin 166: 53 of cap free
Amount of items: 3
Items: 
Size: 11827 Color: 3
Size: 6624 Color: 0
Size: 792 Color: 2

Bin 167: 54 of cap free
Amount of items: 4
Items: 
Size: 9659 Color: 4
Size: 8039 Color: 3
Size: 1240 Color: 0
Size: 304 Color: 2

Bin 168: 59 of cap free
Amount of items: 2
Items: 
Size: 15997 Color: 2
Size: 3240 Color: 4

Bin 169: 60 of cap free
Amount of items: 2
Items: 
Size: 16485 Color: 4
Size: 2751 Color: 2

Bin 170: 63 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 3
Size: 5465 Color: 4

Bin 171: 66 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 2
Size: 4564 Color: 4

Bin 172: 72 of cap free
Amount of items: 2
Items: 
Size: 16488 Color: 2
Size: 2736 Color: 0

Bin 173: 72 of cap free
Amount of items: 2
Items: 
Size: 17008 Color: 3
Size: 2216 Color: 4

Bin 174: 73 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 0
Size: 5487 Color: 3

Bin 175: 76 of cap free
Amount of items: 2
Items: 
Size: 15224 Color: 3
Size: 3996 Color: 4

Bin 176: 76 of cap free
Amount of items: 2
Items: 
Size: 16172 Color: 3
Size: 3048 Color: 0

Bin 177: 77 of cap free
Amount of items: 2
Items: 
Size: 15752 Color: 1
Size: 3467 Color: 2

Bin 178: 79 of cap free
Amount of items: 2
Items: 
Size: 13477 Color: 1
Size: 5740 Color: 4

Bin 179: 81 of cap free
Amount of items: 2
Items: 
Size: 16311 Color: 4
Size: 2904 Color: 0

Bin 180: 86 of cap free
Amount of items: 3
Items: 
Size: 12842 Color: 0
Size: 5456 Color: 1
Size: 912 Color: 4

Bin 181: 86 of cap free
Amount of items: 2
Items: 
Size: 17007 Color: 3
Size: 2203 Color: 1

Bin 182: 88 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 0
Size: 3864 Color: 1

Bin 183: 101 of cap free
Amount of items: 7
Items: 
Size: 9649 Color: 3
Size: 1660 Color: 0
Size: 1640 Color: 2
Size: 1606 Color: 1
Size: 1604 Color: 4
Size: 1604 Color: 3
Size: 1432 Color: 4

Bin 184: 101 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 3
Size: 6251 Color: 4

Bin 185: 106 of cap free
Amount of items: 2
Items: 
Size: 13428 Color: 0
Size: 5762 Color: 3

Bin 186: 109 of cap free
Amount of items: 3
Items: 
Size: 10384 Color: 3
Size: 8035 Color: 1
Size: 768 Color: 4

Bin 187: 112 of cap free
Amount of items: 2
Items: 
Size: 9680 Color: 4
Size: 9504 Color: 2

Bin 188: 122 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 3
Size: 6390 Color: 1

Bin 189: 124 of cap free
Amount of items: 2
Items: 
Size: 10668 Color: 4
Size: 8504 Color: 3

Bin 190: 126 of cap free
Amount of items: 2
Items: 
Size: 14546 Color: 1
Size: 4624 Color: 2

Bin 191: 129 of cap free
Amount of items: 6
Items: 
Size: 9650 Color: 4
Size: 2668 Color: 1
Size: 2339 Color: 2
Size: 2190 Color: 0
Size: 1808 Color: 1
Size: 512 Color: 4

Bin 192: 136 of cap free
Amount of items: 2
Items: 
Size: 14736 Color: 3
Size: 4424 Color: 1

Bin 193: 141 of cap free
Amount of items: 2
Items: 
Size: 15816 Color: 1
Size: 3339 Color: 4

Bin 194: 168 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 1
Size: 6288 Color: 3

Bin 195: 208 of cap free
Amount of items: 2
Items: 
Size: 14680 Color: 4
Size: 4408 Color: 0

Bin 196: 220 of cap free
Amount of items: 2
Items: 
Size: 11032 Color: 4
Size: 8044 Color: 0

Bin 197: 234 of cap free
Amount of items: 16
Items: 
Size: 1604 Color: 4
Size: 1600 Color: 2
Size: 1600 Color: 2
Size: 1376 Color: 4
Size: 1368 Color: 4
Size: 1250 Color: 3
Size: 1096 Color: 3
Size: 1092 Color: 1
Size: 1068 Color: 3
Size: 1056 Color: 2
Size: 1056 Color: 2
Size: 1056 Color: 0
Size: 1056 Color: 0
Size: 960 Color: 3
Size: 936 Color: 2
Size: 888 Color: 1

Bin 198: 302 of cap free
Amount of items: 33
Items: 
Size: 928 Color: 2
Size: 864 Color: 2
Size: 856 Color: 1
Size: 854 Color: 1
Size: 832 Color: 0
Size: 768 Color: 4
Size: 768 Color: 1
Size: 768 Color: 0
Size: 704 Color: 1
Size: 672 Color: 0
Size: 624 Color: 2
Size: 584 Color: 1
Size: 554 Color: 1
Size: 548 Color: 1
Size: 512 Color: 2
Size: 512 Color: 2
Size: 512 Color: 0
Size: 504 Color: 3
Size: 502 Color: 2
Size: 496 Color: 0
Size: 480 Color: 0
Size: 472 Color: 3
Size: 464 Color: 3
Size: 464 Color: 1
Size: 440 Color: 4
Size: 436 Color: 4
Size: 432 Color: 4
Size: 424 Color: 4
Size: 424 Color: 3
Size: 416 Color: 3
Size: 400 Color: 3
Size: 396 Color: 0
Size: 384 Color: 4

Bin 199: 14346 of cap free
Amount of items: 14
Items: 
Size: 384 Color: 4
Size: 384 Color: 2
Size: 384 Color: 0
Size: 368 Color: 4
Size: 368 Color: 2
Size: 364 Color: 4
Size: 352 Color: 4
Size: 352 Color: 3
Size: 352 Color: 3
Size: 336 Color: 3
Size: 334 Color: 0
Size: 332 Color: 0
Size: 320 Color: 1
Size: 320 Color: 1

Total size: 3820608
Total free space: 19296

