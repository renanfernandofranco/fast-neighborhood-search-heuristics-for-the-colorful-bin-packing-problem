Capicity Bin: 7880
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 3943 Color: 1
Size: 2382 Color: 2
Size: 1229 Color: 15
Size: 192 Color: 4
Size: 134 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 2
Size: 3084 Color: 3
Size: 608 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 2
Size: 2564 Color: 10
Size: 504 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 6
Size: 2620 Color: 0
Size: 272 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5132 Color: 19
Size: 2412 Color: 2
Size: 336 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 10
Size: 2062 Color: 9
Size: 262 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 0
Size: 2149 Color: 17
Size: 116 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 18
Size: 1380 Color: 17
Size: 742 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 1
Size: 1692 Color: 0
Size: 368 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5884 Color: 19
Size: 1500 Color: 0
Size: 496 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 2
Size: 1724 Color: 12
Size: 160 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 17
Size: 1668 Color: 3
Size: 128 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6222 Color: 13
Size: 1382 Color: 15
Size: 276 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6372 Color: 4
Size: 1080 Color: 9
Size: 428 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 15
Size: 1224 Color: 2
Size: 140 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6527 Color: 1
Size: 1287 Color: 14
Size: 66 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 9
Size: 1226 Color: 1
Size: 106 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 3
Size: 764 Color: 16
Size: 566 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6566 Color: 8
Size: 960 Color: 16
Size: 354 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 6
Size: 981 Color: 4
Size: 244 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 19
Size: 1140 Color: 15
Size: 74 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 0
Size: 836 Color: 9
Size: 352 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 8
Size: 749 Color: 14
Size: 428 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 3
Size: 744 Color: 12
Size: 412 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6774 Color: 11
Size: 954 Color: 13
Size: 152 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6791 Color: 13
Size: 677 Color: 19
Size: 412 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6845 Color: 9
Size: 863 Color: 14
Size: 172 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 10
Size: 718 Color: 7
Size: 304 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 10
Size: 708 Color: 3
Size: 312 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6910 Color: 17
Size: 490 Color: 12
Size: 480 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6922 Color: 1
Size: 774 Color: 15
Size: 184 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6979 Color: 17
Size: 685 Color: 7
Size: 216 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7021 Color: 12
Size: 717 Color: 1
Size: 142 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 3
Size: 662 Color: 9
Size: 196 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 17
Size: 552 Color: 4
Size: 292 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7068 Color: 4
Size: 652 Color: 3
Size: 160 Color: 14

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4937 Color: 3
Size: 2798 Color: 6
Size: 144 Color: 4

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 1
Size: 2806 Color: 5
Size: 150 Color: 15

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 7
Size: 2447 Color: 5
Size: 476 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5303 Color: 7
Size: 2436 Color: 18
Size: 140 Color: 12

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5607 Color: 7
Size: 2100 Color: 15
Size: 172 Color: 17

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6307 Color: 7
Size: 1572 Color: 16

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6371 Color: 15
Size: 1148 Color: 15
Size: 360 Color: 14

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6394 Color: 2
Size: 1357 Color: 12
Size: 128 Color: 8

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6410 Color: 17
Size: 1469 Color: 11

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6418 Color: 15
Size: 1077 Color: 14
Size: 384 Color: 8

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6453 Color: 2
Size: 1362 Color: 5
Size: 64 Color: 12

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6667 Color: 9
Size: 656 Color: 5
Size: 556 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 2
Size: 931 Color: 8
Size: 136 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6983 Color: 7
Size: 564 Color: 2
Size: 332 Color: 18

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 7059 Color: 15
Size: 820 Color: 18

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 7069 Color: 13
Size: 652 Color: 0
Size: 158 Color: 2

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 7077 Color: 6
Size: 802 Color: 15

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 18
Size: 787 Color: 11

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4380 Color: 19
Size: 3282 Color: 8
Size: 216 Color: 4

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 1
Size: 3266 Color: 2
Size: 176 Color: 16

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4514 Color: 9
Size: 3172 Color: 5
Size: 192 Color: 6

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 1
Size: 2052 Color: 3
Size: 112 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 14
Size: 1084 Color: 12
Size: 566 Color: 11

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 1
Size: 1480 Color: 9
Size: 152 Color: 3

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6687 Color: 5
Size: 1191 Color: 15

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 11
Size: 940 Color: 8

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 7012 Color: 15
Size: 866 Color: 7

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 7037 Color: 13
Size: 841 Color: 3

Bin 65: 3 of cap free
Amount of items: 14
Items: 
Size: 684 Color: 18
Size: 671 Color: 19
Size: 660 Color: 7
Size: 656 Color: 14
Size: 656 Color: 1
Size: 652 Color: 1
Size: 648 Color: 16
Size: 648 Color: 13
Size: 632 Color: 4
Size: 560 Color: 12
Size: 504 Color: 18
Size: 356 Color: 6
Size: 348 Color: 11
Size: 202 Color: 18

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 19
Size: 3281 Color: 3
Size: 520 Color: 12

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 5734 Color: 16
Size: 2143 Color: 1

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 5750 Color: 11
Size: 1711 Color: 12
Size: 416 Color: 2

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 13
Size: 1770 Color: 7
Size: 220 Color: 9

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 10
Size: 1167 Color: 17
Size: 32 Color: 5

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 17
Size: 1121 Color: 5

Bin 72: 4 of cap free
Amount of items: 4
Items: 
Size: 3942 Color: 12
Size: 2770 Color: 8
Size: 852 Color: 10
Size: 312 Color: 4

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 4994 Color: 13
Size: 2402 Color: 10
Size: 480 Color: 3

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 14
Size: 854 Color: 4
Size: 32 Color: 0

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 15
Size: 2159 Color: 4
Size: 296 Color: 18

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6312 Color: 0
Size: 1563 Color: 11

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6765 Color: 1
Size: 1110 Color: 4

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 4740 Color: 3
Size: 2444 Color: 10
Size: 690 Color: 15

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 4848 Color: 0
Size: 2536 Color: 19
Size: 490 Color: 3

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 5755 Color: 0
Size: 1895 Color: 10
Size: 224 Color: 3

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6213 Color: 16
Size: 1661 Color: 7

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6992 Color: 19
Size: 882 Color: 13

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 4945 Color: 12
Size: 2928 Color: 7

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 6095 Color: 15
Size: 1554 Color: 2
Size: 224 Color: 4

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 6871 Color: 8
Size: 1002 Color: 18

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 5390 Color: 10
Size: 2482 Color: 2

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 5
Size: 1260 Color: 11

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 7086 Color: 6
Size: 786 Color: 10

Bin 89: 9 of cap free
Amount of items: 3
Items: 
Size: 4489 Color: 10
Size: 3262 Color: 9
Size: 120 Color: 0

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 6937 Color: 2
Size: 934 Color: 6

Bin 91: 9 of cap free
Amount of items: 3
Items: 
Size: 6925 Color: 9
Size: 568 Color: 18
Size: 378 Color: 2

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 3970 Color: 4
Size: 2833 Color: 3
Size: 1067 Color: 17

Bin 93: 12 of cap free
Amount of items: 3
Items: 
Size: 5430 Color: 17
Size: 2078 Color: 2
Size: 360 Color: 5

Bin 94: 12 of cap free
Amount of items: 2
Items: 
Size: 7058 Color: 17
Size: 810 Color: 11

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 5026 Color: 14
Size: 2841 Color: 4

Bin 96: 14 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 6
Size: 2042 Color: 1
Size: 148 Color: 10

Bin 97: 14 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 8
Size: 1782 Color: 16
Size: 272 Color: 3

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 6340 Color: 7
Size: 1526 Color: 18

Bin 99: 15 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 7
Size: 1011 Color: 9
Size: 32 Color: 17

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 14
Size: 1284 Color: 5

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 6900 Color: 5
Size: 964 Color: 8

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 6972 Color: 6
Size: 892 Color: 11

Bin 103: 17 of cap free
Amount of items: 2
Items: 
Size: 6954 Color: 14
Size: 909 Color: 4

Bin 104: 20 of cap free
Amount of items: 3
Items: 
Size: 3948 Color: 5
Size: 3472 Color: 13
Size: 440 Color: 10

Bin 105: 20 of cap free
Amount of items: 3
Items: 
Size: 3954 Color: 11
Size: 3274 Color: 19
Size: 632 Color: 4

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 7
Size: 2158 Color: 2
Size: 296 Color: 12

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 6762 Color: 9
Size: 1098 Color: 19

Bin 108: 22 of cap free
Amount of items: 2
Items: 
Size: 3962 Color: 12
Size: 3896 Color: 2

Bin 109: 24 of cap free
Amount of items: 3
Items: 
Size: 6407 Color: 6
Size: 1391 Color: 14
Size: 58 Color: 5

Bin 110: 24 of cap free
Amount of items: 2
Items: 
Size: 6842 Color: 14
Size: 1014 Color: 5

Bin 111: 26 of cap free
Amount of items: 2
Items: 
Size: 4929 Color: 6
Size: 2925 Color: 18

Bin 112: 29 of cap free
Amount of items: 2
Items: 
Size: 7054 Color: 6
Size: 797 Color: 9

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 6005 Color: 12
Size: 1844 Color: 18

Bin 114: 34 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 19
Size: 1311 Color: 3

Bin 115: 37 of cap free
Amount of items: 2
Items: 
Size: 6119 Color: 11
Size: 1724 Color: 16

Bin 116: 37 of cap free
Amount of items: 2
Items: 
Size: 6601 Color: 3
Size: 1242 Color: 10

Bin 117: 38 of cap free
Amount of items: 3
Items: 
Size: 4526 Color: 19
Size: 3180 Color: 12
Size: 136 Color: 4

Bin 118: 38 of cap free
Amount of items: 2
Items: 
Size: 4558 Color: 3
Size: 3284 Color: 15

Bin 119: 40 of cap free
Amount of items: 2
Items: 
Size: 6050 Color: 18
Size: 1790 Color: 17

Bin 120: 45 of cap free
Amount of items: 2
Items: 
Size: 5374 Color: 4
Size: 2461 Color: 0

Bin 121: 48 of cap free
Amount of items: 29
Items: 
Size: 472 Color: 13
Size: 408 Color: 16
Size: 408 Color: 8
Size: 404 Color: 8
Size: 348 Color: 2
Size: 344 Color: 18
Size: 336 Color: 2
Size: 328 Color: 3
Size: 320 Color: 14
Size: 312 Color: 15
Size: 308 Color: 16
Size: 276 Color: 1
Size: 272 Color: 4
Size: 256 Color: 5
Size: 248 Color: 13
Size: 244 Color: 6
Size: 244 Color: 5
Size: 240 Color: 5
Size: 236 Color: 11
Size: 216 Color: 12
Size: 212 Color: 6
Size: 200 Color: 13
Size: 200 Color: 9
Size: 184 Color: 16
Size: 180 Color: 11
Size: 176 Color: 0
Size: 168 Color: 7
Size: 156 Color: 7
Size: 136 Color: 14

Bin 122: 51 of cap free
Amount of items: 2
Items: 
Size: 5002 Color: 6
Size: 2827 Color: 15

Bin 123: 56 of cap free
Amount of items: 2
Items: 
Size: 6018 Color: 0
Size: 1806 Color: 18

Bin 124: 62 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 10
Size: 2876 Color: 3
Size: 996 Color: 19

Bin 125: 63 of cap free
Amount of items: 2
Items: 
Size: 5364 Color: 8
Size: 2453 Color: 17

Bin 126: 67 of cap free
Amount of items: 3
Items: 
Size: 5309 Color: 12
Size: 1388 Color: 9
Size: 1116 Color: 3

Bin 127: 82 of cap free
Amount of items: 2
Items: 
Size: 6220 Color: 7
Size: 1578 Color: 6

Bin 128: 88 of cap free
Amount of items: 2
Items: 
Size: 5852 Color: 14
Size: 1940 Color: 19

Bin 129: 89 of cap free
Amount of items: 6
Items: 
Size: 3941 Color: 1
Size: 1222 Color: 12
Size: 751 Color: 10
Size: 703 Color: 15
Size: 686 Color: 10
Size: 488 Color: 11

Bin 130: 103 of cap free
Amount of items: 2
Items: 
Size: 6006 Color: 16
Size: 1771 Color: 5

Bin 131: 116 of cap free
Amount of items: 2
Items: 
Size: 4481 Color: 7
Size: 3283 Color: 12

Bin 132: 125 of cap free
Amount of items: 2
Items: 
Size: 4473 Color: 5
Size: 3282 Color: 19

Bin 133: 6068 of cap free
Amount of items: 12
Items: 
Size: 184 Color: 19
Size: 168 Color: 9
Size: 168 Color: 2
Size: 156 Color: 17
Size: 156 Color: 11
Size: 152 Color: 8
Size: 148 Color: 6
Size: 144 Color: 15
Size: 136 Color: 17
Size: 136 Color: 7
Size: 132 Color: 14
Size: 132 Color: 0

Total size: 1040160
Total free space: 7880

