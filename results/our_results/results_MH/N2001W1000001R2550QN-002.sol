Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2001

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 392448 Color: 1561
Size: 344514 Color: 1258
Size: 263039 Color: 290

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 395481 Color: 1582
Size: 307701 Color: 891
Size: 296819 Color: 775

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 408594 Color: 1667
Size: 341316 Color: 1231
Size: 250091 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 379827 Color: 1491
Size: 370053 Color: 1417
Size: 250121 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 384953 Color: 1523
Size: 356779 Color: 1347
Size: 258269 Color: 212

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 400238 Color: 1611
Size: 346235 Color: 1272
Size: 253528 Color: 103

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 407217 Color: 1660
Size: 339788 Color: 1219
Size: 252996 Color: 88

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 388744 Color: 1546
Size: 361116 Color: 1376
Size: 250141 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 375831 Color: 1461
Size: 352279 Color: 1308
Size: 271891 Color: 444

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 367850 Color: 1408
Size: 345291 Color: 1266
Size: 286860 Color: 663

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 404085 Color: 1639
Size: 336690 Color: 1192
Size: 259226 Color: 231

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374957 Color: 1457
Size: 366063 Color: 1396
Size: 258981 Color: 224

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 401827 Color: 1621
Size: 336928 Color: 1193
Size: 261246 Color: 261

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 381710 Color: 1500
Size: 350106 Color: 1300
Size: 268185 Color: 385

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 394115 Color: 1572
Size: 338966 Color: 1210
Size: 266920 Color: 360

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 382593 Color: 1509
Size: 357294 Color: 1349
Size: 260114 Color: 245

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 404057 Color: 1638
Size: 337316 Color: 1196
Size: 258628 Color: 220

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 385501 Color: 1525
Size: 360626 Color: 1370
Size: 253874 Color: 111

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 388508 Color: 1545
Size: 338193 Color: 1202
Size: 273300 Color: 477

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 382062 Color: 1505
Size: 326730 Color: 1098
Size: 291209 Color: 706

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 398034 Color: 1595
Size: 333401 Color: 1163
Size: 268566 Color: 392

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 402462 Color: 1627
Size: 341489 Color: 1233
Size: 256050 Color: 163

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 406321 Color: 1654
Size: 325925 Color: 1087
Size: 267755 Color: 373

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 373199 Color: 1445
Size: 356814 Color: 1348
Size: 269988 Color: 417

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 396793 Color: 1589
Size: 334191 Color: 1171
Size: 269017 Color: 400

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 380945 Color: 1498
Size: 341061 Color: 1228
Size: 277995 Color: 552

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 395032 Color: 1578
Size: 336162 Color: 1188
Size: 268807 Color: 396

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 402101 Color: 1623
Size: 333104 Color: 1159
Size: 264796 Color: 328

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 398510 Color: 1599
Size: 340089 Color: 1222
Size: 261402 Color: 265

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 370106 Color: 1418
Size: 349774 Color: 1298
Size: 280121 Color: 584

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 358297 Color: 1359
Size: 342623 Color: 1240
Size: 299081 Color: 798

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 400644 Color: 1614
Size: 326412 Color: 1092
Size: 272945 Color: 467

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 394998 Color: 1577
Size: 329327 Color: 1130
Size: 275676 Color: 515

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 398180 Color: 1596
Size: 332813 Color: 1155
Size: 269008 Color: 399

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 393172 Color: 1567
Size: 314520 Color: 957
Size: 292309 Color: 724

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 389896 Color: 1551
Size: 337166 Color: 1195
Size: 272939 Color: 466

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 363107 Color: 1384
Size: 360758 Color: 1371
Size: 276136 Color: 526

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 407905 Color: 1661
Size: 339309 Color: 1215
Size: 252787 Color: 82

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 382828 Color: 1510
Size: 326314 Color: 1088
Size: 290859 Color: 703

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 403573 Color: 1634
Size: 316171 Color: 977
Size: 280257 Color: 587

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 407177 Color: 1659
Size: 334428 Color: 1174
Size: 258396 Color: 214

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 393334 Color: 1568
Size: 354529 Color: 1325
Size: 252138 Color: 66

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 378628 Color: 1479
Size: 365259 Color: 1391
Size: 256114 Color: 166

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 386295 Color: 1531
Size: 327357 Color: 1111
Size: 286349 Color: 655

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 369646 Color: 1414
Size: 356640 Color: 1343
Size: 273715 Color: 482

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 406832 Color: 1656
Size: 324262 Color: 1071
Size: 268907 Color: 397

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 408548 Color: 1665
Size: 329750 Color: 1138
Size: 261703 Color: 268

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 397940 Color: 1594
Size: 335146 Color: 1180
Size: 266915 Color: 358

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 394181 Color: 1573
Size: 332986 Color: 1156
Size: 272834 Color: 462

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 374860 Color: 1455
Size: 343279 Color: 1244
Size: 281862 Color: 611

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 377976 Color: 1472
Size: 315887 Color: 974
Size: 306138 Color: 873

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 399476 Color: 1604
Size: 317392 Color: 994
Size: 283133 Color: 625

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 378327 Color: 1478
Size: 355262 Color: 1332
Size: 266412 Color: 351

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 374867 Color: 1456
Size: 355307 Color: 1333
Size: 269827 Color: 415

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 408139 Color: 1662
Size: 327274 Color: 1108
Size: 264588 Color: 323

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 373670 Color: 1449
Size: 348138 Color: 1286
Size: 278193 Color: 554

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 371227 Color: 1427
Size: 339411 Color: 1216
Size: 289363 Color: 687

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 391903 Color: 1557
Size: 331755 Color: 1150
Size: 276343 Color: 529

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 369219 Color: 1413
Size: 350764 Color: 1303
Size: 280018 Color: 583

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 387281 Color: 1536
Size: 334011 Color: 1168
Size: 278709 Color: 567

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 381935 Color: 1503
Size: 355600 Color: 1339
Size: 262466 Color: 278

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 371884 Color: 1432
Size: 335543 Color: 1184
Size: 292574 Color: 726

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 402604 Color: 1630
Size: 322054 Color: 1051
Size: 275343 Color: 507

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 377352 Color: 1469
Size: 316672 Color: 985
Size: 305977 Color: 869

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 403992 Color: 1637
Size: 317389 Color: 993
Size: 278620 Color: 562

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 404119 Color: 1640
Size: 325611 Color: 1084
Size: 270271 Color: 421

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 407099 Color: 1658
Size: 337901 Color: 1201
Size: 255001 Color: 133

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 396284 Color: 1587
Size: 309814 Color: 915
Size: 293903 Color: 745

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 404657 Color: 1644
Size: 339223 Color: 1214
Size: 256121 Color: 167

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 380567 Color: 1494
Size: 334788 Color: 1178
Size: 284646 Color: 642

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 406184 Color: 1652
Size: 335220 Color: 1181
Size: 258597 Color: 219

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 387669 Color: 1542
Size: 334738 Color: 1177
Size: 277594 Color: 545

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 383356 Color: 1515
Size: 361251 Color: 1377
Size: 255394 Color: 145

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 398439 Color: 1598
Size: 326681 Color: 1097
Size: 274881 Color: 500

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 380868 Color: 1497
Size: 340649 Color: 1226
Size: 278484 Color: 559

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 371126 Color: 1425
Size: 318474 Color: 1008
Size: 310401 Color: 917

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 375805 Color: 1460
Size: 366317 Color: 1399
Size: 257879 Color: 203

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 370681 Color: 1422
Size: 329512 Color: 1136
Size: 299808 Color: 804

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 364551 Color: 1388
Size: 355019 Color: 1328
Size: 280431 Color: 590

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 404275 Color: 1641
Size: 319032 Color: 1017
Size: 276694 Color: 532

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 386071 Color: 1528
Size: 328269 Color: 1118
Size: 285661 Color: 648

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 367662 Color: 1405
Size: 359328 Color: 1366
Size: 273011 Color: 468

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 367671 Color: 1407
Size: 322803 Color: 1057
Size: 309527 Color: 910

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 377281 Color: 1467
Size: 367670 Color: 1406
Size: 255050 Color: 136

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 392979 Color: 1566
Size: 348977 Color: 1292
Size: 258045 Color: 206

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 389274 Color: 1547
Size: 358103 Color: 1356
Size: 252624 Color: 79

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 399843 Color: 1607
Size: 347609 Color: 1282
Size: 252549 Color: 77

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 406088 Color: 1651
Size: 334398 Color: 1172
Size: 259515 Color: 237

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 392948 Color: 1565
Size: 345741 Color: 1269
Size: 261312 Color: 264

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 382934 Color: 1511
Size: 359502 Color: 1367
Size: 257565 Color: 194

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 395117 Color: 1579
Size: 335662 Color: 1186
Size: 269222 Color: 403

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 362830 Color: 1382
Size: 352798 Color: 1310
Size: 284373 Color: 638

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 358481 Color: 1362
Size: 335541 Color: 1183
Size: 305979 Color: 870

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 379199 Color: 1482
Size: 343525 Color: 1247
Size: 277277 Color: 539

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 357515 Color: 1352
Size: 356662 Color: 1344
Size: 285824 Color: 650

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 374765 Color: 1454
Size: 357667 Color: 1354
Size: 267569 Color: 370

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 355076 Color: 1329
Size: 352989 Color: 1314
Size: 291936 Color: 721

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 368570 Color: 1412
Size: 356754 Color: 1346
Size: 274677 Color: 496

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 348578 Color: 1290
Size: 348549 Color: 1289
Size: 302874 Color: 842

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 377641 Color: 1470
Size: 347141 Color: 1278
Size: 275219 Color: 504

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 376477 Color: 1464
Size: 314085 Color: 952
Size: 309439 Color: 908

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 369758 Color: 1416
Size: 369717 Color: 1415
Size: 260526 Color: 256

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 379643 Color: 1487
Size: 341651 Color: 1235
Size: 278707 Color: 566

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 370324 Color: 1419
Size: 353964 Color: 1321
Size: 275713 Color: 516

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 378271 Color: 1476
Size: 341261 Color: 1229
Size: 280469 Color: 592

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 400182 Color: 1610
Size: 306799 Color: 882
Size: 293020 Color: 730

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 379793 Color: 1490
Size: 343795 Color: 1251
Size: 276413 Color: 530

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 366191 Color: 1398
Size: 354244 Color: 1323
Size: 279566 Color: 578

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 343300 Color: 1245
Size: 339857 Color: 1221
Size: 316844 Color: 988

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 385642 Color: 1526
Size: 343051 Color: 1242
Size: 271308 Color: 437

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 406915 Color: 1657
Size: 339683 Color: 1218
Size: 253403 Color: 100

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 408642 Color: 1668
Size: 300914 Color: 820
Size: 290445 Color: 697

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 408733 Color: 1669
Size: 304516 Color: 856
Size: 286752 Color: 660

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 408909 Color: 1670
Size: 324091 Color: 1069
Size: 267001 Color: 363

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 409097 Color: 1671
Size: 301065 Color: 821
Size: 289839 Color: 691

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 409290 Color: 1672
Size: 331392 Color: 1147
Size: 259319 Color: 235

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 409531 Color: 1673
Size: 314666 Color: 962
Size: 275804 Color: 520

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 409677 Color: 1674
Size: 321075 Color: 1044
Size: 269249 Color: 405

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 409764 Color: 1675
Size: 326406 Color: 1091
Size: 263831 Color: 305

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 409891 Color: 1676
Size: 333438 Color: 1164
Size: 256672 Color: 179

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 409921 Color: 1677
Size: 297386 Color: 780
Size: 292694 Color: 727

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 410142 Color: 1678
Size: 338226 Color: 1203
Size: 251633 Color: 57

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 410269 Color: 1679
Size: 319122 Color: 1019
Size: 270610 Color: 426

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410378 Color: 1680
Size: 298186 Color: 787
Size: 291437 Color: 714

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 410587 Color: 1682
Size: 329349 Color: 1131
Size: 260065 Color: 244

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 410670 Color: 1683
Size: 323209 Color: 1060
Size: 266122 Color: 347

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 410901 Color: 1684
Size: 305617 Color: 865
Size: 283483 Color: 628

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 410981 Color: 1685
Size: 326736 Color: 1099
Size: 262284 Color: 274

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 410989 Color: 1686
Size: 315205 Color: 969
Size: 273807 Color: 484

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 411244 Color: 1687
Size: 315157 Color: 968
Size: 273600 Color: 481

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 411248 Color: 1688
Size: 309344 Color: 906
Size: 279409 Color: 575

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 411716 Color: 1690
Size: 308636 Color: 901
Size: 279649 Color: 579

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 412323 Color: 1691
Size: 325807 Color: 1086
Size: 261871 Color: 270

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 412456 Color: 1692
Size: 321324 Color: 1045
Size: 266221 Color: 348

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 412487 Color: 1693
Size: 323645 Color: 1065
Size: 263869 Color: 308

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 412630 Color: 1694
Size: 320883 Color: 1039
Size: 266488 Color: 352

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 412677 Color: 1695
Size: 309980 Color: 916
Size: 277344 Color: 541

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 413198 Color: 1697
Size: 310681 Color: 922
Size: 276122 Color: 525

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 413216 Color: 1698
Size: 322505 Color: 1054
Size: 264280 Color: 320

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 413230 Color: 1699
Size: 303291 Color: 846
Size: 283480 Color: 627

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 413599 Color: 1700
Size: 310598 Color: 921
Size: 275804 Color: 519

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 413747 Color: 1701
Size: 294211 Color: 751
Size: 292043 Color: 722

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 414118 Color: 1703
Size: 317192 Color: 990
Size: 268691 Color: 393

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 414403 Color: 1704
Size: 325450 Color: 1082
Size: 260148 Color: 246

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 414846 Color: 1705
Size: 327759 Color: 1115
Size: 257396 Color: 186

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 415646 Color: 1706
Size: 317440 Color: 996
Size: 266915 Color: 359

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 415906 Color: 1707
Size: 311947 Color: 931
Size: 272148 Color: 449

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 415924 Color: 1708
Size: 304716 Color: 859
Size: 279361 Color: 574

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 416805 Color: 1709
Size: 319647 Color: 1023
Size: 263549 Color: 300

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 417136 Color: 1710
Size: 327186 Color: 1106
Size: 255679 Color: 154

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 417834 Color: 1711
Size: 314150 Color: 953
Size: 268017 Color: 380

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 418122 Color: 1712
Size: 321974 Color: 1050
Size: 259905 Color: 240

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 418684 Color: 1713
Size: 296853 Color: 776
Size: 284464 Color: 639

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 419087 Color: 1714
Size: 328234 Color: 1117
Size: 252680 Color: 80

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 419634 Color: 1717
Size: 326820 Color: 1101
Size: 253547 Color: 105

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 419645 Color: 1718
Size: 305701 Color: 866
Size: 274655 Color: 495

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 419774 Color: 1719
Size: 317503 Color: 997
Size: 262724 Color: 286

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 420008 Color: 1720
Size: 316125 Color: 976
Size: 263868 Color: 307

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 420117 Color: 1721
Size: 315894 Color: 975
Size: 263990 Color: 313

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 420281 Color: 1722
Size: 296987 Color: 777
Size: 282733 Color: 619

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 420626 Color: 1723
Size: 329308 Color: 1128
Size: 250067 Color: 6

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 420704 Color: 1724
Size: 314689 Color: 963
Size: 264608 Color: 325

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 420985 Color: 1725
Size: 326632 Color: 1095
Size: 252384 Color: 71

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 421066 Color: 1726
Size: 314991 Color: 965
Size: 263944 Color: 311

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 421100 Color: 1727
Size: 323242 Color: 1062
Size: 255659 Color: 153

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 421118 Color: 1728
Size: 323881 Color: 1067
Size: 255002 Color: 134

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 421195 Color: 1729
Size: 306165 Color: 875
Size: 272641 Color: 455

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 421687 Color: 1730
Size: 315875 Color: 972
Size: 262439 Color: 276

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 421746 Color: 1731
Size: 307821 Color: 894
Size: 270434 Color: 424

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 421926 Color: 1732
Size: 316766 Color: 986
Size: 261309 Color: 263

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 422276 Color: 1733
Size: 305602 Color: 864
Size: 272123 Color: 448

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 423235 Color: 1734
Size: 316544 Color: 982
Size: 260222 Color: 249

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 423315 Color: 1735
Size: 325434 Color: 1081
Size: 251252 Color: 51

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 423467 Color: 1736
Size: 300034 Color: 807
Size: 276500 Color: 531

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 423739 Color: 1737
Size: 319006 Color: 1016
Size: 257256 Color: 184

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 423751 Color: 1738
Size: 322213 Color: 1052
Size: 254037 Color: 115

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 423861 Color: 1739
Size: 317777 Color: 1000
Size: 258363 Color: 213

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 423932 Color: 1740
Size: 293400 Color: 736
Size: 282669 Color: 618

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 423968 Color: 1741
Size: 289361 Color: 686
Size: 286672 Color: 659

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 424140 Color: 1742
Size: 297263 Color: 778
Size: 278598 Color: 561

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 424708 Color: 1743
Size: 302125 Color: 836
Size: 273168 Color: 473

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 424886 Color: 1745
Size: 291482 Color: 716
Size: 283633 Color: 629

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 424926 Color: 1746
Size: 318698 Color: 1009
Size: 256377 Color: 171

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 425746 Color: 1747
Size: 306149 Color: 874
Size: 268106 Color: 382

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 425913 Color: 1748
Size: 287463 Color: 670
Size: 286625 Color: 657

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 426117 Color: 1749
Size: 323427 Color: 1063
Size: 250457 Color: 29

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 426143 Color: 1750
Size: 309304 Color: 905
Size: 264554 Color: 322

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 426189 Color: 1751
Size: 311137 Color: 927
Size: 262675 Color: 283

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 426256 Color: 1752
Size: 295804 Color: 766
Size: 277941 Color: 551

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 426287 Color: 1753
Size: 306412 Color: 880
Size: 267302 Color: 365

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 426300 Color: 1754
Size: 305931 Color: 868
Size: 267770 Color: 374

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 426354 Color: 1755
Size: 317423 Color: 995
Size: 256224 Color: 170

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 426361 Color: 1756
Size: 301615 Color: 829
Size: 272025 Color: 446

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 426417 Color: 1757
Size: 300407 Color: 814
Size: 273177 Color: 475

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 426633 Color: 1758
Size: 312101 Color: 936
Size: 261267 Color: 262

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 427041 Color: 1759
Size: 295276 Color: 762
Size: 277684 Color: 547

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 427940 Color: 1760
Size: 289477 Color: 688
Size: 282584 Color: 617

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 428745 Color: 1761
Size: 289275 Color: 684
Size: 281981 Color: 614

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 428928 Color: 1762
Size: 302906 Color: 844
Size: 268167 Color: 383

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 429031 Color: 1763
Size: 314576 Color: 958
Size: 256394 Color: 172

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 429461 Color: 1764
Size: 293510 Color: 738
Size: 277030 Color: 536

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 429995 Color: 1765
Size: 300228 Color: 811
Size: 269778 Color: 414

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 430468 Color: 1766
Size: 284894 Color: 644
Size: 284639 Color: 641

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 430525 Color: 1767
Size: 300423 Color: 815
Size: 269053 Color: 401

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 430786 Color: 1768
Size: 309647 Color: 911
Size: 259568 Color: 238

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 430982 Color: 1769
Size: 313864 Color: 949
Size: 255155 Color: 138

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 431018 Color: 1770
Size: 288551 Color: 677
Size: 280432 Color: 591

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 431313 Color: 1771
Size: 313297 Color: 944
Size: 255391 Color: 144

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 431331 Color: 1772
Size: 306094 Color: 871
Size: 262576 Color: 280

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 431412 Color: 1773
Size: 293211 Color: 734
Size: 275378 Color: 509

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 431491 Color: 1774
Size: 316350 Color: 978
Size: 252160 Color: 67

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 431527 Color: 1775
Size: 284590 Color: 640
Size: 283884 Color: 633

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 431577 Color: 1776
Size: 292799 Color: 728
Size: 275625 Color: 513

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 432495 Color: 1778
Size: 303656 Color: 849
Size: 263850 Color: 306

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 432789 Color: 1779
Size: 306848 Color: 883
Size: 260364 Color: 254

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 432948 Color: 1780
Size: 294768 Color: 757
Size: 272285 Color: 452

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 433109 Color: 1781
Size: 307822 Color: 895
Size: 259070 Color: 228

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 433229 Color: 1782
Size: 294002 Color: 747
Size: 272770 Color: 460

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 433508 Color: 1783
Size: 291385 Color: 711
Size: 275108 Color: 501

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 433745 Color: 1784
Size: 287980 Color: 674
Size: 278276 Color: 556

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 434649 Color: 1785
Size: 309452 Color: 909
Size: 255900 Color: 161

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 434862 Color: 1786
Size: 314743 Color: 964
Size: 250396 Color: 24

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 435321 Color: 1787
Size: 282763 Color: 620
Size: 281917 Color: 612

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 435373 Color: 1788
Size: 310728 Color: 923
Size: 253900 Color: 113

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 435458 Color: 1789
Size: 314476 Color: 955
Size: 250067 Color: 5

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 435586 Color: 1790
Size: 311516 Color: 928
Size: 252899 Color: 85

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 435607 Color: 1791
Size: 289078 Color: 681
Size: 275316 Color: 506

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 435770 Color: 1792
Size: 291348 Color: 708
Size: 272883 Color: 464

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 435888 Color: 1793
Size: 288882 Color: 680
Size: 275231 Color: 505

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 435942 Color: 1794
Size: 309769 Color: 914
Size: 254290 Color: 122

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 436059 Color: 1795
Size: 299017 Color: 796
Size: 264925 Color: 330

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 436242 Color: 1796
Size: 302992 Color: 845
Size: 260767 Color: 258

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 436265 Color: 1797
Size: 310564 Color: 920
Size: 253172 Color: 94

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 436449 Color: 1798
Size: 298098 Color: 785
Size: 265454 Color: 339

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 436479 Color: 1799
Size: 305390 Color: 862
Size: 258132 Color: 210

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 436766 Color: 1800
Size: 293712 Color: 740
Size: 269523 Color: 410

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 436871 Color: 1801
Size: 293008 Color: 729
Size: 270122 Color: 419

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 437493 Color: 1802
Size: 296237 Color: 769
Size: 266271 Color: 349

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 437901 Color: 1803
Size: 300586 Color: 816
Size: 261514 Color: 267

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 438037 Color: 1804
Size: 285954 Color: 651
Size: 276010 Color: 524

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 438937 Color: 1805
Size: 298169 Color: 786
Size: 262895 Color: 287

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 438989 Color: 1806
Size: 307773 Color: 893
Size: 253239 Color: 96

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 439181 Color: 1807
Size: 295447 Color: 764
Size: 265373 Color: 336

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 439266 Color: 1808
Size: 288017 Color: 675
Size: 272718 Color: 457

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 440394 Color: 1809
Size: 282318 Color: 616
Size: 277289 Color: 540

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 440771 Color: 1810
Size: 280583 Color: 596
Size: 278647 Color: 564

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 441373 Color: 1812
Size: 306337 Color: 878
Size: 252291 Color: 70

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 441539 Color: 1813
Size: 280572 Color: 595
Size: 277890 Color: 549

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 441835 Color: 1814
Size: 299052 Color: 797
Size: 259114 Color: 229

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 441889 Color: 1815
Size: 294026 Color: 748
Size: 264086 Color: 315

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 442589 Color: 1816
Size: 290426 Color: 695
Size: 266986 Color: 362

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 442840 Color: 1817
Size: 291607 Color: 718
Size: 265554 Color: 340

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 443360 Color: 1819
Size: 306169 Color: 876
Size: 250472 Color: 30

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 443691 Color: 1820
Size: 301335 Color: 826
Size: 254975 Color: 132

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 444184 Color: 1821
Size: 279876 Color: 580
Size: 275941 Color: 522

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 444506 Color: 1822
Size: 284837 Color: 643
Size: 270658 Color: 427

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 444519 Color: 1823
Size: 296573 Color: 772
Size: 258909 Color: 223

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 444638 Color: 1824
Size: 294711 Color: 756
Size: 260652 Color: 257

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 444665 Color: 1825
Size: 287971 Color: 673
Size: 267365 Color: 368

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 444694 Color: 1826
Size: 298505 Color: 791
Size: 256802 Color: 181

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 444944 Color: 1827
Size: 277844 Color: 548
Size: 277213 Color: 537

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 445230 Color: 1828
Size: 291581 Color: 717
Size: 263190 Color: 294

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 445273 Color: 1829
Size: 296637 Color: 774
Size: 258091 Color: 209

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 445479 Color: 1830
Size: 294300 Color: 752
Size: 260222 Color: 248

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 445520 Color: 1831
Size: 296348 Color: 771
Size: 258133 Color: 211

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 445568 Color: 1832
Size: 286438 Color: 656
Size: 267995 Color: 379

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 446170 Color: 1833
Size: 303343 Color: 848
Size: 250488 Color: 32

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 446253 Color: 1834
Size: 278982 Color: 569
Size: 274766 Color: 499

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 446623 Color: 1835
Size: 293136 Color: 732
Size: 260242 Color: 250

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 447106 Color: 1836
Size: 288668 Color: 678
Size: 264227 Color: 319

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 447398 Color: 1837
Size: 302113 Color: 835
Size: 250490 Color: 33

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 447556 Color: 1838
Size: 293177 Color: 733
Size: 259268 Color: 234

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 447764 Color: 1839
Size: 297308 Color: 779
Size: 254929 Color: 130

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 447777 Color: 1840
Size: 291931 Color: 720
Size: 260293 Color: 253

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 448099 Color: 1841
Size: 301156 Color: 823
Size: 250746 Color: 39

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 448640 Color: 1842
Size: 297818 Color: 784
Size: 253543 Color: 104

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 448785 Color: 1843
Size: 287398 Color: 669
Size: 263818 Color: 304

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 448831 Color: 1844
Size: 294583 Color: 754
Size: 256587 Color: 177

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 449121 Color: 1845
Size: 282984 Color: 623
Size: 267896 Color: 377

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 449324 Color: 1846
Size: 281450 Color: 604
Size: 269227 Color: 404

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 449837 Color: 1848
Size: 282965 Color: 622
Size: 267199 Color: 364

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 450095 Color: 1849
Size: 281103 Color: 601
Size: 268803 Color: 395

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 450148 Color: 1850
Size: 275721 Color: 517
Size: 274132 Color: 488

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 450497 Color: 1851
Size: 281018 Color: 600
Size: 268486 Color: 391

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 451043 Color: 1852
Size: 294187 Color: 750
Size: 254771 Color: 126

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 451206 Color: 1853
Size: 293102 Color: 731
Size: 255693 Color: 155

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 452284 Color: 1855
Size: 295306 Color: 763
Size: 252411 Color: 74

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 453536 Color: 1856
Size: 283796 Color: 632
Size: 262669 Color: 282

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 453702 Color: 1857
Size: 274532 Color: 492
Size: 271767 Color: 442

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 453874 Color: 1858
Size: 289598 Color: 689
Size: 256529 Color: 176

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 454109 Color: 1859
Size: 287314 Color: 668
Size: 258578 Color: 218

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 454383 Color: 1860
Size: 291427 Color: 712
Size: 254191 Color: 118

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 455118 Color: 1861
Size: 274146 Color: 489
Size: 270737 Color: 430

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 455381 Color: 1862
Size: 272892 Color: 465
Size: 271728 Color: 441

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 455623 Color: 1863
Size: 278993 Color: 570
Size: 265385 Color: 337

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 456441 Color: 1864
Size: 290360 Color: 693
Size: 253200 Color: 95

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 456642 Color: 1865
Size: 290443 Color: 696
Size: 252916 Color: 86

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 457209 Color: 1866
Size: 277214 Color: 538
Size: 265578 Color: 342

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 457747 Color: 1867
Size: 276697 Color: 533
Size: 265557 Color: 341

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 457875 Color: 1868
Size: 279940 Color: 582
Size: 262186 Color: 273

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 458068 Color: 1869
Size: 285423 Color: 647
Size: 256510 Color: 175

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 458354 Color: 1870
Size: 283931 Color: 634
Size: 257716 Color: 199

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 458368 Color: 1871
Size: 287779 Color: 671
Size: 253854 Color: 109

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 458691 Color: 1872
Size: 271305 Color: 436
Size: 270005 Color: 418

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 458775 Color: 1873
Size: 273583 Color: 480
Size: 267643 Color: 371

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 459091 Color: 1874
Size: 271924 Color: 445
Size: 268986 Color: 398

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 459166 Color: 1875
Size: 278155 Color: 553
Size: 262680 Color: 285

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 459238 Color: 1876
Size: 277645 Color: 546
Size: 263118 Color: 293

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 459885 Color: 1877
Size: 273138 Color: 472
Size: 266978 Color: 361

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 460148 Color: 1878
Size: 273021 Color: 469
Size: 266832 Color: 356

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 460359 Color: 1879
Size: 287805 Color: 672
Size: 251837 Color: 61

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 460781 Color: 1880
Size: 270912 Color: 433
Size: 268308 Color: 388

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 460906 Color: 1881
Size: 281519 Color: 605
Size: 257576 Color: 195

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 460919 Color: 1882
Size: 279121 Color: 573
Size: 259961 Color: 242

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 461203 Color: 1883
Size: 273498 Color: 479
Size: 265300 Color: 334

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 461513 Color: 1884
Size: 270435 Color: 425
Size: 268053 Color: 381

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 461588 Color: 1885
Size: 270684 Color: 428
Size: 267729 Color: 372

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 461960 Color: 1886
Size: 275408 Color: 510
Size: 262633 Color: 281

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 462388 Color: 1887
Size: 269347 Color: 407
Size: 268266 Color: 387

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 462515 Color: 1888
Size: 279431 Color: 576
Size: 258055 Color: 207

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 462518 Color: 1889
Size: 281715 Color: 608
Size: 255768 Color: 156

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 462833 Color: 1890
Size: 281301 Color: 602
Size: 255867 Color: 160

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 462908 Color: 1891
Size: 274416 Color: 491
Size: 262677 Color: 284

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 463374 Color: 1892
Size: 268452 Color: 390
Size: 268175 Color: 384

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 463562 Color: 1893
Size: 280509 Color: 593
Size: 255930 Color: 162

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 464040 Color: 1894
Size: 278518 Color: 560
Size: 257443 Color: 189

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 464334 Color: 1895
Size: 271702 Color: 440
Size: 263965 Color: 312

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 464647 Color: 1896
Size: 271699 Color: 439
Size: 263655 Color: 303

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 464935 Color: 1897
Size: 283014 Color: 624
Size: 252052 Color: 64

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 465133 Color: 1898
Size: 268222 Color: 386
Size: 266646 Color: 353

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 465165 Color: 1899
Size: 267952 Color: 378
Size: 266884 Color: 357

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 465285 Color: 1900
Size: 280556 Color: 594
Size: 254160 Color: 117

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 465294 Color: 1901
Size: 271055 Color: 434
Size: 263652 Color: 302

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 465469 Color: 1902
Size: 276903 Color: 534
Size: 257629 Color: 197

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 465514 Color: 1903
Size: 272723 Color: 458
Size: 261764 Color: 269

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 465855 Color: 1905
Size: 279003 Color: 571
Size: 255143 Color: 137

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 465923 Color: 1906
Size: 272670 Color: 456
Size: 261408 Color: 266

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 466263 Color: 1907
Size: 270138 Color: 420
Size: 263600 Color: 301

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 466503 Color: 1908
Size: 280243 Color: 586
Size: 253255 Color: 97

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 466715 Color: 1909
Size: 271301 Color: 435
Size: 261985 Color: 271

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 467005 Color: 1910
Size: 282159 Color: 615
Size: 250837 Color: 40

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 467009 Color: 1911
Size: 269494 Color: 409
Size: 263498 Color: 299

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 467328 Color: 1912
Size: 280693 Color: 599
Size: 251980 Color: 63

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 467479 Color: 1913
Size: 278328 Color: 557
Size: 254194 Color: 119

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 467512 Color: 1914
Size: 281359 Color: 603
Size: 251130 Color: 46

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 468715 Color: 1915
Size: 273209 Color: 476
Size: 258077 Color: 208

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 468829 Color: 1916
Size: 273960 Color: 486
Size: 257212 Color: 182

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 470077 Color: 1917
Size: 273174 Color: 474
Size: 256750 Color: 180

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 470219 Color: 1918
Size: 272204 Color: 450
Size: 257578 Color: 196

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 470612 Color: 1919
Size: 275495 Color: 512
Size: 253894 Color: 112

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 470723 Color: 1920
Size: 273952 Color: 485
Size: 255326 Color: 142

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 471065 Color: 1921
Size: 272849 Color: 463
Size: 256087 Color: 165

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 471402 Color: 1922
Size: 273037 Color: 470
Size: 255562 Color: 150

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 471982 Color: 1923
Size: 273733 Color: 483
Size: 254286 Color: 121

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 472895 Color: 1924
Size: 275788 Color: 518
Size: 251318 Color: 52

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 472920 Color: 1925
Size: 274689 Color: 497
Size: 252392 Color: 73

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 472966 Color: 1926
Size: 271435 Color: 438
Size: 255600 Color: 152

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 473590 Color: 1927
Size: 275965 Color: 523
Size: 250446 Color: 27

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 473929 Color: 1928
Size: 275159 Color: 503
Size: 250913 Color: 42

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 474444 Color: 1929
Size: 273991 Color: 487
Size: 251566 Color: 55

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 475315 Color: 1931
Size: 273467 Color: 478
Size: 251219 Color: 50

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 475352 Color: 1932
Size: 267323 Color: 366
Size: 257326 Color: 185

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 475549 Color: 1933
Size: 274183 Color: 490
Size: 250269 Color: 18

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 475995 Color: 1934
Size: 269733 Color: 413
Size: 254273 Color: 120

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 476276 Color: 1935
Size: 265086 Color: 331
Size: 258639 Color: 222

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 476460 Color: 1936
Size: 269935 Color: 416
Size: 253606 Color: 106

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 476466 Color: 1937
Size: 262520 Color: 279
Size: 261015 Color: 260

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 476672 Color: 1938
Size: 265592 Color: 343
Size: 257737 Color: 200

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 476839 Color: 1939
Size: 265310 Color: 335
Size: 257852 Color: 201

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 477391 Color: 1940
Size: 264184 Color: 317
Size: 258426 Color: 216

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 477937 Color: 1941
Size: 267397 Color: 369
Size: 254667 Color: 124

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 478058 Color: 1942
Size: 271883 Color: 443
Size: 250060 Color: 4

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 478123 Color: 1943
Size: 263373 Color: 296
Size: 258505 Color: 217

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 478151 Color: 1944
Size: 263887 Color: 309
Size: 257963 Color: 205

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 478485 Color: 1945
Size: 269668 Color: 412
Size: 251848 Color: 62

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 478549 Color: 1946
Size: 267843 Color: 375
Size: 253609 Color: 107

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 479036 Color: 1948
Size: 265733 Color: 344
Size: 255232 Color: 141

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 479074 Color: 1949
Size: 265768 Color: 345
Size: 255159 Color: 139

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 479419 Color: 1950
Size: 265789 Color: 346
Size: 254793 Color: 127

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 480394 Color: 1951
Size: 264855 Color: 329
Size: 254752 Color: 125

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 480695 Color: 1952
Size: 266733 Color: 354
Size: 252573 Color: 78

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 481008 Color: 1953
Size: 259936 Color: 241
Size: 259057 Color: 226

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 481866 Color: 1954
Size: 262071 Color: 272
Size: 256064 Color: 164

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 481982 Color: 1955
Size: 264147 Color: 316
Size: 253872 Color: 110

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 483167 Color: 1956
Size: 264014 Color: 314
Size: 252820 Color: 83

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 484007 Color: 1957
Size: 265123 Color: 333
Size: 250871 Color: 41

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 484384 Color: 1958
Size: 262451 Color: 277
Size: 253166 Color: 93

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 484783 Color: 1959
Size: 263493 Color: 298
Size: 251725 Color: 59

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 484798 Color: 1960
Size: 264752 Color: 327
Size: 250451 Color: 28

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 485394 Color: 1961
Size: 259149 Color: 230
Size: 255458 Color: 149

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 485463 Color: 1962
Size: 263941 Color: 310
Size: 250597 Color: 35

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 485551 Color: 1963
Size: 262943 Color: 288
Size: 251507 Color: 54

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 485947 Color: 1964
Size: 259243 Color: 232
Size: 254811 Color: 128

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 486414 Color: 1965
Size: 257456 Color: 190
Size: 256131 Color: 168

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 486717 Color: 1966
Size: 257857 Color: 202
Size: 255427 Color: 147

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 487650 Color: 1967
Size: 260065 Color: 243
Size: 252286 Color: 69

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 487745 Color: 1968
Size: 259256 Color: 233
Size: 253000 Color: 89

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 488254 Color: 1969
Size: 258637 Color: 221
Size: 253110 Color: 92

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 488730 Color: 1970
Size: 257478 Color: 192
Size: 253793 Color: 108

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 489490 Color: 1971
Size: 260268 Color: 251
Size: 250243 Color: 17

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 489995 Color: 1972
Size: 258407 Color: 215
Size: 251599 Color: 56

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 490556 Color: 1974
Size: 259051 Color: 225
Size: 250394 Color: 23

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 490703 Color: 1975
Size: 255200 Color: 140
Size: 254098 Color: 116

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 491199 Color: 1976
Size: 255350 Color: 143
Size: 253452 Color: 101

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 491217 Color: 1977
Size: 255444 Color: 148
Size: 253340 Color: 98

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 491840 Color: 1978
Size: 257229 Color: 183
Size: 250932 Color: 43

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 492192 Color: 1979
Size: 254838 Color: 129
Size: 252971 Color: 87

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 492446 Color: 1980
Size: 255849 Color: 159
Size: 251706 Color: 58

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 492472 Color: 1981
Size: 255018 Color: 135
Size: 252511 Color: 76

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 492576 Color: 1982
Size: 254332 Color: 123
Size: 253093 Color: 91

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 492986 Color: 1983
Size: 256656 Color: 178
Size: 250359 Color: 21

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 493126 Color: 1984
Size: 255787 Color: 158
Size: 251088 Color: 44

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 493763 Color: 1985
Size: 254026 Color: 114
Size: 252212 Color: 68

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 493856 Color: 1986
Size: 254946 Color: 131
Size: 251199 Color: 49

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 495377 Color: 1987
Size: 252833 Color: 84
Size: 251791 Color: 60

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 496253 Color: 1988
Size: 253008 Color: 90
Size: 250740 Color: 38

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 496577 Color: 1989
Size: 253400 Color: 99
Size: 250024 Color: 2

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 497359 Color: 1990
Size: 251471 Color: 53
Size: 251171 Color: 47

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 498581 Color: 1991
Size: 251121 Color: 45
Size: 250299 Color: 19

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 498990 Color: 1992
Size: 250646 Color: 36
Size: 250365 Color: 22

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 499307 Color: 1993
Size: 250484 Color: 31
Size: 250210 Color: 15

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 499362 Color: 1994
Size: 250410 Color: 25
Size: 250229 Color: 16

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 499466 Color: 1995
Size: 250420 Color: 26
Size: 250115 Color: 9

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 499558 Color: 1996
Size: 250325 Color: 20
Size: 250118 Color: 10

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 499827 Color: 1999
Size: 250144 Color: 14
Size: 250030 Color: 3

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 499874 Color: 2000
Size: 250106 Color: 8
Size: 250021 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 410418 Color: 1681
Size: 310943 Color: 924
Size: 278639 Color: 563

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 411628 Color: 1689
Size: 312084 Color: 935
Size: 276288 Color: 527

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 412855 Color: 1696
Size: 309233 Color: 904
Size: 277912 Color: 550

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 419259 Color: 1715
Size: 293639 Color: 739
Size: 287102 Color: 665

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 419418 Color: 1716
Size: 302245 Color: 838
Size: 278337 Color: 558

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 424719 Color: 1744
Size: 289324 Color: 685
Size: 285957 Color: 652

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 431993 Color: 1777
Size: 307736 Color: 892
Size: 260271 Color: 252

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 440978 Color: 1811
Size: 301501 Color: 827
Size: 257521 Color: 193

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 443229 Color: 1818
Size: 304380 Color: 854
Size: 252391 Color: 72

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 449349 Color: 1847
Size: 286047 Color: 654
Size: 264604 Color: 324

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 451260 Color: 1854
Size: 283296 Color: 626
Size: 265444 Color: 338

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 465777 Color: 1904
Size: 283651 Color: 630
Size: 250572 Color: 34

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 478658 Color: 1947
Size: 263389 Color: 297
Size: 257953 Color: 204

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 383519 Color: 1517
Size: 347728 Color: 1283
Size: 268753 Color: 394

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 399444 Color: 1603
Size: 344779 Color: 1261
Size: 255777 Color: 157

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 395361 Color: 1581
Size: 317566 Color: 998
Size: 287073 Color: 664

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 405404 Color: 1648
Size: 304545 Color: 857
Size: 290051 Color: 692

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 385085 Color: 1524
Size: 362469 Color: 1381
Size: 252446 Color: 75

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 352883 Color: 1312
Size: 333595 Color: 1166
Size: 313522 Color: 948

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 400068 Color: 1609
Size: 329120 Color: 1124
Size: 270812 Color: 431

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 389355 Color: 1549
Size: 326532 Color: 1093
Size: 284113 Color: 637

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 396983 Color: 1591
Size: 350928 Color: 1305
Size: 252089 Color: 65

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 403710 Color: 1635
Size: 325567 Color: 1083
Size: 270723 Color: 429

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 397201 Color: 1592
Size: 346398 Color: 1274
Size: 256401 Color: 173

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 340878 Color: 1227
Size: 337413 Color: 1198
Size: 321709 Color: 1048

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 352708 Color: 1309
Size: 343990 Color: 1253
Size: 303302 Color: 847

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 378792 Color: 1480
Size: 319995 Color: 1026
Size: 301213 Color: 824

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 394035 Color: 1571
Size: 320236 Color: 1032
Size: 285729 Color: 649

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 383945 Color: 1519
Size: 309020 Color: 903
Size: 307035 Color: 884

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 372894 Color: 1444
Size: 366635 Color: 1402
Size: 260471 Color: 255

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 374041 Color: 1451
Size: 345702 Color: 1268
Size: 280257 Color: 588

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 358193 Color: 1358
Size: 351428 Color: 1307
Size: 290379 Color: 694

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 386569 Color: 1533
Size: 322611 Color: 1056
Size: 290820 Color: 701

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 375111 Color: 1458
Size: 328855 Color: 1123
Size: 296034 Color: 768

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 373658 Color: 1448
Size: 367277 Color: 1404
Size: 259065 Color: 227

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 372763 Color: 1443
Size: 362900 Color: 1383
Size: 264337 Color: 321

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 350159 Color: 1301
Size: 336669 Color: 1190
Size: 313172 Color: 943

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 400257 Color: 1612
Size: 329468 Color: 1134
Size: 270275 Color: 422

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 365889 Color: 1395
Size: 365662 Color: 1394
Size: 268449 Color: 389

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 400760 Color: 1616
Size: 343833 Color: 1252
Size: 255407 Color: 146

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 364263 Color: 1386
Size: 363663 Color: 1385
Size: 272074 Color: 447

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 371318 Color: 1428
Size: 352801 Color: 1311
Size: 275881 Color: 521

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 371887 Color: 1433
Size: 341295 Color: 1230
Size: 286818 Color: 661

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 399324 Color: 1602
Size: 318725 Color: 1010
Size: 281951 Color: 613

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 353123 Color: 1315
Size: 347082 Color: 1277
Size: 299795 Color: 803

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 353153 Color: 1317
Size: 326649 Color: 1096
Size: 320198 Color: 1031

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 383390 Color: 1516
Size: 349285 Color: 1296
Size: 267325 Color: 367

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 379217 Color: 1483
Size: 338933 Color: 1209
Size: 281850 Color: 610

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 404884 Color: 1647
Size: 322382 Color: 1053
Size: 272734 Color: 459

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 372516 Color: 1438
Size: 327262 Color: 1107
Size: 300222 Color: 810

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 490066 Color: 1973
Size: 256472 Color: 174
Size: 253461 Color: 102

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 394948 Color: 1576
Size: 341981 Color: 1236
Size: 263070 Color: 291

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 408248 Color: 1664
Size: 327102 Color: 1104
Size: 264649 Color: 326

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 402230 Color: 1625
Size: 337583 Color: 1199
Size: 260186 Color: 247

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 413851 Color: 1702
Size: 294672 Color: 755
Size: 291476 Color: 715

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 396203 Color: 1584
Size: 334164 Color: 1170
Size: 269632 Color: 411

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 371955 Color: 1434
Size: 355812 Color: 1340
Size: 272232 Color: 451

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 375706 Color: 1459
Size: 354821 Color: 1327
Size: 269472 Color: 408

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 378267 Color: 1475
Size: 330939 Color: 1146
Size: 290793 Color: 699

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 379758 Color: 1489
Size: 329424 Color: 1132
Size: 290817 Color: 700

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 379242 Color: 1485
Size: 319668 Color: 1024
Size: 301089 Color: 822

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 405430 Color: 1649
Size: 300731 Color: 818
Size: 293838 Color: 743

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 396230 Color: 1586
Size: 323181 Color: 1058
Size: 280588 Color: 597

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 343709 Color: 1248
Size: 333080 Color: 1158
Size: 323210 Color: 1061

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 396816 Color: 1590
Size: 319196 Color: 1021
Size: 283987 Color: 635

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 402386 Color: 1626
Size: 336663 Color: 1189
Size: 260950 Color: 259

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 372331 Color: 1437
Size: 360862 Color: 1374
Size: 266806 Color: 355

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 347484 Color: 1281
Size: 336043 Color: 1187
Size: 316472 Color: 980

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 353922 Color: 1320
Size: 340621 Color: 1225
Size: 305456 Color: 863

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 345009 Color: 1263
Size: 344026 Color: 1254
Size: 310964 Color: 925

Bin 497: 3 of cap free
Amount of items: 3
Items: 
Size: 404790 Color: 1646
Size: 339069 Color: 1211
Size: 256139 Color: 169

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 401519 Color: 1620
Size: 335368 Color: 1182
Size: 263111 Color: 292

Bin 499: 3 of cap free
Amount of items: 3
Items: 
Size: 380671 Color: 1496
Size: 346548 Color: 1275
Size: 272779 Color: 461

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 380187 Color: 1493
Size: 340332 Color: 1224
Size: 279479 Color: 577

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 408216 Color: 1663
Size: 332395 Color: 1153
Size: 259387 Color: 236

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 396344 Color: 1588
Size: 334565 Color: 1176
Size: 269089 Color: 402

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 406376 Color: 1655
Size: 315424 Color: 970
Size: 278198 Color: 555

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 400527 Color: 1613
Size: 326358 Color: 1090
Size: 273113 Color: 471

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 380592 Color: 1495
Size: 343743 Color: 1249
Size: 275663 Color: 514

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 387542 Color: 1540
Size: 321497 Color: 1046
Size: 290959 Color: 704

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 398217 Color: 1597
Size: 314636 Color: 960
Size: 287145 Color: 666

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 381940 Color: 1504
Size: 320383 Color: 1034
Size: 297675 Color: 782

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 360611 Color: 1369
Size: 339181 Color: 1213
Size: 300206 Color: 809

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 375967 Color: 1462
Size: 316444 Color: 979
Size: 307587 Color: 890

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 370992 Color: 1424
Size: 329140 Color: 1126
Size: 299866 Color: 806

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 372155 Color: 1436
Size: 315018 Color: 966
Size: 312825 Color: 942

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 381920 Color: 1502
Size: 347740 Color: 1284
Size: 270338 Color: 423

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 390656 Color: 1553
Size: 331820 Color: 1151
Size: 277522 Color: 544

Bin 515: 4 of cap free
Amount of items: 3
Items: 
Size: 392891 Color: 1563
Size: 332519 Color: 1154
Size: 274587 Color: 493

Bin 516: 4 of cap free
Amount of items: 3
Items: 
Size: 391499 Color: 1555
Size: 326880 Color: 1102
Size: 281618 Color: 606

Bin 517: 4 of cap free
Amount of items: 3
Items: 
Size: 388026 Color: 1544
Size: 324753 Color: 1074
Size: 287218 Color: 667

Bin 518: 4 of cap free
Amount of items: 3
Items: 
Size: 389633 Color: 1550
Size: 359183 Color: 1365
Size: 251181 Color: 48

Bin 519: 4 of cap free
Amount of items: 3
Items: 
Size: 380999 Color: 1499
Size: 320146 Color: 1030
Size: 298852 Color: 795

Bin 520: 4 of cap free
Amount of items: 3
Items: 
Size: 378830 Color: 1481
Size: 318854 Color: 1012
Size: 302313 Color: 839

Bin 521: 4 of cap free
Amount of items: 3
Items: 
Size: 372606 Color: 1440
Size: 314610 Color: 959
Size: 312781 Color: 941

Bin 522: 4 of cap free
Amount of items: 3
Items: 
Size: 374574 Color: 1453
Size: 326754 Color: 1100
Size: 298669 Color: 793

Bin 523: 4 of cap free
Amount of items: 3
Items: 
Size: 349598 Color: 1297
Size: 329473 Color: 1135
Size: 320926 Color: 1041

Bin 524: 4 of cap free
Amount of items: 3
Items: 
Size: 365073 Color: 1390
Size: 330277 Color: 1142
Size: 304647 Color: 858

Bin 525: 4 of cap free
Amount of items: 3
Items: 
Size: 398953 Color: 1601
Size: 343369 Color: 1246
Size: 257675 Color: 198

Bin 526: 4 of cap free
Amount of items: 3
Items: 
Size: 353669 Color: 1318
Size: 352959 Color: 1313
Size: 293369 Color: 735

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 367136 Color: 1403
Size: 324775 Color: 1075
Size: 308086 Color: 896

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 353134 Color: 1316
Size: 325150 Color: 1080
Size: 321713 Color: 1049

Bin 529: 5 of cap free
Amount of items: 3
Items: 
Size: 386675 Color: 1534
Size: 321027 Color: 1042
Size: 292294 Color: 723

Bin 530: 5 of cap free
Amount of items: 3
Items: 
Size: 392898 Color: 1564
Size: 343753 Color: 1250
Size: 263345 Color: 295

Bin 531: 5 of cap free
Amount of items: 3
Items: 
Size: 382468 Color: 1507
Size: 317330 Color: 991
Size: 300198 Color: 808

Bin 532: 5 of cap free
Amount of items: 3
Items: 
Size: 387361 Color: 1538
Size: 312331 Color: 938
Size: 300304 Color: 813

Bin 533: 5 of cap free
Amount of items: 3
Items: 
Size: 386702 Color: 1535
Size: 314660 Color: 961
Size: 298634 Color: 792

Bin 534: 5 of cap free
Amount of items: 3
Items: 
Size: 386080 Color: 1529
Size: 315489 Color: 971
Size: 298427 Color: 789

Bin 535: 5 of cap free
Amount of items: 3
Items: 
Size: 408587 Color: 1666
Size: 302211 Color: 837
Size: 289198 Color: 683

Bin 536: 5 of cap free
Amount of items: 3
Items: 
Size: 394827 Color: 1575
Size: 312674 Color: 940
Size: 292495 Color: 725

Bin 537: 5 of cap free
Amount of items: 3
Items: 
Size: 376868 Color: 1465
Size: 331755 Color: 1149
Size: 291373 Color: 710

Bin 538: 5 of cap free
Amount of items: 3
Items: 
Size: 360914 Color: 1375
Size: 355426 Color: 1335
Size: 283656 Color: 631

Bin 539: 6 of cap free
Amount of items: 3
Items: 
Size: 391917 Color: 1558
Size: 342963 Color: 1241
Size: 265115 Color: 332

Bin 540: 6 of cap free
Amount of items: 3
Items: 
Size: 381884 Color: 1501
Size: 316526 Color: 981
Size: 301585 Color: 828

Bin 541: 6 of cap free
Amount of items: 3
Items: 
Size: 402181 Color: 1624
Size: 318889 Color: 1014
Size: 278925 Color: 568

Bin 542: 6 of cap free
Amount of items: 3
Items: 
Size: 371528 Color: 1430
Size: 316797 Color: 987
Size: 311670 Color: 929

Bin 543: 6 of cap free
Amount of items: 3
Items: 
Size: 372721 Color: 1442
Size: 317581 Color: 999
Size: 309693 Color: 912

Bin 544: 6 of cap free
Amount of items: 3
Items: 
Size: 371137 Color: 1426
Size: 319103 Color: 1018
Size: 309755 Color: 913

Bin 545: 6 of cap free
Amount of items: 3
Items: 
Size: 399642 Color: 1605
Size: 324918 Color: 1077
Size: 275435 Color: 511

Bin 546: 6 of cap free
Amount of items: 3
Items: 
Size: 338423 Color: 1205
Size: 334069 Color: 1169
Size: 327503 Color: 1112

Bin 547: 6 of cap free
Amount of items: 3
Items: 
Size: 366397 Color: 1400
Size: 327305 Color: 1109
Size: 306293 Color: 877

Bin 548: 6 of cap free
Amount of items: 3
Items: 
Size: 377300 Color: 1468
Size: 316561 Color: 983
Size: 306134 Color: 872

Bin 549: 6 of cap free
Amount of items: 3
Items: 
Size: 392190 Color: 1559
Size: 357664 Color: 1353
Size: 250141 Color: 12

Bin 550: 6 of cap free
Amount of items: 3
Items: 
Size: 384831 Color: 1522
Size: 355564 Color: 1336
Size: 259600 Color: 239

Bin 551: 6 of cap free
Amount of items: 3
Items: 
Size: 365413 Color: 1392
Size: 354413 Color: 1324
Size: 280169 Color: 585

Bin 552: 6 of cap free
Amount of items: 3
Items: 
Size: 373372 Color: 1446
Size: 324780 Color: 1076
Size: 301843 Color: 833

Bin 553: 6 of cap free
Amount of items: 3
Items: 
Size: 393645 Color: 1570
Size: 315088 Color: 967
Size: 291262 Color: 707

Bin 554: 6 of cap free
Amount of items: 3
Items: 
Size: 379409 Color: 1486
Size: 345983 Color: 1271
Size: 274603 Color: 494

Bin 555: 7 of cap free
Amount of items: 3
Items: 
Size: 402050 Color: 1622
Size: 347233 Color: 1279
Size: 250711 Color: 37

Bin 556: 7 of cap free
Amount of items: 3
Items: 
Size: 400022 Color: 1608
Size: 329152 Color: 1127
Size: 270820 Color: 432

Bin 557: 7 of cap free
Amount of items: 3
Items: 
Size: 396219 Color: 1585
Size: 334515 Color: 1175
Size: 269260 Color: 406

Bin 558: 7 of cap free
Amount of items: 3
Items: 
Size: 386087 Color: 1530
Size: 324150 Color: 1070
Size: 289757 Color: 690

Bin 559: 7 of cap free
Amount of items: 3
Items: 
Size: 380117 Color: 1492
Size: 318044 Color: 1005
Size: 301833 Color: 832

Bin 560: 7 of cap free
Amount of items: 3
Items: 
Size: 379742 Color: 1488
Size: 319497 Color: 1022
Size: 300755 Color: 819

Bin 561: 7 of cap free
Amount of items: 3
Items: 
Size: 347483 Color: 1280
Size: 331473 Color: 1148
Size: 321038 Color: 1043

Bin 562: 7 of cap free
Amount of items: 3
Items: 
Size: 386437 Color: 1532
Size: 307191 Color: 886
Size: 306366 Color: 879

Bin 563: 7 of cap free
Amount of items: 3
Items: 
Size: 358779 Color: 1363
Size: 339432 Color: 1217
Size: 301783 Color: 831

Bin 564: 7 of cap free
Amount of items: 3
Items: 
Size: 346237 Color: 1273
Size: 340259 Color: 1223
Size: 313498 Color: 947

Bin 565: 8 of cap free
Amount of items: 3
Items: 
Size: 405870 Color: 1650
Size: 344106 Color: 1255
Size: 250017 Color: 0

Bin 566: 8 of cap free
Amount of items: 3
Items: 
Size: 372113 Color: 1435
Size: 324990 Color: 1078
Size: 302890 Color: 843

Bin 567: 8 of cap free
Amount of items: 3
Items: 
Size: 400647 Color: 1615
Size: 335119 Color: 1179
Size: 264227 Color: 318

Bin 568: 8 of cap free
Amount of items: 3
Items: 
Size: 384803 Color: 1521
Size: 330151 Color: 1139
Size: 285039 Color: 645

Bin 569: 8 of cap free
Amount of items: 3
Items: 
Size: 401264 Color: 1618
Size: 303770 Color: 852
Size: 294959 Color: 759

Bin 570: 8 of cap free
Amount of items: 3
Items: 
Size: 378294 Color: 1477
Size: 330497 Color: 1143
Size: 291202 Color: 705

Bin 571: 8 of cap free
Amount of items: 3
Items: 
Size: 359101 Color: 1364
Size: 333630 Color: 1167
Size: 307262 Color: 887

Bin 572: 8 of cap free
Amount of items: 3
Items: 
Size: 344534 Color: 1259
Size: 336684 Color: 1191
Size: 318775 Color: 1011

Bin 573: 9 of cap free
Amount of items: 3
Items: 
Size: 350944 Color: 1306
Size: 350821 Color: 1304
Size: 298227 Color: 788

Bin 574: 10 of cap free
Amount of items: 3
Items: 
Size: 403840 Color: 1636
Size: 315884 Color: 973
Size: 280267 Color: 589

Bin 575: 10 of cap free
Amount of items: 3
Items: 
Size: 402593 Color: 1629
Size: 334419 Color: 1173
Size: 262979 Color: 289

Bin 576: 10 of cap free
Amount of items: 3
Items: 
Size: 383226 Color: 1514
Size: 344323 Color: 1257
Size: 272442 Color: 454

Bin 577: 10 of cap free
Amount of items: 3
Items: 
Size: 402471 Color: 1628
Size: 302475 Color: 840
Size: 295045 Color: 760

Bin 578: 10 of cap free
Amount of items: 3
Items: 
Size: 364702 Color: 1389
Size: 317952 Color: 1004
Size: 317337 Color: 992

Bin 579: 10 of cap free
Amount of items: 3
Items: 
Size: 357987 Color: 1355
Size: 347972 Color: 1285
Size: 294032 Color: 749

Bin 580: 10 of cap free
Amount of items: 3
Items: 
Size: 391267 Color: 1554
Size: 328819 Color: 1122
Size: 279905 Color: 581

Bin 581: 11 of cap free
Amount of items: 3
Items: 
Size: 395990 Color: 1583
Size: 303769 Color: 851
Size: 300231 Color: 812

Bin 582: 11 of cap free
Amount of items: 3
Items: 
Size: 394362 Color: 1574
Size: 329315 Color: 1129
Size: 276313 Color: 528

Bin 583: 11 of cap free
Amount of items: 3
Items: 
Size: 371500 Color: 1429
Size: 314488 Color: 956
Size: 314002 Color: 951

Bin 584: 11 of cap free
Amount of items: 3
Items: 
Size: 357489 Color: 1351
Size: 330683 Color: 1144
Size: 311818 Color: 930

Bin 585: 12 of cap free
Amount of items: 3
Items: 
Size: 404709 Color: 1645
Size: 337812 Color: 1200
Size: 257468 Color: 191

Bin 586: 12 of cap free
Amount of items: 3
Items: 
Size: 389347 Color: 1548
Size: 333136 Color: 1161
Size: 277506 Color: 543

Bin 587: 13 of cap free
Amount of items: 3
Items: 
Size: 379240 Color: 1484
Size: 318066 Color: 1006
Size: 302682 Color: 841

Bin 588: 13 of cap free
Amount of items: 3
Items: 
Size: 393617 Color: 1569
Size: 305098 Color: 860
Size: 301273 Color: 825

Bin 589: 13 of cap free
Amount of items: 3
Items: 
Size: 382257 Color: 1506
Size: 324323 Color: 1072
Size: 293408 Color: 737

Bin 590: 13 of cap free
Amount of items: 3
Items: 
Size: 342221 Color: 1237
Size: 329125 Color: 1125
Size: 328642 Color: 1120

Bin 591: 13 of cap free
Amount of items: 3
Items: 
Size: 383064 Color: 1512
Size: 354605 Color: 1326
Size: 262319 Color: 275

Bin 592: 14 of cap free
Amount of items: 3
Items: 
Size: 349199 Color: 1294
Size: 342539 Color: 1239
Size: 308249 Color: 898

Bin 593: 14 of cap free
Amount of items: 3
Items: 
Size: 368455 Color: 1410
Size: 318903 Color: 1015
Size: 312629 Color: 939

Bin 594: 14 of cap free
Amount of items: 3
Items: 
Size: 370910 Color: 1423
Size: 329553 Color: 1137
Size: 299524 Color: 801

Bin 595: 14 of cap free
Amount of items: 3
Items: 
Size: 356730 Color: 1345
Size: 356611 Color: 1342
Size: 286646 Color: 658

Bin 596: 14 of cap free
Amount of items: 3
Items: 
Size: 361312 Color: 1379
Size: 350421 Color: 1302
Size: 288254 Color: 676

Bin 597: 15 of cap free
Amount of items: 3
Items: 
Size: 391801 Color: 1556
Size: 333045 Color: 1157
Size: 275140 Color: 502

Bin 598: 15 of cap free
Amount of items: 3
Items: 
Size: 373755 Color: 1450
Size: 320367 Color: 1033
Size: 305864 Color: 867

Bin 599: 16 of cap free
Amount of items: 3
Items: 
Size: 387551 Color: 1541
Size: 327106 Color: 1105
Size: 285328 Color: 646

Bin 600: 16 of cap free
Amount of items: 3
Items: 
Size: 365471 Color: 1393
Size: 355425 Color: 1334
Size: 279089 Color: 572

Bin 601: 16 of cap free
Amount of items: 3
Items: 
Size: 349241 Color: 1295
Size: 349090 Color: 1293
Size: 301654 Color: 830

Bin 602: 17 of cap free
Amount of items: 3
Items: 
Size: 382509 Color: 1508
Size: 320857 Color: 1038
Size: 296618 Color: 773

Bin 603: 17 of cap free
Amount of items: 3
Items: 
Size: 374497 Color: 1452
Size: 313437 Color: 946
Size: 312050 Color: 934

Bin 604: 17 of cap free
Amount of items: 3
Items: 
Size: 360847 Color: 1373
Size: 357299 Color: 1350
Size: 281838 Color: 609

Bin 605: 18 of cap free
Amount of items: 3
Items: 
Size: 404380 Color: 1642
Size: 313934 Color: 950
Size: 281669 Color: 607

Bin 606: 18 of cap free
Amount of items: 3
Items: 
Size: 392709 Color: 1562
Size: 326612 Color: 1094
Size: 280662 Color: 598

Bin 607: 18 of cap free
Amount of items: 3
Items: 
Size: 360797 Color: 1372
Size: 360499 Color: 1368
Size: 278687 Color: 565

Bin 608: 19 of cap free
Amount of items: 3
Items: 
Size: 387292 Color: 1537
Size: 337317 Color: 1197
Size: 275373 Color: 508

Bin 609: 19 of cap free
Amount of items: 3
Items: 
Size: 372531 Color: 1439
Size: 320107 Color: 1027
Size: 307344 Color: 889

Bin 610: 19 of cap free
Amount of items: 3
Items: 
Size: 349775 Color: 1299
Size: 348287 Color: 1288
Size: 301920 Color: 834

Bin 611: 20 of cap free
Amount of items: 3
Items: 
Size: 364548 Color: 1387
Size: 341504 Color: 1234
Size: 293929 Color: 746

Bin 612: 21 of cap free
Amount of items: 3
Items: 
Size: 371623 Color: 1431
Size: 317916 Color: 1003
Size: 310441 Color: 918

Bin 613: 22 of cap free
Amount of items: 3
Items: 
Size: 373535 Color: 1447
Size: 328725 Color: 1121
Size: 297719 Color: 783

Bin 614: 23 of cap free
Amount of items: 3
Items: 
Size: 368549 Color: 1411
Size: 317173 Color: 989
Size: 314256 Color: 954

Bin 615: 24 of cap free
Amount of items: 3
Items: 
Size: 338799 Color: 1208
Size: 338604 Color: 1207
Size: 322574 Color: 1055

Bin 616: 25 of cap free
Amount of items: 3
Items: 
Size: 377055 Color: 1466
Size: 348159 Color: 1287
Size: 274762 Color: 498

Bin 617: 25 of cap free
Amount of items: 3
Items: 
Size: 383947 Color: 1520
Size: 339111 Color: 1212
Size: 276918 Color: 535

Bin 618: 26 of cap free
Amount of items: 3
Items: 
Size: 402744 Color: 1632
Size: 339796 Color: 1220
Size: 257435 Color: 188

Bin 619: 26 of cap free
Amount of items: 3
Items: 
Size: 398614 Color: 1600
Size: 310529 Color: 919
Size: 290832 Color: 702

Bin 620: 26 of cap free
Amount of items: 3
Items: 
Size: 358299 Color: 1360
Size: 323887 Color: 1068
Size: 317789 Color: 1001

Bin 621: 28 of cap free
Amount of items: 3
Items: 
Size: 377964 Color: 1471
Size: 354154 Color: 1322
Size: 267855 Color: 376

Bin 622: 28 of cap free
Amount of items: 3
Items: 
Size: 383177 Color: 1513
Size: 320492 Color: 1036
Size: 296304 Color: 770

Bin 623: 28 of cap free
Amount of items: 3
Items: 
Size: 355579 Color: 1338
Size: 355577 Color: 1337
Size: 288817 Color: 679

Bin 624: 29 of cap free
Amount of items: 3
Items: 
Size: 370351 Color: 1420
Size: 330162 Color: 1140
Size: 299459 Color: 800

Bin 625: 30 of cap free
Amount of items: 3
Items: 
Size: 390006 Color: 1552
Size: 309357 Color: 907
Size: 300608 Color: 817

Bin 626: 31 of cap free
Amount of items: 3
Items: 
Size: 402876 Color: 1633
Size: 330799 Color: 1145
Size: 266295 Color: 350

Bin 627: 32 of cap free
Amount of items: 3
Items: 
Size: 366486 Color: 1401
Size: 324521 Color: 1073
Size: 308962 Color: 902

Bin 628: 33 of cap free
Amount of items: 3
Items: 
Size: 395177 Color: 1580
Size: 327348 Color: 1110
Size: 277443 Color: 542

Bin 629: 37 of cap free
Amount of items: 3
Items: 
Size: 355086 Color: 1330
Size: 325126 Color: 1079
Size: 319752 Color: 1025

Bin 630: 42 of cap free
Amount of items: 3
Items: 
Size: 392278 Color: 1560
Size: 313313 Color: 945
Size: 294368 Color: 753

Bin 631: 44 of cap free
Amount of items: 3
Items: 
Size: 383697 Color: 1518
Size: 332198 Color: 1152
Size: 284062 Color: 636

Bin 632: 48 of cap free
Amount of items: 3
Items: 
Size: 346567 Color: 1276
Size: 335543 Color: 1185
Size: 317843 Color: 1002

Bin 633: 51 of cap free
Amount of items: 3
Items: 
Size: 401163 Color: 1617
Size: 311950 Color: 932
Size: 286837 Color: 662

Bin 634: 53 of cap free
Amount of items: 3
Items: 
Size: 345224 Color: 1265
Size: 328396 Color: 1119
Size: 326328 Color: 1089

Bin 635: 55 of cap free
Amount of items: 3
Items: 
Size: 338545 Color: 1206
Size: 333586 Color: 1165
Size: 327815 Color: 1116

Bin 636: 59 of cap free
Amount of items: 3
Items: 
Size: 355941 Color: 1341
Size: 323867 Color: 1066
Size: 320134 Color: 1029

Bin 637: 69 of cap free
Amount of items: 3
Items: 
Size: 401445 Color: 1619
Size: 307132 Color: 885
Size: 291355 Color: 709

Bin 638: 70 of cap free
Amount of items: 3
Items: 
Size: 474779 Color: 1930
Size: 272369 Color: 453
Size: 252783 Color: 81

Bin 639: 79 of cap free
Amount of items: 3
Items: 
Size: 372697 Color: 1441
Size: 323460 Color: 1064
Size: 303765 Color: 850

Bin 640: 80 of cap free
Amount of items: 3
Items: 
Size: 355148 Color: 1331
Size: 338242 Color: 1204
Size: 306531 Color: 881

Bin 641: 81 of cap free
Amount of items: 3
Items: 
Size: 406290 Color: 1653
Size: 298498 Color: 790
Size: 295132 Color: 761

Bin 642: 83 of cap free
Amount of items: 3
Items: 
Size: 368404 Color: 1409
Size: 345475 Color: 1267
Size: 286039 Color: 653

Bin 643: 84 of cap free
Amount of items: 3
Items: 
Size: 404447 Color: 1643
Size: 299813 Color: 805
Size: 295657 Color: 765

Bin 644: 90 of cap free
Amount of items: 3
Items: 
Size: 361279 Color: 1378
Size: 344898 Color: 1262
Size: 293734 Color: 742

Bin 645: 99 of cap free
Amount of items: 3
Items: 
Size: 358455 Color: 1361
Size: 333156 Color: 1162
Size: 308291 Color: 899

Bin 646: 108 of cap free
Amount of items: 3
Items: 
Size: 361943 Color: 1380
Size: 325734 Color: 1085
Size: 312216 Color: 937

Bin 647: 110 of cap free
Amount of items: 3
Items: 
Size: 344727 Color: 1260
Size: 343119 Color: 1243
Size: 312045 Color: 933

Bin 648: 120 of cap free
Amount of items: 3
Items: 
Size: 386035 Color: 1527
Size: 320133 Color: 1028
Size: 293713 Color: 741

Bin 649: 125 of cap free
Amount of items: 3
Items: 
Size: 397254 Color: 1593
Size: 305211 Color: 861
Size: 297411 Color: 781

Bin 650: 134 of cap free
Amount of items: 3
Items: 
Size: 378191 Color: 1474
Size: 330248 Color: 1141
Size: 291428 Color: 713

Bin 651: 143 of cap free
Amount of items: 3
Items: 
Size: 376276 Color: 1463
Size: 366177 Color: 1397
Size: 257405 Color: 187

Bin 652: 154 of cap free
Amount of items: 3
Items: 
Size: 358140 Color: 1357
Size: 320923 Color: 1040
Size: 320784 Color: 1037

Bin 653: 154 of cap free
Amount of items: 3
Items: 
Size: 342305 Color: 1238
Size: 337063 Color: 1194
Size: 320479 Color: 1035

Bin 654: 197 of cap free
Amount of items: 3
Items: 
Size: 348579 Color: 1291
Size: 333111 Color: 1160
Size: 318114 Color: 1007

Bin 655: 230 of cap free
Amount of items: 3
Items: 
Size: 387475 Color: 1539
Size: 323197 Color: 1059
Size: 289099 Color: 682

Bin 656: 269 of cap free
Amount of items: 3
Items: 
Size: 353782 Color: 1319
Size: 327068 Color: 1103
Size: 318882 Color: 1013

Bin 657: 347 of cap free
Amount of items: 3
Items: 
Size: 399745 Color: 1606
Size: 344311 Color: 1256
Size: 255598 Color: 151

Bin 658: 368 of cap free
Amount of items: 3
Items: 
Size: 387900 Color: 1543
Size: 307321 Color: 888
Size: 304412 Color: 855

Bin 659: 468 of cap free
Amount of items: 3
Items: 
Size: 345946 Color: 1270
Size: 345185 Color: 1264
Size: 308402 Color: 900

Bin 660: 477 of cap free
Amount of items: 3
Items: 
Size: 378012 Color: 1473
Size: 327640 Color: 1114
Size: 293872 Color: 744

Bin 661: 512 of cap free
Amount of items: 2
Items: 
Size: 499774 Color: 1998
Size: 499715 Color: 1997

Bin 662: 1626 of cap free
Amount of items: 3
Items: 
Size: 402707 Color: 1631
Size: 299739 Color: 802
Size: 295929 Color: 767

Bin 663: 1647 of cap free
Amount of items: 3
Items: 
Size: 341380 Color: 1232
Size: 329451 Color: 1133
Size: 327523 Color: 1113

Bin 664: 1897 of cap free
Amount of items: 3
Items: 
Size: 370466 Color: 1421
Size: 316623 Color: 984
Size: 311015 Color: 926

Bin 665: 51003 of cap free
Amount of items: 3
Items: 
Size: 321650 Color: 1047
Size: 319155 Color: 1020
Size: 308193 Color: 897

Bin 666: 97542 of cap free
Amount of items: 3
Items: 
Size: 304349 Color: 853
Size: 299321 Color: 799
Size: 298789 Color: 794

Bin 667: 122578 of cap free
Amount of items: 3
Items: 
Size: 294958 Color: 758
Size: 291730 Color: 719
Size: 290735 Color: 698

Bin 668: 717189 of cap free
Amount of items: 1
Items: 
Size: 282812 Color: 621

Total size: 667000667
Total free space: 1000001

