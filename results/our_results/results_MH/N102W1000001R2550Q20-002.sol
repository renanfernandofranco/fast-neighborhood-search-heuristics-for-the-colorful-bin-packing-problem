Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 456063 Color: 13
Size: 274680 Color: 2
Size: 269258 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 460722 Color: 18
Size: 272332 Color: 10
Size: 266947 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 471147 Color: 19
Size: 275559 Color: 3
Size: 253295 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 301701 Color: 5
Size: 322722 Color: 1
Size: 375578 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 411511 Color: 5
Size: 311068 Color: 3
Size: 277422 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 407284 Color: 2
Size: 341447 Color: 13
Size: 251270 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 378727 Color: 13
Size: 348784 Color: 19
Size: 272490 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 473255 Color: 3
Size: 274849 Color: 12
Size: 251897 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 370571 Color: 14
Size: 358339 Color: 17
Size: 271091 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374917 Color: 4
Size: 364008 Color: 3
Size: 261076 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 492885 Color: 16
Size: 256059 Color: 3
Size: 251057 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 421741 Color: 2
Size: 327907 Color: 18
Size: 250353 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 481507 Color: 6
Size: 259563 Color: 6
Size: 258931 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 444188 Color: 8
Size: 291514 Color: 3
Size: 264299 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 401207 Color: 10
Size: 293504 Color: 9
Size: 305290 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 398311 Color: 5
Size: 339000 Color: 9
Size: 262690 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 445465 Color: 2
Size: 280086 Color: 16
Size: 274450 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 362611 Color: 16
Size: 354321 Color: 6
Size: 283069 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 391486 Color: 3
Size: 342413 Color: 9
Size: 266102 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 406087 Color: 9
Size: 302711 Color: 18
Size: 291203 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 452818 Color: 0
Size: 280250 Color: 4
Size: 266933 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 393535 Color: 14
Size: 349246 Color: 11
Size: 257220 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 393456 Color: 1
Size: 350439 Color: 8
Size: 256106 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 389535 Color: 14
Size: 350640 Color: 5
Size: 259826 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 389204 Color: 12
Size: 319565 Color: 16
Size: 291232 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 394747 Color: 13
Size: 335055 Color: 3
Size: 270199 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 434230 Color: 12
Size: 306709 Color: 17
Size: 259062 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 436475 Color: 16
Size: 294948 Color: 18
Size: 268578 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 447130 Color: 18
Size: 291715 Color: 4
Size: 261156 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 453990 Color: 18
Size: 294842 Color: 4
Size: 251169 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 462836 Color: 11
Size: 285420 Color: 0
Size: 251745 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 463026 Color: 6
Size: 272068 Color: 13
Size: 264907 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 470349 Color: 5
Size: 271697 Color: 17
Size: 257955 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 477488 Color: 10
Size: 268669 Color: 10
Size: 253844 Color: 8

Total size: 34000034
Total free space: 0

