Capicity Bin: 1000001
Lower Bound: 4512

Bins used: 4513
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 611289 Color: 7
Size: 255645 Color: 1
Size: 133067 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 760790 Color: 19
Size: 126527 Color: 8
Size: 112684 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 615601 Color: 16
Size: 192467 Color: 6
Size: 191933 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 738045 Color: 9
Size: 148405 Color: 2
Size: 113551 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 725501 Color: 11
Size: 146484 Color: 16
Size: 128016 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 709137 Color: 7
Size: 152655 Color: 11
Size: 138209 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 606664 Color: 9
Size: 248462 Color: 1
Size: 144875 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 760611 Color: 18
Size: 120732 Color: 1
Size: 118658 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 743541 Color: 17
Size: 136818 Color: 14
Size: 119642 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 742258 Color: 10
Size: 128986 Color: 19
Size: 128757 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 663627 Color: 18
Size: 184178 Color: 8
Size: 152196 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 498659 Color: 10
Size: 264081 Color: 15
Size: 237261 Color: 11

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 444351 Color: 2
Size: 293192 Color: 7
Size: 131694 Color: 13
Size: 130764 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 752093 Color: 0
Size: 132858 Color: 9
Size: 115050 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 751868 Color: 15
Size: 126064 Color: 18
Size: 122069 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 674640 Color: 4
Size: 163161 Color: 5
Size: 162200 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 686008 Color: 2
Size: 196944 Color: 16
Size: 117049 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 638805 Color: 11
Size: 198714 Color: 18
Size: 162482 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 705128 Color: 0
Size: 147477 Color: 10
Size: 147396 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 735600 Color: 3
Size: 132540 Color: 14
Size: 131861 Color: 5

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 736257 Color: 15
Size: 159398 Color: 13
Size: 104346 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 782456 Color: 13
Size: 116004 Color: 3
Size: 101541 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 667966 Color: 11
Size: 174343 Color: 2
Size: 157692 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 759229 Color: 12
Size: 135415 Color: 16
Size: 105357 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 536368 Color: 7
Size: 253593 Color: 5
Size: 210040 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 686576 Color: 17
Size: 159161 Color: 13
Size: 154264 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 694884 Color: 0
Size: 164780 Color: 8
Size: 140337 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 743944 Color: 11
Size: 134180 Color: 19
Size: 121877 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 697975 Color: 1
Size: 171916 Color: 19
Size: 130110 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 755365 Color: 19
Size: 134666 Color: 4
Size: 109970 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 685125 Color: 9
Size: 159352 Color: 1
Size: 155524 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 661358 Color: 8
Size: 189478 Color: 1
Size: 149165 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 768862 Color: 4
Size: 126257 Color: 16
Size: 104882 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 757602 Color: 9
Size: 126554 Color: 19
Size: 115845 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 521317 Color: 19
Size: 240281 Color: 7
Size: 238403 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 371726 Color: 6
Size: 348120 Color: 10
Size: 280155 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 658923 Color: 11
Size: 170598 Color: 9
Size: 170480 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 409686 Color: 0
Size: 297062 Color: 19
Size: 293253 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 785468 Color: 11
Size: 111469 Color: 12
Size: 103064 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 788084 Color: 6
Size: 109845 Color: 13
Size: 102072 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 789798 Color: 5
Size: 108052 Color: 10
Size: 102151 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 725835 Color: 12
Size: 143148 Color: 12
Size: 131018 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 641139 Color: 1
Size: 179550 Color: 2
Size: 179312 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 656561 Color: 18
Size: 175095 Color: 0
Size: 168345 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 592386 Color: 16
Size: 207493 Color: 15
Size: 200122 Color: 13

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 751772 Color: 17
Size: 136251 Color: 17
Size: 111978 Color: 12

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 768438 Color: 14
Size: 117564 Color: 19
Size: 113999 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 750901 Color: 2
Size: 129875 Color: 15
Size: 119225 Color: 14

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 750880 Color: 9
Size: 125179 Color: 13
Size: 123942 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 754512 Color: 7
Size: 129227 Color: 18
Size: 116262 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 795540 Color: 4
Size: 103089 Color: 13
Size: 101372 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 672161 Color: 14
Size: 174682 Color: 4
Size: 153158 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 718913 Color: 6
Size: 153274 Color: 6
Size: 127814 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 608933 Color: 18
Size: 264126 Color: 10
Size: 126942 Color: 16

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 769681 Color: 0
Size: 120233 Color: 5
Size: 110087 Color: 5

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 784658 Color: 13
Size: 108834 Color: 12
Size: 106509 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 663039 Color: 17
Size: 181312 Color: 15
Size: 155650 Color: 4

Bin 58: 0 of cap free
Amount of items: 4
Items: 
Size: 409838 Color: 0
Size: 284601 Color: 16
Size: 155987 Color: 17
Size: 149575 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 686841 Color: 3
Size: 167962 Color: 3
Size: 145198 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 771531 Color: 15
Size: 127022 Color: 3
Size: 101448 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 614580 Color: 4
Size: 193027 Color: 15
Size: 192394 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 352617 Color: 3
Size: 329685 Color: 3
Size: 317699 Color: 7

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 774544 Color: 15
Size: 121491 Color: 8
Size: 103966 Color: 19

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 724592 Color: 9
Size: 144118 Color: 18
Size: 131291 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 738531 Color: 1
Size: 141619 Color: 1
Size: 119851 Color: 16

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 750062 Color: 19
Size: 132670 Color: 9
Size: 117269 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 684195 Color: 15
Size: 187276 Color: 14
Size: 128530 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 379583 Color: 7
Size: 330276 Color: 11
Size: 290142 Color: 3

Bin 69: 0 of cap free
Amount of items: 4
Items: 
Size: 531560 Color: 18
Size: 254841 Color: 7
Size: 111050 Color: 2
Size: 102550 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 715889 Color: 1
Size: 144206 Color: 8
Size: 139906 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 561770 Color: 18
Size: 223461 Color: 11
Size: 214770 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 749514 Color: 3
Size: 133107 Color: 6
Size: 117380 Color: 5

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 723241 Color: 18
Size: 152804 Color: 4
Size: 123956 Color: 5

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 728522 Color: 6
Size: 146156 Color: 1
Size: 125323 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 791975 Color: 19
Size: 107410 Color: 17
Size: 100616 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 715834 Color: 18
Size: 143323 Color: 11
Size: 140844 Color: 11

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 539765 Color: 13
Size: 273171 Color: 7
Size: 187065 Color: 5

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 587228 Color: 17
Size: 290050 Color: 8
Size: 122723 Color: 7

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 763849 Color: 17
Size: 122614 Color: 1
Size: 113538 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 604913 Color: 10
Size: 225127 Color: 5
Size: 169961 Color: 16

Bin 81: 0 of cap free
Amount of items: 4
Items: 
Size: 594630 Color: 15
Size: 197877 Color: 17
Size: 105808 Color: 11
Size: 101686 Color: 13

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 689427 Color: 18
Size: 164242 Color: 16
Size: 146332 Color: 13

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 726346 Color: 4
Size: 165861 Color: 19
Size: 107794 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 724159 Color: 15
Size: 138155 Color: 15
Size: 137687 Color: 14

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 383939 Color: 17
Size: 352251 Color: 10
Size: 263811 Color: 16

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 664784 Color: 13
Size: 167653 Color: 10
Size: 167564 Color: 18

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 736782 Color: 3
Size: 156306 Color: 16
Size: 106913 Color: 16

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 745479 Color: 4
Size: 133525 Color: 17
Size: 120997 Color: 17

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 646154 Color: 16
Size: 353847 Color: 14

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 731429 Color: 9
Size: 143294 Color: 13
Size: 125278 Color: 10

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 784172 Color: 8
Size: 110459 Color: 9
Size: 105370 Color: 9

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 362969 Color: 14
Size: 330289 Color: 10
Size: 306743 Color: 17

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 759013 Color: 15
Size: 129332 Color: 18
Size: 111656 Color: 13

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 725773 Color: 15
Size: 159354 Color: 12
Size: 114874 Color: 9

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 682404 Color: 17
Size: 180694 Color: 6
Size: 136903 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 722151 Color: 3
Size: 150548 Color: 11
Size: 127302 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 631069 Color: 16
Size: 251812 Color: 10
Size: 117120 Color: 13

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 676684 Color: 0
Size: 172184 Color: 7
Size: 151133 Color: 15

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 632698 Color: 19
Size: 184999 Color: 17
Size: 182304 Color: 17

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 538144 Color: 18
Size: 270491 Color: 17
Size: 191366 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 736462 Color: 6
Size: 146405 Color: 17
Size: 117134 Color: 9

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 636441 Color: 7
Size: 229783 Color: 12
Size: 133777 Color: 12

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 730679 Color: 2
Size: 135463 Color: 14
Size: 133859 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 753001 Color: 11
Size: 138212 Color: 18
Size: 108788 Color: 8

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 702622 Color: 12
Size: 173409 Color: 9
Size: 123970 Color: 12

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 790111 Color: 15
Size: 104973 Color: 10
Size: 104917 Color: 8

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 488945 Color: 11
Size: 275859 Color: 9
Size: 235197 Color: 15

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 417871 Color: 3
Size: 321475 Color: 18
Size: 260655 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 590063 Color: 3
Size: 210755 Color: 19
Size: 199183 Color: 12

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 592783 Color: 7
Size: 237864 Color: 13
Size: 169354 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 653794 Color: 18
Size: 187903 Color: 16
Size: 158304 Color: 7

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 592572 Color: 8
Size: 228034 Color: 14
Size: 179395 Color: 19

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 606684 Color: 18
Size: 211198 Color: 3
Size: 182119 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 568163 Color: 0
Size: 231825 Color: 4
Size: 200013 Color: 5

Bin 115: 0 of cap free
Amount of items: 2
Items: 
Size: 755426 Color: 4
Size: 244575 Color: 2

Bin 116: 0 of cap free
Amount of items: 4
Items: 
Size: 520769 Color: 13
Size: 237321 Color: 16
Size: 128202 Color: 6
Size: 113709 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 504310 Color: 1
Size: 364518 Color: 15
Size: 131173 Color: 18

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 653012 Color: 13
Size: 200309 Color: 0
Size: 146680 Color: 5

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 668342 Color: 14
Size: 168948 Color: 1
Size: 162711 Color: 16

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 438373 Color: 11
Size: 338178 Color: 19
Size: 223450 Color: 6

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 733744 Color: 6
Size: 142299 Color: 0
Size: 123958 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 752049 Color: 12
Size: 132522 Color: 15
Size: 115430 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 738833 Color: 18
Size: 137177 Color: 4
Size: 123991 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 522404 Color: 5
Size: 347114 Color: 14
Size: 130483 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 765438 Color: 5
Size: 122686 Color: 6
Size: 111877 Color: 10

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 745743 Color: 13
Size: 131547 Color: 16
Size: 122711 Color: 10

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 750830 Color: 3
Size: 125147 Color: 6
Size: 124024 Color: 5

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 637860 Color: 1
Size: 191056 Color: 0
Size: 171085 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 724846 Color: 14
Size: 152212 Color: 3
Size: 122943 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 724671 Color: 17
Size: 159351 Color: 15
Size: 115979 Color: 18

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 520810 Color: 7
Size: 243758 Color: 9
Size: 235433 Color: 14

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 761454 Color: 8
Size: 129007 Color: 7
Size: 109540 Color: 8

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 719338 Color: 6
Size: 167202 Color: 7
Size: 113461 Color: 12

Bin 134: 0 of cap free
Amount of items: 4
Items: 
Size: 562778 Color: 8
Size: 217936 Color: 11
Size: 114984 Color: 13
Size: 104303 Color: 10

Bin 135: 0 of cap free
Amount of items: 4
Items: 
Size: 536640 Color: 14
Size: 218687 Color: 6
Size: 135067 Color: 15
Size: 109607 Color: 14

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 740101 Color: 14
Size: 130825 Color: 18
Size: 129075 Color: 15

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 724611 Color: 1
Size: 156561 Color: 3
Size: 118829 Color: 11

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 753374 Color: 0
Size: 135300 Color: 6
Size: 111327 Color: 16

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 573874 Color: 6
Size: 240429 Color: 0
Size: 185698 Color: 15

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 679690 Color: 10
Size: 160910 Color: 14
Size: 159401 Color: 18

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 790164 Color: 17
Size: 105928 Color: 2
Size: 103909 Color: 11

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 738593 Color: 18
Size: 149091 Color: 0
Size: 112317 Color: 9

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 740826 Color: 4
Size: 134251 Color: 15
Size: 124924 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 767913 Color: 19
Size: 130635 Color: 18
Size: 101453 Color: 6

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 750000 Color: 6
Size: 127154 Color: 15
Size: 122847 Color: 6

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 793033 Color: 16
Size: 104562 Color: 18
Size: 102406 Color: 13

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 740780 Color: 5
Size: 134623 Color: 1
Size: 124598 Color: 15

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 740795 Color: 2
Size: 142762 Color: 9
Size: 116444 Color: 9

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 748483 Color: 1
Size: 128735 Color: 16
Size: 122783 Color: 8

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 739702 Color: 17
Size: 137565 Color: 14
Size: 122734 Color: 3

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 739684 Color: 14
Size: 144723 Color: 3
Size: 115594 Color: 6

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 740623 Color: 2
Size: 136572 Color: 18
Size: 122806 Color: 18

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 520679 Color: 9
Size: 274371 Color: 11
Size: 204951 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 780208 Color: 17
Size: 114159 Color: 4
Size: 105634 Color: 5

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 786936 Color: 18
Size: 108940 Color: 10
Size: 104125 Color: 14

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 768704 Color: 10
Size: 123320 Color: 12
Size: 107977 Color: 6

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 739661 Color: 4
Size: 138707 Color: 15
Size: 121633 Color: 14

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 777732 Color: 12
Size: 112084 Color: 5
Size: 110185 Color: 18

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 759887 Color: 15
Size: 127330 Color: 0
Size: 112784 Color: 12

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 763562 Color: 10
Size: 118586 Color: 15
Size: 117853 Color: 14

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 778719 Color: 11
Size: 119271 Color: 17
Size: 102011 Color: 14

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 684071 Color: 3
Size: 214778 Color: 0
Size: 101152 Color: 8

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 739651 Color: 3
Size: 156515 Color: 5
Size: 103835 Color: 19

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 602688 Color: 13
Size: 295597 Color: 14
Size: 101716 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 371489 Color: 17
Size: 330695 Color: 4
Size: 297817 Color: 12

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 790430 Color: 7
Size: 108435 Color: 10
Size: 101136 Color: 6

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 692158 Color: 3
Size: 155234 Color: 8
Size: 152609 Color: 19

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 727439 Color: 18
Size: 272562 Color: 10

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 762989 Color: 1
Size: 120566 Color: 1
Size: 116446 Color: 11

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 769202 Color: 12
Size: 116324 Color: 2
Size: 114475 Color: 18

Bin 171: 0 of cap free
Amount of items: 4
Items: 
Size: 540035 Color: 12
Size: 237275 Color: 17
Size: 116396 Color: 14
Size: 106295 Color: 19

Bin 172: 0 of cap free
Amount of items: 4
Items: 
Size: 531870 Color: 16
Size: 243988 Color: 7
Size: 113209 Color: 8
Size: 110934 Color: 17

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 609802 Color: 15
Size: 195118 Color: 8
Size: 195081 Color: 15

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 769378 Color: 5
Size: 127988 Color: 8
Size: 102635 Color: 18

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 792747 Color: 10
Size: 104739 Color: 10
Size: 102515 Color: 5

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 732114 Color: 14
Size: 135038 Color: 9
Size: 132849 Color: 19

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 459207 Color: 2
Size: 409372 Color: 6
Size: 131422 Color: 12

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 792098 Color: 11
Size: 105814 Color: 15
Size: 102089 Color: 19

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 713842 Color: 9
Size: 143192 Color: 5
Size: 142967 Color: 10

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 774810 Color: 0
Size: 119871 Color: 12
Size: 105320 Color: 14

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 790629 Color: 3
Size: 109111 Color: 10
Size: 100261 Color: 7

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 520678 Color: 2
Size: 363381 Color: 13
Size: 115942 Color: 16

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 409964 Color: 14
Size: 321361 Color: 3
Size: 268676 Color: 9

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 759829 Color: 5
Size: 128850 Color: 4
Size: 111322 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 779034 Color: 17
Size: 110633 Color: 17
Size: 110334 Color: 18

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 784910 Color: 11
Size: 109083 Color: 9
Size: 106008 Color: 17

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 790189 Color: 19
Size: 106938 Color: 5
Size: 102874 Color: 9

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 756929 Color: 1
Size: 130903 Color: 3
Size: 112169 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 776117 Color: 10
Size: 113630 Color: 10
Size: 110254 Color: 6

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 788177 Color: 10
Size: 110007 Color: 17
Size: 101817 Color: 15

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 374375 Color: 12
Size: 361121 Color: 12
Size: 264505 Color: 8

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 795345 Color: 15
Size: 102953 Color: 11
Size: 101703 Color: 6

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 361653 Color: 7
Size: 339434 Color: 8
Size: 298914 Color: 3

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 757635 Color: 3
Size: 130165 Color: 7
Size: 112201 Color: 17

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 794735 Color: 7
Size: 103690 Color: 15
Size: 101576 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 789388 Color: 17
Size: 110268 Color: 12
Size: 100345 Color: 17

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 378729 Color: 16
Size: 356540 Color: 16
Size: 264732 Color: 3

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 778783 Color: 16
Size: 113663 Color: 3
Size: 107555 Color: 8

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 658127 Color: 7
Size: 172512 Color: 10
Size: 169362 Color: 2

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 686944 Color: 19
Size: 156861 Color: 1
Size: 156196 Color: 2

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 740845 Color: 13
Size: 146189 Color: 4
Size: 112967 Color: 8

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 556383 Color: 7
Size: 443618 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 793877 Color: 17
Size: 104428 Color: 14
Size: 101696 Color: 3

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 722046 Color: 19
Size: 139036 Color: 7
Size: 138919 Color: 19

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 353880 Color: 14
Size: 352392 Color: 18
Size: 293729 Color: 1

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 660455 Color: 18
Size: 339546 Color: 13

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 712440 Color: 0
Size: 144212 Color: 3
Size: 143349 Color: 12

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 780212 Color: 9
Size: 114649 Color: 4
Size: 105140 Color: 11

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 729947 Color: 4
Size: 135150 Color: 19
Size: 134904 Color: 9

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 738847 Color: 15
Size: 131498 Color: 5
Size: 129656 Color: 18

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 373762 Color: 12
Size: 328321 Color: 1
Size: 297918 Color: 2

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 772949 Color: 18
Size: 113631 Color: 17
Size: 113421 Color: 7

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 797683 Color: 9
Size: 101217 Color: 14
Size: 101101 Color: 17

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 789186 Color: 11
Size: 107891 Color: 0
Size: 102924 Color: 9

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 734095 Color: 19
Size: 137260 Color: 4
Size: 128646 Color: 17

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 722743 Color: 3
Size: 138755 Color: 8
Size: 138503 Color: 16

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 730568 Color: 12
Size: 135750 Color: 17
Size: 133683 Color: 19

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 782924 Color: 8
Size: 108970 Color: 18
Size: 108107 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 767907 Color: 11
Size: 121676 Color: 16
Size: 110418 Color: 5

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 441965 Color: 17
Size: 294161 Color: 12
Size: 263875 Color: 15

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 357940 Color: 10
Size: 350139 Color: 16
Size: 291922 Color: 16

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 355750 Color: 18
Size: 338035 Color: 8
Size: 306216 Color: 1

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 764155 Color: 4
Size: 124062 Color: 16
Size: 111784 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 775023 Color: 19
Size: 116320 Color: 17
Size: 108658 Color: 8

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 357425 Color: 8
Size: 345028 Color: 0
Size: 297548 Color: 5

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 531304 Color: 4
Size: 244414 Color: 12
Size: 224283 Color: 8

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 707485 Color: 18
Size: 191512 Color: 12
Size: 101004 Color: 8

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 777015 Color: 10
Size: 113058 Color: 9
Size: 109928 Color: 6

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 736241 Color: 16
Size: 155884 Color: 9
Size: 107876 Color: 16

Bin 230: 0 of cap free
Amount of items: 2
Items: 
Size: 692899 Color: 7
Size: 307102 Color: 10

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 772312 Color: 18
Size: 118094 Color: 12
Size: 109595 Color: 16

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 748695 Color: 2
Size: 128658 Color: 15
Size: 122648 Color: 9

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 597305 Color: 9
Size: 402696 Color: 3

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 784574 Color: 1
Size: 110951 Color: 17
Size: 104476 Color: 2

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 559323 Color: 19
Size: 303341 Color: 1
Size: 137337 Color: 1

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 352275 Color: 17
Size: 339381 Color: 8
Size: 308345 Color: 16

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 760676 Color: 8
Size: 138367 Color: 9
Size: 100958 Color: 10

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 796526 Color: 18
Size: 102352 Color: 10
Size: 101123 Color: 18

Bin 239: 0 of cap free
Amount of items: 4
Items: 
Size: 593971 Color: 6
Size: 202820 Color: 15
Size: 101864 Color: 6
Size: 101346 Color: 7

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 359256 Color: 3
Size: 347094 Color: 8
Size: 293651 Color: 19

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 584043 Color: 5
Size: 415958 Color: 3

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 792463 Color: 8
Size: 207538 Color: 3

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 362355 Color: 11
Size: 339360 Color: 11
Size: 298286 Color: 2

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 368946 Color: 16
Size: 320834 Color: 9
Size: 310221 Color: 10

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 374575 Color: 5
Size: 361210 Color: 6
Size: 264216 Color: 18

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 383700 Color: 9
Size: 369228 Color: 18
Size: 247073 Color: 4

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 387288 Color: 19
Size: 330541 Color: 3
Size: 282172 Color: 7

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 407649 Color: 9
Size: 341141 Color: 16
Size: 251211 Color: 17

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 409638 Color: 12
Size: 306745 Color: 7
Size: 283618 Color: 17

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 416816 Color: 14
Size: 299121 Color: 18
Size: 284064 Color: 17

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 504328 Color: 5
Size: 495673 Color: 7

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 515320 Color: 13
Size: 484681 Color: 7

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 526239 Color: 5
Size: 473762 Color: 16

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 541673 Color: 17
Size: 458328 Color: 5

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 542390 Color: 6
Size: 457611 Color: 18

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 549686 Color: 7
Size: 450315 Color: 2

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 558364 Color: 6
Size: 441637 Color: 4

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 558560 Color: 12
Size: 441441 Color: 15

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 564079 Color: 4
Size: 435922 Color: 18

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 570814 Color: 6
Size: 429187 Color: 12

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 575352 Color: 18
Size: 424649 Color: 10

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 581574 Color: 12
Size: 418427 Color: 1

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 582267 Color: 17
Size: 417734 Color: 11

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 600537 Color: 2
Size: 399464 Color: 19

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 606670 Color: 8
Size: 393331 Color: 4

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 611601 Color: 2
Size: 388400 Color: 1

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 612537 Color: 5
Size: 387464 Color: 11

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 617052 Color: 4
Size: 191514 Color: 1
Size: 191435 Color: 18

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 623004 Color: 11
Size: 376997 Color: 17

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 626988 Color: 4
Size: 186815 Color: 4
Size: 186198 Color: 11

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 627154 Color: 8
Size: 186544 Color: 15
Size: 186303 Color: 17

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 631767 Color: 9
Size: 368234 Color: 14

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 635774 Color: 4
Size: 182435 Color: 12
Size: 181792 Color: 0

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 639084 Color: 17
Size: 360917 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 642111 Color: 15
Size: 179155 Color: 6
Size: 178735 Color: 8

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 645267 Color: 8
Size: 177383 Color: 5
Size: 177351 Color: 2

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 647496 Color: 15
Size: 176281 Color: 5
Size: 176224 Color: 4

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 650293 Color: 18
Size: 349708 Color: 10

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 652991 Color: 10
Size: 347010 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 655822 Color: 16
Size: 172736 Color: 3
Size: 171443 Color: 4

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 660718 Color: 4
Size: 169881 Color: 16
Size: 169402 Color: 2

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 661324 Color: 2
Size: 338677 Color: 10

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 661843 Color: 10
Size: 338158 Color: 13

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 663046 Color: 15
Size: 336955 Color: 14

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 665079 Color: 13
Size: 334922 Color: 10

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 668913 Color: 2
Size: 166898 Color: 8
Size: 164190 Color: 2

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 669989 Color: 8
Size: 166105 Color: 11
Size: 163907 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 685230 Color: 6
Size: 157419 Color: 14
Size: 157352 Color: 14

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 687032 Color: 10
Size: 312969 Color: 9

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 692220 Color: 19
Size: 307781 Color: 0

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 697814 Color: 8
Size: 302187 Color: 0

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 698821 Color: 5
Size: 301180 Color: 12

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 701643 Color: 8
Size: 298358 Color: 17

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 701611 Color: 5
Size: 298390 Color: 13

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 713035 Color: 15
Size: 286966 Color: 3

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 721399 Color: 12
Size: 278602 Color: 2

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 721648 Color: 1
Size: 139228 Color: 9
Size: 139125 Color: 3

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 731866 Color: 4
Size: 134267 Color: 4
Size: 133868 Color: 7

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 734670 Color: 17
Size: 265331 Color: 5

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 745974 Color: 17
Size: 127592 Color: 4
Size: 126435 Color: 10

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 751107 Color: 19
Size: 128895 Color: 17
Size: 119999 Color: 3

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 751473 Color: 0
Size: 129336 Color: 0
Size: 119192 Color: 7

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 752762 Color: 9
Size: 129221 Color: 1
Size: 118018 Color: 10

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 753283 Color: 16
Size: 127901 Color: 8
Size: 118817 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 753899 Color: 4
Size: 127990 Color: 10
Size: 118112 Color: 18

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 753984 Color: 17
Size: 124252 Color: 3
Size: 121765 Color: 8

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 754670 Color: 6
Size: 126927 Color: 3
Size: 118404 Color: 2

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 754703 Color: 12
Size: 123878 Color: 17
Size: 121420 Color: 7

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 755147 Color: 18
Size: 127310 Color: 13
Size: 117544 Color: 5

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 755584 Color: 16
Size: 125932 Color: 14
Size: 118485 Color: 17

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 755466 Color: 19
Size: 122402 Color: 8
Size: 122133 Color: 11

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 755812 Color: 15
Size: 124900 Color: 7
Size: 119289 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 756017 Color: 1
Size: 125284 Color: 8
Size: 118700 Color: 6

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 756223 Color: 15
Size: 243778 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 756487 Color: 4
Size: 122831 Color: 19
Size: 120683 Color: 13

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 757430 Color: 2
Size: 124211 Color: 7
Size: 118360 Color: 10

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 759683 Color: 14
Size: 123757 Color: 12
Size: 116561 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 759969 Color: 10
Size: 121765 Color: 17
Size: 118267 Color: 6

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 760117 Color: 18
Size: 123875 Color: 7
Size: 116009 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 761685 Color: 6
Size: 122106 Color: 19
Size: 116210 Color: 0

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 767526 Color: 17
Size: 232475 Color: 19

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 768264 Color: 17
Size: 231737 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 769981 Color: 17
Size: 116689 Color: 14
Size: 113331 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 773402 Color: 1
Size: 113556 Color: 5
Size: 113043 Color: 2

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 775224 Color: 3
Size: 112566 Color: 15
Size: 112211 Color: 18

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 787857 Color: 12
Size: 108008 Color: 18
Size: 104136 Color: 3

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 788081 Color: 4
Size: 106098 Color: 9
Size: 105822 Color: 6

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 788578 Color: 16
Size: 107925 Color: 12
Size: 103498 Color: 9

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 788948 Color: 5
Size: 107532 Color: 10
Size: 103521 Color: 14

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 789656 Color: 19
Size: 105917 Color: 18
Size: 104428 Color: 3

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 794175 Color: 0
Size: 205826 Color: 11

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 796193 Color: 3
Size: 203808 Color: 19

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 799279 Color: 15
Size: 100665 Color: 4
Size: 100057 Color: 15

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 692743 Color: 16
Size: 169548 Color: 7
Size: 137709 Color: 4

Bin 335: 1 of cap free
Amount of items: 3
Items: 
Size: 722587 Color: 14
Size: 147443 Color: 1
Size: 129970 Color: 4

Bin 336: 1 of cap free
Amount of items: 3
Items: 
Size: 753157 Color: 1
Size: 135655 Color: 17
Size: 111188 Color: 1

Bin 337: 1 of cap free
Amount of items: 2
Items: 
Size: 690612 Color: 12
Size: 309388 Color: 4

Bin 338: 1 of cap free
Amount of items: 3
Items: 
Size: 790188 Color: 8
Size: 108248 Color: 17
Size: 101564 Color: 3

Bin 339: 1 of cap free
Amount of items: 3
Items: 
Size: 589889 Color: 14
Size: 218066 Color: 9
Size: 192045 Color: 8

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 610264 Color: 17
Size: 195263 Color: 13
Size: 194473 Color: 12

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 740766 Color: 0
Size: 145068 Color: 18
Size: 114166 Color: 15

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 602702 Color: 13
Size: 262319 Color: 11
Size: 134979 Color: 14

Bin 343: 1 of cap free
Amount of items: 3
Items: 
Size: 378663 Color: 9
Size: 369822 Color: 12
Size: 251515 Color: 19

Bin 344: 1 of cap free
Amount of items: 3
Items: 
Size: 617674 Color: 9
Size: 191196 Color: 8
Size: 191130 Color: 14

Bin 345: 1 of cap free
Amount of items: 3
Items: 
Size: 626550 Color: 1
Size: 186884 Color: 6
Size: 186566 Color: 1

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 387456 Color: 4
Size: 317827 Color: 14
Size: 294717 Color: 3

Bin 347: 1 of cap free
Amount of items: 3
Items: 
Size: 616631 Color: 9
Size: 191749 Color: 19
Size: 191620 Color: 12

Bin 348: 1 of cap free
Amount of items: 2
Items: 
Size: 715154 Color: 14
Size: 284846 Color: 1

Bin 349: 1 of cap free
Amount of items: 2
Items: 
Size: 738179 Color: 5
Size: 261821 Color: 9

Bin 350: 1 of cap free
Amount of items: 3
Items: 
Size: 705309 Color: 5
Size: 147429 Color: 16
Size: 147262 Color: 0

Bin 351: 1 of cap free
Amount of items: 3
Items: 
Size: 655497 Color: 13
Size: 172311 Color: 18
Size: 172192 Color: 16

Bin 352: 1 of cap free
Amount of items: 3
Items: 
Size: 785362 Color: 18
Size: 109287 Color: 7
Size: 105351 Color: 4

Bin 353: 1 of cap free
Amount of items: 2
Items: 
Size: 747163 Color: 7
Size: 252837 Color: 8

Bin 354: 1 of cap free
Amount of items: 3
Items: 
Size: 628209 Color: 19
Size: 243607 Color: 1
Size: 128184 Color: 11

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 760020 Color: 19
Size: 121651 Color: 15
Size: 118329 Color: 2

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 643015 Color: 7
Size: 178542 Color: 16
Size: 178443 Color: 12

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 787294 Color: 19
Size: 111979 Color: 8
Size: 100727 Color: 0

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 783799 Color: 5
Size: 110837 Color: 1
Size: 105364 Color: 13

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 669813 Color: 2
Size: 166375 Color: 3
Size: 163812 Color: 5

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 770420 Color: 6
Size: 117248 Color: 9
Size: 112332 Color: 8

Bin 361: 1 of cap free
Amount of items: 2
Items: 
Size: 659650 Color: 16
Size: 340350 Color: 19

Bin 362: 1 of cap free
Amount of items: 3
Items: 
Size: 520523 Color: 8
Size: 355588 Color: 3
Size: 123889 Color: 18

Bin 363: 1 of cap free
Amount of items: 2
Items: 
Size: 726321 Color: 3
Size: 273679 Color: 4

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 673725 Color: 12
Size: 163448 Color: 5
Size: 162827 Color: 2

Bin 365: 1 of cap free
Amount of items: 2
Items: 
Size: 680632 Color: 2
Size: 319368 Color: 1

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 668758 Color: 18
Size: 166735 Color: 9
Size: 164507 Color: 5

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 620139 Color: 3
Size: 189942 Color: 14
Size: 189919 Color: 14

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 660488 Color: 13
Size: 170025 Color: 2
Size: 169487 Color: 5

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 733998 Color: 18
Size: 133573 Color: 0
Size: 132429 Color: 10

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 738520 Color: 8
Size: 130753 Color: 11
Size: 130727 Color: 2

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 540241 Color: 0
Size: 229941 Color: 11
Size: 229818 Color: 9

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 682523 Color: 18
Size: 158921 Color: 18
Size: 158556 Color: 15

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 740979 Color: 15
Size: 133404 Color: 3
Size: 125617 Color: 16

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 693281 Color: 16
Size: 153690 Color: 17
Size: 153029 Color: 1

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 363932 Color: 8
Size: 344309 Color: 0
Size: 291759 Color: 16

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 699966 Color: 7
Size: 150235 Color: 8
Size: 149799 Color: 8

Bin 377: 1 of cap free
Amount of items: 2
Items: 
Size: 729038 Color: 5
Size: 270962 Color: 12

Bin 378: 1 of cap free
Amount of items: 2
Items: 
Size: 621114 Color: 10
Size: 378886 Color: 18

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 583969 Color: 5
Size: 216447 Color: 15
Size: 199584 Color: 2

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 770820 Color: 10
Size: 116422 Color: 16
Size: 112758 Color: 8

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 728900 Color: 7
Size: 135702 Color: 6
Size: 135398 Color: 19

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 618667 Color: 10
Size: 193555 Color: 15
Size: 187778 Color: 13

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 795059 Color: 15
Size: 104780 Color: 13
Size: 100161 Color: 12

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 769326 Color: 16
Size: 118076 Color: 4
Size: 112598 Color: 11

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 761032 Color: 16
Size: 119911 Color: 4
Size: 119057 Color: 13

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 782721 Color: 6
Size: 108847 Color: 7
Size: 108432 Color: 13

Bin 387: 1 of cap free
Amount of items: 2
Items: 
Size: 619471 Color: 10
Size: 380529 Color: 14

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 614727 Color: 4
Size: 194354 Color: 12
Size: 190919 Color: 8

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 730785 Color: 5
Size: 135246 Color: 16
Size: 133969 Color: 16

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 752882 Color: 16
Size: 126603 Color: 7
Size: 120515 Color: 14

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 362284 Color: 0
Size: 338567 Color: 3
Size: 299149 Color: 17

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 639337 Color: 16
Size: 198586 Color: 12
Size: 162077 Color: 0

Bin 393: 1 of cap free
Amount of items: 2
Items: 
Size: 734891 Color: 15
Size: 265109 Color: 12

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 592984 Color: 11
Size: 302308 Color: 13
Size: 104708 Color: 8

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 520849 Color: 12
Size: 254600 Color: 7
Size: 224551 Color: 16

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 538345 Color: 17
Size: 230885 Color: 17
Size: 230770 Color: 9

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 522405 Color: 2
Size: 320241 Color: 3
Size: 157354 Color: 13

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 542571 Color: 18
Size: 333114 Color: 5
Size: 124315 Color: 3

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 675633 Color: 16
Size: 165853 Color: 12
Size: 158514 Color: 8

Bin 400: 1 of cap free
Amount of items: 2
Items: 
Size: 618021 Color: 17
Size: 381979 Color: 8

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 386657 Color: 15
Size: 333188 Color: 13
Size: 280155 Color: 12

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 539989 Color: 1
Size: 237309 Color: 13
Size: 222702 Color: 16

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 357218 Color: 10
Size: 339917 Color: 11
Size: 302865 Color: 16

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 726366 Color: 15
Size: 136966 Color: 17
Size: 136668 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 741254 Color: 6
Size: 129518 Color: 19
Size: 129228 Color: 4

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 361621 Color: 14
Size: 344056 Color: 9
Size: 294323 Color: 1

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 679772 Color: 16
Size: 160358 Color: 12
Size: 159870 Color: 6

Bin 408: 1 of cap free
Amount of items: 2
Items: 
Size: 594121 Color: 1
Size: 405879 Color: 11

Bin 409: 1 of cap free
Amount of items: 2
Items: 
Size: 636991 Color: 5
Size: 363009 Color: 19

Bin 410: 1 of cap free
Amount of items: 2
Items: 
Size: 667474 Color: 0
Size: 332526 Color: 4

Bin 411: 1 of cap free
Amount of items: 2
Items: 
Size: 589036 Color: 9
Size: 410964 Color: 2

Bin 412: 1 of cap free
Amount of items: 2
Items: 
Size: 694500 Color: 9
Size: 305500 Color: 2

Bin 413: 1 of cap free
Amount of items: 2
Items: 
Size: 539705 Color: 5
Size: 460295 Color: 16

Bin 414: 1 of cap free
Amount of items: 2
Items: 
Size: 689792 Color: 5
Size: 310208 Color: 16

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 344575 Color: 11
Size: 337612 Color: 10
Size: 317813 Color: 12

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 412598 Color: 15
Size: 293899 Color: 14
Size: 293503 Color: 6

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 504619 Color: 4
Size: 250979 Color: 17
Size: 244402 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 357956 Color: 19
Size: 343990 Color: 8
Size: 298054 Color: 5

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 367987 Color: 11
Size: 338098 Color: 12
Size: 293915 Color: 9

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 372194 Color: 2
Size: 361483 Color: 3
Size: 266323 Color: 8

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 372979 Color: 15
Size: 362558 Color: 2
Size: 264463 Color: 3

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 378642 Color: 5
Size: 341388 Color: 2
Size: 279970 Color: 4

Bin 423: 1 of cap free
Amount of items: 2
Items: 
Size: 502502 Color: 12
Size: 497498 Color: 11

Bin 424: 1 of cap free
Amount of items: 2
Items: 
Size: 504412 Color: 8
Size: 495588 Color: 0

Bin 425: 1 of cap free
Amount of items: 2
Items: 
Size: 505213 Color: 2
Size: 494787 Color: 10

Bin 426: 1 of cap free
Amount of items: 2
Items: 
Size: 511957 Color: 5
Size: 488043 Color: 10

Bin 427: 1 of cap free
Amount of items: 2
Items: 
Size: 513560 Color: 2
Size: 486440 Color: 18

Bin 428: 1 of cap free
Amount of items: 2
Items: 
Size: 514680 Color: 6
Size: 485320 Color: 8

Bin 429: 1 of cap free
Amount of items: 2
Items: 
Size: 525973 Color: 12
Size: 474027 Color: 17

Bin 430: 1 of cap free
Amount of items: 2
Items: 
Size: 528568 Color: 5
Size: 471432 Color: 15

Bin 431: 1 of cap free
Amount of items: 2
Items: 
Size: 539961 Color: 11
Size: 460039 Color: 16

Bin 432: 1 of cap free
Amount of items: 2
Items: 
Size: 546696 Color: 4
Size: 453304 Color: 14

Bin 433: 1 of cap free
Amount of items: 2
Items: 
Size: 556311 Color: 18
Size: 443689 Color: 15

Bin 434: 1 of cap free
Amount of items: 2
Items: 
Size: 578617 Color: 7
Size: 421383 Color: 1

Bin 435: 1 of cap free
Amount of items: 2
Items: 
Size: 580927 Color: 14
Size: 419073 Color: 2

Bin 436: 1 of cap free
Amount of items: 2
Items: 
Size: 591718 Color: 3
Size: 408282 Color: 0

Bin 437: 1 of cap free
Amount of items: 2
Items: 
Size: 606826 Color: 18
Size: 393174 Color: 5

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 607107 Color: 8
Size: 196481 Color: 11
Size: 196412 Color: 9

Bin 439: 1 of cap free
Amount of items: 2
Items: 
Size: 615849 Color: 14
Size: 384151 Color: 18

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 624550 Color: 15
Size: 187762 Color: 12
Size: 187688 Color: 0

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 625975 Color: 8
Size: 187941 Color: 1
Size: 186084 Color: 9

Bin 442: 1 of cap free
Amount of items: 2
Items: 
Size: 637315 Color: 13
Size: 362685 Color: 14

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 641016 Color: 15
Size: 179869 Color: 2
Size: 179115 Color: 15

Bin 444: 1 of cap free
Amount of items: 2
Items: 
Size: 644266 Color: 4
Size: 355734 Color: 12

Bin 445: 1 of cap free
Amount of items: 2
Items: 
Size: 648137 Color: 3
Size: 351863 Color: 1

Bin 446: 1 of cap free
Amount of items: 2
Items: 
Size: 658424 Color: 19
Size: 341576 Color: 5

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 669271 Color: 6
Size: 166155 Color: 2
Size: 164574 Color: 12

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 670195 Color: 8
Size: 165295 Color: 4
Size: 164510 Color: 14

Bin 449: 1 of cap free
Amount of items: 2
Items: 
Size: 672570 Color: 0
Size: 327430 Color: 9

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 675395 Color: 11
Size: 162420 Color: 17
Size: 162185 Color: 0

Bin 451: 1 of cap free
Amount of items: 2
Items: 
Size: 677746 Color: 12
Size: 322254 Color: 9

Bin 452: 1 of cap free
Amount of items: 2
Items: 
Size: 692301 Color: 1
Size: 307699 Color: 14

Bin 453: 1 of cap free
Amount of items: 2
Items: 
Size: 697310 Color: 0
Size: 302690 Color: 15

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 698478 Color: 16
Size: 150841 Color: 1
Size: 150681 Color: 16

Bin 455: 1 of cap free
Amount of items: 2
Items: 
Size: 699768 Color: 1
Size: 300232 Color: 15

Bin 456: 1 of cap free
Amount of items: 2
Items: 
Size: 699783 Color: 3
Size: 300217 Color: 2

Bin 457: 1 of cap free
Amount of items: 2
Items: 
Size: 702383 Color: 14
Size: 297617 Color: 13

Bin 458: 1 of cap free
Amount of items: 2
Items: 
Size: 718708 Color: 14
Size: 281292 Color: 10

Bin 459: 1 of cap free
Amount of items: 2
Items: 
Size: 722644 Color: 14
Size: 277356 Color: 1

Bin 460: 1 of cap free
Amount of items: 2
Items: 
Size: 724307 Color: 9
Size: 275693 Color: 7

Bin 461: 1 of cap free
Amount of items: 2
Items: 
Size: 728782 Color: 12
Size: 271218 Color: 13

Bin 462: 1 of cap free
Amount of items: 2
Items: 
Size: 731688 Color: 4
Size: 268312 Color: 5

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 747583 Color: 12
Size: 128413 Color: 15
Size: 124004 Color: 9

Bin 464: 1 of cap free
Amount of items: 2
Items: 
Size: 769173 Color: 12
Size: 230827 Color: 15

Bin 465: 1 of cap free
Amount of items: 2
Items: 
Size: 771873 Color: 4
Size: 228127 Color: 17

Bin 466: 1 of cap free
Amount of items: 2
Items: 
Size: 776890 Color: 10
Size: 223110 Color: 19

Bin 467: 1 of cap free
Amount of items: 2
Items: 
Size: 795380 Color: 4
Size: 204620 Color: 3

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 479541 Color: 17
Size: 417109 Color: 9
Size: 103349 Color: 6

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 674098 Color: 17
Size: 163247 Color: 13
Size: 162654 Color: 4

Bin 470: 2 of cap free
Amount of items: 2
Items: 
Size: 753498 Color: 12
Size: 246501 Color: 9

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 706815 Color: 3
Size: 146923 Color: 11
Size: 146261 Color: 9

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 787310 Color: 14
Size: 107766 Color: 14
Size: 104923 Color: 1

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 363545 Color: 4
Size: 344120 Color: 13
Size: 292334 Color: 18

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 583649 Color: 3
Size: 216439 Color: 15
Size: 199911 Color: 6

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 658887 Color: 17
Size: 193343 Color: 16
Size: 147769 Color: 13

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 658091 Color: 14
Size: 171490 Color: 19
Size: 170418 Color: 6

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 640884 Color: 16
Size: 180149 Color: 17
Size: 178966 Color: 0

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 617966 Color: 5
Size: 191371 Color: 16
Size: 190662 Color: 17

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 777885 Color: 5
Size: 117321 Color: 5
Size: 104793 Color: 15

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 386439 Color: 5
Size: 319944 Color: 16
Size: 293616 Color: 1

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 659580 Color: 11
Size: 171661 Color: 17
Size: 168758 Color: 16

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 693640 Color: 3
Size: 153998 Color: 6
Size: 152361 Color: 10

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 750301 Color: 16
Size: 125611 Color: 9
Size: 124087 Color: 7

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 371496 Color: 14
Size: 343569 Color: 15
Size: 284934 Color: 8

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 613380 Color: 16
Size: 196343 Color: 9
Size: 190276 Color: 3

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 657950 Color: 15
Size: 171219 Color: 1
Size: 170830 Color: 8

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 639521 Color: 12
Size: 185566 Color: 1
Size: 174912 Color: 8

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 704330 Color: 7
Size: 148401 Color: 19
Size: 147268 Color: 1

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 710391 Color: 1
Size: 146325 Color: 18
Size: 143283 Color: 19

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 493760 Color: 3
Size: 394311 Color: 10
Size: 111928 Color: 18

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 592386 Color: 2
Size: 236448 Color: 3
Size: 171165 Color: 12

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 615503 Color: 7
Size: 192510 Color: 16
Size: 191986 Color: 11

Bin 493: 2 of cap free
Amount of items: 2
Items: 
Size: 668669 Color: 9
Size: 331330 Color: 5

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 646132 Color: 6
Size: 186325 Color: 6
Size: 167542 Color: 2

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 480088 Color: 18
Size: 270098 Color: 11
Size: 249813 Color: 11

Bin 496: 2 of cap free
Amount of items: 2
Items: 
Size: 702180 Color: 0
Size: 297819 Color: 13

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 708770 Color: 6
Size: 159643 Color: 17
Size: 131586 Color: 2

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 688685 Color: 8
Size: 155798 Color: 15
Size: 155516 Color: 6

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 758083 Color: 18
Size: 126424 Color: 5
Size: 115492 Color: 2

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 388098 Color: 19
Size: 317616 Color: 10
Size: 294285 Color: 6

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 625757 Color: 13
Size: 187608 Color: 5
Size: 186634 Color: 14

Bin 502: 2 of cap free
Amount of items: 2
Items: 
Size: 713824 Color: 12
Size: 286175 Color: 2

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 541410 Color: 18
Size: 330102 Color: 7
Size: 128487 Color: 9

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 666104 Color: 17
Size: 167340 Color: 11
Size: 166555 Color: 10

Bin 505: 2 of cap free
Amount of items: 2
Items: 
Size: 749310 Color: 11
Size: 250689 Color: 3

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 361493 Color: 7
Size: 344079 Color: 6
Size: 294427 Color: 14

Bin 507: 2 of cap free
Amount of items: 2
Items: 
Size: 787382 Color: 16
Size: 212617 Color: 0

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 635125 Color: 9
Size: 183051 Color: 3
Size: 181823 Color: 7

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 636474 Color: 10
Size: 181910 Color: 2
Size: 181615 Color: 8

Bin 510: 2 of cap free
Amount of items: 2
Items: 
Size: 666833 Color: 4
Size: 333166 Color: 3

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 654848 Color: 10
Size: 173472 Color: 17
Size: 171679 Color: 15

Bin 512: 2 of cap free
Amount of items: 3
Items: 
Size: 592942 Color: 2
Size: 208659 Color: 16
Size: 198398 Color: 10

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 369577 Color: 14
Size: 332183 Color: 4
Size: 298239 Color: 1

Bin 514: 2 of cap free
Amount of items: 3
Items: 
Size: 596916 Color: 3
Size: 208064 Color: 7
Size: 195019 Color: 18

Bin 515: 2 of cap free
Amount of items: 2
Items: 
Size: 728120 Color: 18
Size: 271879 Color: 10

Bin 516: 2 of cap free
Amount of items: 2
Items: 
Size: 742622 Color: 5
Size: 257377 Color: 0

Bin 517: 2 of cap free
Amount of items: 2
Items: 
Size: 729976 Color: 2
Size: 270023 Color: 12

Bin 518: 2 of cap free
Amount of items: 3
Items: 
Size: 588016 Color: 11
Size: 212062 Color: 3
Size: 199921 Color: 7

Bin 519: 2 of cap free
Amount of items: 3
Items: 
Size: 409222 Color: 6
Size: 306811 Color: 13
Size: 283966 Color: 0

Bin 520: 2 of cap free
Amount of items: 3
Items: 
Size: 433168 Color: 11
Size: 285297 Color: 10
Size: 281534 Color: 1

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 434189 Color: 5
Size: 298324 Color: 0
Size: 267486 Color: 9

Bin 522: 2 of cap free
Amount of items: 2
Items: 
Size: 504924 Color: 4
Size: 495075 Color: 12

Bin 523: 2 of cap free
Amount of items: 2
Items: 
Size: 508166 Color: 3
Size: 491833 Color: 7

Bin 524: 2 of cap free
Amount of items: 2
Items: 
Size: 519549 Color: 0
Size: 480450 Color: 1

Bin 525: 2 of cap free
Amount of items: 2
Items: 
Size: 532222 Color: 12
Size: 467777 Color: 0

Bin 526: 2 of cap free
Amount of items: 2
Items: 
Size: 533071 Color: 1
Size: 466928 Color: 5

Bin 527: 2 of cap free
Amount of items: 2
Items: 
Size: 536013 Color: 9
Size: 463986 Color: 13

Bin 528: 2 of cap free
Amount of items: 2
Items: 
Size: 544218 Color: 8
Size: 455781 Color: 7

Bin 529: 2 of cap free
Amount of items: 2
Items: 
Size: 545797 Color: 10
Size: 454202 Color: 18

Bin 530: 2 of cap free
Amount of items: 2
Items: 
Size: 548058 Color: 7
Size: 451941 Color: 19

Bin 531: 2 of cap free
Amount of items: 2
Items: 
Size: 555816 Color: 5
Size: 444183 Color: 13

Bin 532: 2 of cap free
Amount of items: 2
Items: 
Size: 558902 Color: 16
Size: 441097 Color: 3

Bin 533: 2 of cap free
Amount of items: 2
Items: 
Size: 559222 Color: 2
Size: 440777 Color: 15

Bin 534: 2 of cap free
Amount of items: 2
Items: 
Size: 583577 Color: 4
Size: 416422 Color: 10

Bin 535: 2 of cap free
Amount of items: 3
Items: 
Size: 596483 Color: 6
Size: 202916 Color: 19
Size: 200600 Color: 14

Bin 536: 2 of cap free
Amount of items: 3
Items: 
Size: 596609 Color: 19
Size: 207462 Color: 18
Size: 195928 Color: 11

Bin 537: 2 of cap free
Amount of items: 3
Items: 
Size: 597505 Color: 4
Size: 201683 Color: 1
Size: 200811 Color: 7

Bin 538: 2 of cap free
Amount of items: 2
Items: 
Size: 598078 Color: 8
Size: 401921 Color: 13

Bin 539: 2 of cap free
Amount of items: 2
Items: 
Size: 599298 Color: 17
Size: 400701 Color: 15

Bin 540: 2 of cap free
Amount of items: 3
Items: 
Size: 599854 Color: 17
Size: 200482 Color: 19
Size: 199663 Color: 19

Bin 541: 2 of cap free
Amount of items: 2
Items: 
Size: 600432 Color: 15
Size: 399567 Color: 0

Bin 542: 2 of cap free
Amount of items: 2
Items: 
Size: 603710 Color: 8
Size: 396289 Color: 13

Bin 543: 2 of cap free
Amount of items: 3
Items: 
Size: 604196 Color: 15
Size: 199299 Color: 15
Size: 196504 Color: 12

Bin 544: 2 of cap free
Amount of items: 2
Items: 
Size: 606246 Color: 19
Size: 393753 Color: 2

Bin 545: 2 of cap free
Amount of items: 2
Items: 
Size: 609479 Color: 11
Size: 390520 Color: 15

Bin 546: 2 of cap free
Amount of items: 2
Items: 
Size: 620958 Color: 11
Size: 379041 Color: 10

Bin 547: 2 of cap free
Amount of items: 2
Items: 
Size: 628025 Color: 11
Size: 371974 Color: 12

Bin 548: 2 of cap free
Amount of items: 2
Items: 
Size: 636112 Color: 12
Size: 363887 Color: 10

Bin 549: 2 of cap free
Amount of items: 2
Items: 
Size: 639935 Color: 5
Size: 360064 Color: 15

Bin 550: 2 of cap free
Amount of items: 2
Items: 
Size: 645455 Color: 6
Size: 354544 Color: 15

Bin 551: 2 of cap free
Amount of items: 2
Items: 
Size: 648378 Color: 11
Size: 351621 Color: 8

Bin 552: 2 of cap free
Amount of items: 2
Items: 
Size: 651841 Color: 4
Size: 348158 Color: 2

Bin 553: 2 of cap free
Amount of items: 3
Items: 
Size: 666387 Color: 8
Size: 168119 Color: 14
Size: 165493 Color: 2

Bin 554: 2 of cap free
Amount of items: 3
Items: 
Size: 669362 Color: 17
Size: 165643 Color: 0
Size: 164994 Color: 18

Bin 555: 2 of cap free
Amount of items: 2
Items: 
Size: 676071 Color: 17
Size: 323928 Color: 13

Bin 556: 2 of cap free
Amount of items: 2
Items: 
Size: 689053 Color: 9
Size: 310946 Color: 15

Bin 557: 2 of cap free
Amount of items: 2
Items: 
Size: 694145 Color: 2
Size: 305854 Color: 11

Bin 558: 2 of cap free
Amount of items: 2
Items: 
Size: 718827 Color: 10
Size: 281172 Color: 18

Bin 559: 2 of cap free
Amount of items: 2
Items: 
Size: 721540 Color: 11
Size: 278459 Color: 3

Bin 560: 2 of cap free
Amount of items: 2
Items: 
Size: 729135 Color: 1
Size: 270864 Color: 4

Bin 561: 2 of cap free
Amount of items: 2
Items: 
Size: 733219 Color: 14
Size: 266780 Color: 12

Bin 562: 2 of cap free
Amount of items: 2
Items: 
Size: 735971 Color: 2
Size: 264028 Color: 10

Bin 563: 2 of cap free
Amount of items: 2
Items: 
Size: 744139 Color: 3
Size: 255860 Color: 1

Bin 564: 2 of cap free
Amount of items: 2
Items: 
Size: 746460 Color: 6
Size: 253539 Color: 14

Bin 565: 2 of cap free
Amount of items: 2
Items: 
Size: 754308 Color: 12
Size: 245691 Color: 9

Bin 566: 2 of cap free
Amount of items: 3
Items: 
Size: 756812 Color: 0
Size: 123248 Color: 19
Size: 119939 Color: 19

Bin 567: 2 of cap free
Amount of items: 2
Items: 
Size: 771306 Color: 17
Size: 228693 Color: 8

Bin 568: 2 of cap free
Amount of items: 2
Items: 
Size: 788711 Color: 12
Size: 211288 Color: 15

Bin 569: 2 of cap free
Amount of items: 2
Items: 
Size: 795863 Color: 18
Size: 204136 Color: 17

Bin 570: 2 of cap free
Amount of items: 2
Items: 
Size: 796127 Color: 17
Size: 203872 Color: 13

Bin 571: 3 of cap free
Amount of items: 3
Items: 
Size: 479624 Color: 16
Size: 400211 Color: 17
Size: 120163 Color: 12

Bin 572: 3 of cap free
Amount of items: 3
Items: 
Size: 770180 Color: 18
Size: 116924 Color: 11
Size: 112894 Color: 6

Bin 573: 3 of cap free
Amount of items: 3
Items: 
Size: 615398 Color: 13
Size: 192411 Color: 0
Size: 192189 Color: 1

Bin 574: 3 of cap free
Amount of items: 3
Items: 
Size: 614933 Color: 10
Size: 192542 Color: 4
Size: 192523 Color: 1

Bin 575: 3 of cap free
Amount of items: 3
Items: 
Size: 645898 Color: 18
Size: 177275 Color: 1
Size: 176825 Color: 3

Bin 576: 3 of cap free
Amount of items: 2
Items: 
Size: 651894 Color: 9
Size: 348104 Color: 17

Bin 577: 3 of cap free
Amount of items: 3
Items: 
Size: 771481 Color: 12
Size: 118133 Color: 2
Size: 110384 Color: 16

Bin 578: 3 of cap free
Amount of items: 3
Items: 
Size: 599683 Color: 1
Size: 200401 Color: 1
Size: 199914 Color: 10

Bin 579: 3 of cap free
Amount of items: 3
Items: 
Size: 701508 Color: 5
Size: 149299 Color: 15
Size: 149191 Color: 10

Bin 580: 3 of cap free
Amount of items: 3
Items: 
Size: 651194 Color: 16
Size: 174866 Color: 0
Size: 173938 Color: 4

Bin 581: 3 of cap free
Amount of items: 3
Items: 
Size: 370173 Color: 5
Size: 339743 Color: 3
Size: 290082 Color: 18

Bin 582: 3 of cap free
Amount of items: 3
Items: 
Size: 724229 Color: 1
Size: 137917 Color: 4
Size: 137852 Color: 19

Bin 583: 3 of cap free
Amount of items: 2
Items: 
Size: 704632 Color: 9
Size: 295366 Color: 14

Bin 584: 3 of cap free
Amount of items: 3
Items: 
Size: 642053 Color: 12
Size: 179607 Color: 1
Size: 178338 Color: 0

Bin 585: 3 of cap free
Amount of items: 2
Items: 
Size: 754992 Color: 16
Size: 245006 Color: 8

Bin 586: 3 of cap free
Amount of items: 3
Items: 
Size: 362231 Color: 13
Size: 320098 Color: 14
Size: 317669 Color: 7

Bin 587: 3 of cap free
Amount of items: 3
Items: 
Size: 725505 Color: 19
Size: 151646 Color: 8
Size: 122847 Color: 3

Bin 588: 3 of cap free
Amount of items: 3
Items: 
Size: 680171 Color: 9
Size: 160237 Color: 11
Size: 159590 Color: 2

Bin 589: 3 of cap free
Amount of items: 2
Items: 
Size: 640711 Color: 2
Size: 359287 Color: 5

Bin 590: 3 of cap free
Amount of items: 3
Items: 
Size: 377311 Color: 16
Size: 319739 Color: 10
Size: 302948 Color: 1

Bin 591: 3 of cap free
Amount of items: 3
Items: 
Size: 616309 Color: 10
Size: 192485 Color: 5
Size: 191204 Color: 8

Bin 592: 3 of cap free
Amount of items: 3
Items: 
Size: 632797 Color: 16
Size: 183728 Color: 11
Size: 183473 Color: 2

Bin 593: 3 of cap free
Amount of items: 3
Items: 
Size: 716532 Color: 14
Size: 148661 Color: 3
Size: 134805 Color: 18

Bin 594: 3 of cap free
Amount of items: 3
Items: 
Size: 680476 Color: 10
Size: 160159 Color: 16
Size: 159363 Color: 2

Bin 595: 3 of cap free
Amount of items: 2
Items: 
Size: 756885 Color: 8
Size: 243113 Color: 9

Bin 596: 3 of cap free
Amount of items: 3
Items: 
Size: 649594 Color: 15
Size: 197345 Color: 0
Size: 153059 Color: 15

Bin 597: 3 of cap free
Amount of items: 3
Items: 
Size: 382726 Color: 4
Size: 354726 Color: 1
Size: 262546 Color: 3

Bin 598: 3 of cap free
Amount of items: 3
Items: 
Size: 358131 Color: 4
Size: 343497 Color: 15
Size: 298370 Color: 14

Bin 599: 3 of cap free
Amount of items: 3
Items: 
Size: 369653 Color: 6
Size: 330008 Color: 3
Size: 300337 Color: 0

Bin 600: 3 of cap free
Amount of items: 3
Items: 
Size: 657425 Color: 2
Size: 172149 Color: 14
Size: 170424 Color: 4

Bin 601: 3 of cap free
Amount of items: 3
Items: 
Size: 383759 Color: 14
Size: 333483 Color: 4
Size: 282756 Color: 6

Bin 602: 3 of cap free
Amount of items: 3
Items: 
Size: 646896 Color: 8
Size: 176716 Color: 16
Size: 176386 Color: 19

Bin 603: 3 of cap free
Amount of items: 3
Items: 
Size: 696597 Color: 11
Size: 151748 Color: 9
Size: 151653 Color: 19

Bin 604: 3 of cap free
Amount of items: 3
Items: 
Size: 680426 Color: 7
Size: 160374 Color: 18
Size: 159198 Color: 10

Bin 605: 3 of cap free
Amount of items: 3
Items: 
Size: 751111 Color: 16
Size: 129647 Color: 10
Size: 119240 Color: 12

Bin 606: 3 of cap free
Amount of items: 3
Items: 
Size: 689223 Color: 0
Size: 157540 Color: 3
Size: 153235 Color: 12

Bin 607: 3 of cap free
Amount of items: 3
Items: 
Size: 536161 Color: 14
Size: 232899 Color: 9
Size: 230938 Color: 13

Bin 608: 3 of cap free
Amount of items: 3
Items: 
Size: 703407 Color: 9
Size: 148459 Color: 16
Size: 148132 Color: 9

Bin 609: 3 of cap free
Amount of items: 3
Items: 
Size: 659415 Color: 3
Size: 170528 Color: 2
Size: 170055 Color: 17

Bin 610: 3 of cap free
Amount of items: 3
Items: 
Size: 675144 Color: 16
Size: 162983 Color: 11
Size: 161871 Color: 16

Bin 611: 3 of cap free
Amount of items: 3
Items: 
Size: 637517 Color: 0
Size: 181387 Color: 19
Size: 181094 Color: 7

Bin 612: 3 of cap free
Amount of items: 3
Items: 
Size: 739359 Color: 17
Size: 132532 Color: 12
Size: 128107 Color: 17

Bin 613: 3 of cap free
Amount of items: 3
Items: 
Size: 373764 Color: 11
Size: 340667 Color: 16
Size: 285567 Color: 8

Bin 614: 3 of cap free
Amount of items: 2
Items: 
Size: 563767 Color: 17
Size: 436231 Color: 4

Bin 615: 3 of cap free
Amount of items: 2
Items: 
Size: 745607 Color: 16
Size: 254391 Color: 2

Bin 616: 3 of cap free
Amount of items: 3
Items: 
Size: 588270 Color: 9
Size: 212136 Color: 3
Size: 199592 Color: 0

Bin 617: 3 of cap free
Amount of items: 2
Items: 
Size: 775261 Color: 19
Size: 224737 Color: 14

Bin 618: 3 of cap free
Amount of items: 3
Items: 
Size: 372332 Color: 0
Size: 329946 Color: 5
Size: 297720 Color: 0

Bin 619: 3 of cap free
Amount of items: 2
Items: 
Size: 751115 Color: 5
Size: 248883 Color: 2

Bin 620: 3 of cap free
Amount of items: 2
Items: 
Size: 528647 Color: 6
Size: 471351 Color: 10

Bin 621: 3 of cap free
Amount of items: 2
Items: 
Size: 764489 Color: 3
Size: 235509 Color: 8

Bin 622: 3 of cap free
Amount of items: 2
Items: 
Size: 789659 Color: 7
Size: 210339 Color: 0

Bin 623: 3 of cap free
Amount of items: 3
Items: 
Size: 563348 Color: 18
Size: 218421 Color: 10
Size: 218229 Color: 13

Bin 624: 3 of cap free
Amount of items: 3
Items: 
Size: 417910 Color: 7
Size: 338357 Color: 14
Size: 243731 Color: 9

Bin 625: 3 of cap free
Amount of items: 2
Items: 
Size: 533957 Color: 6
Size: 466041 Color: 14

Bin 626: 3 of cap free
Amount of items: 2
Items: 
Size: 537633 Color: 3
Size: 462365 Color: 2

Bin 627: 3 of cap free
Amount of items: 2
Items: 
Size: 540147 Color: 11
Size: 459851 Color: 13

Bin 628: 3 of cap free
Amount of items: 2
Items: 
Size: 547717 Color: 14
Size: 452281 Color: 13

Bin 629: 3 of cap free
Amount of items: 2
Items: 
Size: 550996 Color: 2
Size: 449002 Color: 0

Bin 630: 3 of cap free
Amount of items: 2
Items: 
Size: 558930 Color: 8
Size: 441068 Color: 4

Bin 631: 3 of cap free
Amount of items: 2
Items: 
Size: 577351 Color: 1
Size: 422647 Color: 5

Bin 632: 3 of cap free
Amount of items: 2
Items: 
Size: 582838 Color: 18
Size: 417160 Color: 8

Bin 633: 3 of cap free
Amount of items: 2
Items: 
Size: 597948 Color: 0
Size: 402050 Color: 16

Bin 634: 3 of cap free
Amount of items: 2
Items: 
Size: 599543 Color: 4
Size: 400455 Color: 14

Bin 635: 3 of cap free
Amount of items: 2
Items: 
Size: 606419 Color: 10
Size: 393579 Color: 8

Bin 636: 3 of cap free
Amount of items: 2
Items: 
Size: 606625 Color: 8
Size: 393373 Color: 18

Bin 637: 3 of cap free
Amount of items: 2
Items: 
Size: 615614 Color: 2
Size: 384384 Color: 17

Bin 638: 3 of cap free
Amount of items: 3
Items: 
Size: 617580 Color: 19
Size: 191323 Color: 6
Size: 191095 Color: 3

Bin 639: 3 of cap free
Amount of items: 3
Items: 
Size: 619460 Color: 16
Size: 190402 Color: 1
Size: 190136 Color: 7

Bin 640: 3 of cap free
Amount of items: 2
Items: 
Size: 630429 Color: 17
Size: 369569 Color: 9

Bin 641: 3 of cap free
Amount of items: 3
Items: 
Size: 630592 Color: 15
Size: 184777 Color: 14
Size: 184629 Color: 4

Bin 642: 3 of cap free
Amount of items: 2
Items: 
Size: 640029 Color: 12
Size: 359969 Color: 5

Bin 643: 3 of cap free
Amount of items: 2
Items: 
Size: 641175 Color: 8
Size: 358823 Color: 12

Bin 644: 3 of cap free
Amount of items: 2
Items: 
Size: 645117 Color: 3
Size: 354881 Color: 15

Bin 645: 3 of cap free
Amount of items: 2
Items: 
Size: 646781 Color: 10
Size: 353217 Color: 5

Bin 646: 3 of cap free
Amount of items: 2
Items: 
Size: 647917 Color: 6
Size: 352081 Color: 10

Bin 647: 3 of cap free
Amount of items: 2
Items: 
Size: 657782 Color: 17
Size: 342216 Color: 8

Bin 648: 3 of cap free
Amount of items: 2
Items: 
Size: 667970 Color: 13
Size: 332028 Color: 7

Bin 649: 3 of cap free
Amount of items: 2
Items: 
Size: 687297 Color: 8
Size: 312701 Color: 12

Bin 650: 3 of cap free
Amount of items: 2
Items: 
Size: 695086 Color: 11
Size: 304912 Color: 1

Bin 651: 3 of cap free
Amount of items: 2
Items: 
Size: 702472 Color: 1
Size: 297526 Color: 5

Bin 652: 3 of cap free
Amount of items: 3
Items: 
Size: 713203 Color: 16
Size: 143429 Color: 1
Size: 143366 Color: 4

Bin 653: 3 of cap free
Amount of items: 2
Items: 
Size: 739559 Color: 7
Size: 260439 Color: 15

Bin 654: 3 of cap free
Amount of items: 2
Items: 
Size: 739870 Color: 15
Size: 260128 Color: 12

Bin 655: 3 of cap free
Amount of items: 2
Items: 
Size: 743796 Color: 12
Size: 256202 Color: 4

Bin 656: 3 of cap free
Amount of items: 2
Items: 
Size: 751316 Color: 0
Size: 248682 Color: 8

Bin 657: 3 of cap free
Amount of items: 2
Items: 
Size: 756704 Color: 3
Size: 243294 Color: 8

Bin 658: 3 of cap free
Amount of items: 2
Items: 
Size: 766352 Color: 4
Size: 233646 Color: 1

Bin 659: 3 of cap free
Amount of items: 2
Items: 
Size: 774092 Color: 8
Size: 225906 Color: 5

Bin 660: 3 of cap free
Amount of items: 2
Items: 
Size: 775060 Color: 17
Size: 224938 Color: 18

Bin 661: 3 of cap free
Amount of items: 2
Items: 
Size: 785571 Color: 10
Size: 214427 Color: 1

Bin 662: 3 of cap free
Amount of items: 2
Items: 
Size: 795389 Color: 1
Size: 204609 Color: 18

Bin 663: 4 of cap free
Amount of items: 2
Items: 
Size: 791282 Color: 3
Size: 208715 Color: 8

Bin 664: 4 of cap free
Amount of items: 3
Items: 
Size: 684428 Color: 6
Size: 158222 Color: 16
Size: 157347 Color: 18

Bin 665: 4 of cap free
Amount of items: 3
Items: 
Size: 684621 Color: 14
Size: 157991 Color: 7
Size: 157385 Color: 1

Bin 666: 4 of cap free
Amount of items: 2
Items: 
Size: 787670 Color: 12
Size: 212327 Color: 2

Bin 667: 4 of cap free
Amount of items: 3
Items: 
Size: 651855 Color: 13
Size: 186089 Color: 5
Size: 162053 Color: 14

Bin 668: 4 of cap free
Amount of items: 3
Items: 
Size: 356860 Color: 3
Size: 340910 Color: 4
Size: 302227 Color: 19

Bin 669: 4 of cap free
Amount of items: 2
Items: 
Size: 704506 Color: 14
Size: 295491 Color: 17

Bin 670: 4 of cap free
Amount of items: 3
Items: 
Size: 703079 Color: 15
Size: 148932 Color: 12
Size: 147986 Color: 3

Bin 671: 4 of cap free
Amount of items: 2
Items: 
Size: 742003 Color: 16
Size: 257994 Color: 17

Bin 672: 4 of cap free
Amount of items: 3
Items: 
Size: 647848 Color: 6
Size: 179085 Color: 7
Size: 173064 Color: 15

Bin 673: 4 of cap free
Amount of items: 3
Items: 
Size: 657419 Color: 18
Size: 172196 Color: 19
Size: 170382 Color: 0

Bin 674: 4 of cap free
Amount of items: 3
Items: 
Size: 669709 Color: 10
Size: 166856 Color: 2
Size: 163432 Color: 0

Bin 675: 4 of cap free
Amount of items: 3
Items: 
Size: 634445 Color: 8
Size: 186990 Color: 13
Size: 178562 Color: 11

Bin 676: 4 of cap free
Amount of items: 3
Items: 
Size: 660398 Color: 6
Size: 170157 Color: 9
Size: 169442 Color: 16

Bin 677: 4 of cap free
Amount of items: 3
Items: 
Size: 668157 Color: 14
Size: 167949 Color: 9
Size: 163891 Color: 3

Bin 678: 4 of cap free
Amount of items: 4
Items: 
Size: 266916 Color: 1
Size: 266820 Color: 16
Size: 244089 Color: 4
Size: 222172 Color: 15

Bin 679: 4 of cap free
Amount of items: 2
Items: 
Size: 726637 Color: 19
Size: 273360 Color: 13

Bin 680: 4 of cap free
Amount of items: 2
Items: 
Size: 781347 Color: 18
Size: 218650 Color: 2

Bin 681: 4 of cap free
Amount of items: 3
Items: 
Size: 761525 Color: 0
Size: 119400 Color: 12
Size: 119072 Color: 8

Bin 682: 4 of cap free
Amount of items: 2
Items: 
Size: 685829 Color: 19
Size: 314168 Color: 10

Bin 683: 4 of cap free
Amount of items: 3
Items: 
Size: 640747 Color: 11
Size: 179946 Color: 10
Size: 179304 Color: 7

Bin 684: 4 of cap free
Amount of items: 3
Items: 
Size: 611610 Color: 2
Size: 194390 Color: 9
Size: 193997 Color: 6

Bin 685: 4 of cap free
Amount of items: 3
Items: 
Size: 496074 Color: 16
Size: 378187 Color: 6
Size: 125736 Color: 0

Bin 686: 4 of cap free
Amount of items: 3
Items: 
Size: 567245 Color: 5
Size: 219282 Color: 3
Size: 213470 Color: 18

Bin 687: 4 of cap free
Amount of items: 3
Items: 
Size: 520722 Color: 12
Size: 254771 Color: 6
Size: 224504 Color: 3

Bin 688: 4 of cap free
Amount of items: 3
Items: 
Size: 610169 Color: 4
Size: 198926 Color: 19
Size: 190902 Color: 15

Bin 689: 4 of cap free
Amount of items: 2
Items: 
Size: 616120 Color: 19
Size: 383877 Color: 16

Bin 690: 4 of cap free
Amount of items: 2
Items: 
Size: 539010 Color: 6
Size: 460987 Color: 11

Bin 691: 4 of cap free
Amount of items: 3
Items: 
Size: 536257 Color: 17
Size: 240209 Color: 11
Size: 223531 Color: 1

Bin 692: 4 of cap free
Amount of items: 2
Items: 
Size: 684376 Color: 8
Size: 315621 Color: 9

Bin 693: 4 of cap free
Amount of items: 3
Items: 
Size: 624578 Color: 8
Size: 189056 Color: 14
Size: 186363 Color: 9

Bin 694: 4 of cap free
Amount of items: 2
Items: 
Size: 528224 Color: 10
Size: 471773 Color: 15

Bin 695: 4 of cap free
Amount of items: 2
Items: 
Size: 751217 Color: 8
Size: 248780 Color: 0

Bin 696: 4 of cap free
Amount of items: 2
Items: 
Size: 722272 Color: 6
Size: 277725 Color: 18

Bin 697: 4 of cap free
Amount of items: 3
Items: 
Size: 378402 Color: 7
Size: 338425 Color: 9
Size: 283170 Color: 5

Bin 698: 4 of cap free
Amount of items: 2
Items: 
Size: 617988 Color: 7
Size: 382009 Color: 5

Bin 699: 4 of cap free
Amount of items: 3
Items: 
Size: 533644 Color: 11
Size: 244954 Color: 17
Size: 221399 Color: 17

Bin 700: 4 of cap free
Amount of items: 3
Items: 
Size: 594459 Color: 4
Size: 203088 Color: 10
Size: 202450 Color: 19

Bin 701: 4 of cap free
Amount of items: 3
Items: 
Size: 592150 Color: 9
Size: 207593 Color: 19
Size: 200254 Color: 14

Bin 702: 4 of cap free
Amount of items: 3
Items: 
Size: 486912 Color: 10
Size: 293716 Color: 15
Size: 219369 Color: 15

Bin 703: 4 of cap free
Amount of items: 3
Items: 
Size: 374711 Color: 7
Size: 337927 Color: 14
Size: 287359 Color: 5

Bin 704: 4 of cap free
Amount of items: 3
Items: 
Size: 387100 Color: 10
Size: 318201 Color: 6
Size: 294696 Color: 7

Bin 705: 4 of cap free
Amount of items: 2
Items: 
Size: 510469 Color: 9
Size: 489528 Color: 1

Bin 706: 4 of cap free
Amount of items: 2
Items: 
Size: 517586 Color: 13
Size: 482411 Color: 5

Bin 707: 4 of cap free
Amount of items: 2
Items: 
Size: 522534 Color: 16
Size: 477463 Color: 5

Bin 708: 4 of cap free
Amount of items: 2
Items: 
Size: 528346 Color: 19
Size: 471651 Color: 9

Bin 709: 4 of cap free
Amount of items: 2
Items: 
Size: 531172 Color: 11
Size: 468825 Color: 10

Bin 710: 4 of cap free
Amount of items: 2
Items: 
Size: 532371 Color: 11
Size: 467626 Color: 0

Bin 711: 4 of cap free
Amount of items: 3
Items: 
Size: 541228 Color: 10
Size: 237348 Color: 5
Size: 221421 Color: 12

Bin 712: 4 of cap free
Amount of items: 2
Items: 
Size: 545330 Color: 2
Size: 454667 Color: 7

Bin 713: 4 of cap free
Amount of items: 2
Items: 
Size: 550487 Color: 4
Size: 449510 Color: 17

Bin 714: 4 of cap free
Amount of items: 2
Items: 
Size: 561920 Color: 16
Size: 438077 Color: 5

Bin 715: 4 of cap free
Amount of items: 2
Items: 
Size: 567388 Color: 11
Size: 432609 Color: 2

Bin 716: 4 of cap free
Amount of items: 2
Items: 
Size: 567814 Color: 4
Size: 432183 Color: 14

Bin 717: 4 of cap free
Amount of items: 2
Items: 
Size: 569124 Color: 4
Size: 430873 Color: 15

Bin 718: 4 of cap free
Amount of items: 2
Items: 
Size: 573505 Color: 9
Size: 426492 Color: 12

Bin 719: 4 of cap free
Amount of items: 2
Items: 
Size: 574371 Color: 16
Size: 425626 Color: 15

Bin 720: 4 of cap free
Amount of items: 2
Items: 
Size: 577701 Color: 4
Size: 422296 Color: 13

Bin 721: 4 of cap free
Amount of items: 2
Items: 
Size: 578369 Color: 7
Size: 421628 Color: 16

Bin 722: 4 of cap free
Amount of items: 2
Items: 
Size: 583808 Color: 7
Size: 416189 Color: 4

Bin 723: 4 of cap free
Amount of items: 2
Items: 
Size: 592792 Color: 11
Size: 407205 Color: 15

Bin 724: 4 of cap free
Amount of items: 3
Items: 
Size: 617637 Color: 5
Size: 191181 Color: 19
Size: 191179 Color: 4

Bin 725: 4 of cap free
Amount of items: 2
Items: 
Size: 623986 Color: 14
Size: 376011 Color: 9

Bin 726: 4 of cap free
Amount of items: 2
Items: 
Size: 632589 Color: 19
Size: 367408 Color: 14

Bin 727: 4 of cap free
Amount of items: 2
Items: 
Size: 632829 Color: 3
Size: 367168 Color: 7

Bin 728: 4 of cap free
Amount of items: 3
Items: 
Size: 633087 Color: 13
Size: 183493 Color: 13
Size: 183417 Color: 7

Bin 729: 4 of cap free
Amount of items: 3
Items: 
Size: 643724 Color: 2
Size: 178201 Color: 7
Size: 178072 Color: 9

Bin 730: 4 of cap free
Amount of items: 2
Items: 
Size: 646346 Color: 3
Size: 353651 Color: 2

Bin 731: 4 of cap free
Amount of items: 2
Items: 
Size: 666167 Color: 12
Size: 333830 Color: 16

Bin 732: 4 of cap free
Amount of items: 2
Items: 
Size: 668767 Color: 11
Size: 331230 Color: 14

Bin 733: 4 of cap free
Amount of items: 2
Items: 
Size: 676473 Color: 2
Size: 323524 Color: 6

Bin 734: 4 of cap free
Amount of items: 2
Items: 
Size: 677833 Color: 9
Size: 322164 Color: 5

Bin 735: 4 of cap free
Amount of items: 2
Items: 
Size: 682477 Color: 19
Size: 317520 Color: 5

Bin 736: 4 of cap free
Amount of items: 2
Items: 
Size: 687103 Color: 2
Size: 312894 Color: 6

Bin 737: 4 of cap free
Amount of items: 2
Items: 
Size: 700370 Color: 5
Size: 299627 Color: 16

Bin 738: 4 of cap free
Amount of items: 2
Items: 
Size: 705507 Color: 19
Size: 294490 Color: 10

Bin 739: 4 of cap free
Amount of items: 2
Items: 
Size: 710642 Color: 17
Size: 289355 Color: 6

Bin 740: 4 of cap free
Amount of items: 2
Items: 
Size: 713183 Color: 1
Size: 286814 Color: 6

Bin 741: 4 of cap free
Amount of items: 2
Items: 
Size: 718710 Color: 8
Size: 281287 Color: 4

Bin 742: 4 of cap free
Amount of items: 2
Items: 
Size: 720287 Color: 3
Size: 279710 Color: 19

Bin 743: 4 of cap free
Amount of items: 2
Items: 
Size: 731752 Color: 19
Size: 268245 Color: 12

Bin 744: 4 of cap free
Amount of items: 2
Items: 
Size: 734107 Color: 1
Size: 265890 Color: 8

Bin 745: 4 of cap free
Amount of items: 2
Items: 
Size: 744313 Color: 8
Size: 255684 Color: 7

Bin 746: 4 of cap free
Amount of items: 2
Items: 
Size: 769638 Color: 15
Size: 230359 Color: 2

Bin 747: 4 of cap free
Amount of items: 2
Items: 
Size: 769782 Color: 13
Size: 230215 Color: 1

Bin 748: 4 of cap free
Amount of items: 2
Items: 
Size: 770716 Color: 14
Size: 229281 Color: 13

Bin 749: 4 of cap free
Amount of items: 2
Items: 
Size: 771422 Color: 0
Size: 228575 Color: 13

Bin 750: 4 of cap free
Amount of items: 2
Items: 
Size: 780947 Color: 11
Size: 219050 Color: 14

Bin 751: 4 of cap free
Amount of items: 2
Items: 
Size: 795318 Color: 4
Size: 204679 Color: 5

Bin 752: 4 of cap free
Amount of items: 2
Items: 
Size: 798997 Color: 7
Size: 201000 Color: 13

Bin 753: 5 of cap free
Amount of items: 3
Items: 
Size: 766424 Color: 4
Size: 129751 Color: 9
Size: 103821 Color: 14

Bin 754: 5 of cap free
Amount of items: 3
Items: 
Size: 698476 Color: 1
Size: 162997 Color: 4
Size: 138523 Color: 10

Bin 755: 5 of cap free
Amount of items: 3
Items: 
Size: 362391 Color: 3
Size: 331809 Color: 10
Size: 305796 Color: 16

Bin 756: 5 of cap free
Amount of items: 3
Items: 
Size: 751490 Color: 10
Size: 127232 Color: 10
Size: 121274 Color: 2

Bin 757: 5 of cap free
Amount of items: 3
Items: 
Size: 747012 Color: 8
Size: 126772 Color: 2
Size: 126212 Color: 13

Bin 758: 5 of cap free
Amount of items: 3
Items: 
Size: 745665 Color: 9
Size: 129243 Color: 16
Size: 125088 Color: 11

Bin 759: 5 of cap free
Amount of items: 2
Items: 
Size: 698257 Color: 14
Size: 301739 Color: 8

Bin 760: 5 of cap free
Amount of items: 3
Items: 
Size: 786452 Color: 7
Size: 108221 Color: 17
Size: 105323 Color: 8

Bin 761: 5 of cap free
Amount of items: 3
Items: 
Size: 357198 Color: 8
Size: 344032 Color: 9
Size: 298766 Color: 17

Bin 762: 5 of cap free
Amount of items: 3
Items: 
Size: 726435 Color: 4
Size: 142976 Color: 6
Size: 130585 Color: 8

Bin 763: 5 of cap free
Amount of items: 3
Items: 
Size: 699144 Color: 12
Size: 170594 Color: 3
Size: 130258 Color: 2

Bin 764: 5 of cap free
Amount of items: 3
Items: 
Size: 368439 Color: 13
Size: 337549 Color: 3
Size: 294008 Color: 14

Bin 765: 5 of cap free
Amount of items: 3
Items: 
Size: 788431 Color: 13
Size: 108016 Color: 1
Size: 103549 Color: 5

Bin 766: 5 of cap free
Amount of items: 2
Items: 
Size: 648657 Color: 11
Size: 351339 Color: 4

Bin 767: 5 of cap free
Amount of items: 3
Items: 
Size: 703725 Color: 1
Size: 155416 Color: 7
Size: 140855 Color: 0

Bin 768: 5 of cap free
Amount of items: 3
Items: 
Size: 626944 Color: 5
Size: 187104 Color: 5
Size: 185948 Color: 9

Bin 769: 5 of cap free
Amount of items: 3
Items: 
Size: 756066 Color: 9
Size: 123652 Color: 11
Size: 120278 Color: 10

Bin 770: 5 of cap free
Amount of items: 3
Items: 
Size: 768021 Color: 2
Size: 118344 Color: 17
Size: 113631 Color: 14

Bin 771: 5 of cap free
Amount of items: 3
Items: 
Size: 641653 Color: 11
Size: 179240 Color: 6
Size: 179103 Color: 11

Bin 772: 5 of cap free
Amount of items: 2
Items: 
Size: 780763 Color: 18
Size: 219233 Color: 2

Bin 773: 5 of cap free
Amount of items: 3
Items: 
Size: 634907 Color: 2
Size: 182804 Color: 9
Size: 182285 Color: 16

Bin 774: 5 of cap free
Amount of items: 2
Items: 
Size: 723369 Color: 18
Size: 276627 Color: 6

Bin 775: 5 of cap free
Amount of items: 3
Items: 
Size: 668857 Color: 12
Size: 166778 Color: 10
Size: 164361 Color: 13

Bin 776: 5 of cap free
Amount of items: 3
Items: 
Size: 607046 Color: 16
Size: 197254 Color: 4
Size: 195696 Color: 3

Bin 777: 5 of cap free
Amount of items: 3
Items: 
Size: 625778 Color: 19
Size: 189383 Color: 0
Size: 184835 Color: 9

Bin 778: 5 of cap free
Amount of items: 3
Items: 
Size: 616513 Color: 9
Size: 191925 Color: 13
Size: 191558 Color: 8

Bin 779: 5 of cap free
Amount of items: 3
Items: 
Size: 362582 Color: 1
Size: 337370 Color: 3
Size: 300044 Color: 11

Bin 780: 5 of cap free
Amount of items: 2
Items: 
Size: 708482 Color: 2
Size: 291514 Color: 19

Bin 781: 5 of cap free
Amount of items: 2
Items: 
Size: 776715 Color: 17
Size: 223281 Color: 0

Bin 782: 5 of cap free
Amount of items: 3
Items: 
Size: 541571 Color: 18
Size: 234081 Color: 5
Size: 224344 Color: 13

Bin 783: 5 of cap free
Amount of items: 3
Items: 
Size: 732218 Color: 16
Size: 134001 Color: 7
Size: 133777 Color: 18

Bin 784: 5 of cap free
Amount of items: 2
Items: 
Size: 787613 Color: 19
Size: 212383 Color: 13

Bin 785: 5 of cap free
Amount of items: 3
Items: 
Size: 623964 Color: 8
Size: 188811 Color: 13
Size: 187221 Color: 6

Bin 786: 5 of cap free
Amount of items: 2
Items: 
Size: 737121 Color: 1
Size: 262875 Color: 10

Bin 787: 5 of cap free
Amount of items: 2
Items: 
Size: 761040 Color: 10
Size: 238956 Color: 18

Bin 788: 5 of cap free
Amount of items: 3
Items: 
Size: 492130 Color: 9
Size: 360123 Color: 17
Size: 147743 Color: 17

Bin 789: 5 of cap free
Amount of items: 2
Items: 
Size: 775409 Color: 10
Size: 224587 Color: 12

Bin 790: 5 of cap free
Amount of items: 3
Items: 
Size: 793053 Color: 0
Size: 105431 Color: 3
Size: 101512 Color: 0

Bin 791: 5 of cap free
Amount of items: 2
Items: 
Size: 699215 Color: 8
Size: 300781 Color: 6

Bin 792: 5 of cap free
Amount of items: 3
Items: 
Size: 617232 Color: 10
Size: 195002 Color: 7
Size: 187762 Color: 9

Bin 793: 5 of cap free
Amount of items: 2
Items: 
Size: 761951 Color: 17
Size: 238045 Color: 16

Bin 794: 5 of cap free
Amount of items: 2
Items: 
Size: 732758 Color: 5
Size: 267238 Color: 18

Bin 795: 5 of cap free
Amount of items: 2
Items: 
Size: 756215 Color: 11
Size: 243781 Color: 16

Bin 796: 5 of cap free
Amount of items: 2
Items: 
Size: 760155 Color: 12
Size: 239841 Color: 16

Bin 797: 5 of cap free
Amount of items: 2
Items: 
Size: 502115 Color: 5
Size: 497881 Color: 9

Bin 798: 5 of cap free
Amount of items: 2
Items: 
Size: 609235 Color: 7
Size: 390761 Color: 15

Bin 799: 5 of cap free
Amount of items: 3
Items: 
Size: 355634 Color: 8
Size: 339709 Color: 15
Size: 304653 Color: 18

Bin 800: 5 of cap free
Amount of items: 3
Items: 
Size: 377309 Color: 10
Size: 330414 Color: 6
Size: 292273 Color: 15

Bin 801: 5 of cap free
Amount of items: 3
Items: 
Size: 437610 Color: 18
Size: 282176 Color: 1
Size: 280210 Color: 8

Bin 802: 5 of cap free
Amount of items: 2
Items: 
Size: 500264 Color: 10
Size: 499732 Color: 19

Bin 803: 5 of cap free
Amount of items: 2
Items: 
Size: 513296 Color: 18
Size: 486700 Color: 15

Bin 804: 5 of cap free
Amount of items: 2
Items: 
Size: 514063 Color: 3
Size: 485933 Color: 8

Bin 805: 5 of cap free
Amount of items: 2
Items: 
Size: 514203 Color: 12
Size: 485793 Color: 19

Bin 806: 5 of cap free
Amount of items: 2
Items: 
Size: 550365 Color: 18
Size: 449631 Color: 9

Bin 807: 5 of cap free
Amount of items: 2
Items: 
Size: 550392 Color: 1
Size: 449604 Color: 12

Bin 808: 5 of cap free
Amount of items: 2
Items: 
Size: 550739 Color: 14
Size: 449257 Color: 7

Bin 809: 5 of cap free
Amount of items: 2
Items: 
Size: 554504 Color: 10
Size: 445492 Color: 7

Bin 810: 5 of cap free
Amount of items: 2
Items: 
Size: 570267 Color: 5
Size: 429729 Color: 16

Bin 811: 5 of cap free
Amount of items: 3
Items: 
Size: 605914 Color: 12
Size: 198137 Color: 3
Size: 195945 Color: 14

Bin 812: 5 of cap free
Amount of items: 2
Items: 
Size: 606854 Color: 18
Size: 393142 Color: 6

Bin 813: 5 of cap free
Amount of items: 2
Items: 
Size: 607755 Color: 13
Size: 392241 Color: 15

Bin 814: 5 of cap free
Amount of items: 3
Items: 
Size: 625694 Color: 9
Size: 187396 Color: 17
Size: 186906 Color: 11

Bin 815: 5 of cap free
Amount of items: 3
Items: 
Size: 626371 Color: 0
Size: 186873 Color: 11
Size: 186752 Color: 6

Bin 816: 5 of cap free
Amount of items: 2
Items: 
Size: 629924 Color: 3
Size: 370072 Color: 9

Bin 817: 5 of cap free
Amount of items: 3
Items: 
Size: 634532 Color: 17
Size: 182846 Color: 17
Size: 182618 Color: 0

Bin 818: 5 of cap free
Amount of items: 2
Items: 
Size: 639115 Color: 1
Size: 360881 Color: 16

Bin 819: 5 of cap free
Amount of items: 3
Items: 
Size: 641542 Color: 8
Size: 179278 Color: 14
Size: 179176 Color: 14

Bin 820: 5 of cap free
Amount of items: 2
Items: 
Size: 642985 Color: 5
Size: 357011 Color: 13

Bin 821: 5 of cap free
Amount of items: 2
Items: 
Size: 650775 Color: 6
Size: 349221 Color: 18

Bin 822: 5 of cap free
Amount of items: 2
Items: 
Size: 655401 Color: 2
Size: 344595 Color: 7

Bin 823: 5 of cap free
Amount of items: 2
Items: 
Size: 660350 Color: 18
Size: 339646 Color: 1

Bin 824: 5 of cap free
Amount of items: 3
Items: 
Size: 661396 Color: 16
Size: 169701 Color: 17
Size: 168899 Color: 6

Bin 825: 5 of cap free
Amount of items: 2
Items: 
Size: 661596 Color: 9
Size: 338400 Color: 12

Bin 826: 5 of cap free
Amount of items: 2
Items: 
Size: 672795 Color: 15
Size: 327201 Color: 2

Bin 827: 5 of cap free
Amount of items: 2
Items: 
Size: 676645 Color: 3
Size: 323351 Color: 0

Bin 828: 5 of cap free
Amount of items: 2
Items: 
Size: 678297 Color: 6
Size: 321699 Color: 4

Bin 829: 5 of cap free
Amount of items: 2
Items: 
Size: 681406 Color: 10
Size: 318590 Color: 5

Bin 830: 5 of cap free
Amount of items: 2
Items: 
Size: 686875 Color: 4
Size: 313121 Color: 2

Bin 831: 5 of cap free
Amount of items: 2
Items: 
Size: 696163 Color: 10
Size: 303833 Color: 5

Bin 832: 5 of cap free
Amount of items: 2
Items: 
Size: 701701 Color: 15
Size: 298295 Color: 16

Bin 833: 5 of cap free
Amount of items: 2
Items: 
Size: 714174 Color: 8
Size: 285822 Color: 2

Bin 834: 5 of cap free
Amount of items: 2
Items: 
Size: 716510 Color: 17
Size: 283486 Color: 12

Bin 835: 5 of cap free
Amount of items: 2
Items: 
Size: 729032 Color: 5
Size: 270964 Color: 15

Bin 836: 5 of cap free
Amount of items: 2
Items: 
Size: 736040 Color: 0
Size: 263956 Color: 6

Bin 837: 5 of cap free
Amount of items: 2
Items: 
Size: 740800 Color: 2
Size: 259196 Color: 1

Bin 838: 5 of cap free
Amount of items: 2
Items: 
Size: 761934 Color: 13
Size: 238062 Color: 7

Bin 839: 5 of cap free
Amount of items: 2
Items: 
Size: 767107 Color: 6
Size: 232889 Color: 5

Bin 840: 6 of cap free
Amount of items: 3
Items: 
Size: 554150 Color: 16
Size: 225582 Color: 3
Size: 220263 Color: 19

Bin 841: 6 of cap free
Amount of items: 3
Items: 
Size: 641655 Color: 7
Size: 179902 Color: 19
Size: 178438 Color: 16

Bin 842: 6 of cap free
Amount of items: 3
Items: 
Size: 531700 Color: 16
Size: 245226 Color: 0
Size: 223069 Color: 2

Bin 843: 6 of cap free
Amount of items: 3
Items: 
Size: 745391 Color: 17
Size: 128224 Color: 7
Size: 126380 Color: 10

Bin 844: 6 of cap free
Amount of items: 2
Items: 
Size: 667398 Color: 16
Size: 332597 Color: 10

Bin 845: 6 of cap free
Amount of items: 2
Items: 
Size: 635456 Color: 17
Size: 364539 Color: 15

Bin 846: 6 of cap free
Amount of items: 3
Items: 
Size: 655582 Color: 8
Size: 173081 Color: 19
Size: 171332 Color: 15

Bin 847: 6 of cap free
Amount of items: 3
Items: 
Size: 614779 Color: 17
Size: 193704 Color: 9
Size: 191512 Color: 2

Bin 848: 6 of cap free
Amount of items: 3
Items: 
Size: 531825 Color: 0
Size: 245585 Color: 16
Size: 222585 Color: 4

Bin 849: 6 of cap free
Amount of items: 3
Items: 
Size: 599054 Color: 18
Size: 203628 Color: 3
Size: 197313 Color: 8

Bin 850: 6 of cap free
Amount of items: 3
Items: 
Size: 726270 Color: 10
Size: 158315 Color: 11
Size: 115410 Color: 2

Bin 851: 6 of cap free
Amount of items: 3
Items: 
Size: 417892 Color: 9
Size: 337605 Color: 7
Size: 244498 Color: 3

Bin 852: 6 of cap free
Amount of items: 3
Items: 
Size: 590973 Color: 9
Size: 210204 Color: 9
Size: 198818 Color: 16

Bin 853: 6 of cap free
Amount of items: 3
Items: 
Size: 503753 Color: 7
Size: 301365 Color: 11
Size: 194877 Color: 16

Bin 854: 6 of cap free
Amount of items: 3
Items: 
Size: 668361 Color: 14
Size: 167670 Color: 8
Size: 163964 Color: 17

Bin 855: 6 of cap free
Amount of items: 3
Items: 
Size: 668722 Color: 7
Size: 166388 Color: 8
Size: 164885 Color: 11

Bin 856: 6 of cap free
Amount of items: 2
Items: 
Size: 630756 Color: 14
Size: 369239 Color: 19

Bin 857: 6 of cap free
Amount of items: 2
Items: 
Size: 778640 Color: 15
Size: 221355 Color: 10

Bin 858: 6 of cap free
Amount of items: 3
Items: 
Size: 457398 Color: 13
Size: 280532 Color: 2
Size: 262065 Color: 0

Bin 859: 6 of cap free
Amount of items: 2
Items: 
Size: 780971 Color: 14
Size: 219024 Color: 12

Bin 860: 6 of cap free
Amount of items: 3
Items: 
Size: 438387 Color: 6
Size: 294644 Color: 16
Size: 266964 Color: 15

Bin 861: 6 of cap free
Amount of items: 2
Items: 
Size: 728757 Color: 5
Size: 271238 Color: 16

Bin 862: 6 of cap free
Amount of items: 2
Items: 
Size: 663151 Color: 11
Size: 336844 Color: 19

Bin 863: 6 of cap free
Amount of items: 2
Items: 
Size: 788377 Color: 0
Size: 211618 Color: 7

Bin 864: 6 of cap free
Amount of items: 2
Items: 
Size: 704533 Color: 17
Size: 295462 Color: 18

Bin 865: 6 of cap free
Amount of items: 3
Items: 
Size: 521324 Color: 11
Size: 263206 Color: 16
Size: 215465 Color: 16

Bin 866: 6 of cap free
Amount of items: 2
Items: 
Size: 732824 Color: 0
Size: 267171 Color: 19

Bin 867: 6 of cap free
Amount of items: 3
Items: 
Size: 511147 Color: 18
Size: 263688 Color: 13
Size: 225160 Color: 1

Bin 868: 6 of cap free
Amount of items: 2
Items: 
Size: 521343 Color: 13
Size: 478652 Color: 9

Bin 869: 6 of cap free
Amount of items: 3
Items: 
Size: 374939 Color: 9
Size: 318169 Color: 17
Size: 306887 Color: 11

Bin 870: 6 of cap free
Amount of items: 2
Items: 
Size: 502309 Color: 15
Size: 497686 Color: 2

Bin 871: 6 of cap free
Amount of items: 2
Items: 
Size: 540216 Color: 1
Size: 459779 Color: 19

Bin 872: 6 of cap free
Amount of items: 2
Items: 
Size: 540716 Color: 7
Size: 459279 Color: 18

Bin 873: 6 of cap free
Amount of items: 2
Items: 
Size: 545633 Color: 4
Size: 454362 Color: 14

Bin 874: 6 of cap free
Amount of items: 2
Items: 
Size: 551736 Color: 18
Size: 448259 Color: 5

Bin 875: 6 of cap free
Amount of items: 2
Items: 
Size: 556292 Color: 2
Size: 443703 Color: 8

Bin 876: 6 of cap free
Amount of items: 2
Items: 
Size: 557477 Color: 18
Size: 442518 Color: 0

Bin 877: 6 of cap free
Amount of items: 2
Items: 
Size: 558873 Color: 7
Size: 441122 Color: 6

Bin 878: 6 of cap free
Amount of items: 2
Items: 
Size: 568501 Color: 4
Size: 431494 Color: 16

Bin 879: 6 of cap free
Amount of items: 2
Items: 
Size: 569083 Color: 13
Size: 430912 Color: 1

Bin 880: 6 of cap free
Amount of items: 2
Items: 
Size: 571515 Color: 15
Size: 428480 Color: 5

Bin 881: 6 of cap free
Amount of items: 2
Items: 
Size: 577933 Color: 9
Size: 422062 Color: 19

Bin 882: 6 of cap free
Amount of items: 2
Items: 
Size: 596329 Color: 14
Size: 403666 Color: 18

Bin 883: 6 of cap free
Amount of items: 2
Items: 
Size: 612546 Color: 10
Size: 387449 Color: 19

Bin 884: 6 of cap free
Amount of items: 2
Items: 
Size: 623147 Color: 15
Size: 376848 Color: 4

Bin 885: 6 of cap free
Amount of items: 2
Items: 
Size: 635475 Color: 3
Size: 364520 Color: 4

Bin 886: 6 of cap free
Amount of items: 3
Items: 
Size: 639755 Color: 9
Size: 180411 Color: 14
Size: 179829 Color: 16

Bin 887: 6 of cap free
Amount of items: 2
Items: 
Size: 642307 Color: 17
Size: 357688 Color: 12

Bin 888: 6 of cap free
Amount of items: 2
Items: 
Size: 649186 Color: 0
Size: 350809 Color: 18

Bin 889: 6 of cap free
Amount of items: 2
Items: 
Size: 662089 Color: 2
Size: 337906 Color: 18

Bin 890: 6 of cap free
Amount of items: 2
Items: 
Size: 664042 Color: 6
Size: 335953 Color: 14

Bin 891: 6 of cap free
Amount of items: 2
Items: 
Size: 686072 Color: 4
Size: 313923 Color: 11

Bin 892: 6 of cap free
Amount of items: 2
Items: 
Size: 690701 Color: 16
Size: 309294 Color: 13

Bin 893: 6 of cap free
Amount of items: 3
Items: 
Size: 720720 Color: 14
Size: 139875 Color: 2
Size: 139400 Color: 8

Bin 894: 6 of cap free
Amount of items: 2
Items: 
Size: 720988 Color: 7
Size: 279007 Color: 12

Bin 895: 6 of cap free
Amount of items: 2
Items: 
Size: 726421 Color: 4
Size: 273574 Color: 17

Bin 896: 6 of cap free
Amount of items: 2
Items: 
Size: 727746 Color: 13
Size: 272249 Color: 1

Bin 897: 6 of cap free
Amount of items: 2
Items: 
Size: 747702 Color: 5
Size: 252293 Color: 14

Bin 898: 6 of cap free
Amount of items: 2
Items: 
Size: 756773 Color: 1
Size: 243222 Color: 0

Bin 899: 6 of cap free
Amount of items: 2
Items: 
Size: 764144 Color: 5
Size: 235851 Color: 4

Bin 900: 6 of cap free
Amount of items: 2
Items: 
Size: 777417 Color: 13
Size: 222578 Color: 3

Bin 901: 6 of cap free
Amount of items: 2
Items: 
Size: 783280 Color: 9
Size: 216715 Color: 14

Bin 902: 6 of cap free
Amount of items: 2
Items: 
Size: 785811 Color: 12
Size: 214184 Color: 3

Bin 903: 7 of cap free
Amount of items: 3
Items: 
Size: 358388 Color: 6
Size: 343543 Color: 8
Size: 298063 Color: 15

Bin 904: 7 of cap free
Amount of items: 3
Items: 
Size: 701242 Color: 1
Size: 150273 Color: 6
Size: 148479 Color: 5

Bin 905: 7 of cap free
Amount of items: 3
Items: 
Size: 670381 Color: 14
Size: 171408 Color: 7
Size: 158205 Color: 7

Bin 906: 7 of cap free
Amount of items: 3
Items: 
Size: 675282 Color: 14
Size: 163196 Color: 15
Size: 161516 Color: 7

Bin 907: 7 of cap free
Amount of items: 3
Items: 
Size: 699912 Color: 13
Size: 150049 Color: 11
Size: 150033 Color: 11

Bin 908: 7 of cap free
Amount of items: 3
Items: 
Size: 739055 Color: 0
Size: 130486 Color: 1
Size: 130453 Color: 19

Bin 909: 7 of cap free
Amount of items: 3
Items: 
Size: 594345 Color: 18
Size: 207571 Color: 12
Size: 198078 Color: 17

Bin 910: 7 of cap free
Amount of items: 3
Items: 
Size: 695704 Color: 7
Size: 152468 Color: 18
Size: 151822 Color: 15

Bin 911: 7 of cap free
Amount of items: 3
Items: 
Size: 613731 Color: 17
Size: 193271 Color: 3
Size: 192992 Color: 5

Bin 912: 7 of cap free
Amount of items: 2
Items: 
Size: 622757 Color: 13
Size: 377237 Color: 4

Bin 913: 7 of cap free
Amount of items: 3
Items: 
Size: 720095 Color: 11
Size: 140151 Color: 11
Size: 139748 Color: 3

Bin 914: 7 of cap free
Amount of items: 3
Items: 
Size: 697463 Color: 12
Size: 152904 Color: 8
Size: 149627 Color: 4

Bin 915: 7 of cap free
Amount of items: 2
Items: 
Size: 735819 Color: 18
Size: 264175 Color: 4

Bin 916: 7 of cap free
Amount of items: 3
Items: 
Size: 671730 Color: 2
Size: 164144 Color: 7
Size: 164120 Color: 1

Bin 917: 7 of cap free
Amount of items: 3
Items: 
Size: 656591 Color: 0
Size: 177123 Color: 5
Size: 166280 Color: 10

Bin 918: 7 of cap free
Amount of items: 3
Items: 
Size: 760282 Color: 6
Size: 122422 Color: 5
Size: 117290 Color: 19

Bin 919: 7 of cap free
Amount of items: 3
Items: 
Size: 628069 Color: 16
Size: 186012 Color: 1
Size: 185913 Color: 15

Bin 920: 7 of cap free
Amount of items: 3
Items: 
Size: 372139 Color: 17
Size: 329709 Color: 7
Size: 298146 Color: 0

Bin 921: 7 of cap free
Amount of items: 3
Items: 
Size: 682710 Color: 2
Size: 158982 Color: 17
Size: 158302 Color: 12

Bin 922: 7 of cap free
Amount of items: 4
Items: 
Size: 594576 Color: 12
Size: 185929 Color: 17
Size: 112862 Color: 19
Size: 106627 Color: 17

Bin 923: 7 of cap free
Amount of items: 2
Items: 
Size: 680259 Color: 11
Size: 319735 Color: 19

Bin 924: 7 of cap free
Amount of items: 3
Items: 
Size: 537010 Color: 13
Size: 233245 Color: 1
Size: 229739 Color: 8

Bin 925: 7 of cap free
Amount of items: 4
Items: 
Size: 592395 Color: 8
Size: 197889 Color: 15
Size: 109078 Color: 0
Size: 100632 Color: 16

Bin 926: 7 of cap free
Amount of items: 3
Items: 
Size: 785716 Color: 8
Size: 108629 Color: 9
Size: 105649 Color: 8

Bin 927: 7 of cap free
Amount of items: 3
Items: 
Size: 702487 Color: 2
Size: 149387 Color: 16
Size: 148120 Color: 8

Bin 928: 7 of cap free
Amount of items: 3
Items: 
Size: 672700 Color: 0
Size: 172757 Color: 8
Size: 154537 Color: 13

Bin 929: 7 of cap free
Amount of items: 2
Items: 
Size: 716046 Color: 3
Size: 283948 Color: 17

Bin 930: 7 of cap free
Amount of items: 2
Items: 
Size: 737831 Color: 5
Size: 262163 Color: 0

Bin 931: 7 of cap free
Amount of items: 3
Items: 
Size: 552761 Color: 3
Size: 225611 Color: 2
Size: 221622 Color: 12

Bin 932: 7 of cap free
Amount of items: 3
Items: 
Size: 609136 Color: 14
Size: 195808 Color: 2
Size: 195050 Color: 15

Bin 933: 7 of cap free
Amount of items: 3
Items: 
Size: 593982 Color: 2
Size: 210948 Color: 17
Size: 195064 Color: 14

Bin 934: 7 of cap free
Amount of items: 2
Items: 
Size: 554604 Color: 12
Size: 445390 Color: 6

Bin 935: 7 of cap free
Amount of items: 3
Items: 
Size: 364771 Color: 9
Size: 355072 Color: 16
Size: 280151 Color: 0

Bin 936: 7 of cap free
Amount of items: 3
Items: 
Size: 444980 Color: 4
Size: 287316 Color: 7
Size: 267698 Color: 17

Bin 937: 7 of cap free
Amount of items: 2
Items: 
Size: 515589 Color: 8
Size: 484405 Color: 0

Bin 938: 7 of cap free
Amount of items: 2
Items: 
Size: 525400 Color: 7
Size: 474594 Color: 9

Bin 939: 7 of cap free
Amount of items: 2
Items: 
Size: 532533 Color: 13
Size: 467461 Color: 0

Bin 940: 7 of cap free
Amount of items: 2
Items: 
Size: 539074 Color: 3
Size: 460920 Color: 9

Bin 941: 7 of cap free
Amount of items: 2
Items: 
Size: 545412 Color: 7
Size: 454582 Color: 1

Bin 942: 7 of cap free
Amount of items: 2
Items: 
Size: 546864 Color: 10
Size: 453130 Color: 9

Bin 943: 7 of cap free
Amount of items: 2
Items: 
Size: 551549 Color: 7
Size: 448445 Color: 10

Bin 944: 7 of cap free
Amount of items: 2
Items: 
Size: 555526 Color: 3
Size: 444468 Color: 10

Bin 945: 7 of cap free
Amount of items: 2
Items: 
Size: 556534 Color: 9
Size: 443460 Color: 5

Bin 946: 7 of cap free
Amount of items: 2
Items: 
Size: 559222 Color: 18
Size: 440772 Color: 4

Bin 947: 7 of cap free
Amount of items: 2
Items: 
Size: 564845 Color: 18
Size: 435149 Color: 6

Bin 948: 7 of cap free
Amount of items: 2
Items: 
Size: 565177 Color: 5
Size: 434817 Color: 10

Bin 949: 7 of cap free
Amount of items: 2
Items: 
Size: 571622 Color: 18
Size: 428372 Color: 8

Bin 950: 7 of cap free
Amount of items: 2
Items: 
Size: 597715 Color: 3
Size: 402279 Color: 2

Bin 951: 7 of cap free
Amount of items: 2
Items: 
Size: 603686 Color: 12
Size: 396308 Color: 14

Bin 952: 7 of cap free
Amount of items: 2
Items: 
Size: 610059 Color: 5
Size: 389935 Color: 12

Bin 953: 7 of cap free
Amount of items: 2
Items: 
Size: 613149 Color: 1
Size: 386845 Color: 5

Bin 954: 7 of cap free
Amount of items: 2
Items: 
Size: 621572 Color: 8
Size: 378422 Color: 1

Bin 955: 7 of cap free
Amount of items: 2
Items: 
Size: 625597 Color: 1
Size: 374397 Color: 13

Bin 956: 7 of cap free
Amount of items: 2
Items: 
Size: 635564 Color: 14
Size: 364430 Color: 5

Bin 957: 7 of cap free
Amount of items: 3
Items: 
Size: 650836 Color: 9
Size: 174637 Color: 14
Size: 174521 Color: 12

Bin 958: 7 of cap free
Amount of items: 2
Items: 
Size: 665255 Color: 15
Size: 334739 Color: 10

Bin 959: 7 of cap free
Amount of items: 2
Items: 
Size: 668807 Color: 1
Size: 331187 Color: 18

Bin 960: 7 of cap free
Amount of items: 2
Items: 
Size: 671926 Color: 16
Size: 328068 Color: 2

Bin 961: 7 of cap free
Amount of items: 2
Items: 
Size: 678810 Color: 8
Size: 321184 Color: 17

Bin 962: 7 of cap free
Amount of items: 2
Items: 
Size: 686721 Color: 10
Size: 313273 Color: 7

Bin 963: 7 of cap free
Amount of items: 2
Items: 
Size: 713361 Color: 17
Size: 286633 Color: 9

Bin 964: 7 of cap free
Amount of items: 3
Items: 
Size: 716112 Color: 12
Size: 142801 Color: 11
Size: 141081 Color: 7

Bin 965: 7 of cap free
Amount of items: 2
Items: 
Size: 719945 Color: 4
Size: 280049 Color: 6

Bin 966: 7 of cap free
Amount of items: 2
Items: 
Size: 722460 Color: 10
Size: 277534 Color: 9

Bin 967: 7 of cap free
Amount of items: 2
Items: 
Size: 724185 Color: 15
Size: 275809 Color: 0

Bin 968: 7 of cap free
Amount of items: 2
Items: 
Size: 739276 Color: 16
Size: 260718 Color: 9

Bin 969: 7 of cap free
Amount of items: 2
Items: 
Size: 763786 Color: 9
Size: 236208 Color: 2

Bin 970: 8 of cap free
Amount of items: 3
Items: 
Size: 704023 Color: 0
Size: 155127 Color: 12
Size: 140843 Color: 0

Bin 971: 8 of cap free
Amount of items: 3
Items: 
Size: 768590 Color: 2
Size: 123329 Color: 13
Size: 108074 Color: 7

Bin 972: 8 of cap free
Amount of items: 3
Items: 
Size: 712490 Color: 11
Size: 143773 Color: 18
Size: 143730 Color: 9

Bin 973: 8 of cap free
Amount of items: 2
Items: 
Size: 528534 Color: 13
Size: 471459 Color: 9

Bin 974: 8 of cap free
Amount of items: 3
Items: 
Size: 673493 Color: 14
Size: 171705 Color: 13
Size: 154795 Color: 5

Bin 975: 8 of cap free
Amount of items: 3
Items: 
Size: 532128 Color: 10
Size: 313090 Color: 0
Size: 154775 Color: 8

Bin 976: 8 of cap free
Amount of items: 3
Items: 
Size: 695366 Color: 2
Size: 153011 Color: 11
Size: 151616 Color: 5

Bin 977: 8 of cap free
Amount of items: 3
Items: 
Size: 637057 Color: 16
Size: 181620 Color: 3
Size: 181316 Color: 13

Bin 978: 8 of cap free
Amount of items: 3
Items: 
Size: 680360 Color: 5
Size: 160295 Color: 9
Size: 159338 Color: 1

Bin 979: 8 of cap free
Amount of items: 3
Items: 
Size: 607153 Color: 16
Size: 196510 Color: 4
Size: 196330 Color: 0

Bin 980: 8 of cap free
Amount of items: 2
Items: 
Size: 728010 Color: 10
Size: 271983 Color: 12

Bin 981: 8 of cap free
Amount of items: 2
Items: 
Size: 778671 Color: 1
Size: 221322 Color: 3

Bin 982: 8 of cap free
Amount of items: 3
Items: 
Size: 387321 Color: 19
Size: 319677 Color: 14
Size: 292995 Color: 3

Bin 983: 8 of cap free
Amount of items: 3
Items: 
Size: 612187 Color: 18
Size: 193904 Color: 14
Size: 193902 Color: 15

Bin 984: 8 of cap free
Amount of items: 2
Items: 
Size: 750183 Color: 6
Size: 249810 Color: 4

Bin 985: 8 of cap free
Amount of items: 3
Items: 
Size: 594003 Color: 0
Size: 208124 Color: 0
Size: 197866 Color: 7

Bin 986: 8 of cap free
Amount of items: 3
Items: 
Size: 665898 Color: 8
Size: 167276 Color: 14
Size: 166819 Color: 3

Bin 987: 8 of cap free
Amount of items: 3
Items: 
Size: 663099 Color: 15
Size: 168572 Color: 17
Size: 168322 Color: 1

Bin 988: 8 of cap free
Amount of items: 3
Items: 
Size: 658898 Color: 7
Size: 170613 Color: 12
Size: 170482 Color: 6

Bin 989: 8 of cap free
Amount of items: 3
Items: 
Size: 588580 Color: 4
Size: 212361 Color: 2
Size: 199052 Color: 1

Bin 990: 8 of cap free
Amount of items: 3
Items: 
Size: 684883 Color: 15
Size: 157955 Color: 4
Size: 157155 Color: 5

Bin 991: 8 of cap free
Amount of items: 3
Items: 
Size: 491227 Color: 15
Size: 357182 Color: 9
Size: 151584 Color: 11

Bin 992: 8 of cap free
Amount of items: 2
Items: 
Size: 581116 Color: 6
Size: 418877 Color: 3

Bin 993: 8 of cap free
Amount of items: 2
Items: 
Size: 543080 Color: 16
Size: 456913 Color: 17

Bin 994: 8 of cap free
Amount of items: 2
Items: 
Size: 514693 Color: 12
Size: 485300 Color: 1

Bin 995: 8 of cap free
Amount of items: 2
Items: 
Size: 712715 Color: 16
Size: 287278 Color: 7

Bin 996: 8 of cap free
Amount of items: 3
Items: 
Size: 372966 Color: 5
Size: 341595 Color: 1
Size: 285432 Color: 0

Bin 997: 8 of cap free
Amount of items: 3
Items: 
Size: 373506 Color: 13
Size: 333139 Color: 14
Size: 293348 Color: 16

Bin 998: 8 of cap free
Amount of items: 2
Items: 
Size: 506587 Color: 11
Size: 493406 Color: 9

Bin 999: 8 of cap free
Amount of items: 2
Items: 
Size: 506618 Color: 3
Size: 493375 Color: 10

Bin 1000: 8 of cap free
Amount of items: 2
Items: 
Size: 536842 Color: 17
Size: 463151 Color: 6

Bin 1001: 8 of cap free
Amount of items: 2
Items: 
Size: 545984 Color: 16
Size: 454009 Color: 12

Bin 1002: 8 of cap free
Amount of items: 2
Items: 
Size: 550577 Color: 8
Size: 449416 Color: 19

Bin 1003: 8 of cap free
Amount of items: 2
Items: 
Size: 554289 Color: 18
Size: 445704 Color: 4

Bin 1004: 8 of cap free
Amount of items: 2
Items: 
Size: 555640 Color: 8
Size: 444353 Color: 7

Bin 1005: 8 of cap free
Amount of items: 2
Items: 
Size: 556252 Color: 0
Size: 443741 Color: 18

Bin 1006: 8 of cap free
Amount of items: 2
Items: 
Size: 571405 Color: 14
Size: 428588 Color: 9

Bin 1007: 8 of cap free
Amount of items: 2
Items: 
Size: 571955 Color: 11
Size: 428038 Color: 1

Bin 1008: 8 of cap free
Amount of items: 2
Items: 
Size: 581980 Color: 16
Size: 418013 Color: 3

Bin 1009: 8 of cap free
Amount of items: 2
Items: 
Size: 584184 Color: 13
Size: 415809 Color: 2

Bin 1010: 8 of cap free
Amount of items: 2
Items: 
Size: 586531 Color: 15
Size: 413462 Color: 19

Bin 1011: 8 of cap free
Amount of items: 2
Items: 
Size: 595881 Color: 6
Size: 404112 Color: 2

Bin 1012: 8 of cap free
Amount of items: 3
Items: 
Size: 604512 Color: 1
Size: 198027 Color: 13
Size: 197454 Color: 19

Bin 1013: 8 of cap free
Amount of items: 2
Items: 
Size: 610141 Color: 6
Size: 389852 Color: 18

Bin 1014: 8 of cap free
Amount of items: 2
Items: 
Size: 614880 Color: 0
Size: 385113 Color: 3

Bin 1015: 8 of cap free
Amount of items: 2
Items: 
Size: 626197 Color: 2
Size: 373796 Color: 13

Bin 1016: 8 of cap free
Amount of items: 3
Items: 
Size: 626524 Color: 15
Size: 187032 Color: 9
Size: 186437 Color: 2

Bin 1017: 8 of cap free
Amount of items: 2
Items: 
Size: 634790 Color: 6
Size: 365203 Color: 5

Bin 1018: 8 of cap free
Amount of items: 2
Items: 
Size: 637532 Color: 2
Size: 362461 Color: 10

Bin 1019: 8 of cap free
Amount of items: 2
Items: 
Size: 638472 Color: 17
Size: 361521 Color: 16

Bin 1020: 8 of cap free
Amount of items: 2
Items: 
Size: 643853 Color: 12
Size: 356140 Color: 9

Bin 1021: 8 of cap free
Amount of items: 3
Items: 
Size: 644012 Color: 14
Size: 178042 Color: 19
Size: 177939 Color: 10

Bin 1022: 8 of cap free
Amount of items: 2
Items: 
Size: 665195 Color: 12
Size: 334798 Color: 19

Bin 1023: 8 of cap free
Amount of items: 2
Items: 
Size: 677502 Color: 6
Size: 322491 Color: 5

Bin 1024: 8 of cap free
Amount of items: 2
Items: 
Size: 681336 Color: 12
Size: 318657 Color: 3

Bin 1025: 8 of cap free
Amount of items: 2
Items: 
Size: 681807 Color: 18
Size: 318186 Color: 6

Bin 1026: 8 of cap free
Amount of items: 2
Items: 
Size: 690569 Color: 19
Size: 309424 Color: 2

Bin 1027: 8 of cap free
Amount of items: 3
Items: 
Size: 695619 Color: 18
Size: 152305 Color: 10
Size: 152069 Color: 7

Bin 1028: 8 of cap free
Amount of items: 2
Items: 
Size: 695799 Color: 5
Size: 304194 Color: 13

Bin 1029: 8 of cap free
Amount of items: 2
Items: 
Size: 698156 Color: 0
Size: 301837 Color: 11

Bin 1030: 8 of cap free
Amount of items: 2
Items: 
Size: 708559 Color: 8
Size: 291434 Color: 1

Bin 1031: 8 of cap free
Amount of items: 2
Items: 
Size: 725542 Color: 9
Size: 274451 Color: 5

Bin 1032: 8 of cap free
Amount of items: 2
Items: 
Size: 737146 Color: 8
Size: 262847 Color: 6

Bin 1033: 8 of cap free
Amount of items: 2
Items: 
Size: 745115 Color: 7
Size: 254878 Color: 19

Bin 1034: 8 of cap free
Amount of items: 2
Items: 
Size: 753798 Color: 18
Size: 246195 Color: 0

Bin 1035: 9 of cap free
Amount of items: 2
Items: 
Size: 796159 Color: 18
Size: 203833 Color: 19

Bin 1036: 9 of cap free
Amount of items: 3
Items: 
Size: 651689 Color: 12
Size: 177428 Color: 18
Size: 170875 Color: 2

Bin 1037: 9 of cap free
Amount of items: 3
Items: 
Size: 788132 Color: 4
Size: 107919 Color: 15
Size: 103941 Color: 15

Bin 1038: 9 of cap free
Amount of items: 3
Items: 
Size: 749637 Color: 1
Size: 125397 Color: 18
Size: 124958 Color: 4

Bin 1039: 9 of cap free
Amount of items: 2
Items: 
Size: 715542 Color: 13
Size: 284450 Color: 16

Bin 1040: 9 of cap free
Amount of items: 2
Items: 
Size: 717037 Color: 3
Size: 282955 Color: 2

Bin 1041: 9 of cap free
Amount of items: 3
Items: 
Size: 684454 Color: 15
Size: 157971 Color: 4
Size: 157567 Color: 2

Bin 1042: 9 of cap free
Amount of items: 3
Items: 
Size: 373904 Color: 19
Size: 341950 Color: 17
Size: 284138 Color: 8

Bin 1043: 9 of cap free
Amount of items: 3
Items: 
Size: 731447 Color: 3
Size: 135628 Color: 16
Size: 132917 Color: 4

Bin 1044: 9 of cap free
Amount of items: 2
Items: 
Size: 738178 Color: 3
Size: 261814 Color: 16

Bin 1045: 9 of cap free
Amount of items: 3
Items: 
Size: 695662 Color: 17
Size: 152316 Color: 13
Size: 152014 Color: 0

Bin 1046: 9 of cap free
Amount of items: 3
Items: 
Size: 595018 Color: 0
Size: 207468 Color: 9
Size: 197506 Color: 11

Bin 1047: 9 of cap free
Amount of items: 2
Items: 
Size: 673733 Color: 19
Size: 326259 Color: 9

Bin 1048: 9 of cap free
Amount of items: 3
Items: 
Size: 543025 Color: 1
Size: 231372 Color: 0
Size: 225595 Color: 6

Bin 1049: 9 of cap free
Amount of items: 2
Items: 
Size: 673424 Color: 4
Size: 326568 Color: 6

Bin 1050: 9 of cap free
Amount of items: 3
Items: 
Size: 660147 Color: 11
Size: 170284 Color: 16
Size: 169561 Color: 12

Bin 1051: 9 of cap free
Amount of items: 2
Items: 
Size: 595399 Color: 7
Size: 404593 Color: 11

Bin 1052: 9 of cap free
Amount of items: 2
Items: 
Size: 520354 Color: 8
Size: 479638 Color: 9

Bin 1053: 9 of cap free
Amount of items: 2
Items: 
Size: 640796 Color: 16
Size: 359196 Color: 9

Bin 1054: 9 of cap free
Amount of items: 3
Items: 
Size: 442873 Color: 3
Size: 281825 Color: 4
Size: 275294 Color: 16

Bin 1055: 9 of cap free
Amount of items: 2
Items: 
Size: 747605 Color: 2
Size: 252387 Color: 13

Bin 1056: 9 of cap free
Amount of items: 2
Items: 
Size: 744883 Color: 16
Size: 255109 Color: 3

Bin 1057: 9 of cap free
Amount of items: 3
Items: 
Size: 355533 Color: 16
Size: 324278 Color: 9
Size: 320181 Color: 13

Bin 1058: 9 of cap free
Amount of items: 3
Items: 
Size: 361067 Color: 18
Size: 330593 Color: 8
Size: 308332 Color: 3

Bin 1059: 9 of cap free
Amount of items: 3
Items: 
Size: 382845 Color: 4
Size: 373457 Color: 4
Size: 243690 Color: 5

Bin 1060: 9 of cap free
Amount of items: 2
Items: 
Size: 508135 Color: 3
Size: 491857 Color: 14

Bin 1061: 9 of cap free
Amount of items: 2
Items: 
Size: 510189 Color: 18
Size: 489803 Color: 19

Bin 1062: 9 of cap free
Amount of items: 2
Items: 
Size: 512685 Color: 7
Size: 487307 Color: 17

Bin 1063: 9 of cap free
Amount of items: 2
Items: 
Size: 517642 Color: 14
Size: 482350 Color: 6

Bin 1064: 9 of cap free
Amount of items: 2
Items: 
Size: 532576 Color: 8
Size: 467416 Color: 10

Bin 1065: 9 of cap free
Amount of items: 2
Items: 
Size: 534292 Color: 5
Size: 465700 Color: 8

Bin 1066: 9 of cap free
Amount of items: 2
Items: 
Size: 538741 Color: 2
Size: 461251 Color: 19

Bin 1067: 9 of cap free
Amount of items: 2
Items: 
Size: 539400 Color: 9
Size: 460592 Color: 0

Bin 1068: 9 of cap free
Amount of items: 2
Items: 
Size: 549570 Color: 3
Size: 450422 Color: 19

Bin 1069: 9 of cap free
Amount of items: 2
Items: 
Size: 550144 Color: 9
Size: 449848 Color: 10

Bin 1070: 9 of cap free
Amount of items: 2
Items: 
Size: 550526 Color: 18
Size: 449466 Color: 0

Bin 1071: 9 of cap free
Amount of items: 2
Items: 
Size: 560103 Color: 2
Size: 439889 Color: 7

Bin 1072: 9 of cap free
Amount of items: 2
Items: 
Size: 582246 Color: 12
Size: 417746 Color: 16

Bin 1073: 9 of cap free
Amount of items: 2
Items: 
Size: 588847 Color: 5
Size: 411145 Color: 18

Bin 1074: 9 of cap free
Amount of items: 2
Items: 
Size: 590264 Color: 12
Size: 409728 Color: 1

Bin 1075: 9 of cap free
Amount of items: 2
Items: 
Size: 592315 Color: 8
Size: 407677 Color: 0

Bin 1076: 9 of cap free
Amount of items: 2
Items: 
Size: 601517 Color: 18
Size: 398475 Color: 6

Bin 1077: 9 of cap free
Amount of items: 2
Items: 
Size: 605621 Color: 17
Size: 394371 Color: 16

Bin 1078: 9 of cap free
Amount of items: 2
Items: 
Size: 606707 Color: 1
Size: 393285 Color: 16

Bin 1079: 9 of cap free
Amount of items: 2
Items: 
Size: 613936 Color: 7
Size: 386056 Color: 0

Bin 1080: 9 of cap free
Amount of items: 2
Items: 
Size: 621485 Color: 5
Size: 378507 Color: 12

Bin 1081: 9 of cap free
Amount of items: 3
Items: 
Size: 624166 Color: 1
Size: 189437 Color: 16
Size: 186389 Color: 16

Bin 1082: 9 of cap free
Amount of items: 2
Items: 
Size: 628277 Color: 16
Size: 371715 Color: 13

Bin 1083: 9 of cap free
Amount of items: 2
Items: 
Size: 638665 Color: 13
Size: 361327 Color: 18

Bin 1084: 9 of cap free
Amount of items: 2
Items: 
Size: 640061 Color: 0
Size: 359931 Color: 3

Bin 1085: 9 of cap free
Amount of items: 2
Items: 
Size: 640170 Color: 11
Size: 359822 Color: 12

Bin 1086: 9 of cap free
Amount of items: 2
Items: 
Size: 646042 Color: 0
Size: 353950 Color: 2

Bin 1087: 9 of cap free
Amount of items: 2
Items: 
Size: 651044 Color: 0
Size: 348948 Color: 13

Bin 1088: 9 of cap free
Amount of items: 2
Items: 
Size: 676978 Color: 13
Size: 323014 Color: 4

Bin 1089: 9 of cap free
Amount of items: 2
Items: 
Size: 690048 Color: 15
Size: 309944 Color: 6

Bin 1090: 9 of cap free
Amount of items: 2
Items: 
Size: 701862 Color: 8
Size: 298130 Color: 10

Bin 1091: 9 of cap free
Amount of items: 2
Items: 
Size: 718583 Color: 11
Size: 281409 Color: 18

Bin 1092: 9 of cap free
Amount of items: 2
Items: 
Size: 742634 Color: 11
Size: 257358 Color: 15

Bin 1093: 9 of cap free
Amount of items: 2
Items: 
Size: 748205 Color: 2
Size: 251787 Color: 18

Bin 1094: 9 of cap free
Amount of items: 2
Items: 
Size: 764332 Color: 18
Size: 235660 Color: 7

Bin 1095: 9 of cap free
Amount of items: 2
Items: 
Size: 772622 Color: 3
Size: 227370 Color: 5

Bin 1096: 9 of cap free
Amount of items: 2
Items: 
Size: 777527 Color: 8
Size: 222465 Color: 12

Bin 1097: 9 of cap free
Amount of items: 2
Items: 
Size: 782356 Color: 3
Size: 217636 Color: 4

Bin 1098: 9 of cap free
Amount of items: 2
Items: 
Size: 788352 Color: 18
Size: 211640 Color: 1

Bin 1099: 10 of cap free
Amount of items: 3
Items: 
Size: 743578 Color: 4
Size: 153667 Color: 18
Size: 102746 Color: 8

Bin 1100: 10 of cap free
Amount of items: 3
Items: 
Size: 689815 Color: 13
Size: 155581 Color: 18
Size: 154595 Color: 16

Bin 1101: 10 of cap free
Amount of items: 3
Items: 
Size: 697833 Color: 2
Size: 153073 Color: 7
Size: 149085 Color: 2

Bin 1102: 10 of cap free
Amount of items: 3
Items: 
Size: 685818 Color: 15
Size: 157113 Color: 11
Size: 157060 Color: 16

Bin 1103: 10 of cap free
Amount of items: 3
Items: 
Size: 593364 Color: 10
Size: 234627 Color: 17
Size: 172000 Color: 16

Bin 1104: 10 of cap free
Amount of items: 3
Items: 
Size: 723074 Color: 6
Size: 138531 Color: 4
Size: 138386 Color: 7

Bin 1105: 10 of cap free
Amount of items: 3
Items: 
Size: 568237 Color: 4
Size: 243418 Color: 17
Size: 188336 Color: 12

Bin 1106: 10 of cap free
Amount of items: 3
Items: 
Size: 589084 Color: 18
Size: 211856 Color: 8
Size: 199051 Color: 15

Bin 1107: 10 of cap free
Amount of items: 3
Items: 
Size: 664333 Color: 15
Size: 167988 Color: 13
Size: 167670 Color: 6

Bin 1108: 10 of cap free
Amount of items: 2
Items: 
Size: 754479 Color: 0
Size: 245512 Color: 16

Bin 1109: 10 of cap free
Amount of items: 3
Items: 
Size: 363037 Color: 8
Size: 330465 Color: 1
Size: 306489 Color: 16

Bin 1110: 10 of cap free
Amount of items: 2
Items: 
Size: 655008 Color: 17
Size: 344983 Color: 6

Bin 1111: 10 of cap free
Amount of items: 2
Items: 
Size: 710966 Color: 9
Size: 289025 Color: 16

Bin 1112: 10 of cap free
Amount of items: 2
Items: 
Size: 770244 Color: 16
Size: 229747 Color: 2

Bin 1113: 10 of cap free
Amount of items: 3
Items: 
Size: 359061 Color: 14
Size: 342118 Color: 2
Size: 298812 Color: 17

Bin 1114: 10 of cap free
Amount of items: 2
Items: 
Size: 503658 Color: 7
Size: 496333 Color: 18

Bin 1115: 10 of cap free
Amount of items: 2
Items: 
Size: 637093 Color: 18
Size: 362898 Color: 7

Bin 1116: 10 of cap free
Amount of items: 3
Items: 
Size: 338411 Color: 1
Size: 330925 Color: 19
Size: 330655 Color: 3

Bin 1117: 10 of cap free
Amount of items: 2
Items: 
Size: 511324 Color: 13
Size: 488667 Color: 17

Bin 1118: 10 of cap free
Amount of items: 2
Items: 
Size: 544763 Color: 19
Size: 455228 Color: 17

Bin 1119: 10 of cap free
Amount of items: 2
Items: 
Size: 553299 Color: 6
Size: 446692 Color: 19

Bin 1120: 10 of cap free
Amount of items: 2
Items: 
Size: 567688 Color: 18
Size: 432303 Color: 19

Bin 1121: 10 of cap free
Amount of items: 2
Items: 
Size: 569586 Color: 0
Size: 430405 Color: 1

Bin 1122: 10 of cap free
Amount of items: 2
Items: 
Size: 572026 Color: 9
Size: 427965 Color: 19

Bin 1123: 10 of cap free
Amount of items: 2
Items: 
Size: 573108 Color: 16
Size: 426883 Color: 8

Bin 1124: 10 of cap free
Amount of items: 2
Items: 
Size: 578768 Color: 16
Size: 421223 Color: 7

Bin 1125: 10 of cap free
Amount of items: 2
Items: 
Size: 581432 Color: 18
Size: 418559 Color: 7

Bin 1126: 10 of cap free
Amount of items: 2
Items: 
Size: 586975 Color: 8
Size: 413016 Color: 16

Bin 1127: 10 of cap free
Amount of items: 2
Items: 
Size: 594095 Color: 7
Size: 405896 Color: 11

Bin 1128: 10 of cap free
Amount of items: 3
Items: 
Size: 596143 Color: 9
Size: 202181 Color: 4
Size: 201667 Color: 1

Bin 1129: 10 of cap free
Amount of items: 2
Items: 
Size: 602259 Color: 18
Size: 397732 Color: 6

Bin 1130: 10 of cap free
Amount of items: 2
Items: 
Size: 607251 Color: 7
Size: 392740 Color: 12

Bin 1131: 10 of cap free
Amount of items: 2
Items: 
Size: 610641 Color: 0
Size: 389350 Color: 10

Bin 1132: 10 of cap free
Amount of items: 2
Items: 
Size: 613094 Color: 0
Size: 386897 Color: 15

Bin 1133: 10 of cap free
Amount of items: 2
Items: 
Size: 614054 Color: 5
Size: 385937 Color: 11

Bin 1134: 10 of cap free
Amount of items: 2
Items: 
Size: 617784 Color: 14
Size: 382207 Color: 5

Bin 1135: 10 of cap free
Amount of items: 2
Items: 
Size: 644304 Color: 12
Size: 355687 Color: 0

Bin 1136: 10 of cap free
Amount of items: 2
Items: 
Size: 645654 Color: 9
Size: 354337 Color: 4

Bin 1137: 10 of cap free
Amount of items: 2
Items: 
Size: 654027 Color: 10
Size: 345964 Color: 17

Bin 1138: 10 of cap free
Amount of items: 2
Items: 
Size: 656392 Color: 5
Size: 343599 Color: 17

Bin 1139: 10 of cap free
Amount of items: 2
Items: 
Size: 673571 Color: 10
Size: 326420 Color: 17

Bin 1140: 10 of cap free
Amount of items: 2
Items: 
Size: 713028 Color: 13
Size: 286963 Color: 10

Bin 1141: 10 of cap free
Amount of items: 2
Items: 
Size: 739821 Color: 18
Size: 260170 Color: 8

Bin 1142: 10 of cap free
Amount of items: 2
Items: 
Size: 746657 Color: 2
Size: 253334 Color: 17

Bin 1143: 10 of cap free
Amount of items: 2
Items: 
Size: 763966 Color: 15
Size: 236025 Color: 4

Bin 1144: 10 of cap free
Amount of items: 2
Items: 
Size: 773717 Color: 19
Size: 226274 Color: 18

Bin 1145: 10 of cap free
Amount of items: 2
Items: 
Size: 793273 Color: 12
Size: 206718 Color: 15

Bin 1146: 10 of cap free
Amount of items: 2
Items: 
Size: 795026 Color: 1
Size: 204965 Color: 15

Bin 1147: 10 of cap free
Amount of items: 2
Items: 
Size: 798655 Color: 8
Size: 201336 Color: 7

Bin 1148: 10 of cap free
Amount of items: 2
Items: 
Size: 799673 Color: 1
Size: 200318 Color: 14

Bin 1149: 11 of cap free
Amount of items: 3
Items: 
Size: 693861 Color: 15
Size: 185252 Color: 6
Size: 120877 Color: 9

Bin 1150: 11 of cap free
Amount of items: 2
Items: 
Size: 721331 Color: 12
Size: 278659 Color: 19

Bin 1151: 11 of cap free
Amount of items: 3
Items: 
Size: 377223 Color: 7
Size: 315965 Color: 3
Size: 306802 Color: 9

Bin 1152: 11 of cap free
Amount of items: 2
Items: 
Size: 640729 Color: 13
Size: 359261 Color: 4

Bin 1153: 11 of cap free
Amount of items: 2
Items: 
Size: 758866 Color: 1
Size: 241124 Color: 15

Bin 1154: 11 of cap free
Amount of items: 3
Items: 
Size: 352794 Color: 15
Size: 352173 Color: 19
Size: 295023 Color: 15

Bin 1155: 11 of cap free
Amount of items: 2
Items: 
Size: 616630 Color: 3
Size: 383360 Color: 16

Bin 1156: 11 of cap free
Amount of items: 2
Items: 
Size: 782450 Color: 2
Size: 217540 Color: 8

Bin 1157: 11 of cap free
Amount of items: 3
Items: 
Size: 354733 Color: 19
Size: 350658 Color: 5
Size: 294599 Color: 9

Bin 1158: 11 of cap free
Amount of items: 2
Items: 
Size: 719750 Color: 17
Size: 280240 Color: 8

Bin 1159: 11 of cap free
Amount of items: 2
Items: 
Size: 756647 Color: 0
Size: 243343 Color: 3

Bin 1160: 11 of cap free
Amount of items: 2
Items: 
Size: 745492 Color: 7
Size: 254498 Color: 18

Bin 1161: 11 of cap free
Amount of items: 2
Items: 
Size: 786480 Color: 14
Size: 213510 Color: 6

Bin 1162: 11 of cap free
Amount of items: 2
Items: 
Size: 791711 Color: 17
Size: 208279 Color: 11

Bin 1163: 11 of cap free
Amount of items: 2
Items: 
Size: 649867 Color: 16
Size: 350123 Color: 11

Bin 1164: 11 of cap free
Amount of items: 3
Items: 
Size: 387297 Color: 9
Size: 328200 Color: 19
Size: 284493 Color: 14

Bin 1165: 11 of cap free
Amount of items: 2
Items: 
Size: 747080 Color: 10
Size: 252910 Color: 7

Bin 1166: 11 of cap free
Amount of items: 3
Items: 
Size: 520923 Color: 16
Size: 255418 Color: 9
Size: 223649 Color: 1

Bin 1167: 11 of cap free
Amount of items: 2
Items: 
Size: 781940 Color: 19
Size: 218050 Color: 4

Bin 1168: 11 of cap free
Amount of items: 3
Items: 
Size: 437350 Color: 7
Size: 291753 Color: 11
Size: 270887 Color: 8

Bin 1169: 11 of cap free
Amount of items: 3
Items: 
Size: 498805 Color: 2
Size: 251435 Color: 12
Size: 249750 Color: 17

Bin 1170: 11 of cap free
Amount of items: 2
Items: 
Size: 666794 Color: 15
Size: 333196 Color: 9

Bin 1171: 11 of cap free
Amount of items: 3
Items: 
Size: 385645 Color: 3
Size: 316095 Color: 6
Size: 298250 Color: 2

Bin 1172: 11 of cap free
Amount of items: 3
Items: 
Size: 378059 Color: 0
Size: 339462 Color: 2
Size: 282469 Color: 15

Bin 1173: 11 of cap free
Amount of items: 3
Items: 
Size: 382798 Color: 0
Size: 319585 Color: 19
Size: 297607 Color: 13

Bin 1174: 11 of cap free
Amount of items: 2
Items: 
Size: 512788 Color: 9
Size: 487202 Color: 18

Bin 1175: 11 of cap free
Amount of items: 2
Items: 
Size: 545749 Color: 9
Size: 454241 Color: 4

Bin 1176: 11 of cap free
Amount of items: 2
Items: 
Size: 550266 Color: 9
Size: 449724 Color: 11

Bin 1177: 11 of cap free
Amount of items: 2
Items: 
Size: 557264 Color: 14
Size: 442726 Color: 16

Bin 1178: 11 of cap free
Amount of items: 2
Items: 
Size: 561200 Color: 18
Size: 438790 Color: 2

Bin 1179: 11 of cap free
Amount of items: 2
Items: 
Size: 579397 Color: 14
Size: 420593 Color: 9

Bin 1180: 11 of cap free
Amount of items: 2
Items: 
Size: 585586 Color: 6
Size: 414404 Color: 0

Bin 1181: 11 of cap free
Amount of items: 3
Items: 
Size: 596028 Color: 11
Size: 202116 Color: 19
Size: 201846 Color: 8

Bin 1182: 11 of cap free
Amount of items: 2
Items: 
Size: 599396 Color: 10
Size: 400594 Color: 11

Bin 1183: 11 of cap free
Amount of items: 2
Items: 
Size: 598341 Color: 3
Size: 401649 Color: 9

Bin 1184: 11 of cap free
Amount of items: 2
Items: 
Size: 602737 Color: 13
Size: 397253 Color: 15

Bin 1185: 11 of cap free
Amount of items: 2
Items: 
Size: 610542 Color: 13
Size: 389448 Color: 17

Bin 1186: 11 of cap free
Amount of items: 2
Items: 
Size: 619924 Color: 7
Size: 380066 Color: 5

Bin 1187: 11 of cap free
Amount of items: 2
Items: 
Size: 620094 Color: 2
Size: 379896 Color: 16

Bin 1188: 11 of cap free
Amount of items: 3
Items: 
Size: 626255 Color: 19
Size: 187497 Color: 7
Size: 186238 Color: 5

Bin 1189: 11 of cap free
Amount of items: 2
Items: 
Size: 651549 Color: 4
Size: 348441 Color: 17

Bin 1190: 11 of cap free
Amount of items: 2
Items: 
Size: 654106 Color: 6
Size: 345884 Color: 13

Bin 1191: 11 of cap free
Amount of items: 3
Items: 
Size: 657114 Color: 8
Size: 171801 Color: 14
Size: 171075 Color: 18

Bin 1192: 11 of cap free
Amount of items: 2
Items: 
Size: 664400 Color: 4
Size: 335590 Color: 5

Bin 1193: 11 of cap free
Amount of items: 3
Items: 
Size: 667882 Color: 15
Size: 166560 Color: 17
Size: 165548 Color: 6

Bin 1194: 11 of cap free
Amount of items: 2
Items: 
Size: 668006 Color: 4
Size: 331984 Color: 6

Bin 1195: 11 of cap free
Amount of items: 2
Items: 
Size: 699119 Color: 2
Size: 300871 Color: 18

Bin 1196: 11 of cap free
Amount of items: 2
Items: 
Size: 715324 Color: 8
Size: 284666 Color: 17

Bin 1197: 11 of cap free
Amount of items: 2
Items: 
Size: 749224 Color: 19
Size: 250766 Color: 1

Bin 1198: 11 of cap free
Amount of items: 2
Items: 
Size: 783564 Color: 4
Size: 216426 Color: 19

Bin 1199: 11 of cap free
Amount of items: 2
Items: 
Size: 799177 Color: 4
Size: 200813 Color: 18

Bin 1200: 12 of cap free
Amount of items: 3
Items: 
Size: 640645 Color: 8
Size: 179980 Color: 5
Size: 179364 Color: 6

Bin 1201: 12 of cap free
Amount of items: 3
Items: 
Size: 703227 Color: 8
Size: 148582 Color: 12
Size: 148180 Color: 2

Bin 1202: 12 of cap free
Amount of items: 3
Items: 
Size: 745616 Color: 15
Size: 129917 Color: 17
Size: 124456 Color: 17

Bin 1203: 12 of cap free
Amount of items: 2
Items: 
Size: 662164 Color: 17
Size: 337825 Color: 19

Bin 1204: 12 of cap free
Amount of items: 2
Items: 
Size: 757599 Color: 13
Size: 242390 Color: 10

Bin 1205: 12 of cap free
Amount of items: 3
Items: 
Size: 663656 Color: 8
Size: 168201 Color: 0
Size: 168132 Color: 18

Bin 1206: 12 of cap free
Amount of items: 3
Items: 
Size: 710564 Color: 1
Size: 144859 Color: 19
Size: 144566 Color: 11

Bin 1207: 12 of cap free
Amount of items: 2
Items: 
Size: 698962 Color: 3
Size: 301027 Color: 4

Bin 1208: 12 of cap free
Amount of items: 3
Items: 
Size: 634019 Color: 1
Size: 183007 Color: 5
Size: 182963 Color: 9

Bin 1209: 12 of cap free
Amount of items: 2
Items: 
Size: 612562 Color: 12
Size: 387427 Color: 8

Bin 1210: 12 of cap free
Amount of items: 2
Items: 
Size: 730764 Color: 18
Size: 269225 Color: 17

Bin 1211: 12 of cap free
Amount of items: 3
Items: 
Size: 378161 Color: 18
Size: 339359 Color: 19
Size: 282469 Color: 7

Bin 1212: 12 of cap free
Amount of items: 3
Items: 
Size: 527022 Color: 15
Size: 251553 Color: 14
Size: 221414 Color: 19

Bin 1213: 12 of cap free
Amount of items: 2
Items: 
Size: 697503 Color: 8
Size: 302486 Color: 1

Bin 1214: 12 of cap free
Amount of items: 2
Items: 
Size: 742795 Color: 10
Size: 257194 Color: 16

Bin 1215: 12 of cap free
Amount of items: 2
Items: 
Size: 529892 Color: 16
Size: 470097 Color: 12

Bin 1216: 12 of cap free
Amount of items: 2
Items: 
Size: 746114 Color: 19
Size: 253875 Color: 8

Bin 1217: 12 of cap free
Amount of items: 2
Items: 
Size: 500607 Color: 13
Size: 499382 Color: 8

Bin 1218: 12 of cap free
Amount of items: 2
Items: 
Size: 500894 Color: 19
Size: 499095 Color: 11

Bin 1219: 12 of cap free
Amount of items: 2
Items: 
Size: 505076 Color: 18
Size: 494913 Color: 8

Bin 1220: 12 of cap free
Amount of items: 2
Items: 
Size: 511618 Color: 2
Size: 488371 Color: 15

Bin 1221: 12 of cap free
Amount of items: 2
Items: 
Size: 540833 Color: 11
Size: 459156 Color: 13

Bin 1222: 12 of cap free
Amount of items: 2
Items: 
Size: 543710 Color: 8
Size: 456279 Color: 10

Bin 1223: 12 of cap free
Amount of items: 2
Items: 
Size: 547995 Color: 17
Size: 451994 Color: 2

Bin 1224: 12 of cap free
Amount of items: 2
Items: 
Size: 552718 Color: 16
Size: 447271 Color: 7

Bin 1225: 12 of cap free
Amount of items: 2
Items: 
Size: 557164 Color: 9
Size: 442825 Color: 2

Bin 1226: 12 of cap free
Amount of items: 2
Items: 
Size: 558153 Color: 10
Size: 441836 Color: 9

Bin 1227: 12 of cap free
Amount of items: 2
Items: 
Size: 577615 Color: 2
Size: 422374 Color: 1

Bin 1228: 12 of cap free
Amount of items: 2
Items: 
Size: 581691 Color: 4
Size: 418298 Color: 15

Bin 1229: 12 of cap free
Amount of items: 2
Items: 
Size: 587450 Color: 15
Size: 412539 Color: 19

Bin 1230: 12 of cap free
Amount of items: 2
Items: 
Size: 587792 Color: 19
Size: 412197 Color: 1

Bin 1231: 12 of cap free
Amount of items: 2
Items: 
Size: 595949 Color: 18
Size: 404040 Color: 7

Bin 1232: 12 of cap free
Amount of items: 2
Items: 
Size: 599983 Color: 14
Size: 400006 Color: 2

Bin 1233: 12 of cap free
Amount of items: 2
Items: 
Size: 600197 Color: 5
Size: 399792 Color: 4

Bin 1234: 12 of cap free
Amount of items: 2
Items: 
Size: 622422 Color: 9
Size: 377567 Color: 10

Bin 1235: 12 of cap free
Amount of items: 2
Items: 
Size: 623342 Color: 3
Size: 376647 Color: 4

Bin 1236: 12 of cap free
Amount of items: 2
Items: 
Size: 638638 Color: 12
Size: 361351 Color: 3

Bin 1237: 12 of cap free
Amount of items: 2
Items: 
Size: 694780 Color: 1
Size: 305209 Color: 5

Bin 1238: 12 of cap free
Amount of items: 2
Items: 
Size: 710594 Color: 4
Size: 289395 Color: 15

Bin 1239: 12 of cap free
Amount of items: 2
Items: 
Size: 713421 Color: 13
Size: 286568 Color: 10

Bin 1240: 12 of cap free
Amount of items: 2
Items: 
Size: 724913 Color: 3
Size: 275076 Color: 8

Bin 1241: 12 of cap free
Amount of items: 2
Items: 
Size: 745553 Color: 1
Size: 254436 Color: 2

Bin 1242: 12 of cap free
Amount of items: 2
Items: 
Size: 763699 Color: 15
Size: 236290 Color: 12

Bin 1243: 12 of cap free
Amount of items: 2
Items: 
Size: 777067 Color: 4
Size: 222922 Color: 17

Bin 1244: 13 of cap free
Amount of items: 3
Items: 
Size: 745240 Color: 9
Size: 132976 Color: 7
Size: 121772 Color: 12

Bin 1245: 13 of cap free
Amount of items: 3
Items: 
Size: 652016 Color: 13
Size: 174196 Color: 5
Size: 173776 Color: 2

Bin 1246: 13 of cap free
Amount of items: 3
Items: 
Size: 701738 Color: 2
Size: 149162 Color: 1
Size: 149088 Color: 10

Bin 1247: 13 of cap free
Amount of items: 3
Items: 
Size: 712817 Color: 0
Size: 151843 Color: 5
Size: 135328 Color: 7

Bin 1248: 13 of cap free
Amount of items: 3
Items: 
Size: 585173 Color: 3
Size: 215622 Color: 14
Size: 199193 Color: 15

Bin 1249: 13 of cap free
Amount of items: 3
Items: 
Size: 656217 Color: 5
Size: 173135 Color: 9
Size: 170636 Color: 8

Bin 1250: 13 of cap free
Amount of items: 3
Items: 
Size: 684344 Color: 7
Size: 173995 Color: 13
Size: 141649 Color: 8

Bin 1251: 13 of cap free
Amount of items: 3
Items: 
Size: 624605 Color: 0
Size: 188250 Color: 8
Size: 187133 Color: 2

Bin 1252: 13 of cap free
Amount of items: 3
Items: 
Size: 682658 Color: 7
Size: 158949 Color: 16
Size: 158381 Color: 10

Bin 1253: 13 of cap free
Amount of items: 3
Items: 
Size: 742275 Color: 15
Size: 141821 Color: 6
Size: 115892 Color: 7

Bin 1254: 13 of cap free
Amount of items: 2
Items: 
Size: 797482 Color: 6
Size: 202506 Color: 11

Bin 1255: 13 of cap free
Amount of items: 3
Items: 
Size: 625253 Color: 14
Size: 187467 Color: 12
Size: 187268 Color: 12

Bin 1256: 13 of cap free
Amount of items: 3
Items: 
Size: 533872 Color: 2
Size: 357432 Color: 16
Size: 108684 Color: 11

Bin 1257: 13 of cap free
Amount of items: 3
Items: 
Size: 666702 Color: 4
Size: 167312 Color: 4
Size: 165974 Color: 17

Bin 1258: 13 of cap free
Amount of items: 3
Items: 
Size: 726901 Color: 15
Size: 136658 Color: 4
Size: 136429 Color: 9

Bin 1259: 13 of cap free
Amount of items: 2
Items: 
Size: 581242 Color: 2
Size: 418746 Color: 17

Bin 1260: 13 of cap free
Amount of items: 3
Items: 
Size: 371274 Color: 9
Size: 339226 Color: 15
Size: 289488 Color: 7

Bin 1261: 13 of cap free
Amount of items: 3
Items: 
Size: 622896 Color: 8
Size: 189056 Color: 13
Size: 188036 Color: 18

Bin 1262: 13 of cap free
Amount of items: 2
Items: 
Size: 710916 Color: 3
Size: 289072 Color: 16

Bin 1263: 13 of cap free
Amount of items: 2
Items: 
Size: 562513 Color: 16
Size: 437475 Color: 6

Bin 1264: 13 of cap free
Amount of items: 2
Items: 
Size: 508600 Color: 17
Size: 491388 Color: 10

Bin 1265: 13 of cap free
Amount of items: 2
Items: 
Size: 518511 Color: 15
Size: 481477 Color: 19

Bin 1266: 13 of cap free
Amount of items: 2
Items: 
Size: 546092 Color: 10
Size: 453896 Color: 16

Bin 1267: 13 of cap free
Amount of items: 2
Items: 
Size: 556430 Color: 12
Size: 443558 Color: 3

Bin 1268: 13 of cap free
Amount of items: 2
Items: 
Size: 556562 Color: 3
Size: 443426 Color: 19

Bin 1269: 13 of cap free
Amount of items: 2
Items: 
Size: 563634 Color: 16
Size: 436354 Color: 11

Bin 1270: 13 of cap free
Amount of items: 2
Items: 
Size: 572590 Color: 15
Size: 427398 Color: 0

Bin 1271: 13 of cap free
Amount of items: 2
Items: 
Size: 585809 Color: 11
Size: 414179 Color: 12

Bin 1272: 13 of cap free
Amount of items: 2
Items: 
Size: 588427 Color: 16
Size: 411561 Color: 15

Bin 1273: 13 of cap free
Amount of items: 2
Items: 
Size: 596220 Color: 0
Size: 403768 Color: 16

Bin 1274: 13 of cap free
Amount of items: 2
Items: 
Size: 605147 Color: 18
Size: 394841 Color: 4

Bin 1275: 13 of cap free
Amount of items: 2
Items: 
Size: 610601 Color: 5
Size: 389387 Color: 19

Bin 1276: 13 of cap free
Amount of items: 2
Items: 
Size: 623810 Color: 12
Size: 376178 Color: 13

Bin 1277: 13 of cap free
Amount of items: 2
Items: 
Size: 642326 Color: 4
Size: 357662 Color: 6

Bin 1278: 13 of cap free
Amount of items: 3
Items: 
Size: 646936 Color: 10
Size: 176554 Color: 9
Size: 176498 Color: 0

Bin 1279: 13 of cap free
Amount of items: 2
Items: 
Size: 671218 Color: 6
Size: 328770 Color: 12

Bin 1280: 13 of cap free
Amount of items: 2
Items: 
Size: 688479 Color: 18
Size: 311509 Color: 1

Bin 1281: 13 of cap free
Amount of items: 2
Items: 
Size: 689069 Color: 6
Size: 310919 Color: 15

Bin 1282: 13 of cap free
Amount of items: 2
Items: 
Size: 689690 Color: 10
Size: 310298 Color: 11

Bin 1283: 13 of cap free
Amount of items: 2
Items: 
Size: 716022 Color: 10
Size: 283966 Color: 6

Bin 1284: 13 of cap free
Amount of items: 2
Items: 
Size: 718335 Color: 18
Size: 281653 Color: 12

Bin 1285: 13 of cap free
Amount of items: 2
Items: 
Size: 765999 Color: 3
Size: 233989 Color: 4

Bin 1286: 13 of cap free
Amount of items: 2
Items: 
Size: 777799 Color: 11
Size: 222189 Color: 10

Bin 1287: 13 of cap free
Amount of items: 2
Items: 
Size: 781402 Color: 13
Size: 218586 Color: 8

Bin 1288: 13 of cap free
Amount of items: 2
Items: 
Size: 799112 Color: 12
Size: 200876 Color: 3

Bin 1289: 14 of cap free
Amount of items: 3
Items: 
Size: 693443 Color: 5
Size: 153596 Color: 12
Size: 152948 Color: 15

Bin 1290: 14 of cap free
Amount of items: 3
Items: 
Size: 688852 Color: 7
Size: 155608 Color: 11
Size: 155527 Color: 7

Bin 1291: 14 of cap free
Amount of items: 3
Items: 
Size: 695513 Color: 13
Size: 161404 Color: 11
Size: 143070 Color: 7

Bin 1292: 14 of cap free
Amount of items: 3
Items: 
Size: 619677 Color: 0
Size: 190168 Color: 2
Size: 190142 Color: 11

Bin 1293: 14 of cap free
Amount of items: 2
Items: 
Size: 510866 Color: 5
Size: 489121 Color: 11

Bin 1294: 14 of cap free
Amount of items: 3
Items: 
Size: 556334 Color: 19
Size: 222095 Color: 18
Size: 221558 Color: 12

Bin 1295: 14 of cap free
Amount of items: 3
Items: 
Size: 613707 Color: 14
Size: 193145 Color: 1
Size: 193135 Color: 11

Bin 1296: 14 of cap free
Amount of items: 2
Items: 
Size: 635275 Color: 8
Size: 364712 Color: 2

Bin 1297: 14 of cap free
Amount of items: 2
Items: 
Size: 773914 Color: 5
Size: 226073 Color: 15

Bin 1298: 14 of cap free
Amount of items: 2
Items: 
Size: 614369 Color: 1
Size: 385618 Color: 9

Bin 1299: 14 of cap free
Amount of items: 2
Items: 
Size: 573490 Color: 3
Size: 426497 Color: 12

Bin 1300: 14 of cap free
Amount of items: 2
Items: 
Size: 500777 Color: 12
Size: 499210 Color: 19

Bin 1301: 14 of cap free
Amount of items: 2
Items: 
Size: 502693 Color: 11
Size: 497294 Color: 6

Bin 1302: 14 of cap free
Amount of items: 2
Items: 
Size: 509475 Color: 9
Size: 490512 Color: 14

Bin 1303: 14 of cap free
Amount of items: 2
Items: 
Size: 524364 Color: 16
Size: 475623 Color: 14

Bin 1304: 14 of cap free
Amount of items: 2
Items: 
Size: 535090 Color: 9
Size: 464897 Color: 3

Bin 1305: 14 of cap free
Amount of items: 2
Items: 
Size: 543903 Color: 19
Size: 456084 Color: 14

Bin 1306: 14 of cap free
Amount of items: 2
Items: 
Size: 557636 Color: 12
Size: 442351 Color: 15

Bin 1307: 14 of cap free
Amount of items: 2
Items: 
Size: 561885 Color: 1
Size: 438102 Color: 10

Bin 1308: 14 of cap free
Amount of items: 2
Items: 
Size: 562674 Color: 2
Size: 437313 Color: 14

Bin 1309: 14 of cap free
Amount of items: 2
Items: 
Size: 569731 Color: 10
Size: 430256 Color: 14

Bin 1310: 14 of cap free
Amount of items: 2
Items: 
Size: 577838 Color: 14
Size: 422149 Color: 2

Bin 1311: 14 of cap free
Amount of items: 2
Items: 
Size: 586923 Color: 9
Size: 413064 Color: 15

Bin 1312: 14 of cap free
Amount of items: 2
Items: 
Size: 591822 Color: 3
Size: 408165 Color: 6

Bin 1313: 14 of cap free
Amount of items: 2
Items: 
Size: 618351 Color: 5
Size: 381636 Color: 18

Bin 1314: 14 of cap free
Amount of items: 2
Items: 
Size: 619726 Color: 3
Size: 380261 Color: 4

Bin 1315: 14 of cap free
Amount of items: 3
Items: 
Size: 622207 Color: 11
Size: 188895 Color: 7
Size: 188885 Color: 5

Bin 1316: 14 of cap free
Amount of items: 2
Items: 
Size: 627305 Color: 14
Size: 372682 Color: 7

Bin 1317: 14 of cap free
Amount of items: 3
Items: 
Size: 643015 Color: 16
Size: 178671 Color: 6
Size: 178301 Color: 12

Bin 1318: 14 of cap free
Amount of items: 2
Items: 
Size: 648589 Color: 19
Size: 351398 Color: 7

Bin 1319: 14 of cap free
Amount of items: 2
Items: 
Size: 664529 Color: 4
Size: 335458 Color: 18

Bin 1320: 14 of cap free
Amount of items: 2
Items: 
Size: 667592 Color: 0
Size: 332395 Color: 2

Bin 1321: 14 of cap free
Amount of items: 2
Items: 
Size: 674077 Color: 14
Size: 325910 Color: 17

Bin 1322: 14 of cap free
Amount of items: 2
Items: 
Size: 683683 Color: 0
Size: 316304 Color: 7

Bin 1323: 14 of cap free
Amount of items: 2
Items: 
Size: 708871 Color: 12
Size: 291116 Color: 11

Bin 1324: 14 of cap free
Amount of items: 2
Items: 
Size: 722668 Color: 6
Size: 277319 Color: 14

Bin 1325: 14 of cap free
Amount of items: 2
Items: 
Size: 751051 Color: 10
Size: 248936 Color: 13

Bin 1326: 14 of cap free
Amount of items: 2
Items: 
Size: 765494 Color: 2
Size: 234493 Color: 17

Bin 1327: 14 of cap free
Amount of items: 2
Items: 
Size: 774042 Color: 14
Size: 225945 Color: 1

Bin 1328: 14 of cap free
Amount of items: 2
Items: 
Size: 790874 Color: 4
Size: 209113 Color: 14

Bin 1329: 14 of cap free
Amount of items: 2
Items: 
Size: 791340 Color: 4
Size: 208647 Color: 10

Bin 1330: 15 of cap free
Amount of items: 3
Items: 
Size: 780109 Color: 8
Size: 110351 Color: 7
Size: 109526 Color: 12

Bin 1331: 15 of cap free
Amount of items: 3
Items: 
Size: 659497 Color: 3
Size: 170245 Color: 3
Size: 170244 Color: 2

Bin 1332: 15 of cap free
Amount of items: 3
Items: 
Size: 737995 Color: 19
Size: 131149 Color: 11
Size: 130842 Color: 13

Bin 1333: 15 of cap free
Amount of items: 3
Items: 
Size: 792602 Color: 7
Size: 104152 Color: 6
Size: 103232 Color: 1

Bin 1334: 15 of cap free
Amount of items: 2
Items: 
Size: 610954 Color: 11
Size: 389032 Color: 9

Bin 1335: 15 of cap free
Amount of items: 2
Items: 
Size: 721536 Color: 18
Size: 278450 Color: 0

Bin 1336: 15 of cap free
Amount of items: 2
Items: 
Size: 787558 Color: 10
Size: 212428 Color: 4

Bin 1337: 15 of cap free
Amount of items: 2
Items: 
Size: 778338 Color: 3
Size: 221648 Color: 17

Bin 1338: 15 of cap free
Amount of items: 2
Items: 
Size: 505705 Color: 16
Size: 494281 Color: 10

Bin 1339: 15 of cap free
Amount of items: 2
Items: 
Size: 779012 Color: 16
Size: 220974 Color: 2

Bin 1340: 15 of cap free
Amount of items: 2
Items: 
Size: 500156 Color: 16
Size: 499830 Color: 19

Bin 1341: 15 of cap free
Amount of items: 3
Items: 
Size: 378720 Color: 13
Size: 341282 Color: 15
Size: 279984 Color: 12

Bin 1342: 15 of cap free
Amount of items: 2
Items: 
Size: 520311 Color: 18
Size: 479675 Color: 16

Bin 1343: 15 of cap free
Amount of items: 2
Items: 
Size: 520594 Color: 4
Size: 479392 Color: 3

Bin 1344: 15 of cap free
Amount of items: 2
Items: 
Size: 530550 Color: 9
Size: 469436 Color: 12

Bin 1345: 15 of cap free
Amount of items: 2
Items: 
Size: 545850 Color: 9
Size: 454136 Color: 19

Bin 1346: 15 of cap free
Amount of items: 2
Items: 
Size: 546557 Color: 7
Size: 453429 Color: 5

Bin 1347: 15 of cap free
Amount of items: 2
Items: 
Size: 551601 Color: 6
Size: 448385 Color: 15

Bin 1348: 15 of cap free
Amount of items: 2
Items: 
Size: 577865 Color: 18
Size: 422121 Color: 4

Bin 1349: 15 of cap free
Amount of items: 2
Items: 
Size: 608366 Color: 11
Size: 391620 Color: 8

Bin 1350: 15 of cap free
Amount of items: 2
Items: 
Size: 608789 Color: 16
Size: 391197 Color: 7

Bin 1351: 15 of cap free
Amount of items: 2
Items: 
Size: 644706 Color: 4
Size: 355280 Color: 6

Bin 1352: 15 of cap free
Amount of items: 2
Items: 
Size: 647269 Color: 6
Size: 352717 Color: 4

Bin 1353: 15 of cap free
Amount of items: 2
Items: 
Size: 652343 Color: 18
Size: 347643 Color: 12

Bin 1354: 15 of cap free
Amount of items: 2
Items: 
Size: 662802 Color: 19
Size: 337184 Color: 14

Bin 1355: 15 of cap free
Amount of items: 2
Items: 
Size: 696326 Color: 8
Size: 303660 Color: 11

Bin 1356: 15 of cap free
Amount of items: 2
Items: 
Size: 701190 Color: 4
Size: 298796 Color: 12

Bin 1357: 15 of cap free
Amount of items: 2
Items: 
Size: 720913 Color: 4
Size: 279073 Color: 3

Bin 1358: 15 of cap free
Amount of items: 2
Items: 
Size: 734066 Color: 13
Size: 265920 Color: 9

Bin 1359: 15 of cap free
Amount of items: 2
Items: 
Size: 746241 Color: 7
Size: 253745 Color: 11

Bin 1360: 16 of cap free
Amount of items: 3
Items: 
Size: 708461 Color: 16
Size: 173449 Color: 12
Size: 118075 Color: 18

Bin 1361: 16 of cap free
Amount of items: 3
Items: 
Size: 614563 Color: 18
Size: 195174 Color: 5
Size: 190248 Color: 12

Bin 1362: 16 of cap free
Amount of items: 3
Items: 
Size: 747129 Color: 17
Size: 129381 Color: 1
Size: 123475 Color: 7

Bin 1363: 16 of cap free
Amount of items: 2
Items: 
Size: 791138 Color: 14
Size: 208847 Color: 9

Bin 1364: 16 of cap free
Amount of items: 2
Items: 
Size: 570361 Color: 10
Size: 429624 Color: 13

Bin 1365: 16 of cap free
Amount of items: 2
Items: 
Size: 500105 Color: 4
Size: 499880 Color: 19

Bin 1366: 16 of cap free
Amount of items: 2
Items: 
Size: 562984 Color: 10
Size: 437001 Color: 8

Bin 1367: 16 of cap free
Amount of items: 2
Items: 
Size: 636034 Color: 12
Size: 363951 Color: 8

Bin 1368: 16 of cap free
Amount of items: 2
Items: 
Size: 597438 Color: 2
Size: 402547 Color: 3

Bin 1369: 16 of cap free
Amount of items: 2
Items: 
Size: 506215 Color: 12
Size: 493770 Color: 15

Bin 1370: 16 of cap free
Amount of items: 2
Items: 
Size: 520855 Color: 0
Size: 479130 Color: 1

Bin 1371: 16 of cap free
Amount of items: 2
Items: 
Size: 522476 Color: 4
Size: 477509 Color: 13

Bin 1372: 16 of cap free
Amount of items: 2
Items: 
Size: 523317 Color: 10
Size: 476668 Color: 13

Bin 1373: 16 of cap free
Amount of items: 2
Items: 
Size: 524276 Color: 6
Size: 475709 Color: 16

Bin 1374: 16 of cap free
Amount of items: 2
Items: 
Size: 542419 Color: 12
Size: 457566 Color: 19

Bin 1375: 16 of cap free
Amount of items: 2
Items: 
Size: 550554 Color: 3
Size: 449431 Color: 1

Bin 1376: 16 of cap free
Amount of items: 2
Items: 
Size: 552360 Color: 2
Size: 447625 Color: 14

Bin 1377: 16 of cap free
Amount of items: 2
Items: 
Size: 570541 Color: 11
Size: 429444 Color: 7

Bin 1378: 16 of cap free
Amount of items: 2
Items: 
Size: 574576 Color: 11
Size: 425409 Color: 8

Bin 1379: 16 of cap free
Amount of items: 2
Items: 
Size: 580740 Color: 0
Size: 419245 Color: 11

Bin 1380: 16 of cap free
Amount of items: 2
Items: 
Size: 583077 Color: 9
Size: 416908 Color: 13

Bin 1381: 16 of cap free
Amount of items: 2
Items: 
Size: 584848 Color: 9
Size: 415137 Color: 5

Bin 1382: 16 of cap free
Amount of items: 3
Items: 
Size: 611968 Color: 5
Size: 194208 Color: 10
Size: 193809 Color: 7

Bin 1383: 16 of cap free
Amount of items: 3
Items: 
Size: 656300 Color: 5
Size: 171867 Color: 2
Size: 171818 Color: 16

Bin 1384: 16 of cap free
Amount of items: 2
Items: 
Size: 672625 Color: 15
Size: 327360 Color: 19

Bin 1385: 16 of cap free
Amount of items: 2
Items: 
Size: 685046 Color: 4
Size: 314939 Color: 5

Bin 1386: 16 of cap free
Amount of items: 2
Items: 
Size: 687891 Color: 18
Size: 312094 Color: 2

Bin 1387: 16 of cap free
Amount of items: 2
Items: 
Size: 688242 Color: 11
Size: 311743 Color: 7

Bin 1388: 16 of cap free
Amount of items: 3
Items: 
Size: 700940 Color: 17
Size: 149536 Color: 17
Size: 149509 Color: 19

Bin 1389: 16 of cap free
Amount of items: 2
Items: 
Size: 719695 Color: 17
Size: 280290 Color: 12

Bin 1390: 16 of cap free
Amount of items: 2
Items: 
Size: 730220 Color: 16
Size: 269765 Color: 0

Bin 1391: 16 of cap free
Amount of items: 2
Items: 
Size: 737160 Color: 9
Size: 262825 Color: 14

Bin 1392: 17 of cap free
Amount of items: 2
Items: 
Size: 743538 Color: 17
Size: 256446 Color: 9

Bin 1393: 17 of cap free
Amount of items: 3
Items: 
Size: 702969 Color: 11
Size: 150173 Color: 14
Size: 146842 Color: 19

Bin 1394: 17 of cap free
Amount of items: 3
Items: 
Size: 650530 Color: 1
Size: 174850 Color: 8
Size: 174604 Color: 14

Bin 1395: 17 of cap free
Amount of items: 3
Items: 
Size: 644476 Color: 3
Size: 177774 Color: 9
Size: 177734 Color: 10

Bin 1396: 17 of cap free
Amount of items: 3
Items: 
Size: 681675 Color: 7
Size: 159800 Color: 19
Size: 158509 Color: 2

Bin 1397: 17 of cap free
Amount of items: 3
Items: 
Size: 673109 Color: 16
Size: 164058 Color: 13
Size: 162817 Color: 4

Bin 1398: 17 of cap free
Amount of items: 2
Items: 
Size: 692754 Color: 11
Size: 307230 Color: 17

Bin 1399: 17 of cap free
Amount of items: 2
Items: 
Size: 794230 Color: 3
Size: 205754 Color: 13

Bin 1400: 17 of cap free
Amount of items: 2
Items: 
Size: 738534 Color: 9
Size: 261450 Color: 18

Bin 1401: 17 of cap free
Amount of items: 3
Items: 
Size: 371085 Color: 19
Size: 348870 Color: 16
Size: 280029 Color: 18

Bin 1402: 17 of cap free
Amount of items: 2
Items: 
Size: 756110 Color: 16
Size: 243874 Color: 15

Bin 1403: 17 of cap free
Amount of items: 3
Items: 
Size: 355582 Color: 4
Size: 333019 Color: 8
Size: 311383 Color: 2

Bin 1404: 17 of cap free
Amount of items: 2
Items: 
Size: 660904 Color: 17
Size: 339080 Color: 16

Bin 1405: 17 of cap free
Amount of items: 2
Items: 
Size: 791306 Color: 15
Size: 208678 Color: 16

Bin 1406: 17 of cap free
Amount of items: 2
Items: 
Size: 587168 Color: 16
Size: 412816 Color: 3

Bin 1407: 17 of cap free
Amount of items: 2
Items: 
Size: 667634 Color: 19
Size: 332350 Color: 8

Bin 1408: 17 of cap free
Amount of items: 2
Items: 
Size: 639751 Color: 16
Size: 360233 Color: 8

Bin 1409: 17 of cap free
Amount of items: 2
Items: 
Size: 510099 Color: 3
Size: 489885 Color: 6

Bin 1410: 17 of cap free
Amount of items: 2
Items: 
Size: 511618 Color: 10
Size: 488366 Color: 1

Bin 1411: 17 of cap free
Amount of items: 2
Items: 
Size: 512825 Color: 2
Size: 487159 Color: 17

Bin 1412: 17 of cap free
Amount of items: 2
Items: 
Size: 513980 Color: 0
Size: 486004 Color: 10

Bin 1413: 17 of cap free
Amount of items: 2
Items: 
Size: 524582 Color: 15
Size: 475402 Color: 5

Bin 1414: 17 of cap free
Amount of items: 2
Items: 
Size: 530497 Color: 5
Size: 469487 Color: 18

Bin 1415: 17 of cap free
Amount of items: 2
Items: 
Size: 542489 Color: 13
Size: 457495 Color: 5

Bin 1416: 17 of cap free
Amount of items: 2
Items: 
Size: 546032 Color: 19
Size: 453952 Color: 2

Bin 1417: 17 of cap free
Amount of items: 2
Items: 
Size: 555421 Color: 11
Size: 444563 Color: 7

Bin 1418: 17 of cap free
Amount of items: 2
Items: 
Size: 582474 Color: 1
Size: 417510 Color: 5

Bin 1419: 17 of cap free
Amount of items: 2
Items: 
Size: 584604 Color: 8
Size: 415380 Color: 17

Bin 1420: 17 of cap free
Amount of items: 2
Items: 
Size: 602304 Color: 0
Size: 397680 Color: 14

Bin 1421: 17 of cap free
Amount of items: 2
Items: 
Size: 605580 Color: 4
Size: 394404 Color: 2

Bin 1422: 17 of cap free
Amount of items: 3
Items: 
Size: 607319 Color: 3
Size: 196669 Color: 10
Size: 195996 Color: 4

Bin 1423: 17 of cap free
Amount of items: 2
Items: 
Size: 611867 Color: 7
Size: 388117 Color: 15

Bin 1424: 17 of cap free
Amount of items: 2
Items: 
Size: 613043 Color: 17
Size: 386941 Color: 0

Bin 1425: 17 of cap free
Amount of items: 2
Items: 
Size: 616455 Color: 19
Size: 383529 Color: 17

Bin 1426: 17 of cap free
Amount of items: 2
Items: 
Size: 634293 Color: 11
Size: 365691 Color: 5

Bin 1427: 17 of cap free
Amount of items: 2
Items: 
Size: 649781 Color: 17
Size: 350203 Color: 0

Bin 1428: 17 of cap free
Amount of items: 2
Items: 
Size: 653064 Color: 8
Size: 346920 Color: 15

Bin 1429: 17 of cap free
Amount of items: 2
Items: 
Size: 665565 Color: 5
Size: 334419 Color: 9

Bin 1430: 17 of cap free
Amount of items: 2
Items: 
Size: 692075 Color: 14
Size: 307909 Color: 19

Bin 1431: 17 of cap free
Amount of items: 2
Items: 
Size: 718607 Color: 0
Size: 281377 Color: 18

Bin 1432: 17 of cap free
Amount of items: 2
Items: 
Size: 733714 Color: 5
Size: 266270 Color: 7

Bin 1433: 17 of cap free
Amount of items: 2
Items: 
Size: 742544 Color: 19
Size: 257440 Color: 15

Bin 1434: 17 of cap free
Amount of items: 2
Items: 
Size: 743722 Color: 4
Size: 256262 Color: 13

Bin 1435: 17 of cap free
Amount of items: 2
Items: 
Size: 743818 Color: 0
Size: 256166 Color: 11

Bin 1436: 17 of cap free
Amount of items: 2
Items: 
Size: 749768 Color: 5
Size: 250216 Color: 17

Bin 1437: 17 of cap free
Amount of items: 2
Items: 
Size: 777346 Color: 15
Size: 222638 Color: 6

Bin 1438: 18 of cap free
Amount of items: 3
Items: 
Size: 745327 Color: 8
Size: 127632 Color: 16
Size: 127024 Color: 7

Bin 1439: 18 of cap free
Amount of items: 3
Items: 
Size: 668625 Color: 0
Size: 166143 Color: 19
Size: 165215 Color: 4

Bin 1440: 18 of cap free
Amount of items: 3
Items: 
Size: 639970 Color: 10
Size: 180376 Color: 2
Size: 179637 Color: 10

Bin 1441: 18 of cap free
Amount of items: 3
Items: 
Size: 689157 Color: 7
Size: 173970 Color: 3
Size: 136856 Color: 15

Bin 1442: 18 of cap free
Amount of items: 2
Items: 
Size: 791911 Color: 12
Size: 208072 Color: 17

Bin 1443: 18 of cap free
Amount of items: 2
Items: 
Size: 736815 Color: 7
Size: 263168 Color: 2

Bin 1444: 18 of cap free
Amount of items: 2
Items: 
Size: 787442 Color: 19
Size: 212541 Color: 5

Bin 1445: 18 of cap free
Amount of items: 3
Items: 
Size: 652987 Color: 15
Size: 175470 Color: 0
Size: 171526 Color: 0

Bin 1446: 18 of cap free
Amount of items: 2
Items: 
Size: 707970 Color: 2
Size: 292013 Color: 18

Bin 1447: 18 of cap free
Amount of items: 2
Items: 
Size: 561815 Color: 9
Size: 438168 Color: 15

Bin 1448: 18 of cap free
Amount of items: 3
Items: 
Size: 372215 Color: 3
Size: 344242 Color: 17
Size: 283526 Color: 13

Bin 1449: 18 of cap free
Amount of items: 2
Items: 
Size: 590741 Color: 11
Size: 409242 Color: 16

Bin 1450: 18 of cap free
Amount of items: 2
Items: 
Size: 508378 Color: 8
Size: 491605 Color: 16

Bin 1451: 18 of cap free
Amount of items: 3
Items: 
Size: 368572 Color: 19
Size: 337427 Color: 5
Size: 293984 Color: 8

Bin 1452: 18 of cap free
Amount of items: 2
Items: 
Size: 502347 Color: 5
Size: 497636 Color: 11

Bin 1453: 18 of cap free
Amount of items: 2
Items: 
Size: 511021 Color: 3
Size: 488962 Color: 19

Bin 1454: 18 of cap free
Amount of items: 2
Items: 
Size: 540534 Color: 0
Size: 459449 Color: 9

Bin 1455: 18 of cap free
Amount of items: 2
Items: 
Size: 541391 Color: 17
Size: 458592 Color: 2

Bin 1456: 18 of cap free
Amount of items: 2
Items: 
Size: 549619 Color: 8
Size: 450364 Color: 11

Bin 1457: 18 of cap free
Amount of items: 2
Items: 
Size: 567056 Color: 2
Size: 432927 Color: 1

Bin 1458: 18 of cap free
Amount of items: 2
Items: 
Size: 574276 Color: 11
Size: 425707 Color: 12

Bin 1459: 18 of cap free
Amount of items: 2
Items: 
Size: 592869 Color: 11
Size: 407114 Color: 3

Bin 1460: 18 of cap free
Amount of items: 2
Items: 
Size: 604160 Color: 17
Size: 395823 Color: 18

Bin 1461: 18 of cap free
Amount of items: 2
Items: 
Size: 612393 Color: 13
Size: 387590 Color: 19

Bin 1462: 18 of cap free
Amount of items: 2
Items: 
Size: 614242 Color: 9
Size: 385741 Color: 4

Bin 1463: 18 of cap free
Amount of items: 2
Items: 
Size: 627014 Color: 15
Size: 372969 Color: 18

Bin 1464: 18 of cap free
Amount of items: 2
Items: 
Size: 634904 Color: 19
Size: 365079 Color: 2

Bin 1465: 18 of cap free
Amount of items: 2
Items: 
Size: 635710 Color: 19
Size: 364273 Color: 4

Bin 1466: 18 of cap free
Amount of items: 2
Items: 
Size: 641233 Color: 12
Size: 358750 Color: 14

Bin 1467: 18 of cap free
Amount of items: 3
Items: 
Size: 646517 Color: 3
Size: 176739 Color: 4
Size: 176727 Color: 14

Bin 1468: 18 of cap free
Amount of items: 2
Items: 
Size: 676043 Color: 4
Size: 323940 Color: 19

Bin 1469: 18 of cap free
Amount of items: 2
Items: 
Size: 702520 Color: 0
Size: 297463 Color: 13

Bin 1470: 18 of cap free
Amount of items: 2
Items: 
Size: 714211 Color: 8
Size: 285772 Color: 5

Bin 1471: 18 of cap free
Amount of items: 2
Items: 
Size: 728700 Color: 13
Size: 271283 Color: 19

Bin 1472: 18 of cap free
Amount of items: 2
Items: 
Size: 741397 Color: 8
Size: 258586 Color: 7

Bin 1473: 18 of cap free
Amount of items: 2
Items: 
Size: 749815 Color: 11
Size: 250168 Color: 8

Bin 1474: 18 of cap free
Amount of items: 2
Items: 
Size: 750018 Color: 14
Size: 249965 Color: 15

Bin 1475: 18 of cap free
Amount of items: 2
Items: 
Size: 784083 Color: 6
Size: 215900 Color: 11

Bin 1476: 19 of cap free
Amount of items: 3
Items: 
Size: 679444 Color: 9
Size: 160940 Color: 3
Size: 159598 Color: 12

Bin 1477: 19 of cap free
Amount of items: 2
Items: 
Size: 606081 Color: 0
Size: 393901 Color: 12

Bin 1478: 19 of cap free
Amount of items: 3
Items: 
Size: 652715 Color: 0
Size: 174917 Color: 12
Size: 172350 Color: 18

Bin 1479: 19 of cap free
Amount of items: 3
Items: 
Size: 674652 Color: 5
Size: 163369 Color: 4
Size: 161961 Color: 1

Bin 1480: 19 of cap free
Amount of items: 2
Items: 
Size: 753526 Color: 11
Size: 246456 Color: 8

Bin 1481: 19 of cap free
Amount of items: 3
Items: 
Size: 690341 Color: 6
Size: 154908 Color: 4
Size: 154733 Color: 4

Bin 1482: 19 of cap free
Amount of items: 2
Items: 
Size: 595321 Color: 7
Size: 404661 Color: 5

Bin 1483: 19 of cap free
Amount of items: 2
Items: 
Size: 782408 Color: 3
Size: 217574 Color: 4

Bin 1484: 19 of cap free
Amount of items: 2
Items: 
Size: 545330 Color: 11
Size: 454652 Color: 2

Bin 1485: 19 of cap free
Amount of items: 2
Items: 
Size: 777947 Color: 14
Size: 222035 Color: 9

Bin 1486: 19 of cap free
Amount of items: 2
Items: 
Size: 761034 Color: 4
Size: 238948 Color: 1

Bin 1487: 19 of cap free
Amount of items: 2
Items: 
Size: 788074 Color: 16
Size: 211908 Color: 4

Bin 1488: 19 of cap free
Amount of items: 2
Items: 
Size: 507880 Color: 8
Size: 492102 Color: 10

Bin 1489: 19 of cap free
Amount of items: 2
Items: 
Size: 636651 Color: 8
Size: 363331 Color: 19

Bin 1490: 19 of cap free
Amount of items: 2
Items: 
Size: 624124 Color: 5
Size: 375858 Color: 15

Bin 1491: 19 of cap free
Amount of items: 2
Items: 
Size: 652235 Color: 13
Size: 347747 Color: 5

Bin 1492: 19 of cap free
Amount of items: 2
Items: 
Size: 506758 Color: 17
Size: 493224 Color: 3

Bin 1493: 19 of cap free
Amount of items: 2
Items: 
Size: 515356 Color: 0
Size: 484626 Color: 4

Bin 1494: 19 of cap free
Amount of items: 3
Items: 
Size: 531872 Color: 19
Size: 244345 Color: 10
Size: 223765 Color: 19

Bin 1495: 19 of cap free
Amount of items: 2
Items: 
Size: 547824 Color: 14
Size: 452158 Color: 3

Bin 1496: 19 of cap free
Amount of items: 2
Items: 
Size: 560468 Color: 6
Size: 439514 Color: 14

Bin 1497: 19 of cap free
Amount of items: 2
Items: 
Size: 578715 Color: 15
Size: 421267 Color: 4

Bin 1498: 19 of cap free
Amount of items: 2
Items: 
Size: 583825 Color: 15
Size: 416157 Color: 17

Bin 1499: 19 of cap free
Amount of items: 2
Items: 
Size: 587092 Color: 15
Size: 412890 Color: 16

Bin 1500: 19 of cap free
Amount of items: 2
Items: 
Size: 624737 Color: 9
Size: 375245 Color: 17

Bin 1501: 19 of cap free
Amount of items: 2
Items: 
Size: 629048 Color: 5
Size: 370934 Color: 1

Bin 1502: 19 of cap free
Amount of items: 3
Items: 
Size: 629409 Color: 2
Size: 185342 Color: 15
Size: 185231 Color: 6

Bin 1503: 19 of cap free
Amount of items: 2
Items: 
Size: 632536 Color: 17
Size: 367446 Color: 0

Bin 1504: 19 of cap free
Amount of items: 3
Items: 
Size: 668850 Color: 18
Size: 166365 Color: 11
Size: 164767 Color: 2

Bin 1505: 19 of cap free
Amount of items: 2
Items: 
Size: 692802 Color: 2
Size: 307180 Color: 9

Bin 1506: 19 of cap free
Amount of items: 2
Items: 
Size: 694551 Color: 7
Size: 305431 Color: 0

Bin 1507: 19 of cap free
Amount of items: 2
Items: 
Size: 697293 Color: 1
Size: 302689 Color: 13

Bin 1508: 19 of cap free
Amount of items: 2
Items: 
Size: 698424 Color: 9
Size: 301558 Color: 10

Bin 1509: 19 of cap free
Amount of items: 2
Items: 
Size: 713562 Color: 19
Size: 286420 Color: 17

Bin 1510: 19 of cap free
Amount of items: 2
Items: 
Size: 715035 Color: 17
Size: 284947 Color: 5

Bin 1511: 19 of cap free
Amount of items: 2
Items: 
Size: 729934 Color: 10
Size: 270048 Color: 3

Bin 1512: 19 of cap free
Amount of items: 2
Items: 
Size: 746789 Color: 0
Size: 253193 Color: 11

Bin 1513: 19 of cap free
Amount of items: 2
Items: 
Size: 762765 Color: 3
Size: 237217 Color: 7

Bin 1514: 19 of cap free
Amount of items: 2
Items: 
Size: 777243 Color: 10
Size: 222739 Color: 11

Bin 1515: 20 of cap free
Amount of items: 3
Items: 
Size: 757720 Color: 15
Size: 139967 Color: 5
Size: 102294 Color: 2

Bin 1516: 20 of cap free
Amount of items: 2
Items: 
Size: 765838 Color: 9
Size: 234143 Color: 18

Bin 1517: 20 of cap free
Amount of items: 3
Items: 
Size: 690885 Color: 11
Size: 154744 Color: 0
Size: 154352 Color: 13

Bin 1518: 20 of cap free
Amount of items: 3
Items: 
Size: 694297 Color: 6
Size: 158010 Color: 9
Size: 147674 Color: 11

Bin 1519: 20 of cap free
Amount of items: 2
Items: 
Size: 720146 Color: 15
Size: 279835 Color: 16

Bin 1520: 20 of cap free
Amount of items: 3
Items: 
Size: 677450 Color: 1
Size: 161359 Color: 4
Size: 161172 Color: 6

Bin 1521: 20 of cap free
Amount of items: 2
Items: 
Size: 769505 Color: 10
Size: 230476 Color: 2

Bin 1522: 20 of cap free
Amount of items: 2
Items: 
Size: 731907 Color: 5
Size: 268074 Color: 18

Bin 1523: 20 of cap free
Amount of items: 2
Items: 
Size: 536419 Color: 8
Size: 463562 Color: 16

Bin 1524: 20 of cap free
Amount of items: 2
Items: 
Size: 538237 Color: 15
Size: 461744 Color: 3

Bin 1525: 20 of cap free
Amount of items: 2
Items: 
Size: 568822 Color: 15
Size: 431159 Color: 10

Bin 1526: 20 of cap free
Amount of items: 2
Items: 
Size: 592742 Color: 19
Size: 407239 Color: 12

Bin 1527: 20 of cap free
Amount of items: 2
Items: 
Size: 600986 Color: 11
Size: 398995 Color: 13

Bin 1528: 20 of cap free
Amount of items: 2
Items: 
Size: 604046 Color: 11
Size: 395935 Color: 5

Bin 1529: 20 of cap free
Amount of items: 2
Items: 
Size: 612439 Color: 0
Size: 387542 Color: 16

Bin 1530: 20 of cap free
Amount of items: 3
Items: 
Size: 627259 Color: 15
Size: 186925 Color: 7
Size: 185797 Color: 17

Bin 1531: 20 of cap free
Amount of items: 2
Items: 
Size: 634337 Color: 2
Size: 365644 Color: 6

Bin 1532: 20 of cap free
Amount of items: 2
Items: 
Size: 639070 Color: 17
Size: 360911 Color: 19

Bin 1533: 20 of cap free
Amount of items: 2
Items: 
Size: 647199 Color: 15
Size: 352782 Color: 8

Bin 1534: 20 of cap free
Amount of items: 2
Items: 
Size: 653151 Color: 2
Size: 346830 Color: 6

Bin 1535: 20 of cap free
Amount of items: 2
Items: 
Size: 659027 Color: 13
Size: 340954 Color: 3

Bin 1536: 20 of cap free
Amount of items: 2
Items: 
Size: 759487 Color: 13
Size: 240494 Color: 18

Bin 1537: 20 of cap free
Amount of items: 2
Items: 
Size: 767864 Color: 17
Size: 232117 Color: 13

Bin 1538: 20 of cap free
Amount of items: 2
Items: 
Size: 772114 Color: 2
Size: 227867 Color: 6

Bin 1539: 20 of cap free
Amount of items: 2
Items: 
Size: 777151 Color: 19
Size: 222830 Color: 0

Bin 1540: 20 of cap free
Amount of items: 2
Items: 
Size: 777572 Color: 17
Size: 222409 Color: 6

Bin 1541: 20 of cap free
Amount of items: 2
Items: 
Size: 786129 Color: 2
Size: 213852 Color: 13

Bin 1542: 20 of cap free
Amount of items: 2
Items: 
Size: 793175 Color: 11
Size: 206806 Color: 17

Bin 1543: 21 of cap free
Amount of items: 3
Items: 
Size: 663444 Color: 15
Size: 168379 Color: 5
Size: 168157 Color: 6

Bin 1544: 21 of cap free
Amount of items: 2
Items: 
Size: 639965 Color: 17
Size: 360015 Color: 0

Bin 1545: 21 of cap free
Amount of items: 2
Items: 
Size: 651104 Color: 9
Size: 348876 Color: 15

Bin 1546: 21 of cap free
Amount of items: 3
Items: 
Size: 621787 Color: 7
Size: 190409 Color: 14
Size: 187784 Color: 9

Bin 1547: 21 of cap free
Amount of items: 2
Items: 
Size: 654804 Color: 0
Size: 345176 Color: 16

Bin 1548: 21 of cap free
Amount of items: 2
Items: 
Size: 617004 Color: 12
Size: 382976 Color: 1

Bin 1549: 21 of cap free
Amount of items: 3
Items: 
Size: 659985 Color: 6
Size: 170215 Color: 9
Size: 169780 Color: 11

Bin 1550: 21 of cap free
Amount of items: 2
Items: 
Size: 545425 Color: 16
Size: 454555 Color: 15

Bin 1551: 21 of cap free
Amount of items: 2
Items: 
Size: 541566 Color: 16
Size: 458414 Color: 18

Bin 1552: 21 of cap free
Amount of items: 3
Items: 
Size: 441993 Color: 9
Size: 293748 Color: 4
Size: 264239 Color: 18

Bin 1553: 21 of cap free
Amount of items: 2
Items: 
Size: 659908 Color: 17
Size: 340072 Color: 18

Bin 1554: 21 of cap free
Amount of items: 2
Items: 
Size: 512536 Color: 14
Size: 487444 Color: 9

Bin 1555: 21 of cap free
Amount of items: 2
Items: 
Size: 513434 Color: 1
Size: 486546 Color: 6

Bin 1556: 21 of cap free
Amount of items: 2
Items: 
Size: 516544 Color: 5
Size: 483436 Color: 1

Bin 1557: 21 of cap free
Amount of items: 2
Items: 
Size: 522137 Color: 19
Size: 477843 Color: 15

Bin 1558: 21 of cap free
Amount of items: 2
Items: 
Size: 523147 Color: 13
Size: 476833 Color: 5

Bin 1559: 21 of cap free
Amount of items: 2
Items: 
Size: 555608 Color: 5
Size: 444372 Color: 1

Bin 1560: 21 of cap free
Amount of items: 2
Items: 
Size: 558400 Color: 18
Size: 441580 Color: 12

Bin 1561: 21 of cap free
Amount of items: 2
Items: 
Size: 577348 Color: 14
Size: 422632 Color: 15

Bin 1562: 21 of cap free
Amount of items: 2
Items: 
Size: 589389 Color: 1
Size: 410591 Color: 4

Bin 1563: 21 of cap free
Amount of items: 2
Items: 
Size: 596633 Color: 6
Size: 403347 Color: 7

Bin 1564: 21 of cap free
Amount of items: 2
Items: 
Size: 598855 Color: 3
Size: 401125 Color: 16

Bin 1565: 21 of cap free
Amount of items: 2
Items: 
Size: 630539 Color: 0
Size: 369441 Color: 9

Bin 1566: 21 of cap free
Amount of items: 2
Items: 
Size: 666131 Color: 9
Size: 333849 Color: 12

Bin 1567: 21 of cap free
Amount of items: 2
Items: 
Size: 679076 Color: 3
Size: 320904 Color: 14

Bin 1568: 21 of cap free
Amount of items: 2
Items: 
Size: 694394 Color: 17
Size: 305586 Color: 1

Bin 1569: 21 of cap free
Amount of items: 2
Items: 
Size: 702349 Color: 18
Size: 297631 Color: 19

Bin 1570: 21 of cap free
Amount of items: 2
Items: 
Size: 738600 Color: 1
Size: 261380 Color: 15

Bin 1571: 21 of cap free
Amount of items: 2
Items: 
Size: 763729 Color: 1
Size: 236251 Color: 6

Bin 1572: 21 of cap free
Amount of items: 2
Items: 
Size: 764973 Color: 8
Size: 235007 Color: 7

Bin 1573: 21 of cap free
Amount of items: 2
Items: 
Size: 765677 Color: 7
Size: 234303 Color: 3

Bin 1574: 22 of cap free
Amount of items: 3
Items: 
Size: 779796 Color: 6
Size: 117546 Color: 17
Size: 102637 Color: 1

Bin 1575: 22 of cap free
Amount of items: 3
Items: 
Size: 715968 Color: 11
Size: 142590 Color: 11
Size: 141421 Color: 13

Bin 1576: 22 of cap free
Amount of items: 3
Items: 
Size: 715882 Color: 5
Size: 143768 Color: 13
Size: 140329 Color: 19

Bin 1577: 22 of cap free
Amount of items: 2
Items: 
Size: 780942 Color: 14
Size: 219037 Color: 0

Bin 1578: 22 of cap free
Amount of items: 2
Items: 
Size: 621907 Color: 6
Size: 378072 Color: 5

Bin 1579: 22 of cap free
Amount of items: 2
Items: 
Size: 788337 Color: 16
Size: 211642 Color: 18

Bin 1580: 22 of cap free
Amount of items: 3
Items: 
Size: 368435 Color: 13
Size: 337387 Color: 17
Size: 294157 Color: 7

Bin 1581: 22 of cap free
Amount of items: 3
Items: 
Size: 531587 Color: 0
Size: 244445 Color: 4
Size: 223947 Color: 1

Bin 1582: 22 of cap free
Amount of items: 3
Items: 
Size: 703321 Color: 1
Size: 148487 Color: 12
Size: 148171 Color: 10

Bin 1583: 22 of cap free
Amount of items: 2
Items: 
Size: 627104 Color: 8
Size: 372875 Color: 19

Bin 1584: 22 of cap free
Amount of items: 2
Items: 
Size: 594817 Color: 9
Size: 405162 Color: 2

Bin 1585: 22 of cap free
Amount of items: 3
Items: 
Size: 368940 Color: 13
Size: 347543 Color: 9
Size: 283496 Color: 16

Bin 1586: 22 of cap free
Amount of items: 2
Items: 
Size: 528788 Color: 11
Size: 471191 Color: 4

Bin 1587: 22 of cap free
Amount of items: 2
Items: 
Size: 538389 Color: 12
Size: 461590 Color: 2

Bin 1588: 22 of cap free
Amount of items: 2
Items: 
Size: 542622 Color: 3
Size: 457357 Color: 4

Bin 1589: 22 of cap free
Amount of items: 2
Items: 
Size: 544423 Color: 1
Size: 455556 Color: 5

Bin 1590: 22 of cap free
Amount of items: 2
Items: 
Size: 545507 Color: 8
Size: 454472 Color: 12

Bin 1591: 22 of cap free
Amount of items: 2
Items: 
Size: 557767 Color: 7
Size: 442212 Color: 9

Bin 1592: 22 of cap free
Amount of items: 2
Items: 
Size: 560064 Color: 16
Size: 439915 Color: 15

Bin 1593: 22 of cap free
Amount of items: 2
Items: 
Size: 562114 Color: 11
Size: 437865 Color: 10

Bin 1594: 22 of cap free
Amount of items: 2
Items: 
Size: 566238 Color: 6
Size: 433741 Color: 19

Bin 1595: 22 of cap free
Amount of items: 2
Items: 
Size: 600652 Color: 14
Size: 399327 Color: 15

Bin 1596: 22 of cap free
Amount of items: 2
Items: 
Size: 624683 Color: 4
Size: 375296 Color: 18

Bin 1597: 22 of cap free
Amount of items: 2
Items: 
Size: 637385 Color: 14
Size: 362594 Color: 6

Bin 1598: 22 of cap free
Amount of items: 2
Items: 
Size: 651571 Color: 10
Size: 348408 Color: 7

Bin 1599: 22 of cap free
Amount of items: 2
Items: 
Size: 718501 Color: 4
Size: 281478 Color: 11

Bin 1600: 22 of cap free
Amount of items: 2
Items: 
Size: 723739 Color: 6
Size: 276240 Color: 14

Bin 1601: 22 of cap free
Amount of items: 2
Items: 
Size: 733653 Color: 2
Size: 266326 Color: 5

Bin 1602: 22 of cap free
Amount of items: 2
Items: 
Size: 761505 Color: 18
Size: 238474 Color: 0

Bin 1603: 23 of cap free
Amount of items: 2
Items: 
Size: 682843 Color: 8
Size: 317135 Color: 13

Bin 1604: 23 of cap free
Amount of items: 2
Items: 
Size: 605680 Color: 17
Size: 394298 Color: 19

Bin 1605: 23 of cap free
Amount of items: 2
Items: 
Size: 612891 Color: 8
Size: 387087 Color: 14

Bin 1606: 23 of cap free
Amount of items: 3
Items: 
Size: 597351 Color: 14
Size: 203103 Color: 15
Size: 199524 Color: 5

Bin 1607: 23 of cap free
Amount of items: 3
Items: 
Size: 639568 Color: 12
Size: 180946 Color: 5
Size: 179464 Color: 16

Bin 1608: 23 of cap free
Amount of items: 2
Items: 
Size: 735788 Color: 13
Size: 264190 Color: 12

Bin 1609: 23 of cap free
Amount of items: 2
Items: 
Size: 572315 Color: 17
Size: 427663 Color: 9

Bin 1610: 23 of cap free
Amount of items: 2
Items: 
Size: 709513 Color: 13
Size: 290465 Color: 18

Bin 1611: 23 of cap free
Amount of items: 2
Items: 
Size: 738836 Color: 5
Size: 261142 Color: 14

Bin 1612: 23 of cap free
Amount of items: 2
Items: 
Size: 720307 Color: 7
Size: 279671 Color: 0

Bin 1613: 23 of cap free
Amount of items: 2
Items: 
Size: 783873 Color: 1
Size: 216105 Color: 15

Bin 1614: 23 of cap free
Amount of items: 2
Items: 
Size: 548185 Color: 18
Size: 451793 Color: 5

Bin 1615: 23 of cap free
Amount of items: 2
Items: 
Size: 592007 Color: 16
Size: 407971 Color: 19

Bin 1616: 23 of cap free
Amount of items: 2
Items: 
Size: 595211 Color: 10
Size: 404767 Color: 5

Bin 1617: 23 of cap free
Amount of items: 2
Items: 
Size: 597496 Color: 7
Size: 402482 Color: 8

Bin 1618: 23 of cap free
Amount of items: 2
Items: 
Size: 635998 Color: 14
Size: 363980 Color: 6

Bin 1619: 23 of cap free
Amount of items: 2
Items: 
Size: 653037 Color: 8
Size: 346941 Color: 12

Bin 1620: 23 of cap free
Amount of items: 2
Items: 
Size: 662656 Color: 19
Size: 337322 Color: 3

Bin 1621: 23 of cap free
Amount of items: 2
Items: 
Size: 664641 Color: 4
Size: 335337 Color: 1

Bin 1622: 23 of cap free
Amount of items: 2
Items: 
Size: 727582 Color: 4
Size: 272396 Color: 10

Bin 1623: 23 of cap free
Amount of items: 2
Items: 
Size: 744443 Color: 16
Size: 255535 Color: 4

Bin 1624: 23 of cap free
Amount of items: 2
Items: 
Size: 777752 Color: 2
Size: 222226 Color: 19

Bin 1625: 23 of cap free
Amount of items: 2
Items: 
Size: 783164 Color: 4
Size: 216814 Color: 6

Bin 1626: 24 of cap free
Amount of items: 3
Items: 
Size: 692981 Color: 5
Size: 153873 Color: 3
Size: 153123 Color: 1

Bin 1627: 24 of cap free
Amount of items: 3
Items: 
Size: 692872 Color: 4
Size: 154060 Color: 17
Size: 153045 Color: 10

Bin 1628: 24 of cap free
Amount of items: 2
Items: 
Size: 616601 Color: 4
Size: 383376 Color: 19

Bin 1629: 24 of cap free
Amount of items: 3
Items: 
Size: 683200 Color: 16
Size: 158414 Color: 11
Size: 158363 Color: 8

Bin 1630: 24 of cap free
Amount of items: 2
Items: 
Size: 561788 Color: 7
Size: 438189 Color: 0

Bin 1631: 24 of cap free
Amount of items: 3
Items: 
Size: 583251 Color: 19
Size: 216379 Color: 2
Size: 200347 Color: 7

Bin 1632: 24 of cap free
Amount of items: 2
Items: 
Size: 647882 Color: 9
Size: 352095 Color: 6

Bin 1633: 24 of cap free
Amount of items: 2
Items: 
Size: 668132 Color: 12
Size: 331845 Color: 18

Bin 1634: 24 of cap free
Amount of items: 2
Items: 
Size: 720739 Color: 11
Size: 279238 Color: 4

Bin 1635: 24 of cap free
Amount of items: 2
Items: 
Size: 789087 Color: 4
Size: 210890 Color: 18

Bin 1636: 24 of cap free
Amount of items: 2
Items: 
Size: 563762 Color: 3
Size: 436215 Color: 17

Bin 1637: 24 of cap free
Amount of items: 2
Items: 
Size: 502484 Color: 10
Size: 497493 Color: 13

Bin 1638: 24 of cap free
Amount of items: 2
Items: 
Size: 509039 Color: 3
Size: 490938 Color: 13

Bin 1639: 24 of cap free
Amount of items: 2
Items: 
Size: 509444 Color: 8
Size: 490533 Color: 1

Bin 1640: 24 of cap free
Amount of items: 2
Items: 
Size: 510260 Color: 19
Size: 489717 Color: 16

Bin 1641: 24 of cap free
Amount of items: 2
Items: 
Size: 525866 Color: 16
Size: 474111 Color: 2

Bin 1642: 24 of cap free
Amount of items: 2
Items: 
Size: 532684 Color: 9
Size: 467293 Color: 4

Bin 1643: 24 of cap free
Amount of items: 2
Items: 
Size: 540459 Color: 17
Size: 459518 Color: 10

Bin 1644: 24 of cap free
Amount of items: 2
Items: 
Size: 567287 Color: 15
Size: 432690 Color: 12

Bin 1645: 24 of cap free
Amount of items: 3
Items: 
Size: 613226 Color: 15
Size: 193698 Color: 15
Size: 193053 Color: 6

Bin 1646: 24 of cap free
Amount of items: 2
Items: 
Size: 623751 Color: 13
Size: 376226 Color: 4

Bin 1647: 24 of cap free
Amount of items: 2
Items: 
Size: 645843 Color: 15
Size: 354134 Color: 4

Bin 1648: 24 of cap free
Amount of items: 2
Items: 
Size: 650501 Color: 15
Size: 349476 Color: 1

Bin 1649: 24 of cap free
Amount of items: 2
Items: 
Size: 653575 Color: 3
Size: 346402 Color: 10

Bin 1650: 24 of cap free
Amount of items: 2
Items: 
Size: 679992 Color: 7
Size: 319985 Color: 18

Bin 1651: 24 of cap free
Amount of items: 2
Items: 
Size: 701542 Color: 18
Size: 298435 Color: 16

Bin 1652: 24 of cap free
Amount of items: 2
Items: 
Size: 708613 Color: 11
Size: 291364 Color: 6

Bin 1653: 24 of cap free
Amount of items: 2
Items: 
Size: 721625 Color: 10
Size: 278352 Color: 8

Bin 1654: 24 of cap free
Amount of items: 2
Items: 
Size: 738276 Color: 7
Size: 261701 Color: 18

Bin 1655: 25 of cap free
Amount of items: 2
Items: 
Size: 664767 Color: 11
Size: 335209 Color: 14

Bin 1656: 25 of cap free
Amount of items: 2
Items: 
Size: 695450 Color: 3
Size: 304526 Color: 10

Bin 1657: 25 of cap free
Amount of items: 3
Items: 
Size: 605707 Color: 8
Size: 197212 Color: 18
Size: 197057 Color: 1

Bin 1658: 25 of cap free
Amount of items: 2
Items: 
Size: 714924 Color: 8
Size: 285052 Color: 11

Bin 1659: 25 of cap free
Amount of items: 3
Items: 
Size: 725331 Color: 14
Size: 137348 Color: 5
Size: 137297 Color: 4

Bin 1660: 25 of cap free
Amount of items: 3
Items: 
Size: 371275 Color: 5
Size: 366209 Color: 9
Size: 262492 Color: 13

Bin 1661: 25 of cap free
Amount of items: 3
Items: 
Size: 620750 Color: 4
Size: 189846 Color: 19
Size: 189380 Color: 11

Bin 1662: 25 of cap free
Amount of items: 2
Items: 
Size: 680267 Color: 12
Size: 319709 Color: 15

Bin 1663: 25 of cap free
Amount of items: 2
Items: 
Size: 743610 Color: 13
Size: 256366 Color: 16

Bin 1664: 25 of cap free
Amount of items: 2
Items: 
Size: 609156 Color: 5
Size: 390820 Color: 11

Bin 1665: 25 of cap free
Amount of items: 2
Items: 
Size: 781465 Color: 12
Size: 218511 Color: 18

Bin 1666: 25 of cap free
Amount of items: 3
Items: 
Size: 374975 Color: 8
Size: 340488 Color: 5
Size: 284513 Color: 6

Bin 1667: 25 of cap free
Amount of items: 2
Items: 
Size: 508114 Color: 19
Size: 491862 Color: 3

Bin 1668: 25 of cap free
Amount of items: 2
Items: 
Size: 516606 Color: 6
Size: 483370 Color: 8

Bin 1669: 25 of cap free
Amount of items: 2
Items: 
Size: 530238 Color: 0
Size: 469738 Color: 13

Bin 1670: 25 of cap free
Amount of items: 3
Items: 
Size: 530410 Color: 6
Size: 244274 Color: 15
Size: 225292 Color: 7

Bin 1671: 25 of cap free
Amount of items: 2
Items: 
Size: 567824 Color: 8
Size: 432152 Color: 4

Bin 1672: 25 of cap free
Amount of items: 2
Items: 
Size: 582312 Color: 0
Size: 417664 Color: 15

Bin 1673: 25 of cap free
Amount of items: 2
Items: 
Size: 622815 Color: 9
Size: 377161 Color: 14

Bin 1674: 25 of cap free
Amount of items: 2
Items: 
Size: 631883 Color: 14
Size: 368093 Color: 15

Bin 1675: 25 of cap free
Amount of items: 2
Items: 
Size: 653822 Color: 18
Size: 346154 Color: 3

Bin 1676: 25 of cap free
Amount of items: 2
Items: 
Size: 673042 Color: 13
Size: 326934 Color: 17

Bin 1677: 25 of cap free
Amount of items: 2
Items: 
Size: 681874 Color: 0
Size: 318102 Color: 1

Bin 1678: 25 of cap free
Amount of items: 2
Items: 
Size: 689674 Color: 15
Size: 310302 Color: 10

Bin 1679: 25 of cap free
Amount of items: 2
Items: 
Size: 695359 Color: 4
Size: 304617 Color: 2

Bin 1680: 25 of cap free
Amount of items: 2
Items: 
Size: 697881 Color: 2
Size: 302095 Color: 16

Bin 1681: 25 of cap free
Amount of items: 2
Items: 
Size: 728323 Color: 16
Size: 271653 Color: 5

Bin 1682: 25 of cap free
Amount of items: 2
Items: 
Size: 730646 Color: 17
Size: 269330 Color: 2

Bin 1683: 25 of cap free
Amount of items: 2
Items: 
Size: 733038 Color: 15
Size: 266938 Color: 18

Bin 1684: 25 of cap free
Amount of items: 2
Items: 
Size: 749439 Color: 6
Size: 250537 Color: 11

Bin 1685: 26 of cap free
Amount of items: 2
Items: 
Size: 615194 Color: 16
Size: 384781 Color: 7

Bin 1686: 26 of cap free
Amount of items: 2
Items: 
Size: 714799 Color: 9
Size: 285176 Color: 12

Bin 1687: 26 of cap free
Amount of items: 2
Items: 
Size: 718847 Color: 13
Size: 281128 Color: 7

Bin 1688: 26 of cap free
Amount of items: 3
Items: 
Size: 720270 Color: 10
Size: 157619 Color: 13
Size: 122086 Color: 16

Bin 1689: 26 of cap free
Amount of items: 2
Items: 
Size: 668618 Color: 3
Size: 331357 Color: 7

Bin 1690: 26 of cap free
Amount of items: 3
Items: 
Size: 661932 Color: 13
Size: 169214 Color: 14
Size: 168829 Color: 5

Bin 1691: 26 of cap free
Amount of items: 2
Items: 
Size: 631373 Color: 3
Size: 368602 Color: 17

Bin 1692: 26 of cap free
Amount of items: 3
Items: 
Size: 616765 Color: 1
Size: 225483 Color: 16
Size: 157727 Color: 15

Bin 1693: 26 of cap free
Amount of items: 3
Items: 
Size: 684204 Color: 15
Size: 160070 Color: 15
Size: 155701 Color: 2

Bin 1694: 26 of cap free
Amount of items: 2
Items: 
Size: 629996 Color: 17
Size: 369979 Color: 11

Bin 1695: 26 of cap free
Amount of items: 2
Items: 
Size: 759567 Color: 13
Size: 240408 Color: 2

Bin 1696: 26 of cap free
Amount of items: 2
Items: 
Size: 771694 Color: 18
Size: 228281 Color: 10

Bin 1697: 26 of cap free
Amount of items: 2
Items: 
Size: 509417 Color: 6
Size: 490558 Color: 18

Bin 1698: 26 of cap free
Amount of items: 2
Items: 
Size: 674536 Color: 17
Size: 325439 Color: 0

Bin 1699: 26 of cap free
Amount of items: 2
Items: 
Size: 527653 Color: 17
Size: 472322 Color: 8

Bin 1700: 26 of cap free
Amount of items: 3
Items: 
Size: 479531 Color: 8
Size: 268660 Color: 5
Size: 251784 Color: 10

Bin 1701: 26 of cap free
Amount of items: 2
Items: 
Size: 502789 Color: 0
Size: 497186 Color: 5

Bin 1702: 26 of cap free
Amount of items: 2
Items: 
Size: 516291 Color: 14
Size: 483684 Color: 7

Bin 1703: 26 of cap free
Amount of items: 2
Items: 
Size: 519105 Color: 19
Size: 480870 Color: 18

Bin 1704: 26 of cap free
Amount of items: 2
Items: 
Size: 527871 Color: 8
Size: 472104 Color: 4

Bin 1705: 26 of cap free
Amount of items: 2
Items: 
Size: 535388 Color: 11
Size: 464587 Color: 15

Bin 1706: 26 of cap free
Amount of items: 2
Items: 
Size: 544256 Color: 4
Size: 455719 Color: 14

Bin 1707: 26 of cap free
Amount of items: 2
Items: 
Size: 556482 Color: 11
Size: 443493 Color: 2

Bin 1708: 26 of cap free
Amount of items: 2
Items: 
Size: 558309 Color: 0
Size: 441666 Color: 19

Bin 1709: 26 of cap free
Amount of items: 2
Items: 
Size: 564206 Color: 17
Size: 435769 Color: 8

Bin 1710: 26 of cap free
Amount of items: 2
Items: 
Size: 570479 Color: 19
Size: 429496 Color: 7

Bin 1711: 26 of cap free
Amount of items: 2
Items: 
Size: 590858 Color: 19
Size: 409117 Color: 0

Bin 1712: 26 of cap free
Amount of items: 2
Items: 
Size: 598387 Color: 7
Size: 401588 Color: 2

Bin 1713: 26 of cap free
Amount of items: 2
Items: 
Size: 603236 Color: 4
Size: 396739 Color: 19

Bin 1714: 26 of cap free
Amount of items: 2
Items: 
Size: 612796 Color: 14
Size: 387179 Color: 12

Bin 1715: 26 of cap free
Amount of items: 2
Items: 
Size: 614081 Color: 14
Size: 385894 Color: 6

Bin 1716: 26 of cap free
Amount of items: 2
Items: 
Size: 620492 Color: 8
Size: 379483 Color: 3

Bin 1717: 26 of cap free
Amount of items: 2
Items: 
Size: 629087 Color: 16
Size: 370888 Color: 11

Bin 1718: 26 of cap free
Amount of items: 2
Items: 
Size: 632181 Color: 4
Size: 367794 Color: 6

Bin 1719: 26 of cap free
Amount of items: 2
Items: 
Size: 643142 Color: 6
Size: 356833 Color: 9

Bin 1720: 26 of cap free
Amount of items: 3
Items: 
Size: 644753 Color: 9
Size: 177869 Color: 11
Size: 177353 Color: 5

Bin 1721: 26 of cap free
Amount of items: 2
Items: 
Size: 673597 Color: 6
Size: 326378 Color: 9

Bin 1722: 26 of cap free
Amount of items: 2
Items: 
Size: 682472 Color: 10
Size: 317503 Color: 15

Bin 1723: 26 of cap free
Amount of items: 2
Items: 
Size: 684349 Color: 17
Size: 315626 Color: 6

Bin 1724: 26 of cap free
Amount of items: 2
Items: 
Size: 692606 Color: 12
Size: 307369 Color: 0

Bin 1725: 26 of cap free
Amount of items: 2
Items: 
Size: 713558 Color: 6
Size: 286417 Color: 1

Bin 1726: 26 of cap free
Amount of items: 2
Items: 
Size: 721393 Color: 0
Size: 278582 Color: 7

Bin 1727: 26 of cap free
Amount of items: 2
Items: 
Size: 734004 Color: 4
Size: 265971 Color: 12

Bin 1728: 26 of cap free
Amount of items: 2
Items: 
Size: 762394 Color: 17
Size: 237581 Color: 16

Bin 1729: 27 of cap free
Amount of items: 2
Items: 
Size: 682646 Color: 6
Size: 317328 Color: 8

Bin 1730: 27 of cap free
Amount of items: 3
Items: 
Size: 676614 Color: 17
Size: 161680 Color: 12
Size: 161680 Color: 2

Bin 1731: 27 of cap free
Amount of items: 3
Items: 
Size: 595665 Color: 8
Size: 203326 Color: 10
Size: 200983 Color: 5

Bin 1732: 27 of cap free
Amount of items: 2
Items: 
Size: 607727 Color: 2
Size: 392247 Color: 12

Bin 1733: 27 of cap free
Amount of items: 2
Items: 
Size: 735065 Color: 16
Size: 264909 Color: 3

Bin 1734: 27 of cap free
Amount of items: 3
Items: 
Size: 741890 Color: 19
Size: 150462 Color: 1
Size: 107622 Color: 3

Bin 1735: 27 of cap free
Amount of items: 2
Items: 
Size: 542557 Color: 12
Size: 457417 Color: 11

Bin 1736: 27 of cap free
Amount of items: 2
Items: 
Size: 624561 Color: 11
Size: 375413 Color: 3

Bin 1737: 27 of cap free
Amount of items: 2
Items: 
Size: 507227 Color: 12
Size: 492747 Color: 0

Bin 1738: 27 of cap free
Amount of items: 2
Items: 
Size: 513210 Color: 16
Size: 486764 Color: 19

Bin 1739: 27 of cap free
Amount of items: 2
Items: 
Size: 519016 Color: 2
Size: 480958 Color: 8

Bin 1740: 27 of cap free
Amount of items: 2
Items: 
Size: 523484 Color: 17
Size: 476490 Color: 5

Bin 1741: 27 of cap free
Amount of items: 2
Items: 
Size: 541360 Color: 3
Size: 458614 Color: 13

Bin 1742: 27 of cap free
Amount of items: 2
Items: 
Size: 551979 Color: 13
Size: 447995 Color: 5

Bin 1743: 27 of cap free
Amount of items: 2
Items: 
Size: 567524 Color: 15
Size: 432450 Color: 0

Bin 1744: 27 of cap free
Amount of items: 2
Items: 
Size: 569819 Color: 4
Size: 430155 Color: 12

Bin 1745: 27 of cap free
Amount of items: 2
Items: 
Size: 570605 Color: 9
Size: 429369 Color: 0

Bin 1746: 27 of cap free
Amount of items: 2
Items: 
Size: 592630 Color: 2
Size: 407344 Color: 11

Bin 1747: 27 of cap free
Amount of items: 2
Items: 
Size: 617824 Color: 8
Size: 382150 Color: 18

Bin 1748: 27 of cap free
Amount of items: 2
Items: 
Size: 665849 Color: 18
Size: 334125 Color: 3

Bin 1749: 27 of cap free
Amount of items: 2
Items: 
Size: 729709 Color: 5
Size: 270265 Color: 0

Bin 1750: 27 of cap free
Amount of items: 2
Items: 
Size: 736195 Color: 17
Size: 263779 Color: 5

Bin 1751: 27 of cap free
Amount of items: 2
Items: 
Size: 763106 Color: 11
Size: 236868 Color: 19

Bin 1752: 27 of cap free
Amount of items: 2
Items: 
Size: 768538 Color: 14
Size: 231436 Color: 5

Bin 1753: 27 of cap free
Amount of items: 2
Items: 
Size: 771447 Color: 11
Size: 228527 Color: 17

Bin 1754: 27 of cap free
Amount of items: 2
Items: 
Size: 793578 Color: 8
Size: 206396 Color: 5

Bin 1755: 27 of cap free
Amount of items: 2
Items: 
Size: 794296 Color: 11
Size: 205678 Color: 5

Bin 1756: 28 of cap free
Amount of items: 2
Items: 
Size: 765634 Color: 1
Size: 234339 Color: 2

Bin 1757: 28 of cap free
Amount of items: 2
Items: 
Size: 685297 Color: 6
Size: 314676 Color: 17

Bin 1758: 28 of cap free
Amount of items: 3
Items: 
Size: 787226 Color: 10
Size: 107206 Color: 8
Size: 105541 Color: 16

Bin 1759: 28 of cap free
Amount of items: 3
Items: 
Size: 731556 Color: 13
Size: 134795 Color: 16
Size: 133622 Color: 14

Bin 1760: 28 of cap free
Amount of items: 2
Items: 
Size: 710085 Color: 19
Size: 289888 Color: 0

Bin 1761: 28 of cap free
Amount of items: 2
Items: 
Size: 751713 Color: 15
Size: 248260 Color: 4

Bin 1762: 28 of cap free
Amount of items: 3
Items: 
Size: 752382 Color: 0
Size: 125936 Color: 11
Size: 121655 Color: 3

Bin 1763: 28 of cap free
Amount of items: 2
Items: 
Size: 672763 Color: 19
Size: 327210 Color: 15

Bin 1764: 28 of cap free
Amount of items: 2
Items: 
Size: 515077 Color: 6
Size: 484896 Color: 14

Bin 1765: 28 of cap free
Amount of items: 2
Items: 
Size: 614971 Color: 10
Size: 385002 Color: 4

Bin 1766: 28 of cap free
Amount of items: 3
Items: 
Size: 629476 Color: 9
Size: 185897 Color: 15
Size: 184600 Color: 8

Bin 1767: 28 of cap free
Amount of items: 2
Items: 
Size: 731805 Color: 6
Size: 268168 Color: 14

Bin 1768: 28 of cap free
Amount of items: 2
Items: 
Size: 679732 Color: 10
Size: 320241 Color: 12

Bin 1769: 28 of cap free
Amount of items: 2
Items: 
Size: 790649 Color: 6
Size: 209324 Color: 2

Bin 1770: 28 of cap free
Amount of items: 2
Items: 
Size: 588054 Color: 18
Size: 411919 Color: 12

Bin 1771: 28 of cap free
Amount of items: 2
Items: 
Size: 778296 Color: 16
Size: 221677 Color: 8

Bin 1772: 28 of cap free
Amount of items: 2
Items: 
Size: 535878 Color: 5
Size: 464095 Color: 17

Bin 1773: 28 of cap free
Amount of items: 2
Items: 
Size: 550689 Color: 3
Size: 449284 Color: 5

Bin 1774: 28 of cap free
Amount of items: 2
Items: 
Size: 574033 Color: 12
Size: 425940 Color: 5

Bin 1775: 28 of cap free
Amount of items: 2
Items: 
Size: 598956 Color: 1
Size: 401017 Color: 19

Bin 1776: 28 of cap free
Amount of items: 2
Items: 
Size: 606347 Color: 18
Size: 393626 Color: 19

Bin 1777: 28 of cap free
Amount of items: 2
Items: 
Size: 620864 Color: 11
Size: 379109 Color: 17

Bin 1778: 28 of cap free
Amount of items: 2
Items: 
Size: 627208 Color: 14
Size: 372765 Color: 7

Bin 1779: 28 of cap free
Amount of items: 2
Items: 
Size: 627748 Color: 6
Size: 372225 Color: 16

Bin 1780: 28 of cap free
Amount of items: 2
Items: 
Size: 631235 Color: 9
Size: 368738 Color: 6

Bin 1781: 28 of cap free
Amount of items: 2
Items: 
Size: 662461 Color: 8
Size: 337512 Color: 11

Bin 1782: 28 of cap free
Amount of items: 2
Items: 
Size: 670475 Color: 7
Size: 329498 Color: 19

Bin 1783: 28 of cap free
Amount of items: 2
Items: 
Size: 672461 Color: 7
Size: 327512 Color: 17

Bin 1784: 28 of cap free
Amount of items: 2
Items: 
Size: 691806 Color: 13
Size: 308167 Color: 17

Bin 1785: 28 of cap free
Amount of items: 2
Items: 
Size: 733438 Color: 6
Size: 266535 Color: 0

Bin 1786: 28 of cap free
Amount of items: 2
Items: 
Size: 759925 Color: 6
Size: 240048 Color: 17

Bin 1787: 28 of cap free
Amount of items: 2
Items: 
Size: 771336 Color: 0
Size: 228637 Color: 16

Bin 1788: 28 of cap free
Amount of items: 2
Items: 
Size: 771532 Color: 4
Size: 228441 Color: 16

Bin 1789: 28 of cap free
Amount of items: 2
Items: 
Size: 771647 Color: 19
Size: 228326 Color: 18

Bin 1790: 28 of cap free
Amount of items: 2
Items: 
Size: 772952 Color: 9
Size: 227021 Color: 10

Bin 1791: 28 of cap free
Amount of items: 2
Items: 
Size: 794259 Color: 0
Size: 205714 Color: 5

Bin 1792: 29 of cap free
Amount of items: 3
Items: 
Size: 756538 Color: 3
Size: 131707 Color: 6
Size: 111727 Color: 7

Bin 1793: 29 of cap free
Amount of items: 3
Items: 
Size: 649712 Color: 12
Size: 175198 Color: 5
Size: 175062 Color: 9

Bin 1794: 29 of cap free
Amount of items: 2
Items: 
Size: 700707 Color: 11
Size: 299265 Color: 19

Bin 1795: 29 of cap free
Amount of items: 2
Items: 
Size: 654876 Color: 13
Size: 345096 Color: 19

Bin 1796: 29 of cap free
Amount of items: 3
Items: 
Size: 650333 Color: 11
Size: 175234 Color: 7
Size: 174405 Color: 16

Bin 1797: 29 of cap free
Amount of items: 2
Items: 
Size: 676850 Color: 0
Size: 323122 Color: 4

Bin 1798: 29 of cap free
Amount of items: 2
Items: 
Size: 507489 Color: 19
Size: 492483 Color: 14

Bin 1799: 29 of cap free
Amount of items: 2
Items: 
Size: 685255 Color: 15
Size: 314717 Color: 7

Bin 1800: 29 of cap free
Amount of items: 2
Items: 
Size: 723902 Color: 16
Size: 276070 Color: 17

Bin 1801: 29 of cap free
Amount of items: 2
Items: 
Size: 502244 Color: 2
Size: 497728 Color: 12

Bin 1802: 29 of cap free
Amount of items: 2
Items: 
Size: 547904 Color: 17
Size: 452068 Color: 16

Bin 1803: 29 of cap free
Amount of items: 2
Items: 
Size: 552279 Color: 7
Size: 447693 Color: 19

Bin 1804: 29 of cap free
Amount of items: 2
Items: 
Size: 566120 Color: 4
Size: 433852 Color: 5

Bin 1805: 29 of cap free
Amount of items: 2
Items: 
Size: 567077 Color: 13
Size: 432895 Color: 19

Bin 1806: 29 of cap free
Amount of items: 2
Items: 
Size: 572691 Color: 2
Size: 427281 Color: 0

Bin 1807: 29 of cap free
Amount of items: 2
Items: 
Size: 577960 Color: 0
Size: 422012 Color: 4

Bin 1808: 29 of cap free
Amount of items: 2
Items: 
Size: 584977 Color: 6
Size: 414995 Color: 19

Bin 1809: 29 of cap free
Amount of items: 2
Items: 
Size: 604004 Color: 17
Size: 395968 Color: 18

Bin 1810: 29 of cap free
Amount of items: 2
Items: 
Size: 611528 Color: 10
Size: 388444 Color: 14

Bin 1811: 29 of cap free
Amount of items: 2
Items: 
Size: 653570 Color: 15
Size: 346402 Color: 7

Bin 1812: 29 of cap free
Amount of items: 2
Items: 
Size: 683328 Color: 8
Size: 316644 Color: 15

Bin 1813: 29 of cap free
Amount of items: 2
Items: 
Size: 709958 Color: 17
Size: 290014 Color: 3

Bin 1814: 29 of cap free
Amount of items: 2
Items: 
Size: 710637 Color: 3
Size: 289335 Color: 12

Bin 1815: 29 of cap free
Amount of items: 2
Items: 
Size: 712087 Color: 9
Size: 287885 Color: 17

Bin 1816: 29 of cap free
Amount of items: 2
Items: 
Size: 713360 Color: 6
Size: 286612 Color: 14

Bin 1817: 29 of cap free
Amount of items: 2
Items: 
Size: 715027 Color: 12
Size: 284945 Color: 7

Bin 1818: 29 of cap free
Amount of items: 2
Items: 
Size: 737765 Color: 7
Size: 262207 Color: 18

Bin 1819: 29 of cap free
Amount of items: 2
Items: 
Size: 771818 Color: 9
Size: 228154 Color: 5

Bin 1820: 30 of cap free
Amount of items: 3
Items: 
Size: 668063 Color: 13
Size: 168282 Color: 8
Size: 163626 Color: 10

Bin 1821: 30 of cap free
Amount of items: 2
Items: 
Size: 617945 Color: 11
Size: 382026 Color: 7

Bin 1822: 30 of cap free
Amount of items: 2
Items: 
Size: 607833 Color: 0
Size: 392138 Color: 15

Bin 1823: 30 of cap free
Amount of items: 2
Items: 
Size: 752553 Color: 2
Size: 247418 Color: 13

Bin 1824: 30 of cap free
Amount of items: 3
Items: 
Size: 644970 Color: 16
Size: 177503 Color: 4
Size: 177498 Color: 12

Bin 1825: 30 of cap free
Amount of items: 2
Items: 
Size: 742956 Color: 13
Size: 257015 Color: 8

Bin 1826: 30 of cap free
Amount of items: 2
Items: 
Size: 655737 Color: 12
Size: 344234 Color: 1

Bin 1827: 30 of cap free
Amount of items: 2
Items: 
Size: 655351 Color: 19
Size: 344620 Color: 0

Bin 1828: 30 of cap free
Amount of items: 2
Items: 
Size: 611835 Color: 8
Size: 388136 Color: 11

Bin 1829: 30 of cap free
Amount of items: 2
Items: 
Size: 648822 Color: 19
Size: 351149 Color: 16

Bin 1830: 30 of cap free
Amount of items: 2
Items: 
Size: 622108 Color: 15
Size: 377863 Color: 7

Bin 1831: 30 of cap free
Amount of items: 2
Items: 
Size: 591182 Color: 2
Size: 408789 Color: 9

Bin 1832: 30 of cap free
Amount of items: 3
Items: 
Size: 605600 Color: 13
Size: 197378 Color: 15
Size: 196993 Color: 11

Bin 1833: 30 of cap free
Amount of items: 2
Items: 
Size: 711424 Color: 9
Size: 288547 Color: 0

Bin 1834: 30 of cap free
Amount of items: 2
Items: 
Size: 724766 Color: 9
Size: 275205 Color: 16

Bin 1835: 30 of cap free
Amount of items: 2
Items: 
Size: 568573 Color: 11
Size: 431398 Color: 5

Bin 1836: 30 of cap free
Amount of items: 2
Items: 
Size: 515796 Color: 14
Size: 484175 Color: 12

Bin 1837: 30 of cap free
Amount of items: 2
Items: 
Size: 522974 Color: 10
Size: 476997 Color: 14

Bin 1838: 30 of cap free
Amount of items: 2
Items: 
Size: 534319 Color: 6
Size: 465652 Color: 12

Bin 1839: 30 of cap free
Amount of items: 2
Items: 
Size: 537859 Color: 18
Size: 462112 Color: 19

Bin 1840: 30 of cap free
Amount of items: 2
Items: 
Size: 551294 Color: 3
Size: 448677 Color: 10

Bin 1841: 30 of cap free
Amount of items: 2
Items: 
Size: 555405 Color: 5
Size: 444566 Color: 11

Bin 1842: 30 of cap free
Amount of items: 2
Items: 
Size: 557547 Color: 15
Size: 442424 Color: 3

Bin 1843: 30 of cap free
Amount of items: 2
Items: 
Size: 576504 Color: 15
Size: 423467 Color: 17

Bin 1844: 30 of cap free
Amount of items: 2
Items: 
Size: 585929 Color: 14
Size: 414042 Color: 16

Bin 1845: 30 of cap free
Amount of items: 2
Items: 
Size: 595177 Color: 10
Size: 404794 Color: 2

Bin 1846: 30 of cap free
Amount of items: 2
Items: 
Size: 626190 Color: 15
Size: 373781 Color: 0

Bin 1847: 30 of cap free
Amount of items: 2
Items: 
Size: 705461 Color: 13
Size: 294510 Color: 7

Bin 1848: 30 of cap free
Amount of items: 2
Items: 
Size: 723587 Color: 4
Size: 276384 Color: 14

Bin 1849: 30 of cap free
Amount of items: 2
Items: 
Size: 724042 Color: 14
Size: 275929 Color: 11

Bin 1850: 30 of cap free
Amount of items: 2
Items: 
Size: 772424 Color: 7
Size: 227547 Color: 18

Bin 1851: 31 of cap free
Amount of items: 3
Items: 
Size: 657024 Color: 9
Size: 173699 Color: 18
Size: 169247 Color: 13

Bin 1852: 31 of cap free
Amount of items: 2
Items: 
Size: 775982 Color: 19
Size: 223988 Color: 7

Bin 1853: 31 of cap free
Amount of items: 3
Items: 
Size: 716971 Color: 6
Size: 142241 Color: 9
Size: 140758 Color: 4

Bin 1854: 31 of cap free
Amount of items: 3
Items: 
Size: 719202 Color: 9
Size: 140664 Color: 4
Size: 140104 Color: 6

Bin 1855: 31 of cap free
Amount of items: 3
Items: 
Size: 611957 Color: 16
Size: 194050 Color: 12
Size: 193963 Color: 9

Bin 1856: 31 of cap free
Amount of items: 2
Items: 
Size: 749699 Color: 11
Size: 250271 Color: 15

Bin 1857: 31 of cap free
Amount of items: 3
Items: 
Size: 489166 Color: 13
Size: 266816 Color: 16
Size: 243988 Color: 3

Bin 1858: 31 of cap free
Amount of items: 2
Items: 
Size: 677133 Color: 5
Size: 322837 Color: 16

Bin 1859: 31 of cap free
Amount of items: 2
Items: 
Size: 641368 Color: 1
Size: 358602 Color: 16

Bin 1860: 31 of cap free
Amount of items: 2
Items: 
Size: 576078 Color: 4
Size: 423892 Color: 14

Bin 1861: 31 of cap free
Amount of items: 2
Items: 
Size: 501972 Color: 9
Size: 497998 Color: 11

Bin 1862: 31 of cap free
Amount of items: 2
Items: 
Size: 513701 Color: 4
Size: 486269 Color: 17

Bin 1863: 31 of cap free
Amount of items: 2
Items: 
Size: 518886 Color: 11
Size: 481084 Color: 6

Bin 1864: 31 of cap free
Amount of items: 2
Items: 
Size: 541077 Color: 16
Size: 458893 Color: 4

Bin 1865: 31 of cap free
Amount of items: 2
Items: 
Size: 564108 Color: 1
Size: 435862 Color: 8

Bin 1866: 31 of cap free
Amount of items: 2
Items: 
Size: 572734 Color: 9
Size: 427236 Color: 2

Bin 1867: 31 of cap free
Amount of items: 2
Items: 
Size: 583100 Color: 2
Size: 416870 Color: 10

Bin 1868: 31 of cap free
Amount of items: 2
Items: 
Size: 585664 Color: 8
Size: 414306 Color: 17

Bin 1869: 31 of cap free
Amount of items: 2
Items: 
Size: 588549 Color: 13
Size: 411421 Color: 4

Bin 1870: 31 of cap free
Amount of items: 2
Items: 
Size: 599466 Color: 10
Size: 400504 Color: 3

Bin 1871: 31 of cap free
Amount of items: 2
Items: 
Size: 639302 Color: 2
Size: 360668 Color: 0

Bin 1872: 31 of cap free
Amount of items: 2
Items: 
Size: 671050 Color: 13
Size: 328920 Color: 8

Bin 1873: 31 of cap free
Amount of items: 2
Items: 
Size: 691869 Color: 13
Size: 308101 Color: 2

Bin 1874: 31 of cap free
Amount of items: 2
Items: 
Size: 695852 Color: 12
Size: 304118 Color: 2

Bin 1875: 31 of cap free
Amount of items: 2
Items: 
Size: 770653 Color: 12
Size: 229317 Color: 6

Bin 1876: 32 of cap free
Amount of items: 3
Items: 
Size: 719560 Color: 9
Size: 145813 Color: 18
Size: 134596 Color: 15

Bin 1877: 32 of cap free
Amount of items: 3
Items: 
Size: 752587 Color: 5
Size: 130991 Color: 4
Size: 116391 Color: 14

Bin 1878: 32 of cap free
Amount of items: 2
Items: 
Size: 639782 Color: 13
Size: 360187 Color: 0

Bin 1879: 32 of cap free
Amount of items: 2
Items: 
Size: 635729 Color: 2
Size: 364240 Color: 17

Bin 1880: 32 of cap free
Amount of items: 2
Items: 
Size: 698142 Color: 19
Size: 301827 Color: 9

Bin 1881: 32 of cap free
Amount of items: 2
Items: 
Size: 796315 Color: 9
Size: 203654 Color: 10

Bin 1882: 32 of cap free
Amount of items: 2
Items: 
Size: 532138 Color: 6
Size: 467831 Color: 2

Bin 1883: 32 of cap free
Amount of items: 2
Items: 
Size: 759521 Color: 3
Size: 240448 Color: 17

Bin 1884: 32 of cap free
Amount of items: 2
Items: 
Size: 597228 Color: 4
Size: 402741 Color: 18

Bin 1885: 32 of cap free
Amount of items: 2
Items: 
Size: 775466 Color: 5
Size: 224503 Color: 16

Bin 1886: 32 of cap free
Amount of items: 3
Items: 
Size: 616117 Color: 18
Size: 192317 Color: 14
Size: 191535 Color: 2

Bin 1887: 32 of cap free
Amount of items: 2
Items: 
Size: 701655 Color: 13
Size: 298314 Color: 0

Bin 1888: 32 of cap free
Amount of items: 3
Items: 
Size: 444648 Color: 4
Size: 283814 Color: 8
Size: 271507 Color: 16

Bin 1889: 32 of cap free
Amount of items: 2
Items: 
Size: 515517 Color: 2
Size: 484452 Color: 7

Bin 1890: 32 of cap free
Amount of items: 2
Items: 
Size: 522321 Color: 17
Size: 477648 Color: 8

Bin 1891: 32 of cap free
Amount of items: 3
Items: 
Size: 532299 Color: 3
Size: 244400 Color: 14
Size: 223270 Color: 5

Bin 1892: 32 of cap free
Amount of items: 2
Items: 
Size: 553195 Color: 1
Size: 446774 Color: 9

Bin 1893: 32 of cap free
Amount of items: 2
Items: 
Size: 563022 Color: 18
Size: 436947 Color: 4

Bin 1894: 32 of cap free
Amount of items: 2
Items: 
Size: 570172 Color: 16
Size: 429797 Color: 12

Bin 1895: 32 of cap free
Amount of items: 2
Items: 
Size: 580596 Color: 4
Size: 419373 Color: 1

Bin 1896: 32 of cap free
Amount of items: 2
Items: 
Size: 584915 Color: 7
Size: 415054 Color: 12

Bin 1897: 32 of cap free
Amount of items: 2
Items: 
Size: 588766 Color: 11
Size: 411203 Color: 5

Bin 1898: 32 of cap free
Amount of items: 2
Items: 
Size: 593697 Color: 9
Size: 406272 Color: 8

Bin 1899: 32 of cap free
Amount of items: 2
Items: 
Size: 593916 Color: 0
Size: 406053 Color: 18

Bin 1900: 32 of cap free
Amount of items: 2
Items: 
Size: 598139 Color: 2
Size: 401830 Color: 7

Bin 1901: 32 of cap free
Amount of items: 2
Items: 
Size: 619118 Color: 14
Size: 380851 Color: 15

Bin 1902: 32 of cap free
Amount of items: 2
Items: 
Size: 627422 Color: 17
Size: 372547 Color: 19

Bin 1903: 32 of cap free
Amount of items: 2
Items: 
Size: 630300 Color: 11
Size: 369669 Color: 13

Bin 1904: 32 of cap free
Amount of items: 2
Items: 
Size: 638268 Color: 12
Size: 361701 Color: 11

Bin 1905: 32 of cap free
Amount of items: 2
Items: 
Size: 647663 Color: 19
Size: 352306 Color: 13

Bin 1906: 32 of cap free
Amount of items: 2
Items: 
Size: 680733 Color: 14
Size: 319236 Color: 11

Bin 1907: 32 of cap free
Amount of items: 2
Items: 
Size: 684961 Color: 4
Size: 315008 Color: 19

Bin 1908: 32 of cap free
Amount of items: 2
Items: 
Size: 740667 Color: 10
Size: 259302 Color: 15

Bin 1909: 32 of cap free
Amount of items: 2
Items: 
Size: 785596 Color: 0
Size: 214373 Color: 14

Bin 1910: 33 of cap free
Amount of items: 3
Items: 
Size: 734874 Color: 17
Size: 135910 Color: 6
Size: 129184 Color: 19

Bin 1911: 33 of cap free
Amount of items: 3
Items: 
Size: 746926 Color: 12
Size: 126847 Color: 19
Size: 126195 Color: 10

Bin 1912: 33 of cap free
Amount of items: 2
Items: 
Size: 677037 Color: 5
Size: 322931 Color: 14

Bin 1913: 33 of cap free
Amount of items: 2
Items: 
Size: 709475 Color: 9
Size: 290493 Color: 1

Bin 1914: 33 of cap free
Amount of items: 3
Items: 
Size: 492336 Color: 10
Size: 262062 Color: 11
Size: 245570 Color: 12

Bin 1915: 33 of cap free
Amount of items: 3
Items: 
Size: 722947 Color: 5
Size: 138615 Color: 8
Size: 138406 Color: 16

Bin 1916: 33 of cap free
Amount of items: 3
Items: 
Size: 378160 Color: 3
Size: 357332 Color: 4
Size: 264476 Color: 15

Bin 1917: 33 of cap free
Amount of items: 2
Items: 
Size: 770170 Color: 6
Size: 229798 Color: 2

Bin 1918: 33 of cap free
Amount of items: 2
Items: 
Size: 696867 Color: 8
Size: 303101 Color: 16

Bin 1919: 33 of cap free
Amount of items: 3
Items: 
Size: 371422 Color: 1
Size: 329833 Color: 18
Size: 298713 Color: 0

Bin 1920: 33 of cap free
Amount of items: 2
Items: 
Size: 536531 Color: 5
Size: 463437 Color: 16

Bin 1921: 33 of cap free
Amount of items: 2
Items: 
Size: 546468 Color: 8
Size: 453500 Color: 9

Bin 1922: 33 of cap free
Amount of items: 2
Items: 
Size: 549266 Color: 7
Size: 450702 Color: 8

Bin 1923: 33 of cap free
Amount of items: 2
Items: 
Size: 567574 Color: 19
Size: 432394 Color: 9

Bin 1924: 33 of cap free
Amount of items: 2
Items: 
Size: 620237 Color: 9
Size: 379731 Color: 16

Bin 1925: 33 of cap free
Amount of items: 2
Items: 
Size: 622369 Color: 0
Size: 377599 Color: 13

Bin 1926: 33 of cap free
Amount of items: 2
Items: 
Size: 645596 Color: 18
Size: 354372 Color: 5

Bin 1927: 33 of cap free
Amount of items: 2
Items: 
Size: 678364 Color: 10
Size: 321604 Color: 8

Bin 1928: 33 of cap free
Amount of items: 2
Items: 
Size: 687955 Color: 18
Size: 312013 Color: 1

Bin 1929: 33 of cap free
Amount of items: 2
Items: 
Size: 759104 Color: 1
Size: 240864 Color: 4

Bin 1930: 33 of cap free
Amount of items: 2
Items: 
Size: 771875 Color: 6
Size: 228093 Color: 11

Bin 1931: 33 of cap free
Amount of items: 2
Items: 
Size: 788869 Color: 7
Size: 211099 Color: 0

Bin 1932: 33 of cap free
Amount of items: 2
Items: 
Size: 798675 Color: 14
Size: 201293 Color: 2

Bin 1933: 34 of cap free
Amount of items: 2
Items: 
Size: 704654 Color: 18
Size: 295313 Color: 13

Bin 1934: 34 of cap free
Amount of items: 2
Items: 
Size: 502847 Color: 4
Size: 497120 Color: 7

Bin 1935: 34 of cap free
Amount of items: 2
Items: 
Size: 769195 Color: 3
Size: 230772 Color: 14

Bin 1936: 34 of cap free
Amount of items: 2
Items: 
Size: 649931 Color: 14
Size: 350036 Color: 10

Bin 1937: 34 of cap free
Amount of items: 2
Items: 
Size: 692303 Color: 0
Size: 307664 Color: 9

Bin 1938: 34 of cap free
Amount of items: 2
Items: 
Size: 795472 Color: 9
Size: 204495 Color: 17

Bin 1939: 34 of cap free
Amount of items: 2
Items: 
Size: 609212 Color: 12
Size: 390755 Color: 16

Bin 1940: 34 of cap free
Amount of items: 2
Items: 
Size: 500394 Color: 14
Size: 499573 Color: 7

Bin 1941: 34 of cap free
Amount of items: 2
Items: 
Size: 513030 Color: 2
Size: 486937 Color: 9

Bin 1942: 34 of cap free
Amount of items: 2
Items: 
Size: 526453 Color: 16
Size: 473514 Color: 13

Bin 1943: 34 of cap free
Amount of items: 2
Items: 
Size: 528499 Color: 5
Size: 471468 Color: 6

Bin 1944: 34 of cap free
Amount of items: 2
Items: 
Size: 530832 Color: 9
Size: 469135 Color: 19

Bin 1945: 34 of cap free
Amount of items: 2
Items: 
Size: 534346 Color: 4
Size: 465621 Color: 15

Bin 1946: 34 of cap free
Amount of items: 2
Items: 
Size: 536696 Color: 8
Size: 463271 Color: 1

Bin 1947: 34 of cap free
Amount of items: 2
Items: 
Size: 537380 Color: 12
Size: 462587 Color: 14

Bin 1948: 34 of cap free
Amount of items: 3
Items: 
Size: 545016 Color: 18
Size: 232228 Color: 6
Size: 222723 Color: 1

Bin 1949: 34 of cap free
Amount of items: 2
Items: 
Size: 555866 Color: 12
Size: 444101 Color: 9

Bin 1950: 34 of cap free
Amount of items: 2
Items: 
Size: 566446 Color: 13
Size: 433521 Color: 8

Bin 1951: 34 of cap free
Amount of items: 2
Items: 
Size: 589714 Color: 2
Size: 410253 Color: 8

Bin 1952: 34 of cap free
Amount of items: 3
Items: 
Size: 618806 Color: 1
Size: 190584 Color: 14
Size: 190577 Color: 1

Bin 1953: 34 of cap free
Amount of items: 2
Items: 
Size: 623307 Color: 16
Size: 376660 Color: 11

Bin 1954: 34 of cap free
Amount of items: 2
Items: 
Size: 663109 Color: 14
Size: 336858 Color: 15

Bin 1955: 34 of cap free
Amount of items: 2
Items: 
Size: 676698 Color: 7
Size: 323269 Color: 11

Bin 1956: 34 of cap free
Amount of items: 2
Items: 
Size: 684251 Color: 4
Size: 315716 Color: 3

Bin 1957: 34 of cap free
Amount of items: 2
Items: 
Size: 685651 Color: 6
Size: 314316 Color: 12

Bin 1958: 34 of cap free
Amount of items: 2
Items: 
Size: 687992 Color: 15
Size: 311975 Color: 7

Bin 1959: 34 of cap free
Amount of items: 2
Items: 
Size: 700888 Color: 11
Size: 299079 Color: 19

Bin 1960: 34 of cap free
Amount of items: 2
Items: 
Size: 748916 Color: 3
Size: 251051 Color: 10

Bin 1961: 34 of cap free
Amount of items: 2
Items: 
Size: 779607 Color: 14
Size: 220360 Color: 8

Bin 1962: 34 of cap free
Amount of items: 2
Items: 
Size: 794458 Color: 2
Size: 205509 Color: 8

Bin 1963: 35 of cap free
Amount of items: 3
Items: 
Size: 667959 Color: 19
Size: 167036 Color: 7
Size: 164971 Color: 9

Bin 1964: 35 of cap free
Amount of items: 2
Items: 
Size: 763836 Color: 4
Size: 236130 Color: 5

Bin 1965: 35 of cap free
Amount of items: 2
Items: 
Size: 742264 Color: 3
Size: 257702 Color: 2

Bin 1966: 35 of cap free
Amount of items: 3
Items: 
Size: 630686 Color: 8
Size: 184642 Color: 11
Size: 184638 Color: 1

Bin 1967: 35 of cap free
Amount of items: 2
Items: 
Size: 759433 Color: 13
Size: 240533 Color: 14

Bin 1968: 35 of cap free
Amount of items: 2
Items: 
Size: 592390 Color: 0
Size: 407576 Color: 14

Bin 1969: 35 of cap free
Amount of items: 2
Items: 
Size: 791301 Color: 10
Size: 208665 Color: 17

Bin 1970: 35 of cap free
Amount of items: 2
Items: 
Size: 701839 Color: 10
Size: 298127 Color: 6

Bin 1971: 35 of cap free
Amount of items: 2
Items: 
Size: 562443 Color: 15
Size: 437523 Color: 3

Bin 1972: 35 of cap free
Amount of items: 2
Items: 
Size: 511701 Color: 4
Size: 488265 Color: 1

Bin 1973: 35 of cap free
Amount of items: 2
Items: 
Size: 517775 Color: 1
Size: 482191 Color: 3

Bin 1974: 35 of cap free
Amount of items: 2
Items: 
Size: 548220 Color: 10
Size: 451746 Color: 16

Bin 1975: 35 of cap free
Amount of items: 2
Items: 
Size: 559942 Color: 10
Size: 440024 Color: 17

Bin 1976: 35 of cap free
Amount of items: 2
Items: 
Size: 560897 Color: 16
Size: 439069 Color: 18

Bin 1977: 35 of cap free
Amount of items: 2
Items: 
Size: 580993 Color: 19
Size: 418973 Color: 10

Bin 1978: 35 of cap free
Amount of items: 2
Items: 
Size: 603234 Color: 17
Size: 396732 Color: 15

Bin 1979: 35 of cap free
Amount of items: 2
Items: 
Size: 608238 Color: 14
Size: 391728 Color: 13

Bin 1980: 35 of cap free
Amount of items: 2
Items: 
Size: 654052 Color: 13
Size: 345914 Color: 18

Bin 1981: 35 of cap free
Amount of items: 2
Items: 
Size: 687023 Color: 8
Size: 312943 Color: 0

Bin 1982: 35 of cap free
Amount of items: 3
Items: 
Size: 718017 Color: 4
Size: 141354 Color: 7
Size: 140595 Color: 8

Bin 1983: 35 of cap free
Amount of items: 2
Items: 
Size: 741850 Color: 10
Size: 258116 Color: 2

Bin 1984: 35 of cap free
Amount of items: 2
Items: 
Size: 749799 Color: 1
Size: 250167 Color: 2

Bin 1985: 35 of cap free
Amount of items: 2
Items: 
Size: 753622 Color: 4
Size: 246344 Color: 12

Bin 1986: 36 of cap free
Amount of items: 3
Items: 
Size: 613555 Color: 14
Size: 226360 Color: 16
Size: 160050 Color: 17

Bin 1987: 36 of cap free
Amount of items: 3
Items: 
Size: 667320 Color: 14
Size: 179696 Color: 15
Size: 152949 Color: 11

Bin 1988: 36 of cap free
Amount of items: 2
Items: 
Size: 782285 Color: 15
Size: 217680 Color: 19

Bin 1989: 36 of cap free
Amount of items: 3
Items: 
Size: 793663 Color: 18
Size: 103648 Color: 16
Size: 102654 Color: 12

Bin 1990: 36 of cap free
Amount of items: 3
Items: 
Size: 543623 Color: 16
Size: 231202 Color: 3
Size: 225140 Color: 12

Bin 1991: 36 of cap free
Amount of items: 2
Items: 
Size: 781341 Color: 11
Size: 218624 Color: 6

Bin 1992: 36 of cap free
Amount of items: 2
Items: 
Size: 719688 Color: 17
Size: 280277 Color: 15

Bin 1993: 36 of cap free
Amount of items: 2
Items: 
Size: 725725 Color: 16
Size: 274240 Color: 19

Bin 1994: 36 of cap free
Amount of items: 3
Items: 
Size: 369493 Color: 5
Size: 319606 Color: 2
Size: 310866 Color: 9

Bin 1995: 36 of cap free
Amount of items: 3
Items: 
Size: 362360 Color: 6
Size: 338361 Color: 15
Size: 299244 Color: 11

Bin 1996: 36 of cap free
Amount of items: 3
Items: 
Size: 521608 Color: 10
Size: 245129 Color: 8
Size: 233228 Color: 9

Bin 1997: 36 of cap free
Amount of items: 2
Items: 
Size: 730779 Color: 8
Size: 269186 Color: 17

Bin 1998: 36 of cap free
Amount of items: 2
Items: 
Size: 666949 Color: 15
Size: 333016 Color: 5

Bin 1999: 36 of cap free
Amount of items: 2
Items: 
Size: 562291 Color: 4
Size: 437674 Color: 8

Bin 2000: 36 of cap free
Amount of items: 2
Items: 
Size: 721957 Color: 1
Size: 278008 Color: 16

Bin 2001: 36 of cap free
Amount of items: 2
Items: 
Size: 529983 Color: 14
Size: 469982 Color: 17

Bin 2002: 36 of cap free
Amount of items: 2
Items: 
Size: 524030 Color: 10
Size: 475935 Color: 3

Bin 2003: 36 of cap free
Amount of items: 2
Items: 
Size: 548860 Color: 11
Size: 451105 Color: 14

Bin 2004: 36 of cap free
Amount of items: 2
Items: 
Size: 564687 Color: 4
Size: 435278 Color: 5

Bin 2005: 36 of cap free
Amount of items: 2
Items: 
Size: 578971 Color: 19
Size: 420994 Color: 14

Bin 2006: 36 of cap free
Amount of items: 2
Items: 
Size: 588862 Color: 10
Size: 411103 Color: 16

Bin 2007: 36 of cap free
Amount of items: 2
Items: 
Size: 592537 Color: 6
Size: 407428 Color: 8

Bin 2008: 36 of cap free
Amount of items: 2
Items: 
Size: 600005 Color: 18
Size: 399960 Color: 8

Bin 2009: 36 of cap free
Amount of items: 2
Items: 
Size: 615352 Color: 18
Size: 384613 Color: 11

Bin 2010: 36 of cap free
Amount of items: 2
Items: 
Size: 640402 Color: 14
Size: 359563 Color: 10

Bin 2011: 36 of cap free
Amount of items: 2
Items: 
Size: 656324 Color: 1
Size: 343641 Color: 12

Bin 2012: 36 of cap free
Amount of items: 3
Items: 
Size: 661367 Color: 18
Size: 169836 Color: 0
Size: 168762 Color: 5

Bin 2013: 36 of cap free
Amount of items: 2
Items: 
Size: 678598 Color: 9
Size: 321367 Color: 10

Bin 2014: 36 of cap free
Amount of items: 2
Items: 
Size: 679073 Color: 8
Size: 320892 Color: 3

Bin 2015: 36 of cap free
Amount of items: 2
Items: 
Size: 683830 Color: 17
Size: 316135 Color: 13

Bin 2016: 36 of cap free
Amount of items: 2
Items: 
Size: 688108 Color: 11
Size: 311857 Color: 9

Bin 2017: 36 of cap free
Amount of items: 2
Items: 
Size: 709273 Color: 11
Size: 290692 Color: 0

Bin 2018: 36 of cap free
Amount of items: 2
Items: 
Size: 780437 Color: 1
Size: 219528 Color: 19

Bin 2019: 36 of cap free
Amount of items: 2
Items: 
Size: 782894 Color: 13
Size: 217071 Color: 8

Bin 2020: 36 of cap free
Amount of items: 2
Items: 
Size: 786723 Color: 7
Size: 213242 Color: 3

Bin 2021: 37 of cap free
Amount of items: 2
Items: 
Size: 722905 Color: 6
Size: 277059 Color: 13

Bin 2022: 37 of cap free
Amount of items: 2
Items: 
Size: 655568 Color: 14
Size: 344396 Color: 10

Bin 2023: 37 of cap free
Amount of items: 3
Items: 
Size: 762757 Color: 9
Size: 122101 Color: 1
Size: 115106 Color: 9

Bin 2024: 37 of cap free
Amount of items: 2
Items: 
Size: 710906 Color: 15
Size: 289058 Color: 1

Bin 2025: 37 of cap free
Amount of items: 2
Items: 
Size: 687667 Color: 7
Size: 312297 Color: 18

Bin 2026: 37 of cap free
Amount of items: 2
Items: 
Size: 511200 Color: 16
Size: 488764 Color: 6

Bin 2027: 37 of cap free
Amount of items: 2
Items: 
Size: 769718 Color: 16
Size: 230246 Color: 1

Bin 2028: 37 of cap free
Amount of items: 2
Items: 
Size: 507072 Color: 2
Size: 492892 Color: 6

Bin 2029: 37 of cap free
Amount of items: 2
Items: 
Size: 501072 Color: 15
Size: 498892 Color: 0

Bin 2030: 37 of cap free
Amount of items: 2
Items: 
Size: 511476 Color: 17
Size: 488488 Color: 18

Bin 2031: 37 of cap free
Amount of items: 2
Items: 
Size: 607787 Color: 1
Size: 392177 Color: 9

Bin 2032: 37 of cap free
Amount of items: 2
Items: 
Size: 612434 Color: 0
Size: 387530 Color: 3

Bin 2033: 37 of cap free
Amount of items: 2
Items: 
Size: 637660 Color: 16
Size: 362304 Color: 19

Bin 2034: 37 of cap free
Amount of items: 2
Items: 
Size: 644257 Color: 8
Size: 355707 Color: 3

Bin 2035: 38 of cap free
Amount of items: 2
Items: 
Size: 722190 Color: 16
Size: 277773 Color: 9

Bin 2036: 38 of cap free
Amount of items: 3
Items: 
Size: 686295 Color: 6
Size: 156879 Color: 14
Size: 156789 Color: 8

Bin 2037: 38 of cap free
Amount of items: 2
Items: 
Size: 661925 Color: 1
Size: 338038 Color: 7

Bin 2038: 38 of cap free
Amount of items: 2
Items: 
Size: 689906 Color: 4
Size: 310057 Color: 9

Bin 2039: 38 of cap free
Amount of items: 2
Items: 
Size: 763601 Color: 9
Size: 236362 Color: 0

Bin 2040: 38 of cap free
Amount of items: 2
Items: 
Size: 780597 Color: 17
Size: 219366 Color: 7

Bin 2041: 38 of cap free
Amount of items: 2
Items: 
Size: 541498 Color: 15
Size: 458465 Color: 0

Bin 2042: 38 of cap free
Amount of items: 2
Items: 
Size: 567328 Color: 1
Size: 432635 Color: 0

Bin 2043: 38 of cap free
Amount of items: 2
Items: 
Size: 569101 Color: 0
Size: 430862 Color: 2

Bin 2044: 38 of cap free
Amount of items: 2
Items: 
Size: 580050 Color: 3
Size: 419913 Color: 13

Bin 2045: 38 of cap free
Amount of items: 2
Items: 
Size: 629588 Color: 9
Size: 370375 Color: 3

Bin 2046: 38 of cap free
Amount of items: 2
Items: 
Size: 653576 Color: 16
Size: 346387 Color: 17

Bin 2047: 38 of cap free
Amount of items: 2
Items: 
Size: 690842 Color: 5
Size: 309121 Color: 9

Bin 2048: 38 of cap free
Amount of items: 2
Items: 
Size: 767167 Color: 18
Size: 232796 Color: 14

Bin 2049: 38 of cap free
Amount of items: 2
Items: 
Size: 789827 Color: 10
Size: 210136 Color: 12

Bin 2050: 39 of cap free
Amount of items: 2
Items: 
Size: 683194 Color: 18
Size: 316768 Color: 19

Bin 2051: 39 of cap free
Amount of items: 2
Items: 
Size: 621398 Color: 2
Size: 378564 Color: 9

Bin 2052: 39 of cap free
Amount of items: 3
Items: 
Size: 732559 Color: 1
Size: 133736 Color: 0
Size: 133667 Color: 19

Bin 2053: 39 of cap free
Amount of items: 3
Items: 
Size: 605088 Color: 9
Size: 208164 Color: 14
Size: 186710 Color: 10

Bin 2054: 39 of cap free
Amount of items: 2
Items: 
Size: 633113 Color: 9
Size: 366849 Color: 19

Bin 2055: 39 of cap free
Amount of items: 2
Items: 
Size: 795267 Color: 16
Size: 204695 Color: 11

Bin 2056: 39 of cap free
Amount of items: 2
Items: 
Size: 698587 Color: 1
Size: 301375 Color: 17

Bin 2057: 39 of cap free
Amount of items: 2
Items: 
Size: 554912 Color: 3
Size: 445050 Color: 9

Bin 2058: 39 of cap free
Amount of items: 2
Items: 
Size: 520328 Color: 18
Size: 479634 Color: 14

Bin 2059: 39 of cap free
Amount of items: 2
Items: 
Size: 521906 Color: 16
Size: 478056 Color: 3

Bin 2060: 39 of cap free
Amount of items: 2
Items: 
Size: 525011 Color: 12
Size: 474951 Color: 7

Bin 2061: 39 of cap free
Amount of items: 2
Items: 
Size: 566026 Color: 4
Size: 433936 Color: 8

Bin 2062: 39 of cap free
Amount of items: 2
Items: 
Size: 572252 Color: 16
Size: 427710 Color: 19

Bin 2063: 39 of cap free
Amount of items: 2
Items: 
Size: 593550 Color: 11
Size: 406412 Color: 4

Bin 2064: 39 of cap free
Amount of items: 2
Items: 
Size: 596668 Color: 9
Size: 403294 Color: 19

Bin 2065: 39 of cap free
Amount of items: 2
Items: 
Size: 623474 Color: 16
Size: 376488 Color: 12

Bin 2066: 39 of cap free
Amount of items: 2
Items: 
Size: 631880 Color: 8
Size: 368082 Color: 15

Bin 2067: 39 of cap free
Amount of items: 2
Items: 
Size: 653189 Color: 19
Size: 346773 Color: 18

Bin 2068: 39 of cap free
Amount of items: 2
Items: 
Size: 671116 Color: 3
Size: 328846 Color: 18

Bin 2069: 39 of cap free
Amount of items: 2
Items: 
Size: 694794 Color: 0
Size: 305168 Color: 17

Bin 2070: 39 of cap free
Amount of items: 2
Items: 
Size: 702039 Color: 15
Size: 297923 Color: 3

Bin 2071: 39 of cap free
Amount of items: 2
Items: 
Size: 717170 Color: 14
Size: 282792 Color: 12

Bin 2072: 39 of cap free
Amount of items: 2
Items: 
Size: 726023 Color: 6
Size: 273939 Color: 4

Bin 2073: 39 of cap free
Amount of items: 2
Items: 
Size: 745850 Color: 13
Size: 254112 Color: 6

Bin 2074: 39 of cap free
Amount of items: 2
Items: 
Size: 752245 Color: 8
Size: 247717 Color: 9

Bin 2075: 39 of cap free
Amount of items: 2
Items: 
Size: 779971 Color: 11
Size: 219991 Color: 0

Bin 2076: 39 of cap free
Amount of items: 2
Items: 
Size: 787136 Color: 10
Size: 212826 Color: 11

Bin 2077: 40 of cap free
Amount of items: 3
Items: 
Size: 747188 Color: 11
Size: 140049 Color: 12
Size: 112724 Color: 7

Bin 2078: 40 of cap free
Amount of items: 3
Items: 
Size: 638004 Color: 13
Size: 180984 Color: 6
Size: 180973 Color: 11

Bin 2079: 40 of cap free
Amount of items: 2
Items: 
Size: 778648 Color: 1
Size: 221313 Color: 6

Bin 2080: 40 of cap free
Amount of items: 2
Items: 
Size: 727866 Color: 13
Size: 272095 Color: 18

Bin 2081: 40 of cap free
Amount of items: 3
Items: 
Size: 710981 Color: 6
Size: 145443 Color: 17
Size: 143537 Color: 14

Bin 2082: 40 of cap free
Amount of items: 2
Items: 
Size: 677603 Color: 15
Size: 322358 Color: 9

Bin 2083: 40 of cap free
Amount of items: 2
Items: 
Size: 656643 Color: 1
Size: 343318 Color: 4

Bin 2084: 40 of cap free
Amount of items: 2
Items: 
Size: 667015 Color: 1
Size: 332946 Color: 8

Bin 2085: 40 of cap free
Amount of items: 2
Items: 
Size: 506076 Color: 6
Size: 493885 Color: 5

Bin 2086: 40 of cap free
Amount of items: 2
Items: 
Size: 520718 Color: 15
Size: 479243 Color: 14

Bin 2087: 40 of cap free
Amount of items: 2
Items: 
Size: 551540 Color: 14
Size: 448421 Color: 1

Bin 2088: 40 of cap free
Amount of items: 2
Items: 
Size: 592940 Color: 7
Size: 407021 Color: 15

Bin 2089: 40 of cap free
Amount of items: 2
Items: 
Size: 509703 Color: 8
Size: 490258 Color: 5

Bin 2090: 40 of cap free
Amount of items: 2
Items: 
Size: 559549 Color: 0
Size: 440412 Color: 9

Bin 2091: 40 of cap free
Amount of items: 2
Items: 
Size: 596397 Color: 12
Size: 403564 Color: 17

Bin 2092: 40 of cap free
Amount of items: 2
Items: 
Size: 634428 Color: 16
Size: 365533 Color: 5

Bin 2093: 40 of cap free
Amount of items: 2
Items: 
Size: 641490 Color: 17
Size: 358471 Color: 19

Bin 2094: 40 of cap free
Amount of items: 2
Items: 
Size: 645964 Color: 8
Size: 353997 Color: 1

Bin 2095: 40 of cap free
Amount of items: 2
Items: 
Size: 673948 Color: 5
Size: 326013 Color: 10

Bin 2096: 40 of cap free
Amount of items: 2
Items: 
Size: 682897 Color: 2
Size: 317064 Color: 18

Bin 2097: 40 of cap free
Amount of items: 2
Items: 
Size: 728630 Color: 18
Size: 271331 Color: 17

Bin 2098: 40 of cap free
Amount of items: 2
Items: 
Size: 750794 Color: 2
Size: 249167 Color: 18

Bin 2099: 41 of cap free
Amount of items: 2
Items: 
Size: 682345 Color: 8
Size: 317615 Color: 0

Bin 2100: 41 of cap free
Amount of items: 2
Items: 
Size: 782571 Color: 9
Size: 217389 Color: 1

Bin 2101: 41 of cap free
Amount of items: 2
Items: 
Size: 617657 Color: 13
Size: 382303 Color: 6

Bin 2102: 41 of cap free
Amount of items: 2
Items: 
Size: 690428 Color: 18
Size: 309532 Color: 1

Bin 2103: 41 of cap free
Amount of items: 2
Items: 
Size: 763825 Color: 5
Size: 236135 Color: 10

Bin 2104: 41 of cap free
Amount of items: 2
Items: 
Size: 651419 Color: 5
Size: 348541 Color: 19

Bin 2105: 41 of cap free
Amount of items: 2
Items: 
Size: 586484 Color: 17
Size: 413476 Color: 5

Bin 2106: 41 of cap free
Amount of items: 2
Items: 
Size: 636480 Color: 2
Size: 363480 Color: 15

Bin 2107: 41 of cap free
Amount of items: 2
Items: 
Size: 503463 Color: 5
Size: 496497 Color: 1

Bin 2108: 41 of cap free
Amount of items: 2
Items: 
Size: 520238 Color: 2
Size: 479722 Color: 1

Bin 2109: 41 of cap free
Amount of items: 2
Items: 
Size: 521436 Color: 15
Size: 478524 Color: 6

Bin 2110: 41 of cap free
Amount of items: 2
Items: 
Size: 524255 Color: 12
Size: 475705 Color: 0

Bin 2111: 41 of cap free
Amount of items: 2
Items: 
Size: 526320 Color: 6
Size: 473640 Color: 18

Bin 2112: 41 of cap free
Amount of items: 2
Items: 
Size: 540507 Color: 13
Size: 459453 Color: 0

Bin 2113: 41 of cap free
Amount of items: 2
Items: 
Size: 558927 Color: 7
Size: 441033 Color: 8

Bin 2114: 41 of cap free
Amount of items: 2
Items: 
Size: 564836 Color: 5
Size: 435124 Color: 19

Bin 2115: 41 of cap free
Amount of items: 2
Items: 
Size: 565727 Color: 2
Size: 434233 Color: 17

Bin 2116: 41 of cap free
Amount of items: 2
Items: 
Size: 581879 Color: 11
Size: 418081 Color: 6

Bin 2117: 41 of cap free
Amount of items: 2
Items: 
Size: 585157 Color: 14
Size: 414803 Color: 7

Bin 2118: 41 of cap free
Amount of items: 2
Items: 
Size: 607971 Color: 2
Size: 391989 Color: 7

Bin 2119: 41 of cap free
Amount of items: 2
Items: 
Size: 666580 Color: 17
Size: 333380 Color: 12

Bin 2120: 41 of cap free
Amount of items: 2
Items: 
Size: 703552 Color: 2
Size: 296408 Color: 4

Bin 2121: 41 of cap free
Amount of items: 2
Items: 
Size: 760815 Color: 2
Size: 239145 Color: 12

Bin 2122: 41 of cap free
Amount of items: 2
Items: 
Size: 763243 Color: 14
Size: 236717 Color: 12

Bin 2123: 42 of cap free
Amount of items: 3
Items: 
Size: 539677 Color: 13
Size: 231501 Color: 7
Size: 228781 Color: 2

Bin 2124: 42 of cap free
Amount of items: 2
Items: 
Size: 765891 Color: 10
Size: 234068 Color: 0

Bin 2125: 42 of cap free
Amount of items: 3
Items: 
Size: 711985 Color: 10
Size: 145955 Color: 11
Size: 142019 Color: 2

Bin 2126: 42 of cap free
Amount of items: 2
Items: 
Size: 695543 Color: 11
Size: 304416 Color: 19

Bin 2127: 42 of cap free
Amount of items: 2
Items: 
Size: 722753 Color: 5
Size: 277206 Color: 14

Bin 2128: 42 of cap free
Amount of items: 2
Items: 
Size: 696396 Color: 3
Size: 303563 Color: 8

Bin 2129: 42 of cap free
Amount of items: 2
Items: 
Size: 739132 Color: 19
Size: 260827 Color: 7

Bin 2130: 42 of cap free
Amount of items: 2
Items: 
Size: 777874 Color: 9
Size: 222085 Color: 3

Bin 2131: 42 of cap free
Amount of items: 4
Items: 
Size: 573745 Color: 6
Size: 169431 Color: 19
Size: 135618 Color: 7
Size: 121165 Color: 10

Bin 2132: 42 of cap free
Amount of items: 2
Items: 
Size: 737814 Color: 12
Size: 262145 Color: 18

Bin 2133: 42 of cap free
Amount of items: 2
Items: 
Size: 790255 Color: 6
Size: 209704 Color: 16

Bin 2134: 42 of cap free
Amount of items: 2
Items: 
Size: 746902 Color: 2
Size: 253057 Color: 12

Bin 2135: 42 of cap free
Amount of items: 2
Items: 
Size: 653650 Color: 9
Size: 346309 Color: 16

Bin 2136: 42 of cap free
Amount of items: 3
Items: 
Size: 373872 Color: 7
Size: 343556 Color: 1
Size: 282531 Color: 6

Bin 2137: 42 of cap free
Amount of items: 2
Items: 
Size: 500728 Color: 9
Size: 499231 Color: 1

Bin 2138: 42 of cap free
Amount of items: 2
Items: 
Size: 523026 Color: 9
Size: 476933 Color: 3

Bin 2139: 42 of cap free
Amount of items: 2
Items: 
Size: 538932 Color: 0
Size: 461027 Color: 12

Bin 2140: 42 of cap free
Amount of items: 2
Items: 
Size: 544159 Color: 19
Size: 455800 Color: 5

Bin 2141: 42 of cap free
Amount of items: 2
Items: 
Size: 561177 Color: 2
Size: 438782 Color: 18

Bin 2142: 42 of cap free
Amount of items: 2
Items: 
Size: 570073 Color: 18
Size: 429886 Color: 9

Bin 2143: 42 of cap free
Amount of items: 2
Items: 
Size: 583196 Color: 14
Size: 416763 Color: 15

Bin 2144: 42 of cap free
Amount of items: 2
Items: 
Size: 628350 Color: 15
Size: 371609 Color: 18

Bin 2145: 42 of cap free
Amount of items: 2
Items: 
Size: 682425 Color: 8
Size: 317534 Color: 14

Bin 2146: 42 of cap free
Amount of items: 3
Items: 
Size: 691159 Color: 16
Size: 154584 Color: 17
Size: 154216 Color: 8

Bin 2147: 42 of cap free
Amount of items: 2
Items: 
Size: 733088 Color: 18
Size: 266871 Color: 4

Bin 2148: 42 of cap free
Amount of items: 2
Items: 
Size: 769813 Color: 17
Size: 230146 Color: 15

Bin 2149: 42 of cap free
Amount of items: 2
Items: 
Size: 778908 Color: 17
Size: 221051 Color: 18

Bin 2150: 42 of cap free
Amount of items: 2
Items: 
Size: 780542 Color: 14
Size: 219417 Color: 5

Bin 2151: 43 of cap free
Amount of items: 3
Items: 
Size: 539911 Color: 10
Size: 299503 Color: 18
Size: 160544 Color: 4

Bin 2152: 43 of cap free
Amount of items: 3
Items: 
Size: 755178 Color: 4
Size: 124811 Color: 9
Size: 119969 Color: 12

Bin 2153: 43 of cap free
Amount of items: 2
Items: 
Size: 787436 Color: 9
Size: 212522 Color: 3

Bin 2154: 43 of cap free
Amount of items: 2
Items: 
Size: 726284 Color: 16
Size: 273674 Color: 1

Bin 2155: 43 of cap free
Amount of items: 2
Items: 
Size: 727865 Color: 19
Size: 272093 Color: 18

Bin 2156: 43 of cap free
Amount of items: 3
Items: 
Size: 750076 Color: 0
Size: 125751 Color: 15
Size: 124131 Color: 4

Bin 2157: 43 of cap free
Amount of items: 2
Items: 
Size: 555090 Color: 0
Size: 444868 Color: 17

Bin 2158: 43 of cap free
Amount of items: 2
Items: 
Size: 756728 Color: 13
Size: 243230 Color: 16

Bin 2159: 43 of cap free
Amount of items: 2
Items: 
Size: 723896 Color: 6
Size: 276062 Color: 14

Bin 2160: 43 of cap free
Amount of items: 2
Items: 
Size: 522966 Color: 5
Size: 476992 Color: 11

Bin 2161: 43 of cap free
Amount of items: 2
Items: 
Size: 586436 Color: 11
Size: 413522 Color: 3

Bin 2162: 43 of cap free
Amount of items: 2
Items: 
Size: 510137 Color: 15
Size: 489821 Color: 6

Bin 2163: 43 of cap free
Amount of items: 2
Items: 
Size: 551333 Color: 6
Size: 448625 Color: 15

Bin 2164: 43 of cap free
Amount of items: 2
Items: 
Size: 582017 Color: 6
Size: 417941 Color: 14

Bin 2165: 43 of cap free
Amount of items: 2
Items: 
Size: 595553 Color: 6
Size: 404405 Color: 18

Bin 2166: 43 of cap free
Amount of items: 2
Items: 
Size: 618330 Color: 12
Size: 381628 Color: 3

Bin 2167: 43 of cap free
Amount of items: 2
Items: 
Size: 654492 Color: 10
Size: 345466 Color: 11

Bin 2168: 43 of cap free
Amount of items: 2
Items: 
Size: 677935 Color: 9
Size: 322023 Color: 17

Bin 2169: 44 of cap free
Amount of items: 3
Items: 
Size: 679858 Color: 13
Size: 160621 Color: 15
Size: 159478 Color: 11

Bin 2170: 44 of cap free
Amount of items: 2
Items: 
Size: 700281 Color: 16
Size: 299676 Color: 6

Bin 2171: 44 of cap free
Amount of items: 3
Items: 
Size: 712347 Color: 12
Size: 144029 Color: 18
Size: 143581 Color: 19

Bin 2172: 44 of cap free
Amount of items: 2
Items: 
Size: 614970 Color: 15
Size: 384987 Color: 11

Bin 2173: 44 of cap free
Amount of items: 2
Items: 
Size: 668052 Color: 13
Size: 331905 Color: 1

Bin 2174: 44 of cap free
Amount of items: 3
Items: 
Size: 407882 Color: 15
Size: 339886 Color: 0
Size: 252189 Color: 16

Bin 2175: 44 of cap free
Amount of items: 2
Items: 
Size: 784061 Color: 8
Size: 215896 Color: 16

Bin 2176: 44 of cap free
Amount of items: 2
Items: 
Size: 531716 Color: 10
Size: 468241 Color: 7

Bin 2177: 44 of cap free
Amount of items: 3
Items: 
Size: 442100 Color: 18
Size: 296359 Color: 16
Size: 261498 Color: 3

Bin 2178: 44 of cap free
Amount of items: 2
Items: 
Size: 511955 Color: 5
Size: 488002 Color: 15

Bin 2179: 44 of cap free
Amount of items: 2
Items: 
Size: 515278 Color: 5
Size: 484679 Color: 15

Bin 2180: 44 of cap free
Amount of items: 2
Items: 
Size: 561127 Color: 6
Size: 438830 Color: 10

Bin 2181: 44 of cap free
Amount of items: 2
Items: 
Size: 565528 Color: 0
Size: 434429 Color: 7

Bin 2182: 44 of cap free
Amount of items: 2
Items: 
Size: 581980 Color: 17
Size: 417977 Color: 6

Bin 2183: 44 of cap free
Amount of items: 2
Items: 
Size: 619193 Color: 15
Size: 380764 Color: 7

Bin 2184: 44 of cap free
Amount of items: 2
Items: 
Size: 678995 Color: 11
Size: 320962 Color: 0

Bin 2185: 44 of cap free
Amount of items: 2
Items: 
Size: 713749 Color: 15
Size: 286208 Color: 6

Bin 2186: 44 of cap free
Amount of items: 2
Items: 
Size: 749575 Color: 17
Size: 250382 Color: 14

Bin 2187: 44 of cap free
Amount of items: 2
Items: 
Size: 789452 Color: 9
Size: 210505 Color: 12

Bin 2188: 45 of cap free
Amount of items: 3
Items: 
Size: 680798 Color: 9
Size: 165971 Color: 3
Size: 153187 Color: 4

Bin 2189: 45 of cap free
Amount of items: 2
Items: 
Size: 735423 Color: 14
Size: 264533 Color: 15

Bin 2190: 45 of cap free
Amount of items: 2
Items: 
Size: 616999 Color: 5
Size: 382957 Color: 19

Bin 2191: 45 of cap free
Amount of items: 2
Items: 
Size: 610263 Color: 12
Size: 389693 Color: 19

Bin 2192: 45 of cap free
Amount of items: 2
Items: 
Size: 618871 Color: 13
Size: 381085 Color: 4

Bin 2193: 45 of cap free
Amount of items: 2
Items: 
Size: 659048 Color: 17
Size: 340908 Color: 4

Bin 2194: 45 of cap free
Amount of items: 3
Items: 
Size: 654381 Color: 8
Size: 173170 Color: 6
Size: 172405 Color: 15

Bin 2195: 45 of cap free
Amount of items: 2
Items: 
Size: 746634 Color: 14
Size: 253322 Color: 10

Bin 2196: 45 of cap free
Amount of items: 3
Items: 
Size: 710152 Color: 13
Size: 161762 Color: 11
Size: 128042 Color: 8

Bin 2197: 45 of cap free
Amount of items: 2
Items: 
Size: 693478 Color: 18
Size: 306478 Color: 8

Bin 2198: 45 of cap free
Amount of items: 2
Items: 
Size: 541549 Color: 2
Size: 458407 Color: 3

Bin 2199: 45 of cap free
Amount of items: 2
Items: 
Size: 616796 Color: 19
Size: 383160 Color: 12

Bin 2200: 45 of cap free
Amount of items: 2
Items: 
Size: 773804 Color: 10
Size: 226152 Color: 17

Bin 2201: 45 of cap free
Amount of items: 2
Items: 
Size: 734986 Color: 4
Size: 264970 Color: 6

Bin 2202: 45 of cap free
Amount of items: 2
Items: 
Size: 741722 Color: 13
Size: 258234 Color: 19

Bin 2203: 45 of cap free
Amount of items: 2
Items: 
Size: 574754 Color: 16
Size: 425202 Color: 4

Bin 2204: 45 of cap free
Amount of items: 2
Items: 
Size: 596313 Color: 8
Size: 403643 Color: 19

Bin 2205: 45 of cap free
Amount of items: 2
Items: 
Size: 612976 Color: 18
Size: 386980 Color: 0

Bin 2206: 45 of cap free
Amount of items: 2
Items: 
Size: 620945 Color: 16
Size: 379011 Color: 2

Bin 2207: 45 of cap free
Amount of items: 2
Items: 
Size: 671555 Color: 14
Size: 328401 Color: 6

Bin 2208: 45 of cap free
Amount of items: 2
Items: 
Size: 724076 Color: 4
Size: 275880 Color: 19

Bin 2209: 45 of cap free
Amount of items: 2
Items: 
Size: 771390 Color: 19
Size: 228566 Color: 5

Bin 2210: 46 of cap free
Amount of items: 4
Items: 
Size: 333515 Color: 5
Size: 272352 Color: 10
Size: 245804 Color: 16
Size: 148284 Color: 1

Bin 2211: 46 of cap free
Amount of items: 3
Items: 
Size: 372496 Color: 17
Size: 329693 Color: 4
Size: 297766 Color: 8

Bin 2212: 46 of cap free
Amount of items: 2
Items: 
Size: 747121 Color: 1
Size: 252834 Color: 7

Bin 2213: 46 of cap free
Amount of items: 2
Items: 
Size: 682719 Color: 16
Size: 317236 Color: 13

Bin 2214: 46 of cap free
Amount of items: 2
Items: 
Size: 747665 Color: 14
Size: 252290 Color: 0

Bin 2215: 46 of cap free
Amount of items: 2
Items: 
Size: 647876 Color: 10
Size: 352079 Color: 19

Bin 2216: 46 of cap free
Amount of items: 2
Items: 
Size: 688728 Color: 14
Size: 311227 Color: 19

Bin 2217: 46 of cap free
Amount of items: 2
Items: 
Size: 630923 Color: 4
Size: 369032 Color: 7

Bin 2218: 46 of cap free
Amount of items: 2
Items: 
Size: 701515 Color: 0
Size: 298440 Color: 18

Bin 2219: 46 of cap free
Amount of items: 2
Items: 
Size: 531234 Color: 8
Size: 468721 Color: 10

Bin 2220: 46 of cap free
Amount of items: 2
Items: 
Size: 542190 Color: 10
Size: 457765 Color: 16

Bin 2221: 46 of cap free
Amount of items: 2
Items: 
Size: 575058 Color: 15
Size: 424897 Color: 17

Bin 2222: 46 of cap free
Amount of items: 2
Items: 
Size: 585599 Color: 4
Size: 414356 Color: 19

Bin 2223: 46 of cap free
Amount of items: 2
Items: 
Size: 601021 Color: 8
Size: 398934 Color: 7

Bin 2224: 46 of cap free
Amount of items: 2
Items: 
Size: 606849 Color: 14
Size: 393106 Color: 0

Bin 2225: 46 of cap free
Amount of items: 2
Items: 
Size: 613933 Color: 10
Size: 386022 Color: 12

Bin 2226: 46 of cap free
Amount of items: 2
Items: 
Size: 651325 Color: 19
Size: 348630 Color: 2

Bin 2227: 46 of cap free
Amount of items: 2
Items: 
Size: 663949 Color: 18
Size: 336006 Color: 15

Bin 2228: 46 of cap free
Amount of items: 2
Items: 
Size: 674366 Color: 15
Size: 325589 Color: 19

Bin 2229: 46 of cap free
Amount of items: 2
Items: 
Size: 778225 Color: 16
Size: 221730 Color: 5

Bin 2230: 46 of cap free
Amount of items: 2
Items: 
Size: 783204 Color: 3
Size: 216751 Color: 9

Bin 2231: 46 of cap free
Amount of items: 2
Items: 
Size: 784111 Color: 5
Size: 215844 Color: 4

Bin 2232: 46 of cap free
Amount of items: 2
Items: 
Size: 791997 Color: 17
Size: 207958 Color: 3

Bin 2233: 47 of cap free
Amount of items: 2
Items: 
Size: 595277 Color: 5
Size: 404677 Color: 3

Bin 2234: 47 of cap free
Amount of items: 2
Items: 
Size: 762297 Color: 10
Size: 237657 Color: 11

Bin 2235: 47 of cap free
Amount of items: 3
Items: 
Size: 758731 Color: 1
Size: 126937 Color: 14
Size: 114286 Color: 19

Bin 2236: 47 of cap free
Amount of items: 3
Items: 
Size: 371102 Color: 1
Size: 343858 Color: 12
Size: 284994 Color: 11

Bin 2237: 47 of cap free
Amount of items: 2
Items: 
Size: 756802 Color: 17
Size: 243152 Color: 13

Bin 2238: 47 of cap free
Amount of items: 3
Items: 
Size: 377227 Color: 14
Size: 340558 Color: 8
Size: 282169 Color: 18

Bin 2239: 47 of cap free
Amount of items: 2
Items: 
Size: 689498 Color: 18
Size: 310456 Color: 2

Bin 2240: 47 of cap free
Amount of items: 3
Items: 
Size: 639008 Color: 8
Size: 185902 Color: 17
Size: 175044 Color: 9

Bin 2241: 47 of cap free
Amount of items: 3
Items: 
Size: 377283 Color: 7
Size: 369546 Color: 2
Size: 253125 Color: 10

Bin 2242: 47 of cap free
Amount of items: 2
Items: 
Size: 589803 Color: 4
Size: 410151 Color: 6

Bin 2243: 47 of cap free
Amount of items: 3
Items: 
Size: 364339 Color: 3
Size: 341633 Color: 1
Size: 293982 Color: 10

Bin 2244: 47 of cap free
Amount of items: 2
Items: 
Size: 632243 Color: 5
Size: 367711 Color: 15

Bin 2245: 47 of cap free
Amount of items: 2
Items: 
Size: 512862 Color: 6
Size: 487092 Color: 18

Bin 2246: 47 of cap free
Amount of items: 2
Items: 
Size: 540731 Color: 6
Size: 459223 Color: 5

Bin 2247: 47 of cap free
Amount of items: 2
Items: 
Size: 563133 Color: 6
Size: 436821 Color: 5

Bin 2248: 47 of cap free
Amount of items: 2
Items: 
Size: 564437 Color: 7
Size: 435517 Color: 17

Bin 2249: 47 of cap free
Amount of items: 2
Items: 
Size: 626649 Color: 11
Size: 373305 Color: 14

Bin 2250: 47 of cap free
Amount of items: 3
Items: 
Size: 664892 Color: 14
Size: 167574 Color: 0
Size: 167488 Color: 2

Bin 2251: 48 of cap free
Amount of items: 3
Items: 
Size: 625439 Color: 7
Size: 256455 Color: 7
Size: 118059 Color: 16

Bin 2252: 48 of cap free
Amount of items: 3
Items: 
Size: 684795 Color: 10
Size: 196055 Color: 18
Size: 119103 Color: 19

Bin 2253: 48 of cap free
Amount of items: 2
Items: 
Size: 502414 Color: 13
Size: 497539 Color: 3

Bin 2254: 48 of cap free
Amount of items: 2
Items: 
Size: 595685 Color: 12
Size: 404268 Color: 19

Bin 2255: 48 of cap free
Amount of items: 2
Items: 
Size: 682830 Color: 18
Size: 317123 Color: 15

Bin 2256: 48 of cap free
Amount of items: 2
Items: 
Size: 517101 Color: 17
Size: 482852 Color: 7

Bin 2257: 48 of cap free
Amount of items: 2
Items: 
Size: 553373 Color: 8
Size: 446580 Color: 5

Bin 2258: 48 of cap free
Amount of items: 2
Items: 
Size: 601131 Color: 19
Size: 398822 Color: 17

Bin 2259: 48 of cap free
Amount of items: 2
Items: 
Size: 601634 Color: 8
Size: 398319 Color: 17

Bin 2260: 48 of cap free
Amount of items: 2
Items: 
Size: 605434 Color: 8
Size: 394519 Color: 15

Bin 2261: 48 of cap free
Amount of items: 2
Items: 
Size: 609589 Color: 9
Size: 390364 Color: 1

Bin 2262: 48 of cap free
Amount of items: 2
Items: 
Size: 610727 Color: 19
Size: 389226 Color: 10

Bin 2263: 48 of cap free
Amount of items: 2
Items: 
Size: 615088 Color: 14
Size: 384865 Color: 8

Bin 2264: 48 of cap free
Amount of items: 3
Items: 
Size: 642875 Color: 10
Size: 178869 Color: 6
Size: 178209 Color: 6

Bin 2265: 48 of cap free
Amount of items: 3
Items: 
Size: 648247 Color: 4
Size: 176287 Color: 14
Size: 175419 Color: 4

Bin 2266: 48 of cap free
Amount of items: 2
Items: 
Size: 665108 Color: 3
Size: 334845 Color: 12

Bin 2267: 48 of cap free
Amount of items: 2
Items: 
Size: 711652 Color: 5
Size: 288301 Color: 16

Bin 2268: 48 of cap free
Amount of items: 2
Items: 
Size: 743445 Color: 7
Size: 256508 Color: 17

Bin 2269: 48 of cap free
Amount of items: 2
Items: 
Size: 756342 Color: 13
Size: 243611 Color: 6

Bin 2270: 48 of cap free
Amount of items: 2
Items: 
Size: 795955 Color: 6
Size: 203998 Color: 19

Bin 2271: 49 of cap free
Amount of items: 2
Items: 
Size: 792978 Color: 12
Size: 206974 Color: 4

Bin 2272: 49 of cap free
Amount of items: 2
Items: 
Size: 508533 Color: 0
Size: 491419 Color: 1

Bin 2273: 49 of cap free
Amount of items: 2
Items: 
Size: 654608 Color: 15
Size: 345344 Color: 2

Bin 2274: 49 of cap free
Amount of items: 3
Items: 
Size: 735125 Color: 13
Size: 132603 Color: 5
Size: 132224 Color: 15

Bin 2275: 49 of cap free
Amount of items: 3
Items: 
Size: 728413 Color: 6
Size: 141471 Color: 10
Size: 130068 Color: 13

Bin 2276: 49 of cap free
Amount of items: 2
Items: 
Size: 781183 Color: 6
Size: 218769 Color: 2

Bin 2277: 49 of cap free
Amount of items: 2
Items: 
Size: 501230 Color: 17
Size: 498722 Color: 14

Bin 2278: 49 of cap free
Amount of items: 2
Items: 
Size: 511544 Color: 2
Size: 488408 Color: 4

Bin 2279: 49 of cap free
Amount of items: 2
Items: 
Size: 518179 Color: 3
Size: 481773 Color: 14

Bin 2280: 49 of cap free
Amount of items: 2
Items: 
Size: 537130 Color: 1
Size: 462822 Color: 6

Bin 2281: 49 of cap free
Amount of items: 2
Items: 
Size: 554828 Color: 12
Size: 445124 Color: 8

Bin 2282: 49 of cap free
Amount of items: 2
Items: 
Size: 574449 Color: 12
Size: 425503 Color: 7

Bin 2283: 49 of cap free
Amount of items: 2
Items: 
Size: 594200 Color: 15
Size: 405752 Color: 0

Bin 2284: 49 of cap free
Amount of items: 2
Items: 
Size: 598743 Color: 7
Size: 401209 Color: 14

Bin 2285: 49 of cap free
Amount of items: 2
Items: 
Size: 610658 Color: 12
Size: 389294 Color: 4

Bin 2286: 49 of cap free
Amount of items: 2
Items: 
Size: 657069 Color: 8
Size: 342883 Color: 5

Bin 2287: 49 of cap free
Amount of items: 2
Items: 
Size: 683124 Color: 1
Size: 316828 Color: 2

Bin 2288: 49 of cap free
Amount of items: 2
Items: 
Size: 743384 Color: 11
Size: 256568 Color: 9

Bin 2289: 49 of cap free
Amount of items: 2
Items: 
Size: 771970 Color: 12
Size: 227982 Color: 17

Bin 2290: 49 of cap free
Amount of items: 2
Items: 
Size: 776489 Color: 19
Size: 223463 Color: 2

Bin 2291: 49 of cap free
Amount of items: 2
Items: 
Size: 797332 Color: 7
Size: 202620 Color: 5

Bin 2292: 50 of cap free
Amount of items: 3
Items: 
Size: 696072 Color: 2
Size: 168587 Color: 18
Size: 135292 Color: 6

Bin 2293: 50 of cap free
Amount of items: 3
Items: 
Size: 740979 Color: 17
Size: 139444 Color: 6
Size: 119528 Color: 10

Bin 2294: 50 of cap free
Amount of items: 3
Items: 
Size: 753549 Color: 4
Size: 124827 Color: 19
Size: 121575 Color: 11

Bin 2295: 50 of cap free
Amount of items: 2
Items: 
Size: 595106 Color: 19
Size: 404845 Color: 1

Bin 2296: 50 of cap free
Amount of items: 2
Items: 
Size: 755077 Color: 17
Size: 244874 Color: 15

Bin 2297: 50 of cap free
Amount of items: 2
Items: 
Size: 782278 Color: 18
Size: 217673 Color: 11

Bin 2298: 50 of cap free
Amount of items: 2
Items: 
Size: 762437 Color: 17
Size: 237514 Color: 8

Bin 2299: 50 of cap free
Amount of items: 2
Items: 
Size: 578526 Color: 2
Size: 421425 Color: 10

Bin 2300: 50 of cap free
Amount of items: 2
Items: 
Size: 580471 Color: 2
Size: 419480 Color: 7

Bin 2301: 50 of cap free
Amount of items: 2
Items: 
Size: 581665 Color: 11
Size: 418286 Color: 8

Bin 2302: 50 of cap free
Amount of items: 2
Items: 
Size: 662743 Color: 19
Size: 337208 Color: 14

Bin 2303: 50 of cap free
Amount of items: 2
Items: 
Size: 738649 Color: 5
Size: 261302 Color: 19

Bin 2304: 50 of cap free
Amount of items: 2
Items: 
Size: 744946 Color: 18
Size: 255005 Color: 19

Bin 2305: 50 of cap free
Amount of items: 2
Items: 
Size: 771034 Color: 6
Size: 228917 Color: 18

Bin 2306: 50 of cap free
Amount of items: 2
Items: 
Size: 782782 Color: 3
Size: 217169 Color: 5

Bin 2307: 51 of cap free
Amount of items: 3
Items: 
Size: 386901 Color: 0
Size: 310241 Color: 17
Size: 302808 Color: 1

Bin 2308: 51 of cap free
Amount of items: 3
Items: 
Size: 606690 Color: 10
Size: 199738 Color: 11
Size: 193522 Color: 1

Bin 2309: 51 of cap free
Amount of items: 2
Items: 
Size: 797206 Color: 1
Size: 202744 Color: 4

Bin 2310: 51 of cap free
Amount of items: 2
Items: 
Size: 710472 Color: 15
Size: 289478 Color: 4

Bin 2311: 51 of cap free
Amount of items: 2
Items: 
Size: 712765 Color: 16
Size: 287185 Color: 0

Bin 2312: 51 of cap free
Amount of items: 2
Items: 
Size: 658116 Color: 19
Size: 341834 Color: 3

Bin 2313: 51 of cap free
Amount of items: 2
Items: 
Size: 582293 Color: 11
Size: 417657 Color: 3

Bin 2314: 51 of cap free
Amount of items: 2
Items: 
Size: 509470 Color: 4
Size: 490480 Color: 13

Bin 2315: 51 of cap free
Amount of items: 2
Items: 
Size: 533809 Color: 7
Size: 466141 Color: 16

Bin 2316: 51 of cap free
Amount of items: 2
Items: 
Size: 543482 Color: 8
Size: 456468 Color: 13

Bin 2317: 51 of cap free
Amount of items: 2
Items: 
Size: 610204 Color: 11
Size: 389746 Color: 18

Bin 2318: 51 of cap free
Amount of items: 2
Items: 
Size: 617440 Color: 9
Size: 382510 Color: 1

Bin 2319: 51 of cap free
Amount of items: 2
Items: 
Size: 638702 Color: 6
Size: 361248 Color: 13

Bin 2320: 51 of cap free
Amount of items: 2
Items: 
Size: 671381 Color: 10
Size: 328569 Color: 4

Bin 2321: 51 of cap free
Amount of items: 2
Items: 
Size: 690830 Color: 18
Size: 309120 Color: 4

Bin 2322: 51 of cap free
Amount of items: 2
Items: 
Size: 708539 Color: 7
Size: 291411 Color: 19

Bin 2323: 51 of cap free
Amount of items: 2
Items: 
Size: 773486 Color: 11
Size: 226464 Color: 9

Bin 2324: 51 of cap free
Amount of items: 2
Items: 
Size: 774181 Color: 13
Size: 225769 Color: 1

Bin 2325: 51 of cap free
Amount of items: 2
Items: 
Size: 779363 Color: 8
Size: 220587 Color: 5

Bin 2326: 52 of cap free
Amount of items: 3
Items: 
Size: 709999 Color: 6
Size: 145414 Color: 1
Size: 144536 Color: 0

Bin 2327: 52 of cap free
Amount of items: 2
Items: 
Size: 528323 Color: 19
Size: 471626 Color: 5

Bin 2328: 52 of cap free
Amount of items: 2
Items: 
Size: 537675 Color: 2
Size: 462274 Color: 15

Bin 2329: 52 of cap free
Amount of items: 2
Items: 
Size: 550127 Color: 14
Size: 449822 Color: 1

Bin 2330: 52 of cap free
Amount of items: 2
Items: 
Size: 550234 Color: 4
Size: 449715 Color: 8

Bin 2331: 52 of cap free
Amount of items: 2
Items: 
Size: 560954 Color: 14
Size: 438995 Color: 11

Bin 2332: 52 of cap free
Amount of items: 2
Items: 
Size: 561246 Color: 10
Size: 438703 Color: 8

Bin 2333: 52 of cap free
Amount of items: 2
Items: 
Size: 572095 Color: 0
Size: 427854 Color: 18

Bin 2334: 52 of cap free
Amount of items: 2
Items: 
Size: 597547 Color: 7
Size: 402402 Color: 3

Bin 2335: 52 of cap free
Amount of items: 2
Items: 
Size: 603875 Color: 14
Size: 396074 Color: 16

Bin 2336: 52 of cap free
Amount of items: 2
Items: 
Size: 616376 Color: 4
Size: 383573 Color: 0

Bin 2337: 52 of cap free
Amount of items: 2
Items: 
Size: 622489 Color: 16
Size: 377460 Color: 6

Bin 2338: 52 of cap free
Amount of items: 2
Items: 
Size: 629245 Color: 3
Size: 370704 Color: 9

Bin 2339: 52 of cap free
Amount of items: 2
Items: 
Size: 634498 Color: 5
Size: 365451 Color: 6

Bin 2340: 52 of cap free
Amount of items: 2
Items: 
Size: 661504 Color: 11
Size: 338445 Color: 12

Bin 2341: 52 of cap free
Amount of items: 2
Items: 
Size: 690236 Color: 14
Size: 309713 Color: 5

Bin 2342: 52 of cap free
Amount of items: 2
Items: 
Size: 721521 Color: 8
Size: 278428 Color: 14

Bin 2343: 52 of cap free
Amount of items: 2
Items: 
Size: 784793 Color: 0
Size: 215156 Color: 4

Bin 2344: 52 of cap free
Amount of items: 2
Items: 
Size: 796411 Color: 1
Size: 203538 Color: 8

Bin 2345: 53 of cap free
Amount of items: 3
Items: 
Size: 715164 Color: 11
Size: 143027 Color: 1
Size: 141757 Color: 10

Bin 2346: 53 of cap free
Amount of items: 3
Items: 
Size: 357433 Color: 3
Size: 339730 Color: 3
Size: 302785 Color: 10

Bin 2347: 53 of cap free
Amount of items: 2
Items: 
Size: 548297 Color: 11
Size: 451651 Color: 5

Bin 2348: 53 of cap free
Amount of items: 2
Items: 
Size: 603484 Color: 6
Size: 396464 Color: 12

Bin 2349: 53 of cap free
Amount of items: 3
Items: 
Size: 633287 Color: 7
Size: 183334 Color: 7
Size: 183327 Color: 4

Bin 2350: 53 of cap free
Amount of items: 2
Items: 
Size: 767629 Color: 11
Size: 232319 Color: 4

Bin 2351: 53 of cap free
Amount of items: 2
Items: 
Size: 777216 Color: 5
Size: 222732 Color: 2

Bin 2352: 54 of cap free
Amount of items: 3
Items: 
Size: 704781 Color: 12
Size: 160335 Color: 7
Size: 134831 Color: 7

Bin 2353: 54 of cap free
Amount of items: 2
Items: 
Size: 622595 Color: 12
Size: 377352 Color: 8

Bin 2354: 54 of cap free
Amount of items: 2
Items: 
Size: 724570 Color: 16
Size: 275377 Color: 10

Bin 2355: 54 of cap free
Amount of items: 2
Items: 
Size: 681981 Color: 3
Size: 317966 Color: 2

Bin 2356: 54 of cap free
Amount of items: 2
Items: 
Size: 670013 Color: 4
Size: 329934 Color: 6

Bin 2357: 54 of cap free
Amount of items: 2
Items: 
Size: 526452 Color: 9
Size: 473495 Color: 3

Bin 2358: 54 of cap free
Amount of items: 2
Items: 
Size: 561947 Color: 11
Size: 438000 Color: 10

Bin 2359: 54 of cap free
Amount of items: 2
Items: 
Size: 598466 Color: 2
Size: 401481 Color: 17

Bin 2360: 54 of cap free
Amount of items: 2
Items: 
Size: 614418 Color: 15
Size: 385529 Color: 14

Bin 2361: 54 of cap free
Amount of items: 2
Items: 
Size: 727778 Color: 11
Size: 272169 Color: 9

Bin 2362: 54 of cap free
Amount of items: 2
Items: 
Size: 743091 Color: 3
Size: 256856 Color: 5

Bin 2363: 54 of cap free
Amount of items: 2
Items: 
Size: 761313 Color: 5
Size: 238634 Color: 1

Bin 2364: 54 of cap free
Amount of items: 2
Items: 
Size: 795260 Color: 11
Size: 204687 Color: 1

Bin 2365: 55 of cap free
Amount of items: 2
Items: 
Size: 785381 Color: 7
Size: 214565 Color: 19

Bin 2366: 55 of cap free
Amount of items: 2
Items: 
Size: 522023 Color: 8
Size: 477923 Color: 1

Bin 2367: 55 of cap free
Amount of items: 2
Items: 
Size: 762252 Color: 13
Size: 237694 Color: 2

Bin 2368: 55 of cap free
Amount of items: 2
Items: 
Size: 796743 Color: 11
Size: 203203 Color: 6

Bin 2369: 55 of cap free
Amount of items: 2
Items: 
Size: 675101 Color: 16
Size: 324845 Color: 6

Bin 2370: 55 of cap free
Amount of items: 2
Items: 
Size: 538596 Color: 16
Size: 461350 Color: 14

Bin 2371: 55 of cap free
Amount of items: 2
Items: 
Size: 647132 Color: 12
Size: 352814 Color: 16

Bin 2372: 55 of cap free
Amount of items: 2
Items: 
Size: 718136 Color: 3
Size: 281810 Color: 7

Bin 2373: 55 of cap free
Amount of items: 2
Items: 
Size: 507135 Color: 10
Size: 492811 Color: 15

Bin 2374: 55 of cap free
Amount of items: 2
Items: 
Size: 562850 Color: 19
Size: 437096 Color: 12

Bin 2375: 55 of cap free
Amount of items: 2
Items: 
Size: 592842 Color: 17
Size: 407104 Color: 13

Bin 2376: 55 of cap free
Amount of items: 2
Items: 
Size: 599395 Color: 17
Size: 400551 Color: 4

Bin 2377: 55 of cap free
Amount of items: 2
Items: 
Size: 646040 Color: 17
Size: 353906 Color: 1

Bin 2378: 55 of cap free
Amount of items: 2
Items: 
Size: 654300 Color: 0
Size: 345646 Color: 14

Bin 2379: 55 of cap free
Amount of items: 2
Items: 
Size: 713239 Color: 19
Size: 286707 Color: 11

Bin 2380: 55 of cap free
Amount of items: 2
Items: 
Size: 733766 Color: 3
Size: 266180 Color: 14

Bin 2381: 55 of cap free
Amount of items: 2
Items: 
Size: 742332 Color: 2
Size: 257614 Color: 6

Bin 2382: 55 of cap free
Amount of items: 2
Items: 
Size: 765744 Color: 3
Size: 234202 Color: 9

Bin 2383: 55 of cap free
Amount of items: 2
Items: 
Size: 784423 Color: 14
Size: 215523 Color: 9

Bin 2384: 56 of cap free
Amount of items: 2
Items: 
Size: 714110 Color: 0
Size: 285835 Color: 3

Bin 2385: 56 of cap free
Amount of items: 2
Items: 
Size: 680249 Color: 6
Size: 319696 Color: 19

Bin 2386: 56 of cap free
Amount of items: 2
Items: 
Size: 759747 Color: 2
Size: 240198 Color: 4

Bin 2387: 56 of cap free
Amount of items: 3
Items: 
Size: 722660 Color: 6
Size: 164610 Color: 5
Size: 112675 Color: 1

Bin 2388: 56 of cap free
Amount of items: 3
Items: 
Size: 567926 Color: 16
Size: 251111 Color: 15
Size: 180908 Color: 9

Bin 2389: 56 of cap free
Amount of items: 3
Items: 
Size: 635185 Color: 7
Size: 182540 Color: 4
Size: 182220 Color: 16

Bin 2390: 56 of cap free
Amount of items: 2
Items: 
Size: 702426 Color: 2
Size: 297519 Color: 12

Bin 2391: 56 of cap free
Amount of items: 2
Items: 
Size: 581088 Color: 3
Size: 418857 Color: 15

Bin 2392: 56 of cap free
Amount of items: 2
Items: 
Size: 585479 Color: 6
Size: 414466 Color: 8

Bin 2393: 56 of cap free
Amount of items: 2
Items: 
Size: 610105 Color: 0
Size: 389840 Color: 8

Bin 2394: 56 of cap free
Amount of items: 2
Items: 
Size: 714641 Color: 19
Size: 285304 Color: 7

Bin 2395: 56 of cap free
Amount of items: 2
Items: 
Size: 717347 Color: 2
Size: 282598 Color: 17

Bin 2396: 56 of cap free
Amount of items: 2
Items: 
Size: 725016 Color: 11
Size: 274929 Color: 0

Bin 2397: 56 of cap free
Amount of items: 2
Items: 
Size: 737617 Color: 18
Size: 262328 Color: 15

Bin 2398: 56 of cap free
Amount of items: 2
Items: 
Size: 796953 Color: 16
Size: 202992 Color: 14

Bin 2399: 56 of cap free
Amount of items: 2
Items: 
Size: 798727 Color: 16
Size: 201218 Color: 14

Bin 2400: 57 of cap free
Amount of items: 3
Items: 
Size: 704469 Color: 1
Size: 148217 Color: 16
Size: 147258 Color: 0

Bin 2401: 57 of cap free
Amount of items: 2
Items: 
Size: 707499 Color: 13
Size: 292445 Color: 10

Bin 2402: 57 of cap free
Amount of items: 2
Items: 
Size: 780593 Color: 11
Size: 219351 Color: 19

Bin 2403: 57 of cap free
Amount of items: 2
Items: 
Size: 567738 Color: 16
Size: 432206 Color: 12

Bin 2404: 57 of cap free
Amount of items: 2
Items: 
Size: 601971 Color: 4
Size: 397973 Color: 14

Bin 2405: 57 of cap free
Amount of items: 2
Items: 
Size: 601711 Color: 3
Size: 398233 Color: 19

Bin 2406: 57 of cap free
Amount of items: 2
Items: 
Size: 626640 Color: 16
Size: 373304 Color: 12

Bin 2407: 57 of cap free
Amount of items: 2
Items: 
Size: 659297 Color: 3
Size: 340647 Color: 19

Bin 2408: 57 of cap free
Amount of items: 2
Items: 
Size: 680653 Color: 3
Size: 319291 Color: 8

Bin 2409: 57 of cap free
Amount of items: 2
Items: 
Size: 695220 Color: 13
Size: 304724 Color: 15

Bin 2410: 57 of cap free
Amount of items: 2
Items: 
Size: 766231 Color: 0
Size: 233713 Color: 12

Bin 2411: 57 of cap free
Amount of items: 2
Items: 
Size: 785538 Color: 10
Size: 214406 Color: 1

Bin 2412: 58 of cap free
Amount of items: 2
Items: 
Size: 615285 Color: 6
Size: 384658 Color: 16

Bin 2413: 58 of cap free
Amount of items: 2
Items: 
Size: 686440 Color: 18
Size: 313503 Color: 7

Bin 2414: 58 of cap free
Amount of items: 3
Items: 
Size: 607543 Color: 11
Size: 197242 Color: 1
Size: 195158 Color: 1

Bin 2415: 58 of cap free
Amount of items: 2
Items: 
Size: 673991 Color: 10
Size: 325952 Color: 8

Bin 2416: 58 of cap free
Amount of items: 2
Items: 
Size: 752463 Color: 14
Size: 247480 Color: 18

Bin 2417: 58 of cap free
Amount of items: 2
Items: 
Size: 658113 Color: 1
Size: 341830 Color: 0

Bin 2418: 58 of cap free
Amount of items: 2
Items: 
Size: 638152 Color: 2
Size: 361791 Color: 16

Bin 2419: 58 of cap free
Amount of items: 2
Items: 
Size: 502177 Color: 13
Size: 497766 Color: 4

Bin 2420: 58 of cap free
Amount of items: 2
Items: 
Size: 507241 Color: 0
Size: 492702 Color: 6

Bin 2421: 58 of cap free
Amount of items: 2
Items: 
Size: 510027 Color: 14
Size: 489916 Color: 17

Bin 2422: 58 of cap free
Amount of items: 2
Items: 
Size: 513147 Color: 7
Size: 486796 Color: 8

Bin 2423: 58 of cap free
Amount of items: 2
Items: 
Size: 515575 Color: 15
Size: 484368 Color: 0

Bin 2424: 58 of cap free
Amount of items: 2
Items: 
Size: 542182 Color: 8
Size: 457761 Color: 18

Bin 2425: 58 of cap free
Amount of items: 2
Items: 
Size: 543869 Color: 8
Size: 456074 Color: 17

Bin 2426: 58 of cap free
Amount of items: 2
Items: 
Size: 544521 Color: 3
Size: 455422 Color: 14

Bin 2427: 58 of cap free
Amount of items: 2
Items: 
Size: 551157 Color: 16
Size: 448786 Color: 8

Bin 2428: 58 of cap free
Amount of items: 2
Items: 
Size: 570103 Color: 3
Size: 429840 Color: 1

Bin 2429: 58 of cap free
Amount of items: 2
Items: 
Size: 581370 Color: 0
Size: 418573 Color: 18

Bin 2430: 58 of cap free
Amount of items: 3
Items: 
Size: 585140 Color: 6
Size: 215398 Color: 7
Size: 199405 Color: 14

Bin 2431: 58 of cap free
Amount of items: 2
Items: 
Size: 646304 Color: 19
Size: 353639 Color: 11

Bin 2432: 58 of cap free
Amount of items: 2
Items: 
Size: 784483 Color: 6
Size: 215460 Color: 12

Bin 2433: 58 of cap free
Amount of items: 2
Items: 
Size: 796407 Color: 17
Size: 203536 Color: 19

Bin 2434: 59 of cap free
Amount of items: 2
Items: 
Size: 717752 Color: 3
Size: 282190 Color: 17

Bin 2435: 59 of cap free
Amount of items: 2
Items: 
Size: 692575 Color: 14
Size: 307367 Color: 16

Bin 2436: 59 of cap free
Amount of items: 2
Items: 
Size: 503230 Color: 2
Size: 496712 Color: 16

Bin 2437: 59 of cap free
Amount of items: 2
Items: 
Size: 511616 Color: 7
Size: 488326 Color: 15

Bin 2438: 59 of cap free
Amount of items: 2
Items: 
Size: 522890 Color: 19
Size: 477052 Color: 1

Bin 2439: 59 of cap free
Amount of items: 2
Items: 
Size: 591584 Color: 19
Size: 408358 Color: 14

Bin 2440: 59 of cap free
Amount of items: 2
Items: 
Size: 649799 Color: 14
Size: 350143 Color: 4

Bin 2441: 59 of cap free
Amount of items: 2
Items: 
Size: 676034 Color: 6
Size: 323908 Color: 1

Bin 2442: 59 of cap free
Amount of items: 2
Items: 
Size: 683304 Color: 7
Size: 316638 Color: 10

Bin 2443: 59 of cap free
Amount of items: 2
Items: 
Size: 687453 Color: 8
Size: 312489 Color: 5

Bin 2444: 60 of cap free
Amount of items: 3
Items: 
Size: 679269 Color: 1
Size: 169983 Color: 8
Size: 150689 Color: 13

Bin 2445: 60 of cap free
Amount of items: 2
Items: 
Size: 580874 Color: 13
Size: 419067 Color: 6

Bin 2446: 60 of cap free
Amount of items: 2
Items: 
Size: 658916 Color: 8
Size: 341025 Color: 17

Bin 2447: 60 of cap free
Amount of items: 2
Items: 
Size: 640318 Color: 9
Size: 359623 Color: 1

Bin 2448: 60 of cap free
Amount of items: 2
Items: 
Size: 631627 Color: 8
Size: 368314 Color: 7

Bin 2449: 60 of cap free
Amount of items: 2
Items: 
Size: 639828 Color: 13
Size: 360113 Color: 15

Bin 2450: 60 of cap free
Amount of items: 3
Items: 
Size: 735680 Color: 18
Size: 132265 Color: 13
Size: 131996 Color: 7

Bin 2451: 60 of cap free
Amount of items: 3
Items: 
Size: 697851 Color: 18
Size: 151425 Color: 7
Size: 150665 Color: 13

Bin 2452: 60 of cap free
Amount of items: 2
Items: 
Size: 588956 Color: 12
Size: 410985 Color: 17

Bin 2453: 60 of cap free
Amount of items: 2
Items: 
Size: 573845 Color: 7
Size: 426096 Color: 19

Bin 2454: 60 of cap free
Amount of items: 2
Items: 
Size: 509687 Color: 4
Size: 490254 Color: 13

Bin 2455: 60 of cap free
Amount of items: 2
Items: 
Size: 511287 Color: 19
Size: 488654 Color: 9

Bin 2456: 60 of cap free
Amount of items: 2
Items: 
Size: 514320 Color: 14
Size: 485621 Color: 12

Bin 2457: 60 of cap free
Amount of items: 2
Items: 
Size: 519777 Color: 13
Size: 480164 Color: 19

Bin 2458: 60 of cap free
Amount of items: 2
Items: 
Size: 521694 Color: 12
Size: 478247 Color: 18

Bin 2459: 60 of cap free
Amount of items: 2
Items: 
Size: 539801 Color: 8
Size: 460140 Color: 10

Bin 2460: 60 of cap free
Amount of items: 2
Items: 
Size: 597848 Color: 2
Size: 402093 Color: 4

Bin 2461: 60 of cap free
Amount of items: 2
Items: 
Size: 611429 Color: 5
Size: 388512 Color: 15

Bin 2462: 60 of cap free
Amount of items: 2
Items: 
Size: 621600 Color: 14
Size: 378341 Color: 17

Bin 2463: 60 of cap free
Amount of items: 2
Items: 
Size: 671173 Color: 7
Size: 328768 Color: 11

Bin 2464: 60 of cap free
Amount of items: 2
Items: 
Size: 699080 Color: 7
Size: 300861 Color: 13

Bin 2465: 60 of cap free
Amount of items: 2
Items: 
Size: 720939 Color: 5
Size: 279002 Color: 12

Bin 2466: 60 of cap free
Amount of items: 2
Items: 
Size: 730344 Color: 5
Size: 269597 Color: 0

Bin 2467: 60 of cap free
Amount of items: 2
Items: 
Size: 732904 Color: 18
Size: 267037 Color: 6

Bin 2468: 60 of cap free
Amount of items: 2
Items: 
Size: 742017 Color: 14
Size: 257924 Color: 13

Bin 2469: 60 of cap free
Amount of items: 2
Items: 
Size: 761126 Color: 14
Size: 238815 Color: 12

Bin 2470: 60 of cap free
Amount of items: 2
Items: 
Size: 771788 Color: 12
Size: 228153 Color: 19

Bin 2471: 61 of cap free
Amount of items: 3
Items: 
Size: 420620 Color: 1
Size: 297050 Color: 2
Size: 282270 Color: 3

Bin 2472: 61 of cap free
Amount of items: 2
Items: 
Size: 691186 Color: 5
Size: 308754 Color: 2

Bin 2473: 61 of cap free
Amount of items: 2
Items: 
Size: 708977 Color: 8
Size: 290963 Color: 13

Bin 2474: 61 of cap free
Amount of items: 2
Items: 
Size: 699637 Color: 18
Size: 300303 Color: 2

Bin 2475: 61 of cap free
Amount of items: 3
Items: 
Size: 696731 Color: 18
Size: 156501 Color: 6
Size: 146708 Color: 4

Bin 2476: 61 of cap free
Amount of items: 2
Items: 
Size: 644440 Color: 17
Size: 355500 Color: 15

Bin 2477: 61 of cap free
Amount of items: 2
Items: 
Size: 744500 Color: 15
Size: 255440 Color: 6

Bin 2478: 61 of cap free
Amount of items: 2
Items: 
Size: 770163 Color: 4
Size: 229777 Color: 7

Bin 2479: 61 of cap free
Amount of items: 2
Items: 
Size: 535801 Color: 16
Size: 464139 Color: 8

Bin 2480: 61 of cap free
Amount of items: 2
Items: 
Size: 522684 Color: 4
Size: 477256 Color: 14

Bin 2481: 61 of cap free
Amount of items: 2
Items: 
Size: 573333 Color: 19
Size: 426607 Color: 17

Bin 2482: 61 of cap free
Amount of items: 2
Items: 
Size: 574669 Color: 1
Size: 425271 Color: 2

Bin 2483: 61 of cap free
Amount of items: 2
Items: 
Size: 597323 Color: 11
Size: 402617 Color: 3

Bin 2484: 61 of cap free
Amount of items: 2
Items: 
Size: 702850 Color: 2
Size: 297090 Color: 4

Bin 2485: 62 of cap free
Amount of items: 2
Items: 
Size: 630634 Color: 15
Size: 369305 Color: 17

Bin 2486: 62 of cap free
Amount of items: 2
Items: 
Size: 647310 Color: 14
Size: 352629 Color: 9

Bin 2487: 62 of cap free
Amount of items: 3
Items: 
Size: 364012 Color: 10
Size: 333072 Color: 6
Size: 302855 Color: 15

Bin 2488: 62 of cap free
Amount of items: 2
Items: 
Size: 709624 Color: 4
Size: 290315 Color: 13

Bin 2489: 62 of cap free
Amount of items: 2
Items: 
Size: 773253 Color: 8
Size: 226686 Color: 11

Bin 2490: 62 of cap free
Amount of items: 3
Items: 
Size: 692956 Color: 13
Size: 153594 Color: 10
Size: 153389 Color: 4

Bin 2491: 62 of cap free
Amount of items: 2
Items: 
Size: 789626 Color: 18
Size: 210313 Color: 15

Bin 2492: 62 of cap free
Amount of items: 2
Items: 
Size: 638313 Color: 13
Size: 361626 Color: 5

Bin 2493: 62 of cap free
Amount of items: 2
Items: 
Size: 590492 Color: 4
Size: 409447 Color: 18

Bin 2494: 62 of cap free
Amount of items: 2
Items: 
Size: 643802 Color: 10
Size: 356137 Color: 1

Bin 2495: 62 of cap free
Amount of items: 2
Items: 
Size: 667192 Color: 12
Size: 332747 Color: 18

Bin 2496: 62 of cap free
Amount of items: 2
Items: 
Size: 742850 Color: 4
Size: 257089 Color: 10

Bin 2497: 62 of cap free
Amount of items: 2
Items: 
Size: 743434 Color: 1
Size: 256505 Color: 3

Bin 2498: 63 of cap free
Amount of items: 2
Items: 
Size: 522367 Color: 17
Size: 477571 Color: 19

Bin 2499: 63 of cap free
Amount of items: 2
Items: 
Size: 761825 Color: 5
Size: 238113 Color: 14

Bin 2500: 63 of cap free
Amount of items: 2
Items: 
Size: 537494 Color: 15
Size: 462444 Color: 16

Bin 2501: 63 of cap free
Amount of items: 2
Items: 
Size: 565363 Color: 13
Size: 434575 Color: 1

Bin 2502: 63 of cap free
Amount of items: 2
Items: 
Size: 511747 Color: 0
Size: 488191 Color: 6

Bin 2503: 63 of cap free
Amount of items: 2
Items: 
Size: 545197 Color: 3
Size: 454741 Color: 16

Bin 2504: 63 of cap free
Amount of items: 2
Items: 
Size: 575484 Color: 2
Size: 424454 Color: 10

Bin 2505: 63 of cap free
Amount of items: 2
Items: 
Size: 582943 Color: 8
Size: 416995 Color: 7

Bin 2506: 63 of cap free
Amount of items: 2
Items: 
Size: 586015 Color: 17
Size: 413923 Color: 18

Bin 2507: 63 of cap free
Amount of items: 2
Items: 
Size: 624669 Color: 12
Size: 375269 Color: 2

Bin 2508: 63 of cap free
Amount of items: 2
Items: 
Size: 635011 Color: 18
Size: 364927 Color: 0

Bin 2509: 63 of cap free
Amount of items: 2
Items: 
Size: 639493 Color: 19
Size: 360445 Color: 12

Bin 2510: 63 of cap free
Amount of items: 2
Items: 
Size: 647977 Color: 4
Size: 351961 Color: 17

Bin 2511: 63 of cap free
Amount of items: 2
Items: 
Size: 715275 Color: 14
Size: 284663 Color: 0

Bin 2512: 63 of cap free
Amount of items: 2
Items: 
Size: 747236 Color: 15
Size: 252702 Color: 12

Bin 2513: 63 of cap free
Amount of items: 2
Items: 
Size: 766528 Color: 3
Size: 233410 Color: 14

Bin 2514: 64 of cap free
Amount of items: 2
Items: 
Size: 635847 Color: 11
Size: 364090 Color: 17

Bin 2515: 64 of cap free
Amount of items: 3
Items: 
Size: 645395 Color: 3
Size: 177387 Color: 10
Size: 177155 Color: 4

Bin 2516: 64 of cap free
Amount of items: 2
Items: 
Size: 677315 Color: 16
Size: 322622 Color: 3

Bin 2517: 64 of cap free
Amount of items: 2
Items: 
Size: 501611 Color: 11
Size: 498326 Color: 9

Bin 2518: 64 of cap free
Amount of items: 2
Items: 
Size: 564830 Color: 13
Size: 435107 Color: 4

Bin 2519: 64 of cap free
Amount of items: 2
Items: 
Size: 565930 Color: 6
Size: 434007 Color: 19

Bin 2520: 64 of cap free
Amount of items: 2
Items: 
Size: 569171 Color: 11
Size: 430766 Color: 0

Bin 2521: 64 of cap free
Amount of items: 2
Items: 
Size: 587603 Color: 1
Size: 412334 Color: 4

Bin 2522: 64 of cap free
Amount of items: 2
Items: 
Size: 598216 Color: 19
Size: 401721 Color: 11

Bin 2523: 64 of cap free
Amount of items: 2
Items: 
Size: 616657 Color: 9
Size: 383280 Color: 14

Bin 2524: 64 of cap free
Amount of items: 2
Items: 
Size: 705588 Color: 15
Size: 294349 Color: 19

Bin 2525: 65 of cap free
Amount of items: 2
Items: 
Size: 697880 Color: 0
Size: 302056 Color: 19

Bin 2526: 65 of cap free
Amount of items: 3
Items: 
Size: 621905 Color: 11
Size: 189172 Color: 18
Size: 188859 Color: 14

Bin 2527: 65 of cap free
Amount of items: 2
Items: 
Size: 792805 Color: 8
Size: 207131 Color: 14

Bin 2528: 65 of cap free
Amount of items: 2
Items: 
Size: 754603 Color: 2
Size: 245333 Color: 16

Bin 2529: 65 of cap free
Amount of items: 2
Items: 
Size: 773787 Color: 6
Size: 226149 Color: 9

Bin 2530: 65 of cap free
Amount of items: 2
Items: 
Size: 736987 Color: 19
Size: 262949 Color: 7

Bin 2531: 65 of cap free
Amount of items: 2
Items: 
Size: 707194 Color: 3
Size: 292742 Color: 16

Bin 2532: 65 of cap free
Amount of items: 2
Items: 
Size: 571120 Color: 11
Size: 428816 Color: 5

Bin 2533: 65 of cap free
Amount of items: 2
Items: 
Size: 501474 Color: 13
Size: 498462 Color: 8

Bin 2534: 65 of cap free
Amount of items: 2
Items: 
Size: 518030 Color: 3
Size: 481906 Color: 11

Bin 2535: 65 of cap free
Amount of items: 2
Items: 
Size: 528719 Color: 2
Size: 471217 Color: 8

Bin 2536: 65 of cap free
Amount of items: 2
Items: 
Size: 551781 Color: 11
Size: 448155 Color: 5

Bin 2537: 65 of cap free
Amount of items: 2
Items: 
Size: 580565 Color: 0
Size: 419371 Color: 14

Bin 2538: 65 of cap free
Amount of items: 2
Items: 
Size: 611097 Color: 15
Size: 388839 Color: 6

Bin 2539: 65 of cap free
Amount of items: 2
Items: 
Size: 652515 Color: 17
Size: 347421 Color: 19

Bin 2540: 65 of cap free
Amount of items: 2
Items: 
Size: 667680 Color: 11
Size: 332256 Color: 7

Bin 2541: 65 of cap free
Amount of items: 2
Items: 
Size: 778861 Color: 0
Size: 221075 Color: 17

Bin 2542: 65 of cap free
Amount of items: 2
Items: 
Size: 783312 Color: 7
Size: 216624 Color: 10

Bin 2543: 65 of cap free
Amount of items: 2
Items: 
Size: 797330 Color: 19
Size: 202606 Color: 3

Bin 2544: 66 of cap free
Amount of items: 2
Items: 
Size: 729092 Color: 13
Size: 270843 Color: 9

Bin 2545: 66 of cap free
Amount of items: 2
Items: 
Size: 788233 Color: 2
Size: 211702 Color: 16

Bin 2546: 66 of cap free
Amount of items: 2
Items: 
Size: 680329 Color: 16
Size: 319606 Color: 3

Bin 2547: 66 of cap free
Amount of items: 3
Items: 
Size: 666755 Color: 15
Size: 169026 Color: 2
Size: 164154 Color: 1

Bin 2548: 66 of cap free
Amount of items: 2
Items: 
Size: 626793 Color: 8
Size: 373142 Color: 9

Bin 2549: 66 of cap free
Amount of items: 2
Items: 
Size: 670257 Color: 11
Size: 329678 Color: 15

Bin 2550: 66 of cap free
Amount of items: 2
Items: 
Size: 545663 Color: 5
Size: 454272 Color: 18

Bin 2551: 66 of cap free
Amount of items: 2
Items: 
Size: 552610 Color: 14
Size: 447325 Color: 9

Bin 2552: 66 of cap free
Amount of items: 2
Items: 
Size: 574507 Color: 9
Size: 425428 Color: 16

Bin 2553: 66 of cap free
Amount of items: 2
Items: 
Size: 584150 Color: 14
Size: 415785 Color: 9

Bin 2554: 66 of cap free
Amount of items: 2
Items: 
Size: 591430 Color: 0
Size: 408505 Color: 10

Bin 2555: 66 of cap free
Amount of items: 2
Items: 
Size: 628736 Color: 12
Size: 371199 Color: 3

Bin 2556: 66 of cap free
Amount of items: 2
Items: 
Size: 632304 Color: 12
Size: 367631 Color: 17

Bin 2557: 66 of cap free
Amount of items: 2
Items: 
Size: 756948 Color: 3
Size: 242987 Color: 17

Bin 2558: 67 of cap free
Amount of items: 3
Items: 
Size: 649554 Color: 17
Size: 176608 Color: 12
Size: 173772 Color: 0

Bin 2559: 67 of cap free
Amount of items: 3
Items: 
Size: 710680 Color: 18
Size: 175539 Color: 0
Size: 113715 Color: 3

Bin 2560: 67 of cap free
Amount of items: 2
Items: 
Size: 745602 Color: 4
Size: 254332 Color: 9

Bin 2561: 67 of cap free
Amount of items: 2
Items: 
Size: 607173 Color: 19
Size: 392761 Color: 11

Bin 2562: 67 of cap free
Amount of items: 2
Items: 
Size: 706061 Color: 10
Size: 293873 Color: 4

Bin 2563: 67 of cap free
Amount of items: 2
Items: 
Size: 718024 Color: 1
Size: 281910 Color: 13

Bin 2564: 67 of cap free
Amount of items: 2
Items: 
Size: 518980 Color: 3
Size: 480954 Color: 8

Bin 2565: 67 of cap free
Amount of items: 2
Items: 
Size: 509618 Color: 8
Size: 490316 Color: 14

Bin 2566: 67 of cap free
Amount of items: 2
Items: 
Size: 564251 Color: 9
Size: 435683 Color: 5

Bin 2567: 67 of cap free
Amount of items: 2
Items: 
Size: 572995 Color: 6
Size: 426939 Color: 11

Bin 2568: 67 of cap free
Amount of items: 2
Items: 
Size: 573752 Color: 9
Size: 426182 Color: 13

Bin 2569: 67 of cap free
Amount of items: 2
Items: 
Size: 579590 Color: 9
Size: 420344 Color: 3

Bin 2570: 67 of cap free
Amount of items: 2
Items: 
Size: 584749 Color: 1
Size: 415185 Color: 5

Bin 2571: 67 of cap free
Amount of items: 2
Items: 
Size: 627329 Color: 11
Size: 372605 Color: 2

Bin 2572: 67 of cap free
Amount of items: 2
Items: 
Size: 648347 Color: 13
Size: 351587 Color: 12

Bin 2573: 67 of cap free
Amount of items: 2
Items: 
Size: 713081 Color: 19
Size: 286853 Color: 7

Bin 2574: 68 of cap free
Amount of items: 2
Items: 
Size: 769140 Color: 11
Size: 230793 Color: 13

Bin 2575: 68 of cap free
Amount of items: 2
Items: 
Size: 583638 Color: 9
Size: 416295 Color: 13

Bin 2576: 68 of cap free
Amount of items: 2
Items: 
Size: 778112 Color: 9
Size: 221821 Color: 15

Bin 2577: 68 of cap free
Amount of items: 3
Items: 
Size: 630440 Color: 8
Size: 185012 Color: 18
Size: 184481 Color: 4

Bin 2578: 68 of cap free
Amount of items: 3
Items: 
Size: 690423 Color: 5
Size: 160176 Color: 10
Size: 149334 Color: 11

Bin 2579: 68 of cap free
Amount of items: 2
Items: 
Size: 500603 Color: 15
Size: 499330 Color: 0

Bin 2580: 68 of cap free
Amount of items: 2
Items: 
Size: 527051 Color: 15
Size: 472882 Color: 7

Bin 2581: 68 of cap free
Amount of items: 2
Items: 
Size: 539421 Color: 18
Size: 460512 Color: 3

Bin 2582: 68 of cap free
Amount of items: 2
Items: 
Size: 541766 Color: 11
Size: 458167 Color: 2

Bin 2583: 68 of cap free
Amount of items: 2
Items: 
Size: 575626 Color: 12
Size: 424307 Color: 1

Bin 2584: 68 of cap free
Amount of items: 2
Items: 
Size: 588646 Color: 4
Size: 411287 Color: 10

Bin 2585: 68 of cap free
Amount of items: 2
Items: 
Size: 608033 Color: 17
Size: 391900 Color: 19

Bin 2586: 68 of cap free
Amount of items: 2
Items: 
Size: 637246 Color: 11
Size: 362687 Color: 9

Bin 2587: 68 of cap free
Amount of items: 2
Items: 
Size: 707791 Color: 5
Size: 292142 Color: 18

Bin 2588: 69 of cap free
Amount of items: 2
Items: 
Size: 751427 Color: 2
Size: 248505 Color: 9

Bin 2589: 69 of cap free
Amount of items: 2
Items: 
Size: 592938 Color: 18
Size: 406994 Color: 1

Bin 2590: 69 of cap free
Amount of items: 2
Items: 
Size: 645160 Color: 10
Size: 354772 Color: 19

Bin 2591: 69 of cap free
Amount of items: 2
Items: 
Size: 510467 Color: 0
Size: 489465 Color: 11

Bin 2592: 69 of cap free
Amount of items: 2
Items: 
Size: 516827 Color: 3
Size: 483105 Color: 6

Bin 2593: 69 of cap free
Amount of items: 2
Items: 
Size: 574275 Color: 2
Size: 425657 Color: 6

Bin 2594: 69 of cap free
Amount of items: 2
Items: 
Size: 575698 Color: 11
Size: 424234 Color: 6

Bin 2595: 69 of cap free
Amount of items: 2
Items: 
Size: 580193 Color: 11
Size: 419739 Color: 8

Bin 2596: 69 of cap free
Amount of items: 2
Items: 
Size: 610011 Color: 3
Size: 389921 Color: 4

Bin 2597: 69 of cap free
Amount of items: 2
Items: 
Size: 614846 Color: 9
Size: 385086 Color: 1

Bin 2598: 69 of cap free
Amount of items: 2
Items: 
Size: 654181 Color: 6
Size: 345751 Color: 4

Bin 2599: 69 of cap free
Amount of items: 2
Items: 
Size: 695119 Color: 15
Size: 304813 Color: 2

Bin 2600: 69 of cap free
Amount of items: 2
Items: 
Size: 711257 Color: 9
Size: 288675 Color: 1

Bin 2601: 69 of cap free
Amount of items: 2
Items: 
Size: 763476 Color: 6
Size: 236456 Color: 9

Bin 2602: 69 of cap free
Amount of items: 2
Items: 
Size: 770634 Color: 8
Size: 229298 Color: 11

Bin 2603: 70 of cap free
Amount of items: 2
Items: 
Size: 503153 Color: 12
Size: 496778 Color: 19

Bin 2604: 70 of cap free
Amount of items: 2
Items: 
Size: 524537 Color: 10
Size: 475394 Color: 5

Bin 2605: 70 of cap free
Amount of items: 2
Items: 
Size: 526188 Color: 12
Size: 473743 Color: 9

Bin 2606: 70 of cap free
Amount of items: 2
Items: 
Size: 543700 Color: 19
Size: 456231 Color: 9

Bin 2607: 70 of cap free
Amount of items: 2
Items: 
Size: 548172 Color: 2
Size: 451759 Color: 3

Bin 2608: 70 of cap free
Amount of items: 2
Items: 
Size: 557499 Color: 9
Size: 442432 Color: 0

Bin 2609: 70 of cap free
Amount of items: 2
Items: 
Size: 584616 Color: 3
Size: 415315 Color: 1

Bin 2610: 70 of cap free
Amount of items: 2
Items: 
Size: 591367 Color: 3
Size: 408564 Color: 16

Bin 2611: 70 of cap free
Amount of items: 2
Items: 
Size: 596240 Color: 18
Size: 403691 Color: 9

Bin 2612: 70 of cap free
Amount of items: 2
Items: 
Size: 602104 Color: 4
Size: 397827 Color: 15

Bin 2613: 70 of cap free
Amount of items: 2
Items: 
Size: 602960 Color: 15
Size: 396971 Color: 17

Bin 2614: 70 of cap free
Amount of items: 2
Items: 
Size: 646395 Color: 4
Size: 353536 Color: 18

Bin 2615: 70 of cap free
Amount of items: 2
Items: 
Size: 685475 Color: 6
Size: 314456 Color: 19

Bin 2616: 71 of cap free
Amount of items: 2
Items: 
Size: 612254 Color: 5
Size: 387676 Color: 0

Bin 2617: 71 of cap free
Amount of items: 2
Items: 
Size: 694417 Color: 11
Size: 305513 Color: 10

Bin 2618: 71 of cap free
Amount of items: 2
Items: 
Size: 780308 Color: 14
Size: 219622 Color: 10

Bin 2619: 71 of cap free
Amount of items: 3
Items: 
Size: 686355 Color: 11
Size: 157208 Color: 18
Size: 156367 Color: 16

Bin 2620: 71 of cap free
Amount of items: 2
Items: 
Size: 704298 Color: 2
Size: 295632 Color: 5

Bin 2621: 71 of cap free
Amount of items: 2
Items: 
Size: 608515 Color: 10
Size: 391415 Color: 12

Bin 2622: 71 of cap free
Amount of items: 2
Items: 
Size: 635593 Color: 15
Size: 364337 Color: 0

Bin 2623: 71 of cap free
Amount of items: 2
Items: 
Size: 511943 Color: 16
Size: 487987 Color: 19

Bin 2624: 71 of cap free
Amount of items: 2
Items: 
Size: 613675 Color: 6
Size: 386255 Color: 17

Bin 2625: 71 of cap free
Amount of items: 2
Items: 
Size: 500486 Color: 19
Size: 499444 Color: 17

Bin 2626: 71 of cap free
Amount of items: 2
Items: 
Size: 501783 Color: 15
Size: 498147 Color: 6

Bin 2627: 71 of cap free
Amount of items: 2
Items: 
Size: 514711 Color: 6
Size: 485219 Color: 3

Bin 2628: 71 of cap free
Amount of items: 2
Items: 
Size: 574097 Color: 15
Size: 425833 Color: 13

Bin 2629: 71 of cap free
Amount of items: 2
Items: 
Size: 589159 Color: 8
Size: 410771 Color: 6

Bin 2630: 71 of cap free
Amount of items: 2
Items: 
Size: 619869 Color: 17
Size: 380061 Color: 10

Bin 2631: 71 of cap free
Amount of items: 2
Items: 
Size: 633297 Color: 5
Size: 366633 Color: 8

Bin 2632: 71 of cap free
Amount of items: 2
Items: 
Size: 677482 Color: 10
Size: 322448 Color: 4

Bin 2633: 71 of cap free
Amount of items: 2
Items: 
Size: 741484 Color: 10
Size: 258446 Color: 15

Bin 2634: 72 of cap free
Amount of items: 2
Items: 
Size: 744976 Color: 19
Size: 254953 Color: 16

Bin 2635: 72 of cap free
Amount of items: 2
Items: 
Size: 793904 Color: 0
Size: 206025 Color: 18

Bin 2636: 72 of cap free
Amount of items: 2
Items: 
Size: 730606 Color: 19
Size: 269323 Color: 17

Bin 2637: 72 of cap free
Amount of items: 2
Items: 
Size: 691951 Color: 0
Size: 307978 Color: 10

Bin 2638: 72 of cap free
Amount of items: 2
Items: 
Size: 530300 Color: 5
Size: 469629 Color: 19

Bin 2639: 72 of cap free
Amount of items: 2
Items: 
Size: 530538 Color: 9
Size: 469391 Color: 8

Bin 2640: 72 of cap free
Amount of items: 2
Items: 
Size: 559339 Color: 13
Size: 440590 Color: 2

Bin 2641: 72 of cap free
Amount of items: 2
Items: 
Size: 565578 Color: 19
Size: 434351 Color: 10

Bin 2642: 72 of cap free
Amount of items: 2
Items: 
Size: 575126 Color: 8
Size: 424803 Color: 12

Bin 2643: 72 of cap free
Amount of items: 2
Items: 
Size: 600754 Color: 8
Size: 399175 Color: 3

Bin 2644: 72 of cap free
Amount of items: 2
Items: 
Size: 688198 Color: 2
Size: 311731 Color: 18

Bin 2645: 72 of cap free
Amount of items: 2
Items: 
Size: 723709 Color: 1
Size: 276220 Color: 19

Bin 2646: 73 of cap free
Amount of items: 2
Items: 
Size: 760559 Color: 6
Size: 239369 Color: 7

Bin 2647: 73 of cap free
Amount of items: 2
Items: 
Size: 748165 Color: 8
Size: 251763 Color: 4

Bin 2648: 73 of cap free
Amount of items: 2
Items: 
Size: 555004 Color: 4
Size: 444924 Color: 9

Bin 2649: 73 of cap free
Amount of items: 2
Items: 
Size: 585343 Color: 15
Size: 414585 Color: 13

Bin 2650: 73 of cap free
Amount of items: 2
Items: 
Size: 604965 Color: 0
Size: 394963 Color: 11

Bin 2651: 73 of cap free
Amount of items: 2
Items: 
Size: 646624 Color: 0
Size: 353304 Color: 8

Bin 2652: 73 of cap free
Amount of items: 2
Items: 
Size: 692068 Color: 11
Size: 307860 Color: 14

Bin 2653: 74 of cap free
Amount of items: 2
Items: 
Size: 799840 Color: 14
Size: 200087 Color: 19

Bin 2654: 74 of cap free
Amount of items: 3
Items: 
Size: 418383 Color: 4
Size: 296287 Color: 3
Size: 285257 Color: 5

Bin 2655: 74 of cap free
Amount of items: 2
Items: 
Size: 528303 Color: 6
Size: 471624 Color: 17

Bin 2656: 74 of cap free
Amount of items: 2
Items: 
Size: 761917 Color: 11
Size: 238010 Color: 6

Bin 2657: 74 of cap free
Amount of items: 2
Items: 
Size: 768180 Color: 5
Size: 231747 Color: 14

Bin 2658: 74 of cap free
Amount of items: 2
Items: 
Size: 534338 Color: 1
Size: 465589 Color: 11

Bin 2659: 74 of cap free
Amount of items: 2
Items: 
Size: 534606 Color: 5
Size: 465321 Color: 13

Bin 2660: 74 of cap free
Amount of items: 2
Items: 
Size: 548792 Color: 6
Size: 451135 Color: 11

Bin 2661: 74 of cap free
Amount of items: 2
Items: 
Size: 566736 Color: 3
Size: 433191 Color: 4

Bin 2662: 75 of cap free
Amount of items: 2
Items: 
Size: 640040 Color: 0
Size: 359886 Color: 4

Bin 2663: 75 of cap free
Amount of items: 2
Items: 
Size: 504749 Color: 2
Size: 495177 Color: 12

Bin 2664: 75 of cap free
Amount of items: 3
Items: 
Size: 373933 Color: 19
Size: 362171 Color: 0
Size: 263822 Color: 11

Bin 2665: 75 of cap free
Amount of items: 2
Items: 
Size: 502221 Color: 1
Size: 497705 Color: 8

Bin 2666: 75 of cap free
Amount of items: 2
Items: 
Size: 516408 Color: 9
Size: 483518 Color: 0

Bin 2667: 75 of cap free
Amount of items: 2
Items: 
Size: 536884 Color: 0
Size: 463042 Color: 14

Bin 2668: 75 of cap free
Amount of items: 2
Items: 
Size: 687852 Color: 15
Size: 312074 Color: 10

Bin 2669: 75 of cap free
Amount of items: 2
Items: 
Size: 711750 Color: 2
Size: 288176 Color: 6

Bin 2670: 75 of cap free
Amount of items: 2
Items: 
Size: 758776 Color: 15
Size: 241150 Color: 17

Bin 2671: 75 of cap free
Amount of items: 2
Items: 
Size: 777040 Color: 9
Size: 222886 Color: 0

Bin 2672: 75 of cap free
Amount of items: 2
Items: 
Size: 784908 Color: 6
Size: 215018 Color: 11

Bin 2673: 76 of cap free
Amount of items: 3
Items: 
Size: 660160 Color: 12
Size: 170873 Color: 16
Size: 168892 Color: 2

Bin 2674: 76 of cap free
Amount of items: 2
Items: 
Size: 680010 Color: 18
Size: 319915 Color: 10

Bin 2675: 76 of cap free
Amount of items: 2
Items: 
Size: 662905 Color: 10
Size: 337020 Color: 2

Bin 2676: 76 of cap free
Amount of items: 2
Items: 
Size: 682952 Color: 12
Size: 316973 Color: 0

Bin 2677: 76 of cap free
Amount of items: 2
Items: 
Size: 716576 Color: 13
Size: 283349 Color: 2

Bin 2678: 77 of cap free
Amount of items: 3
Items: 
Size: 684080 Color: 17
Size: 170715 Color: 14
Size: 145129 Color: 3

Bin 2679: 77 of cap free
Amount of items: 3
Items: 
Size: 793642 Color: 8
Size: 105666 Color: 16
Size: 100616 Color: 0

Bin 2680: 77 of cap free
Amount of items: 2
Items: 
Size: 513555 Color: 16
Size: 486369 Color: 6

Bin 2681: 77 of cap free
Amount of items: 2
Items: 
Size: 579933 Color: 14
Size: 419991 Color: 17

Bin 2682: 77 of cap free
Amount of items: 2
Items: 
Size: 593426 Color: 2
Size: 406498 Color: 18

Bin 2683: 77 of cap free
Amount of items: 2
Items: 
Size: 621666 Color: 11
Size: 378258 Color: 5

Bin 2684: 77 of cap free
Amount of items: 2
Items: 
Size: 641663 Color: 18
Size: 358261 Color: 1

Bin 2685: 77 of cap free
Amount of items: 2
Items: 
Size: 687438 Color: 17
Size: 312486 Color: 18

Bin 2686: 77 of cap free
Amount of items: 2
Items: 
Size: 767602 Color: 13
Size: 232322 Color: 11

Bin 2687: 78 of cap free
Amount of items: 3
Items: 
Size: 613298 Color: 8
Size: 232709 Color: 18
Size: 153916 Color: 4

Bin 2688: 78 of cap free
Amount of items: 2
Items: 
Size: 738354 Color: 3
Size: 261569 Color: 6

Bin 2689: 78 of cap free
Amount of items: 2
Items: 
Size: 628811 Color: 16
Size: 371112 Color: 7

Bin 2690: 78 of cap free
Amount of items: 2
Items: 
Size: 517678 Color: 17
Size: 482245 Color: 12

Bin 2691: 78 of cap free
Amount of items: 2
Items: 
Size: 549681 Color: 3
Size: 450242 Color: 18

Bin 2692: 78 of cap free
Amount of items: 2
Items: 
Size: 576262 Color: 16
Size: 423661 Color: 4

Bin 2693: 78 of cap free
Amount of items: 2
Items: 
Size: 750627 Color: 6
Size: 249296 Color: 0

Bin 2694: 79 of cap free
Amount of items: 2
Items: 
Size: 640469 Color: 17
Size: 359453 Color: 8

Bin 2695: 79 of cap free
Amount of items: 2
Items: 
Size: 670637 Color: 11
Size: 329285 Color: 16

Bin 2696: 79 of cap free
Amount of items: 2
Items: 
Size: 791748 Color: 15
Size: 208174 Color: 18

Bin 2697: 79 of cap free
Amount of items: 2
Items: 
Size: 760007 Color: 13
Size: 239915 Color: 9

Bin 2698: 79 of cap free
Amount of items: 3
Items: 
Size: 536233 Color: 18
Size: 243412 Color: 15
Size: 220277 Color: 3

Bin 2699: 79 of cap free
Amount of items: 2
Items: 
Size: 785699 Color: 7
Size: 214223 Color: 5

Bin 2700: 79 of cap free
Amount of items: 2
Items: 
Size: 519800 Color: 15
Size: 480122 Color: 6

Bin 2701: 79 of cap free
Amount of items: 2
Items: 
Size: 540898 Color: 19
Size: 459024 Color: 3

Bin 2702: 79 of cap free
Amount of items: 2
Items: 
Size: 547078 Color: 0
Size: 452844 Color: 1

Bin 2703: 79 of cap free
Amount of items: 2
Items: 
Size: 554452 Color: 13
Size: 445470 Color: 16

Bin 2704: 79 of cap free
Amount of items: 2
Items: 
Size: 556756 Color: 8
Size: 443166 Color: 18

Bin 2705: 79 of cap free
Amount of items: 2
Items: 
Size: 565440 Color: 17
Size: 434482 Color: 18

Bin 2706: 79 of cap free
Amount of items: 2
Items: 
Size: 565910 Color: 8
Size: 434012 Color: 19

Bin 2707: 79 of cap free
Amount of items: 2
Items: 
Size: 580978 Color: 19
Size: 418944 Color: 6

Bin 2708: 79 of cap free
Amount of items: 3
Items: 
Size: 689402 Color: 8
Size: 155282 Color: 7
Size: 155238 Color: 14

Bin 2709: 79 of cap free
Amount of items: 2
Items: 
Size: 767070 Color: 9
Size: 232852 Color: 7

Bin 2710: 80 of cap free
Amount of items: 2
Items: 
Size: 694507 Color: 3
Size: 305414 Color: 6

Bin 2711: 80 of cap free
Amount of items: 2
Items: 
Size: 756069 Color: 7
Size: 243852 Color: 5

Bin 2712: 80 of cap free
Amount of items: 2
Items: 
Size: 568537 Color: 7
Size: 431384 Color: 3

Bin 2713: 80 of cap free
Amount of items: 2
Items: 
Size: 792090 Color: 17
Size: 207831 Color: 3

Bin 2714: 80 of cap free
Amount of items: 2
Items: 
Size: 590409 Color: 7
Size: 409512 Color: 15

Bin 2715: 80 of cap free
Amount of items: 2
Items: 
Size: 604787 Color: 5
Size: 395134 Color: 16

Bin 2716: 80 of cap free
Amount of items: 2
Items: 
Size: 618581 Color: 6
Size: 381340 Color: 2

Bin 2717: 80 of cap free
Amount of items: 2
Items: 
Size: 734231 Color: 15
Size: 265690 Color: 5

Bin 2718: 81 of cap free
Amount of items: 2
Items: 
Size: 793206 Color: 7
Size: 206714 Color: 12

Bin 2719: 81 of cap free
Amount of items: 3
Items: 
Size: 749101 Color: 15
Size: 133877 Color: 10
Size: 116942 Color: 11

Bin 2720: 81 of cap free
Amount of items: 2
Items: 
Size: 729073 Color: 7
Size: 270847 Color: 10

Bin 2721: 81 of cap free
Amount of items: 2
Items: 
Size: 744843 Color: 6
Size: 255077 Color: 19

Bin 2722: 81 of cap free
Amount of items: 2
Items: 
Size: 665604 Color: 8
Size: 334316 Color: 10

Bin 2723: 81 of cap free
Amount of items: 3
Items: 
Size: 372148 Color: 5
Size: 361505 Color: 13
Size: 266267 Color: 4

Bin 2724: 81 of cap free
Amount of items: 2
Items: 
Size: 575958 Color: 7
Size: 423962 Color: 1

Bin 2725: 81 of cap free
Amount of items: 2
Items: 
Size: 576761 Color: 11
Size: 423159 Color: 18

Bin 2726: 81 of cap free
Amount of items: 2
Items: 
Size: 583627 Color: 12
Size: 416293 Color: 10

Bin 2727: 81 of cap free
Amount of items: 2
Items: 
Size: 599696 Color: 7
Size: 400224 Color: 10

Bin 2728: 81 of cap free
Amount of items: 2
Items: 
Size: 661341 Color: 10
Size: 338579 Color: 4

Bin 2729: 81 of cap free
Amount of items: 2
Items: 
Size: 673693 Color: 19
Size: 326227 Color: 1

Bin 2730: 81 of cap free
Amount of items: 2
Items: 
Size: 740521 Color: 9
Size: 259399 Color: 10

Bin 2731: 81 of cap free
Amount of items: 2
Items: 
Size: 747214 Color: 15
Size: 252706 Color: 16

Bin 2732: 81 of cap free
Amount of items: 2
Items: 
Size: 757870 Color: 3
Size: 242050 Color: 16

Bin 2733: 82 of cap free
Amount of items: 4
Items: 
Size: 568668 Color: 3
Size: 157584 Color: 10
Size: 143860 Color: 14
Size: 129807 Color: 16

Bin 2734: 82 of cap free
Amount of items: 2
Items: 
Size: 635705 Color: 17
Size: 364214 Color: 3

Bin 2735: 82 of cap free
Amount of items: 2
Items: 
Size: 799843 Color: 1
Size: 200076 Color: 2

Bin 2736: 82 of cap free
Amount of items: 2
Items: 
Size: 695407 Color: 4
Size: 304512 Color: 19

Bin 2737: 82 of cap free
Amount of items: 2
Items: 
Size: 728905 Color: 8
Size: 271014 Color: 1

Bin 2738: 82 of cap free
Amount of items: 2
Items: 
Size: 680971 Color: 15
Size: 318948 Color: 13

Bin 2739: 82 of cap free
Amount of items: 2
Items: 
Size: 528621 Color: 6
Size: 471298 Color: 7

Bin 2740: 82 of cap free
Amount of items: 2
Items: 
Size: 780923 Color: 0
Size: 218996 Color: 7

Bin 2741: 82 of cap free
Amount of items: 2
Items: 
Size: 512771 Color: 3
Size: 487148 Color: 18

Bin 2742: 82 of cap free
Amount of items: 2
Items: 
Size: 537448 Color: 3
Size: 462471 Color: 15

Bin 2743: 82 of cap free
Amount of items: 2
Items: 
Size: 557195 Color: 10
Size: 442724 Color: 12

Bin 2744: 82 of cap free
Amount of items: 2
Items: 
Size: 558371 Color: 8
Size: 441548 Color: 11

Bin 2745: 82 of cap free
Amount of items: 2
Items: 
Size: 707111 Color: 1
Size: 292808 Color: 9

Bin 2746: 82 of cap free
Amount of items: 2
Items: 
Size: 756183 Color: 10
Size: 243736 Color: 12

Bin 2747: 82 of cap free
Amount of items: 2
Items: 
Size: 758516 Color: 10
Size: 241403 Color: 18

Bin 2748: 83 of cap free
Amount of items: 2
Items: 
Size: 752625 Color: 10
Size: 247293 Color: 2

Bin 2749: 83 of cap free
Amount of items: 2
Items: 
Size: 776615 Color: 11
Size: 223303 Color: 7

Bin 2750: 83 of cap free
Amount of items: 2
Items: 
Size: 696231 Color: 6
Size: 303687 Color: 3

Bin 2751: 83 of cap free
Amount of items: 2
Items: 
Size: 559628 Color: 6
Size: 440290 Color: 19

Bin 2752: 83 of cap free
Amount of items: 2
Items: 
Size: 598543 Color: 2
Size: 401375 Color: 5

Bin 2753: 83 of cap free
Amount of items: 2
Items: 
Size: 607617 Color: 15
Size: 392301 Color: 11

Bin 2754: 83 of cap free
Amount of items: 2
Items: 
Size: 619396 Color: 17
Size: 380522 Color: 9

Bin 2755: 83 of cap free
Amount of items: 2
Items: 
Size: 682424 Color: 9
Size: 317494 Color: 10

Bin 2756: 83 of cap free
Amount of items: 2
Items: 
Size: 702661 Color: 19
Size: 297257 Color: 6

Bin 2757: 83 of cap free
Amount of items: 2
Items: 
Size: 760870 Color: 12
Size: 239048 Color: 15

Bin 2758: 83 of cap free
Amount of items: 2
Items: 
Size: 793489 Color: 8
Size: 206429 Color: 18

Bin 2759: 84 of cap free
Amount of items: 3
Items: 
Size: 590666 Color: 17
Size: 280554 Color: 11
Size: 128697 Color: 11

Bin 2760: 84 of cap free
Amount of items: 2
Items: 
Size: 703066 Color: 14
Size: 296851 Color: 12

Bin 2761: 84 of cap free
Amount of items: 2
Items: 
Size: 681720 Color: 1
Size: 318197 Color: 11

Bin 2762: 84 of cap free
Amount of items: 2
Items: 
Size: 776890 Color: 6
Size: 223027 Color: 8

Bin 2763: 84 of cap free
Amount of items: 2
Items: 
Size: 646303 Color: 12
Size: 353614 Color: 14

Bin 2764: 84 of cap free
Amount of items: 2
Items: 
Size: 744225 Color: 2
Size: 255692 Color: 9

Bin 2765: 84 of cap free
Amount of items: 2
Items: 
Size: 612153 Color: 7
Size: 387764 Color: 13

Bin 2766: 84 of cap free
Amount of items: 2
Items: 
Size: 649339 Color: 5
Size: 350578 Color: 16

Bin 2767: 84 of cap free
Amount of items: 2
Items: 
Size: 530553 Color: 2
Size: 469364 Color: 14

Bin 2768: 84 of cap free
Amount of items: 2
Items: 
Size: 530912 Color: 2
Size: 469005 Color: 10

Bin 2769: 84 of cap free
Amount of items: 2
Items: 
Size: 558659 Color: 3
Size: 441258 Color: 15

Bin 2770: 84 of cap free
Amount of items: 2
Items: 
Size: 600473 Color: 18
Size: 399444 Color: 15

Bin 2771: 84 of cap free
Amount of items: 2
Items: 
Size: 603115 Color: 5
Size: 396802 Color: 16

Bin 2772: 84 of cap free
Amount of items: 2
Items: 
Size: 611096 Color: 5
Size: 388821 Color: 16

Bin 2773: 84 of cap free
Amount of items: 2
Items: 
Size: 641895 Color: 0
Size: 358022 Color: 9

Bin 2774: 84 of cap free
Amount of items: 2
Items: 
Size: 650063 Color: 5
Size: 349854 Color: 6

Bin 2775: 85 of cap free
Amount of items: 3
Items: 
Size: 596159 Color: 3
Size: 219855 Color: 5
Size: 183902 Color: 1

Bin 2776: 85 of cap free
Amount of items: 2
Items: 
Size: 665688 Color: 8
Size: 334228 Color: 0

Bin 2777: 85 of cap free
Amount of items: 3
Items: 
Size: 689980 Color: 1
Size: 154982 Color: 3
Size: 154954 Color: 3

Bin 2778: 85 of cap free
Amount of items: 2
Items: 
Size: 752087 Color: 12
Size: 247829 Color: 7

Bin 2779: 85 of cap free
Amount of items: 2
Items: 
Size: 542090 Color: 7
Size: 457826 Color: 0

Bin 2780: 85 of cap free
Amount of items: 2
Items: 
Size: 552470 Color: 12
Size: 447446 Color: 6

Bin 2781: 85 of cap free
Amount of items: 2
Items: 
Size: 565140 Color: 12
Size: 434776 Color: 15

Bin 2782: 85 of cap free
Amount of items: 2
Items: 
Size: 592246 Color: 16
Size: 407670 Color: 19

Bin 2783: 85 of cap free
Amount of items: 2
Items: 
Size: 599198 Color: 16
Size: 400718 Color: 3

Bin 2784: 85 of cap free
Amount of items: 2
Items: 
Size: 720542 Color: 6
Size: 279374 Color: 15

Bin 2785: 85 of cap free
Amount of items: 2
Items: 
Size: 721812 Color: 2
Size: 278104 Color: 18

Bin 2786: 85 of cap free
Amount of items: 2
Items: 
Size: 723434 Color: 15
Size: 276482 Color: 14

Bin 2787: 85 of cap free
Amount of items: 2
Items: 
Size: 758539 Color: 18
Size: 241377 Color: 13

Bin 2788: 85 of cap free
Amount of items: 2
Items: 
Size: 772422 Color: 14
Size: 227494 Color: 5

Bin 2789: 86 of cap free
Amount of items: 2
Items: 
Size: 651875 Color: 16
Size: 348040 Color: 18

Bin 2790: 86 of cap free
Amount of items: 3
Items: 
Size: 698263 Color: 11
Size: 150938 Color: 18
Size: 150714 Color: 17

Bin 2791: 86 of cap free
Amount of items: 2
Items: 
Size: 688621 Color: 14
Size: 311294 Color: 15

Bin 2792: 86 of cap free
Amount of items: 2
Items: 
Size: 653631 Color: 11
Size: 346284 Color: 16

Bin 2793: 86 of cap free
Amount of items: 2
Items: 
Size: 508353 Color: 16
Size: 491562 Color: 15

Bin 2794: 86 of cap free
Amount of items: 2
Items: 
Size: 524948 Color: 14
Size: 474967 Color: 16

Bin 2795: 86 of cap free
Amount of items: 2
Items: 
Size: 537828 Color: 16
Size: 462087 Color: 6

Bin 2796: 86 of cap free
Amount of items: 2
Items: 
Size: 540303 Color: 2
Size: 459612 Color: 17

Bin 2797: 86 of cap free
Amount of items: 2
Items: 
Size: 545653 Color: 17
Size: 454262 Color: 8

Bin 2798: 86 of cap free
Amount of items: 2
Items: 
Size: 550114 Color: 13
Size: 449801 Color: 19

Bin 2799: 86 of cap free
Amount of items: 2
Items: 
Size: 573133 Color: 16
Size: 426782 Color: 19

Bin 2800: 86 of cap free
Amount of items: 2
Items: 
Size: 580977 Color: 0
Size: 418938 Color: 9

Bin 2801: 86 of cap free
Amount of items: 2
Items: 
Size: 606487 Color: 1
Size: 393428 Color: 6

Bin 2802: 86 of cap free
Amount of items: 2
Items: 
Size: 616091 Color: 5
Size: 383824 Color: 18

Bin 2803: 86 of cap free
Amount of items: 2
Items: 
Size: 648328 Color: 4
Size: 351587 Color: 1

Bin 2804: 86 of cap free
Amount of items: 2
Items: 
Size: 658624 Color: 0
Size: 341291 Color: 6

Bin 2805: 86 of cap free
Amount of items: 2
Items: 
Size: 673315 Color: 11
Size: 326600 Color: 4

Bin 2806: 86 of cap free
Amount of items: 2
Items: 
Size: 777565 Color: 13
Size: 222350 Color: 4

Bin 2807: 87 of cap free
Amount of items: 2
Items: 
Size: 797972 Color: 9
Size: 201942 Color: 5

Bin 2808: 87 of cap free
Amount of items: 2
Items: 
Size: 572252 Color: 12
Size: 427662 Color: 15

Bin 2809: 87 of cap free
Amount of items: 2
Items: 
Size: 762845 Color: 12
Size: 237069 Color: 13

Bin 2810: 87 of cap free
Amount of items: 2
Items: 
Size: 609844 Color: 16
Size: 390070 Color: 19

Bin 2811: 87 of cap free
Amount of items: 2
Items: 
Size: 504236 Color: 10
Size: 495678 Color: 6

Bin 2812: 87 of cap free
Amount of items: 2
Items: 
Size: 582282 Color: 16
Size: 417632 Color: 8

Bin 2813: 87 of cap free
Amount of items: 2
Items: 
Size: 638594 Color: 4
Size: 361320 Color: 13

Bin 2814: 87 of cap free
Amount of items: 2
Items: 
Size: 643515 Color: 12
Size: 356399 Color: 2

Bin 2815: 87 of cap free
Amount of items: 2
Items: 
Size: 643942 Color: 0
Size: 355972 Color: 3

Bin 2816: 87 of cap free
Amount of items: 2
Items: 
Size: 770634 Color: 5
Size: 229280 Color: 12

Bin 2817: 87 of cap free
Amount of items: 2
Items: 
Size: 794559 Color: 8
Size: 205355 Color: 3

Bin 2818: 88 of cap free
Amount of items: 3
Items: 
Size: 746531 Color: 2
Size: 136069 Color: 16
Size: 117313 Color: 0

Bin 2819: 88 of cap free
Amount of items: 2
Items: 
Size: 778768 Color: 2
Size: 221145 Color: 8

Bin 2820: 88 of cap free
Amount of items: 2
Items: 
Size: 783663 Color: 5
Size: 216250 Color: 15

Bin 2821: 88 of cap free
Amount of items: 2
Items: 
Size: 559445 Color: 3
Size: 440468 Color: 15

Bin 2822: 88 of cap free
Amount of items: 2
Items: 
Size: 575288 Color: 10
Size: 424625 Color: 18

Bin 2823: 89 of cap free
Amount of items: 2
Items: 
Size: 758911 Color: 11
Size: 241001 Color: 18

Bin 2824: 89 of cap free
Amount of items: 2
Items: 
Size: 775108 Color: 18
Size: 224804 Color: 3

Bin 2825: 89 of cap free
Amount of items: 2
Items: 
Size: 600683 Color: 17
Size: 399229 Color: 16

Bin 2826: 90 of cap free
Amount of items: 2
Items: 
Size: 729791 Color: 13
Size: 270120 Color: 16

Bin 2827: 90 of cap free
Amount of items: 2
Items: 
Size: 533907 Color: 3
Size: 466004 Color: 13

Bin 2828: 90 of cap free
Amount of items: 2
Items: 
Size: 799312 Color: 11
Size: 200599 Color: 16

Bin 2829: 90 of cap free
Amount of items: 2
Items: 
Size: 642614 Color: 9
Size: 357297 Color: 14

Bin 2830: 90 of cap free
Amount of items: 2
Items: 
Size: 526867 Color: 15
Size: 473044 Color: 7

Bin 2831: 90 of cap free
Amount of items: 2
Items: 
Size: 507924 Color: 5
Size: 491987 Color: 1

Bin 2832: 90 of cap free
Amount of items: 2
Items: 
Size: 578172 Color: 8
Size: 421739 Color: 16

Bin 2833: 90 of cap free
Amount of items: 2
Items: 
Size: 619493 Color: 1
Size: 380418 Color: 7

Bin 2834: 90 of cap free
Amount of items: 2
Items: 
Size: 646826 Color: 0
Size: 353085 Color: 4

Bin 2835: 90 of cap free
Amount of items: 2
Items: 
Size: 786211 Color: 0
Size: 213700 Color: 5

Bin 2836: 91 of cap free
Amount of items: 2
Items: 
Size: 727164 Color: 4
Size: 272746 Color: 6

Bin 2837: 91 of cap free
Amount of items: 2
Items: 
Size: 728180 Color: 10
Size: 271730 Color: 13

Bin 2838: 91 of cap free
Amount of items: 2
Items: 
Size: 738929 Color: 14
Size: 260981 Color: 12

Bin 2839: 91 of cap free
Amount of items: 3
Items: 
Size: 628831 Color: 6
Size: 185540 Color: 2
Size: 185539 Color: 11

Bin 2840: 91 of cap free
Amount of items: 2
Items: 
Size: 666270 Color: 8
Size: 333640 Color: 13

Bin 2841: 91 of cap free
Amount of items: 2
Items: 
Size: 603214 Color: 19
Size: 396696 Color: 0

Bin 2842: 91 of cap free
Amount of items: 2
Items: 
Size: 532101 Color: 18
Size: 467809 Color: 9

Bin 2843: 91 of cap free
Amount of items: 2
Items: 
Size: 624924 Color: 19
Size: 374986 Color: 18

Bin 2844: 91 of cap free
Amount of items: 2
Items: 
Size: 663906 Color: 10
Size: 336004 Color: 0

Bin 2845: 91 of cap free
Amount of items: 2
Items: 
Size: 674949 Color: 7
Size: 324961 Color: 4

Bin 2846: 91 of cap free
Amount of items: 2
Items: 
Size: 694160 Color: 11
Size: 305750 Color: 0

Bin 2847: 91 of cap free
Amount of items: 2
Items: 
Size: 704870 Color: 14
Size: 295040 Color: 17

Bin 2848: 92 of cap free
Amount of items: 2
Items: 
Size: 594648 Color: 3
Size: 405261 Color: 13

Bin 2849: 92 of cap free
Amount of items: 2
Items: 
Size: 770120 Color: 7
Size: 229789 Color: 15

Bin 2850: 92 of cap free
Amount of items: 2
Items: 
Size: 589492 Color: 3
Size: 410417 Color: 10

Bin 2851: 92 of cap free
Amount of items: 2
Items: 
Size: 507433 Color: 13
Size: 492476 Color: 17

Bin 2852: 92 of cap free
Amount of items: 2
Items: 
Size: 533231 Color: 6
Size: 466678 Color: 18

Bin 2853: 92 of cap free
Amount of items: 2
Items: 
Size: 597058 Color: 12
Size: 402851 Color: 2

Bin 2854: 92 of cap free
Amount of items: 2
Items: 
Size: 523264 Color: 8
Size: 476645 Color: 0

Bin 2855: 92 of cap free
Amount of items: 2
Items: 
Size: 553754 Color: 18
Size: 446155 Color: 10

Bin 2856: 92 of cap free
Amount of items: 2
Items: 
Size: 600233 Color: 19
Size: 399676 Color: 18

Bin 2857: 92 of cap free
Amount of items: 2
Items: 
Size: 724865 Color: 5
Size: 275044 Color: 8

Bin 2858: 93 of cap free
Amount of items: 2
Items: 
Size: 654827 Color: 16
Size: 345081 Color: 12

Bin 2859: 93 of cap free
Amount of items: 3
Items: 
Size: 648606 Color: 16
Size: 179940 Color: 17
Size: 171362 Color: 4

Bin 2860: 93 of cap free
Amount of items: 2
Items: 
Size: 690407 Color: 17
Size: 309501 Color: 11

Bin 2861: 93 of cap free
Amount of items: 2
Items: 
Size: 782516 Color: 9
Size: 217392 Color: 12

Bin 2862: 93 of cap free
Amount of items: 2
Items: 
Size: 514944 Color: 19
Size: 484964 Color: 16

Bin 2863: 93 of cap free
Amount of items: 2
Items: 
Size: 540907 Color: 13
Size: 459001 Color: 16

Bin 2864: 93 of cap free
Amount of items: 2
Items: 
Size: 545505 Color: 4
Size: 454403 Color: 0

Bin 2865: 93 of cap free
Amount of items: 2
Items: 
Size: 545919 Color: 13
Size: 453989 Color: 8

Bin 2866: 93 of cap free
Amount of items: 2
Items: 
Size: 547943 Color: 11
Size: 451965 Color: 9

Bin 2867: 93 of cap free
Amount of items: 2
Items: 
Size: 568358 Color: 12
Size: 431550 Color: 19

Bin 2868: 93 of cap free
Amount of items: 2
Items: 
Size: 645681 Color: 19
Size: 354227 Color: 15

Bin 2869: 93 of cap free
Amount of items: 2
Items: 
Size: 718560 Color: 11
Size: 281348 Color: 7

Bin 2870: 93 of cap free
Amount of items: 2
Items: 
Size: 768344 Color: 4
Size: 231564 Color: 9

Bin 2871: 94 of cap free
Amount of items: 2
Items: 
Size: 745953 Color: 16
Size: 253954 Color: 3

Bin 2872: 94 of cap free
Amount of items: 2
Items: 
Size: 739985 Color: 16
Size: 259922 Color: 6

Bin 2873: 94 of cap free
Amount of items: 2
Items: 
Size: 533793 Color: 1
Size: 466114 Color: 18

Bin 2874: 94 of cap free
Amount of items: 2
Items: 
Size: 697493 Color: 12
Size: 302414 Color: 3

Bin 2875: 94 of cap free
Amount of items: 2
Items: 
Size: 513320 Color: 15
Size: 486587 Color: 17

Bin 2876: 94 of cap free
Amount of items: 2
Items: 
Size: 576959 Color: 4
Size: 422948 Color: 9

Bin 2877: 94 of cap free
Amount of items: 2
Items: 
Size: 582968 Color: 7
Size: 416939 Color: 14

Bin 2878: 94 of cap free
Amount of items: 2
Items: 
Size: 593183 Color: 15
Size: 406724 Color: 4

Bin 2879: 94 of cap free
Amount of items: 2
Items: 
Size: 646614 Color: 4
Size: 353293 Color: 15

Bin 2880: 94 of cap free
Amount of items: 2
Items: 
Size: 736030 Color: 8
Size: 263877 Color: 13

Bin 2881: 95 of cap free
Amount of items: 2
Items: 
Size: 668347 Color: 18
Size: 331559 Color: 15

Bin 2882: 95 of cap free
Amount of items: 2
Items: 
Size: 687730 Color: 16
Size: 312176 Color: 17

Bin 2883: 95 of cap free
Amount of items: 2
Items: 
Size: 500048 Color: 16
Size: 499858 Color: 18

Bin 2884: 95 of cap free
Amount of items: 2
Items: 
Size: 506928 Color: 5
Size: 492978 Color: 10

Bin 2885: 95 of cap free
Amount of items: 2
Items: 
Size: 515839 Color: 10
Size: 484067 Color: 2

Bin 2886: 95 of cap free
Amount of items: 2
Items: 
Size: 516035 Color: 10
Size: 483871 Color: 11

Bin 2887: 95 of cap free
Amount of items: 2
Items: 
Size: 519382 Color: 6
Size: 480524 Color: 4

Bin 2888: 95 of cap free
Amount of items: 2
Items: 
Size: 580666 Color: 0
Size: 419240 Color: 8

Bin 2889: 95 of cap free
Amount of items: 2
Items: 
Size: 584542 Color: 13
Size: 415364 Color: 9

Bin 2890: 95 of cap free
Amount of items: 2
Items: 
Size: 598318 Color: 18
Size: 401588 Color: 0

Bin 2891: 96 of cap free
Amount of items: 2
Items: 
Size: 749528 Color: 16
Size: 250377 Color: 9

Bin 2892: 96 of cap free
Amount of items: 2
Items: 
Size: 656728 Color: 15
Size: 343177 Color: 13

Bin 2893: 96 of cap free
Amount of items: 2
Items: 
Size: 751809 Color: 0
Size: 248096 Color: 8

Bin 2894: 96 of cap free
Amount of items: 2
Items: 
Size: 675089 Color: 18
Size: 324816 Color: 15

Bin 2895: 96 of cap free
Amount of items: 2
Items: 
Size: 513933 Color: 18
Size: 485972 Color: 13

Bin 2896: 96 of cap free
Amount of items: 2
Items: 
Size: 518378 Color: 9
Size: 481527 Color: 2

Bin 2897: 96 of cap free
Amount of items: 2
Items: 
Size: 691556 Color: 1
Size: 308349 Color: 2

Bin 2898: 97 of cap free
Amount of items: 2
Items: 
Size: 751422 Color: 4
Size: 248482 Color: 7

Bin 2899: 97 of cap free
Amount of items: 3
Items: 
Size: 714726 Color: 17
Size: 142631 Color: 12
Size: 142547 Color: 1

Bin 2900: 97 of cap free
Amount of items: 2
Items: 
Size: 695617 Color: 6
Size: 304287 Color: 7

Bin 2901: 97 of cap free
Amount of items: 2
Items: 
Size: 697854 Color: 11
Size: 302050 Color: 3

Bin 2902: 97 of cap free
Amount of items: 2
Items: 
Size: 576451 Color: 5
Size: 423453 Color: 3

Bin 2903: 97 of cap free
Amount of items: 2
Items: 
Size: 762427 Color: 12
Size: 237477 Color: 16

Bin 2904: 97 of cap free
Amount of items: 2
Items: 
Size: 500217 Color: 0
Size: 499687 Color: 19

Bin 2905: 97 of cap free
Amount of items: 2
Items: 
Size: 522789 Color: 8
Size: 477115 Color: 11

Bin 2906: 97 of cap free
Amount of items: 2
Items: 
Size: 535346 Color: 1
Size: 464558 Color: 19

Bin 2907: 97 of cap free
Amount of items: 2
Items: 
Size: 555200 Color: 19
Size: 444704 Color: 6

Bin 2908: 97 of cap free
Amount of items: 2
Items: 
Size: 559899 Color: 8
Size: 440005 Color: 7

Bin 2909: 97 of cap free
Amount of items: 2
Items: 
Size: 562838 Color: 13
Size: 437066 Color: 2

Bin 2910: 97 of cap free
Amount of items: 2
Items: 
Size: 631742 Color: 1
Size: 368162 Color: 17

Bin 2911: 97 of cap free
Amount of items: 2
Items: 
Size: 742469 Color: 1
Size: 257435 Color: 5

Bin 2912: 97 of cap free
Amount of items: 2
Items: 
Size: 746724 Color: 6
Size: 253180 Color: 8

Bin 2913: 98 of cap free
Amount of items: 2
Items: 
Size: 691941 Color: 8
Size: 307962 Color: 17

Bin 2914: 98 of cap free
Amount of items: 2
Items: 
Size: 637599 Color: 10
Size: 362304 Color: 15

Bin 2915: 98 of cap free
Amount of items: 2
Items: 
Size: 692933 Color: 14
Size: 306970 Color: 7

Bin 2916: 98 of cap free
Amount of items: 2
Items: 
Size: 799830 Color: 13
Size: 200073 Color: 10

Bin 2917: 98 of cap free
Amount of items: 2
Items: 
Size: 569687 Color: 13
Size: 430216 Color: 18

Bin 2918: 98 of cap free
Amount of items: 2
Items: 
Size: 571856 Color: 15
Size: 428047 Color: 11

Bin 2919: 98 of cap free
Amount of items: 2
Items: 
Size: 579192 Color: 18
Size: 420711 Color: 7

Bin 2920: 99 of cap free
Amount of items: 4
Items: 
Size: 352349 Color: 12
Size: 320144 Color: 7
Size: 183690 Color: 17
Size: 143719 Color: 1

Bin 2921: 99 of cap free
Amount of items: 3
Items: 
Size: 674569 Color: 4
Size: 199693 Color: 12
Size: 125640 Color: 3

Bin 2922: 99 of cap free
Amount of items: 2
Items: 
Size: 689518 Color: 15
Size: 310384 Color: 16

Bin 2923: 99 of cap free
Amount of items: 2
Items: 
Size: 766937 Color: 10
Size: 232965 Color: 6

Bin 2924: 99 of cap free
Amount of items: 2
Items: 
Size: 640301 Color: 16
Size: 359601 Color: 14

Bin 2925: 99 of cap free
Amount of items: 2
Items: 
Size: 549036 Color: 4
Size: 450866 Color: 17

Bin 2926: 100 of cap free
Amount of items: 2
Items: 
Size: 521975 Color: 12
Size: 477926 Color: 6

Bin 2927: 100 of cap free
Amount of items: 2
Items: 
Size: 798506 Color: 0
Size: 201395 Color: 14

Bin 2928: 100 of cap free
Amount of items: 2
Items: 
Size: 772948 Color: 15
Size: 226953 Color: 9

Bin 2929: 100 of cap free
Amount of items: 2
Items: 
Size: 544947 Color: 9
Size: 454954 Color: 2

Bin 2930: 100 of cap free
Amount of items: 2
Items: 
Size: 582601 Color: 16
Size: 417300 Color: 14

Bin 2931: 100 of cap free
Amount of items: 2
Items: 
Size: 603456 Color: 12
Size: 396445 Color: 0

Bin 2932: 100 of cap free
Amount of items: 2
Items: 
Size: 675687 Color: 9
Size: 324214 Color: 5

Bin 2933: 101 of cap free
Amount of items: 2
Items: 
Size: 766231 Color: 7
Size: 233669 Color: 10

Bin 2934: 101 of cap free
Amount of items: 2
Items: 
Size: 789818 Color: 6
Size: 210082 Color: 16

Bin 2935: 101 of cap free
Amount of items: 3
Items: 
Size: 377861 Color: 19
Size: 319828 Color: 5
Size: 302211 Color: 16

Bin 2936: 101 of cap free
Amount of items: 2
Items: 
Size: 736454 Color: 13
Size: 263446 Color: 12

Bin 2937: 101 of cap free
Amount of items: 2
Items: 
Size: 602511 Color: 10
Size: 397389 Color: 1

Bin 2938: 101 of cap free
Amount of items: 2
Items: 
Size: 615651 Color: 3
Size: 384249 Color: 12

Bin 2939: 101 of cap free
Amount of items: 2
Items: 
Size: 711869 Color: 7
Size: 288031 Color: 3

Bin 2940: 102 of cap free
Amount of items: 2
Items: 
Size: 707296 Color: 13
Size: 292603 Color: 14

Bin 2941: 102 of cap free
Amount of items: 2
Items: 
Size: 781431 Color: 7
Size: 218468 Color: 14

Bin 2942: 102 of cap free
Amount of items: 2
Items: 
Size: 517765 Color: 10
Size: 482134 Color: 3

Bin 2943: 102 of cap free
Amount of items: 2
Items: 
Size: 561389 Color: 17
Size: 438510 Color: 18

Bin 2944: 102 of cap free
Amount of items: 2
Items: 
Size: 623057 Color: 6
Size: 376842 Color: 11

Bin 2945: 102 of cap free
Amount of items: 2
Items: 
Size: 677185 Color: 15
Size: 322714 Color: 18

Bin 2946: 102 of cap free
Amount of items: 2
Items: 
Size: 677936 Color: 16
Size: 321963 Color: 19

Bin 2947: 103 of cap free
Amount of items: 3
Items: 
Size: 629387 Color: 16
Size: 229167 Color: 5
Size: 141344 Color: 18

Bin 2948: 103 of cap free
Amount of items: 3
Items: 
Size: 684631 Color: 3
Size: 162192 Color: 16
Size: 153075 Color: 1

Bin 2949: 103 of cap free
Amount of items: 3
Items: 
Size: 685197 Color: 1
Size: 186260 Color: 7
Size: 128441 Color: 10

Bin 2950: 103 of cap free
Amount of items: 2
Items: 
Size: 670942 Color: 14
Size: 328956 Color: 8

Bin 2951: 103 of cap free
Amount of items: 2
Items: 
Size: 517182 Color: 5
Size: 482716 Color: 17

Bin 2952: 103 of cap free
Amount of items: 2
Items: 
Size: 598850 Color: 10
Size: 401048 Color: 12

Bin 2953: 103 of cap free
Amount of items: 2
Items: 
Size: 634601 Color: 19
Size: 365297 Color: 10

Bin 2954: 103 of cap free
Amount of items: 2
Items: 
Size: 641436 Color: 18
Size: 358462 Color: 7

Bin 2955: 103 of cap free
Amount of items: 2
Items: 
Size: 663576 Color: 15
Size: 336322 Color: 17

Bin 2956: 104 of cap free
Amount of items: 2
Items: 
Size: 705081 Color: 13
Size: 294816 Color: 12

Bin 2957: 104 of cap free
Amount of items: 2
Items: 
Size: 537814 Color: 2
Size: 462083 Color: 11

Bin 2958: 104 of cap free
Amount of items: 2
Items: 
Size: 547396 Color: 19
Size: 452501 Color: 1

Bin 2959: 104 of cap free
Amount of items: 2
Items: 
Size: 584267 Color: 5
Size: 415630 Color: 17

Bin 2960: 104 of cap free
Amount of items: 3
Items: 
Size: 608715 Color: 9
Size: 195845 Color: 3
Size: 195337 Color: 10

Bin 2961: 104 of cap free
Amount of items: 2
Items: 
Size: 690405 Color: 10
Size: 309492 Color: 2

Bin 2962: 105 of cap free
Amount of items: 2
Items: 
Size: 635114 Color: 3
Size: 364782 Color: 5

Bin 2963: 105 of cap free
Amount of items: 2
Items: 
Size: 788446 Color: 10
Size: 211450 Color: 9

Bin 2964: 105 of cap free
Amount of items: 2
Items: 
Size: 696214 Color: 17
Size: 303682 Color: 11

Bin 2965: 105 of cap free
Amount of items: 2
Items: 
Size: 799438 Color: 15
Size: 200458 Color: 12

Bin 2966: 105 of cap free
Amount of items: 2
Items: 
Size: 528273 Color: 10
Size: 471623 Color: 13

Bin 2967: 105 of cap free
Amount of items: 2
Items: 
Size: 523406 Color: 14
Size: 476490 Color: 17

Bin 2968: 105 of cap free
Amount of items: 2
Items: 
Size: 537006 Color: 8
Size: 462890 Color: 12

Bin 2969: 105 of cap free
Amount of items: 2
Items: 
Size: 591429 Color: 17
Size: 408467 Color: 1

Bin 2970: 105 of cap free
Amount of items: 2
Items: 
Size: 631208 Color: 12
Size: 368688 Color: 3

Bin 2971: 105 of cap free
Amount of items: 2
Items: 
Size: 677751 Color: 12
Size: 322145 Color: 18

Bin 2972: 105 of cap free
Amount of items: 2
Items: 
Size: 713639 Color: 18
Size: 286257 Color: 2

Bin 2973: 105 of cap free
Amount of items: 2
Items: 
Size: 722944 Color: 12
Size: 276952 Color: 2

Bin 2974: 106 of cap free
Amount of items: 2
Items: 
Size: 582419 Color: 1
Size: 417476 Color: 11

Bin 2975: 106 of cap free
Amount of items: 2
Items: 
Size: 696758 Color: 14
Size: 303137 Color: 17

Bin 2976: 106 of cap free
Amount of items: 2
Items: 
Size: 520550 Color: 8
Size: 479345 Color: 1

Bin 2977: 106 of cap free
Amount of items: 2
Items: 
Size: 526867 Color: 7
Size: 473028 Color: 13

Bin 2978: 106 of cap free
Amount of items: 2
Items: 
Size: 544630 Color: 12
Size: 455265 Color: 10

Bin 2979: 106 of cap free
Amount of items: 2
Items: 
Size: 585692 Color: 17
Size: 414203 Color: 12

Bin 2980: 106 of cap free
Amount of items: 2
Items: 
Size: 600465 Color: 12
Size: 399430 Color: 6

Bin 2981: 106 of cap free
Amount of items: 2
Items: 
Size: 685141 Color: 15
Size: 314754 Color: 14

Bin 2982: 106 of cap free
Amount of items: 2
Items: 
Size: 688174 Color: 5
Size: 311721 Color: 15

Bin 2983: 106 of cap free
Amount of items: 2
Items: 
Size: 766774 Color: 11
Size: 233121 Color: 19

Bin 2984: 107 of cap free
Amount of items: 2
Items: 
Size: 629128 Color: 9
Size: 370766 Color: 1

Bin 2985: 107 of cap free
Amount of items: 2
Items: 
Size: 604764 Color: 19
Size: 395130 Color: 4

Bin 2986: 107 of cap free
Amount of items: 2
Items: 
Size: 572978 Color: 2
Size: 426916 Color: 17

Bin 2987: 107 of cap free
Amount of items: 2
Items: 
Size: 692376 Color: 13
Size: 307518 Color: 17

Bin 2988: 107 of cap free
Amount of items: 2
Items: 
Size: 735300 Color: 12
Size: 264594 Color: 8

Bin 2989: 107 of cap free
Amount of items: 2
Items: 
Size: 777337 Color: 4
Size: 222557 Color: 12

Bin 2990: 108 of cap free
Amount of items: 2
Items: 
Size: 799231 Color: 11
Size: 200662 Color: 19

Bin 2991: 108 of cap free
Amount of items: 2
Items: 
Size: 747643 Color: 11
Size: 252250 Color: 17

Bin 2992: 108 of cap free
Amount of items: 2
Items: 
Size: 712758 Color: 17
Size: 287135 Color: 3

Bin 2993: 108 of cap free
Amount of items: 2
Items: 
Size: 636639 Color: 2
Size: 363254 Color: 14

Bin 2994: 108 of cap free
Amount of items: 2
Items: 
Size: 734440 Color: 0
Size: 265453 Color: 7

Bin 2995: 108 of cap free
Amount of items: 2
Items: 
Size: 592720 Color: 18
Size: 407173 Color: 7

Bin 2996: 108 of cap free
Amount of items: 2
Items: 
Size: 646822 Color: 17
Size: 353071 Color: 7

Bin 2997: 108 of cap free
Amount of items: 2
Items: 
Size: 685587 Color: 2
Size: 314306 Color: 7

Bin 2998: 108 of cap free
Amount of items: 2
Items: 
Size: 795929 Color: 18
Size: 203964 Color: 10

Bin 2999: 109 of cap free
Amount of items: 2
Items: 
Size: 715586 Color: 9
Size: 284306 Color: 1

Bin 3000: 109 of cap free
Amount of items: 2
Items: 
Size: 599389 Color: 2
Size: 400503 Color: 15

Bin 3001: 109 of cap free
Amount of items: 2
Items: 
Size: 768700 Color: 17
Size: 231192 Color: 13

Bin 3002: 109 of cap free
Amount of items: 2
Items: 
Size: 568223 Color: 16
Size: 431669 Color: 14

Bin 3003: 109 of cap free
Amount of items: 2
Items: 
Size: 592935 Color: 7
Size: 406957 Color: 10

Bin 3004: 110 of cap free
Amount of items: 2
Items: 
Size: 686390 Color: 4
Size: 313501 Color: 16

Bin 3005: 110 of cap free
Amount of items: 2
Items: 
Size: 657018 Color: 7
Size: 342873 Color: 11

Bin 3006: 110 of cap free
Amount of items: 2
Items: 
Size: 553336 Color: 10
Size: 446555 Color: 9

Bin 3007: 110 of cap free
Amount of items: 2
Items: 
Size: 555991 Color: 0
Size: 443900 Color: 14

Bin 3008: 110 of cap free
Amount of items: 2
Items: 
Size: 735294 Color: 7
Size: 264597 Color: 12

Bin 3009: 111 of cap free
Amount of items: 2
Items: 
Size: 599029 Color: 3
Size: 400861 Color: 10

Bin 3010: 111 of cap free
Amount of items: 2
Items: 
Size: 679274 Color: 2
Size: 320616 Color: 16

Bin 3011: 111 of cap free
Amount of items: 2
Items: 
Size: 655053 Color: 2
Size: 344837 Color: 4

Bin 3012: 112 of cap free
Amount of items: 2
Items: 
Size: 630779 Color: 10
Size: 369110 Color: 1

Bin 3013: 112 of cap free
Amount of items: 2
Items: 
Size: 613681 Color: 17
Size: 386208 Color: 16

Bin 3014: 112 of cap free
Amount of items: 2
Items: 
Size: 693584 Color: 0
Size: 306305 Color: 8

Bin 3015: 112 of cap free
Amount of items: 2
Items: 
Size: 536039 Color: 10
Size: 463850 Color: 6

Bin 3016: 112 of cap free
Amount of items: 2
Items: 
Size: 506559 Color: 13
Size: 493330 Color: 14

Bin 3017: 112 of cap free
Amount of items: 2
Items: 
Size: 509029 Color: 13
Size: 490860 Color: 14

Bin 3018: 112 of cap free
Amount of items: 2
Items: 
Size: 670701 Color: 16
Size: 329188 Color: 12

Bin 3019: 112 of cap free
Amount of items: 2
Items: 
Size: 767599 Color: 0
Size: 232290 Color: 13

Bin 3020: 113 of cap free
Amount of items: 2
Items: 
Size: 519620 Color: 14
Size: 480268 Color: 19

Bin 3021: 113 of cap free
Amount of items: 2
Items: 
Size: 537517 Color: 18
Size: 462371 Color: 6

Bin 3022: 113 of cap free
Amount of items: 2
Items: 
Size: 706834 Color: 13
Size: 293054 Color: 11

Bin 3023: 114 of cap free
Amount of items: 2
Items: 
Size: 761103 Color: 9
Size: 238784 Color: 3

Bin 3024: 114 of cap free
Amount of items: 2
Items: 
Size: 608486 Color: 7
Size: 391401 Color: 8

Bin 3025: 114 of cap free
Amount of items: 2
Items: 
Size: 791057 Color: 17
Size: 208830 Color: 2

Bin 3026: 114 of cap free
Amount of items: 3
Items: 
Size: 623557 Color: 0
Size: 188546 Color: 1
Size: 187784 Color: 17

Bin 3027: 114 of cap free
Amount of items: 2
Items: 
Size: 555840 Color: 11
Size: 444047 Color: 6

Bin 3028: 114 of cap free
Amount of items: 2
Items: 
Size: 564554 Color: 14
Size: 435333 Color: 16

Bin 3029: 115 of cap free
Amount of items: 2
Items: 
Size: 573821 Color: 18
Size: 426065 Color: 12

Bin 3030: 115 of cap free
Amount of items: 2
Items: 
Size: 539198 Color: 17
Size: 460688 Color: 4

Bin 3031: 115 of cap free
Amount of items: 2
Items: 
Size: 554550 Color: 17
Size: 445336 Color: 3

Bin 3032: 115 of cap free
Amount of items: 2
Items: 
Size: 590225 Color: 0
Size: 409661 Color: 7

Bin 3033: 115 of cap free
Amount of items: 2
Items: 
Size: 681548 Color: 5
Size: 318338 Color: 6

Bin 3034: 116 of cap free
Amount of items: 2
Items: 
Size: 711130 Color: 7
Size: 288755 Color: 2

Bin 3035: 116 of cap free
Amount of items: 2
Items: 
Size: 727437 Color: 0
Size: 272448 Color: 5

Bin 3036: 116 of cap free
Amount of items: 2
Items: 
Size: 505948 Color: 2
Size: 493937 Color: 14

Bin 3037: 116 of cap free
Amount of items: 2
Items: 
Size: 543678 Color: 8
Size: 456207 Color: 14

Bin 3038: 116 of cap free
Amount of items: 2
Items: 
Size: 602091 Color: 15
Size: 397794 Color: 14

Bin 3039: 116 of cap free
Amount of items: 2
Items: 
Size: 625592 Color: 1
Size: 374293 Color: 8

Bin 3040: 116 of cap free
Amount of items: 2
Items: 
Size: 629580 Color: 16
Size: 370305 Color: 19

Bin 3041: 117 of cap free
Amount of items: 2
Items: 
Size: 676653 Color: 12
Size: 323231 Color: 3

Bin 3042: 117 of cap free
Amount of items: 2
Items: 
Size: 665883 Color: 2
Size: 334001 Color: 0

Bin 3043: 117 of cap free
Amount of items: 2
Items: 
Size: 787784 Color: 5
Size: 212100 Color: 2

Bin 3044: 117 of cap free
Amount of items: 2
Items: 
Size: 534334 Color: 8
Size: 465550 Color: 2

Bin 3045: 117 of cap free
Amount of items: 2
Items: 
Size: 678211 Color: 4
Size: 321673 Color: 6

Bin 3046: 117 of cap free
Amount of items: 2
Items: 
Size: 757202 Color: 13
Size: 242682 Color: 4

Bin 3047: 118 of cap free
Amount of items: 2
Items: 
Size: 657772 Color: 13
Size: 342111 Color: 17

Bin 3048: 118 of cap free
Amount of items: 3
Items: 
Size: 492468 Color: 14
Size: 374828 Color: 18
Size: 132587 Color: 2

Bin 3049: 118 of cap free
Amount of items: 2
Items: 
Size: 529518 Color: 19
Size: 470365 Color: 15

Bin 3050: 118 of cap free
Amount of items: 2
Items: 
Size: 684953 Color: 1
Size: 314930 Color: 8

Bin 3051: 118 of cap free
Amount of items: 2
Items: 
Size: 719852 Color: 17
Size: 280031 Color: 18

Bin 3052: 118 of cap free
Amount of items: 2
Items: 
Size: 733491 Color: 5
Size: 266392 Color: 2

Bin 3053: 119 of cap free
Amount of items: 2
Items: 
Size: 738201 Color: 16
Size: 261681 Color: 12

Bin 3054: 119 of cap free
Amount of items: 2
Items: 
Size: 500444 Color: 3
Size: 499438 Color: 18

Bin 3055: 119 of cap free
Amount of items: 2
Items: 
Size: 571610 Color: 11
Size: 428272 Color: 13

Bin 3056: 119 of cap free
Amount of items: 2
Items: 
Size: 574848 Color: 12
Size: 425034 Color: 7

Bin 3057: 119 of cap free
Amount of items: 2
Items: 
Size: 608222 Color: 18
Size: 391660 Color: 13

Bin 3058: 119 of cap free
Amount of items: 2
Items: 
Size: 633421 Color: 19
Size: 366461 Color: 11

Bin 3059: 119 of cap free
Amount of items: 2
Items: 
Size: 704048 Color: 10
Size: 295834 Color: 13

Bin 3060: 120 of cap free
Amount of items: 2
Items: 
Size: 639100 Color: 4
Size: 360781 Color: 7

Bin 3061: 120 of cap free
Amount of items: 2
Items: 
Size: 780288 Color: 6
Size: 219593 Color: 19

Bin 3062: 120 of cap free
Amount of items: 2
Items: 
Size: 769366 Color: 15
Size: 230515 Color: 16

Bin 3063: 120 of cap free
Amount of items: 2
Items: 
Size: 558010 Color: 14
Size: 441871 Color: 16

Bin 3064: 120 of cap free
Amount of items: 2
Items: 
Size: 777028 Color: 10
Size: 222853 Color: 11

Bin 3065: 120 of cap free
Amount of items: 2
Items: 
Size: 534797 Color: 5
Size: 465084 Color: 19

Bin 3066: 120 of cap free
Amount of items: 2
Items: 
Size: 552258 Color: 8
Size: 447623 Color: 1

Bin 3067: 120 of cap free
Amount of items: 2
Items: 
Size: 767593 Color: 2
Size: 232288 Color: 15

Bin 3068: 121 of cap free
Amount of items: 3
Items: 
Size: 635674 Color: 3
Size: 183165 Color: 9
Size: 181041 Color: 18

Bin 3069: 121 of cap free
Amount of items: 2
Items: 
Size: 503126 Color: 16
Size: 496754 Color: 1

Bin 3070: 121 of cap free
Amount of items: 2
Items: 
Size: 669054 Color: 0
Size: 330826 Color: 12

Bin 3071: 121 of cap free
Amount of items: 2
Items: 
Size: 603438 Color: 13
Size: 396442 Color: 6

Bin 3072: 121 of cap free
Amount of items: 2
Items: 
Size: 688028 Color: 13
Size: 311852 Color: 6

Bin 3073: 122 of cap free
Amount of items: 2
Items: 
Size: 575299 Color: 12
Size: 424580 Color: 2

Bin 3074: 122 of cap free
Amount of items: 2
Items: 
Size: 786505 Color: 8
Size: 213374 Color: 1

Bin 3075: 122 of cap free
Amount of items: 2
Items: 
Size: 789303 Color: 5
Size: 210576 Color: 18

Bin 3076: 122 of cap free
Amount of items: 2
Items: 
Size: 726940 Color: 15
Size: 272939 Color: 17

Bin 3077: 123 of cap free
Amount of items: 2
Items: 
Size: 711117 Color: 16
Size: 288761 Color: 4

Bin 3078: 123 of cap free
Amount of items: 2
Items: 
Size: 783011 Color: 11
Size: 216867 Color: 0

Bin 3079: 123 of cap free
Amount of items: 2
Items: 
Size: 538535 Color: 1
Size: 461343 Color: 3

Bin 3080: 123 of cap free
Amount of items: 2
Items: 
Size: 664082 Color: 16
Size: 335796 Color: 19

Bin 3081: 123 of cap free
Amount of items: 2
Items: 
Size: 518362 Color: 9
Size: 481516 Color: 15

Bin 3082: 123 of cap free
Amount of items: 2
Items: 
Size: 599375 Color: 4
Size: 400503 Color: 6

Bin 3083: 123 of cap free
Amount of items: 2
Items: 
Size: 683485 Color: 17
Size: 316393 Color: 15

Bin 3084: 124 of cap free
Amount of items: 3
Items: 
Size: 754389 Color: 14
Size: 124535 Color: 7
Size: 120953 Color: 4

Bin 3085: 124 of cap free
Amount of items: 2
Items: 
Size: 577881 Color: 2
Size: 421996 Color: 6

Bin 3086: 124 of cap free
Amount of items: 2
Items: 
Size: 618106 Color: 4
Size: 381771 Color: 18

Bin 3087: 125 of cap free
Amount of items: 2
Items: 
Size: 692981 Color: 14
Size: 306895 Color: 9

Bin 3088: 125 of cap free
Amount of items: 2
Items: 
Size: 725517 Color: 5
Size: 274359 Color: 11

Bin 3089: 125 of cap free
Amount of items: 2
Items: 
Size: 523402 Color: 5
Size: 476474 Color: 4

Bin 3090: 125 of cap free
Amount of items: 2
Items: 
Size: 572782 Color: 16
Size: 427094 Color: 6

Bin 3091: 125 of cap free
Amount of items: 2
Items: 
Size: 727289 Color: 16
Size: 272587 Color: 8

Bin 3092: 125 of cap free
Amount of items: 2
Items: 
Size: 763984 Color: 19
Size: 235892 Color: 7

Bin 3093: 126 of cap free
Amount of items: 3
Items: 
Size: 787359 Color: 3
Size: 108595 Color: 3
Size: 103921 Color: 5

Bin 3094: 126 of cap free
Amount of items: 2
Items: 
Size: 796232 Color: 9
Size: 203643 Color: 7

Bin 3095: 126 of cap free
Amount of items: 2
Items: 
Size: 500440 Color: 7
Size: 499435 Color: 17

Bin 3096: 126 of cap free
Amount of items: 2
Items: 
Size: 769812 Color: 2
Size: 230063 Color: 1

Bin 3097: 127 of cap free
Amount of items: 2
Items: 
Size: 725285 Color: 17
Size: 274589 Color: 14

Bin 3098: 127 of cap free
Amount of items: 2
Items: 
Size: 714107 Color: 9
Size: 285767 Color: 1

Bin 3099: 127 of cap free
Amount of items: 2
Items: 
Size: 531229 Color: 16
Size: 468645 Color: 10

Bin 3100: 127 of cap free
Amount of items: 2
Items: 
Size: 519217 Color: 17
Size: 480657 Color: 11

Bin 3101: 127 of cap free
Amount of items: 2
Items: 
Size: 547377 Color: 4
Size: 452497 Color: 13

Bin 3102: 127 of cap free
Amount of items: 2
Items: 
Size: 700514 Color: 4
Size: 299360 Color: 17

Bin 3103: 128 of cap free
Amount of items: 2
Items: 
Size: 660517 Color: 4
Size: 339356 Color: 18

Bin 3104: 128 of cap free
Amount of items: 2
Items: 
Size: 717551 Color: 5
Size: 282322 Color: 0

Bin 3105: 128 of cap free
Amount of items: 2
Items: 
Size: 796235 Color: 17
Size: 203638 Color: 4

Bin 3106: 128 of cap free
Amount of items: 2
Items: 
Size: 713212 Color: 10
Size: 286661 Color: 3

Bin 3107: 128 of cap free
Amount of items: 2
Items: 
Size: 742332 Color: 2
Size: 257541 Color: 12

Bin 3108: 128 of cap free
Amount of items: 2
Items: 
Size: 777019 Color: 9
Size: 222854 Color: 14

Bin 3109: 128 of cap free
Amount of items: 2
Items: 
Size: 794765 Color: 2
Size: 205108 Color: 0

Bin 3110: 129 of cap free
Amount of items: 2
Items: 
Size: 756169 Color: 7
Size: 243703 Color: 5

Bin 3111: 129 of cap free
Amount of items: 2
Items: 
Size: 527800 Color: 3
Size: 472072 Color: 11

Bin 3112: 129 of cap free
Amount of items: 2
Items: 
Size: 556314 Color: 14
Size: 443558 Color: 8

Bin 3113: 129 of cap free
Amount of items: 2
Items: 
Size: 593537 Color: 11
Size: 406335 Color: 15

Bin 3114: 130 of cap free
Amount of items: 2
Items: 
Size: 660376 Color: 2
Size: 339495 Color: 8

Bin 3115: 130 of cap free
Amount of items: 2
Items: 
Size: 743219 Color: 13
Size: 256652 Color: 10

Bin 3116: 130 of cap free
Amount of items: 2
Items: 
Size: 704399 Color: 8
Size: 295472 Color: 19

Bin 3117: 130 of cap free
Amount of items: 2
Items: 
Size: 699578 Color: 17
Size: 300293 Color: 0

Bin 3118: 130 of cap free
Amount of items: 2
Items: 
Size: 749255 Color: 3
Size: 250616 Color: 16

Bin 3119: 130 of cap free
Amount of items: 2
Items: 
Size: 552564 Color: 6
Size: 447307 Color: 19

Bin 3120: 130 of cap free
Amount of items: 2
Items: 
Size: 560252 Color: 0
Size: 439619 Color: 18

Bin 3121: 130 of cap free
Amount of items: 2
Items: 
Size: 672254 Color: 16
Size: 327617 Color: 3

Bin 3122: 130 of cap free
Amount of items: 2
Items: 
Size: 719520 Color: 2
Size: 280351 Color: 9

Bin 3123: 130 of cap free
Amount of items: 2
Items: 
Size: 734659 Color: 4
Size: 265212 Color: 15

Bin 3124: 131 of cap free
Amount of items: 2
Items: 
Size: 624106 Color: 8
Size: 375764 Color: 3

Bin 3125: 131 of cap free
Amount of items: 3
Items: 
Size: 715105 Color: 13
Size: 142647 Color: 9
Size: 142118 Color: 3

Bin 3126: 131 of cap free
Amount of items: 2
Items: 
Size: 581937 Color: 12
Size: 417933 Color: 8

Bin 3127: 131 of cap free
Amount of items: 2
Items: 
Size: 699809 Color: 2
Size: 300061 Color: 19

Bin 3128: 131 of cap free
Amount of items: 2
Items: 
Size: 714774 Color: 0
Size: 285096 Color: 10

Bin 3129: 131 of cap free
Amount of items: 2
Items: 
Size: 764917 Color: 0
Size: 234953 Color: 13

Bin 3130: 132 of cap free
Amount of items: 2
Items: 
Size: 629992 Color: 4
Size: 369877 Color: 10

Bin 3131: 132 of cap free
Amount of items: 2
Items: 
Size: 668776 Color: 17
Size: 331093 Color: 7

Bin 3132: 132 of cap free
Amount of items: 2
Items: 
Size: 686566 Color: 6
Size: 313303 Color: 5

Bin 3133: 132 of cap free
Amount of items: 2
Items: 
Size: 761892 Color: 6
Size: 237977 Color: 3

Bin 3134: 132 of cap free
Amount of items: 2
Items: 
Size: 528272 Color: 15
Size: 471597 Color: 0

Bin 3135: 132 of cap free
Amount of items: 2
Items: 
Size: 533511 Color: 1
Size: 466358 Color: 9

Bin 3136: 132 of cap free
Amount of items: 2
Items: 
Size: 638561 Color: 2
Size: 361308 Color: 6

Bin 3137: 132 of cap free
Amount of items: 2
Items: 
Size: 662708 Color: 12
Size: 337161 Color: 15

Bin 3138: 133 of cap free
Amount of items: 2
Items: 
Size: 687630 Color: 3
Size: 312238 Color: 5

Bin 3139: 133 of cap free
Amount of items: 2
Items: 
Size: 522305 Color: 15
Size: 477563 Color: 3

Bin 3140: 133 of cap free
Amount of items: 2
Items: 
Size: 501296 Color: 15
Size: 498572 Color: 6

Bin 3141: 133 of cap free
Amount of items: 2
Items: 
Size: 528024 Color: 14
Size: 471844 Color: 6

Bin 3142: 133 of cap free
Amount of items: 2
Items: 
Size: 550832 Color: 9
Size: 449036 Color: 19

Bin 3143: 133 of cap free
Amount of items: 2
Items: 
Size: 578450 Color: 14
Size: 421418 Color: 4

Bin 3144: 133 of cap free
Amount of items: 2
Items: 
Size: 614385 Color: 18
Size: 385483 Color: 1

Bin 3145: 133 of cap free
Amount of items: 2
Items: 
Size: 672231 Color: 8
Size: 327637 Color: 18

Bin 3146: 133 of cap free
Amount of items: 2
Items: 
Size: 685423 Color: 13
Size: 314445 Color: 2

Bin 3147: 133 of cap free
Amount of items: 2
Items: 
Size: 743998 Color: 9
Size: 255870 Color: 17

Bin 3148: 134 of cap free
Amount of items: 2
Items: 
Size: 708145 Color: 11
Size: 291722 Color: 7

Bin 3149: 134 of cap free
Amount of items: 2
Items: 
Size: 700356 Color: 14
Size: 299511 Color: 17

Bin 3150: 134 of cap free
Amount of items: 2
Items: 
Size: 507874 Color: 17
Size: 491993 Color: 5

Bin 3151: 134 of cap free
Amount of items: 2
Items: 
Size: 529079 Color: 1
Size: 470788 Color: 16

Bin 3152: 134 of cap free
Amount of items: 2
Items: 
Size: 757365 Color: 13
Size: 242502 Color: 18

Bin 3153: 134 of cap free
Amount of items: 2
Items: 
Size: 664920 Color: 8
Size: 334947 Color: 5

Bin 3154: 135 of cap free
Amount of items: 2
Items: 
Size: 629430 Color: 8
Size: 370436 Color: 5

Bin 3155: 135 of cap free
Amount of items: 2
Items: 
Size: 759731 Color: 0
Size: 240135 Color: 3

Bin 3156: 135 of cap free
Amount of items: 2
Items: 
Size: 562287 Color: 15
Size: 437579 Color: 1

Bin 3157: 135 of cap free
Amount of items: 2
Items: 
Size: 509244 Color: 10
Size: 490622 Color: 17

Bin 3158: 135 of cap free
Amount of items: 2
Items: 
Size: 532749 Color: 8
Size: 467117 Color: 5

Bin 3159: 135 of cap free
Amount of items: 2
Items: 
Size: 717844 Color: 15
Size: 282022 Color: 6

Bin 3160: 136 of cap free
Amount of items: 2
Items: 
Size: 618709 Color: 7
Size: 381156 Color: 3

Bin 3161: 136 of cap free
Amount of items: 2
Items: 
Size: 580808 Color: 14
Size: 419057 Color: 5

Bin 3162: 136 of cap free
Amount of items: 2
Items: 
Size: 514292 Color: 1
Size: 485573 Color: 9

Bin 3163: 136 of cap free
Amount of items: 2
Items: 
Size: 523624 Color: 4
Size: 476241 Color: 15

Bin 3164: 136 of cap free
Amount of items: 2
Items: 
Size: 608635 Color: 5
Size: 391230 Color: 12

Bin 3165: 136 of cap free
Amount of items: 2
Items: 
Size: 671535 Color: 3
Size: 328330 Color: 13

Bin 3166: 137 of cap free
Amount of items: 2
Items: 
Size: 706830 Color: 19
Size: 293034 Color: 1

Bin 3167: 137 of cap free
Amount of items: 2
Items: 
Size: 622195 Color: 3
Size: 377669 Color: 16

Bin 3168: 137 of cap free
Amount of items: 2
Items: 
Size: 720863 Color: 10
Size: 279001 Color: 8

Bin 3169: 138 of cap free
Amount of items: 2
Items: 
Size: 758759 Color: 5
Size: 241104 Color: 11

Bin 3170: 138 of cap free
Amount of items: 2
Items: 
Size: 679489 Color: 8
Size: 320374 Color: 9

Bin 3171: 138 of cap free
Amount of items: 2
Items: 
Size: 586719 Color: 11
Size: 413144 Color: 12

Bin 3172: 138 of cap free
Amount of items: 2
Items: 
Size: 604548 Color: 15
Size: 395315 Color: 3

Bin 3173: 139 of cap free
Amount of items: 2
Items: 
Size: 588608 Color: 3
Size: 411254 Color: 12

Bin 3174: 139 of cap free
Amount of items: 2
Items: 
Size: 731724 Color: 13
Size: 268138 Color: 18

Bin 3175: 139 of cap free
Amount of items: 2
Items: 
Size: 755740 Color: 7
Size: 244122 Color: 18

Bin 3176: 139 of cap free
Amount of items: 2
Items: 
Size: 727134 Color: 17
Size: 272728 Color: 2

Bin 3177: 139 of cap free
Amount of items: 2
Items: 
Size: 611093 Color: 11
Size: 388769 Color: 9

Bin 3178: 139 of cap free
Amount of items: 2
Items: 
Size: 549522 Color: 14
Size: 450340 Color: 15

Bin 3179: 139 of cap free
Amount of items: 2
Items: 
Size: 606173 Color: 11
Size: 393689 Color: 13

Bin 3180: 140 of cap free
Amount of items: 2
Items: 
Size: 680962 Color: 19
Size: 318899 Color: 14

Bin 3181: 140 of cap free
Amount of items: 2
Items: 
Size: 506124 Color: 19
Size: 493737 Color: 5

Bin 3182: 140 of cap free
Amount of items: 2
Items: 
Size: 552559 Color: 7
Size: 447302 Color: 12

Bin 3183: 140 of cap free
Amount of items: 2
Items: 
Size: 599185 Color: 7
Size: 400676 Color: 5

Bin 3184: 140 of cap free
Amount of items: 2
Items: 
Size: 625667 Color: 10
Size: 374194 Color: 13

Bin 3185: 141 of cap free
Amount of items: 3
Items: 
Size: 787113 Color: 5
Size: 108628 Color: 16
Size: 104119 Color: 0

Bin 3186: 141 of cap free
Amount of items: 2
Items: 
Size: 531557 Color: 8
Size: 468303 Color: 9

Bin 3187: 141 of cap free
Amount of items: 2
Items: 
Size: 713179 Color: 11
Size: 286681 Color: 10

Bin 3188: 141 of cap free
Amount of items: 2
Items: 
Size: 719030 Color: 7
Size: 280830 Color: 16

Bin 3189: 141 of cap free
Amount of items: 2
Items: 
Size: 617368 Color: 2
Size: 382492 Color: 14

Bin 3190: 142 of cap free
Amount of items: 3
Items: 
Size: 371740 Color: 1
Size: 343084 Color: 8
Size: 285035 Color: 11

Bin 3191: 142 of cap free
Amount of items: 2
Items: 
Size: 792804 Color: 5
Size: 207055 Color: 7

Bin 3192: 142 of cap free
Amount of items: 2
Items: 
Size: 619661 Color: 12
Size: 380198 Color: 10

Bin 3193: 142 of cap free
Amount of items: 2
Items: 
Size: 739039 Color: 13
Size: 260820 Color: 0

Bin 3194: 142 of cap free
Amount of items: 2
Items: 
Size: 732677 Color: 8
Size: 267182 Color: 1

Bin 3195: 142 of cap free
Amount of items: 2
Items: 
Size: 522465 Color: 7
Size: 477394 Color: 5

Bin 3196: 142 of cap free
Amount of items: 2
Items: 
Size: 539180 Color: 3
Size: 460679 Color: 15

Bin 3197: 142 of cap free
Amount of items: 2
Items: 
Size: 619106 Color: 18
Size: 380753 Color: 19

Bin 3198: 142 of cap free
Amount of items: 2
Items: 
Size: 628339 Color: 3
Size: 371520 Color: 13

Bin 3199: 142 of cap free
Amount of items: 2
Items: 
Size: 773409 Color: 5
Size: 226450 Color: 10

Bin 3200: 143 of cap free
Amount of items: 2
Items: 
Size: 595604 Color: 4
Size: 404254 Color: 19

Bin 3201: 143 of cap free
Amount of items: 2
Items: 
Size: 763325 Color: 17
Size: 236533 Color: 11

Bin 3202: 143 of cap free
Amount of items: 2
Items: 
Size: 685882 Color: 17
Size: 313976 Color: 4

Bin 3203: 143 of cap free
Amount of items: 2
Items: 
Size: 605341 Color: 6
Size: 394517 Color: 7

Bin 3204: 143 of cap free
Amount of items: 2
Items: 
Size: 641892 Color: 17
Size: 357966 Color: 12

Bin 3205: 143 of cap free
Amount of items: 2
Items: 
Size: 647090 Color: 11
Size: 352768 Color: 14

Bin 3206: 144 of cap free
Amount of items: 3
Items: 
Size: 355746 Color: 13
Size: 349965 Color: 5
Size: 294146 Color: 9

Bin 3207: 144 of cap free
Amount of items: 2
Items: 
Size: 527454 Color: 7
Size: 472403 Color: 3

Bin 3208: 144 of cap free
Amount of items: 2
Items: 
Size: 641891 Color: 2
Size: 357966 Color: 15

Bin 3209: 144 of cap free
Amount of items: 2
Items: 
Size: 681226 Color: 3
Size: 318631 Color: 7

Bin 3210: 145 of cap free
Amount of items: 2
Items: 
Size: 783163 Color: 3
Size: 216693 Color: 1

Bin 3211: 145 of cap free
Amount of items: 2
Items: 
Size: 741956 Color: 2
Size: 257900 Color: 1

Bin 3212: 145 of cap free
Amount of items: 3
Items: 
Size: 640982 Color: 13
Size: 179918 Color: 15
Size: 178956 Color: 12

Bin 3213: 145 of cap free
Amount of items: 2
Items: 
Size: 524358 Color: 16
Size: 475498 Color: 5

Bin 3214: 145 of cap free
Amount of items: 2
Items: 
Size: 631697 Color: 4
Size: 368159 Color: 11

Bin 3215: 145 of cap free
Amount of items: 2
Items: 
Size: 786189 Color: 14
Size: 213667 Color: 15

Bin 3216: 146 of cap free
Amount of items: 2
Items: 
Size: 779789 Color: 17
Size: 220066 Color: 19

Bin 3217: 146 of cap free
Amount of items: 2
Items: 
Size: 525658 Color: 1
Size: 474197 Color: 18

Bin 3218: 146 of cap free
Amount of items: 2
Items: 
Size: 574658 Color: 0
Size: 425197 Color: 1

Bin 3219: 146 of cap free
Amount of items: 2
Items: 
Size: 585522 Color: 6
Size: 414333 Color: 3

Bin 3220: 146 of cap free
Amount of items: 2
Items: 
Size: 609504 Color: 13
Size: 390351 Color: 2

Bin 3221: 146 of cap free
Amount of items: 2
Items: 
Size: 619488 Color: 6
Size: 380367 Color: 0

Bin 3222: 146 of cap free
Amount of items: 2
Items: 
Size: 632642 Color: 5
Size: 367213 Color: 13

Bin 3223: 146 of cap free
Amount of items: 2
Items: 
Size: 730953 Color: 7
Size: 268902 Color: 5

Bin 3224: 147 of cap free
Amount of items: 2
Items: 
Size: 655793 Color: 8
Size: 344061 Color: 13

Bin 3225: 147 of cap free
Amount of items: 2
Items: 
Size: 798897 Color: 5
Size: 200957 Color: 7

Bin 3226: 147 of cap free
Amount of items: 2
Items: 
Size: 666253 Color: 15
Size: 333601 Color: 13

Bin 3227: 147 of cap free
Amount of items: 2
Items: 
Size: 675805 Color: 15
Size: 324049 Color: 4

Bin 3228: 147 of cap free
Amount of items: 2
Items: 
Size: 530121 Color: 8
Size: 469733 Color: 1

Bin 3229: 147 of cap free
Amount of items: 2
Items: 
Size: 541039 Color: 3
Size: 458815 Color: 8

Bin 3230: 147 of cap free
Amount of items: 2
Items: 
Size: 790455 Color: 12
Size: 209399 Color: 1

Bin 3231: 148 of cap free
Amount of items: 2
Items: 
Size: 698226 Color: 8
Size: 301627 Color: 17

Bin 3232: 148 of cap free
Amount of items: 3
Items: 
Size: 662082 Color: 7
Size: 184539 Color: 3
Size: 153232 Color: 9

Bin 3233: 148 of cap free
Amount of items: 2
Items: 
Size: 707948 Color: 9
Size: 291905 Color: 11

Bin 3234: 148 of cap free
Amount of items: 2
Items: 
Size: 504467 Color: 4
Size: 495386 Color: 17

Bin 3235: 148 of cap free
Amount of items: 2
Items: 
Size: 514239 Color: 7
Size: 485614 Color: 4

Bin 3236: 148 of cap free
Amount of items: 2
Items: 
Size: 615836 Color: 14
Size: 384017 Color: 3

Bin 3237: 149 of cap free
Amount of items: 2
Items: 
Size: 775598 Color: 3
Size: 224254 Color: 1

Bin 3238: 149 of cap free
Amount of items: 2
Items: 
Size: 617622 Color: 11
Size: 382230 Color: 1

Bin 3239: 149 of cap free
Amount of items: 2
Items: 
Size: 543157 Color: 3
Size: 456695 Color: 8

Bin 3240: 149 of cap free
Amount of items: 2
Items: 
Size: 558353 Color: 6
Size: 441499 Color: 7

Bin 3241: 149 of cap free
Amount of items: 2
Items: 
Size: 564229 Color: 8
Size: 435623 Color: 13

Bin 3242: 149 of cap free
Amount of items: 2
Items: 
Size: 576941 Color: 14
Size: 422911 Color: 12

Bin 3243: 149 of cap free
Amount of items: 2
Items: 
Size: 713877 Color: 17
Size: 285975 Color: 1

Bin 3244: 150 of cap free
Amount of items: 3
Items: 
Size: 704191 Color: 3
Size: 174562 Color: 14
Size: 121098 Color: 5

Bin 3245: 150 of cap free
Amount of items: 2
Items: 
Size: 640813 Color: 13
Size: 359038 Color: 4

Bin 3246: 150 of cap free
Amount of items: 2
Items: 
Size: 671864 Color: 4
Size: 327987 Color: 11

Bin 3247: 150 of cap free
Amount of items: 2
Items: 
Size: 644958 Color: 8
Size: 354893 Color: 12

Bin 3248: 150 of cap free
Amount of items: 2
Items: 
Size: 590794 Color: 17
Size: 409057 Color: 19

Bin 3249: 150 of cap free
Amount of items: 2
Items: 
Size: 620944 Color: 4
Size: 378907 Color: 2

Bin 3250: 150 of cap free
Amount of items: 2
Items: 
Size: 698729 Color: 9
Size: 301122 Color: 10

Bin 3251: 150 of cap free
Amount of items: 2
Items: 
Size: 720211 Color: 3
Size: 279640 Color: 9

Bin 3252: 151 of cap free
Amount of items: 2
Items: 
Size: 541982 Color: 4
Size: 457868 Color: 7

Bin 3253: 151 of cap free
Amount of items: 2
Items: 
Size: 659122 Color: 7
Size: 340728 Color: 18

Bin 3254: 151 of cap free
Amount of items: 2
Items: 
Size: 764140 Color: 17
Size: 235710 Color: 19

Bin 3255: 151 of cap free
Amount of items: 2
Items: 
Size: 513941 Color: 13
Size: 485909 Color: 5

Bin 3256: 151 of cap free
Amount of items: 2
Items: 
Size: 534787 Color: 11
Size: 465063 Color: 18

Bin 3257: 151 of cap free
Amount of items: 2
Items: 
Size: 544751 Color: 9
Size: 455099 Color: 7

Bin 3258: 152 of cap free
Amount of items: 3
Items: 
Size: 646243 Color: 11
Size: 230877 Color: 3
Size: 122729 Color: 15

Bin 3259: 152 of cap free
Amount of items: 2
Items: 
Size: 629979 Color: 16
Size: 369870 Color: 10

Bin 3260: 152 of cap free
Amount of items: 2
Items: 
Size: 555148 Color: 14
Size: 444701 Color: 9

Bin 3261: 153 of cap free
Amount of items: 3
Items: 
Size: 631642 Color: 7
Size: 211091 Color: 13
Size: 157115 Color: 2

Bin 3262: 153 of cap free
Amount of items: 2
Items: 
Size: 759512 Color: 18
Size: 240336 Color: 7

Bin 3263: 153 of cap free
Amount of items: 2
Items: 
Size: 647477 Color: 3
Size: 352371 Color: 11

Bin 3264: 153 of cap free
Amount of items: 2
Items: 
Size: 536823 Color: 8
Size: 463025 Color: 7

Bin 3265: 153 of cap free
Amount of items: 2
Items: 
Size: 579497 Color: 17
Size: 420351 Color: 16

Bin 3266: 153 of cap free
Amount of items: 2
Items: 
Size: 597799 Color: 13
Size: 402049 Color: 14

Bin 3267: 153 of cap free
Amount of items: 2
Items: 
Size: 623870 Color: 0
Size: 375978 Color: 13

Bin 3268: 154 of cap free
Amount of items: 2
Items: 
Size: 639068 Color: 1
Size: 360779 Color: 4

Bin 3269: 154 of cap free
Amount of items: 2
Items: 
Size: 759145 Color: 19
Size: 240702 Color: 4

Bin 3270: 154 of cap free
Amount of items: 3
Items: 
Size: 492688 Color: 15
Size: 308674 Color: 8
Size: 198485 Color: 15

Bin 3271: 154 of cap free
Amount of items: 2
Items: 
Size: 511915 Color: 16
Size: 487932 Color: 9

Bin 3272: 154 of cap free
Amount of items: 2
Items: 
Size: 595930 Color: 18
Size: 403917 Color: 9

Bin 3273: 154 of cap free
Amount of items: 2
Items: 
Size: 612374 Color: 6
Size: 387473 Color: 11

Bin 3274: 154 of cap free
Amount of items: 2
Items: 
Size: 653452 Color: 0
Size: 346395 Color: 4

Bin 3275: 155 of cap free
Amount of items: 3
Items: 
Size: 541206 Color: 15
Size: 277580 Color: 17
Size: 181060 Color: 9

Bin 3276: 155 of cap free
Amount of items: 3
Items: 
Size: 751495 Color: 12
Size: 131003 Color: 16
Size: 117348 Color: 10

Bin 3277: 155 of cap free
Amount of items: 2
Items: 
Size: 568705 Color: 7
Size: 431141 Color: 10

Bin 3278: 155 of cap free
Amount of items: 2
Items: 
Size: 518525 Color: 14
Size: 481321 Color: 5

Bin 3279: 155 of cap free
Amount of items: 2
Items: 
Size: 790224 Color: 18
Size: 209622 Color: 5

Bin 3280: 155 of cap free
Amount of items: 2
Items: 
Size: 538982 Color: 7
Size: 460864 Color: 0

Bin 3281: 156 of cap free
Amount of items: 2
Items: 
Size: 779585 Color: 17
Size: 220260 Color: 18

Bin 3282: 156 of cap free
Amount of items: 2
Items: 
Size: 745939 Color: 18
Size: 253906 Color: 11

Bin 3283: 156 of cap free
Amount of items: 2
Items: 
Size: 667001 Color: 16
Size: 332844 Color: 2

Bin 3284: 156 of cap free
Amount of items: 2
Items: 
Size: 638135 Color: 1
Size: 361710 Color: 12

Bin 3285: 156 of cap free
Amount of items: 2
Items: 
Size: 625209 Color: 5
Size: 374636 Color: 3

Bin 3286: 156 of cap free
Amount of items: 2
Items: 
Size: 551729 Color: 0
Size: 448116 Color: 19

Bin 3287: 157 of cap free
Amount of items: 2
Items: 
Size: 583289 Color: 16
Size: 416555 Color: 14

Bin 3288: 157 of cap free
Amount of items: 2
Items: 
Size: 749745 Color: 17
Size: 250099 Color: 10

Bin 3289: 157 of cap free
Amount of items: 2
Items: 
Size: 655046 Color: 18
Size: 344798 Color: 4

Bin 3290: 158 of cap free
Amount of items: 2
Items: 
Size: 775041 Color: 6
Size: 224802 Color: 8

Bin 3291: 158 of cap free
Amount of items: 2
Items: 
Size: 770105 Color: 11
Size: 229738 Color: 17

Bin 3292: 158 of cap free
Amount of items: 2
Items: 
Size: 503104 Color: 11
Size: 496739 Color: 12

Bin 3293: 158 of cap free
Amount of items: 2
Items: 
Size: 582555 Color: 1
Size: 417288 Color: 7

Bin 3294: 159 of cap free
Amount of items: 2
Items: 
Size: 626722 Color: 5
Size: 373120 Color: 2

Bin 3295: 159 of cap free
Amount of items: 3
Items: 
Size: 633033 Color: 3
Size: 184436 Color: 17
Size: 182373 Color: 0

Bin 3296: 159 of cap free
Amount of items: 2
Items: 
Size: 710388 Color: 14
Size: 289454 Color: 9

Bin 3297: 159 of cap free
Amount of items: 2
Items: 
Size: 721453 Color: 19
Size: 278389 Color: 9

Bin 3298: 159 of cap free
Amount of items: 2
Items: 
Size: 516999 Color: 6
Size: 482843 Color: 1

Bin 3299: 159 of cap free
Amount of items: 2
Items: 
Size: 519203 Color: 10
Size: 480639 Color: 17

Bin 3300: 159 of cap free
Amount of items: 2
Items: 
Size: 544149 Color: 6
Size: 455693 Color: 15

Bin 3301: 159 of cap free
Amount of items: 2
Items: 
Size: 584541 Color: 14
Size: 415301 Color: 11

Bin 3302: 159 of cap free
Amount of items: 2
Items: 
Size: 677744 Color: 1
Size: 322098 Color: 4

Bin 3303: 160 of cap free
Amount of items: 2
Items: 
Size: 732020 Color: 10
Size: 267821 Color: 4

Bin 3304: 160 of cap free
Amount of items: 2
Items: 
Size: 781558 Color: 18
Size: 218283 Color: 17

Bin 3305: 161 of cap free
Amount of items: 2
Items: 
Size: 698511 Color: 15
Size: 301329 Color: 6

Bin 3306: 161 of cap free
Amount of items: 2
Items: 
Size: 734893 Color: 0
Size: 264947 Color: 7

Bin 3307: 162 of cap free
Amount of items: 2
Items: 
Size: 737115 Color: 3
Size: 262724 Color: 8

Bin 3308: 162 of cap free
Amount of items: 2
Items: 
Size: 608837 Color: 9
Size: 391002 Color: 0

Bin 3309: 162 of cap free
Amount of items: 2
Items: 
Size: 603986 Color: 12
Size: 395853 Color: 9

Bin 3310: 162 of cap free
Amount of items: 2
Items: 
Size: 758466 Color: 7
Size: 241373 Color: 11

Bin 3311: 163 of cap free
Amount of items: 2
Items: 
Size: 694461 Color: 10
Size: 305377 Color: 6

Bin 3312: 163 of cap free
Amount of items: 2
Items: 
Size: 794378 Color: 7
Size: 205460 Color: 13

Bin 3313: 163 of cap free
Amount of items: 2
Items: 
Size: 501559 Color: 4
Size: 498279 Color: 6

Bin 3314: 163 of cap free
Amount of items: 2
Items: 
Size: 521421 Color: 2
Size: 478417 Color: 10

Bin 3315: 163 of cap free
Amount of items: 2
Items: 
Size: 664813 Color: 16
Size: 335025 Color: 10

Bin 3316: 163 of cap free
Amount of items: 2
Items: 
Size: 711116 Color: 2
Size: 288722 Color: 9

Bin 3317: 163 of cap free
Amount of items: 2
Items: 
Size: 779073 Color: 3
Size: 220765 Color: 17

Bin 3318: 164 of cap free
Amount of items: 2
Items: 
Size: 684251 Color: 3
Size: 315586 Color: 4

Bin 3319: 164 of cap free
Amount of items: 3
Items: 
Size: 614828 Color: 5
Size: 192833 Color: 15
Size: 192176 Color: 2

Bin 3320: 164 of cap free
Amount of items: 2
Items: 
Size: 591085 Color: 17
Size: 408752 Color: 19

Bin 3321: 164 of cap free
Amount of items: 2
Items: 
Size: 772084 Color: 5
Size: 227753 Color: 12

Bin 3322: 164 of cap free
Amount of items: 2
Items: 
Size: 528272 Color: 2
Size: 471565 Color: 11

Bin 3323: 164 of cap free
Amount of items: 2
Items: 
Size: 533224 Color: 11
Size: 466613 Color: 9

Bin 3324: 164 of cap free
Amount of items: 2
Items: 
Size: 669014 Color: 1
Size: 330823 Color: 13

Bin 3325: 164 of cap free
Amount of items: 2
Items: 
Size: 642611 Color: 8
Size: 357226 Color: 7

Bin 3326: 164 of cap free
Amount of items: 2
Items: 
Size: 599664 Color: 9
Size: 400173 Color: 15

Bin 3327: 165 of cap free
Amount of items: 2
Items: 
Size: 583933 Color: 14
Size: 415903 Color: 8

Bin 3328: 165 of cap free
Amount of items: 2
Items: 
Size: 576743 Color: 13
Size: 423093 Color: 7

Bin 3329: 166 of cap free
Amount of items: 2
Items: 
Size: 728577 Color: 16
Size: 271258 Color: 15

Bin 3330: 166 of cap free
Amount of items: 2
Items: 
Size: 644257 Color: 18
Size: 355578 Color: 13

Bin 3331: 166 of cap free
Amount of items: 2
Items: 
Size: 530455 Color: 17
Size: 469380 Color: 10

Bin 3332: 166 of cap free
Amount of items: 2
Items: 
Size: 637159 Color: 15
Size: 362676 Color: 13

Bin 3333: 167 of cap free
Amount of items: 2
Items: 
Size: 698212 Color: 9
Size: 301622 Color: 4

Bin 3334: 167 of cap free
Amount of items: 2
Items: 
Size: 720181 Color: 1
Size: 279653 Color: 3

Bin 3335: 167 of cap free
Amount of items: 2
Items: 
Size: 560934 Color: 13
Size: 438900 Color: 9

Bin 3336: 167 of cap free
Amount of items: 2
Items: 
Size: 622184 Color: 13
Size: 377650 Color: 19

Bin 3337: 167 of cap free
Amount of items: 3
Items: 
Size: 638805 Color: 12
Size: 180517 Color: 4
Size: 180512 Color: 7

Bin 3338: 167 of cap free
Amount of items: 2
Items: 
Size: 673677 Color: 3
Size: 326157 Color: 19

Bin 3339: 168 of cap free
Amount of items: 2
Items: 
Size: 782531 Color: 5
Size: 217302 Color: 12

Bin 3340: 168 of cap free
Amount of items: 2
Items: 
Size: 572229 Color: 17
Size: 427604 Color: 15

Bin 3341: 168 of cap free
Amount of items: 2
Items: 
Size: 773767 Color: 4
Size: 226066 Color: 11

Bin 3342: 168 of cap free
Amount of items: 2
Items: 
Size: 528061 Color: 10
Size: 471772 Color: 6

Bin 3343: 168 of cap free
Amount of items: 2
Items: 
Size: 619098 Color: 18
Size: 380735 Color: 11

Bin 3344: 168 of cap free
Amount of items: 2
Items: 
Size: 643329 Color: 19
Size: 356504 Color: 5

Bin 3345: 169 of cap free
Amount of items: 2
Items: 
Size: 757598 Color: 10
Size: 242234 Color: 2

Bin 3346: 169 of cap free
Amount of items: 2
Items: 
Size: 774173 Color: 15
Size: 225659 Color: 6

Bin 3347: 169 of cap free
Amount of items: 2
Items: 
Size: 679464 Color: 0
Size: 320368 Color: 5

Bin 3348: 169 of cap free
Amount of items: 2
Items: 
Size: 747991 Color: 11
Size: 251841 Color: 6

Bin 3349: 170 of cap free
Amount of items: 2
Items: 
Size: 790030 Color: 14
Size: 209801 Color: 19

Bin 3350: 170 of cap free
Amount of items: 2
Items: 
Size: 530716 Color: 6
Size: 469115 Color: 1

Bin 3351: 170 of cap free
Amount of items: 2
Items: 
Size: 734410 Color: 14
Size: 265421 Color: 3

Bin 3352: 170 of cap free
Amount of items: 3
Items: 
Size: 433578 Color: 13
Size: 293471 Color: 14
Size: 272782 Color: 16

Bin 3353: 170 of cap free
Amount of items: 2
Items: 
Size: 524505 Color: 12
Size: 475326 Color: 18

Bin 3354: 170 of cap free
Amount of items: 2
Items: 
Size: 581649 Color: 4
Size: 418182 Color: 16

Bin 3355: 170 of cap free
Amount of items: 2
Items: 
Size: 725997 Color: 2
Size: 273834 Color: 13

Bin 3356: 171 of cap free
Amount of items: 2
Items: 
Size: 796564 Color: 0
Size: 203266 Color: 4

Bin 3357: 171 of cap free
Amount of items: 2
Items: 
Size: 798136 Color: 8
Size: 201694 Color: 12

Bin 3358: 171 of cap free
Amount of items: 2
Items: 
Size: 664864 Color: 10
Size: 334966 Color: 1

Bin 3359: 171 of cap free
Amount of items: 2
Items: 
Size: 675738 Color: 5
Size: 324092 Color: 14

Bin 3360: 171 of cap free
Amount of items: 2
Items: 
Size: 742836 Color: 18
Size: 256994 Color: 3

Bin 3361: 172 of cap free
Amount of items: 2
Items: 
Size: 650760 Color: 7
Size: 349069 Color: 9

Bin 3362: 172 of cap free
Amount of items: 2
Items: 
Size: 503437 Color: 4
Size: 496392 Color: 18

Bin 3363: 172 of cap free
Amount of items: 2
Items: 
Size: 577385 Color: 8
Size: 422444 Color: 14

Bin 3364: 172 of cap free
Amount of items: 2
Items: 
Size: 660967 Color: 9
Size: 338862 Color: 3

Bin 3365: 173 of cap free
Amount of items: 2
Items: 
Size: 771594 Color: 4
Size: 228234 Color: 3

Bin 3366: 173 of cap free
Amount of items: 2
Items: 
Size: 749248 Color: 11
Size: 250580 Color: 16

Bin 3367: 173 of cap free
Amount of items: 2
Items: 
Size: 510564 Color: 11
Size: 489264 Color: 16

Bin 3368: 173 of cap free
Amount of items: 2
Items: 
Size: 602064 Color: 11
Size: 397764 Color: 18

Bin 3369: 173 of cap free
Amount of items: 2
Items: 
Size: 690341 Color: 10
Size: 309487 Color: 2

Bin 3370: 174 of cap free
Amount of items: 2
Items: 
Size: 607113 Color: 16
Size: 392714 Color: 3

Bin 3371: 174 of cap free
Amount of items: 2
Items: 
Size: 589345 Color: 16
Size: 410482 Color: 7

Bin 3372: 174 of cap free
Amount of items: 2
Items: 
Size: 726381 Color: 18
Size: 273446 Color: 9

Bin 3373: 174 of cap free
Amount of items: 2
Items: 
Size: 564000 Color: 13
Size: 435827 Color: 7

Bin 3374: 174 of cap free
Amount of items: 2
Items: 
Size: 584228 Color: 0
Size: 415599 Color: 10

Bin 3375: 175 of cap free
Amount of items: 3
Items: 
Size: 662883 Color: 18
Size: 177301 Color: 2
Size: 159642 Color: 17

Bin 3376: 175 of cap free
Amount of items: 2
Items: 
Size: 772573 Color: 13
Size: 227253 Color: 2

Bin 3377: 175 of cap free
Amount of items: 2
Items: 
Size: 610083 Color: 16
Size: 389743 Color: 17

Bin 3378: 175 of cap free
Amount of items: 2
Items: 
Size: 609762 Color: 2
Size: 390064 Color: 8

Bin 3379: 175 of cap free
Amount of items: 2
Items: 
Size: 549223 Color: 3
Size: 450603 Color: 2

Bin 3380: 175 of cap free
Amount of items: 2
Items: 
Size: 608019 Color: 8
Size: 391807 Color: 17

Bin 3381: 176 of cap free
Amount of items: 2
Items: 
Size: 621388 Color: 0
Size: 378437 Color: 19

Bin 3382: 176 of cap free
Amount of items: 2
Items: 
Size: 727919 Color: 18
Size: 271906 Color: 2

Bin 3383: 176 of cap free
Amount of items: 2
Items: 
Size: 566491 Color: 5
Size: 433334 Color: 7

Bin 3384: 177 of cap free
Amount of items: 3
Items: 
Size: 618679 Color: 16
Size: 198047 Color: 5
Size: 183098 Color: 2

Bin 3385: 178 of cap free
Amount of items: 2
Items: 
Size: 613437 Color: 4
Size: 386386 Color: 2

Bin 3386: 178 of cap free
Amount of items: 2
Items: 
Size: 541957 Color: 9
Size: 457866 Color: 7

Bin 3387: 178 of cap free
Amount of items: 2
Items: 
Size: 524132 Color: 12
Size: 475691 Color: 5

Bin 3388: 178 of cap free
Amount of items: 2
Items: 
Size: 552256 Color: 7
Size: 447567 Color: 4

Bin 3389: 178 of cap free
Amount of items: 2
Items: 
Size: 664389 Color: 17
Size: 335434 Color: 11

Bin 3390: 178 of cap free
Amount of items: 2
Items: 
Size: 786791 Color: 12
Size: 213032 Color: 2

Bin 3391: 179 of cap free
Amount of items: 2
Items: 
Size: 672693 Color: 15
Size: 327129 Color: 17

Bin 3392: 179 of cap free
Amount of items: 2
Items: 
Size: 594417 Color: 8
Size: 405405 Color: 13

Bin 3393: 179 of cap free
Amount of items: 2
Items: 
Size: 795195 Color: 3
Size: 204627 Color: 10

Bin 3394: 179 of cap free
Amount of items: 2
Items: 
Size: 643920 Color: 4
Size: 355902 Color: 3

Bin 3395: 179 of cap free
Amount of items: 2
Items: 
Size: 739803 Color: 14
Size: 260019 Color: 7

Bin 3396: 180 of cap free
Amount of items: 2
Items: 
Size: 673301 Color: 19
Size: 326520 Color: 16

Bin 3397: 180 of cap free
Amount of items: 2
Items: 
Size: 538523 Color: 2
Size: 461298 Color: 11

Bin 3398: 180 of cap free
Amount of items: 2
Items: 
Size: 527423 Color: 19
Size: 472398 Color: 7

Bin 3399: 180 of cap free
Amount of items: 2
Items: 
Size: 524910 Color: 1
Size: 474911 Color: 5

Bin 3400: 181 of cap free
Amount of items: 2
Items: 
Size: 619460 Color: 9
Size: 380360 Color: 5

Bin 3401: 181 of cap free
Amount of items: 2
Items: 
Size: 523610 Color: 10
Size: 476210 Color: 3

Bin 3402: 181 of cap free
Amount of items: 2
Items: 
Size: 657526 Color: 15
Size: 342294 Color: 11

Bin 3403: 182 of cap free
Amount of items: 3
Items: 
Size: 646239 Color: 16
Size: 186895 Color: 6
Size: 166685 Color: 5

Bin 3404: 182 of cap free
Amount of items: 3
Items: 
Size: 780261 Color: 11
Size: 110062 Color: 7
Size: 109496 Color: 2

Bin 3405: 182 of cap free
Amount of items: 2
Items: 
Size: 525690 Color: 17
Size: 474129 Color: 1

Bin 3406: 182 of cap free
Amount of items: 2
Items: 
Size: 558343 Color: 19
Size: 441476 Color: 15

Bin 3407: 182 of cap free
Amount of items: 2
Items: 
Size: 618300 Color: 13
Size: 381519 Color: 18

Bin 3408: 182 of cap free
Amount of items: 2
Items: 
Size: 644566 Color: 3
Size: 355253 Color: 4

Bin 3409: 183 of cap free
Amount of items: 2
Items: 
Size: 747533 Color: 6
Size: 252285 Color: 11

Bin 3410: 183 of cap free
Amount of items: 2
Items: 
Size: 592932 Color: 10
Size: 406886 Color: 11

Bin 3411: 183 of cap free
Amount of items: 2
Items: 
Size: 503707 Color: 16
Size: 496111 Color: 12

Bin 3412: 183 of cap free
Amount of items: 2
Items: 
Size: 572729 Color: 10
Size: 427089 Color: 6

Bin 3413: 183 of cap free
Amount of items: 2
Items: 
Size: 585469 Color: 8
Size: 414349 Color: 6

Bin 3414: 184 of cap free
Amount of items: 2
Items: 
Size: 554038 Color: 11
Size: 445779 Color: 17

Bin 3415: 184 of cap free
Amount of items: 2
Items: 
Size: 714096 Color: 19
Size: 285721 Color: 11

Bin 3416: 184 of cap free
Amount of items: 2
Items: 
Size: 694864 Color: 2
Size: 304953 Color: 15

Bin 3417: 184 of cap free
Amount of items: 2
Items: 
Size: 531822 Color: 6
Size: 467995 Color: 8

Bin 3418: 184 of cap free
Amount of items: 2
Items: 
Size: 541687 Color: 9
Size: 458130 Color: 19

Bin 3419: 185 of cap free
Amount of items: 2
Items: 
Size: 753608 Color: 7
Size: 246208 Color: 16

Bin 3420: 185 of cap free
Amount of items: 2
Items: 
Size: 527150 Color: 19
Size: 472666 Color: 2

Bin 3421: 185 of cap free
Amount of items: 2
Items: 
Size: 559626 Color: 10
Size: 440190 Color: 5

Bin 3422: 185 of cap free
Amount of items: 2
Items: 
Size: 575891 Color: 12
Size: 423925 Color: 18

Bin 3423: 185 of cap free
Amount of items: 2
Items: 
Size: 618675 Color: 10
Size: 381141 Color: 7

Bin 3424: 186 of cap free
Amount of items: 3
Items: 
Size: 752634 Color: 3
Size: 129007 Color: 12
Size: 118174 Color: 19

Bin 3425: 186 of cap free
Amount of items: 2
Items: 
Size: 590793 Color: 17
Size: 409022 Color: 7

Bin 3426: 186 of cap free
Amount of items: 2
Items: 
Size: 639483 Color: 8
Size: 360332 Color: 14

Bin 3427: 186 of cap free
Amount of items: 3
Items: 
Size: 715656 Color: 4
Size: 142335 Color: 8
Size: 141824 Color: 15

Bin 3428: 186 of cap free
Amount of items: 2
Items: 
Size: 795731 Color: 9
Size: 204084 Color: 10

Bin 3429: 186 of cap free
Amount of items: 2
Items: 
Size: 563124 Color: 0
Size: 436691 Color: 6

Bin 3430: 187 of cap free
Amount of items: 2
Items: 
Size: 799192 Color: 10
Size: 200622 Color: 7

Bin 3431: 188 of cap free
Amount of items: 2
Items: 
Size: 599151 Color: 19
Size: 400662 Color: 4

Bin 3432: 188 of cap free
Amount of items: 2
Items: 
Size: 705076 Color: 11
Size: 294737 Color: 14

Bin 3433: 188 of cap free
Amount of items: 2
Items: 
Size: 622445 Color: 13
Size: 377368 Color: 12

Bin 3434: 188 of cap free
Amount of items: 2
Items: 
Size: 743701 Color: 13
Size: 256112 Color: 6

Bin 3435: 190 of cap free
Amount of items: 2
Items: 
Size: 704827 Color: 3
Size: 294984 Color: 18

Bin 3436: 190 of cap free
Amount of items: 2
Items: 
Size: 718821 Color: 14
Size: 280990 Color: 7

Bin 3437: 190 of cap free
Amount of items: 2
Items: 
Size: 568971 Color: 7
Size: 430840 Color: 1

Bin 3438: 190 of cap free
Amount of items: 2
Items: 
Size: 574170 Color: 19
Size: 425641 Color: 5

Bin 3439: 190 of cap free
Amount of items: 2
Items: 
Size: 709604 Color: 5
Size: 290207 Color: 18

Bin 3440: 190 of cap free
Amount of items: 2
Items: 
Size: 786783 Color: 2
Size: 213028 Color: 12

Bin 3441: 191 of cap free
Amount of items: 2
Items: 
Size: 596969 Color: 9
Size: 402841 Color: 5

Bin 3442: 191 of cap free
Amount of items: 2
Items: 
Size: 678975 Color: 13
Size: 320835 Color: 4

Bin 3443: 192 of cap free
Amount of items: 2
Items: 
Size: 577822 Color: 5
Size: 421987 Color: 0

Bin 3444: 192 of cap free
Amount of items: 2
Items: 
Size: 723688 Color: 3
Size: 276121 Color: 14

Bin 3445: 193 of cap free
Amount of items: 2
Items: 
Size: 626081 Color: 11
Size: 373727 Color: 3

Bin 3446: 193 of cap free
Amount of items: 2
Items: 
Size: 788972 Color: 3
Size: 210836 Color: 15

Bin 3447: 193 of cap free
Amount of items: 2
Items: 
Size: 557668 Color: 6
Size: 442140 Color: 15

Bin 3448: 194 of cap free
Amount of items: 2
Items: 
Size: 722823 Color: 15
Size: 276984 Color: 3

Bin 3449: 194 of cap free
Amount of items: 2
Items: 
Size: 502330 Color: 14
Size: 497477 Color: 6

Bin 3450: 194 of cap free
Amount of items: 2
Items: 
Size: 501542 Color: 14
Size: 498265 Color: 8

Bin 3451: 194 of cap free
Amount of items: 2
Items: 
Size: 566076 Color: 2
Size: 433731 Color: 6

Bin 3452: 194 of cap free
Amount of items: 2
Items: 
Size: 772359 Color: 8
Size: 227448 Color: 19

Bin 3453: 195 of cap free
Amount of items: 2
Items: 
Size: 785291 Color: 16
Size: 214515 Color: 4

Bin 3454: 195 of cap free
Amount of items: 2
Items: 
Size: 754406 Color: 12
Size: 245400 Color: 14

Bin 3455: 195 of cap free
Amount of items: 2
Items: 
Size: 710368 Color: 12
Size: 289438 Color: 3

Bin 3456: 195 of cap free
Amount of items: 2
Items: 
Size: 747226 Color: 1
Size: 252580 Color: 18

Bin 3457: 195 of cap free
Amount of items: 2
Items: 
Size: 695597 Color: 13
Size: 304209 Color: 0

Bin 3458: 195 of cap free
Amount of items: 2
Items: 
Size: 650788 Color: 16
Size: 349018 Color: 19

Bin 3459: 195 of cap free
Amount of items: 2
Items: 
Size: 600680 Color: 2
Size: 399126 Color: 11

Bin 3460: 196 of cap free
Amount of items: 2
Items: 
Size: 614838 Color: 3
Size: 384967 Color: 11

Bin 3461: 196 of cap free
Amount of items: 3
Items: 
Size: 536643 Color: 13
Size: 298897 Color: 18
Size: 164265 Color: 10

Bin 3462: 196 of cap free
Amount of items: 2
Items: 
Size: 544000 Color: 16
Size: 455805 Color: 15

Bin 3463: 196 of cap free
Amount of items: 2
Items: 
Size: 558785 Color: 5
Size: 441020 Color: 7

Bin 3464: 197 of cap free
Amount of items: 2
Items: 
Size: 561718 Color: 2
Size: 438086 Color: 4

Bin 3465: 198 of cap free
Amount of items: 2
Items: 
Size: 668509 Color: 2
Size: 331294 Color: 17

Bin 3466: 198 of cap free
Amount of items: 2
Items: 
Size: 655975 Color: 0
Size: 343828 Color: 14

Bin 3467: 198 of cap free
Amount of items: 2
Items: 
Size: 504133 Color: 10
Size: 495670 Color: 8

Bin 3468: 198 of cap free
Amount of items: 2
Items: 
Size: 532956 Color: 0
Size: 466847 Color: 5

Bin 3469: 199 of cap free
Amount of items: 2
Items: 
Size: 685975 Color: 2
Size: 313827 Color: 14

Bin 3470: 199 of cap free
Amount of items: 2
Items: 
Size: 761381 Color: 9
Size: 238421 Color: 19

Bin 3471: 199 of cap free
Amount of items: 2
Items: 
Size: 590784 Color: 7
Size: 409018 Color: 3

Bin 3472: 200 of cap free
Amount of items: 3
Items: 
Size: 706606 Color: 15
Size: 147002 Color: 10
Size: 146193 Color: 8

Bin 3473: 200 of cap free
Amount of items: 2
Items: 
Size: 668717 Color: 5
Size: 331084 Color: 12

Bin 3474: 200 of cap free
Amount of items: 2
Items: 
Size: 561939 Color: 13
Size: 437862 Color: 11

Bin 3475: 200 of cap free
Amount of items: 2
Items: 
Size: 564526 Color: 4
Size: 435275 Color: 0

Bin 3476: 200 of cap free
Amount of items: 2
Items: 
Size: 678229 Color: 16
Size: 321572 Color: 19

Bin 3477: 200 of cap free
Amount of items: 2
Items: 
Size: 716242 Color: 4
Size: 283559 Color: 16

Bin 3478: 201 of cap free
Amount of items: 2
Items: 
Size: 673594 Color: 19
Size: 326206 Color: 3

Bin 3479: 201 of cap free
Amount of items: 2
Items: 
Size: 681951 Color: 15
Size: 317849 Color: 3

Bin 3480: 201 of cap free
Amount of items: 2
Items: 
Size: 532053 Color: 15
Size: 467747 Color: 3

Bin 3481: 201 of cap free
Amount of items: 2
Items: 
Size: 571559 Color: 13
Size: 428241 Color: 9

Bin 3482: 202 of cap free
Amount of items: 2
Items: 
Size: 766917 Color: 10
Size: 232882 Color: 9

Bin 3483: 204 of cap free
Amount of items: 2
Items: 
Size: 594564 Color: 19
Size: 405233 Color: 8

Bin 3484: 204 of cap free
Amount of items: 2
Items: 
Size: 770532 Color: 9
Size: 229265 Color: 18

Bin 3485: 205 of cap free
Amount of items: 2
Items: 
Size: 782571 Color: 17
Size: 217225 Color: 16

Bin 3486: 205 of cap free
Amount of items: 2
Items: 
Size: 637513 Color: 9
Size: 362283 Color: 2

Bin 3487: 205 of cap free
Amount of items: 2
Items: 
Size: 579846 Color: 19
Size: 419950 Color: 1

Bin 3488: 206 of cap free
Amount of items: 2
Items: 
Size: 525291 Color: 3
Size: 474504 Color: 0

Bin 3489: 206 of cap free
Amount of items: 2
Items: 
Size: 582552 Color: 6
Size: 417243 Color: 9

Bin 3490: 207 of cap free
Amount of items: 2
Items: 
Size: 624101 Color: 2
Size: 375693 Color: 6

Bin 3491: 207 of cap free
Amount of items: 2
Items: 
Size: 529472 Color: 4
Size: 470322 Color: 13

Bin 3492: 207 of cap free
Amount of items: 2
Items: 
Size: 628232 Color: 17
Size: 371562 Color: 8

Bin 3493: 208 of cap free
Amount of items: 2
Items: 
Size: 696320 Color: 6
Size: 303473 Color: 10

Bin 3494: 208 of cap free
Amount of items: 2
Items: 
Size: 631717 Color: 11
Size: 368076 Color: 2

Bin 3495: 208 of cap free
Amount of items: 2
Items: 
Size: 591080 Color: 2
Size: 408713 Color: 12

Bin 3496: 208 of cap free
Amount of items: 2
Items: 
Size: 763063 Color: 10
Size: 236730 Color: 14

Bin 3497: 208 of cap free
Amount of items: 2
Items: 
Size: 546084 Color: 14
Size: 453709 Color: 15

Bin 3498: 209 of cap free
Amount of items: 2
Items: 
Size: 615557 Color: 5
Size: 384235 Color: 6

Bin 3499: 209 of cap free
Amount of items: 2
Items: 
Size: 679441 Color: 2
Size: 320351 Color: 13

Bin 3500: 209 of cap free
Amount of items: 2
Items: 
Size: 723625 Color: 1
Size: 276167 Color: 3

Bin 3501: 209 of cap free
Amount of items: 2
Items: 
Size: 517673 Color: 19
Size: 482119 Color: 6

Bin 3502: 210 of cap free
Amount of items: 2
Items: 
Size: 710114 Color: 18
Size: 289677 Color: 14

Bin 3503: 210 of cap free
Amount of items: 2
Items: 
Size: 601352 Color: 19
Size: 398439 Color: 4

Bin 3504: 211 of cap free
Amount of items: 2
Items: 
Size: 689009 Color: 2
Size: 310781 Color: 9

Bin 3505: 211 of cap free
Amount of items: 2
Items: 
Size: 743350 Color: 16
Size: 256440 Color: 9

Bin 3506: 211 of cap free
Amount of items: 2
Items: 
Size: 504674 Color: 18
Size: 495116 Color: 5

Bin 3507: 211 of cap free
Amount of items: 2
Items: 
Size: 534265 Color: 13
Size: 465525 Color: 14

Bin 3508: 212 of cap free
Amount of items: 2
Items: 
Size: 631327 Color: 2
Size: 368462 Color: 18

Bin 3509: 212 of cap free
Amount of items: 2
Items: 
Size: 695628 Color: 16
Size: 304161 Color: 5

Bin 3510: 212 of cap free
Amount of items: 2
Items: 
Size: 589785 Color: 7
Size: 410004 Color: 19

Bin 3511: 212 of cap free
Amount of items: 2
Items: 
Size: 557068 Color: 13
Size: 442721 Color: 5

Bin 3512: 212 of cap free
Amount of items: 2
Items: 
Size: 535284 Color: 9
Size: 464505 Color: 14

Bin 3513: 212 of cap free
Amount of items: 2
Items: 
Size: 542272 Color: 7
Size: 457517 Color: 18

Bin 3514: 214 of cap free
Amount of items: 2
Items: 
Size: 725208 Color: 15
Size: 274579 Color: 12

Bin 3515: 214 of cap free
Amount of items: 2
Items: 
Size: 671814 Color: 9
Size: 327973 Color: 6

Bin 3516: 214 of cap free
Amount of items: 2
Items: 
Size: 755006 Color: 0
Size: 244781 Color: 7

Bin 3517: 214 of cap free
Amount of items: 2
Items: 
Size: 500205 Color: 1
Size: 499582 Color: 6

Bin 3518: 215 of cap free
Amount of items: 2
Items: 
Size: 685870 Color: 11
Size: 313916 Color: 8

Bin 3519: 215 of cap free
Amount of items: 2
Items: 
Size: 525689 Color: 8
Size: 474097 Color: 6

Bin 3520: 216 of cap free
Amount of items: 2
Items: 
Size: 546700 Color: 7
Size: 453085 Color: 6

Bin 3521: 216 of cap free
Amount of items: 2
Items: 
Size: 556656 Color: 0
Size: 443129 Color: 1

Bin 3522: 216 of cap free
Amount of items: 2
Items: 
Size: 569926 Color: 9
Size: 429859 Color: 19

Bin 3523: 216 of cap free
Amount of items: 2
Items: 
Size: 653113 Color: 7
Size: 346672 Color: 5

Bin 3524: 218 of cap free
Amount of items: 2
Items: 
Size: 652495 Color: 12
Size: 347288 Color: 4

Bin 3525: 218 of cap free
Amount of items: 2
Items: 
Size: 666240 Color: 17
Size: 333543 Color: 19

Bin 3526: 218 of cap free
Amount of items: 2
Items: 
Size: 541668 Color: 18
Size: 458115 Color: 4

Bin 3527: 218 of cap free
Amount of items: 2
Items: 
Size: 566895 Color: 11
Size: 432888 Color: 18

Bin 3528: 219 of cap free
Amount of items: 2
Items: 
Size: 524108 Color: 6
Size: 475674 Color: 13

Bin 3529: 219 of cap free
Amount of items: 2
Items: 
Size: 639726 Color: 3
Size: 360056 Color: 4

Bin 3530: 220 of cap free
Amount of items: 2
Items: 
Size: 709129 Color: 4
Size: 290652 Color: 18

Bin 3531: 220 of cap free
Amount of items: 2
Items: 
Size: 674292 Color: 10
Size: 325489 Color: 4

Bin 3532: 221 of cap free
Amount of items: 2
Items: 
Size: 578426 Color: 11
Size: 421354 Color: 14

Bin 3533: 222 of cap free
Amount of items: 2
Items: 
Size: 760219 Color: 16
Size: 239560 Color: 11

Bin 3534: 222 of cap free
Amount of items: 2
Items: 
Size: 514827 Color: 12
Size: 484952 Color: 7

Bin 3535: 222 of cap free
Amount of items: 2
Items: 
Size: 728177 Color: 14
Size: 271602 Color: 4

Bin 3536: 222 of cap free
Amount of items: 2
Items: 
Size: 569586 Color: 11
Size: 430193 Color: 1

Bin 3537: 222 of cap free
Amount of items: 2
Items: 
Size: 633347 Color: 3
Size: 366432 Color: 19

Bin 3538: 223 of cap free
Amount of items: 2
Items: 
Size: 639446 Color: 10
Size: 360332 Color: 9

Bin 3539: 225 of cap free
Amount of items: 2
Items: 
Size: 795698 Color: 3
Size: 204078 Color: 11

Bin 3540: 226 of cap free
Amount of items: 2
Items: 
Size: 643875 Color: 1
Size: 355900 Color: 11

Bin 3541: 227 of cap free
Amount of items: 2
Items: 
Size: 593421 Color: 15
Size: 406353 Color: 11

Bin 3542: 228 of cap free
Amount of items: 2
Items: 
Size: 537743 Color: 8
Size: 462030 Color: 16

Bin 3543: 228 of cap free
Amount of items: 2
Items: 
Size: 749194 Color: 13
Size: 250579 Color: 19

Bin 3544: 228 of cap free
Amount of items: 2
Items: 
Size: 513892 Color: 14
Size: 485881 Color: 9

Bin 3545: 228 of cap free
Amount of items: 2
Items: 
Size: 529610 Color: 16
Size: 470163 Color: 14

Bin 3546: 230 of cap free
Amount of items: 2
Items: 
Size: 776258 Color: 8
Size: 223513 Color: 18

Bin 3547: 230 of cap free
Amount of items: 2
Items: 
Size: 785698 Color: 2
Size: 214073 Color: 11

Bin 3548: 231 of cap free
Amount of items: 2
Items: 
Size: 521071 Color: 17
Size: 478699 Color: 14

Bin 3549: 232 of cap free
Amount of items: 2
Items: 
Size: 665242 Color: 4
Size: 334527 Color: 16

Bin 3550: 232 of cap free
Amount of items: 2
Items: 
Size: 696677 Color: 9
Size: 303092 Color: 17

Bin 3551: 232 of cap free
Amount of items: 2
Items: 
Size: 636823 Color: 14
Size: 362946 Color: 2

Bin 3552: 233 of cap free
Amount of items: 2
Items: 
Size: 524902 Color: 12
Size: 474866 Color: 10

Bin 3553: 233 of cap free
Amount of items: 2
Items: 
Size: 548346 Color: 16
Size: 451422 Color: 3

Bin 3554: 234 of cap free
Amount of items: 2
Items: 
Size: 662226 Color: 18
Size: 337541 Color: 19

Bin 3555: 234 of cap free
Amount of items: 2
Items: 
Size: 565039 Color: 16
Size: 434728 Color: 18

Bin 3556: 235 of cap free
Amount of items: 2
Items: 
Size: 658769 Color: 14
Size: 340997 Color: 6

Bin 3557: 235 of cap free
Amount of items: 2
Items: 
Size: 759098 Color: 7
Size: 240668 Color: 5

Bin 3558: 235 of cap free
Amount of items: 2
Items: 
Size: 648965 Color: 1
Size: 350801 Color: 16

Bin 3559: 236 of cap free
Amount of items: 2
Items: 
Size: 528018 Color: 6
Size: 471747 Color: 18

Bin 3560: 238 of cap free
Amount of items: 2
Items: 
Size: 787906 Color: 2
Size: 211857 Color: 8

Bin 3561: 238 of cap free
Amount of items: 2
Items: 
Size: 619037 Color: 1
Size: 380726 Color: 19

Bin 3562: 238 of cap free
Amount of items: 2
Items: 
Size: 683428 Color: 15
Size: 316335 Color: 10

Bin 3563: 239 of cap free
Amount of items: 3
Items: 
Size: 719176 Color: 8
Size: 157888 Color: 18
Size: 122698 Color: 8

Bin 3564: 239 of cap free
Amount of items: 2
Items: 
Size: 506771 Color: 16
Size: 492991 Color: 1

Bin 3565: 240 of cap free
Amount of items: 2
Items: 
Size: 587542 Color: 13
Size: 412219 Color: 0

Bin 3566: 241 of cap free
Amount of items: 2
Items: 
Size: 504671 Color: 11
Size: 495089 Color: 5

Bin 3567: 241 of cap free
Amount of items: 2
Items: 
Size: 600158 Color: 6
Size: 399602 Color: 15

Bin 3568: 242 of cap free
Amount of items: 2
Items: 
Size: 679173 Color: 13
Size: 320586 Color: 18

Bin 3569: 242 of cap free
Amount of items: 2
Items: 
Size: 587727 Color: 0
Size: 412032 Color: 13

Bin 3570: 243 of cap free
Amount of items: 2
Items: 
Size: 788385 Color: 9
Size: 211373 Color: 8

Bin 3571: 243 of cap free
Amount of items: 2
Items: 
Size: 506448 Color: 5
Size: 493310 Color: 4

Bin 3572: 244 of cap free
Amount of items: 3
Items: 
Size: 753307 Color: 14
Size: 130560 Color: 3
Size: 115890 Color: 9

Bin 3573: 244 of cap free
Amount of items: 2
Items: 
Size: 751375 Color: 2
Size: 248382 Color: 1

Bin 3574: 245 of cap free
Amount of items: 2
Items: 
Size: 727911 Color: 9
Size: 271845 Color: 13

Bin 3575: 245 of cap free
Amount of items: 2
Items: 
Size: 708495 Color: 5
Size: 291261 Color: 3

Bin 3576: 246 of cap free
Amount of items: 2
Items: 
Size: 779035 Color: 5
Size: 220720 Color: 17

Bin 3577: 246 of cap free
Amount of items: 2
Items: 
Size: 511910 Color: 13
Size: 487845 Color: 0

Bin 3578: 247 of cap free
Amount of items: 2
Items: 
Size: 682534 Color: 13
Size: 317220 Color: 17

Bin 3579: 247 of cap free
Amount of items: 2
Items: 
Size: 511122 Color: 14
Size: 488632 Color: 2

Bin 3580: 247 of cap free
Amount of items: 2
Items: 
Size: 556631 Color: 5
Size: 443123 Color: 15

Bin 3581: 248 of cap free
Amount of items: 2
Items: 
Size: 607470 Color: 1
Size: 392283 Color: 4

Bin 3582: 248 of cap free
Amount of items: 2
Items: 
Size: 619863 Color: 0
Size: 379890 Color: 9

Bin 3583: 249 of cap free
Amount of items: 3
Items: 
Size: 632210 Color: 16
Size: 185005 Color: 11
Size: 182537 Color: 17

Bin 3584: 249 of cap free
Amount of items: 2
Items: 
Size: 782479 Color: 5
Size: 217273 Color: 13

Bin 3585: 249 of cap free
Amount of items: 2
Items: 
Size: 548502 Color: 13
Size: 451250 Color: 16

Bin 3586: 250 of cap free
Amount of items: 2
Items: 
Size: 724295 Color: 9
Size: 275456 Color: 19

Bin 3587: 250 of cap free
Amount of items: 2
Items: 
Size: 686814 Color: 7
Size: 312937 Color: 19

Bin 3588: 250 of cap free
Amount of items: 2
Items: 
Size: 615157 Color: 19
Size: 384594 Color: 11

Bin 3589: 250 of cap free
Amount of items: 2
Items: 
Size: 713830 Color: 10
Size: 285921 Color: 4

Bin 3590: 250 of cap free
Amount of items: 2
Items: 
Size: 757118 Color: 5
Size: 242633 Color: 13

Bin 3591: 251 of cap free
Amount of items: 2
Items: 
Size: 723895 Color: 0
Size: 275855 Color: 14

Bin 3592: 251 of cap free
Amount of items: 2
Items: 
Size: 797675 Color: 10
Size: 202075 Color: 7

Bin 3593: 251 of cap free
Amount of items: 2
Items: 
Size: 504104 Color: 7
Size: 495646 Color: 16

Bin 3594: 251 of cap free
Amount of items: 2
Items: 
Size: 549427 Color: 11
Size: 450323 Color: 18

Bin 3595: 251 of cap free
Amount of items: 2
Items: 
Size: 551681 Color: 18
Size: 448069 Color: 17

Bin 3596: 252 of cap free
Amount of items: 2
Items: 
Size: 724295 Color: 18
Size: 275454 Color: 16

Bin 3597: 252 of cap free
Amount of items: 2
Items: 
Size: 667539 Color: 2
Size: 332210 Color: 14

Bin 3598: 252 of cap free
Amount of items: 2
Items: 
Size: 542267 Color: 2
Size: 457482 Color: 9

Bin 3599: 252 of cap free
Amount of items: 2
Items: 
Size: 571532 Color: 4
Size: 428217 Color: 0

Bin 3600: 253 of cap free
Amount of items: 2
Items: 
Size: 506611 Color: 3
Size: 493137 Color: 7

Bin 3601: 254 of cap free
Amount of items: 2
Items: 
Size: 532030 Color: 2
Size: 467717 Color: 11

Bin 3602: 254 of cap free
Amount of items: 2
Items: 
Size: 718736 Color: 15
Size: 281011 Color: 9

Bin 3603: 254 of cap free
Amount of items: 2
Items: 
Size: 753995 Color: 12
Size: 245752 Color: 7

Bin 3604: 254 of cap free
Amount of items: 2
Items: 
Size: 552975 Color: 1
Size: 446772 Color: 14

Bin 3605: 254 of cap free
Amount of items: 2
Items: 
Size: 591806 Color: 1
Size: 407941 Color: 8

Bin 3606: 254 of cap free
Amount of items: 2
Items: 
Size: 678719 Color: 3
Size: 321028 Color: 2

Bin 3607: 255 of cap free
Amount of items: 2
Items: 
Size: 502590 Color: 14
Size: 497156 Color: 8

Bin 3608: 255 of cap free
Amount of items: 2
Items: 
Size: 543365 Color: 13
Size: 456381 Color: 15

Bin 3609: 255 of cap free
Amount of items: 2
Items: 
Size: 550069 Color: 12
Size: 449677 Color: 17

Bin 3610: 255 of cap free
Amount of items: 2
Items: 
Size: 644571 Color: 4
Size: 355175 Color: 14

Bin 3611: 256 of cap free
Amount of items: 2
Items: 
Size: 632651 Color: 3
Size: 367094 Color: 16

Bin 3612: 256 of cap free
Amount of items: 2
Items: 
Size: 530638 Color: 9
Size: 469107 Color: 16

Bin 3613: 256 of cap free
Amount of items: 2
Items: 
Size: 512350 Color: 8
Size: 487395 Color: 15

Bin 3614: 256 of cap free
Amount of items: 2
Items: 
Size: 563021 Color: 1
Size: 436724 Color: 3

Bin 3615: 256 of cap free
Amount of items: 2
Items: 
Size: 578392 Color: 10
Size: 421353 Color: 14

Bin 3616: 256 of cap free
Amount of items: 2
Items: 
Size: 711109 Color: 2
Size: 288636 Color: 17

Bin 3617: 257 of cap free
Amount of items: 2
Items: 
Size: 793997 Color: 4
Size: 205747 Color: 0

Bin 3618: 257 of cap free
Amount of items: 2
Items: 
Size: 677668 Color: 14
Size: 322076 Color: 2

Bin 3619: 258 of cap free
Amount of items: 2
Items: 
Size: 791294 Color: 17
Size: 208449 Color: 2

Bin 3620: 258 of cap free
Amount of items: 2
Items: 
Size: 567416 Color: 16
Size: 432327 Color: 12

Bin 3621: 259 of cap free
Amount of items: 2
Items: 
Size: 650696 Color: 12
Size: 349046 Color: 5

Bin 3622: 259 of cap free
Amount of items: 2
Items: 
Size: 630512 Color: 7
Size: 369230 Color: 11

Bin 3623: 259 of cap free
Amount of items: 2
Items: 
Size: 516249 Color: 8
Size: 483493 Color: 14

Bin 3624: 259 of cap free
Amount of items: 2
Items: 
Size: 517980 Color: 15
Size: 481762 Color: 8

Bin 3625: 260 of cap free
Amount of items: 2
Items: 
Size: 554407 Color: 19
Size: 445334 Color: 6

Bin 3626: 260 of cap free
Amount of items: 2
Items: 
Size: 574682 Color: 18
Size: 425059 Color: 3

Bin 3627: 260 of cap free
Amount of items: 2
Items: 
Size: 634585 Color: 14
Size: 365156 Color: 1

Bin 3628: 261 of cap free
Amount of items: 2
Items: 
Size: 664312 Color: 0
Size: 335428 Color: 6

Bin 3629: 262 of cap free
Amount of items: 2
Items: 
Size: 550068 Color: 10
Size: 449671 Color: 2

Bin 3630: 262 of cap free
Amount of items: 2
Items: 
Size: 657486 Color: 14
Size: 342253 Color: 10

Bin 3631: 263 of cap free
Amount of items: 2
Items: 
Size: 536806 Color: 3
Size: 462932 Color: 11

Bin 3632: 263 of cap free
Amount of items: 2
Items: 
Size: 515692 Color: 11
Size: 484046 Color: 4

Bin 3633: 264 of cap free
Amount of items: 2
Items: 
Size: 742776 Color: 18
Size: 256961 Color: 13

Bin 3634: 265 of cap free
Amount of items: 2
Items: 
Size: 795129 Color: 2
Size: 204607 Color: 10

Bin 3635: 266 of cap free
Amount of items: 2
Items: 
Size: 629872 Color: 9
Size: 369863 Color: 3

Bin 3636: 266 of cap free
Amount of items: 2
Items: 
Size: 611372 Color: 17
Size: 388363 Color: 18

Bin 3637: 266 of cap free
Amount of items: 2
Items: 
Size: 778182 Color: 7
Size: 221553 Color: 15

Bin 3638: 267 of cap free
Amount of items: 2
Items: 
Size: 544042 Color: 12
Size: 455692 Color: 7

Bin 3639: 267 of cap free
Amount of items: 2
Items: 
Size: 613921 Color: 16
Size: 385813 Color: 11

Bin 3640: 268 of cap free
Amount of items: 2
Items: 
Size: 738940 Color: 16
Size: 260793 Color: 4

Bin 3641: 268 of cap free
Amount of items: 3
Items: 
Size: 739719 Color: 9
Size: 130150 Color: 17
Size: 129864 Color: 12

Bin 3642: 268 of cap free
Amount of items: 2
Items: 
Size: 637114 Color: 12
Size: 362619 Color: 10

Bin 3643: 269 of cap free
Amount of items: 2
Items: 
Size: 692328 Color: 16
Size: 307404 Color: 14

Bin 3644: 269 of cap free
Amount of items: 2
Items: 
Size: 796550 Color: 12
Size: 203182 Color: 11

Bin 3645: 269 of cap free
Amount of items: 2
Items: 
Size: 794669 Color: 19
Size: 205063 Color: 3

Bin 3646: 269 of cap free
Amount of items: 2
Items: 
Size: 646724 Color: 2
Size: 353008 Color: 17

Bin 3647: 270 of cap free
Amount of items: 2
Items: 
Size: 538904 Color: 19
Size: 460827 Color: 17

Bin 3648: 270 of cap free
Amount of items: 2
Items: 
Size: 505153 Color: 9
Size: 494578 Color: 17

Bin 3649: 270 of cap free
Amount of items: 2
Items: 
Size: 539350 Color: 0
Size: 460381 Color: 4

Bin 3650: 270 of cap free
Amount of items: 2
Items: 
Size: 552189 Color: 12
Size: 447542 Color: 0

Bin 3651: 271 of cap free
Amount of items: 2
Items: 
Size: 527145 Color: 8
Size: 472585 Color: 18

Bin 3652: 271 of cap free
Amount of items: 2
Items: 
Size: 601338 Color: 0
Size: 398392 Color: 1

Bin 3653: 271 of cap free
Amount of items: 2
Items: 
Size: 793352 Color: 17
Size: 206378 Color: 12

Bin 3654: 272 of cap free
Amount of items: 3
Items: 
Size: 751359 Color: 0
Size: 127801 Color: 13
Size: 120569 Color: 8

Bin 3655: 272 of cap free
Amount of items: 2
Items: 
Size: 774947 Color: 19
Size: 224782 Color: 0

Bin 3656: 273 of cap free
Amount of items: 2
Items: 
Size: 537425 Color: 8
Size: 462303 Color: 2

Bin 3657: 273 of cap free
Amount of items: 2
Items: 
Size: 606143 Color: 18
Size: 393585 Color: 15

Bin 3658: 274 of cap free
Amount of items: 2
Items: 
Size: 508191 Color: 8
Size: 491536 Color: 18

Bin 3659: 274 of cap free
Amount of items: 2
Items: 
Size: 553264 Color: 6
Size: 446463 Color: 10

Bin 3660: 274 of cap free
Amount of items: 2
Items: 
Size: 741328 Color: 3
Size: 258399 Color: 9

Bin 3661: 276 of cap free
Amount of items: 2
Items: 
Size: 621865 Color: 19
Size: 377860 Color: 0

Bin 3662: 277 of cap free
Amount of items: 2
Items: 
Size: 645681 Color: 3
Size: 354043 Color: 6

Bin 3663: 277 of cap free
Amount of items: 2
Items: 
Size: 758061 Color: 17
Size: 241663 Color: 9

Bin 3664: 277 of cap free
Amount of items: 2
Items: 
Size: 689331 Color: 7
Size: 310393 Color: 8

Bin 3665: 277 of cap free
Amount of items: 2
Items: 
Size: 559539 Color: 0
Size: 440185 Color: 9

Bin 3666: 278 of cap free
Amount of items: 2
Items: 
Size: 680926 Color: 6
Size: 318797 Color: 10

Bin 3667: 278 of cap free
Amount of items: 2
Items: 
Size: 677032 Color: 16
Size: 322691 Color: 13

Bin 3668: 278 of cap free
Amount of items: 2
Items: 
Size: 574844 Color: 7
Size: 424879 Color: 14

Bin 3669: 278 of cap free
Amount of items: 2
Items: 
Size: 648234 Color: 18
Size: 351489 Color: 1

Bin 3670: 280 of cap free
Amount of items: 2
Items: 
Size: 780895 Color: 14
Size: 218826 Color: 5

Bin 3671: 281 of cap free
Amount of items: 2
Items: 
Size: 605522 Color: 11
Size: 394198 Color: 16

Bin 3672: 281 of cap free
Amount of items: 2
Items: 
Size: 768972 Color: 4
Size: 230748 Color: 5

Bin 3673: 281 of cap free
Amount of items: 2
Items: 
Size: 592835 Color: 4
Size: 406885 Color: 8

Bin 3674: 283 of cap free
Amount of items: 2
Items: 
Size: 740075 Color: 18
Size: 259643 Color: 12

Bin 3675: 284 of cap free
Amount of items: 2
Items: 
Size: 555831 Color: 2
Size: 443886 Color: 16

Bin 3676: 285 of cap free
Amount of items: 2
Items: 
Size: 736315 Color: 19
Size: 263401 Color: 8

Bin 3677: 286 of cap free
Amount of items: 3
Items: 
Size: 339192 Color: 19
Size: 330700 Color: 5
Size: 329823 Color: 14

Bin 3678: 286 of cap free
Amount of items: 2
Items: 
Size: 693421 Color: 16
Size: 306294 Color: 7

Bin 3679: 286 of cap free
Amount of items: 2
Items: 
Size: 730820 Color: 18
Size: 268895 Color: 11

Bin 3680: 287 of cap free
Amount of items: 2
Items: 
Size: 766356 Color: 19
Size: 233358 Color: 7

Bin 3681: 287 of cap free
Amount of items: 2
Items: 
Size: 759079 Color: 1
Size: 240635 Color: 15

Bin 3682: 287 of cap free
Amount of items: 2
Items: 
Size: 764363 Color: 14
Size: 235351 Color: 19

Bin 3683: 288 of cap free
Amount of items: 2
Items: 
Size: 575827 Color: 4
Size: 423886 Color: 10

Bin 3684: 289 of cap free
Amount of items: 2
Items: 
Size: 754071 Color: 7
Size: 245641 Color: 11

Bin 3685: 289 of cap free
Amount of items: 2
Items: 
Size: 556265 Color: 12
Size: 443447 Color: 10

Bin 3686: 291 of cap free
Amount of items: 2
Items: 
Size: 769402 Color: 4
Size: 230308 Color: 10

Bin 3687: 291 of cap free
Amount of items: 2
Items: 
Size: 611368 Color: 14
Size: 388342 Color: 8

Bin 3688: 291 of cap free
Amount of items: 2
Items: 
Size: 553701 Color: 11
Size: 446009 Color: 0

Bin 3689: 292 of cap free
Amount of items: 2
Items: 
Size: 691114 Color: 5
Size: 308595 Color: 4

Bin 3690: 292 of cap free
Amount of items: 2
Items: 
Size: 728554 Color: 1
Size: 271155 Color: 11

Bin 3691: 292 of cap free
Amount of items: 2
Items: 
Size: 719479 Color: 11
Size: 280230 Color: 4

Bin 3692: 293 of cap free
Amount of items: 3
Items: 
Size: 745459 Color: 14
Size: 140293 Color: 0
Size: 113956 Color: 11

Bin 3693: 293 of cap free
Amount of items: 2
Items: 
Size: 538522 Color: 17
Size: 461186 Color: 9

Bin 3694: 294 of cap free
Amount of items: 2
Items: 
Size: 517164 Color: 17
Size: 482543 Color: 8

Bin 3695: 294 of cap free
Amount of items: 2
Items: 
Size: 527128 Color: 11
Size: 472579 Color: 0

Bin 3696: 294 of cap free
Amount of items: 2
Items: 
Size: 553251 Color: 12
Size: 446456 Color: 7

Bin 3697: 294 of cap free
Amount of items: 2
Items: 
Size: 569870 Color: 6
Size: 429837 Color: 4

Bin 3698: 295 of cap free
Amount of items: 2
Items: 
Size: 536712 Color: 6
Size: 462994 Color: 16

Bin 3699: 296 of cap free
Amount of items: 2
Items: 
Size: 765053 Color: 16
Size: 234652 Color: 12

Bin 3700: 296 of cap free
Amount of items: 2
Items: 
Size: 586368 Color: 6
Size: 413337 Color: 4

Bin 3701: 296 of cap free
Amount of items: 2
Items: 
Size: 605204 Color: 1
Size: 394501 Color: 7

Bin 3702: 296 of cap free
Amount of items: 2
Items: 
Size: 627896 Color: 12
Size: 371809 Color: 14

Bin 3703: 297 of cap free
Amount of items: 2
Items: 
Size: 550931 Color: 19
Size: 448773 Color: 5

Bin 3704: 298 of cap free
Amount of items: 2
Items: 
Size: 720841 Color: 18
Size: 278862 Color: 11

Bin 3705: 299 of cap free
Amount of items: 2
Items: 
Size: 758030 Color: 4
Size: 241672 Color: 13

Bin 3706: 299 of cap free
Amount of items: 2
Items: 
Size: 773579 Color: 7
Size: 226123 Color: 17

Bin 3707: 300 of cap free
Amount of items: 2
Items: 
Size: 741828 Color: 12
Size: 257873 Color: 11

Bin 3708: 302 of cap free
Amount of items: 2
Items: 
Size: 712177 Color: 13
Size: 287522 Color: 8

Bin 3709: 302 of cap free
Amount of items: 2
Items: 
Size: 763677 Color: 17
Size: 236022 Color: 2

Bin 3710: 302 of cap free
Amount of items: 2
Items: 
Size: 686414 Color: 4
Size: 313285 Color: 12

Bin 3711: 302 of cap free
Amount of items: 2
Items: 
Size: 584138 Color: 3
Size: 415561 Color: 6

Bin 3712: 303 of cap free
Amount of items: 2
Items: 
Size: 716217 Color: 16
Size: 283481 Color: 0

Bin 3713: 304 of cap free
Amount of items: 2
Items: 
Size: 779483 Color: 7
Size: 220214 Color: 2

Bin 3714: 305 of cap free
Amount of items: 2
Items: 
Size: 747447 Color: 0
Size: 252249 Color: 4

Bin 3715: 305 of cap free
Amount of items: 2
Items: 
Size: 704383 Color: 16
Size: 295313 Color: 8

Bin 3716: 305 of cap free
Amount of items: 2
Items: 
Size: 600612 Color: 15
Size: 399084 Color: 1

Bin 3717: 305 of cap free
Amount of items: 2
Items: 
Size: 758386 Color: 19
Size: 241310 Color: 12

Bin 3718: 306 of cap free
Amount of items: 2
Items: 
Size: 647080 Color: 3
Size: 352615 Color: 2

Bin 3719: 306 of cap free
Amount of items: 2
Items: 
Size: 600612 Color: 14
Size: 399083 Color: 12

Bin 3720: 306 of cap free
Amount of items: 2
Items: 
Size: 620721 Color: 7
Size: 378974 Color: 4

Bin 3721: 307 of cap free
Amount of items: 2
Items: 
Size: 594467 Color: 13
Size: 405227 Color: 3

Bin 3722: 307 of cap free
Amount of items: 2
Items: 
Size: 570347 Color: 16
Size: 429347 Color: 0

Bin 3723: 307 of cap free
Amount of items: 2
Items: 
Size: 588764 Color: 2
Size: 410930 Color: 19

Bin 3724: 307 of cap free
Amount of items: 2
Items: 
Size: 668967 Color: 1
Size: 330727 Color: 13

Bin 3725: 307 of cap free
Amount of items: 2
Items: 
Size: 544742 Color: 2
Size: 454952 Color: 13

Bin 3726: 308 of cap free
Amount of items: 3
Items: 
Size: 636062 Color: 7
Size: 242037 Color: 1
Size: 121594 Color: 19

Bin 3727: 308 of cap free
Amount of items: 3
Items: 
Size: 624029 Color: 14
Size: 189476 Color: 4
Size: 186188 Color: 11

Bin 3728: 308 of cap free
Amount of items: 2
Items: 
Size: 586916 Color: 1
Size: 412777 Color: 18

Bin 3729: 312 of cap free
Amount of items: 2
Items: 
Size: 665488 Color: 1
Size: 334201 Color: 4

Bin 3730: 312 of cap free
Amount of items: 2
Items: 
Size: 769986 Color: 18
Size: 229703 Color: 19

Bin 3731: 312 of cap free
Amount of items: 2
Items: 
Size: 673585 Color: 18
Size: 326104 Color: 3

Bin 3732: 312 of cap free
Amount of items: 2
Items: 
Size: 554427 Color: 6
Size: 445262 Color: 13

Bin 3733: 313 of cap free
Amount of items: 2
Items: 
Size: 642265 Color: 17
Size: 357423 Color: 12

Bin 3734: 313 of cap free
Amount of items: 2
Items: 
Size: 547622 Color: 10
Size: 452066 Color: 1

Bin 3735: 314 of cap free
Amount of items: 2
Items: 
Size: 654724 Color: 14
Size: 344963 Color: 11

Bin 3736: 314 of cap free
Amount of items: 2
Items: 
Size: 609475 Color: 1
Size: 390212 Color: 2

Bin 3737: 315 of cap free
Amount of items: 2
Items: 
Size: 663516 Color: 8
Size: 336170 Color: 7

Bin 3738: 315 of cap free
Amount of items: 2
Items: 
Size: 785634 Color: 7
Size: 214052 Color: 6

Bin 3739: 316 of cap free
Amount of items: 2
Items: 
Size: 583245 Color: 3
Size: 416440 Color: 8

Bin 3740: 316 of cap free
Amount of items: 2
Items: 
Size: 557009 Color: 17
Size: 442676 Color: 19

Bin 3741: 317 of cap free
Amount of items: 2
Items: 
Size: 728553 Color: 9
Size: 271131 Color: 1

Bin 3742: 317 of cap free
Amount of items: 2
Items: 
Size: 515692 Color: 12
Size: 483992 Color: 0

Bin 3743: 317 of cap free
Amount of items: 2
Items: 
Size: 559825 Color: 11
Size: 439859 Color: 18

Bin 3744: 317 of cap free
Amount of items: 2
Items: 
Size: 771968 Color: 7
Size: 227716 Color: 10

Bin 3745: 318 of cap free
Amount of items: 2
Items: 
Size: 656998 Color: 13
Size: 342685 Color: 5

Bin 3746: 318 of cap free
Amount of items: 2
Items: 
Size: 673240 Color: 9
Size: 326443 Color: 17

Bin 3747: 318 of cap free
Amount of items: 2
Items: 
Size: 582523 Color: 3
Size: 417160 Color: 14

Bin 3748: 318 of cap free
Amount of items: 2
Items: 
Size: 570364 Color: 3
Size: 429319 Color: 17

Bin 3749: 318 of cap free
Amount of items: 2
Items: 
Size: 625665 Color: 11
Size: 374018 Color: 12

Bin 3750: 320 of cap free
Amount of items: 2
Items: 
Size: 632177 Color: 13
Size: 367504 Color: 9

Bin 3751: 320 of cap free
Amount of items: 2
Items: 
Size: 548451 Color: 10
Size: 451230 Color: 3

Bin 3752: 322 of cap free
Amount of items: 3
Items: 
Size: 640630 Color: 16
Size: 198736 Color: 14
Size: 160313 Color: 9

Bin 3753: 322 of cap free
Amount of items: 2
Items: 
Size: 628984 Color: 9
Size: 370695 Color: 1

Bin 3754: 324 of cap free
Amount of items: 2
Items: 
Size: 720477 Color: 13
Size: 279200 Color: 14

Bin 3755: 324 of cap free
Amount of items: 2
Items: 
Size: 667179 Color: 11
Size: 332498 Color: 2

Bin 3756: 324 of cap free
Amount of items: 2
Items: 
Size: 593414 Color: 16
Size: 406263 Color: 10

Bin 3757: 325 of cap free
Amount of items: 2
Items: 
Size: 547611 Color: 10
Size: 452065 Color: 2

Bin 3758: 325 of cap free
Amount of items: 2
Items: 
Size: 556230 Color: 19
Size: 443446 Color: 15

Bin 3759: 326 of cap free
Amount of items: 2
Items: 
Size: 672579 Color: 6
Size: 327096 Color: 4

Bin 3760: 326 of cap free
Amount of items: 2
Items: 
Size: 771523 Color: 1
Size: 228152 Color: 4

Bin 3761: 327 of cap free
Amount of items: 2
Items: 
Size: 798342 Color: 5
Size: 201332 Color: 18

Bin 3762: 328 of cap free
Amount of items: 2
Items: 
Size: 667844 Color: 10
Size: 331829 Color: 19

Bin 3763: 329 of cap free
Amount of items: 2
Items: 
Size: 617605 Color: 13
Size: 382067 Color: 18

Bin 3764: 330 of cap free
Amount of items: 2
Items: 
Size: 699196 Color: 18
Size: 300475 Color: 9

Bin 3765: 330 of cap free
Amount of items: 2
Items: 
Size: 543123 Color: 5
Size: 456548 Color: 13

Bin 3766: 331 of cap free
Amount of items: 2
Items: 
Size: 631633 Color: 7
Size: 368037 Color: 16

Bin 3767: 331 of cap free
Amount of items: 2
Items: 
Size: 531056 Color: 1
Size: 468614 Color: 17

Bin 3768: 331 of cap free
Amount of items: 2
Items: 
Size: 516206 Color: 4
Size: 483464 Color: 1

Bin 3769: 331 of cap free
Amount of items: 2
Items: 
Size: 565900 Color: 15
Size: 433770 Color: 2

Bin 3770: 332 of cap free
Amount of items: 2
Items: 
Size: 672172 Color: 9
Size: 327497 Color: 16

Bin 3771: 333 of cap free
Amount of items: 2
Items: 
Size: 517922 Color: 18
Size: 481746 Color: 12

Bin 3772: 333 of cap free
Amount of items: 2
Items: 
Size: 741807 Color: 18
Size: 257861 Color: 6

Bin 3773: 334 of cap free
Amount of items: 2
Items: 
Size: 529428 Color: 13
Size: 470239 Color: 18

Bin 3774: 335 of cap free
Amount of items: 2
Items: 
Size: 786057 Color: 12
Size: 213609 Color: 4

Bin 3775: 335 of cap free
Amount of items: 2
Items: 
Size: 541634 Color: 14
Size: 458032 Color: 9

Bin 3776: 336 of cap free
Amount of items: 2
Items: 
Size: 681866 Color: 14
Size: 317799 Color: 6

Bin 3777: 337 of cap free
Amount of items: 2
Items: 
Size: 659096 Color: 13
Size: 340568 Color: 6

Bin 3778: 338 of cap free
Amount of items: 2
Items: 
Size: 520157 Color: 6
Size: 479506 Color: 7

Bin 3779: 338 of cap free
Amount of items: 2
Items: 
Size: 581233 Color: 0
Size: 418430 Color: 5

Bin 3780: 338 of cap free
Amount of items: 2
Items: 
Size: 548801 Color: 16
Size: 450862 Color: 3

Bin 3781: 339 of cap free
Amount of items: 2
Items: 
Size: 590068 Color: 3
Size: 409594 Color: 16

Bin 3782: 341 of cap free
Amount of items: 2
Items: 
Size: 738099 Color: 6
Size: 261561 Color: 9

Bin 3783: 341 of cap free
Amount of items: 2
Items: 
Size: 754443 Color: 15
Size: 245217 Color: 17

Bin 3784: 341 of cap free
Amount of items: 2
Items: 
Size: 663377 Color: 3
Size: 336283 Color: 6

Bin 3785: 341 of cap free
Amount of items: 2
Items: 
Size: 734898 Color: 7
Size: 264762 Color: 8

Bin 3786: 341 of cap free
Amount of items: 2
Items: 
Size: 511905 Color: 9
Size: 487755 Color: 4

Bin 3787: 342 of cap free
Amount of items: 2
Items: 
Size: 676537 Color: 19
Size: 323122 Color: 16

Bin 3788: 342 of cap free
Amount of items: 2
Items: 
Size: 513782 Color: 19
Size: 485877 Color: 13

Bin 3789: 343 of cap free
Amount of items: 2
Items: 
Size: 633909 Color: 2
Size: 365749 Color: 11

Bin 3790: 344 of cap free
Amount of items: 2
Items: 
Size: 783908 Color: 10
Size: 215749 Color: 17

Bin 3791: 344 of cap free
Amount of items: 2
Items: 
Size: 578357 Color: 2
Size: 421300 Color: 3

Bin 3792: 345 of cap free
Amount of items: 2
Items: 
Size: 714765 Color: 19
Size: 284891 Color: 13

Bin 3793: 346 of cap free
Amount of items: 2
Items: 
Size: 529428 Color: 13
Size: 470227 Color: 15

Bin 3794: 346 of cap free
Amount of items: 2
Items: 
Size: 598100 Color: 18
Size: 401555 Color: 9

Bin 3795: 347 of cap free
Amount of items: 2
Items: 
Size: 642943 Color: 5
Size: 356711 Color: 6

Bin 3796: 347 of cap free
Amount of items: 2
Items: 
Size: 725916 Color: 6
Size: 273738 Color: 10

Bin 3797: 347 of cap free
Amount of items: 2
Items: 
Size: 643739 Color: 5
Size: 355915 Color: 16

Bin 3798: 349 of cap free
Amount of items: 2
Items: 
Size: 731647 Color: 19
Size: 268005 Color: 11

Bin 3799: 349 of cap free
Amount of items: 2
Items: 
Size: 722036 Color: 16
Size: 277616 Color: 1

Bin 3800: 349 of cap free
Amount of items: 2
Items: 
Size: 572187 Color: 11
Size: 427465 Color: 8

Bin 3801: 351 of cap free
Amount of items: 2
Items: 
Size: 575273 Color: 4
Size: 424377 Color: 13

Bin 3802: 351 of cap free
Amount of items: 2
Items: 
Size: 610427 Color: 13
Size: 389223 Color: 16

Bin 3803: 352 of cap free
Amount of items: 2
Items: 
Size: 651730 Color: 12
Size: 347919 Color: 16

Bin 3804: 355 of cap free
Amount of items: 2
Items: 
Size: 575776 Color: 1
Size: 423870 Color: 5

Bin 3805: 356 of cap free
Amount of items: 2
Items: 
Size: 611754 Color: 6
Size: 387891 Color: 11

Bin 3806: 357 of cap free
Amount of items: 2
Items: 
Size: 511540 Color: 11
Size: 488104 Color: 15

Bin 3807: 358 of cap free
Amount of items: 2
Items: 
Size: 707075 Color: 3
Size: 292568 Color: 9

Bin 3808: 358 of cap free
Amount of items: 2
Items: 
Size: 627506 Color: 3
Size: 372137 Color: 11

Bin 3809: 358 of cap free
Amount of items: 2
Items: 
Size: 512732 Color: 4
Size: 486911 Color: 6

Bin 3810: 358 of cap free
Amount of items: 2
Items: 
Size: 623368 Color: 19
Size: 376275 Color: 5

Bin 3811: 359 of cap free
Amount of items: 2
Items: 
Size: 670945 Color: 2
Size: 328697 Color: 15

Bin 3812: 360 of cap free
Amount of items: 2
Items: 
Size: 695229 Color: 15
Size: 304412 Color: 18

Bin 3813: 361 of cap free
Amount of items: 3
Items: 
Size: 637453 Color: 2
Size: 181439 Color: 15
Size: 180748 Color: 2

Bin 3814: 362 of cap free
Amount of items: 2
Items: 
Size: 632627 Color: 0
Size: 367012 Color: 5

Bin 3815: 362 of cap free
Amount of items: 2
Items: 
Size: 644523 Color: 10
Size: 355116 Color: 17

Bin 3816: 363 of cap free
Amount of items: 2
Items: 
Size: 793996 Color: 13
Size: 205642 Color: 14

Bin 3817: 363 of cap free
Amount of items: 2
Items: 
Size: 676542 Color: 1
Size: 323096 Color: 11

Bin 3818: 363 of cap free
Amount of items: 2
Items: 
Size: 602951 Color: 10
Size: 396687 Color: 16

Bin 3819: 364 of cap free
Amount of items: 2
Items: 
Size: 569912 Color: 8
Size: 429725 Color: 13

Bin 3820: 365 of cap free
Amount of items: 2
Items: 
Size: 566834 Color: 1
Size: 432802 Color: 4

Bin 3821: 366 of cap free
Amount of items: 2
Items: 
Size: 668943 Color: 4
Size: 330692 Color: 13

Bin 3822: 366 of cap free
Amount of items: 2
Items: 
Size: 629372 Color: 17
Size: 370263 Color: 12

Bin 3823: 366 of cap free
Amount of items: 2
Items: 
Size: 734003 Color: 3
Size: 265632 Color: 10

Bin 3824: 370 of cap free
Amount of items: 2
Items: 
Size: 758011 Color: 5
Size: 241620 Color: 1

Bin 3825: 370 of cap free
Amount of items: 2
Items: 
Size: 593385 Color: 19
Size: 406246 Color: 9

Bin 3826: 371 of cap free
Amount of items: 2
Items: 
Size: 694014 Color: 8
Size: 305616 Color: 7

Bin 3827: 372 of cap free
Amount of items: 2
Items: 
Size: 607403 Color: 13
Size: 392226 Color: 16

Bin 3828: 372 of cap free
Amount of items: 2
Items: 
Size: 736286 Color: 6
Size: 263343 Color: 2

Bin 3829: 372 of cap free
Amount of items: 2
Items: 
Size: 798565 Color: 16
Size: 201064 Color: 11

Bin 3830: 375 of cap free
Amount of items: 2
Items: 
Size: 721282 Color: 1
Size: 278344 Color: 0

Bin 3831: 375 of cap free
Amount of items: 2
Items: 
Size: 787502 Color: 18
Size: 212124 Color: 5

Bin 3832: 375 of cap free
Amount of items: 2
Items: 
Size: 535225 Color: 18
Size: 464401 Color: 3

Bin 3833: 376 of cap free
Amount of items: 2
Items: 
Size: 644892 Color: 16
Size: 354733 Color: 12

Bin 3834: 377 of cap free
Amount of items: 2
Items: 
Size: 736927 Color: 12
Size: 262697 Color: 7

Bin 3835: 377 of cap free
Amount of items: 2
Items: 
Size: 631625 Color: 4
Size: 367999 Color: 6

Bin 3836: 377 of cap free
Amount of items: 2
Items: 
Size: 579700 Color: 2
Size: 419924 Color: 1

Bin 3837: 378 of cap free
Amount of items: 3
Items: 
Size: 356727 Color: 19
Size: 339507 Color: 6
Size: 303389 Color: 7

Bin 3838: 378 of cap free
Amount of items: 2
Items: 
Size: 728506 Color: 18
Size: 271117 Color: 16

Bin 3839: 379 of cap free
Amount of items: 2
Items: 
Size: 538479 Color: 3
Size: 461143 Color: 0

Bin 3840: 379 of cap free
Amount of items: 2
Items: 
Size: 555739 Color: 9
Size: 443883 Color: 13

Bin 3841: 379 of cap free
Amount of items: 2
Items: 
Size: 741270 Color: 12
Size: 258352 Color: 17

Bin 3842: 381 of cap free
Amount of items: 2
Items: 
Size: 582400 Color: 5
Size: 417220 Color: 9

Bin 3843: 381 of cap free
Amount of items: 2
Items: 
Size: 613445 Color: 12
Size: 386175 Color: 17

Bin 3844: 382 of cap free
Amount of items: 2
Items: 
Size: 614686 Color: 13
Size: 384933 Color: 2

Bin 3845: 383 of cap free
Amount of items: 2
Items: 
Size: 625655 Color: 1
Size: 373963 Color: 2

Bin 3846: 384 of cap free
Amount of items: 2
Items: 
Size: 744237 Color: 13
Size: 255380 Color: 18

Bin 3847: 384 of cap free
Amount of items: 2
Items: 
Size: 716685 Color: 12
Size: 282932 Color: 5

Bin 3848: 384 of cap free
Amount of items: 2
Items: 
Size: 583758 Color: 15
Size: 415859 Color: 3

Bin 3849: 384 of cap free
Amount of items: 2
Items: 
Size: 750458 Color: 12
Size: 249159 Color: 11

Bin 3850: 387 of cap free
Amount of items: 2
Items: 
Size: 655504 Color: 4
Size: 344110 Color: 10

Bin 3851: 387 of cap free
Amount of items: 3
Items: 
Size: 528479 Color: 17
Size: 323007 Color: 17
Size: 148128 Color: 5

Bin 3852: 388 of cap free
Amount of items: 2
Items: 
Size: 691055 Color: 10
Size: 308558 Color: 14

Bin 3853: 388 of cap free
Amount of items: 2
Items: 
Size: 752429 Color: 15
Size: 247184 Color: 17

Bin 3854: 388 of cap free
Amount of items: 2
Items: 
Size: 567317 Color: 14
Size: 432296 Color: 17

Bin 3855: 392 of cap free
Amount of items: 2
Items: 
Size: 524825 Color: 2
Size: 474784 Color: 1

Bin 3856: 393 of cap free
Amount of items: 2
Items: 
Size: 512701 Color: 14
Size: 486907 Color: 6

Bin 3857: 394 of cap free
Amount of items: 2
Items: 
Size: 607391 Color: 19
Size: 392216 Color: 13

Bin 3858: 394 of cap free
Amount of items: 2
Items: 
Size: 741265 Color: 7
Size: 258342 Color: 10

Bin 3859: 395 of cap free
Amount of items: 2
Items: 
Size: 690218 Color: 15
Size: 309388 Color: 2

Bin 3860: 395 of cap free
Amount of items: 2
Items: 
Size: 766882 Color: 1
Size: 232724 Color: 12

Bin 3861: 396 of cap free
Amount of items: 2
Items: 
Size: 716657 Color: 0
Size: 282948 Color: 12

Bin 3862: 396 of cap free
Amount of items: 2
Items: 
Size: 712067 Color: 9
Size: 287538 Color: 18

Bin 3863: 396 of cap free
Amount of items: 2
Items: 
Size: 500186 Color: 4
Size: 499419 Color: 13

Bin 3864: 396 of cap free
Amount of items: 2
Items: 
Size: 564950 Color: 12
Size: 434655 Color: 8

Bin 3865: 397 of cap free
Amount of items: 2
Items: 
Size: 770767 Color: 18
Size: 228837 Color: 11

Bin 3866: 398 of cap free
Amount of items: 2
Items: 
Size: 775522 Color: 8
Size: 224081 Color: 15

Bin 3867: 398 of cap free
Amount of items: 2
Items: 
Size: 519377 Color: 13
Size: 480226 Color: 14

Bin 3868: 398 of cap free
Amount of items: 2
Items: 
Size: 707035 Color: 0
Size: 292568 Color: 2

Bin 3869: 399 of cap free
Amount of items: 2
Items: 
Size: 511905 Color: 15
Size: 487697 Color: 14

Bin 3870: 399 of cap free
Amount of items: 2
Items: 
Size: 535210 Color: 17
Size: 464392 Color: 12

Bin 3871: 399 of cap free
Amount of items: 2
Items: 
Size: 646619 Color: 15
Size: 352983 Color: 16

Bin 3872: 400 of cap free
Amount of items: 2
Items: 
Size: 602929 Color: 11
Size: 396672 Color: 19

Bin 3873: 400 of cap free
Amount of items: 2
Items: 
Size: 726435 Color: 16
Size: 273166 Color: 4

Bin 3874: 402 of cap free
Amount of items: 3
Items: 
Size: 352550 Color: 15
Size: 344068 Color: 1
Size: 302981 Color: 19

Bin 3875: 402 of cap free
Amount of items: 2
Items: 
Size: 720837 Color: 3
Size: 278762 Color: 16

Bin 3876: 404 of cap free
Amount of items: 2
Items: 
Size: 611326 Color: 10
Size: 388271 Color: 15

Bin 3877: 405 of cap free
Amount of items: 2
Items: 
Size: 632157 Color: 6
Size: 367439 Color: 7

Bin 3878: 405 of cap free
Amount of items: 2
Items: 
Size: 575327 Color: 5
Size: 424269 Color: 3

Bin 3879: 406 of cap free
Amount of items: 2
Items: 
Size: 670845 Color: 0
Size: 328750 Color: 13

Bin 3880: 406 of cap free
Amount of items: 2
Items: 
Size: 673157 Color: 16
Size: 326438 Color: 2

Bin 3881: 408 of cap free
Amount of items: 2
Items: 
Size: 764356 Color: 16
Size: 235237 Color: 10

Bin 3882: 409 of cap free
Amount of items: 2
Items: 
Size: 759075 Color: 1
Size: 240517 Color: 4

Bin 3883: 412 of cap free
Amount of items: 2
Items: 
Size: 791039 Color: 11
Size: 208550 Color: 19

Bin 3884: 413 of cap free
Amount of items: 2
Items: 
Size: 672593 Color: 6
Size: 326995 Color: 11

Bin 3885: 413 of cap free
Amount of items: 2
Items: 
Size: 525584 Color: 15
Size: 474004 Color: 2

Bin 3886: 414 of cap free
Amount of items: 2
Items: 
Size: 700263 Color: 16
Size: 299324 Color: 15

Bin 3887: 415 of cap free
Amount of items: 2
Items: 
Size: 534534 Color: 12
Size: 465052 Color: 14

Bin 3888: 416 of cap free
Amount of items: 2
Items: 
Size: 775521 Color: 3
Size: 224064 Color: 13

Bin 3889: 418 of cap free
Amount of items: 2
Items: 
Size: 646600 Color: 1
Size: 352983 Color: 19

Bin 3890: 419 of cap free
Amount of items: 2
Items: 
Size: 767340 Color: 9
Size: 232242 Color: 14

Bin 3891: 420 of cap free
Amount of items: 2
Items: 
Size: 679233 Color: 10
Size: 320348 Color: 12

Bin 3892: 422 of cap free
Amount of items: 2
Items: 
Size: 664250 Color: 2
Size: 335329 Color: 7

Bin 3893: 422 of cap free
Amount of items: 2
Items: 
Size: 535188 Color: 12
Size: 464391 Color: 2

Bin 3894: 423 of cap free
Amount of items: 2
Items: 
Size: 676485 Color: 13
Size: 323093 Color: 10

Bin 3895: 423 of cap free
Amount of items: 2
Items: 
Size: 645462 Color: 9
Size: 354116 Color: 18

Bin 3896: 423 of cap free
Amount of items: 2
Items: 
Size: 603789 Color: 2
Size: 395789 Color: 17

Bin 3897: 424 of cap free
Amount of items: 2
Items: 
Size: 505870 Color: 0
Size: 493707 Color: 1

Bin 3898: 426 of cap free
Amount of items: 2
Items: 
Size: 656936 Color: 8
Size: 342639 Color: 14

Bin 3899: 426 of cap free
Amount of items: 2
Items: 
Size: 745892 Color: 17
Size: 253683 Color: 7

Bin 3900: 427 of cap free
Amount of items: 2
Items: 
Size: 616327 Color: 12
Size: 383247 Color: 4

Bin 3901: 430 of cap free
Amount of items: 2
Items: 
Size: 674758 Color: 15
Size: 324813 Color: 10

Bin 3902: 433 of cap free
Amount of items: 2
Items: 
Size: 769985 Color: 2
Size: 229583 Color: 9

Bin 3903: 434 of cap free
Amount of items: 2
Items: 
Size: 652417 Color: 0
Size: 347150 Color: 14

Bin 3904: 435 of cap free
Amount of items: 2
Items: 
Size: 618073 Color: 5
Size: 381493 Color: 14

Bin 3905: 437 of cap free
Amount of items: 2
Items: 
Size: 602929 Color: 10
Size: 396635 Color: 19

Bin 3906: 437 of cap free
Amount of items: 2
Items: 
Size: 500199 Color: 16
Size: 499365 Color: 1

Bin 3907: 437 of cap free
Amount of items: 2
Items: 
Size: 546616 Color: 13
Size: 452948 Color: 7

Bin 3908: 437 of cap free
Amount of items: 2
Items: 
Size: 571349 Color: 15
Size: 428215 Color: 16

Bin 3909: 439 of cap free
Amount of items: 2
Items: 
Size: 551504 Color: 2
Size: 448058 Color: 8

Bin 3910: 440 of cap free
Amount of items: 2
Items: 
Size: 666080 Color: 4
Size: 333481 Color: 16

Bin 3911: 441 of cap free
Amount of items: 3
Items: 
Size: 774863 Color: 14
Size: 124366 Color: 7
Size: 100331 Color: 5

Bin 3912: 441 of cap free
Amount of items: 2
Items: 
Size: 707011 Color: 16
Size: 292549 Color: 13

Bin 3913: 442 of cap free
Amount of items: 2
Items: 
Size: 509291 Color: 7
Size: 490268 Color: 16

Bin 3914: 446 of cap free
Amount of items: 2
Items: 
Size: 588698 Color: 10
Size: 410857 Color: 3

Bin 3915: 446 of cap free
Amount of items: 2
Items: 
Size: 572968 Color: 4
Size: 426587 Color: 19

Bin 3916: 447 of cap free
Amount of items: 2
Items: 
Size: 704294 Color: 11
Size: 295260 Color: 1

Bin 3917: 448 of cap free
Amount of items: 2
Items: 
Size: 540187 Color: 19
Size: 459366 Color: 9

Bin 3918: 449 of cap free
Amount of items: 2
Items: 
Size: 771497 Color: 3
Size: 228055 Color: 1

Bin 3919: 450 of cap free
Amount of items: 2
Items: 
Size: 782399 Color: 12
Size: 217152 Color: 14

Bin 3920: 450 of cap free
Amount of items: 2
Items: 
Size: 524783 Color: 15
Size: 474768 Color: 17

Bin 3921: 453 of cap free
Amount of items: 3
Items: 
Size: 787089 Color: 12
Size: 107021 Color: 14
Size: 105438 Color: 15

Bin 3922: 454 of cap free
Amount of items: 2
Items: 
Size: 662898 Color: 7
Size: 336649 Color: 6

Bin 3923: 454 of cap free
Amount of items: 2
Items: 
Size: 648067 Color: 17
Size: 351480 Color: 10

Bin 3924: 456 of cap free
Amount of items: 2
Items: 
Size: 797022 Color: 8
Size: 202523 Color: 10

Bin 3925: 458 of cap free
Amount of items: 2
Items: 
Size: 500192 Color: 8
Size: 499351 Color: 15

Bin 3926: 458 of cap free
Amount of items: 2
Items: 
Size: 505863 Color: 12
Size: 493680 Color: 13

Bin 3927: 460 of cap free
Amount of items: 2
Items: 
Size: 623339 Color: 11
Size: 376202 Color: 17

Bin 3928: 463 of cap free
Amount of items: 2
Items: 
Size: 651768 Color: 11
Size: 347770 Color: 15

Bin 3929: 463 of cap free
Amount of items: 2
Items: 
Size: 545895 Color: 2
Size: 453643 Color: 11

Bin 3930: 463 of cap free
Amount of items: 2
Items: 
Size: 600458 Color: 6
Size: 399080 Color: 12

Bin 3931: 464 of cap free
Amount of items: 2
Items: 
Size: 556989 Color: 15
Size: 442548 Color: 14

Bin 3932: 464 of cap free
Amount of items: 2
Items: 
Size: 582400 Color: 11
Size: 417137 Color: 5

Bin 3933: 465 of cap free
Amount of items: 2
Items: 
Size: 738774 Color: 1
Size: 260762 Color: 6

Bin 3934: 465 of cap free
Amount of items: 2
Items: 
Size: 607928 Color: 14
Size: 391608 Color: 4

Bin 3935: 466 of cap free
Amount of items: 2
Items: 
Size: 699155 Color: 4
Size: 300380 Color: 12

Bin 3936: 466 of cap free
Amount of items: 2
Items: 
Size: 548443 Color: 1
Size: 451092 Color: 15

Bin 3937: 470 of cap free
Amount of items: 2
Items: 
Size: 776854 Color: 16
Size: 222677 Color: 2

Bin 3938: 471 of cap free
Amount of items: 2
Items: 
Size: 617563 Color: 12
Size: 381967 Color: 6

Bin 3939: 471 of cap free
Amount of items: 2
Items: 
Size: 576740 Color: 6
Size: 422790 Color: 12

Bin 3940: 475 of cap free
Amount of items: 2
Items: 
Size: 648058 Color: 12
Size: 351468 Color: 5

Bin 3941: 477 of cap free
Amount of items: 2
Items: 
Size: 789269 Color: 6
Size: 210255 Color: 16

Bin 3942: 477 of cap free
Amount of items: 2
Items: 
Size: 613399 Color: 15
Size: 386125 Color: 1

Bin 3943: 479 of cap free
Amount of items: 2
Items: 
Size: 603776 Color: 15
Size: 395746 Color: 16

Bin 3944: 480 of cap free
Amount of items: 2
Items: 
Size: 588592 Color: 2
Size: 410929 Color: 9

Bin 3945: 480 of cap free
Amount of items: 2
Items: 
Size: 777456 Color: 4
Size: 222065 Color: 19

Bin 3946: 483 of cap free
Amount of items: 2
Items: 
Size: 698192 Color: 15
Size: 301326 Color: 12

Bin 3947: 484 of cap free
Amount of items: 2
Items: 
Size: 554362 Color: 1
Size: 445155 Color: 12

Bin 3948: 485 of cap free
Amount of items: 2
Items: 
Size: 722816 Color: 4
Size: 276700 Color: 3

Bin 3949: 486 of cap free
Amount of items: 2
Items: 
Size: 606026 Color: 0
Size: 393489 Color: 1

Bin 3950: 492 of cap free
Amount of items: 2
Items: 
Size: 761225 Color: 19
Size: 238284 Color: 1

Bin 3951: 493 of cap free
Amount of items: 2
Items: 
Size: 718821 Color: 11
Size: 280687 Color: 12

Bin 3952: 493 of cap free
Amount of items: 2
Items: 
Size: 743653 Color: 10
Size: 255855 Color: 15

Bin 3953: 493 of cap free
Amount of items: 2
Items: 
Size: 545884 Color: 12
Size: 453624 Color: 19

Bin 3954: 494 of cap free
Amount of items: 2
Items: 
Size: 550044 Color: 7
Size: 449463 Color: 18

Bin 3955: 496 of cap free
Amount of items: 2
Items: 
Size: 653979 Color: 2
Size: 345526 Color: 19

Bin 3956: 498 of cap free
Amount of items: 2
Items: 
Size: 619695 Color: 8
Size: 379808 Color: 11

Bin 3957: 498 of cap free
Amount of items: 2
Items: 
Size: 501463 Color: 3
Size: 498040 Color: 1

Bin 3958: 500 of cap free
Amount of items: 2
Items: 
Size: 529417 Color: 2
Size: 470084 Color: 14

Bin 3959: 502 of cap free
Amount of items: 2
Items: 
Size: 712060 Color: 9
Size: 287439 Color: 13

Bin 3960: 502 of cap free
Amount of items: 2
Items: 
Size: 693257 Color: 9
Size: 306242 Color: 17

Bin 3961: 503 of cap free
Amount of items: 2
Items: 
Size: 709588 Color: 7
Size: 289910 Color: 16

Bin 3962: 503 of cap free
Amount of items: 2
Items: 
Size: 677018 Color: 14
Size: 322480 Color: 10

Bin 3963: 503 of cap free
Amount of items: 2
Items: 
Size: 617535 Color: 5
Size: 381963 Color: 2

Bin 3964: 504 of cap free
Amount of items: 2
Items: 
Size: 662849 Color: 8
Size: 336648 Color: 5

Bin 3965: 507 of cap free
Amount of items: 2
Items: 
Size: 572959 Color: 14
Size: 426535 Color: 2

Bin 3966: 509 of cap free
Amount of items: 3
Items: 
Size: 784700 Color: 15
Size: 107418 Color: 3
Size: 107374 Color: 18

Bin 3967: 509 of cap free
Amount of items: 2
Items: 
Size: 663358 Color: 8
Size: 336134 Color: 9

Bin 3968: 510 of cap free
Amount of items: 2
Items: 
Size: 727099 Color: 16
Size: 272392 Color: 13

Bin 3969: 510 of cap free
Amount of items: 2
Items: 
Size: 654749 Color: 11
Size: 344742 Color: 9

Bin 3970: 511 of cap free
Amount of items: 2
Items: 
Size: 743651 Color: 4
Size: 255839 Color: 12

Bin 3971: 512 of cap free
Amount of items: 2
Items: 
Size: 676454 Color: 3
Size: 323035 Color: 6

Bin 3972: 515 of cap free
Amount of items: 2
Items: 
Size: 562957 Color: 9
Size: 436529 Color: 5

Bin 3973: 517 of cap free
Amount of items: 2
Items: 
Size: 600446 Color: 11
Size: 399038 Color: 18

Bin 3974: 518 of cap free
Amount of items: 2
Items: 
Size: 651003 Color: 1
Size: 348480 Color: 0

Bin 3975: 519 of cap free
Amount of items: 2
Items: 
Size: 525585 Color: 2
Size: 473897 Color: 3

Bin 3976: 519 of cap free
Amount of items: 2
Items: 
Size: 534526 Color: 2
Size: 464956 Color: 15

Bin 3977: 520 of cap free
Amount of items: 2
Items: 
Size: 784633 Color: 0
Size: 214848 Color: 8

Bin 3978: 521 of cap free
Amount of items: 2
Items: 
Size: 748481 Color: 1
Size: 250999 Color: 10

Bin 3979: 524 of cap free
Amount of items: 2
Items: 
Size: 747425 Color: 5
Size: 252052 Color: 17

Bin 3980: 524 of cap free
Amount of items: 2
Items: 
Size: 620689 Color: 1
Size: 378788 Color: 10

Bin 3981: 525 of cap free
Amount of items: 2
Items: 
Size: 752376 Color: 17
Size: 247100 Color: 7

Bin 3982: 525 of cap free
Amount of items: 2
Items: 
Size: 576726 Color: 19
Size: 422750 Color: 14

Bin 3983: 526 of cap free
Amount of items: 2
Items: 
Size: 724971 Color: 13
Size: 274504 Color: 8

Bin 3984: 526 of cap free
Amount of items: 2
Items: 
Size: 580446 Color: 13
Size: 419029 Color: 9

Bin 3985: 527 of cap free
Amount of items: 3
Items: 
Size: 653885 Color: 13
Size: 213366 Color: 18
Size: 132223 Color: 19

Bin 3986: 528 of cap free
Amount of items: 2
Items: 
Size: 663356 Color: 1
Size: 336117 Color: 4

Bin 3987: 528 of cap free
Amount of items: 2
Items: 
Size: 516952 Color: 12
Size: 482521 Color: 5

Bin 3988: 528 of cap free
Amount of items: 2
Items: 
Size: 571296 Color: 6
Size: 428177 Color: 17

Bin 3989: 529 of cap free
Amount of items: 2
Items: 
Size: 707873 Color: 5
Size: 291599 Color: 13

Bin 3990: 529 of cap free
Amount of items: 2
Items: 
Size: 586354 Color: 9
Size: 413118 Color: 4

Bin 3991: 530 of cap free
Amount of items: 2
Items: 
Size: 633951 Color: 11
Size: 365520 Color: 0

Bin 3992: 530 of cap free
Amount of items: 2
Items: 
Size: 538363 Color: 16
Size: 461108 Color: 17

Bin 3993: 530 of cap free
Amount of items: 2
Items: 
Size: 561700 Color: 9
Size: 437771 Color: 7

Bin 3994: 530 of cap free
Amount of items: 2
Items: 
Size: 529504 Color: 6
Size: 469967 Color: 7

Bin 3995: 531 of cap free
Amount of items: 2
Items: 
Size: 739736 Color: 1
Size: 259734 Color: 6

Bin 3996: 532 of cap free
Amount of items: 2
Items: 
Size: 565857 Color: 0
Size: 433612 Color: 11

Bin 3997: 533 of cap free
Amount of items: 3
Items: 
Size: 683272 Color: 7
Size: 198811 Color: 1
Size: 117385 Color: 18

Bin 3998: 535 of cap free
Amount of items: 2
Items: 
Size: 713066 Color: 10
Size: 286400 Color: 9

Bin 3999: 535 of cap free
Amount of items: 2
Items: 
Size: 513667 Color: 12
Size: 485799 Color: 7

Bin 4000: 536 of cap free
Amount of items: 2
Items: 
Size: 615459 Color: 3
Size: 384006 Color: 6

Bin 4001: 537 of cap free
Amount of items: 2
Items: 
Size: 735241 Color: 2
Size: 264223 Color: 0

Bin 4002: 537 of cap free
Amount of items: 2
Items: 
Size: 502316 Color: 3
Size: 497148 Color: 17

Bin 4003: 538 of cap free
Amount of items: 2
Items: 
Size: 751640 Color: 13
Size: 247823 Color: 10

Bin 4004: 539 of cap free
Amount of items: 2
Items: 
Size: 680677 Color: 8
Size: 318785 Color: 1

Bin 4005: 540 of cap free
Amount of items: 2
Items: 
Size: 594306 Color: 15
Size: 405155 Color: 6

Bin 4006: 540 of cap free
Amount of items: 2
Items: 
Size: 504376 Color: 19
Size: 495085 Color: 3

Bin 4007: 541 of cap free
Amount of items: 2
Items: 
Size: 721931 Color: 6
Size: 277529 Color: 11

Bin 4008: 542 of cap free
Amount of items: 2
Items: 
Size: 764881 Color: 18
Size: 234578 Color: 19

Bin 4009: 542 of cap free
Amount of items: 2
Items: 
Size: 509866 Color: 9
Size: 489593 Color: 3

Bin 4010: 543 of cap free
Amount of items: 2
Items: 
Size: 637909 Color: 5
Size: 361549 Color: 2

Bin 4011: 543 of cap free
Amount of items: 2
Items: 
Size: 653014 Color: 8
Size: 346444 Color: 0

Bin 4012: 545 of cap free
Amount of items: 2
Items: 
Size: 618768 Color: 8
Size: 380688 Color: 7

Bin 4013: 547 of cap free
Amount of items: 2
Items: 
Size: 710995 Color: 11
Size: 288459 Color: 6

Bin 4014: 547 of cap free
Amount of items: 2
Items: 
Size: 576709 Color: 5
Size: 422745 Color: 19

Bin 4015: 549 of cap free
Amount of items: 2
Items: 
Size: 710840 Color: 7
Size: 288612 Color: 15

Bin 4016: 554 of cap free
Amount of items: 3
Items: 
Size: 344521 Color: 13
Size: 340253 Color: 7
Size: 314673 Color: 0

Bin 4017: 554 of cap free
Amount of items: 2
Items: 
Size: 605178 Color: 15
Size: 394269 Color: 0

Bin 4018: 555 of cap free
Amount of items: 2
Items: 
Size: 736810 Color: 6
Size: 262636 Color: 1

Bin 4019: 555 of cap free
Amount of items: 2
Items: 
Size: 683239 Color: 18
Size: 316207 Color: 10

Bin 4020: 556 of cap free
Amount of items: 2
Items: 
Size: 648723 Color: 11
Size: 350722 Color: 3

Bin 4021: 556 of cap free
Amount of items: 2
Items: 
Size: 516940 Color: 15
Size: 482505 Color: 8

Bin 4022: 557 of cap free
Amount of items: 2
Items: 
Size: 758915 Color: 18
Size: 240529 Color: 5

Bin 4023: 557 of cap free
Amount of items: 2
Items: 
Size: 730571 Color: 2
Size: 268873 Color: 6

Bin 4024: 560 of cap free
Amount of items: 2
Items: 
Size: 563956 Color: 7
Size: 435485 Color: 11

Bin 4025: 561 of cap free
Amount of items: 2
Items: 
Size: 697221 Color: 8
Size: 302219 Color: 7

Bin 4026: 562 of cap free
Amount of items: 2
Items: 
Size: 684773 Color: 13
Size: 314666 Color: 12

Bin 4027: 563 of cap free
Amount of items: 2
Items: 
Size: 769804 Color: 12
Size: 229634 Color: 11

Bin 4028: 565 of cap free
Amount of items: 2
Items: 
Size: 537358 Color: 0
Size: 462078 Color: 3

Bin 4029: 567 of cap free
Amount of items: 2
Items: 
Size: 778027 Color: 4
Size: 221407 Color: 2

Bin 4030: 569 of cap free
Amount of items: 2
Items: 
Size: 662066 Color: 17
Size: 337366 Color: 7

Bin 4031: 571 of cap free
Amount of items: 2
Items: 
Size: 731639 Color: 9
Size: 267791 Color: 7

Bin 4032: 572 of cap free
Amount of items: 2
Items: 
Size: 781338 Color: 18
Size: 218091 Color: 0

Bin 4033: 572 of cap free
Amount of items: 2
Items: 
Size: 522095 Color: 2
Size: 477334 Color: 14

Bin 4034: 575 of cap free
Amount of items: 2
Items: 
Size: 661462 Color: 10
Size: 337964 Color: 12

Bin 4035: 575 of cap free
Amount of items: 2
Items: 
Size: 610370 Color: 17
Size: 389056 Color: 7

Bin 4036: 575 of cap free
Amount of items: 2
Items: 
Size: 596614 Color: 18
Size: 402812 Color: 4

Bin 4037: 577 of cap free
Amount of items: 2
Items: 
Size: 522950 Color: 19
Size: 476474 Color: 15

Bin 4038: 578 of cap free
Amount of items: 2
Items: 
Size: 500149 Color: 18
Size: 499274 Color: 12

Bin 4039: 582 of cap free
Amount of items: 2
Items: 
Size: 515593 Color: 16
Size: 483826 Color: 7

Bin 4040: 583 of cap free
Amount of items: 2
Items: 
Size: 654688 Color: 14
Size: 344730 Color: 17

Bin 4041: 584 of cap free
Amount of items: 2
Items: 
Size: 674756 Color: 19
Size: 324661 Color: 3

Bin 4042: 585 of cap free
Amount of items: 3
Items: 
Size: 742567 Color: 14
Size: 129458 Color: 7
Size: 127391 Color: 1

Bin 4043: 586 of cap free
Amount of items: 2
Items: 
Size: 579572 Color: 10
Size: 419843 Color: 9

Bin 4044: 588 of cap free
Amount of items: 2
Items: 
Size: 636161 Color: 16
Size: 363252 Color: 7

Bin 4045: 589 of cap free
Amount of items: 4
Items: 
Size: 607377 Color: 8
Size: 146648 Color: 13
Size: 133684 Color: 15
Size: 111703 Color: 4

Bin 4046: 590 of cap free
Amount of items: 2
Items: 
Size: 516923 Color: 4
Size: 482488 Color: 18

Bin 4047: 591 of cap free
Amount of items: 2
Items: 
Size: 552158 Color: 2
Size: 447252 Color: 4

Bin 4048: 592 of cap free
Amount of items: 2
Items: 
Size: 786469 Color: 9
Size: 212940 Color: 2

Bin 4049: 595 of cap free
Amount of items: 2
Items: 
Size: 679182 Color: 7
Size: 320224 Color: 9

Bin 4050: 595 of cap free
Amount of items: 2
Items: 
Size: 722752 Color: 18
Size: 276654 Color: 12

Bin 4051: 595 of cap free
Amount of items: 2
Items: 
Size: 574613 Color: 7
Size: 424793 Color: 14

Bin 4052: 596 of cap free
Amount of items: 3
Items: 
Size: 655676 Color: 4
Size: 198602 Color: 10
Size: 145127 Color: 8

Bin 4053: 597 of cap free
Amount of items: 2
Items: 
Size: 595818 Color: 2
Size: 403586 Color: 12

Bin 4054: 597 of cap free
Amount of items: 2
Items: 
Size: 623864 Color: 13
Size: 375540 Color: 9

Bin 4055: 597 of cap free
Amount of items: 2
Items: 
Size: 553668 Color: 8
Size: 445736 Color: 13

Bin 4056: 613 of cap free
Amount of items: 2
Items: 
Size: 608409 Color: 6
Size: 390979 Color: 2

Bin 4057: 614 of cap free
Amount of items: 2
Items: 
Size: 690110 Color: 4
Size: 309277 Color: 11

Bin 4058: 618 of cap free
Amount of items: 2
Items: 
Size: 587357 Color: 6
Size: 412026 Color: 7

Bin 4059: 619 of cap free
Amount of items: 2
Items: 
Size: 753221 Color: 12
Size: 246161 Color: 9

Bin 4060: 619 of cap free
Amount of items: 2
Items: 
Size: 653935 Color: 18
Size: 345447 Color: 17

Bin 4061: 620 of cap free
Amount of items: 2
Items: 
Size: 580467 Color: 9
Size: 418914 Color: 13

Bin 4062: 620 of cap free
Amount of items: 2
Items: 
Size: 584219 Color: 6
Size: 415162 Color: 17

Bin 4063: 621 of cap free
Amount of items: 2
Items: 
Size: 702759 Color: 18
Size: 296621 Color: 16

Bin 4064: 622 of cap free
Amount of items: 2
Items: 
Size: 623205 Color: 10
Size: 376174 Color: 18

Bin 4065: 623 of cap free
Amount of items: 2
Items: 
Size: 691929 Color: 18
Size: 307449 Color: 14

Bin 4066: 629 of cap free
Amount of items: 2
Items: 
Size: 601727 Color: 1
Size: 397645 Color: 9

Bin 4067: 630 of cap free
Amount of items: 2
Items: 
Size: 786087 Color: 16
Size: 213284 Color: 12

Bin 4068: 630 of cap free
Amount of items: 2
Items: 
Size: 738629 Color: 4
Size: 260742 Color: 8

Bin 4069: 631 of cap free
Amount of items: 2
Items: 
Size: 764783 Color: 15
Size: 234587 Color: 14

Bin 4070: 634 of cap free
Amount of items: 2
Items: 
Size: 562766 Color: 3
Size: 436601 Color: 16

Bin 4071: 638 of cap free
Amount of items: 2
Items: 
Size: 578128 Color: 17
Size: 421235 Color: 1

Bin 4072: 638 of cap free
Amount of items: 2
Items: 
Size: 643609 Color: 6
Size: 355754 Color: 14

Bin 4073: 640 of cap free
Amount of items: 2
Items: 
Size: 572317 Color: 12
Size: 427044 Color: 3

Bin 4074: 641 of cap free
Amount of items: 2
Items: 
Size: 540167 Color: 5
Size: 459193 Color: 15

Bin 4075: 641 of cap free
Amount of items: 2
Items: 
Size: 647932 Color: 9
Size: 351428 Color: 6

Bin 4076: 642 of cap free
Amount of items: 2
Items: 
Size: 546431 Color: 10
Size: 452928 Color: 15

Bin 4077: 643 of cap free
Amount of items: 2
Items: 
Size: 667181 Color: 2
Size: 332177 Color: 10

Bin 4078: 644 of cap free
Amount of items: 2
Items: 
Size: 729441 Color: 10
Size: 269916 Color: 7

Bin 4079: 644 of cap free
Amount of items: 2
Items: 
Size: 547376 Color: 14
Size: 451981 Color: 11

Bin 4080: 647 of cap free
Amount of items: 2
Items: 
Size: 598053 Color: 16
Size: 401301 Color: 10

Bin 4081: 648 of cap free
Amount of items: 2
Items: 
Size: 534495 Color: 10
Size: 464858 Color: 9

Bin 4082: 649 of cap free
Amount of items: 2
Items: 
Size: 572013 Color: 3
Size: 427339 Color: 8

Bin 4083: 649 of cap free
Amount of items: 2
Items: 
Size: 633077 Color: 17
Size: 366275 Color: 9

Bin 4084: 654 of cap free
Amount of items: 2
Items: 
Size: 595804 Color: 18
Size: 403543 Color: 12

Bin 4085: 655 of cap free
Amount of items: 2
Items: 
Size: 686556 Color: 12
Size: 312790 Color: 9

Bin 4086: 655 of cap free
Amount of items: 2
Items: 
Size: 659617 Color: 7
Size: 339729 Color: 17

Bin 4087: 656 of cap free
Amount of items: 2
Items: 
Size: 642127 Color: 2
Size: 357218 Color: 0

Bin 4088: 656 of cap free
Amount of items: 2
Items: 
Size: 537309 Color: 17
Size: 462036 Color: 12

Bin 4089: 658 of cap free
Amount of items: 2
Items: 
Size: 707745 Color: 16
Size: 291598 Color: 17

Bin 4090: 660 of cap free
Amount of items: 2
Items: 
Size: 556217 Color: 0
Size: 443124 Color: 5

Bin 4091: 662 of cap free
Amount of items: 2
Items: 
Size: 582238 Color: 11
Size: 417101 Color: 15

Bin 4092: 663 of cap free
Amount of items: 2
Items: 
Size: 732587 Color: 16
Size: 266751 Color: 7

Bin 4093: 665 of cap free
Amount of items: 2
Items: 
Size: 504358 Color: 10
Size: 494978 Color: 19

Bin 4094: 671 of cap free
Amount of items: 2
Items: 
Size: 529400 Color: 1
Size: 469930 Color: 11

Bin 4095: 672 of cap free
Amount of items: 2
Items: 
Size: 684743 Color: 2
Size: 314586 Color: 16

Bin 4096: 672 of cap free
Amount of items: 2
Items: 
Size: 623174 Color: 2
Size: 376155 Color: 1

Bin 4097: 673 of cap free
Amount of items: 2
Items: 
Size: 774570 Color: 6
Size: 224758 Color: 18

Bin 4098: 678 of cap free
Amount of items: 2
Items: 
Size: 601693 Color: 4
Size: 397630 Color: 8

Bin 4099: 679 of cap free
Amount of items: 2
Items: 
Size: 765975 Color: 9
Size: 233347 Color: 8

Bin 4100: 686 of cap free
Amount of items: 2
Items: 
Size: 653916 Color: 1
Size: 345399 Color: 0

Bin 4101: 689 of cap free
Amount of items: 2
Items: 
Size: 502167 Color: 15
Size: 497145 Color: 10

Bin 4102: 690 of cap free
Amount of items: 3
Items: 
Size: 674109 Color: 6
Size: 169494 Color: 3
Size: 155708 Color: 6

Bin 4103: 698 of cap free
Amount of items: 2
Items: 
Size: 705697 Color: 6
Size: 293606 Color: 15

Bin 4104: 698 of cap free
Amount of items: 2
Items: 
Size: 525580 Color: 13
Size: 473723 Color: 6

Bin 4105: 699 of cap free
Amount of items: 2
Items: 
Size: 578124 Color: 9
Size: 421178 Color: 12

Bin 4106: 701 of cap free
Amount of items: 2
Items: 
Size: 593347 Color: 18
Size: 405953 Color: 10

Bin 4107: 701 of cap free
Amount of items: 2
Items: 
Size: 546425 Color: 18
Size: 452875 Color: 0

Bin 4108: 704 of cap free
Amount of items: 2
Items: 
Size: 556215 Color: 11
Size: 443082 Color: 19

Bin 4109: 705 of cap free
Amount of items: 2
Items: 
Size: 798861 Color: 1
Size: 200435 Color: 10

Bin 4110: 706 of cap free
Amount of items: 2
Items: 
Size: 664091 Color: 8
Size: 335204 Color: 1

Bin 4111: 712 of cap free
Amount of items: 2
Items: 
Size: 505655 Color: 10
Size: 493634 Color: 2

Bin 4112: 713 of cap free
Amount of items: 2
Items: 
Size: 776602 Color: 10
Size: 222686 Color: 4

Bin 4113: 715 of cap free
Amount of items: 2
Items: 
Size: 505653 Color: 13
Size: 493633 Color: 9

Bin 4114: 716 of cap free
Amount of items: 2
Items: 
Size: 569602 Color: 1
Size: 429683 Color: 6

Bin 4115: 717 of cap free
Amount of items: 2
Items: 
Size: 721766 Color: 11
Size: 277518 Color: 15

Bin 4116: 718 of cap free
Amount of items: 2
Items: 
Size: 690075 Color: 18
Size: 309208 Color: 11

Bin 4117: 721 of cap free
Amount of items: 2
Items: 
Size: 769694 Color: 4
Size: 229586 Color: 6

Bin 4118: 722 of cap free
Amount of items: 2
Items: 
Size: 588514 Color: 18
Size: 410765 Color: 17

Bin 4119: 727 of cap free
Amount of items: 2
Items: 
Size: 680651 Color: 3
Size: 318623 Color: 17

Bin 4120: 729 of cap free
Amount of items: 2
Items: 
Size: 730437 Color: 8
Size: 268835 Color: 17

Bin 4121: 730 of cap free
Amount of items: 2
Items: 
Size: 617332 Color: 17
Size: 381939 Color: 8

Bin 4122: 731 of cap free
Amount of items: 2
Items: 
Size: 668205 Color: 10
Size: 331065 Color: 4

Bin 4123: 732 of cap free
Amount of items: 2
Items: 
Size: 525548 Color: 9
Size: 473721 Color: 12

Bin 4124: 735 of cap free
Amount of items: 2
Items: 
Size: 693970 Color: 5
Size: 305296 Color: 10

Bin 4125: 737 of cap free
Amount of items: 2
Items: 
Size: 578120 Color: 19
Size: 421144 Color: 6

Bin 4126: 737 of cap free
Amount of items: 2
Items: 
Size: 517614 Color: 13
Size: 481650 Color: 15

Bin 4127: 737 of cap free
Amount of items: 2
Items: 
Size: 546421 Color: 17
Size: 452843 Color: 3

Bin 4128: 740 of cap free
Amount of items: 2
Items: 
Size: 709066 Color: 15
Size: 290195 Color: 19

Bin 4129: 742 of cap free
Amount of items: 2
Items: 
Size: 548405 Color: 18
Size: 450854 Color: 17

Bin 4130: 744 of cap free
Amount of items: 2
Items: 
Size: 684674 Color: 4
Size: 314583 Color: 7

Bin 4131: 747 of cap free
Amount of items: 2
Items: 
Size: 664016 Color: 19
Size: 335238 Color: 13

Bin 4132: 747 of cap free
Amount of items: 2
Items: 
Size: 643508 Color: 17
Size: 355746 Color: 0

Bin 4133: 749 of cap free
Amount of items: 2
Items: 
Size: 597975 Color: 2
Size: 401277 Color: 16

Bin 4134: 755 of cap free
Amount of items: 2
Items: 
Size: 736663 Color: 11
Size: 262583 Color: 14

Bin 4135: 765 of cap free
Amount of items: 2
Items: 
Size: 783638 Color: 18
Size: 215598 Color: 5

Bin 4136: 766 of cap free
Amount of items: 3
Items: 
Size: 689952 Color: 0
Size: 157556 Color: 17
Size: 151727 Color: 16

Bin 4137: 769 of cap free
Amount of items: 3
Items: 
Size: 750997 Color: 13
Size: 130545 Color: 18
Size: 117690 Color: 6

Bin 4138: 769 of cap free
Amount of items: 2
Items: 
Size: 636027 Color: 11
Size: 363205 Color: 14

Bin 4139: 775 of cap free
Amount of items: 2
Items: 
Size: 543021 Color: 5
Size: 456205 Color: 0

Bin 4140: 777 of cap free
Amount of items: 2
Items: 
Size: 768144 Color: 3
Size: 231080 Color: 17

Bin 4141: 777 of cap free
Amount of items: 2
Items: 
Size: 629020 Color: 12
Size: 370204 Color: 4

Bin 4142: 777 of cap free
Amount of items: 2
Items: 
Size: 540075 Color: 18
Size: 459149 Color: 12

Bin 4143: 778 of cap free
Amount of items: 2
Items: 
Size: 722504 Color: 18
Size: 276719 Color: 3

Bin 4144: 779 of cap free
Amount of items: 2
Items: 
Size: 640965 Color: 2
Size: 358257 Color: 17

Bin 4145: 779 of cap free
Amount of items: 2
Items: 
Size: 597954 Color: 8
Size: 401268 Color: 10

Bin 4146: 780 of cap free
Amount of items: 2
Items: 
Size: 765925 Color: 0
Size: 233296 Color: 9

Bin 4147: 782 of cap free
Amount of items: 2
Items: 
Size: 548385 Color: 15
Size: 450834 Color: 10

Bin 4148: 782 of cap free
Amount of items: 2
Items: 
Size: 618575 Color: 7
Size: 380644 Color: 17

Bin 4149: 783 of cap free
Amount of items: 2
Items: 
Size: 524745 Color: 8
Size: 474473 Color: 9

Bin 4150: 784 of cap free
Amount of items: 2
Items: 
Size: 722574 Color: 12
Size: 276643 Color: 3

Bin 4151: 784 of cap free
Amount of items: 2
Items: 
Size: 720391 Color: 8
Size: 278826 Color: 12

Bin 4152: 786 of cap free
Amount of items: 2
Items: 
Size: 556162 Color: 11
Size: 443053 Color: 2

Bin 4153: 788 of cap free
Amount of items: 2
Items: 
Size: 782476 Color: 5
Size: 216737 Color: 16

Bin 4154: 789 of cap free
Amount of items: 2
Items: 
Size: 729331 Color: 15
Size: 269881 Color: 4

Bin 4155: 789 of cap free
Amount of items: 2
Items: 
Size: 688881 Color: 16
Size: 310331 Color: 2

Bin 4156: 791 of cap free
Amount of items: 2
Items: 
Size: 737693 Color: 19
Size: 261517 Color: 10

Bin 4157: 795 of cap free
Amount of items: 3
Items: 
Size: 489123 Color: 17
Size: 341750 Color: 12
Size: 168333 Color: 10

Bin 4158: 797 of cap free
Amount of items: 2
Items: 
Size: 753170 Color: 2
Size: 246034 Color: 4

Bin 4159: 797 of cap free
Amount of items: 2
Items: 
Size: 783802 Color: 16
Size: 215402 Color: 6

Bin 4160: 800 of cap free
Amount of items: 2
Items: 
Size: 695202 Color: 19
Size: 303999 Color: 9

Bin 4161: 805 of cap free
Amount of items: 2
Items: 
Size: 505598 Color: 13
Size: 493598 Color: 1

Bin 4162: 808 of cap free
Amount of items: 2
Items: 
Size: 537129 Color: 3
Size: 462064 Color: 17

Bin 4163: 808 of cap free
Amount of items: 2
Items: 
Size: 509859 Color: 12
Size: 489334 Color: 3

Bin 4164: 809 of cap free
Amount of items: 2
Items: 
Size: 687649 Color: 13
Size: 311543 Color: 6

Bin 4165: 811 of cap free
Amount of items: 2
Items: 
Size: 646210 Color: 18
Size: 352980 Color: 1

Bin 4166: 812 of cap free
Amount of items: 2
Items: 
Size: 620615 Color: 1
Size: 378574 Color: 8

Bin 4167: 812 of cap free
Amount of items: 2
Items: 
Size: 559422 Color: 19
Size: 439767 Color: 14

Bin 4168: 814 of cap free
Amount of items: 2
Items: 
Size: 665066 Color: 15
Size: 334121 Color: 3

Bin 4169: 819 of cap free
Amount of items: 2
Items: 
Size: 795406 Color: 7
Size: 203776 Color: 0

Bin 4170: 826 of cap free
Amount of items: 2
Items: 
Size: 710762 Color: 17
Size: 288413 Color: 15

Bin 4171: 829 of cap free
Amount of items: 2
Items: 
Size: 574653 Color: 3
Size: 424519 Color: 19

Bin 4172: 831 of cap free
Amount of items: 2
Items: 
Size: 627052 Color: 15
Size: 372118 Color: 19

Bin 4173: 833 of cap free
Amount of items: 2
Items: 
Size: 698180 Color: 15
Size: 300988 Color: 2

Bin 4174: 834 of cap free
Amount of items: 2
Items: 
Size: 500012 Color: 16
Size: 499155 Color: 12

Bin 4175: 836 of cap free
Amount of items: 2
Items: 
Size: 747129 Color: 7
Size: 252036 Color: 17

Bin 4176: 836 of cap free
Amount of items: 2
Items: 
Size: 605058 Color: 2
Size: 394107 Color: 11

Bin 4177: 837 of cap free
Amount of items: 2
Items: 
Size: 603669 Color: 16
Size: 395495 Color: 8

Bin 4178: 840 of cap free
Amount of items: 2
Items: 
Size: 760977 Color: 13
Size: 238184 Color: 5

Bin 4179: 854 of cap free
Amount of items: 2
Items: 
Size: 547230 Color: 14
Size: 451917 Color: 15

Bin 4180: 856 of cap free
Amount of items: 2
Items: 
Size: 619360 Color: 11
Size: 379785 Color: 18

Bin 4181: 857 of cap free
Amount of items: 2
Items: 
Size: 532543 Color: 11
Size: 466601 Color: 10

Bin 4182: 857 of cap free
Amount of items: 2
Items: 
Size: 576703 Color: 16
Size: 422441 Color: 12

Bin 4183: 863 of cap free
Amount of items: 2
Items: 
Size: 639698 Color: 15
Size: 359440 Color: 9

Bin 4184: 864 of cap free
Amount of items: 2
Items: 
Size: 622989 Color: 5
Size: 376148 Color: 11

Bin 4185: 866 of cap free
Amount of items: 2
Items: 
Size: 502065 Color: 6
Size: 497070 Color: 19

Bin 4186: 871 of cap free
Amount of items: 2
Items: 
Size: 564799 Color: 2
Size: 434331 Color: 12

Bin 4187: 872 of cap free
Amount of items: 2
Items: 
Size: 562703 Color: 11
Size: 436426 Color: 3

Bin 4188: 874 of cap free
Amount of items: 2
Items: 
Size: 511435 Color: 18
Size: 487692 Color: 5

Bin 4189: 875 of cap free
Amount of items: 2
Items: 
Size: 720376 Color: 0
Size: 278750 Color: 17

Bin 4190: 877 of cap free
Amount of items: 2
Items: 
Size: 586623 Color: 4
Size: 412501 Color: 6

Bin 4191: 879 of cap free
Amount of items: 2
Items: 
Size: 698101 Color: 17
Size: 301021 Color: 15

Bin 4192: 881 of cap free
Amount of items: 2
Items: 
Size: 733963 Color: 10
Size: 265157 Color: 11

Bin 4193: 886 of cap free
Amount of items: 2
Items: 
Size: 739734 Color: 13
Size: 259381 Color: 3

Bin 4194: 889 of cap free
Amount of items: 2
Items: 
Size: 749037 Color: 5
Size: 250075 Color: 17

Bin 4195: 895 of cap free
Amount of items: 3
Items: 
Size: 491437 Color: 6
Size: 347638 Color: 18
Size: 160031 Color: 16

Bin 4196: 895 of cap free
Amount of items: 2
Items: 
Size: 620417 Color: 5
Size: 378689 Color: 7

Bin 4197: 896 of cap free
Amount of items: 2
Items: 
Size: 757078 Color: 8
Size: 242027 Color: 7

Bin 4198: 896 of cap free
Amount of items: 2
Items: 
Size: 517610 Color: 13
Size: 481495 Color: 18

Bin 4199: 898 of cap free
Amount of items: 2
Items: 
Size: 502043 Color: 10
Size: 497060 Color: 15

Bin 4200: 898 of cap free
Amount of items: 2
Items: 
Size: 509777 Color: 9
Size: 489326 Color: 18

Bin 4201: 900 of cap free
Amount of items: 2
Items: 
Size: 680519 Color: 12
Size: 318582 Color: 0

Bin 4202: 929 of cap free
Amount of items: 2
Items: 
Size: 716905 Color: 5
Size: 282167 Color: 16

Bin 4203: 929 of cap free
Amount of items: 2
Items: 
Size: 781059 Color: 4
Size: 218013 Color: 5

Bin 4204: 931 of cap free
Amount of items: 2
Items: 
Size: 521843 Color: 0
Size: 477227 Color: 6

Bin 4205: 940 of cap free
Amount of items: 2
Items: 
Size: 593315 Color: 1
Size: 405746 Color: 6

Bin 4206: 942 of cap free
Amount of items: 2
Items: 
Size: 773404 Color: 5
Size: 225655 Color: 3

Bin 4207: 942 of cap free
Amount of items: 2
Items: 
Size: 695072 Color: 8
Size: 303987 Color: 3

Bin 4208: 944 of cap free
Amount of items: 2
Items: 
Size: 783647 Color: 8
Size: 215410 Color: 5

Bin 4209: 948 of cap free
Amount of items: 2
Items: 
Size: 798510 Color: 16
Size: 200543 Color: 11

Bin 4210: 953 of cap free
Amount of items: 2
Items: 
Size: 534221 Color: 13
Size: 464827 Color: 5

Bin 4211: 953 of cap free
Amount of items: 3
Items: 
Size: 492277 Color: 5
Size: 369035 Color: 9
Size: 137736 Color: 19

Bin 4212: 955 of cap free
Amount of items: 2
Items: 
Size: 665064 Color: 13
Size: 333982 Color: 19

Bin 4213: 957 of cap free
Amount of items: 2
Items: 
Size: 713413 Color: 9
Size: 285631 Color: 17

Bin 4214: 961 of cap free
Amount of items: 2
Items: 
Size: 670356 Color: 10
Size: 328684 Color: 14

Bin 4215: 964 of cap free
Amount of items: 2
Items: 
Size: 540052 Color: 9
Size: 458985 Color: 3

Bin 4216: 968 of cap free
Amount of items: 2
Items: 
Size: 710629 Color: 0
Size: 288404 Color: 7

Bin 4217: 968 of cap free
Amount of items: 2
Items: 
Size: 526743 Color: 18
Size: 472290 Color: 14

Bin 4218: 977 of cap free
Amount of items: 2
Items: 
Size: 603570 Color: 6
Size: 395454 Color: 0

Bin 4219: 985 of cap free
Amount of items: 2
Items: 
Size: 689813 Color: 17
Size: 309203 Color: 1

Bin 4220: 986 of cap free
Amount of items: 2
Items: 
Size: 650577 Color: 13
Size: 348438 Color: 6

Bin 4221: 987 of cap free
Amount of items: 2
Items: 
Size: 676463 Color: 2
Size: 322551 Color: 14

Bin 4222: 993 of cap free
Amount of items: 2
Items: 
Size: 686351 Color: 17
Size: 312657 Color: 0

Bin 4223: 995 of cap free
Amount of items: 2
Items: 
Size: 595518 Color: 8
Size: 403488 Color: 4

Bin 4224: 995 of cap free
Amount of items: 3
Items: 
Size: 523967 Color: 3
Size: 301006 Color: 13
Size: 174033 Color: 14

Bin 4225: 996 of cap free
Amount of items: 2
Items: 
Size: 722529 Color: 13
Size: 276476 Color: 4

Bin 4226: 1003 of cap free
Amount of items: 2
Items: 
Size: 529078 Color: 3
Size: 469920 Color: 17

Bin 4227: 1004 of cap free
Amount of items: 2
Items: 
Size: 583877 Color: 9
Size: 415120 Color: 4

Bin 4228: 1008 of cap free
Amount of items: 2
Items: 
Size: 632584 Color: 14
Size: 366409 Color: 3

Bin 4229: 1010 of cap free
Amount of items: 2
Items: 
Size: 700053 Color: 10
Size: 298938 Color: 9

Bin 4230: 1014 of cap free
Amount of items: 2
Items: 
Size: 597780 Color: 6
Size: 401207 Color: 18

Bin 4231: 1015 of cap free
Amount of items: 3
Items: 
Size: 707932 Color: 17
Size: 147797 Color: 14
Size: 143257 Color: 2

Bin 4232: 1016 of cap free
Amount of items: 2
Items: 
Size: 684707 Color: 8
Size: 314278 Color: 19

Bin 4233: 1026 of cap free
Amount of items: 2
Items: 
Size: 793986 Color: 10
Size: 204989 Color: 6

Bin 4234: 1034 of cap free
Amount of items: 2
Items: 
Size: 564762 Color: 18
Size: 434205 Color: 7

Bin 4235: 1043 of cap free
Amount of items: 2
Items: 
Size: 700206 Color: 12
Size: 298752 Color: 1

Bin 4236: 1054 of cap free
Amount of items: 2
Items: 
Size: 743351 Color: 9
Size: 255596 Color: 5

Bin 4237: 1055 of cap free
Amount of items: 2
Items: 
Size: 786010 Color: 14
Size: 212936 Color: 18

Bin 4238: 1057 of cap free
Amount of items: 2
Items: 
Size: 576677 Color: 4
Size: 422267 Color: 19

Bin 4239: 1058 of cap free
Amount of items: 2
Items: 
Size: 710650 Color: 12
Size: 288293 Color: 8

Bin 4240: 1062 of cap free
Amount of items: 2
Items: 
Size: 650501 Color: 15
Size: 348438 Color: 19

Bin 4241: 1063 of cap free
Amount of items: 2
Items: 
Size: 576662 Color: 16
Size: 422276 Color: 15

Bin 4242: 1065 of cap free
Amount of items: 2
Items: 
Size: 656829 Color: 4
Size: 342107 Color: 8

Bin 4243: 1066 of cap free
Amount of items: 2
Items: 
Size: 590998 Color: 14
Size: 407937 Color: 6

Bin 4244: 1068 of cap free
Amount of items: 2
Items: 
Size: 501971 Color: 10
Size: 496962 Color: 14

Bin 4245: 1071 of cap free
Amount of items: 2
Items: 
Size: 687425 Color: 10
Size: 311505 Color: 8

Bin 4246: 1072 of cap free
Amount of items: 2
Items: 
Size: 522780 Color: 6
Size: 476149 Color: 13

Bin 4247: 1072 of cap free
Amount of items: 2
Items: 
Size: 559332 Color: 5
Size: 439597 Color: 10

Bin 4248: 1089 of cap free
Amount of items: 2
Items: 
Size: 532392 Color: 3
Size: 466520 Color: 10

Bin 4249: 1096 of cap free
Amount of items: 2
Items: 
Size: 773331 Color: 14
Size: 225574 Color: 8

Bin 4250: 1102 of cap free
Amount of items: 2
Items: 
Size: 780880 Color: 9
Size: 218019 Color: 14

Bin 4251: 1105 of cap free
Amount of items: 2
Items: 
Size: 650495 Color: 1
Size: 348401 Color: 10

Bin 4252: 1110 of cap free
Amount of items: 2
Items: 
Size: 670343 Color: 3
Size: 328548 Color: 1

Bin 4253: 1120 of cap free
Amount of items: 2
Items: 
Size: 687390 Color: 17
Size: 311491 Color: 5

Bin 4254: 1125 of cap free
Amount of items: 2
Items: 
Size: 640675 Color: 15
Size: 358201 Color: 10

Bin 4255: 1127 of cap free
Amount of items: 2
Items: 
Size: 687848 Color: 10
Size: 311026 Color: 16

Bin 4256: 1129 of cap free
Amount of items: 2
Items: 
Size: 640684 Color: 10
Size: 358188 Color: 3

Bin 4257: 1131 of cap free
Amount of items: 2
Items: 
Size: 515512 Color: 9
Size: 483358 Color: 4

Bin 4258: 1132 of cap free
Amount of items: 2
Items: 
Size: 580414 Color: 15
Size: 418455 Color: 6

Bin 4259: 1133 of cap free
Amount of items: 2
Items: 
Size: 569556 Color: 19
Size: 429312 Color: 0

Bin 4260: 1139 of cap free
Amount of items: 2
Items: 
Size: 600127 Color: 7
Size: 398735 Color: 6

Bin 4261: 1141 of cap free
Amount of items: 3
Items: 
Size: 436523 Color: 7
Size: 353524 Color: 10
Size: 208813 Color: 16

Bin 4262: 1143 of cap free
Amount of items: 2
Items: 
Size: 633831 Color: 9
Size: 365027 Color: 1

Bin 4263: 1156 of cap free
Amount of items: 2
Items: 
Size: 778626 Color: 2
Size: 220219 Color: 0

Bin 4264: 1158 of cap free
Amount of items: 2
Items: 
Size: 559278 Color: 8
Size: 439565 Color: 13

Bin 4265: 1163 of cap free
Amount of items: 2
Items: 
Size: 780830 Color: 9
Size: 218008 Color: 11

Bin 4266: 1181 of cap free
Amount of items: 2
Items: 
Size: 704248 Color: 9
Size: 294572 Color: 4

Bin 4267: 1187 of cap free
Amount of items: 2
Items: 
Size: 600125 Color: 17
Size: 398689 Color: 7

Bin 4268: 1188 of cap free
Amount of items: 2
Items: 
Size: 720069 Color: 11
Size: 278744 Color: 14

Bin 4269: 1189 of cap free
Amount of items: 2
Items: 
Size: 747060 Color: 13
Size: 251752 Color: 18

Bin 4270: 1196 of cap free
Amount of items: 2
Items: 
Size: 695089 Color: 18
Size: 303716 Color: 16

Bin 4271: 1198 of cap free
Amount of items: 3
Items: 
Size: 357391 Color: 10
Size: 321332 Color: 9
Size: 320080 Color: 12

Bin 4272: 1206 of cap free
Amount of items: 2
Items: 
Size: 790727 Color: 7
Size: 208068 Color: 5

Bin 4273: 1207 of cap free
Amount of items: 2
Items: 
Size: 687360 Color: 12
Size: 311434 Color: 5

Bin 4274: 1226 of cap free
Amount of items: 2
Items: 
Size: 515423 Color: 5
Size: 483352 Color: 7

Bin 4275: 1233 of cap free
Amount of items: 2
Items: 
Size: 511098 Color: 3
Size: 487670 Color: 10

Bin 4276: 1234 of cap free
Amount of items: 2
Items: 
Size: 638000 Color: 7
Size: 360767 Color: 17

Bin 4277: 1235 of cap free
Amount of items: 2
Items: 
Size: 697820 Color: 4
Size: 300946 Color: 12

Bin 4278: 1236 of cap free
Amount of items: 2
Items: 
Size: 555713 Color: 13
Size: 443052 Color: 5

Bin 4279: 1243 of cap free
Amount of items: 2
Items: 
Size: 782195 Color: 14
Size: 216563 Color: 11

Bin 4280: 1245 of cap free
Amount of items: 2
Items: 
Size: 580350 Color: 12
Size: 418406 Color: 18

Bin 4281: 1248 of cap free
Amount of items: 2
Items: 
Size: 751070 Color: 17
Size: 247683 Color: 13

Bin 4282: 1263 of cap free
Amount of items: 3
Items: 
Size: 418483 Color: 18
Size: 379796 Color: 18
Size: 200459 Color: 12

Bin 4283: 1263 of cap free
Amount of items: 2
Items: 
Size: 511082 Color: 12
Size: 487656 Color: 0

Bin 4284: 1270 of cap free
Amount of items: 2
Items: 
Size: 798708 Color: 2
Size: 200023 Color: 5

Bin 4285: 1277 of cap free
Amount of items: 2
Items: 
Size: 532433 Color: 10
Size: 466291 Color: 4

Bin 4286: 1278 of cap free
Amount of items: 2
Items: 
Size: 580337 Color: 7
Size: 418386 Color: 8

Bin 4287: 1279 of cap free
Amount of items: 3
Items: 
Size: 363206 Color: 1
Size: 341558 Color: 13
Size: 293958 Color: 9

Bin 4288: 1285 of cap free
Amount of items: 2
Items: 
Size: 659662 Color: 10
Size: 339054 Color: 1

Bin 4289: 1286 of cap free
Amount of items: 2
Items: 
Size: 536695 Color: 5
Size: 462020 Color: 14

Bin 4290: 1289 of cap free
Amount of items: 2
Items: 
Size: 646170 Color: 4
Size: 352542 Color: 17

Bin 4291: 1294 of cap free
Amount of items: 2
Items: 
Size: 515407 Color: 5
Size: 483300 Color: 4

Bin 4292: 1302 of cap free
Amount of items: 2
Items: 
Size: 505452 Color: 14
Size: 493247 Color: 1

Bin 4293: 1304 of cap free
Amount of items: 2
Items: 
Size: 529078 Color: 17
Size: 469619 Color: 10

Bin 4294: 1311 of cap free
Amount of items: 2
Items: 
Size: 633799 Color: 17
Size: 364891 Color: 9

Bin 4295: 1313 of cap free
Amount of items: 2
Items: 
Size: 526681 Color: 17
Size: 472007 Color: 15

Bin 4296: 1317 of cap free
Amount of items: 2
Items: 
Size: 782192 Color: 13
Size: 216492 Color: 0

Bin 4297: 1325 of cap free
Amount of items: 2
Items: 
Size: 782256 Color: 12
Size: 216420 Color: 8

Bin 4298: 1346 of cap free
Amount of items: 2
Items: 
Size: 597491 Color: 7
Size: 401164 Color: 8

Bin 4299: 1348 of cap free
Amount of items: 2
Items: 
Size: 796132 Color: 7
Size: 202521 Color: 15

Bin 4300: 1350 of cap free
Amount of items: 2
Items: 
Size: 733746 Color: 5
Size: 264905 Color: 0

Bin 4301: 1354 of cap free
Amount of items: 2
Items: 
Size: 764127 Color: 18
Size: 234520 Color: 17

Bin 4302: 1355 of cap free
Amount of items: 2
Items: 
Size: 529036 Color: 4
Size: 469610 Color: 1

Bin 4303: 1363 of cap free
Amount of items: 2
Items: 
Size: 576667 Color: 18
Size: 421971 Color: 13

Bin 4304: 1368 of cap free
Amount of items: 2
Items: 
Size: 549203 Color: 12
Size: 449430 Color: 8

Bin 4305: 1369 of cap free
Amount of items: 2
Items: 
Size: 787269 Color: 11
Size: 211363 Color: 18

Bin 4306: 1369 of cap free
Amount of items: 2
Items: 
Size: 595175 Color: 18
Size: 403457 Color: 5

Bin 4307: 1386 of cap free
Amount of items: 2
Items: 
Size: 549187 Color: 12
Size: 449428 Color: 8

Bin 4308: 1388 of cap free
Amount of items: 2
Items: 
Size: 782166 Color: 15
Size: 216447 Color: 0

Bin 4309: 1398 of cap free
Amount of items: 2
Items: 
Size: 672548 Color: 14
Size: 326055 Color: 6

Bin 4310: 1404 of cap free
Amount of items: 2
Items: 
Size: 665129 Color: 12
Size: 333468 Color: 15

Bin 4311: 1423 of cap free
Amount of items: 2
Items: 
Size: 559248 Color: 13
Size: 439330 Color: 8

Bin 4312: 1442 of cap free
Amount of items: 2
Items: 
Size: 533744 Color: 17
Size: 464815 Color: 2

Bin 4313: 1443 of cap free
Amount of items: 3
Items: 
Size: 444365 Color: 12
Size: 357474 Color: 5
Size: 196719 Color: 2

Bin 4314: 1464 of cap free
Amount of items: 2
Items: 
Size: 662693 Color: 14
Size: 335844 Color: 8

Bin 4315: 1467 of cap free
Amount of items: 2
Items: 
Size: 733735 Color: 12
Size: 264799 Color: 13

Bin 4316: 1469 of cap free
Amount of items: 2
Items: 
Size: 571201 Color: 0
Size: 427331 Color: 6

Bin 4317: 1474 of cap free
Amount of items: 2
Items: 
Size: 687372 Color: 16
Size: 311155 Color: 11

Bin 4318: 1478 of cap free
Amount of items: 2
Items: 
Size: 542940 Color: 6
Size: 455583 Color: 5

Bin 4319: 1486 of cap free
Amount of items: 2
Items: 
Size: 515216 Color: 3
Size: 483299 Color: 0

Bin 4320: 1522 of cap free
Amount of items: 2
Items: 
Size: 551443 Color: 7
Size: 447036 Color: 0

Bin 4321: 1523 of cap free
Amount of items: 2
Items: 
Size: 756474 Color: 4
Size: 242004 Color: 16

Bin 4322: 1542 of cap free
Amount of items: 3
Items: 
Size: 595645 Color: 9
Size: 221450 Color: 1
Size: 181364 Color: 2

Bin 4323: 1548 of cap free
Amount of items: 2
Items: 
Size: 559155 Color: 12
Size: 439298 Color: 9

Bin 4324: 1552 of cap free
Amount of items: 2
Items: 
Size: 720126 Color: 11
Size: 278323 Color: 4

Bin 4325: 1553 of cap free
Amount of items: 2
Items: 
Size: 760663 Color: 10
Size: 237785 Color: 13

Bin 4326: 1559 of cap free
Amount of items: 2
Items: 
Size: 685748 Color: 19
Size: 312694 Color: 18

Bin 4327: 1568 of cap free
Amount of items: 2
Items: 
Size: 515180 Color: 4
Size: 483253 Color: 14

Bin 4328: 1570 of cap free
Amount of items: 2
Items: 
Size: 542913 Color: 18
Size: 455518 Color: 19

Bin 4329: 1587 of cap free
Amount of items: 2
Items: 
Size: 597400 Color: 19
Size: 401014 Color: 1

Bin 4330: 1605 of cap free
Amount of items: 2
Items: 
Size: 505137 Color: 14
Size: 493259 Color: 0

Bin 4331: 1606 of cap free
Amount of items: 2
Items: 
Size: 542892 Color: 18
Size: 455503 Color: 15

Bin 4332: 1607 of cap free
Amount of items: 2
Items: 
Size: 647836 Color: 14
Size: 350558 Color: 5

Bin 4333: 1609 of cap free
Amount of items: 2
Items: 
Size: 731417 Color: 15
Size: 266975 Color: 1

Bin 4334: 1618 of cap free
Amount of items: 2
Items: 
Size: 590557 Color: 1
Size: 407826 Color: 9

Bin 4335: 1622 of cap free
Amount of items: 2
Items: 
Size: 559091 Color: 16
Size: 439288 Color: 19

Bin 4336: 1630 of cap free
Amount of items: 2
Items: 
Size: 632131 Color: 7
Size: 366240 Color: 12

Bin 4337: 1646 of cap free
Amount of items: 3
Items: 
Size: 502311 Color: 9
Size: 250976 Color: 15
Size: 245068 Color: 18

Bin 4338: 1651 of cap free
Amount of items: 2
Items: 
Size: 653809 Color: 3
Size: 344541 Color: 13

Bin 4339: 1657 of cap free
Amount of items: 2
Items: 
Size: 622866 Color: 16
Size: 375478 Color: 12

Bin 4340: 1658 of cap free
Amount of items: 2
Items: 
Size: 653803 Color: 18
Size: 344540 Color: 15

Bin 4341: 1674 of cap free
Amount of items: 2
Items: 
Size: 764140 Color: 2
Size: 234187 Color: 11

Bin 4342: 1674 of cap free
Amount of items: 2
Items: 
Size: 632097 Color: 0
Size: 366230 Color: 18

Bin 4343: 1676 of cap free
Amount of items: 2
Items: 
Size: 618553 Color: 7
Size: 379772 Color: 17

Bin 4344: 1683 of cap free
Amount of items: 2
Items: 
Size: 524728 Color: 19
Size: 473590 Color: 17

Bin 4345: 1693 of cap free
Amount of items: 2
Items: 
Size: 614367 Color: 4
Size: 383941 Color: 15

Bin 4346: 1694 of cap free
Amount of items: 2
Items: 
Size: 742722 Color: 10
Size: 255585 Color: 5

Bin 4347: 1695 of cap free
Amount of items: 2
Items: 
Size: 632087 Color: 2
Size: 366219 Color: 4

Bin 4348: 1703 of cap free
Amount of items: 2
Items: 
Size: 515074 Color: 6
Size: 483224 Color: 19

Bin 4349: 1703 of cap free
Amount of items: 2
Items: 
Size: 532252 Color: 16
Size: 466046 Color: 6

Bin 4350: 1716 of cap free
Amount of items: 2
Items: 
Size: 559074 Color: 16
Size: 439211 Color: 13

Bin 4351: 1720 of cap free
Amount of items: 2
Items: 
Size: 790750 Color: 9
Size: 207531 Color: 4

Bin 4352: 1723 of cap free
Amount of items: 2
Items: 
Size: 586401 Color: 4
Size: 411877 Color: 13

Bin 4353: 1729 of cap free
Amount of items: 2
Items: 
Size: 720036 Color: 18
Size: 278236 Color: 4

Bin 4354: 1743 of cap free
Amount of items: 3
Items: 
Size: 778628 Color: 1
Size: 114674 Color: 15
Size: 104956 Color: 7

Bin 4355: 1745 of cap free
Amount of items: 2
Items: 
Size: 680100 Color: 16
Size: 318156 Color: 12

Bin 4356: 1747 of cap free
Amount of items: 2
Items: 
Size: 532236 Color: 16
Size: 466018 Color: 6

Bin 4357: 1753 of cap free
Amount of items: 2
Items: 
Size: 793347 Color: 11
Size: 204901 Color: 12

Bin 4358: 1753 of cap free
Amount of items: 2
Items: 
Size: 548153 Color: 8
Size: 450095 Color: 12

Bin 4359: 1768 of cap free
Amount of items: 2
Items: 
Size: 607262 Color: 15
Size: 390971 Color: 18

Bin 4360: 1776 of cap free
Amount of items: 2
Items: 
Size: 505054 Color: 13
Size: 493171 Color: 3

Bin 4361: 1781 of cap free
Amount of items: 2
Items: 
Size: 501290 Color: 17
Size: 496930 Color: 18

Bin 4362: 1790 of cap free
Amount of items: 2
Items: 
Size: 532380 Color: 19
Size: 465831 Color: 4

Bin 4363: 1794 of cap free
Amount of items: 2
Items: 
Size: 542795 Color: 5
Size: 455412 Color: 2

Bin 4364: 1797 of cap free
Amount of items: 2
Items: 
Size: 505102 Color: 7
Size: 493102 Color: 11

Bin 4365: 1801 of cap free
Amount of items: 2
Items: 
Size: 639957 Color: 12
Size: 358243 Color: 16

Bin 4366: 1804 of cap free
Amount of items: 2
Items: 
Size: 610309 Color: 18
Size: 387888 Color: 6

Bin 4367: 1805 of cap free
Amount of items: 2
Items: 
Size: 563876 Color: 8
Size: 434320 Color: 18

Bin 4368: 1807 of cap free
Amount of items: 2
Items: 
Size: 676301 Color: 7
Size: 321893 Color: 3

Bin 4369: 1814 of cap free
Amount of items: 2
Items: 
Size: 787099 Color: 3
Size: 211088 Color: 16

Bin 4370: 1831 of cap free
Amount of items: 2
Items: 
Size: 559034 Color: 8
Size: 439136 Color: 11

Bin 4371: 1834 of cap free
Amount of items: 2
Items: 
Size: 526661 Color: 16
Size: 471506 Color: 17

Bin 4372: 1838 of cap free
Amount of items: 2
Items: 
Size: 524700 Color: 2
Size: 473463 Color: 4

Bin 4373: 1841 of cap free
Amount of items: 2
Items: 
Size: 650456 Color: 14
Size: 347704 Color: 0

Bin 4374: 1846 of cap free
Amount of items: 2
Items: 
Size: 618547 Color: 5
Size: 379608 Color: 8

Bin 4375: 1847 of cap free
Amount of items: 2
Items: 
Size: 542790 Color: 1
Size: 455364 Color: 12

Bin 4376: 1868 of cap free
Amount of items: 2
Items: 
Size: 509015 Color: 12
Size: 489118 Color: 17

Bin 4377: 1881 of cap free
Amount of items: 2
Items: 
Size: 586344 Color: 5
Size: 411776 Color: 2

Bin 4378: 1884 of cap free
Amount of items: 2
Items: 
Size: 593288 Color: 1
Size: 404829 Color: 5

Bin 4379: 1886 of cap free
Amount of items: 2
Items: 
Size: 576202 Color: 8
Size: 421913 Color: 9

Bin 4380: 1889 of cap free
Amount of items: 2
Items: 
Size: 586297 Color: 8
Size: 411815 Color: 3

Bin 4381: 1895 of cap free
Amount of items: 2
Items: 
Size: 729317 Color: 5
Size: 268789 Color: 0

Bin 4382: 1896 of cap free
Amount of items: 2
Items: 
Size: 742317 Color: 10
Size: 255788 Color: 0

Bin 4383: 1912 of cap free
Amount of items: 2
Items: 
Size: 729312 Color: 6
Size: 268777 Color: 14

Bin 4384: 1918 of cap free
Amount of items: 2
Items: 
Size: 570759 Color: 3
Size: 427324 Color: 4

Bin 4385: 1928 of cap free
Amount of items: 2
Items: 
Size: 676250 Color: 13
Size: 321823 Color: 10

Bin 4386: 1929 of cap free
Amount of items: 2
Items: 
Size: 555670 Color: 13
Size: 442402 Color: 5

Bin 4387: 1937 of cap free
Amount of items: 2
Items: 
Size: 676174 Color: 18
Size: 321890 Color: 8

Bin 4388: 1945 of cap free
Amount of items: 2
Items: 
Size: 526652 Color: 5
Size: 471404 Color: 10

Bin 4389: 1964 of cap free
Amount of items: 2
Items: 
Size: 602910 Color: 6
Size: 395127 Color: 8

Bin 4390: 1966 of cap free
Amount of items: 2
Items: 
Size: 533756 Color: 2
Size: 464279 Color: 15

Bin 4391: 1969 of cap free
Amount of items: 2
Items: 
Size: 650351 Color: 8
Size: 347681 Color: 2

Bin 4392: 1974 of cap free
Amount of items: 2
Items: 
Size: 676196 Color: 16
Size: 321831 Color: 13

Bin 4393: 1975 of cap free
Amount of items: 2
Items: 
Size: 798160 Color: 13
Size: 199866 Color: 2

Bin 4394: 1997 of cap free
Amount of items: 2
Items: 
Size: 760662 Color: 15
Size: 237342 Color: 7

Bin 4395: 2014 of cap free
Amount of items: 2
Items: 
Size: 586280 Color: 19
Size: 411707 Color: 16

Bin 4396: 2016 of cap free
Amount of items: 2
Items: 
Size: 650379 Color: 2
Size: 347606 Color: 3

Bin 4397: 2020 of cap free
Amount of items: 2
Items: 
Size: 501283 Color: 9
Size: 496698 Color: 0

Bin 4398: 2028 of cap free
Amount of items: 2
Items: 
Size: 607089 Color: 2
Size: 390884 Color: 7

Bin 4399: 2036 of cap free
Amount of items: 2
Items: 
Size: 528978 Color: 7
Size: 468987 Color: 11

Bin 4400: 2049 of cap free
Amount of items: 2
Items: 
Size: 639720 Color: 0
Size: 358232 Color: 16

Bin 4401: 2050 of cap free
Amount of items: 2
Items: 
Size: 602839 Color: 11
Size: 395112 Color: 14

Bin 4402: 2085 of cap free
Amount of items: 2
Items: 
Size: 731597 Color: 10
Size: 266319 Color: 12

Bin 4403: 2089 of cap free
Amount of items: 2
Items: 
Size: 650310 Color: 12
Size: 347602 Color: 17

Bin 4404: 2102 of cap free
Amount of items: 2
Items: 
Size: 586209 Color: 13
Size: 411690 Color: 1

Bin 4405: 2111 of cap free
Amount of items: 2
Items: 
Size: 521792 Color: 18
Size: 476098 Color: 11

Bin 4406: 2112 of cap free
Amount of items: 2
Items: 
Size: 650571 Color: 3
Size: 347318 Color: 10

Bin 4407: 2113 of cap free
Amount of items: 2
Items: 
Size: 558759 Color: 17
Size: 439129 Color: 1

Bin 4408: 2117 of cap free
Amount of items: 2
Items: 
Size: 763683 Color: 14
Size: 234201 Color: 2

Bin 4409: 2130 of cap free
Amount of items: 2
Items: 
Size: 586135 Color: 0
Size: 411736 Color: 13

Bin 4410: 2167 of cap free
Amount of items: 2
Items: 
Size: 602732 Color: 6
Size: 395102 Color: 14

Bin 4411: 2234 of cap free
Amount of items: 2
Items: 
Size: 524347 Color: 5
Size: 473420 Color: 0

Bin 4412: 2235 of cap free
Amount of items: 2
Items: 
Size: 576638 Color: 12
Size: 421128 Color: 9

Bin 4413: 2236 of cap free
Amount of items: 2
Items: 
Size: 704206 Color: 16
Size: 293559 Color: 4

Bin 4414: 2273 of cap free
Amount of items: 2
Items: 
Size: 613912 Color: 16
Size: 383816 Color: 8

Bin 4415: 2281 of cap free
Amount of items: 2
Items: 
Size: 542784 Color: 9
Size: 454936 Color: 19

Bin 4416: 2286 of cap free
Amount of items: 2
Items: 
Size: 671809 Color: 3
Size: 325906 Color: 16

Bin 4417: 2286 of cap free
Amount of items: 2
Items: 
Size: 600108 Color: 19
Size: 397607 Color: 11

Bin 4418: 2304 of cap free
Amount of items: 2
Items: 
Size: 607035 Color: 5
Size: 390662 Color: 14

Bin 4419: 2327 of cap free
Amount of items: 2
Items: 
Size: 576614 Color: 11
Size: 421060 Color: 8

Bin 4420: 2330 of cap free
Amount of items: 3
Items: 
Size: 357133 Color: 18
Size: 347114 Color: 9
Size: 293424 Color: 0

Bin 4421: 2338 of cap free
Amount of items: 2
Items: 
Size: 600106 Color: 14
Size: 397557 Color: 13

Bin 4422: 2371 of cap free
Amount of items: 2
Items: 
Size: 542695 Color: 14
Size: 454935 Color: 19

Bin 4423: 2371 of cap free
Amount of items: 2
Items: 
Size: 600079 Color: 0
Size: 397551 Color: 18

Bin 4424: 2375 of cap free
Amount of items: 2
Items: 
Size: 551260 Color: 10
Size: 446366 Color: 9

Bin 4425: 2380 of cap free
Amount of items: 2
Items: 
Size: 596612 Color: 10
Size: 401009 Color: 7

Bin 4426: 2434 of cap free
Amount of items: 2
Items: 
Size: 600072 Color: 4
Size: 397495 Color: 14

Bin 4427: 2450 of cap free
Amount of items: 2
Items: 
Size: 515074 Color: 7
Size: 482477 Color: 19

Bin 4428: 2461 of cap free
Amount of items: 2
Items: 
Size: 753069 Color: 1
Size: 244471 Color: 14

Bin 4429: 2481 of cap free
Amount of items: 2
Items: 
Size: 585976 Color: 14
Size: 411544 Color: 1

Bin 4430: 2493 of cap free
Amount of items: 3
Items: 
Size: 593144 Color: 17
Size: 246967 Color: 16
Size: 157397 Color: 10

Bin 4431: 2501 of cap free
Amount of items: 2
Items: 
Size: 542678 Color: 9
Size: 454822 Color: 13

Bin 4432: 2516 of cap free
Amount of items: 2
Items: 
Size: 676001 Color: 3
Size: 321484 Color: 15

Bin 4433: 2532 of cap free
Amount of items: 2
Items: 
Size: 606935 Color: 11
Size: 390534 Color: 16

Bin 4434: 2571 of cap free
Amount of items: 2
Items: 
Size: 606900 Color: 19
Size: 390530 Color: 17

Bin 4435: 2584 of cap free
Amount of items: 2
Items: 
Size: 524000 Color: 3
Size: 473417 Color: 8

Bin 4436: 2674 of cap free
Amount of items: 2
Items: 
Size: 650276 Color: 11
Size: 347051 Color: 18

Bin 4437: 2679 of cap free
Amount of items: 2
Items: 
Size: 545426 Color: 1
Size: 451896 Color: 17

Bin 4438: 2686 of cap free
Amount of items: 2
Items: 
Size: 558671 Color: 13
Size: 438644 Color: 7

Bin 4439: 2694 of cap free
Amount of items: 2
Items: 
Size: 501212 Color: 0
Size: 496095 Color: 4

Bin 4440: 2726 of cap free
Amount of items: 2
Items: 
Size: 514813 Color: 16
Size: 482462 Color: 11

Bin 4441: 2732 of cap free
Amount of items: 2
Items: 
Size: 662690 Color: 15
Size: 334579 Color: 8

Bin 4442: 2779 of cap free
Amount of items: 2
Items: 
Size: 542528 Color: 12
Size: 454694 Color: 2

Bin 4443: 2784 of cap free
Amount of items: 2
Items: 
Size: 787344 Color: 13
Size: 209873 Color: 11

Bin 4444: 2822 of cap free
Amount of items: 2
Items: 
Size: 671804 Color: 3
Size: 325375 Color: 4

Bin 4445: 2825 of cap free
Amount of items: 2
Items: 
Size: 558715 Color: 2
Size: 438461 Color: 10

Bin 4446: 2871 of cap free
Amount of items: 2
Items: 
Size: 742200 Color: 10
Size: 254930 Color: 9

Bin 4447: 2902 of cap free
Amount of items: 2
Items: 
Size: 577287 Color: 11
Size: 419812 Color: 3

Bin 4448: 2949 of cap free
Amount of items: 2
Items: 
Size: 742153 Color: 12
Size: 254899 Color: 6

Bin 4449: 2949 of cap free
Amount of items: 2
Items: 
Size: 514634 Color: 6
Size: 482418 Color: 3

Bin 4450: 2964 of cap free
Amount of items: 2
Items: 
Size: 771476 Color: 1
Size: 225561 Color: 19

Bin 4451: 2979 of cap free
Amount of items: 2
Items: 
Size: 742152 Color: 15
Size: 254870 Color: 13

Bin 4452: 3050 of cap free
Amount of items: 2
Items: 
Size: 675538 Color: 6
Size: 321413 Color: 17

Bin 4453: 3063 of cap free
Amount of items: 3
Items: 
Size: 720458 Color: 7
Size: 151311 Color: 18
Size: 125169 Color: 7

Bin 4454: 3066 of cap free
Amount of items: 2
Items: 
Size: 675529 Color: 8
Size: 321406 Color: 6

Bin 4455: 3090 of cap free
Amount of items: 2
Items: 
Size: 569583 Color: 0
Size: 427328 Color: 7

Bin 4456: 3112 of cap free
Amount of items: 2
Items: 
Size: 558559 Color: 9
Size: 438330 Color: 2

Bin 4457: 3212 of cap free
Amount of items: 2
Items: 
Size: 796405 Color: 0
Size: 200384 Color: 4

Bin 4458: 3235 of cap free
Amount of items: 2
Items: 
Size: 569452 Color: 8
Size: 427314 Color: 10

Bin 4459: 3329 of cap free
Amount of items: 2
Items: 
Size: 554277 Color: 9
Size: 442395 Color: 1

Bin 4460: 3442 of cap free
Amount of items: 2
Items: 
Size: 797500 Color: 9
Size: 199059 Color: 8

Bin 4461: 3490 of cap free
Amount of items: 2
Items: 
Size: 639442 Color: 0
Size: 357069 Color: 18

Bin 4462: 3511 of cap free
Amount of items: 2
Items: 
Size: 792605 Color: 11
Size: 203885 Color: 0

Bin 4463: 3539 of cap free
Amount of items: 2
Items: 
Size: 569405 Color: 19
Size: 427057 Color: 12

Bin 4464: 3623 of cap free
Amount of items: 2
Items: 
Size: 532335 Color: 10
Size: 464043 Color: 13

Bin 4465: 3726 of cap free
Amount of items: 2
Items: 
Size: 514596 Color: 18
Size: 481679 Color: 13

Bin 4466: 3936 of cap free
Amount of items: 2
Items: 
Size: 585303 Color: 11
Size: 410762 Color: 2

Bin 4467: 3944 of cap free
Amount of items: 2
Items: 
Size: 532029 Color: 10
Size: 464028 Color: 12

Bin 4468: 4016 of cap free
Amount of items: 2
Items: 
Size: 741118 Color: 11
Size: 254867 Color: 15

Bin 4469: 4016 of cap free
Amount of items: 2
Items: 
Size: 616302 Color: 16
Size: 379683 Color: 6

Bin 4470: 4027 of cap free
Amount of items: 2
Items: 
Size: 595505 Color: 5
Size: 400469 Color: 0

Bin 4471: 4039 of cap free
Amount of items: 2
Items: 
Size: 568874 Color: 14
Size: 427088 Color: 2

Bin 4472: 4091 of cap free
Amount of items: 2
Items: 
Size: 595448 Color: 11
Size: 400462 Color: 2

Bin 4473: 4128 of cap free
Amount of items: 2
Items: 
Size: 514554 Color: 3
Size: 481319 Color: 9

Bin 4474: 4193 of cap free
Amount of items: 2
Items: 
Size: 585065 Color: 1
Size: 410743 Color: 17

Bin 4475: 4195 of cap free
Amount of items: 2
Items: 
Size: 770181 Color: 16
Size: 225625 Color: 13

Bin 4476: 4253 of cap free
Amount of items: 2
Items: 
Size: 595389 Color: 9
Size: 400359 Color: 11

Bin 4477: 4286 of cap free
Amount of items: 2
Items: 
Size: 514543 Color: 10
Size: 481172 Color: 1

Bin 4478: 4550 of cap free
Amount of items: 2
Items: 
Size: 553637 Color: 18
Size: 441814 Color: 12

Bin 4479: 4565 of cap free
Amount of items: 2
Items: 
Size: 568876 Color: 2
Size: 426560 Color: 18

Bin 4480: 4576 of cap free
Amount of items: 2
Items: 
Size: 585021 Color: 4
Size: 410404 Color: 14

Bin 4481: 4599 of cap free
Amount of items: 2
Items: 
Size: 568876 Color: 2
Size: 426526 Color: 18

Bin 4482: 4661 of cap free
Amount of items: 2
Items: 
Size: 584969 Color: 15
Size: 410371 Color: 0

Bin 4483: 4683 of cap free
Amount of items: 2
Items: 
Size: 514510 Color: 8
Size: 480808 Color: 16

Bin 4484: 4979 of cap free
Amount of items: 2
Items: 
Size: 568532 Color: 11
Size: 426490 Color: 7

Bin 4485: 5093 of cap free
Amount of items: 2
Items: 
Size: 639339 Color: 11
Size: 355569 Color: 16

Bin 4486: 5232 of cap free
Amount of items: 2
Items: 
Size: 573718 Color: 19
Size: 421051 Color: 0

Bin 4487: 5254 of cap free
Amount of items: 2
Items: 
Size: 498795 Color: 7
Size: 495952 Color: 6

Bin 4488: 5373 of cap free
Amount of items: 2
Items: 
Size: 573579 Color: 1
Size: 421049 Color: 16

Bin 4489: 5398 of cap free
Amount of items: 2
Items: 
Size: 616190 Color: 6
Size: 378413 Color: 15

Bin 4490: 5451 of cap free
Amount of items: 2
Items: 
Size: 573573 Color: 5
Size: 420977 Color: 14

Bin 4491: 5660 of cap free
Amount of items: 2
Items: 
Size: 573522 Color: 8
Size: 420819 Color: 1

Bin 4492: 5888 of cap free
Amount of items: 2
Items: 
Size: 756713 Color: 6
Size: 237400 Color: 3

Bin 4493: 6440 of cap free
Amount of items: 2
Items: 
Size: 513498 Color: 0
Size: 480063 Color: 11

Bin 4494: 6686 of cap free
Amount of items: 2
Items: 
Size: 513308 Color: 0
Size: 480007 Color: 13

Bin 4495: 7189 of cap free
Amount of items: 2
Items: 
Size: 592471 Color: 2
Size: 400341 Color: 1

Bin 4496: 7305 of cap free
Amount of items: 2
Items: 
Size: 496367 Color: 11
Size: 496329 Color: 4

Bin 4497: 7700 of cap free
Amount of items: 2
Items: 
Size: 541474 Color: 19
Size: 450827 Color: 8

Bin 4498: 9123 of cap free
Amount of items: 2
Items: 
Size: 541509 Color: 0
Size: 449369 Color: 11

Bin 4499: 9153 of cap free
Amount of items: 2
Items: 
Size: 541437 Color: 0
Size: 449411 Color: 19

Bin 4500: 9376 of cap free
Amount of items: 2
Items: 
Size: 541293 Color: 1
Size: 449332 Color: 11

Bin 4501: 9953 of cap free
Amount of items: 2
Items: 
Size: 521586 Color: 17
Size: 468462 Color: 11

Bin 4502: 10835 of cap free
Amount of items: 2
Items: 
Size: 793146 Color: 6
Size: 196020 Color: 9

Bin 4503: 11320 of cap free
Amount of items: 2
Items: 
Size: 551253 Color: 13
Size: 437428 Color: 9

Bin 4504: 11504 of cap free
Amount of items: 2
Items: 
Size: 650482 Color: 13
Size: 338015 Color: 6

Bin 4505: 12714 of cap free
Amount of items: 2
Items: 
Size: 630570 Color: 16
Size: 356717 Color: 11

Bin 4506: 14034 of cap free
Amount of items: 2
Items: 
Size: 680647 Color: 10
Size: 305320 Color: 1

Bin 4507: 14257 of cap free
Amount of items: 2
Items: 
Size: 492875 Color: 10
Size: 492869 Color: 7

Bin 4508: 14984 of cap free
Amount of items: 2
Items: 
Size: 492604 Color: 8
Size: 492413 Color: 10

Bin 4509: 16141 of cap free
Amount of items: 2
Items: 
Size: 491948 Color: 14
Size: 491912 Color: 13

Bin 4510: 16494 of cap free
Amount of items: 2
Items: 
Size: 791034 Color: 2
Size: 192473 Color: 11

Bin 4511: 17368 of cap free
Amount of items: 2
Items: 
Size: 491336 Color: 16
Size: 491297 Color: 9

Bin 4512: 17653 of cap free
Amount of items: 2
Items: 
Size: 491185 Color: 11
Size: 491163 Color: 0

Bin 4513: 49710 of cap free
Amount of items: 2
Items: 
Size: 769800 Color: 15
Size: 180491 Color: 10

Total size: 4511670666
Total free space: 1333847

