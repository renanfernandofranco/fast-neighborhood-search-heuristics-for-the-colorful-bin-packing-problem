Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 367160 Color: 14
Size: 333116 Color: 14
Size: 299725 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 371053 Color: 19
Size: 317780 Color: 9
Size: 311168 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 371361 Color: 19
Size: 350283 Color: 15
Size: 278357 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 371562 Color: 14
Size: 336241 Color: 10
Size: 292198 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 371640 Color: 3
Size: 318178 Color: 10
Size: 310183 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 372446 Color: 19
Size: 338364 Color: 16
Size: 289191 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 372595 Color: 0
Size: 315770 Color: 8
Size: 311636 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 373277 Color: 16
Size: 324819 Color: 3
Size: 301905 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 373351 Color: 14
Size: 370907 Color: 11
Size: 255743 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 373451 Color: 1
Size: 317169 Color: 13
Size: 309381 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 374044 Color: 2
Size: 364219 Color: 14
Size: 261738 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374214 Color: 3
Size: 318250 Color: 10
Size: 307537 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 374224 Color: 15
Size: 370381 Color: 0
Size: 255396 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 374425 Color: 11
Size: 362763 Color: 7
Size: 262813 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 374430 Color: 2
Size: 344362 Color: 19
Size: 281209 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 374620 Color: 3
Size: 323290 Color: 5
Size: 302091 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 374693 Color: 16
Size: 346846 Color: 14
Size: 278462 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375079 Color: 6
Size: 332856 Color: 14
Size: 292066 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375526 Color: 16
Size: 344939 Color: 5
Size: 279536 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375987 Color: 18
Size: 360240 Color: 0
Size: 263774 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 376074 Color: 1
Size: 316046 Color: 17
Size: 307881 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 377057 Color: 3
Size: 334969 Color: 16
Size: 287975 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 377580 Color: 5
Size: 345960 Color: 17
Size: 276461 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377339 Color: 19
Size: 361243 Color: 4
Size: 261419 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 378314 Color: 11
Size: 354801 Color: 0
Size: 266886 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 378678 Color: 14
Size: 315084 Color: 13
Size: 306239 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 379212 Color: 5
Size: 346662 Color: 7
Size: 274127 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 379687 Color: 4
Size: 365406 Color: 0
Size: 254908 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 380954 Color: 12
Size: 368010 Color: 19
Size: 251037 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 381800 Color: 0
Size: 319685 Color: 8
Size: 298516 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382391 Color: 8
Size: 332162 Color: 13
Size: 285448 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383302 Color: 3
Size: 325029 Color: 2
Size: 291670 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 383366 Color: 12
Size: 365323 Color: 17
Size: 251312 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 383719 Color: 14
Size: 345448 Color: 12
Size: 270834 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 383914 Color: 18
Size: 318921 Color: 17
Size: 297166 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 384497 Color: 3
Size: 358818 Color: 13
Size: 256686 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 383968 Color: 11
Size: 350360 Color: 7
Size: 265673 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 384009 Color: 15
Size: 353608 Color: 18
Size: 262384 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 384432 Color: 4
Size: 339758 Color: 11
Size: 275811 Color: 7

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 385102 Color: 2
Size: 358113 Color: 12
Size: 256786 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 385202 Color: 8
Size: 338321 Color: 18
Size: 276478 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 385538 Color: 12
Size: 357547 Color: 17
Size: 256916 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 385633 Color: 8
Size: 356385 Color: 6
Size: 257983 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 385909 Color: 15
Size: 326331 Color: 10
Size: 287761 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 386512 Color: 12
Size: 313770 Color: 7
Size: 299719 Color: 8

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 386855 Color: 11
Size: 330396 Color: 13
Size: 282750 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 387083 Color: 2
Size: 355668 Color: 11
Size: 257250 Color: 19

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 387396 Color: 17
Size: 352052 Color: 1
Size: 260553 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 387743 Color: 7
Size: 334476 Color: 9
Size: 277782 Color: 13

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 388647 Color: 12
Size: 319673 Color: 12
Size: 291681 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 388654 Color: 17
Size: 340516 Color: 4
Size: 270831 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 388815 Color: 10
Size: 325413 Color: 5
Size: 285773 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 389692 Color: 3
Size: 305656 Color: 6
Size: 304653 Color: 15

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 389647 Color: 19
Size: 358136 Color: 14
Size: 252218 Color: 5

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 389661 Color: 4
Size: 330177 Color: 17
Size: 280163 Color: 9

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 389718 Color: 10
Size: 307223 Color: 19
Size: 303060 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 389752 Color: 19
Size: 316105 Color: 18
Size: 294144 Color: 9

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 390219 Color: 12
Size: 340220 Color: 19
Size: 269562 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 390746 Color: 10
Size: 324253 Color: 6
Size: 285002 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 390989 Color: 4
Size: 344850 Color: 15
Size: 264162 Color: 10

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 391218 Color: 16
Size: 310660 Color: 1
Size: 298123 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 391395 Color: 18
Size: 330944 Color: 15
Size: 277662 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 391655 Color: 12
Size: 320133 Color: 12
Size: 288213 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 392334 Color: 0
Size: 329488 Color: 5
Size: 278179 Color: 19

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 392792 Color: 8
Size: 324372 Color: 8
Size: 282837 Color: 13

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 393148 Color: 5
Size: 312173 Color: 2
Size: 294680 Color: 19

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 393783 Color: 1
Size: 310745 Color: 19
Size: 295473 Color: 8

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 393979 Color: 9
Size: 328626 Color: 5
Size: 277396 Color: 9

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 394766 Color: 3
Size: 306171 Color: 19
Size: 299064 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 394008 Color: 5
Size: 320777 Color: 0
Size: 285216 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 394678 Color: 1
Size: 353521 Color: 19
Size: 251802 Color: 12

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 395000 Color: 6
Size: 306635 Color: 13
Size: 298366 Color: 19

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 395041 Color: 7
Size: 347416 Color: 1
Size: 257544 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 395828 Color: 4
Size: 343276 Color: 3
Size: 260897 Color: 18

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 395870 Color: 17
Size: 344380 Color: 17
Size: 259751 Color: 9

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 395940 Color: 15
Size: 341588 Color: 9
Size: 262473 Color: 7

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 396230 Color: 8
Size: 328206 Color: 19
Size: 275565 Color: 16

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 396388 Color: 13
Size: 323191 Color: 17
Size: 280422 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 396883 Color: 0
Size: 340529 Color: 4
Size: 262589 Color: 8

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 397340 Color: 3
Size: 321118 Color: 6
Size: 281543 Color: 14

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 397233 Color: 1
Size: 331807 Color: 4
Size: 270961 Color: 12

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 397329 Color: 14
Size: 345692 Color: 19
Size: 256980 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 397361 Color: 14
Size: 343966 Color: 15
Size: 258674 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 399073 Color: 17
Size: 304076 Color: 16
Size: 296852 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 400146 Color: 8
Size: 348628 Color: 7
Size: 251227 Color: 5

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 400444 Color: 9
Size: 325279 Color: 14
Size: 274278 Color: 6

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 400481 Color: 19
Size: 343202 Color: 17
Size: 256318 Color: 14

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 400718 Color: 9
Size: 339831 Color: 8
Size: 259452 Color: 18

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 401068 Color: 9
Size: 304456 Color: 12
Size: 294477 Color: 16

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 401156 Color: 12
Size: 331203 Color: 16
Size: 267642 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 401386 Color: 19
Size: 309091 Color: 17
Size: 289524 Color: 9

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 401548 Color: 13
Size: 307961 Color: 16
Size: 290492 Color: 12

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 401518 Color: 3
Size: 347294 Color: 6
Size: 251189 Color: 10

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 401767 Color: 10
Size: 317402 Color: 11
Size: 280832 Color: 8

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 402591 Color: 12
Size: 338072 Color: 5
Size: 259338 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 402691 Color: 13
Size: 323741 Color: 6
Size: 273569 Color: 5

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 403102 Color: 18
Size: 317175 Color: 3
Size: 279724 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 403129 Color: 15
Size: 322856 Color: 18
Size: 274016 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 403382 Color: 7
Size: 320480 Color: 4
Size: 276139 Color: 7

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 403572 Color: 19
Size: 302754 Color: 10
Size: 293675 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 403893 Color: 9
Size: 298454 Color: 8
Size: 297654 Color: 8

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 404137 Color: 7
Size: 341482 Color: 14
Size: 254382 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 404385 Color: 6
Size: 307214 Color: 19
Size: 288402 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 404385 Color: 2
Size: 298544 Color: 10
Size: 297072 Color: 8

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 404665 Color: 17
Size: 304101 Color: 2
Size: 291235 Color: 5

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 405025 Color: 10
Size: 344239 Color: 11
Size: 250737 Color: 17

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 405238 Color: 12
Size: 334430 Color: 0
Size: 260333 Color: 11

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 405415 Color: 4
Size: 300846 Color: 19
Size: 293740 Color: 6

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 405705 Color: 15
Size: 341222 Color: 18
Size: 253074 Color: 10

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 405756 Color: 17
Size: 314590 Color: 14
Size: 279655 Color: 7

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 406046 Color: 6
Size: 322909 Color: 1
Size: 271046 Color: 17

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 406115 Color: 12
Size: 327573 Color: 7
Size: 266313 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 406493 Color: 5
Size: 324509 Color: 6
Size: 268999 Color: 10

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 407036 Color: 4
Size: 320642 Color: 3
Size: 272323 Color: 13

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 407134 Color: 12
Size: 309612 Color: 13
Size: 283255 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 407956 Color: 14
Size: 296591 Color: 9
Size: 295454 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 408221 Color: 11
Size: 326426 Color: 5
Size: 265354 Color: 8

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 408462 Color: 3
Size: 308195 Color: 13
Size: 283344 Color: 14

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 408588 Color: 9
Size: 315405 Color: 10
Size: 276008 Color: 15

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 409024 Color: 2
Size: 302109 Color: 11
Size: 288868 Color: 7

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 409026 Color: 14
Size: 324667 Color: 3
Size: 266308 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 409682 Color: 14
Size: 324424 Color: 12
Size: 265895 Color: 6

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 409827 Color: 6
Size: 324669 Color: 8
Size: 265505 Color: 12

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410063 Color: 14
Size: 329208 Color: 16
Size: 260730 Color: 16

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 410239 Color: 14
Size: 328973 Color: 5
Size: 260789 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 410882 Color: 3
Size: 295267 Color: 14
Size: 293852 Color: 15

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 410601 Color: 18
Size: 328492 Color: 18
Size: 260908 Color: 9

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 410654 Color: 15
Size: 316212 Color: 8
Size: 273135 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 410737 Color: 0
Size: 318694 Color: 0
Size: 270570 Color: 11

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 410951 Color: 11
Size: 305922 Color: 2
Size: 283128 Color: 8

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 411488 Color: 0
Size: 322275 Color: 2
Size: 266238 Color: 12

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 412106 Color: 14
Size: 295244 Color: 7
Size: 292651 Color: 19

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 412121 Color: 9
Size: 306168 Color: 1
Size: 281712 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 412577 Color: 3
Size: 337194 Color: 6
Size: 250230 Color: 10

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 412372 Color: 9
Size: 303462 Color: 19
Size: 284167 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 412391 Color: 9
Size: 315813 Color: 8
Size: 271797 Color: 13

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 413001 Color: 16
Size: 315460 Color: 8
Size: 271540 Color: 6

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 413011 Color: 0
Size: 336941 Color: 5
Size: 250049 Color: 6

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 413560 Color: 17
Size: 327941 Color: 9
Size: 258500 Color: 19

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 414284 Color: 3
Size: 330978 Color: 10
Size: 254739 Color: 18

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 413572 Color: 16
Size: 299982 Color: 11
Size: 286447 Color: 12

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 413889 Color: 4
Size: 309385 Color: 6
Size: 276727 Color: 15

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 414118 Color: 9
Size: 293317 Color: 2
Size: 292566 Color: 12

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 415941 Color: 3
Size: 314342 Color: 19
Size: 269718 Color: 12

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 415491 Color: 1
Size: 308266 Color: 6
Size: 276244 Color: 7

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 415736 Color: 13
Size: 319978 Color: 16
Size: 264287 Color: 9

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 415811 Color: 4
Size: 315399 Color: 0
Size: 268791 Color: 17

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 416808 Color: 3
Size: 292937 Color: 15
Size: 290256 Color: 11

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 415892 Color: 10
Size: 320229 Color: 12
Size: 263880 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 415963 Color: 12
Size: 303285 Color: 5
Size: 280753 Color: 12

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 416218 Color: 13
Size: 309251 Color: 6
Size: 274532 Color: 14

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 417004 Color: 3
Size: 318249 Color: 4
Size: 264748 Color: 19

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 416416 Color: 9
Size: 332117 Color: 11
Size: 251468 Color: 7

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 416439 Color: 2
Size: 328147 Color: 9
Size: 255415 Color: 19

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 416689 Color: 8
Size: 323406 Color: 11
Size: 259906 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 417058 Color: 3
Size: 303910 Color: 13
Size: 279033 Color: 17

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 417127 Color: 7
Size: 328411 Color: 8
Size: 254463 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 417456 Color: 5
Size: 300121 Color: 14
Size: 282424 Color: 7

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 417680 Color: 16
Size: 305720 Color: 14
Size: 276601 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 417984 Color: 12
Size: 319418 Color: 6
Size: 262599 Color: 18

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 418124 Color: 15
Size: 291364 Color: 0
Size: 290513 Color: 16

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 418371 Color: 15
Size: 315083 Color: 0
Size: 266547 Color: 2

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 420144 Color: 3
Size: 308312 Color: 11
Size: 271545 Color: 12

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 418611 Color: 18
Size: 331279 Color: 7
Size: 250111 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 418853 Color: 16
Size: 318627 Color: 5
Size: 262521 Color: 13

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 420186 Color: 3
Size: 318382 Color: 2
Size: 261433 Color: 7

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 419024 Color: 4
Size: 308437 Color: 2
Size: 272540 Color: 10

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 419303 Color: 13
Size: 315434 Color: 9
Size: 265264 Color: 14

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 419662 Color: 16
Size: 292012 Color: 17
Size: 288327 Color: 13

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 420367 Color: 2
Size: 312762 Color: 8
Size: 266872 Color: 13

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 420733 Color: 10
Size: 326768 Color: 1
Size: 252500 Color: 17

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 420956 Color: 8
Size: 296122 Color: 0
Size: 282923 Color: 13

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 421773 Color: 19
Size: 305772 Color: 15
Size: 272456 Color: 12

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 422585 Color: 10
Size: 301512 Color: 12
Size: 275904 Color: 19

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 422825 Color: 6
Size: 326823 Color: 17
Size: 250353 Color: 11

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 423122 Color: 19
Size: 300339 Color: 3
Size: 276540 Color: 8

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 423125 Color: 1
Size: 289370 Color: 2
Size: 287506 Color: 9

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 423318 Color: 11
Size: 324406 Color: 1
Size: 252277 Color: 10

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 423941 Color: 9
Size: 294156 Color: 17
Size: 281904 Color: 9

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 424038 Color: 2
Size: 324251 Color: 5
Size: 251712 Color: 13

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 424187 Color: 0
Size: 316915 Color: 8
Size: 258899 Color: 17

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 424781 Color: 19
Size: 314111 Color: 17
Size: 261109 Color: 8

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 424783 Color: 0
Size: 316379 Color: 8
Size: 258839 Color: 4

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 424902 Color: 15
Size: 310121 Color: 10
Size: 264978 Color: 7

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 425235 Color: 3
Size: 304107 Color: 0
Size: 270659 Color: 8

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 425082 Color: 16
Size: 312859 Color: 5
Size: 262060 Color: 18

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 425287 Color: 13
Size: 317020 Color: 6
Size: 257694 Color: 15

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 425736 Color: 13
Size: 312833 Color: 4
Size: 261432 Color: 8

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 425784 Color: 15
Size: 291311 Color: 5
Size: 282906 Color: 13

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 425861 Color: 8
Size: 321072 Color: 1
Size: 253068 Color: 10

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 426097 Color: 6
Size: 314655 Color: 8
Size: 259249 Color: 15

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 426155 Color: 17
Size: 304769 Color: 13
Size: 269077 Color: 15

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 426926 Color: 10
Size: 293156 Color: 13
Size: 279919 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 427036 Color: 19
Size: 290296 Color: 11
Size: 282669 Color: 6

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 427047 Color: 7
Size: 306809 Color: 11
Size: 266145 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 427075 Color: 16
Size: 290642 Color: 12
Size: 282284 Color: 14

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 427389 Color: 3
Size: 306616 Color: 16
Size: 265996 Color: 6

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 427317 Color: 14
Size: 308228 Color: 19
Size: 264456 Color: 4

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 427438 Color: 0
Size: 306104 Color: 6
Size: 266459 Color: 12

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 427715 Color: 14
Size: 316333 Color: 1
Size: 255953 Color: 8

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 428035 Color: 14
Size: 320592 Color: 19
Size: 251374 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 428137 Color: 6
Size: 305076 Color: 11
Size: 266788 Color: 17

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 429931 Color: 10
Size: 285825 Color: 17
Size: 284245 Color: 5

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 430349 Color: 11
Size: 312161 Color: 5
Size: 257491 Color: 7

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 430821 Color: 5
Size: 313985 Color: 3
Size: 255195 Color: 19

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 431019 Color: 16
Size: 309994 Color: 15
Size: 258988 Color: 6

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 431293 Color: 13
Size: 315562 Color: 8
Size: 253146 Color: 5

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 431423 Color: 5
Size: 290281 Color: 18
Size: 278297 Color: 14

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 431491 Color: 16
Size: 287650 Color: 8
Size: 280860 Color: 11

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 431809 Color: 3
Size: 288332 Color: 19
Size: 279860 Color: 4

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 431555 Color: 4
Size: 288991 Color: 15
Size: 279455 Color: 18

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 431728 Color: 1
Size: 311475 Color: 10
Size: 256798 Color: 5

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 431814 Color: 2
Size: 288748 Color: 12
Size: 279439 Color: 4

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 431884 Color: 11
Size: 284956 Color: 5
Size: 283161 Color: 6

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 431993 Color: 9
Size: 292558 Color: 18
Size: 275450 Color: 10

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 432435 Color: 19
Size: 308787 Color: 17
Size: 258779 Color: 5

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 432493 Color: 7
Size: 307742 Color: 13
Size: 259766 Color: 19

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 432924 Color: 9
Size: 291581 Color: 10
Size: 275496 Color: 9

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 433627 Color: 14
Size: 283444 Color: 7
Size: 282930 Color: 15

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 433783 Color: 7
Size: 298359 Color: 6
Size: 267859 Color: 7

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 433836 Color: 11
Size: 303417 Color: 9
Size: 262748 Color: 5

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 434223 Color: 17
Size: 315119 Color: 11
Size: 250659 Color: 17

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 435237 Color: 15
Size: 291652 Color: 7
Size: 273112 Color: 6

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 435500 Color: 19
Size: 308987 Color: 7
Size: 255514 Color: 10

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 435655 Color: 10
Size: 313283 Color: 1
Size: 251063 Color: 11

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 435659 Color: 17
Size: 310434 Color: 8
Size: 253908 Color: 7

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 435695 Color: 13
Size: 307175 Color: 13
Size: 257131 Color: 2

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 435782 Color: 1
Size: 303241 Color: 18
Size: 260978 Color: 18

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 435796 Color: 14
Size: 292026 Color: 14
Size: 272179 Color: 17

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 435910 Color: 8
Size: 304892 Color: 5
Size: 259199 Color: 13

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 436274 Color: 19
Size: 301874 Color: 17
Size: 261853 Color: 16

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 436626 Color: 16
Size: 300144 Color: 5
Size: 263231 Color: 6

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 437033 Color: 8
Size: 312588 Color: 11
Size: 250380 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 437075 Color: 4
Size: 293256 Color: 2
Size: 269670 Color: 11

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 437085 Color: 14
Size: 291159 Color: 17
Size: 271757 Color: 2

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 437663 Color: 8
Size: 283468 Color: 12
Size: 278870 Color: 4

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 437685 Color: 17
Size: 306673 Color: 5
Size: 255643 Color: 4

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 437707 Color: 14
Size: 286586 Color: 10
Size: 275708 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 437755 Color: 16
Size: 291184 Color: 10
Size: 271062 Color: 4

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 438282 Color: 19
Size: 300894 Color: 7
Size: 260825 Color: 9

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 438293 Color: 19
Size: 304308 Color: 19
Size: 257400 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 438310 Color: 19
Size: 304935 Color: 12
Size: 256756 Color: 8

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 438577 Color: 10
Size: 303767 Color: 11
Size: 257657 Color: 4

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 439535 Color: 19
Size: 309457 Color: 17
Size: 251009 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 439838 Color: 3
Size: 305863 Color: 4
Size: 254300 Color: 9

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 439597 Color: 0
Size: 301612 Color: 8
Size: 258792 Color: 2

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 440079 Color: 13
Size: 308612 Color: 7
Size: 251310 Color: 5

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 440241 Color: 2
Size: 302942 Color: 14
Size: 256818 Color: 13

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 440372 Color: 7
Size: 303093 Color: 7
Size: 256536 Color: 15

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 441436 Color: 19
Size: 279482 Color: 5
Size: 279083 Color: 8

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 441848 Color: 6
Size: 284680 Color: 16
Size: 273473 Color: 4

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 441945 Color: 18
Size: 296645 Color: 14
Size: 261411 Color: 17

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 441997 Color: 19
Size: 298648 Color: 16
Size: 259356 Color: 15

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 442429 Color: 2
Size: 289204 Color: 9
Size: 268368 Color: 19

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 442603 Color: 0
Size: 285434 Color: 9
Size: 271964 Color: 11

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 443443 Color: 13
Size: 280362 Color: 7
Size: 276196 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 443554 Color: 0
Size: 300479 Color: 4
Size: 255968 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 443931 Color: 4
Size: 289654 Color: 3
Size: 266416 Color: 8

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 444354 Color: 4
Size: 303748 Color: 13
Size: 251899 Color: 16

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 444440 Color: 1
Size: 299585 Color: 13
Size: 255976 Color: 9

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 444741 Color: 18
Size: 279987 Color: 6
Size: 275273 Color: 4

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 444755 Color: 11
Size: 297253 Color: 9
Size: 257993 Color: 13

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 444993 Color: 11
Size: 284273 Color: 19
Size: 270735 Color: 16

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 445488 Color: 13
Size: 286440 Color: 17
Size: 268073 Color: 14

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 445488 Color: 7
Size: 294200 Color: 11
Size: 260313 Color: 18

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 445614 Color: 13
Size: 299316 Color: 15
Size: 255071 Color: 9

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 445961 Color: 16
Size: 288943 Color: 15
Size: 265097 Color: 15

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 445963 Color: 17
Size: 301857 Color: 11
Size: 252181 Color: 19

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 446256 Color: 17
Size: 277062 Color: 10
Size: 276683 Color: 15

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 446274 Color: 6
Size: 298925 Color: 4
Size: 254802 Color: 5

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 446354 Color: 0
Size: 277765 Color: 15
Size: 275882 Color: 11

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 446611 Color: 0
Size: 297150 Color: 6
Size: 256240 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 447850 Color: 4
Size: 299037 Color: 2
Size: 253114 Color: 11

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 448030 Color: 18
Size: 300209 Color: 5
Size: 251762 Color: 14

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 448721 Color: 18
Size: 279551 Color: 17
Size: 271729 Color: 7

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 448917 Color: 5
Size: 297404 Color: 7
Size: 253680 Color: 6

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 448926 Color: 5
Size: 290452 Color: 3
Size: 260623 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 449393 Color: 19
Size: 291948 Color: 5
Size: 258660 Color: 14

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 449342 Color: 8
Size: 298182 Color: 5
Size: 252477 Color: 12

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 449435 Color: 5
Size: 292793 Color: 8
Size: 257773 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 450240 Color: 12
Size: 284324 Color: 5
Size: 265437 Color: 17

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 450443 Color: 11
Size: 283756 Color: 8
Size: 265802 Color: 18

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 450535 Color: 16
Size: 284735 Color: 10
Size: 264731 Color: 18

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 450595 Color: 13
Size: 284189 Color: 15
Size: 265217 Color: 17

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 452069 Color: 19
Size: 281633 Color: 8
Size: 266299 Color: 11

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 451503 Color: 13
Size: 287695 Color: 5
Size: 260803 Color: 2

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 451615 Color: 14
Size: 291493 Color: 7
Size: 256893 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 451862 Color: 1
Size: 288631 Color: 0
Size: 259508 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 452188 Color: 19
Size: 278514 Color: 16
Size: 269299 Color: 10

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 453542 Color: 5
Size: 287989 Color: 17
Size: 258470 Color: 2

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 453728 Color: 10
Size: 283485 Color: 14
Size: 262788 Color: 13

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 453814 Color: 6
Size: 285406 Color: 16
Size: 260781 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 454681 Color: 17
Size: 295284 Color: 2
Size: 250036 Color: 8

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 454707 Color: 16
Size: 290340 Color: 18
Size: 254954 Color: 11

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 454724 Color: 13
Size: 286120 Color: 8
Size: 259157 Color: 4

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 454786 Color: 5
Size: 278570 Color: 10
Size: 266645 Color: 5

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 454877 Color: 10
Size: 277395 Color: 15
Size: 267729 Color: 14

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 454946 Color: 0
Size: 279369 Color: 10
Size: 265686 Color: 4

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 455074 Color: 8
Size: 289943 Color: 16
Size: 254984 Color: 15

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 455108 Color: 11
Size: 294667 Color: 7
Size: 250226 Color: 3

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 455143 Color: 18
Size: 281440 Color: 9
Size: 263418 Color: 13

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 455567 Color: 11
Size: 290562 Color: 10
Size: 253872 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 456138 Color: 4
Size: 284629 Color: 17
Size: 259234 Color: 11

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 456810 Color: 6
Size: 271715 Color: 16
Size: 271476 Color: 8

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 457243 Color: 19
Size: 290039 Color: 13
Size: 252719 Color: 8

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 457295 Color: 3
Size: 272244 Color: 1
Size: 270462 Color: 15

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 458096 Color: 10
Size: 280808 Color: 8
Size: 261097 Color: 8

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 459465 Color: 2
Size: 288072 Color: 7
Size: 252464 Color: 8

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 459795 Color: 8
Size: 279322 Color: 1
Size: 260884 Color: 12

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 460610 Color: 15
Size: 280281 Color: 3
Size: 259110 Color: 11

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 460647 Color: 0
Size: 281747 Color: 5
Size: 257607 Color: 16

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 460884 Color: 18
Size: 287914 Color: 6
Size: 251203 Color: 9

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 461235 Color: 12
Size: 269769 Color: 10
Size: 268997 Color: 17

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 461719 Color: 12
Size: 288119 Color: 3
Size: 250163 Color: 4

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 461852 Color: 7
Size: 269912 Color: 14
Size: 268237 Color: 16

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 462468 Color: 18
Size: 270741 Color: 18
Size: 266792 Color: 5

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 462469 Color: 14
Size: 273860 Color: 8
Size: 263672 Color: 3

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 462637 Color: 10
Size: 282578 Color: 11
Size: 254786 Color: 17

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 463945 Color: 15
Size: 269554 Color: 14
Size: 266502 Color: 13

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 464278 Color: 8
Size: 284692 Color: 16
Size: 251031 Color: 8

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 464465 Color: 2
Size: 278282 Color: 15
Size: 257254 Color: 14

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 464724 Color: 13
Size: 268550 Color: 17
Size: 266727 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 464860 Color: 1
Size: 268257 Color: 13
Size: 266884 Color: 5

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 464975 Color: 16
Size: 282861 Color: 4
Size: 252165 Color: 18

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 465568 Color: 17
Size: 280062 Color: 0
Size: 254371 Color: 12

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 465702 Color: 9
Size: 269557 Color: 15
Size: 264742 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 465918 Color: 15
Size: 276407 Color: 14
Size: 257676 Color: 19

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 466382 Color: 13
Size: 275378 Color: 9
Size: 258241 Color: 9

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 466600 Color: 5
Size: 269525 Color: 0
Size: 263876 Color: 4

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 466902 Color: 0
Size: 279179 Color: 12
Size: 253920 Color: 15

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 467159 Color: 12
Size: 276853 Color: 5
Size: 255989 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 467181 Color: 6
Size: 282268 Color: 8
Size: 250552 Color: 16

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 467559 Color: 3
Size: 271271 Color: 7
Size: 261171 Color: 6

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 467786 Color: 16
Size: 280675 Color: 16
Size: 251540 Color: 7

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 468588 Color: 19
Size: 276880 Color: 15
Size: 254533 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 468181 Color: 1
Size: 266098 Color: 7
Size: 265722 Color: 4

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 468248 Color: 12
Size: 272050 Color: 11
Size: 259703 Color: 9

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 468656 Color: 14
Size: 270185 Color: 6
Size: 261160 Color: 18

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 469168 Color: 3
Size: 269089 Color: 7
Size: 261744 Color: 15

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 469324 Color: 11
Size: 280151 Color: 15
Size: 250526 Color: 3

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 470374 Color: 19
Size: 272571 Color: 16
Size: 257056 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 470370 Color: 14
Size: 270933 Color: 5
Size: 258698 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 470550 Color: 2
Size: 274592 Color: 1
Size: 254859 Color: 5

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 471583 Color: 4
Size: 271199 Color: 10
Size: 257219 Color: 3

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 472355 Color: 19
Size: 276820 Color: 15
Size: 250826 Color: 4

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 472233 Color: 5
Size: 266258 Color: 14
Size: 261510 Color: 4

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 472438 Color: 7
Size: 277526 Color: 1
Size: 250037 Color: 11

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 472937 Color: 14
Size: 273230 Color: 11
Size: 253834 Color: 13

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 473570 Color: 2
Size: 270832 Color: 12
Size: 255599 Color: 1

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 473780 Color: 16
Size: 270230 Color: 1
Size: 255991 Color: 15

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 473912 Color: 2
Size: 274092 Color: 6
Size: 251997 Color: 3

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 474028 Color: 4
Size: 275058 Color: 6
Size: 250915 Color: 18

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 474089 Color: 7
Size: 271810 Color: 17
Size: 254102 Color: 2

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 474238 Color: 15
Size: 275493 Color: 11
Size: 250270 Color: 17

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 474340 Color: 15
Size: 275137 Color: 14
Size: 250524 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 475000 Color: 13
Size: 266293 Color: 13
Size: 258708 Color: 15

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 475350 Color: 19
Size: 274552 Color: 18
Size: 250099 Color: 17

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 475149 Color: 15
Size: 265760 Color: 12
Size: 259092 Color: 16

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 475536 Color: 1
Size: 271186 Color: 13
Size: 253279 Color: 15

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 476084 Color: 13
Size: 268405 Color: 1
Size: 255512 Color: 6

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 476195 Color: 1
Size: 272469 Color: 7
Size: 251337 Color: 3

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 476452 Color: 5
Size: 265111 Color: 7
Size: 258438 Color: 10

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 476695 Color: 10
Size: 262643 Color: 3
Size: 260663 Color: 3

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 476751 Color: 8
Size: 263615 Color: 18
Size: 259635 Color: 17

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 477053 Color: 5
Size: 265777 Color: 17
Size: 257171 Color: 18

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 477079 Color: 10
Size: 269569 Color: 4
Size: 253353 Color: 19

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 478280 Color: 14
Size: 269984 Color: 12
Size: 251737 Color: 7

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 478526 Color: 6
Size: 269207 Color: 12
Size: 252268 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 478574 Color: 1
Size: 267426 Color: 13
Size: 254001 Color: 6

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 479235 Color: 1
Size: 267349 Color: 2
Size: 253417 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 479281 Color: 3
Size: 267925 Color: 11
Size: 252795 Color: 10

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 479624 Color: 14
Size: 269796 Color: 10
Size: 250581 Color: 16

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 479627 Color: 8
Size: 262466 Color: 1
Size: 257908 Color: 3

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 480530 Color: 19
Size: 262281 Color: 12
Size: 257190 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 479890 Color: 3
Size: 269876 Color: 4
Size: 250235 Color: 8

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 480417 Color: 15
Size: 263989 Color: 6
Size: 255595 Color: 1

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 480445 Color: 13
Size: 269094 Color: 12
Size: 250462 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 480618 Color: 2
Size: 266932 Color: 4
Size: 252451 Color: 10

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 481190 Color: 19
Size: 262279 Color: 14
Size: 256532 Color: 14

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 480735 Color: 1
Size: 260370 Color: 3
Size: 258896 Color: 18

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 481158 Color: 15
Size: 264840 Color: 10
Size: 254003 Color: 15

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 481414 Color: 10
Size: 261029 Color: 7
Size: 257558 Color: 7

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 482911 Color: 8
Size: 263320 Color: 11
Size: 253770 Color: 6

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 483761 Color: 14
Size: 263720 Color: 0
Size: 252520 Color: 6

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 484003 Color: 4
Size: 265886 Color: 11
Size: 250112 Color: 17

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 484331 Color: 5
Size: 261165 Color: 17
Size: 254505 Color: 14

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 484970 Color: 19
Size: 264616 Color: 8
Size: 250415 Color: 11

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 484945 Color: 10
Size: 262845 Color: 7
Size: 252211 Color: 13

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 484952 Color: 7
Size: 259204 Color: 0
Size: 255845 Color: 18

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 485237 Color: 15
Size: 260620 Color: 14
Size: 254144 Color: 16

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 485313 Color: 0
Size: 257417 Color: 2
Size: 257271 Color: 11

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 485753 Color: 19
Size: 262522 Color: 12
Size: 251726 Color: 18

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 485788 Color: 0
Size: 257764 Color: 15
Size: 256449 Color: 14

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 485982 Color: 14
Size: 257100 Color: 5
Size: 256919 Color: 16

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 486589 Color: 12
Size: 257646 Color: 18
Size: 255766 Color: 9

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 487936 Color: 4
Size: 261001 Color: 17
Size: 251064 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 488249 Color: 6
Size: 261444 Color: 12
Size: 250308 Color: 18

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 489276 Color: 19
Size: 255977 Color: 7
Size: 254748 Color: 8

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 489267 Color: 8
Size: 257833 Color: 5
Size: 252901 Color: 4

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 489573 Color: 10
Size: 260351 Color: 6
Size: 250077 Color: 14

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 490129 Color: 8
Size: 255257 Color: 12
Size: 254615 Color: 11

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 490967 Color: 2
Size: 256220 Color: 2
Size: 252814 Color: 1

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 491655 Color: 4
Size: 254223 Color: 2
Size: 254123 Color: 9

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 494571 Color: 13
Size: 254304 Color: 7
Size: 251126 Color: 12

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 495154 Color: 18
Size: 254040 Color: 2
Size: 250807 Color: 9

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 495231 Color: 15
Size: 252867 Color: 10
Size: 251903 Color: 3

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 495541 Color: 15
Size: 252249 Color: 4
Size: 252211 Color: 19

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 495576 Color: 3
Size: 254069 Color: 15
Size: 250356 Color: 6

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 496094 Color: 3
Size: 253063 Color: 8
Size: 250844 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 496302 Color: 9
Size: 253031 Color: 16
Size: 250668 Color: 14

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 496496 Color: 15
Size: 253044 Color: 6
Size: 250461 Color: 13

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 496776 Color: 4
Size: 253116 Color: 12
Size: 250109 Color: 8

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 497066 Color: 15
Size: 252328 Color: 11
Size: 250607 Color: 14

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 497872 Color: 19
Size: 251774 Color: 3
Size: 250355 Color: 8

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 497373 Color: 2
Size: 251712 Color: 17
Size: 250916 Color: 9

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 497478 Color: 14
Size: 252515 Color: 13
Size: 250008 Color: 6

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 497952 Color: 17
Size: 251597 Color: 19
Size: 250452 Color: 10

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 360289 Color: 4
Size: 332724 Color: 17
Size: 306987 Color: 1

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 361608 Color: 3
Size: 333128 Color: 8
Size: 305264 Color: 13

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 363057 Color: 14
Size: 334089 Color: 6
Size: 302854 Color: 15

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 364444 Color: 18
Size: 338909 Color: 6
Size: 296647 Color: 7

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 363991 Color: 16
Size: 339884 Color: 16
Size: 296125 Color: 13

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 368651 Color: 16
Size: 319888 Color: 7
Size: 311461 Color: 19

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 368800 Color: 0
Size: 355707 Color: 18
Size: 275493 Color: 4

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 370954 Color: 5
Size: 366832 Color: 10
Size: 262214 Color: 13

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 371129 Color: 4
Size: 363833 Color: 15
Size: 265038 Color: 12

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 371390 Color: 15
Size: 333542 Color: 14
Size: 295068 Color: 18

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 372716 Color: 2
Size: 346757 Color: 19
Size: 280527 Color: 5

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 374309 Color: 7
Size: 343593 Color: 4
Size: 282098 Color: 8

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 376963 Color: 3
Size: 332888 Color: 18
Size: 290149 Color: 10

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 377442 Color: 12
Size: 314149 Color: 17
Size: 308409 Color: 18

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 378118 Color: 19
Size: 325257 Color: 4
Size: 296625 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 380853 Color: 4
Size: 346791 Color: 15
Size: 272356 Color: 9

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 380151 Color: 0
Size: 322853 Color: 19
Size: 296996 Color: 12

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 380294 Color: 6
Size: 313329 Color: 15
Size: 306377 Color: 10

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 381744 Color: 8
Size: 363181 Color: 15
Size: 255075 Color: 9

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 383426 Color: 9
Size: 314658 Color: 10
Size: 301916 Color: 19

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 385509 Color: 8
Size: 353473 Color: 12
Size: 261018 Color: 17

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 386263 Color: 13
Size: 347875 Color: 9
Size: 265862 Color: 6

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 388021 Color: 18
Size: 350644 Color: 13
Size: 261335 Color: 4

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 388273 Color: 9
Size: 360173 Color: 1
Size: 251554 Color: 0

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 390017 Color: 8
Size: 341259 Color: 7
Size: 268724 Color: 0

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 390589 Color: 7
Size: 317820 Color: 13
Size: 291591 Color: 5

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 390967 Color: 5
Size: 337220 Color: 12
Size: 271813 Color: 0

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 390985 Color: 9
Size: 357289 Color: 18
Size: 251726 Color: 3

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 391213 Color: 7
Size: 326232 Color: 5
Size: 282555 Color: 14

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 392299 Color: 6
Size: 346833 Color: 16
Size: 260868 Color: 8

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 392362 Color: 15
Size: 309785 Color: 14
Size: 297853 Color: 15

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 392729 Color: 13
Size: 314283 Color: 7
Size: 292988 Color: 11

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 395103 Color: 17
Size: 309979 Color: 10
Size: 294918 Color: 12

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 395876 Color: 6
Size: 316484 Color: 5
Size: 287640 Color: 17

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 395893 Color: 7
Size: 333393 Color: 16
Size: 270714 Color: 17

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 396391 Color: 2
Size: 344626 Color: 9
Size: 258983 Color: 13

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 396489 Color: 6
Size: 312625 Color: 18
Size: 290886 Color: 8

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 399475 Color: 6
Size: 349553 Color: 7
Size: 250972 Color: 0

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 403036 Color: 1
Size: 317441 Color: 9
Size: 279523 Color: 14

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 404549 Color: 6
Size: 320145 Color: 5
Size: 275306 Color: 14

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 407389 Color: 13
Size: 327555 Color: 14
Size: 265056 Color: 9

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 408820 Color: 16
Size: 316986 Color: 12
Size: 274194 Color: 10

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 408951 Color: 13
Size: 318339 Color: 18
Size: 272710 Color: 19

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 412160 Color: 4
Size: 299228 Color: 7
Size: 288612 Color: 0

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 413035 Color: 8
Size: 295810 Color: 9
Size: 291155 Color: 10

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 413895 Color: 0
Size: 307251 Color: 12
Size: 278854 Color: 16

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 415630 Color: 10
Size: 303935 Color: 9
Size: 280435 Color: 1

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 415855 Color: 9
Size: 295818 Color: 4
Size: 288327 Color: 5

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 416025 Color: 5
Size: 333282 Color: 15
Size: 250693 Color: 6

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 417224 Color: 1
Size: 307506 Color: 10
Size: 275270 Color: 11

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 418132 Color: 6
Size: 308929 Color: 18
Size: 272939 Color: 1

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 418964 Color: 18
Size: 307380 Color: 14
Size: 273656 Color: 8

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 420292 Color: 3
Size: 308129 Color: 17
Size: 271579 Color: 14

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 420430 Color: 3
Size: 299135 Color: 5
Size: 280435 Color: 18

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 424064 Color: 15
Size: 314185 Color: 19
Size: 261751 Color: 3

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 425312 Color: 14
Size: 305450 Color: 0
Size: 269238 Color: 4

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 425441 Color: 4
Size: 297605 Color: 6
Size: 276954 Color: 10

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 431437 Color: 6
Size: 299281 Color: 13
Size: 269282 Color: 13

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 431906 Color: 14
Size: 300392 Color: 0
Size: 267702 Color: 3

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 434956 Color: 0
Size: 313749 Color: 10
Size: 251295 Color: 3

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 440470 Color: 15
Size: 279895 Color: 3
Size: 279635 Color: 7

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 454826 Color: 13
Size: 273244 Color: 18
Size: 271930 Color: 19

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 455168 Color: 18
Size: 280659 Color: 17
Size: 264173 Color: 15

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 461537 Color: 7
Size: 282926 Color: 8
Size: 255537 Color: 19

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 476230 Color: 4
Size: 266349 Color: 0
Size: 257421 Color: 19

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 481258 Color: 1
Size: 260564 Color: 19
Size: 258178 Color: 5

Bin 484: 1 of cap free
Amount of items: 3
Items: 
Size: 485215 Color: 15
Size: 257416 Color: 15
Size: 257369 Color: 7

Bin 485: 1 of cap free
Amount of items: 3
Items: 
Size: 488191 Color: 2
Size: 256125 Color: 1
Size: 255684 Color: 15

Bin 486: 1 of cap free
Amount of items: 3
Items: 
Size: 488849 Color: 13
Size: 261011 Color: 18
Size: 250140 Color: 8

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 360987 Color: 8
Size: 322858 Color: 14
Size: 316154 Color: 18

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 368008 Color: 10
Size: 319602 Color: 13
Size: 312389 Color: 15

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 369411 Color: 2
Size: 333731 Color: 15
Size: 296857 Color: 10

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 371881 Color: 19
Size: 328220 Color: 11
Size: 299898 Color: 17

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 372606 Color: 13
Size: 357013 Color: 19
Size: 270380 Color: 2

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 372867 Color: 17
Size: 349004 Color: 11
Size: 278128 Color: 4

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 375356 Color: 3
Size: 343419 Color: 16
Size: 281224 Color: 18

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 374824 Color: 13
Size: 328703 Color: 2
Size: 296472 Color: 17

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 375587 Color: 19
Size: 331476 Color: 0
Size: 292936 Color: 19

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 379746 Color: 12
Size: 321637 Color: 7
Size: 298616 Color: 14

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 384612 Color: 4
Size: 341371 Color: 2
Size: 274016 Color: 0

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 384733 Color: 6
Size: 335084 Color: 13
Size: 280182 Color: 18

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 385831 Color: 18
Size: 326557 Color: 9
Size: 287611 Color: 14

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 388408 Color: 2
Size: 308919 Color: 13
Size: 302672 Color: 3

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 389503 Color: 0
Size: 329640 Color: 2
Size: 280856 Color: 7

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 394015 Color: 11
Size: 306486 Color: 7
Size: 299498 Color: 9

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 397937 Color: 12
Size: 335961 Color: 12
Size: 266101 Color: 2

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 399239 Color: 4
Size: 318556 Color: 10
Size: 282204 Color: 19

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 399311 Color: 17
Size: 330350 Color: 1
Size: 270338 Color: 14

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 419203 Color: 8
Size: 291102 Color: 14
Size: 289694 Color: 10

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 426651 Color: 0
Size: 309457 Color: 1
Size: 263891 Color: 3

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 427608 Color: 19
Size: 287269 Color: 5
Size: 285122 Color: 1

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 427971 Color: 17
Size: 288564 Color: 3
Size: 283464 Color: 19

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 361954 Color: 10
Size: 345099 Color: 18
Size: 292945 Color: 15

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 369418 Color: 8
Size: 344582 Color: 7
Size: 285998 Color: 12

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 374353 Color: 9
Size: 322742 Color: 12
Size: 302903 Color: 14

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 375379 Color: 11
Size: 338249 Color: 15
Size: 286370 Color: 7

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 379264 Color: 11
Size: 344101 Color: 6
Size: 276633 Color: 16

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 379370 Color: 8
Size: 359994 Color: 17
Size: 260634 Color: 14

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 381353 Color: 2
Size: 343266 Color: 7
Size: 275379 Color: 13

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 381455 Color: 8
Size: 362981 Color: 19
Size: 255562 Color: 3

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 383257 Color: 9
Size: 346462 Color: 10
Size: 270279 Color: 14

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 401741 Color: 16
Size: 342152 Color: 8
Size: 256105 Color: 10

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 404637 Color: 5
Size: 325672 Color: 19
Size: 269689 Color: 3

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 409513 Color: 19
Size: 302312 Color: 18
Size: 288173 Color: 2

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 411446 Color: 17
Size: 335166 Color: 3
Size: 253386 Color: 8

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 437134 Color: 10
Size: 291881 Color: 3
Size: 270983 Color: 0

Bin 524: 3 of cap free
Amount of items: 3
Items: 
Size: 442100 Color: 19
Size: 301236 Color: 3
Size: 256662 Color: 15

Bin 525: 3 of cap free
Amount of items: 3
Items: 
Size: 464853 Color: 4
Size: 280499 Color: 19
Size: 254646 Color: 15

Bin 526: 3 of cap free
Amount of items: 3
Items: 
Size: 497268 Color: 7
Size: 252698 Color: 15
Size: 250032 Color: 0

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 366382 Color: 19
Size: 317972 Color: 7
Size: 315643 Color: 2

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 367205 Color: 3
Size: 339460 Color: 6
Size: 293332 Color: 12

Bin 529: 4 of cap free
Amount of items: 3
Items: 
Size: 374163 Color: 3
Size: 347106 Color: 1
Size: 278728 Color: 15

Bin 530: 4 of cap free
Amount of items: 3
Items: 
Size: 381158 Color: 6
Size: 354204 Color: 12
Size: 264635 Color: 3

Bin 531: 4 of cap free
Amount of items: 3
Items: 
Size: 385000 Color: 13
Size: 319961 Color: 4
Size: 295036 Color: 17

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 389908 Color: 18
Size: 309162 Color: 4
Size: 300927 Color: 3

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 393800 Color: 7
Size: 344713 Color: 0
Size: 261484 Color: 10

Bin 534: 4 of cap free
Amount of items: 3
Items: 
Size: 433107 Color: 1
Size: 285640 Color: 6
Size: 281250 Color: 3

Bin 535: 4 of cap free
Amount of items: 3
Items: 
Size: 438033 Color: 15
Size: 308598 Color: 3
Size: 253366 Color: 16

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 455158 Color: 5
Size: 293894 Color: 9
Size: 250945 Color: 19

Bin 537: 4 of cap free
Amount of items: 3
Items: 
Size: 472889 Color: 3
Size: 276255 Color: 19
Size: 250853 Color: 3

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 473930 Color: 9
Size: 270200 Color: 19
Size: 255867 Color: 8

Bin 539: 5 of cap free
Amount of items: 3
Items: 
Size: 360857 Color: 15
Size: 346272 Color: 2
Size: 292867 Color: 6

Bin 540: 5 of cap free
Amount of items: 3
Items: 
Size: 364969 Color: 15
Size: 354204 Color: 17
Size: 280823 Color: 10

Bin 541: 5 of cap free
Amount of items: 3
Items: 
Size: 368709 Color: 14
Size: 336173 Color: 2
Size: 295114 Color: 10

Bin 542: 5 of cap free
Amount of items: 3
Items: 
Size: 369055 Color: 3
Size: 351386 Color: 18
Size: 279555 Color: 7

Bin 543: 5 of cap free
Amount of items: 3
Items: 
Size: 370905 Color: 14
Size: 359680 Color: 12
Size: 269411 Color: 0

Bin 544: 5 of cap free
Amount of items: 3
Items: 
Size: 378889 Color: 0
Size: 350494 Color: 11
Size: 270613 Color: 3

Bin 545: 5 of cap free
Amount of items: 3
Items: 
Size: 381075 Color: 10
Size: 319561 Color: 11
Size: 299360 Color: 13

Bin 546: 5 of cap free
Amount of items: 3
Items: 
Size: 383026 Color: 7
Size: 318820 Color: 11
Size: 298150 Color: 14

Bin 547: 5 of cap free
Amount of items: 3
Items: 
Size: 395301 Color: 18
Size: 320158 Color: 19
Size: 284537 Color: 12

Bin 548: 5 of cap free
Amount of items: 3
Items: 
Size: 400884 Color: 14
Size: 346279 Color: 3
Size: 252833 Color: 12

Bin 549: 5 of cap free
Amount of items: 3
Items: 
Size: 445001 Color: 19
Size: 295954 Color: 13
Size: 259041 Color: 3

Bin 550: 6 of cap free
Amount of items: 3
Items: 
Size: 359003 Color: 18
Size: 321688 Color: 16
Size: 319304 Color: 9

Bin 551: 6 of cap free
Amount of items: 3
Items: 
Size: 370555 Color: 5
Size: 340761 Color: 4
Size: 288679 Color: 19

Bin 552: 6 of cap free
Amount of items: 3
Items: 
Size: 370942 Color: 5
Size: 336595 Color: 17
Size: 292458 Color: 10

Bin 553: 6 of cap free
Amount of items: 3
Items: 
Size: 377809 Color: 1
Size: 347750 Color: 3
Size: 274436 Color: 4

Bin 554: 6 of cap free
Amount of items: 3
Items: 
Size: 425694 Color: 6
Size: 305627 Color: 13
Size: 268674 Color: 3

Bin 555: 6 of cap free
Amount of items: 3
Items: 
Size: 460539 Color: 18
Size: 289137 Color: 19
Size: 250319 Color: 14

Bin 556: 7 of cap free
Amount of items: 3
Items: 
Size: 334490 Color: 2
Size: 333595 Color: 18
Size: 331909 Color: 8

Bin 557: 7 of cap free
Amount of items: 3
Items: 
Size: 366461 Color: 10
Size: 342582 Color: 14
Size: 290951 Color: 16

Bin 558: 7 of cap free
Amount of items: 3
Items: 
Size: 372778 Color: 17
Size: 362463 Color: 13
Size: 264753 Color: 1

Bin 559: 7 of cap free
Amount of items: 3
Items: 
Size: 377187 Color: 0
Size: 316807 Color: 11
Size: 306000 Color: 11

Bin 560: 7 of cap free
Amount of items: 3
Items: 
Size: 391495 Color: 14
Size: 342062 Color: 9
Size: 266437 Color: 3

Bin 561: 8 of cap free
Amount of items: 3
Items: 
Size: 362817 Color: 5
Size: 354551 Color: 15
Size: 282625 Color: 1

Bin 562: 8 of cap free
Amount of items: 3
Items: 
Size: 364371 Color: 12
Size: 320606 Color: 14
Size: 315016 Color: 19

Bin 563: 8 of cap free
Amount of items: 3
Items: 
Size: 368380 Color: 10
Size: 362863 Color: 9
Size: 268750 Color: 0

Bin 564: 8 of cap free
Amount of items: 3
Items: 
Size: 369566 Color: 5
Size: 340702 Color: 15
Size: 289725 Color: 4

Bin 565: 8 of cap free
Amount of items: 3
Items: 
Size: 371541 Color: 6
Size: 370032 Color: 5
Size: 258420 Color: 4

Bin 566: 8 of cap free
Amount of items: 3
Items: 
Size: 387137 Color: 15
Size: 335066 Color: 3
Size: 277790 Color: 5

Bin 567: 8 of cap free
Amount of items: 3
Items: 
Size: 396134 Color: 11
Size: 316078 Color: 12
Size: 287781 Color: 3

Bin 568: 8 of cap free
Amount of items: 3
Items: 
Size: 398533 Color: 10
Size: 338978 Color: 15
Size: 262482 Color: 3

Bin 569: 8 of cap free
Amount of items: 3
Items: 
Size: 491174 Color: 2
Size: 256521 Color: 5
Size: 252298 Color: 19

Bin 570: 9 of cap free
Amount of items: 3
Items: 
Size: 361127 Color: 7
Size: 349732 Color: 9
Size: 289133 Color: 9

Bin 571: 9 of cap free
Amount of items: 3
Items: 
Size: 364071 Color: 12
Size: 320105 Color: 9
Size: 315816 Color: 12

Bin 572: 9 of cap free
Amount of items: 3
Items: 
Size: 364124 Color: 10
Size: 322256 Color: 6
Size: 313612 Color: 8

Bin 573: 9 of cap free
Amount of items: 3
Items: 
Size: 379801 Color: 13
Size: 355062 Color: 13
Size: 265129 Color: 6

Bin 574: 9 of cap free
Amount of items: 3
Items: 
Size: 381018 Color: 2
Size: 366919 Color: 4
Size: 252055 Color: 8

Bin 575: 9 of cap free
Amount of items: 3
Items: 
Size: 454095 Color: 15
Size: 292243 Color: 19
Size: 253654 Color: 16

Bin 576: 9 of cap free
Amount of items: 3
Items: 
Size: 487472 Color: 8
Size: 257890 Color: 19
Size: 254630 Color: 2

Bin 577: 10 of cap free
Amount of items: 3
Items: 
Size: 366265 Color: 2
Size: 324353 Color: 9
Size: 309373 Color: 11

Bin 578: 10 of cap free
Amount of items: 3
Items: 
Size: 378158 Color: 7
Size: 311515 Color: 14
Size: 310318 Color: 9

Bin 579: 10 of cap free
Amount of items: 3
Items: 
Size: 394930 Color: 18
Size: 323552 Color: 11
Size: 281509 Color: 3

Bin 580: 10 of cap free
Amount of items: 3
Items: 
Size: 404048 Color: 11
Size: 311195 Color: 19
Size: 284748 Color: 3

Bin 581: 10 of cap free
Amount of items: 3
Items: 
Size: 447232 Color: 5
Size: 302689 Color: 19
Size: 250070 Color: 9

Bin 582: 11 of cap free
Amount of items: 3
Items: 
Size: 364835 Color: 7
Size: 333834 Color: 4
Size: 301321 Color: 11

Bin 583: 12 of cap free
Amount of items: 3
Items: 
Size: 363886 Color: 11
Size: 335983 Color: 11
Size: 300120 Color: 17

Bin 584: 12 of cap free
Amount of items: 3
Items: 
Size: 371490 Color: 4
Size: 367717 Color: 9
Size: 260782 Color: 4

Bin 585: 12 of cap free
Amount of items: 3
Items: 
Size: 373375 Color: 10
Size: 347524 Color: 5
Size: 279090 Color: 10

Bin 586: 12 of cap free
Amount of items: 3
Items: 
Size: 392708 Color: 7
Size: 327340 Color: 16
Size: 279941 Color: 3

Bin 587: 13 of cap free
Amount of items: 3
Items: 
Size: 364918 Color: 3
Size: 322726 Color: 2
Size: 312344 Color: 5

Bin 588: 13 of cap free
Amount of items: 3
Items: 
Size: 366555 Color: 3
Size: 355655 Color: 1
Size: 277778 Color: 16

Bin 589: 13 of cap free
Amount of items: 3
Items: 
Size: 370511 Color: 3
Size: 366463 Color: 8
Size: 263014 Color: 16

Bin 590: 14 of cap free
Amount of items: 3
Items: 
Size: 362543 Color: 2
Size: 328385 Color: 8
Size: 309059 Color: 12

Bin 591: 14 of cap free
Amount of items: 3
Items: 
Size: 365423 Color: 1
Size: 354351 Color: 14
Size: 280213 Color: 17

Bin 592: 14 of cap free
Amount of items: 3
Items: 
Size: 370339 Color: 17
Size: 352881 Color: 11
Size: 276767 Color: 3

Bin 593: 14 of cap free
Amount of items: 3
Items: 
Size: 463842 Color: 12
Size: 274539 Color: 19
Size: 261606 Color: 4

Bin 594: 15 of cap free
Amount of items: 3
Items: 
Size: 359709 Color: 17
Size: 359373 Color: 18
Size: 280904 Color: 0

Bin 595: 15 of cap free
Amount of items: 3
Items: 
Size: 385418 Color: 7
Size: 361613 Color: 3
Size: 252955 Color: 17

Bin 596: 17 of cap free
Amount of items: 3
Items: 
Size: 405627 Color: 13
Size: 329676 Color: 5
Size: 264681 Color: 3

Bin 597: 17 of cap free
Amount of items: 3
Items: 
Size: 436502 Color: 0
Size: 307681 Color: 3
Size: 255801 Color: 15

Bin 598: 18 of cap free
Amount of items: 3
Items: 
Size: 381905 Color: 3
Size: 340788 Color: 4
Size: 277290 Color: 14

Bin 599: 19 of cap free
Amount of items: 3
Items: 
Size: 385868 Color: 5
Size: 347199 Color: 3
Size: 266915 Color: 4

Bin 600: 21 of cap free
Amount of items: 3
Items: 
Size: 362242 Color: 0
Size: 332377 Color: 16
Size: 305361 Color: 11

Bin 601: 21 of cap free
Amount of items: 3
Items: 
Size: 399528 Color: 12
Size: 304384 Color: 6
Size: 296068 Color: 3

Bin 602: 21 of cap free
Amount of items: 3
Items: 
Size: 435760 Color: 0
Size: 301798 Color: 1
Size: 262422 Color: 3

Bin 603: 23 of cap free
Amount of items: 3
Items: 
Size: 365483 Color: 5
Size: 318551 Color: 0
Size: 315944 Color: 17

Bin 604: 23 of cap free
Amount of items: 3
Items: 
Size: 372159 Color: 8
Size: 344158 Color: 9
Size: 283661 Color: 8

Bin 605: 24 of cap free
Amount of items: 3
Items: 
Size: 366939 Color: 2
Size: 352216 Color: 10
Size: 280822 Color: 9

Bin 606: 25 of cap free
Amount of items: 3
Items: 
Size: 361509 Color: 5
Size: 346699 Color: 0
Size: 291768 Color: 4

Bin 607: 26 of cap free
Amount of items: 3
Items: 
Size: 367449 Color: 3
Size: 350189 Color: 12
Size: 282337 Color: 19

Bin 608: 27 of cap free
Amount of items: 3
Items: 
Size: 360585 Color: 9
Size: 350399 Color: 19
Size: 288990 Color: 16

Bin 609: 28 of cap free
Amount of items: 3
Items: 
Size: 369109 Color: 9
Size: 317813 Color: 7
Size: 313051 Color: 16

Bin 610: 29 of cap free
Amount of items: 3
Items: 
Size: 374916 Color: 3
Size: 317629 Color: 4
Size: 307427 Color: 19

Bin 611: 30 of cap free
Amount of items: 3
Items: 
Size: 357492 Color: 16
Size: 341178 Color: 0
Size: 301301 Color: 1

Bin 612: 30 of cap free
Amount of items: 3
Items: 
Size: 360751 Color: 5
Size: 360719 Color: 10
Size: 278501 Color: 17

Bin 613: 30 of cap free
Amount of items: 3
Items: 
Size: 364357 Color: 19
Size: 364349 Color: 12
Size: 271265 Color: 0

Bin 614: 32 of cap free
Amount of items: 3
Items: 
Size: 356361 Color: 3
Size: 325665 Color: 11
Size: 317943 Color: 6

Bin 615: 32 of cap free
Amount of items: 3
Items: 
Size: 373995 Color: 2
Size: 326658 Color: 4
Size: 299316 Color: 11

Bin 616: 33 of cap free
Amount of items: 3
Items: 
Size: 356683 Color: 17
Size: 348976 Color: 13
Size: 294309 Color: 12

Bin 617: 34 of cap free
Amount of items: 3
Items: 
Size: 357112 Color: 7
Size: 324201 Color: 15
Size: 318654 Color: 8

Bin 618: 34 of cap free
Amount of items: 3
Items: 
Size: 370521 Color: 0
Size: 369855 Color: 3
Size: 259591 Color: 9

Bin 619: 34 of cap free
Amount of items: 3
Items: 
Size: 384667 Color: 19
Size: 344301 Color: 16
Size: 270999 Color: 3

Bin 620: 36 of cap free
Amount of items: 3
Items: 
Size: 350374 Color: 16
Size: 349836 Color: 19
Size: 299755 Color: 10

Bin 621: 39 of cap free
Amount of items: 3
Items: 
Size: 372299 Color: 11
Size: 333214 Color: 4
Size: 294449 Color: 0

Bin 622: 40 of cap free
Amount of items: 3
Items: 
Size: 366365 Color: 6
Size: 327419 Color: 13
Size: 306177 Color: 6

Bin 623: 45 of cap free
Amount of items: 3
Items: 
Size: 356404 Color: 8
Size: 323906 Color: 2
Size: 319646 Color: 16

Bin 624: 56 of cap free
Amount of items: 3
Items: 
Size: 364742 Color: 3
Size: 354861 Color: 4
Size: 280342 Color: 9

Bin 625: 66 of cap free
Amount of items: 3
Items: 
Size: 356093 Color: 9
Size: 324001 Color: 5
Size: 319841 Color: 19

Bin 626: 69 of cap free
Amount of items: 3
Items: 
Size: 355787 Color: 13
Size: 335635 Color: 19
Size: 308510 Color: 17

Bin 627: 81 of cap free
Amount of items: 3
Items: 
Size: 344358 Color: 8
Size: 331661 Color: 9
Size: 323901 Color: 7

Bin 628: 81 of cap free
Amount of items: 3
Items: 
Size: 354278 Color: 3
Size: 333062 Color: 3
Size: 312580 Color: 0

Bin 629: 85 of cap free
Amount of items: 3
Items: 
Size: 362265 Color: 6
Size: 319891 Color: 17
Size: 317760 Color: 9

Bin 630: 87 of cap free
Amount of items: 3
Items: 
Size: 359150 Color: 11
Size: 349546 Color: 8
Size: 291218 Color: 19

Bin 631: 90 of cap free
Amount of items: 3
Items: 
Size: 351981 Color: 19
Size: 347304 Color: 10
Size: 300626 Color: 9

Bin 632: 102 of cap free
Amount of items: 3
Items: 
Size: 361420 Color: 4
Size: 324306 Color: 11
Size: 314173 Color: 12

Bin 633: 104 of cap free
Amount of items: 3
Items: 
Size: 355024 Color: 7
Size: 326604 Color: 18
Size: 318269 Color: 2

Bin 634: 108 of cap free
Amount of items: 3
Items: 
Size: 360151 Color: 15
Size: 335436 Color: 2
Size: 304306 Color: 11

Bin 635: 129 of cap free
Amount of items: 3
Items: 
Size: 354081 Color: 8
Size: 332985 Color: 13
Size: 312806 Color: 13

Bin 636: 136 of cap free
Amount of items: 3
Items: 
Size: 356082 Color: 15
Size: 344522 Color: 7
Size: 299261 Color: 6

Bin 637: 144 of cap free
Amount of items: 3
Items: 
Size: 356498 Color: 4
Size: 353824 Color: 2
Size: 289535 Color: 10

Bin 638: 212 of cap free
Amount of items: 3
Items: 
Size: 359468 Color: 9
Size: 342042 Color: 4
Size: 298279 Color: 3

Bin 639: 224 of cap free
Amount of items: 3
Items: 
Size: 356313 Color: 8
Size: 323393 Color: 0
Size: 320071 Color: 3

Bin 640: 230 of cap free
Amount of items: 3
Items: 
Size: 337732 Color: 4
Size: 331708 Color: 17
Size: 330331 Color: 0

Bin 641: 251 of cap free
Amount of items: 3
Items: 
Size: 337332 Color: 8
Size: 331988 Color: 5
Size: 330430 Color: 7

Bin 642: 255 of cap free
Amount of items: 3
Items: 
Size: 351904 Color: 11
Size: 325783 Color: 14
Size: 322059 Color: 13

Bin 643: 277 of cap free
Amount of items: 3
Items: 
Size: 356753 Color: 6
Size: 336570 Color: 15
Size: 306401 Color: 3

Bin 644: 298 of cap free
Amount of items: 3
Items: 
Size: 360666 Color: 0
Size: 354347 Color: 16
Size: 284690 Color: 18

Bin 645: 591 of cap free
Amount of items: 3
Items: 
Size: 341608 Color: 19
Size: 330436 Color: 4
Size: 327366 Color: 16

Bin 646: 656 of cap free
Amount of items: 3
Items: 
Size: 343301 Color: 2
Size: 328388 Color: 1
Size: 327656 Color: 13

Bin 647: 760 of cap free
Amount of items: 3
Items: 
Size: 354994 Color: 15
Size: 345996 Color: 4
Size: 298251 Color: 12

Bin 648: 1708 of cap free
Amount of items: 2
Items: 
Size: 499313 Color: 7
Size: 498980 Color: 17

Bin 649: 1990 of cap free
Amount of items: 3
Items: 
Size: 351902 Color: 17
Size: 324886 Color: 17
Size: 321223 Color: 10

Bin 650: 2509 of cap free
Amount of items: 2
Items: 
Size: 498789 Color: 11
Size: 498703 Color: 14

Bin 651: 3114 of cap free
Amount of items: 2
Items: 
Size: 498654 Color: 15
Size: 498233 Color: 16

Bin 652: 14080 of cap free
Amount of items: 3
Items: 
Size: 354345 Color: 1
Size: 348821 Color: 4
Size: 282755 Color: 12

Bin 653: 17936 of cap free
Amount of items: 3
Items: 
Size: 351856 Color: 5
Size: 351773 Color: 3
Size: 278436 Color: 12

Bin 654: 22756 of cap free
Amount of items: 3
Items: 
Size: 354009 Color: 0
Size: 335574 Color: 11
Size: 287662 Color: 1

Bin 655: 23753 of cap free
Amount of items: 3
Items: 
Size: 353459 Color: 2
Size: 341810 Color: 16
Size: 280979 Color: 5

Bin 656: 38905 of cap free
Amount of items: 3
Items: 
Size: 351116 Color: 19
Size: 349666 Color: 6
Size: 260314 Color: 11

Bin 657: 40768 of cap free
Amount of items: 3
Items: 
Size: 351468 Color: 18
Size: 341928 Color: 4
Size: 265837 Color: 13

Bin 658: 41160 of cap free
Amount of items: 3
Items: 
Size: 349555 Color: 12
Size: 347885 Color: 3
Size: 261401 Color: 16

Bin 659: 41782 of cap free
Amount of items: 3
Items: 
Size: 351347 Color: 14
Size: 341688 Color: 7
Size: 265184 Color: 16

Bin 660: 47026 of cap free
Amount of items: 3
Items: 
Size: 347441 Color: 13
Size: 347431 Color: 1
Size: 258103 Color: 11

Bin 661: 55340 of cap free
Amount of items: 3
Items: 
Size: 346181 Color: 17
Size: 345930 Color: 3
Size: 252550 Color: 19

Bin 662: 56214 of cap free
Amount of items: 3
Items: 
Size: 347186 Color: 18
Size: 340828 Color: 8
Size: 255773 Color: 9

Bin 663: 56823 of cap free
Amount of items: 3
Items: 
Size: 347188 Color: 6
Size: 337895 Color: 4
Size: 258095 Color: 12

Bin 664: 64276 of cap free
Amount of items: 3
Items: 
Size: 341218 Color: 1
Size: 340160 Color: 19
Size: 254347 Color: 0

Bin 665: 68063 of cap free
Amount of items: 3
Items: 
Size: 338192 Color: 15
Size: 338059 Color: 17
Size: 255687 Color: 7

Bin 666: 71830 of cap free
Amount of items: 3
Items: 
Size: 339535 Color: 6
Size: 332745 Color: 9
Size: 255891 Color: 11

Bin 667: 82298 of cap free
Amount of items: 3
Items: 
Size: 334144 Color: 15
Size: 332458 Color: 10
Size: 251101 Color: 9

Bin 668: 241081 of cap free
Amount of items: 3
Items: 
Size: 255390 Color: 11
Size: 252666 Color: 16
Size: 250864 Color: 18

Total size: 667000667
Total free space: 1000001

