Capicity Bin: 2492
Lower Bound: 65

Bins used: 65
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 2018 Color: 1
Size: 398 Color: 1
Size: 40 Color: 0
Size: 36 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1278 Color: 1
Size: 1038 Color: 1
Size: 148 Color: 0
Size: 24 Color: 0
Size: 4 Color: 0

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1362 Color: 1
Size: 1014 Color: 1
Size: 108 Color: 0
Size: 8 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 1
Size: 934 Color: 1
Size: 184 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2173 Color: 1
Size: 267 Color: 1
Size: 52 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 806 Color: 1
Size: 112 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 1
Size: 282 Color: 1
Size: 52 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 1
Size: 254 Color: 1
Size: 48 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 1
Size: 397 Color: 1
Size: 78 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1875 Color: 1
Size: 515 Color: 1
Size: 102 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2029 Color: 1
Size: 387 Color: 1
Size: 76 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2222 Color: 1
Size: 226 Color: 1
Size: 44 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 1
Size: 505 Color: 1
Size: 100 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1946 Color: 1
Size: 458 Color: 1
Size: 88 Color: 0

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 1228 Color: 1
Size: 932 Color: 1
Size: 188 Color: 0
Size: 144 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 1
Size: 669 Color: 1
Size: 132 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 650 Color: 1
Size: 128 Color: 0

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 1088 Color: 1
Size: 852 Color: 1
Size: 352 Color: 0
Size: 200 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2175 Color: 1
Size: 265 Color: 1
Size: 52 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 1
Size: 585 Color: 1
Size: 116 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 1
Size: 889 Color: 1
Size: 176 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2117 Color: 1
Size: 313 Color: 1
Size: 62 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1253 Color: 1
Size: 1033 Color: 1
Size: 206 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 1
Size: 650 Color: 1
Size: 116 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 1
Size: 887 Color: 1
Size: 176 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 1
Size: 542 Color: 1
Size: 104 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 530 Color: 1
Size: 104 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2126 Color: 1
Size: 306 Color: 1
Size: 60 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 630 Color: 1
Size: 124 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 1
Size: 994 Color: 1
Size: 196 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2121 Color: 1
Size: 311 Color: 1
Size: 60 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 1
Size: 550 Color: 1
Size: 108 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 1
Size: 1031 Color: 1
Size: 204 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 1
Size: 345 Color: 1
Size: 68 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 1
Size: 405 Color: 1
Size: 80 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2149 Color: 1
Size: 287 Color: 1
Size: 56 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 1
Size: 346 Color: 1
Size: 68 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 769 Color: 1
Size: 152 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1249 Color: 1
Size: 1037 Color: 1
Size: 206 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 1
Size: 274 Color: 1
Size: 52 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2021 Color: 1
Size: 447 Color: 1
Size: 24 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 1
Size: 338 Color: 1
Size: 64 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 614 Color: 1
Size: 120 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 1
Size: 285 Color: 1
Size: 56 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 1247 Color: 1
Size: 1133 Color: 1
Size: 112 Color: 0

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 2000 Color: 1
Size: 212 Color: 1
Size: 204 Color: 0
Size: 76 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 1
Size: 466 Color: 1
Size: 92 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 1879 Color: 1
Size: 511 Color: 1
Size: 102 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 1
Size: 667 Color: 1
Size: 132 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 1
Size: 358 Color: 1
Size: 68 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 1
Size: 406 Color: 1
Size: 80 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 2234 Color: 1
Size: 218 Color: 1
Size: 40 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 2109 Color: 1
Size: 321 Color: 1
Size: 62 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 782 Color: 1
Size: 156 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 1
Size: 942 Color: 1
Size: 300 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 1
Size: 750 Color: 1
Size: 148 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 2217 Color: 1
Size: 273 Color: 1
Size: 2 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 1
Size: 802 Color: 1
Size: 160 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 1
Size: 443 Color: 1
Size: 88 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 2197 Color: 1
Size: 247 Color: 1
Size: 48 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 1949 Color: 1
Size: 453 Color: 1
Size: 90 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 2199 Color: 1
Size: 271 Color: 1
Size: 22 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 2067 Color: 1
Size: 355 Color: 1
Size: 70 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 583 Color: 1
Size: 116 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 1
Size: 767 Color: 1
Size: 152 Color: 0

Total size: 161980
Total free space: 0

